unit fTipoFrequenciaDistribuicao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, ER2Lookup, cxContainer, cxEdit,
  cxGroupBox, cxRadioGroup, cxPC, cxControls;

type
  TfrmTipoFrequenciaDistribuicao = class(TfrmCadastro_Padrao)
    qryMasternCdTipoFrequenciaDistribuicao: TIntegerField;
    qryMastercNmTipoFrequenciaDistribuicao: TStringField;
    qryMasternCdTabTipoFrequenciaDistribuicao: TIntegerField;
    qryMastercFlgSegunda: TIntegerField;
    qryMastercFlgTerca: TIntegerField;
    qryMastercFlgQuarta: TIntegerField;
    qryMastercFlgQuinta: TIntegerField;
    qryMastercFlgSexta: TIntegerField;
    qryMastercFlgSabado: TIntegerField;
    qryMastercFlgDomingo: TIntegerField;
    qryMasteriDiaDistribuicao: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    ER2LookupDBEdit1: TER2LookupDBEdit;
    qryTabTipoFrequenciaDistribuicao: TADOQuery;
    dsTabTipoFrequenciaDistribuicao: TDataSource;
    qryTabTipoFrequenciaDistribuicaonCdTabTipoFrequenciaDistribuicao: TIntegerField;
    qryTabTipoFrequenciaDistribuicaocNmTabTipoFrequenciaDistribuicao: TStringField;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    pageFrequencia: TcxPageControl;
    tabDiaria: TcxTabSheet;
    tabSemanal: TcxTabSheet;
    tabQuinzenal: TcxTabSheet;
    tabMensal: TcxTabSheet;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    rgSemanal: TcxRadioGroup;
    rbConsiderarUltimaDistribuicao: TcxRadioButton;
    rbDiaDoMes: TcxRadioButton;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    rbUltimaDistrib: TcxRadioButton;
    qryMastercFlgConsiderarUltimaDistribuicao: TIntegerField;
    Label5: TLabel;
    Label6: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure desabilitaAbas;
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryTabTipoFrequenciaDistribuicaoAfterOpen(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure rbDiaDoMesClick(Sender: TObject);
    procedure rbConsiderarUltimaDistribuicaoClick(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoFrequenciaDistribuicao: TfrmTipoFrequenciaDistribuicao;

implementation

uses
    fMenu;

{$R *.dfm}

procedure TfrmTipoFrequenciaDistribuicao.desabilitaAbas;
begin
    tabDiaria.Enabled    := False;
    tabSemanal.Enabled   := False;
    tabQuinzenal.Enabled := False;
    tabMensal.Enabled    := False;
end;

procedure TfrmTipoFrequenciaDistribuicao.FormCreate(Sender: TObject);
begin
    inherited;

    nCdTabelaSistema  := 532;
    nCdConsultaPadrao := 781;
    cNmTabelaMaster   := 'TIPOFREQUENCIADISTRIBUICAO';
end;

procedure TfrmTipoFrequenciaDistribuicao.qryMasterBeforePost(
  DataSet: TDataSet);
var
    i, qtdDias : integer;
begin
    // Valida a descrição do Tipo de Frequência
    if (Trim(qryMastercNmTipoFrequenciaDistribuicao.Value) = '') then
    begin
        MensagemAlerta('Informe a descrição do Tipo de Frequência de Distribuição.');
        DBEdit2.SetFocus;
        Abort;
    end;

    // Valida a tipificação do Tipo de Frequência
    if (qryTabTipoFrequenciaDistribuicao.IsEmpty) then
    begin
        MensagemAlerta('Informe o Tipo de Frequência do Tipo de Frequência de Distribuição.');
        ER2LookupDBEdit1.SetFocus;
        Abort;
    end;

    if (Trim(ER2LookupDBEdit1.Text) <> qryTabTipoFrequenciaDistribuicaonCdTabTipoFrequenciaDistribuicao.AsString) then
    begin
        qryTabTipoFrequenciaDistribuicao.Close;
        PosicionaQuery(qryTabTipoFrequenciaDistribuicao, ER2LookupDBEdit1.Text);

        if (qryTabTipoFrequenciaDistribuicao.IsEmpty) then
        begin
            MensagemAlerta('Informe a tipificação do Tipo de Frequência de Distribuição.');
            ER2LookupDBEdit1.SetFocus;
            Abort;
        end;
    end;

    // Valida se foi selecionado mais de 1 dia quando a tipificação for Diária
    if (qryTabTipoFrequenciaDistribuicaonCdTabTipoFrequenciaDistribuicao.Value = 1) then
    begin
        qtdDias := 0;

        for i := 0 to ComponentCount - 1 do
        begin
            if (Components[i] is TDBCheckBox) then
                if ((Components[i] as TDBCheckBox).Checked) then
                    Inc(qtdDias);
        end;

        if (qtdDias <= 1) then
        begin
            MensagemAlerta('Selecione mais de um dia da semana para a Distribuição Diária.');
            Abort;
        end;
    end;

    // Reseta os registros de dias da semana
    if (qryTabTipoFrequenciaDistribuicaonCdTabTipoFrequenciaDistribuicao.Value <> 1) then
    begin
        qryMastercFlgSegunda.Value := 0;
        qryMastercFlgTerca.Value   := 0;
        qryMastercFlgQuarta.Value  := 0;
        qryMastercFlgQuinta.Value  := 0;
        qryMastercFlgSexta.Value   := 0;
        qryMastercFlgSabado.Value  := 0;
        qryMastercFlgDomingo.Value := 0;
    end;

    // Valida o dia para a distribuição mensal
    if (qryTabTipoFrequenciaDistribuicaonCdTabTipoFrequenciaDistribuicao.Value = 4) then
    begin
        if (rbConsiderarUltimaDistribuicao.Checked) then
            qryMastercFlgConsiderarUltimaDistribuicao.Value := 1
        else
            qryMastercFlgConsiderarUltimaDistribuicao.Value := 0;

        if (qryMastercFlgConsiderarUltimaDistribuicao.Value = 0) then
        begin
            if (qryMasteriDiaDistribuicao.IsNull) then
            begin
                MensagemAlerta('Informe o dia para a Distribuição Mensal.');
                DBEdit4.SetFocus;
                Abort;
            end;

            if ((qryMasteriDiaDistribuicao.Value <= 0) or (qryMasteriDiaDistribuicao.Value > 31)) then
            begin
                MensagemAlerta('Informe um dia mensal válido (1 a 31).');
                DBEdit4.SetFocus;
                Abort;
            end;
        end
        else
            qryMasteriDiaDistribuicao.Clear;
    end;

    // Trata a Distribuição Semanal
    if (qryTabTipoFrequenciaDistribuicaonCdTabTipoFrequenciaDistribuicao.Value = 2) then
    begin
        // Atribui o dia da semana selecionado
        case (rgSemanal.ItemIndex) of
        0: qryMastercFlgSegunda.Value := 1;
        1: qryMastercFlgTerca.Value   := 1;
        2: qryMastercFlgQuarta.Value  := 1;
        3: qryMastercFlgQuinta.Value  := 1;
        4: qryMastercFlgSexta.Value   := 1;
        5: qryMastercFlgSabado.Value  := 1;
        6: qryMastercFlgDomingo.Value := 1;
        end;
    end;

    // Trata a Distribuição Quinzenal
    if (qryTabTipoFrequenciaDistribuicaonCdTabTipoFrequenciaDistribuicao.Value = 3) then
        qryMastercFlgConsiderarUltimaDistribuicao.Value := 1;

    inherited;
end;

procedure TfrmTipoFrequenciaDistribuicao.qryTabTipoFrequenciaDistribuicaoAfterOpen(
  DataSet: TDataSet);
begin
    inherited;

    if not (qryTabTipoFrequenciaDistribuicao.IsEmpty) then
    begin
        desabilitaAbas;

        case (qryTabTipoFrequenciaDistribuicaonCdTabTipoFrequenciaDistribuicao.Value) of

        // Aba Diária
        1: begin
               tabDiaria.Enabled         := True;
               pageFrequencia.ActivePage := tabDiaria;
           end;

        // Aba Semanal
        2: begin
               if (qryMastercFlgSegunda.Value = 1) then rgSemanal.ItemIndex := 0;
               if (qryMastercFlgTerca.Value   = 1) then rgSemanal.ItemIndex := 1;
               if (qryMastercFlgQuarta.Value  = 1) then rgSemanal.ItemIndex := 2;
               if (qryMastercFlgQuinta.Value  = 1) then rgSemanal.ItemIndex := 3;
               if (qryMastercFlgSexta.Value   = 1) then rgSemanal.ItemIndex := 4;
               if (qryMastercFlgSabado.Value  = 1) then rgSemanal.ItemIndex := 5;
               if (qryMastercFlgDomingo.Value = 1) then rgSemanal.ItemIndex := 6;

               tabSemanal.Enabled        := True;
               pageFrequencia.ActivePage := tabSemanal;
           end;

        // Aba Quinzenal
        3: begin
               rbUltimaDistrib.Checked   := True;

               tabQuinzenal.Enabled      := True;
               pageFrequencia.ActivePage := tabQuinzenal;
           end;

        // Aba Mensal
        4: begin
               tabMensal.Enabled         := True;
               pageFrequencia.ActivePage := tabMensal;

               if (qryMastercFlgConsiderarUltimaDistribuicao.Value = 0) then
               begin
                   rbDiaDoMes.Checked := True;
                   DBEdit4.Enabled    := True;
               end

               else
               begin
                  rbConsiderarUltimaDistribuicao.Checked := True;
                  DBEdit4.Enabled                        := False;
               end;
           end;
        end;

        pageFrequencia.Enabled := True;
        pageFrequencia.ActivePage.SetFocus;
    end

    else
    begin
        pageFrequencia.ActivePage := tabDiaria;
        pageFrequencia.Enabled    := False;
    end;
end;

procedure TfrmTipoFrequenciaDistribuicao.qryMasterAfterScroll(
  DataSet: TDataSet);
begin
    inherited;

    if not (qryMasternCdTabTipoFrequenciaDistribuicao.IsNull) then
    begin
        qryTabTipoFrequenciaDistribuicao.Close;
        PosicionaQuery(qryTabTipoFrequenciaDistribuicao, qryMasternCdTabTipoFrequenciaDistribuicao.AsString);
    end
    else
    begin
        qryTabTipoFrequenciaDistribuicao.Close;

        pageFrequencia.ActivePage := tabDiaria;
        pageFrequencia.Enabled    := False;

        DBEdit2.SetFocus;
    end;

end;

procedure TfrmTipoFrequenciaDistribuicao.rbDiaDoMesClick(Sender: TObject);
begin
    inherited;

    DBEdit4.Enabled := True;
end;

procedure TfrmTipoFrequenciaDistribuicao.rbConsiderarUltimaDistribuicaoClick(
  Sender: TObject);
begin
    inherited;

    DBEdit4.Clear;
    DBEdit4.Enabled := False;
    DBEdit4.SetFocus;
end;

procedure TfrmTipoFrequenciaDistribuicao.qryMasterAfterClose(
  DataSet: TDataSet);
begin
    inherited;

    qryTabTipoFrequenciaDistribuicao.Close;

    pageFrequencia.ActivePage := tabDiaria;
    pageFrequencia.Enabled    := False;
end;

initialization
    RegisterClass(TfrmTipoFrequenciaDistribuicao);

end.
