unit fTabCFOP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, ER2Lookup;

type
  TfrmTabCFOP = class(TfrmCadastro_Padrao)
    qryMastercCFOP: TStringField;
    qryMastercCFOPPai: TStringField;
    qryMastercNmCFOP: TStringField;
    qryMastercFlgGeraLivroFiscal: TIntegerField;
    qryMastercFlgGeraCreditoICMS: TIntegerField;
    qryMastercFlgGeraCreditoIPI: TIntegerField;
    qryMastercFlgGeraCreditoPIS: TIntegerField;
    qryMastercFlgGeraCreditoCOFINS: TIntegerField;
    qryMastercFlgImportacao: TIntegerField;
    qryMasternCdTipoICMS: TIntegerField;
    qryMasternCdTipoIPI: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    GroupBox1: TGroupBox;
    DBCheckBox4: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    qryMasternCdCFOP: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTabCFOP: TfrmTabCFOP;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmTabCFOP.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TABCFOP' ;
  nCdTabelaSistema  := 318 ;
  nCdConsultaPadrao := 747 ;
end;

procedure TfrmTabCFOP.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Digite o CFOP.');
      DBEdit2.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit4.Text) = '') then
  begin
      MensagemAlerta('Digite a descri��o do CFOP');
      DBEdit2.SetFocus;
      Abort;
  end;

  inherited;

end;

procedure TfrmTabCFOP.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
end;

initialization
    RegisterClass(TfrmTabCFOP);

end.
