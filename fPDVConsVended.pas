unit fPDVConsVended;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxLookAndFeelPainters, StdCtrls,
  cxButtons, jpeg, ExtCtrls, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, ADODB;

type
  TfrmPDVConsVended = class(TForm)
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Image1: TImage;
    btSeleciona: TcxButton;
    btFechar: TcxButton;
    qryVendedor: TADOQuery;
    dsVendedor: TDataSource;
    qryVendedornCdUsuario: TIntegerField;
    qryVendedorcNmUsuario: TStringField;
    cxGrid1DBTableView1nCdUsuario: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure btFecharClick(Sender: TObject);
    procedure btSelecionaClick(Sender: TObject);
    procedure ConsultaVendedor;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPDVConsVended: TfrmPDVConsVended;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmPDVConsVended.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case key of

      vk_return : btSeleciona.Click;
      VK_ESCAPE : btFechar.Click;

  end

end;

procedure TfrmPDVConsVended.FormShow(Sender: TObject);
begin
    cxGrid1.SetFocus;
end;

procedure TfrmPDVConsVended.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        btSeleciona.Click;

end;

procedure TfrmPDVConsVended.btFecharClick(Sender: TObject);
begin
    qryVendedor.Close ;
    close ;
end;

procedure TfrmPDVConsVended.btSelecionaClick(Sender: TObject);
begin
    Close;
end;

procedure TfrmPDVConsVended.ConsultaVendedor;
begin
    qryVendedor.Close ;
    qryVendedor.Parameters.ParamByName('nCdLoja').Value       := frmMenu.nCdLojaAtiva;
    qryVendedor.Open;

    Self.ShowModal;
end;

end.
