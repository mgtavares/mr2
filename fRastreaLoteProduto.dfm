inherited frmRastreaLoteProduto: TfrmRastreaLoteProduto
  Left = -7
  Top = 16
  Width = 1152
  Height = 786
  Caption = 'Rastreamento de Lote'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 145
    Width = 1136
    Height = 271
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    ButtonWidth = 71
    inherited ToolButton1: TToolButton
      Caption = 'Consultar'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 71
    end
    inherited ToolButton2: TToolButton
      Left = 79
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 116
    Align = alTop
    TabOrder = 1
    object Label5: TLabel
      Tag = 1
      Left = 53
      Top = 24
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Produto'
    end
    object Label1: TLabel
      Tag = 1
      Left = 14
      Top = 96
      Width = 77
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Lote/S'#233'rie'
    end
    object Label2: TLabel
      Tag = 1
      Left = 52
      Top = 48
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro'
    end
    object Label3: TLabel
      Tag = 1
      Left = 22
      Top = 72
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo Movto'
    end
    object Label6: TLabel
      Tag = 1
      Left = 185
      Top = 72
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object edtProduto: TMaskEdit
      Left = 96
      Top = 16
      Width = 89
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      OnChange = edtProdutoChange
      OnExit = edtProdutoExit
      OnKeyDown = edtProdutoKeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 191
      Top = 16
      Width = 866
      Height = 21
      DataField = 'cNmProduto'
      DataSource = DataSource1
      TabOrder = 5
    end
    object edtNumLoteSerie: TEdit
      Left = 96
      Top = 88
      Width = 121
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 30
      TabOrder = 4
      OnChange = edtNumLoteSerieChange
    end
    object edtCdTerceiro: TER2LookupMaskEdit
      Left = 96
      Top = 40
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 1
      Text = '         '
      OnChange = edtCdTerceiroChange
      CodigoLookup = 17
      QueryLookup = qryTerceiro
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 191
      Top = 40
      Width = 654
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = DataSource2
      TabOrder = 6
    end
    object edtDtInicial: TMaskEdit
      Left = 96
      Top = 64
      Width = 81
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
      OnChange = edtDtInicialChange
    end
    object edtDtFinal: TMaskEdit
      Left = 208
      Top = 64
      Width = 81
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
      OnChange = edtDtFinalChange
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 145
    Width = 1136
    Height = 271
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 267
    ClientRectLeft = 4
    ClientRectRight = 1132
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Lotes'
      ImageIndex = 0
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 1128
        Height = 243
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView1: TcxGridDBTableView
          DataController.DataSource = dsLoteProd
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView1nCdLoteProduto: TcxGridDBColumn
            DataBinding.FieldName = 'nCdLoteProduto'
            Visible = False
          end
          object cxGridDBTableView1cNrLote: TcxGridDBColumn
            Caption = 'N'#250'm. Lote/S'#233'rie'
            DataBinding.FieldName = 'cNrLote'
            Width = 114
          end
          object cxGridDBTableView1dDtFabricacao: TcxGridDBColumn
            Caption = 'Fabrica'#231#227'o'
            DataBinding.FieldName = 'dDtFabricacao'
            Width = 100
          end
          object cxGridDBTableView1dDtDisponibilidade: TcxGridDBColumn
            Caption = 'Disponibilidade'
            DataBinding.FieldName = 'dDtDisponibilidade'
            Width = 115
          end
          object cxGridDBTableView1dDtValidade: TcxGridDBColumn
            Caption = 'Validade'
            DataBinding.FieldName = 'dDtValidade'
            Width = 100
          end
          object cxGridDBTableView1nQtdeTotal: TcxGridDBColumn
            Caption = 'Qtde. Total'
            DataBinding.FieldName = 'nQtdeTotal'
            Width = 89
          end
          object cxGridDBTableView1nSaldoLote: TcxGridDBColumn
            Caption = 'Saldo Lote'
            DataBinding.FieldName = 'nSaldoLote'
            Width = 85
          end
          object cxGridDBTableView1cOBSLote: TcxGridDBColumn
            Caption = 'Observa'#231#227'o'
            DataBinding.FieldName = 'cOBSLote'
            Width = 200
          end
          object cxGridDBTableView1nCdLocalEstoque: TcxGridDBColumn
            DataBinding.FieldName = 'nCdLocalEstoque'
            Visible = False
          end
          object cxGridDBTableView1cNmLocalEstoque: TcxGridDBColumn
            Caption = 'Local Estoque'
            DataBinding.FieldName = 'cNmLocalEstoque'
            Width = 100
          end
          object cxGridDBTableView1cFlgBloqueado: TcxGridDBColumn
            Caption = 'Bloqueado ?'
            DataBinding.FieldName = 'cFlgBloqueado'
            Width = 90
          end
          object cxGridDBTableView1cNmStatus: TcxGridDBColumn
            Caption = 'Status'
            DataBinding.FieldName = 'cNmStatus'
            Width = 100
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  object cxPageControl2: TcxPageControl [4]
    Left = 0
    Top = 416
    Width = 1136
    Height = 334
    ActivePage = cxTabSheet2
    Align = alBottom
    LookAndFeel.NativeStyle = True
    TabOrder = 3
    ClientRectBottom = 330
    ClientRectLeft = 4
    ClientRectRight = 1132
    ClientRectTop = 24
    object cxTabSheet2: TcxTabSheet
      Caption = 'Movimenta'#231#245'es'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1128
        Height = 306
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView2: TcxGridDBTableView
          DataController.DataSource = dsMovto
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView2dDtMovto: TcxGridDBColumn
            Caption = 'Data Movto'
            DataBinding.FieldName = 'dDtMovto'
          end
          object cxGridDBTableView2cNmTabTipoOrigemMovLote: TcxGridDBColumn
            Caption = 'Origem Movimento'
            DataBinding.FieldName = 'cNmTabTipoOrigemMovLote'
            Width = 121
          end
          object cxGridDBTableView2iIDRegistro: TcxGridDBColumn
            Caption = 'ID Registro'
            DataBinding.FieldName = 'iIDRegistro'
            Width = 94
          end
          object cxGridDBTableView2cNmTerceiro: TcxGridDBColumn
            Caption = 'Terceiro'
            DataBinding.FieldName = 'cNmTerceiro'
            Width = 100
          end
          object cxGridDBTableView2cNmUsuario: TcxGridDBColumn
            Caption = 'Usu'#225'rio Movto'
            DataBinding.FieldName = 'cNmUsuario'
            Width = 100
          end
          object cxGridDBTableView2nSaldoAnterior: TcxGridDBColumn
            Caption = 'Saldo Anterior'
            DataBinding.FieldName = 'nSaldoAnterior'
            Width = 115
          end
          object cxGridDBTableView2nQtdeMov: TcxGridDBColumn
            Caption = 'Qtde. Movto.'
            DataBinding.FieldName = 'nQtdeMov'
            Width = 115
          end
          object cxGridDBTableView2nSaldoPosterior: TcxGridDBColumn
            Caption = 'Saldo Posterior'
            DataBinding.FieldName = 'nSaldoPosterior'
            Width = 115
          end
          object cxGridDBTableView2cOBSMov: TcxGridDBColumn
            Caption = 'Observa'#231#227'o'
            DataBinding.FieldName = 'cOBSMov'
            Width = 100
          end
          object cxGridDBTableView2cFlgDirecao: TcxGridDBColumn
            Caption = 'Dire'#231#227'o'
            DataBinding.FieldName = 'cFlgDirecao'
            Width = 100
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C00636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF81C00000FE00FFFF01800000
      FE00C00300000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010181000000018001818100000003800181810000
      0077C00381810000007FFFFF83830000}
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmProduto'
      '  FROM Produto'
      ' WHERE nCdTabTipoModoGestaoProduto > 1 '
      
        '   AND NOT EXISTS(SELECT 1 FROM Produto Filho WHERE Filho.nCdPro' +
        'dutoPai = Produto.nCdProduto)'
      '   AND nCdProduto = :nPK')
    Left = 408
    Top = 136
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object DataSource1: TDataSource
    DataSet = qryProduto
    Left = 424
    Top = 240
  end
  object qryLoteProd: TADOQuery
    Connection = frmMenu.Connection
    AfterClose = qryLoteProdAfterClose
    AfterScroll = qryLoteProdAfterScroll
    Parameters = <
      item
        Name = 'nCdProduto'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cNrLote'
        DataType = ftString
        Precision = 20
        Size = 20
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    SQL.Strings = (
      'DECLARE @nCdProduto  int'
      '       ,@nCdTerceiro int'
      '       ,@cNrLote     varchar(30)'
      '       ,@dDtInicial  varchar(10)'
      '       ,@dDtFinal    varchar(10)'
      ''
      'Set @nCdProduto  = :nCdProduto'
      'Set @nCdTerceiro = :nCdTerceiro'
      'Set @cNrLote     = :cNrLote'
      'Set @dDtInicial  = :dDtInicial'
      'Set @dDtFinal    = :dDtFinal'
      ''
      'SELECT DISTINCT Lote.nCdLoteProduto'
      '      ,Lote.cNrLote'
      '      ,Lote.dDtFabricacao'
      '      ,Lote.dDtDisponibilidade'
      '      ,Lote.dDtValidade'
      '      ,Lote.nQtdeTotal'
      '      ,Lote.nSaldoLote'
      '      ,Lote.cOBS as cOBSLote'
      '      ,LocalEstoque.nCdLocalEstoque'
      '      ,LocalEstoque.cNmLocalEstoque'
      '      ,CASE WHEN cFlgBloqueado = 1 THEN '#39'Sim'#39
      '            ELSE NULL'
      '       END cFlgBloqueado'
      '      ,Status.cNmStatus'
      '  FROM MovLoteProduto Mov'
      
        '       INNER JOIN LoteProduto  Lote     ON Lote.nCdLoteProduto  ' +
        '                        = Mov.nCdLoteProduto'
      
        '       LEFT  JOIN LocalEstoque          ON LocalEstoque.nCdLocal' +
        'Estoque                 = Lote.nCdLocalEstoque'
      
        '       LEFT  JOIN Status                ON Status.nCdStatus     ' +
        '                        = Lote.nCdStatus'
      
        '       LEFT  JOIN TabTipoOrigemMovLote  ON TabTipoOrigemMovLote.' +
        'nCdTabTipoOrigemMovLote = Mov.nCdTabTipoOrigemMovLote'
      
        '       LEFT  JOIN Terceiro              ON Terceiro.nCdTerceiro ' +
        '                        = Mov.nCdTerceiro'
      
        '       LEFT  JOIN Usuario               ON Usuario.nCdUsuario   ' +
        '                        = Mov.nCdUsuario'
      ' WHERE Mov.nCdProduto     = @nCdProduto'
      '   AND ((Mov.nCdTerceiro  = @nCdTerceiro) OR (@nCdTerceiro = 0))'
      
        '   AND ((Lote.cNrLote     = @cNrLote)     OR (RTRIM( LTRIM( IsNu' +
        'll(@cNrLote,'#39#39') ) ) = '#39#39' ))'
      
        '   AND ((Mov.dDtMovto    >= Convert(DATETIME,@dDtInicial,103))  ' +
        ' OR (@dDtInicial = '#39'01/01/1900'#39'))'
      
        '   AND ((Mov.dDtMovto     < (Convert(DATETIME,@dDtFinal,103)+1))' +
        ' OR (@dDtFinal   = '#39'01/01/1900'#39'))'
      ' ORDER BY cNrLote'
      '')
    Left = 808
    Top = 144
    object qryLoteProdnCdLoteProduto: TAutoIncField
      FieldName = 'nCdLoteProduto'
      ReadOnly = True
    end
    object qryLoteProdcNrLote: TStringField
      FieldName = 'cNrLote'
      Size = 30
    end
    object qryLoteProddDtFabricacao: TDateTimeField
      FieldName = 'dDtFabricacao'
    end
    object qryLoteProddDtDisponibilidade: TDateTimeField
      FieldName = 'dDtDisponibilidade'
    end
    object qryLoteProddDtValidade: TDateTimeField
      FieldName = 'dDtValidade'
    end
    object qryLoteProdnQtdeTotal: TBCDField
      FieldName = 'nQtdeTotal'
      Precision = 12
      Size = 2
    end
    object qryLoteProdnSaldoLote: TBCDField
      FieldName = 'nSaldoLote'
      Precision = 12
      Size = 2
    end
    object qryLoteProdcOBSLote: TStringField
      FieldName = 'cOBSLote'
      Size = 100
    end
    object qryLoteProdnCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLoteProdcNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
    object qryLoteProdcFlgBloqueado: TStringField
      FieldName = 'cFlgBloqueado'
      ReadOnly = True
      Size = 3
    end
    object qryLoteProdcNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object dsLoteProd: TDataSource
    DataSet = qryLoteProd
    Left = 856
    Top = 144
  end
  object qryMovto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoteProduto'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoteProduto  int'
      '       ,@nCdTerceiro     int'
      '       ,@dDtInicial      varchar(10)'
      '       ,@dDtFinal        varchar(10)'
      ''
      'Set @nCdLoteProduto  = :nCdLoteProduto'
      'Set @nCdTerceiro     = :nCdTerceiro'
      'Set @dDtInicial      = :dDtInicial'
      'Set @dDtFinal        = :dDtFinal'
      ''
      'SELECT Mov.dDtMovto'
      '      ,cNmTabTipoOrigemMovLote'
      '      ,Mov.iIDRegistro'
      '      ,cNmTerceiro'
      '      ,cNmUsuario'
      '      ,Mov.nSaldoAnterior'
      '      ,Mov.nQtde as nQtdeMov'
      '      ,Mov.nSaldoPosterior'
      '      ,Mov.cOBS  as cOBSMov'
      
        '      ,CASE WHEN TabTipoOrigemMovLote.cFlgEntradaSaida = '#39'E'#39' THE' +
        'N '#39'Entrada'#39
      '            ELSE '#39'Sa'#237'da'#39
      '       END cFlgDirecao      '
      '  FROM MovLoteProduto Mov'
      
        '       LEFT  JOIN TabTipoOrigemMovLote  ON TabTipoOrigemMovLote.' +
        'nCdTabTipoOrigemMovLote = Mov.nCdTabTipoOrigemMovLote'
      
        '       LEFT  JOIN Terceiro              ON Terceiro.nCdTerceiro ' +
        '                        = Mov.nCdTerceiro'
      
        '       LEFT  JOIN Usuario               ON Usuario.nCdUsuario   ' +
        '                        = Mov.nCdUsuario'
      ' WHERE Mov.nCdLoteProduto = @nCdLoteProduto'
      '   AND ((Mov.nCdTerceiro  = @nCdTerceiro) OR (@nCdTerceiro = 0))'
      
        '   AND ((Mov.dDtMovto    >= Convert(DATETIME,@dDtInicial,103))  ' +
        ' OR (@dDtInicial = '#39'01/01/1900'#39'))'
      
        '   AND ((Mov.dDtMovto     < (Convert(DATETIME,@dDtFinal,103)+1))' +
        ' OR (@dDtFinal   = '#39'01/01/1900'#39'))'
      ' ORDER BY dDtMovto')
    Left = 824
    Top = 360
    object qryMovtodDtMovto: TDateTimeField
      FieldName = 'dDtMovto'
    end
    object qryMovtocNmTabTipoOrigemMovLote: TStringField
      FieldName = 'cNmTabTipoOrigemMovLote'
      Size = 50
    end
    object qryMovtoiIDRegistro: TIntegerField
      FieldName = 'iIDRegistro'
    end
    object qryMovtocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMovtocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryMovtonSaldoAnterior: TBCDField
      FieldName = 'nSaldoAnterior'
      Precision = 12
    end
    object qryMovtonQtdeMov: TBCDField
      FieldName = 'nQtdeMov'
      Precision = 12
      Size = 2
    end
    object qryMovtonSaldoPosterior: TBCDField
      FieldName = 'nSaldoPosterior'
      Precision = 12
    end
    object qryMovtocOBSMov: TStringField
      FieldName = 'cOBSMov'
      Size = 50
    end
    object qryMovtocFlgDirecao: TStringField
      FieldName = 'cFlgDirecao'
      ReadOnly = True
      Size = 7
    end
  end
  object dsMovto: TDataSource
    DataSet = qryMovto
    Left = 872
    Top = 360
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 456
    Top = 125
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryTerceiro
    Left = 560
    Top = 376
  end
end
