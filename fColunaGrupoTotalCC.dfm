inherited frmColunaGrupoTotalCC: TfrmColunaGrupoTotalCC
  Left = 318
  Top = 288
  VertScrollBar.Range = 0
  BorderStyle = bsToolWindow
  Caption = 'Totalizador Centro Custo'
  ClientHeight = 349
  ClientWidth = 649
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 649
    Height = 320
  end
  object Label1: TLabel [1]
    Left = 16
    Top = 44
    Width = 102
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Totalizador'
    FocusControl = DBEdit1
  end
  inherited ToolBar1: TToolBar
    Width = 649
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit1: TDBEdit [3]
    Left = 126
    Top = 36
    Width = 510
    Height = 21
    DataField = 'cNmColunaGrupoTotalCC'
    DataSource = DataSource1
    TabOrder = 1
    OnKeyDown = DBEdit1KeyDown
  end
  object cxPageControl1: TcxPageControl [4]
    Left = 16
    Top = 68
    Width = 619
    Height = 257
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 253
    ClientRectLeft = 4
    ClientRectRight = 615
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Centros de Custo'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 611
        Height = 229
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsCentroCustoColunaGrupoTotalCC
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        OnKeyDown = DBGridEh1KeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdCC'
            Footers = <>
            Title.Caption = 'C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmCC'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Descri'#231#227'o'
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 368
  end
  object qryColunaGrupoTotalCC: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryColunaGrupoTotalCCBeforePost
    AfterScroll = qryColunaGrupoTotalCCAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdColunaGrupoTotalCC'
      '      ,nCdGrupoTotalCC'
      '      ,cNmColunaGrupoTotalCC'
      '  FROM ColunaGrupoTotalCC'
      ' WHERE nCdColunaGrupoTotalCC = :nPK')
    Left = 496
    Top = 32
    object qryColunaGrupoTotalCCnCdColunaGrupoTotalCC: TIntegerField
      FieldName = 'nCdColunaGrupoTotalCC'
    end
    object qryColunaGrupoTotalCCnCdGrupoTotalCC: TIntegerField
      FieldName = 'nCdGrupoTotalCC'
    end
    object qryColunaGrupoTotalCCcNmColunaGrupoTotalCC: TStringField
      FieldName = 'cNmColunaGrupoTotalCC'
      Size = 50
    end
  end
  object qryCentroCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC'
      '      ,cNmCC'
      '  FROM CentroCusto'
      ' WHERE nCdCC  = :nPK'
      '   AND iNivel = 3')
    Left = 176
    Top = 232
    object qryCentroCustonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCentroCustocNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object qryCentroCustoColunaGrupoTotalCC: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCentroCustoColunaGrupoTotalCCBeforePost
    OnCalcFields = qryCentroCustoColunaGrupoTotalCCCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCentroCustoColunaGrupoTotalCC'
      '      ,nCdColunaGrupoTotalCC'
      '      ,nCdCC'
      '  FROM CentroCustoColunaGrupoTotalCC'
      ' WHERE nCdColunaGrupoTotalCC = :nPK')
    Left = 456
    Top = 224
    object qryCentroCustoColunaGrupoTotalCCnCdCentroCustoColunaGrupoTotalCC: TIntegerField
      FieldName = 'nCdCentroCustoColunaGrupoTotalCC'
    end
    object qryCentroCustoColunaGrupoTotalCCnCdColunaGrupoTotalCC: TIntegerField
      FieldName = 'nCdColunaGrupoTotalCC'
    end
    object qryCentroCustoColunaGrupoTotalCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCentroCustoColunaGrupoTotalCCcNmCC: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmCC'
      Size = 50
      Calculated = True
    end
  end
  object DataSource1: TDataSource
    DataSet = qryColunaGrupoTotalCC
    Left = 512
    Top = 64
  end
  object dsCentroCustoColunaGrupoTotalCC: TDataSource
    DataSet = qryCentroCustoColunaGrupoTotalCC
    Left = 472
    Top = 256
  end
  object qryVerificaCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdColunaGrupoTotalCC'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdGrupoTotalCC'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCC'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT GrupoTotalCC.cNmGrupoTotalCC'
      '      ,CentroCusto.cNmCC'
      '      ,ColunaGrupoTotalCC.cNmColunaGrupoTotalCC'
      '  FROM CentroCustoColunaGrupoTotalCC CCGrupoTotal'
      
        '       INNER JOIN ColunaGrupoTotalCC ON ColunaGrupoTotalCC.nCdCo' +
        'lunaGrupoTotalCC = CCGrupoTotal.nCdColunaGrupoTotalCC'
      
        '       INNER JOIN GrupoTotalCC       ON GrupoTotalCC.nCdGrupoTot' +
        'alCC             = ColunaGrupoTotalCC.nCdGrupoTotalCC'
      
        '       INNER JOIN CentroCusto        ON CentroCusto.nCdCC       ' +
        '                 = CCGrupoTotal.nCdCC'
      
        ' WHERE CCGrupoTotal.nCdColunaGrupoTotalCC != :nCdColunaGrupoTota' +
        'lCC'
      '   AND GrupoTotalCC.nCdGrupoTotalCC        = :nCdGrupoTotalCC'
      '   AND CCGrupoTotal.nCdCC                  = :nCdCC')
    Left = 252
    Top = 252
    object qryVerificaCCcNmGrupoTotalCC: TStringField
      FieldName = 'cNmGrupoTotalCC'
      Size = 50
    end
    object qryVerificaCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
    object qryVerificaCCcNmColunaGrupoTotalCC: TStringField
      FieldName = 'cNmColunaGrupoTotalCC'
      Size = 50
    end
  end
  object qryExcluirDpCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdColunaGrupoTotalCC'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DELETE'
      '  FROM CentroCustoColunaGrupoTotalCC'
      ' WHERE nCdColunaGrupoTotalCC = :nCdColunaGrupoTotalCC'
      '')
    Left = 300
    Top = 252
  end
end
