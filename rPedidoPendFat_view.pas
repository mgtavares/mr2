unit rPedidoPendFat_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPedidoPendFat_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText4: TQRDBText;
    QRBand2: TQRBand;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    lblFiltro3: TQRLabel;
    lblFiltro2: TQRLabel;
    usp_Relatorio: TADOStoredProc;
    QRDBText1: TQRDBText;
    lblFiltro4: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape3: TQRShape;
    QRExpr1: TQRExpr;
    QRDBText5: TQRDBText;
    QRLabel4: TQRLabel;
    usp_RelatorionCdPedido: TIntegerField;
    usp_RelatoriodDtPedido: TDateTimeField;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatoriocNmTabStatusPed: TStringField;
    usp_RelatorionValPedido: TBCDField;
    usp_RelatorionValFaturado: TBCDField;
    usp_RelatorionSaldoFat: TBCDField;
    usp_RelatoriocFlgBonif: TStringField;
    QRDBText6: TQRDBText;
    QRLabel5: TQRLabel;
    usp_RelatorionValAdiantamento: TBCDField;
    usp_RelatorionSaldoAdiant: TBCDField;
    QRDBText3: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRShape2: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel8: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPedidoPendFat_view: TrptPedidoPendFat_view;

implementation

{$R *.dfm}

end.
