unit rBorderoCaixaNovo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, RDprint;

type
  TrptBorderoCaixaNovo = class(TForm)
    qryResumoCaixa: TADOQuery;
    qryResumoFormaPagto: TADOQuery;
    qryLanctoManual: TADOQuery;
    SPREL_BORDEROINDIV_CABECALHO: TADOStoredProc;
    SPREL_BORDEROINDIV_CABECALHOcNmCaixa: TStringField;
    SPREL_BORDEROINDIV_CABECALHOcNmUsuario: TStringField;
    SPREL_BORDEROINDIV_CABECALHOcIntervalo: TStringField;
    SPREL_BORDEROINDIV_CABECALHOnSaldoAbertura: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnValMovimentacao: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnSaldoFechamento: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnValSangria: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnValSuprimento: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnValSaidaDeposito: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnValOutrosCreditos: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnValOutrosDebitos: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnValEstornoVenda: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOiQtdeEstornoVenda: TIntegerField;
    SPREL_BORDEROINDIV_CABECALHOnValEstornoRecParc: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOiQtdeEstornoRecParc: TIntegerField;
    SPREL_BORDEROINDIV_CABECALHOnValEstornoOutros: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOiQtdeEstornoOutros: TIntegerField;
    SPREL_BORDEROINDIV_CABECALHOnValValeMercEmitido: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnValValeMercRecebido: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnValValeMercRecomprado: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnValValePresenteEmitido: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnValValePresenteRecebido: TBCDField;
    SPREL_BORDEROINDIV_RESUMOFORMA: TADOStoredProc;
    SPREL_BORDEROINDIV_LANCTOMANUAL: TADOStoredProc;
    SPREL_BORDEROINDIV_RESUMOFORMAnCdTabTipoFormaPagto: TIntegerField;
    SPREL_BORDEROINDIV_RESUMOFORMAcNmTabTipoFormaPagto: TStringField;
    SPREL_BORDEROINDIV_RESUMOFORMAnValCalculado: TBCDField;
    SPREL_BORDEROINDIV_RESUMOFORMAnValInformado: TBCDField;
    SPREL_BORDEROINDIV_RESUMOFORMAnValDiferenca: TBCDField;
    SPREL_BORDEROINDIV_LANCTOMANUALnCdLanctoFin: TStringField;
    SPREL_BORDEROINDIV_LANCTOMANUALdDtLancto: TStringField;
    SPREL_BORDEROINDIV_LANCTOMANUALnValLancto: TBCDField;
    SPREL_BORDEROINDIV_LANCTOMANUALcNmTipoLancto: TStringField;
    RDprint1: TRDprint;
    SPREL_BORDEROINDIV_CABECALHOnValValeDescontoEmitido: TBCDField;
    SPREL_BORDEROINDIV_CABECALHOnValValeDescontoRecebido: TBCDField;
    procedure RDprint1NewPage(Sender: TObject; Pagina: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ExibeBordero();
    procedure BorderoIndividual(nCdResumoCaixa:integer);
  end;

var
  rptBorderoCaixaNovo: TrptBorderoCaixaNovo;
  iLinha : integer ;
  iPagina: integer ;

implementation

uses fMenu;

{$R *.dfm}

{ TrptBorderoCaixaNovo }

procedure TrptBorderoCaixaNovo.BorderoIndividual(nCdResumoCaixa: integer);
begin

    iPagina := 0 ;
    
    SPREL_BORDEROINDIV_CABECALHO.Close ;
    SPREL_BORDEROINDIV_CABECALHO.Parameters.ParamByName('@nCdResumoCaixa').Value := nCdResumoCaixa ;
    SPREL_BORDEROINDIV_CABECALHO.Open ;

    if (SPREL_BORDEROINDIV_CABECALHO.eof) then
    begin
        ShowMessage('O resumo de caixa ' + intToStr(nCdResumoCaixa) + ' n�o existe.') ;
        exit ;
    end ;

    SPREL_BORDEROINDIV_RESUMOFORMA.Close ;
    SPREL_BORDEROINDIV_RESUMOFORMA.Parameters.ParamByName('@nCdResumoCaixa').Value := nCdResumoCaixa ;
    SPREL_BORDEROINDIV_RESUMOFORMA.Open ;

    SPREL_BORDEROINDIV_LANCTOMANUAL.Close ;
    SPREL_BORDEROINDIV_LANCTOMANUAL.Parameters.ParamByName('@nCdResumoCaixa').Value := nCdResumoCaixa ;
    SPREL_BORDEROINDIV_LANCTOMANUAL.Open ;

    ExibeBordero;

end;

procedure TrptBorderoCaixaNovo.ExibeBordero;
var
    nTotalCalculado, nTotalInformado, nTotalDiferenca : double ;
begin
  rdprint1.Abrir;

  rdprint1.OpcoesPreview.PaginaZebrada := false ;
  rdprint1.OpcoesPreview.Remalina      := false ;

  iPagina := 1 ;
  {rdprint1.FonteTamanhoPadrao := s05cpp;}

  rdPrint1.ImpF(1,1,frmMenu.cNmFantasiaEmpresa,[negrito]);
  rdPrint1.ImpD(1,80,'Data Emiss�o: ' + DateTimeToStr(Now()),[comp17]);
  rdPrint1.ImpF(2,1,'BORDER� DE CAIXA INDIVIDUAL',[negrito])  ;
  rdPrint1.ImpD(2,80,'P�gina: ' + frmMenu.ZeroEsquerda(intToStr(iPagina),3),[comp17]);
  rdPrint1.impBox(03,01,'--------------------------------------------------------------------------------');

  rdPrint1.ImpF(04,01,'Loja/Caixa: '    + SPREL_BORDEROINDIV_CABECALHOcNmCaixa.Value   ,[comp17]);
  rdPrint1.ImpF(04,45,'Operador     : ' + SPREL_BORDEROINDIV_CABECALHOcNmUsuario.Value ,[comp17]);
  rdPrint1.ImpF(05,01,'Per�odo   : '     + SPREL_BORDEROINDIV_CABECALHOcIntervalo.Value , [comp17]) ;

  rdPrint1.ImpF(05,45,'Saldo Inicial:',[comp17]) ;
  rdprint1.ImpVal(05,75,'###,##0.00',SPREL_BORDEROINDIV_CABECALHOnSaldoAbertura.Value,[negrito,comp17]);
  rdPrint1.ImpF(06,45,'Movimenta��o :',[comp17]) ;
  rdprint1.ImpVal(06,75,'###,##0.00',SPREL_BORDEROINDIV_CABECALHOnValMovimentacao.Value,[negrito,comp17]);
  rdPrint1.ImpF(07,45,'Saldo Final  :',[comp17]) ;
  rdprint1.ImpVal(07,75,'###,##0.00',SPREL_BORDEROINDIV_CABECALHOnSaldoFechamento.Value,[negrito,comp17]);

  rdPrint1.ImpF  (09,01,'RESUMO POR FORMA DE PAGAMENTO',[negrito,comp17,sublinhado]) ;

  rdPrint1.ImpD  (11,01,'Forma Pagamento',[negrito,comp17]) ;
  rdPrint1.ImpD  (11,35,'Valor Calculado',[negrito,comp17]) ;
  rdPrint1.ImpD  (11,55,'Valor Informado',[negrito,comp17]) ;
  rdPrint1.ImpD  (11,75,'Valor Diferen�a',[negrito,comp17]) ;

  iLinha := 12 ;

  nTotalCalculado := 0 ;
  nTotalInformado := 0 ;
  nTotalDiferenca := 0 ;

  SPREL_BORDEROINDIV_RESUMOFORMA.First;

  while not SPREL_BORDEROINDIV_RESUMOFORMA.Eof do
  begin
      rdPrint1.ImpF(iLinha,01,SPREL_BORDEROINDIV_RESUMOFORMAcNmTabTipoFormaPagto.Value,[comp17]) ;
      RDprint1.IMPVal(iLinha,35,'###,##0.00',SPREL_BORDEROINDIV_RESUMOFORMAnValCalculado.Value,[comp17]);
      RDprint1.IMPVal(iLinha,55,'###,##0.00',SPREL_BORDEROINDIV_RESUMOFORMAnValInformado.Value,[comp17]);
      RDprint1.IMPVal(iLinha,75,'###,##0.00',SPREL_BORDEROINDIV_RESUMOFORMAnValDiferenca.Value,[comp17]);

      nTotalCalculado := nTotalCalculado + SPREL_BORDEROINDIV_RESUMOFORMAnValCalculado.Value;
      nTotalInformado := nTotalInformado + SPREL_BORDEROINDIV_RESUMOFORMAnValInformado.Value;
      nTotalDiferenca := nTotalDiferenca + SPREL_BORDEROINDIV_RESUMOFORMAnValDiferenca.Value;
      iLinha          := iLinha + 1 ;
      SPREL_BORDEROINDIV_RESUMOFORMA.Next ;
  end ;

  rdPrint1.ImpF(19,01,'Total',[negrito,comp17]);
  RDprint1.IMPVal(19,35,'###,##0.00',nTotalCalculado,[negrito,comp17]);
  RDprint1.IMPVal(19,55,'###,##0.00',nTotalInformado,[negrito,comp17]);
  RDprint1.IMPVal(19,75,'###,##0.00',nTotalDiferenca,[negrito,comp17]);

  rdPrint1.ImpF(20,01,'RESUMO DE MOVIMENTO ESTORNADO',[negrito,comp17,sublinhado]);

  rdPrint1.ImpF(21,01,'Vendas: ' + frmMenu.ZeroEsquerda(SPREL_BORDEROINDIV_CABECALHOiQtdeEstornoVenda.asString,3) + '/',[comp17]) ;
  RDprint1.IMPVal(21,15,'###,##0.00',SPREL_BORDEROINDIV_CABECALHOnValEstornoVenda.Value,[negrito,comp17]);

  rdPrint1.ImpF(21,27,'Rec.Parc: ' + frmMenu.ZeroEsquerda(SPREL_BORDEROINDIV_CABECALHOiQtdeEstornoRecParc.asString,3) + '/',[comp17]) ;
  RDprint1.IMPVal(21,43,'###,##0.00',SPREL_BORDEROINDIV_CABECALHOnValEstornoRecParc.Value,[negrito,comp17]);

  rdPrint1.ImpF(21,53,'Outros: ' + frmMenu.ZeroEsquerda(SPREL_BORDEROINDIV_CABECALHOiQtdeEstornoOutros.asString,3) + '/',[comp17]) ;
  RDprint1.IMPVal(21,67,'###,##0.00',SPREL_BORDEROINDIV_CABECALHOnValEstornoOutros.Value,[negrito,comp17]);

  rdPrint1.ImpF(23,01,'RESUMO DE MOVIMENTA��O DE VALES',[negrito,comp17,sublinhado]) ;
  rdPrint1.ImpF(24,01,'Vale Mercadoria - Emitido   :  ',[comp17]) ;
  rdPrint1.ImpF(25,01,'                  Recebido  :  ',[comp17]) ;
  rdPrint1.ImpF(26,01,'                  Recomprado:  ',[comp17]) ;

  rdPrint1.ImpF(24,30,'Vale Presente - Emitido  :  ',[comp17]) ;
  rdPrint1.ImpF(25,30,'                Recebido :  ',[comp17]) ;

  rdPrint1.ImpF(24,58,'Vale Desconto - Emitido  :  ',[comp17]) ;
  rdPrint1.ImpF(25,58,'                Recebido :  ',[comp17]) ;

  RDprint1.IMPVal(24,20,'#,##0.00',SPREL_BORDEROINDIV_CABECALHOnValValeMercEmitido.Value,[negrito,comp17]);
  RDprint1.IMPVal(25,20,'#,##0.00',SPREL_BORDEROINDIV_CABECALHOnValValeMercRecebido.Value,[negrito,comp17]);
  RDprint1.IMPVal(26,20,'#,##0.00',SPREL_BORDEROINDIV_CABECALHOnValValeMercRecomprado.Value,[negrito,comp17]);

  RDprint1.IMPVal(24,48,'#,##0.00',SPREL_BORDEROINDIV_CABECALHOnValValePresenteEmitido.Value,[negrito,comp17]);
  RDprint1.IMPVal(25,48,'#,##0.00',SPREL_BORDEROINDIV_CABECALHOnValValePresenteRecebido.Value,[negrito,comp17]);

  RDprint1.IMPVal(24,76,'#,##0.00',SPREL_BORDEROINDIV_CABECALHOnValValeDescontoEmitido.Value,[negrito,comp17]);
  RDprint1.IMPVal(25,76,'#,##0.00',SPREL_BORDEROINDIV_CABECALHOnValValeDescontoRecebido.Value,[negrito,comp17]);

  rdPrint1.ImpF(28,01,'RESUMO DE MOVIMENTA��O MANUAL',[negrito,comp17,sublinhado]) ;

  rdPrint1.ImpF(29,01,'Sangria : ',[comp17]) ;
  RDprint1.IMPVal(29,15,'###,##0.00',SPREL_BORDEROINDIV_CABECALHOnValSangria.Value,[negrito,comp17]);

  rdPrint1.ImpF(30,01,'Dep�sito: ',[comp17]) ;
  RDprint1.IMPVal(30,15,'###,##0.00',SPREL_BORDEROINDIV_CABECALHOnValSaidaDeposito.Value,[negrito,comp17]);

  rdPrint1.ImpF(29,27,'Suprimento: ',[comp17]) ;
  RDprint1.IMPVal(29,43,'###,##0.00',SPREL_BORDEROINDIV_CABECALHOnValSuprimento.Value,[negrito,comp17]);

  rdPrint1.ImpF(29,53,'Outros Cr�ditos:',[comp17]) ;
  RDprint1.IMPVal(29,70,'###,##0.00',SPREL_BORDEROINDIV_CABECALHOnValOutrosCreditos.Value,[negrito,comp17]);
  rdPrint1.ImpF(30,53,'Outros D�bitos :',[comp17]) ;
  RDprint1.IMPVal(30,70,'###,##0.00',SPREL_BORDEROINDIV_CABECALHOnValOutrosDebitos.Value,[negrito,comp17]);

  rdPrint1.ImpF  (32,01,'MOVIMENTA��O MANUAL DETALHADA',[negrito,comp17,sublinhado]) ;
  rdPrint1.ImpF  (33,01,'C�digo',[negrito,comp17]);
  rdPrint1.ImpF  (33,08,'Tipo Lan�amento',[negrito,comp17]);
  rdPrint1.ImpD  (33,60,'Valor',[negrito,comp17]);
  rdPrint1.ImpF  (33,70,'Data',[negrito,comp17]);

  iLinha := 34 ;

  SPREL_BORDEROINDIV_LANCTOMANUAL.First;

  while not SPREL_BORDEROINDIV_LANCTOMANUAL.Eof do
  begin
      rdPrint1.ImpF(iLinha,01,SPREL_BORDEROINDIV_LANCTOMANUALnCdLanctoFin.Value,[comp17]) ;
      rdPrint1.ImpF(iLinha,08,SPREL_BORDEROINDIV_LANCTOMANUALcNmTipoLancto.Value,[comp17]) ;
      RDprint1.IMPVal(iLinha,60,'#,##0.00',SPREL_BORDEROINDIV_LANCTOMANUALnValLancto.Value,[comp17]);
      rdPrint1.ImpF(iLinha,70,SPREL_BORDEROINDIV_LANCTOMANUALdDtLancto.Value,[comp17]) ;
      iLinha          := iLinha + 1 ;
      SPREL_BORDEROINDIV_LANCTOMANUAL.Next ;

      if (iLinha > 66) then
      begin
          rdPrint1.Novapagina;

      end ;
  end ;

  rdprint1.OpcoesPreview.Preview := true ;
  {rdprint1.OpcoesPreview.Remalina:= true ;
  rdprint1.OpcoesPreview.PaginaZebrada:= true ;}

  rdprint1.Fechar;

end;

procedure TrptBorderoCaixaNovo.RDprint1NewPage(Sender: TObject;
  Pagina: Integer);
begin
  iPagina := iPagina+ 1 ;

  rdPrint1.ImpF(1,1,frmMenu.cNmFantasiaEmpresa,[negrito]);
  rdPrint1.ImpD(1,80,'Data Emiss�o: ' + DateTimeToStr(Now()),[comp17]);
  rdPrint1.ImpF(2,1,'BORDER� DE CAIXA INDIVIDUAL',[negrito])  ;
  rdPrint1.ImpD(2,80,'P�gina: ' + frmMenu.ZeroEsquerda(intToStr(iPagina),3),[comp17]);
  rdPrint1.impBox(03,01,'--------------------------------------------------------------------------------');

  if (iPagina > 1) then
  begin
      rdPrint1.ImpF  (05,01,' MOVIMENTA��O MANUAL DETALHADA',[negrito,comp17]) ;
      rdPrint1.ImpF  (06,01,'C�digo',[negrito,comp17]);
      rdPrint1.ImpF  (06,08,'Tipo Lan�amento',[negrito,comp17]);
      rdPrint1.ImpD  (06,60,'Valor',[negrito,comp17]);
      rdPrint1.ImpF  (06,70,'Data',[negrito,comp17]);

      iLinha := 7 ;
  end ;

end;

end.
