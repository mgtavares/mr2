unit fDoctoFiscalManual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxMaskEdit,
  cxDropDownEdit, cxDBEdit, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh,
  cxPC, cxLookAndFeelPainters, cxButtons, DBCtrlsEh;

type
  TfrmDoctoFiscalManual = class(TfrmCadastro_Padrao)
    qryMasternCdDoctoFiscal: TAutoIncField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdSerieFiscal: TIntegerField;
    qryMastercFlgEntSai: TStringField;
    qryMastercModelo: TStringField;
    qryMastercSerie: TStringField;
    qryMastercCFOP: TStringField;
    qryMastercTextoCFOP: TStringField;
    qryMasteriNrDocto: TIntegerField;
    qryMasternCdTipoDoctoFiscal: TIntegerField;
    qryMasterdDtEmissao: TDateTimeField;
    qryMasterdDtSaida: TDateTimeField;
    qryMasterdDtImpressao: TDateTimeField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNmTerceiro: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMastercIE: TStringField;
    qryMastercTelefone: TStringField;
    qryMasternCdEndereco: TIntegerField;
    qryMastercEndereco: TStringField;
    qryMasteriNumero: TIntegerField;
    qryMastercBairro: TStringField;
    qryMastercCidade: TStringField;
    qryMastercCep: TStringField;
    qryMastercUF: TStringField;
    qryMasternValTotal: TBCDField;
    qryMasternValProduto: TBCDField;
    qryMasternValBaseICMS: TBCDField;
    qryMasternValICMS: TBCDField;
    qryMasternValBaseICMSSub: TBCDField;
    qryMasternValICMSSub: TBCDField;
    qryMasternValIsenta: TBCDField;
    qryMasternValOutras: TBCDField;
    qryMasternValDespesa: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMasterdDtCad: TDateTimeField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMastercFlgFaturado: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMastercNmTransp: TStringField;
    qryMastercCNPJTransp: TStringField;
    qryMastercIETransp: TStringField;
    qryMastercEnderecoTransp: TStringField;
    qryMastercBairroTransp: TStringField;
    qryMastercCidadeTransp: TStringField;
    qryMastercUFTransp: TStringField;
    qryMasteriQtdeVolume: TIntegerField;
    qryMasternPesoBruto: TBCDField;
    qryMasternPesoLiq: TBCDField;
    qryMastercPlaca: TStringField;
    qryMastercUFPlaca: TStringField;
    qryMasternValIPI: TBCDField;
    qryMasternValFatura: TBCDField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternCdUsuarioImpr: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternCdUsuarioCancel: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    DBEdit6: TDBEdit;
    DataSource1: TDataSource;
    qryTipoDoctoFiscal: TADOQuery;
    qryTipoDoctoFiscalnCdTipoDoctoFiscal: TIntegerField;
    qryTipoDoctoFiscalcNmTipoDoctoFiscal: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit7: TDBEdit;
    DataSource2: TDataSource;
    DBEdit8: TDBEdit;
    qryEndereco: TADOQuery;
    qryEnderecocEndereco: TStringField;
    cxPageControl1: TcxPageControl;
    tabItens: TcxTabSheet;
    tabTotal: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryItens: TADOQuery;
    qryItensnCdItemDoctoFiscal: TAutoIncField;
    qryItensnCdProduto: TIntegerField;
    qryItenscNmItem: TStringField;
    qryItenscUnidadeMedida: TStringField;
    qryItensnQtde: TBCDField;
    qryItensnValUnitario: TBCDField;
    qryItensnValTotal: TBCDField;
    qryItensnCdGrupoImposto: TIntegerField;
    qryItenscNCM: TStringField;
    dsItens: TDataSource;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    Image2: TImage;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label7: TLabel;
    DBEdit9: TDBEdit;
    Label8: TLabel;
    DBEdit10: TDBEdit;
    Label9: TLabel;
    DBEdit11: TDBEdit;
    Label10: TLabel;
    DBEdit12: TDBEdit;
    Label11: TLabel;
    DBEdit13: TDBEdit;
    Label12: TLabel;
    DBEdit14: TDBEdit;
    Label13: TLabel;
    DBEdit15: TDBEdit;
    Label14: TLabel;
    DBEdit16: TDBEdit;
    Label15: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DataSource3: TDataSource;
    qryIncoterms: TADOQuery;
    qryIncotermsnCdIncoterms: TIntegerField;
    qryIncotermscSigla: TStringField;
    qryIncotermscNmIncoterms: TStringField;
    qryIncotermscFlgPagador: TStringField;
    Label16: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DataSource4: TDataSource;
    dsEndereco: TDataSource;
    DBComboBoxEh1: TDBComboBoxEh;
    qryAux: TADOQuery;
    qryEndereconCdEndereco: TAutoIncField;
    Label17: TLabel;
    DBEdit21: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit19Exit(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure DBEdit19KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDoctoFiscalManual: TfrmDoctoFiscalManual;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmDoctoFiscalManual.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'DOCTOFISCAL' ;
  nCdTabelaSistema  := 42 ;
  nCdConsultaPadrao := 119 ;

end;

procedure TfrmDoctoFiscalManual.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT nCdTipoDoctoFiscal FROM TipoDoctoFiscal') ;
  qryAux.Open ;

  if qryAux.eof then
  begin
      MensagemAlerta('Nenhum tipo de documento fiscal configurado.') ;
      exit ;
  end ;

  if (qryAux.RecordCount = 1) then
  begin
      qryMasternCdTipoDoctoFiscal.Value := qryAux.FieldList[0].Value ;
      PosicionaQuery(qryTipoDoctoFiscal, qryMasternCdTipoDoctoFiscal.AsString);

      DbEdit3.SetFocus ;
  end ;

  qryAux.Close ;


  

end;

procedure TfrmDoctoFiscalManual.DBEdit3Exit(Sender: TObject);
begin
  inherited;
  qryTipoPedido.Close ;
  PosicionaQuery(qryTipoPedido, qryMasternCdTipoPedido.AsString) ;
  
end;

procedure TfrmDoctoFiscalManual.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  if (qryTerceiro.eof) then
      exit ;
      
  PosicionaQuery(qryEndereco, qryMasternCdTerceiro.asString) ;

  if (qryEndereco.Eof) then
  begin
      MensagemAlerta('Nenhum endere�o ativo para este terceiro.') ;
      exit ;
  end ;

  DBComboBoxEh1.Enabled := True ;

  if (qryEndereco.RecordCount = 1) then
  begin
      qryMasternCdEndereco.Value := qryEndereconCdEndereco.Value ;
      DbComboBoxEh1.Enabled := False ;
      Dbedit19.SetFocus ;
  end ;

  
  

end;

procedure TfrmDoctoFiscalManual.DBEdit19Exit(Sender: TObject);
begin
  inherited;
  qryIncoterms.Close;
  PosicionaQuery(qryIncoterms, qryMasternCdIncoterms.AsString) ;
  
end;

procedure TfrmDoctoFiscalManual.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(77);

            If (nPK > 0) then
            begin
                qryMasternCdTipoDoctoFiscal.Value := nPK ;
                PosicionaQuery(qryTipoDoctoFiscal, qryMasternCdTipoDoctoFiscal.asString) ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDoctoFiscalManual.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(118);

            If (nPK > 0) then
            begin
                qryMasternCdTipoPedido.Value := nPK ;
                PosicionaQuery(qryTipoPedido, qryMasternCdTipoPedido.asString) ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDoctoFiscalManual.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(80);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
                PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDoctoFiscalManual.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  
  qryItens.Close ;
  PosicionaQuery(qryItens, qryMasternCdDoctoFiscal.AsString) ;

end;

procedure TfrmDoctoFiscalManual.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryItens.Close ;
  qryTipoDoctoFiscal.Close ;
  qryTipoPedido.Close ;
  qryTerceiro.Close ;
  qryEndereco.Close ;
  qryIncoterms.Close ;
  
end;

procedure TfrmDoctoFiscalManual.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasternCdTipoDoctoFiscal.Value = 0) or (dbEdit18.Text = '') then
  begin
      MensagemAlerta('Informe o tipo do documento fiscal.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTipoPedido.Value = 0) or (dbEdit6.Text = '') then
  begin
      MensagemAlerta('Informe o tipo do pedido.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTerceiro.Value = 0) or (dbEdit8.Text = '') then
  begin
      MensagemAlerta('Informe o Terceiro.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (qryEndereconCdEndereco.Value = 0) or (DBComboBoxEh1.Text = '') then
  begin
      MensagemAlerta('Selecione o endere�o de destino.') ;
      abort ;
  end ;

  if (qryMasternCdIncoterms.Value = 0) or (dbEdit20.Text = '') then
  begin
      MensagemAlerta('Informe o incoterms.') ;
      DbEdit19.SetFocus ;
      abort ;
  end ;

  {inherited;}

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmDoctoFiscalManual.btSalvarClick(Sender: TObject);
begin
  inherited;

  frmMenu.Connection.BeginTrans ;

  try
      qryMaster.UpdateBatch;
      qryItens.UpdateBatch;
  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

end;

procedure TfrmDoctoFiscalManual.DBEdit19KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
                PosicionaQuery(qryIncoterms, qryMasternCdIncoterms.asString) ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDoctoFiscalManual.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;
      
end;

initialization
    RegisterClass(TfrmDoctoFiscalManual) ;
    
end.
