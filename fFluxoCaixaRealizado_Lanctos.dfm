inherited frmFluxoCaixaRealizado_Lanctos: TfrmFluxoCaixaRealizado_Lanctos
  Left = 178
  Top = 208
  Caption = 'An'#225'lise de Lan'#231'amentos'
  OldCreateOrder = True
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    LookAndFeel.NativeStyle = True
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsLanctos
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'nValLancto'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          Column = cxGrid1DBTableView1nValor
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValLancto'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValor
        end>
      DataController.Summary.SummaryGroups = <
        item
          Links = <
            item
            end>
          SummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'nValLancto'
            end>
        end>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLineColor = clAqua
      OptionsView.GroupFooters = gfAlwaysVisible
      Styles.Content = frmMenu.ConteudoCaixa
      object cxGrid1DBTableView1cNmColunaGrupoTotalCC: TcxGridDBColumn
        DataBinding.FieldName = 'cNmColunaGrupoTotalCC'
        Visible = False
        GroupIndex = 0
        Width = 178
      end
      object cxGrid1DBTableView1dDtLancto: TcxGridDBColumn
        Caption = 'Dt. Lancto'
        DataBinding.FieldName = 'dDtLancto'
        Width = 90
      end
      object cxGrid1DBTableView1nCdLojaPG: TcxGridDBColumn
        Caption = 'Loja PG'
        DataBinding.FieldName = 'nCdLojaPG'
        Width = 67
      end
      object cxGrid1DBTableView1nCdLojaTit: TcxGridDBColumn
        Caption = 'Loja Tit.'
        DataBinding.FieldName = 'nCdLojaTit'
        Width = 76
      end
      object cxGrid1DBTableView1nCdLanctoFin: TcxGridDBColumn
        Caption = 'Lancto Fin.'
        DataBinding.FieldName = 'nCdLanctoFin'
        Width = 90
      end
      object cxGrid1DBTableView1cNrDocto: TcxGridDBColumn
        Caption = 'N'#250'm. Docto'
        DataBinding.FieldName = 'cNrDocto'
      end
      object cxGrid1DBTableView1cHistorico: TcxGridDBColumn
        Caption = 'Hist'#243'rico'
        DataBinding.FieldName = 'cHistorico'
        Width = 119
      end
      object cxGrid1DBTableView1nValor: TcxGridDBColumn
        Caption = 'Valor'
        DataBinding.FieldName = 'nValor'
      end
      object cxGrid1DBTableView1iNrCheque: TcxGridDBColumn
        Caption = 'Cheque'
        DataBinding.FieldName = 'iNrCheque'
      end
      object cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn
        Caption = 'Usu'#225'rio'
        DataBinding.FieldName = 'cNmUsuario'
        Width = 122
      end
      object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
        Caption = 'ID T'#237'tulo'
        DataBinding.FieldName = 'nCdTitulo'
        Width = 79
      end
      object cxGrid1DBTableView1nCdSP: TcxGridDBColumn
        Caption = 'N'#250'm. SP'
        DataBinding.FieldName = 'nCdSP'
      end
      object cxGrid1DBTableView1nCdProvisaoTit: TcxGridDBColumn
        Caption = 'Provis'#227'o'
        DataBinding.FieldName = 'nCdProvisaoTit'
      end
      object cxGrid1DBTableView1cNrNF: TcxGridDBColumn
        Caption = 'N'#250'm. NF'
        DataBinding.FieldName = 'cNrNF'
      end
      object cxGrid1DBTableView1cFlgManual: TcxGridDBColumn
        Caption = 'Manual ?'
        DataBinding.FieldName = 'cFlgManual'
        Width = 65
      end
      object cxGrid1DBTableView1nCdConta: TcxGridDBColumn
        Caption = 'Conta Movimento'
        DataBinding.FieldName = 'nCdConta'
        Width = 168
      end
      object cxGrid1DBTableView1cNmCC: TcxGridDBColumn
        Caption = 'Centro de Custo'
        DataBinding.FieldName = 'cNmCC'
        Width = 166
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 360
    Top = 112
  end
  object qryLanctos: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nCdPlanoConta'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempResumoCaixa'#39') IS NULL)'
      'BEGIN'
      ''
      #9'CREATE TABLE #TempResumoCaixa (nCdPlanoConta         int null'
      #9#9#9#9#9#9#9#9'                ,nCdLoja               int null'
      
        #9#9#9#9#9#9#9#9'                ,nValor                decimal(12,2) def' +
        'ault 0 not null'
      '                                ,nCdLanctoFin          int'
      '                                ,nCdLojaEmissora       int'
      '                                ,nCdTitulo             int'
      '                                ,nCdContaBancaria      int'
      '                                ,nCdCC1                int'
      '                                ,nCdCC2                int'
      '                                ,nCdCC3                int'
      
        '                                ,cHistorico            varchar(1' +
        '00)'
      '                                ,dDtLancto             datetime'
      
        '                                ,cNrDocto              varchar(2' +
        '0)'
      '                                ,nCdUsuario            int'
      
        '                                ,cNmColunaGrupoTotalCC varchar(5' +
        '0))'
      ''
      'END'
      ''
      'DECLARE @nCdPlanoConta int'
      ''
      'Set @nCdPlanoConta = :nCdPlanoConta'
      ''
      'SELECT cNmColunaGrupoTotalCC'
      '      ,Temp.dDtLancto'
      '      ,dbo.fn_ZeroEsquerda(Loja.nCdLoja,3)         as nCdLojaPG'
      '      ,dbo.fn_ZeroEsquerda(LojaEmissora.nCdLoja,3) as nCdLojaTit'
      '      ,Temp.nCdLanctoFin'
      #9'    ,Temp.cNrDocto'
      
        #9'    ,Temp.cHistorico + '#39'    '#39' + IsNull(Terceiro.cNmTerceiro,'#39#39')' +
        ' as cHistorico'
      #9'    ,Temp.nValor'
      #9'    ,Cheque.iNrCheque'
      #9'    ,Usuario.cNmUsuario'
      #9'    ,Titulo.nCdTitulo'
      #9'    ,Titulo.nCdSP'
      #9'    ,LanctoFin.nCdProvisaoTit'
      #9'    ,Titulo.cNrNF'
      #9'    ,Terceiro.cNmTerceiro'
      #9'    ,CASE WHEN cFlgManual = 0 THEN NULL'
      #9#9#9'      ELSE '#39'Sim'#39
      #9'     END cFlgManual'
      
        '      ,CASE WHEN (ContaBancaria.cFlgCaixa = 0 AND ContaBancaria.' +
        'cFlgCofre = 0) THEN (dbo.fn_ZeroEsquerda((Convert(VARCHAR(5),Con' +
        'taBancaria.nCdBanco)),3) + '#39' - '#39' + ContaBancaria.nCdConta + '#39' - ' +
        #39' + IsNull(SUBSTRING(ContaBancaria.cNmTitular,1,15),'#39' '#39'))'
      '            ELSE ContaBancaria.nCdConta'
      '       END nCdConta'
      '      ,CentroCusto.cCdCC + '#39' - '#39' + CentroCusto.cNmCC as cNmCC'
      '  FROM #TempResumoCaixa Temp'
      
        '       LEFT  JOIN LanctoFin                  ON LanctoFin.nCdLan' +
        'ctoFin         = Temp.nCdLanctoFin'
      
        #9'     LEFT  JOIN ContaBancaria              ON ContaBancaria.nCd' +
        'ContaBancaria = Temp.nCdContaBancaria'
      
        '       LEFT  JOIN Loja                       ON Loja.nCdLoja    ' +
        '               = ContaBancaria.nCdLoja'
      
        '       LEFT  JOIN Loja          LojaEmissora ON LojaEmissora.nCd' +
        'Loja           = Temp.nCdLojaEmissora'
      
        '       LEFT  JOIN TipoLancto                 ON TipoLancto.nCdTi' +
        'poLancto       = LanctoFin.nCdTipoLancto'
      
        '       LEFT  JOIN Usuario                    ON Usuario.nCdUsuar' +
        'io             = Temp.nCdUsuario'
      
        #9'     LEFT  JOIN Titulo                     ON Titulo.nCdTitulo ' +
        '              = Temp.nCdTitulo'
      
        #9'     LEFT  JOIN Terceiro                   ON Terceiro.nCdTerce' +
        'iro           = Titulo.nCdTerceiro'
      
        #9'     LEFT  JOIN Cheque                     ON Cheque.nCdCheque ' +
        '              = LanctoFin.nCdCheque'
      
        '       LEFT  JOIN CentroCusto                ON CentroCusto.nCdC' +
        'C              = Temp.nCdCC3'
      ' WHERE Temp.nCdPlanoConta = @nCdPlanoConta'
      ' ORDER BY 1')
    Left = 176
    Top = 112
    object qryLanctosdDtLancto: TDateTimeField
      FieldName = 'dDtLancto'
    end
    object qryLanctosnCdLojaPG: TStringField
      FieldName = 'nCdLojaPG'
      ReadOnly = True
      Size = 15
    end
    object qryLanctosnCdLojaTit: TStringField
      FieldName = 'nCdLojaTit'
      ReadOnly = True
      Size = 15
    end
    object qryLanctosnCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryLanctoscNrDocto: TStringField
      FieldName = 'cNrDocto'
    end
    object qryLanctoscHistorico: TStringField
      FieldName = 'cHistorico'
      ReadOnly = True
      Size = 154
    end
    object qryLanctosnValor: TBCDField
      FieldName = 'nValor'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLanctosiNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryLanctoscNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryLanctosnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryLanctosnCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryLanctosnCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
    object qryLanctoscNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryLanctoscNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryLanctoscFlgManual: TStringField
      FieldName = 'cFlgManual'
      ReadOnly = True
      Size = 3
    end
    object qryLanctosnCdConta: TStringField
      FieldName = 'nCdConta'
      ReadOnly = True
      Size = 51
    end
    object qryLanctoscNmCC: TStringField
      FieldName = 'cNmCC'
      ReadOnly = True
      Size = 61
    end
    object qryLanctoscNmColunaGrupoTotalCC: TStringField
      DisplayLabel = 'Totalizador'
      FieldName = 'cNmColunaGrupoTotalCC'
      Size = 50
    end
  end
  object dsLanctos: TDataSource
    DataSet = qryLanctos
    Left = 232
    Top = 112
  end
end
