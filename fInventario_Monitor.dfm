inherited frmInventario_Monitor: TfrmInventario_Monitor
  Left = 220
  Top = 56
  Width = 1065
  Height = 636
  Caption = 'Invent'#225'rio Loja - Monitor'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 426
    Width = 1049
    Height = 172
  end
  inherited ToolBar1: TToolBar
    Width = 1049
    inherited ToolButton1: TToolButton
      Caption = '&Consultar'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1049
    Height = 164
    Align = alTop
    Caption = ' Filtros '
    TabOrder = 1
    object Label2: TLabel
      Tag = 1
      Left = 75
      Top = 19
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
    end
    object Label4: TLabel
      Tag = 1
      Left = 95
      Top = 43
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
    end
    object Label5: TLabel
      Tag = 1
      Left = 17
      Top = 115
      Width = 98
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo Fechamento'
    end
    object Label1: TLabel
      Tag = 1
      Left = 203
      Top = 139
      Width = 17
      Height = 13
      Alignment = taRightJustify
      Caption = 'At'#233
    end
    object Label3: TLabel
      Tag = 1
      Left = 49
      Top = 67
      Width = 66
      Height = 13
      Alignment = taRightJustify
      Caption = 'Local Estoque'
    end
    object Label8: TLabel
      Tag = 1
      Left = 7
      Top = 139
      Width = 108
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo Ence. Contag.'
    end
    object Label9: TLabel
      Tag = 1
      Left = 203
      Top = 115
      Width = 17
      Height = 13
      Alignment = taRightJustify
      Caption = 'At'#233
    end
    object Label11: TLabel
      Tag = 1
      Left = 203
      Top = 91
      Width = 17
      Height = 13
      Alignment = taRightJustify
      Caption = 'At'#233
    end
    object Label12: TLabel
      Tag = 1
      Left = 33
      Top = 91
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo Abertura'
    end
    object ER2LookupMaskEdit1: TER2LookupMaskEdit
      Left = 124
      Top = 11
      Width = 53
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      CodigoLookup = 25
      QueryLookup = qryEmpresa
    end
    object ER2LookupMaskEdit2: TER2LookupMaskEdit
      Left = 124
      Top = 35
      Width = 53
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 1
      Text = '         '
      CodigoLookup = 86
      QueryLookup = qryLoja
    end
    object edtDtFinal: TMaskEdit
      Left = 229
      Top = 83
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 4
      Text = '  /  /    '
    end
    object edtDtInicial: TMaskEdit
      Left = 124
      Top = 83
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
    object ER2LookupMaskEdit3: TER2LookupMaskEdit
      Left = 124
      Top = 59
      Width = 53
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 2
      Text = '         '
      CodigoLookup = 87
      QueryLookup = qryLocalEstoque
    end
    object edtDtFechFim: TMaskEdit
      Left = 229
      Top = 107
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 6
      Text = '  /  /    '
    end
    object edtDtFechIni: TMaskEdit
      Left = 124
      Top = 107
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 5
      Text = '  /  /    '
    end
    object edtDtEncConIni: TMaskEdit
      Left = 124
      Top = 131
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 7
      Text = '  /  /    '
    end
    object edtDtEncConFim: TMaskEdit
      Left = 229
      Top = 131
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 8
      Text = '  /  /    '
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 179
      Top = 35
      Width = 529
      Height = 21
      DataField = 'cNmLoja'
      DataSource = dsLoja
      TabOrder = 9
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 179
      Top = 11
      Width = 529
      Height = 21
      DataField = 'cNmEmpresa'
      DataSource = dsEmpresa
      TabOrder = 10
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 179
      Top = 59
      Width = 529
      Height = 21
      DataField = 'cNmLocalEstoque'
      DataSource = dsLocalEstoque
      TabOrder = 11
    end
    object RadioGroup1: TRadioGroup
      Left = 317
      Top = 83
      Width = 391
      Height = 72
      Caption = ' Situa'#231#227'o '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Aberto'
        'Contagem Encerrada'
        'Finalizado'
        'Cancelado')
      TabOrder = 12
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 193
    Width = 1049
    Height = 233
    Align = alTop
    TabOrder = 2
    object cxGrid1DBTableView1: TcxGridDBTableView
      PopupMenu = PopupMenu1
      DataController.DataSource = dsInventario
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OnCellClick = cxGrid1DBTableView1CellClick
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object cxGrid1DBTableView1nCdLoja: TcxGridDBColumn
        Caption = 'Loja'
        DataBinding.FieldName = 'nCdLoja'
        Visible = False
        GroupIndex = 0
      end
      object cxGrid1DBTableView1cNmInventario: TcxGridDBColumn
        Caption = 'Inventario'
        DataBinding.FieldName = 'cNmInventario'
        Width = 134
      end
      object cxGrid1DBTableView1cNmLocalEstoque: TcxGridDBColumn
        Caption = 'Local Estoque'
        DataBinding.FieldName = 'cNmLocalEstoque'
        Width = 131
      end
      object cxGrid1DBTableView1dDtAbertura: TcxGridDBColumn
        Caption = 'Dt. Abertura'
        DataBinding.FieldName = 'dDtAbertura'
      end
      object cxGrid1DBTableView1cNmUsuarioAbertaura: TcxGridDBColumn
        Caption = 'Usuario Abertura'
        DataBinding.FieldName = 'cNmUsuarioAbertaura'
        Width = 143
      end
      object cxGrid1DBTableView1dDtFech: TcxGridDBColumn
        Caption = 'Dt. Fechamento'
        DataBinding.FieldName = 'dDtFech'
      end
      object cxGrid1DBTableView1cNmUsuarioFech: TcxGridDBColumn
        Caption = 'Usu'#225'rio Fechamento'
        DataBinding.FieldName = 'cNmUsuarioFech'
        Width = 154
      end
      object cxGrid1DBTableView1dDtCancel: TcxGridDBColumn
        Caption = 'Dt. Cancelamento'
        DataBinding.FieldName = 'dDtCancel'
      end
      object cxGrid1DBTableView1cNmUsuarioCancel: TcxGridDBColumn
        Caption = 'Usu'#225'rio Cancelamento'
        DataBinding.FieldName = 'cNmUsuarioCancel'
        Width = 163
      end
      object cxGrid1DBTableView1dDtContagemEncerrada: TcxGridDBColumn
        Caption = 'Dt. Enc Contagem'
        DataBinding.FieldName = 'dDtContagemEncerrada'
      end
      object cxGrid1DBTableView1cOBS: TcxGridDBColumn
        Caption = 'Observa'#231#227'o'
        DataBinding.FieldName = 'cOBS'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxGrid2: TcxGrid [4]
    Left = 0
    Top = 426
    Width = 1049
    Height = 172
    Align = alClient
    TabOrder = 3
    object cxGrid2DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsItemLoteContagem
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object cxGrid2DBTableView1cNrLote: TcxGridDBColumn
        DataBinding.FieldName = 'cNrLote'
        Visible = False
        GroupIndex = 0
      end
      object cxGrid2DBTableView1nCdProduto: TcxGridDBColumn
        Caption = 'C'#243'd. Prod.'
        DataBinding.FieldName = 'nCdProduto'
        Width = 62
      end
      object cxGrid2DBTableView1cNmProduto: TcxGridDBColumn
        DataBinding.FieldName = 'cNmProduto'
        Width = 472
      end
      object cxGrid2DBTableView1nQtdeContada: TcxGridDBColumn
        DataBinding.FieldName = 'nQtdeContada'
        Width = 123
      end
    end
    object cxGrid2Level1: TcxGridLevel
      GridView = cxGrid2DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 784
    Top = 79
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003E39340039343000332F
      2B002C29250027242100201D1B00E7E7E700333130000B0A0900070706000404
      0300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000046413B00857A7000C3B8
      AE007C7268007F756B0036322D00F2F2F1004C4A470095897D00BAAEA2007C72
      68007F756B000101010000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF00000084000000000000000000000000004D47410083786F00CCC3
      BA00786F65007B71670034302D00FEFEFE002C2A270095897D00C2B8AD00786F
      65007C7268000605050000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000554E480083786F00CCC3
      BA007970660071685F00585550000000000049464500857A7000C2B8AD00786F
      65007B7167000D0C0B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000817B76009F928600CCC3
      BA00C0B4AA00A6988B00807D79000000000074726F0090847900C2B8AD00C0B4
      AA00A89B8E004947470000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000FCFCFC0060595200423D38005851
      4A003D383300332F2B0039373400D3D3D3005F5E5C001A181600252220001917
      15000F0E0D0012121200FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF00000084000000000000000000FDFDFD009D918500B1A396007F75
      6B007C726800776D64006C635B002E2A2600564F480080766C007C726800776D
      640070675E0001010100FAFAFA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000FEFDFD00B8ACA100BAAEA2008277
      6D0082776D00AA917B00BAA79400B8A69000B09781009F8D7D00836D5B007163
      570095897D0023232200FCFCFC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000FDFCFC00DDDAD7009B8E82009D91
      8500867B7100564F4800504A440080766C006E665D00826C5800A6917D009484
      7400564F48008B8A8A00FEFEFE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000746B6200A497
      8A0095897D009F9286003E393400000000004C4640007E746A00857A70003E39
      340085817E00F5F5F500FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      00009B918700C3B8AE00655D5500000000007C726800A89B8E00A69B90000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000000000000000
      0000A79C9100BCB0A4009D91850000000000AEA093009D9185007B756E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFFF0000
      FE00C00380030000000080018003000000008001800300000000800181030000
      0000800181030000000080010001000000008001000100000000800100010000
      000080010001000000008001C101000000018001F11F000000038001F11F0000
      0077C003FFFF0000007FFFFFFFFF0000}
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE nCdEmpresa = :nPK')
    Left = 816
    Top = 48
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK')
    Left = 848
    Top = 48
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque'
      '      ,cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK')
    Left = 880
    Top = 48
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 848
    Top = 80
  end
  object dsLocalEstoque: TDataSource
    DataSet = qryLocalEstoque
    Left = 880
    Top = 80
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 816
    Top = 80
  end
  object qryInventario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLocalEstoque'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtAberturaIni'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtAberturaFim'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFechIni'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFechFim'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtContagemEncerradaIni'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtContagemEncerradaFim'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'cFlgStatus'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa              int'
      '       ,@nCdLoja                 int'
      '       ,@nCdLocalEstoque         int'
      '       ,@dDtAberturaIni          varchar(10)'
      '       ,@dDtAberturaFim          varchar(10)'
      '       ,@dDtFechIni              varchar(10)'
      '       ,@dDtFechFim              varchar(10)'
      '       ,@dDtContagemEncerradaIni varchar(10)'
      '       ,@dDtContagemEncerradaFim varchar(10)'
      '       ,@cFlgStatus              int'
      ''
      'SET @nCdEmpresa              = :nCdEmpresa'
      'SET @nCdLoja                 = :nCdLoja'
      'SET @nCdLocalEstoque         = :nCdLocalEstoque'
      'SET @dDtAberturaIni          = :dDtAberturaIni'
      'SET @dDtAberturaFim          = :dDtAberturaFim'
      'SET @dDtFechIni              = :dDtFechIni'
      'SET @dDtFechFim              = :dDtFechFim'
      'SET @dDtContagemEncerradaIni = :dDtContagemEncerradaIni'
      'SET @dDtContagemEncerradaFim = :dDtContagemEncerradaFim'
      'SET @cFlgStatus              = :cFlgStatus'
      ''
      '-- Todos              = 0'
      '-- Aberto             = 1'
      '-- Contagem Encerrado = 2'
      '-- Fechado            = 3'
      '-- Cancelado          = 4'
      ''
      'SELECT nCdInventario'
      
        '      ,CONVERT(VARCHAR,nCdInventario) + '#39' - '#39' + cNmInventario  c' +
        'NmInventario'
      
        '      ,CONVERT(VARCHAR,Inventario.nCdLocalEstoque) + '#39' - '#39' + Loc' +
        'alEstoque.cNmLocalEstoque cNmLocalEstoque'
      '      ,dDtAbertura'
      '      ,nCdUsuarioAbertura'
      '      ,(SELECT cNmUsuario'
      '          FROM Usuario'
      
        '         WHERE Usuario.nCdUsuario = nCdUsuarioAbertura) AS cNmUs' +
        'uarioAbertaura'
      '      ,dDtFech'
      '      ,nCdUsuarioFech'
      '      ,(SELECT cNmUsuario'
      '          FROM Usuario'
      
        '         WHERE Usuario.nCdUsuario = nCdUsuarioFech) AS cNmUsuari' +
        'oFech'
      '      ,dDtCancel'
      '      ,nCdUsuarioCancel'
      '     ,(SELECT cNmUsuario'
      '          FROM Usuario'
      
        '         WHERE Usuario.nCdUsuario = nCdUsuarioCancel) AS cNmUsua' +
        'rioCancel'
      '      ,dDtContagemEncerrada      '
      '      ,cOBS'
      '   ,Inventario.nCdLoja'
      '  FROM Inventario'
      
        '       INNER JOIN LocalEstoque ON LocalEstoque.nCdLocalEstoque =' +
        ' Inventario.nCdLocalEstoque'
      ' WHERE Inventario.nCdEmpresa        = @nCdEmpresa'
      
        '   AND ((Inventario.nCdLoja         = @nCdLoja)         OR (@nCd' +
        'Loja         = 0)) '
      
        '   AND ((Inventario.nCdLocalEstoque = @nCdLocalEstoque) OR (@nCd' +
        'LocalEstoque = 0))'
      
        '   AND ((dbo.fn_OnlyDate(dDtAbertura)            >= CONVERT(DATE' +
        'TIME,@dDtAberturaIni,103))             OR (@dDtAberturaIni      ' +
        '    = '#39'01/01/1900'#39'))'
      
        '   AND ((dbo.fn_OnlyDate(dDtAbertura)            <  CONVERT(DATE' +
        'TIME,@dDtAberturaFim,103)+ 1)          OR (@dDtAberturaFim      ' +
        '    = '#39'01/01/1900'#39'))'
      
        '   AND ((dbo.fn_OnlyDate(dDtFech)                >= CONVERT(DATE' +
        'TIME,@dDtFechIni,103))                 OR (@dDtFechIni          ' +
        '    = '#39'01/01/1900'#39'))'
      
        '   AND ((dbo.fn_OnlyDate(dDtFech)                <  CONVERT(DATE' +
        'TIME,@dDtFechFim,103)+ 1)              OR (@dDtFechFim          ' +
        '    = '#39'01/01/1900'#39'))'
      
        '   AND ((dbo.fn_OnlyDate(dDtContagemEncerrada)   >= CONVERT(DATE' +
        'TIME,@dDtContagemEncerradaIni,103))    OR (@dDtContagemEncerrada' +
        'Ini = '#39'01/01/1900'#39'))'
      
        '   AND ((dbo.fn_OnlyDate(dDtContagemEncerrada)   <  CONVERT(DATE' +
        'TIME,@dDtContagemEncerradaFim,103)+ 1) OR (@dDtContagemEncerrada' +
        'Fim = '#39'01/01/1900'#39'))'
      '   AND ((@cFlgStatus = 1 AND dDtAbertura          IS NOT NULL'
      '                         AND dDtFech              IS NULL'
      '                         AND dDtContagemEncerrada IS NULL'
      
        '                         AND dDtCancel            IS NULL) --Ape' +
        'nas aberto'
      '     OR (@cFlgStatus = 2 AND dDtContagemEncerrada IS NOT NULL'
      '                         AND dDtCancel            IS NULL'
      
        '                         AND dDtFech              IS NULL)-- Ape' +
        'nas contagem encerrada sem ser encerrado'
      '     OR (@cFlgStatus = 3 AND dDtFech              IS NOT NULL'
      
        '                         AND dDtCancel            IS NULL) -- Ap' +
        'enas fechados'
      
        '     OR (@cFlgStatus = 4 AND dDtCancel            IS NOT NULL) -' +
        '-Apenas cancelamento.'
      '     OR (@cFlgStatus = 0)) --Todos ')
    Left = 912
    Top = 48
    object qryInventariocNmInventario: TStringField
      FieldName = 'cNmInventario'
      ReadOnly = True
      Size = 83
    end
    object qryInventariocNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      ReadOnly = True
      Size = 83
    end
    object qryInventariodDtAbertura: TDateTimeField
      FieldName = 'dDtAbertura'
    end
    object qryInventarionCdUsuarioAbertura: TIntegerField
      FieldName = 'nCdUsuarioAbertura'
    end
    object qryInventariocNmUsuarioAbertaura: TStringField
      FieldName = 'cNmUsuarioAbertaura'
      ReadOnly = True
      Size = 50
    end
    object qryInventariodDtFech: TDateTimeField
      FieldName = 'dDtFech'
    end
    object qryInventarionCdUsuarioFech: TIntegerField
      FieldName = 'nCdUsuarioFech'
    end
    object qryInventariocNmUsuarioFech: TStringField
      FieldName = 'cNmUsuarioFech'
      ReadOnly = True
      Size = 50
    end
    object qryInventariodDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryInventarionCdUsuarioCancel: TIntegerField
      FieldName = 'nCdUsuarioCancel'
    end
    object qryInventariocNmUsuarioCancel: TStringField
      FieldName = 'cNmUsuarioCancel'
      ReadOnly = True
      Size = 50
    end
    object qryInventariodDtContagemEncerrada: TDateTimeField
      FieldName = 'dDtContagemEncerrada'
    end
    object qryInventariocOBS: TStringField
      FieldName = 'cOBS'
      Size = 50
    end
    object qryInventarionCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryInventarionCdInventario: TIntegerField
      FieldName = 'nCdInventario'
    end
  end
  object dsInventario: TDataSource
    DataSet = qryInventario
    Left = 912
    Top = 80
  end
  object qryItemLoteContagem: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT LC.nCdLoteContagemInventario'
      #9'  ,cNrLote'
      '      ,P.nCdProduto'
      '      ,cNmProduto'
      '      ,SUM(nQtdeContada) nQtdeContada'#9#9#9#9#9' '
      '  FROM ItemLoteContagemInventario ILC'
      
        '       INNER JOIN LoteContagemInventario LC ON LC.nCdLoteContage' +
        'mInventario = ILC.nCdLoteContagemInventario'
      '       INNER JOIN Produto P ON P.nCdProduto =  ILC.nCdProduto'
      ' WHERE nCdInventario = :nPK'
      ' GROUP BY LC.nCdLoteContagemInventario'
      #9'     ,cNrLote'
      '         ,P.nCdProduto'
      '         ,cNmProduto'
      ' ORDER BY cNrLote')
    Left = 944
    Top = 53
    object cNrLote: TStringField
      DisplayLabel = 'Lote'
      FieldName = 'cNrLote'
      Size = 30
    end
    object qryItemLoteContagemnCdProduto: TIntegerField
      DisplayLabel = 'C'#243'd.'
      FieldName = 'nCdProduto'
    end
    object qryItemLoteContagemcNmProduto: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryItemLoteContagemnQtdeContada: TBCDField
      DisplayLabel = 'Qtde Contada'
      FieldName = 'nQtdeContada'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object dsItemLoteContagem: TDataSource
    DataSet = qryItemLoteContagem
    Left = 944
    Top = 85
  end
  object PopupMenu1: TPopupMenu
    Left = 784
    Top = 49
    object ExportarInventario1: TMenuItem
      Caption = 'Exportar Inventario'
      OnClick = ExportarInventario1Click
    end
  end
end
