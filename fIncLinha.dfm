inherited frmIncLinha: TfrmIncLinha
  Left = 354
  Top = 268
  Width = 530
  Height = 117
  Caption = 'Linha de Produtos'
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 514
    Height = 52
  end
  object Label1: TLabel [1]
    Left = 16
    Top = 48
    Width = 26
    Height = 13
    Caption = 'Linha'
  end
  inherited ToolBar1: TToolBar
    Width = 514
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object Edit1: TEdit [3]
    Left = 48
    Top = 40
    Width = 441
    Height = 21
    MaxLength = 50
    TabOrder = 1
    OnKeyDown = Edit1KeyDown
  end
  object usp_Linha: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_INSERI_LINHA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@cNmLinha'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
      end
      item
        Name = '@nCdMarca'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdLinha'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
      end>
    Left = 280
    Top = 40
  end
end
