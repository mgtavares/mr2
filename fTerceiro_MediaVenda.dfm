inherited frmTerceiro_MediaVenda: TfrmTerceiro_MediaVenda
  Top = 46
  Width = 907
  Height = 639
  Caption = 'Evolu'#231#227'o Venda Terceiro'
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 89
    Width = 891
    Height = 512
  end
  inherited ToolBar1: TToolBar
    Width = 891
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object RadioGroup1: TRadioGroup [2]
    Left = 0
    Top = 29
    Width = 891
    Height = 60
    Align = alTop
    Caption = 'Per'#237'odo'
    Columns = 5
    ItemIndex = 1
    Items.Strings = (
      #218'ltimos 6 meses'
      #218'ltimos 12 meses'
      #218'ltimos 24 meses'
      #218'ltimos 36 meses'
      'Hist'#243'rico Total')
    TabOrder = 1
    OnClick = RadioGroup1Click
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 89
    Width = 891
    Height = 512
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 508
    ClientRectLeft = 4
    ClientRectRight = 887
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'An'#225'lise Quantitativa'
      ImageIndex = 0
      object cxPageControl2: TcxPageControl
        Left = 0
        Top = 0
        Width = 289
        Height = 484
        ActivePage = cxTabSheet3
        Align = alLeft
        LookAndFeel.NativeStyle = True
        TabOrder = 0
        ClientRectBottom = 480
        ClientRectLeft = 4
        ClientRectRight = 285
        ClientRectTop = 24
        object cxTabSheet3: TcxTabSheet
          Caption = 'An'#225'lise Mensal'
          ImageIndex = 0
          object cxGrid2: TcxGrid
            Left = 0
            Top = 0
            Width = 281
            Height = 456
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Consolas'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView1: TcxGridDBTableView
              OnDblClick = cxGridDBTableView1DblClick
              DataController.DataSource = dsFaturamentoMensal
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  FieldName = 'nValTotal'
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  Column = cxGridDBTableView1nValorTotal
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsCustomize.ColumnGrouping = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsSelection.CellMultiSelect = True
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              Styles.Content = frmMenu.FonteSomenteLeitura
              object cxGridDBTableView1cMesAno: TcxGridDBColumn
                DataBinding.FieldName = 'cMesAno'
                Width = 88
              end
              object cxGridDBTableView1nValorTotal: TcxGridDBColumn
                DataBinding.FieldName = 'nValorTotal'
                HeaderAlignmentHorz = taRightJustify
                Width = 107
              end
              object cxGridDBTableView1nVariacao: TcxGridDBColumn
                DataBinding.FieldName = 'nVariacao'
                HeaderAlignmentHorz = taRightJustify
                Width = 65
              end
            end
            object cxGridLevel1: TcxGridLevel
              GridView = cxGridDBTableView1
            end
          end
        end
      end
      object cxPageControl3: TcxPageControl
        Left = 289
        Top = 0
        Width = 311
        Height = 484
        ActivePage = cxTabSheet4
        Align = alClient
        LookAndFeel.NativeStyle = True
        TabOrder = 1
        ClientRectBottom = 480
        ClientRectLeft = 4
        ClientRectRight = 307
        ClientRectTop = 24
        object cxTabSheet4: TcxTabSheet
          Caption = 'An'#225'lise Semestral'
          ImageIndex = 0
          object cxGrid1: TcxGrid
            Left = 0
            Top = 0
            Width = 303
            Height = 456
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Consolas'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView2: TcxGridDBTableView
              OnDblClick = cxGridDBTableView2DblClick
              DataController.DataSource = dsFaturamentoSemestral
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  FieldName = 'nValTotal'
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  Column = cxGridDBTableView2nValorTotal
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsCustomize.ColumnGrouping = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsSelection.CellMultiSelect = True
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              Styles.Content = frmMenu.FonteSomenteLeitura
              object cxGridDBTableView2cSemestreAno: TcxGridDBColumn
                DataBinding.FieldName = 'cSemestreAno'
                Width = 114
              end
              object cxGridDBTableView2nValorTotal: TcxGridDBColumn
                DataBinding.FieldName = 'nValorTotal'
                HeaderAlignmentHorz = taRightJustify
                Width = 107
              end
              object cxGridDBTableView2nVariacao: TcxGridDBColumn
                DataBinding.FieldName = 'nVariacao'
                HeaderAlignmentHorz = taRightJustify
                Width = 65
              end
            end
            object cxGridLevel2: TcxGridLevel
              GridView = cxGridDBTableView2
            end
          end
        end
      end
      object cxPageControl4: TcxPageControl
        Left = 600
        Top = 0
        Width = 283
        Height = 484
        ActivePage = cxTabSheet5
        Align = alRight
        LookAndFeel.NativeStyle = True
        TabOrder = 2
        ClientRectBottom = 480
        ClientRectLeft = 4
        ClientRectRight = 279
        ClientRectTop = 24
        object cxTabSheet5: TcxTabSheet
          Caption = 'An'#225'lise Anual'
          ImageIndex = 0
          object cxGrid3: TcxGrid
            Left = 0
            Top = 0
            Width = 275
            Height = 456
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Consolas'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView3: TcxGridDBTableView
              OnDblClick = cxGridDBTableView3DblClick
              DataController.DataSource = dsFaturamentoAnual
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  FieldName = 'nValTotal'
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  Column = cxGridDBTableView3nValorTotal
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsCustomize.ColumnGrouping = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsSelection.CellMultiSelect = True
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              Styles.Content = frmMenu.FonteSomenteLeitura
              object cxGridDBTableView3iAno: TcxGridDBColumn
                DataBinding.FieldName = 'iAno'
              end
              object cxGridDBTableView3nValorTotal: TcxGridDBColumn
                DataBinding.FieldName = 'nValorTotal'
                HeaderAlignmentHorz = taRightJustify
                Width = 107
              end
              object cxGridDBTableView3nVariacao: TcxGridDBColumn
                DataBinding.FieldName = 'nVariacao'
                HeaderAlignmentHorz = taRightJustify
                Width = 65
              end
            end
            object cxGridLevel3: TcxGridLevel
              GridView = cxGridDBTableView3
            end
          end
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'An'#225'lise Gr'#225'fica'
      ImageIndex = 1
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 883
        Height = 37
        Align = alTop
        TabOrder = 0
        object cxButton1: TcxButton
          Left = 4
          Top = 4
          Width = 85
          Height = 29
          Caption = 'Imprimir'
          TabOrder = 0
          Visible = False
          OnClick = cxButton1Click
          Glyph.Data = {
            0A020000424D0A0200000000000036000000280000000C0000000D0000000100
            180000000000D401000000000000000000000000000000000000C2C2C29E9E9E
            656464656464656464656464656464656464656464656464898A89B9BABAD5D5
            D5515757313332484E4E3133323D42423D42423D42423D4242191D23515757C6
            C6C68C908F191D23313332313332313332313332313332313332313332191D23
            040506898A89656464898A89DADDD7B9BABABEBEBEBEBEBEBEBEBEBEBEBEBEBA
            BCCCCCCC5157576564647172723D4242898A89777B7B7B85857B85857B85857B
            85857B85859392923D4242777B7BB6B6B63133327B8585AEAEAEA4A8A8A4A8A8
            A4A8A8A4A8A8A4A8A8484E4E313332B9BABAD5D5D5484E4EC6C6C6FCFEFEFCFE
            FEFCFEFEFCFEFEFCFEFEFCFEFE898A89484E4EC6C6C6CCCCCC484E4EC6C6C6FC
            FEFEFCFEFEFCFEFEFCFEFEFCFEFEFCFEFE7B85853D4242C2C2C2CCCCCC484E4E
            C6C6C6FCFEFEFCFEFEFCFEFEFCFEFEFCFEFED5D5D57172723D4242C2C2C2CCCC
            CC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE7B8585191D23191D23515757C6
            C6C6CCCCCC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE717272484E4E3D4242
            939292C6C6C6CCCCCC484E4E313332575D5E515757515757575D5E191D23191D
            23939292CCCCCCBEBEBEC2C2C2B6B6B69E9E9E9E9E9E9E9E9E9E9E9E9E9E9EA4
            A8A8AEAEAEC6C6C6BEBEBEBEBEBE}
          LookAndFeel.NativeStyle = True
        end
      end
      object DBChart1: TDBChart
        Left = 0
        Top = 37
        Width = 883
        Height = 449
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        Title.Text.Strings = (
          'An'#225'lise de Vendas')
        Align = alClient
        TabOrder = 1
        object Series1: TBarSeries
          ColorEachPoint = True
          Marks.ArrowLength = 20
          Marks.Style = smsValue
          Marks.Visible = True
          DataSource = qryFaturamentoMensal
          SeriesColor = clRed
          XLabelsSource = 'cMesAno'
          XValues.DateTime = False
          XValues.Name = 'X'
          XValues.Multiplier = 1.000000000000000000
          XValues.Order = loAscending
          YValues.DateTime = False
          YValues.Name = 'Bar'
          YValues.Multiplier = 1.000000000000000000
          YValues.Order = loNone
          YValues.ValueSource = 'nValorTotal'
        end
      end
    end
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempFaturamentoTerceiro'#39') IS NOT NULL)'
      '    DROP TABLE #TempFaturamentoTerceiro'
      ''
      'CREATE TABLE #TempFaturamentoTerceiro (iAno        int'
      '                                      ,iMes        int'
      '                                      ,cMesAno     varchar(7)'
      
        '                                      ,iSemestre   int          ' +
        ' default 0 not null'
      
        '                                      ,nValorTotal DECIMAL(12,2)' +
        ' default 0 not null)'
      ''
      ''
      
        'IF (OBJECT_ID('#39'tempdb..#TempFaturamentoTerceiroMensal'#39') IS NOT N' +
        'ULL)'
      '    DROP TABLE #TempFaturamentoTerceiroMensal'
      ''
      'CREATE TABLE #TempFaturamentoTerceiroMensal (iAno        int'
      '                                            ,iMes        int'
      
        '                                            ,cMesAno     varchar' +
        '(7)'
      
        '                                            ,nValorTotal DECIMAL' +
        '(12,2) default 0 not null'
      
        '                                            ,nVariacao   DECIMAL' +
        '(12,2))'
      ''
      ''
      
        'IF (OBJECT_ID('#39'tempdb..#TempFaturamentoTerceiroSemestre'#39') IS NOT' +
        ' NULL)'
      '    DROP TABLE #TempFaturamentoTerceiroSemestre'
      ''
      'CREATE TABLE #TempFaturamentoTerceiroSemestre (iAno          int'
      '                                              ,iSemestre     int'
      
        '                                              ,cSemestreAno  var' +
        'char(15)'
      
        '                                              ,nValorTotal   DEC' +
        'IMAL(12,2) default 0 not null'
      
        '                                              ,nVariacao     DEC' +
        'IMAL(12,2))'
      ''
      
        'IF (OBJECT_ID('#39'tempdb..#TempFaturamentoTerceiroAno'#39') IS NOT NULL' +
        ')'
      '    DROP TABLE #TempFaturamentoTerceiroAno'
      ''
      'CREATE TABLE #TempFaturamentoTerceiroAno (iAno        int'
      
        '                                         ,nValorTotal DECIMAL(12' +
        ',2) default 0 not null'
      
        '                                         ,nVariacao   DECIMAL(12' +
        ',2))'
      '                                      ')
    Left = 88
    Top = 128
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'dDtInicialAux'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @dDtInicialAux       VARCHAR(10)'
      '       ,@dDtInicial          DATETIME'
      
        #9'     ,@dDtFinal            DATETIME --A data final '#233' datetime p' +
        'orque a data sempre ser'#225' fixada dentro dessa SQL e n'#227'o pela inte' +
        'rface.'
      '       ,@nCdTerceiro         int'
      '       ,@nValorTotal         DECIMAL(12,2)'
      '       ,@nValorTotalAnterior DECIMAl(12,2)'
      '       ,@iAno                int'
      '       ,@iMes                int'
      '       ,@iSemestre           int'
      ''
      'Set @dDtInicialAux  = :dDtInicialAux'
      'Set @dDtInicial     = Convert(DATETIME,@dDtInicialAux,103)'
      'Set @dDtFinal       = dbo.fn_OnlyDate(GetDate()) +1'
      'Set @nCdTerceiro    = :nPK'
      ''
      '--'
      '-- Prepara os dados do faturamento mensal'
      '--'
      'IF (OBJECT_ID('#39'tempdb..#TempFaturamentoTerceiro'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempFaturamentoTerceiro (iAno        int'
      '                                          ,iMes        int'
      
        '                                          ,cMesAno     varchar(7' +
        ')'
      
        '                                          ,iSemestre   int      ' +
        '     default 0 not null'
      
        '                                          ,nValorTotal DECIMAL(1' +
        '2,2) default 0 not null)'
      ''
      'END'
      ''
      'INSERT INTO #TempFaturamentoTerceiro (iAno'
      '                                     ,iMes'
      '                                     ,nValorTotal)'
      #9#9#9#9#9#9#9#9'SELECT Dados.iAno'
      #9#9#9#9#9#9#9#9#9'  ,Dados.iMes'
      
        #9#9#9#9#9#9#9#9#9'  ,Convert(DECIMAL(12,2),Sum(Dados.nValor1))  as nValor' +
        'Total'
      
        #9#9#9#9#9#9#9#9'  FROM (SELECT DatePart(year,dDtAtendimento)      as iAn' +
        'o'
      #9#9#9#9#9#9#9#9#9#9#9'  ,DatePart(month,dDtAtendimento)     as iMes'
      #9#9#9#9#9#9#9#9#9#9#9'  ,Sum(nQtdeAtendida * nValCustoUnit) as nValor1'
      #9#9#9#9#9#9#9#9#9#9'  FROM ItemPedidoAtendido'
      
        #9#9#9#9#9#9#9#9#9#9#9'   INNER JOIN ItemPedido IP  ON IP.nCdItemPedido     ' +
        '            = ItemPedidoAtendido.nCdItemPedido'
      
        #9#9#9#9#9#9#9#9#9#9#9'   INNER JOIN Pedido         ON Pedido.nCdPedido     ' +
        '            = IP.nCdPedido'
      
        #9#9#9#9#9#9#9#9#9#9#9'   INNER JOIN TipoPedido     ON TipoPedido.nCdTipoPed' +
        'ido         = Pedido.nCdTipoPedido'
      
        #9#9#9#9#9#9#9#9#9#9#9'   INNER JOIN Terceiro       ON Terceiro.nCdTerceiro ' +
        '            = Pedido.nCdTerceiroPagador'
      #9#9#9#9#9#9#9#9#9#9' WHERE cFlgVenda                  = 1'
      #9#9#9#9#9#9#9#9#9#9'   AND cGerarFinanc               = 1'
      #9#9#9#9#9#9#9#9#9#9'   AND Pedido.nCdTabStatusPed    IN (3,4,5)'
      
        #9#9#9#9#9#9#9#9#9#9'   AND IP.nCdTipoItemPed         <> 6  -- N'#227'o incluir ' +
        'os itens do kit formulado'
      
        #9#9#9#9#9#9#9#9#9#9'   AND dDtAtendimento             BETWEEN @dDtInicial ' +
        'AND @dDtFinal'
      #9#9#9#9#9#9#9#9#9#9'   AND Pedido.nCdTerceiroPagador  = @nCdTerceiro'
      #9#9#9#9#9#9#9#9#9#9' GROUP BY DatePart(year,dDtAtendimento)'
      #9#9#9#9#9#9#9#9#9#9#9#9' ,DatePart(month,dDtAtendimento)'
      #9#9#9#9#9#9#9#9'         '
      #9#9#9#9#9#9#9#9#9#9'UNION ALL'
      #9#9#9#9#9#9#9#9#9#9'SELECT DatePart(year,dDtAtendimento)      as iAno'
      #9#9#9#9#9#9#9#9#9#9#9'  ,DatePart(month,dDtAtendimento)     as iMes'
      
        #9#9#9#9#9#9#9#9#9#9#9'  ,Sum(((IPF.nQtdePed / IP.nQtdePed) * nQtdeAtendida)' +
        ' * IPF.nValCustoUnit) as nValor1'
      #9#9#9#9#9#9#9#9#9#9'  FROM ItemPedidoAtendido'
      
        #9#9#9#9#9#9#9#9#9#9#9'   INNER JOIN ItemPedido IP  ON IP.nCdItemPedido     ' +
        '            = ItemPedidoAtendido.nCdItemPedido'
      
        #9#9#9#9#9#9#9#9#9#9#9'   INNER JOIN ItemPedido IPF ON IPF.nCdItemPedidoPai ' +
        '            = IP.nCdItemPedido'
      
        #9#9#9#9#9#9#9#9#9#9#9'   INNER JOIN Pedido         ON Pedido.nCdPedido     ' +
        '            = IP.nCdPedido'
      
        #9#9#9#9#9#9#9#9#9#9#9'   INNER JOIN TipoPedido     ON TipoPedido.nCdTipoPed' +
        'ido         = Pedido.nCdTipoPedido'
      #9#9#9#9#9#9#9#9#9#9' WHERE cFlgVenda                  = 1'
      #9#9#9#9#9#9#9#9#9#9'   AND cGerarFinanc               = 1'
      #9#9#9#9#9#9#9#9#9#9'   AND Pedido.nCdTabStatusPed    IN (3,4,5)'
      
        #9#9#9#9#9#9#9#9#9#9'   AND IP.nCdTipoItemPed          = 6  -- Somente os i' +
        'tens do kit formulado'
      
        #9#9#9#9#9#9#9#9#9#9'   AND dDtAtendimento             BETWEEN @dDtInicial ' +
        'AND @dDtFinal'
      #9#9#9#9#9#9#9#9#9#9'   AND Pedido.nCdTerceiroPagador  = @nCdTerceiro'
      #9#9#9#9#9#9#9#9#9#9' GROUP BY DatePart(year,dDtAtendimento) '
      #9#9#9#9#9#9#9#9#9#9#9#9' ,DatePart(month,dDtAtendimento)) Dados'
      #9#9#9#9#9#9#9#9'  GROUP BY Dados.iAno'
      #9#9#9#9#9#9#9#9#9#9'  ,Dados.iMes'
      ''
      ''
      'UPDATE #TempFaturamentoTerceiro'
      
        '   Set cMesAno   = dbo.fn_ZeroEsquerda(iMes,2) + '#39'/'#39' + dbo.fn_Ze' +
        'roEsquerda(iAno,4)'
      '      ,iSemestre = CASE WHEN iMes <= 6 THEN 1'
      '                        ELSE 2'
      '                   END'
      ''
      ''
      ''
      '--'
      '-- Prepara os dados do faturamento semestral'
      '--'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempFaturamentoTerceiroMensal'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempFaturamentoTerceiroMensal (iAno        int'
      '                                                ,iMes        int'
      
        '                                                ,cMesAno     var' +
        'char(7)'
      
        '                                                ,nValorTotal DEC' +
        'IMAL(12,2) default 0 not null'
      
        '                                                ,nVariacao   DEC' +
        'IMAL(12,2))'
      ''
      'END'
      ''
      'INSERT INTO #TempFaturamentoTerceiroMensal (iAno'
      '                                           ,iMes'
      '                                           ,nValorTotal)'
      '                                     SELECT iAno'
      '                                           ,iMes'
      '                                           ,nValorTotal'
      
        '                                       FROM #TempFaturamentoTerc' +
        'eiro'
      '                                      ORDER BY iAno'
      '                                              ,iMes'
      ''
      'Set @nValorTotal         = 0'
      'Set @nValorTotalAnterior = NULL'
      ''
      'DECLARE curFatMensal CURSOR'
      '    FOR SELECT iAno'
      '              ,iMes'
      '              ,nValorTotal'
      '          FROM #TempFaturamentoTerceiroMensal'
      '         ORDER BY iAno'
      '                 ,iMes'
      ''
      'OPEN curFatMensal'
      ''
      'FETCH NEXT'
      ' FROM curFatMensal'
      ' INTO @iAno'
      '     ,@iMes'
      '     ,@nValorTotal'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      '    IF (@nValorTotalAnterior IS NULL)'
      '    BEGIN'
      ''
      '        UPDATE #TempFaturamentoTerceiroMensal'
      '           SET nVariacao = NULL'
      '         WHERE iAno      = @iAno'
      '           AND iMes      = @iMes'
      ''
      '    END'
      ''
      '    IF (@nValorTotalAnterior = 0)'
      '    BEGIN'
      ''
      '        UPDATE #TempFaturamentoTerceiroMensal'
      '           SET nVariacao = 100'
      '         WHERE iAno      = @iAno'
      '           AND iMes      = @iMes'
      ''
      '    END'
      ''
      '    IF (@nValorTotalAnterior <> 0)'
      '    BEGIN'
      ''
      '        UPDATE #TempFaturamentoTerceiroMensal'
      
        '           SET nVariacao = ((nValorTotal / @nValorTotalAnterior)' +
        ' * 100) - 100'
      '         WHERE iAno      = @iAno'
      '           AND iMes      = @iMes'
      '           AND nValorTotal <> 0'
      ''
      '    END'
      ''
      #9'UPDATE #TempFaturamentoTerceiroMensal'
      
        #9'   Set cMesAno   = dbo.fn_ZeroEsquerda(iAno,4) + '#39'/'#39' + dbo.fn_Z' +
        'eroEsquerda(iMes,2) '
      '     WHERE iAno      = @iAno'
      '       AND iMes      = @iMes'
      ''
      '    Set @nValorTotalAnterior = @nValorTotal'
      ''
      #9'FETCH NEXT'
      #9' FROM curFatMensal'
      #9' INTO @iAno'
      #9#9' ,@iMes'
      #9#9' ,@nValorTotal'
      ''
      'END'
      ''
      'CLOSE curFatMensal'
      'DEALLOCATE curFatMensal'
      ''
      ''
      ''
      ''
      '--'
      '-- Prepara os dados do faturamento semestral'
      '--'
      ''
      
        'IF (OBJECT_ID('#39'tempdb..#TempFaturamentoTerceiroSemestre'#39') IS NUL' +
        'L)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempFaturamentoTerceiroSemestre (iAno         ' +
        ' int'
      
        '                                                  ,iSemestre    ' +
        ' int'
      
        '                                                  ,cSemestreAno ' +
        ' varchar(15)'
      
        '                                                  ,nValorTotal  ' +
        ' DECIMAL(12,2) default 0 not null'
      
        '                                                  ,nVariacao    ' +
        ' DECIMAL(12,2))'
      ''
      'END'
      ''
      'INSERT INTO #TempFaturamentoTerceiroSemestre (iAno'
      '                                             ,iSemestre'
      '                                             ,cSemestreAno'
      '                                             ,nValorTotal)'
      '                                       SELECT iAno'
      '                                             ,iSemestre'
      
        '                                             ,Convert(VARCHAR(1)' +
        ',iSemestre) + '#39#186' de '#39' + Convert(VARCHAR(4),iAno)'
      '                                             ,Sum(nValorTotal)'
      
        '                                         FROM #TempFaturamentoTe' +
        'rceiro'
      '                                        GROUP BY iAno'
      '                                                ,iSemestre'
      
        '                                                ,Convert(VARCHAR' +
        '(1),iSemestre) + '#39#186' de '#39' + Convert(VARCHAR(4),iAno)'
      ''
      'Set @nValorTotal         = 0'
      'Set @nValorTotalAnterior = NULL'
      ''
      'DECLARE curFatSemestreal CURSOR'
      '    FOR SELECT iAno'
      '              ,iSemestre'
      '              ,nValorTotal'
      '          FROM #TempFaturamentoTerceiroSemestre'
      '         ORDER BY iAno'
      '                 ,iSemestre'
      ''
      'OPEN curFatSemestreal'
      ''
      'FETCH NEXT'
      ' FROM curFatSemestreal'
      ' INTO @iAno'
      '     ,@iSemestre'
      '     ,@nValorTotal'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      '    IF (@nValorTotalAnterior IS NULL)'
      '    BEGIN'
      ''
      '        UPDATE #TempFaturamentoTerceiroSemestre'
      '           SET nVariacao = NULL'
      '         WHERE iAno      = @iAno'
      '           AND iSemestre = @iSemestre'
      ''
      '    END'
      ''
      '    IF (@nValorTotalAnterior = 0)'
      '    BEGIN'
      ''
      '        UPDATE #TempFaturamentoTerceiroSemestre'
      '           SET nVariacao = 100'
      '         WHERE iAno      = @iAno'
      '           AND iSemestre = @iSemestre'
      ''
      '    END'
      ''
      '    IF (@nValorTotalAnterior <> 0)'
      '    BEGIN'
      ''
      '        UPDATE #TempFaturamentoTerceiroSemestre'
      
        '           SET nVariacao = ((nValorTotal / @nValorTotalAnterior)' +
        ' * 100) - 100'
      '         WHERE iAno      = @iAno'
      '           AND iSemestre = @iSemestre'
      '           AND nValorTotal <> 0'
      ''
      '    END'
      ''
      '    Set @nValorTotalAnterior = @nValorTotal'
      ''
      #9'FETCH NEXT'
      #9' FROM curFatSemestreal'
      #9' INTO @iAno'
      #9#9' ,@iSemestre'
      #9#9' ,@nValorTotal'
      ''
      'END'
      ''
      'CLOSE curFatSemestreal'
      'DEALLOCATE curFatSemestreal'
      ''
      ''
      '--'
      '-- Prepara os dados do faturamento anual'
      '--'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempFaturamentoTerceiroAno'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempFaturamentoTerceiroAno (iAno        int'
      
        '                                             ,nValorTotal DECIMA' +
        'L(12,2) default 0 not null'
      
        '                                             ,nVariacao   DECIMA' +
        'L(12,2))'
      ''
      'END'
      ''
      'INSERT INTO #TempFaturamentoTerceiroAno (iAno'
      '                                        ,nValorTotal)'
      '                                  SELECT iAno'
      '                                        ,Sum(nValorTotal)'
      
        '                                    FROM #TempFaturamentoTerceir' +
        'o'
      '                                   GROUP BY iAno'
      ''
      'Set @nValorTotal         = 0'
      'Set @nValorTotalAnterior = NULL'
      ''
      'DECLARE curFatAnual CURSOR'
      '    FOR SELECT iAno'
      '              ,nValorTotal'
      '          FROM #TempFaturamentoTerceiroAno'
      '         ORDER BY iAno'
      ''
      'OPEN curFatAnual'
      ''
      'FETCH NEXT'
      ' FROM curFatAnual'
      ' INTO @iAno'
      '     ,@nValorTotal'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      '    IF (@nValorTotalAnterior IS NULL)'
      '    BEGIN'
      ''
      '        UPDATE #TempFaturamentoTerceiroAno'
      '           SET nVariacao = NULL'
      '         WHERE iAno      = @iAno'
      ''
      '    END'
      ''
      '    IF (@nValorTotalAnterior = 0)'
      '    BEGIN'
      ''
      '        UPDATE #TempFaturamentoTerceiroAno'
      '           SET nVariacao = 100'
      '         WHERE iAno      = @iAno'
      ''
      '    END'
      ''
      '    IF (@nValorTotalAnterior <> 0)'
      '    BEGIN'
      ''
      '        UPDATE #TempFaturamentoTerceiroAno'
      
        '           SET nVariacao = ((nValorTotal / @nValorTotalAnterior)' +
        ' * 100) - 100'
      '         WHERE iAno      = @iAno'
      '           AND nValorTotal <> 0'
      ''
      '    END'
      ''
      '    Set @nValorTotalAnterior = @nValorTotal'
      ''
      #9'FETCH NEXT'
      #9' FROM curFatAnual'
      #9' INTO @iAno'
      #9#9' ,@nValorTotal'
      ''
      'END'
      ''
      'CLOSE curFatAnual'
      'DEALLOCATE curFatAnual'
      '')
    Left = 88
    Top = 176
  end
  object qryFaturamentoMensal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempFaturamentoTerceiroMensal'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempFaturamentoTerceiroMensal (iAno        int'
      '                                                ,iMes        int'
      
        '                                                ,cMesAno     var' +
        'char(7)'
      
        '                                                ,nValorTotal DEC' +
        'IMAL(12,2) default 0 not null'
      
        '                                                ,nVariacao   DEC' +
        'IMAL(12,2))'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #TempFaturamentoTerceiroMensal'
      ' ORDER BY iAno'
      '         ,iMes')
    Left = 24
    Top = 272
    object qryFaturamentoMensaliAno: TIntegerField
      FieldName = 'iAno'
    end
    object qryFaturamentoMensaliMes: TIntegerField
      FieldName = 'iMes'
    end
    object qryFaturamentoMensalcMesAno: TStringField
      DisplayLabel = 'M'#234's / Ano'
      FieldName = 'cMesAno'
      Size = 7
    end
    object qryFaturamentoMensalnValorTotal: TBCDField
      DisplayLabel = 'Valor Venda'
      FieldName = 'nValorTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryFaturamentoMensalnVariacao: TBCDField
      DisplayLabel = '% Var.'
      FieldName = 'nVariacao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object qryFaturamentoSemestral: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      
        'IF (OBJECT_ID('#39'tempdb..#TempFaturamentoTerceiroSemestre'#39') IS NUL' +
        'L)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempFaturamentoTerceiroSemestre (iAno         ' +
        ' int'
      
        '                                                  ,iSemestre    ' +
        ' int'
      
        '                                                  ,cSemestreAno ' +
        ' varchar(15)'
      
        '                                                  ,nValorTotal  ' +
        ' DECIMAL(12,2) default 0 not null'
      
        '                                                  ,nVariacao    ' +
        ' DECIMAL(12,2))'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #TempFaturamentoTerceiroSemestre'
      ' ORDER BY iAno'
      '         ,iSemestre')
    Left = 64
    Top = 264
    object qryFaturamentoSemestraliAno: TIntegerField
      FieldName = 'iAno'
    end
    object qryFaturamentoSemestraliSemestre: TIntegerField
      FieldName = 'iSemestre'
    end
    object qryFaturamentoSemestralcSemestreAno: TStringField
      DisplayLabel = 'Semestre'
      FieldName = 'cSemestreAno'
      Size = 15
    end
    object qryFaturamentoSemestralnValorTotal: TBCDField
      DisplayLabel = 'Valor Venda'
      FieldName = 'nValorTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryFaturamentoSemestralnVariacao: TBCDField
      DisplayLabel = '% Var.'
      FieldName = 'nVariacao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object qryFaturamentoAnual: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempFaturamentoTerceiroAno'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempFaturamentoTerceiroAno (iAno        int'
      
        '                                             ,nValorTotal DECIMA' +
        'L(12,2) default 0 not null'
      
        '                                             ,nVariacao   DECIMA' +
        'L(12,2))'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #TempFaturamentoTerceiroAno'
      ' ORDER BY iAno')
    Left = 104
    Top = 264
    object qryFaturamentoAnualiAno: TIntegerField
      DisplayLabel = 'Ano'
      FieldName = 'iAno'
      DisplayFormat = ',0'
    end
    object qryFaturamentoAnualnValorTotal: TBCDField
      DisplayLabel = 'Valor Venda'
      FieldName = 'nValorTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryFaturamentoAnualnVariacao: TBCDField
      DisplayLabel = '% Var.'
      FieldName = 'nVariacao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsFaturamentoMensal: TDataSource
    DataSet = qryFaturamentoMensal
    Left = 24
    Top = 304
  end
  object dsFaturamentoSemestral: TDataSource
    DataSet = qryFaturamentoSemestral
    Left = 64
    Top = 304
  end
  object dsFaturamentoAnual: TDataSource
    DataSet = qryFaturamentoAnual
    Left = 104
    Top = 304
  end
end
