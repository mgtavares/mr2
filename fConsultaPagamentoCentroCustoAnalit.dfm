inherited frmConsultaPagamentoCentroCustoAnalit: TfrmConsultaPagamentoCentroCustoAnalit
  Left = -8
  Top = -8
  Width = 1152
  Height = 850
  Caption = 'Consulta Pagamento Centro Custo Analit'#237'tico'
  OldCreateOrder = True
  Position = poDesktopCenter
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1136
    Height = 785
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 785
    Align = alClient
    DataSource = DataSource1
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 39
      end
      item
        EditButtons = <>
        FieldName = 'nCdLojaTit'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 39
      end
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 71
      end
      item
        EditButtons = <>
        FieldName = 'iParcela'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 39
      end
      item
        EditButtons = <>
        FieldName = 'nCdRecebimento'
        Footers = <>
        Width = 75
      end
      item
        EditButtons = <>
        FieldName = 'cNrNf'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 51
      end
      item
        EditButtons = <>
        FieldName = 'nCdSP'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 44
      end
      item
        EditButtons = <>
        FieldName = 'nValTit'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 67
      end
      item
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 47
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 140
      end
      item
        EditButtons = <>
        FieldName = 'dDtLiq'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 79
      end
      item
        EditButtons = <>
        FieldName = 'nValMov'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 67
      end
      item
        EditButtons = <>
        FieldName = 'nValor'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 67
      end
      item
        EditButtons = <>
        FieldName = 'valorProporcional'
        Footers = <>
        Width = 77
      end
      item
        EditButtons = <>
        FieldName = 'nPercent'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 62
      end
      item
        EditButtons = <>
        FieldName = 'ContaBancaria'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 135
      end
      item
        EditButtons = <>
        FieldName = 'cNmFormaPagto'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 133
      end
      item
        EditButtons = <>
        FieldName = 'iNrCheque'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdFormaPagto'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdContaBancaria'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdCC'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdCC1'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdCC2'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cCdCC'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cNmCC'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'dDtMov'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Visible = False
      end>
  end
  inherited ImageList1: TImageList
    Left = 384
    Top = 184
  end
  object uspConsulta: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SPREL_PAGAMENTOS_CENTROCUSTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdCentroCustoNivel1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdCentroCustoNivel2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdCentroCustoNivel3'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@dDtInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@dDtFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 624
    Top = 288
    object uspConsultanValMov: TBCDField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Valor Mov.'
      FieldName = 'nValMov'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspConsultanCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|ID'
      FieldName = 'nCdTitulo'
    end
    object uspConsultanCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object uspConsultanCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object uspConsultacNmFormaPagto: TStringField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Forma Pagamento'
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object uspConsultaiNrCheque: TIntegerField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Nr. Cheque'
      FieldName = 'iNrCheque'
    end
    object uspConsultaContaBancaria: TStringField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Conta Bancaria'
      FieldName = 'ContaBancaria'
      Size = 92
    end
    object uspConsultanCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object uspConsultanCdCC1: TIntegerField
      FieldName = 'nCdCC1'
    end
    object uspConsultanCdCC2: TIntegerField
      FieldName = 'nCdCC2'
    end
    object uspConsultacCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object uspConsultacNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
    object uspConsultanPercent: TBCDField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|% Rateio'
      FieldName = 'nPercent'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspConsultavalorProporcional: TBCDField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Valor Proporcional'
      FieldName = 'valorProporcional'
      DisplayFormat = '#,##0.00'
      Precision = 29
      Size = 8
    end
    object uspConsultanValor: TBCDField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Valor T'#237'tulo'
      FieldName = 'nValor'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object uspConsultanCdSP: TIntegerField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|SP'
      FieldName = 'nCdSP'
    end
    object uspConsultacNrTit: TStringField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      Size = 48
    end
    object uspConsultanCdTerceiro: TIntegerField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Terceiro|C'#243'd.'
      FieldName = 'nCdTerceiro'
    end
    object uspConsultacNmTerceiro: TStringField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Terceiro|Nome'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object uspConsultadDtLiq: TDateTimeField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Data Liquida'#231#227'o'
      FieldName = 'dDtLiq'
    end
    object uspConsultadDtMov: TDateTimeField
      FieldName = 'dDtMov'
    end
    object uspConsultanValTit: TBCDField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Valor T'#237'tulo'
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object uspConsultaiParcela: TIntegerField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Parc.'
      FieldName = 'iParcela'
    end
    object uspConsultanCdLojaTit: TIntegerField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Loja'
      FieldName = 'nCdLojaTit'
    end
    object uspConsultanCdEmpresa: TIntegerField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Emp.'
      FieldName = 'nCdEmpresa'
    end
    object uspConsultacNrNf: TStringField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Nr. NF'
      FieldName = 'cNrNf'
      FixedChar = True
      Size = 15
    end
    object uspConsultanCdRecebimento: TIntegerField
      DisplayLabel = 'T'#237'tulos Pagos Neste Centro de Custo|Recebimento'
      FieldName = 'nCdRecebimento'
    end
  end
  object DataSource1: TDataSource
    DataSet = uspConsulta
    Left = 656
    Top = 288
  end
end
