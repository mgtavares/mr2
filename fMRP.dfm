inherited frmMRP: TfrmMRP
  Left = 196
  Top = 172
  Caption = 'MRP'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel [1]
    Left = 6
    Top = 120
    Width = 114
    Height = 13
    Alignment = taRightJustify
    Caption = 'Previs'#227'o de Vendas At'#233
  end
  object Label5: TLabel [2]
    Left = 35
    Top = 72
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo de Produto'
  end
  object Label1: TLabel [3]
    Left = 52
    Top = 48
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo de MRP'
  end
  object Label2: TLabel [4]
    Left = 82
    Top = 96
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
  end
  object Label4: TLabel [5]
    Left = 214
    Top = 120
    Width = 146
    Height = 13
    Alignment = taRightJustify
    Caption = 'Previs'#227'o de Recebimentos At'#233
  end
  inherited ToolBar1: TToolBar
    TabOrder = 9
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtVendasAte: TMaskEdit [7]
    Left = 128
    Top = 112
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object edtGrupoProduto: TMaskEdit [8]
    Left = 128
    Top = 64
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtGrupoProdutoExit
    OnKeyDown = edtGrupoProdutoKeyDown
  end
  object edtGrupoMRP: TMaskEdit [9]
    Left = 128
    Top = 40
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = edtGrupoMRPExit
    OnKeyDown = edtGrupoMRPKeyDown
  end
  object edtProduto: TMaskEdit [10]
    Left = 128
    Top = 88
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = edtProdutoExit
    OnKeyDown = edtProdutoKeyDown
  end
  object edtRecebAte: TMaskEdit [11]
    Left = 368
    Top = 112
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object chkEstEmpenhado: TCheckBox [12]
    Left = 128
    Top = 144
    Width = 177
    Height = 17
    Caption = 'Considerar Estoque Empenhado'
    TabOrder = 5
  end
  object chkEstTerceiro: TCheckBox [13]
    Left = 128
    Top = 168
    Width = 177
    Height = 17
    Caption = 'Considerar Estoque de Terceiros'
    TabOrder = 6
  end
  object chkAgregarReceb: TCheckBox [14]
    Left = 128
    Top = 192
    Width = 305
    Height = 17
    Caption = 'Agregar Previs'#227'o de Recebimentos no Estoque Dispon'#237'vel'
    TabOrder = 7
  end
  object chkItemZero: TCheckBox [15]
    Left = 128
    Top = 216
    Width = 305
    Height = 17
    Caption = 'Exibir Itens sem Necessidade de Ressuprimento'
    TabOrder = 8
  end
  object DBEdit1: TDBEdit [16]
    Tag = 1
    Left = 192
    Top = 40
    Width = 449
    Height = 21
    DataField = 'cNmGrupoMRP'
    DataSource = DataSource1
    TabOrder = 10
  end
  object DBEdit2: TDBEdit [17]
    Tag = 1
    Left = 192
    Top = 64
    Width = 449
    Height = 21
    DataField = 'cNmGrupoProduto'
    DataSource = DataSource2
    TabOrder = 11
  end
  object DBEdit3: TDBEdit [18]
    Tag = 1
    Left = 192
    Top = 88
    Width = 449
    Height = 21
    DataField = 'cNmProduto'
    DataSource = DataSource3
    TabOrder = 12
  end
  object chkLeadCumulativo: TCheckBox [19]
    Left = 128
    Top = 240
    Width = 305
    Height = 17
    Caption = 'Lead Time Cumulativo'
    TabOrder = 13
  end
  inherited ImageList1: TImageList
    Left = 392
  end
  object qryGrupoMRP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoMRP'
      'WHERE nCdGrupoMRP = :nPK'
      'AND EXISTS(SELECT 1 FROM '
      'UsuarioGrupoMRP UGM'
      'WHERE UGM.nCdGrupoMRP = GrupoMRP.nCdGrupoMRP'
      'AND UGM.nCdUsuario = :nCdUsuario)')
    Left = 232
    Top = 248
    object qryGrupoMRPnCdGrupoMRP: TIntegerField
      FieldName = 'nCdGrupoMRP'
    end
    object qryGrupoMRPcNmGrupoMRP: TStringField
      FieldName = 'cNmGrupoMRP'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryGrupoMRP
    Left = 384
    Top = 240
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto, cNmGrupoProduto'
      'FROM GrupoProduto'
      'WHERE nCdGrupoProduto = :nPK')
    Left = 232
    Top = 296
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryGrupoProduto
    Left = 392
    Top = 248
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmProduto'
      '  FROM Produto'
      ' WHERE nCdStatus = 1'
      '   AND NOT EXISTS(SELECT 1'
      '                    FROM Produto Filho'
      
        '                   WHERE Filho.nCdProdutoPai = Produto.nCdProdut' +
        'o)'
      '   AND nCdTipoPlanejamento = 1'
      '   AND nCdProduto = :nPK')
    Left = 304
    Top = 304
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object DataSource3: TDataSource
    DataSet = qryProduto
    Left = 400
    Top = 256
  end
end
