inherited rptPerfOrcamento: TrptPerfOrcamento
  Caption = 'Performance de Or'#231'amentos'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 65
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label5: TLabel [2]
    Left = 73
    Top = 120
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
  end
  object Label3: TLabel [3]
    Left = 14
    Top = 144
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Or'#231'amento'
  end
  object Label6: TLabel [4]
    Left = 188
    Top = 144
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label2: TLabel [5]
    Left = 24
    Top = 96
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Economico'
  end
  object Label4: TLabel [6]
    Left = 11
    Top = 72
    Width = 95
    Height = 13
    Alignment = taRightJustify
    Caption = 'Unidade de Neg'#243'cio'
  end
  inherited ToolBar1: TToolBar
    TabOrder = 6
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [8]
    Tag = 1
    Left = 184
    Top = 40
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 7
  end
  object DBEdit3: TDBEdit [9]
    Tag = 1
    Left = 256
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 8
  end
  object DBEdit9: TDBEdit [10]
    Tag = 1
    Left = 184
    Top = 112
    Width = 129
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = DataSource4
    TabOrder = 9
  end
  object DBEdit10: TDBEdit [11]
    Tag = 1
    Left = 320
    Top = 112
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource4
    TabOrder = 10
  end
  object MaskEdit1: TMaskEdit [12]
    Left = 112
    Top = 136
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [13]
    Left = 208
    Top = 136
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [14]
    Left = 112
    Top = 40
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object MaskEdit6: TMaskEdit [15]
    Left = 112
    Top = 112
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object DBEdit1: TDBEdit [16]
    Tag = 1
    Left = 184
    Top = 88
    Width = 653
    Height = 21
    DataField = 'cNmGrupoEconomico'
    DataSource = dsGrupoEconomico
    TabOrder = 11
  end
  object MaskEdit4: TMaskEdit [17]
    Left = 112
    Top = 88
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object DBEdit4: TDBEdit [18]
    Tag = 1
    Left = 184
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmUnidadeNegocio'
    DataSource = DataSource6
    TabOrder = 12
  end
  object MaskEdit5: TMaskEdit [19]
    Left = 112
    Top = 64
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  inherited ImageList1: TImageList
    Left = 808
    Top = 80
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 488
    Top = 112
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nCdTerceiro')
    Left = 616
    Top = 104
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 304
  end
  object DataSource2: TDataSource
    Left = 632
    Top = 312
  end
  object DataSource3: TDataSource
    Left = 640
    Top = 320
  end
  object DataSource4: TDataSource
    DataSet = qryTerceiro
    Left = 648
    Top = 328
  end
  object DataSource5: TDataSource
    Left = 656
    Top = 336
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoEconomico'
      'WHERE nCdGrupoEconomico = :nPK')
    Left = 424
    Top = 288
    object qryGrupoEconomiconCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object dsGrupoEconomico: TDataSource
    DataSet = qryGrupoEconomico
    Left = 480
    Top = 280
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUnidadeNegocio, cNmUnidadeNegocio'
      'FROM UnidadeNegocio'
      'WHERE nCdUnidadeNegocio = :nPK')
    Left = 424
    Top = 208
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object DataSource6: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 664
    Top = 344
  end
end
