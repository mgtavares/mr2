inherited frmMetaVendedor: TfrmMetaVendedor
  Left = 126
  Top = 116
  Width = 1110
  Height = 570
  Caption = 'Distribui'#231#227'o de Meta - Vendedor'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 118
    Width = 1094
    Height = 416
  end
  inherited ToolBar1: TToolBar
    Width = 1094
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1094
    Height = 89
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 50
      Top = 20
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
    end
    object Label3: TLabel
      Tag = 1
      Left = 22
      Top = 68
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#234's/Ano Meta'
    end
    object Label2: TLabel
      Tag = 1
      Left = 70
      Top = 44
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
    end
    object edtMesAno: TMaskEdit
      Left = 96
      Top = 60
      Width = 65
      Height = 21
      EditMask = '99/9999'
      MaxLength = 7
      TabOrder = 2
      Text = '  /    '
      OnChange = edtMesAnoChange
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 168
      Top = 36
      Width = 385
      Height = 21
      DataField = 'cNmLoja'
      DataSource = dsLoja
      TabOrder = 5
    end
    object cxButton1: TcxButton
      Left = 168
      Top = 59
      Width = 105
      Height = 25
      Caption = 'Exibir Meta'
      TabOrder = 3
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000C6D6DEC6948C
        C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
        8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
        D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
        00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
        FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
        948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
        FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
        C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
        FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
        948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
        8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
        D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
      LookAndFeel.NativeStyle = True
    end
    object cxButton2: TcxButton
      Left = 280
      Top = 59
      Width = 105
      Height = 25
      Caption = 'Nova Meta'
      TabOrder = 4
      OnClick = cxButton2Click
      Glyph.Data = {
        2E020000424D2E0200000000000036000000280000000C0000000E0000000100
        180000000000F801000000000000000000000000000000000000EEFEFEEEFEFE
        EEFEFEEEFEFE9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F0000
        000000000000000000000000000000000000000000000000000000000000009C
        8C6F000000FFFFFFC0928FC0928FC0928FC0928FC0928FC0928FC0928FC0928F
        0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9F9EED9F9EED9F9EE
        D9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9F9EED9F9
        EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9
        F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EE
        D9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9
        EED9F9EED9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFF
        F9EED9F9EED9F9EED9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F0000
        00FFFFFFF9EED9F9EED9F9EED9F9EED90000000000000000000000000000009C
        8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED90000009C8C6F9C8C6F9C8C6F
        000000EEFEFE000000FFFFFFF9EED9F9EED9F9EED9F9EED90000009C8C6F9C8C
        6F000000EEFEFEEEFEFE000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000009C
        8C6F000000EEFEFEEEFEFEEEFEFE000000000000000000000000000000000000
        000000000000EEFEFEEEFEFEEEFEFEEEFEFE}
      LookAndFeel.NativeStyle = True
    end
    object edtEmpresa: TMaskEdit
      Left = 96
      Top = 12
      Width = 65
      Height = 21
      EditMask = '########;1; '
      MaxLength = 8
      TabOrder = 0
      Text = '        '
      OnChange = edtEmpresaChange
      OnExit = edtEmpresaExit
      OnKeyDown = edtEmpresaKeyDown
    end
    object edtLoja: TMaskEdit
      Left = 96
      Top = 36
      Width = 65
      Height = 21
      EditMask = '########;1; '
      MaxLength = 8
      TabOrder = 1
      Text = '        '
      OnChange = edtLojaChange
      OnExit = edtLojaExit
      OnKeyDown = edtLojaKeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 168
      Top = 12
      Width = 385
      Height = 21
      DataField = 'cNmEmpresa'
      DataSource = dsEmpresa
      TabOrder = 6
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 118
    Width = 1094
    Height = 416
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 412
    ClientRectLeft = 4
    ClientRectRight = 1090
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Meta Semanal'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1086
        Height = 388
        Align = alClient
        AllowedOperations = []
        DataGrouping.GroupLevels = <>
        DataSource = dsMetaSemana
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh1DblClick
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdMetaSemana'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdMetaMes'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iSemana'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nValMetaMinima'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nValMeta'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nValVendaMediaPrev'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nItensPorVendaPrev'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nValItemMedioPrev'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nValVendaHoraPrev'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nTxConversaoPrev'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'iTotalHorasPrev'
            Footers = <>
            ReadOnly = True
            Width = 86
          end
          item
            EditButtons = <>
            FieldName = 'nPercDistrib'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'iTotalHorasReal'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nPercentReal'
            Footers = <>
            ReadOnly = True
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 7
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa  '
      '  FROM Empresa'
      ' WHERE nCdEmpresa =:nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE '
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa '
      '                 AND UE.nCdUsuario = :nCdUsuario)')
    Left = 360
    Top = 305
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 376
    Top = 336
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '8'
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS (SELECT 1'
      '                 FROM UsuarioLoja UL'
      '                WHERE UL.nCdUsuario = :nCdUsuario'
      '                  AND UL.nCdLoja    = Loja.nCdLoja) ')
    Left = 404
    Top = 305
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 420
    Top = 334
  end
  object qryMetaSemana: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'iAno'
        DataType = ftString
        Size = 4
        Value = '2010'
      end
      item
        Name = 'iMes'
        DataType = ftString
        Size = 2
        Value = '12'
      end>
    SQL.Strings = (
      'DECLARE @nPercDistrib       decimal(12,2)'
      '       ,@nPercentReal       decimal(12,2)'
      '       ,@iHoraVendaVendedor decimal(12,2)'
      '       ,@iHoraVendaSemana   decimal(12,2)'
      '       ,@iTotalHorasReal    decimal(12,2)'
      '       ,@nCdMetaSemana      int'
      '       ,@iMes               int'
      '       ,@iAno               int'
      '       ,@nCdLoja            int'
      '       ,@nCdEmpresa         int'
      '       ,@nCdMetaMes         int'
      ''
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempPercDistrib'#39') IS NULL)'
      'BEGIN'
      
        '    CREATE TABLE #TempPercDistrib (nCdMetaSemana         int not' +
        ' null'
      
        '                                  ,nPercDistrib          decimal' +
        '(12,2) default 0 not null'
      
        '                                  ,iTotalHorasReal       decimal' +
        '(12,2) default 0 not null'
      
        '                                  ,nPercentReal          decimal' +
        '(12,2) default 0 not null)'
      'END'
      ''
      'TRUNCATE TABLE #TempPercDistrib'
      ''
      'SET @nCdEmpresa    = :nCdEmpresa'
      'SET @nCdLoja       = :nCdLoja'
      'SET @iAno          = :iAno'
      'SET @iMes          = :iMes'
      ''
      'SELECT @nCdMetaMes = nCdMetaMes  '
      '  FROM MetaMes '
      ' WHERE nCdEmpresa = @nCdEmpresa'
      '   AND nCdLoja    = @nCdLoja'
      '   AND iAno       = @iAno'
      '   AND iMes       = @iMes'
      ''
      'DECLARE curAux CURSOR'
      '        FOR SELECT nCdMetaSemana'
      '              FROM MetaSemana'
      '             WHERE nCdMetaMes = @nCdMetaMes'
      ''
      '    OPEN curAux'
      ''
      '    FETCH NEXT'
      '     FROM curAux'
      '     INTO @nCdMetaSemana'
      ''
      '    WHILE (@@FETCH_STATUS = 0)'
      '    BEGIN'
      ''
      '        SET @iHoraVendaSemana   = isNull((SELECT iTotalHorasPrev'
      '                                            FROM MetaSemana'
      
        '                                           WHERE nCdMetaSemana =' +
        ' @nCdMetaSemana),0)'
      ''
      
        '        SET @iHoraVendaVendedor = isNull((SELECT SUM(iTotalHoras' +
        'Prev)'
      '                                            FROM MetaVendedor'
      
        '                                           WHERE nCdMetaSemana =' +
        ' @nCdMetaSemana),0)'
      ''
      '        --'
      
        '        -- Calcula aqui os percentuais de Horas Realizadas e hor' +
        'as distribuidas'
      '        --'
      ''
      '        SELECT @nPercDistrib = (CASE WHEN @iHoraVendaSemana > 0'
      
        '                                     THEN (SELECT (@iHoraVendaVe' +
        'ndedor/@iHoraVendaSemana) * 100)'
      '                                     ELSE 0'
      '                                END)'
      ''
      '        SELECT @nPercentReal = (CASE WHEN @iHoraVendaSemana > 0'
      
        '                                     THEN (SELECT (@iHoraVendaVe' +
        'ndedor/@iHoraVendaSemana) * 100)'
      '                                     ELSE 0'
      '                                END)'
      ''
      '        INSERT INTO #TempPercDistrib (nCdMetaSemana'
      '                                     ,nPercDistrib'
      '                                     ,iTotalHorasReal)'
      '                               SELECT @nCdMetaSemana'
      '                                     ,@nPercDistrib'
      
        '                                     ,isNull(SUM(MetaVendedorDia' +
        '.iTotalHorasReal),0)'
      '                                 FROM MetaVendedorDia '
      
        '                                      INNER JOIN MetaVendedor ON' +
        ' MetaVendedor.nCdMetaVendedor = MetaVendedorDia.nCdMetaVendedor'
      
        '                                WHERE MetaVendedor.nCdMetaSemana' +
        ' = @nCdMetaSemana'
      ''
      '        UPDATE #TempPercDistrib'
      
        '           SET nPercentReal = (SELECT CASE WHEN MetaSemana.iTota' +
        'lHorasPrev > 0'
      
        '                                           THEN (iTotalHorasReal' +
        '/iTotalHorasPrev) * 100'
      '                                           ELSE 0'
      '                                      END'
      '                                 FROM MetaSemana'
      
        '                                WHERE nCdMetaSemana = @nCdMetaSe' +
        'mana)'
      '         WHERE nCdMetaSemana = @nCdMetaSemana'
      ''
      ''
      '        FETCH NEXT'
      '         FROM curAux'
      '         INTO @nCdMetaSemana'
      ''
      'END'
      ''
      'CLOSE curAux'
      'DEALLOCATE curAux'
      ''
      'SELECT MetaSemana.nCdMetaSemana'
      '      ,MetaSemana.nCdMetaMes'
      '      ,MetaSemana.iSemana'
      '      ,MetaSemana.nValMetaMinima'
      '      ,MetaSemana.nValMeta'
      '      ,MetaSemana.nValVendaMediaPrev'
      '      ,MetaSemana.nItensPorVendaPrev'
      '      ,MetaSemana.nValItemMedioPrev'
      '      ,MetaSemana.nValVendaHoraPrev'
      '      ,MetaSemana.nTxConversaoPrev'
      '      ,MetaSemana.iTotalHorasPrev'
      '      ,TPD.nPercDistrib'
      '      ,TPD.iTotalHorasReal'
      '      ,TPD.nPercentReal'
      '  FROM MetaSemana'
      
        '       INNER JOIN #TempPercDistrib TPD ON TPD.nCdMetaSemana = Me' +
        'taSemana.nCdMetaSemana'
      ' WHERE nCdMetaMes = @nCdMetaMes')
    Left = 444
    Top = 305
    object qryMetaSemananCdMetaSemana: TIntegerField
      FieldName = 'nCdMetaSemana'
    end
    object qryMetaSemananCdMetaMes: TIntegerField
      FieldName = 'nCdMetaMes'
    end
    object qryMetaSemanaiSemana: TIntegerField
      DisplayLabel = 'Semana'
      FieldName = 'iSemana'
    end
    object qryMetaSemananValMetaMinima: TBCDField
      DisplayLabel = 'Meta em Valor|Meta Minima'
      FieldName = 'nValMetaMinima'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaSemananValMeta: TBCDField
      DisplayLabel = 'Meta em Valor|Meta'
      FieldName = 'nValMeta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaSemananValVendaMediaPrev: TBCDField
      DisplayLabel = 'Indicadores de Meta|Venda M'#233'dia'
      FieldName = 'nValVendaMediaPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaSemananItensPorVendaPrev: TBCDField
      DisplayLabel = 'Indicadores de Meta|Itens por Venda'
      FieldName = 'nItensPorVendaPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaSemananValItemMedioPrev: TBCDField
      DisplayLabel = 'Indicadores de Meta|Valor Item M'#233'dio'
      FieldName = 'nValItemMedioPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaSemananValVendaHoraPrev: TBCDField
      DisplayLabel = 'Indicadores de Meta|Venda p/ Hora'
      FieldName = 'nValVendaHoraPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaSemananTxConversaoPrev: TBCDField
      DisplayLabel = 'Indicadores de Meta|Taxa Convers'#227'o'
      FieldName = 'nTxConversaoPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaSemananPercDistrib: TBCDField
      DisplayLabel = 'Indicadores de Meta|% Distrib. '
      FieldName = 'nPercDistrib'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaSemanaiTotalHorasReal: TBCDField
      DisplayLabel = 'Horas Realizadas|Horas Venda'
      FieldName = 'iTotalHorasReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaSemananPercentReal: TBCDField
      DisplayLabel = 'Horas Realizadas|% Realizada'
      FieldName = 'nPercentReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaSemanaiTotalHorasPrev: TBCDField
      DisplayLabel = 'Indicadores de Meta|Horas de Venda'
      FieldName = 'iTotalHorasPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsMetaSemana: TDataSource
    DataSet = qryMetaSemana
    Left = 464
    Top = 336
  end
end
