inherited frmPedidoComercial_ProgEntrega: TfrmPedidoComercial_ProgEntrega
  Left = 13
  Top = 104
  Width = 988
  Caption = 'Programa'#231#227'o de Entrega'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 972
  end
  inherited ToolBar1: TToolBar
    Width = 972
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 972
    Height = 435
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsItemPedido
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdItemPedido'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'cNmitem'
        Footers = <>
        ReadOnly = True
        Width = 397
      end
      item
        EditButtons = <>
        FieldName = 'nQtdePed'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'nQtdePrev'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'dDtEntregaIni'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtEntregaFim'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 368
    Top = 272
  end
  object qryItemPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdItemPedido'
      '      ,nCdPedido'
      '      ,nCdProduto'
      '      ,cNmitem'
      '      ,nQtdePed'
      '      ,nQtdePrev'
      '      ,dDtEntregaIni'
      '      ,dDtEntregaFim'
      '  FROM ItemPedido'
      ' WHERE nCdPedido = :nPK'
      '   AND nCdItemPedidoPai IS NULL'
      ' ORDER BY cNmItem')
    Left = 296
    Top = 272
    object qryItemPedidonCdItemPedido: TAutoIncField
      DisplayLabel = 'Item|ID'
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryItemPedidonCdPedido: TIntegerField
      DisplayLabel = 'Item|'
      FieldName = 'nCdPedido'
    end
    object qryItemPedidonCdProduto: TIntegerField
      DisplayLabel = 'Item|C'#243'digo'
      FieldName = 'nCdProduto'
    end
    object qryItemPedidocNmitem: TStringField
      DisplayLabel = 'Item|Descri'#231#227'o Item'
      FieldName = 'cNmitem'
      Size = 150
    end
    object qryItemPedidonQtdePed: TBCDField
      DisplayLabel = 'Quantidades|Pedida'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemPedidonQtdePrev: TBCDField
      DisplayLabel = 'Quantidades|Prevista'
      FieldName = 'nQtdePrev'
      Precision = 12
    end
    object qryItemPedidodDtEntregaIni: TDateTimeField
      DisplayLabel = 'Previs'#227'o de Entrega|Data Inicial'
      FieldName = 'dDtEntregaIni'
    end
    object qryItemPedidodDtEntregaFim: TDateTimeField
      DisplayLabel = 'Previs'#227'o de Entrega|Data Final'
      FieldName = 'dDtEntregaFim'
    end
  end
  object dsItemPedido: TDataSource
    DataSet = qryItemPedido
    Left = 336
    Top = 272
  end
end
