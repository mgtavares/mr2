inherited frmTipoOrdemProducao: TfrmTipoOrdemProducao
  Left = 66
  Top = 155
  Width = 1009
  Height = 602
  Caption = 'Tipo Ordem Produ'#231#227'o'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 993
    Height = 541
  end
  object Label1: TLabel [1]
    Left = 76
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 65
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 14
    Top = 86
    Width = 100
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo OP Retrabalho'
    FocusControl = DBEdit2
  end
  object Label10: TLabel [4]
    Left = 33
    Top = 110
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Requisi'#231#227'o'
    FocusControl = DBEdit2
  end
  inherited ToolBar2: TToolBar
    Width = 993
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 120
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTipoOP'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [7]
    Left = 120
    Top = 56
    Width = 529
    Height = 19
    DataField = 'cNmTipoOP'
    DataSource = dsMaster
    TabOrder = 2
  end
  object edtOpRetrabalho: TER2LookupDBEdit [8]
    Left = 120
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdTipoOPRetrabalho'
    DataSource = dsMaster
    TabOrder = 3
    OnBeforeLookup = edtOpRetrabalhoBeforeLookup
    CodigoLookup = 739
    QueryLookup = qryTipoOPRet
  end
  object DBEdit3: TDBEdit [9]
    Tag = 1
    Left = 192
    Top = 80
    Width = 457
    Height = 19
    DataField = 'cNmTipoOP'
    DataSource = dsTipoOPRet
    TabOrder = 6
  end
  object GroupBox1: TGroupBox [10]
    Left = 16
    Top = 152
    Width = 633
    Height = 65
    Caption = 'Encerramento Produ'#231#227'o'
    TabOrder = 7
    object Label4: TLabel
      Left = 39
      Top = 20
      Width = 107
      Height = 13
      Alignment = taRightJustify
      Caption = 'Opera'#231#227'o Conclus'#227'o'
      FocusControl = DBEdit2
    end
    object Label5: TLabel
      Left = 17
      Top = 44
      Width = 129
      Height = 13
      Alignment = taRightJustify
      Caption = 'Opera'#231#227'o Baixa Explos'#227'o'
      FocusControl = DBEdit2
    end
    object edtEstoqueCP: TER2LookupDBEdit
      Left = 152
      Top = 14
      Width = 65
      Height = 19
      DataField = 'nCdOperacaoEstoqueCP'
      DataSource = dsMaster
      TabOrder = 0
      OnBeforeLookup = edtEstoqueCPBeforeLookup
      CodigoLookup = 84
      WhereAdicional.Strings = (
        'cSinalOper = '#39'+'#39)
      QueryLookup = qryOpEstoqueCP
    end
    object edtEstoqueRE: TER2LookupDBEdit
      Left = 152
      Top = 38
      Width = 65
      Height = 19
      DataField = 'nCdOperEstoqueBaixaExplosao'
      DataSource = dsMaster
      TabOrder = 1
      OnBeforeLookup = edtEstoqueREBeforeLookup
      CodigoLookup = 84
      WhereAdicional.Strings = (
        'cSinalOper = '#39'-'#39)
      QueryLookup = qryOpEstoqueSLP
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 224
      Top = 14
      Width = 401
      Height = 19
      DataField = 'cNmOperacaoEstoque'
      DataSource = dsOPEstoqueCP
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 224
      Top = 38
      Width = 401
      Height = 19
      DataField = 'cNmOperacaoEstoque'
      DataSource = dsOPEstoqueRE
      TabOrder = 3
    end
  end
  object GroupBox2: TGroupBox [11]
    Left = 16
    Top = 224
    Width = 633
    Height = 65
    Caption = 'Transfer'#234'ncia Estoque p/ Processo'
    TabOrder = 8
    object Label6: TLabel
      Left = 20
      Top = 20
      Width = 126
      Height = 13
      Alignment = taRightJustify
      Caption = 'Opera'#231#227'o Sa'#237'da Estoque'
      FocusControl = DBEdit2
    end
    object Label7: TLabel
      Left = 8
      Top = 44
      Width = 138
      Height = 13
      Alignment = taRightJustify
      Caption = 'Opera'#231#227'o Entrada Estoque'
      FocusControl = DBEdit2
    end
    object edtTransfEntrada: TER2LookupDBEdit
      Left = 152
      Top = 38
      Width = 65
      Height = 19
      DataField = 'nCdOperTransfEstoqueEntrada'
      DataSource = dsMaster
      TabOrder = 1
      OnBeforeLookup = edtTransfEntradaBeforeLookup
      CodigoLookup = 84
      WhereAdicional.Strings = (
        'cSinalOper = '#39'+'#39)
      QueryLookup = qryOpEstoqueELP
    end
    object edtTransfSaida: TER2LookupDBEdit
      Left = 152
      Top = 14
      Width = 65
      Height = 19
      DataField = 'nCdOperTransfEstoqueSaida'
      DataSource = dsMaster
      TabOrder = 0
      OnBeforeLookup = edtTransfSaidaBeforeLookup
      CodigoLookup = 84
      WhereAdicional.Strings = (
        'cSinalOper = '#39'-'#39)
      QueryLookup = qryOpEstoqueBLP
    end
    object DBEdit6: TDBEdit
      Tag = 1
      Left = 224
      Top = 38
      Width = 401
      Height = 19
      DataField = 'cNmOperacaoEstoque'
      DataSource = dsOpEstoqueELP
      TabOrder = 2
    end
    object DBEdit7: TDBEdit
      Tag = 1
      Left = 224
      Top = 14
      Width = 401
      Height = 19
      DataField = 'cNmOperacaoEstoque'
      DataSource = dsOpEstoqueBLP
      TabOrder = 3
    end
  end
  object GroupBox3: TGroupBox [12]
    Left = 16
    Top = 296
    Width = 633
    Height = 65
    Caption = 'Devolu'#231#227'o Processo p/ Estoque'
    TabOrder = 9
    object Label8: TLabel
      Left = 17
      Top = 20
      Width = 129
      Height = 13
      Alignment = taRightJustify
      Caption = 'Opera'#231#227'o Sa'#237'da Processo'
      FocusControl = DBEdit2
    end
    object Label9: TLabel
      Left = 8
      Top = 44
      Width = 138
      Height = 13
      Alignment = taRightJustify
      Caption = 'Opera'#231#227'o Entrada Estoque'
      FocusControl = DBEdit2
    end
    object edtDevEntrada: TER2LookupDBEdit
      Left = 152
      Top = 38
      Width = 65
      Height = 19
      DataField = 'nCdOperDevEstoqueEntrada'
      DataSource = dsMaster
      TabOrder = 1
      OnBeforeLookup = edtDevEntradaBeforeLookup
      CodigoLookup = 84
      WhereAdicional.Strings = (
        'cSinalOper = '#39'+'#39)
      QueryLookup = qryOpEstoqueDPA
    end
    object edtDevSaida: TER2LookupDBEdit
      Left = 152
      Top = 14
      Width = 65
      Height = 19
      DataField = 'nCdOperDevEstoqueSaida'
      DataSource = dsMaster
      TabOrder = 0
      OnBeforeLookup = edtDevSaidaBeforeLookup
      CodigoLookup = 84
      WhereAdicional.Strings = (
        'cSinalOper = '#39'-'#39)
      QueryLookup = qryOpEstoqueDLP
    end
    object DBEdit8: TDBEdit
      Tag = 1
      Left = 224
      Top = 38
      Width = 401
      Height = 19
      DataField = 'cNmOperacaoEstoque'
      DataSource = dsOpEstoqueDPA
      TabOrder = 2
    end
    object DBEdit9: TDBEdit
      Tag = 1
      Left = 224
      Top = 14
      Width = 401
      Height = 19
      DataField = 'cNmOperacaoEstoque'
      DataSource = dsOpEstoqueDLP
      TabOrder = 3
    end
  end
  object edtTipoRequisicao: TER2LookupDBEdit [13]
    Left = 120
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdTipoRequisicao'
    DataSource = dsMaster
    TabOrder = 4
    OnBeforeLookup = edtOpRetrabalhoBeforeLookup
    CodigoLookup = 121
    QueryLookup = qryTipoRequisicao
  end
  object DBEdit10: TDBEdit [14]
    Tag = 1
    Left = 192
    Top = 104
    Width = 457
    Height = 19
    DataField = 'cNmTipoRequisicao'
    DataSource = DataSource1
    TabOrder = 10
  end
  object DBCheckBox1: TDBCheckBox [15]
    Left = 120
    Top = 128
    Width = 185
    Height = 17
    Caption = 'Atualizar Pre'#231'o Custo Produto'
    DataField = 'cFlgAtualizaPrecoCusto'
    DataSource = dsMaster
    TabOrder = 5
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited qryMaster: TADOQuery
    BeforeClose = qryMasterBeforeClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM TipoOP'
      ' WHERE nCdTipoOP = :nPK')
    Left = 728
    Top = 64
    object qryMasternCdTipoOP: TIntegerField
      FieldName = 'nCdTipoOP'
    end
    object qryMastercNmTipoOP: TStringField
      FieldName = 'cNmTipoOP'
      Size = 50
    end
    object qryMasternCdTipoOPRetrabalho: TIntegerField
      FieldName = 'nCdTipoOPRetrabalho'
    end
    object qryMasternCdOperacaoEstoqueCP: TIntegerField
      FieldName = 'nCdOperacaoEstoqueCP'
    end
    object qryMasternCdOperEstoqueBaixaExplosao: TIntegerField
      FieldName = 'nCdOperEstoqueBaixaExplosao'
    end
    object qryMasternCdOperTransfEstoqueSaida: TIntegerField
      FieldName = 'nCdOperTransfEstoqueSaida'
    end
    object qryMasternCdOperTransfEstoqueEntrada: TIntegerField
      FieldName = 'nCdOperTransfEstoqueEntrada'
    end
    object qryMasternCdOperDevEstoqueSaida: TIntegerField
      FieldName = 'nCdOperDevEstoqueSaida'
    end
    object qryMasternCdOperDevEstoqueEntrada: TIntegerField
      FieldName = 'nCdOperDevEstoqueEntrada'
    end
    object qryMasternCdTipoRequisicao: TIntegerField
      FieldName = 'nCdTipoRequisicao'
    end
    object qryMastercFlgAtualizaPrecoCusto: TIntegerField
      FieldName = 'cFlgAtualizaPrecoCusto'
    end
  end
  inherited dsMaster: TDataSource
    Left = 776
    Top = 64
  end
  inherited qryID: TADOQuery
    Left = 824
    Top = 64
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 832
    Top = 120
  end
  inherited qryStat: TADOQuery
    Left = 736
    Top = 136
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 800
    Top = 176
  end
  inherited ImageList1: TImageList
    Left = 936
    Top = 160
  end
  object qryTipoOPRet: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoOP'
      '             ,cNmTipoOP'
      '  FROM TipoOP'
      ' WHERE nCdTipoOP = :nPK')
    Left = 664
    Top = 80
    object qryTipoOPRetnCdTipoOP: TIntegerField
      FieldName = 'nCdTipoOP'
    end
    object qryTipoOPRetcNmTipoOP: TStringField
      FieldName = 'cNmTipoOP'
      Size = 50
    end
  end
  object qryOpEstoqueCP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOperacaoEstoque'
      '      ,cNmOperacaoEstoque '
      '  FROM OperacaoEstoque'
      ' WHERE nCdOperacaoEstoque = :nPK'
      '   AND cSinalOper = '#39'+'#39)
    Left = 40
    Top = 376
    object qryOpEstoqueCPnCdOperacaoEstoque: TIntegerField
      FieldName = 'nCdOperacaoEstoque'
    end
    object qryOpEstoqueCPcNmOperacaoEstoque: TStringField
      FieldName = 'cNmOperacaoEstoque'
      Size = 50
    end
  end
  object qryOpEstoqueSLP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOperacaoEstoque'
      '      ,cNmOperacaoEstoque '
      '  FROM OperacaoEstoque'
      ' WHERE nCdOperacaoEstoque = :nPK'
      '  AND cSinalOper = '#39'-'#39)
    Left = 80
    Top = 376
    object qryOpEstoqueSLPnCdOperacaoEstoque: TIntegerField
      FieldName = 'nCdOperacaoEstoque'
    end
    object qryOpEstoqueSLPcNmOperacaoEstoque: TStringField
      FieldName = 'cNmOperacaoEstoque'
      Size = 50
    end
  end
  object dsTipoOPRet: TDataSource
    DataSet = qryTipoOPRet
    Left = 680
    Top = 112
  end
  object dsOPEstoqueCP: TDataSource
    DataSet = qryOpEstoqueCP
    Left = 56
    Top = 392
  end
  object dsOPEstoqueRE: TDataSource
    DataSet = qryOpEstoqueSLP
    Left = 96
    Top = 392
  end
  object qryOpEstoqueELP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOperacaoEstoque'
      '      ,cNmOperacaoEstoque '
      '  FROM OperacaoEstoque'
      ' WHERE nCdOperacaoEstoque = :nPK'
      '   AND cSinalOper = '#39'+'#39)
    Left = 120
    Top = 376
    object IntegerField1: TIntegerField
      FieldName = 'nCdOperacaoEstoque'
    end
    object StringField1: TStringField
      FieldName = 'cNmOperacaoEstoque'
      Size = 50
    end
  end
  object dsOpEstoqueELP: TDataSource
    DataSet = qryOpEstoqueELP
    Left = 136
    Top = 392
  end
  object qryOpEstoqueBLP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOperacaoEstoque'
      '      ,cNmOperacaoEstoque '
      '  FROM OperacaoEstoque'
      ' WHERE nCdOperacaoEstoque = :nPK'
      '  AND cSinalOper = '#39'-'#39)
    Left = 160
    Top = 376
    object IntegerField2: TIntegerField
      FieldName = 'nCdOperacaoEstoque'
    end
    object StringField2: TStringField
      FieldName = 'cNmOperacaoEstoque'
      Size = 50
    end
  end
  object dsOpEstoqueBLP: TDataSource
    DataSet = qryOpEstoqueBLP
    Left = 176
    Top = 392
  end
  object qryOpEstoqueDLP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOperacaoEstoque'
      '      ,cNmOperacaoEstoque '
      '  FROM OperacaoEstoque'
      ' WHERE nCdOperacaoEstoque = :nPK'
      '   AND cSinalOper = '#39'-'#39)
    Left = 200
    Top = 376
    object IntegerField3: TIntegerField
      FieldName = 'nCdOperacaoEstoque'
    end
    object StringField3: TStringField
      FieldName = 'cNmOperacaoEstoque'
      Size = 50
    end
  end
  object dsOpEstoqueDLP: TDataSource
    DataSet = qryOpEstoqueDLP
    Left = 216
    Top = 392
  end
  object qryOpEstoqueDPA: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOperacaoEstoque'
      '      ,cNmOperacaoEstoque '
      '  FROM OperacaoEstoque'
      ' WHERE nCdOperacaoEstoque = :nPK'
      '  AND cSinalOper = '#39'+'#39)
    Left = 240
    Top = 376
    object IntegerField4: TIntegerField
      FieldName = 'nCdOperacaoEstoque'
    end
    object StringField4: TStringField
      FieldName = 'cNmOperacaoEstoque'
      Size = 50
    end
  end
  object dsOpEstoqueDPA: TDataSource
    DataSet = qryOpEstoqueDPA
    Left = 256
    Top = 392
  end
  object qryTipoRequisicao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTipoRequisicao'
      'FROM TipoRequisicao'
      'WHERE nCdTipoRequisicao = :nPK')
    Left = 752
    Top = 192
    object qryTipoRequisicaocNmTipoRequisicao: TStringField
      FieldName = 'cNmTipoRequisicao'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTipoRequisicao
    Left = 488
    Top = 304
  end
end
