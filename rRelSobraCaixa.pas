unit rRelSobraCaixa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, DBCtrls, StdCtrls, Mask, ER2Excel;

type
  TrptRelSobraCaixa = class(TfrmRelatorio_Padrao)
    Image2: TImage;
    Image3: TImage;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    MaskEdit2: TMaskEdit;
    Label6: TLabel;
    MaskEdit1: TMaskEdit;
    Label3: TLabel;
    Label1: TLabel;
    Label5: TLabel;
    qryTotalContaBancaria: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    SPREL_RESUMO_MOVTO_CAIXA: TADOStoredProc;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    qrySaldoConta: TADOQuery;
    qrySaldoContanCdLoja: TStringField;
    qrySaldoContanCdConta: TStringField;
    qrySaldoContanSaldoConta: TBCDField;
    qryTotalContaBancariacNmFormaPagto: TStringField;
    qryTotalContaBancariaTotalValPagto: TBCDField;
    qryTotalContaBancariaiQtdVendas: TIntegerField;
    qryRecebParcFormaPagto: TADOQuery;
    qryRecebParcFormaPagtocNmFormaPagto: TStringField;
    qryRecebParcFormaPagtoTotalValPagto: TBCDField;
    qryRecebParcFormaPagtoiQtdeParcPagaTotal: TIntegerField;
    Edit1: TEdit;
    Edit2: TEdit;
    dsLoja: TDataSource;
    dsContaBancaria: TDataSource;
    dsSaldoConta: TDataSource;
    dsTotalContaBancaria: TDataSource;
    dsRecebParcFormaPagto: TDataSource;
    RadioGroup2: TRadioGroup;
    rgModeloImp: TRadioGroup;
    ER2Excel1: TER2Excel;
    procedure ToolButton1Click(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRelSobraCaixa: TrptRelSobraCaixa;
  iLinha : integer ;
  iPagina: integer ;

implementation

uses fMenu, fLookup_Padrao, Math, rRelSobraCaixa_view;

{$R *.dfm}

procedure TrptRelSobraCaixa.Edit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(147);

            If (nPK > 0) then
            begin
                Edit1.Text := IntToStr(nPK);
                PosicionaQuery(qryLoja, Edit1.Text);
            end ;

    end ;

  end ;
end;

procedure TrptRelSobraCaixa.Edit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        If (Trim(Edit1.Text)='') then
        begin
            MensagemAlerta('Para selecionar uma conta caixa, selecione primeiro uma loja');
            Abort;
        end
        else begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(126,'ContaBancaria.nCdLoja = ' + Edit1.Text);
        end;

        If (nPK > 0) then
        begin
            Edit2.Text := IntToStr(nPK) ;
            qryContaBancaria.Parameters.ParamByName('nCdLoja').Value := Edit1.Text;
            PosicionaQuery(qryContaBancaria, Edit2.Text) ;
        end ;

    end ;
    end;

end;

procedure TrptRelSobraCaixa.Edit1Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  qryContaBancaria.Close;
  PosicionaQuery(qryLoja,Edit1.Text);
end;

procedure TrptRelSobraCaixa.Edit2Exit(Sender: TObject);
begin
  inherited;
  if (Trim(Edit2.Text) = '') and (Trim(Edit1.Text)='') then
  begin
  end
  else if (Trim(Edit1.Text)='') then
  begin
      MensagemAlerta('Para selecionar uma conta caixa, selecione primeiro uma loja');
      Abort;
  end
  else begin
      qryContaBancaria.Close;
      qryContaBancaria.Parameters.ParamByName('nCdLoja').Value := Edit1.Text;
      PosicionaQuery(qryContaBancaria, Edit2.Text) ;
  end;
end;

procedure Verifica_Data(Componente: TMaskEdit);
begin
  try
    if Componente.Text <> '  /  /    ' Then
      StrToDate(Componente.Text);
  except
    frmMenu.MensagemAlerta('Data Inv�lida');
    Componente.Clear;
    Componente.SetFocus;
    Abort;
  end;
end;

procedure TrptRelSobraCaixa.ToolButton1Click(Sender: TObject);
var objRel : TrptRelSobraCaixa_view;
    dtInicial       : string;
    dtFinal         : string;
    nCdLoja         : integer;
    nCdContaBancaria: integer;
    cSituacao       : string;
    cSituacaoTexto  : string;
    cCdLojaAtual    : string;
    cCdCaixaAtual   : string;
    bPrimeiraVez    : boolean;
    bQuebrouLoja    : boolean;
    nLinhaInicial   : integer;
begin
    if (MaskEdit1.Text = '  /  /    ') OR (MaskEdit2.Text = '  /  /    ') Then
    Begin
        MensagemAlerta('Por favor informar a Data Inicial e Final do Relat�rio');
        Abort;
    End;

    Verifica_Data(MaskEdit1);
    Verifica_Data(MaskEdit2);

    if (frmMenu.ConvData(MaskEdit1.Text) > frmMenu.ConvData(MaskEdit2.Text)) then
    begin
        MensagemAlerta('Data inicial maior do que a data final, por favor verifique');
        Abort;
    end;

    inherited;

    objRel := TrptRelSobraCaixa_view.Create(nil);

    if (RadioGroup2.ItemIndex = 0) then
    Begin
        cSituacao := 'T';
        cSituacaoTexto := 'Todas';
    End;

    if (RadioGroup2.ItemIndex = 1) then
    Begin
        cSituacao := '-';
        cSituacaoTexto := 'Sobra Cx';
    End;

    if (RadioGroup2.ItemIndex = 2) then
    Begin
        cSituacao := '+';
        cSituacaoTexto := 'Falta Cx';
    End;

    nCdLoja := 0;
    if (Edit1.Text <> '') Then nCdLoja := frmMenu.ConvInteiro(Edit1.Text) ;

    nCdContaBancaria := 0;
    if (Edit2.Text <> '') Then nCdContaBancaria := frmMenu.ConvInteiro(Edit2.Text) ;

    bPrimeiraVez  := true;
    bQuebrouLoja  := false;

    Try
        Try
            objRel.usp_Relatorio.Close ;

            objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value          := frmMenu.nCdEmpresaAtiva ;
            objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value             := nCdLoja ;
            objRel.usp_Relatorio.Parameters.ParamByName('@cDtInicial').Value          := frmMenu.ConvData(MaskEdit1.Text) ;
            objRel.usp_Relatorio.Parameters.ParamByName('@cDtFinal').Value            := frmMenu.ConvData(MaskEdit2.Text) ;
            objRel.usp_Relatorio.Parameters.ParamByName('@nCdContaBancaria').Value    := nCdContaBancaria ;
            objRel.usp_Relatorio.Parameters.ParamByName('@cDiferencaAMostrar').Value  := cSituacao ;

            objRel.usp_Relatorio.Open;

            objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
            objRel.lblFiltro1.Caption := 'Loja: ' + IntToStr(nCdLoja) + ', Per�odo : ' + String(MaskEdit1.Text) + ' a ' + String(MaskEdit2.Text) + ', Caixa: ' + IntToStr(nCdContaBancaria) + ', Diferen�as: ' + cSituacaoTexto;

            case (rgModeloImp.ItemIndex) of
                0 : objRel.QuickRep1.PreviewModal;
                1 : begin
                        frmMenu.mensagemUsuario('Exportando Planilha...');

                        { -- formata c�lulas -- }
                        ER2Excel1.Celula['A1'].FontName := 'Calibri';
                        ER2Excel1.Celula['A2'].FontName := 'Calibri';
                        ER2Excel1.Celula['A1'].FontSize := 10;
                        ER2Excel1.Celula['A2'].FontSize := 10;

                        ER2Excel1.Celula['A1'].Background := RGB(221, 221, 221);
                        ER2Excel1.Celula['A2'].Background := RGB(221, 221, 221);
                        ER2Excel1.Celula['A3'].Background := RGB(221, 221, 221);
                        ER2Excel1.Celula['A4'].Background := RGB(221, 221, 221);

                        ER2Excel1.Celula['A1'].Mesclar('J1');
                        ER2Excel1.Celula['A2'].Mesclar('J2');
                        ER2Excel1.Celula['A3'].Mesclar('J3');
                        ER2Excel1.Celula['A4'].Mesclar('J4');

                        { -- inseri informa��es do cabe�alho -- }
                        ER2Excel1.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                        ER2Excel1.Celula['A2'].Text := 'Diferen�as de Caixa';
                        ER2Excel1.Celula['A4'].Text := 'Filtros: ' + 'Loja: ' + IntToStr(nCdLoja) + ', Per�odo : ' + String(MaskEdit1.Text) + ' a ' + String(MaskEdit2.Text) + ', Caixa: ' + IntToStr(nCdContaBancaria) + ', Diferen�as: ' + cSituacaoTexto;

                        iLinha := 9;

                        objRel.usp_Relatorio.First;

                        cCdLojaAtual  := objRel.usp_RelatorionCdLoja.AsString;
                        cCdCaixaAtual := objRel.usp_RelatorionCdConta.AsString;

                        ER2Excel1.Celula['A6'].Text := 'Loja';
                        ER2Excel1.Celula['A6'].HorizontalAlign := haRight;
                        ER2Excel1.Celula['A6'].Negrito;
                        ER2Excel1.Celula['B6'].Text := objRel.usp_RelatorionCdLoja.AsString + ' ' + objRel.usp_RelatoriocNmLoja.AsString;

                        ER2Excel1.Celula['B8'].Text := 'Caixa';
                        ER2Excel1.Celula['B8'].HorizontalAlign := haRight;                        
                        ER2Excel1.Celula['B8'].Negrito;
                        ER2Excel1.Celula['C8'].Text := objRel.usp_RelatorionCdContaBancaria.AsString + ' ' + objRel.usp_RelatorionCdConta.AsString;

                        ER2Excel1.Celula['A'  + IntToStr(iLinha)].Text := 'Abertura';
                        ER2Excel1.Celula['B'  + IntToStr(iLinha)].Text := 'Fechamento';
                        ER2Excel1.Celula['C'  + IntToStr(iLinha)].Text := 'Sdo Abertura';
                        ER2Excel1.Celula['D'  + IntToStr(iLinha)].Text := 'Cr�dito';
                        ER2Excel1.Celula['E'  + IntToStr(iLinha)].Text := 'D�bito';
                        ER2Excel1.Celula['F'  + IntToStr(iLinha)].Text := 'Sdo Fecham.';
                        ER2Excel1.Celula['G'  + IntToStr(iLinha)].Text := 'Valor Inform.';
                        ER2Excel1.Celula['H'  + IntToStr(iLinha)].Text := 'Valor Calcul.';
                        ER2Excel1.Celula['I'  + IntToStr(iLinha)].Text := 'Diferen�a';
                        ER2Excel1.Celula['J'  + IntToStr(iLinha)].Text := 'Usu�rio Fecham.';

                        ER2Excel1.Celula['C' + IntToStr(iLinha)].HorizontalAlign := haRight;
                        ER2Excel1.Celula['D' + IntToStr(iLinha)].HorizontalAlign := haRight;
                        ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight;
                        ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight;
                        ER2Excel1.Celula['G' + IntToStr(iLinha)].HorizontalAlign := haRight;
                        ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haRight;
                        ER2Excel1.Celula['I' + IntToStr(iLinha)].HorizontalAlign := haRight;

                        Inc(iLinha);

                        nLinhaInicial := 9;

                        while (not objRel.usp_Relatorio.Eof) do
                        begin

                            if (cCdLojaAtual <> objRel.usp_RelatorionCdLoja.AsString) Then
                            Begin
                                Inc(iLinha);

                                //quebrou Loja, soltar subtotal do �ltimo...
                                ER2Excel1.Celula['H'  + IntToStr(iLinha)].Text := 'SubTotal';
                                ER2Excel1.Celula['H'  + IntToStr(iLinha)].HorizontalAlign := haRight;
                                ER2Excel1.Celula['H'  + IntToStr(iLinha)].Negrito;
                                ER2Excel1.Celula['I'  + IntToStr(iLinha)].AutoSoma('I' + IntToStr(nLinhaInicial+1),'I' + IntToStr(iLinha-1));
                                ER2Excel1.Celula['I' + IntToStr(iLinha)].HorizontalAlign := haRight;
                                ER2Excel1.Celula['I' + IntToStr(iLinha)].Mascara := '#.##0,00';
                                Inc(iLinha);
                                nLinhaInicial := iLinha + 3;

                                bQuebrouLoja  := true;

                                ER2Excel1.Celula['A' + IntToStr(iLinha)].Text := 'Loja';
                                ER2Excel1.Celula['A' + IntToStr(iLinha)].HorizontalAlign := haRight;
                                ER2Excel1.Celula['A' + IntToStr(iLinha)].Negrito;
                                ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := objRel.usp_RelatorionCdLoja.AsString + ' ' + objRel.usp_RelatoriocNmLoja.AsString;
                                Inc(iLinha);
                                cCdLojaAtual := objRel.usp_RelatorionCdLoja.AsString;
                            End;

                            if (cCdCaixaAtual <> objRel.usp_RelatorionCdConta.AsString) Then
                            Begin
                                Inc(iLinha);

                                if (not bPrimeiraVez) then
                                Begin
                                    if (not bQuebrouLoja) then
                                    Begin
                                        ER2Excel1.Celula['H'  + IntToStr(iLinha)].Text := 'SubTotal';
                                        ER2Excel1.Celula['H'  + IntToStr(iLinha)].HorizontalAlign := haRight;
                                        ER2Excel1.Celula['H'  + IntToStr(iLinha)].Negrito;
                                        ER2Excel1.Celula['I'  + IntToStr(iLinha)].AutoSoma('I' + IntToStr(nLinhaInicial+1),'I' + IntToStr(iLinha-1));
                                        ER2Excel1.Celula['I' + IntToStr(iLinha)].HorizontalAlign := haRight;
                                        ER2Excel1.Celula['I' + IntToStr(iLinha)].Mascara := '#.##0,00';
                                        Inc(iLinha);
                                        nLinhaInicial := iLinha + 1;
                                    End;
                                End;

                                bQuebrouLoja := false;
                                ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := 'Caixa';
                                ER2Excel1.Celula['B' + IntToStr(iLinha)].HorizontalAlign := haRight;
                                ER2Excel1.Celula['B' + IntToStr(iLinha)].Negrito;
                                ER2Excel1.Celula['C' + IntToStr(iLinha)].Text := objRel.usp_RelatorionCdContaBancaria.AsString + ' ' + objRel.usp_RelatorionCdConta.AsString;

                                Inc(iLinha);
                                cCdCaixaAtual := objRel.usp_RelatorionCdConta.AsString;

                                ER2Excel1.Celula['A'  + IntToStr(iLinha)].Text := 'Abertura';
                                ER2Excel1.Celula['B'  + IntToStr(iLinha)].Text := 'Fechamento';
                                ER2Excel1.Celula['C'  + IntToStr(iLinha)].Text := 'Sdo Abertura';
                                ER2Excel1.Celula['D'  + IntToStr(iLinha)].Text := 'Cr�dito';
                                ER2Excel1.Celula['E'  + IntToStr(iLinha)].Text := 'D�bito';
                                ER2Excel1.Celula['F'  + IntToStr(iLinha)].Text := 'Sdo Fecham.';
                                ER2Excel1.Celula['G'  + IntToStr(iLinha)].Text := 'Valor Inform.';
                                ER2Excel1.Celula['H'  + IntToStr(iLinha)].Text := 'Valor Calcul.';
                                ER2Excel1.Celula['I'  + IntToStr(iLinha)].Text := 'Diferen�a';
                                ER2Excel1.Celula['J'  + IntToStr(iLinha)].Text := 'Usu�rio Fecham.';

                                ER2Excel1.Celula['C' + IntToStr(iLinha)].HorizontalAlign := haRight;
                                ER2Excel1.Celula['D' + IntToStr(iLinha)].HorizontalAlign := haRight;
                                ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight;
                                ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight;
                                ER2Excel1.Celula['G' + IntToStr(iLinha)].HorizontalAlign := haRight;
                                ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haRight;
                                ER2Excel1.Celula['I' + IntToStr(iLinha)].HorizontalAlign := haRight;


                                Inc(iLinha);
                            End;

                            bPrimeiraVez := false;

                            ER2Excel1.Celula['A' + IntToStr(iLinha)].Text := objRel.usp_RelatoriodDtAbertura.Value;
                            ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := objRel.usp_RelatoriodDtFechamento.Value;
                            ER2Excel1.Celula['C' + IntToStr(iLinha)].Text := objRel.usp_RelatorionSaldoAbertura.Value;
                            ER2Excel1.Celula['D' + IntToStr(iLinha)].Text := objRel.usp_RelatorionValCredito.Value;
                            ER2Excel1.Celula['E' + IntToStr(iLinha)].Text := objRel.usp_RelatorionValDebito.Value;
                            ER2Excel1.Celula['F' + IntToStr(iLinha)].Text := objRel.usp_RelatorionSaldoFechamento.Value;
                            ER2Excel1.Celula['G' + IntToStr(iLinha)].Text := objRel.usp_RelatorionValInformado.Value;
                            ER2Excel1.Celula['H' + IntToStr(iLinha)].Text := objRel.usp_RelatorionValCalculado.Value;
                            ER2Excel1.Celula['I' + IntToStr(iLinha)].Text := objRel.usp_RelatorionValDiferenca.Value;
                            ER2Excel1.Celula['J' + IntToStr(iLinha)].Text := objRel.usp_RelatoriocNmUsuario.Value;

                            ER2Excel1.Celula['C' + IntToStr(iLinha)].HorizontalAlign := haRight;
                            ER2Excel1.Celula['D' + IntToStr(iLinha)].HorizontalAlign := haRight;
                            ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight;
                            ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight;
                            ER2Excel1.Celula['G' + IntToStr(iLinha)].HorizontalAlign := haRight;
                            ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haRight;
                            ER2Excel1.Celula['I' + IntToStr(iLinha)].HorizontalAlign := haRight;

                            ER2Excel1.Celula['C' + IntToStr(iLinha)].Mascara := '#.##0,00';
                            ER2Excel1.Celula['D' + IntToStr(iLinha)].Mascara := '#.##0,00';
                            ER2Excel1.Celula['E' + IntToStr(iLinha)].Mascara := '#.##0,00';
                            ER2Excel1.Celula['F' + IntToStr(iLinha)].Mascara := '#.##0,00';
                            ER2Excel1.Celula['G' + IntToStr(iLinha)].Mascara := '#.##0,00';
                            ER2Excel1.Celula['H' + IntToStr(iLinha)].Mascara := '#.##0,00';
                            ER2Excel1.Celula['I' + IntToStr(iLinha)].Mascara := '#.##0,00';

                            Inc(iLinha);

                            objRel.usp_Relatorio.Next;
                        end;

                        Inc(iLinha);

                        //soltar subtotal do �ltimo...
                        ER2Excel1.Celula['H'  + IntToStr(iLinha)].Text := 'SubTotal';
                        ER2Excel1.Celula['H'  + IntToStr(iLinha)].HorizontalAlign := haRight;
                        ER2Excel1.Celula['H'  + IntToStr(iLinha)].Negrito;
                        ER2Excel1.Celula['I'  + IntToStr(iLinha)].AutoSoma('I' + IntToStr(nLinhaInicial+1),'I' + IntToStr(iLinha-1));
                        ER2Excel1.Celula['I' + IntToStr(iLinha)].HorizontalAlign := haRight;
                        ER2Excel1.Celula['I' + IntToStr(iLinha)].Mascara := '#.##0,00';


                        ER2Excel1.Celula['A1'].Width := 14;
                        ER2Excel1.Celula['B1'].Width := 14;
                        ER2Excel1.Celula['J1'].Width := 30;

                        { -- exporta planilha e limpa result do componente -- }
                        ER2Excel1.ExportXLS;
                        ER2Excel1.CleanupInstance;

                        frmMenu.mensagemUsuario('');
                    end;
            end;
        except
            MensagemErro('Erro ao criar o relat�rio');
            raise;
        end;
    Finally;
      FreeAndNil(objRel);
    end;
end;

initialization
    RegisterClass(TrptRelSobraCaixa) ;

end.
