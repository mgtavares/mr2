unit fEditorHTML;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, cxPC, cxControls, OleCtrls, SHDocVw,Activex, MsHtml, Buttons,  ShellAPI,
  Menus, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP;
const
  IDM_MARCADOR = 2184;
  IDM_MARCADOR_LISTA = 2185;
  IDM_OUTDENT = 2187;
  IDM_INDENT = 2186;
  IDM_ALINHARESQ = 59;
  IDM_CENTRALIZAR = 57;
  IDM_ALINHADIR = 60;
  IDM_IMAGEM = 2168;
  IDM_LINHAHORIZ = 2150;
  IDM_RECORTAR = 16;
  IDM_COPIAR = 15;
  IDM_COLAR = 26;
  IDM_HYPERLINK = 2124;
  IDM_DESFAZER = 43;

type
  TfrmEditorHTML = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    memoCodigoHTML: TMemo;
    ToolBar2: TToolBar;
    ToolButton6: TToolButton;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    ToolButton7: TToolButton;
    ToolButton9: TToolButton;
    bListaNumerica: TToolButton;
    listaMarcador: TToolButton;
    bDiminuRecuo: TToolButton;
    ToolButton14: TToolButton;
    bAumentaRecuo: TToolButton;
    ToolButton16: TToolButton;
    bAlinhaEsquerda: TToolButton;
    bCentraliza: TToolButton;
    bAlinhaDireita: TToolButton;
    webCodigoHTML: TWebBrowser;
    cdColor: TColorDialog;
    comboFonte: TComboBox;
    ToolButton20: TToolButton;
    bLink: TToolButton;
    ToolButton21: TToolButton;
    bDivisoria: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    bTabelas: TToolButton;
    ckFontes: TComboBox;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    btAbrirArquivo: TMenuItem;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    cxTreeView: TTreeView;
    procedure ToolButton6Click(Sender: TObject);
    procedure ToolButton8Click(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
    procedure comboFonteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure bListaNumericaClick(Sender: TObject);
    procedure listaMarcadorClick(Sender: TObject);
    procedure bDiminuRecuoClick(Sender: TObject);
    procedure bAumentaRecuoClick(Sender: TObject);
    procedure bAlinhaEsquerdaClick(Sender: TObject);
    procedure bCentralizaClick(Sender: TObject);
    procedure bAlinhaDireitaClick(Sender: TObject);
    procedure cxPageControl1Change(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure CarregarHTML(cCodigoHTML : String);
    function GeraHTML(Browser : TWebBrowser):String;
    procedure ToolButton2Click(Sender: TObject);
    procedure bLinkClick(Sender: TObject);
    procedure bDivisoriaClick(Sender: TObject);
    procedure bTabelasClick(Sender: TObject);
    procedure ckFontesChange(Sender: TObject);
    procedure ToolButton13Click(Sender: TObject);
    procedure btAbrirArquivoClick(Sender: TObject);
    procedure Salvar1Click(Sender: TObject);
    procedure webCodigoHTMLDocumentComplete(Sender: TObject;
      const pDisp: IDispatch; var URL: OleVariant);
    procedure ToolButton15Click(Sender: TObject);
    procedure cxTreeViewDblClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }

    cHTMLOriginal : String;
    BtFechar      : Integer;
    cNomeArquivo  : String;

  end;

var
  frmEditorHTML : TfrmEditorHTML;
  HTMLDocumento : IHTMLDocument2;

implementation

{$R *.dfm}

uses
  fMenu, fEditorHTML_PropTabela;

procedure TfrmEditorHTML.CarregarHTML(cCodigoHTML : String);
var
   sl: TStringList;
   ms: TMemoryStream;
begin
  sl := TStringList.Create;
  ms := TMemoryStream.Create;

  cHTMLOriginal := cCodigoHTML;

  while (webCodigoHTML.ReadyState < READYSTATE_INTERACTIVE) do
      Application.ProcessMessages;

  if Assigned(webCodigoHTML.Document) then
  begin
      try
          try
              sl.Text := cCodigoHTML;
              sl.SaveToStream(ms);
              ms.Seek(0,0);
             (webCodigoHTML.Document as IPersistStreamInit).Load(TStreamAdapter.Create(ms)) ;
          finally
              ms.Free;
          end;
      finally
          sl.Free;
      end;
  end;
end;

function GetIEHandle(WebBrowser: TWebbrowser; ClassName: string): HWND;
var
  hwndChild, hwndTmp: HWND;
  oleCtrl: TOleControl;
  szClass: array [0..255] of char;
begin
  oleCtrl := WebBrowser;
  hwndTmp := oleCtrl.Handle;

  while (true) do
  begin
    hwndChild := GetWindow(hwndTmp, GW_CHILD);
    GetClassName(hwndChild, szClass, SizeOf(szClass));
    if (string(szClass)=ClassName) then
    begin
      Result :=hwndChild;
      Exit;
    end;
    hwndTmp := hwndChild;
  end;

  Result := 0;
end;

function TfrmEditorHTML.GeraHTML(Browser : TWebBrowser):String;
var
  iall : IHTMLElement;
  HTML : String;
begin
  if BtFechar = 1 then
  begin
      Result := cHTMLOriginal;
      exit;
  end;

  if Assigned(Browser.Document) then
  begin
      iall := (Browser.Document AS IHTMLDocument2).body;
      while iall.parentElement <> nil do
      begin
          iall := iall.parentElement;
      end;

      HTML := iall.outerHTML;
      Result := HTML;
  end;
end;

procedure TfrmEditorHTML.ToolButton6Click(Sender: TObject);
begin
  inherited;

  HTMLDocumento := webCodigoHTML.Document as IHTMLDocument2;
  HTMLDocumento.execCommand('Bold', False,0);
end;

procedure TfrmEditorHTML.ToolButton8Click(Sender: TObject);
begin
  inherited;

  HTMLDocumento := webCodigoHTML.Document as IHTMLDocument2;
  HTMLDocumento.execCommand('Italic', False,0);
end;

procedure TfrmEditorHTML.ToolButton10Click(Sender: TObject);
begin
  inherited;

  HTMLDocumento := webCodigoHTML.Document as IHTMLDocument2;
  HTMLDocumento.execCommand('Underline', False,0);
end;

procedure TfrmEditorHTML.ToolButton9Click(Sender: TObject);
begin
  inherited;

  HTMLDocumento := webCodigoHTML.Document as IHTMLDocument2;
  if cdColor.Execute then
      HTMLDocumento.execCommand('ForeColor', False,cdColor.Color)
  else
      Abort;
end;

procedure TfrmEditorHTML.comboFonteChange(Sender: TObject);
begin
  inherited;

  //altera o tamanho da fonte
  HTMLDocumento := webCodigoHTML.Document as IHTMLDocument2;
  case comboFonte.ItemIndex of
      1: HTMLDocumento.execCommand('FontSize', False,1);
      2: HTMLDocumento.execCommand('FontSize', False,2);
      3: HTMLDocumento.execCommand('FontSize', False,3);
      4: HTMLDocumento.execCommand('FontSize', False,5);
      5: HTMLDocumento.execCommand('FontSize', False,6);
      6: HTMLDocumento.execCommand('FontSize', False,7);
  end;
end;

procedure DocumentoEmBranco(WebBrowser: TWebBrowser);
begin
  WebBrowser.Navigate('about:blank');
end;

procedure TfrmEditorHTML.FormCreate(Sender: TObject);
var
  DocHTML: IHTMLDocument2;
begin
  inherited;
  BtFechar := 0;
  cxPageControl1.ActivePage := cxTabSheet1;

  { -- define p�gina em branco -- }
  webCodigoHTML.Navigate('about:blank');

  { -- habilita edi��o do WebBrowser -- }
  DocHTML := webCodigoHTML.Document as IHtmlDocument2;
  DocHTML.designMode := 'On';
end;

procedure TfrmEditorHTML.bListaNumericaClick(Sender: TObject);
begin
  inherited;

  SendMessage(GetIEHandle(webCodigoHTML, 'Internet Explorer_Server'),
  WM_COMMAND,IDM_MARCADOR, 0);
end;

procedure TfrmEditorHTML.listaMarcadorClick(Sender: TObject);
begin
  inherited;

  SendMessage(GetIEHandle(webCodigoHTML, 'Internet Explorer_Server'),
  WM_COMMAND,IDM_MARCADOR_LISTA, 0);
end;

procedure TfrmEditorHTML.bDiminuRecuoClick(Sender: TObject);
begin
  inherited;

  SendMessage(GetIEHandle(webCodigoHTML, 'Internet Explorer_Server'),
  WM_COMMAND,IDM_OUTDENT, 0);
end;

procedure TfrmEditorHTML.bAumentaRecuoClick(Sender: TObject);
begin
  inherited;

  SendMessage(GetIEHandle(webCodigoHTML, 'Internet Explorer_Server'),
  WM_COMMAND,IDM_INDENT, 0);
end;

procedure TfrmEditorHTML.bAlinhaEsquerdaClick(Sender: TObject);
begin
  inherited;

  SendMessage(GetIEHandle(webCodigoHTML, 'Internet Explorer_Server'),
  WM_COMMAND,IDM_ALINHARESQ, 0);
end;

procedure TfrmEditorHTML.bCentralizaClick(Sender: TObject);
begin
  inherited;

  SendMessage(GetIEHandle(webCodigoHTML, 'Internet Explorer_Server'),
  WM_COMMAND,IDM_CENTRALIZAR, 0);
end;

procedure TfrmEditorHTML.bAlinhaDireitaClick(Sender: TObject);
begin
  inherited;

  SendMessage(GetIEHandle(webCodigoHTML, 'Internet Explorer_Server'),
  WM_COMMAND,IDM_ALINHADIR, 0);
end;

procedure TfrmEditorHTML.cxPageControl1Change(Sender: TObject);
begin
  inherited;

  if cxPageControl1.ActivePage.Caption = 'C�digo HTML' then
  begin
      memoCodigoHTML.Clear;
      memoCodigoHTML.Lines.Add(GeraHTML(webCodigoHTML));
  end;
end;

procedure TfrmEditorHTML.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (MessageDLG('Confirma processamento do c�digo HTML ?',mtConfirmation,[mbYes,mbNo],0) =  mrNo) then
      Exit
  else
      Close;
end;

procedure TfrmEditorHTML.ToolButton2Click(Sender: TObject);
begin
  BtFechar := 1;

  inherited;
end;

procedure TfrmEditorHTML.bLinkClick(Sender: TObject);
begin
  inherited;
  SendMessage(GetIEHandle(webCodigoHTML, 'Internet Explorer_Server'),
  WM_COMMAND,IDM_HYPERLINK,0);
end;

procedure TfrmEditorHTML.bDivisoriaClick(Sender: TObject);
begin
  inherited;
  SendMessage(GetIEHandle(webCodigoHTML, 'Internet Explorer_Server'),
  WM_COMMAND,IDM_LINHAHORIZ, 0);
end;

procedure TfrmEditorHTML.bTabelasClick(Sender: TObject);
var
  objPropTabela : TfrmEditorHTML_PropTabela;
  Tabela        : IHTMLTable;
  Linha         : IHTMLTableRow;
  Celula        : IHTMLTableCell;

  cAlinhamento  : String;
  contLinhas    : Integer;
  contCelulas   : Integer;
  contBorda     : Integer;
  contEspac     : Integer;
  contLarg      : Integer;
  contAlt       : Integer;
  iLinha        : Integer;
  iCelula       : Integer;
  iRed          : Integer;
  iGreen        : Integer;
  iBlue         : Integer;
begin
  inherited;
  try
      {-- Instancia o HTMLDocumento --}
      HTMLDocumento := webCodigoHTML.Document as IHTMLDocument2;

      objPropTabela := TfrmEditorHTML_PropTabela.Create(Nil);
      showForm(objPropTabela, False);

      {-- Propriedades da Tabela --}
      with(objPropTabela) do
      begin
          if(not bProcesso) then
              exit;

          {-- Cria a tabela --}
          Tabela := HTMLDocumento.createElement('<table>') as IHTMLTable;

          {-- Pega as defini��es da tabela e seta os padr�es --}

          // Linhas: Padr�o = 1
          if (trim(edtLinhas.Text) <> '') then
              contLinhas := StrToInt(edtLinhas.Text)
          else
              contLinhas := 1;

          // C�lulas: Padr�o = 1
          if (trim(edtCelulas.Text) <> '') then
              contCelulas := StrToInt(edtCelulas.Text)
          else
              contCelulas := 1;

          // Alinhamento Vertical
          if (trim(ckAlinhamento.Text) = 'Superior') then cAlinhamento := 'Top';
          if (trim(ckAlinhamento.Text) = 'Centro')   then cAlinhamento := 'Middle';
          if (trim(ckAlinhamento.Text) = 'Inferior') then cAlinhamento := 'Bottom';

          // Bordas: Padr�o = 0
          if (ckBorda.Items[ckBorda.ItemIndex] <> '') then
              contBorda := StrToInt(ckBorda.Items[ckBorda.ItemIndex])
          else
              contBorda := 0;

          // Largura: Padr�o = 50
          if (trim(edtLargura.Text) <> '') then
              contLarg := StrToInt(edtLargura.Text)
          else
              contLarg := 50;

          // Altura: Padr�o = 20
          if (trim(edtAltura.Text) <> '') then
              contAlt := StrToInt(edtAltura.Text)
          else
              contAlt := 20;

          // Espa�amento: Padr�o = 2
          if (trim(edtEspacamento.Text) <> '') then
              contEspac := StrToInt(edtEspacamento.Text)
          else
              contEspac := 2;

          // Cor da Tabela
          if (trim(edtRGB.Text)) <> '' then
          begin
              {-- Cor da fundo da tabela --}
              Tabela.bgColor := RGB(
                                    GetBValue(ColorToRGB(corFundo))   // Azul
                                   ,GetGValue(ColorToRGB(corFundo))   // Verde
                                   ,GetRValue(ColorToRGB(corFundo))); // Vermelho
          end
          else
              Tabela.bgColor := RGB(255,255,255); // Branco

          {-- Cria as linhas --}
          for iLinha := 1 to contLinhas do
          begin
              Linha := Tabela.insertRow(-1) as IHTMLTableRow;
              {-- Cria as celulas --}
              for iCelula := 1 to contCelulas do
              begin
                  Celula := Linha.insertCell(-1) as IHTMLTableCell;
                  {-- Altura e Largura --}
                  Celula.colSpan := contLarg;
                  Celula.height  := contAlt;
                  Celula.vAlign  := cAlinhamento;
                  Celula.borderColor := rgb(245,245,245); // cor da borda preta por padr�o
              end;
          end;

          {-- Coloca a borda na tabela --}
          Tabela.border := contBorda;
          
          {-- Espa�amento das C�lulas --}
          Tabela.cellPadding := contEspac;
          Tabela.cellSpacing := 0; // Padr�o
          {-- Adiciona ao editor --}
          (HTMLDocumento.body as IHTMLDOMNode).appendChild(Tabela as IHTMLDOMNode);
      end;
  finally
      FreeAndNil(objPropTabela);
  end;
end;

procedure TfrmEditorHTML.ckFontesChange(Sender: TObject);
var
  font : IHTMLStyle;
begin
  inherited;
  {-- Altera a Fonte --}
  HTMLDocumento := webCodigoHTML.Document as IHTMLDocument2;
  HTMLDocumento.execCommand('FontName', False, ckFontes.Text);
end;

procedure TfrmEditorHTML.ToolButton13Click(Sender: TObject);
var
  Imagem     : IHTMLImgElement;
  ImagemSrc  : String;
begin
  inherited;
  if (OpenDialog1.Execute) then
      ImagemSrc    := OpenDialog1.FileName;

  HTMLDocumento := webCodigoHTML.Document as IHTMLDocument2;
  Imagem := HTMLDocumento.createElement('<img>') as IHTMLImgElement;
  Imagem.src := ImagemSrc;
  Imagem.alt := ImagemSrc;
  (HTMLDocumento.body as IHTMLDOMNode).appendChild(Imagem as IHTMLDOMNode);
end;

procedure TfrmEditorHTML.btAbrirArquivoClick(Sender: TObject);
begin
  inherited;
  if (OpenDialog1.Execute) then
  begin
      webCodigoHTML.Navigate(OpenDialog1.FileName);
      HTMLDocumento := webCodigoHTML.Document as IHTMLDocument2;
      cNomeArquivo := OpenDialog1.FileName;
  end;
end;

procedure TfrmEditorHTML.Salvar1Click(Sender: TObject);
var
  i            : Integer;
  cFileName    : String;
  cCaminho     : String;
  cPath        : String;
  cPathSave    : String;
  Persistencia : IPersistFile;
begin
  inherited;

  HTMLDocumento := webCodigoHTML.Document as IHTMLDocument2;

  {-- Verifica se o arquivo ja existe, e ent�o salva por cima --}
  if(trim(cNomeArquivo) <> '') then
  begin
      Persistencia := HTMLDocumento as IPersistFile;
      Persistencia.Save(StringToOleStr(cNomeArquivo), System.True);
      ShowMessage('Arquivo Salvo com Sucesso!');
      abort;
  end;

  SaveDialog1.InitialDir := ExtractFilePath(Application.ExeName) + '\mod_documento\';
  SaveDialog1.DefaultExt := 'html';
  SaveDialog1.Filter := 'Arquivo HTML (.HTML)|*.html|';
  if (SaveDialog1.Execute) then
  begin
    {-- Seta o caminho da pasta raiz --}
    cPath := ExtractFilePath(Application.ExeName) + 'mod_documento\';

    {-- Seta o caminho onde sera salvo --}
    cPathSave := ExtractFilePath(SaveDialog1.FileName);
    
    try
        {-- Salva as imagens, caso tenha --}
        for i := 0 to HTMLDocumento.images.length - 1 do
        begin
            cFileName := (HTMLDocumento.images.item(i, 0) as IHTMLImgElement).nameProp; // nome do arquivo
            cCaminho  := (HTMLDocumento.images.item(i, 0) as IHTMLImgElement).alt; // Caminho do arquivo

            {-- Transfere a imagem para a pasta com o nome do arquivo salvo --}
            CopyFile(PChar(cCaminho), PChar(cPathSave + cFileName), true);

            {-- Altera o valor de Src e Alt --}
            (HTMLDocumento.images.item(i, 0) as IHTMLImgElement).src := cFileName; // Coloca apenas o nome do arquivo
            (HTMLDocumento.images.item(i, 0) as IHTMLImgElement).alt := cPathSave + cFileName; // Coloca o novo caminho
        end;
        
        {-- Salva o arquivo .html --}
        Persistencia := HTMLDocumento as IPersistFile;
        Persistencia.Save(StringToOleStr(SaveDialog1.FileName), System.True);

    finally
        SaveDialog1.Free;
        ShowMessage('Arquivo Salvo com sucesso!');
        for i:= 0 to HTMLDocumento.images.length - 1 do
            {-- Seta com o novo caminho para voltar ao normal e exibir no WebBrowser --}
            (HTMLDocumento.images.item(i, 0) as IHTMLImgElement).src := (HTMLDocumento.images.item(i, 0) as IHTMLImgElement).alt;
    end;
  end
  else
      abort;
end;


procedure TfrmEditorHTML.webCodigoHTMLDocumentComplete(Sender: TObject;
  const pDisp: IDispatch; var URL: OleVariant);
begin
  inherited;
  {-- habilita o modo de edi��o depois de carregar a p�gina --}
  HTMLDocumento := webCodigoHTML.Document as IHTMLDocument2;
  HTMLDocumento.designMode := 'On';
end;

procedure TfrmEditorHTML.ToolButton15Click(Sender: TObject);
var
  Selection : IHTMLSelectionObject;
  range     : IHTMLTxtRange;
begin
  inherited;
  HTMLDocumento := webCodigoHTML.Document as IHTMLDocument2;
  Selection := HTMLDocumento.selection;
  range     := Selection.createRange as IHTMLTxtRange;
end;

procedure TfrmEditorHTML.cxTreeViewDblClick(Sender: TObject);
var
  variavelSistema  : IHTMLElement;
  TextoSelecionado : IHTMLSelectionObject;
  TextoRange       : IHTMLTxtRange;
  cGrupos          : TStringList;
  I                : Integer;
begin
  inherited;
  HTMLDocumento              := webCodigoHTML.Document as IHTMLDocument2;
  variavelSistema            := HTMLDocumento.createElement('SPAN') as IHTMLElement;
  {-- Pega texto Selecionado --}
  TextoSelecionado           := HTMLDocumento.Selection;
  TextoRange                 := TextoSelecionado.createRange as IHTMLTxtRange;

  {-- cria lista de grupos --}
  cGrupos := TStringList.Create;
  cGrupos.add('DoctoFiscal');
  cGrupos.add('Empresa');
  cGrupos.add('Loja');
  cGrupos.add('Terceiro');

  {-- Verifica se o usuario n�o clicou em cima dos grupos --}
  if (not cGrupos.Find(cxTreeView.Selected.Text, I)) then
  begin
      {-- Verifica se selecionou algum texto --}
      if (TextoRange.text <> '') then
      begin
          TextoRange.text := cxTreeView.Selected.Text;
          Abort;
      end;

      {-- Insere Dentro do Dom caso n�o seja selecionado nenhum texto --}
      variavelSistema.innerText  := cxTreeView.Selected.Text;  // atribui texto ao span
      (HTMLDocumento.body as IHTMLDOMNode).appendChild(variavelSistema as IHTMLDOMNode); // insere no dom do webbrowser
  end;
  
end;

procedure TfrmEditorHTML.Button1Click(Sender: TObject);
begin
  inherited;
  // code
end;

end.
