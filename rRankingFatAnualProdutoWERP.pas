unit rRankingFatAnualProdutoWERP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons, comObj ;

type
  TrptRankingFatAnualProdutoWERP = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit4: TMaskEdit;
    Label5: TLabel;
    MaskEdit6: TMaskEdit;
    Label6: TLabel;
    RadioGroup1: TRadioGroup;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DBEdit1: TDBEdit;
    DataSource6: TDataSource;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    DBEdit4: TDBEdit;
    DataSource7: TDataSource;
    qryRegiao: TADOQuery;
    qryRegiaonCdRegiao: TIntegerField;
    qryRegiaocNmRegiao: TStringField;
    DataSource8: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    MaskEdit9: TMaskEdit;
    Label10: TLabel;
    DataSource9: TDataSource;
    DBEdit8: TDBEdit;
    DataSource10: TDataSource;
    Label4: TLabel;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit4Exit(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRankingFatAnualProdutoWERP: TrptRankingFatAnualProdutoWERP;

implementation

uses fMenu, fLookup_Padrao, QRExport , rRankingFatAnualProduto_view;

{$R *.dfm}

procedure TrptRankingFatAnualProdutoWERP.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin

  iAux := frmMenu.nCdEmpresaAtiva;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  if (iAux > 0) then
  begin

    MaskEdit3.Text := IntToStr(iAux) ;

    qryEmpresa.Close ;
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := iAux ;
    qryEmpresa.Open ;

  end ;

  qryTerceiro.Close ;
  MaskEdit9.Text := intToStr(frmMenu.nCdTerceiroUsuario) ;
  PosicionaQuery(qryTerceiro, MaskEdit9.Text) ;

  MaskEdit3.SetFocus ;

  inherited;
  
end;


procedure TrptRankingFatAnualProdutoWERP.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

function MesExtenso(dData: TDateTime):string;
begin

    if StrToInt(Copy(DateToStr(dData),4,2)) = 1 then
        result := 'jan/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 2 then
        result := 'fev/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 3 then
        result := 'mar/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 4 then
        result := 'abr/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 5 then
        result := 'mai/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 6 then
        result := 'jun/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 7 then
        result := 'jul/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 8 then
        result := 'ago/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 9 then
        result := 'set/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 10 then
        result := 'out/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 11 then
        result := 'nov/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 12 then
        result := 'dez/' + Copy(DateToStr(dData),9,2) ;

end ;

procedure TrptRankingFatAnualProdutoWERP.ToolButton1Click(Sender: TObject);
var
    dAux : TDateTime ;
    dAux1, dAux2, dAux3, dAux4, dAux5, dAux6 : TDateTime ;
    dAux7, dAux8, dAux9, dAux10, dAux11, dAux12 : TDateTime ;

var linha, coluna : integer;
var planilha : variant;
var valorcampo : string;

begin

  If (Trim(MaskEdit3.Text) = '') or (DBEDit3.Text = '') Then
  begin
      ShowMessage('Informe a Empresa.') ;
      exit ;
  end ;

  if (Length(Trim(MaskEdit1.Text)) <> 7) then
  begin
      ShowMessage('Compet�ncia Inicial Inv�lida.') ;
      exit ;
  end ;

  try
      dAux := StrToDateTime('01/' + MaskEdit1.Text) ;
  except
      ShowMessage('Compet�ncia Inicial Inv�lida.') ;
      exit ;
  end ;

  dAux1 := StrToDateTime('01/' + MaskEdit1.Text) ;
  dAux2 := dAux1  + 31 ;
  dAux3 := dAux2  + 31 ;
  dAux4 := dAux3  + 31 ;
  dAux5 := dAux4  + 31 ;
  dAux6 := dAux5  + 31 ;
  dAux7 := dAux6  + 31 ;
  dAux8 := dAux7  + 31 ;
  dAux9 := dAux8  + 31 ;
  dAux10:= dAux9  + 31 ;
  dAux11:= dAux10 + 31 ;
  dAux12:= dAux11 + 31 ;


  rptRankingFatAnualProduto_view.lblmes1.Caption  := MesExtenso(dAux1) ;
  rptRankingFatAnualProduto_view.lblmes2.Caption  := MesExtenso(dAux2) ;
  rptRankingFatAnualProduto_view.lblmes3.Caption  := MesExtenso(dAux3) ;
  rptRankingFatAnualProduto_view.lblmes4.Caption  := MesExtenso(dAux4) ;
  rptRankingFatAnualProduto_view.lblmes5.Caption  := MesExtenso(dAux5) ;
  rptRankingFatAnualProduto_view.lblmes6.Caption  := MesExtenso(dAux6) ;
  rptRankingFatAnualProduto_view.lblmes7.Caption  := MesExtenso(dAux7) ;
  rptRankingFatAnualProduto_view.lblmes8.Caption  := MesExtenso(dAux8) ;
  rptRankingFatAnualProduto_view.lblmes9.Caption  := MesExtenso(dAux9) ;
  rptRankingFatAnualProduto_view.lblmes10.Caption := MesExtenso(dAux10) ;
  rptRankingFatAnualProduto_view.lblmes11.Caption := MesExtenso(dAux11) ;
  rptRankingFatAnualProduto_view.lblmes12.Caption := MesExtenso(dAux12) ;

  rptRankingFatAnualProduto_view.usp_Relatorio.Close ;
  rptRankingFatAnualProduto_view.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  rptRankingFatAnualProduto_view.usp_Relatorio.Parameters.ParamByName('@nCdUnidadeNegocio').Value := 0 ;
  rptRankingFatAnualProduto_view.usp_Relatorio.Parameters.ParamByName('@nCdProduto').Value        := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  rptRankingFatAnualProduto_view.usp_Relatorio.Parameters.ParamByName('@nCdLinha').Value          := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  rptRankingFatAnualProduto_view.usp_Relatorio.Parameters.ParamByName('@nCdRegiao').Value         := 0 ;
  rptRankingFatAnualProduto_view.usp_Relatorio.Parameters.ParamByName('@cUF').Value               := '' ;
  rptRankingFatAnualProduto_view.usp_Relatorio.Parameters.ParamByName('@cMesAnoIni').Value        := Trim(MaskEdit1.Text) ;
  rptRankingFatAnualProduto_view.usp_Relatorio.Parameters.ParamByName('@cMesAnoFim').Value        := Trim(MaskEdit1.Text) ;
  rptRankingFatAnualProduto_view.usp_Relatorio.Parameters.ParamByName('@nCdGrupoEconomico').Value := 0 ;
  rptRankingFatAnualProduto_view.usp_Relatorio.Parameters.ParamByName('@nCdTerceiroAux').Value    := frmMenu.ConvInteiro(MaskEdit9.Text) ;

  {If (RadioGroup1.ItemIndex = 0) then
      rptRankingFatAnualProduto_view.usp_Relatorio.Parameters.ParamByName('@cFlgTipo').Value := 'V' ;

  If (RadioGroup1.ItemIndex = 1) then}
  rptRankingFatAnualProduto_view.usp_Relatorio.Parameters.ParamByName('@cFlgTipo').Value := 'Q' ;

  rptRankingFatAnualProduto_view.usp_Relatorio.Open;


  rptRankingFatAnualProduto_view.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
  rptRankingFatAnualProduto_view.lblFiltro1.Caption := 'Empresa: ' + Trim(DbEdit3.Text) ;

  rptRankingFatAnualProduto_view.lblFiltro1.Caption := rptRankingFatAnualProduto_view.lblFiltro1.Caption + ' / Compet�ncia Inicial: ' + MaskEdit1.Text ;

  if (dbEdit8.Text <> '') then rptRankingFatAnualProduto_view.lblFiltro1.Caption := rptRankingFatAnualProduto_view.lblFiltro1.Caption + ' / Cliente: ' + Trim(DbEdit8.Text) ;

  if (dbEdit1.Text <> '') then rptRankingFatAnualProduto_view.lblFiltro1.Caption := rptRankingFatAnualProduto_view.lblFiltro1.Caption + ' / Produto: ' + Trim(DbEdit1.Text) ;
  if (dbEdit4.Text <> '') then rptRankingFatAnualProduto_view.lblFiltro1.Caption := rptRankingFatAnualProduto_view.lblFiltro1.Caption + ' / Linha: ' + Trim(DbEdit4.Text) ;

  {If (RadioGroup1.ItemIndex = 0) then
      rptRankingFatAnualProduto_view.lblFiltro1.Caption := rptRankingFatAnualProduto_view.lblFiltro1.Caption + ' / Modo: VALOR ' ;

  If (RadioGroup1.ItemIndex = 1) then}
  rptRankingFatAnualProduto_view.lblFiltro1.Caption := rptRankingFatAnualProduto_view.lblFiltro1.Caption + ' / Modo: QUANTIDADE ' ;


  rptRankingFatAnualProduto_view.QuickRep1.PreviewModal;


{  if (RadioGroup1.ItemIndex = 1) then
  begin

    planilha:= CreateoleObject('Excel.Application');
    planilha.WorkBooks.add(1);
    planilha.caption := 'DRE - Demonstrativo de Resultados do Exerc�cio';
    planilha.visible := true;

    for linha := 0 to rptDRE_view.usp_Relatorio.RecordCount - 1 do
    begin
       for coluna := 1 to rptDRE_view.usp_Relatorio.FieldCount do
       begin
         valorcampo := rptDRE_view.usp_Relatorio.Fields[coluna - 1].AsString;
         planilha.cells[linha + 2,coluna] := valorCampo;
       end;
       rptDRE_view.usp_Relatorio.Next;
    end;
    for coluna := 1 to rptDRE_view.usp_Relatorio.FieldCount do
    begin

       valorcampo := rptDRE_view.usp_Relatorio.Fields[coluna - 1].DisplayLabel;
       planilha.cells[1,coluna] := valorcampo;

    end;
    planilha.columns.Autofit;

  end
  else
  begin

  end ; }

end;


procedure TrptRankingFatAnualProdutoWERP.cxButton1Click(Sender: TObject);
begin
  inherited;
  //rptDRE_View.QuickRep1.ExportToFilter(TQRXLSFilter.Create('c:\teste.xls'));
end;

procedure TrptRankingFatAnualProdutoWERP.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(106);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryProduto, MaskEdit4.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRankingFatAnualProdutoWERP.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLinha, MaskEdit6.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRankingFatAnualProdutoWERP.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryLinha.Close ;
  PosicionaQuery(qryLinha, Maskedit6.Text) ;

end;

procedure TrptRankingFatAnualProdutoWERP.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryProduto.Close ;
  PosicionaQuery(qryProduto, Maskedit4.Text) ;

end;

initialization
     RegisterClass(TrptRankingFatAnualProdutoWERP) ;

end.
