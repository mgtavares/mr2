unit rClientesSemCompraPeriodo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, DBCtrls, StdCtrls, Mask;

type
  TrptClientesSemCompraPeriodo = class(TfrmRelatorio_Padrao)
    Label1: TLabel;
    MaskEdit3: TMaskEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label2: TLabel;
    MaskEdit4: TMaskEdit;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    MaskEdit6: TMaskEdit;
    DBEdit9: TDBEdit;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    Label4: TLabel;
    MaskEdit5: TMaskEdit;
    DBEdit4: TDBEdit;
    Label7: TLabel;
    MaskEdit7: TMaskEdit;
    DBEdit5: TDBEdit;
    qryGrupoEconomico: TADOQuery;
    qryRepresentante: TADOQuery;
    qryRegiao: TADOQuery;
    qryRamoAtividade: TADOQuery;
    qryRepresentantenCdTerceiro: TIntegerField;
    qryRepresentantecNmTerceiro: TStringField;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    qryRegiaonCdRegiao: TIntegerField;
    qryRegiaocNmRegiao: TStringField;
    qryRamoAtividadenCdRamoAtividade: TIntegerField;
    qryRamoAtividadecNmRamoAtividade: TStringField;
    dsEmpresa: TDataSource;
    dsGrupoEconomico: TDataSource;
    dsRegiao: TDataSource;
    dsRamoAtividade: TDataSource;
    dsRepresentante: TDataSource;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptClientesSemCompraPeriodo: TrptClientesSemCompraPeriodo;

implementation

uses fLookup_Padrao, fMenu, rClientesSemCompraPeriodo_View;

{$R *.dfm}

procedure TrptClientesSemCompraPeriodo.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptClientesSemCompraPeriodo.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close ;

  PosicionaQuery(qryGrupoEconomico, MaskEdit4.Text) ;

end;

procedure TrptClientesSemCompraPeriodo.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptClientesSemCompraPeriodo.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TrptClientesSemCompraPeriodo.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  iAux := frmMenu.UsuarioEmpresaAutorizada;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  MaskEdit3.SetFocus ;

  CheckBox1.Checked := True;
  CheckBox2.Checked := True;

end;

procedure TrptClientesSemCompraPeriodo.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(103);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
            PosicionaQuery(qryRepresentante, IntToStr(nPK));
        end ;

    end ;

  end ;


end;

procedure TrptClientesSemCompraPeriodo.MaskEdit6Exit(Sender: TObject);
begin
  inherited;

  qryRepresentante.Close ;

  PosicionaQuery(qryRepresentante, MaskEdit6.Text) ;

end;

procedure TrptClientesSemCompraPeriodo.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(21);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryRegiao, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptClientesSemCompraPeriodo.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryRegiao.Close ;

  PosicionaQuery(qryRegiao, MaskEdit5.Text) ;

end;

procedure TrptClientesSemCompraPeriodo.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(158);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
            PosicionaQuery(qryRamoAtividade, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptClientesSemCompraPeriodo.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryRamoAtividade.Close ;

  PosicionaQuery(qryRamoAtividade, MaskEdit7.Text) ;

end;

procedure TrptClientesSemCompraPeriodo.ToolButton1Click(Sender: TObject);
var
   objRel : TrptClientesSemCompraPeriodo_view ;
begin
  inherited;

  objRel := TrptClientesSemCompraPeriodo_view.Create(nil);
  
  Try
      Try
          objRel.uspRelatorio.Close ;
          objRel.uspRelatorio.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
          objRel.uspRelatorio.Parameters.ParamByName('@nCdGrupoEconomico').Value := frmMenu.ConvInteiro(MaskEdit4.Text) ;
          objRel.uspRelatorio.Parameters.ParamByName('@nCdRepresentante').Value  := frmMenu.ConvInteiro(MaskEdit6.Text) ;
          objRel.uspRelatorio.Parameters.ParamByName('@nCdRegiao').Value         := frmMenu.ConvInteiro(MaskEdit5.Text) ;
          objRel.uspRelatorio.Parameters.ParamByName('@nCdRamoAtividade').Value  := frmMenu.ConvInteiro(MaskEdit7.Text) ;
          objRel.uspRelatorio.Parameters.ParamByName('@dDtInicial').Value        := frmMenu.ConvData(MaskEdit1.Text) ;
          objRel.uspRelatorio.Parameters.ParamByName('@dDtFinal').Value          := frmMenu.ConvData(MaskEdit2.Text) ;
          objRel.uspRelatorio.Parameters.ParamByName('@nCdUsuario').Value        := frmMenu.nCdUsuarioLogado ;

          if CheckBox1.Checked then objRel.uspRelatorio.Parameters.ParamByName('@flgClienteQueJaComprou').Value := 1
          else objRel.uspRelatorio.Parameters.ParamByName('@flgClienteQueJaComprou').Value := 0;

          if CheckBox2.Checked then objRel.uspRelatorio.Parameters.ParamByName('@flgClienteQueComprouAposFinalPeriodo').Value := 1
          else objRel.uspRelatorio.Parameters.ParamByName('@flgClienteQueComprouAposFinalPeriodo').Value := 0;

          objRel.uspRelatorio.Open  ;

          objRel.lblFiltro2.Caption := '';
          objRel.lblFiltro3.Caption := '';
          objRel.lblFiltro4.Caption := '';
          objRel.lblFiltro5.Caption := '';
          objRel.lblFiltro6.Caption := '';
          objRel.lblFiltro7.Caption := '';
          objRel.lblFiltro8.Caption := '';

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := 'Empresa : '        + Trim(MaskEdit3.Text)          + ' ' + DbEdit3.Text ;
          if Trim(DbEdit9.Text)   <> '' then objRel.lblFiltro6.Caption := 'Repres.: '      + Trim(String(MaskEdit6.Text)) + ' ' + DbEdit9.Text ;
          if Trim(DbEdit4.Text)   <> '' then objRel.lblFiltro3.Caption := 'Regi�o: '       + Trim(String(MaskEdit5.Text)) + ' ' + DbEdit4.Text ;
          if Trim(DbEdit5.Text)   <> '' then objRel.lblFiltro4.Caption := 'R.Atividade: '  + Trim(String(MaskEdit7.Text)) + ' ' + DbEdit5.Text ;
          if Trim(DbEdit1.Text)   <> '' then objRel.lblFiltro5.Caption := 'G.Econ�mico: '  + Trim(String(MaskEdit4.Text)) + ' ' + DbEdit1.Text ;
          if Trim(MaskEdit1.Text) <> '/  /' then objRel.lblFiltro2.Caption := 'Per�odo : ' + String(MaskEdit1.Text) + ' ' + String(MaskEdit2.Text) ;

          if CheckBox1.Checked then objRel.lblFiltro7.Caption := 'Somente clientes que j� realizaram compra.' ;
          if CheckBox2.Checked then objRel.lblFiltro8.Caption := 'Clientes que compraram ap�s a data final do filtro.' ;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do Relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

initialization
     RegisterClass(TrptClientesSemCompraPeriodo) ;

end.
