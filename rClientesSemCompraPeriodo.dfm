inherited rptClientesSemCompraPeriodo: TrptClientesSemCompraPeriodo
  Left = 372
  Top = 230
  Width = 857
  Height = 366
  Caption = 'Clientes Sem Compra no Per'#237'odo'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 841
    Height = 299
  end
  object Label1: TLabel [1]
    Left = 57
    Top = 42
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label2: TLabel [2]
    Left = 16
    Top = 66
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Econ'#244'mico'
  end
  object Label5: TLabel [3]
    Left = 26
    Top = 90
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Representante'
  end
  object Label3: TLabel [4]
    Left = 62
    Top = 162
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo'
  end
  object Label6: TLabel [5]
    Left = 184
    Top = 162
    Width = 16
    Height = 13
    Alignment = taRightJustify
    Caption = 'at'#233
  end
  object Label4: TLabel [6]
    Left = 65
    Top = 113
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Regi'#227'o'
  end
  object Label7: TLabel [7]
    Left = 8
    Top = 138
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ramo de Atividade'
  end
  inherited ToolBar1: TToolBar
    Width = 841
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit3: TMaskEdit [9]
    Left = 104
    Top = 34
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object DBEdit2: TDBEdit [10]
    Tag = 1
    Left = 171
    Top = 34
    Width = 65
    Height = 21
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [11]
    Tag = 1
    Left = 238
    Top = 34
    Width = 584
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 3
  end
  object MaskEdit4: TMaskEdit [12]
    Left = 104
    Top = 58
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object DBEdit1: TDBEdit [13]
    Tag = 1
    Left = 171
    Top = 58
    Width = 651
    Height = 21
    DataField = 'cNmGrupoEconomico'
    DataSource = dsGrupoEconomico
    TabOrder = 5
  end
  object MaskEdit6: TMaskEdit [14]
    Left = 104
    Top = 82
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object DBEdit9: TDBEdit [15]
    Tag = 1
    Left = 171
    Top = 82
    Width = 651
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsRepresentante
    TabOrder = 9
  end
  object MaskEdit1: TMaskEdit [16]
    Left = 104
    Top = 154
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 10
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [17]
    Left = 204
    Top = 154
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 11
    Text = '  /  /    '
  end
  object MaskEdit5: TMaskEdit [18]
    Left = 104
    Top = 106
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 7
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object DBEdit4: TDBEdit [19]
    Tag = 1
    Left = 171
    Top = 106
    Width = 651
    Height = 21
    DataField = 'cNmRegiao'
    DataSource = dsRegiao
    TabOrder = 12
  end
  object MaskEdit7: TMaskEdit [20]
    Left = 104
    Top = 130
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 8
    Text = '      '
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object DBEdit5: TDBEdit [21]
    Tag = 1
    Left = 171
    Top = 130
    Width = 651
    Height = 21
    DataField = 'cNmRamoAtividade'
    DataSource = dsRamoAtividade
    TabOrder = 13
  end
  object CheckBox1: TCheckBox [22]
    Left = 104
    Top = 183
    Width = 281
    Height = 17
    Caption = 'Listar somente clientes que j'#225' realizaram compra.'
    TabOrder = 14
  end
  object CheckBox2: TCheckBox [23]
    Left = 104
    Top = 205
    Width = 305
    Height = 17
    Caption = 'Listar clientes que compraram ap'#243's a data final do filtro'
    TabOrder = 15
  end
  inherited ImageList1: TImageList
    Left = 488
    Top = 168
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdGrupoEconomico, cNmGrupoEconomico  FROM  GrupoEconomic' +
        'o   WHERE  nCdGrupoEconomico = :nPk')
    Left = 552
    Top = 168
    object qryGrupoEconomiconCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object qryRepresentante: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPk'
      '   AND nCdStatus   = 1'
      '   AND EXISTS(SELECT 1'
      '                FROM TerceiroTipoTerceiro TTT'
      '               WHERE TTT.nCdTerceiro     = Terceiro.nCdTerceiro'
      '                 AND TTT.nCdTipoTerceiro = 4)'
      ''
      '')
    Left = 584
    Top = 168
    object qryRepresentantenCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryRepresentantecNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryRegiao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdRegiao, cNmRegiao FROM Regiao WHERE nCdRegiao = :nPk')
    Left = 616
    Top = 168
    object qryRegiaonCdRegiao: TIntegerField
      FieldName = 'nCdRegiao'
    end
    object qryRegiaocNmRegiao: TStringField
      FieldName = 'cNmRegiao'
      Size = 50
    end
  end
  object qryRamoAtividade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdRamoAtividade, cNmRamoAtividade FROM RamoAtividade '
      'WHERE nCdRamoAtividade = :nPk')
    Left = 648
    Top = 168
    object qryRamoAtividadenCdRamoAtividade: TIntegerField
      FieldName = 'nCdRamoAtividade'
    end
    object qryRamoAtividadecNmRamoAtividade: TStringField
      FieldName = 'cNmRamoAtividade'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 520
    Top = 200
  end
  object dsGrupoEconomico: TDataSource
    DataSet = qryGrupoEconomico
    Left = 552
    Top = 200
  end
  object dsRegiao: TDataSource
    DataSet = qryRegiao
    Left = 616
    Top = 200
  end
  object dsRamoAtividade: TDataSource
    DataSet = qryRamoAtividade
    Left = 648
    Top = 200
  end
  object dsRepresentante: TDataSource
    DataSet = qryRepresentante
    Left = 584
    Top = 200
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 520
    Top = 168
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
end
