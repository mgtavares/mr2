unit rLoteLiqCobradora_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptLoteLiqCobradora_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRBand5: TQRBand;
    QRShape2: TQRShape;
    QRDBText8: TQRDBText;
    QRDBText10: TQRDBText;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    qryLoteLiqCobradora: TADOQuery;
    QRLabel14: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel16: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel18: TQRLabel;
    QRDBText30: TQRDBText;
    qryLoteLiqCobradoranCdLoteLiqCobradora: TStringField;
    qryLoteLiqCobradoracNmCobradora: TStringField;
    qryLoteLiqCobradoranValTotal: TBCDField;
    qryLoteLiqCobradoracNmUsuario: TStringField;
    qryLoteLiqCobradoranCdTerceiro: TIntegerField;
    qryLoteLiqCobradoracCNPJCPF: TStringField;
    qryLoteLiqCobradoracNmTerceiro: TStringField;
    qryLoteLiqCobradoracFlgTipoLiq: TStringField;
    qryLoteLiqCobradoranValLiq: TBCDField;
    qryLoteLiqCobradoracFlgDiverg: TStringField;
    QRLabel6: TQRLabel;
    QRDBText4: TQRDBText;
    qryLoteLiqCobradoranSaldoTerceiro: TBCDField;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabel10: TQRLabel;
    qryLoteLiqCobradoracNrBorderoCob: TStringField;
    QRLabel11: TQRLabel;
    QRDBText7: TQRDBText;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel12: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptLoteLiqCobradora_view: TrptLoteLiqCobradora_view;

implementation

{$R *.dfm}

end.
