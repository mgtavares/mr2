unit rItensPorVendaPar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, ER2Lookup, DBCtrls, StdCtrls, Mask, DBGridEhGrouping, ToolCtrlsEh,
  GridsEh, DBGridEh, cxPC, cxControls, ER2Excel;

type
  TrptItensPorVendaPar = class(TfrmRelatorio_Padrao)
    RadioGroup1: TRadioGroup;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    er2LkpLoja: TER2LookupMaskEdit;
    er2LkpVendedor: TER2LookupMaskEdit;
    Label1: TLabel;
    Label5: TLabel;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    dsVendedor: TDataSource;
    qryVendedor: TADOQuery;
    qryVendedornCdUsuario: TIntegerField;
    qryVendedorcNmUsuario: TStringField;
    pagDeptos: TcxPageControl;
    tabDeptos: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cmdTempDepto: TADOCommand;
    qryDepartamento: TADOQuery;
    qryTempDepto: TADOQuery;
    dsTempDepto: TDataSource;
    qryTempDeptonCdTempDepto: TIntegerField;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryTempDeptocNmDepartamento: TStringField;
    ER2Excel: TER2Excel;
    rgModeloImp: TRadioGroup;
    procedure er2LkpVendedorBeforeLookup(Sender: TObject);
    procedure qryVendedorBeforeOpen(DataSet: TDataSet);
    procedure qryLojaBeforeOpen(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryTempDeptoBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptItensPorVendaPar : TrptItensPorVendaPar;
  cFiltro               : string;


implementation

uses fMenu, rItensPorVendaPar_view, fLookup_Padrao;

{$R *.dfm}

procedure TrptItensPorVendaPar.er2LkpVendedorBeforeLookup(
  Sender: TObject);
begin
  inherited;

  qryVendedorBeforeOpen(qryVendedor);
end;

procedure TrptItensPorVendaPar.qryVendedorBeforeOpen(DataSet: TDataSet);
begin
  inherited;

  if (Trim(DBEdit1.Text) = '') then
  begin
      MensagemAlerta('Para selecionar um vendedor, selecione primeiro uma loja.');
      er2LkpVendedor.Clear;
      er2LkpLoja.SetFocus;
      Abort;
  end;

  qryVendedor.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value;

  er2LkpVendedor.WhereAdicional.Text := 'EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdUsuario = Usuario.nCdUsuario AND UL.nCdLoja = ' + Trim(er2LkpLoja.Text) + ')' + ' ORDER BY Usuario.cNmUsuario';
end;

procedure TrptItensPorVendaPar.qryLojaBeforeOpen(DataSet: TDataSet);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

procedure TrptItensPorVendaPar.ToolButton1Click(Sender: TObject);
var
  objRel : TrptItensPorVendaPar_view;
  iLinha     : Integer;  
begin
  inherited;

  if (MaskEdit1.Text = '  /  /    ') then
      MaskEdit1.Text := DateToStr(Now())
  else
  begin
      if (frmMenu.ConvData(MaskEdit1.Text) = frmMenu.ConvData('01/01/1900')) then
      begin
          MaskEdit1.Clear;
          MaskEdit1.SetFocus;
          Abort;
      end;
  end;

  if (MaskEdit2.Text = '  /  /    ') then
      MaskEdit2.Text := DateToStr(Now())
  else
  begin
      if (frmMenu.ConvData(MaskEdit2.Text) = frmMenu.ConvData('01/01/1900')) then
      begin
          MaskEdit2.Clear;
          MaskEdit2.SetFocus;
          Abort;
      end;
  end;

  if (frmMenu.ConvData(MaskEdit1.Text) > frmMenu.ConvData(MaskEdit2.Text)) then
  begin
      MensagemAlerta('Data inicial maior do que a data final.');
      MaskEdit1.SetFocus;
      Abort;
  end;

  objRel := TrptItensPorVendaPar_view.Create(nil);

  try
      try
          objRel.SPREL_ITENS_POR_VENDA_PAR.Close;
          objRel.SPREL_ITENS_POR_VENDA_PAR.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.ConvInteiro(er2LkpLoja.Text);
          objRel.SPREL_ITENS_POR_VENDA_PAR.Parameters.ParamByName('@nCdTerceiroColab').Value := frmMenu.ConvInteiro(er2LkpVendedor.Text);
          objRel.SPREL_ITENS_POR_VENDA_PAR.Parameters.ParamByName('@dDtInicial').Value       := frmMenu.ConvData(MaskEdit1.Text);
          objRel.SPREL_ITENS_POR_VENDA_PAR.Parameters.ParamByName('@dDtFinal').Value         := frmMenu.ConvData(MaskEdit2.Text);
          objRel.SPREL_ITENS_POR_VENDA_PAR.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
          objRel.SPREL_ITENS_POR_VENDA_PAR.Parameters.ParamByName('@cFlgAtivo').Value        := RadioGroup1.ItemIndex;
          objRel.cmdPreparaTemp.Execute;
          objRel.SPREL_ITENS_POR_VENDA_PAR.ExecProc;

          objRel.qryResultado.Close;
          objRel.qryResultado.Open;

          {--envia filtros utilizados--}
          cFiltro := '';

          if (DBEdit1.Text <> '') then
            cFiltro := cFiltro + 'Loja: ' + DBEdit1.Text + '/ ';

          if (DBEdit2.Text <> '') then
            cFiltro := cFiltro + 'Vendedor: ' + DBEdit2.Text + '/ ';

          cFiltro := cFiltro  + 'Per�odo: ' + MaskEdit1.Text + ' � ' + MaskEdit2.Text;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := cFiltro;

          {--visualiza o relat�rio--}
          case (rgModeloImp.ItemIndex) of
          0 : objRel.QuickRep1.PreviewModal;
          1 : begin

                  frmMenu.mensagemUsuario('Exportando Planilha...');

                  ER2Excel.Celula['A1'].Mesclar('Y1');
                  ER2Excel.Celula['A2'].Mesclar('Y2');
                  ER2Excel.Celula['A3'].Mesclar('Y3');
                  ER2Excel.Celula['A4'].Mesclar('Y4');
                  ER2Excel.Celula['H5'].Mesclar('L5');
                  ER2Excel.Celula['M5'].Mesclar('T5');
                  ER2Excel.Celula['V5'].Mesclar('Z5');
                  ER2Excel.Celula['B6'].Mesclar('F6');

                  ER2Excel.Celula['A1'].Background := RGB(221, 221, 221);
                  ER2Excel.Celula['A2'].Background := RGB(221, 221, 221);
                  ER2Excel.Celula['A3'].Background := RGB(221, 221, 221);
                  ER2Excel.Celula['A4'].Background := RGB(221, 221, 221);


                  { -- inseri informa��es do cabe�alho -- }
                  ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                  ER2Excel.Celula['A2'].Text := 'Rel. Itens por Venda - Par';
                  ER2Excel.Celula['A3'].Text := 'Filtros: ' + cFiltro;
                  ER2Excel.Celula['H5'].Text := '* ------------ TOTAL GERAL DE VENDAS ------------*';
                  ER2Excel.Celula['M5'].Text := '* ----------------------------------------------------- TOTAL DE VENDAS -----------------------------------------------------*';
                  ER2Excel.Celula['V5'].Text := '* ------------------ TOTAL DE TROCAS ------------------*';
                  ER2Excel.Celula['A6'].Text := 'C�digo';
                  ER2Excel.Celula['B6'].Text := 'Vendedor';
                  ER2Excel.Celula['H6'].Text := 'Total Geral';
                  ER2Excel.Celula['I6'].Text := '%Part';
                  ER2Excel.Celula['J6'].Text := 'Qt.';
                  ER2Excel.Celula['K6'].Text := 'Itens';
                  ER2Excel.Celula['L6'].Text := 'Qt. Part';
                  ER2Excel.Celula['M6'].Text := 'Total Venda';
                  ER2Excel.Celula['N6'].Text := 'Qt. Vd';
                  ER2Excel.Celula['O6'].Text := 'Itens';
                  ER2Excel.Celula['P6'].Text := 'Qt. Par';
                  ER2Excel.Celula['Q6'].Text := 'Vd M�dia';
                  ER2Excel.Celula['R6'].Text := 'Itens Vd';
                  ER2Excel.Celula['S6'].Text := 'VM Item';
                  ER2Excel.Celula['T6'].Text := 'Par/Vd';
                  ER2Excel.Celula['V6'].Text := 'Tot Troca';
                  ER2Excel.Celula['W6'].Text := 'Qt. Tr';
                  ER2Excel.Celula['X6'].Text := 'Itens';
                  ER2Excel.Celula['Y6'].Text := 'Qt. Par';

                  iLinha := 7;

                  objRel.qryResultado.First;

                  while (not objRel.qryResultado.Eof) do
                  begin
                      ER2Excel.Celula['A' + IntToStr(iLinha)].Text := objRel.qryResultadonCdVendedor.AsString;
                      ER2Excel.Celula['B' + IntToStr(iLinha)].Text := objRel.qryResultadocNmVendedor.AsString; //'Vendedor';
                      ER2Excel.Celula['H' + IntToStr(iLinha)].Text := objRel.qryResultadonValPedidosGeral.AsString; //'Total Geral';
                      ER2Excel.Celula['I' + IntToStr(iLinha)].Text := objRel.qryResultadonPercentParticip.AsString; //'%Part';
                      ER2Excel.Celula['J' + IntToStr(iLinha)].Text := objRel.qryResultadoiTotalPedidosGeral.AsString; //'Qt.';
                      ER2Excel.Celula['K' + IntToStr(iLinha)].Text := objRel.qryResultadoiQtdItensGeral.AsString; //'Itens';
                      ER2Excel.Celula['L' + IntToStr(iLinha)].Text := objRel.qryResultadoiQtdParGeral.AsString; //'Qt. Part';
                      ER2Excel.Celula['M' + IntToStr(iLinha)].Text := objRel.qryResultadonValPedidosVenda.AsString; //'Total Venda';
                      ER2Excel.Celula['N' + IntToStr(iLinha)].Text := objRel.qryResultadoiQtdVenda.AsString; //'Qt. Vd';
                      ER2Excel.Celula['O' + IntToStr(iLinha)].Text := objRel.qryResultadoiQtdItensVenda.AsString; //'Itens';
                      ER2Excel.Celula['P' + IntToStr(iLinha)].Text := objRel.qryResultadoiQtdParVendas.AsString; //'Qt. Par';
                      ER2Excel.Celula['Q' + IntToStr(iLinha)].Text := objRel.qryResultadonValMedio.AsString; //'Vd M�dia';
                      ER2Excel.Celula['R' + IntToStr(iLinha)].Text := objRel.qryResultadonItemPorVenda.AsString; //'Itens Vd';
                      ER2Excel.Celula['S' + IntToStr(iLinha)].Text := objRel.qryResultadonValItemMedio.AsString; //'VM Item';
                      ER2Excel.Celula['T' + IntToStr(iLinha)].Text := objRel.qryResultadonParPorVenda.AsString; //'Par/Vd';
                      ER2Excel.Celula['V' + IntToStr(iLinha)].Text := objRel.qryResultadonValPedidosTroca.AsString; //'Tot Troca';
                      ER2Excel.Celula['W' + IntToStr(iLinha)].Text := objRel.qryResultadoiTotalTrocas.AsString; //'Qt. Tr';
                      ER2Excel.Celula['X' + IntToStr(iLinha)].Text := objRel.qryResultadoiQtdItensTrocas.AsString; //'Itens';
                      ER2Excel.Celula['Y' + IntToStr(iLinha)].Text := objRel.qryResultadoiQtdParTrocas.AsString; //'Qt. Par';

                      ER2Excel.Celula['G' + IntToStr(iLinha)].Text := 'R$';
                      ER2Excel.Celula['U' + IntToStr(iLinha)].Text := 'R$';
                      
                      objRel.qryResultado.Next;

                      Inc(iLinha);

                  end;

                  { -- exporta planilha e limpa result do componente -- }
                  ER2Excel.ExportXLS;
                  ER2Excel.CleanupInstance;

                  frmMenu.mensagemUsuario('');

              end;
          end;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel) ;
  end;
end;

procedure TrptItensPorVendaPar.FormCreate(Sender: TObject);
begin
  inherited;

  cmdTempDepto.Execute;

  qryTempDepto.Open;
end;

procedure TrptItensPorVendaPar.qryTempDeptoBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (Trim(qryTempDeptocNmDepartamento.Value) = '') then
  begin
      MensagemAlerta('Informe o departamento.');

      Abort;
  end;
end;

procedure TrptItensPorVendaPar.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : integer;
begin
  inherited;

  if (Key = VK_F4) then
  begin
      nPK := frmLookup_Padrao.ExecutaConsulta2(129,'NOT EXISTS (SELECT 1 FROM #tempDepto WHERE nCdTempDepto = nCdDepartamento)');

      if (nPK <> 0) then
      begin
          if ((qryTempDepto.State <> dsInsert) and (qryTempDepto.State <> dsEdit)) then
              qryTempDepto.Insert;

          qryTempDeptonCdTempDepto.Value := nPK;
      end;
  end;
end;

initialization
    RegisterClass(TrptItensPorVendaPar);

end.
