object frmPdvDesconto: TfrmPdvDesconto
  Left = 541
  Top = 223
  AutoScroll = False
  BorderIcons = [biSystemMenu]
  Caption = 'Desconto'
  ClientHeight = 279
  ClientWidth = 303
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 28
    Top = 108
    Width = 106
    Height = 21
    Alignment = taRightJustify
    Caption = 'Valor Desconto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 52
    Top = 68
    Width = 82
    Height = 21
    Alignment = taRightJustify
    Caption = '% Desconto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 28
    Top = 28
    Width = 106
    Height = 21
    Alignment = taRightJustify
    Caption = 'Valor da Venda'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label4: TLabel
    Left = 41
    Top = 188
    Width = 93
    Height = 21
    Alignment = taRightJustify
    Caption = 'Valor L'#237'quido'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object Label5: TLabel
    Left = 21
    Top = 148
    Width = 113
    Height = 21
    Alignment = taRightJustify
    Caption = 'Valor Acr'#233'scimo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
  end
  object edtValDesconto: TcxCurrencyEdit
    Left = 144
    Top = 96
    Width = 145
    Height = 33
    BeepOnEnter = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.EditFormat = ',0.00;-,0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebs3D
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    TabOrder = 2
    OnExit = edtValDescontoExit
    OnKeyDown = FormKeyDown
    OnKeyPress = edtValDescontoKeyPress
  end
  object edtPercent: TcxCurrencyEdit
    Left = 144
    Top = 56
    Width = 73
    Height = 33
    BeepOnEnter = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = ',0.00%;-,0.00%'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebs3D
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    TabOrder = 1
    OnExit = edtPercentExit
    OnKeyDown = FormKeyDown
    OnKeyPress = edtPercentKeyPress
  end
  object edtValVenda: TcxCurrencyEdit
    Left = 144
    Top = 16
    Width = 145
    Height = 33
    BeepOnEnter = False
    Enabled = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebs3D
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    TabOrder = 0
    OnKeyDown = FormKeyDown
  end
  object edtValliquido: TcxCurrencyEdit
    Left = 144
    Top = 176
    Width = 145
    Height = 33
    BeepOnEnter = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.EditFormat = ',0.00;-,0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebs3D
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    TabOrder = 3
    OnKeyDown = FormKeyDown
    OnKeyPress = edtValliquidoKeyPress
  end
  object edtValAcrescimo: TcxCurrencyEdit
    Left = 144
    Top = 136
    Width = 145
    Height = 33
    BeepOnEnter = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.EditFormat = ',0.00;-,0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebs3D
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -19
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    TabOrder = 4
    OnExit = edtValAcrescimoExit
    OnKeyDown = FormKeyDown
    OnKeyPress = edtValAcrescimoKeyPress
  end
  object btSair: TcxButton
    Left = 144
    Top = 224
    Width = 145
    Height = 41
    Caption = 'F12 - Sair'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
    TabStop = False
    OnClick = btSairClick
    Glyph.Data = {
      06030000424D06030000000000003600000028000000100000000F0000000100
      180000000000D002000000000000000000000000000000000000C6D6DEC6D6DE
      C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE0000000000000000000000000000000000
      00000000000000000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE00
      0000084A21084A21084A21084A21084A21084A21084A21000000000000000000
      00000000000000000000000000000000000000000000000018A54A18A54A18A5
      4A18A54A084A21000000000000D6A58CD6A58CD6A58CD6A58CD6A58CD6A58CD6
      A58C00000052B5F700000018A54A18A54A18A54A084A21000000000000EFFFFF
      FFEFDEFFEFDE00000000000000000000000000000000FFFF52B5F700000018A5
      4A18A54A084A21000000000000EFFFFFFFEFDEFFEFDE00000000FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF52B5F700000018A54A084A21000000000000EFFFFF
      FFEFDEFFEFDE000000EFFFFFEFFFFFEFFFFFEFFFFF00FFFF00FFFFEFFFFF0000
      0018A54A084A21000000000000EFFFFFFFEFDEFFEFDE00000000000000000000
      000000000000FFFFEFFFFF00000018A54A18A54A084A21000000000000EFFFFF
      FFEFDEFFEFDEFFEFDEFFEFDEFFEFDED6A58C000000EFFFFF00000018A54A18A5
      4A18A54A084A21000000000000EFFFFFFFEFDEFFEFDEFFEFDEA56B5AA56B5AD6
      A58C00000000000018A54A18A54A18A54A18A54A084A21000000000000EFFFFF
      FFEFDEFFEFDEFFEFDEFFEFDEFFEFDED6A58C00000018A54A18A54A18A54A18A5
      4A316363316363000000000000EFFFFFFFEFDEFFEFDEFFEFDEA56B5AA56B5AD6
      A58C0000009CD6B59CD6B59CD6B59CD6B5316363000000C6D6DE000000EFFFFF
      FFEFDEFFEFDEFFEFDEFFEFDEFFEFDED6A58C0000000000000000000000000000
      00000000C6D6DEC6D6DE000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEF
      FFFF000000C6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE000000000000
      000000000000000000000000000000000000000000C6D6DEC6D6DEC6D6DEC6D6
      DEC6D6DEC6D6DEC6D6DE}
    LookAndFeel.NativeStyle = True
  end
  object qryMetaDesconto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      
        'SELECT ISNULL(SUM(MetaDescontoUtil.nValDescontoUtil),0) nValDesc' +
        'ontoUtil'
      '      ,MetaVendedorDesconto.nValCotaDesconto'
      '  FROM Usuario'
      
        '       INNER JOIN Terceiro             ON Terceiro.nCdTerceiro  ' +
        '                   = Usuario.nCdTerceiroResponsavel'
      
        '       INNER JOIN MetaVendedorDesconto ON MetaVendedorDesconto.n' +
        'CdTerceiroColab    = Terceiro.nCdTerceiro'
      
        '       INNER JOIN MetaDescontoMes      ON MetaDescontoMes.nCdMet' +
        'aDescontoMes       = MetaVendedorDesconto.nCdMetaDescontoMes'
      
        '       LEFT  JOIN MetaDescontoUtil     ON MetaDescontoUtil.nCdMe' +
        'taVendedorDesconto = MetaVendedorDesconto.nCdMetaVendedorDescont' +
        'o'
      ' WHERE Usuario.nCdUsuario      = :nPK'
      '   AND MetaDescontoMes.nCdLoja = :nCdLoja'
      '   AND MetaDescontoMes.iMes    = MONTH(GETDATE())'
      '   AND MetaDescontoMes.iAno    = YEAR(GETDATE())'
      ' GROUP BY MetaVendedorDesconto.nValCotaDesconto')
    Left = 32
    Top = 232
    object qryMetaDescontonValDescontoUtil: TBCDField
      FieldName = 'nValDescontoUtil'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryMetaDescontonValCotaDesconto: TBCDField
      FieldName = 'nValCotaDesconto'
      Precision = 12
      Size = 2
    end
  end
end
