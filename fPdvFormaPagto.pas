unit fPdvFormaPagto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, DB, ADODB, GridsEh, DBGridEh, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGrid;

type
  TfrmPdvFormaPagto = class(TForm)
    qryFormaPagto: TADOQuery;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    dsFormaPagto: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1nCdFormaPagto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmFormaPagto: TcxGridDBColumn;
    qryFormaPagtocFlgTEF: TIntegerField;
    function SelecionaFormaPagto(): integer ;
    procedure Grid1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private

    { Private declarations }
    nCdMaiorCodigo     : integer ;
    dDtUltAtualizacao  : TDateTime ;
    nCdFormaPagtoSelec : integer ;

  public
    cFlgLiqCrediario : integer ;
    nCdFormaPagtoPre : integer ;
    bSomenteCartao   : boolean ;
    { Public declarations }
  end;

var
  frmPdvFormaPagto: TfrmPdvFormaPagto;

implementation

{$R *.dfm}

function TfrmPdvFormaPagto.SelecionaFormaPagto(): integer ;
begin

    //if (DateTimeToStr(dDtUltAtualizacao) = '30/12/1899') or (StrToInt(FormatDateTime('hh',(Now() - dDtUltAtualizacao))) >= 3) then
    //begin

        qryFormaPagto.Close ;
        qryFormaPagto.Parameters.ParamByName('cFlgLiqCrediario').Value  := cFlgLiqCrediario ;
        qryFormaPagto.Parameters.ParamByName('cFlgSomenteCartao').Value := 0 ;

        if (bSomenteCartao) then
            qryFormaPagto.Parameters.ParamByName('cFlgSomenteCartao').Value := 1 ;

        qryFormaPagto.Open ;

        qryFormaPagto.First ;

        if (ncdFormaPagtoPre > 0) then
        begin

            while (nCdFormaPagtoPre <> qryFormaPagtonCdFormaPagto.Value) and (not qryFormaPagto.Eof) do
            begin
                qryFormaPagto.next ;
            end ;

        end ;

        dDtUltAtualizacao := Now() ;

    //end ;

    Self.ShowModal();

    Result := qryFormaPagtonCdFormaPagto.Value ;

end;

procedure TfrmPdvFormaPagto.Grid1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

    case Key of
        VK_RETURN : close ;
    end ;

end;

procedure TfrmPdvFormaPagto.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    case key of
        vk_return : close ;
    end ;

end;

procedure TfrmPdvFormaPagto.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
    close ;
end;

procedure TfrmPdvFormaPagto.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    case key of
        vk_return : close ;
    end ;

end;

end.
