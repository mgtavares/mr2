unit fCobranca_VisaoContato_Titulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, GridsEh, DBGridEh, ImgList,
  ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping;

type
  TfrmCobranca_VisaoContato_Titulos = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryTitulos: TADOQuery;
    dsTitulos: TDataSource;
    qryTitulosnCdTitulo: TIntegerField;
    qryTituloscNrTit: TStringField;
    qryTitulosiParcela: TIntegerField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTitulosnValTit: TBCDField;
    qryTitulosnSaldoTit: TBCDField;
    qryTitulosdDtLiq: TDateTimeField;
    qryTituloscNmEspTit: TStringField;
    qryTitulosnCdLojaTit: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ExibeTitulos(nCdItemListaCobranca:integer) ;
  end;

var
  frmCobranca_VisaoContato_Titulos: TfrmCobranca_VisaoContato_Titulos;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmCobranca_VisaoContato_Titulos }

procedure TfrmCobranca_VisaoContato_Titulos.ExibeTitulos(
  nCdItemListaCobranca: integer);
begin

    qryTitulos.Close;
    PosicionaQuery(qryTitulos, intToStr(nCdItemListaCobranca)) ;

    DBGridEh1.Columns[9].Visible := (frmMenu.LeParametro('VAREJO') = 'S') ;

    Self.ShowModal;

end;

end.
