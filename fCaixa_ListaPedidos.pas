unit fCaixa_ListaPedidos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxCheckBox, jpeg, ExtCtrls,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid;

type
  TfrmCaixa_ListaPedidos = class(TForm)
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgMarcado: TcxGridDBColumn;
    cxGrid1DBTableView1nCdCrediario: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1iDiasAtraso: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1nValJuro: TcxGridDBColumn;
    cxGrid1DBTableView1nValDesconto: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLoja: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgEntrada: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Image2: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_ListaPedidos: TfrmCaixa_ListaPedidos;

implementation

{$R *.dfm}

end.
