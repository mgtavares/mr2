inherited frmListaCobranca_Itens_NovaLista: TfrmListaCobranca_Itens_NovaLista
  Left = 280
  Top = 278
  Height = 137
  BorderIcons = [biSystemMenu]
  Caption = 'Gerar Nova Lista Cobran'#231'a'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 72
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 51
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'Analista Cobran'#231'a'
  end
  inherited ToolBar1: TToolBar
    ButtonWidth = 88
    TabOrder = 1
    inherited ToolButton1: TToolButton
      Caption = '&Gerar Lista'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 88
    end
    inherited ToolButton2: TToolButton
      Left = 96
    end
  end
  object edtUsuario: TMaskEdit [3]
    Left = 99
    Top = 43
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnChange = edtUsuarioChange
    OnExit = edtUsuarioExit
    OnKeyDown = edtUsuarioKeyDown
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 166
    Top = 43
    Width = 603
    Height = 21
    DataField = 'cNmUsuario'
    DataSource = DataSource1
    TabOrder = 2
  end
  object CheckBox1: TCheckBox [5]
    Left = 99
    Top = 75
    Width = 198
    Height = 17
    Caption = 'N'#227'o atribibuir analista de cobran'#231'a'
    TabOrder = 3
    OnClick = CheckBox1Click
  end
  inherited ImageList1: TImageList
    Left = 368
    Top = 32
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Usuario.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '  FROM USUARIO'
      ' WHERE nCdStatus            = 1'
      '   AND cFlgAnalistaCobranca = 1'
      '   AND nCdUsuario = :nPK')
    Left = 304
    Top = 32
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryUsuario
    Left = 336
    Top = 32
  end
end
