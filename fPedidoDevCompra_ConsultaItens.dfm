inherited frmPedidoDevCompra_ConsultaItens: TfrmPedidoDevCompra_ConsultaItens
  Left = 161
  Top = 74
  Width = 963
  Height = 599
  Caption = 'Consulta de Recebimentos'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 947
    Height = 532
  end
  inherited ToolBar1: TToolBar
    Width = 947
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 947
    Height = 532
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 528
    ClientRectLeft = 4
    ClientRectRight = 943
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Consulta por Produto'
      ImageIndex = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 939
        Height = 116
        Align = alTop
        Caption = ' Filtros '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label6: TLabel
          Tag = 1
          Left = 228
          Top = 96
          Width = 18
          Height = 13
          Alignment = taRightJustify
          Caption = 'at'#233
        end
        object Label3: TLabel
          Tag = 1
          Left = 9
          Top = 96
          Width = 132
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo de Recebimento'
        end
        object Label5: TLabel
          Tag = 1
          Left = 99
          Top = 24
          Width = 42
          Height = 13
          Alignment = taRightJustify
          Caption = 'Produto'
        end
        object Label1: TLabel
          Tag = 1
          Left = 15
          Top = 72
          Width = 126
          Height = 13
          Alignment = taRightJustify
          Caption = 'Quantidade a Devolver'
        end
        object Label2: TLabel
          Tag = 1
          Left = 69
          Top = 48
          Width = 72
          Height = 13
          Alignment = taRightJustify
          Caption = 'UM de Compra'
        end
        object edtDtFinal: TMaskEdit
          Left = 256
          Top = 88
          Width = 81
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 3
          Text = '  /  /    '
          OnChange = edtDtFinalChange
        end
        object edtDtInicial: TMaskEdit
          Left = 144
          Top = 88
          Width = 81
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 2
          Text = '  /  /    '
          OnChange = edtDtInicialChange
        end
        object edtProduto: TMaskEdit
          Left = 144
          Top = 16
          Width = 129
          Height = 21
          EditMask = '#########;1; '
          MaxLength = 9
          TabOrder = 0
          Text = '         '
          OnChange = edtProdutoChange
          OnExit = edtProdutoExit
          OnKeyDown = edtProdutoKeyDown
        end
        object edtQtde: TMaskEdit
          Left = 144
          Top = 64
          Width = 71
          Height = 21
          EditMask = '#########;1; '
          MaxLength = 9
          TabOrder = 1
          Text = '         '
          OnChange = edtQtdeChange
          OnEnter = edtQtdeEnter
        end
        object DBEdit1: TDBEdit
          Tag = 1
          Left = 280
          Top = 16
          Width = 553
          Height = 21
          DataField = 'cNmProduto'
          DataSource = dsProduto
          TabOrder = 5
        end
        object DBEdit2: TDBEdit
          Tag = 1
          Left = 224
          Top = 64
          Width = 49
          Height = 21
          DataField = 'cUnidadeMedida'
          DataSource = dsProduto
          TabOrder = 6
        end
        object DBEdit3: TDBEdit
          Tag = 1
          Left = 144
          Top = 40
          Width = 129
          Height = 21
          DataField = 'cUnidadeMedidaCompra'
          DataSource = dsProduto
          TabOrder = 7
        end
        object cxButton1: TcxButton
          Left = 344
          Top = 80
          Width = 137
          Height = 29
          Caption = 'Consultar'
          TabOrder = 4
          OnClick = cxButton1Click
          Glyph.Data = {
            36030000424D360300000000000036000000280000000F000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
            393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
            00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
            F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
            83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
            68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
            FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
            9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
            8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
            D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
            B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
            5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
            A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
            9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
            488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
            FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
            FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
            FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
          LookAndFeel.NativeStyle = True
        end
      end
      object cxGrid1: TcxGrid
        Left = 0
        Top = 116
        Width = 939
        Height = 388
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnDblClick = cxGrid1DBTableView1DblClick
          DataController.DataSource = dsConsultaReceb
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GridLineColor = clSilver
          OptionsView.GridLines = glVertical
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGrid1DBTableView1nCdRecebimento: TcxGridDBColumn
            Caption = 'Receb.'
            DataBinding.FieldName = 'nCdRecebimento'
          end
          object cxGrid1DBTableView1dDtReceb: TcxGridDBColumn
            Caption = 'Data Receb.'
            DataBinding.FieldName = 'dDtReceb'
          end
          object cxGrid1DBTableView1cNrDocto: TcxGridDBColumn
            Caption = 'Nr. NF'
            DataBinding.FieldName = 'cNrDocto'
            Width = 66
          end
          object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
            Caption = 'Terceiro'
            DataBinding.FieldName = 'cNmTerceiro'
            Width = 230
          end
          object cxGrid1DBTableView1nQtde: TcxGridDBColumn
            Caption = 'Qtde Rec.'
            DataBinding.FieldName = 'nQtde'
          end
          object cxGrid1DBTableView1nValUnitario: TcxGridDBColumn
            Caption = 'Val. Unit'#225'rio Rec.'
            DataBinding.FieldName = 'nValUnitario'
            Width = 105
          end
          object cxGrid1DBTableView1nPercIPI: TcxGridDBColumn
            Caption = '% IPI'
            DataBinding.FieldName = 'nPercIPI'
            Width = 59
          end
          object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
            Caption = 'Pedido Compra'
            DataBinding.FieldName = 'nCdPedido'
            Width = 103
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Consulta por Documento de Entrada'
      ImageIndex = 1
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 939
        Height = 73
        Align = alTop
        Caption = ' Filtros '
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object Label4: TLabel
          Tag = 1
          Left = 228
          Top = 48
          Width = 18
          Height = 13
          Alignment = taRightJustify
          Caption = 'at'#233
        end
        object Label7: TLabel
          Tag = 1
          Left = 9
          Top = 48
          Width = 132
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo de Recebimento'
        end
        object Label9: TLabel
          Tag = 1
          Left = 39
          Top = 24
          Width = 102
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero NF Entrada'
        end
        object edtDtFinalNF: TMaskEdit
          Left = 256
          Top = 40
          Width = 81
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 2
          Text = '  /  /    '
          OnChange = edtDtFinalChange
        end
        object edtDtInicialNF: TMaskEdit
          Left = 144
          Top = 40
          Width = 81
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 1
          Text = '  /  /    '
          OnChange = edtDtInicialChange
        end
        object edtNumNF: TMaskEdit
          Left = 144
          Top = 16
          Width = 71
          Height = 21
          EditMask = '#########;1; '
          MaxLength = 9
          TabOrder = 0
          Text = '         '
          OnChange = edtQtdeChange
          OnEnter = edtQtdeEnter
        end
        object cxButton2: TcxButton
          Left = 344
          Top = 32
          Width = 137
          Height = 29
          Caption = 'Consultar'
          TabOrder = 3
          OnClick = cxButton2Click
          Glyph.Data = {
            36030000424D360300000000000036000000280000000F000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
            393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
            00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
            F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
            83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
            68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
            FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
            9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
            8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
            D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
            B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
            5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
            A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
            9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
            488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
            FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
            FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
            FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
          LookAndFeel.NativeStyle = True
        end
      end
      object cxGrid2: TcxGrid
        Left = 0
        Top = 73
        Width = 939
        Height = 431
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        object cxGridDBTableView1: TcxGridDBTableView
          OnDblClick = cxGridDBTableView1DblClick
          DataController.DataSource = dsConsultaRecebNF
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GridLineColor = clSilver
          OptionsView.GridLines = glVertical
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBColumn1: TcxGridDBColumn
            Caption = 'Receb.'
            DataBinding.FieldName = 'nCdRecebimento'
            Width = 77
          end
          object cxGridDBColumn2: TcxGridDBColumn
            Caption = 'Data Receb.'
            DataBinding.FieldName = 'dDtReceb'
            Width = 95
          end
          object cxGridDBColumn3: TcxGridDBColumn
            Caption = 'Nr. NF'
            DataBinding.FieldName = 'cNrDocto'
            Width = 66
          end
          object cxGridDBTableView1cCdProduto: TcxGridDBColumn
            Caption = 'C'#243'd. Prod.'
            DataBinding.FieldName = 'cCdProduto'
            Width = 89
          end
          object cxGridDBTableView1cNmItem: TcxGridDBColumn
            Caption = 'Descri'#231#227'o Produto'
            DataBinding.FieldName = 'cNmItem'
            Width = 187
          end
          object cxGridDBTableView1cUnidadeMedida: TcxGridDBColumn
            Caption = 'U.M'
            DataBinding.FieldName = 'cUnidadeMedida'
            Width = 47
          end
          object cxGridDBColumn5: TcxGridDBColumn
            Caption = 'Qtde Rec.'
            DataBinding.FieldName = 'nQtde'
            Width = 85
          end
          object cxGridDBColumn6: TcxGridDBColumn
            Caption = 'Val. Unit'#225'rio Rec.'
            DataBinding.FieldName = 'nValUnitario'
            Width = 105
          end
          object cxGridDBColumn7: TcxGridDBColumn
            Caption = '% IPI'
            DataBinding.FieldName = 'nPercIPI'
            Width = 59
          end
          object cxGridDBColumn8: TcxGridDBColumn
            Caption = 'Pedido Compra'
            DataBinding.FieldName = 'nCdPedido'
            Width = 103
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 584
    Top = 240
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTipoPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmProduto'
      '      ,cUnidadeMedida'
      
        '      ,CASE WHEN nFatorCompra > 1 THEN cUnidadeMedidaCompra + '#39' ' +
        'c/ '#39'+ Convert(varchar,Convert(int,nFatorCompra))'
      '            ELSE cUnidadeMedidaCompra'
      '       END cUnidadeMedidaCompra'
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM GrupoProdutoTipoPedido GPTP'
      '               WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto'
      '                 AND GPTP.nCdTipoPedido   = :nCdTipoPedido)'
      '   AND NOT EXISTS(SELECT 1'
      '                    FROM Produto Produto2'
      
        '                   WHERE Produto2.nCdProdutoPai = Produto.nCdPro' +
        'duto)'
      '')
    Left = 520
    Top = 205
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutocUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryProdutocUnidadeMedidaCompra: TStringField
      FieldName = 'cUnidadeMedidaCompra'
      ReadOnly = True
      Size = 37
    end
  end
  object dsProduto: TDataSource
    DataSet = qryProduto
    Left = 520
    Top = 240
  end
  object qryConsultaReceb: TADOQuery
    Connection = frmMenu.Connection
    EnableBCD = False
    Parameters = <
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1901'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1901'
      end
      item
        Name = 'nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @cDtInicial VARCHAR(10)'
      '       ,@cDtFinal   VARCHAR(10)'
      '       ,@nCdProduto int'
      '       ,@nCdEmpresa int'
      '       ,@nCdLoja    int'
      ''
      'Set @cDtInicial = :dDtInicial'
      'Set @cDtFinal   = :dDtFinal'
      'Set @nCdProduto = :nCdProduto'
      'Set @nCdEmpresa = :nCdEmpresa'
      'Set @nCdLoja    = :nCdLoja'
      ''
      'SELECT ItemRecebimento.nCdRecebimento'
      '      ,Recebimento.dDtReceb'
      '      ,Recebimento.cNrDocto'
      '      ,Terceiro.cNmTerceiro'
      '      ,ItemRecebimento.nQtde'
      
        '      ,CASE WHEN ItemRecebimento.nValUnitarioEsp = 0 THEN ItemRe' +
        'cebimento.nValUnitario'
      '            ELSE ItemRecebimento.nValUnitarioPed'
      '       END nValUnitario'
      '      ,ItemRecebimento.nValUnitarioEsp'
      '      ,ItemRecebimento.nPercIPI'
      '      ,ItemPedido.nCdPedido'
      '      ,ItemRecebimento.nCdTipoItemPed'
      '      ,ItemRecebimento.nCdItemRecebimento'
      '      ,cChaveNfe'
      '      ,ItemRecebimento.nCdProduto'
      '  FROM Recebimento'
      
        '       INNER JOIN Terceiro        ON Terceiro.nCdTerceiro       ' +
        '    = Recebimento.nCdTerceiro'
      
        '       INNER JOIN ItemRecebimento ON ItemRecebimento.nCdRecebime' +
        'nto = Recebimento.nCdRecebimento'
      
        '       LEFT  JOIN ItemPedido      ON ItemPedido.nCdItemPedido   ' +
        '    = ItemRecebimento.nCdItemPedido'
      ' WHERE ItemRecebimento.nCdTipoItemPed IN (2,4,5,6)'
      '   AND Recebimento.nCdEmpresa          = @nCdEmpresa'
      
        '   AND ((Recebimento.nCdLoja           = @nCdLoja) or (@nCdLoja ' +
        '= 0))'
      
        '   AND Recebimento.dDtReceb           >= Convert(DATETIME,@cDtIn' +
        'icial,103)'
      
        '   AND Recebimento.dDtReceb            < Convert(DATETIME,@cDtFi' +
        'nal,103) + 1'
      '   AND ItemRecebimento.nCdProduto      = @nCdProduto'
      '   AND Recebimento.nCdTabStatusReceb  IN (4,5,6,7)'
      ' ORDER BY 1 DESC')
    Left = 552
    Top = 208
    object qryConsultaRecebnCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryConsultaRecebdDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
    end
    object qryConsultaRecebcNrDocto: TStringField
      FieldName = 'cNrDocto'
      FixedChar = True
      Size = 17
    end
    object qryConsultaRecebcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryConsultaRecebnQtde: TBCDField
      FieldName = 'nQtde'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryConsultaRecebnPercIPI: TBCDField
      FieldName = 'nPercIPI'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryConsultaRecebnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryConsultaRecebnCdTipoItemPed: TIntegerField
      FieldName = 'nCdTipoItemPed'
    end
    object qryConsultaRecebnCdItemRecebimento: TAutoIncField
      FieldName = 'nCdItemRecebimento'
      ReadOnly = True
    end
    object qryConsultaRecebnValUnitario: TFloatField
      FieldName = 'nValUnitario'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
    end
    object qryConsultaRecebnValUnitarioEsp: TFloatField
      FieldName = 'nValUnitarioEsp'
      DisplayFormat = '#,##0.00'
    end
    object qryConsultaRecebcChaveNfe: TStringField
      FieldName = 'cChaveNfe'
      Size = 100
    end
    object qryConsultaRecebnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
  end
  object dsConsultaReceb: TDataSource
    DataSet = qryConsultaReceb
    Left = 552
    Top = 240
  end
  object SP_INCLUI_ITEM_DEVOLUCAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_INCLUI_ITEM_DEVOLUCAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNmItem'
        Attributes = [paNullable]
        DataType = ftString
        Size = 150
        Value = Null
      end
      item
        Name = '@cSiglaUnidadeMedida'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@nQtdePed'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nValUnitario'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 16
        Value = Null
      end
      item
        Name = '@nValUnitarioEsp'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 16
        Value = Null
      end
      item
        Name = '@nPercIPI'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nCdItem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTipoItemPed'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cUFOrigem'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end>
    Left = 584
    Top = 205
  end
  object qryConsultaRecebNF: TADOQuery
    Connection = frmMenu.Connection
    EnableBCD = False
    Parameters = <
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1901'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1901'
      end
      item
        Name = 'cNrDocto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @cDtInicial  VARCHAR(10)'
      '       ,@cDtFinal    VARCHAR(10)'
      '       ,@nCdEmpresa  int'
      '       ,@cNrDocto    int'
      '       ,@nCdTerceiro int'
      '       ,@nCdLoja     int'
      ''
      'Set @cDtInicial  = :dDtInicial'
      'Set @cDtFinal    = :dDtFinal'
      'Set @cNrDocto    = :cNrDocto'
      'Set @nCdEmpresa  = :nCdEmpresa'
      'Set @nCdTerceiro = :nCdTerceiro'
      'Set @nCdLoja     = :nCdLoja'
      ''
      'SELECT ItemRecebimento.nCdRecebimento'
      '      ,Recebimento.dDtReceb'
      '      ,Recebimento.cNrDocto'
      '      ,Terceiro.cNmTerceiro'
      '      ,ItemRecebimento.cCdProduto'
      '      ,ItemRecebimento.cNmItem'
      '      ,ItemRecebimento.cUnidadeMedida'
      '      ,ItemRecebimento.nQtde'
      
        '      ,CASE WHEN ItemRecebimento.nValUnitarioEsp = 0 THEN ItemRe' +
        'cebimento.nValUnitario'
      '            ELSE ItemRecebimento.nValUnitarioPed'
      '       END nValUnitario'
      '      ,ItemRecebimento.nValUnitarioEsp'
      '      ,ItemRecebimento.nPercIPI'
      '      ,ItemPedido.nCdPedido'
      '      ,ItemRecebimento.nCdTipoItemPed'
      '      ,ItemRecebimento.nCdItemRecebimento'
      '      ,Recebimento.cUFOrigemNF'
      '      ,cChaveNfe'
      '      ,ItemRecebimento.nCdProduto'
      '  FROM Recebimento'
      
        '       INNER JOIN Terceiro        ON Terceiro.nCdTerceiro       ' +
        '    = Recebimento.nCdTerceiro'
      
        '       INNER JOIN ItemRecebimento ON ItemRecebimento.nCdRecebime' +
        'nto = Recebimento.nCdRecebimento'
      
        '       LEFT  JOIN ItemPedido      ON ItemPedido.nCdItemPedido   ' +
        '    = ItemRecebimento.nCdItemPedido'
      ' WHERE ItemRecebimento.nCdTipoItemPed IN (2,4,5,6)'
      '   AND Recebimento.nCdEmpresa          = @nCdEmpresa'
      
        '   AND ((Recebimento.nCdLoja           = @nCdLoja) or (@nCdLoja ' +
        '= 0))'
      
        '   AND Recebimento.dDtReceb           >= Convert(DATETIME,@cDtIn' +
        'icial,103)'
      
        '   AND Recebimento.dDtReceb            < Convert(DATETIME,@cDtFi' +
        'nal,103) + 1'
      '   AND Recebimento.nCdTabStatusReceb  IN (4,5,6,7)'
      '   AND Recebimento.nCdTerceiro         = @nCdTerceiro'
      
        '   AND ((Recebimento.cNrDocto = Convert(VARCHAR,@cNrDocto)) OR (' +
        '@cNrDocto = 0))'
      
        ' ORDER BY ItemRecebimento.nCdRecebimento, ItemRecebimento.cNmIte' +
        'm')
    Left = 488
    Top = 208
    object qryConsultaRecebNFnCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryConsultaRecebNFdDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
    end
    object qryConsultaRecebNFcNrDocto: TStringField
      FieldName = 'cNrDocto'
      FixedChar = True
      Size = 17
    end
    object qryConsultaRecebNFcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryConsultaRecebNFcCdProduto: TStringField
      FieldName = 'cCdProduto'
      Size = 15
    end
    object qryConsultaRecebNFcNmItem: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryConsultaRecebNFcUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryConsultaRecebNFnQtde: TFloatField
      FieldName = 'nQtde'
      DisplayFormat = '#,##0.00'
    end
    object qryConsultaRecebNFnValUnitario: TFloatField
      FieldName = 'nValUnitario'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
    end
    object qryConsultaRecebNFnValUnitarioEsp: TFloatField
      FieldName = 'nValUnitarioEsp'
      DisplayFormat = '#,##0.00'
    end
    object qryConsultaRecebNFnPercIPI: TFloatField
      FieldName = 'nPercIPI'
      DisplayFormat = '#,##0.00'
    end
    object qryConsultaRecebNFnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryConsultaRecebNFnCdTipoItemPed: TIntegerField
      FieldName = 'nCdTipoItemPed'
    end
    object qryConsultaRecebNFnCdItemRecebimento: TIntegerField
      FieldName = 'nCdItemRecebimento'
    end
    object qryConsultaRecebNFcUFOrigemNF: TStringField
      FieldName = 'cUFOrigemNF'
      FixedChar = True
      Size = 2
    end
    object qryConsultaRecebNFcChaveNfe: TStringField
      FieldName = 'cChaveNfe'
      Size = 100
    end
    object qryConsultaRecebNFnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
  end
  object dsConsultaRecebNF: TDataSource
    DataSet = qryConsultaRecebNF
    Left = 488
    Top = 240
  end
  object qrySaldoItemDevoluc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTipoPedido'
        Size = -1
        Value = Null
      end
      item
        Name = 'cDoctoCompra'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'nCdPedido'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdProduto'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdTipoPedido int'
      '       ,@cDoctoCompra  varchar(100)'
      '       ,@nCdPedido     int'
      '       ,@nCdProdtuto   int'
      ''
      'SET @nCdTipoPedido = :nCdTipoPedido'
      'SET @cDoctoCompra  = :cDoctoCompra'
      'SET @nCdPedido     = :nCdPedido'
      'SET @nCdProdtuto   = :nCdProduto'
      ''
      'SELECT SUM(nQtdePed) nQtdePed'
      '  FROM ItemPedido'
      
        '       INNER JOIN Pedido ON Pedido.nCdPedido = ItemPedido.nCdPed' +
        'ido'
      ' WHERE nCdTabStatusPed  <> 10'
      '   AND Pedido.nCdPedido <> @nCdPedido'
      '   AND nCdProduto       =  @nCdProdtuto'
      '   AND nCdTipoPedido    =  @nCdTipoPedido'
      
        '   AND (cDoctoCompra    =  @cDoctoCompra AND @cDoctoCompra <> '#39#39 +
        ')'
      '')
    Left = 460
    Top = 208
    object qrySaldoItemDevolucnQtdePed: TBCDField
      FieldName = 'nQtdePed'
      ReadOnly = True
      Precision = 32
    end
  end
end
