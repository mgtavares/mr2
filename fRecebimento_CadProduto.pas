unit fRecebimento_CadProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons, cxPC, cxControls;

type
  TfrmRecebimento_CadProduto = class(TfrmCadastro_Padrao)
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    dsDepartamento: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TAutoIncField;
    qryDepartamentocNmDepartamento: TStringField;
    qryDepartamentonCdStatus: TIntegerField;
    qryCategoria: TADOQuery;
    qryCategorianCdCategoria: TAutoIncField;
    qryCategorianCdDepartamento: TIntegerField;
    qryCategoriacNmCategoria: TStringField;
    qryCategorianCdStatus: TIntegerField;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DataSource1: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategorianCdCategoria: TIntegerField;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySubCategorianCdStatus: TIntegerField;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DataSource2: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentonCdSubCategoria: TIntegerField;
    qrySegmentocNmSegmento: TStringField;
    qrySegmentonCdStatus: TIntegerField;
    DBEdit9: TDBEdit;
    DataSource3: TDataSource;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TAutoIncField;
    qryMarcacNmMarca: TStringField;
    qryMarcanCdStatus: TIntegerField;
    Label3: TLabel;
    DBEdit10: TDBEdit;
    Label5: TLabel;
    DBEdit11: TDBEdit;
    Label8: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DataSource4: TDataSource;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhanCdMarca: TIntegerField;
    qryLinhacNmLinha: TStringField;
    qryLinhanCdStatus: TIntegerField;
    DBEdit14: TDBEdit;
    DataSource5: TDataSource;
    qryGrade: TADOQuery;
    qryColecao: TADOQuery;
    qryGradenCdGrade: TAutoIncField;
    qryGradecNmGrade: TStringField;
    Label9: TLabel;
    DBEdit15: TDBEdit;
    Label10: TLabel;
    DBEdit16: TDBEdit;
    Label11: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DataSource6: TDataSource;
    qryColecaonCdColecao: TAutoIncField;
    qryColecaocNmColecao: TStringField;
    DBEdit19: TDBEdit;
    DataSource7: TDataSource;
    Label12: TLabel;
    DBEdit20: TDBEdit;
    qryClasse: TADOQuery;
    qryClassenCdClasseProduto: TAutoIncField;
    qryClassecNmClasseProduto: TStringField;
    DBEdit22: TDBEdit;
    DataSource8: TDataSource;
    qryMasternCdProduto: TIntegerField;
    qryMasternCdDepartamento: TIntegerField;
    qryMasternCdCategoria: TIntegerField;
    qryMasternCdSubCategoria: TIntegerField;
    qryMasternCdSegmento: TIntegerField;
    qryMasternCdMarca: TIntegerField;
    qryMasternCdLinha: TIntegerField;
    qryMastercReferencia: TStringField;
    qryMasternCdTabTipoProduto: TIntegerField;
    qryMasternCdProdutoPai: TIntegerField;
    qryMasternCdGrade: TIntegerField;
    qryMasternCdColecao: TIntegerField;
    qryMastercNmProduto: TStringField;
    qryMasternCdClasseProduto: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMasternCdCor: TIntegerField;
    qryMasternCdMaterial: TIntegerField;
    qryMasteriTamanho: TIntegerField;
    qryMasternQtdeEstoque: TBCDField;
    qryMasternValCusto: TBCDField;
    qryMasternValVenda: TBCDField;
    qryMastercEAN: TStringField;
    qryMastercEANFornec: TStringField;
    qryMasterdDtUltReceb: TDateTimeField;
    qryMasterdDtUltVenda: TDateTimeField;
    qryConsultaProd: TADOQuery;
    qryConsultaProdnCdProduto: TIntegerField;
    Label15: TLabel;
    DBEdit25: TDBEdit;
    Label18: TLabel;
    DBEdit28: TDBEdit;
    qryMastercUnidadeMedida: TStringField;
    qryMastercFlgProdVenda: TIntegerField;
    qryMastercUnidadeMedidaCompra: TStringField;
    qryMasternFatorCompra: TBCDField;
    qryMasternQtdeMinimaCompra: TBCDField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasterdDtCad: TDateTimeField;
    qryDepartamentonCdGrupoProduto: TIntegerField;
    qryMasternCdGrupo_Produto: TIntegerField;
    qryMastercFlgProdEstoque: TIntegerField;
    qryMastercFlgProdCompra: TIntegerField;
    qryMastercFlgAtivoFixo: TIntegerField;
    edtCor: TEdit;
    edtMaterial: TEdit;
    lblCor: TLabel;
    Label1: TLabel;
    btGerarProduto: TcxButton;
    btLimpar: TcxButton;
    btSair: TcxButton;
    Label14: TLabel;
    qryPreparaTemp: TADOQuery;
    qryPreparaTempnCdCor: TIntegerField;
    qryPreparaTempcCor: TStringField;
    qryPreparaTempcMaterial: TStringField;
    qryPreparaTempnValCusto: TBCDField;
    qryPreparaTempnValVenda: TBCDField;
    cmdAddItem: TADOCommand;
    usp_Gera_Grade: TADOStoredProc;
    qryDescobreProduto: TADOQuery;
    btConsulta: TcxButton;
    qryDescobreProdutonCdProduto: TIntegerField;
    btDepartamento: TcxButton;
    btMarca: TcxButton;
    btGrade: TcxButton;
    btColecao: TcxButton;
    btClasse: TcxButton;
    btCorPredom: TcxButton;
    btCor: TcxButton;
    qryMaterialAux: TADOQuery;
    qryMaterialAuxcNmMaterial: TStringField;
    qryCorAux: TADOQuery;
    qryCorAuxcNmCor: TStringField;
    Label13: TLabel;
    Label16: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit10Exit(Sender: TObject);
    procedure DBEdit11Exit(Sender: TObject);
    procedure DBEdit15Exit(Sender: TObject);
    procedure DBEdit16Exit(Sender: TObject);
    procedure DBEdit20Exit(Sender: TObject);
    procedure DBEdit17Enter(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit15KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit16KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit21KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit12Exit(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
    procedure btGerarProdutoClick(Sender: TObject);
    procedure btLimparClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btConsultaClick(Sender: TObject);
    procedure desativaCampos();
    procedure ativaCampos();
    procedure TrataTeclaFuncao(Key: Word);
    procedure edtCorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMaterialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit12KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit17KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit25KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit28KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btDepartamentoClick(Sender: TObject);
    procedure btMarcaClick(Sender: TObject);
    procedure btGradeClick(Sender: TObject);
    procedure btColecaoClick(Sender: TObject);
    procedure btClasseClick(Sender: TObject);
    procedure btCorPredomClick(Sender: TObject);
    procedure btCorClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    nCdProdutoGerado : integer ;
    nValCustoUnitario: double ;

  end;

var
  frmRecebimento_CadProduto: TfrmRecebimento_CadProduto;
  formItemMenu : TForm ;

implementation

uses fGerarGrade, fLookup_Padrao, fIncLinha, fProduto_Duplicar, fMenu,
  fProdutoPosicaoEstoque, fProduto_DetalheGrade, fDepartamento, fMarca,
  fGrade, fCor, fCorPredom, fClasseProduto, fColecaoProduto;

{$R *.dfm}

procedure TfrmRecebimento_CadProduto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PRODUTO' ;
  nCdTabelaSistema  := 25 ;
  nCdConsultaPadrao := 42 ;
  bLimpaAposSalvar  := False ;
end;

procedure TfrmRecebimento_CadProduto.DBEdit2Exit(Sender: TObject);
begin
  PosicionaQuery(qryDepartamento, DBEdit2.Text) ;

  if (qryDepartamento.Active) then
    qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TfrmRecebimento_CadProduto.PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
begin
    if (Trim(cValor) = '') then
        exit ;

    oQuery.Close ;
    oQuery.Parameters.ParamByName('nPK').Value := cValor ;
    oQuery.Open ;
end ;

procedure TfrmRecebimento_CadProduto.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCategoria, DBEdit4.Text) ;

  if (qryCategoria.Active) then
      qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qrySubCategoria, DBEdit6.Text) ;

  if (qrySubCategoria.Active) then
  begin
    qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
    qryMarca.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  end;

end;

procedure TfrmRecebimento_CadProduto.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qrySegmento, DBEdit7.Text) ;
end;

procedure TfrmRecebimento_CadProduto.DBEdit10Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryMarca, DBEdit10.Text) ;

  if (qryMarca.Active) then
    qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit11Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryLinha, DBEdit11.Text) ;
end;

procedure TfrmRecebimento_CadProduto.DBEdit15Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryGrade, DBEdit15.Text) ;
end;

procedure TfrmRecebimento_CadProduto.DBEdit16Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryColecao, DBEdit16.Text) ;
end;

procedure TfrmRecebimento_CadProduto.DBEdit20Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryClasse, DBEdit20.Text) ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit17Enter(Sender: TObject);
begin
  inherited;

  If (DbEdit17.Text = '') and (qryMaster.state = dsInsert) then
  begin
      qryMastercNmProduto.Value := Trim(DBEdit8.Text) + ' ' + Trim(DBEdit13.Text) + ' ' + Trim(DBEdit12.Text) ;
  end ;
  
end;

procedure TfrmRecebimento_CadProduto.btSalvarClick(Sender: TObject);
var
    bInclusao : Boolean ;
begin
  inherited;

end;

procedure TfrmRecebimento_CadProduto.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;
      
  PosicionaQuery(qryDepartamento, qryMasternCdDepartamento.asString) ;

  qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;


  PosicionaQuery(qryCategoria, qryMasternCdCategoria.asString) ;
  qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryMasternCdCategoria.Value ;

  PosicionaQuery(qrySubCategoria, qryMasternCdSubCategoria.asString) ;
  qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  qryMarca.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;

  PosicionaQuery(qrySegmento, qryMasternCdSegmento.asString) ;

  PosicionaQuery(qryMarca, qryMasternCdMarca.asString) ;

  qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

  PosicionaQuery(qryLinha, qryMasternCdLinha.asString) ;

  PosicionaQuery(qryGrade, qryMasternCdGrade.asString) ;

  PosicionaQuery(qryColecao, qryMasternCdColecao.asString) ;

  PosicionaQuery(qryClasse, qryMasternCdClasseProduto.asString) ;

end;

procedure TfrmRecebimento_CadProduto.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryDepartamento.Close ;
  qryCategoria.Close ;
  qrySubCategoria.Close ;
  qrySegmento.Close ;
  qryMarca.Close ;
  qryLinha.Close ;
  qryGrade.Close ;
  qryColecao.Close ;
  qryClasse.Close ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  TrataTeclaFuncao(Key) ;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            nPK := frmLookup_Padrao.ExecutaConsulta(45);
            If (nPK > 0) then
            begin
                qryMasternCdDepartamento.Value := nPK ;
                PosicionaQuery(qryDepartamento, qryMasternCdDepartamento.asString) ;
                qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

                // consulta de categoria
                nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);
                If (nPK > 0) then
                begin
                    qryMasternCdCategoria.Value := nPK ;
                    PosicionaQuery(qryCategoria, qryMasternCdCategoria.asString) ;
                    qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

                    // consulta de subcategoria
                    nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
                    If (nPK > 0) then
                    begin
                        qryMasternCdSubCategoria.Value := nPK ;
                        PosicionaQuery(qrySubCategoria,qryMasternCdSubCategoria.asString) ;

                        qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
                        qryMarca.Parameters.ParamByName('nCdSubCategoria').Value    := qrySubCategorianCdSubCategoria.Value ;

                        // Consulta de segmento
                        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
                        If (nPK > 0) then
                        begin
                            qryMasternCdSegmento.Value := nPK ;
                            PosicionaQuery(qrySegmento,qryMasternCdSegmento.AsString) ;

                            // consulta de marca
                            nPK := frmLookup_Padrao.ExecutaConsulta2(49,'nCdStatus = 1 AND EXISTS(SELECT 1 FROM MarcaSubcategoria MSC WHERE MSC.nCdMarca = Marca.nCdMarca AND MSC.nCdSubCategoria = ' + Chr(39) + qrySubCategorianCdSubCategoria.asString + Chr(39) + ')');
                            If (nPK > 0) then
                            begin
                                qryMasternCdMarca.Value := nPK ;
                                PosicionaQuery(qryMarca, qryMasternCdMarca.asString) ;
                                qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

                                DBEdit10.SetFocus ;
                            end ;

                        end ;



                    end ;

                end ;

            end ;
        end ;
    end ;
  end ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  TrataTeclaFuncao(Key) ;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryDepartamento.active or (qryDepartamentonCdDepartamento.Value = 0) then
            begin
                MensagemAlerta('Selecione um departamento.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);
            If (nPK > 0) then
                qryMasternCdCategoria.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  TrataTeclaFuncao(Key) ;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryCategoria.active or (qryCategorianCdCategoria.Value = 0) then
            begin
                MensagemAlerta('Selecione uma Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
            If (nPK > 0) then
                qryMasternCdSubCategoria.Value := nPK ;
        end ;
    end ;
  end ;
end;

procedure TfrmRecebimento_CadProduto.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  TrataTeclaFuncao(Key) ;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qrySubCategoria.active or (qrySubCategorianCdCategoria.Value = 0) then
            begin
                MensagemAlerta('Selecione uma Sub Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
            If (nPK > 0) then
                qryMasternCdSegmento.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit10KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  TrataTeclaFuncao(Key) ;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if not qrySubCategoria.active or (qrySubCategorianCdSubCategoria.Value = 0) then
            begin
                MensagemAlerta('Selecione uma Sub Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(49,'nCdStatus = 1 AND EXISTS(SELECT 1 FROM MarcaSubcategoria MSC WHERE MSC.nCdMarca = Marca.nCdMarca AND MSC.nCdSubCategoria = ' + Chr(39) + qrySubCategorianCdSubCategoria.asString + Chr(39) + ')');
            If (nPK > 0) then
                qryMasternCdMarca.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  TrataTeclaFuncao(Key) ;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryMarca.active or (qryMarcanCdMarca.Value = 0) then
            begin
                MensagemAlerta('Selecione uma Marca.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdStatus = 1 and nCdMarca = ' + qryMarcanCdMarca.asString);
            If (nPK > 0) then
                qryMasternCdLinha.Value := nPK ;
        end ;
    end ;

    73 : begin
        if (qryMaster.State <> dsInsert) then
        begin
            MensagemAlerta('Fun��o s� permitida na inclus�o de produtos.') ;
            exit ;
        end ;

        if (DBEdit13.Text = '') then
        begin
            MensagemAlerta('Selecione a marca.');
            DBEdit10.SetFocus;
            abort ;
        end ;

        frmIncLinha.nCdMarca   := qryMarcanCdMarca.Value ;
        frmIncLinha.Edit1.Text := '' ;
        frmIncLinha.ShowModal ;

        if (frmIncLinha.nCdLinha > 0) then
        begin
            qryMasternCdLinha.Value := frmIncLinha.nCdLinha ;
            PosicionaQuery(qryLinha, IntToStr(frmIncLinha.nCdLinha)) ;
            //DBEdit12.SetFocus ;
        end ;

    end ;

  end ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit15KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  TrataTeclaFuncao(Key) ;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(51);
            If (nPK > 0) then
                qryMasternCdGrade.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit16KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  TrataTeclaFuncao(Key) ;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(52);
            If (nPK > 0) then
                qryMasternCdColecao.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit20KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;
  TrataTeclaFuncao(Key) ;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(53);
            If (nPK > 0) then
                qryMasternCdClasseProduto.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit21KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);
            If (nPK > 0) then
                qryMasternCdStatus.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit12Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsInsert) then
      exit ;
      
  qryConsultaProd.Close ;

  if (Trim(dbEdit2.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdDepartamento').Value := StrToInt(Trim(DbEdit2.Text)) ;

  if (Trim(dbEdit4.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdCategoria').Value    := StrToInt(Trim(DbEdit4.Text)) ;

  if (Trim(dbEdit6.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdSubCategoria').Value := StrToInt(Trim(DbEdit6.Text)) ;

  if (Trim(dbEdit7.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdSegmento').Value     := StrToInt(Trim(DbEdit7.Text)) ;

  if (Trim(dbEdit10.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdMarca').Value        := StrToInt(Trim(DbEdit10.Text)) ;

  if (Trim(dbEdit11.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('nCdLinha').Value        := StrToInt(Trim(DbEdit11.Text)) ;

  if (Trim(dbEdit12.Text) <> '') then
    qryConsultaProd.Parameters.ParamByName('cReferencia').Value     := Trim(DbEdit12.Text) ;

  qryConsultaProd.Open ;

  if not qryConsultaProd.eof then
  begin
      MensagemAlerta('Produto j� cadastrado.') ;

      PosicionaPK(qryConsultaProdnCdProduto.Value) ;

      desativaCampos() ;
      frmMenu.limpaBufferTeclado;
      edtCor.SetFocus;
      
  end ;

end;

procedure TfrmRecebimento_CadProduto.btIncluirClick(Sender: TObject);
begin
  inherited;
  DbEdit2.SetFocus ;
end;

procedure TfrmRecebimento_CadProduto.qryMasterBeforePost(DataSet: TDataSet);
begin

  If (dbEdit2.Text = '') or (dbEdit3.Text = '') then
  begin
      MensagemAlerta('Informe o departamento.') ;
      dbEdit2.SetFocus ;
      Abort ;
  end ;

  If (dbEdit4.Text = '') or (dbEdit5.Text = '') then
  begin
      MensagemAlerta('Informe a categoria.') ;
      dbEdit4.SetFocus ;
      Abort ;
  end ;

  If (dbEdit6.Text = '') or (dbEdit8.Text = '') then
  begin
      MensagemAlerta('Informe a Sub Categoria.') ;
      dbEdit6.SetFocus ;
      Abort ;
  end ;

  If (dbEdit7.Text = '') or (dbEdit9.Text = '') then
  begin
      MensagemAlerta('Informe o Segmento.') ;
      dbEdit7.SetFocus ;
      Abort ;
  end ;

  If (dbEdit10.Text = '') or (dbEdit13.Text = '') then
  begin
      MensagemAlerta('Informe a marca.') ;
      dbEdit10.SetFocus ;
      Abort ;
  end ;

  If (dbEdit11.Text = '') or (dbEdit14.Text = '') then
  begin
      MensagemAlerta('Informe a Linha.') ;
      dbEdit11.SetFocus ;
      Abort ;
  end ;

  If (dbEdit12.Text = '') then
  begin
      MensagemAlerta('Informe a Refer�ncia.') ;
      dbEdit12.SetFocus ;
      Abort ;
  end ;

  If (dbEdit18.Text = '') then
  begin
      MensagemAlerta('Informe a Grade.') ;
      dbEdit15.SetFocus ;
      Abort ;
  end ;

  If (dbEdit12.Text = '') and (dbEdit15.Text <> '') then
  begin
      MensagemAlerta('Informe a Refer�ncia.') ;
      dbEdit12.SetFocus ;
      Abort ;
  end ;

  If (dbEdit16.Text = '') or (dbEdit19.Text = '') then
  begin
      MensagemAlerta('Informe a Cole��o.') ;
      dbEdit16.SetFocus ;
      Abort ;
  end ;

  If (dbEdit17.Text = '') then
  begin
      MensagemAlerta('Informe a Descri��o.') ;
      dbEdit17.SetFocus ;
      Abort ;
  end ;

  if (qryMasternValCusto.Value <= 0) then
  begin
      MensagemAlerta('Informe o pre�o de custo.') ;
      DBEdit25.SetFocus;
      abort ;
  end ;

  if (qryMasternValVenda.Value <= 0) then
  begin
      MensagemAlerta('Informe o pre�o de venda.') ;
      DBEdit28.SetFocus;
      abort ;
  end ;

  if (trim(edtCor.Text) = '') then
  begin
      MensagemAlerta('Informe a cor.') ;
      edtCor.SetFocus;
      abort ;
  end ;

  if (trim(edtMaterial.Text) = '') then
  begin
      MensagemAlerta('Informe o material.') ;
      edtMaterial.SetFocus;
      abort ;
  end ;

  qryMastercFlgProdEstoque.Value := 1 ;
  qryMastercFlgProdCompra.Value  := 1 ;
  qryMastercFlgAtivoFixo.Value   := 0 ;
  inherited;

end;

procedure TfrmRecebimento_CadProduto.ToolButton10Click(Sender: TObject);
begin
  inherited;

  if qryMaster.eof then
  begin
      MensagemAlerta('Nenhum produto ativo para duplicar.') ;
      exit ;
  end ;

  frmProduto_Duplicar.nCdProduto         := qryMasternCdProduto.Value ;
  frmProduto_Duplicar.cDescricaoAnterior := qryMastercNmproduto.Value ;
  frmProduto_Duplicar.ShowModal;

  if (frmProduto_Duplicar.nCdProdutoNovo > 0) then
      PosicionaQuery(qryMaster, IntToStr(frmProduto_Duplicar.nCdProdutoNovo)) ;

end;

procedure TfrmRecebimento_CadProduto.btGerarProdutoClick(Sender: TObject);
begin
  inherited;

  if (qryMaster.State in [dsBrowse,dsEdit]) then
      if (MessageDlg('Confirma a gera��o desta nova grade ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      begin
          Sleep(100) ;
          frmMenu.limpaBufferTeclado;
          edtCor.setFocus;
          abort ;
      end ;

  nCdProdutoGerado := 0 ;

  try
      if (qryMaster.State = dsInsert) then
      begin
          qryMasternCdTabTipoProduto.Value    := 1 ;
          qryMasternCdStatus.Value            := 1 ;
          qryMastercFlgProdVenda.Value        := 1 ;
          qryMastercUnidadeMedida.Value       := 'UN' ;
          qryMastercUnidadeMedidaCompra.Value := 'UN' ;
          qryMasternFatorCompra.Value         := 1 ;
          qryMasternQtdeMinimaCompra.Value    := 1 ;
          qryMasterdDtCad.Value               := Now() ;
          qryMasternCdUsuarioCad.Value        := frmMenu.nCdUsuarioLogado;
          qryMasternCdGrupo_Produto.Value     := qryDepartamentonCdGrupoProduto.Value ;
      end ;

      if (qryMaster.State in [dsInsert,dsEdit]) then
          qryMaster.Post ;

      {-- prepara a tabela tempor�ria --}
      qryPreparaTemp.ExecSQL;

      {-- inseri o registro na temporaria que a procedure usa --}
      cmdAddItem.Parameters.ParamByName('nCdCor').Value    := 0 ;
      cmdAddItem.Parameters.ParamByName('cCor').Value      := edtCor.Text ;
      cmdAddItem.Parameters.ParamByName('cMaterial').Value := edtMaterial.Text ;
      cmdAddItem.Parameters.ParamByName('nValCusto').Value := qryMasternValCusto.Value ;
      cmdAddItem.Parameters.ParamByName('nValVenda').Value := qryMasternValVenda.Value ;
      cmdAddItem.Execute;

      {-- executa a procedure que gera a grade --}
      usp_Gera_Grade.Close ;
      usp_Gera_Grade.Parameters.ParamByName('@nCdProduto').Value  := qryMasternCdProduto.Value ;
      usp_Gera_Grade.Parameters.ParamByName('@nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
      usp_Gera_Grade.ExecProc;

      {-- descobre o ID do produto n�vel 2 gerado --}
      qryDescobreProduto.Close;
      qryDescobreProduto.Parameters.ParamByName('nCdProdutoPai').Value := qryMasternCdProduto.Value ;
      qryDescobreProduto.Parameters.ParamByName('cNmCor').Value        := edtCor.Text ;
      qryDescobreProduto.Parameters.ParamByName('cNmMaterial').Value   := edtMaterial.Text ;
      qryDescobreProduto.Open;

      if not qryDescobreProduto.eof then
      begin
          nCdProdutoGerado  := qryDescobreProdutonCdProduto.Value ;
          nValCustoUnitario := qryMasternValCusto.Value ;
      end ;

  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  Close;

end;

procedure TfrmRecebimento_CadProduto.btLimparClick(Sender: TObject);
begin

  qryMaster.Close;

  ativaCampos() ;
  btIncluir.Click;

  edtCor.Text      := '' ;
  edtMaterial.Text := '' ;
  
  DBEdit2.SetFocus;
  
end;

procedure TfrmRecebimento_CadProduto.btSairClick(Sender: TObject);
begin

  nCdProdutoGerado := 0 ;
  Close ;

end;

procedure TfrmRecebimento_CadProduto.FormShow(Sender: TObject);
begin
  inherited;

  nCdProdutoGerado := 0 ;

  if (qryMaster.Active) and (qryMaster.RecordCount > 0) then
  begin
      desativaCampos();
      edtCor.SetFocus;
  end
  else
  begin
      ativaCampos();
      btIncluir.Click;
  end ;

end;

procedure TfrmRecebimento_CadProduto.btConsultaClick(Sender: TObject);
var
    nCdPK : integer ;
begin
  nCdPK := frmLookup_Padrao.ExecutaConsulta(nCdConsultaPadrao);

  If (nCdPK > 0) then
  begin
      PosicionaPK(nCdPK) ;

      desativaCampos();
      edtCor.SetFocus;
      
  end ;

end;

procedure TfrmRecebimento_CadProduto.desativaCampos;
begin

    DBEdit2.ReadOnly  := True ;
    DBEdit4.ReadOnly  := True ;
    DBEdit6.ReadOnly  := True ;
    DBEdit7.ReadOnly  := True ;
    DBEdit10.ReadOnly := True ;
    DBEdit11.ReadOnly := True ;
    DBEdit15.ReadOnly := True ;
    DBEdit16.ReadOnly := True ;
    DBEdit2.ReadOnly  := True ;
    DBEdit12.ReadOnly := True ;
    DBEdit17.ReadOnly := True ;
    DBEdit20.ReadOnly := True ;

    DBEdit2.Color    := $00E9E4E4  ;
    DBEdit4.Color    := $00E9E4E4  ;
    DBEdit6.Color    := $00E9E4E4  ;
    DBEdit7.Color    := $00E9E4E4  ;
    DBEdit10.Color   := $00E9E4E4  ;
    DBEdit11.Color   := $00E9E4E4  ;
    DBEdit15.Color   := $00E9E4E4  ;
    DBEdit16.Color   := $00E9E4E4  ;
    DBEdit2.Color    := $00E9E4E4  ;
    DBEdit12.Color   := $00E9E4E4  ;
    DBEdit17.Color   := $00E9E4E4  ;
    DBEdit20.Color   := $00E9E4E4  ;

    edtCor.Text      := '' ;

end;

procedure TfrmRecebimento_CadProduto.ativaCampos;
begin

    DBEdit2.ReadOnly  := False ;
    DBEdit4.ReadOnly  := False ;
    DBEdit6.ReadOnly  := False ;
    DBEdit7.ReadOnly  := False ;
    DBEdit10.ReadOnly := False ;
    DBEdit11.ReadOnly := False ;
    DBEdit15.ReadOnly := False ;
    DBEdit16.ReadOnly := False ;
    DBEdit2.ReadOnly  := False ;
    DBEdit12.ReadOnly := False ;
    DBEdit17.ReadOnly := False ;
    DBEdit20.ReadOnly := False ;

    DBEdit2.Color    := clWhite  ;
    DBEdit4.Color    := clWhite  ;
    DBEdit6.Color    := clWhite  ;
    DBEdit7.Color    := clWhite  ;
    DBEdit10.Color   := clWhite  ;
    DBEdit11.Color   := clWhite  ;
    DBEdit15.Color   := clWhite  ;
    DBEdit16.Color   := clWhite  ;
    DBEdit2.Color    := clWhite  ;
    DBEdit12.Color   := clWhite  ;
    DBEdit17.Color   := clWhite  ;
    DBEdit20.Color   := clWhite  ;

end;

procedure TfrmRecebimento_CadProduto.TrataTeclaFuncao(Key: Word);
begin

  if (key = VK_F2) then
      btDepartamento.Click;

  if (key = VK_F3) then
      btMarca.Click;

  if (key = VK_F5) then
      btGrade.Click;

  if (key = VK_F6) then
      btColecao.Click;

  if (key = VK_F7) then
      btClasse.Click;

  if (key = VK_F8) then
      btCorPredom.Click;

  if (key = VK_F9) then
      btCor.Click;

  if (key = VK_ESCAPE) then
      btLimpar.Click;

  if (key = VK_F10) then
      btConsulta.Click;

  if (key = VK_F12) then
      btSair.Click;

end;

procedure TfrmRecebimento_CadProduto.edtCorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;
  TrataTeclaFuncao(Key) ;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(160);

        If (nPK > 0) then
        begin
            PosicionaQuery(qryCorAux,IntToStr(nPK)) ;
            edtCor.Text := Trim(qryCorAuxcNmCor.Value) ;
        end ;

    end ;

  end ;

end;

procedure TfrmRecebimento_CadProduto.edtMaterialKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;
  TrataTeclaFuncao(Key) ;

  case key of
    vk_F4 : begin

        if (DBEdit13.Text = '') then
        begin
            MensagemAlerta('Selecione a marca.') ;
            DBEdit10.Setfocus;
            abort ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(44,'nCdMarca = ' + qryMasternCdMarca.AsString);

        If (nPK > 0) then
        begin
            PosicionaQuery(qryMaterialAux,IntToStr(nPK)) ;
            edtMaterial.Text := Trim(qryMaterialAuxcNmMaterial.Value) ;
        end ;

    end ;

  end ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit12KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  TrataTeclaFuncao(Key) ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit17KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  TrataTeclaFuncao(Key) ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit25KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  TrataTeclaFuncao(Key) ;

end;

procedure TfrmRecebimento_CadProduto.DBEdit28KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  TrataTeclaFuncao(Key) ;

end;

procedure TfrmRecebimento_CadProduto.btDepartamentoClick(Sender: TObject);
begin

   formItemMenu := TfrmDepartamento.Create(nil) ;
   try
     formItemMenu.WindowState := wsMaximized ;
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmRecebimento_CadProduto.btMarcaClick(Sender: TObject);
begin
   formItemMenu := TfrmMarca.Create(nil) ;
   try
     formItemMenu.WindowState := wsMaximized ;
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmRecebimento_CadProduto.btGradeClick(Sender: TObject);
begin

   formItemMenu := TfrmGrade.Create(nil) ;
   try
     formItemMenu.WindowState := wsMaximized ;
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmRecebimento_CadProduto.btColecaoClick(Sender: TObject);
begin

   formItemMenu := TfrmColecaoProduto.Create(nil) ;
   try
     formItemMenu.WindowState := wsMaximized ;
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmRecebimento_CadProduto.btClasseClick(Sender: TObject);
begin

   formItemMenu := TfrmClasseProduto.Create(nil) ;
   try
     formItemMenu.WindowState := wsMaximized ;
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmRecebimento_CadProduto.btCorPredomClick(Sender: TObject);
begin

   formItemMenu := TfrmCorPredom.Create(nil) ;
   try
     formItemMenu.WindowState := wsMaximized ;
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmRecebimento_CadProduto.btCorClick(Sender: TObject);
begin

   formItemMenu := TfrmCor.Create(nil) ;
   try
     formItemMenu.WindowState := wsMaximized ;
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

initialization
    RegisterClass(TfrmRecebimento_CadProduto) ;

end.
