unit fBaixaChequePre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, DBCtrls, StdCtrls, Mask, GridsEh, DBGridEh, DBGridEhGrouping,
  cxPC, cxControls;

type
  TfrmBaixaChequePre = class(TfrmProcesso_Padrao)
    qryContaBancaria: TADOQuery;
    qryContaBancariacNmContaBancaria: TStringField;
    qryContaBancarianCdContaBancaria: TIntegerField;
    DataSource1: TDataSource;
    qryAux: TADOQuery;
    qryCheque: TADOQuery;
    qryChequedDtDeposito: TDateTimeField;
    qryChequeiNrCheque: TIntegerField;
    qryChequenValCheque: TBCDField;
    dsCheque: TDataSource;
    qryChequecFlgCompensado: TIntegerField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryChequecNmFavorecido: TStringField;
    GroupBox1: TGroupBox;
    MaskEdit1: TMaskEdit;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBaixaChequePre: TfrmBaixaChequePre;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmBaixaChequePre.FormShow(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT nCdContaBancaria                                                          ') ;
  qryAux.SQL.Add('  FROM ContaBancaria                                                             ') ;
  qryAux.SQL.Add(' WHERE nCdEmpresa       = ' + IntToStr(frmMenu.nCdEmpresaAtiva)                   ) ;
  qryAux.SQL.Add('   AND cFlgEmiteCheque  = 1                                                      ') ;
  qryAux.SQL.Add('   AND EXISTS(SELECT 1                                                           ') ;
  qryAux.SQL.Add('                FROM UsuarioContaBancaria UCB                                    ') ;
  qryAux.SQL.Add('               WHERE UCB.nCdContaBancaria = ContaBancaria.nCdContaBancaria       ') ;
  qryAux.SQL.Add('                 AND UCB.nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ')') ;
  qryAux.Open ;

  if qryAux.Eof then
  begin
      qryAux.Close ;
      MensagemAlerta('Nenhuma conta banc�ria que movimente cheque vinculada para este usu�rio.') ;
      exit ;
  end ;

  if (qryAux.RecordCount = 1) then
  begin

      qryContaBancaria.Close ;
      Posicionaquery(qryContaBancaria, qryAux.FieldList[0].AsString) ;
      MaskEdit1.Text := qryAux.FieldList[0].AsString ;

  end ;

  qryAux.Close ;

  MaskEdit1.SetFocus ;

end;

procedure TfrmBaixaChequePre.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (not qryContaBancaria.Active) or (qryContaBancaria.Eof) then
  begin
      MensagemAlerta('Selecione uma conta banc�ria.') ;
      MaskEdit1.SetFocus;
      exit ;
  end ;

  PosicionaQuery(qryCheque, qryContaBancarianCdContaBancaria.AsString) ;

  if (qryCheque.Eof) then
  begin
      ShowMessage('Nenhum cheque pendente de baixa.') ;
  end ;

end;

procedure TfrmBaixaChequePre.ToolButton4Click(Sender: TObject);
begin
  inherited;

  case MessageDlg('Confirma a baixa deste(s) cheque(s) ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryCheque.UpdateBatch;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryCheque.Close ;

  ShowMessage('Registros atualizados com sucesso.') ;

end;

procedure TfrmBaixaChequePre.ToolButton2Click(Sender: TObject);
begin
  inherited;

  qryCheque.Close ;
  qryContaBancaria.Close ;
  
end;

procedure TfrmBaixaChequePre.MaskEdit1Exit(Sender: TObject);
begin
  inherited;
  qryContaBancaria.Close ;
  PosicionaQuery(qryContaBancaria, MaskEdit1.Text) ;

end;

procedure TfrmBaixaChequePre.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(107);

        If (nPK > 0) then
        begin
            MaskEdit1.Text := IntToStr(nPK) ;
            PosicionaQuery(qryContaBancaria, MaskEdit1.Text) ;
        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmBaixaChequePre) ;

end.
