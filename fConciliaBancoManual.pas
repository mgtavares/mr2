unit fConciliaBancoManual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBCtrls, StdCtrls, ADODB, Mask, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxCurrencyEdit, GridsEh, DBGridEh, cxPC,
  cxLookAndFeelPainters, cxButtons, DBClient, Menus, DBGridEhGrouping,
  ToolCtrlsEh, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid;

type
  TfrmConciliaBancoManual = class(TfrmProcesso_Padrao)
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancariaDadosConta: TStringField;
    qryContaBancarianValLimiteCredito: TBCDField;
    DataSource1: TDataSource;
    qrySaldoConta: TADOQuery;
    qrySaldoContanSaldo: TBCDField;
    DataSource2: TDataSource;
    qryNaoConciliados: TADOQuery;
    qryNaoConciliadosnCdLanctoFin: TIntegerField;
    qryNaoConciliadosdDtLancto: TDateTimeField;
    qryNaoConciliadosnValCredito: TBCDField;
    qryNaoConciliadosnValDebito: TBCDField;
    qryNaoConciliadosnCdTipoLancto: TIntegerField;
    qryNaoConciliadoscFlgConciliado: TIntegerField;
    qryNaoConciliadoscDocumento: TStringField;
    qryNaoConciliadoscHistorico: TStringField;
    qryNaoConciliadoscFlgManual: TIntegerField;
    dsNaoConciliados: TDataSource;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryConciliados: TADOQuery;
    dsConciliados: TDataSource;
    qryConciliadosnCdLanctoFin: TIntegerField;
    qryConciliadosdDtLancto: TDateTimeField;
    qryConciliadosnValCredito: TBCDField;
    qryConciliadosnValDebito: TBCDField;
    qryConciliadosnCdTipoLancto: TIntegerField;
    qryConciliadoscFlgConciliado: TIntegerField;
    qryConciliadoscDocumento: TStringField;
    qryConciliadoscHistorico: TStringField;
    qryConciliadoscFlgManual: TIntegerField;
    qrySaldoContadDtSaldo: TDateTimeField;
    SP_PROCESSA_CONCILIACAO: TADOStoredProc;
    cmdPreparaTemp: TADOCommand;
    qryConsultaCheque: TADOQuery;
    SP_REGISTRA_CHEQUE_DEVOLVIDO_EXTRATO: TADOStoredProc;
    dsConciliacao: TDataSource;
    qryConciliacaoBancaria: TADOQuery;
    qryConciliacaoBancarianCdConciliacaoBancaria: TAutoIncField;
    qryConciliacaoBancarianCdContaBancaria: TIntegerField;
    qryConciliacaoBancariadDtConciliacaoBancaria: TDateTimeField;
    qryConciliacaoBancariadDtExtrato: TDateTimeField;
    qryConciliacaoBancarianSaldoContaBancaria: TBCDField;
    qryConciliacaoBancarianValCredito: TBCDField;
    qryConciliacaoBancarianValDebito: TBCDField;
    qryConciliacaoBancarianCdUsuario: TIntegerField;
    cdsSaldoCalculado: TClientDataSet;
    cdsSaldoCalculadonSaldoFinal: TFloatField;
    dsSaldoCalculado: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label9: TLabel;
    DBEdit5: TDBEdit;
    Label10: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    DBEdit10: TDBEdit;
    qryAux: TADOQuery;
    qrySomaLancamentos: TADOQuery;
    qrySomaLancamentosnValCredito: TBCDField;
    qrySomaLancamentosnValDebito: TBCDField;
    qryPopulaTemp: TADOQuery;
    edtDtInicial: TDateTimePicker;
    Label3: TLabel;
    cxButton4: TcxButton;
    edtDtFinal: TDateTimePicker;
    Label11: TLabel;
    RadioGroup1: TRadioGroup;
    qryLanctoFin: TADOQuery;
    qryLanctoFincFlgManual: TIntegerField;
    qryLanctoFinnCdUsuario: TIntegerField;
    qryLanctoFincNmUsuario: TStringField;
    qryLanctoFincFlgConciliado: TIntegerField;
    PopupMenu1: TPopupMenu;
    ExcluirLanamento1: TMenuItem;
    qryContaBancariacFlgReapresentaCheque: TIntegerField;
    SP_REAPRESENTA_CHEQUE: TADOStoredProc;
    qryConsultaChequenCdCheque: TAutoIncField;
    qryConsultaChequedDtDevol: TDateTimeField;
    qryConsultaChequedDtSegDevol: TDateTimeField;
    ExibirTtulosMovimentados1: TMenuItem;
    qryLanctoCartao: TADOQuery;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn2: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn4: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn5: TcxGridDBColumn;
    cxGrid5DBTableView1: TcxGridDBTableView;
    cxGrid5Level1: TcxGridLevel;
    cxGrid5: TcxGrid;
    cxGrid5DBTableView1DBColumn1: TcxGridDBColumn;
    cxGrid5DBTableView1DBColumn2: TcxGridDBColumn;
    cxGrid5DBTableView1DBColumn3: TcxGridDBColumn;
    cxGrid5DBTableView1DBColumn4: TcxGridDBColumn;
    cxGrid5DBTableView1DBColumn5: TcxGridDBColumn;
    procedure ExibeRegistros();
    procedure ToolButton4Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure edtDtInicialChange(Sender: TObject);
    procedure edtDtFinalChange(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure ExcluirLanamento1Click(Sender: TObject);
    procedure ExibirTtulosMovimentados1Click(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid5DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConciliaBancoManual: TfrmConciliaBancoManual;

implementation

uses fMenu, fLookup_Padrao, fConciliaBancoManual_IncluiLancto,
  fConciliaBancoManual__ConsultaCheques,
  fConciliaBancoManual_TituloMovimentado,
  fConciliaBancoManual_TituloMovimentado_Cartao;

{$R *.dfm}

procedure TfrmConciliaBancoManual.ExibeRegistros();
begin

    Screen.Cursor := crSQLWait ;

    try

        cmdPreparaTemp.Execute;

        qryPopulaTemp.Parameters.ParamByName('nCdContaBancaria').Value  := qryContaBancarianCdContaBancaria.Value ;
        qryPopulaTemp.Parameters.ParamByName('cDtExtrato').Value        := qryConciliacaoBancariadDtExtrato.AsString ;
        qryPopulaTemp.ExecSQL;

        qrySaldoConta.Close ;
        qrySaldoConta.Parameters.ParamByName('nCdContaBancaria').Value  := qryContaBancarianCdContaBancaria.Value ;
        qrySaldoConta.Parameters.ParamByName('cDtSaldo').Value          := qryConciliacaoBancariadDtExtrato.AsString ;
        qrySaldoConta.Open ;

        Screen.Cursor := crDefault ;

        cxButton4.Click;

        qryConciliados.Close ;
        qryConciliados.Open ;

    except
        Screen.Cursor := crDefault ;
        MensagemErro('Erro no processamento') ;
        raise ;
    end ;

    cdsSaldoCalculadonSaldoFinal.Value := qrySaldoContanSaldo.Value ;

    cxButton1.Enabled   := True ;
    cxButton2.Enabled   := True ;
    ToolButton4.Enabled := True ;

    qryNaoConciliados.Close ;
    qryNaoConciliados.Open ;

    qryConciliados.Close ;
    qryConciliados.Open ;


end ;

procedure TfrmConciliaBancoManual.ToolButton4Click(Sender: TObject);
begin
  inherited;

  if (qryConciliados.Eof) then
  begin
      MensagemAlerta('Nenhum lan�amento conciliado.') ;
      exit ;
  end ;

  if ((StrToFloat(cdsSaldoCalculadonSaldoFinal.AsString) - StrToFloat(qryConciliacaoBancarianSaldoContaBancaria.AsString)) <> 0) then
  begin
      MensagemAlerta('O saldo final calculado n�o confere com o saldo do extrato digitado.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a concilia��o dos lan�amentos ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_PROCESSA_CONCILIACAO.Close;
      SP_PROCESSA_CONCILIACAO.Parameters.ParamByName('@nCdContaBancaria').Value := qryContaBancarianCdContaBancaria.Value ;
      SP_PROCESSA_CONCILIACAO.Parameters.ParamByName('@cDtExtrato').Value       := DBEdit5.Text        ;
      SP_PROCESSA_CONCILIACAO.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado ;
      SP_PROCESSA_CONCILIACAO.ExecProc;

      qryConciliacaoBancarianCdContaBancaria.Value       := qryContaBancarianCdContaBancaria.Value ;
      qryConciliacaoBancarianCdUsuario.Value             := frmMenu.nCdUsuarioLogado;
      qryConciliacaoBancariadDtConciliacaoBancaria.Value := Now() ;
      qryConciliacaoBancaria.Post ;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Lan�amentos conciliados com sucesso.') ;

  close ;

end;

procedure TfrmConciliaBancoManual.cxButton1Click(Sender: TObject);
var
  objForm : TfrmConciliaBancoManual_IncluiLancto ;
begin
  inherited;

  objForm := TfrmConciliaBancoManual_IncluiLancto.Create( Self );

  objForm.nCdContaBancaria := qryContaBancarianCdContaBancaria.Value ;
  objForm.dDtExtrato       := DBEdit5.Text ;

  showForm( objForm , TRUE );

  qryNaoConciliados.Close() ;
  qryNaoConciliados.Open();

  Screen.Cursor := crSQLWait ;

  try

        qrySaldoConta.Close ;
        qrySaldoConta.Parameters.ParamByName('nCdContaBancaria').Value  := qryContaBancarianCdContaBancaria.Value ;
        qrySaldoConta.Parameters.ParamByName('cDtSaldo').Value          := DBEdit5.Text ;
        qrySaldoConta.Open ;

  except
        Screen.Cursor := crDefault ;
        MensagemErro('Erro no processamento') ;
        raise ;
  end ;

  qrySomaLancamentos.Close ;
  qrySomaLancamentos.Open ;

  qryConciliacaoBancarianValCredito.Value := qrySomaLancamentosnValCredito.Value ;
  qryConciliacaoBancarianValDebito.Value  := qrySomaLancamentosnValDebito.Value ;
  cdsSaldoCalculadonSaldoFinal.Value      := qrySaldoContanSaldo.Value - qryConciliacaoBancarianValDebito.Value + qryConciliacaoBancarianValCredito.Value ;

  Screen.Cursor := crDefault ;

end;

procedure TfrmConciliaBancoManual.cxButton2Click(Sender: TObject);
var
    cNrCheque : string ;
    iNrCheque : integer ;
    nCdCheque : integer ;
    objForm   : TfrmConciliaBancoManual__ConsultaCheques ;
begin

    InputQuery('ER2Soft','N�mero do Cheque',cNrCheque) ;

    if (trim(cNrCheque) = '') then
    begin
        cNrCheque := '0' ;
    end ;

    try
        iNrCheque := StrToInt(trim(cNrCheque))
    except
        MensagemAlerta('N�mero de Cheque inv�lido.') ;
        exit ;
    end ;

    qryConsultaCheque.Close ;
    qryConsultaCheque.Parameters.ParamByName('nCdContaBancariaDep').Value := qryContaBancarianCdContaBancaria.Value ;
    qryConsultaCheque.Parameters.ParamByName('iNrCheque').Value           := iNrCheque ;
    qryConsultaCheque.Open ;

    if qryConsultaCheque.Eof then
    begin
        MensagemAlerta('Nenhum cheque com este n�mero depositado nesta conta.') ;
        exit ;
    end ;

    objForm := TfrmConciliaBancoManual__ConsultaCheques.Create( Self ) ;

    objForm.qryCheques.Close ;
    objForm.qryCheques.Parameters.ParamByName('nCdContaBancariaDep').Value := qryContaBancarianCdContaBancaria.Value ;
    objForm.qryCheques.Parameters.ParamByName('iNrCheque').Value           := iNrCheque ;
    objForm.qryCheques.Open ;

    nCdCheque := objForm.ExibeCheques() ;

    freeAndNil( objForm ) ;

    if (nCdCheque > 0) then
    begin

        frmMenu.Connection.BeginTrans;

        try
            SP_REGISTRA_CHEQUE_DEVOLVIDO_EXTRATO.Close ;
            SP_REGISTRA_CHEQUE_DEVOLVIDO_EXTRATO.Parameters.ParamByName('@nCdContaBancariaDep').Value := qryContaBancarianCdContaBancaria.Value ;
            SP_REGISTRA_CHEQUE_DEVOLVIDO_EXTRATO.Parameters.ParamByName('@nCdCheque').Value           := nCdCheque ;
            SP_REGISTRA_CHEQUE_DEVOLVIDO_EXTRATO.Parameters.ParamByName('@cDtExtrato').Value          := DBEdit5.Text;
            SP_REGISTRA_CHEQUE_DEVOLVIDO_EXTRATO.Parameters.ParamByName('@nCdUsuario').Value          := frmMenu.nCdUsuarioLogado;
            SP_REGISTRA_CHEQUE_DEVOLVIDO_EXTRATO.ExecProc;
        except
            frmMenu.Connection.RollbackTrans;
            MensagemErro('Erro no processamento.') ;
            raise ;
        end ;

        frmMenu.Connection.CommitTrans;

        ExibeRegistros() ;

        //se for primeira devolu��o, pergunta se deseja reapresentar automatico

        if (qryConsultaChequedDtDevol.AsString = '') and (qryContaBancariacFlgReapresentaCheque.Value = 1) then
        begin

            case MessageDlg('O Banco ir� reapresentar o cheque ?',mtConfirmation,[mbYes,mbNo],0) of
                mrYes: begin
                    frmMenu.Connection.BeginTrans;

                    try
                        SP_REAPRESENTA_CHEQUE.Close ;
                        SP_REAPRESENTA_CHEQUE.Parameters.ParamByName('@nCdCheque').Value  := nCdCheque ;
                        SP_REAPRESENTA_CHEQUE.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
                        SP_REAPRESENTA_CHEQUE.ExecProc;
                    except
                        frmMenu.Connection.RollbackTrans;
                        MensagemErro('Erro no processamento.') ;
                        raise ;
                    end ;

                    frmMenu.Connection.CommitTrans;

                    ShowMessage('Reapresenta��o registrada com sucesso.') ;
                end ;
            end ;

        end ;

    end ;

end;

procedure TfrmConciliaBancoManual.FormShow(Sender: TObject);
begin
  inherited;

  qryConciliacaoBancaria.Close ;
  qryConciliacaoBancaria.Open ;

  qryConciliacaoBancaria.Insert ;

  cdsSaldoCalculado.CreateDataSet;
  cdsSaldoCalculado.Close ;
  cdsSaldoCalculado.Open ;
  cdsSaldoCalculado.Insert ;

  cxButton1.Enabled := False ;
  cxButton2.Enabled := False ;
  cxButton3.Enabled := True  ;
  cxButton4.Enabled := False ;

  DBEdit5.SetFocus ;

end;



procedure TfrmConciliaBancoManual.cxButton3Click(Sender: TObject);
begin

  if (trim(DBEdit5.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data do extrato.') ;
      DBEdit5.SetFocus ;
      exit ;
  end ;

  if (StrToDate(DBEdit5.Text) >= StrToDate(DateToStr(Now()))) then
  begin
      MensagemAlerta('A data da concilia��o deve ser menor que a data de hoje.') ;
      DBEdit5.SetFocus;
      abort ;
  end ;

  if (qryConciliacaoBancarianSaldoContaBancaria.Value = 0) then
  begin

    case MessageDlg('O saldo final do extrato est� zerado. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo: begin
          DbEdit6.SetFocus ;
          abort ;
      end ;
      mrYes: qryConciliacaoBancarianSaldoContaBancaria.Value := 0 ;
    end ;

  end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM ConciliacaoBancaria WHERE nCdContaBancaria = ' + qryContaBancarianCdContaBancaria.AsString + ' AND dDtExtrato = Convert(DATETIME,' + Chr(39) + DBEdit5.Text + Chr(39) + ',103)') ;
  qryAux.Open ;

  if not qryAux.Eof then
  begin
  
      qryAux.Close ;
      MensagemAlerta('Esta data j� foi conciliada e n�o pode ser alterada.') ;

      qryConciliacaoBancaria.Close ;
      qryConciliacaoBancaria.Open ;

      qryConciliacaoBancaria.Insert ;

      DBEdit5.SetFocus ;
      abort ;

  end ;

  qryAux.Close ;

  edtDtInicial.DateTime := (qryConciliacaoBancariadDtExtrato.Value - 90) ;
  edtDtFinal.DateTime   := qryConciliacaoBancariadDtExtrato.Value ;


  cmdPreparaTemp.Execute;

  qryAux.SQL.Clear ;
  qryAux.SQL.Add('TRUNCATE TABLE #Temp_LanctoFin');
  qryAux.ExecSQL;

  ExibeRegistros() ;

  cxButton1.Enabled := True ;
  cxButton2.Enabled := True ;


  cxButton3.Enabled := False ;
  cxButton4.Enabled := True ;

  DBEdit5.Enabled := False ;

  cxGrid1.SetFocus ;

end;

procedure TfrmConciliaBancoManual.cxButton4Click(Sender: TObject);
begin
  inherited;

  qryNaoConciliados.Close ;
  qryNaoConciliados.Parameters.ParamByName('dDtInicial').Value := DateToStr(edtDtInicial.Date) ;
  qryNaoConciliados.Parameters.ParamByName('dDtFinal').Value   := DateToStr(edtDtFinal.Date) ;
  qryNaoConciliados.Parameters.ParamByName('iTipo').Value      := RadioGroup1.ItemIndex ;
  qryNaoConciliados.Open ;

end;

procedure TfrmConciliaBancoManual.edtDtInicialChange(Sender: TObject);
begin
  inherited;

  qryNaoConciliados.Close ;

end;

procedure TfrmConciliaBancoManual.edtDtFinalChange(Sender: TObject);
begin
  inherited;
  qryNaoConciliados.Close ;

end;

procedure TfrmConciliaBancoManual.RadioGroup1Click(Sender: TObject);
begin
  inherited;
  qryNaoConciliados.Close ;

end;

procedure TfrmConciliaBancoManual.ExcluirLanamento1Click(Sender: TObject);
begin
  inherited;

  if not qryNaoConciliados.eof then
  begin
      PosicionaQuery(qryLanctoFin, qryNaoConciliadosnCdLanctoFin.AsString) ;

      if not qryLanctoFin.eof then
      begin

          if (qryLanctoFincFlgManual.Value = 0) then
          begin
              MensagemAlerta('Somente lan�amento manual pode ser exclu�do.') ;
              exit ;
          end ;

          if (qryLanctoFincFlgConciliado.Value = 1) then
          begin
              MensagemAlerta('Este lan�amento j� foi conciliado e n�o pode ser exclu�do.') ;
              exit ;
          end ;

          if (qryLanctoFinnCdUsuario.Value <> frmMenu.nCdUsuarioLogado) then
          begin
              MensagemAlerta('Somente o usu�rio ' + trim(qryLanctoFincNmUsuario.Value) + ' pode excluir este lan�amento.') ;
              exit ;
          end ;

          case MessageDlg('Confirma a exclus�o deste lan�amento ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo:exit ;
          end ;

          frmMenu.Connection.BeginTrans;

          try
              qryAux.Close ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('DELETE FROM LanctoFin WHERE nCdLanctoFin = ' + qryNaoConciliadosnCdLanctoFin.AsString) ;
              qryAux.ExecSQL;

              qryAux.Close ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('DELETE FROM #Temp_LanctoFin WHERE nCdLanctoFin = ' + qryNaoConciliadosnCdLanctoFin.AsString) ;
              qryAux.ExecSQL;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          frmMenu.Connection.CommitTrans;

          cxButton4.Click;


      end ;

  end ;
end;

procedure TfrmConciliaBancoManual.ExibirTtulosMovimentados1Click(
  Sender: TObject);
var
  objForm : TfrmConciliaBancoManual_TituloMovimentado ;
  objFormCartao : TfrmConciliaBancoManual_TituloMovimentado_Cartao;
begin
  inherited;

  if qryNaoConciliados.Active and (qryNaoConciliadosnCdLanctoFin.Value > 0) then
  begin

      qryLanctoCartao.Close;
      qryLanctoCartao.Parameters.ParamByName('nCdLanctoFin').Value := qryNaoConciliadosnCdLanctoFin.Value;
      qryLanctoCartao.Open;

      if (qryLanctoCartao.RecordCount > 0) then
      begin
          objFormCartao := TfrmConciliaBancoManual_TituloMovimentado_Cartao.Create( Self );
          objFormCartao.exibeTitulos(qryNaoConciliadosnCdLanctoFin.Value);

          freeAndNil( objFormCartao ) ;
      end else
      begin
          objForm := TfrmConciliaBancoManual_TituloMovimentado.Create( Self );
          objForm.exibeTitulos(qryNaoConciliadosnCdLanctoFin.Value);

          freeAndNil( objForm ) ;
      end;
  end ;

end;

procedure TfrmConciliaBancoManual.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  if not qryNaoConciliados.Eof then
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE #Temp_LanctoFin Set cFlgConciliado = 1 WHERE nCdLanctoFin = ' + qryNaoConciliadosnCdLanctoFin.AsString) ;
      qryAux.ExecSQL;

      if (qryConciliacaoBancaria.State = dsBrowse) then
          qryConciliacaoBancaria.Edit ;

      qryConciliacaoBancarianValCredito.Value := qryConciliacaoBancarianValCredito.Value + qryNaoConciliadosnValCredito.Value ;
      qryConciliacaoBancarianValDebito.Value  := qryConciliacaoBancarianValDebito.Value  + qryNaoConciliadosnValDebito.Value  ;
      cdsSaldoCalculadonSaldoFinal.Value      := qrySaldoContanSaldo.Value - qryConciliacaoBancarianValDebito.Value + qryConciliacaoBancarianValCredito.Value ;

      qryNaoConciliados.Close();
      qryNaoConciliados.Open();

      qryConciliados.Close();
      qryConciliados.Open();

  end ;


end;

procedure TfrmConciliaBancoManual.cxGrid5DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  if not qryConciliados.Eof then
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE #Temp_LanctoFin Set cFlgConciliado = 0 WHERE nCdLanctoFin = ' + qryConciliadosnCdLanctoFin.AsString) ;
      qryAux.ExecSQL;

      if (qryConciliacaoBancaria.State = dsBrowse) then
          qryConciliacaoBancaria.Edit ;

      qryConciliacaoBancarianValCredito.Value := qryConciliacaoBancarianValCredito.Value - qryConciliadosnValCredito.Value ;
      qryConciliacaoBancarianValDebito.Value  := qryConciliacaoBancarianValDebito.Value  - qryConciliadosnValDebito.Value  ;
      cdsSaldoCalculadonSaldoFinal.Value      := qrySaldoContanSaldo.Value - qryConciliacaoBancarianValDebito.Value + qryConciliacaoBancarianValCredito.Value ;

      qryNaoConciliados.Close();
      qryNaoConciliados.Open();

      qryConciliados.Close();
      qryConciliados.Open();

  end ;

end;

end.
