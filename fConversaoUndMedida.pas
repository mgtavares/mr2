unit fConversaoUndMedida;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, DBGridEhGrouping, GridsEh, DBGridEh;

type
  TfrmConversaoUndMedida = class(TfrmProcesso_Padrao)
    qryConvUnidadeMedida: TADOQuery;
    DBGridEh1: TDBGridEh;
    qryConvUnidadeMedidanCdConvUnidadeMedida: TIntegerField;
    qryConvUnidadeMedidacUnidadeMedidaDe: TStringField;
    qryConvUnidadeMedidacUnidadeMedidaPara: TStringField;
    dsConvUnidadeMedida: TDataSource;
    qryConvUnidadeMedidanFatorConversaoUM: TFloatField;
    procedure FormShow(Sender: TObject);
    procedure qryConvUnidadeMedidaBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConversaoUndMedida: TfrmConversaoUndMedida;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmConversaoUndMedida.FormShow(Sender: TObject);
begin
  inherited;
  
  qryConvUnidadeMedida.Close;
  qryConvUnidadeMedida.Open;

  qryConvUnidadeMedidanFatorConversaoUM.DisplayFormat := '#,##0.000000';
end;

procedure TfrmConversaoUndMedida.qryConvUnidadeMedidaBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (Trim(qryConvUnidadeMedidacUnidadeMedidaDe.Value) = '') then
  begin
      MensagemAlerta('O nome da Unidade de Medida "De" est� em branco');
      abort;
  end;

  if (Trim(qryConvUnidadeMedidacUnidadeMedidaPara.Value) = '') then
  begin
      MensagemAlerta('O nome da Unidade de Medida "Para" est� em branco');
      abort;
  end;

  if (qryConvUnidadeMedidanFatorConversaoUM.AsFloat <= 0) then
  begin
      MensagemAlerta('Digite um valor v�lido maior que zero para o Fator de convers�o');
      abort;
  end;

  qryConvUnidadeMedidacUnidadeMedidaDe.Value   := UpperCase(qryConvUnidadeMedidacUnidadeMedidaDe.Value);
  qryConvUnidadeMedidacUnidadeMedidaPara.Value := UpperCase(qryConvUnidadeMedidacUnidadeMedidaPara.Value);

  if (qryConvUnidadeMedida.State = dsInsert) then
      qryConvUnidadeMedidanCdConvUnidadeMedida.Value := frmMenu.fnProximoCodigo('CONVUNIDADEMEDIDA');
      
end;

initialization
    RegisterClass(TfrmConversaoUndMedida);

end.
