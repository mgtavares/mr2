unit fCapptAPI;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, OleServer, Cappta_Gp_Api_Com_TLB, ActiveX;

type
  TfrmCapptAPI = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
      cappta: IClienteCappta;
      cnpj: String;
      pdv: Integer;
      chaveAutenticacao: String;
      GPPAtivo : Boolean;
      procedure autenticaPDV;
      procedure configuraInterface;
      procedure iniciaPagtoDebito(valor: Double);
      procedure iniciaPagtoCredito(valor: Double; parcelada: Boolean; qtParcelas: Integer; tpParc: Integer);
      procedure cancelaPagamento(senha: String; controle: String);
      procedure reimprimeCupom(controle: String; tipoVia: Integer);
  end;

var
  frmCapptAPI: TfrmCapptAPI;

implementation

uses
  fMenu;

{$R *.dfm}

procedure TfrmCapptAPI.FormCreate(Sender: TObject);
begin
  cappta            := CoClienteCappta.Create;
  cnpj              := frmMenu.qryConfigAmbientecCNPJTEF.Value;
  pdv               := frmMenu.qryConfigAmbientenPDVTEF.Value;
  chaveAutenticacao := frmMenu.qryConfigAmbientecChaveAutTEF.Value;
  autenticaPDV;
end;

procedure TfrmCapptAPI.autenticaPDV;
var
  respostaAutenticacao: Integer;
begin
   respostaAutenticacao := cappta.AutenticarPdv(cnpj, pdv, chaveAutenticacao);
   if (respostaAutenticacao <> 0) then
    begin
      if (respostaAutenticacao = 1) then
        frmMenu.ShowMessage('N�o foi poss�vel autenticar com o CapptaGpPlus, informa��es fornecidas n�o s�o v�lidas')
      else
        frmMenu.ShowMessage('O CapptaGpPlus est� sendo inicializado, tente novamente em alguns instantes');
      GPPAtivo := False;
      Exit;
    end;
    configuraInterface;
    GPPAtivo := True;
    // Incluido para facilitar monitoramento
    // frmMenu.ShowMessage('O CapptaGpPlus Autenticado');
end;

procedure TfrmCapptAPI.configuraInterface;
var
  configs: IConfiguracoes;
  resultadoConfigs: Integer;
begin
  configs := CoConfiguracoes.Create;
  configs.Set_ExibirInterface(true);

  resultadoConfigs := cappta.Configurar(configs);

  if (resultadoConfigs <> 0) then
    begin
      frmMenu.ShowMessage('N�o foi poss�vel realizar a configura��o');
      Exit;
    end;
end;

procedure TfrmCapptAPI.iniciaPagtoDebito(valor: Double);
var
  resultado: Integer;
begin
  resultado := cappta.PagamentoDebito(valor);
  if resultado <> 0 then
    begin
       frmMenu.ShowMessage('N�o foi poss�vel iniciar a opera��o');
       exit;
    end;
end;

procedure TfrmCapptAPI.iniciaPagtoCredito(valor: Double; parcelada: Boolean; qtParcelas: Integer; tpParc: Integer);
var
  resultado: Integer;
  detalhes: IDetalhesCredito;
begin
  detalhes := CoDetalhesCredito.Create;
  detalhes.Set_TransacaoParcelada(parcelada);
  detalhes.Set_QuantidadeParcelas(qtParcelas);
  detalhes.Set_TipoParcelamento(tpParc);

  resultado := cappta.PagamentoCredito(valor,detalhes);
  if (resultado <> 0) then
    begin
      frmMenu.ShowMessage(Format('N�o foi poss�vel iniciar a opera��o. C�digo de erro %d', [resultado]));
       exit;
    end;
end;

procedure TfrmCapptAPI.cancelaPagamento(senha: String; controle: String);
var
  resultado : integer;
begin
  resultado := cappta.CancelarPagamento(senha,controle);
    if resultado <> 0 then
    begin
       frmMenu.ShowMessage(Format('N�o foi poss�vel iniciar a opera��o. C�digo de erro %d', [resultado]));
       exit;
    end;
end;

procedure TfrmCapptAPI.reimprimeCupom(controle: String; tipoVia: Integer);
var
  resultado : Integer;
begin
  resultado := cappta.ReimprimirCupom(controle, tipoVia);
  if resultado <> 0 then
  begin
    frmMenu.ShowMessage(Format('N�o foi poss�vel iniciar a opera��o. C�digo de erro %d', [resultado]));
    exit;
  end;
end;

initialization
  registerClass(TfrmCapptAPI);

end.
