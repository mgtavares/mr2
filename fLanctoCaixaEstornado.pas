unit fLanctoCaixaEstornado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, StdCtrls, ADODB, DBCtrls, Mask,
  cxLookAndFeelPainters, cxButtons;

type
  TfrmLanctoCaixaEstornado = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryResultado: TADOQuery;
    qryResultadonCdLanctoFin: TAutoIncField;
    qryResultadodDtLancto: TDateTimeField;
    qryResultadodDtEstorno: TDateTimeField;
    qryResultadocNmTipoLancto: TStringField;
    qryResultadonValLancto: TBCDField;
    qryResultadocNmUsuario: TStringField;
    qryResultadonCdConta: TStringField;
    dsResultado: TDataSource;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    MaskEdit2: TMaskEdit;
    Label4: TLabel;
    MaskEdit3: TMaskEdit;
    edtLoja: TMaskEdit;
    edtCaixa: TMaskEdit;
    edtTipoLancto: TMaskEdit;
    qryLoja: TADOQuery;
    qryCaixa: TADOQuery;
    qryTipoLancto: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryCaixanCdContaBancaria: TIntegerField;
    qryCaixanCdConta: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryTipoLanctonCdTipoLancto: TIntegerField;
    qryTipoLanctocNmTipoLancto: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    qryResultadocMotivoEstorno: TStringField;
    cxGridDBTableView1nCdLanctoFin: TcxGridDBColumn;
    cxGridDBTableView1dDtLancto: TcxGridDBColumn;
    cxGridDBTableView1dDtEstorno: TcxGridDBColumn;
    cxGridDBTableView1cNmTipoLancto: TcxGridDBColumn;
    cxGridDBTableView1nValLancto: TcxGridDBColumn;
    cxGridDBTableView1cNmUsuario: TcxGridDBColumn;
    cxGridDBTableView1nCdConta: TcxGridDBColumn;
    cxGridDBTableView1cMotivoEstorno: TcxGridDBColumn;
    qryResultadonCdTipoLancto: TIntegerField;
    qryResultadocHistorico: TStringField;
    cxGridDBTableView1cHistorico: TcxGridDBColumn;
    qryResultadonCdLoja: TStringField;
    cxGridDBTableView1nCdLoja: TcxGridDBColumn;
    qryVerificaTipoLancto: TADOQuery;
    qryVerificaTipoLanctonCdTipoLancto: TIntegerField;
    btConsLancto: TcxButton;
    procedure edtLojaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure edtCaixaExit(Sender: TObject);
    procedure edtCaixaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTipoLanctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTipoLanctoExit(Sender: TObject);
    procedure btConsLanctoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLanctoCaixaEstornado: TfrmLanctoCaixaEstornado;

implementation

uses fMenu, fLookup_Padrao, fConsultaVendaProduto_Dados,
  fConsultaMovtoCaixa_Parcelas;

{$R *.dfm}

procedure TfrmLanctoCaixaEstornado.edtLojaExit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
end;

procedure TfrmLanctoCaixaEstornado.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(147,'EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja = Loja.nCdLoja AND UL.nCdUsuario = @@Usuario)');

        If (nPK > 0) then
        begin
            edtLoja.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;
end;


procedure TfrmLanctoCaixaEstornado.ToolButton1Click(Sender: TObject);
begin
  inherited;
  if (Trim(MaskEdit2.Text) = '/  /') then
      MaskEdit2.Text := DateToStr(Now());

  if (Trim(MaskEdit3.Text) = '/  /') then
      MaskEdit3.Text := DateToStr(Now());

  qryResultado.Close;
  qryResultado.Parameters.ParamByName('nCdLoja').Value          := frmMenu.ConvInteiro(edtLoja.Text);
  qryResultado.Parameters.ParamByName('nCdContaBancaria').Value := frmMenu.ConvInteiro(edtCaixa.Text);
  qryResultado.Parameters.ParamByName('dDtInicial').Value       := frmMenu.ConvData(MaskEdit2.Text);
  qryResultado.Parameters.ParamByName('dDtFinal').Value         := frmMenu.ConvData(MaskEdit3.Text);
  qryResultado.Parameters.ParamByName('nCdTipoLancto').Value    := frmMenu.ConvInteiro(edtTipoLancto.Text);
  qryResultado.Open;

  if (qryResultado.Eof) then
      showMessage('Nenhum lan�amento encontrado para o crit�rio informado.') ;

end;

procedure TfrmLanctoCaixaEstornado.cxGridDBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmConsultaVendaProduto_Dados;
  objFormParcelas : TfrmConsultaMovtoCaixa_Parcelas;
begin
  inherited;

  if (qryResultado.RecordCount = 0) then
      exit;

  if (qryResultadonCdTipoLancto.Value = strtoInt(frmMenu.LeParametro('TIPOLANCVDCX'))) then
  begin
      objForm := TfrmConsultaVendaProduto_Dados.Create(nil);

      PosicionaQuery(objForm.qryItemPedido, qryResultadonCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryCondicao  , qryResultadonCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryPrestacoes, qryResultadonCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryCheques   , qryResultadonCdLanctoFin.AsString) ;
      showForm(objForm,true);

  end ;

  if (qryResultadonCdTipoLancto.Value = strtoInt(frmMenu.LeParametro('TIPOLANCRPCX'))) then
  begin
      objFormParcelas := TfrmConsultaMovtoCaixa_Parcelas.Create(nil);
      
      PosicionaQuery(objFormParcelas.qryParcelas   , qryResultadonCdLanctoFin.AsString) ;
      showForm(objFormParcelas,true);

  end ;

end;

procedure TfrmLanctoCaixaEstornado.edtCaixaExit(Sender: TObject);
begin
  inherited;
  qryCaixa.Close;

  if (Trim(edtCaixa.Text) = '') then
      exit;

  if((Trim(edtLoja.Text) = '') and (Trim(edtCaixa.Text) <> '')) then
  begin
      MensagemAlerta('Selecione uma loja antes de selecionar um Caixa');
      edtLoja.SetFocus;
      edtCaixa.Text := '';
      exit;
  end;

  qryCaixa.Parameters.ParamByName('nCdLoja').Value := edtLoja.Text;
  PosicionaQuery(qryCaixa,edtCaixa.Text);

end;

procedure TfrmLanctoCaixaEstornado.edtCaixaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (Trim(edtLoja.Text) = '') then
        begin
            MensagemAlerta('Selecione uma loja antes de selecionar um Caixa');
            edtLoja.SetFocus;
            exit;
        end;

        nPK := frmLookup_Padrao.ExecutaConsulta2(126,'ContaBancaria.cFlgCofre = 0');

        If (nPK > 0) then
        begin
            edtCaixa.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmLanctoCaixaEstornado.edtTipoLanctoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(130,'TipoLancto.cFlgContaCaixa = 1');

        If (nPK > 0) then
        begin
            edtTipoLancto.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;
end;

procedure TfrmLanctoCaixaEstornado.edtTipoLanctoExit(Sender: TObject);
begin
  inherited;

  qryTipoLancto.Close;
  PosicionaQuery(qryTipoLancto,edtTipoLancto.Text);
end;

procedure TfrmLanctoCaixaEstornado.btConsLanctoClick(Sender: TObject);
var
  cLancto : string ;
  objForm : TfrmConsultaVendaProduto_Dados;
  objFormParcelas : TfrmConsultaMovtoCaixa_Parcelas;
begin
  inherited;
  InputQuery('Consulta de Lan�amento','Informe o N�mero do Lan�amento',cLancto);

  if (Trim(cLancto) <> '') then
  begin
      try
          StrToInt(cLancto);
      except
          MensagemAlerta('N�mero do lan�amento n�o � um n�mero v�lido');
          exit;
      end;
  end
  else exit;

  qryVerificaTipoLancto.Close;
  qryVerificaTipoLancto.Parameters.ParamByName('nCdLanctoFin').Value := cLancto;
  qryVerificaTipoLancto.Open;

  if (qryVerificaTipoLancto.Eof) then
  begin
      MensagemAlerta('Nenhum Lan�amento encontrado para o Crit�rio Utilizado');
      exit;
  end;

  if (qryVerificaTipoLanctonCdTipoLancto.Value = strtoInt(frmMenu.LeParametro('TIPOLANCVDCX'))) then
  begin
      objForm := TfrmConsultaVendaProduto_Dados.Create(nil);

      PosicionaQuery(objForm.qryItemPedido, cLancto) ;
      PosicionaQuery(objForm.qryCondicao  , cLancto) ;
      PosicionaQuery(objForm.qryPrestacoes, cLancto) ;
      PosicionaQuery(objForm.qryCheques   , cLancto) ;
      showForm(objForm,true);

  end ;

  if (qryVerificaTipoLanctonCdTipoLancto.Value = strtoInt(frmMenu.LeParametro('TIPOLANCRPCX'))) then
  begin
      objFormParcelas := TfrmConsultaMovtoCaixa_Parcelas.Create(nil);

      PosicionaQuery(objFormParcelas.qryParcelas   , cLancto) ;
      showForm(objFormParcelas,true);

  end ;


end;

initialization
    RegisterClass(TfrmLanctoCaixaEstornado);
end.
