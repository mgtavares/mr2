unit fOperacaoEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask;

type
  TfrmOperacaoEstoque = class(TfrmCadastro_Padrao)
    qryMasternCdOperacaoEstoque: TIntegerField;
    qryMastercNmOperacaoEstoque: TStringField;
    qryMastercSinalOper: TStringField;
    qryMastercFlgPermManual: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBCheckBox1: TDBCheckBox;
    qryMastercFlgConsumoMedio: TIntegerField;
    DBCheckBox2: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOperacaoEstoque: TfrmOperacaoEstoque;

implementation

{$R *.dfm}

procedure TfrmOperacaoEstoque.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'OPERACAOESTOQUE' ;
  nCdTabelaSistema  := 38 ;
  nCdConsultaPadrao := 84 ;
end;

procedure TfrmOperacaoEstoque.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DBEdit2.Text) = '') then
  begin
      ShowMessage('Informe a descri��o.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  If (DBRadioGroup1.ItemIndex = -1) then
  begin
      ShowMessage('Selecione o efeito no estoque.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmOperacaoEstoque.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

Initialization
    RegisterClass(TfrmOperacaoEstoque) ;

end.
