unit fRoteiroProducao_Etapas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  DB, Mask, DBCtrls, ADODB, cxPC, cxControls, DBGridEhGrouping, GridsEh,
  DBGridEh, cxLookAndFeelPainters, cxButtons, Shellapi;

type
  TfrmRoteiroProducao_Etapas = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    qryProduto: TADOQuery;
    qryTipoOP: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    qryTipoOPnCdTipoOP: TIntegerField;
    qryTipoOPcNmTipoOP: TStringField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    DBEdit4: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    qryRoteiroEtapaProducao: TADOQuery;
    qryRoteiroEtapaProducaonCdRoteiroEtapaProducao: TIntegerField;
    qryRoteiroEtapaProducaonCdRoteiroProducao: TIntegerField;
    qryRoteiroEtapaProducaoiSeq: TIntegerField;
    qryRoteiroEtapaProducaonCdCentroProdutivo: TIntegerField;
    qryRoteiroEtapaProducaonCdLinhaProducao: TIntegerField;
    qryRoteiroEtapaProducaonCdEtapaProducao: TIntegerField;
    qryRoteiroEtapaProducaonQtdeLotePadrao: TBCDField;
    qryRoteiroEtapaProducaonCdTabTipoCalculoEtapa: TIntegerField;
    qryRoteiroEtapaProducaoiTempoPreparacao: TBCDField;
    qryRoteiroEtapaProducaoiTempoSetup: TBCDField;
    qryRoteiroEtapaProducaoiTempoProcesso: TBCDField;
    qryRoteiroEtapaProducaoiTempoEspera: TBCDField;
    qryRoteiroEtapaProducaocInstrucoesProducao: TMemoField;
    qryRoteiroEtapaProducaocArquivoMidia: TStringField;
    dsRoteiroEtapaProducao: TDataSource;
    DBGridEh1: TDBGridEh;
    qryRoteiroEtapaProducaocNmCentroProdutivo: TStringField;
    qryRoteiroEtapaProducaocNmLinhaProducao: TStringField;
    qryRoteiroEtapaProducaocNmTabTipoCalculoEtapa: TStringField;
    qryRoteiroEtapaProducaocNmEtapaProducao: TStringField;
    qryCentroProdutivo: TADOQuery;
    qryCentroProdutivocNmCentroProdutivo: TStringField;
    qryLinhaProducao: TADOQuery;
    qryLinhaProducaocNmLinhaProducao: TStringField;
    qryEtapaProducao: TADOQuery;
    qryEtapaProducaocNmEtapaProducao: TStringField;
    qryEtapaProducaocFlgGeraPedidoComercial: TIntegerField;
    qryTipoCalculoEtapa: TADOQuery;
    qryTipoCalculoEtapacNmTabTipoCalculoEtapa: TStringField;
    GroupBox2: TGroupBox;
    btExibirProdutoEtapa: TcxButton;
    OpenDialog1: TOpenDialog;
    qryRoteiroEtapaProducaonCdTipoMaquinaPCP: TIntegerField;
    qryTipoMaquinaPCP: TADOQuery;
    qryTipoMaquinaPCPnCdTipoMaquinaPCP: TIntegerField;
    qryTipoMaquinaPCPcNmTipoMaquinaPCP: TStringField;
    qryTipoMaquinaPCPnCdCentroProdutivo: TIntegerField;
    qryTipoMaquinaPCPnCdLinhaProducao: TIntegerField;
    qryRoteiroEtapaProducaocNmTipoMaquinaPCP: TStringField;
    procedure exibeRoteiroProducao(nCdProdutoAux, nCdTipoOPAux, nCdRoteiroProducaoAux:integer);
    procedure FormShow(Sender: TObject);
    procedure qryRoteiroEtapaProducaoCalcFields(DataSet: TDataSet);
    procedure qryRoteiroEtapaProducaonCdCentroProdutivoChange(
      Sender: TField);
    procedure qryRoteiroEtapaProducaonCdLinhaProducaoChange(
      Sender: TField);
    procedure qryRoteiroEtapaProducaonCdEtapaProducaoChange(
      Sender: TField);
    procedure qryRoteiroEtapaProducaonCdTabTipoCalculoEtapaChange(
      Sender: TField);
    procedure qryRoteiroEtapaProducaoBeforePost(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure btExibirProdutoEtapaClick(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryRoteiroEtapaProducaonCdTipoMaquinaPCPChange(
      Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTipoOP, nCdProduto, nCdRoteiroProducao : integer ;
  end;

var
  frmRoteiroProducao_Etapas: TfrmRoteiroProducao_Etapas;

implementation

uses fMenu, fMemoText, fProdutoERPInsumos, fLookup_Padrao;

{$R *.dfm}

{ TfrmRoteiroProducao_Etapas }

{ TfrmRoteiroProducao_Etapas }

procedure TfrmRoteiroProducao_Etapas.exibeRoteiroProducao(nCdProdutoAux,
  nCdTipoOPAux, nCdRoteiroProducaoAux: integer);
begin

    nCdProduto         := nCdProdutoAux ;
    nCdTipoOP          := nCdTipoOPAux  ;
    nCdRoteiroProducao := nCdRoteiroProducaoAux ;

    PosicionaQuery(qryProduto             , intToStr( nCdProduto )) ;
    PosicionaQuery(qryTipoOP              , intToStr( nCdTipoOP )) ;
    PosicionaQuery(qryRoteiroEtapaProducao, intToStr( nCdRoteiroProducao )) ;

    Self.ShowModal ;

end;

procedure TfrmRoteiroProducao_Etapas.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.SetFocus ;
  
end;

procedure TfrmRoteiroProducao_Etapas.qryRoteiroEtapaProducaoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryRoteiroEtapaProducao.State in [dsCalcFields,dsBrowse]) then
  begin

      PosicionaQuery(qryCentroProdutivo, qryRoteiroEtapaProducaonCdCentroProdutivo.AsString) ;

      qryLinhaProducao.Parameters.ParamByName('nCdCentroProdutivo').Value := qryRoteiroEtapaProducaonCdCentroProdutivo.Value ;

      PosicionaQuery(qryLinhaProducao, qryRoteiroEtapaProducaonCdLinhaProducao.AsString) ;

      qryTipoMaquinaPCP.Close;
      PosicionaQuery(qryTipoMaquinaPCP, qryRoteiroEtapaProducaonCdTipoMaquinaPCP.AsString) ;

      PosicionaQuery(qryEtapaProducao, qryRoteiroEtapaProducaonCdEtapaProducao.AsString) ;
      PosicionaQuery(qryTipoCalculoEtapa, qryRoteiroEtapaProducaonCdTabTipoCalculoEtapa.AsString) ;

      if not qryCentroProdutivo.eof then
          qryRoteiroEtapaProducaocNmCentroProdutivo.Value := qryCentroProdutivocNmCentroProdutivo.Value;

      if not qryLinhaProducao.Eof then
          qryRoteiroEtapaProducaocNmLinhaProducao.Value := qryLinhaProducaocNmLinhaProducao.Value ;

      if not qryTipoMaquinaPCP.Eof then
          qryRoteiroEtapaProducaocNmTipoMaquinaPCP.Value := qryTipoMaquinaPCPcNmTipoMaquinaPCP.Value ;

      if not qryEtapaProducao.Eof then
          qryRoteiroEtapaProducaocNmEtapaProducao.Value := qryEtapaProducaocNmEtapaProducao.Value ;

      if not qryTipoCalculoEtapa.Eof then
          qryRoteiroEtapaProducaocNmTabTipoCalculoEtapa.Value := qryTipoCalculoEtapacNmTabTipoCalculoEtapa.Value ;

  end ;

end;

procedure TfrmRoteiroProducao_Etapas.qryRoteiroEtapaProducaonCdCentroProdutivoChange(
  Sender: TField);
begin
  inherited;

  if (qryRoteiroEtapaProducao.State = dsInsert) then
  begin
  
    qryRoteiroEtapaProducaonCdTabTipoCalculoEtapa.Value := 1 ;
    qryRoteiroEtapaProducaonQtdeLotePadrao.Value        := 1 ;

  end ;

  if (qryRoteiroEtapaProducao.State in [dsInsert, dsEdit]) then
  begin

      qryRoteiroEtapaProducaocNmCentroProdutivo.Value := '' ;

      PosicionaQuery(qryCentroProdutivo, qryRoteiroEtapaProducaonCdCentroProdutivo.AsString) ;

      if not qryCentroProdutivo.eof then
          qryRoteiroEtapaProducaocNmCentroProdutivo.Value := qryCentroProdutivocNmCentroProdutivo.Value;

  end ;

end;

procedure TfrmRoteiroProducao_Etapas.qryRoteiroEtapaProducaonCdLinhaProducaoChange(
  Sender: TField);
begin
  inherited;

  if (qryRoteiroEtapaProducao.State in [dsInsert, dsEdit]) then
  begin

      qryRoteiroEtapaProducaocNmLinhaProducao.Value := '' ;

      qryLinhaProducao.Parameters.ParamByName('nCdCentroProdutivo').Value := qryRoteiroEtapaProducaonCdCentroProdutivo.Value ;

      PosicionaQuery(qryLinhaProducao, qryRoteiroEtapaProducaonCdLinhaProducao.AsString) ;

      if not qryLinhaProducao.Eof then
          qryRoteiroEtapaProducaocNmLinhaProducao.Value := qryLinhaProducaocNmLinhaProducao.Value ;

  end ;
  
end;

procedure TfrmRoteiroProducao_Etapas.qryRoteiroEtapaProducaonCdEtapaProducaoChange(
  Sender: TField);
begin
  inherited;

  if (qryRoteiroEtapaProducao.State in [dsInsert, dsEdit]) then
  begin

      qryRoteiroEtapaProducaocNmEtapaProducao.Value := '' ;

      PosicionaQuery(qryEtapaProducao, qryRoteiroEtapaProducaonCdEtapaProducao.AsString) ;

      if not qryEtapaProducao.Eof then
          qryRoteiroEtapaProducaocNmEtapaProducao.Value := qryEtapaProducaocNmEtapaProducao.Value ;

  end ;

end;

procedure TfrmRoteiroProducao_Etapas.qryRoteiroEtapaProducaonCdTabTipoCalculoEtapaChange(
  Sender: TField);
begin
  inherited;

  if (qryRoteiroEtapaProducao.State in [dsInsert, dsEdit]) then
  begin

      qryRoteiroEtapaProducaocNmTabTipoCalculoEtapa.Value := '' ;

      PosicionaQuery(qryTipoCalculoEtapa, qryRoteiroEtapaProducaonCdTabTipoCalculoEtapa.AsString) ;

      if not qryTipoCalculoEtapa.Eof then
          qryRoteiroEtapaProducaocNmTabTipoCalculoEtapa.Value := qryTipoCalculoEtapacNmTabTipoCalculoEtapa.Value ;

  end ;

end;

procedure TfrmRoteiroProducao_Etapas.qryRoteiroEtapaProducaoBeforePost(
  DataSet: TDataSet);
begin

  if (qryRoteiroEtapaProducaoiSeq.Value <= 0) then
  begin
      MensagemAlerta('Informe um c�digo para a seq�encia da etapa.') ;
      abort ;
  end ;

  if (qryRoteiroEtapaProducaocNmCentroProdutivo.Value = '') then
  begin
      MensagemAlerta('Informe o centro produtivo.') ;
      abort ;
  end ;

  if (qryRoteiroEtapaProducaocNmLinhaProducao.Value = '') then
  begin
      MensagemAlerta('Informe a linha de produ��o.') ;
      abort ;
  end ;

  if (qryRoteiroEtapaProducaocNmTipoMaquinaPCP.Value = '') and (qryRoteiroEtapaProducaonCdTipoMaquinaPCP.Value > 0) then
  begin
      MensagemAlerta('C�digo de M�quina inv�lido ou inexistente.') ;
      abort ;
  end ;

  if (qryRoteiroEtapaProducaocNmTipoMaquinaPCP.Value <> '') then
  begin
      if (qryTipoMaquinaPCPnCdCentroProdutivo.Value <> qryRoteiroEtapaProducaonCdCentroProdutivo.Value) then
      begin
          MensagemAlerta('Est� m�quina n�o pertence a este centro produtivo.') ;
          abort ;
      end ;

      if (qryTipoMaquinaPCPnCdLinhaProducao.Value <> qryRoteiroEtapaProducaonCdLinhaProducao.Value) then
      begin
          MensagemAlerta('Est� m�quina n�o pertence a esta linah de produ��o.') ;
          abort ;
      end ;
  end ;

  if (qryRoteiroEtapaProducaocNmEtapaProducao.Value = '') then
  begin
      MensagemAlerta('Informe a etapa da produ��o.') ;
      abort ;
  end ;

  if (qryRoteiroEtapaProducaocNmTabTipoCalculoEtapa.Value = '') then
  begin
      MensagemAlerta('Informe o tipo de c�lculo da etapa.') ;
      abort ;
  end ;

  if (qryRoteiroEtapaProducaonQtdeLotePadrao.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade do lote padr�o.') ;
      abort ;
  end ;

  if (qryRoteiroEtapaProducaoiTempoPreparacao.Value < 0) or (not frmMenu.fnValidaHoraDecimal( qryRoteiroEtapaProducaoiTempoPreparacao.Value )) then
  begin
      MensagemAlerta('Tempo de prepara��o inv�lido. Ex: 0,05 = 05 minutos / 1,30 = 01 hora e meia.') ;
      abort ;
  end ;

  if (qryRoteiroEtapaProducaoiTempoSetup.Value < 0) or (not frmMenu.fnValidaHoraDecimal( qryRoteiroEtapaProducaoiTempoSetup.Value )) then
  begin
      MensagemAlerta('Tempo de setup inv�lido. Ex: 0,05 = 05 minutos / 1,30 = 01 hora e meia.') ;
      abort ;
  end ;

  if (qryRoteiroEtapaProducaoiTempoProcesso.Value < 0) or (not frmMenu.fnValidaHoraDecimal( qryRoteiroEtapaProducaoiTempoProcesso.Value )) then
  begin
      MensagemAlerta('Tempo de processo inv�lido. Ex: 0,05 = 05 minutos / 1,30 = 01 hora e meia.') ;
      abort ;
  end ;

  if (qryRoteiroEtapaProducaoiTempoEspera.Value < 0) or (not frmMenu.fnValidaHoraDecimal( qryRoteiroEtapaProducaoiTempoEspera.Value )) then
  begin
      MensagemAlerta('Tempo de espera inv�lido. Ex: 0,05 = 05 minutos / 1,30 = 01 hora e meia.') ;
      abort ;
  end ;

  inherited;

  if (qryRoteiroEtapaProducaonCdRoteiroEtapaProducao.Value = 0) then
      qryRoteiroEtapaProducaonCdRoteiroEtapaProducao.Value := frmMenu.fnProximoCodigo( 'ROTEIROETAPAPRODUCAO' ) ;

  qryRoteiroEtapaProducaonCdRoteiroProducao.Value := nCdRoteiroProducao ;

end;

procedure TfrmRoteiroProducao_Etapas.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmRoteiroProducao_Etapas.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmRoteiroProducao_Etapas.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  objMemo : TfrmMemoText ;
begin
  inherited;

  if (Shift = [ssCtrl]) and (key = vk_return) then
  begin

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cInstrucoesProducao') then
      begin

          if (qryRoteiroEtapaProducao.Active) and (qryRoteiroEtapaProducao.RecordCount > 0) then
          begin

              objMemo := TfrmMemoText.Create( Self );

              objMemo.campoMemo.Clear;
              objMemo.campoMemo.Lines.Add( qryRoteiroEtapaProducaocInstrucoesProducao.Value ) ;

              showForm( objMemo , FALSE ) ;

              if (qryRoteiroEtapaProducao.State = dsBrowse) then
                  qryRoteiroEtapaProducao.Edit ;

              qryRoteiroEtapaProducaocInstrucoesProducao.Value := objMemo.campoMemo.Lines.Text ;

              freeAndNil( objMemo ) ;

          end ;

      end ;

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cArquivoMidia') then
      begin

          if (qryRoteiroEtapaProducaocArquivoMidia.Value <> '') then
              ShellExecute(0, nil, pAnsiChar( Trim( qryRoteiroEtapaProducaocArquivoMidia.Value ) ), nil, nil, SW_NORMAL);

      end ;

  end ;

end;

procedure TfrmRoteiroProducao_Etapas.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  btExibirProdutoEtapa.Click;

end;

procedure TfrmRoteiroProducao_Etapas.btExibirProdutoEtapaClick(
  Sender: TObject);
var
  objForm : TfrmProdutoERPInsumos ;
begin
  inherited;

  if (not qryRoteiroEtapaProducao.Active) or (qryRoteiroEtapaProducaonCdRoteiroEtapaProducao.Value = 0) then
  begin
      MensagemAlerta('Nenhuma etapa selecionada.') ;
      abort ;
  end ;

  objForm := TfrmProdutoERPInsumos.Create( Self ) ;

  objForm.exibeEstruturaEtapaProducao( nCdProduto
                                     , qryRoteiroEtapaProducaonCdRoteiroEtapaProducao.Value ) ;

  freeAndNil( objForm ) ;

end;

procedure TfrmRoteiroProducao_Etapas.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    objMemo : TfrmMemoText ;
    nPK     : integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdCentroProdutivo') then
        begin

            if (qryRoteiroEtapaProducao.State = dsBrowse) then
                 qryRoteiroEtapaProducao.Edit ;

            if (qryRoteiroEtapaProducao.State = dsInsert) or (qryRoteiroEtapaProducao.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(738);

                If (nPK > 0) then
                    qryRoteiroEtapaProducaonCdCentroProdutivo.Value := nPK ;

            end ;

        end ;

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdLinhaProducao') then
        begin

            if (qryRoteiroEtapaProducaocNmCentroProdutivo.Value = '') then
            begin
                MensagemAlerta('Selecione um centro produtivo.') ;
                abort ;
            end ;

            if (qryRoteiroEtapaProducao.State = dsBrowse) then
                 qryRoteiroEtapaProducao.Edit ;

            if (qryRoteiroEtapaProducao.State = dsInsert) or (qryRoteiroEtapaProducao.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(207,'LinhaProducao.nCdCentroProdutivo = ' + qryRoteiroEtapaProducaonCdCentroProdutivo.asString);

                If (nPK > 0) then
                    qryRoteiroEtapaProducaonCdLinhaProducao.Value := nPK ;

            end ;

        end ;

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdTipoMaquinaPCP') then
        begin

            if (qryRoteiroEtapaProducao.State = dsBrowse) then
                 qryRoteiroEtapaProducao.Edit ;

            if (qryRoteiroEtapaProducao.State = dsInsert) or (qryRoteiroEtapaProducao.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(209);

                If (nPK > 0) then
                    qryRoteiroEtapaProducaonCdTipoMaquinaPCP.Value := nPK ;

            end ;

        end ;

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdEtapaProducao') then
        begin

            if (qryRoteiroEtapaProducao.State = dsBrowse) then
                 qryRoteiroEtapaProducao.Edit ;

            if (qryRoteiroEtapaProducao.State = dsInsert) or (qryRoteiroEtapaProducao.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(741);

                If (nPK > 0) then
                    qryRoteiroEtapaProducaonCdEtapaProducao.Value := nPK ;

            end ;

        end ;

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdTabTipoCalculoEtapa') then
        begin

            if (qryRoteiroEtapaProducao.State = dsBrowse) then
                 qryRoteiroEtapaProducao.Edit ;

            if (qryRoteiroEtapaProducao.State = dsInsert) or (qryRoteiroEtapaProducao.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(208);

                If (nPK > 0) then
                    qryRoteiroEtapaProducaonCdTabTipoCalculoEtapa.Value := nPK ;

            end ;

        end ;

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cInstrucoesProducao') then
        begin

            if (qryRoteiroEtapaProducao.State = dsBrowse) then
                 qryRoteiroEtapaProducao.Edit ;

            if (qryRoteiroEtapaProducao.State = dsInsert) or (qryRoteiroEtapaProducao.State = dsEdit) then
            begin

              objMemo := TfrmMemoText.Create( Self );

              objMemo.campoMemo.Clear;
              objMemo.campoMemo.Lines.Add( qryRoteiroEtapaProducaocInstrucoesProducao.Value ) ;

              showForm( objMemo , FALSE ) ;

              if (qryRoteiroEtapaProducao.State = dsBrowse) then
                  qryRoteiroEtapaProducao.Edit ;

              qryRoteiroEtapaProducaocInstrucoesProducao.Value := objMemo.campoMemo.Lines.Text ;

              freeAndNil( objMemo ) ;

          end ;

        end ;

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cArquivoMidia') then
        begin

          if (qryRoteiroEtapaProducao.State = dsBrowse) then
               qryRoteiroEtapaProducao.Edit ;

          if (qryRoteiroEtapaProducao.State = dsInsert) or (qryRoteiroEtapaProducao.State = dsEdit) then
          begin

              if (OpenDialog1.Execute) then
                  if (OpenDialog1.Files.Text <> '') then
                  begin

                      if (qryRoteiroEtapaProducao.State = dsBrowse) then
                          qryRoteiroEtapaProducao.Edit ;

                      if (qryRoteiroEtapaProducao.State in [dsInsert,dsEdit]) then
                          qryRoteiroEtapaProducaocArquivoMidia.Value := OpenDialog1.Files.Text ;

                  end ;

          end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRoteiroProducao_Etapas.qryRoteiroEtapaProducaonCdTipoMaquinaPCPChange(
  Sender: TField);
begin
  inherited;

  if (qryRoteiroEtapaProducao.State in [dsInsert, dsEdit]) then
  begin

      qryRoteiroEtapaProducaocNmTipoMaquinaPCP.Value := '' ;

      PosicionaQuery(qryTipoMaquinaPCP, qryRoteiroEtapaProducaonCdTipoMaquinaPCP.AsString) ;

      if not qryTipoMaquinaPCP.eof then
          qryRoteiroEtapaProducaocNmTipoMaquinaPCP.Value := qryTipoMaquinaPCPcNmTipoMaquinaPCP.Value;

  end ;

end;

end.
