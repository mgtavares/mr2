program Er2Werp;

uses
  Forms,
  fBuscaPK in 'fBuscaPK.pas' {frmBuscaPK},
  fMensagemPadrao in 'fMensagemPadrao.pas',
  fMenu in 'fMenu.pas' {frmMenu},
  fAddServer in 'fAddServer.pas' {frmAddServer},
  fAlertas in 'fAlertas.pas' {frmAlertas},
  fAltSenha in 'fAltSenha.pas' {frmAltSenha},
  fAuditoria in 'fAuditoria.pas' {frmAuditoria},
  fCadastro_Template in 'fCadastro_Template.pas' {frmCadastro_Padrao},
  fConfigAmbiente in 'fConfigAmbiente.pas' {frmConfigAmbiente},
  fLogin in 'fLogin.pas' {frmLogin},
  fLookup_Padrao in 'fLookup_Padrao.pas' {frmLookup_Padrao},
  fMensagem in 'fMensagem.pas' {frmMensagem},
  fProcesso_Padrao in 'fProcesso_Padrao.pas' {frmProcesso_Padrao},
  frmSQLWait in 'frmSQLWait.pas' {frmWait},
  fSelecEmpresa in 'fSelecEmpresa.pas' {frmSelecEmpresa},
  fSelecLoja in 'fSelecLoja.pas' {frmSelecLoja},
  fSelecServer in 'fSelecServer.pas' {frmSelecServer},
  fTimedMessage in 'fTimedMessage.pas' {frmTimedMessage},
  fUtils in 'fUtils.pas',
  fPedidoVendaWERP in 'fPedidoVendaWERP.pas' {frmPedidoVendaWERP},
  fFollowUPWERP in 'fFollowUPWERP.pas' {frmFollowUPWERP},
  dcVendaProdutoCliente_view in 'dcVendaProdutoCliente_view.pas' {dcmVendaProdutoCliente_view},
  fConsultaPedComAberTerceiro in 'fConsultaPedComAberTerceiro.pas' {frmConsultaPedComAberTerceiro},
  fConsultaPedComAberTerceiro_Itens in 'fConsultaPedComAberTerceiro_Itens.pas' {frmConsultaPedComAberTerceiro_Itens},
  fItemPedidoAtendido in 'fItemPedidoAtendido.pas' {frmItemPedidoAtendido},
  rPedidoVenda in 'rPedidoVenda.pas' {rptPedidoVenda},
  fFiltroFollowUp in 'fFiltroFollowUp.pas' {frmFiltroFollowUp},
  dcVendaProdutoClienteWERP in 'dcVendaProdutoClienteWERP.pas' {dcmVendaProdutoClienteWERP},
  fConsTituloAbertoWERP in 'fConsTituloAbertoWERP.pas' {frmConsTituloAbertoWERP},
  fConsultaNotaFiscalWERP in 'fConsultaNotaFiscalWERP.pas' {frmConsultaNotaFiscalWERP},
  fConsultaPagamentoWERP in 'fConsultaPagamentoWERP.pas' {frmConsultaPagamentoWERP},
  rRankingFatAnualProdutoWERP in 'rRankingFatAnualProdutoWERP.pas',
  fRelatorio_Padrao in 'fRelatorio_Padrao.pas' {frmRelatorio_Padrao},
  rRomaneio in 'rRomaneio.pas' {rptRomaneio},
  rBaixaColetiva_view in 'rBaixaColetiva_view.pas' {rptBaixaColetiva_view},
  fSugerirCheques in 'fSugerirCheques.pas' {frmSugerirCheques},
  fBaixaColetiva_ConsTitulo in 'fBaixaColetiva_ConsTitulo.pas' {frmBaixaColetiva_ConsTitulo},
  rRankingFatAnualProduto_view in 'rRankingFatAnualProduto_view.pas' {rptRankingFatAnualProduto_view},
  fEmbFormula in 'fEmbFormula.pas' {frmEmbFormula},
  fSplash in 'fSplash.pas' {SplashScreen},
  fErroPadrao in 'fErroPadrao.pas' {frmErroPadrao};

{$R *.res}

begin
  SplashScreen := TSplashScreen.Create(Application) ;
  SplashScreen.Show;
  Application.Initialize; //this line exists!
  SplashScreen.Update;

  Application.Title := 'ER2Soft - WERP';
  Application.CreateForm(TfrmMenu, frmMenu);
  Application.CreateForm(TfrmBuscaPK, frmBuscaPK);
  Application.CreateForm(TfrmAddServer, frmAddServer);
  Application.CreateForm(TfrmAlertas, frmAlertas);
  Application.CreateForm(TfrmAltSenha, frmAltSenha);
  Application.CreateForm(TfrmAuditoria, frmAuditoria);
  Application.CreateForm(TfrmCadastro_Padrao, frmCadastro_Padrao);
  Application.CreateForm(TfrmConfigAmbiente, frmConfigAmbiente);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TfrmLookup_Padrao, frmLookup_Padrao);
  Application.CreateForm(TfrmMensagem, frmMensagem);
  Application.CreateForm(TfrmProcesso_Padrao, frmProcesso_Padrao);
  Application.CreateForm(TfrmWait, frmWait);
  Application.CreateForm(TfrmSelecEmpresa, frmSelecEmpresa);
  Application.CreateForm(TfrmSelecLoja, frmSelecLoja);
  Application.CreateForm(TfrmSelecServer, frmSelecServer);
  Application.CreateForm(TfrmPedidoVendaWERP, frmPedidoVendaWERP);
  Application.CreateForm(TfrmFollowUPWERP, frmFollowUPWERP);
  Application.CreateForm(TdcmVendaProdutoCliente_view, dcmVendaProdutoCliente_view);
  Application.CreateForm(TfrmConsultaPedComAberTerceiro, frmConsultaPedComAberTerceiro);
  Application.CreateForm(TfrmConsultaPedComAberTerceiro_Itens, frmConsultaPedComAberTerceiro_Itens);
  Application.CreateForm(TfrmItemPedidoAtendido, frmItemPedidoAtendido);
  Application.CreateForm(TrptPedidoVenda, rptPedidoVenda);
  Application.CreateForm(TfrmFiltroFollowUp, frmFiltroFollowUp);
  Application.CreateForm(TdcmVendaProdutoClienteWERP, dcmVendaProdutoClienteWERP);
  Application.CreateForm(TfrmConsTituloAbertoWERP, frmConsTituloAbertoWERP);
  Application.CreateForm(TfrmConsultaNotaFiscalWERP, frmConsultaNotaFiscalWERP);
  Application.CreateForm(TfrmConsultaPagamentoWERP, frmConsultaPagamentoWERP);
  Application.CreateForm(TfrmRelatorio_Padrao, frmRelatorio_Padrao);
  Application.CreateForm(TrptRomaneio, rptRomaneio);
  Application.CreateForm(TrptBaixaColetiva_view, rptBaixaColetiva_view);
  Application.CreateForm(TfrmSugerirCheques, frmSugerirCheques);
  Application.CreateForm(TfrmBaixaColetiva_ConsTitulo, frmBaixaColetiva_ConsTitulo);
  Application.CreateForm(TrptRankingFatAnualProduto_view, rptRankingFatAnualProduto_view);
  Application.CreateForm(TfrmEmbFormula, frmEmbFormula);
  Application.CreateForm(TfrmErroPadrao, frmErroPadrao);
  {Application.CreateForm(TSplashScreen, SplashScreen);}

  SplashScreen.Hide;
  SplashScreen.Free;

  Application.Run;
end.
