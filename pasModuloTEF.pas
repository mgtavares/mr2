unit pasModuloTEF;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls;

// Fun��o para bloquear o teclado
function BlockInput( fBlockIt:boolean):Boolean;  StdCall; External 'User32.dll';

// Fun��es do Gerenciamento do TEF

function VerificaGerenciadorPadrao: boolean;
function RealizaTransacao( cIdentificacao: TDateTime; cNumeroCupom: string; cValorPago: string; iConta: integer): integer;
function ImprimeTransacao( cFormaPGTO: string; cValorPago: string; cCOO: string; cIdentificacao: TDateTime; iConta: integer ): boolean;
function ConfirmaTransacao( iConta: integer ): boolean;
function NaoConfirmaTransacao( iConta: integer ): boolean;
function CancelaTransacaoTEF( cNSU: string; cValor: string; cNomeRede: string; cNumeroDOC: string; cData: string; cHora: string; iVezes: integer ): boolean;
function ImprimeGerencial( iConta: integer ): integer;
function FuncaoAdministrativaTEF( cIdentificacao: TDateTime ): integer;
//function VerificaRetornoFuncaoImpressora( iRetorno: integer ): boolean;

implementation

uses fCaixa_Recebimento, fMenu, fTimedMessage;

////////////////////////////////////////////////////////////////////////////////
//
// Fun��o:
//    VerificaGerenciadorPadrao
// Objetivo:
//    Verificar se o Gerenciador Padr�o est� ativo
// Par�metro:
//    n�o h�
// Retorno:
//    True para Gerenciador Padr�o ATIVO
//    False para Gerenciador Padr�o INATIVO
//
////////////////////////////////////////////////////////////////////////////////
function VerificaGerenciadorPadrao: boolean;
var
   cConteudoArquivo: string;
   cIdentificacao  : TDateTime;
   cArquivo        : TextFile;
   iTentativas     : integer;
   lFlag           : longbool;
begin
   AssignFile( cArquivo, 'INTPOS.001');
   cConteudoArquivo := '';
   cIdentificacao   := Time;
   cConteudoArquivo := '000-000 = ATV' + #13 + #10 +
                       '001-000 = ' + FormatDateTime( 'hhmmss', cIdentificacao ) + #13 + #10 +
                       '999-999 = 0';
   ReWrite( cArquivo );
   WriteLn( cArquivo, cConteudoArquivo );
   CloseFile( cArquivo );

   CopyFile( pchar( 'INTPOS.001' ), pchar( 'C:\TEF_DIAL\REQ\INTPOS.001' ), lFlag );
   DeleteFile( 'INTPOS.001' );

   for iTentativas := 1 to 7 do
      begin
         if ( FileExists( 'C:\TEF_DIAL\RESP\ATIVO.001' ) ) or ( FileExists( 'C:\TEF_DIAL\RESP\INTPOS.STS' ) ) then
            begin
               result := True;
               break;
            end;

         Sleep( 1000 );
         if ( iTentativas = 7 ) then
            begin
               result := False;
               Break;
            end;
      end;
end;

////////////////////////////////////////////////////////////////////////////////
//
// Fun��o:
//    RealizaTransacao
// Objetivo:
//    Realiza a transa��o TEF
// Par�metros:
//   TDateTime para identificar o n�mero da transa��o
//   String para o N�mero do Cupom Fiscal (COO)
//   String para a Valor da Forma de Pagamento
//   Integer com o n�mero da transa��o
// Retorno:
//    True para OK
//    False para n�o OK
//
////////////////////////////////////////////////////////////////////////////////
function RealizaTransacao( cIdentificacao: TDateTime; cNumeroCupom: string;
                           cValorPago: string; iConta: integer): integer;
var
   cConteudoArquivo, cLinhaArquivo, cLinha, cCampoArquivo: string;
   cArquivo  : TextFile;
   lFlag     : longbool;
   iTentativas, iVezes: integer;
   cMensagem : TForm;
   bTransacao: boolean;
begin

   AssignFile( cArquivo, 'INTPOS.001');

   // Conte�do do arquivo INTPOS.001 para solicitar a transa��o TEF.

   cConteudoArquivo := '';
   cConteudoArquivo := '000-000 = CRT' + #13 + #10 +
                       '001-000 = ' + FormatDateTime( 'hhmmss', cIdentificacao ) + #13 + #10 +
                       '002-000 = ' + cNumeroCupom + #13 + #10 +
                       '003-000 = ' + cValorPago + #13 + #10 +
                       '999-999 = 0';
   ReWrite( cArquivo );
   WriteLn( cArquivo, cConteudoArquivo );
   CloseFile( cArquivo );

   CopyFile( pchar( 'INTPOS.001' ), pchar( 'C:\TEF_DIAL\REQ\INTPOS.001' ), lFlag );
   DeleteFile( 'INTPOS.001' );

   if FileExists( 'IMPRIME' + inttostr( iConta ) + '.TXT' ) then
      DeleteFile( 'IMPRIME' + inttostr( iConta ) + '.TXT' );

   result := -2;

   iTentativas := 0 ;

   for iTentativas := 1 to 7 do
      begin

         // Verifica se o Gerenciador Padr�o recebeu o INTPOS.001 da solicita��o.
         if ( FileExists( 'C:\TEF_DIAL\RESP\INTPOS.STS' ) ) then
            begin
               cLinhaArquivo := '';
               cLinha := '';

               while ( result = -2 ) do
                  begin
                     if FileExists( 'C:\TEF_DIAL\RESP\INTPOS.001' ) then // Verifica o arquivo INTPOS.001 de resposta.
                        begin

                           if ( FileExists( 'C:\TEF_DIAL\REQ\INTPOS.001' ) ) then
                               DeleteFile( 'C:\TEF_DIAL\REQ\INTPOS.001' );

                           Sleep(1000) ;

                           try
                               AssignFile( cArquivo, 'C:\TEF_DIAL\RESP\INTPOS.001' );
                           except
                               TfrmTimedMessage.ShowMessage('N�o foi poss�vel abrir o arquivo de resposta da administradora de cart�o.',0,'Aviso') ;
                               frmCaixa_Recebimento.lblMensagem.Caption := '' ;
                               abort ;
                           end ;

                           try
                               Reset( cArquivo );
                           except
                               TfrmTimedMessage.ShowMessage('N�o foi poss�vel acessar o arquivo de resposta da administradora de cart�o.',0,'Aviso') ;
                               frmCaixa_Recebimento.lblMensagem.Caption := '' ;
                               abort ;
                           end ;

                           while not EOF( cArquivo ) do
                              begin

                                 ReadLn( cArquivo, cLinhaArquivo );
                                 cCampoArquivo := copy( cLinhaArquivo, 1, 3 );

                                 case StrToInt( cCampoArquivo ) of

                                    1: // Verifica se o campo de identifica��o � o mesmo do solicitado.
                                       if ( copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ) <> FormatDateTime( 'hhmmss', cIdentificacao ) ) then
                                          begin
                                             CloseFile( cArquivo );
                                             break;
                                          end;

                                    9: // Verifica se a Transa��o foi Aprovada.
                                       begin
                                          if ( copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ) ) = '0' then
                                             begin
                                                bTransacao := True;
                                                result := 1;
                                             end;
                                          if ( copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ) ) <> '0' then
                                             begin
                                                bTransacao := False;
                                                result := -1;
                                             end;
                                          end;

                                    10: // Obtem o nome da administradora
                                        begin
                                            frmMenu.cCartaoOperadora := copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ) ;
                                        end ;

                                    12: // Obtem o numero do documento
                                        begin
                                            frmMenu.cCartaoNSU := copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ) ;
                                        end ;

                                    13: // Obtem o numero da autoriza��o
                                        begin
                                            frmMenu.cCartaoTransacao := copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ) ;
                                        end ;

                                    28: // Verifica se existem linhas para serem impressas.
                                       if ( StrToInt( copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ) ) <> 0 ) and ( bTransacao = True ) then
                                          begin

                                             // � realizada uma c�pia tempor�ria do arquivo INTPOS.001 para cada transa��o efetuada.
                                             // Caso a transa��o necessite ser cancelada, as informa��es estar�o neste arquivo.
                                             CopyFile( pchar( 'C:\TEF_DIAL\RESP\INTPOS.001' ), pchar( 'C:\TEF_DIAL\RESP\INTPOS' + inttostr( iConta ) + '.001' ), lFlag );

                                             result := 1;
                                             for iVezes := 1 to StrToInt( copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ) ) do
                                                begin
                                                   ReadLn( cArquivo, cLinhaArquivo );
                                                   if copy( cLinhaArquivo, 1, 3 ) = '029' then
                                                      cLinha := cLinha + copy( cLinhaArquivo, 12, Length( cLinhaArquivo ) - 12 ) + #13 + #10;
                                                end;
                                          end;

                                    30: // Verifica se o campo � o 030 para mostrar a mensagem para o operador
                                       if cLinha <> '' then
                                          begin
                                             // Mensagem somente para exibicao
                                             frmCaixa_Recebimento.Refresh ;
                                             frmMenu.MensagemPadrao((copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 )),0,5000) ;
                                          end
                                       else
                                          begin
                                             while not FileExists( 'C:\TEF_DIAL\RESP\INTPOS.STS' ) do sleep( 1000 );
                                             if ( FileExists( 'C:\TEF_DIAL\REQ\INTPOS.001' ) ) then
                                                   DeleteFile( 'C:\TEF_DIAL\REQ\INTPOS.001' );

                                             // Mensagem aguardando OK
                                             frmCaixa_Recebimento.Repaint;
                                             frmMenu.MensagemPadrao(copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ),1,0) ;
                                             result := -1;
                                          end;


                                 end;
                              end;
                        end;
                  end;

                  // Cria o arquivo tempor�rio IMPRIME.TXT com a imagem do comprovante
                  if ( cLinha <> '' ) then
                     begin
                        CloseFile( cArquivo );
                        AssignFile( cArquivo, 'IMPRIME' + inttostr( iConta ) + '.TXT' );
                        ReWrite( cArquivo );
                        WriteLn( cArquivo, cLinha );
                        CloseFile( cArquivo );
                        Break;
                     end;
            end;

         Sleep( 1000 );
            
         // O arquivo INTPOS.STS n�o retornou em 7 segundos, ent�o o operador � informado.
         if ( iTentativas = 7 ) then
            begin
               if ( FileExists( 'C:\TEF_DIAL\REQ\INTPOS.001' ) ) then
                  DeleteFile( 'C:\TEF_DIAL\REQ\INTPOS.001' );

               frmCaixa_Recebimento.Repaint;
               frmMenu.MensagemPadrao ('Gerenciador Padr�o n�o est� ativo!',0,5000) ;
               result := 0;
               break;
            end;
         if ( result = 0 ) or ( result = -1 )then
            begin
               CloseFile( cArquivo );
               break;
            end;
       end;
       if ( result = 1 ) then
          begin
             result := 1;
             AssignFile( cArquivo, 'PENDENTE.TXT' );
             ReWrite( cArquivo );
             WriteLn( cArquivo, inttostr( iConta ) );
             CloseFile( cArquivo );
          end;

   if ( FileExists( 'C:\TEF_DIAL\RESP\INTPOS.STS' ) ) then
      DeleteFile( 'C:\TEF_DIAL\RESP\INTPOS.STS' );

   if ( FileExists( 'C:\TEF_DIAL\RESP\INTPOS.001' ) ) then
      DeleteFile( 'C:\TEF_DIAL\RESP\INTPOS.001' );

end;

////////////////////////////////////////////////////////////////////////////////
//
// Fun��o:
//    ImprimeTransacao
// Objetivo:
//    Realiza a impress�o da Transa��o TEF
// Par�metros:
//   String para a Forma de Pagamento
//   String para a Valor da Forma de Pagamento
//   String para o N�mero do Cupom Fiscal (COO)
//   TDateTime para identificar o n�mero da transa��o
//   Integer com o n�mero da transa��o
// Retorno:
//    True para OK
//    False para n�o OK
//
////////////////////////////////////////////////////////////////////////////////
function ImprimeTransacao( cFormaPGTO: string; cValorPago: string;
                           cCOO: string; cIdentificacao: TDateTime; iConta: integer ): boolean;
var
   cLinhaArquivo, cLinha, cSaltaLinha: string;
   cMensagem: TForm;
   cArquivo : TextFile;
   iVezes, iRetorno, iVias, iTipoImpressora, iErro: integer;
begin

   // Neste ponto � criado o arquivo TEF.TXT, indicando que h� uma
   // opera��o de TEF sendo realizada. Caso ocorra uma queda de energia,
   // no momento da impress�o do TEF, e a aplica��o for inicializada,
   // ao identificar a exist�ncia deste arquivo, a transa��o do TEF
   // dever� ser concelada.
   AssignFile( cArquivo, 'TEF.TXT');
   ReWrite( cArquivo );
   WriteLn( cArquivo, IntToStr( iTransacao ) );
   CloseFile( cArquivo );

   result := true;
   if FileExists( 'IMPRIME' + inttostr( iConta ) + '.TXT' ) then
      begin

         // Fun��o que realiza o bloqueio do teclado e do mouse para a impress�o do TEF
         frmMenu.ACBrECF1.BloqueiaMouseTeclado := True ;

         // Fun��o que realiza a abertura do comprovante n�o fiscal vinculado.
         if not Gerencial then
            frmMenu.ACBrECF1.AbreCupomVinculado (pchar( cCOO ) , pchar( cFormaPGTO ), StrToFloat(cValorPago));

            for iVias := 1 to iQtdeVias do
                begin
                   AssignFile( cArquivo, 'IMPRIME' + inttostr( iConta ) + '.TXT' );
                   Reset( cArquivo );
                   cLinha := '';
                   while not EOF( cArquivo ) do
                      begin
                         ReadLn( cArquivo, cLinha );

                         // Fun��o que realiza a impress�o no comprovante n�o fiscal vinculado.
                         if not Gerencial then
                            frmMenu.ACBrECF1.LinhaCupomVinculado ( pchar( cLinha ) + #13 + #10 )
                         else
                            frmMenu.ACBrECF1.LinhaRelatorioGerencial ( pchar( cLinha ) + #13 + #10 );

                            if not (frmMenu.ACBrECF1.Ativo) then
                            begin
                                result := False;
                                iErro  := 1;
                                break;
                            end

                         else
                            if ( EOF( cArquivo ) ) and ( iVias <> 2 ) and ( iQtdeVias > 1 ) then
                                begin
                                   cSaltaLinha := #13 + #10 + #13 + #10 + #13 + #10 + #13 + #10 + #13 + #10;

                                   // Fun��o que realiza a impress�o no comprovante n�o fiscal vinculado.
                                   frmMenu.ACBrECF1.LinhaCupomVinculado ( pchar( cSaltaLinha) );

                                  // Fun��o que verifica o tipo da impressora fiscal.
                                  // Se for a impressora com cutter, � acionado o corte do papel (guilhotina).
                                  if (frmMenu.ACBrECF1.MFD) then
                                      frmMenu.ACBrECF1.CortaPapel(TRUE) ;

                                  frmCaixa_Recebimento.Repaint;
                                  frmMenu.MensagemPadrao('Por favor, destaque a 1� via.',0,5000) ;

                                end;
                      end;
                      
                      CloseFile( cArquivo );
                      if iErro = 1 then
                         break;

                end;

         // Fun��o que realiza o desbloqueio do teclado e do mouse.
         frmMenu.ACBrECF1.BloqueiaMouseTeclado := FALSE ;

         // Fun��o que realiza o fechamento do comprovante n�o fiscal vinculado.
         {frmMenu.ACBrECF1.FechaNaoFiscal(pchar(''));}
         
      end;
end;

////////////////////////////////////////////////////////////////////////////////
//
// Fun��o:
//    ConfirmaTransacao
// Objetivo:
//    Confirmar a Transa��o TEF
// Par�metros:
//   Integer com o n�mero da transa��o
// Retorno:
//    True para OK
//    False para n�o OK
//
////////////////////////////////////////////////////////////////////////////////
function ConfirmaTransacao( iConta: integer ): boolean;
var
   cLinhaArquivo, cConteudo: string;
   cArquivo: TextFile;
   lFlag   : longbool;
   iVezes  : integer;
begin
   cLinhaArquivo := '';
   cConteudo     := '';

   frmCaixa_Recebimento.lblMensagem.Caption := 'Confirmando Transa��o TEF...' ;

   if FileExists( 'C:\TEF_DIAL\RESP\INTPOS' + inttostr( iConta ) + '.001' ) then
   begin

        AssignFile( cArquivo, 'C:\TEF_DIAL\RESP\INTPOS' + inttostr( iConta ) + '.001' );

        {if ( iConta <> 0 ) then
            AssignFile( cArquivo, 'C:\TEF_DIAL\RESP\INTPOS' + inttostr( iConta ) + '.001' )
         else
            AssignFile( cArquivo, 'C:\TEF_DIAL\RESP\INTPOS.001' );}

         Reset( cArquivo );
         while not EOF( cArquivo ) do
            begin
               ReadLn( cArquivo, cLinhaArquivo );
               if ( copy( cLinhaArquivo, 1, 3 ) = '001' ) or
                  ( copy( cLinhaArquivo, 1, 3 ) = '002' ) or
                  ( copy( cLinhaArquivo, 1, 3 ) = '010' ) or
                  ( copy( cLinhaArquivo, 1, 3 ) = '012' ) or
                  ( copy( cLinhaArquivo, 1, 3 ) = '027' ) then
                     cConteudo := cConteudo + cLinhaArquivo + #13 + #10;
               if ( copy( cLinhaArquivo, 1, 3 ) = '999' ) then
                  cConteudo := cConteudo + cLinhaArquivo;
            end;
         CloseFile( cArquivo );

         cConteudo := '000-000 = CNF' + #13 + #10 + cConteudo;

         AssignFile( cArquivo, 'INTPOS.001' );
         ReWrite( cArquivo );
         WriteLn( cArquivo, cConteudo );
         CloseFile( cArquivo );
         
         CopyFile( pchar( 'INTPOS.001' ), pchar( 'C:\TEF_DIAL\REQ\INTPOS.001' ), lFlag );

         DeleteFile( 'INTPOS.001' );

         Sleep(2000) ;

         while not FileExists( 'C:\TEF_DIAL\RESP\INTPOS.STS' ) do sleep( 1000 );

         DeleteFile( 'C:\TEF_DIAL\RESP\INTPOS.STS' );

      end;

   // Se o arquivo TEF.TXT, que identifica que houve uma transa��o impressa
   // existir, o mesmo ser� exlu�do.
   if ( FileExists( 'TEF.TXT' ) ) then
      DeleteFile( 'TEF.TXT' );
end;

////////////////////////////////////////////////////////////////////////////////
//
// Fun��o:
//    NaoConfirmaTransacao
// Objetivo:
//    N�o Confirmar a Transa��o TEF
// Par�metros:
//   Integer com o n�mero da transa��o
// Retorno:
//    True para OK
//    False para n�o OK
//
////////////////////////////////////////////////////////////////////////////////
function NaoConfirmaTransacao( iConta: integer ): boolean;
var cLinhaArquivo, cConteudo, cCampoArquivo: string;
    cArquivo: TextFile;
    lFlag   : longbool;
    cValor, cNomeRede, cNSU, cIdent, cData, cHora: string;
    iVezes: integer;
begin

   DeleteFile( 'IMPRIME' + IntToStr( iConta ) + '.TXT' );

   cLinhaArquivo := '';
   cConteudo     := '';

   if FileExists( 'C:\TEF_DIAL\RESP\INTPOS' + inttostr( iConta ) + '.001' ) then
      begin

         AssignFile( cArquivo, 'C:\TEF_DIAL\RESP\INTPOS' + inttostr( iConta ) + '.001' );
         Reset( cArquivo );

         while not EOF( cArquivo ) do
            begin
               ReadLn( cArquivo, cLinhaArquivo );
               cCampoArquivo := copy( cLinhaArquivo, 1, 3 );
               case StrToInt( cCampoArquivo ) of
                  1: cConteudo := cConteudo + cLinhaArquivo + #13 + #10;
                  3: cValor := copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 );
                  10:
                     begin
                        cConteudo := cConteudo + cLinhaArquivo + #13 + #10;
                        cNomeRede := copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 );
                     end;
                  12:
                     begin
                        cConteudo := cConteudo + cLinhaArquivo + #13 + #10;
                        cNSU := copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 );
                     end;
                  27:  cConteudo := cConteudo + cLinhaArquivo + #13 + #10;
                  999: cConteudo := cConteudo + cLinhaArquivo;
               end;
            end;

         CloseFile( cArquivo );

         cConteudo := '000-000 = NCN' + #13 + #10 + cConteudo;

         AssignFile( cArquivo, 'INTPOS.001' );
         ReWrite( cArquivo );
         WriteLn( cArquivo, cConteudo );
         CloseFile( cArquivo );
         CopyFile( pchar( 'INTPOS.001' ), pchar( 'C:\TEF_DIAL\REQ\INTPOS.001' ), lFlag );

         DeleteFile( 'INTPOS.001' );

         while not FileExists( 'C:\TEF_DIAL\RESP\INTPOS.STS' ) do sleep( 1000 );

         DeleteFile( 'C:\TEF_DIAL\RESP\INTPOS.STS' );

         // Se o arquivo TEF.TXT, que identifica que houve uma transa��o impressa
         // existir, o mesmo ser� exlu�do.

         if ( FileExists( 'TEF.TXT' ) ) then
            DeleteFile( 'TEF.TXT' );

         TfrmTimedMessage.ShowMessage( pchar( 'Cancelada a Transa��o' + #13 + #13 + 'Rede: ' +
                                 cNomeRede + #13 + 'Doc N�: ' + cNSU + #13 + 'Valor: ' +
                                 FormatFloat( '#,##0.00', StrToFloat( cValor ) / 100 ) ),
                                 5,'Aten��o');

         DeleteFile( 'C:\TEF_DIAL\RESP\INTPOS' + inttostr( iConta ) + '.001' );
      end;

   iConta := iConta - 1;
   if iConta > 0 then
      begin
         for iVezes := 1 to iConta do
            begin
               if FileExists( 'C:\TEF_DIAL\RESP\INTPOS' + inttostr( iVezes ) + '.001' ) then
                  begin
                     cLinhaArquivo := '';
                     cConteudo     := '';

                     AssignFile( cArquivo, 'C:\TEF_DIAL\RESP\INTPOS' + inttostr( iVezes ) + '.001' );

                     Reset( cArquivo );
                     while not EOF( cArquivo ) do
                        begin
                           ReadLn( cArquivo, cLinhaArquivo );
                           cCampoArquivo := copy( cLinhaArquivo, 1, 3 );
                           case StrToInt( cCampoArquivo ) of
                              1:  cIdent    := copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 );
                              3:  cValor    := copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 );
                              10: cNomeRede := copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 );
                              12: cNSU      := copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 );
                              22: cData     := copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 );
                              23: cHora     := copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 );
                           end;
                        end;

                     CloseFile( cArquivo );

                     DeleteFile( 'C:\TEF_DIAL\RESP\INTPOS.STS' );

                     CancelaTransacaoTEF( cNSU, cValor, cNomeRede, cNSU, cData, cHora, iVezes );

                     ConfirmaTransacao( iVezes );

                     ImprimeGerencial( iVezes );

                     DeleteFile( 'C:\TEF_DIAL\RESP\INTPOS.STS' );

                     // Se o arquivo TEF.TXT, que identifica que houve uma transa��o impressa
                     // existir, o mesmo ser� exclu�do.
                     if ( FileExists( 'TEF.TXT' ) ) then
                        DeleteFile( 'TEF.TXT' );

                     Application.MessageBox( pchar( 'Cancelada a Transa��o' + #13 + #13 + 'Rede: ' +
                                             cNomeRede + #13 + 'Doc N�: ' + cNSU + #13 + 'Valor: ' +
                                             FormatFloat( '#,##0.00', StrToFloat( cValor ) / 100 ) ),
                                             'Aten��o', MB_IconInformation + MB_OK );
                  end;
            end;
      end;

   if iConta > 0 then
      begin
         for iVezes := 1 to iConta do
            begin
               if ( FileExists( 'C:\TEF_DIAL\RESP\INTPOS' + inttostr( iVezes ) + '.001' ) ) then
               DeleteFile( 'C:\TEF_DIAL\RESP\INTPOS' + inttostr( iVezes ) + '.001' );
               DeleteFile( 'C:\TEF_DIAL\RESP\CANCEL' + inttostr( iVezes ) + '.001' );
               DeleteFile( 'IMPRIME' + inttostr( iConta ) + '.TXT' );
               DeleteFile( 'PENDENTE.TXT' );
            end;
       end;
end;

////////////////////////////////////////////////////////////////////////////////
//
// Fun��o:
//    CancelaTransacaoTEF
// Objetivo:
//    Cancelar uma transa��o j� confirmada
// Par�metros:
//    String com o n�mero de identifica��o (NSU)
//    String com o valor da transa��o
//    String com o valor da transa��o
//    String com o nome e bandeira (REDE)
//    String com o n�mero do documento
//    String com a data da transa��o no formato DDMMAAAA
//    String com a hora da transa��o no formato HHSMMSS
//    Integer com o n�mero da transa��o
// Retorno:
//    True para OK
//    False para n�o OK
//
////////////////////////////////////////////////////////////////////////////////
function CancelaTransacaoTEF( cNSU: string; cValor: string; cNomeRede: string;
         cNumeroDOC: string; cData: string; cHora: string; iVezes: integer ): boolean;
var cConteudo: string;
    cArquivo: TextFile;
    lFlag   : longbool;
begin

   frmCaixa_Recebimento.lblMensagem.Caption := 'Cancelando Transa��o TEF...' ;

   cConteudo := '';
   cConteudo := '000-000 = CNC'           + #13 + #10 +
                '001-000 = ' + cNSU       + #13 + #10 +
                '003-000 = ' + cValor     + #13 + #10 +
                '010-000 = ' + cNomeRede  + #13 + #10 +
                '012-000 = ' + cNumeroDOC + #13 + #10 +
                '022-000 = ' + cData      + #13 + #10 +
                '023-000 = ' + cHora      + #13 + #10 +
                '999-999 = 0';
   AssignFile( cArquivo, 'INTPOS.001' );
   ReWrite( cArquivo );
   WriteLn( cArquivo, cConteudo );
   CloseFile( cArquivo );
   CopyFile( pchar( 'INTPOS.001' ), pchar( 'C:\TEF_DIAL\REQ\INTPOS.001' ), lFlag );
   DeleteFile( 'INTPOS.001' );

   while not FileExists( 'C:\TEF_DIAL\RESP\INTPOS.001' ) do sleep( 1000 );

   DeleteFile( 'C:\TEF_DIAL\RESP\INTPOS.STS' );
   CopyFile( pchar( 'C:\TEF_DIAL\RESP\INTPOS.001' ), pchar( 'C:\TEF_DIAL\RESP\CANCEL' + inttostr( iVezes ) +'.001' ), lFlag );

   DeleteFile( 'C:\TEF_DIAL\RESP\INTPOS.001' );

end;

////////////////////////////////////////////////////////////////////////////////
// Fun��o:
//    FuncaoAdministrativaTEF
// Objetivo:
//    Chamar o m�dulo administrativo da bandeira
// Par�metro:
//    String com o identificador
// Retorno:
//    1 para OK
//    diferente de 1 para n�o OK
////////////////////////////////////////////////////////////////////////////////
function FuncaoAdministrativaTEF( cIdentificacao: TDateTime ): integer;
var cArquivo        : TextFile;
    lFlag           : longbool;
    cConteudoArquivo: string;
begin

   AssignFile( cArquivo, 'INTPOS.001');

   // Conte�do do arquivo INTPOS.001 para solicitar a transa��o TEF

   cConteudoArquivo := '';
   cConteudoArquivo := '000-000 = ADM' + #13 + #10 +
                       '001-000 = ' + FormatDateTime( 'hhmmss', cIdentificacao ) + #13 + #10 +
                       '999-999 = 0';
   ReWrite( cArquivo );
   WriteLn( cArquivo, cConteudoArquivo );
   CloseFile( cArquivo );

   CopyFile( pchar( 'INTPOS.001' ), pchar( 'C:\TEF_DIAL\REQ\INTPOS.001' ), lFlag );
   DeleteFile( 'INTPOS.001' );

end;




////////////////////////////////////////////////////////////////////////////////
//
// Fun��o:
//    ImprimeGerencial
// Objetivo:
//    Imprimir atrav�s do Relat�rio Gerencial a transa��o efetuada.
// Par�metro:
//    Integer com o n�mero da transa��o
// Retorno:
//    1 para OK
//    diferente de 1 para n�o OK
//
////////////////////////////////////////////////////////////////////////////////
function ImprimeGerencial( iConta: integer ): integer;
var
   cConteudo, cConteudoAux, cLinha, cSaltaLinha, cLinhaArquivo: string;
   cArquivo: TextFile;
   iTentativas, iVezes, iRetorno, iVias, iTipoImpressora: integer;
   cMensagem: TForm;
   bTransacao: boolean;
   cArquivoTexto, cArquivoIntPos, cArquivoCancel, cCampoArquivo: string;
begin

   frmCaixa_Recebimento.Refresh;

   if ( iConta = 0 ) then
      begin
         cArquivoTexto  := 'IMPRIME.TXT';
         cArquivoIntPos := 'INTPOS.001';
      end
   else
      begin
         cArquivoTexto  := 'IMPRIME' + IntToStr( iConta ) + '.TXT';
         cArquivoIntPos := 'INTPOS'  + IntToStr( iConta ) + '.001';
         cArquivoCancel := 'CANCEL'  + IntToStr( iConta ) + '.001';
      end;

   if FileExists( cArquivoTexto ) then DeleteFile( cArquivoTexto );

   if ( FileExists( 'C:\TEF_DIAL\RESP\' + cArquivoCancel ) ) then
      cArquivoIntPos := 'CANCEL'  + IntToStr( iConta ) + '.001';


   result := -2;

   for iTentativas := 1 to 7 do
      begin

         cLinhaArquivo := '';
         cLinha := '';
         while ( result = -2 ) do
            begin
               if FileExists( 'C:\TEF_DIAL\RESP\' + cArquivoIntPos ) then
                  begin

                     Sleep(5000) ;
                     
                     AssignFile( cArquivo, 'C:\TEF_DIAL\RESP\' + cArquivoIntPos );
                     Reset( cArquivo );

                     while not EOF( cArquivo ) do
                        begin

                           ReadLn( cArquivo, cLinhaArquivo );
                           cCampoArquivo := copy( cLinhaArquivo, 1, 3 );

                           case StrToInt( cCampoArquivo ) of

                              9: // Verifica se a Transa��o foi Aprovada
                                 begin
                                    if ( copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ) ) = '0' then
                                       begin
                                          bTransacao := True;
                                          result := 1;
                                       end;
                                    if ( copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ) ) <> '0' then
                                       begin
                                          bTransacao := False;
                                          result := -1;
                                       end;
                                    end;

                              28: // Verifica se existem linhas para serem impressas
                                 if ( StrToInt( copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ) ) <> 0 ) and ( bTransacao = True ) then
                                    begin
                                       result := 1;
                                       for iVezes := 1 to StrToInt( copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ) ) do
                                          begin
                                             ReadLn( cArquivo, cLinhaArquivo );
                                             if copy( cLinhaArquivo, 1, 3 ) = '029' then
                                                cLinha := cLinha + copy( cLinhaArquivo, 12, Length( cLinhaArquivo ) - 12 ) + #13 + #10;
                                          end;
                                    end;

                              30: // Verifica se o campo � o 030 para mostrar a mensagem para o operador
                                 if cLinha <> '' then
                                    begin
                                        frmCaixa_Recebimento.Repaint;
                                        frmMenu.MensagemPadrao(copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ),0,5000) ;

                                    end
                                 else
                                    begin
                                       if ( FileExists( 'C:\TEF_DIAL\REQ\INTPOS.001' ) ) then
                                          DeleteFile( 'C:\TEF_DIAL\REQ\INTPOS.001' );

                                       frmCaixa_Recebimento.Repaint;
                                       frmMenu.MensagemPadrao(copy( cLinhaArquivo, 11, Length( cLinhaArquivo ) - 10 ),0,0) ;
                                       result := -1;
                                    end;

                           end;
                        end;
                  end;
            end;

            // Cria o arquivo tempor�rio IMPRIME.TXT com a imagem do comprovante
            if ( cLinha <> '' ) then
               begin
                  CloseFile( cArquivo );
                  AssignFile( cArquivo, 'IMPRIME' + inttostr( iConta ) + '.TXT' );
                  ReWrite( cArquivo );
                  WriteLn( cArquivo, cLinha );
                  CloseFile( cArquivo );
                  Break;
               end;

         Sleep( 1000 );

         // O arquivo INTPOS.STS n�o retornou em 7 segundos, ent�o o operador � informado.
         if ( iTentativas = 7 ) then
            begin
               if ( FileExists( 'C:\TEF_DIAL\REQ\INTPOS.001' ) ) then
                  DeleteFile( 'C:\TEF_DIAL\REQ\INTPOS.001' );

               frmCaixa_Recebimento.Repaint;
               frmMenu.MensagemPadrao ('Gerenciador Padr�o n�o est� ativo!',0,5000) ;
               result := 0;
               break;
            end;
         if ( result = 0 ) or ( result = -1 )then
            begin
               CloseFile( cArquivo );
               break;
            end;
      end;

   if ( FileExists( 'C:\TEF_DIAL\RESP\INTPOS.STS' ) ) then
      DeleteFile( 'C:\TEF_DIAL\RESP\INTPOS.STS' );

   if ( FileExists( 'C:\TEF_DIAL\RESP\INTPOS.001' ) ) then
      DeleteFile( 'C:\TEF_DIAL\RESP\INTPOS.001' );

   if FileExists( 'IMPRIME' + inttostr( iConta ) + '.TXT' ) then
      begin

         // Bloqueia o teclado e o mouse para a impress�o do TEF
         frmMenu.ACBrECF1.BloqueiaMouseTeclado := True ;

{         TfrmTimedMessage.ShowMessage('Aguarde a impress�o...',5,'Aten��o') ;}

         for iVias := 1 to iQtdeVias do
            begin
               AssignFile( cArquivo, 'IMPRIME' + inttostr( iConta ) + '.TXT' );
               Reset( cArquivo );
               while not EOF( cArquivo ) do
                  begin
                     cConteudo := ''; cLinha := '';
                     ReadLn( cArquivo, cLinha );
                     //cConteudo := cConteudo + cLinha + #13 + #10;
                     frmMenu.ACBrECF1.LinhaRelatorioGerencial ( pchar( cLinha ) + #13 + #10 );

                     // Aqui � feito o tratamento de erro de comunica��o com a impressora
                     // (desligamento da impressora durante a impress�o do comprovante).
                     if not (frmMenu.ACBrECF1.Ativo) then
                        begin
                           frmMenu.ACBrECF1.BloqueiaMouseTeclado := False ;
                           if ( Application.MessageBox( 'A impressora n�o responde!' + #13 +
                                                        'Deseja imprimir novamente?', 'Aten��o',
                                                        MB_IconInformation + MB_YESNO ) = IDYES ) then
                              begin
                                 CloseFile( cArquivo );
                                 frmMenu.ACBrECF1.FechaRelatorio;
                                 ImprimeGerencial( iConta );
                                 exit;
                              end
                           else
                              begin
                                 CloseFile( cArquivo );
                                 frmMenu.ACBrECF1.FechaRelatorio;
                                 result := 0;
                                 exit;
                              end;
                        end;

                     if ( EOF( cArquivo ) ) and ( iVias <> 2 ) and ( iQtdeVias > 1 ) then
                        begin

                           cSaltaLinha := #13 + #10 + #13 + #10 + #13 + #10 + #13 + #10 + #13 + #10;
                           frmMenu.ACBrECF1.LinhaRelatorioGerencial ( pchar( cSaltaLinha) );
                           //VerificaRetornoFuncaoImpressora( iRetorno );

                           // Fun��o que verifica o tipo da impressora fiscal.
                           // Se for a impressora com cutter, � acionado o corte do papel (guilhotina).
                           if (frmMenu.ACBrECF1.MFD) then
                               frmMenu.ACBrECF1.CortaPapel(TRUE);

                           frmCaixa_Recebimento.Repaint;
                           frmMenu.MensagemPadrao('Por favor, destaque a 1� via.',0,5000) ;
                           Sleep( 5000 );

                        end;
                  end;
            CloseFile( cArquivo );
            end;
         frmMenu.ACBrECF1.FechaRelatorio;

         //VerificaRetornoFuncaoImpressora( iRetorno );
      end;

   // Desbloqeia o teclado e o mouse
   frmMenu.ACBrECF1.BloqueiaMouseTeclado := True ;

   DeleteFile( 'IMPRIME' + inttostr( iConta ) + '.TXT' );

end;




////////////////////////////////////////////////////////////////////////////////
//
// Fun��o:
//    VerificaRetornoFuncaoImpressora
// Objetivo:
//    Verificar o retorno da impressora e da fun��o utilizada
// Retorno:
//    True para OK
//    False para n�o OK
//
////////////////////////////////////////////////////////////////////////////////
{function VerificaRetornoFuncaoImpressora( iRetorno: integer ): boolean;
var
   cMSGErro: string;
   iACK, iST1, iST2: integer;

begin
   iACK := 0; iST1 := 0; iST2 := 0;
   cMSGErro := '';
   result   := False;
   case iRetorno of
        0: cMSGErro := 'Erro de Comunica��o !';
       -1: cMSGErro := 'Erro de execu��o na Fun��o !';
       -2: cMSGErro := 'Par�metro inv�lido na Fun��o !';
       -3: cMSGErro := 'Al�quota n�o Programada !';
       -4: cMSGErro := 'Arquivo BEMAFI32.INI n�o Encontrado !';
       -5: cMSGErro := 'Erro ao abrir a Porta de Comunica��o !';
       -6: cMSGErro := 'Impressora Desligada ou Cabo de Comunica��o Desconectado !';
       -7: cMSGErro := 'C�digo do Banco n�o encontrado no arquivo BEMAFI32.INI !';
       -8: cMSGErro := 'Erro ao criar ou gravar arquivo STATUS.TXT ou RETORNO.TXT !';
      -27: cMSGErro := 'Status diferente de 6, 0, 0 !';
      -30: cMSGErro := 'Fun��o incompat�vel com a impressora fiscal YANCO !';
   end;

   cMSGErro := '';
   if ( iRetorno = 1 ) then
      begin
         Bematech_FI_RetornoImpressora( iACK, iST1, iST2 );
         if ( iACK = 21 ) then
            begin
               Bematech_FI_FinalizaModoTEF();
               Application.MessageBox( 'A Impressora retornou NAK !' + #13 +
                                       'Erro de Protocolo de Comunica��o !',
                                       'Aten��o', MB_IconError + MB_OK );
               result := False;
            end
         else
            if ( iST1 <> 0 ) or ( iST2 <> 0 ) then
               begin

                  // Analisa ST1
                  if ( iST1 >= 128 ) then
                     begin
                        iST1 := iST1 - 128;
                        cMSGErro := cMSGErro + 'Fim de Papel' + #13;
                     end;
                  if ( iST1 >= 64 ) then
                     begin
                        iST1 := iST1 - 64;
                        cMSGErro := cMSGErro + 'Pouco Papel' + #13;
                     end;
                  if ( iST1 >= 32 ) then
                     begin
                        iST1 := iST1 - 32;
                        cMSGErro := cMSGErro + 'Erro no Rel�gio' + #13;
                     end;
                  if ( iST1 >= 16 ) then
                     begin
                        iST1 := iST1 - 16;
                        cMSGErro := cMSGErro + 'Impressora em Erro' + #13;
                     end;
                  if ( iST1 >= 8 ) then
                     begin
                        iST1 := iST1 - 8;
                        cMSGErro := cMSGErro + 'Primeiro Dado do Comando n�o foi ESC' + #13;
                     end;
                  if iST1 >= 4 then
                     begin
                        iST1 := iST1 - 4;
                        cMSGErro := cMSGErro + 'Comando Inexistente' + #13;
                     end;
                  if iST1 >= 2 then
                     begin
                        iST1 := iST1 - 2;
                        cMSGErro := cMSGErro + 'Cupom Fiscal Aberto' + #13;
                     end;
                  if iST1 >= 1 then
                     begin
                        iST1 := iST1 - 1;
                        cMSGErro := cMSGErro + 'N�mero de Par�metros Inv�lidos' + #13;
                     end;

                  // Analisa ST2
                  if iST2 >= 128 then
                     begin
                        iST2 := iST2 - 128;
                        cMSGErro := cMSGErro + 'Tipo de Par�metro de Comando Inv�lido' + #13;
                     end;
                  if iST2 >= 64 then
                     begin
                        iST2 := iST2 - 64;
                        cMSGErro := cMSGErro + 'Mem�ria Fiscal Lotada' + #13;
                     end;
                  if iST2 >= 32 then
                     begin
                        iST2 := iST2 - 32;
                        cMSGErro := cMSGErro + 'Erro na CMOS' + #13;
                     end;
                  if iST2 >= 16 then
                     begin
                        iST2 := iST2 - 16;
                        cMSGErro := cMSGErro + 'Al�quota n�o Programada' + #13;
                     end;
                  if iST2 >= 8 then
                     begin
                        iST2 := iST2 - 8;
                        cMSGErro := cMSGErro + 'Capacidade de Al�quota Program�veis Lotada' + #13;
                     end;
                  if iST2 >= 4 then
                     begin
                        iST2 := iST2 - 4;
                        cMSGErro := cMSGErro + 'Cancelamento n�o permitido' + #13;
                     end;
                  if iST2 >= 2 then
                     begin
                        iST2 := iST2 - 2;
                        cMSGErro := cMSGErro + 'CGC/IE do Propriet�rio n�o Programados' + #13;
                     end;
                  if iST2 >= 1 then
                     begin
                        iST2 := iST2 - 1;
                        cMSGErro := cMSGErro + 'Comando n�o executado' + #13;
                     end;
                  if ( cMSGErro <> '' ) then
                     begin
                        Bematech_FI_FinalizaModoTEF();
                        Application.MessageBox( pchar( cMSGErro ), 'Aten��o', MB_IconError + MB_OK );
                        result := False;
                     end;
               end
            else
               result := True;
      end;
end;}

end.
