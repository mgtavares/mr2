unit fEmiteNFE2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ACBrNFeDANFEClass, ACBrNFe, pcnConversao, ACBrUtil,DB, ADODB, StrUtils,
  ACBrNFeDANFERave, IdMessage, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdMessageClient, IdSMTP,
  ACBrNFeDANFERaveCB, pcnAuxiliar, xmldom, XMLIntf, msxmldom, XMLDoc;
  //ACBrNFeDANFeQRClass : uses removida devido erro de compila��o ACBr (Erro QRNFe.Page.Continuous not found)

type
    TStatusWebService = record
        nCdStatusSCAN  : integer;
        nCdStatusSEFAZ : integer;
        cNmStatusSCAN  : String;
        cNmStatusSEFAZ : String;
        cObsSCAN       : String;
        cObsSEFAZ      : String;
    end;

 type
  TfrmEmiteNFe2 = class(TForm)
    qryConfigAmbiente: TADOQuery;
    qryConfigAmbientecUFEmissaoNfe: TStringField;
    qryConfigAmbientecPathLogoNFe: TStringField;
    qryConfigAmbientenCdTipoAmbienteNFe: TIntegerField;
    qryConfigAmbientecFlgEmiteNFe: TIntegerField;
    qryConfigAmbientecNrSerieCertificadoNFe: TStringField;
    qryDoctoFiscal: TADOQuery;
    qryDoctoFiscalnCdDoctoFiscal: TAutoIncField;
    qryDoctoFiscalnCdEmpresa: TIntegerField;
    qryDoctoFiscalnCdLoja: TIntegerField;
    qryDoctoFiscalnCdSerieFiscal: TIntegerField;
    qryDoctoFiscalcFlgEntSai: TStringField;
    qryDoctoFiscalcModelo: TStringField;
    qryDoctoFiscalcCFOP: TStringField;
    qryDoctoFiscalcTextoCFOP: TStringField;
    qryDoctoFiscaliNrDocto: TIntegerField;
    qryDoctoFiscalnCdTipoDoctoFiscal: TIntegerField;
    qryDoctoFiscaldDtEmissao: TDateTimeField;
    qryDoctoFiscaldDtSaida: TDateTimeField;
    qryDoctoFiscaldDtImpressao: TDateTimeField;
    qryDoctoFiscalnCdTerceiro: TIntegerField;
    qryDoctoFiscalcNmTerceiro: TStringField;
    qryDoctoFiscalcCNPJCPF: TStringField;
    qryDoctoFiscalcIE: TStringField;
    qryDoctoFiscalcTelefone: TStringField;
    qryDoctoFiscalnCdEndereco: TIntegerField;
    qryDoctoFiscalcEndereco: TStringField;
    qryDoctoFiscaliNumero: TIntegerField;
    qryDoctoFiscalcBairro: TStringField;
    qryDoctoFiscalcCidade: TStringField;
    qryDoctoFiscalcCep: TStringField;
    qryDoctoFiscalcUF: TStringField;
    qryDoctoFiscalnValTotal: TBCDField;
    qryDoctoFiscalnValProduto: TBCDField;
    qryDoctoFiscalnValBaseICMS: TBCDField;
    qryDoctoFiscalnValICMS: TBCDField;
    qryDoctoFiscalnValBaseICMSSub: TBCDField;
    qryDoctoFiscalnValICMSSub: TBCDField;
    qryDoctoFiscalnValIsenta: TBCDField;
    qryDoctoFiscalnValOutras: TBCDField;
    qryDoctoFiscalnValDespesa: TBCDField;
    qryDoctoFiscalnValFrete: TBCDField;
    qryDoctoFiscaldDtCad: TDateTimeField;
    qryDoctoFiscalnCdUsuarioCad: TIntegerField;
    qryDoctoFiscalnCdStatus: TIntegerField;
    qryDoctoFiscalcFlgFaturado: TIntegerField;
    qryDoctoFiscalnCdTerceiroTransp: TIntegerField;
    qryDoctoFiscalcNmTransp: TStringField;
    qryDoctoFiscalcCNPJTransp: TStringField;
    qryDoctoFiscalcIETransp: TStringField;
    qryDoctoFiscalcEnderecoTransp: TStringField;
    qryDoctoFiscalcBairroTransp: TStringField;
    qryDoctoFiscalcCidadeTransp: TStringField;
    qryDoctoFiscalcUFTransp: TStringField;
    qryDoctoFiscaliQtdeVolume: TIntegerField;
    qryDoctoFiscalnPesoBruto: TBCDField;
    qryDoctoFiscalnPesoLiq: TBCDField;
    qryDoctoFiscalcPlaca: TStringField;
    qryDoctoFiscalcUFPlaca: TStringField;
    qryDoctoFiscalnValIPI: TBCDField;
    qryDoctoFiscalnValFatura: TBCDField;
    qryDoctoFiscalnCdCondPagto: TIntegerField;
    qryDoctoFiscalnCdIncoterms: TIntegerField;
    qryDoctoFiscalnCdUsuarioImpr: TIntegerField;
    qryDoctoFiscalnCdTerceiroPagador: TIntegerField;
    qryDoctoFiscaldDtCancel: TDateTimeField;
    qryDoctoFiscalnCdUsuarioCancel: TIntegerField;
    qryDoctoFiscalnCdTipoPedido: TIntegerField;
    qryDoctoFiscalcFlgIntegrado: TIntegerField;
    qryDoctoFiscalnValCredAdiant: TBCDField;
    qryDoctoFiscalnValIcentivoFiscal: TBCDField;
    qryDoctoFiscalnCdLanctoFinDoctoFiscal: TIntegerField;
    qryDoctoFiscaldDtContab: TDateTimeField;
    qryDoctoFiscaldDtIntegracao: TDateTimeField;
    qryDoctoFiscalnCdServidorOrigem: TIntegerField;
    qryDoctoFiscaldDtReplicacao: TDateTimeField;
    qryDoctoFiscalcFlgBoletoImpresso: TIntegerField;
    qryDoctoFiscalcNrProtocoloNFe: TStringField;
    qryDoctoFiscalcNrReciboNFe: TStringField;
    qryDoctoFiscalcFlgAPrazo: TIntegerField;
    qryConfigAmbientenCdUFEmissaoNFe: TIntegerField;
    qryConfigAmbientenCdMunicipioEmissaoNFe: TIntegerField;
    qryItemDoctoFiscal: TADOQuery;
    qryDoctoFiscalcComplEnderDestino: TStringField;
    qryDoctoFiscalnCdMunicipioDestinoIBGE: TIntegerField;
    qryDoctoFiscalnCdPaisDestino: TIntegerField;
    qryDoctoFiscalcNmPaisDestino: TStringField;
    qryDoctoFiscalcCNPJCPFEmitente: TStringField;
    qryDoctoFiscalcIEEmitente: TStringField;
    qryDoctoFiscalcNmRazaoSocialEmitente: TStringField;
    qryDoctoFiscalcNmFantasiaEmitente: TStringField;
    qryDoctoFiscalcTelefoneEmitente: TStringField;
    qryDoctoFiscalnCdEnderecoEmitente: TIntegerField;
    qryDoctoFiscalcEnderecoEmitente: TStringField;
    qryDoctoFiscaliNumeroEmitente: TIntegerField;
    qryDoctoFiscalcComplEnderEmitente: TStringField;
    qryDoctoFiscalcBairroEmitente: TStringField;
    qryDoctoFiscalnCdMunicipioEmitenteIBGE: TIntegerField;
    qryDoctoFiscalcCidadeEmitente: TStringField;
    qryDoctoFiscalcUFEmitente: TStringField;
    qryDoctoFiscalcCepEmitente: TStringField;
    qryDoctoFiscalnCdPaisEmitente: TIntegerField;
    qryDoctoFiscalcNmPaisEmitente: TStringField;
    qryDoctoFiscalcSuframa: TStringField;
    qryItemDoctoFiscalnCdProduto: TStringField;
    qryItemDoctoFiscalcNmItem: TStringField;
    qryItemDoctoFiscalcDescricaoAdicional: TStringField;
    qryItemDoctoFiscalcCFOP: TStringField;
    qryItemDoctoFiscalcNCM: TStringField;
    qryItemDoctoFiscalcCdSTIPI: TStringField;
    qryItemDoctoFiscalcUnidadeMedida: TStringField;
    qryItemDoctoFiscalnCdTabTipoOrigemMercadoria: TIntegerField;
    qryItemDoctoFiscalnValUnitario: TBCDField;
    qryItemDoctoFiscalnAliqICMS: TBCDField;
    qryItemDoctoFiscalnAliqIPI: TBCDField;
    qryItemDoctoFiscalnPercIVA: TBCDField;
    qryItemDoctoFiscalnPercBCICMS: TBCDField;
    qryItemDoctoFiscalnPercBCIVA: TBCDField;
    qryItemDoctoFiscalnPercBCICMSST: TBCDField;
    qryItemDoctoFiscalnQtde: TBCDField;
    qryItemDoctoFiscalnValTotal: TBCDField;
    qryItemDoctoFiscalnValBaseICMS: TBCDField;
    qryItemDoctoFiscalnValICMS: TBCDField;
    qryItemDoctoFiscalnValIPI: TBCDField;
    qryItemDoctoFiscalnValBaseIPI: TBCDField;
    qryItemDoctoFiscalnValBASEICMSSub: TBCDField;
    qryItemDoctoFiscalnValICMSSub: TBCDField;
    qryItemDoctoFiscalnValIsenta: TBCDField;
    qryItemDoctoFiscalnValOutras: TBCDField;
    qryItemDoctoFiscalnValDespesa: TBCDField;
    qryItemDoctoFiscalnValFrete: TBCDField;
    qryPrazoDoctoFiscal: TADOQuery;
    qryPrazoDoctoFiscaliParcela: TIntegerField;
    qryPrazoDoctoFiscaldDtVenc: TDateTimeField;
    qryPrazoDoctoFiscalnValPagto: TBCDField;
    qryDoctoFiscalcXMLNFe: TMemoField;
    qryDoctoFiscalcChaveNFe: TStringField;
    qryMsgDoctoFiscal: TADOQuery;
    qryMsgDoctoFiscalcMsg: TStringField;
    qryDoctoFiscalcNrProtocoloCancNFe: TStringField;
    qryDoctoFiscalcCaminhoXML: TStringField;
    qryDoctoFiscalcArquivoXML: TStringField;
    qryConfigAmbientecPathArqNFe: TStringField;
    qryConfigAmbientecServidorSMTP: TStringField;
    qryConfigAmbientenPortaSMTP: TIntegerField;
    qryConfigAmbientecEmail: TStringField;
    qryConfigAmbientecSenhaEmail: TStringField;
    qryConfigAmbientecFlgUsaSSL: TIntegerField;
    qryDescontoTotal: TADOQuery;
    qryDescontoTotalnValDescontoItens: TBCDField;
    qryTerceiro: TADOQuery;
    qryTerceirocFlgTipoDescontoDanfe: TIntegerField;
    qryItemDoctoFiscalnValDesconto: TBCDField;
    qryDoctoFiscalnValSeguro: TBCDField;
    qryDoctoFiscalcFlgComplementar: TIntegerField;
    qryDoctoFiscalnCdDoctoFiscalOrigemCompl: TIntegerField;
    qryDoctoFiscalcNmMotivoComplemento: TStringField;
    qryDoctoFiscalcNFeRef: TStringField;
    ACBrNFe1: TACBrNFe;
    ACBrNFeDANFERave1: TACBrNFeDANFERave;
    qryDoctoFiscalcNrProtocoloDPEC: TStringField;
    qryDoctoFiscalnCdTabTipoEmissaoDoctoFiscal: TIntegerField;
    qryStatusDPEC: TADOQuery;
    qryStatusDPECnCdStatusDPEC: TIntegerField;
    qryStatusDPECcMotivoDPEC: TStringField;
    qryStatusDPECdDtInicioDPEC: TDateTimeField;
    qryStatusDPECcNmStatusDPEC: TStringField;
    qryDoctoFiscalcSerie: TStringField;
    qryItemDoctoFiscalnCdTabTipoCodProdFat: TIntegerField;
    qryItemDoctoFiscalcCdFabricante: TStringField;
    qryItemDoctoFiscalcEAN: TStringField;
    qryItemDoctoFiscalcUnidadeMedidaTribuvel: TStringField;
    qryDoctoFiscalcFlgFormularioProprio: TIntegerField;
    qryDoctoFiscalnCdPedidoGerador: TIntegerField;
    qryDoctoFiscalcNmMotivoComplemento2: TStringField;
    qryDoctoFiscalcFlgDPECTransmitida: TIntegerField;
    qryDoctoFiscalnValBaseIPI: TBCDField;
    qryDoctoFiscalnValPIS: TBCDField;
    qryDoctoFiscalnValCOFINS: TBCDField;
    qryItemDoctoFiscalcCSTPIS: TStringField;
    qryItemDoctoFiscalnAliqPIS: TBCDField;
    qryItemDoctoFiscalcCSTCOFINS: TStringField;
    qryItemDoctoFiscalnAliqCOFINS: TBCDField;
    qryItemDoctoFiscalnValBasePIS: TBCDField;
    qryItemDoctoFiscalnValPIS: TBCDField;
    qryItemDoctoFiscalnValBaseCOFINS: TBCDField;
    qryItemDoctoFiscalnValCOFINS: TBCDField;
    qryItemDoctoFiscalcEnqLegalIPI: TStringField;
    qryItemDoctoFiscalcEXTIPI: TStringField;
    qryItemDoctoFiscalnValIcentivoFiscal: TBCDField;
    qryItemDoctoFiscalnAliqICMSInternaOrigem: TBCDField;
    qryCartaCorrecaoNFe: TADOQuery;
    qryCartaCorrecaoNFenCdCartaCorrecaoNFe: TIntegerField;
    qryCartaCorrecaoNFenCdDoctoFiscal: TIntegerField;
    qryCartaCorrecaoNFedDtEmissao: TDateTimeField;
    qryCartaCorrecaoNFecCorrecao: TStringField;
    qryCartaCorrecaoNFeiSeq: TIntegerField;
    qryCartaCorrecaoNFeiLote: TIntegerField;
    qryCartaCorrecaoNFecCaminhoXML: TStringField;
    qryCartaCorrecaoNFecArquivoXML: TStringField;
    qryCartaCorrecaoNFecChaveNFe: TStringField;
    qryCartaCorrecaoNFenCdServidorOrigem: TIntegerField;
    qryCartaCorrecaoNFedDtReplicacao: TDateTimeField;
    qryCartaCorrecaoNFecNrProtocoloCCe: TStringField;
    qryCartaCorrecaoNFecXMLCCe: TMemoField;
    qryDoctoFiscalcCdNumSerieECF: TStringField;
    qryDoctoFiscalcJustCancelamento: TStringField;
    qryDoctoFiscalcFlgCancForaPrazo: TIntegerField;
    qryItemDoctoFiscalcCdST: TStringField;
    qryDoctoFiscaliStatusRetorno: TIntegerField;
    qryDoctoFiscalcNmStatusRetorno: TStringField;
    qryTerceironCdTabTipoEnquadTributario: TIntegerField;
    qryDoctoFiscalnCdTabTipoModFrete: TIntegerField;
    qryTerceiroEmitente: TADOQuery;
    qryTerceiroEmitentenCdTerceiro: TIntegerField;
    qryTerceiroEmitentenCdTabTipoEnquadTributario: TIntegerField;
    qryDoctoFiscalnValTotalImpostoAproxFed: TBCDField;
    qryDoctoFiscalnValTotalImpostoAproxEst: TBCDField;
    qryDoctoFiscalnValTotalImpostoAproxMun: TBCDField;
    qryTabIBPT: TADOQuery;
    qryTerceiroEmitentenCdEstado: TIntegerField;
    qryItemDoctoFiscalnValImpostoAproxFed: TBCDField;
    qryItemDoctoFiscalnValImpostoAproxEst: TBCDField;
    qryItemDoctoFiscalnValImpostoAproxMun: TBCDField;
    qryTabIBPTcChave: TStringField;
    qryTabIBPTiDiasValidade: TIntegerField;
    qryDoctoFiscalRef: TADOQuery;
    qryDoctoFiscalRefcChaveNFe: TStringField;
    qryTerceirocFlgIsentoCNPJCPF: TIntegerField;
    qryTerceiroiTipoPessoa: TIntegerField;
    qryDoctoFiscalcFlgDevolucao: TIntegerField;
    qryConsultaDadosCert: TADOQuery;
    qryConsultaDadosCertnCdMunicipio: TIntegerField;
    qryConsultaDadosCertnCdEstado: TIntegerField;
    qryConsultaDadosCertcUF: TStringField;
    qryDoctoFiscalcEspecieTransp: TStringField;
    qryItemDoctoFiscalnValBaseICMSSubRet: TBCDField;
    qryItemDoctoFiscalnValICMSSubRet: TBCDField;
    qryAux: TADOQuery;
    qryModeloDocumento: TADOQuery;
    qryModeloDocumentocDescricaoModeloDoc: TMemoField;
    qryModeloDocumentonCdTabTipoModeloDoc: TIntegerField;
    qryDoctoFiscalcFlgAmbienteHom: TIntegerField;
    qryCartaCorrecaoNFecFlgAmbienteHom: TIntegerField;
    qryDoctoFiscaliNrDoctoPend: TIntegerField;
    procedure preparaNFe();
    procedure preparaNFCe();
    procedure gerarNFe(nCdDoctoFiscal : Integer; bEspelho : Boolean = False);
    procedure gerarCCe(cChaveNFe, cCNPJCPFEmitente, xCorrecao : String; nSeqEvento, idLote, nCdCCe : Integer);
    procedure imprimirCCe(nCdCartaCorrecaoNFe : Integer);
    procedure imprimirDANFe(nCdDoctoFiscal:integer) ;
    procedure ReimprimirDANFe(nCdDoctoFiscal:integer) ;
    procedure cancelarNFe(nCdDoctoFiscal:integer);
    procedure enviaEmailNFe(nCdDoctoFiscal:integer; cPara, cCopia : String);
    procedure enviaEmailCancNFe(nCdDoctoFiscal:integer; cPara, cCopia : String);
    function StatusWebService(nCdWebService : integer) : TStatusWebService; // 0-Para consultar SEFAZ e SCAN
    function ConsultaNFe(iNrDocto : Integer) : String;
    procedure gerarNFCe(nCdDoctoFiscal : Integer);
    procedure cancelarNFCe(nCdDoctoFiscal : Integer);
    procedure prSelCertificadoDigital;
  private
    { Private declarations }
    cFlgAmbienteHom : Integer;
  public
    { Public declarations }
    Retorno       : String;
    bConsulta     : Boolean;
    cCertNumSerie : String;
    cCertCNPJ     : String;
    cCertMun      : String;
    cCertUF       : String;
    cCertPais     : String;
  end;

var
  frmEmiteNFe2: TfrmEmiteNFe2;

implementation

uses fMenu, fConfigAmbiente, ACBrNFeWebServices, dateUtils, pcnNFe,
  ACBrNFeConfiguracoes, pcnRetCCeNFe, pcnRetEnvEventoNFe, pcnEventoNFe,
  pcnEnvEventoNFe, rCartaCorrecaoNFe_View, pcnCCeNFe, QRCtrls, ComCtrls, fModeloDocumento;

{$R *.dfm}

{ TfrmEmiteNFe }

procedure TfrmEmiteNFe2.cancelarNFe(nCdDoctoFiscal: integer);
var
  cChaveNFe   : String;
  vAux        : String;
  iCodRetorno : Integer;
  //i               : Integer;
  //cFlgNFCancelada : Boolean;
label
   pedeJustificativa;
begin
  {-- prepara o ambiente para cancelamento da NFe --}
  preparaNFe() ;

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryDoctoFiscal.Open;

  if (qryDoctoFiscalcNrProtocoloCancNFe.Value <> '') then
      Exit;

  { -- se existe documento pendente, verifica se o mesmo esta ativo na sefaz    -- }
  { -- este processo se faz necess�rio devido problemas de duplicidade de notas -- }
  { -- que ocorrem no momento da atualiza��o do doc. ap�s transmiss�o           -- }
  if (qryDoctoFiscaliNrDoctoPend.Value > 0) then
  begin
      cChaveNFe := ConsultaNFe(qryDoctoFiscaliNrDoctoPend.Value);
      
      if (cChaveNFe <> '') then
      begin
          Raise Exception.Create('Este documento fiscal est� ativo na SEFAZ e ainda n�o foi atualizado no sistema.'
                                +#13
                                +'Consulte este documento no monitor de impress�es fiscais e transmista novamente para que seus dados sejam atualizados permitindo seu cancelamento posteriormente.'
                                +#13#13
                                +'Chave NFe: ' + cChaveNFe);
      end;

      Exit;
  end;

  { -- verifica se nf j� est� cancelada -- }
  try
      frmMenu.mensagemUsuario('Consultando NFe...');

      ACBrNFe1.NotasFiscais.Clear;
      ACBrNFe1.WebServices.Consulta.NFeChave := qryDoctoFiscalcChaveNFe.Value;
      ACBrNFe1.WebServices.Consulta.Executar;

      { -- se nota fiscal j� estiver cancelada, atualiza apenas informa��es de cancelamento -- }
      if (ACBrNFe1.WebServices.Consulta.cStat in [101,151]) then
      begin
          frmMenu.mensagemUsuario('Atualizando inf. NFe...');

          qryDoctoFiscal.Edit;
          qryDoctoFiscalcNrProtocoloCancNFe.Value := ACBrNFe1.WebServices.Consulta.procEventoNFe.Items[0].RetEventoNFe.retEvento.Items[0].RetInfEvento.nProt;
          qryDoctoFiscalcJustCancelamento.Value   := ACBrNFe1.WebServices.Consulta.procEventoNFe.Items[0].RetEventoNFe.InfEvento.detEvento.xJust;
          qryDoctoFiscaliStatusRetorno.Value      := ACBrNFe1.WebServices.Consulta.procEventoNFe.Items[0].RetEventoNFe.retEvento.Items[0].RetInfEvento.cStat;
          qryDoctoFiscalcNmStatusRetorno.Value    := ACBrNFe1.WebServices.Consulta.procEventoNFe.Items[0].RetEventoNFe.retEvento.Items[0].RetInfEvento.xMotivo;
          qryDoctoFiscalcFlgAmbienteHom.Value     := cFlgAmbienteHom;
          qryDoctoFiscal.Post;

          Exit;
      end;
  except
      frmMenu.mensagemUsuario('');
      Raise;
  end;

  try
      frmMenu.mensagemUsuario('Carregando XML NFe...');
      ACBrNFe1.NotasFiscais.Clear;
      ACBrNFe1.NotasFiscais.LoadFromFile(qryDoctoFiscalcCaminhoXML.Value + qryDoctoFiscalcArquivoXML.Value) ;

  except
      frmMenu.mensagemUsuario('');
      raise ;
  end ;

  { -- bloco removido devido processo acima j� verificar se nota fiscal j� foi cancelada --
  ACBrNFe1.Consultar;

  cFlgNFCancelada := False;

  for i:= 0 to ACBrNFe1.EventoNFe.Evento.Count - 1 do
  begin
      if (ACBrNFe1.EventoNFe.Evento.Items[1].RetInfEvento.tpEvento = teCancelamento) then
      begin
          cFlgNFCancelada := True;
          Break;
      end;
  end;}

  //if (not cFlgNFCancelada) then
  //begin
      frmMenu.mensagemUsuario('Cancelando NFe...');

      pedeJustificativa:

      if not(frmMenu.InputQuery('Cancelamento de NFe', 'Justificativa', vAux)) then
          exit;

      if (Length(vAux) < 15) then
      begin
          frmMenu.MensagemAlerta('A justificativa deve ter no m�nimo 15 letras.') ;
          goto pedeJustificativa;
      end ;

      // processa o cancelamento
      try
          frmMenu.mensagemUsuario('Carregando XML NFe...');

          { -- Desativado de acordo com Ajuste SINIEF 16/12. Usar o Evento de Cancelamento (NT 2011/006) -- }
          //ACBrNFe1.NotasFiscais.Clear;
          //ACBrNFe1.NotasFiscais.LoadFromFile(qryDoctoFiscalcCaminhoXML.Value + qryDoctoFiscalcArquivoXML.Value) ;
          //ACBrNFe1.Cancelamento(vAux);

          ACBrNFe1.EventoNFe.Evento.Clear;

          with (ACBrNFe1.EventoNFe.Evento.Add) do
          begin
              infEvento.chNFe               := qryDoctoFiscalcChaveNFe.Value;
              infEvento.CNPJ                := qryDoctoFiscalcCNPJCPFEmitente.Value;
              infEvento.dhEvento            := Now;
              infEvento.tpEvento            := teCancelamento;
              infEvento.detEvento.nProt     := qryDoctoFiscalcNrProtocoloNFe.Value;
              infEvento.detEvento.xJust     := vAux;
          end;

          { -- envia solicita��o de cancelamento -- }
          frmMenu.mensagemUsuario('Cancelando NFe...');

          ACBrNFe1.EnviarEventoNFe(1);

          {-- Se o c�digo de retorno da SEFAZ for diferente dos abaixo, cancela todo o processo. --}
          // 135       = Evento registrado e vinculado a NF-e
          // 151 e 155 = Cancelamento de NF-e fora de prazo
          iCodRetorno := ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat;

          if not (iCodRetorno in [135,151,155]) then
          begin
              Raise EDatabaseError.CreateFmt('Ocorreu o seguinte erro ao cancelar a nota fiscal eletr�nica:' + sLineBreak + 'C�digo:%d' + sLineBreak + 'Motivo: %s', [ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat, ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.xMotivo]);
              Exit;
          end
      except
          frmMenu.mensagemUsuario('');
          Raise ;
      end ;

      frmMenu.mensagemUsuario('');

      { -- Desativado de acordo com Ajuste SINIEF 16/12. Usar o Evento de Cancelamento (NT 2011/006) -- }
      //qryDoctoFiscal.Edit ;
      //qryDoctoFiscalcNrProtocoloCancNFe.Value := ACBrNFe1.WebServices.Cancelamento.Protocolo ;
      //qryDoctoFiscal.Post;

      { -- Insere o protocolo de cancelamento e a justificativa --}
      qryDoctoFiscal.Edit;
      qryDoctoFiscalcNrProtocoloCancNFe.Value := ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.nProt;
      qryDoctoFiscalcJustCancelamento.Value   := vAux;
      qryDoctoFiscaliStatusRetorno.Value      := ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat;
      qryDoctoFiscalcNmStatusRetorno.Value    := ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.xMotivo;
      qryDoctoFiscalcFlgAmbienteHom.Value     := cFlgAmbienteHom;

      {-- Se a NF foi cancelada fora do prazo atualiza a flag de cancel. fora de prazo --}
      if (iCodRetorno in [151,155]) then
      begin
          qryDoctoFiscalcFlgCancForaPrazo.Value := 1;

          frmMenu.ShowMessage('Cancelamento homologado fora de prazo.');
      end;

      qryDoctoFiscal.Post;
  //end
  //else
  //frmMenu.MensagemAlerta('Nota Fiscal j� cancelada no SEFAZ.') ;
end;

function TfrmEmiteNFe2.ConsultaNFe(iNrDocto : Integer) : String;
var
  cChaveNFe : String;
  nCdUF     : Integer;
begin
  try
      frmMenu.mensagemUsuario('Verificando exist�ncia NFe...');

      if (qryConfigAmbientenCdUFEmissaoNFe.Value <> 0) then
          nCdUF := qryConfigAmbientenCdUFEmissaoNFe.Value
      else
          nCdUF := qryConsultaDadosCertnCdEstado.Value;

      GerarChave(cChaveNFe, nCdUF, iNrDocto, 55, StrToInt(Trim(qryDoctoFiscalcSerie.Value)), iNrDocto, 1, qryDoctoFiscaldDtEmissao.Value, qryDoctoFiscalcCNPJCPFEmitente.Value);

      cChaveNFe := StringReplace(cChaveNFe,'NFe','',[rfIgnoreCase,rfReplaceAll]);

      ACBrNFe1.WebServices.Consulta.NFeChave := cChaveNFe;
      
      if (ACBrNFe1.WebServices.Consulta.Executar) then
          Result := cChaveNFe
      else
          Result := '';
  except
      raise;
  end;
end;

procedure TfrmEmiteNFe2.gerarNFe(nCdDoctoFiscal : Integer; bEspelho : Boolean = False);
var
  iItem           : integer;
  iAux            : integer;
  iDiasIBPT       : Integer;
  vAux            : String;
  cCdST           : String;
  ok              : boolean;
  cChaveNFe       : String;
  cNrReciboNFe    : String;
  cPatchReciboNFe : String;
  ArqReciboNFe    : TextFile;
label
   pedeJustificativa;
begin

  {-- prepara o ambiente para gera��o da NFe --}
  preparaNFe() ;

  {-- se o dpec foi ativado muda as configura��es para emissao de nf em contingengia
  -- em modo de Emissao DPEC, ignorando o status do webservice --}

  qryStatusDPEC.Close;
  qryStatusDPEC.Parameters.ParamByName('nPK').Value := frmMenu.nCdEmpresaAtiva;
  qryStatusDPEC.Open;

  if (qryStatusDPECnCdStatusDPEC.Value = 1) then
      ACBrNFe1.Configuracoes.Geral.FormaEmissao := teDPEC
  else begin

      if (StatusWebService(1).nCdStatusSEFAZ <> 107) then //Se SEFAZ Origem n�o estiver em opera��o
      begin
          if(frmMenu.LeParametro('PERMITEMODOSCAN') = 'S') then
          begin
              if(frmMenu.MessageDLG('O Webservice SEFAZ de Origem est� Inoperante no momento. Deseja emitir a nota fiscal em modo de Conting�ncia ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
                  abort;

              ACBrNFe1.Configuracoes.Geral.FormaEmissao := teSCAN ;
          end else
          begin
              frmMenu.MensagemAlerta('Webservice SEFAZ de Origem est� Inoperante no momento e o modo de Conting�ncia SCAN n�o est� habilitado.'
                                   + #13#13 + 'Se deseja prosseguir e emitir esta NFe, ative o Modo Conting�ncia em DPEC.');
              abort;
          end;
      end;
      {-- se SCAN n�o estiver habilitado avisa sobre o uso da DPEC --}
      if ((ACBrNFe1.Configuracoes.Geral.FormaEmissaoCodigo = 3) and (StatusWebService(3).nCdStatusSCAN <> 107)) then
      begin
          frmMenu.MensagemAlerta('Webservice de Conting�ncia em SCAN n�o responde.' + #13#13 + 'Se deseja prosseguir e emitir esta NFe, ative o Modo Contingencia em DPEC.');
          abort;
      end;
  end;

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryDoctoFiscal.Open;

  qryItemDoctoFiscal.Close;
  qryItemDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryItemDoctoFiscal.Open;

  // Limpas as Notas Fiscais que possam estar na mem�ria do componente
  frmMenu.mensagemUsuario('Gerando NFe...');

  ACBrNFe1.NotasFiscais.Clear;

  iItem := 1 ;

  with ACBrNFe1.NotasFiscais.Add.NFe do
  begin
      {-- trata a identifica��o da nota fiscal --}
      Ide.natOp   := qryDoctoFiscalcTextoCFOP.Value ;
      Ide.nNF     := qryDoctoFiscaliNrDocto.Value;
      Ide.cNF     := qryDoctoFiscaliNrDocto.Value;
      Ide.modelo  := 55;
      Ide.serie   := StrToInt(Trim(qryDoctoFiscalcSerie.Value));
      Ide.dEmi    := qryDoctoFiscaldDtEmissao.Value;
      Ide.verProc := frmMenu.cNmVersaoSistema; //vers�o sistema (max 20car.)

      if qryDoctoFiscaldDtSaida.Value <> NULL then ide.dSaiEnt := qryDoctoFiscaldDtSaida.Value
      else ide.dSaiEnt := Date;

      if (qryConfigAmbientenCdTipoAmbienteNFe.Value = 0) then
          Ide.tpAmb := taProducao
      else Ide.tpAmb := taHomologacao;

      { -- classifica tipo da nota fiscal (entrada/sa�da) -- }
      if (qryDoctoFiscalcFlgEntSai.Value = 'S') then
      begin
          Ide.tpNF   := tnSaida;
          Ide.finNFe := fnNormal;
      end
      else
      begin
          Ide.tpNF   := tnEntrada;
          Ide.finNFe := fnNormal;
      end;

      { -- verifica se � uma nota fiscal complementar -- }
      if (qryDoctoFiscalcFlgComplementar.Value = 1) then
      begin
          Ide.finNFe           := fnComplementar;
          Ide.NFref.Add.refNFe := qryDoctoFiscalcNFeRef.Value;
      end;

      { -- verifica se � uma nota fiscal de devolu��o -- }
      if (qryDoctoFiscalcFlgDevolucao.Value = 1) then
      begin
          Ide.finNFe := fnDevolucao;

          { -- adiciona nota(s) de refer�ncia -- }
          qryDoctoFiscalRef.Close;
          qryDoctoFiscalRef.Parameters.ParamByName('nCdDoctoFiscal').Value := qryDoctoFiscalnCdDoctoFiscal.Value;
          qryDoctoFiscalRef.Parameters.ParamByName('cFlgEntSai').Value     := qryDoctoFiscalcFlgEntSai.Value;
          qryDoctoFiscalRef.Open;

          qryDoctoFiscalRef.First;

          while (not qryDoctoFiscalRef.Eof) do
          begin
              Ide.NFref.Add.refNFe := qryDoctoFiscalRefcChaveNFe.Value;

              qryDoctoFiscalRef.Next;
          end;
      end;

      if (qryDoctoFiscalnValFatura.Value > 0) then
          if (qryDoctoFiscalcFlgAPrazo.Value = 1) then
              Ide.indPag  := ipPrazo
          else Ide.indPag := ipVista ;

      if (qryDoctoFiscalnValFatura.Value = 0) then
          Ide.indPag := ipOutras ;

      if (qryConfigAmbientenCdMunicipioEmissaoNFe.Value <> 0) then
          Ide.cMunFG := qryConfigAmbientenCdMunicipioEmissaoNFe.Value
      else
          Ide.cMunFG := qryConsultaDadosCertnCdMunicipio.Value;

      if (qryConfigAmbientenCdUFEmissaoNFe.Value <> 0) then
          Ide.cUF := qryConfigAmbientenCdUFEmissaoNFe.Value
      else
          Ide.cUF := qryConsultaDadosCertnCdEstado.Value;

      {-- Como padr�o mant�m o tipo de Emiss�o como normal --}
      Ide.tpEmis  := teNormal ;

      {-- verifica se foi setado pra SCAN --}
      if (ACBrNFe1.Configuracoes.Geral.FormaEmissaoCodigo = 3) then
      begin
          Ide.tpEmis := teSCAN;
          Ide.xJust  := 'WEBSERVICE SEFAZ DE ORIGEM INOPERANTE';
          ide.dhCont := Now();
      end;

      {-- verifica se o modo DPEC est� ativo --}
      if (qryStatusDPECnCdStatusDPEC.Value = 1) then
      begin
          Ide.tpEmis  := teDPEC ;
          Ide.xJust   := qryStatusDPECcMotivoDPEC.Value;
          Ide.dhCont  := qryStatusDPECdDtInicioDPEC.Value;
      end;

      Ide.procEmi := peAplicativoContribuinte ;
      ide.tpImp   := tiRetrato ;

      { -- consulta dados adicionais do emitente -- }
      qryTerceiroEmitente.Close;
      qryTerceiroEmitente.Parameters.ParamByName('nCdEmpresa').Value := qryDoctoFiscalnCdEmpresa.Value;
      qryTerceiroEmitente.Parameters.ParamByName('nCdLoja').Value    := qryDoctoFiscalnCdLoja.Value;
      qryTerceiroEmitente.Open;

      { -- trata a identifica��o do emitente da nota fiscal -- }
      Emit.CNPJCPF           := qryDoctoFiscalcCNPJCPFEmitente.Value ;
      Emit.IE                := qryDoctoFiscalcIEEmitente.Value ;
      Emit.xNome             := qryDoctoFiscalcNmRazaoSocialEmitente.Value ;
      Emit.xFant             := qryDoctoFiscalcNmFantasiaEmitente.Value ;
      Emit.EnderEmit.fone    := qryDoctoFiscalcTelefoneEmitente.Value ;
      Emit.EnderEmit.CEP     := qryDoctoFiscalcCepEmitente.asInteger ;
      Emit.EnderEmit.xLgr    := qryDoctoFiscalcEnderecoEmitente.Value ;
      Emit.EnderEmit.nro     := qryDoctoFiscaliNumeroEmitente.asString ;
      Emit.EnderEmit.xCpl    := qryDoctoFiscalcComplEnderEmitente.Value ;
      Emit.EnderEmit.xBairro := qryDoctoFiscalcBairroEmitente.Value ;
      Emit.EnderEmit.cMun    := qryDoctoFiscalnCdMunicipioEmitenteIBGE.Value ;
      Emit.EnderEmit.xMun    := qryDoctoFiscalcCidadeEmitente.Value ;
      Emit.EnderEmit.UF      := qryDoctoFiscalcUFEmitente.Value ;
      Emit.enderEmit.cPais   := qryDoctoFiscalnCdPaisEmitente.Value ;
      Emit.enderEmit.xPais   := qryDoctoFiscalcNmPaisEmitente.Value ;

      { -- seleciona regime tribut�rio do emitente -- }
      case (qryTerceiroEmitentenCdTabTipoEnquadTributario.Value) of
          1 : Emit.CRT := crtSimplesNacional;       //simples nacional
          2 : Emit.CRT := crtSimplesExcessoReceita; //simples nacional excesso de sublimite da receita bruta
          3 : Emit.CRT := crtRegimeNormal;          //regime normal
      end;

      { -- trata a identifica��o do destinat�rio da nota fiscal -- }
      qryTerceiro.Close;
      qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := qryDoctoFiscalnCdTerceiro.Value;
      qryTerceiro.Open;

      if (Ide.tpAmb = taProducao) then
      begin
          Dest.CNPJCPF   := qryDoctoFiscalcCNPJCPF.Value;
          Dest.xNome     := qryDoctoFiscalcNmTerceiro.Value;
          Dest.IE        := qryDoctoFiscalcIE.Value;
          Dest.indIEDest := inContribuinte;

          case (qryTerceiroiTipoPessoa.Value) of
              { -- se for pessoa f�sica (PF) -- }
              1 : begin
                  Dest.IE        := '';
                  Dest.indIEDest := inNaoContribuinte;
              end;
              { -- se for pessoa jur�dica (PJ) -- }
              2 : begin
                  { -- se terceiro n�o possuir IE categoriza como isento -- }
                  if (qryDoctoFiscalcIE.Value = '') or (qryDoctoFiscalcIE.Value = 'ISENTO') then
                  begin
                      Dest.IE        := 'ISENTO';
                      Dest.indIEDest := inIsento;
                  end;
              end;
          end;
      end
      else
      begin
          Dest.CNPJCPF   := '99999999000191';
          Dest.xNome     := 'NF-E EMITIDA EM AMBIENTE DE HOMOLOGACAO - SEM VALOR FISCAL';
          Dest.IE        := '';
          Dest.indIEDest := inIsento;
      end;

      Dest.EnderDest.CEP     := qryDoctoFiscalcCEP.asInteger ;
      Dest.EnderDest.xLgr    := qryDoctoFiscalcEndereco.Value ;
      Dest.EnderDest.nro     := qryDoctoFiscaliNumero.asString ;
      Dest.EnderDest.xCpl    := qryDoctoFiscalcComplEnderDestino.Value;
      Dest.EnderDest.xBairro := qryDoctoFiscalcBairro.Value ;
      Dest.EnderDest.cMun    := qryDoctoFiscalnCdMunicipioDestinoIBGE.Value;
      Dest.EnderDest.xMun    := qryDoctoFiscalcCidade.Value;
      Dest.EnderDest.UF      := qryDoctoFiscalcUF.Value ;
      Dest.EnderDest.Fone    := qryDoctoFiscalcTelefone.Value;
      Dest.EnderDest.cPais   := qryDoctoFiscalnCdPaisDestino.Value ;
      Dest.EnderDest.xPais   := qryDoctoFiscalcNmPaisDestino.Value ;
      Dest.ISUF              := qryDoctoFiscalcSuframa.Value ;

      { -- valida se � interestadual ou interna -- }
      if Dest.EnderDest.UF <> Emit.EnderEmit.UF then
          Ide.idDest := doInterestadual
      else
          Ide.idDest := doInterna;

      { -- trata dos itens da nota fiscal -- }
      qryItemDoctoFiscal.First;

      while not qryItemDoctoFiscal.Eof do
      begin

          with Det.Add do
          begin
              infAdProd     := qryItemDoctoFiscalcDescricaoAdicional.Value;
              Prod.nItem    := iItem;
              Prod.CFOP     := qryItemDoctoFiscalcCFOP.Value;

              if ((qryItemDoctoFiscalnCdTabTipoCodProdFat.Value <= 1) or (qryItemDoctoFiscalnCdTabTipoCodProdFat.Value = 4)) then
                  Prod.cProd := qryItemDoctoFiscalnCdProduto.Value;

              if (qryItemDoctoFiscalnCdTabTipoCodProdFat.Value = 2) then
              begin

                  if ( Length ( Trim ( qryItemDoctoFiscalcCdFabricante.Value ) ) <= 0 ) then
                  begin
                      frmMenu.MensagemErro('C�digo do fabricante n�o informado para o produto: ' + qryItemDoctoFiscalnCdProduto.asString + ' - ' + qryItemDoctoFiscalcNmItem.Value);
                      abort ;
                  end ;

                  Prod.cProd := qryItemDoctoFiscalcCdFabricante.Value;

              end ;

              if (qryItemDoctoFiscalnCdTabTipoCodProdFat.Value = 3) then
              begin

                  if ( Length ( Trim ( qryItemDoctoFiscalcEAN.Value ) ) <= 0 ) then
                  begin
                      frmMenu.MensagemErro('C�digo EAN/UPC n�o informado para o produto: ' + qryItemDoctoFiscalnCdProduto.asString + ' - ' + qryItemDoctoFiscalcNmItem.Value);
                      abort ;
                  end ;

                  Prod.cProd := qryItemDoctoFiscalcEAN.Value;

              end ;

              Prod.xProd    := qryItemDoctoFiscalcNmItem.Value;
              Prod.vProd    := qryItemDoctoFiscalnValTotal.Value;
              Prod.vUnCom   := qryItemDoctoFiscalnValUnitario.Value;
              Prod.vUnTrib  := qryItemDoctoFiscalnValUnitario.Value;
              Prod.uCom     := qryItemDoctoFiscalcUnidadeMedida.Value;
              Prod.uTrib    := qryItemDoctoFiscalcUnidadeMedidaTribuvel.Value;
              Prod.qCom     := qryItemDoctoFiscalnQtde.Value;
              Prod.qTrib    := qryItemDoctoFiscalnQtde.Value;
              Prod.NCM      := qryItemDoctoFiscalcNCM.Value;
              Prod.vFrete   := qryItemDoctoFiscalnValFrete.Value;

              if ((trim( qryItemDoctoFiscalcEAN.Value ) <> '') and (qryItemDoctoFiscalnCdTabTipoCodProdFat.Value <> 4)) then
                  Prod.cEAN := qryItemDoctoFiscalcEAN.Value;

              // desconto no total da nf, imprime o valor bruto unitario
              if qryTerceirocFlgTipoDescontoDanfe.Value = 1 then
              begin
                  Prod.vDesc    := (qryItemDoctoFiscalnQtde.Value * qryItemDoctoFiscalnValDesconto.Value);
                  Prod.vProd    := qryItemDoctoFiscalnValTotal.Value + (qryItemDoctoFiscalnQtde.Value * qryItemDoctoFiscalnValDesconto.Value);
                  Prod.vUnTrib  := (qryItemDoctoFiscalnValUnitario.Value + qryItemDoctoFiscalnValDesconto.Value);
                  Prod.vUnCom   := (qryItemDoctoFiscalnValUnitario.Value + qryItemDoctoFiscalnValDesconto.Value);
              end;

              Prod.vDesc := Prod.vDesc + qryItemDoctoFiscalnValIcentivoFiscal.Value ;

              with Imposto do
              begin

                  { -- inf. valor do imposto aproximado no item (federal + estadual + municipal) -- }
                  vTotTrib := qryItemDoctoFiscalnValImpostoAproxFed.Value + qryItemDoctoFiscalnValImpostoAproxEst.Value + qryItemDoctoFiscalnValImpostoAproxMun.Value;

                  with ICMS do
                  begin

                      cCdST := Trim(qryItemDoctoFiscalcCdST.Value);

                      case StrToInt(cCdST) of
                              0   : CST   := cst00 ;
                              10  : CST   := cst10 ;
                              20  : CST   := cst20 ;
                              30  : CST   := cst30 ;
                              40  : CST   := cst40 ;
                              41  : CST   := cst41 ;
                              45  : CST   := cst45 ;
                              50  : CST   := cst50 ;
                              51  : CST   := cst51 ;
                              60  : CST   := cst60 ;
                              70  : CST   := cst70 ;
                              80  : CST   := cst80 ;
                              81  : CST   := cst81 ;
                              90  : CST   := cst90 ;
                              101 : CSOSN := csosn101;
                              102 : CSOSN := csosn102;
                              103 : CSOSN := csosn103;
                              201 : CSOSN := csosn201;
                              202 : CSOSN := csosn202;
                              203 : CSOSN := csosn203;
                              300 : CSOSN := csosn300;
                              400 : CSOSN := csosn400;
                              500 : CSOSN := csosn500;
                              900 : CSOSN := csosn900;
                      end ;


                      if (StrToInt(cCdST) > 90) then
                          ICMS.CSOSN := CSOSN
                      else ICMS.CST   := CST;

                      { -- condi��o comentada devido problemas de emiss�o de notas fiscais com ICMS zerado
                           resultando problemas sobre o preenchimento da origem da mercadoria -- }
                      //if (qryItemDoctoFiscalnValICMS.Value > 0) then
                      //begin

                          {CST           := TpcnCSTIcms(AnsiIndexStr(qryItemDoctoFiscalcCdST.asString, ['00', '10','20','30','40','41','45','50','51','60','70','80','81','90'])) ;}
                          ICMS.modBC    := dbiValorOperacao;
                          ICMS.pICMS    := qryItemDoctoFiscalnAliqICMS.Value;
                          ICMS.vICMS    := qryItemDoctoFiscalnValICMS.Value;
                          ICMS.vBC      := qryItemDoctoFiscalnValBaseICMS.Value;
                          ICMS.orig     := TpcnOrigemMercadoria(qryItemDoctoFiscalnCdTabTipoOrigemMercadoria.Value) ;

                         // ICMS.CST   := CST;
                          {if (qryItemDoctoFiscalnValIcentivoFiscal.Value > 0) then
                          begin

                              with ICMS do
                              begin

                                  //CST             := cst40 ;
                                  ICMS.motDesICMS := TpcnMotivoDesoneracaoICMS( 6 ) ;

                              end ;

                          end ;}

                          if (qryItemDoctoFiscalnPercBCICMS.Value > 0) and (qryItemDoctoFiscalnPercBCICMS.Value < 100) then
                              ICMS.pRedBC := qryItemDoctoFiscalnPercBCICMS.Value ;

                      //end;

                      if ((qryItemDoctoFiscalnValICMSSub.Value > 0) or (qryItemDoctoFiscalnValICMSSubRet.Value > 0)) then
                      begin
                          {if (qryItemDoctoFiscalnPercIVA.Value > 0) then
                          begin}
                          ICMS.modBC   := dbiMargemValorAgregado ;
                          ICMS.modBCST := dbisMargemValorAgregado ;
                          {end ;}

                          ICMS.pMVAST   := qryItemDoctoFiscalnPercIVA.Value ;

                          if (qryItemDoctoFiscalnPercBCICMSST.Value > 0) and (qryItemDoctoFiscalnPercBCICMSST.Value < 100) then
                              ICMS.pRedBCST := (100-qryItemDoctoFiscalnPercBCICMSST.Value) ;

                          ICMS.vBCST    := qryItemDoctoFiscalnValBaseICMSSub.Value ;
                          ICMS.vICMSST  := qryItemDoctoFiscalnValICMSSub.Value ;
                          ICMS.pICMSST  := qryItemDoctoFiscalnAliqICMSInternaOrigem.Value ;

                          ICMS.vBCSTRet   := qryItemDoctoFiscalnValBaseICMSSubRet.Value;
                          ICMS.vICMSSTRet := qryItemDoctoFiscalnValICMSSubRet.Value;
                      end;
                  end;

                  if (qryItemDoctoFiscalnValIPI.Value > 0) then
                  begin

                      IPI.CST     := StrToCSTIPI( ok , qryItemDoctoFiscalcCdSTIPI.Value ) ;
                      Prod.EXTIPI := qryItemDoctoFiscalcEXTIPI.Value ;

                      {--s� informar qunado o calculo do IPI for baseado em al�quota, n�o em quantidade --}
                      IPI.vBC  := qryItemDoctoFiscalnValBaseIPI.Value ;
                      IPI.pIPI := qryItemDoctoFiscalnAliqIPI.Value ;
                      IPI.vIPI := qryItemDoctoFiscalnValIPI.Value ;

                  end ;

                  PIS.CST := StrToCSTPIS( ok , qryItemDoctoFiscalcCSTPIS.Value ) ;
                  PIS.vBC  := qryItemDoctoFiscalnValBasePIS.Value ;
                  PIS.pPIS := qryItemDoctoFiscalnAliqPIS.Value ;
                  PIS.vPIS := qryItemDoctoFiscalnValPIS.Value ;

                  if (   (qryItemDoctoFiscalcCSTPIS.Value = '03')
                      or (qryItemDoctoFiscalcCSTPIS.Value = '99') ) then
                  begin
                      PIS.qBCProd   := qryItemDoctoFiscalnQtde.Value ;
                      PIS.vAliqProd := qryItemDoctoFiscalnAliqPIS.Value ;
                  end
                  else
                  begin
                      PIS.pPIS := qryItemDoctoFiscalnAliqPIS.Value ;
                  end ;

                  COFINS.CST := StrToCSTCOFINS( ok , qryItemDoctoFiscalcCSTCOFINS.Value ) ;
                  COFINS.vBC     := qryItemDoctoFiscalnValBaseCOFINS.Value ;
                  COFINS.vCOFINS := qryItemDoctoFiscalnValCOFINS.Value ;

                  if (   (qryItemDoctoFiscalcCSTCOFINS.Value = '03')
                      or (qryItemDoctoFiscalcCSTCOFINS.Value = '99') ) then
                  begin
                      COFINS.qBCProd   := qryItemDoctoFiscalnQtde.Value ;
                      COFINS.vAliqProd := qryItemDoctoFiscalnAliqCOFINS.Value ;
                  end
                  else
                  begin
                      COFINS.pCOFINS := qryItemDoctoFiscalnAliqCOFINS.Value ;
                  end ;

              end;

          end ;


          {-- incrementa a variavel --}
          inc(iItem) ;

          qryItemDoctoFiscal.Next;

      end;

      qryItemDoctoFiscal.Close;

      {-- grava os dados do transporte --}
      Transp.Transporta.CNPJCPF := qryDoctoFiscalcCNPJTransp.Value ;
      Transp.Transporta.xNome   := qryDoctoFiscalcNmTransp.Value ;
      Transp.Transporta.IE      := qryDoctoFiscalcIETransp.Value ;
      Transp.Transporta.xEnder  := qryDoctoFiscalcEnderecoTransp.Value ;
      Transp.Transporta.xMun    := qryDoctoFiscalcCidadeTransp.Value ;
      Transp.Transporta.UF      := qryDoctoFiscalcUFTransp.Value ;

      Transp.veicTransp.placa   := qryDoctoFiscalcPlaca.Value ;
      Transp.veicTransp.UF      := qryDoctoFiscalcUFPlaca.Value ;

      { -- configura modo de frete -- }
      case (qryDoctoFiscalnCdTabTipoModFrete.Value) of
          0 : Transp.modFrete := mfContaEmitente;     //por conta do emitente
          1 : Transp.modFrete := mfContaDestinatario; //por conta do destinat�rio
          2 : Transp.modFrete := mfContaTerceiros;    //por conta de terceiros
          9 : Transp.modFrete := mfSemFrete;          //sem frete
      end;

      case (qryDoctoFiscalnCdIncoterms.Value) of
          1 : Transp.modFrete := TpcnModalidadeFrete(0); //CIF : por conta do emitente
          2 : Transp.modFrete := TpcnModalidadeFrete(1); //FOB : pro conta do destinat�rio
      end;

      with Transp.Vol.Add do
      begin
          qVol  := qryDoctoFiscaliQtdeVolume.Value ;
          pesoL := qryDoctoFiscalnPesoLiq.Value ;
          pesoB := qryDoctoFiscalnPesoBruto.Value ;
          esp   := qryDoctoFiscalcEspecieTransp.Value;
      end ;

      {-- grava os totais da nota --}
      Total.ICMSTot.vBC      := qryDoctoFiscalnValBaseICMS.Value;
      Total.ICMSTot.vICMS    := qryDoctoFiscalnValICMS.Value;
      Total.ICMSTot.vBCST    := qryDoctoFiscalnValBaseICMSSub.Value;
      Total.ICMSTot.vST      := qryDoctoFiscalnValICMSSub.Value;
      Total.ICMSTot.vFrete   := qryDoctoFiscalnValFrete.Value;
      Total.ICMSTot.vIPI     := qryDoctoFiscalnValIPI.Value;
      Total.ICMSTot.vOutro   := qryDoctoFiscalnValOutras.Value;
      Total.ICMSTot.vProd    := qryDoctoFiscalnValProduto.Value;
      Total.ICMSTot.vNF      := qryDoctoFiscalnValTotal.Value;
      Total.ICMSTot.vSeg     := qryDoctoFiscalnValSeguro.Value;
      Total.ICMSTot.vPIS     := qryDoctoFiscalnValPIS.Value ;
      Total.ICMSTot.vCOFINS  := qryDoctoFiscalnValCOFINS.Value ;
      Total.ICMSTot.vTotTrib := qryDoctoFiscalnValTotalImpostoAproxFed.Value + qryDoctoFiscalnValTotalImpostoAproxEst.Value + qryDoctoFiscalnValTotalImpostoAproxMun.Value;

      // desconto no total da nf
      if qryTerceirocFlgTipoDescontoDanfe.Value = 1 then
      begin
          qryDescontoTotal.Close;
          qryDescontoTotal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
          qryDescontoTotal.Open;
          Total.ICMSTot.vDesc  := qryDescontoTotalnValDescontoItens.Value;
          Total.ICMSTot.vProd  := qryDoctoFiscalnValProduto.Value + qryDescontoTotalnValDescontoItens.Value;
      end;

      Total.ICMSTot.vDesc := Total.ICMSTot.vDesc + qryDoctoFiscalnValIcentivoFiscal.Value ;

      {-- informa os dados da cobran�a --}
      iItem := 0 ;

      qryPrazoDoctoFiscal.Close;
      qryPrazoDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal ;
      qryPrazoDoctoFiscal.Open;

      qryPrazoDoctoFiscal.First ;

      for iAux := 1 to qryPrazoDoctoFiscal.recordCount do
      begin
          with Cobr.Dup.Add do
          begin
              nDup  := frmMenu.ZeroEsquerda(qryDoctoFiscaliNrDocto.AsString,5)  ;
              dVenc := qryPrazoDoctoFiscaldDtVenc.Value ;
              vDup  := qryPrazoDoctoFiscalnValPagto.Value ;
          end ;

          qryPrazoDoctoFiscal.Next;
      end ;

      qryPrazoDoctoFiscal.Close;

      {-- gera as observa��es da nota --}
      qryMsgDoctoFiscal.Close;
      qryMsgDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal ;
      qryMsgDoctoFiscal.Open ;

      if (not qryMsgDoctoFiscal.Eof) then
      begin
          while not qryMsgDoctoFiscal.Eof do
          begin

              if (InfAdic.infCpl <> '') then
                  infAdic.infCpl := infAdic.infCpl + ';' ;

              infAdic.infCpl := infAdic.infCpl + qryMsgDoctofiscalcMsg.Value ;

              qryMsgDoctoFiscal.Next;
          end;
      end;

      qryMsgDoctoFiscal.Close;

      qryTabIBPT.Close;
      qryTabIBPT.Parameters.ParamByName('nCdEstadoEmitente').Value := qryTerceiroEmitentenCdEstado.Value;
      qryTabIBPT.Open;

      iDiasIBPT := StrToIntDef(frmMenu.LeParametro('DIASVENCTABIBPT'),0);

      if ((qryTerceiroEmitentenCdEstado.Value > 0) and (not qryTabIBPT.IsEmpty) and ((qryDoctoFiscalnValTotalImpostoAproxFed.Value > 0) or (qryDoctoFiscalnValTotalImpostoAproxEst.Value > 0) or (qryDoctoFiscalnValTotalImpostoAproxMun.Value > 0))) then
      begin
          if ((qryTabIBPTiDiasValidade.Value <= iDiasIBPT) and (iDiasIBPT > 0)) then
              frmMenu.MensagemAlerta('Faltam ' + qryTabIBPTiDiasValidade.AsString + ' dias para o vencimento das al�quotas para atendimento da lei 12.741 da Transpar�ncia Fiscal.'
                                    +#13#13
                                    +'Caso n�o seja atualizado dentro do prazo, o c�lculo deixar� de aparecer no nota fiscal.');

          if (InfAdic.infCpl <> '') then
              infAdic.infCpl := infAdic.infCpl + ';';

          InfAdic.infCpl := InfAdic.infCpl + 'Valor Aprox. Tributos: ';

          if (qryDoctoFiscalnValTotalImpostoAproxFed.Value > 0) then
              InfAdic.infCpl := InfAdic.infCpl + 'Federal R$ ' + qryDoctoFiscalnValTotalImpostoAproxFed.AsString;

          if (qryDoctoFiscalnValTotalImpostoAproxEst.Value > 0) then
              InfAdic.infCpl := InfAdic.infCpl + ' Estadual R$ ' + qryDoctoFiscalnValTotalImpostoAproxEst.AsString;

          if (qryDoctoFiscalnValTotalImpostoAproxMun.Value > 0) then
              InfAdic.infCpl := InfAdic.infCpl + ' Municipal R$ ' + qryDoctoFiscalnValTotalImpostoAproxMun.AsString;

          InfAdic.infCpl := InfAdic.infCpl + ' Fonte: IBPT/FECOMERCIO (' + qryTabIBPTcChave.Value + ').';
      end;
  end;

  if (qryStatusDPECnCdStatusDPEC.Value = 1) then
  begin
      {-- gera a DPEC--}

      if (not ACBrNFe1.WebServices.EnviarDPEC.Executar) then
      begin
          frmMenu.MensagemAlerta('Erro ao obter N�mero de Registro DPEC, tente novamente mais tarde.');
          abort;
      end;

      ACBrNFe1.DANFE.ProtocoloNFe := ACBrNFe1.WebServices.EnviarDPEC.nRegDPEC
                                   + ' ' + DateTimeToStr(ACBrNFe1.WebServices.EnviarDPEC.DhRegDPEC);
  end;

  if (bEspelho) then
  begin
      frmMenu.mensagemUsuario('Validando NFe...');
      ACBrNFe1.NotasFiscais.Valida;

      frmMenu.mensagemUsuario('Imprimindo NFe...');
      ACBrNFe1.NotasFiscais.Imprimir;

      Exit;
  end;

  {--gera a nfe--}
  if ((ACBrNFe1.Configuracoes.Geral.FormaEmissao = teNormal) or (ACBrNFe1.Configuracoes.Geral.FormaEmissao = teSCAN))then
      ACBrNFe1.NotasFiscais.GerarNFe;

  {-- assina eletronicamente a nfe --}
  frmMenu.mensagemUsuario('Assinando NFe...');
  ACBrNFe1.NotasFiscais.Assinar;

  {-- valida o xml da nota --}
  frmMenu.mensagemUsuario('Validando NFe...');
  ACBrNFe1.NotasFiscais.Valida;

  {-- envia para a srf a nfe --}
  frmMenu.mensagemUsuario('Transmitindo NFe...');

  try

      {-- se n�o tem recibo ainda da NFe, faz todo o processamento --}
      if (qryDoctoFiscalcNrReciboNFe.Value = '') then
      begin

          if ((ACBrNFe1.Configuracoes.Geral.FormaEmissao = teNormal) or (ACBrNFe1.Configuracoes.Geral.FormaEmissao = teSCAN)) then
          begin

              { -- verifica se nfe j� foi transmitida, se existir atualiza doc. fiscal e finaliza processo -- }
              cChaveNFe := ConsultaNFe(qryDoctoFiscaliNrDocto.Value);

              if (cChaveNFe <> '') then
              begin
                  frmMenu.mensagemUsuario('Consultando recibo NFe...');

                  {-- Recupera caminho aonde o recibo � salvo}
                  cPatchReciboNFe := ACBrNFe1.Configuracoes.Geral.PathSalvar + qryDoctoFiscalnCdDoctoFiscal.AsString + '-rec.xml';

                  {-- Verifica se o recibo existe --}
                  if FileExists(cPatchReciboNFe) then
                  begin
                      AssignFile(ArqReciboNFe, cPatchReciboNFe);
                      Reset(ArqReciboNFe);
                      Read(ArqReciboNFe, cNrReciboNFe);
                      CloseFile(ArqReciboNFe);
                      
                      Delete(cNrReciboNFe,1,Pos('<nRec>',cNrReciboNFe) + 5);
                      
                      cNrReciboNFe := StringReplace(cNrReciboNFe,'</nRec><tMed>1</tMed></infRec></retEnviNFe>','',[rfReplaceAll,rfIgnoreCase]);
                  end;

                  frmMenu.mensagemUsuario('Atualizando inf. NFe...');

                  {-- Corre��o pelo tamanho do XML --}
                  qryAux.Close;
                  qryAux.SQL.Clear;
                  qryAux.SQL.Add('UPDATE DoctoFiscal');
                  qryAux.SQL.Add('   SET cXMLNFe        = ' + #39 + StringReplace(ACBrNFe1.NotasFiscais.Items[0].XML,':',''' + CHAR(50) + ''',[rfReplaceAll]) + #39);
                  qryAux.SQL.Add(' WHERE nCdDoctoFiscal = ' + qryDoctoFiscalnCdDoctoFiscal.AsString);
                  qryAux.ExecSQL;

                  qryDoctoFiscal.Edit;
                  qryDoctoFiscalcCaminhoXML.Value      := ACBrNFe1.Configuracoes.Geral.PathSalvar;
                  qryDoctoFiscalcArquivoXML.Value      := cChaveNFe + '-NFe.xml';
                  qryDoctoFiscalcNrProtocoloNFe.Value  := ACBrNFe1.WebServices.Consulta.Protocolo;
                  qryDoctoFiscalcChaveNFe.Value        := cChaveNFe;
                  qryDoctoFiscaliStatusRetorno.Value   := ACBrNFe1.WebServices.Consulta.cStat;
                  qryDoctoFiscalcNmStatusRetorno.Value := ACBrNFe1.WebServices.Consulta.XMotivo;
                  qryDoctoFiscalcNrReciboNFe.Value     := cNrReciboNFe;
                  qryDoctoFiscal.Post;

                  {-- salva a nfe em disco --}
                  ACBrNFe1.NotasFiscais.SaveToFile();

                  {-- Gera o PDF da Nota Fiscal --}
                  ACBrNFeDANFERave1.PathPDF := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\';
                  ACBrNFe1.NotasFiscais.ImprimirPDF;

                  qryDoctoFiscal.Close;
                  frmMenu.mensagemUsuario('');

                  Exit;
              end;

              if ACBrNFe1.WebServices.Envia( qryDoctoFiscalnCdDoctoFiscal.Value ) then
              begin

                  {-- atualiza o documento fiscal com o n�mero de recibo de entrega da NFe --}
                  qryDoctoFiscal.Edit;
                  qryDoctoFiscalcNrReciboNFe.Value                 := ACBrNFe1.WebServices.Retorno.NFeRetorno.nRec;
                  qryDoctoFiscalnCdTabTipoEmissaoDoctoFiscal.Value := ACBrNFe1.Configuracoes.Geral.FormaEmissaoCodigo;
                  qryDoctoFiscalcCaminhoXML.Value                  := ACBrNFe1.Configuracoes.Geral.PathSalvar;
                  qryDoctoFiscalcFlgAmbienteHom.Value              := cFlgAmbienteHom;
                  qryDoctoFiscal.Post;

              end ;

          end ;

      end
      else  {-- se ja tem recibo da NFe, tenta obter o resultado do processamento --}
      begin


          ACBrNFe1.WebServices.Retorno.Recibo := qryDoctoFiscalcNrReciboNFe.Value;
          ACBrNFe1.WebServices.Retorno.Executar;

          {ACBrNFe1.WebServices.Consulta.Executar}

      end ;

      {-- salva a nfe em disco --}
      ACBrNFe1.NotasFiscais.SaveToFile();

      {-- lote em processamento --}
      if ( ACBrNFe1.WebServices.Retorno.CStat = 105 ) then
      begin

          frmMenu.MensagemAlerta('Lote em processamento! Aguarde alguns instantes e tente imprimir novamente este documento.') ;
          exit ;

      end ;

      { -- lote processado OK -- }
      if    ( ACBrNFe1.WebServices.Retorno.cStat = 100 ) //100 : autorizado o uso da nf-e
         or ( ACBrNFe1.WebServices.Retorno.cStat = 104 ) //104 : lote processado
         or ( ACBrNFe1.WebServices.Retorno.cStat = 302 ) //302 : uso denegado (irregularidade fiscal do destinat�rio)
      then
      begin

          {-- atualiza o documento fiscal com os dados da NFe --}

          qryDoctoFiscal.Edit;
          {qryDoctoFiscalcXMLNFe.Value  := StringReplace(ACBrNFe1.NotasFiscais.Items[0].XML,'''',''' + CHAR(39) + ''',[rfReplaceAll]);}

          {-- Corre��o pelo tamanho do XML --} 
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('UPDATE DoctoFiscal');
          qryAux.SQL.Add('   SET cXMLNFe        = ' + #39 + StringReplace(ACBrNFe1.NotasFiscais.Items[0].XML,':',''' + CHAR(50) + ''',[rfReplaceAll]) + #39);
          qryAux.SQL.Add(' WHERE nCdDoctoFiscal = ' + qryDoctoFiscalnCdDoctoFiscal.AsString);
          qryAux.ExecSQL;

          if (ACBrNFe1.Configuracoes.Geral.FormaEmissao = teDPEC) then
          begin
              qryDoctoFiscalcNrProtocoloNFe.Value  := ACBrNFe1.DANFE.ProtocoloNFe;
              qryDoctoFiscalcNrProtocoloDPEC.Value := ACBrNFe1.DANFE.ProtocoloNFe;
              qryDoctoFiscalcChaveNFe.Value        := ACBrNFe1.WebServices.EnviarDPEC.NFeChave;
              qryDoctoFiscalcArquivoXML.Value      := ACBrNFe1.WebServices.EnviarDPEC.NFeChave + '-NFe.xml';
              qryDoctoFiscaliStatusRetorno.Value   := ACBrNFe1.WebServices.Retorno.cStat;
              qryDoctoFiscalcNmStatusRetorno.Value := ACBrNFe1.WebServices.Retorno.xMotivo;
              qryDoctoFiscalcFlgAmbienteHom.Value  := cFlgAmbienteHom;
          end
          else
          begin
              qryDoctoFiscalcNrProtocoloNFe.Value  := ACBrNFe1.WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].nProt;
              qryDoctoFiscalcChaveNFe.Value        := ACBrNFe1.WebServices.Retorno.ChaveNFe;
              qryDoctoFiscalcArquivoXML.Value      := ACBrNFe1.WebServices.Retorno.ChaveNFe + '-NFe.xml';
              qryDoctoFiscaliStatusRetorno.Value   := ACBrNFe1.WebServices.Retorno.cStat;
              qryDoctoFiscalcNmStatusRetorno.Value := ACBrNFe1.WebServices.Retorno.xMotivo;
              qryDoctoFiscalcFlgAmbienteHom.Value  := cFlgAmbienteHom;
          end;

          qryDoctoFiscal.Post;

      end ;

  except
      raise;
  end ;

  if (qryDoctoFiscalnCdTabTipoEmissaoDoctoFiscal.Value = 4) then
      ACBrNFe1.DANFE.ProtocoloNFe := qryDoctoFiscalcNrProtocoloDPEC.Value;

  {-- Gera o PDF da Nota Fiscal --}
  ACBrNFeDANFERave1.PathPDF := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\';

  ACBrNFe1.NotasFiscais.ImprimirPDF;

  qryDoctoFiscal.Close;

  frmMenu.mensagemUsuario('');

end;

procedure TfrmEmiteNFe2.imprimirDANFe(nCdDoctoFiscal: integer);
begin

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryDoctoFiscal.Open;

  // Limpas as Notas Fiscais que possam estar na mem�ria do componente
  frmMenu.mensagemUsuario('Carregando XML NFe...');
  ACBrNFe1.NotasFiscais.Clear;

  ACBrNFe1.NotasFiscais.LoadFromFile(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\Envio e Resposta\' + qryDoctoFiscalcArquivoXML.Value) ;

  {-- se o tipo de emiss�o for DEPEC - tpEmiss = 4, adiciona o protocolo --}
  {--

  *** BLOCO MOVIDO PARA A ROTINA QUE GERA A NFE ***
  if (qryDoctoFiscalnCdTabTipoEmissaoDoctoFiscal.Value = 4) then
      ACBrNFe1.DANFE.ProtocoloNFe := qryDoctoFiscalcNrProtocoloDPEC.Value;

  ACBrNFeDANFERave1.PathPDF := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\';

  ACBrNFe1.NotasFiscais.ImprimirPDF; --}

  {-- se o tipo de emiss�o for DEPEC - tpEmiss = 4, adiciona o protocolo --}
  if (qryDoctoFiscalnCdTabTipoEmissaoDoctoFiscal.Value = 4) then
      ACBrNFe1.DANFE.ProtocoloNFe := qryDoctoFiscalcNrProtocoloDPEC.Value;

  {-- imprime o danfe --}
  frmMenu.mensagemUsuario('Imprimindo DANFe...');

  ACBrNFe1.NotasFiscais.Imprimir;

  frmMenu.mensagemUsuario('');
end;

procedure TfrmEmiteNFe2.enviaEmailNFe(nCdDoctoFiscal : integer; cPara, cCopia : String);
var
  Assunto, Servidor, Usuario, Senha, DanfeXML, DanfePDF, DanfePDF2 : String;
  Mensagem, Copia : TStrings;
  Porta           : Integer;
  SMTP            : TIdSMTP;
  idMessage       : TIdMessage;
  idTextPart      : TIdText;
  nCdModDocumento : Integer;
  i               : Integer;
  arrParam        : Array of String;
  strArquivos     : TStringList;
  Busca           : TSearchRec;
  objModDoc       : TfrmModeloDocumento;
begin

  Mensagem  := TStringList.Create;
  Copia     := TStringList.Create;
  SMTP      := TIdSMTP.Create(nil);
  idMessage := TIdMessage.Create(nil);

  {-- Prepara qrys para o envio do email --}

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryDoctoFiscal.Open;

  qryConfigAmbiente.Close ;
  qryConfigAmbiente.Parameters.ParamByName('cNmComputador').Value := frmMenu.cNomeComputador;
  qryConfigAmbiente.Open ;

  frmMenu.mensagemUsuario('Carregando XML/PDF NFe...');

  // Armazena o caminho dos arquivos para poder anexar a mensagem;
  DanfeXML  := qryConfigAmbientecPathArqNfe.Value + 'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\Envio e Resposta\' + qryDoctoFiscalcArquivoXML.Value ;
  DanfePDF  := qryConfigAmbientecPathArqNfe.Value + 'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\' + qryDoctoFiscalcChaveNFe.Value + '.pdf' ;
  DanfePDF2 := qryConfigAmbientecPathArqNfe.Value + 'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\' + qryDoctoFiscalcChaveNFe.Value + '-nfe.pdf' ;

  { -- verifica se arquivos existem -- }
  if (not FilesExists(DanfeXML)) then
  begin
      frmMenu.MensagemAlerta('Arquivo XML n�o encontrado.' + #13 + 'Caminho: ' + DanfeXML);
      Abort;
  end;

  if (not FilesExists(DanfePDF)) then
  begin
      if (not FilesExists(DanfePDF2)) then
      begin
          frmMenu.MensagemAlerta('Arquivo PDF n�o encontrado.' + #13 + 'Caminho: ' + DanfePDF);
          Abort;
      end;

      DanfePDF := DanfePDF2;
  end;

  //Prepara mensagem
  frmMenu.mensagemUsuario('Preparando corpo do e-mail...');
  Assunto := 'Entrega da NFe n� ' + qryDoctoFiscaliNrDocto.AsString;

    {-- Verifica se tem parametro --}
  nCdModDocumento := StrToIntDef(frmMenu.LeParametro('MODDOCEMAILNFE'),0);

  if (nCdModDocumento <> 0) then
  begin
      try
          objModDoc := TfrmModeloDocumento.Create(Nil);

          qryModeloDocumento.Close;
          qryModeloDocumento.Parameters.ParamByName('nPK').Value := nCdModDocumento;
          qryModeloDocumento.Open;

          {-- Parametros de variaveis do Documento --}
          SetLength(arrParam,5);
          arrParam[0] := 'Loja,'            + IntToStr(qryDoctoFiscalnCdLoja.Value);     // Cod da Loja
          arrParam[1] := 'TerceiroOrigem,'  + IntToStr(qryDoctoFiscalnCdTerceiro.Value); // Cod do Terceiro Origem
          arrParam[2] := 'TerceiroDestino,' + IntToStr(qryDoctoFiscalnCdTerceiro.Value); // Cod do Terceiro Destino
          arrParam[3] := 'Empresa,'         + IntToStr(qryDoctoFiscalnCdEmpresa.Value);  // Cod da Empresa
          arrParam[4] := 'DoctoFiscal,'     + IntToStr(nCdDoctoFiscal);

          {-- Verifica se � Texto ou HTML --}

          {-- Se for Texto --}
          if (qryModeloDocumentonCdTabTipoModeloDoc.Value = 1) then
          begin
              Mensagem.Clear;
              {-- Substitui as Variaveis --}
              Mensagem.Add(objModDoc.fnSubistituiVariaveis(nCdModDocumento, arrParam));
              idMessage.Body := Mensagem;
          end
          else
          {-- Se for HTML --}
          begin
              {-- Pega os anexos --}
              strArquivos := TStringList.Create;
              if FindFirst('.\Arquivos\ModeloDocumento\' + IntToStr(nCdModDocumento) + '\*.jpg', faArchive, Busca) = 0 then
              begin
                  repeat
                      strArquivos.Add('.\Arquivos\ModeloDocumento\' + IntToStr(nCdModDocumento) + '\' + Busca.Name);
                  until FindNext(Busca) <> 0;
                  FindClose(Busca);
              end;

              {-- Cria o corpo do e-mail em HTML --}
              idTextPart             := TIdText.Create(idMessage.MessageParts, nil);
              idTextPart.ContentType := 'text/plain';
              idTextPart.Body.Add('E-mail');

              idTextPart             := TIdText.Create(idMessage.MessageParts, nil);
              idTextPart.ContentType := 'text/html';

              {-- Chama a fun��o da ModeloDocumento, fnSubistituiVariaveis, que retorna o conteudo do texto formatado --}
              idTextPart.Body.Add(objModDoc.fnSubistituiVariaveis(nCdModDocumento, arrParam) );

              {-- Anexa Arquivos -- }
              for i := 0 to strArquivos.Count - 1 do
                  TIdAttachment.Create(idMessage.MessageParts, strArquivos[i]);
          end;
      finally
          FreeAndNil(objModDoc);
      end;
  end
  else
  begin
      if(frmMenu.LeParametro('VAREJO') = 'N') then
      begin
          Mensagem.Clear;
          Mensagem.Add(qryDoctoFiscalcNmTerceiro.AsString + #13#13);
          Mensagem.Add('Entrega da NF-e n� ' + qryDoctoFiscaliNrDocto.AsString + ' com a chave ' + qryDoctoFiscalcChaveNFe.Value +'.');
          Mensagem.Add(#13#13 + 'Veja o XML em anexo, ou fa�a a Consulta de autenticidade no portal nacional da NF-e <www.nfe.fazenda.gov.br/portal> ou no site da Sefaz.');
          Mensagem.Add(#13#13#13 + '<http://www.er2soft.com.br> '+ #13#13);
          Mensagem.Add('Conhe�a o Sistema de Gest�o Empresarial que integra sua empresa e seus parceiros de neg�cios com simplicidade.');
          Mensagem.Add(#13#13 +'O ER2SOFT ERP <http://www.er2soft.com.br > � um sistema f�cil de usar que atende totalmente as necessidades das empresas com baixo custo.');
          Mensagem.Add(#13#13 +'Ligue para a ER2Soft e tenha maiores informa��es (11) 2378-3200 ou acesse o site da ER2Soft.');

          {-- adiciona ao corpo do e-mail --}
          idMessage.Body := Mensagem;
      end
      else
      begin
          Mensagem.Clear;
          Mensagem.Add(qryDoctoFiscalcNmTerceiro.AsString + #13#13);
          Mensagem.Add('Entrega da NF-e n� ' + qryDoctoFiscaliNrDocto.AsString + ' com a chave ' + qryDoctoFiscalcChaveNFe.Value +'.');
          Mensagem.Add(#13#13 + 'Veja o XML em anexo, ou fa�a a Consulta de autenticidade no portal nacional da NF-e <www.nfe.fazenda.gov.br/portal> ou no site da Sefaz.');
          Mensagem.Add(#13#13#13 + '<http://www.er2soft.com.br> '+ #13#13);
          Mensagem.Add('Conhe�a o Sistema de Automa��o Comercial que integra suas lojas e seus parceiros de neg�cios com simplicidade.');
          Mensagem.Add(#13#13 +'O ER2SOFT ERP <http://www.er2soft.com.br > � um sistema f�cil de usar que atende totalmente as necessidades dos lojistas com baixo custo.');
          Mensagem.Add(#13#13 +'Ligue para a ER2Soft e tenha maiores informa��es (11) 2378-3200 ou acesse o site da ER2Soft.');

          {-- adiciona ao corpo do e-mail --}
          idMessage.Body := Mensagem;
      end;
  end;

  frmMenu.mensagemUsuario('Enviando e-mail...');

  {-- Pega as Configura��es de ambiente --}

  Servidor := qryConfigAmbientecServidorSMTP.Value;
  Porta    := qryConfigAmbientenPortaSMTP.Value;
  Usuario  := qryConfigAmbientecEmail.Value;

  Senha    := qryConfigAmbientecSenhaEmail.Value;

  {-- Passa as configura��es de conex�o para o INDY --}

  SMTP.Host               := Servidor;
  SMTP.Port               := Porta;
  SMTP.Username           := Usuario;
  SMTP.Password           := Senha;
  SMTP.AuthenticationType := atLogin;

  {-- prepara os dados da mensagem --}

  idMessage.From.Address              := Usuario;
  idMessage.Recipients.EMailAddresses := cPara;
  idMessage.CCList.EMailAddresses     := cCopia;
  idMessage.Subject                   := Assunto;
  idMessage.Body                      := Mensagem;

  {-- Anexa os arquivos � mensagem se esses existirem --}

  if FileExists(DanfeXML) then
       TIdAttachment.Create(idMessage.MessageParts, DanfeXML)
  else
  begin
      frmMenu.MensagemErro(DanfeXML + ' parace ser um caminho inv�lido, a mensagem n�o pode ser enviada.');
      abort;
  end;

  if FileExists(DanfePDF) then
      TIdAttachment.Create(idMessage.MessageParts, DanfePDF)
  else if (frmMenu.MessageDlg(DanfePDF + ' parace ser um caminho inv�lido, Deseja enviar apenas o XML. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
          abort;

  {-- envia o email --}

  try
      SMTP.Connect(2000);
      SMTP.Send(idMessage);
  except on E:Exception do
       begin
           frmMenu.MensagemErro('Erro: ' + E.Message) ;

           if SMTP.Connected then SMTP.Disconnect;
           frmMenu.mensagemUsuario('');
           raise;
      end;
  end;

  if SMTP.Connected then SMTP.Disconnect;

  frmMenu.mensagemUsuario('');

end;

procedure TfrmEmiteNFe2.prSelCertificadoDigital;
var
  stringListDadosCert : TStringList;
  i                   : Integer;

  function fnTrataString(cValor : String; cTipoTrat : Char) : String;
  begin
      if (   (cTipoTrat <> 'T')
          and(cTipoTrat <> 'V')) then
        Result := Null;

      if (cTipoTrat = 'T') then
          Result := TrimLeft(TrimRight((Copy(cValor,1,(Pos('=',cValor)-1)))));

      if (cTipoTrat = 'V') then
          Result := TrimLeft(TrimRight((Copy(cValor,(Pos('=',cValor)+1),Length(cValor)))));
  end;
begin
  cCertNumSerie := '';
  cCertCNPJ     := '';
  cCertMun      := '';
  cCertUF       := '';
  cCertPais     := '';

  try
      {$IFNDEF ACBrNFeOpenSSL}
      cCertNumSerie   := Trim(ACBrNFe1.Configuracoes.Certificados.SelecionarCertificado);
      {$ENDIF}
  except
      frmMenu.MensagemAlerta('Certificado digital n�o selecionado.');
      Exit;
  end;

  cCertCNPJ := Trim(ACBrNFe1.Configuracoes.Certificados.CNPJ);

  stringListDadosCert := TStringList.Create;

  ExtractStrings([','],[], PChar(ACBrNFe1.Configuracoes.Certificados.SubjectName), stringListDadosCert);

  for i := 0 to stringListDadosCert.Count -1 do
  begin
      if (fnTrataString(stringListDadosCert[i],'T') = 'L') then
          cCertMun := AnsiUpperCase(fnTrataString(stringListDadosCert[i],'V'));

      if (fnTrataString(stringListDadosCert[i],'T') = 'S') then
          cCertUF := AnsiUpperCase(fnTrataString(stringListDadosCert[i],'V'));

      if (fnTrataString(stringListDadosCert[i],'T') = 'C') then
          cCertPais := AnsiUpperCase(fnTrataString(stringListDadosCert[i],'V'));
  end;

  qryConsultaDadosCert.Close;
  qryConsultaDadosCert.Parameters.ParamByName('cNmMunicipio').Value := cCertMun;
  qryConsultaDadosCert.Parameters.ParamByName('cUF').Value          := cCertUF;
  qryConsultaDadosCert.Open;
end;

procedure TfrmEmiteNFe2.preparaNFe;
var
  cPathSistemaNFe : String;
begin
    qryConfigAmbiente.Close;
    qryConfigAmbiente.Parameters.ParamByName('cNmComputador').Value := frmMenu.cNomeComputador;
    qryConfigAmbiente.Open;

  if (qryConfigAmbientenCdTipoAmbienteNFe.Value = 0) then
  begin
      cFlgAmbienteHom := 0;
      cPathSistemaNFe := frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + 'Producao\NFe\'
  end
  else
  begin
      cFlgAmbienteHom := 1;
      cPathSistemaNFe := frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + 'Homologacao\NFe\';
  end;

    frmMenu.mensagemUsuario('Preparando NFe...');

    if (qryConfigAmbiente.Eof) then
    begin
        frmMenu.MensagemErro('Configura��o de ambiente n�o encontrada para emiss�o da NFe.') ;
        abort ;
    end ;

    if (qryConfigAmbientecFlgEmiteNFe.Value = 0) then
    begin
        frmMenu.ShowMessage('Esta esta��o de trabalho n�o est� configurada para emiss�o de NFe.');
        abort ;
    end ;

    { -- se certificado n�o foi informado na configura��o de ambiente, selecione certificado digital -- }
    if (qryConfigAmbientecNrSerieCertificadoNFe.Value = '') then
    begin
        prSelCertificadoDigital;

        if (cCertNumSerie = '') then
        begin
            //frmMenu.MensagemErro('N�mero de s�rie do certificado digital n�o informado na configura��o do ambiente.');
            Abort;
        end;
    end
    else
        cCertNumSerie := Trim(qryConfigAmbientecNrSerieCertificadoNFe.Value);

    // verifica se os diret�rios de log existem, se n�o existir, cria.
    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value) then
    begin
        frmMenu.MensagemErro('Caminho para grava��o dos arquivos da Nfe n�o encontrado.' +#13#13 + 'Caminho : ' + qryConfigAmbientecPathArqNfe.Value);
        abort ;
    end ;

    // verifica se o arquivo .rav existe em subdiretorio onde est� o executavel.
    if not FileExists(frmMenu.cPathSistema + 'NFe\Reports\NotaFiscalEletronica.rav') then
    begin
        frmMenu.MensagemErro('Arquivo .\Nfe\Reports\NotaFiscalEletronica.rav. n�o encontrado.');
        abort ;
    end ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\Envio e Resposta\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\Envio e Resposta\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFe\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFe\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeCan\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeCan\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeDPEC\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeDPEC\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeInu\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeInu\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\CCe\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\CCe\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'NFe\Schemas\') then
    begin
        frmMenu.MensagemErro('Caminho de Schemas da NFe n�o encontrado.' +#13#13 + 'Caminho: ' + qryConfigAmbientecPathArqNfe.Value+'NFe\Schemas\');
        abort ;
    end ;

    ACBrNFe1.Configuracoes.Geral.PathSalvar  := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\Envio e Resposta\';
    ACBrNFe1.Configuracoes.Geral.PathSchemas := qryConfigAmbientecPathArqNfe.Value+'NFe\Schemas\';
    ACBrNFe1.Configuracoes.Geral.VersaoDF    := ve310;
    ACBrNFe1.Configuracoes.Geral.Salvar      := True ;

    ACBrNFe1.Configuracoes.Arquivos.PathNFe  := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFe\';
    ACBrNFe1.Configuracoes.Arquivos.PathCan  := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeCan\';
    ACBrNFe1.Configuracoes.Arquivos.PathCCe  := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\CCe\';
    ACBrNFe1.Configuracoes.Arquivos.PathDPEC := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeDPEC\';
    ACBrNFe1.Configuracoes.Arquivos.PathInu  := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeInu\';
    ACBrNFe1.Configuracoes.Arquivos.Salvar   := True ;

    if (Trim(qryConfigAmbientecUFEmissaoNFe.Value) <> '') then
        ACBrNFe1.Configuracoes.WebServices.UF := qryConfigAmbientecUFEmissaoNFe.Value
    else
        ACBrNFe1.Configuracoes.WebServices.UF := qryConsultaDadosCertcUF.Value;


    if (qryConfigAmbientenCdTipoAmbienteNFe.Value = 0) then
        ACBrNFe1.Configuracoes.WebServices.Ambiente := taProducao
    else ACBrNFe1.Configuracoes.WebServices.Ambiente := taHomologacao ;

    ACBrNFe1.Configuracoes.WebServices.Visualizar   := false;
    ACBrNFe1.Configuracoes.Certificados.NumeroSerie := cCertNumSerie;

    // configura��es do DANFE

    ACBrNFe1.DANFE.Logo       := qryConfigAmbientecPathLogoNFe.Value ;
    ACBrNFeDANFERave1.PathPDF := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\';

    // Caminho fixo, para evitar poss�vel erro de arquivo n�o encontrado, em atualiza��es
    ACBrNFeDANFERave1.RavFile := frmMenu.cPathSistema + 'NFe\Reports\NotaFiscalEletronica.rav';

    {-- Como padr�o configura a forma de emiss�o para normal --}
    ACBrNFe1.Configuracoes.Geral.FormaEmissao := teNormal;

    if (Date > ACBrNFe1.Configuracoes.Certificados.DataVenc) then
    begin
        frmMenu.MensagemAlerta('O Certificado digital (NS: ' + cCertNumSerie + ') est� vencido. Imposs�vel transmitir a nota fiscal eletr�nica.') ;
        abort;
    end ;

    if (daysBetween(Date,ACBrNFe1.Configuracoes.Certificados.DataVenc) <= 90) then
    begin
        frmMenu.MensagemAlerta('O Certificado digital (NS: ' + cCertNumSerie + ') vencer� em ' + intToStr( daysBetween(Date,ACBrNFe1.Configuracoes.Certificados.DataVenc) ) + ' dias. Favor providenciar um novo certificado.') ;
    end ;
end;

procedure TfrmEmiteNFe2.preparaNFCe();
var
  cPathSistemaNFCe : String;
  buscaArq         : TSearchRec;
begin
  qryConfigAmbiente.Close;
  qryConfigAmbiente.Parameters.ParamByName('cNmComputador').Value := frmMenu.cNomeComputador;
  qryConfigAmbiente.Open;

  if (qryConfigAmbientenCdTipoAmbienteNFe.Value = 0) then
  begin
      cFlgAmbienteHom  := 0;
      cPathSistemaNFCe := frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + 'Producao\NFCe\';
  end
  else
  begin
      cFlgAmbienteHom  := 1;
      cPathSistemaNFCe := frmMenu.cPathSistema + frmMenu.cPathEmpAtiva + 'Homologacao\NFCe\';
  end;

  frmMenu.mensagemUsuario('Preparando ambiente...');

  if (qryConfigAmbiente.Eof) then
  begin
      Raise Exception.Create('Configura��o de ambiente n�o encontrada para emiss�o da NFe/NFCe.');
  end;

  if (qryConfigAmbientecFlgEmiteNFe.Value = 0) then
  begin
      Raise Exception.Create('Esta esta��o de trabalho n�o est� configurada para emiss�o da NFe/NFCe.');
  end;

  if (qryConfigAmbientecNrSerieCertificadoNFe.Value = '') then
  begin
      Raise Exception.Create('N�mero de s�rie do certificado digital n�o informado na configura��o de ambiente.');
  end;

  frmMenu.prPreparaAmbiente;

  if (not FileExists(frmMenu.cPathSistema + 'Arquivos\Reports\NotaFiscalEletronica.rav')) then
  begin
      Raise Exception.Create('Arquivo "NotaFiscalEletronica.rav" n�o encontrado no diret�rio: .\Arquivos\Reports\NotaFiscalEletronica.rav');
  end;

  if (FindFirst(frmMenu.cPathSistema + 'Arquivos\Schemas\*.*',faAnyFile - faDirectory, buscaArq) <> 0) then
  begin
      Raise Exception.Create('Arquivos "Schemas (*.xsd)" n�o encontrado no diret�rio: .\Arquivos\Schemas\*.xsd');
  end;

  ACBrNFeDANFERave1.RavFile := frmMenu.cPathSistema + 'Arquivos\Reports\NotaFiscalEletronica.rav';
  ACBrNFeDANFERave1.PathPDF := cPathSistemaNFCe + 'PDF\';

  ACBrNFe1.Configuracoes.Geral.PathSalvar   := cPathSistemaNFCe + 'Envio e Resposta\';
  ACBrNFe1.Configuracoes.Geral.PathSchemas  := frmMenu.cPathSistema + 'Arquivos\Schemas\';
  ACBrNFe1.Configuracoes.Geral.ModeloDF     := moNFCe;
  ACBrNFe1.Configuracoes.Geral.FormaEmissao := teNormal;
  ACBrNFe1.Configuracoes.Geral.VersaoDF     := ve310;
  ACBrNFe1.Configuracoes.Geral.Salvar       := True;

  ACBrNFe1.Configuracoes.Arquivos.PathNFe := cPathSistemaNFCe + 'NFCeAtivo\';
  ACBrNFe1.Configuracoes.Arquivos.PathCan := cPathSistemaNFCe + 'NFCeCanc\';
  ACBrNFe1.Configuracoes.Arquivos.Salvar  := True;

  ACBrNFe1.Configuracoes.Certificados.NumeroSerie := qryConfigAmbientecNrSerieCertificadoNFe.Value ;

  ACBrNFe1.Configuracoes.WebServices.UF         := qryConfigAmbientecUFEmissaoNfe.Value;
  ACBrNFe1.Configuracoes.WebServices.Visualizar := False;

  if (qryConfigAmbientenCdTipoAmbienteNFe.Value = 0) then
      ACBrNFe1.Configuracoes.WebServices.Ambiente := taProducao
  else
      ACBrNFe1.Configuracoes.WebServices.Ambiente := taHomologacao;

  if (Date > ACBrNFe1.Configuracoes.Certificados.DataVenc) then
  begin
      Raise Exception.Create('O Certificado digital (NS: ' + trim( qryConfigAmbientecNrSerieCertificadoNFe.Value ) + ') est� vencido, impedindo a transmiss�o da nota fiscal de consumidor eletr�nica.');
  end;

  if (daysBetween(Date,ACBrNFe1.Configuracoes.Certificados.DataVenc) <= 90) then
  begin
      frmMenu.MensagemAlerta('O Certificado digital (NS: ' + trim( qryConfigAmbientecNrSerieCertificadoNFe.Value ) + ') vencer� em ' + intToStr( daysBetween(Date,ACBrNFe1.Configuracoes.Certificados.DataVenc) ) + ' dias. Favor providenciar um novo certificado.');
  end;
end;

procedure TfrmEmiteNFe2.ReimprimirDANFe(nCdDoctoFiscal: integer);
var
  streamNF : TStringStream ;
begin

  // seta as propriedades da NFe

  qryConfigAmbiente.Close;
  qryConfigAmbiente.Parameters.ParamByName('cNmComputador').Value := frmMenu.cNomeComputador;
  qryConfigAmbiente.Open;

  // configura��es do DANFE

  ACBrNFe1.DANFE.Logo       := qryConfigAmbientecPathLogoNFe.Value ;
  ACBrNFeDANFERave1.PathPDF := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\';

  // Caminho fixo, para evitar poss�vel erro de arquivo n�o encontrado, em atualiza��es
  ACBrNFeDANFERave1.RavFile := '.\Nfe\Reports\NotaFiscalEletronica.rav';

  
  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryDoctoFiscal.Open;

  {preparaNFe();}

  streamNF := TStringStream.Create( qryDoctoFiscalcXMLNFe.Value ) ;
  {streamNF.WriteString(  );}

  // Limpas as Notas Fiscais que possam estar na mem�ria do componente
  frmMenu.mensagemUsuario('Carregando XML NFe...');
  ACBrNFe1.NotasFiscais.Clear;

  ACBrNFe1.NotasFiscais.LoadFromStream( streamNF );

  //.LoadFromFile(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\Envio e Resposta\' + qryDoctoFiscalcArquivoXML.Value) ;

  if (qryDoctoFiscalnCdTabTipoEmissaoDoctoFiscal.Value = 4) then
      ACBrNFe1.DANFE.ProtocoloNFe := qryDoctoFiscalcNrProtocoloDPEC.Value;

  {-- imprime o danfe --}
  ACBrNFe1.NotasFiscais.Imprimir;

  frmMenu.mensagemUsuario('');

end;

function TfrmEmiteNFe2.StatusWebService(nCdWebService : integer) : TStatusWebService;
begin

    frmMenu.mensagemUsuario('Consultando Status...');

    if ((nCdWebService = 0) or (nCdWebService = 1)) then
    begin
        {-- muda a forma de emiss�o para normal, para consultar o webservice da
        sefaz de origem  --}
        ACBrNFe1.Configuracoes.Geral.FormaEmissao := teNormal;

        try
            ACBrNFe1.WebServices.StatusServico.Executar;

            Result.nCdStatusSEFAZ := ACBrNFe1.WebServices.StatusServico.cStat;
            Result.cNmStatusSEFAZ := ACBrNFe1.WebServices.StatusServico.xMotivo;
            Result.cObsSEFAZ      := ACBrNFe1.WebServices.StatusServico.xObs;
        except on E: Exception do
            begin
                Result.nCdStatusSEFAZ := 404;
                Result.cNmStatusSEFAZ := 'Imposs�vel estabelecer conex�o';
                Result.cObsSEFAZ      := E.Message;
            end;
        end;

    end;

    if ((nCdWebService = 0) or (nCdWebService = 3)) then
    begin
        {-- muda a forma de emiss�o para scan, para consultar o webservice de scan  --}
        ACBrNFe1.Configuracoes.Geral.FormaEmissao := teSCAN;

        try
            ACBrNFe1.WebServices.StatusServico.Executar;

            Result.nCdStatusSCAN := ACBrNFe1.WebServices.StatusServico.cStat;
            Result.cNmStatusSCAN := ACBrNFe1.WebServices.StatusServico.xMotivo;
            Result.cObsSCAN      := ACBrNFe1.WebServices.StatusServico.xObs;
        except on E: Exception do
            begin
                Result.nCdStatusSEFAZ := 404;
                Result.cNmStatusSEFAZ := 'Imposs�vel estabelecer conex�o';
                Result.cObsSEFAZ      := E.Message;
            end;
        end;
    end;

    frmMenu.mensagemUsuario('');
end;

procedure TfrmEmiteNFe2.enviaEmailCancNFe(nCdDoctoFiscal: integer; cPara,
  cCopia: String);
var
  Assunto, Servidor, Usuario, Senha, DanfeXML, DanfePDF, cChave, cPathXML: String;
  Mensagem, Copia : TStrings;
  Porta : Integer;
  SMTP : TIdSMTP;
  idMessage : TIdMessage;
begin

  Mensagem  := TStringList.Create;
  Copia     := TStringList.Create;
  SMTP      := TIdSMTP.Create(nil);
  idMessage := TIdMessage.Create(nil);

  {-- Prepara qrys para o envio do email --}

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryDoctoFiscal.Open;

  qryConfigAmbiente.Close ;
  qryConfigAmbiente.Parameters.ParamByName('cNmComputador').Value := frmMenu.cNomeComputador;
  qryConfigAmbiente.Open ;

  frmMenu.mensagemUsuario('Carregando XML/PDF NFe...');

  // Armazena o caminho do arquivo PDF
  DanfePDF := qryConfigAmbientecPathArqNfe.Value + 'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\' + qryDoctoFiscalcChaveNFe.Value + '.pdf' ;

  // Armazena o caminho das pastas
  cPathXML := qryConfigAmbientecPathArqNfe.Value + 'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3);

  // Chave da NFe
  cChave := qryDoctoFiscalcChaveNFe.Value;

  // Valida o arquivo de cancelamento (Dentro do prazo e Fora do prazo)
  if (FileExists(cPathXML + '\NFeCan\' + cChave + '1101111-ProcEventoNFe.xml')) then
      DanfeXML := cPathXML + '\NFeCan\' + cChave + '1101111-ProcEventoNFe.xml'
  else
      if (FileExists(cPathXML + '\Envio e Resposta\' + cChave + '-sit.xml')) then
          DanfeXML := cPathXML + '\Envio e Resposta\' + cChave + '-sit.xml'
      else
      begin
          frmMenu.MensagemErro('Arquivo XML n�o encontrado.');

          Abort;
      end;

  //Prepara a mensagem

  frmMenu.mensagemUsuario('Preparando corpo do e-mail...');
  Assunto := 'Protocolo de Cancelamento da NFe n� ' + qryDoctoFiscaliNrDocto.AsString;

  if(frmMenu.LeParametro('VAREJO') = 'N') then
  begin
      Mensagem.Clear;
      Mensagem.Add(qryDoctoFiscalcNmTerceiro.AsString + #13#13);
      Mensagem.Add('Protocolo de Cancelamento da NF-e n� ' + qryDoctoFiscaliNrDocto.AsString + ' com a chave ' + qryDoctoFiscalcChaveNFe.Value +'.');
      Mensagem.Add(#13#13#13#13);
      Mensagem.Add(#13#13#13 + '<http://www.er2soft.com.br> '+ #13#13);
      Mensagem.Add('Conhe�a o Sistema de Gest�o Empresarial que integra sua empresa e seus parceiros de neg�cios com simplicidade.');
      Mensagem.Add(#13#13 +'O ER2SOFT ERP <http://www.er2soft.com.br > � um sistema f�cil de usar que atende totalmente as necessidades das empresas com baixo custo.');
      Mensagem.Add(#13#13 +'Ligue para a ER2Soft e tenha maiores informa��es (11) 2378-3200 ou acesse o site da ER2Soft.');
  end else
  begin
      Mensagem.Clear;
      Mensagem.Add(qryDoctoFiscalcNmTerceiro.AsString + #13#13);
      Mensagem.Add('Protocolo de Cancelamento da NF-e n� ' + qryDoctoFiscaliNrDocto.AsString + ' com a chave ' + qryDoctoFiscalcChaveNFe.Value +'.');
      Mensagem.Add(#13#13#13#13);
      Mensagem.Add(#13#13#13 + '<http://www.er2soft.com.br> '+ #13#13);
      Mensagem.Add('Conhe�a o Sistema de Automa��o Comercial que integra suas lojas e seus parceiros de neg�cios com simplicidade.');
      Mensagem.Add(#13#13 +'O ER2SOFT ERP <http://www.er2soft.com.br > � um sistema f�cil de usar que atende totalmente as necessidades dos lojistas com baixo custo.');
      Mensagem.Add(#13#13 +'Ligue para a ER2Soft e tenha maiores informa��es (11) 2378-3200 ou acesse o site da ER2Soft.');
  end;

  frmMenu.mensagemUsuario('Enviando e-mail...');

  {-- Pega as Configura��es de ambiente --}

  Servidor := qryConfigAmbientecServidorSMTP.Value;
  Porta    := qryConfigAmbientenPortaSMTP.Value;
  Usuario  := qryConfigAmbientecEmail.Value;

  Senha    := qryConfigAmbientecSenhaEmail.Value;

  {-- Passa as configura��es de conex�o para o INDY --}

  SMTP.Host               := Servidor;
  SMTP.Port               := Porta;
  SMTP.Username           := Usuario;
  SMTP.Password           := Senha;
  SMTP.AuthenticationType := atLogin;

  {-- prepara os dados da mensagem --}

  idMessage.From.Address              := Usuario;
  idMessage.Recipients.EMailAddresses := cPara;
  idMessage.CCList.EMailAddresses     := cCopia;
  idMessage.Subject                   := Assunto;
  idMessage.Body                      := Mensagem;

  {-- Anexa os arquivos � mensagem se esses existirem --}

  if FileExists(DanfeXML) then
       TIdAttachment.Create(idMessage.MessageParts, DanfeXML)
  else
  begin
      frmMenu.MensagemErro(DanfeXML + ' parace ser um caminho inv�lido, a mensagem n�o pode ser enviada.');
      abort;
  end;

  if FileExists(DanfePDF) then
      TIdAttachment.Create(idMessage.MessageParts, DanfePDF)
  else if (frmMenu.MessageDlg(DanfePDF + ' parace ser um caminho inv�lido, Deseja enviar apenas o XML. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
          abort;

  {-- envia o email --}

  try
      SMTP.Connect(2000);
      SMTP.Send(idMessage);
  except on E:Exception do
       begin
           frmMenu.MensagemErro('Erro: ' + E.Message) ;

           if SMTP.Connected then SMTP.Disconnect;
           frmMenu.mensagemUsuario('');
           raise;
      end;
  end;

  if SMTP.Connected then SMTP.Disconnect;

  frmMenu.mensagemUsuario('');

end;

procedure TfrmEmiteNFe2.gerarCCe(cChaveNFe, cCNPJCPFEmitente, xCorrecao : String;
                                   nSeqEvento, idLote, nCdCCe : Integer);
var
  cArquivoXML : String;
begin
  {-- prepara o ambiente para gera��o da CC-e --}
  preparaNFe() ;

  { -- se o dpec foi ativado encerra processo de envio -- }
  qryStatusDPEC.Close;
  qryStatusDPEC.Parameters.ParamByName('nPK').Value := frmMenu.nCdEmpresaAtiva;
  qryStatusDPEC.Open;

  if (qryStatusDPECnCdStatusDPEC.Value = 1) then
  begin
      frmMenu.MensagemErro('Webservice SEFAZ de origem est� inoperante no momento. Tente novamente mais tarde.');
      Abort;
  end;

  ACBrNFe1.EventoNFe.Evento.Clear;

  with (ACBrNFe1.EventoNFe.Evento.Add) do
  begin
      infEvento.chNFe               := cChaveNFe;
      infEvento.CNPJ                := cCNPJCPFEmitente;
      infEvento.dhEvento            := Now;
      infEvento.tpEvento            := teCCe;
      infEvento.nSeqEvento          := nSeqEvento;
      infEvento.detEvento.xCorrecao := xCorrecao;
  end;

  { -- envia CC-e -- }
  frmMenu.mensagemUsuario('Enviando Carta de Corre��o...');
  ACBrNFe1.EnviarEventoNFe(idLote);

  { -- se houve erro no envio cancela todo o processo -- }
  if not(ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat in [135, 136]) then
  begin
      Raise EDatabaseError.CreateFmt('Ocorreu o seguinte erro ao enviar a carta de corre��o:' + sLineBreak + 'C�digo:%d' + sLineBreak + 'Motivo: %s', [ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.cStat, ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.xMotivo]);
      Exit;
  end;

  { -- concatena nome do arquivo -- }
  cArquivoXML := ACBrNFe1.EventoNFe.Evento.Items[0].InfEvento.TipoEvento + ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.chNFe + '0' + IntToStr(nSeqEvento) + '-procEventoNFe.xml';

  { -- atualiza registro CC-e -- }
  qryCartaCorrecaoNFe.Close;
  qryCartaCorrecaoNFe.Parameters.ParamByName('nPK').Value := nCdCCe;
  qryCartaCorrecaoNFe.Open;
  qryCartaCorrecaoNFe.Edit;

  qryCartaCorrecaoNFedDtEmissao.Value      := ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.dhRegEvento;
  qryCartaCorrecaoNFecCaminhoXML.Value     := ACBrNFe1.Configuracoes.Arquivos.PathCCe;
  qryCartaCorrecaoNFecNrProtocoloCCe.Value := ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.nProt;
  qryCartaCorrecaoNFecArquivoXML.Value     := cArquivoXML;
  qryCartaCorrecaoNFecXMLCCe.Value         := ACBrNFe1.WebServices.EnvEvento.EventoRetorno.retEvento.Items[0].RetInfEvento.XML;
  qryCartaCorrecaoNFecFlgAmbienteHom.Value := cFlgAmbienteHom;
  qryCartaCorrecaoNFe.Post;
end;

procedure TfrmEmiteNFe2.imprimirCCe(nCdCartaCorrecaoNFe : Integer);
var
  objRel : TrptCartaCorrecaoNFe_View;
begin
  qryCartaCorrecaoNFe.Close;
  qryCartaCorrecaoNFe.Parameters.ParamByName('nPK').Value := nCdCartaCorrecaoNFe;
  qryCartaCorrecaoNFe.Open;

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := qryCartaCorrecaoNFenCdDoctoFiscal.Value;
  qryDoctoFiscal.Open;

  try
      objRel := TrptCartaCorrecaoNFe_View.Create(nil);

      { -- carrega xml -- }
      ACBrNFe1.EventoNFe.Evento.Clear;
      ACBrNFe1.EventoNFe.LerXMLFromString(qryCartaCorrecaoNFecXMLCCe.Value);// LerXML(qryCartaCorrecaoNFecCaminhoXML.Value + qryCartaCorrecaoNFecArquivoXML.Value);

      { -- xml em ambiente de homologa��o -- }
      if (ACBrNFe1.EventoNFe.Evento.Items[0].RetInfEvento.tpAmb = taHomologacao) then
      begin
          objRel.lblMsgCabecalho.Caption := 'N�o possui valor fiscal, simples representa��o do evento indicado abaixo' + #13 + 'Consulte a autenticidade no site da Sefaz Autorizada';
          objRel.lblAmbiente.Caption     := 'HOMOLOGA��O - SEM VALOR FISCAL';
      end
      { -- xml em ambiente de produ��o -- }
      else
      begin
          objRel.lblMsgCabecalho.Caption := 'Consulta de autenticidade no portal nacional da NF-e' + #13 + 'www.nfe.fazenda.gov.br/portal ou no site da Sefaz Autorizada';
          objRel.lblAmbiente.Caption     := 'PRODU��O';
      end;

      objRel.lblModelo.Caption        := qryDoctoFiscalcModelo.Value;
      objRel.lblSerie.Caption         := qryDoctoFiscalcSerie.Value;
      objRel.lblNumero.Caption        := qryDoctoFiscaliNrDocto.AsString;
      objRel.lblMesAnoEmissao.Caption := FormatDateTime('mm', qryCartaCorrecaoNFedDtEmissao.Value) + '/' + FormatDateTime('yyyy', qryCartaCorrecaoNFedDtEmissao.Value);
      objRel.lblChave.Caption         := qryCartaCorrecaoNFecChaveNFe.Value;
      objRel.lblOrgao.Caption         := IntToStr(ACBrNFe1.EventoNFe.Evento.Items[0].RetInfEvento.cOrgao);
      objRel.lblDtEvento.Caption      := DateTimeToStr(qryCartaCorrecaoNFedDtEmissao.Value);
      objRel.lblSeqEvento.Caption     := IntToStr(qryCartaCorrecaoNFeiSeq.Value);
      objRel.lblStatus.Caption        := IntToStr(ACBrNFe1.EventoNFe.Evento.Items[0].RetInfEvento.cStat) + ' - ' + ACBrNFe1.EventoNFe.Evento.Items[0].RetInfEvento.xMotivo;
      objRel.lblProtocolo.Caption     := qryCartaCorrecaoNFecNrProtocoloCCe.Value;
      objRel.lblDtRegistro.Caption    := DateTimeToStr(qryCartaCorrecaoNFedDtEmissao.Value);
      objRel.lblCorrecao.Caption      := qryCartaCorrecaoNFecCorrecao.Value;

      objRel.QuickRep1.PreviewModal;

  except
      frmMenu.MensagemErro('Erro no processamento.');
      Raise;
  end;
end;

procedure TfrmEmiteNFe2.gerarNFCe(nCdDoctoFiscal : Integer);
var
  iItem     : Integer;
  iDiasIBPT : Integer;
  cCdST     : String;
  ok        : Boolean;
begin
  { -- prepara o ambiente para gera��o da NFCe -- }
  preparaNFCe;

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryDoctoFiscal.Open;

  qryItemDoctoFiscal.Close;
  qryItemDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryItemDoctoFiscal.Open;

  frmMenu.mensagemUsuario('Gerando NFCe...');

  ACBrNFe1.NotasFiscais.Clear;

  with (ACBrNFe1.NotasFiscais.Add.NFe) do
  begin
      { -- identifica��o da NFCe -- }
      Ide.tpImp    := tiNFCe;
      Ide.finNFe   := fnNormal;
      Ide.indFinal := cfConsumidorFinal;
      Ide.indPres  := pcPresencial;
      Ide.natOp    := qryDoctoFiscalcTextoCFOP.Value;
      Ide.nNF      := qryDoctoFiscaliNrDocto.Value;
      Ide.cNF      := qryDoctoFiscaliNrDocto.Value;
      Ide.modelo   := 65;
      Ide.serie    := StrToInt(Trim(qryDoctoFiscalcSerie.Value));
      Ide.dEmi     := qryDoctoFiscaldDtEmissao.Value;
      Ide.tpNF     := tnSaida;
      Ide.tpEmis   := teNormal;
      Ide.verProc  := frmMenu.cNmVersaoSistema; //vers�o sistema (max 20car.)
      Ide.cUF      := qryConfigAmbientenCdUFEmissaoNFe.Value;
      Ide.cMunFG   := qryConfigAmbientenCdMunicipioEmissaoNFe.Value;

      if (qryDoctoFiscaldDtSaida.Value <> Null) then
      begin
          Ide.dSaiEnt := qryDoctoFiscaldDtSaida.Value;
          Ide.hSaiEnt := qryDoctoFiscaldDtSaida.Value;
      end
      else
      begin
          Ide.dSaiEnt := Now();
          Ide.hSaiEnt := Now();
      end;

      { -- tipo de ambiente de emiss�o da NFC-e -- }
      if (qryConfigAmbientenCdTipoAmbienteNFe.Value = 0) then
          Ide.tpAmb := taProducao
      else
          Ide.tpAmb := taHomologacao;

      { -- forma de pagamento -- }
      Raise Exception.Create('Verificar dados do pagamento.');
      { -- incluso erro acima pois linha abaixo deve ser corrigida                              -- }
      { -- utilizar procedure de gera��o de cupom fiscal e incluir registro na prazodoctofiscal -- }
      //Ide.indPag   := !;

      { -- consulta dados adicionais do emitente -- }
      qryTerceiroEmitente.Close;
      qryTerceiroEmitente.Parameters.ParamByName('nCdEmpresa').Value := qryDoctoFiscalnCdEmpresa.Value;
      qryTerceiroEmitente.Parameters.ParamByName('nCdLoja').Value    := qryDoctoFiscalnCdLoja.Value;
      qryTerceiroEmitente.Open;

      { -- dados do emitente -- }
      Emit.CNPJCPF           := qryDoctoFiscalcCNPJCPFEmitente.Value ;
      Emit.IE                := qryDoctoFiscalcIEEmitente.Value ;
      Emit.xNome             := qryDoctoFiscalcNmRazaoSocialEmitente.Value ;
      Emit.xFant             := qryDoctoFiscalcNmFantasiaEmitente.Value ;
      Emit.EnderEmit.fone    := qryDoctoFiscalcTelefoneEmitente.Value ;
      Emit.EnderEmit.CEP     := qryDoctoFiscalcCepEmitente.asInteger ;
      Emit.EnderEmit.xLgr    := qryDoctoFiscalcEnderecoEmitente.Value ;
      Emit.EnderEmit.nro     := qryDoctoFiscaliNumeroEmitente.asString ;
      Emit.EnderEmit.xCpl    := qryDoctoFiscalcComplEnderEmitente.Value ;
      Emit.EnderEmit.xBairro := qryDoctoFiscalcBairroEmitente.Value ;
      Emit.EnderEmit.cMun    := qryDoctoFiscalnCdMunicipioEmitenteIBGE.Value ;
      Emit.EnderEmit.xMun    := qryDoctoFiscalcCidadeEmitente.Value ;
      Emit.EnderEmit.UF      := qryDoctoFiscalcUFEmitente.Value ;
      Emit.enderEmit.cPais   := qryDoctoFiscalnCdPaisEmitente.Value ;
      Emit.enderEmit.xPais   := qryDoctoFiscalcNmPaisEmitente.Value ;

      { -- seleciona regime tribut�rio do emitente -- }
      case (qryTerceiroEmitentenCdTabTipoEnquadTributario.Value) of
          1 : Emit.CRT := crtSimplesNacional;       //simples nacional
          2 : Emit.CRT := crtSimplesExcessoReceita; //simples nacional excesso de sublimite da receita bruta
          3 : Emit.CRT := crtRegimeNormal;          //regime normal
      end;

      { -- consulta dados adicionais do destinat�rio -- }
      qryTerceiro.Close;
      qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := qryDoctoFiscalnCdTerceiro.Value;
      qryTerceiro.Open;

      { -- dados do destinat�rio -- }
      Dest.CNPJCPF           := qryDoctoFiscalcCNPJCPF.Value;
      Dest.xNome             := qryDoctoFiscalcNmTerceiro.Value;
      Dest.IE                := '';
      Dest.indIEDest         := inNaoContribuinte;
      Dest.EnderDest.xLgr    := qryDoctoFiscalcEndereco.Value ;
      Dest.EnderDest.xCpl    := qryDoctoFiscalcComplEnderDestino.Value;
      Dest.EnderDest.xBairro := qryDoctoFiscalcBairro.Value ;
      Dest.EnderDest.cMun    := qryDoctoFiscalnCdMunicipioDestinoIBGE.Value;
      Dest.EnderDest.xMun    := qryDoctoFiscalcCidade.Value;
      Dest.EnderDest.UF      := qryDoctoFiscalcUF.Value ;
      Dest.EnderDest.Fone    := qryDoctoFiscalcTelefone.Value;
      Dest.EnderDest.cPais   := qryDoctoFiscalnCdPaisDestino.Value ;
      Dest.EnderDest.xPais   := qryDoctoFiscalcNmPaisDestino.Value ;
      Dest.ISUF              := '';

      if (qryDoctoFiscalcCNPJCPF.Value <> '') then
          Ide.indFinal := cfConsumidorFinal;

      if (qryDoctoFiscalcCEP.Value <> '') then
          Dest.EnderDest.CEP := qryDoctoFiscalcCEP.AsInteger;

      if (qryDoctoFiscaliNumero.Value <> 0) then
          Dest.EnderDest.nro := qryDoctoFiscaliNumero.AsString;

      if (qryDoctoFiscalnCdPaisDestino.Value = 0) then
          Dest.EnderDest.cPais := 1058;

      { -- Use os campos abaixo para informar o endere�o de retirada quando for diferente do Remetente/Destinat�rio -- }
      {Retirada.CNPJCPF := '';
      Retirada.xLgr    := '';
      Retirada.nro     := '';
      Retirada.xCpl    := '';
      Retirada.xBairro := '';
      Retirada.cMun    := 0;
      Retirada.xMun    := '';
      Retirada.UF      := '';}

      { -- Use os campos abaixo para informar o endere�o de entrega quando for diferente do Remetente/Destinat�rio -- }
      {Entrega.CNPJCPF := '';
      Entrega.xLgr    := '';
      Entrega.nro     := '';
      Entrega.xCpl    := '';
      Entrega.xBairro := '';
      Entrega.cMun    := 0;
      Entrega.xMun    := '';
      Entrega.UF      := '';}

      { -- dados dos itens -- }
      qryItemDoctoFiscal.First;
      Inc(iItem);

      while (not qryItemDoctoFiscal.Eof) do
      begin
          with (Det.Add) do
          begin
              { -- 1 : c�digo pr�prio    -- }
              { -- 2 : c�digo fabricante -- }
              { -- 3 : c�digo barra      -- }
              { -- 4 : n�o informar      -- }

              { -- c�digo pr�prio ou n�o informar c�digo -- }
              if ((qryItemDoctoFiscalnCdTabTipoCodProdFat.Value <= 1) or (qryItemDoctoFiscalnCdTabTipoCodProdFat.Value = 4)) then
                  Prod.cProd := qryItemDoctoFiscalnCdProduto.Value;

              { -- c�digo do fabricante -- }
              if (qryItemDoctoFiscalnCdTabTipoCodProdFat.Value = 2) then
              begin
                  if (Length(Trim(qryItemDoctoFiscalcCdFabricante.Value)) <= 0) then
                  begin
                      Raise Exception.Create('C�digo do fabricante n�o informado para o produto: ' + qryItemDoctoFiscalnCdProduto.asString + ' - ' + qryItemDoctoFiscalcNmItem.Value);
                  end;

                  Prod.cProd := qryItemDoctoFiscalcCdFabricante.Value;
              end;

              { -- c�digo EAN -- }
              if (qryItemDoctoFiscalnCdTabTipoCodProdFat.Value = 3) then
              begin
                  if (Length(Trim(qryItemDoctoFiscalcEAN.Value)) <= 0) then
                  begin
                      Raise Exception.Create('C�digo EAN/UPC n�o informado para o produto: ' + qryItemDoctoFiscalnCdProduto.asString + ' - ' + qryItemDoctoFiscalcNmItem.Value);
                  end;

                  Prod.cProd := qryItemDoctoFiscalcEAN.Value;
              end;

              { -- se produto possuir EAN e deve informar c�digo -- }
              if ((Trim(qryItemDoctoFiscalcEAN.Value ) <> '') and (qryItemDoctoFiscalnCdTabTipoCodProdFat.Value <> 4)) then
              begin
                  Prod.cEAN     := qryItemDoctoFiscalcEAN.Value;
                  Prod.cEANTrib := qryItemDoctoFiscalcEAN.Value;
              end;

              Prod.nItem   := iItem;
              Prod.xProd   := qryItemDoctoFiscalcNmItem.Value;
              Prod.NCM     := qryItemDoctoFiscalcNCM.Value;
              Prod.CFOP    := qryItemDoctoFiscalcCFOP.Value;
              Prod.EXTIPI  := '';
              Prod.uCom    := qryItemDoctoFiscalcUnidadeMedida.Value;
              Prod.uTrib   := qryItemDoctoFiscalcUnidadeMedidaTribuvel.Value;
              Prod.qCom    := qryItemDoctoFiscalnQtde.Value;
              Prod.qTrib   := qryItemDoctoFiscalnQtde.Value;
              Prod.vProd   := qryItemDoctoFiscalnValTotal.Value + (qryItemDoctoFiscalnQtde.Value * qryItemDoctoFiscalnValDesconto.Value);
              Prod.vUnTrib := qryItemDoctoFiscalnValUnitario.Value + qryItemDoctoFiscalnValDesconto.Value;
              Prod.vUnCom  := qryItemDoctoFiscalnValUnitario.Value + qryItemDoctoFiscalnValDesconto.Value;
              Prod.vDesc   := qryItemDoctoFiscalnQtde.Value * qryItemDoctoFiscalnValDesconto.Value;
              Prod.vFrete  := 0; //NFC-e n�o possui frete
              Prod.vOutro  := 0;
              Prod.vSeg    := 0;

              with (Imposto) do
              begin
                  { -- inf. valor do imposto aproximado no item (federal + estadual + municipal) -- }
                  vTotTrib := qryItemDoctoFiscalnValImpostoAproxFed.Value + qryItemDoctoFiscalnValImpostoAproxEst.Value + qryItemDoctoFiscalnValImpostoAproxMun.Value;

                  { -- icms -- }
                  with (ICMS) do
                  begin
                      cCdST := Trim(qryItemDoctoFiscalcCdST.Value);

                      case StrToInt(cCdST) of
                          0   : CST   := cst00;
                          10  : CST   := cst10;
                          20  : CST   := cst20;
                          30  : CST   := cst30;
                          40  : CST   := cst40;
                          41  : CST   := cst41;
                          45  : CST   := cst45;
                          50  : CST   := cst50;
                          51  : CST   := cst51;
                          60  : CST   := cst60;
                          70  : CST   := cst70;
                          80  : CST   := cst80;
                          81  : CST   := cst81;
                          90  : CST   := cst90;
                          101 : CSOSN := csosn101;
                          102 : CSOSN := csosn102;
                          103 : CSOSN := csosn103;
                          201 : CSOSN := csosn201;
                          202 : CSOSN := csosn202;
                          203 : CSOSN := csosn203;
                          300 : CSOSN := csosn300;
                          400 : CSOSN := csosn400;
                          500 : CSOSN := csosn500;
                          900 : CSOSN := csosn900;
                      end;

                      if (StrToInt(cCdST) > 90) then
                          ICMS.CSOSN := CSOSN
                      else
                          ICMS.CST   := CST;

                      ICMS.modBC := dbiValorOperacao;
                      ICMS.pICMS := qryItemDoctoFiscalnAliqICMS.Value;
                      ICMS.vICMS := qryItemDoctoFiscalnValICMS.Value;
                      ICMS.vBC   := qryItemDoctoFiscalnValBaseICMS.Value;
                      ICMS.orig  := TpcnOrigemMercadoria(qryItemDoctoFiscalnCdTabTipoOrigemMercadoria.Value);

                      if (qryItemDoctoFiscalnPercBCICMS.Value > 0) and (qryItemDoctoFiscalnPercBCICMS.Value < 100) then
                          ICMS.pRedBC := qryItemDoctoFiscalnPercBCICMS.Value;

                      if (qryItemDoctoFiscalnValICMSSub.Value > 0) then
                      begin
                          ICMS.modBC   := dbiMargemValorAgregado;
                          ICMS.modBCST := dbisMargemValorAgregado;
                          ICMS.pMVAST  := qryItemDoctoFiscalnPercIVA.Value;

                          if (qryItemDoctoFiscalnPercBCICMSST.Value > 0) and (qryItemDoctoFiscalnPercBCICMSST.Value < 100) then
                              ICMS.pRedBCST := (100 - qryItemDoctoFiscalnPercBCICMSST.Value);

                          ICMS.vBCST    := qryItemDoctoFiscalnValBaseICMSSub.Value;
                          ICMS.vICMSST  := qryItemDoctoFiscalnValICMSSub.Value;
                          ICMS.pICMSST  := qryItemDoctoFiscalnAliqICMSInternaOrigem.Value;
                      end;
                  end;

                  { -- ipi -- }
                  if (qryItemDoctoFiscalnValIPI.Value > 0) then
                  begin
                      IPI.CST     := StrToCSTIPI(ok, qryItemDoctoFiscalcCdSTIPI.Value);
                      Prod.EXTIPI := qryItemDoctoFiscalcEXTIPI.Value;

                      { -- s� informar quando o c�lculo do IPI for baseado em al�quota, n�o em quantidade -- }
                      IPI.vBC  := qryItemDoctoFiscalnValBaseIPI.Value;
                      IPI.pIPI := qryItemDoctoFiscalnAliqIPI.Value;
                      IPI.vIPI := qryItemDoctoFiscalnValIPI.Value;
                  end;

                  { -- pis -- }
                  PIS.CST  := StrToCSTPIS(ok, qryItemDoctoFiscalcCSTPIS.Value);
                  PIS.vBC  := qryItemDoctoFiscalnValBasePIS.Value;
                  PIS.pPIS := qryItemDoctoFiscalnAliqPIS.Value;
                  PIS.vPIS := qryItemDoctoFiscalnValPIS.Value;

                  if (   (qryItemDoctoFiscalcCSTPIS.Value = '03')
                      or (qryItemDoctoFiscalcCSTPIS.Value = '99')) then
                  begin
                      PIS.qBCProd   := qryItemDoctoFiscalnQtde.Value;
                      PIS.vAliqProd := qryItemDoctoFiscalnAliqPIS.Value;
                  end
                  else
                      PIS.pPIS := qryItemDoctoFiscalnAliqPIS.Value;

                  { -- cofins -- }
                  COFINS.CST     := StrToCSTCOFINS(ok, qryItemDoctoFiscalcCSTCOFINS.Value);
                  COFINS.vBC     := qryItemDoctoFiscalnValBaseCOFINS.Value;
                  COFINS.vCOFINS := qryItemDoctoFiscalnValCOFINS.Value;

                  if (   (qryItemDoctoFiscalcCSTCOFINS.Value = '03')
                      or (qryItemDoctoFiscalcCSTCOFINS.Value = '99')) then
                  begin
                      COFINS.qBCProd   := qryItemDoctoFiscalnQtde.Value;
                      COFINS.vAliqProd := qryItemDoctoFiscalnAliqCOFINS.Value;
                  end
                  else
                      COFINS.pCOFINS := qryItemDoctoFiscalnAliqCOFINS.Value;
              end;
          end;

          qryItemDoctoFiscal.Next;
          Inc(iItem);
      end;

      qryItemDoctoFiscal.Close;

      { -- dados do frete -- }
      Transp.modFrete := mfSemFrete; //NFC-e n�o possui frete

      { -- dados de totaliza��o -- }
      qryDescontoTotal.Close;
      qryDescontoTotal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
      qryDescontoTotal.Open;

      Total.ICMSTot.vBC      := qryDoctoFiscalnValBaseICMS.Value;
      Total.ICMSTot.vICMS    := qryDoctoFiscalnValICMS.Value;
      Total.ICMSTot.vBCST    := qryDoctoFiscalnValBaseICMSSub.Value;
      Total.ICMSTot.vST      := qryDoctoFiscalnValICMSSub.Value;
      Total.ICMSTot.vIPI     := qryDoctoFiscalnValIPI.Value;
      Total.ICMSTot.vPIS     := qryDoctoFiscalnValPIS.Value ;
      Total.ICMSTot.vCOFINS  := qryDoctoFiscalnValCOFINS.Value ;
      Total.ICMSTot.vProd    := qryDoctoFiscalnValProduto.Value + qryDescontoTotalnValDescontoItens.Value;
      Total.ICMSTot.vDesc    := qryDescontoTotalnValDescontoItens.Value;
      Total.ICMSTot.vFrete   := 0; //NFC-e n�o possui frete
      Total.ICMSTot.vSeg     := 0;
      Total.ICMSTot.vOutro   := 0;
      Total.ICMSTot.vNF      := qryDoctoFiscalnValTotal.Value;
      Total.ICMSTot.vTotTrib := qryDoctoFiscalnValTotalImpostoAproxFed.Value + qryDoctoFiscalnValTotalImpostoAproxEst.Value + qryDoctoFiscalnValTotalImpostoAproxMun.Value;

      { -- dados de pagamento -- }
      with (pag.Add) do
      begin
          Raise Exception.Create('Verificar dados do pagamento.');
          { -- incluso erro acima pois linha abaixo deve ser corrigida                              -- }
          { -- utilizar procedure de gera��o de cupom fiscal e incluir registro na prazodoctofiscal -- }
          //tPag := !;
          //vPag := !;
      end;

      { -- dados da transpar�ncia fiscal --}
      qryTabIBPT.Close;
      qryTabIBPT.Parameters.ParamByName('nCdEstadoEmitente').Value := qryTerceiroEmitentenCdEstado.Value;
      qryTabIBPT.Open;

      iDiasIBPT := StrToIntDef(frmMenu.LeParametro('DIASVENCTABIBPT'),0);

      if ((qryTerceiroEmitentenCdEstado.Value > 0) and (not qryTabIBPT.IsEmpty) and ((qryDoctoFiscalnValTotalImpostoAproxFed.Value > 0) or (qryDoctoFiscalnValTotalImpostoAproxEst.Value > 0) or (qryDoctoFiscalnValTotalImpostoAproxMun.Value > 0))) then
      begin
          if ((qryTabIBPTiDiasValidade.Value <= iDiasIBPT) and (iDiasIBPT > 0)) then
              frmMenu.MensagemAlerta('Faltam ' + qryTabIBPTiDiasValidade.AsString + ' dias para o vencimento das al�quotas para atendimento da lei 12.741 da Transpar�ncia Fiscal.'
                                    +#13#13
                                    +'Caso n�o seja atualizado dentro do prazo, o c�lculo deixar� de aparecer no nota fiscal ao consumidor.');

          if (InfAdic.infCpl <> '') then
              infAdic.infCpl := infAdic.infCpl + ';';

          InfAdic.infCpl := InfAdic.infCpl + 'Valor Aprox. Tributos: ';

          if (qryDoctoFiscalnValTotalImpostoAproxFed.Value > 0) then
              InfAdic.infCpl := InfAdic.infCpl + 'Federal R$ ' + qryDoctoFiscalnValTotalImpostoAproxFed.AsString;

          if (qryDoctoFiscalnValTotalImpostoAproxEst.Value > 0) then
              InfAdic.infCpl := InfAdic.infCpl + ' Estadual R$ ' + qryDoctoFiscalnValTotalImpostoAproxEst.AsString;

          if (qryDoctoFiscalnValTotalImpostoAproxMun.Value > 0) then
              InfAdic.infCpl := InfAdic.infCpl + ' Municipal R$ ' + qryDoctoFiscalnValTotalImpostoAproxMun.AsString;

          InfAdic.infCpl := InfAdic.infCpl + ' Fonte: IBPT/FECOMERCIO (' + qryTabIBPTcChave.Value + ').';
      end;
  end;

  frmMenu.mensagemUsuario('Transmitindo NFCe...');

  { -- validar processo abaixo ap�s libera��o do projeto piloto da SEFAZ -- }
  { -- remover linha de erro abaixo ap�s libera��o                       -- }
  Raise Exception.Create('Processo NFC-e em desenvolvimento.');

  try
      if (ACBrNFe1.Enviar(qryDoctoFiscaliNrDocto.Value,True,True)) then
      begin
          qryDoctoFiscal.Edit;
          qryDoctoFiscalcNrReciboNFe.Value                 := ACBrNFe1.WebServices.Retorno.NFeRetorno.nRec;
          qryDoctoFiscalnCdTabTipoEmissaoDoctoFiscal.Value := ACBrNFe1.Configuracoes.Geral.FormaEmissaoCodigo;
          qryDoctoFiscalcCaminhoXML.Value                  := ACBrNFe1.Configuracoes.Geral.PathSalvar;
          qryDoctoFiscal.Post;
      end;

      ACBrNFe1.NotasFiscais.SaveToFile();

      if (ACBrNFe1.WebServices.Retorno.CStat = 105) then
      begin
          frmMenu.MensagemAlerta('Lote em processamento! Aguarde alguns instantes e tente imprimir novamente este documento.');
          Exit;
      end;

      { -- lote processado OK -- }
      if    ( ACBrNFe1.WebServices.Retorno.cStat = 100 ) //100 : autorizado o uso da nf-e
         or ( ACBrNFe1.WebServices.Retorno.cStat = 104 ) //104 : lote processado
         or ( ACBrNFe1.WebServices.Retorno.cStat = 302 ) //302 : uso denegado (irregularidade fiscal do destinat�rio)
      then
      begin
          qryDoctoFiscal.Edit;
          //qryDoctoFiscalcXMLNFe.Value          := StringReplace(ACBrNFe1.NotasFiscais.Items[0].XML,'''',''' + CHAR(39) + ''',[rfReplaceAll]);
          qryDoctoFiscalcNrProtocoloNFe.Value  := ACBrNFe1.WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].nProt;
          qryDoctoFiscalcChaveNFe.Value        := ACBrNFe1.WebServices.Retorno.ChaveNFe;
          qryDoctoFiscalcArquivoXML.Value      := ACBrNFe1.WebServices.Retorno.ChaveNFe + '-nfce.xml';
          qryDoctoFiscaliStatusRetorno.Value   := ACBrNFe1.WebServices.Retorno.cStat;
          qryDoctoFiscalcNmStatusRetorno.Value := ACBrNFe1.WebServices.Retorno.xMotivo;
          qryDoctoFiscal.Post;
          
          {-- Corre��o pelo tamanho do XML --}
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('UPDATE DoctoFiscal');
          qryAux.SQL.Add('   SET cXMLNFe        = ' + #39 + StringReplace(ACBrNFe1.NotasFiscais.Items[0].XML,':',''' + CHAR(50) + ''',[rfReplaceAll]) + #39);
          qryAux.SQL.Add(' WHERE nCdDoctoFiscal = ' + qryDoctoFiscalnCdDoctoFiscal.AsString);
          qryAux.ExecSQL;

      end;

      ACBrNFeDANFERave1.PathPDF := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\';
      ACBrNFe1.NotasFiscais.ImprimirPDF;

      ACBrNFe1.NotasFiscais.Clear;
  except
      Raise;
  end;

  frmMenu.mensagemUsuario('');

  qryDoctoFiscal.Close;
end;
procedure TfrmEmiteNFe2.cancelarNFCe(nCdDoctoFiscal : Integer);
begin
  //
end;

end.

