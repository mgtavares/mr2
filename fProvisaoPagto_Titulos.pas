unit fProvisaoPagto_Titulos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxCurrencyEdit, GridsEh, DBGridEh, DB, ADODB,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxDBData, cxCheckBox, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  Menus;

type
  TfrmProvisaoPagto_Titulos = class(TfrmProcesso_Padrao)
    Panel1: TPanel;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    qryTemp: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    dsTemp: TDataSource;
    qryPreparaTemp: TADOQuery;
    Panel4: TPanel;
    edtTotalSelec: TcxCurrencyEdit;
    Panel3: TPanel;
    Panel5: TPanel;
    cxButton4: TcxButton;
    Label1: TLabel;
    Label2: TLabel;
    qryProvisaoTit: TADOQuery;
    qryProvisaoTitnCdProvisaoTit: TAutoIncField;
    qryProvisaoTitnCdContaBancaria: TIntegerField;
    qryProvisaoTitnCdBanco: TIntegerField;
    qryProvisaoTitcAgencia: TIntegerField;
    qryProvisaoTitnCdConta: TStringField;
    qryProvisaoTitdDtPagto: TDateTimeField;
    qryProvisaoTitnCdFormaPagto: TIntegerField;
    qryProvisaoTitcNmFormaPagto: TStringField;
    qryProvisaoTitdDtPrev: TDateTimeField;
    dsProvisaoTit: TDataSource;
    qryProvisaoTitnValProvisao: TBCDField;
    qryProvisaoTitcNmTitular: TStringField;
    cxButton3: TcxButton;
    cxButton5: TcxButton;
    qryAux: TADOQuery;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgOK: TcxGridDBColumn;
    cxGrid1DBTableView1nCdSP: TcxGridDBColumn;
    cxGrid1DBTableView1nCdRecebimento: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLoja: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPagto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1cNmFormaPagto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCategFinanc: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdProvisaoTit: TcxGridDBColumn;
    cxGridDBTableView1dDtPrev: TcxGridDBColumn;
    cxGridDBTableView1nCdContaBancaria: TcxGridDBColumn;
    cxGridDBTableView1nCdBanco: TcxGridDBColumn;
    cxGridDBTableView1cAgencia: TcxGridDBColumn;
    cxGridDBTableView1nCdConta: TcxGridDBColumn;
    cxGridDBTableView1dDtPagto: TcxGridDBColumn;
    cxGridDBTableView1nCdFormaPagto: TcxGridDBColumn;
    cxGridDBTableView1cNmFormaPagto: TcxGridDBColumn;
    cxGridDBTableView1nValProvisao: TcxGridDBColumn;
    cxGridDBTableView1cNmTitular: TcxGridDBColumn;
    qryConfereBaixaProvisao: TADOQuery;
    qryConfereBaixaProvisaocFlgBaixado: TIntegerField;
    qryTempnCdTitulo: TIntegerField;
    qryTempcFlgOK: TIntegerField;
    qryTempnCdSP: TIntegerField;
    qryTempnCdRecebimento: TIntegerField;
    qryTempdDtVenc: TDateTimeField;
    qryTempnCdTerceiro: TIntegerField;
    qryTempcNmTerceiro: TStringField;
    qryTempnSaldoTit: TBCDField;
    qryTempcNmLoja: TStringField;
    qryTempdDtPagto: TDateTimeField;
    qryTempcNmEspTit: TStringField;
    qryTempcNrTit: TStringField;
    qryTempcNrNF: TStringField;
    qryTempiParcela: TIntegerField;
    qryTempcNmFormaPagto: TStringField;
    qryTempcNmCategFinanc: TStringField;
    qryTempcFlgTituloAdiant: TIntegerField;
    PopupMenu1: TPopupMenu;
    ExibirAdiantamentos1: TMenuItem;
    cxTextEdit1: TcxTextEdit;
    Label20: TLabel;
    cxGrid1DBTableView1cFlgTituloAdiant: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    Atualizar1: TMenuItem;
    procedure qryTempcFlgOKChange(Sender: TField);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure cxGrid1DBTableView1cFlgOKPropertiesEditValueChanged(
      Sender: TObject);
    procedure ExibirAdiantamentos1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure cxGrid1DBTableView1cNmTerceiroStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure Atualizar1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    dDtPagto : TDateTime ;
  end;

var
  frmProvisaoPagto_Titulos: TfrmProvisaoPagto_Titulos;

implementation

uses fTitulo, fMenu, fProvisaoPagto_ContaBancaria,
  fProvisaoPagto_DetalheProvisao, fProvisaoPagto_TitulosAdiantamento;

{$R *.dfm}

procedure TfrmProvisaoPagto_Titulos.qryTempcFlgOKChange(Sender: TField);
begin
  inherited;

{  if (qryTempcFlgOK.Value = 1) then
      edtTotalSelec.Value := edtTotalSelec.Value + qryTempnSaldoTit.Value
  else edtTotalSelec.Value := edtTotalSelec.Value - qryTempnSaldoTit.Value ;}

end;

procedure TfrmProvisaoPagto_Titulos.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  {if not qryTemp.eof then
  begin

      frmTitulo.PosicionaQuery(frmTitulo.qryMaster,qryTempnCdTitulo.asString) ;

      frmTitulo.btIncluir.Visible   := False ;
      frmTitulo.btSalvar.Visible    := False ;
      frmTitulo.btConsultar.Visible := False ;
      frmTitulo.btExcluir.Visible   := False ;
      frmTitulo.btCancelar.Visible  := False ;

      frmTitulo.ShowModal;

      frmTitulo.btIncluir.Visible   := True ;
      frmTitulo.btSalvar.Visible    := True ;
      frmTitulo.btConsultar.Visible := True ;
      frmTitulo.btExcluir.Visible   := True ;
      frmTitulo.btCancelar.Visible  := True ;

  end ;}

end;

procedure TfrmProvisaoPagto_Titulos.FormShow(Sender: TObject);
begin
  inherited;


  Label1.Font.Name  := 'Tahoma' ;
  Label1.Font.Size  := 12 ;
  Label1.Font.Color := clBlue ;

  Label2.Font.Name  := 'Tahoma' ;
  Label2.Font.Size  := 12 ;
  Label2.Font.Color := clBlue ;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
  begin
      cxGrid1DBTableView1cNmLoja.Visible := False ;
  end ;

  qryProvisaoTit.Close ;

end;

procedure TfrmProvisaoPagto_Titulos.cxButton4Click(Sender: TObject);
begin
  inherited;

  qryProvisaoTit.Close ;
  qryProvisaoTit.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryProvisaoTit.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryProvisaoTit.Open  ;

end;

procedure TfrmProvisaoPagto_Titulos.cxButton1Click(Sender: TObject);
var
  Mensagem : String;
  objForm  : TfrmProvisaoPagto_ContaBancaria ;

begin

    Application.ProcessMessages;

    if (edtTotalSelec.Value <= 0) then
    begin
      MensagemAlerta('Nenhum t�tulo selecionado.') ;
      exit ;
    end ;

    qryTemp.First;
    while not qryTemp.Eof do
    begin
        if ((qryTempcFlgOK.Value = 1) and (qryTempcFlgTituloAdiant.Value = 1)) then
        begin
            Mensagem := 'O Terceiro ' + qryTempcNmTerceiro.Value + ' (T�tulo: ' + qryTempnCdTitulo.AsString + ') possui adiantamento em aberto. Deseja Continuar ?';
            case MessageDlg(Mensagem,mtConfirmation,[mbYes,mbNo],0) of
                mrNo: begin
                    exit;
                end ;
            end ;
        end;
        qryTemp.Next;
    end;

    objForm := TfrmProvisaoPagto_ContaBancaria.Create(Self) ;

    objForm.cmdPreparaTemp.Execute;

    objForm.qryFormaPagto.Close ;
    objForm.qryFormaPagto.Open ;

    objForm.qryContaBancaria.Close ;
    objForm.qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
    objForm.qryContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
    objForm.qryContaBancaria.Open ;

    objForm.nValProvisao := edtTotalSelec.Value ;
    objForm.dDtPagto     := dDtPagto ;

    showForm( objForm , TRUE ) ;

    qryTemp.Close ;
    cmdPreparaTemp.Execute;
    qryTemp.Open  ;

    edtTotalSelec.Value := 0 ;

end;

procedure TfrmProvisaoPagto_Titulos.cxButton3Click(Sender: TObject);
var
    objForm : TfrmProvisaoPagto_DetalheProvisao ;
begin

    if (qryProvisaoTit.eof) then
    begin
        MensagemAlerta('Selecione uma provis�o.') ;
        exit ;
    end ;

    objForm := TfrmProvisaoPagto_DetalheProvisao.Create(nil) ;

    objForm.qryTitulos.Close ;
    PosicionaQuery(objForm.qryTitulos, qryProvisaoTitnCdProvisaoTit.AsString) ;

    showForm( objForm , TRUE ) ;
end;

procedure TfrmProvisaoPagto_Titulos.cxButton5Click(Sender: TObject);
begin

    if (qryProvisaoTit.eof) then
    begin
        MensagemAlerta('Selecione uma provis�o.') ;
        exit ;
    end ;

    case MessageDlg('Esta a��o ir� reabrir todos os t�tulos previstos para pagamento nesta provis�o. Continuar ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
    end ;

    qryConfereBaixaProvisao.Close;
    PosicionaQuery(qryConfereBaixaProvisao, qryProvisaoTitnCdProvisaoTit.asString) ;

    if not qryConfereBaixaProvisao.eof then
        if (qryConfereBaixaProvisaocFlgBaixado.Value = 1) then
        begin
            MensagemErro('Esta provis�o de pagamento j� foi baixada, por favor verifique. C�digo da Provis�o: ' + qryProvisaoTitnCdProvisaoTit.asString) ;
            qryProvisaoTit.Requery();
            abort ;
        end ;

    frmMenu.Connection.BeginTrans;

    try
        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('UPDATE Titulo SET nCdProvisaoTit = NULL WHERE nCdProvisaoTit = ' + qryProvisaoTitnCdProvisaoTit.AsString) ;
        qryAux.ExecSQL;

        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('UPDATE ProvisaoTit SET dDtCancel = GetDate(), nCdUsuarioCancel = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ' WHERE nCdProvisaoTit = ' + qryProvisaoTitnCdProvisaoTit.AsString) ;
        qryAux.ExecSQL;
    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento.');
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    ShowMessage('Provis�o cancelada com sucesso.') ;

    qryProvisaoTit.Close ;
    qryProvisaoTit.Open ;

    cmdPreparaTemp.Execute;
    qryTemp.Close ;
    qryTemp.Open ;

end;

procedure TfrmProvisaoPagto_Titulos.cxButton2Click(Sender: TObject);
var
    cMsg : string ;
begin
  inherited;

  Application.ProcessMessages;

  if (edtTotalSelec.Value <= 0) then
  begin
      MensagemAlerta('Nenhum t�tulo selecionado.') ;
      exit ;
  end ;

  if qryProvisaoTit.eof or (qryProvisaoTitnCdProvisaoTit.Value = 0) then
  begin
      MensagemAlerta('Selecione a provis�o que deseja agregar os t�tulos selecionados.') ;
      exit ;
  end ;

  cMsg := 'Confirma a provis�o dos t�tulos selecionados ?' ;
  cMsg := cMsg + #13#13 + 'N�mero da Provis�o: ' + qryProvisaoTitnCdProvisaoTit.asString + '  Data de Pagamento: ' + qryProvisaoTitdDtPagto.asString + '   Forma: ' + qryProvisaoTitcNmFormaPagto.Value ;
  cMsg := cMsg + #13#13 + 'Conta D�bito: ' + Trim(qryProvisaoTitnCdBanco.AsString) + ' - ' + Trim(qryProvisaoTitcAgencia.AsString) + ' - ' + Trim(qryProvisaoTitnCdConta.AsString) + ' - ' + Trim(qryProvisaoTitcNmTitular.Value) ;

  case MessageDlg(cMsg,mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      qryTemp.DisableControls ;
      qryTemp.First ;

      while not qryTemp.Eof do
      begin

          if (qryTempcFlgOK.Value = 1) then
          begin

              qryAux.Close ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('UPDATE Titulo SET nCdProvisaoTit = ' + qryProvisaoTitnCdProvisaoTit.AsString + ' WHERE nCdTitulo = ' + qryTempnCdTitulo.asString) ;
              qryAux.ExecSQL ;
              
          end ;

          qryTemp.Next ;

      end ;

      qryTemp.First ;

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE ProvisaoTit SET nValProvisao = nValProvisao + ' + StringReplace(edtTotalSelec.EditValue,',','.',[rfReplaceAll, rfIgnoreCase]) + ' WHERE nCdProvisaoTit = ' + qryProvisaoTitnCdProvisaoTit.AsString) ;
      qryAux.ExecSQL;

  except
      qryTemp.EnableControls ;
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  qryTemp.EnableControls ;
  frmMenu.Connection.CommitTrans;

  ShowMessage('Provis�o atualizada com sucesso.') ;

  qryTemp.Close ;
  cmdPreparaTemp.Execute;
  qryTemp.Open  ;

  edtTotalSelec.Value := 0 ;

end;

procedure TfrmProvisaoPagto_Titulos.qryTempAfterPost(DataSet: TDataSet);
begin
  inherited;

  if (qryTempcFlgOK.Value = 1) then
      edtTotalSelec.Value := edtTotalSelec.Value + qryTempnSaldoTit.Value
  else edtTotalSelec.Value := edtTotalSelec.Value - qryTempnSaldoTit.Value ;

end;

procedure TfrmProvisaoPagto_Titulos.cxGrid1DBTableView1cFlgOKPropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;

  if (qryTemp.State = dsEdit) then
      qryTemp.Post;

end;

procedure TfrmProvisaoPagto_Titulos.ExibirAdiantamentos1Click(
  Sender: TObject);
var
   nPk : integer;
   objForm : TfrmProvisaoPagtoTituloAdiantamento ;
begin
  inherited;

  objForm := TfrmProvisaoPagtoTituloAdiantamento.Create(nil) ;

  PosicionaQuery(objForm.qryTitulosAdiantamento,qryTempnCdTerceiro.AsString);

  showForm( objForm , TRUE ) ;
  
end;

procedure TfrmProvisaoPagto_Titulos.PopupMenu1Popup(Sender: TObject);
begin
  inherited;
  ExibirAdiantamentos1.Enabled :=  (qryTempcFlgTituloAdiant.Value = 1) ;
end;

procedure TfrmProvisaoPagto_Titulos.cxGrid1DBTableView1cNmTerceiroStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;
    if (ARecord.Values[16] = 1) then
  begin
     AStyle := frmMenu.LinhaVermelha ;
  end;

end;

procedure TfrmProvisaoPagto_Titulos.Atualizar1Click(Sender: TObject);
begin
  inherited;

    cmdPreparaTemp.Execute;
    qryTemp.Close ;
    qryTemp.Open  ;

    if not (qryProvisaoTit.eof) then cxButton4.click();
  
end;

end.
