unit fAcompanhaGiroProduto_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, GridsEh, DBGridEh, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmAcompanhaGiroProduto_view = class(TfrmProcesso_Padrao)
    SPREL_ACOMPANHA_GIRO_PRODUTO: TADOStoredProc;
    SPREL_ACOMPANHA_GIRO_PRODUTOnCdProduto: TIntegerField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnCdTabTipoProduto: TIntegerField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmProduto: TStringField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmMarca: TStringField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnQtdeEstoqueAtual: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOiDiasEstoqueMedio: TIntegerField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnQtdeVendaPeriodo: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnGiroPeriodo: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnQtdeVendaTotal: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnGiroTotal: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnPercVenda: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnPercVendaIdeal: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmDepartamento: TStringField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmCategoria: TStringField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmSubCategoria: TStringField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmSegmento: TStringField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmMarca_1: TStringField;
    DataSource1: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTabTipoProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmMarca: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeEstoqueAtual: TcxGridDBColumn;
    cxGrid1DBTableView1iDiasEstoqueMedio: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeVendaPeriodo: TcxGridDBColumn;
    cxGrid1DBTableView1nGiroPeriodo: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeVendaTotal: TcxGridDBColumn;
    cxGrid1DBTableView1nGiroTotal: TcxGridDBColumn;
    cxGrid1DBTableView1nPercVenda: TcxGridDBColumn;
    cxGrid1DBTableView1nPercVendaIdeal: TcxGridDBColumn;
    cxGrid1DBTableView1cNmDepartamento: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCategoria: TcxGridDBColumn;
    cxGrid1DBTableView1cNmSubCategoria: TcxGridDBColumn;
    cxGrid1DBTableView1cNmSegmento: TcxGridDBColumn;
    cxGrid1DBTableView1cNmMarca_1: TcxGridDBColumn;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAcompanhaGiroProduto_view: TfrmAcompanhaGiroProduto_view;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAcompanhaGiroProduto_view.ToolButton1Click(Sender: TObject);
begin
  inherited;

  {frmMenu.ImprimeDBGrid(DBGridEh1,'ER2Soft - Gest�o de Estoque')} 
end;

end.
