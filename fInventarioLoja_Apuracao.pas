unit fInventarioLoja_Apuracao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping;

type
  TfrmInventarioLoja_Apuracao = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryInventario: TADOQuery;
    dsInventario: TDataSource;
    qryInventarionCdInventario: TIntegerField;
    qryInventarionCdLoja: TStringField;
    qryInventariodDtAbertura: TDateTimeField;
    qryInventariocNmInventario: TStringField;
    qryInventariocNmLocalEstoque: TStringField;
    qryInventariocNmUsuario: TStringField;
    qryInventariodDtContagemEncerrada: TDateTimeField;
    qryInventariocFlgApuradoDivergencia: TIntegerField;
    SP_APURA_DIVERGENCIA_INVENTARIO_LOJA: TADOStoredProc;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInventarioLoja_Apuracao: TfrmInventarioLoja_Apuracao;

implementation

uses fMenu, fInventarioLoja_Contagem_ExibeLotes,
  fInventarioLoja_Apuracao_ExibeDiverg;

{$R *.dfm}

procedure TfrmInventarioLoja_Apuracao.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient ;

  ToolButton1.Click;
  
end;

procedure TfrmInventarioLoja_Apuracao.ToolButton1Click(Sender: TObject);
begin
  inherited;
  qryInventario.Close;
  qryInventario.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryInventario.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  qryInventario.Open;


end;

procedure TfrmInventarioLoja_Apuracao.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmInventarioLoja_Apuracao_ExibeDiverg;
begin
  inherited;

  if (qryInventarionCdInventario.Value > 0) then
  begin

      if (qryInventariodDtContagemEncerrada.AsString = '') then
      begin
          MensagemErro('A contagem deste invent�rio n�o foi encerrada.') ;
          abort ;
      end ;

      if (qryInventariocFlgApuradoDivergencia.Value = 0) then
      begin
          frmMenu.Connection.BeginTrans;

          try
              SP_APURA_DIVERGENCIA_INVENTARIO_LOJA.Close;
              SP_APURA_DIVERGENCIA_INVENTARIO_LOJA.Parameters.ParamByName('@nCdInventario').Value := qryInventarionCdInventario.Value ;
              SP_APURA_DIVERGENCIA_INVENTARIO_LOJA.ExecProc;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          frmMenu.Connection.CommitTrans;

      end ;

      objForm := TfrmInventarioLoja_Apuracao_ExibeDiverg.Create(nil);

      objForm.qryDivergencias.Close;
      PosicionaQuery(objForm.qryDivergencias,qryInventarionCdInventario.asString) ;

      if (objForm.qryDivergencias.eof) then
          ShowMessage('Nenhuma diverg�ncia encontrada.') ;

      objForm.bEncerramento := False ;
      showForm(objForm,true);

      qryInventario.Requery();

  end ;

end;

initialization
    registerClass(TfrmInventarioLoja_Apuracao) ;

end.
