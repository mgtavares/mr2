inherited frmCancelaBorderoRecebimento_titulo: TfrmCancelaBorderoRecebimento_titulo
  Width = 872
  Caption = 'Cancelar Border'#244' Recebimento'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 856
  end
  inherited ToolBar1: TToolBar
    Width = 856
    ButtonWidth = 117
    inherited ToolButton1: TToolButton
      Caption = '&Cancelar Remessa'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 117
    end
    inherited ToolButton2: TToolButton
      Left = 125
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 856
    Height = 435
    Align = alClient
    AllowedOperations = []
    DataGrouping.GroupLevels = <>
    DataSource = dsTitulosRemessa
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footers = <
          item
          end>
      end
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footers = <
          item
          end>
      end
      item
        EditButtons = <>
        FieldName = 'cNrNF'
        Footers = <
          item
          end>
      end
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footers = <
          item
          end>
      end
      item
        EditButtons = <>
        FieldName = 'iParcela'
        Footers = <
          item
          end>
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <
          item
          end>
        Width = 244
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoTit'
        Footers = <
          item
            DisplayFormat = '#,##0.00'
            FieldName = 'nSaldoTit'
            ValueType = fvtSum
          end>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryTitulosRemessa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.cNrNF'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.iParcela'
      '      ,Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Titulo.nSaldoTit'
      '      ,ContaBancaria.cDiretorioArquivoRemessa'
      '  FROM BorderoFinanceiro '
      
        '       INNER JOIN TituloBorderoFinanceiro TBF ON TBF.nCdBorderoF' +
        'inanceiro       = BorderoFinanceiro.nCdBorderoFinanceiro'
      
        '       INNER JOIN Titulo                      ON Titulo.nCdTitul' +
        'o               = TBF.nCdTitulo'
      
        '       LEFT  JOIN Terceiro                    ON Terceiro.nCdTer' +
        'ceiro           = Titulo.nCdTerceiro'
      
        '       INNER JOIN ContaBancaria               ON ContaBancaria.n' +
        'CdContaBancaria = BorderoFinanceiro.nCdContaBancaria'
      ' WHERE BorderoFinanceiro.nCdBorderoFinanceiro = :nPK')
    Left = 328
    Top = 216
    object qryTitulosRemessanCdTitulo: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'nCdTitulo'
    end
    object qryTitulosRemessacNrTit: TStringField
      DisplayLabel = 'N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosRemessacNrNF: TStringField
      DisplayLabel = 'N'#250'm. NF'
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTitulosRemessadDtVenc: TDateTimeField
      DisplayLabel = 'Data Venc.'
      FieldName = 'dDtVenc'
    end
    object qryTitulosRemessaiParcela: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'iParcela'
    end
    object qryTitulosRemessanCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTitulosRemessacNmTerceiro: TStringField
      DisplayLabel = 'Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTitulosRemessanSaldoTit: TBCDField
      DisplayLabel = 'Saldo T'#237'tulo'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosRemessacDiretorioArquivoRemessa: TStringField
      FieldName = 'cDiretorioArquivoRemessa'
      Size = 50
    end
  end
  object dsTitulosRemessa: TDataSource
    DataSet = qryTitulosRemessa
    Left = 344
    Top = 248
  end
  object SP_CANCELA_BORDERO_RECEBIMENTO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CANCELA_BORDERO_RECEBIMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdBorderoFinanceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 440
    Top = 216
  end
end
