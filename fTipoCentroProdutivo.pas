unit fTipoOrdemProducao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmTipoOrdemProducao = class(TfrmCadastro_Padrao)
    qryMasternCdTipoOP: TIntegerField;
    qryMastercNmTipoOP: TStringField;
    qryMasternCdTipoOPRetrabalho: TIntegerField;
    qryMasternCdOperacaoEstoqueCP: TIntegerField;
    qryMasternCdOperacaoEstoqueRE: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoOrdemProducao: TfrmTipoOrdemProducao;

implementation

{$R *.dfm}

procedure TfrmTipoOrdemProducao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CENTROPRODUTIVO' ;
  nCdTabelaSistema  := 312 ;
  nCdConsultaPadrao := 738 ;
  bCodigoAutomatico := True ;
end;

end.
