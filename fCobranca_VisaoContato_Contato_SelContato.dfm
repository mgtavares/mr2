inherited frmCobranca_VisaoContato_Contato_SelContato: TfrmCobranca_VisaoContato_Contato_SelContato
  Left = 458
  Top = 242
  Width = 552
  Height = 389
  BorderIcons = [biSystemMenu]
  Caption = 'Sele'#231#227'o de Contato'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 536
    Height = 322
  end
  inherited ToolBar1: TToolBar
    Width = 536
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 536
    Height = 322
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsListaContato
    Flat = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = []
    Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoTelefone'
        Footers = <>
        Width = 135
      end
      item
        EditButtons = <>
        FieldName = 'cTelefone'
        Footers = <>
        Width = 128
      end
      item
        EditButtons = <>
        FieldName = 'cRamal'
        Footers = <>
        Width = 52
      end
      item
        EditButtons = <>
        FieldName = 'cContato'
        Footers = <>
        Width = 90
      end
      item
        EditButtons = <>
        FieldName = 'cDepartamento'
        Footers = <>
        Width = 90
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryListaContato: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#ContatoCliente'#39') IS NULL)'
      'BEGIN'
      ''
      #9'CREATE TABLE #ContatoCliente (nCdTerceiro     int'
      #9#9#9#9#9#9#9#9' ,cNmTipoTelefone varchar(50)'
      #9#9#9#9#9#9#9#9' ,cTelefone       varchar(20)'
      #9#9#9#9#9#9#9#9' ,cRamal          varchar(5)'
      #9#9#9#9#9#9#9#9' ,cContato        varchar(50)'
      '                                 ,cDepartamento   varchar(50))'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #ContatoCliente')
    Left = 184
    Top = 200
    object qryListaContatonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryListaContatocNmTipoTelefone: TStringField
      DisplayLabel = 'Contatos|Tipo Contato'
      FieldName = 'cNmTipoTelefone'
      Size = 135
    end
    object qryListaContatocTelefone: TStringField
      DisplayLabel = 'Contatos|Telefone'
      FieldName = 'cTelefone'
    end
    object qryListaContatocRamal: TStringField
      DisplayLabel = 'Contatos|Ramal'
      FieldName = 'cRamal'
      Size = 5
    end
    object qryListaContatocContato: TStringField
      DisplayLabel = 'Contatos|Contato'
      FieldName = 'cContato'
      Size = 50
    end
    object qryListaContatocDepartamento: TStringField
      DisplayLabel = 'Contatos|Departamento'
      FieldName = 'cDepartamento'
      Size = 50
    end
  end
  object dsListaContato: TDataSource
    DataSet = qryListaContato
    Left = 224
    Top = 192
  end
end
