inherited frmRecebimento_Divergencias: TfrmRecebimento_Divergencias
  Left = 119
  Top = 273
  Width = 922
  Height = 344
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Lista de Diverg'#234'ncias do Item'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 906
    Height = 279
  end
  inherited ToolBar1: TToolBar
    Width = 906
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 906
    Height = 279
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsDivergencias
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTabTipoDiverg'
        Footers = <>
        Width = 38
      end
      item
        EditButtons = <>
        FieldName = 'cNmTabTipoDiverg'
        Footers = <>
        Width = 191
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
        Width = 158
      end
      item
        EditButtons = <>
        FieldName = 'dDtautorizacao'
        Footers = <>
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'cFlgAutorizAutomatica'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cOBS'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryDivergencias: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TabTipoDiverg.nCdTabTipoDiverg'
      '      ,TabTipoDiverg.cNmTabTipoDiverg'
      '      ,Usuario.cNmUsuario'
      '      ,dDtautorizacao'
      '      ,HistAutorDiverg.cFlgAutorizAutomatica'
      '      ,HistAutorDiverg.cOBS'
      '  FROM HistAutorDiverg'
      
        '       INNER JOIN TabTipoDiverg ON TabTipoDiverg.nCdTabTipoDiver' +
        'g = HistAutorDiverg.nCdTabTipoDiverg'
      
        '       LEFT  JOIN Usuario       ON Usuario.nCdUsuario           ' +
        '  = HistAutorDiverg.nCdUsuario'
      ' WHERE nCdItemRecebimento = :nPK')
    Left = 136
    Top = 104
    object qryDivergenciasnCdTabTipoDiverg: TIntegerField
      DisplayLabel = 'Diverg'#234'ncias do Item|C'#243'd'
      FieldName = 'nCdTabTipoDiverg'
    end
    object qryDivergenciascNmTabTipoDiverg: TStringField
      DisplayLabel = 'Diverg'#234'ncias do Item|Descri'#231#227'o'
      FieldName = 'cNmTabTipoDiverg'
      Size = 50
    end
    object qryDivergenciascNmUsuario: TStringField
      DisplayLabel = 'Dados da Autoriza'#231#227'o|Usu'#225'rio'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryDivergenciasdDtautorizacao: TDateTimeField
      DisplayLabel = 'Dados da Autoriza'#231#227'o|Data'
      FieldName = 'dDtautorizacao'
    end
    object qryDivergenciascFlgAutorizAutomatica: TIntegerField
      DisplayLabel = 'Dados da Autoriza'#231#227'o|Autom'#225'tico'
      FieldName = 'cFlgAutorizAutomatica'
    end
    object qryDivergenciascOBS: TStringField
      DisplayLabel = 'Dados da Autoriza'#231#227'o|Observa'#231#227'o'
      FieldName = 'cOBS'
      Size = 50
    end
  end
  object dsDivergencias: TDataSource
    DataSet = qryDivergencias
    Left = 192
    Top = 112
  end
end
