unit fApropAdiantamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping;

type
  TfrmApropAdiantamento = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryPedido: TADOQuery;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidonValAdiantamento: TBCDField;
    qryPedidonValPedido: TBCDField;
    dsPedido: TDataSource;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmApropAdiantamento: TfrmApropAdiantamento;

implementation

uses fMenu, fApropAdiantamento_dados;

{$R *.dfm}

procedure TfrmApropAdiantamento.ToolButton1Click(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryPedido, IntToStr(frmMenu.nCdEmpresaAtiva)) ;

end;

procedure TfrmApropAdiantamento.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient ;
  
  ToolButton1.Click;
end;

procedure TfrmApropAdiantamento.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmApropAdiantamento_dados ;
begin
  inherited;

  if qryPedido.Active and (qryPedidonCdpedido.Value > 0) then
  begin

      objForm := TfrmApropAdiantamento_dados.Create(Self) ;

      objForm.nValAdiantamento := qryPedidonValAdiantamento.Value ;
      objForm.nCdPedido        := qryPedidonCdPedido.Value ;

      showForm( objForm , TRUE ) ;

      ToolButton1.Click;

  end ;

end;

initialization
    RegisterClass(TfrmApropAdiantamento) ;
    
end.
