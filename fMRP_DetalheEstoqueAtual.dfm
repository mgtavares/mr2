inherited frmMRP_DetalheEstoqueAtual: TfrmMRP_DetalheEstoqueAtual
  Left = 206
  Top = 168
  Width = 688
  BorderIcons = [biSystemMenu]
  Caption = 'Estoque Atual'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 672
  end
  inherited ToolBar1: TToolBar
    Width = 672
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 672
    Height = 435
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.FonteSomenteLeitura
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1nCdLocalEstoque: TcxGridDBColumn
        Caption = 'C'#243'd'
        DataBinding.FieldName = 'nCdLocalEstoque'
        Width = 54
      end
      object cxGrid1DBTableView1cNmLocalEstoque: TcxGridDBColumn
        Caption = 'Local de Estoque'
        DataBinding.FieldName = 'cNmLocalEstoque'
        Width = 209
      end
      object cxGrid1DBTableView1nQtdeDisponivel: TcxGridDBColumn
        Caption = 'Saldo Estoque'
        DataBinding.FieldName = 'nQtdeDisponivel'
        Width = 113
      end
      object cxGrid1DBTableView1cLocalizacao: TcxGridDBColumn
        Caption = 'Endere'#231'o'
        DataBinding.FieldName = 'cLocalizacao'
      end
      object cxGrid1DBTableView1dDtUltInventario: TcxGridDBColumn
        Caption = #218'lt. Invent'#225'rio'
        DataBinding.FieldName = 'dDtUltInventario'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryPosicaoEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT LocalEstoque.nCdLocalEstoque'
      '      ,LocalEstoque.cNmLocalEstoque'
      '      ,nQtdeDisponivel'
      '      ,cLocalizacao'
      '      ,dbo.fn_OnlyDate(dDtUltInventario) as dDtUltInventario'
      '  FROM PosicaoEstoque'
      
        '       INNER JOIN LocalEstoque ON LocalEstoque.nCdLocalEstoque =' +
        ' PosicaoEstoque.nCdLocalEstoque'
      ' WHERE nCdProduto        = :nPK'
      '   AND cFlgEstoqueDisp   = 1'
      '   AND cFlgInfluenciaMRP = 1')
    Left = 272
    Top = 240
    object qryPosicaoEstoquenCdLocalEstoque: TIntegerField
      DisplayLabel = 'Estoque|C'#243'd'
      FieldName = 'nCdLocalEstoque'
    end
    object qryPosicaoEstoquecNmLocalEstoque: TStringField
      DisplayLabel = 'Estoque|Descri'#231#227'o'
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
    object qryPosicaoEstoquenQtdeDisponivel: TBCDField
      DisplayLabel = 'Saldo|Estoque'
      FieldName = 'nQtdeDisponivel'
      DisplayFormat = ',0'
      Precision = 12
    end
    object qryPosicaoEstoquecLocalizacao: TStringField
      DisplayLabel = 'Estoque|Local F'#237'sico'
      FieldName = 'cLocalizacao'
    end
    object qryPosicaoEstoquedDtUltInventario: TDateTimeField
      FieldName = 'dDtUltInventario'
      ReadOnly = True
    end
  end
  object DataSource1: TDataSource
    DataSet = qryPosicaoEstoque
    Left = 384
    Top = 240
  end
end
