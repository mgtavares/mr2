inherited frmTipoSP: TfrmTipoSP
  Left = 248
  Top = 115
  Height = 565
  Caption = 'frmTipoSP'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 502
  end
  object Label1: TLabel [1]
    Left = 63
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 52
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 15
    Top = 94
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Esp'#233'cie de T'#237'tulo'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 48
    Top = 166
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de SP'
    FocusControl = DBEdit5
  end
  object Label5: TLabel [5]
    Left = 136
    Top = 166
    Width = 168
    Height = 13
    Caption = '(P-Pagamento / A-Adiantamento)'
  end
  object Label6: TLabel [6]
    Left = 3
    Top = 118
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Adiantamento'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 16
    Top = 142
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Hist'#243'rico Padr'#227'o'
    FocusControl = DBEdit8
  end
  inherited ToolBar2: TToolBar
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [9]
    Tag = 1
    Left = 104
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdTipoSP'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [10]
    Left = 104
    Top = 64
    Width = 490
    Height = 19
    DataField = 'cNmTipoSP'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [11]
    Left = 104
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdEspTit'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit5: TDBEdit [12]
    Left = 104
    Top = 160
    Width = 25
    Height = 19
    DataField = 'cFlgTipoSP'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBCheckBox1: TDBCheckBox [13]
    Left = 104
    Top = 185
    Width = 149
    Height = 17
    Caption = 'Obrigar Digita'#231#227'o de NF'
    Color = clWhite
    Ctl3D = False
    DataField = 'cFlgExigeNF'
    DataSource = dsMaster
    ParentColor = False
    ParentCtl3D = False
    TabOrder = 7
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox2: TDBCheckBox [14]
    Left = 266
    Top = 185
    Width = 179
    Height = 17
    Caption = 'SP de pagamento de impostos'
    Ctl3D = False
    DataField = 'cFlgImposto'
    DataSource = dsMaster
    ParentCtl3D = False
    TabOrder = 8
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object cxPageControl1: TcxPageControl [15]
    Left = 16
    Top = 217
    Width = 577
    Height = 294
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 9
    ClientRectBottom = 290
    ClientRectLeft = 4
    ClientRectRight = 573
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Usuarios Permitidos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 569
        Height = 266
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuarioTipoSP
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTipoSP'
            Footers = <>
            Visible = False
          end
          item
            DropDownSizing = True
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Title.Caption = 'Usu'#225'rios Permitidos|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Width = 467
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit6: TDBEdit [16]
    Left = 104
    Top = 112
    Width = 65
    Height = 19
    DataField = 'nCdTipoAdiantamento'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit6Exit
    OnKeyDown = DBEdit6KeyDown
  end
  object DBEdit7: TDBEdit [17]
    Tag = 1
    Left = 172
    Top = 112
    Width = 422
    Height = 19
    DataField = 'cNmTipoAdiantamento'
    DataSource = dsTipoAdiantamento
    TabOrder = 10
  end
  object DBEdit8: TDBEdit [18]
    Left = 104
    Top = 136
    Width = 490
    Height = 19
    DataField = 'cHistoricoPadrao'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit4: TDBEdit [19]
    Tag = 1
    Left = 172
    Top = 88
    Width = 422
    Height = 19
    DataField = 'cNmEspTit'
    DataSource = dsEspTit
    TabOrder = 11
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TIPOSP'
      'WHERE nCdTipoSP = :nPK')
    Left = 184
    Top = 347
    object qryMasternCdTipoSP: TIntegerField
      FieldName = 'nCdTipoSP'
    end
    object qryMastercNmTipoSP: TStringField
      FieldName = 'cNmTipoSP'
      Size = 50
    end
    object qryMastercFlgTipoSP: TStringField
      FieldName = 'cFlgTipoSP'
      FixedChar = True
      Size = 1
    end
    object qryMastercFlgExigeNF: TIntegerField
      FieldName = 'cFlgExigeNF'
    end
    object qryMastercFlgImposto: TIntegerField
      FieldName = 'cFlgImposto'
    end
    object qryMasternCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryMastercNmEspTit: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmEspTit'
      LookupDataSet = qryEspTit
      LookupKeyFields = 'nCdEspTit'
      LookupResultField = 'cNmEspTit'
      KeyFields = 'nCdEspTit'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMasternCdTipoAdiantamento: TIntegerField
      FieldName = 'nCdTipoAdiantamento'
    end
    object qryMastercHistoricoPadrao: TStringField
      FieldName = 'cHistoricoPadrao'
      Size = 100
    end
  end
  inherited dsMaster: TDataSource
    Left = 184
    Top = 379
  end
  inherited qryID: TADOQuery
    Left = 216
    Top = 347
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 248
    Top = 347
  end
  inherited qryStat: TADOQuery
    Left = 216
    Top = 379
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 248
    Top = 379
  end
  inherited ImageList1: TImageList
    Left = 152
    Top = 347
  end
  object qryEspTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM EspTit'
      'WHERE nCdEspTit = :nPK')
    Left = 344
    Top = 347
    object qryEspTitnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitnCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryEspTitcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryEspTitcTipoMov: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object qryEspTitcFlgPrev: TIntegerField
      FieldName = 'cFlgPrev'
    end
  end
  object qryUsuarioTipoSP: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioTipoSPBeforePost
    Parameters = <
      item
        Name = 'nCdTipoSP'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM UsuarioTipoSP'
      'Where nCdTipoSP = :nCdTipoSP')
    Left = 280
    Top = 347
    object qryUsuarioTipoSPnCdTipoSP: TIntegerField
      FieldName = 'nCdTipoSP'
    end
    object qryUsuarioTipoSPnCdUsuario: TIntegerField
      DisplayLabel = 'Usu'#225'rios Permitidos|C'#243'd'
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioTipoSPcNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rios Permitidos|Nome Usu'#225'rio'
      FieldKind = fkLookup
      FieldName = 'cNmUsuario'
      LookupDataSet = qryUsuario
      LookupKeyFields = 'nCdUsuario'
      LookupResultField = 'cNmUsuario'
      KeyFields = 'nCdUsuario'
      Size = 50
      Lookup = True
    end
    object qryUsuarioTipoSPnCdUsuarioTipoSP: TIntegerField
      FieldName = 'nCdUsuarioTipoSP'
    end
  end
  object dsUsuarioTipoSP: TDataSource
    DataSet = qryUsuarioTipoSP
    Left = 280
    Top = 379
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM USUARIO'
      'WHERE nCdStatus = 1')
    Left = 376
    Top = 347
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmLogin: TStringField
      FieldName = 'cNmLogin'
    end
    object qryUsuariocSenha: TStringField
      FieldName = 'cSenha'
    end
    object qryUsuariocFlgAtivo: TSmallintField
      FieldName = 'cFlgAtivo'
    end
    object qryUsuariodDtUltAcesso: TDateTimeField
      FieldName = 'dDtUltAcesso'
    end
    object qryUsuariodDtUltAltSenha: TDateTimeField
      FieldName = 'dDtUltAltSenha'
    end
    object qryUsuarionCdEmpPadrao: TIntegerField
      FieldName = 'nCdEmpPadrao'
    end
    object qryUsuarionCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryUsuariocCPF: TStringField
      FieldName = 'cCPF'
      Size = 14
    end
    object qryUsuarionCdTerceiroResponsavel: TIntegerField
      FieldName = 'nCdTerceiroResponsavel'
    end
    object qryUsuariocFlagMaster: TSmallintField
      FieldName = 'cFlagMaster'
    end
    object qryUsuariocAcessoWERP: TSmallintField
      FieldName = 'cAcessoWERP'
    end
    object qryUsuariocTrocaSenhaProxLogin: TSmallintField
      FieldName = 'cTrocaSenhaProxLogin'
    end
  end
  object qryTipoAdiantamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoAdiantamento'
      'where nCdTipoAdiantamento = :nPk')
    Left = 312
    Top = 347
    object qryTipoAdiantamentonCdTipoAdiantamento: TIntegerField
      FieldName = 'nCdTipoAdiantamento'
    end
    object qryTipoAdiantamentocNmTipoAdiantamento: TStringField
      FieldName = 'cNmTipoAdiantamento'
      Size = 50
    end
    object qryTipoAdiantamentonCdEspTitAdiant: TIntegerField
      FieldName = 'nCdEspTitAdiant'
    end
    object qryTipoAdiantamentonCdPlanoContaSaida: TIntegerField
      FieldName = 'nCdPlanoContaSaida'
    end
    object qryTipoAdiantamentonCdPlanoContaEntrada: TIntegerField
      FieldName = 'nCdPlanoContaEntrada'
    end
  end
  object dsTipoAdiantamento: TDataSource
    DataSet = qryTipoAdiantamento
    Left = 312
    Top = 379
  end
  object dsEspTit: TDataSource
    DataSet = qryEspTit
    Left = 344
    Top = 379
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 377
    Top = 379
  end
end
