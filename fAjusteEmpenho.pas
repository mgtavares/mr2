unit fAjusteEmpenho;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxLookAndFeelPainters, StdCtrls, cxButtons, Mask, ER2Lookup,
  DBCtrls;

type
  TfrmAjusteEmpenho = class(TfrmProcesso_Padrao)
    qryProduto: TADOQuery;
    qryLocalEstoque: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    edtProduto: TER2LookupMaskEdit;
    edtLocalEstoque: TER2LookupMaskEdit;
    edtNumOP: TEdit;
    edtPedido: TMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    cxButton1: TcxButton;
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAjusteEmpenho: TfrmAjusteEmpenho;

implementation

uses fAjusteEmpenho_Produtos, fMenu;

{$R *.dfm}

procedure TfrmAjusteEmpenho.cxButton1Click(Sender: TObject);
var
  objForm : TfrmAjusteEmpenho_Produtos;
begin
  inherited;

  objForm := TfrmAjusteEmpenho_Produtos.Create(nil);

  objForm.qryProdutosEmpenhados.Close;
  objForm.qryProdutosEmpenhados.Parameters.ParamByName('nCdProduto').Value      := frmMenu.ConvInteiro(edtProduto.Text);
  objForm.qryProdutosEmpenhados.Parameters.ParamByName('nCdLocalEstoque').Value := frmMenu.ConvInteiro(edtLocalEstoque.Text);
  objForm.qryProdutosEmpenhados.Parameters.ParamByName('cNumeroOP').Value       := Trim(edtNumOP.Text);
  objForm.qryProdutosEmpenhados.Parameters.ParamByName('nCdPedido').Value       := frmMenu.ConvInteiro(edtPedido.Text);
  objForm.qryProdutosEmpenhados.Open;

  if (objForm.qryProdutosEmpenhados.Eof) then
  begin
      MensagemAlerta('Nenhum registro encontrado para o filtro selecionado.');
      FreeAndNil(objForm);
      exit;
  end;

  showForm(objForm,True);

end;

initialization
    RegisterClass(TfrmAjusteEmpenho);

end.
