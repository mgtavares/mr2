unit rPosicaoInadimplencia_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosicaoInadimplencia_view = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_POSICAO_INADIMPLENCIA: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRShape2: TQRShape;
    QRDBText1: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    SPREL_POSICAO_INADIMPLENCIAcNmTerceiro: TStringField;
    SPREL_POSICAO_INADIMPLENCIAcCNPJCPF: TStringField;
    SPREL_POSICAO_INADIMPLENCIAdDtCadastro: TStringField;
    SPREL_POSICAO_INADIMPLENCIAcMesAno: TStringField;
    SPREL_POSICAO_INADIMPLENCIAiAno: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAiMes: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAnValTotal: TFloatField;
    SPREL_POSICAO_INADIMPLENCIAiQtdeTituloTotal: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAnValTotalInadimp: TFloatField;
    SPREL_POSICAO_INADIMPLENCIAiQtdeTituloInadimp: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAnValTotalInadimpAtual: TFloatField;
    SPREL_POSICAO_INADIMPLENCIAiQtdeTituloInadimpAtual: TIntegerField;
    SPREL_POSICAO_INADIMPLENCIAnMediaCompetencia: TFloatField;
    SPREL_POSICAO_INADIMPLENCIAnMediaAtual: TFloatField;
    QRBand2: TQRBand;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRShape6: TQRShape;
    QRLabel11: TQRLabel;
    QRBand5: TQRBand;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoInadimplencia_view: TrptPosicaoInadimplencia_view;

implementation

{$R *.dfm}

procedure TrptPosicaoInadimplencia_view.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin

    if (QRBand3.Color = clWhite) then
        QRBand3.Color := clSilver
    else QRBand3.Color := clWhite ;

end;

end.
