inherited frmBaixaTituloDeposito_Titulos: TfrmBaixaTituloDeposito_Titulos
  Left = 36
  Top = 129
  Width = 1081
  Height = 546
  Caption = 'T'#237'tulos em Aberto'
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 289
    Width = 1065
    Height = 219
  end
  inherited ToolBar1: TToolBar
    Width = 1065
    ButtonWidth = 105
    inherited ToolButton1: TToolButton
      Caption = '&Processar Baixa'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 105
    end
    inherited ToolButton2: TToolButton
      Left = 113
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1065
    Height = 52
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 8
      Top = 26
      Width = 136
      Height = 13
      Caption = 'Saldo Dispon'#237'vel para Baixas'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object edtSaldoBaixar: TcxCurrencyEdit
      Left = 152
      Top = 14
      Width = 145
      Height = 27
      Enabled = False
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.Alignment.Vert = taBottomJustify
      Properties.ReadOnly = True
      Properties.ValidateOnEnter = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'Consolas'
      Style.Font.Style = [fsBold]
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox [3]
    Left = 0
    Top = 81
    Width = 1065
    Height = 208
    Align = alTop
    Caption = 'T'#237'tulos em Aberto'
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 2
      Top = 15
      Width = 1061
      Height = 200
      Align = alTop
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dsTitulo_NaoSelec
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GridLineColor = clSilver
        OptionsView.GridLines = glVertical
        OptionsView.GroupByBox = False
        Styles.Content = frmMenu.FonteSomenteLeitura
        Styles.Header = frmMenu.Header
        object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
          DataBinding.FieldName = 'nCdTitulo'
          Visible = False
        end
        object cxGrid1DBTableView1cNrNF: TcxGridDBColumn
          Caption = 'Nr. NF'
          DataBinding.FieldName = 'cNrNF'
          Width = 76
        end
        object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
          Caption = 'Nr. T'#237'tulo'
          DataBinding.FieldName = 'cNrTit'
        end
        object cxGrid1DBTableView1iParcela: TcxGridDBColumn
          Caption = 'Parcela'
          DataBinding.FieldName = 'iParcela'
          Width = 67
        end
        object cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn
          Caption = 'Data Emiss'#227'o'
          DataBinding.FieldName = 'dDtEmissao'
        end
        object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
          Caption = 'Data Vencimento'
          DataBinding.FieldName = 'dDtVenc'
        end
        object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
          Caption = 'Saldo T'#237'tulo'
          DataBinding.FieldName = 'nSaldoTit'
          Width = 105
        end
        object cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn
          Caption = 'Esp'#233'cie T'#237'tulo'
          DataBinding.FieldName = 'cNmEspTit'
          Width = 187
        end
        object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
          Caption = 'Terceiro'
          DataBinding.FieldName = 'cNmTerceiro'
          Width = 222
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object GroupBox3: TGroupBox [4]
    Left = 0
    Top = 289
    Width = 1065
    Height = 219
    Align = alClient
    Caption = 'T'#237'tulos Selecionados'
    TabOrder = 3
    object cxGrid2: TcxGrid
      Left = 2
      Top = 15
      Width = 1061
      Height = 200
      Align = alTop
      TabOrder = 0
      object cxGridDBTableView1: TcxGridDBTableView
        OnDblClick = cxGridDBTableView1DblClick
        DataController.DataSource = dsTitulo_Selec
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GridLineColor = clSilver
        OptionsView.GridLines = glVertical
        OptionsView.GroupByBox = False
        Styles.Content = frmMenu.FonteSomenteLeitura
        Styles.Header = frmMenu.Header
        object cxGridDBTableView1nCdTitulo: TcxGridDBColumn
          DataBinding.FieldName = 'nCdTitulo'
          Visible = False
        end
        object cxGridDBTableView1cNrNF: TcxGridDBColumn
          Caption = 'Nr. NF'
          DataBinding.FieldName = 'cNrNF'
          Width = 73
        end
        object cxGridDBTableView1cNrTit: TcxGridDBColumn
          Caption = 'Nr. T'#237'tulo'
          DataBinding.FieldName = 'cNrTit'
        end
        object cxGridDBTableView1iParcela: TcxGridDBColumn
          Caption = 'Parcela'
          DataBinding.FieldName = 'iParcela'
          Width = 68
        end
        object cxGridDBTableView1dDtEmissao: TcxGridDBColumn
          Caption = 'Data Emiss'#227'o'
          DataBinding.FieldName = 'dDtEmissao'
        end
        object cxGridDBTableView1dDtVenc: TcxGridDBColumn
          Caption = 'Data Vencimento'
          DataBinding.FieldName = 'dDtVenc'
        end
        object cxGridDBTableView1nSaldoTit: TcxGridDBColumn
          Caption = 'Saldo T'#237'tulo'
          DataBinding.FieldName = 'nSaldoTit'
          Width = 111
        end
        object cxGridDBTableView1nValLiq: TcxGridDBColumn
          Caption = 'Valor Baixado'
          DataBinding.FieldName = 'nValLiq'
          Width = 117
        end
        object cxGridDBTableView1cNmEspTit: TcxGridDBColumn
          Caption = 'Esp'#233'cie T'#237'tulo'
          DataBinding.FieldName = 'cNmEspTit'
          Width = 144
        end
        object cxGridDBTableView1cNmTerceiro: TcxGridDBColumn
          Caption = 'Terceiro'
          DataBinding.FieldName = 'cNmTerceiro'
          Width = 194
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 264
    Top = 129
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Titulos'#39') IS  NULL) '
      'BEGIN'
      ''
      #9'CREATE TABLE #Temp_Titulos (nCdTitulo   int'
      #9#9#9#9#9#9#9'   ,cNrNF       varchar(20)'
      #9#9#9#9#9#9#9'   ,cNrTit      varchar(20)'
      #9#9#9#9#9#9#9'   ,iParcela    int'
      #9#9#9#9#9#9#9'   ,dDtEmissao  datetime'
      #9#9#9#9#9#9#9'   ,dDtVenc     datetime'
      #9#9#9#9#9#9#9'   ,nSaldoTit   decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,cNmEspTit   varchar(50)'
      '                 ,cNmTerceiro varchar(100)'
      '                 ,nValLiq     decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,cFlgAux     int default 0 not null)'
      ''
      'END'
      ''
      'TRUNCATE TABLE #Temp_Titulos')
    Left = 136
    Top = 129
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Titulos'#39') IS  NULL) '
      'BEGIN'
      ''
      #9'CREATE TABLE #Temp_Titulos (nCdTitulo   int'
      #9#9#9#9#9#9#9'   ,cNrNF       varchar(20)'
      #9#9#9#9#9#9#9'   ,cNrTit      varchar(20)'
      #9#9#9#9#9#9#9'   ,iParcela    int'
      #9#9#9#9#9#9#9'   ,dDtEmissao  datetime'
      #9#9#9#9#9#9#9'   ,dDtVenc     datetime'
      #9#9#9#9#9#9#9'   ,nSaldoTit   decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,cNmEspTit   varchar(50)'
      '                 ,cNmTerceiro varchar(100)'
      '                 ,nValLiq     decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,cFlgAux     int default 0 not null)'
      ''
      'END'
      ''
      'DECLARE @nCdTerceiro       int'
      '       ,@nCdGrupoEconomico int'
      '       ,@nCdEmpresa        int'
      ''
      'Set @nCdTerceiro = :nCdTerceiro'
      'Set @nCdEmpresa  = :nCdEmpresa'
      ''
      'SELECT @nCdGrupoEconomico = nCdGrupoEconomico'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = @nCdTerceiro'
      ''
      'INSERT INTO #Temp_Titulos (nCdTitulo '
      '                          ,cNrNF     '
      '                          ,cNrTit    '
      '                          ,iParcela  '
      '                          ,dDtEmissao'
      '                          ,dDtVenc  '
      '                          ,nSaldoTit'
      '                          ,cNmEspTit'
      '                          ,cNmTerceiro)'
      '                    SELECT nCdTitulo'
      '                          ,cNrNF'
      '                          ,cNrTit'
      '                          ,iParcela'
      '                          ,dDtEmissao'
      '                          ,dDtVenc'
      '                          ,nSaldoTit'
      '                          ,cNmEspTit'
      '                          ,cNmTerceiro'
      '                      FROM Titulo'
      
        '                           INNER JOIN Terceiro ON Terceiro.nCdTe' +
        'rceiro = Titulo.nCdTerceiro'
      
        '                           INNER JOIN EspTit   ON EspTit.nCdEspT' +
        'it     = Titulo.nCdEspTit'
      
        '                     WHERE ((Titulo.nCdTerceiro = @nCdTerceiro) ' +
        'OR (Terceiro.nCdGrupoEconomico = @nCdGrupoEconomico))'
      '                       AND cSenso      = '#39'C'#39
      '                       AND dDtCancel  IS NULL'
      '                       AND dDtBloqTit IS NULL'
      '                       AND nSaldoTit   > 0'
      '                       AND nCdEmpresa  = @nCdEmpresa   '
      '')
    Left = 176
    Top = 129
  end
  object qryTitulo_NaoSelec: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Titulos'#39') IS  NULL) '
      'BEGIN'
      ''
      #9'CREATE TABLE #Temp_Titulos (nCdTitulo   int'
      #9#9#9#9#9#9#9'   ,cNrNF       varchar(15)'
      #9#9#9#9#9#9#9'   ,cNrTit      varchar(15)'
      #9#9#9#9#9#9#9'   ,iParcela    int'
      #9#9#9#9#9#9#9'   ,dDtEmissao  datetime'
      #9#9#9#9#9#9#9'   ,dDtVenc     datetime'
      #9#9#9#9#9#9#9'   ,nSaldoTit   decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,cNmEspTit   varchar(50)'
      '                 ,cNmTerceiro varchar(50)'
      '                 ,nValLiq     decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,cFlgAux     int default 0 not null)'
      ''
      'END'
      ''
      'SELECT nCdTitulo'
      '      ,cNrNF'
      '      ,cNrTit'
      '      ,iParcela'
      '      ,dDtEmissao'
      '      ,dDtVenc'
      '      ,nSaldoTit'
      '      ,cNmEspTit'
      '      ,cNmTerceiro'
      '  FROM #Temp_Titulos'
      ' WHERE cFlgAux = 0'
      ' ORDER BY dDtVenc, cNrNF'
      '')
    Left = 216
    Top = 185
    object qryTitulo_NaoSelecnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTitulo_NaoSeleccNrNF: TStringField
      FieldName = 'cNrNF'
      Size = 15
    end
    object qryTitulo_NaoSeleccNrTit: TStringField
      FieldName = 'cNrTit'
      Size = 15
    end
    object qryTitulo_NaoSeleciParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTitulo_NaoSelecdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTitulo_NaoSelecdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulo_NaoSelecnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulo_NaoSeleccNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTitulo_NaoSeleccNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryTitulo_Selec: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Titulos'#39') IS  NULL) '
      'BEGIN'
      ''
      #9'CREATE TABLE #Temp_Titulos (nCdTitulo   int'
      #9#9#9#9#9#9#9'   ,cNrNF       varchar(15)'
      #9#9#9#9#9#9#9'   ,cNrTit      varchar(15)'
      #9#9#9#9#9#9#9'   ,iParcela    int'
      #9#9#9#9#9#9#9'   ,dDtEmissao  datetime'
      #9#9#9#9#9#9#9'   ,dDtVenc     datetime'
      #9#9#9#9#9#9#9'   ,nSaldoTit   decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,cNmEspTit   varchar(50)'
      '                 ,cNmTerceiro varchar(50)'
      '                 ,nValLiq     decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,cFlgAux     int default 0 not null)'
      ''
      'END'
      ''
      'SELECT nCdTitulo'
      '      ,cNrNF'
      '      ,cNrTit'
      '      ,iParcela'
      '      ,dDtEmissao'
      '      ,dDtVenc'
      '      ,cNmEspTit'
      '      ,cNmTerceiro'
      '      ,nSaldoTit'
      '      ,nValLiq'
      '  FROM #Temp_Titulos'
      ' WHERE cFlgAux = 1'
      ' ORDER BY dDtVenc, cNrNF'
      '')
    Left = 272
    Top = 185
    object qryTitulo_SelecnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTitulo_SeleccNrNF: TStringField
      FieldName = 'cNrNF'
      Size = 15
    end
    object qryTitulo_SeleccNrTit: TStringField
      FieldName = 'cNrTit'
      Size = 15
    end
    object qryTitulo_SeleciParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTitulo_SelecdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTitulo_SelecdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulo_SelecnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulo_SeleccNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTitulo_SeleccNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTitulo_SelecnValLiq: TBCDField
      FieldName = 'nValLiq'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsTitulo_NaoSelec: TDataSource
    DataSet = qryTitulo_NaoSelec
    Left = 176
    Top = 185
  end
  object dsTitulo_Selec: TDataSource
    DataSet = qryTitulo_Selec
    Left = 304
    Top = 185
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 368
    Top = 161
  end
  object SP_PROCESSA_BAIXA_TITULO_DEPOSITO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_PROCESSA_BAIXA_TITULO_DEPOSITO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtDeposito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@nCdLanctoFin'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 600
    Top = 161
  end
end
