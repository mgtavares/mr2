unit rMovTit_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptMovTit_view = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel10: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText6: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText9: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText11: TQRDBText;
    usp_RelatoriodDtMov: TDateTimeField;
    usp_RelatorionCdTitulo: TIntegerField;
    usp_RelatoriocNrTit: TStringField;
    usp_RelatorioiParcela: TIntegerField;
    usp_RelatoriocSigla: TStringField;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatoriocNmCategFinanc: TStringField;
    usp_RelatoriocNmOperacao: TStringField;
    usp_RelatorionValMov: TFloatField;
    usp_RelatoriocNmContaBancaria: TStringField;
    usp_RelatoriocNmFormaPagto: TStringField;
    usp_RelatorioiNrCheque: TStringField;
    usp_RelatoriodDtCad: TDateTimeField;
    usp_RelatoriodDtCancel: TDateTimeField;
    usp_RelatoriodDtContab: TDateTimeField;
    usp_RelatoriocSenso: TStringField;
    QRLabel9: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel18: TQRLabel;
    usp_RelatoriocNmEspTit: TStringField;
    usp_RelatoriocNrNF: TStringField;
    QRLabel15: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRDBText14: TQRDBText;
    usp_RelatorionCdLojaTit: TIntegerField;
    QRLabel22: TQRLabel;
    usp_RelatorionCdEmpresa: TIntegerField;
    QRLabel23: TQRLabel;
    QRDBText15: TQRDBText;
    Cancel: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel14: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptMovTit_view: TrptMovTit_view;

implementation

{$R *.dfm}

end.
