unit fGeracaoBorderoRecebimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DBCtrls, Mask, ER2Lookup, cxLookAndFeelPainters, cxButtons, DB,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, cxPC, DBGridEhGrouping, GridsEh,
  DBGridEh, Menus;

type
  TfrmGeracaoBorderoRecebimento = class(TfrmProcesso_Padrao)
    Image2: TImage;
    Label3: TLabel;
    edtFormaPagto: TER2LookupMaskEdit;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    MaskEditEmissaoInicial: TMaskEdit;
    Label11: TLabel;
    MaskEditEmissaoFinal: TMaskEdit;
    Label1: TLabel;
    MaskEditVenctoInicial: TMaskEdit;
    Label2: TLabel;
    MaskEditVenctoFinal: TMaskEdit;
    Label4: TLabel;
    edtTerceiro: TER2LookupMaskEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    MaskEditNumeroNF: TMaskEdit;
    Label6: TLabel;
    qryContaBancaria: TADOQuery;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryFormaPagamento: TADOQuery;
    qryFormaPagamentonCdFormaPagto: TIntegerField;
    qryFormaPagamentocNmFormaPagto: TStringField;
    edtContaBancaria: TER2LookupMaskEdit;
    dsTerceiro: TDataSource;
    dsContaBancaria: TDataSource;
    dsFormaPagto: TDataSource;
    btnExibirTitulos: TcxButton;
    qryTitulos: TADOQuery;
    qryTitulosnCdTitulo: TIntegerField;
    qryTituloscFlgOK: TIntegerField;
    qryTituloscNrTit: TStringField;
    qryTituloscNrNF: TStringField;
    qryTitulosiParcela: TIntegerField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTitulosnSaldoTit: TBCDField;
    qryTitulosnCdTerceiro: TIntegerField;
    qryTituloscNmTerceiro: TStringField;
    qryTitulosnCdFormaPagto: TIntegerField;
    qryTituloscNmFormaPagto: TStringField;
    dsTitulos: TDataSource;
    CheckBox2: TCheckBox;
    Label5: TLabel;
    qryTempInstrucaoTitulo: TADOQuery;
    dsTempInstrucaoTitulo: TDataSource;
    qryTempInstrucaoTitulonCdTituloInstrucaoBoleto: TIntegerField;
    qryTempInstrucaoTitulocNmMensagem: TStringField;
    qryTempInstrucaoTitulocFlgMensagemPadrao: TIntegerField;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancariacNmBanco: TStringField;
    qryContaBancariacAgencia: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancariacNmTitular: TStringField;
    qryContaBancariaiUltimoBoleto: TIntegerField;
    qryContaBancariacPrimeiraInstrucaoBoleto: TStringField;
    qryContaBancariacSegundaInstrucaoBoleto: TStringField;
    qryContaBancarianCdTabTipoModeloImpBoleto: TIntegerField;
    procedure edtContaBancariaBeforePosicionaQry(Sender: TObject);
    procedure btnExibirTitulosClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    function BoolToInt (Valor : Boolean) : integer;
  public
    { Public declarations }
    nCdTitulo : integer;
  end;

var
  frmGeracaoBorderoRecebimento: TfrmGeracaoBorderoRecebimento;

implementation

uses fMenu, fGeracaoBorderoRecebimento_titulos;

{$R *.dfm}
procedure TfrmGeracaoBorderoRecebimento.edtContaBancariaBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;
  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmGeracaoBorderoRecebimento.btnExibirTitulosClick(
  Sender: TObject);
var
  objForm : TfrmGeracaoBorderoRecebimento_titulos;
begin
  inherited;

  if (Trim(edtContaBancaria.Text) = '') then
  begin
      MensagemAlerta('Para exibir os titulos selecione uma conta portadora.');
      edtContaBancaria.SetFocus;
      abort;
  end;

  qryContaBancaria.Close;
  PosicionaQuery(qryContaBancaria,edtContaBancaria.Text);

  if (qryContaBancarianCdTabTipoModeloImpBoleto.Value = 2) then
  begin
      MensagemAlerta('Esta Conta Banc�ria Emite Boleto em Formul�rio do Banco e n�o deve gerar Arquivos de Remessa.');
      abort;
  end;

  objForm := TfrmGeracaoBorderoRecebimento_titulos.Create(nil);

  objForm.cmdPreparaTemp.Execute;

  objForm.qryPopulaTemp.Close;

  objForm.qryPopulaTemp.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva;
  objForm.qryPopulaTemp.Parameters.ParamByName('nCdTerceiro').Value       := frmMenu.ConvInteiro(edtTerceiro.Text);
  objForm.qryPopulaTemp.Parameters.ParamByName('nCdFormaPagto').Value     := frmMenu.ConvInteiro(edtFormaPagto.Text);
  objForm.qryPopulaTemp.Parameters.ParamByName('dDtEmissaoInicial').Value := frmMenu.ConvData(MaskEditEmissaoInicial.Text) ;
  objForm.qryPopulaTemp.Parameters.ParamByName('dDtEmissaoFinal').Value   := frmMenu.ConvData(MaskEditEmissaoFinal.Text) ;
  objForm.qryPopulaTemp.Parameters.ParamByName('dDtVenctoInicial').Value  := frmMenu.ConvData(MaskEditVenctoInicial.Text) ;
  objForm.qryPopulaTemp.Parameters.ParamByName('dDtVenctoFinal').Value    := frmMenu.ConvData(MaskEditVenctoFinal.Text) ;
  objForm.qryPopulaTemp.Parameters.ParamByName('cNrNf').Value             := MasKEditNumeroNF.Text;
  objForm.qryPopulaTemp.Parameters.ParamByName('cFlgEmBordero').Value     := BoolToInt(CheckBox2.Checked);

  objForm.qryPopulaTemp.ExecSQL;

  objForm.qryTemp.Close;
  objForm.qryTemp.Open;

  if (objForm.qryTemp.RecordCount = 0) then
  begin
      MensagemAlerta('N�o foi encontrado nenhum resultado para os crit�rios utilizados.');
      abort;
  end;

  objForm.nCdContaBancaria := qryContaBancarianCdContaBancaria.Value;

  showForm(objForm,True);

  edtContaBancaria.setFocus;

end;

function TfrmGeracaoBorderoRecebimento.BoolToInt(Valor: Boolean): integer;
begin
    if (Valor) then
        Result := 1
    else Result := 0;
end;

procedure TfrmGeracaoBorderoRecebimento.FormShow(Sender: TObject);
begin
  inherited;

  edtFormaPagto.Text := frmMenu.LeParametro('FORMAPAGTOBOL');
  edtFormaPagto.PosicionaQuery;
end;

initialization
    RegisterClass(TfrmGeracaoBorderoRecebimento) ;

end.
