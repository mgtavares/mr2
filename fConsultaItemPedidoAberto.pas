unit fConsultaItemPedidoAberto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, GridsEh, DBGridEh, DB, ADODB, cxPC, cxControls,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmConsultaItemPedidoAberto = class(TfrmProcesso_Padrao)
    qryPedido: TADOQuery;
    DataSource1: TDataSource;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidodDtPrevEntIni: TDateTimeField;
    qryPedidodDtPrevEntFim: TDateTimeField;
    qryPedidocNrPedTerceiro: TStringField;
    qryPedidocNmContato: TStringField;
    qryItemPedido: TADOQuery;
    DataSource2: TDataSource;
    qryItemPedidonCdItemPedido: TAutoIncField;
    qryItemPedidocNmTipoItemPed: TStringField;
    qryItemPedidonCdProduto: TIntegerField;
    qryItemPedidocNmItem: TStringField;
    qryItemPedidonQtde: TBCDField;
    qryPedidocOBS: TStringField;
    qryItemPedidocSiglaUnidadeMedida: TStringField;
    qryItemPedidocReferencia: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cxPageControl2: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryItemPedidocCdProduto: TStringField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidocNmFantasia: TStringField;
    qryPedidocNmGrupoEconomico: TStringField;
    qryItemPedidonQtdeVinculada: TBCDField;
    qryPedidocNmTipoPedido: TStringField;
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    Label1: TLabel;
    qryItemPedidocNmCor: TStringField;
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure qryPedidoAfterScroll(DataSet: TDataSet);
    procedure ToolButton2Click(Sender: TObject);
    procedure DBGridEh2DblClick(Sender: TObject);
    procedure DBGridEh2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGridEh2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure Edit1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTerceiro    : Integer ;
    nCdTipoReceb   : Integer ;
    nCdLoja        : Integer ;
    bSelecionado   : Boolean ;
    nCdRecebimento : Integer ;
    nCdPedido      : integer ;
    nCdProduto     : integer ;
  end;

var
  frmConsultaItemPedidoAberto: TfrmConsultaItemPedidoAberto;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmConsultaItemPedidoAberto.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.OptionsEh := [dghFixed3D,dghHighlightFocus,dghClearSelection] ;
  DBGridEh2.OptionsEh := [dghFixed3D,dghHighlightFocus,dghClearSelection] ;

  qryPedido.Close ;
  qryPedido.Parameters.ParamByName('nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva ;
  qryPedido.Parameters.ParamByName('nCdLoja').Value      := nCdLoja      ;
  qryPedido.Parameters.ParamByName('nCdTerceiro').Value  := nCdTerceiro  ;
  qryPedido.Parameters.ParamByName('nCdTipoReceb').Value := nCdTipoReceb ;
  qryPedido.Parameters.ParamByName('nCdUsuario').Value   := frmMenu.nCdUsuarioLogado;
  qryPedido.Parameters.ParamByName('nCdProduto').Value   := nCdProduto;
  qryPedido.Open ;

  bSelecionado := False ;

  if qryPedido.Eof then
  begin

      if (frmMenu.nCdLojaAtiva = 0) then
          MensagemAlerta('Nenhuma previs�o de recebimento para este terceiro ou nenhuma entrega para os locais de estoque que voc� pode movimentar.')
      else MensagemAlerta('Nenhuma previs�o de recebimento para este fornecedor nesta loja.') ;
      
      close ;
      exit ;
      
  end ;

  DBGridEh1.SetFocus ;

  if (nCdPedido > 0) then
  begin

      qryPedido.First ;

      while not qryPedido.Eof do
      begin
          if (qryPedidonCdPedido.Value = nCdPedido) then
          begin
              break;
          end ;

          qryPedido.Next ;
      end ;

      if not qryPedido.eof then
      begin

          qryItemPedido.Parameters.ParamByName('nCdRecebimento').Value := nCdRecebimento ;
          qryItemPedido.Parameters.ParamByName('nCdProduto').Value     := nCdProduto ;
          PosicionaQuery(qryItemPedido, qryPedidonCdPedido.asString) ;

      end
      else qryPedido.First ;

      DBGridEh2.SetFocus ;
  end ;

end;

procedure TfrmConsultaItemPedidoAberto.DBGridEh1DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
var
  R : TRect;
begin

  R:=Rect;
  Dec(R.Bottom,2);

  if Column.Field.Name  = 'qryPedidocOBS' then
  begin

    if not (gdSelected in State) then
      DBGridEh1.Canvas.FillRect(Rect);

    DBGridEh1.Canvas.TextRect(R,R.Left,R.Top,qryPedidocOBS.AsString);
   end;

end;

procedure TfrmConsultaItemPedidoAberto.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  if (qryPedido.Active) then
  begin
      if (qryPedidonCdPedido.Value > 0) then
      begin
          qryItemPedido.Parameters.ParamByName('nCdRecebimento').Value := nCdRecebimento ;
          qryItemPedido.Parameters.ParamByName('nCdProduto').Value     := nCdProduto ;
          PosicionaQuery(qryItemPedido, qryPedidonCdPedido.asString) ;
      end ;
  end ;
end;

procedure TfrmConsultaItemPedidoAberto.qryPedidoAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  qryItemPedido.Close ;
end;

procedure TfrmConsultaItemPedidoAberto.ToolButton2Click(Sender: TObject);
begin
  qryItemPedido.Close ;
  qryPedido.Close ;

  inherited;
end;

procedure TfrmConsultaItemPedidoAberto.DBGridEh2DblClick(Sender: TObject);
begin
  inherited;

  bSelecionado := True ;
  nCdProduto   := 0 ;
  close ;

end;

procedure TfrmConsultaItemPedidoAberto.DBGridEh2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = 13) then
  begin
      bSelecionado := True ;
      nCdProduto   := 0 ;
      close ;
  end ;
  
end;

procedure TfrmConsultaItemPedidoAberto.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  
  if (Key = 13) then
  begin

      if (qryPedido.Active) then
      begin

          if (qryPedidonCdPedido.Value > 0) then
          begin
              qryItemPedido.Parameters.ParamByName('nCdRecebimento').Value := nCdRecebimento ;
              qryItemPedido.Parameters.ParamByName('nCdProduto').Value     := nCdProduto ;
              PosicionaQuery(qryItemPedido, qryPedidonCdPedido.asString) ;
              DBGridEh2.SetFocus ;
          end ;

      end ;

  end ;

end;

procedure TfrmConsultaItemPedidoAberto.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
   i : integer ;
begin
  i := 0 ;

end;

procedure TfrmConsultaItemPedidoAberto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  nCdProduto := 0 ;
  inherited;

end;

procedure TfrmConsultaItemPedidoAberto.DBGridEh2DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (qryItemPedidonQtdeVinculada.Value > 0) then
  begin

      if (Column = DBGridEh2.Columns[DBGridEh2.FieldColumns['nQtdeVinculada'].Index]) then
      begin
          DBGridEh2.Canvas.Brush.Color := clYellow ;
          DBGridEh2.Canvas.Font.Color  := clBlack;
          DBGridEh2.Canvas.FillRect(Rect);
          DBGridEh2.DefaultDrawDataCell(Rect,Column.Field,State);
      end;

  end ;

end;

procedure TfrmConsultaItemPedidoAberto.Edit1Change(Sender: TObject);
begin
  inherited;

  if not (qryItemPedido.Active) then
      exit;

  qryItemPedido.Filtered := False;

  if (Trim(edit1.Text) <> '') then
      qryItemPedido.Filter := 'cReferencia LIKE ' + #39 + '%' + edit1.Text + '%' + #39
  else qryItemPedido.Filter := '';

  qryItemPedido.Filtered := True;
end;

end.
