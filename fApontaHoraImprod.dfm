inherited frmApontaHoraImprod: TfrmApontaHoraImprod
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'Apontamento Hora Improdutiva'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1152
    Height = 789
  end
  object Label1: TLabel [1]
    Left = 83
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 33
    Top = 61
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro Produtivo'
  end
  object Label3: TLabel [3]
    Left = 75
    Top = 84
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'M'#225'quina'
  end
  object Label4: TLabel [4]
    Left = 71
    Top = 108
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Operador'
  end
  object Label5: TLabel [5]
    Left = 14
    Top = 132
    Width = 107
    Height = 13
    Alignment = taRightJustify
    Caption = 'Motivo Hora Improd.'
  end
  object Label6: TLabel [6]
    Left = 64
    Top = 155
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Inicial'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 229
    Top = 155
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Hora Inicial'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 69
    Top = 178
    Width = 52
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Final'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 234
    Top = 178
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'Hora Final'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [10]
    Left = 59
    Top = 201
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tempo Total'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [11]
    Left = 734
    Top = 38
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio'
  end
  object Label12: TLabel [12]
    Left = 676
    Top = 61
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Apontamento'
    FocusControl = DBEdit12
  end
  object Label13: TLabel [13]
    Left = 61
    Top = 221
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observa'#231#227'o'
    FocusControl = DBMemo1
  end
  inherited ToolBar2: TToolBar
    Width = 1152
  end
  object DBEdit1: TDBEdit [15]
    Tag = 1
    Left = 126
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdHoraImprodutivaPCP'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit6: TDBEdit [16]
    Left = 126
    Top = 149
    Width = 89
    Height = 19
    DataField = 'dDtInicial'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit7: TDBEdit [17]
    Left = 290
    Top = 149
    Width = 91
    Height = 19
    DataField = 'cHoraInicial'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit7Exit
  end
  object DBEdit8: TDBEdit [18]
    Left = 126
    Top = 172
    Width = 89
    Height = 19
    DataField = 'dDtFinal'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit8Exit
  end
  object DBEdit9: TDBEdit [19]
    Left = 290
    Top = 172
    Width = 91
    Height = 19
    DataField = 'cHoraFinal'
    DataSource = dsMaster
    TabOrder = 5
    OnExit = DBEdit9Exit
  end
  object DBEdit10: TDBEdit [20]
    Left = 126
    Top = 195
    Width = 75
    Height = 19
    DataField = 'nTempoTotal'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit12: TDBEdit [21]
    Tag = 1
    Left = 778
    Top = 55
    Width = 143
    Height = 19
    DataField = 'dDtApontamento'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBMemo1: TDBMemo [22]
    Left = 126
    Top = 219
    Width = 539
    Height = 206
    DataField = 'cOBS'
    DataSource = dsMaster
    TabOrder = 8
  end
  object edtCentroProdutivo: TER2LookupDBEdit [23]
    Left = 126
    Top = 55
    Width = 65
    Height = 19
    DataField = 'nCdCentroProdutivo'
    DataSource = dsMaster
    TabOrder = 9
    CodigoLookup = 738
    QueryLookup = qryCentroProdutivo
  end
  object edtTipoMaquina: TER2LookupDBEdit [24]
    Left = 126
    Top = 78
    Width = 65
    Height = 19
    DataField = 'nCdTipoMaquinaPCP'
    DataSource = dsMaster
    TabOrder = 10
    CodigoLookup = 209
    QueryLookup = qryTipoMaquina
  end
  object edtOperadorPCP: TER2LookupDBEdit [25]
    Left = 126
    Top = 102
    Width = 65
    Height = 19
    DataField = 'nCdOperadorPCP'
    DataSource = dsMaster
    TabOrder = 11
    CodigoLookup = 1022
    QueryLookup = qryOperadorPCP
  end
  object DBEdit2: TDBEdit [26]
    Tag = 1
    Left = 194
    Top = 78
    Width = 471
    Height = 19
    DataField = 'cNmTipoMaquinaPCP'
    DataSource = dsTipoMaquina
    TabOrder = 12
  end
  object DBEdit14: TDBEdit [27]
    Tag = 1
    Left = 194
    Top = 102
    Width = 471
    Height = 19
    DataField = 'cNmOperadorPCP'
    DataSource = dsOperadorPCP
    TabOrder = 13
  end
  object edtMotivoHoraImprod: TER2LookupDBEdit [28]
    Left = 126
    Top = 126
    Width = 65
    Height = 19
    DataField = 'nCdMotivoHoraImprodutiva'
    DataSource = dsMaster
    TabOrder = 14
    CodigoLookup = 1024
    QueryLookup = qryMotivoHoraImprod
  end
  object edtUsuarioApontamento: TER2LookupDBEdit [29]
    Tag = 1
    Left = 778
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdUsuario'
    DataSource = dsMaster
    TabOrder = 15
    CodigoLookup = 5
    QueryLookup = qryUsuario
  end
  object DBEdit16: TDBEdit [30]
    Tag = 1
    Left = 845
    Top = 32
    Width = 132
    Height = 19
    DataField = 'cNmUsuario'
    DataSource = dsUsuario
    TabOrder = 16
  end
  object DBEdit3: TDBEdit [31]
    Tag = 1
    Left = 194
    Top = 55
    Width = 471
    Height = 19
    DataField = 'cNmCentroProdutivo'
    DataSource = dsCentroProdutivo
    TabOrder = 17
  end
  object DBEdit4: TDBEdit [32]
    Tag = 1
    Left = 194
    Top = 126
    Width = 471
    Height = 19
    DataField = 'cNmMotivoHoraImprodutiva'
    DataSource = dsMotivoHoraImprod
    TabOrder = 18
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM HoraImprodutivaPCP'
      'WHERE nCdHoraImprodutivaPCP = :nPK'
      '')
    Top = 184
    object qryMasternCdHoraImprodutivaPCP: TIntegerField
      FieldName = 'nCdHoraImprodutivaPCP'
    end
    object qryMasternCdCentroProdutivo: TIntegerField
      FieldName = 'nCdCentroProdutivo'
    end
    object qryMasternCdTipoMaquinaPCP: TIntegerField
      FieldName = 'nCdTipoMaquinaPCP'
    end
    object qryMasternCdOperadorPCP: TIntegerField
      FieldName = 'nCdOperadorPCP'
    end
    object qryMasternCdMotivoHoraImprodutiva: TIntegerField
      FieldName = 'nCdMotivoHoraImprodutiva'
    end
    object qryMasterdDtInicial: TDateTimeField
      FieldName = 'dDtInicial'
      EditMask = '!99/99/0000;1;_'
    end
    object qryMastercHoraInicial: TStringField
      FieldName = 'cHoraInicial'
      EditMask = '##:##;1;_'
      FixedChar = True
      Size = 7
    end
    object qryMasterdDtFinal: TDateTimeField
      FieldName = 'dDtFinal'
      EditMask = '!99/99/0000;1;_'
    end
    object qryMastercHoraFinal: TStringField
      FieldName = 'cHoraFinal'
      EditMask = '##:##'
      FixedChar = True
      Size = 7
    end
    object qryMasternTempoTotal: TBCDField
      FieldName = 'nTempoTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryMasterdDtApontamento: TDateTimeField
      FieldName = 'dDtApontamento'
    end
    object qryMastercOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
  end
  inherited dsMaster: TDataSource
    Top = 184
  end
  inherited qryID: TADOQuery
    Top = 184
  end
  inherited usp_ProximoID: TADOStoredProc
    Top = 240
  end
  inherited qryStat: TADOQuery
    Top = 256
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Top = 296
  end
  inherited ImageList1: TImageList
    Top = 280
  end
  object qryCentroProdutivo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmCentroProdutivo'
      'FROM CentroProdutivo'
      'WHERE nCdCentroProdutivo = :nPK')
    Left = 688
    Top = 88
    object qryCentroProdutivocNmCentroProdutivo: TStringField
      FieldName = 'cNmCentroProdutivo'
      Size = 50
    end
  end
  object dsCentroProdutivo: TDataSource
    DataSet = qryCentroProdutivo
    Left = 720
    Top = 88
  end
  object qryTipoMaquina: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTipoMaquinaPCP'
      'FROM TipoMaquinaPCP'
      'WHERE nCdTipoMaquinaPCP = :nPK'
      '')
    Left = 688
    Top = 120
    object qryTipoMaquinacNmTipoMaquinaPCP: TStringField
      FieldName = 'cNmTipoMaquinaPCP'
      Size = 50
    end
  end
  object dsTipoMaquina: TDataSource
    DataSet = qryTipoMaquina
    Left = 720
    Top = 120
  end
  object qryOperadorPCP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmOperadorPCP'
      'FROM OperadorPCP'
      'WHERE nCdOperadorPCP = :nPK'
      ''
      '')
    Left = 688
    Top = 152
    object qryOperadorPCPcNmOperadorPCP: TStringField
      FieldName = 'cNmOperadorPCP'
      Size = 50
    end
  end
  object dsOperadorPCP: TDataSource
    DataSet = qryOperadorPCP
    Left = 720
    Top = 152
  end
  object qryMotivoHoraImprod: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmMotivoHoraImprodutiva'
      'FROM MotivoHoraImprodutiva'
      'WHERE nCdMotivoHoraImprodutiva = :nPK'
      ''
      ''
      '')
    Left = 688
    Top = 184
    object qryMotivoHoraImprodcNmMotivoHoraImprodutiva: TStringField
      FieldName = 'cNmMotivoHoraImprodutiva'
      Size = 50
    end
  end
  object dsMotivoHoraImprod: TDataSource
    DataSet = qryMotivoHoraImprod
    Left = 720
    Top = 184
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmUsuario '
      '   FROM Usuario'
      'WHERE nCdUsuario = :nPk')
    Left = 688
    Top = 216
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsUsuario: TDataSource
    DataSet = qryUsuario
    Left = 720
    Top = 216
  end
  object qryCalculaDifTempo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cDtInicial'
        Size = -1
        Value = Null
      end
      item
        Name = 'cDtFinal'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @cDtInicial   Varchar(20)'
      'DECLARE @cDtFinal     Varchar(20)'
      ''
      'DECLARE @dDtInicial   Datetime'
      'DECLARE @dDtFinal     Datetime'
      ''
      'SET @cDtInicial  = :cDtInicial'
      'SET @cDtFinal    = :cDtFinal'
      ''
      'SET @dDtInicial = Convert(Datetime,@cDtInicial,103)'
      'SET @dDtFinal   = Convert(Datetime,@cDtFinal,103)'
      ''
      'SELECT Convert(DECIMAL(12,2)'
      
        '      ,Convert(VARCHAR,DateDiff(Mi,@dDtInicial,@dDtFinal)/60) + ' +
        #39'.'#39' + '
      
        '       Convert(VARCHAR,datediff(Mi,@dDtInicial,@dDtFinal)%60)) a' +
        's nTempoTotal'
      ''
      ''
      '')
    Left = 688
    Top = 248
    object qryCalculaDifTemponTempoTotal: TBCDField
      FieldName = 'nTempoTotal'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
  end
end
