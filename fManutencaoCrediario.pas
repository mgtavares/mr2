unit fManutencaoCrediario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, Grids, DBGrids,
  GridsEh, DBGridEh, cxLookAndFeelPainters, cxButtons, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmManutencaoCrediario = class(TfrmCadastro_Padrao)
    qryInfParcelas: TADOQuery;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBEdit18: TDBEdit;
    Label18: TLabel;
    dsInfParcelas: TDataSource;
    qryRestricoesLiberadas: TADOQuery;
    qryMasternCdCrediario: TIntegerField;
    qryMastercNmLojaVenda: TStringField;
    qryMasterdDtVenda: TDateTimeField;
    qryMasternCdLanctoFin: TIntegerField;
    qryMasternCdConta: TStringField;
    qryMasterdDtUltVencto: TDateTimeField;
    qryMastercNmStatusDocto: TStringField;
    qryMasterdDtEstorno: TDateTimeField;
    qryMastercNmUsuarioEstorno: TStringField;
    qryMastercMotivoEstorno: TStringField;
    qryMasternCdLanctoFinEstorno: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNmTerceiro: TStringField;
    qryMasternValCrediario: TBCDField;
    qryMasternValEntrada: TBCDField;
    qryMasteriParcelas: TIntegerField;
    qryMastercFlgRenegociacao: TIntegerField;
    DBEdit8: TDBEdit;
    qryRestricoesLiberadascNmTabTipoRestricaoVenda: TStringField;
    dsRestricoesLiberadas: TDataSource;
    qryMasterRenegociacao: TStringField;
    qryRestricoesLiberadascJustificativa: TStringField;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    qryInfParcelasnCdTitulo: TIntegerField;
    qryInfParcelascNrTit: TStringField;
    qryInfParcelasnCdLanctoFin: TIntegerField;
    qryInfParcelasiParcela: TIntegerField;
    qryInfParcelasdDtVenc: TDateTimeField;
    qryInfParcelasdDtLiq: TDateTimeField;
    qryInfParcelasnValTit: TBCDField;
    qryInfParcelasnValLiq: TBCDField;
    qryInfParcelasnValJuro: TBCDField;
    qryInfParcelasnValDesconto: TBCDField;
    qryInfParcelasnValAbatimento: TBCDField;
    qryInfParcelasnSaldoTit: TBCDField;
    qryInfParcelasnCdConta: TStringField;
    qryInfParcelasnCdLoja: TStringField;
    qryMastercIdExterno: TStringField;
    Label8: TLabel;
    DBEdit17: TDBEdit;
    qryRestricoesLiberadascNmLogin: TStringField;
    cxButton1: TcxButton;
    GroupBox1: TGroupBox;
    Memo1: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterCalcFields(DataSet: TDataSet);
    procedure cxButton1Click(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryRestricoesLiberadasAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmManutencaoCrediario: TfrmManutencaoCrediario;

implementation

uses fConsultaVendaProduto_Dados;

{$R *.dfm}

procedure TfrmManutencaoCrediario.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CREDIARIO' ;
  nCdTabelaSistema  := 66 ;
  nCdConsultaPadrao := 705 ;
  bCodigoAutomatico := False ;
end;

procedure TfrmManutencaoCrediario.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryInfParcelas.Close;
  qryInfParcelas.Parameters.ParamByName('nCdCrediario').Value := qryMasternCdCrediario.Value;
  qryInfParcelas.Open;
  
  qryRestricoesLiberadas.Close;
  qryRestricoesLiberadas.Parameters.ParamByName('nCdCrediario').Value := qryMasternCdCrediario.Value;
  qryRestricoesLiberadas.Open;
end;

procedure TfrmManutencaoCrediario.qryMasterCalcFields(DataSet: TDataSet);
begin
  inherited;
  if(qryMastercFlgRenegociacao.Value = 1) then
      qryMasterRenegociacao.Value := 'SIM';
end;

procedure TfrmManutencaoCrediario.cxButton1Click(Sender: TObject);
var
  objForm : TfrmConsultaVendaProduto_Dados ;
begin

  if (qryMaster.Active) and (qryMaster.RecordCount > 0) then
  begin

      if (qryMasterRenegociacao.Value = 'SIM') then
      begin
          MensagemAlerta('Este crediário foi gerado por renegociação.') ;
          exit ;
      end ;

      objForm := TfrmConsultaVendaProduto_Dados.Create(Self) ;

      PosicionaQuery(objForm.qryItemPedido, qryMasternCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryCondicao  , qryMasternCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryPrestacoes, qryMasternCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryCheques   , qryMasternCdLanctoFin.AsString) ;

      showForm( objForm , TRUE ) ;

  end ;

end;

procedure TfrmManutencaoCrediario.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryInfParcelas.Close;
  qryRestricoesLiberadas.Close;
  
end;

procedure TfrmManutencaoCrediario.qryRestricoesLiberadasAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  
  Memo1.Text := '';
  Memo1.Text := qryRestricoesLiberadascJustificativa.Value;
end;

initialization
    RegisterClass(TfrmManutencaoCrediario);
end.
