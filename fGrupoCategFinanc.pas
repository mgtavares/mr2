unit fGrupoCategFinanc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ADODB, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DBCtrls, ImgList;

type
  TfrmGrupoCategFinanc = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoCategFinanc: TIntegerField;
    qryMastercNmGrupoCategFinanc: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoCategFinanc: TfrmGrupoCategFinanc;

implementation

{$R *.dfm}

procedure TfrmGrupoCategFinanc.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmGrupoCategFinanc.FormShow(Sender: TObject);
begin
  cNmTabelaMaster   := 'GRUPOCATEGFINANC' ;
  nCdTabelaSistema  := 9 ;
  nCdConsultaPadrao := 12 ;
  inherited;

end;

initialization
    RegisterClass(tFrmGrupoCategFinanc) ;

end.
