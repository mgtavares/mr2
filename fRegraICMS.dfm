inherited frmRegraICMS: TfrmRegraICMS
  Left = 184
  Top = 23
  Width = 1152
  Height = 683
  Caption = 'Regra ICMS'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1136
    Height = 616
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    inherited ToolButton1: TToolButton
      Enabled = False
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 616
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsRegraICMS
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdRegraICMS'
        Footers = <>
        ReadOnly = True
        Width = 49
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cUFOrigem'
        Footers = <>
        Width = 44
      end
      item
        EditButtons = <>
        FieldName = 'cUFDestino'
        Footers = <>
        Width = 44
      end
      item
        EditButtons = <>
        FieldName = 'nCdGrupoImposto'
        Footers = <>
        Width = 33
      end
      item
        EditButtons = <>
        FieldName = 'cNmGrupoImposto'
        Footers = <>
        ReadOnly = True
        Width = 200
      end
      item
        EditButtons = <>
        FieldName = 'nCdTipoTributacaoICMS'
        Footers = <>
        Title.Caption = 'ICMS Pr'#243'prio|CST CSOSN'
        Width = 53
      end
      item
        EditButtons = <>
        FieldName = 'nPercBC'
        Footers = <>
        Title.Caption = 'ICMS Pr'#243'prio|% BC'
        Width = 53
      end
      item
        EditButtons = <>
        FieldName = 'nAliqICMSInterna'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'nAliqICMS'
        Footers = <>
        Width = 76
      end
      item
        EditButtons = <>
        FieldName = 'nAliqICMSInternaDestino'
        Footers = <>
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'nAliqICMSPartOrig'
        Footers = <>
        Title.Caption = 'ICMS Partilhado|% Part. Origem'
        Title.Hint = 
          'Percentual provis'#243'rio de partilha do ICMS Interestadual na UF de' +
          ' origem.'
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'nAliqICMSPartDest'
        Footers = <>
        Title.Caption = 'ICMS Partilhado|% Part. Destino'
        Title.Hint = 
          'Percentual provis'#243'rio de partilha do ICMS Interestadual na UF de' +
          ' destino.'
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'nAliqICMSInter'
        Footers = <>
        Title.Caption = 'ICMS Partilhado|Al'#237'q. Interes.'
        Title.Hint = 'Al'#237'quota interestadual.'
        Title.ToolTips = True
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'nAliqICMSDest'
        Footers = <>
        Title.Caption = 'ICMS Partilhado|Al'#237'q. Destino'
        Title.Hint = 'Al'#237'quota interna da UF destino.'
        Title.ToolTips = True
        ToolTips = True
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'nPercIVA'
        Footers = <>
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'nPercCargaMediaST'
        Footers = <>
        Width = 53
      end
      item
        EditButtons = <>
        FieldName = 'nPercBCIVA'
        Footers = <>
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'cCdSTIPI'
        Footers = <>
        Title.Caption = 'IPI|CST'
        Width = 29
      end
      item
        EditButtons = <>
        FieldName = 'nPercAliqIPI'
        Footers = <>
        Title.Caption = 'IPI|% Al'#237'q.'
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'nPercBaseCalcIPI'
        Footers = <>
        Title.Caption = 'IPI|% BC'
        Width = 54
      end
      item
        EditButtons = <>
        FieldName = 'nPercIncentivoFiscal'
        Footers = <>
        Width = 53
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'cFlgST'
        Footers = <>
        KeyList.Strings = (
          'S'
          'N')
        Width = 69
      end
      item
        EditButtons = <>
        FieldName = 'nCdTipoPedido'
        Footers = <>
        Width = 33
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoPedido'
        Footers = <>
        ReadOnly = True
        Width = 200
      end
      item
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footers = <>
        Width = 40
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        ReadOnly = True
        Width = 200
      end
      item
        EditButtons = <>
        FieldName = 'cMensagemNF'
        Footers = <>
        Width = 200
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 328
    Top = 336
  end
  object qryRegraICMS: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryRegraICMSBeforePost
    OnCalcFields = qryRegraICMSCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM RegraICMS'
      'WHERE nCdEmpresa = :nPK'
      
        'ORDER BY cUFOrigem, cUFDestino, nCdGrupoImposto, nCdTipoPedido, ' +
        'nCdTerceiro')
    Left = 360
    Top = 336
    object qryRegraICMSnCdRegraICMS: TIntegerField
      DisplayLabel = 'Regra|ID'
      FieldName = 'nCdRegraICMS'
    end
    object qryRegraICMSnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryRegraICMScUFOrigem: TStringField
      DisplayLabel = 'Estado|Origem'
      FieldName = 'cUFOrigem'
      FixedChar = True
      Size = 2
    end
    object qryRegraICMScUFDestino: TStringField
      DisplayLabel = 'Estado|Destino'
      FieldName = 'cUFDestino'
      FixedChar = True
      Size = 2
    end
    object qryRegraICMSnCdGrupoImposto: TIntegerField
      DisplayLabel = 'Grupo Imposto|C'#243'd'
      FieldName = 'nCdGrupoImposto'
    end
    object qryRegraICMScNmGrupoImposto: TStringField
      DisplayLabel = 'Grupo Imposto|Descri'#231#227'o'
      FieldKind = fkLookup
      FieldName = 'cNmGrupoImposto'
      LookupDataSet = qryGrupoImposto
      LookupKeyFields = 'nCdGrupoImposto'
      LookupResultField = 'cNmGrupoImposto'
      KeyFields = 'nCdGrupoImposto'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryRegraICMSnAliqICMS: TBCDField
      DisplayLabel = 'ICMS Pr'#243'prio|Al'#237'quota ICMS|Interestadual'
      FieldName = 'nAliqICMS'
      Precision = 4
      Size = 2
    end
    object qryRegraICMSnPercBC: TBCDField
      DisplayLabel = 'ICMS|% BC'
      FieldName = 'nPercBC'
      Precision = 5
      Size = 2
    end
    object qryRegraICMSnPercIVA: TBCDField
      DisplayLabel = 'ICMS ST|MVA'
      FieldName = 'nPercIVA'
      Precision = 5
      Size = 2
    end
    object qryRegraICMSnPercBCIVA: TBCDField
      DisplayLabel = 'ICMS ST|% BC'
      FieldName = 'nPercBCIVA'
      Precision = 12
      Size = 2
    end
    object qryRegraICMScFlgST: TStringField
      DisplayLabel = 'Substitui'#231#227'o|Tribut'#225'ria'
      FieldName = 'cFlgST'
      FixedChar = True
      Size = 1
    end
    object qryRegraICMSnCdTipoPedido: TIntegerField
      DisplayLabel = 'Tipo Pedido|C'#243'd'
      FieldName = 'nCdTipoPedido'
    end
    object qryRegraICMScNmTipoPedido: TStringField
      DisplayLabel = 'Tipo Pedido|Descri'#231#227'o'
      FieldKind = fkLookup
      FieldName = 'cNmTipoPedido'
      LookupDataSet = qryTipoPedido
      LookupKeyFields = 'nCdTipoPedido'
      LookupResultField = 'cNmTipoPedido'
      KeyFields = 'nCdTipoPedido'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryRegraICMSnCdTerceiro: TIntegerField
      DisplayLabel = 'Terceiro|C'#243'd'
      FieldName = 'nCdTerceiro'
    end
    object qryRegraICMScNmTerceiro: TStringField
      DisplayLabel = 'Terceiro|Nome Terceiro'
      FieldKind = fkCalculated
      FieldName = 'cNmTerceiro'
      Size = 50
      Calculated = True
    end
    object qryRegraICMScMensagemNF: TStringField
      DisplayLabel = 'Mensagem Fiscal'
      FieldName = 'cMensagemNF'
      Size = 150
    end
    object qryRegraICMSnAliqICMSInterna: TBCDField
      DisplayLabel = 'ICMS Pr'#243'prio|Al'#237'quota ICMS|Interna Origem'
      FieldName = 'nAliqICMSInterna'
      Precision = 5
      Size = 2
    end
    object qryRegraICMSnPercIncentivoFiscal: TBCDField
      DisplayLabel = 'Incentivo|Fiscal'
      FieldName = 'nPercIncentivoFiscal'
      Precision = 5
      Size = 2
    end
    object qryRegraICMSnCdTipoTributacaoICMS: TStringField
      DisplayLabel = 'ICMS Pr'#243'prio|CST'
      DisplayWidth = 3
      FieldName = 'nCdTipoTributacaoICMS'
      FixedChar = True
      Size = 3
    end
    object qryRegraICMSnPercAliqIPI: TBCDField
      FieldName = 'nPercAliqIPI'
      Precision = 12
      Size = 2
    end
    object qryRegraICMSnPercBaseCalcIPI: TBCDField
      FieldName = 'nPercBaseCalcIPI'
      Precision = 12
      Size = 2
    end
    object qryRegraICMScCdSTIPI: TStringField
      FieldName = 'cCdSTIPI'
      FixedChar = True
      Size = 2
    end
    object qryRegraICMSnPercCargaMediaST: TBCDField
      DisplayLabel = 'ICMS ST|% Carga M'#233'dia'
      FieldName = 'nPercCargaMediaST'
      Precision = 12
      Size = 2
    end
    object qryRegraICMSnAliqICMSInternaDestino: TBCDField
      DisplayLabel = 'ICMS Pr'#243'prio|Al'#237'quota ICMS|Interna Destino'
      FieldName = 'nAliqICMSInternaDestino'
      Precision = 12
      Size = 2
    end
    object qryRegraICMSnAliqICMSPartOrig: TBCDField
      FieldName = 'nAliqICMSPartOrig'
      Precision = 5
      Size = 2
    end
    object qryRegraICMSnAliqICMSPartDest: TBCDField
      FieldName = 'nAliqICMSPartDest'
      Precision = 5
      Size = 2
    end
    object qryRegraICMSnAliqICMSInter: TBCDField
      FieldName = 'nAliqICMSInter'
      Precision = 5
      Size = 2
    end
    object qryRegraICMSnAliqICMSDest: TBCDField
      FieldName = 'nAliqICMSDest'
      Precision = 5
      Size = 2
    end
  end
  object dsRegraICMS: TDataSource
    DataSet = qryRegraICMS
    Left = 360
    Top = 368
  end
  object qryGrupoImposto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdGrupoImposto, cNmGrupoImposto'
      'FROM GrupoImposto')
    Left = 392
    Top = 336
    object qryGrupoImpostonCdGrupoImposto: TIntegerField
      FieldName = 'nCdGrupoImposto'
    end
    object qryGrupoImpostocNmGrupoImposto: TStringField
      FieldName = 'cNmGrupoImposto'
      Size = 35
    end
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdTipoPedido'
      ',cNmTipoPedido'
      'FROM TipoPedido')
    Left = 392
    Top = 368
    object qryTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 424
    Top = 336
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryTipoTributacaoICMS: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoTributacaoICMS'
      'WHERE nCdTipoTributacaoICMS = :nPK')
    Left = 424
    Top = 368
    object qryTipoTributacaoICMSnCdTipoTributacaoICMS: TAutoIncField
      FieldName = 'nCdTipoTributacaoICMS'
      ReadOnly = True
    end
    object qryTipoTributacaoICMScCdST: TStringField
      DisplayWidth = 3
      FieldName = 'cCdST'
      FixedChar = True
      Size = 3
    end
    object qryTipoTributacaoICMScNmTipoTributacaoICMS: TStringField
      FieldName = 'cNmTipoTributacaoICMS'
      Size = 150
    end
  end
  object qryTipoTributacaoIPI: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoTributacaoIPI'
      'WHERE nCdTipoTributacaoIPI = :nPK')
    Left = 456
    Top = 336
    object qryTipoTributacaoIPInCdTipoTributacaoIPI: TIntegerField
      FieldName = 'nCdTipoTributacaoIPI'
    end
    object qryTipoTributacaoIPIcCdStIPI: TStringField
      FieldName = 'cCdStIPI'
      FixedChar = True
      Size = 2
    end
    object qryTipoTributacaoIPIcNmTipoTributacaoIPI: TStringField
      FieldName = 'cNmTipoTributacaoIPI'
      Size = 100
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 456
    Top = 368
  end
end
