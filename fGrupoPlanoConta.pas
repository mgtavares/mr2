unit fGrupoPlanoConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmGrupoPlanoConta = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoPlanoConta: TIntegerField;
    qryMastercNmGrupoPlanoConta: TStringField;
    qryMastercFlgExibeDRE: TIntegerField;
    qryMastercFlgRecDes: TStringField;
    qryMasteriSeqDRE: TIntegerField;
    qryMasternCdTipoResultadoDRE: TIntegerField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    qryTipoResultadoDRE: TADOQuery;
    qryTipoResultadoDREnCdTipoResultadoDRE: TIntegerField;
    qryTipoResultadoDREcNmTipoResultadoDRE: TStringField;
    DBEdit3: TDBEdit;
    DataSource1: TDataSource;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    DBEdit5: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    qryMastercMascaraGrupo: TStringField;
    Label6: TLabel;
    DBEdit7: TDBEdit;
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure DBEdit3Exit(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoPlanoConta: TfrmGrupoPlanoConta;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmGrupoPlanoConta.DBEdit3KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(702);

            If (nPK > 0) then
            begin
                qryMasternCdTipoResultadoDRE.Value := nPK ;
                PosicionaQuery(qryTipoResultadoDRE,qryMasternCdTipoResultadoDRE.AsString);
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmGrupoPlanoConta.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOPLANOCONTA' ;
  nCdTabelaSistema  := 23 ;
  nCdConsultaPadrao := 40 ;

end;

procedure TfrmGrupoPlanoConta.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qryTipoResultadoDRE.Close;
end;

procedure TfrmGrupoPlanoConta.DBEdit3Exit(Sender: TObject);
begin
  inherited;
  qryTipoResultadoDRE.Close;
  PosicionaQuery(qryTipoResultadoDRE,qryMasternCdTipoResultadoDRE.AsString);
end;

procedure TfrmGrupoPlanoConta.btIncluirClick(Sender: TObject);
begin
  inherited;
    DBEdit2.SetFocus;
end;

procedure TfrmGrupoPlanoConta.qryMasterBeforePost(DataSet: TDataSet);
begin

  if Trim(DBEdit2.Text) = '' then begin
      MensagemAlerta('Informe a descri��o do Grupo.');
      DBEdit2.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit3.Text) = '') or (DBEdit4.Text = '') then begin
      MensagemAlerta('Tipo de Resultado do DRE inv�lido');
      DBEdit3.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit6.Text) = '') or ((DBEdit6.Text <> 'R') and (DBEdit6.Text <> 'D')) then begin
      MensagemAlerta('Selecione R - Receita / D - Despesa');
      DBEdit6.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit5.Text) = '') then begin
      MensagemAlerta('Informe a Sequ�ncia de exibi��o no DRE');
      DBEdit5.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit7.Text) = '') then begin
      MensagemAlerta('Informe a M�scara do Grupo');
      DBEdit7.SetFocus;
      Abort;
  end;

  inherited;

end;

procedure TfrmGrupoPlanoConta.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryTipoResultadoDRE.Close;
end;

procedure TfrmGrupoPlanoConta.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryTipoResultadoDRE.Close;
  PosicionaQuery(qryTipoResultadoDRE,qryMasternCdTipoResultadoDRE.AsString);
end;

initialization
    RegisterClass(TfrmGrupoPlanoConta);

end.
