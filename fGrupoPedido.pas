unit fGrupoPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, DBCtrls, Mask, ImgList, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmGrupoPedido = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoPedido: TIntegerField;
    qryMastercNmGrupoPedido: TStringField;
    qryMastercFlgFaturar: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoPedido: TfrmGrupoPedido;

implementation

{$R *.dfm}

procedure TfrmGrupoPedido.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOPEDIDO' ;
  nCdTabelaSistema  := 37 ;
  nCdConsultaPadrao := 79 ;
end;

procedure TfrmGrupoPedido.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEDit2.SetFocus ;
end;

initialization
    RegisterClass(TfrmGrupoPedido) ;
    
end.
