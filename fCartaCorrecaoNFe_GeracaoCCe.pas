unit fCartaCorrecaoNFe_GeracaoCCe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, DB, ADODB, Mask, ER2Lookup, StdCtrls,
  DBCtrls, GridsEh, DBGridEh, cxPC, cxControls, DBCtrlsEh, Spin, Gauges,
  OleCtrls, SHDocVw;

type
  TfrmCartaCorrecaoNFe_GeracaoCCe = class(TfrmProcesso_Padrao)
    GroupBox2: TGroupBox;
    Label1: TLabel;
    qryDoctoFiscal: TADOQuery;
    qryDoctoFiscalnCdDoctoFiscal: TIntegerField;
    qryDoctoFiscaliNrDocto: TIntegerField;
    qryDoctoFiscalcSerie: TStringField;
    qryDoctoFiscaldDtEmissao: TDateTimeField;
    qryDoctoFiscalcChaveNFe: TStringField;
    qryDoctoFiscalcFlgEntSai: TStringField;
    qryDoctoFiscaldDtImpressao: TDateTimeField;
    qryDoctoFiscalnValTotal: TBCDField;
    DBEdit1: TDBEdit;
    dsDoctoFiscal: TDataSource;
    qryDoctoFiscalnCdTerceiro: TIntegerField;
    qryDoctoFiscalcNmTerceiro: TStringField;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    DBEdit8: TDBEdit;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit9: TDBEdit;
    Label9: TLabel;
    DBEdit10: TDBEdit;
    GroupBox1: TGroupBox;
    Memo1: TMemo;
    GroupBox3: TGroupBox;
    mskSequencia: TMaskEdit;
    mskLote: TMaskEdit;
    xCorrecao: TMemo;
    qryDoctoFiscalcCNPJCPFEmitente: TStringField;
    qryCartaCorrecaoNFe: TADOQuery;
    qryCartaCorrecaoNFenCdCartaCorrecaoNFe: TIntegerField;
    qryCartaCorrecaoNFenCdDoctoFiscal: TIntegerField;
    qryCartaCorrecaoNFedDtEmissao: TDateTimeField;
    qryCartaCorrecaoNFecCorrecao: TStringField;
    qryCartaCorrecaoNFeiSeq: TIntegerField;
    qryCartaCorrecaoNFeiLote: TIntegerField;
    qryCartaCorrecaoNFecCaminhoXML: TStringField;
    qryCartaCorrecaoNFecArquivoXML: TStringField;
    qryCartaCorrecaoNFecChaveNFe: TStringField;
    qryCartaCorrecaoNFenCdServidorOrigem: TIntegerField;
    qryCartaCorrecaoNFedDtReplicacao: TDateTimeField;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    qryCartaCorrecaoNFecNrProtocoloCCe: TStringField;
    qryCartaCorrecaoNFecXMLCCe: TMemoField;
    qryVerificaSeqCCe: TADOQuery;
    qryVerificaSeqCCeiSeq: TIntegerField;
    Label10: TLabel;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCartaCorrecaoNFe_GeracaoCCe: TfrmCartaCorrecaoNFe_GeracaoCCe;

implementation

uses
  fMenu, fEmiteNFE2;

{$R *.dfm}

procedure TfrmCartaCorrecaoNFe_GeracaoCCe.ToolButton1Click(
  Sender: TObject);
var
  objCCe     : TfrmEmiteNFe2;
  nPK        : Integer;
  nSeqEvento : Integer;
  idLote     : Integer;
begin
  inherited;

  if (MessageDlg('Confirma envio de carta de corre��o ?', mtConfirmation, [mbYes,mbNo], 0) = mrNo) then
      Exit;

  if (StrToInt(Trim(mskSequencia.Text)) < 1) or (StrToInt(Trim(mskSequencia.Text)) > 20) then
  begin
      MensagemAlerta('N�mero de sequ�ncia do evento inv�lido.' + #13 + 'Informe um n�mero entre 1 a 20.');
      mskSequencia.SetFocus;
      Abort;
  end;

  nPK        := frmMenu.fnProximoCodigo('CARTACORRECAONFE');
  nSeqEvento := StrToInt(Trim(mskSequencia.Text));
  idLote     := StrToInt(Trim(mskLote.Text));

  if (Length(Trim(xCorrecao.Lines.Text)) < 15) then
  begin
      MensagemAlerta('Justificativa n�o pode ser inferior a 15 caracteres.');
      xCorrecao.SetFocus;
      Abort;
  end;

  try
      try
          frmMenu.Connection.BeginTrans;

          frmMenu.mensagemUsuario('Processando Carta de Corre��o...');

          objCCe := TfrmEmiteNFe2.Create(nil);

          { -- gera registro CC-e -- }
          qryCartaCorrecaoNFe.Close;
          qryCartaCorrecaoNFe.Open;
          qryCartaCorrecaoNFe.Insert;
          qryCartaCorrecaoNFenCdCartaCorrecaoNFe.Value := nPK;
          qryCartaCorrecaoNFenCdDoctoFiscal.Value      := qryDoctoFiscalnCdDoctoFiscal.Value;
          qryCartaCorrecaoNFecCorrecao.Value           := xCorrecao.Lines.Text;
          qryCartaCorrecaoNFeiSeq.Value                := nSeqEvento;
          qryCartaCorrecaoNFeiLote.Value               := idLote;
          qryCartaCorrecaoNFecChaveNFe.Value           := qryDoctoFiscalcChaveNFe.Value;
          qryCartaCorrecaoNFe.Post;

          { -- envia CC-e -- }
          objCCe.gerarCCe(qryDoctoFiscalcChaveNFe.Value, qryDoctoFiscalcCNPJCPFEmitente.Value, xCorrecao.Lines.Text, nSeqEvento, idLote, nPK);

          frmMenu.mensagemUsuario('');
          frmMenu.Connection.CommitTrans;

          ShowMessage('Carta de Corre��o gerada com sucesso.');
          ShowMessage('Posicione o formul�rio na impressora e clique em OK para iniciar a impress�o.');

          objCCe.imprimirCCe(nPK);
      except
          MensagemErro('Erro no processamento.');
          frmMenu.mensagemUsuario('');
          frmMenu.Connection.RollbackTrans;
          Raise;
          Abort;
      end;
  finally
      FreeAndNil(objCCe);
  end;

  Close;
end;

procedure TfrmCartaCorrecaoNFe_GeracaoCCe.FormShow(Sender: TObject);
begin
  inherited;

  Memo1.Color := $00C7AE9E;
  xCorrecao.SetFocus;
end;

end.
