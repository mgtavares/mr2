unit fMensagem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, Buttons, cxLookAndFeelPainters, cxButtons,
  jpeg;

type
  TfrmMensagem = class(TForm)
    lblMensagem: TLabel;
    Shape1: TShape;
    btnNao: TcxButton;
    btnSim: TcxButton;
    btnOK: TcxButton;
    imgAlerta: TImage;
    imgInfo: TImage;
    imgConfirmation: TImage;
    imgErro: TImage;
    Image1: TImage;
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    AType: TMsgDlgType ;
  end;

var
  frmMensagem: TfrmMensagem;

implementation

uses fMenu;

{$R *.dfm}


{ TfrmMensagem }

procedure TfrmMensagem.FormShow(Sender: TObject);
begin

    imgConfirmation.Visible := False ;
    imgInfo.Visible         := False ;
    imgAlerta.Visible       := false ;
    imgErro.Visible         := false ;

    if Atype = mtConfirmation then
    begin
        imgConfirmation.Visible := true ;
        Caption := 'ER2Soft - Confirmação';
        btnOK.Visible  := false ;
        btnSim.Visible := true ;
        btnNao.Visible := true ;
    end
    else if AType = mtWarning then
    begin
        imgAlerta.Visible := true ;
        Caption := 'ER2Soft - Aviso' ;
        btnOK.Visible  := True ;
        btnSim.Visible := false ;
        btnNao.Visible := false ;
    end
    else if AType = mtError then
    begin
        imgErro.Visible := true ;
        Caption := 'ER2Soft - Erro' ;
        btnOK.Visible  := True ;
        btnSim.Visible := false ;
        btnNao.Visible := false ;
    end
    else if AType = mtInformation then
    begin
        imgInfo.Visible := true ;
        Caption := 'ER2Soft - Informação';
        btnOK.Visible  := True ;
        btnSim.Visible := false ;
        btnNao.Visible := false ;
    end ;

    Application.BringToFront;

    if (btnOK.Visible) then btnOK.SetFocus
    else btnSim.SetFocus;

end;

procedure TfrmMensagem.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

    {case key of
        S_letter := btnSim.Click;
        vk_N := btnNao.Click;
        vk_O := btnOK.Click;
    end ;}

end;

end.
