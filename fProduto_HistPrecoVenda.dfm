inherited frmProduto_HistPrecoVenda: TfrmProduto_HistPrecoVenda
  Left = 237
  Top = 206
  Width = 894
  Height = 396
  BorderIcons = [biSystemMenu]
  Caption = 'Hist'#243'rico Pre'#231'o Venda'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 878
    Height = 329
  end
  inherited ToolBar1: TToolBar
    Width = 878
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh2: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 878
    Height = 329
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsHistorico
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'dDtAltPreco'
        Footers = <>
        Width = 144
      end
      item
        EditButtons = <>
        FieldName = 'nValPrecoAtual'
        Footers = <>
        Title.Caption = 'Hist'#243'rico de Altera'#231#227'o de Pre'#231'o de Venda|Pre'#231'o Novo'
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'nValPrecoAnterior'
        Footers = <>
        Title.Caption = 'Hist'#243'rico de Altera'#231#227'o de Pre'#231'o de Venda|Pre'#231'o Anterior'
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
        Width = 187
      end
      item
        EditButtons = <>
        FieldName = 'cOBS'
        Footers = <>
        Width = 301
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryHistorico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT dDtAltPreco             '
      '      ,nValPrecoAtual                          '
      '      ,nValPrecoAnterior                       '
      '      ,cNmUsuario'
      '      ,cOBS'
      '  FROM HistAltPrecoProduto '
      
        '       LEFT JOIN Usuario ON Usuario.nCdUsuario = HistAltPrecoPro' +
        'duto.nCdUsuario'
      ' WHERE nCdProduto = :nPK'
      ' ORDER BY dDtAltPreco DESC')
    Left = 184
    Top = 104
    object qryHistoricodDtAltPreco: TDateTimeField
      DisplayLabel = 'Hist'#243'rico de Altera'#231#227'o de Pre'#231'o de Venda|Data Altera'#231#227'o'
      FieldName = 'dDtAltPreco'
    end
    object qryHistoriconValPrecoAtual: TBCDField
      DisplayLabel = 'Hist'#243'rico de Altera'#231#227'o de Pre'#231'o de Venda|Pre'#231'o Venda Novo'
      FieldName = 'nValPrecoAtual'
      Precision = 12
      Size = 2
    end
    object qryHistoriconValPrecoAnterior: TBCDField
      DisplayLabel = 'Hist'#243'rico de Altera'#231#227'o de Pre'#231'o de Venda|Pre'#231'o Venda Anterior'
      FieldName = 'nValPrecoAnterior'
      Precision = 12
      Size = 2
    end
    object qryHistoricocNmUsuario: TStringField
      DisplayLabel = 'Hist'#243'rico de Altera'#231#227'o de Pre'#231'o de Venda|Usu'#225'rio'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryHistoricocOBS: TStringField
      DisplayLabel = 'Hist'#243'rico de Altera'#231#227'o de Pre'#231'o de Venda|Observa'#231#227'o'
      FieldName = 'cOBS'
      Size = 50
    end
  end
  object dsHistorico: TDataSource
    DataSet = qryHistorico
    Left = 216
    Top = 104
  end
end
