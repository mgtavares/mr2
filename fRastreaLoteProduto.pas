unit fRastreaLoteProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, GridsEh, DBGridEh, DBCtrls, ADODB, DBGridEhGrouping,
  cxPC, cxControls, ER2Lookup, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid;

type
  TfrmRastreaLoteProduto = class(TfrmProcesso_Padrao)
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DataSource1: TDataSource;
    qryLoteProd: TADOQuery;
    dsLoteProd: TDataSource;
    qryMovto: TADOQuery;
    dsMovto: TDataSource;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label1: TLabel;
    edtProduto: TMaskEdit;
    DBEdit1: TDBEdit;
    edtNumLoteSerie: TEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxPageControl2: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    Label2: TLabel;
    edtCdTerceiro: TER2LookupMaskEdit;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    Label3: TLabel;
    edtDtInicial: TMaskEdit;
    Label6: TLabel;
    edtDtFinal: TMaskEdit;
    qryLoteProdnCdLoteProduto: TAutoIncField;
    qryLoteProdcNrLote: TStringField;
    qryLoteProddDtFabricacao: TDateTimeField;
    qryLoteProddDtDisponibilidade: TDateTimeField;
    qryLoteProddDtValidade: TDateTimeField;
    qryLoteProdnQtdeTotal: TBCDField;
    qryLoteProdnSaldoLote: TBCDField;
    qryLoteProdcOBSLote: TStringField;
    qryLoteProdnCdLocalEstoque: TIntegerField;
    qryLoteProdcNmLocalEstoque: TStringField;
    qryLoteProdcFlgBloqueado: TStringField;
    qryLoteProdcNmStatus: TStringField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdLoteProduto: TcxGridDBColumn;
    cxGridDBTableView1cNrLote: TcxGridDBColumn;
    cxGridDBTableView1dDtFabricacao: TcxGridDBColumn;
    cxGridDBTableView1dDtDisponibilidade: TcxGridDBColumn;
    cxGridDBTableView1dDtValidade: TcxGridDBColumn;
    cxGridDBTableView1nQtdeTotal: TcxGridDBColumn;
    cxGridDBTableView1nSaldoLote: TcxGridDBColumn;
    cxGridDBTableView1cOBSLote: TcxGridDBColumn;
    cxGridDBTableView1nCdLocalEstoque: TcxGridDBColumn;
    cxGridDBTableView1cNmLocalEstoque: TcxGridDBColumn;
    cxGridDBTableView1cFlgBloqueado: TcxGridDBColumn;
    cxGridDBTableView1cNmStatus: TcxGridDBColumn;
    qryMovtodDtMovto: TDateTimeField;
    qryMovtocNmTabTipoOrigemMovLote: TStringField;
    qryMovtoiIDRegistro: TIntegerField;
    qryMovtocNmTerceiro: TStringField;
    qryMovtocNmUsuario: TStringField;
    qryMovtonSaldoAnterior: TBCDField;
    qryMovtonQtdeMov: TBCDField;
    qryMovtonSaldoPosterior: TBCDField;
    qryMovtocOBSMov: TStringField;
    qryMovtocFlgDirecao: TStringField;
    cxGrid1: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView2dDtMovto: TcxGridDBColumn;
    cxGridDBTableView2cNmTabTipoOrigemMovLote: TcxGridDBColumn;
    cxGridDBTableView2iIDRegistro: TcxGridDBColumn;
    cxGridDBTableView2cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView2cNmUsuario: TcxGridDBColumn;
    cxGridDBTableView2nSaldoAnterior: TcxGridDBColumn;
    cxGridDBTableView2nQtdeMov: TcxGridDBColumn;
    cxGridDBTableView2nSaldoPosterior: TcxGridDBColumn;
    cxGridDBTableView2cOBSMov: TcxGridDBColumn;
    cxGridDBTableView2cFlgDirecao: TcxGridDBColumn;
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure qryLoteProdAfterScroll(DataSet: TDataSet);
    procedure edtProdutoExit(Sender: TObject);
    procedure edtProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtProdutoChange(Sender: TObject);
    procedure edtCdTerceiroChange(Sender: TObject);
    procedure edtDtInicialChange(Sender: TObject);
    procedure edtDtFinalChange(Sender: TObject);
    procedure edtNumLoteSerieChange(Sender: TObject);
    procedure qryLoteProdAfterClose(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRastreaLoteProduto: TfrmRastreaLoteProduto;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmRastreaLoteProduto.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryLoteProd.Close ;
  qryMovto.Close ;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione um produto') ;
      edtProduto.SetFocus ;
      exit ;
  end ;

  if (trim(edtCdTerceiro.Text) <> '') and (DBEdit2.Text = '') then
  begin
      MensagemAlerta('C�digo de terceiro inv�lido.') ;
      edtCdTerceiro.SetFocus;
      abort ;
  end ;

  if (frmMenu.convData(edtDtFinal.Text) < frmMenu.ConvData(edtDtInicial.Text)) then
  begin
      MensagemAlerta('Per�odo inv�lido.') ;
      edtDtInicial.SetFocus;
      abort ;
  end ;

  qryLoteProd.Close ;
  qryLoteProd.Parameters.ParamByName('nCdProduto').Value  := qryProdutonCdProduto.Value ;
  qryLoteProd.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(edtCdTerceiro.Text);
  qryLoteProd.Parameters.ParamByName('cNrLote').Value     := Trim(edtNumLoteSerie.Text) ;
  qryLoteProd.Parameters.ParamByName('dDtInicial').Value  := frmMenu.ConvData(edtDtInicial.Text) ;
  qryLoteProd.Parameters.ParamByName('dDtFinal').Value    := frmMenu.ConvData(edtDtFinal.Text) ;
  qryLoteProd.Open ;

  if qryLoteProd.eof then
      MensagemAlerta('Nenhuma movimenta��o encontrada para o crit�rio selecionado.') ;

end;

procedure TfrmRastreaLoteProduto.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  if not qryLoteProd.Active then
      exit ;

  {-- j� deixa posicionado os parametros na query de movimento anal�tico --}
  qryMovto.Close ;
  qryMovto.Parameters.ParamByName('nCdTerceiro').Value    := frmMenu.ConvInteiro(edtCdTerceiro.Text);
  qryMovto.Parameters.ParamByName('dDtInicial').Value     := frmMenu.ConvData(edtDtInicial.Text) ;
  qryMovto.Parameters.ParamByName('dDtFinal').Value       := frmMenu.ConvData(edtDtFinal.Text) ;
  qryMovto.Parameters.ParamByName('nCdLoteProduto').Value := qryLoteProdnCdLoteProduto.Value ;
  qryMovto.Open ;

end;

procedure TfrmRastreaLoteProduto.qryLoteProdAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryMovto.Close ;
  qryMovto.Parameters.ParamByName('nCdLoteProduto').Value := qryLoteProdnCdLoteProduto.Value ;
  qryMovto.Open ;

end;

procedure TfrmRastreaLoteProduto.edtProdutoExit(Sender: TObject);
begin
  inherited;

  qryProduto.Close ;
  PosicionaQuery(qryProduto, edtProduto.Text) ;

end;

procedure TfrmRastreaLoteProduto.edtProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(112);

        If (nPK > 0) then
        begin
            edtProduto.Text := IntToStr(nPK) ;
            PosicionaQuery(qryProduto, edtProduto.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmRastreaLoteProduto.edtProdutoChange(Sender: TObject);
begin
  inherited;

  qryLoteProd.Close ;

end;

procedure TfrmRastreaLoteProduto.edtCdTerceiroChange(Sender: TObject);
begin
  inherited;
  qryLoteProd.Close ;

end;

procedure TfrmRastreaLoteProduto.edtDtInicialChange(Sender: TObject);
begin
  inherited;
  qryLoteProd.Close ;

end;

procedure TfrmRastreaLoteProduto.edtDtFinalChange(Sender: TObject);
begin
  inherited;
  qryLoteProd.Close ;

end;

procedure TfrmRastreaLoteProduto.edtNumLoteSerieChange(Sender: TObject);
begin
  inherited;
  qryLoteProd.Close ;

end;

procedure TfrmRastreaLoteProduto.qryLoteProdAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryMovto.Close ;

end;

initialization
    RegisterClass(TfrmRastreaLoteProduto) ;
    
end.
