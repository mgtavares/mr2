unit fOperadoraCartao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, DBCtrls, Mask, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, DBGridEhGrouping,
  ToolCtrlsEh, cxPC, cxControls;

type
  TfrmOperadoraCartao = class(TfrmCadastro_Padrao)
    qryMasternCdOperadoraCartao: TIntegerField;
    qryMastercNmOperadoraCartao: TStringField;
    qryMasteriFloating: TIntegerField;
    qryMastercFlgDiaUtil: TIntegerField;
    qryMastercFlgTipoOperacao: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label7: TLabel;
    qryLojaOperadora: TADOQuery;
    qryLojaOperadoranCdOperadoraCartao: TIntegerField;
    qryLojaOperadoranCdLoja: TIntegerField;
    qryLojaOperadoranCdContaBancaria: TIntegerField;
    qryLojaOperadoranTaxaOperacao: TBCDField;
    qryLojaOperadoranTaxaOperacaoPrazo: TBCDField;
    dsLojaOperadora: TDataSource;
    qryLojaOperadoracNmLoja: TStringField;
    qryLojaOperadoracNmContaBancaria: TStringField;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancariacNmContaBancaria: TStringField;
    qryGrupoOperadora: TADOQuery;
    qryMasternCdGrupoOperadoraCartao: TIntegerField;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    qryGrupoOperadoranCdGrupoOperadoraCartao: TIntegerField;
    qryGrupoOperadoracNmGrupoOperadoraCartao: TStringField;
    DBEdit6: TDBEdit;
    dsGrupoOperadora: TDataSource;
    qryMastercNmRedeTEF: TStringField;
    Label6: TLabel;
    DBEdit7: TDBEdit;
    qryLojaOperadoranCdLojaOperadoraCartao: TAutoIncField;
    cxPageControl: TcxPageControl;
    tabLojaOperadora: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryMasternCdStatus: TIntegerField;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    qryMastercNmStatus: TStringField;
    DBEdit9: TDBEdit;
    qryTabTipoCredencCartao: TADOQuery;
    qryMasternCdTabTipoCredencCartao: TIntegerField;
    Label8: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    qryMastercNmTabTipoCredencCartao: TStringField;
    qryTabTipoCredencCartaonCdTabTipoCredencCartao: TIntegerField;
    qryTabTipoCredencCartaocNmTabTipoCredencCartao: TStringField;
    Label10: TLabel;
    Label11: TLabel;
    qryMasternCodigoBandeiraCartao: TIntegerField;
    DBEdit12: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryLojaOperadoraCalcFields(DataSet: TDataSet);
    procedure qryLojaOperadoraBeforePost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOperadoraCartao: TfrmOperadoraCartao;

implementation

uses fLookup_Padrao,fLojaOperadoraCartaoTaxaJuros, fMenu;

{$R *.dfm}

procedure TfrmOperadoraCartao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'OPERADORACARTAO' ;
  nCdTabelaSistema  := 69  ;
  nCdConsultaPadrao := 149 ;
  bLimpaAposSalvar  := false ;

  cxPageControl.ActivePage := tabLojaOperadora;
end;

procedure TfrmOperadoraCartao.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMastercFlgDiaUtil.Value := 1 ;

  DBEdit2.SetFocus ;
end;

procedure TfrmOperadoraCartao.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DbEdit2.Text = '') then
  begin
      MensagemAlerta('Informe o nome da operadora.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (DbEdit6.Text = '') then
  begin
      MensagemAlerta('Informe o grupo da operadora.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  if (DBEdit3.Text = '') then
  begin
      MensagemAlerta('Informe o n�mero de dias em que a operadora paga a transa��o.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;

  if (DbEdit4.Text <> 'D') and (DbEdit4.Text <> 'C') then
  begin
      MensagemAlerta('Informe o tipo de opera��o (Cr�dito ou D�bito).') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (DBEdit9.Text = '') then
  begin
      MensagemAlerta('Informe o status da operadora.');
      DBEdit8.SetFocus;
      Abort;
  end;

  inherited;

end;

procedure TfrmOperadoraCartao.qryLojaOperadoraCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryLojaOperadoracNmLoja.Value = '') and (qryLojaOperadoranCdLoja.Value > 0) then
  begin

      PosicionaQuery(qryLoja, qryLojaOperadoranCdLoja.AsString) ;
      if not qryLoja.eof then
          qryLojaOperadoracNmLoja.Value := qryLojacNmLoja.Value ;

  end ;

  if (qryLojaOperadoracNmContaBancaria.Value = '') and (qryLojaOperadoranCdContaBancaria.Value > 0) then
  begin

      PosicionaQuery(qryContaBancaria, qryLojaOperadoranCdContaBancaria.AsString) ;
      if not qryContaBancaria.eof then
          qryLojaOperadoracNmContaBancaria.Value := qryContaBancariacNmContaBancaria.Value ;

  end ;

end;

procedure TfrmOperadoraCartao.qryLojaOperadoraBeforePost(
  DataSet: TDataSet);
begin

  if (qryLojaOperadoracNmLoja.Value = '') then
  begin
      MensagemAlerta('Selecione a loja.') ;
      abort ;
  end ;

  if (qryLojaOperadoracNmContaBancaria.Value = '') then
  begin
      MensagemAlerta('Selecione a conta banc�ria.') ;
      abort ;
  end ;

  if ((qryLojaOperadoranTaxaOperacao.Value < 0) or (qryLojaOperadoranTaxaOperacao.Value > 100)) then
  begin
      MensagemAlerta('Taxa de Opera��o A Vista inv�lida.') ;
      abort ;
  end ;

  if ((qryLojaOperadoranTaxaOperacaoPrazo.Value < 0) or (qryLojaOperadoranTaxaOperacaoPrazo.Value > 100)) then
  begin
      MensagemAlerta('Taxa de Opera��o A Prazo inv�lida.') ;
      abort ;
  end ;

  qryLojaOperadoranCdOperadoraCartao.Value := qryMasternCdOperadoraCartao.Value ;

  if (qryLojaOperadora.State = dsInsert) then
      qryLojaOperadoranCdLojaOperadoraCartao.Value := frmMenu.fnProximoCodigo('LOJAOPERADORACARTAO') ;

  inherited;

end;

procedure TfrmOperadoraCartao.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  if qryMaster.Active and (qryMasternCdOperadoraCartao.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmOperadoraCartao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryLojaOperadora, qryMasternCdOperadoraCartao.AsString) ;
  PosicionaQuery(qryGrupoOperadora, qryMasternCdGrupoOperadoraCartao.asString) ;

end;

procedure TfrmOperadoraCartao.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryLojaOperadora.Close ;
  qryGrupoOperadora.Close ;
  
end;

procedure TfrmOperadoraCartao.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryLojaOperadora, qryMasternCdOperadoraCartao.AsString) ;

end;

procedure TfrmOperadoraCartao.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryLojaOperadora.State = dsBrowse) then
             qryLojaOperadora.Edit ;
        

        if (qryLojaOperadora.State = dsInsert) or (qryLojaOperadora.State = dsEdit) then
        begin

            // loja
            if (dbGridEh1.Col = 2) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(86);

                If (nPK > 0) then
                begin
                    qryLojaOperadoranCdLoja.Value := nPK ;
                end ;

            end ;

            // conta banc�ria
            if (dbGridEh1.Col = 4) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(37,'cFlgCaixa = 0 AND cFlgCofre    = 0 AND cFlgDeposito = 1');

                If (nPK > 0) then
                begin
                    qryLojaOperadoranCdContaBancaria.Value := nPK ;
                end ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOperadoraCartao.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  qryGrupoOperadora.Close ;
  PosicionaQuery(qryGrupoOperadora, DBEdit5.Text) ;
  
end;

procedure TfrmOperadoraCartao.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(172);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoOperadoraCartao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOperadoraCartao.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmLojaOperadoraCartaoTaxaJuros ;

begin
  inherited;
  if(qryLojaOperadora.Active) and (qryLojaOperadoranCdLojaOperadoraCartao.Value > 0)  then
  begin

      objForm := TfrmLojaOperadoraCartaoTaxaJuros.Create(Self) ;

      PosicionaQuery(objForm.qryLojaOperadoraCartaoTaxaJuros,qryLojaOperadoranCdLojaOperadoraCartao.AsString);
      nCdLojaOperadoraCartao := qryLojaOperadoranCdLojaOperadoraCartao.Value;

      showForm( objForm , TRUE ) ;

  end;
end;

procedure TfrmOperadoraCartao.DBEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);

            If (nPK > 0) then
            begin
                qryMasternCdStatus.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOperadoraCartao.DBEdit10KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(788);

            If (nPK > 0) then
            begin
                qryMasternCdTabTipoCredencCartao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmOperadoraCartao) ;

end.
