unit fTerceiro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ADODB, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls, ImgList,
  ACBrBase, ACBrValidador, cxLookAndFeelPainters, cxButtons,
  DBGridEhGrouping, ER2Lookup, ToolCtrlsEh, ACBrSocket, ACBrCEP;

type
  TfrmTerceiro = class(TfrmCadastro_Padrao)
    qryMasternCdTerceiro: TIntegerField;
    qryMastercIDExterno: TStringField;
    qryMastercNmTerceiro: TStringField;
    qryMastercNmFantasia: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMasterdDtCadastro: TDateTimeField;
    qryMastercFlgMicroEmpresa: TIntegerField;
    qryMastercFlgOptSimples: TIntegerField;
    qryMastercFlgContICMS: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMastercNmStatus: TStringField;
    qryTerceiroPagador: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    qryTerceiroTransp: TADOQuery;
    IntegerField2: TIntegerField;
    StringField2: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    DBEdit14: TDBEdit;
    qryTipoTerceiro: TADOQuery;
    qryTerceiroTipoTerceiro: TADOQuery;
    dsTerceiroTipoTerceiro: TDataSource;
    qryTipoTerceironCdTipoTerceiro: TIntegerField;
    qryTipoTerceirocNmTipoTerceiro: TStringField;
    qryTerceiroTipoTerceironCdTerceiro: TIntegerField;
    qryTerceiroTipoTerceironCdTipoTerceiro: TIntegerField;
    qryTerceiroTipoTerceirocNmTipoTerceiro: TStringField;
    qryValidaCNPJ: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    Image2: TImage;
    DBGridEh2: TDBGridEh;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    Image3: TImage;
    Image4: TImage;
    qryMastercFlgIsentoCNPJCPF: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    qryTipoEnd: TADOQuery;
    qryTipoEndnCdTipoEnd: TIntegerField;
    qryTipoEndcNmTipoEnd: TStringField;
    qryEndereco: TADOQuery;
    qryPais: TADOQuery;
    qryPaisnCdPais: TIntegerField;
    qryPaiscNmPais: TStringField;
    qryPaiscSigla: TStringField;
    qryRegiao: TADOQuery;
    qryRegiaonCdRegiao: TIntegerField;
    qryRegiaocNmRegiao: TStringField;
    dsEndereco: TDataSource;
    qryEndereconCdEndereco: TAutoIncField;
    qryEndereconCdTerceiro: TIntegerField;
    qryEnderecocEndereco: TStringField;
    qryEnderecoiNumero: TIntegerField;
    qryEnderecocBairro: TStringField;
    qryEnderecocCidade: TStringField;
    qryEnderecocUF: TStringField;
    qryEndereconCdTipoEnd: TIntegerField;
    qryEndereconCdRegiao: TIntegerField;
    qryEnderecocFlgZFM: TIntegerField;
    qryEnderecocSuframa: TStringField;
    qryEnderecocIE: TStringField;
    qryEnderecocIM: TStringField;
    qryEnderecocNmContato: TStringField;
    qryEnderecocTelefone: TStringField;
    qryEnderecocFax: TStringField;
    qryEnderecocEmail: TStringField;
    qryEndereconCdPais: TIntegerField;
    qryEndereconCdStatus: TIntegerField;
    qryEnderecocNmRegiao: TStringField;
    qryEnderecocSiglaTipoEnd: TStringField;
    qryTipoEndcSigla: TStringField;
    qryEnderecocCep: TStringField;
    qryValidaEndereco: TADOQuery;
    qryEnderecocComplemento: TStringField;
    cxTabSheet5: TcxTabSheet;
    Image6: TImage;
    qryMasternCdBanco: TIntegerField;
    qryMastercAgencia: TStringField;
    qryMastercNumConta: TStringField;
    qryMastercDigitoConta: TStringField;
    qryMasternCdFormaPagto: TIntegerField;
    qryFormaPagto: TADOQuery;
    qryBanco: TADOQuery;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    qryBanconCdBanco: TIntegerField;
    qryBancocNmBanco: TStringField;
    Label11: TLabel;
    DBEdit15: TDBEdit;
    Label12: TLabel;
    DBEdit16: TDBEdit;
    Label13: TLabel;
    DBEdit17: TDBEdit;
    Label14: TLabel;
    DBEdit18: TDBEdit;
    Label15: TLabel;
    DBEdit19: TDBEdit;
    qryIncoterms: TADOQuery;
    qryMoeda: TADOQuery;
    qryIncotermsnCdIncoterms: TIntegerField;
    qryIncotermscSigla: TStringField;
    qryIncotermscNmIncoterms: TStringField;
    qryIncotermscFlgPagador: TStringField;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternCdMoeda: TIntegerField;
    qryMastercSiglaIncoterms: TStringField;
    qryMastercNmIncoterms: TStringField;
    Label16: TLabel;
    DBEdit22: TDBEdit;
    Label17: TLabel;
    DBEdit23: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBGridEh1: TDBGridEh;
    DBRadioGroup2: TDBRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryMasternValCredFinanc: TBCDField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternPercDesconto: TBCDField;
    qryMasternPercAcrescimo: TBCDField;
    qryMastercNmCondPagto: TStringField;
    Label18: TLabel;
    DBEdit28: TDBEdit;
    Label19: TLabel;
    DBEdit30: TDBEdit;
    Label20: TLabel;
    DBEdit31: TDBEdit;
    qryMasteriQtdeChequeDev: TIntegerField;
    qryMasterdDtUltChequeDev: TDateTimeField;
    qryMasternValMaiorChequeDev: TBCDField;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    qryMastercFlgCobrarJuro: TIntegerField;
    qryMasternValMultaAtraso: TBCDField;
    qryMasternPercJuroDia: TBCDField;
    qryMasternCdGrupoEconomico: TIntegerField;
    DBCheckBox2: TDBCheckBox;
    qryMasternValLimiteCred: TBCDField;
    qryMasternValCreditoUtil: TBCDField;
    qryMasternValPedidoAberto: TBCDField;
    qryMasternValRecAtraso: TBCDField;
    qryMasterdMaiorRecAtraso: TDateTimeField;
    qryMastercFlgBloqPedVenda: TIntegerField;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    DBEdit9: TDBEdit;
    Label9: TLabel;
    cxTabSheet6: TcxTabSheet;
    qryContatoTerceiro: TADOQuery;
    qryContatoTerceironCdContatoTerceiro: TAutoIncField;
    qryContatoTerceironCdTerceiro: TIntegerField;
    qryContatoTerceirocNmContato: TStringField;
    qryContatoTerceirocDepartamento: TStringField;
    qryContatoTerceirocTelefone1: TStringField;
    qryContatoTerceirocTelefone2: TStringField;
    qryContatoTerceirocFax: TStringField;
    qryContatoTerceirocEmail: TStringField;
    qryContatoTerceirocOBS: TStringField;
    qryContatoTerceirocFlgContatoPadrao: TIntegerField;
    dsContatoTerceiro: TDataSource;
    DBGridEh3: TDBGridEh;
    qryAux: TADOQuery;
    qryMastercRG: TStringField;
    qryMasternCdBanco2: TIntegerField;
    qryMastercAgencia2: TStringField;
    qryMastercNumConta2: TStringField;
    qryMastercDigitoConta2: TStringField;
    Label21: TLabel;
    DBEdit32: TDBEdit;
    Label22: TLabel;
    DBEdit33: TDBEdit;
    Label23: TLabel;
    DBEdit34: TDBEdit;
    Label24: TLabel;
    DBEdit35: TDBEdit;
    cxTabSheet4: TcxTabSheet;
    Image5: TImage;
    qryMastercNmContato: TStringField;
    qryMastercTelefone1: TStringField;
    qryMastercTelefone2: TStringField;
    qryMastercTelefoneMovel: TStringField;
    qryMastercFax: TStringField;
    qryMastercEmail: TStringField;
    qryMastercWebSite: TStringField;
    qryMastercOBS: TMemoField;
    Label28: TLabel;
    DBEdit21: TDBEdit;
    Label29: TLabel;
    DBEdit40: TDBEdit;
    Label30: TLabel;
    DBEdit41: TDBEdit;
    Label31: TLabel;
    DBEdit42: TDBEdit;
    Label32: TLabel;
    DBEdit43: TDBEdit;
    Label33: TLabel;
    DBEdit44: TDBEdit;
    Label34: TLabel;
    DBEdit45: TDBEdit;
    Label35: TLabel;
    DBMemo1: TDBMemo;
    DBEdit38: TDBEdit;
    Label27: TLabel;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    qryMasternCdRamoAtividade: TIntegerField;
    Label36: TLabel;
    DBEdit46: TDBEdit;
    qryRamoAtividade: TADOQuery;
    qryRamoAtividadenCdRamoAtividade: TIntegerField;
    qryRamoAtividadecNmRamoAtividade: TStringField;
    DBEdit47: TDBEdit;
    dsRamoAtividade: TDataSource;
    DBEdit39: TDBEdit;
    dsGrupoEconomico: TDataSource;
    DBEdit12: TDBEdit;
    dsTerceiroPagador: TDataSource;
    DBEdit48: TDBEdit;
    dsCondPagto: TDataSource;
    DBEdit24: TDBEdit;
    dsMoeda: TDataSource;
    DBEdit13: TDBEdit;
    dsTerceiroTransp: TDataSource;
    DBEdit20: TDBEdit;
    dsFormaPagto: TDataSource;
    qryMasternCdGrupoComissao: TIntegerField;
    Label37: TLabel;
    DBEdit25: TDBEdit;
    qryGrupoComissao: TADOQuery;
    qryGrupoComissaonCdGrupoComissao: TIntegerField;
    qryGrupoComissaocNmGrupoComissao: TStringField;
    DBEdit29: TDBEdit;
    dsGrupoComissao: TDataSource;
    qryMastercNmTitularConta1: TStringField;
    qryMastercNmTitularConta2: TStringField;
    Label38: TLabel;
    DBEdit49: TDBEdit;
    Label39: TLabel;
    DBEdit50: TDBEdit;
    DBCheckBox3: TDBCheckBox;
    qryMastercFlgHomologado: TIntegerField;
    ToolButton3: TToolButton;
    Label40: TLabel;
    DBEdit51: TDBEdit;
    qryTipoTabPreco: TADOQuery;
    qryTipoTabPreconCdTipoTabPreco: TIntegerField;
    qryTipoTabPrecocNmTipoTabPreco: TStringField;
    qryMasternCdTipoTabPreco: TIntegerField;
    Label41: TLabel;
    DBEdit52: TDBEdit;
    DBEdit53: TDBEdit;
    dsTipoTabPreco: TDataSource;
    qryEndereconCdEstado: TIntegerField;
    qryEndereconCdMunicipio: TIntegerField;
    qryEnderecocNmPais: TStringField;
    qryEstado: TADOQuery;
    qryEstadonCdEstado: TIntegerField;
    qryEstadocUF: TStringField;
    qryMunicipio: TADOQuery;
    qryMunicipionCdMunicipio: TIntegerField;
    qryMunicipiocNmMunicipio: TStringField;
    qryBuscaEnderecoCEP: TADOQuery;
    qryEnderecocxxNmPais: TStringField;
    qryEnderecocxxNmMunicipio: TStringField;
    qryEnderecocxxNmEstado: TStringField;
    qryBuscaEnderecoCEPcEndereco: TStringField;
    qryBuscaEnderecoCEPcBairro: TStringField;
    qryBuscaEnderecoCEPnCdPais: TIntegerField;
    qryBuscaEnderecoCEPcNmPais: TStringField;
    qryBuscaEnderecoCEPnCdEstado: TIntegerField;
    qryBuscaEnderecoCEPcUF: TStringField;
    qryBuscaEnderecoCEPnCdMunicipio: TIntegerField;
    qryBuscaEnderecoCEPcNmMunicipio: TStringField;
    qryMastercFlgEnviaNFeEmail: TIntegerField;
    ACBrValidador1: TACBrValidador;
    Label25: TLabel;
    DBEdit36: TDBEdit;
    Label26: TLabel;
    DBEdit37: TDBEdit;
    DBCheckBox4: TDBCheckBox;
    qryMastercEmailCopiaNFe: TStringField;
    qryMastercEmailNFe: TStringField;
    qryTerceiroTipoTerceironCdTerceiroTipoTerceiro: TIntegerField;
    DBCheckBox5: TDBCheckBox;
    qryMastercFlgTipoDescontoDanfe: TIntegerField;
    qryPosicaoFinanceira: TADOQuery;
    qryPosicaoFinanceiranValLimiteCred: TBCDField;
    qryPosicaoFinanceiranValPendenciaFinanceiraReceber: TBCDField;
    qryPosicaoFinanceiranValPendenciaFinanceiraPagar: TBCDField;
    qryPosicaoFinanceiranValPendenciaFinanceiraAtrasado: TBCDField;
    qryPosicaoFinanceiranValPedidoVendaAberto: TBCDField;
    qryPosicaoFinanceiranValPedidoCompraAberto: TBCDField;
    qryPosicaoFinanceiranValChequePendente: TBCDField;
    qryPosicaoFinanceiranValChequeRisco: TBCDField;
    qryPosicaoFinanceiranSaldoLimite: TBCDField;
    dsPosicaoFinanceira: TDataSource;
    qryMediaVenda: TADOQuery;
    dsMediaVenda: TDataSource;
    qryMediaVendanValorMedio: TBCDField;
    qryMediaVendanValorTotal: TBCDField;
    cxTabSheet7: TcxTabSheet;
    DBGridEh4: TDBGridEh;
    qryTerceiroCpfRisco: TADOQuery;
    dsTerceiroCpfRisco: TDataSource;
    qryTerceiroCpfRisconCdTerceiroCPFRisco: TIntegerField;
    qryTerceiroCpfRisconCdTerceiro: TIntegerField;
    qryTerceiroCpfRiscocCNPJCPF: TStringField;
    qryTerceiroCpfRiscocNmEmissor: TStringField;
    qryTerceiroCpfRiscocOBS: TStringField;
    GroupBox1: TGroupBox;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    DBEdit54: TDBEdit;
    DBEdit55: TDBEdit;
    DBEdit56: TDBEdit;
    DBEdit57: TDBEdit;
    DBEdit58: TDBEdit;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    DBEdit61: TDBEdit;
    DBEdit62: TDBEdit;
    qryUsuario: TADOQuery;
    qryUsuarionCdTerceiroResponsavel: TIntegerField;
    qryUsuariocFlgRepresentante: TIntegerField;
    qryAreaVenda: TADOQuery;
    qryDivisaoVenda: TADOQuery;
    qryCanalDistribuicao: TADOQuery;
    edtAreaVenda: TER2LookupDBEdit;
    edtDivisaoVenda: TER2LookupDBEdit;
    edtCanalDistribuicao: TER2LookupDBEdit;
    qryAreaVendanCdAreaVenda: TIntegerField;
    qryAreaVendacNmAreaVenda: TStringField;
    Label51: TLabel;
    DBEdit63: TDBEdit;
    dsAreaVenda: TDataSource;
    dsDivisaoVenda: TDataSource;
    dsCanalDistribuicao: TDataSource;
    qryDivisaoVendanCdDivisaoVenda: TIntegerField;
    qryDivisaoVendacNmDivisaoVenda: TStringField;
    Label52: TLabel;
    DBEdit64: TDBEdit;
    qryCanalDistribuicaonCdCanalDistribuicao: TIntegerField;
    qryCanalDistribuicaocNmCanalDistribuicao: TStringField;
    Label53: TLabel;
    DBEdit65: TDBEdit;
    qryMasternCdAreaVenda: TIntegerField;
    qryMasternCdDivisaoVenda: TIntegerField;
    qryMasternCdCanalDistribuicao: TIntegerField;
    qryMasternCdGrupoCredito: TIntegerField;
    qryGrupoCredito: TADOQuery;
    dsGrupoCredito: TDataSource;
    Label54: TLabel;
    edtGrupoCredito: TER2LookupDBEdit;
    qryGrupoCreditonCdGrupoCredito: TIntegerField;
    qryGrupoCreditocNmGrupoCredito: TStringField;
    DBEdit66: TDBEdit;
    qryMasternCdPlanoContaContabil: TIntegerField;
    Label55: TLabel;
    edtCdPlanoConta: TER2LookupDBEdit;
    qryPlanoConta: TADOQuery;
    qryPlanoContacNmPlanoConta: TStringField;
    DBEdit67: TDBEdit;
    dsPlanoConta: TDataSource;
    qryMasternCdGrupoTerceiro: TIntegerField;
    qryMasternCdProjeto: TIntegerField;
    Label56: TLabel;
    edtGrupoTerceiro: TER2LookupDBEdit;
    qryGrupoTerceiro: TADOQuery;
    qryGrupoTerceironCdGrupoTerceiro: TIntegerField;
    qryGrupoTerceirocNmGrupoTerceiro: TStringField;
    DBEdit68: TDBEdit;
    dsGrupoTerceiro: TDataSource;
    qryProjeto: TADOQuery;
    qryProjetocNmProjeto: TStringField;
    Label57: TLabel;
    edtProjeto: TER2LookupDBEdit;
    DBEdit69: TDBEdit;
    dsProjeto: TDataSource;
    cxTabSheet8: TcxTabSheet;
    DBGridEh5: TDBGridEh;
    qryTerceiroCategFinanc: TADOQuery;
    qryCategFinanc: TADOQuery;
    dsTerceiroCategFinanc: TDataSource;
    qryTerceiroCategFinancnCdTerceiroCategFinanc: TIntegerField;
    qryTerceiroCategFinancnCdTerceiro: TIntegerField;
    qryTerceiroCategFinancnCdCategFinanc: TIntegerField;
    qryCategFinanccNmCategFinanc: TStringField;
    qryTerceiroCategFinanccNmCategFinanc: TStringField;
    cxTabSheet9: TcxTabSheet;
    DBGridEh6: TDBGridEh;
    qryTerceiroRepresentante: TADOQuery;
    dsTerceiroRepresentante: TDataSource;
    qryTerceiroRepresentantenCdTerceiroRepresentante: TIntegerField;
    qryTerceiroRepresentantenCdTerceiro: TIntegerField;
    qryTerceiroRepresentantenCdTerceiroRepres: TIntegerField;
    qryTerceiroRepres2: TADOQuery;
    qryTerceiroRepres2cNmTerceiro: TStringField;
    qryTerceiroRepresentantecNmTerceiro: TStringField;
    qryValidaTerceiroxRepresentante: TADOQuery;
    qryValidaTerceiroxRepresentanteiRetorno: TIntegerField;
    cxTabSheet10: TcxTabSheet;
    DBGridEh7: TDBGridEh;
    qryTerceiroTipoTabPreco: TADOQuery;
    qryTerceiroTipoTabPreconCdTerceiroTipoTabPreco: TIntegerField;
    qryTerceiroTipoTabPreconCdTerceiro: TIntegerField;
    qryTerceiroTipoTabPreconCdTipoPedido: TIntegerField;
    qryTerceiroTipoTabPreconCdTipoTabPreco: TIntegerField;
    dsTerceiroTipoTabPreco: TDataSource;
    qryTerceiroTipoTabPrecocNmTipoPedido: TStringField;
    qryTerceiroTipoTabPrecocNmTipoTabPreco: TStringField;
    qryTipoPedido: TADOQuery;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTipoTabPrecoAux: TADOQuery;
    qryTipoTabPrecoAuxcNmTipoTabPreco: TStringField;
    qrySegmentoCobranca: TADOQuery;
    qrySegmentoCobrancanCdSegmentoCobranca: TIntegerField;
    qryPerfilCobranca: TADOQuery;
    qryPerfilCobrancanCdPerfilCobranca: TIntegerField;
    qryMasternCdLojaTerceiro: TIntegerField;
    qryMastercFlgTipoFat: TIntegerField;
    qryMasterdDtUltNegocio: TDateTimeField;
    qryMasternValChequePendComp: TBCDField;
    qryMasterdDtNegativacao: TDateTimeField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMastercInstrucaoBoleto1: TStringField;
    qryMastercInstrucaoBoleto2: TStringField;
    qryMasteriDiasProtesto: TIntegerField;
    qryMasternPercDesctoBolAteVencto: TBCDField;
    qryMasteriDiasCarencia: TIntegerField;
    qryMasternPercMultaBoleto: TBCDField;
    qryMasternPercJurosMesBoleto: TBCDField;
    qryMastercFlgPermitirEmissaoBoleto: TIntegerField;
    qryMastercFlgNaoEnviarProtesto: TIntegerField;
    qryMasternCdSegmentoCobranca: TIntegerField;
    qryMasternCdPerfilCobranca: TIntegerField;
    Label58: TLabel;
    edtSegmentoCobranca: TER2LookupDBEdit;
    edtPerfilCobranca: TER2LookupDBEdit;
    Label59: TLabel;
    dsSegmentoCobranca: TDataSource;
    qrySegmentoCobrancacNmSegmentoCobranca: TStringField;
    qryPerfilCobrancacNmPerfilCobranca: TStringField;
    DBEdit70: TDBEdit;
    DBEdit71: TDBEdit;
    dsPerfilCobranca: TDataSource;
    GroupBox2: TGroupBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    Label60: TLabel;
    DBEdit72: TDBEdit;
    Label61: TLabel;
    DBEdit73: TDBEdit;
    Label62: TLabel;
    DBEdit74: TDBEdit;
    Label63: TLabel;
    DBEdit75: TDBEdit;
    Label64: TLabel;
    DBEdit76: TDBEdit;
    DBEdit77: TDBEdit;
    Label66: TLabel;
    DBEdit78: TDBEdit;
    Label7: TLabel;
    qryTabTipoEnquadTributario: TADOQuery;
    DBEdit11: TDBEdit;
    dsTabTipoEnquadTributario: TDataSource;
    er2LkpTabTipoEnquadTributario: TER2LookupDBEdit;
    ACBrCEP1: TACBrCEP;
    ToolButton10: TToolButton;
    qryEndereconCdTabTipoLogradouro: TIntegerField;
    qryEnderecocDescricaoLogradouro: TStringField;
    qryTipoLogradouro: TADOQuery;
    qryTipoLogradouronCdTabTipoLogradouro: TIntegerField;
    qryTipoLogradourocDescricao: TStringField;
    dsTipoLogradouro: TDataSource;
    qryMastercIDTransportadora: TStringField;
    DBEdit79: TDBEdit;
    Label65: TLabel;
    btPosicaoReceber: TcxButton;
    btPosicaoRecebAtraso: TcxButton;
    btVendaAutorizada: TcxButton;
    btChequePend: TcxButton;
    btChequePendRisco: TcxButton;
    btMediaVenda: TcxButton;
    btVenda: TcxButton;
    qryTabTipoEnquadTributarionCdTabTipoEnquadTributario: TIntegerField;
    qryTabTipoEnquadTributariocNmTabTipoEnquadTributario: TStringField;
    qryMasternCdTabTipoEnquadTributario: TIntegerField;
    qryMasternValPedidoAbertoWeb: TBCDField;
    qryTabTipoPorteEmp: TADOQuery;
    qryTabTipoPorteEmpnCdTabTipoPorteEmp: TIntegerField;
    qryTabTipoPorteEmpcNmTabTipoPorteEmp: TStringField;
    qryMasternCdTabTipoPorteEmp: TIntegerField;
    dsTabTipoPorteEmp: TDataSource;
    er2LkpTabTipoPorteEmp: TER2LookupDBEdit;
    DBEdit7: TDBEdit;
    Label67: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryTerceiroTipoTerceiroBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryEnderecoBeforePost(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBEdit15KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit16KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit22KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit28KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit38KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryContatoTerceiroBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBEdit10Exit(Sender: TObject);
    procedure DBEdit10Enter(Sender: TObject);
    procedure DBEdit46Exit(Sender: TObject);
    procedure DBEdit38Exit(Sender: TObject);
    procedure DBEdit46KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8Exit(Sender: TObject);
    procedure DBEdit28Exit(Sender: TObject);
    procedure DBEdit15Exit(Sender: TObject);
    procedure DBEdit9Exit(Sender: TObject);
    procedure DBEdit22Exit(Sender: TObject);
    procedure DBEdit25Exit(Sender: TObject);
    procedure DBEdit25KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton3Click(Sender: TObject);
    procedure DBEdit52Exit(Sender: TObject);
    procedure DBEdit52KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryEnderecoCalcFields(DataSet: TDataSet);
    procedure DBGridEh2ColExit(Sender: TObject);
    procedure btPosicaoReceberClick(Sender: TObject);
    procedure btPosicaoRecebAtrasoClick(Sender: TObject);
    procedure btVendaAutorizadaClick(Sender: TObject);
    procedure btChequePendClick(Sender: TObject);
    procedure btChequePendRiscoClick(Sender: TObject);
    procedure btVendaClick(Sender: TObject);
    procedure qryTerceiroCpfRiscoBeforePost(DataSet: TDataSet);
    procedure btMediaVendaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure edtDivisaoVendaBeforeLookup(Sender: TObject);
    procedure edtDivisaoVendaBeforePosicionaQry(Sender: TObject);
    procedure qryTerceiroCategFinancBeforePost(DataSet: TDataSet);
    procedure DBGridEh5Enter(Sender: TObject);
    procedure qryTerceiroCategFinancCalcFields(DataSet: TDataSet);
    procedure DBGridEh5KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh6Enter(Sender: TObject);
    procedure DBGridEh6KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTerceiroRepresentanteBeforePost(DataSet: TDataSet);
    procedure qryTerceiroRepresentanteCalcFields(DataSet: TDataSet);
    procedure qryTerceiroTipoTabPrecoBeforePost(DataSet: TDataSet);
    procedure DBGridEh7KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh7Enter(Sender: TObject);
    procedure qryTerceiroTipoTabPrecoCalcFields(DataSet: TDataSet);
    procedure er2LkpTabTipoEnquadTributarioExit(Sender: TObject);
    procedure er2LkpTabTipoPorteEmpExit(Sender: TObject);
  private
    { Private declarations }
    bRepresentante  : boolean ;
  public
    { Public declarations }
  end;

var
  frmTerceiro: TfrmTerceiro;

implementation

uses fMenu, fLookup_Padrao, fCentralArquivos, fTerceiro_PosicaoReceber,
  fTerceiro_VendaAutorizada, fTerceiro_ChequePendente, dcVendaProdutoCliente_view,
  fTerceiro_MediaVenda;

{$R *.dfm}

procedure TfrmTerceiro.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TERCEIRO' ;
  nCdTabelaSistema  := 13 ;
  nCdConsultaPadrao := 225 ;

  bLimpaAposSalvar  := False ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

{-- fun��o que v�lida E-mail--}

Function ValidaEMail(const EMailIn: PChar):Boolean;
  const
    CaraEsp: array[1..40] of string[1] =
    ( '!','#','$','%','�','&','*',
    '(',')','+','=','�','�','�','�','�',
    '�','�','�','`','�','�',',',';',':',
    '<','>','~','^','?','/','','|','[',']','{','}',
    '�','�','�');
  var
    i,cont   : integer;
    EMail    : ShortString;
  begin
    EMail := EMailIn;
    Result := True;
    cont := 0;
    if EMail <> '' then
      if (Pos('@', EMail)<>0) and (Pos('.', EMail)<>0) then    // existe @ .
      begin
        if (Pos('@', EMail)=1) or (Pos('@', EMail)= Length(EMail)) or (Pos('.', EMail)=1) or (Pos('.', EMail)= Length(EMail)) or (Pos(' ', EMail)<>0) then
          Result := False
        else                                   // @ seguido de . e vice-versa
          if (abs(Pos('@', EMail) - Pos('.', EMail)) = 1) then
            Result := False
          else
            begin
              for i := 1 to 40 do            // se existe Caracter Especial
                if Pos(CaraEsp[i], EMail)<>0 then
                  Result := False;
              for i := 1 to length(EMail) do
              begin                                 // se existe apenas 1 @
                if EMail[i] = '@' then
                  cont := cont + 1;                    // . seguidos de .
                if (EMail[i] = '.') and (EMail[i+1] = '.') then
                  Result := false;
              end;
                                   // . no f, 2ou+ @, . no i, - no i, _ no i
              if (cont >=2) or ( EMail[length(EMail)]= '.' )
                or ( EMail[1]= '.' ) or ( EMail[1]= '_' )
                or ( EMail[1]= '-' )  then
                  Result := false;
                                            // @ seguido de COM e vice-versa
              if (abs(Pos('@', EMail) - Pos('com', EMail)) = 1) then
                Result := False;
                                              // @ seguido de - e vice-versa
              if (abs(Pos('@', EMail) - Pos('-', EMail)) = 1) then
                Result := False;
                                              // @ seguido de _ e vice-versa
              if (abs(Pos('@', EMail) - Pos('_', EMail)) = 1) then
                Result := False;
            end;
      end
      else
        Result := False;
end;

procedure TfrmTerceiro.btIncluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;
  DBEdit3.SetFocus ;
end;

procedure TfrmTerceiro.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBEdit3.Text = '') then
  begin
      ShowMessage('Informe o Nome/Raz�o Social');
      DBEdit3.SetFocus;
      Abort ;
  end ;

  if (qryMastercNmFantasia.Value = '') then
      qryMastercNmFantasia.Value := qryMastercNmTerceiro.Value;

  if ((DBEdit14.Text = '') or (qryMasternCdStatus.Value = 0)) then
  begin
      qryMasternCdStatus.Value := 1 ;
  end ;

  if ((DBEdit24.Text = '') or (qryMasternCdMoeda.Value = 0)) then
  begin
      qryMasternCdmoeda.Value := 1 ;
  end ;

  If (qryMaster.State = dsInsert) then
      qryMasterdDtCadastro.Value := Now() ;

  if ((qryMastercCNPJCPF.Value <> '') And (frmMenu.TestaCpfCgc(qryMastercCNPJCPF.Value) = '')) then
  begin
      DBEdit5.SetFocus;
      Abort ;
  end ;

  if ((qryMastercCNPJCPF.Value = '') and (qryMastercFlgIsentoCNPJCPF.Value = 0)) then
  begin
      ShowMessage('Informe o CNPJ/CPF ou marque como isento da inscri��o.') ;
      DBEdit5.SetFocus ;
      Abort ;
  end ;

  // Valida se o CPF / CNPJ n�o existe
  If (qryMastercCNPJCPF.Value <> '') then
  begin
      qryValidaCNPJ.Close ;
      qryValidaCNPJ.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
      qryValidaCNPJ.Parameters.ParamByName('cCNPJ').Value       := qryMastercCNPJCPF.Value ;
      qryValidaCNPJ.Open ;

      if not qryValidaCNPJ.Eof then
      begin
          ShowMessage('CNPJ / CPF j� cadastrado para outro terceiro. Utilize a consulta para localizar.') ;
          DbEdit5.SetFocus;
          Abort ;
      end ;

  end ;

  if((DBCheckBox2.Checked = true) and (DBEdit36.Text = '')) then
  begin
      MensagemAlerta('Para enviar a NFe por e-mail digite um endere�o de e-mail principal.');
      cxPageControl1.ActivePageIndex := 2;
      DBEdit36.SetFocus;
      Abort ;
  end;

  if((DBEdit36.Text <> '') and (ValidaEMail(PChar(DBEdit36.Text)) = false)) then
      begin
          MensagemAlerta('O e-mail principal digitado � inv�lido.');
          cxPageControl1.ActivePageIndex := 2;
          DBEdit36.SetFocus;
          Abort ;
      end
      else if((DBEdit37.Text <> '') and (ValidaEMail(PChar(DBEdit37.Text)) = false)) then
      begin
          MensagemAlerta('O e-mail de c�pia digitado � inv�lido.');
          cxPageControl1.ActivePageIndex := 2;
          DBEdit37.SetFocus;
          Abort ;
      end;

  // Aba Dados Gerais
  if (cxPageControl1.ActivePageIndex <> 0) then
      cxPageControl1.ActivePageIndex := 0;

  // Grupo de Cr�dito
  if ((Trim(edtGrupoCredito.Text) <> '') and (qryGrupoCredito.IsEmpty)) then
  begin
      MensagemAlerta('Grupo de Cr�dito inv�lido.');
      edtGrupoCredito.SetFocus;
      Abort;
  end;

  // �rea de Venda
  if ((Trim(edtAreaVenda.Text) <> '') and (qryAreaVenda.IsEmpty)) then
  begin
      MensagemAlerta('�rea de Venda inv�lida.');
      edtAreaVenda.SetFocus;
      Abort;
  end;

  // Divis�o de Venda
  if ((Trim(edtDivisaoVenda.Text) <> '') and (qryDivisaoVenda.IsEmpty)) then
  begin
      MensagemAlerta('Divis�o de Venda inv�lida.');
      edtDivisaoVenda.SetFocus;
      Abort;
  end;

  // Canal de Contribui��o
  if ((Trim(edtCanalDistribuicao.Text) <> '') and (qryCanalDistribuicao.IsEmpty)) then
  begin
      MensagemAlerta('Canal de Contribui��o inv�lido.');
      edtCanalDistribuicao.SetFocus;
      Abort;
  end;

  // Grupo Econ�mico
  if ((qryMasternCdGrupoEconomico.IsNull = False) and (qryGrupoEconomico.IsEmpty)) then
  begin
      MensagemAlerta('Grupo Econ�mico inv�lido.');
      DBEdit38.SetFocus;
      Abort;
  end;

  // Ramo de Atividade
  if ((qryMasternCdRamoAtividade.IsNull = False) and (qryRamoAtividade.IsEmpty)) then
  begin
      MensagemAlerta('Ramo de Atividade inv�lido.');
      DBEdit46.SetFocus;
      Abort;
  end;

  // Grupo Terceiro
  if ((Trim(edtGrupoTerceiro.Text) <> '') and (qryGrupoTerceiro.IsEmpty)) then
  begin
      MensagemAlerta('Grupo de Terceiro inv�lido.');
      edtGrupoTerceiro.SetFocus;
      Abort;
  end;

  // Projeto
  if ((Trim(edtProjeto.Text) <> '') and (qryProjeto.IsEmpty)) then
  begin
      MensagemAlerta('Projeto inv�lido.');
      edtProjeto.SetFocus;
      Abort;
  end;

  // Segmento Cobran�a
  if ((Trim(edtSegmentoCobranca.Text) <> '') and (qrySegmentoCobranca.IsEmpty)) then
  begin
      MensagemAlerta('Segmento de Cobran�a inv�lido.');
      edtSegmentoCobranca.SetFocus;
      Abort;
  end;

  // Perfil Cobran�a
  if ((Trim(edtPerfilCobranca.Text) <> '') and (qryPerfilCobranca.IsEmpty)) then
  begin
      MensagemAlerta('Perfil de Cobran�a inv�lido.');
      edtPerfilCobranca.SetFocus;
      Abort;
  end;

  // Aba Termos de Pagamento
  if (cxPageControl1.ActivePageIndex <> 1) then
      cxPageControl1.ActivePageIndex := 1;

  // Terceiro Pagador
  if ((qryMasternCdTerceiroPagador.IsNull = False) and (qryTerceiroPagador.IsEmpty)) then
  begin
      MensagemAlerta('Terceiro Pagador inv�lido.');
      DBEdit8.SetFocus;
      Abort;
  end;

  // Tipo Tabela Pre�o
  if ((qryMasternCdTipoTabPreco.IsNull = False) and (qryTipoTabPreco.IsEmpty)) then
  begin
      MensagemAlerta('Tipo de Tabela de Pre�o inv�lido.');
      DBEdit52.SetFocus;
      Abort;
  end;

  // Condi��o Pr�-Fixada
  if ((qryMasternCdCondPagto.IsNull = False) and (qryCondPagto.IsEmpty)) then
  begin
      MensagemAlerta('Condi��o Pr�-Fixada inv�lida.');
      DBEdit28.SetFocus;
      Abort;
  end;

  // Forma de Pagamento
  if ((qryMasternCdFormaPagto.IsNull = False) and (qryFormaPagto.IsEmpty)) then
  begin
      MensagemAlerta('Forma de Pagamento inv�lida.');
      DBEdit15.SetFocus;
      Abort;
  end;

  // Aba Faturamento
  if (cxPageControl1.ActivePageIndex <> 2) then
      cxPageControl1.ActivePageIndex := 2;

  // Conta Cont�bil
  if ((Trim(edtCdPlanoConta.Text) <> '') and (qryPlanoConta.IsEmpty)) then
  begin
      MensagemAlerta('Conta Cont�bil inv�lida.');
      edtCdPlanoConta.SetFocus;
      Abort;
  end;

  // Transportadora
  if ((qryMasternCdTerceiroTransp.IsNull = False) and (qryTerceiroTransp.IsEmpty)) then
  begin
      MensagemAlerta('Transportadora inv�lida.');
      DBEdit9.SetFocus;
      Abort;
  end;

  // Incoterms
  if ((qryMasternCdIncoterms.IsNull = False) and (DBEdit27.Text = '')) then
  begin
      MensagemAlerta('Incoterms inv�lido.');
      DBEdit23.SetFocus;
      Abort;
  end;

  // Grupo Comiss�o
  if ((qryMasternCdGrupoComissao.IsNull = False) and (qryGrupoComissao.IsEmpty)) then
  begin
      MensagemAlerta('Grupo de Comiss�o inv�lido.');
      DBEdit25.SetFocus;
      Abort;
  end;

  if ( qryMastercFlgPermitirEmissaoBoleto.Value = 1 ) then
  begin

      if ( qryMastercFlgNaoEnviarProtesto.Value = 0 ) and ( qryMasteriDiasProtesto.Value <= 0 ) then
      begin
          MensagemAlerta('Informe os dias para enviar para protesto.');
          abort;
      end ;

  end ;

  qryMasternValMultaAtraso.Value := abs(qryMasternValMultaAtraso.Value) ;
  qryMasternPercJuroDia.Value := abs(qryMasternPercJuroDia.Value) ;

  if (qryMasternCdTabTipoEnquadTributario.Value = 1) then
      qryMastercFlgOptSimples.Value := 1
  else
      qryMastercFlgOptSimples.Value := 0;

  inherited;
end;

procedure TfrmTerceiro.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryTerceiroTipoTerceiro.Close ;
  qryTerceiroTipoTerceiro.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  qryTerceiroTipoTerceiro.Open ;

  qryEndereco.Close ;
  qryEndereco.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  qryEndereco.Open ;

  qryTerceiroCpfRisco.Close ;
  qryTerceiroCpfRisco.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  qryTerceiroCpfRisco.Open ;

  PosicionaQuery(qryAreaVenda, qryMasternCdAreaVenda.asString) ;
  PosicionaQuery(qryCanalDistribuicao, qryMasternCdCanalDistribuicao.asString) ;
  PosicionaQuery(qryGrupoTerceiro, qryMasternCdGrupoTerceiro.asString) ;

  qryDivisaoVenda.Close;
  qryDivisaoVenda.Parameters.ParamByName('nCdAreaVenda').Value := qryMasternCdAreaVenda.Value ;
  qryDivisaoVenda.Open;
  qryTerceiroCategFinanc.Close;

  PosicionaQuery(qryDivisaoVenda, qryMasternCdDivisaoVenda.asString) ;

  PosicionaQuery(qryContatoTerceiro, qryMasternCdTerceiro.asString) ;
  PosicionaQuery(qryRamoAtividade, qryMasternCdRamoAtividade.asString) ;
  PosicionaQuery(qryGrupoEconomico, qryMasternCdGrupoEconomico.asString) ;
  PosicionaQuery(qryTerceiroTransp, qryMasternCdTerceiroTransp.asString) ;
  PosicionaQuery(qryMoeda, qryMasternCdMoeda.asString) ;
  PosicionaQuery(qryCondPagto, qryMasternCdCondPagto.asString) ;
  PosicionaQuery(qryFormaPagto, qryMasternCdFormaPagto.asString) ;
  PosicionaQuery(qryTerceiroPagador, qryMasternCdTerceiroPagador.asString) ;
  PosicionaQuery(qryGrupoComissao, qryMasternCdGrupoComissao.AsString) ;
  PosicionaQuery(qryTipoTabPreco, qryMasternCdTipoTabPreco.AsString) ;
  Posicionaquery(qryPosicaoFinanceira, qryMasternCdTerceiro.asString) ;
  Posicionaquery(qryMediaVenda, qryMasternCdTerceiro.asString) ;
  PosicionaQuery(qryProjeto, qryMasternCdProjeto.asString) ;
  PosicionaQuery(qryTabTipoEnquadTributario, qryMasternCdTabTipoEnquadTributario.AsString);
  PosicionaQuery(qryTabTipoPorteEmp, qryMasternCdTabTipoPorteEmp.AsString);
  Posicionaquery(qryGrupoCredito, qryMasternCdGrupoCredito.asString) ;
  PosicionaQuery(qryPlanoConta, qryMasternCdPlanoContaContabil.asString) ;
  PosicionaQuery(qryTerceiroCategFinanc, qryMasternCdTerceiro.asString ) ;
  PosicionaQuery(qryTerceiroRepresentante, qryMasternCdTerceiro.asString) ;
  PosicionaQuery(qryTerceiroTipoTabPreco, qryMasternCdTerceiro.asString) ;
  PosicionaQuery(qrySegmentoCobranca, qryMasternCdSegmentoCobranca.AsString) ;
  PosicionaQuery(qryPerfilCobranca, qryMasternCdPerfilCobranca.AsString) ;

end;

procedure TfrmTerceiro.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryTerceiroTipoTerceiro.Close ;
  qryEndereco.Close ;
  qryContatoTerceiro.Close ;
  qryRamoAtividade.Close ;
  qryGrupoEconomico.Close ;
  qryTerceiroTransp.Close ;
  qryMoeda.Close ;
  qryCondPagto.Close ;
  qryFormaPagto.Close ;
  qryTerceiroPagador.Close ;
  qryGrupoComissao.Close ;
  qryTipoTabPreco.Close;
  qryPosicaoFinanceira.Close;
  qryMediaVenda.Close;
  qryTerceiroCpfRisco.Close;
  qryTabTipoEnquadTributario.Close;
  qryTabTipoPorteEmp.Close;

  qryAreaVenda.Close;
  qryDivisaoVenda.Close;
  qryCanalDistribuicao.Close;
  qryGrupoTerceiro.Close;
  qryProjeto.Close;

  qryGrupoCredito.Close;
  qryPlanoConta.Close;
  qryTerceiroCategFinanc.Close;
  qryTerceiroRepresentante.Close;
  qryTerceiroTipoTabPreco.Close;
  qrySegmentoCobranca.Close;
  qryPerfilCobranca.Close;

end;

procedure TfrmTerceiro.qryTerceiroTipoTerceiroBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
  begin
      qryMaster.Post ;
      qryMaster.Edit ;
  end ;

  qryTerceiroTipoTerceironCdTerceiro.Value := qryMasternCdTerceiro.Value ;

  if (qryTerceiroTipoTerceiro.State = dsInsert) then
      qryTerceiroTipoTerceironCdTerceiroTipoTerceiro.Value := frmMenu.fnProximoCodigo('TERCEIROTIPOTERCEIRO') ;

  inherited;

end;

procedure TfrmTerceiro.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTerceiroTipoTerceiro.State = dsBrowse) then
             qryTerceiroTipoTerceiro.Edit ;
        

        if (qryTerceiroTipoTerceiro.State = dsInsert) or (qryTerceiroTipoTerceiro.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(18);

            If (nPK > 0) then
            begin
                qryTerceiroTipoTerceironCdTipoTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.DBEdit9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.DBEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.qryEnderecoBeforePost(DataSet: TDataSet);
var strEstados : TstringList;
begin

  if (trim(qryEnderecocCEP.Value) = '') then
  begin
      MensagemAlerta('Informe o CEP.') ;
      abort ;
  end ;

  if (length(trim(qryEnderecocCep.Value)) <> 8) then
  begin
      MensagemAlerta('O CEP deve conter 8 d�gitos. Verifique.') ;
      abort ;
  end ;

  try
      strToint(qryEnderecocCEP.Value) ;
  except
      MensagemAlerta('CEP inv�lido. Informe somente os n�meros do cep.') ;
      abort ;
  end ;

  if (qryEnderecocDescricaoLogradouro.Value = '') then
  begin
      MensagemAlerta('Informe o Tipo do Logradouro.');
      Abort;
  end;

  if (qryEnderecocEndereco.Value = '') then
  begin
      MensagemAlerta('Informe o Endere�o.') ;
      Abort ;
  end ;

  if (qryEnderecocBairro.Value = '') then
  begin
      MensagemAlerta('Informe o Bairro.') ;
      Abort ;
  end ;

  if (qryEnderecocxxNmPais.Value = '') then
  begin
      MensagemAlerta('Selecione o pa�s.') ;
      abort ;
  end ;

  if (qryEnderecocxxNmEstado.Value = '') then
  begin
      MensagemAlerta('Selecione o Estado.') ;
      Abort ;
  end ;

  if (qryEnderecocxxNmMunicipio.Value = '') then
  begin
      MensagemAlerta('Selecione o Munic�pio.') ;
      Abort ;
  end ;


  if (qryEndereconCdRegiao.Value = 0) or (qryEnderecocNmRegiao.Value = '') then
  begin
      MensagemAlerta('Informe a regi�o.') ;
      Abort ;
  end ;

  if (qryEnderecocSiglaTipoend.Value = '') then
  begin
      MensagemAlerta('Informe o tipo de endere�o.') ;
      Abort ;
  end ;

  if (qryEnderecocFlgZFM.value = 1) and (qryEnderecocSuframa.Value = '') then
  begin
      MensagemAlerta('Informe a inscri��o do Suframa') ;
      Abort ;
  end ;

  if (qryEndereconCdStatus.Value = 0) then
      qryEndereconCdStatus.Value := 1 ;

  if (qryEnderecocIE.Value = '') then
  begin
      if (qryMastercFlgIsentoCNPJCPF.Value = 0) then
      begin
          if (Length(qryMastercCNPJCPF.Value) > 11) then
          begin
              MensagemAlerta('Informe a inscri��o estadual ou escreva ISENTO.') ;
              abort ;
          end ;
      end ;
  end
  else
  begin
      {-- v�lida a inscri��o estadual --}
      ACBrValidador1.Documento   := qryEnderecocIE.Value;
      ACBrValidador1.Complemento := qryEnderecocUF.Value;

      ACBrValidador1.Formatar;

      if not (ACBrValidador1.Validar) then
      begin
          MensagemAlerta('Inscri��o estadual digitada � inv�lida');
          Abort;
      end;
  end;

  if (qryMaster.FieldList[0].Value > 0) and (qryEndereconCdTipoEnd.Value = 1) then
  begin
      qryValidaEndereco.Close ;
      qryValidaEndereco.Parameters.ParamByName('nCdTerceiro').Value := qryMaster.FieldList[0].Value ;
      qryValidaEndereco.Parameters.ParamByName('nCdTipOEnd').Value  := qryEndereconCdTipoEnd.Value  ;
      qryValidaEndereco.Parameters.ParamByName('nCdEndereco').Value := qryEndereconCdEndereco.Value ;
      qryValidaEndereco.Open ;

      if not qryValidaEndereco.eof then
      begin
          ShowMessage('J� existe um endere�o para este tipo. Troque o tipo ou altere o existente.') ;
          abort ;
      end ;

  end ;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryEndereconCdTerceiro.Value  := qryMasternCdTerceiro.Value ;
  qryEnderecocEndereco.Value    := UpperCase(qryEnderecocEndereco.Value) ;
  qryEnderecocBairro.Value      := UpperCase(qryEnderecocBairro.Value) ;
  qryEnderecocCidade.Value      := UpperCase(qryEnderecocCidade.Value) ;
  qryEnderecocUF.Value          := UpperCase(qryEnderecocUF.Value) ;
  qryEnderecocIE.Value          := UpperCase(qryEnderecocIE.Value) ;
  qryEnderecocIM.Value          := UpperCase(qryEnderecocIM.Value) ;
  qryEnderecocNmContato.Value   := UpperCase(qryEnderecocNmContato.Value) ;
  qryEnderecocComplemento.Value := UpperCase(qryEnderecocComplemento.Value) ;
  qryEnderecocNmPais.Value      := qryEnderecocxxNmPais.Value ;
  qryEnderecocUF.Value          := qryEstadocUF.Value ;
  qryEnderecocCidade.Value      := qryEnderecocxxNmMunicipio.Value ;

  inherited;

  if (qryEndereco.State = dsInsert) then
      qryEndereconCdEndereco.Value := frmMenu.fnProximoCodigo('ENDERECO') ;

end;

procedure TfrmTerceiro.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        // 7 -- pais
        // 9 -- estado
        // 11 -- municipio
        // 13 -- tipo endere�o
        // 15 -- regi�o
        
        if (qryEndereco.State = dsBrowse) then
             qryEndereco.Edit ;


        if (qryEndereco.State = dsInsert) or (qryEndereco.State = dsEdit) then
        begin


            if (DBGridEH2.Col = 3) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(772);

                If (nPK > 0) then
                    qryEndereconCdTabTipoLogradouro.Value := nPK ;

            end ;

            if (DBGridEH2.Col = 9) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(20);

                If (nPK > 0) then
                    qryEndereconCdPais.Value := nPK ;

            end ;

            if (DBGridEH2.Col = 11) then
            begin

                if (qryEnderecocxxNmPais.Value = '') then
                begin
                    MensagemAlerta('Selecione o pa�s.') ;
                    abort ;
                end ;

                nPK := frmLookup_Padrao.ExecutaConsulta2(204,'Estado.nCdPais = ' + qryEndereconCdPais.asString);

                If (nPK > 0) then
                    qryEndereconCdEstado.Value := nPK ;

            end ;

            if (DBGridEH2.Col = 13) then
            begin
                if (qryEnderecocxxNmEstado.Value = '') then
                begin
                    MensagemAlerta('Selecione o estado.') ;
                    abort ;
                end ;

                nPK := frmLookup_Padrao.ExecutaConsulta2(205,'Municipio.nCdEstado = ' + qryEndereconCdEstado.asString);

                If (nPK > 0) then
                    qryEndereconCdMunicipio.Value := nPK ;

            end ;

            if (DBGridEH2.Col = 15) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(22);

                If (nPK > 0) then
                    qryEndereconCdTipoEnd.Value := nPK ;

            end ;

            if (DBGridEH2.Col = 17) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(21);

                If (nPK > 0) then
                    qryEndereconCdRegiao.Value := nPK ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.btSalvarClick(Sender: TObject);
begin

  if (qryMaster.State = dsEdit) and (qryTerceiroTipoTerceiro.Eof) then
  begin
      MensagemAlerta('Nenhuma categoria informada para este terceiro.') ;
      abort ;
  end ;

  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmTerceiro.btCancelarClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmTerceiro.ToolButton1Click(Sender: TObject);
var
  nPK : integer ;
begin
  if (bRepresentante) then
  begin
      nPk := frmMenu.BuscaPK() ;
      if (nPk > 0) then
      begin
          qryValidaTerceiroxRepresentante.Close;
          qryValidaTerceiroxRepresentante.Parameters.ParamByName('nCdTerceiro').Value       := nPK;
          qryValidaTerceiroxRepresentante.Parameters.ParamByName('nCdTerceiroRepres').Value := frmMenu.nCdTerceiroUsuario;
          qryValidaTerceiroxRepresentante.Open;

          if ( qryValidaTerceiroxRepresentante.IsEmpty ) then
          begin
              MensagemAlerta('Terceiro n�o pertence ao Representante logado.') ;
              Exit ;
          end;
          PosicionaPK(nPk) ;
      end;
  end
  else inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmTerceiro.DBEdit15KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(15);

            If (nPK > 0) then
            begin
                qryMasternCdFormaPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.DBEdit16KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(23);

            If (nPK > 0) then
            begin
                qryMasternCdBanco.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.DBEdit22KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(16);

            If (nPK > 0) then
            begin
                qryMasternCdMoeda.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.DBEdit23KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.DBEdit28KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(61);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.DBEdit38KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(94);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoEconomico.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.qryContatoTerceiroBeforePost(DataSet: TDataSet);
begin

  if (qryContatoTerceirocNmContato.Value = '') then
  begin
      MensagemAlerta('Informe o nome do contato.') ;
      abort ;
  end ;

  qryContatoTerceirocNmContato.Value    := Uppercase(qryContatoTerceirocNmContato.Value) ;
  qryContatoTerceirocDepartamento.Value := Uppercase (qryContatoTerceirocDepartamento.Value) ;
  qryContatoTerceironCdTerceiro.Value   := qryMasternCdTerceiro.Value ;

  if (qryContatoTerceiro.State = dsInsert) then
      qryContatoTerceironCdContatoTerceiro.Value := frmMenu.fnProximoCodigo('CONTATOTERCEIRO') ;

  inherited;


end;

procedure TfrmTerceiro.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryTerceiroTipoTerceiro.Close ;
  qryTerceiroTipoTerceiro.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  qryTerceiroTipoTerceiro.Open ;

  qryEndereco.Close ;
  qryEndereco.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  qryEndereco.Open ;

  PosicionaQuery(qryContatoTerceiro, qryMasternCdTerceiro.asString) ;
  PosicionaQuery(qryTerceiroRepresentante, qryMasternCdTerceiro.asString) ;

  qryTerceiroCategFinanc.Close;
  PosicionaQuery( qryTerceiroCategFinanc, qryMasternCdTerceiro.asString ) ;

  qryTerceiroTipoTabPreco.Close;
  PosicionaQuery(qryTerceiroTipoTabPreco, qryMasternCdTerceiro.asString) ;

end;

procedure TfrmTerceiro.DBEdit10Exit(Sender: TObject);
begin
  inherited;

  btSalvar.Click ;
end;

procedure TfrmTerceiro.DBEdit10Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMasternCdStatus.Value := 1 ;
end;

procedure TfrmTerceiro.DBEdit46Exit(Sender: TObject);
begin
  inherited;

  qryRamoAtividade.Close ;
  PosicionaQuery(qryRamoAtividade, DBEdit46.Text);
  
end;

procedure TfrmTerceiro.DBEdit38Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close ;
  PosicionaQuery(qryGrupoEconomico, DBEdit38.Text) ;
  
end;

procedure TfrmTerceiro.DBEdit46KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(158);

            If (nPK > 0) then
            begin
                qryMasternCdRamoAtividade.Value := nPK ;
                PosicionaQuery(qryRamoAtividade, IntToStr(nPK)) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.DBEdit8Exit(Sender: TObject);
begin
  inherited;

  qryTerceiroPagador.Close ;
  PosicionaQuery(qryTerceiroPagador, DBEdit8.Text) ;
  
end;

procedure TfrmTerceiro.DBEdit28Exit(Sender: TObject);
begin
  inherited;

  qryCondPagto.Close ;
  PosicionaQuery(qryCondPagto, DBEdit28.Text) ;
  
end;

procedure TfrmTerceiro.DBEdit15Exit(Sender: TObject);
begin
  inherited;

  qryFormaPagto.Close ;
  PosicionaQuery(qryFormaPagto, DBEdit15.Text) ;
  
end;

procedure TfrmTerceiro.DBEdit9Exit(Sender: TObject);
begin
  inherited;

  qryTerceiroTransp.Close ;
  PosicionaQuery(qryTerceiroTransp, DBEdit9.Text) ;
  
end;

procedure TfrmTerceiro.DBEdit22Exit(Sender: TObject);
begin
  inherited;

  qryMoeda.Close ;
  PosicionaQuery(qryMoeda, DBEdit22.Text) ;
  
end;

procedure TfrmTerceiro.DBEdit25Exit(Sender: TObject);
begin
  inherited;

  qryGrupoComissao.Close ;
  PosicionaQuery(qryGrupoComissao, DbEdit25.Text) ;

end;

procedure TfrmTerceiro.DBEdit25KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(159);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoComissao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.ToolButton3Click(Sender: TObject);
var
    objForm : TfrmCentralArquivos ;
begin
  inherited;
  if not qryMaster.Active then
      exit ;

  if (qryMasternCdTerceiro.Value > 0) then
  begin

      objForm := TfrmCentralArquivos.Create(Self) ;
      objForm.GerenciaArquivos('TERCEIRO',qryMasternCdTerceiro.Value) ;

  end ;

end;

procedure TfrmTerceiro.DBEdit52Exit(Sender: TObject);
begin
  inherited;

  qryTipoTabPreco.Close;
  PosicionaQuery(qryTipoTabPreco,DBEdit52.Text) ;

end;

procedure TfrmTerceiro.DBEdit52KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(192);

            If (nPK > 0) then
            begin
                qryMasternCdTipoTabPreco.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.qryEnderecoCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryEndereco.Active) then
  begin

      if (qryEndereconCdPais.Value > 0) then
      begin

          qryPais.Close;
          PosicionaQuery(qryPais, qryEndereconCdPais.AsString) ;

          if not qryPais.Eof then
              qryEnderecocxxNmPais.Value := qryPaiscNmPais.Value ;
          
      end ;

      if (qryEndereconCdEstado.Value > 0) then
      begin

          qryEstado.Close;
          qryEstado.Parameters.ParamByName('nCdPais').Value := qryEndereconCdPais.Value ;
          PosicionaQuery(qryEstado, qryEndereconCdEstado.AsString) ;

          if not qryEstado.Eof then
              qryEnderecocxxNmEstado.Value := qryEstadocUF.Value ;

      end ;

      if (qryEndereconCdMunicipio.Value > 0) then
      begin

          qryMunicipio.Close;
          qryMunicipio.Parameters.ParamByName('nCdEstado').Value := qryEndereconCdEstado.Value ;
          PosicionaQuery(qryMunicipio, qryEndereconCdMunicipio.AsString) ;

          if not qryMunicipio.Eof then
              qryEnderecocxxNmMunicipio.Value := qryMunicipiocNmMunicipio.Value ;

      end ;

      if qryEndereconCdTabTipoLogradouro.Value > 0 then
      begin
          PosicionaQuery(qryTipoLogradouro,qryEndereconCdTabTipoLogradouro.AsString);
          qryEnderecocDescricaoLogradouro.Value := qryTipoLogradourocDescricao.Value;
      end;
  end ;

end;

procedure TfrmTerceiro.DBGridEh2ColExit(Sender: TObject);
var
  I : integer;
begin
  inherited;

  if (qryEndereco.State in [dsInsert,dsEdit]) and (DBGridEH2.Col = 2) then
  begin
      qryBuscaEnderecoCEP.Close;
      PosicionaQuery(qryBuscaEnderecoCEP, qryEnderecocCEP.Value) ;

      if (not qryBuscaEnderecoCEP.Eof) then
      begin
          qryEnderecocEndereco.Value    := qryBuscaEnderecoCEPcEndereco.Value ;
          qryEnderecocBairro.Value      := qryBuscaEnderecoCEPcBairro.Value ;
          qryEndereconCdPais.Value      := qryBuscaEnderecoCEPnCdPais.Value ;
          qryEnderecocNmPais.Value      := qryBuscaEnderecoCEPcNmPais.Value ;
          qryEndereconCdEstado.Value    := qryBuscaEnderecoCEPnCdEstado.Value ;
          qryEnderecocUF.Value          := qryBuscaEnderecoCEPcUF.Value ;
          qryEndereconCdMunicipio.Value := qryBuscaEnderecoCEPnCdMunicipio.Value ;
          qryEnderecocCidade.Value      := qryBuscaEnderecoCEPcNmMunicipio.Value ;

          qryBuscaEnderecoCEP.Close ;
      end
      else
      begin
          if (Trim(qryEnderecocCEP.Value) = '') then
              Exit;
              
          { -- Efetua consulta atrav�s do componente ACBrCEP caso endere�o n�o exista na base de dados -- }
          { -- WebService: wsByJG / login: admsoft / senha: ed101580 / site: http://www.byjg.com.br -- }
          try
              ACBrCEP1.BuscarPorCEP(qryEnderecocCEP.Value);

              if (ACBrCEP1.Enderecos.Count < 1) then
                  MensagemAlerta('CEP n�o localizado em nossa base de endere�os.')
              else
              begin
                  for i := 0 to ACBrCEP1.Enderecos.Count-1 do
                  begin
                      with ACBrCEP1.Enderecos[i] do
                      begin
                          qryEnderecocEndereco.Value    := Logradouro ;
                          qryEnderecocComplemento.Value := Complemento ;
                          qryEnderecocBairro.Value      := Bairro ;
                          qryEnderecocCidade.Value      := Municipio ;
                          qryEnderecocUF.Value          := UF ;
                      end ;
                  end ;
              end ;
          except
              MensagemErro('Erro no processamento.');
              Raise;
          end;
      end;
  end ;
end;

procedure TfrmTerceiro.btPosicaoReceberClick(Sender: TObject);
var
    objForm : TfrmTerceiro_PosicaoReceber ;
begin
  inherited;
  if (qryMaster.Active) then
  begin

      objForm := TfrmTerceiro_PosicaoReceber.Create(nil) ;

      objForm.exibeTitulos(qryMasternCdTerceiro.Value
                          ,FALSE);

      FreeAndNil(objForm) ;

  end ;

end;

procedure TfrmTerceiro.btPosicaoRecebAtrasoClick(Sender: TObject);
var
    objForm : TfrmTerceiro_PosicaoReceber ;
begin
  inherited;
  if (qryMaster.Active) then
  begin

      objForm := TfrmTerceiro_PosicaoReceber.Create(nil) ;

      objForm.exibeTitulos(qryMasternCdTerceiro.Value
                          ,TRUE);

      FreeAndNil(objForm) ;

  end ;

end;

procedure TfrmTerceiro.btVendaAutorizadaClick(Sender: TObject);
var
    objForm : TfrmTerceiro_VendaAutorizada ;
begin
  inherited;

  objForm := TfrmTerceiro_VendaAutorizada.Create(nil) ;

  if (qryMaster.Active) then
  begin

      objForm.exibeValores(qryMasternCdTerceiro.Value);

  end ;

end;

procedure TfrmTerceiro.btChequePendClick(Sender: TObject);
var
  objForm : TfrmTerceiro_ChequePendente ;
begin
  inherited;

  if (qryMaster.Active) then
  begin

      objForm := TfrmTerceiro_ChequePendente.Create(nil) ;

      objForm.exibeValores(qryMasternCdTerceiro.Value,0);

      FreeAndNil(objForm) ;

  end ;

end;

procedure TfrmTerceiro.btChequePendRiscoClick(Sender: TObject);
var
  objForm : TfrmTerceiro_ChequePendente ;
begin
  inherited;

  if qryMaster.Active then
  begin

      objForm := TfrmTerceiro_ChequePendente.Create(nil) ;

      objForm.exibeValores(qryMasternCdTerceiro.Value,1);

      FreeAndNil(objForm) ;

  end ;

end;

procedure TfrmTerceiro.btVendaClick(Sender: TObject);
var
  dataInicial, dataFinal : String ;
  objForm : TdcmVendaProdutoCliente_view ;
begin

  if qryMaster.Active then
  begin

      dataInicial := DateToStr(date()-365);
      dataFinal   := DateToStr(date());

      objForm := TdcmVendaProdutoCliente_view.Create(nil) ;

      objForm.ADODataSet1.Close ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdGrupoEconomico').Value := 0 ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdTerceiro').Value       := qryMasternCdTerceiro.Value;
      objForm.ADODataSet1.Parameters.ParamByName('nCdDepartamento').Value   := 0 ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdMarca').Value          := 0 ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdLinha').Value          := 0 ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdClasseProduto').Value  := 0 ;
      objForm.ADODataSet1.Parameters.ParamByName('dDtInicial').Value        := frmMenu.ConvData(dataInicial) ;
      objForm.ADODataSet1.Parameters.ParamByName('dDtFinal').Value          := frmMenu.ConvData(dataFinal) ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdRamoAtividade').Value  := 0 ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdTerceiroRepres').Value := 0 ;
      objForm.ADODataSet1.Open ;

      if (objForm.ADODataSet1.eof) then
      begin
          ShowMessage('Nenhuma informa��o encontrada.') ;
          FreeAndNil(objForm) ;
          exit ;
      end ;

      frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

      if objForm.PivotCube1.Active then
      begin
          objForm.PivotCube1.Active := False;
      end;

      objForm.PivotCube1.ExtendedMode := True;
      objForm.PVMeasureToolBar1.HideButtons := False;
      objForm.PivotCube1.Active := True;

      objForm.PivotMap1.Measures[2].ColumnPercent := True;
      objForm.PivotMap1.Measures[2].Value := False;

      objForm.PivotMap1.Measures[3].Rank := True ;
      objForm.PivotMap1.Measures[3].Value := False;

      objForm.PivotMap1.SortColumn(0,3,False) ;

      objForm.PivotMap1.Title := 'ER2Soft - An�lise de Vendas por Clientes' ;
      objForm.PivotGrid1.RefreshData;

      frmMenu.StatusBar1.Panels[6].Text := '' ;

      objForm.ShowModal ;
      Self.Activate ;

  end;

end;

procedure TfrmTerceiro.qryTerceiroCpfRiscoBeforePost(DataSet: TDataSet);
begin

  if (qryTerceiroCpfRiscocCNPJCPF.Value = '') then
  begin
      ShowMessage('CPF / CNPJ Inv�lido');
      Abort ;
  end ;

  if (qryTerceiroCpfRiscocNmEmissor.Value = '') then
  begin
      ShowMessage('Nome Inv�lido');
      Abort ;
  end ;

  if ((qryTerceiroCpfRiscocCNPJCPF.Value <> '') and (frmMenu.TestaCpfCgc(qryTerceiroCpfRiscocCNPJCPF.Value) = '')) then
  begin
      ShowMessage('CPF / CNPJ Inv�lido');
      Abort ;
  end ;

  qryTerceiroCpfRisconCdTerceiro.Value := qryMasternCdTerceiro.Value;
  qryTerceiroCpfRiscocNmEmissor.Value  := UpperCase(qryTerceiroCpfRiscocNmEmissor.Value);
  if (qryTerceiroCpfRisco.State = dsInsert) then
      qryTerceiroCpfRisconCdTerceiroCpfRisco.Value := frmMenu.fnProximoCodigo('TERCEIROCPFRISCO') ;

  inherited;

end;

procedure TfrmTerceiro.btMediaVendaClick(Sender: TObject);
var
  objForm : TfrmTerceiro_MediaVenda;
begin
  inherited;

  if (not qryMediaVenda.Active) or (qryMediaVendanValorMedio.Value = 0) then
  begin
      MensagemAlerta('Nenhuma venda nos �ltimos 12 meses para esse terceiro.') ;
      abort ;
  end ;

  objForm := TfrmTerceiro_MediaVenda.Create(nil) ;
  objForm.exibeValores(qryMasternCdTerceiro.Value);

  FreeAndNil( objForm ) ;

end;

procedure TfrmTerceiro.FormShow(Sender: TObject);
begin
  inherited;
  qryUsuario.Close;
  posicionaQuery(qryUsuario, intToStr(frmMenu.nCdUsuarioLogado));
  bRepresentante :=  (qryUsuariocFlgRepresentante.Value = 1);
  qryUsuario.Close;
end;

procedure TfrmTerceiro.btConsultarClick(Sender: TObject);
var
  nPk : Integer;
begin
  if (bRepresentante) then
  begin
      nPK := frmLookup_Padrao.ExecutaConsulta2(225,'EXISTS(SELECT 1 FROM TerceiroRepresentante WHERE nCdTerceiroRepres = ' + intToStr(frmMenu.nCdTerceiroUsuario) + ' AND nCdTerceiro = Terceiro.nCdTerceiro)');
      PosicionaPK(nPK);
  end
  else inherited;

end;

procedure TfrmTerceiro.edtDivisaoVendaBeforeLookup(Sender: TObject);
begin
  inherited;

  if (Trim(edtAreaVenda.Text) = '') then
  begin
      MensagemAlerta('Para selecionar a Divis�o de Venda, selecione a �rea de Venda.');
      edtAreaVenda.SetFocus;
      abort;
  end;

  edtDivisaoVenda.WhereAdicional.Text := 'nCdStatus = 1 AND nCdAreaVenda = ' + edtAreaVenda.Text;

end;

procedure TfrmTerceiro.edtDivisaoVendaBeforePosicionaQry(Sender: TObject);
begin
  inherited;

  if (Trim(edtAreaVenda.Text) = '') then
      exit;
      
  qryDivisaoVenda.Parameters.ParamByName('nCdAreaVenda').Value := edtAreaVenda.Text;
end;

procedure TfrmTerceiro.qryTerceiroCategFinancBeforePost(DataSet: TDataSet);
begin

  if (qryTerceiroCategFinanccNmCategFinanc.Value = '') then
  begin
      MensagemAlerta('Selecione a categoria financeira.') ;
      abort;
  end ;

  inherited;

  qryTerceiroCategFinancnCdTerceiro.Value            := qryMasternCdTerceiro.Value;
  qryTerceiroCategFinancnCdTerceiroCategFinanc.Value := frmMenu.fnProximoCodigo('TERCEIROCATEGFINANC') ;

end;

procedure TfrmTerceiro.DBGridEh5Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;

end;

procedure TfrmTerceiro.qryTerceiroCategFinancCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  inherited;

  qryCategFinanc.Close;
  PosicionaQuery(qryCategFinanc, qryTerceiroCategFinancnCdCategFinanc.AsString) ;

  qryTerceiroCategFinanccNmCategFinanc.Value := qryCategFinanccNmCategFinanc.Value;

end;

procedure TfrmTerceiro.DBGridEh5KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTerceiroCategFinanc.State = dsBrowse) then
            qryTerceiroCategFinanc.Edit ;

            nPK := frmLookup_Padrao.ExecutaConsulta(13);

            If (nPK > 0) then
                qryTerceiroCategFinancnCdCategFinanc.Value := nPK ;

    end ;

  end ;

end;

procedure TfrmTerceiro.DBGridEh6Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;
  
end;

procedure TfrmTerceiro.DBGridEh6KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTerceiroRepresentante.State = dsBrowse) then
            qryTerceiroRepresentante.Edit ;

            nPK := frmLookup_Padrao.ExecutaConsulta(103);

            If (nPK > 0) then
                qryTerceiroRepresentantenCdTerceiroRepres.Value := nPK ;

    end ;

  end ;

end;

procedure TfrmTerceiro.qryTerceiroRepresentanteBeforePost(
  DataSet: TDataSet);
begin

  if ( qryTerceiroRepresentantecNmTerceiro.Value = '' ) then
  begin
      MensagemAlerta('Selecione um terceiro representante.') ;
      abort;
  end ;

  inherited;

  if (qryTerceiroRepresentante.State = dsInsert) then
      qryTerceiroRepresentantenCdTerceiroRepresentante.Value := frmMenu.fnProximoCodigo('TERCEIROREPRESENTANTE');

  qryTerceiroRepresentantenCdTerceiro.Value := qryMasternCdTerceiro.Value;

end;

procedure TfrmTerceiro.qryTerceiroRepresentanteCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  inherited;

  qryTerceiroRepres2.Close;
  PosicionaQuery(qryTerceiroRepres2, qryTerceiroRepresentantenCdTerceiroRepres.AsString) ;

  qryTerceiroRepresentantecNmTerceiro.Value := qryTerceiroRepres2cNmTerceiro.Value;

end;

procedure TfrmTerceiro.qryTerceiroTipoTabPrecoBeforePost(
  DataSet: TDataSet);
begin

  if ( qryTerceiroTipoTabPrecocnmTipoPedido.Value = '' ) then
  begin
      MensagemAlerta('Selecione um tipo de pedido.') ;
      abort;
  end ;

  if ( qryTerceiroTipoTabPrecocnmTipoTabPreco.Value = '' ) then
  begin
      MensagemAlerta('Selecione um tipo de tabela de pre�o.') ;
      abort;
  end ;

  inherited;

  if (qryTerceiroTipoTabPreco.State = dsInsert) then
      qryTerceiroTipoTabPreconCdTerceiroTipoTabPreco.Value := frmMenu.fnProximoCodigo('TERCEIROTIPOTABPRECO');

  qryTerceiroTipoTabPreconCdTerceiro.Value := qryMasternCdTerceiro.Value;
  
end;

procedure TfrmTerceiro.DBGridEh7KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTerceiroTipoTabPreco.State = dsBrowse) then
             qryTerceiroTipoTabPreco.Edit ;

        if (DBGridEh7.Col = 3) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(54);

            If (nPK > 0) then
                qryTerceiroTipoTabPreconCdTipoPedido.Value := nPK ;

        end ;

        if (DBGridEh7.Col = 5) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(192);

            If (nPK > 0) then
                qryTerceiroTipoTabPreconCdTipoTabPreco.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmTerceiro.DBGridEh7Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert ) then
      qryMaster.Post;

end;

procedure TfrmTerceiro.qryTerceiroTipoTabPrecoCalcFields(
  DataSet: TDataSet);
begin

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  inherited;

  qryTipoPedido.Close;
  PosicionaQuery(qryTipoPedido, qryTerceiroTipoTabPreconCdTipoPedido.AsString) ;

  if not qryTipoPedido.IsEmpty then
      qryTerceiroTipoTabPrecocNmTipoPedido.Value := qryTipoPedidocNmTipoPedido.Value;


  qryTipoTabPrecoAux.Close;
  PosicionaQuery(qryTipoTabPrecoAux, qryTerceiroTipoTabPreconCdTipoTabPreco.AsString) ;

  if not qryTipoTabPrecoAux.IsEmpty then
      qryTerceiroTipoTabPrecocNmTipoTabPreco.Value := qryTipoTabPrecoAuxcNmTipoTabPreco.Value;

end;

procedure TfrmTerceiro.er2LkpTabTipoEnquadTributarioExit(Sender: TObject);
begin
  inherited;

  qryTabTipoEnquadTributario.Close;
  PosicionaQuery(qryTabTipoEnquadTributario, er2LkpTabTipoEnquadTributario.Text);
end;

procedure TfrmTerceiro.er2LkpTabTipoPorteEmpExit(Sender: TObject);
begin
  inherited;

  qryTabTipoPorteEmp.Close;
  PosicionaQuery(qryTabTipoPorteEmp, er2LkpTabTipoPorteEmp.Text);
end;

initialization
  RegisterClass(tfrmTerceiro);

end.
