unit fCaixa_DescontoJuros;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, DB, GridsEh, DBGridEh, ADODB,
  cxLookAndFeelPainters, StdCtrls, cxButtons, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmCaixa_DescontoJuros = class(TForm)
    Image1: TImage;
    qryParcelas: TADOQuery;
    qryParcelasnCdTitulo: TIntegerField;
    qryParcelascNrTit: TStringField;
    qryParcelascNmEspTit: TStringField;
    qryParcelasdDtEmissao: TDateTimeField;
    qryParcelasdDtVenc: TDateTimeField;
    qryParcelasnValTit: TBCDField;
    qryParcelasnValJuro: TBCDField;
    qryParcelasnValDesconto: TBCDField;
    qryParcelasnSaldoTit: TBCDField;
    qryParcelascNmLoja: TStringField;
    qryParcelasiDiasAtraso: TIntegerField;
    qryParcelascFlgEntrada: TIntegerField;
    qryParcelasnCdCrediario: TIntegerField;
    qryParcelasiParcela: TIntegerField;
    qryParcelascFlgMarcado: TIntegerField;
    qryParcelasnCdTerceiro: TIntegerField;
    qryParcelascFlgDescontoJuros: TIntegerField;
    qryParcelasnValJurosDescontado: TBCDField;
    DBGridEh1: TDBGridEh;
    dsParcelas: TDataSource;
    btFechar: TcxButton;
    procedure ExibeParcelas();
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btFecharClick(Sender: TObject);
    procedure qryParcelasBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_DescontoJuros: TfrmCaixa_DescontoJuros;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmCaixa_DescontoJuros }

procedure TfrmCaixa_DescontoJuros.ExibeParcelas;
begin

    qryParcelas.Close;
    qryParcelas.Open ;

    Self.ShowModal;

end;

procedure TfrmCaixa_DescontoJuros.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if (Key = VK_ESCAPE) then
    begin

        if (qryParcelas.State <> dsBrowse) then
            qryParcelas.Post;

        Close;
    end ;

end;

procedure TfrmCaixa_DescontoJuros.btFecharClick(Sender: TObject);
begin

    if (qryParcelas.State <> dsBrowse) then
        qryParcelas.Post;

    close ;
end;

procedure TfrmCaixa_DescontoJuros.qryParcelasBeforePost(DataSet: TDataSet);
begin

    if (qryParcelasnValJuro.Value < qryParcelasnValJurosDescontado.Value) then
    begin
        qryParcelasnValJurosDescontado.Value := 0 ;
        frmMenu.MensagemErro('O Valor do desconto n�o pode ser maior que o valor do juros.') ;
        abort ;
    end ;

end;

end.
