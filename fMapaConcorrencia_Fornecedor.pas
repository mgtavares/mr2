unit fMapaConcorrencia_Fornecedor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  cxLookAndFeelPainters, cxButtons, DB, ADODB;

type
  TfrmMapaConcorrencia_Fornecedor = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    edtNome1: TEdit;
    edtFone1: TEdit;
    edtContato1: TEdit;
    edtEmail1: TEdit;
    btSel1: TcxButton;
    edtNome2: TEdit;
    edtFone2: TEdit;
    edtContato2: TEdit;
    edtEmail2: TEdit;
    btSel2: TcxButton;
    edtNome3: TEdit;
    edtFone3: TEdit;
    edtContato3: TEdit;
    edtEmail3: TEdit;
    btSel3: TcxButton;
    edtID1: TEdit;
    edtID2: TEdit;
    edtID3: TEdit;
    edtNmMapa: TEdit;
    Label8: TLabel;
    SP_ADD_FORNEC_MAPA: TADOStoredProc;
    SP_GERA_MAPA_COMPRA: TADOStoredProc;
    SP_EXPLODE_ITENS_MAPA: TADOStoredProc;
    qryFornecedor: TADOQuery;
    qryFornecedorcNmFantasia: TStringField;
    qryFornecedorcNmContato: TStringField;
    qryFornecedorcTelefone: TStringField;
    qryFornecedorcEmail: TStringField;
    procedure LimpaDados();
    procedure ToolButton1Click(Sender: TObject);
    procedure btSel1Click(Sender: TObject);
    procedure btSel2Click(Sender: TObject);
    procedure btSel3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMapaConcorrencia_Fornecedor: TfrmMapaConcorrencia_Fornecedor;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmMapaConcorrencia_Fornecedor.LimpaDados();
begin

    edtNome1.Text := '' ;
    edtNome2.Text := '' ;
    edtNome3.Text := '' ;

    edtFone1.Text := '' ;
    edtFone2.Text := '' ;
    edtFone3.Text := '' ;

    edtContato1.Text := '' ;
    edtContato2.Text := '' ;
    edtContato3.Text := '' ;

    edtEmail1.Text := '' ;
    edtEmail2.Text := '' ;
    edtEmail3.Text := '' ;

    edtNmMapa.Text := '' ;

    edtID1.Text := '0' ;
    edtID2.Text := '0' ;
    edtID3.Text := '0' ;

end ;

procedure TfrmMapaConcorrencia_Fornecedor.ToolButton1Click(
  Sender: TObject);
var
    nCdMapaCompra : integer ;
begin
  inherited;

  if (edtID1.Text = '') then
      edtID1.Text := '0' ;

  if (edtID2.Text = '') then
      edtID2.Text := '0' ;

  if (edtID3.Text = '') then
      edtID3.Text := '0' ;

  if (Trim(edtNome1.Text) = '') then
  begin
      MensagemAlerta('Informe o nome do primeiro fornecedor.') ;
      edtNome1.SetFocus;
      exit ;
  end ;

  case MessageDlg('Confirma o(s) fornecedore(s) do mapa ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_GERA_MAPA_COMPRA.Close ;
      SP_GERA_MAPA_COMPRA.Parameters.ParamByName('@nCdEmpresa').Value    := frmMenu.nCdEmpresaAtiva ;
      SP_GERA_MAPA_COMPRA.Parameters.ParamByName('@nCdUsuario').Value    := frmMenu.nCdUsuarioLogado;
      SP_GERA_MAPA_COMPRA.Parameters.ParamByName('@cNmMapaCompra').Value := edtNmMapa.Text          ;
      SP_GERA_MAPA_COMPRA.ExecProc ;

      nCdMapaCompra := SP_GERA_MAPA_COMPRA.Parameters.ParamByName('@nCdMapaCompra').Value ;

      // Adiciona os fornecedores
      SP_ADD_FORNEC_MAPA.Close ;
      SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@nCdMapaCompra').Value := nCdMapaCompra         ;
      SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@nCdTerceiro').Value   := StrToInt(edtID1.Text) ;
      SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@cNmFornecedor').Value := edtNome1.Text         ;
      SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@cNmContato').Value    := edtContato1.Text      ;
      SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@cTelefone').Value     := edtFone1.Text         ;
      SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@cEmail').Value        := edtEmail1.Text        ;
      SP_ADD_FORNEC_MAPA.ExecProc ;

      if (Trim(edtNome2.Text) <> '') then
      begin
          SP_ADD_FORNEC_MAPA.Close ;
          SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@nCdMapaCompra').Value := nCdMapaCompra         ;
          SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@nCdTerceiro').Value   := StrToInt(edtID2.Text) ;
          SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@cNmFornecedor').Value := edtNome2.Text         ;
          SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@cNmContato').Value    := edtContato2.Text      ;
          SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@cTelefone').Value     := edtFone2.Text         ;
          SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@cEmail').Value        := edtEmail2.Text        ;
          SP_ADD_FORNEC_MAPA.ExecProc ;
      end ;

      if (Trim(edtNome3.Text) <> '') then
      begin
          SP_ADD_FORNEC_MAPA.Close ;
          SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@nCdMapaCompra').Value := nCdMapaCompra         ;
          SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@nCdTerceiro').Value   := StrToInt(edtID3.Text) ;
          SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@cNmFornecedor').Value := edtNome3.Text         ;
          SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@cNmContato').Value    := edtContato3.Text      ;
          SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@cTelefone').Value     := edtFone3.Text         ;
          SP_ADD_FORNEC_MAPA.Parameters.ParamByName('@cEmail').Value        := edtEmail3.Text        ;
          SP_ADD_FORNEC_MAPA.ExecProc ;
      end ;

      SP_EXPLODE_ITENS_MAPA.Close ;
      SP_EXPLODE_ITENS_MAPA.Parameters.ParamByName('@nCdMapaCompra').Value := nCdMapaCompra ;
      SP_EXPLODE_ITENS_MAPA.ExecProc ;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Mapa de Compra gerado com sucesso!.' + #13#13 + 'N�mero do mapa: ' + IntToStr(nCdMapaCompra)) ;

  Close ;

end;

procedure TfrmMapaConcorrencia_Fornecedor.btSel1Click(Sender: TObject);
var
    nPK : Integer ;
begin
  inherited;

  nPK := frmLookup_Padrao.ExecutaConsulta(90);

  If (nPK > 0) then
  begin

      edtID1.Text := IntToStr(nPK) ;
      PosicionaQuery(qryFornecedor, edtID1.Text) ;

      if not qryFornecedor.eof then
      begin
          edtNome1.Text    := qryFornecedorcNmFantasia.Value ;
          edtContato1.Text := qryFornecedorcNmContato.Value  ;
          edtFone1.Text    := qryFornecedorcTelefone.Value   ;
          edtEmail1.Text   := qryFornecedorcEmail.Value      ;
      end ;

  end ;

end;

procedure TfrmMapaConcorrencia_Fornecedor.btSel2Click(Sender: TObject);
var
    nPK : Integer ;
begin
  inherited;

  nPK := frmLookup_Padrao.ExecutaConsulta(90);

  If (nPK > 0) then
  begin

      edtID2.Text := IntToStr(nPK) ;
      PosicionaQuery(qryFornecedor, edtID2.Text) ;

      if not qryFornecedor.eof then
      begin
          edtNome2.Text    := qryFornecedorcNmFantasia.Value ;
          edtContato2.Text := qryFornecedorcNmContato.Value  ;
          edtFone2.Text    := qryFornecedorcTelefone.Value   ;
          edtEmail2.Text   := qryFornecedorcEmail.Value      ;
      end ;

  end ;

end;

procedure TfrmMapaConcorrencia_Fornecedor.btSel3Click(Sender: TObject);
var
    nPK : Integer ;
begin
  inherited;

  nPK := frmLookup_Padrao.ExecutaConsulta(90);

  If (nPK > 0) then
  begin

      edtID3.Text := IntToStr(nPK) ;
      PosicionaQuery(qryFornecedor, edtID3.Text) ;

      if not qryFornecedor.eof then
      begin
          edtNome3.Text    := qryFornecedorcNmFantasia.Value ;
          edtContato3.Text := qryFornecedorcNmContato.Value  ;
          edtFone3.Text    := qryFornecedorcTelefone.Value   ;
          edtEmail3.Text   := qryFornecedorcEmail.Value      ;
      end ;

  end ;

end;

procedure TfrmMapaConcorrencia_Fornecedor.FormShow(Sender: TObject);
begin
  inherited;

  edtNmMapa.SetFocus ;
  
end;

end.
