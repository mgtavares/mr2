inherited frmMotBloqPed: TfrmMotBloqPed
  Caption = 'Motivo Bloqueio Pedido'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 56
    Top = 38
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 58
    Top = 62
    Width = 36
    Height = 13
    Caption = 'Motivo'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 96
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdMotBloqPed'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 96
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmMotBloqPed'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBGridEh1: TDBGridEh [6]
    Left = 16
    Top = 88
    Width = 729
    Height = 345
    DataSource = dsTipoPedidoMotBloqPed
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    OnEnter = DBGridEh1Enter
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdMotBloqPed'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdTipoPedido'
        Footers = <>
        Width = 74
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoPedido'
        Footers = <>
        ReadOnly = True
        Width = 407
      end>
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterPost = qryMasterAfterPost
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM MotBloqPed'
      'WHERE nCdMotBloqPed = :nPK')
    object qryMasternCdMotBloqPed: TIntegerField
      FieldName = 'nCdMotBloqPed'
    end
    object qryMastercNmMotBloqPed: TStringField
      FieldName = 'cNmMotBloqPed'
      Size = 50
    end
  end
  object qryTipoPedidoMotBloqPed: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTipoPedidoMotBloqPedBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoPedidoMotBloqPed'
      'WHERE nCdMotBloqPed = :nPK')
    Left = 824
    Top = 248
    object qryTipoPedidoMotBloqPednCdMotBloqPed: TIntegerField
      FieldName = 'nCdMotBloqPed'
    end
    object qryTipoPedidoMotBloqPednCdTipoPedido: TIntegerField
      DisplayLabel = 'Tipos de Pedido quem podem ser bloqueados por este motivo|C'#243'd'
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidoMotBloqPedcNmTipoPedido: TStringField
      DisplayLabel = 
        'Tipos de Pedido quem podem ser bloqueados por este motivo|Descri' +
        #231#227'o'
      FieldKind = fkLookup
      FieldName = 'cNmTipoPedido'
      LookupDataSet = qryTipoPedido
      LookupKeyFields = 'nCdTipoPedido'
      LookupResultField = 'cNmTipoPedido'
      KeyFields = 'nCdTipoPedido'
      LookupCache = True
      Size = 50
      Lookup = True
    end
  end
  object dsTipoPedidoMotBloqPed: TDataSource
    DataSet = qryTipoPedidoMotBloqPed
    Left = 856
    Top = 248
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdTipoPedido'
      ',cNmTipoPedido'
      'FROM TipoPedido')
    Left = 888
    Top = 248
    object qryTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
end
