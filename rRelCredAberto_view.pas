unit rRelCredAberto_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, ExtCtrls, QRCtrls, jpeg, DB, ADODB;

type
  TrptRelCredAberto_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand2: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRBand3: TQRBand;
    QRShape6: TQRShape;
    QRBand5: TQRBand;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRBand1: TQRBand;
    lblTipoRelatorio: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    lblCodCred: TQRLabel;
    qryRelCredAberto: TADOQuery;
    dsRelCredAberto: TDataSource;
    qryRelCredAbertonCdCrediario: TIntegerField;
    qryRelCredAbertoDataVenda: TStringField;
    qryRelCredAbertonCdTerceiro: TIntegerField;
    qryRelCredAbertocNmTerceiro: TStringField;
    qryRelCredAbertoiParcelas: TIntegerField;
    qryRelCredAbertonValCrediario: TBCDField;
    qryRelCredAbertonSaldo: TBCDField;
    qryRelCredAbertoDataLiq: TStringField;
    QRLabel2: TQRLabel;
    QRlblCred: TQRLabel;
    QRDBText7: TQRDBText;
    QRDBText6: TQRDBText;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRelCredAberto_view: TrptRelCredAberto_view;

implementation

{$R *.dfm}
uses
  fMenu;

end.
