inherited frmGeraRemessaRecebimento_titulo: TfrmGeraRemessaRecebimento_titulo
  Width = 872
  Caption = 'Gera'#231#227'o Arquivo Remessa Recebimento'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 78
    Width = 856
    Height = 386
  end
  inherited ToolBar1: TToolBar
    Width = 856
    ButtonWidth = 100
    inherited ToolButton1: TToolButton
      Caption = '&Gerar Arquivo'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 100
    end
    inherited ToolButton2: TToolButton
      Left = 108
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 78
    Width = 856
    Height = 386
    Align = alClient
    AllowedOperations = []
    DataGrouping.GroupLevels = <>
    DataSource = dsTitulosRemessa
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footers = <
          item
          end>
      end
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footers = <
          item
          end>
      end
      item
        EditButtons = <>
        FieldName = 'cNrNF'
        Footers = <
          item
          end>
      end
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footers = <
          item
          end>
      end
      item
        EditButtons = <>
        FieldName = 'iParcela'
        Footers = <
          item
          end>
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <
          item
          end>
        Width = 244
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoTit'
        Footers = <
          item
            DisplayFormat = '#,##0.00'
            FieldName = 'nSaldoTit'
            ValueType = fvtSum
          end>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 856
    Height = 49
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Tag = 1
      Left = 16
      Top = 24
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Diret'#243'rio Arquivo'
    end
    object cxButton1: TcxButton
      Left = 540
      Top = 16
      Width = 27
      Height = 20
      TabOrder = 0
      OnClick = cxButton1Click
      Glyph.Data = {
        36090000424D3609000000000000360000002800000018000000180000000100
        20000000000000090000C30E0000C30E00000000000000000000D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00100D1300100D1300100D1300100D
        1300100D1300100D1300100D1300100D1300100D1300100D1300100D1300100D
        130009060A00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00100D130030A6DD0030A6DD0030A6
        DE002FA6DE0030A6DD0030A6DE0030A6DD0030A6DD002FA7DD0030A6DD000105
        0700100D1300D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00100D13003AABDF0039ABDF003AAADF0039AB
        DF003AABDF0039ABDF003AAADE003AAADF003AAADF003AABDF00020507003AAB
        DF00100D1300D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00100D140046B0E10046AFE10046B0E10046AF
        E10046B0E10045B0E00046B0E10045AFE10046AFE10046AFE1000206070046AF
        E100100D1400D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00100D140054B5E30067BFE80054B6E30053B6
        E30053B5E30053B6E30053B5E30053B6E30053B5E30054B6E3000306070053B5
        E400100D1400D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00110D140062BCE50062BCE60062BCE60062BCE60062BC
        E60062BCE50062BCE50063BBE60062BCE50063BCE50003090A0062BCE60062BC
        E500110D1400D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00110D140072C3E80072C2E80072C2E80080CBEC0072C3
        E80072C3E80072C3E80072C2E90072C3E80072C3E800040607007DC8EA0072C2
        E800110D1400D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00110D1400110D14000406070004060700040607000406
        070004060700040607000406070004060700040607000406070083CAEA0082CA
        EB00110D1400D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF000203040093D1EE0093D1EE0094D1
        ED0093D1EE0093D1EE0094D0ED0093D0ED0093D1EE0093D1EE0093D1EE0094D1
        ED00110D1400D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF0003030400A4D8F000A4D8F100A4D8
        F000A4D9F000A4D8F100A4D8F000A7D5F000120D1400120D1400120D1400120D
        1400120D1400D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00120D1400BAD3F000B5DF
        F300B5DFF300B5DFF200B9D3F000120D1400D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00120E1400120E
        1400120E1400120E1400120E1400D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200
        FF00D200FF00D200FF00D200FF00D200FF00D200FF00D200FF00}
      LookAndFeel.NativeStyle = True
    end
    object edtDiretorio: TEdit
      Left = 104
      Top = 16
      Width = 433
      Height = 21
      TabOrder = 1
    end
  end
  object qryTitulosRemessa: TADOQuery
    Connection = frmMenu.Connection
    AfterOpen = qryTitulosRemessaAfterOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.cNrNF'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.dDtEmissao'
      '      ,Titulo.iParcela'
      '      ,Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Titulo.nSaldoTit'
      '      ,ContaBancaria.cDiretorioArquivoRemessa'
      '      ,ContaBancaria.nCdContaBancaria'
      '      ,BorderoFinanceiro.nCdBorderoFinanceiro'
      '  FROM BorderoFinanceiro '
      
        '       INNER JOIN TituloBorderoFinanceiro TBF ON TBF.nCdBorderoF' +
        'inanceiro       = BorderoFinanceiro.nCdBorderoFinanceiro'
      
        '       INNER JOIN Titulo                      ON Titulo.nCdTitul' +
        'o               = TBF.nCdTitulo'
      
        '       LEFT  JOIN Terceiro                    ON Terceiro.nCdTer' +
        'ceiro           = Titulo.nCdTerceiro'
      
        '       INNER JOIN ContaBancaria               ON ContaBancaria.n' +
        'CdContaBancaria = BorderoFinanceiro.nCdContaBancaria'
      ' WHERE BorderoFinanceiro.nCdBorderoFinanceiro = :nPK')
    Left = 328
    Top = 216
    object qryTitulosRemessanCdTitulo: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'nCdTitulo'
    end
    object qryTitulosRemessacNrTit: TStringField
      DisplayLabel = 'N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosRemessacNrNF: TStringField
      DisplayLabel = 'N'#250'm. NF'
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTitulosRemessadDtVenc: TDateTimeField
      DisplayLabel = 'Data Venc.'
      FieldName = 'dDtVenc'
    end
    object qryTitulosRemessaiParcela: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'iParcela'
    end
    object qryTitulosRemessanCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTitulosRemessacNmTerceiro: TStringField
      DisplayLabel = 'Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTitulosRemessanSaldoTit: TBCDField
      DisplayLabel = 'Saldo T'#237'tulo'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosRemessacDiretorioArquivoRemessa: TStringField
      FieldName = 'cDiretorioArquivoRemessa'
      Size = 50
    end
    object qryTitulosRemessanCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryTitulosRemessadDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTitulosRemessanCdBorderoFinanceiro: TIntegerField
      FieldName = 'nCdBorderoFinanceiro'
    end
  end
  object dsTitulosRemessa: TDataSource
    DataSet = qryTitulosRemessa
    Left = 344
    Top = 248
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,cCedenteAgencia'
      '      ,cCedenteAgenciaDigito'
      '      ,cCedenteConta'
      '      ,cCedenteContaDigito'
      '      ,cCedenteCodCedente'
      '      ,cDiretorioArquivoLicenca'
      '      ,cPrimeiraInstrucaoBoleto'
      '      ,cSegundaInstrucaoBoleto'
      '      ,iUltimoBoleto'
      '      ,iSeqArquivoRemessa'
      '      ,nCdTabTipoLayOutRemessa'
      '      ,cFlgAceite'
      '      ,iDiasProtesto'
      '  FROM ContaBancaria'
      ' WHERE nCdContaBancaria = :nPK')
    Left = 416
    Top = 224
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariacCedenteAgencia: TStringField
      FieldName = 'cCedenteAgencia'
      Size = 5
    end
    object qryContaBancariacCedenteAgenciaDigito: TStringField
      FieldName = 'cCedenteAgenciaDigito'
      FixedChar = True
      Size = 1
    end
    object qryContaBancariacCedenteConta: TStringField
      FieldName = 'cCedenteConta'
      Size = 10
    end
    object qryContaBancariacCedenteContaDigito: TStringField
      FieldName = 'cCedenteContaDigito'
      FixedChar = True
      Size = 1
    end
    object qryContaBancariacCedenteCodCedente: TStringField
      FieldName = 'cCedenteCodCedente'
    end
    object qryContaBancariacDiretorioArquivoLicenca: TStringField
      FieldName = 'cDiretorioArquivoLicenca'
      Size = 50
    end
    object qryContaBancariacPrimeiraInstrucaoBoleto: TStringField
      FieldName = 'cPrimeiraInstrucaoBoleto'
      Size = 100
    end
    object qryContaBancariacSegundaInstrucaoBoleto: TStringField
      FieldName = 'cSegundaInstrucaoBoleto'
      Size = 100
    end
    object qryContaBancariaiUltimoBoleto: TIntegerField
      FieldName = 'iUltimoBoleto'
    end
    object qryContaBancariacFlgAceite: TIntegerField
      FieldName = 'cFlgAceite'
    end
    object qryContaBancariaiDiasProtesto: TIntegerField
      FieldName = 'iDiasProtesto'
    end
    object qryContaBancariaiSeqArquivoRemessa: TIntegerField
      FieldName = 'iSeqArquivoRemessa'
    end
    object qryContaBancarianCdTabTipoLayOutRemessa: TIntegerField
      FieldName = 'nCdTabTipoLayOutRemessa'
    end
  end
  object qryEndereco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro int'
      '       ,@nCdEndereco int'
      ''
      'SET @nCdTerceiro = :nCdTerceiro'
      ''
      ''
      'IF (@nCdEndereco IS NULL)'
      'BEGIN'
      ''
      #9'SELECT @nCdEndereco = nCdEndereco'
      #9'  FROM Endereco'
      #9' WHERE nCdTerceiro = @nCdTerceiro'
      #9'   AND nCdTipoEnd  = 2 -- Cobran'#231'a'
      #9'   AND nCdStatus   = 1 -- Ativo'
      ''
      #9'IF (@nCdEndereco IS NULL) '
      #9'BEGIN'
      ''
      #9#9'SELECT TOP 1 @nCdEndereco = nCdEndereco'
      #9#9'  FROM Endereco'
      #9#9' WHERE nCdTerceiro = @nCdTerceiro'
      #9#9'   AND nCdStatus   = 1 -- Ativo'
      #9#9' ORDER BY nCdTipoEnd'
      ''
      #9'END'
      ''
      'END'
      ''
      'SELECT nCdEndereco'
      '      ,Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Terceiro.cCNPJCPF'
      '      ,cEndereco + '#39', '#39' +  Convert(varchar,iNumero) cEndereco'
      '      ,cBairro'
      '      ,cCidade'
      '      ,cUF'
      '      ,cCep'
      '  FROM Endereco'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Endereco.nC' +
        'dTerceiro'
      ' WHERE nCdEndereco = @nCdEndereco')
    Left = 448
    Top = 224
    object qryEndereconCdEndereco: TIntegerField
      FieldName = 'nCdEndereco'
    end
    object qryEndereconCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryEnderecocEndereco: TStringField
      FieldName = 'cEndereco'
      ReadOnly = True
      Size = 82
    end
    object qryEnderecocBairro: TStringField
      FieldName = 'cBairro'
      Size = 35
    end
    object qryEnderecocCidade: TStringField
      FieldName = 'cCidade'
      Size = 35
    end
    object qryEnderecocUF: TStringField
      FieldName = 'cUF'
      FixedChar = True
      Size = 2
    end
    object qryEnderecocCEP: TStringField
      FieldName = 'cCEP'
      ReadOnly = True
      Size = 9
    end
    object qryEnderecocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryEnderecocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
  end
  object qryTabelaJuros: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nPercMulta'
      '      ,nPercJuroDia '
      '  FROM TabelaJuros'
      ' WHERE nCdEspTit = dbo.fn_LeParametro('#39'CDESPTIT-BOLETO'#39')'
      '   AND dDtValidade > GetDate() - 1')
    Left = 416
    Top = 256
    object qryTabelaJurosnPercMulta: TBCDField
      FieldName = 'nPercMulta'
      Precision = 12
      Size = 2
    end
    object qryTabelaJurosnPercJuroDia: TBCDField
      FieldName = 'nPercJuroDia'
      Precision = 12
    end
  end
  object qryBorderoFinanceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdBorderoFinanceiro'
      '      ,cNmArquivoRemessa'
      '      ,cNmComputadorGeracao'
      '      ,iNrSeqArquivoRemessa'
      '  FROM BorderoFinanceiro'
      ' WHERE nCdBorderoFinanceiro = :nPK')
    Left = 448
    Top = 256
    object qryBorderoFinanceironCdBorderoFinanceiro: TIntegerField
      FieldName = 'nCdBorderoFinanceiro'
    end
    object qryBorderoFinanceirocNmArquivoRemessa: TStringField
      FieldName = 'cNmArquivoRemessa'
      Size = 50
    end
    object qryBorderoFinanceirocNmComputadorGeracao: TStringField
      FieldName = 'cNmComputadorGeracao'
      Size = 50
    end
    object qryBorderoFinanceiroiNrSeqArquivoRemessa: TIntegerField
      FieldName = 'iNrSeqArquivoRemessa'
    end
  end
end
