unit pasEPL2;

interface

uses Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls, ExtCtrls, Printers, WinProcs, WinTypes;

// Fun��o para Imprimir Texto EPL2
procedure directPrint (s: String);
function Texto_EPL2(cPosicaoHorizontal, cPosicaoVertical, cRotacao, cFonte, cMultiplicadorH, cMultiplicadorV, cTipoImpressao, cTexto : string):string;
function Codigo_Barras_EPL2(cPosicaoHorizontal, cPosicaoVertical, cRotacao, cTipoBarra, cEspessuraBFina, cEspessuraBLarga, cAlturaBarra, cImprimeLegivel, cValor : string) : string;
function Linha_EPL2(cPosicaoHorizontal, cPosicaoVertical, cLargura, cAltura: String; iTipo : integer) : String;
function Abrir_Etiqueta_EPL2: String ;
function ImprimeEtiquetaEPL2(iCopias:integer): string ;
procedure EnviarImpressoraEPL2(cPorta, cTexto: string);

implementation

Type
    TPassThroughData = Record
            nLen : Integer;
            Data : Array[0..255] of byte;
    end;

procedure directPrint (s: String);
var
    PTBlock: TPassThroughData;
begin
    PTBlock.nLen := Length(s) ;
    StrPCopy(@PTBlock.Data,s) ;
    Escape(printer.handle, PASSTHROUGH, 0, @PTBlock, nil) ;
end ;

function Abrir_Etiqueta_EPL2: String ;
begin
    Result := 'N' + #13#10 ;
end ;

function Texto_EPL2(cPosicaoHorizontal, cPosicaoVertical, cRotacao, cFonte, cMultiplicadorH, cMultiplicadorV, cTipoImpressao, cTexto : string):string;
begin

    Result := 'A' + Trim(cPosicaoHorizontal) + ',' + Trim(cPosicaoVertical) + ',' + Trim(cRotacao) + ',' + Trim(cFonte) + ',' + Trim(cMultiplicadorH) + ',' + Trim(cMultiplicadorV) + ',' + Trim(cTipoImpressao) + ',"' + Trim(cTexto) + '"' + #13#10 ;

end ;

function Codigo_Barras_EPL2(cPosicaoHorizontal, cPosicaoVertical, cRotacao, cTipoBarra, cEspessuraBFina, cEspessuraBLarga, cAlturaBarra, cImprimeLegivel, cValor : string) : string;
begin
    Result := 'B' + Trim(cPosicaoHorizontal) + ',' + Trim(cPosicaoVertical) + ',' + Trim(cRotacao) + ',' + Trim(cTipoBarra) + ',' + Trim(cEspessuraBFina) + ',' + Trim(cEspessuraBLarga) + ',' + Trim(cAlturaBarra) + ',' + cImprimeLegivel + ',"' + Trim(cValor) + '"' + #13#10 ;
end ;

function Linha_EPL2(cPosicaoHorizontal, cPosicaoVertical, cLargura, cAltura: String; iTipo : integer) : String;
var
    cTipo :String;
begin

    case iTipo of
        0 : cTipo := 'O';
        1 : cTipo := 'W';
        2 : cTipo := 'E';
    end;

    Result := 'L' + cTipo + Trim(cPosicaoHorizontal) + ',' + Trim(cPosicaoVertical) + ',' + Trim(cLargura) + ',' + Trim(cAltura) + #13#10;
end;

function ImprimeEtiquetaEPL2(iCopias:integer): string ;
begin
    Result := 'P' + intToStr(iCopias) + #13#10 + 'N' + #13#10 ;
end ;

procedure EnviarImpressoraEPL2(cPorta, cTexto: string);
var
   F : TextFile;
begin

    AssignFile(F,cPorta) ;
    ReWrite(F) ;
    WriteLn(F,cTexto);
    CloseFile(F) ;


end ;

end.
