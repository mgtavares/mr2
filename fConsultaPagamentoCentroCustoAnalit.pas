unit fConsultaPagamentoCentroCustoAnalit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  GridsEh, DBGridEh, ADODB;

type
  TfrmConsultaPagamentoCentroCustoAnalit = class(TfrmProcesso_Padrao)
    uspConsulta: TADOStoredProc;
    DBGridEh1: TDBGridEh;
    DataSource1: TDataSource;
    uspConsultanValMov: TBCDField;
    uspConsultanCdTitulo: TIntegerField;
    uspConsultanCdContaBancaria: TIntegerField;
    uspConsultanCdFormaPagto: TIntegerField;
    uspConsultacNmFormaPagto: TStringField;
    uspConsultaiNrCheque: TIntegerField;
    uspConsultaContaBancaria: TStringField;
    uspConsultanCdCC: TIntegerField;
    uspConsultanCdCC1: TIntegerField;
    uspConsultanCdCC2: TIntegerField;
    uspConsultacCdCC: TStringField;
    uspConsultacNmCC: TStringField;
    uspConsultanPercent: TBCDField;
    uspConsultavalorProporcional: TBCDField;
    uspConsultanValor: TBCDField;
    uspConsultanCdSP: TIntegerField;
    uspConsultacNrTit: TStringField;
    uspConsultanCdTerceiro: TIntegerField;
    uspConsultacNmTerceiro: TStringField;
    uspConsultadDtLiq: TDateTimeField;
    uspConsultadDtMov: TDateTimeField;
    uspConsultanValTit: TBCDField;
    uspConsultaiParcela: TIntegerField;
    uspConsultanCdLojaTit: TIntegerField;
    uspConsultanCdEmpresa: TIntegerField;
    uspConsultacNrNf: TStringField;
    uspConsultanCdRecebimento: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaPagamentoCentroCustoAnalit: TfrmConsultaPagamentoCentroCustoAnalit;

implementation

{$R *.dfm}

end.
