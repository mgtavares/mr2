inherited rptPreVendaPendente: TrptPreVendaPendente
  Left = 95
  Top = 147
  Caption = 'Relat'#243'rio Pr'#233'-Venda Pendente'
  PixelsPerInch = 96
  TextHeight = 13
  object Label6: TLabel [1]
    Left = 172
    Top = 72
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label3: TLabel [2]
    Left = 16
    Top = 72
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Venda'
  end
  object Label5: TLabel [3]
    Left = 65
    Top = 48
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtDtFinal: TMaskEdit [5]
    Left = 200
    Top = 64
    Width = 71
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object edtDtInicial: TMaskEdit [6]
    Left = 88
    Top = 64
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object edtCdLoja: TMaskEdit [7]
    Left = 88
    Top = 40
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtCdLojaExit
    OnKeyDown = edtCdLojaKeyDown
  end
  object DBEdit1: TDBEdit [8]
    Tag = 1
    Left = 157
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmLoja'
    DataSource = DataSource1
    TabOrder = 4
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdLoja    = Loja.nCdLoja'
      '                 AND UL.nCdUsuario = :nCdUsuario)')
    Left = 344
    Top = 168
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLoja
    Left = 624
    Top = 272
  end
end
