inherited frmImportaMunicipioIBGE: TfrmImportaMunicipioIBGE
  Left = 340
  Top = 154
  Width = 690
  Height = 223
  Caption = 'Importar Munic'#237'pio IBGE'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 674
    Height = 156
  end
  object Label1: TLabel [1]
    Left = 44
    Top = 48
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo do Estado'
  end
  object Label2: TLabel [2]
    Left = 12
    Top = 72
    Width = 116
    Height = 13
    Alignment = taRightJustify
    Caption = 'Caminho Arquivo (*.txt)'
  end
  inherited ToolBar1: TToolBar
    Width = 674
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtCaminho: TEdit [4]
    Left = 132
    Top = 64
    Width = 497
    Height = 21
    TabOrder = 1
  end
  object cxButton1: TcxButton [5]
    Left = 635
    Top = 62
    Width = 28
    Height = 28
    Hint = 'Selecionar Arquivo'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnClick = cxButton1Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000A2CAEE76B2E6
      3E91DB348CD9348CD9348CD9348CD9348CD9348CD9348CD9348CD9348BD9398F
      DA85B9E9C9C9C9C9C9C94799DDDEF1FAA8DDF49EDBF496DAF38ED8F386D7F37F
      D4F279D3F272D2F16CD0F169CFF1C2EAF83F95DBC9C9C9C9C9C93B97DBEFFAFE
      A1E9F991E5F881E1F772DEF663DAF554D7F447D3F339D0F22ECDF126CBF0CAF2
      FB3B97DBC9C9C9C9C9C93C9DDBF2FAFDB3EDFAA4E9F995E6F885E2F781E1F77A
      E0F76FDDF662DAF554D6F347D3F2E8F9FD3594DAC9C9C9C9C9C93BA3DBF6FCFE
      C8F2FCB9EFFBACECFA8CE4F88AE3F882E1F779DFF76DDDF661DAF557D7F4E7F8
      FD3594DAC9C9C9C9C9C93BA8DBFEFFFFF8FDFFF6FDFFF5FCFFE8FAFEAFECFA8E
      E4F887E3F87DE0F772DDF668DBF5E9F9FD3594DAC9C9C9C9C9C939ADDBE8F6FB
      7EC5EA5BAEE351A8E160AFE4EBFAFDECFAFEE5F5FCE5F6FCE4F5FCE4F5FCFEFF
      FF3594DAC9C9C9C9C9C940AEDCF1FAFD94DEF593DCF481D5F260C0E94FAEE135
      94DA3594DA3594DA3594DA3594DA3594DA3594DA3836343D3D3C41B4DCF7FCFE
      8EE4F891DEF59FE0F5ACE1F6EFFBFE635B54A79A8D70675E44403BA3AAAC5C55
      4E968A7F665E561B1A193CB5DBFDFEFEFEFFFFFEFEFFFDFEFFFEFFFFEAF7FB71
      685FC1B6AB7970664B55556097A86E665DBDB1A57E746A33302F59C2E061C3E2
      63C4E363C4E363C4E362C4E356C0E0786F67C4B9AFA79A8D7D7C78CDD3D46C64
      5EBBAFA4A6988B54524FEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE70
      6861696158564F484D4B488C8C8B403C3946423C3D38332D2C2BEEEEEEEEEEEE
      EEEEEEEEEEEEEEEEEEEEEEEEEEEEEE9A98955B544DB0A090695D518674656358
      4EAA98885A514A868584EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
      EEEE6D665E85786F4A413B95897E564A409283744F4740E8E8E7EEEEEEEEEEEE
      EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEC5BFB89C8F83524D48CDCAC8544D
      4790867C9C9A97EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
      EEEEEEEEEE797067766D66DFDBD76F675F6C6864EEEEEEEEEEEE}
    LookAndFeel.NativeStyle = True
  end
  object er2LkpEstado: TER2LookupMaskEdit [6]
    Left = 133
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 3
    Text = '         '
    CodigoLookup = 204
    QueryLookup = qryEstado
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 233
    Top = 40
    Width = 396
    Height = 21
    DataField = 'cNmEstado'
    DataSource = DataSource1
    TabOrder = 4
  end
  object DBEdit2: TDBEdit [8]
    Tag = 1
    Left = 201
    Top = 40
    Width = 29
    Height = 21
    DataField = 'cUF'
    DataSource = DataSource1
    TabOrder = 5
  end
  inherited ImageList1: TImageList
    Left = 336
    Top = 96
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*-nfe.XML'
    Filter = 
      'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|To' +
      'dos os Arquivos (*.*)|*.*'
    Title = 'Selecione a NFe'
    Left = 336
    Top = 128
  end
  object qryMunicipio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 1 *'
      'FROM Municipio')
    Left = 240
    Top = 128
    object qryMunicipionCdMunicipio: TIntegerField
      FieldName = 'nCdMunicipio'
    end
    object qryMunicipionCdEstado: TIntegerField
      FieldName = 'nCdEstado'
    end
    object qryMunicipiocNmMunicipio: TStringField
      FieldName = 'cNmMunicipio'
      Size = 50
    end
    object qryMunicipionCdMunicipioIBGE: TIntegerField
      FieldName = 'nCdMunicipioIBGE'
    end
  end
  object qryChecaMunicipioCodigo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEstado'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdMunicipio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      '  FROM Municipio'
      ' WHERE nCdEstado = :nCdEstado'
      '   AND nCdMunicipio = :nCdMunicipio')
    Left = 240
    Top = 96
    object qryChecaMunicipioCodigoCOLUMN1: TIntegerField
      FieldName = 'COLUMN1'
      ReadOnly = True
    end
  end
  object qryChecaMunicipioNome: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEstado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cNmMunicipio'
        DataType = ftString
        Precision = 50
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      'FROM Municipio'
      'WHERE nCdEstado = :nCdEstado'
      'AND cNmMunicipio = :cNmMunicipio')
    Left = 272
    Top = 128
    object IntegerField1: TIntegerField
      FieldName = 'COLUMN1'
      ReadOnly = True
    end
  end
  object qryAtualizaMunicipio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNmMunicipio'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 50
        Value = Null
      end
      item
        Name = 'nCdMunicipio'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE Municipio'
      '   SET cNmMunicipio = :cNmMunicipio'
      ' WHERE nCdMunicipio = :nCdMunicipio')
    Left = 272
    Top = 96
  end
  object qryEstado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Estado'
      ' WHERE nCdEstado = :nPK ')
    Left = 304
    Top = 96
    object qryEstadonCdEstado: TIntegerField
      FieldName = 'nCdEstado'
    end
    object qryEstadonCdPais: TIntegerField
      FieldName = 'nCdPais'
    end
    object qryEstadocUF: TStringField
      FieldName = 'cUF'
      FixedChar = True
      Size = 2
    end
    object qryEstadocNmEstado: TStringField
      FieldName = 'cNmEstado'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEstado
    Left = 304
    Top = 128
  end
end
