inherited frmTipoDiverg: TfrmTipoDiverg
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'Tipo de Diverg'#234'ncia'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1152
    Height = 789
  end
  object Label1: TLabel [1]
    Left = 51
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 40
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  inherited ToolBar2: TToolBar
    Width = 1152
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited btSalvar: TToolButton
      Visible = False
    end
    inherited ToolButton50: TToolButton
      Left = 465
      Wrap = False
    end
    inherited btCancelar: TToolButton
      Left = 473
      Top = 0
    end
    inherited ToolButton11: TToolButton
      Left = 562
      Top = 0
    end
    inherited btConsultar: TToolButton
      Left = 570
      Top = 0
    end
    inherited ToolButton8: TToolButton
      Left = 659
      Top = 0
    end
    inherited ToolButton5: TToolButton
      Left = 667
      Top = 0
    end
    inherited ToolButton4: TToolButton
      Left = 756
      Top = 0
    end
    inherited ToolButton9: TToolButton
      Left = 764
      Top = 0
    end
    inherited btnAuditoria: TToolButton
      Left = 772
      Top = 0
    end
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 96
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoDiverg'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 96
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmTabTipoDiverg'
    DataSource = dsMaster
    TabOrder = 2
  end
  object cxPageControl1: TcxPageControl [6]
    Left = 16
    Top = 96
    Width = 737
    Height = 377
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 3
    ClientRectBottom = 373
    ClientRectLeft = 4
    ClientRectRight = 733
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Usu'#225'rios Permitidos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 729
        Height = 349
        Align = alClient
        DataSource = dsUsuarioTabTipoDiverg
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoDiverg'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Width = 483
          end>
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabTipoDiverg'
      'WHERE nCdTabTipoDiverg = :nPK')
    object qryMasternCdTabTipoDiverg: TIntegerField
      FieldName = 'nCdTabTipoDiverg'
    end
    object qryMastercNmTabTipoDiverg: TStringField
      FieldName = 'cNmTabTipoDiverg'
      Size = 50
    end
  end
  object qryUsuarioTabTipoDiverg: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioTabTipoDivergBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM UsuarioTabTipoDiverg'
      'WHERE nCdTabTipoDiverg = :nPK')
    Left = 688
    Top = 192
    object qryUsuarioTabTipoDivergnCdTabTipoDiverg: TIntegerField
      FieldName = 'nCdTabTipoDiverg'
    end
    object qryUsuarioTabTipoDivergnCdUsuario: TIntegerField
      DisplayLabel = 'Usu'#225'rios que podem liberar este tipo de diverg'#234'ncia|C'#243'd'
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioTabTipoDivergcNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rios que podem liberar este tipo de diverg'#234'ncia|Nome Usu'#225'rio'
      FieldKind = fkLookup
      FieldName = 'cNmUsuario'
      LookupDataSet = qryUsuario
      LookupKeyFields = 'nCdUsuario'
      LookupResultField = 'cNmUsuario'
      KeyFields = 'nCdUsuario'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryUsuarioTabTipoDivergnCdUsuarioTabTipoDiverg: TIntegerField
      FieldName = 'nCdUsuarioTabTipoDiverg'
    end
  end
  object dsUsuarioTabTipoDiverg: TDataSource
    DataSet = qryUsuarioTabTipoDiverg
    Left = 720
    Top = 192
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdUsuario'
      ',cNmUsuario'
      'FROM Usuario')
    Left = 776
    Top = 192
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
end
