inherited frmAlteraContaBancariaDeposito: TfrmAlteraContaBancariaDeposito
  Left = 243
  Top = 165
  Caption = 'Alterar Conta Bancaria Deposito'
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel [1]
    Left = 31
    Top = 239
    Width = 111
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Banc'#225'ria Cr'#233'dito'
  end
  object Label1: TLabel [2]
    Left = 109
    Top = 40
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
  end
  object Label2: TLabel [3]
    Left = 41
    Top = 64
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Banc'#225'ria Atual'
    FocusControl = DBEdit7
  end
  object Label3: TLabel [4]
    Left = 101
    Top = 89
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [5]
    Left = 122
    Top = 114
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit3
  end
  object Label5: TLabel [6]
    Left = 59
    Top = 164
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor em Dinheiro'
    FocusControl = DBEdit4
  end
  object Label6: TLabel [7]
    Left = 9
    Top = 189
    Width = 133
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor em Cheque Numer'#225'rio'
    FocusControl = DBEdit5
  end
  object Label8: TLabel [8]
    Left = 312
    Top = 164
    Width = 139
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor em Cheque Pr'#233'-Datado'
    FocusControl = DBEdit6
  end
  object Label9: TLabel [9]
    Left = 340
    Top = 189
    Width = 111
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Total do Dep'#243'sito'
    FocusControl = DBEdit8
  end
  object Label10: TLabel [10]
    Left = 58
    Top = 214
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Qtde de Cheques'
    FocusControl = DBEdit9
  end
  object Label11: TLabel [11]
    Left = 59
    Top = 139
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data de Dep'#243'sito'
    FocusControl = DBEdit12
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit1: TMaskEdit [13]
    Left = 144
    Top = 32
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit1Exit
    OnKeyDown = MaskEdit1KeyDown
  end
  object MaskEdit2: TMaskEdit [14]
    Left = 144
    Top = 231
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit2Exit
    OnKeyDown = MaskEdit2KeyDown
  end
  object DBEdit16: TDBEdit [15]
    Tag = 1
    Left = 216
    Top = 231
    Width = 401
    Height = 21
    DataField = 'cNmConta'
    DataSource = DataSource2
    TabOrder = 3
  end
  object DBEdit7: TDBEdit [16]
    Tag = 1
    Left = 144
    Top = 56
    Width = 65
    Height = 21
    DataField = 'nCdContaBancaria'
    DataSource = DataSource1
    TabOrder = 4
  end
  object DBEdit1: TDBEdit [17]
    Tag = 1
    Left = 216
    Top = 56
    Width = 401
    Height = 21
    DataField = 'cNmConta'
    DataSource = DataSource1
    TabOrder = 5
  end
  object DBEdit2: TDBEdit [18]
    Tag = 1
    Left = 144
    Top = 81
    Width = 65
    Height = 21
    DataField = 'nCdEmpresa'
    DataSource = DataSource1
    TabOrder = 6
  end
  object DBEdit3: TDBEdit [19]
    Tag = 1
    Left = 144
    Top = 106
    Width = 65
    Height = 21
    DataField = 'nCdLoja'
    DataSource = DataSource1
    TabOrder = 7
  end
  object DBEdit4: TDBEdit [20]
    Tag = 1
    Left = 144
    Top = 156
    Width = 160
    Height = 21
    DataField = 'nValDinheiro'
    DataSource = DataSource1
    TabOrder = 8
  end
  object DBEdit5: TDBEdit [21]
    Tag = 1
    Left = 144
    Top = 181
    Width = 160
    Height = 21
    DataField = 'nValChequeNumerario'
    DataSource = DataSource1
    TabOrder = 9
  end
  object DBEdit6: TDBEdit [22]
    Tag = 1
    Left = 457
    Top = 156
    Width = 160
    Height = 21
    DataField = 'nValChequePreDatado'
    DataSource = DataSource1
    TabOrder = 10
  end
  object DBEdit8: TDBEdit [23]
    Tag = 1
    Left = 457
    Top = 181
    Width = 160
    Height = 21
    DataField = 'nValTotalDeposito'
    DataSource = DataSource1
    TabOrder = 11
  end
  object DBEdit9: TDBEdit [24]
    Tag = 1
    Left = 144
    Top = 206
    Width = 134
    Height = 21
    DataField = 'iQtdeCheques'
    DataSource = DataSource1
    TabOrder = 12
  end
  object DBEdit10: TDBEdit [25]
    Tag = 1
    Left = 216
    Top = 106
    Width = 401
    Height = 21
    DataField = 'cNmLoja'
    DataSource = DataSource1
    TabOrder = 13
  end
  object DBEdit11: TDBEdit [26]
    Tag = 1
    Left = 216
    Top = 81
    Width = 401
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 14
  end
  object DBEdit12: TDBEdit [27]
    Tag = 1
    Left = 144
    Top = 131
    Width = 113
    Height = 21
    DataField = 'dDtDeposito'
    DataSource = DataSource1
    TabOrder = 15
  end
  inherited ImageList1: TImageList
    Left = 400
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      ''
      'Set @nCdLoja = :nCdLoja'
      ''
      'SELECT nCdContaBancaria'
      
        '      ,(Convert(VARCHAR(5),nCdBanco) + '#39' - '#39' + Convert(VARCHAR(5' +
        '),cAgencia) + '#39' - '#39' + nCdConta + '#39' - '#39' + IsNull(cNmTitular,'#39' '#39'))' +
        ' cNmConta'
      '  FROM ContaBancaria'
      ' WHERE cFlgDeposito     = 1'
      '   AND nCdEmpresa       = :nCdEmpresa'
      '   AND nCdContaBancaria = :nPK'
      '   AND ((@nCdLoja = 0) OR (nCdLoja = @nCdLoja))')
    Left = 432
    Top = 272
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariacNmConta: TStringField
      FieldName = 'cNmConta'
      ReadOnly = True
      Size = 84
    end
  end
  object qryDepositoBancario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT DepositoBancario.nCdDepositoBancario'
      '      ,DepositoBancario.nCdEmpresa'
      '      ,Empresa.cNmEmpresa'
      '      ,DepositoBancario.nCdLoja'
      '      ,Loja.cNmLoja'
      '      ,DepositoBancario.dDtDeposito'
      '      ,DepositoBancario.nCdContaBancaria'
      
        '      ,(Convert(VARCHAR(5),nCdBanco) + '#39' - '#39' + Convert(VARCHAR(5' +
        '),cAgencia) + '#39' - '#39' + nCdConta + '#39' - '#39' + IsNull(cNmTitular,'#39' '#39'))' +
        ' cNmConta'
      '      ,DepositoBancario.nValDinheiro'
      '      ,DepositoBancario.nValChequeNumerario'
      '      ,DepositoBancario.nValChequePreDatado'
      '      ,DepositoBancario.nValTotalDeposito'
      '      ,DepositoBancario.iQtdeCheques'
      '  FROM DepositoBancario'
      
        '       LEFT  JOIN Loja          ON Loja.nCdLoja                 ' +
        '  = DepositoBancario.nCdLoja'
      
        '       LEFT  JOIN Empresa       ON Empresa.nCdEmpresa           ' +
        '  = DepositoBancario.nCdEmpresa'
      
        '       INNER JOIN ContaBancaria ON ContaBancaria.nCdContaBancari' +
        'a = DepositoBancario.nCdContaBancaria'
      ' WHERE nCdDepositoBancario =:nPK'
      '   AND dDtProcesso         IS NOT NULL'
      '   AND dDtCancel           IS     NULL'
      '')
    Left = 472
    Top = 272
    object qryDepositoBancarionCdDepositoBancario: TIntegerField
      FieldName = 'nCdDepositoBancario'
    end
    object qryDepositoBancarionCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryDepositoBancarionCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryDepositoBancariodDtDeposito: TDateTimeField
      FieldName = 'dDtDeposito'
    end
    object qryDepositoBancarionCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryDepositoBancarionValDinheiro: TBCDField
      FieldName = 'nValDinheiro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDepositoBancarionValChequeNumerario: TBCDField
      FieldName = 'nValChequeNumerario'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDepositoBancarionValChequePreDatado: TBCDField
      FieldName = 'nValChequePreDatado'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDepositoBancarionValTotalDeposito: TBCDField
      FieldName = 'nValTotalDeposito'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDepositoBancarioiQtdeCheques: TIntegerField
      FieldName = 'iQtdeCheques'
    end
    object qryDepositoBancariocNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryDepositoBancariocNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryDepositoBancariocNmConta: TStringField
      FieldName = 'cNmConta'
      ReadOnly = True
      Size = 84
    end
  end
  object SP_ALTERA_CONTA_DEPOSITO_BANCARIO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ALTERA_CONTA_DEPOSITO_BANCARIO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdDepositoBancario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 552
    Top = 272
  end
  object DataSource1: TDataSource
    DataSet = qryDepositoBancario
    Left = 488
    Top = 304
  end
  object DataSource2: TDataSource
    DataSet = qryContaBancaria
    Left = 448
    Top = 304
  end
end
