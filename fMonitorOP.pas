unit fMonitorOP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, cxPC,
  cxControls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ADODB, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, Menus, StdCtrls, Mask, DBCtrls;

type
  TfrmMonitorOP = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryOPPlanejada: TADOQuery;
    qryOPPlanejadanCdOrdemProducao: TIntegerField;
    qryOPPlanejadacNumeroOP: TStringField;
    qryOPPlanejadacNmTabTipoOrigemOP: TStringField;
    qryOPPlanejadadDtAbertura: TDateTimeField;
    qryOPPlanejadadDtPrevConclusao: TDateTimeField;
    qryOPPlanejadacNmTipoOP: TStringField;
    qryOPPlanejadanCdProduto: TIntegerField;
    qryOPPlanejadacReferencia: TStringField;
    qryOPPlanejadacNmProduto: TStringField;
    qryOPPlanejadanQtdePlanejada: TBCDField;
    qryOPPlanejadanCdPedido: TIntegerField;
    qryOPPlanejadacNmTerceiro: TStringField;
    dsOPPlanejada: TDataSource;
    cxGridDBTableView1nCdOrdemProducao: TcxGridDBColumn;
    cxGridDBTableView1cNumeroOP: TcxGridDBColumn;
    cxGridDBTableView1cNmTabTipoOrigemOP: TcxGridDBColumn;
    cxGridDBTableView1dDtAbertura: TcxGridDBColumn;
    cxGridDBTableView1dDtPrevConclusao: TcxGridDBColumn;
    cxGridDBTableView1cNmTipoOP: TcxGridDBColumn;
    cxGridDBTableView1nCdProduto: TcxGridDBColumn;
    cxGridDBTableView1cReferencia: TcxGridDBColumn;
    cxGridDBTableView1cNmProduto: TcxGridDBColumn;
    cxGridDBTableView1nQtdePlanejada: TcxGridDBColumn;
    cxGridDBTableView1nCdPedido: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    AtualizarOrdensPlanejadas1: TMenuItem;
    cxGrid1: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    qryOPAutorizada: TADOQuery;
    dsOPAutorizada: TDataSource;
    cxGridDBTableView2dDtInicioPrevisto: TcxGridDBColumn;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    qryOPAutorizadanCdOrdemProducao: TIntegerField;
    qryOPAutorizadacNumeroOP: TStringField;
    qryOPAutorizadacNmTabTipoOrigemOP: TStringField;
    qryOPAutorizadadDtAbertura: TDateTimeField;
    qryOPAutorizadadDtInicioPrevisto: TDateTimeField;
    qryOPAutorizadadDtPrevConclusao: TDateTimeField;
    qryOPAutorizadacNmTipoOP: TStringField;
    qryOPAutorizadanCdProduto: TIntegerField;
    qryOPAutorizadacReferencia: TStringField;
    qryOPAutorizadacNmProduto: TStringField;
    qryOPAutorizadanQtdePlanejada: TBCDField;
    qryOPAutorizadanCdPedido: TIntegerField;
    qryOPAutorizadacNmTerceiro: TStringField;
    N1: TMenuItem;
    AutorizarOP1: TMenuItem;
    CancelarOP1: TMenuItem;
    VisualizarOP1: TMenuItem;
    SP_CANCELA_OP: TADOStoredProc;
    qryOPPlanejadacFlgEstoqueEmpenhado: TIntegerField;
    SP_AUTORIZA_OP: TADOStoredProc;
    N2: TMenuItem;
    VisualizarOP2: TMenuItem;
    IniciarProcessoProdutivo1: TMenuItem;
    N3: TMenuItem;
    CancelarOP2: TMenuItem;
    N4: TMenuItem;
    qryOPProcesso: TADOQuery;
    dsOPProcesso: TDataSource;
    cxGrid3: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridDBColumn13: TcxGridDBColumn;
    cxGridDBColumn14: TcxGridDBColumn;
    cxGridDBColumn15: TcxGridDBColumn;
    cxGridDBColumn16: TcxGridDBColumn;
    cxGridDBColumn17: TcxGridDBColumn;
    cxGridDBColumn18: TcxGridDBColumn;
    cxGridDBColumn19: TcxGridDBColumn;
    cxGridDBColumn20: TcxGridDBColumn;
    cxGridDBColumn21: TcxGridDBColumn;
    cxGridDBColumn22: TcxGridDBColumn;
    cxGridDBColumn23: TcxGridDBColumn;
    cxGridDBColumn24: TcxGridDBColumn;
    cxGridDBColumn25: TcxGridDBColumn;
    cxGridLevel3: TcxGridLevel;
    PopupMenu3: TPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    qryOPProcessonCdOrdemProducao: TIntegerField;
    qryOPProcessocNumeroOP: TStringField;
    qryOPProcessocNmTabTipoOrigemOP: TStringField;
    qryOPProcessodDtAbertura: TDateTimeField;
    qryOPProcessodDtInicioPrevisto: TDateTimeField;
    qryOPProcessodDtPrevConclusao: TDateTimeField;
    qryOPProcessocNmTipoOP: TStringField;
    qryOPProcessonCdProduto: TIntegerField;
    qryOPProcessocReferencia: TStringField;
    qryOPProcessocNmProduto: TStringField;
    qryOPProcessonQtdePlanejada: TBCDField;
    qryOPProcessonCdPedido: TIntegerField;
    qryOPProcessocNmTerceiro: TStringField;
    SP_INICIA_PROCESSO_OP: TADOStoredProc;
    cxPageControl2: TcxPageControl;
    cxTabSheet4: TcxTabSheet;
    cxTabSheet5: TcxTabSheet;
    qryOPProcessonQtdeCancelada: TIntegerField;
    qryOPProcessonQtdeRetrabalho: TIntegerField;
    qryOPProcessonQtdeRefugo: TIntegerField;
    qryOPProcessonSaldoProduzir: TBCDField;
    cxTabSheet6: TcxTabSheet;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    RequisitarProdutos1: TMenuItem;
    cxGrid4: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridLevel4: TcxGridLevel;
    qryRegistroEntrada: TADOQuery;
    dsRegistroEntrada: TDataSource;
    qryRegistroEntradacDocumento: TStringField;
    qryRegistroEntradacNmTabTipoEntradaProducao: TStringField;
    qryRegistroEntradanQtdeEntrada: TBCDField;
    qryRegistroEntradacNmLocalEstoque: TStringField;
    qryRegistroEntradadDtEntrada: TDateTimeField;
    qryRegistroEntradacNmUsuario: TStringField;
    qryRegistroEntradacOBSLivro: TStringField;
    cxGridDBTableView4cDocumento: TcxGridDBColumn;
    cxGridDBTableView4cNmTabTipoEntradaProducao: TcxGridDBColumn;
    cxGridDBTableView4nQtdeEntrada: TcxGridDBColumn;
    cxGridDBTableView4cNmLocalEstoque: TcxGridDBColumn;
    cxGridDBTableView4dDtEntrada: TcxGridDBColumn;
    cxGridDBTableView4cNmUsuario: TcxGridDBColumn;
    cxGridDBTableView4cOBSLivro: TcxGridDBColumn;
    RequisitarProdutos2: TMenuItem;
    cxGridDBTableView3nQtdeConcluida: TcxGridDBColumn;
    cxGridDBTableView3nSaldoProduzir: TcxGridDBColumn;
    AgendarIncioProduo1: TMenuItem;
    SP_AGENDA_INICIO_OP: TADOStoredProc;
    AgendarIncioProduo2: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    qryOPProcessocFlgAtraso: TStringField;
    qryOPProcessonQtdeConcluida: TIntegerField;
    cxGridDBTableView3cFlgAtraso: TcxGridDBColumn;
    qryOPAutorizadacFlgAtraso: TStringField;
    cxGridDBTableView2cFlgAtraso: TcxGridDBColumn;
    qryOPPlanejadacFlgAtraso: TStringField;
    cxGridDBTableView1cFlgAtraso: TcxGridDBColumn;
    cxTabSheet7: TcxTabSheet;
    cxGrid5: TcxGrid;
    cxGridDBTableView5: TcxGridDBTableView;
    cxGridDBColumn26: TcxGridDBColumn;
    cxGridDBColumn27: TcxGridDBColumn;
    cxGridDBColumn28: TcxGridDBColumn;
    cxGridDBColumn29: TcxGridDBColumn;
    cxGridDBColumn30: TcxGridDBColumn;
    cxGridDBColumn31: TcxGridDBColumn;
    cxGridDBColumn32: TcxGridDBColumn;
    cxGridDBColumn33: TcxGridDBColumn;
    cxGridDBColumn34: TcxGridDBColumn;
    cxGridDBColumn35: TcxGridDBColumn;
    cxGridDBColumn36: TcxGridDBColumn;
    cxGridDBColumn37: TcxGridDBColumn;
    cxGridDBColumn38: TcxGridDBColumn;
    cxGridDBColumn39: TcxGridDBColumn;
    cxGridDBColumn40: TcxGridDBColumn;
    cxGridDBColumn41: TcxGridDBColumn;
    cxGridLevel5: TcxGridLevel;
    qryOPAberta: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    DateTimeField3: TDateTimeField;
    StringField3: TStringField;
    IntegerField2: TIntegerField;
    StringField4: TStringField;
    StringField5: TStringField;
    BCDField1: TBCDField;
    IntegerField3: TIntegerField;
    StringField6: TStringField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    BCDField2: TBCDField;
    StringField7: TStringField;
    IntegerField7: TIntegerField;
    dsOPAberta: TDataSource;
    qryOPAbertacNmTabTipoStatusOP: TStringField;
    cxGridDBTableView5cNmTabTipoStatusOP: TcxGridDBColumn;
    PopupMenu4: TPopupMenu;
    AtualizarOrdensEmProcesso1: TMenuItem;
    N7: TMenuItem;
    RegistrarConcluso1: TMenuItem;
    RegistrarRetrabalho1: TMenuItem;
    RegistrarPerda1: TMenuItem;
    procedure AtualizarOrdensPlanejadas1Click(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure emitirOP( nCdOrdemProducao:integer ) ;
    procedure MenuItem1Click(Sender: TObject);
    procedure cxGridDBTableView2DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure VisualizarOP1Click(Sender: TObject);
    procedure CancelarOP1Click(Sender: TObject);
    procedure AutorizarOP1Click(Sender: TObject);
    procedure VisualizarOP2Click(Sender: TObject);
    procedure PopupMenu2Popup(Sender: TObject);
    procedure CancelarOP2Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure IniciarProcessoProdutivo1Click(Sender: TObject);
    procedure RequisitarProdutos1Click(Sender: TObject);
    procedure qryOPProcessoAfterScroll(DataSet: TDataSet);
    procedure qryOPProcessoAfterClose(DataSet: TDataSet);
    procedure RequisitarProdutos2Click(Sender: TObject);
    procedure AgendarIncioProduo1Click(Sender: TObject);
    procedure AgendarIncioProduo2Click(Sender: TObject);
    procedure AtualizarOrdensEmProcesso1Click(Sender: TObject);
    procedure RegistrarConcluso1Click(Sender: TObject);
    procedure RegistrarRetrabalho1Click(Sender: TObject);
    procedure RegistrarPerda1Click(Sender: TObject);
    procedure PopupMenu3Popup(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMonitorOP: TfrmMonitorOP;

implementation

uses fMenu, rOP, fOP, fMonitorOP_DataInicio, fGerarRequisicaoOP, fEntradaProducao;

{$R *.dfm}

procedure TfrmMonitorOP.AtualizarOrdensPlanejadas1Click(Sender: TObject);
begin
  inherited;

  PosicionaQuery( qryOPPlanejada , intToStr( frmMenu.nCdEmpresaAtiva ) ) ;

end;

procedure TfrmMonitorOP.cxGridDBTableView1DblClick(Sender: TObject);
begin
  inherited;

  if (qryOPPlanejada.Eof) or (qryOPPlanejada.RecordCount = 0) then
      exit ;

  emitirOP( qryOPPlanejadanCdOrdemProducao.Value ) ;

end;

procedure TfrmMonitorOP.emitirOP(nCdOrdemProducao: integer);
var
  objRel : TrptOP ;
begin
  inherited;

  objRel := TrptOP.Create( Self ) ;

  PosicionaQuery( objRel.qryOP , intToStr( nCdOrdemProducao ) ) ;

  try

      try

          objRel.QuickRep1.Preview;

      except

          MensagemErro('Erro no Processamento.') ;
          raise ;

      end ;

  finally

      freeAndNil( objRel ) ;

  end ;

end;

procedure TfrmMonitorOP.MenuItem1Click(Sender: TObject);
begin
  inherited;

  PosicionaQuery( qryOPAutorizada , intToStr( frmMenu.nCdEmpresaAtiva ) ) ;

end;

procedure TfrmMonitorOP.cxGridDBTableView2DblClick(Sender: TObject);
begin
  inherited;

  if (qryOPProcesso.Eof) or (qryOPProcesso.RecordCount = 0) then
      exit ;

  emitirOP( qryOPProcessonCdOrdemProducao.Value ) ;

end;

procedure TfrmMonitorOP.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl2.ActivePageIndex := 0 ;
  cxPageControl1.ActivePageIndex := 0 ;
  
end;

procedure TfrmMonitorOP.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  AutorizarOP1.Enabled        := ((qryOPPlanejada.Active) and (qryOPPlanejada.RecordCount > 0)) ;
  CancelarOP1.Enabled         := AutorizarOP1.Enabled ;
  VisualizarOP1.Enabled       := AutorizarOP1.Enabled ;
  AgendarIncioProduo2.Enabled := AutorizarOP1.Enabled ;

end;

procedure TfrmMonitorOP.VisualizarOP1Click(Sender: TObject);
var
  objForm : TfrmOP ;
begin
  inherited;

  objForm := TfrmOP.Create( Self ) ;

  objForm.PosicionaPK( qryOPPlanejadanCdOrdemProducao.Value );

  showForm( objForm , TRUE ) ;

end;

procedure TfrmMonitorOP.CancelarOP1Click(Sender: TObject);
begin
  inherited;

  if (MessageDlg('Confirma o cancelamento da OP : ' + qryOPPlanejadacNumeroOP.Value + ' ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  if (qryOPPlanejadanCdPedido.Value > 0) then
      if (MessageDlg('Esta ordem de produ��o foi gerado para o pedido de venda : ' + qryOPPlanejadanCdPedido.AsString + ' - ' + qryOPPlanejadacNmTerceiro.Value + '. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

  frmMenu.Connection.BeginTrans ;

  try

      SP_CANCELA_OP.Close ;
      SP_CANCELA_OP.Parameters.ParamByName('@nCdOrdemProducao').Value := qryOPPlanejadanCdOrdemProducao.Value ;
      SP_CANCELA_OP.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado ;
      SP_CANCELA_OP.ExecProc ;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Ordem de produ��o cancelada com Sucesso!') ;

  {-- atualiza o grid de op�s planejadas --}
  qryOPPlanejada.Requery();

end;

procedure TfrmMonitorOP.AutorizarOP1Click(Sender: TObject);
var
  objForm     : TfrmMonitorOP_DataInicio ;
  dDtAgendada : TDateTime ;
begin
  inherited;

  if (MessageDlg('Confirma a autoriza��o da OP : ' + qryOPPlanejadacNumeroOP.Value + ' ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  objForm := TfrmMonitorOP_DataInicio.Create( Self ) ;

  objForm.dDtPrevConclusao := qryOPPlanejadadDtPrevConclusao.Value ;

  showForm( objForm , FALSE ) ;

  if (objForm.bCancelado) then
  begin
      freeAndNil( objForm ) ;
      abort ;
  end ;

  dDtAgendada := objForm.MonthCalendar1.Date;

  freeAndNil( objForm ) ;

  if (MessageDlg('A data de in�cio prevista da OP � : ' + DateToStr(dDtAgendada) + '. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans ;

  try

      SP_AUTORIZA_OP.Close ;
      SP_AUTORIZA_OP.Parameters.ParamByName('@nCdOrdemProducao').Value := qryOPPlanejadanCdOrdemProducao.Value ;
      SP_AUTORIZA_OP.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado ;
      SP_AUTORIZA_OP.Parameters.ParamByName('@cDtPrevInicio').Value    := DateToStr(dDtAgendada) ;
      SP_AUTORIZA_OP.ExecProc ;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Ordem de produ��o autorizada com Sucesso!') ;

  {-- atualiza o grid de op�s planejadas --}
  qryOPPlanejada.Requery();

end;

procedure TfrmMonitorOP.VisualizarOP2Click(Sender: TObject);
var
  objForm : TfrmOP ;
begin
  inherited;

  objForm := TfrmOP.Create( Self ) ;

  objForm.PosicionaPK( qryOPAutorizadanCdOrdemProducao.Value );

  showForm( objForm , TRUE ) ;

end;

procedure TfrmMonitorOP.PopupMenu2Popup(Sender: TObject);
begin
  inherited;

  IniciarProcessoProdutivo1.Enabled  := ((qryOPAutorizada.Active) and (qryOPAutorizada.RecordCount > 0)) ;
  CancelarOP2.Enabled                := IniciarProcessoProdutivo1.Enabled ;
  VisualizarOP2.Enabled              := IniciarProcessoProdutivo1.Enabled ;
  RequisitarProdutos1.Enabled        := IniciarProcessoProdutivo1.Enabled ;
  AgendarIncioProduo1.Enabled        := IniciarProcessoProdutivo1.Enabled ;

end;

procedure TfrmMonitorOP.CancelarOP2Click(Sender: TObject);
begin
  inherited;

  if (MessageDlg('Confirma o cancelamento da OP : ' + qryOPAutorizadacNumeroOP.Value + ' ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  if (qryOPAutorizadanCdPedido.Value > 0) then
      if (MessageDlg('Esta ordem de produ��o foi gerado para o pedido de venda : ' + qryOPAutorizadanCdPedido.AsString + ' - ' + qryOPAutorizadacNmTerceiro.Value + '. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

  frmMenu.Connection.BeginTrans ;

  try

      SP_CANCELA_OP.Close ;
      SP_CANCELA_OP.Parameters.ParamByName('@nCdOrdemProducao').Value := qryOPAutorizadanCdOrdemProducao.Value ;
      SP_CANCELA_OP.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado ;
      SP_CANCELA_OP.ExecProc ;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Ordem de produ��o cancelada com Sucesso!') ;

  {-- atualiza o grid de op�s planejadas --}
  qryOPAutorizada.Requery();

end;

procedure TfrmMonitorOP.MenuItem2Click(Sender: TObject);
begin
  inherited;

  PosicionaQuery( qryOPProcesso , intToStr( frmMenu.nCdEmpresaAtiva ) ) ;

end;

procedure TfrmMonitorOP.MenuItem6Click(Sender: TObject);
begin
  inherited;

  if (MessageDlg('Confirma o cancelamento da OP : ' + qryOPProcessocNumeroOP.Value + ' ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  if (qryOPProcessonCdPedido.Value > 0) then
      if (MessageDlg('Esta ordem de produ��o foi gerado para o pedido de venda : ' + qryOPProcessonCdPedido.AsString + ' - ' + qryOPProcessocNmTerceiro.Value + '. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

  frmMenu.Connection.BeginTrans ;

  try

      SP_CANCELA_OP.Close ;
      SP_CANCELA_OP.Parameters.ParamByName('@nCdOrdemProducao').Value := qryOPProcessonCdOrdemProducao.Value ;
      SP_CANCELA_OP.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado ;
      SP_CANCELA_OP.ExecProc ;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Ordem de produ��o cancelada com Sucesso!') ;

  {-- atualiza o grid de op�s planejadas --}
  qryOPProcesso.Requery();

end;

procedure TfrmMonitorOP.MenuItem7Click(Sender: TObject);
var
  objForm : TfrmOP ;
begin
  inherited;

  objForm := TfrmOP.Create( Self ) ;

  objForm.PosicionaPK( qryOPProcessonCdOrdemProducao.Value );

  showForm( objForm , TRUE ) ;

end;

procedure TfrmMonitorOP.IniciarProcessoProdutivo1Click(Sender: TObject);
begin
  inherited;

  if (MessageDlg('Confirma o in�cio do processo da OP : ' + qryOPAutorizadacNumeroOP.Value + ' ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans ;

  try

      SP_INICIA_PROCESSO_OP.Close ;
      SP_INICIA_PROCESSO_OP.Parameters.ParamByName('@nCdOrdemProducao').Value := qryOPAutorizadanCdOrdemProducao.Value ;
      SP_INICIA_PROCESSO_OP.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado ;
      SP_INICIA_PROCESSO_OP.ExecProc ;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Ordem de produ��o Iniciada com Sucesso!') ;

  {-- atualiza o grid de op�s autorizadas --}
  qryOPAutorizada.Requery();

end;

procedure TfrmMonitorOP.RequisitarProdutos1Click(Sender: TObject);
var
   objForm : TfrmGerarRequisicaoOP ;
begin
  inherited;

   objForm := TfrmGerarRequisicaoOP.Create( Self ) ;
   objForm.exibeOrdemProducao ( qryOPAutorizadanCdOrdemProducao.Value ) ;

   freeAndNil( objForm ) ;

end;

procedure TfrmMonitorOP.qryOPProcessoAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryRegistroEntrada.Close;
  PosicionaQuery( qryRegistroEntrada , qryOPProcessonCdOrdemProducao.AsString ) ;

end;

procedure TfrmMonitorOP.qryOPProcessoAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryRegistroEntrada.Close;

end;

procedure TfrmMonitorOP.RequisitarProdutos2Click(Sender: TObject);
var
   objForm : TfrmGerarRequisicaoOP ;
begin
  inherited;

   objForm := TfrmGerarRequisicaoOP.Create( Self ) ;
   objForm.exibeOrdemProducao ( qryOPProcessonCdOrdemProducao.Value ) ;

   freeAndNil( objForm ) ;

end;

procedure TfrmMonitorOP.AgendarIncioProduo1Click(Sender: TObject);
var
  objForm     : TfrmMonitorOP_DataInicio ;
  dDtAgendada : TDateTime ;
begin
  inherited;

  objForm := TfrmMonitorOP_DataInicio.Create( Self ) ;

  objForm.dDtPrevConclusao := qryOPAutorizadadDtPrevConclusao.Value ;

  showForm( objForm , FALSE ) ;

  if (objForm.bCancelado) then
  begin
      freeAndNil( objForm ) ;
      abort ;
  end ;

  dDtAgendada := objForm.MonthCalendar1.Date;

  freeAndNil( objForm ) ;

  if (MessageDlg('A data de in�cio prevista da OP � : ' + DateToStr(dDtAgendada) + '. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans ;

  try

      SP_AGENDA_INICIO_OP.Close ;
      SP_AGENDA_INICIO_OP.Parameters.ParamByName('@nCdOrdemProducao').Value := qryOPAutorizadanCdOrdemProducao.Value ;
      SP_AGENDA_INICIO_OP.Parameters.ParamByName('@cDtPrevInicio').Value    := DateToStr(dDtAgendada) ;
      SP_AGENDA_INICIO_OP.ExecProc ;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Ordem de produ��o agendada com Sucesso!') ;

end;

procedure TfrmMonitorOP.AgendarIncioProduo2Click(Sender: TObject);
var
  objForm     : TfrmMonitorOP_DataInicio ;
  dDtAgendada : TDateTime ;
begin
  inherited;

  objForm := TfrmMonitorOP_DataInicio.Create( Self ) ;

  objForm.dDtPrevConclusao := qryOPPlanejadadDtPrevConclusao.Value ;

  showForm( objForm , FALSE ) ;

  if (objForm.bCancelado) then
  begin
      freeAndNil( objForm ) ;
      abort ;
  end ;

  dDtAgendada := objForm.MonthCalendar1.Date;

  freeAndNil( objForm ) ;

  if (MessageDlg('A data de in�cio prevista da OP � : ' + DateToStr(dDtAgendada) + '. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans ;

  try

      SP_AGENDA_INICIO_OP.Close ;
      SP_AGENDA_INICIO_OP.Parameters.ParamByName('@nCdOrdemProducao').Value := qryOPPlanejadanCdOrdemProducao.Value ;
      SP_AGENDA_INICIO_OP.Parameters.ParamByName('@cDtPrevInicio').Value    := DateToStr(dDtAgendada) ;
      SP_AGENDA_INICIO_OP.ExecProc ;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Ordem de produ��o agendada com Sucesso!') ;

end;

procedure TfrmMonitorOP.AtualizarOrdensEmProcesso1Click(Sender: TObject);
begin
  inherited;

  PosicionaQuery( qryOPAberta , intToStr( frmMenu.nCdEmpresaAtiva ) ) ;

end;

procedure TfrmMonitorOP.RegistrarConcluso1Click(Sender: TObject);
var
    objForm : TfrmEntradaProducao ;
begin
  inherited;

  objForm := TfrmEntradaProducao.Create( Self );
  objForm.registraConclusaoExterna(qryOPProcessocNumeroOP.Value , 1);

  freeAndNil( objForm ) ;

  qryRegistroEntrada.Close;
  PosicionaQuery( qryRegistroEntrada , qryOPProcessonCdOrdemProducao.AsString ) ;
  
end;

procedure TfrmMonitorOP.RegistrarRetrabalho1Click(Sender: TObject);
var
    objForm : TfrmEntradaProducao ;
begin
  inherited;

  objForm := TfrmEntradaProducao.Create( Self );
  objForm.registraConclusaoExterna(qryOPProcessocNumeroOP.Value , 2);

  freeAndNil( objForm ) ;

  qryRegistroEntrada.Close;
  PosicionaQuery( qryRegistroEntrada , qryOPProcessonCdOrdemProducao.AsString ) ;

end;

procedure TfrmMonitorOP.RegistrarPerda1Click(Sender: TObject);
var
    objForm : TfrmEntradaProducao ;
begin
  inherited;

  objForm := TfrmEntradaProducao.Create( Self );
  objForm.registraConclusaoExterna(qryOPProcessocNumeroOP.Value , 3);

  freeAndNil( objForm ) ;

  qryRegistroEntrada.Close;
  PosicionaQuery( qryRegistroEntrada , qryOPProcessonCdOrdemProducao.AsString ) ;

end;

procedure TfrmMonitorOP.PopupMenu3Popup(Sender: TObject);
begin
  inherited;

  MenuItem4.Enabled                  := ((qryOPProcesso.Active) and (qryOPProcesso.RecordCount > 0)) ;
  MenuItem6.Enabled                  := MenuItem4.Enabled ;
  MenuItem7.Enabled                  := MenuItem4.Enabled ;
  CancelarOP2.Enabled                := MenuItem4.Enabled ;
  VisualizarOP2.Enabled              := MenuItem4.Enabled ;
  RequisitarProdutos2.Enabled        := MenuItem4.Enabled ;

  RegistrarPerda1.Enabled            := (MenuItem4.Enabled) and (frmMenu.LeParametro('PERMREGCONCMOP') = 'S');
  RegistrarRetrabalho1.Enabled       := MenuItem4.Enabled ;
  RegistrarConcluso1.Enabled         := MenuItem4.Enabled ;

end;

initialization
    RegisterClass( TfrmMonitorOP ) ;

end.
