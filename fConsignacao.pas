unit fConsignacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, DBGridEhGrouping, cxLookAndFeelPainters, StdCtrls, cxButtons,
  GridsEh, DBGridEh, cxPC, cxControls, Mask, DBCtrls, Menus, cxContainer,
  cxEdit, cxGroupBox, RDprint, ToolCtrlsEh;

type
  TfrmConsignacao = class(TfrmCadastro_Padrao)
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit13: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    btnLeitor: TcxButton;
    dsItemConsignacao: TDataSource;
    qryProduto: TADOQuery;
    qryProdutocReferencia: TStringField;
    qryProdutonValVenda: TBCDField;
    qryItemConsignacao: TADOQuery;
    qryVendedor: TADOQuery;
    qryTerceiro: TADOQuery;
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    dsTerceiro: TDataSource;
    dsVendedor: TDataSource;
    dsEmpresa: TDataSource;
    dsLoja: TDataSource;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    qryVendedornCdUsuario: TIntegerField;
    qryVendedorcNmUsuario: TStringField;
    DBEdit14: TDBEdit;
    btnFinalizar: TToolButton;
    btnProtocolo: TToolButton;
    btnGerarPedido: TcxButton;
    btnRecebDev: TcxButton;
    SP_BAIXA_ESTOQUE_CONSIG: TADOStoredProc;
    cxGroupBox1: TcxGroupBox;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label5: TLabel;
    qryUsuDtPedido: TADOQuery;
    qryUsuDtDev: TADOQuery;
    qryUsuDtCancel: TADOQuery;
    qryUsuDtFinaliza: TADOQuery;
    dsUsuDtPed: TDataSource;
    dsUsuDtDev: TDataSource;
    dsUsuDtCancel: TDataSource;
    dsUsuDtFinaliza: TDataSource;
    qryUsuDtPedidonCdUsuario: TIntegerField;
    qryUsuDtPedidocNmLogin: TStringField;
    qryUsuDtDevnCdUsuario: TIntegerField;
    qryUsuDtDevcNmLogin: TStringField;
    qryUsuDtCancelnCdUsuario: TIntegerField;
    qryUsuDtCancelcNmLogin: TStringField;
    qryUsuDtFinalizanCdUsuario: TIntegerField;
    qryUsuDtFinalizacNmLogin: TStringField;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    DataSource6: TDataSource;
    DataSource7: TDataSource;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    qryLojanCdEmpresa: TIntegerField;
    qryLojanCdLocalEstoquePadrao: TIntegerField;
    DBEdit6: TDBEdit;
    qryTipoStatusConsig: TADOQuery;
    dsTipoStatusConsig: TDataSource;
    qryTipoStatusConsignCdTipoStatusConsig: TIntegerField;
    qryTipoStatusConsigcTipoStatusConsig: TStringField;
    DBEdit7: TDBEdit;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryItemConsignacaonCdItemConsignacao: TIntegerField;
    qryItemConsignacaonCdConsignacao: TIntegerField;
    qryItemConsignacaonCdProduto: TIntegerField;
    qryItemConsignacaocNmProduto: TStringField;
    qryItemConsignacaocReferencia: TStringField;
    qryItemConsignacaonQtdePedido: TIntegerField;
    qryItemConsignacaonQtdeDevolvida: TIntegerField;
    qryItemConsignacaonQtdeRetirada: TIntegerField;
    qryItemConsignacaonValUnitario: TBCDField;
    SP_PDV_NOVO_PEDIDO: TADOStoredProc;
    qryTerceirocRG: TStringField;
    qryTerceirocEmail: TStringField;
    qryTerceirocTelefone1: TStringField;
    SP_PDV_NOVO_PRODUTO: TADOStoredProc;
    SP_PDV_ENCERRA_PDV: TADOStoredProc;
    qryProdutocEAN: TStringField;
    qryVendedornCdTerceiroResponsavel: TIntegerField;
    Label12: TLabel;
    DBEdit8: TDBEdit;
    btnCancelarConsig: TcxButton;
    qryMasternCdConsignacao: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdPedido: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdUsuario: TIntegerField;
    qryMasternCdUsuarioPed: TIntegerField;
    qryMasternCdUsuarioCancel: TIntegerField;
    qryMasternCdUsuarioDev: TIntegerField;
    qryMasternCdUsuarioFinaliza: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtDevolucao: TDateTimeField;
    qryMasterdDtCancelamento: TDateTimeField;
    qryMasterdDtFinalizacao: TDateTimeField;
    qryMasternCdStatusConsig: TIntegerField;
    qryItemConsignacaonValTotal: TBCDField;
    qryItemConsignacaonValTotalCalc: TFloatField;
    qryItemConsignacaonQtdePedidoCalc: TIntegerField;
    SP_PDV_NOVO_PRODUTOcMsg: TStringField;
    qryAtualizaPed: TADOQuery;
    SP_DEVOLUCAO_ITEM_CONSIG: TADOStoredProc;
    SP_CANCELA_CONSIGNACAO: TADOStoredProc;
    SP_BUSCA_PRECO_PROMOCIONAL: TADOStoredProc;
    qryAtualizaItemPed: TADOQuery;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ValidaInfo;
    procedure DBGridEh1Enter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit10Exit(Sender: TObject);
    procedure DBEdit13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnFinalizarClick(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit13Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure qryItemConsignacaoBeforePost(DataSet: TDataSet);
    procedure HabilitaCampos;
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btnLeitorClick(Sender: TObject);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure btnRecebDevClick(Sender: TObject);
    procedure ExibeTelaLeitor(Acao : integer);
    procedure btnGerarPedidoClick(Sender: TObject);
    procedure AtualizaQry;
    procedure qryItemConsignacaoCalcFields(DataSet: TDataSet);
    procedure btnCancelarConsigClick(Sender: TObject);
    procedure btnProtocoloClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsignacao: TfrmConsignacao;

implementation

uses
    fMenu, fLookup_Padrao, fConsignacao_Leitor, fCaixa_Recebimento, rProtocoloConsignacao_view,
  ConvUtils;

{$R *.dfm}

procedure TfrmConsignacao.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK          : integer;
    nValPromocao : Real;
begin
    {Se o status da consigna��o for igual a 1 (Digitado), possibilita a consulta do produto no grid}

    if ((Key = VK_F4) and (DBGridEh1.Col = 1) and (qryMasternCdStatusConsig.Value = 1)) then
    begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(106, 'nCdTabTipoProduto = 3');

        qryProduto.Close;
        PosicionaQuery(qryProduto, IntToStr(nPK));

        if (nPK <= 0) then
            exit;

        frmMenu.Connection.BeginTrans;

        try
            SP_BUSCA_PRECO_PROMOCIONAL.Close;
            SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nCdEmpresa').Value        := qryEmpresanCdEmpresa.Value;
            SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nCdLoja').Value           := qryLojanCdLoja.Value;
            SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nCdProduto').Value        := qryProdutonCdProduto.Value;
            SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nQtdePDV').Value          := 1;
            SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nValPDV').Value           := qryProdutonValVenda.Value;
            SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nCdTipoTabPreco').Value   := Null;
            SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nCdCampanhaPromoc').Value := 0;
            SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nValPromocao').Value      := 0;
            SP_BUSCA_PRECO_PROMOCIONAL.ExecProc;

            nValPromocao := SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nValPromocao').Value;
        except
            MensagemErro('Erro no processamento.');

            frmMenu.Connection.RollbackTrans;

            Raise;
        end;

        frmMenu.Connection.CommitTrans;

        if (qryItemConsignacao.State = dsBrowse) then
            qryItemConsignacao.Edit;

        qryItemConsignacaonCdProduto.Value   := qryProdutonCdProduto.Value;
        qryItemConsignacaocNmProduto.Value   := qryProdutocNmProduto.Value;
        qryItemConsignacaocReferencia.Value  := qryProdutocReferencia.Value;

        if (nValPromocao > 0) then
            qryItemConsignacaonValUnitario.Value := nValPromocao
        else
            qryItemConsignacaonValUnitario.Value := qryProdutonValVenda.Value;

    end;

end;

procedure TfrmConsignacao.ValidaInfo;
begin
    {Valida o cabe�alho da consigna��o}

    if (qryMasternCdTerceiro.IsNull) then
    begin
        MensagemAlerta('Informe o terceiro.');

        DBEdit10.SetFocus;

        Abort;
    end

    else if (qryMasternCdUsuario.IsNull) then
    begin
        MensagemAlerta('Informe o vendedor.');

        DBEdit13.SetFocus;

        Abort;
    end
end;

procedure TfrmConsignacao.DBGridEh1Enter(Sender: TObject);
begin
    {Valida a informa��o do cabe�alho e habilita o grid para edi��o}

    if (qryMaster.State = dsInsert) then
        if (MessageDLG('Confirma a inclus�o do registro?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
        begin
            btSalvarClick(Self);

            qryItemConsignacao.Open;

            DBGridEh1.ReadOnly := False;

            btnFinalizar.Enabled := True;
        end;
end;

procedure TfrmConsignacao.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster   := 'CONSIGNACAO' ;
  nCdTabelaSistema  := 508 ;
  nCdConsultaPadrao := 370 ;

  bLimpaAposSalvar  := False ;

  btnFinalizar.Enabled := False;
end;

procedure TfrmConsignacao.DBEdit10KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : integer;
begin
    {Se a query estiver em modo de insert, ou o status da consigna��o for igual a 1 (Digitado), possibilita a consulta do Terceiros}

    if ((Key = VK_F4) and ((qryMaster.State = dsInsert) or (qryMasternCdStatusConsig.Value = 1))) then
    begin
        nPK := frmLookup_Padrao.ExecutaConsulta(200);

        if (nPK > 0) then
        begin
            qryMasternCdTerceiro.Value := nPK;

            qryTerceiro.Close;
            PosicionaQuery(qryTerceiro, IntToStr(nPK));
        end;
    end;
end;

procedure TfrmConsignacao.DBEdit10Exit(Sender: TObject);
begin
  inherited;

  {Busca e retorna o nome do terceiro ao sair do campo de c�digo de terceiro}

  if not (Trim(DBEdit10.Text) = '') then
  begin
      qryTerceiro.Close;
      PosicionaQuery(qryTerceiro, DBEdit10.Text);

      if (qryTerceiro.IsEmpty) then
      begin
          MensagemAlerta('Terceiro n�o localizado.');

          DBEdit10.SetFocus;

          qryMasternCdTerceiro.Clear;
      end;
  end;
end;

procedure TfrmConsignacao.DBEdit13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : integer;
begin
    {Se a query estiver em modo de insert, ou o status da consigna��o for igual a 1 (Digitado), possibilita a consulta de Vendedores}

    if ((Key = VK_F4) and ((qryMaster.State = dsInsert) or (qryMasternCdStatusConsig.Value = 1))) then
    begin
        nPK := frmLookup_Padrao.ExecutaConsulta(155);

        if (nPK > 0) then
        begin
            qryMasternCdUsuario.Value := nPK;

            qryVendedor.Close;
            PosicionaQuery(qryVendedor, IntToStr(nPK));
        end;
    end;
end;

procedure TfrmConsignacao.btnFinalizarClick(Sender: TObject);
begin
    if (qryItemConsignacao.IsEmpty) then
        MensagemAlerta('N�o h� nenhum produto na tabela.')

    else if (qryMasternCdStatusConsig.Value = 1) and (MessageDLG('Ao finalizar n�o ser� mais poss�vel alterar os produtos retirados. Confirma a finaliza��o da consigna��o?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
    begin
        {Valida as informa��es da tela, muda o status da consignacao para 2 (Finalizado - Aguardando devolu��o), e realiza as opera��es necess�rias no estoque}

        qryMasternCdStatusConsig.Value := 2;

        ValidaInfo;

        frmMenu.Connection.BeginTrans;

        try
            SP_BAIXA_ESTOQUE_CONSIG.Close;
            SP_BAIXA_ESTOQUE_CONSIG.Parameters.ParamByName('@nCdConsignacao').Value := qryMasternCdConsignacao.Value;
            SP_BAIXA_ESTOQUE_CONSIG.ExecProc;
        except
            frmMenu.Connection.RollbackTrans;
            MensagemErro('Erro no processamento.');
            raise;
        end;

        frmMenu.Connection.CommitTrans;

        qryMaster.Post;

        qryItemConsignacao.Close;
        qryItemConsignacao.Open;

        HabilitaCampos;

        ShowMessage('Consigna��o finalizada com sucesso.');
    end;
end;

procedure TfrmConsignacao.btIncluirClick(Sender: TObject);
begin
  inherited;

  {Insere no cabe�alho da consigna��o a loja ativa e a empresa da respectiva loja}

  qryLoja.Close;
  PosicionaQuery(qryLoja, IntToStr(frmMenu.nCdLojaAtiva));

  qryMasternCdLoja.Value    := qryLojanCdLoja.Value;
  qryMasternCdEmpresa.Value := qryLojanCdEmpresa.Value;

  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString);

  HabilitaCampos;

  DBEdit10.SetFocus;
end;

procedure TfrmConsignacao.btSalvarClick(Sender: TObject);
begin
    ValidaInfo;

    inherited;

    if (qryMasternCdStatusConsig.Value = 1) then
    begin
        qryItemConsignacao.Open;

        btnLeitor.Enabled := True;
    end;

    ShowMessage('Registro salvo com sucesso.');
end;

procedure TfrmConsignacao.qryMasterBeforePost(DataSet: TDataSet);
begin
    if (qryMaster.State = dsInsert) then
    begin
        qryMasternCdConsignacao.Value  := frmMenu.fnProximoCodigo('CONSIGNACAO');
        qryMasterdDtPedido.Value       := Now;
        qryMasternCdStatusConsig.Value := 1;
        qryMasternCdUsuarioPed.Value   := frmMenu.nCdUsuarioLogado;
    end;

    AtualizaQry;
end;

procedure TfrmConsignacao.btCancelarClick(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close;
  qryLoja.Close;
  qryTerceiro.Close;
  qryVendedor.Close;
  qryTipoStatusConsig.Close;
  qryUsuDtPedido.Close;
  qryUsuDtDev.Close;
  qryUsuDtCancel.Close;
  qryUsuDtFinaliza.Close;
  qryItemConsignacao.Close;

  HabilitaCampos;
end;

procedure TfrmConsignacao.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : integer;
begin
    if ((Key = VK_F4) and (qryMaster.State = dsInsert)) then
    begin
        nPK := frmLookup_Padrao.ExecutaConsulta(325);

        if (nPK > 0) then
        begin
            qryMasternCdStatusConsig.Value := nPK;

            qryTipoStatusConsig.Close;
            PosicionaQuery(qryTipoStatusConsig, IntToStr(nPk));
        end;
    end;
end;

procedure TfrmConsignacao.DBEdit13Exit(Sender: TObject);
begin
    if not (Trim(DBEdit13.Text) = '') then
    begin
        qryVendedor.Close;
        PosicionaQuery(qryVendedor, DBEdit13.Text);

        if (qryVendedor.IsEmpty) then
        begin
            MensagemAlerta('Terceiro n�o localizado.');

            qryMasternCdUsuario.Clear;

            Abort;
        end;

        DBGridEh1.SetFocus;
    end;
end;

procedure TfrmConsignacao.DBEdit6Exit(Sender: TObject);
begin
  inherited;
    if not (Trim(DBEdit6.Text) = '') then
    begin
        qryTipoStatusConsig.Close;
        PosicionaQuery(qryTipoStatusConsig, DBEdit6.Text);

        if (qryTipoStatusConsig.IsEmpty) then
        begin
            MensagemAlerta('Status inv�lido.');

            qryMasternCdStatusConsig.Clear;

            Abort;
        end;
    end;
end;

procedure TfrmConsignacao.qryItemConsignacaoBeforePost(DataSet: TDataSet);
begin
    if (Trim(qryItemConsignacaocNmProduto.Value) = '') then
    begin
        MensagemAlerta('Informe um produto v�lido.');
        Abort;
    end

    else if (qryItemConsignacaonQtdeRetirada.Value < 1) then
    begin
        MensagemAlerta('Informe a quantidade do produto.');
        Abort;
    end;

    qryItemConsignacaonCdConsignacao.Value := qryMasternCdConsignacao.Value;
    qryItemConsignacaonValTotal.Value      := qryItemConsignacaonValTotalCalc.Value;
    qryItemConsignacaonQtdePedido.Value    := qryItemConsignacaonQtdePedidoCalc.Value;

    if (qryItemConsignacao.State = dsInsert) then
        qryItemConsignacaonCdItemConsignacao.Value := frmMenu.fnProximoCodigo('ITEMCONSIGNACAO');
end;

procedure TfrmConsignacao.HabilitaCampos;
begin
    if not (qryMaster.State = dsInsert) then
    begin
        case (qryMasternCdStatusConsig.Value) of

        1: begin
               DBGridEh1.ReadOnly := False;

               DBEdit10.ReadOnly := False;
               DBEdit13.ReadOnly := False;

               btnLeitor.Enabled         := True;
               btnRecebDev.Enabled       := False;
               btnGerarPedido.Enabled    := False;
               btnFinalizar.Enabled      := True;
               btnProtocolo.Enabled      := False;
               btnCancelarConsig.Enabled := True;
           end;

        2: begin
               DBGridEh1.ReadOnly := True;

               DBEdit10.ReadOnly := True;
               DBEdit13.ReadOnly := True;

               btnLeitor.Enabled         := False;
               btnRecebDev.Enabled       := True;
               btnGerarPedido.Enabled    := True;
               btnFinalizar.Enabled      := False;
               btnProtocolo.Enabled      := True;
               btnCancelarConsig.Enabled := True;
           end;

        3: begin
               DBGridEh1.ReadOnly := True;

               DBEdit10.ReadOnly := True;
               DBEdit13.ReadOnly := True;

               btnLeitor.Enabled         := False;
               btnRecebDev.Enabled       := False;
               btnGerarPedido.Enabled    := True;
               btnFinalizar.Enabled      := False;
               btnProtocolo.Enabled      := True;
               btnCancelarConsig.Enabled := True;
           end;

        4: begin
               DBGridEh1.ReadOnly := True;

               DBEdit10.ReadOnly := True;
               DBEdit13.ReadOnly := True;

               btnLeitor.Enabled         := False;
               btnRecebDev.Enabled       := False;
               btnGerarPedido.Enabled    := False;
               btnFinalizar.Enabled      := False;
               btnProtocolo.Enabled      := True;
               btnCancelarConsig.Enabled := False;
           end;

        5: begin
               DBGridEh1.ReadOnly := True;

               DBEdit10.ReadOnly := True;
               DBEdit13.ReadOnly := True;

               btnLeitor.Enabled         := False;
               btnRecebDev.Enabled       := False;
               btnGerarPedido.Enabled    := False;
               btnFinalizar.Enabled      := False;
               btnProtocolo.Enabled      := True;
               btnCancelarConsig.Enabled := False;
           end;
        end;
    end

    else
    begin
        DBGridEh1.ReadOnly := True;

        DBEdit10.ReadOnly := False;
        DBEdit13.ReadOnly := False;

        btnLeitor.Enabled         := False;
        btnRecebDev.Enabled       := False;
        btnGerarPedido.Enabled    := False;
        btnFinalizar.Enabled      := False;
        btnProtocolo.Enabled      := False;
        btnCancelarConsig.Enabled := False;
    end;
end;

procedure TfrmConsignacao.qryMasterAfterPost(DataSet: TDataSet);
begin
{  inherited; }
end;

procedure TfrmConsignacao.ExibeTelaLeitor(Acao: integer);
var
    objForm : TfrmConsignacao_Leitor;
    nPK     : integer;
begin
    {Cria a tela de leitor, e transfere os itens da consigna��o para uma tabela tempor�ria da tela de leitor, para compara��o dos produtos}

    if (qryItemConsignacao.State = dsEdit) then
        qryItemConsignacao.Post;

    qryItemConsignacao.First;

    objForm := TfrmConsignacao_Leitor.Create(Self);

    objForm.qryTempItens.Open;

    qryItemConsignacao.First;

    while not (qryItemConsignacao.Eof) do
    begin
        objForm.qryTempItens.Insert;
        objForm.qryTempItensnCdProduto.Value    := qryItemConsignacaonCdProduto.Value;
        objForm.qryTempItensnQtdeRetirada.Value := qryItemConsignacaonQtdeRetirada.Value;
        objForm.qryTempItens.Post;

        qryItemConsignacao.Next;
    end;

    {Transfere o c�digo da consigna��o, o local de estoque padrao da loja, e o c�digo da a��o � ser realizada no bot�o processar da tela do leitor}

    objForm.nCdConsignacao  := qryMasternCdConsignacao.Value;
    objForm.nCdLocalEstoque := qryLojanCdLocalEstoquePadrao.Value;
    objForm.nCdAcao         := Acao;

    objForm.ShowModal;

    { -- verifica se consigna��o foi processada -- }
    if (not objForm.bProcessado) then
        Exit;

    qryItemConsignacao.Close;
    qryItemConsignacao.Open;

    {Se houverem registros devolvidos na devolu��o de produtos, atualiza o status da consigna��o}

    if (objForm.iQtdeDev > 0) then
    begin
        qryMaster.Edit;

        qryMasternCdStatusConsig.Value := 3;
        qryMasterdDtDevolucao.Value    := Now;
        qryMasternCdUsuarioDev.Value   := frmMenu.nCdUsuarioLogado;

        nPK := qryMasternCdConsignacao.Value;

        qryMaster.Post;

        qryItemConsignacao.Close;
        qryItemConsignacao.Open;

        if (ObjForm.iQtdeDev >= DBGridEh1.FieldColumns['nQtdeRetirada'].Footer.SumValue) then
        begin

            frmMenu.Connection.BeginTrans;

            try
                {Atualiza o estoque de acordo com os itens da consigna��o}

                SP_CANCELA_CONSIGNACAO.Close;
                SP_CANCELA_CONSIGNACAO.Parameters.ParamByName('@nCdConsignacao').Value := qryMasternCdConsignacao.Value;
                SP_CANCELA_CONSIGNACAO.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
                SP_CANCELA_CONSIGNACAO.ExecProc;
            except
                frmMenu.Connection.RollbackTrans;

                MensagemErro('Erro no processamento');

                raise;
            end;

            frmMenu.Connection.CommitTrans;

            ShowMessage('Consigna��o cancelada por falta de saldo.');

            qryMaster.Close;
            PosicionaQuery(qryMaster, IntToStr(nPK));
        end;

    end;

    HabilitaCampos;

    FreeAndNil(objForm);
end;

procedure TfrmConsignacao.btnLeitorClick(Sender: TObject);
begin
    ExibeTelaLeitor(1);
end;

procedure TfrmConsignacao.btnRecebDevClick(Sender: TObject);
begin
    ExibeTelaLeitor(2);
end;

procedure TfrmConsignacao.DBGridEh1ColExit(Sender: TObject);
var
    nValPromocao : real;
begin
    {Se o status da consigna��o for igual a 1 (Digitado), busca e retorna os dados do produto ao sair da primeira coluna do grid}

    if ((DBGridEh1.Col = 1) and (qryMasternCdStatusConsig.Value = 1)) then
    begin
        qryProduto.Close;
        PosicionaQuery(qryProduto, qryItemConsignacaonCdProduto.AsString);

        if (qryProduto.IsEmpty) and ((qryItemConsignacao.State = dsInsert) or (qryItemConsignacao.State = dsEdit)) then
        begin
            MensagemAlerta('Produto n�o encontrado.');

            qryItemConsignacaonCdProduto.Clear;
            qryItemConsignacaocNmProduto.Clear;
            qryItemConsignacaocReferencia.Clear;
            qryItemConsignacaonValUnitario.Clear;
        end

        else if (qryItemConsignacao.State = dsInsert) or (qryItemConsignacao.State = dsEdit) then
        begin
            frmMenu.Connection.BeginTrans;

            try
                SP_BUSCA_PRECO_PROMOCIONAL.Close;
                SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nCdEmpresa').Value        := qryMasternCdEmpresa.Value;
                SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nCdLoja').Value           := qryMasternCdLoja.Value;
                SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nCdProduto').Value        := qryProdutonCdProduto.Value;
                SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nQtdePDV').Value          := 1;
                SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nValPDV').Value           := qryProdutonValVenda.Value;
                SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nCdTipoTabPreco').Value   := Null;
                SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nCdCampanhaPromoc').Value := 0;
                SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nValPromocao').Value      := 0;
                SP_BUSCA_PRECO_PROMOCIONAL.ExecProc;

                nValPromocao := SP_BUSCA_PRECO_PROMOCIONAL.Parameters.ParamByName('@nValPromocao').Value;
            except
                MensagemErro('Erro no processamento.');

                frmMenu.Connection.RollbackTrans;

                Raise;
            end;

            frmMenu.Connection.CommitTrans;

            qryItemConsignacaocNmProduto.Value   := qryProdutocNmProduto.Value;
            qryItemConsignacaocReferencia.Value  := qryProdutocReferencia.Value;

            if (nValPromocao > 0) then
                qryItemConsignacaonValUnitario.Value := nValPromocao
            else
                qryItemConsignacaonValUnitario.Value := qryProdutonValVenda.Value;

        end;
    end;

    HabilitaCampos;

    DBGridEh1.SetFocus;
end;

procedure TfrmConsignacao.btnGerarPedidoClick(Sender: TObject);
var
    nCdPedido, cont : integer;
    objForm : TfrmCaixa_Recebimento;
begin
    nCdPedido := 0;

    {Armazena a quantidade de total de produtos devolvidos}

    cont := 0;

    qryItemConsignacao.First;

    while not (qryItemConsignacao.Eof) do
    begin
        cont := cont + qryItemConsignacaonQtdeDevolvida.Value;

        qryItemConsignacao.Next;
    end;

    if (MessageDlg('Confirma a gera��o do pedido?', mtConfirmation, [mbYes,mbNo], 0) = mrNo) then
        exit;

    {-- inicia a gera��o do pedido --}

    if (cont = 0) then
        if (MessageDLG('Nenhuma devolu��o de produto encontrada, deseja continuar?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
        begin
            qryMaster.Edit;

            qryMasterdDtDevolucao.Value  := Now;
            qryMasternCdUsuarioDev.Value := frmMenu.nCdUsuarioLogado;

            qryMaster.Post;
        end else
            Abort;

    {-- inicia o processo de gera��o do pedido --}
    frmMenu.Connection.BeginTrans;

    try

        {Gera um novo pedido de venda}

        SP_PDV_NOVO_PEDIDO.Close;
        SP_PDV_NOVO_PEDIDO.Parameters.ParamByName('@nCdLoja').Value          := qryLojanCdLoja.Value;
        SP_PDV_NOVO_PEDIDO.Parameters.ParamByName('@nCdTerceiroColab').Value := qryVendedornCdTerceiroResponsavel.Value;
        SP_PDV_NOVO_PEDIDO.Parameters.ParamByName('@nCdTerceiro').Value      := qryTerceironCdTerceiro.Value;
        SP_PDV_NOVO_PEDIDO.Parameters.ParamByName('@nCdCondPagto').Value     := 0;
        SP_PDV_NOVO_PEDIDO.Parameters.ParamByName('@cCNPJCPF').Value         := qryTerceirocCNPJCPF.Value;
        SP_PDV_NOVO_PEDIDO.Parameters.ParamByName('@cNmTerceiro').Value      := qryTerceirocNmTerceiro.Value;
        SP_PDV_NOVO_PEDIDO.Parameters.ParamByName('@cRG').Value              := qryTerceirocRG.Value;
        SP_PDV_NOVO_PEDIDO.Parameters.ParamByName('@cEmail').Value           := qryTerceirocEmail.Value;
        SP_PDV_NOVO_PEDIDO.Parameters.ParamByName('@cTelefone').Value        := qryTerceirocTelefone1.Value;
        SP_PDV_NOVO_PEDIDO.Parameters.ParamByName('@cNrCartao').Value        := '';
        SP_PDV_NOVO_PEDIDO.Parameters.ParamByName('@nCdPedido').Value        := nCdPedido;
        SP_PDV_NOVO_PEDIDO.ExecProc;

        nCdPedido := SP_PDV_NOVO_PEDIDO.Parameters.ParamByName('@nCdPedido').Value;

        {Adiciona os produtos da consignacao como itens do pedido gerado}

        qryItemConsignacao.First;

        while not (qryItemConsignacao.Eof) do
        begin

            {Se ap�s a devolu��o houver quantidade adiciona o produto como um produto do pedido}

            if (qryItemConsignacaonQtdePedido.Value > 0) then
            begin

                SP_PDV_NOVO_PRODUTO.Close;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdLoja').Value            := qryLojanCdLoja.Value;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdTerceiroColab').Value   := qryVendedornCdUsuario.Value;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdTerceiro').Value        := qryTerceironCdTerceiro.Value;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdCondPagto').Value       := 0;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdPedido').Value          := nCdPedido;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cEAN').Value               := qryItemConsignacaonCdProduto.Value;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cAcao').Value              := 'V';
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nQtde').Value              := qryItemConsignacaonQtdePedido.Value;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cNmProduto').Value         := qryItemConsignacaocNmProduto.Value;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cTipoVenda').Value         := 'P';
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdVale').Value            := 0;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cCNPJCPF').Value           := qryTerceirocCNPJCPF.Value;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cNmTerceiro').Value        := qryTerceirocNmTerceiro.Value;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cRG').Value                := qryTerceirocRG.Value;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cEmail').Value             := qryTerceirocEmail.Value;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cTelefone').Value          := qryTerceirocTelefone1.Value;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cNrCartao').Value          := '';
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdItemPedidoTroca').Value := 0;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdProdutoTroca').Value    := 0;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nValUnitarioTroca').Value  := 0;
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@cOBS').Value               := '';
                SP_PDV_NOVO_PRODUTO.Parameters.ParamByName('@nCdUsuarioAutorPDV').Value := 0;
                SP_PDV_NOVO_PRODUTO.Open;

                if (SP_PDV_NOVO_PRODUTO.FieldByName('cMsg').Value <> 'OK') then
                    RaiseConversionError(SP_PDV_NOVO_PRODUTO.FieldByName('cMsg').Value);

            end;

            qryItemConsignacao.Next;
        end;



        {Encerra o Pedido de venda gerado}

        SP_PDV_ENCERRA_PDV.Close;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@nCdPedido').Value   := nCdPedido;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@nCdTerceiro').Value := qryTerceironCdTerceiro.Value;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@cCNPJCPF').Value    := qryTerceirocNmTerceiro.Value;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@cNmTerceiro').Value := qryTerceirocNmTerceiro.Value;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@cRG').Value         := qryTerceirocRG.Value;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@cEmail').Value      := qryTerceirocEmail.Value;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@cTelefone').Value   := qryTerceirocTelefone1.Value;
        SP_PDV_ENCERRA_PDV.Parameters.ParamByName('@cNrCartao').Value   := '';
        SP_PDV_ENCERRA_PDV.ExecProc;

        qryAtualizaItemPed.Close;
        qryAtualizaItemPed.Parameters.ParamByName('nPK').Value := nCdPedido;
        qryAtualizaItemPed.ExecSQL;

        qryAtualizaPed.Close;
        qryAtualizaPed.Parameters.ParamByName('nPK').Value := nCdPedido;
        qryAtualizaPed.ExecSQL;
    except
        frmMenu.Connection.RollbackTrans;

        MensagemErro('Erro no processamento.');

        raise;
    end;

    frmMenu.Connection.CommitTrans;

    ShowMessage('Pedido gerado com sucesso.');

    {Atualiza o status da consigna��o}

    qryMaster.Edit;

    qryMasternCdStatusConsig.Value    := 4;
    qryMasterdDtFinalizacao.Value     := Now;
    qryMasternCdUsuarioFinaliza.Value := frmMenu.nCdUsuarioLogado;
    qryMasternCdPedido.Value          := nCdPedido;

    qryMaster.Post;

    HabilitaCampos;

    {Exibe a tela de recebimento do caixa j� com o n�mero do pedido no edit}

    objForm := TfrmCaixa_Recebimento.Create(nil);

    objForm.cFlgUsaConsig := 1;

    objForm.edtNumPedido.Text := IntToStr(nCdPedido);

    objForm.ShowModal;

    FreeAndNil(objForm);

    DBGridEh1.SetFocus;
end;

procedure TfrmConsignacao.AtualizaQry;
begin
    {Atualiza as querys de acordo com os valores do cabe�alho da consigna��o}

    qryEmpresa.Close;
    PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString);

    qryLoja.Close;
    PosicionaQuery(qryLoja, qrymasternCdLoja.AsString);

    qryTerceiro.Close;
    PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.AsString);

    qryVendedor.Close;
    PosicionaQuery(qryVendedor, qryMasternCdUsuario.AsString);

    qryTipoStatusConsig.Close;
    PosicionaQuery(qryTipoStatusConsig, qryMasternCdStatusConsig.AsString);

    qryUsuDtPedido.Close;
    PosicionaQuery(qryUsuDtPedido, qryMasternCdUsuarioPed.AsString);

    qryUsuDtCancel.Close;
    PosicionaQuery(qryUsuDtCancel, qryMasternCdUsuarioCancel.AsString);

    qryUsuDtDev.Close;
    PosicionaQuery(qryUsuDtDev, qryMasternCdUsuarioDev.AsString);

    qryUsuDtFinaliza.Close;
    PosicionaQuery(qryUsuDtFinaliza, qryMasternCdUsuarioFinaliza.AsString);

    qryItemConsignacao.Close;
    PosicionaQuery(qryItemConsignacao, qryMasternCdConsignacao.AsString);
end;

procedure TfrmConsignacao.qryItemConsignacaoCalcFields(DataSet: TDataSet);
begin
    {Atribui os valores necess�rios aos campos calculados}

    qryItemConsignacaonValTotalCalc.Value   := (qryItemConsignacaonQtdeRetirada.Value - qryItemConsignacaonQtdeDevolvida.Value) * qryItemConsignacaonValUnitario.Value;
    qryItemConsignacaonQtdePedidoCalc.Value := qryItemConsignacaonQtdeRetirada.Value - qryItemConsignacaonQtdeDevolvida.Value;
end;

procedure TfrmConsignacao.btnCancelarConsigClick(Sender: TObject);
begin
    if (MessageDLG('Confirma o cancelamento dessa consigna��o?', mtConfirmation, [mbYes,mbNo], 0) = mrNo) then
        exit;

    if (qryMasternCdStatusConsig.Value IN [1,2,3]) then
    begin

        frmMenu.Connection.BeginTrans;

        try
            {Atualiza o estoque de acordo com os itens da consigna��o}

            SP_CANCELA_CONSIGNACAO.Close;
            SP_CANCELA_CONSIGNACAO.Parameters.ParamByName('@nCdConsignacao').Value := qryMasternCdConsignacao.Value;
            SP_CANCELA_CONSIGNACAO.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
            SP_CANCELA_CONSIGNACAO.ExecProc;
        except
            frmMenu.Connection.RollbackTrans;

            MensagemErro('Erro no processamento');

            raise;
        end;

        frmMenu.Connection.CommitTrans;

        ShowMessage('Consigna��o cancelada com sucesso.');

        qryMaster.Close;
        qryMaster.Open;

        HabilitaCampos;
    end;

end;

procedure TfrmConsignacao.btnProtocoloClick(Sender: TObject);
var
    objForm : TrptProtocoloConsignacao_view;
begin
    {Exibe a tela do protocolo de consigna��o}

    objForm := TrptProtocoloConsignacao_view.Create(nil);

    objForm.cmdPreparaTemp.Execute;

    objForm.SPREL_CONSIGNACAO.Close;
    objForm.SPREL_CONSIGNACAO.Parameters.ParamByName('@nCdConsignacao').Value := qryMasternCdConsignacao.Value;
    objForm.SPREL_CONSIGNACAO.ExecProc;

    objForm.qryConsignacao.Open;
    objForm.qryItemConsignacao.Open;
    objForm.qryTotal.Open;

    objForm.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

    objForm.Preview;

    FreeAndNil(objForm);
end;

procedure TfrmConsignacao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  AtualizaQry;

  HabilitaCampos;
end;

procedure TfrmConsignacao.qryMasterAfterOpen(DataSet: TDataSet);
begin
  inherited;

  HabilitaCampos;

  AtualizaQry;
end;

initialization

    RegisterClass(TfrmConsignacao);
end.
