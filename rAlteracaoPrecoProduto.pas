unit rAlteracaoPrecoProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, StdCtrls, Mask, ImgList, ComCtrls, ToolWin,
  ExtCtrls;

type
  TrptAlteracaoPrecoProduto = class(TfrmRelatorio_Padrao)
    edtDtFinal: TMaskEdit;
    Label6: TLabel;
    edtDtInicial: TMaskEdit;
    Label3: TLabel;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptAlteracaoPrecoProduto: TrptAlteracaoPrecoProduto;

implementation

uses rAlteracaoPrecoProduto_view, fMenu;

{$R *.dfm}

procedure TrptAlteracaoPrecoProduto.ToolButton1Click(Sender: TObject);
var
  objRel : TrptAlteracaoPrecoProduto_view;
begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') then
      edtDtInicial.Text := DateToStr(Date) ;

  if (trim(edtDtFinal.Text) = '/  /') then
      edtDtFinal.Text := DateToStr(Date) ;

  if (StrToDate(edtDtInicial.Text) > StrToDate(edtDtFinal.Text)) then
  begin
      MensagemAlerta('Per�odo inv�lido.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  objRel := TrptAlteracaoPrecoProduto_view.Create(nil);

  try
      try
          objRel.qryHistAltPreco.Close;
          objRel.qryHistAltPreco.Parameters.ParamByName('dDtInicial').Value := frmMenu.ConvData(edtDtInicial.Text) ;
          objRel.qryHistAltPreco.Parameters.ParamByName('dDtFinal').Value   := frmMenu.ConvData(edtDtFinal.Text) ;
          objRel.qryHistAltPreco.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
          objRel.qryHistAltPreco.Open ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := 'Per�odo de Altera��o : ' + edtDtInicial.Text + ' a ' + edtDtFinal.Text;

          if (frmMenu.nCdLojaAtiva > 0) then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Loja : ' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdLojaAtiva),3) ;

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel) ;
  end;
end;

initialization
    RegisterClass(TrptAlteracaoPrecoProduto) ;

end.
