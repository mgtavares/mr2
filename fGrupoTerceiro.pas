unit fGrupoTerceiro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmGrupoTerceiro = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoTerceiro: TIntegerField;
    qryMastercNmGrupoTerceiro: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoTerceiro: TfrmGrupoTerceiro;

implementation

{$R *.dfm}

procedure TfrmGrupoTerceiro.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
  
end;

procedure TfrmGrupoTerceiro.qryMasterBeforePost(DataSet: TDataSet);
begin

  if ( qryMastercNmGrupoTerceiro.Value = '' ) then
  begin
      MensagemAlerta('Informe a descri��o.') ;
      DBEdit2.SetFocus;
      abort;
  end ;

  inherited;

end;

procedure TfrmGrupoTerceiro.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOTERCEIRO' ;
  nCdTabelaSistema  := 231 ;
  nCdConsultaPadrao := 228 ;

end;

initialization
    RegisterClass( tFrmGrupoTerceiro );
end.
