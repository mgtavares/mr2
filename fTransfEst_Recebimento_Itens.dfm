inherited frmTransfEst_Recebimento_Itens: TfrmTransfEst_Recebimento_Itens
  Left = 33
  Top = 172
  Width = 858
  Height = 346
  BorderIcons = []
  Caption = 'Itens da Transfer'#234'ncia'
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 850
    Height = 286
  end
  inherited ToolBar1: TToolBar
    Width = 850
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 850
    Height = 286
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsItens
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        Width = 44
      end
      item
        EditButtons = <>
        FieldName = 'cEAN'
        Footers = <>
        Width = 109
      end
      item
        EditButtons = <>
        FieldName = 'nQtde'
        Footers = <>
        Width = 76
      end
      item
        EditButtons = <>
        FieldName = 'cNmProduto'
        Footers = <>
        Width = 579
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryItens: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ItemTransfEst.nCdProduto'
      '      ,ItemTransfEst.cEAN'
      '      ,cNmProduto'
      '      ,Sum(ItemTransfEst.nQtde) nQtde'
      '  FROM ItemTransfEst'
      
        '       INNER JOIN Produto ON Produto.nCdProduto = ItemTransfEst.' +
        'nCdProduto'
      ' WHERE nCdTransfEst = :nPK'
      ' GROUP BY ItemTransfEst.nCdProduto'
      '         ,ItemTransfEst.cEAN'
      '         ,cNmProduto')
    Left = 256
    Top = 96
    object qryItensnCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryItenscEAN: TStringField
      DisplayLabel = 'Produto|EAN'
      FieldName = 'cEAN'
    end
    object qryItenscNmProduto: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryItensnQtde: TBCDField
      DisplayLabel = 'Quantidade|Transferida'
      FieldName = 'nQtde'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object dsItens: TDataSource
    DataSet = qryItens
    Left = 296
    Top = 96
  end
end
