unit fCobranca_VisaoContato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  cxPC, cxControls, cxLookAndFeelPainters, cxButtons, DB, Mask, DBCtrls,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  GridsEh, DBGridEh, Menus, DBGridEhGrouping,fCobranca_VisaoContato_Contato,
  ToolCtrlsEh;

type
  TfrmCobranca_VisaoContato = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    cxPageControl1: TcxPageControl;
    tabTitulos: TcxTabSheet;
    tabPagamentos: TcxTabSheet;
    tabPromessas: TcxTabSheet;
    tabContatos: TcxTabSheet;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocRG: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    dsTerceiro: TDataSource;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    DBEdit4: TDBEdit;
    btExibeCliente: TcxButton;
    qryContatoTerceiro: TADOQuery;
    qryContatoTerceironCdTerceiro: TIntegerField;
    qryContatoTerceirocNmTipoTelefone: TStringField;
    qryContatoTerceirocTelefone: TStringField;
    qryContatoTerceirocRamal: TStringField;
    qryContatoTerceirocContato: TStringField;
    qryContatoTerceirocDepartamento: TStringField;
    dsContatoTerceiro: TDataSource;
    Image2: TImage;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    qryTituloAberto: TADOQuery;
    qryTituloAbertocNrTit: TStringField;
    qryTituloAbertocNmEspTit: TStringField;
    qryTituloAbertodDtEmissao: TDateTimeField;
    qryTituloAbertodDtVenc: TDateTimeField;
    qryTituloAbertoiDiasAtraso: TIntegerField;
    qryTituloAbertonValTit: TBCDField;
    qryTituloAbertonSaldoTit: TBCDField;
    qryTituloAbertonValLiq: TBCDField;
    qryTituloAbertonValAbatimento: TBCDField;
    qryTituloAbertonValJuro: TBCDField;
    dsTituloAberto: TDataSource;
    btDetalheCompra: TcxButton;
    qryTituloAbertonCdLanctoFin: TIntegerField;
    qryTituloAbertoiParcela: TIntegerField;
    qryTituloAbertonCdTitulo: TIntegerField;
    qryTerceirocFlgPessoaFisica: TStringField;
    qryPagamentos: TADOQuery;
    qryPagamentoscNrTit: TStringField;
    qryPagamentoscNmEspTit: TStringField;
    qryPagamentosdDtEmissao: TDateTimeField;
    qryPagamentosdDtVenc: TDateTimeField;
    qryPagamentoscNmOperacao: TStringField;
    qryPagamentosdDtMov: TDateTimeField;
    qryPagamentosnValMov: TBCDField;
    qryPagamentoscOBSMov: TMemoField;
    dsPagamentos: TDataSource;
    Label4: TLabel;
    edtDtIniPagto: TMaskEdit;
    Label6: TLabel;
    edtDtFimPagto: TMaskEdit;
    cxButton4: TcxButton;
    Image3: TImage;
    Image4: TImage;
    Label5: TLabel;
    Label7: TLabel;
    edtDtIniContato: TMaskEdit;
    edtDtFimContato: TMaskEdit;
    cxButton5: TcxButton;
    qryPagamentosiParcela: TIntegerField;
    qryPagamentosiDiasAtraso: TIntegerField;
    qryContatos: TADOQuery;
    dsContatos: TDataSource;
    qryContatosnCdContatoCobranca: TAutoIncField;
    qryContatosdDtContato: TDateTimeField;
    qryContatoscNmPessoaContato: TStringField;
    qryContatoscTelefoneContato: TStringField;
    qryContatoscNmTipoResultadoContato: TStringField;
    qryContatosnCdLoja: TStringField;
    qryContatoscNmUsuario: TStringField;
    qryContatoscTextoContato: TMemoField;
    qryContatoscHoraContato: TStringField;
    cxButton7: TcxButton;
    qryAux: TADOQuery;
    qryPreparaTemp: TADOQuery;
    qryContatosdDtReagendado: TDateTimeField;
    qryContatoscFlgCarta: TIntegerField;
    qryContatosnCdItemListaCobranca: TIntegerField;
    PopupMenu1: TPopupMenu;
    ExibirTtulosVinculados1: TMenuItem;
    cxPageControl2: TcxPageControl;
    tabContato: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    cxPageControl3: TcxPageControl;
    tabHistContato: TcxTabSheet;
    DBGridEh4: TDBGridEh;
    cxPageControl4: TcxPageControl;
    tabPagamento: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    cxPageControl5: TcxPageControl;
    tabTituloPendPagto: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    CheckBox1: TCheckBox;
    qryContatoscTipoInclusao: TStringField;
    procedure ExibeDados(nCdTerceiro:integer);
    procedure btDetalheCompraClick(Sender: TObject);
    procedure qryTituloAbertoAfterScroll(DataSet: TDataSet);
    procedure cxButton3Click(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure btExibeClienteClick(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure RegistraContato(cFlgForcaContato: boolean);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure DBGridEh4DblClick(Sender: TObject);
    procedure ExibirTtulosVinculados1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    objfrmCobranca_VisaoContato_Contato : TfrmCobranca_VisaoContato_Contato;
  public
    { Public declarations }
    nCdItemListaCobranca : integer ;
    nCdListaCobranca     : integer ;
    cContatoPendente     : boolean ;
  end;

var
  frmCobranca_VisaoContato: TfrmCobranca_VisaoContato;

implementation

uses fMenu, fConsultaVendaProduto_Dados, fCobranca_VisaoContato_MovTit,
  fClienteVarejoPessoaFisica, fTerceiro,
  fCobranca_VisaoContato_Titulos, fTitulo, fGeraPropostaRenegociacao;

{$R *.dfm}

{ TfrmCobranca_VisaoContato }

procedure TfrmCobranca_VisaoContato.ExibeDados(nCdTerceiro: integer);
begin

    qryTerceiro.Close;
    PosicionaQuery(qryTerceiro, intToStr(nCdTerceiro)) ;

    qryPreparaTemp.ExecSQL;
    
    qryContatoTerceiro.Close;
    PosicionaQuery(qryContatoTerceiro, intToStr(nCdTerceiro)) ;

    qryTituloAberto.Close;
    qryTituloAberto.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
    qryTituloAberto.Parameters.ParamByName('cTituloVencer').Value := 0 ;

    if (CheckBox1.Checked) then
        qryTituloAberto.Parameters.ParamByName('cTituloVencer').Value := 1 ;

    qryTituloAberto.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva ;
    PosicionaQuery(qryTituloAberto, intToStr(nCdTerceiro)) ;

    DBGridEh3.TitleFont.Size := 8;
    DBGridEh3.Font.Size      := 8;
    cxPageControl1.ActivePageIndex := 0 ;

    qryContatos.Close;
    qryPagamentos.Close;

    Self.ShowModal;

end;

procedure TfrmCobranca_VisaoContato.btDetalheCompraClick(Sender: TObject);
var
  objForm : TfrmConsultaVendaProduto_Dados;
begin
  if (qryTituloAberto.Active) and (qryTituloAbertonCdLanctoFin.Value > 0) then
  begin

      objForm := TfrmConsultaVendaProduto_Dados.Create(nil);

      PosicionaQuery(objForm.qryItemPedido, qryTituloAbertonCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryCondicao  , qryTituloAbertonCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryPrestacoes, qryTituloAbertonCdLanctoFin.AsString) ;
      PosicionaQuery(objForm.qryCheques   , qryTituloAbertonCdLanctoFin.AsString) ;
      showForm(objForm,true);
  end ;

end;

procedure TfrmCobranca_VisaoContato.qryTituloAbertoAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  btDetalheCompra.Enabled := (qryTituloAbertonCdLanctoFin.Value > 0) ;
  
end;

procedure TfrmCobranca_VisaoContato.cxButton3Click(Sender: TObject);
var
  objForm : TfrmCobranca_VisaoContato_MovTit;
begin
  inherited;

  if (qryTituloAberto.Active) then
  begin

      objForm := TfrmCobranca_VisaoContato_MovTit.Create(nil);

      objForm.qryMovimentos.Close ;
      PosicionaQuery(objForm.qryMovimentos, qryTituloAbertonCdTitulo.asString) ;

      if objForm.qryMovimentos.eof then
      begin
          ShowMessage('Nenhum movimento para este t�tulo.') ;
          exit ;
      end ;

      showForm(objForm,true);

  end ;

end;

procedure TfrmCobranca_VisaoContato.CheckBox1Click(Sender: TObject);
begin
  inherited;

    qryTituloAberto.Close;
    qryTituloAberto.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
    qryTituloAberto.Parameters.ParamByName('cTituloVencer').Value := 0 ;

    if (CheckBox1.Checked) then
        qryTituloAberto.Parameters.ParamByName('cTituloVencer').Value := 1 ;

    qryTituloAberto.Open;

end;

procedure TfrmCobranca_VisaoContato.btExibeClienteClick(Sender: TObject);
var
  objForm  : TfrmClienteVarejoPessoaFisica;
  objForm2 : TfrmTerceiro;
begin
  inherited;

  if (qryTerceirocFlgPessoaFisica.Value = 'S') then
  begin

    objForm := TfrmClienteVarejoPessoaFisica.Create(nil);

    objForm.PosicionaQuery(objForm.qryMaster, qryTerceironCdTerceiro.asString);
    objForm.btIncluir.Visible   := False ;
    objForm.btExcluir.Visible   := False ;
    objForm.ToolButton1.Visible := False ;
    objForm.btCancelar.Visible  := False ;
    objForm.btConsultar.Visible := False ;
    objForm.bChamadaExterna     := True ;

    showForm(objForm,true);
  end
  else
  begin

      objForm2 := TfrmTerceiro.Create(nil);

      objForm2.PosicionaQuery(objForm2.qryMaster, qryTerceironCdTerceiro.asString);
      showForm(objForm2,true);
  end ;

end;

procedure TfrmCobranca_VisaoContato.cxButton4Click(Sender: TObject);
begin
  inherited;

  if (trim(edtDtFimPagto.Text) = '/  /') then
      edtDtFimPagto.Text := dateToStr(Date) ;

  if (trim(edtDtIniPagto.Text) = '/  /') then
      edtDtIniPagto.Text := dateToStr(Date-30) ;

  qryPagamentos.Close;
  qryPagamentos.Parameters.ParamByName('dDtInicial').Value := frmMenu.ConvData(edtDtIniPagto.Text) ;
  qryPagamentos.Parameters.ParamByName('dDtFinal').Value   := frmMenu.ConvData(edtDtFimPagto.Text) ;
  qryPagamentos.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  PosicionaQuery(qryPagamentos, qryTerceironCdTerceiro.AsString) ;

  if qryPagamentos.Eof then
      ShowMessage('Nenhum pagamento registrado para o per�odo selecionado.') ;

end;

procedure TfrmCobranca_VisaoContato.cxButton5Click(Sender: TObject);
begin
  inherited;
  if (trim(edtDtFimContato.Text) = '/  /') then
      edtDtFimContato.Text := dateToStr(Date) ;

  if (trim(edtDtIniContato.Text) = '/  /') then
      edtDtIniContato.Text := dateToStr(Date-30) ;

  qryContatos.Close;
  qryContatos.Parameters.ParamByName('dDtInicial').Value := frmMenu.ConvData(edtDtIniContato.Text) ;
  qryContatos.Parameters.ParamByName('dDtFinal').Value   := frmMenu.ConvData(edtDtFimContato.Text) ;
  PosicionaQuery(qryContatos, qryTerceironCdTerceiro.asString) ;

  if qryContatos.Eof then
      ShowMessage('Nenhum contato registrado para o per�odo selecionado.') ;

end;

procedure TfrmCobranca_VisaoContato.RegistraContato(cFlgForcaContato: boolean);
begin

    objfrmCobranca_VisaoContato_Contato := TfrmCobranca_VisaoContato_Contato.Create(Self);

    objfrmCobranca_VisaoContato_Contato.cFlgForcaContato     := cFlgForcaContato;
    objfrmCobranca_VisaoContato_Contato.nCdItemListaCobranca := nCdItemListaCobranca ;
    
    PosicionaQuery(objfrmCobranca_VisaoContato_Contato.qryContatoCobranca,'0');
    objfrmCobranca_VisaoContato_Contato.NovoContato(qryTerceironCdTerceiro.Value);

end;

procedure TfrmCobranca_VisaoContato.cxButton6Click(Sender: TObject);
begin
  inherited;

  registraContato(false) ;
  
end;

procedure TfrmCobranca_VisaoContato.cxButton7Click(Sender: TObject);
begin
  inherited;

  registraContato(false) ;

  if (objfrmCobranca_VisaoContato_Contato.cGravacaoEfetuada) and (cContatoPendente) then
  begin

      if (MessageDlg('Deseja remover este cliente da sua lista de cobran�a ?',mtConfirmation,[mbYes,mbNo],0)=MrYes) then
      begin
          try

              qryAux.SQL.Clear ;
              {-- Atualiza a quantidade de contatos da lista --}
              if (objfrmCobranca_VisaoContato_Contato.cClienteContatado) then
                  qryAux.SQL.Add('UPDATE ListaCobranca SET iQtdeClienteContato = iQtdeClienteContato + 1 WHERE nCdListaCobranca = ' + intToStr(nCdListaCobranca))
              else
                  qryAux.SQL.Add('UPDATE ListaCobranca SET iQtdeSemContato = iQtdeSemContato + 1 WHERE nCdListaCobranca = ' + intToStr(nCdListaCobranca)) ;

              qryAux.ExecSQL;

              {-- Caso a lista n�o tenha mais clientes para cobrar, encerra a lista --}
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('UPDATE ListaCobranca SET dDtEncerramento = GetDate() WHERE (iQtdeClienteTotal - iQtdeClienteContato - iQtdeSemContato) <= 0 AND nCdListaCobranca = ' + intToStr(nCdListaCobranca)) ;
              qryAux.ExecSQL;

              {-- Encerra o contato da lista --}
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('UPDATE ItemListaCobranca SET dDtEncerramento = GetDate() WHERE nCdItemListaCobranca = ' + intToStr(nCdItemListaCobranca)) ;
              qryAux.ExecSQL;

          except
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          close ;

      end ;

  end ;

  FreeAndNil(objfrmCobranca_VisaoContato_Contato);

end;

procedure TfrmCobranca_VisaoContato.DBGridEh4DblClick(Sender: TObject);
var
  objForm : TfrmCobranca_VisaoContato_Contato;
begin
  inherited;

  objForm := TfrmCobranca_VisaoContato_Contato.Create(Self);
  if qryContatos.Active then
  begin
      if (qryContatoscFlgCarta.Value = 1) then
          MensagemErro('N�o � permitida altera��o de registro de envio de carta.')
      else objForm.exibeContato(qryContatosnCdContatoCobranca.Value);
  end ;

end;

procedure TfrmCobranca_VisaoContato.ExibirTtulosVinculados1Click(
  Sender: TObject);
var
  objForm : TfrmCobranca_VisaoContato_Titulos;
begin
  inherited;

  if qryContatos.Active then
  begin
      objForm := TfrmCobranca_VisaoContato_Titulos.Create(Self);
      objForm.ExibeTitulos(qryContatosnCdItemListaCobranca.Value);
  end;
      
end;

procedure TfrmCobranca_VisaoContato.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmTitulo;
begin
  inherited;

  if (qryTituloAberto.Active) then
  begin

      objForm := TfrmTitulo.Create(nil);

      objForm.PosicionaQuery(objForm.qryMaster,qryTituloAbertonCdTitulo.AsString);

      objForm.btIncluir.Visible   := False ;
      objForm.btSalvar.Visible    := False ;
      objForm.btConsultar.Visible := False ;
      objForm.btExcluir.Visible   := False ;
      objForm.ToolButton1.Visible := False ;
      objForm.btCancelar.Visible  := False ;
      
      showForm(objForm,true);
  end ;

end;

procedure TfrmCobranca_VisaoContato.cxButton2Click(Sender: TObject);
var
  objForm : TfrmGeraPropostaRenegociacao;
begin
  inherited;

  objForm := TfrmGeraPropostaRenegociacao.Create(nil);

  objForm.WindowState := wsMaximized ;
  objForm.ChamadaListaCobranca(qryTerceironCdTerceiro.Value, nCdItemListaCobranca);
  Close;

end;

procedure TfrmCobranca_VisaoContato.FormShow(Sender: TObject);
begin
  inherited;
  qryTituloAberto.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva ;

end;

end.
