inherited frmTipoServico: TfrmTipoServico
  Left = 44
  Top = 109
  Caption = 'Tipo de Servi'#231'o'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 75
    Top = 45
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 64
    Top = 69
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 8
    Top = 93
    Width = 105
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Detalhada'
    FocusControl = DBEdit3
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 119
    Top = 39
    Width = 65
    Height = 19
    DataField = 'nCdTipoServico'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 119
    Top = 63
    Width = 650
    Height = 19
    DataField = 'cNmTipoServico'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [7]
    Left = 119
    Top = 87
    Width = 650
    Height = 19
    DataField = 'cDescricaoDetalhada'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBCheckBox1: TDBCheckBox [8]
    Left = 119
    Top = 112
    Width = 113
    Height = 17
    Caption = 'Servi'#231'o Tomado'
    DataField = 'cFlgServicoTomado'
    DataSource = dsMaster
    TabOrder = 4
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox2: TDBCheckBox [9]
    Left = 256
    Top = 112
    Width = 121
    Height = 17
    Caption = 'Servi'#231'o Prestado'
    DataField = 'cFlgServicoPrestado'
    DataSource = dsMaster
    TabOrder = 5
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox3: TDBCheckBox [10]
    Left = 401
    Top = 112
    Width = 193
    Height = 17
    Caption = 'Ativar reten'#231#227'o de impostos'
    DataField = 'cFlgRetencaoAtiva'
    DataSource = dsMaster
    TabOrder = 6
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object cxPageControl1: TcxPageControl [11]
    Left = 24
    Top = 155
    Width = 1025
    Height = 337
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 7
    ClientRectBottom = 333
    ClientRectLeft = 4
    ClientRectRight = 1021
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Dados dos Impostos'
      ImageIndex = 0
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 1017
        Height = 309
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTipoServicoTipoImposto
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnColExit = DBGridEh2ColExit
        OnEnter = DBGridEh2Enter
        OnKeyUp = DBGridEh2KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoPessoa'
            Footers = <>
            Title.Caption = 'Tipo de Pessoa|C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoPessoa'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Tipo de Pessoa|Nome'
            Width = 223
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoImposto'
            Footers = <>
            Title.Caption = 'Tipo de Imposto|C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoImposto'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Tipo de Imposto|Descri'#231#227'o'
            Width = 180
          end
          item
            EditButtons = <>
            FieldName = 'nPercBaseCalc'
            Footers = <>
            Title.Caption = 'F'#243'rmula de C'#225'lculo| % Base de C'#225'lculo'
          end
          item
            EditButtons = <>
            FieldName = 'nPercAliq'
            Footers = <>
            Title.Caption = 'F'#243'rmula de C'#225'lculo| % Al'#237'quota'
          end
          item
            EditButtons = <>
            FieldName = 'cFlgProtegerBase'
            Footers = <>
            KeyList.Strings = (
              '0'
              '1')
            PickList.Strings = (
              'SIM'
              'N'#195'O')
            Title.Caption = 'F'#243'rmula de C'#225'lculo|Protege Base?'
          end
          item
            EditButtons = <>
            FieldName = 'cCdCodRecolhimento'
            Footers = <>
            Title.Caption = 'C'#243'd. Recolhimento'
            Width = 93
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Munic'#237'pio X C'#243'digo de Servi'#231'o'
      ImageIndex = 1
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1017
        Height = 309
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTipoServicoTipoMunicipio
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdMunicipio'
            Footers = <>
            Title.Caption = 'Munic'#237'pio|C'#243'digo'
          end
          item
            EditButtons = <>
            FieldName = 'cNmMunicipio'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Munic'#237'pio|Descri'#231#227'o'
            Width = 286
          end
          item
            EditButtons = <>
            FieldName = 'cCdServMunicipal'
            Footers = <>
            Title.Caption = 'C'#243'd. Servi'#231'o Municipal'
            Width = 147
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '    FROM  TipoServico'
      'WHERE nCdTipoServico = :nPk')
    object qryMasternCdTipoServico: TIntegerField
      FieldName = 'nCdTipoServico'
    end
    object qryMastercNmTipoServico: TStringField
      FieldName = 'cNmTipoServico'
      Size = 50
    end
    object qryMastercDescricaoDetalhada: TStringField
      FieldName = 'cDescricaoDetalhada'
      Size = 100
    end
    object qryMastercFlgServicoTomado: TIntegerField
      FieldName = 'cFlgServicoTomado'
    end
    object qryMastercFlgServicoPrestado: TIntegerField
      FieldName = 'cFlgServicoPrestado'
    end
    object qryMastercFlgRetencaoAtiva: TIntegerField
      FieldName = 'cFlgRetencaoAtiva'
    end
  end
  object qryTipoServicoTipoImposto: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTipoServicoTipoImpostoBeforePost
    OnCalcFields = qryTipoServicoTipoImpostoCalcFields
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '   FROM TipoServicoTipoImposto'
      'WHERE nCdTipoServico = :nPk')
    Left = 872
    Top = 160
    object qryTipoServicoTipoImpostonCdTipoServicoTipoImposto: TIntegerField
      FieldName = 'nCdTipoServicoTipoImposto'
    end
    object qryTipoServicoTipoImpostonCdTabTipoPessoa: TIntegerField
      FieldName = 'nCdTabTipoPessoa'
    end
    object qryTipoServicoTipoImpostonCdTipoImposto: TIntegerField
      FieldName = 'nCdTipoImposto'
    end
    object qryTipoServicoTipoImpostonPercBaseCalc: TBCDField
      FieldName = 'nPercBaseCalc'
      Precision = 12
      Size = 2
    end
    object qryTipoServicoTipoImpostonPercAliq: TBCDField
      FieldName = 'nPercAliq'
      Precision = 12
      Size = 2
    end
    object qryTipoServicoTipoImpostocFlgProtegerBase: TIntegerField
      FieldName = 'cFlgProtegerBase'
    end
    object qryTipoServicoTipoImpostocCdCodRecolhimento: TStringField
      FieldName = 'cCdCodRecolhimento'
    end
    object qryTipoServicoTipoImpostocNmTipoImposto: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmTipoImposto'
      Size = 50
      Calculated = True
    end
    object qryTipoServicoTipoImpostocNmTipoPessoa: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmTipoPessoa'
      Size = 50
      Calculated = True
    end
    object qryTipoServicoTipoImpostonCdTipoServico: TIntegerField
      FieldName = 'nCdTipoServico'
    end
  end
  object qryTipoServicoMunicipio: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTipoServicoMunicipioBeforePost
    OnCalcFields = qryTipoServicoMunicipioCalcFields
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM TipoServicoMunicipio'
      'WHERE nCdTipoServico = :nPk')
    Left = 872
    Top = 200
    object qryTipoServicoMunicipionCdTipoServicoMunicipio: TIntegerField
      FieldName = 'nCdTipoServicoMunicipio'
    end
    object qryTipoServicoMunicipionCdTipoServico: TIntegerField
      FieldName = 'nCdTipoServico'
    end
    object qryTipoServicoMunicipionCdMunicipio: TIntegerField
      FieldName = 'nCdMunicipio'
    end
    object qryTipoServicoMunicipiocCdServMunicipal: TStringField
      FieldName = 'cCdServMunicipal'
    end
    object qryTipoServicoMunicipiocNmMunicipio: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmMunicipio'
      Size = 50
      Calculated = True
    end
  end
  object dsTipoServicoTipoImposto: TDataSource
    DataSet = qryTipoServicoTipoImposto
    Left = 904
    Top = 160
  end
  object qryTipoImposto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoImposto'
      '              ,cNmTipoImposto'
      ' FROM TipoImposto'
      'WHERE nCdTipoImposto =:nPk')
    Left = 944
    Top = 160
    object qryTipoImpostonCdTipoImposto: TIntegerField
      FieldName = 'nCdTipoImposto'
    end
    object qryTipoImpostocNmTipoImposto: TStringField
      FieldName = 'cNmTipoImposto'
      Size = 50
    end
  end
  object qryTipoPessoa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabTipoPessoa'
      '             ,cNmTabTipoPessoa '
      'FROM TabTipoPessoa'
      'WHERE nCdTabTipoPessoa = :nPk')
    Left = 984
    Top = 160
    object qryTipoPessoanCdTabTipoPessoa: TIntegerField
      FieldName = 'nCdTabTipoPessoa'
    end
    object qryTipoPessoacNmTabTipoPessoa: TStringField
      FieldName = 'cNmTabTipoPessoa'
      Size = 50
    end
  end
  object dsAbatimento: TDataSource
    DataSet = frmMenu.qryTabSimNao
    Left = 872
    Top = 520
  end
  object qryMunicipio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      ' FROM Municipio'
      'WHERE nCdMunicipio = :nPk')
    Left = 944
    Top = 200
    object qryMunicipionCdMunicipio: TIntegerField
      FieldName = 'nCdMunicipio'
    end
    object qryMunicipionCdEstado: TIntegerField
      FieldName = 'nCdEstado'
    end
    object qryMunicipiocNmMunicipio: TStringField
      FieldName = 'cNmMunicipio'
      Size = 50
    end
    object qryMunicipionCdMunicipioIBGE: TIntegerField
      FieldName = 'nCdMunicipioIBGE'
    end
  end
  object dsTipoServicoTipoMunicipio: TDataSource
    DataSet = qryTipoServicoMunicipio
    Left = 904
    Top = 202
  end
end
