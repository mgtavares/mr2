unit fEncerrarMovtoPOS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  cxLookAndFeelPainters, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ADODB, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGrid, cxButtons, DBCtrls, Mask;

type
  TfrmEncerrarMovtoPOS = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    MaskEdit6: TMaskEdit;
    Label5: TLabel;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    qryGrupoOperadora: TADOQuery;
    qryGrupoOperadoranCdGrupoOperadoraCartao: TIntegerField;
    qryGrupoOperadoracNmGrupoOperadoraCartao: TStringField;
    DBEdit1: TDBEdit;
    dsGrupoOperadora: TDataSource;
    cxButton1: TcxButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryTransacaoCartao: TADOQuery;
    qryTransacaoCartaonCdTransacaoCartao: TAutoIncField;
    qryTransacaoCartaodDtTransacao: TDateTimeField;
    qryTransacaoCartaoiNrCartao: TLargeintField;
    qryTransacaoCartaoiNrDocto: TIntegerField;
    qryTransacaoCartaonCdOperadoraCartao: TIntegerField;
    qryTransacaoCartaocNmOperadoraCartao: TStringField;
    qryTransacaoCartaoiParcelas: TIntegerField;
    qryTransacaoCartaonValTransacao: TBCDField;
    qryTransacaoCartaonValLiquido: TBCDField;
    qryTransacaoCartaonValTaxaOperadora: TBCDField;
    qryTransacaoCartaonCdStatusDocto: TIntegerField;
    qryTransacaoCartaocNmStatusDocto: TStringField;
    cmdPreparaTemp: TADOCommand;
    qryTransacaoPendente: TADOQuery;
    qryTransacaoSelecionada: TADOQuery;
    qryTransacaoPendentenCdTransacaoCartao: TIntegerField;
    qryTransacaoPendentedDtTransacao: TDateTimeField;
    qryTransacaoPendenteiNrCartao: TLargeintField;
    qryTransacaoPendenteiNrDocto: TIntegerField;
    qryTransacaoPendentenCdOperadoraCartao: TIntegerField;
    qryTransacaoPendentecNmOperadoraCartao: TStringField;
    qryTransacaoPendenteiParcelas: TIntegerField;
    qryTransacaoPendentenValTransacao: TBCDField;
    qryTransacaoPendentenValLiquido: TBCDField;
    qryTransacaoPendentenValTaxaOperadora: TBCDField;
    qryTransacaoPendentenCdStatusDocto: TIntegerField;
    qryTransacaoPendentecNmStatusDocto: TStringField;
    qryTransacaoPendentecFlgSelecionado: TIntegerField;
    qryTransacaoSelecionadanCdTransacaoCartao: TIntegerField;
    qryTransacaoSelecionadadDtTransacao: TDateTimeField;
    qryTransacaoSelecionadaiNrCartao: TLargeintField;
    qryTransacaoSelecionadaiNrDocto: TIntegerField;
    qryTransacaoSelecionadanCdOperadoraCartao: TIntegerField;
    qryTransacaoSelecionadacNmOperadoraCartao: TStringField;
    qryTransacaoSelecionadaiParcelas: TIntegerField;
    qryTransacaoSelecionadanValTransacao: TBCDField;
    qryTransacaoSelecionadanValLiquido: TBCDField;
    qryTransacaoSelecionadanValTaxaOperadora: TBCDField;
    qryTransacaoSelecionadanCdStatusDocto: TIntegerField;
    qryTransacaoSelecionadacNmStatusDocto: TStringField;
    qryTransacaoSelecionadacFlgSelecionado: TIntegerField;
    dsTransacaoPendente: TDataSource;
    dsTransacaoSelecionada: TDataSource;
    cxGrid1DBTableView1nCdTransacaoCartao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtTransacao: TcxGridDBColumn;
    cxGrid1DBTableView1iNrCartao: TcxGridDBColumn;
    cxGrid1DBTableView1iNrDocto: TcxGridDBColumn;
    cxGrid1DBTableView1nCdOperadoraCartao: TcxGridDBColumn;
    cxGrid1DBTableView1cNmOperadoraCartao: TcxGridDBColumn;
    cxGrid1DBTableView1iParcelas: TcxGridDBColumn;
    cxGrid1DBTableView1nValTransacao: TcxGridDBColumn;
    cxGrid1DBTableView1nValLiquido: TcxGridDBColumn;
    cxGrid1DBTableView1nValTaxaOperadora: TcxGridDBColumn;
    cxGrid1DBTableView1nCdStatusDocto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmStatusDocto: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgSelecionado: TcxGridDBColumn;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    cxGridDBTableView1nCdTransacaoCartao: TcxGridDBColumn;
    cxGridDBTableView1dDtTransacao: TcxGridDBColumn;
    cxGridDBTableView1iNrCartao: TcxGridDBColumn;
    cxGridDBTableView1iNrDocto: TcxGridDBColumn;
    cxGridDBTableView1nCdOperadoraCartao: TcxGridDBColumn;
    cxGridDBTableView1cNmOperadoraCartao: TcxGridDBColumn;
    cxGridDBTableView1iParcelas: TcxGridDBColumn;
    cxGridDBTableView1nValTransacao: TcxGridDBColumn;
    cxGridDBTableView1nValLiquido: TcxGridDBColumn;
    cxGridDBTableView1nValTaxaOperadora: TcxGridDBColumn;
    cxGridDBTableView1nCdStatusDocto: TcxGridDBColumn;
    cxGridDBTableView1cNmStatusDocto: TcxGridDBColumn;
    cxGridDBTableView1cFlgSelecionado: TcxGridDBColumn;
    qryAux: TADOQuery;
    qryGrupoOperadoracFlgDataCredFechPOS: TIntegerField;
    qryGrupoOperadoracFlgTotalLiqFechPOS: TIntegerField;
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure MaskEdit6Change(Sender: TObject);
    procedure MaskEdit1Change(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEncerrarMovtoPOS: TfrmEncerrarMovtoPOS;

implementation

uses fLookup_Padrao, fMenu, fEncerrarMovtoPOS_GeraLote;

{$R *.dfm}

procedure TfrmEncerrarMovtoPOS.MaskEdit6Exit(Sender: TObject);
begin
  inherited;

  qryGrupoOperadora.Close ;
  PosicionaQuery(qryGrupoOperadora, MaskEdit6.Text) ;
  
end;

procedure TfrmEncerrarMovtoPOS.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(172);

        If (nPK > 0) then
        begin
            MaskEdit6.Text := intToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmEncerrarMovtoPOS.cxButton1Click(Sender: TObject);
begin
  inherited;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione o grupo de operadora.') ;
      MaskEdit6.SetFocus ;
      exit ;
  end ;

  if (Trim(MaskEdit1.Text) = '/  /') then
  begin
      MensagemAlerta('Informa a data da movimenta��o.') ;
      MaskEdit1.SetFocus ;
      exit ;
  end ;

  cmdPreparaTemp.Execute;

  qryTransacaoCartao.Close ;
  qryTransacaoCartao.Parameters.ParamByName('nCdLoja').Value                 := frmMenu.nCdLojaAtiva ;
  qryTransacaoCartao.Parameters.ParamByName('nCdGrupoOperadoraCartao').Value := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  qryTransacaoCartao.Parameters.ParamByName('dDtTransacao').Value            := MaskEdit1.Text ;
  qryTransacaoCartao.ExecSQL ;

  qryTransacaoPendente.Close ;
  qryTransacaoPendente.Open ;

  qryTransacaoSelecionada.Close ;

  if (qryTransacaoPendente.eof) then
  begin
      ShowMessage('Nenhum comprovante pendente de confer�ncia.') ;
      MaskEdit6.SetFocus ;
      exit ;
  end ;

  qryTransacaoPendente.First ;

  if (qryTransacaoPendentedDtTransacao.Value < frmMenu.ConvData(MaskEdit1.Text)) then
  begin
      MensagemAlerta('Existe comprovante pendente de confer�ncia no dia ' + qryTransacaoPendentedDtTransacao.AsString + #13#13 + 'Para continuar, � necess�rio conferir este dia primeiro.') ;
      qryTransacaoCartao.Close ;
      qryTransacaoPendente.Close ;
      MaskEdit1.SetFocus ;
      exit ;
  end ;

  qryTransacaoSelecionada.Close ;
  qryTransacaoSelecionada.Open ;

end;

procedure TfrmEncerrarMovtoPOS.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE #TempTransacaoCartao Set cFlgSelecionado = 1 WHERE nCdTransacaoCartao = ' + qryTransacaoPendentenCdTransacaoCartao.AsString) ;
  qryAux.ExecSQL ;

  qryTransacaoPendente.Requery() ;
  qryTransacaoSelecionada.Requery();
end;

procedure TfrmEncerrarMovtoPOS.cxGridDBTableView1DblClick(Sender: TObject);
begin
  inherited;
  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE #TempTransacaoCartao Set cFlgSelecionado = 0 WHERE nCdTransacaoCartao = ' + qryTransacaoSelecionadanCdTransacaoCartao.AsString) ;
  qryAux.ExecSQL ;

  qryTransacaoPendente.Requery() ;
  qryTransacaoSelecionada.Requery();  

end;

procedure TfrmEncerrarMovtoPOS.MaskEdit6Change(Sender: TObject);
begin
  inherited;

  if (qryTransacaoPendente.Active) then
      qryTransacaoPendente.Close ;

  if (qryTransacaoSelecionada.Active) then
      qryTransacaoSelecionada.Close ;

end;

procedure TfrmEncerrarMovtoPOS.MaskEdit1Change(Sender: TObject);
begin
  inherited;

  if (qryTransacaoPendente.Active) then
      qryTransacaoPendente.Close ;

  if (qryTransacaoSelecionada.Active) then
      qryTransacaoSelecionada.Close ;

end;

procedure TfrmEncerrarMovtoPOS.ToolButton1Click(Sender: TObject);
var
    nTotalBrutoSelecionado : double ;
    objForm : TfrmEncerrarMovtoPOS_GeraLote;
begin
  inherited;

  if (qryTransacaoSelecionada.eof) then
  begin
      MensagemAlerta('Nenhum comprovante selecionado para gera��o do lote.') ;
      exit ;
  end ;

  objForm := TfrmEncerrarMovtoPOS_GeraLote.Create(nil);

  objForm.cmdPreparaTemp.Execute;

  objForm.qryTempResumo.Close ;
  objForm.qryTempResumo.Parameters.ParamByName('cGerarParcelas').Value := qryGrupoOperadoracFlgDataCredFechPOS.AsString ;
  objForm.qryTempResumo.Open ;

  objForm.DBGridEh2.Visible := True ;

  if (qryGrupoOperadoracFlgDataCredFechPOS.Value = 0) then
      objForm.DBGridEh2.Visible := False ;


  if (qryGrupoOperadoracFlgTotalLiqFechPOS.Value = 1) then
      objForm.DBGridEh2.Visible := True ;

  objForm.cFlgTotalLiqFechPOS := qryGrupoOperadoracFlgTotalLiqFechPOS.Value ;
  objForm.cFlgDataCredFechPOS := qryGrupoOperadoracFlgDataCredFechPOS.Value ;
  objForm.dDtTransacao        := frmMenu.ConvData(MaskEdit1.Text) ;

  nTotalBrutoSelecionado := 0 ;

  qryTransacaoSelecionada.DisableControls;

  qryTransacaoSelecionada.First ;

  while not qryTransacaoSelecionada.eof do
  begin
      nTotalBrutoSelecionado := nTotalBrutoSelecionado + qryTransacaoSelecionadanValTransacao.Value ;
      qryTransacaoSelecionada.Next ;
  end ;

  qryTransacaoSelecionada.First ;

  qryTransacaoSelecionada.EnableControls ;

  objForm.nTotalBrutoSelecionado := nTotalBrutoSelecionado ;
  showForm(objForm,true);

  if (qryTransacaoPendente.Active) then
      qryTransacaoPendente.Close ;

  if (qryTransacaoSelecionada.Active) then
      qryTransacaoSelecionada.Close ;

  MaskEdit6.SetFocus ;

end;

initialization
    RegisterClass(TfrmEncerrarMovtoPOS) ;

end.
