inherited frmRegiao: TfrmRegiao
  Caption = 'Regiao'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 39
    Top = 46
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 70
    Width = 69
    Height = 13
    Caption = 'Nome Regi'#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 80
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdRegiao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 80
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmRegiao'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM REGIAO'
      'WHERE nCdRegiao = :nPK')
    object qryMasternCdRegiao: TIntegerField
      FieldName = 'nCdRegiao'
    end
    object qryMastercNmRegiao: TStringField
      FieldName = 'cNmRegiao'
      Size = 50
    end
  end
end
