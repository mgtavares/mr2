unit fFeedback;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ClipBrd, jpeg, DB, ADODB,
  Mask, DBCtrls;

type
  TfrmFeedback = class(TfrmProcesso_Padrao)
    OpenDialog1: TOpenDialog;
    Label2: TLabel;
    mFeedback: TMemo;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    edtVersao: TEdit;
    Label8: TLabel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edtPrint: TEdit;
    cxButton1: TcxButton;
    ckPrint: TCheckBox;
    edtEmail: TEdit;
    edtUsuario: TEdit;
    edtEmpresa: TEdit;
    edtLoja: TEdit;
    edtLogin: TEdit;
    procedure cxButton1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SalvaPrint;
    procedure ValidaTexto();
  private
    { Private declarations }
  public
    { Public declarations }
    printFeedback : string;
  end;

var
  frmFeedback: TfrmFeedback;

implementation

{$R *.dfm}

uses
  fMenu, fEnviaEmailPadrao;

procedure TfrmFeedback.ValidaTexto();
var
  i : integer;
begin
  for i := 0 to ComponentCount -1 do
      begin
          {
              # Se a Tag do campo for 2, ele sera obrigat�rio
          }

          // Se for do tipo TEdit
          if (Components[I] is TEdit) then
          begin
              if (Components[I] as TEdit).Tag = 2 then
              begin
                  if trim((Components[I] as TEdit).Text) = '' then
                  begin
                      showMessage((Components[I] as TEdit).Hint);
                      (Components[I] as TEdit).SetFocus;
                      abort;
                  end;
              end;
          end;

          // Se for do tipo TMemo
          if (Components[I] is TMemo) then
          begin
              if (Components[I] as TMemo).Tag = 2 then
              begin
                  if trim((Components[I] as TMemo).Text) = '' then
                  begin
                      showMessage((Components[I] as TMemo).Hint);
                      (Components[I] as TMemo).SetFocus;
                      abort;
                  end;
              end;
          end;

          // Se for do tipo DBEdit
          if (Components[I] is TDBEdit) then
          begin
              if (Components[I] as TDBEdit).Tag = 2 then
              begin
                  if trim((Components[I] as TDBEdit).Text) = '' then
                  begin
                      showMessage((Components[I] as TDBEdit).Hint);
                      (Components[I] as TDBEdit).SetFocus;
                      abort;
                  end;
              end;
          end;

          // Se for do tipo DBMemo
          if (Components[I] is TDBMemo) then
          begin
              if (Components[I] as TDBMemo).Tag = 2 then
              begin
                  if trim((Components[I] as TDBMemo).Text) = '' then
                  begin
                      showMessage((Components[I] as TDBMemo).Hint);
                      (Components[I] as TDBMemo).SetFocus;
                      abort;
                  end;
              end;
          end;
      end;
end;

procedure TfrmFeedback.cxButton1Click(Sender: TObject);
begin
  inherited;

  if OpenDialog1.Execute then
      edtPrint.Text := OpenDialog1.FileName;

end;

procedure TfrmFeedback.ToolButton1Click(Sender: TObject);
var
  objEnviaEmail : TfrmEnviaEmailPadrao;
  arrAnexos     : array[0..2] of String;
  cMensagem     : WideString;
begin
  inherited;

  // Valida os campos obrigatorios definidos pela TAG 2
  ValidaTexto();

  // Adiciona o Print automatico da tela.
  if (ckPrint.Checked) then
      arrAnexos[0] := 'C:\Temp\ER2Print.jpg';
  // Valida e-mail
  if  not frmMenu.fnValidarEMail(edtEmail.Text) then
      Begin
          frmMenu.MensagemAlerta('E-mail incorreto.');
          edtEmail.SetFocus;
          abort;
      end;

   if trim(edtPrint.Text) <> '' then
      arrAnexos[1] := edtPrint.Text;

   objEnviaEmail := TfrmEnviaEmailPadrao.Create(Nil);

   cMensagem := 'Feedback: ' + mFeedback.Text
              + #13
              + '------------------------------------------------------------'
              + #13#13
              + '********** Dados do Cliente **********'
              + #13
              + 'Empresa: '+ frmMenu.cNmEmpresaAtiva
              + #13
              + 'Loja: ' + frmMenu.cNmLojaAtiva
              + #13
              + 'Login: ' + frmMenu.cNmUsuarioLogado
              + #13
              + 'Nome: ' + frmMenu.cNmCompletoUsuario
              + #13
              + 'E-mail: ' + edtEmail.Text
              + #13#13
              + '********** Dados do Sistema **********'
              + #13
              + 'Vers�o: ' + frmMenu.cNmVersaoSistema
              + #13
              + 'IP: ' + frmMenu.cIPServidor
              + #13
              + 'Base de Dados: ' + frmMenu.cInstancia;

   try
       objEnviaEmail.prEnviaEmail('mail.er2soft.com.br', 'feedback@er2soft.com.br', '##admsoftFeedback', 'feedback@er2soft.com.br', 'Feedback: ' + frmMenu.cNmEmpresaAtiva + ' - ' + frmMenu.cNmLojaAtiva, 587, cMensagem, arrAnexos);
       frmMenu.ShowMessage('Obrigado por enviar seu feedback, em breve entraremos em contato ;)');
       Close;
   finally
       FreeAndNil(objEnviaEmail);
   end;
end;

procedure TfrmFeedback.SalvaPrint;
var
  dc  : hdc;
  cv  : TCanvas;
  aux : TBitmap;
  jpg : TJPEGImage;
begin
    inherited;
    jpg         := TJPEGImage.Create;
    aux         := TBitmap.Create;
    aux.Height  := Screen.Height;
    aux.Width   := Screen.Width;
    dc          := GetDC(0);
    cv          := TCanvas.Create;
    cv.Handle   := dc;

    //--Define o tamanho da imagem
    aux.Canvas.CopyRect(Rect(0,0,Application.MainForm.Width,Application.MainForm.Height),cv,
    Rect(0,0,Application.MainForm.Width,Application.MainForm.Height));

    cv.Free;
    ReleaseDC(0,dc);
    //-- Compacta o BMP para JPEG
    jpg.Assign(aux);
    jpg.Compress;

  { -- diret�rios de arquivos tempor�rios -- }
  if (not DirectoryExists('C:\Temp\')) then
      CreateDir('C:\Temp\');

    // Salva Print na pasta temporario
    jpg.SaveToFile('C:\temp\ER2Print.jpg');
end;

procedure TfrmFeedback.FormShow(Sender: TObject);
begin
    inherited;
    SalvaPrint;
    edtUsuario.Text := frmMenu.cNmCompletoUsuario;
    edtEmpresa.Text := frmMenu.cNmEmpresaAtiva;
    edtLogin.Text   := frmMenu.cNmUsuarioLogado;
    edtLoja.Text    := frmMenu.cNmLojaAtiva;
    edtVersao.Text  := frmMenu.cNmVersaoSistema;
    edtEmail.Text   := frmMenu.cEmailUsuario;
end;

end.

