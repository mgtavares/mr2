unit fTipoAdiantamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, ImgList, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmTipoAdiantamento = class(TfrmCadastro_Padrao)
    qryMasternCdTipoAdiantamento: TIntegerField;
    qryMastercNmTipoAdiantamento: TStringField;
    qryMasternCdEspTitAdiant: TIntegerField;
    qryMasternCdPlanoContaSaida: TIntegerField;
    qryMasternCdPlanoContaEntrada: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label5: TLabel;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitnCdGrupoEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitcFlgPrev: TIntegerField;
    qryEspTitcFlgCobranca: TIntegerField;
    qryEspTitnCdServidorOrigem: TIntegerField;
    qryEspTitdDtReplicacao: TDateTimeField;
    qryEspTitcFlgInadimplencia: TIntegerField;
    qryEspTitcFlgAdiantamento: TIntegerField;
    qryPlanoConta: TADOQuery;
    qryPlanoContanCdPlanoConta: TIntegerField;
    qryPlanoContacNmPlanoConta: TStringField;
    qryPlanoContanCdGrupoPlanoConta: TIntegerField;
    qryPlanoContacFlgRecDes: TStringField;
    qryPlanoContacMascara: TStringField;
    qryPlanoContacFlgLanc: TIntegerField;
    qryPlanoContacSenso: TStringField;
    qryPlanoContanCdServidorOrigem: TIntegerField;
    qryPlanoContadDtReplicacao: TDateTimeField;
    qryPlanoContacFlgExibeDRE: TIntegerField;
    dsEspTit: TDataSource;
    dsPlanoConta: TDataSource;
    qryPlanoContaEntrada: TADOQuery;
    dsPlanoContaEntrada: TDataSource;
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6Exit(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoAdiantamento: TfrmTipoAdiantamento;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmTipoAdiantamento.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(10,'cFlgAdiantamento=1');

            If (nPK > 0) then
            begin
                qryMasternCdEspTitAdiant.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTipoAdiantamento.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryEspTit.Close ;
  PosicionaQuery(qryEspTit, DBEdit3.Text) ;

end;
  procedure TfrmTipoAdiantamento.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(41);

            If (nPK > 0) then qryMasternCdPlanoContaSaida.Value := nPK ;

        end ;

    end ;
  end;
end;

procedure TfrmTipoAdiantamento.DBEdit5Exit(Sender: TObject);
begin
  inherited;
  qryPlanoConta.Close ;
  PosicionaQuery(qryPlanoConta, DBEdit5.Text) ;

end;

procedure TfrmTipoAdiantamento.DBEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(41);
            If (nPK > 0) then qryMasternCdPlanoContaEntrada.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoAdiantamento.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  qryPlanoContaEntrada.Close ;
  PosicionaQuery(qryPlanoContaEntrada, DBEdit6.Text) ;

end;

procedure TfrmTipoAdiantamento.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;

end;

procedure TfrmTipoAdiantamento.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryEspTit, qryMasternCdEspTitAdiant.AsString) ;
  PosicionaQuery(qryPlanoConta, qryMasternCdPlanoContaSaida.AsString) ;
  PosicionaQuery(qryPlanoContaEntrada, qryMasternCdPlanoContaEntrada.AsString) ;
end;

procedure TfrmTipoAdiantamento.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOADIANTAMENTO' ;
  nCdTabelaSistema  := 210 ;
  nCdConsultaPadrao := 1002 ;

end;

procedure TfrmTipoAdiantamento.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryEspTit.Close ;
  qryPlanoConta.Close ;
  qryPlanoContaEntrada.Close ;

end;

procedure TfrmTipoAdiantamento.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryEspTit.Close ;
  qryPlanoConta.Close ;
  qryPlanoContaEntrada.Close ;

end;

procedure TfrmTipoAdiantamento.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a Descri��o do Tipo de Adiantamento.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (DBEdit3.Text = '') then
  begin
      MensagemAlerta('Informe a Esp�cie de T�tulo.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;
  if (DBEdit5.Text = '') then
  begin
      MensagemAlerta('Informe a Conta de Sa�da.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;
  if (DBEdit6.Text = '') then
  begin
      MensagemAlerta('Informe a Conta de Entrada.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdPlanoContaSaida.Value = qryMasternCdPlanoContaEntrada.Value) then
  begin
      MensagemAlerta('Conta de Entrada igual a conta de sa�da.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;
  inherited;

end;

initialization
    RegisterClass(tFrmTipoAdiantamento) ;

end.
