unit fConciliaLoteLiqCobradora;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping;

type
  TfrmConciliaLoteLiqCobradora = class(TfrmProcesso_Padrao)
    qryLoteLiqCobradora: TADOQuery;
    DBGridEh1: TDBGridEh;
    dsLoteLiqCobradora: TDataSource;
    qryLoteLiqCobradoranCdLoteLiqCobradora: TIntegerField;
    qryLoteLiqCobradoracNrBorderoCob: TStringField;
    qryLoteLiqCobradoradDtFecham: TDateTimeField;
    qryLoteLiqCobradoranValTotal: TBCDField;
    qryLoteLiqCobradoranCdCobradora: TIntegerField;
    qryLoteLiqCobradoracNmCobradora: TStringField;
    qryLoteLiqCobradoracNmUsuario: TStringField;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConciliaLoteLiqCobradora: TfrmConciliaLoteLiqCobradora;

implementation

uses fMenu, fConciliaLoteLiqCobradora_Efetiva;

{$R *.dfm}

procedure TfrmConciliaLoteLiqCobradora.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryLoteLiqCobradora.Close;
  qryLoteLiqCobradora.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
  qryLoteLiqCobradora.Parameters.ParamByName('nCdLojaCobradora').Value := frmMenu.nCdLojaAtiva ;
  qryLoteLiqCobradora.Open ;

end;

procedure TfrmConciliaLoteLiqCobradora.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient;

  ToolButton1.Click;


end;

procedure TfrmConciliaLoteLiqCobradora.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmConciliaLoteLiqCobradora_Efetiva ;
begin
  inherited;

  if (qryLoteLiqCobradora.RecordCount > 0) then
  begin

      objForm := TfrmConciliaLoteLiqCobradora_Efetiva.Create( Self ) ;

      PosicionaQuery(objForm.qryLoteLiqCobradora, qryLoteLiqCobradoranCdLoteLiqCobradora.asString) ;
      showForm( objForm , TRUE ) ;

      Toolbutton1.Click;

  end ;

end;

initialization
    RegisterClass(TfrmConciliaLoteLiqCobradora) ;

end.
