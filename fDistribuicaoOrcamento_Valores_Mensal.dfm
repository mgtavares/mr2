inherited frmDistribuicaoOrcamento_Valores_Mensal: TfrmDistribuicaoOrcamento_Valores_Mensal
  Left = 181
  Top = 122
  Height = 620
  Caption = 'Distribui'#231#227'o Mensal'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 129
    Height = 319
  end
  inherited ToolBar1: TToolBar
    ButtonWidth = 115
    inherited ToolButton1: TToolButton
      Caption = 'Salvar Distribui'#231#227'o'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 115
    end
    inherited ToolButton2: TToolButton
      Left = 123
    end
    object ToolButton4: TToolButton
      Left = 238
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 246
      Top = 0
      Caption = 'Atualizar Diferen'#231'a'
      ImageIndex = 1
      OnClick = ToolButton5Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 100
    Align = alTop
    Caption = 'Dados da Conta'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 8
      Top = 16
      Width = 29
      Height = 13
      Caption = 'Conta'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Tag = 1
      Left = 8
      Top = 56
      Width = 127
      Height = 13
      Caption = 'Or'#231'amento Total da Conta'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Tag = 1
      Left = 184
      Top = 56
      Width = 109
      Height = 13
      Caption = 'M'#233'todo de Distribui'#231#227'o'
      FocusControl = DBEdit3
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 8
      Top = 32
      Width = 617
      Height = 21
      DataField = 'cNmPlanoConta'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 8
      Top = 72
      Width = 173
      Height = 21
      DataField = 'nValorOrcamentoConta'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 184
      Top = 72
      Width = 105
      Height = 21
      DataField = 'cNmMetodoDistribOrcamento'
      DataSource = DataSource1
      TabOrder = 2
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 129
    Width = 784
    Height = 319
    ActivePage = cxTabSheet2
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 315
    ClientRectLeft = 4
    ClientRectRight = 780
    ClientRectTop = 24
    object cxTabSheet2: TcxTabSheet
      Caption = 'Distriui'#231#227'o Mensal'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 776
        Height = 291
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsItemOrcamentoMes
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItemOrcamentoMes'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdItemOrcamento'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoMes'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cNmTabTipoMes'
            Footers = <>
            ReadOnly = True
            Width = 121
          end
          item
            EditButtons = <>
            FieldName = 'nValOrcamento'
            Footers = <>
            Title.Alignment = taRightJustify
            Width = 95
          end
          item
            EditButtons = <>
            FieldName = 'nPercDistrib'
            Footers = <>
            ReadOnly = True
            Title.Alignment = taRightJustify
          end
          item
            EditButtons = <>
            FieldName = 'cOBS'
            Footers = <>
            Width = 431
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object cxPageControl2: TcxPageControl [4]
    Left = 0
    Top = 448
    Width = 784
    Height = 136
    ActivePage = cxTabSheet1
    Align = alBottom
    LookAndFeel.NativeStyle = True
    TabOrder = 3
    ClientRectBottom = 132
    ClientRectLeft = 4
    ClientRectRight = 780
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Observa'#231#227'o'
      ImageIndex = 0
      object DBMemo1: TDBMemo
        Left = 0
        Top = 0
        Width = 776
        Height = 108
        Align = alClient
        DataField = 'cOBSGeral'
        DataSource = DataSource2
        TabOrder = 0
      end
    end
  end
  object qryItemOrcamentoTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Item.nCdItemOrcamento'
      '      ,Item.nCdPlanoConta'
      
        '      ,SUBSTRING(RTRIM(LTRIM(IsNull(LTRIM(RTRIM(GrupoPlanoConta.' +
        'cMascaraGrupo)),'#39#39') + '#39'.'#39' + IsNull(PlanoConta.cMascara,'#39#39'))) + '#39 +
        ' - '#39' + PlanoConta.cNmPlanoConta,1,50) as cNmPlanoConta'
      '      ,MetodoDistribOrcamento.cNmMetodoDistribOrcamento'
      '      ,Item.nValorOrcamentoConta      '
      '  FROM ItemOrcamento Item'
      
        '       INNER JOIN PlanoConta                ON PlanoConta.nCdPla' +
        'noConta                         = Item.nCdPlanoConta'
      
        '       LEFT  JOIN PlanoConta PlanoContaBase ON PlanoContaBase.nC' +
        'dPlanoConta                     = Item.nCdPlanoContaBase'
      
        '       INNER JOIN GrupoPlanoConta           ON GrupoPlanoConta.n' +
        'CdGrupoPlanoConta               = PlanoConta.nCdGrupoPlanoConta'
      
        '       INNER JOIN MetodoDistribOrcamento    ON MetodoDistribOrca' +
        'mento.nCdMetodoDistribOrcamento = Item.nCdMetodoDistribOrcamento'
      ' WHERE Item.nCdItemOrcamento = :nPK')
    Left = 416
    Top = 120
    object qryItemOrcamentoTempnCdItemOrcamento: TIntegerField
      FieldName = 'nCdItemOrcamento'
    end
    object qryItemOrcamentoTempnCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryItemOrcamentoTempcNmMetodoDistribOrcamento: TStringField
      FieldName = 'cNmMetodoDistribOrcamento'
      Size = 50
    end
    object qryItemOrcamentoTempnValorOrcamentoConta: TBCDField
      FieldName = 'nValorOrcamentoConta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemOrcamentoTempcNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      ReadOnly = True
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryItemOrcamentoTemp
    Left = 368
    Top = 112
  end
  object qryItemOrcamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ItemOrcamento'
      'WHERE nCdItemOrcamento = :nPK')
    Left = 292
    Top = 201
    object qryItemOrcamentonCdItemOrcamento: TIntegerField
      FieldName = 'nCdItemOrcamento'
    end
    object qryItemOrcamentonCdOrcamento: TIntegerField
      FieldName = 'nCdOrcamento'
    end
    object qryItemOrcamentonCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryItemOrcamentonCdPlanoContaBase: TIntegerField
      FieldName = 'nCdPlanoContaBase'
    end
    object qryItemOrcamentonPercentBase: TBCDField
      FieldName = 'nPercentBase'
      Precision = 12
      Size = 2
    end
    object qryItemOrcamentonCdMetodoDistribOrcamento: TIntegerField
      FieldName = 'nCdMetodoDistribOrcamento'
    end
    object qryItemOrcamentonValorOrcamentoConta: TBCDField
      FieldName = 'nValorOrcamentoConta'
      Precision = 12
      Size = 2
    end
    object qryItemOrcamentocOBS: TStringField
      FieldName = 'cOBS'
      Size = 100
    end
    object qryItemOrcamentocOBSGeral: TMemoField
      FieldName = 'cOBSGeral'
      BlobType = ftMemo
    end
    object qryItemOrcamentocFlgGerarRateio: TIntegerField
      FieldName = 'cFlgGerarRateio'
    end
  end
  object DataSource2: TDataSource
    DataSet = qryItemOrcamento
    Left = 384
    Top = 240
  end
  object qryItemOrcamentoMes: TADOQuery
    Connection = frmMenu.Connection
    LockType = ltBatchOptimistic
    OnCalcFields = qryItemOrcamentoMesCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM ItemOrcamentoMes '
      'WHERE nCdItemOrcamento = :nPK '
      'ORDER BY nCdTabTipoMes')
    Left = 484
    Top = 185
    object qryItemOrcamentoMesnCdItemOrcamentoMes: TIntegerField
      FieldName = 'nCdItemOrcamentoMes'
    end
    object qryItemOrcamentoMesnCdItemOrcamento: TIntegerField
      FieldName = 'nCdItemOrcamento'
    end
    object qryItemOrcamentoMesnCdTabTipoMes: TIntegerField
      FieldName = 'nCdTabTipoMes'
    end
    object qryItemOrcamentoMesnValOrcamento: TBCDField
      DisplayLabel = 'Valor Or'#231'amento'
      FieldName = 'nValOrcamento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemOrcamentoMesnPercDistrib: TBCDField
      DisplayLabel = '% Distribui'#231#227'o'
      FieldName = 'nPercDistrib'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryItemOrcamentoMescOBS: TStringField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'cOBS'
      Size = 100
    end
    object qryItemOrcamentoMescNmTabTipoMes: TStringField
      DisplayLabel = 'M'#234's'
      FieldKind = fkCalculated
      FieldName = 'cNmTabTipoMes'
      Size = 50
      Calculated = True
    end
  end
  object dsItemOrcamentoMes: TDataSource
    DataSet = qryItemOrcamentoMes
    Left = 532
    Top = 193
  end
  object qryTabTipoMes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTabTipoMes'
      'FROM TabTipoMes'
      'WHERE nCdTabTipoMes = :nPK')
    Left = 228
    Top = 281
    object qryTabTipoMescNmTabTipoMes: TStringField
      FieldName = 'cNmTabTipoMes'
      Size = 50
    end
  end
end
