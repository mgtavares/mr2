inherited dcmAnaliseLucratividade_view: TdcmAnaliseLucratividade_view
  Left = 65
  Top = 128
  Width = 984
  Height = 598
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'ER2Soft - Data Analysis'
  OldCreateOrder = True
  Position = poScreenCenter
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 113
    Width = 968
    Height = 405
  end
  inherited ToolBar1: TToolBar
    Width = 968
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object PVRowToolBar1: TPVRowToolBar [2]
    Left = 0
    Top = 71
    Width = 968
    Height = 42
    CheckOrder = True
    ItemWidth = 80
    ItemHeight = 21
    Collapsed = False
    DropDownColor = clBtnFace
    DropDownFont.Charset = DEFAULT_CHARSET
    DropDownFont.Color = clWindowText
    DropDownFont.Height = -11
    DropDownFont.Name = 'MS Sans Serif'
    DropDownFont.Style = []
    DropDownAutoExpand = True
    Floating = False
    Map = PivotMap1
    ItemSettings.Color = clBtnFace
    ItemSettings.Font.Charset = DEFAULT_CHARSET
    ItemSettings.Font.Color = clWindowText
    ItemSettings.Font.Height = -11
    ItemSettings.Font.Name = 'MS Sans Serif'
    ItemSettings.Font.Style = []
    ItemSettings.Style = psWinXP
    ItemSettings.Direction = gsTop
    ItemSettings.Intensity = gsNormal
    ItemSettings.FilteredColor = clLime
    ItemSettings.InvisibleColor = clBlue
    ItemSettings.ShowHint = True
    ItemSettings.Tracking = True
    ItemSettings.TrackingColor = clBtnFace
    Settings.Color = clBtnFace
    Settings.Font.Charset = DEFAULT_CHARSET
    Settings.Font.Color = clWindowText
    Settings.Font.Height = -11
    Settings.Font.Name = 'MS Sans Serif'
    Settings.Font.Style = []
    Settings.Style = psFlat
    Settings.Direction = gsTop
    Settings.Intensity = gsNormal
    ModalDropDown = True
    Chink = 4
    TabOrder = 1
    Align = alTop
    MultiLine = False
    MaxLineCount = 2
    ViewFilter = False
  end
  object PVMeasureToolBar1: TPVMeasureToolBar [3]
    Left = 0
    Top = 518
    Width = 968
    Height = 42
    CheckOrder = True
    ItemWidth = 80
    ItemHeight = 21
    Collapsed = False
    DropDownColor = clBtnFace
    DropDownFont.Charset = DEFAULT_CHARSET
    DropDownFont.Color = clWindowText
    DropDownFont.Height = -11
    DropDownFont.Name = 'MS Sans Serif'
    DropDownFont.Style = []
    DropDownAutoExpand = True
    Floating = False
    Map = PivotMap1
    ItemSettings.Color = clBtnFace
    ItemSettings.Font.Charset = DEFAULT_CHARSET
    ItemSettings.Font.Color = clWindowText
    ItemSettings.Font.Height = -11
    ItemSettings.Font.Name = 'MS Sans Serif'
    ItemSettings.Font.Style = []
    ItemSettings.Style = psWinXP
    ItemSettings.Direction = gsTop
    ItemSettings.Intensity = gsNormal
    ItemSettings.FilteredColor = clLime
    ItemSettings.InvisibleColor = clBlue
    ItemSettings.ShowHint = True
    ItemSettings.Tracking = True
    ItemSettings.TrackingColor = clBtnFace
    Settings.Color = clBtnFace
    Settings.Font.Charset = DEFAULT_CHARSET
    Settings.Font.Color = clWindowText
    Settings.Font.Height = -11
    Settings.Font.Name = 'MS Sans Serif'
    Settings.Font.Style = []
    Settings.Style = psWinXP
    Settings.Direction = gsTop
    Settings.Intensity = gsNormal
    ModalDropDown = True
    Chink = 4
    TabOrder = 2
    Align = alBottom
    MultiLine = True
    MaxLineCount = 2
    HideButtons = True
  end
  object PVColToolBar1: TPVColToolBar [4]
    Left = 0
    Top = 29
    Width = 968
    Height = 42
    CheckOrder = True
    ItemWidth = 80
    ItemHeight = 21
    Collapsed = False
    DropDownColor = clBtnFace
    DropDownFont.Charset = DEFAULT_CHARSET
    DropDownFont.Color = clWindowText
    DropDownFont.Height = -11
    DropDownFont.Name = 'MS Sans Serif'
    DropDownFont.Style = []
    DropDownAutoExpand = True
    Floating = False
    Map = PivotMap1
    ItemSettings.Color = clBtnFace
    ItemSettings.Font.Charset = DEFAULT_CHARSET
    ItemSettings.Font.Color = clWindowText
    ItemSettings.Font.Height = -11
    ItemSettings.Font.Name = 'MS Sans Serif'
    ItemSettings.Font.Style = []
    ItemSettings.Style = psWinXP
    ItemSettings.Direction = gsTop
    ItemSettings.Intensity = gsNormal
    ItemSettings.FilteredColor = clLime
    ItemSettings.InvisibleColor = clBlue
    ItemSettings.ShowHint = True
    ItemSettings.Tracking = True
    ItemSettings.TrackingColor = clBtnFace
    Settings.Color = clBtnFace
    Settings.Font.Charset = DEFAULT_CHARSET
    Settings.Font.Color = clWindowText
    Settings.Font.Height = -11
    Settings.Font.Name = 'MS Sans Serif'
    Settings.Font.Style = []
    Settings.Style = psWinXP
    Settings.Direction = gsTop
    Settings.Intensity = gsNormal
    ModalDropDown = True
    Chink = 4
    TabOrder = 3
    Align = alTop
    MultiLine = False
    MaxLineCount = 2
    ViewFilter = False
  end
  object PivotGrid1: TPivotGrid [5]
    Left = 0
    Top = 113
    Width = 968
    Height = 405
    Map = PivotMap1
    MinColWidth = 40
    MaxColWidth = 80
    MinRowHeight = 18
    MaxRowHeight = 54
    RowTotalsHeight = 24
    Align = alClient
    TabOrder = 4
    TabStop = True
    UseDockManager = True
    OnGetCellValue = PivotGrid1GetCellValue
    OnGetColumnTotal = PivotGrid1GetColumnTotal
    FixedColWidth = 150
    Settings.RowHeader.Font.Charset = DEFAULT_CHARSET
    Settings.RowHeader.Font.Color = clWindowText
    Settings.RowHeader.Font.Height = -9
    Settings.RowHeader.Font.Name = 'Arial'
    Settings.RowHeader.Font.Style = []
    Settings.RowHeader.Color = clBtnFace
    Settings.RowHeader.Style = psWinXP
    Settings.RowHeader.Direction = gsLeft
    Settings.RowHeader.Intensity = gsSmall
    Settings.RowHeader.XPButtonColor = clBtnFace
    Settings.RowHeader.XPExpandDirection = gsBottom
    Settings.RowHeader.CollapseStyle = cdsButtons
    Settings.ColumnHeader.Font.Charset = DEFAULT_CHARSET
    Settings.ColumnHeader.Font.Color = clWindowText
    Settings.ColumnHeader.Font.Height = -9
    Settings.ColumnHeader.Font.Name = 'Arial'
    Settings.ColumnHeader.Font.Style = []
    Settings.ColumnHeader.Color = clBtnFace
    Settings.ColumnHeader.Style = psWinXP
    Settings.ColumnHeader.Direction = gsTop
    Settings.ColumnHeader.Intensity = gsSmall
    Settings.ColumnHeader.XPButtonColor = clBtnFace
    Settings.ColumnHeader.XPExpandDirection = gsBottom
    Settings.ColumnHeader.CollapseStyle = cdsButtons
    Settings.Cells.Zero.Font.Charset = DEFAULT_CHARSET
    Settings.Cells.Zero.Font.Color = clBlue
    Settings.Cells.Zero.Font.Height = -9
    Settings.Cells.Zero.Font.Name = 'Arial'
    Settings.Cells.Zero.Font.Style = []
    Settings.Cells.Zero.Color = clWhite
    Settings.Cells.Zero.Style = psWinXP
    Settings.Cells.Zero.Direction = gsLeft
    Settings.Cells.Zero.Intensity = gsSmall
    Settings.Cells.Negative.Font.Charset = DEFAULT_CHARSET
    Settings.Cells.Negative.Font.Color = clRed
    Settings.Cells.Negative.Font.Height = -9
    Settings.Cells.Negative.Font.Name = 'Arial'
    Settings.Cells.Negative.Font.Style = []
    Settings.Cells.Negative.Color = clWhite
    Settings.Cells.Negative.Style = psWinXP
    Settings.Cells.Negative.Direction = gsLeft
    Settings.Cells.Negative.Intensity = gsSmall
    Settings.Cells.Positive.Font.Charset = DEFAULT_CHARSET
    Settings.Cells.Positive.Font.Color = clWindowText
    Settings.Cells.Positive.Font.Height = -9
    Settings.Cells.Positive.Font.Name = 'Arial'
    Settings.Cells.Positive.Font.Style = []
    Settings.Cells.Positive.Color = clWhite
    Settings.Cells.Positive.Style = psWinXP
    Settings.Cells.Positive.Direction = gsLeft
    Settings.Cells.Positive.Intensity = gsSmall
    Settings.Cells.Focused.Font.Charset = DEFAULT_CHARSET
    Settings.Cells.Focused.Font.Color = clWhite
    Settings.Cells.Focused.Font.Height = -9
    Settings.Cells.Focused.Font.Name = 'Arial'
    Settings.Cells.Focused.Font.Style = []
    Settings.Cells.Focused.Color = clTeal
    Settings.Cells.Focused.Style = psWinXP
    Settings.Cells.Focused.Direction = gsLeft
    Settings.Cells.Focused.Intensity = gsSmall
    Settings.Cells.Selected.Font.Charset = DEFAULT_CHARSET
    Settings.Cells.Selected.Font.Color = clWindowText
    Settings.Cells.Selected.Font.Height = -9
    Settings.Cells.Selected.Font.Name = 'Arial'
    Settings.Cells.Selected.Font.Style = []
    Settings.Cells.Selected.Color = clAqua
    Settings.Cells.Selected.Style = psWinXP
    Settings.Cells.Selected.Direction = gsLeft
    Settings.Cells.Selected.Intensity = gsSmall
    Settings.Cells.Sort.Font.Charset = DEFAULT_CHARSET
    Settings.Cells.Sort.Font.Color = clYellow
    Settings.Cells.Sort.Font.Height = -9
    Settings.Cells.Sort.Font.Name = 'Arial'
    Settings.Cells.Sort.Font.Style = [fsBold]
    Settings.Cells.Sort.Color = clBlue
    Settings.Cells.Sort.Style = psWinXP
    Settings.Cells.Sort.Direction = gsLeft
    Settings.Cells.Sort.Intensity = gsSmall
    Settings.Cells.SelectionTotal.Font.Charset = DEFAULT_CHARSET
    Settings.Cells.SelectionTotal.Font.Color = clWindowText
    Settings.Cells.SelectionTotal.Font.Height = -9
    Settings.Cells.SelectionTotal.Font.Name = 'Arial'
    Settings.Cells.SelectionTotal.Font.Style = []
    Settings.Cells.SelectionTotal.Color = clYellow
    Settings.Cells.SelectionTotal.Style = psWinXP
    Settings.Cells.SelectionTotal.Direction = gsLeft
    Settings.Cells.SelectionTotal.Intensity = gsSmall
    Settings.Totals.Font.Charset = DEFAULT_CHARSET
    Settings.Totals.Font.Color = clWindowText
    Settings.Totals.Font.Height = -11
    Settings.Totals.Font.Name = 'Arial'
    Settings.Totals.Font.Style = [fsBold]
    Settings.Totals.Color = clBtnFace
    Settings.Totals.Style = psWinXP
    Settings.Totals.Direction = gsLeft
    Settings.Totals.Intensity = gsSmall
    Settings.Specific.Background.Font.Charset = DEFAULT_CHARSET
    Settings.Specific.Background.Font.Color = clWindowText
    Settings.Specific.Background.Font.Height = -9
    Settings.Specific.Background.Font.Name = 'Arial'
    Settings.Specific.Background.Font.Style = []
    Settings.Specific.Background.Color = clBtnFace
    Settings.Specific.Background.Style = psFlat
    Settings.Specific.Background.Direction = gsLeft
    Settings.Specific.Background.Intensity = gsSmall
    Settings.Specific.Lines = False
    Settings.Specific.LineWidth = 1
    Settings.Specific.LineColor = clBtnShadow
    Settings.Specific.RowTotals = pvgtAtEnd
    Settings.Specific.ColumnTotals = pvgtFixed
    Settings.Specific.ShowDimensionNames = True
    Settings.Specific.DisableCache = False
    Settings.Specific.LoadAllCells = False
    Settings.Specific.ShowZero = True
    Settings.Specific.MultiLineHeaders = True
    Settings.Specific.DoubleBuffering = True
    Settings.Specific.Dimension.Font.Charset = DEFAULT_CHARSET
    Settings.Specific.Dimension.Font.Color = clWindowText
    Settings.Specific.Dimension.Font.Height = -11
    Settings.Specific.Dimension.Font.Name = 'Arial'
    Settings.Specific.Dimension.Font.Style = []
    Settings.Specific.Dimension.Color = clBtnFace
    Settings.Specific.Dimension.Style = psWinXP
    Settings.Specific.Dimension.Direction = gsTop
    Settings.Specific.Dimension.Intensity = gsSmall
    Settings.Specific.Dimension.XPButtonColor = clBtnFace
    Settings.Specific.Dimension.XPExpandDirection = gsBottom
    Settings.Specific.Dimension.CollapseStyle = cdsButtons
    Settings.Specific.ChartDropDownInView = True
    Settings.Specific.ChartDropDownInColumn = True
    Settings.Specific.AllowSelectionSubTotals = True
    Settings.Specific.ShowViewNames = True
    Settings.Specific.ShowMeasureNames = True
    Settings.Specific.OddCellColor = clNone
    Settings.Specific.DialogButtons = True
    Settings.Specific.CustomResize = False
    Settings.Specific.AutoEdit = False
    Settings.SubtotalSettings.Enabled = False
    Settings.SubtotalSettings.ExpandDown = True
    Settings.SubtotalSettings.DisableIfOnlyOne = True
    Settings.SubtotalSettings.SubTotalCell = False
    Settings.SubtotalSettings.IncludeName = False
    Settings.PrintSettings.TrueTypeFont.Charset = DEFAULT_CHARSET
    Settings.PrintSettings.TrueTypeFont.Color = clWindowText
    Settings.PrintSettings.TrueTypeFont.Height = -13
    Settings.PrintSettings.TrueTypeFont.Name = 'Times New Roman'
    Settings.PrintSettings.TrueTypeFont.Style = []
    Settings.PrintSettings.PageHeader = 'ER2Soft - An'#225'lise de Vendas de Clientes e Produtos'
    Settings.PrintSettings.HeaderSize = 12
    Settings.PrintSettings.PageHeaderFont.Charset = DEFAULT_CHARSET
    Settings.PrintSettings.PageHeaderFont.Color = clWindowText
    Settings.PrintSettings.PageHeaderFont.Height = -13
    Settings.PrintSettings.PageHeaderFont.Name = 'Times New Roman'
    Settings.PrintSettings.PageHeaderFont.Style = []
    Settings.PrintSettings.PageFooter = 'www.er2soft.com.br'
    Settings.PrintSettings.PageFooterFont.Charset = DEFAULT_CHARSET
    Settings.PrintSettings.PageFooterFont.Color = clWindowText
    Settings.PrintSettings.PageFooterFont.Height = -13
    Settings.PrintSettings.PageFooterFont.Name = 'Times New Roman'
    Settings.PrintSettings.PageFooterFont.Style = []
    Settings.PrintSettings.FooterSize = 12
    Settings.PrintSettings.PageNumber = True
    Settings.PrintSettings.DateTime = True
    ExtraViewInExport = True
  end
  object ADODataSet1: TADODataSet
    Connection = frmMenu.Connection
    CommandText = 
      'DECLARE @dDtInicial        VARCHAR(10)'#13#10#9'   ,@dDtFinal          ' +
      'VARCHAR(10)'#13#10'       ,@nCdTerceiro       int'#13#10'       ,@nCdGrupoEc' +
      'onomico int'#13#10'       ,@nCdDepartamento   int'#13#10'       ,@nCdMarca  ' +
      '        int'#13#10'       ,@nCdLinha          int'#13#10'       ,@nCdClasseP' +
      'roduto  int'#13#10'       ,@nCdRamoAtividade  int'#13#10#13#10'Set @dDtInicial  ' +
      '      = :dDtInicial'#13#10'Set @dDtFinal          = :dDtFinal'#13#10'Set @nC' +
      'dTerceiro       = :nCdTerceiro'#13#10'Set @nCdGrupoEconomico = :nCdGru' +
      'poEconomico'#13#10'Set @nCdDepartamento   = :nCdDepartamento'#13#10'Set @nCd' +
      'Marca          = :nCdMarca'#13#10'Set @nCdLinha          = :nCdLinha'#13#10 +
      'Set @nCdClasseProduto  = :nCdClasseProduto'#13#10'Set @nCdRamoAtividad' +
      'e  = :nCdRamoAtividade'#13#10#13#10#13#10'IF (OBJECT_ID('#39'tempdb..#Temp_Produto' +
      '_Detalhe'#39') IS NOT NULL) '#13#10'  DROP TABLE #Temp_Produto_Detalhe '#13#10#13 +
      #10'IF (OBJECT_ID('#39'tempdb..#Temp_Produto_Resumo'#39') IS NOT NULL) '#13#10'  ' +
      'DROP TABLE #Temp_Produto_Resumo '#13#10#13#10'SELECT dDtPedido as Ano'#13#10'   ' +
      '   ,dDtPedido as Mes'#13#10'      ,cNmDepartamento'#13#10'      ,cNmCategori' +
      'a'#13#10'      ,cNmSubCategoria'#13#10'      ,cNmSegmento'#13#10'      ,cNmMarca'#13#10 +
      '      ,cNmLinha'#13#10'      ,cNmClasseProduto'#13#10'      ,cNmProduto'#13#10'   ' +
      '   ,cNmRamoAtividade'#13#10'      ,cNmTerceiro'#13#10'      ,nValCustoUnit *' +
      ' (nQtdePed-nQtdeCanc) as nValor'#13#10'      ,nQtdePed-nQtdeCanc as nQ' +
      'tde'#13#10'      ,nQtdePed * CASE WHEN IsNull((SELECT TOP 1 nValPreco ' +
      'FROM PrecoMedio WHERE PrecoMedio.nCdEmpresa = Pedido.nCdEmpresa ' +
      'AND PrecoMedio.nCdProduto = IP.nCdProduto AND PrecoMedio.dDtPrec' +
      'o <= Pedido.dDtPedido ORDER BY dDtPreco DESC),0) > 0 THEN '#13#10'    ' +
      '                        IsNull((SELECT TOP 1 nValPreco FROM Prec' +
      'oMedio WHERE PrecoMedio.nCdEmpresa = Pedido.nCdEmpresa AND Preco' +
      'Medio.nCdProduto = IP.nCdProduto AND PrecoMedio.dDtPreco <= Pedi' +
      'do.dDtPedido ORDER BY dDtPreco DESC),0)'#13#10'                       ' +
      'ELSE Produto.nValCusto'#13#10'                  END nValCustoMedio'#13#10'  ' +
      '    ,0.00 nValLucro'#13#10'  INTO #Temp_Produto_Detalhe'#13#10'  FROM Pedido' +
      #13#10#9'   INNER JOIN ItemPedido IP  ON IP.nCdPedido                 ' +
      '    = Pedido.nCdPedido'#13#10#9'   INNER JOIN TipoPedido     ON TipoPed' +
      'ido.nCdTipoPedido         = Pedido.nCdTipoPedido'#13#10#9'   INNER JOIN' +
      ' Terceiro       ON Terceiro.nCdTerceiro             = Pedido.nCd' +
      'Terceiro'#13#10#9'   LEFT  JOIN GrupoEconomico ON GrupoEconomico.nCdGru' +
      'poEconomico = Terceiro.nCdGrupoEconomico'#13#10#9'   INNER JOIN Produto' +
      '        ON Produto.nCdProduto               = IP.nCdProduto'#13#10#9'  ' +
      ' INNER JOIN Departamento   ON Departamento.nCdDepartamento     =' +
      ' Produto.nCdDepartamento'#13#10#9'   INNER JOIN Categoria      ON Categ' +
      'oria.nCdCategoria           = Produto.nCdCategoria'#13#10#9'   INNER JO' +
      'IN SubCategoria   ON SubCategoria.nCdSubCategoria     = Produto.' +
      'nCdSubCategoria'#13#10#9'   INNER JOIN Segmento       ON Segmento.nCdSe' +
      'gmento             = Produto.nCdSegmento'#13#10'       LEFT  JOIN Marc' +
      'a          ON Marca.nCdMarca                   = Produto.nCdMarc' +
      'a'#13#10'       LEFT  JOIN Linha          ON Linha.nCdLinha           ' +
      '        = Produto.nCdLinha'#13#10'       LEFT  JOIN ClasseProduto  ON ' +
      'ClasseProduto.nCdClasseProduto   = Produto.nCdClasseProduto'#13#10'   ' +
      '    LEFT  JOIN RamoAtividade  ON RamoAtividade.nCdRamoAtividade ' +
      '  = Terceiro.nCdRamoAtividade'#13#10' WHERE cFlgVenda        = 1'#13#10'   A' +
      'ND cGerarFinanc = 1'#13#10'   AND Pedido.nCdTabStatusPed       IN (3,4' +
      ',5)'#13#10'   AND nCdTipoItemPed NOT IN (4,6,8)  -- N'#227'o incluir os ite' +
      'ns do kit formulado'#13#10'   AND Pedido.nCdEmpresa = :nCdEmpresa'#13#10'   ' +
      'AND ((dDtPedido                  >= Convert(DATETIME,@dDtInicial' +
      ',103)) OR (@dDtInicial = '#39'01/01/1900'#39'))'#13#10'   AND ((dDtPedido     ' +
      '              < Convert(DATETIME,@dDtFinal,103)+1) OR (@dDtFinal' +
      '   = '#39'01/01/1900'#39')) '#13#10'   AND ((Pedido.nCdTerceiro         = @nCd' +
      'Terceiro) OR (@nCdTerceiro = 0))'#13#10'   AND ((Terceiro.nCdGrupoEcon' +
      'omico = @nCdGrupoEconomico) OR (@nCdGrupoEconomico = 0))'#13#10'   AND' +
      ' ((Produto.nCdDepartamento    = @nCdDepartamento)   OR (@nCdDepa' +
      'rtamento = 0))'#13#10'   AND ((Produto.nCdMarca           = @nCdMarca)' +
      '          OR (@nCdMarca = 0))'#13#10'   AND ((Produto.nCdLinha        ' +
      '   = @nCdLinha)          OR (@nCdLinha = 0))'#13#10'   AND ((Produto.n' +
      'CdClasseProduto   = @nCdClasseProduto)  OR (@nCdClasseProduto = ' +
      '0))'#13#10'   AND ((Terceiro.nCdRamoAtividade  = @nCdRamoAtividade)  O' +
      'R (@nCdRamoAtividade = 0))'#13#10#13#10'SELECT Ano'#13#10'      ,Mes'#13#10'      ,cNm' +
      'Departamento'#13#10'      ,cNmCategoria'#13#10'      ,cNmSubCategoria'#13#10'     ' +
      ' ,cNmSegmento'#13#10'      ,cNmMarca'#13#10'      ,cNmLinha'#13#10'      ,cNmClass' +
      'eProduto'#13#10'      ,cNmProduto'#13#10'      ,cNmRamoAtividade'#13#10'      ,cNm' +
      'Terceiro'#13#10'      ,Sum(nValor) nValor'#13#10'      ,Sum(nQtde) nQtde'#13#10'  ' +
      '    ,Sum(nValCustoMedio) nValCustoMedio'#13#10'      ,Sum(nValLucro) n' +
      'ValLucro'#13#10'      ,Sum(0.00) nPercLucro'#13#10'  INTO #Temp_Produto_Resu' +
      'mo'#13#10'  FROM #Temp_Produto_Detalhe'#13#10' GROUP BY Ano'#13#10'      ,Mes'#13#10'   ' +
      '   ,cNmTerceiro'#13#10'      ,cNmDepartamento'#13#10'      ,cNmCategoria'#13#10'  ' +
      '    ,cNmSubCategoria'#13#10'      ,cNmSegmento'#13#10'      ,cNmMarca'#13#10'     ' +
      ' ,cNmLinha'#13#10'      ,cNmClasseProduto'#13#10'      ,cNmProduto'#13#10'      ,c' +
      'NmRamoAtividade'#13#10#13#10'UPDATE #Temp_Produto_Resumo'#13#10'   SET nValLucro' +
      ' = nValor - nValCustoMedio'#13#10#13#10'UPDATE #Temp_Produto_Resumo'#13#10'   SE' +
      'T nPercLucro = ( (nValor / nValCustoMedio) -1) * 100'#13#10' WHERE nVa' +
      'lCustoMedio > 0'#13#10#13#10'UPDATE #Temp_Produto_Resumo'#13#10'   SET nPercLucr' +
      'o = 100'#13#10' WHERE nValCustoMedio = 0'#13#10#13#10'SELECT * '#13#10'  FROM #Temp_Pr' +
      'oduto_Resumo'#13#10' '#13#10'DROP TABLE #Temp_Produto_Detalhe '#13#10'DROP TABLE #' +
      'Temp_Produto_Resumo '#13#10
    Parameters = <
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1901'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1901'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdGrupoEconomico'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdDepartamento'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdMarca'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLinha'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdClasseProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdRamoAtividade'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    Left = 432
    Top = 192
    object ADODataSet1Ano: TDateTimeField
      FieldName = 'Ano'
    end
    object ADODataSet1Mes: TDateTimeField
      FieldName = 'Mes'
    end
    object ADODataSet1cNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object ADODataSet1cNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
    object ADODataSet1cNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object ADODataSet1cNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
    object ADODataSet1cNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
    object ADODataSet1cNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
    object ADODataSet1cNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
    object ADODataSet1cNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object ADODataSet1cNmRamoAtividade: TStringField
      FieldName = 'cNmRamoAtividade'
      Size = 50
    end
    object ADODataSet1cNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object ADODataSet1nValor: TBCDField
      FieldName = 'nValor'
      Precision = 32
      Size = 6
    end
    object ADODataSet1nQtde: TBCDField
      FieldName = 'nQtde'
      Precision = 32
    end
    object ADODataSet1nValCustoMedio: TBCDField
      FieldName = 'nValCustoMedio'
      Precision = 32
      Size = 8
    end
    object ADODataSet1nValLucro: TBCDField
      FieldName = 'nValLucro'
      Precision = 32
      Size = 2
    end
    object ADODataSet1nPercLucro: TBCDField
      FieldName = 'nPercLucro'
      Precision = 32
      Size = 2
    end
  end
  object PivotCube1: TPivotCube
    FactTableDataSet = ADODataSet1
    Dimensions = <
      item
        Enabled = True
        AliasName = 'Ano'
        FieldName = 'Ano'
        DataSet = ADODataSet1
        WrapTo = wt_Years
        Sorting = dmtNoSort
        NullName = '[Null]'
        UnknownName = '[UNKNOWN]'
        DisplayName = 'Ano'
        Forecasting.PrecedingName = '[Preceding]'
        Forecasting.ConsequentName = '[Consequent]'
        Forecasting.Method = dftTripleExponentialSmoothing
        Forecasting.Enabled = False
        EmptyItems = deiMarkDisabled
      end
      item
        Enabled = True
        AliasName = 'Mes'
        FieldName = 'Mes'
        DataSet = ADODataSet1
        WrapTo = wt_Months
        Sorting = dmtNoSort
        NullName = '[Null]'
        UnknownName = '[UNKNOWN]'
        DisplayName = 'Mes'
        Forecasting.PrecedingName = '[Preceding]'
        Forecasting.ConsequentName = '[Consequent]'
        Forecasting.Method = dftTripleExponentialSmoothing
        Forecasting.Enabled = False
        EmptyItems = deiMarkDisabled
      end
      item
        Enabled = True
        AliasName = 'cNmTerceiro'
        FieldName = 'cNmTerceiro'
        DataSet = ADODataSet1
        WrapTo = wt_None
        Sorting = dmtNameSort
        NullName = 'N\A'
        UnknownName = '[UNKNOWN]'
        DisplayName = 'Terceiro/Cliente'
        Forecasting.PrecedingName = '[Preceding]'
        Forecasting.ConsequentName = '[Consequent]'
        Forecasting.Method = dftTripleExponentialSmoothing
        Forecasting.Enabled = False
        EmptyItems = deiMarkDisabled
      end
      item
        Enabled = True
        AliasName = 'cNmDepartamento'
        FieldName = 'cNmDepartamento'
        DataSet = ADODataSet1
        WrapTo = wt_None
        Sorting = dmtNoSort
        NullName = 'N\A'
        UnknownName = '[UNKNOWN]'
        DisplayName = 'Departamento'
        Forecasting.PrecedingName = '[Preceding]'
        Forecasting.ConsequentName = '[Consequent]'
        Forecasting.Method = dftTripleExponentialSmoothing
        Forecasting.Enabled = False
        EmptyItems = deiMarkDisabled
      end
      item
        Enabled = True
        AliasName = 'cNmCategoria'
        FieldName = 'cNmCategoria'
        DataSet = ADODataSet1
        WrapTo = wt_None
        Sorting = dmtNoSort
        NullName = 'N\A'
        UnknownName = '[UNKNOWN]'
        DisplayName = 'Categoria'
        Forecasting.PrecedingName = '[Preceding]'
        Forecasting.ConsequentName = '[Consequent]'
        Forecasting.Method = dftTripleExponentialSmoothing
        Forecasting.Enabled = False
        EmptyItems = deiMarkDisabled
      end
      item
        Enabled = True
        AliasName = 'cNmSubCategoria'
        FieldName = 'cNmSubCategoria'
        DataSet = ADODataSet1
        WrapTo = wt_None
        Sorting = dmtNoSort
        NullName = 'N\A'
        UnknownName = '[UNKNOWN]'
        DisplayName = 'SubCategoria'
        Forecasting.PrecedingName = '[Preceding]'
        Forecasting.ConsequentName = '[Consequent]'
        Forecasting.Method = dftTripleExponentialSmoothing
        Forecasting.Enabled = False
        EmptyItems = deiMarkDisabled
      end
      item
        Enabled = True
        AliasName = 'cNmSegmento'
        FieldName = 'cNmSegmento'
        DataSet = ADODataSet1
        WrapTo = wt_None
        Sorting = dmtNoSort
        NullName = 'N\A'
        UnknownName = '[UNKNOWN]'
        DisplayName = 'Segmento'
        Forecasting.PrecedingName = '[Preceding]'
        Forecasting.ConsequentName = '[Consequent]'
        Forecasting.Method = dftTripleExponentialSmoothing
        Forecasting.Enabled = False
        EmptyItems = deiMarkDisabled
      end
      item
        Enabled = True
        AliasName = 'cNmMarca'
        FieldName = 'cNmMarca'
        DataSet = ADODataSet1
        WrapTo = wt_None
        Sorting = dmtNoSort
        NullName = 'N\A'
        UnknownName = '[UNKNOWN]'
        DisplayName = 'Marca'
        Forecasting.PrecedingName = '[Preceding]'
        Forecasting.ConsequentName = '[Consequent]'
        Forecasting.Method = dftTripleExponentialSmoothing
        Forecasting.Enabled = False
        EmptyItems = deiMarkDisabled
      end
      item
        Enabled = True
        AliasName = 'cNmLinha'
        FieldName = 'cNmLinha'
        DataSet = ADODataSet1
        WrapTo = wt_None
        Sorting = dmtNoSort
        NullName = 'N\A'
        UnknownName = '[UNKNOWN]'
        DisplayName = 'Linha'
        Forecasting.PrecedingName = '[Preceding]'
        Forecasting.ConsequentName = '[Consequent]'
        Forecasting.Method = dftTripleExponentialSmoothing
        Forecasting.Enabled = False
        EmptyItems = deiMarkDisabled
      end
      item
        Enabled = True
        AliasName = 'cNmClasseProduto'
        FieldName = 'cNmClasseProduto'
        DataSet = ADODataSet1
        WrapTo = wt_None
        Sorting = dmtNoSort
        NullName = 'N\A'
        UnknownName = '[UNKNOWN]'
        DisplayName = 'Classe Produto'
        Forecasting.PrecedingName = '[Preceding]'
        Forecasting.ConsequentName = '[Consequent]'
        Forecasting.Method = dftTripleExponentialSmoothing
        Forecasting.Enabled = False
        EmptyItems = deiMarkDisabled
      end
      item
        Enabled = True
        AliasName = 'cNmProduto'
        FieldName = 'cNmProduto'
        DataSet = ADODataSet1
        WrapTo = wt_None
        Sorting = dmtNoSort
        NullName = '[Null]'
        UnknownName = '[UNKNOWN]'
        DisplayName = 'Produto'
        Forecasting.PrecedingName = '[Preceding]'
        Forecasting.ConsequentName = '[Consequent]'
        Forecasting.Method = dftTripleExponentialSmoothing
        Forecasting.Enabled = False
        EmptyItems = deiMarkDisabled
      end
      item
        Enabled = True
        AliasName = 'cNmRamoAtividade'
        FieldName = 'cNmRamoAtividade'
        DataSet = ADODataSet1
        WrapTo = wt_None
        Sorting = dmtNoSort
        NullName = '[Null]'
        UnknownName = '[UNKNOWN]'
        DisplayName = 'Ramo de Atividade'
        Forecasting.PrecedingName = '[Preceding]'
        Forecasting.ConsequentName = '[Consequent]'
        Forecasting.Method = dftTripleExponentialSmoothing
        Forecasting.Enabled = False
        EmptyItems = deiMarkDisabled
      end>
    LNG.Strings = (
      'RowTotalName=Total'
      'ColumnTotalName=Total'
      'PanelRowsCaption=Linhas'
      'PanelColumnsCaption=Colunas'
      'PanelDimsCaption=Dimensoes'
      'CustomFilter=Custom Filter'
      
        'pvToolBarCollapseError=Could not collapse TOOLBAR without border' +
        ' alignment'
      
        'pvToolBarTreeFilterWarn=Cannot use incremental filtering on hier' +
        'arhical dimensions!'
      'pvToolBarNoFilter=No filter'
      'pvToolBarCustomFilter=Custom filter'
      'pvCubeErr1=Could not load dimension data! '
      'pvCubeErr2=Could not load measure data! '
      'pvCubeErr3=: You must specify dataset for dimension!'
      
        'pvCubeErr4=Could not load dimension items! Dimension: %s  Dimens' +
        'ion item: %s '
      'pvCubeErr5=Could not start building cube! Unknown error'
      
        'pvCubeErr6=You must set appropriate EventHandler for custom wrap' +
        'ped dimensions! : %s'
      'pvCubeErr7=You must set FactTableDataSet property!'
      'pvCubeErr8=You must set at least ONE measure'
      'pvCubeErr9=You must set at least ONE dimension'
      'pvMapErr1=Invalid measure index '
      'pvMapErr2=One measure always must be visible! '
      'pvGridExcel0=Export note'
      
        'pvGridExcel1=Number of columns more than 256. Only first 255 col' +
        'umns will be exported!'
      'pvGridExcel2=Number of columns must be more than 0!'
      'pvGridExcel3=Number of rows must be more than 0!'
      
        'pvGridExcel4=Could not export data to Excel! Check your MS Excel' +
        ' installation'
      'pvAMFAddMeasureForm=Calculated measures manager'
      'pvAMFAddMeasureBtn=Add new measure'
      'pvAMFEditMeasureBtn=Edit measure name'
      'pvAMFDelMeasureBtn=Delete calculated measure'
      'pvAMFCalcLabel=Calculated measures'
      'pvAMFCalcEditLabel=Calculation formula'
      'pvAMFMeasureLabel=Available measures'
      'pvAMFViewsLabel=Available views'
      'pvAMFCheckBtn=Check formula '
      'pvAMFSynaxError=Syntax Error'
      'pvAMFFormulaError=: Error in formula '
      'pvAMFDuplicateError=Duplicate measure name'
      'pvAMFCheckOKName=Checked!'
      'pvToolBarDDOKBtnHint=Accept changes'
      'pvToolBarDDCancelBtnHint=Cancel changes'
      'pvToolBarDDFillBtnHint=Set all items to selected'
      'pvToolBarDDClearBtnHint=Set all items to un-selected'
      
        'pvToolBarDDRevertBtnHint=Set all selected items to unselected an' +
        'd all select all unselected items'
      'pvToolBarDDExpandBtnHint=Expand/Collapse all tree nodes(if any)'
      'pvMapValueName=Valor'
      'pvMapRowPercentName=Percents by ROW'
      'pvMapColPercentName=% Particip.'
      'pvMapRecordCountName=Count'
      'pvMapRankName=Rank'
      'pvMapRankRName=Rank'
      'pvMapSubPrevCName=Difference with previous Column'
      'pvMapSubPrevRName=Difference with previous Row'
      'pvMapAddPrevCName=Sum with previous Column'
      'pvMapAddPrevRName=Sum with previous Row'
      'pvMapRunningTotalName1=Running Total by Column'
      'pvMapRunningTotalName2=Running Total by Row'
      'pvMapRowOwnerPercentName=% by r group'
      'pvMapColOwnerPercentName=% by c group'
      'pvBtnHintAccept=Accept'
      'pvBtnHintCancel=Cancel'
      'pvBtnHintExpandTree=Expand/Collapse tree nodes'
      
        'pvBtnHintCalcMeasureDialog=Run calculated measure dialog - to cr' +
        'eate,edit or delete calculate measures'
      'pvBtnHintMeasureFillEnable=Set all measures to enabled state'
      'pvBtnHintMeasureFillDisable=Set all measures to disabled state'
      
        'pvBtnHintMeasureRevert=Toggle measure state - enabled to disable' +
        'd, disabled to enabled'
      'pvBtnHintDimFillActive=Set all dimensions to visible state'
      'pvBtnHintDimFillHided=Set all dimensions to invisible state'
      'pvBtnHintDimFillFiltered=Set all dimensions to filtered state'
      
        'pvBtnHintDimRevert=Circular shift of dimension state - visible t' +
        'o invisible, invisible to filtered filtered to visible'
      'pvBtnHintDimAddGroup=Add hierarchy group'
      'pvBtnHintDimDelGroup=Delete hierarchy group'
      'mbFormCaption=Map builder'
      'mbRowsCaption=Rows'
      'mbColumnsCaption=Columns'
      'mbDimsCaption=Dimensions'
      'mbDisabledDimsCaption=Disabled Dimensions'
      'mbRunEditBtnHint=Execute dimension editor for selected dimension'
      
        'mbTreeViewHint=Drag'#39'n'#39'Drop dimension lable to choosen group. Dbl' +
        '-click to run dimension editor'
      'DimensionFormCaption=Dimension editor : '
      'dimDisplayName=Caption'
      'dimCount=Items count'
      'dimSOrtBtn=Sort'
      'dimSortBtnHint=Sort dimension items for specified order'
      'dimActiveItems=Active items'
      'dimInActiveItems=Repository(inactive items)'
      'dimFilterError=At least 1 dimension item must be active!'
      'dimSearch=Find'
      
        'dimTVHint=Drag'#39'n'#39'drop selected item(s) to group or repository ar' +
        'ea'
      'dimSortByName=Sort by name'
      'dimSortByKey=Sort by key'
      'dimNoSort=Unsorted'
      'dimSortAscending=Ascending'
      'dimSortDescending=Descending'
      'dimCustomSort=Custom sort'
      'dimForecastingCB1=Prev.'
      'dimForecastingCB2=Next.'
      'dimForecastingType=Method'
      'dimForecastingMethod1=Simple moving average'
      'dimForecastingMethod2=Weighted Moving Average'
      'dimForecastingMethod3=Double Exponential Smoothing'
      'dimForecastingMethod4=Triple Exponential Smoothing'
      'dimForecastingMethod5=Show Data Margins Only'
      'dimForecastingMethod6=Show First and Last members'
      'MeasureCommonFormCaption=Measure manager'
      'MFormatBtnName=Build format'
      'MFormatFormCaption=Build format string'
      'pvMapMVFmt0=Change measure type'
      'pvMapMVFmt1=Cell'
      'pvMapMVFmt2=Percent by Column'
      'pvMapMVFmt3=Percent by Row'
      'pvMapMVFmt4=Record count'
      'pvMapMVFmt5=Rank by Column'
      'pvMapMVFmt6=Rank by Row'
      'pvMapMVFmt7=Previous member by Row'
      'pvMapMVFmt8=Next member by Row'
      'pvMapMVFmt9=Previous member by Column'
      'pvMapMVFmt10=Next member by Column'
      'pvMapMVFmt11=Running Total by Column'
      'pvMapMVFmt12=Running Total by Row'
      'pmMapMVOp1=Value'
      'pmMapMVOp2=Subtract'
      'pmMapMVOp3=Add'
      'pmMapMVOp4=Divide'
      'pmMapMVOp5=Multiply'
      'pvMeasureCommon5=Min. value'
      'pvMeasureCommon6=Max. value'
      'pvMeasureCommon9=Value'
      'pvMeasureCommon10=Operation'
      'pvMeasureCommon11=Only one measure visible'
      'pvMeasureCommon12=Value representation'
      'pvMeasureCommon13=Filters'
      'pvMeasureCommon14=Filter by Map cells'
      'pvMeasureCommon15=Filter by fact table records'
      'pvMeasureCommon16=Measure formating'
      'pvMeasureCommon17=Formula (is calculated measure)'
      'pvMeasureCommon18=Enabled'
      'pvMeasureCommon19=Running Total by Column'
      'pvMeasureCommon20=Running Total by Row'
      'pvMeasureCommon21=Display caption'
      'MeasureFilterError= Measure filter error'
      'mfeWrongValue=Wrong value in  measure filter: '
      'MeasureCommonFormDistinctName=Distinct'
      'pvgSearchFormCaption=Search'
      'pvgSearchArea=Search area'
      'pvgSearchRow=Row header'
      'pvgSearchCol=Column header'
      'pvgSearchText=Text for search'
      'pvgSearch1=Next'
      'pvgSearch2=Forward'
      'pvgSearch3=Case sensitive'
      'pvgSearchBtn=Find again'
      'pvgSearchFinish=Cannot find more'
      'uHtmBuildHeaders=building headers...'
      'uHtmPreparing=prepearing...'
      'uHtmColTotal=Total by COLUMNS'
      'uHtmRowTotal=Total by ROWS'
      'uHtmExportColHeader=exporting column headers...'
      
        'uHtmExportCells=exporting row headers, cell values and row total' +
        's...'
      'uHtmExportColTotal=exporting column totals...'
      'uHtmSave=saving and executing...'
      'uHtmLabel=Action...'
      'ChartDDRows=Rows'
      'ChartDDColumns=Columns'
      'ddChartViews=Views'
      'ddChartLegend=Legend'
      'ddChartMarks=Marks'
      'ChartXNextPage=Next page'
      'ChartXPrevPage=Prev. page'
      'CubeButtonCaption=Cube'
      'ChartButtonCaption=Chart'
      'pvGridSubTotal=Subtotal'
      'pvGridSubTotalwName=Sub-total by '
      'MFormatCount=3'
      'MFormatName0=Generic'
      'MFormatValue0=#0.00'
      'MFormatName1=Price (US)'
      'MFormatValue1=$ #0.00'
      'MFormatName2=Summa'
      'MFormatValue2=# ##0.00'
      'MFormatCap1=Predefined formats'
      'MFormatCap2=Format string'
      'MFormatCap3=Number of decimals'
      'MFormatCap4=Before'
      'MFormatCap5=After'
      'MFormatCap6=Design'
      'MFormatCap7=Symbol'
      'MFormatCap8=Example'
      'MFormatCap9=Thousand separator'
      'uXLSActionLabel=Action'
      'uXLSAction1=Prepearing basic fonts'
      'uXLSAction2=Prepearing basic formats'
      'uXLSAction3=Applying row totals headers merging'
      'uXLSAction4=Applying cells format'
      'uXLSAction5=Applying columns headers merging'
      'uXLSAction6=Applying rows headers merging'
      'uXLSAction7=Applying column totals headers merging'
      'uXLSAction8=Applying auto width for columns'
      'uXLActionLabel=Action'
      'uXLAction1=Prepearing cells array'
      'uXLAction2=Prepearing column headers'
      'uXLAction3=Processing'
      'uXLAction4=Merging column headers'
      'uXLAction5=Merging row headers'
      'uXLAction6=Merging row total headers'
      'uXLAction7=Applying cells format'
      'pvGridPrint1=Print All'
      'pvGridPrint2=Print selected'
      'pvGridPrint3=Right click to scale menu'
      'uXLSActionLabel2=Action progress'
      'uXLSActionLabel2=Full progress'
      'uHtmLabel2=Action progress'
      'uHtmLabel3=Full progress'
      'pvGridEditor1=Table'
      'pvGridEditor2=Chart'
      'pvGridEditor3=Method'
      'pvGridEditor4=Summa'
      'pvGridEditor5=Count'
      'pvGridEditor6=Average'
      'pvGridEditor7=Equal'
      'pvGridEditor8=Proportional'
      'pvGridPrint4=Print preview'
      'pvGridPrint5=Scale page'
      'pvGridPrint6=Scale print'
      'pvGridPrint7=Fit page'
      'pvGridPrint8=Fit width'
      'pvGridPrint9=Fit height'
      'pvGridPrint10=50%'
      'pvGridPrint11=100%'
      'pvGridPrint12=200%'
      'pvGridPrint13=400%'
      'PvCubeMeasureCType=Calculation type'
      'PvCubeMeasureType0=ctSumma'
      'PvCubeMeasureType1=ctCount'
      'PvCubeMeasureType2=ctAverage'
      'PvCubeMeasureType3=ctMax'
      'PvCubeMeasureType4=ctMin'
      'PvCubeMeasureType5=ctWAverage'
      'PvCubeMeasureType6=ctMedian'
      'PvCubeMeasureType7=ctVariance'
      'PvCubeMeasureType8=ctDeviation'
      'PvCubeMeasureType9=ctCoeffDeviation'
      'PvCubeMeasureType10=ct1stQuartile'
      'PvCubeMeasureType11=ct3rdQuartile'
      'PvCubeMeasureType12=ctInterQuartile'
      'PvCubeMeasureType13=ctQuartileDeviation'
      'PvCubeMeasureType14=ctCoeffQuartVariance'
      'PvCubeMeasureType15=ctSkewness'
      'PvCubeMeasureType16=ctKurtosis'
      'PvCubeMeasureType17=ctMeanAbsDeviation'
      'PvCubeMeasureType18=ctMeanStError'
      'PvCubeMeasureType19=ctRepeatableSet'
      'PvCubeMeasureType20=ctCustom'
      'dimNewGroupDefName=New hierarchy group'
      
        'PVChartIncorrectSerieType=You should to have at least 2 values f' +
        'or line-type series!'
      'pvGridPrintLocked=Process locked! Please wait....'
      'dimAutoFilter1=none'
      'dimAutoFilter2=value'
      'dimAutoFilter3=rank'
      'dimAutoFilter4=more'
      'dimAutoFilter5=less'
      'dimAutoFilter6=first'
      'dimAutoFilter7=last')
    Measures = <
      item
        AliasName = 'nValVenda'
        FieldName = 'nValor'
        CalcType = ctSumma
        DataType = ftBCD
        FormatString = '#,##0.00'
        DisplayName = 'Valor Venda'
      end
      item
        AliasName = 'nValCustoMedio'
        FieldName = 'nValCustoMedio'
        CalcType = ctSumma
        DataType = ftBCD
        FormatString = '#,##0.00'
        DisplayName = 'Custo M'#233'dio'
      end
      item
        AliasName = 'nValLucro'
        FieldName = 'nValLucro'
        CalcType = ctSumma
        DataType = ftBCD
        FormatString = '#,##0.00'
        DisplayName = 'Lucro Venda'
      end
      item
        AliasName = 'nPercLucroPourcentGroupByRow'
        FieldName = 'nValLucro'
        CalcType = ctCustom
        DataType = ftBCD
        FormatString = '#,##0.00%'
        DisplayName = '% Lucro'
      end
      item
        AliasName = 'nParticip'
        FieldName = 'nValLucro'
        CalcType = ctSumma
        DataType = ftBCD
        FormatString = '#,##0.00'
        DisplayName = '% Particip.'
      end
      item
        AliasName = 'nPercLucro2PourcentGroupByRow'
        FieldName = 'nValLucro'
        CalcType = ctSumma
        DataType = ftBCD
        FormatString = '########0.00##'
        DisplayName = 'Rank'
      end>
    CubeName = 'Cube1'
    OptimizeLevel = optNormal
    ExtendedMode = True
    AlwaysSaveCounts = True
    ExcludeZeros = True
    ClearTimeInfo = True
    Left = 333
    Top = 207
  end
  object PivotMap1: TPivotMap
    Cube = PivotCube1
    Columns = <
      item
        Name = 'Ano'
      end
      item
        Name = 'Mes'
      end>
    Rows = <
      item
        Name = 'cNmTerceiro'
      end
      item
        Name = 'cNmDepartamento'
      end
      item
        Name = 'cNmCategoria'
      end
      item
        Name = 'cNmSubCategoria'
      end
      item
        Name = 'cNmSegmento'
      end
      item
        Name = 'cNmMarca'
      end
      item
        Name = 'cNmLinha'
      end
      item
        Name = 'cNmClasseProduto'
      end
      item
        Name = 'cNmProduto'
      end
      item
        Name = 'cNmRamoAtividade'
      end>
    HideEmptyRows = True
    HideEmptyColumns = True
    TableMode = True
    DDirection = True
    AutoSaveMap = False
    AutoExpand = False
    Title = 'ER2Soft - Vendas por Clientes x Produtos'
    Left = 413
    Top = 271
  end
end
