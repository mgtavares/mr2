unit rPosicaoEstoqueLoja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  ER2Excel, DB, ADODB, DBCtrls, StdCtrls, Mask,ExcelXP,comObj, OleServer;

type
  TrptPosicaoEstoqueLoja = class(TfrmRelatorio_Padrao)
    Image2: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    edtLoja: TMaskEdit;
    edtDepartamento: TMaskEdit;
    edtCategoria: TMaskEdit;
    edtSubCategoria: TMaskEdit;
    edtSegmento: TMaskEdit;
    edtMarca: TMaskEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    edtGrupoProduto: TMaskEdit;
    DBEdit7: TDBEdit;
    edtLinha: TMaskEdit;
    edtColecao: TMaskEdit;
    edtClasseProduto: TMaskEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    qryDepartamento: TADOQuery;
    qryDepartamentocNmDepartamento: TStringField;
    qryDepartamentonCdDepartamento: TIntegerField;
    dsDepartamento: TDataSource;
    qryCategoria: TADOQuery;
    qryCategoriacNmCategoria: TStringField;
    qryCategorianCdCategoria: TAutoIncField;
    dsCategoria: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    dsSubCategoria: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentocNmSegmento: TStringField;
    qrySegmentonCdSegmento: TAutoIncField;
    dsSegmento: TDataSource;
    qryMarca: TADOQuery;
    qryMarcacNmMarca: TStringField;
    qryMarcanCdMarca: TIntegerField;
    dsMarca: TDataSource;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    dsGrupoProduto: TDataSource;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    dsLinha: TDataSource;
    qryColecao: TADOQuery;
    qryColecaonCdColecao: TIntegerField;
    qryColecaocNmColecao: TStringField;
    dsColecao: TDataSource;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TIntegerField;
    qryClasseProdutocNmClasseProduto: TStringField;
    dsClasseProduto: TDataSource;
    dsLoja: TDataSource;
    rgProdAtivo: TRadioGroup;
    qryResultado: TADOQuery;
    procedure edtLojaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDepartamentoExit(Sender: TObject);
    procedure edtDepartamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCategoriaExit(Sender: TObject);
    procedure edtCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSubCategoriaExit(Sender: TObject);
    procedure edtSubCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSegmentoExit(Sender: TObject);
    procedure edtSegmentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMarcaExit(Sender: TObject);
    procedure edtMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLinhaExit(Sender: TObject);
    procedure edtLinhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtColecaoExit(Sender: TObject);
    procedure edtColecaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClasseProdutoExit(Sender: TObject);
    procedure edtClasseProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoProdutoExit(Sender: TObject);
    procedure edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoEstoqueLoja: TrptPosicaoEstoqueLoja;

implementation

uses fLookup_Padrao, fMenu, Math;

{$R *.dfm}

procedure TrptPosicaoEstoqueLoja.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;

end;

procedure TrptPosicaoEstoqueLoja.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;
end;

procedure TrptPosicaoEstoqueLoja.edtDepartamentoExit(Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

  if not qryDepartamento.eof then
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;


end;

procedure TrptPosicaoEstoqueLoja.edtDepartamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(129);

        If (nPK > 0) then
        begin

            edtDepartamento.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

        end ;

    end ;

  end ;
  
end;

procedure TrptPosicaoEstoqueLoja.edtCategoriaExit(Sender: TObject);
begin
  inherited;
  qryCategoria.Close ;
  PosicionaQuery(qryCategoria, edtCategoria.Text) ;

  if not qryCategoria.eof then
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

  

end;

procedure TrptPosicaoEstoqueLoja.edtCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit2.Text = '') then
        begin
            MensagemAlerta('Selecione um departamento.') ;
            edtDepartamento.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(46,'Categoria.nCdDepartamento = ' + edtDepartamento.Text);

        If (nPK > 0) then
        begin

            edtCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCategoria, edtCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueLoja.edtSubCategoriaExit(Sender: TObject);
begin
  inherited;
  
  qrySubCategoria.Close ;
  PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

  if not qrySubCategoria.eof then
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
 
end;

procedure TrptPosicaoEstoqueLoja.edtSubCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma Categoria.') ;
            edtCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(47,'SubCategoria.nCdCategoria = ' + edtCategoria.Text);

        If (nPK > 0) then
        begin

            edtSubCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueLoja.edtSegmentoExit(Sender: TObject);
begin
  inherited;
  qrySegmento.Close ;
  PosicionaQuery(qrySegmento, edtSegmento.Text) ;
end;

procedure TrptPosicaoEstoqueLoja.edtSegmentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit4.Text = '') then
        begin
            MensagemAlerta('Selecione uma SubCategoria.') ;
            edtSubCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'Segmento.nCdSubCategoria = ' + edtSubCategoria.Text);

        If (nPK > 0) then
        begin

            edtSegmento.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySegmento, edtSegmento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueLoja.edtMarcaExit(Sender: TObject);
begin
  inherited;
  qryMarca.Close ;
  PosicionaQuery(qryMarca, edtMarca.Text) ;
end;

procedure TrptPosicaoEstoqueLoja.edtMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin

            edtMarca.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, edtMarca.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueLoja.edtLinhaExit(Sender: TObject);
begin
  inherited;
  qryLinha.Close;
  PosicionaQuery(qryLinha, edtLinha.Text) ;
  
end;

procedure TrptPosicaoEstoqueLoja.edtLinhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit6.Text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdMarca = ' + edtMarca.Text)
        else nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
            edtLinha.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueLoja.edtColecaoExit(Sender: TObject);
begin
  inherited;
  qryColecao.Close;
  PosicionaQuery(qryColecao, edtColecao.Text) ;end;

procedure TrptPosicaoEstoqueLoja.edtColecaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(52);

        If (nPK > 0) then
            edtColecao.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueLoja.edtClasseProdutoExit(Sender: TObject);
begin
  qryClasseProduto.Close;
  PosicionaQuery(qryClasseProduto, edtClasseProduto.Text) ;
end;

procedure TrptPosicaoEstoqueLoja.edtClasseProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
            edtClasseProduto.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueLoja.edtGrupoProdutoExit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
  
end;

procedure TrptPosicaoEstoqueLoja.edtGrupoProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin

            edtGrupoProduto.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;

        end ;

    end ;

  end ;

end;
procedure TrptPosicaoEstoqueLoja.FormShow(Sender: TObject);
begin
  inherited;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value           := frmMenu.nCdUsuarioLogado ;
end;

procedure TrptPosicaoEstoqueLoja.ToolButton1Click(Sender: TObject);
var
  cComando : string;
  cFiltro  : string;
begin

  if not (DBEdit1.Text = '') then
      cFiltro := 'Loja: ' + Trim(edtLoja.Text) + ' ' + Trim(qryLojacNmLoja.Value);

  if not (DBEdit2.Text = '') then
      cFiltro := cFiltro + ' Departamento: ' + Trim(edtDepartamento.Text) + ' ' + Trim(qryDepartamentocNmDepartamento.Value);

  if not (DBEdit3.Text = '') then
      cFiltro := cFiltro + ' Categoria: ' + Trim(edtCategoria.Text) + ' ' + Trim(qryCategoriacNmCategoria.Value);

  if not (DBEdit4.Text = '') then
      cFiltro := cFiltro + ' Sub Categoria: ' + Trim(edtSubCategoria.Text) + ' ' + Trim(qrySubCategoriacNmSubCategoria.Value);

  if not (DBEdit5.Text = '') then
      cFiltro := cFiltro + ' Segmento: '  + Trim(edtSegmento.Text) + ' ' + Trim(qrySegmentocNmSegmento.Value);

  if not (DBEdit6.Text = '') then
      cFiltro := cFiltro + ' Marca: '  + Trim(edtMarca.Text) + ' ' + Trim(qryMarcacNmMarca.Value);

  if not (DBEdit8.Text = '') then
      cFiltro := cFiltro + ' Linha: '  + Trim(edtLinha.Text) + ' ' + Trim(qryLinhacNmLinha.Value);

  if not (DBEdit9.Text = '') then
      cFiltro := cFiltro + ' Cole��o: '  + Trim(edtColecao.Text) + ' ' + Trim(qryColecaocNmColecao.Value);

  if not (DBEdit10.Text = '') then
      cFiltro := cFiltro + ' Classe: '  + Trim(edtClasseProduto.Text) + ' ' + Trim(qryClasseProdutocNmClasseProduto.Value);

  if not (DBEdit7.Text = '') then
      cFiltro := cFiltro + ' Grupo Produto: '  + Trim(edtGrupoProduto.Text) + ' ' + Trim(qryGrupoProdutocNmGrupoProduto.Value);

  frmMenu.mensagemUsuario('Exportando Planilha');

  try

      {-- Exporta excel dinamico --}
      qryResultado.Close;
      qryResultado.Parameters.ParamByName('nCdDepartamento').Value := IntToStr(frmMenu.ConvInteiro(edtDepartamento.Text));
      qryResultado.Parameters.ParamByName('nCdCategoria').Value := IntToStr(frmMenu.ConvInteiro(edtCategoria.Text)) ;
      qryResultado.Parameters.ParamByName('nCdSubCategoria').Value := IntToStr(frmMenu.ConvInteiro(edtSubCategoria.Text));
      qryResultado.Parameters.ParamByName('nCdSegmento').Value := IntToStr(frmMenu.ConvInteiro(edtSegmento.Text));
      qryResultado.Parameters.ParamByName('nCdMarca').Value := IntToStr(frmMenu.ConvInteiro(edtMarca.Text));
      qryResultado.Parameters.ParamByName('nCdLinha').Value := IntToStr(frmMenu.ConvInteiro(edtLinha.Text));
      qryResultado.Parameters.ParamByName('nCdColecao').Value := IntToStr(frmMenu.ConvInteiro(edtColecao.Text));
      qryResultado.Parameters.ParamByName('nCdGrupoProduto').Value := IntToStr(frmMenu.ConvInteiro(edtGrupoProduto.Text));
      qryResultado.Parameters.ParamByName('nCdClasseProduto').Value := IntToStr(frmMenu.ConvInteiro(edtClasseProduto.Text));
      qryResultado.Parameters.ParamByName('nCdEmpresaAtiva').Value := IntToStr(frmMenu.nCdEmpresaAtiva);
      qryResultado.Parameters.ParamByName('nCdLoja').Value := IntToStr(frmMenu.ConvInteiro(edtLoja.Text));
      qryResultado.Parameters.ParamByName('cFlgProdAtivo').Value := IntToStr(rgProdAtivo.ItemIndex);
      qryResultado.Open;

      frmMenu.ExportExcelDinamic('Rel. Posi��o Estoque - Loja',cFiltro,'',qryResultado);

  except
      MensagemErro('Erro ao exportar planilha.');
      raise;
  end;

  frmMenu.mensagemUsuario('');

end;

initialization
    RegisterClass(TrptPosicaoEstoqueLoja) ;
end.
