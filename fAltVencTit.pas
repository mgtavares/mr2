unit fAltVencTit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ImgList, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons;

type
  TfrmAltVencTit = class(TfrmCadastro_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitnCdGrupoEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitcFlgPrev: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceironCdStatus: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    Label21: TLabel;
    DBEdit21: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    qryTerceironCdMoeda: TIntegerField;
    dsMovimento: TDataSource;
    qryAux: TADOQuery;
    qryMasternCdTitulo: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdEspTit: TIntegerField;
    qryMastercNrTit: TStringField;
    qryMasteriParcela: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdCategFinanc: TIntegerField;
    qryMasternCdMoeda: TIntegerField;
    qryMastercSenso: TStringField;
    qryMasterdDtEmissao: TDateTimeField;
    qryMasterdDtReceb: TDateTimeField;
    qryMasterdDtVenc: TDateTimeField;
    qryMasterdDtLiq: TDateTimeField;
    qryMasterdDtCad: TDateTimeField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternValTit: TBCDField;
    qryMasternValLiq: TBCDField;
    qryMasternSaldoTit: TBCDField;
    qryMasternValJuro: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMastercObsTit: TMemoField;
    qryMasternCdSP: TIntegerField;
    qryMasternCdBancoPortador: TIntegerField;
    qryMasterdDtRemPortador: TDateTimeField;
    qryMasterdDtBloqTit: TDateTimeField;
    qryMastercObsBloqTit: TStringField;
    qryMasternCdUnidadeNegocio: TIntegerField;
    qryMasternCdCC: TIntegerField;
    qryMasterdDtContab: TDateTimeField;
    qryMasternCdDoctoFiscal: TIntegerField;
    qryMasterdDtCalcJuro: TDateTimeField;
    qryMasternCdRecebimento: TIntegerField;
    qryMasternCdCrediario: TIntegerField;
    qryMasternCdTransacaoCartao: TIntegerField;
    qryMasternCdLanctoFin: TIntegerField;
    qryMastercFlgIntegrado: TIntegerField;
    dsEspTit: TDataSource;
    dsTerceiro: TDataSource;
    dsEmpresa: TDataSource;
    Label7: TLabel;
    DBMemo1: TDBMemo;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    Label10: TLabel;
    DBEdit8: TDBEdit;
    qryMastercNrNF: TStringField;
    qryMasternCdLojaTit: TIntegerField;
    Label11: TLabel;
    DBEdit10: TDBEdit;
    Label13: TLabel;
    DBEdit11: TDBEdit;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit13: TDBEdit;
    DataSource1: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryTerceiroAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBEdit34KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit22KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAltVencTit: TfrmAltVencTit;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmAltVencTit.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TITULO' ;
  nCdTabelaSistema  := 21 ;
  nCdConsultaPadrao := 36 ;
end;

procedure TfrmAltVencTit.btSalvarClick(Sender: TObject);
var
    qryAux : TADOQuery ;
begin

  if not qryMaster.Active or (qryMasternCdTitulo.Value = 0) then
  begin
      MensagemAlerta('Nenhum t�tulo ativo para salvar.') ;
      exit ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin

      if (qryMasterdDtCancel.AsString <> '') then
      begin
          MensagemAlerta('T�tulo cancelado n�o pode ser alterado.') ;
          exit ;
      end ;

      if (qryMasterdDtLiq.AsString <> '') then
      begin
          MensagemAlerta('T�tulo liquidado n�o pode ser alterado.') ;
          exit ;
      end ;

  end ;

  inherited;

end;

procedure TfrmAltVencTit.qryTerceiroAfterScroll(DataSet: TDataSet);
begin
  inherited;
//  if (qryMaster.State = dsInsert) then
//      qryMasternCdMoeda.Value := qryTerceironCdMoeda.Value ;

end;

procedure TfrmAltVencTit.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;

  If (qryMasterdDtCancel.AsString <> '') then
  begin
    qryMaster.Close ;
    MensagemAlerta('T�tulo cancelado, imposs�vel alterar.') ;
    btCancelar.Click;
    exit ;
  end;

  if (qryMasterdDtLiq.AsString <> '') then
  begin
      qryMaster.Close ;
      MensagemAlerta('T�tulo liquidado n�o pode ser alterado.') ;
      btCancelar.Click;
      exit ;
  end ;

  if (qryMasternCdCrediario.AsString <> '') then
  begin
      qryMaster.Close ;
      MensagemAlerta('T�tulo gerado pelo credi�rio n�o pode ser alterado.') ;
      btCancelar.Click;
      exit ;
  end ;

  PosicionaQuery(qryEspTit, qryMasternCdEspTit.AsString) ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.AsString) ;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  PosicionaQuery(qryLoja, qryMasternCdLojaTit.asString) ;
  
end;

procedure TfrmAltVencTit.DBEdit34KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(35,'EXISTS(SELECT 1 FROM EmpresaUnidadeNegocio EUN WHERE EUN.nCdUnidadeNegocio = UnidadeNegocio.nCdUnidadeNegocio AND EUN.nCdEmpresa = ' + DBEdit2.Text + ')');

            If (nPK > 0) then
            begin
                qryMasternCdUnidadeNegocio.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmAltVencTit.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(38);

            If (nPK > 0) then
            begin
                qryMasternCdEspTit.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmAltVencTit.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmAltVencTit.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(13,'cFlgRecDes = ' + Chr(39) + 'R' + Chr(39));

            If (nPK > 0) then
            begin
                qryMasternCdCategFinanc.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmAltVencTit.DBEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(16);

            If (nPK > 0) then
            begin
                qryMasternCdMoeda.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmAltVencTit.DBEdit22KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(23);

            If (nPK > 0) then
            begin
                qryMasternCdBancoPortador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmAltVencTit.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTerceiro.Close ;
  qryEspTit.Close ;
  qryLoja.Close;
  
end;

procedure TfrmAltVencTit.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryEmpresa.Close ;
  qryTerceiro.Close ;
  qryEspTit.Close ;
  qryLoja.Close;

end;

initialization
    RegisterClass(TfrmAltVencTit) ;


end.
