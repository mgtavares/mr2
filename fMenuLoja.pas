unit fMenuLoja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Menus, ImgList, ComCtrls, ExtCtrls, jpeg;

type
  TfrmMenuLoja = class(TForm)
    MainMenu1: TMainMenu;
    Venda: TMenuItem;
    Caixa: TMenuItem;
    Credirio1: TMenuItem;
    Gerente1: TMenuItem;
    Relatrios1: TMenuItem;
    Relatrios2: TMenuItem;
    itemPreVenda: TMenuItem;
    itemConsultaVenda: TMenuItem;
    itemCaixa: TMenuItem;
    ItemRemessaNum: TMenuItem;
    Credirio2: TMenuItem;
    itemCliente: TMenuItem;
    itemAprovaCliente: TMenuItem;
    itemBloqueiaCliente: TMenuItem;
    itemLimCreditoAutomatico: TMenuItem;
    itemLimCreditoManual: TMenuItem;
    ImageList1: TImageList;
    ItemConsultaLanctoCofre: TMenuItem;
    itemExtratoCofre: TMenuItem;
    itemLanctoManCofre: TMenuItem;
    itemGerenciaCofre: TMenuItem;
    Numerrios1: TMenuItem;
    itemRecebNum: TMenuItem;
    DataAnalysis1: TMenuItem;
    itemAnaliseVenda: TMenuItem;
    itemRelPreVendaPend: TMenuItem;
    itemRelVendaCrediario: TMenuItem;
    itemRelVendaEstruturaMarca: TMenuItem;
    N1: TMenuItem;
    itemRelPosicaoVale: TMenuItem;
    N2: TMenuItem;
    itemRelPrecoAlterado: TMenuItem;
    N3: TMenuItem;
    itemTipoBloqueioCliente: TMenuItem;
    N5: TMenuItem;
    itemCadastroCaixa: TMenuItem;
    N6: TMenuItem;
    itemCondPagto: TMenuItem;
    StatusBar1: TStatusBar;
    Timer1: TTimer;
    Image1: TImage;
    Sair1: TMenuItem;
    Logoff1: TMenuItem;
    Sair2: TMenuItem;
    N7: TMenuItem;
    ConfiguraoAmbiente1: TMenuItem;
    imgFundoMenu: TImage;
    itemEncerraMovtoPOS: TMenuItem;
    N4: TMenuItem;
    itemResumoLoja: TMenuItem;
    itemRegEntrada: TMenuItem;
    itemConsignacao: TMenuItem;
    Utilitrios1: TMenuItem;
    itemManutencaoPedidoVenda: TMenuItem;
    itemClienteSimples: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    SobreoER2Loja1: TMenuItem;
    btFeedback: TMenuItem;
    btHistVersao: TMenuItem;
    Ajuda1: TMenuItem;
    btGuiaOperacional: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    btSuporteInt: TMenuItem;
    itemAcompMetaDesc: TMenuItem;
    procedure itemPreVendaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure itemConsultaVendaClick(Sender: TObject);
    procedure itemCaixaClick(Sender: TObject);
    procedure ItemRemessaNumClick(Sender: TObject);
    procedure itemClienteClick(Sender: TObject);
    procedure itemAprovaClienteClick(Sender: TObject);
    procedure itemBloqueiaClienteClick(Sender: TObject);
    procedure itemLimCreditoAutomaticoClick(Sender: TObject);
    procedure itemLimCreditoManualClick(Sender: TObject);
    procedure itemRelPreVendaPendClick(Sender: TObject);
    procedure itemRelVendaCrediarioClick(Sender: TObject);
    procedure itemRelVendaEstruturaMarcaClick(Sender: TObject);
    procedure itemRelPosicaoValeClick(Sender: TObject);
    procedure itemRelPrecoAlteradoClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Sair2Click(Sender: TObject);
    procedure Logoff1Click(Sender: TObject);
    procedure itemTipoBloqueioClienteClick(Sender: TObject);
    procedure ItemConsultaLanctoCofreClick(Sender: TObject);
    procedure itemExtratoCofreClick(Sender: TObject);
    procedure itemLanctoManCofreClick(Sender: TObject);
    procedure itemGerenciaCofreClick(Sender: TObject);
    procedure itemCadastroCaixaClick(Sender: TObject);
    procedure itemRecebNumClick(Sender: TObject);
    procedure itemAnaliseVendaClick(Sender: TObject);
    procedure itemCondPagtoClick(Sender: TObject);
    procedure ConfiguraoAmbiente1Click(Sender: TObject);
    procedure verificaPermissaoAcesso();
    procedure atualizaStatusBar();
    procedure itemEncerraMovtoPOSClick(Sender: TObject);
    procedure itemResumoLojaClick(Sender: TObject);
    procedure itemRegEntradaClick(Sender: TObject);
    procedure itemConsignacaoClick(Sender: TObject);
    procedure itemManutencaoPedidoVendaClick(Sender: TObject);
    procedure itemClienteSimplesClick(Sender: TObject);
    procedure SobreoER2Loja1Click(Sender: TObject);
    procedure btFeedbackClick(Sender: TObject);
    procedure btHistVersaoClick(Sender: TObject);
    procedure btGuiaOperacionalClick(Sender: TObject);
    procedure btSuporteIntClick(Sender: TObject);
    procedure itemAcompMetaDescClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMenuLoja: TfrmMenuLoja;
  formItemMenu : TForm;

implementation

uses fPdv, fMenu, fSelecServer, fLogin, fConsultaVendaProduto,
  fCaixa_Recebimento, fRemessa_Numerario, fClienteVarejoPessoaFisica,
  fClienteVarejoPessoaFisica_Simples, fAprovacaoClienteVarejo, fManutBloqueioCliente,
  fManutLimiteCliente, fManutLimiteClienteManual, rPreVendaPendente, rVendaCrediario,
  rVendaEstruturaMarcaLoja, rPosicaoVale, rAlteracaoPrecoProduto,
  fTipoBloqueioCliente, fConsultaMovtoCaixa, rExtratoContaBancaria,
  fLancManConta, fExploraCaixaCofre, fContaCaixaCofre,
  fConfirma_Numerarios, dcVendaLoja, fCondPagtoPDV, fConfigAmbiente,
  fEncerrarMovtoPOS, rResumoMovimentoLoja, fRegistroEntradaFuncionario,
  fConsignacao, fManutencaoPedidoVenda, fSobre, fFeedback, fHistoricoVersoes,
  fGuiaOperacional, fAcompanhaMetaDescVendedor;

{$R *.dfm}

procedure TfrmMenuLoja.itemPreVendaClick(Sender: TObject);
begin

   formItemMenu := TfrmPDV.Create(nil) ;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.FormShow(Sender: TObject);
begin


    if FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoFundo.JPG') then
        imgFundoMenu.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoFundo.JPG')
    else if FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoFundo.JPEG') then
        imgFundoMenu.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoFundo.JPEG')
    else if FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoFundo.GIF') then
        imgFundoMenu.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoFundo.GIF')
    else if FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoFundo.BMP') then
        imgFundoMenu.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoFundo.BMP') ;

    Application.ProcessMessages;

    if (frmMenu.nCdUsuarioLogado = 0) then
    begin
        frmSelecServer.ShowModal; //Modal ;

        frmLogin.ShowModal ;
        (Sender as TForm).Repaint;

        frmMenu.configuraAmbiente(frmMenu.cNomeComputador);
        frmMenu.SelecionaEmpresa() ;

        frmMenu.Timer2.Enabled := True ;

    end ;

    frmMenu.cNmEmpresaCupom  := frmMenu.LeParametro('NMEMCUPOMPDV') ;
    frmMenu.cNmEmpresaCupom2 := frmMenu.LeParametro('NMEMCUPOMPDV1') ;
    frmMenu.iCasaDecimal     := StrToint(frmMenu.LeParametro('CASADECCOMPRA')) ;
    frmMenu.cFlgLogAcesso    := frmMenu.LeParametro('GERALOGACESSO') ;
    frmMenu.cMascaraCompras  := frmMenu.MascaraDecimal() ;

    frmMenuLoja.Caption := frmMenuLoja.Caption + '   ' + frmMenu.cNmEmpresaAtiva + ' - ' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdLojaAtiva),3) + '-' + frmMenu.cNmLojaAtiva;

    verificaPermissaoAcesso();
    atualizaStatusBar();

end;


procedure TfrmMenuLoja.itemConsultaVendaClick(
  Sender: TObject);
begin

   formItemMenu := TfrmConsultaVendaProduto.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemAcompMetaDescClick(Sender: TObject);
begin
   formItemMenu := TfrmAcompanhaMetaDescVendedor.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;
end;

procedure TfrmMenuLoja.itemCaixaClick(Sender: TObject);
begin

   formItemMenu := TfrmCaixa_recebimento.Create(nil) ;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.ItemRemessaNumClick(Sender: TObject);
begin

   formItemMenu := TfrmRemessa_Numerarios.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemClienteClick(Sender: TObject);
begin

   formItemMenu := TfrmClienteVarejoPessoaFisica.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemClienteSimplesClick(Sender: TObject);
begin

   formItemMenu := TfrmClienteVarejoPessoaFisica_Simples.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemAprovaClienteClick(Sender: TObject);
begin

   formItemMenu := TfrmAprovacaoClienteVarejo.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemBloqueiaClienteClick(Sender: TObject);
begin

   formItemMenu := TfrmManutBloqueioCliente.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemLimCreditoAutomaticoClick(Sender: TObject);
begin

   formItemMenu := TfrmManutLimiteCliente.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemLimCreditoManualClick(Sender: TObject);
begin

   formItemMenu := TfrmManutLimiteClienteManual.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemRelPreVendaPendClick(Sender: TObject);
begin

   formItemMenu := TrptPreVendaPendente.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;


end;

procedure TfrmMenuLoja.itemRelVendaCrediarioClick(Sender: TObject);
begin

   formItemMenu := TrptVendaCrediario.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemRelVendaEstruturaMarcaClick(Sender: TObject);
begin

   formItemMenu := TrptVendaEstruturaMarcaLoja.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemRelPosicaoValeClick(Sender: TObject);
begin

   formItemMenu := TrptPosicaoVale.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemRelPrecoAlteradoClick(Sender: TObject);
begin

   formItemMenu := TrptAlteracaoPrecoProduto.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.Timer1Timer(Sender: TObject);
begin
    StatusBar1.Panels[3].Text := DateTimeToStr(Now) ;

end;

procedure TfrmMenuLoja.Sair2Click(Sender: TObject);
begin

    Application.Terminate ;

end;

procedure TfrmMenuLoja.Logoff1Click(Sender: TObject);
begin

    frmSelecServer.ShowModal; //Modal ;

    frmLogin.EdtLogin.Text := '' ;
    frmLogin.edtSenha.Text := '' ;
    frmLogin.ShowModal ;

    frmMenu.SelecionaEmpresa() ;

    verificaPermissaoAcesso();
    atualizaStatusBar();

end;

procedure TfrmMenuLoja.itemTipoBloqueioClienteClick(Sender: TObject);
begin

   formItemMenu := TfrmTipoBloqueioCliente.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.ItemConsultaLanctoCofreClick(Sender: TObject);
begin

   formItemMenu := TfrmConsultaMovtoCaixa.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemExtratoCofreClick(Sender: TObject);
begin

   formItemMenu := TrptExtratoContaBancaria.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemLanctoManCofreClick(Sender: TObject);
begin

   formItemMenu := TfrmLancManConta.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemGerenciaCofreClick(Sender: TObject);
begin

   formItemMenu := TfrmExploraCaixaCofre.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemCadastroCaixaClick(Sender: TObject);
begin

   formItemMenu := TfrmContaCaixaCofre.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemRecebNumClick(Sender: TObject);
begin

   formItemMenu := TfrmConfirma_Numerarios.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemAnaliseVendaClick(Sender: TObject);
begin

   formItemMenu := TdcmVendaLoja.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

    
end;

procedure TfrmMenuLoja.itemCondPagtoClick(Sender: TObject);
begin

   formItemMenu := TfrmCondPagtoPDV.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.ConfiguraoAmbiente1Click(Sender: TObject);
begin

   formItemMenu := TfrmConfigAmbiente.Create(nil) ;
   //formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.verificaPermissaoAcesso;
begin

    {--desativa todos os itens do menu--}
    itemPreVenda.Enabled               := False ;
    itemConsultaVenda.Enabled          := False ;
    itemAcompMetaDesc.Enabled          := False ;
    itemCaixa.Enabled                  := False ;
    ItemRemessaNum.Enabled             := False ;
    itemCliente.Enabled                := False ;
    itemClienteSimples.Enabled         := False ;
    itemAprovaCliente.Enabled          := False ;
    itemBloqueiaCliente.Enabled        := False ;
    itemLimCreditoAutomatico.Enabled   := False ;
    itemLimCreditoManual.Enabled       := False ;
    ItemConsultaLanctoCofre.Enabled    := False ;
    itemExtratoCofre.Enabled           := False ;
    itemLanctoManCofre.Enabled         := False ;
    itemGerenciaCofre.Enabled          := False ;
    itemRecebNum.Enabled               := False ;
    itemAnaliseVenda.Enabled           := False ;
    itemRelPreVendaPend.Enabled        := False ;
    itemRelVendaCrediario.Enabled      := False ;
    itemRelVendaEstruturaMarca.Enabled := False ;
    itemRelPosicaoVale.Enabled         := False ;
    itemRelPrecoAlterado.Enabled       := False ;
    itemTipoBloqueioCliente.Enabled    := False ;
    itemCadastroCaixa.Enabled          := False ;
    itemCondPagto.Enabled              := False ;
    itemEncerraMovtoPOS.Enabled        := False ;
    itemResumoLoja.Enabled             := False ;
    itemConsignacao.Visible            := False ;
    itemRegEntrada.Visible             := False ;
    itemManutencaoPedidoVenda.Enabled  := False ;

    { --verifica as permiss�es de acesso do usu�rio -- }
    frmMenu.usp_PreparaMenu.Close ;
    frmMenu.usp_PreparaMenu.Parameters.ParamByName('@nCdUsuario').Value   := frmMenu.nCdUsuarioLogado ;
    frmMenu.usp_PreparaMenu.Parameters.ParamByName('@cNmAplicacao').Value := ''; //informa filtro vazio devido n�o existir campo de busca no m�dulo loja
    frmMenu.usp_PreparaMenu.Open ;

    frmMenu.usp_PreparaMenu.First;

    if (frmMenu.usp_PreparaMenu.Eof) then
        frmMenu.MensagemErro('Nenhum grupo de usu�rio ou previl�gio de acesso concedido.') ;

    frmMenu.usp_PreparaMenu.First ;

    while not frmMenu.usp_PreparaMenu.Eof do
    begin

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('MODAL_FRMPDV')) then
            itemPreVenda.Enabled               := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('frmConsultaVendaProduto')) then
            itemConsultaVenda.Enabled          := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('frmAcompanhaMetaDescVendedor')) then
            itemAcompMetaDesc.Enabled          := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('MODAL_FRMCAIXA_RECEBIMENTO')) then
            itemCaixa.Enabled                  := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMREMESSA_NUMERARIOS')) then
            ItemRemessaNum.Enabled             := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMCLIENTEVAREJOPESSOAFISICA')) then
            itemCliente.Enabled                := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMCLIENTEVAREJOPESSOAFISICA_SIMPLES')) then
            itemClienteSimples.Enabled         := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMAPROVACAOCLIENTEVAREJO')) then
            itemAprovaCliente.Enabled          := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMMANUTBLOQUEIOCLIENTE')) then
            itemBloqueiaCliente.Enabled        := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMMANUTLIMITECLIENTE')) then
            itemLimCreditoAutomatico.Enabled   := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMMANUTLIMITECLIENTEMANUAL')) then
            itemLimCreditoManual.Enabled       := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMCONSULTAMOVTOCAIXA')) then
            ItemConsultaLanctoCofre.Enabled    := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('RPTEXTRATOCONTABANCARIA')) then
            itemExtratoCofre.Enabled           := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMLANCMANCONTA')) then
            itemLanctoManCofre.Enabled         := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMEXPLORACAIXACOFRE')) then
            itemGerenciaCofre.Enabled          := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMCONFIRMA_NUMERARIOS')) then
            itemRecebNum.Enabled               := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('DCMVENDALOJA')) then
            itemAnaliseVenda.Enabled           := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('RPTPREVENDAPENDENTE')) then
            itemRelPreVendaPend.Enabled        := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('RPTVENDACREDIARIO')) then
            itemRelVendaCrediario.Enabled      := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('RPTVENDAESTRUTURAMARCALOJA')) then
            itemRelVendaEstruturaMarca.Enabled := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('RPTPOSICAOVALE')) then
            itemRelPosicaoVale.Enabled         := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('RPTALTERACAOPRECOPRODUTO')) then
            itemRelPrecoAlterado.Enabled       := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMTIPOBLOQUEIOCLIENTE')) then
            itemTipoBloqueioCliente.Enabled    := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMCONTACAIXACOFRE')) then
            itemCadastroCaixa.Enabled          := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMCONDPAGTOPDV')) then
            itemCondPagto.Enabled              := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMENCERRARMOVTOPOS')) then
            itemEncerraMovtoPOS.Enabled        := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('RPTRESUMOMOVIMENTOLOJA')) then
            itemResumoLoja.Enabled             := True ;

        if (frmMenu.usp_PreparaMenucNmObjeto.Value = Uppercase('FRMMANUTENCAOPEDIDOVENDA')) then
            itemManutencaoPedidoVenda.Enabled  := True ;

        frmMenu.usp_PreparaMenu.Next ;
    end ;

    frmMenu.usp_PreparaMenu.Close ;

    itemConsignacao.Visible := (frmMenu.LeParametro('USACONSIGNCAO') = 'S') ;
    itemRegEntrada.Visible  := (frmMenu.LeParametro('USAREGENTSAIDA') = 'S') ;
end;

procedure TfrmMenuLoja.atualizaStatusBar;
begin
    if (frmMenu.LeParametro('VAREJO') = 'S') then
        StatusBar1.Panels[0].Text := frmMenu.cNmVersaoSistema + '-Varejo'
    else
        StatusBar1.Panels[0].Text := frmMenu.cNmVersaoSistema + '-ERP';

    StatusBar1.Panels[1].Text := frmMenu.cNmLojaAtiva ;
    StatusBar1.Panels[2].Text := frmMenu.cNmUsuarioLogado ;
    StatusBar1.Panels[4].Text := ('Servidor : ' + frmLogin.EdtServidor.Text);
end;

procedure TfrmMenuLoja.itemEncerraMovtoPOSClick(Sender: TObject);
begin

   formItemMenu := TfrmEncerrarMovtoPOS.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemResumoLojaClick(Sender: TObject);
begin

   formItemMenu := TrptResumoMovimentoLoja.Create(nil) ;
   formItemMenu.WindowState := wsMaximized;
   try
     formItemMenu.ShowModal;
   finally
     formItemMenu.Free;
   end;

end;

procedure TfrmMenuLoja.itemRegEntradaClick(Sender: TObject);
var
   objForm : TfrmRegistroEntradaFuncionario;
begin
   objForm := TfrmRegistroEntradaFuncionario.Create(nil);
   objForm.ShowModal;
   FreeAndNil(objForm);
end;

procedure TfrmMenuLoja.itemConsignacaoClick(Sender: TObject);
var
    objForm : TFrmConsignacao;
begin

    objForm := TFrmConsignacao.Create(nil);
    frmMenu.showForm(objForm,true);
end;

procedure TfrmMenuLoja.itemManutencaoPedidoVendaClick(Sender: TObject);
var
    objForm : TfrmManutencaoPedidoVenda;
begin

    objForm := TfrmManutencaoPedidoVenda.Create(nil) ;
    frmMenu.showForm(objForm,true);
end;

procedure TfrmMenuLoja.SobreoER2Loja1Click(Sender: TObject);
var objSobre : TfrmSobre;
begin
  try
      objSobre := TfrmSobre.Create(Nil);

      objSobre.ShowModal;
  finally
      FreeAndNil(objSobre);
  end;
end;

procedure TfrmMenuLoja.btFeedbackClick(Sender: TObject);
var
  objFeedback : TfrmFeedback;
begin
  objFeedback := TfrmFeedback.Create(Nil);

  frmMenu.showForm(objFeedback, True);
end;

procedure TfrmMenuLoja.btHistVersaoClick(Sender: TObject);
var
  objHistoricoVersoes : TfrmHistoricoVersoes;
begin
  objHistoricoVersoes := TfrmHistoricoVersoes.Create(Nil);

  frmMenu.showForm(objHistoricoVersoes, True);
end;

procedure TfrmMenuLoja.btGuiaOperacionalClick(Sender: TObject);
var
  objGuiaOper : TfrmGuiaOperacional;
begin
  objGuiaOper := TfrmGuiaOperacional.Create(Nil);

  frmMenu.showForm(objGuiaOper, True);
end;

procedure TfrmMenuLoja.btSuporteIntClick(Sender: TObject);
begin
  frmMenu.btSuporteInt.Click;
end;

end.
