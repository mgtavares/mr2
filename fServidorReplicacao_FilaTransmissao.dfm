inherited frmServidorReplicacao_FilaTransmissao: TfrmServidorReplicacao_FilaTransmissao
  Left = 200
  Top = 46
  Width = 1152
  Height = 666
  Caption = 'Fila Transmiss'#227'o'
  OldCreateOrder = True
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1136
    Height = 599
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 599
    ActivePage = cxTabSheet2
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 595
    ClientRectLeft = 4
    ClientRectRight = 1132
    ClientRectTop = 24
    object cxTabSheet4: TcxTabSheet
      Caption = 'Registros Pendentes - Por Servidor Destino'
      ImageIndex = 3
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 1128
        Height = 571
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView3: TcxGridDBTableView
          PopupMenu = PopupMenu4
          DataController.DataSource = dsPendentesServidor
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#'
              Kind = skCount
            end
            item
              Format = ',0'
              Kind = skSum
              Column = cxGridDBTableView3iQtdePendente
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView3nCdServidor: TcxGridDBColumn
            Caption = 'C'#243'd. Servidor'
            DataBinding.FieldName = 'nCdServidor'
            Width = 108
          end
          object cxGridDBTableView3cNmServidor: TcxGridDBColumn
            Caption = 'Servidor Destino'
            DataBinding.FieldName = 'cNmServidor'
          end
          object cxGridDBTableView3iQtdePendente: TcxGridDBColumn
            Caption = 'Registros Pendentes'
            DataBinding.FieldName = 'iQtdePendente'
            Width = 141
          end
        end
        object cxGridLevel3: TcxGridLevel
          GridView = cxGridDBTableView3
        end
      end
    end
    object cxTabSheet5: TcxTabSheet
      Caption = 'Registros Pendentes - Por Tabela'
      ImageIndex = 4
      object cxGrid4: TcxGrid
        Left = 0
        Top = 0
        Width = 1128
        Height = 571
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView4: TcxGridDBTableView
          PopupMenu = PopupMenu5
          DataController.DataSource = dsPendentesTabela
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#'
              Kind = skCount
            end
            item
              Format = ',0'
              Kind = skSum
              Column = cxGridDBTableView4iQtdePendente
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView4nCdTabelaSistema: TcxGridDBColumn
            Caption = 'C'#243'd.'
            DataBinding.FieldName = 'nCdTabelaSistema'
          end
          object cxGridDBTableView4cTabela: TcxGridDBColumn
            Caption = 'Tabela'
            DataBinding.FieldName = 'cTabela'
            Width = 202
          end
          object cxGridDBTableView4iQtdePendente: TcxGridDBColumn
            Caption = 'Registros Pendentes'
            DataBinding.FieldName = 'iQtdePendente'
            Width = 141
          end
          object cxGridDBTableView4cNmTabela: TcxGridDBColumn
            Caption = 'Descri'#231#227'o Tabela'
            DataBinding.FieldName = 'cNmTabela'
            Width = 538
          end
        end
        object cxGridLevel4: TcxGridLevel
          GridView = cxGridDBTableView4
        end
      end
    end
    object cxTabSheet1: TcxTabSheet
      Caption = 'Registros Pendentes - Por Registro'
      ImageIndex = 0
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 1128
        Height = 571
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView1: TcxGridDBTableView
          PopupMenu = PopupMenu1
          DataController.DataSource = dsTempRegistro
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#'
              Kind = skCount
              Column = cxGridDBTableView1nCdTempRegistro
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView1nCdTempRegistro: TcxGridDBColumn
            Caption = 'ID'
            DataBinding.FieldName = 'nCdTempRegistro'
          end
          object cxGridDBTableView1dDtInclusao: TcxGridDBColumn
            Caption = 'Dt. Inclus'#227'o'
            DataBinding.FieldName = 'dDtInclusao'
            Width = 177
          end
          object cxGridDBTableView1cTabela: TcxGridDBColumn
            Caption = 'Tabela'
            DataBinding.FieldName = 'cTabela'
            Width = 218
          end
          object cxGridDBTableView1iIDRegistro: TcxGridDBColumn
            Caption = 'C'#243'digo Registro'
            DataBinding.FieldName = 'iIDRegistro'
            Width = 122
          end
          object cxGridDBTableView1cFlgAcao: TcxGridDBColumn
            Caption = 'A'#231#227'o'
            DataBinding.FieldName = 'cFlgAcao'
            Width = 135
          end
          object cxGridDBTableView1cNmServidor: TcxGridDBColumn
            Caption = 'Servidor Destino'
            DataBinding.FieldName = 'cNmServidor'
            Width = 270
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Erro de Transmiss'#227'o'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 1128
        Height = 571
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsErros
        DrawMemoText = True
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        PopupMenu = PopupMenu2
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTempRegistro'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'dDtInclusao'
            Footers = <>
            Tag = 2
            Width = 177
          end
          item
            EditButtons = <>
            FieldName = 'cTabela'
            Footers = <>
            Width = 200
          end
          item
            EditButtons = <>
            FieldName = 'iIDRegistro'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cFlgAcao'
            Footers = <>
            Width = 79
          end
          item
            EditButtons = <>
            FieldName = 'cNmServidor'
            Footers = <>
            Width = 200
          end
          item
            EditButtons = <>
            FieldName = 'dDtUltTentativa'
            Footers = <>
            Width = 114
          end
          item
            EditButtons = <>
            FieldName = 'iQtdeTentativas'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cMsgErro'
            Footers = <>
            Width = 221
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Registros Descartados'
      ImageIndex = 2
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1128
        Height = 571
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        PopupMenu = PopupMenu3
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView2: TcxGridDBTableView
          PopupMenu = PopupMenu3
          DataController.DataSource = dsDescartados
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#'
              Kind = skCount
            end
            item
              Format = '#'
              Kind = skCount
              Column = cxGridDBTableView2nCdTempRegistro
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView2nCdTempRegistro: TcxGridDBColumn
            Caption = 'ID'
            DataBinding.FieldName = 'nCdTempRegistro'
          end
          object cxGridDBTableView2cTabela: TcxGridDBColumn
            Caption = 'Tabela'
            DataBinding.FieldName = 'cTabela'
            Width = 235
          end
          object cxGridDBTableView2iIDRegistro: TcxGridDBColumn
            Caption = 'C'#243'digo Registro'
            DataBinding.FieldName = 'iIDRegistro'
          end
          object cxGridDBTableView2cFlgAcao: TcxGridDBColumn
            Caption = 'A'#231#227'o'
            DataBinding.FieldName = 'cFlgAcao'
          end
          object cxGridDBTableView2cNmServidor: TcxGridDBColumn
            Caption = 'Servidor Destino'
            DataBinding.FieldName = 'cNmServidor'
            Width = 178
          end
          object cxGridDBTableView2dDtUltTentativa: TcxGridDBColumn
            Caption = #218'lt. Tentativa'
            DataBinding.FieldName = 'dDtUltTentativa'
          end
          object cxGridDBTableView2iQtdeTentativas: TcxGridDBColumn
            Caption = 'Tentativas'
            DataBinding.FieldName = 'iQtdeTentativas'
          end
          object cxGridDBTableView2cMsgErro: TcxGridDBColumn
            Caption = 'Mensagem Erro'
            DataBinding.FieldName = 'cMsgErro'
            Width = 242
          end
          object cxGridDBTableView2dDtInclusao: TcxGridDBColumn
            Caption = 'Dt. Inclus'#227'o'
            DataBinding.FieldName = 'dDtInclusao'
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 264
    Top = 184
  end
  object qryTempRegistro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdTempRegistro'
      '      ,cTabela'
      '      ,iIDRegistro'
      '      ,CASE WHEN cFlgAcao = '#39'I'#39' THEN '#39'Inclus'#227'o'#39
      '            ELSE '#39'Exclus'#227'o'#39
      '       END cFlgAcao'
      '      ,Servidor.cNmServidor'
      ',TempRegistro.dDtInclusao'
      '  FROM TempRegistro'
      
        '       INNER JOIN TabelaSistema ON TabelaSistema.nCdTabelaSistem' +
        'a = TempRegistro.nCdTabelaSistema'
      
        '       INNER JOIN Servidor      ON Servidor.nCdServidor         ' +
        '  = TempRegistro.nCdServidorDestino'
      ' WHERE ((    cFlgIntegrado  = 0'
      '        AND cFlgBroken     = 0'
      '        AND cFlgObsoleto   = 0)'
      '    OR cFlgReprocessa = 1)'
      ' ORDER BY dDtInclusao')
    Left = 192
    Top = 264
    object qryTempRegistronCdTempRegistro: TAutoIncField
      DisplayLabel = 'Registros Pendentes de Transmiss'#227'o|C'#243'd'
      FieldName = 'nCdTempRegistro'
      ReadOnly = True
    end
    object qryTempRegistrocTabela: TStringField
      DisplayLabel = 'Registros Pendentes de Transmiss'#227'o|Tabela'
      FieldName = 'cTabela'
      Size = 50
    end
    object qryTempRegistroiIDRegistro: TLargeintField
      DisplayLabel = 'Registros Pendentes de Transmiss'#227'o|ID Registro'
      FieldName = 'iIDRegistro'
    end
    object qryTempRegistrocFlgAcao: TStringField
      DisplayLabel = 'Registros Pendentes de Transmiss'#227'o|A'#231#227'o'
      FieldName = 'cFlgAcao'
      ReadOnly = True
      Size = 8
    end
    object qryTempRegistrocNmServidor: TStringField
      DisplayLabel = 'Registros Pendentes de Transmiss'#227'o|Servidor Destino'
      FieldName = 'cNmServidor'
      Size = 50
    end
    object qryTempRegistrodDtInclusao: TDateTimeField
      DisplayLabel = 'Registros Pendentes de Transmiss'#227'o|Dt. Inclus'#227'o'
      FieldName = 'dDtInclusao'
    end
  end
  object dsTempRegistro: TDataSource
    DataSet = qryTempRegistro
    Left = 232
    Top = 264
  end
  object qryErros: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT nCdTempRegistro'
      '      ,cTabela'
      '      ,iIDRegistro'
      '      ,CASE WHEN cFlgAcao = '#39'I'#39' THEN '#39'Inclus'#227'o'#39
      '            ELSE '#39'Exclus'#227'o'#39
      '       END cFlgAcao'
      '      ,Servidor.cNmServidor'
      '      ,TempRegistro.*'
      '  FROM TempRegistro'
      
        '       INNER JOIN TabelaSistema ON TabelaSistema.nCdTabelaSistem' +
        'a = TempRegistro.nCdTabelaSistema'
      
        '       INNER JOIN Servidor      ON Servidor.nCdServidor         ' +
        '  = TempRegistro.nCdServidorDestino'
      ' WHERE cFlgErro       = 1'
      '   AND cFlgIntegrado  = 0'
      '   AND cFlgBroken     = 0'
      '   AND dDtInclusao   >= (dbo.fn_OnlyDate(GetDate()) - 15)'
      ' ORDER BY dDtInclusao DESC')
    Left = 368
    Top = 264
    object AutoIncField1: TAutoIncField
      DisplayLabel = 'Registros com Erro de Transmiss'#227'o ('#218'ltimos 15 dias)|C'#243'd'
      FieldName = 'nCdTempRegistro'
      ReadOnly = True
    end
    object StringField1: TStringField
      DisplayLabel = 'Registros com Erro de Transmiss'#227'o ('#218'ltimos 15 dias)|Tabela'
      FieldName = 'cTabela'
      Size = 50
    end
    object StringField2: TStringField
      DisplayLabel = 'Registros com Erro de Transmiss'#227'o ('#218'ltimos 15 dias)|A'#231#227'o'
      FieldName = 'cFlgAcao'
      ReadOnly = True
      Size = 8
    end
    object StringField3: TStringField
      DisplayLabel = 
        'Registros com Erro de Transmiss'#227'o ('#218'ltimos 15 dias)|Servidor Des' +
        'tino'
      FieldName = 'cNmServidor'
      Size = 50
    end
    object qryErrosdDtUltTentativa: TDateTimeField
      DisplayLabel = 
        'Registros com Erro de Transmiss'#227'o ('#218'ltimos 15 dias)|'#218'lt. Tentati' +
        'va'
      FieldName = 'dDtUltTentativa'
    end
    object qryErrosiQtdeTentativas: TIntegerField
      DisplayLabel = 'Registros com Erro de Transmiss'#227'o ('#218'ltimos 15 dias)|Tentativas'
      FieldName = 'iQtdeTentativas'
    end
    object qryErroscFlgBroken: TIntegerField
      DisplayLabel = 'Registros com Erro de Transmiss'#227'o ('#218'ltimos 15 dias)|Descartado'
      FieldName = 'cFlgBroken'
    end
    object qryErroscMsgErro: TMemoField
      DisplayLabel = 
        'Registros com Erro de Transmiss'#227'o ('#218'ltimos 15 dias)|Mensagem Err' +
        'o'
      FieldName = 'cMsgErro'
      BlobType = ftMemo
    end
    object qryErrosdDtInclusao: TDateTimeField
      DisplayLabel = 'Registros com Erro de Transmiss'#227'o ('#218'ltimos 15 dias)|Dt. Inclus'#227'o'
      FieldName = 'dDtInclusao'
    end
    object qryErrosiIDRegistro: TLargeintField
      FieldName = 'iIDRegistro'
    end
  end
  object dsErros: TDataSource
    DataSet = qryErros
    Left = 408
    Top = 264
  end
  object PopupMenu1: TPopupMenu
    Left = 176
    Top = 136
    object Atualizar1: TMenuItem
      Caption = 'Atualizar'
      OnClick = Atualizar1Click
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 216
    Top = 136
    object MenuItem1: TMenuItem
      Caption = 'Atualizar'
      OnClick = MenuItem1Click
    end
    object ExibirErroCompleto1: TMenuItem
      Caption = 'Exibir Erro Completo'
      OnClick = ExibirErroCompleto1Click
    end
  end
  object PopupMenu3: TPopupMenu
    Left = 256
    Top = 136
    object MenuItem2: TMenuItem
      Caption = 'Atualizar'
      OnClick = MenuItem2Click
    end
    object MenuItem3: TMenuItem
      Caption = 'Exibir Erro Completo'
      OnClick = MenuItem3Click
    end
    object Retransmitir1: TMenuItem
      Caption = 'Retransmitir'
      OnClick = Retransmitir1Click
    end
  end
  object qryDescartados: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT nCdTempRegistro'
      '      ,cTabela'
      '      ,iIDRegistro'
      '      ,CASE WHEN cFlgAcao = '#39'I'#39' THEN '#39'Inclus'#227'o'#39
      '            ELSE '#39'Exclus'#227'o'#39
      '       END cFlgAcao'
      '      ,Servidor.cNmServidor'
      '      ,TempRegistro.*'
      '  FROM TempRegistro(NOLOCK)'
      
        '       INNER JOIN TabelaSistema ON TabelaSistema.nCdTabelaSistem' +
        'a = TempRegistro.nCdTabelaSistema'
      
        '       INNER JOIN Servidor      ON Servidor.nCdServidor         ' +
        '  = TempRegistro.nCdServidorDestino'
      ' WHERE cFlgBroken     = 1'
      '   AND cFlgIntegrado  = 0'
      '   AND dDtInclusao   >= (dbo.fn_OnlyDate(GetDate()) - 15)'
      ' ORDER BY dDtInclusao DESC')
    Left = 368
    Top = 296
    object AutoIncField2: TAutoIncField
      DisplayLabel = 'Registros Descartados ('#218'ltimos 15 dias)|C'#243'd'
      FieldName = 'nCdTempRegistro'
      ReadOnly = True
    end
    object StringField4: TStringField
      DisplayLabel = 'Registros Descartados ('#218'ltimos 15 dias)|Tabela'
      FieldName = 'cTabela'
      Size = 50
    end
    object StringField5: TStringField
      DisplayLabel = 'Registros Descartados ('#218'ltimos 15 dias)|A'#231#227'o'
      FieldName = 'cFlgAcao'
      ReadOnly = True
      Size = 8
    end
    object StringField6: TStringField
      DisplayLabel = 'Registros Descartados ('#218'ltimos 15 dias)|Servidor Destino'
      FieldName = 'cNmServidor'
      Size = 50
    end
    object DateTimeField1: TDateTimeField
      DisplayLabel = 'Registros Descartados ('#218'ltimos 15 dias)|'#218'lt. Tentativa'
      FieldName = 'dDtUltTentativa'
    end
    object IntegerField3: TIntegerField
      DisplayLabel = 'Registros Descartados ('#218'ltimos 15 dias)|Tentativas'
      FieldName = 'iQtdeTentativas'
    end
    object IntegerField4: TIntegerField
      DisplayLabel = 'Registros Descartados ('#218'ltimos 15 dias)|Descartado'
      FieldName = 'cFlgBroken'
    end
    object MemoField1: TMemoField
      DisplayLabel = 'Registros Descartados ('#218'ltimos 15 dias)|Mensagem Erro'
      FieldName = 'cMsgErro'
      BlobType = ftMemo
    end
    object DateTimeField2: TDateTimeField
      DisplayLabel = 'Registros Descartados ('#218'ltimos 15 dias)|Dt. Inclus'#227'o'
      FieldName = 'dDtInclusao'
    end
    object qryDescartadosiIDRegistro: TLargeintField
      FieldName = 'iIDRegistro'
    end
  end
  object dsDescartados: TDataSource
    DataSet = qryDescartados
    Left = 408
    Top = 296
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 364
    Top = 221
  end
  object qryPendentesServidor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT Servidor.nCdServidor'
      '      ,Servidor.cNmServidor'
      '      ,Count(1) as iQtdePendente'
      '  FROM TempRegistro'
      
        '       INNER JOIN TabelaSistema ON TabelaSistema.nCdTabelaSistem' +
        'a = TempRegistro.nCdTabelaSistema'
      
        '       INNER JOIN Servidor      ON Servidor.nCdServidor         ' +
        '  = TempRegistro.nCdServidorDestino'
      ' WHERE ((    cFlgIntegrado  = 0'
      '        AND cFlgBroken     = 0'
      '        AND cFlgObsoleto   = 0)'
      '    OR cFlgReprocessa = 1)'
      ' GROUP BY Servidor.nCdServidor'
      '         ,Servidor.cNmServidor'
      ' ORDER BY 1')
    Left = 192
    Top = 312
    object qryPendentesServidornCdServidor: TIntegerField
      FieldName = 'nCdServidor'
    end
    object qryPendentesServidorcNmServidor: TStringField
      FieldName = 'cNmServidor'
      Size = 50
    end
    object qryPendentesServidoriQtdePendente: TIntegerField
      FieldName = 'iQtdePendente'
      ReadOnly = True
      DisplayFormat = ',0'
    end
  end
  object dsPendentesServidor: TDataSource
    DataSet = qryPendentesServidor
    Left = 232
    Top = 312
  end
  object PopupMenu4: TPopupMenu
    Left = 136
    Top = 136
    object MenuItem4: TMenuItem
      Caption = 'Atualizar'
      OnClick = MenuItem4Click
    end
  end
  object qryPendentesTabela: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT TabelaSistema.nCdTabelaSistema'
      '      ,TabelaSistema.cTabela'
      '      ,TabelaSistema.cNmTabela'
      '      ,Count(1) as iQtdePendente'
      '  FROM TempRegistro'
      
        '       INNER JOIN TabelaSistema ON TabelaSistema.nCdTabelaSistem' +
        'a = TempRegistro.nCdTabelaSistema'
      ' WHERE ((    cFlgIntegrado  = 0'
      '        AND cFlgBroken     = 0'
      '        AND cFlgObsoleto   = 0)'
      '    OR cFlgReprocessa = 1)'
      ' GROUP BY TabelaSistema.nCdTabelaSistema'
      '         ,TabelaSistema.cTabela'
      '         ,TabelaSistema.cNmTabela'
      ' ORDER BY 4 DESC')
    Left = 192
    Top = 360
    object qryPendentesTabelanCdTabelaSistema: TIntegerField
      FieldName = 'nCdTabelaSistema'
    end
    object qryPendentesTabelacTabela: TStringField
      FieldName = 'cTabela'
      Size = 50
    end
    object qryPendentesTabelacNmTabela: TStringField
      FieldName = 'cNmTabela'
      Size = 50
    end
    object qryPendentesTabelaiQtdePendente: TIntegerField
      FieldName = 'iQtdePendente'
      ReadOnly = True
      DisplayFormat = ',0'
    end
  end
  object dsPendentesTabela: TDataSource
    DataSet = qryPendentesTabela
    Left = 232
    Top = 360
  end
  object PopupMenu5: TPopupMenu
    Left = 96
    Top = 136
    object MenuItem5: TMenuItem
      Caption = 'Atualizar'
      OnClick = MenuItem5Click
    end
  end
end
