unit fParam_TipoPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, GridsEh, DBGridEh, DB, ADODB, ImgList,
  ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmParam_TipoPedido = class(TfrmProcesso_Padrao)
    qryTipoPedido: TADOQuery;
    DBGridEh1: TDBGridEh;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    dsTipoPedido: TDataSource;
    qryTipoPedidocMsgNF: TStringField;
    qryTipoPedidonCdNaturezaOperacao: TIntegerField;
    qryNatureza: TADOQuery;
    qryNaturezanCdNaturezaOperacao: TIntegerField;
    qryNaturezacNmNaturezaOperacao: TStringField;
    qryTipoPedidocNmNaturezaOperacao: TStringField;
    qryTipoPedidonCdTipoDoctoFiscal: TIntegerField;
    qryTipoDoctoFiscal: TADOQuery;
    qryTipoDoctoFiscalnCdTipoDoctoFiscal: TIntegerField;
    qryTipoDoctoFiscalcNmTipoDoctoFiscal: TStringField;
    qryTipoPedidocNmTipoDoctoFiscal: TStringField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmParam_TipoPedido: TfrmParam_TipoPedido;

implementation

uses fLookup_Padrao, fParam_TipoPedido_Ajuda;

{$R *.dfm}

procedure TfrmParam_TipoPedido.FormShow(Sender: TObject);
begin
  inherited;
  qryTipoPedido.Close ;
  qryTipoPedido.Open ;

  DBGridEh1.Align := alClient ;
end;

procedure TfrmParam_TipoPedido.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Col = 3) then
        begin
          if (qryTipoPedido.State = dsBrowse) then
             qryTipoPedido.Edit ;

          if (qryTipoPedido.State = dsInsert) or (qryTipoPedido.State = dsEdit) then
          begin

            nPK := frmLookup_Padrao.ExecutaConsulta(74);

            If (nPK > 0) then
            begin
                qryTipoPedidonCdNaturezaOperacao.Value := nPK ;
            end ;

          end ;
        end ;

        if (DBGridEh1.Col = 5) then
        begin
          if (qryTipoPedido.State = dsBrowse) then
             qryTipoPedido.Edit ;

          if (qryTipoPedido.State = dsInsert) or (qryTipoPedido.State = dsEdit) then
          begin

            nPK := frmLookup_Padrao.ExecutaConsulta(77);

            If (nPK > 0) then
            begin
                qryTipoPedidonCdTipoDoctoFiscal.Value := nPK ;
            end ;

          end ;
        end ;

    end ;

  end ;

end;

procedure TfrmParam_TipoPedido.ToolButton5Click(Sender: TObject);
var
  objForm : TfrmParam_TipoPedido_Ajuda;
begin
  inherited;

  objForm := TfrmParam_TipoPedido_Ajuda.Create(nil);
  showForm(objForm, true);

end;

initialization
    RegisterClass(TfrmParam_TipoPedido) ;

end.
