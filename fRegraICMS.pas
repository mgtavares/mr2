unit fRegraICMS;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmRegraICMS = class(TfrmProcesso_Padrao)
    qryRegraICMS: TADOQuery;
    qryRegraICMSnCdRegraICMS: TIntegerField;
    qryRegraICMSnCdEmpresa: TIntegerField;
    qryRegraICMScUFOrigem: TStringField;
    qryRegraICMScUFDestino: TStringField;
    qryRegraICMSnCdGrupoImposto: TIntegerField;
    qryRegraICMSnAliqICMS: TBCDField;
    qryRegraICMSnPercBC: TBCDField;
    qryRegraICMSnPercIVA: TBCDField;
    qryRegraICMScFlgST: TStringField;
    qryRegraICMSnCdTipoPedido: TIntegerField;
    qryRegraICMSnCdTerceiro: TIntegerField;
    qryRegraICMScMensagemNF: TStringField;
    dsRegraICMS: TDataSource;
    qryGrupoImposto: TADOQuery;
    qryGrupoImpostonCdGrupoImposto: TIntegerField;
    qryGrupoImpostocNmGrupoImposto: TStringField;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryRegraICMScNmGrupoImposto: TStringField;
    qryRegraICMScNmTipoPedido: TStringField;
    qryRegraICMScNmTerceiro: TStringField;
    DBGridEh1: TDBGridEh;
    qryRegraICMSnAliqICMSInterna: TBCDField;
    qryRegraICMSnPercIncentivoFiscal: TBCDField;
    qryRegraICMSnPercBCIVA: TBCDField;
    qryRegraICMSnCdTipoTributacaoICMS: TStringField;
    qryRegraICMSnPercAliqIPI: TBCDField;
    qryRegraICMSnPercBaseCalcIPI: TBCDField;
    qryRegraICMScCdSTIPI: TStringField;
    qryTipoTributacaoICMS: TADOQuery;
    qryTipoTributacaoICMSnCdTipoTributacaoICMS: TAutoIncField;
    qryTipoTributacaoICMScCdST: TStringField;
    qryTipoTributacaoICMScNmTipoTributacaoICMS: TStringField;
    qryTipoTributacaoIPI: TADOQuery;
    qryTipoTributacaoIPInCdTipoTributacaoIPI: TIntegerField;
    qryTipoTributacaoIPIcCdStIPI: TStringField;
    qryTipoTributacaoIPIcNmTipoTributacaoIPI: TStringField;
    qryAux: TADOQuery;
    qryRegraICMSnPercCargaMediaST: TBCDField;
    qryRegraICMSnAliqICMSInternaDestino: TBCDField;
    qryRegraICMSnAliqICMSPartOrig: TBCDField;
    qryRegraICMSnAliqICMSPartDest: TBCDField;
    qryRegraICMSnAliqICMSInter: TBCDField;
    qryRegraICMSnAliqICMSDest: TBCDField;
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryRegraICMSCalcFields(DataSet: TDataSet);
    procedure qryRegraICMSBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegraICMS: TfrmRegraICMS;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmRegraICMS.FormShow(Sender: TObject);
begin

  inherited;
  PosicionaQuery(qryRegraICMS,IntToStr(frmMenu.nCdEmpresaAtiva)) ;

  DBGridEh1.Align := alClient ;

end;

procedure TfrmRegraICMS.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryRegraICMS.State = dsBrowse) then
             qryRegraICMS.Edit ;


        if (qryRegraICMS.State = dsInsert) or (qryRegraICMS.State = dsEdit) then
        begin

            if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdGrupoImposto') then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(73);

                If (nPK > 0) then
                    qryRegraICMSnCdGrupoImposto.Value := nPK ;
            end ;

            if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdTipoTributacaoICMS') then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(1027);

                If (nPK >= 0) then
                begin

                    qryTipoTributacaoICMS.Close;
                    PosicionaQuery( qryTipoTributacaoICMS , intToStr( nPK ) ) ;

                    if not qryTipoTributacaoICMS.eof then
                        qryRegraICMSnCdTipoTributacaoICMS.Value := qryTipoTributacaoICMScCdST.Value ;
                        
                end;
            end ;

            if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cCdSTIPI') then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(1026);

                If (nPK >= 0) then
                begin

                    qryTipoTributacaoIPI.Close;
                    PosicionaQuery( qryTipoTributacaoIPI , intToStr( nPK ) ) ;

                    if not qryTipoTributacaoIPI.eof then
                        qryRegraICMScCdSTIPI.Value := qryTipoTributacaoIPIcCdSTIPI.Value ;

                end;
            end ;

            if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdTipoPedido') then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(71);

                If (nPK > 0) then
                    qryRegraICMSnCdTipoPedido.Value := nPK ;
            end ;

            if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdTerceiro') then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(17);

                If (nPK > 0) then
                begin
                    qryRegraICMSnCdTerceiro.Value := nPK ;
                    PosicionaQuery(qryTerceiro, IntToStr(nPK)) ;

                    if not qryTerceiro.eof then
                        qryRegraICMScNmTerceiro.Value := qryTerceirocNmTerceiro.Value ;
                end ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRegraICMS.qryRegraICMSCalcFields(DataSet: TDataSet);
begin
  inherited;
  qryRegraICMScNmTerceiro.Value := '' ;

  qryTerceiro.Close ;
  
  PosicionaQuery(qryTerceiro, qryRegraICMSnCdTerceiro.asString) ;

  if not qryTerceiro.eof then
    qryRegraICMScNmTerceiro.Value := qryTerceirocNmTerceiro.Value ;

end;

procedure TfrmRegraICMS.qryRegraICMSBeforePost(DataSet: TDataSet);
begin
  qryRegraICMSnCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;

  if (qryRegraICMScFlgST.Value = '') then
      qryRegraICMScFlgST.Value := 'N' ;

  if ( trim( qryRegraICMScCdSTIPI.Value ) = '') then
      qryRegraICMScCdSTIPI.AsVariant := null;

  if ( trim( qryRegraICMSnCdTipoTributacaoICMS.Value ) <> '') then
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM TipoTributacaoICMS WHERE cCdST = ' + Chr(39) + qryRegraICMSnCdTipoTributacaoICMS.Value + Chr(39)) ;
      qryAux.Open;

      if qryAux.Eof then
      begin
          MensagemAlerta('CST do ICMS inv�lido.') ;
          abort ;
      end ;

  end ;

  if ( trim( qryRegraICMScCdSTIPI.Value ) <> '') then
  begin
      qryRegraICMScCdSTIPI.Value := frmMenu.ZeroEsquerda(qryRegraICMScCdSTIPI.Value,2) ;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM TipoTributacaoIPI WHERE cCdSTIPI = ' + Chr(39) + qryRegraICMScCdSTIPI.Value + Chr(39)) ;
      qryAux.Open;

      if qryAux.Eof then
      begin
          MensagemAlerta('CST do IPI inv�lido.') ;
          abort ;
      end ;
  end ;

  qryRegraICMScUFOrigem.Value  := Uppercase(qryRegraICMScUFOrigem.Value) ;
  qryRegraICMScUFDestino.Value := Uppercase(qryRegraICMScUFDestino.Value) ;
  qryRegraICMScFlgST.Value     := Uppercase(qryRegraICMScFlgST.Value) ;

  if ( Pos(qryRegraICMScUFOrigem.Value, frmMenu.estadosBrasileiros ) = 0 ) then
  begin
      MensagemAlerta('Unidade Federativa de Origem inv�lida.') ;
      abort ;
  end ;

  if ( Pos(qryRegraICMScUFDestino.Value, frmMenu.estadosBrasileiros ) = 0 ) then
  begin
      MensagemAlerta('Unidade Federativa de Destino inv�lida.') ;
      abort ;
  end ;

  if Pos(qryRegraICMScFlgST.Value,'S,N') = 0 then
  begin
      MensagemAlerta('Indicativo de Substitui��o tribut�ria inv�lido. Utilize S ou N.') ;
      abort ;
  end ;

  if (qryRegraICMSnAliqICMS.Value < 0) then
  begin
      MensagemAlerta('Al�quota de ICMS Externa inv�lida.') ;
      abort ;
  end ;

  if (qryRegraICMSnAliqICMSPartOrig.Value < 0) then
  begin
      MensagemAlerta('Al�quota de ICMS partilhada na origem inv�lida.');
      Abort;
  end;

  if (qryRegraICMSnAliqICMSPartDest.Value < 0) then
  begin
      MensagemAlerta('Al�quota de ICMS partilhada no destino inv�lida.');
      Abort;
  end;

  if (qryRegraICMSnAliqICMSInter.Value < 0) then
  begin
      MensagemAlerta('Al�quota de ICMS partilhada interestadual inv�lida.');
      Abort;
  end;

  if (qryRegraICMSnAliqICMSDest.Value < 0) then
  begin
      MensagemAlerta('Al�quota de ICMS partilhada no estado de destino inv�lida.');
      Abort;
  end;

  if (qryRegraICMSnAliqICMSInterna.Value < 0) then
  begin
      MensagemAlerta('Al�quota de ICMS Interna inv�lida.') ;
      abort ;
  end ;

  if (qryRegraICMSnPercBC.Value < 0) then
  begin
      MensagemAlerta('Percentual da Base de C�lculo do ICMS inv�lida.') ;
      abort ;
  end ;

  if (qryRegraICMSnPercIva.Value > 0) and (qryRegraICMSnAliqICMSInterna.Value <= 0) then
  begin
      MensagemAlerta('Informe a al�quota interna do estado de origem.') ;
      abort ;
  end ;

  if (qryRegraICMSnPercIncentivoFiscal.Value < 0) then
  begin
      MensagemAlerta('Percentual de incentivo fiscal inv�lido.') ;
      abort ;
  end ;

  if (qryRegraICMSnCdGrupoImposto.Value = 0) then
  begin
      MensagemAlerta('Informe o grupo de imposto.') ;
      abort ;
  end ;

  if (qryRegraICMSnPercBaseCalcIPI.Value < 0) then
  begin
      MensagemAlerta('Percentual da Base de C�lculo do IPI inv�lida.') ;
      abort ;
  end ;

  if ((qryRegraICMSnPercAliqIPI.Value < 0) or (qryRegraICMSnPercAliqIPI.Value > 100)) then
  begin
      MensagemAlerta('Percentual de Al�quota do IPI inv�lida.') ;
      abort ;
  end ;

  {-- faz as consistencias se for ST por carga media --}
  if ( qryRegraICMSnPercCargaMediaST.Value > 0 ) then
  begin

      if ( qryRegraICMSnPercIVA.Value > 0 ) then
      begin
          MensagemAlerta('Informe o Percentual de MVA ou o Percentual de Carga M�dia.') ;
          abort ;
      end ;

      if ( qryRegraICMSnAliqICMSInternaDestino.Value <= 0 ) then
      begin
          MensagemAlerta('Informe o % de ICMS no estado de destino.') ;
          abort ;
      end ;

      if ( qryRegraICMSnAliqICMS.Value <= 0 ) then
      begin
          MensagemAlerta('Informe o % de ICMS da opera��o interestadual.') ;
          abort ;
      end ;

  end ;

  if ((qryRegraICMSnPercIVA.Value > 0) and (qryRegraICMScFlgST.Value = 'N')) then
  begin
      MensagemAlerta('O percentual de MVA foi informado. Classifique a regra como Substitui��o Tribut�ria.');
      Abort;
  end;

  inherited;

  if (qryRegraICMS.State = dsInsert) then
      qryRegraICMSnCdRegraICMS.Value := frmMenu.fnProximoCodigo('REGRAICMS') ;

end;

initialization
    RegisterClass(TfrmRegraICMS) ;

end.
