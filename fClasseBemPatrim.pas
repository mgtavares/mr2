unit fClasseBemPatrim;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmClasseBemPatrim = class(TfrmCadastro_Padrao)
    qryMasternCdClasseBemPatrim: TIntegerField;
    qryMastercNmClasseBemPatrim: TStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label2: TLabel;
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmClasseBemPatrim: TfrmClasseBemPatrim;

implementation

{$R *.dfm}

procedure TfrmClasseBemPatrim.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;
  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a Descri��o da Classe.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

end;
procedure TfrmClasseBemPatrim.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CLASSEBEMPATRIM' ;
  nCdTabelaSistema  := 501 ;
  nCdConsultaPadrao := 1005 ;
end;

procedure TfrmClasseBemPatrim.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

initialization
    RegisterClass(TfrmClasseBemPatrim) ;

end.
