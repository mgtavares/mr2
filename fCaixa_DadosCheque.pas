unit fCaixa_DadosCheque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  StdCtrls, cxLookAndFeelPainters, cxButtons;

type
  TfrmCaixa_DadosCheque = class(TForm)
    Image2: TImage;
    edtCodBanco: TcxTextEdit;
    edtCodAgencia: TcxTextEdit;
    edtNumConta: TcxTextEdit;
    edtDV: TcxTextEdit;
    edtNumCheque: TcxTextEdit;
    edtCPFCNPJ: TcxTextEdit;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    btnConfirmar: TcxButton;
    btnCancelar: TcxButton;
    procedure edtCodBancoKeyPress(Sender: TObject; var Key: Char);
    procedure edtCodAgenciaKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumContaKeyPress(Sender: TObject; var Key: Char);
    procedure edtDVKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumChequeKeyPress(Sender: TObject; var Key: Char);
    procedure edtCodBancoExit(Sender: TObject);
    procedure edtCodAgenciaExit(Sender: TObject);
    procedure edtNumContaExit(Sender: TObject);
    procedure edtCPFCNPJExit(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtCPFCNPJKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_DadosCheque: TfrmCaixa_DadosCheque;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmCaixa_DadosCheque.edtCodBancoKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        edtCodAgencia.SetFocus;

end;

procedure TfrmCaixa_DadosCheque.edtCodAgenciaKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (Key = #13) then
        edtNumConta.SetFocus;

end;

procedure TfrmCaixa_DadosCheque.edtNumContaKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (Key = #13) then
        edtDV.SetFocus;

end;

procedure TfrmCaixa_DadosCheque.edtDVKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (Key = #13) then
        edtNumCheque.SetFocus;

end;

procedure TfrmCaixa_DadosCheque.edtNumChequeKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        edtCPFCNPJ.SetFocus;

end;

procedure TfrmCaixa_DadosCheque.edtCodBancoExit(Sender: TObject);
begin

    edtCodBanco.Text := frmMenu.ZeroEsquerda(edtCodBanco.Text,3) ;

end;

procedure TfrmCaixa_DadosCheque.edtCodAgenciaExit(Sender: TObject);
begin
    edtCodAgencia.Text := frmMenu.ZeroEsquerda(edtCodAgencia.Text,4) ;
end;

procedure TfrmCaixa_DadosCheque.edtNumContaExit(Sender: TObject);
begin

    edtNumConta.Text := frmMenu.ZeroEsquerda(edtNumConta.Text,10) ;

end;

procedure TfrmCaixa_DadosCheque.edtCPFCNPJExit(Sender: TObject);
begin

    if (frmMenu.TestaCpfCgc(edtCPFCNPJ.Text) = '') then
    begin
        edtCPFCNPJ.Text := '';
        edtCPFCNPJ.SetFocus;
        exit ;
    end ;

end;

procedure TfrmCaixa_DadosCheque.btnConfirmarClick(Sender: TObject);
begin

    if (frmMenu.ConvInteiro(edtCodBanco.Text) <= 0) then
    begin
        frmMenu.MensagemErro('C�digo do banco inv�lido.') ;
        edtCodBanco.SetFocus;
        exit ;
    end ;

    if (frmMenu.ConvInteiro(edtCodAgencia.Text) <= 0) then
    begin
        frmMenu.MensagemErro('C�digo de ag�ncia inv�lida.') ;
        edtCodAgencia.SetFocus;
        exit ;
    end ;

    if (edtNumConta.Text = '') or (edtNumConta.Text = '0000000') then
    begin
        frmMenu.MensagemErro('N�mero de conta corrente inv�lida.') ;
        edtNumConta.SetFocus;
        exit ;
    end ;

    if (edtDV.Text = '') then
    begin
        frmMenu.MensagemErro('D�gito verificador inv�lido.') ;
        edtDV.SetFocus;
        exit ;
    end ;

    if (frmMenu.ConvInteiro(edtNumCheque.Text) <= 0) then
    begin
        frmMenu.MensagemErro('N�mero do cheque inv�lido.') ;
        edtNumCheque.SetFocus;
        exit ;
    end ;

    if (edtCPFCNPJ.Text = '') then
    begin
        frmMenu.MensagemErro('CPF/CNPJ inv�lido.') ;
        edtCPFCNPJ.SetFocus;
        exit ;
    end ;

    close ;

end;

procedure TfrmCaixa_DadosCheque.btnCancelarClick(Sender: TObject);
begin

    if (frmMenu.MessageDlg('Confirma o cancelamento do saldo deste item?',mtConfirmation,[mbYes,mbNo],0)=MRNO) then
        exit ;

    edtCodBanco.Text   := '' ;
    edtCodAgencia.Text := '' ;
    edtNumConta.Text   := '' ;
    edtDV.Text         := '' ;
    edtNumCheque.Text  := '' ;
    edtCPFCNPJ.Text    := '' ;

    close ;

end;

procedure TfrmCaixa_DadosCheque.FormShow(Sender: TObject);
begin

    edtCodBanco.Text   := '' ;
    edtCodAgencia.Text := '' ;
    edtNumConta.Text   := '' ;
    edtDV.Text         := '' ;
    edtNumCheque.Text  := '' ;
    edtCPFCNPJ.Text    := '' ;

    edtCodBanco.SetFocus ;

end;

procedure TfrmCaixa_DadosCheque.edtCPFCNPJKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        btnConfirmar.SetFocus;

end;

end.
