unit rOrcamentoPrevReal_View_Semestral;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptOrcamentoPrevReal_View_Semestral = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_ORCAMENTO_PREVREAL_SEMESTRE: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel4: TQRLabel;
    QRShape2: TQRShape;
    QRLabel11: TQRLabel;
    QRGroup1: TQRGroup;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRLabel57: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel59: TQRLabel;
    QRDBText42: TQRDBText;
    QRDBText43: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText36: TQRDBText;
    QRDBText40: TQRDBText;
    QRDBText41: TQRDBText;
    QRLabel5: TQRLabel;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREcNmOrcamento: TStringField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREcNmCC: TStringField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREnCdGrupoPlanoConta: TIntegerField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREcNmGrupoPlanoConta: TStringField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREnCdPlanoConta: TIntegerField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREcNmPlanoConta: TStringField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREnValPrev1Sem: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREnValReal1Sem: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREnPerc1Sem: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREnValPrev2Sem: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREnValReal2Sem: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREnPerc2Sem: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREnValPrevTotal: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREnValRealTotal: TFloatField;
    SPREL_ORCAMENTO_PREVREAL_SEMESTREnPercTotal: TFloatField;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    bZebrado : boolean ;
  end;

var
  rptOrcamentoPrevReal_View_Semestral: TrptOrcamentoPrevReal_View_Semestral;

implementation

{$R *.dfm}

procedure TrptOrcamentoPrevReal_View_Semestral.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin

    if (bZebrado) then
        if (QRBand3.Color = clSilver) then QRBand3.Color := clWhite
        else QRBand3.Color := clSilver ;

end;

end.
