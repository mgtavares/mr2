unit fConsultaStatusServico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, StdCtrls, cxButtons, ACBrNFeConfiguracoes,pcnConversao;

type
  TfrmConsultaStatusServico = class(TfrmProcesso_Padrao)
    RadioGroup1: TRadioGroup;
    edtStatus: TEdit;
    Label1: TLabel;
    cOBS: TMemo;
    Label2: TLabel;
    Label3: TLabel;
    edtUltCons: TEdit;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaStatusServico: TfrmConsultaStatusServico;

implementation

uses fEmiteNFe2, ACBrNFe, ACBrNFeWebServices, fMenu;

{$R *.dfm}

procedure TfrmConsultaStatusServico.FormShow(Sender: TObject);
begin
  inherited;

  edtStatus.Color  := $00E9E4E4;
  cOBS.Color       := $00E9E4E4;
  edtUltCons.Color := $00E9E4E4;
end;

procedure TfrmConsultaStatusServico.ToolButton1Click(Sender: TObject);
var
  objForm : TfrmEmiteNFe2;
begin
  inherited;

  objForm := TfrmEmiteNFe2.Create(nil);

  try
      objForm.bConsulta := True;

      objForm.preparaNFe;

      if (RadioGroup1.ItemIndex = 0) then
      begin
          edtStatus.Text  := IntToStr(objForm.StatusWebService(1).nCdStatusSEFAZ);
          edtStatus.Text  := edtStatus.Text + ' - ' + objForm.StatusWebService(1).cNmStatusSEFAZ;
          cOBS.Text       := objForm.StatusWebService(1).cObsSEFAZ;
          edtUltCons.Text := DateTimeToStr(Now());
      end else
      if (RadioGroup1.ItemIndex = 1) then
      begin
          edtStatus.Text  := IntToStr(objForm.StatusWebService(3).nCdStatusSCAN);
          edtStatus.Text  := edtStatus.Text + ' - ' + objForm.StatusWebService(3).cNmStatusSCAN;
          cOBS.Text       := objForm.StatusWebService(3).cObsSCAN;
          edtUltCons.Text := DateTimeToStr(Now());
      end;

  finally
      FreeAndNil(objForm);
  end;
end;

initialization
    RegisterClass(TfrmConsultaStatusServico);

end.
