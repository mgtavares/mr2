unit fHistoricoLimiteCredito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ADODB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmHistoricoLimiteCredito = class(TfrmProcesso_Padrao)
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    qryLimite: TADOQuery;
    dsLimite: TDataSource;
    qryLimitedDtConcessao: TDateTimeField;
    qryLimitenValLimite: TBCDField;
    qryLimitenPercEntrada: TBCDField;
    qryLimitecAutomatico: TStringField;
    qryLimitecOBS: TStringField;
    qryLimitecNmUsuario: TStringField;
    cxGrid1DBTableView1dDtConcessao: TcxGridDBColumn;
    cxGrid1DBTableView1nValLimite: TcxGridDBColumn;
    cxGrid1DBTableView1nPercEntrada: TcxGridDBColumn;
    cxGrid1DBTableView1cAutomatico: TcxGridDBColumn;
    cxGrid1DBTableView1cOBS: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmHistoricoLimiteCredito: TfrmHistoricoLimiteCredito;

implementation

{$R *.dfm}

end.
