unit fEncerrarMovtoPOS_GeraLote;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, ADODB,
  DB, StdCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, GridsEh, DBGridEh, Mask, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmEncerrarMovtoPOS_GeraLote = class(TfrmProcesso_Padrao)
    cmdPreparaTemp: TADOCommand;
    qryTempResumo: TADOQuery;
    qryTempResumonCdOperadoraCartao: TIntegerField;
    qryTempResumocNmOperadoraCartao: TStringField;
    qryTempResumocNmForma: TStringField;
    qryTempResumonValTransacaoCalc: TBCDField;
    qryTempResumonValLiquidoCalc: TBCDField;
    qryTempResumonValTaxaOperadoraCalc: TBCDField;
    qryTempResumonValTransacaoInf: TBCDField;
    qryTempResumonValLiquidoInf: TBCDField;
    qryTempResumonValTaxaOperadoraInf: TBCDField;
    qryTempResumonValTransacaoDif: TBCDField;
    qryTempResumonValLiquidoDif: TBCDField;
    qryTempResumonValTaxaOperadoraDif: TBCDField;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label1: TLabel;
    dsTempResumo: TDataSource;
    qryTempParcelaResumoOperadora: TADOQuery;
    qryTempParcelaResumoOperadoracNmOperadora: TStringField;
    qryTempParcelaResumoOperadoracNmForma: TStringField;
    qryTempParcelaResumoOperadoraiParcela: TIntegerField;
    qryTempParcelaResumoOperadoradDtCredito: TDateTimeField;
    qryTempParcelaResumoOperadoranValLiquido: TBCDField;
    dsTempParcelaResumoOperadora: TDataSource;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    qryOperadora: TADOQuery;
    qryOperadoraiFloating: TIntegerField;
    SP_GERA_LOTE_OPERADORA_CARTAO: TADOStoredProc;
    Edit1: TMaskEdit;
    Edit2: TMaskEdit;
    qryTotalLiqParc: TADOQuery;
    qryTotalLiqParcnSaldoTit: TBCDField;
    procedure FormShow(Sender: TObject);
    procedure Edit1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTempResumoAfterScroll(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nTotalBrutoSelecionado : double  ;
    cFlgTotalLiqFechPOS    : integer ;
    cFlgDataCredFechPOS    : integer ;
    dDtTransacao           : Tdate ;

  end;

var
  frmEncerrarMovtoPOS_GeraLote: TfrmEncerrarMovtoPOS_GeraLote;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmEncerrarMovtoPOS_GeraLote.FormShow(Sender: TObject);
begin
  inherited;

  Edit1.Text := '' ;
  Edit2.Text := '' ;

  Edit1.SetFocus ;

  DBGridEh1.Columns[7].Visible := False ;
  DBGridEh1.Columns[8].Visible := False ;
  DBGridEh2.Columns[4].Visible := False ;

  if (cFlgTotalLiqFechPOS = 1) then
  begin
      DBGridEh1.Columns[7].Visible := True ;
      DBGridEh2.Columns[4].Visible := True ;
  end ;

end;

procedure TfrmEncerrarMovtoPOS_GeraLote.Edit1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

{  if (key = vk_return) then
  begin
      Edit2.SetFocus ;
  end ;}

end;

procedure TfrmEncerrarMovtoPOS_GeraLote.qryTempResumoAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryTempParcelaResumoOperadora.Close ;
  qryTempParcelaResumoOperadora.Parameters.ParamByName('cNmOperadora').Value := qryTempResumocNmOperadoraCartao.Value ;
  qryTempParcelaResumoOperadora.Parameters.ParamByName('cNmForma').Value     := qryTempResumocNmForma.Value ;
  qryTempParcelaResumoOperadora.Open ;

end;

procedure TfrmEncerrarMovtoPOS_GeraLote.ToolButton1Click(Sender: TObject);
var
    nTotalBrutoDigitado : Double;
    nTotalLiqParcelas   : Double;
    nPercDif            : Double;
    nValDiferenca       : Double;
    iDiasTolerancia     : Integer;
    dDtPrevistaCredito  : TDate;
begin
  inherited;

  iDiasTolerancia := StrToint(frmMenu.LeParametro('DIASTOLPGCART')) ;

  if (Trim(Edit1.Text) = '') then
  begin
      MensagemAlerta('Informe o n�mero do lote.') ;
      exit ;
  end ;

  if (Trim(Edit1.Text) <> Trim(Edit2.Text)) then
  begin
      MensagemAlerta('O n�mero do lote digitado � diferente do n�mero do lote da confirma��o.') ;
      Edit1.Text := '' ;
      Edit2.Text := '' ;
      Edit1.SetFocus ;
      exit ;
  end ;

  if (qryTempResumo.State = dsEdit) then
      qryTempResumo.Post ;

  if (qryTempParcelaResumoOperadora.State = dsEdit) then
      qryTempParcelaResumoOperadora.Post ;

  nTotalBrutoDigitado := 0 ;
  nTotalLiqParcelas   := 0 ;

  { -- valida par�metro -- }
  try
      nPercDif := Abs(StrToFloat(frmMenu.LeParametro('PERCDIFLOTEPOS')));
  except
      frmMenu.MensagemErro('Parametro PERCDIFLOTEPOS n�o configurado corretamente.' + #13#13 + 'Exemplo: 0,00');
      Abort;
  end;

  if (nPercDif > 20) then
  begin
      MensagemAlerta('Par�metro PERCDIFLOTEPOS n�o configurado corretamente.' + #13#13 + 'Limite dispon�vel: 20%.');
      Abort;
  end;

  qryTempResumo.First ;

  qryTempResumo.DisableControls;

  while not qryTempResumo.eof do
  begin
      if (DBGridEh2.Visible) then
      begin
          qryTempParcelaResumoOperadora.DisableControls ;

          qryTempParcelaResumoOperadora.First ;

          nTotalLiqParcelas := 0 ;

          if (cFlgDataCredFechPOS = 1) then
          begin
              qryOperadora.Close ;
              PosicionaQuery(qryOperadora, qryTempResumonCdOperadoraCartao.AsString) ;
          end ;

          while not qryTempParcelaResumoOperadora.Eof do
          begin

              if (cFlgTotalLiqFechPOS = 1) and (qryTempParcelaResumoOperadoranValLiquido.Value <= 0) then
              begin
                  MensagemAlerta('Informe o total l�quido da parcela. Operadora: ' + qryTempResumocNmOperadoraCartao.Value + '   Condi��o: ' + qryTempResumocNmForma.Value + '   Parcela: ' + qryTempParcelaResumoOperadoraiParcela.AsString) ;
                  qryTempParcelaResumoOperadora.EnableControls ;
                  qryTempResumo.EnableControls;
                  exit ;
              end ;

              if (cFlgDataCredFechPOS = 1) then
              begin
                  if (qryTempParcelaResumoOperadoradDtCredito.AsString = '') then
                  begin
                      MensagemAlerta('Informe a data de cr�dito da parcela. Operadora: ' + qryTempResumocNmOperadoraCartao.Value + '   Condi��o: ' + qryTempResumocNmForma.Value + '   Parcela: ' + qryTempParcelaResumoOperadoraiParcela.AsString) ;
                      qryTempParcelaResumoOperadora.EnableControls ;
                      qryTempResumo.EnableControls;
                      exit ;
                  end ;

                  dDtPrevistaCredito := (dDtTransacao + (qryOperadoraiFloating.Value * qryTempParcelaResumoOperadoraiParcela.Value)) ;

                  if (abs(qryTempParcelaResumoOperadoradDtCredito.Value - dDtPrevistaCredito) > iDiasTolerancia) then
                  begin
                      MensagemAlerta('Data de pagamento da parcela fora do prazo previsto. Operadora: ' + qryTempResumocNmOperadoraCartao.Value + '   Condi��o: ' + qryTempResumocNmForma.Value + '   Parcela: ' + qryTempParcelaResumoOperadoraiParcela.AsString) ;
                      qryTempParcelaResumoOperadora.EnableControls ;
                      qryTempResumo.EnableControls;
                      exit ;
                  end ;

              end ;

              { -- valida diferen�a no valor informado das parcelas do lote com o limite permitido -- }
              qryTotalLiqParc.Close;
              qryTotalLiqParc.Parameters.ParamByName('iParcela').Value     := qryTempParcelaResumoOperadoraiParcela.Value;
              qryTotalLiqParc.Parameters.ParamByName('cNmOperadora').Value := qryTempParcelaResumoOperadoracNmOperadora.Value;
              qryTotalLiqParc.Parameters.ParamByName('cNmForma').Value     := qryTempParcelaResumoOperadoracNmForma.Value;
              qryTotalLiqParc.Open;

              nValDiferenca := Abs(qryTotalLiqParcnSaldoTit.Value - qryTempParcelaResumoOperadoranValLiquido.Value);

              if ((cFlgTotalLiqFechPOS = 1) and (nValDiferenca > Abs(qryTotalLiqParcnSaldoTit.Value * (nPercDif / 100)))) then
              begin
                  MensagemAlerta('Diferen�a na somat�ria do valor l�quido para a parcela ' + qryTempParcelaResumoOperadoraiParcela.AsString + ' � maior que o limite permitido.'
                                +#13#13
                                +'Dif.: R$ ' + FloatToStr(nValDiferenca) + '.'
                                +#13
                                +'Operadora: ' + qryTempResumocNmOperadoraCartao.Value + '.'
                                +#13
                                +'Condi��o: ' + qryTempResumocNmForma.Value + '.');
                  qryTempParcelaResumoOperadora.EnableControls;
                  Abort;
              end;

              nTotalLiqParcelas := nTotalLiqParcelas + qryTempParcelaResumoOperadoranValLiquido.Value ;

              qryTempParcelaResumoOperadora.Next ;
          end ;

          qryTempParcelaResumoOperadora.First ;

          qryTempParcelaResumoOperadora.EnableControls ;

      end ;

       if (cFlgTotalLiqFechPOS = 1) and (abs(nTotalLiqParcelas - qryTempResumonValLiquidoInf.Value) > 0.01) then
      begin
          MensagemAlerta('O total l�quido informado n�o confere com a soma do valor l�quido das parcelas. Operadora: ' + qryTempResumocNmOperadoraCartao.Value + '   Condi��o: ' + qryTempResumocNmForma.Value) ;
          qryTempResumo.EnableControls;
          exit ;
      end ;

      if (qryTempResumonValTransacaoInf.Value < qryTempResumonValLiquidoInf.Value) then
      begin
          MensagemAlerta('O total bruto n�o pode ser menor que o total l�quido. Operadora: ' + qryTempResumocNmOperadoraCartao.Value + '   Condi��o: ' + qryTempResumocNmForma.Value) ;
          qryTempResumo.EnableControls;
          exit ;
      end ;

      nTotalBrutoDigitado := nTotalBrutoDigitado + qryTempResumonValTransacaoInf.Value ;
      qryTempResumo.Next ;
  end ;

  qryTempResumo.First ;

  qryTempResumo.EnableControls;

  if (frmMenu.TBRound(nTotalBrutoDigitado,2) <> frmMenu.TBRound(nTotalBrutoSelecionado,2)) then
  begin
      MensagemAlerta('O soma do total bruto informado nas condi��es � diferente da soma do total bruto dos comprovantes selecionados.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a gera��o deste lote ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try

      SP_GERA_LOTE_OPERADORA_CARTAO.Close ;
      SP_GERA_LOTE_OPERADORA_CARTAO.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
      SP_GERA_LOTE_OPERADORA_CARTAO.Parameters.ParamByName('@nCdLoja').Value    := frmMenu.nCdLojaAtiva ;
      SP_GERA_LOTE_OPERADORA_CARTAO.Parameters.ParamByName('@cNrLote').Value    := Edit1.Text ;
      SP_GERA_LOTE_OPERADORA_CARTAO.ExecProc;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Lote gerado com sucesso.') ;

  close ;

end;

end.
