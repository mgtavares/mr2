unit rClientesCadastroPeriodo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBCtrls, StdCtrls, Mask, DB, ADODB, ER2Excel;

type
  TrptClientesCadastroPeriodo = class(TfrmRelatorio_Padrao)
    Label8: TLabel;
    MaskEdit8: TMaskEdit;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    dsEmpresa: TDataSource;
    rgModeloImp: TRadioGroup;
    ER2Excel: TER2Excel;
    rgTipoCadastro: TRadioGroup;
    RadioGroup1: TRadioGroup;
    rgStatus: TRadioGroup;
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptClientesCadastroPeriodo: TrptClientesCadastroPeriodo;

implementation

uses fMenu, fLookup_Padrao,rClientesCadastroPeriodo_View;

{$R *.dfm}

procedure TrptClientesCadastroPeriodo.MaskEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptClientesCadastroPeriodo.MaskEdit8Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryLoja, MaskEdit8.Text) ;
  if qryLoja.Eof then MaskEdit8.Text := '';
end;

procedure TrptClientesCadastroPeriodo.ToolButton1Click(Sender: TObject);
var
  objRel : TrClientesCadastroPeriodoView;
  iLinha : Integer;
begin
  inherited;

  objRel := TrClientesCadastroPeriodoView.Create(nil);

  try
      try
          objRel.uspRelatorio.Close;

          {--posiciona qryLoja para verificar se a loja digitada � valida
           --e tem acesso permitido para o usu�rio logado(quando em Modo Varejo)--}

          if (frmMenu.LeParametro('VAREJO') = 'N') then
              objRel.uspRelatorio.Parameters.ParamByName('@nCdLoja').Value    := 0
          else begin
              qryLoja.Close ;
              If (Trim(MaskEdit8.Text) <> '') then
              begin
                 qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
                 qryLoja.Parameters.ParamByName('nPK').Value := MaskEdit8.Text ;
                 qryLoja.Open ;
                 if qryLoja.Eof then MaskEdit8.Text := '';
              end;
              objRel.uspRelatorio.Parameters.ParamByName('@nCdLoja').Value   := frmMenu.ConvInteiro(MaskEdit8.Text) ;
          end ;

          objRel.uspRelatorio.Parameters.ParamByName('@dDtInicial').Value     := frmMenu.ConvData(MaskEdit1.Text) ;
          objRel.uspRelatorio.Parameters.ParamByName('@dDtFinal').Value       := frmMenu.ConvData(MaskEdit2.Text) ;
          objRel.uspRelatorio.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado ;
          objRel.uspRelatorio.Parameters.ParamByName('@cFlgCadSimples').Value := rgTipoCadastro.ItemIndex;
          objRel.uspRelatorio.Parameters.ParamByName('@nCdStatus').Value      := rgStatus.ItemIndex;

          objRel.uspRelatorio.Open;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          objRel.lblFiltro1.Caption := '';
          if MaskEdit8.Text <> '' then
              objRel.lblFiltro1.Caption := 'Loja: ' + ' ' + MaskEdit8.Text + ' - ' +  DbEdit1.Text;

          if (((Trim(MaskEdit1.Text)) <> '/  /') or ((Trim(MaskEdit2.Text)) <> '/  /')) then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + '  Per�odo : ' + trim(MaskEdit1.Text) + '-' + trim(MaskEdit2.Text) ;

          { -- exibe dados complementares do cliente no relat�rio -- }
          if (RadioGroup1.ItemIndex = 0) then
              objRel.QRSubDetail1.PrintIfEmpty := false;


          case (rgModeloImp.ItemIndex) of
              0 : objRel.QuickRep1.PreviewModal;
              1 : begin
                      frmMenu.mensagemUsuario('Exportando Planilha...');

                      ER2Excel.Celula['D5'].Mesclar('E5');
                      ER2Excel.Celula['F5'].Mesclar('I5');
                      ER2Excel.Celula['J5'].Mesclar('L5');
                      ER2Excel.Celula['P5'].Mesclar('S5');
                      ER2Excel.Celula['U5'].Mesclar('V5');
                      ER2Excel.Celula['W5'].Mesclar('X5');
                      ER2Excel.Celula['Z5'].Mesclar('AA5');

                      ER2Excel.Celula['A1'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A1'].Range('AC1');
                      ER2Excel.Celula['A2'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A2'].Range('AC2');
                      ER2Excel.Celula['A3'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A3'].Range('AC3');
                      ER2Excel.Celula['A4'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A4'].Range('AC4');
                      ER2Excel.Celula['A5'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A5'].Range('AC5');

                      { -- inseri informa��es do cabe�alho -- }
                      ER2Excel.Celula['A1'].Text  := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                      ER2Excel.Celula['A2'].Text  := 'Rel. Clientes Cadastrados por Periodo.';
                      ER2Excel.Celula['A3'].Text  := 'Filtros: Loja: ' + MaskEdit8.Text + ' - ' + DBEdit1.Text + ' Periodo: ' + MaskEdit1.Text + ' At� ' + MaskEdit2.Text;
                      ER2Excel.Celula['A4'].Text  := 'Status (ST): AT - Ativo / IN - Inativo';
                      ER2Excel.Celula['A5'].Text  := 'Loja ';
                      ER2Excel.Celula['B5'].Text  := 'Dt. Cadastro ';
                      ER2Excel.Celula['C5'].Text  := 'C�d. ';
                      ER2Excel.Celula['D5'].Text  := 'CNPJ/CPF';
                      ER2Excel.Celula['F5'].Text  := 'Nome Cliente';
                      ER2Excel.Celula['J5'].Text  := 'E-mail';
                      ER2Excel.Celula['M5'].Text  := 'Telefone 1';
                      ER2Excel.Celula['N5'].Text  := 'Telefone 2';
                      ER2Excel.Celula['O5'].Text  := 'Telefone Movel';
                      ER2Excel.Celula['P5'].Text  := 'Endere�o';
                      ER2Excel.Celula['T5'].Text  := 'N� ';
                      ER2Excel.Celula['U5'].Text  := 'Bairro';
                      ER2Excel.Celula['W5'].Text  := 'Cidade';
                      ER2Excel.Celula['Y5'].Text  := 'CEP';
                      ER2Excel.Celula['Z5'].Text  := 'Complemento';
                      ER2Excel.Celula['AB5'].Text := 'Tipo Cadastro';
                      ER2Excel.Celula['AC5'].Text := 'Status';

                      iLinha := 6;

                      ER2Excel.Celula['A6'].Congelar('A6');

                      objRel.uspRelatorio.First;

                      while (not objRel.uspRelatorio.Eof) do
                      begin
                          ER2Excel.Celula['A' + IntToStr(iLinha)].Text  := objRel.uspRelatorionCdLojaTerceiro.Value;
                          ER2Excel.Celula['B' + IntToStr(iLinha)].Text  := objRel.uspRelatoriodDtCadastro.Value;
                          ER2Excel.Celula['C' + IntToStr(iLinha)].Text  := objRel.uspRelatorionCdTerceiro.Value;
                          ER2Excel.Celula['D' + IntToStr(iLinha)].Text  := objRel.uspRelatoriocCnpjCpf.Value;
                          ER2Excel.Celula['F' + IntToStr(iLinha)].Text  := objRel.uspRelatoriocNmTerceiro.Value;
                          ER2Excel.Celula['J' + IntToStr(iLinha)].Text  := objRel.uspRelatoriocEmail.Value;
                          ER2Excel.Celula['M' + IntToStr(iLinha)].Text  := objRel.uspRelatoriocTelefone1.Value;
                          ER2Excel.Celula['N' + IntToStr(iLinha)].Text  := objRel.uspRelatoriocTelefone2.Value;
                          ER2Excel.Celula['O' + IntToStr(iLinha)].Text  := objRel.uspRelatoriocTelefoneMovel.Value;
                          ER2Excel.Celula['P' + IntToStr(iLinha)].Text  := objRel.uspRelatoriocEndereco.Value;
                          ER2Excel.Celula['T' + IntToStr(iLinha)].Text  := objRel.uspRelatorioiNumero.Value;
                          ER2Excel.Celula['U' + IntToStr(iLinha)].Text  := objRel.uspRelatoriocBairro.Value;
                          ER2Excel.Celula['W' + IntToStr(iLinha)].Text  := objRel.uspRelatoriocCidade.Value;
                          ER2Excel.Celula['Y' + IntToStr(iLinha)].Text  := objRel.uspRelatoriocCep.Value;
                          ER2Excel.Celula['Z' + IntToStr(iLinha)].Text  := objRel.uspRelatoriocComplemento.Value;
                          ER2Excel.Celula['AB' + IntToStr(iLinha)].Text := objRel.uspRelatoriocTipoCadastro.Value;
                          ER2Excel.Celula['AC' + IntToStr(iLinha)].Text := objRel.uspRelatoriocStatus.Value;

                          ER2Excel.Celula['A' + IntToStr(iLinha)].Mascara := '###000';

                          objRel.uspRelatorio.Next;

                          Inc(iLinha);
                      end;

                      { -- exporta planilha e limpa result do componente -- }
                      ER2Excel.ExportXLS;
                      ER2Excel.CleanupInstance;

                      frmMenu.mensagemUsuario('');
                  end;
          end;

      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel) ;
  end;

end;

initialization
   RegisterClass(TrptClientesCadastroPeriodo) ;

end.
