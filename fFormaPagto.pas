unit fFormaPagto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, ImgList, cxPC, cxControls;

type
  TfrmFormaPagto = class(TfrmCadastro_Padrao)
    qryMasternCdFormaPagto: TIntegerField;
    qryMastercNmFormaPagto: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryMastercFlgAtivo: TIntegerField;
    qryMastercFlgGeraComprov: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    qryMasternCdTabTipoFormaPagto: TIntegerField;
    qryTabTipoFormaPagto: TADOQuery;
    qryTabTipoFormaPagtonCdTabTipoFormaPagto: TIntegerField;
    qryTabTipoFormaPagtocNmTabTipoFormaPagto: TStringField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DataSource1: TDataSource;
    qryMastercFlgLiqCrediario: TIntegerField;
    qryMastercFlgPermFinanceiro: TIntegerField;
    qryMastercFlgPermCaixa: TIntegerField;
    qryMastercFlgImpactaCaixa: TIntegerField;
    qryMastercFlgTEF: TIntegerField;
    qryMastercFlgTP: TIntegerField;
    qryMastercFlgExigeCB: TIntegerField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMastercFlgExigeCliente: TIntegerField;
    qryMastercFlgTipoCadCliente: TIntegerField;
    DBCheckBox5: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    GroupBox1: TGroupBox;
    DBCheckBox10: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    GroupBox2: TGroupBox;
    DBCheckBox12: TDBCheckBox;
    DBCheckBox11: TDBCheckBox;
    qryTabTipoFormaPagtocFlgExigeCliente: TIntegerField;
    rgTipoCad: TRadioGroup;
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBCheckBox4Click(Sender: TObject);
    procedure DBCheckBox5Click(Sender: TObject);
    procedure qryMasterAfterEdit(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFormaPagto: TfrmFormaPagto;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmFormaPagto.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmFormaPagto.FormShow(Sender: TObject);
begin
  cNmTabelaMaster   := 'FORMAPAGTO' ;
  nCdTabelaSistema  := 11 ;
  nCdConsultaPadrao := 15 ;
  inherited;

end;

procedure TfrmFormaPagto.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryTabTipoFormaPagto.Close ;
  PosicionaQuery(qryTabTipoFormaPagto, DBEdit3.Text) ;

end;

procedure TfrmFormaPagto.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DbEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o.') ;
      DbEdit2.SetFocus ;
  end ;

  if (qryMasternCdTabTipoFormaPagto.Value = 0) or (DbEdit4.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de pagamento.');
      DbEdit3.SetFocus ;
  end ;

  if (qryTabTipoFormaPagtocFlgExigeCliente.Value = 1) and (qryMastercFlgExigeCliente.Value = 0) then
  begin
      MensagemAlerta('Formas de pagamento com o tipo ' + UpperCase(qryTabTipoFormaPagtocNmTabTipoFormaPagto.AsString) + ' exige informar o cliente.');
      DBCheckBox10.SetFocus;
      Abort;
  end;

  if ((qryTabTipoFormaPagtonCdTabTipoFormaPagto.Value in [2,5]) and (rgTipoCad.ItemIndex <> 1)) then { -- 2 : Cheque / 5 : Credi�rio -- }
  begin
      MensagemAlerta('Formas de pagamento com o tipo ' + UpperCase(qryTabTipoFormaPagtocNmTabTipoFormaPagto.AsString) + ' exige Cadastro de Cliente Completo.');
      rgTipoCad.SetFocus;
      Abort;
  end;

  qryMastercFlgTipoCadCliente.Value := rgTipoCad.ItemIndex;

  inherited;
end;

procedure TfrmFormaPagto.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryTabTipoFormaPagto.Close ;
  PosicionaQuery(qryTabTipoFormaPagto, qryMasternCdTabTipoFormaPagto.asString) ;

end;

procedure TfrmFormaPagto.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryTabTipoFormaPagto.Close ;
end;

procedure TfrmFormaPagto.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryTabTipoFormaPagto.Close ;
end;

procedure TfrmFormaPagto.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(113);

            If (nPK > 0) then
            begin
                qryMasternCdTabTipoFormaPagto.Value := nPK ;
                PosicionaQuery(qryTabTipoFormaPagto, qryMasternCdTabTipoFormaPagto.asString) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmFormaPagto.DBCheckBox4Click(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsInsert) and (qryMaster.State <> dsEdit) then
      exit ;

  if (DBCheckBox4.Checked) then
      DBCheckBox4.Checked := True
  else
  begin
      qryMastercFlgTP.Value      := 0 ;
      qryMastercFlgExigeCB.Value := 0 ;
  end ;

  GroupBox2.Enabled := DBCheckBox4.Checked;

end;

procedure TfrmFormaPagto.DBCheckBox5Click(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsInsert) and (qryMaster.State <> dsEdit) then
      exit ;

  if (DBCheckBox5.Checked) then
      DBCheckBox5.Checked := True
  else
  begin
      qryMastercFlgLiqCrediario.Value := 0 ;
      qryMastercFlgImpactaCaixa.Value := 0 ;
      qryMastercFlgTEF.Value          := 0 ;
      qryMastercFlgGeraComprov.Value  := 0 ;
  end ;

  GroupBox1.Enabled := DBCheckBox5.Checked;

end;

procedure TfrmFormaPagto.qryMasterAfterEdit(DataSet: TDataSet);
begin
    inherited;

    rgTipoCad.ItemIndex := qryMastercFlgTipoCadCliente.Value;

    GroupBox1.Enabled := DBCheckBox5.Checked;
    GroupBox2.Enabled := DBCheckBox4.Checked;
end;

initialization
    RegisterClass(tFrmFormaPagto) ;

end.
