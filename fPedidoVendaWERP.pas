unit fPedidoVendaWERP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, cxContainer, cxEdit, cxTextEdit, Menus,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmPedidoVendaWERP = class(TfrmCadastro_Padrao)
    qryMasternCdPedido: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    qryMasternCdTabTipoPedido: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdTerceiroColab: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternCdTabStatusPed: TIntegerField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternPercDesconto: TBCDField;
    qryMasternPercAcrescimo: TBCDField;
    qryMasternValProdutos: TBCDField;
    qryMasternValServicos: TBCDField;
    qryMasternValImposto: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMasternValAcrescimo: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMasternValOutros: TBCDField;
    qryMasternValPedido: TBCDField;
    qryMastercNrPedTerceiro: TStringField;
    qryMastercOBS: TMemoField;
    qryMasterdDtAutor: TDateTimeField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMasterdDtRejeicao: TDateTimeField;
    qryMasternCdUsuarioRejeicao: TIntegerField;
    qryMastercMotivoRejeicao: TMemoField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTipoPedidonCdTabTipoPedido: TIntegerField;
    qryTipoPedidocFlgCalcImposto: TIntegerField;
    qryTipoPedidocFlgComporRanking: TIntegerField;
    qryTipoPedidocFlgVenda: TIntegerField;
    qryTipoPedidocGerarFinanc: TIntegerField;
    qryTipoPedidocExigeAutor: TIntegerField;
    qryTipoPedidonCdEspTitPrev: TIntegerField;
    qryTipoPedidonCdEspTit: TIntegerField;
    qryTipoPedidonCdCategFinanc: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceiroColab: TADOQuery;
    qryTerceiroColabnCdTerceiro: TIntegerField;
    qryTerceiroColabcCNPJCPF: TStringField;
    qryTerceiroColabcNmTerceiro: TStringField;
    qryTerceiroTransp: TADOQuery;
    qryTerceiroTranspnCdTerceiro: TIntegerField;
    qryTerceiroTranspcCNPJCPF: TStringField;
    qryTerceiroTranspcNmTerceiro: TStringField;
    qryIncoterms: TADOQuery;
    qryIncotermsnCdIncoterms: TIntegerField;
    qryIncotermscSigla: TStringField;
    qryIncotermscNmIncoterms: TStringField;
    qryIncotermscFlgPagador: TStringField;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    qryEstoque: TADOQuery;
    qryEstoquenCdEstoque: TAutoIncField;
    qryEstoquenCdEmpresa: TIntegerField;
    qryEstoquenCdLoja: TIntegerField;
    qryEstoquenCdTipoEstoque: TIntegerField;
    qryEstoquecNmEstoque: TStringField;
    qryEstoquenCdStatus: TIntegerField;
    qryStatusPed: TADOQuery;
    qryStatusPednCdTabStatusPed: TIntegerField;
    qryStatusPedcNmTabStatusPed: TStringField;
    qryTerceiroPagador: TADOQuery;
    qryTerceiroPagadornCdTerceiro: TIntegerField;
    qryTerceiroPagadorcCNPJCPF: TStringField;
    qryTerceiroPagadorcNmTerceiro: TStringField;
    qryMastercNmContato: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DBEdit14: TDBEdit;
    DataSource3: TDataSource;
    DBEdit15: TDBEdit;
    DataSource4: TDataSource;
    DBEdit17: TDBEdit;
    DataSource5: TDataSource;
    qryMasternCdEstoqueMov: TIntegerField;
    DataSource6: TDataSource;
    DataSource7: TDataSource;
    DataSource8: TDataSource;
    DataSource9: TDataSource;
    DataSource10: TDataSource;
    cxPageControl1: TcxPageControl;
    TabItemEstoque: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioTipoPedido: TADOQuery;
    qryUsuarioTipoPedidonCdTipoPedido: TIntegerField;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdLoja: TIntegerField;
    qryTerceironCdTerceiroPagador: TIntegerField;
    Label26: TLabel;
    DBEdit37: TDBEdit;
    DataSource11: TDataSource;
    TabParcela: TcxTabSheet;
    qryItemEstoque: TADOQuery;
    qryItemEstoquenCdProduto: TIntegerField;
    qryItemEstoquecNmItem: TStringField;
    qryItemEstoquenQtdePed: TBCDField;
    qryItemEstoquenQtdeExpRec: TBCDField;
    qryItemEstoquenQtdeCanc: TBCDField;
    qryItemEstoquenValUnitario: TBCDField;
    qryItemEstoquenValDesconto: TBCDField;
    qryItemEstoquenValSugVenda: TBCDField;
    qryItemEstoquenValTotalItem: TBCDField;
    qryItemEstoquenPercIPI: TBCDField;
    qryItemEstoquenValIPI: TBCDField;
    dsItemEstoque: TDataSource;
    qryItemEstoquenCdPedido: TIntegerField;
    qryItemEstoquenCdTipoItemPed: TIntegerField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryItemEstoquecCalc: TStringField;
    qryTerceironCdTerceiroTransp: TIntegerField;
    qryTerceironCdIncoterms: TIntegerField;
    qryTerceironCdCondPagto: TIntegerField;
    qryTerceironPercDesconto: TBCDField;
    qryTerceironPercAcrescimo: TBCDField;
    DBGridEh2: TDBGridEh;
    usp_Grade: TADOStoredProc;
    dsGrade: TDataSource;
    qryTemp: TADOQuery;
    usp_Gera_SubItem: TADOStoredProc;
    qryItemEstoquenCdItemPedido: TAutoIncField;
    cmdExcluiSubItem: TADOCommand;
    qryAux: TADOQuery;
    qryItemEstoquenValAcrescimo: TBCDField;
    qryItemEstoquenValCustoUnit: TBCDField;
    DBGridEh3: TDBGridEh;
    qryPrazoPedido: TADOQuery;
    dsPrazoPedido: TDataSource;
    qryPrazoPedidonCdPrazoPedido: TAutoIncField;
    qryPrazoPedidonCdPedido: TIntegerField;
    qryPrazoPedidodVencto: TDateTimeField;
    qryPrazoPedidoiDias: TIntegerField;
    qryPrazoPedidonValPagto: TBCDField;
    btSugParcela: TcxButton;
    usp_Sugere_Parcela: TADOStoredProc;
    Image3: TImage;
    ToolButton3: TToolButton;
    qryProdutonCdGrade: TIntegerField;
    qryItemAD: TADOQuery;
    qryItemADcCdProduto: TStringField;
    qryItemADcNmItem: TStringField;
    qryItemADnQtdePed: TBCDField;
    qryItemADnQtdeExpRec: TBCDField;
    qryItemADnQtdeCanc: TBCDField;
    qryItemADnValTotalItem: TBCDField;
    qryItemADnValCustoUnit: TBCDField;
    qryItemADnCdPedido: TIntegerField;
    qryItemADnCdTipoItemPed: TIntegerField;
    qryItemADnCdItemPedido: TAutoIncField;
    dsItemAD: TDataSource;
    qryItemADcSiglaUnidadeMedida: TStringField;
    qryTipoPedidocFlgAtuPreco: TIntegerField;
    qryTipoPedidocFlgItemEstoque: TIntegerField;
    qryTipoPedidocFlgItemAD: TIntegerField;
    qryTipoPedidonCdTipoPedido_1: TIntegerField;
    qryTipoPedidonCdUsuario: TIntegerField;
    usp_Finaliza: TADOStoredProc;
    Label23: TLabel;
    Label24: TLabel;
    Label29: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    qryDadoAutorz: TADOQuery;
    qryDadoAutorzcDadoAutoriz: TStringField;
    DataSource12: TDataSource;
    qryMastercFlgCritico: TIntegerField;
    qryMasternSaldoFat: TBCDField;
    qryFollow: TADOQuery;
    qryFollowdDtFollowUp: TDateTimeField;
    qryFollowcNmUsuario: TStringField;
    qryFollowcOcorrenciaResum: TStringField;
    qryFollowdDtProxAcao: TDateTimeField;
    qryFollowcOcorrencia: TMemoField;
    DataSource13: TDataSource;
    TabFormula: TcxTabSheet;
    qryItemFormula: TADOQuery;
    qryItemFormulacNmItem: TStringField;
    qryItemFormulanQtdePed: TBCDField;
    qryItemFormulanQtdeExpRec: TBCDField;
    qryItemFormulanQtdeCanc: TBCDField;
    qryItemFormulanValTotalItem: TBCDField;
    qryItemFormulanValCustoUnit: TBCDField;
    qryItemFormulanCdPedido: TIntegerField;
    qryItemFormulanCdTipoItemPed: TIntegerField;
    qryItemFormulanCdItemPedido: TAutoIncField;
    qryItemFormulacSiglaUnidadeMedida: TStringField;
    dsItemFormula: TDataSource;
    DBGridEh6: TDBGridEh;
    Label33: TLabel;
    cxTextEdit7: TcxTextEdit;
    cxTextEdit8: TcxTextEdit;
    cxTextEdit9: TcxTextEdit;
    qryItemFormulanCdProduto: TIntegerField;
    qryProdutoFormulado: TADOQuery;
    qryProdutoFormuladonCdProduto: TIntegerField;
    qryProdutoFormuladocNmProduto: TStringField;
    usp_gera_item_embalagem: TADOStoredProc;
    cxButton2: TcxButton;
    qryTipoPedidocFlgExibeAcomp: TIntegerField;
    qryTipoPedidocFlgCompra: TIntegerField;
    qryTipoPedidocFlgItemFormula: TIntegerField;
    qryMastercOBSFinanc: TMemoField;
    usp_valida_credito: TADOStoredProc;
    sp_tab_preco: TADOStoredProc;
    qryMastercMsgNF1: TStringField;
    qryMastercMsgNF2: TStringField;
    qryMasternPercDescontoVencto: TBCDField;
    qryMasternPercAcrescimoVendor: TBCDField;
    qryMasternCdMotBloqPed: TIntegerField;
    qryMasternCdTerceiroRepres: TIntegerField;
    qryMastercAtuCredito: TIntegerField;
    qryMastercCreditoLibMan: TIntegerField;
    qryMasterdDtIniProducao: TDateTimeField;
    qryGrupoImposto: TADOQuery;
    qryGrupoImpostonCdGrupoImposto: TIntegerField;
    qryGrupoImpostocNmGrupoImposto: TStringField;
    qryItemADnCdGrupoImposto: TIntegerField;
    qryItemADcNmGrupoImposto: TStringField;
    Label15: TLabel;
    DBEdit21: TDBEdit;
    DBEdit23: TDBEdit;
    qryDescrTecnica: TADOQuery;
    qryDescrTecnicanCdItemPedido: TAutoIncField;
    qryDescrTecnicanCdProduto: TIntegerField;
    qryDescrTecnicacNmItem: TStringField;
    qryDescrTecnicanQtdePed: TBCDField;
    qryDescrTecnicacDescricaoTecnica: TMemoField;
    DataSource14: TDataSource;
    qryMastercAnotacao: TMemoField;
    qryMastercFlgOrcamento: TIntegerField;
    SP_EMPENHA_ESTOQUE_PEDIDO: TADOStoredProc;
    qryMasternValAdiantamento: TBCDField;
    qryMastercFlgAdiantConfirm: TIntegerField;
    qryMasternCdUsuarioConfirmAdiant: TIntegerField;
    Label16: TLabel;
    DBEdit24: TDBEdit;
    PopupMenu1: TPopupMenu;
    ExibirAtendimentosdoItem1: TMenuItem;
    qryTerceiroRepres: TADOQuery;
    qryTerceiroRepresnCdTerceiro: TIntegerField;
    qryTerceiroReprescNmTerceiro: TStringField;
    DataSource15: TDataSource;
    qryTerceironCdTerceiroRepres: TIntegerField;
    qryItemFormulanValUnitario: TBCDField;
    qryItemFormulanValDesconto: TBCDField;
    qryItemFormulanValAcrescimo: TBCDField;
    Label18: TLabel;
    DBEdit27: TDBEdit;
    Label19: TLabel;
    DBEdit28: TDBEdit;
    Label20: TLabel;
    DBEdit29: TDBEdit;
    Label12: TLabel;
    DBEdit38: TDBEdit;
    DBEdit22: TDBEdit;
    Label21: TLabel;
    qryTipoPedidocLimiteCredito: TIntegerField;
    qryPrecoAux: TADOQuery;
    qryPrecoAuxnPrecoAux: TBCDField;
    qryItemEstoquenValUnitarioEsp: TBCDField;
    qryItemFormulanValUnitarioEsp: TBCDField;
    qryTerceirocFlgTipoFat: TIntegerField;
    btContatoPadrao: TcxButton;
    qryContatoPadrao: TADOQuery;
    qryContatoPadraocNmContato: TStringField;
    DBEdit5: TDBEdit;
    Label3: TLabel;
    Label8: TLabel;
    btFinalizar: TcxButton;
    btImprimir: TcxButton;
    btCancelarPed: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTerceiroAfterScroll(DataSet: TDataSet);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit25KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit24KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit38KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit38Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryItemEstoqueBeforePost(DataSet: TDataSet);
    procedure qryItemEstoqueCalcFields(DataSet: TDataSet);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit19KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueBeforeDelete(DataSet: TDataSet);
    procedure cxButton1Click(Sender: TObject);
    procedure qryItemEstoquenValUnitarioValidate(Sender: TField);
    procedure qryItemEstoquenValAcrescimoValidate(Sender: TField);
    procedure qryItemEstoqueAfterDelete(DataSet: TDataSet);
    procedure qryPrazoPedidoBeforePost(DataSet: TDataSet);
    procedure btSugParcelaClick(Sender: TObject);
    procedure qryTempAfterCancel(DataSet: TDataSet);
    procedure qryItemADBeforePost(DataSet: TDataSet);
    procedure qryItemADAfterPost(DataSet: TDataSet);
    procedure qryItemADBeforeDelete(DataSet: TDataSet);
    procedure qryItemADAfterDelete(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh4Enter(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
    procedure qryItemFormulaBeforePost(DataSet: TDataSet);
    procedure qryItemFormulaAfterPost(DataSet: TDataSet);
    procedure qryItemFormulaBeforeDelete(DataSet: TDataSet);
    procedure qryItemFormulaAfterDelete(DataSet: TDataSet);
    procedure DBGridEh6DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh6KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh6ColExit(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure DBGridEh6Enter(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure DBEdit41KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh4KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryItemADCalcFields(DataSet: TDataSet);
    procedure DBMemo4Exit(Sender: TObject);
    procedure TabDesTecnicaEnter(Sender: TObject);
    procedure ExibirAtendimentosdoItem1Click(Sender: TObject);
    procedure DBEdit11Enter(Sender: TObject);
    procedure btContatoPadraoClick(Sender: TObject);
    procedure DBEdit38Enter(Sender: TObject);
    procedure DBEdit11Exit(Sender: TObject);
    procedure qryItemFormulaAfterScroll(DataSet: TDataSet);
    procedure btFinalizarClick(Sender: TObject);
    procedure btImprimirClick(Sender: TObject);
    procedure btCancelarPedClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPedidoVendaWERP: TfrmPedidoVendaWERP;
  iDiasPrevEntrega  : integer ;

implementation

uses Math,fMenu, fLookup_Padrao, fEmbFormula,
  fPrecoEspPedCom, rPedidoVenda, fItemPedidoAtendido;

{$R *.dfm}


procedure TfrmPedidoVendaWERP.GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
var
  X:integer;
  dr,dg,db:Extended;
  C1,C2:TColor;
  r1,r2,g1,g2,b1,b2:Byte;
  R,G,B:Byte;
  cnt:integer;
begin
  C1 := FromColor;
  R1 := GetRValue(C1) ;
  G1 := GetGValue(C1) ;
  B1 := GetBValue(C1) ;

  C2 := ToColor;
  R2 := GetRValue(C2) ;
  G2 := GetGValue(C2) ;
  B2 := GetBValue(C2) ;

  dr := (R2-R1) / Rect.Right-Rect.Left;
  dg := (G2-G1) / Rect.Right-Rect.Left;
  db := (B2-B1) / Rect.Right-Rect.Left;

  cnt := 0;
  for X := Rect.Left to Rect.Right-1 do
  begin
    R := R1+Ceil(dr*cnt) ;
    G := G1+Ceil(dg*cnt) ;
    B := B1+Ceil(db*cnt) ;

    Canvas.Pen.Color := RGB(R,G,B) ;
    Canvas.MoveTo(X,Rect.Top) ;
    Canvas.LineTo(X,Rect.Bottom) ;
    inc(cnt) ;
  end;
end;

procedure TfrmPedidoVendaWERP.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PEDIDOWERP' ;
  nCdTabelaSistema  := 83 ;
  nCdConsultaPadrao := 190 ;
  bLimpaAposSalvar  := False ;

  DBGridEh2.Top  := 408;
  DBGridEh2.Left := 80 ;

end;

procedure TfrmPedidoVendaWERP.btIncluirClick(Sender: TObject);
begin
  inherited;

  if not qryEmpresa.Active then
  begin
      qryEmpresa.Close ;
      qryEmpresa.Parameters.ParamByName('nPK').Value := frmMenu.nCdEmpresaAtiva ;
      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryEmpresa.Open ;

      if not qryEmpresa.eof then
      begin
          qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
      end ;
  end ;

  qryUsuarioTipoPedido.Close ;
  qryUsuarioTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryUsuarioTipoPedido.Open ;

  if (qryUsuarioTipoPedido.eof) then
  begin
      MensagemAlerta('Nenhum tipo de pedido comercial vinculado para este usu�rio.') ;
      btCancelar.Click;
      abort ;
  end ;

  if (qryUsuarioTipoPedido.RecordCount = 1) then
  begin
      PosicionaQuery(qryTipoPedido,qryUsuarioTipoPedidonCdTipoPedido.AsString) ;
      qryMasternCdTipoPedido.Value := qryUsuarioTipoPedidonCdTipoPedido.Value ;
      DBEdit4.OnExit(nil) ;
  end ;

  qryUsuarioTipoPedido.Close ;


  If (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      qryUsuarioLoja.Close ;
      qryUsuarioLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryUsuarioLoja.Open ;

      if (qryUsuarioLoja.eof) then
      begin
          MensagemAlerta('Nenhuma loja vinculada para este usu�rio.') ;
          btCancelar.Click;
          abort ;
      end ;

      if (qryUsuarioLoja.RecordCount = 1) then
      begin
          PosicionaQuery(qryLoja,qryUsuarioLojanCdLoja.AsString) ;
          qryMasternCdLoja.Value := qryUsuarioLojanCdLoja.Value ;
      end ;

      qryUsuarioLoja.Close ;

  end ;

  if (qryMasternCdTipoPedido.Value = 0) then
      DbEdit4.SetFocus
  Else DBEdit5.SetFocus ;

  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryEstoque.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value ;

  DBGridEh1.ReadOnly           := False ;
  btSugParcela.Enabled         := True ;
  btSalvar.Enabled             := True ;
  btFinalizar.Enabled          := True ;
  qryMasterdDtPedido.Value     := StrToDate(DateToStr(Now())) ;
  qryMasterdDtPrevEntIni.Value := qryMasterdDtPedido.Value + 1 ;
  qryMasterdDtPrevEntFim.Value := qryMasterdDtPrevEntIni.Value + iDiasPrevEntrega ;
  DBEdit24.Enabled             := True ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmPedidoVendaWERP.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTipoPedido.Close ;

end;

procedure TfrmPedidoVendaWERP.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(60,'nCdTabTipoPedido = 1');

            If (nPK > 0) then
            begin
                qryMasternCdTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(191);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit20KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.qryTerceiroAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State <> dsBrowse) then
  begin
    if (qryTerceironCdTerceiroPagador.Value <> 0) and (qryMasternCdTerceiroPagador.Value = 0) then
    begin
      PosicionaQuery(qryTerceiroPagador,qryTerceironCdTerceiroPagador.AsString) ;
      qryMasternCdTerceiroPagador.Value := qryTerceironCdTerceiroPagador.Value ;
    end ;
  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit23KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit25KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(103);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroRepres.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit24KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(61);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DBedit5.Text) ;
  
  if (qryMaster.State = dsInsert) then
  begin
      if (qryTerceironCdCondPagto.Value > 0) then
      begin
          qryMasternCdCondPagto.Value := qryTerceironCdCondPagto.Value ;
          PosicionaQuery(qryCondPagto, qryTerceironCdCondPagto.asString) ;
      end
      else begin
          MensagemErro('Cliente sem condi��o de pagamento definida.') ;
          DBEdit5.SetFocus;
          abort ;
      end ;
  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit38KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(61);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTipoPedido, DBEdit4.Text) ;

  TabItemEstoque.Enabled := False ;
  TabFormula.Enabled     := False ;

  if not qryTipoPedido.eof then
  begin
    if (qryTipoPedidocFlgItemEstoque.Value = 1) then
        TabItemEstoque.Enabled := True ;

    if (qryTipoPedidocFlgItemFormula.Value = 1) then
        TabFormula.Enabled := True ;

    if (qryTipoPedidocGerarFinanc.Value = 0) and (qryMaster.State = dsInsert) then
    begin
      case MessageDlg('Este tipo de pedido n�o gera cobran�a financeira. Confirma o tipo do pedido ?',mtConfirmation,[mbNo,mbYes],0) of
          mrNo: begin
              DBEdit4.SetFocus ;
              exit ;
          end ;
      end ;
    end ;

  end ;

  if tabItemEstoque.Enabled then
      cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmPedidoVendaWERP.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryEmpresa, DBEdit2.Text) ;
  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;

end;

procedure TfrmPedidoVendaWERP.DBEdit38Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCondPagto, DBEdit38.Text) ;

  if (qryMaster.state = dsInsert) then
      qryMaster.Post ;


end;

procedure TfrmPedidoVendaWERP.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  DBGridEh1.ReadOnly   := False ;
  DBGridEh6.ReadOnly   := False ;

  btSugParcela.Enabled := True ;
  btSalvar.Enabled     := True ;
  btFinalizar.Enabled  := True ;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
      MensagemErro('Este pedido n�o pertence a esta empresa.') ;
      btCancelar.Click;
      exit ;
  end ;

  if (qryMastercFlgOrcamento.Value <> 0) then
  begin
      MensagemAlerta('Este pedido � um or�amento e n�o pode ser visualizado nesta tela.') ;
      btCancelar.Click;
      exit ;
  end ;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;
  PosicionaQuery(qryTipoPedido, qryMasternCdTipoPedido.asString) ;

  if (qryTipoPedido.eof) then
  begin
      MensagemErro('Voc� n�o tem autoriza��o para visualizar este tipo de pedido.') ;
      btCancelar.Click;
      exit;
  end ;

  PosicionaQuery(qryTerceiroPagador,qryMasternCdTerceiroPagador.asString) ;
  PosicionaQuery(qryCondPagto, qryMasternCdCondPagto.asString) ;
  PosicionaQuery(qryIncoterms,qryMasternCdIncoterms.asString) ;
  PosicionaQuery(qryTerceiroTransp, qryMasternCdTerceiroTransp.asString) ;
  PosicionaQuery(qryTerceiroRepres, qryMasternCdTerceiroRepres.asString) ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryDadoAutorz,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryFollow,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryItemFormula,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryDescrTecnica, qryMasternCdPedido.AsString) ;

  if (qryMasternCdTabStatusPed.Value > 1) and (frmMenu.LeParametro('ALTPEDFIN') = 'N') then
  begin
      DBGridEh1.ReadOnly   := True ;
      //DBGridEh3.ReadOnly   := True ;
      DBGridEh6.ReadOnly   := True ;
      btSugParcela.Enabled := False ;
      btSalvar.Enabled     := False ;
      btFinalizar.Enabled  := False ;
  end ;

  TabItemEstoque.Enabled := False ;
  TabFormula.Enabled     := False ;

  if (qryTipoPedidocFlgItemEstoque.Value = 1) then
      TabItemEstoque.Enabled := True ;

  if (qryTipoPedidocFlgItemFormula.Value = 1) then
      TabFormula.Enabled := True ;

  if tabItemEstoque.Enabled then
      cxPageControl1.ActivePageIndex := 0 ;

  DBEdit24.Enabled := True ;

  if (qryMastercFlgAdiantConfirm.Value = 1) then
      DBEdit24.Enabled := False ;

end;

procedure TfrmPedidoVendaWERP.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTipoPedido.Close ;
  qryLoja.Close ;
  qryEstoque.Close ;
  qryTerceiroColab.Close ;
  qryTerceiroRepres.Close ;
  qryTerceiroPagador.Close ;
  qryCondPagto.Close ;
  qryIncoterms.Close ;
  qryTerceiroTransp.Close ;
  qryTerceiro.Close ;
  qryItemEstoque.Close ;
  qryStatusPed.Close ;
  qryPrazoPedido.Close ;
  qryItemAD.Close ;
  qryDadoAutorz.Close ;
  qryFollow.Close ;
  qryItemFormula.Close ;
  qryDescrTecnica.Close ;

  DBEdit24.Enabled := True ;
  
end;

procedure TfrmPedidoVendaWERP.qryItemEstoqueBeforePost(DataSet: TDataSet);
begin

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      MensagemAlerta('Informe o produto.') ;
      abort ;
  end ;

  if (qryItemEstoque.State = dsInsert) then
  begin

      qryItemEstoquenCdItemPedido.Value  := frmMenu.fnProximoCodigo('ITEMPEDIDO');
      qryItemEstoquenCdPedido.Value      := qryMasternCdPedido.Value ;

      if (qryProdutonCdGrade.Value = 0) then
          qryItemEstoquenCdTipoItemPed.Value := 2
      else  qryItemEstoquenCdTipoItemPed.Value := 1 ;


      sp_tab_preco.Close;
      sp_tab_preco.Parameters.ParamByName('@nCdPedido').Value  := qryMasternCdPedido.Value ;
      sp_tab_preco.Parameters.ParamByName('@nCdProduto').Value := qryItemEstoquenCdProduto.Value ;
      sp_tab_preco.ExecProc;

      if (sp_tab_preco.Parameters.ParamByName('@nValor').Value = -1.00 ) then
      begin
          MensagemAlerta('Tabela de pre�o n�o encontrada. Verifique.') ;
          abort ;
      end ;

      if (sp_tab_preco.Parameters.ParamByName('@nValor').Value > 0) then
      begin

          qryItemEstoquenValUnitario.Value  := sp_tab_preco.Parameters.ParamByName('@nValor').Value ;
          qryItemEstoquenValSugVenda.Value  := sp_tab_preco.Parameters.ParamByName('@nValor').Value ;
          qryItemEstoquenValDesconto.Value  := sp_tab_preco.Parameters.ParamByName('@nValDesconto').Value  ;
          qryItemEstoquenValAcrescimo.Value := sp_tab_preco.Parameters.ParamByName('@nValAcrescimo').Value ;
          qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) ;
          qryItemEstoquenValTotalItem.Value := (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value) ;

      end ;

  end ;

  if (qryTerceirocFlgTipoFat.Value = 1) then
  begin
      qryPrecoAux.Close ;
      qryPrecoAux.Parameters.ParamByName('nCdEmpresa').Value    := frmMenu.nCdEmpresaAtiva;
      qryPrecoAux.Parameters.ParamByName('nCdProduto').Value    := qryItemEstoquenCdProduto.Value ;
      qryPrecoAux.Parameters.ParamByName('nCdTerceiro').Value   := qryMasternCdTerceiro.Value ;
      qryPrecoAux.Parameters.ParamByName('nValorInicial').Value := (qryItemEstoquenValUnitario.Value-qryItemEstoquenValDesconto.Value+qryItemEstoquenValACrescimo.Value) ;
      qryPrecoAux.Open ;

      if not qryPrecoAux.Eof then
          qryItemEstoquenValUnitarioEsp.Value := qryPrecoAuxnPrecoAux.Value ;
  end ;

  if (qryItemEstoquenValUnitario.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor unit�rio do produto.') ;
      abort ;
  end ;
  
  inherited;

  qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) ;
  qryItemEstoquenValTotalItem.Value := (qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value) ; //+ 0.005  ;

  if (qryItemEstoque.State = dsEdit) then
  begin
      qryMasternValProdutos.Value  := qryMasternValProdutos.Value  - (StrToFloat(qryItemEstoquenQtdePed.oldValue) * StrToFloat(qryItemEstoquenValUnitario.oldValue)) ;
      qryMasternValDesconto.Value  := qryMasternValDesconto.Value  - (StrToFloat(qryItemEstoquenQtdePed.oldValue) * StrToFloat(qryItemEstoquenValDesconto.oldValue)) ;
      qryMasternValAcrescimo.Value := qryMasternValAcrescimo.Value - (StrToFloat(qryItemEstoquenQtdePed.oldValue) * StrToFloat(qryItemEstoquenValAcrescimo.oldValue)) ;
  end ;

  qryItemEstoquecNmItem.Value := Uppercase(qryItemEstoquecNmItem.Value) ;

end;

procedure TfrmPedidoVendaWERP.qryItemEstoqueCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
      qryProduto.Close ;
      PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

      if not qryProduto.eof then
          qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;
  end ;

end;

procedure TfrmPedidoVendaWERP.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  {if (DbGridEh1.Col = 2) then
      DBGridEh1.Col := 3 ;}
  
  If (DBGridEh1.Col = 1) then
  begin

      DBGridEh1.Columns[2].ReadOnly   := True ;

      if (qryItemEstoque.State <> dsBrowse) then
      begin

        {qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('SELECT 1 FROM ItemPedido WHERE nCdPedido = ' + qryMasternCdPedido.asString + ' AND nCdProduto = ' + qryItemEstoquenCdProduto.asString) ;
        qryAux.Open ;

        if not qryAux.Eof then
        begin
            qryAux.Close ;
            ShowMessage('Item j� digitado neste pedido.') ;
            abort ;
        end ;

        qryAux.Close ;}

        qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

        PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

        if not qryProduto.eof and (qryProdutonCdGrade.Value > 0) then
        begin
            qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;

            DBGridEh2.AutoFitColWidths := True ;

            usp_Grade.Close ;
            usp_Grade.Parameters.ParamByName('@nCdPedido').Value     := qryMasternCdPedido.Value ;
            usp_Grade.Parameters.ParamByName('@nCdProduto').Value    := qryItemEstoquenCdProduto.Value ;
            usp_Grade.Parameters.ParamByName('@nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
            usp_Grade.ExecProc ;

            qryTemp.Close ;
            qryTemp.SQL.Clear ;
            qryTemp.SQL.Add(usp_Grade.Parameters.ParamByName('@cSQLRetorno').Value);
            qryTemp.Open ;

            qryTemp.Edit ;

            DBGridEh2.Columns.Items[0].Visible := False ;

            DBGridEh2.Visible := True ;

            DBGridEh2.SetFocus ;

            DBGridEh1.Col := 3 ;
        end ;

        if not qryProduto.eof and (qryProdutonCdGrade.Value = 0) then
        begin

            DBGridEh1.Columns[2].ReadOnly   := False ;
            DBGridEh1.Col := 2 ;

        end ;

        qryItemEstoquenValUnitario.Value  := 0 ;
        qryItemEstoquenValTotalItem.Value := 0 ;
        qryItemEstoquenValCustoUnit.Value := 0 ;
        qryItemEstoquenValSugVenda.Value  := 0 ;

      end ;

  end ;


end;

procedure TfrmPedidoVendaWERP.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.cFlgWERP = 0) then
  begin
      MensagemErro('Usu�rio sem permiss�o de acesso ao WERP.') ;
      PostMessage(Self.Handle, WM_CLOSE, 0, 0);
      exit ;
  end ;

  if (frmMenu.nCdTerceiroUsuario <= 0) then
  begin
      MensagemErro('C�digo de terceiro padr�o n�o vinculado ao usu�rio.') ;
      PostMessage(Self.Handle, WM_CLOSE, 0, 0);
      exit ;
  end ;

  iDiasPrevEntrega := strToint(frmMenu.LeParametro('DIASPRVWERP')) ;

  cxPageControl1.ActivePageIndex := 0 ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

end;

procedure TfrmPedidoVendaWERP.DBEdit19KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  if not qryItemEstoque.Active then
      PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;

  if not qryItemAD.Active then
      PosicionaQuery(qryItemAD,qryMasternCdPedido.asString) ;

  if not qryItemFormula.Active then
      PosicionaQuery(qryItemFormula,qryMasternCdPedido.asString) ;
      
end;

procedure TfrmPedidoVendaWERP.qryItemEstoqueAfterPost(DataSet: TDataSet);
begin
  usp_Gera_SubItem.Close ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdPedido').Value     := qryMasternCdPedido.Value ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
  usp_Gera_SubItem.ExecProc ;

  inherited;
  qryMasternValProdutos.Value  := qryMasternValProdutos.Value  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value) ;
  qryMasternValDesconto.Value  := qryMasternValDesconto.Value  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDesconto.Value) ;
  qryMasternValAcrescimo.Value := qryMasternValAcrescimo.Value + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValAcrescimo.Value) ;

  qryMaster.Post ;
end;

procedure TfrmPedidoVendaWERP.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabTipoPedido.Value := qryTipoPedidonCdTabTipoPedido.Value ;
      qryMasternCdTabStatusPed.Value  := 1 ;
  end ;

  if (qryMasternCdEmpresa.Value = 0) then
  begin
      MensagemAlerta('Informe a empresa.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTipoPedido.Value = 0) or (DbEdit14.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de pedido.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (DBEdit22.Text = '') then
  begin
      MensagemAlerta('Informe a condi��o de pagamento.') ;
      DBedit38.SetFocus;
      abort ;
  end ;

  inherited;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabStatusPed.Value    := 1;
      qryMasternCdTerceiro.Value        := frmMenu.nCdTerceiroUsuario;
      
      if (qryTerceironCdTerceiroPagador.Value > 0) then
          qryMasternCdTerceiroPagador.Value := qryTerceironCdTerceiroPagador.Value
      else qryMasternCdTerceiroPagador.Value   := qryMasternCdTerceiro.Value ;

      if (qryTerceironCdTerceiroTransp.Value > 0) then
          qryMasternCdTerceiroTransp.Value  := qryTerceironCdTerceiroTransp.Value  ;

      if (qryTerceironCdIncoterms.Value > 0) then
          qryMasternCdIncoterms.Value       := qryTerceironCdIncoterms.Value       ;
  end ;

  // se estiver aprovado, volta para digitado
  {if (qryMasternCdTabStatusPed.Value = 3) then
  begin
      qryMasternCdTabStatusPed.Value := 1;
  end ;}
      
  qryMasternValPedido.Value := qryMasternValOutros.Value + qryMasternValAcrescimo.Value - qryMasternValDesconto.Value + qryMasternValImposto.Value + qryMasternValServicos.Value + qryMasternValProdutos.Value ;
end;

procedure TfrmPedidoVendaWERP.qryTempAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmPedidoVendaWERP.qryItemEstoqueBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemEstoquenCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  qryMasternValProdutos.Value  := qryMasternValProdutos.Value  - (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value) ;
  qryMasternValDesconto.Value  := qryMasternValDesconto.Value  - (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDesconto.Value) ;
  qryMasternValAcrescimo.Value := qryMasternValAcrescimo.Value - (qryItemEstoquenQtdePed.Value * qryItemEstoquenValAcrescimo.Value) ;


end;

procedure TfrmPedidoVendaWERP.cxButton1Click(Sender: TObject);
begin
  if not qryItemEstoque.Active then
  begin
      ShowMessage('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
  PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

  if qryProduto.eof then
  begin
      ShowMessage('Selecione um item.') ;
      exit ;
  end ;

  if (qryProdutonCdGrade.Value = 0) then
  begin
      MensagemAlerta('Este item n�o est� configurado para trabalhar em grade.') ;
      exit ;
  end ;


  DBGridEh2.AutoFitColWidths := True ;

  usp_Grade.Close ;
  usp_Grade.Parameters.ParamByName('@nCdPedido').Value     := qryMasternCdPedido.Value ;
  usp_Grade.Parameters.ParamByName('@nCdProduto').Value    := qryItemEstoquenCdProduto.Value ;
  usp_Grade.Parameters.ParamByName('@nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
  usp_Grade.ExecProc ;

  qryTemp.Close ;
  qryTemp.SQL.Clear ;
  qryTemp.SQL.Add(usp_Grade.Parameters.ParamByName('@cSQLRetorno').Value);
  qryTemp.Open ;

  qryTemp.First ;

  if (qryTemp.State = dsBrowse) then
      qryTemp.Edit
  else qryTemp.Insert ;

  qryTemp.FieldList[0].Value := qryTemp.FieldList[0].Value ; 

  DBGridEh2.Columns.Items[0].Visible := False ;

  DBGridEh2.Visible := True ;
  DBGridEh2.SetFocus ;

end;

procedure TfrmPedidoVendaWERP.qryItemEstoquenValUnitarioValidate(
  Sender: TField);
begin
  inherited;

  if (qryMasternPercDesconto.Value > 0) then
      qryItemEstoquenValDesconto.Value := qryItemEstoquenValUnitario.Value * (qryMasternPercDesconto.AsFloat  / 100) ;

  if (qryMasternPercAcrescimo.Value > 0) then
      qryItemEstoquenValAcrescimo.Value := qryItemEstoquenValUnitario.Value * (qryMasternPercAcrescimo.AsFloat / 100) ;

end;

procedure TfrmPedidoVendaWERP.qryItemEstoquenValAcrescimoValidate(
  Sender: TField);
begin
  inherited;
  qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) ;
  qryItemEstoquenValCustoUnit.Value := qryItemEstoquenValCustoUnit.Value ;

  qryItemEstoquenValTotalItem.Value := qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value ;

end;

procedure TfrmPedidoVendaWERP.qryItemEstoqueAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmPedidoVendaWERP.qryPrazoPedidoBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryPrazoPedidonCdPedido.Value := qryMasternCdPedido.Value ;

  if (qryPrazoPedidoiDias.Value > 0) then
      qryPrazoPedidodVencto.Value := qryMasterdDtPrevEntIni.Value + qryPrazoPedidoiDias.Value ;

  if (qryPrazoPedidodVencto.asString = '') then
  begin
      MensagemAlerta('Informe a data do vencimento.') ;
      abort ;
  end ;

  if (qryPrazoPedidonValPagto.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

end;

procedure TfrmPedidoVendaWERP.btSugParcelaClick(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternValPedido.Value = 0) then
  begin
      ShowMessage('Pedido sem valor.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_Sugere_Parcela.Close ;
      usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Sugere_Parcela.ExecProc ;
  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

end;

procedure TfrmPedidoVendaWERP.qryTempAfterCancel(DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmPedidoVendaWERP.qryItemADBeforePost(DataSet: TDataSet);
begin

  if (qryItemADcNmItem.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o do item.') ;
      abort ;
  end ;

  if (qryItemADcSiglaUnidadeMedida.Value = '') then
  begin
      MensagemAlerta('Informe a unidade de medida do item.') ;
      abort ;
  end ;

  if (qryItemADnQtdePed.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade do item.') ;
      abort ;
  end ;

  if (qryItemADnValCustoUnit.Value < 0) then
  begin
      MensagemAlerta('Valor unit�rio inv�lido.') ;
      abort ;
  end ;

  if (qryItemADnCdGrupoImposto.Value = 0) or (qryItemADcNmGrupoImposto.Value = '') then
  begin
      MensagemAlerta('Informe o grupo de imposto') ;
      abort ;
  end ;

  inherited;

  if (qryItemAD.State = dsInsert) then
  begin
      qryItemADnCdPedido.Value      := qryMasternCdPedido.Value ;
      qryItemADnCdTipoItemPed.Value := 5 ;
      qryItemADcCdProduto.Value     := 'AD' + qryItemADnCdItemPedido.AsString;
  end ;

  qryItemADcNmItem.Value             := UpperCase(qryItemADcNmItem.Value) ;
  qryItemADcSiglaUnidadeMedida.Value := UpperCase(qryItemADcSiglaUnidadeMedida.Value) ;

  qryItemADnValTotalItem.Value := qryItemADnQtdePed.Value * qryItemADnValCustoUnit.Value ;

  if (qryItemAD.State = dsEdit) then
  begin
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloat(qryItemADnValTotalItem.OldValue) ;
  end ;

end;

procedure TfrmPedidoVendaWERP.qryItemADAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryItemAD.Edit ;
  qryItemADcCdProduto.Value   := 'AD' + qryItemADnCdItemPedido.AsString;
  //qryItemAD.Post ;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemADnValTotalItem.Value ;
  qryMaster.Post ;

end;

procedure TfrmPedidoVendaWERP.qryItemADBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemADnValTotalItem.Value ;

end;

procedure TfrmPedidoVendaWERP.qryItemADAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;
end;

procedure TfrmPedidoVendaWERP.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemEstoque.State = dsBrowse) then
             qryItemEstoque.Edit ;

        if (qryItemEstoque.State = dsInsert) or (qryItemEstoque.State = dsEdit) then
        begin
            If (qryMasternCdTipoPedido.asString = '') then
            begin
                ShowMessage('Selecione um tipo de pedido.') ;
                abort ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(141,'EXISTS(SELECT 1 FROM GrupoProdutoTipoPedido GPTP WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto AND GPTP.nCdTipoPedido = ' + qryMasternCdTipoPedido.AsString + ')');

            If (nPK > 0) then
            begin
                qryItemEstoquenCdProduto.Value := nPK ;

                qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
                PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

                if not qryProduto.eof then
                    qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  if qryMaster.Active and (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

  DBGridEh1.Columns[8].ReadOnly := True ;

  if (qryTipoPedidocFlgAtuPreco.Value = 1) then
      DBGridEh1.Columns[8].ReadOnly := false ;


end;

procedure TfrmPedidoVendaWERP.DBGridEh4Enter(Sender: TObject);
begin
  inherited;
  if qryMaster.Active and (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmPedidoVendaWERP.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  If (dataCol = 3) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) < qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clYellow;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) >= qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clBlue;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  If (dataCol = 4) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemEstoquenQtdeCanc.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clRed;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.qryItemFormulaBeforePost(DataSet: TDataSet);
begin

  if (qryItemFormulacNmItem.Value = '') then
  begin
      ShowMessage('Informe a descri��o do item.') ;
      abort ;
  end ;

  if (qryItemFormulanQtdePed.Value <= 0) then
  begin
      ShowMessage('Informe a quantidade do item.') ;
      abort ;
  end ;

  if (qryItemFormulanValCustoUnit.Value < 0) then
  begin
      ShowMessage('Valor unit�rio inv�lido.') ;
      abort ;
  end ;

  inherited;

  if (qryItemFormula.State = dsInsert) then
  begin
      qryItemFormulanCdItemPedido.Value  := frmMenu.fnProximoCodigo('ITEMPEDIDO');
      qryItemFormulanCdPedido.Value      := qryMasternCdPedido.Value ;
      qryItemFormulanCdTipoItemPed.Value := 6 ;
  end ;

  qryItemFormulanValTotalItem.Value := qryItemFormulanQtdePed.Value * qryItemFormulanValCustoUnit.Value ;

  if (qryItemFormula.State = dsEdit) then
  begin
      qryMasternValProdutos.Value  := qryMasternValProdutos.Value  - (StrToFloat(qryItemFormulanQtdePed.oldValue) * StrToFloat(qryItemFormulanValUnitario.OldValue))  ;
      qryMasternValDesconto.Value  := qryMasternValDesconto.Value  - (StrToFloat(qryItemFormulanQtdePed.oldValue) * StrToFloat(qryItemFormulanValDesconto.OldValue))  ;
      qryMasternValAcrescimo.Value := qryMasternValAcrescimo.Value - (StrToFloat(qryItemFormulanQtdePed.oldValue) * StrToFloat(qryItemFormulanValAcrescimo.OldValue)) ;
  end ;

end;

procedure TfrmPedidoVendaWERP.qryItemFormulaAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryMasternValProdutos.Value  := qryMasternValProdutos.Value  + (qryItemFormulanQtdePed.Value * qryItemFormulanValUnitario.Value)  ;
  qryMasternValDesconto.Value  := qryMasternValDesconto.Value  + (qryItemFormulanQtdePed.Value * qryItemFormulanValDesconto.Value)  ;
  qryMasternValAcrescimo.Value := qryMasternValAcrescimo.Value + (qryItemFormulanQtdePed.Value * qryItemFormulanValAcrescimo.Value) ;

  qryMaster.Post ;

  qryItemFormula.Requery([]);
  qryItemFormula.Last;

end;

procedure TfrmPedidoVendaWERP.qryItemFormulaBeforeDelete(
  DataSet: TDataSet);
begin

  qryMasternValProdutos.Value  := qryMasternValProdutos.Value  - (qryItemFormulanQtdePed.Value * qryItemFormulanValUnitario.Value)  ;
  qryMasternValDesconto.Value  := qryMasternValDesconto.Value  - (qryItemFormulanQtdePed.Value * qryItemFormulanValDesconto.Value)  ;
  qryMasternValAcrescimo.Value := qryMasternValAcrescimo.Value - (qryItemFormulanQtdePed.Value * qryItemFormulanValAcrescimo.Value) ;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemFormulanCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  inherited;

end;

procedure TfrmPedidoVendaWERP.qryItemFormulaAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmPedidoVendaWERP.DBGridEh6DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;
  If (dataCol = 3) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemFormulanQtdeExpRec.Value+qryItemFormulanQtdeCanc.Value) < qryItemFormulanQtdePed.Value) and (qryItemFormulanQtdeExpRec.Value > 0) then
      begin
        DBGridEh6.Canvas.Brush.Color := clYellow;
        DBGridEh6.Canvas.FillRect(Rect);
        DBGridEh6.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemFormulanQtdeExpRec.Value+qryItemFormulanQtdeCanc.Value) >= qryItemFormulanQtdePed.Value) and (qryItemFormulanQtdeExpRec.Value > 0) then
      begin
        DBGridEh6.Canvas.Brush.Color := clBlue;
        DBGridEh6.Canvas.FillRect(Rect);
        DBGridEh6.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  If (dataCol = 4) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemFormulanQtdeCanc.Value > 0) then
      begin
        DBGridEh6.Canvas.Brush.Color := clRed;
        DBGridEh6.Canvas.FillRect(Rect);
        DBGridEh6.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.DBGridEh6KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemFormula.State = dsBrowse) then
             qryItemFormula.Edit ;


        if (qryItemFormula.State = dsInsert) or (qryItemFormula.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(70,'cFlgProdVenda = 1 AND EXISTS(SELECT 1 FROM FormulaTipoPedido PTP WHERE PTP.nCdProdutoPai = Produto.nCdProduto AND nCdTipoPedido = ' + qryMasternCdTipoPedido.AsString + ')');

            If (nPK > 0) then
            begin
                qryItemFormulanCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmPedidoVendaWERP.DBGridEh6ColExit(Sender: TObject);
var
    iRetorno : integer ;
begin
  inherited;

  if (DBGridEh6.Col = 1) then
  begin

      if (qryItemFormula.State <> dsBrowse) then
      begin

        PosicionaQuery(qryProdutoFormulado, qryItemFormulanCdProduto.AsString) ;

        if not qryProdutoFormulado.eof then
        begin
            qryItemFormulacNmItem.Value := qryProdutoFormuladocNmProduto.Value ;

            qryAux.Close ;
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('SELECT TOP 1 1 FROM FormulaColuna WHERE nCdProdutoPai = ' + qryItemFormulanCdProduto.asString) ;
            qryAux.Open ;

            if not qryAux.eof then
            begin

                frmEmbFormula.ToolButton1.Visible := True ;
                frmEmbFormula.nCdPedido       := qryMasternCdPedido.Value          ;
                frmEmbFormula.nCdProduto      := qryItemFormulanCdProduto.Value    ;
                frmEmbFormula.nCdItemPedido   := qryItemFormulanCdItemPedido.Value ;
                frmEmbFormula.nCdTabStatusPed := qryMasternCdTabStatusPed.Value    ;
                frmEmbFormula.nCdTerceiro     := qryMasternCdTerceiro.Value        ;
                frmEmbFormula.cFlgTipoFat     := qryTerceirocFlgTipoFat.Value      ;
                frmEmbFormula.bWERP               := True ;
                frmEmbFormula.RenderizaGrade() ;
                frmEmbFormula.bWERP               := False ;

                if (not frmEmbFormula.qryTemp.Active) then
                begin
                    MensagemErro('A composi��o da caixa n�o foi digitada corretamente.') ;
                    qryItemFormula.Cancel;
                    DBGridEh6.Col := 1 ;
                    abort ;
                end ;

                // calcula o pre�o
                qryAux.Close ;
                qryAux.SQL.Clear ;
                qryAux.SQL.Add('SELECT Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValUnitProduto,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValProduto        ') ;
                qryAux.SQL.Add('      ,Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValDescontoProd,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValDescontoProd   ') ;
                qryAux.SQL.Add('      ,Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValAcrescimoProd,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValAcrescimoProd  ') ;
                qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValUnitAmostra,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValAmostra        ') ;
                qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValDescontoAmos,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValDescontoAmos   ') ;
                qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValAcrescimoAmos,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValAcresimoAmos   ') ;
                qryAux.SQL.Add('  FROM #Temp_Grade_Embalagem          ') ;
                qryAux.Open ;

                qryItemFormulanValUnitario.Value  := qryAux.FieldList[0].Value + qryAux.FieldList[3].Value ;
                qryItemFormulanValDesconto.Value  := qryAux.FieldList[1].Value + qryAux.FieldList[4].Value ;
                qryItemFormulanValAcrescimo.Value := qryAux.FieldList[2].Value + qryAux.FieldList[5].Value ;
                qryItemFormulanValCustoUnit.Value := qryItemFormulanValUnitario.Value - qryItemFormulanValDesconto.Value + qryItemFormulanValAcrescimo.Value ;

                // calcula o pre�o esperado
                qryAux.Close ;
                qryAux.SQL.Clear ;
                qryAux.SQL.Add('SELECT Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValUnitarioProdEsp,0))') ;
                qryAux.SQL.Add('                ELSE 0                ') ;
                qryAux.SQL.Add('           END) as nValProdutoEsp        ') ;
                qryAux.SQL.Add('  FROM #Temp_Grade_Embalagem          ') ;
                qryAux.Open ;

                qryItemFormulanValUnitarioEsp.Value  := qryAux.FieldList[0].Value ;

            end ;

            qryAux.Close ;

        end ;

      end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.cxButton2Click(Sender: TObject);
begin

  if not qryItemFormula.Active then
  begin
      ShowMessage('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT TOP 1 1 FROM FormulaColuna WHERE nCdProdutoPai = ' + qryItemFormulanCdProduto.asString) ;
  qryAux.Open ;

  if not qryAux.eof then
  begin

      frmEmbFormula.ToolButton1.Visible := False ;
      frmEmbFormula.nCdPedido           := qryMasternCdPedido.Value          ;
      frmEmbFormula.nCdProduto          := qryItemFormulanCdProduto.Value    ;
      frmEmbFormula.nCdItemPedido       := qryItemFormulanCdItemPedido.Value ;
      frmEmbFormula.nCdTabStatusPed     := qryMasternCdTabStatusPed.Value    ;
      frmEmbFormula.bWERP               := True ;
      frmEmbFormula.RenderizaGrade() ;
      frmEmbFormula.bWERP               := False ;

    end
    else begin
        ShowMessage('Produto n�o tem configura��o de embalagem.') ;
    end ;

    qryAux.Close ;


end;

procedure TfrmPedidoVendaWERP.DBGridEh6Enter(Sender: TObject);
begin
  inherited;
  if qryMaster.Active and (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmPedidoVendaWERP.cxButton3Click(Sender: TObject);
begin

    if not qryMaster.active then
        exit ;

    if (qryMasternCdPedido.Value = 0) then
    begin
        ShowMessage('Nenhum pedido ativo.') ;
        exit ;
    end ;

    frmPrecoEspPedCom.nCdPedido := qryMasternCdPedido.Value ;
    frmPrecoEspPedCom.ShowModal ;

end;

procedure TfrmPedidoVendaWERP.btSalvarClick(Sender: TObject);
begin

  if (qryMaster.State <> dsInsert) then
  begin
      if (qryMasternCdTabStatusPed.Value > 1) and (frmMenu.LeParametro('ALTPEDFIN') = 'N') then
      begin
          MensagemAlerta('Pedido Finalizado n�o pode ser alterado.') ;
          abort ;
      end ;
  end ;

  inherited;

end;

procedure TfrmPedidoVendaWERP.DBEdit41KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoVendaWERP.DBGridEh4KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

end;

procedure TfrmPedidoVendaWERP.qryItemADCalcFields(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryGrupoImposto, qryItemADnCdGrupoImposto.AsString) ;

  if not qryGrupoImposto.eof then
      qryItemADcNmGrupoImposto.Value := qryGrupoImpostocNmGrupoImposto.Value ;
      
end;

procedure TfrmPedidoVendaWERP.DBMemo4Exit(Sender: TObject);
begin
  inherited;

  if (qryDescrTecnica.Active) then
      if (qryDescrTecnica.State = dsEdit) then
      begin
          qryDescrTecnica.Post ;
      end ;
      
end;

procedure TfrmPedidoVendaWERP.TabDesTecnicaEnter(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryDescrTecnica, qryMasternCdPedido.AsString) ;
  
end;

procedure TfrmPedidoVendaWERP.ExibirAtendimentosdoItem1Click(
  Sender: TObject);
begin
  inherited;

  if (cxPageControl1.ActivePage = TabItemEstoque) then
  begin
      if (qryItemEstoque.Active) and (qryItemEstoquenCdItemPedido.Value > 0) then
      begin
          PosicionaQuery(frmItemPedidoAtendido.qryItem, qryItemEstoquenCdItemPedido.AsString) ;
          frmItemPedidoAtendido.ShowModal ;
      end ;
  end ;

  if (cxPageControl1.ActivePage = TabFormula) then
  begin
      if (qryItemFormula.Active) and (qryItemFormulanCdItemPedido.Value > 0) then
      begin
          PosicionaQuery(frmItemPedidoAtendido.qryItem, qryItemFormulanCdItemPedido.AsString) ;
          frmItemPedidoAtendido.ShowModal ;
      end ;
  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit11Enter(Sender: TObject);
begin
  inherited;
  if (DBEdit11.Text = '') and (qryMaster.State = dsInsert) then
      btContatoPadrao.Click;

end;

procedure TfrmPedidoVendaWERP.btContatoPadraoClick(Sender: TObject);
begin
  inherited;
  if not (qryMaster.Active) then
      exit ;

  if (qryMaster.State = dsBrowse) then
      qryMaster.Edit ;

  qryContatoPadrao.Close ;
  PosicionaQuery(qryContatoPadrao, qryTerceironCdTerceiro.AsString) ;

  if not qryContatoPadrao.Eof then
      qryMastercNmContato.Value := qryContatoPadraocNmContato.Value ;

end;

procedure TfrmPedidoVendaWERP.DBEdit38Enter(Sender: TObject);
begin
  inherited;
  if (qryTerceironCdCondPagto.Value > 0) then
  begin
      qryMasternCdCondPagto.Value := qryTerceironCdCondPagto.Value ;
      PosicionaQuery(qryCondPagto, qryTerceironCdCondPagto.asString) ;
  end ;

end;

procedure TfrmPedidoVendaWERP.DBEdit11Exit(Sender: TObject);
begin
  inherited;
  if (qryMaster.state = dsInsert) then
      qryMaster.Post ;

  DBGridEh1.SetFocus ;
end;

procedure TfrmPedidoVendaWERP.qryItemFormulaAfterScroll(DataSet: TDataSet);
begin
  inherited;

  DBGridEh6.Columns[2].ReadOnly         := True   ;
  DBGridEh6.Columns[2].Title.Font.Color := clRed  ;
  DBGridEh6.Columns[2].Font.color       := clTeal ;

  if (qryItemFormula.State = dsInsert) then
  begin
      DBGridEh6.Columns[2].ReadOnly         := False ;
      DBGridEh6.Columns[2].Title.Font.Color := clBlack ;
      DBGridEh6.Columns[2].Font.color       := clBlack ;
  end ;

  
end;

procedure TfrmPedidoVendaWERP.btFinalizarClick(Sender: TObject);
var
  nValProdutos, nValDesconto, nValAcrescimo : double ;
  bAlerta : boolean ;
begin

  nValProdutos  := 0 ;
  nValDesconto  := 0 ;
  nValAcrescimo := 0 ;
  bAlerta       := false ;

  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if qryItemEstoque.Active then
      qryItemEstoque.First ;

  if qryItemAD.Active then
      qryItemAD.First ;

  if qryItemFormula.Active then
      qryItemformula.First ;

  // calcula o total do pedido
  if qryItemEstoque.Active and not qryItemEstoque.eof then
  begin

      qryItemEstoque.First ;

      while not qryItemEstoque.Eof do
      begin
          nValProdutos  := nValProdutos  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value) ;
          nValDesconto  := nValDesconto  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDesconto.Value)  ;
          nValAcrescimo := nValAcrescimo + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValAcrescimo.Value) ;
          qryItemEstoque.Next ;
      end ;

      qryItemEstoque.First ;

  end ;

  if qryItemAD.Active and not qryItemAD.eof then
  begin

      qryItemAD.First ;

      while not qryItemAD.Eof do
      begin
          nValProdutos  := nValProdutos + qryItemADnValTotalItem.Value ;
          qryItemAD.Next ;
      end ;

      qryItemAD.First ;

  end ;

  if qryItemFormula.Active and not qryItemFormula.eof then
  begin

      qryItemFormula.First ;

      while not qryItemFormula.Eof do
      begin
          nValProdutos  := nValProdutos  + (qryItemFormulanQtdePed.Value * qryItemFormulanValUnitario.Value) ;
          nValDesconto  := nValDesconto  + (qryItemFormulanQtdePed.Value * qryItemFormulanValDesconto.Value)  ;
          nValAcrescimo := nValAcrescimo + (qryItemFormulanQtdePed.Value * qryItemFormulanValAcrescimo.Value) ;
          qryItemFormula.Next ;
      end ;

      qryItemFormula.First ;

  end ;

  qryMaster.Edit ;
  qryMasternValProdutos.Value  := nValProdutos  ;
  qryMasternValDesconto.Value  := nValDesconto  ;
  qryMasternValAcrescimo.Value := nValAcrescimo ;
  qryMaster.Post ;

  if (qryMasternValPedido.Value = 0) then
  begin

      case MessageDlg('Pedido sem valor. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
        mrNo:Abort;
      end;

  end ;

  if (qryTipoPedidocGerarFinanc.Value = 1) then
  begin

      if (dbEdit38.Text = '') then
      begin
          MensagemAlerta('Informe a condi��o de pagamento.') ;
          exit ;
      end ;

      frmMenu.Connection.BeginTrans;

      try
          usp_Sugere_Parcela.Close ;
          usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
          usp_Sugere_Parcela.ExecProc ;
      except
          frmMenu.Connection.RollbackTrans ;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans ;

      PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT Sum(nValPagto) FROM PrazoPedido WHERE nCdPedido = ' + qryMasternCdPedido.asString) ;
      qryAux.Open ;

      If (qryAux.Eof and (qryMasternValPedido.Value > 0)) or (qryAux.FieldList[0].Value <> qryMasternValPedido.Value) then
      begin
          qryAux.Close ;
          MensagemAlerta('Valor da soma das parcelas � diferente do valor total dos pedidos') ;
          exit ;
      end ;

      qryAux.Close ;
  end ;

  case MessageDlg('O pedido ser� finalizado e n�o poder� sofrer altera��es. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Abort;
  end;

    if (qryTipoPedidocGerarFinanc.Value = 0) then
    begin
      case MessageDlg('Este tipo de pedido n�o gera cobran�a financeira. Confirma o tipo do pedido ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;
    end ;

  frmMenu.Connection.BeginTrans ;

  try
      qryMaster.Edit ;
      qryMasternCdTabStatusPed.Value := 2 ; // Aguardando aprova��o
      qryMasternSaldoFat.Value := qryMasternValPedido.Value ;
      qryMaster.Post ;

      usp_Finaliza.Close ;
      usp_Finaliza.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Finaliza.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  PosicionaQuery(qryMaster, qryMasternCdPedido.AsString) ;

  if (qryTipoPedidocExigeAutor.Value = 1) and (qryMasterdDtAutor.asString = '') then
  begin
      frmMenu.SP_GERA_ALERTA.Close;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@nCdTipoAlerta').Value := 2 ;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cAssunto').Value      := 'LIBERAR PEDIDO DE VENDA' ;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cMensagem').Value     := 'No Pedido: ' + qryMasternCdPedido.AsString + '  Terceiro: ' + qryTerceirocNmTerceiro.Value + '   Valor Pedido : ' + Format('%m',[qryMasternValPedido.Value]) ;
      frmMenu.SP_GERA_ALERTA.ExecProc;
  end ;

  ShowMessage('Pedido finalizado com sucesso.') ;

  case MessageDlg('Deseja imprimir o pedido ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes: btImprimir.Click;
  end ;

  btCancelar.Click;

end;

procedure TfrmPedidoVendaWERP.btImprimirClick(Sender: TObject);
begin
  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

{  if ToolButton10.Enabled and (qryMaster.State <> dsBrowse) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
      exit ;
  end ; }

  PosicionaQuery(rptPedidoVenda.qryPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(rptPedidoVenda.qryItemEstoque_Grade,qryMasternCdPedido.asString) ;
  PosicionaQuery(rptPedidoVenda.qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(rptPedidoVenda.qryItemFormula,qryMasternCdPedido.asString) ;

  rptPedidoVenda.QRSubDetail1.Enabled := True ;
  rptPedidoVenda.QRSubDetail2.Enabled := True ;
  rptPedidoVenda.QRSubDetail3.Enabled := True ;

  if (rptPedidoVenda.qryItemEstoque_Grade.eof) then
      rptPedidoVenda.QRSubDetail1.Enabled := False ;

  if (rptPedidoVenda.qryItemAD.eof) then
      rptPedidoVenda.QRSubDetail2.Enabled := False ;

  if (rptPedidoVenda.qryItemFormula.eof) then
      rptPedidoVenda.QRSubDetail3.Enabled := False ;

  rptPedidoVenda.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

  rptPedidoVenda.QuickRep1.PreviewModal;

end;

procedure TfrmPedidoVendaWERP.btCancelarPedClick(Sender: TObject);
begin

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 2) then
  begin
      MensagemAlerta('O status do pedido n�o permite mais cancelamento.') ;
      exit ;
  end ;

  case MessageDlg('O pedido ser� cancelado. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Exit;
  end;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Edit ;
      qryMasternCdTabStatusPed.Value := 10 ;
      qryMaster.Post ;

      frmMenu.LogAuditoria(30,3,qryMasternCdPedido.Value,'Pedido Cancelado') ;

  except
      frmMenu.Connection.RollbackTrans;
      qryMaster.Cancel;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Pedido cancelado com sucesso.') ;
  btCancelar.Click;

end;

initialization
    RegisterClass(TfrmPedidoVendaWERP) ;

end.
