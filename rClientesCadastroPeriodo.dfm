inherited rptClientesCadastroPeriodo: TrptClientesCadastroPeriodo
  Left = 362
  Top = 260
  Width = 693
  Height = 240
  Caption = 'Rel. Clientes Cadastrados por Per'#237'odo'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 677
    Height = 178
  end
  object Label8: TLabel [1]
    Left = 72
    Top = 46
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label3: TLabel [2]
    Left = 9
    Top = 70
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Cadastro'
  end
  object Label6: TLabel [3]
    Left = 181
    Top = 70
    Width = 16
    Height = 13
    Alignment = taRightJustify
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    Width = 677
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit8: TMaskEdit [5]
    Left = 101
    Top = 38
    Width = 65
    Height = 21
    TabOrder = 1
    OnExit = MaskEdit8Exit
    OnKeyDown = MaskEdit8KeyDown
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 169
    Top = 38
    Width = 349
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 2
  end
  object MaskEdit1: TMaskEdit [7]
    Left = 100
    Top = 62
    Width = 63
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [8]
    Left = 203
    Top = 62
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object rgModeloImp: TRadioGroup [9]
    Left = 293
    Top = 132
    Width = 225
    Height = 41
    Caption = ' Modelo '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Planilha')
    ParentColor = False
    TabOrder = 8
  end
  object rgTipoCadastro: TRadioGroup [10]
    Left = 293
    Top = 86
    Width = 225
    Height = 41
    BiDiMode = bdLeftToRight
    Caption = 'Tipo de cadastro'
    Color = 13086366
    Columns = 3
    ItemIndex = 2
    Items.Strings = (
      'Completo'
      'Simples'
      'Todos')
    ParentBiDiMode = False
    ParentColor = False
    TabOrder = 6
  end
  object RadioGroup1: TRadioGroup [11]
    Left = 100
    Top = 132
    Width = 185
    Height = 41
    BiDiMode = bdLeftToRight
    Caption = ' Exibir Dados Complementares '
    Color = 13086366
    Columns = 2
    Ctl3D = True
    ItemIndex = 0
    Items.Strings = (
      'N'#227'o'
      'Sim')
    ParentBiDiMode = False
    ParentColor = False
    ParentCtl3D = False
    TabOrder = 7
  end
  object rgStatus: TRadioGroup [12]
    Left = 100
    Top = 86
    Width = 185
    Height = 41
    Caption = ' Status '
    Color = 13086366
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'Ativo'
      'Inativo')
    ParentColor = False
    TabOrder = 5
  end
  inherited ImageList1: TImageList
    Left = 616
    Top = 112
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK'
      
        '      AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja =' +
        ' Loja.nCdLoja AND UL.nCdUsuario = :nCdUsuario)')
    Left = 555
    Top = 114
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 586
    Top = 113
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 554
    Top = 81
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 586
    Top = 81
  end
  object ER2Excel: TER2Excel
    Titulo = 'Rel. Cliente Cadastro'
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 616
    Top = 80
  end
end
