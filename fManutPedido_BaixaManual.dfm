inherited frmManutPedido_BaixaManual: TfrmManutPedido_BaixaManual
  Left = 101
  Top = 148
  Width = 983
  BorderIcons = [biSystemMenu]
  Caption = 'Baixa Manual'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 967
  end
  inherited ToolBar1: TToolBar
    Width = 967
    ButtonWidth = 84
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 84
    end
    inherited ToolButton2: TToolButton
      Left = 92
      Caption = 'Fechar'
    end
    object ToolButton4: TToolButton
      Left = 176
      Top = 0
      Caption = 'Baixar Todos'
      ImageIndex = 1
      OnClick = ToolButton4Click
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 967
    Height = 435
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsItemPedido
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdItemPedido'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdPedido'
        Footers = <>
        ReadOnly = True
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'cNmItem'
        Footers = <>
        ReadOnly = True
        Width = 553
      end
      item
        EditButtons = <>
        FieldName = 'nQtdePed'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeExpRec'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeCanc'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryItemPedido: TADOQuery
    Connection = frmMenu.Connection
    LockType = ltBatchOptimistic
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdItemPedido'
      '      ,nCdPedido'
      '      ,nCdProduto'
      '      ,cNmItem'
      '      ,nQtdePed'
      '      ,nQtdeExpRec'
      '      ,nQtdeCanc '
      '  FROM ItemPedido'
      ' WHERE nCdPedido       = :nPK'
      '   AND nCdTipoItemPed IN (2,3,1,5,6,10)'
      ' ORDER BY cNmItem')
    Left = 288
    Top = 224
    object qryItemPedidonCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryItemPedidonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryItemPedidonCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryItemPedidocNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemPedidonQtdePed: TBCDField
      DisplayLabel = 'Quantidades|Pedido'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemPedidonQtdeExpRec: TBCDField
      DisplayLabel = 'Quantidades|Faturado'
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object qryItemPedidonQtdeCanc: TBCDField
      DisplayLabel = 'Quantidades|Cancelado'
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
  end
  object dsItemPedido: TDataSource
    DataSet = qryItemPedido
    Left = 328
    Top = 224
  end
  object qryAtualizaPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdPedido int'
      ''
      'Set @nCdPedido = :nPK'
      ''
      ''
      'UPDATE Pedido'
      
        '   SET nSaldoFat = (SELECT Sum(nValCustoUnit * (nQtdePed-nQtdeEx' +
        'pRec-nQtdeCanc))'
      '                      FROM ItemPedido'
      
        '                     WHERE ItemPedido.nCdPedido = Pedido.nCdPedi' +
        'do'
      '                       AND nCdItemPedidoPai     IS NULL)'
      '      ,dDtIntegracao = NULL'
      ' WHERE nCdPedido = @nCdPedido'
      ''
      
        '/* Se n'#227'o tem nenhum item recebido, deixa o status como autoriza' +
        'do */'
      'UPDATE Pedido'
      '   Set nCdTabStatusPed = 3'
      ' WHERE nCdPedido = @nCdPedido'
      '   AND EXISTS(SELECT TOP 1 1'
      '                FROM ItemPedido'
      '               WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      
        '                 AND (ItemPedido.nQtdePed - ItemPedido.nQtdeExpR' +
        'ec - ItemPedido.nQtdeCanc) > 0)'
      '   AND NOT EXISTS(SELECT TOP 1 1'
      '                    FROM ItemPedido'
      
        '                   WHERE ItemPedido.nCdPedido   = Pedido.nCdPedi' +
        'do'
      '                     AND ItemPedido.nQtdeExpRec > 0)'
      ''
      
        '/* Se tem saldo em aberto e tamb'#233'm item recebido, deixa como fat' +
        'urado parcial */'
      'UPDATE Pedido'
      '   Set nCdTabStatusPed = 4'
      ' WHERE nCdPedido = @nCdPedido'
      '   AND EXISTS(SELECT TOP 1 1'
      '                FROM ItemPedido'
      '               WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      
        '                 AND (ItemPedido.nQtdePed - ItemPedido.nQtdeExpR' +
        'ec - ItemPedido.nQtdeCanc) > 0)'
      '   AND EXISTS(SELECT TOP 1 1'
      '                FROM ItemPedido'
      '               WHERE ItemPedido.nCdPedido   = Pedido.nCdPedido'
      '                 AND ItemPedido.nQtdeExpRec > 0)'
      ''
      
        '/* Se n'#227'o tem saldo em aberto e tamb'#233'm item recebido, deixa como' +
        ' faturado total */'
      'UPDATE Pedido'
      '   Set nCdTabStatusPed = 5'
      ' WHERE nCdPedido = @nCdPedido'
      '   AND NOT EXISTS(SELECT TOP 1 1'
      '                    FROM ItemPedido'
      '                   WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      
        '                     AND (ItemPedido.nQtdePed - ItemPedido.nQtde' +
        'ExpRec - ItemPedido.nQtdeCanc) > 0)'
      '   AND EXISTS(SELECT TOP 1 1'
      '                FROM ItemPedido'
      '               WHERE ItemPedido.nCdPedido   = Pedido.nCdPedido'
      '                 AND ItemPedido.nQtdeExpRec > 0)'
      ''
      
        '/* Se n'#227'o tem saldo em aberto e tamb'#233'm n'#227'o tem item recebido, de' +
        'ixa como cancelado */'
      'UPDATE Pedido'
      '   Set nCdTabStatusPed = 10'
      ' WHERE nCdPedido = @nCdPedido'
      '   AND NOT EXISTS(SELECT TOP 1 1'
      '                    FROM ItemPedido'
      '                   WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      
        '                     AND (ItemPedido.nQtdePed - ItemPedido.nQtde' +
        'ExpRec - ItemPedido.nQtdeCanc) > 0)'
      '   AND NOT EXISTS(SELECT TOP 1 1'
      '                    FROM ItemPedido'
      
        '                   WHERE ItemPedido.nCdPedido   = Pedido.nCdPedi' +
        'do'
      '                     AND ItemPedido.nQtdeExpRec > 0)')
    Left = 408
    Top = 232
  end
end
