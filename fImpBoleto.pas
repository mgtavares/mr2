unit fImpBoleto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DBCtrls, Mask, ER2Lookup, cxLookAndFeelPainters, cxButtons, DB,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxCheckBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, cxPC, DBGridEhGrouping, GridsEh,
  DBGridEh, Menus, ToolCtrlsEh;

type
  TfrmImpBoleto = class(TfrmProcesso_Padrao)
    qryContaBancaria: TADOQuery;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryFormaPagamento: TADOQuery;
    qryFormaPagamentonCdFormaPagto: TIntegerField;
    qryFormaPagamentocNmFormaPagto: TStringField;
    dsTerceiro: TDataSource;
    dsContaBancaria: TDataSource;
    dsFormaPagto: TDataSource;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    qryTitulos: TADOQuery;
    qryTemp: TADOQuery;
    qryTitulosnCdTitulo: TIntegerField;
    qryTituloscFlgOK: TIntegerField;
    qryTituloscNrTit: TStringField;
    qryTituloscNrNF: TStringField;
    qryTitulosiParcela: TIntegerField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTitulosnSaldoTit: TBCDField;
    qryTitulosnCdTerceiro: TIntegerField;
    qryTituloscNmTerceiro: TStringField;
    qryTitulosnCdFormaPagto: TIntegerField;
    qryTituloscNmFormaPagto: TStringField;
    dsTitulos: TDataSource;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgOK: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmFormaPagto: TcxGridDBColumn;
    qryPreparaTemp: TADOQuery;
    dsTemp: TDataSource;
    qryTempnCdTitulo: TIntegerField;
    qryTempcFlgOK: TIntegerField;
    qryTempcNrTit: TStringField;
    qryTempcNrNF: TStringField;
    qryTempiParcela: TIntegerField;
    qryTempdDtEmissao: TDateTimeField;
    qryTempdDtVenc: TDateTimeField;
    qryTempnSaldoTit: TBCDField;
    qryTempnCdTerceiro: TIntegerField;
    qryTempcNmTerceiro: TStringField;
    qryTempnCdFormaPagto: TIntegerField;
    qryTempcNmFormaPagto: TStringField;
    cmdPreparaTemp: TADOCommand;
    qryTitulo: TADOQuery;
    qryLanctoFin: TADOQuery;
    qryLanctoFinnCdLanctoFin: TIntegerField;
    qryLanctoFinnCdResumoCaixa: TIntegerField;
    qryLanctoFinnCdContaBancaria: TIntegerField;
    qryLanctoFindDtLancto: TDateTimeField;
    qryLanctoFinnCdTipoLancto: TIntegerField;
    qryLanctoFinnValLancto: TBCDField;
    qryLanctoFinnCdUsuario: TIntegerField;
    qryLanctoFinnCdUsuarioAutor: TIntegerField;
    qryLanctoFincFlgManual: TIntegerField;
    qryLanctoFindDtEstorno: TDateTimeField;
    qryLanctoFinnCdLanctoFinPai: TIntegerField;
    qryLanctoFincMotivoEstorno: TStringField;
    qryLanctoFincHistorico: TStringField;
    qryLanctoFincFlgConciliado: TIntegerField;
    qryLanctoFinnCdUsuarioConciliacao: TIntegerField;
    qryLanctoFindDtConciliacao: TDateTimeField;
    qryLanctoFincDocumento: TStringField;
    qryLanctoFinnCdCheque: TIntegerField;
    qryLanctoFindDtExtrato: TDateTimeField;
    qryLanctoFindDtContab: TDateTimeField;
    qryLanctoFinnCdProvisaoTit: TIntegerField;
    qryLanctoFinnCdTituloDA: TIntegerField;
    qryLanctoFinnCdOperadoraCartao: TIntegerField;
    qryLanctoFincFlgLiqCrediario: TIntegerField;
    qryLanctoFinnCdTituloCobranca: TIntegerField;
    qryLanctoFincFlgCredNaoIdent: TIntegerField;
    qryLanctoFinnSaldoPendenteIdent: TBCDField;
    qryLanctoFinnCdTerceiroDep: TIntegerField;
    qryLanctoFinnCdServidorOrigem: TIntegerField;
    qryLanctoFindDtReplicacao: TDateTimeField;
    qryLanctoFinnCdDepositoBancario: TIntegerField;
    qryLanctoFinnCdUsuarioEstorno: TIntegerField;
    qryLanctoFinnCdCCLancto: TIntegerField;
    SP_GERA_LANCTOFIN_EMISSAO_BOLETO: TADOStoredProc;
    qryTitulonCdTitulo: TIntegerField;
    qryTitulonCdEmpresa: TIntegerField;
    qryTitulonCdEspTit: TIntegerField;
    qryTitulocNrTit: TStringField;
    qryTituloiParcela: TIntegerField;
    qryTitulonCdTerceiro: TIntegerField;
    qryTitulonCdCategFinanc: TIntegerField;
    qryTitulonCdMoeda: TIntegerField;
    qryTitulocSenso: TStringField;
    qryTitulodDtEmissao: TDateTimeField;
    qryTitulodDtReceb: TDateTimeField;
    qryTitulodDtVenc: TDateTimeField;
    qryTitulodDtLiq: TDateTimeField;
    qryTitulodDtCad: TDateTimeField;
    qryTitulodDtCancel: TDateTimeField;
    qryTitulonValTit: TBCDField;
    qryTitulonValLiq: TBCDField;
    qryTitulonSaldoTit: TBCDField;
    qryTitulonValJuro: TBCDField;
    qryTitulonValDesconto: TBCDField;
    qryTitulocObsTit: TMemoField;
    qryTitulonCdSP: TIntegerField;
    qryTitulonCdBancoPortador: TIntegerField;
    qryTitulodDtRemPortador: TDateTimeField;
    qryTitulodDtBloqTit: TDateTimeField;
    qryTitulocObsBloqTit: TStringField;
    qryTitulonCdUnidadeNegocio: TIntegerField;
    qryTitulonCdCC: TIntegerField;
    qryTitulodDtContab: TDateTimeField;
    qryTitulonCdDoctoFiscal: TIntegerField;
    qryTitulodDtCalcJuro: TDateTimeField;
    qryTitulonCdRecebimento: TIntegerField;
    qryTitulonCdCrediario: TIntegerField;
    qryTitulonCdTransacaoCartao: TIntegerField;
    qryTitulonCdLanctoFin: TIntegerField;
    qryTitulocFlgIntegrado: TIntegerField;
    qryTitulocFlgDocCobranca: TIntegerField;
    qryTitulocNrNF: TStringField;
    qryTitulonCdProvisaoTit: TIntegerField;
    qryTitulocFlgDA: TIntegerField;
    qryTitulonCdContaBancariaDA: TIntegerField;
    qryTitulonCdTipoLanctoDA: TIntegerField;
    qryTitulonCdLojaTit: TIntegerField;
    qryTitulocFlgInclusaoManual: TIntegerField;
    qryTitulodDtVencOriginal: TDateTimeField;
    qryTitulocFlgCartaoConciliado: TIntegerField;
    qryTitulonCdGrupoOperadoraCartao: TIntegerField;
    qryTitulonCdOperadoraCartao: TIntegerField;
    qryTitulonValTaxaOperadora: TBCDField;
    qryTitulonCdLoteConciliacaoCartao: TIntegerField;
    qryTitulodDtIntegracao: TDateTimeField;
    qryTitulonCdParcContratoEmpImobiliario: TIntegerField;
    qryTitulonValAbatimento: TBCDField;
    qryTitulonValMulta: TBCDField;
    qryTitulodDtNegativacao: TDateTimeField;
    qryTitulonCdUsuarioNeg: TIntegerField;
    qryTitulonCdItemListaCobrancaNeg: TIntegerField;
    qryTitulonCdLojaNeg: TIntegerField;
    qryTitulodDtReabNeg: TDateTimeField;
    qryTitulocFlgCobradora: TIntegerField;
    qryTitulonCdCobradora: TIntegerField;
    qryTitulodDtRemCobradora: TDateTimeField;
    qryTitulonCdUsuarioRemCobradora: TIntegerField;
    qryTitulonCdItemListaCobRemCobradora: TIntegerField;
    qryTitulocFlgReabManual: TIntegerField;
    qryTitulonCdUsuarioReabManual: TIntegerField;
    qryTitulonCdFormaPagtoTit: TIntegerField;
    qryTitulonPercTaxaJurosDia: TBCDField;
    qryTitulonPercTaxaMulta: TBCDField;
    qryTituloiDiaCarenciaJuros: TIntegerField;
    qryTitulocFlgRenegociado: TIntegerField;
    qryTitulocCodBarra: TStringField;
    qryTitulonValEncargoCobradora: TBCDField;
    qryTitulocFlgRetCobradoraManual: TIntegerField;
    qryTitulonCdServidorOrigem: TIntegerField;
    qryTitulodDtReplicacao: TDateTimeField;
    qryTitulonValIndiceCorrecao: TBCDField;
    qryTitulonValCorrecao: TBCDField;
    qryTitulocFlgNegativadoManual: TIntegerField;
    qryTitulocIDExternoTit: TStringField;
    qryTitulonCdTipoAdiantamento: TIntegerField;
    qryTitulocFlgAdiantamento: TIntegerField;
    qryTitulonCdMTituloGerador: TIntegerField;
    qryTitulocFlgBoletoImpresso: TIntegerField;
    cxTabSheet2: TcxTabSheet;
    qryTempInstrucaoTitulo: TADOQuery;
    dsTempInstrucaoTitulo: TDataSource;
    DBGridEh1: TDBGridEh;
    qryTempInstrucaoTitulonCdTituloInstrucaoBoleto: TIntegerField;
    qryTempInstrucaoTitulocNmMensagem: TStringField;
    qryTempInstrucaoTitulocFlgMensagemPadrao: TIntegerField;
    qryTituloInstrucaoBoleto: TADOQuery;
    qryTituloInstrucaoBoletonCdTituloInstrucaoBoleto: TIntegerField;
    qryTituloInstrucaoBoletonCdTitulo: TIntegerField;
    dsTituloInstrucaoBoleto: TDataSource;
    qryJurosDiaAtraso: TADOQuery;
    qryJurosDiaAtrasonPercJuroDia: TBCDField;
    qryJurosDiaAtrasonPercMulta: TBCDField;
    qryInstrucaoContaBancaria: TADOQuery;
    dsInstrucaoContaBancaria: TDataSource;
    qryInstrucaoContaBancarianCdTituloInstrucaoBoleto: TAutoIncField;
    qryInstrucaoContaBancariacNmMensagem: TStringField;
    qryInstrucaoContaBancariacFlgMensagemPadrao: TIntegerField;
    qryApagaInstrucaoTitulo: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancariacNmBanco: TStringField;
    qryContaBancariacAgencia: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancariacNmTitular: TStringField;
    qryContaBancariaiUltimoBoleto: TIntegerField;
    qryContaBancariacPrimeiraInstrucaoBoleto: TStringField;
    qryContaBancariacSegundaInstrucaoBoleto: TStringField;
    qryTempnCdEspTit: TIntegerField;
    qryTituloInstrucaoBoletocMensagem: TStringField;
    qryTituloInstrucaoBoletocFlgMensagemAgrupamento: TIntegerField;
    PopupMenu1: TPopupMenu;
    InstruesdeCobrancadoTtulo1: TMenuItem;
    qryContaBancarianCdTabTipoModeloImpBoleto: TIntegerField;
    qryContaBancariaiSeqNossoNumero: TIntegerField;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label5: TLabel;
    edtFormaPagto: TER2LookupMaskEdit;
    DBEdit4: TDBEdit;
    MaskEditEmissaoInicial: TMaskEdit;
    MaskEditEmissaoFinal: TMaskEdit;
    MaskEditVenctoInicial: TMaskEdit;
    MaskEditVenctoFinal: TMaskEdit;
    edtTerceiro: TER2LookupMaskEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    MaskEditNumeroNF: TMaskEdit;
    edtContaBancaria: TER2LookupMaskEdit;
    btnExibirTitulos: TcxButton;
    CheckBox2: TCheckBox;
    GroupBox2: TGroupBox;
    btnSelecionarTodosTit: TcxButton;
    btnLimparSelecaoTit: TcxButton;
    btnImprimirBoleto: TcxButton;
    btnAgruparTitulos: TcxButton;
    qryContaBancarianCdTabTipoRespEmissaoBoleto: TIntegerField;
    Label7: TLabel;
    edtEmpresa: TER2LookupMaskEdit;
    DBEdit8: TDBEdit;
    DataSource1: TDataSource;
    DBEdit9: TDBEdit;
    qryTempcFlgPermitirEmissaoBoleto: TIntegerField;
    qryAux: TADOQuery;
    qryDadosTerceiro: TADOQuery;
    qryDadosTerceirocInstrucaoBoleto1: TStringField;
    qryDadosTerceirocInstrucaoBoleto2: TStringField;
    qryDadosTerceiroiDiasProtesto: TIntegerField;
    qryDadosTerceironPercDesctoBolAteVencto: TBCDField;
    qryDadosTerceiroiDiasCarencia: TIntegerField;
    qryDadosTerceironPercMultaBoleto: TBCDField;
    qryDadosTerceironPercJurosMesBoleto: TBCDField;
    qryDadosTerceirocFlgPermitirEmissaoBoleto: TIntegerField;
    qryDadosTerceirocFlgNaoEnviarProtesto: TIntegerField;
    procedure edtContaBancariaBeforePosicionaQry(Sender: TObject);
    procedure btnExibirTitulosClick(Sender: TObject);
    procedure btnSelecionarTodosTitClick(Sender: TObject);
    procedure btnLimparSelecaoTitClick(Sender: TObject);
    procedure btnImprimirBoletoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnAgruparTitulosClick(Sender: TObject);
    procedure edtContaBancariaExit(Sender: TObject);
    procedure qryInstrucaoContaBancariaBeforeDelete(DataSet: TDataSet);
    procedure qryInstrucaoContaBancariaBeforePost(DataSet: TDataSet);
    procedure InstruesdeCobrancadoTtulo1Click(Sender: TObject);
    procedure edtContaBancariaBeforeLookup(Sender: TObject);
    procedure edtEmpresaBeforePosicionaQry(Sender: TObject);
    procedure qryTempBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTitulo : integer;
  end;

var
  frmImpBoleto: TfrmImpBoleto;

implementation

uses fMenu, fImpBOLPadrao, fImpBoletoAgrupamento,fImpBoletoInstrucaoTitulo;

{$R *.dfm}
procedure TfrmImpBoleto.edtContaBancariaBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;
  if ( DBEdit9.Text = '' ) then
  begin
      MensagemAlerta('Selecione a empresa.');
      edtEmpresa.SetFocus;
      abort;
  end ;

  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro( edtEmpresa.Text );

end;

procedure TfrmImpBoleto.btnExibirTitulosClick(Sender: TObject);
begin
  inherited;

  if ( DBEdit9.Text = '' ) then
  begin
      MensagemAlerta('Selecione a empresa.');
      edtEmpresa.SetFocus;
      abort;
  end ;

  cxPageControl1.ActivePageIndex := 0;

  if checkBox2.Checked then
  begin
      if (Trim(edtTerceiro.Text) = '') and (Trim(MaskEditEmissaoInicial.Text) = '/  /') and (Trim(MaskEditEmissaoFinal.Text) = '/  /') and
          (Trim(MaskEditVenctoInicial.Text) = '/  /') and (Trim(MaskEditVenctoFinal.Text) = '/  /') and (Trim(MaskEditNumeroNF.Text) = '') then
          begin
              MensagemAlerta('Informe pelo menos um filtro para a reemiss�o.') ;
              edtTerceiro.SetFocus;
              abort ;
          end;
  end;

  qryTemp.Close ;

  qryPreparaTemp.Close ;
  qryPreparaTemp.ExecSQL;

  cmdPreparaTemp.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.ConvInteiro(edtEmpresa.Text);
  cmdPreparaTemp.Parameters.ParamByName('nCdTerceiro').Value       := frmMenu.ConvInteiro(edtTerceiro.Text);
  cmdPreparaTemp.Parameters.ParamByName('nCdFormaPagto').Value     := frmMenu.ConvInteiro(edtFormaPagto.Text);
  cmdPreparaTemp.Parameters.ParamByName('dDtEmissaoInicial').Value := frmMenu.ConvData(MaskEditEmissaoInicial.Text) ;
  cmdPreparaTemp.Parameters.ParamByName('dDtEmissaoFinal').Value   := frmMenu.ConvData(MaskEditEmissaoFinal.Text) ;
  cmdPreparaTemp.Parameters.ParamByName('dDtVenctoInicial').Value  := frmMenu.ConvData(MaskEditVenctoInicial.Text) ;
  cmdPreparaTemp.Parameters.ParamByName('dDtVenctoFinal').Value    := frmMenu.ConvData(MaskEditVenctoFinal.Text) ;
  cmdPreparaTemp.Parameters.ParamByName('cNrNf').Value             := MasKEditNumeroNF.Text;
  cmdPreparaTemp.Parameters.ParamByName('nCdTitulo').Value         := nCdTitulo;

  if checkBox2.Checked then
      cmdPreparaTemp.Parameters.ParamByName('cFlgRemissao').Value  := 1
  else cmdPreparaTemp.Parameters.ParamByName('cFlgRemissao').Value := 0;

  cmdPreparaTemp.Execute;

  qryTemp.Close ;
  qryTemp.Open  ;

  if (qryTemp.eof) then
  begin
      MensagemAlerta('Nenhum t�tulo encontrado para o crit�rio utilizado.') ;
      edtContaBancaria.setFocus;
      abort;
  end ;

end;

procedure TfrmImpBoleto.btnSelecionarTodosTitClick(Sender: TObject);
begin
  inherited;
  if (qryTemp.Active) then
  begin

      qryTemp.DisableControls;

      qryTemp.First;
      while not qryTemp.Eof do
      begin
          if ( qryTempcFlgPermitirEmissaoBoleto.Value = 1 ) then
          begin
              qryTemp.Edit;
              qryTempcFlgOk.Value := 1;
              qryTemp.Post;
          end ;

          qryTemp.Next;
      end;

      qryTemp.Close ;
      qryTemp.Open  ;

      qryTemp.EnableControls;

  end;

end;

procedure TfrmImpBoleto.btnLimparSelecaoTitClick(Sender: TObject);
begin
  inherited;
  
  if qryTemp.Active then
  begin

      qryTemp.DisableControls;

      qryTemp.First;

      while not qryTemp.Eof do
      begin
          qryTemp.Edit;
          qryTempcFlgOk.Value := 0;
          qryTemp.Post;
          qryTemp.Next;
      end;

      qryTemp.Close ;
      qryTemp.Open  ;

      qryTemp.EnableControls;

  end;
end;

procedure TfrmImpBoleto.btnImprimirBoletoClick(Sender: TObject);
var
    cProximoBoleto : string ;
    iProximoBoleto : integer ;
    objForm        : TfrmImpBolPadrao ;
    arrID          : array of integer ;
    iAuxTotal      : integer ;

begin
  inherited;

  if (not qryTemp.Active) then
      Exit;

  objForm := TfrmImpBolPadrao.Create(nil) ;

  if (qryTemp.Active) then
      qryTemp.First;

  if ((not qryContaBancaria.Active) or (qryContaBancaria.Eof)) then
  begin
      MensagemAlerta('Informe a Conta Portadora.') ;
      edtContaBancaria.SetFocus;
      abort ;
  end;

  if (frmMenu.LeParametro('FORMAPAGTOBOL') = '') then
  begin
      MensagemAlerta('Par�metro (FORMAPAGTOBOL) n�o configurado.') ;
      abort ;
  end;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('DELETE FROM #Temp_TitulosBoleto WHERE cFlgOK = 0');
  qryAux.ExecSQL;

  qryTemp.Close;
  qryTemp.Open;

  if ((qryTemp.Active) and (not qryTemp.eof)) then
  begin

      iAuxTotal := 0;
      qryTemp.First;
      while not qryTemp.eof do
      begin
          if (qryTempcFlgOk.Value = 1) then
              inc (iAuxTotal);
          qryTemp.Next;
      end;

      if (iAuxTotal = 0) then
      begin
          ShowMessage('Nenhum T�tulo selecionado.') ;
          exit ;
      end;

      iAuxTotal := 0;

      if (qryContaBancarianCdTabTipoRespEmissaoBoleto.Value = 2) then
      begin
          MensagemAlerta('N�o � poss�vel imprimir o boleto. Conta Banc�ria Cedente est� configurada para emitir o boleto pelo banco.');
          abort;
      end;

      case MessageDlg('Confirma a impress�o dos boletos ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      // grava as instru��es do boleto
      qryTemp.First;
      while not qryTemp.eof do
      begin
          if (qryTempcFlgOk.Value = 1) then
          begin

              //apaga instru��es existentes no tituloInstru��oBoleto
              qryApagaInstrucaoTitulo.Close;
              qryApagaInstrucaoTitulo.SQL.Clear;
              qryApagaInstrucaoTitulo.sql.add('DELETE FROM TituloInstrucaoBoleto WHERE nCdTitulo = :nCdTitulo');
              qryApagaInstrucaoTitulo.sql.add(' AND cFlgMensagemAgrupamento <> 1');
              qryApagaInstrucaoTitulo.Parameters.ParamByName('nCdTitulo').Value := qryTempnCdTitulo.Value;
              qryApagaInstrucaoTitulo.ExecSql;

              //grava as instru��es padrao da conta bancaria para cada boleto a ser impresso
              qryTituloInstrucaoBoleto.Open;
              qryInstrucaoContaBancaria.Open;
              qryInstrucaoContaBancaria.First;
              while not qryInstrucaoContaBancaria.Eof do
              begin
                  qryTituloInstrucaoBoleto.Insert;
                  qryTituloInstrucaoBoletonCdTituloInstrucaoBoleto.Value := frmMenu.fnProximoCodigo('TITULOINSTRUCAOBOLETO');
                  qryTituloInstrucaoBoletonCdTitulo.Value                := qryTempnCdTitulo.Value;
                  qryTituloInstrucaoBoletocMensagem.Value                := qryInstrucaoContaBancariacNmMensagem.Value;
                  qryTituloInstrucaoBoleto.Post;
                  qryInstrucaoContaBancaria.Next;
              end;

          end;
          qryTemp.Next;
      end;

      if (qryContaBancarianCdTabTipoModeloImpBoleto.Value = 2) then
      begin
          qryContaBancaria.Requery();
          cProximoBoleto := intToStr(qryContaBancariaiUltimoBoleto.Value +1) ;

          InputQuery('�ltimo Boleto: ' + qryContaBancariaiUltimoBoleto.asString,'N�mero do Pr�ximo Boleto',cProximoBoleto) ;

          try
              strToInt(trim(cProximoBoleto));
          except
               MensagemAlerta('N�mero de boleto inv�lido ou n�o informado.') ;
               abort ;
          end ;

          if (strToInt(trim(cProximoBoleto)) <= 0) then
          begin
              MensagemAlerta('N�mero de boleto inv�lido ou n�o informado.') ;
             abort ;
          end ;

          if (strToint(trim(cProximoBoleto)) <= qryContaBancariaiUltimoBoleto.Value) then
          begin
              MensagemAlerta('O N�mero do pr�ximo boleto n�o pode ser menor ou igual ao �ltimo boleto impresso.') ;
              abort ;
          end ;

          case MessageDlg('Confirma a numera��o informada ?  N�mero Pr�ximo Boleto: ' + cProximoBoleto,mtConfirmation,[mbYes,mbNo],0) of
              mrNo:exit ;
          end ;

          //objForm.qryResultado.
      end;

      SetLength(arrID,0) ;

      qryTemp.First;
      while not qryTemp.eof do
      begin
          if (qryTempcFlgOk.Value = 1) then
          begin
              SetLength(arrID,Length(arrID)+1) ;
              arrID[iAuxTotal] := qryTempnCdTitulo.Value ;
              inc(iAuxTotal) ;
          end;
          qryTemp.Next;
      end;

      qryTemp.First;
      
      if (qryContaBancarianCdTabTipoModeloImpBoleto.Value = 1) then
          objForm.emissaoBoletoA4(arrID,qryContaBancarianCdContaBancaria.Value)
      else if (qryContaBancarianCdTabTipoModeloImpBoleto.Value = 2) then
      begin
          objForm.nCdBanco := 1;
          objForm.emissaoBoleto(arrID,0,qryContaBancarianCdBanco.Value) ;
      end ;

      case MessageDlg('Os boletos foram impressos corretamente ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo: begin
             ShowMessage('Nenhum registro foi atualizado, imprima novamente o boleto.') ;
             exit ;
          end ;
      end ;

      {-- quando impresso em formul�rio pega o cod do ultimo formul�rio --}
      if (qryContaBancarianCdTabTipoModeloImpBoleto.Value = 2) then
          iProximoBoleto := StrToInt(cProximoBoleto);

      frmMenu.Connection.BeginTrans;

      try

          qryTemp.First;

          while not qryTemp.eof do
          begin

              if (qryTempcFlgOk.Value = 1) then
              begin

                 PosicionaQuery(qryTitulo,qryTempnCdTitulo.AsString);

                 // gera os lan�amentos financeiros na conta
                 if (qryTitulocFlgBoletoImpresso.Value = 0) then
                 begin
                     {-- o lan�amento ser� gerado no momento da baixa do boleto --}
                     {SP_GERA_LANCTOFIN_EMISSAO_BOLETO.Close ;
                     SP_GERA_LANCTOFIN_EMISSAO_BOLETO.Parameters.ParamByName('@nCdDoctoFiscal').Value   := 0;
                     SP_GERA_LANCTOFIN_EMISSAO_BOLETO.Parameters.ParamByName('@nCdTituloEmissao').Value := qryTempnCdTitulo.Value ;
                     SP_GERA_LANCTOFIN_EMISSAO_BOLETO.Parameters.ParamByName('@nCdBanco').Value         := qryContaBancarianCdBanco.Value;
                     SP_GERA_LANCTOFIN_EMISSAO_BOLETO.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado ;
                     SP_GERA_LANCTOFIN_EMISSAO_BOLETO.ExecProc;}


                     qryDadosTerceiro.Close;
                     PosicionaQuery( qryDadosTerceiro, qryTitulonCdTerceiro.AsString ) ;

                     qryTitulo.Edit;
                     qryTitulonCdFormaPagtoTit.Value   := strToInt(frmMenu.LeParametro('FORMAPAGTOBOL'));
                     qryTitulonCdEspTit.Value          := strToInt(frmMenu.LeParametro('CDESPTIT-BOLETO'));
                     qryTitulonCdBancoPortador.Value   := qryContaBancarianCdBanco.Value;
                     qryTitulocFlgBoletoImpresso.Value := 1;
                     qryTituloiDiaCarenciaJuros.Value  := qryDadosTerceiroiDiasCarencia.Value;
                     qryTitulonPercTaxaJurosDia.Value  := ( qryDadosTerceironPercJurosMesBoleto.Value / 30 );
                     qryTitulonPercTaxaMulta.Value     := qryDadosTerceironPercMultaBoleto.Value;
                     qryTitulo.Post;

                     inc(iProximoBoleto);

                     qryContaBancaria.Edit;

                     {-- quando impresso em formulario atualiza o cod do proximo formul�rio a ser impresso --}
                     if (qryContaBancarianCdTabTipoModeloImpBoleto.Value = 2) then
                         qryContaBancariaiUltimoBoleto.Value := (iProximoBoleto -1);

                     qryContaBancaria.Post;
                 end;

              end;
              qryTemp.Next;
          end;

      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      cmdPreparaTemp.Execute;

      qryTemp.Close;
      qryTemp.Open;

      //busca instru��es da conta bancaria apos agrupar e/ou imprimr
      qryInstrucaoContaBancaria.Close;
      qryInstrucaoContaBancaria.Parameters.ParamByName('nCdContaBancaria').Value := qryContaBancarianCdContaBancaria.Value;
      qryInstrucaoContaBancaria.Open;

  end ;

  FreeAndNil(objForm);
  
end;

procedure TfrmImpBoleto.FormShow(Sender: TObject);
begin
  inherited;
  if (frmMenu.LeParametro('FORMAPAGTOBOL') <> '') then
  begin
      qryFormaPagamento.Close;
      PosicionaQuery(qryFormaPagamento,(frmMenu.LeParametro('FORMAPAGTOBOL')));
      edtFormaPagto.Text := intToStr(qryFormaPagamentonCdFormaPagto.Value);
  end;

  checkBox2.Checked := False;
  nCdTitulo := 0;

  qryTempInstrucaoTitulo.Open;
  cxPageControl1.ActivePageIndex := 0;

  qryInstrucaoContaBancaria.Open;

end;

procedure TfrmImpBoleto.btnAgruparTitulosClick(Sender: TObject);
var
  iAuxTotal    : integer ;
  iAux         : integer ;
  iTerceiro    : integer ;
  inCdTitulo   : integer ;
  cDtVencto    : String  ;
  nValBoleto   : Double  ;
  nCdMovimento : integer ;
  cObsTit      : String  ;
  cNrNf        : String  ;
  dDtVenctoAux : TDateTime;
  iQtdeVenctos : integer ;

  arrNumeroNf  : Array of String;

  objForm : TfrmImpBoletoAgrupamento ;

begin
  inherited;

  if (qryTemp.RecordCount = 0) then
  begin
      ShowMessage('Nenhum T�tulo Dispon�vel para Agrupamento.') ;
      exit ;
  end;

  iAuxTotal    := 0;
  iQtdeVenctos := 0;

  // Verifica regras para agrupamento
  iTerceiro := 0;

  qryTemp.First;
  
  while not qryTemp.eof do
  begin
      if (qryTempcFlgOk.Value = 1) then
      begin

          inc (iAuxTotal);

          if (iTerceiro = 0) then
              iTerceiro := qryTempnCdTerceiro.Value;

          if (iTerceiro <> qryTempnCdTerceiro.Value) then
          begin
              ShowMessage('T�tulos selecionados para Terceiros diferentes.') ;
              exit ;
          end;

          if (dDtVenctoAux <> qryTempdDtVenc.Value) then
          begin
              dDtVenctoAux := qryTempdDtVenc.Value ;
              inc( iQtdeVenctos ) ;
          end ;

      end;
      qryTemp.Next;
  end;

  if (iAuxTotal < 2) then
  begin
      ShowMessage('Selecione no m�nimo dois t�tulos para agrupamento.') ;
      exit ;
  end;

  if (iQtdeVenctos > 1) then
  begin
      if (MessageDlg('Os t�tulos selecionados para agrupamento tem data de vencimento diferente. Tem Certeza que deseja agrup�-los ?',mtConfirmation,[mbYes,mbNo],0) = MrNO) then
          exit ;

  end ;

  iAuxTotal := 0;


  qryTempInstrucaoTitulo.Open;

  if (qryTempInstrucaoTitulo.Eof) then
  begin
      if (trim(qryContaBancariacPrimeiraInstrucaoBoleto.Value) <> '') then
      begin
          qryTempInstrucaoTitulo.Insert;
          qryTempInstrucaoTitulocNmMensagem.Value := qryContaBancariacPrimeiraInstrucaoBoleto.Value;
          qryTempInstrucaoTitulo.Post;
      end ;

      if (trim(qryContaBancariacSegundaInstrucaoBoleto.Value) <> '') then
      begin
          qryTempInstrucaoTitulo.Insert;
          qryTempInstrucaoTitulocNmMensagem.Value := qryContaBancariacSegundaInstrucaoBoleto.Value;
          qryTempInstrucaoTitulo.Post;
      end ;
  end;


  // chama formulario com os itens selecionados
  objForm := TfrmImpBoletoAgrupamento.Create(nil);

  objForm.qryTempTitSelecionado.Close;
  objForm.qryTempTitSelecionado.Open;

  {-- deixa na tela o vencimento/num. doc. primeiro t�tulo --}
  if not objForm.qryTempTitSelecionado.eof then
  begin
      objForm.MaskEditVencto.Text      := objForm.qryTempTitSelecionadodDtVenc.asString ;
      objForm.MaskEditNumeroDocto.Text := objForm.qryTempTitSelecionadocNrTit.Value;
  end ;

  showForm(objForm,false);


  nCdTitulo := 0;
  if (objForm.bAgrupado) then
  begin
      nCdTitulo         :=  objForm.nCdTituloAgrupado;
      checkBox2.Checked := False;
  end;

  btnExibirTitulos.Click;

  if (objForm.bAgrupado) then
  begin

      //grava instrucao da obs para impressao posterior
      qryTituloInstrucaoBoleto.Open;
      qryTituloInstrucaoBoleto.Insert;
      qryTituloInstrucaoBoletonCdTituloInstrucaoBoleto.Value := frmMenu.fnProximoCodigo('TITULOINSTRUCAOBOLETO');
      qryTituloInstrucaoBoletonCdTitulo.Value                := qryTempnCdTitulo.Value;
      qryTituloInstrucaoBoletocFlgMensagemAgrupamento.Value  := 1;
      qryTituloInstrucaoBoletocMensagem.Value                := objForm.MaskEditObs.Text; ;
      qryTituloInstrucaoBoleto.Post;

      btnSelecionarTodosTit.Click;
      btnImprimirBoleto.Click;
  end;

  //busca instru��es da conta bancaria apos agrupar e/ou imprimr
  qryInstrucaoContaBancaria.Close;
  qryInstrucaoContaBancaria.Parameters.ParamByName('nCdContaBancaria').Value := qryContaBancarianCdContaBancaria.Value;
  qryInstrucaoContaBancaria.Open;

  nCdTitulo := 0;

  objForm.qryTempTitSelecionado.Close;

  FreeAndNil(objForm);

  cxPageControl1.ActivePageIndex := 0;

end;

procedure TfrmImpBoleto.edtContaBancariaExit(Sender: TObject);
begin
  inherited;

  if not qryContaBancaria.eof then
  begin

      qryInstrucaoContaBancaria.Close;
      qryInstrucaoContaBancaria.Parameters.ParamByName('nCdContaBancaria').Value := qryContaBancarianCdContaBancaria.Value;
      qryInstrucaoContaBancaria.Open;

  end ;

end;

procedure TfrmImpBoleto.qryInstrucaoContaBancariaBeforeDelete(
  DataSet: TDataSet);
begin
  if (qryInstrucaoContabancariacFlgMensagemPadrao.Value = 0) then
  begin
      MensagemAlerta('Mensagem Padr�o n�o pode ser excluida');
      Abort;
  end;
  inherited;

end;

procedure TfrmImpBoleto.qryInstrucaoContaBancariaBeforePost(
  DataSet: TDataSet);
begin
  if (qryInstrucaoContabancaria.State = dsInsert) then
      qryInstrucaoContabancariacFlgMensagemPadrao.Value := 1;

  if ((qryInstrucaoContabancaria.State = dsEdit) and (qryInstrucaoContabancariacFlgMensagemPadrao.Value = 0)) then
  begin
      MensagemAlerta('Mensagem Padr�o n�o pode ser alterada, pressione (ESC) para desfazer a altera��o.');
      Abort;
  end;

  inherited;

end;

procedure TfrmImpBoleto.InstruesdeCobrancadoTtulo1Click(Sender: TObject);
var
  objForm : TfrmImpBoletoInstrucaoTitulo ;
begin
  inherited;
 if (qryTemp.Active) then
  begin
      objForm := TfrmImpBoletoInstrucaoTitulo.Create(nil) ;
      objForm.qryTituloInstrucaoBoleto.Parameters.ParamByName('nCdTitulo').Value := qryTempnCdTitulo.Value;
      objForm.qryTituloInstrucaoBoleto.Open;
      objForm.nCdTitulo := qryTempnCdTitulo.Value;

      showForm( objForm , TRUE ) ;

  end ;

end;

procedure TfrmImpBoleto.edtContaBancariaBeforeLookup(Sender: TObject);
begin
  inherited;

  if ( DBEdit9.Text = '' ) then
  begin
      MensagemAlerta('Selecione a empresa.');
      edtEmpresa.SetFocus;
      abort;
  end ;

  edtContaBancaria.WhereAdicional.Clear;
  edtContaBancaria.WhereAdicional.Add('(cFlgCofre = 0 and cFlgEmiteBoleto = 1) and ContaBancaria.nCdEmpresa = ' + edtEmpresa.Text);
end;

procedure TfrmImpBoleto.edtEmpresaBeforePosicionaQry(Sender: TObject);
begin
  inherited;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

procedure TfrmImpBoleto.qryTempBeforePost(DataSet: TDataSet);
begin
  inherited;

  if ( qryTempcFlgOK.Value = 1 ) and ( qryTempcFlgPermitirEmissaoBoleto.Value = 0 ) then
  begin
      MensagemAlerta('Terceiro n�o configurado para emiss�o de boleto.') ;
      qryTempcFlgOK.Value := 0 ;
      abort;
  end ;

end;

initialization
    RegisterClass(TfrmImpBoleto) ;

end.
