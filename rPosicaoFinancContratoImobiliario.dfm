inherited rptPosicaoFinancContratoImobiliario: TrptPosicaoFinancContratoImobiliario
  Left = 53
  Top = 131
  Caption = 'Rel. Posi'#231#227'o Carteira Financiamento'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 62
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label2: TLabel [2]
    Left = 23
    Top = 72
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empreendimento'
  end
  object Label3: TLabel [3]
    Left = 70
    Top = 120
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
  end
  object Label4: TLabel [4]
    Left = 48
    Top = 96
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Bloco/Torre'
  end
  object Label5: TLabel [5]
    Left = 10
    Top = 144
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Vencimento'
  end
  object Label6: TLabel [6]
    Left = 188
    Top = 144
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtCdEmpresa: TER2LookupMaskEdit [8]
    Left = 106
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    CodigoLookup = 25
    QueryLookup = qryEmpresa
  end
  object edtCdEmpreendimento: TER2LookupMaskEdit [9]
    Left = 106
    Top = 64
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    OnExit = edtCdEmpreendimentoExit
    CodigoLookup = 185
    QueryLookup = qryEmpImobiliario
  end
  object edtCdTerceiro: TER2LookupMaskEdit [10]
    Left = 106
    Top = 112
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 4
    Text = '         '
    CodigoLookup = 0
    QueryLookup = qryTerceiro
  end
  object DBEdit1: TDBEdit [11]
    Tag = 1
    Left = 176
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 5
  end
  object DBEdit2: TDBEdit [12]
    Tag = 1
    Left = 176
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmEmpImobiliario'
    DataSource = DataSource2
    TabOrder = 6
  end
  object DBEdit3: TDBEdit [13]
    Tag = 1
    Left = 176
    Top = 112
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource3
    TabOrder = 7
  end
  object edtBlocoTorre: TER2LookupMaskEdit [14]
    Left = 106
    Top = 88
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 3
    Text = '         '
    OnBeforeLookup = edtBlocoTorreBeforeLookup
    CodigoLookup = 187
    QueryLookup = qryBloco
  end
  object DBEdit4: TDBEdit [15]
    Tag = 1
    Left = 176
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmBlocoEmpImobiliario'
    DataSource = DataSource4
    TabOrder = 8
  end
  object edtDtInicial: TMaskEdit [16]
    Left = 106
    Top = 136
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 9
    Text = '  /  /    '
  end
  object edtDtFinal: TMaskEdit [17]
    Left = 208
    Top = 136
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 10
    Text = '  /  /    '
  end
  object RadioGroup1: TRadioGroup [18]
    Left = 106
    Top = 168
    Width = 185
    Height = 105
    Caption = 'Modelo Relat'#243'rio'
    ItemIndex = 2
    Items.Strings = (
      'Resumido Por Empreendimento'
      'Resumido Por Contrato'
      'Di'#225'rio por Parcela')
    TabOrder = 11
  end
  object RadioGroup2: TRadioGroup [19]
    Left = 296
    Top = 168
    Width = 185
    Height = 105
    Caption = 'Modo Exibi'#231#227'o'
    ItemIndex = 0
    Items.Strings = (
      'Relat'#243'rio'
      'Planilha')
    TabOrder = 12
  end
  inherited ImageList1: TImageList
    Top = 216
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 616
    Top = 176
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 496
    Top = 272
  end
  object qryEmpImobiliario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM EmpImobiliario '
      '  WHERE nCdEmpImobiliario = :nPK')
    Left = 616
    Top = 232
    object qryEmpImobiliarionCdEmpImobiliario: TIntegerField
      FieldName = 'nCdEmpImobiliario'
    end
    object qryEmpImobiliarionCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpImobiliarionCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryEmpImobiliariocNmEmpImobiliario: TStringField
      FieldName = 'cNmEmpImobiliario'
      Size = 100
    end
    object qryEmpImobiliariodDtLancamento: TDateTimeField
      FieldName = 'dDtLancamento'
    end
    object qryEmpImobiliariocEndereco: TStringField
      FieldName = 'cEndereco'
      Size = 100
    end
  end
  object DataSource2: TDataSource
    DataSet = qryEmpImobiliario
    Left = 504
    Top = 280
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 616
    Top = 272
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object DataSource3: TDataSource
    DataSet = qryTerceiro
    Left = 512
    Top = 288
  end
  object qryBloco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpImobiliario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * FROM BlocoEmpImobiliario'
      'WHERE nCdBlocoEmpImobiliario = :nPK'
      'AND nCdEmpImobiliario = :nCdEmpImobiliario')
    Left = 616
    Top = 312
    object qryBloconCdBlocoEmpImobiliario: TAutoIncField
      FieldName = 'nCdBlocoEmpImobiliario'
      ReadOnly = True
    end
    object qryBloconCdEmpImobiliario: TIntegerField
      FieldName = 'nCdEmpImobiliario'
    end
    object qryBlococNmBlocoEmpImobiliario: TStringField
      FieldName = 'cNmBlocoEmpImobiliario'
      Size = 50
    end
    object qryBloconCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryBlocodDtHabitese: TDateTimeField
      FieldName = 'dDtHabitese'
    end
    object qryBlocodDtChaves: TDateTimeField
      FieldName = 'dDtChaves'
    end
    object qryBlocoiQtdeUnidade: TIntegerField
      FieldName = 'iQtdeUnidade'
    end
  end
  object DataSource4: TDataSource
    DataSet = qryBloco
    Left = 520
    Top = 296
  end
  object SPREL_POSICAO_CARTEIRAFINANC_IMOB_ANALITICA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SPREL_POSICAO_CARTEIRAFINANC_IMOB_ANALITICA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdEmpImobiliario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdBlocoEmpImobiliario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@cDtVencInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
      end
      item
        Name = '@cDtVencFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
      end>
    Left = 696
    Top = 264
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      '    IF (OBJECT_ID('#39'tempdb..#TempPosicaoCarteiraFinanc'#39') IS NULL)' +
      #13#10'    BEGIN'#13#10#13#10'        CREATE TABLE #TempPosicaoCarteiraFinanc (' +
      'nCdTitulo              int'#13#10'                                    ' +
      '            ,cNmTerceiro            VARCHAR(50)'#13#10'               ' +
      '                                 ,cNmEmpreendimentoBloco VARCHAR' +
      '(50)'#13#10'                                                ,dDtVenc  ' +
      '              DATETIME'#13#10'                                        ' +
      '        ,iParcela               int           default 0 not null' +
      #13#10'                                                ,cNmTipoParcel' +
      'a         VARCHAR(15)'#13#10'                                         ' +
      '       ,nValTit                DECIMAL(12,2) default 0 not null'#13 +
      #10'                                                ,nValCorrecao  ' +
      '         DECIMAL(12,2) default 0 not null'#13#10'                     ' +
      '                           ,nValJuroMulta          DECIMAL(12,2)' +
      ' default 0 not null'#13#10'                                           ' +
      '     ,nSaldoFinal            DECIMAL(12,2) default 0 not null'#13#10' ' +
      '                                               ,cNumContrato    ' +
      '       VARCHAR(15)'#13#10'                                            ' +
      '    ,cNrUnidade             VARCHAR(15))'#13#10#13#10'    END'#13#10#13#10'    TRUNC' +
      'ATE TABLE #TempPosicaoCarteiraFinanc'#13#10#13#10'IF (OBJECT_ID('#39'tempdb..#' +
      'TempPosicaoCarteiraEmpreendimento'#39') IS NULL)'#13#10'BEGIN'#13#10#13#10'    CREAT' +
      'E TABLE #TempPosicaoCarteiraEmpreendimento (cNmEmpreendimentoBlo' +
      'co VARCHAR(50)'#13#10'                                                ' +
      '    ,iQtdeParcVencida       int'#13#10'                               ' +
      '                     ,nValVencido            decimal(12,2) defau' +
      'lt 0 not null'#13#10'                                                 ' +
      '   ,nValTotalVencido       decimal(12,2) default 0 not null'#13#10'   ' +
      '                                                 ,iQtdeParcVence' +
      'r        int'#13#10'                                                  ' +
      '  ,nValTotalVencer        decimal(12,2) default 0 not null'#13#10'    ' +
      '                                                ,iQtdeParcTotal ' +
      '        int'#13#10'                                                   ' +
      ' ,nValTotal              decimal(12,2) default 0 not null)'#13#10#13#10'EN' +
      'D'#13#10#13#10'TRUNCATE TABLE #TempPosicaoCarteiraEmpreendimento'#13#10#13#10'IF (OB' +
      'JECT_ID('#39'tempdb..#TempPosicaoCarteiraContrato'#39') IS NULL)'#13#10'BEGIN'#13 +
      #10#13#10'    CREATE TABLE #TempPosicaoCarteiraContrato (cNumContrato  ' +
      '         VARCHAR(15)'#13#10'                                          ' +
      '    ,cNmTerceiro            VARCHAR(50)'#13#10'                       ' +
      '                       ,cNmEmpreendimentoBloco VARCHAR(50)'#13#10'    ' +
      '                                          ,cNrUnidade           ' +
      '  VARCHAR(15)'#13#10'                                              ,iQ' +
      'tdeParcVencida       int'#13#10'                                      ' +
      '        ,nValVencido            decimal(12,2) default 0 not null' +
      #13#10'                                              ,nValTotalVencid' +
      'o       decimal(12,2) default 0 not null'#13#10'                      ' +
      '                        ,iQtdeParcVencer        int'#13#10'           ' +
      '                                   ,nValTotalVencer        decim' +
      'al(12,2) default 0 not null'#13#10'                                   ' +
      '           ,nValTotal              decimal(12,2) default 0 not n' +
      'ull)'#13#10#13#10'END'#13#10#13#10'TRUNCATE TABLE #TempPosicaoCarteiraContrato '
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 704
    Top = 312
  end
  object qryTempParcelas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempPosicaoCarteiraFinanc'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempPosicaoCarteiraFinanc (nCdTitulo          ' +
        '    int'
      
        '                                            ,cNmTerceiro        ' +
        '    VARCHAR(50)'
      
        '                                            ,cNmEmpreendimentoBl' +
        'oco VARCHAR(50)'
      
        '                                            ,dDtVenc            ' +
        '    DATETIME'
      
        '                                            ,iParcela           ' +
        '    int           default 0 not null'
      
        '                                            ,cNmTipoParcela     ' +
        '    VARCHAR(15)'
      
        '                                            ,nValTit            ' +
        '    DECIMAL(12,2) default 0 not null'
      
        '                                            ,nValCorrecao       ' +
        '    DECIMAL(12,2) default 0 not null'
      
        '                                            ,nValJuroMulta      ' +
        '    DECIMAL(12,2) default 0 not null'
      
        '                                            ,nSaldoFinal        ' +
        '    DECIMAL(12,2) default 0 not null'
      
        '                                            ,cNumContrato       ' +
        '    VARCHAR(15)'
      
        '                                            ,cNrUnidade         ' +
        '    VARCHAR(15))'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #TempPosicaoCarteiraFinanc'
      ' ORDER BY dDtVenc')
    Left = 608
    Top = 376
    object qryTempParcelasnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTempParcelascNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTempParcelascNmEmpreendimentoBloco: TStringField
      FieldName = 'cNmEmpreendimentoBloco'
      Size = 50
    end
    object qryTempParcelasdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTempParcelasiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTempParcelascNmTipoParcela: TStringField
      FieldName = 'cNmTipoParcela'
      Size = 15
    end
    object qryTempParcelasnValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTempParcelasnValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      Precision = 12
      Size = 2
    end
    object qryTempParcelasnValJuroMulta: TBCDField
      FieldName = 'nValJuroMulta'
      Precision = 12
      Size = 2
    end
    object qryTempParcelasnSaldoFinal: TBCDField
      FieldName = 'nSaldoFinal'
      Precision = 12
      Size = 2
    end
    object qryTempParcelascNumContrato: TStringField
      FieldName = 'cNumContrato'
      Size = 15
    end
    object qryTempParcelascNrUnidade: TStringField
      FieldName = 'cNrUnidade'
      Size = 15
    end
  end
  object qryPopulaTempResumoEmpreendimento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempPosicaoCarteiraFinanc'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempPosicaoCarteiraFinanc (nCdTitulo          ' +
        '    int'
      
        '                                            ,cNmTerceiro        ' +
        '    VARCHAR(50)'
      
        '                                            ,cNmEmpreendimentoBl' +
        'oco VARCHAR(50)'
      
        '                                            ,dDtVenc            ' +
        '    DATETIME'
      
        '                                            ,iParcela           ' +
        '    int           default 0 not null'
      
        '                                            ,cNmTipoParcela     ' +
        '    VARCHAR(15)'
      
        '                                            ,nValTit            ' +
        '    DECIMAL(12,2) default 0 not null'
      
        '                                            ,nValCorrecao       ' +
        '    DECIMAL(12,2) default 0 not null'
      
        '                                            ,nValJuroMulta      ' +
        '    DECIMAL(12,2) default 0 not null'
      
        '                                            ,nSaldoFinal        ' +
        '    DECIMAL(12,2) default 0 not null'
      
        '                                            ,cNumContrato       ' +
        '    VARCHAR(15)'
      
        '                                            ,cNrUnidade         ' +
        '    VARCHAR(15))'
      ''
      'END'
      ''
      
        'IF (OBJECT_ID('#39'tempdb..#TempPosicaoCarteiraEmpreendimento'#39') IS N' +
        'ULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempPosicaoCarteiraEmpreendimento (cNmEmpreend' +
        'imentoBloco VARCHAR(50)'
      
        '                                                    ,iQtdeParcVe' +
        'ncida       int'
      
        '                                                    ,nValVencido' +
        '            decimal(12,2) default 0 not null'
      
        '                                                    ,nValTotalVe' +
        'ncido       decimal(12,2) default 0 not null'
      
        '                                                    ,iQtdeParcVe' +
        'ncer        int'
      
        '                                                    ,nValTotalVe' +
        'ncer        decimal(12,2) default 0 not null'
      
        '                                                    ,iQtdeParcTo' +
        'tal         int'
      
        '                                                    ,nValTotal  ' +
        '            decimal(12,2) default 0 not null)'
      ''
      'END'
      ''
      
        'INSERT INTO #TempPosicaoCarteiraEmpreendimento (cNmEmpreendiment' +
        'oBloco '
      
        '                                               ,iQtdeParcVencida' +
        '       '
      
        '                                               ,nValVencido     ' +
        '       '
      
        '                                               ,nValTotalVencido' +
        '       '
      '                                               ,iQtdeParcVencer'
      
        '                                               ,nValTotalVencer ' +
        '       '
      
        '                                               ,iQtdeParcTotal  ' +
        '       '
      '                                               ,nValTotal)'
      
        '                                         SELECT cNmEmpreendiment' +
        'oBloco'
      
        '                                               ,Sum(CASE WHEN dD' +
        'tVenc < dbo.fn_OnlyDate(GetDate()) THEN 1'
      
        '                                                           ELSE ' +
        '0'
      '                                                      END)'
      
        '                                               ,Sum(CASE WHEN dD' +
        'tVenc < dbo.fn_OnlyDate(GetDate()) THEN nValTit'
      '                                                         ELSE 0'
      '                                                    END)'
      
        '                                               ,Sum(CASE WHEN dD' +
        'tVenc < dbo.fn_OnlyDate(GetDate()) THEN nSaldoFinal'
      '                                                         ELSE 0'
      '                                                    END)'
      
        '                                               ,Sum(CASE WHEN dD' +
        'tVenc >= dbo.fn_OnlyDate(GetDate()) THEN 1'
      
        '                                                           ELSE ' +
        '0'
      '                                                      END)'
      
        '                                               ,Sum(CASE WHEN dD' +
        'tVenc >= dbo.fn_OnlyDate(GetDate()) THEN nSaldoFinal'
      '                                                         ELSE 0'
      '                                                    END)'
      '                                               ,Count(1)'
      '                                               ,Sum(nSaldoFinal)'
      
        '                                           FROM #TempPosicaoCart' +
        'eiraFinanc'
      
        '                                          GROUP BY cNmEmpreendim' +
        'entoBloco')
    Left = 616
    Top = 416
  end
  object qryPopulaTempResumoContrato: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempPosicaoCarteiraFinanc'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempPosicaoCarteiraFinanc (nCdTitulo          ' +
        '    int'
      
        '                                            ,cNmTerceiro        ' +
        '    VARCHAR(50)'
      
        '                                            ,cNmEmpreendimentoBl' +
        'oco VARCHAR(50)'
      
        '                                            ,dDtVenc            ' +
        '    DATETIME'
      
        '                                            ,iParcela           ' +
        '    int           default 0 not null'
      
        '                                            ,cNmTipoParcela     ' +
        '    VARCHAR(15)'
      
        '                                            ,nValTit            ' +
        '    DECIMAL(12,2) default 0 not null'
      
        '                                            ,nValCorrecao       ' +
        '    DECIMAL(12,2) default 0 not null'
      
        '                                            ,nValJuroMulta      ' +
        '    DECIMAL(12,2) default 0 not null'
      
        '                                            ,nSaldoFinal        ' +
        '    DECIMAL(12,2) default 0 not null'
      
        '                                            ,cNumContrato       ' +
        '    VARCHAR(15)'
      
        '                                            ,cNrUnidade         ' +
        '    VARCHAR(15))'
      ''
      'END'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempPosicaoCarteiraContrato'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempPosicaoCarteiraContrato (cNumContrato     ' +
        '      VARCHAR(15)'
      
        '                                              ,cNmTerceiro      ' +
        '      VARCHAR(50)'
      
        '                                              ,cNmEmpreendimento' +
        'Bloco VARCHAR(50)'
      
        '                                              ,cNrUnidade       ' +
        '      VARCHAR(15)'
      
        '                                              ,iQtdeParcVencida ' +
        '      int'
      
        '                                              ,nValVencido      ' +
        '      decimal(12,2) default 0 not null'
      
        '                                              ,nValTotalVencido ' +
        '      decimal(12,2) default 0 not null'
      
        '                                              ,iQtdeParcVencer  ' +
        '      int'
      
        '                                              ,nValTotalVencer  ' +
        '      decimal(12,2) default 0 not null'
      
        '                                              ,nValTotal        ' +
        '      decimal(12,2) default 0 not null)'
      ''
      'END'
      ''
      
        'INSERT INTO #TempPosicaoCarteiraContrato (cNumContrato          ' +
        ' '
      
        '                                         ,cNmTerceiro           ' +
        ' '
      
        '                                         ,cNmEmpreendimentoBloco' +
        ' '
      '                                         ,cNrUnidade'
      
        '                                         ,iQtdeParcVencida      ' +
        ' '
      
        '                                         ,nValVencido           ' +
        ' '
      
        '                                         ,nValTotalVencido      ' +
        ' '
      '                                         ,iQtdeParcVencer'
      
        '                                         ,nValTotalVencer       ' +
        ' '
      '                                         ,nValTotal)'
      '                                 SELECT cNumContrato           '
      '                                       ,cNmTerceiro            '
      '                                       ,cNmEmpreendimentoBloco '
      '                                       ,cNrUnidade             '
      
        '                                       ,Sum(CASE WHEN dDtVenc < ' +
        'dbo.fn_OnlyDate(GetDate()) THEN 1'
      '                                                   ELSE 0'
      '                                              END)'
      
        '                                       ,Sum(CASE WHEN dDtVenc < ' +
        'dbo.fn_OnlyDate(GetDate()) THEN nValTit'
      '                                                 ELSE 0'
      '                                            END)'
      
        '                                       ,Sum(CASE WHEN dDtVenc < ' +
        'dbo.fn_OnlyDate(GetDate()) THEN nSaldoFinal'
      '                                                 ELSE 0'
      '                                            END)'
      
        '                                       ,Sum(CASE WHEN dDtVenc >=' +
        ' dbo.fn_OnlyDate(GetDate()) THEN 1'
      '                                                   ELSE 0'
      '                                              END)'
      
        '                                       ,Sum(CASE WHEN dDtVenc >=' +
        ' dbo.fn_OnlyDate(GetDate()) THEN nSaldoFinal'
      '                                                 ELSE 0'
      '                                            END)'
      '                                       ,Sum(nSaldoFinal)'
      
        '                                   FROM #TempPosicaoCarteiraFina' +
        'nc'
      
        '                                  GROUP BY cNumContrato         ' +
        '  '
      
        '                                          ,cNmTerceiro          ' +
        '  '
      
        '                                          ,cNmEmpreendimentoBloc' +
        'o '
      '                                          ,cNrUnidade '
      '')
    Left = 656
    Top = 416
  end
end
