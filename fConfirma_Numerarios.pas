unit fConfirma_Numerarios;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBCtrls, ADODB, StdCtrls, Mask;

type
  TfrmConfirma_Numerarios = class(TfrmProcesso_Padrao)
    EdtCodRemessa: TMaskEdit;
    Label1: TLabel;
    edtContaDinheiro: TMaskEdit;
    qryContaBancariaDin: TADOQuery;
    qryContaBancariaDinnCdContaBancaria: TIntegerField;
    qryContaBancariaDinnCdConta: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    edtValDinheiro: TMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    edtValCheque: TMaskEdit;
    edtContaCheque: TMaskEdit;
    Label5: TLabel;
    edtContaCartao: TMaskEdit;
    edtValCartao: TMaskEdit;
    Label6: TLabel;
    Label7: TLabel;
    edtContaCrediario: TMaskEdit;
    edtValCrediario: TMaskEdit;
    Label8: TLabel;
    Label9: TLabel;
    qryContaBancariaCheque: TADOQuery;
    qryContaBancariaChequenCdContaBancaria: TIntegerField;
    qryContaBancariaChequenCdConta: TStringField;
    qryContaBancariaCartao: TADOQuery;
    qryContaBancariaCartaonCdContaBancaria: TIntegerField;
    qryContaBancariaCartaonCdConta: TStringField;
    qryContaBancariaCrediario: TADOQuery;
    qryContaBancariaCrediarionCdContaBancaria: TIntegerField;
    qryContaBancariaCrediarionCdConta: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    DBEdit4: TDBEdit;
    DataSource4: TDataSource;
    edtContaDestino: TMaskEdit;
    Label10: TLabel;
    qryContaDestino: TADOQuery;
    qryContaDestinonCdContaBancaria: TIntegerField;
    qryContaDestinonCdConta: TStringField;
    DBEdit5: TDBEdit;
    DataSource5: TDataSource;
    qryRemessaNum: TADOQuery;
    qryRemessaNumnCdRemessaNum: TIntegerField;
    qryRemessaNumnCdEmpresa: TIntegerField;
    qryRemessaNumnCdLoja: TIntegerField;
    qryRemessaNumnCdContaOrigem: TIntegerField;
    qryRemessaNumnCdContaDestino: TIntegerField;
    qryRemessaNumnValDinheiro: TBCDField;
    qryRemessaNumnCdUsuarioCad: TIntegerField;
    qryRemessaNumdDtRemessa: TDateTimeField;
    qryRemessaNumdDtFech: TDateTimeField;
    qryRemessaNumdDtCancel: TDateTimeField;
    qryRemessaNumcOBS: TStringField;
    qryRemessaNumdDtConfirma: TDateTimeField;
    qryRemessaNumnCdUsuarioConfirma: TIntegerField;
    qryRemessaNumnValCheque: TBCDField;
    qryRemessaNumnValCartao: TBCDField;
    qryRemessaNumnValCrediario: TBCDField;
    SP_CONFIRMA_NUMERARIO: TADOStoredProc;
    procedure edtContaDinheiroExit(Sender: TObject);
    procedure edtContaChequeExit(Sender: TObject);
    procedure edtContaCartaoExit(Sender: TObject);
    procedure edtContaCrediarioExit(Sender: TObject);
    procedure EdtCodRemessaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtContaDestinoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtValDinheiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtContaDinheiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtValChequeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtContaChequeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtValCartaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtContaCartaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtValCrediarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtContaCrediarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure edtValDinheiroExit(Sender: TObject);
    procedure edtValChequeExit(Sender: TObject);
    procedure edtValCartaoExit(Sender: TObject);
    procedure edtValCrediarioExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtContaDestinoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfirma_Numerarios: TfrmConfirma_Numerarios;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmConfirma_Numerarios.edtContaDinheiroExit(Sender: TObject);
begin
  inherited;

  qryContaBancariaDin.Close ;
  PosicionaQuery(qryContaBancariaDin, edtContaDinheiro.Text) ;

end;

procedure TfrmConfirma_Numerarios.edtContaChequeExit(Sender: TObject);
begin
  inherited;

  qryContaBancariaCheque.Close ;
  PosicionaQuery(qryContaBancariaCheque, edtContaCheque.Text) ;

end;

procedure TfrmConfirma_Numerarios.edtContaCartaoExit(Sender: TObject);
begin
  inherited;

  qryContaBancariaCartao.Close ;
  PosicionaQuery(qryContaBancariaCartao, edtContaCartao.Text) ;

end;

procedure TfrmConfirma_Numerarios.edtContaCrediarioExit(Sender: TObject);
begin
  inherited;

  qryContaBancariaCrediario.Close ;
  PosicionaQuery(qryContaBancariaCrediario, edtContaCrediario.Text) ;

end;

procedure TfrmConfirma_Numerarios.EdtCodRemessaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
{  case key of
      vk_return : edtContaDestino.SetFocus;
  end ;}

end;

procedure TfrmConfirma_Numerarios.edtContaDestinoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : integer ;
begin
  inherited;
  case key of
{      vk_return : edtValDinheiro.SetFocus;}
      vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdLoja = ' + IntToStr(frmMenu.nCdLojaAtiva));

            If (nPK > 0) then
            begin
                edtContaDestino.Text := IntToStr(nPK) ;
                PosicionaQuery(qryContaDestino, edtContaDestino.Text) ;
            end ;

      end ;

  end ;

end;

procedure TfrmConfirma_Numerarios.edtValDinheiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      vk_return : edtContaDinheiro.SetFocus;
  end ;

end;

procedure TfrmConfirma_Numerarios.edtContaDinheiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : integer ;
begin
  inherited;
  case key of
      vk_return : edtValCheque.SetFocus;
      vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdLoja = ' + IntToStr(frmMenu.nCdLojaAtiva));

            If (nPK > 0) then
            begin
                edtContaDinheiro.Text := IntToStr(nPK) ;
                PosicionaQuery(qryContaBancariaDin, IntToStr(nPK)) ;
            end ;

      end ;
  end ;

end;

procedure TfrmConfirma_Numerarios.edtValChequeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      vk_return : edtContaCheque.SetFocus;
  end ;

end;

procedure TfrmConfirma_Numerarios.edtContaChequeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : integer ;
begin
  inherited;
  case key of
      vk_return : edtValCartao.SetFocus;
      vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdLoja = ' + IntToStr(frmMenu.nCdLojaAtiva));

            If (nPK > 0) then
            begin
                edtContaCheque.Text := IntToStr(nPK) ;
                PosicionaQuery(qryContaBancariaCheque, IntToStr(nPK)) ;
            end ;

      end ;
  end ;

end;

procedure TfrmConfirma_Numerarios.edtValCartaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      vk_return : edtContaCartao.SetFocus;
  end ;

end;

procedure TfrmConfirma_Numerarios.edtContaCartaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : integer ;
begin
  inherited;
  case key of
      vk_return : edtValCrediario.SetFocus;
      vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdLoja = ' + IntToStr(frmMenu.nCdLojaAtiva));

            If (nPK > 0) then
            begin
                edtContaCartao.Text := IntToStr(nPK) ;
                PosicionaQuery(qryContaBancariaCartao, IntToStr(nPK)) ;
            end ;

      end ;
  end ;

end;

procedure TfrmConfirma_Numerarios.edtValCrediarioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      vk_return : edtContaCrediario.SetFocus;
  end ;

end;

procedure TfrmConfirma_Numerarios.edtContaCrediarioKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : integer ;
begin
  inherited;
  case key of
      vk_return : edtCodRemessa.SetFocus;
      vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdLoja = ' + IntToStr(frmMenu.nCdLojaAtiva));

            If (nPK > 0) then
            begin
                edtContaCrediario.Text := IntToStr(nPK) ;
                PosicionaQuery(qryContaBancariaCrediario, IntToStr(nPK)) ;
            end ;

      end ;
  end ;

end;

procedure TfrmConfirma_Numerarios.FormShow(Sender: TObject);
begin
  inherited;

  qryContaDestino.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva;
  qryContaBancariaDin.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva;
  qryContaBancariaCheque.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva;
  qryContaBancariaCartao.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva;
  qryContaBancariaCrediario.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva;

end;

procedure TfrmConfirma_Numerarios.edtValDinheiroExit(Sender: TObject);
begin
  inherited;

  if (edtValDinheiro.Text = '') then
      edtValDinheiro.Text := '0' ;

  try
      strToFloat(edtValDinheiro.Text) ;
  except
      MensagemErro('Valor inv�lido.') ;
      edtValDinheiro.Text := '' ;
      edtValDinheiro.setFocus;
      exit ;
  end ;

  edtValDinheiro.Text := FormatCurr('0.00',StrToFloat(EdtValDinheiro.Text)) ;

end;

procedure TfrmConfirma_Numerarios.edtValChequeExit(Sender: TObject);
begin
  inherited;
  if (edtValCheque.Text = '') then
      edtValCheque.Text := '0' ;

  try
      strToFloat(edtValCheque.Text) ;
  except
      MensagemErro('Valor inv�lido.') ;
      edtValCheque.Text := '' ;
      edtValCheque.setFocus;
      exit ;
  end ;

  edtValCheque.Text := FormatCurr('0.00',StrToFloat(edtValCheque.Text)) ;

end;

procedure TfrmConfirma_Numerarios.edtValCartaoExit(Sender: TObject);
begin
  inherited;
  if (edtValCartao.Text = '') then
      edtValCartao.Text := '0' ;

  try
      strToFloat(edtValCartao.Text) ;
  except
      MensagemErro('Valor inv�lido.') ;
      edtValCartao.Text := '' ;
      edtValCartao.setFocus;
      exit ;
  end ;

  edtValCartao.Text := FormatCurr('0.00',StrToFloat(edtValCartao.Text)) ;

end;

procedure TfrmConfirma_Numerarios.edtValCrediarioExit(Sender: TObject);
begin
  inherited;
  if (edtValCrediario.Text = '') then
      edtValCrediario.Text := '0' ;

  try
      strToFloat(edtValCrediario.Text) ;
  except
      MensagemErro('Valor inv�lido.') ;
      edtValCrediario.Text := '' ;
      edtValCrediario.setFocus;
      exit ;
  end ;

  edtValCrediario.Text := FormatCurr('0.00',StrToFloat(edtValCrediario.Text)) ;

end;

procedure TfrmConfirma_Numerarios.ToolButton1Click(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryRemessaNum, EdtCodRemessa.Text) ;

  If not qryRemessaNum.Active or qryRemessaNum.Eof then
  begin
      MensagemAlerta('Remessa Inv�lida.') ;
      EdtCodRemessa.SetFocus;
      exit ;
  end ;

  if (qryRemessaNumdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Remessa cancelada.') ;
      exit ;
  end ;

  if (qryRemessaNumdDtFech.AsString = '') then
  begin
      MensagemAlerta('Remessa n�o foi finalizada.') ;
      exit ;
  end ;

  if (qryRemessaNumdDtConfirma.AsString <> '') then
  begin
      MensagemAlerta('Remessa j� confirmada.') ;
      exit ;
  end ;

  if (Trim(edtContaDestino.Text) = '') or (DbEdit5.Text = '') then
  begin
      MensagemAlerta('Informe a conta para destino dos numer�rios.') ;
      edtContaDestino.SetFocus;
      exit ;
  end ;

  if (Trim(edtContaDestino.Text) <> qryRemessaNumnCdContaDestino.AsString) then
  begin
      MensagemAlerta('Conta de destino n�o confere com a conta de destino da remessa.') ;
      edtContaDestino.SetFocus;
      exit ;
  end ;

  if (Trim(edtValDinheiro.Text) = '') then
      edtValDinheiro.Text := '0,00' ;

  if (Trim(edtValCheque.Text) = '') then
      edtValCheque.Text := '0,00' ;

  if (Trim(edtValCartao.Text) = '') then
      edtValCartao.Text := '0,00' ;

  if (Trim(edtValCrediario.Text) = '') then
      edtValCrediario.Text := '0,00' ;

  If (frmMenu.TBRound(qryRemessaNumnValDinheiro.Value,2) <> frmMenu.TBRound(StrToFloat(edtValDinheiro.Text),2)) then
  begin
      MensagemAlerta('Diverg�ncia no valor do dinheiro, favor conferir.' +#13#13 + 'Utilize a c�pia da remessa para confer�ncia.') ;
      exit ;
  end ;

  If (frmMenu.TBRound(qryRemessaNumnValCheque.Value,2) <> frmMenu.TBRound(StrToFloat(edtValCheque.Text),2)) then
  begin
      MensagemAlerta('Diverg�ncia no valor dos cheques, favor conferir.' +#13#13 + 'Utilize a c�pia da remessa para confer�ncia.') ;
      exit ;
  end ;

  If (frmMenu.TBRound(qryRemessaNumnValCartao.Value,2) <> frmMenu.TBRound(StrToFloat(edtValCartao.Text),2)) then
  begin
      MensagemAlerta('Diverg�ncia no valor do cart�o, favor conferir.' +#13#13 + 'Utilize a c�pia da remessa para confer�ncia.') ;
      exit ;
  end ;

  If (frmMenu.TBRound(qryRemessaNumnValCrediario.Value,2) <> frmMenu.TBRound(StrToFloat(edtValCrediario.Text),2)) then
  begin
      MensagemAlerta('Diverg�ncia no valor do credi�rio, favor conferir.' +#13#13 + 'Utilize a c�pia da remessa para confer�ncia.') ;
      exit ;
  end ;

  case MessageDlg('Confirma o recebimento de todo os documentos desta transfer�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_CONFIRMA_NUMERARIO.Close ;
      SP_CONFIRMA_NUMERARIO.Parameters.ParamByName('@nCdRemessaNum').Value     := frmMenu.ConvInteiro(edtCodRemessa.Text);
      SP_CONFIRMA_NUMERARIO.Parameters.ParamByName('@nCdContaDestino').Value   := frmMenu.ConvInteiro(edtContaDestino.Text);
      SP_CONFIRMA_NUMERARIO.Parameters.ParamByName('@nCdContaDin').Value       := frmMenu.ConvInteiro(edtContaDinheiro.Text);
      SP_CONFIRMA_NUMERARIO.Parameters.ParamByName('@nCdContaCheque').Value    := frmMenu.ConvInteiro(edtContaCheque.Text);
      SP_CONFIRMA_NUMERARIO.Parameters.ParamByName('@nCdContaCartao').Value    := frmMenu.ConvInteiro(edtContaCartao.Text);
      SP_CONFIRMA_NUMERARIO.Parameters.ParamByName('@nCdContaCrediario').Value := frmMenu.ConvInteiro(edtContaCrediario.Text);
      SP_CONFIRMA_NUMERARIO.Parameters.ParamByName('@nCdUsuario').Value        := frmMenu.nCdUsuarioLogado;
      SP_CONFIRMA_NUMERARIO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Transa��o realizada com sucesso.') ;

  edtCodRemessa.Text     := '' ;
  edtContaDestino.Text   := '' ;

  edtValDinheiro.Text    := '' ;
  edtContaDinheiro.Text  := '' ;

  edtContaCheque.Text    := '' ;
  edtValCheque.Text      := '' ;

  edtContaCartao.Text    := '' ;
  edtValCartao.Text      := '' ;

  edtContaCrediario.Text := '' ;
  edtValCrediario.Text   := '' ;

  qryContaDestino.Close ;
  qryContaBancariaDin.Close ;
  qryContaBancariaCheque.Close ;
  qryContaBancariaCartao.Close ;
  qryContaBancariaCrediario.Close ;

  edtCodRemessa.SetFocus;


end;

procedure TfrmConfirma_Numerarios.edtContaDestinoExit(Sender: TObject);
begin
  inherited;

  qryContaDestino.Close ;
  PosicionaQuery(qryContaDestino, edtContaDestino.Text) ;
  
end;

initialization
    RegisterClass(TfrmConfirma_Numerarios) ;
    
end.
