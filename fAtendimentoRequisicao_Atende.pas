unit fAtendimentoRequisicao_Atende;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, cxPC, cxControls, DBGridEhGrouping, GridsEh, DBGridEh,
  ToolCtrlsEh;

type
  TfrmAtendimentoRequisicao_Atende = class(TfrmProcesso_Padrao)
    qryPreparaTemp: TADOQuery;
    qryPopulaTemp: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    dsItens: TDataSource;
    qryPopulaTempnCdItemRequisicao: TIntegerField;
    qryPopulaTempnCdProduto: TIntegerField;
    qryPopulaTempcNmProduto: TStringField;
    qryPopulaTempcUnidadeMedida: TStringField;
    qryPopulaTempnQtdeSaldo: TBCDField;
    qryPopulaTempnQtdeAtend: TBCDField;
    qryPopulaTempnCdLocalEstoque: TIntegerField;
    qryPopulaTempcReferencia: TStringField;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryPopulaTempcNmLocalEstoque: TStringField;
    SP_PROCESSA_ATENDIMENTO_REQUISICAO: TADOStoredProc;
    btRegMovLote: TToolButton;
    ToolButton5: TToolButton;
    qryPopulaTempnCdTabTipoModoGestaoProduto: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure qryPopulaTempnCdLocalEstoqueChange(Sender: TField);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryPopulaTempBeforePost(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure qryPopulaTempCalcFields(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btRegMovLoteClick(Sender: TObject);
    procedure qryPopulaTempAfterScroll(DataSet: TDataSet);
    procedure qryPopulaTempAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    cNmCentroProdutivo : string ;
    nCdRequisicao      : integer ;
    cOBSMovLote        : string ;
  end;

var
  frmAtendimentoRequisicao_Atende: TfrmAtendimentoRequisicao_Atende;

implementation

uses fMenu, fLookup_Padrao, fRegistroMovLote;

{$R *.dfm}

procedure TfrmAtendimentoRequisicao_Atende.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.SetFocus;
  DBGridEh1.Col := 7 ;

  qryLocalEstoque.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

end;

procedure TfrmAtendimentoRequisicao_Atende.qryPopulaTempnCdLocalEstoqueChange(
  Sender: TField);
begin
  inherited;

  qryPopulaTempcNmLocalEstoque.Value := '' ;

  qryLocalEstoque.Close ;

  PosicionaQuery(qryLocalEstoque , qryPopulaTempnCdLocalEstoque.AsString ) ;

  if not qryLocalEstoque.eof then
  begin
      qryPopulaTempcNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;
  end ;

end;

procedure TfrmAtendimentoRequisicao_Atende.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmAtendimentoRequisicao_Atende.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmAtendimentoRequisicao_Atende.qryPopulaTempBeforePost(
  DataSet: TDataSet);
begin

  if (qryPopulaTempnQtdeAtend.Value < 0) then
  begin
      MensagemAlerta('A quantidade atendida n�o pode ser menor que zero.') ;
      abort ;
  end ;

  if (qryPopulaTempnQtdeAtend.Value > 0) and (qryPopulaTempcNmLocalEstoque.Value = '') then
  begin
      MensagemAlerta('Informe o local de estoque de sa�da do item.') ;
      abort ;
  end ;

  if (qryPopulaTempnQtdeAtend.asString = '') then
      qryPopulaTempnQtdeAtend.Value := 0 ;

  inherited;


end;

procedure TfrmAtendimentoRequisicao_Atende.ToolButton1Click(
  Sender: TObject);
var
  objForm              : TfrmRegistroMovLote ;
  nQtdeAtend,nQtdeLote : double ;
begin
  inherited;

  nQtdeAtend := 0 ;

  qryPopulaTemp.First;

  while not qryPopulaTemp.eof do
  begin
      nQtdeAtend := nQtdeAtend + qryPopulaTempnQtdeAtend.Value ;

      if (qryPopulaTempnQtdeAtend.Value > 0) and (qryPopulaTempcNmLocalEstoque.Value = '') then
      begin
          MensagemAlerta('Informe o local de estoque de sa�da do item.') ;
          abort ;
      end ;

      {-- se o item for gerenciado por lote/serial, faz a conferencia do valor informado --}

      if (qryPopulaTempnQtdeAtend.Value > 0) and (qryPopulaTempnCdTabTipoModoGestaoProduto.Value > 1) then
      begin

          objForm := TfrmRegistroMovLote.Create( Self ) ;

          try

              nQtdeLote := objForm.retornaQuantidadeRegistradaTemporaria( qryPopulaTempnCdProduto.Value
                                                                        , 3 {-- Requisi��o Estoque --}
                                                                        , qryPopulaTempnCdItemRequisicao.Value ) ;

              {-- quando o produto n�o tem lote/serial controlados a fun��o retorna -1 --}
              if (nQtdeLote <> -1) then
              begin

                  if ( nQtdeLote <> qryPopulaTempnQtdeAtend.Value ) then
                  begin
                      MensagemAlerta('A quantidade especificada no Registro de Lote/Serial n�o confere com o total atendido.' + #13#13 + 'Produto : ' + qryPopulaTempnCdProduto.asString + ' - ' + Trim(qryPopulaTempcNmProduto.Value)) ;
                      btRegMovLote.Click;
                      abort ;
                  end ;

              end ;

          finally
              freeAndNil( objForm ) ;
          end ;

      end ;

      qryPopulaTemp.Next;
      
  end ;

  qryPopulaTemp.First;

  if (nQtdeAtend = 0) then
  begin
      MensagemAlerta('Nenhum item com quantidade para atendimento informada.') ;
      abort ;
  end ;

  if (MessageDlg('Confirma a sa�da destes itens para atendimento desta requisi��o ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  if ( cNmCentroProdutivo <> '' ) then
      if (MessageDlg('Os itens ser�o transferidos para o estoque do centro produtivo ' + cNmCentroProdutivo + '. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

  frmMenu.Connection.BeginTrans;

  try

      SP_PROCESSA_ATENDIMENTO_REQUISICAO.Close;
      SP_PROCESSA_ATENDIMENTO_REQUISICAO.Parameters.ParamByName('@nCdRequisicao').Value := nCdRequisicao ;
      SP_PROCESSA_ATENDIMENTO_REQUISICAO.Parameters.ParamByName('@nCdUsuario').Value    := frmMenu.nCdUsuarioLogado;
      SP_PROCESSA_ATENDIMENTO_REQUISICAO.ExecProc;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans;

  Close ;

end;

procedure TfrmAtendimentoRequisicao_Atende.qryPopulaTempCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if ( qryPopulaTempcNmLocalEstoque.Value = '' ) then
  begin

      qryLocalEstoque.Close ;
      PosicionaQuery(qryLocalEstoque , qryPopulaTempnCdLocalEstoque.AsString ) ;

      if not qryLocalEstoque.eof then
          qryPopulaTempcNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;
          
  end ;

end;

procedure TfrmAtendimentoRequisicao_Atende.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdLocalEstoque') then
        begin

            if (qryPopulaTemp.State = dsBrowse) then
                qryPopulaTemp.Edit ;


            if (qryPopulaTemp.State = dsInsert) or (qryPopulaTemp.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(87,'EXISTS(SELECT 1 FROM UsuarioLocalEstoque ULE WHERE ULE.nCdLocalEstoque = LocalEstoque.nCdLocalEstoque AND ULE.nCdUsuario = ' + intToStr( frmMenu.nCdUsuarioLogado ) + ')');

                If (nPK > 0) then
                    qryPopulaTempnCdLocalEstoque.Value := nPK ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmAtendimentoRequisicao_Atende.btRegMovLoteClick(
  Sender: TObject);
var
    objForm : TfrmRegistroMovLote ;
begin

  if (qryPopulaTemp.State = dsEdit) then
      qryPopulaTemp.Post ;

  if (not qryPopulaTemp.Active) or (qryPopulaTemp.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum Registro Ativo.') ;
      abort ;
  end ;

  {-- produto n�o � gerenciado nem por numero de lote nem por numero de s�rie --}
  if (qryPopulaTempnCdTabTipoModoGestaoProduto.Value = 1) then
      exit ;

  if (qryPopulaTempnCdLocalEstoque.Value = 0) then
  begin
      MensagemAlerta('Informe o local de estoque de sa�da do produto.') ;
      exit ;
  end ;

  if (qryPopulaTempnQtdeAtend.Value <= 0) then
  begin
      MensagemAlerta('Quantidade atendida n�o informada.') ;
      exit ;
  end ;

  objForm := TfrmRegistroMovLote.Create( Self ) ;

  objForm.registraMovLote( qryPopulaTempnCdProduto.Value
                         , 0
                         , 3 {-- Requisi��o de Estoque --}
                         , qryPopulaTempnCdItemRequisicao.Value
                         , qryPopulaTempnCdLocalEstoque.Value
                         , qryPopulaTempnQtdeAtend.Value
                         , cOBSMovLote
                         , FALSE
                         , FALSE
                         , FALSE) ;

  freeAndNil( objForm ) ;

end;

procedure TfrmAtendimentoRequisicao_Atende.qryPopulaTempAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  btRegMovLote.Enabled := (qryPopulaTempnCdTabTipoModoGestaoProduto.Value > 1) ;
  
end;

procedure TfrmAtendimentoRequisicao_Atende.qryPopulaTempAfterPost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryPopulaTempnQtdeAtend.Value > 0) and (qryPopulaTempnCdTabTipoModoGestaoProduto.Value > 1) then
      btRegMovLote.Click;

end;

end.
