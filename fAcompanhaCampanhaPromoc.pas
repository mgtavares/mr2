unit fAcompanhaCampanhaPromoc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, Menus, DB, ADODB, GridsEh, DBGridEh, ImgList,
  ComCtrls, ToolWin, ExtCtrls, StdCtrls, DBGridEhGrouping;

type
  TfrmAcompanhaCampanhaPromoc = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryMaster: TADOQuery;
    dsMaster: TDataSource;
    PopupMenu1: TPopupMenu;
    AtivarCampanha1: TMenuItem;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    qryMasternCdCampanhaPromoc: TIntegerField;
    qryMastercChaveCampanha: TStringField;
    qryMastercNmCampanhaPromoc: TStringField;
    qryMastercNmTipoTabPreco: TStringField;
    qryMasterdDtValidadeIni: TDateTimeField;
    qryMasterdDtValidadeFim: TDateTimeField;
    qryMasternValVenda: TBCDField;
    qryMasternValCMV: TBCDField;
    qryMasternMargemLucro: TBCDField;
    qryMastercFlgSuspensa: TIntegerField;
    qryMastercSuspensa: TStringField;
    qryMastercVencida: TStringField;
    SuspenderCampanha1: TMenuItem;
    AnaliseFinanceira: TMenuItem;
    qryAux: TADOQuery;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure AtivarCampanha1Click(Sender: TObject);
    procedure AnaliseFinanceiraClick(Sender: TObject);
    procedure SuspenderCampanha1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAcompanhaCampanhaPromoc: TfrmAcompanhaCampanhaPromoc;

implementation

uses fMenu, fCampanhaPromoc_AnaliseVendas,
  fCampanhaPromoc_AnaliseVendasERP;

{$R *.dfm}

procedure TfrmAcompanhaCampanhaPromoc.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryMaster.Close ;
  qryMaster.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva ;
  qryMaster.Parameters.ParamByName('cFlgExibeSuspensa').Value := 0 ;
  qryMaster.Parameters.ParamByName('cFlgExibeVencida').Value  := 0 ;

  if (CheckBox1.Checked) then
      qryMaster.Parameters.ParamByName('cFlgExibeSuspensa').Value := 1 ;

  if (CheckBox2.Checked) then
      qryMaster.Parameters.ParamByName('cFlgExibeVencida').Value  := 1 ;

  qryMaster.Open ;
  
end;

procedure TfrmAcompanhaCampanhaPromoc.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient ;
  ToolButton1.Click;
  
end;

procedure TfrmAcompanhaCampanhaPromoc.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  AtivarCampanha1.Enabled    := False ;
  SuspenderCampanha1.Enabled := False ;
  AnaliseFinanceira.Enabled  := True ;

  if not qryMaster.eof then
  begin
      if (qryMastercFlgSuspensa.Value = 1) then
          AtivarCampanha1.Enabled := true
      else begin
          SuspenderCampanha1.Enabled := true ;
      end ;

  end ;

end;

procedure TfrmAcompanhaCampanhaPromoc.AtivarCampanha1Click(
  Sender: TObject);
begin
  inherited;

  if (qryMasterdDtValidadeFim.Value < Date) then
  begin
      MensagemErro('N�o � permitido ativar uma campanha vencida.') ;
      exit ;
  end ;

  if (MessageDlg('Confirma a ativa��o desta campanha ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  qryAux.Close ;
  qryAux.SQL.clear ;
  qryAux.SQL.Add('UPDATE CampanhaPromoc SET cFlgAtivada = 1, cFlgSuspensa = 0 , dDtAtivacao = GetDate(), nCdUsuarioAtivacao = ' + intToStr(frmMenu.nCdUsuarioLogado) + ' WHERE nCdCampanhaPromoc = ' + qryMasternCdCampanhaPromoc.AsString) ;

  try
      qryAux.ExecSQL;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  ShowMessage('Campanha Ativada com Sucesso.') ;

  ToolButton1.Click;

end;

procedure TfrmAcompanhaCampanhaPromoc.AnaliseFinanceiraClick(
  Sender: TObject);
var
  objCons    : TfrmCampanhaPromoc_AnaliseVendas ;
  objConsERP : TfrmCampanhaPromoc_AnaliseVendasERP ;

begin
  inherited;

  objCons    := TfrmCampanhaPromoc_AnaliseVendas.Create(nil);
  objConsERP := TfrmCampanhaPromoc_AnaliseVendasERP.Create(nil);

  if qryMaster.Active and (not qryMaster.eof) then
  begin
      try
          try
              if (frmMenu.LeParametro('VAREJO') = 'S') then
              begin
                  objCons.qryVendas.Close;
                  objCons.qryVendas.Parameters.ParamByName('nPk').Value := qryMasternCdCampanhaPromoc.Value ;
                  objCons.qryVendas.Open;

                  objCons.cxGridDBTableView1.ViewData.Expand(True) ;
                  objCons.ShowModal ;
              end
              else
              begin
                  objConsERP.qryVendas.Close;
                  objConsERP.qryVendas.Parameters.ParamByName('nPk').Value := qryMasternCdCampanhaPromoc.Value ;
                  objConsERP.qryVendas.Open;

                  objConsERP.cxGridDBTableView1.ViewData.Expand(True) ;
                  objConsERP.ShowModal ;
              end ;

          except
              MensagemErro('Erro na cria��o do consulta');
              raise;
          end;
      finally
          FreeAndNil(objCons);
          FreeAndNil(objConsERP);
      end;

  end ;

end;

procedure TfrmAcompanhaCampanhaPromoc.SuspenderCampanha1Click(
  Sender: TObject);
begin
  inherited;
  if (MessageDlg('Confirma a suspens�o desta campanha ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  if (qryMasterdDtValidadeIni.Value <= Date) and (qryMasterdDtValidadeFim.Value >= Date) then
  begin
      if (MessageDlg('Esta campanha est� vig�nte e os descontos nas vendas ser�o interrompidos imediatamente.' +#13#13+ 'Tem certeza que deseja suspender esta campanha ?' ,mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;
  end ;

  qryAux.Close ;
  qryAux.SQL.clear ;
  qryAux.SQL.Add('UPDATE CampanhaPromoc SET cFlgSuspensa = 1 WHERE nCdCampanhaPromoc = ' + qryMasternCdCampanhaPromoc.AsString) ;

  try
      qryAux.ExecSQL;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  ShowMessage('Campanha Suspensa com Sucesso.') ;

  ToolButton1.Click;

end;

initialization
    RegisterClass(TfrmAcompanhaCampanhaPromoc) ;

end.
