inherited frmMetaVenda: TfrmMetaVenda
  Caption = 'Meta de Venda'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 64
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label4: TLabel [2]
    Left = 80
    Top = 70
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ano'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [3]
    Left = 80
    Top = 94
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'M'#234's'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [4]
    Left = 5
    Top = 166
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Meta Mensal'
    FocusControl = DBEdit6
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 104
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdMetaVenda'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit4: TDBEdit [7]
    Left = 104
    Top = 64
    Width = 65
    Height = 19
    DataField = 'iAno'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit5: TDBEdit [8]
    Left = 104
    Top = 88
    Width = 33
    Height = 19
    DataField = 'iMes'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBCheckBox1: TDBCheckBox [9]
    Left = 104
    Top = 112
    Width = 169
    Height = 17
    Caption = 'Considerar S'#225'bado dia '#250'til'
    DataField = 'cFlgSabado'
    DataSource = dsMaster
    TabOrder = 4
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox2: TDBCheckBox [10]
    Left = 104
    Top = 136
    Width = 169
    Height = 17
    Caption = 'Considerar Domingo dia '#250'til'
    DataField = 'cFlgDomingo'
    DataSource = dsMaster
    TabOrder = 5
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBEdit6: TDBEdit [11]
    Left = 104
    Top = 160
    Width = 97
    Height = 19
    DataField = 'nValMetaMes'
    DataSource = dsMaster
    TabOrder = 6
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM MetaVenda'
      'WHERE nCdMetaVenda = :nPK'
      'AND nCdEmpresa = :nCdEmpresa')
    object qryMasternCdMetaVenda: TIntegerField
      FieldName = 'nCdMetaVenda'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasteriAno: TIntegerField
      FieldName = 'iAno'
    end
    object qryMasteriMes: TIntegerField
      FieldName = 'iMes'
    end
    object qryMastercFlgSabado: TIntegerField
      FieldName = 'cFlgSabado'
    end
    object qryMastercFlgDomingo: TIntegerField
      FieldName = 'cFlgDomingo'
    end
    object qryMasternValMetaMes: TBCDField
      FieldName = 'nValMetaMes'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
end
