unit fAlteraChequeTerceiro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls, ER2Lookup, ACBrBase, ACBrValidador;

type
  TfrmAlteraChequeTerceiro = class(TfrmCadastro_Padrao)
    qryMasternCdCheque: TIntegerField;
    qryMasternCdBanco: TIntegerField;
    qryMastercAgencia: TStringField;
    qryMastercConta: TStringField;
    qryMastercDigito: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMasteriNrCheque: TIntegerField;
    qryMasternValCheque: TBCDField;
    qryMasterdDtDeposito: TDateTimeField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    edtBanco: TER2LookupDBEdit;
    qryBanco: TADOQuery;
    qryBanconCdBanco: TIntegerField;
    qryBancocNmBanco: TStringField;
    dsBanco: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforeClose(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAlteraChequeTerceiro: TfrmAlteraChequeTerceiro;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAlteraChequeTerceiro.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster   := 'CHEQUE' ;
  nCdTabelaSistema  := 32 ;
  nCdConsultaPadrao := 67 ;
end;

procedure TfrmAlteraChequeTerceiro.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  edtBanco.Text := qryMasternCdBanco.AsString;
  edtBanco.PosicionaQuery;
end;

procedure TfrmAlteraChequeTerceiro.qryMasterBeforeClose(DataSet: TDataSet);
begin
  inherited;

  qryBanco.Close;
end;

procedure TfrmAlteraChequeTerceiro.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (Trim(edtBanco.Text) = '') then
  begin
      MensagemAlerta('Insira o c�digo do Banco.');
      edtBanco.SetFocus;
      abort;
  end;

  if (Trim(DBEdit3.Text) = '') then
  begin
      MensagemAlerta('Digite a Ag�ncia.');
      DBEdit3.SetFocus;
      abort;
  end;

  if (Trim(DBEdit4.Text) = '') then
  begin
      MensagemAlerta('Digite a Conta Banc�ria.');
      DBEdit4.SetFocus;
      abort;
  end;

  if (Trim(DBEdit5.Text) = '') then
  begin
      MensagemAlerta('Insira o digito da Conta Banc�ria.');
      DBEdit5.SetFocus;
      abort;
  end;

  if (Trim(DBEdit7.Text) = '') then
  begin
      MensagemAlerta('Digite o n�mero do Cheque.');
      DBEdit7.SetFocus;
      abort;
  end;

  if (frmMenu.TestaCpfCgc(Trim(DBEdit6.Text)) = '') then
  begin
      DBEdit6.SetFocus;
      abort;
  end;

  if (Trim(DBEdit9.Text) = '/  /') then
  begin
      MensagemAlerta('Data de dep�sito n�o preenchido.');
      DBEdit9.SetFocus;
      abort;
  end;

  if (qryMasternValCheque.Value <= 0) then
  begin
      MensagemAlerta('O valor do cheque n�o pode ser menor ou igual a zero.');
      DBEdit8.SetFocus;
      abort;
  end;
end;

initialization
    RegisterClass(TfrmAlteraChequeTerceiro);

end.
