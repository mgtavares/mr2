inherited frmBaixaProvisao: TfrmBaixaProvisao
  Left = 89
  Top = 68
  Width = 1168
  Height = 614
  Caption = 'Baixa de Provis'#227'o'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 81
    Width = 1152
    Height = 495
  end
  inherited ToolBar1: TToolBar
    Width = 1152
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 81
    Width = 1152
    Height = 495
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsContaBancaria
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdBanco'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 43
      end
      item
        EditButtons = <>
        FieldName = 'cNmBanco'
        Footers = <>
        Width = 162
      end
      item
        EditButtons = <>
        FieldName = 'cAgencia'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 49
      end
      item
        EditButtons = <>
        FieldName = 'nCdConta'
        Footers = <>
        Width = 139
      end
      item
        EditButtons = <>
        FieldName = 'cNmTitular'
        Footers = <>
        Width = 266
      end
      item
        EditButtons = <>
        FieldName = 'nValTotal'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'Consolas'
        Footer.Font.Style = []
        Footers = <>
        Width = 121
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1152
    Height = 52
    Align = alTop
    Caption = ' Filtro '
    TabOrder = 2
    object Label1: TLabel
      Tag = 1
      Left = 9
      Top = 24
      Width = 95
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data do Pagamento'
    end
    object cxButton1: TcxButton
      Left = 216
      Top = 15
      Width = 121
      Height = 23
      Caption = 'Exibir Provis'#245'es'
      TabOrder = 0
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D360300000000000036000000280000000F000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
        393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
        00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
        F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
        83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
        68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
        FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
        9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
        8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
        D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
        B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
        5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
        A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
        9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
        488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
        FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
        FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
        FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
    object edtDtPagto: TDateTimePicker
      Left = 112
      Top = 16
      Width = 97
      Height = 21
      Date = 39868.000000000000000000
      Time = 39868.000000000000000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
  end
  inherited ImageList1: TImageList
    Left = 272
    Top = 152
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtPagto'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa int'
      '       ,@dDtPagto   VARCHAR(10)'
      '       ,@nCdUsuario int'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      'Set @dDtPagto   = :dDtPagto'
      'Set @nCdUsuario = :nCdUsuario'
      ''
      'SELECT ContaBancaria.nCdBanco'
      '      ,Banco.cNmBanco'
      '      ,ContaBancaria.cAgencia'
      '      ,ContaBancaria.nCdConta'
      '      ,ContaBancaria.cNmTitular'
      '      ,ContaBancaria.nCdContaBancaria'
      '      ,Sum(nValProvisao) nValTotal'
      '  FROM ProvisaoTit'
      
        '       INNER JOIN ContaBancaria ON ContaBancaria.nCdContaBancari' +
        'a = ProvisaoTit.nCdContaBancaria'
      
        '       LEFT  JOIN Banco         ON Banco.nCdBanco               ' +
        '  = ContaBancaria.nCdBanco'
      ' WHERE dDtCancel   IS NULL'
      '   AND cFlgBaixado  = 0'
      '   AND dDtPagto     = Convert(DATETIME,@dDtPagto,103)'
      '   AND ProvisaoTit.nCdEmpresa = @nCdEmpresa'
      '   AND (EXISTS(SELECT 1'
      '                FROM UsuarioContaBancaria UCB'
      
        '               Where UCB.nCdContaBancaria = ContaBancaria.nCdCon' +
        'taBancaria'
      
        '                 AND UCB.nCdUsuario = @nCdUsuario) OR (cFlgCofre' +
        ' = 1 AND (   nCdUsuarioOperador IS NULL'
      
        '                                                                ' +
        '          OR nCdUsuarioOperador = @nCdUsuario)))'
      ' GROUP BY ContaBancaria.nCdBanco'
      '         ,Banco.cNmBanco'
      '         ,ContaBancaria.cAgencia'
      '         ,ContaBancaria.nCdConta'
      '         ,ContaBancaria.cNmTitular'
      '         ,ContaBancaria.nCdContaBancaria'
      '')
    Left = 240
    Top = 152
    object qryContaBancarianCdBanco: TIntegerField
      DisplayLabel = 'Provi'#245'es|Banco'
      FieldName = 'nCdBanco'
    end
    object qryContaBancariacNmBanco: TStringField
      DisplayLabel = 'Provi'#245'es|Nome Banco'
      FieldName = 'cNmBanco'
      Size = 50
    end
    object qryContaBancariacAgencia: TIntegerField
      DisplayLabel = 'Provi'#245'es|Ag'#234'ncia'
      FieldName = 'cAgencia'
    end
    object qryContaBancarianCdConta: TStringField
      DisplayLabel = 'Provi'#245'es|N'#250'm. Conta'
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryContaBancariacNmTitular: TStringField
      DisplayLabel = 'Provi'#245'es|Titular'
      FieldName = 'cNmTitular'
      Size = 50
    end
    object qryContaBancarianValTotal: TBCDField
      DisplayLabel = 'Provi'#245'es|Valor Provisionado'
      FieldName = 'nValTotal'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
  end
  object dsContaBancaria: TDataSource
    DataSet = qryContaBancaria
    Left = 240
    Top = 184
  end
end
