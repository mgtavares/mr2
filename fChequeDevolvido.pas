unit fChequeDevolvido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, Mask, ImgList, ComCtrls, ToolWin,
  ExtCtrls, DB, ADODB;

type
  TfrmChequeDevolvido = class(TfrmProcesso_Padrao)
    CheckBox1: TCheckBox;
    MaskEdit1: TMaskEdit;
    Label1: TLabel;
    MaskEdit2: TMaskEdit;
    Label2: TLabel;
    MaskEdit3: TMaskEdit;
    Label3: TLabel;
    usp_Processo: TADOStoredProc;
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdCheque : Integer ;
  end;

var
  frmChequeDevolvido: TfrmChequeDevolvido;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmChequeDevolvido.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  If (trim(MaskEdit3.Text) <> '') then
      MaskEdit3.text := formatcurr('##,##0.00',StrToFloat(StringReplace(MaskEdit3.text,'.',',',[rfReplaceAll,rfIgnoreCase]))) ;

end;

procedure TfrmChequeDevolvido.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
    13 : Begin
        MaskEdit2.SetFocus ;
    end ;

  end ;

end;

procedure TfrmChequeDevolvido.MaskEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
    13 : Begin
        MaskEdit3.SetFocus ;
    end ;

  end ;

end;

procedure TfrmChequeDevolvido.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
    13 : Begin
        MaskEdit1.SetFocus ;
    end ;

  end ;

end;

procedure TfrmChequeDevolvido.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(MaskEdit3.Text) = '') then
  begin
      ShowMessage('Informe o valor da tarifa adicional.') ;
      MaskEdit3.SetFocus;
      exit ;
  end ;

  if (Trim(MaskEdit1.Text) = '/  /') then
  begin
      ShowMessage('Informe a data da devolu��o.') ;
      MaskEdit1.SetFocus ;
      exit ;
  end ;

  if (Trim(MaskEdit2.Text) = '/  /') then
  begin
      ShowMessage('Informe a data do vencimento.') ;
      MaskEdit2.SetFocus ;
      exit ;
  end ;

  if StrToDate(frmMenu.LeParametro('DTCONTAB')) >= (StrToDate(MaskEdit2.Text)) then
  begin
      ShowMessage('Data de vencimento deve ser maior que a �ltima data cont�bil.') ;
      exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try

      usp_Processo.Close ;
      usp_Processo.Parameters.ParamByName('@nCdCheque').Value  := nCdCheque ;

      if (CheckBox1.Checked) then
          usp_Processo.Parameters.ParamByName('@cFlgVoltaTerceiro').Value := 'S'
      else usp_Processo.Parameters.ParamByName('@cFlgVoltaTerceiro').Value := 'N' ;

      usp_Processo.Parameters.ParamByName('@dDtDevol').Value   := frmMenu.ConvData(MaskEdit1.Text) ;
      usp_Processo.Parameters.ParamByName('@dDtVencto').Value  := frmMenu.ConvData(MaskEdit1.Text) ;
      usp_Processo.Parameters.ParamByName('@nValTarifa').Value := StrToFloat(MaskEdit3.Text) ;
      usp_Processo.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      usp_Processo.ExecProc;

  except
      frmMenu.Connection.RollbackTrans;
      ShowMessage('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;
  ShowMessage('Processamento Conclu�do. T�tulos financeiros gerados com sucesso.') ;

  close ;


end;

procedure TfrmChequeDevolvido.FormShow(Sender: TObject);
begin
  inherited;
  MaskEdit1.SetFocus ;
end;

end.
