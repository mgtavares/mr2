unit fClienteVarejoPessoaFisica_HistAltEnd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid;

type
  TfrmClienteVarejoPessoaFisica_HistAltEnd = class(TfrmProcesso_Padrao)
    qryEndereco: TADOQuery;
    qryEnderecocEndereco: TStringField;
    qryEnderecocComplemento: TStringField;
    qryEnderecocBairro: TStringField;
    qryEnderecocCidade: TStringField;
    qryEnderecocTelefone: TStringField;
    qryEnderecocFax: TStringField;
    qryEnderecocCEP: TStringField;
    qryEnderecocNmTabTipoResidencia: TStringField;
    qryEnderecocTempoResid: TStringField;
    qryEndereconValAluguel: TBCDField;
    qryEnderecocNmTabTipoComprov: TStringField;
    qryEnderecocOBSComprov: TStringField;
    qryEnderecocNmUsuario: TStringField;
    qryEnderecodDtCadastro: TDateTimeField;
    qryEnderecocNmLoja: TStringField;
    dsEndereco: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1cEndereco: TcxGridDBColumn;
    cxGrid1DBTableView1cComplemento: TcxGridDBColumn;
    cxGrid1DBTableView1cBairro: TcxGridDBColumn;
    cxGrid1DBTableView1cCidade: TcxGridDBColumn;
    cxGrid1DBTableView1cTelefone: TcxGridDBColumn;
    cxGrid1DBTableView1cFax: TcxGridDBColumn;
    cxGrid1DBTableView1cCEP: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTabTipoResidencia: TcxGridDBColumn;
    cxGrid1DBTableView1cTempoResid: TcxGridDBColumn;
    cxGrid1DBTableView1nValAluguel: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTabTipoComprov: TcxGridDBColumn;
    cxGrid1DBTableView1cOBSComprov: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    cxGrid1DBTableView1dDtCadastro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLoja: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmClienteVarejoPessoaFisica_HistAltEnd: TfrmClienteVarejoPessoaFisica_HistAltEnd;

implementation

{$R *.dfm}

end.
