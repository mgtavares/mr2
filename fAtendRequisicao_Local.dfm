inherited frmAtendRequisicao_Local: TfrmAtendRequisicao_Local
  Left = 340
  Top = 279
  Width = 436
  Height = 133
  BorderIcons = []
  Caption = 'Atendimento de Requisi'#231#227'o'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 420
    Height = 68
  end
  object Label1: TLabel [1]
    Left = 37
    Top = 48
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Estoque D'#233'bito'
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 72
    Width = 102
    Height = 13
    Alignment = taRightJustify
    Caption = 'Quantidade Atendida'
  end
  inherited ToolBar1: TToolBar
    Width = 420
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit1: TMaskEdit [4]
    Left = 112
    Top = 64
    Width = 121
    Height = 21
    TabOrder = 1
    OnExit = MaskEdit1Exit
  end
  object dbComboBoxEh1: TDBLookupComboboxEh [5]
    Left = 112
    Top = 40
    Width = 297
    Height = 21
    EditButtons = <>
    KeyField = 'nCdLocalEstoque'
    ListField = 'cNmLocalEstoque'
    ListSource = dsLocalEstoque
    TabOrder = 2
    Visible = True
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      '       ,@nCdGrupoProduto int'
      '       ,@nCdUsuario int'
      ''
      'Set @nCdLoja    = IsNull(:nCdLoja,0)'
      'Set @nCdUsuario = IsNull(:nCdUsuario,0)'
      ''
      'SELECT @nCdGrupoProduto = nCdGrupoProduto'
      '  FROM Produto'
      
        '       INNER JOIN Departamento ON Departamento.nCdDepartamento =' +
        ' Produto.nCdDepartamento'
      ' WHERE nCdProduto = :nCdProduto'
      ''
      'SELECT nCdLocalEstoque'
      '      ,cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdEmpresa = :nCdEmpresa'
      '   AND ((@nCdLoja = 0) OR (nCdLoja = @nCdLoja))'
      '   AND EXISTS(SELECT 1'
      '                FROM GrupoProdutoLocalEstoque GPLE'
      
        '               WHERE GPLE.nCdLocalEstoque = LocalEstoque.nCdLoca' +
        'lEstoque'
      '                 AND GPLE.nCdGrupoProduto = @nCdGrupoProduto)'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLocalEstoque'
      
        '               WHERE nCdLocalEstoque = LocalEstoque.nCdLocalEsto' +
        'que'
      '                 AND nCdUsuario      = @nCdUsuario)')
    Left = 304
    Top = 64
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object dsLocalEstoque: TDataSource
    DataSet = qryLocalEstoque
    Left = 272
    Top = 64
  end
end
