unit fCanalDistribuicao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmCanalDistribuicao = class(TfrmCadastro_Padrao)
    qryMasternCdCanalDistribuicao: TIntegerField;
    qryMastercNmCanalDistribuicao: TStringField;
    qryMasternCdStatus: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    dsStatus: TDataSource;
    DBEdit4: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit3Enter(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCanalDistribuicao: TfrmCanalDistribuicao;

implementation

{$R *.dfm}

procedure TfrmCanalDistribuicao.FormCreate(Sender: TObject);
begin
  inherited;

  nCdTabelaSistema  := 431;
  cNmTabelaMaster   := 'CANALDISTRIBUICAO';
  nCdConsultaPadrao := 755;
end;

procedure TfrmCanalDistribuicao.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
  
end;

procedure TfrmCanalDistribuicao.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(qryMastercNmCanalDistribuicao.Value) = '') then
  begin
      MensagemAlerta('Digite a Descrição do Canal de Distribuição.');
      DBEdit2.SetFocus;
      abort;
  end;

  inherited;

end;

procedure TfrmCanalDistribuicao.DBEdit3Enter(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      exit;

  if (qryMaster.State <> dsInsert) then
      exit;

  qryMasternCdStatus.Value := 1;

  qryStatus.Close;
  PosicionaQuery(qryStatus,qryMasternCdStatus.AsString);
  
end;

procedure TfrmCanalDistribuicao.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryStatus.Close;
  PosicionaQuery(qryStatus,qryMasternCdStatus.AsString);
end;

procedure TfrmCanalDistribuicao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryStatus.Close;
  PosicionaQuery(qryStatus,qryMasternCdStatus.AsString);
end;

procedure TfrmCanalDistribuicao.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryStatus.Close;
end;

initialization
    RegisterClass(TfrmCanalDistribuicao);

end.
