inherited frmConsultaVendaProduto_Dados: TfrmConsultaVendaProduto_Dados
  Left = 169
  Top = 79
  Width = 1010
  Height = 613
  Caption = 'Dados Adicionais da Venda'
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 994
    Height = 154
  end
  inherited ToolBar1: TToolBar
    Width = 994
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 994
    Height = 154
    Align = alClient
    Caption = 'Todos os Produtos do Movimento'
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 2
      Top = 15
      Width = 990
      Height = 137
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfUltraFlat
      LookAndFeel.NativeStyle = True
      object cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dsItemPedido
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsCustomize.ColumnFiltering = False
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GridLineColor = clSilver
        OptionsView.GridLines = glVertical
        OptionsView.GroupByBox = False
        Styles.Content = frmMenu.ConteudoCaixa
        object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
          Caption = 'Pedido'
          DataBinding.FieldName = 'nCdPedido'
          Width = 49
        end
        object cxGrid1DBTableView1nCdItemPedido: TcxGridDBColumn
          Caption = 'Item'
          DataBinding.FieldName = 'nCdItemPedido'
          Width = 43
        end
        object cxGrid1DBTableView1nCdProduto: TcxGridDBColumn
          DataBinding.FieldName = 'nCdProduto'
          Visible = False
        end
        object cxGrid1DBTableView1cNmItem: TcxGridDBColumn
          Caption = 'Produto'
          DataBinding.FieldName = 'cNmItem'
          Width = 257
        end
        object cxGrid1DBTableView1nQtdeExpRec: TcxGridDBColumn
          Caption = 'Qt.'
          DataBinding.FieldName = 'nQtdeExpRec'
          Width = 35
        end
        object cxGrid1DBTableView1nValUnitario: TcxGridDBColumn
          Caption = 'Val. Unit'#225'rio'
          DataBinding.FieldName = 'nValUnitario'
          Width = 94
        end
        object cxGrid1DBTableView1nValDesconto: TcxGridDBColumn
          Caption = 'Descto'
          DataBinding.FieldName = 'nValDesconto'
          Width = 72
        end
        object cxGrid1DBTableView1DBColumn1: TcxGridDBColumn
          Caption = '% Descto'
          DataBinding.FieldName = 'nPercDescontoItem'
          Width = 61
        end
        object cxGrid1DBTableView1nValAcrescimo: TcxGridDBColumn
          Caption = 'Acresc.'
          DataBinding.FieldName = 'nValAcrescimo'
          Width = 69
        end
        object cxGrid1DBTableView1nValTotal: TcxGridDBColumn
          Caption = 'Val. Total'
          DataBinding.FieldName = 'nValTotal'
          Width = 74
        end
        object cxGrid1DBTableView1cFlgTrocado: TcxGridDBColumn
          Caption = 'Trocado'
          DataBinding.FieldName = 'cFlgTrocado'
          Width = 52
        end
        object cxGrid1DBTableView1nCdUsuario: TcxGridDBColumn
          Caption = 'C'#243'd. Vendedor'
          DataBinding.FieldName = 'nCdUsuario'
          Width = 75
        end
        object cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn
          Caption = 'Vendedor'
          DataBinding.FieldName = 'cNmUsuario'
          Width = 106
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object GroupBox2: TGroupBox [3]
    Left = 0
    Top = 183
    Width = 994
    Height = 123
    Align = alBottom
    Caption = 'Forma e Condi'#231#227'o da Liquida'#231#227'o do Movimento'
    TabOrder = 2
    object cxGrid2: TcxGrid
      Left = 2
      Top = 15
      Width = 990
      Height = 106
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfUltraFlat
      LookAndFeel.NativeStyle = True
      object cxGridDBTableView1: TcxGridDBTableView
        DataController.DataSource = dsCondicao
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsCustomize.ColumnFiltering = False
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        Styles.Content = frmMenu.ConteudoCaixa
        object cxGridDBTableView1nCdFormaPagto: TcxGridDBColumn
          DataBinding.FieldName = 'nCdFormaPagto'
          Visible = False
        end
        object cxGridDBTableView1cNmFormaPagto: TcxGridDBColumn
          Caption = 'Forma de Pagamento'
          DataBinding.FieldName = 'cNmFormaPagto'
          Width = 151
        end
        object cxGridDBTableView1nCdCondPagto: TcxGridDBColumn
          DataBinding.FieldName = 'nCdCondPagto'
          Visible = False
        end
        object cxGridDBTableView1cNmCondPagto: TcxGridDBColumn
          Caption = 'Condi'#231#227'o'
          DataBinding.FieldName = 'cNmCondPagto'
          Width = 200
        end
        object cxGridDBTableView1nValPagto: TcxGridDBColumn
          Caption = 'Valor Pago'
          DataBinding.FieldName = 'nValPagto'
          Width = 113
        end
        object cxGridDBTableView1nValDescontoCondPagto: TcxGridDBColumn
          Caption = 'Valor Desconto'
          DataBinding.FieldName = 'nValDescontoCondPagto'
          Width = 112
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
  end
  object GroupBox3: TGroupBox [4]
    Left = 0
    Top = 306
    Width = 994
    Height = 133
    Align = alBottom
    Caption = 'Presta'#231#245'es do Movimento'
    TabOrder = 3
    object cxGrid3: TcxGrid
      Left = 2
      Top = 15
      Width = 990
      Height = 116
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfUltraFlat
      LookAndFeel.NativeStyle = True
      object cxGridDBTableView2: TcxGridDBTableView
        OnDblClick = cxGridDBTableView2DblClick
        DataController.DataSource = dsPrestacoes
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsCustomize.ColumnFiltering = False
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        Styles.Content = frmMenu.ConteudoCaixa
        object cxGridDBTableView2nCdTitulo: TcxGridDBColumn
          DataBinding.FieldName = 'nCdTitulo'
          Visible = False
        end
        object cxGridDBTableView2cNmTerceiro: TcxGridDBColumn
          Caption = 'Cliente'
          DataBinding.FieldName = 'cNmTerceiro'
          Width = 138
        end
        object cxGridDBTableView2nCdCrediario: TcxGridDBColumn
          Caption = 'Carnet'
          DataBinding.FieldName = 'nCdCrediario'
        end
        object cxGridDBTableView2iParcela: TcxGridDBColumn
          Caption = 'Parcela'
          DataBinding.FieldName = 'iParcela'
          Width = 52
        end
        object cxGridDBTableView2dDtVenc: TcxGridDBColumn
          Caption = 'Dt. Vencimento'
          DataBinding.FieldName = 'dDtVenc'
          Width = 98
        end
        object cxGridDBTableView2dDtLiq: TcxGridDBColumn
          Caption = 'Dt. Pagamento'
          DataBinding.FieldName = 'dDtLiq'
          Width = 91
        end
        object cxGridDBTableView2nValTit: TcxGridDBColumn
          Caption = 'Valor Parcela'
          DataBinding.FieldName = 'nValTit'
          Width = 91
        end
        object cxGridDBTableView2nValJuro: TcxGridDBColumn
          Caption = 'Juros'
          DataBinding.FieldName = 'nValJuro'
          Width = 46
        end
        object cxGridDBTableView2nValLiq: TcxGridDBColumn
          Caption = 'Valor Pago'
          DataBinding.FieldName = 'nValLiq'
          Width = 68
        end
        object cxGridDBTableView2nSaldoTit: TcxGridDBColumn
          Caption = 'Saldo em Aberto'
          DataBinding.FieldName = 'nSaldoTit'
          Width = 98
        end
      end
      object cxGridLevel2: TcxGridLevel
        GridView = cxGridDBTableView2
      end
    end
  end
  object GroupBox4: TGroupBox [5]
    Left = 0
    Top = 439
    Width = 994
    Height = 136
    Align = alBottom
    Caption = 'Cheques do Movimento'
    TabOrder = 4
    object cxGrid4: TcxGrid
      Left = 2
      Top = 15
      Width = 990
      Height = 119
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfUltraFlat
      LookAndFeel.NativeStyle = True
      object cxGridDBTableView3: TcxGridDBTableView
        DataController.DataSource = dsCheques
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsCustomize.ColumnFiltering = False
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        Styles.Content = frmMenu.ConteudoCaixa
        object cxGridDBTableView3nCdCheque: TcxGridDBColumn
          Caption = 'C'#243'digo'
          DataBinding.FieldName = 'nCdCheque'
          Width = 48
        end
        object cxGridDBTableView3nCdBanco: TcxGridDBColumn
          Caption = 'Banco'
          DataBinding.FieldName = 'nCdBanco'
          Width = 39
        end
        object cxGridDBTableView3cAgencia: TcxGridDBColumn
          Caption = 'Ag'#234'ncia'
          DataBinding.FieldName = 'cAgencia'
          Width = 50
        end
        object cxGridDBTableView3cConta: TcxGridDBColumn
          Caption = 'Conta'
          DataBinding.FieldName = 'cConta'
          Width = 48
        end
        object cxGridDBTableView3iNrCheque: TcxGridDBColumn
          Caption = 'N'#250'm. Cheque'
          DataBinding.FieldName = 'iNrCheque'
          Width = 78
        end
        object cxGridDBTableView3nValCheque: TcxGridDBColumn
          Caption = 'Valor Original'
          DataBinding.FieldName = 'nValCheque'
          Width = 111
        end
        object cxGridDBTableView3dDtDeposito: TcxGridDBColumn
          Caption = 'Dt. Vencimento'
          DataBinding.FieldName = 'dDtDeposito'
        end
        object cxGridDBTableView3dDtDevol: TcxGridDBColumn
          Caption = 'Dt. 1 Dev.'
          DataBinding.FieldName = 'dDtDevol'
          Width = 72
        end
        object cxGridDBTableView3dDtSegDevol: TcxGridDBColumn
          Caption = 'Dt. 2 Dev.'
          DataBinding.FieldName = 'dDtSegDevol'
          Width = 72
        end
        object cxGridDBTableView3cNmTabStatusCheque: TcxGridDBColumn
          Caption = 'Situa'#231#227'o'
          DataBinding.FieldName = 'cNmTabStatusCheque'
          Width = 115
        end
      end
      object cxGridLevel3: TcxGridLevel
        GridView = cxGridDBTableView3
      end
    end
  end
  object qryPrestacoes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTitulo'
      '      ,cNmTerceiro'
      '      ,nCdCrediario'
      '      ,iParcela'
      '      ,dDtVenc'
      '      ,dDtLiq'
      '      ,nValTit'
      '      ,nValJuro     '
      '      ,nValLiq'
      '      ,nSaldoTit'
      '  FROM Titulo '
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdT' +
        'erceiro'
      ' WHERE nCdLanctoFin  = :nPK'
      '   AND dDtCancel    IS NULL'
      '   AND nCdCrediario IS NOT NULL'
      ' ORDER BY 2,3')
    Left = 120
    Top = 372
    object qryPrestacoesnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryPrestacoescNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryPrestacoesnCdCrediario: TIntegerField
      FieldName = 'nCdCrediario'
    end
    object qryPrestacoesiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryPrestacoesdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryPrestacoesdDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
    object qryPrestacoesnValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryPrestacoesnValJuro: TBCDField
      FieldName = 'nValJuro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryPrestacoesnValLiq: TBCDField
      FieldName = 'nValLiq'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryPrestacoesnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsPrestacoes: TDataSource
    DataSet = qryPrestacoes
    Left = 160
    Top = 372
  end
  object qryCondicao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT FormaPagto.nCdFormaPagto'
      '      ,FormaPagto.cNmFormaPagto'
      '      ,CondPagto.nCdCondPagto'
      '      ,CondPagto.cNmCondPagto'
      '      ,nValPagto'
      '      ,nValDescontoCondPagto'
      '  FROM FormaPagtoLanctoFin FPLF'
      
        '       INNER JOIN FormaPagto ON FormaPagto.nCdFormaPagto = FPLF.' +
        'nCdFormaPagto'
      
        '       INNER JOIN CondPagto  ON CondPagto.nCdCondPagto   = FPLF.' +
        'nCdCondPagto'
      ' WHERE FPLF.nCdLanctoFin = :nPK')
    Left = 120
    Top = 220
    object qryCondicaonCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryCondicaocNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryCondicaonCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryCondicaocNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryCondicaonValPagto: TBCDField
      FieldName = 'nValPagto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCondicaonValDescontoCondPagto: TBCDField
      FieldName = 'nValDescontoCondPagto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsCondicao: TDataSource
    DataSet = qryCondicao
    Left = 160
    Top = 220
  end
  object qryItemPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT ItemPedido.nCdPedido'
      '      ,ItemPedido.nCdItemPedido '
      '      ,ItemPedido.nCdProduto'
      '      ,ItemPedido.cNmItem'
      '      ,ItemPedido.nQtdeExpRec'
      '      ,ItemPedido.nValUnitario'
      '      ,ItemPedido.nValDesconto'
      '      ,CASE WHEN ItemPedido.nValUnitario > 0 '
      
        '            THEN dbo.fn_ArredondaVarejo((ItemPedido.nValDesconto' +
        ' * 100) / ItemPedido.nValUnitario) '
      '            ELSE 0'
      '       END nPercDescontoItem   '
      '      ,ItemPedido.nValAcrescimo'
      
        '      ,(ItemPedido.nQtdeExpRec * (ItemPedido.nValUnitario - Item' +
        'Pedido.nValDesconto + ItemPedido.nValAcrescimo)) nValTotal'
      '      ,CASE WHEN ItemPedido.cFlgTrocado = 0 THEN '#39'N'#227'o'#39
      '            ELSE '#39'Sim'#39
      '       END cFlgTrocado'
      '      ,Usuario.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '  FROM ItemPedido'
      
        '       INNER JOIN Pedido  ON Pedido.nCdPedido = ItemPedido.nCdPe' +
        'dido'
      
        '       INNER JOIN Usuario ON Usuario.ncdTerceiroResponsavel = Pe' +
        'dido.nCdTerceiroColab'
      ' WHERE Pedido.nCdLanctoFin = :nPK'
      '')
    Left = 120
    Top = 100
    object qryItemPedidonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryItemPedidonCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryItemPedidonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryItemPedidocNmItem: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemPedidonQtdeExpRec: TBCDField
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object qryItemPedidonValUnitario: TBCDField
      FieldName = 'nValUnitario'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryItemPedidonValDesconto: TBCDField
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryItemPedidonValAcrescimo: TBCDField
      FieldName = 'nValAcrescimo'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryItemPedidonValTotal: TBCDField
      FieldName = 'nValTotal'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 27
      Size = 8
    end
    object qryItemPedidocFlgTrocado: TStringField
      FieldName = 'cFlgTrocado'
      ReadOnly = True
      Size = 3
    end
    object qryItemPedidonCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryItemPedidocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryItemPedidonPercDescontoItem: TBCDField
      FieldName = 'nPercDescontoItem'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsItemPedido: TDataSource
    DataSet = qryItemPedido
    Left = 160
    Top = 100
  end
  object qryCheques: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Cheque.nCdCheque   '
      '      ,Cheque.nCdBanco    '
      '      ,Cheque.cAgencia '
      '      ,Cheque.cConta'
      '      ,Cheque.iNrCheque   '
      '      ,Cheque.nValCheque                              '
      '      ,Cheque.dDtDeposito             '
      '      ,Cheque.dDtDevol'
      '      ,Cheque.dDtSegDevol'
      '      ,cNmTabStatusCheque'
      '  FROM Cheque'
      
        '       LEFT JOIN Terceiro ON Terceiro.nCdTerceiro = Cheque.nCdTe' +
        'rceiroResp'
      
        '       INNER JOIN TabStatusCheque ON TabStatusCheque.nCdTabStatu' +
        'sCheque = Cheque.nCdTabStatusCheque      '
      ' WHERE nCdLanctoFin = :nPK'
      '    ORDER BY 1')
    Left = 112
    Top = 484
    object qryChequesnCdCheque: TAutoIncField
      FieldName = 'nCdCheque'
      ReadOnly = True
    end
    object qryChequesnCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryChequescAgencia: TStringField
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryChequescConta: TStringField
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryChequesiNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryChequesnValCheque: TBCDField
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryChequesdDtDeposito: TDateTimeField
      FieldName = 'dDtDeposito'
    end
    object qryChequesdDtDevol: TDateTimeField
      FieldName = 'dDtDevol'
    end
    object qryChequesdDtSegDevol: TDateTimeField
      FieldName = 'dDtSegDevol'
    end
    object qryChequescNmTabStatusCheque: TStringField
      FieldName = 'cNmTabStatusCheque'
      Size = 50
    end
  end
  object dsCheques: TDataSource
    DataSet = qryCheques
    Left = 152
    Top = 476
  end
end
