inherited frmSeparaEmbalaPedido: TfrmSeparaEmbalaPedido
  Left = 180
  Top = 0
  Width = 1152
  Height = 720
  Caption = 'Sepera'#231#227'o/Embalagem Pedido'
  OldCreateOrder = True
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1136
    Height = 653
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    ButtonWidth = 76
    inherited ToolButton1: TToolButton
      Caption = '&Atualizar'
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Left = 76
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Left = 84
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 653
    ActivePage = cxTabSheet2
    Align = alClient
    LookAndFeel.NativeStyle = True
    PopupMenu = popEmSeparacao
    TabOrder = 1
    ClientRectBottom = 649
    ClientRectLeft = 4
    ClientRectRight = 1132
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Aguardando Produ'#231#227'o'
      ImageIndex = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1128
        Height = 46
        Align = alTop
        TabOrder = 0
        object btExibeOP: TcxButton
          Left = 6
          Top = 10
          Width = 147
          Height = 31
          Caption = 'Exibir OP'
          TabOrder = 0
          OnClick = btExibeOPClick
          Glyph.Data = {
            06030000424D060300000000000036000000280000000F0000000F0000000100
            180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
            00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
            FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
            80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
            0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
            000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
            00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
            E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
            E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
            99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
            E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
            E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
            99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
            E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
            999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
            99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
            E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
            999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
            9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
            FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000}
          LookAndFeel.NativeStyle = True
        end
      end
      object cxPageControl5: TcxPageControl
        Left = 0
        Top = 46
        Width = 1128
        Height = 579
        ActivePage = cxTabSheet10
        Align = alClient
        LookAndFeel.NativeStyle = True
        TabOrder = 1
        ClientRectBottom = 575
        ClientRectLeft = 4
        ClientRectRight = 1124
        ClientRectTop = 24
        object cxTabSheet10: TcxTabSheet
          Caption = 'Em Produ'#231#227'o'
          ImageIndex = 0
          object cxGrid1: TcxGrid
            Left = 0
            Top = 0
            Width = 1120
            Height = 551
            Align = alClient
            PopupMenu = popAguardProducao
            TabOrder = 0
            object cxGrid1DBTableView1: TcxGridDBTableView
              PopupMenu = popAguardProducao
              OnDblClick = cxGrid1DBTableView1DblClick
              DataController.DataSource = dsAguardProd
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              Styles.Content = frmMenu.FonteSomenteLeitura
              object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
                DataBinding.FieldName = 'nCdPedido'
              end
              object cxGrid1DBTableView1nCdOrdemProducao: TcxGridDBColumn
                DataBinding.FieldName = 'nCdOrdemProducao'
                Visible = False
                SortIndex = 0
                SortOrder = soAscending
              end
              object cxGrid1DBTableView1cNumeroOP: TcxGridDBColumn
                DataBinding.FieldName = 'cNumeroOP'
              end
              object cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTipoPedido'
                Width = 124
              end
              object cxGrid1DBTableView1dDtEntrega: TcxGridDBColumn
                DataBinding.FieldName = 'dDtEntrega'
              end
              object cxGrid1DBTableView1cAtraso: TcxGridDBColumn
                DataBinding.FieldName = 'cAtraso'
                Width = 65
              end
              object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiro'
                Width = 226
              end
              object cxGrid1DBTableView1cNmTerceiroPagador: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiroPagador'
                Width = 236
              end
              object cxGrid1DBTableView1cAutFin: TcxGridDBColumn
                DataBinding.FieldName = 'cAutFin'
                Width = 79
              end
              object cxGrid1DBTableView1nCdProduto: TcxGridDBColumn
                DataBinding.FieldName = 'nCdProduto'
                Width = 89
              end
              object cxGrid1DBTableView1cNmProduto: TcxGridDBColumn
                DataBinding.FieldName = 'cNmProduto'
                Width = 363
              end
              object cxGrid1DBTableView1nQtdePed: TcxGridDBColumn
                DataBinding.FieldName = 'nQtdePed'
              end
              object cxGrid1DBTableView1nQtdeAberto: TcxGridDBColumn
                DataBinding.FieldName = 'nQtdeAberto'
              end
              object cxGrid1DBTableView1DBColumn1: TcxGridDBColumn
                DataBinding.FieldName = 'nPercCumprimento'
              end
            end
            object cxGrid1Level1: TcxGridLevel
              GridView = cxGrid1DBTableView1
            end
          end
        end
        object cxTabSheet12: TcxTabSheet
          Caption = 'Produzido Parcialmente'
          ImageIndex = 1
          object cxGrid8: TcxGrid
            Left = 0
            Top = 0
            Width = 1120
            Height = 551
            Align = alClient
            PopupMenu = popProduzidoParcial
            TabOrder = 0
            object cxGridDBTableView1: TcxGridDBTableView
              PopupMenu = popProduzidoParcial
              OnDblClick = cxGridDBTableView1DblClick
              DataController.DataSource = dsProdParcial
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              Styles.Content = frmMenu.FonteSomenteLeitura
              object cxGridDBTableView1nCdPedido: TcxGridDBColumn
                DataBinding.FieldName = 'nCdPedido'
              end
              object cxGridDBTableView1nCdOrdemProducao: TcxGridDBColumn
                DataBinding.FieldName = 'nCdOrdemProducao'
                Visible = False
              end
              object cxGridDBTableView1cNumeroOP: TcxGridDBColumn
                Caption = 'N'#250'mero OP'
                DataBinding.FieldName = 'cNumeroOP'
                Width = 124
              end
              object cxGridDBTableView1cNmTipoPedido: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTipoPedido'
                Width = 124
              end
              object cxGridDBTableView1dDtEntrega: TcxGridDBColumn
                DataBinding.FieldName = 'dDtEntrega'
              end
              object cxGridDBTableView1cAtraso: TcxGridDBColumn
                DataBinding.FieldName = 'cAtraso'
                Width = 66
              end
              object cxGridDBTableView1cNmTerceiro: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiro'
                Width = 224
              end
              object cxGridDBTableView1cNmTerceiroPagador: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiroPagador'
                Width = 220
              end
              object cxGridDBTableView1cAutFin: TcxGridDBColumn
                DataBinding.FieldName = 'cAutFin'
                Width = 74
              end
              object cxGridDBTableView1nCdProduto: TcxGridDBColumn
                DataBinding.FieldName = 'nCdProduto'
                Width = 92
              end
              object cxGridDBTableView1cNmProduto: TcxGridDBColumn
                DataBinding.FieldName = 'cNmProduto'
                Width = 276
              end
              object cxGridDBTableView1nQtdePed: TcxGridDBColumn
                DataBinding.FieldName = 'nQtdePed'
              end
              object cxGridDBTableView1nQtdeProdParcial: TcxGridDBColumn
                DataBinding.FieldName = 'nQtdeProdParcial'
                Width = 97
              end
            end
            object cxGridLevel1: TcxGridLevel
              GridView = cxGridDBTableView1
            end
          end
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Aguardando Separa'#231#227'o'
      ImageIndex = 1
      object cxPageControl2: TcxPageControl
        Left = 0
        Top = 46
        Width = 1128
        Height = 579
        ActivePage = cxTabSheet5
        Align = alClient
        LookAndFeel.NativeStyle = True
        PopupMenu = popAguardSeparacaoResumo
        TabOrder = 0
        ClientRectBottom = 575
        ClientRectLeft = 4
        ClientRectRight = 1124
        ClientRectTop = 24
        object cxTabSheet5: TcxTabSheet
          Caption = 'Resumido'
          ImageIndex = 0
          object cxGrid2: TcxGrid
            Left = 0
            Top = 0
            Width = 1120
            Height = 551
            Align = alClient
            PopupMenu = popAguardSeparacaoResumo
            TabOrder = 0
            object cxGrid2DBTableView1: TcxGridDBTableView
              OnDblClick = cxGrid2DBTableView1DblClick
              DataController.DataSource = dsAguardSepResum
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              Styles.Content = frmMenu.FonteSomenteLeitura
              object cxGrid2DBTableView1nCdPedido: TcxGridDBColumn
                DataBinding.FieldName = 'nCdPedido'
              end
              object cxGrid2DBTableView1cNmTipoPedido: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTipoPedido'
                Width = 116
              end
              object cxGrid2DBTableView1dDtPedido: TcxGridDBColumn
                DataBinding.FieldName = 'dDtPedido'
              end
              object cxGrid2DBTableView1dDtPrevEntFim: TcxGridDBColumn
                DataBinding.FieldName = 'dDtPrevEntFim'
              end
              object cxGrid2DBTableView1cAtraso: TcxGridDBColumn
                DataBinding.FieldName = 'cAtraso'
                Width = 67
              end
              object cxGrid2DBTableView1cNmTerceiro: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiro'
                Width = 281
              end
              object cxGrid2DBTableView1cNmTerceiroPagador: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiroPagador'
                Width = 267
              end
              object cxGrid2DBTableView1cAutFin: TcxGridDBColumn
                DataBinding.FieldName = 'cAutFin'
                Width = 75
              end
              object cxGrid2DBTableView1DBColumn1: TcxGridDBColumn
                Caption = 'Tipo de Entrega'
                DataBinding.FieldName = 'cTipoEntrega'
                Width = 120
              end
            end
            object cxGrid2Level1: TcxGridLevel
              GridView = cxGrid2DBTableView1
            end
          end
        end
        object cxTabSheet11: TcxTabSheet
          Caption = 'Detalhado'
          ImageIndex = 1
          object cxGrid3: TcxGrid
            Left = 0
            Top = 0
            Width = 1120
            Height = 551
            Align = alClient
            PopupMenu = popAguardSeparacaoAnalitico
            TabOrder = 0
            object cxGrid3DBTableView1: TcxGridDBTableView
              OnDblClick = cxGrid3DBTableView1DblClick
              DataController.DataSource = dsAguardSepDetail
              DataController.Summary.DefaultGroupSummaryItems = <
                item
                  Format = ',0'
                  Kind = skSum
                  Position = spFooter
                  Column = cxGrid3DBTableView1nQtdePed
                end
                item
                  Format = ',0'
                  Kind = skSum
                  Position = spFooter
                  Column = cxGrid3DBTableView1nQtdeAberto
                end>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = ',0'
                  Kind = skSum
                  Column = cxGrid3DBTableView1nQtdePed
                end
                item
                  Format = ',0'
                  Kind = skSum
                  Column = cxGrid3DBTableView1nQtdeAberto
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              Styles.Content = frmMenu.FonteSomenteLeitura
              object cxGrid3DBTableView1nCdPedido: TcxGridDBColumn
                DataBinding.FieldName = 'nCdPedido'
              end
              object cxGrid3DBTableView1cNmTipoPedido: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTipoPedido'
                Width = 173
              end
              object cxGrid3DBTableView1dDtEntrega: TcxGridDBColumn
                DataBinding.FieldName = 'dDtEntrega'
              end
              object cxGrid3DBTableView1cAtraso: TcxGridDBColumn
                DataBinding.FieldName = 'cAtraso'
                Width = 64
              end
              object cxGrid3DBTableView1cNmTerceiroEntrega: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiroEntrega'
                Width = 156
              end
              object cxGrid3DBTableView1cNmTerceiroPagador: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiroPagador'
                Width = 165
              end
              object cxGrid3DBTableView1cAutFin: TcxGridDBColumn
                DataBinding.FieldName = 'cAutFin'
                Width = 77
              end
              object cxGrid3DBTableView1nCdProduto: TcxGridDBColumn
                DataBinding.FieldName = 'nCdProduto'
                Width = 95
              end
              object cxGrid3DBTableView1cNmItem: TcxGridDBColumn
                DataBinding.FieldName = 'cNmItem'
                Width = 243
              end
              object cxGrid3DBTableView1nQtdePed: TcxGridDBColumn
                DataBinding.FieldName = 'nQtdePed'
              end
              object cxGrid3DBTableView1nQtdeAberto: TcxGridDBColumn
                DataBinding.FieldName = 'nQtdeAberto'
                Width = 79
              end
              object cxGrid3DBTableView1nPercCumprimento: TcxGridDBColumn
                DataBinding.FieldName = 'nPercCumprimento'
                Width = 111
              end
            end
            object cxGrid3Level1: TcxGridLevel
              GridView = cxGrid3DBTableView1
            end
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1128
        Height = 46
        Align = alTop
        TabOrder = 1
        object btGerarPicking: TcxButton
          Left = 6
          Top = 10
          Width = 147
          Height = 31
          Caption = 'Gerar Picking'
          TabOrder = 0
          OnClick = btGerarPickingClick
          Glyph.Data = {
            36030000424D360300000000000036000000280000000F000000100000000100
            1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFD6D9D868513A60504060483070504070605070
            5040604830604830604830604830604830A6A6A9FFFFFF000000FFFFFFC0A8A0
            F0F0F0E0D8D0E0D0C0E0C8C09090B0D0C0B0E0B8A0D0B0A0D0B0A0D0A890D0A0
            90604830FFFFFF000000FFFFFFC0A8A0FFF0F0F0F0F0F0E8E03050C01038B070
            78C0E0D0D0F0D0C0E0D0C0E0C8B0D0A890604830FFFFFF000000FFFFFFC0A8A0
            FFF0F0D0D0E03050C03058F02048E01038B0A098C0F0D0C0F0D0C0E0C8B0D0A8
            90604830FFFFFF000000FFFFFFC0B0A0FFF8F02040C03058F06080FF5078F040
            60F02040B0D0C0C0F0D8D0E0C8C0D0B0A0604830FFFFFF000000FFFFFFC0B0A0
            FFF8F080A0FF8098FF8090F0D0D0E08098F04060E04058B0F0D8D0F0D8D0D0B8
            A0605040FFFFFF000000FFFFFFD0B0A0FFF8FFE0E8FFC0C8F0F0F0F0F0F0E0E0
            D8E08090F03058E05068B0F0E0D0E0C8B0705840FFFFFF000000FFFFFFD0B8A0
            FFFFFFFFF8FFFFF8F0FFF8F0FFF0F0F0F0E0F0E0E07088F02050D09090C0E0D0
            C0807060FFFFFF000000FFFFFFD0B8B0FFFFFFFFFFFFFFF8FFFFF8F0FFF8F0F0
            F0F0F0E0E0F0E8E08090F02048D0A098C0A09080FFFFFF000000FFFFFFD0C0B0
            FFFFFFFFFFFF80A0B060889060889060789060788070809090A0B090A0F03050
            D0B09890FFFFFF000000FFFFFFD0C0B0FFFFFFFFFFFF80A8B090D8E090E8F080
            D8F060C8E05098B0708090F0E8E0E0D8D0A09890FFFFFF000000FFFFFFD1C2B3
            FFFFFFFFFFFFF0F8FF80A8B0A0A8A095867780C8D0507080F0F0F0F0E0E0F0E0
            E0807060FFFFFF000000FFFFFFE2E5E1D1C2B3D0C0B0D0C0B070A8B0A0E8F0A0
            E8F090D0E0406870C0A8A0C0A8A0C0A890D6DAD6FFFFFF000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFDCE3E180B0C080A0B07090A0D8DDD9FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
          LookAndFeel.NativeStyle = True
        end
        object cxButton5: TcxButton
          Left = 158
          Top = 10
          Width = 147
          Height = 31
          Caption = 'Autorizar Faturamento'
          TabOrder = 1
          OnClick = cxButton5Click
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            1800000000000003000000000000000000000000000000000000F6DFCDF6DFCD
            F6DFCDF6DFCDF6DFCDF6DFCDEBDBCD2078B02078B0306890304850D0C4B8F6DF
            CDF6DFCDF6DFCDF6DFCDF5DCCAF5DCCAF5DCCAF5DCCAF5DCCAF5DCCAB0D8F0C0
            F0F040D0FF30C0FF30A8F0304850207090304850CFC3B9F5DCCAF3DBC8E8D7C9
            2078B02078B0306890304050E6D2C2A0C8D0A0C0C090A0A080808070D0F040D0
            FF40B8F0405870F3DBC8F1D9C690B0C0C0F0F040D0FF30C0FF30B0F0103850F1
            D9C6F1D9C6E6D6C71078B01070A0205870203840B4B4B1F1D9C6F0D7C5F0D7C5
            90B0C0A0C0C090A0A0808890E8D3C2F0D7C5F0D7C590B0C0D0FFFF60D8FF40D0
            FF40B8F0305860F0D7C5EFD6C3EFD6C3EFD6C3EFD6C3EFD6C3EFD6C3EFD6C3EF
            D6C3EFD6C3E5D3C41068A01070A0205870203840B3B2AFEFD6C3EDD5C2EDD5C2
            EDD5C2EDD5C2EDD5C2EDD5C2EDD5C2EDD5C2EDD5C280C0D0D0FFFF60D8FF40D0
            FF40B8F0204860EDD5C2D0785090482090482090482080402080402080402080
            3820803820C0B8C00068A01070A02058702030408098A0802020D07850C08870
            C08050B09070D0A890E0C8B0C08060B06830C0505080B8E0C0F0F040D0FF30C0
            FF30A8F0205070802020D08050B09080D0C0B0E0D0C0D0B0A0E0C8C0B08870B0
            7040C07050D0B8A080B0E0A0C0D08098B07080A0D0B890802020D08860E0D0C0
            E0B8A0C08860C08050D0B8A0E0B090B07850B07050E0A880C09890905830A060
            40B0A090C09880802820E09070FFFFFFD09880D08860C08860E0D8D0C09880C0
            8050B07050C09070C0B8B0A05830905830A06030C0C0C0802820E09870E0D0D0
            F0C8B0D09870D08860F0D8D0C09880C08860B08050C09070D0C0B0A06030A060
            30B08870C09080803020E0A080B0A090E0C8C0F0E0E0F0C8B0E0B0A0F0C8B0D0
            A890C09880E0B8A0C09080C0A090D0B8B0B09880A06030803020E0A080A08880
            B09080B0A090D0C0B0FFF0E0FFF0F0F0F0F0F0F0F0F0E0E0F0D8C0C0A890A078
            60905830A05820803820E0A080E0A080E0A080E0A080E09880E09870D09070D0
            8870C08060C07860B07850B07050B06840A06040A05840905030}
          LookAndFeel.NativeStyle = True
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Em Separa'#231#227'o'
      ImageIndex = 2
      object cxPageControl3: TcxPageControl
        Left = 0
        Top = 46
        Width = 1128
        Height = 579
        ActivePage = cxTabSheet6
        Align = alClient
        LookAndFeel.NativeStyle = True
        PopupMenu = popEmSeparacao
        TabOrder = 0
        ClientRectBottom = 575
        ClientRectLeft = 4
        ClientRectRight = 1124
        ClientRectTop = 24
        object cxTabSheet6: TcxTabSheet
          Caption = 'Resumido'
          ImageIndex = 0
          object cxGrid4: TcxGrid
            Left = 0
            Top = 0
            Width = 1120
            Height = 551
            Align = alClient
            PopupMenu = popEmSeparacao
            TabOrder = 0
            object cxGrid4DBTableView1: TcxGridDBTableView
              OnDblClick = cxGrid4DBTableView1DblClick
              DataController.DataSource = dsEmSeparacao
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              Styles.Content = frmMenu.FonteSomenteLeitura
              object cxGrid4DBTableView1nCdPickList: TcxGridDBColumn
                DataBinding.FieldName = 'nCdPickList'
              end
              object cxGrid4DBTableView1nCdPedido: TcxGridDBColumn
                DataBinding.FieldName = 'nCdPedido'
              end
              object cxGrid4DBTableView1dDtGeracao: TcxGridDBColumn
                DataBinding.FieldName = 'dDtGeracao'
              end
              object cxGrid4DBTableView1cNmUsuarioGeracao: TcxGridDBColumn
                DataBinding.FieldName = 'cNmUsuarioGeracao'
              end
              object cxGrid4DBTableView1dDtPrimImp: TcxGridDBColumn
                DataBinding.FieldName = 'dDtPrimImp'
              end
              object cxGrid4DBTableView1cNmUsuarioPrimImp: TcxGridDBColumn
                DataBinding.FieldName = 'cNmUsuarioPrimImp'
              end
              object cxGrid4DBTableView1dDtEntrega: TcxGridDBColumn
                DataBinding.FieldName = 'dDtEntrega'
              end
              object cxGrid4DBTableView1cAtraso: TcxGridDBColumn
                DataBinding.FieldName = 'cAtraso'
                Width = 64
              end
              object cxGrid4DBTableView1cNmTerceiro: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiro'
                Width = 239
              end
              object cxGrid4DBTableView1cNmTerceiroPagador: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiroPagador'
                Width = 238
              end
              object cxGrid4DBTableView1cAutFin: TcxGridDBColumn
                DataBinding.FieldName = 'cAutFin'
                Width = 66
              end
            end
            object cxGrid4Level1: TcxGridLevel
              GridView = cxGrid4DBTableView1
            end
          end
        end
        object cxTabSheet7: TcxTabSheet
          Caption = 'Detalhado'
          ImageIndex = 1
          object cxGrid5: TcxGrid
            Left = 0
            Top = 0
            Width = 1120
            Height = 551
            Align = alClient
            PopupMenu = popEmSeparacao
            TabOrder = 0
            object cxGrid5DBTableView1: TcxGridDBTableView
              OnDblClick = cxGrid5DBTableView1DblClick
              DataController.DataSource = dsEmSepDetail
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              Styles.Content = frmMenu.FonteSomenteLeitura
              object cxGrid5DBTableView1nCdPickList: TcxGridDBColumn
                DataBinding.FieldName = 'nCdPickList'
              end
              object cxGrid5DBTableView1nCdPedido: TcxGridDBColumn
                DataBinding.FieldName = 'nCdPedido'
              end
              object cxGrid5DBTableView1dDtGeracao: TcxGridDBColumn
                DataBinding.FieldName = 'dDtGeracao'
              end
              object cxGrid5DBTableView1cNmUsuarioGeracao: TcxGridDBColumn
                DataBinding.FieldName = 'cNmUsuarioGeracao'
              end
              object cxGrid5DBTableView1dDtEntrega: TcxGridDBColumn
                DataBinding.FieldName = 'dDtEntrega'
              end
              object cxGrid5DBTableView1cAtraso: TcxGridDBColumn
                DataBinding.FieldName = 'cAtraso'
                Width = 75
              end
              object cxGrid5DBTableView1cNmTerceiro: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiro'
                Width = 256
              end
              object cxGrid5DBTableView1cNmTerceiroPagador: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiroPagador'
                Width = 252
              end
              object cxGrid5DBTableView1cAutFin: TcxGridDBColumn
                DataBinding.FieldName = 'cAutFin'
                Width = 74
              end
              object cxGrid5DBTableView1nCdProduto: TcxGridDBColumn
                DataBinding.FieldName = 'nCdProduto'
                Width = 89
              end
              object cxGrid5DBTableView1cNmItem: TcxGridDBColumn
                DataBinding.FieldName = 'cNmItem'
                Width = 302
              end
              object cxGrid5DBTableView1nQtdePick: TcxGridDBColumn
                DataBinding.FieldName = 'nQtdePick'
              end
            end
            object cxGrid5Level1: TcxGridLevel
              GridView = cxGrid5DBTableView1
            end
          end
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 0
        Width = 1128
        Height = 46
        Align = alTop
        TabOrder = 1
        object cxButton4: TcxButton
          Left = 310
          Top = 10
          Width = 147
          Height = 31
          Caption = 'Autorizar Faturamento'
          TabOrder = 0
          OnClick = cxButton4Click
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            1800000000000003000000000000000000000000000000000000F6DFCDF6DFCD
            F6DFCDF6DFCDF6DFCDF6DFCDEBDBCD2078B02078B0306890304850D0C4B8F6DF
            CDF6DFCDF6DFCDF6DFCDF5DCCAF5DCCAF5DCCAF5DCCAF5DCCAF5DCCAB0D8F0C0
            F0F040D0FF30C0FF30A8F0304850207090304850CFC3B9F5DCCAF3DBC8E8D7C9
            2078B02078B0306890304050E6D2C2A0C8D0A0C0C090A0A080808070D0F040D0
            FF40B8F0405870F3DBC8F1D9C690B0C0C0F0F040D0FF30C0FF30B0F0103850F1
            D9C6F1D9C6E6D6C71078B01070A0205870203840B4B4B1F1D9C6F0D7C5F0D7C5
            90B0C0A0C0C090A0A0808890E8D3C2F0D7C5F0D7C590B0C0D0FFFF60D8FF40D0
            FF40B8F0305860F0D7C5EFD6C3EFD6C3EFD6C3EFD6C3EFD6C3EFD6C3EFD6C3EF
            D6C3EFD6C3E5D3C41068A01070A0205870203840B3B2AFEFD6C3EDD5C2EDD5C2
            EDD5C2EDD5C2EDD5C2EDD5C2EDD5C2EDD5C2EDD5C280C0D0D0FFFF60D8FF40D0
            FF40B8F0204860EDD5C2D0785090482090482090482080402080402080402080
            3820803820C0B8C00068A01070A02058702030408098A0802020D07850C08870
            C08050B09070D0A890E0C8B0C08060B06830C0505080B8E0C0F0F040D0FF30C0
            FF30A8F0205070802020D08050B09080D0C0B0E0D0C0D0B0A0E0C8C0B08870B0
            7040C07050D0B8A080B0E0A0C0D08098B07080A0D0B890802020D08860E0D0C0
            E0B8A0C08860C08050D0B8A0E0B090B07850B07050E0A880C09890905830A060
            40B0A090C09880802820E09070FFFFFFD09880D08860C08860E0D8D0C09880C0
            8050B07050C09070C0B8B0A05830905830A06030C0C0C0802820E09870E0D0D0
            F0C8B0D09870D08860F0D8D0C09880C08860B08050C09070D0C0B0A06030A060
            30B08870C09080803020E0A080B0A090E0C8C0F0E0E0F0C8B0E0B0A0F0C8B0D0
            A890C09880E0B8A0C09080C0A090D0B8B0B09880A06030803020E0A080A08880
            B09080B0A090D0C0B0FFF0E0FFF0F0F0F0F0F0F0F0F0E0E0F0D8C0C0A890A078
            60905830A05820803820E0A080E0A080E0A080E0A080E09880E09870D09070D0
            8870C08060C07860B07850B07050B06840A06040A05840905030}
          LookAndFeel.NativeStyle = True
        end
        object cxButton6: TcxButton
          Left = 158
          Top = 10
          Width = 147
          Height = 31
          Caption = 'Cancelar Picking'
          TabOrder = 1
          OnClick = cxButton6Click
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3E7F79AA7E34C62CC36
            4FC5344DC3465DC795A1DEE1E5F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFBFC7EF4C63D15264D48490E795A0EE959FED838EE54C5DCE3D54C3B8C1
            E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1CAF14760D57584E3A1ACF47F8BEC5C
            67E45B66E37D87EA9FA8F16F7CDD324BC2B9C1EAFFFFFFFFFFFFFFFFFFE7EAFA
            5970DE7888E6A3B0F55767E75665E68992ED8892EC535FE2525DE19FA9F26F7D
            DD4157C6E2E6F6FFFFFFFFFFFFA8B4F06073E0A4B3F75A6EEB596CEA5869E8FC
            FCFCFCFCFC5562E55461E3535FE29FA9F25061D197A4E1FFFFFFFFFFFF6A82E9
            8E9FF08499F45C73EE5B70EC5A6EEB909DF1A6AFF35767E75665E65562E57D89
            EB8591E74E64CEFFFFFFFFFFFF5D76EAA0B3F76580F25F78F05D76EF5C73EEFC
            FCFCFCFCFC596CEA5869E85767E75D6CE799A5F13C55CCFFFFFFFFFFFF617BEE
            A1B6F86784F4607CF35F7AF15F78F0FCFCFCFCFCFC5B70EC5A6EEB596CEA5F6F
            E99BA8F14159D0FFFFFFFFFFFF768DF391A6F388A1F86280F4617EF3607CF3FC
            FCFCFCFCFC5D76EF5C73EE5B70EC8293F18998EC5970D8FFFFFFFFFFFFB2BFFA
            6C81ECA9BDFB6382F56281F56280F4FCFCFCFCFCFC5F7AF15F78F05D76EFA5B5
            F85D70DDA2AFEBFFFFFFFFFFFFEBEEFE758CF78397F0A9BDFB6382F56382F5FC
            FCFCFCFCFC617EF3607CF3A6B9F97B8DEA5C74E1E7EAFAFFFFFFFFFFFFFFFFFF
            CED7FD6D86F88497F1A9BDFB8AA3F86B89F66B89F689A2F8A8BCFA7F92EC5972
            E5C6CEF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCED7FD778EFA6E83EE92A6F4A0
            B4F8A0B4F891A6F3687DE96981EDC8D1F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFEBEEFFB5C3FD7C94FA6C86F76A84F5778EF5B1BEF8E9ECFDFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
        object btImprimePicking: TcxButton
          Left = 6
          Top = 10
          Width = 147
          Height = 31
          Caption = 'Imprimir Picking'
          TabOrder = 2
          OnClick = btImprimePickingClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFF97979764616063605F625F5E615E5E615E5D615E5D615E5D615E5D615E
            5DBAB1A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF999999FFFFFFFEFEFEFEFEFEFE
            FEFEFEFEFEFEFEFEFEFEFEFFFFFF615E5DBAB1A5FFFFFFFFFFFFD5CABCB9B0A4
            B9B0A49B9B9BFFFFFFA5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5FFFFFF615E
            5DB8B0A4B8B0A4D5CABBABA59C6666666464649D9D9DFFFFFFFDFDFDFDFDFDFD
            FDFDFDFDFDFDFDFDFDFDFDFFFFFF615E5D4848484545459D968E6969699B9B9B
            B8B8B88F8F8FE4E4E4949494949494949494949494949494949494E4E4E45B58
            57A5A5A58E918E4343436A6A6AB8B8B8B6B6B67A7A7AC1C1C1C1C1C1C1C1C1C1
            C1C1C1C1C1C1C1C1C1C1C1C1C1C153504FA3A3A335FE354444446C6C6CB8B8B8
            4742410101010202020202020101010101010101010101010101010101010101
            01464241A5A5A54646466D6D6DB8B8B8B8B8B85252526969696565656060605B
            5B5B5656565252524D4D4D484848333333A5A5A5A5A5A54747476E6E6EB8B8B8
            B8B8B85454546B6B6B6666666262625D5D5D5858585353534F4F4F4A4A4A3434
            34A5A5A5A5A5A5484848707070E7E7E7FFFFFFB7B7B7C5C5C5C0C0C0B8B8B8B2
            B2B2ABABABA3A3A39B9B9B9393936F6F6FFFFFFFCCCCCC4A4A4AACA69D6F6F6F
            6C6C6C6969696E6E6E6C6C6C6A6A6A6868686666666666666666666666665353
            535050504E4E4E9E988FFFFFFFFFFFFFFFFFFF9A9A9AF3F3F3EFEFEFEAEAEAE6
            E6E6E1E1E1DFDFDFDFDFDFDFDFDF615E5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF9B9B9BF7F7F7F3F3F3EEEEEEEAEAEAE5E5E5E1E1E1DFDFDFDFDFDF615E
            5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9D9D9DFBFBFBF7F7F7F2F2F2EE
            EEEEEAEAEAE5E5E5E1E1E1DFDFDF615E5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF615E
            5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1A1A19F9F9F9C9C9C9A9A9A96
            96969494949191918F8F8F8C8C8C8C8C8CFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = 'Aguardando Faturamento'
      ImageIndex = 3
      object cxPageControl4: TcxPageControl
        Left = 0
        Top = 46
        Width = 1128
        Height = 579
        ActivePage = cxTabSheet8
        Align = alClient
        LookAndFeel.NativeStyle = True
        TabOrder = 0
        ClientRectBottom = 575
        ClientRectLeft = 4
        ClientRectRight = 1124
        ClientRectTop = 24
        object cxTabSheet8: TcxTabSheet
          Caption = 'Resumido'
          ImageIndex = 0
          object cxGrid7: TcxGrid
            Left = 0
            Top = 0
            Width = 1120
            Height = 551
            Align = alClient
            PopupMenu = popAguardFaturamento
            TabOrder = 0
            object cxGrid7DBTableView1: TcxGridDBTableView
              OnDblClick = cxGrid7DBTableView1DblClick
              DataController.DataSource = dsAguardandoFat
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              Styles.Content = frmMenu.FonteSomenteLeitura
              object cxGrid7DBTableView1nCdPedido: TcxGridDBColumn
                DataBinding.FieldName = 'nCdPedido'
              end
              object cxGrid7DBTableView1cNmTipoPedido: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTipoPedido'
                Width = 217
              end
              object cxGrid7DBTableView1dDtPedido: TcxGridDBColumn
                DataBinding.FieldName = 'dDtPedido'
              end
              object cxGrid7DBTableView1dDtPrevEntFim: TcxGridDBColumn
                DataBinding.FieldName = 'dDtPrevEntFim'
              end
              object cxGrid7DBTableView1cAtraso: TcxGridDBColumn
                DataBinding.FieldName = 'cAtraso'
                Width = 66
              end
              object cxGrid7DBTableView1cNmTerceiro: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiro'
                Width = 255
              end
              object cxGrid7DBTableView1cNmTerceiroPagador: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiroPagador'
                Width = 256
              end
              object cxGrid7DBTableView1cAutFin: TcxGridDBColumn
                DataBinding.FieldName = 'cAutFin'
                Width = 73
              end
            end
            object cxGrid7Level1: TcxGridLevel
              GridView = cxGrid7DBTableView1
            end
          end
        end
        object cxTabSheet9: TcxTabSheet
          Caption = 'Detalhado'
          ImageIndex = 1
          object cxGrid6: TcxGrid
            Left = 0
            Top = 0
            Width = 1120
            Height = 551
            Align = alClient
            PopupMenu = popAguardFaturamento
            TabOrder = 0
            object cxGrid6DBTableView1: TcxGridDBTableView
              OnDblClick = cxGrid6DBTableView1DblClick
              DataController.DataSource = dsAguardFatDetail
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              Styles.Content = frmMenu.FonteSomenteLeitura
              object cxGrid6DBTableView1nCdPedido: TcxGridDBColumn
                DataBinding.FieldName = 'nCdPedido'
              end
              object cxGrid6DBTableView1cNmTipoPedido: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTipoPedido'
                Width = 118
              end
              object cxGrid6DBTableView1dDtEntrega: TcxGridDBColumn
                DataBinding.FieldName = 'dDtEntrega'
              end
              object cxGrid6DBTableView1cAtraso: TcxGridDBColumn
                DataBinding.FieldName = 'cAtraso'
                Width = 70
              end
              object cxGrid6DBTableView1cNmTerceiroEntrega: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiroEntrega'
                Width = 239
              end
              object cxGrid6DBTableView1cNmTerceiroPagador: TcxGridDBColumn
                DataBinding.FieldName = 'cNmTerceiroPagador'
                Width = 244
              end
              object cxGrid6DBTableView1cAutFin: TcxGridDBColumn
                DataBinding.FieldName = 'cAutFin'
                Width = 73
              end
              object cxGrid6DBTableView1nCdProduto: TcxGridDBColumn
                DataBinding.FieldName = 'nCdProduto'
                Width = 96
              end
              object cxGrid6DBTableView1cNmProduto: TcxGridDBColumn
                DataBinding.FieldName = 'cNmProduto'
                Width = 304
              end
              object cxGrid6DBTableView1nQtdePed: TcxGridDBColumn
                DataBinding.FieldName = 'nQtdePed'
              end
              object cxGrid6DBTableView1nQtdeLibFat: TcxGridDBColumn
                DataBinding.FieldName = 'nQtdeLibFat'
              end
              object cxGrid6DBTableView1nPercCumprimento: TcxGridDBColumn
                DataBinding.FieldName = 'nPercCumprimento'
                Width = 106
              end
            end
            object cxGrid6Level1: TcxGridLevel
              GridView = cxGrid6DBTableView1
            end
          end
        end
      end
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 1128
        Height = 46
        Align = alTop
        TabOrder = 1
        object btGerarNF: TcxButton
          Left = 6
          Top = 10
          Width = 133
          Height = 31
          Caption = 'Gerar Nota Fiscal'
          TabOrder = 0
          OnClick = btGerarNFClick
          Glyph.Data = {
            36030000424D360300000000000036000000280000000F000000100000000100
            1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFD6D9D868513A60504060483070504070605070
            5040604830604830604830604830604830A6A6A9FFFFFF000000FFFFFFC0A8A0
            F0F0F0E0D8D0E0D0C0E0C8C09090B0D0C0B0E0B8A0D0B0A0D0B0A0D0A890D0A0
            90604830FFFFFF000000FFFFFFC0A8A0FFF0F0F0F0F0F0E8E03050C01038B070
            78C0E0D0D0F0D0C0E0D0C0E0C8B0D0A890604830FFFFFF000000FFFFFFC0A8A0
            FFF0F0D0D0E03050C03058F02048E01038B0A098C0F0D0C0F0D0C0E0C8B0D0A8
            90604830FFFFFF000000FFFFFFC0B0A0FFF8F02040C03058F06080FF5078F040
            60F02040B0D0C0C0F0D8D0E0C8C0D0B0A0604830FFFFFF000000FFFFFFC0B0A0
            FFF8F080A0FF8098FF8090F0D0D0E08098F04060E04058B0F0D8D0F0D8D0D0B8
            A0605040FFFFFF000000FFFFFFD0B0A0FFF8FFE0E8FFC0C8F0F0F0F0F0F0E0E0
            D8E08090F03058E05068B0F0E0D0E0C8B0705840FFFFFF000000FFFFFFD0B8A0
            FFFFFFFFF8FFFFF8F0FFF8F0FFF0F0F0F0E0F0E0E07088F02050D09090C0E0D0
            C0807060FFFFFF000000FFFFFFD0B8B0FFFFFFFFFFFFFFF8FFFFF8F0FFF8F0F0
            F0F0F0E0E0F0E8E08090F02048D0A098C0A09080FFFFFF000000FFFFFFD0C0B0
            FFFFFFFFFFFF80A0B060889060889060789060788070809090A0B090A0F03050
            D0B09890FFFFFF000000FFFFFFD0C0B0FFFFFFFFFFFF80A8B090D8E090E8F080
            D8F060C8E05098B0708090F0E8E0E0D8D0A09890FFFFFF000000FFFFFFD1C2B3
            FFFFFFFFFFFFF0F8FF80A8B0A0A8A095867780C8D0507080F0F0F0F0E0E0F0E0
            E0807060FFFFFF000000FFFFFFE2E5E1D1C2B3D0C0B0D0C0B070A8B0A0E8F0A0
            E8F090D0E0406870C0A8A0C0A8A0C0A890D6DAD6FFFFFF000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFDCE3E180B0C080A0B07090A0D8DDD9FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
          LookAndFeel.NativeStyle = True
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 320
    Top = 280
    Bitmap = {
      494C010104000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EBDBCD002078B0002078B0003068900030485000D0C4
      B800000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6D9D80068513A006050
      4000604830007050400070605000705040006048300060483000604830006048
      300060483000A6A6A9000000000000000000F5DCCA00F5DCCA00F5DCCA00F5DC
      CA00F5DCCA00F5DCCA00B0D8F000C0F0F00040D0FF0030C0FF0030A8F0003048
      50002070900030485000CFC3B900F5DCCA000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0A8A000F0F0F000E0D8
      D000E0D0C000E0C8C0009090B000D0C0B000E0B8A000D0B0A000D0B0A000D0A8
      9000D0A09000604830000000000000000000F3DBC800E8D7C9002078B0002078
      B0003068900030405000E6D2C200A0C8D000A0C0C00090A0A0008080800070D0
      F00040D0FF0040B8F00040587000F3DBC8000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000840000008400000000000000000000000000C0A8A000FFF0F000F0F0
      F000F0E8E0003050C0001038B0007078C000E0D0D000F0D0C000E0D0C000E0C8
      B000D0A89000604830000000000000000000F1D9C60090B0C000C0F0F00040D0
      FF0030C0FF0030B0F00010385000F1D9C600F1D9C600E6D6C7001078B0001070
      A0002058700020384000B4B4B100F1D9C60000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000C0A8A000FFF0F000D0D0
      E0003050C0003058F0002048E0001038B000A098C000F0D0C000F0D0C000E0C8
      B000D0A89000604830000000000000000000F0D7C500F0D7C50090B0C000A0C0
      C00090A0A00080889000E8D3C200F0D7C500F0D7C50090B0C000D0FFFF0060D8
      FF0040D0FF0040B8F00030586000F0D7C50000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000C0B0A000FFF8F0002040
      C0003058F0006080FF005078F0004060F0002040B000D0C0C000F0D8D000E0C8
      C000D0B0A000604830000000000000000000EFD6C300EFD6C300EFD6C300EFD6
      C300EFD6C300EFD6C300EFD6C300EFD6C300EFD6C300E5D3C4001068A0001070
      A0002058700020384000B3B2AF00EFD6C30000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000C0B0A000FFF8F00080A0
      FF008098FF008090F000D0D0E0008098F0004060E0004058B000F0D8D000F0D8
      D000D0B8A000605040000000000000000000EDD5C200EDD5C200EDD5C200EDD5
      C200EDD5C200EDD5C200EDD5C200EDD5C200EDD5C20080C0D000D0FFFF0060D8
      FF0040D0FF0040B8F00020486000EDD5C20000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000D0B0A000FFF8FF00E0E8
      FF00C0C8F000F0F0F000F0F0E000E0D8E0008090F0003058E0005068B000F0E0
      D000E0C8B000705840000000000000000000D078500090482000904820009048
      20008040200080402000804020008038200080382000C0B8C0000068A0001070
      A00020587000203040008098A0008020200000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000000000000D0B8A00000000000FFF8
      FF00FFF8F000FFF8F000FFF0F000F0F0E000F0E0E0007088F0002050D0009090
      C000E0D0C000807060000000000000000000D0785000C0887000C0805000B090
      7000D0A89000E0C8B000C0806000B0683000C050500080B8E000C0F0F00040D0
      FF0030C0FF0030A8F000205070008020200000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000000000000D0B8B000000000000000
      0000FFF8FF00FFF8F000FFF8F000F0F0F000F0E0E000F0E8E0008090F0002048
      D000A098C000A09080000000000000000000D0805000B0908000D0C0B000E0D0
      C000D0B0A000E0C8C000B0887000B0704000C0705000D0B8A00080B0E000A0C0
      D0008098B0007080A000D0B890008020200000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000000000000D0C0B000000000000000
      000080A0B000608890006088900060789000607880007080900090A0B00090A0
      F0003050D000B09890000000000000000000D0886000E0D0C000E0B8A000C088
      6000C0805000D0B8A000E0B09000B0785000B0705000E0A88000C09890009058
      3000A0604000B0A09000C09880008028200000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000D0C0B000000000000000
      000080A8B00090D8E00090E8F00080D8F00060C8E0005098B00070809000F0E8
      E000E0D8D000A09890000000000000000000E0907000FFFFFF00D0988000D088
      6000C0886000E0D8D000C0988000C0805000B0705000C0907000C0B8B000A058
      300090583000A0603000C0C0C0008028200000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF0000008400000000000000000000000000D1C2B300000000000000
      0000F0F8FF0080A8B000A0A8A0009586770080C8D00050708000F0F0F000F0E0
      E000F0E0E000807060000000000000000000E0987000E0D0D000F0C8B000D098
      7000D0886000F0D8D000C0988000C0886000B0805000C0907000D0C0B000A060
      3000A0603000B0887000C09080008030200000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF00000000000000000000000000E2E5E100D1C2B300D0C0
      B000D0C0B00070A8B000A0E8F000A0E8F00090D0E00040687000C0A8A000C0A8
      A000C0A89000D6DAD6000000000000000000E0A08000B0A09000E0C8C000F0E0
      E000F0C8B000E0B0A000F0C8B000D0A89000C0988000E0B8A000C0908000C0A0
      9000D0B8B000B0988000A06030008030200000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DCE3E10080B0C00080A0B0007090A000D8DDD900000000000000
      000000000000000000000000000000000000E0A08000A0888000B0908000B0A0
      9000D0C0B000FFF0E000FFF0F000F0F0F000F0F0F000F0E0E000F0D8C000C0A8
      9000A078600090583000A0582000803820000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E0A08000E0A08000E0A08000E0A0
      8000E0988000E0987000D0907000D0887000C0806000C0786000B0785000B070
      5000B0684000A0604000A058400090503000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFFFC0FFE00FFFF80030000
      FE00C00380030000000080018003000000008001800300000000800180030000
      0000800180030000000080018003000000008001A003000000008001B0030000
      00008001B003000000008001B003000000018001B00300000003800180030000
      0077C003F83F0000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryAguardProd: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempAguardandoProd'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempAguardandoProd (nCdPedido          int'
      '                                     ,nCdOrdemProducao   int'
      
        '                                     ,cNumeroOP          varchar' +
        '(20)'
      
        '                                     ,cNmTipoPedido      varchar' +
        '(50)'
      
        '                                     ,dDtEntrega         datetim' +
        'e'
      
        '                                     ,cAtraso            varchar' +
        '(3)'
      
        '                                     ,cNmTerceiro        varchar' +
        '(100)'
      
        '                                     ,cNmTerceiroPagador varchar' +
        '(100)'
      
        '                                     ,cAutFin            varchar' +
        '(7)'
      '                                     ,nCdProduto         int'
      
        '                                     ,cNmProduto         varchar' +
        '(150)'
      
        '                                     ,nQtdePed           decimal' +
        '(12,4)'
      
        '                                     ,nQtdeAberto        decimal' +
        '(12,4) default 0 not null'
      
        '                                     ,nPercCumprimento   decimal' +
        '(12,2))'
      'END'
      ''
      'TRUNCATE TABLE #TempAguardandoProd'
      ''
      ''
      'INSERT INTO #TempAguardandoProd (nCdPedido'
      '                                ,nCdOrdemProducao'
      '                                ,cNumeroOP'
      '                                ,cNmTipoPedido'
      '                                ,dDtEntrega'
      '                                ,cAtraso'
      '                                ,cNmTerceiro'
      '                                ,cNmTerceiroPagador'
      '                                ,cAutFin'
      '                                ,nCdProduto'
      '                                ,cNmProduto'
      '                                ,nQtdePed'
      '                                ,nQtdeAberto)'
      ''
      'SELECT Pedido.nCdPedido'
      '      ,OP.nCdOrdemProducao'
      '      ,cNumeroOP'
      '      ,TP.cNmTipoPedido'
      '      ,Pedido.dDtPrevEntFim'
      '      ,(CASE WHEN (Pedido.dDtPrevEntFim < GetDate())'
      '             THEN '#39'SIM'#39
      '             ELSE Null'
      '       END) cAtraso'
      '      ,Terceiro.cNmTerceiro'
      '      ,TPag.cNmTerceiro'
      '      ,(CASE WHEN (Pedido.nCdTabStatusPed IN (3,4))'
      '             THEN '#39'SIM'#39
      
        '             WHEN ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlg' +
        'LibParcial = 1))'
      '             THEN '#39'PARCIAL'#39
      '             ELSE Null'
      '       END) cAutFin'
      '      ,ItemPedido.nCdProduto'
      '      ,Produto.cNmProduto'
      '      ,ItemPedido.nQtdePed'
      '      ,(SELECT ItemPedido.nQtdePed'
      '               - isNull((SELECT SUM(ItemPickList.nQtdePick)'
      '                           FROM ItemPickList'
      
        '                                INNER JOIN PickList ON PickList.' +
        'nCdPickList = ItemPickList.nCdPickList'
      
        '                          WHERE PickList.nCdPedido         = Ite' +
        'mPedido.nCdPedido'
      '                            AND PickList.dDtCancel IS NULL'
      
        '                            AND ItemPickList.nCdItemPedido = Ite' +
        'mPedido.nCdItemPedido'
      
        '                            AND PickList.cFlgBaixado       = 0),' +
        '0)'
      '               - ItemPedido.nQtdeLibFat'
      '               - ItemPedido.nQtdeExpRec'
      '               - ItemPedido.nQtdeCanc'
      '               - isNull(OP.nQtdeConcluida,0))'
      '  FROM ItemPedido'
      
        '       INNER JOIN Pedido           ON Pedido.nCdPedido     = Ite' +
        'mPedido.nCdPedido'
      
        '       INNER JOIN TipoPedido    TP ON TP.nCdTipoPedido     = Ped' +
        'ido.nCdTipoPedido'
      
        '       INNER JOIN Produto          ON Produto.nCdProduto   = Ite' +
        'mPedido.nCdProduto'
      
        '       LEFT  JOIN OrdemProducao OP ON OP.nCdPedido         = Ite' +
        'mPedido.nCdPedido'
      
        '       LEFT  JOIN Terceiro         ON Terceiro.nCdTerceiro = Ped' +
        'ido.nCdTerceiro'
      
        '       LEFT  JOIN Terceiro    TPag ON TPag.nCdTerceiro     = Ped' +
        'ido.nCdTerceiroPagador'
      ' WHERE Pedido.nCdEmpresa = :nCdEmpresa'
      
        '   AND ((Pedido.nCdTabStatusPed = 3) OR (Pedido.nCdTabStatusPed ' +
        '= 4) OR ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlgLibParcial' +
        ' = 1)))'
      '   AND OP.nCdTabTipoStatusOP     <> 6'
      '   AND OP.nCdPedido               = ItemPedido.nCdPedido'
      '   AND OP.nCdProduto              = ItemPedido.nCdProduto'
      ''
      'UPDATE #TempAguardandoProd'
      '   SET nPercCumprimento = (CASE WHEN (nQtdePed > 0)'
      
        '                                THEN CONVERT (decimal(12,2),((1 ' +
        '- (nQtdeAberto/ nQtdePed)) * 100))'
      '                                ELSE 0'
      '                           END)'
      ''
      'SELECT * '
      '  FROM #TempAguardandoProd'
      ' WHERE nQtdeAberto > 0')
    Left = 84
    Top = 245
    object qryAguardProdnCdPedido: TIntegerField
      DisplayLabel = 'Pedido'
      FieldName = 'nCdPedido'
    end
    object qryAguardProdnCdOrdemProducao: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object qryAguardProdcNumeroOP: TStringField
      DisplayLabel = 'N'#250'mero OP'
      FieldName = 'cNumeroOP'
    end
    object qryAguardProdcNmTipoPedido: TStringField
      DisplayLabel = 'Tipo Pedido'
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryAguardProddDtEntrega: TDateTimeField
      DisplayLabel = 'Data Entrega'
      FieldName = 'dDtEntrega'
    end
    object qryAguardProdcAtraso: TStringField
      DisplayLabel = 'Atraso?'
      FieldName = 'cAtraso'
      Size = 3
    end
    object qryAguardProdcNmTerceiro: TStringField
      DisplayLabel = 'Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryAguardProdcNmTerceiroPagador: TStringField
      DisplayLabel = 'Terceiro Pagador'
      FieldName = 'cNmTerceiroPagador'
      Size = 50
    end
    object qryAguardProdcAutFin: TStringField
      DisplayLabel = 'Aut. Fin ?'
      FieldName = 'cAutFin'
      Size = 7
    end
    object qryAguardProdnCdProduto: TIntegerField
      DisplayLabel = 'C'#243'd. Produto'
      FieldName = 'nCdProduto'
    end
    object qryAguardProdcNmProduto: TStringField
      DisplayLabel = 'Descri'#231#227'o Produto'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryAguardProdnQtdePed: TBCDField
      DisplayLabel = 'Qt. Pedido'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryAguardProdnQtdeAberto: TBCDField
      DisplayLabel = 'Qt. Aberta'
      FieldName = 'nQtdeAberto'
      Precision = 12
    end
    object qryAguardProdnPercCumprimento: TBCDField
      DisplayLabel = '% Cumprimento'
      FieldName = 'nPercCumprimento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsAguardProd: TDataSource
    DataSet = qryAguardProd
    Left = 84
    Top = 277
  end
  object qryAguardSepResum: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      'SET NOCOUNT ON'
      ''
      'SELECT Pedido.nCdPedido'
      '      ,TipoPedido.cNmTipoPedido'
      '      ,dbo.fn_OnlyDate(Pedido.dDtPedido) dDtPedido'
      '      ,dbo.fn_OnlyDate(Pedido.dDtPrevEntFim) dDtPrevEntFim'
      '      ,(CASE WHEN (Pedido.dDtPrevEntFim < GetDate())'
      '             THEN '#39'SIM'#39
      '             ELSE Null'
      '       END) cAtraso'
      '      ,Terceiro.cNmTerceiro'
      '      ,TTransp.cNmTerceiro cNmTerceiroPagador'
      '      ,(CASE WHEN (Pedido.nCdTabStatusPed IN (3,4))'
      '             THEN '#39'SIM'#39
      
        '             WHEN ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlg' +
        'LibParcial = 1))'
      '             THEN '#39'PARCIAL'#39
      '             ELSE Null'
      '       END) cAutFin'
      '      ,Pedido.cAnotacao cOBS'
      '      ,UPPER(PedidoWeb.cTipoEntrega) as cTipoEntrega'
      '  FROM Pedido'
      
        '       INNER JOIN TipoPedido       ON TipoPedido.nCdTipoPedido =' +
        ' Pedido.nCdTipoPedido'
      
        '       LEFT  JOIN PedidoWeb        ON PedidoWeb.nCdPedido      =' +
        ' Pedido.nCdPedido'
      
        '       LEFT  JOIN Terceiro         ON Terceiro.nCdTerceiro     =' +
        ' Pedido.nCdTerceiro'
      
        '       LEFT  JOIN Terceiro TTransp ON TTransp.nCdTerceiro      =' +
        ' Pedido.nCdTerceiroPagador'
      ' WHERE Pedido.nCdEmpresa               = :nCdEmpresa'
      '   AND TipoPedido.cFlgAguardarMontagem = 1'
      
        '   AND ((Pedido.nCdTabStatusPed = 3) OR (Pedido.nCdTabStatusPed ' +
        '= 4) OR ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlgLibParcial' +
        ' = 1)))'
      '   AND EXISTS(SELECT TOP 1 1 '
      '                FROM ItemPedido '
      
        '                     LEFT JOIN OrdemProducao OP ON OP.nCdPedido ' +
        '= ItemPedido.nCdPedido AND ItemPedido.nCdProduto = OP.nCdProduto'
      '                WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      
        '                  AND OP.nCdOrdemProducao IS NULL)              ' +
        '     '
      '   AND EXISTS(SELECT 1'
      '                FROM ItemPedido '
      '               WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      '                 AND  (ItemPedido.nQtdePed'
      
        '                        - isNull((SELECT SUM(ItemPickList.nQtdeP' +
        'ick)'
      '                                  FROM ItemPickList'
      
        '                                       INNER JOIN PickList ON Pi' +
        'ckList.nCdPickList = ItemPickList.nCdPickList'
      
        '                                 WHERE PickList.nCdPedido       ' +
        '  = ItemPedido.nCdPedido'
      
        '                                   AND PickList.dDtCancel IS NUL' +
        'L'
      
        '                                   AND ItemPickList.nCdItemPedid' +
        'o = ItemPedido.nCdItemPedido'
      
        '                                   AND PickList.cFlgBaixado = 0)' +
        ',0)'
      '                        - ItemPedido.nQtdeLibFat'
      '                        - ItemPedido.nQtdeExpRec'
      '                        - ItemPedido.nQtdeCanc) > 0)')
    Left = 116
    Top = 245
    object qryAguardSepResumnCdPedido: TIntegerField
      DisplayLabel = 'Pedido'
      FieldName = 'nCdPedido'
    end
    object qryAguardSepResumcNmTipoPedido: TStringField
      DisplayLabel = 'Tipo Pedido'
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryAguardSepResumdDtPedido: TDateTimeField
      DisplayLabel = 'Data Gera'#231#227'o'
      FieldName = 'dDtPedido'
    end
    object qryAguardSepResumdDtPrevEntFim: TDateTimeField
      DisplayLabel = 'Data Entrega'
      FieldName = 'dDtPrevEntFim'
    end
    object qryAguardSepResumcAtraso: TStringField
      DisplayLabel = 'Atraso?'
      FieldName = 'cAtraso'
      ReadOnly = True
      Size = 3
    end
    object qryAguardSepResumcNmTerceiro: TStringField
      DisplayLabel = 'Terceiro Entrega'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryAguardSepResumcNmTerceiroPagador: TStringField
      DisplayLabel = 'Terceiro Pagador'
      FieldName = 'cNmTerceiroPagador'
      Size = 50
    end
    object qryAguardSepResumcAutFin: TStringField
      DisplayLabel = 'Aut. Fin ?'
      FieldName = 'cAutFin'
      ReadOnly = True
      Size = 7
    end
    object qryAguardSepResumcOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
    object qryAguardSepResumcTipoEntrega: TStringField
      FieldName = 'cTipoEntrega'
      Size = 100
    end
  end
  object dsAguardSepResum: TDataSource
    DataSet = qryAguardSepResum
    Left = 120
    Top = 278
  end
  object qryEmSeparacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      'SET NOCOUNT ON'
      ''
      'SELECT PickList.nCdPickList'
      '      ,PickList.nCdPedido'
      '      ,PickList.dDtGeracao'
      '      ,Usuario.cNmLogin cNmUsuarioGeracao'
      '      ,PickList.dDtPrimImp'
      '      ,UPI.cNmLogin cNmUsuarioPrimImp'
      '      ,dbo.fn_OnlyDate(Pedido.dDtPrevEntFim) dDtEntrega'
      '      ,(CASE WHEN (Pedido.dDtPrevEntFim < GetDate())'
      '             THEN '#39'SIM'#39
      '             ELSE Null'
      '       END) cAtraso'
      '      ,Terceiro.cNmTerceiro'
      '      ,TPag.cNmTerceiro cNmTerceiroPagador'
      '      ,(CASE WHEN (Pedido.nCdTabStatusPed IN (3,4))'
      '             THEN '#39'SIM'#39
      
        '             WHEN ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlg' +
        'LibParcial = 1))'
      '             THEN '#39'PARCIAL'#39
      '             ELSE Null'
      '       END) cAutFin'
      '  FROM PickList'
      
        '       INNER JOIN Pedido           ON Pedido.nCdPedido         =' +
        ' PickList.nCdPedido'
      
        '       INNER JOIN Usuario          ON Usuario.nCdUsuario       =' +
        ' PickList.nCdUsuarioGeracao'
      
        '       LEFT  JOIN Usuario UPI      ON UPI.nCdUsuario           =' +
        ' PickList.nCdUsuarioPrimImp'
      
        '       LEFT  JOIN Terceiro         ON Terceiro.nCdTerceiro     =' +
        ' Pedido.nCdTerceiro'
      
        '       LEFT  JOIN Terceiro TPag    ON TPag.nCdTerceiro         =' +
        ' Pedido.nCdTerceiroPagador'
      ' WHERE Pedido.nCdEmpresa = :nCdEmpresa'
      '   AND PickList.cFlgBaixado = 0'
      '   AND PickList.dDtCancel IS NULL'
      
        '   AND ((Pedido.nCdTabStatusPed = 3) OR (Pedido.nCdTabStatusPed ' +
        '= 4) OR ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlgLibParcial' +
        ' = 1)))')
    Left = 148
    Top = 245
    object qryEmSeparacaonCdPickList: TIntegerField
      DisplayLabel = 'Picking'
      FieldName = 'nCdPickList'
    end
    object qryEmSeparacaonCdPedido: TIntegerField
      DisplayLabel = 'Pedido'
      FieldName = 'nCdPedido'
    end
    object qryEmSeparacaodDtGeracao: TDateTimeField
      DisplayLabel = 'Data Gera'#231#227'o'
      FieldName = 'dDtGeracao'
    end
    object qryEmSeparacaocNmUsuarioGeracao: TStringField
      DisplayLabel = 'Usu'#225'rio Gerador'
      FieldName = 'cNmUsuarioGeracao'
    end
    object qryEmSeparacaodDtPrimImp: TDateTimeField
      DisplayLabel = 'Data 1a Impr.'
      FieldName = 'dDtPrimImp'
    end
    object qryEmSeparacaocNmUsuarioPrimImp: TStringField
      DisplayLabel = 'Usu'#225'rio 1a Impr.'
      FieldName = 'cNmUsuarioPrimImp'
    end
    object qryEmSeparacaodDtEntrega: TDateTimeField
      DisplayLabel = 'Data Entrega'
      FieldName = 'dDtEntrega'
      ReadOnly = True
    end
    object qryEmSeparacaocAtraso: TStringField
      DisplayLabel = 'Atraso?'
      FieldName = 'cAtraso'
      ReadOnly = True
      Size = 3
    end
    object qryEmSeparacaocNmTerceiro: TStringField
      DisplayLabel = 'Terceiro Entrega'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryEmSeparacaocNmTerceiroPagador: TStringField
      DisplayLabel = 'Terceiro Pagador'
      FieldName = 'cNmTerceiroPagador'
      Size = 50
    end
    object qryEmSeparacaocAutFin: TStringField
      DisplayLabel = 'Aut.Fin ?'
      FieldName = 'cAutFin'
      ReadOnly = True
      Size = 7
    end
  end
  object dsEmSeparacao: TDataSource
    DataSet = qryEmSeparacao
    Left = 152
    Top = 278
  end
  object qryAguardandoFat: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      'SET NOCOUNT ON'
      ''
      'SELECT nCdPedido'
      '      ,TipoPedido.cNmTipoPedido'
      '      ,dDtPedido'
      '      ,dDtPrevEntFim'
      '      ,(CASE WHEN (Pedido.dDtPrevEntFim < GetDate())'
      '             THEN '#39'SIM'#39
      '             ELSE Null'
      '       END) cAtraso'
      '      ,Terceiro.cNmTerceiro'
      '      ,TTransp.cNmTerceiro cNmTerceiroPagador'
      '      ,(CASE WHEN (Pedido.nCdTabStatusPed IN (3,4))'
      '             THEN '#39'SIM'#39
      
        '             WHEN ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlg' +
        'LibParcial = 1))'
      '             THEN '#39'PARCIAL'#39
      '             ELSE Null'
      '       END) cAutFin '
      '      ,Pedido.nCdEmpresa'
      '  FROM Pedido'
      
        '       LEFT  JOIN Terceiro         ON Terceiro.nCdTerceiro     =' +
        ' Pedido.nCdTerceiro'
      
        '       LEFT  JOIN Terceiro TTransp ON TTransp.nCdTerceiro      =' +
        ' Pedido.nCdTerceiroPagador'
      
        '       INNER JOIN TipoPedido       ON TipoPedido.nCdTipoPedido =' +
        ' Pedido.nCdTipoPedido'
      ' WHERE Pedido.nCdEmpresa = :nCdEmpresa'
      '   AND TipoPedido.cFlgAguardarMontagem = 1'
      
        '   AND ((Pedido.nCdTabStatusPed = 3) OR (Pedido.nCdTabStatusPed ' +
        '= 4) OR ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlgLibParcial' +
        ' = 1)))'
      '   AND EXISTS(SELECT TOP 1 1 '
      '                FROM ItemPedido '
      '               WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      '                 AND ItemPedido.nQtdeLibFat > 0)')
    Left = 184
    Top = 246
    object qryAguardandoFatnCdPedido: TIntegerField
      DisplayLabel = 'Pedido'
      FieldName = 'nCdPedido'
    end
    object qryAguardandoFatcNmTipoPedido: TStringField
      DisplayLabel = 'Tipo Pedido'
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryAguardandoFatdDtPedido: TDateTimeField
      DisplayLabel = 'Data Gera'#231#227'o'
      FieldName = 'dDtPedido'
    end
    object qryAguardandoFatdDtPrevEntFim: TDateTimeField
      DisplayLabel = 'Data Entrega'
      FieldName = 'dDtPrevEntFim'
    end
    object qryAguardandoFatcAtraso: TStringField
      DisplayLabel = 'Atraso ?'
      FieldName = 'cAtraso'
      ReadOnly = True
      Size = 3
    end
    object qryAguardandoFatcNmTerceiro: TStringField
      DisplayLabel = 'Terceiro Entrega'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryAguardandoFatcNmTerceiroPagador: TStringField
      DisplayLabel = 'Terceiro Pagador'
      FieldName = 'cNmTerceiroPagador'
      Size = 50
    end
    object qryAguardandoFatcAutFin: TStringField
      DisplayLabel = 'Aut.Fin ?'
      FieldName = 'cAutFin'
      ReadOnly = True
      Size = 7
    end
    object qryAguardandoFatnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
  end
  object dsAguardandoFat: TDataSource
    DataSet = qryAguardandoFat
    Left = 184
    Top = 278
  end
  object qryAguardSepDetail: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryAguardSepDetailAfterScroll
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      'SET NOCOUNT ON'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempAguardSepDetail'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempAguardSepDetail (nCdPedido          int'
      
        '                                      ,cNmTipoPedido      varcha' +
        'r(50)'
      
        '                                      ,dDtEntrega         dateti' +
        'me'
      
        '                                      ,cAtraso            varcha' +
        'r(3)'
      
        '                                      ,cNmTerceiroEntrega varcha' +
        'r(100)'
      
        '                                      ,cNmTerceiroPagador varcha' +
        'r(100)'
      
        '                                      ,cAutFin            varcha' +
        'r(7)'
      '                                      ,nCdProduto         int'
      
        '                                      ,cNmItem            varcha' +
        'r(150)'
      
        '                                      ,nQtdePed           decima' +
        'l(12,4)'
      '                                      ,nQtdeAberto        int'
      
        '                                      ,nPercCumprimento   decima' +
        'l(12,2))'
      ''
      'END'
      ''
      'TRUNCATE TABLE #TempAguardSepDetail'
      ''
      'INSERT INTO #TempAguardSepDetail (nCdPedido'
      '                                 ,cNmTipoPedido'
      '                                 ,dDtEntrega'
      '                                 ,cAtraso'
      '                                 ,cNmTerceiroEntrega'
      '                                 ,cNmTerceiroPagador'
      '                                 ,cAutFin'
      '                                 ,nCdProduto'
      '                                 ,cNmItem'
      '                                 ,nQtdePed'
      '                                 ,nQtdeAberto)'
      ''
      'SELECT Pedido.nCdPedido'
      '      ,TipoPedido.cNmTipoPedido'
      '      ,dbo.fn_OnlyDate(Pedido.dDtPrevEntFim) dDtPrevEntFim'
      '      ,(CASE WHEN (Pedido.dDtPrevEntFim < GetDate())'
      '             THEN '#39'SIM'#39
      '             ELSE Null'
      '       END) cAtraso'
      '      ,Terceiro.cNmTerceiro'
      '      ,TTransp.cNmTerceiro cNmTerceiroPagador'
      '      ,(CASE WHEN (Pedido.nCdTabStatusPed IN (3,4))'
      '             THEN '#39'SIM'#39
      
        '             WHEN ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlg' +
        'LibParcial = 1))'
      '             THEN '#39'PARCIAL'#39
      '             ELSE Null'
      '       END) cAutFin'
      '      ,ItemPedido.nCdProduto'
      '      ,ItemPedido.cNmItem'
      '      ,ItemPedido.nQtdePed'
      '      ,(SELECT ItemPedido.nQtdePed'
      '               - isNull((SELECT SUM(ItemPickList.nQtdePick)'
      '                           FROM ItemPickList'
      
        '                                INNER JOIN PickList ON PickList.' +
        'nCdPickList = ItemPickList.nCdPickList'
      
        '                          WHERE PickList.nCdPedido         = Ite' +
        'mPedido.nCdPedido'
      '                            AND PickList.dDtCancel IS NULL'
      
        '                            AND ItemPickList.nCdItemPedido = Ite' +
        'mPedido.nCdItemPedido'
      
        '                            AND PickList.cFlgBaixado       = 0),' +
        '0)'
      '               - ItemPedido.nQtdeLibFat'
      '               - ItemPedido.nQtdeExpRec'
      '               - ItemPedido.nQtdeCanc) nQtdeAberto'
      '  FROM Pedido'
      
        '       INNER JOIN TipoPedido       ON TipoPedido.nCdTipoPedido =' +
        ' Pedido.nCdTipoPedido'
      
        '       INNER JOIN ItemPedido       ON ItemPedido.nCdPedido     =' +
        ' Pedido.nCdPedido'
      
        '       LEFT  JOIN Terceiro         ON Terceiro.nCdTerceiro     =' +
        ' Pedido.nCdTerceiro'
      
        '       LEFT  JOIN Terceiro TTransp ON TTransp.nCdTerceiro      =' +
        ' Pedido.nCdTerceiroPagador'
      ' WHERE Pedido.nCdEmpresa = :nCdEmpresa'
      '   AND TipoPedido.cFlgAguardarMontagem = 1'
      
        '   AND ((Pedido.nCdTabStatusPed = 3) OR (Pedido.nCdTabStatusPed ' +
        '= 4) OR ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlgLibParcial' +
        ' = 1)))'
      '   AND ItemPedido.nCdTipoItemPed      IN (6,1,2,5)'
      '   AND NOT EXISTS(SELECT TOP 1 1'
      '                    FROM OrdemProducao OP'
      '                   WHERE OP.nCdPedido = Pedido.nCdPedido'
      '                     AND OP.nCdProduto = ItemPedido.nCdProduto)'
      '   AND isNull((SELECT ItemPedido.nQtdePed'
      
        '                      - isNull((SELECT SUM(ItemPickList.nQtdePic' +
        'k)'
      '                                  FROM ItemPickList'
      
        '                                       INNER JOIN PickList ON Pi' +
        'ckList.nCdPickList = ItemPickList.nCdPickList'
      
        '                                 WHERE PickList.nCdPedido       ' +
        '  = ItemPedido.nCdPedido'
      
        '                                   AND PickList.dDtCancel IS NUL' +
        'L'
      
        '                                   AND ItemPickList.nCdItemPedid' +
        'o = ItemPedido.nCdItemPedido'
      
        '                                   AND PickList.cFlgBaixado = 0)' +
        ',0)'
      '                      - ItemPedido.nQtdeLibFat'
      '                      - ItemPedido.nQtdeExpRec'
      '                      - ItemPedido.nQtdeCanc),0) > 0'
      ''
      'UPDATE #TempAguardSepDetail'
      '   SET nPercCumprimento = (CASE WHEN (nQtdePed > 0)'
      
        '                                THEN CONVERT (decimal(12,2),((1 ' +
        '- (nQtdeAberto/ nQtdePed)) * 100))'
      '                                ELSE 0'
      '                           END)'
      ''
      'SELECT *'
      '  FROM #TempAguardSepDetail'
      ' WHERE nQtdeAberto > 0')
    Left = 220
    Top = 245
    object qryAguardSepDetailnCdPedido: TIntegerField
      DisplayLabel = 'Pedido'
      FieldName = 'nCdPedido'
    end
    object qryAguardSepDetailcNmTipoPedido: TStringField
      DisplayLabel = 'Tipo Pedido'
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryAguardSepDetaildDtEntrega: TDateTimeField
      DisplayLabel = 'Data Entrega'
      FieldName = 'dDtEntrega'
    end
    object qryAguardSepDetailcAtraso: TStringField
      DisplayLabel = 'Atraso?'
      FieldName = 'cAtraso'
      Size = 3
    end
    object qryAguardSepDetailcNmTerceiroEntrega: TStringField
      DisplayLabel = 'Terceiro Entrega'
      FieldName = 'cNmTerceiroEntrega'
      Size = 50
    end
    object qryAguardSepDetailcNmTerceiroPagador: TStringField
      DisplayLabel = 'Terceiro Pagador'
      FieldName = 'cNmTerceiroPagador'
      Size = 50
    end
    object qryAguardSepDetailcAutFin: TStringField
      DisplayLabel = 'Aut. Fin ?'
      FieldName = 'cAutFin'
      Size = 7
    end
    object qryAguardSepDetailnCdProduto: TIntegerField
      DisplayLabel = 'C'#243'd. Produto'
      FieldName = 'nCdProduto'
    end
    object qryAguardSepDetailcNmItem: TStringField
      DisplayLabel = 'Descri'#231#227'o Produto'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryAguardSepDetailnQtdePed: TBCDField
      DisplayLabel = 'Qt. Pedido'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryAguardSepDetailnQtdeAberto: TIntegerField
      DisplayLabel = 'Qt. Aberta'
      FieldName = 'nQtdeAberto'
    end
    object qryAguardSepDetailnPercCumprimento: TBCDField
      DisplayLabel = '% Cumprimento'
      FieldName = 'nPercCumprimento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsAguardSepDetail: TDataSource
    DataSet = qryAguardSepDetail
    Left = 220
    Top = 277
  end
  object qryEmSepDetail: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryEmSepDetailAfterScroll
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      'SET NOCOUNT ON'
      ''
      'SELECT PickList.nCdPickList'
      '      ,PickList.nCdPedido'
      '      ,PickList.dDtGeracao'
      '      ,Usuario.cNmLogin cNmUsuarioGeracao'
      '      ,dbo.fn_OnlyDate(Pedido.dDtPrevEntFim) dDtEntrega'
      '      ,(CASE WHEN (Pedido.dDtPrevEntFim < GetDate())'
      '             THEN '#39'SIM'#39
      '             ELSE Null'
      '       END) cAtraso'
      '      ,Terceiro.cNmTerceiro'
      '      ,TPag.cNmTerceiro cNmTerceiroPagador'
      '      ,(CASE WHEN (Pedido.nCdTabStatusPed IN (3,4))'
      '             THEN '#39'SIM'#39
      
        '             WHEN ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlg' +
        'LibParcial = 1))'
      '             THEN '#39'PARCIAL'#39
      '             ELSE Null'
      '       END) cAutFin'
      '      ,ItemPedido.nCdProduto'
      '      ,ItemPedido.cNmItem'
      '      ,ItemPickList.nQtdePick'
      '  FROM PickList'
      
        '       INNER JOIN Pedido           ON Pedido.nCdPedido         =' +
        ' PickList.nCdPedido'
      
        '       INNER JOIN Usuario          ON Usuario.nCdUsuario       =' +
        ' PickList.nCdUsuarioGeracao'
      
        '       INNER JOIN ItemPickList     ON ItemPickList.nCdPickList =' +
        ' PickList.nCdPickList'
      
        '       INNER JOIN ItemPedido       ON ItemPedido.nCdItemPedido =' +
        ' ItemPickList.nCdItemPedido'
      
        '       LEFT  JOIN Terceiro         ON Terceiro.nCdTerceiro     =' +
        ' Pedido.nCdTerceiro'
      
        '       LEFT  JOIN Terceiro TPag    ON TPag.nCdTerceiro         =' +
        ' Pedido.nCdTerceiroPagador'
      ' WHERE Pedido.nCdEmpresa            = :nCdEmpresa'
      '   AND ItemPedido.nCdTipoItemPed   IN (6,1,2,5)'
      '   AND PickList.cFlgBaixado         = 0'
      '   AND PickList.dDtCancel          IS NULL'
      
        '   AND ((Pedido.nCdTabStatusPed = 3) OR (Pedido.nCdTabStatusPed ' +
        '= 4) OR ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlgLibParcial' +
        ' = 1)))'
      '')
    Left = 252
    Top = 245
    object qryEmSepDetailnCdPickList: TIntegerField
      DisplayLabel = 'Picking'
      FieldName = 'nCdPickList'
    end
    object qryEmSepDetailnCdPedido: TIntegerField
      DisplayLabel = 'Pedido'
      FieldName = 'nCdPedido'
    end
    object qryEmSepDetaildDtGeracao: TDateTimeField
      DisplayLabel = 'Data Gera'#231#227'o'
      FieldName = 'dDtGeracao'
    end
    object qryEmSepDetailcNmUsuarioGeracao: TStringField
      DisplayLabel = 'Usu'#225'rio Gerador'
      FieldName = 'cNmUsuarioGeracao'
    end
    object qryEmSepDetaildDtEntrega: TDateTimeField
      DisplayLabel = 'Data Entrega'
      FieldName = 'dDtEntrega'
      ReadOnly = True
    end
    object qryEmSepDetailcAtraso: TStringField
      DisplayLabel = 'Atraso?'
      FieldName = 'cAtraso'
      ReadOnly = True
      Size = 3
    end
    object qryEmSepDetailcNmTerceiro: TStringField
      DisplayLabel = 'Terceiro Entrega'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryEmSepDetailcNmTerceiroPagador: TStringField
      DisplayLabel = 'Terceiro Pagador'
      FieldName = 'cNmTerceiroPagador'
      Size = 50
    end
    object qryEmSepDetailcAutFin: TStringField
      DisplayLabel = 'Aut. Fin ?'
      FieldName = 'cAutFin'
      ReadOnly = True
      Size = 7
    end
    object qryEmSepDetailnCdProduto: TIntegerField
      DisplayLabel = 'C'#243'd. Produto'
      FieldName = 'nCdProduto'
    end
    object qryEmSepDetailcNmItem: TStringField
      DisplayLabel = 'Descri'#231#227'o Produto'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryEmSepDetailnQtdePick: TBCDField
      DisplayLabel = 'Qt. Picking'
      FieldName = 'nQtdePick'
      Precision = 14
      Size = 6
    end
  end
  object dsEmSepDetail: TDataSource
    DataSet = qryEmSepDetail
    Left = 252
    Top = 277
  end
  object qryAguardFatDetail: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryAguardFatDetailAfterScroll
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      'SET NOCOUNT ON'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempAguardFatDetail'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempAguardFatDetail (nCdPedido          int'
      
        '                                      ,cNmTipoPedido      varcha' +
        'r(50)'
      
        '                                      ,dDtEntrega         dateti' +
        'me'
      
        '                                      ,cAtraso            varcha' +
        'r(3)'
      
        '                                      ,cNmTerceiroEntrega varcha' +
        'r(100)'
      
        '                                      ,cNmTerceiroPagador varcha' +
        'r(100)'
      
        '                                      ,cAutFin            varcha' +
        'r(7)'
      '                                      ,nCdProduto         int'
      
        '                                      ,cNmProduto         varcha' +
        'r(150)'
      
        '                                      ,nQtdePed           decima' +
        'l(12,4)'
      
        '                                      ,nQtdeLibFat        decima' +
        'l(12,4)'
      
        '                                      ,nPercCumprimento   decima' +
        'l(12,2))'
      ''
      'END'
      ''
      'TRUNCATE TABLE #TempAguardFatDetail'
      ''
      'INSERT INTO #TempAguardFatDetail (nCdPedido'
      '                                 ,cNmTipoPedido'
      '                                 ,dDtEntrega'
      '                                 ,cAtraso'
      '                                 ,cNmTerceiroEntrega'
      '                                 ,cNmTerceiroPagador'
      '                                 ,cAutFin'
      '                                 ,nCdProduto'
      '                                 ,cNmProduto'
      '                                 ,nQtdePed'
      '                                 ,nQtdeLibFat)'
      ''
      'SELECT Pedido.nCdPedido'
      '      ,TipoPedido.cNmTipoPedido'
      '      ,Pedido.dDtPrevEntFim'
      '      ,(CASE WHEN (Pedido.dDtPrevEntFim < GetDate())'
      '             THEN '#39'SIM'#39
      '             ELSE Null'
      '       END) cAtraso'
      '      ,Terceiro.cNmTerceiro'
      '      ,TTransp.cNmTerceiro cNmTerceiroPagador'
      '      ,(CASE WHEN (Pedido.nCdTabStatusPed IN (3,4))'
      '             THEN '#39'SIM'#39
      
        '             WHEN ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlg' +
        'LibParcial = 1))'
      '             THEN '#39'PARCIAL'#39
      '             ELSE Null'
      '       END) cAutFin '
      '      ,ItemPedido.nCdProduto'
      '      ,ItemPedido.cNmItem'
      '      ,ItemPedido.nQtdePed'
      '      ,ItemPedido.nQtdeLibFat'
      '  FROM Pedido'
      
        '       LEFT  JOIN Terceiro         ON Terceiro.nCdTerceiro     =' +
        ' Pedido.nCdTerceiro'
      
        '       LEFT  JOIN Terceiro TTransp ON TTransp.nCdTerceiro      =' +
        ' Pedido.nCdTerceiroPagador'
      
        '       INNER JOIN ItemPedido       ON ItemPedido.nCdPedido     =' +
        ' Pedido.nCdPedido'
      
        '       INNER JOIN TipoPedido       ON TipoPedido.nCdTipoPedido =' +
        ' Pedido.nCdTipoPedido'
      ' WHERE Pedido.nCdEmpresa               = :nCdEmpresa'
      '   AND TipoPedido.cFlgAguardarMontagem = 1'
      '   AND ItemPedido.nCdTipoItemPed      IN (6,1,2,5)'
      
        '   AND ((Pedido.nCdTabStatusPed = 3) OR (Pedido.nCdTabStatusPed ' +
        '= 4) OR ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlgLibParcial' +
        ' = 1)))'
      '   AND EXISTS(SELECT TOP 1 1 '
      '                FROM ItemPedido '
      '               WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      '                 AND ItemPedido.nQtdeLibFat > 0)'
      ''
      'UPDATE #TempAguardFatDetail'
      '   SET nPercCumprimento = (CASE WHEN (nQtdePed > 0)'
      
        '                                THEN CONVERT (decimal(12,2),((nQ' +
        'tdeLibFat/ nQtdePed) * 100)) '
      '                                ELSE 0'
      '                           END)'
      ''
      'SELECT * '
      '  FROM #TempAguardFatDetail'
      ' WHERE nQtdeLibFat > 0')
    Left = 284
    Top = 245
    object qryAguardFatDetailnCdPedido: TIntegerField
      DisplayLabel = 'Pedido'
      FieldName = 'nCdPedido'
    end
    object qryAguardFatDetailcNmTipoPedido: TStringField
      DisplayLabel = 'Tipo Pedido'
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryAguardFatDetaildDtEntrega: TDateTimeField
      DisplayLabel = 'Data Entrega'
      FieldName = 'dDtEntrega'
    end
    object qryAguardFatDetailcAtraso: TStringField
      DisplayLabel = 'Atraso?'
      FieldName = 'cAtraso'
      Size = 3
    end
    object qryAguardFatDetailcNmTerceiroEntrega: TStringField
      DisplayLabel = 'Terceiro Entrega'
      FieldName = 'cNmTerceiroEntrega'
      Size = 50
    end
    object qryAguardFatDetailcNmTerceiroPagador: TStringField
      DisplayLabel = 'Terceiro Pagador'
      FieldName = 'cNmTerceiroPagador'
      Size = 50
    end
    object qryAguardFatDetailcAutFin: TStringField
      DisplayLabel = 'Aut. Fin ?'
      FieldName = 'cAutFin'
      Size = 7
    end
    object qryAguardFatDetailnCdProduto: TIntegerField
      DisplayLabel = 'C'#243'd. Produto'
      FieldName = 'nCdProduto'
    end
    object qryAguardFatDetailcNmProduto: TStringField
      DisplayLabel = 'Descri'#231#227'o Produto'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryAguardFatDetailnQtdePed: TBCDField
      DisplayLabel = 'Qt. Pedido'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryAguardFatDetailnQtdeLibFat: TBCDField
      DisplayLabel = 'Qt. Lib. Fat.'
      FieldName = 'nQtdeLibFat'
      Precision = 12
    end
    object qryAguardFatDetailnPercCumprimento: TBCDField
      DisplayLabel = '% Cumprimento'
      FieldName = 'nPercCumprimento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsAguardFatDetail: TDataSource
    DataSet = qryAguardFatDetail
    Left = 284
    Top = 277
  end
  object qryCancelaPicking: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdPickList'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE PickList'
      '   SET nCdUsuarioCancel = :nCdUsuario'
      '      ,dDtCancel        = GetDate()'
      ' WHERE nCdPickList = :nCdPickList')
    Left = 320
    Top = 246
  end
  object popAguardProducao: TPopupMenu
    Images = ImageList1
    Left = 148
    Top = 405
    object Atualizar1: TMenuItem
      Caption = '&Atualizar'
      ImageIndex = 1
      OnClick = Atualizar1Click
    end
  end
  object popAguardSeparacaoResumo: TPopupMenu
    Images = ImageList1
    Left = 148
    Top = 437
    object Atualizar2: TMenuItem
      Caption = '&Atualizar'
      ImageIndex = 1
      OnClick = Atualizar2Click
    end
  end
  object popEmSeparacao: TPopupMenu
    Images = ImageList1
    Left = 148
    Top = 469
    object Atualizar3: TMenuItem
      Caption = '&Atualizar'
      ImageIndex = 1
      OnClick = Atualizar3Click
    end
  end
  object popAguardFaturamento: TPopupMenu
    Images = ImageList1
    Left = 148
    Top = 501
    object Atualizar4: TMenuItem
      Caption = '&Atualizar'
      ImageIndex = 1
      OnClick = Atualizar4Click
    end
  end
  object popProduzidoParcial: TPopupMenu
    Images = ImageList1
    Left = 184
    Top = 502
    object Atualizar5: TMenuItem
      Caption = 'Atualizar'
      ImageIndex = 1
      OnClick = Atualizar5Click
    end
    object N1: TMenuItem
      Caption = '-'
      Enabled = False
    end
    object GerarPickList1: TMenuItem
      Caption = 'Gerar Picking'
      ImageIndex = 2
      OnClick = GerarPickList1Click
    end
    object AutorizarFaturamento1: TMenuItem
      Caption = 'Autorizar Faturamento'
      ImageIndex = 3
      OnClick = AutorizarFaturamento1Click
    end
  end
  object qryProdParcial: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryProdParcialAfterScroll
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempProdParcial'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempProdParcial (nCdPedido          int'
      '                                  ,nCdOrdemProducao   int'
      
        '                                  ,cNumeroOP          varchar(20' +
        ')'
      
        '                                  ,cNmTipoPedido      varchar(50' +
        ')'
      '                                  ,dDtEntrega         datetime'
      '                                  ,cAtraso            varchar(3)'
      
        '                                  ,cNmTerceiro        varchar(10' +
        '0)'
      
        '                                  ,cNmTerceiroPagador varchar(10' +
        '0)'
      '                                  ,cAutFin            varchar(7)'
      '                                  ,nCdProduto         int'
      
        '                                  ,cNmProduto         varchar(15' +
        '0)'
      
        '                                  ,nQtdePed           decimal(12' +
        ',4)'
      
        '                                  ,nQtdeProdParcial   decimal(12' +
        ',4) default 0 not null)'
      'END'
      ''
      'TRUNCATE TABLE #TempProdParcial'
      ''
      ''
      'INSERT INTO #TempProdParcial (nCdPedido'
      '                                ,nCdOrdemProducao'
      '                                ,cNumeroOP'
      '                                ,cNmTipoPedido'
      '                                ,dDtEntrega'
      '                                ,cAtraso'
      '                                ,cNmTerceiro'
      '                                ,cNmTerceiroPagador'
      '                                ,cAutFin'
      '                                ,nCdProduto'
      '                                ,cNmProduto'
      '                                ,nQtdePed'
      '                                ,nQtdeProdParcial)'
      ''
      'SELECT Pedido.nCdPedido'
      '      ,OP.nCdOrdemProducao'
      '      ,cNumeroOP'
      '      ,TP.cNmTipoPedido'
      '      ,Pedido.dDtPrevEntFim'
      '      ,(CASE WHEN (Pedido.dDtPrevEntFim < GetDate())'
      '             THEN '#39'SIM'#39
      '             ELSE Null'
      '       END) cAtraso'
      '      ,Terceiro.cNmTerceiro'
      '      ,TPag.cNmTerceiro'
      '      ,(CASE WHEN (Pedido.nCdTabStatusPed IN (3,4))'
      '             THEN '#39'SIM'#39
      
        '             WHEN ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlg' +
        'LibParcial = 1))'
      '             THEN '#39'PARCIAL'#39
      '             ELSE Null'
      '       END) cAutFin'
      '      ,ItemPedido.nCdProduto'
      '      ,Produto.cNmProduto'
      '      ,ItemPedido.nQtdePed'
      '      ,(SELECT OP.nQtdeConcluida'
      '               - isNull((SELECT SUM(ItemPickList.nQtdePick)'
      '                           FROM ItemPickList'
      
        '                                INNER JOIN PickList ON PickList.' +
        'nCdPickList = ItemPickList.nCdPickList'
      
        '                          WHERE PickList.nCdPedido         = Ite' +
        'mPedido.nCdPedido'
      '                            AND PickList.dDtCancel IS NULL'
      
        '                            AND ItemPickList.nCdItemPedido = Ite' +
        'mPedido.nCdItemPedido'
      
        '                            AND PickList.cFlgBaixado       = 0),' +
        '0) -- A quantidade em Picking'
      '               - ItemPedido.nQtdeLibFat'
      '               - ItemPedido.nQtdeExpRec'
      '               - ItemPedido.nQtdeCanc)'
      '  FROM ItemPedido'
      
        '       INNER JOIN Pedido           ON Pedido.nCdPedido     = Ite' +
        'mPedido.nCdPedido'
      
        '       INNER JOIN TipoPedido    TP ON TP.nCdTipoPedido     = Ped' +
        'ido.nCdTipoPedido'
      
        '       INNER JOIN Produto          ON Produto.nCdProduto   = Ite' +
        'mPedido.nCdProduto'
      
        '       LEFT  JOIN OrdemProducao OP ON OP.nCdPedido         = Ite' +
        'mPedido.nCdPedido'
      
        '       LEFT  JOIN Terceiro         ON Terceiro.nCdTerceiro = Ped' +
        'ido.nCdTerceiro'
      
        '       LEFT  JOIN Terceiro    TPag ON TPag.nCdTerceiro     = Ped' +
        'ido.nCdTerceiroPagador'
      ' WHERE Pedido.nCdEmpresa       = :nCdEmpresa'
      
        '   AND ((Pedido.nCdTabStatusPed = 3) OR (Pedido.nCdTabStatusPed ' +
        '= 4) OR ((Pedido.nCdTabStatusPed = 2) AND (Pedido.cFlgLibParcial' +
        ' = 1)))'
      '   AND OP.nCdTabTipoStatusOP  <> 6'
      '   AND OP.nCdPedido            = ItemPedido.nCdPedido'
      '   AND OP.nCdProduto           = ItemPedido.nCdProduto'
      ''
      'SELECT * '
      '  FROM #TempProdParcial'
      ' WHERE nQtdeProdParcial > 0')
    Left = 48
    Top = 246
    object qryProdParcialnCdPedido: TIntegerField
      DisplayLabel = 'Pedido'
      FieldName = 'nCdPedido'
    end
    object qryProdParcialnCdOrdemProducao: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object qryProdParcialcNumeroOP: TStringField
      DisplayLabel = 'N'#250'm. OP'
      FieldName = 'cNumeroOP'
    end
    object qryProdParcialcNmTipoPedido: TStringField
      DisplayLabel = 'Tipo Pedido'
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryProdParcialdDtEntrega: TDateTimeField
      DisplayLabel = 'Data Entrega'
      FieldName = 'dDtEntrega'
    end
    object qryProdParcialcAtraso: TStringField
      DisplayLabel = 'Atraso?'
      FieldName = 'cAtraso'
      Size = 3
    end
    object qryProdParcialcNmTerceiro: TStringField
      DisplayLabel = 'Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryProdParcialcNmTerceiroPagador: TStringField
      DisplayLabel = 'Terceiro Pagador'
      FieldName = 'cNmTerceiroPagador'
      Size = 50
    end
    object qryProdParcialcAutFin: TStringField
      DisplayLabel = 'Aut. Fin ?'
      FieldName = 'cAutFin'
      Size = 7
    end
    object qryProdParcialnCdProduto: TIntegerField
      DisplayLabel = 'C'#243'd. Produto'
      FieldName = 'nCdProduto'
    end
    object qryProdParcialcNmProduto: TStringField
      DisplayLabel = 'Descri'#231#227'o Produto'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdParcialnQtdePed: TBCDField
      DisplayLabel = 'Qt. Pedido'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryProdParcialnQtdeProdParcial: TBCDField
      DisplayLabel = 'Qt. Produzida'
      FieldName = 'nQtdeProdParcial'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
  end
  object dsProdParcial: TDataSource
    DataSet = qryProdParcial
    Left = 48
    Top = 278
  end
  object popAguardSeparacaoAnalitico: TPopupMenu
    Images = ImageList1
    Left = 188
    Top = 437
    object MenuItem1: TMenuItem
      Caption = '&Atualizar'
      ImageIndex = 1
      OnClick = MenuItem1Click
    end
  end
end
