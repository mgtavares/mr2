inherited frmPlanoConta: TfrmPlanoConta
  Caption = 'Plano de Contas'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar2: TToolBar
    inherited btIncluir: TToolButton
      Enabled = False
    end
    inherited ToolButton1: TToolButton
      Enabled = False
    end
    inherited btConsultar: TToolButton
      Enabled = False
    end
    inherited btnAuditoria: TToolButton
      Enabled = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 8
    Top = 48
    Width = 857
    Height = 577
    DataSource = dsConta
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    OnColExit = DBGridEh1ColExit
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdPlanoConta'
        Footers = <>
        Width = 44
      end
      item
        EditButtons = <>
        FieldName = 'cMascara'
        Footers = <>
        Width = 106
      end
      item
        EditButtons = <>
        FieldName = 'cSenso'
        Footers = <>
        KeyList.Strings = (
          'C'
          'D')
        PickList.Strings = (
          'Cr'#233'dito'
          'D'#233'bito')
        Width = 76
      end
      item
        EditButtons = <>
        FieldName = 'cNmPlanoConta'
        Footers = <>
        Width = 504
      end
      item
        EditButtons = <>
        FieldName = 'nCdGrupoPlanoConta'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cFlgRecDes'
        Footers = <>
        Visible = False
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'cFlgLanc'
        Footers = <>
        KeyList.Strings = (
          '1'
          '0')
        Width = 61
      end>
  end
  object StaticText1: TStaticText [3]
    Left = 8
    Top = 32
    Width = 857
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Rela'#231#227'o de Contas'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM GrupoPlanoConta'
      ' WHERE nCdGrupoPlanoConta = :nPK')
    Left = 1000
    Top = 64
    object qryMasternCdGrupoPlanoConta: TIntegerField
      FieldName = 'nCdGrupoPlanoConta'
    end
    object qryMastercNmGrupoPlanoConta: TStringField
      FieldName = 'cNmGrupoPlanoConta'
      Size = 50
    end
    object qryMastercFlgExibeDRE: TIntegerField
      FieldName = 'cFlgExibeDRE'
    end
    object qryMastercFlgRecDes: TStringField
      FieldName = 'cFlgRecDes'
      FixedChar = True
      Size = 1
    end
  end
  inherited dsMaster: TDataSource
    Left = 1048
    Top = 64
  end
  inherited qryID: TADOQuery
    Left = 1096
    Top = 64
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 1104
    Top = 120
  end
  inherited qryStat: TADOQuery
    Left = 1008
    Top = 136
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 1072
    Top = 176
  end
  inherited ImageList1: TImageList
    Left = 1208
    Top = 160
  end
  object qryContas: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryContasBeforePost
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM PlanoConta'
      'ORDER BY cMascara')
    Left = 1208
    Top = 56
    object qryContasnCdPlanoConta: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdPlanoConta'
      ReadOnly = True
    end
    object qryContascNmPlanoConta: TStringField
      DisplayLabel = 'Nome Conta'
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
    object qryContasnCdGrupoPlanoConta: TIntegerField
      FieldName = 'nCdGrupoPlanoConta'
    end
    object qryContascFlgRecDes: TStringField
      FieldName = 'cFlgRecDes'
      FixedChar = True
      Size = 1
    end
    object qryContascMascara: TStringField
      DisplayLabel = 'M'#225'scara'
      DisplayWidth = 8
      FieldName = 'cMascara'
      EditMask = '!9.99.999;1; '
      FixedChar = True
      Size = 8
    end
    object qryContascFlgLanc: TIntegerField
      DisplayLabel = 'Anal'#237'tica'
      FieldName = 'cFlgLanc'
    end
    object qryContascSenso: TStringField
      DisplayLabel = 'Senso'
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
  end
  object dsConta: TDataSource
    DataSet = qryContas
    Left = 1168
    Top = 216
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 944
    Top = 288
  end
end
