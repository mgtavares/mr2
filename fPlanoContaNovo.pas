unit fPlanoContaNovo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmPlanoContaNovo = class(TfrmCadastro_Padrao)
    qryGrupoPlanoConta: TADOQuery;
    qryGrupoPlanoContanCdGrupoPlanoConta: TIntegerField;
    qryGrupoPlanoContacNmGrupoPlanoConta: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DataSource1: TDataSource;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryMasternCdPlanoConta: TIntegerField;
    qryMastercNmPlanoConta: TStringField;
    qryMasternCdGrupoPlanoConta: TIntegerField;
    qryMastercFlgRecDes: TStringField;
    qryMastercMascara: TStringField;
    qryMastercFlgLanc: TIntegerField;
    qryMastercSenso: TStringField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMastercFlgExibeDre: TIntegerField;
    Label4: TLabel;
    DBEdit6: TDBEdit;
    qryGrupoPlanoContacMascaraGrupo: TStringField;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    qryMastercFlgTransfRecurso: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPlanoContaNovo: TfrmPlanoContaNovo;

implementation

uses fLookup_Padrao, fArvorePlanoConta;

{$R *.dfm}

procedure TfrmPlanoContaNovo.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PLANOCONTA' ;
  nCdTabelaSistema  := 63 ;
  nCdConsultaPadrao := 41 ;

end;

procedure TfrmPlanoContaNovo.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit3.SetFocus ;
end;

procedure TfrmPlanoContaNovo.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryGrupoPlanoConta.Close ;
  PosicionaQuery(qryGrupoPlanoConta, Dbedit3.Text) ;
  
end;

procedure TfrmPlanoContaNovo.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(40);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoPlanoConta.Value := nPK ;
                PosicionaQuery(qryGrupoPlanoConta, qryMasternCdGrupoPlanoConta.AsString) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPlanoContaNovo.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a Descri��o da Conta.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (DbEdit4.Text = '') then
  begin
      MensagemAlerta('Informe o Grupo de Contas.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;

  if (DbEdit5.Text <> 'C') and (DbEdit5.Text <> 'D') then
  begin
      MensagemAlerta('Informe o Senso da Conta.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  if (Trim(DBEdit6.Text) = '') then begin
      MensagemAlerta('Informe a M�scara da conta');
      DBEdit6.SetFocus;
      Abort;
  end;

  if (DbEdit5.Text = 'C') then qryMastercFlgRecDes.Value := 'R' ;
  if (DbEdit5.Text = 'D') then qryMastercFlgRecDes.Value := 'D' ;

  inherited;

end;

procedure TfrmPlanoContaNovo.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryGrupoPlanoConta, qryMasternCdGrupoPlanoConta.AsString) ;

end;

procedure TfrmPlanoContaNovo.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryGrupoPlanoConta.Close ;

end;

procedure TfrmPlanoContaNovo.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryGrupoPlanoConta.Close ;

end;

procedure TfrmPlanoContaNovo.ToolButton10Click(Sender: TObject);
var
  objForm : TfrmArvorePlanoConta ;
begin
  inherited;

  objForm := TfrmArvorePlanoConta.Create(Self) ;

  showForm( objForm , TRUE ) ;
    
end;

initialization
    RegisterClass(TfrmPlanoContaNovo) ;
    
end.
