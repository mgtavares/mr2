inherited frmGrupoProduto: TfrmGrupoProduto
  Caption = 'Grupo Produto'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 63
    Top = 46
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 16
    Top = 70
    Width = 85
    Height = 13
    Caption = 'Descri'#231#227'o Grupo'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 104
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdGrupoProduto'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 104
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmGrupoProduto'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBCheckBox1: TDBCheckBox [6]
    Left = 104
    Top = 88
    Width = 145
    Height = 17
    Caption = 'Exportar para PDV'
    DataField = 'cFlgExpPDV'
    DataSource = dsMaster
    TabOrder = 3
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object cxPageControl1: TcxPageControl [7]
    Left = 16
    Top = 128
    Width = 737
    Height = 241
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 4
    ClientRectBottom = 237
    ClientRectLeft = 4
    ClientRectRight = 733
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Dados Gerais'
      ImageIndex = 0
      object Image2: TImage
        Left = 0
        Top = 0
        Width = 729
        Height = 213
        Align = alClient
        Picture.Data = {
          07544269746D6170A6290000424DA62900000000000036000000280000005A00
          0000270000000100180000000000702900000000000000000000000000000000
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000}
        Stretch = True
      end
      object Label3: TLabel
        Left = 8
        Top = 14
        Width = 130
        Height = 13
        Alignment = taRightJustify
        Caption = 'M'#233'todo de Planejamento'
        FocusControl = DBEdit2
      end
      object Label4: TLabel
        Left = 27
        Top = 38
        Width = 111
        Height = 13
        Alignment = taRightJustify
        Caption = 'M'#233'todo de Obten'#231#227'o'
        FocusControl = DBEdit2
      end
      object Label5: TLabel
        Left = 34
        Top = 62
        Width = 104
        Height = 13
        Alignment = taRightJustify
        Caption = 'Grupo de Invent'#225'rio'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 63
        Top = 86
        Width = 75
        Height = 13
        Alignment = taRightJustify
        Caption = 'Grupo de MRP'
        FocusControl = DBEdit2
      end
      object Label7: TLabel
        Left = 216
        Top = 110
        Width = 22
        Height = 13
        Caption = 'Dias'
        FocusControl = DBEdit7
      end
      object Label8: TLabel
        Left = 74
        Top = 134
        Width = 64
        Height = 13
        Alignment = taRightJustify
        Caption = 'Lote M'#237'nimo'
        FocusControl = DBEdit8
      end
      object Label9: TLabel
        Left = 53
        Top = 158
        Width = 85
        Height = 13
        Alignment = taRightJustify
        Caption = 'M'#250'ltiplo de Lote'
        FocusControl = DBEdit9
      end
      object Label10: TLabel
        Left = 88
        Top = 110
        Width = 50
        Height = 13
        Alignment = taRightJustify
        Caption = 'Lead Time'
        FocusControl = DBEdit7
      end
      object edtPlanejamento: TER2LookupDBEdit
        Left = 144
        Top = 8
        Width = 65
        Height = 19
        DataField = 'nCdTipoPlanejamentoGP'
        DataSource = dsMaster
        TabOrder = 0
        CodigoLookup = 213
        QueryLookup = qryTipoPlanejamento
      end
      object edtObtencao: TER2LookupDBEdit
        Left = 144
        Top = 32
        Width = 65
        Height = 19
        DataField = 'nCdTipoObtencaoGP'
        DataSource = dsMaster
        TabOrder = 1
        CodigoLookup = 214
        QueryLookup = qryTipoObtencao
      end
      object edtGrupoInventario: TER2LookupDBEdit
        Left = 144
        Top = 56
        Width = 65
        Height = 19
        DataField = 'nCdGrupoInventarioGP'
        DataSource = dsMaster
        TabOrder = 2
        CodigoLookup = 153
        QueryLookup = qryGrupoInventario
      end
      object edtGrupoMRP: TER2LookupDBEdit
        Left = 144
        Top = 80
        Width = 65
        Height = 19
        DataField = 'nCdGrupoMRPGP'
        DataSource = dsMaster
        TabOrder = 3
        CodigoLookup = 174
        QueryLookup = qryGrupoMRP
      end
      object DBEdit3: TDBEdit
        Tag = 1
        Left = 216
        Top = 8
        Width = 321
        Height = 19
        DataField = 'cNmTipoPlanejamento'
        DataSource = DataSource1
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Tag = 1
        Left = 216
        Top = 32
        Width = 321
        Height = 19
        DataField = 'cNmTipoObtencao'
        DataSource = DataSource2
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Tag = 1
        Left = 216
        Top = 56
        Width = 321
        Height = 19
        DataField = 'cNmGrupoInventario'
        DataSource = DataSource3
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Tag = 1
        Left = 216
        Top = 80
        Width = 321
        Height = 19
        DataField = 'cNmGrupoMRP'
        DataSource = DataSource4
        TabOrder = 7
      end
      object DBEdit7: TDBEdit
        Left = 144
        Top = 104
        Width = 65
        Height = 19
        DataField = 'iLeadTimeGP'
        DataSource = dsMaster
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 144
        Top = 128
        Width = 65
        Height = 19
        DataField = 'nLoteMinimoGP'
        DataSource = dsMaster
        TabOrder = 9
      end
      object DBEdit9: TDBEdit
        Left = 144
        Top = 152
        Width = 65
        Height = 19
        DataField = 'nLoteMultiploGP'
        DataSource = dsMaster
        TabOrder = 10
      end
      object DBCheckBox2: TDBCheckBox
        Left = 144
        Top = 181
        Width = 153
        Height = 17
        Caption = 'Alertar Ciclo Invent'#225'rio'
        DataField = 'cFlgAlertarInventarioGP'
        DataSource = dsMaster
        TabOrder = 11
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object GroupBox1: TGroupBox
        Left = 540
        Top = 8
        Width = 185
        Height = 105
        Caption = 'Tipo Produto'
        TabOrder = 12
        object DBCheckBox3: TDBCheckBox
          Left = 8
          Top = 16
          Width = 150
          Height = 17
          Caption = 'Item de Estoque'
          DataField = 'cFlgProdEstoqueGP'
          DataSource = dsMaster
          TabOrder = 0
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox4: TDBCheckBox
          Left = 8
          Top = 37
          Width = 150
          Height = 17
          Caption = 'Item de Venda'
          DataField = 'cFlgProdVendaGP'
          DataSource = dsMaster
          TabOrder = 1
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox5: TDBCheckBox
          Left = 8
          Top = 58
          Width = 157
          Height = 17
          Caption = 'Item de Compra/Produ'#231#227'o'
          DataField = 'cFlgProdCompraGP'
          DataSource = dsMaster
          TabOrder = 2
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
        object DBCheckBox6: TDBCheckBox
          Left = 8
          Top = 80
          Width = 150
          Height = 17
          Caption = 'Ativo Fixo'
          DataField = 'cFlgAtivoFixoGP'
          DataSource = dsMaster
          TabOrder = 3
          ValueChecked = '1'
          ValueUnchecked = '0'
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM GRUPOPRODUTO'
      'WHERE nCdGrupoProduto = :nPK')
    Left = 616
    Top = 168
    object qryMasternCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryMastercNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
    object qryMastercFlgExpPDV: TIntegerField
      FieldName = 'cFlgExpPDV'
    end
    object qryMasternCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMasterdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryMasternCdGrupoMRPGP: TIntegerField
      FieldName = 'nCdGrupoMRPGP'
    end
    object qryMasternCdGrupoInventarioGP: TIntegerField
      FieldName = 'nCdGrupoInventarioGP'
    end
    object qryMasternCdTipoPlanejamentoGP: TIntegerField
      FieldName = 'nCdTipoPlanejamentoGP'
    end
    object qryMasternCdTipoObtencaoGP: TIntegerField
      FieldName = 'nCdTipoObtencaoGP'
    end
    object qryMasteriLeadTimeGP: TIntegerField
      FieldName = 'iLeadTimeGP'
    end
    object qryMasternLoteMinimoGP: TBCDField
      FieldName = 'nLoteMinimoGP'
      Precision = 12
      Size = 2
    end
    object qryMasternLoteMultiploGP: TBCDField
      FieldName = 'nLoteMultiploGP'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgAlertarInventarioGP: TIntegerField
      FieldName = 'cFlgAlertarInventarioGP'
    end
    object qryMastercFlgProdVendaGP: TIntegerField
      FieldName = 'cFlgProdVendaGP'
    end
    object qryMastercFlgProdEstoqueGP: TIntegerField
      FieldName = 'cFlgProdEstoqueGP'
    end
    object qryMastercFlgProdCompraGP: TIntegerField
      FieldName = 'cFlgProdCompraGP'
    end
    object qryMastercFlgAtivoFixoGP: TIntegerField
      FieldName = 'cFlgAtivoFixoGP'
    end
  end
  inherited dsMaster: TDataSource
    Left = 648
    Top = 184
  end
  object qryTipoPlanejamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoPlanejamento'
      'WHERE nCdTipoPlanejamento = :nPK')
    Left = 340
    Top = 224
    object qryTipoPlanejamentonCdTipoPlanejamento: TIntegerField
      FieldName = 'nCdTipoPlanejamento'
    end
    object qryTipoPlanejamentocNmTipoPlanejamento: TStringField
      FieldName = 'cNmTipoPlanejamento'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTipoPlanejamento
    Left = 560
    Top = 400
  end
  object qryTipoObtencao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoObtencao'
      'WHERE nCdTipoObtencao = :nPK')
    Left = 340
    Top = 264
    object qryTipoObtencaonCdTipoObtencao: TIntegerField
      FieldName = 'nCdTipoObtencao'
    end
    object qryTipoObtencaocNmTipoObtencao: TStringField
      FieldName = 'cNmTipoObtencao'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryTipoObtencao
    Left = 568
    Top = 408
  end
  object qryGrupoInventario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoInventario'
      'WHERE nCdGrupoInventario = :nPK')
    Left = 340
    Top = 296
    object qryGrupoInventarionCdGrupoInventario: TIntegerField
      FieldName = 'nCdGrupoInventario'
    end
    object qryGrupoInventariocNmGrupoInventario: TStringField
      FieldName = 'cNmGrupoInventario'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qryGrupoInventario
    Left = 576
    Top = 416
  end
  object qryGrupoMRP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoMRP'
      'WHERE nCdGrupoMRP = :nPK')
    Left = 340
    Top = 328
    object qryGrupoMRPnCdGrupoMRP: TIntegerField
      FieldName = 'nCdGrupoMRP'
    end
    object qryGrupoMRPcNmGrupoMRP: TStringField
      FieldName = 'cNmGrupoMRP'
      Size = 50
    end
    object qryGrupoMRPnCdTipoRequisicao: TIntegerField
      FieldName = 'nCdTipoRequisicao'
    end
    object qryGrupoMRPnCdSetor: TIntegerField
      FieldName = 'nCdSetor'
    end
  end
  object DataSource4: TDataSource
    DataSet = qryGrupoMRP
    Left = 584
    Top = 424
  end
end
