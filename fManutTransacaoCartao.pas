unit fManutTransacaoCartao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, DB, ADODB, StdCtrls, cxButtons, DBCtrls, Mask,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, Menus, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmManutTransacaoCartao = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    edtLoja: TMaskEdit;
    edtGrupoOperadora: TMaskEdit;
    edtOperadora: TMaskEdit;
    edtDtTransacaoFim: TMaskEdit;
    edtDtTransacaoIni: TMaskEdit;
    edtNrParcela: TMaskEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    cxButton1: TcxButton;
    edtNrDocto: TMaskEdit;
    qryTransacoes: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryGrupoOperadora: TADOQuery;
    qryGrupoOperadoranCdGrupoOperadoraCartao: TIntegerField;
    qryGrupoOperadoracNmGrupoOperadoraCartao: TStringField;
    qryOperadora: TADOQuery;
    qryOperadoranCdOperadoraCartao: TIntegerField;
    qryOperadoracNmOperadoraCartao: TStringField;
    dsOperadora: TDataSource;
    dsGrupoOperadora: TDataSource;
    dsLoja: TDataSource;
    qryTransacaoPendente: TADOQuery;
    qryTransacaoPendentenCdTitulo: TIntegerField;
    qryTransacaoPendentenCdTransacaoCartao: TIntegerField;
    qryTransacaoPendentedDtVenc: TDateTimeField;
    qryTransacaoPendentedDtTransacao: TDateTimeField;
    qryTransacaoPendentecNrLote: TStringField;
    qryTransacaoPendentecParcela: TStringField;
    qryTransacaoPendentenCdOperadora: TIntegerField;
    qryTransacaoPendentecNmOperadora: TStringField;
    qryTransacaoPendentenCdGrupoOperadora: TIntegerField;
    qryTransacaoPendentecNmGrupoOperadora: TStringField;
    qryTransacaoPendentenValTit: TBCDField;
    qryTransacaoPendentenSaldoTit: TBCDField;
    qryTransacaoPendentedDtVencOriginal: TDateTimeField;
    qryTransacaoPendentenSaldoTitOriginal: TBCDField;
    qryTransacaoPendentecStatus: TStringField;
    qryTransacaoPendenteiParcela: TIntegerField;
    qryTransacaoPendentecNmOperadoraCartao: TStringField;
    qryTransacaoPendenteiNrDocto: TIntegerField;
    qryTransacaoPendenteiNrAutorizacao: TIntegerField;
    dsTransacaoPendente: TDataSource;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1nCdTitulo: TcxGridDBColumn;
    cxGridDBTableView1nCdTransacaoCartao: TcxGridDBColumn;
    cxGridDBTableView1dDtVenc: TcxGridDBColumn;
    cxGridDBTableView1dDtTransacao: TcxGridDBColumn;
    cxGridDBTableView1DBcNmOperadoraCartao: TcxGridDBColumn;
    cxGridDBTableView1DBiNrDocto: TcxGridDBColumn;
    cxGridDBTableView1DBiNrAutorizacao: TcxGridDBColumn;
    cxGridDBTableView1cNrLote: TcxGridDBColumn;
    cxGridDBTableView1cParcela: TcxGridDBColumn;
    cxGridDBTableView1nCdOperadora: TcxGridDBColumn;
    cxGridDBTableView1cNmOperadora: TcxGridDBColumn;
    cxGridDBTableView1nCdGrupoOperadora: TcxGridDBColumn;
    cxGridDBTableView1cNmGrupoOperadora: TcxGridDBColumn;
    cxGridDBTableView1nValTit: TcxGridDBColumn;
    cxGridDBTableView1nSaldoTit: TcxGridDBColumn;
    cxGridDBTableView1dDtVencOriginal: TcxGridDBColumn;
    cxGridDBTableView1nSaldoTitOriginal: TcxGridDBColumn;
    cxGridDBTableView1cStatus: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    PopupMenu1: TPopupMenu;
    AlterarDadosdoPagamento1: TMenuItem;
    qryTransacaoCartao: TADOQuery;
    qryTransacaoCartaonCdTransacaoCartao: TIntegerField;
    qryTransacaoCartaodDtTransacao: TDateTimeField;
    qryTransacaoCartaodDtCredito: TDateTimeField;
    qryTransacaoCartaoiNrCartao: TLargeintField;
    qryTransacaoCartaoiNrDocto: TIntegerField;
    qryTransacaoCartaoiNrAutorizacao: TIntegerField;
    qryTransacaoCartaonCdOperadoraCartao: TIntegerField;
    qryTransacaoCartaonCdLanctoFin: TIntegerField;
    qryTransacaoCartaonValTransacao: TBCDField;
    qryTransacaoCartaonCdContaBancaria: TIntegerField;
    qryTransacaoCartaocFlgConferencia: TIntegerField;
    qryTransacaoCartaonCdUsuarioConf: TIntegerField;
    qryTransacaoCartaodDtConferencia: TDateTimeField;
    qryTransacaoCartaonCdStatusDocto: TIntegerField;
    qryTransacaoCartaoiParcelas: TIntegerField;
    qryTransacaoCartaocNrLotePOS: TStringField;
    qryTransacaoCartaonCdGrupoOperadoraCartao: TIntegerField;
    qryTransacaoCartaocFlgPOSEncerrado: TIntegerField;
    qryTransacaoCartaonCdLojaCartao: TIntegerField;
    qryTransacaoCartaonValTaxaOperadora: TBCDField;
    qryTransacaoCartaonCdCondPagtoCartao: TIntegerField;
    qryTransacaoCartaonCdServidorOrigem: TIntegerField;
    qryTransacaoCartaodDtReplicacao: TDateTimeField;
    qryTransacaoCartaocIDExterno: TStringField;
    qryDadosTitulo: TADOQuery;
    qryDadosTitulonCdTitulo: TIntegerField;
    qryDadosTitulonCdLojaTit: TIntegerField;
    qryCartaoSituacaoAtual: TADOQuery;
    qryCartaoSituacaoAtualcNmOperadoraCartao: TStringField;
    qryCartaoSituacaoAtualcNmCondPagto: TStringField;
    qryCartaoSituacaoAtualcNmFormaPagto: TStringField;
    qryCondPagto: TADOQuery;
    qryCondPagtonPercAcrescimo: TBCDField;
    qryCondPagtonCdTabTipoFormaPagto: TIntegerField;
    SP_ALTERA_CONDICAO_TRANSACAO_CARTAO: TADOStoredProc;
    qryTransacaoCartaocNrAutorizacao: TStringField;
    qryTransacaoCartaocNrDoctoNSU: TStringField;
    procedure AlterarDadosdoPagamento1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaExit(Sender: TObject);
    procedure edtGrupoOperadoraExit(Sender: TObject);
    procedure edtGrupoOperadoraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtOperadoraExit(Sender: TObject);
    procedure edtOperadoraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmManutTransacaoCartao: TfrmManutTransacaoCartao;

implementation

{$R *.dfm}

uses fMenu, fLookup_Padrao, fPdvFormaPagto, fPdvCondPagto,
  fCaixa_DadosCartao, fCaixa_AlteraCartao, StrUtils;

procedure TfrmManutTransacaoCartao.AlterarDadosdoPagamento1Click(
  Sender: TObject);
var
    nPK : Integer ;
    nCdFormapagto, nCdCondPagto : integer ;
    objDadosCartao  : TfrmCaixa_dadosCartao ;
    objAlteraCartao : TfrmCaixa_AlteraCartao ;
    objSelFormaPagto: TfrmPdvFormaPagto ;
    objSelCondPagto : TfrmPdvCondPagto ;
begin
  inherited;

  if not qryTransacaoPendente.Eof then
  begin

    qryTransacaoCartao.Close;
    PosicionaQuery(qryTransacaoCartao, qryTransacaoPendentenCdTransacaoCartao.AsString) ;

    if qryTransacaoCartao.eof then
    begin
        MensagemErro('A transa��o do cart�o n�o foi encontrada na tabela TRANSACAOCARTAO. ID: ' + qryTransacaoPendentenCdTransacaoCartao.asString) ;
        abort ;
    end ;

    if (qryTransacaoPendenteiParcela.Value > 1) then
    begin
        MensagemAlerta('A altera��o s� pode ser feita na parcela n�mero 1 da transa��o do cart�o.') ;
        abort ;
    end ;

    {-- inst�ncia os objetos das telas --}
    objDadosCartao  := TfrmCaixa_dadosCartao.Create(Self) ;
    objAlteraCartao := TfrmCaixa_AlteraCartao.Create(Self) ;
    objSelCondPagto := TfrmPdvCondPagto.Create(Self) ;
    objSelFormaPagto:= TfrmPdvFormaPagto.Create(Self) ;

    objSelFormaPagto.cFlgLiqCrediario := 0 ;
    objSelFormaPagto.bSomenteCartao   := True ;
    nCdFormaPagto                     := objSelFormaPagto.SelecionaFormaPagto() ;
    objSelFormaPagto.bSomenteCartao   := False ;

    if (nCdFormaPagto <= 0) then
        abort ;

    {-- passa os parametros para a tela de consulta de condi��es --}

    qryDadosTitulo.Close;
    PosicionaQuery(qryDadosTitulo, qryTransacaoPendentenCdTitulo.AsString) ;

    objSelCondPagto.nCdLoja               := qryDadosTitulonCdLojaTit.Value ;
    objSelCondPagto.nCdFormaPagto         := nCdFormaPagto                  ;
    objSelCondPagto.cFlgLiqCrediario      := 0                              ;

    objSelCondPagto.edtValorPagar.Value   := qryTransacaoCartaonValTransacao.Value ;
    objSelCondPagto.edtSaldo.Value        := qryTransacaoCartaonValTransacao.Value ;
    objSelCondPagto.cPermDesconto         := 'N' ;
    objSelCondPagto.edtValorPagar.Enabled := False ;

    {-- Processa a consulta --}
    nCdCondPagto  := objSelCondPagto.SelecionaCondPagto() ;

    {-- se o operador desistiu da consulta, retorna por aqui --}
    if (nCdCondPagto <= 0) then
        abort ;

    qryCondPagto.Close ;
    PosicionaQuery(qryCondPagto, IntToStr(nCdCondPagto)) ;

    // debito

    objDadosCartao.nCdLoja := qryDadosTitulonCdLojaTit.Value ;

    if (qryCondPagtonCdTabTipoFormaPagto.Value = 3) then
        objDadosCartao.cTipoOperacao       := 'D'
    else if (qryCondPagtonCdTabTipoFormaPagto.Value = 4) then
        objDadosCartao.cTipoOperacao       := 'C' ;

    objDadosCartao.edtValorPagar.Value := qryTransacaoCartaonValTransacao.Value ;

    showForm( objDadosCartao , FALSE ) ;

    objDadosCartao.nCdLoja := 0 ;

    if (Trim(objDadosCartao.edtNrDocto.Text) = '') then
    begin
        MensagemErro('Processo cancelado.') ;
        exit ;
    end ;

    qryCartaoSituacaoAtual.Close ;
    PosicionaQuery(qryCartaoSituacaoAtual, qryTransacaoPendentenCdTransacaoCartao.asString) ;

    objAlteraCartao.edtFormaPagto_Atual.Text  := qryCartaoSituacaoAtualcNmFormaPagto.Value;
    objAlteraCartao.edtCondPagto_Atual.Text   := qryCartaoSituacaoAtualcNmCondPagto.Value;
    objAlteraCartao.edtOperadora_Atual.Text   := qryCartaoSituacaoAtualcNmOperadoraCartao.Value ;
    objAlteraCartao.edtDocto_Atual.Text       := qryTransacaoCartaocNrDoctoNSU.Text;
    objAlteraCartao.edtAutorizacao_Atual.Text := qryTransacaoCartaocNrAutorizacao.Text ;

    objAlteraCartao.edtFormaPagto_Nova.Text  := objSelFormaPagto.qryFormaPagtocNmFormaPagto.Value;
    objAlteraCartao.edtCondPagto_Nova.Text   := objSelCondPagto.qryCondPagtocNmCondPagto.Value;
    objAlteraCartao.edtOperadora_Nova.Text   := objDadosCartao.edtNmOperadora.Text ;
    objAlteraCartao.edtDocto_Novo.Text       := objDadosCartao.edtNrDocto.Text;
    objAlteraCartao.edtAutorizacao_Nova.Text := objDadosCartao.edtNrAutorizacao.Text;

    if (objAlteraCartao.ShowModal = MrNo) then
        exit ;

    frmMenu.Connection.BeginTrans;

    try
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Close ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@nCdTransacaoCartao').Value := qryTransacaoPendentenCdTransacaoCartao.Value ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@nCdCondPagto').Value       := nCdCondPagto ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@nCdOperadoraCartao').Value := strToint(objDadosCartao.edtnCdOperadoraCartao.Text) ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@iNrDocto').Value           := strToint(frmMenu.RetNumeros(objDadosCartao.edtNrDocto.Text)) ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@iNrAutorizacao').Value     := strToint(frmMenu.RetNumeros(objDadosCartao.edtNrAutorizacao.Text)) ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@cNrDoctoNSU').Value        := objDadosCartao.edtNrDocto.Text ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@cNrAutorizacao').Value     := objDadosCartao.edtNrAutorizacao.Text ;
        SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.ExecProc;
    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento.') ;
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    freeAndNil( objSelFormaPagto ) ;
    freeAndNil( objSelCondPagto ) ;
    freeAndNil( objDadosCartao ) ;
    freeAndNil( objAlteraCartao ) ;

    ShowMessage('Processamento realizado com sucesso.') ;

    cxButton1.Click;

  end ;


end;

procedure TfrmManutTransacaoCartao.cxButton1Click(Sender: TObject);
begin
  inherited;

  if (DBedit1.Text = '') then
  begin
      MensagemAlerta('Seleciona a loja.') ;
      edtLoja.SetFocus;
      exit ;
  end ;

  cmdPreparaTemp.Execute;
  
  qryTransacoes.Close ;
  qryTransacoes.Parameters.ParamByName('nCdEmpresa').Value              := frmMenu.nCdEmpresaAtiva;
  qryTransacoes.Parameters.ParamByName('nCdLoja').Value                 := frmMenu.ConvInteiro(edtLoja.Text) ;
  qryTransacoes.Parameters.ParamByName('nCdGrupoOperadoraCartao').Value := frmMenu.ConvInteiro(edtGrupoOperadora.Text) ;
  qryTransacoes.Parameters.ParamByName('nCdOperadoraCartao').Value      := frmMenu.ConvInteiro(edtOperadora.Text) ;
  qryTransacoes.Parameters.ParamByName('dDtTransacaoIni').Value         := DateToStr(frmMenu.ConvData(edtDtTransacaoIni.Text)) ;
  qryTransacoes.Parameters.ParamByName('dDtTransacaoFim').Value         := DateToStr(frmMenu.ConvData(edtDtTransacaoFim.Text)) ;
  qryTransacoes.Parameters.ParamByName('iParcela').Value                := frmMenu.ConvInteiro(edtNrParcela.Text) ;
  qryTransacoes.Parameters.ParamByName('iNrDocto').Value                := frmMenu.ConvInteiro(edtNrDocto.Text) ;
  qryTransacoes.ExecSQL;

  qryTransacaoPendente.Close ;
  qryTransacaoPendente.Open ;


end;
procedure TfrmManutTransacaoCartao.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            edtLoja.Text := intToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmManutTransacaoCartao.edtLojaExit(Sender: TObject);
begin
  qryLoja.Close ;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;

end;

procedure TfrmManutTransacaoCartao.edtGrupoOperadoraExit(Sender: TObject);
begin
  inherited;

  qryGrupoOperadora.Close ;
  PosicionaQuery(qryGrupoOperadora, edtGrupoOperadora.Text) ;

end;

procedure TfrmManutTransacaoCartao.edtGrupoOperadoraKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(172);

        If (nPK > 0) then
        begin
            edtGrupoOperadora.Text := intToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmManutTransacaoCartao.edtOperadoraExit(Sender: TObject);
begin
  inherited;

  qryOperadora.Close ;
  PosicionaQuery(qryOperadora, edtOperadora.Text) ;

end;

procedure TfrmManutTransacaoCartao.edtOperadoraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(149);

        If (nPK > 0) then
        begin
            edtOperadora.Text := intToStr(nPK) ;
        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmManutTransacaoCartao) ;

end.
