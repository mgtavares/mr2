inherited frmGerarPickingList: TfrmGerarPickingList
  Left = 292
  Top = 111
  Width = 883
  Height = 529
  Caption = 'Gerar Picking List'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 68
    Width = 867
    Height = 423
  end
  inherited ToolBar1: TToolBar
    Width = 867
    ButtonWidth = 96
    inherited ToolButton1: TToolButton
      Caption = '&Gerar Picking'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 96
    end
    inherited ToolButton2: TToolButton
      Left = 104
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 68
    Width = 867
    Height = 423
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsItensPicking
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    STFilter.InstantApply = False
    STFilter.Location = stflInTitleFilterEh
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        ReadOnly = True
        Width = 71
      end
      item
        EditButtons = <>
        FieldName = 'cNmProduto'
        Footers = <>
        ReadOnly = True
        Width = 305
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeDisponivel'
        Footers = <>
        ReadOnly = True
        Width = 94
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeSelecionado'
        Footers = <>
        Width = 106
      end
      item
        EditButtons = <>
        FieldName = 'cNmLocalEstoque'
        Footers = <>
        ReadOnly = True
        Width = 250
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 867
    Height = 39
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Tag = 1
      Left = 8
      Top = 20
      Width = 58
      Height = 13
      Caption = 'Observa'#231#227'o'
    end
    object edtOBS: TEdit
      Left = 72
      Top = 12
      Width = 721
      Height = 21
      MaxLength = 100
      TabOrder = 0
    end
  end
  inherited ImageList1: TImageList
    Left = 264
    Top = 192
  end
  object qryItensPicking: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItensPickingBeforePost
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempItensPicking'
      ' WHERE nQtdeDisponivel > 0')
    Left = 200
    Top = 192
    object qryItensPickingnCdItensPicking: TAutoIncField
      FieldName = 'nCdItensPicking'
      ReadOnly = True
    end
    object qryItensPickingnCdProduto: TIntegerField
      DisplayLabel = 'C'#243'd. Produto'
      FieldName = 'nCdProduto'
    end
    object qryItensPickingcNmProduto: TStringField
      DisplayLabel = 'Descri'#231#227'o Produto'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryItensPickingnQtdeDisponivel: TBCDField
      DisplayLabel = 'Qtd. Disponivel'
      FieldName = 'nQtdeDisponivel'
      Precision = 12
    end
    object qryItensPickingnQtdeSelecionado: TBCDField
      DisplayLabel = 'Qtd. Selecionado'
      FieldName = 'nQtdeSelecionado'
      Precision = 12
    end
    object qryItensPickingcNmLocalEstoque: TStringField
      DisplayLabel = 'Local Estoque'
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
    object qryItensPickingnCdItemPedido: TIntegerField
      FieldName = 'nCdItemPedido'
    end
    object qryItensPickingnCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryItensPickingnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
  end
  object dsItensPicking: TDataSource
    DataSet = qryItensPicking
    Left = 200
    Top = 224
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempItensPicking'#39') IS NULL)'#13#10'BEGIN'#13#10'    ' +
      'CREATE TABLE #TempItensPicking (nCdItensPicking    int IDENTITY(' +
      '1,1) PRIMARY KEY'#13#10'                                   ,nCdItemPed' +
      'ido      int'#13#10'                                   ,nCdProduto    ' +
      '     int'#13#10'                                   ,nCdEmpresa        ' +
      ' int'#13#10'                                   ,cNmProduto         var' +
      'char(150)'#13#10'                                   ,nQtdeDisponivel  ' +
      '  decimal(12,4)'#13#10'                                   ,nQtdeSeleci' +
      'onado   decimal(12,4)'#13#10'                                   ,nCdLo' +
      'calEstoque    int'#13#10'                                   ,cNmLocalE' +
      'stoque    varchar(50))'#13#10'END'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 168
    Top = 192
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdPedido'
        Size = -1
        Value = Null
      end
      item
        Name = 'cFlgOrdemProducao'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdOrdemProducao'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdLocalEstoque    int'
      '       ,@nCdEmpresa         int'
      '       ,@nCdOperacaoEstoque int'
      '       ,@nCdGrupoProduto    int'
      '       ,@nCdProduto         int'
      '       ,@cFlgOrdemProducao  int'
      '       ,@nCdOrdemProducao   int'
      '       ,@nCdPedido          int'
      '       ,@nQtdePed           decimal(12,4)'
      '       ,@nCdItemPedido      int'
      '       ,@nCdLoja            int'
      ''
      'SET @nCdPedido         = :nCdPedido'
      'SET @cFlgOrdemProducao = :cFlgOrdemProducao'
      'SET @nCdOrdemProducao  = :nCdOrdemProducao'
      ''
      'TRUNCATE TABLE #TempItensPicking'
      ''
      'INSERT INTO #TempItensPicking (nCdItemPedido'
      '                              ,nCdProduto'
      '                              ,cNmProduto)'
      'SELECT ItemPedido.nCdItemPedido'
      '      ,ItemPedido.nCdProduto'
      '      ,ItemPedido.cNmItem'
      '  FROM ItemPedido'
      
        '       LEFT  JOIN OrdemProducao OP ON OP.nCdPedido = ItemPedido.' +
        'nCdPedido AND ItemPedido.nCdProduto = OP.nCdProduto'
      ' WHERE ItemPedido.nCdPedido = @nCdPedido'
      '   AND ItemPedido.nCdTipoItemPed   IN (1,2,5,6)'
      ''
      '--'
      '-- consulta loja do pedido'
      '--'
      'SELECT @nCdLoja  = nCdLoja'
      '  FROM Pedido'
      ' WHERE nCdPedido = @nCdPedido'
      ''
      '--'
      '-- Descobre o Estoque do item'
      '--'
      'DECLARE curItens CURSOR FOR'
      '    SELECT ItemPedido.nCdProduto'
      '          ,ItemPedido.nCdItemPedido'
      '          ,Pedido.nCdEmpresa'
      '          ,Produto.nCdGrupo_Produto'
      '          ,TipoPedido.nCdOperacaoEstoque'
      '      FROM ItemPedido'
      
        '           INNER JOIN Pedido           ON Pedido.nCdPedido      ' +
        '   = ItemPedido.nCdPedido'
      
        '           LEFT  JOIN Produto          ON Produto.nCdProduto    ' +
        '   = ItemPedido.nCdProduto'
      
        '           INNER JOIN TipoPedido       ON TipoPedido.nCdTipoPedi' +
        'do = Pedido.nCdTipoPedido'
      
        '           LEFT  JOIN OrdemProducao OP ON OP.nCdPedido = ItemPed' +
        'ido.nCdPedido AND ItemPedido.nCdProduto = OP.nCdProduto'
      '     WHERE Pedido.nCdPedido = @nCdPedido'
      '       AND ItemPedido.nCdTipoItemPed   IN (1,2,5,6)'
      ''
      'OPEN curItens'
      ''
      'FETCH NEXT'
      ' FROM curItens'
      ' INTO @nCdProduto'
      '     ,@nCdItemPedido'
      '     ,@nCdEmpresa'
      '     ,@nCdGrupoProduto'
      '     ,@nCdOperacaoEstoque'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      
        '    SET @nQtdePed = isNull((SELECT isNull(OP.nQtdePlanejada,Item' +
        'Pedido.nQtdePed)'
      
        '                                   - isNull((SELECT SUM(ItemPick' +
        'List.nQtdePick)'
      '                                               FROM ItemPickList'
      
        '                                                    INNER JOIN P' +
        'ickList ON PickList.nCdPickList = ItemPickList.nCdPickList'
      
        '                                              WHERE PickList.nCd' +
        'Pedido         = ItemPedido.nCdPedido'
      
        '                                                AND PickList.dDt' +
        'Cancel        IS NULL'
      
        '                                                AND ItemPickList' +
        '.nCdItemPedido = ItemPedido.nCdItemPedido'
      
        '                                                AND PickList.cFl' +
        'gBaixado       = 0),0)'
      '                                   - ItemPedido.nQtdeLibFat'
      '                                   - ItemPedido.nQtdeExpRec'
      '                                   - ItemPedido.nQtdeCanc'
      
        '                                   - (isNull(OP.nQtdePlanejada,0' +
        ') - isNull(OP.nQtdeConcluida,0))'
      '                              FROM ItemPedido'
      
        '                                   LEFT  JOIN OrdemProducao OP O' +
        'N OP.nCdPedido = ItemPedido.nCdPedido AND ItemPedido.nCdProduto ' +
        '= OP.nCdProduto'
      
        '                             WHERE ItemPedido.nCdPedido       = ' +
        '@nCdPedido'
      
        '                               AND ItemPedido.nCdTipoItemPed IN ' +
        '(1,2,5,6)'
      
        '                               AND ItemPedido.nCdItemPedido   = ' +
        '@nCdItemPedido'
      
        '                               AND (((@cFlgOrdemProducao = 0) AN' +
        'D (OP.nCdOrdemProducao IS NULL)) OR ((OP.nCdProduto = @nCdProdut' +
        'o) AND (OP.nCdOrdemProducao = @nCdOrdemProducao)))),0)'
      ''
      '    SELECT @nCdLocalEstoque = nCdLocalEstoque'
      '      FROM OperacaoLocalEstoque'
      '     WHERE nCdEmpresa         = @nCdEmpresa'
      '       AND ((nCdLoja          IS NULL) OR (nCdLoja = @nCdLoja))'
      '       AND nCdGrupoProduto    = @nCdGrupoProduto'
      '       AND nCdOperacaoEstoque = @nCdOperacaoEstoque'
      ''
      '    IF (@nCdLocalEstoque IS NULL)'
      '    BEGIN'
      ''
      '        SELECT @nCdLocalEstoque = nCdLocalEstoque'
      '          FROM OperacaoLocalEstoque'
      '         WHERE nCdEmpresa          = @nCdEmpresa'
      
        '           AND ((nCdLoja           IS NULL) OR (nCdLoja = @nCdLo' +
        'ja))'
      '           AND nCdOperacaoEstoque  = @nCdOperacaoEstoque'
      ''
      '    END'
      ''
      '    UPDATE #TempItensPicking'
      '       SET nCdLocalEstoque  = @nCdLocalEstoque'
      '          ,nCdEmpresa       = @nCdEmpresa'
      '          ,cNmLocalEstoque  = (SELECT cNmLocalEstoque'
      '                                 FROM LocalEstoque'
      
        '                                WHERE nCdLocalEstoque = @nCdLoca' +
        'lEstoque)'
      '          ,nQtdeDisponivel  = isNull(@nQtdePed,0)'
      '          ,nQtdeSelecionado = isNull(@nQtdePed,0)'
      '     WHERE nCdItemPedido    = @nCdItemPedido'
      ''
      '    FETCH NEXT'
      '     FROM curItens'
      '     INTO @nCdProduto'
      '         ,@nCdItemPedido'
      '         ,@nCdEmpresa'
      '         ,@nCdGrupoProduto'
      '         ,@nCdOperacaoEstoque'
      'END'
      ''
      'CLOSE curItens'
      'DEALLOCATE curItens'
      ''
      'SELECT *'
      '  FROM #TempItensPicking'
      ' WHERE nQtdeDisponivel > 0')
    Left = 168
    Top = 224
  end
  object SP_GERA_PICKLIST: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_PICKLIST;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cOBS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@nCdPickList'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 232
    Top = 192
  end
  object qryRegHoraProd: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdPickList'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'UPDATE PickList'
      '   SET nCdUsuarioPrimImp = :nCdUsuario'
      '      ,dDtPrimImp        = GetDate()'
      ' WHERE nCdPickList = :nCdPickList')
    Left = 232
    Top = 224
  end
end
