inherited frmAnaliseQualidade: TfrmAnaliseQualidade
  Left = 120
  Top = 145
  Caption = 'Analise de Qualidade'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    ButtonWidth = 93
    inherited ToolButton1: TToolButton
      Caption = 'Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 93
    end
    inherited ToolButton2: TToolButton
      Left = 101
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsMaster
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdRecebimento'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtReceb'
        Footers = <>
        Width = 114
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoReceb'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Recebimento.nCdRecebimento'
      '      ,dDtReceb'
      '      ,cNmTipoReceb'
      '      ,cNmTerceiro'
      '  FROM Recebimento'
      
        '       INNER JOIN Terceiro  ON Terceiro.nCdTerceiro   = Recebime' +
        'nto.nCdTerceiro'
      
        '       INNER JOIN TipoReceb ON TipoReceb.nCdTipoReceb = Recebime' +
        'nto.nCdTipoReceb'
      ' WHERE cFlgAnaliseQualidade = 1'
      '   AND cFlgAnaliseQualidadeOK = 0'
      '   AND nCdTabStatusReceb    = 6'
      '   AND dDtFech              IS NOT NULL'
      '   AND Recebimento.nCdEmpresa = :nPK')
    Left = 344
    Top = 248
    object qryMasternCdRecebimento: TIntegerField
      DisplayLabel = 
        'Recebimentos aguardando libera'#231#227'o da inspe'#231#227'o de qualidade|C'#243'dig' +
        'o'
      FieldName = 'nCdRecebimento'
    end
    object qryMasterdDtReceb: TDateTimeField
      DisplayLabel = 
        'Recebimentos aguardando libera'#231#227'o da inspe'#231#227'o de qualidade|Data ' +
        'Receb.'
      FieldName = 'dDtReceb'
    end
    object qryMastercNmTipoReceb: TStringField
      DisplayLabel = 
        'Recebimentos aguardando libera'#231#227'o da inspe'#231#227'o de qualidade|Tipo ' +
        'Recebimento'
      FieldName = 'cNmTipoReceb'
      Size = 50
    end
    object qryMastercNmTerceiro: TStringField
      DisplayLabel = 
        'Recebimentos aguardando libera'#231#227'o da inspe'#231#227'o de qualidade|Terce' +
        'iro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsMaster: TDataSource
    DataSet = qryMaster
    Left = 496
    Top = 80
  end
end
