unit rPerfOrcamento_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPerfOrcamento_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText4: TQRDBText;
    QRBand2: TQRBand;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    lblFiltro3: TQRLabel;
    lblFiltro2: TQRLabel;
    usp_Relatorio: TADOStoredProc;
    lblFiltro4: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape3: TQRShape;
    QRDBText5: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText3: TQRDBText;
    QRLabel6: TQRLabel;
    usp_RelatorionCdPedido: TIntegerField;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatoriodDtPedido: TDateTimeField;
    usp_RelatoriodDtPrevEntIni: TDateTimeField;
    usp_RelatoriodDtPrevEntFim: TDateTimeField;
    usp_RelatorionCdtabStatusPed: TIntegerField;
    usp_RelatorionValPedido: TBCDField;
    usp_RelatoriodDtAutor: TDateTimeField;
    usp_RelatoriodDtRejeicao: TDateTimeField;
    usp_RelatoriocMotivoRejeicao: TMemoField;
    QRGroup1: TQRGroup;
    usp_RelatoriocStatus: TStringField;
    QRDBText8: TQRDBText;
    QRLabel5: TQRLabel;
    QRExpr1: TQRExpr;
    QRLabel7: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText6: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRTotal: TQRLabel;
    QRAprovado: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRReprovado: TQRLabel;
    QRPendente: TQRLabel;
    QRLabel21: TQRLabel;
    QRAprovadoPercent: TQRLabel;
    QRReprovadoPercent: TQRLabel;
    QRPendentePercent: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel19: TQRLabel;
    QRQtdeTotal: TQRLabel;
    QRQtdeAprovado: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRQtdeReprovado: TQRLabel;
    QRQtdePendente: TQRLabel;
    QRLabel27: TQRLabel;
    QRQtdeAprovadoPercent: TQRLabel;
    QRQtdeReprovadoPercent: TQRLabel;
    QRQtdePendentePercent: TQRLabel;
    QRLabel31: TQRLabel;
    QRShape2: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    procedure QRDBText8Print(sender: TObject; var Value: String);
    procedure QRDBText3Print(sender: TObject; var Value: String);
    procedure QRBand4BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand4AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    cStatus : string ;
    nTotalAprovado : double ;
    nTotalReprovado : double ;
    nTotalPendente : double ;

    iQtdeAprovado : integer ;
    iQtdeReprovado : integer ;
    iQtdePendente : integer ;

  end;

var
  rptPerfOrcamento_view: TrptPerfOrcamento_view;

implementation

{$R *.dfm}

procedure TrptPerfOrcamento_view.QRDBText8Print(sender: TObject;
  var Value: String);
begin

    cStatus := QRDBText8.Caption ;

end;

procedure TrptPerfOrcamento_view.QRDBText3Print(sender: TObject;
  var Value: String);
begin

    if (usp_RelatoriocStatus.Value = 'APROVADO') then
    begin
        nTotalAprovado := nTotalAprovado + usp_RelatorionValPedido.Value ;
        iQtdeAprovado  := iQtdeAprovado + 1 ;
    end ;

    if (usp_RelatoriocStatus.Value = 'REPROVADO') then
    begin
        nTotalReprovado := nTotalReprovado + usp_RelatorionValPedido.Value ;
        iQtdeReprovado  := iQtdeReprovado + 1 ;
    end ;

    if (usp_RelatoriocStatus.Value = 'PENDENTE') then
    begin
        nTotalPendente := nTotalPendente + usp_RelatorionValPedido.Value ;
        iQtdePendente  := iQtdePendente + 1 ;
    end ;

end;

procedure TrptPerfOrcamento_view.QRBand4BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

    qrQtdeTotal.Caption     := intToStr(iQtdeAprovado+iQtdeReprovado+iQtdePendente) ;
    QRQtdeAprovado.Caption  := intToStr(iQtdeAprovado) ;
    QRQtdeReprovado.Caption := intToStr(iQtdeReprovado) ;
    QRQtdePendente.Caption  := intToStr(iQtdePendente) ;

    QRQtdeAprovadoPercent.Caption  := '0.00%' ;
    QRQtdeReprovadoPercent.Caption := '0.00%' ;
    QRQtdePendentePercent.Caption  := '0.00%' ;

    if (iQtdeAprovado > 0) then
        QRQtdeAprovadoPercent.Caption := FormatCurr('#,##0.00%',(iQtdeAprovado/(iQtdeAprovado+iQtdeReprovado+iQtdePendente))*100) ;

    if (iQtdeReprovado > 0) then
        QRQtdeReprovadoPercent.Caption := FormatCurr('#,##0.00%',(iQtdeReprovado/(iQtdeAprovado+iQtdeReprovado+iQtdePendente))*100) ;

    if (iQtdePendente > 0) then
        QRQtdePendentePercent.Caption := FormatCurr('#,##0.00%',(iQtdePendente/(iQtdeAprovado+iQtdeReprovado+iQtdePendente))*100) ;

    qrTotal.Caption     := FormatCurr('#,##0.00',nTotalAprovado+nTotalReprovado+nTotalPendente) ;
    QRAprovado.Caption  := FormatCurr('#,##0.00',nTotalAprovado) ;
    QRReprovado.Caption := FormatCurr('#,##0.00',nTotalReprovado) ;
    QRPendente.Caption  := FormatCurr('#,##0.00',nTotalPendente) ;

    QRAprovadoPercent.Caption  := '0.00%' ;
    QRReprovadoPercent.Caption := '0.00%' ;
    QRPendentePercent.Caption  := '0.00%' ;

    if (nTotalAprovado > 0) then
        QRAprovadoPercent.Caption := FormatCurr('#,##0.00%',(nTotalAprovado/(nTotalAprovado+nTotalReprovado+nTotalPendente))*100) ;

    if (nTotalReprovado > 0) then
        QRReprovadoPercent.Caption := FormatCurr('#,##0.00%',(nTotalReprovado/(nTotalAprovado+nTotalReprovado+nTotalPendente))*100) ;

    if (nTotalPendente > 0) then
        QRPendentePercent.Caption := FormatCurr('#,##0.00%',(nTotalPendente/(nTotalAprovado+nTotalReprovado+nTotalPendente))*100) ;

end;

procedure TrptPerfOrcamento_view.QRBand4AfterPrint(Sender: TQRCustomBand;
  BandPrinted: Boolean);
begin

    nTotalAprovado  := 0 ;
    nTotalReprovado := 0 ;
    nTotalPendente  := 0 ;

    iQtdeAprovado  := 0 ;
    iQtdeReprovado := 0 ;
    iQtdePendente  := 0 ;
    
end;

end.
