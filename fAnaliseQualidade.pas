unit fAnaliseQualidade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  GridsEh, DBGridEh, ADODB, DBGridEhGrouping;

type
  TfrmAnaliseQualidade = class(TfrmProcesso_Padrao)
    qryMaster: TADOQuery;
    qryMasternCdRecebimento: TIntegerField;
    qryMasterdDtReceb: TDateTimeField;
    qryMastercNmTipoReceb: TStringField;
    qryMastercNmTerceiro: TStringField;
    DBGridEh1: TDBGridEh;
    dsMaster: TDataSource;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAnaliseQualidade: TfrmAnaliseQualidade;

implementation

uses fMenu, fAnaliseQualidade_Itens;

{$R *.dfm}

procedure TfrmAnaliseQualidade.ToolButton1Click(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryMaster, IntToStr(frmMenu.nCdEmpresaAtiva)) ;

  if (qryMaster.eof) then
  begin
      MensagemAlerta('Nenhum recebimento pendente de inspe��o.') ;
  end ;
  
end;

procedure TfrmAnaliseQualidade.FormActivate(Sender: TObject);
begin
  inherited;

  ToolButton1.Click;
end;

procedure TfrmAnaliseQualidade.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmAnaliseQualidade_Itens;
begin
  inherited;

  if ((qryMaster.Active) and (qryMaster.RecordCount > 0)) then
  begin

      objForm := TfrmAnaliseQualidade_Itens.Create(nil);

      objForm.nCdRecebimento := qryMasternCdRecebimento.Value ;
      objForm.PosicionaQuery(objForm.qryItens, qryMasternCdRecebimento.AsString );
      showForm(objForm,True);

      ToolButton1.Click;
  end ;
end;

initialization
    RegisterClass(TfrmAnaliseQualidade) ;
    
end.
