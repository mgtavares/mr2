inherited frmEncerrarMovtoPOS_GeraLote: TfrmEncerrarMovtoPOS_GeraLote
  Left = 320
  Top = 135
  Width = 771
  Height = 494
  Caption = 'Gera'#231#227'o de Lote'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 89
    Width = 755
    Height = 143
  end
  inherited ToolBar1: TToolBar
    Width = 755
    ButtonWidth = 94
    inherited ToolButton1: TToolButton
      Caption = 'Processar Lote'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 94
    end
    inherited ToolButton2: TToolButton
      Left = 102
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 755
    Height = 60
    Align = alTop
    Caption = ' Identifica'#231#227'o do Lote '
    TabOrder = 1
    object Label2: TLabel
      Tag = 1
      Left = 242
      Top = 32
      Width = 154
      Height = 13
      Alignment = taRightJustify
      Caption = 'Confirma'#231#227'o do N'#250'mero do Lote'
    end
    object Label1: TLabel
      Tag = 1
      Left = 16
      Top = 32
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero do Lote'
    end
    object Edit1: TMaskEdit
      Left = 104
      Top = 24
      Width = 121
      Height = 21
      PasswordChar = '#'
      TabOrder = 0
    end
    object Edit2: TMaskEdit
      Left = 408
      Top = 24
      Width = 121
      Height = 21
      PasswordChar = '#'
      TabOrder = 1
    end
  end
  object DBGridEh1: TDBGridEh [3]
    Left = 0
    Top = 89
    Width = 755
    Height = 143
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsTempResumo
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    IndicatorOptions = [gioShowRowIndicatorEh]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
    SumList.Active = True
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdOperadoraCartao'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cNmOperadoraCartao'
        Footers = <>
        ReadOnly = True
        Width = 202
      end
      item
        EditButtons = <>
        FieldName = 'cNmForma'
        Footers = <>
        ReadOnly = True
        Width = 175
      end
      item
        EditButtons = <>
        FieldName = 'nValTransacaoCalc'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nValLiquidoCalc'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nValTaxaOperadoraCalc'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nValTransacaoInf'
        Footer.DisplayFormat = '#,##0.00'
        Footer.FieldName = 'nValTransacaoInf'
        Footer.ValueType = fvtSum
        Footers = <>
        Width = 141
      end
      item
        EditButtons = <>
        FieldName = 'nValLiquidoInf'
        Footer.DisplayFormat = '#,##0.00'
        Footer.FieldName = 'nValLiquidoInf'
        Footer.ValueType = fvtSum
        Footers = <>
        Width = 101
      end
      item
        EditButtons = <>
        FieldName = 'nValTaxaOperadoraInf'
        Footer.DisplayFormat = '#,##0.00'
        Footer.FieldName = 'nValTaxaOperadoraInf'
        Footer.ValueType = fvtSum
        Footers = <>
        ReadOnly = True
        Width = 96
      end
      item
        EditButtons = <>
        FieldName = 'nValTransacaoDif'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nValLiquidoDif'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nValTaxaOperadoraDif'
        Footers = <>
        Visible = False
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object DBGridEh2: TDBGridEh [4]
    Left = 0
    Top = 232
    Width = 755
    Height = 224
    Align = alBottom
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsTempParcelaResumoOperadora
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    IndicatorOptions = [gioShowRowIndicatorEh]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
    SumList.Active = True
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'cNmOperadora'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cNmForma'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'iParcela'
        Footers = <>
        ReadOnly = True
        Width = 58
      end
      item
        EditButtons = <>
        FieldName = 'dDtCredito'
        Footers = <>
        Width = 118
      end
      item
        EditButtons = <>
        FieldName = 'nValLiquido'
        Footer.DisplayFormat = '#,##0.00'
        Footer.FieldName = 'nValLiquido'
        Footer.ValueType = fvtSum
        Footers = <>
        Width = 94
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 368
    Top = 112
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempTransacaoCartao'#39') IS NULL)'#13#10'BEGIN'#13#10#13 +
      #10#9'SELECT TOP 0 TransacaoCartao.nCdTransacaoCartao'#13#10#9#9'  ,Transaca' +
      'oCartao.dDtTransacao'#13#10#9#9'  ,TransacaoCartao.iNrCartao'#13#10#9#9'  ,Trans' +
      'acaoCartao.iNrDocto'#13#10#9#9'  ,TransacaoCartao.nCdOperadoraCartao'#13#10#9#9 +
      '  ,OperadoraCartao.cNmOperadoraCartao'#13#10#9#9'  ,TransacaoCartao.iPar' +
      'celas'#13#10#9#9'  ,nValTransacao'#13#10#9#9'  ,(nValTransacao - nValTaxaOperado' +
      'ra) nValLiquido'#13#10#9#9'  ,nValTaxaOperadora'#13#10#9#9'  ,StatusDocto.nCdSta' +
      'tusDocto'#13#10#9#9'  ,StatusDocto.cNmStatusDocto'#13#10#9#9'  ,0 cFlgSelecionad' +
      'o'#13#10#9'  INTO #TempTransacaoCartao'#13#10#9'  FROM TransacaoCartao'#13#10#9#9'   I' +
      'NNER JOIN OperadoraCartao ON OperadoraCartao.nCdOperadoraCartao ' +
      '= TransacaoCartao.nCdOperadoraCartao'#13#10#9#9'   INNER JOIN StatusDoct' +
      'o     ON StatusDocto.nCdStatusDocto         = TransacaoCartao.nC' +
      'dStatusDocto'#13#10#13#10'END'#13#10#13#10'IF (OBJECT_ID('#39'tempdb..#TempResumoOperado' +
      'ra'#39') IS NULL)'#13#10'BEGIN'#13#10#13#10#9'SELECT TOP 0 Temp.nCdOperadoraCartao'#13#10#9 +
      #9'  ,OperadoraCartao.cNmOperadoraCartao'#13#10#9#9'  ,CASE WHEN Temp.iPar' +
      'celas = 1 THEN '#39'A VISTA'#39#13#10#9#9#9#9'ELSE '#39'PARCELADO'#39#13#10#9#9'   END cNmForm' +
      'a'#13#10#9#9'  ,Convert(DECIMAL(12,2),0.00)                             ' +
      '        nValTransacaoCalc'#13#10#9#9'  ,Convert(DECIMAL(12,2),0.00)     ' +
      '                                nValLiquidoCalc'#13#10#9#9'  ,Convert(DE' +
      'CIMAL(12,2),0.00)                                     nValTaxaOp' +
      'eradoraCalc'#13#10#9#9'  ,Convert(DECIMAL(12,2),0.00)                   ' +
      '                  nValTransacaoInf'#13#10#9#9'  ,Convert(DECIMAL(12,2),0' +
      '.00)                                     nValLiquidoInf'#13#10#9#9'  ,Co' +
      'nvert(DECIMAL(12,2),0.00)                                     nV' +
      'alTaxaOperadoraInf'#13#10#9#9'  ,Convert(DECIMAL(12,2),0.00)            ' +
      '                         nValTransacaoDif'#13#10#9#9'  ,Convert(DECIMAL(' +
      '12,2),0.00)                                     nValLiquidoDif'#13#10 +
      #9#9'  ,Convert(DECIMAL(12,2),0.00)                                ' +
      '     nValTaxaOperadoraDif'#13#10#9'  INTO #TempResumoOperadora'#13#10#9'  FROM' +
      ' #TempTransacaoCartao Temp'#13#10#9#9'   INNER JOIN OperadoraCartao ON O' +
      'peradoraCartao.nCdOperadoraCartao = Temp.nCdOperadoraCartao'#13#10#9#9' ' +
      '  INNER JOIN StatusDocto     ON StatusDocto.nCdStatusDocto      ' +
      '   = Temp.nCdStatusDocto'#13#10#9' WHERE cFlgSelecionado = 1'#13#10#9' GROUP B' +
      'Y Temp.nCdOperadoraCartao'#13#10#9#9#9' ,OperadoraCartao.cNmOperadoraCart' +
      'ao'#13#10#9#9#9' ,CASE WHEN Temp.iParcelas = 1 THEN '#39'A VISTA'#39#13#10#9#9#9#9'   ELS' +
      'E '#39'PARCELADO'#39#13#10#9#9#9'  END '#13#10#13#10'END'#13#10#13#10'IF (OBJECT_ID('#39'tempdb..#TempP' +
      'arcelaResumoOperadora'#39') IS NULL)'#13#10'BEGIN'#13#10#13#10'    CREATE TABLE #Tem' +
      'pParcelaResumoOperadora (cNmOperadora varchar(50)'#13#10'             ' +
      '                                ,cNmForma     varchar(50)'#13#10'     ' +
      '                                        ,iParcela     int'#13#10'     ' +
      '                                        ,dDtCredito   datetime'#13#10 +
      '                                             ,nValLiquido  decim' +
      'al(12,2) default 0 not null)'#13#10'                                  ' +
      '           '#13#10'END'#13#10'TRUNCATE TABLE #TempResumoOperadora'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 336
    Top = 112
  end
  object qryTempResumo: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryTempResumoAfterScroll
    Parameters = <
      item
        Name = 'cGerarParcelas'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @cNmOperadora   varchar(50)'
      '       ,@cNmForma       varchar(50)'
      '       ,@iParcelas      int'
      '       ,@iParcela       int'
      '       ,@cGerarParcelas char(1)'
      ''
      'Set @cGerarParcelas = :cGerarParcelas'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempTransacaoCartao'#39') IS NULL)'
      'BEGIN'
      ''
      #9'SELECT TOP 0 TransacaoCartao.nCdTransacaoCartao'
      #9#9'  ,TransacaoCartao.dDtTransacao'
      #9#9'  ,TransacaoCartao.iNrCartao'
      #9#9'  ,TransacaoCartao.iNrDocto'
      #9#9'  ,TransacaoCartao.nCdOperadoraCartao'
      #9#9'  ,OperadoraCartao.cNmOperadoraCartao'
      #9#9'  ,TransacaoCartao.iParcelas'
      #9#9'  ,nValTransacao'
      #9#9'  ,(nValTransacao - nValTaxaOperadora) nValLiquido'
      #9#9'  ,nValTaxaOperadora'
      #9#9'  ,StatusDocto.nCdStatusDocto'
      #9#9'  ,StatusDocto.cNmStatusDocto'
      #9#9'  ,0 cFlgSelecionado'
      #9'  INTO #TempTransacaoCartao'
      #9'  FROM TransacaoCartao'
      
        #9#9'   INNER JOIN OperadoraCartao ON OperadoraCartao.nCdOperadoraC' +
        'artao = TransacaoCartao.nCdOperadoraCartao'
      
        #9#9'   INNER JOIN StatusDocto     ON StatusDocto.nCdStatusDocto   ' +
        '      = TransacaoCartao.nCdStatusDocto'
      ''
      'END'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempResumoOperadora'#39') IS NULL)'
      'BEGIN'
      ''
      #9'SELECT TOP 0 Temp.nCdOperadoraCartao'
      #9#9'  ,OperadoraCartao.cNmOperadoraCartao'
      #9#9'  ,CASE WHEN Temp.iParcelas = 1 THEN '#39'A VISTA'#39
      #9#9#9#9'ELSE '#39'PARCELADO'#39
      #9#9'   END cNmForma'
      #9#9'  ,0.00                                     nValTransacaoCalc'
      #9#9'  ,0.00                                     nValLiquidoCalc'
      
        #9#9'  ,0.00                                     nValTaxaOperadoraC' +
        'alc'
      #9#9'  ,0.00                                     nValTransacaoInf'
      #9#9'  ,0.00                                     nValLiquidoInf'
      
        #9#9'  ,0.00                                     nValTaxaOperadoraI' +
        'nf'
      #9#9'  ,0.00                                     nValTransacaoDif'
      #9#9'  ,0.00                                     nValLiquidoDif'
      
        #9#9'  ,0.00                                     nValTaxaOperadoraD' +
        'if'
      #9'  INTO #TempResumoOperadora'
      #9'  FROM #TempTransacaoCartao Temp'
      
        #9#9'   INNER JOIN OperadoraCartao ON OperadoraCartao.nCdOperadoraC' +
        'artao = Temp.nCdOperadoraCartao'
      
        #9#9'   INNER JOIN StatusDocto     ON StatusDocto.nCdStatusDocto   ' +
        '      = Temp.nCdStatusDocto'
      #9' WHERE cFlgSelecionado = 1'
      #9' GROUP BY Temp.nCdOperadoraCartao'
      #9#9#9' ,OperadoraCartao.cNmOperadoraCartao'
      #9#9#9' ,CASE WHEN Temp.iParcelas = 1 THEN '#39'A VISTA'#39
      #9#9#9#9'   ELSE '#39'PARCELADO'#39
      #9#9#9'  END '
      ''
      'END'
      ''
      'TRUNCATE TABLE #TempResumoOperadora'
      ''
      'INSERT INTO #TempResumoOperadora'
      #9'SELECT Temp.nCdOperadoraCartao'
      #9#9'  ,OperadoraCartao.cNmOperadoraCartao'
      #9#9'  ,CASE WHEN Temp.iParcelas = 1 THEN '#39'A VISTA'#39
      #9#9#9#9'ELSE '#39'PARCELADO'#39
      #9#9'   END cNmForma'
      #9#9'  ,Sum(nValTransacao)                       nValTransacaoCalc'
      #9#9'  ,Sum((nValTransacao - nValTaxaOperadora)) nValLiquidoCalc'
      
        #9#9'  ,Sum(nValTaxaOperadora)                   nValTaxaOperadoraC' +
        'alc'
      #9#9'  ,0.00                                     nValTransacaoInf'
      #9#9'  ,0.00                                     nValLiquidoInf'
      
        #9#9'  ,0.00                                     nValTaxaOperadoraI' +
        'nf'
      #9#9'  ,0.00                                     nValTransacaoDif'
      #9#9'  ,0.00                                     nValLiquidoDif'
      
        #9#9'  ,0.00                                     nValTaxaOperadoraD' +
        'if'
      #9'  FROM #TempTransacaoCartao Temp'
      
        #9#9'   INNER JOIN OperadoraCartao ON OperadoraCartao.nCdOperadoraC' +
        'artao = Temp.nCdOperadoraCartao'
      
        #9#9'   INNER JOIN StatusDocto     ON StatusDocto.nCdStatusDocto   ' +
        '      = Temp.nCdStatusDocto'
      #9' WHERE cFlgSelecionado = 1'
      #9' GROUP BY Temp.nCdOperadoraCartao'
      #9#9#9' ,OperadoraCartao.cNmOperadoraCartao'
      #9#9#9' ,CASE WHEN Temp.iParcelas = 1 THEN '#39'A VISTA'#39
      #9#9#9#9'   ELSE '#39'PARCELADO'#39
      #9#9#9'  END '
      #9' ORDER BY 2,3'
      ''
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempParcelaResumoOperadora'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempParcelaResumoOperadora (cNmOperadora varch' +
        'ar(50)'
      
        '                                             ,cNmForma     varch' +
        'ar(50)'
      '                                             ,iParcela     int'
      
        '                                             ,dDtCredito   datet' +
        'ime'
      
        '                                             ,nValLiquido  decim' +
        'al(12,2) default 0 not null)'
      ''
      'END'
      ''
      'TRUNCATE TABLE #TempParcelaResumoOperadora'
      ''
      'IF (@cGerarParcelas = '#39'1'#39')'
      'BEGIN'
      ''
      #9'DECLARE curParcelas CURSOR'
      #9#9'FOR SELECT OperadoraCartao.cNmOperadoraCartao'
      #9#9#9#9'  ,CASE WHEN Temp.iParcelas = 1 THEN '#39'A VISTA'#39
      #9#9#9#9#9#9'ELSE '#39'PARCELADO'#39
      #9#9#9#9'   END cNmForma'
      #9#9#9#9'  ,MAX(iParcelas) iParcelas'
      #9#9#9'  FROM #TempTransacaoCartao Temp'
      
        #9#9#9#9'   INNER JOIN OperadoraCartao ON OperadoraCartao.nCdOperador' +
        'aCartao = Temp.nCdOperadoraCartao'
      
        #9#9#9#9'   INNER JOIN StatusDocto     ON StatusDocto.nCdStatusDocto ' +
        '        = Temp.nCdStatusDocto'
      #9#9#9' WHERE cFlgSelecionado = 1'
      #9#9#9' GROUP BY OperadoraCartao.cNmOperadoraCartao'
      #9#9#9#9#9' ,CASE WHEN Temp.iParcelas = 1 THEN '#39'A VISTA'#39
      #9#9#9#9#9#9'   ELSE '#39'PARCELADO'#39
      #9#9#9#9#9'  END'
      #9#9#9' ORDER BY 1,2'
      ''
      #9'OPEN curParcelas'
      ''
      #9'FETCH NEXT'
      #9' FROM curParcelas'
      #9' INTO @cNmOperadora'
      #9#9' ,@cNmForma'
      #9#9' ,@iParcelas'
      ''
      #9'WHILE (@@FETCH_STATUS = 0)'
      #9'BEGIN'
      ''
      #9#9'Set @iParcela = 1'
      ''
      #9#9'WHILE (@iParcela <= @iParcelas)'
      #9#9'BEGIN'
      ''
      #9#9#9'INSERT INTO #TempParcelaResumoOperadora (cNmOperadora'
      #9#9#9#9#9#9#9#9#9#9#9#9#9',cNmForma'
      #9#9#9#9#9#9#9#9#9#9#9#9#9',iParcela)'
      #9#9#9#9#9#9#9#9#9#9#9'  VALUES(@cNmOperadora'
      #9#9#9#9#9#9#9#9#9#9#9#9#9',@cNmForma'
      #9#9#9#9#9#9#9#9#9#9#9#9#9',@iParcela)'
      ''
      #9#9#9'Set @iParcela = @iParcela + 1'
      ''
      #9#9'END'
      ''
      #9#9'FETCH NEXT'
      #9#9' FROM curParcelas'
      #9#9' INTO @cNmOperadora'
      #9#9#9' ,@cNmForma'
      #9#9' ,@iParcelas'
      ''
      #9'END'
      ''
      #9'CLOSE curParcelas'
      #9'DEALLOCATE curParcelas'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #TempResumoOperadora'
      ' ORDER BY 2,3')
    Left = 336
    Top = 176
    object qryTempResumonCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryTempResumocNmOperadoraCartao: TStringField
      DisplayLabel = 'Operadora'
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryTempResumocNmForma: TStringField
      DisplayLabel = 'Condi'#231#227'o'
      FieldName = 'cNmForma'
      Size = 9
    end
    object qryTempResumonValTransacaoCalc: TBCDField
      FieldName = 'nValTransacaoCalc'
      DisplayFormat = '#,##0.00'
      Precision = 2
      Size = 2
    end
    object qryTempResumonValLiquidoCalc: TBCDField
      FieldName = 'nValLiquidoCalc'
      DisplayFormat = '#,##0.00'
      Precision = 2
      Size = 2
    end
    object qryTempResumonValTaxaOperadoraCalc: TBCDField
      FieldName = 'nValTaxaOperadoraCalc'
      DisplayFormat = '#,##0.00'
      Precision = 2
      Size = 2
    end
    object qryTempResumonValTransacaoInf: TBCDField
      DisplayLabel = 'Valor Bruto'
      FieldName = 'nValTransacaoInf'
      DisplayFormat = '#,##0.00'
      Precision = 2
      Size = 2
    end
    object qryTempResumonValLiquidoInf: TBCDField
      DisplayLabel = 'Valor L'#237'quido'
      FieldName = 'nValLiquidoInf'
      DisplayFormat = '#,##0.00'
      Precision = 2
      Size = 2
    end
    object qryTempResumonValTaxaOperadoraInf: TBCDField
      DisplayLabel = 'Desconto'
      FieldName = 'nValTaxaOperadoraInf'
      DisplayFormat = '#,##0.00'
      Precision = 2
      Size = 2
    end
    object qryTempResumonValTransacaoDif: TBCDField
      FieldName = 'nValTransacaoDif'
      DisplayFormat = '#,##0.00'
      Precision = 2
      Size = 2
    end
    object qryTempResumonValLiquidoDif: TBCDField
      FieldName = 'nValLiquidoDif'
      DisplayFormat = '#,##0.00'
      Precision = 2
      Size = 2
    end
    object qryTempResumonValTaxaOperadoraDif: TBCDField
      FieldName = 'nValTaxaOperadoraDif'
      DisplayFormat = '#,##0.00'
      Precision = 2
      Size = 2
    end
  end
  object dsTempResumo: TDataSource
    DataSet = qryTempResumo
    Left = 368
    Top = 176
  end
  object qryTempParcelaResumoOperadora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNmOperadora'
        Size = -1
        Value = Null
      end
      item
        Name = 'cNmForma'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempParcelaResumoOperadora'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempParcelaResumoOperadora (cNmOperadora varch' +
        'ar(50)'
      
        '                                             ,cNmForma     varch' +
        'ar(50)'
      '                                             ,iParcela     int'
      
        '                                             ,dDtCredito   datet' +
        'ime'
      
        '                                             ,nValLiquido  decim' +
        'al(12,2) default 0 not null)'
      '                                             '
      'END'
      ''
      'SELECT *'
      'FROM #TempParcelaResumoOperadora'
      'WHERE cNmOperadora = :cNmOperadora'
      'AND cNmForma = :cNmForma')
    Left = 336
    Top = 208
    object qryTempParcelaResumoOperadoracNmOperadora: TStringField
      FieldName = 'cNmOperadora'
      Size = 50
    end
    object qryTempParcelaResumoOperadoracNmForma: TStringField
      FieldName = 'cNmForma'
      Size = 50
    end
    object qryTempParcelaResumoOperadoraiParcela: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'iParcela'
    end
    object qryTempParcelaResumoOperadoradDtCredito: TDateTimeField
      DisplayLabel = 'Data de Cr'#233'dito'
      FieldName = 'dDtCredito'
    end
    object qryTempParcelaResumoOperadoranValLiquido: TBCDField
      DisplayLabel = 'Valor L'#237'quido'
      FieldName = 'nValLiquido'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsTempParcelaResumoOperadora: TDataSource
    DataSet = qryTempParcelaResumoOperadora
    Left = 368
    Top = 208
  end
  object qryOperadora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT iFloating'
      '  FROM OperadoraCartao'
      ' WHERE nCdOperadoraCartao = :nPK')
    Left = 336
    Top = 144
    object qryOperadoraiFloating: TIntegerField
      FieldName = 'iFloating'
    end
  end
  object SP_GERA_LOTE_OPERADORA_CARTAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_LOTE_OPERADORA_CARTAO'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNrLote'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end>
    Left = 368
    Top = 144
  end
  object qryTotalLiqParc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'iParcela'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'cNmOperadora'
        Size = -1
        Value = Null
      end
      item
        Name = 'cNmForma'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @iParcela     int'
      '       ,@cNmOperadora varchar(50)'
      '       ,@cNmForma     varchar(50)'
      ''
      'SET @iParcela     = :iParcela'
      'SET @cNmOperadora = :cNmOperadora'
      'SET @cNmForma     = :cNmForma'
      ''
      'SELECT SUM(Titulo.nSaldoTit) nSaldoTit'
      '  FROM Titulo'
      ' WHERE Titulo.iParcela = @iParcela'
      '   AND EXISTS(SELECT 1'
      '                FROM #TempTransacaoCartao Temp'
      
        '               WHERE Temp.nCdTransacaoCartao = Titulo.nCdTransac' +
        'aoCartao'
      '                 AND Temp.cFlgSelecionado    = 1'
      '                 AND Temp.cNmOperadoraCartao = @cNmOperadora'
      
        '                 AND (  (Temp.iParcelas      = 1 AND @cNmForma =' +
        ' '#39'A VISTA'#39') '
      
        '                      OR(Temp.iParcelas      > 1 AND @cNmForma =' +
        ' '#39'PARCELADO'#39')))')
    Left = 336
    Top = 240
    object qryTotalLiqParcnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
end
