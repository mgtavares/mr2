unit rVendaPeriodoProduto_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptVendaPeriodoProduto_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRBand2: TQRBand;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    lblFiltro3: TQRLabel;
    lblFiltro2: TQRLabel;
    usp_Relatorio: TADOStoredProc;
    QRDBText1: TQRDBText;
    lblFiltro4: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape3: TQRShape;
    QRExpr1: TQRExpr;
    QRDBText5: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRDBText6: TQRDBText;
    usp_RelatoriocNmDepartamento: TStringField;
    usp_RelatoriocNmCategoria: TStringField;
    usp_RelatoriocNmSubCategoria: TStringField;
    usp_RelatoriocNmSegmento: TStringField;
    usp_RelatorionValor: TBCDField;
    usp_RelatorionPercParticip: TBCDField;
    usp_RelatoriocOrdem: TStringField;
    QRImage1: TQRImage;
    QRLabel14: TQRLabel;
    QRLabel22: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape2: TQRShape;
    QRShape1: TQRShape;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaPeriodoProduto_view: TrptVendaPeriodoProduto_view;

implementation

{$R *.dfm}

end.
