inherited frmMRP_GeraPedido: TfrmMRP_GeraPedido
  Left = 210
  Top = 208
  Width = 864
  Height = 330
  BorderIcons = [biSystemMenu]
  Caption = 'Gera'#231#227'o Pedido'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 848
    Height = 265
  end
  object Label1: TLabel [1]
    Left = 32
    Top = 88
    Width = 100
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sugest'#227'o de Compra'
  end
  object Label3: TLabel [2]
    Left = 372
    Top = 112
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data para Entrega'
  end
  object Label7: TLabel [3]
    Left = 52
    Top = 40
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto Principal'
  end
  object Label9: TLabel [4]
    Left = 648
    Top = 337
    Width = 177
    Height = 13
    Caption = 'Produto sem fornecedor homologado'
    Visible = False
  end
  object Label10: TLabel [5]
    Left = 38
    Top = 64
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Un. Medida Compra'
  end
  object Label11: TLabel [6]
    Left = 184
    Top = 64
    Width = 81
    Height = 13
    Caption = 'Fator Convers'#227'o'
  end
  object Label12: TLabel [7]
    Left = 352
    Top = 64
    Width = 109
    Height = 13
    Alignment = taRightJustify
    Caption = 'Quant. M'#237'nima Compra'
  end
  object Label13: TLabel [8]
    Left = 8
    Top = 112
    Width = 124
    Height = 13
    Alignment = taRightJustify
    Caption = 'Qtde para Pedido Compra'
  end
  object Label14: TLabel [9]
    Left = 25
    Top = 136
    Width = 107
    Height = 13
    Alignment = taRightJustify
    Caption = 'Local Estoque Entrega'
  end
  object Label15: TLabel [10]
    Left = 137
    Top = 151
    Width = 229
    Height = 13
    Alignment = taRightJustify
    Caption = '(deixe em branco para entrega no local padr'#227'o)'
    FocusControl = cxTextEdit2
  end
  inherited ToolBar1: TToolBar
    Width = 848
    ButtonWidth = 102
    inherited ToolButton1: TToolButton
      Caption = 'Gerar Pedido'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 102
    end
    inherited ToolButton2: TToolButton
      Left = 110
    end
    object ToolButton4: TToolButton
      Left = 212
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 220
      Top = 0
      Caption = #218'ltimos Pedidos'
      ImageIndex = 2
      OnClick = ToolButton5Click
    end
  end
  object edtQtdeSugerida: TcxCurrencyEdit [12]
    Left = 136
    Top = 80
    Width = 81
    Height = 21
    Enabled = False
    ParentFont = False
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ReadOnly = True
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    TabOrder = 1
  end
  object DBGridEh1: TDBGridEh [13]
    Left = 8
    Top = 184
    Width = 601
    Height = 105
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsFornecedores
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    RowDetailPanel.Color = clBtnFace
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nPercentCompra'
        Footers = <>
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footers = <>
        ReadOnly = True
        Width = 32
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'nPrecoUnit'
        Footers = <>
        ReadOnly = True
        Tag = 1
        Width = 87
      end
      item
        EditButtons = <>
        FieldName = 'cNrContrato'
        Footers = <>
        ReadOnly = True
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StaticText2: TStaticText [14]
    Left = 8
    Top = 168
    Width = 601
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Fornecedores Homologados do Produto Principal'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 7
  end
  object DBGridEh2: TDBGridEh [15]
    Left = 8
    Top = 312
    Width = 1065
    Height = 273
    AllowedOperations = [alopUpdateEh]
    AutoFitColWidths = True
    DataGrouping.GroupLevels = <>
    DataSource = dsFormula
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    PopupMenu = PopupMenu1
    RowDetailPanel.Color = clBtnFace
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Visible = False
    OnDrawColumnCell = DBGridEh2DrawColumnCell
    OnKeyUp = DBGridEh2KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        ReadOnly = True
        Width = 46
      end
      item
        EditButtons = <>
        FieldName = 'cNmProduto'
        Footers = <>
        ReadOnly = True
        Width = 294
      end
      item
        EditButtons = <>
        FieldName = 'cUnidadeMedida'
        Footers = <>
        ReadOnly = True
        Width = 21
      end
      item
        EditButtons = <>
        FieldName = 'nCdLocalEstoqueEntrega'
        Footers = <>
        Width = 30
      end
      item
        EditButtons = <>
        FieldName = 'cNmLocalEstoque'
        Footers = <>
        ReadOnly = True
        Width = 216
      end
      item
        EditButtons = <>
        FieldName = 'cUnidadeMedidaCompra'
        Footers = <>
        ReadOnly = True
        Width = 21
      end
      item
        EditButtons = <>
        FieldName = 'nFatorCompra'
        Footers = <>
        ReadOnly = True
        Width = 59
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeMinimaCompra'
        Footers = <>
        ReadOnly = True
        Width = 68
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeNecessaria'
        Footers = <>
        ReadOnly = True
        Width = 66
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeCompra'
        Footers = <>
        Width = 62
      end
      item
        EditButtons = <>
        FieldName = 'dDtPrevEntrega'
        Footers = <>
        Width = 66
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeFirme'
        Footers = <>
        Width = 67
      end
      item
        EditButtons = <>
        FieldName = 'nQtdePrev'
        Footers = <>
        Visible = False
        Width = 60
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeEstoque'
        Footers = <>
        ReadOnly = True
        Visible = False
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'nTempEstoque'
        Footers = <>
        ReadOnly = True
        Visible = False
        Width = 75
      end
      item
        EditButtons = <>
        FieldName = 'nConsumoMedDia'
        Footers = <>
        ReadOnly = True
        Visible = False
        Width = 60
      end
      item
        EditButtons = <>
        FieldName = 'nQtdePedido'
        Footers = <>
        ReadOnly = True
        Visible = False
        Width = 74
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StaticText1: TStaticText [16]
    Left = 8
    Top = 296
    Width = 1065
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Insumos do Item'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 8
    Visible = False
  end
  object cxTextEdit2: TcxTextEdit [17]
    Left = 616
    Top = 329
    Width = 25
    Height = 21
    Properties.ReadOnly = True
    Style.Color = clYellow
    StyleDisabled.Color = clBlue
    TabOrder = 9
    Visible = False
  end
  object edtUnidadeMedidaCompra: TEdit [18]
    Tag = 1
    Left = 136
    Top = 56
    Width = 41
    Height = 21
    Enabled = False
    ReadOnly = True
    TabOrder = 10
  end
  object edtProduto: TEdit [19]
    Tag = 1
    Left = 136
    Top = 32
    Width = 641
    Height = 21
    Enabled = False
    ReadOnly = True
    TabOrder = 11
  end
  object edtFatorCompra: TEdit [20]
    Tag = 1
    Left = 272
    Top = 56
    Width = 57
    Height = 21
    Enabled = False
    ReadOnly = True
    TabOrder = 12
  end
  object edtQtdeMinimaCompra: TEdit [21]
    Tag = 1
    Left = 464
    Top = 56
    Width = 57
    Height = 21
    Enabled = False
    ReadOnly = True
    TabOrder = 13
  end
  object edtUnidadeMedida1: TEdit [22]
    Tag = 1
    Left = 224
    Top = 80
    Width = 41
    Height = 21
    Enabled = False
    ReadOnly = True
    TabOrder = 14
  end
  object edtUnidadeMedidaCompra2: TEdit [23]
    Tag = 1
    Left = 224
    Top = 104
    Width = 41
    Height = 21
    Enabled = False
    ReadOnly = True
    TabOrder = 15
  end
  object edtQtdeParaPedido: TcxCurrencyEdit [24]
    Left = 136
    Top = 104
    Width = 81
    Height = 21
    ParentFont = False
    Properties.DisplayFormat = '0.00;-0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    TabOrder = 2
    OnExit = edtQtdeRealExit
  end
  object GroupBox1: TGroupBox [25]
    Left = 616
    Top = 168
    Width = 225
    Height = 121
    Caption = 'Quantidade de Pedidos'
    TabOrder = 4
    object Label6: TLabel
      Left = 21
      Top = 77
      Width = 84
      Height = 13
      Caption = 'Quantidade Itens'
    end
    object Label5: TLabel
      Left = 11
      Top = 53
      Width = 95
      Height = 13
      Caption = '# Pedidos Previstos'
    end
    object Label4: TLabel
      Left = 21
      Top = 29
      Width = 82
      Height = 13
      Caption = '# Pedidos Firmes'
    end
    object edtQtdeItens: TcxCurrencyEdit
      Left = 112
      Top = 69
      Width = 57
      Height = 21
      ParentFont = False
      Properties.DisplayFormat = ',0.00;-,0.00'
      Properties.ValidateOnEnter = True
      Style.BorderStyle = ebsFlat
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Consolas'
      Style.Font.Style = []
      TabOrder = 2
    end
    object edtPedidoPrev: TcxMaskEdit
      Left = 112
      Top = 45
      Width = 41
      Height = 21
      ParentFont = False
      Properties.EditMask = '!99999;1; '
      Properties.MaxLength = 0
      Style.BorderStyle = ebsFlat
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Consolas'
      Style.Font.Style = []
      TabOrder = 1
      Text = '     '
    end
    object edtPedidoFirme: TcxMaskEdit
      Left = 112
      Top = 21
      Width = 41
      Height = 21
      ParentFont = False
      Properties.EditMask = '!99999;1; '
      Properties.MaxLength = 0
      Style.BorderStyle = ebsFlat
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Consolas'
      Style.Font.Style = []
      TabOrder = 0
      Text = '     '
    end
    object edtUnidadeMedidaCompra3: TEdit
      Tag = 1
      Left = 176
      Top = 69
      Width = 41
      Height = 21
      Enabled = False
      ReadOnly = True
      TabOrder = 3
    end
  end
  object MaskEdit6: TMaskEdit [26]
    Left = 136
    Top = 128
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnEnter = MaskEdit6Enter
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object DBEdit1: TDBEdit [27]
    Tag = 1
    Left = 208
    Top = 128
    Width = 377
    Height = 21
    DataField = 'cNmLocalEstoque'
    DataSource = DataSource1
    TabOrder = 16
  end
  object GroupBox2: TGroupBox [28]
    Left = 8
    Top = 592
    Width = 1065
    Height = 73
    Caption = 'Dados Adicionais'
    TabOrder = 17
    Visible = False
    object Label8: TLabel
      Left = 472
      Top = 24
      Width = 67
      Height = 13
      Caption = 'Estoque Atual'
      FocusControl = DBEdit2
    end
    object Label16: TLabel
      Left = 584
      Top = 24
      Width = 127
      Height = 13
      Caption = 'Dura'#231#227'o do Estoque (dias)'
      FocusControl = DBEdit3
    end
    object Label17: TLabel
      Left = 728
      Top = 24
      Width = 132
      Height = 13
      Caption = 'Consumo M'#233'dio Di'#225'rio (p'#231's)'
      FocusControl = DBEdit4
    end
    object Label18: TLabel
      Left = 872
      Top = 24
      Width = 183
      Height = 13
      Caption = 'Quantidade Aguardando Recebimento'
      FocusControl = DBEdit5
    end
    object Label19: TLabel
      Left = 16
      Top = 24
      Width = 33
      Height = 13
      Caption = 'C'#243'digo'
      FocusControl = DBEdit6
    end
    object Label20: TLabel
      Left = 88
      Top = 24
      Width = 46
      Height = 13
      Caption = 'Descri'#231#227'o'
      FocusControl = DBEdit7
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 472
      Top = 40
      Width = 100
      Height = 21
      DataField = 'nQtdeEstoque'
      DataSource = dsFormula
      TabOrder = 0
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 584
      Top = 40
      Width = 100
      Height = 21
      DataField = 'nTempEstoque'
      DataSource = dsFormula
      TabOrder = 1
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 728
      Top = 40
      Width = 100
      Height = 21
      DataField = 'nConsumoMedDia'
      DataSource = dsFormula
      TabOrder = 2
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 872
      Top = 40
      Width = 100
      Height = 21
      DataField = 'nQtdePedido'
      DataSource = dsFormula
      TabOrder = 3
    end
    object DBEdit6: TDBEdit
      Tag = 1
      Left = 16
      Top = 40
      Width = 65
      Height = 21
      DataField = 'nCdProduto'
      DataSource = dsFormula
      TabOrder = 4
    end
    object DBEdit7: TDBEdit
      Tag = 1
      Left = 88
      Top = 40
      Width = 377
      Height = 21
      DataField = 'cNmProduto'
      DataSource = dsFormula
      TabOrder = 5
    end
  end
  object edtPrevEntrega: TDateTimePicker [29]
    Left = 464
    Top = 104
    Width = 129
    Height = 21
    Date = 39868.000000000000000000
    Time = 39868.000000000000000000
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 18
  end
  inherited ImageList1: TImageList
    Left = 392
    Top = 296
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C00636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF81C00000FE00FFFF01800000
      FE00C00300000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010181000000018001818100000003800181810000
      0077C00381810000007FFFFF8383000000000000000000000000000000000000
      000000000000}
  end
  object qryFornecedores: TADOQuery
    Connection = frmMenu.Connection
    EnableBCD = False
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdProdutoPai int'
      '       ,@nCdProduto    int'
      ''
      'Set @nCdProduto = :nPK'
      ''
      'IF (OBJECT_ID('#39'tempdb..#Temp_Fornecedores'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #Temp_Fornecedores (nCdTerceiro    int'
      '                                    ,cNmTerceiro    varchar(50)'
      
        '                                    ,nPercentCompra decimal(12,2' +
        ')'
      '                                    ,cNrContrato    varchar(50)'
      '                                    ,nCdProduto     int'
      
        '                                    ,nPrecoUnit     decimal(14,6' +
        ')'
      '                                    ,nCdProdutoFornecedor int)'
      ''
      'END'
      ''
      'TRUNCATE TABLE #Temp_Fornecedores'
      ''
      'IF EXISTS(SELECT 1'
      '            FROM ProdutoFornecedor'
      '           WHERE nCdProduto = @nCdProduto)'
      'BEGIN'
      ''
      '    INSERT INTO #Temp_Fornecedores'
      '              (nCdTerceiro'
      '              ,cNmTerceiro'
      '              ,nPercentCompra'
      '              ,cNrContrato'
      '              ,nCdProduto'
      '              ,nPrecoUnit'
      '              ,nCdProdutoFornecedor)'
      '        SELECT Terceiro.nCdTerceiro'
      '              ,Terceiro.cNmTerceiro'
      '              ,nPercentCompra'
      '              ,cNrContrato'
      '              ,nCdProduto'
      '              ,nPrecoUnit'
      '              ,nCdProdutoFornecedor'
      '          FROM ProdutoFornecedor'
      
        '               INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Pro' +
        'dutoFornecedor.nCdTerceiro'
      '         WHERE nCdProduto                 = @nCdProduto'
      '           --AND nCdTipoRessuprimentoFornec = 2'
      ''
      'END'
      'ELSE'
      'BEGIN'
      ''
      '    INSERT INTO #Temp_Fornecedores'
      '              (nCdTerceiro'
      '              ,cNmTerceiro'
      '              ,nPercentCompra'
      '              ,cNrContrato'
      '              ,nCdProduto'
      '              ,nPrecoUnit'
      '              ,nCdProdutoFornecedor)'
      '        SELECT Terceiro.nCdTerceiro'
      '              ,Terceiro.cNmTerceiro'
      '              ,nPercentCompra'
      '              ,cNrContrato'
      '              ,@nCdProduto'
      '              ,nPrecoUnit'
      '              ,nCdProdutoFornecedor'
      '          FROM ProdutoFornecedor'
      
        '               INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Pro' +
        'dutoFornecedor.nCdTerceiro'
      '         WHERE nCdProduto = (SELECT nCdProdutoPai'
      '                               FROM Produto'
      '                              WHERE nCdProduto = @nCdProduto)'
      ''
      'END'
      ''
      '/*'
      'INSERT INTO #Temp_Fornecedores'
      '          (nCdTerceiro'
      '          ,cNmTerceiro'
      '          ,nPercentCompra'
      '          ,cNrContrato'
      '          ,nCdProduto'
      '          ,nPrecoUnit'
      '          ,nCdProdutoFornecedor)'
      '    SELECT Terceiro.nCdTerceiro'
      '          ,Terceiro.cNmTerceiro'
      '          ,nPercentCompra'
      '          ,cNrContrato'
      '          ,FormulaProduto.nCdProduto'
      '          ,ProdutoFornecedor.nPrecoUnit'
      '          ,nCdProdutoFornecedor'
      '      FROM FormulaProduto'
      
        '           INNER JOIN ProdutoFornecedor ON ProdutoFornecedor.nCd' +
        'Produto = FormulaProduto.nCdProduto'
      
        '           INNER JOIN Terceiro          ON Terceiro.nCdTerceiro ' +
        '        = ProdutoFornecedor.nCdTerceiro'
      '     WHERE FormulaProduto.nCdProdutoPai = @nCdProduto'
      ''
      'INSERT INTO #Temp_Fornecedores'
      '          (nCdTerceiro'
      '          ,cNmTerceiro'
      '          ,nPercentCompra'
      '          ,cNrContrato'
      '          ,nCdProduto'
      '          ,nPrecoUnit'
      '          ,nCdProdutoFornecedor)'
      '    SELECT Terceiro.nCdTerceiro'
      '          ,Terceiro.cNmTerceiro'
      '          ,nPercentCompra'
      '          ,cNrContrato'
      '          ,FormulaProduto.nCdProduto'
      '          ,ProdutoFornecedor.nPrecoUnit'
      '          ,nCdProdutoFornecedor'
      '      FROM FormulaProduto'
      
        '           INNER JOIN ProdutoFornecedor ON ProdutoFornecedor.nCd' +
        'Produto = FormulaProduto.nCdProduto'
      
        '           INNER JOIN Terceiro          ON Terceiro.nCdTerceiro ' +
        '        = ProdutoFornecedor.nCdTerceiro'
      
        '     WHERE FormulaProduto.nCdProdutoPai = (SELECT nCdProdutoPai ' +
        'FROM Produto WHERE nCdProduto = @nCdProduto)'
      '*/'
      ''
      'SELECT *'
      '  FROM #Temp_Fornecedores'
      ' WHERE nCdProduto = @nCdProduto')
    Left = 712
    Top = 72
    object qryFornecedoresnCdTerceiro: TIntegerField
      DisplayLabel = 'Fornecedor|C'#243'd'
      FieldName = 'nCdTerceiro'
    end
    object qryFornecedorescNmTerceiro: TStringField
      DisplayLabel = 'Fornecedor|Nome'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryFornecedoresnPercentCompra: TBCDField
      DisplayLabel = '% Compra'
      FieldName = 'nPercentCompra'
      Precision = 12
      Size = 2
    end
    object qryFornecedorescNrContrato: TStringField
      DisplayLabel = 'N'#250'mero|Contrato'
      FieldName = 'cNrContrato'
      Size = 15
    end
    object qryFornecedoresnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryFornecedoresnPrecoUnit: TFloatField
      DisplayLabel = 'Pre'#231'o Custo|Unit'#225'rio'
      FieldName = 'nPrecoUnit'
    end
  end
  object dsFornecedores: TDataSource
    DataSet = qryFornecedores
    Left = 776
    Top = 72
  end
  object qryFormula: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryFormulaBeforePost
    OnCalcFields = qryFormulaCalcFields
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nQtdeCompra'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdProdutoPai int'
      '       ,@nCdProduto    int'
      '       ,@nQtdeCompra   decimal(12,2)'
      '       ,@nCdEmpresa    int'
      '       ,@nCdLoja       int'
      ''
      'Set @nCdEmpresa  = :nCdEmpresa'
      'Set @nCdProduto  = :nCdProduto'
      'Set @nQtdeCompra = :nQtdeCompra'
      'Set @nCdLoja     = :nCdLoja'
      ''
      'IF (OBJECT_ID('#39'tempdb..#Temp_Formula_Insumo'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #Temp_Formula_Insumo (nCdProduto              i' +
        'nt'
      
        '                                      ,cNmProduto              v' +
        'archar(150)'
      
        '                                      ,nQtdeNecessaria         D' +
        'ECIMAL(12,2)'
      
        '                                      ,nQtdeCompra             D' +
        'ECIMAL(12,2)'
      
        '                                      ,nQtdeFirme              D' +
        'ECIMAL(12,2)'
      
        '                                      ,nQtdePrev               D' +
        'ECIMAL(12,2)'
      
        '                                      ,nQtdeEstoque            D' +
        'ECIMAL(12,2)'
      
        '                                      ,nTempEstoque            i' +
        'nt'
      
        '                                      ,nConsumoMedDia          D' +
        'ECIMAL(12,2)'
      
        '                                      ,dDtPrevEntrega          D' +
        'ATETIME'
      
        '                                      ,nQtdePedido             D' +
        'ECIMAL(12,2)'
      
        '                                      ,cFlgFornecHom           i' +
        'nt'
      
        '                                      ,cUnidadeMedidaCompra    c' +
        'har(3)'
      
        '                                      ,nFatorCompra            d' +
        'ecimal(12,2)'
      
        '                                      ,nQtdeMinimaCompra       d' +
        'ecimal(12,2)'
      
        '                                      ,cUnidadeMedida          c' +
        'har(3)'
      
        '                                      ,nCdLocalEstoqueEntrega  i' +
        'nt)'
      '                        '
      ''
      'END'
      ''
      'TRUNCATE TABLE #Temp_Formula_Insumo'
      ''
      '--'
      '-- Inseri os produtos da formula direto do produto'
      '--'
      'INSERT INTO #Temp_Formula_Insumo (nCdProduto'
      '                          ,cNmProduto'
      '                          ,nQtdeNecessaria'
      '                          ,nQtdeCompra'
      '                          ,nQtdeFirme'
      '                          ,nQtdePrev'
      '                          ,nQtdeEstoque'
      '                          ,nTempEstoque'
      '                          ,nConsumoMedDia'
      '                          ,dDtPrevEntrega'
      '                          ,nQtdePedido'
      '                          ,cFlgFornecHom'
      '                          ,cUnidadeMedidaCompra'
      '                          ,nFatorCompra'
      '                          ,nQtdeMinimaCompra'
      '                          ,cUnidadeMedida)'
      #9#9#9#9#9'SELECT FormulaProduto.nCdProduto'
      #9#9#9#9#9#9'  ,Produto.cNmProduto'
      
        #9#9#9#9#9#9'  ,Convert(DECIMAL(12,2),(FormulaProduto.nQtde * @nQtdeCom' +
        'pra))'
      
        #9#9#9#9#9#9'  ,Convert(DECIMAL(12,2),(FormulaProduto.nQtde * @nQtdeCom' +
        'pra))'
      
        #9#9#9#9#9#9'  ,Convert(DECIMAL(12,2),(FormulaProduto.nQtde * @nQtdeCom' +
        'pra))'
      #9#9#9#9#9#9'  ,0.00'
      #9#9#9#9#9#9'  ,Produto.nQtdeEstoque'
      
        #9#9#9#9#9#9'  ,CASE WHEN nQtdeEstoque   <= 0 OR nConsumoMedDia <= 0   ' +
        '                                THEN 0'
      
        #9#9#9#9#9#9#9#9'WHEN nConsumoMedDia <> 0 AND nQtdeEstoque <> 0 AND nQtde' +
        'Estoque >= nConsumoMedDia THEN Convert(int,(nQtdeEstoque / nCons' +
        'umoMedDia))'
      
        #9#9#9#9#9#9#9#9'WHEN nConsumoMedDia <> 0 AND nQtdeEstoque <> 0 AND nQtde' +
        'Estoque < nConsumoMedDia  THEN 0'
      #9#9#9#9#9#9'   END'
      #9#9#9#9#9#9'  ,nConsumoMedDia'
      
        #9#9#9#9#9#9'  ,CASE WHEN iTipoEntrega = 0 AND iDiaEntrega > 0 THEN dbo' +
        '.fn_DiaEntregaContrato(iDiaEntrega)'
      #9#9#9#9#9#9#9#9'WHEN iTipoEntrega = 1 AND iDiaEntrega = 0 THEN NULL'
      
        #9#9#9#9#9#9#9#9'WHEN iTipoEntrega = 1 AND iDiaEntrega > 0 THEN dbo.fn_On' +
        'lyDate(GetDate()) + iDiaEntrega'
      #9#9#9#9#9#9'   END'
      #9#9#9#9#9#9'  ,IsNull((SELECT Sum(nQtdePed - nQtdeExpRec - nQtdeCanc)'
      #9#9#9#9#9#9#9#9#9' FROM ItemPedido'
      
        '                                          INNER JOIN Pedido     ' +
        'ON Pedido.nCdPedido         = ItemPedido.nCdPedido'
      
        '                                          INNER JOIN TipoPedido ' +
        'ON TipoPedido.nCdTipoPedido = Pedido.nCdTipoPedido'
      
        '                                    WHERE ItemPedido.nCdProduto ' +
        ' = FormulaProduto.nCdProduto'
      
        '                                      AND TipoPedido.cFlgCompra ' +
        ' = 1'
      
        '                                      AND (nQtdePed-nQtdeExpRec-' +
        'nQtdeCanc)  > 0'
      
        '                                      AND Pedido.dDtAutor       ' +
        'IS NOT NULL'
      
        '                                      AND Pedido.nCdEmpresa     ' +
        ' = @nCdEmpresa'
      
        '                                      AND ((Pedido.nCdLoja = @nC' +
        'dLoja) OR (@nCdLoja = 0))),0)'
      
        #9#9#9#9#9#9'  ,CASE WHEN EXISTS(SELECT 1 FROM ProdutoFornecedor WHERE ' +
        'ProdutoFornecedor.nCdProduto = FormulaProduto.nCdProduto OR Prod' +
        'utoFornecedor.nCdProduto = Produto.nCdProdutoPai) THEN 1'
      #9#9#9#9#9#9#9#9'ELSE 0'
      #9#9#9#9#9#9'   END'
      '              ,cUnidadeMedidaCompra'
      '              ,nFatorCompra'
      '              ,nQtdeMinimaCompra'
      '              ,cUnidadeMedida'
      #9#9#9#9'  FROM FormulaProduto'
      
        #9#9#9#9#9#9'   INNER JOIN Produto ON Produto.nCdProduto           = Fo' +
        'rmulaProduto.nCdProduto'
      '         WHERE FormulaProduto.nCdProdutoPai = @nCdProduto'
      ''
      'Set @nCdProdutoPai = NULL'
      ''
      'SELECT @nCdProdutoPai = nCdProdutoPai'
      '  FROM Produto'
      ' WHERE nCdProduto = @nCdProduto'
      ''
      'IF (@nCdProdutoPai IS NOT NULL)'
      'BEGIN'
      ''
      #9'--'
      #9'-- Inseri os produtos da formula do produto pai do item'
      #9'--'
      #9'INSERT INTO #Temp_Formula_Insumo (nCdProduto'
      #9#9#9#9#9#9#9'  ,cNmProduto'
      #9#9#9#9#9#9#9'  ,nQtdeNecessaria'
      #9#9#9#9#9#9#9'  ,nQtdeCompra'
      #9#9#9#9#9#9#9'  ,nQtdeFirme'
      #9#9#9#9#9#9#9'  ,nQtdePrev'
      #9#9#9#9#9#9#9'  ,nQtdeEstoque'
      #9#9#9#9#9#9#9'  ,nTempEstoque'
      #9#9#9#9#9#9#9'  ,nConsumoMedDia'
      #9#9#9#9#9#9#9'  ,dDtPrevEntrega'
      #9#9#9#9#9#9#9'  ,nQtdePedido'
      #9#9#9#9#9#9#9'  ,cFlgFornecHom'
      #9#9#9#9#9#9#9'  ,cUnidadeMedidaCompra'
      #9#9#9#9#9#9#9'  ,nFatorCompra'
      #9#9#9#9#9#9#9'  ,nQtdeMinimaCompra'
      #9#9#9#9#9#9#9'  ,cUnidadeMedida)'
      #9#9#9#9#9#9'SELECT FormulaProduto.nCdProduto'
      #9#9#9#9#9#9#9'  ,Produto.cNmProduto'
      
        #9#9#9#9#9#9#9'  ,Convert(DECIMAL(12,2),(FormulaProduto.nQtde * @nQtdeCo' +
        'mpra))'
      
        #9#9#9#9#9#9#9'  ,Convert(DECIMAL(12,2),(FormulaProduto.nQtde * @nQtdeCo' +
        'mpra))'
      
        #9#9#9#9#9#9#9'  ,Convert(DECIMAL(12,2),(FormulaProduto.nQtde * @nQtdeCo' +
        'mpra))'
      #9#9#9#9#9#9#9'  ,0.00'
      #9#9#9#9#9#9#9'  ,Produto.nQtdeEstoque'
      
        #9#9#9#9#9#9#9'  ,CASE WHEN nQtdeEstoque   <= 0 OR nConsumoMedDia <= 0  ' +
        '                                 THEN 0'
      
        #9#9#9#9#9#9#9#9#9'WHEN nConsumoMedDia <> 0 AND nQtdeEstoque <> 0 AND nQtd' +
        'eEstoque >= nConsumoMedDia THEN Convert(int,(nQtdeEstoque / nCon' +
        'sumoMedDia))'
      
        #9#9#9#9#9#9#9#9#9'WHEN nConsumoMedDia <> 0 AND nQtdeEstoque <> 0 AND nQtd' +
        'eEstoque < nConsumoMedDia  THEN 0'
      #9#9#9#9#9#9#9'   END'
      #9#9#9#9#9#9#9'  ,nConsumoMedDia'
      
        #9#9#9#9#9#9#9'  ,CASE WHEN iTipoEntrega = 0 AND iDiaEntrega > 0 THEN db' +
        'o.fn_DiaEntregaContrato(iDiaEntrega)'
      #9#9#9#9#9#9#9#9#9'WHEN iTipoEntrega = 1 AND iDiaEntrega = 0 THEN NULL'
      
        #9#9#9#9#9#9#9#9#9'WHEN iTipoEntrega = 1 AND iDiaEntrega > 0 THEN dbo.fn_O' +
        'nlyDate(GetDate()) + iDiaEntrega'
      #9#9#9#9#9#9#9'   END'
      #9#9#9#9#9#9#9'  ,IsNull((SELECT Sum(nQtdePed - nQtdeExpRec - nQtdeCanc)'
      #9#9#9#9#9#9#9#9#9#9' FROM ItemPedido'
      
        #9#9#9#9#9#9#9#9#9#9#9'  INNER JOIN Pedido     ON Pedido.nCdPedido         =' +
        ' ItemPedido.nCdPedido'
      
        #9#9#9#9#9#9#9#9#9#9#9'  INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido =' +
        ' Pedido.nCdTipoPedido'
      
        #9#9#9#9#9#9#9#9#9#9'WHERE ItemPedido.nCdProduto  = FormulaProduto.nCdProdu' +
        'to'
      #9#9#9#9#9#9#9#9#9#9'  AND TipoPedido.cFlgCompra  = 1'
      #9#9#9#9#9#9#9#9#9#9'  AND (nQtdePed-nQtdeExpRec-nQtdeCanc)  > 0'
      #9#9#9#9#9#9#9#9#9#9'  AND Pedido.dDtAutor       IS NOT NULL'
      #9#9#9#9#9#9#9#9#9#9'  AND Pedido.nCdEmpresa      = @nCdEmpresa'
      
        #9#9#9#9#9#9#9#9#9#9'  AND ((Pedido.nCdLoja = @nCdLoja) OR (@nCdLoja = 0)))' +
        ',0)'
      
        #9#9#9#9#9#9#9'  ,CASE WHEN EXISTS(SELECT 1 FROM ProdutoFornecedor WHERE' +
        ' ProdutoFornecedor.nCdProduto = FormulaProduto.nCdProduto OR Pro' +
        'dutoFornecedor.nCdProduto = Produto.nCdProdutoPai) THEN 1'
      #9#9#9#9#9#9#9#9#9'ELSE 0'
      #9#9#9#9#9#9#9'   END'
      #9#9#9#9'  ,cUnidadeMedidaCompra'
      #9#9#9#9'  ,nFatorCompra'
      #9#9#9#9'  ,nQtdeMinimaCompra'
      #9#9#9#9'  ,cUnidadeMedida'
      #9#9#9#9#9'  FROM FormulaProduto'
      
        #9#9#9#9#9#9#9'   INNER JOIN Produto ON Produto.nCdProduto           = F' +
        'ormulaProduto.nCdProduto'
      #9#9#9' WHERE FormulaProduto.nCdProdutoPai = @nCdProdutoPai'
      
        #9#9#9'   AND NOT EXISTS(SELECT 1 FROM #Temp_Formula_Insumo Temp WHE' +
        'RE Temp.nCdProduto = FormulaProduto.nCdProduto)'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #Temp_Formula_Insumo'
      ''
      '')
    Left = 640
    Top = 504
    object qryFormulanCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryFormulacNmProduto: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryFormulanQtdeNecessaria: TBCDField
      DisplayLabel = 'Quantidades|Necess'#225'ria'
      FieldName = 'nQtdeNecessaria'
      Precision = 25
    end
    object qryFormulanQtdeCompra: TBCDField
      DisplayLabel = 'Quantidades|a Comprar'
      FieldName = 'nQtdeCompra'
      Precision = 25
    end
    object qryFormulanQtdeEstoque: TBCDField
      DisplayLabel = 'Estoque|Atual'
      FieldName = 'nQtdeEstoque'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryFormulanQtdePedido: TBCDField
      DisplayLabel = 'Pedido|Aprov.'
      FieldName = 'nQtdePedido'
      DisplayFormat = '#,##0.00'
      Precision = 32
    end
    object qryFormulanTempEstoque: TIntegerField
      DisplayLabel = 'Estoque|Dura'#231#227'o (dias)'
      FieldName = 'nTempEstoque'
      DisplayFormat = '#,##0.00'
    end
    object qryFormulanConsumoMedDia: TBCDField
      DisplayLabel = 'Estoque|Cons. M'#233'd.'
      FieldName = 'nConsumoMedDia'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryFormuladDtPrevEntrega: TDateTimeField
      DisplayLabel = 'Previs'#227'o|Entrega'
      FieldName = 'dDtPrevEntrega'
      EditMask = '99/99/9999;1;_'
    end
    object qryFormulacFlgFornecHom: TIntegerField
      FieldName = 'cFlgFornecHom'
    end
    object qryFormulanQtdeFirme: TBCDField
      DisplayLabel = 'Quantidade|Px Pedidos'
      FieldName = 'nQtdeFirme'
      Precision = 25
    end
    object qryFormulanQtdePrev: TBCDField
      DisplayLabel = 'Px Pedidos|Previs'#227'o'
      FieldName = 'nQtdePrev'
      Precision = 25
    end
    object qryFormulacUnidadeMedidaCompra: TStringField
      DisplayLabel = 'Informa'#231#245'es de Compra|UM'
      FieldName = 'cUnidadeMedidaCompra'
      FixedChar = True
      Size = 3
    end
    object qryFormulanFatorCompra: TBCDField
      DisplayLabel = 'Informa'#231#245'es de Compra|Fator'
      FieldName = 'nFatorCompra'
      Precision = 12
      Size = 2
    end
    object qryFormulanQtdeMinimaCompra: TBCDField
      DisplayLabel = 'Informa'#231#245'es de Compra|Qtde M'#237'nima'
      FieldName = 'nQtdeMinimaCompra'
      Precision = 12
      Size = 2
    end
    object qryFormulacUnidadeMedida: TStringField
      DisplayLabel = 'Produto|UM'
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryFormulanCdLocalEstoqueEntrega: TIntegerField
      DisplayLabel = 'Local Estoque Entrega|C'#243'd'
      FieldName = 'nCdLocalEstoqueEntrega'
    end
    object qryFormulacNmLocalEstoque: TStringField
      DisplayLabel = 'Local Estoque Entrega|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmLocalEstoque'
      Size = 50
      Calculated = True
    end
  end
  object dsFormula: TDataSource
    DataSet = qryFormula
    Left = 672
    Top = 504
  end
  object PopupMenu1: TPopupMenu
    Left = 232
    Top = 512
    object FornecedoresHomologados1: TMenuItem
      Caption = 'Fornecedores Homologados'
      OnClick = FornecedoresHomologados1Click
    end
    object PedidosemAberto1: TMenuItem
      Caption = 'Pedidos em Aberto'
      OnClick = PedidosemAberto1Click
    end
    object ltimosPedidos1: TMenuItem
      Caption = #218'ltimos Pedidos'
      OnClick = ltimosPedidos1Click
    end
    object DetalharEstoqueAtual1: TMenuItem
      Caption = 'Detalhar Estoque Atual'
      OnClick = DetalharEstoqueAtual1Click
    end
    object BalanceamentoFornecedores1: TMenuItem
      Caption = 'Balanceamento Fornecedores'
      OnClick = BalanceamentoFornecedores1Click
    end
    object VerCadastrodoProduto1: TMenuItem
      Caption = 'Cadastro do Produto'
      OnClick = VerCadastrodoProduto1Click
    end
  end
  object qryConferePercentCompra: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Fornecedores'#39') IS NULL)'
      'BEGIN'
      ''
      '    SELECT TOP 0 Terceiro.nCdTerceiro'
      '          ,Terceiro.cNmTerceiro'
      '          ,nPercentCompra'
      '          ,cNrContrato'
      '          ,ProdutoFornecedor.nCdProduto'
      '      INTO #Temp_Fornecedores'
      '      FROM ProdutoFornecedor'
      
        '           INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Produto' +
        'Fornecedor.nCdTerceiro'
      ''
      ''
      'END'
      ''
      'SELECT nCdProduto'
      '      ,Sum(nPercentCompra) nPercentCompra'
      '  FROM #Temp_Fornecedores'
      ' GROUP BY nCdProduto')
    Left = 776
    Top = 104
    object qryConferePercentCompranCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryConferePercentCompranPercentCompra: TBCDField
      FieldName = 'nPercentCompra'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object SP_GERA_PEDIDO_MRP: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_PEDIDO_MRP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nQtdeComprar'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@dDtEntregaPrev'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@iPedidoFirme'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iPedidoPrev'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nQtdeFirme'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLocalEstoqueEntrega'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 744
    Top = 104
  end
  object qryPrepara_Temp_Fornecedores: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdProdutoPai int'
      '       ,@nCdProduto    int'
      ''
      'Set @nCdProduto = :nPK'
      ''
      'SELECT @nCdProdutoPai = nCdProdutoPai'
      '  FROM Produto'
      ' WHERE nCdProduto = @nCdProduto'
      ''
      '--Set @nCdProduto = IsNull(@nCdProdutoPai,@nCdProduto)'
      ''
      'IF (OBJECT_ID('#39'tempdb..#Temp_Fornecedores'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #Temp_Fornecedores (nCdTerceiro    int'
      '                                    ,cNmTerceiro    varchar(50)'
      
        '                                    ,nPercentCompra decimal(12,2' +
        ')'
      '                                    ,cNrContrato    varchar(50)'
      '                                    ,nCdProduto     int'
      
        '                                    ,nPrecoUnit     decimal(12,4' +
        ')'
      '                                    ,nCdProdutoFornecedor int)'
      ''
      'END'
      ''
      'TRUNCATE TABLE #Temp_Fornecedores'
      ''
      'IF EXISTS(SELECT 1'
      '            FROM ProdutoFornecedor'
      '           WHERE nCdProduto = @nCdProduto)'
      'BEGIN'
      ''
      '    INSERT INTO #Temp_Fornecedores'
      '              (nCdTerceiro'
      '              ,cNmTerceiro'
      '              ,nPercentCompra'
      '              ,cNrContrato'
      '              ,nCdProduto'
      '              ,nPrecoUnit'
      '              ,nCdProdutoFornecedor)'
      '        SELECT Terceiro.nCdTerceiro'
      '              ,Terceiro.cNmTerceiro'
      '              ,nPercentCompra'
      '              ,cNrContrato'
      '              ,nCdProduto'
      '              ,nPrecoUnit'
      '              ,nCdProdutoFornecedor'
      '          FROM ProdutoFornecedor'
      
        '               INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Pro' +
        'dutoFornecedor.nCdTerceiro'
      '         WHERE nCdProduto = @nCdProduto'
      ''
      'END'
      'ELSE'
      'BEGIN'
      ''
      '    INSERT INTO #Temp_Fornecedores'
      '              (nCdTerceiro'
      '              ,cNmTerceiro'
      '              ,nPercentCompra'
      '              ,cNrContrato'
      '              ,nCdProduto'
      '              ,nPrecoUnit'
      '              ,nCdProdutoFornecedor)'
      '        SELECT Terceiro.nCdTerceiro'
      '              ,Terceiro.cNmTerceiro'
      '              ,nPercentCompra'
      '              ,cNrContrato'
      '              ,@nCdProduto'
      '              ,nPrecoUnit'
      '              ,nCdProdutoFornecedor'
      '          FROM ProdutoFornecedor'
      
        '               INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Pro' +
        'dutoFornecedor.nCdTerceiro'
      '         WHERE nCdProduto = (SELECT nCdProdutoPai'
      '                               FROM Produto'
      '                              WHERE nCdProduto = @nCdProduto)'
      ''
      'END'
      ''
      ''
      'INSERT INTO #Temp_Fornecedores'
      '          (nCdTerceiro'
      '          ,cNmTerceiro'
      '          ,nPercentCompra'
      '          ,cNrContrato'
      '          ,nCdProduto'
      '          ,nPrecoUnit'
      '          ,nCdProdutoFornecedor)'
      '    SELECT Terceiro.nCdTerceiro'
      '          ,Terceiro.cNmTerceiro'
      '          ,nPercentCompra'
      '          ,cNrContrato'
      '          ,FormulaProduto.nCdProduto'
      '          ,ProdutoFornecedor.nPrecoUnit'
      '          ,nCdProdutoFornecedor'
      '      FROM FormulaProduto'
      
        '           INNER JOIN ProdutoFornecedor ON ProdutoFornecedor.nCd' +
        'Produto = FormulaProduto.nCdProduto'
      
        '           INNER JOIN Terceiro          ON Terceiro.nCdTerceiro ' +
        '        = ProdutoFornecedor.nCdTerceiro'
      '     WHERE FormulaProduto.nCdProdutoPai = @nCdProduto'
      ''
      ''
      'INSERT INTO #Temp_Fornecedores'
      '          (nCdTerceiro'
      '          ,cNmTerceiro'
      '          ,nPercentCompra'
      '          ,cNrContrato'
      '          ,nCdProduto'
      '          ,nPrecoUnit'
      '          ,nCdProdutoFornecedor)'
      '    SELECT Terceiro.nCdTerceiro'
      '          ,Terceiro.cNmTerceiro'
      '          ,nPercentCompra'
      '          ,cNrContrato'
      '          ,FormulaProduto.nCdProduto'
      '          ,ProdutoFornecedor.nPrecoUnit'
      '          ,nCdProdutoFornecedor'
      '      FROM FormulaProduto'
      
        '           INNER JOIN ProdutoFornecedor ON ProdutoFornecedor.nCd' +
        'Produto = FormulaProduto.nCdProduto'
      
        '           INNER JOIN Terceiro          ON Terceiro.nCdTerceiro ' +
        '        = ProdutoFornecedor.nCdTerceiro'
      
        '     WHERE FormulaProduto.nCdProdutoPai = (SELECT nCdProdutoPai ' +
        'FROM Produto WHERE nCdProduto = @nCdProduto)'
      '')
    Left = 712
    Top = 104
  end
  object cmdTempFormula: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#Temp_Formula_Insumo'#39') IS NULL)'#13#10'BEGIN'#13#10#13 +
      #10'    CREATE TABLE #Temp_Formula_Insumo (nCdProduto              ' +
      'int'#13#10'                                      ,cNmProduto          ' +
      '    varchar(150)'#13#10'                                      ,nQtdeNe' +
      'cessaria         DECIMAL(12,2)'#13#10'                                ' +
      '      ,nQtdeCompra             DECIMAL(12,2)'#13#10'                  ' +
      '                    ,nQtdeFirme              DECIMAL(12,2)'#13#10'    ' +
      '                                  ,nQtdePrev               DECIM' +
      'AL(12,2)'#13#10'                                      ,nQtdeEstoque   ' +
      '         DECIMAL(12,2)'#13#10'                                      ,n' +
      'TempEstoque            int'#13#10'                                    ' +
      '  ,nConsumoMedDia          DECIMAL(12,2)'#13#10'                      ' +
      '                ,dDtPrevEntrega          DATETIME'#13#10'             ' +
      '                         ,nQtdePedido             DECIMAL(12,2)'#13 +
      #10'                                      ,cFlgFornecHom           ' +
      'int'#13#10'                                      ,cUnidadeMedidaCompra' +
      '    char(3)'#13#10'                                      ,nFatorCompra' +
      '            decimal(12,2)'#13#10'                                     ' +
      ' ,nQtdeMinimaCompra       decimal(12,2)'#13#10'                       ' +
      '               ,cUnidadeMedida          char(3)'#13#10'               ' +
      '                       ,nCdLocalEstoqueEntrega  int)'#13#10'          ' +
      '              '#13#10#13#10'END'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 472
    Top = 504
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cUnidadeMedidaCompra'
      '      ,nFatorCompra'
      '      ,nQtdeMinimaCompra'
      '      ,cUnidadeMedida'
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK')
    Left = 744
    Top = 72
    object qryProdutocUnidadeMedidaCompra: TStringField
      FieldName = 'cUnidadeMedidaCompra'
      FixedChar = True
      Size = 3
    end
    object qryProdutonFatorCompra: TBCDField
      FieldName = 'nFatorCompra'
      Precision = 12
      Size = 2
    end
    object qryProdutonQtdeMinimaCompra: TBCDField
      FieldName = 'nQtdeMinimaCompra'
      Precision = 12
      Size = 2
    end
    object qryProdutocUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdProduto'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque, cNmLocalEstoque'
      'FROM LocalEstoque'
      'WHERE nCdEmpresa = :nCdEmpresa'
      'AND nCdLocalEstoque = :nPK'
      'AND EXISTS(SELECT 1'
      '             FROM GrupoProdutoLocalEstoque GPLE'
      
        '            WHERE GPLE.nCdLocalEstoque = LocalEstoque.nCdLocalEs' +
        'toque'
      
        '              AND GPLE.nCdGrupoProduto = (SELECT nCdGrupo_Produt' +
        'o'
      '                                            FROM Produto'
      
        '                                           WHERE Produto.nCdProd' +
        'uto = :nCdProduto))')
    Left = 376
    Top = 160
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLocalEstoque
    Left = 624
    Top = 376
  end
  object qryLocalizaEstoqueEntrega: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa         int'
      '       ,@nCdLoja            int'
      '       ,@nCdProduto         int'
      '       ,@nCdOperacaoEstoque int'
      '       ,@nCdGrupoProduto    int'
      '       ,@nCdLocalEstoque    int'
      '       ,@cNmGrupoProduto    varchar(50)'
      ''
      'Set @nCdEmpresa      = :nCdEmpresa'
      'Set @nCdLoja         = :nCdLoja'
      'Set @nCdProduto      = :nCdProduto'
      ''
      'IF (@nCdLoja = 0) Set @nCdLoja = NULL'
      ''
      '--'
      
        '-- Primeiro verifica no MRP se o local de entrega j'#225' est'#225' defini' +
        'do.'
      '--'
      'SELECT @nCdLocalEstoque = nCdLocalEstoqueEntrega'
      '  FROM ProdutoEmpresa'
      ' WHERE nCdProduto      = @nCdProduto'
      '   AND nCdEmpresa      = @nCdEmpresa'
      '   AND cFlgAtivoCompra = 1'
      
        '   AND ((@nCdLoja IS NULL AND nCdLoja IS NULL) OR (nCdLoja = @nC' +
        'dLoja))'
      ''
      'IF (@nCdLocalEstoque IS NOT NULL)'
      'BEGIN'
      '    SELECT @nCdLocalEstoque nCdLocalEstoque'
      '    RETURN'
      'END'
      ''
      '--'
      
        '-- Se n'#227'o encontrar no mrp, procura no vinculo entre opera'#231#227'o e ' +
        'local de estoque'
      '--'
      'SELECT @nCdOperacaoEstoque = Convert(int,cValor)'
      '  FROM Parametro'
      ' WHERE cParametro = '#39'OPERPADCOM'#39
      ''
      'Set @nCdGrupoProduto = NULL'
      ''
      'SELECT @nCdGrupoProduto = Produto.nCdGrupo_Produto'
      '      ,@cNmGrupoProduto = cNmGrupoProduto'
      '  FROM Produto'
      
        '       INNER JOIN GrupoProduto ON GrupoProduto.nCdGrupoProduto =' +
        ' Produto.nCdGrupo_Produto'
      ' WHERE nCdProduto = @nCdProduto'
      ''
      '--'
      '-- Descobre o Estoque que ir'#225' movimentar'
      '--'
      'IF (@nCdLoja IS NULL)'
      'BEGIN'
      ''
      #9'SELECT @nCdLocalEstoque = nCdLocalEstoque'
      #9'  FROM OperacaoLocalEstoque'
      #9' WHERE nCdEmpresa         = @nCdEmpresa'
      #9'   AND nCdLoja           IS NULL'
      #9'   AND nCdGrupoProduto    = @nCdGrupoProduto'
      #9'   AND nCdOperacaoEstoque = @nCdOperacaoEstoque'
      ''
      #9'IF (@nCdLocalEstoque IS NULL)'
      #9'BEGIN'
      ''
      #9#9'SELECT @nCdLocalEstoque = nCdLocalEstoque'
      #9#9'  FROM OperacaoLocalEstoque'
      #9#9' WHERE nCdEmpresa          = @nCdEmpresa'
      #9#9'   AND nCdLoja            IS NULL'
      #9#9'   AND nCdOperacaoEstoque  = @nCdOperacaoEstoque'
      ''
      #9'END'
      ''
      'END'
      'ELSE'
      'BEGIN'
      ''
      #9'SELECT @nCdLocalEstoque = nCdLocalEstoque'
      #9'  FROM OperacaoLocalEstoque'
      #9' WHERE nCdEmpresa         = @nCdEmpresa'
      #9'   AND nCdLoja            = @nCdLoja'
      #9'   AND nCdGrupoProduto    = @nCdGrupoProduto'
      #9'   AND nCdOperacaoEstoque = @nCdOperacaoEstoque'
      ''
      #9'IF (@nCdLocalEstoque IS NULL)'
      #9'BEGIN'
      ''
      #9#9'SELECT @nCdLocalEstoque = nCdLocalEstoque'
      #9#9'  FROM OperacaoLocalEstoque'
      #9#9' WHERE nCdEmpresa         = @nCdEmpresa'
      #9#9'   AND nCdLoja            = @nCdLoja'
      #9#9'   AND nCdOperacaoEstoque = @nCdOperacaoEstoque'
      ''
      #9'END'
      ''
      'END'
      ''
      'SELECT @nCdLocalEstoque nCdLocalEstoque')
    Left = 624
    Top = 120
    object qryLocalizaEstoqueEntreganCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
      ReadOnly = True
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 672
    Top = 88
  end
end
