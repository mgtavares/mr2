inherited frmConciliaBancoManual__ConsultaCheques: TfrmConciliaBancoManual__ConsultaCheques
  Left = 221
  Top = 176
  Width = 866
  BorderIcons = [biSystemMenu]
  Caption = 'Consulta de Cheques'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 850
  end
  inherited ToolBar1: TToolBar
    Width = 850
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 850
    Height = 435
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsCheques
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyPress = DBGridEh1KeyPress
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdBanco'
        Footers = <>
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'iNrCheque'
        Footers = <>
        Width = 85
      end
      item
        EditButtons = <>
        FieldName = 'nValCheque'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtRemessaPort'
        Footers = <>
        Width = 89
      end
      item
        EditButtons = <>
        FieldName = 'cCNPJCPF'
        Footers = <>
        Width = 116
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        Width = 370
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryCheques: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdContaBancariaDep'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'iNrCheque'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Cheque.nCdCheque'
      '      ,Cheque.nCdBanco'
      '      ,Cheque.iNrCheque'
      '      ,Cheque.nValCheque'
      '      ,Cheque.cCNPJCPF'
      '      ,cNmTerceiro'
      '      ,dDtDeposito'
      '      ,dDtRemessaPort'
      '  FROM Cheque'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Cheque.nCdT' +
        'erceiroResp'
      ' WHERE nCdContaBancariaDep = :nCdContaBancariaDep'
      '   AND iNrCheque           = :iNrCheque'
      '   AND nCdTabStatusCheque  IN (2,3,4,9)'
      ' ORDER BY dDtRemessaPort DESC')
    Left = 184
    Top = 104
    object qryChequesnCdCheque: TAutoIncField
      DisplayLabel = 'Cheques|'
      FieldName = 'nCdCheque'
      ReadOnly = True
    end
    object qryChequesnCdBanco: TIntegerField
      DisplayLabel = 'Cheques|Banco'
      FieldName = 'nCdBanco'
    end
    object qryChequesiNrCheque: TIntegerField
      DisplayLabel = 'Cheques|Nr. Cheque'
      FieldName = 'iNrCheque'
    end
    object qryChequesnValCheque: TBCDField
      DisplayLabel = 'Cheques|Valor'
      FieldName = 'nValCheque'
      Precision = 12
      Size = 2
    end
    object qryChequescCNPJCPF: TStringField
      DisplayLabel = 'Cheques|CNPJ/CPF'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequescNmTerceiro: TStringField
      DisplayLabel = 'Cheques|Cliente Respons'#225'vel'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryChequesdDtDeposito: TDateTimeField
      DisplayLabel = 'Cheques|Data Prev. Dep'#243'sito'
      FieldName = 'dDtDeposito'
    end
    object qryChequesdDtRemessaPort: TDateTimeField
      DisplayLabel = 'Cheques|Data Dep'#243'sito'
      FieldName = 'dDtRemessaPort'
    end
  end
  object dsCheques: TDataSource
    DataSet = qryCheques
    Left = 232
    Top = 104
  end
end
