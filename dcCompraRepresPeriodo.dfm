inherited dcmCompraRepresPeriodo: TdcmCompraRepresPeriodo
  Left = -8
  Top = -8
  Width = 1280
  Height = 786
  Caption = 'Data Analysis - Compras por Representante'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1264
    Height = 721
  end
  object Label1: TLabel [1]
    Left = 66
    Top = 72
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label3: TLabel [2]
    Left = 31
    Top = 208
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Compra'
  end
  object Label6: TLabel [3]
    Left = 188
    Top = 208
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label4: TLabel [4]
    Left = 38
    Top = 112
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
  end
  object Label7: TLabel [5]
    Left = 78
    Top = 136
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  object Label8: TLabel [6]
    Left = 82
    Top = 160
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Linha'
  end
  object Label9: TLabel [7]
    Left = 76
    Top = 184
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Classe'
  end
  object Label10: TLabel [8]
    Left = 35
    Top = 48
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Representante'
  end
  inherited ToolBar1: TToolBar
    Width = 1264
    TabOrder = 16
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [10]
    Tag = 1
    Left = 184
    Top = 64
    Width = 60
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 8
  end
  object DBEdit3: TDBEdit [11]
    Tag = 1
    Left = 248
    Top = 64
    Width = 645
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 9
  end
  object MaskEdit1: TMaskEdit [12]
    Left = 112
    Top = 200
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [13]
    Left = 208
    Top = 200
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [14]
    Left = 112
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object MaskEdit5: TMaskEdit [15]
    Left = 112
    Top = 104
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object DBEdit4: TDBEdit [16]
    Tag = 1
    Left = 184
    Top = 104
    Width = 650
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = dsDepartamento
    TabOrder = 10
  end
  object MaskEdit7: TMaskEdit [17]
    Left = 112
    Top = 128
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object DBEdit5: TDBEdit [18]
    Tag = 1
    Left = 184
    Top = 128
    Width = 650
    Height = 21
    DataField = 'cNmMarca'
    DataSource = dsMarca
    TabOrder = 11
  end
  object MaskEdit8: TMaskEdit [19]
    Left = 112
    Top = 152
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = MaskEdit8Exit
    OnKeyDown = MaskEdit8KeyDown
  end
  object DBEdit6: TDBEdit [20]
    Tag = 1
    Left = 184
    Top = 152
    Width = 650
    Height = 21
    DataField = 'cNmLinha'
    DataSource = dsLinha
    TabOrder = 12
  end
  object MaskEdit9: TMaskEdit [21]
    Left = 112
    Top = 176
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = MaskEdit9Exit
    OnKeyDown = MaskEdit9KeyDown
  end
  object DBEdit7: TDBEdit [22]
    Tag = 1
    Left = 184
    Top = 176
    Width = 650
    Height = 21
    DataField = 'cNmClasseProduto'
    DataSource = dsClasseProduto
    TabOrder = 13
  end
  object DBEdit8: TDBEdit [23]
    Tag = 1
    Left = 304
    Top = 40
    Width = 644
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiroRepres
    TabOrder = 14
  end
  object DBEdit11: TDBEdit [24]
    Tag = 1
    Left = 184
    Top = 40
    Width = 119
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = dsTerceiroRepres
    TabOrder = 15
  end
  object MaskEdit10: TMaskEdit [25]
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 544
    Top = 232
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryTerceiroRepres: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cCNPJCPF'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nCdTerceiro'
      '   AND EXISTS(SELECT 1'
      '                FROM TerceiroTipoTerceiro TTT'
      '               WHERE TTT.nCdTerceiro     = Terceiro.nCdTerceiro'
      '                AND TTT.nCdTipoTerceiro = 4)')
    Left = 576
    Top = 232
    object qryTerceiroRepresnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroReprescCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceiroReprescNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 304
  end
  object DataSource2: TDataSource
    Left = 632
    Top = 312
  end
  object DataSource3: TDataSource
    Left = 640
    Top = 320
  end
  object dsTerceiroRepres: TDataSource
    DataSet = qryTerceiroRepres
    Left = 576
    Top = 264
  end
  object DataSource5: TDataSource
    Left = 640
    Top = 240
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento, cNmDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 784
    Top = 280
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarca, cNmMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 824
    Top = 280
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'npk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha'
      'WHERE nCdLinha = :npk')
    Left = 864
    Top = 280
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object qryClasseProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM ClasseProduto'
      'WHERE nCdClasseProduto = :nPK')
    Left = 904
    Top = 272
    object qryClasseProdutonCdClasseProduto: TAutoIncField
      FieldName = 'nCdClasseProduto'
      ReadOnly = True
    end
    object qryClasseProdutocNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
  end
  object dsDepartamento: TDataSource
    DataSet = qryDepartamento
    Left = 784
    Top = 288
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 824
    Top = 296
  end
  object dsLinha: TDataSource
    DataSet = qryLinha
    Left = 864
    Top = 296
  end
  object dsClasseProduto: TDataSource
    DataSet = qryClasseProduto
    Left = 912
    Top = 296
  end
end
