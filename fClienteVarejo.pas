unit fClienteVarejo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons;

type
  TfrmClienteVarejo = class(TfrmCadastro_Padrao)
    qryMasternCdTerceiro: TIntegerField;
    qryMastercIDExterno: TStringField;
    qryMastercNmTerceiro: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMasterdDtCadastro: TDateTimeField;
    qryMasternCdStatus: TIntegerField;
    qryMasternValLimiteCred: TBCDField;
    qryMastercRG: TStringField;
    qryMastercTelefone1: TStringField;
    qryMastercTelefone2: TStringField;
    qryMastercTelefoneMovel: TStringField;
    qryMastercEmail: TStringField;
    qryPessoaFisica: TADOQuery;
    qryPessoaFisicanCdPessoaFisica: TAutoIncField;
    qryPessoaFisicanCdTerceiro: TIntegerField;
    qryPessoaFisicadDtNasc: TDateTimeField;
    qryPessoaFisicacUFRG: TStringField;
    qryPessoaFisicanCdTabTipoComprovRG: TIntegerField;
    qryPessoaFisicacOBSRG: TStringField;
    qryPessoaFisicadDtEmissaoRG: TDateTimeField;
    qryPessoaFisicanCdTabTipoSexo: TIntegerField;
    qryPessoaFisicanCdTabTipoEstadoCivil: TIntegerField;
    qryPessoaFisicaiNrDependentes: TIntegerField;
    qryPessoaFisicacNaturalidade: TStringField;
    qryPessoaFisicanCdTabTipoGrauEscola: TIntegerField;
    qryPessoaFisicaiDiaVenctoPref: TIntegerField;
    qryPessoaFisicacNmEmpTrab: TStringField;
    qryPessoaFisicacTelefoneEmpTrab: TStringField;
    qryPessoaFisicacRamalEmpTrab: TStringField;
    qryPessoaFisicacEnderecoEmpTrab: TStringField;
    qryPessoaFisicaiNrEnderecoEmpTrab: TIntegerField;
    qryPessoaFisicacBairroEmpTrab: TStringField;
    qryPessoaFisicacCidadeEmpTrab: TStringField;
    qryPessoaFisicacUFEmpTrab: TStringField;
    qryPessoaFisicacCEPEmpTrab: TStringField;
    qryPessoaFisicanCdTabTipoClasseProf: TIntegerField;
    qryPessoaFisicanCdTabTipoProfissao: TIntegerField;
    qryPessoaFisicacCargoTrab: TStringField;
    qryPessoaFisicadDtAdmissao: TDateTimeField;
    qryPessoaFisicanCdTabTipoComprovAdm: TIntegerField;
    qryPessoaFisicacOBSAdmissao: TStringField;
    qryPessoaFisicanValRendaBruta: TBCDField;
    qryPessoaFisicanCdTabTipoComprovRenda: TIntegerField;
    qryPessoaFisicacOBSRenda: TStringField;
    qryPessoaFisicacNmCjg: TStringField;
    qryPessoaFisicadDtNascCjg: TDateTimeField;
    qryPessoaFisicacRGCjg: TStringField;
    qryPessoaFisicacUFRGCjg: TStringField;
    qryPessoaFisicanCdTabTipoComprovRGCjg: TIntegerField;
    qryPessoaFisicacOBSRGCjg: TStringField;
    qryPessoaFisicadDtEmissaoRGCjg: TDateTimeField;
    qryPessoaFisicacCPFCjg: TStringField;
    qryPessoaFisicacTelefoneCelCjg: TStringField;
    qryPessoaFisicacNmEmpresaTrabCjg: TStringField;
    qryPessoaFisicacTelefoneEmpTrabCjg: TStringField;
    qryPessoaFisicacRamalEmpTrabCjg: TStringField;
    qryPessoaFisicanCdTabTipoClasseProfCjg: TIntegerField;
    qryPessoaFisicanCdTabTipoProfissaoCjg: TIntegerField;
    qryPessoaFisicacCargoTrabCjg: TStringField;
    qryPessoaFisicadDtAdmissaoCjg: TDateTimeField;
    qryPessoaFisicanCdTabTipoComprovCjg: TIntegerField;
    qryPessoaFisicacOBSAdmissaoCjg: TStringField;
    qryPessoaFisicanValRendaBrutaCjg: TBCDField;
    qryPessoaFisicanCdTabTipoComprovRendaCjg: TIntegerField;
    qryPessoaFisicacOBSRendaCjg: TStringField;
    qryPessoaFisicacNmRendaAdd1: TStringField;
    qryPessoaFisicanValRendaAdd1: TBCDField;
    qryPessoaFisicanCdTabTipoRendaAdd1: TIntegerField;
    qryPessoaFisicanCdTabTipoFonteRendaAdd1: TIntegerField;
    qryPessoaFisicanCdTabTipoComprovRendaAdd1: TIntegerField;
    qryPessoaFisicacOBSRendaAdd1: TStringField;
    qryPessoaFisicacNmRendaAdd2: TStringField;
    qryPessoaFisicanValRendaAdd2: TBCDField;
    qryPessoaFisicanCdTabTipoRendaAdd2: TIntegerField;
    qryPessoaFisicanCdTabTipoFonteRendaAdd2: TIntegerField;
    qryPessoaFisicanCdTabTipoComprovRendaAdd2: TIntegerField;
    qryPessoaFisicacOBSRendaAdd2: TStringField;
    qryPessoaFisicacNmRendaAdd3: TStringField;
    qryPessoaFisicanValRendaAdd3: TBCDField;
    qryPessoaFisicanCdTabTipoRendaAdd3: TIntegerField;
    qryPessoaFisicanCdTabTipoFonteRendaAdd3: TIntegerField;
    qryPessoaFisicanCdTabTipoComprovRendaAdd3: TIntegerField;
    qryPessoaFisicacOBSRendaAdd3: TStringField;
    qryPessoaFisicacNmRefPessoal1: TStringField;
    qryPessoaFisicacTelefoneRefPessoal1: TStringField;
    qryPessoaFisicacNmRefPessoal2: TStringField;
    qryPessoaFisicacTelefoneRefPessoal2: TStringField;
    qryPessoaFisicacNmRefPessoal3: TStringField;
    qryPessoaFisicacTelefoneRefPessoal3: TStringField;
    qryPessoaFisicacNmRefComercial1: TStringField;
    qryPessoaFisicacRefComercial1: TStringField;
    qryPessoaFisicacNmRefComercial2: TStringField;
    qryPessoaFisicacRefComercial2: TStringField;
    qryPessoaFisicacNmRefComercial3: TStringField;
    qryPessoaFisicacRefComercial3: TStringField;
    qryPessoaFisicanCdUsuarioCadastro: TIntegerField;
    qryPessoaFisicanCdLojaCadastro: TIntegerField;
    qryPessoaFisicanCdUsuarioUltAlt: TIntegerField;
    qryPessoaFisicanCdLojaUltAlt: TIntegerField;
    qryPessoaFisicadDtUltAlt: TDateTimeField;
    qryPessoaFisicanCdTabTipoOrigem: TIntegerField;
    qryPessoaFisicanCdTabTipoSituacao: TIntegerField;
    dsPessoaFisica: TDataSource;
    qrySexo: TADOQuery;
    qrySexonCdTabTipoSexo: TIntegerField;
    qrySexocNmTabTipoSexo: TStringField;
    dsTabTipoSexo: TDataSource;
    qryEstadoCivil: TADOQuery;
    qryEstadoCivilnCdTabTipoEstadoCivil: TIntegerField;
    qryEstadoCivilcNmTabTipoEstadoCivil: TStringField;
    dsTabTipoEstadoCivil: TDataSource;
    qryEscolaridade: TADOQuery;
    dsEscolaridade: TDataSource;
    qryEscolaridadenCdTabTipoGrauEscola: TIntegerField;
    qryEscolaridadecNmTabTipoGrauEscola: TStringField;
    cxPageControl1: TcxPageControl;
    Tab1: TcxTabSheet;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    ComboSexo: TDBLookupComboBox;
    ComboEstadoCivil: TDBLookupComboBox;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    ComboEscolaridade: TDBLookupComboBox;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    qryComprovanteCID: TADOQuery;
    qryComprovanteCIDnCdTabTipoComprov: TIntegerField;
    qryComprovanteCIDcNmTabTipoComprov: TStringField;
    dsComprovanteCID: TDataSource;
    ComboComprovCID: TDBLookupComboBox;
    Label7: TLabel;
    qryEndereco: TADOQuery;
    qryEndereconCdEndereco: TAutoIncField;
    qryEndereconCdTerceiro: TIntegerField;
    qryEnderecocEndereco: TStringField;
    qryEnderecoiNumero: TIntegerField;
    qryEnderecocBairro: TStringField;
    qryEnderecocCidade: TStringField;
    qryEnderecocUF: TStringField;
    qryEndereconCdTipoEnd: TIntegerField;
    qryEndereconCdRegiao: TIntegerField;
    qryEnderecocTelefone: TStringField;
    qryEnderecocFax: TStringField;
    qryEndereconCdPais: TIntegerField;
    qryEndereconCdStatus: TIntegerField;
    qryEnderecocCep: TStringField;
    qryEnderecocComplemento: TStringField;
    qryEndereconCdTabTipoResidencia: TIntegerField;
    qryEnderecoiTempoResidAno: TIntegerField;
    qryEnderecoiTempoResidMes: TIntegerField;
    qryEndereconValAluguel: TBCDField;
    qryEndereconCdTabTipoComprovEnd: TIntegerField;
    qryEnderecocOBSComprov: TStringField;
    dsEndereco: TDataSource;
    qryTipoResidencia: TADOQuery;
    qryTipoResidencianCdTabTipoResidencia: TIntegerField;
    qryTipoResidenciacNmTabTipoResidencia: TStringField;
    dsTipoResidencia: TDataSource;
    qryComprovanteCRS: TADOQuery;
    qryComprovanteCRSnCdTabTipoComprov: TIntegerField;
    qryComprovanteCRScNmTabTipoComprov: TStringField;
    dsComprovanteCRS: TDataSource;
    GroupBox3: TGroupBox;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    ComboTipoResid: TDBLookupComboBox;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    ComboComprovCRS: TDBLookupComboBox;
    DBEdit40: TDBEdit;
    Image2: TImage;
    qryPessoaFisicacNmMae: TStringField;
    qryPessoaFisicacNmPai: TStringField;
    Tab2: TcxTabSheet;
    GroupBox5: TGroupBox;
    Label21: TLabel;
    DBEdit17: TDBEdit;
    Label22: TLabel;
    DBEdit18: TDBEdit;
    Label23: TLabel;
    DBEdit19: TDBEdit;
    Label24: TLabel;
    DBEdit20: TDBEdit;
    Label26: TLabel;
    DBEdit22: TDBEdit;
    Label27: TLabel;
    DBEdit23: TDBEdit;
    Label53: TLabel;
    DBEdit46: TDBEdit;
    Label54: TLabel;
    DBEdit47: TDBEdit;
    ComboComprovCIDCjg: TDBLookupComboBox;
    Label55: TLabel;
    Label25: TLabel;
    DBEdit21: TDBEdit;
    Label56: TLabel;
    DBEdit48: TDBEdit;
    Label57: TLabel;
    DBEdit49: TDBEdit;
    Label61: TLabel;
    DBEdit52: TDBEdit;
    qryClasseProf: TADOQuery;
    qryClasseProfnCdTabTipoClasseProf: TIntegerField;
    qryClasseProfcNmTabTipoClasseProf: TStringField;
    dsClasseProf: TDataSource;
    qryProfissao: TADOQuery;
    qryProfissaonCdTabTipoProfissao: TIntegerField;
    qryProfissaocNmTabTipoProfissao: TStringField;
    dsProfissao: TDataSource;
    qryComprovanteCTS: TADOQuery;
    qryComprovanteCTSnCdTabTipoComprov: TIntegerField;
    qryComprovanteCTScNmTabTipoComprov: TStringField;
    dsComprovanteCTS: TDataSource;
    qryComprovanteCRD: TADOQuery;
    dsComprovanteCRD: TDataSource;
    qryComprovanteCRDnCdTabTipoComprov: TIntegerField;
    qryComprovanteCRDcNmTabTipoComprov: TStringField;
    GroupBox4: TGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label99: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label75: TLabel;
    Label71: TLabel;
    Label74: TLabel;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    ComboClasseProf: TDBLookupComboBox;
    comboProfissao: TDBLookupComboBox;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    DBEdit62: TDBEdit;
    DBEdit63: TDBEdit;
    DBEdit65: TDBEdit;
    ComboTipoComprovCTS: TDBLookupComboBox;
    ComboTipoComprovCRD: TDBLookupComboBox;
    Label76: TLabel;
    Label77: TLabel;
    ComboClasseProfCjg: TDBLookupComboBox;
    ComboProfissaoCjg: TDBLookupComboBox;
    Label58: TLabel;
    Label60: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    DBEdit50: TDBEdit;
    DBEdit51: TDBEdit;
    DBEdit53: TDBEdit;
    DBEdit54: TDBEdit;
    ComboTipoComprovCTSCjg: TDBLookupComboBox;
    ComboTipoComprovCRDCjg: TDBLookupComboBox;
    GroupBox2: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    DBEdit7: TDBEdit;
    DBEdit16: TDBEdit;
    Image3: TImage;
    Tab3: TcxTabSheet;
    GroupBox6: TGroupBox;
    qryTipoRenda: TADOQuery;
    dsTipoRenda: TDataSource;
    qryTipoRendanCdTabTipoRenda: TIntegerField;
    qryTipoRendacNmTabTipoRenda: TStringField;
    qryTipoFonteRenda: TADOQuery;
    qryTipoFonteRendanCdTabTipoFonteRenda: TIntegerField;
    qryTipoFonteRendacNmTabTipoFonteRenda: TStringField;
    dsTipoFonteRenda: TDataSource;
    GroupBox7: TGroupBox;
    Label79: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    ComboTipoRenda1: TDBLookupComboBox;
    DBEdit64: TDBEdit;
    DBEdit70: TDBEdit;
    DBEdit71: TDBEdit;
    DBEdit72: TDBEdit;
    DBEdit73: TDBEdit;
    DBEdit74: TDBEdit;
    DBEdit75: TDBEdit;
    DBEdit76: TDBEdit;
    DBEdit77: TDBEdit;
    ComboTipoRenda2: TDBLookupComboBox;
    ComboTipoRenda3: TDBLookupComboBox;
    ComboFonteRenda1: TDBLookupComboBox;
    ComboFonteRenda2: TDBLookupComboBox;
    ComboFonteRenda3: TDBLookupComboBox;
    GroupBox8: TGroupBox;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    DBEdit67: TDBEdit;
    DBEdit68: TDBEdit;
    DBEdit69: TDBEdit;
    DBEdit78: TDBEdit;
    DBEdit79: TDBEdit;
    DBEdit80: TDBEdit;
    GroupBox9: TGroupBox;
    Label67: TLabel;
    Label78: TLabel;
    DBEdit81: TDBEdit;
    DBEdit82: TDBEdit;
    DBEdit83: TDBEdit;
    DBEdit84: TDBEdit;
    DBEdit85: TDBEdit;
    DBEdit86: TDBEdit;
    qryPessoaAutorizada: TADOQuery;
    qryPessoaAutorizadanCdPessoaAutorizada: TAutoIncField;
    qryPessoaAutorizadanCdTerceiro: TIntegerField;
    qryPessoaAutorizadacNmPessoaAutorizada: TStringField;
    qryPessoaAutorizadadDtNasc: TDateTimeField;
    qryPessoaAutorizadacCPF: TStringField;
    qryPessoaAutorizadacRG: TStringField;
    qryPessoaAutorizadanCdTabTipoComprovRG: TIntegerField;
    qryPessoaAutorizadacOBSRG: TStringField;
    qryPessoaAutorizadanCdTabTipoGrauParente: TIntegerField;
    qryPessoaAutorizadanCdStatus: TIntegerField;
    dsPessoaAutorizada: TDataSource;
    DBGridEh1: TDBGridEh;
    qryTipoComprovCIDPA: TADOQuery;
    qryPessoaAutorizadacNmTabTipoComprov: TStringField;
    qryGrauParente: TADOQuery;
    qryGrauParentenCdTabTipoGrauParente: TIntegerField;
    qryGrauParentecNmTabTipoGrauParente: TStringField;
    qryPessoaAutorizadacNmTabTipoGrauParente: TStringField;
    qryPessoaAutorizadacNmStatus: TStringField;
    qryTipoComprovCIDPAnCdTabTipoComprov: TIntegerField;
    qryTipoComprovCIDPAcNmTabTipoComprov: TStringField;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    Tab4: TcxTabSheet;
    GroupBox10: TGroupBox;
    DBGridEh2: TDBGridEh;
    qrySPCPessoaFisica: TADOQuery;
    qrySPCPessoaFisicanCdSPCPessoaFisica: TAutoIncField;
    qrySPCPessoaFisicanCdTerceiro: TIntegerField;
    qrySPCPessoaFisicanCdTabTipoSPC: TIntegerField;
    qrySPCPessoaFisicacFlgRestricao: TIntegerField;
    qrySPCPessoaFisicacDadosAdicionais: TStringField;
    qrySPCPessoaFisicadDtConsulta: TDateTimeField;
    qrySPCPessoaFisicanCdUsuarioConsulta: TIntegerField;
    qrySPCPessoaFisicanCdLojaConsulta: TIntegerField;
    qrySPCPessoaFisicadDtCad: TDateTimeField;
    dsSPCPessoaFisica: TDataSource;
    qrySPCPessoaFisicacNmTabTipoSPC: TStringField;
    qrySPCPessoaFisicacNmUsuarioConsulta: TStringField;
    qryTabTipoSPC: TADOQuery;
    qryTabTipoSPCnCdTabTipoSPC: TIntegerField;
    qryTabTipoSPCcNmTabTipoSPC: TStringField;
    qryUsuarioSPC: TADOQuery;
    qryUsuarioSPCnCdUsuario: TIntegerField;
    qryUsuarioSPCcNmUsuario: TStringField;
    Image4: TImage;
    Tab5: TcxTabSheet;
    GroupBox11: TGroupBox;
    qryMasternValCreditoUtil: TBCDField;
    qryMasternValRecAtraso: TBCDField;
    qryMasternValChequePendComp: TBCDField;
    qryMasternValPedidoAberto: TBCDField;
    qryMasternSaldoLimiteCredito: TBCDField;
    GroupBox12: TGroupBox;
    Label94: TLabel;
    Label95: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    Label98: TLabel;
    Label100: TLabel;
    DBEdit87: TDBEdit;
    DBEdit88: TDBEdit;
    DBEdit89: TDBEdit;
    DBEdit90: TDBEdit;
    DBEdit91: TDBEdit;
    DBEdit92: TDBEdit;
    Label59: TLabel;
    DBEdit55: TDBEdit;
    Label66: TLabel;
    DBEdit56: TDBEdit;
    Label84: TLabel;
    DBEdit57: TDBEdit;
    Label85: TLabel;
    DBEdit58: TDBEdit;
    Label92: TLabel;
    DBEdit61: TDBEdit;
    Label93: TLabel;
    DBEdit66: TDBEdit;
    Label101: TLabel;
    DBEdit93: TDBEdit;
    Label102: TLabel;
    DBEdit94: TDBEdit;
    Label103: TLabel;
    DBEdit95: TDBEdit;
    qryUsuarioCad: TADOQuery;
    qryUsuarioCadcNmUsuario: TStringField;
    DBEdit96: TDBEdit;
    DataSource1: TDataSource;
    qryUsuarioUltAlt: TADOQuery;
    qryUsuarioUltAltcNmUsuario: TStringField;
    DBEdit97: TDBEdit;
    DataSource2: TDataSource;
    qryTabTipoOrigem: TADOQuery;
    qryTabTipoOrigemcNmTabTipoOrigem: TStringField;
    DBEdit98: TDBEdit;
    DataSource3: TDataSource;
    qryTabTipoSituacao: TADOQuery;
    qryTabTipoSituacaocNmTabTipoSituacao: TStringField;
    DBEdit99: TDBEdit;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    qryAux: TADOQuery;
    DBEdit100: TDBEdit;
    DataSource6: TDataSource;
    qryPessoaFisicanCdUsuarioAprova: TIntegerField;
    qryPessoaFisicadDtUsuarioAprova: TDateTimeField;
    Label104: TLabel;
    DBEdit101: TDBEdit;
    Label105: TLabel;
    DBEdit102: TDBEdit;
    qryUsuarioAprova: TADOQuery;
    qryUsuarioAprovacNmUsuario: TStringField;
    DBEdit103: TDBEdit;
    DataSource7: TDataSource;
    Image5: TImage;
    qryTerceiroTipoTerceiro: TADOQuery;
    qryTerceiroTipoTerceironCdTerceiro: TIntegerField;
    qryTerceiroTipoTerceironCdTipoTerceiro: TIntegerField;
    qryBuscaCEP: TADOQuery;
    qryBuscaCEPcUF: TStringField;
    qryBuscaCEPcEndereco: TStringField;
    qryBuscaCEPcBairro: TStringField;
    qryBuscaCEPcCidade: TStringField;
    btAltEndereco: TcxButton;
    qryBuscaClienteCPF: TADOQuery;
    qryBuscaClienteCPFnCdTerceiro: TIntegerField;
    btAltTelefone: TcxButton;
    btConsultaEndereco: TcxButton;
    btConsultaEndereco2: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryPessoaAutorizadaCalcFields(DataSet: TDataSet);
    procedure qryPessoaAutorizadaBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qrySPCPessoaFisicaBeforePost(DataSet: TDataSet);
    procedure qrySPCPessoaFisicaCalcFields(DataSet: TDataSet);
    procedure DBGridEh2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qrySPCPessoaFisicaBeforeDelete(DataSet: TDataSet);
    procedure qryPessoaFisicaBeforePost(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure qryEnderecoBeforePost(DataSet: TDataSet);
    procedure Tab5Show(Sender: TObject);
    procedure Tab3Show(Sender: TObject);
    procedure Tab4Show(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit28Enter(Sender: TObject);
    procedure DBEdit28Exit(Sender: TObject);
    procedure DBEdit27Exit(Sender: TObject);
    procedure DBEdit27Enter(Sender: TObject);
    procedure DesativaEndereco() ;
    procedure AtivaEndereco() ;
    procedure btAltEnderecoClick(Sender: TObject);
    procedure DBEdit46Exit(Sender: TObject);
    procedure qrySPCPessoaFisicaAfterPost(DataSet: TDataSet);
    procedure btAltTelefoneClick(Sender: TObject);
    procedure btConsultaEnderecoClick(Sender: TObject);
    procedure btConsultaEndereco2Click(Sender: TObject);
    procedure AtivaLimite() ;
    procedure DesativaLimite() ;
  private
    { Private declarations }
    cCepAux : string ;
    cEnderecoManual : string ;
  public
    { Public declarations }
  end;

var
  frmClienteVarejo: TfrmClienteVarejo;

implementation

uses fLookup_Padrao, fMenu, fConsultaEndereco;

{$R *.dfm}

procedure TfrmClienteVarejo.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CLIENTECREDIARIO' ;
  nCdTabelaSistema  := 13 ;
  nCdConsultaPadrao := 180 ;
  bCodigoAutomatico := False ;

end;

procedure TfrmClienteVarejo.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryPessoaFisica.Close ;
  qryPessoaFisica.Parameters.ParamByName('nPK').Value := 0 ;
  qryPessoaFisica.Open ;
  qryPessoaFisica.Insert ;

  qryEndereco.Close ;
  qryEndereco.Parameters.ParamByName('nPK').Value := 0 ;
  qryEndereco.Open ;
  qryEndereco.Insert ;

  AtivaEndereco() ;
  AtivaLimite() ;

  qryPessoaAutorizada.Close ;
  qryPessoaAutorizada.Parameters.ParamByName('nPK').Value := 0 ;
  qryPessoaAutorizada.Open ;

  qrySPCPessoaFisica.Close ;
  qrySPCPessoaFisica.Parameters.ParamByName('nPK').Value := 0 ;
  qrySPCPessoaFisica.Open ;

  cxPageControl1.ActivePage := Tab1 ;

  //gera um novo c�digo para o registro
  usp_ProximoID.Close;
  usp_ProximoID.Parameters.ParamByName('@cNmTabela').Value := cNmTabelaMaster ;
  usp_ProximoID.ExecProc ;

  qryMasternCdTerceiro.Value := usp_ProximoID.Parameters.ParamByName('@iUltimoCodigo').Value ;

  qryID.Close ;

  btAltEndereco.Visible      := False ;
  btAltTelefone.Visible      := False ;
  btConsultaEndereco.Visible := True  ;

  DBEDit3.SetFOcus ;
end;

procedure TfrmClienteVarejo.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      MensagemAlerta('Nenhuma loja ativa.') ;
  end ;

  cEnderecoManual := frmMenu.LeParametro('CLIPERMENDMAN') ;

  qrySexo.Open;
  qryEstadoCivil.Open ;
  qryComprovanteCRS.Open ;
  qryComprovanteCID.Open ;
  qryTipoResidencia.Open ;
  qryEscolaridade.Open ;
  qryComprovanteCRD.Open ;
  qryClasseProf.Open ;
  qryProfissao.Open ;
  qryComprovanteCTS.Open ;
  qryTipoFonteRenda.Open ;
  qryTipoRenda.Open ;

  cxPageControl1.ActivePage := Tab1 ;

end;

procedure TfrmClienteVarejo.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryPessoaFisica.Close ;
  PosicionaQuery(qryPessoaFisica, qryMasternCdTerceiro.AsString) ;

  qryEndereco.Close ;
  PosicionaQuery(qryEndereco, qryMasternCdTerceiro.AsString) ;

  if (qryPessoaFisicadDtUsuarioAprova.AsString <> '') then
  begin
      DesativaEndereco() ;
      DesativaLimite() ;

      btAltEndereco.Visible      := True ;
      btAltTelefone.Visible      := True ;
  end
  else
  begin
      AtivaEndereco() ;
      AtivaLimite() ;
  end ;


end;

procedure TfrmClienteVarejo.qryPessoaAutorizadaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryPessoaAutorizada.State <> dsBrowse) or (qryPessoaAutorizadacNmTabTipoComprov.Value = '') then
  begin

      qryTipoComprovCIDPA.Close ;
      PosicionaQuery(qryTipoComprovCIDPA, qryPessoaAutorizadanCdTabTipoComprovRG.AsString) ;

      if not qryTipoComprovCIDPA.Eof then
          qryPessoaAutorizadacNmTabTipoComprov.Value := qryTipoComprovCIDPAcNmTabTipoComprov.Value ;

  end ;

  if (qryPessoaAutorizada.State <> dsBrowse) or (qryPessoaAutorizadacNmTabTipoGrauParente.Value = '') then
  begin

      qryGrauParente.Close ;
      PosicionaQuery(qryGrauParente, qryPessoaAutorizadanCdTabTipoGrauParente.AsString) ;

      if not qryGrauParente.Eof then
          qryPessoaAutorizadacNmTabTipoGrauParente.Value := qryGrauParentecNmTabTipoGrauParente.Value ;

  end ;

  if (qryPessoaAutorizada.State <> dsBrowse) or (qryPessoaAutorizadacNmStatus.Value = '') then
  begin

      qryStatus.Close ;
      PosicionaQuery(qryStatus, qryPessoaAutorizadanCdStatus.AsString) ;

      if not qryStatus.Eof then
          qryPessoaAutorizadacNmStatus.Value := qryStatuscNmStatus.Value ;

  end ;

end;

procedure TfrmClienteVarejo.qryPessoaAutorizadaBeforePost(
  DataSet: TDataSet);
begin

  if (qryPessoaAutorizadacCPF.Value = qryMastercCNPJCPF.Value) then
  begin
      MensagemAlerta('O CPF da pessoa autorizada n�o pode ser igual o CPF do cliente.') ;
      abort ;
  end ;

  if (qryPessoaAutorizadacNmPessoaAutorizada.Value = '') then
  begin
      MensagemAlerta('Informe o nome completo da pessoa autorizada.') ;
      abort ;
  end ;

  if (qryPessoaAutorizadacRG.Value = '') then
  begin
      MensagemAlerta('Informe o n�mero do documento de identifica��o.') ;
      abort ;
  end ;

  if (qryPessoaAutorizadacNmTabTipoComprov.Value = '') then
  begin
      MensagemAlerta('Informe o tipo do documento de identifica��o.') ;
      abort ;
  end ;

  if (qryPessoaAutorizadacNmTabTipoGrauParente.Value = '') then
  begin
      MensagemAlerta('Informe o grau de parentesco.') ;
      abort ;
  end ;

  if (qryPessoaAutorizadacNmStatus.Value = '') then
  begin
      MensagemAlerta('Informe o status da pessoa.'+#13#13+'1 - Ativo / 2 - Inativo') ;
      abort ;
  end ;

  qryBuscaClienteCPF.Close ;
  PosicionaQuery(qryBuscaClienteCPF, qryPessoaAutorizadacCPF.Value) ;

  if not qryBuscaClienteCPF.eof then
  begin
      MensagemAlerta('Esta pessoa j� est� cadastrada como cliente. Clique em OK para visualizar o c�digo do cliente.') ;
      ShowMessage('C�digo : ' + qryBuscaClienteCPFnCdTerceiro.asString) ;
      abort ;
  end ;

  inherited;

  qryPessoaAutorizadacNmPessoaAutorizada.Value := Uppercase(qryPessoaAutorizadacNmPessoaAutorizada.Value) ;
  qryPessoaAutorizadanCdTerceiro.Value         := qryMasternCdTerceiro.Value ;

end;

procedure TfrmClienteVarejo.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryPessoaAutorizada.State = dsBrowse) then
             qryPessoaAutorizada.Edit ;

        if (qryPessoaAutorizada.State = dsInsert) or (qryPessoaAutorizada.State = dsEdit) then
        begin

            if (DBGridEh1.Col = 7) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(177);

                If (nPK > 0) then
                begin
                    qryPessoaAutorizadanCdTabTipoComprovRG.Value := nPK ;
                end ;
            end ;

            if (DBGridEh1.Col = 10) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(178);

                If (nPK > 0) then
                begin
                    qryPessoaAutorizadanCdTabTipoGrauParente.Value := nPK ;
                end ;
            end ;

            if (DBGridEh1.Col = 12) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(2);

                If (nPK > 0) then
                begin
                    qryPessoaAutorizadanCdStatus.Value := nPK ;
                end ;
            end ;

        end ;

    end ;

  end ;


end;

procedure TfrmClienteVarejo.qrySPCPessoaFisicaBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qrySPCPessoaFisicacNmTabTipoSPC.Value = '') then
  begin
      MensagemAlerta('Informe o Tipo de Consulta') ;
      abort ;
  end ;

  if (qrySPCPessoaFisicadDtConsulta.asString = '') then
  begin
      MensagemAlerta('Informe a data da consulta.') ;
      abort ;
  end ;

  if (qrySPCPessoaFisica.State = dsEdit) then
  begin

      if (qrySPCPessoaFisicadDtCad.Value < Date) then
      begin
          MensagemAlerta('A consulta s� pode ser alterada no dia da inclus�o.') ;
          abort ;
      end ;

      if (qrySPCPessoaFisicanCdUsuarioConsulta.Value <> frmMenu.nCdUsuarioLogado) then
      begin
          MensagemAlerta('Somente o usu�rio que incluiu a consulta pode alterar as informa��es.') ;
          abort ;
      end;

  end ;

  if (qrySPCPessoaFisica.State = dsInsert) then
  begin
      qrySPCPessoaFisicadDtCad.Value             := Now() ;
      qrySPCPessoaFisicanCdUsuarioConsulta.Value := frmMenu.nCdUsuarioLogado ;
      qrySPCPessoaFisicanCdTerceiro.Value        := qryMasternCdTerceiro.Value ;

      if (frmMenu.nCdLojaAtiva > 0) then
          qrySPCPessoaFisicanCdLojaConsulta.Value    := frmMenu.nCdLojaAtiva;

  end ;

  qrySPCPessoaFisicacDadosAdicionais.Value := Uppercase(qrySPCPessoaFisicacDadosAdicionais.Value) ;
  
end;

procedure TfrmClienteVarejo.qrySPCPessoaFisicaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qrySPCPessoaFisica.State <> dsBrowse) or (qrySPCPessoaFisicacNmTabTipoSPC.Value = '') then
  begin

      qryTabTipoSPC.Close ;
      PosicionaQuery(qryTabTipoSPC, qrySPCPessoaFisicanCdTabTipoSPC.AsString) ;

      if not qryTabTipoSPC.Eof then
          qrySPCPessoaFisicacNmTabTipoSPC.Value := qryTabTipoSPCcNmTabTipoSPC.Value ;

  end ;

  if (qrySPCPessoaFisica.State <> dsBrowse) or (qrySPCPessoaFisicacNmUsuarioConsulta.Value = '') then
  begin

      qryUsuarioSPC.Close ;
      PosicionaQuery(qryUsuarioSPC, qrySPCPessoaFisicanCdUsuarioConsulta.AsString) ;

      if not qryUsuarioSPC.Eof then
          qrySPCPessoaFisicacNmUsuarioConsulta.Value := qryUsuarioSPCcNmUsuario.Value ;

  end ;
        

end;

procedure TfrmClienteVarejo.DBGridEh2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qrySPCPessoaFisica.State = dsBrowse) then
             qrySPCPessoaFisica.Edit ;

        if (qrySPCPessoaFisica.State = dsInsert) or (qrySPCPessoaFisica.State = dsEdit) then
        begin

            if (DBGridEh1.Col = 3) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(179);

                If (nPK > 0) then
                begin
                    qrySPCPessoaFisicanCdTabTipoSPC.Value := nPK ;
                end ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmClienteVarejo.qrySPCPessoaFisicaBeforeDelete(
  DataSet: TDataSet);
begin

  if (qrySPCPessoaFisicadDtCad.Value < Date) then
  begin
      MensagemAlerta('A consulta s� pode ser exclu�da no dia da inclus�o.') ;
      abort ;
  end ;

  if (qrySPCPessoaFisicanCdUsuarioConsulta.Value <> frmMenu.nCdUsuarioLogado) then
  begin
      MensagemAlerta('Somente o usu�rio que incluiu a consulta pode excluir.') ;
      abort ;
  end;

  inherited;

end;

procedure TfrmClienteVarejo.qryPessoaFisicaBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryPessoaFisica.State = dsInsert) then
      qryPessoaFisicanCdTerceiro.Value := qryMasternCdTerceiro.Value ;

  if (qryPessoaFisica.State = dsEdit) then
  begin
      qryPessoaFisicanCdUsuarioUltAlt.Value := frmMenu.nCdUsuarioLogado;
      qryPessoaFisicadDtUltAlt.Value        := Now() ;
      qryPessoaFisicanCdLojaUltAlt.Value    := frmMenu.nCdLojaAtiva;
  end ;

  

end;

procedure TfrmClienteVarejo.btSalvarClick(Sender: TObject);
begin
  {inherited;}

  if (qryMaster.State = dsInsert) then
  begin

      case MessageDlg('Confirma a inclus�o deste cliente ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:abort ;
      end ;

  end
  else
  begin

      case MessageDlg('Confirma a altera��o deste cliente ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:abort ;
      end ;

  end ;

  cxPageControl1.ActivePage := Tab1 ;
  
  frmMenu.Connection.BeginTrans;

  try
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;

      if (qryEndereco.State <> dsInactive) then
          if (qryEndereco.State <> dsBrowse) then
              qryEndereco.Post ;

      if (qryPessoaFisica.State <> dsBrowse) then
          qryPessoaFisica.Post ;

      if (qrySPCPessoaFisica.State <> dsInactive) then
          if (qrySPCPessoaFisica.State <> dsBrowse) then
              qrySPCPessoaFisica.Post ;

      if (qryPessoaAutorizada.State <> dsInactive) then
          if (qryPessoaAutorizada.State <> dsBrowse) then
              qryPessoaAutorizada.Post ;

      // envia tudo que est� no cache para o banco de dados
      qryMaster.UpdateBatch();

      if (qryTerceiroTipoTerceiro.Active) then
          if (qryTerceiroTipoTerceiro.RecordCount > 0) then
              qryTerceiroTipoTerceiro.UpdateBatch();

      qryEndereco.UpdateBatch();

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE Endereco SET nCdStatus = 2 WHERE nCdTerceiro = ' + qryMasternCdTerceiro.AsString + ' AND nCdEndereco <> ' + qryEndereconCdEndereco.asString) ;
      qryAux.ExecSQL ;

      qryPessoaFisica.UpdateBatch();

      if (qryPessoaAutorizada.State <> dsInactive) then
          qryPessoaAutorizada.UpdateBatch();

      if (qrySPCPessoaFisica.State <> dsInactive) then
          qrySPCPessoaFisica.UpdateBatch();

  except
      frmMenu.Connection.RollbackTrans;
      {qryMaster.CancelBatch();
      qryPessoaFisica.CancelBatch();
      qryPessoaAutorizada.CancelBatch();
      qrySPCPessoaFisica.CancelBatch();
      qryTerceiroTipoTerceiro.CancelBatch();
      qryEndereco.CancelBatch();}
      //MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Informa��es Atualizadas com Sucesso!') ;

  cxPageControl1.ActivePage := Tab1 ;

  if (qryPessoaFisicadDtUsuarioAprova.AsString <> '') then
  begin

      DesativaEndereco() ;
      DesativaLimite()   ;

      btAltEndereco.Visible := True ;
      btAltTelefone.Visible := True ;
      
  end ;

end;

procedure TfrmClienteVarejo.qryEnderecoBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryEndereconCdTerceiro.Value := qryMasternCdTerceiro.Value ;
  qryEndereconCdStatus.Value   := 1 ;
  qryEndereconCdTipoEnd.Value  := 1 ;
  qryEndereconCdRegiao.Value   := 1 ;
  qryEndereconCdPais.Value     := 1 ;
  

end;

procedure TfrmClienteVarejo.Tab5Show(Sender: TObject);
begin
  inherited;

  if (qryMaster.eof) then
      exit ;
  
  qryUsuarioCad.Close ;
  qryUsuarioUltAlt.Close ;
  qryTabTipoSituacao.Close ;
  qryTabTipoOrigem.Close ;
  qryStatus.Close ;
  qryUsuarioAprova.Close ;

  PosicionaQuery(qryUsuarioCad, qryPessoaFisicanCdUsuarioCadastro.AsString) ;
  PosicionaQuery(qryUsuarioUltAlt, qryPessoaFisicanCdUsuarioUltAlt.AsString) ;
  PosicionaQuery(qryTabTipoSituacao, qryPessoaFisicanCdTabTipoSituacao.AsString) ;
  PosicionaQuery(qryTabTipoOrigem, qryPessoaFisicanCdTabTipoOrigem.AsString) ;
  PosicionaQuery(qryStatus, qryMasternCdStatus.AsString) ;
  PosicionaQuery(qryUsuarioAprova, qryPessoaFisicanCdUsuarioAprova.AsString) ;

end;

procedure TfrmClienteVarejo.Tab3Show(Sender: TObject);
begin
  inherited;

  if (qryMaster.eof) then
      exit ;

  //qryPessoaAutorizada.Close ;

  if (not qryPessoaAutorizada.Active)
      or ((qryPessoaAutorizadanCdTerceiro.Value <> qryMasternCdTerceiro.Value) and (qryPessoaAutorizada.RecordCount > 0)) then
      PosicionaQuery(qryPessoaAutorizada, qryMasternCdTerceiro.AsString) ;

end;

procedure TfrmClienteVarejo.Tab4Show(Sender: TObject);
begin
  inherited;

  if (qryMaster.eof) then
      exit ;

  if (not qrySPCPessoaFisica.Active)
      or ((qrySPCPessoaFisicanCdTerceiro.Value <> qryMasternCdTerceiro.Value) and (qrySPCPessoaFisica.RecordCount > 0)) then
      PosicionaQuery(qrySPCPessoaFisica, qryMasternCdTerceiro.AsString) ;

end;

procedure TfrmClienteVarejo.qryMasterAfterPost(DataSet: TDataSet);
begin
  {inherited;}

  {if (qryPessoaAutorizada.Active) and (qryPessoaAutorizada.RecordCount > 0) then
      qryPessoaAutorizada.First;

  if (qrySPCPessoaFisica.Active) and (qrySPCPessoaFisica.RecordCount > 0) then
      qrySPCPessoaFisica.First ;

  if (not qryPessoaAutorizada.Active)
      or ((qryPessoaAutorizadanCdTerceiro.Value <> qryMasternCdTerceiro.Value) and (qryPessoaAutorizada.RecordCount > 0)) then
      PosicionaQuery(qryPessoaAutorizada, qryMasternCdTerceiro.AsString) ;

  if (not qrySPCPessoaFisica.Active)
      or ((qrySPCPessoaFisicanCdTerceiro.Value <> qryMasternCdTerceiro.Value) and (qrySPCPessoaFisica.RecordCount > 0)) then
      PosicionaQuery(qrySPCPessoaFisica, qryMasternCdTerceiro.AsString)  ;}

end;

procedure TfrmClienteVarejo.qryMasterBeforePost(DataSet: TDataSet);
begin
  {inherited;}

  // faz as consist�ncias
  if (qryMastercNmTerceiro.Value = '') then
  begin
      MensagemAlerta('Informe o nome completo do cliente.') ;
      DBEdit3.SetFocus ;
      abort ;
  end ;

  if (qryPessoaFisicadDtNasc.AsString = '') then
  begin
      MensagemAlerta('Informe a data de nascimento.') ;
      DBEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMastercCNPJCPF.Value = '') then
  begin
      MensagemAlerta('Informe o CPF do cliente.') ;
      DBEdit10.SetFocus ;
      abort ;
  end ;

  if (qryMastercRG.Value = '') then
  begin
      MensagemAlerta('Informe o documento de identidade.') ;
      DBEdit5.SetFocus ;
      abort ;
  end ;

  if (qryPessoaFisicacUFRG.Value = '') then
  begin
      MensagemAlerta('Informe o estado de emiss�o do documento de identidade.') ;
      DBEdit6.SetFocus ;
      abort ;
  end ;

  if (qryPessoaFisicadDtEmissaoRG.asString = '') then
  begin
      MensagemAlerta('Informe a data de emiss�o do documento de identidade.') ;
      DBEdit9.SetFocus ;
      abort ;
  end ;

  if (qryPessoaFisicanCdTabTipoComprovRG.Value = 0) then
  begin
      MensagemAlerta('Informe o tipo do documento de identidade.') ;
      ComboComprovCID.SetFocus ;
      abort ;
  end ;

  if (qryPessoaFisicanCdTabTipoComprovRG.Value = 99) and (qryPessoaFisicacOBSRG.Value = '') then
  begin
      MensagemAlerta('Especifique o tipo de documento de identidade.') ;
      DBEdit8.SetFocus ;
      abort ;
  end ;

  if (qryPessoaFisicanCdTabTipoSexo.Value = 0) then
  begin
      MensagemAlerta('Selecione o sexo do cliente.') ;
      ComboSexo.SetFocus ;
      abort ;
  end ;

  if (qryPessoaFisicanCdTabTipoEstadoCivil.Value = 0) then
  begin
      MensagemAlerta('Selecione o estado civil.') ;
      ComboEstadoCivil.SetFocus;
      abort ;
  end ;

  if (qryPessoaFisicaiNrDependentes.Value < 0) then
  begin
      MensagemAlerta('N�mero de dependentes inv�lido.') ;
      DBEdit11.SetFocus;
      abort ;
  end ;

  if (qryPessoaFisicacNaturalidade.Value = '') then
  begin
      MensagemAlerta('Informe a naturalidade do cliente.') ;
      DBEdit12.SetFocus;
      abort ;
  end ;

  if (qryPessoaFisicanCdTabTipoGrauEscola.Value = 0) then
  begin
      MensagemAlerta('Informe o grau de escolaridade do cliente.') ;
      ComboEscolaridade.SetFocus;
      abort ;
  end ;

  if (Length(trim(qryMastercTelefoneMovel.Value)) > 0) and (Length(trim(qryMastercTelefoneMovel.Value)) <> 10) then
  begin
      MensagemAlerta('Telefone celular inv�lido. Informe o DDD e os 8 d�gitos do n�mero.') ;
      DBedit14.SetFocus;
      abort ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasterdDtCadastro.Value              := Now() ;
      qryMasternCdStatus.Value                := 1 ;
      qryPessoaFisicanCdUsuarioCadastro.Value := frmMenu.nCdUsuarioLogado;
      qryPessoaFisicanCdLojaCadastro.Value    := frmMenu.nCdLojaAtiva;
      qryPessoaFisicanCdTabTipoOrigem.Value   := 1 ;
      qryPessoaFisicanCdTabTipoSituacao.Value := 1 ;

      if not qryTerceiroTipoTerceiro.Active then
      begin
          qryTerceiroTipoTerceiro.Close ;
          qryTerceiroTipoTerceiro.Open ;
      end ;

      qryTerceiroTipoTerceiro.Insert ;
      qryTerceiroTipoTerceironCdTerceiro.Value     := qryMasternCdTerceiro.Value ;
      qryTerceiroTipoTerceironCdTipoTerceiro.Value := 2 ;
      qryTerceiroTipoTerceiro.Post ;

  end ;

  if (qryMaster.State = dsEdit) then
  begin
      if (qryPessoaFisica.State = dsBrowse) then
          qryPessoaFisica.Edit ;
          
      qryPessoaFisicanCdUsuarioUltAlt.Value := frmMenu.nCdUsuarioLogado;
      qryPessoaFisicadDtUltAlt.Value        := Now() ;
      qryPessoaFisicanCdLojaUltAlt.Value    := frmMenu.nCdLojaAtiva;

      if (qryPessoaFisicanCdTabTipoOrigem.Value = 0) then
          qryPessoaFisicanCdTabTipoOrigem.Value := 1 ;

      if (qryPessoaFisicanCdTabTipoSituacao.Value = 0) then
          qryPessoaFisicanCdTabTipoSituacao.Value := 1 ;

  end ;

end;

procedure TfrmClienteVarejo.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryPessoaFisica.Close ;
  qryEndereco.Close ;
  qrySPCPessoaFisica.Close ;
  qryPessoaAutorizada.Close ;
  qryUsuarioCad.Close ;
  qryUsuarioUltAlt.Close ;
  qryUsuarioAprova.Close ;
  qryTabTipoSituacao.Close ;
  qryStatus.Close ;
  qryTabTipoOrigem.Close ;

  btAltEndereco.Visible      := False ;
  btAltTelefone.Visible      := False ;
  btConsultaEndereco.Visible := False ;

end;

procedure TfrmClienteVarejo.btCancelarClick(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePage := Tab1 ;
  
end;

procedure TfrmClienteVarejo.DBEdit28Enter(Sender: TObject);
begin
  inherited;

  cCepAux := DBEdit28.Text ;
  
end;

procedure TfrmClienteVarejo.DBEdit28Exit(Sender: TObject);
begin
  inherited;

  if (qryEndereco.State <> dsBrowse) and (Trim(DBEdit28.Text) <> '') and (Trim(DBEDit28.Text) <> cCepAUX) then
  begin
      qryBuscaCEP.Close ;
      PosicionaQuery(qryBuscaCEP, DBEdit28.Text) ;

      qryEnderecocEndereco.Value    := '' ;
      qryEnderecocBairro.Value      := '' ;
      qryEnderecocCidade.Value      := '' ;
      qryEnderecocUF.Value          := '' ;
      qryEnderecoiNumero.Value      := 0  ;
      qryEnderecocComplemento.Value := '' ;

      if not qryBuscaCEP.Eof then
      begin
      
          qryEnderecocEndereco.Value := qryBuscaCEPcEndereco.Value ;
          qryEnderecocBairro.Value   := qryBuscaCEPcBairro.Value   ;
          qryEnderecocCidade.Value   := qryBuscaCEPcCidade.Value   ;
          qryEnderecocUF.Value       := qryBuscaCEPcUF.Value       ;

          DBEdit30.SetFocus ;

      end ;

  end ;

end;


procedure TfrmClienteVarejo.DBEdit27Exit(Sender: TObject);
begin
  inherited;

  if (qryPessoaFisica.State <> dsBrowse) and (Trim(DBEdit27.Text) <> '') and (Trim(DBEDit27.Text) <> cCepAUX) then
  begin
      qryBuscaCEP.Close ;
      PosicionaQuery(qryBuscaCEP, DBEdit27.Text) ;

      qryPessoaFisicacEnderecoEmpTrab.Value   := '' ;
      qryPessoaFisicacBairroEmpTrab.Value     := '' ;
      qryPessoaFisicacCidadeEmpTrab.Value     := '' ;
      qryPessoaFisicacUFEmpTrab.Value         := '' ;
      qryPessoaFisicaiNrEnderecoEmpTrab.Value := 0 ;

      if not qryBuscaCEP.Eof then
      begin
          qryPessoaFisicacEnderecoEmpTrab.Value := qryBuscaCEPcEndereco.Value ;
          qryPessoaFisicacBairroEmpTrab.Value   := qryBuscaCEPcBairro.Value   ;
          qryPessoaFisicacCidadeEmpTrab.Value   := qryBuscaCEPcCidade.Value   ;
          qryPessoaFisicacUFEmpTrab.Value       := qryBuscaCEPcUF.Value       ;
          DBEdit42.SetFocus ;
      end ;

  end ;

end;

procedure TfrmClienteVarejo.DBEdit27Enter(Sender: TObject);
begin
  inherited;
  cCepAux := DBEdit27.Text ;

end;

procedure TfrmClienteVarejo.DesativaEndereco;
begin

    DBEDit28.ReadOnly       := True  ;
    DBEdit29.ReadOnly       := true  ;
    DBEdit30.ReadOnly       := True  ;
    DBEdit34.ReadOnly       := True  ;
    DBEdit31.ReadOnly       := True  ;
    DBEdit32.ReadOnly       := True  ;
    DBEdit33.ReadOnly       := True  ;
    DBEdit35.ReadOnly       := True  ;
    DBEdit36.ReadOnly       := True  ;
    DBEdit37.ReadOnly       := True  ;
    DBEdit38.ReadOnly       := True  ;
    DBEdit39.ReadOnly       := True  ;
    DBEdit40.ReadOnly       := True  ;

    DBEDit28.Color          := $00E9E4E4  ;
    DBEdit29.Color          := $00E9E4E4  ;
    DBEdit30.Color          := $00E9E4E4  ;
    DBEdit34.Color          := $00E9E4E4  ;
    DBEdit31.Color          := $00E9E4E4  ;
    DBEdit32.Color          := $00E9E4E4  ;
    DBEdit33.Color          := $00E9E4E4  ;
    DBEdit35.Color          := $00E9E4E4  ;
    DBEdit36.Color          := $00E9E4E4  ;
    DBEdit37.Color          := $00E9E4E4  ;
    DBEdit38.Color          := $00E9E4E4  ;
    DBEdit39.Color          := $00E9E4E4  ;
    DBEdit40.Color          := $00E9E4E4  ;

    ComboTipoResid.ReadOnly  := True ;
    ComboComprovCRS.ReadOnly := True ;

end;

procedure TfrmClienteVarejo.AtivaEndereco;
begin

    DBEDit28.ReadOnly       := False  ;
    DBEdit30.ReadOnly       := False  ;
    DBEdit34.ReadOnly       := False  ;
    DBEdit35.ReadOnly       := False  ;
    DBEdit36.ReadOnly       := False  ;
    DBEdit37.ReadOnly       := False  ;
    DBEdit38.ReadOnly       := False  ;
    DBEdit39.ReadOnly       := False  ;
    DBEdit40.ReadOnly       := False  ;

    DBEDit28.Color          := clWhite  ;
    DBEdit30.Color          := clWhite  ;
    DBEdit34.Color          := clWhite  ;
    DBEdit35.Color          := clWhite  ;
    DBEdit36.Color          := clWhite  ;
    DBEdit37.Color          := clWhite  ;
    DBEdit38.Color          := clWhite  ;
    DBEdit39.Color          := clWhite  ;
    DBEdit40.Color          := clWhite  ;

    if (cEnderecoManual = 'S') then
    begin
        DBEdit29.ReadOnly       := False  ;
        DBEdit31.ReadOnly       := False  ;
        DBEdit32.ReadOnly       := False  ;
        DBEdit33.ReadOnly       := False  ;

        DBEdit29.Color          := clWhite  ;
        DBEdit31.Color          := clWhite  ;
        DBEdit32.Color          := clWhite  ;
        DBEdit33.Color          := clWhite  ;
    end ;

    ComboTipoResid.ReadOnly  := False ;
    ComboComprovCRS.ReadOnly := False ;

    btConsultaEndereco.Visible := True ;

end;

procedure TfrmClienteVarejo.btAltEnderecoClick(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) and (qryMaster.RecordCount > 0) then
  begin
      qryEndereco.Edit ;
      qryEndereconCdStatus.Value := 2 ;
      qryEndereco.Post ;

      qryEndereco.Insert ;

      AtivaEndereco() ;

      DBEdit28.SetFocus ;

  end ;



end;

procedure TfrmClienteVarejo.DBEdit46Exit(Sender: TObject);
begin
  inherited;

  if (DBEdit46.Text = DBEdit10.Text) then
  begin
      MensagemAlerta('CPF do c�njuge n�o pode ser igual CPF do Cliente.') ;
      DBEdit46.SetFocus ;
      abort ;
  end ;

end;

procedure TfrmClienteVarejo.qrySPCPessoaFisicaAfterPost(DataSet: TDataSet);
begin
  inherited;

  if (qrySPCPessoaFisicacFlgRestricao.Value = 1) then
  begin
      qryPessoaFisica.Edit ;
      qrypessoaFisicanCdTabTipoSituacao.Value := 4 ;
      qryPessoaFisica.Post ;
  end ;

  if (qrySPCPessoaFisicacFlgRestricao.Value = 0) and (qryPessoaFisicanCdUsuarioAprova.Value > 0) then
  begin
      qryPessoaFisica.Edit ;
      qrypessoaFisicanCdTabTipoSituacao.Value := 2 ;
      qryPessoaFisica.Post ;
  end ;

  if (qrySPCPessoaFisicacFlgRestricao.Value = 0) and (qryPessoaFisicanCdUsuarioAprova.Value = 0) and (qryPessoaFisicanCdTabTipoSituacao.Value <> 1 ) then
  begin
      qryPessoaFisica.Edit ;
      qrypessoaFisicanCdTabTipoSituacao.Value := 1 ;
      qryPessoaFisica.Post ;
  end ;

end;

procedure TfrmClienteVarejo.btAltTelefoneClick(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) and (qryMaster.RecordCount > 0) then
  begin

    DBEdit38.ReadOnly       := False  ;
    DBEdit39.ReadOnly       := False  ;

    DBEdit38.Color          := clWhite  ;
    DBEdit39.Color          := clWhite  ;

    DBEdit38.SetFocus ;

  end ;

end;

procedure TfrmClienteVarejo.btConsultaEnderecoClick(Sender: TObject);
begin
  inherited;

  frmConsultaEndereco.ShowModal() ;

  if not frmConsultaEndereco.qryConsultaEndereco.Eof then
  begin
      qryEnderecocEndereco.Value := frmConsultaEndereco.qryConsultaEnderecocEndereco.Value ;
      qryEnderecocCEP.Value      := frmConsultaEndereco.qryConsultaEnderecocCEP.Value      ;
      qryEnderecocBairro.Value   := frmConsultaEndereco.qryConsultaEnderecocBairro.Value   ;
      qryEnderecocCidade.Value   := frmConsultaEndereco.qryConsultaEnderecocCidade.Value   ;
      qryEnderecocUF.Value       := frmConsultaEndereco.edtUF.Text                         ;

      DBEdit30.SetFocus ;
  end ;

end;

procedure TfrmClienteVarejo.btConsultaEndereco2Click(Sender: TObject);
begin
  inherited;

  frmConsultaEndereco.ShowModal() ;

  if not frmConsultaEndereco.qryConsultaEndereco.Eof then
  begin
      qryPessoaFisica.Edit ;
      qryPessoaFisicacEnderecoEmpTrab.Value := frmConsultaEndereco.qryConsultaEnderecocEndereco.Value ;
      qryPessoaFisicacCEPEmpTrab.Value      := frmConsultaEndereco.qryConsultaEnderecocCEP.Value      ;
      qryPessoaFisicacBairroEmpTrab.Value   := frmConsultaEndereco.qryConsultaEnderecocBairro.Value   ;
      qryPessoaFisicacCidadeEmpTrab.Value   := frmConsultaEndereco.qryConsultaEnderecocCidade.Value   ;
      qryPessoaFisicacUFEmpTrab.Value       := frmConsultaEndereco.edtUF.Text                         ;

      DBEdit42.SetFocus ;
  end ;
end;

procedure TfrmClienteVarejo.AtivaLimite;
begin

    DBEdit87.ReadOnly  := False  ;
    DBEDit87.Color     := clWhite  ;

end;

procedure TfrmClienteVarejo.DesativaLimite;
begin

    DBEdit87.ReadOnly  := True  ;
    DBEDit87.Color     := $00E9E4E4  ;

end;

initialization
    RegisterClass(TfrmClienteVarejo) ;

end.
