unit fCadTransacaoCartao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons;

type
  TfrmCadTransacaoCartao = class(TfrmCadastro_Padrao)
    qryMasternCdTransacaoCartao: TAutoIncField;
    qryMasterdDtTransacao: TDateTimeField;
    qryMasterdDtCredito: TDateTimeField;
    qryMasteriNrCartao: TLargeintField;
    qryMasteriNrDocto: TIntegerField;
    qryMasteriNrAutorizacao: TIntegerField;
    qryMasternCdOperadoraCartao: TIntegerField;
    qryMasternCdLanctoFin: TIntegerField;
    qryMasternValTransacao: TBCDField;
    qryMasternCdContaBancaria: TIntegerField;
    qryMastercFlgConferencia: TIntegerField;
    qryMasternCdUsuarioConf: TIntegerField;
    qryMasterdDtConferencia: TDateTimeField;
    qryMasternCdStatusDocto: TIntegerField;
    qryMasteriParcelas: TIntegerField;
    qryMastercNrLotePOS: TStringField;
    qryMasternCdGrupoOperadoraCartao: TIntegerField;
    qryMastercFlgPOSEncerrado: TIntegerField;
    qryMasternCdLojaCartao: TIntegerField;
    qryMasternValTaxaOperadora: TBCDField;
    qryMasternCdCondPagtoCartao: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    qryStatusDocto: TADOQuery;
    qryCondPagto: TADOQuery;
    qryOperCartao: TADOQuery;
    qryLojaCartao: TADOQuery;
    qryContaBancaria: TADOQuery;
    qryStatusDoctonCdStatusDocto: TIntegerField;
    qryStatusDoctocNmStatusDocto: TStringField;
    DBEdit14: TDBEdit;
    DataSource1: TDataSource;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryCondPagtoiQtdeParcelas: TIntegerField;
    DBEdit15: TDBEdit;
    DataSource2: TDataSource;
    DBEdit11: TDBEdit;
    qryOperCartaonCdOperadoraCartao: TIntegerField;
    qryOperCartaocNmOperadoraCartao: TStringField;
    qryOperCartaonCdGrupoOperadoraCartao: TIntegerField;
    qryOperCartaocNmGrupoOperadoraCartao: TStringField;
    DBEdit16: TDBEdit;
    DataSource3: TDataSource;
    DBEdit13: TDBEdit;
    DBEdit17: TDBEdit;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    DBEdit18: TDBEdit;
    DataSource4: TDataSource;
    qryLojaCartaonCdLoja: TIntegerField;
    qryLojaCartaocNmLoja: TStringField;
    DBEdit19: TDBEdit;
    DataSource5: TDataSource;
    Label14: TLabel;
    qryOperCartaoiFloating: TIntegerField;
    qryMastercFlgInclusoManual: TIntegerField;
    qryMastercFlgGeradoFinanceiro: TIntegerField;
    SP_GERA_TITULO_CARTAO: TADOStoredProc;
    btEncerrar: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit9Exit(Sender: TObject);
    procedure DBEdit10Exit(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure btEncerrarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadTransacaoCartao: TfrmCadTransacaoCartao;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmCadTransacaoCartao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TRANSACAOCARTAO' ;
  nCdTabelaSistema  := 184 ;
  nCdConsultaPadrao := 731 ;

  bLimpaAposSalvar := false;

end;

procedure TfrmCadTransacaoCartao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryLojaCartao.Close;
  qryLojaCartao.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLojaCartao,qryMasternCdLojaCartao.AsString);

  qryCondPagto.Close;
  PosicionaQuery(qryCondPagto,qryMasternCdCondPagtoCartao.AsString);

  qryOperCartao.Close;
  PosicionaQuery(qryOperCartao,qryMasternCdOperadoraCartao.AsString);

  qryContaBancaria.Close;
  qryContaBancaria.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva;
  PosicionaQuery(qryContaBancaria,qryMasternCdContaBancaria.AsString);

  qryStatusDocto.Close;
  PosicionaQuery(qryStatusDocto,qryMasternCdStatusDocto.AsString);

  btEncerrar.Enabled := true;
  btSalvar.Visible   := true;
  btExcluir.Visible  := true;

  if (qryMastercFlgGeradoFinanceiro.Value = 1) then
  begin
      btEncerrar.Enabled := false;
      btSalvar.Visible   := false;
      btExcluir.Visible  := false;
  end;
  
end;

procedure TfrmCadTransacaoCartao.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  if not qryMaster.Active then
      Exit;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLojaCartao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCadTransacaoCartao.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  if not qryMaster.Active then
      Exit;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(732);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagtoCartao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCadTransacaoCartao.DBEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  if not qryMaster.Active then
      Exit;
  
  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(149,'nCdStatus = 1');

            If (nPK > 0) then
            begin
                qryMasternCdOperadoraCartao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCadTransacaoCartao.DBEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  if not qryMaster.Active then
      Exit;
      
  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'cFlgCofre = 1  AND nCdLoja = @@Loja' );

            If (nPK > 0) then
            begin
                qryMasternCdContaBancaria.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCadTransacaoCartao.DBEdit2Exit(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      Exit;

  qryLojaCartao.Close;
  qryLojaCartao.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLojaCartao,qryMasternCdLojaCartao.AsString);
end;

procedure TfrmCadTransacaoCartao.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      Exit;

  qryCondPagto.Close;
  PosicionaQuery(qryCondPagto,qryMasternCdCondPagtoCartao.AsString);

end;

procedure TfrmCadTransacaoCartao.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      Exit;

  qryOperCartao.Close;
  PosicionaQuery(qryOperCartao,qryMasternCdOperadoraCartao.AsString);

end;

procedure TfrmCadTransacaoCartao.DBEdit9Exit(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      Exit;

  qryContaBancaria.Close;
  qryContaBancaria.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva;
  PosicionaQuery(qryContaBancaria,qryMasternCdContaBancaria.AsString);

end;

procedure TfrmCadTransacaoCartao.DBEdit10Exit(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      Exit;

  qryStatusDocto.Close;
  PosicionaQuery(qryStatusDocto,qryMasternCdStatusDocto.AsString);

end;

procedure TfrmCadTransacaoCartao.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryLojaCartao.Close;
  qryCondPagto.Close;
  qryOperCartao.Close;
  qryContaBancaria.Close;
  qryStatusDocto.Close;

end;

procedure TfrmCadTransacaoCartao.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBEdit2.Text = '') then 
  begin
      MensagemAlerta('Selecione uma loja para a transa��o');
      DBEdit2.SetFocus;
      abort;
  end;

  if (DBEdit4.Text = '') then
  begin
      MensagemAlerta('Selecione uma condi��o de pagamento para a transa��o');
      DBEdit4.SetFocus;
      abort;
  end;

  if (DBEdit7.Text = '') then
  begin
      MensagemAlerta('Selecione uma Operadora de Cart�o');
      DBEdit7.SetFocus;
      abort;
  end;

  if (DBEdit9.Text = '') then
  begin
      MensagemAlerta('Selecione uma conta cofre para a transa��o');
      DBEdit9.SetFocus;
      abort;
  end;

  if (DBEdit5.Text = '') then
  begin
      MensagemAlerta('Digite o n�mero do Documento');
      DBEdit5.SetFocus;
      abort;
  end;

  if (DBEdit6.Text = '') then
  begin
      MensagemAlerta('Digite o n�mero da Autoriza��o');
      DBEdit6.SetFocus;
      abort;
  end;

  if (DBEdit10.Text = '') then
  begin
      MensagemAlerta('Selecione o Status do Documento');
      DBEdit10.SetFocus;
      abort;
  end;

  if (Trim(DBEdit3.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data da transa��o');
      DBEdit3.SetFocus;
      abort;
  end;

  if (qryMasternValTransacao.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da transa��o');
      DBEdit8.SetFocus;
      abort;
  end;

  qryMastercFlgInclusoManual.Value       := 1;
  qryMasterdDtCredito.Value              := qryMasterdDtTransacao.Value + qryOperCartaoiFloating.Value;
  qryMasteriParcelas.Value               := qryCondPagtoiQtdeParcelas.Value;
  qryMasternCdGrupoOperadoraCartao.Value := qryOperCartaonCdGrupoOperadoraCartao.Value;
  qryMastercFlgPOSEncerrado.Value        := 1;

  inherited;

end;

procedure TfrmCadTransacaoCartao.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmCadTransacaoCartao.btEncerrarClick(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMasternCdTransacaoCartao.AsString = '') then
  begin
      MensagemAlerta('Salve a transa��o antes de gerar o t�tulo financeiro');
      exit;
  end;

  if (MessageDlg('Deseja gerar o t�tulo financeiro para esta transa��o ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;
  if (MessageDlg('Essa a��o n�o poder� ser cancelada. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      SP_GERA_TITULO_CARTAO.Close;
      SP_GERA_TITULO_CARTAO.Parameters.ParamByName('@nCdTransacaoCartao').Value := qryMasternCdTransacaoCartao.Value;
      SP_GERA_TITULO_CARTAO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('T�tulo Financeiro gerado com sucesso.') ;

  btCancelar.Click;
  DBEdit1.SetFocus;
end;

initialization
  RegisterClass(TfrmCadTransacaoCartao);

end.
