inherited frmAplicacao_CONS: TfrmAplicacao_CONS
  Caption = 'Aplica'#231#227'o do Menu'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton3: TToolButton
      OnClick = ToolButton3Click
    end
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  inherited DBGridEh1: TDBGridEh
    OnDblClick = DBGridEh1DblClick
  end
  inherited ADOQuery1: TADOQuery
    SQL.Strings = (
      'SELECT TOP 0 * FROM APLICACAO')
    object ADOQuery1nCdAplicacao: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdAplicacao'
    end
    object ADOQuery1cNmAplicacao: TStringField
      DisplayLabel = 'Nome Aplica'#231#227'o'
      FieldName = 'cNmAplicacao'
      Size = 50
    end
    object ADOQuery1nCdStatus: TIntegerField
      DisplayLabel = 'Status'
      FieldName = 'nCdStatus'
    end
    object ADOQuery1nCdModulo: TIntegerField
      DisplayLabel = 'C'#243'digo M'#243'dulo'
      FieldName = 'nCdModulo'
    end
    object ADOQuery1cNmModulo: TStringField
      DisplayLabel = 'Nome M'#243'dulo'
      FieldKind = fkLookup
      FieldName = 'cNmModulo'
      LookupDataSet = ADOQuery2
      LookupKeyFields = 'nCdModulo'
      LookupResultField = 'cNmModulo'
      KeyFields = 'nCdModulo'
      Lookup = True
    end
  end
  object ADOQuery2: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM MODULO')
    Left = 640
    Top = 280
    object ADOQuery2nCdModulo: TIntegerField
      FieldName = 'nCdModulo'
    end
    object ADOQuery2cNmModulo: TStringField
      FieldName = 'cNmModulo'
      Size = 50
    end
    object ADOQuery2nCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
end
