unit rVendaxVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, ER2Lookup, DBCtrls;

type
  TrptVendaxVenda = class(TfrmProcesso_Padrao)
    ER2LookupMaskEdit1: TER2LookupMaskEdit;
    ER2LookupMaskEdit2: TER2LookupMaskEdit;
    qryLoja: TADOQuery;
    qryTerceiro: TADOQuery;
    dsTerceiro: TDataSource;
    dsLoja: TDataSource;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit2: TDBEdit;
    edtDtFinal: TMaskEdit;
    Label11: TLabel;
    edtDtInicial: TMaskEdit;
    Label12: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    RadioGroup1: TRadioGroup;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaxVenda: TrptVendaxVenda;

implementation

{$R *.dfm}
uses
 fMenu,rVendaxVenda_View;

procedure TrptVendaxVenda.ToolButton1Click(Sender: TObject);
var
  ObjRel : TrptVendaxVenda_View;
  cFiltro : string;
begin

  if (edtDtInicial.Text <> '  /  /    ') then
  begin
      if (edtDtInicial.Text  > edtDtFinal.Text) then
      begin
          MensagemAlerta('Data de abertura inicial maior que data final.');
          edtDtInicial.Focused;
          abort;
      end;
  end;

  if (DBEdit1.Text <> '') then
    cFiltro := 'Loja: ' +  DBEdit1.Text
  else
    cFiltro := 'Loja: TODOS' ;

  if (DBEdit2.Text <> '') then
    cFiltro := cFiltro + ' Terceiro: ' +  DBEdit2.Text
  else
    cFiltro := cFiltro + ' Terceiro: TODOS';

  if (edtDtInicial.Text <> '  /  /    ') then
    cFiltro := cFiltro + ' Data Inicial: ' +  edtDtInicial.Text
  else
    cFiltro := cFiltro + ' Data Inicial: TODOS';

  if (edtDtInicial.Text <> '  /  /    ') then
    cFiltro := cFiltro + ' Data Fim: ' +  edtDtFinal.Text
  else
    cFiltro := cFiltro + ' Data Fim: TODOS';

  try

      ObjRel := TrptVendaxVenda_View.Create(nil);
      objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

      ObjRel.qryRelatorio.Close;
      ObjRel.qryRelatorio.Parameters.ParamByName('nCdLoja').Value     := frmMenu.ConvInteiro(ER2LookupMaskEdit1.Text);
      ObjRel.qryRelatorio.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(ER2LookupMaskEdit2.Text);
      ObjRel.qryRelatorio.Parameters.ParamByName('dDtIni').Value      := frmMenu.ConvData(edtDtInicial.Text);
      ObjRel.qryRelatorio.Parameters.ParamByName('dDtFim').Value      := frmMenu.ConvData(edtDtFinal.Text);
      ObjRel.qryRelatorio.Open;


      case (RadioGroup1.ItemIndex) of
      0 : begin
            ObjRel.lblFiltro1.Caption := cFiltro;

            ObjRel.QuickRep1.PreviewModal;
          end;
       1: begin
              frmMenu.ExportExcelDinamic('Rel. Compras Cliente - Per�odo',cFiltro,'',ObjRel.qryRelatorio);
          end;   
      end;
  except
      MensagemErro('Erro no processamento.');
      raise;
  end;


end;

initialization
    RegisterClass(TrptVendaxVenda) ;

end.
