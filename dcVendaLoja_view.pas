unit dcVendaLoja_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, PivotMap_SRC, PivotCube_SRC, PivotGrid_SRC, PivotToolBar_SRC,comObj, ExcelXP,
  Menus,Jpeg,PivotPaintner_SRC, GanttCh, zCube_TLB;

type
  TdcmVendaLoja_view = class(TfrmProcesso_Padrao)
    ADODataSet1: TADODataSet;
    PVRowToolBar1: TPVRowToolBar;
    PVMeasureToolBar1: TPVMeasureToolBar;
    PVColToolBar1: TPVColToolBar;
    PivotGrid1: TPivotGrid;
    PivotCube1: TPivotCube;
    PivotMap1: TPivotMap;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    PopupMenu1: TPopupMenu;
    AnalisePadrao: TMenuItem;
    CurvaABC1: TMenuItem;
    N20801: TMenuItem;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    ADODataSet1Ano: TDateTimeField;
    ADODataSet1Mes: TDateTimeField;
    ADODataSet1nCdLoja: TStringField;
    ADODataSet1cNmDepartamento: TStringField;
    ADODataSet1cNmCategoria: TStringField;
    ADODataSet1cNmSubCategoria: TStringField;
    ADODataSet1cNmSegmento: TStringField;
    ADODataSet1cNmMarca: TStringField;
    ADODataSet1cNmProdutoEstrut: TStringField;
    ADODataSet1cNmProdutoFinal: TStringField;
    ADODataSet1nQtdeVenda: TBCDField;
    ADODataSet1nValorCusto: TBCDField;
    ADODataSet1nValorVenda: TBCDField;
    ADODataSet1nValMC: TBCDField;
    ADODataSet1nValLB: TBCDField;
    ADODataSet1nQtdeEstoque: TBCDField;
    ADODataSet1dDtPedido: TDateTimeField;
    ADODataSet1cNmVendedor: TStringField;
    ADODataSet1cNmFormaPagto: TStringField;
    ADODataSet1cNmCondPagto: TStringField;
    ADODataSet1nCdPedido: TIntegerField;
    procedure ToolButton5Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure AnalisePadraoClick(Sender: TObject);
    procedure CurvaABC1Click(Sender: TObject);
    procedure PivotGrid1GetCellValue(Sender: TObject; X, Y, RowIndex,
      ColumnIndex, MeasureIndex, ViewIndex: Integer; var Value: Double;
      var CellString: String; Cell: TpvCellDrawing);
    procedure PivotGrid1GetColumnTotal(Sender: TObject; X, Y, RowIndex,
      ColumnIndex, MeasureIndex, ViewIndex: Integer; var Value: Double;
      var CellString: String; Cell: TpvCellDrawing);
    procedure PivotGrid1GetCubeTotal(Sender: TObject; X, Y, RowIndex,
      ColumnIndex, MeasureIndex, ViewIndex: Integer; var Value: Double;
      var CellString: String; Cell: TpvCellDrawing);
    procedure PivotGrid1GetRowTotal(Sender: TObject; X, Y, RowIndex,
      ColumnIndex, MeasureIndex, ViewIndex: Integer; var Value: Double;
      var CellString: String; Cell: TpvCellDrawing);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmVendaLoja_view: TdcmVendaLoja_view;
  nValorVenda : Double ;
  nValorCusto : Double ;

implementation

{$R *.dfm}
uses
 fMenu;

procedure TdcmVendaLoja_view.ToolButton5Click(Sender: TObject);
begin
  inherited;
  PivotGrid1.FAllowPaint := true;
  PivotGrid1.Print(Rect(20,5,5,5),True,pqFlat,false);
end;

procedure TdcmVendaLoja_view.MenuItem1Click(Sender: TObject);
var
  Planilha  : Olevariant;

begin
  inherited;

    try
        planilha:= CreateoleObject('Excel.Application');
    except
        MensagemErro('Ocorreu um erro no processamento da planilha, talvez o aplicativo de planilhas n�o esteja instalado corretamente.') ;
        exit ;
    end ;

    PivotGrid1.ExportToExcel(Planilha,True,True,True,'ER2Soft - An�lise de Vendas por Loja');

    Planilha.columns.Autofit;
    Planilha.Visible := True;

end;

procedure TdcmVendaLoja_view.MenuItem2Click(Sender: TObject);
var
   image : TJPEGImage;
   jpg_array : array of TJpegImage;
begin
    inherited;

   if PivotMap1.Active then
   begin
       image := TJPegImage.Create;
       Setlength(jpg_array,2);
       {PivotChart1.SaveToJpegImage(image);}
       jpg_array[0] := image;
       PivotGrid1.ExportToHTML('',True,True,jpg_array,'ER2Soft - An�lise de Vendas por Loja');  
       image.Free;
       setLength(jpg_array,0);
   end;

end;

procedure TdcmVendaLoja_view.AnalisePadraoClick(Sender: TObject);
var
   i,j : integer;
   d : TMapDimension;
   a,item : TMapDImensionItem;
begin

    d := nil;

    for i := 0 to PivotMap1.DimensionCount - 1 do
        if PivotMap1.Dimensions[i].AliasName = 'Ano' then
        begin
            d := PivotMap1.Dimensions[i];
            Break;
        end;

    if d = nil then
        Exit;

    if d.isHierarchy then
    begin

        for i := d.Count - 1 downto 0 do
        begin

            item := d.Items[i];

            if (item.ID >= $FFFFFA) and (item.ID <= $FFFFFC) then
            begin

                for j := item.Count - 1 downto 0 do
                begin

                    a := item.Items[j];
                    item.DeleteItem(a);
                    d.AddItem(a);

                end;

                d.DeleteItem(item);

            end;

        end;

        PivotMap1.RefreshData(True);

    end;

end;

procedure TdcmVendaLoja_view.CurvaABC1Click(Sender: TObject);
var
   i : integer;
   d : TMapDimension;
   a,b,c,item : TMapDImensionItem;
   total,f,avg : double;
begin

    AnalisePadraoClick(Sender);

    d := nil;

    for I := 0 to PivotMap1.DimensionCount - 1 do
        if PivotMap1.Dimensions[i].AliasName = 'cNmProdutoFinal' then
        begin
            d := PivotMap1.Dimensions[i];
            Break;
        end;

    if d = nil then
        Exit;

    total := PivotMap1.Total[0,mvtValue];
    avg := total/d.Count;
    a := TMapDimensionItem.Create;
    a.ID := $FFFFFA;
    a.Key := $FFFFFA;
    a.Name := 'A';
    a.Dimension := d;
    a.Description := '20% items which produce near 70% profit';
    a.State := disActive;
    a.isGroup := True;
    a.isCustomGroup := True;
    a.ExpandedToNextLevel := True;

    b := TMapDimensionItem.Create;
    b.ID := $FFFFFB;
    b.Key := $FFFFFB;
    b.Name := 'B';
    b.Description := '30% items which produce near 20% profit';
    b.Dimension := d;
    b.State := disActive;
    b.isGroup := True;
    b.isCustomGroup := True;
    b.ExpandedToNextLevel := True;

    c := TMapDimensionItem.Create;
    c.ID := $FFFFFC;
    c.Key := $FFFFFC;
    c.Name := 'C';
    c.Description := '50% items which produce near 10% profit';
    c.Dimension := d;
    c.State := disActive;
    c.isGroup := True;
    c.isCustomGroup := True;
    c.ExpandedToNextLevel := True;

    for i := d.Count - 1 downto 0 do
    begin
        item := d.Items[i];
        PivotCube1.Intf._GetItemValue(PivotMap1,d.FIndex,item.ID,0,f);
        d.DeleteItem(item);

        if f / avg  > 4 then
            a.AddItem(item)
        else
            if f / avg  > 0.5 then
                b.AddItem(item)
            else
                c.AddItem(item);
    end;

    d.AddItem(a);
    d.AddItem(b);
    d.AddItem(c);
    d.DefaultSort := ddsSortByNameA; // force resort!
    PivotMap1.RefreshData(True);

    ShowMessage('Dimension Ware structure was changed, so now you will see 3 separate groups (A,B,C) which was created by overall sales volume by each Ware item. Please note: Current dimension filters will used in group membership calculation!') ;

end;

procedure TdcmVendaLoja_view.PivotGrid1GetCellValue(Sender: TObject; X, Y,
  RowIndex, ColumnIndex, MeasureIndex, ViewIndex: Integer;
  var Value: Double; var CellString: String; Cell: TpvCellDrawing);
var
tmpMeasure: TMapMeasure;
begin
    tmpMeasure := PivotMap1.MeasureByIndex[MeasureIndex];

    if (tmpMeasure.AliasName = 'nValPercLB') then
    begin
        nValorCusto := PivotMap1.Cells[ColumnIndex, RowIndex, MeasureIndex-1, 0];
        nValorVenda := PivotMap1.Cells[ColumnIndex, RowIndex, MeasureIndex-2, 0];

        if (nValorCusto > 0) then
            Value := ((nValorVenda / nValorCusto) - 1 ) * 100 ;

        if (nValorCusto = 0) or (nValorVenda = 0) then
            Value := 0 ;
    end ;

end;

procedure TdcmVendaLoja_view.PivotGrid1GetColumnTotal(Sender: TObject; X,
  Y, RowIndex, ColumnIndex, MeasureIndex, ViewIndex: Integer;
  var Value: Double; var CellString: String; Cell: TpvCellDrawing);
var
tmpMeasure: TMapMeasure;
begin
    tmpMeasure := PivotMap1.MeasureByIndex[MeasureIndex];

    if (tmpMeasure.AliasName = 'nValPercLB') then
    begin
        nValorCusto := PivotMap1.Cells[ColumnIndex, RowIndex, MeasureIndex-1, 0];
        nValorVenda := PivotMap1.Cells[ColumnIndex, RowIndex, MeasureIndex-2, 0];

        if (nValorCusto > 0) then
            Value := ((nValorVenda / nValorCusto) - 1 ) * 100 ;

        if (nValorCusto = 0) or (nValorVenda = 0) then
            Value := 0 ;
    end ;

end;

procedure TdcmVendaLoja_view.PivotGrid1GetCubeTotal(Sender: TObject; X, Y,
  RowIndex, ColumnIndex, MeasureIndex, ViewIndex: Integer;
  var Value: Double; var CellString: String; Cell: TpvCellDrawing);
var
tmpMeasure: TMapMeasure;
begin
    tmpMeasure := PivotMap1.MeasureByIndex[MeasureIndex];

    if (tmpMeasure.AliasName = 'nValPercLB') then
    begin
        nValorCusto := PivotMap1.Cells[ColumnIndex, RowIndex, MeasureIndex-1, 0];
        nValorVenda := PivotMap1.Cells[ColumnIndex, RowIndex, MeasureIndex-2, 0];

        if (nValorCusto > 0) then
            Value := ((nValorVenda / nValorCusto) - 1 ) * 100 ;

        if (nValorCusto = 0) or (nValorVenda = 0) then
            Value := 0 ;
    end ;

end;

procedure TdcmVendaLoja_view.PivotGrid1GetRowTotal(Sender: TObject; X, Y,
  RowIndex, ColumnIndex, MeasureIndex, ViewIndex: Integer;
  var Value: Double; var CellString: String; Cell: TpvCellDrawing);
var
tmpMeasure: TMapMeasure;
begin
    tmpMeasure := PivotMap1.MeasureByIndex[MeasureIndex];

    if (tmpMeasure.AliasName = 'nValPercLB') then
    begin
        nValorCusto := PivotMap1.Cells[ColumnIndex, RowIndex, MeasureIndex-1, 0];
        nValorVenda := PivotMap1.Cells[ColumnIndex, RowIndex, MeasureIndex-2, 0];

        if (nValorCusto > 0) then
            Value := ((nValorVenda / nValorCusto) - 1 ) * 100 ;

        if (nValorCusto = 0) or (nValorVenda = 0) then
            Value := 0 ;
    end ;

end;

end.
