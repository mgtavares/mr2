unit rRecebimento_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptRecebimento_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand3: TQRBand;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText9: TQRDBText;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRDBText7: TQRDBText;
    QRDBText12: TQRDBText;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    qryMaster: TADOQuery;
    qryMasternCdRecebimento: TIntegerField;
    qryMasternCdTipoReceb: TIntegerField;
    qryMastercNmTipoReceb: TStringField;
    qryMasternCdLoja: TAutoIncField;
    qryMastercNmLoja: TStringField;
    qryMasterdDtReceb: TDateTimeField;
    qryMastercNrDocto: TStringField;
    qryMasterdDtDocto: TDateTimeField;
    qryMasternValDocto: TBCDField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNmTerceiro: TStringField;
    qryMasternCdTabStatusReceb: TIntegerField;
    qryMastercNmTabStatusReceb: TStringField;
    qryMastercNmUsuario: TStringField;
    qryMasterdDtFech: TDateTimeField;
    qryMastercOBS: TStringField;
    qryMastercNmTipoItemPed: TStringField;
    qryMasternCdProduto: TIntegerField;
    qryMastercNmItem: TStringField;
    qryMasternQtde: TBCDField;
    qryMasternValTotal: TBCDField;
    QRExpr1: TQRExpr;
    QRImage1: TQRImage;
    QRLabel14: TQRLabel;
    QRLabel22: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    qryMastercNmProdutoPai: TStringField;
    QRGroup2: TQRGroup;
    QRShape7: TQRShape;
    QRDBText2: TQRDBText;
    qryMasternValTotalNF: TBCDField;
    qryMasternValBaseICMS: TBCDField;
    qryMasternValICMS: TBCDField;
    qryMasternValIsenta: TBCDField;
    qryMasternValOutras: TBCDField;
    qryMasternValIPI: TBCDField;
    qryMasternValICMSSub: TBCDField;
    qryMasternValDespesas: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMastercCFOP: TStringField;
    qryMastercCNPJEmissor: TStringField;
    qryMastercIEEmissor: TStringField;
    qryMastercUFEmissor: TStringField;
    qryMastercModeloNF: TStringField;
    qryMastercSerieDocto: TStringField;
    qryMasternValUnitario: TFloatField;
    qryMasternValUnitarioEsp: TFloatField;
    qryMasternValCustoSemDesconto: TFloatField;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRDBText14: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText15: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText17: TQRDBText;
    QRLabel7: TQRLabel;
    QRDBText18: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText19: TQRDBText;
    QRLabel9: TQRLabel;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRDBText22: TQRDBText;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel23: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel24: TQRLabel;
    QRDBText8: TQRDBText;
    QRLabel25: TQRLabel;
    QRDBText11: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel26: TQRLabel;
    QRDBText23: TQRDBText;
    QRLabel27: TQRLabel;
    QRDBText24: TQRDBText;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRLabel30: TQRLabel;
    QRDBText27: TQRDBText;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRLabel33: TQRLabel;
    QRDBText30: TQRDBText;
    QRLabel34: TQRLabel;
    QRDBText31: TQRDBText;
    QRLabel35: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel37: TQRLabel;
    qryMasternCdItemPedido: TIntegerField;
    qryMasternCdPedido: TIntegerField;
    QRLabel13: TQRLabel;
    QRLabel38: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText32: TQRDBText;
    qryMasternValVenda: TFloatField;
    QRLabel39: TQRLabel;
    QRDBText33: TQRDBText;
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRecebimento_view: TrptRecebimento_view;

implementation

uses fMenu;

{$R *.dfm}

procedure TrptRecebimento_view.QuickRep1BeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin

    QRDBText2.Mask  := frmMenu.cMascaraCompras;
    QRDBText12.Mask := frmMenu.cMascaraCompras;

end;

end.
