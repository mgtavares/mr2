unit fSP_ProvisaoAberta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, cxPC, cxControls,
  StdCtrls, DBCtrls;

type
  TfrmSP_ProvisaoAberta = class(TfrmProcesso_Padrao)
    qryProvisoes: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DataSource1: TDataSource;
    qryProvisoesnCdEmpresa: TIntegerField;
    qryProvisoesnCdLojaTit: TIntegerField;
    qryProvisoescNrTit: TStringField;
    qryProvisoescNmEspTit: TStringField;
    qryProvisoescNmCategFinanc: TStringField;
    qryProvisoesdDtVenc: TDateTimeField;
    qryProvisoesnValTit: TBCDField;
    qryProvisoescOBSTit: TMemoField;
    GroupBox1: TGroupBox;
    DBMemo1: TDBMemo;
    qryProvisoesnCdTitulo: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSP_ProvisaoAberta: TfrmSP_ProvisaoAberta;

implementation

uses fTituloProv ;

{$R *.dfm}

procedure TfrmSP_ProvisaoAberta.FormShow(Sender: TObject);
begin
  inherited;

  DbGridEh1.SetFocus;
  
end;

procedure TfrmSP_ProvisaoAberta.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  if ( Key = VK_ESCAPE ) then
      Self.Close;
      
end;

procedure TfrmSP_ProvisaoAberta.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
var
  R : TRect;
begin

    R := Rect;
    Dec(R.Bottom,2);

    if Column.Field = qryProvisoes.FieldByName('cOBSTit') then
    begin

        if not (gdSelected in State) then
            DBGridEh1.Canvas.FillRect(Rect);

        DBGridEh1.Canvas.TextRect(R,R.Left,R.Top, qryProvisoes.FieldByName('cOBSTit').AsString);

    end;

end;

procedure TfrmSP_ProvisaoAberta.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmTituloProv ;
begin
  inherited;

  if not qryProvisoes.IsEmpty then
  begin

      objForm := TfrmTituloProv.Create( Self ) ;
      objForm.PosicionaQuery( objForm.qryMaster , qryProvisoesnCdTitulo.asString );

      showForm( objForm , TRUE ) ;

      qryProvisoes.Close;
      qryProvisoes.Open;

  end ;

end;

end.
