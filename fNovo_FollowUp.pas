unit fNovo_FollowUp;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, DBCtrls, DB, Mask, ADODB, ImgList,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmNovo_FollowUp = class(TfrmProcesso_Padrao)
    qryInsereFollow: TADOQuery;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    qryTempFollowUP: TADOQuery;
    MaskEdit1: TMaskEdit;
    Memo1: TMemo;
    Edit1: TEdit;
    qryFollow: TADOQuery;
    qryFollownCdFollowUp: TAutoIncField;
    qryFollownCdPedido: TIntegerField;
    qryFollownCdTitulo: TIntegerField;
    qryFollowdDtFollowUp: TDateTimeField;
    qryFollownCdUsuario: TIntegerField;
    qryFollowcOcorrenciaResum: TStringField;
    qryFollowcOcorrencia: TMemoField;
    qryFollowdDtProxAcao: TDateTimeField;
    qryFollowcFlgOK: TIntegerField;
    qryFollownCdTerceiro: TIntegerField;
    qryFollownCdCheque: TIntegerField;
    qryTempFollowUPnCdPedido: TIntegerField;
    qryTempFollowUPdDtFollowUp: TDateTimeField;
    qryTempFollowUPnCdUsuario: TIntegerField;
    qryTempFollowUPcOcorrenciaResum: TStringField;
    qryTempFollowUPcOcorrencia: TMemoField;
    qryTempFollowUPdDtProxAcao: TDateTimeField;
    MaskEdit2: TMaskEdit;
    Label4: TLabel;
    MaskEdit3: TMaskEdit;
    Label5: TLabel;
    qryAlteraPrevisaoPedido: TADOQuery;
    qryPreparaTemp: TADOQuery;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdPedido: Integer ;
    nCdTitulo: Integer ;
  end;

var
  frmNovo_FollowUp: TfrmNovo_FollowUp;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmNovo_FollowUp.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(Edit1.Text) = '') then
  begin
      ShowMessage('Informe a descri��o resumida.') ;
      Edit1.SetFocus ;
      Abort ;
  end ;

  if ((Trim(MaskEdit2.Text) <> '/  /') and (Trim(MaskEdit3.Text) = '/  /'))then
  begin
      ShowMessage('Para informar uma nova previs�o de entrega, informe a data final.') ;
      MaskEdit3.SetFocus ;
      Abort ;
  end;

  if ((Trim(MaskEdit2.Text) = '/  /') and (Trim(MaskEdit3.Text) <> '/  /'))then
  begin
      ShowMessage('Para informar uma nova previs�o de entrega, informe a data inicial.') ;
      MaskEdit2.SetFocus ;
      Abort ;
  end;

  if ((Trim(MaskEdit2.Text) <> '/  /') and (Trim(MaskEdit3.Text) <> '/  /'))then
  begin
      if (strToDate(MaskEdit2.Text) > strToDate(MaskEdit3.Text)) then
      begin
          MensagemAlerta('Data final menor que data inicial.') ;
          MaskEdit2.SetFocus;
          Abort ;
      end ;
  end;

  if (MessageDlg('Confirma a gera��o desta ocorr�ncia ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans ;

  try
      qryInsereFollow.Close;
      qryInsereFollow.Parameters.ParamByName('cOcorrenciaResum').Value := Edit1.Text;
      qryInsereFollow.Parameters.ParamByName('cOcorrencia').Value      := Memo1.Text;

      if (Trim(MaskEdit1.Text) = '/  /') then
          qryInsereFollow.Parameters.ParamByName('dDtProxAcao').Value  := Null
      else qryInsereFollow.Parameters.ParamByName('dDtProxAcao').Value := MaskEdit1.Text;

      qryInsereFollow.ExecSQL;

      if ((Trim(MaskEdit2.Text) <> '/  /') and (Trim(MaskEdit3.Text) <> '/  /'))then
      begin

          qryTempFollowUP.First;

          while not qryTempFollowUP.Eof do
          begin
              qryAlteraPrevisaoPedido.Close;
              qryAlteraPrevisaoPedido.Parameters.ParamByName('nCdPedido').Value  := qryTempFollowUPnCdPedido.Value;
              qryAlteraPrevisaoPedido.Parameters.ParamByName('dDtInicial').Value := MaskEdit2.Text;
              qryAlteraPrevisaoPedido.Parameters.ParamByName('dDtFinal').Value   := MaskEdit3.Text;
              qryAlteraPrevisaoPedido.ExecSQL;

              qryTempFollowUP.Next;
          end;
          
      end;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  Close ;

end;

procedure TfrmNovo_FollowUp.FormShow(Sender: TObject);
begin
  inherited;
  Edit1.SetFocus ;

end;

end.
