inherited frmProdutoERPInsumos: TfrmProdutoERPInsumos
  Left = 118
  Top = 128
  Width = 994
  BorderIcons = [biSystemMenu]
  Caption = 'Estrutura do Produto'
  OldCreateOrder = True
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 978
  end
  inherited ToolBar1: TToolBar
    Width = 978
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 978
    Height = 435
    ActivePage = cxTabSheet2
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 431
    ClientRectLeft = 4
    ClientRectRight = 974
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Estrutura para Compra'
      ImageIndex = 0
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 970
        Height = 407
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsFormulaProduto
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh3KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdprodutoPai'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            Width = 87
          end
          item
            EditButtons = <>
            FieldName = 'cNmProduto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 301
          end
          item
            EditButtons = <>
            FieldName = 'cUnidadeMedidaEstoque'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'cUnidadeMedida'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'nFatorConversaoUM'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Tag = 1
            Width = 71
          end
          item
            EditButtons = <>
            FieldName = 'nCdLocalEstoque'
            Footers = <>
            Width = 40
          end
          item
            EditButtons = <>
            FieldName = 'cNmLocalEstoque'
            Footers = <>
            Width = 98
          end
          item
            EditButtons = <>
            FieldName = 'nQtde'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            Tag = 1
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'nFator'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            Width = 52
          end
          item
            Alignment = taLeftJustify
            EditButtons = <>
            FieldName = 'iMetodoUso'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            KeyList.Strings = (
              '1'
              '2')
            PickList.Strings = (
              'Manual'
              'Backflush')
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'nValCustoUnit'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Tag = 1
            Width = 76
          end
          item
            EditButtons = <>
            FieldName = 'nValCustoTotal'
            Footers = <>
            ReadOnly = True
            Tag = 1
            Width = 82
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Roteiro de Produ'#231#227'o'
      ImageIndex = 1
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 970
        Height = 44
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Tag = 1
          Left = 8
          Top = 19
          Width = 52
          Height = 13
          Alignment = taRightJustify
          Caption = 'Tipo de OP'
        end
        object cmbTipoOP: TDBLookupComboBox
          Left = 69
          Top = 15
          Width = 252
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Consolas'
          Font.Style = []
          KeyField = 'nCdTipoOP'
          ListField = 'cNmTipoOP'
          ListSource = dsTipoOP
          ParentFont = False
          TabOrder = 0
        end
      end
      object cxPageControl2: TcxPageControl
        Left = 0
        Top = 44
        Width = 970
        Height = 363
        ActivePage = cxTabSheet3
        Align = alClient
        LookAndFeel.NativeStyle = True
        TabOrder = 1
        ClientRectBottom = 359
        ClientRectLeft = 4
        ClientRectRight = 966
        ClientRectTop = 24
        object cxTabSheet3: TcxTabSheet
          Caption = 'Roteiros de Produ'#231#227'o'
          ImageIndex = 0
          object GroupBox2: TGroupBox
            Left = 0
            Top = 0
            Width = 962
            Height = 50
            Align = alTop
            TabOrder = 0
            object cxButton1: TcxButton
              Left = 8
              Top = 12
              Width = 121
              Height = 31
              Caption = 'Novo Roteiro'
              TabOrder = 0
              OnClick = cxButton1Click
              Glyph.Data = {
                2E020000424D2E0200000000000036000000280000000C0000000E0000000100
                180000000000F801000000000000000000000000000000000000EEFEFEEEFEFE
                EEFEFEEEFEFE9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F0000
                000000000000000000000000000000000000000000000000000000000000009C
                8C6F000000FFFFFFC0928FC0928FC0928FC0928FC0928FC0928FC0928FC0928F
                0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9F9EED9F9EED9F9EE
                D9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9F9EED9F9
                EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9
                F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EE
                D9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9
                EED9F9EED9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFF
                F9EED9F9EED9F9EED9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F0000
                00FFFFFFF9EED9F9EED9F9EED9F9EED90000000000000000000000000000009C
                8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED90000009C8C6F9C8C6F9C8C6F
                000000EEFEFE000000FFFFFFF9EED9F9EED9F9EED9F9EED90000009C8C6F9C8C
                6F000000EEFEFEEEFEFE000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000009C
                8C6F000000EEFEFEEEFEFEEEFEFE000000000000000000000000000000000000
                000000000000EEFEFEEEFEFEEEFEFEEEFEFE}
              LookAndFeel.NativeStyle = True
            end
            object cxButton2: TcxButton
              Left = 136
              Top = 11
              Width = 121
              Height = 31
              Caption = 'Ativar Roteiro'
              TabOrder = 1
              OnClick = cxButton2Click
              Glyph.Data = {
                6E040000424D6E04000000000000360000002800000013000000120000000100
                1800000000003804000000000000000000000000000000000000C6D6DEC6D6DE
                C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
                DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6
                D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE0000000000000000008C9C9CC6D6DE
                C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DEC6DEC6C6D6DEC6D6
                DEC6D6DEC6D6DEC6D6DE00000018A54A18A54A18A54A0000008C9C9CC6D6DEC6
                D6DEC6D6DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
                00000000000018A54A18A54A18A54A18A54A18A54A0000008C9C9CC6D6DEC6D6
                DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6D6DE0000000000000000006B
                D6BD18A54A6BD6BD0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE
                C6D6DE000000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFF0000006BD6
                BD000000EFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE00
                0000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFFEFFFFF000000EFFFFF
                EFFFFFEFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DE000000C6DE
                C6C6D6DE000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFBDBDBDEFFFFFEF
                FFFFEFFFFF0000006BD6BD18A54A18A54A000000C6D6DE000000C6D6DEC6D6DE
                000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDBDBDBDBDBDBDBDBD
                BDEFFFFF0000006BD6BD18A54A000000C6D6DE000000C6D6DEC6D6DE000000BD
                BDBDEFFFFFEFFFFFEFFFFF000000000000000000000000000000EFFFFFEFFFFF
                000000000000000000C6D6DEC6D6DE000000C6D6DEC6D6DE000000BDBDBDEFFF
                FFEFFFFFEFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6
                D6DEC6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DE000000BDBDBDEFFFFFEFFFFF
                EFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6
                DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DE000000BDBDBDEFFFFFEFFFFFEF
                FFFF000000BDBDBDEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6DEC6D6DEC6D6DE
                C6D6DE000000C6D6DEC6DEC6C6D6DE000000BDBDBDBDBDBDEFFFFFEFFFFF0000
                00EFFFFFEFFFFFDEBDD6DEBDD6000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE00
                0000C6D6DEC6D6DEC6D6DEC6D6DE000000000000BDBDBDBDBDBDBDBDBDBDBDBD
                BDBDBD000000000000C6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DE000000C6D6
                DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000000000000000000000C6
                D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DE
                C6DEC6C6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
                DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6DEC6000000C6DEC6C6D6DEC6D6DEC6
                D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
                C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000}
              LookAndFeel.NativeStyle = True
            end
            object cxButton3: TcxButton
              Left = 264
              Top = 11
              Width = 121
              Height = 31
              Caption = 'Inativar Roteiro'
              TabOrder = 2
              OnClick = cxButton3Click
              Glyph.Data = {
                7E030000424D7E030000000000003600000028000000120000000F0000000100
                1800000000004803000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFB0A0906048306048306048
                30604830604830604830604830604830604830604830FFFFFF8692BE1028908C
                91B9FFFFFFFFFFFF0000FFFFFFB0A090FFFFFFE0E0E0E0E0E0E0D0D0D0C8C0D0
                C0C0D0B8B0D0B8B0D0B8B0604830FFFFFF7088E01048FF102890FFFFFFFFFFFF
                0000FFFFFFB0A090FFFFFFD0B8B0D0B8A0D0B0A0C0A890C0A090C09890C09890
                C0A8A0604830FFFFFFAEB1CE7088E0ADABC3FFFFFFFFFFFF0000FFFFFFB0A090
                FFFFFFD0B8B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC09890C0A8A0604830FFFF
                FFFFFFFF9397B9FFFFFFFFFFFFFFFFFF0000FFFFFFC0A890FFFFFFD0B8B0D0B8
                A0D0B0A0C0A890C0A090C09890C09890C0B0A0604830FFFFFFFFFFFF4050B0FF
                FFFFFFFFFFFFFFFF0000FFFFFFC0A8A0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F8F0F0E8E0F0E0E0C0B8B0604830FFFFFF9393B10038F085859CFFFFFFFFFFFF
                0000FFFFFFC0B0A0FFFFFFD0C0B0D0C0B0D0B8B0D0B0A0D0A8A0C0A890C0A890
                D0B8B0604830FFFFFF2040C00038F00E3293FFFFFFFFFFFF0000FFFFFFD0B0A0
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0F0F0F0D0C0C0604830FFFF
                FF5070E00040FF0030D0FFFFFFFFFFFF0000FFFFFFD0B8A0FFFFFFE0C8C0E0C8
                C0D0C0B0D0B8B0D0B8B0F0E8E0D0D0D0D0C8C06048307784C25078E01048FF00
                40F0767995FFFFFF0000FFFFFFD0B8B0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFB0A0906048306048306048305068D07090FF1050FF1040F00E32A2FFFFFF
                0000FFFFFFD0C0B0FFFFFFE0C8C0E0C8C0E0C8C0D0C0B0F0E8E0C0A890D0C8C0
                604830FFFFFF6078D08098FF3060FF1050FF1D41C0FFFFFF0000FFFFFFE0C0B0
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0A8A0604830DDC2B2FFFFFF7088
                E090A8F080A0FF6080F03952A5FFFFFF0000FFFFFFE0C0B0E0C0B0E0C0B0E0C0
                B0E0C0B0D0C0B0D0B8B0D0B0A0DCC1B2FFFFFFFFFFFFFFFFFF7088E06078D050
                68D0FFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                0000}
              LookAndFeel.NativeStyle = True
            end
          end
          object DBGridEh1: TDBGridEh
            Left = 0
            Top = 50
            Width = 962
            Height = 285
            Align = alClient
            AllowedOperations = [alopUpdateEh]
            DataGrouping.GroupLevels = <>
            DataSource = dsRoteiroProducao
            Flat = False
            FooterColor = clWindow
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'Tahoma'
            FooterFont.Style = []
            RowDetailPanel.Color = clBtnFace
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            UseMultiTitle = True
            OnDblClick = DBGridEh1DblClick
            Columns = <
              item
                EditButtons = <>
                FieldName = 'nCdRoteiroProducao'
                Footers = <>
                Visible = False
              end
              item
                EditButtons = <>
                FieldName = 'nCdProduto'
                Footers = <>
                Visible = False
              end
              item
                EditButtons = <>
                FieldName = 'cNmRoteiro'
                Footers = <>
                Width = 274
              end
              item
                EditButtons = <>
                FieldName = 'dDtUltAlt'
                Footers = <>
                ReadOnly = True
                Width = 179
              end
              item
                EditButtons = <>
                FieldName = 'cNmUsuario'
                Footers = <>
                ReadOnly = True
                Width = 157
              end
              item
                EditButtons = <>
                FieldName = 'nCdStatus'
                Footers = <>
                ReadOnly = True
                Width = 49
              end
              item
                EditButtons = <>
                FieldName = 'cNmStatus'
                Footers = <>
                ReadOnly = True
                Width = 124
              end
              item
                EditButtons = <>
                FieldName = 'nCdUsuarioUltAlt'
                Footers = <>
                ReadOnly = True
                Visible = False
              end
              item
                EditButtons = <>
                FieldName = 'nCdTipoOP'
                Footers = <>
                ReadOnly = True
                Visible = False
              end>
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 328
    Top = 304
  end
  object dsFormulaProduto: TDataSource
    DataSet = qryFormulaProduto
    Left = 200
    Top = 280
  end
  object qryProdutoFormula: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdProduto, cNmProduto, nValCusto,cUnidadeMedida, cFlgFan' +
        'tasma, nCdTipoObtencao, nCdTabTipoMetodoUso, nCdTabTipoMetodoSai' +
        'daEstoque'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 152
    Top = 288
    object qryProdutoFormulanCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutoFormulacNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutoFormulanValCusto: TBCDField
      FieldName = 'nValCusto'
      Precision = 14
      Size = 6
    end
    object qryProdutoFormulacUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryProdutoFormulacFlgFantasma: TIntegerField
      FieldName = 'cFlgFantasma'
    end
    object qryProdutoFormulanCdTipoObtencao: TIntegerField
      FieldName = 'nCdTipoObtencao'
    end
    object qryProdutoFormulanCdTabTipoMetodoUso: TIntegerField
      FieldName = 'nCdTabTipoMetodoUso'
    end
    object qryProdutoFormulanCdTabTipoMetodoSaidaEstoque: TIntegerField
      FieldName = 'nCdTabTipoMetodoSaidaEstoque'
    end
  end
  object qryFormulaProduto: TADOQuery
    AutoCalcFields = False
    Connection = frmMenu.Connection
    BeforePost = qryFormulaProdutoBeforePost
    OnCalcFields = qryFormulaProdutoCalcFields
    Parameters = <
      item
        Name = 'nCdRoteiroEtapaProducao'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdRoteiroEtapaProducao int'
      ''
      
        'Set @nCdRoteiroEtapaProducao = IsNull(:nCdRoteiroEtapaProducao,0' +
        ')'
      ''
      'SELECT *'
      ' FROM FormulaProduto'
      'WHERE nCdProdutoPai = :nPK'
      '  AND (   (@nCdRoteiroEtapaProducao = 0)'
      
        '       OR (FormulaProduto.nCdRoteiroEtapaProducao = @nCdRoteiroE' +
        'tapaProducao))')
    Left = 120
    Top = 224
    object qryFormulaProdutonCdprodutoPai: TIntegerField
      FieldName = 'nCdprodutoPai'
    end
    object qryFormulaProdutonCdProduto: TIntegerField
      DisplayLabel = 'Estrutura do Produto|C'#243'd'
      FieldName = 'nCdProduto'
      OnChange = qryFormulaProdutonCdProdutoChange
    end
    object qryFormulaProdutocNmProduto: TStringField
      DisplayLabel = 'Estrutura do Produto|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmProduto'
      FixedChar = True
      Size = 150
      Calculated = True
    end
    object qryFormulaProdutonQtde: TBCDField
      DisplayLabel = 'Quantidade|Qtde. Base'
      FieldName = 'nQtde'
      DisplayFormat = '#,##0.000000'
      Precision = 12
      Size = 2
    end
    object qryFormulaProdutonCdFormulaProduto: TAutoIncField
      FieldName = 'nCdFormulaProduto'
      ReadOnly = True
    end
    object qryFormulaProdutoiMetodoUso: TIntegerField
      DisplayLabel = 'Quantidade|M'#233'todo Uso'
      FieldName = 'iMetodoUso'
    end
    object qryFormulaProdutocUnidadeMedida: TStringField
      DisplayLabel = 'Unidade Medida|F'#243'rmula'
      FieldName = 'cUnidadeMedida'
      OnChange = qryFormulaProdutocUnidadeMedidaChange
      FixedChar = True
      Size = 3
    end
    object qryFormulaProdutonFator: TBCDField
      DisplayLabel = 'Quantidade|Fator'
      FieldName = 'nFator'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryFormulaProdutonValCustoUnit: TFloatField
      DisplayLabel = 'Custos|Unit'#225'rio'
      FieldKind = fkCalculated
      FieldName = 'nValCustoUnit'
      DisplayFormat = '#,##0.000000'
      Calculated = True
    end
    object qryFormulaProdutonValCustoTotal: TFloatField
      DisplayLabel = 'Custos|Total'
      FieldKind = fkCalculated
      FieldName = 'nValCustoTotal'
      DisplayFormat = '#,##0.000000'
      Calculated = True
    end
    object qryFormulaProdutonFatorConversaoUM: TBCDField
      DisplayLabel = 'Unidade Medida|Fator Conv.'
      FieldName = 'nFatorConversaoUM'
      DisplayFormat = '#,##0.000000'
      Precision = 12
      Size = 6
    end
    object qryFormulaProdutocUnidadeMedidaEstoque: TStringField
      DisplayLabel = 'Unidade Medida|Estoque'
      FieldKind = fkCalculated
      FieldName = 'cUnidadeMedidaEstoque'
      Size = 3
      Calculated = True
    end
    object qryFormulaProdutonCdTabTipoMetodoSaidaEstoque: TIntegerField
      FieldName = 'nCdTabTipoMetodoSaidaEstoque'
    end
    object qryFormulaProdutonCdLocalEstoque: TIntegerField
      DisplayLabel = 'Local de Estoque|C'#243'd.'
      FieldName = 'nCdLocalEstoque'
      OnChange = qryFormulaProdutonCdLocalEstoqueChange
    end
    object qryFormulaProdutocNmLocalEstoque: TStringField
      DisplayLabel = 'Local de Estoque|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmLocalEstoque'
      ReadOnly = True
      Size = 50
      Calculated = True
    end
    object qryFormulaProdutonCdRoteiroEtapaProducao: TIntegerField
      FieldName = 'nCdRoteiroEtapaProducao'
    end
  end
  object qryAtualizaPrecoCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nValCusto'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 6
        Precision = 14
        Size = 19
        Value = Null
      end
      item
        Name = 'nCdProduto'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE Produto'
      'SET nValCusto = :nValCusto'
      'WHERE nCdProduto = :nCdProduto')
    Left = 164
    Top = 245
  end
  object qryTipoOP: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryTipoOPAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdTipoOP'
      
        '      ,dbo.fn_ZeroEsquerda(nCdTipoOP,3) + '#39' - '#39' + cNmTipoOP as c' +
        'NmTipoOP'
      '  FROM TipoOP'
      ' ORDER BY nCdTipoOP')
    Left = 424
    Top = 145
    object qryTipoOPnCdTipoOP: TIntegerField
      FieldName = 'nCdTipoOP'
    end
    object qryTipoOPcNmTipoOP: TStringField
      FieldName = 'cNmTipoOP'
      ReadOnly = True
      Size = 68
    end
  end
  object dsTipoOP: TDataSource
    DataSet = qryTipoOP
    Left = 472
    Top = 145
  end
  object qryRoteiroProducao: TADOQuery
    Connection = frmMenu.Connection
    OnCalcFields = qryRoteiroProducaoCalcFields
    Parameters = <
      item
        Name = 'nCdProduto'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTipoOP'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM RoteiroProducao'
      ' WHERE RoteiroProducao.nCdProduto = :nCdProduto'
      '   AND RoteiroProducao.nCdTipoOP  = :nCdTipoOP'
      ' ORDER BY nCdRoteiroProducao')
    Left = 424
    Top = 297
    object qryRoteiroProducaonCdRoteiroProducao: TIntegerField
      FieldName = 'nCdRoteiroProducao'
    end
    object qryRoteiroProducaonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryRoteiroProducaocNmRoteiro: TStringField
      DisplayLabel = 
        'Roteiros Dispon'#237'veis Para Este Tipo de Ordem de Produ'#231#227'o|Nome/Ve' +
        'rs'#227'o Roteiro'
      FieldName = 'cNmRoteiro'
      Size = 50
    end
    object qryRoteiroProducaonCdTipoOP: TIntegerField
      FieldName = 'nCdTipoOP'
    end
    object qryRoteiroProducaodDtUltAlt: TDateTimeField
      DisplayLabel = 
        'Roteiros Dispon'#237'veis Para Este Tipo de Ordem de Produ'#231#227'o|Data '#218'l' +
        't. Altera'#231#227'o'
      FieldName = 'dDtUltAlt'
    end
    object qryRoteiroProducaonCdUsuarioUltAlt: TIntegerField
      FieldName = 'nCdUsuarioUltAlt'
    end
    object qryRoteiroProducaocNmUsuario: TStringField
      DisplayLabel = 
        'Roteiros Dispon'#237'veis Para Este Tipo de Ordem de Produ'#231#227'o|Usu'#225'rio' +
        ' Ult. Alt.'
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 50
      Calculated = True
    end
    object qryRoteiroProducaonCdStatus: TIntegerField
      DisplayLabel = 'Situa'#231#227'o Roteiro|C'#243'd.'
      FieldName = 'nCdStatus'
    end
    object qryRoteiroProducaocNmStatus: TStringField
      DisplayLabel = 'Situa'#231#227'o Roteiro|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmStatus'
      Size = 50
      Calculated = True
    end
  end
  object dsRoteiroProducao: TDataSource
    DataSet = qryRoteiroProducao
    Left = 456
    Top = 297
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 456
    Top = 353
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object qryStatus: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Status'
      ' WHERE nCdStatus = :nPK')
    Left = 504
    Top = 345
    object qryStatusnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryStatuscNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 240
    Top = 353
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLocalEstoque'
      '   FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK')
    Left = 236
    Top = 397
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
end
