unit fCaixa_ConfUsuarioCaixa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, jpeg, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, DB, ADODB;

type
  TfrmCaixa_ConfUsuarioCaixa = class(TForm)
    Image1: TImage;
    btnConfirmar: TcxButton;
    btnCancelar: TcxButton;
    edtUsuario: TcxTextEdit;
    Label2: TLabel;
    edtSenha: TcxTextEdit;
    Label1: TLabel;
    qryValidaLogin: TADOQuery;
    qryValidaLoginCOLUMN1: TIntegerField;
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure edtSenhaKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_ConfUsuarioCaixa: TfrmCaixa_ConfUsuarioCaixa;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmCaixa_ConfUsuarioCaixa.btnCancelarClick(Sender: TObject);
begin
    qryValidaLogin.Close ;
    close;
end;

procedure TfrmCaixa_ConfUsuarioCaixa.FormShow(Sender: TObject);
begin

    edtSenha.Text := '' ;
    edtSenha.SetFocus;

end;

procedure TfrmCaixa_ConfUsuarioCaixa.btnConfirmarClick(Sender: TObject);
begin

    if (edtSenha.Text = '') then
    begin
        frmMenu.MensagemAlerta('Senha inv�lida.') ;
        qryValidaLogin.Close ;
        close ;
    end ;

    qryValidaLogin.Close ;
    qryValidaLogin.Parameters.ParamByName('cNmLogin').Value := edtUsuario.Text ;
    qryValidaLogin.Parameters.ParamByName('cSenha').Value   := frmMenu.Cripto(UpperCase(edtSenha.Text));
    qryValidaLogin.Open ;

    if (qryValidaLogin.Eof) then
    begin
        frmMenu.MensagemAlerta('Senha inv�lida.') ;
        btnCancelar.Click;
        abort ;
    end ;

    Close;

end;

procedure TfrmCaixa_ConfUsuarioCaixa.edtSenhaKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        btnConfirmar.SetFocus;

end;

end.
