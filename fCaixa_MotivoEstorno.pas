unit fCaixa_MotivoEstorno;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls;

type
  TfrmCaixa_MotivoEstorno = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    EDit1: TEdit;
    procedure ToolButton2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EDit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_MotivoEstorno: TfrmCaixa_MotivoEstorno;

implementation

{$R *.dfm}

procedure TfrmCaixa_MotivoEstorno.ToolButton2Click(Sender: TObject);
begin

  If (Edit1.Text = '') then
  begin
      MensagemAlerta('Informe o motivo do estorno.') ;
      Edit1.SetFocus ;
      exit ;
  end ;

  inherited;

end;

procedure TfrmCaixa_MotivoEstorno.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmCaixa_MotivoEstorno.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmCaixa_MotivoEstorno.EDit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      vk_return: ToolButton2.Click;
  end ;

end;

end.
