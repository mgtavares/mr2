inherited frmMotivoPerdaProducao: TfrmMotivoPerdaProducao
  Caption = 'Motivo Perda Produ'#231#227'o'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 19
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit2: TDBEdit [4]
    Left = 64
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmMotivoPerdaProducao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 64
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdMotivoPerdaProducao'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Size = 1
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * FROM MotivoPerdaProducao'
      'WHERE nCdMotivoPerdaProducao = :nPk')
    object qryMasternCdMotivoPerdaProducao: TIntegerField
      FieldName = 'nCdMotivoPerdaProducao'
    end
    object qryMastercNmMotivoPerdaProducao: TStringField
      FieldName = 'cNmMotivoPerdaProducao'
      Size = 50
    end
  end
end
