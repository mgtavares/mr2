unit rOperacaoCartao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls;

type
  TrptOperacaoCartao = class(TfrmRelatorio_Padrao)
    edtOperadora: TMaskEdit;
    Label1: TLabel;
    edtLoja: TMaskEdit;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    dsLoja: TDataSource;
    qryOperadora: TADOQuery;
    qryOperadoranCdOperadoraCartao: TIntegerField;
    qryOperadoracNmOperadoraCartao: TStringField;
    DBEdit2: TDBEdit;
    dsOperadora: TDataSource;
    RadioGroup3: TRadioGroup;
    edtDtVendaIni: TMaskEdit;
    Label6: TLabel;
    edtDtVendaFim: TMaskEdit;
    edtDtCreditoIni: TMaskEdit;
    Label5: TLabel;
    edtDtCreditoFim: TMaskEdit;
    RadioGroup4: TRadioGroup;
    rgOrigemTransacao: TRadioGroup;
    procedure edtLojaExit(Sender: TObject);
    procedure edtOperadoraExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtOperadoraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptOperacaoCartao: TrptOperacaoCartao;

implementation

uses fMenu, fLookup_Padrao, rOperacaoCartao_view;

{$R *.dfm}

procedure TrptOperacaoCartao.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, edtLoja.Text) ;

end;

procedure TrptOperacaoCartao.edtOperadoraExit(Sender: TObject);
begin
  inherited;

  qryOperadora.Close ;
  PosicionaQuery(qryOperadora, edtOperadora.Text) ;
  
end;

procedure TrptOperacaoCartao.edtLojaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            edtLoja.Text := IntToStr(nPK) ;
            qryLoja.Close ;
            qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
            PosicionaQuery(qryLoja, edtLoja.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptOperacaoCartao.edtOperadoraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(149);

        If (nPK > 0) then
        begin
            edtOperadora.Text := IntToStr(nPK) ;
            qryOperadora.Close ;
            PosicionaQuery(qryOperadora, edtOperadora.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptOperacaoCartao.ToolButton1Click(Sender: TObject);
var
    cFiltro : string ;
    objRel  : TrptOperacaoCartao_view ;
begin
  inherited;

  if (DBEdit1.Text = '') then
      edtLoja.Text := '' ;

  if (DBEdit2.Text = '') then
     edtOperadora.Text := '' ;

  if (trim(edtDtVendaIni.Text) = '/  /') and (trim(edtDtVendaFim.Text) = '/  /') and (trim(edtDtCreditoIni.Text) = '/  /') and (trim(edtDtCreditoFim.Text) = '/  /') then
  begin
      MensagemAlerta('Selecione um per�odo para processamento do relat�rio.') ;
      edtDtVendaIni.SetFocus ;
      exit ;
  end ;

  objRel  := TrptOperacaoCartao_view.Create(Self) ;

  objRel.qryResultado.Close ;
  objRel.qryResultado.Parameters.ParamByName('nCdEmpresa').Value           := frmMenu.nCdEmpresaAtiva;
  objRel.qryResultado.Parameters.ParamByName('nCdLoja').Value              := frmMenu.ConvInteiro(edtLoja.Text) ;
  objRel.qryResultado.Parameters.ParamByName('nCdOperadoraCartao').Value   := frmMenu.ConvInteiro(edtOperadora.Text) ;
  objRel.qryResultado.Parameters.ParamByName('nCdUsuario').Value           := frmMenu.nCdUsuarioLogado;
  objRel.qryResultado.Parameters.ParamByName('dDtVendaIni').Value          := frmMenu.ConvData(edtDtVendaIni.Text) ;
  objRel.qryResultado.Parameters.ParamByName('dDtVendaFim').Value          := frmMenu.ConvData(edtDtVendaFim.Text) ;
  objRel.qryResultado.Parameters.ParamByName('dDtCreditoIni').Value        := frmMenu.ConvData(edtDtCreditoIni.Text) ;
  objRel.qryResultado.Parameters.ParamByName('dDtCreditoFim').Value        := frmMenu.ConvData(edtDtCreditoFim.Text) ;
  objRel.qryResultado.Parameters.ParamByName('cFlgCondPagto').Value        := RadioGroup1.ItemIndex;
  objRel.qryResultado.Parameters.ParamByName('cFlgSituacao').Value         := RadioGroup2.ItemIndex;
  objRel.qryResultado.Parameters.ParamByName('cFlgTipoOperacao').Value     := RadioGroup3.ItemIndex;
  objRel.qryResultado.Parameters.ParamByName('cFlgCartaoConciliado').Value := RadioGroup4.ItemIndex;
  objRel.qryResultado.Parameters.ParamByName('cFlgOrigemTransacao').Value  := rgOrigemTransacao.ItemIndex;
  objRel.qryResultado.Open ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  cFiltro := '' ;

  if (DbEdit1.Text <> '') then
      cFiltro := cFiltro + '/ Loja: ' + edtLoja.Text + '-' + DBEdit1.Text ;

  if (DbEdit2.Text <> '') then
      cFiltro := cFiltro + '/ Operadora: ' + edtOperadora.Text + '-' + DBEdit2.Text ;

  if (Trim(edtDtVendaIni.Text) <> '/  /') or (Trim(edtDtVendaFim.Text) <> '/  /') then
      cFiltro := cFiltro + '/ Intervalo de Opera��es: ' + edtDtVendaIni.Text + ' a ' + edtDtVendaFim.Text ;

  if (Trim(edtDtCreditoIni.Text) <> '/  /') or (Trim(edtDtCreditoFim.Text) <> '/  /') then
      cFiltro := cFiltro + '/ Previs�o de Pagamento: ' + edtDtCreditoIni.Text + ' a ' + edtDtCreditoFim.Text ;

  cFiltro := cFiltro + '/ Condi��o de Pagamento: ' ;
  if (RadioGroup1.ItemIndex = 0) then
      cFiltro := cFiltro + 'TODOS' ;
  if (RadioGroup1.ItemIndex = 1) then
      cFiltro := cFiltro + 'A vista' ;
  if (RadioGroup1.ItemIndex = 2) then
      cFiltro := cFiltro + 'A prazo' ;

  cFiltro := cFiltro + '/ Situa��o da Opera��o: ' ;
  if (RadioGroup2.ItemIndex = 0) then
      cFiltro := cFiltro + 'TODOS' ;
  if (RadioGroup2.ItemIndex = 1) then
      cFiltro := cFiltro + 'em aberto' ;
  if (RadioGroup2.ItemIndex = 2) then
      cFiltro := cFiltro + 'Pago pela operadora' ;

  cFiltro := cFiltro + '/ Tipo Opera��o: ' ;
  if (RadioGroup3.ItemIndex = 0) then
      cFiltro := cFiltro + 'TODOS' ;
  if (RadioGroup3.ItemIndex = 1) then
      cFiltro := cFiltro + 'cr�dito' ;
  if (RadioGroup3.ItemIndex = 2) then
      cFiltro := cFiltro + 'd�bito' ;

  cFiltro := cFiltro + '/ Conciliado Extrato Operadora: ' ;
  if (RadioGroup4.ItemIndex = 0) then
      cFiltro := cFiltro + 'TODOS' ;
  if (RadioGroup4.ItemIndex = 1) then
      cFiltro := cFiltro + 'sim' ;
  if (RadioGroup4.ItemIndex = 2) then
      cFiltro := cFiltro + 'n�o' ;

  objRel.lblFiltro1.Caption := cFiltro ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

initialization
    RegisterClass(TrptOperacaoCartao) ;

end.
