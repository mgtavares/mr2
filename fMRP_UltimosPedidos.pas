unit fMRP_UltimosPedidos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, GridsEh, DBGridEh, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmMRP_UltimosPedidos = class(TfrmProcesso_Padrao)
    qryResultado: TADOQuery;
    qryResultadonCdPedido: TIntegerField;
    qryResultadocNmTerceiro: TStringField;
    qryResultadodDtPedido: TDateTimeField;
    qryResultadodDtEntregaIni: TDateTimeField;
    qryResultadodDtEntregaFim: TDateTimeField;
    qryResultadonPercentCompra: TBCDField;
    qryResultadonQtdePed: TBCDField;
    qryResultadonQtdeRec: TBCDField;
    dsResultado: TDataSource;
    qryResultadocSiglaUnidadeMedida: TStringField;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEntregaIni: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEntregaFim: TcxGridDBColumn;
    cxGrid1DBTableView1nPercentCompra: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdePed: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeRec: TcxGridDBColumn;
    cxGrid1DBTableView1cSiglaUnidadeMedida: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMRP_UltimosPedidos: TfrmMRP_UltimosPedidos;

implementation

{$R *.dfm}

end.
