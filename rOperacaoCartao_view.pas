unit rOperacaoCartao_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptOperacaoCartao_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape2: TQRShape;
    QRDBText8: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    qryResultado: TADOQuery;
    qryResultadodDtTransacao: TDateTimeField;
    qryResultadodDtVenc: TDateTimeField;
    qryResultadoiNrCartao: TLargeintField;
    qryResultadoiNrDocto: TIntegerField;
    qryResultadoiNrAutorizacao: TIntegerField;
    qryResultadonCdOperadoraCartao: TIntegerField;
    qryResultadocNmOperadoraCartao: TStringField;
    qryResultadonValTransacao: TBCDField;
    qryResultadonValTit: TBCDField;
    qryResultadonSaldoTit: TBCDField;
    qryResultadoiParcelas: TIntegerField;
    qryResultadoiParcela: TIntegerField;
    qryResultadonCdLoja: TIntegerField;
    qryResultadocNmLoja: TStringField;
    qryResultadonCdTransacaoCartao: TAutoIncField;
    QRLabel8: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    qryResultadodDtLiq: TDateTimeField;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRDBText11: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText12: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText13: TQRDBText;
    QRExpr3: TQRExpr;
    QRDBText14: TQRDBText;
    QRShape6: TQRShape;
    QRLabel20: TQRLabel;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRExpr4: TQRExpr;
    QRLabel21: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptOperacaoCartao_view: TrptOperacaoCartao_view;

implementation

{$R *.dfm}

end.
