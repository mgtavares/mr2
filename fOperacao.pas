unit fOperacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ADODB, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DBCtrls, ImgList;

type
  TfrmOperacao = class(TfrmCadastro_Padrao)
    qryMasternCdOperacao: TIntegerField;
    qryMastercNmOperacao: TStringField;
    qryMastercTipoOper: TStringField;
    qryMastercSinalOper: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    qryPlanoConta: TADOQuery;
    qryPlanoContanCdPlanoConta: TIntegerField;
    qryPlanoContacNmPlanoConta: TStringField;
    qryMasternCdPlanoConta: TIntegerField;
    Label7: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DataSource1: TDataSource;
    qryMastercFlgApuraDRE: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOperacao: TfrmOperacao;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmOperacao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'OPERACAO' ;
  nCdTabelaSistema  := 19 ;
  nCdConsultaPadrao := 30 ;
end;

procedure TfrmOperacao.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmOperacao.qryMasterBeforePost(DataSet: TDataSet);
begin

  If (qryMastercNmOperacao.Value = '') Then
  begin
      MensagemAlerta('Informe a Descri��o') ;
      DBEdit2.SetFocus;
      Abort ;
  end ;

  if (qryMastercTipoOper.Value <> 'A') And (qryMastercTipoOper.Value <> 'P') And (qryMastercTipoOper.Value <> 'J') And (qryMastercTipoOper.Value <> 'D') then
  begin
      MensagemAlerta('Tipo de Opera��o Inv�lida') ;
      DBEdit3.SetFocus;
      Abort ;
  end ;

  if (qryMastercSinalOper.Value <> '+') And (qryMastercSinalOper.Value <> '-') And (qryMastercSinalOper.Value <> '#') then
  begin
      MensagemAlerta('Efeito no Saldo do T�tulo Inv�lido') ;
      DBEdit4.SetFocus;
      Abort ;
  end ;

  if (qryMastercTipoOper.Value = 'P') and (qryMastercFlgApuraDRE.Value = 1) then
  begin
      MensagemAlerta('As opera��es de pagamento n�o podem ser apuradas no DRE, somente no Fluxo de Caixa Realizado.') ;
      abort ;
  end ;

  if (qryMastercFlgApuraDRE.Value = 1) and (DBEdit6.Text = '') then
  begin
      MensagemAlerta('Selecione a conta contabil para apura��o no DRE.') ;
      DBEdit5.SetFocus;
      abort ;
  end ;


  inherited;

end;

procedure TfrmOperacao.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(41);

            If (nPK > 0) then
            begin
                qryMasternCdPlanoConta.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOperacao.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  qryPlanoConta.Close ;
  PosicionaQuery(qryPlanoConta, DBEdit5.Text) ;

end;

procedure TfrmOperacao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryPlanoConta, qryMasternCdPlanoConta.AsString) ;

end;

procedure TfrmOperacao.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryPlanoConta.Close ;
  
end;

procedure TfrmOperacao.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryPlanoConta.Close ;
  
end;

initialization
    RegisterClass(tFrmOperacao) ;

end.
