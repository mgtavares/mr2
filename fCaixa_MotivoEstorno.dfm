inherited frmCaixa_MotivoEstorno: TfrmCaixa_MotivoEstorno
  Left = 395
  Top = 326
  Width = 625
  Height = 107
  BorderIcons = []
  Caption = 'Caixa - Motivo Estorno'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 617
    Height = 47
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 48
    Width = 72
    Height = 13
    Caption = 'Motivo Estorno'
  end
  inherited ToolBar1: TToolBar
    Width = 617
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object EDit1: TEdit [3]
    Left = 88
    Top = 40
    Width = 505
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 150
    TabOrder = 1
    OnKeyDown = EDit1KeyDown
  end
  inherited ImageList1: TImageList
    Left = 360
    Top = 40
  end
end
