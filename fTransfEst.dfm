inherited frmTransfEst: TfrmTransfEst
  Left = 217
  Top = 40
  Width = 1081
  Height = 651
  Caption = 'Transfer'#234'ncia entre Estoques'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1065
    Height = 588
  end
  object Label1: TLabel [1]
    Left = 14
    Top = 54
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label9: TLabel [2]
    Left = 14
    Top = 217
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Inclus'#227'o'
    FocusControl = DBEdit9
  end
  object Label15: TLabel [3]
    Left = 452
    Top = 217
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Confirma'#231#227'o'
    FocusControl = DBEdit15
  end
  object Label11: TLabel [4]
    Left = 233
    Top = 218
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Finaliza'#231#227'o'
  end
  object Label13: TLabel [5]
    Left = 672
    Top = 216
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cancelamento'
    FocusControl = DBEdit13
  end
  inherited ToolBar2: TToolBar
    Width = 1065
    inherited ToolButton9: TToolButton
      Visible = False
    end
    object ToolButton10: TToolButton
      Left = 856
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object btFinalizar: TToolButton
      Left = 864
      Top = 0
      Caption = '&Finalizar'
      ImageIndex = 8
      OnClick = btFinalizarClick
    end
    object ToolButton12: TToolButton
      Left = 952
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton13: TToolButton
      Left = 960
      Top = 0
      Caption = 'Op'#231#245'es'
      DropdownMenu = PopupMenu1
      ImageIndex = 12
    end
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 58
    Top = 48
    Width = 65
    Height = 19
    DataField = 'nCdTransfEst'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBRadioGroup1: TDBRadioGroup [8]
    Left = 13
    Top = 76
    Width = 884
    Height = 42
    Caption = ' Tipo de Transfer'#234'ncia '
    Color = 13086366
    Columns = 4
    DataField = 'nCdTabTipoTransf'
    DataSource = dsMaster
    Enabled = False
    Items.Strings = (
      'Interna'
      'Interestabelecimento'
      'Consigna'#231#227'o'
      'Transf. Faturada')
    ParentColor = False
    TabOrder = 2
    Values.Strings = (
      '1'
      '2'
      '3'
      '4')
    OnChange = DBRadioGroup1Change
  end
  object GroupBox1: TGroupBox [9]
    Left = 13
    Top = 120
    Width = 441
    Height = 89
    Caption = ' Estoque de Origem '
    Color = 13086366
    ParentColor = False
    TabOrder = 3
    object Label4: TLabel
      Left = 9
      Top = 68
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = 'Estoque'
      FocusControl = DBEdit4
    end
    object Label3: TLabel
      Left = 30
      Top = 44
      Width = 21
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
      FocusControl = DBEdit3
    end
    object Label2: TLabel
      Left = 8
      Top = 20
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
      FocusControl = DBEdit2
    end
    object DBEdit4: TDBEdit
      Left = 56
      Top = 62
      Width = 65
      Height = 19
      DataField = 'nCdLocalEstoqueOrigem'
      DataSource = dsMaster
      TabOrder = 2
      OnExit = DBEdit4Exit
      OnKeyDown = DBEdit4KeyDown
    end
    object DBEdit3: TDBEdit
      Left = 56
      Top = 38
      Width = 65
      Height = 19
      DataField = 'nCdLojaOrigem'
      DataSource = dsMaster
      TabOrder = 1
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 56
      Top = 14
      Width = 65
      Height = 19
      DataField = 'nCdEmpresaOrigem'
      DataSource = dsMaster
      TabOrder = 0
    end
    object DBEdit16: TDBEdit
      Tag = 1
      Left = 124
      Top = 14
      Width = 310
      Height = 19
      DataField = 'cNmEmpresa'
      DataSource = dsEmpresaOrigem
      TabOrder = 3
    end
    object DBEdit17: TDBEdit
      Tag = 1
      Left = 124
      Top = 38
      Width = 310
      Height = 19
      DataField = 'cNmLoja'
      DataSource = dsLojaOrigem
      TabOrder = 4
    end
    object DBEdit18: TDBEdit
      Tag = 1
      Left = 124
      Top = 62
      Width = 310
      Height = 19
      DataField = 'cNmLocalEstoque'
      DataSource = dsLocalEstoqueOrigem
      TabOrder = 5
    end
  end
  object GroupBox2: TGroupBox [10]
    Left = 456
    Top = 120
    Width = 441
    Height = 89
    Caption = ' Estoque de Destino '
    Color = 13086366
    ParentColor = False
    TabOrder = 4
    OnEnter = GroupBox2Enter
    object Label16: TLabel
      Left = 9
      Top = 68
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = 'Estoque'
      FocusControl = DBEdit4
    end
    object Label17: TLabel
      Left = 30
      Top = 44
      Width = 21
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
      FocusControl = DBEdit3
    end
    object Label18: TLabel
      Left = 8
      Top = 20
      Width = 43
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
      FocusControl = DBEdit2
    end
    object DBEdit5: TDBEdit
      Left = 56
      Top = 14
      Width = 65
      Height = 19
      DataField = 'nCdEmpresaDestino'
      DataSource = dsMaster
      TabOrder = 0
      OnKeyDown = DBEdit5KeyDown
    end
    object DBEdit6: TDBEdit
      Left = 56
      Top = 38
      Width = 65
      Height = 19
      DataField = 'nCdLojaDestino'
      DataSource = dsMaster
      TabOrder = 1
      OnExit = DBEdit6Exit
      OnKeyDown = DBEdit6KeyDown
    end
    object DBEdit7: TDBEdit
      Left = 56
      Top = 38
      Width = 65
      Height = 19
      DataField = 'nCdTerceiro'
      DataSource = dsMaster
      TabOrder = 2
      Visible = False
      OnExit = DBEdit7Exit
      OnKeyDown = DBEdit7KeyDown
    end
    object DBEdit8: TDBEdit
      Left = 56
      Top = 62
      Width = 65
      Height = 19
      DataField = 'nCdLocalEstoqueDestino'
      DataSource = dsMaster
      TabOrder = 3
      OnExit = DBEdit8Exit
      OnKeyDown = DBEdit8KeyDown
    end
    object DBEdit19: TDBEdit
      Tag = 1
      Left = 124
      Top = 14
      Width = 309
      Height = 19
      DataField = 'cNmEmpresa'
      DataSource = dsEmpresaDestino
      TabOrder = 4
    end
    object DBEdit20: TDBEdit
      Tag = 1
      Left = 124
      Top = 38
      Width = 309
      Height = 19
      DataField = 'cNmLoja'
      DataSource = dsLojaDestino
      TabOrder = 5
    end
    object DBEdit21: TDBEdit
      Tag = 1
      Left = 124
      Top = 62
      Width = 309
      Height = 19
      DataField = 'cNmLocalEstoque'
      DataSource = dsLocalEstoqueDestino
      TabOrder = 6
    end
    object DBEdit22: TDBEdit
      Tag = 1
      Left = 124
      Top = 38
      Width = 309
      Height = 19
      DataField = 'cNmTerceiro'
      DataSource = dsTerceiro
      TabOrder = 7
      Visible = False
    end
  end
  object DBEdit9: TDBEdit [11]
    Tag = 1
    Left = 12
    Top = 234
    Width = 100
    Height = 19
    DataField = 'dDtCad'
    DataSource = dsMaster
    TabOrder = 5
  end
  object Edit1: TEdit [12]
    Tag = 1
    Left = 115
    Top = 234
    Width = 113
    Height = 19
    TabOrder = 6
  end
  object DBEdit11: TDBEdit [13]
    Tag = 1
    Left = 233
    Top = 234
    Width = 100
    Height = 19
    DataField = 'dDtFinalizacao'
    DataSource = dsMaster
    TabOrder = 7
  end
  object Edit2: TEdit [14]
    Tag = 1
    Left = 336
    Top = 234
    Width = 113
    Height = 19
    TabOrder = 8
  end
  object DBEdit15: TDBEdit [15]
    Tag = 1
    Left = 452
    Top = 234
    Width = 100
    Height = 19
    DataField = 'dDtConfirmacao'
    DataSource = dsMaster
    TabOrder = 9
  end
  object Edit4: TEdit [16]
    Tag = 1
    Left = 555
    Top = 234
    Width = 113
    Height = 19
    TabOrder = 10
  end
  object DBEdit13: TDBEdit [17]
    Tag = 1
    Left = 672
    Top = 234
    Width = 99
    Height = 19
    DataField = 'dDtCancel'
    DataSource = dsMaster
    TabOrder = 11
  end
  object Edit3: TEdit [18]
    Tag = 1
    Left = 775
    Top = 234
    Width = 113
    Height = 19
    TabOrder = 12
  end
  object cxPageControl1: TcxPageControl [19]
    Left = 8
    Top = 267
    Width = 889
    Height = 337
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 13
    ClientRectBottom = 333
    ClientRectLeft = 4
    ClientRectRight = 885
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Itens'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 40
        Width = 881
        Height = 269
        Align = alClient
        AllowedOperations = [alopDeleteEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsItem
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItem'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            ReadOnly = True
            Width = 403
          end
          item
            EditButtons = <>
            FieldName = 'nQtde'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 86
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeConf'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeDiv'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 80
          end>
        object RowDetailData: TRowDetailPanelControlEh
          object Panel1: TPanel
            Left = 0
            Top = 0
            Width = 0
            Height = 41
            Align = alTop
            BevelOuter = bvNone
            Color = clWhite
            TabOrder = 0
            object Label5: TLabel
              Tag = 2
              Left = 960
              Top = 20
              Width = 146
              Height = 13
              Caption = 'Abatimento Desconsiderado'
            end
            object cxTextEdit3: TcxTextEdit
              Left = 928
              Top = 12
              Width = 25
              Height = 21
              Enabled = False
              Style.Color = clBlue
              StyleDisabled.Color = clRed
              TabOrder = 0
            end
            object cxTextEdit4: TcxTextEdit
              Left = 776
              Top = 12
              Width = 25
              Height = 21
              Enabled = False
              Style.Color = clBlue
              StyleDisabled.Color = 8454143
              TabOrder = 1
            end
            object cxButton8: TcxButton
              Left = 2
              Top = 4
              Width = 175
              Height = 33
              Caption = 'Visualizar / Alterar Grade'
              TabOrder = 2
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2F2E2E2E2E2E2
                E2E2E2E2E2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFF2F2F2E2E2E27F7F7F4444445555559B9B9BFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFF2F2F2F2F2F2F2F2F2F2F2F2F2F2F2E2E2E27D7D7D4E4E4E9393
                938E8E8E636363FFFFFFFFFFFFFFFFFFF2F2F2F2F2F2E2E2E2E2E2E2E2E2E2E2
                E2E2E2E2E27B7B7B4F4F4F9191918F8F8FACACAC565656FFFFFFFFFFFFF2F2F2
                E2E2E27171714141415C5C5C5C5C5C3E3E3E3B3B3B3D3D3D949494969696ACAC
                AC636363949494FFFFFFF2F2F2E2E2E2494949A0A0A0E3E3E3EEEEEEEEEEEEE2
                E2E2ABABAB3A3A3A6A6A6AAFAFAF646464949494FFFFFFFFFFFFE2E2E24C4C4C
                CECECEECEAE6E3D9BCDCCC9BDCCC9BE3D9BCECEAE6CBCBCB4141415252529494
                94FFFFFFFFFFFFFFFFFF8787879A9A9AEDEBE7DBCA95DAC586DBC78ADCC88BDB
                C78ADDCC99ECEAE6ABABAB505050FFFFFFFFFFFFFFFFFFFFFFFF4F4F4FE8E8E8
                E4DBBEDBC688DECC91E1D099E2D29CE1D099DECC91E4D9BAF2F2F2474747FFFF
                FFFFFFFFFFFFFFFFFFFF5F5F5FF2F2F2DFCFA0DECC91E2D4A0E6DAACE7DDB1E6
                DAACE2D4A0E2D4A8F1F1F1737373FFFFFFFFFFFFFFFFFFFFFFFF606060F4F4F4
                E0D1A3E1D099E6DAACEBE5C0EEEAC9EBE5C0E6DAACE4D8AEF2F2F2777777FFFF
                FFFFFFFFFFFFFFFFFFFF555555EDEDEDE8DFC4E2D29CE7DDB1EEEAC9F3F3DBEE
                EAC9E7DDB1E9E1C6F9F9F94D4D4DFFFFFFFFFFFFFFFFFFFFFFFF8D8D8DB0B0B0
                F6F6F6E5D8AFE6DAACEBE5C0EEEAC9EBE5C0E8DEB9F4F3EFBABABA838383FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF5A5A5ADDDDDDF9F9F9EDE8D4ECE5CBECE7CEEE
                EAD8F8F8F8DBDBDB5A5A5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                5B5B5BBCBCBCF4F4F4FDFDFDFDFDFDFFFFFFBCBCBC5B5B5BFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8686865A5A5A7C7C7C7C7C7C51
                5151868686FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              LookAndFeel.Kind = lfOffice11
            end
            object btnConferencia: TcxButton
              Left = 176
              Top = 4
              Width = 175
              Height = 33
              Caption = 'Confer'#234'ncia'
              TabOrder = 3
              TabStop = False
              Visible = False
              Glyph.Data = {
                36030000424D3603000000000000360000002800000010000000100000000100
                18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FAF7F9FBF9FF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFF7FAF837833D347D3AF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FBF8408E4754A35C4F9F5733
                7D39F8FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                F8FBF8499A515BAC6477CA8274C87E51A059347E3AF8FBF9FFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFF8FCF951A65A63B56D7ECE897BCC8776CA8176
                C98152A25A357F3BF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FCFA59B063
                6BBD7684D2907AC98560B26A63B46D78C98378CB8253A35C36803CF9FBF9FFFF
                FFFFFFFFFFFFFFFFFFFFD3ECD66CBD7679C98680CE8D53A75CB2D6B59CC9A05C
                AD677CCC8679CB8554A45D37813DF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD9EFDC
                6CBD756DC079B5DBB9FFFFFFFFFFFF98C79D5EAE687DCD897CCD8756A55F3882
                3EF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD5EDD8BEE2C3FFFFFFFFFFFFFFFFFFFF
                FFFF99C89D5FAF697FCE8A7ECE8957A66039833FF9FBF9FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF99C89E60B06A81CF8D7FCF
                8B58A761398540F9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF99C99E62B26C82D18F7AC88557A6609FC4A2FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9F63B3
                6D5FAF69A5CBA9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9FA5CEA9FFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
              LookAndFeel.NativeStyle = True
            end
            object btnVisualizaConfe: TcxButton
              Left = 350
              Top = 4
              Width = 175
              Height = 33
              Caption = 'Visualizar Itens Confer'#234'ncia'
              TabOrder = 4
              TabStop = False
              Visible = False
              Glyph.Data = {
                06030000424D060300000000000036000000280000000F0000000F0000000100
                180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFF00000000000000000000000000000000000000
                0000000000000000000000000000000000000000FFFFFF000000FFFFFF000000
                F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4
                F4000000FFFFFF000000FFFFFF000000F4F4F4CCCBCAD5D4D4DCDBDB936700EB
                EBEAEBEBEAECECEBECEBEBEAE9E9F4F4F4000000FFFFFF000000FFFFFF000000
                F4F4F4C6C4C2E9E9E9936700936700936700F6F6F6F6F6F6F6F6F6E6E6E6F4F4
                F4000000FFFFFF000000FFFFFF000000F4F4F4C2BFBC93670093670093670093
                6700936700F5F5F5F4F4F4E2E2E1F4F4F4000000FFFFFF000000FFFFFF000000
                F4F4F4936700936700936700EAEAEA936700936700936700F2F2F2DEDDDCF4F4
                F4000000FFFFFF000000FFFFFF000000F4F4F4BCB7B2936700DFDCDAE3E1E0E8
                E8E8936700936700936700D6D5D4F4F4F4000000FFFFFF000000FFFFFF000000
                F4F4F4B9B3AED7D1CDD9D4D0DBD7D4DFDDDBE3E2E1936700936700936700F4F4
                F4000000FFFFFF000000FFFFFF000000F4F4F4B9B3AED5CFCBD5CFCBD6D1CDDA
                D5D2DEDBD8E1DFDD936700936700936700000000FFFFFF000000FFFFFF000000
                F4F4F4B9B3AED5CFCBD5CFCBD5CFCBD5CFCBD8D3D0DCD8D5DFDDDB936700F4F4
                F4000000FFFFFF000000FFFFFF000000F4F4F4B9B3AEB9B3AEB9B3AEB9B3AEB9
                B3AEB9B3AEBAB4AFBDB9B4C1BEBBF4F4F4000000FFFFFF000000FFFFFF000000
                F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4
                F4000000FFFFFF000000FFFFFF00000000000000000000000000000000000000
                0000000000000000000000000000000000000000FFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000}
              LookAndFeel.NativeStyle = True
            end
          end
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 881
        Height = 40
        Align = alTop
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        object btExcluirItens: TcxButton
          Left = 296
          Top = 2
          Width = 146
          Height = 33
          Caption = 'Excluir Itens'
          Enabled = False
          TabOrder = 0
          OnClick = btExcluirItensClick
          Glyph.Data = {
            0A020000424D0A0200000000000036000000280000000B0000000D0000000100
            180000000000D401000000000000000000000000000000000000F0F0F0333333
            333333333333333333333333333333333333333333333333F0F0F0000000F0F0
            F0333333FFFFFFCCCCCCCCCCCC999999999999999999666666333333F0F0F000
            0000F0F0F0333333FFFFFF999999CCCCCC666666999999333333666666333333
            F0F0F0000000F0F0F0333333FFFFFF999999CCCCCC6666669999993333336666
            66333333F0F0F0000000F0F0F0333333FFFFFF999999CCCCCC66666699999933
            3333666666333333F0F0F0000000F0F0F0333333FFFFFF999999CCCCCC666666
            999999333333666666333333F0F0F0000000F0F0F0333333FFFFFF999999CCCC
            CC666666999999333333666666333333F0F0F0000000F0F0F0333333FFFFFF99
            9999CCCCCC666666999999333333666666333333F0F0F0000000F0F0F0333333
            FFFFFFCCCCCCCCCCCC999999999999999999666666333333F0F0F00000003333
            3333333333333333333333333333333333333333333333333333333333333300
            0000333333CCCCCCCCCCCCCCCCCCCCCCCC999999999999999999999999666666
            3333330000003333333333333333333333333333333333333333333333333333
            33333333333333000000F0F0F0F0F0F0F0F0F0F0F0F0333333333333333333F0
            F0F0F0F0F0F0F0F0F0F0F0000000}
          LookAndFeel.NativeStyle = True
        end
        object btAddItem: TcxButton
          Left = 443
          Top = 2
          Width = 146
          Height = 33
          Caption = 'Adicionar Itens'
          Enabled = False
          TabOrder = 1
          OnClick = btAddItemClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFC5DFEF5FA7D458A3D2BDDAED84B094257341196B
            3725734184B094FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDCEDF678B7DC2D8EC88F
            CDEB6FB7E22C7C7D288C5364BA8D95D2B264BA8D288C5381AE91FFFFFFFFFFFF
            EEF6FB95C9E43F9CCE82C4E5CCF4FFC4EFFF8BD2F11E6F3F62BA8B60BA87FFFF
            FF60B98767BC8F20703DFAFDFEB1D9EC55ABD57DC0E0C7EEFCCCF2FFA8E8FF94
            E0FE41BAE7317B4C9CD4B6FFFFFFFFFFFFFFFFFF95D2B2196B3773BDDE77BDDC
            BFE5F6DBF6FFC1EEFFA5E5FF9FE3FF94E1FE46C1EA44886190D3B192D6B1FFFF
            FF65BC8C67BC8F1B6D3C46AAD4E7FBFEDDF6FFC1EFFFB7EBFFABE8FFA4E4FF96
            E1FE48C6EB49A6A861AB8195D4B4BAE6D06ABB8F2D8F572179774FAFD7E2F6FC
            D4F3FFC9F0FFBEEDFFB3EAFFADE7FF7CD9FE48C7EF43C4EA4EAAAD5796744F8E
            664086605AA09D338EC852B2D7E2F6FCD7F4FFCEF2FFC8EFFFBAEBFF92DBFB56
            C1F148C2F93BBDF047C5EC45BDE942B5E647B1E688CAEE3591C954B5D8E2F6FD
            DAF4FFD5F3FFBDEBFF89D5F769C9F54CB4E98DDAFB8CDCFF48C4F938B6EC48BF
            E84FBBE88CD0F03794CA56B7D9E2F8FDD4F3FFB0E4FA86CFF17FD0F578D0F54C
            B1E4B0E4FAB6E9FF9BE1FF78D6FE40BDF53DB5E990D5F13996CB50B5D9E1F8FE
            CDEBF992D2ED84CCEB6FBFE556B1DB3B94C8CEECFAD9F5FFB9EAFF95DFFE77D5
            FFA5E4FF84DCFB3294CA91D2E74EB5D9A5D9EDD2EBF5BEDEED95C9DE89C3DB70
            B8D669B9DD90D7F57FCFF59DDBF8AAE3FA84CAEC51A6D57ABADDFFFFFFCFEBF5
            78C7E27EC6E0D1EEF7F6FFFFF0FEFFCBEDFB50ADDA8BD7F7AAE1F995D6F262B2
            DB64B2D9C2E1F0FFFFFFFFFFFFFFFFFFFCFEFEB9E2F065BFDE92CFE5E6F8FCE3
            F6FEAFDDF2B2E4F772C0E156AFD7ADD8EBF8FCFDFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFF0F9FCA3D8EB56B7DA9CD5EA88CCE74EB0D798D0E7ECF6FBFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE1F2F883
            CAE47AC5E2DCEFF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
        object btConferir: TcxButton
          Left = 150
          Top = 2
          Width = 145
          Height = 33
          Caption = 'Confer'#234'ncia'
          Enabled = False
          TabOrder = 2
          OnClick = btConferirClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FAF7F9FBF9FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFF7FAF837833D347D3AF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FBF8408E4754A35C4F9F5733
            7D39F8FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            F8FBF8499A515BAC6477CA8274C87E51A059347E3AF8FBF9FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFF8FCF951A65A63B56D7ECE897BCC8776CA8176
            C98152A25A357F3BF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FCFA59B063
            6BBD7684D2907AC98560B26A63B46D78C98378CB8253A35C36803CF9FBF9FFFF
            FFFFFFFFFFFFFFFFFFFFD3ECD66CBD7679C98680CE8D53A75CB2D6B59CC9A05C
            AD677CCC8679CB8554A45D37813DF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD9EFDC
            6CBD756DC079B5DBB9FFFFFFFFFFFF98C79D5EAE687DCD897CCD8756A55F3882
            3EF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD5EDD8BEE2C3FFFFFFFFFFFFFFFFFFFF
            FFFF99C89D5FAF697FCE8A7ECE8957A66039833FF9FBF9FFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF99C89E60B06A81CF8D7FCF
            8B58A761398540F9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF99C99E62B26C82D18F7AC88557A6609FC4A2FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9F63B3
            6D5FAF69A5CBA9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9FA5CEA9FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
        object btLeitura: TcxButton
          Left = 4
          Top = 2
          Width = 145
          Height = 33
          Caption = 'Digita'#231#227'o/Leitura'
          Enabled = False
          TabOrder = 3
          OnClick = btLeituraClick
          Glyph.Data = {
            46020000424D460200000000000036000000280000000F0000000B0000000100
            1800000000001002000000000000000000000000000000000000FFFFFF000000
            FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFF
            FF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FF
            FFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000
            FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFF
            FF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FF
            FFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF7F7F7F
            FFFFFF7F7F7F7F7F7FFFFFFF7F7F7FFFFFFF7F7F7FFFFFFF7F7F7F7F7F7FFFFF
            FF7F7F7FFFFFFF0000000D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D
            06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF0D06FF000000FFFFFF7F7F7F
            FFFFFF7F7F7F7F7F7FFFFFFF7F7F7FFFFFFF7F7F7FFFFFFF7F7F7F7F7F7FFFFF
            FF7F7F7FFFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FF
            FFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000
            FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFF
            FF000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFFFF000000FF
            FFFF000000FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000
            FFFFFF000000000000FFFFFF000000FFFFFF000000FFFFFF000000000000FFFF
            FF000000FFFFFF000000}
          LookAndFeel.NativeStyle = True
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Valores'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 881
        Height = 309
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsItem
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh2Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 50
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            ReadOnly = True
            Width = 557
          end
          item
            EditButtons = <>
            FieldName = 'nQtde'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 77
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValTotal'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM TransfEst'
      'WHERE nCdEmpresaOrigem = :nCdEmpresa'
      'AND nCdTransfEst = :nPK')
    Left = 80
    Top = 408
    object qryMasternCdTransfEst: TIntegerField
      FieldName = 'nCdTransfEst'
    end
    object qryMasternCdEmpresaOrigem: TIntegerField
      FieldName = 'nCdEmpresaOrigem'
    end
    object qryMasternCdLojaOrigem: TIntegerField
      FieldName = 'nCdLojaOrigem'
    end
    object qryMasternCdLocalEstoqueOrigem: TIntegerField
      FieldName = 'nCdLocalEstoqueOrigem'
    end
    object qryMasternCdTabTipoTransf: TIntegerField
      FieldName = 'nCdTabTipoTransf'
    end
    object qryMasternCdEmpresaDestino: TIntegerField
      FieldName = 'nCdEmpresaDestino'
    end
    object qryMasternCdLojaDestino: TIntegerField
      FieldName = 'nCdLojaDestino'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasternCdLocalEstoqueDestino: TIntegerField
      FieldName = 'nCdLocalEstoqueDestino'
    end
    object qryMasterdDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryMasternCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qryMasterdDtFinalizacao: TDateTimeField
      FieldName = 'dDtFinalizacao'
    end
    object qryMasternCdUsuarioFinal: TIntegerField
      FieldName = 'nCdUsuarioFinal'
    end
    object qryMasterdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryMasternCdUsuarioCancel: TIntegerField
      FieldName = 'nCdUsuarioCancel'
    end
    object qryMasterdDtConfirmacao: TDateTimeField
      FieldName = 'dDtConfirmacao'
    end
    object qryMasternCdUsuarioConfirm: TIntegerField
      FieldName = 'nCdUsuarioConfirm'
    end
    object qryMasternValTransferencia: TBCDField
      FieldName = 'nValTransferencia'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgModoManual: TIntegerField
      FieldName = 'cFlgModoManual'
    end
    object qryMastercFlgAutomaticaCD: TIntegerField
      FieldName = 'cFlgAutomaticaCD'
    end
    object qryMastercFlgFaturaTransfEst: TIntegerField
      FieldName = 'cFlgFaturaTransfEst'
    end
  end
  inherited dsMaster: TDataSource
    Left = 80
    Top = 440
  end
  inherited qryID: TADOQuery
    Left = 400
    Top = 408
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 560
    Top = 408
  end
  inherited qryStat: TADOQuery
    Left = 432
    Top = 408
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 528
    Top = 440
  end
  inherited ImageList1: TImageList
    Left = 560
    Top = 440
    Bitmap = {
      494C01010D000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F1F1F100E6E6E600E1E1
      E100E0E0E000DEDEDE00DDDDDD00DBDBDB00DADADA00D9D9D900D7D7D700D6D6
      D600D5D5D500DADADA00E9E9E900FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F4F4F4007898B4001C5D95001F60
      98001C5D95001B5C96001B5B95001A5B95001A5994001A5994001A5994001959
      9300195892001958910063819E00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EFEFEF001F6098003AA9D9001F60
      98003AA9D90046C6F30044C4F30042C4F30042C3F30042C3F40041C3F40041C2
      F40040C2F40034A3D9001A5A9200FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F1F1F1001F649C0050CBF2001F64
      9C0050CBF2004CCAF30049C9F30047C7F30046C6F20043C5F30043C4F30042C4
      F30042C3F30041C3F3001A599400FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F3F3F30021679E0065D4F4002167
      9E0065D4F4005BD1F30051CEF3004ECCF2004BC9F20048C9F20047C7F20045C6
      F20044C5F20043C5F2001B5C9500FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F4F4F400246CA2007FDFF600246C
      A2007FDFF60067D7F4005DD3F30058D1F20052CEF2004ECDF1004CCAF20048C9
      F10046C7F10046C7F1001D5E9800FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F6F6F600276FA6009BE9F800276F
      A6009BE9F80074DEF50069DAF40062D7F3005BD4F20054D0F10051CEF1004ECC
      F10048C9F00049C9F0001E619A00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F8F8F8002A74AA00B3F1FB0081E4
      F7001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E
      98001D5E98001D5E980091B0C800FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F9F9F9002D7AAE00C6F7FD008EE8
      F80088E7F80080E4F60078E1F50070DEF40067DAF3005ED6F10057D2F0004ECE
      EE0064D6F2002268A000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FAFAFA00307FB300D4FAFE0099EC
      FA0092EBF9008BE8F800C2F6FC00B9F4FB00AFF1FA00A3EEF90097EAF80081E2
      F50062C2DF00266EA300FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003386B800DFFCFE00A4F0
      FB009DEFFA00D6FBFE002F7EB1002E7BB0002D7BAF002C7AAE002C78AD002974
      AA003076AB0091B0C800FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFDFD00388BBD00BADFED00E7FD
      FF00E4FDFF00B3DEED003383B600F3F3F300F7F7F700F6F6F600F5F5F500F3F3
      F300F4F4F400F9F9F900FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00A8CDE2004295C300398F
      C000378DBE003D8EBE00A2C6DB0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000369DD9003199D8002C94
      D7002890D600238CD5001E88D4001A84D3001580D200117CD1000E79D1000A76
      D0000773CF000470CF00016ECE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003DA3DA00BCEBFA00BCEB
      FC00BFEEFE00C6F4FF00CEF8FF00D3FAFF00D0F8FF00C7F2FF00BAE9FC00B3E4
      F900B0E2F800B0E2F8000571CF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000043A8DB00BFECFB0059CF
      F50041B0EC004EBAEF005AC2EF0060C6EF005CC4EF004CB6EF0037A5E6002A9A
      E10038B8EE00B1E3F8000975D0000000000000000000FEEABA00755D29007257
      1E006D4B0F007F5C1A00835A1500724A0200774E09006946020069470B007156
      1E00644E1E00FFFBD000FCEFC900FFFFDE000000000000000000000000009E9E
      9E00656464006564640065646400656464006564640065646400656464006564
      6400898A8900B9BABA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000049ADDC00C1EEFB005FD3
      F7006CDBFC007FE5FF008FEDFF0097F2FF0093EDFF007CDFFF005BCCF80046BE
      EF003CBAEE00B3E3F9000E79D10000000000FFFFD70068531C00D7BE8600F9DD
      A100D9B97800F7D49000F0CA8200EDC57D00FAD48C00F2CF8B00FCDC9B006C50
      1400BEA8740067562500FFFDCF00FFFACE000000000000000000D5D5D5005157
      570031333200484E4E00313332003D4242003D4242003D4242003D424200191D
      230051575700C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000004EB2DD00C3EFFB0065D6
      F8004CB6EC005ABDEF0095EBFF003097DD004D82AB0084E1FF0041A9E900329F
      E10042BEEF00B4E5F900137ED200000000005037000070571700785C1B005C3F
      000084661F0074530A0079560C00815E14007A570D0078570E00593A00008064
      23006C531500BFAA6D0075612800FFF3BA0000000000000000008C908F00191D
      230031333200313332003133320031333200313332003133320031333200191D
      230004050600898A890000000000000000000000000000000000000000000000
      000039A3B50039A3B50039A3B50039A3B50039A3B50039A3B50039A3B5000000
      0000000000000000000000000000000000000000000053B7DE00C6F0FC006AD9
      F8007CE2FD0090E8FF0099E9FF00329FDF00548BB2008AE2FF006AD0F90050C5
      F10046C1F000B6E7F9001883D300000000008D702D00E0C38000E6C98400F2D4
      8D00EACD8300E3C37A00ECCC8100FFF2A700FFF0A500FFF0A500FEE19700E1C4
      7F0076591600765A1900543A0000FFFFCF00000000000000000065646400898A
      8900DADDD700B9BABA00BEBEBE00BEBEBE00BEBEBE00BEBEBE00BEBABC00CCCC
      CC005157570065646400000000000000000000000000000000000000000043B4
      C500AEF4FF009FF1FE009CF0FD0099EEFC0095EDFB0092EBF90097EDFA0043B4
      C500000000000000000000000000000000000000000058BBDF00C7F1FC006FDC
      F90056BBED0061BDEF009BE7FF0035A6E2004BA4E10090E2FF0049ADE90038A4
      E30049C4F000B8E8F9001E88D400000000005A3B0000FDDE9900DFC07B00F7D8
      9300F5D79000E8CA8300F1D38C00CAAC6500D1B46A00D5B86E00E1C37C00EED0
      89006A4B0600DFC07B0080611C00FFF5B0000000000000000000717272003D42
      4200898A8900777B7B007B8585007B8585007B8585007B8585007B8585009392
      92003D424200777B7B00000000000000000000000000000000000000000047BA
      CB009FF1FE0085ECFD0083EAFB0080E8F9007DE6F7007AE4F50088E8F70047BA
      CB0000000000000000000000000000000000000000005CBFE000C8F3FC0075DF
      F90089E6FD0095E7FF009AE5FF00AAEEFF00A8EDFF0099E3FF0074D5F90059CC
      F3004FC8F100BBE9FA00248DD50000000000735213007F5F1E007A5A19006244
      030071531200765A19006E5211006F5312006E531000765916006F520F007959
      17007B591700E2C17C00D3B06E007B5814000000000000000000B6B6B6003133
      32007B858500AEAEAE00A4A8A800A4A8A800A4A8A800A4A8A800A4A8A800484E
      4E0031333200B9BABA0000000000000000000000000000000000000000004ABE
      CF009CF0FD0083EAFB0080E8F9007DE6F7007AE4F50076E1F2007AE3F3004ABE
      CF00000000000000000000000000000000000000000060C2E100C9F3FC00CBF3
      FD00D4F6FE00D7F6FF00D8F4FF00E0F8FF00DFF8FF00DAF5FF00CDF1FC00C2ED
      FA00BDEBFA00BDEBFA002B93D6000000000076541F00DCBA8400DEBF8800F3D5
      9E00E7C99200D8BD8500FADFA600E2C78E00F1D59900E3C78B00D0B37600EFD0
      93007250140075521300EDC88C007A5315000000000000000000D5D5D500484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00898A
      8900484E4E00C6C6C60000000000000000000000000000000000000000004DC2
      D30099EEFC0080E8F9007DE6F7007AE4F50036AEBF0036AEBF0036AEBF0040AF
      C100000000000000000000000000000000000000000061C3E10088A0A8009191
      91008E8E8E005AB9DC0055B8DF0051B5DE004DB1DD0049ADDC0046A8D7007878
      780076767600657E8D003199D80000000000FFFAD1007E6135006D5024005B41
      1300755B2D006D5324005E4513006048140063481500694D17007D5E2900BE9F
      6800DBB9830079551F00734D17007C5620000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE007B85
      85003D4242000000000000000000000000000000000000000000000000004FC5
      D70095EDFB007DE6F7007AE4F50076E1F20044BCCD00BBF6FF004FC5D700C1EA
      F000000000000000000000000000000000000000000000000000B1B1B100C6C6
      C60094949400FBFBFB0000000000000000000000000000000000FBFBFB007D7D
      7D00ABABAB00969696000000000000000000FFEFD400FFF7D9005A432300FFFE
      DB00FFF4D000FFFFE000FFFFD800FFFFDC00FFFCCC00FFFFDB00FFFDCE007759
      2A00DDBC8E00E0BD9100C5A276006B481C000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00D5D5D5007172
      72003D42420000000000000000000000000000000000000000000000000051C9
      DA009CEEFB008DEAF80087E8F7007AE2F3004CC4D50051C9DA00C1EBF100FFFF
      FF00000000000000000000000000000000000000000000000000BCBCBC00C4C4
      C400A1A1A100EEEEEE0000000000000000000000000000000000EBEBEB008989
      8900A9A9A900A4A4A4000000000000000000FFF3E200FFF1DE00FFFFEE005342
      2800FFFFE7006C593600554118005E481E00695225006C542600FFFFD300573C
      100070532C006F522D0074543100FFF7D4000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE007B858500191D2300191D
      230051575700C6C6C60000000000000000000000000000000000000000007ED8
      E50053CBDC0053CBDC0053CBDC0053CBDC0053CBDC00C3EDF300FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000D4D4D400BABA
      BA00BFBFBF00A6A6A600F2F2F200FDFDFD00FDFDFD00F1F1F10093939300A8A8
      A8009E9E9E00C3C3C3000000000000000000FAF0E900FBF0E800F1E6D8005348
      3400FFFFE800FFF3D200FFFFE200FFFFDF00FFFFDD00FFFFD800FFFFDC00FFFF
      E0005C442000FFECCC00FFE8CC00FFF5DB000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE0071727200484E4E003D42
      420093929200C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FBFBFB00AEAE
      AE00C4C4C400BEBEBE00A1A1A100969696009393930097979700AEAEAE00AEAE
      AE0095959500FBFBFB000000000000000000F1EEF000FAF6F500FEFBF300FBF4
      E500564C3400FFFFE70064552E004E3E13005F4D1E005F4D1E0065522700FFFF
      E0005D4A2900FFFDE200FFFFED00FFEED9000000000000000000CCCCCC00484E
      4E0031333200575D5E005157570051575700575D5E00191D2300191D23009392
      9200CCCCCC00BEBEBE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EEEE
      EE00AEAEAE00BCBCBC00CACACA00CCCCCC00CACACA00C2C2C200ADADAD009B9B
      9B00E9E9E900000000000000000000000000F2F8FF00DBE0E300F3F4F000EFEE
      E0005A563E00FFFFE700FCF0C800FFFFDD00FFFFD000FFFFDB00FFFED000FFFF
      E000FFFFE5005E4F3500EADCC900FFFBE900000000000000000000000000B6B6
      B6009E9E9E009E9E9E009E9E9E009E9E9E009E9E9E00A4A8A800AEAEAE00C6C6
      C600BEBEBE00BEBEBE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FBFBFB00D0D0D000BABABA00B1B1B100AEAEAE00B3B3B300C9C9C900FAFA
      FA0000000000000000000000000000000000E5EEF700E4EDF100F3F8F600EEF0
      E400FDFBE3004B4423005F562B005D51210067582700584918005F5022005A4B
      24005B4E2E0052472C00FFFEEC00FAF0DF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      6300636363006363630063636300636363006363630063636300C6948C00C694
      8C000000000000000000C6948C00C6948C000000000000000000000000000000
      0000C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFFFFF008C9C9C0000000000C6948C000000000000000000000000000000
      00000000000000000000EFFFFF00000000000000840000008400000000000000
      0000EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C00000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C6000000000000000000C6DEC60000000000636363000000
      0000C6948C008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C0000000000EFFF
      FF008C9C9C008C9C9C0000000000C6948C000000000000000000000000000000
      84000000840000000000EFFFFF00EFFFFF00000000000000840000000000EFFF
      FF00EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000BDBDBD000000
      000000000000000000000000000000000000C6948C0000000000EFFFFF008C9C
      9C008C9C9C000000000000000000C6948C000000000000000000000084002100
      C6002100C6000000000000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF008C9C
      9C000000000000000000000000006363630000000000000000002100C6000000
      84002100C6002100C6002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF00092369005157570000000000000000000000000000000000000000000000
      000000000000E7F7F700EFFFFF00000000000000000000000000000000000000
      00000000000000000000000000006363630000000000000084002100C6002100
      C6002100C6000000FF002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF0009236900515757000000000000000000000000000000000000000000EFFF
      FF00EFFFFF00DEBDD600DEBDD600EFFFFF00EFFFFF00C6948C00000000008C9C
      9C008C9C9C008C9C9C000000000063636300000000002100C6000000FF002100
      C6000000FF000000FF0000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF0009236900515757000000000000000000000000008C9C9C00000000000000
      000000000000000000000000000000000000EFFFFF0000000000000000000000
      00008C9C9C008C9C9C000000000063636300000000000000FF002100C6000000
      FF000000FF0000000000EFFFFF00EFFFFF002100C6000000FF0000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      00008C9C9C008C9C9C000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000EFFFFF00000000000000FF002100C6000000FF000000
      0000EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100A56300000000
      000000000000000000000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000000000000000FF000000FF000000FF002100C6000000
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C0000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF000000FF002100
      C6002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000000000008C9C9C00FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100000000006B42
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF002100C6000000
      FF002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000000000008C9C9C008C9C
      9C0000000000000000000000000000000000000000000000000000000000A563
      0000A5630000A563000000000000636363000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF002100
      C600000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A80000000000000000000000000000000000000000000000
      00008C9C9C008C9C9C008C9C9C008C9C9C000000000000000000000000000000
      0000000000000000000000000000636363000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      630000000000000000000000000000000000C6948C00C6948C00C6948C006363
      6300636363006363630063636300636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADBBBA0096ADAF009BB6
      BA0091AAAE00A7BABF0096A9AE00A0B0B6009CACB200A0B0B600A0B0B60093A9
      AE0089A2A6009AB5B9009FB3B400C1D3D2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AEBCB800BECCCA00BAD3D500B8D2
      D800C0D8DE00BFD2D700C6D7DA00B9C8CB00B9C8CB00C0CFD200C2D3D600C8DC
      E100C8E0E600BCD6DC00BBCDCE00ADBBB9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A3B3AC00C2D4CD00C3D8D900C2D7
      D900B5C6C9008F9B9B00939B9A00979E9B009095930089908D009BA3A20093A2
      A4008195960097ACAE00CCDAD800A0ABA8000000000000000000BACACE00BACA
      CE00BACACE00BACACE00BACACE00BACACE00BACACE0000000000C2D2D600AEBE
      C2009DABAC00C6D6DA000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A210000000000A2B6B100C5DBD600CFE4E200ADBF
      BE0002100F00000200000002000010100A000D0E050002020000090D08000A15
      1300000201007A898B00C8D4D400A0ABA90000000000C6D6DA00C6DADA00C6DA
      DA00C6DADA00CADADE00CDDEE200CDDEE200CDDEE200CEE2E400BECED200636C
      6C003D4242009AA6AA0000000000000000000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000091AAA600BFDAD600BED1CE00C9D6
      D40000080600C5C7C100C5C3BB00BEB9B000C2BEB300B9B7AD00D8D6CE00A7AC
      AA000F1A18008D9A9800CDD9DB0097A3A50000000000C2D6D600C6D6DA00C6DA
      DA00CDDEE20000000000ABB9BA00ABB9BA00B2C1C200000000005C646500AEAE
      AE00898A89003D42420000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF000923690051575700000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000094B0B000C2E0E100BDCDCC00CDD6
      D300050B0600F8F4E900EFE6D900FFF5E500FFF3E100FFF2E200E3DACD00C9CA
      C100000300008E979400C8D4DA00A2AFB70000000000C2D6D600C6DADA00CDDE
      E200A6B4B60051575700191D230031333200575D5E00484E4E00BEBEBE00898A
      890031333200A2AFB000000000000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000096ACAA00C5DADB00C4D6D700CAD6
      D60000020000F0EADF00F7EADA00FFEFDC00FFEFDA00FCEBD800FFFCEC00C7C8
      BF0000020000A5B1B100C4D7DC0097AAAF0000000000C2D6D600CADEDE00909E
      A0003133320084909100D2DEDF00CEDADE00777B7B003D4242009E9E9E003D42
      42009AA6AA00D6EAEB00000000000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000099ABAA00C6DBD900C4D5D800C9D6
      D80000020000F0EADF00F7EADA00FFEFDA00FFF3DD00F5E2CD00F0E3D300B2B4
      AE000C1512007A878900C3D7DC0096AAAF0000000000C2D6D600C6DADA003D42
      4200BECED200BAC5C600D5D5D500CCD5D500BECACA00ABB9BA00191D23008490
      9100CDDEE200CADEDE00000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000099ABAA00C6DADB00C6D5D800C9D6
      D80000010000EEEADF00F5EBDA00FEEEDD00FDECD900FFFFEE00D8CDBF00BCBE
      B8000002010099A6A800C4D7DC0097AAAD0000000000CDDEE2006D7778005157
      5700313332003133320031333200313332003133320031333200515757005C64
      6500C2D2D200CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000ECEAE000F2EBDC00FBEEDE00FAECDA00FFFBE900E6DED100C1C5
      C0000001000089969800C4D7DC0097AAAD0000000000D3E4E500575D5E00575D
      5E001070820007F0F90007F0F90007F0F90007F0F9003D424200575D5E00484E
      4E00C6D6DA00CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000EBE9E100EEEADF00F7EEE000E3D9C800F6EDDF00FCF8ED00B6BC
      B700070F0E0093A0A200C6D6DC0097AAAD0000000000D3E4E500575D5E00575D
      5E00191D2300107082001070820010708200107082003133320071727200484E
      4E00C2D6D600CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8D9DC00C8D4D800CAD6
      D80000010100E5E9E300E9EAE100F0EEE300E3DFD400F4F2E7000203000099A0
      9D0000020200C3CFD100C6D7DA0099ABAC0000000000C6DADA00A6B4B6003D42
      42006D6C6B00777B7B00898A89008C908F00939292008E9A9A003D42420096A3
      A600C6DADA00CADADE00000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A003163630031636300000000009BACAF00C7D7DD00CDD9DD00C6D2
      D40000020200FBFFFC00F6FAF400EFF2E900FFFFF700F0F3EA00000200000001
      0000CDD7D700C8D4D600C5D7D8009AACAD0000000000C2D6D600CEE2E4005157
      57005C646500909EA00000000000D6E6EA00B2C1C2007B8585005C646500D3E4
      E500C6D6DA00CADADA00000000000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B5003163630000000000000000009AABAE00C3D3D900CFDBDF00C1CC
      D00000010300000200000003000001080100000500000E150E0000020000CFD9
      D900BECACC00DEE9ED00CFDEE0009CAEAF0000000000C2D6D600CADEDE00C2D2
      D2005C646500484E4E003D4242003D424200484E4E00636C6C00CADADE00CADA
      DA00C6DADA00CADADE0000000000000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A800000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      000000000000000000000000000000000000A5B8BD00B7C7CD00C5D1D500BDC8
      CC00CCD8DA00D7E3E300B3C0BE00BFCDC900E0EEEA00B5C3BF00E0EDEB00B4C0
      C200BBC7C900E5F0F400B7C5C400A5B5B40000000000BED2D200C2D2D200C2D6
      D600D6EAEB00A6B4B6007B8585007B858500AFBDBE00D6EAEB00C2D2D600C2D2
      D200C2D2D200C2D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC600000000000000000000000000B1C4C900AEBEC50095A1A500ADB8
      BC00B2BEC200AAB8B70097A8A500A5B6B30094A5A20096A7A40090A19E009EAB
      AD00BBC7CB00939EA200B2C0BF00B8C6C4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF00FFFF000000000000FFFF000000000000
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000080000000000000000000000000000000
      010000000000000000000000000000008001FFFFFFFFFFFF8001FFFFFFFFFFFF
      80018000E003FFFF80010000C003FFFF80010000C003F01F80010000C003E00F
      80010000C003E00F80010000C003E00F80010000C003E00F80010000C007E00F
      C3C30000C007E00FC3C30000C003E00FC0030000C003FFFFC0030000C003FFFF
      E0070000E003FFFFF00F0000FFFFFFFFFFFFFFFF81C0FFFFC000F0030180FFFF
      DFE0E0010000E0035000C0010000C003D00280010000C003CF0680010000C003
      B9CE00010000C003A00200010000C0033F6200010000C003200200010000C003
      200E00010000C003200280030181C003800280038181C0038FC2C0078181C003
      C03EE00F8181FFFFC000F83F8383FFFF8000FFFFFFFFFFFF0000FFFFFFFFFE00
      0000C043E003FE0000008003C003000000008443C003000000008003C0030000
      00008003C003000000008003C003000000008003C003000000008003C0030000
      00008003C003000000008003C003000000008203C003000100008003C0030003
      00008003FFFF00770000FFFFFFFF007F00000000000000000000000000000000
      000000000000}
  end
  object qryEmpresaOrigem: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 144
    Top = 408
    object qryEmpresaOrigemnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresaOrigemcNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryEmpresaDestino: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 176
    Top = 408
    object qryEmpresaDestinonCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresaDestinocNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLojaOrigem: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK'
      'AND nCdEmpresa = :nCdEmpresa'
      'AND EXISTS(SELECT 1'
      '             FROM UsuarioLoja'
      '            WHERE UsuarioLoja.nCdUsuario = :nCdUsuario'
      '              AND UsuarioLoja.nCdLoja = Loja.nCdLoja)')
    Left = 208
    Top = 408
    object qryLojaOrigemnCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojaOrigemcNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryLojaDestino: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 240
    Top = 408
    object qryLojaDestinonCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojaDestinocNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryLocalEstoqueOrigem: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      ''
      'Set @nCdLoja = :nCdLoja'
      'Set @nCdLoja = IsNull(@nCdLoja,0)'
      ''
      'SELECT nCdLocalEstoque, cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdEmpresa = :nCdEmpresa'
      '   AND ((nCdLoja = @nCdLoja) OR (@nCdLoja = 0))'
      '   AND nCdLocalEstoque = :nPK'
      '')
    Left = 272
    Top = 408
    object qryLocalEstoqueOrigemnCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoqueOrigemcNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object qryLocalEstoqueDestino: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      '       ,@nCdTerceiro int'
      ''
      'Set @nCdLoja = :nCdLoja'
      'Set @nCdLoja = IsNull(@nCdLoja,0)'
      ''
      'Set @nCdTerceiro = :nCdTerceiro'
      'Set @nCdTerceiro = IsNull(@nCdTerceiro,0)'
      ''
      'SELECT nCdLocalEstoque, cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdEmpresa = :nCdEmpresa'
      '   AND ((nCdLoja = @nCdLoja) OR (@nCdLoja = 0))'
      '   AND ((nCdTerceiro = @nCdTerceiro) OR (@nCdTerceiro = 0))'
      '   AND nCdLocalEstoque = :nPK'
      '')
    Left = 304
    Top = 408
    object qryLocalEstoqueDestinonCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoqueDestinocNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object dsEmpresaOrigem: TDataSource
    DataSet = qryEmpresaOrigem
    Left = 144
    Top = 440
  end
  object dsLojaOrigem: TDataSource
    DataSet = qryLojaOrigem
    Left = 208
    Top = 440
  end
  object dsLocalEstoqueOrigem: TDataSource
    DataSet = qryLocalEstoqueOrigem
    Left = 272
    Top = 440
  end
  object dsEmpresaDestino: TDataSource
    DataSet = qryEmpresaDestino
    Left = 176
    Top = 440
  end
  object dsLojaDestino: TDataSource
    DataSet = qryLojaDestino
    Left = 240
    Top = 440
  end
  object dsLocalEstoqueDestino: TDataSource
    DataSet = qryLocalEstoqueDestino
    Left = 304
    Top = 440
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK '
      '   AND EXISTS(SELECT 1 '
      '                FROM LocalEstoque'
      
        '               WHERE LocalEstoque.nCdTerceiro = Terceiro.nCdTerc' +
        'eiro)')
    Left = 112
    Top = 408
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 112
    Top = 440
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 432
    Top = 440
  end
  object qryItem: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemBeforePost
    BeforeDelete = qryItemBeforeDelete
    OnCalcFields = qryItemCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdItemTransfEst'
      '      ,nCdProduto'
      '      ,nQtde'
      '      ,nValUnitario'
      '      ,nValTotal'
      ',nCdTransfEst'
      ',nQtdeConf'
      '  FROM itemTransfEst'
      ' WHERE nCdTransfEst = :nPK'
      'AND cEAN is null')
    Left = 336
    Top = 408
    object qryItemnCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryItemcNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmItem'
      Size = 150
      Calculated = True
    end
    object qryItemnQtde: TBCDField
      DisplayLabel = 'Quantidades|Digitada'
      FieldName = 'nQtde'
      Precision = 12
      Size = 2
    end
    object qryItemnValUnitario: TBCDField
      DisplayLabel = 'Transfer'#234'ncia|Valor Unit.'
      FieldName = 'nValUnitario'
      Precision = 12
    end
    object qryItemnValTotal: TBCDField
      DisplayLabel = 'Transfer'#234'ncia|Valor Total'
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryItemnCdTransfEst: TIntegerField
      FieldName = 'nCdTransfEst'
    end
    object qryItemnCdItemTransfEst: TAutoIncField
      FieldName = 'nCdItemTransfEst'
    end
    object qryItemnQtdeConf: TBCDField
      DisplayLabel = 'Quantidades|Conferida'
      FieldName = 'nQtdeConf'
      Precision = 12
      Size = 2
    end
    object qryItemnQtdeDiv: TBCDField
      DisplayLabel = 'Quantidades|Diverg'#234'ncia'
      FieldKind = fkCalculated
      FieldName = 'nQtdeDiv'
      Calculated = True
    end
  end
  object dsItem: TDataSource
    DataSet = qryItem
    Left = 336
    Top = 440
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto, cNmProduto, nValCusto'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 464
    Top = 408
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutonValCusto: TBCDField
      FieldName = 'nValCusto'
      Precision = 12
      Size = 2
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 400
    Top = 440
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object qryProdutoEAN: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 10
        Value = '1234567890'
      end>
    SQL.Strings = (
      'DECLARE @cEAN VARCHAR(20)'
      ''
      'Set @cEAN = :nPK'
      ''
      'SELECT nCdProduto'
      '      ,cNmProduto'
      '      ,nValCusto'
      '  FROM Produto'
      ' WHERE cEAN       = @cEAN'
      '    OR cEANFornec = @cEAN'
      
        '   AND ((nCdTabTipoProduto = 3 AND nCdGrade IS NOT NULL) OR nCdG' +
        'rade IS NULL)')
    Left = 464
    Top = 440
    object qryProdutoEANnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutoEANnValCusto: TBCDField
      FieldName = 'nValCusto'
      Precision = 12
      Size = 2
    end
    object qryProdutoEANcNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object SP_PROCESSA_TRANSFERENCIA_ESTOQUE: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_PROCESSA_TRANSFERENCIA_ESTOQUE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTransfEst'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 528
    Top = 408
  end
  object qryTrataProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cModo'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'nQtde'
        DataType = ftFloat
        Size = -1
        Value = Null
      end
      item
        Name = 'cCodigo'
        DataType = ftString
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdTransfEst'
        Size = -1
        Value = Null
      end
      item
        Name = 'iPosicao'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @cModo           CHAR(1)'
      '       ,@nQtde           DECIMAL(12,2)'
      '       ,@cCodigo         VARCHAR(20)'
      '       ,@nCdProdutoLido  int'
      '       ,@nValCusto       DECIMAL(12,4)'
      '       ,@nCdTransfEst    int'
      '       ,@iPosicao        int'
      '       ,@iID             int'
      ''
      'Set @cModo        = :cModo'
      'Set @nQtde        = :nQtde'
      'Set @cCodigo      = :cCodigo'
      'Set @nCdTransfEst = :nCdTransfEst'
      'Set @iPosicao     = :iPosicao'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempNaoLidos'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempNaoLidos (iPosicao int'
      '                               ,cCodigo VARCHAR(20))'
      ''
      'END'
      ''
      'BEGIN TRY'
      #9'SELECT @nCdProdutoLido = nCdProduto'
      #9'  FROM Produto'
      #9' WHERE nCdProduto = Convert(int,@cCodigo)'
      
        '     AND ((nCdGrade IS NOT NULL AND nCdTabTipoProduto = 3) OR (n' +
        'CdGrade IS NULL))'
      'END TRY'
      'BEGIN CATCH'
      '    Set @nCdProdutoLido = NULL'
      'END CATCH'
      ''
      'IF @nCdProdutoLido IS NULL'
      'BEGIN'
      ''
      '    SELECT @nCdProdutoLido = nCdProduto'
      #9'  FROM Produto'
      #9' WHERE cEAN = RTRIM(@cCodigo)'
      
        '     AND ((nCdGrade IS NOT NULL AND nCdTabTipoProduto = 3) OR (n' +
        'CdGrade IS NULL))'
      ''
      'END'
      ''
      'IF @nCdProdutoLido IS NULL'
      'BEGIN'
      ''
      '    SELECT @nCdProdutoLido = nCdProduto'
      #9'  FROM Produto'
      #9' WHERE cEANFornec = RTRIM(@cCodigo)'
      
        '     AND ((nCdGrade IS NOT NULL AND nCdTabTipoProduto = 3) OR (n' +
        'CdGrade IS NULL))'
      '     '
      'END'
      ''
      'IF (@nCdProdutoLido IS NULL)'
      'BEGIN'
      ''
      '    INSERT INTO #TempNaoLidos (iPosicao'
      '                              ,cCodigo)'
      '                        VALUES(@iPosicao'
      '                              ,@cCodigo)'
      ''
      'END'
      ''
      'IF (@nCdProdutoLido IS NOT NULL)'
      'BEGIN'
      ''
      '    IF NOT EXISTS(SELECT 1'
      '                    FROM ItemTransfEst'
      '                   WHERE nCdTransfEst = @nCdTransfEst'
      '                     AND nCdProduto   = @nCdProdutoLido)'
      '    BEGIN'
      ''
      '        SELECT @nValCusto = nValCusto'
      '          FROM Produto'
      '         WHERE nCdProduto = @nCdProdutoLido'
      ''
      '        Set @iID = NULL'
      ''
      '        EXEC usp_ProximoID  '#39'ITEMTRANSFEST'#39
      '                           ,@iID OUTPUT'
      ''
      '        INSERT INTO ItemTransfEst (nCdItemTransfEst'
      '                                  ,nCdTransfEst'
      '                                  ,nCdProduto'
      '                                  ,nValUnitario'
      '                                  ,nValTotal)'
      '                            VALUES(@iID'
      '                                  ,@nCdTransfEst'
      '                                  ,@nCdProdutoLido'
      '                                  ,@nValCusto'
      '                                  ,0)'
      ''
      ''
      '    END'
      ''
      '    IF (@cModo = '#39'D'#39')'
      '    BEGIN'
      ''
      '        UPDATE ItemTransfEst'
      '           SET nQtde        = nQtde + @nQtde'
      '              ,nValTotal    = nValUnitario * (nQtde + @nQtde)'
      '         WHERE nCdProduto   = @nCdProdutoLido'
      '           AND nCdTransfEst = @nCdTransfEst'
      ''
      '    END'
      ''
      '    IF (@cModo = '#39'C'#39')'
      '    BEGIN'
      ''
      '        UPDATE ItemTransfEst'
      '           SET nQtdeConf    = nQtdeConf + @nQtde'
      '         WHERE nCdProduto   = @nCdProdutoLido'
      '           AND nCdTransfEst = @nCdTransfEst'
      ''
      '    END'
      ''
      'END')
    Left = 496
    Top = 408
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempNaoLidos'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempNaoLidos (iPosicao int'
      '                               ,cCodigo VARCHAR(20))'
      ''
      'END'
      ''
      'TRUNCATE TABLE #TempNaoLidos')
    Left = 496
    Top = 440
  end
  object qryTempNaoLidos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempNaoLidos'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempNaoLidos (iPosicao int'
      '                               ,cCodigo VARCHAR(20))'
      ''
      'END'
      ''
      'SELECT *'
      'FROM #TempNaoLidos'
      'ORDER BY iPosicao')
    Left = 368
    Top = 408
    object qryTempNaoLidosiPosicao: TIntegerField
      DisplayLabel = 'Posi'#231#227'o'
      FieldName = 'iPosicao'
    end
    object qryTempNaoLidoscCodigo: TStringField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'cCodigo'
    end
  end
  object dsTempNaoLidos: TDataSource
    DataSet = qryTempNaoLidos
    Left = 368
    Top = 440
  end
  object qryPedidoCD: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT TOP 1 nCdPedido'
      '  FROM ItemTransfEst'
      
        '       INNER JOIN ItemPedido ON ItemPedido.nCdItemTransfEst = It' +
        'emTransfEst.nCdItemTransfEst'
      ' WHERE nCdTransfEst = :nPK')
    Left = 52
    Top = 408
    object qryPedidoCDnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 596
    Top = 421
    object btProtocolo: TMenuItem
      Bitmap.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFF97979764616063605F625F5E615E5E615E5D615E5D615E5D615E5D615E
        5DBAB1A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF999999FFFFFFFEFEFEFEFEFEFE
        FEFEFEFEFEFEFEFEFEFEFEFFFFFF615E5DBAB1A5FFFFFFFFFFFFD5CABCB9B0A4
        B9B0A49B9B9BFFFFFFA5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5FFFFFF615E
        5DB8B0A4B8B0A4D5CABBABA59C6666666464649D9D9DFFFFFFFDFDFDFDFDFDFD
        FDFDFDFDFDFDFDFDFDFDFDFFFFFF615E5D4848484545459D968E6969699B9B9B
        B8B8B88F8F8FE4E4E4949494949494949494949494949494949494E4E4E45B58
        57A5A5A58E918E4343436A6A6AB8B8B8B6B6B67A7A7AC1C1C1C1C1C1C1C1C1C1
        C1C1C1C1C1C1C1C1C1C1C1C1C1C153504FA3A3A335FE354444446C6C6CB8B8B8
        4742410101010202020202020101010101010101010101010101010101010101
        01464241A5A5A54646466D6D6DB8B8B8B8B8B85252526969696565656060605B
        5B5B5656565252524D4D4D484848333333A5A5A5A5A5A54747476E6E6EB8B8B8
        B8B8B85454546B6B6B6666666262625D5D5D5858585353534F4F4F4A4A4A3434
        34A5A5A5A5A5A5484848707070E7E7E7FFFFFFB7B7B7C5C5C5C0C0C0B8B8B8B2
        B2B2ABABABA3A3A39B9B9B9393936F6F6FFFFFFFCCCCCC4A4A4AACA69D6F6F6F
        6C6C6C6969696E6E6E6C6C6C6A6A6A6868686666666666666666666666665353
        535050504E4E4E9E988FFFFFFFFFFFFFFFFFFF9A9A9AF3F3F3EFEFEFEAEAEAE6
        E6E6E1E1E1DFDFDFDFDFDFDFDFDF615E5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9B9B9BF7F7F7F3F3F3EEEEEEEAEAEAE5E5E5E1E1E1DFDFDFDFDFDF615E
        5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9D9D9DFBFBFBF7F7F7F2F2F2EE
        EEEEEAEAEAE5E5E5E1E1E1DFDFDF615E5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF615E
        5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1A1A19F9F9F9C9C9C9A9A9A96
        96969494949191918F8F8F8C8C8C8C8C8CFFFFFFFFFFFFFFFFFF}
      Caption = 'Protocolo'
      SubMenuImages = frmMenu.ImageList1
      ImageIndex = 9
      OnClick = Protocolo1Click
    end
    object EmitirEtiqueta1: TMenuItem
      Bitmap.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D0020000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEDEDB0B0B0B6B6B6F4F4F4FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEEB4
        B4B4E0E0E0D8D8D8B6B6B6F4F4F4FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFEEEEEEB7B7B7E2E2E2FDFDFDFDFDFDD9D9D9B6B6B6F4F4
        F4FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFEFEFEFBBBBBBE4E4E4FD
        FDFDF9F7F3000000FDFBF7D9D9D9B7B7B7F4F4F4FFFFFF000000FFFFFFFFFFFF
        FFFFFFF0F0F0BEBEBEE5E5E5FDFDFDF9F7F3000000F9F7F3000000F9F7F3D9D9
        D9B7B7B7F4F4F4000000FFFFFFFFFFFFF1F1F1C2C2C2E7E7E7FFFFFFFFFFFF00
        0000F9F7F3000000F9F7F3000000FDFCFBD9D9D9B7B7B7000000FFFFFFF2F2F2
        C5C5C5E8E8E8FDFDFDFFCC83FDCD88FFFFFF000000F9F7F3000000F9F7F3F9F7
        F3D7D7D7B5B5B5000000F2F2F2C9C9C9E9E9E9FDFDFDFFCC83FFCC83FFD498FF
        D79EFFFFFF000000F9F7F3FDFDFDD9D9D9B9B9B9F9F9F9000000CDCDCDEBEBEB
        FDFDFDFAFAFAFBF3E7FECE89FFD496FFD59AFFCF8BFFFFFFFDFDFDDADADABDBD
        BDF9F9F9FFFFFF000000CDCDCDFDFDFDFDFDFDFCFCFCF7F7F7FDF5EAFECF8AFF
        CC83FFCC83FDFDFDDCDCDCC0C0C0F9F9F9FFFFFFFFFFFF000000CECECEFDFDFD
        E0E0E0CBCBCBCECECEF7F7F7FBF3E8FFCC83FDFDFDDEDEDEC3C3C3FAFAFAFFFF
        FFFFFFFFFFFFFF000000D0D0D0FDFDFDCDCDCDFFFFFFD4D4D4F3F3F3FBFBFBFD
        FDFDE0E0E0C7C7C7FAFAFAFFFFFFFFFFFFFFFFFFFFFFFF000000D2D2D2FDFDFD
        E2E2E2CECECEE0E0E0FDFDFDFDFDFDE2E2E2CBCBCBFAFAFAFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000D3D3D3FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDE4E4E4CD
        CDCDFAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000D5D5D5D4D4D4
        D2D2D2D1D1D1D0D0D0CECECECDCDCDFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      Caption = 'Emitir Etiqueta'
      OnClick = EmitirEtiqueta1Click
    end
  end
  object qryProdutoSel: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      ''
      'IF (OBJECT_ID('#39'tempdb..#Produtos_SelCampanha'#39') IS NULL)'
      'BEGIN '
      
        '    CREATE TABLE #Produtos_SelCampanha (cFlgAux           int de' +
        'fault 0 not null'
      
        '                                       ,nCdProduto        int PR' +
        'IMARY KEY'
      
        '                                       ,cReferencia       char(1' +
        '5)'
      
        '                                       ,cNmProduto        varcha' +
        'r(150)'
      
        '                                       ,nValCusto         decima' +
        'l(12,2)'
      
        '                                       ,nValVenda         decima' +
        'l(12,2)'
      
        '                                       ,nMarkUp           decima' +
        'l(12,2)'
      
        '                                       ,dDtUltVenda       dateti' +
        'me'
      
        '                                       ,dDtUltReceb       dateti' +
        'me'
      '                                       ,iDiasEstoque      int'
      
        '                                       ,nQtdeEstoque      decima' +
        'l(12,2)'
      
        '                                       ,cNmDepartamento   varcha' +
        'r(50)'
      
        '                                       ,cNmCategoria      varcha' +
        'r(50)'
      
        '                                       ,cNmSubCategoria   varcha' +
        'r(50)'
      
        '                                       ,cNmSegmento       varcha' +
        'r(50)'
      
        '                                       ,cNmLinha          varcha' +
        'r(50)'
      
        '                                       ,cNmMarca          varcha' +
        'r(50)'
      
        '                                       ,cNmGrupoProduto   varcha' +
        'r(50)'
      
        '                                       ,cNmClasseProduto  varcha' +
        'r(50)'
      
        '                                       ,cNmStatus         varcha' +
        'r(50))'
      ''
      'END'
      ''
      'SELECT * '
      '  FROM #Produtos_SelCampanha'
      ' WHERE cFlgAux = 1')
    Left = 48
    Top = 440
    object qryProdutoSelcFlgAux: TIntegerField
      FieldName = 'cFlgAux'
    end
    object qryProdutoSelnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutoSelcReferencia: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object qryProdutoSelcNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutoSelnValCusto: TBCDField
      FieldName = 'nValCusto'
      Precision = 12
      Size = 2
    end
    object qryProdutoSelnValVenda: TBCDField
      FieldName = 'nValVenda'
      Precision = 12
      Size = 2
    end
    object qryProdutoSelnMarkUp: TBCDField
      FieldName = 'nMarkUp'
      Precision = 12
      Size = 2
    end
    object qryProdutoSeldDtUltVenda: TDateTimeField
      FieldName = 'dDtUltVenda'
    end
    object qryProdutoSeldDtUltReceb: TDateTimeField
      FieldName = 'dDtUltReceb'
    end
    object qryProdutoSeliDiasEstoque: TIntegerField
      FieldName = 'iDiasEstoque'
    end
    object qryProdutoSelnQtdeEstoque: TBCDField
      FieldName = 'nQtdeEstoque'
      Precision = 12
      Size = 2
    end
    object qryProdutoSelcNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object qryProdutoSelcNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
    object qryProdutoSelcNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object qryProdutoSelcNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
    object qryProdutoSelcNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
    object qryProdutoSelcNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
    object qryProdutoSelcNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
    object qryProdutoSelcNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
    object qryProdutoSelcNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object qryIncluiProdutos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTransfEst'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdItemTransfEst      int'
      '       ,@nCdTransfEst          int'
      '       ,@nCdProduto            int'
      '       ,@nValVenda             decimal(12,2)'
      ''
      'SET @nCdTransfEst = :nCdTransfEst'
      ''
      'DECLARE curItens CURSOR'
      ' FOR SELECT nCdProduto'
      '           ,nValVenda'
      '       FROM #Produtos_SelCampanha'
      '      WHERE cFlgAux = 1'
      '        AND NOT EXISTS (SELECT TOP 1 1'
      '                          FROM ItemTransfEst'
      
        '                         WHERE ItemTransfEst.nCdTransfEst = @nCd' +
        'TransfEst'
      
        '                           AND ItemTransfEst.nCdProduto   = #Pro' +
        'dutos_SelCampanha.nCdProduto)'
      ''
      'OPEN curItens'
      ''
      'FETCH NEXT'
      ' FROM curItens'
      ' INTO @nCdProduto'
      '     ,@nValVenda'
      '     '
      'WHILE(@@FETCH_STATUS = 0)'
      'BEGIN'
      '                                     '
      '    EXEC usp_ProximoID '#39'ITEMTRANSFEST'#39
      '                       ,@nCdItemTransfEst OUTPUT'
      ''
      '   INSERT INTO ItemTransfEst(nCdItemTransfEst'
      '                            ,nCdProduto'
      '                            ,nQtde'
      '                            ,nValUnitario'
      '                            ,nValTotal'
      '                            ,nCdTransfEst'
      '                            ,nQtdeConf)'
      '                     VALUES (@nCdItemTransfEst'
      #9#9#9#9#9#9#9',@nCdProduto'
      '                            ,1'
      '                            ,@nValVenda'
      '                            ,@nValVenda'
      '                            ,@nCdTransfEst'
      '                            ,0)'
      '  '
      '    FETCH NEXT'
      '     FROM curItens'
      '     INTO @nCdProduto'
      '         ,@nValVenda'
      'END'
      ''
      'CLOSE curItens'
      'DEALLOCATE curItens'
      ''
      'TRUNCATE TABLE #Produtos_SelCampanha')
    Left = 596
    Top = 451
  end
  object qryPopulaConfPadrao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT DISTINCT'
      '       Produto.nCdProduto'
      '      ,Produto.cNmProduto'
      '      ,Produto.cEAN'
      '      ,Produto.cEANFornec'
      '  FROM ItemTransfEst'
      
        '       INNER JOIN Produto ON Produto.nCdProduto = ItemTransfEst.' +
        'nCdProduto'
      ' WHERE nCdTransfEst = :nPK'
      '')
    Left = 120
    Top = 496
    object qryPopulaConfPadraonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryPopulaConfPadraocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryPopulaConfPadraocEAN: TStringField
      FieldName = 'cEAN'
      FixedChar = True
    end
    object qryPopulaConfPadraocEANFornec: TStringField
      FieldName = 'cEANFornec'
      FixedChar = True
    end
  end
  object qryGravaConfPadrao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTransfEst'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cFlgModo'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdTransfEst     int'
      '       ,@cFlgModo         char(1)'
      '       ,@nCdItemTransfEst int'
      '       ,@nCdProduto       int'
      '       ,@nQtdeCont        int'
      '       ,@nQtdeAtual       int'
      '       ,@nQtdeCalc        int'
      '       ,@nValCusto        decimal(12,2)'
      ''
      'SET @nCdTransfEst = :nCdTransfEst'
      'SET @cFlgModo     = :cFlgModo'
      ''
      'IF (@cFlgModo NOT IN('#39'D'#39','#39'C'#39'))'
      'BEGIN'
      '    RAISERROR('#39'Modo de utiliza'#231#227'o inv'#225'lido.'#39',16,1)'
      '    RETURN    '
      'END'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempProdutosBip'#39') IS NULL) '
      'BEGIN'
      ''
      '    CREATE TABLE #TempProdutosBip (cCdProduto  varchar(20)'
      '                                  ,nQtdeBipada int)'
      '                         '
      'END'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempProdutosCont'#39') IS NULL) '
      'BEGIN'
      ''
      '    CREATE TABLE #TempProdutosCont (cCdProduto varchar(20)'
      '                                   ,nQtdeCont  int)'
      '                         '
      'END'
      ''
      'TRUNCATE TABLE #TempProdutosCont'
      ''
      '--'
      '-- contabiliza produtos bipados'
      '--'
      'INSERT INTO #TempProdutosCont (cCdProduto'
      '                              ,nQtdeCont)'
      '                       SELECT cCdProduto'
      '                             ,SUM(nQtdeBipada)'
      '                         FROM #TempProdutosBip'
      '                        GROUP BY cCdProduto'
      '                        ORDER BY cCdProduto'
      ''
      '--'
      '-- digita'#231#227'o/leitura'
      '--'
      'IF (@cFlgModo = '#39'D'#39')'
      'BEGIN'
      '    '
      '    --'
      #9'-- registra digita'#231#227'o de produtos'
      #9'--'
      #9'DECLARE curDigitTransf CURSOR'
      #9#9'FOR SELECT CAST(cCdProduto as int)'
      #9#9'          ,nQtdeCont'
      #9#9#9'  FROM #TempProdutosCont'
      #9#9#9' ORDER BY cCdProduto'
      ''
      #9'OPEN curDigitTransf'
      ''
      #9'FETCH NEXT'
      #9' FROM curDigitTransf'
      #9' INTO @nCdProduto'
      #9#9' ,@nQtdeCont'
      ''
      #9'WHILE (@@FETCH_STATUS = 0)'
      #9'BEGIN'
      ''
      #9#9'--'
      #9#9'-- verifica se produto j'#225' existe na transfer'#234'ncia'
      #9#9'--'
      #9#9'IF NOT EXISTS(SELECT 1'
      '                        FROM ItemTransfEst'
      '                       WHERE nCdTransfEst = @nCdTransfEst'
      '                         AND nCdProduto   = @nCdProduto)'
      '        BEGIN'
      ''
      '            SELECT @nValCusto = nValCusto'
      '              FROM Produto'
      '             WHERE nCdProduto = @nCdProduto'
      ''
      '            SET @nCdItemTransfEst = NULL'
      ''
      '            EXEC usp_ProximoID '#39'ITEMTRANSFEST'#39
      '                              ,@nCdItemTransfEst OUTPUT'
      ''
      '            INSERT INTO ItemTransfEst (nCdItemTransfEst'
      '                                      ,nCdTransfEst'
      '                                      ,nCdProduto'
      '                                      ,nValUnitario'
      '                                      ,nValTotal)'
      '                               VALUES(@nCdItemTransfEst'
      '                                     ,@nCdTransfEst'
      '                                     ,@nCdProduto'
      '                                     ,@nValCusto'
      '                                     ,0.00)'
      '                                     '
      '        END'
      '        '
      '        --'
      '        -- contabiliza item da transfer'#234'ncia'
      '        --'
      '        UPDATE ItemTransfEst'
      '           SET nQtde        = nQtde + @nQtdeCont'
      
        '              ,nValTotal    = nValUnitario * (nQtde + @nQtdeCont' +
        ')'
      '         WHERE nCdProduto   = @nCdProduto'
      '           AND nCdTransfEst = @nCdTransfEst'
      '           '
      #9'    FETCH NEXT'
      #9'     FROM curDigitTransf'
      #9'     INTO @nCdProduto'
      '             ,@nQtdeCont'
      #9'         '
      '    END'
      ''
      #9'CLOSE curDigitTransf'
      #9'DEALLOCATE curDigitTransf'
      #9'RETURN'
      ''
      'END'
      ''
      '--'
      '-- confer'#234'ncia'
      '--'
      'IF (@cFlgModo = '#39'C'#39')'
      'BEGIN'
      ''
      #9'--'
      #9'-- reseta saldo conferido'
      #9'--'
      #9'UPDATE ItemTransfEst '
      #9'   SET nQtdeConf = 0 '
      #9' WHERE nCdTransfEst = @nCdTransfEst'
      ''
      #9'--'
      #9'-- registra confer'#234'ncia de produtos'
      #9'--'
      #9'DECLARE curConfereTransf CURSOR'
      #9#9'FOR SELECT nCdItemTransfEst'
      #9#9#9#9'  ,nCdProduto'
      #9#9#9#9'  ,nQtde'
      #9#9#9'  FROM ItemTransfEst'
      #9#9#9' WHERE nCdTransfEst = @nCdTransfEst'
      #9#9#9' ORDER BY nCdProduto'
      ''
      #9'OPEN curConfereTransf'
      ''
      #9'FETCH NEXT'
      #9' FROM curConfereTransf'
      #9' INTO @nCdItemTransfEst'
      #9#9' ,@nCdProduto'
      #9#9' ,@nQtdeAtual'
      ''
      #9'WHILE (@@FETCH_STATUS = 0)'
      #9'BEGIN'
      ''
      #9#9'--'
      #9#9'-- consulta saldo contabilizado'
      #9#9'--'
      #9#9'SET @nQtdeCont    = 0'
      #9#9'SELECT @nQtdeCont = nQtdeCont'
      #9#9'  FROM #TempProdutosCont'
      #9#9' WHERE CAST(cCdProduto as int) = @nCdProduto'
      #9'     '
      #9#9'--'
      #9#9'-- se produto possuir saldo, atualiza confer'#234'ncia'
      #9#9'--'
      #9#9'IF (ISNULL(@nQtdeCont,0) > 0)'
      #9#9'BEGIN'
      ''
      
        #9#9#9'SET @nQtdeCalc = (CASE WHEN @nQtdeCont > @nQtdeAtual AND (SEL' +
        'ECT COUNT(1)'
      
        '                                                                ' +
        '        FROM ItemTransfEst'
      
        '                                                                ' +
        '       WHERE nCdTransfEst = @nCdTransfEst'
      
        '                                                                ' +
        '         AND nCdProduto   = @nCdProduto'
      
        '                                                                ' +
        '         AND nQtdeConf    = 0) > 1'
      #9#9#9#9#9#9#9' '#9'   THEN @nQtdeAtual'
      #9#9#9#9#9#9#9#9'   ELSE @nQtdeCont'
      #9#9#9#9#9#9#9'  END)'
      #9'    '
      #9#9#9'--'
      #9#9#9'-- atualiza saldo conferido do item'
      #9#9#9'--'
      #9#9#9'UPDATE ItemTransfEst'
      #9#9#9'   SET nQtdeConf = @nQtdeCalc'
      #9#9#9' WHERE nCdItemTransfEst = @nCdItemTransfEst'
      #9'         '
      #9#9#9'--'
      #9#9#9'-- atualiza saldo utilizado na confer'#234'ncia acima'
      #9#9#9'--'
      #9#9#9'UPDATE #TempProdutosCont'
      #9#9#9'   SET nQtdeCont = nQtdeCont - @nQtdeCalc'
      #9#9#9' WHERE cCdProduto = CAST(@nCdProduto as varchar)'
      #9'    '
      #9#9'END'
      #9'    '
      #9#9'FETCH NEXT'
      #9#9' FROM curConfereTransf'
      #9#9' INTO @nCdItemTransfEst'
      #9#9#9' ,@nCdProduto'
      #9#9#9' ,@nQtdeAtual'
      #9'         '
      #9' END'
      ''
      #9'CLOSE curConfereTransf'
      #9'DEALLOCATE curConfereTransf'
      #9'RETURN'
      ''
      'END')
    Left = 152
    Top = 496
  end
end
