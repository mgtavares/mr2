unit fConsNecessidadeCompra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, StdCtrls, DB, ADODB;

type
  TfrmConsNecessidadeCompra = class(TfrmProcesso_Padrao)
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    RadioGroup1: TRadioGroup;
    DBGridEh1: TDBGridEh;
    qryTemp: TADOQuery;
    dsTemp: TDataSource;
    qryTempcFlgAux: TIntegerField;
    qryTempnCdProduto: TIntegerField;
    qryTempcCdProduto: TStringField;
    qryTempcNmProduto: TStringField;
    qryTempcUnidadeMedida: TStringField;
    qryTempnQtdeCompraSug: TBCDField;
    qryTempnQtdeCompraSeg: TBCDField;
    qryTempnQtdeCompraMin: TBCDField;
    qryTempnQtdeRequis: TBCDField;
    qryTempnQtdeEstoqueSeg: TBCDField;
    qryTempnQtdeEstoqueMin: TBCDField;
    qryTempnQtdeEstoqueMax: TBCDField;
    qryTempnQtdeEstoque: TBCDField;
    SP_SUGERIR_ITEM_MAPA: TADOStoredProc;
    qryTempcFlgManual: TIntegerField;
    qryTempnCdRequisicao: TIntegerField;
    qryTempnCdItemRequisicao: TIntegerField;
    ToolButton5: TToolButton;
    ToolButton7: TToolButton;
    qryTempnQtdePedido: TBCDField;
    qryTempnQtdeCotacao: TBCDField;
    procedure ToolButton6Click(Sender: TObject);
    procedure qryTempBeforeDelete(DataSet: TDataSet);
    procedure qryTempBeforePost(DataSet: TDataSet);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsNecessidadeCompra: TfrmConsNecessidadeCompra;

implementation

uses fMenu, fMapaConcorrencia_Fornecedor, fPedidoAbertoProduto;

{$R *.dfm}

procedure TfrmConsNecessidadeCompra.ToolButton6Click(Sender: TObject);
begin
  inherited;

  if (RadioGroup1.ItemIndex < 2) then
  begin
      SP_SUGERIR_ITEM_MAPA.Close ;
      SP_SUGERIR_ITEM_MAPA.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
      SP_SUGERIR_ITEM_MAPA.ExecProc;
  end ;

  qryTemp.Close ;
  qryTemp.Parameters.ParamByName('cTipoSelecao').Value := RadioGroup1.ItemIndex;
  qryTemp.Open ;

  if (qryTemp.eof) then
  begin
      MensagemAlerta('Nenhum item para exibi��o.') ;
  end ;

end;

procedure TfrmConsNecessidadeCompra.qryTempBeforeDelete(DataSet: TDataSet);
begin
  inherited;

  if (qryTempcFlgManual.Value = 0) then
  begin
      MensagemAlerta('N�o � poss�vel excluir item autom�tico.') ;
      abort ;
  end ;

end;

procedure TfrmConsNecessidadeCompra.qryTempBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryTempnQtdeCompraSug.Value < qryTempnQtdeCompraMin.Value) then
  begin
      MensagemAlerta('A quantidade n�o pode ser menor que a quantidade m�nima a comprar.') ;
      abort ;
  end ;

  if (qryTempnQtdeCompraSug.Value < 0) then
  begin
      MensagemAlerta('Quantidade inv�lida.') ;
      abort ;
  end ;

end;

procedure TfrmConsNecessidadeCompra.ToolButton5Click(Sender: TObject);
begin
  inherited;

  if not qryTemp.Active then
      qryTemp.Open ;

  DbGridEh1.SetFocus ;
  
  qryTemp.Last ;
  qryTemp.Insert ;

end;

procedure TfrmConsNecessidadeCompra.ToolButton1Click(Sender: TObject);
var
    iItens : integer ;
begin
  inherited;

  if not qryTemp.Active then
      MensagemAlerta('Nenhum item dispon�vel para compra.') ;

  if (qryTemp.State <> dsBrowse) then
      qryTemp.Post ;

  qryTemp.First ;

  While not qryTemp.eof do
  begin

      if (qryTempcFlgAux.Value = 1) then
          iItens := iItens + 1 ;

      qryTemp.Next ;

  end ;

  qryTemp.First ;

  if (iItens = 0) then
  begin
      MensagemAlerta('Nenhum item selecionado.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a gera��o de um novo mapa de concorr�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMapaConcorrencia_Fornecedor.LimpaDados;
  frmMapaConcorrencia_Fornecedor.ShowModal() ;

  ToolButton6.Click;

end;

procedure TfrmConsNecessidadeCompra.ToolButton7Click(Sender: TObject);
begin
  inherited;

  frmPedidoAbertoProduto.qryMaster.Close ;
  frmPedidoAbertoProduto.qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  frmPedidoAbertoProduto.qryMaster.Parameters.ParamByName('nCdProduto').Value := qryTempnCdProduto.Value ;
  frmPedidoAbertoProduto.qryMaster.Open ;

  if not frmPedidoAbertoProduto.qryMaster.Eof then
      frmPedidoAbertoProduto.ShowModal;
  
end;

initialization
    RegisterClass(TfrmConsNecessidadeCompra) ;

end.
