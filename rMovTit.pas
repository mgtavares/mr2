unit rMovTit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls;

type
  TrptMovTit = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitnCdGrupoEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitcFlgPrev: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    Label2: TLabel;
    DataSource3: TDataSource;
    DBEdit7: TDBEdit;
    Label5: TLabel;
    DataSource4: TDataSource;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DataSource5: TDataSource;
    Label3: TLabel;
    edtDtMovIni: TMaskEdit;
    Label6: TLabel;
    edtDtMovFim: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit5: TMaskEdit;
    MaskEdit6: TMaskEdit;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    MaskEdit4: TMaskEdit;
    Label4: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    MaskEdit7: TMaskEdit;
    DBEdit1: TDBEdit;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    Label8: TLabel;
    qryOperacao: TADOQuery;
    qryOperacaonCdOperacao: TIntegerField;
    qryOperacaocNmOperacao: TStringField;
    qryOperacaocTipoOper: TStringField;
    qryOperacaocSinalOper: TStringField;
    qryOperacaonCdPlanoConta: TIntegerField;
    qryOperacaonCdServidorOrigem: TIntegerField;
    qryOperacaodDtReplicacao: TDateTimeField;
    DBEdit4: TDBEdit;
    DataSource6: TDataSource;
    MaskEdit8: TMaskEdit;
    Label10: TLabel;
    edtDtEmiIni: TMaskEdit;
    Label11: TLabel;
    edtDtEmiFim: TMaskEdit;
    Label12: TLabel;
    edtDtVencIni: TMaskEdit;
    Label13: TLabel;
    edtDtVencFim: TMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8Exit(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptMovTit: TrptMovTit;

implementation

uses fMenu, rImpSP, fLookup_Padrao, rMovTit_view;

{$R *.dfm}

procedure TrptMovTit.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  iAux := frmMenu.nCdEmpresaAtiva;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  if (iAux > 0) then
  begin

    MaskEdit3.Text := IntToStr(iAux) ;

    qryEmpresa.Close ;
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := iAux ;
    qryEmpresa.Open ;

  end ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      MaskEdit7.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLoja, MaskEdit7.text) ;
  end
  else
  begin
      MasKEdit7.ReadOnly := True ;
      MasKEdit7.Color    := $00E9E4E4 ;
  end ;

  MaskEdit3.SetFocus ;

end;


procedure TrptMovTit.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TrptMovTit.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryEspTit.Close ;
  If (Trim(MaskEdit5.Text) <> '') then
  begin
    qryEspTit.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEspTit.Parameters.ParamByName('nCdEspTit').Value := MaskEdit5.Text ;
    qryEspTit.Open ;
  end ;

end;

procedure TrptMovTit.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TrptMovTit.ToolButton1Click(Sender: TObject);
var
  objRel : TrptMovTit_view ;
begin

  objRel := TrptMovTit_view.Create(Self) ;

  qryEmpresa.Close ;
  If (Trim(MaskEdit3.Text) <> '') then
  begin
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;
  if qryEmpresa.Eof then MaskEdit3.Text := '';

  qryLoja.Close ;
  If (Trim(MaskEdit7.Text) <> '') then
  begin
    qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryLoja.Parameters.ParamByName('nPK').Value := MaskEdit7.Text ;
    qryLoja.Open ;
  end ;
  if qryLoja.Eof then MaskEdit7.Text := '';

  qryEspTit.Close ;
  If (Trim(MaskEdit5.Text) <> '') then
  begin
    qryEspTit.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEspTit.Parameters.ParamByName('nCdEspTit').Value := MaskEdit5.Text ;
    qryEspTit.Open ;
  end ;
  if qryEspTit.Eof then MaskEdit5.Text := '';

  objRel.usp_Relatorio.Close ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEspTit').Value        := frmMenu.ConvInteiro(MaskEdit5.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdTerceiro').Value      := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdContaBancaria').Value := 0 ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtInicial').Value       := frmMenu.ConvData(edtDtMovIni.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtFinal').Value         := frmMenu.ConvData(edtDtMovFim.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@iNrCheque').Value        := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdOperacao').Value      := frmMenu.ConvInteiro(MaskEdit8.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtEmissaoIni').Value    := frmMenu.ConvData(edtDtEmiIni.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtEmissaoFim').Value    := frmMenu.ConvData(edtDtEmiFim.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtVenctoIni').Value     := frmMenu.ConvData(edtDtVencIni.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtVenctoFim').Value     := frmMenu.ConvData(edtDtVencFim.Text) ;

  if (RadioGroup1.ItemIndex = 0) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cTipoOper').Value := 'T' ;

  if (RadioGroup1.ItemIndex = 1) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cTipoOper').Value := 'P' ;

  if (RadioGroup1.ItemIndex = 2) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cTipoOper').Value := 'J' ;

  if (RadioGroup1.ItemIndex = 3) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cTipoOper').Value := 'D' ;

  if (RadioGroup1.ItemIndex = 4) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cTipoOper').Value := 'A' ;

  if (RadioGroup2.ItemIndex = 0) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cSenso').Value := 'D' ;

  if (RadioGroup2.ItemIndex = 1) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cSenso').Value := 'C' ;

  if (RadioGroup2.ItemIndex = 0) or (RadioGroup2.ItemIndex = 2) then
  begin
      MaskEdit4.Text := '0' ;
  end ;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value  := 0
  else objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value := frmMenu.ConvInteiro(MaskEdit7.Text) ;

  objRel.usp_Relatorio.Open  ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  objRel.lblFiltro1.Caption := '';

  if (Trim(MaskEdit3.Text) <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + 'Empresa : ' + Trim(MaskEdit3.Text) + '-' + DbEdit3.Text ;

  if (Trim(MaskEdit5.Text) <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Esp�cie : ' + Trim(MaskEdit5.Text) + '-' + DbEdit7.Text ;

  if (Trim(MaskEdit6.Text) <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Terceiro: ' + Trim(MaskEdit6.Text) + '-' + DbEdit10.Text ;

  if (Trim(MaskEdit8.Text) <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Opera��o: ' + Trim(MaskEdit8.Text) + '-' + DbEdit4.Text ;

  if ((Trim(edtDtMovIni.Text) <> '/  /') or (Trim(edtDtMovFim.Text) <> '/  /')) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo Movto: ' + Trim(edtDtMovIni.Text) + '-' + String(edtDtMovFim.Text) ;

  if ((Trim(edtDtEmiIni.Text) <> '/  /') or (Trim(edtDtEmiFim.Text) <> '/  /')) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo Emiss�o: ' + Trim(edtDtEmiIni.Text) + '-' + String(edtDtEmiFim.Text) ;

  if ((Trim(edtDtVencIni.Text) <> '/  /') or (Trim(edtDtVencFim.Text) <> '/  /')) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo Vencto: ' + Trim(edtDtVencIni.Text) + '-' + String(edtDtVencFim.Text) ;

  if (RadioGroup1.ItemIndex = 0) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo Opera��o: TODOS MOVIMENTOS' ;

  if (RadioGroup1.ItemIndex = 1) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo Opera��o: PAGAMENTO/RECEBIMENTO' ;

  if (RadioGroup1.ItemIndex = 2) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo Opera��o: JURO' ;

  if (RadioGroup1.ItemIndex = 3) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo Opera��o: DESCONTO' ;

  if (RadioGroup1.ItemIndex = 4) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo Opera��o: ABATIMENTO' ;

  if (RadioGroup2.ItemIndex = 0) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / T�tulo  : A PAGAR' ;

  if (RadioGroup2.ItemIndex = 1) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + '/ T�tulo  : A RECEBER' ;

  if ((frmMenu.LeParametro('VAREJO') = 'S') AND ((Trim(MaskEdit7.Text)) <> '')) then
     objRel.lblFiltro1.Caption :=  objRel.lblFiltro1.Caption + ' / Loja : ' + Trim(MaskEdit7.Text) + '-' + DbEdit1.Text;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;


procedure TrptMovTit.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptMovTit.MaskEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptMovTit.MaskEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK        : Integer ;
    TipoAcesso : String;
begin
  inherited;

  case key of
    vk_F4 : begin

        TipoAcesso := #39 + 'L' + #39;

        nPK := frmLookup_Padrao.ExecutaConsulta2(10,'Exists(SELECT 1 FROM UsuarioEspTit WHERE UsuarioEspTit.nCdEspTit = EspTit.nCdEspTit AND UsuarioEspTit.nCdUsuario = '+ IntToStr(frmMenu.nCdUsuarioLogado) +' AND UsuarioEspTit.cFlgTipoAcesso = ' + TipoAcesso + ')');

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;


end;

procedure TrptMovTit.MaskEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptMovTit.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  If (Trim(MaskEdit7.Text) <> '') then
  begin
    qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryLoja.Parameters.ParamByName('nPK').Value := MaskEdit7.Text ;
    qryLoja.Open ;
  end ;

end;

procedure TrptMovTit.FormActivate(Sender: TObject);
begin
  inherited;
  if (frmMenu.LeParametro('VAREJO') = 'N') then
      MaskEdit7.Enabled := False ;

end;

procedure TrptMovTit.MaskEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(30);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptMovTit.MaskEdit8Exit(Sender: TObject);
begin
  inherited;
  qryOperacao.Close ;

  If (Trim(MaskEdit8.Text) <> '') then
  begin

    qryOperacao.Parameters.ParamByName('nPk').Value := MaskEdit8.Text ;
    qryOperacao.Open ;
    if qryOperacao.Eof then MaskEdit8.Clear;
  end;

  if (Trim(MaskEdit8.Text) <> '') then

    begin
       RadioGroup1.ItemIndex := 0 ;
       RadioGroup1.Controls[1].Enabled := False;
       RadioGroup1.Controls[2].Enabled := False;
       RadioGroup1.Controls[3].Enabled := False;
       RadioGroup1.Controls[4].Enabled := False;

       ToolBar1.Enabled := True ;
 
    end
    else
    begin
       RadioGroup1.ItemIndex := 0 ;
       RadioGroup1.Controls[1].Enabled := True;
       RadioGroup1.Controls[2].Enabled := True;
       RadioGroup1.Controls[3].Enabled := True;
       RadioGroup1.Controls[4].Enabled := True;
  end;


end;

initialization
     RegisterClass(TrptMovTit) ;

end.
