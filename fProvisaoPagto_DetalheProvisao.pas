unit fProvisaoPagto_DetalheProvisao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping, ToolCtrlsEh, Menus;

type
  TfrmProvisaoPagto_DetalheProvisao = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryTitulos: TADOQuery;
    dsTitulos: TDataSource;
    qryTitulosnCdTitulo: TIntegerField;
    qryTituloscNrTit: TStringField;
    qryTitulosnCdSP: TIntegerField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTituloscNmTerceiro: TStringField;
    qryTitulosnSaldoTit: TBCDField;
    qryTituloscNrNF: TStringField;
    qryTituloscNmEspTit: TStringField;
    PopupMenu1: TPopupMenu;
    ExcluirItemProviso1: TMenuItem;
    qryTitulosnCdProvisaoTit: TIntegerField;
    qryConfereBaixaProvisao: TADOQuery;
    qryConfereBaixaProvisaocFlgBaixado: TIntegerField;
    qryAux: TADOQuery;
    procedure ExcluirItemProviso1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProvisaoPagto_DetalheProvisao: TfrmProvisaoPagto_DetalheProvisao;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmProvisaoPagto_DetalheProvisao.ExcluirItemProviso1Click(
  Sender: TObject);
begin
  inherited;


    if (qryTitulos.eof) then
    begin
        MensagemAlerta('Selecione um t�tulo.') ;
        exit ;
    end ;

    case MessageDlg('Esta a��o ir� reabrir o t�tulo selecionado, removendo-o desta provis�o. Continuar ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
    end ;

    qryConfereBaixaProvisao.Close;
    PosicionaQuery(qryConfereBaixaProvisao, qryTitulosnCdProvisaoTit.asString) ;

    if not qryConfereBaixaProvisao.eof then
        if (qryConfereBaixaProvisaocFlgBaixado.Value = 1) then
        begin
            MensagemErro('Esta provis�o de pagamento j� foi baixada, por favor verifique. C�digo da Provis�o: ' + qryTitulosnCdProvisaoTit.asString) ;
            qryTitulos.Requery();
            abort ;
        end ;

    frmMenu.Connection.BeginTrans;

    try
        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('UPDATE Titulo SET nCdProvisaoTit = NULL WHERE nCdTitulo = ' + qryTitulosnCdTitulo.AsString) ;
        qryAux.ExecSQL;

        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('UPDATE ProvisaoTit SET nValProvisao = nValProvisao - ' + StringReplace(qryTitulosnSaldoTit.AsString,',','.',[rfReplaceAll, rfIgnoreCase]) + ' WHERE nCdProvisaoTit = ' + qryTitulosnCdProvisaoTit.AsString) ;
        qryAux.ExecSQL;
    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento.');
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    ShowMessage('T�tulo removido da Provis�o com sucesso.') ;

    PosicionaQuery(qryTitulos, qryTitulosnCdProvisaoTit.AsString) ;
end;

end.
