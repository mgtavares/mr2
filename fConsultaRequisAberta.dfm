inherited frmConsultaRequisAberta: TfrmConsultaRequisAberta
  Left = 242
  Top = 200
  Caption = 'Consulta de Requisi'#231#245'es em Aberto'
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    ButtonWidth = 93
    inherited ToolButton1: TToolButton
      Caption = 'Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 93
    end
    inherited ToolButton2: TToolButton
      Left = 101
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    Align = alClient
    DataSource = dsRequisicao
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdRequisicao'
        Footers = <>
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'nCdLoja'
        Footers = <>
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoRequisicao'
        Footers = <>
        Width = 260
      end
      item
        EditButtons = <>
        FieldName = 'dDtRequisicao'
        Footers = <>
        Width = 139
      end
      item
        EditButtons = <>
        FieldName = 'cNmSetor'
        Footers = <>
        Width = 143
      end
      item
        EditButtons = <>
        FieldName = 'cNmSolicitante'
        Footers = <>
        Width = 168
      end
      item
        EditButtons = <>
        FieldName = 'dDtAutor'
        Footers = <>
        Width = 134
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuarioAutor'
        Footers = <>
        Width = 116
      end
      item
        EditButtons = <>
        FieldName = 'cNmTabStatusRequis'
        Footers = <>
        Width = 134
      end>
  end
  inherited ImageList1: TImageList
    Left = 352
    Top = 96
  end
  object dsRequisicao: TDataSource
    DataSet = qryRequisicao
    Left = 168
    Top = 96
  end
  object qryRequisicao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Requisicao.nCdRequisicao'
      '      ,Requisicao.nCdTipoRequisicao'
      '      ,Requisicao.nCdLoja'
      '      ,cNmTipoRequisicao'
      '      ,Requisicao.dDtRequisicao'
      '      ,cNmSetor'
      '      ,Requisicao.cNmSolicitante'
      '      ,Requisicao.dDtAutor'
      '      ,cNmUsuario          cNmUsuarioAutor'
      '      ,cNmTabStatusRequis'
      '  FROM Requisicao'
      
        '       INNER JOIN TipoRequisicao  ON TipoRequisicao.nCdTipoRequi' +
        'sicao = Requisicao.nCdTipoRequisicao'
      
        '       INNER JOIN Setor           ON Setor.nCdSetor             ' +
        '      = Requisicao.nCdSetor'
      
        '       INNER JOIN Usuario         ON Usuario.nCdUsuario         ' +
        '      = Requisicao.nCdUsuarioAutor'
      
        '       INNER JOIN TabStatusRequis ON TabStatusRequis.nCdTabStatu' +
        'sRequis = Requisicao.nCdTabStatusRequis'
      ' WHERE Requisicao.nCdEmpresa          = :nPK'
      '   AND Requisicao.nCdTabStatusRequis IN (2,3)'
      'AND TipoRequisicao.nCdTabTipoRequis =1'
      ' ORDER BY dDtAutor ')
    Left = 240
    Top = 88
    object qryRequisicaonCdRequisicao: TIntegerField
      DisplayLabel = 'Requisi'#231#227'o|N'#250'mero'
      FieldName = 'nCdRequisicao'
    end
    object qryRequisicaocNmTipoRequisicao: TStringField
      DisplayLabel = 'Requisi'#231#227'o|Tipo'
      FieldName = 'cNmTipoRequisicao'
      Size = 50
    end
    object qryRequisicaodDtRequisicao: TDateTimeField
      DisplayLabel = 'Requisi'#231#227'o|Data Req.'
      FieldName = 'dDtRequisicao'
    end
    object qryRequisicaocNmSetor: TStringField
      DisplayLabel = 'Solicitante|Setor'
      FieldName = 'cNmSetor'
      Size = 50
    end
    object qryRequisicaocNmSolicitante: TStringField
      DisplayLabel = 'Solicitante|Usu'#225'rio'
      FieldName = 'cNmSolicitante'
      Size = 30
    end
    object qryRequisicaodDtAutor: TDateTimeField
      DisplayLabel = 'Autoriza'#231#227'o|Data'
      FieldName = 'dDtAutor'
    end
    object qryRequisicaocNmUsuarioAutor: TStringField
      DisplayLabel = 'Autoriza'#231#227'o|Usu'#225'rio'
      FieldName = 'cNmUsuarioAutor'
      Size = 50
    end
    object qryRequisicaocNmTabStatusRequis: TStringField
      DisplayLabel = 'Status|Requisi'#231#227'o'
      FieldName = 'cNmTabStatusRequis'
      Size = 50
    end
    object qryRequisicaonCdTipoRequisicao: TIntegerField
      FieldName = 'nCdTipoRequisicao'
    end
    object qryRequisicaonCdLoja: TIntegerField
      DisplayLabel = 'Requisi'#231#227'o|Loja'
      FieldName = 'nCdLoja'
    end
  end
end
