inherited frmManutTransacaoCartao: TfrmManutTransacaoCartao
  Left = 64
  Width = 1304
  Caption = 'frmManutTransacaoCartao'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 126
    Width = 1296
    Height = 347
  end
  inherited ToolBar1: TToolBar
    Width = 1296
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1296
    Height = 97
    Align = alTop
    Caption = ' Filtro '
    TabOrder = 1
    object Label5: TLabel
      Tag = 1
      Left = 73
      Top = 24
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
    end
    object Label1: TLabel
      Tag = 1
      Left = 9
      Top = 48
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Grupo Operadora'
    end
    object Label2: TLabel
      Tag = 1
      Left = 41
      Top = 72
      Width = 52
      Height = 13
      Alignment = taRightJustify
      Caption = 'Operadora'
    end
    object Label4: TLabel
      Tag = 1
      Left = 485
      Top = 23
      Width = 104
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo de Transa'#231#227'o'
    end
    object Label6: TLabel
      Tag = 1
      Left = 554
      Top = 45
      Width = 35
      Height = 13
      Alignment = taRightJustify
      Caption = 'Parcela'
    end
    object Label8: TLabel
      Tag = 1
      Left = 621
      Top = 45
      Width = 56
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Docto'
    end
    object edtLoja: TMaskEdit
      Left = 96
      Top = 16
      Width = 60
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = edtLojaExit
      OnKeyDown = edtLojaKeyDown
    end
    object edtGrupoOperadora: TMaskEdit
      Left = 96
      Top = 40
      Width = 60
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 1
      Text = '      '
      OnExit = edtGrupoOperadoraExit
      OnKeyDown = edtGrupoOperadoraKeyDown
    end
    object edtOperadora: TMaskEdit
      Left = 96
      Top = 64
      Width = 60
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 2
      Text = '      '
      OnExit = edtOperadoraExit
      OnKeyDown = edtOperadoraKeyDown
    end
    object edtDtTransacaoFim: TMaskEdit
      Left = 680
      Top = 15
      Width = 77
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 4
      Text = '  /  /    '
    end
    object edtDtTransacaoIni: TMaskEdit
      Left = 592
      Top = 15
      Width = 81
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
    object edtNrParcela: TMaskEdit
      Left = 592
      Top = 38
      Width = 23
      Height = 21
      EditMask = '##;1; '
      MaxLength = 2
      TabOrder = 5
      Text = '  '
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 159
      Top = 16
      Width = 321
      Height = 21
      DataField = 'cNmLoja'
      DataSource = dsLoja
      TabOrder = 8
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 159
      Top = 40
      Width = 321
      Height = 21
      DataField = 'cNmGrupoOperadoraCartao'
      DataSource = dsGrupoOperadora
      TabOrder = 9
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 159
      Top = 64
      Width = 321
      Height = 21
      DataField = 'cNmOperadoraCartao'
      DataSource = dsOperadora
      TabOrder = 10
    end
    object cxButton1: TcxButton
      Left = 640
      Top = 66
      Width = 121
      Height = 25
      Caption = 'Exibir &Transa'#231#245'es'
      TabOrder = 7
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D360300000000000036000000280000000F000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
        393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
        00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
        F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
        83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
        68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
        FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
        9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
        8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
        D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
        B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
        5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
        A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
        9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
        488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
        FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
        FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
        FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
    object edtNrDocto: TMaskEdit
      Left = 680
      Top = 37
      Width = 80
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 6
      Text = '         '
    end
  end
  object cxGrid2: TcxGrid [3]
    Left = 0
    Top = 126
    Width = 1296
    Height = 347
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 2
    object cxGridDBTableView1: TcxGridDBTableView
      DataController.DataSource = dsTransacaoPendente
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'nValTit'
          Column = cxGridDBTableView1nValTit
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'nSaldoTit'
          Column = cxGridDBTableView1nSaldoTit
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTit'
          Column = cxGridDBTableView1nValTit
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nSaldoTit'
          Column = cxGridDBTableView1nSaldoTit
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnHidingOnGrouping = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      OptionsView.GroupFooters = gfAlwaysVisible
      object cxGridDBTableView1nCdTitulo: TcxGridDBColumn
        DataBinding.FieldName = 'nCdTitulo'
        Visible = False
        Options.Editing = False
      end
      object cxGridDBTableView1nCdTransacaoCartao: TcxGridDBColumn
        DataBinding.FieldName = 'nCdTransacaoCartao'
        Visible = False
        Options.Editing = False
      end
      object cxGridDBTableView1dDtVenc: TcxGridDBColumn
        Caption = 'Dt. Vencimento'
        DataBinding.FieldName = 'dDtVenc'
        Width = 90
      end
      object cxGridDBTableView1dDtTransacao: TcxGridDBColumn
        Caption = 'Dt. Transa'#231#227'o'
        DataBinding.FieldName = 'dDtTransacao'
        Options.Editing = False
        Width = 100
      end
      object cxGridDBTableView1DBcNmOperadoraCartao: TcxGridDBColumn
        Caption = 'Op.Cart'#227'o'
        DataBinding.FieldName = 'cNmOperadoraCartao'
        Width = 100
      end
      object cxGridDBTableView1DBiNrDocto: TcxGridDBColumn
        Caption = 'Docto.'
        DataBinding.FieldName = 'iNrDocto'
        Width = 65
      end
      object cxGridDBTableView1DBiNrAutorizacao: TcxGridDBColumn
        Caption = 'Aut.'
        DataBinding.FieldName = 'iNrAutorizacao'
        Width = 65
      end
      object cxGridDBTableView1cNrLote: TcxGridDBColumn
        Caption = 'Lote'
        DataBinding.FieldName = 'cNrLote'
        Options.Editing = False
        Width = 58
      end
      object cxGridDBTableView1cParcela: TcxGridDBColumn
        Caption = 'Parc.'
        DataBinding.FieldName = 'cParcela'
        Options.Editing = False
        Width = 60
      end
      object cxGridDBTableView1nCdOperadora: TcxGridDBColumn
        DataBinding.FieldName = 'nCdOperadora'
        Visible = False
        Options.Editing = False
      end
      object cxGridDBTableView1cNmOperadora: TcxGridDBColumn
        Caption = 'Operadora'
        DataBinding.FieldName = 'cNmOperadora'
        Visible = False
        GroupIndex = 0
        Options.Editing = False
      end
      object cxGridDBTableView1nCdGrupoOperadora: TcxGridDBColumn
        DataBinding.FieldName = 'nCdGrupoOperadora'
        Visible = False
        Options.Editing = False
      end
      object cxGridDBTableView1cNmGrupoOperadora: TcxGridDBColumn
        DataBinding.FieldName = 'cNmGrupoOperadora'
        Visible = False
        Options.Editing = False
      end
      object cxGridDBTableView1nValTit: TcxGridDBColumn
        Caption = 'Valor'
        DataBinding.FieldName = 'nValTit'
        Options.Editing = False
        Width = 66
      end
      object cxGridDBTableView1nSaldoTit: TcxGridDBColumn
        Caption = 'L'#237'quido'
        DataBinding.FieldName = 'nSaldoTit'
        Width = 66
      end
      object cxGridDBTableView1dDtVencOriginal: TcxGridDBColumn
        DataBinding.FieldName = 'dDtVencOriginal'
        Visible = False
        Options.Editing = False
      end
      object cxGridDBTableView1nSaldoTitOriginal: TcxGridDBColumn
        DataBinding.FieldName = 'nSaldoTitOriginal'
        Visible = False
        Options.Editing = False
      end
      object cxGridDBTableView1cStatus: TcxGridDBColumn
        DataBinding.FieldName = 'cStatus'
        Visible = False
        Options.Editing = False
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 96
    Top = 248
  end
  object qryTransacoes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdLoja'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdGrupoOperadoraCartao'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdOperadoraCartao'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtTransacaoIni'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtTransacaoFim'
        Size = -1
        Value = Null
      end
      item
        Name = 'iParcela'
        Size = -1
        Value = Null
      end
      item
        Name = 'iNrDocto'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      'SET NOCOUNT ON'
      ''
      'DECLARE @nCdEmpresa              int'
      '       ,@nCdLoja                 int'
      '       ,@nCdGrupoOperadoraCartao int'
      '       ,@nCdOperadoraCartao      int'
      '       ,@dDtTransacaoIni         varchar(10)'
      '       ,@dDtTransacaoFim         varchar(10)'
      '       ,@iParcela                int'
      '       ,@iNrDocto                int'
      ''
      'Set @nCdEmpresa              = :nCdEmpresa'
      'Set @nCdLoja                 = :nCdLoja   '
      'Set @nCdGrupoOperadoraCartao = :nCdGrupoOperadoraCartao'
      'Set @nCdOperadoraCartao      = :nCdOperadoraCartao'
      'Set @dDtTransacaoIni         = :dDtTransacaoIni'
      'Set @dDtTransacaoFim         = :dDtTransacaoFim'
      'Set @iParcela                = :iParcela'
      'Set @iNrDocto                = :iNrDocto'
      ''
      'DELETE'
      '  FROM #TempTransacoes'
      ' WHERE cStatus = '#39'PEN'#39
      ''
      'INSERT INTO #TempTransacoes (nCdTitulo'
      #9#9#9#9#9#9#9',nCdTransacaoCartao'
      #9#9#9#9#9#9#9',dDtVenc'
      #9#9#9#9#9#9#9',dDtTransacao'
      #9#9#9#9#9#9#9',cNrLote'
      #9#9#9#9#9#9#9',cParcela'
      #9#9#9#9#9#9#9',nCdOperadora'
      #9#9#9#9#9#9#9',cNmOperadora'
      #9#9#9#9#9#9#9',nCdGrupoOperadora'
      #9#9#9#9#9#9#9',cNmGrupoOperadora'
      #9#9#9#9#9#9#9',cNmOperadoraCartao'
      #9#9#9#9#9#9#9',iNrDocto'
      #9#9#9#9#9#9#9',iNrAutorizacao'
      #9#9#9#9#9#9#9',nValTit'
      #9#9#9#9#9#9#9',nSaldoTit'
      #9#9#9#9#9#9#9',dDtVencOriginal'
      #9#9#9#9#9#9#9',nSaldoTitOriginal'
      #9#9#9#9#9#9#9',cStatus'
      '                            ,iParcela)'
      '                      SELECT Titulo.nCdTitulo'
      '                            ,Titulo.nCdTransacaoCartao'
      '                            ,Titulo.dDtVenc'
      '                            ,Titulo.dDtEmissao'
      '                            ,TransacaoCartao.cNrLotePOS'
      
        '                            ,CASE WHEN TransacaoCartao.iParcelas' +
        ' = 1 THEN '#39'A VISTA'#39
      
        '                                  ELSE Convert(VARCHAR(3),Titulo' +
        '.iParcela) + '#39' de '#39' + Convert(VARCHAR(3),TransacaoCartao.iParcel' +
        'as)'
      '                             END'
      '                            ,Titulo.nCdOperadoraCartao'
      '                            ,OperadoraCartao.cNmOperadoraCartao'
      '                            ,Titulo.nCdGrupoOperadoraCartao'
      
        '                            ,GrupoOperadoraCartao.cNmGrupoOperad' +
        'oraCartao'
      '                            ,OperadoraCartao.cNmOperadoraCartao'
      '                            ,TransacaoCartao.iNrDocto'
      '                            ,TransacaoCartao.iNrAutorizacao'
      
        '                            ,Titulo.nSaldoTit + Titulo.nValTaxaO' +
        'peradora'
      '                            ,Titulo.nSaldoTit'
      '                            ,Titulo.dDtVenc'
      '                            ,Titulo.nSaldoTit'
      '                            ,'#39'PEN'#39
      '                            ,Titulo.iParcela'
      '                        FROM Titulo'
      
        '                             INNER JOIN TransacaoCartao       ON' +
        ' TransacaoCartao.nCdTransacaoCartao           = Titulo.nCdTransa' +
        'caoCartao'
      
        '                             INNER JOIN OperadoraCartao       ON' +
        ' OperadoraCartao.nCdOperadoraCartao           = Titulo.nCdOperad' +
        'oraCartao'
      
        '                             INNER JOIN GrupoOperadoraCartao  ON' +
        ' GrupoOperadoraCartao.nCdGrupoOperadoraCartao = Titulo.nCdGrupoO' +
        'peradoraCartao'
      
        '                       WHERE Titulo.cFlgCartaoConciliado       =' +
        ' 0'
      
        '                         AND TransacaoCartao.cFlgPOSEncerrado  =' +
        ' 0'
      
        '                         AND Titulo.dDtCancel                  I' +
        'S NULL'
      
        '                         AND Titulo.nCdEmpresa                 =' +
        ' @nCdEmpresa'
      
        '                         AND Titulo.nCdLojaTit                 =' +
        ' @nCdLoja'
      
        '                         AND ((Titulo.nCdGrupoOperadoraCartao  =' +
        ' @nCdGrupoOperadoraCartao) OR (@nCdGrupoOperadoraCartao = 0))'
      
        '                         AND ((Titulo.nCdOperadoraCartao       =' +
        ' @nCdOperadoraCartao)      OR (@nCdOperadoraCartao      = 0))'
      
        '                         AND ((Titulo.dDtEmissao              >=' +
        ' Convert(DATETIME,@dDtTransacaoIni,103))     OR (@dDtTransacaoIn' +
        'i = '#39'01/01/1900'#39'))'
      
        '                         AND ((Titulo.dDtEmissao               <' +
        ' (Convert(DATETIME,@dDtTransacaoFim,103)+1)) OR (@dDtTransacaoFi' +
        'm = '#39'01/01/1900'#39'))'
      
        '                         AND ((Titulo.iParcela                 =' +
        ' @iParcela)                                  OR (@iParcela = 0))'
      
        '                         AND ((TransacaoCartao.iNrDocto        =' +
        ' @iNrDocto)                                  OR (@iNrDocto = 0))'
      '                         AND NOT EXISTS(SELECT 1'
      '                                          FROM #TempTransacoes'
      
        '                                         WHERE #TempTransacoes.n' +
        'CdTitulo = Titulo.nCdTitulo)')
    Left = 126
    Top = 248
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempTransacoes'#39') IS NULL) '#13#10'BEGIN'#13#10#13#10#9'CR' +
      'EATE TABLE #TempTransacoes (nCdTitulo          int           pri' +
      'mary key'#13#10#9#9#9#9#9#9#9#9#9#9'  ,nCdTransacaoCartao int'#13#10#9#9#9#9#9#9#9#9#9#9'  ,dDtV' +
      'enc            datetime'#13#10#9#9#9#9#9#9#9#9#9#9'  ,dDtTransacao       datetim' +
      'e '#13#10#9#9#9#9#9#9#9#9#9#9'  ,cNrLote            char(15)'#13#10#9#9#9#9#9#9#9#9#9#9'  ,cParc' +
      'ela           varchar(20)'#13#10#9#9#9#9#9#9#9#9#9#9'  ,nCdOperadora       int'#13#10 +
      #9#9#9#9#9#9#9#9#9#9'  ,cNmOperadora       varchar(50)'#13#10#9#9#9#9#9#9#9#9#9#9'  ,nCdGru' +
      'poOperadora  int'#13#10#9#9#9#9#9#9#9#9#9#9'  ,cNmGrupoOperadora  varchar(50)'#13#10#9 +
      #9#9#9#9#9#9'              ,cNmOperadoraCartao varchar(50)'#13#10#9#9#9#9#9#9#9'    ' +
      '          ,iNrDocto           int'#13#10#9#9#9#9#9#9#9'              ,iNrAuto' +
      'rizacao     int'#13#10#9#9#9#9#9#9#9#9#9#9'  ,nValTit            decimal(12,2) d' +
      'efault 0 not null'#13#10#9#9#9#9#9#9#9#9#9#9'  ,nSaldoTit          decimal(12,2)' +
      ' default 0 not null'#13#10#9#9#9#9#9#9#9#9#9#9'  ,dDtVencOriginal    datetime'#13#10#9 +
      #9#9#9#9#9#9#9#9#9'  ,nSaldoTitOriginal  decimal(12,2) default 0 not null'#13 +
      #10#9#9#9#9#9#9#9#9#9#9'  ,cStatus            char(3), iParcela int)'#13#10#13#10'END'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 158
    Top = 248
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdLoja    = Loja.nCdLoja'
      '                 AND UL.nCdUsuario = :nCdUsuario)')
    Left = 96
    Top = 280
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryGrupoOperadora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoOperadoraCartao, cNmGrupoOperadoraCartao'
      'FROM GrupoOperadoraCartao'
      'WHERE nCdGrupoOperadoraCartao = :nPK')
    Left = 128
    Top = 280
    object qryGrupoOperadoranCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryGrupoOperadoracNmGrupoOperadoraCartao: TStringField
      FieldName = 'cNmGrupoOperadoraCartao'
      Size = 50
    end
  end
  object qryOperadora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOperadoraCartao, cNmOperadoraCartao'
      'FROM OperadoraCartao'
      'WHERE nCdOperadoraCartao = :nPK')
    Left = 160
    Top = 280
    object qryOperadoranCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryOperadoracNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
  end
  object dsOperadora: TDataSource
    DataSet = qryOperadora
    Left = 160
    Top = 312
  end
  object dsGrupoOperadora: TDataSource
    DataSet = qryGrupoOperadora
    Left = 128
    Top = 312
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 96
    Top = 312
  end
  object qryTransacaoPendente: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM #TempTransacoes'
      'WHERE cStatus = '#39'PEN'#39)
    Left = 189
    Top = 248
    object qryTransacaoPendentenCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTransacaoPendentenCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTransacaoPendentedDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTransacaoPendentedDtTransacao: TDateTimeField
      FieldName = 'dDtTransacao'
    end
    object qryTransacaoPendentecNrLote: TStringField
      FieldName = 'cNrLote'
      FixedChar = True
      Size = 15
    end
    object qryTransacaoPendentecParcela: TStringField
      FieldName = 'cParcela'
    end
    object qryTransacaoPendentenCdOperadora: TIntegerField
      FieldName = 'nCdOperadora'
    end
    object qryTransacaoPendentecNmOperadora: TStringField
      FieldName = 'cNmOperadora'
      Size = 50
    end
    object qryTransacaoPendentenCdGrupoOperadora: TIntegerField
      FieldName = 'nCdGrupoOperadora'
    end
    object qryTransacaoPendentecNmGrupoOperadora: TStringField
      FieldName = 'cNmGrupoOperadora'
      Size = 50
    end
    object qryTransacaoPendentenValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTransacaoPendentenSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTransacaoPendentedDtVencOriginal: TDateTimeField
      FieldName = 'dDtVencOriginal'
    end
    object qryTransacaoPendentenSaldoTitOriginal: TBCDField
      FieldName = 'nSaldoTitOriginal'
      Precision = 12
      Size = 2
    end
    object qryTransacaoPendentecStatus: TStringField
      FieldName = 'cStatus'
      FixedChar = True
      Size = 3
    end
    object qryTransacaoPendenteiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTransacaoPendentecNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryTransacaoPendenteiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryTransacaoPendenteiNrAutorizacao: TIntegerField
      FieldName = 'iNrAutorizacao'
    end
  end
  object dsTransacaoPendente: TDataSource
    DataSet = qryTransacaoPendente
    Left = 191
    Top = 280
  end
  object PopupMenu1: TPopupMenu
    Left = 191
    Top = 313
    object AlterarDadosdoPagamento1: TMenuItem
      Caption = 'Alterar Dados do Pagamento'
      OnClick = AlterarDadosdoPagamento1Click
    end
  end
  object qryTransacaoCartao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TransacaoCartao'
      'WHERE nCdTransacaoCartao = :nPK')
    Left = 223
    Top = 280
    object qryTransacaoCartaonCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTransacaoCartaodDtTransacao: TDateTimeField
      FieldName = 'dDtTransacao'
    end
    object qryTransacaoCartaodDtCredito: TDateTimeField
      FieldName = 'dDtCredito'
    end
    object qryTransacaoCartaoiNrCartao: TLargeintField
      FieldName = 'iNrCartao'
    end
    object qryTransacaoCartaoiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryTransacaoCartaoiNrAutorizacao: TIntegerField
      FieldName = 'iNrAutorizacao'
    end
    object qryTransacaoCartaonCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryTransacaoCartaonCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryTransacaoCartaonValTransacao: TBCDField
      FieldName = 'nValTransacao'
      Precision = 12
      Size = 2
    end
    object qryTransacaoCartaonCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryTransacaoCartaocFlgConferencia: TIntegerField
      FieldName = 'cFlgConferencia'
    end
    object qryTransacaoCartaonCdUsuarioConf: TIntegerField
      FieldName = 'nCdUsuarioConf'
    end
    object qryTransacaoCartaodDtConferencia: TDateTimeField
      FieldName = 'dDtConferencia'
    end
    object qryTransacaoCartaonCdStatusDocto: TIntegerField
      FieldName = 'nCdStatusDocto'
    end
    object qryTransacaoCartaoiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryTransacaoCartaocNrLotePOS: TStringField
      FieldName = 'cNrLotePOS'
      FixedChar = True
      Size = 15
    end
    object qryTransacaoCartaonCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryTransacaoCartaocFlgPOSEncerrado: TIntegerField
      FieldName = 'cFlgPOSEncerrado'
    end
    object qryTransacaoCartaonCdLojaCartao: TIntegerField
      FieldName = 'nCdLojaCartao'
    end
    object qryTransacaoCartaonValTaxaOperadora: TBCDField
      FieldName = 'nValTaxaOperadora'
      Precision = 12
      Size = 2
    end
    object qryTransacaoCartaonCdCondPagtoCartao: TIntegerField
      FieldName = 'nCdCondPagtoCartao'
    end
    object qryTransacaoCartaonCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryTransacaoCartaodDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryTransacaoCartaocIDExterno: TStringField
      FieldName = 'cIDExterno'
      Size = 50
    end
    object qryTransacaoCartaocNrAutorizacao: TStringField
      FieldName = 'cNrAutorizacao'
      Size = 12
    end
    object qryTransacaoCartaocNrDoctoNSU: TStringField
      FieldName = 'cNrDoctoNSU'
      Size = 50
    end
  end
  object qryDadosTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTitulo, nCdLojaTit'
      '  FROM Titulo'
      ' WHERE nCdTitulo = :nPK')
    Left = 223
    Top = 248
    object qryDadosTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryDadosTitulonCdLojaTit: TIntegerField
      FieldName = 'nCdLojaTit'
    end
  end
  object qryCartaoSituacaoAtual: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmOperadoraCartao'
      '      ,cNmCondPagto'
      '      ,cNmFormaPagto'
      '  FROM TransacaoCartao'
      
        '       LEFT JOIN OperadoraCartao ON OperadoraCartao.nCdOperadora' +
        'Cartao = TransacaoCartao.nCdOperadoraCartao'
      
        '       LEFT JOIN CondPagto       ON CondPagto.nCdCondPagto      ' +
        '       = TransacaoCartao.nCdCondPagtoCartao'
      
        '       LEFT JOIN FormaPagto      ON FormaPagto.nCdFormaPagto    ' +
        '       = CondPagto.nCdFormaPagto'
      ' WHERE nCdTransacaoCartao = :nPK')
    Left = 224
    Top = 312
    object qryCartaoSituacaoAtualcNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryCartaoSituacaoAtualcNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryCartaoSituacaoAtualcNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
  end
  object qryCondPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nPercAcrescimo'
      ',nCdTabTipoFormaPagto'
      'FROM CondPagto'
      
        'LEFT JOIN FormaPagto ON FormaPagto.nCdFormaPagto = CondPagto.nCd' +
        'FormaPagto'
      'WHERE nCdCondPagto = :nPK')
    Left = 288
    Top = 304
    object qryCondPagtonPercAcrescimo: TBCDField
      FieldName = 'nPercAcrescimo'
      Precision = 12
      Size = 2
    end
    object qryCondPagtonCdTabTipoFormaPagto: TIntegerField
      FieldName = 'nCdTabTipoFormaPagto'
    end
  end
  object SP_ALTERA_CONDICAO_TRANSACAO_CARTAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ALTERA_CONDICAO_TRANSACAO_CARTAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTransacaoCartao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdCondPagto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdOperadoraCartao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNrDocto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNrAutorizacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNrDoctoNSU'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@cNrAutorizacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 12
        Value = Null
      end>
    Left = 320
    Top = 304
  end
end
