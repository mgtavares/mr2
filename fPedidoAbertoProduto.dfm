inherited frmPedidoAbertoProduto: TfrmPedidoAbertoProduto
  Left = 79
  Top = 204
  Width = 1033
  Height = 365
  Caption = 'Pedido em aberto'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1017
    Height = 264
  end
  inherited ToolBar1: TToolBar
    Width = 1017
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1017
    Height = 264
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsMaster
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nSaldo
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nQtdePrev
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.FonteSomenteLeitura
      Styles.OnGetContentStyle = cxGrid1DBTableView1StylesGetContentStyle
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
        Caption = 'Pedido'
        DataBinding.FieldName = 'nCdPedido'
      end
      object cxGrid1DBTableView1dDtPedido: TcxGridDBColumn
        Caption = 'Dt. Pedido'
        DataBinding.FieldName = 'dDtPedido'
        Width = 83
      end
      object cxGrid1DBTableView1dDtAutor: TcxGridDBColumn
        Caption = 'Dt. Autoriz.'
        DataBinding.FieldName = 'dDtAutor'
        Width = 86
      end
      object cxGrid1DBTableView1dDtPrevEntIni: TcxGridDBColumn
        Caption = 'Prev. Entrega Inicial'
        DataBinding.FieldName = 'dDtPrevEntIni'
      end
      object cxGrid1DBTableView1dDtPrevEntFim: TcxGridDBColumn
        Caption = 'Prev. Entrega Final'
        DataBinding.FieldName = 'dDtPrevEntFim'
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro Fornecedor'
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 139
      end
      object cxGrid1DBTableView1nSaldo: TcxGridDBColumn
        Caption = 'Saldo a Receber'
        DataBinding.FieldName = 'nSaldo'
      end
      object cxGrid1DBTableView1nQtdePrev: TcxGridDBColumn
        Caption = 'Qt. Previs'#227'o'
        DataBinding.FieldName = 'nQtdePrev'
      end
      object cxGrid1DBTableView1cSiglaUnidadeMedida: TcxGridDBColumn
        Caption = 'UM'
        DataBinding.FieldName = 'cSiglaUnidadeMedida'
        Width = 42
      end
      object cxGrid1DBTableView1cNrPedTerceiro: TcxGridDBColumn
        Caption = 'N'#250'm. Ped. Terceiro'
        DataBinding.FieldName = 'cNrPedTerceiro'
      end
      object cxGrid1DBTableView1nCdProduto: TcxGridDBColumn
        DataBinding.FieldName = 'nCdProduto'
        Visible = False
      end
      object cxGrid1DBTableView1nCdItemPedido: TcxGridDBColumn
        DataBinding.FieldName = 'nCdItemPedido'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 293
    Width = 1017
    Height = 36
    Align = alBottom
    TabOrder = 2
    object Label2: TLabel
      Tag = 1
      Left = 40
      Top = 16
      Width = 83
      Height = 13
      Caption = 'Pedido em atraso'
    end
    object cxTextEdit3: TcxTextEdit
      Left = 8
      Top = 8
      Width = 25
      Height = 21
      Style.Color = clRed
      StyleDisabled.Color = clBlue
      TabOrder = 0
    end
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'DECLARE @nCdLoja int'
      ''
      'Set @nCdLoja = :nCdLoja'
      ''
      'SELECT Pedido.nCdPedido'
      '      ,dDtPedido'
      '      ,dDtAutor'
      '      ,CASE WHEN dDtPrevEntIni IS NULL THEN dDtEntregaIni'
      '            ELSE dDtPrevEntIni'
      '       END  dDtPrevEntIni'
      '      ,CASE WHEN dDtPrevEntIni IS NULL THEN dDtEntregaFim'
      '            ELSE dDtPrevEntFim'
      '       END dDtPrevEntFim'
      '      ,cNmTerceiro'
      
        '      ,(nQtdePed - nQtdeExpRec - nQtdeCanc) nSaldo,cNrPedTerceir' +
        'o'
      '      ,nQtdePrev'
      '      ,ItemPedido.nCdProduto'
      '      ,ItemPedido.nCdItemPedido'
      '      ,ItemPedido.cSiglaUnidadeMedida'
      '  FROM ItemPedido'
      
        '       INNER JOIN Pedido     ON Pedido.nCdPedido         = ItemP' +
        'edido.nCdPedido'
      
        '       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedid' +
        'o.nCdTipoPedido'
      
        '       INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Pedid' +
        'o.nCdTerceiro'
      ' WHERE ItemPedido.nCdProduto  = :nCdProduto'
      '   AND TipoPedido.cFlgCompra  = 1'
      '   AND Pedido.dDtAutor       IS NOT NULL'
      '   AND Pedido.nCdEmpresa      = :nCdEmpresa'
      '   AND (nQtdePed + nQtdePrev - nQtdeExpRec - nQtdeCanc) > 0'
      '   AND (@nCdLoja = 0 OR (Pedido.nCdLoja = @nCdLoja))'
      ' ORDER BY 5')
    Left = 320
    Top = 200
    object qryMasternCdPedido: TIntegerField
      DisplayLabel = 'N'#250'm.|Pedido'
      FieldName = 'nCdPedido'
    end
    object qryMasterdDtPedido: TDateTimeField
      DisplayLabel = 'Datas|Pedido'
      FieldName = 'dDtPedido'
    end
    object qryMasterdDtAutor: TDateTimeField
      DisplayLabel = 'Datas|Aprova'#231#227'o'
      FieldName = 'dDtAutor'
    end
    object qryMasterdDtPrevEntIni: TDateTimeField
      DisplayLabel = 'Previs'#227'o de entrega|Inicial'
      FieldName = 'dDtPrevEntIni'
    end
    object qryMasterdDtPrevEntFim: TDateTimeField
      DisplayLabel = 'Previs'#227'o de entrega|Final'
      FieldName = 'dDtPrevEntFim'
    end
    object qryMastercNmTerceiro: TStringField
      DisplayLabel = 'Terceiro|Fornecedor'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMasternSaldo: TBCDField
      DisplayLabel = 'Quantidades|Confirmada'
      FieldName = 'nSaldo'
      ReadOnly = True
      DisplayFormat = ',0'
      Precision = 14
    end
    object qryMastercNrPedTerceiro: TStringField
      DisplayLabel = 'Pedido do|Fornecedor'
      FieldName = 'cNrPedTerceiro'
      Size = 15
    end
    object qryMasternQtdePrev: TBCDField
      DisplayLabel = 'Quantidades|Prevista'
      FieldName = 'nQtdePrev'
      Precision = 12
    end
    object qryMasternCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryMasternCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryMastercSiglaUnidadeMedida: TStringField
      DisplayLabel = 'Quantidades|UM'
      FieldName = 'cSiglaUnidadeMedida'
      FixedChar = True
      Size = 2
    end
  end
  object dsMaster: TDataSource
    DataSet = qryMaster
    Left = 360
    Top = 200
  end
  object PopupMenu1: TPopupMenu
    Left = 224
    Top = 112
    object CancelarSaldodoPedido1: TMenuItem
      Caption = 'Cancelar Saldo do Item'
      OnClick = CancelarSaldodoPedido1Click
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 440
    Top = 144
  end
end
