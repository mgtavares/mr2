unit fMonitorOcorrencias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, DB, ADODB, StdCtrls,
  Mask, cxLookAndFeelPainters, cxButtons, cxPC, cxControls, ER2Lookup,
  DBCtrls;

type
  TfrmMonitorOcorrencias = class(TfrmProcesso_Padrao)
    qryOcorrencia: TADOQuery;
    dsOcorrencia: TDataSource;
    qryOcorrencianCdOcorrencia: TIntegerField;
    qryOcorrencianCdLoja: TIntegerField;
    qryOcorrenciacNmLoja: TStringField;
    qryOcorrenciacNmTerceiro: TStringField;
    qryOcorrenciacNmTipoOcorrencia: TStringField;
    qryOcorrenciacDescOcorrencia: TMemoField;
    qryOcorrenciacNmTabStatusOcorrencia: TStringField;
    qryOcorrenciacResumoOcorrencia: TStringField;
    qryOcorrenciacNmUsuarioCad: TStringField;
    qryOcorrenciacNmUsuarioResp: TStringField;
    qryOcorrenciadDtOcorrencia: TStringField;
    qryOcorrenciadDtUsuarioAtend: TStringField;
    qryOcorrenciadDtPrazoAtendIni: TStringField;
    qryOcorrenciadDtPrazoAtendFinal: TStringField;
    qryLoja: TADOQuery;
    dsLoja: TDataSource;
    qryLojacNmLoja: TStringField;
    qryTabStatusOcorrencia: TADOQuery;
    dsTabStatusOcorrencia: TDataSource;
    qryTabStatusOcorrenciacNmTabStatusOcorrencia: TStringField;
    qryTipoOcorrencia: TADOQuery;
    dsTipoOcorrencia: TDataSource;
    qryTipoOcorrenciacNmTipoOcorrencia: TStringField;
    qryOcorrencianCdTerceiro: TIntegerField;
    cxPageControl2: TcxPageControl;
    tabOcorrencia: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    ER2LknCdLoja: TER2LookupMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    lblHelp: TLabel;
    MaskEdit1: TMaskEdit;
    ER2LkTipoOcorrencia: TER2LookupMaskEdit;
    ER2LktStatusOcorrencia: TER2LookupMaskEdit;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    MaskEdit2: TMaskEdit;
    Label1: TLabel;
    cxButton1: TcxButton;
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure qryTipoOcorrenciaBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMonitorOcorrencias: TfrmMonitorOcorrencias;

implementation

uses fRegAtendOcorrencia,fMenu;

{$R *.dfm}

procedure TfrmMonitorOcorrencias.DBGridEh1DblClick(Sender: TObject);
var
  Obj : TfrmRegAtendOcorrencia;
begin
  inherited;

  Obj := TfrmRegAtendOcorrencia.Create(nil);
  Obj.qryMaster.Close;
  Obj.qryMaster.Parameters.ParamByName('nPK').Value := strtoint(DBGridEh1.Fields[0].AsString);
  Obj.qryMaster.Open;

  Obj.ShowModal;

  FreeAndNil(Obj);


end;

procedure TfrmMonitorOcorrencias.cxButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(MaskEdit1.Text) <> '/  /') and (Trim(MaskEdit2.Text) <> '/  /') and (StrToDateTime(MaskEdit1.Text)  > StrToDateTime(MaskEdit2.Text)) then
  begin
      MensagemAlerta('Data inicial � maior que data final. ');
      MaskEdit1.SetFocus;
      Abort;
  end;

  qryOcorrencia.Close;
  qryOcorrencia.Parameters.ParamByName('dDtIni').Value                 := MaskEdit1.Text;
  qryOcorrencia.Parameters.ParamByName('dDtFim').Value                 := MaskEdit2.Text;
  qryOcorrencia.Parameters.ParamByName('nCdLoja').Value                := frmMenu.ConvInteiro(ER2LknCdLoja.Text);
  qryOcorrencia.Parameters.ParamByName('nCdTabStatusOcorrencia').Value := frmMenu.ConvInteiro(ER2LktStatusOcorrencia.Text);
  qryOcorrencia.Parameters.ParamByName('nCdTipoOcorrencia').Value      := frmMenu.ConvInteiro(ER2LkTipoOcorrencia.Text);
  qryOcorrencia.Parameters.ParamByName('nCdUsuario').Value      := frmMenu.nCdUsuarioLogado;
  qryOcorrencia.Open;

end;

procedure TfrmMonitorOcorrencias.qryTipoOcorrenciaBeforeOpen(
  DataSet: TDataSet);
begin
  inherited;
  qryTipoOcorrencia.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

initialization
    RegisterClass(TfrmMonitorOcorrencias) ;

end.
