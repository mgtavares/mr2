unit rRelSobraCaixa_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptRelSobraCaixa_view = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    lblFiltro: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText6: TQRDBText;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    QRGroup2: TQRGroup;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape3: TQRShape;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRDBText8: TQRDBText;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    usp_RelatoriodDtAbertura: TDateTimeField;
    usp_RelatoriodDtFechamento: TDateTimeField;
    usp_RelatorionCdLoja: TIntegerField;
    usp_RelatoriocNmLoja: TStringField;
    usp_RelatorionCdContaBancaria: TIntegerField;
    usp_RelatorionCdConta: TStringField;
    usp_RelatorionValInformado: TFloatField;
    usp_RelatorionValCalculado: TFloatField;
    usp_RelatorionValDiferenca: TFloatField;
    usp_RelatorionSaldoAbertura: TFloatField;
    usp_RelatorionSaldoFechamento: TFloatField;
    usp_RelatorionValCredito: TFloatField;
    usp_RelatorionValDebito: TFloatField;
    usp_RelatoriocNmUsuario: TStringField;
    QRDBText11: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText13: TQRDBText;
    lblFiltro1: TQRLabel;
    QRDBText14: TQRDBText;
    QRBand6: TQRBand;
    QRLabel6: TQRLabel;
    QRExpr3: TQRExpr;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRelSobraCaixa_view: TrptRelSobraCaixa_view;

implementation

{$R *.dfm}

end.
