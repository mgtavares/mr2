inherited frmLiquidaParcela: TfrmLiquidaParcela
  Left = 184
  Top = 186
  Width = 775
  Height = 449
  BorderIcons = [biSystemMenu]
  Caption = 'Liquida'#231#227'o de Parcela'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 759
    Height = 384
  end
  object Label1: TLabel [1]
    Left = 83
    Top = 48
    Width = 11
    Height = 13
    Alignment = taRightJustify
    Caption = 'ID'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 53
    Top = 72
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nr.T'#237'tulo'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 59
    Top = 96
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'Parcela'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 41
    Top = 120
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dt.Emiss'#227'o'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 24
    Top = 144
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dt.Vencimento'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 29
    Top = 168
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dt.Liquida'#231#227'o'
  end
  object Label7: TLabel [7]
    Left = 41
    Top = 216
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor T'#237'tulo'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 44
    Top = 240
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Base'
  end
  object Label9: TLabel [9]
    Left = 41
    Top = 288
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Juros'
  end
  object Label10: TLabel [10]
    Left = 22
    Top = 360
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Desconto'
  end
  object Label11: TLabel [11]
    Left = 12
    Top = 336
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Abatimento'
  end
  object Label12: TLabel [12]
    Left = 41
    Top = 312
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Multa'
  end
  object Label13: TLabel [13]
    Left = 5
    Top = 384
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Vr Total a Receber'
  end
  object Label14: TLabel [14]
    Left = 295
    Top = 73
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Forma Pagto'
  end
  object Label15: TLabel [15]
    Left = 289
    Top = 98
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Cr'#233'dito'
  end
  object Label16: TLabel [16]
    Left = 279
    Top = 121
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Cheque'
  end
  object Label17: TLabel [17]
    Left = 288
    Top = 145
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nr Documento'
  end
  object Label18: TLabel [18]
    Left = 24
    Top = 264
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Corrigido'
  end
  object Label20: TLabel [19]
    Left = 265
    Top = 48
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = #205'ndice de Corre'#231#227'o'
    FocusControl = DBEdit18
  end
  object Label19: TLabel [20]
    Left = 24
    Top = 192
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dias de Atraso'
  end
  inherited ToolBar1: TToolBar
    Width = 759
    TabOrder = 9
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit1: TDBEdit [22]
    Tag = 1
    Left = 99
    Top = 40
    Width = 135
    Height = 21
    DataField = 'nCdTitulo'
    DataSource = DataSource1
    TabOrder = 10
  end
  object DBEdit2: TDBEdit [23]
    Tag = 1
    Left = 99
    Top = 64
    Width = 135
    Height = 21
    DataField = 'cNrTit'
    DataSource = DataSource1
    TabOrder = 11
  end
  object DBEdit3: TDBEdit [24]
    Tag = 1
    Left = 99
    Top = 88
    Width = 135
    Height = 21
    DataField = 'iParcela'
    DataSource = DataSource1
    TabOrder = 12
  end
  object DBEdit4: TDBEdit [25]
    Tag = 1
    Left = 99
    Top = 112
    Width = 135
    Height = 21
    DataField = 'dDtEmissao'
    DataSource = DataSource1
    TabOrder = 13
  end
  object DBEdit5: TDBEdit [26]
    Tag = 1
    Left = 99
    Top = 136
    Width = 135
    Height = 21
    DataField = 'dDtVenc'
    DataSource = DataSource1
    TabOrder = 14
  end
  object DBEdit7: TDBEdit [27]
    Tag = 1
    Left = 99
    Top = 208
    Width = 135
    Height = 21
    DataField = 'nValTit'
    DataSource = DataSource1
    ReadOnly = True
    TabOrder = 15
  end
  object DBEdit15: TDBEdit [28]
    Tag = 1
    Left = 427
    Top = 65
    Width = 321
    Height = 21
    DataField = 'cNmFormaPagto'
    DataSource = DataSource3
    TabOrder = 16
  end
  object edtFormaPagto: TMaskEdit [29]
    Left = 360
    Top = 65
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = edtFormaPagtoExit
    OnKeyDown = edtFormaPagtoKeyDown
  end
  object edtContaBancaria: TMaskEdit [30]
    Left = 360
    Top = 89
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = edtContaBancariaExit
    OnKeyDown = edtContaBancariaKeyDown
  end
  object DBEdit14: TDBEdit [31]
    Tag = 1
    Left = 427
    Top = 89
    Width = 321
    Height = 21
    DataField = 'DadosConta'
    DataSource = DataSource4
    TabOrder = 17
  end
  object edtNrDoc: TEdit [32]
    Left = 360
    Top = 137
    Width = 65
    Height = 21
    TabOrder = 8
    OnExit = edtNrDocExit
  end
  object DBEdit18: TDBEdit [33]
    Tag = 1
    Left = 360
    Top = 40
    Width = 65
    Height = 21
    DataField = 'nCdIndiceReajuste'
    DataSource = DataSource1
    TabOrder = 18
  end
  object DBEdit19: TDBEdit [34]
    Tag = 1
    Left = 427
    Top = 40
    Width = 321
    Height = 21
    DataField = 'cNmIndiceReajuste'
    DataSource = DataSource1
    TabOrder = 19
  end
  object edtDtLiq: TMaskEdit [35]
    Left = 99
    Top = 160
    Width = 135
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 0
    Text = '  /  /    '
    OnEnter = edtDtLiqEnter
    OnExit = edtDtLiqExit
  end
  object edtDiasAtraso: TMaskEdit [36]
    Tag = 1
    Left = 99
    Top = 184
    Width = 65
    Height = 21
    TabOrder = 20
  end
  object edtValCorrecao: TcxCurrencyEdit [37]
    Left = 99
    Top = 255
    Width = 135
    Height = 21
    Enabled = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '#,##0.00;-#,##0.00'
    Properties.ReadOnly = True
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    StyleDisabled.TextColor = clBlack
    TabOrder = 2
    OnExit = edtValCorrecaoExit
  end
  object edtValJuro: TcxCurrencyEdit [38]
    Left = 99
    Top = 279
    Width = 135
    Height = 21
    Enabled = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '#,##0.00;-#,##0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    StyleDisabled.TextColor = clBlack
    TabOrder = 21
  end
  object edtvalMulta: TcxCurrencyEdit [39]
    Left = 99
    Top = 303
    Width = 135
    Height = 21
    Enabled = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '#,##0.00;-#,##0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    StyleDisabled.TextColor = clBlack
    TabOrder = 22
  end
  object edtValAbatimento: TcxCurrencyEdit [40]
    Left = 99
    Top = 327
    Width = 135
    Height = 21
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '#,##0.00;-#,##0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    StyleDisabled.TextColor = clBlack
    TabOrder = 3
    OnExit = edtValAbatimentoExit
  end
  object edtValDesconto: TcxCurrencyEdit [41]
    Left = 99
    Top = 351
    Width = 135
    Height = 21
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '#,##0.00;-#,##0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    StyleDisabled.TextColor = clBlack
    TabOrder = 4
    OnExit = edtValDescontoExit
  end
  object edtValReceber: TcxCurrencyEdit [42]
    Left = 99
    Top = 375
    Width = 135
    Height = 21
    Enabled = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '#,##0.00;-#,##0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    StyleDisabled.TextColor = clBlack
    TabOrder = 23
  end
  object edtNrCheque: TMaskEdit [43]
    Left = 360
    Top = 113
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 7
    Text = '      '
    OnExit = edtNrChequeExit
  end
  object edtValBaseCorrecao: TcxCurrencyEdit [44]
    Left = 99
    Top = 231
    Width = 135
    Height = 21
    Enabled = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.DisplayFormat = '#,##0.00;-#,##0.00'
    Properties.ReadOnly = True
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    StyleDisabled.TextColor = clBlack
    TabOrder = 1
  end
  inherited ImageList1: TImageList
    Left = 688
    Top = 48
  end
  object qryLiquidaParcela: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      ' SELECT TIT.nCdTitulo'
      '              ,TIT.nCdEspTit'
      '              ,cNmEspTit'
      '              ,TIT.nCdSP'
      '              ,TIT.cNrTit'
      '              ,TIT.iParcela'
      '              ,TIT.nCdTerceiro'
      '              ,cNmTerceiro'
      '              ,cCNPJCPF'
      '              ,cRG'
      '              ,TIT.nCdMoeda'
      '              ,TIT.cSenso'
      '              ,TIT.dDtEmissao'
      '              ,TIT.dDtLiq'
      '              ,TIT.dDtVenc'
      
        '              ,convert(char(10), DATEADD(D,1-Day(TIT.dDtVenc),TI' +
        'T.dDtVenc),103) as cDtCotacao'
      '              ,TIT.nValTit'
      '              ,TIT.nSaldoTit'
      '              ,TIT.nValIndiceCorrecao'
      '              ,TIT.nValCorrecao'
      '              ,TIT.nValJuro'
      '              ,TIT.nValMulta'
      '              ,TIT.nvalDesconto'
      '              ,TIT.nValAbatimento'
      '              ,Tit.dDtCancel'
      '              ,nValLiq'
      '              ,ContratoEmpImobiliario.iParcelas'
      '              ,ContratoEmpImobiliario.cNrUnidade'
      '              ,ContratoEmpImobiliario.dDtContrato'
      '              ,ContratoEmpImobiliario.nCdIndiceReajuste'
      '              ,BlocoEmpImobiliario.cNmBlocoEmpImobiliario'
      '              ,EmpImobiliario.cNmEmpImobiliario'
      '              ,EmpImobiliario.cEndereco'
      
        '              ,TabTipoParcContratoEmpImobiliario.cNmTabTipoParcC' +
        'ontratoEmpImobiliario'
      '              ,IndiceReajuste.cNmIndiceReajuste'
      '          FROM Titulo TIT'
      
        '              LEFT  JOIN EspTit                                 ' +
        '             ON EspTit.nCdEspTit                           = TIT' +
        '.nCdEspTit'
      
        '              LEFT  JOIN Terceiro                               ' +
        '            ON Terceiro.nCdTerceiro                       = TIT.' +
        'nCdTerceiro'
      
        '              LEFT  JOIN ContratoEmpImobiliario                 ' +
        '    ON ContratoEmpImobiliario.nCdTerceiro         = Tit.nCdTerce' +
        'iro AND ContratoEmpImobiliario.cNumContrato = TIT.cNrTit '
      
        '              LEFT  JOIN BlocoEmpImobiliario                    ' +
        '      ON BlocoEmpImobiliario.nCdBlocoEmpImobiliario = ContratoEm' +
        'pImobiliario.nCdBlocoEmpImobiliario'
      
        '              LEFT  JOIN EmpImobiliario                         ' +
        '          ON EmpImobiliario.nCdEmpImobiliario           = BlocoE' +
        'mpImobiliario.nCdEmpImobiliario'
      
        '              LEFT  JOIN ParcContratoEmpImobiliario             ' +
        ' ON ParcContratoEmpImobiliario.nCdParcContratoEmpImobiliario    ' +
        '           = TIT.nCdParcContratoEmpImobiliario'
      
        '              LEFT  JOIN TabTipoParcContratoEmpImobiliario ON Ta' +
        'bTipoParcContratoEmpImobiliario.nCdTabTipoParcContratoEmpImobili' +
        'ario = ParcContratoEmpImobiliario.nCdTabTipoParcContratoEmpImobi' +
        'liario'
      
        '              LEFT  JOIN IndiceReajuste                         ' +
        '         ON IndiceReajuste.nCdIndiceReajuste = ContratoEmpImobil' +
        'iario.nCdIndiceReajuste'
      '         WHERE TIT.nCdTitulo =:nPK')
    Left = 704
    Top = 96
    object qryLiquidaParcelanCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryLiquidaParcelanCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryLiquidaParcelacNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryLiquidaParcelanCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryLiquidaParcelacNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryLiquidaParcelaiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryLiquidaParcelanCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryLiquidaParcelacNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryLiquidaParcelacCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryLiquidaParcelacRG: TStringField
      FieldName = 'cRG'
      Size = 15
    end
    object qryLiquidaParcelanCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryLiquidaParcelacSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryLiquidaParceladDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryLiquidaParceladDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
      EditMask = '!90/90/0000;1;_'
    end
    object qryLiquidaParceladDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryLiquidaParcelanValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLiquidaParcelanSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLiquidaParcelanValIndiceCorrecao: TBCDField
      FieldName = 'nValIndiceCorrecao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 8
    end
    object qryLiquidaParcelanValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLiquidaParcelanValJuro: TBCDField
      FieldName = 'nValJuro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLiquidaParcelanValMulta: TBCDField
      FieldName = 'nValMulta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLiquidaParcelanvalDesconto: TBCDField
      FieldName = 'nvalDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLiquidaParcelanValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLiquidaParceladDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryLiquidaParcelanValLiq: TBCDField
      FieldName = 'nValLiq'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLiquidaParcelaiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryLiquidaParcelacNrUnidade: TStringField
      FieldName = 'cNrUnidade'
      Size = 15
    end
    object qryLiquidaParceladDtContrato: TDateTimeField
      FieldName = 'dDtContrato'
    end
    object qryLiquidaParcelacNmBlocoEmpImobiliario: TStringField
      FieldName = 'cNmBlocoEmpImobiliario'
      Size = 50
    end
    object qryLiquidaParcelacNmEmpImobiliario: TStringField
      FieldName = 'cNmEmpImobiliario'
      Size = 100
    end
    object qryLiquidaParcelacEndereco: TStringField
      FieldName = 'cEndereco'
      Size = 100
    end
    object qryLiquidaParcelacNmTabTipoParcContratoEmpImobiliario: TStringField
      FieldName = 'cNmTabTipoParcContratoEmpImobiliario'
      Size = 50
    end
    object qryLiquidaParcelanValRecebido: TCurrencyField
      FieldKind = fkCalculated
      FieldName = 'nValRecebido'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object qryLiquidaParcelanCdIndiceReajuste: TIntegerField
      FieldName = 'nCdIndiceReajuste'
    end
    object qryLiquidaParcelacNmIndiceReajuste: TStringField
      FieldName = 'cNmIndiceReajuste'
      Size = 50
    end
    object qryLiquidaParcelacDtCotacao: TStringField
      FieldName = 'cDtCotacao'
      ReadOnly = True
      FixedChar = True
      Size = 10
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLiquidaParcela
    Left = 736
    Top = 96
  end
  object qryMovtoParcela: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nTitulo'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'select * from mtitulo where nCdtitulo =:ntitulo order by nCdMtit' +
        'ulo')
    Left = 584
    Top = 88
    object qryMovtoParcelanCdMTitulo: TAutoIncField
      FieldName = 'nCdMTitulo'
      ReadOnly = True
    end
    object qryMovtoParcelanCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryMovtoParcelanCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryMovtoParcelanCdOperacao: TIntegerField
      FieldName = 'nCdOperacao'
    end
    object qryMovtoParcelanCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryMovtoParcelaiNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryMovtoParcelanValMov: TBCDField
      FieldName = 'nValMov'
      Precision = 12
      Size = 2
    end
    object qryMovtoParceladDtMov: TDateTimeField
      FieldName = 'dDtMov'
    end
    object qryMovtoParceladDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryMovtoParceladDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryMovtoParcelacNrDoc: TStringField
      FieldName = 'cNrDoc'
      FixedChar = True
      Size = 15
    end
    object qryMovtoParceladDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryMovtoParcelanCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qryMovtoParcelanCdUsuarioCancel: TIntegerField
      FieldName = 'nCdUsuarioCancel'
    end
    object qryMovtoParcelacObsMov: TMemoField
      FieldName = 'cObsMov'
      BlobType = ftMemo
    end
    object qryMovtoParcelanCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMovtoParcelacFlgMovAutomatico: TIntegerField
      FieldName = 'cFlgMovAutomatico'
    end
    object qryMovtoParceladDtBomPara: TDateTimeField
      FieldName = 'dDtBomPara'
    end
    object qryMovtoParcelanCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMovtoParcelacFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryMovtoParcelanCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
    object qryMovtoParcelacFlgMovDA: TIntegerField
      FieldName = 'cFlgMovDA'
    end
    object qryMovtoParceladDtIntegracao: TDateTimeField
      FieldName = 'dDtIntegracao'
    end
  end
  object DataSource2: TDataSource
    DataSet = qryMovtoParcela
    Left = 624
    Top = 88
  end
  object usp_ProximoID: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'usp_ProximoID;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNmTabela'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@iUltimoCodigo'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 720
    Top = 160
  end
  object qryFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM FORMAPAGTO'
      'WHERE cFlgPermFinanceiro = 1'
      'AND nCdFormaPagto = :nPK')
    Left = 672
    Top = 240
    object qryFormaPagtonCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryFormaPagtocNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qryFormaPagto
    Left = 584
    Top = 264
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT CB.*'
      
        '      ,'#39'Banco:'#39' + isNull(RTRIM(Convert(VARCHAR,nCdBanco)),'#39#39') + ' +
        #39' Ag.:'#39' + isNull(RTRIM(Convert(VARCHAR,cAgencia)),'#39#39') + '#39' Conta:' +
        #39' + isNull(RTRIM(nCdConta),'#39#39')  + '#39' Titular: '#39'  +  isNull(cNmTit' +
        'ular,'#39#39')  as DadosConta'
      '  FROM ContaBancaria CB'
      ' WHERE EXISTS(SELECT 1 '
      '                FROM UsuarioContaBancaria UCB '
      '               WHERE UCB.nCdContaBancaria = CB.nCdContaBancaria'
      '                 AND UCB.nCdUsuario       = :nCdUsuario)'
      '   AND nCdEmpresa = :nCdEmpresa'
      'AND CB.nCdContaBancaria = :nPK')
    Left = 656
    Top = 200
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryContaBancarianCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryContaBancarianCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryContaBancariacFlgFluxo: TIntegerField
      FieldName = 'cFlgFluxo'
    end
    object qryContaBancariaiUltimoCheque: TIntegerField
      FieldName = 'iUltimoCheque'
    end
    object qryContaBancariaiUltimoBordero: TIntegerField
      FieldName = 'iUltimoBordero'
    end
    object qryContaBancariaDadosConta: TStringField
      FieldName = 'DadosConta'
      ReadOnly = True
      Size = 56
    end
    object qryContaBancarianValLimiteCredito: TBCDField
      FieldName = 'nValLimiteCredito'
      Precision = 12
      Size = 2
    end
    object qryContaBancariacFlgDeposito: TIntegerField
      FieldName = 'cFlgDeposito'
    end
    object qryContaBancariacFlgEmiteCheque: TIntegerField
      FieldName = 'cFlgEmiteCheque'
    end
    object qryContaBancariacAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryContaBancariacNmTitular: TStringField
      FieldName = 'cNmTitular'
      Size = 50
    end
  end
  object DataSource4: TDataSource
    DataSet = qryContaBancaria
    Left = 672
    Top = 168
  end
  object qryCotacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdIndice'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'iAno'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Imes'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from CotacaoIndiceReajuste'
      'where'
      'nCdIndiceReajuste = :nCdIndice AND'
      'iAnoReferencia =:iAno AND'
      'iMesReferencia =:Imes')
    Left = 720
    Top = 216
    object qryCotacaonCdCotacaoIndiceReajuste: TAutoIncField
      FieldName = 'nCdCotacaoIndiceReajuste'
      ReadOnly = True
    end
    object qryCotacaonCdIndiceReajuste: TIntegerField
      FieldName = 'nCdIndiceReajuste'
    end
    object qryCotacaoiAnoReferencia: TIntegerField
      FieldName = 'iAnoReferencia'
    end
    object qryCotacaoiMesReferencia: TIntegerField
      FieldName = 'iMesReferencia'
    end
    object qryCotacaonIndice: TBCDField
      FieldName = 'nIndice'
      Precision = 12
    end
  end
  object DataSource5: TDataSource
    DataSet = qryCotacao
    Left = 704
    Top = 288
  end
  object usp_atualizaBaixaEmpImobiliario: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ATUALIZA_BAIXA_EMP_IMOBILIARIO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nValCorrecao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = 0c
      end
      item
        Name = '@nValJuro'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = 0c
      end
      item
        Name = '@nValMulta'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = 0c
      end
      item
        Name = '@nValDesconto'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = 0c
      end
      item
        Name = '@nValAbatimento'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = 0c
      end
      item
        Name = '@nValIndiceCorrecao'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 12
        Value = 0c
      end
      item
        Name = '@nCdOperacaoJuro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdOperacaoMulta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdOperacaoDesconto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdOperacaoAbatimento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdOperacaoLiquidacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nValCorrecaoMonetaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdOperacaoCorrecaoMonetaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdFormaPagto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdContaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nNrCheque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@cNrDoc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = '0'
      end
      item
        Name = '@dDtLiq'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    Left = 576
    Top = 320
  end
  object qryUltimaParcelaPaga: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cNrTit'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 17
        Size = 17
        Value = Null
      end>
    SQL.Strings = (
      'select   top 1 titulo.nCdTitulo'
      '            ,titulo.iparcela'
      '            ,titulo.nValIndiceCorrecao'
      '            ,titulo.nValCorrecao '
      '            ,titulo.nSaldoTit  '
      '            ,titulo.nValTit'
      
        '            ,parcContratoEmpImobiliario.nCdTabTipoParcContratoEm' +
        'pImobiliario'
      
        '            ,TabTipoParcContratoEmpImobiliario.cNmTabTipoParcCon' +
        'tratoEmpImobiliario'
      'from titulo'
      
        'LEFT  JOIN ContratoEmpImobiliario                     ON Contrat' +
        'oEmpImobiliario.nCdTerceiro                                     ' +
        '= Titulo.nCdTerceiro AND ContratoEmpImobiliario.cNumContrato = T' +
        'itulo.cNrTit '
      
        'LEFT  JOIN ParcContratoEmpImobiliario              ON ParcContra' +
        'toEmpImobiliario.nCdParcContratoEmpImobiliario               = t' +
        'itulo.nCdParcContratoEmpImobiliario AND ParcContratoEmpImobiliar' +
        'io.nCdContratoEmpImobiliario = ContratoEmpImobiliario.nCdContrat' +
        'oEmpImobiliario'
      
        'LEFT  JOIN TabTipoParcContratoEmpImobiliario ON TabTipoParcContr' +
        'atoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario = ParcCon' +
        'tratoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario'
      'where titulo.nValIndiceCorrecao > 0 '
      
        '      AND ParcContratoEmpImobiliario.nCdTabTipoParcContratoEmpIm' +
        'obiliario = 2 '
      '      AND titulo.dDtLiq is not null '
      '      AND titulo.nCdterceiro = :nCdTerceiro '
      '      AND titulo.cNrTit         = :cNrTit '
      'order by titulo.iParcela desc '
      ''
      '')
    Left = 680
    Top = 336
    object qryUltimaParcelaPaganCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryUltimaParcelaPagaiparcela: TIntegerField
      FieldName = 'iparcela'
    end
    object qryUltimaParcelaPaganValIndiceCorrecao: TBCDField
      FieldName = 'nValIndiceCorrecao'
      Precision = 12
      Size = 8
    end
    object qryUltimaParcelaPaganValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryUltimaParcelaPaganCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryUltimaParcelaPagacNmTabTipoParcContratoEmpImobiliario: TStringField
      FieldName = 'cNmTabTipoParcContratoEmpImobiliario'
      Size = 50
    end
    object qryUltimaParcelaPaganSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryUltimaParcelaPaganValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
  end
  object DataSource6: TDataSource
    DataSet = qryUltimaParcelaPaga
    Left = 680
    Top = 368
  end
end
