unit rOrcamentoPrevReal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, ER2Lookup, DBCtrls;

type
  TrptOrcamentoPrevReal = class(TfrmRelatorio_Padrao)
    edtOrcamento: TER2LookupMaskEdit;
    qryOrcamento: TADOQuery;
    qryOrcamentonCdOrcamento: TIntegerField;
    qryOrcamentocNmOrcamento: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label1: TLabel;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    edtGrupoConta: TER2LookupMaskEdit;
    qryGrupoPlanoConta: TADOQuery;
    qryGrupoPlanoContanCdGrupoPlanoConta: TIntegerField;
    qryGrupoPlanoContacNmGrupoPlanoConta: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    Label2: TLabel;
    Label3: TLabel;
    edtPlanoConta: TER2LookupMaskEdit;
    qryPlanoConta: TADOQuery;
    qryPlanoContanCdPlanoConta: TIntegerField;
    qryPlanoContacNmPlanoConta: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    procedure edtOrcamentoExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtGrupoContaExit(Sender: TObject);
    procedure edtPlanoContaExit(Sender: TObject);
    procedure edtPlanoContaBeforeLookup(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptOrcamentoPrevReal: TrptOrcamentoPrevReal;

implementation

{$R *.dfm}

uses rOrcamentoPrevReal_View_Mensal , fMenu , rOrcamentoPrevReal_View_Tela, rOrcamentoPrevReal_View_Trimestral, rOrcamentoPrevReal_View_Semestral;

procedure TrptOrcamentoPrevReal.edtOrcamentoExit(Sender: TObject);
begin
  inherited;

  qryOrcamento.Close;
  qryOrcamento.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery( qryOrcamento , edtOrcamento.Text) ;

end;

procedure TrptOrcamentoPrevReal.ToolButton1Click(Sender: TObject);
var
  objRel    : TrptOrcamentoPrevReal_View_Mensal;
  objTela   : TrptOrcamentoPrevReal_View_Tela;
  objRelTri : TrptOrcamentoPrevReal_View_Trimestral;
  objRelSem : TrptOrcamentoPrevReal_View_Semestral;
begin
  inherited;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione um or�amento.') ;
      edtOrcamento.SetFocus;
      abort;
  end; ;

  if (RadioGroup2.ItemIndex = 0) then
  begin

      {-- Mensal --}
      if (RadioGroup1.ItemIndex = 0) then
      begin

          objRel := TrptOrcamentoPrevReal_View_Mensal.Create(nil);

          try
              objRel.SPREL_ORCAMENTO_PREVREAL.Close ;
              objRel.SPREL_ORCAMENTO_PREVREAL.Parameters.ParamByName('@nCdOrcamento').Value       := frmMenu.ConvInteiro( edtOrcamento.Text ) ;
              objRel.SPREL_ORCAMENTO_PREVREAL.Parameters.ParamByName('@nCdGrupoPlanoConta').Value := frmMenu.ConvInteiro( edtGrupoConta.Text ) ;
              objRel.SPREL_ORCAMENTO_PREVREAL.Parameters.ParamByName('@nCdPlanoContaAux').Value   := frmMenu.ConvInteiro( edtPlanoConta.Text ) ;
              objRel.SPREL_ORCAMENTO_PREVREAL.Parameters.ParamByName('@cFlgRetornaDataSet').Value := 1 ;
              objRel.SPREL_ORCAMENTO_PREVREAL.Open ;
          except
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          try

              try
                  objRel.QuickRep1.PreviewModal;

              except
                  MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
                  raise ;
              end ;

          finally
              FreeAndNil(objRel);
          end;

      end ;

      {-- Trimestral --}
      if (RadioGroup1.ItemIndex = 1) then
      begin

          objRelTri := TrptOrcamentoPrevReal_View_Trimestral.Create(nil);

          try
              objRelTri.SPREL_ORCAMENTO_PREVREAL_TRIMESTRE.Close ;
              objRelTri.SPREL_ORCAMENTO_PREVREAL_TRIMESTRE.Parameters.ParamByName('@nCdOrcamento').Value       := frmMenu.ConvInteiro( edtOrcamento.Text ) ;
              objRelTri.SPREL_ORCAMENTO_PREVREAL_TRIMESTRE.Parameters.ParamByName('@nCdGrupoPlanoConta').Value := frmMenu.ConvInteiro( edtGrupoConta.Text ) ;
              objRelTri.SPREL_ORCAMENTO_PREVREAL_TRIMESTRE.Parameters.ParamByName('@nCdPlanoContaAux').Value   := frmMenu.ConvInteiro( edtPlanoConta.Text ) ;
              objRelTri.SPREL_ORCAMENTO_PREVREAL_TRIMESTRE.Open ;
          except
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          objRelTri.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          try

              try
                  objRelTri.QuickRep1.PreviewModal;

              except
                  MensagemErro('Erro na cria��o da tela. Classe:  ' + objRelTri.ClassName) ;
                  raise ;
              end ;

          finally
              FreeAndNil(objRelTri);
          end;

      end ;

      {-- Semestral --}
      if (RadioGroup1.ItemIndex = 2) then
      begin

          objRelSem := TrptOrcamentoPrevReal_View_Semestral.Create(nil);

          try
              objRelSem.SPREL_ORCAMENTO_PREVREAL_SEMESTRE.Close ;
              objRelSem.SPREL_ORCAMENTO_PREVREAL_SEMESTRE.Parameters.ParamByName('@nCdOrcamento').Value       := frmMenu.ConvInteiro( edtOrcamento.Text ) ;
              objRelSem.SPREL_ORCAMENTO_PREVREAL_SEMESTRE.Parameters.ParamByName('@nCdGrupoPlanoConta').Value := frmMenu.ConvInteiro( edtGrupoConta.Text ) ;
              objRelSem.SPREL_ORCAMENTO_PREVREAL_SEMESTRE.Parameters.ParamByName('@nCdPlanoContaAux').Value   := frmMenu.ConvInteiro( edtPlanoConta.Text ) ;
              objRelSem.SPREL_ORCAMENTO_PREVREAL_SEMESTRE.Open ;
          except
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          objRelSem.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          try

              try
                  objRelSem.QuickRep1.PreviewModal;

              except
                  MensagemErro('Erro na cria��o da tela. Classe:  ' + objRelSem.ClassName) ;
                  raise ;
              end ;

          finally
              FreeAndNil(objRelSem);
          end;

      end ;

  end
  else
  begin

      objTela := TrptOrcamentoPrevReal_View_Tela.Create(nil);

      try
          objTela.SPREL_ORCAMENTO_PREVREAL.Close ;
          objTela.SPREL_ORCAMENTO_PREVREAL.Parameters.ParamByName('@nCdOrcamento').Value       := frmMenu.ConvInteiro( edtOrcamento.Text ) ;
          objTela.SPREL_ORCAMENTO_PREVREAL.Parameters.ParamByName('@nCdGrupoPlanoConta').Value := frmMenu.ConvInteiro( edtGrupoConta.Text ) ;
          objTela.SPREL_ORCAMENTO_PREVREAL.Parameters.ParamByName('@nCdPlanoContaAux').Value   := frmMenu.ConvInteiro( edtPlanoConta.Text ) ;
          objTela.SPREL_ORCAMENTO_PREVREAL.Parameters.ParamByName('@cFlgRetornaDataSet').Value := 1 ;
          objTela.SPREL_ORCAMENTO_PREVREAL.Open ;
      except
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      showForm( objTela , TRUE ) ;
      
  end ;

end;

procedure TrptOrcamentoPrevReal.edtGrupoContaExit(Sender: TObject);
begin
  inherited;

  qryGrupoPlanoConta.Close;
  PosicionaQuery( qryGrupoPlanoConta, edtGrupoConta.Text ) ;

end;

procedure TrptOrcamentoPrevReal.edtPlanoContaExit(Sender: TObject);
begin
  inherited;

  qryPlanoConta.Close;
  qryPlanoConta.Parameters.ParamByName('nCdGrupoPlanoConta').Value := frmMenu.ConvInteiro( edtGrupoConta.Text ) ;
  PosicionaQuery( qryPlanoConta, edtPlanoConta.Text ) ;

end;

procedure TrptOrcamentoPrevReal.edtPlanoContaBeforeLookup(Sender: TObject);
begin
  inherited;

  edtPlanoConta.WhereAdicional.Clear;

  if (DBEdit2.Text <> '') then
      edtPlanoConta.WhereAdicional.Add('PlanoConta.nCdGrupoPlanoConta = ' + qryGrupoPlanoContanCdGrupoPlanoConta.asString ) ;
      
end;

initialization
    RegisterClass( TrptOrcamentoPrevReal ) ;

end.
