inherited dcmCompraCentroCusto: TdcmCompraCentroCusto
  Left = 144
  Caption = 'Data Analysis - Compras por Centro de Custo'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 937
    Height = 418
  end
  object Label1: TLabel [1]
    Left = 66
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label3: TLabel [2]
    Left = 31
    Top = 96
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Compra'
  end
  object Label6: TLabel [3]
    Left = 196
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label10: TLabel [4]
    Left = 28
    Top = 72
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro de Custo'
  end
  inherited ToolBar1: TToolBar
    Width = 937
    TabOrder = 7
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [6]
    Tag = 1
    Left = 184
    Top = 40
    Width = 60
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 4
  end
  object DBEdit3: TDBEdit [7]
    Tag = 1
    Left = 248
    Top = 40
    Width = 645
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 5
  end
  object MaskEdit1: TMaskEdit [8]
    Left = 112
    Top = 88
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [9]
    Left = 216
    Top = 88
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [10]
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object DBEdit8: TDBEdit [11]
    Tag = 1
    Left = 184
    Top = 64
    Width = 97
    Height = 21
    DataField = 'cCdCC'
    DataSource = dsCentroCusto
    TabOrder = 6
  end
  object MaskEdit10: TMaskEdit [12]
    Left = 112
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit10Exit
    OnKeyDown = MaskEdit10KeyDown
  end
  object DBEdit1: TDBEdit [13]
    Tag = 1
    Left = 288
    Top = 64
    Width = 649
    Height = 21
    DataField = 'cNmCC'
    DataSource = dsCentroCusto
    TabOrder = 8
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 544
    Top = 232
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 304
  end
  object DataSource2: TDataSource
    Left = 632
    Top = 312
  end
  object DataSource3: TDataSource
    Left = 640
    Top = 320
  end
  object DataSource5: TDataSource
    Left = 640
    Top = 240
  end
  object qryCentroCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cCdCC,cNmCC'
      'FROM CentroCusto'
      'WHERE iNivel = 3'
      'AND nCdCC = :nPK')
    Left = 112
    Top = 136
    object qryCentroCustonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCentroCustocCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object qryCentroCustocNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object dsCentroCusto: TDataSource
    DataSet = qryCentroCusto
    Left = 144
    Top = 136
  end
end
