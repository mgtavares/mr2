unit fPais;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmPais = class(TfrmCadastro_Padrao)
    qryMasternCdPais: TIntegerField;
    qryMastercNmPais: TStringField;
    qryMastercSigla: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPais: TfrmPais;

implementation

{$R *.dfm}

procedure TfrmPais.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PAIS' ;
  nCdTabelaSistema  := 15 ;
  nCdConsultaPadrao := 20 ;

end;

procedure TfrmPais.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

initialization
    RegisterClass(tFrmPais);

end.
