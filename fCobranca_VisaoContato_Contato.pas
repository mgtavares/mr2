unit fCobranca_VisaoContato_Contato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  StdCtrls, Mask, DBCtrls, ADODB, cxLookAndFeelPainters, cxButtons;

type
  TfrmCobranca_VisaoContato_Contato = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    qryContatoCobranca: TADOQuery;
    qryContatoCobrancanCdContatoCobranca: TAutoIncField;
    qryContatoCobrancanCdTerceiro: TIntegerField;
    qryContatoCobrancanCdUsuario: TIntegerField;
    qryContatoCobrancanCdLoja: TIntegerField;
    qryContatoCobrancanCdTipoResultadoContato: TIntegerField;
    qryContatoCobrancadDtContato: TDateTimeField;
    qryContatoCobrancacNmPessoaContato: TStringField;
    qryContatoCobrancacTelefoneContato: TStringField;
    qryContatoCobrancacTextoContato: TMemoField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    dsContatoCobranca: TDataSource;
    dbLkpTipoResultadoContato: TDBLookupComboBox;
    qryTipoResultadoContato: TADOQuery;
    dsTipoResultadoContato: TDataSource;
    qryTipoResultadoContatonCdTipoResultadoContato: TIntegerField;
    qryTipoResultadoContatocNmTipoResultadoContato: TStringField;
    qryTipoResultadoContatocFlgAtivo: TIntegerField;
    qryTipoResultadoContatocFlgContatado: TIntegerField;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    GroupBox3: TGroupBox;
    DBMemo1: TDBMemo;
    btExibeCliente: TcxButton;
    qryAux: TADOQuery;
    qryContatoCobrancadDtReagendado: TDateTimeField;
    qryContatoCobrancacFlgPendReagendamento: TIntegerField;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    qryContatoCobrancanCdItemListaCobranca: TIntegerField;
    qryContatoCobrancacFlgCarta: TIntegerField;
    qryTipoResultadoContatonCdTabTipoRestricaoVenda: TIntegerField;
    SP_INSERI_BLOQUEIO_TERCEIRO: TADOStoredProc;
    qryContatoCobrancacFlgInclusoAutomatico: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure novoContato(nCdTerceiro:integer);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure btExibeClienteClick(Sender: TObject);
    procedure exibeContato(nCdContatoCobranca:integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdItemListaCobranca : integer;
    cFlgForcaContato     : boolean;
    cGravacaoEfetuada    : boolean;
    cTextoSugerido       : String;
    cClienteContatado    : boolean;
  end;

var
  frmCobranca_VisaoContato_Contato: TfrmCobranca_VisaoContato_Contato;

implementation

uses fMenu, fCobranca_VisaoContato_Contato_SelContato;

{$R *.dfm}

procedure TfrmCobranca_VisaoContato_Contato.FormShow(Sender: TObject);
begin
  inherited;

  cGravacaoEfetuada := False ;
  cClienteContatado := False ;

  qryTipoResultadoContato.Close;
  qryTipoResultadoContato.Parameters.ParamByName('cFlgContato').Value := 0 ;

  if (cFlgForcaContato) then
      qryTipoResultadoContato.Parameters.ParamByName('cFlgContato').Value := 1 ;

  qryTipoResultadoContato.Open;

  if (GroupBox1.Enabled) then
      dbLkpTipoResultadoContato.SetFocus;

end;

procedure TfrmCobranca_VisaoContato_Contato.novoContato(nCdTerceiro:integer);
begin

    qryContatoCobranca.Close;
    
    if not qryContatoCobranca.Active then
        PosicionaQuery(qryContatoCobranca, '0') ;

    qryContatoCobranca.Insert;
    qryContatoCobrancadDtContato.Value    := Now() ;
    qryContatoCobrancanCdUsuario.Value    := frmMenu.nCdUsuarioLogado ;
    qryContatoCobrancacTextoContato.Value := cTextoSugerido ;
    qryContatoCobrancanCdTerceiro.Value   := nCdTerceiro ;

    if (nCdItemListaCobranca > 0) then
        qryContatoCobrancanCdItemListaCobranca.Value := nCdItemListaCobranca ;
        
    if (frmMenu.nCdLojaAtiva > 0) then
        qryContatoCobrancanCdLoja.Value := frmMenu.nCdLojaAtiva;

    Self.ShowModal;

end;

procedure TfrmCobranca_VisaoContato_Contato.ToolButton1Click(
  Sender: TObject);
var
  bGerouBloqueio : boolean ;
begin
  inherited;

  if (qryContatoCobrancanCdTipoResultadoContato.Value = 0) then
  begin
      MensagemAlerta('Informe o resultado do contato.');
      dbLkpTipoResultadoContato.SetFocus;
      Abort;
  end;

  if (qryTipoResultadoContatocFlgContatado.Value = 1) and (Trim(qryContatoCobrancacNmPessoaContato.Value) = '') then
  begin
      MensagemAlerta('Informe o nome da pessoa de contato.') ;
      DBEdit2.SetFocus;
      abort ;
  end ;

  if (qryTipoResultadoContatocFlgContatado.Value = 1) and (Trim(qryContatoCobrancacTelefoneContato.Value) = '') then
  begin
      MensagemAlerta('Informe o n�mero de telefone de contato.') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (qryContatoCobrancadDtReagendado.AsString <> '') then
  begin
      if (qryContatoCobrancadDtReagendado.Value <= Date) then
      begin
          MensagemAlerta('A data para reagendamento deve ser maior que a data atual.') ;
          exit;
      end ;

      if (DayofWeek(qryContatoCobrancadDtReagendado.Value) = 1) then
          if (MessageDlg('A data para reagendamento � um Domingo. Confirma o reagendamento para esta data ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
              exit ;

  end ;

  if (MessageDlg('Confirma este registro de contato ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
      exit ;

  bGerouBloqueio := false ;

  try
      qryContatoCobrancacFlgInclusoAutomatico.Value := 0;
      qryContatoCobrancacTextoContato.Value         := AnsiUpperCase(DBMemo1.Text) ;
      qryContatoCobrancanCdContatoCobranca.Value    := frmMenu.fnProximoCodigo('CONTATOCOBRANCA') ;
      qryContatoCobranca.Post;

      {-- se o tipo de resultado do contato gerar algum tipo de bloqueio, gera aqui --}
      if (qryTipoResultadoContatonCdTabTipoRestricaoVenda.Value > 0) then
      begin

          {-- verifica se o cliente n�o tem um bloqueio desse tipo pendente de libera��o --}
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('SELECT TOP 1 1                    ') ;
          qryAux.SQL.Add('  FROM RestricaoVendaTerceiro     ') ;
          qryAux.SQL.Add(' WHERE nCdTerceiro              = ' + qryContatoCobrancanCdTerceiro.asString) ;
          qryAux.SQL.Add('   AND nCdTabTipoRestricaoVenda = ' + qryTipoResultadoContatonCdTabTipoRestricaoVenda.asString) ;
          qryAux.SQL.Add('   AND cFlgDesbloqueado         = 0') ;
          qryAux.Open;

          if (qryAux.Eof) then
          begin

              SP_INSERI_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdTerceiro').Value              := qryContatoCobrancanCdTerceiro.Value;
              SP_INSERI_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdTabTipoRestricaoVenda').Value := qryTipoResultadoContatonCdTabTipoRestricaoVenda.Value;
              SP_INSERI_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdUsuarioBloqueio').Value       := frmMenu.nCdUsuarioLogado;
              SP_INSERI_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdLojaBloqueio').Value          := frmMenu.nCdLojaAtiva;
              SP_INSERI_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@cObs').Value                     := 'GERADO AUT. VIA COBRAN�A';
              SP_INSERI_BLOQUEIO_TERCEIRO.ExecProc;

              bGerouBloqueio := true ;

          end ;

      end ;

  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  cGravacaoEfetuada := True ;
  cClienteContatado := False ;

  if (qryTipoResultadoContatocFlgContatado.Value = 1) then
      cClienteContatado := True ;

  if (bGerouBloqueio) then
  begin
      MensagemAlerta('Um bloqueio foi gerado no cadastro do cliente.') ;
  end ;

  Close;

end;

procedure TfrmCobranca_VisaoContato_Contato.ToolButton2Click(
  Sender: TObject);
begin
  cGravacaoEfetuada := false ;
  inherited;

end;

procedure TfrmCobranca_VisaoContato_Contato.btExibeClienteClick(
  Sender: TObject);
var
  objForm : TfrmCobranca_VisaoContato_Contato_SelContato;
begin
  inherited;

  objForm := TfrmCobranca_VisaoContato_Contato_SelContato.Create(nil);

  showForm(objForm,false);

  if not objForm.qryListaContato.Eof then
  begin
  
      qryContatoCobrancacTelefoneContato.Value := objForm.qryListaContatocTelefone.Value;

      if (trim(qryContatoCobrancacNmPessoaContato.Value) = '') then
          qryContatoCobrancacNmPessoaContato.Value := objForm.qryListaContatocContato.Value;

      DBEdit3.SetFocus;

  end ;

end;

procedure TfrmCobranca_VisaoContato_Contato.exibeContato(
  nCdContatoCobranca: integer);
begin
    PosicionaQuery(qryContatoCobranca, intToStr(nCdContatoCobranca)) ;

    ToolButton1.Enabled := False ;
    GroupBox1.Enabled   := False ;
    GroupBox2.Enabled   := False ;
    GroupBox3.Enabled   := False ;

    if (qryContatoCobrancanCdUsuario.Value = frmMenu.nCdUsuarioLogado) and (qryContatoCobrancadDtContato.Value > Date) then
    begin
        qryContatoCobranca.Edit;
        ToolButton1.Enabled := True ;
        GroupBox2.Enabled   := True ;
        GroupBox3.Enabled   := True ;
    end ;

    Self.ShowModal;
    
    ToolButton1.Enabled := True ;
    GroupBox1.Enabled   := True ;
    GroupBox2.Enabled   := True ;
    GroupBox3.Enabled   := True ;

end;

procedure TfrmCobranca_VisaoContato_Contato.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  cTextoSugerido := '';

end;

end.
