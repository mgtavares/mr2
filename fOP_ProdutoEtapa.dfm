inherited frmOP_ProdutoEtapa: TfrmOP_ProdutoEtapa
  Left = 110
  Top = 200
  Width = 900
  Caption = 'Produtos da Etapa'
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 884
  end
  inherited ToolBar1: TToolBar
    Width = 884
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 884
    Height = 435
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 431
    ClientRectLeft = 4
    ClientRectRight = 880
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Produtos'
      ImageIndex = 0
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 876
        Height = 407
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsProdutoEtapa
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh3KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdprodutoPai'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'cNmProduto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 301
          end
          item
            EditButtons = <>
            FieldName = 'cUnidadeMedidaEstoque'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'cUnidadeMedida'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'nFatorConversaoUM'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Tag = 1
            Width = 71
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePlanejada'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeAtendida'
            Footers = <>
            ReadOnly = True
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nCdLocalEstoque'
            Footers = <>
            ReadOnly = True
            Width = 40
          end
          item
            EditButtons = <>
            FieldName = 'cNmLocalEstoque'
            Footers = <>
            Width = 98
          end
          item
            EditButtons = <>
            FieldName = 'cNmTabTipoMetodoSaidaEstoque'
            Footers = <>
            ReadOnly = True
            Width = 86
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Width = 122
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object qryProdutoEtapa: TADOQuery
    AutoCalcFields = False
    Connection = frmMenu.Connection
    AfterInsert = qryProdutoEtapaAfterInsert
    BeforePost = qryProdutoEtapaBeforePost
    BeforeDelete = qryProdutoEtapaBeforeDelete
    AfterScroll = qryProdutoEtapaAfterScroll
    OnCalcFields = qryProdutoEtapaCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM ProdutoEtapaOrdemProducao'
      ' WHERE nCdEtapaOrdemProducao = :nPK'
      '')
    Left = 168
    Top = 152
    object qryProdutoEtapanCdProdutoEtapaOrdemProducao: TIntegerField
      FieldName = 'nCdProdutoEtapaOrdemProducao'
    end
    object qryProdutoEtapanCdEtapaOrdemProducao: TIntegerField
      FieldName = 'nCdEtapaOrdemProducao'
    end
    object qryProdutoEtapanCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd.'
      FieldName = 'nCdProduto'
      OnChange = qryProdutoEtapanCdProdutoChange
    end
    object qryProdutoEtapacNmProduto: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o Produto'
      FieldKind = fkCalculated
      FieldName = 'cNmProduto'
      FixedChar = True
      Size = 150
      Calculated = True
    end
    object qryProdutoEtapacUnidadeMedidaEstoque: TStringField
      DisplayLabel = 'Unidade Medida|Estoque'
      FieldKind = fkCalculated
      FieldName = 'cUnidadeMedidaEstoque'
      Size = 3
      Calculated = True
    end
    object qryProdutoEtapacUnidadeMedida: TStringField
      DisplayLabel = 'Unidade Medida|F'#243'rmula'
      FieldName = 'cUnidadeMedida'
      OnChange = qryProdutoEtapacUnidadeMedidaChange
      FixedChar = True
      Size = 3
    end
    object qryProdutoEtapanFatorConversaoUM: TBCDField
      DisplayLabel = 'Unidade Medida|Fator Conv.'
      FieldName = 'nFatorConversaoUM'
      DisplayFormat = '#,##0.000000'
      Precision = 12
      Size = 6
    end
    object qryProdutoEtapanQtdePlanejada: TBCDField
      DisplayLabel = 'Quantidades|Planejada'
      FieldName = 'nQtdePlanejada'
      Precision = 14
      Size = 6
    end
    object qryProdutoEtapanQtdeAtendida: TBCDField
      DisplayLabel = 'Quantidades|Atendida'
      FieldName = 'nQtdeAtendida'
      Precision = 14
      Size = 6
    end
    object qryProdutoEtapanCdLocalEstoque: TIntegerField
      DisplayLabel = 'Local de Estoque|C'#243'd.'
      FieldName = 'nCdLocalEstoque'
      OnChange = qryProdutoEtapanCdLocalEstoqueChange
    end
    object qryProdutoEtapacNmLocalEstoque: TStringField
      DisplayLabel = 'Local de Estoque|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmLocalEstoque'
      ReadOnly = True
      Size = 50
      Calculated = True
    end
    object qryProdutoEtapacNmTabTipoMetodoSaidaEstoque: TStringField
      DisplayLabel = 'Local de Estoque|M'#233'todo Sa'#237'da'
      FieldKind = fkCalculated
      FieldName = 'cNmTabTipoMetodoSaidaEstoque'
      Size = 50
      Calculated = True
    end
    object qryProdutoEtapaiMetodoUso: TIntegerField
      FieldName = 'iMetodoUso'
    end
    object qryProdutoEtapanCdUsuarioAltManual: TIntegerField
      DisplayLabel = 'Usu'#225'rio|Altera'#231#227'o'
      FieldName = 'nCdUsuarioAltManual'
    end
    object qryProdutoEtapacFlgAltManual: TIntegerField
      FieldName = 'cFlgAltManual'
    end
    object qryProdutoEtapacNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rio|Altera'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 50
      Calculated = True
    end
    object qryProdutoEtapanCdTabTipoMetodoSaidaEstoque: TIntegerField
      FieldName = 'nCdTabTipoMetodoSaidaEstoque'
    end
  end
  object qryProdutoFormula: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdProduto, cNmProduto, nValCusto,cUnidadeMedida, cFlgFan' +
        'tasma, nCdTipoObtencao, nCdTabTipoMetodoUso, nCdTabTipoMetodoSai' +
        'daEstoque'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 200
    Top = 216
    object qryProdutoFormulanCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutoFormulacNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutoFormulanValCusto: TBCDField
      FieldName = 'nValCusto'
      Precision = 14
      Size = 6
    end
    object qryProdutoFormulacUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryProdutoFormulacFlgFantasma: TIntegerField
      FieldName = 'cFlgFantasma'
    end
    object qryProdutoFormulanCdTipoObtencao: TIntegerField
      FieldName = 'nCdTipoObtencao'
    end
    object qryProdutoFormulanCdTabTipoMetodoUso: TIntegerField
      FieldName = 'nCdTabTipoMetodoUso'
    end
    object qryProdutoFormulanCdTabTipoMetodoSaidaEstoque: TIntegerField
      FieldName = 'nCdTabTipoMetodoSaidaEstoque'
    end
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLocalEstoque'
      '   FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK')
    Left = 284
    Top = 325
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 504
    Top = 281
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsProdutoEtapa: TDataSource
    DataSet = qryProdutoEtapa
    Left = 212
    Top = 157
  end
  object qryTabTipoMetodoSaidaEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTabTipoMetodoSaidaEstoque'
      '  FROM TabTipoMetodoSaidaEstoque'
      ' WHERE nCdTabTipoMetodoSaidaEstoque = :nPK')
    Left = 404
    Top = 213
    object qryTabTipoMetodoSaidaEstoquecNmTabTipoMetodoSaidaEstoque: TStringField
      FieldName = 'cNmTabTipoMetodoSaidaEstoque'
      Size = 50
    end
  end
end
