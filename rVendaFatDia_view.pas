unit rVendaFatDia_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptVendaFatDia_view = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand4: TQRBand;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    lblFiltro3: TQRLabel;
    QRShape2: TQRShape;
    qryTemp: TADOQuery;
    qryTempiDiasUteis: TIntegerField;
    qryTempiDiasDecorridos: TIntegerField;
    qryTempnPercDecorridos: TBCDField;
    qryTempnValMeta: TBCDField;
    qryTempnValMetaDia: TBCDField;
    qryTempnPercMetaAting_Venda: TBCDField;
    qryTempnPercMetaAting_Fat: TBCDField;
    qryTempnProjecao_Venda: TBCDField;
    qryTempnProjecao_Fat: TBCDField;
    qryTemp_Dia: TADOQuery;
    qryTemp_DiadData: TDateTimeField;
    qryTemp_DianQtdePedido: TIntegerField;
    qryTemp_DianValVenda: TBCDField;
    qryTemp_DianValFaturamento: TBCDField;
    qryTemp_DianValMeta: TBCDField;
    qryTemp_DianValDiferenca: TBCDField;
    qryTemp_DianValVendaAcum: TBCDField;
    qryTemp_DianValFaturamentoAcum: TBCDField;
    qryTemp_DianValMetaAcum: TBCDField;
    qryTemp_DianValDiferencaAcum: TBCDField;
    qryTemp_DiacFlgDiaUtil: TIntegerField;
    QRDBText8: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    qryTempnValVendaDia_AtMeta: TBCDField;
    qryTempnValFatDia_AtMeta: TBCDField;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRExpr3: TQRExpr;
    QRLabel32: TQRLabel;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel33: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape3: TQRShape;
    QRShape1: TQRShape;
    QRLabel34: TQRLabel;
    qryPreparaTemp: TADOQuery;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaFatDia_view: TrptVendaFatDia_view;

implementation

{$R *.dfm}

end.
