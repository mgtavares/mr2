inherited rptFluxoCaixaNovo: TrptFluxoCaixaNovo
  Left = -8
  Top = -8
  Width = 1168
  Height = 780
  Caption = 'Fluxo de Caixa'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1152
    Height = 718
  end
  object Label1: TLabel [1]
    Left = 35
    Top = 44
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label2: TLabel [2]
    Left = 56
    Top = 68
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label3: TLabel [3]
    Left = 16
    Top = 116
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Qt. Per'#237'odos'
  end
  object Label4: TLabel [4]
    Left = 44
    Top = 92
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Moeda'
  end
  inherited ToolBar1: TToolBar
    Width = 1152
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtEmpresa: TER2LookupMaskEdit [6]
    Left = 80
    Top = 36
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    OnExit = edtEmpresaExit
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  object edtLoja: TER2LookupMaskEdit [7]
    Left = 80
    Top = 60
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    OnExit = edtLojaExit
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  object edtMoeda: TER2LookupMaskEdit [8]
    Left = 80
    Top = 84
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 3
    Text = '         '
    CodigoLookup = 16
    QueryLookup = qryMoeda
  end
  object edtQtdPeriodos: TMaskEdit [9]
    Left = 80
    Top = 108
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 4
    Text = '10       '
  end
  object GroupBox1: TGroupBox [10]
    Left = 16
    Top = 140
    Width = 401
    Height = 185
    TabOrder = 5
    object CheckBox1: TCheckBox
      Left = 8
      Top = 9
      Width = 129
      Height = 17
      Caption = 'T'#237'tulos em Atraso'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object CheckBox2: TCheckBox
      Left = 8
      Top = 33
      Width = 129
      Height = 17
      Caption = 'T'#237'tulos a Receber'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object CheckBox3: TCheckBox
      Left = 8
      Top = 81
      Width = 113
      Height = 17
      Caption = 'T'#237'tulos a Pagar'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object CheckBox4: TCheckBox
      Left = 8
      Top = 153
      Width = 169
      Height = 17
      Caption = 'Pedido de Venda Autorizado'
      TabOrder = 6
    end
    object CheckBox5: TCheckBox
      Left = 176
      Top = 9
      Width = 185
      Height = 17
      Caption = 'Pedido de Compra Autorizado'
      TabOrder = 7
    end
    object CheckBox6: TCheckBox
      Left = 176
      Top = 33
      Width = 209
      Height = 17
      Caption = 'Exibir Saldo Banc'#225'rio Anal'#237'tico'
      Checked = True
      State = cbChecked
      TabOrder = 8
    end
    object CheckBox7: TCheckBox
      Left = 176
      Top = 57
      Width = 217
      Height = 17
      Caption = 'Exibir Movimenta'#231#227'o Anal'#237'tica A Receber'
      TabOrder = 9
    end
    object CheckBox8: TCheckBox
      Left = 176
      Top = 81
      Width = 217
      Height = 17
      Caption = 'Exibir Movimenta'#231#227'o Anal'#237'tica A Pagar'
      TabOrder = 10
    end
    object chkChequeReceber: TCheckBox
      Left = 8
      Top = 57
      Width = 129
      Height = 17
      Caption = 'Cheques a Receber'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object chkChequePagar: TCheckBox
      Left = 8
      Top = 105
      Width = 113
      Height = 17
      Caption = 'Cheques a Pagar'
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
    object chkPrevisaoPagar: TCheckBox
      Left = 8
      Top = 129
      Width = 113
      Height = 17
      Caption = 'Previs'#245'es a Pagar'
      Checked = True
      State = cbChecked
      TabOrder = 5
    end
  end
  object RadioGroup2: TRadioGroup [11]
    Left = 424
    Top = 140
    Width = 401
    Height = 48
    Caption = 'Periodicidade'
    Columns = 4
    ItemIndex = 1
    Items.Strings = (
      'Di'#225'rio'
      'Semanal'
      'Mensal'
      'Trimestral')
    TabOrder = 6
  end
  object RadioGroup3: TRadioGroup [12]
    Left = 424
    Top = 196
    Width = 401
    Height = 48
    Caption = 'Modo Exibi'#231#227'o'
    Columns = 4
    ItemIndex = 0
    Items.Strings = (
      'Relat'#243'rio'
      'Tela'
      'Planilha')
    TabOrder = 7
  end
  object DBEdit1: TDBEdit [13]
    Tag = 1
    Left = 152
    Top = 36
    Width = 393
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 8
  end
  object DBEdit2: TDBEdit [14]
    Tag = 1
    Left = 152
    Top = 60
    Width = 393
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 9
  end
  object DBEdit3: TDBEdit [15]
    Tag = 1
    Left = 152
    Top = 84
    Width = 62
    Height = 21
    DataField = 'cSigla'
    DataSource = dsMoeda
    TabOrder = 10
  end
  object DBEdit4: TDBEdit [16]
    Tag = 1
    Left = 216
    Top = 84
    Width = 329
    Height = 21
    DataField = 'cNmMoeda'
    DataSource = dsMoeda
    TabOrder = 11
  end
  object GroupBox2: TGroupBox [17]
    Left = 424
    Top = 252
    Width = 401
    Height = 73
    Caption = 'Situa'#231#227'o Cheques a Receber'
    TabOrder = 12
    object chkDepositar: TCheckBox
      Left = 8
      Top = 18
      Width = 169
      Height = 17
      Caption = 'Dispon'#237'vel para Dep'#243'sito'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object chkCustodiado: TCheckBox
      Left = 8
      Top = 42
      Width = 177
      Height = 17
      Caption = 'Depositado em Cust'#243'dia'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object chkDepositado: TCheckBox
      Left = 200
      Top = 18
      Width = 193
      Height = 17
      Caption = 'Depositado / Pend. Compensa'#231#227'o'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
  end
  object cxPageControl1: TcxPageControl [18]
    Left = 16
    Top = 332
    Width = 809
    Height = 345
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 13
    ClientRectBottom = 341
    ClientRectLeft = 4
    ClientRectRight = 805
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Composi'#231#227'o Saldo Inicial'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 801
        Height = 317
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsSaldoInicial
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdContaBancaria'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdEmpresa'
            Footers = <>
            ReadOnly = True
            Width = 56
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            ReadOnly = True
            Width = 47
          end
          item
            EditButtons = <>
            FieldName = 'nCdBanco'
            Footers = <>
            ReadOnly = True
            Width = 49
          end
          item
            EditButtons = <>
            FieldName = 'cAgencia'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nCdConta'
            Footers = <>
            ReadOnly = True
            Width = 137
          end
          item
            EditButtons = <>
            FieldName = 'cNmTitular'
            Footers = <>
            ReadOnly = True
            Width = 177
          end
          item
            EditButtons = <>
            FieldName = 'nSaldoAtual'
            Footers = <>
            Width = 127
          end
          item
            EditButtons = <>
            FieldName = 'ddDtSaldo'
            Footers = <>
            ReadOnly = True
            Width = 77
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 560
    Top = 92
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS (SELECT TOP 1 1'
      '                 FROM UsuarioEmpresa UE'
      '                WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                  AND UE.nCdUsuario = :nCdUsuario)'
      '   AND Empresa.nCdEmpresa = :nPK')
    Left = 560
    Top = 28
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE EXISTS (SELECT TOP 1 1'
      '                 FROM UsuarioLoja UL'
      '                WHERE UL.nCdLoja    = Loja.nCdLoja'
      '                  AND UL.nCdUsuario = :nCdUsuario)'
      '   AND Loja.nCdLoja = :nPK')
    Left = 600
    Top = 28
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryMoeda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMoeda'
      '      ,cSigla'
      '      ,cNmMoeda'
      '  FROM Moeda'
      ' WHERE nCdMoeda = :nPK')
    Left = 640
    Top = 28
    object qryMoedanCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMoedacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 10
    end
    object qryMoedacNmMoeda: TStringField
      FieldName = 'cNmMoeda'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 568
    Top = 60
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 608
    Top = 60
  end
  object dsMoeda: TDataSource
    DataSet = qryMoeda
    Left = 648
    Top = 60
  end
  object RDprint1: TRDprint
    ImpressoraPersonalizada.NomeImpressora = 'Modelo Personalizado - (Epson)'
    ImpressoraPersonalizada.AvancaOitavos = '27 48'
    ImpressoraPersonalizada.AvancaSextos = '27 50'
    ImpressoraPersonalizada.SaltoPagina = '12'
    ImpressoraPersonalizada.TamanhoPagina = '27 67 66'
    ImpressoraPersonalizada.Negrito = '27 69'
    ImpressoraPersonalizada.Italico = '27 52'
    ImpressoraPersonalizada.Sublinhado = '27 45 49'
    ImpressoraPersonalizada.Expandido = '27 14'
    ImpressoraPersonalizada.Normal10 = '18 27 80'
    ImpressoraPersonalizada.Comprimir12 = '18 27 77'
    ImpressoraPersonalizada.Comprimir17 = '27 80 27 15'
    ImpressoraPersonalizada.Comprimir20 = '27 77 27 15'
    ImpressoraPersonalizada.Reset = '27 80 18 20 27 53 27 70 27 45 48'
    ImpressoraPersonalizada.Inicializar = '27 64'
    OpcoesPreview.PaginaZebrada = False
    OpcoesPreview.Remalina = False
    OpcoesPreview.CaptionPreview = 'Rdprint Preview'
    OpcoesPreview.PreviewZoom = 165
    OpcoesPreview.CorPapelPreview = clWhite
    OpcoesPreview.CorLetraPreview = clBlack
    OpcoesPreview.Preview = True
    OpcoesPreview.BotaoSetup = Ativo
    OpcoesPreview.BotaoImprimir = Ativo
    OpcoesPreview.BotaoGravar = Ativo
    OpcoesPreview.BotaoLer = Ativo
    OpcoesPreview.BotaoProcurar = Ativo
    Margens.Left = 10
    Margens.Right = 10
    Margens.Top = 10
    Margens.Bottom = 10
    Autor = Deltress
    RegistroUsuario.NomeRegistro = 'EDGAR DE SOUZA'
    RegistroUsuario.SerieProduto = 'SINGLE-0708/01624'
    RegistroUsuario.AutorizacaoKey = 'BWVI-4846-SAHU-8864-PAPQ'
    About = 'RDprint 4.0e - Registrado'
    Acentuacao = Transliterate
    CaptionSetup = 'Rdprint Setup'
    TitulodoRelatorio = 'Gerado por RDprint'
    UsaGerenciadorImpr = True
    CorForm = clBtnFace
    CorFonte = clBlack
    Impressora = Epson
    Mapeamento.Strings = (
      '//--- Grafico Compativel com Windows/USB ---//'
      '//'
      'GRAFICO=GRAFICO'
      'HP=GRAFICO'
      'DESKJET=GRAFICO'
      'LASERJET=GRAFICO'
      'INKJET=GRAFICO'
      'STYLUS=GRAFICO'
      'EPL=GRAFICO'
      'USB=GRAFICO'
      '//'
      '//--- Linha Epson Matricial 9 e 24 agulhas ---//'
      '//'
      'EPSON=EPSON'
      'GENERICO=EPSON'
      'LX-300=EPSON'
      'LX-810=EPSON'
      'FX-2170=EPSON'
      'FX-1170=EPSON'
      'LQ-1170=EPSON'
      'LQ-2170=EPSON'
      'OKIDATA=EPSON'
      '//'
      '//--- Rima e Emilia ---//'
      '//'
      'RIMA=RIMA'
      'EMILIA=RIMA'
      '//'
      '//--- Linha HP/Xerox padr'#227'o PCL ---//'
      '//'
      'PCL=HP'
      '//'
      '//--- Impressoras 40 Colunas ---//'
      '//'
      'DARUMA=BOBINA'
      'SIGTRON=BOBINA'
      'SWEDA=BOBINA'
      'BEMATECH=BOBINA')
    MostrarProgresso = True
    TamanhoQteLinhas = 66
    TamanhoQteColunas = 80
    TamanhoQteLPP = Seis
    NumerodeCopias = 1
    FonteTamanhoPadrao = S10cpp
    FonteEstiloPadrao = []
    Orientacao = poPortrait
    OnNewPage = RDprint1NewPage
    Left = 840
    Top = 20
  end
  object SPREL_FLUXO_CAIXA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SPREL_FLUXO_CAIXA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdMoeda'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nPeriodos'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgAtraso'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgReceber'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgPagar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgExibMovAnaliReceber'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgExibMovAnaliPagar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgPeriodicidade'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgPedidoVendaAutorizado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgPedidoCompraAutorizado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgChequeReceber'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgChequePagar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgPrevPagar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgChkDepositar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgChkDepositado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cFlgChkCustodiado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 728
    Top = 60
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempPosCaixaAtual'#39') IS NULL)'#13#10'    BEGIN'#13 +
      #10' '#13#10'        CREATE TABLE #TempPosCaixaAtual (cEmpresaLoja    var' +
      'char(7)'#13#10'                                        ,cBanco        ' +
      '  varchar(54)'#13#10'                                        ,cAgencia' +
      '        varchar(4)'#13#10'                                        ,nCd' +
      'Conta        char(15)'#13#10'                                        ,' +
      'nSaldoConta     decimal(12,2) default 0 not null'#13#10'              ' +
      '                          ,dDtSaldo        datetime'#13#10'           ' +
      '                             ,nValLimite      decimal(12,2) defa' +
      'ult 0 not null'#13#10'                                        ,nSaldoT' +
      'otal     decimal(12,2) default 0 not null'#13#10'                     ' +
      '                   ,nValLimiteTotal decimal(12,2) default 0 not ' +
      'null)'#13#10#13#10'    END'#13#10#13#10'    IF (OBJECT_ID('#39'tempdb..#TempDemonstraMov' +
      'Futura'#39') IS NULL)'#13#10'    BEGIN'#13#10#13#10'        CREATE TABLE #TempDemons' +
      'traMovFutura (dDtInicial              datetime'#13#10'                ' +
      '                             ,dDtFinal                datetime'#13#10 +
      '                                             ,nValTotalReceber  ' +
      '      decimal(12,2) default 0 not null'#13#10'                        ' +
      '                     ,nValTotalPagar          decimal(12,2) defa' +
      'ult 0 not null'#13#10'                                             ,nV' +
      'alTotalReceberPeriodo decimal(12,2) default 0 not null'#13#10'        ' +
      '                                     ,nValTotalPagarPeriodo   de' +
      'cimal(12,2) default 0 not null'#13#10'                                ' +
      '             ,nValSaldoAcumulado      decimal(12,2) default 0 no' +
      't null)'#13#10#13#10'    END'#13#10'   '#13#10'    IF (OBJECT_ID('#39'tempdb..#TempExpecta' +
      'tivaCaixa'#39') IS NULL)'#13#10'    BEGIN'#13#10'  '#13#10'          CREATE TABLE #Tem' +
      'pExpectativaCaixa (nValTotalAtrasReceber decimal(12,2) default 0' +
      ' not null'#13#10'                                           ,nValTotal' +
      'AtrasPagar   decimal(12,2) default 0 not null'#13#10'                 ' +
      '                          ,nValDispTotal         decimal(12,2) d' +
      'efault 0 not null'#13#10'                                           ,n' +
      'ValDispImediata      decimal(12,2) default 0 not null'#13#10'         ' +
      '                                  ,nValTotalReceber      decimal' +
      '(12,2) default 0 not null'#13#10'                                     ' +
      '      ,nValTotalPagar        decimal(12,2) default 0 not null'#13#10' ' +
      '                                          ,nValSaldoSemLimCred  ' +
      ' decimal(12,2) default 0 not null'#13#10'                             ' +
      '              ,nValLimiteCred        decimal(12,2) default 0 not' +
      ' null'#13#10'                                           ,nSaldoTotal  ' +
      '         decimal(12,2) default 0 not null'#13#10'                     ' +
      '                      ,nValChequeDepositar   decimal(12,2) defau' +
      'lt 0 not null'#13#10'                                           ,nValC' +
      'hequeDepositado  decimal(12,2) default 0 not null'#13#10'             ' +
      '                              ,nValChequeCustodia    decimal(12,' +
      '2) default 0 not null)'#13#10#13#10'    END'#13#10#13#10'    IF (OBJECT_ID('#39'tempdb..' +
      '#TempMovAnalitico'#39') IS NULL)'#13#10'    BEGIN'#13#10#13#10'        CREATE TABLE ' +
      '#TempMovAnalitico (dDtInicial       datetime'#13#10'                  ' +
      '                     ,dDtFinal         datetime'#13#10'               ' +
      '                        ,iPeriodo         int'#13#10'                 ' +
      '                      ,dDtVenc          datetime'#13#10'              ' +
      '                         ,nCdTitulo        int'#13#10'                ' +
      '                       ,cNrTit           char(17)'#13#10'             ' +
      '                          ,cNmEspTit        varchar(50)'#13#10'       ' +
      '                                ,cNmTerceiro      varchar(50)'#13#10' ' +
      '                                      ,cNmCategFinanc   varchar(' +
      '50)'#13#10'                                       ,nSaldoTit        de' +
      'cimal(12,2) default 0 not null '#13#10'                               ' +
      '        ,nValTotalPagar   decimal(12,2) default 0 not null'#13#10'    ' +
      '                                   ,nValTotalReceber decimal(12,' +
      '2) default 0 not null'#13#10'                                       ,c' +
      'Senso           char(1))'#13#10#13#10'    END'#13#10#13#10'IF (OBJECT_ID('#39'tempdb..#T' +
      'empSaldoInicialFluxoCaixa'#39') IS NULL)'#13#10'BEGIN'#13#10#13#10'    CREATE TABLE ' +
      '#TempSaldoInicialFluxoCaixa (nCdContaBancaria int'#13#10'             ' +
      '                                ,nCdEmpresa       varchar(3)'#13#10'  ' +
      '                                           ,nCdLoja          var' +
      'char(3)'#13#10'                                             ,nCdBanco ' +
      '        varchar(3)'#13#10'                                            ' +
      ' ,cAgencia         int'#13#10'                                        ' +
      '     ,nCdConta         char(15)'#13#10'                               ' +
      '              ,cNmTitular       varchar(50)'#13#10'                   ' +
      '                          ,nSaldoAtual      decimal(12,2) defaul' +
      't 0 not null'#13#10'                                             ,ddDt' +
      'Saldo        datetime)'#13#10#13#10'END'#13#10
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 696
    Top = 60
  end
  object qryPosCaixaAtual: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      '    SELECT *'
      '      FROM #TempPosCaixaAtual')
    Left = 696
    Top = 28
    object qryPosCaixaAtualcEmpresaLoja: TStringField
      FieldName = 'cEmpresaLoja'
      Size = 7
    end
    object qryPosCaixaAtualcBanco: TStringField
      FieldName = 'cBanco'
      Size = 54
    end
    object qryPosCaixaAtualcAgencia: TStringField
      FieldName = 'cAgencia'
      Size = 4
    end
    object qryPosCaixaAtualnCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryPosCaixaAtualnSaldoConta: TBCDField
      FieldName = 'nSaldoConta'
      Precision = 12
      Size = 2
    end
    object qryPosCaixaAtualdDtSaldo: TDateTimeField
      FieldName = 'dDtSaldo'
    end
    object qryPosCaixaAtualnValLimite: TBCDField
      FieldName = 'nValLimite'
      Precision = 12
      Size = 2
    end
    object qryPosCaixaAtualnSaldoTotal: TBCDField
      FieldName = 'nSaldoTotal'
      Precision = 12
      Size = 2
    end
    object qryPosCaixaAtualnValLimiteTotal: TBCDField
      FieldName = 'nValLimiteTotal'
      Precision = 12
      Size = 2
    end
  end
  object qryDemonstraMovFutura: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      '    SELECT *'
      '      FROM #TempDemonstraMovFutura')
    Left = 728
    Top = 28
    object qryDemonstraMovFuturadDtInicial: TDateTimeField
      FieldName = 'dDtInicial'
    end
    object qryDemonstraMovFuturadDtFinal: TDateTimeField
      FieldName = 'dDtFinal'
    end
    object qryDemonstraMovFuturanValTotalReceber: TBCDField
      FieldName = 'nValTotalReceber'
      Precision = 12
      Size = 2
    end
    object qryDemonstraMovFuturanValTotalPagar: TBCDField
      FieldName = 'nValTotalPagar'
      Precision = 12
      Size = 2
    end
    object qryDemonstraMovFuturanValSaldoAcumulado: TBCDField
      FieldName = 'nValSaldoAcumulado'
      Precision = 12
      Size = 2
    end
    object qryDemonstraMovFuturanValTotalReceberPeriodo: TBCDField
      FieldName = 'nValTotalReceberPeriodo'
      Precision = 12
      Size = 2
    end
    object qryDemonstraMovFuturanValTotalPagarPeriodo: TBCDField
      FieldName = 'nValTotalPagarPeriodo'
      Precision = 12
      Size = 2
    end
  end
  object qryExpectativaCaixa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      '    SELECT *'
      '      FROM #TempExpectativaCaixa')
    Left = 760
    Top = 28
    object qryExpectativaCaixanValTotalAtrasReceber: TBCDField
      FieldName = 'nValTotalAtrasReceber'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValTotalAtrasPagar: TBCDField
      FieldName = 'nValTotalAtrasPagar'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValDispTotal: TBCDField
      FieldName = 'nValDispTotal'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValDispImediata: TBCDField
      FieldName = 'nValDispImediata'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValTotalReceber: TBCDField
      FieldName = 'nValTotalReceber'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValTotalPagar: TBCDField
      FieldName = 'nValTotalPagar'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValSaldoSemLimCred: TBCDField
      FieldName = 'nValSaldoSemLimCred'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValLimiteCred: TBCDField
      FieldName = 'nValLimiteCred'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanSaldoTotal: TBCDField
      FieldName = 'nSaldoTotal'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValChequeDepositar: TBCDField
      FieldName = 'nValChequeDepositar'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValChequeDepositado: TBCDField
      FieldName = 'nValChequeDepositado'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValChequeCustodia: TBCDField
      FieldName = 'nValChequeCustodia'
      Precision = 12
      Size = 2
    end
  end
  object qryMovAnalitico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      '    SELECT *'
      '      FROM #TempMovAnalitico'
      '     ORDER BY iPeriodo,dDtVenc')
    Left = 792
    Top = 28
    object qryMovAnaliticodDtInicial: TDateTimeField
      FieldName = 'dDtInicial'
    end
    object qryMovAnaliticodDtFinal: TDateTimeField
      FieldName = 'dDtFinal'
    end
    object qryMovAnaliticoiPeriodo: TIntegerField
      FieldName = 'iPeriodo'
    end
    object qryMovAnaliticodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryMovAnaliticonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryMovAnaliticocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryMovAnaliticocNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryMovAnaliticocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMovAnaliticocNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object qryMovAnaliticonSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryMovAnaliticonValTotalPagar: TBCDField
      FieldName = 'nValTotalPagar'
      Precision = 12
      Size = 2
    end
    object qryMovAnaliticonValTotalReceber: TBCDField
      FieldName = 'nValTotalReceber'
      Precision = 12
      Size = 2
    end
    object qryMovAnaliticocSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
  end
  object ER2Excel1: TER2Excel
    Titulo = 'Fluxo de Caixa'
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    OnBeforeExport = ER2Excel1BeforeExport
    OnAfterExport = ER2Excel1AfterExport
    Left = 888
    Top = 32
  end
  object qryCotacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMoeda'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdMoeda int'
      ''
      'SET @nCdMoeda = :nCdMoeda'
      ''
      'SELECT nCdMoeda'
      '      ,dbo.fn_OnlyDate(dDtCotacao) dDtCotacao'
      '      ,nValCotacao'
      '  FROM CotacaoMoeda'
      ' WHERE nCdMoeda   = @nCdMoeda'
      '   AND dDtCotacao = (SELECT MAX(dDtCotacao)'
      '                       FROM CotacaoMoeda'
      '                      WHERE nCdMoeda = @nCdMoeda)')
    Left = 760
    Top = 60
    object qryCotacaonCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryCotacaodDtCotacao: TDateTimeField
      FieldName = 'dDtCotacao'
      ReadOnly = True
    end
    object qryCotacaonValCotacao: TBCDField
      FieldName = 'nValCotacao'
      Precision = 12
    end
  end
  object qrySaldoInicial: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempSaldoInicialFluxoCaixa'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempSaldoInicialFluxoCaixa (nCdContaBancaria i' +
        'nt'
      
        '                                             ,nCdEmpresa       v' +
        'archar(3)'
      
        '                                             ,nCdLoja          v' +
        'archar(3)'
      
        '                                             ,nCdBanco         v' +
        'archar(3)'
      
        '                                             ,cAgencia         i' +
        'nt'
      
        '                                             ,nCdConta         c' +
        'har(15)'
      
        '                                             ,cNmTitular       v' +
        'archar(50)'
      
        '                                             ,nSaldoAtual      d' +
        'ecimal(12,2) default 0 not null'
      
        '                                             ,ddDtSaldo        d' +
        'atetime)'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #TempSaldoInicialFluxoCaixa'
      ' ORDER BY nCdEmpresa, nCdLoja, nCdBanco, cAgencia, nCdConta')
    Left = 800
    Top = 100
    object qrySaldoInicialnCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qrySaldoInicialnCdEmpresa: TStringField
      DisplayLabel = 'Contas Composi'#231#227'o Saldo|Empresa'
      FieldName = 'nCdEmpresa'
      Size = 3
    end
    object qrySaldoInicialnCdLoja: TStringField
      DisplayLabel = 'Contas Composi'#231#227'o Saldo|Loja'
      FieldName = 'nCdLoja'
      Size = 3
    end
    object qrySaldoInicialnCdBanco: TStringField
      DisplayLabel = 'Contas Composi'#231#227'o Saldo|Banco'
      FieldName = 'nCdBanco'
      Size = 3
    end
    object qrySaldoInicialcAgencia: TIntegerField
      DisplayLabel = 'Contas Composi'#231#227'o Saldo|Ag'#234'ncia'
      FieldName = 'cAgencia'
    end
    object qrySaldoInicialnCdConta: TStringField
      DisplayLabel = 'Contas Composi'#231#227'o Saldo|Conta'
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qrySaldoInicialcNmTitular: TStringField
      DisplayLabel = 'Contas Composi'#231#227'o Saldo|Titular'
      FieldName = 'cNmTitular'
      Size = 50
    end
    object qrySaldoInicialnSaldoAtual: TBCDField
      DisplayLabel = 'Contas Composi'#231#227'o Saldo|Saldo Atual'
      FieldName = 'nSaldoAtual'
      Precision = 12
      Size = 2
    end
    object qrySaldoInicialddDtSaldo: TDateTimeField
      DisplayLabel = 'Contas Composi'#231#227'o Saldo|Data Saldo'
      FieldName = 'ddDtSaldo'
    end
  end
  object dsSaldoInicial: TDataSource
    DataSet = qrySaldoInicial
    Left = 840
    Top = 100
  end
  object qryPopulaSaldoInicial: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa int'
      '       ,@nCdLoja    int'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempSaldoInicialFluxoCaixa'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempSaldoInicialFluxoCaixa (nCdContaBancaria i' +
        'nt'
      
        '                                             ,nCdEmpresa       v' +
        'archar(3)'
      
        '                                             ,nCdLoja          v' +
        'archar(3)'
      
        '                                             ,nCdBanco         v' +
        'archar(3)'
      
        '                                             ,cAgencia         i' +
        'nt'
      
        '                                             ,nCdConta         c' +
        'har(15)'
      
        '                                             ,cNmTitular       v' +
        'archar(50)'
      
        '                                             ,nSaldoAtual      d' +
        'ecimal(12,2) default 0 not null'
      
        '                                             ,ddDtSaldo        d' +
        'atetime)'
      ''
      'END'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      'Set @nCdLoja    = :nCdLoja'
      ''
      'DELETE'
      '  FROM #TempSaldoInicialFluxoCaixa'
      ''
      'INSERT INTO #TempSaldoInicialFluxoCaixa (nCdContaBancaria'
      '                                        ,nCdEmpresa'
      '                                        ,nCdLoja   '
      '                                        ,nCdBanco  '
      '                                        ,cAgencia  '
      '                                        ,nCdConta  '
      '                                        ,cNmTitular'
      '                                        ,nSaldoAtual'
      '                                        ,ddDtSaldo)'
      '                                  SELECT nCdContaBancaria'
      
        '                                        ,dbo.fn_ZeroEsquerda(Con' +
        'taBancaria.nCdEmpresa,3)'
      
        '                                        ,dbo.fn_ZeroEsquerda(isN' +
        'ull(ContaBancaria.nCdLoja,0),3)'
      
        '                                        ,dbo.fn_ZeroEsquerda(Con' +
        'taBancaria.nCdBanco,3)'
      '                                        ,ContaBancaria.cAgencia'
      '                                        ,ContaBancaria.nCdConta'
      
        '                                        ,ContaBancaria.cNmTitula' +
        'r'
      
        '                                        ,ContaBancaria.nSaldoCon' +
        'ta'
      
        '                                        ,ContaBancaria.dDtUltCon' +
        'ciliacao'
      '                                    FROM ContaBancaria '
      
        '                                   WHERE ((nCdEmpresa = @nCdEmpr' +
        'esa) OR (@nCdEmpresa = 0))'
      
        '                                     AND ((@nCdLoja = 0) OR (nCd' +
        'Loja = @nCdLoja))'
      '                                     AND cFlgFluxo  = 1')
    Left = 832
    Top = 156
  end
end
