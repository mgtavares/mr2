unit fTabPreco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmTabPreco = class(TfrmCadastro_Padrao)
    qryMasternCdTabPreco: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasterdValidadeIni: TDateTimeField;
    qryMasterdValidadeFim: TDateTimeField;
    qryMastercNmTabela: TStringField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    dsEmpresa: TDataSource;
    DBEdit9: TDBEdit;
    dsLoja: TDataSource;
    DBEdit10: TDBEdit;
    dsTerceiro: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryProdutoTabPreco: TADOQuery;
    dsProdutoTabPreco: TDataSource;
    qryProdutoTabPreconCdTabPreco: TIntegerField;
    qryProdutoTabPreconCdProduto: TIntegerField;
    qryProdutoTabPreconValor: TBCDField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryProdutoTabPrecocNmProduto: TStringField;
    qryCondPagtoTabPreco: TADOQuery;
    dsCondPagtoTabPreco: TDataSource;
    qryCondPagtoTabPreconCdTabPreco: TIntegerField;
    qryCondPagtoTabPreconCdCondPagto: TIntegerField;
    qryCondPagtoTabPreconPercAcrescimo: TBCDField;
    qryCondPagtoTabPrecocNmCondPagto: TStringField;
    DBGridEh2: TDBGridEh;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    Label8: TLabel;
    qryAux: TADOQuery;
    qryMasternCdTipoTabPreco: TIntegerField;
    Label9: TLabel;
    DBEdit11: TDBEdit;
    qryTipoTabPreco: TADOQuery;
    qryTipoTabPreconCdTipoTabPreco: TIntegerField;
    qryTipoTabPrecocNmTipoTabPreco: TStringField;
    DBEdit12: TDBEdit;
    dsTipoTabPreco: TDataSource;
    qryCondPagtoTabPreconCdLinha: TIntegerField;
    qryCondPagtoTabPreconCdMarca: TIntegerField;
    qryCondPagtoTabPrecocNmMarca: TStringField;
    qryCondPagtoTabPrecocNmLinha: TStringField;
    qryMarca: TADOQuery;
    qryLinha: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    ToolButton3: TToolButton;
    btnImprimir: TToolButton;
    procedure DBEdit3Enter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryProdutoTabPrecoBeforePost(DataSet: TDataSet);
    procedure qryCondPagtoTabPrecoBeforePost(DataSet: TDataSet);
    procedure qryProdutoTabPrecoCalcFields(DataSet: TDataSet);
    procedure qryCondPagtoTabPrecoCalcFields(DataSet: TDataSet);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSalvarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1Enter(Sender: TObject);
    procedure cxGrid1DBTableView1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit11Exit(Sender: TObject);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnImprimirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTabPreco: TfrmTabPreco;

implementation

uses fLookup_Padrao, fMenu, rTabelaPreco;

{$R *.dfm}

procedure TfrmTabPreco.DBEdit3Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (((qryMasternCdEmpresa.Value = 0) or (dbEdit8.Text = '')) and (not DBEdit3.ReadOnly)) then
  begin
      MensagemAlerta('Selecione uma empresa.') ;
      DbEdit2.SetFocus;
      exit ;
  end ;

end;

procedure TfrmTabPreco.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TABPRECO' ;
  nCdTabelaSistema  := 51 ;
  nCdConsultaPadrao := 105 ;

end;

procedure TfrmTabPreco.qryProdutoTabPrecoBeforePost(DataSet: TDataSet);
begin

  if (qryProdutoTabPreconValor.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor do produto.') ;
      abort ;
  end ;

  inherited;

  qryProdutoTabPreconCdTabPreco.Value := qryMasternCdTabPreco.Value ;
  
end;

procedure TfrmTabPreco.qryCondPagtoTabPrecoBeforePost(DataSet: TDataSet);
begin

  if (qryCondPagtoTabPreconCdMarca.Value > 0) and (qryCondPagtoTabPrecocNmMarca.Value = '') then
  begin
      MensagemAlerta('Marca inv�lida.') ;
      abort ;
  end ;

  if (qryCondPagtoTabPreconCdLinha.Value > 0) and (qryCondPagtoTabPrecocNmMarca.Value = '') then
  begin
      MensagemAlerta('Selecione uma marca.') ;
      abort ;
  end ;

  if (qryCondPagtoTabPreconCdLinha.Value > 0) and (qryCondPagtoTabPrecocNmLinha.Value = '') then
  begin
      MensagemAlerta('Linha inv�lida.') ;
      abort ;
  end ;

  inherited;

  qryCondPagtoTabPreconCdTabPreco.Value := qryMasternCdTabPreco.Value ;

end;

procedure TfrmTabPreco.qryProdutoTabPrecoCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryProduto.Close ;

  PosicionaQuery(qryProduto, qryProdutoTabPreconCdProduto.asString) ;

  if not qryProduto.eof then
      qryProdutoTabPrecocNmProduto.Value := qryProdutocNmProduto.Value ;

end;

procedure TfrmTabPreco.qryCondPagtoTabPrecoCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryCondPagto.Close ;
  PosicionaQuery(qryCondPagto, qryCondPagtoTabPreconCdCondPagto.AsString) ;

  if not qryCondPagto.Eof then
      qryCondPagtoTabPrecocNmCondPagto.Value := qryCondPagtocNmCondPagto.Value ;

  qryMarca.Close;
  PosicionaQuery(qryMarca, qryCondPagtoTabPreconCdMarca.AsString) ;

  if not qryMarca.Eof then
      qryCondPagtoTabPrecocNmMarca.Value := qryMarcacNmMarca.Value ;

  qryLinha.Close;
  qryLinha.Parameters.ParamByName('nCdMarca').Value := qryCondPagtoTabPreconCdMarca.Value;
  PosicionaQuery(qryLinha, qryCondPagtoTabPreconCdLinha.AsString) ;

  if not qryLinha.eof then
      qryCondPagtoTabPrecocNmLinha.Value := qryLinhacNmLinha.Value ;

      
end;

procedure TfrmTabPreco.DBEdit2Exit(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  PosicionaQuery(qryEmpresa, DBEdit2.Text) ;

  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := 0 ;

  if not qryEmpresa.Eof then
      qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;
end;

procedure TfrmTabPreco.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, DBEdit3.Text) ;

end;

procedure TfrmTabPreco.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DBEdit4.Text) ;

end;

procedure TfrmTabPreco.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTabPreco.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(86);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTabPreco.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(101);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTabPreco.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryProdutoTabPreco.State = dsBrowse) then
             qryProdutoTabPreco.Edit ;

        if (qryProdutoTabPreco.State = dsInsert) or (qryProdutoTabPreco.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(106);

            If (nPK > 0) then
            begin
                qryProdutoTabPreconCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTabPreco.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTerceiro.Close ;
  qryProdutoTabPreco.Close ;
  qryCondPagtoTabPreco.Close ;
  qryTipoTabPreco.Close ;

end;

procedure TfrmTabPreco.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;
      
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;

  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;

  PosicionaQuery(qryTipoTabPreco, qryMasternCdTipoTabPreco.AsString) ;
  PosicionaQuery(qryLoja, qryMasternCdLoja.asString) ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  PosicionaQuery(qryProdutoTabPreco, qryMasternCdTabPreco.asString) ;
  PosicionaQuery(qryCondPagtoTabPreco, qryMasternCdTabPreco.asString) ;

end;

procedure TfrmTabPreco.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (qryMasternCdEmpresa.Value = 0) or (DbEdit8.Text = '') then
  begin
      MensagemAlerta('Informe a empresa.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdLoja.Value > 0) and (DbEdit9.Text = '') then
  begin
      MensagemAlerta('Preencha uma loja v�lida ou deixe em branco.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;

  if (DBEdit12.Text = '') and (DBEdit10.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de tabela de pre�o ou o cliente.') ;
      DbEdit11.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTerceiro.Value > 0) and (DbEdit10.Text = '') then
  begin
      MensagemAlerta('Preencha um cliente v�lido ou deixe em branco.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMasterdValidadeIni.Value > qryMasterdValidadeFim.Value) then
  begin
      MensagemAlerta('Per�odo de validade inv�lido.') ;
      DbEdit5.SetFocus;
      abort ;
  end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT nCdTabPreco ') ;
  qryAux.SQL.Add('  FROM TabPreco    ') ;
  qryAux.SQL.Add(' WHERE nCdEmpresa = ' + qryMasternCdEmpresa.asString) ;

  if (qryMaster.State <> dsInsert) then
      qryAux.SQL.Add(' AND nCdTabPreco <> ' + qryMasternCdTabPreco.AsString) ;

  if (qryMasternCdLoja.Value > 0) then
      qryAux.SQL.Add('   AND nCdLoja    = ' + qryMasternCdLoja.AsString)
  else qryAux.SQL.Add('   AND nCdLoja IS NULL') ;

  if (qryMasternCdTerceiro.Value > 0) then
      qryAux.SQL.Add('   AND nCdTerceiro  = ' + qryMasternCdTerceiro.AsString)
  else qryAux.SQL.Add('   AND nCdTerceiro IS NULL') ;

  if (qryMasternCdTipoTabPreco.asString <> '') then
      qryAux.SQL.Add(' AND nCdTipoTabPreco = ' + qryMasternCdTipoTabPreco.asString)
  else qryAux.SQL.Add(' AND nCdTipoTabPreco IS NULL') ;

  if (qryMasterdValidadeIni.asString <> '') then
  begin

      qryAux.SQL.Add(' AND Convert(DATETIME,' + Chr(39) + qryMasterdValidadeIni.AsString + Chr(39) + ',103) BETWEEN dValidadeIni AND dValidadeFim') ;

      qryAux.Open ;

      if not qryAux.Eof then
      begin
          MensagemAlerta('J� existe uma tabela com essa caracter�stica.' +#13#13+'C�digo Tabela: ' + qryAux.FieldList[0].asString) ;
          DBEdit2.SetFocus ;
          abort ;
      end ;

      qryAux.Close ;

  end ;

  if (qryMasterdValidadeIni.asString = '') then
  begin
      qryAux.SQL.Add('AND dValidadeIni IS NULL') ;
      qryAux.SQL.Add('AND dValidadeFim IS NULL') ;

      qryAux.Open ;

      if not qryAux.Eof then
      begin
          MensagemAlerta('J� existe uma tabela com essa caracter�stica.' +#13#13+'C�digo Tabela: ' + qryAux.FieldList[0].asString) ;
          qryAux.Close ;
          DBEdit2.SetFocus ;
          abort ;
      end ;

  end ;

  qryAux.Close ;

  inherited;

end;

procedure TfrmTabPreco.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;
      
end;

procedure TfrmTabPreco.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryProdutoTabPreco, qryMasternCdTabPreco.asString) ;
  PosicionaQuery(qryCondPagtoTabPreco, qryMasternCdTabPreco.asString) ;

end;

procedure TfrmTabPreco.btIncluirClick(Sender: TObject);
begin
  inherited;
  DbEdit2.SetFocus ;

  cxPageControl1.ActivePageIndex := 0 ;
  
end;

procedure TfrmTabPreco.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryCondPagtoTabPreco.State = dsBrowse) then
             qryCondPagtoTabPreco.Edit ;

        if (qryCondPagtoTabPreco.State = dsInsert) or (qryCondPagtoTabPreco.State = dsEdit) then
        begin

            {-- condi��o de pagamento --}
            if (DBGridEh2.Col = 2) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(61);

                If (nPK > 0) then
                    qryCondPagtoTabPreconCdCondPagto.Value := nPK ;
            end ;

            {-- marca --}
            if (DBGridEh2.Col = 5) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(49);

                If (nPK > 0) then
                    qryCondPagtoTabPreconCdMarca.Value := nPK ;
            end ;

            {-- linha --}
            if (DBGridEh2.Col = 7) then
            begin
                if (qryCondPagtoTabPrecocNmMarca.Value = '') then
                begin
                    MensagemAlerta('Selecione uma marca.') ;
                    abort ;
                end ;

                nPK := frmLookup_Padrao.ExecutaConsulta2(50,'Linha.nCdMarca = ' + qryCondPagtoTabPreconCdMarca.AsString);

                If (nPK > 0) then
                    qryCondPagtoTabPreconCdLinha.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTabPreco.btSalvarClick(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTerceiro.Close ;
  qryProdutoTabPreco.Close ;
  qryCondPagtoTabPreco.Close ;
  qryTipoTabPreco.Close ;
  
end;

procedure TfrmTabPreco.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      desativaDBEdit( DbEdit3 ) ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmTabPreco.cxGrid1Enter(Sender: TObject);
begin
  inherited;
  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

end;

procedure TfrmTabPreco.cxGrid1DBTableView1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryProdutoTabPreco.State = dsBrowse) then
             qryProdutoTabPreco.Edit ;

        if (qryProdutoTabPreco.State = dsInsert) or (qryProdutoTabPreco.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(106);

            If (nPK > 0) then
            begin
                qryProdutoTabPreconCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTabPreco.DBEdit11Exit(Sender: TObject);
begin
  inherited;

  qryTipoTabPreco.Close ;
  PosicionaQuery(qryTipoTabPreco, DBEdit11.Text) ;

end;

procedure TfrmTabPreco.DBEdit11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(192);

            If (nPK > 0) then
            begin
                qryMasternCdTipoTabPreco.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTabPreco.btnImprimirClick(Sender: TObject);
var
    objRel : TrelTabelaPreco;
begin
    inherited;

    if (qryMaster.IsEmpty) then
        Exit;

    objRel := TrelTabelaPreco.Create(nil);

    objRel.exibeRel(qryMasternCdTabPreco.Value);

    FreeAndNil(objRel);
end;

initialization
    RegisterClass(TfrmTabPreco) ;

end.
