unit rItensPreDistribuicao_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, DB, ADODB, QRCtrls;

type
  TrelItensPreDistribuicao_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData3: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape1: TQRShape;
    QRLabel11: TQRLabel;
    QRShape3: TQRShape;
    QRBand3: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText1: TQRDBText;
    QRBand5: TQRBand;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    QRGroup1: TQRGroup;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRLabel4: TQRLabel;
    QRShape2: TQRShape;
    qryResultado: TADOQuery;
    qryResultadonCdItemPedido: TIntegerField;
    qryResultadonCdLoja: TIntegerField;
    qryResultadocNmLoja: TStringField;
    qryResultadonCdProdutoPai: TIntegerField;
    qryResultadocNmProduto: TStringField;
    qryResultadonCdProduto: TIntegerField;
    qryResultadocNmItem: TStringField;
    qryResultadonQtdeDistrib: TIntegerField;
    QRDBText7: TQRDBText;
    QRLabel6: TQRLabel;
    QRShape4: TQRShape;
    procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdLojaAux : integer;
    nCdLoja    : integer;
  end;

var
  relItensPreDistribuicao_view: TrelItensPreDistribuicao_view;

implementation

{$R *.dfm}

procedure TrelItensPreDistribuicao_view.QRGroup1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin

   if (nCdLojaAux = 0) then
       nCdLojaAux := qryResultadonCdLoja.Value;

   if QRGroup1.ForceNewPage = true then
       QRGroup1.ForceNewPage := false;

   if  nCdLojaAux <> qryResultadonCdLoja.Value then
   begin
       QRGroup1.ForceNewPage := True;
       nCdLojaAux := qryResultadonCdLoja.Value;
   end;

end;

end.
