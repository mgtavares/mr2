inherited frmConsultaPedComAberTerceiro_Itens: TfrmConsultaPedComAberTerceiro_Itens
  Left = 27
  Top = 152
  Width = 1033
  BorderIcons = [biSystemMenu]
  Caption = 'Itens'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1017
  end
  inherited ToolBar1: TToolBar
    Width = 1017
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid4: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1017
    Height = 435
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGridDBTableView3: TcxGridDBTableView
      DataController.DataSource = dsItens
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Header = frmMenu.Header
      object cxGridDBTableView3nCdItemPedido: TcxGridDBColumn
        Caption = 'Item'
        DataBinding.FieldName = 'nCdItemPedido'
        Width = 84
      end
      object cxGridDBTableView3cNmTipoItemPed: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'cNmTipoItemPed'
        Width = 118
      end
      object cxGridDBTableView3nCdProduto: TcxGridDBColumn
        Caption = 'C'#243'd.'
        DataBinding.FieldName = 'nCdProduto'
      end
      object cxGridDBTableView3cNmItem: TcxGridDBColumn
        Caption = 'Descri'#231#227'o do Produto'
        DataBinding.FieldName = 'cNmItem'
        Width = 418
      end
      object cxGridDBTableView3cSiglaUnidadeMedida: TcxGridDBColumn
        Caption = 'UM'
        DataBinding.FieldName = 'cSiglaUnidadeMedida'
        Width = 42
      end
      object cxGridDBTableView3nQtde: TcxGridDBColumn
        Caption = 'Saldo em Aberto'
        DataBinding.FieldName = 'nQtde'
        Width = 57
      end
      object cxGridDBTableView3dDtEntregaIni: TcxGridDBColumn
        Caption = 'Prev. Entrega Inicial'
        DataBinding.FieldName = 'dDtEntregaIni'
      end
      object cxGridDBTableView3dDtEntregaFim: TcxGridDBColumn
        Caption = 'Prev. Entrega Final'
        DataBinding.FieldName = 'dDtEntregaFim'
      end
    end
    object cxGridLevel3: TcxGridLevel
      GridView = cxGridDBTableView3
    end
  end
  inherited ImageList1: TImageList
    Left = 528
    Top = 184
  end
  object qryItens: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdItemPedido'
      '      ,cNmTipoItemPed'
      '      ,ItemPedido.nCdProduto'
      '      ,CASE WHEN cNmItem IS NULL THEN cNmProduto'
      '            ELSE cNmItem'
      '       END  cNmItem'
      '      ,(nQtdePed - nQtdeExpRec - nQtdeCanc) nQtde'
      
        '      ,CASE WHEN dDtEntregaIni IS NULL THEN (SELECT dDtPrevEntIn' +
        'i FROM Pedido WHERE Pedido.nCdPedido = ItemPedido.nCdPedido)'
      '            ELSE dDtEntregaIni'
      '       END dDtEntregaIni'
      
        '      ,CASE WHEN dDtEntregaFim IS NULL THEN (SELECT dDtPrevEntFi' +
        'm FROM Pedido WHERE Pedido.nCdPedido = ItemPedido.nCdPedido)'
      '            ELSE dDtEntregaFim'
      '       END dDtEntregaFim'
      '      ,cSiglaUnidadeMedida'
      '  FROM ItemPedido'
      
        '       INNER JOIN TipoItemPed ON TipoItemPed.nCdTipoItemPed = It' +
        'emPedido.nCdTipoItemPed'
      
        '       LEFT  JOIN Produto     ON Produto.nCdProduto         = It' +
        'emPedido.nCdProduto'
      ' WHERE (nQtdePed - nQtdeExpRec - nQtdeCanc)  > 0'
      '   AND nCdItemPedidoPai                     IS NULL'
      '   AND nCdPedido                             = :nPK'
      '')
    Left = 376
    Top = 208
    object qryItensnCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryItenscNmTipoItemPed: TStringField
      DisplayLabel = 'Tipo|Item'
      FieldName = 'cNmTipoItemPed'
      Size = 50
    end
    object qryItensnCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryItenscNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o do Produto'
      FieldName = 'cNmItem'
      ReadOnly = True
      Size = 150
    end
    object qryItensnQtde: TBCDField
      DisplayLabel = 'Dados da Previs'#227'o de Entrega|Quantidade'
      FieldName = 'nQtde'
      ReadOnly = True
      Precision = 14
    end
    object qryItensdDtEntregaIni: TDateTimeField
      DisplayLabel = 'Dados da Previs'#227'o de Entrega|Data Inicial'
      FieldName = 'dDtEntregaIni'
    end
    object qryItensdDtEntregaFim: TDateTimeField
      DisplayLabel = 'Dados da Previs'#227'o de Entrega|Data Final'
      FieldName = 'dDtEntregaFim'
    end
    object qryItenscSiglaUnidadeMedida: TStringField
      DisplayLabel = 'Dados da Previs'#227'o de Entrega|UM'
      DisplayWidth = 3
      FieldName = 'cSiglaUnidadeMedida'
      FixedChar = True
      Size = 3
    end
  end
  object dsItens: TDataSource
    DataSet = qryItens
    Left = 416
    Top = 208
  end
end
