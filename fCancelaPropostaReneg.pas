unit fCancelaPropostaReneg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, Menus, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmCancelaPropostaReneg = class(TfrmProcesso_Padrao)
    qryMaster: TADOQuery;
    qryMasternCdPropostaReneg: TAutoIncField;
    qryMasternCdLoja: TStringField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNmTerceiro: TStringField;
    qryMasterdDtProposta: TDateTimeField;
    qryMasternCdUsuarioGeracao: TIntegerField;
    qryMastercNmUsuario: TStringField;
    qryMasterdDtValidade: TDateTimeField;
    qryMasteriDiasAtraso: TIntegerField;
    qryMasteriParcelas: TIntegerField;
    qryMasternValOriginal: TBCDField;
    qryMasternValJuros: TBCDField;
    qryMasternValDescMaxOriginal: TBCDField;
    qryMasternValDescOriginal: TBCDField;
    qryMasternValDescMaxJuros: TBCDField;
    qryMasternValDescJuros: TBCDField;
    qryMasternSaldoNegociado: TBCDField;
    qryMasternValEntrada: TBCDField;
    qryMasternValEntradaMin: TBCDField;
    qryMasternValParcela: TBCDField;
    qryMasternValParcelaMin: TBCDField;
    dsMaster: TDataSource;
    qryMastercOBS: TMemoField;
    PopupMenu1: TPopupMenu;
    CancelarProposta1: TMenuItem;
    qryAux: TADOQuery;
    DBGridEh1: TDBGridEh;
    qryMastercRenegMultipla: TStringField;
    CancelarPropostasMltiplas1: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure CancelarProposta1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure CancelarPropostasMltiplas1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCancelaPropostaReneg: TfrmCancelaPropostaReneg;

implementation

uses fMenu, fAutorizaPropostaReneg_Titulos;

{$R *.dfm}

procedure TfrmCancelaPropostaReneg.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient ;
  qryMaster.Close;
  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryMaster.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  qryMaster.Open;

  if qryMaster.eof then
      ShowMessage('Nenhuma proposta para ser cancelada.') ;
  
end;

procedure TfrmCancelaPropostaReneg.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmAutorizaPropostaReneg_Titulos;
begin
  inherited;

  if qryMaster.Active then
  begin
      objForm := TfrmAutorizaPropostaReneg_Titulos.Create(nil);

      objForm.dDtProposta := qryMasterdDtProposta.Value;
      objForm.ExibeTitulos(qryMasternCdPropostaReneg.Value);
  end ;
end;

procedure TfrmCancelaPropostaReneg.CancelarProposta1Click(
  Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if qryMaster.Active then
  begin
      if (MessageDlg('Confirma o cancelamento desta proposta ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      if (MessageDlg('Tem certeza que deseja cancelar esta proposta ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      try
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('UPDATE PropostaReneg SET nCdUsuarioCancel = ' + intToStr(frmMenu.nCdUsuarioLogado)+ ', dDtCancel = GetDate(), nCdTabStatusPropostaReneg = 4 WHERE nCdPropostaReneg = ' + qryMasternCdPropostaReneg.AsString) ;
          qryAux.ExecSQL ;
      except
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      ShowMessage('Proposta Cancelada.') ;

      qryMaster.Close;
      qryMaster.Open;

  end ;

end;

procedure TfrmCancelaPropostaReneg.ToolButton1Click(Sender: TObject);
begin
  inherited;
  qryMaster.Close;
  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryMaster.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  qryMaster.Open;

  if qryMaster.eof then
      ShowMessage('Nenhuma proposta para ser cancelada.') ;
end;

procedure TfrmCancelaPropostaReneg.CancelarPropostasMltiplas1Click(
  Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if qryMaster.Active then
  begin
      if (MessageDlg('Confirma o cancelamento de todas as propostas vinculadas aos títulos desta renegociação ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      if (MessageDlg('Tem certeza que deseja cancelar todas as propostas ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      try
          { -- cancela renegociações múltiplas -- }
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('UPDATE PropostaReneg                                                                                                   ');
          qryAux.SQL.Add('   SET nCdUsuarioCancel          = ' + IntToStr(frmMenu.nCdUsuarioLogado)                                               );
          qryAux.SQL.Add('      ,dDtCancel                 = GetDate()                                                                           ');
          qryAux.SQL.Add('      ,nCdTabStatusPropostaReneg = 4                                                                                   ');
          qryAux.SQL.Add(' WHERE nCdPropostaReneg IN (SELECT nCdPropostaReneg                                                                    ');
          qryAux.SQL.Add('                              FROM TituloPropostaReneg Tit                                                             ');
          qryAux.SQL.Add('                             WHERE nCdTitulo IN (SELECT nCdTitulo                                                      ');
          qryAux.SQL.Add('                                                   FROM TituloPropostaReneg                                            ');
          qryAux.SQL.Add('                                                  WHERE nCdPropostaReneg = ' + qryMasternCdPropostaReneg.AsString + '))');
          qryAux.ExecSQL;
      except
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      ShowMessage('Propostas Canceladas.') ;

      qryMaster.Close;
      qryMaster.Open;
  end;

end;

initialization
    RegisterClass(TfrmCancelaPropostaReneg) ;
    
end.
