unit rPagamentoCentroCusto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DBCtrls, DB, ADODB;

type
  TrptRelPagtoCentroCusto = class(TfrmRelatorio_Padrao)
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    Label5: TLabel;
    MaskEdit6: TMaskEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label1: TLabel;
    MaskEdit3: TMaskEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label2: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label4: TLabel;
    Label7: TLabel;
    MaskEdit4: TMaskEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryCentroCustoNivel3: TADOQuery;
    qryCentroCustoNivel3nCdCC: TIntegerField;
    qryCentroCustoNivel3cCdCC: TStringField;
    qryCentroCustoNivel3cNmCC: TStringField;
    dsCentroCustoNivel3: TDataSource;
    MaskEdit5: TMaskEdit;
    MaskEdit7: TMaskEdit;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryCentroCustoNivel1: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    qryCentroCustoNivel2: TADOQuery;
    IntegerField2: TIntegerField;
    StringField3: TStringField;
    StringField4: TStringField;
    dsEmpresa: TDataSource;
    dsTerceiro: TDataSource;
    dsCentroCustoNivel1: TDataSource;
    dsCentroCustoNivel2: TDataSource;
    DBEdit4: TDBEdit;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    dsUnidadeNegocio: TDataSource;
    MaskEdit8: TMaskEdit;
    Label8: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    DBEdit1: TDBEdit;
    RadioGroup1: TRadioGroup;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyPress(Sender: TObject; var Key: Char);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit5Enter(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure CheckBox2Click(Sender: TObject);
    procedure RadioButton1Click(Sender: TObject);
    procedure RadioButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRelPagtoCentroCusto: TrptRelPagtoCentroCusto;

implementation

uses fLookup_Padrao, fMenu, rPagamentoCentroCusto_View, rPagamentoCentroCustoSint_View;

{$R *.dfm}
procedure TrptRelPagtoCentroCusto.FormShow(Sender: TObject);
var
iAux : Integer ;
begin
  inherited;
  iAux := frmMenu.UsuarioEmpresaAutorizada;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit4.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  MaskEdit4.SetFocus ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      MaskEdit8.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLoja, MaskEdit8.text) ;
  end
  else
  begin
      MasKEdit8.ReadOnly := True ;
      MasKEdit8.Color    := $00E9E4E4 ;
  end ;
  RadioButton1.Checked := True;
  CheckBox1.Checked    := True;
  CheckBox2.Checked    := True;

end;

procedure TrptRelPagtoCentroCusto.MaskEdit3KeyDown(Sender: TObject;
var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
        if (frmMenu.ConvInteiro(MaskEdit7.Text) > 0) then
        begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(29, ' nCdCC2 = ' + MaskEdit7.Text);

            If (nPK > 0) then
            begin
               Maskedit3.Text := IntToStr(nPK) ;
               PosicionaQuery(qryCentroCustoNivel3, MaskEdit3.Text) ;
            end ;
        end;
    end ;

  end ;


end;

procedure TrptRelPagtoCentroCusto.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryCentroCustoNivel3.Close ;
  qryCentroCustoNivel3.Parameters.ParamByName('nCdCC1').Value :=  frmMenu.ConvInteiro(MaskEdit5.Text) ;
  qryCentroCustoNivel3.Parameters.ParamByName('nCdCC2').Value :=  frmMenu.ConvInteiro(MaskEdit7.Text) ;
  PosicionaQuery(qryCentroCustoNivel3, MaskEdit3.Text) ;
  if qryCentroCustoNivel3.Eof then MaskEdit3.Clear;
end;

procedure TrptRelPagtoCentroCusto.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(26);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCentroCustoNivel1, MaskEdit5.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRelPagtoCentroCusto.MaskEdit7KeyDown(Sender: TObject;
var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
  begin
  inherited;
  case key of
    vk_F4 : begin
        if (frmMenu.ConvInteiro(MaskEdit5.Text)) > 0 then
        begin
           nPK := frmLookup_Padrao.ExecutaConsulta2(27,'nCdCC1 = ' + MaskEdit5.Text);

           If (nPK > 0) then
           begin
              Maskedit7.Text := IntToStr(nPK) ;
              PosicionaQuery(qryCentroCustoNivel2, MaskEdit7.Text) ;
           end ;
        end;
    end ;

  end ;

end;

procedure TrptRelPagtoCentroCusto.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;
   case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, MaskEdit6.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRelPagtoCentroCusto.MaskEdit6KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TrptRelPagtoCentroCusto.ToolButton1Click(Sender: TObject);
var
 cFiltro         : String;
 objRelAnalitico : TrptPagamentoCentroCusto_View ;
 objRelSintetico : TrptPagamentoCentroCustoSint_View ;
begin
  inherited;

  qryCentroCustoNivel2.Close;
  qryCentroCustoNivel2.Parameters.ParamByName('nCdCC1').Value :=  frmMenu.ConvInteiro(MaskEdit5.Text) ;
  PosicionaQuery(qryCentroCustoNivel2, MaskEdit7.Text) ;

  if qryCentroCustoNivel2.Eof then
      MaskEdit7.Clear;

  qryCentroCustoNivel3.Close ;
  qryCentroCustoNivel3.Parameters.ParamByName('nCdCC1').Value :=  frmMenu.ConvInteiro(MaskEdit5.Text) ;
  qryCentroCustoNivel3.Parameters.ParamByName('nCdCC2').Value :=  frmMenu.ConvInteiro(MaskEdit7.Text) ;
  PosicionaQuery(qryCentroCustoNivel3, MaskEdit3.Text) ;

  if qryCentroCustoNivel3.Eof then
      MaskEdit3.Clear;

  //anal�tico
  if (RadioButton1.Checked = true) then
  begin

     objRelAnalitico := TrptPagamentoCentroCusto_View.Create(Self) ;

     objRelAnalitico.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

     objRelAnalitico.uspRelatorio.Close ;
     objRelAnalitico.uspRelatorio.Parameters.ParamByName('@nCdEmpresa').Value                  :=  frmMenu.ConvInteiro(MaskEdit4.Text);
     objRelAnalitico.uspRelatorio.Parameters.ParamByName('@nCdCentroCustoNivel1').Value        :=  frmMenu.ConvInteiro(MaskEdit5.Text);
     objRelAnalitico.uspRelatorio.Parameters.ParamByName('@nCdCentroCustoNivel2').Value        :=  frmMenu.ConvInteiro(MaskEdit7.Text);
     objRelAnalitico.uspRelatorio.Parameters.ParamByName('@nCdCentroCustoNivel3').Value        :=  frmMenu.ConvInteiro(MaskEdit3.Text);
     objRelAnalitico.uspRelatorio.Parameters.ParamByName('@nCdTerceiro').Value                 :=  frmMenu.ConvInteiro(MaskEdit6.Text);
     objRelAnalitico.uspRelatorio.Parameters.ParamByName('@dDtInicial').Value                  :=  frmMenu.ConvData(MaskEdit1.Text);
     objRelAnalitico.uspRelatorio.Parameters.ParamByName('@dDtFinal').Value                    :=  frmMenu.ConvData(MaskEdit2.Text);
     objRelAnalitico.uspRelatorio.Parameters.ParamByName('@nCdUsuario').Value                  :=  frmMenu.nCdUsuarioLogado;

     if (DBEdit1.Text = '') then
         MaskEdit8.Text :='';

     if (frmMenu.LeParametro('VAREJO') = 'N') then
        objRelAnalitico.uspRelatorio.Parameters.ParamByName('@nCdLoja').Value   := 0
     else objRelAnalitico.uspRelatorio.Parameters.ParamByName('@nCdLoja').Value := frmMenu.ConvInteiro(MaskEdit8.Text) ;

     objRelAnalitico.uspRelatorio.Open ;
  end;

  //sint�tico
  if (RadioButton2.Checked = true) then
  begin

     objRelSintetico := TrptPagamentoCentroCustoSint_View.Create(Self) ;

     objRelSintetico.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

     objRelSintetico.uspRelatorio.Close ;
     objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdEmpresa').Value           :=  frmMenu.ConvInteiro(MaskEdit4.Text);
     objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdCentroCustoNivel1').Value :=  frmMenu.ConvInteiro(MaskEdit5.Text);
     objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdCentroCustoNivel2').Value :=  frmMenu.ConvInteiro(MaskEdit7.Text);
     objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdCentroCustoNivel3').Value :=  frmMenu.ConvInteiro(MaskEdit3.Text);
     objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdTerceiro').Value          :=  frmMenu.ConvInteiro(MaskEdit6.Text);
     objRelSintetico.uspRelatorio.Parameters.ParamByName('@dDtInicial').Value           :=  frmMenu.ConvData(MaskEdit1.Text);
     objRelSintetico.uspRelatorio.Parameters.ParamByName('@dDtFinal').Value             :=  frmMenu.ConvData(MaskEdit2.Text);
     objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdUsuario').Value           :=  frmMenu.nCdUsuarioLogado;

     if (DBEdit1.Text = '') then
         MaskEdit8.Text :='';

     if (frmMenu.LeParametro('VAREJO') = 'N') then
        objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdLoja').Value       := 0
     else objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdLoja').Value     := frmMenu.ConvInteiro(MaskEdit8.Text) ;

     if CheckBox1.Checked then
         objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdEmiteCCNivel2').Value  := 1
     else objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdEmiteCCNivel2').Value := 0;

     if CheckBox2.Checked then
         objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdEmiteCCNivel3').Value  := 1
     else objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdEmiteCCNivel3').Value := 0;

     if CheckBox3.Checked then
         objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdEmiteCCZerado').Value  := 1
     else objRelSintetico.uspRelatorio.Parameters.ParamByName('@nCdEmiteCCZerado').Value := 0;

     objRelSintetico.uspRelatorio.Open ;
  end;

  cFiltro := '';
  if (Trim(DBEdit14.Text) <> '') then
      cFiltro := cFiltro + ' Empresa: ' + Trim(MaskEdit4.Text)  + '-' + DBEdit14.Text ;

  if (Trim(MaskEdit5.Text) <> '') then
      cFiltro := cFiltro + ' / C. Custo N�vel 1 : ' + Trim(MaskEdit5.Text) + '-' + DBEdit11.Text;

  if (Trim(MaskEdit7.Text) <> '') then
      cFiltro := cFiltro + ' / C. Custo N�vel 2 : ' + Trim(MaskEdit7.Text) + '-' + DBEdit12.Text ;

  if (Trim(MaskEdit3.Text) <> '') then
      cFiltro := cFiltro + ' / C. Custo N�vel 3 : ' + Trim(MaskEdit3.Text) + '-' + DBEdit3.Text;

  if (Trim(MaskEdit6.Text) <> '') then
      cFiltro := cFiltro + ' / Terceiro : ' + Trim(MaskEdit6.Text) + '-' + DBEdit10.Text;

  if ((Trim(MaskEdit1.Text) <> '/  /') or (Trim(MaskEdit2.Text) <> '/  /')) then
      cFiltro := cFiltro + ' / Periodo : ' + Trim(MaskEdit1.Text)  + '-' + Trim(MaskEdit2.Text);

  if ((frmMenu.LeParametro('VAREJO') = 'S') AND ((Trim(MaskEdit8.Text)) <> ''))then
     cFiltro := cFiltro +  ' / Loja : ' + trim(MaskEdit8.Text) + '-' + DbEdit1.Text ;

  if ((RadioButton2.Checked) AND (CheckBox3.Checked)) then
      cFiltro := cFiltro + ' / Exibir C.Custo sem movto? Sim';

  {-- analitico --}
  if (RadioButton1.Checked = true) then
  begin

      objRelAnalitico.lblFiltro1.Caption := cFiltro ;

      try
          try
              {--visualiza o relat�rio--}

              objRelAnalitico.QuickRep1.PreviewModal;

          except
              MensagemErro('Erro na cria��o do relat�rio');
              raise;
          end;
      finally
          FreeAndNil(objRelAnalitico);
      end;

  end;

  {-- sint�tico --}
  if (RadioButton2.Checked = true) then
  begin

      objRelSintetico.lblFiltro1.Caption := cFiltro ;

      try
          try
              {--visualiza o relat�rio--}

              objRelSintetico.QuickRep1.PreviewModal;

          except
              MensagemErro('Erro na cria��o do relat�rio');
              raise;
          end;
      finally
          FreeAndNil(objRelSintetico);
      end;

  end;

end;

procedure TrptRelPagtoCentroCusto.MaskEdit5Enter(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCentroCustoNivel1, MaskEdit5.Text) ;

end;

procedure TrptRelPagtoCentroCusto.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryCentroCustoNivel1.Close;
  PosicionaQuery(qryCentroCustoNivel1, MaskEdit5.Text) ;

end;

procedure TrptRelPagtoCentroCusto.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryCentroCustoNivel2.Close;
  qryCentroCustoNivel2.Parameters.ParamByName('nCdCC1').Value :=  frmMenu.ConvInteiro(MaskEdit5.Text) ;
  PosicionaQuery(qryCentroCustoNivel2, MaskEdit7.Text) ;
  if qryCentroCustoNivel2.Eof then MaskEdit7.Clear;

end;

procedure TrptRelPagtoCentroCusto.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, MaskEdit6.Text) ;

end;

procedure TrptRelPagtoCentroCusto.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var
   nPk : integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit4.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptRelPagtoCentroCusto.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit4.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit4.Text ;
    qryEmpresa.Open ;
  end ;
  if qryEmpresa.Eof then MaskEdit4.Text := '';
end;


procedure TrptRelPagtoCentroCusto.MaskEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptRelPagtoCentroCusto.MaskEdit8Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  PosicionaQuery(qryLoja, MaskEdit8.Text) ;
  if qryLoja.Eof then MaskEdit8.Text := '';
end;

procedure TrptRelPagtoCentroCusto.FormActivate(Sender: TObject);
begin
  inherited;
  if (frmMenu.LeParametro('VAREJO') = 'N') then
      MaskEdit8.Enabled := False ;

end;

procedure TrptRelPagtoCentroCusto.CheckBox1Click(Sender: TObject);
begin
  inherited;
  if not CheckBox1.Checked then CheckBox2.Checked := False;
end;

procedure TrptRelPagtoCentroCusto.CheckBox2Click(Sender: TObject);
begin
  inherited;
  if CheckBox2.Checked then CheckBox1.Checked := True;
end;

procedure TrptRelPagtoCentroCusto.RadioButton1Click(Sender: TObject);
begin
  inherited;
  CheckBox1.Enabled    := False;
  CheckBox2.Enabled    := False;
  CheckBox3.Enabled    := False;
end;

procedure TrptRelPagtoCentroCusto.RadioButton2Click(Sender: TObject);
begin
  inherited;
  CheckBox1.Enabled    := True;
  CheckBox2.Enabled    := True;
  CheckBox3.Enabled    := True;
end;

initialization
   RegisterClass(TrptRelPagtoCentroCusto) ;

end.
