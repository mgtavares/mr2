inherited frmOperadorPCP: TfrmOperadorPCP
  Left = 207
  Top = 163
  Caption = 'Operador PCP'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [1]
    Left = 29
    Top = 69
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [2]
    Left = 26
    Top = 92
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
  end
  object Label1: TLabel [3]
    Left = 21
    Top = 45
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 64
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdOperadorPCP'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 64
    Top = 63
    Width = 650
    Height = 19
    DataField = 'cNmOperadorPCP'
    DataSource = dsMaster
    TabOrder = 2
  end
  object edtStatus: TER2LookupDBEdit [7]
    Left = 64
    Top = 86
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 3
    CodigoLookup = 2
    QueryLookup = qryStatus
  end
  object DBEdit3: TDBEdit [8]
    Tag = 1
    Left = 132
    Top = 86
    Width = 582
    Height = 19
    DataField = 'cNmStatus'
    DataSource = DataSource1
    TabOrder = 4
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOperadorPCP'
      '      ,cNmOperadorPCP'
      '     ,nCdStatus'
      ' FROM OperadorPCP'
      ' WHERE nCdOperadorPCP = :nPk')
    Top = 184
    object qryMasternCdOperadorPCP: TIntegerField
      FieldName = 'nCdOperadorPCP'
    end
    object qryMastercNmOperadorPCP: TStringField
      FieldName = 'cNmOperadorPCP'
      Size = 50
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object qryStatus: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '    FROM Status'
      'WHERE nCdStatus = :nPk')
    Left = 688
    Top = 184
    object qryStatusnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryStatuscNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryStatus
    Left = 560
    Top = 392
  end
end
