inherited frmNotificaVencTitEmail: TfrmNotificaVencTitEmail
  Left = 104
  Top = 72
  Width = 1155
  Height = 598
  Caption = 'Notifica Vencimento de T'#237'tulo Financeiro - Email'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 282
    Width = 1139
    Height = 278
  end
  inherited ToolBar1: TToolBar
    Width = 1139
    inherited ToolButton1: TToolButton
      Caption = '&Consultar'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1139
    Height = 212
    Align = alTop
    Caption = ' Filtros '
    TabOrder = 1
    object Label7: TLabel
      Tag = 1
      Left = 42
      Top = 23
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
    end
    object Label1: TLabel
      Tag = 1
      Left = 62
      Top = 46
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
    end
    object Label2: TLabel
      Tag = 1
      Left = 43
      Top = 70
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro'
    end
    object Label3: TLabel
      Tag = 1
      Left = 32
      Top = 94
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Esp. T'#237'tulo'
    end
    object Label4: TLabel
      Tag = 1
      Left = 28
      Top = 118
      Width = 54
      Height = 13
      Alignment = taRightJustify
      Caption = 'Documento'
    end
    object Label6: TLabel
      Tag = 1
      Left = 12
      Top = 142
      Width = 70
      Height = 13
      Alignment = taRightJustify
      Caption = 'Dias de Atraso'
    end
    object Label5: TLabel
      Tag = 1
      Left = 158
      Top = 142
      Width = 131
      Height = 13
      Caption = '(0 = t'#237'tulos vencendo hoje)'
    end
    object edtDias: TEdit
      Left = 88
      Top = 136
      Width = 65
      Height = 21
      TabOrder = 5
      Text = '0'
    end
    object ER2LookupMaskEdit5: TER2LookupMaskEdit
      Left = 88
      Top = 112
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 4
      Text = '         '
      OnExit = ER2LookupMaskEdit5Exit
      CodigoLookup = 784
    end
    object ER2LookupMaskEdit1: TER2LookupMaskEdit
      Left = 88
      Top = 16
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      OnExit = ER2LookupMaskEdit1Exit
      CodigoLookup = 8
    end
    object ER2LookupMaskEdit2: TER2LookupMaskEdit
      Left = 88
      Top = 40
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 1
      Text = '         '
      OnExit = ER2LookupMaskEdit2Exit
      CodigoLookup = 59
    end
    object ER2LookupMaskEdit3: TER2LookupMaskEdit
      Left = 88
      Top = 64
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 2
      Text = '         '
      OnExit = ER2LookupMaskEdit3Exit
      CodigoLookup = 225
    end
    object ER2LookupMaskEdit4: TER2LookupMaskEdit
      Left = 88
      Top = 88
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 3
      Text = '         '
      OnExit = ER2LookupMaskEdit4Exit
      CodigoLookup = 56
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 155
      Top = 112
      Width = 470
      Height = 21
      DataField = 'cNmModeloDocumento'
      DataSource = dsModeloDocumento
      TabOrder = 6
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 155
      Top = 88
      Width = 470
      Height = 21
      DataField = 'cNmEspTit'
      DataSource = dsEspTit
      TabOrder = 7
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 155
      Top = 64
      Width = 470
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = dsTerceiro
      TabOrder = 8
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 155
      Top = 40
      Width = 470
      Height = 21
      DataField = 'cNmLoja'
      DataSource = dsLoja
      TabOrder = 9
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 155
      Top = 16
      Width = 470
      Height = 21
      DataField = 'cNmEmpresa'
      DataSource = dsEmpresa
      TabOrder = 10
    end
    object GroupBox2: TGroupBox
      Left = 640
      Top = 15
      Width = 497
      Height = 195
      Align = alRight
      Caption = ' Log de Envio '
      TabOrder = 11
      object mLog: TMemo
        Left = 2
        Top = 15
        Width = 493
        Height = 178
        Align = alClient
        BorderStyle = bsNone
        Ctl3D = False
        ParentCtl3D = False
        ReadOnly = True
        TabOrder = 0
      end
    end
    object RadioGroup1: TRadioGroup
      Left = 88
      Top = 160
      Width = 209
      Height = 41
      Caption = ' Situa'#231#227'o '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        #192' Vencer'
        'Vencido')
      TabOrder = 12
    end
  end
  object panel: TPanel [3]
    Left = 0
    Top = 241
    Width = 1139
    Height = 41
    Align = alTop
    Color = clWindow
    TabOrder = 2
    object btEnviarLoteEmail: TcxButton
      Left = 276
      Top = 3
      Width = 169
      Height = 33
      Caption = 'Enviar E-&mail'
      TabOrder = 0
      TabStop = False
      OnClick = btEnviarLoteEmailClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000AD919A93897F
        937F7F93766C7F6C6C7F636C7F63596C59596C59596C59596C59596C59596C59
        596C59596C59596C5959978181EBE0D5FFFFEBFFF6D5FFEBBFFFE0BFFFD5A8FF
        D5A8FFD5A8EBCAA8EBCAA8FFBFA8FFCAA8FFCAA8D5A8914C3434978181A89191
        EBEBEBFFFFFFFFFFFFFFF6EBFFF6EBFFF6EBFFF6D5FFEBD5FFEBD5FFEBD5FFE0
        BFEBBFA891634C4C3434978181EBF6EBA89D91EBE0D5FFFFFFFFFFFFFFFFFFFF
        FFEBFFF6EBFFF6EBFFF6D5FFEBD5EBBFA8916363EBB37A4C3434978C81FFFFFF
        FFF6EBA8867AEBE0D5FFFFFFFFFFFFFFFFFFFFFFFFFFF6EBFFF6D5D5B3A87A57
        4CEBE0BFFFEBBF634034978C81FFFFFFEBE0D5BFA8A8BFA8A8EBCABFEBD5D5EB
        D5D5EBD5BFEBE0D5D5B3A8A8867A917A7AEBBFA8FFEBD563404CAD8C81EBE0D5
        BFA891EBEBEBEBEBEBA89D91A89D91A89191A89191A8867AA89191EBE0BFEBD5
        BF917A63EBB391634C4CAD979791917AFFF6EBD5A8A8BFA891BF917AA8867AA8
        6F63A86F63A86F6391634C914C34914C34EBEBD563574C634C4CAD8C97916F63
        FFFFFFFFF6FFFFF6FFFFF6FFFFF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF342013634C4C938383916F63FFFFFFD5A8A8BFA891BF917AA8867AA8
        6F63A86F63A86F6391634C914C34914C34FFFFFF3420136C5454CECECE916F63
        FFFFFFFFF6FFFFF6FFFFF6FFFFF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF342013CECECECECECE916F63FFFFFF34637A205763204C63134C6313
        4063063463062863002063001963001363FFFFFF342013CECECECECECE916F63
        FFFFFF4C7A91A8E0FF91E0FF91D5FF7ABFFF63B3FF4CA8EB3491EB2086EB0013
        63FFFFFF342013CECECECECECE916F63FFFFFF4C7A914C6F914C6F9134637A34
        577A204C7A13407A133463062863062863FFFFFF342013CECECECECECE917A63
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF342013CECECECECECE917A63916F63916F63916F63916F63916F6391
        634C91634C91634C91634C91634C91634C91634C91634CCECECE}
      LookAndFeel.NativeStyle = True
    end
    object btMarcarTodos: TcxButton
      Left = 4
      Top = 3
      Width = 133
      Height = 33
      Caption = 'Marcar &Todos'
      TabOrder = 1
      TabStop = False
      OnClick = btMarcarTodosClick
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFF00000000000000000000000000000000000000
        0000000000000000000000000000000000000000FFFFFF000000FFFFFF000000
        F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4
        F4000000FFFFFF000000FFFFFF000000F4F4F4CCCBCAD5D4D4DCDBDB936700EB
        EBEAEBEBEAECECEBECEBEBEAE9E9F4F4F4000000FFFFFF000000FFFFFF000000
        F4F4F4C6C4C2E9E9E9936700936700936700F6F6F6F6F6F6F6F6F6E6E6E6F4F4
        F4000000FFFFFF000000FFFFFF000000F4F4F4C2BFBC93670093670093670093
        6700936700F5F5F5F4F4F4E2E2E1F4F4F4000000FFFFFF000000FFFFFF000000
        F4F4F4936700936700936700EAEAEA936700936700936700F2F2F2DEDDDCF4F4
        F4000000FFFFFF000000FFFFFF000000F4F4F4BCB7B2936700DFDCDAE3E1E0E8
        E8E8936700936700936700D6D5D4F4F4F4000000FFFFFF000000FFFFFF000000
        F4F4F4B9B3AED7D1CDD9D4D0DBD7D4DFDDDBE3E2E1936700936700936700F4F4
        F4000000FFFFFF000000FFFFFF000000F4F4F4B9B3AED5CFCBD5CFCBD6D1CDDA
        D5D2DEDBD8E1DFDD936700936700936700000000FFFFFF000000FFFFFF000000
        F4F4F4B9B3AED5CFCBD5CFCBD5CFCBD5CFCBD8D3D0DCD8D5DFDDDB936700F4F4
        F4000000FFFFFF000000FFFFFF000000F4F4F4B9B3AEB9B3AEB9B3AEB9B3AEB9
        B3AEB9B3AEBAB4AFBDB9B4C1BEBBF4F4F4000000FFFFFF000000FFFFFF000000
        F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4
        F4000000FFFFFF000000FFFFFF00000000000000000000000000000000000000
        0000000000000000000000000000000000000000FFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
    object btDesmarcarTodos: TcxButton
      Left = 140
      Top = 3
      Width = 133
      Height = 33
      Caption = '&Desmarcar Todos'
      TabOrder = 2
      TabStop = False
      OnClick = btDesmarcarTodosClick
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFF00000000000000000000000000000000000000
        0000000000000000000000000000000000000000FFFFFF000000FFFFFF000000
        F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4
        F4000000FFFFFF000000FFFFFF000000F4F4F4CCCBCAD5D4D4DCDBDBE1E1E0E7
        E7E6EBEBEAECECEBECEBEBEAE9E9F4F4F4000000FFFFFF000000FFFFFF000000
        F4F4F4C6C4C2E9E9E9EDEDEDF0F0F0F4F4F4F6F6F6F6F6F6F6F6F6E6E6E6F4F4
        F4000000FFFFFF000000FFFFFF000000F4F4F4C2BFBCE5E4E3E9E9E9EDEDEDF2
        F2F2F4F4F4F5F5F5F4F4F4E2E2E1F4F4F4000000FFFFFF000000FFFFFF000000
        F4F4F4BFBBB8E1DFDDE5E5E4EAEAEAEFEFEFF2F2F2F2F2F2F2F2F2DEDDDCF4F4
        F4000000FFFFFF000000FFFFFF000000F4F4F4BCB7B2DCD8D5DFDCDAE3E1E0E8
        E8E8ECECECEDEDEDEDEDEDD6D5D4F4F4F4000000FFFFFF000000FFFFFF000000
        F4F4F4B9B3AED7D1CDD9D4D0DBD7D4DFDDDBE3E2E1E6E6E5E8E8E8CDCDCCF4F4
        F4000000FFFFFF000000FFFFFF000000F4F4F4B9B3AED5CFCBD5CFCBD6D1CDDA
        D5D2DEDBD8E1DFDDE4E3E2C8C7C6F4F4F4000000FFFFFF000000FFFFFF000000
        F4F4F4B9B3AED5CFCBD5CFCBD5CFCBD5CFCBD8D3D0DCD8D5DFDDDBC5C3C1F4F4
        F4000000FFFFFF000000FFFFFF000000F4F4F4B9B3AEB9B3AEB9B3AEB9B3AEB9
        B3AEB9B3AEBAB4AFBDB9B4C1BEBBF4F4F4000000FFFFFF000000FFFFFF000000
        F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4F4
        F4000000FFFFFF000000FFFFFF00000000000000000000000000000000000000
        0000000000000000000000000000000000000000FFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
  end
  object cxGridDoctoFiscal: TcxGrid [4]
    Left = 0
    Top = 282
    Width = 1139
    Height = 278
    Align = alClient
    TabOrder = 3
    LookAndFeel.NativeStyle = True
    object cxGridDoctoFiscalDBTableView1: TcxGridDBTableView
      DataController.DataSource = dsTempTerceiro
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      object cxGridDoctoFiscalDBTableView1DBColumn_cNmTerceiro1: TcxGridDBColumn
        Caption = 'Terceiro'
        DataBinding.FieldName = 'cNmTerceiro'
        Visible = False
        GroupIndex = 0
      end
      object cxGridDoctoFiscalDBTableView1DBColumn_cFlgAtivo: TcxGridDBColumn
        DataBinding.FieldName = 'cFlgAtivo'
        PropertiesClassName = 'TcxCheckBoxProperties'
        Properties.Alignment = taLeftJustify
        Properties.ValueChecked = 1
        Properties.ValueUnchecked = 0
        HeaderGlyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FAF7F9FBF9FF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFF7FAF837833D347D3AF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FBF8408E4754A35C4F9F5733
          7D39F8FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          F8FBF8499A515BAC6477CA8274C87E51A059347E3AF8FBF9FFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFF8FCF951A65A63B56D7ECE897BCC8776CA8176
          C98152A25A357F3BF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FCFA59B063
          6BBD7684D2907AC98560B26A63B46D78C98378CB8253A35C36803CF9FBF9FFFF
          FFFFFFFFFFFFFFFFFFFFD3ECD66CBD7679C98680CE8D53A75CB2D6B59CC9A05C
          AD677CCC8679CB8554A45D37813DF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD9EFDC
          6CBD756DC079B5DBB9FFFFFFFFFFFF98C79D5EAE687DCD897CCD8756A55F3882
          3EF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD5EDD8BEE2C3FFFFFFFFFFFFFFFFFFFF
          FFFF99C89D5FAF697FCE8A7ECE8957A66039833FF9FBF9FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF99C89E60B06A81CF8D7FCF
          8B58A761398540F9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF99C99E62B26C82D18F7AC88557A6609FC4A2FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9F63B3
          6D5FAF69A5CBA9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9FA5CEA9FFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        Width = 40
        IsCaptionAssigned = True
      end
      object cxGridDoctoFiscalDBTableView1DBColumn_nCdTerceiro: TcxGridDBColumn
        Caption = 'C'#243'd.'
        DataBinding.FieldName = 'nCdTerceiro'
        Width = 65
      end
      object cxGridDoctoFiscalDBTableView1DBColumn_cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro'
        DataBinding.FieldName = 'cNmTerceiro'
        Options.Editing = False
      end
      object cxGridDoctoFiscalDBTableView1DBColumn_cNrTit: TcxGridDBColumn
        Caption = 'Nr Titulo'
        DataBinding.FieldName = 'cNrTit'
        Options.Editing = False
      end
      object cxGridDoctoFiscalDBTableView1DBColumn_iParcela: TcxGridDBColumn
        Caption = 'Parcela'
        DataBinding.FieldName = 'iParcela'
        Options.Editing = False
      end
      object cxGridDoctoFiscalDBTableView1DBColumn_nSaldoTit: TcxGridDBColumn
        Caption = 'Saldo Tit'
        DataBinding.FieldName = 'nSaldoTit'
        Options.Editing = False
      end
      object cxGridDoctoFiscalDBTableView1DBColumn_dDtVenc: TcxGridDBColumn
        Caption = 'Dt Vencimento'
        DataBinding.FieldName = 'dDtVenc'
        Options.Editing = False
      end
    end
    object cxGridDoctoFiscalLevel1: TcxGridLevel
      GridView = cxGridDoctoFiscalDBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 224
    Top = 368
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003E39340039343000332F
      2B002C29250027242100201D1B00E7E7E700333130000B0A0900070706000404
      0300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000046413B00857A7000C3B8
      AE007C7268007F756B0036322D00F2F2F1004C4A470095897D00BAAEA2007C72
      68007F756B000101010000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF00000084000000000000000000000000004D47410083786F00CCC3
      BA00786F65007B71670034302D00FEFEFE002C2A270095897D00C2B8AD00786F
      65007C7268000605050000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000554E480083786F00CCC3
      BA007970660071685F00585550000000000049464500857A7000C2B8AD00786F
      65007B7167000D0C0B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000817B76009F928600CCC3
      BA00C0B4AA00A6988B00807D79000000000074726F0090847900C2B8AD00C0B4
      AA00A89B8E004947470000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000FCFCFC0060595200423D38005851
      4A003D383300332F2B0039373400D3D3D3005F5E5C001A181600252220001917
      15000F0E0D0012121200FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF00000084000000000000000000FDFDFD009D918500B1A396007F75
      6B007C726800776D64006C635B002E2A2600564F480080766C007C726800776D
      640070675E0001010100FAFAFA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000FEFDFD00B8ACA100BAAEA2008277
      6D0082776D00AA917B00BAA79400B8A69000B09781009F8D7D00836D5B007163
      570095897D0023232200FCFCFC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000FDFCFC00DDDAD7009B8E82009D91
      8500867B7100564F4800504A440080766C006E665D00826C5800A6917D009484
      7400564F48008B8A8A00FEFEFE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000746B6200A497
      8A0095897D009F9286003E393400000000004C4640007E746A00857A70003E39
      340085817E00F5F5F500FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      00009B918700C3B8AE00655D5500000000007C726800A89B8E00A69B90000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000000000000000
      0000A79C9100BCB0A4009D91850000000000AEA093009D9185007B756E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFFF0000
      FE00C00380030000000080018003000000008001800300000000800181030000
      0000800181030000000080010001000000008001000100000000800100010000
      000080010001000000008001C101000000018001F11F000000038001F11F0000
      0077C003FFFF0000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object IdSSLIOHandlerSocket1: TIdSSLIOHandlerSocket
    SSLOptions.Method = sslvSSLv2
    SSLOptions.Mode = sslmUnassigned
    SSLOptions.VerifyMode = []
    SSLOptions.VerifyDepth = 0
    Left = 324
    Top = 336
  end
  object qryConfigAmbiente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNmComputador'
        DataType = ftString
        Precision = 50
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cUFEmissaoNfe'
      
        '      ,cPathLogoNFe                                             ' +
        '                                            '
      '      ,nCdTipoAmbienteNFe'
      '      ,cFlgEmiteNFe '
      '      ,cNrSerieCertificadoNFe'
      '      ,nCdUFEmissaoNFe        '
      '      ,nCdMunicipioEmissaoNFe'
      '      ,CASE WHEN nCdTipoAmbienteNFe = 0 THEN cPathArqNFeProd'
      '            ELSE cPathArqNFeHom'
      '       END cPathArqNFe'
      '      ,cServidorSMTP'
      '      ,nPortaSMTP'
      '      ,cEmail'
      '      ,cSenhaEmail'
      '      ,cFlgUsaSSL'
      '  FROM ConfigAmbiente'
      ' WHERE cNmComputador = :cNmComputador')
    Left = 224
    Top = 329
    object qryConfigAmbientecUFEmissaoNfe: TStringField
      FieldName = 'cUFEmissaoNfe'
      Size = 2
    end
    object qryConfigAmbientecPathLogoNFe: TStringField
      FieldName = 'cPathLogoNFe'
      Size = 100
    end
    object qryConfigAmbientenCdTipoAmbienteNFe: TIntegerField
      FieldName = 'nCdTipoAmbienteNFe'
    end
    object qryConfigAmbientecFlgEmiteNFe: TIntegerField
      FieldName = 'cFlgEmiteNFe'
    end
    object qryConfigAmbientecNrSerieCertificadoNFe: TStringField
      FieldName = 'cNrSerieCertificadoNFe'
      Size = 50
    end
    object qryConfigAmbientenCdUFEmissaoNFe: TIntegerField
      FieldName = 'nCdUFEmissaoNFe'
    end
    object qryConfigAmbientenCdMunicipioEmissaoNFe: TIntegerField
      FieldName = 'nCdMunicipioEmissaoNFe'
    end
    object qryConfigAmbientecPathArqNFe: TStringField
      FieldName = 'cPathArqNFe'
      ReadOnly = True
      Size = 100
    end
    object qryConfigAmbientecServidorSMTP: TStringField
      FieldName = 'cServidorSMTP'
      Size = 50
    end
    object qryConfigAmbientenPortaSMTP: TIntegerField
      FieldName = 'nPortaSMTP'
    end
    object qryConfigAmbientecEmail: TStringField
      FieldName = 'cEmail'
      Size = 50
    end
    object qryConfigAmbientecSenhaEmail: TStringField
      FieldName = 'cSenhaEmail'
      Size = 50
    end
    object qryConfigAmbientecFlgUsaSSL: TIntegerField
      FieldName = 'cFlgUsaSSL'
    end
  end
  object qryModeloDocumento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cDescricaoModeloDoc'
      '     ,cNmModeloDocumento'
      '      ,nCdTabTipoModeloDoc'
      '  FROM ModeloDocumento'
      ' WHERE nCdModeloDocumento = :nPK')
    Left = 400
    Top = 360
    object qryModeloDocumentocDescricaoModeloDoc: TMemoField
      FieldName = 'cDescricaoModeloDoc'
      BlobType = ftMemo
    end
    object qryModeloDocumentonCdTabTipoModeloDoc: TIntegerField
      FieldName = 'nCdTabTipoModeloDoc'
    end
    object qryModeloDocumentocNmModeloDocumento: TStringField
      FieldName = 'cNmModeloDocumento'
      Size = 30
    end
  end
  object IdSMTP1: TIdSMTP
    MaxLineAction = maException
    ReadTimeout = 0
    Port = 25
    AuthenticationType = atNone
    Left = 320
    Top = 368
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE nCdEmpresa = :nPK')
    Left = 464
    Top = 360
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK')
    Left = 496
    Top = 360
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK')
    Left = 528
    Top = 360
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 464
    Top = 392
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 496
    Top = 392
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 528
    Top = 392
  end
  object qryEspTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdEspTit'
      '      ,cNmEspTit'
      '  FROM EspTit'
      ' WHERE nCdEspTit = :nPK')
    Left = 432
    Top = 360
    object qryEspTitnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
  end
  object dsEspTit: TDataSource
    DataSet = qryEspTit
    Left = 432
    Top = 392
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    BeforeInsert = qryMasterBeforeInsert
    Parameters = <
      item
        Name = 'iDias'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdLojaTit'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEspTit'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @dias int'
      '       ,@nCdTerceiro int'
      'SET @dias = :iDias'
      'SET @nCdTerceiro = :nCdTerceiro'
      'SELECT nCdTitulo'
      '      ,Titulo.nCdTerceiro'
      '      ,cNrTit'
      '     ,Terceiro.cEmail'
      '     ,nCdEmpresa'
      '     ,nCdLojaTit'
      '     ,nCdEspTit'
      '      ,nValTit'
      '      ,nValDesconto'
      '      ,nValJuro'
      '      ,nValAbatimento'
      '      ,nSaldoTit'
      '      ,dDtVenc'
      '  FROM Titulo'
      
        '  INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdTercei' +
        'ro'
      ' WHERE  nCdEmpresa       = :nCdEmpresa'
      '   AND nCdLojaTit       = :nCdLojaTit'
      '   AND nCdEspTit        = :nCdEspTit'
      
        '   AND ((Titulo.ncdterceiro = @nCdTerceiro) or (@nCdTerceiro = 0' +
        '))'
      '   AND dDtVenc - @dias = dbo.fn_OnlyDate(GETDATE())'
      'AND Terceiro.cEmail IS NOT NULL'
      '')
    Left = 256
    Top = 368
    object qryMasternCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryMastercNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryMasternValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryMasternValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryMasternValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryMasternValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object qryMasternSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLojaTit: TIntegerField
      FieldName = 'nCdLojaTit'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasternCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryMastercEmail: TStringField
      FieldName = 'cEmail'
      Size = 50
    end
  end
  object qryTerceiroAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cEmail,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nCdTerceiro')
    Left = 352
    Top = 368
    object qryTerceiroAuxcEmail: TStringField
      FieldName = 'cEmail'
      Size = 50
    end
    object qryTerceiroAuxcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object dsModeloDocumento: TDataSource
    DataSet = qryModeloDocumento
    Left = 400
    Top = 392
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 256
    Top = 333
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'iDias'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLojaTit'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cFlgTipoVenc'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEspTit'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @iDiasVenc    int'
      '       ,@nCdTerceiro  int'
      '       ,@nCdLojaTit   int'
      '       ,@cFlgTipoVenc int'
      ''
      'SET @iDiasVenc   = :iDias'
      'SET @nCdTerceiro = ISNULL(:nCdTerceiro,0)'
      'SET @nCdLojaTit  = ISNULL(:nCdLojaTit,0)'
      'SET @cFlgTipoVenc = :cFlgTipoVenc'
      ''
      'INSERT INTO #TempTerceiro'
      'SELECT nCdTitulo'
      '      ,Titulo.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Terceiro.cEmail'
      '      ,cNrTit'
      '      ,iParcela'
      '      ,nCdEmpresa'
      '      ,nCdLojaTit'
      '      ,nCdEspTit'
      '      ,nValTit'
      '      ,nValDesconto'
      '      ,nValJuro'
      '      ,nValAbatimento'
      '      ,nSaldoTit'
      '      ,dDtVenc'
      '      ,1'
      '  FROM Titulo'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdT' +
        'erceiro'
      ' WHERE nCdEmpresa             = :nCdEmpresa'
      '   AND nCdEspTit              = :nCdEspTit'
      
        '   AND ((nCdLojaTit           = @nCdLojaTit)  OR (@nCdLojaTit  =' +
        ' 0))'
      
        '   AND ((Titulo.ncdterceiro   = @nCdTerceiro) OR (@nCdTerceiro =' +
        ' 0))'
      
        '   AND ((@cFlgTipoVenc = 0 AND(dDtVenc - @iDiasVenc) = dbo.fn_On' +
        'lyDate(GETDATE())) '
      
        '      OR (@cFlgTipoVenc = 1 AND dDtVenc < dbo.fn_OnlyDate(GETDAT' +
        'E())))'
      '   AND Terceiro.cEmail        IS NOT NULL')
    Left = 192
    Top = 357
  end
  object qryTempTerceiro: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempTerceiro')
    Left = 288
    Top = 333
    object qryTempTerceironCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTempTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTempTerceirocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTempTerceirocEmail: TStringField
      FieldName = 'cEmail'
      Size = 50
    end
    object qryTempTerceironCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryTempTerceironCdLojaTit: TIntegerField
      FieldName = 'nCdLojaTit'
    end
    object qryTempTerceironCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryTempTerceironValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTempTerceironValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryTempTerceironValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTempTerceironValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object qryTempTerceironSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTempTerceirodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTempTerceirocFlgAtivo: TIntegerField
      FieldName = 'cFlgAtivo'
    end
    object qryTempTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryTempTerceiroiParcela: TIntegerField
      FieldName = 'iParcela'
    end
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempTerceiro'#39') IS NOT NULL)'#13#10'BEGIN'#13#10#13#10'  ' +
      '  DROP TABLE #TempTerceiro'#13#10#13#10'END'#13#10#13#10'SELECT  top 0'#13#10'       nCdTi' +
      'tulo'#13#10'      ,Titulo.nCdTerceiro'#13#10'      ,Terceiro.cNmTerceiro'#13#10'  ' +
      '    ,Terceiro.cEmail'#13#10'      ,cNrTit'#13#10'      ,iParcela'#13#10'      ,nCd' +
      'Empresa'#13#10'      ,nCdLojaTit'#13#10'      ,nCdEspTit'#13#10'      ,nValTit'#13#10'  ' +
      '    ,nValDesconto'#13#10'      ,nValJuro'#13#10'      ,nValAbatimento'#13#10'     ' +
      ' ,nSaldoTit'#13#10'      ,dDtVenc'#13#10'  INTO #TempTerceiro'#13#10'  FROM Titulo' +
      #13#10'  INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdTerc' +
      'eiro'#13#10'   '#13#10'ALTER TABLE #TempTerceiro ADD cFlgAtivo int default 1'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 192
    Top = 328
  end
  object dsTempTerceiro: TDataSource
    DataSet = qryTempTerceiro
    Left = 288
    Top = 368
  end
end
