inherited rptRegistroEntradaFuncionario: TrptRegistroEntradaFuncionario
  Left = 139
  Top = 176
  Caption = 'rptRegistroEntradaFuncionario'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label14: TLabel [1]
    Left = 164
    Top = 144
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label18: TLabel [2]
    Left = 41
    Top = 144
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo'
  end
  object Label1: TLabel [3]
    Left = 36
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label2: TLabel [4]
    Left = 57
    Top = 80
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label3: TLabel [5]
    Left = 22
    Top = 112
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Funcion'#225'rio'
  end
  inherited ToolBar1: TToolBar
    TabOrder = 3
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtDtFinal: TMaskEdit [7]
    Left = 192
    Top = 136
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object edtDtInicial: TMaskEdit [8]
    Left = 80
    Top = 136
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
  end
  object DBEdit1: TDBEdit [9]
    Tag = 1
    Left = 152
    Top = 40
    Width = 545
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 4
  end
  object DBEdit2: TDBEdit [10]
    Tag = 1
    Left = 152
    Top = 72
    Width = 545
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 5
  end
  object DBEdit3: TDBEdit [11]
    Tag = 1
    Left = 152
    Top = 104
    Width = 545
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsFuncionario
    TabOrder = 6
  end
  object edtEmpresa: TER2LookupMaskEdit [12]
    Left = 80
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 7
    Text = '         '
    OnExit = edtEmpresaExit
    OnKeyDown = edtEmpresaKeyDown
    CodigoLookup = 0
  end
  object edtLoja: TER2LookupMaskEdit [13]
    Left = 80
    Top = 72
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 8
    Text = '         '
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
    CodigoLookup = 0
  end
  object edtFuncionario: TER2LookupMaskEdit [14]
    Left = 80
    Top = 104
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    OnExit = edtFuncionarioExit
    OnKeyDown = edtFuncionarioKeyDown
    CodigoLookup = 0
  end
  inherited ImageList1: TImageList
    Left = 768
    Top = 160
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    AfterOpen = qryLojaAfterOpen
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdLoja'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa int'
      '       ,@nCdLoja int'
      ''
      'SET @nCdEmpresa = :nCdEmpresa'
      'SET @nCdLoja    = :nCdLoja'
      ''
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '      ,nCdEmpresa'
      '  FROM Loja'
      ' WHERE nCdEmpresa = @nCdEmpresa'
      '   AND nCdLoja    = @nCdLoja')
    Left = 632
    Top = 64
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryLojanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    AfterOpen = qryEmpresaAfterOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE nCdEmpresa = :nPK')
    Left = 632
    Top = 32
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryFuncionario: TADOQuery
    Connection = frmMenu.Connection
    AfterOpen = qryFuncionarioAfterOpen
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdLojaAtiva'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      '       ,@nCdUsuario int'
      '       ,@nCdLojaAtiva int'
      ''
      'SET @nCdLoja      = :nCdLoja'
      'SET @nCdUsuario   = :nCdUsuario'
      'SET @nCdLojaAtiva = :nCdLojaAtiva'
      ''
      'SELECT nCdUsuario'
      '      ,Terc.cNmTerceiro'
      '  FROM Usuario'
      
        '      INNER JOIN Terceiro Terc ON Terc.nCdTerceiro = Usuario.nCd' +
        'TerceiroResponsavel'
      ' WHERE nCdUsuario = @nCdUsuario'
      '   AND EXISTS (SELECT TOP 1 1'
      '                 FROM UsuarioLoja UL'
      '                WHERE UL.nCdUsuario = Usuario.nCdUsuario'
      '                  AND UL.nCdLoja    = @nCdLoja)')
    Left = 632
    Top = 96
    object qryFuncionarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryFuncionariocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 664
    Top = 32
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 664
    Top = 64
  end
  object dsFuncionario: TDataSource
    DataSet = qryFuncionario
    Left = 664
    Top = 96
  end
end
