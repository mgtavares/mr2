unit rDoctoFiscalEmitido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, DB, ADODB, StdCtrls, Mask, ImgList, ComCtrls,
  ToolWin, ExtCtrls, DBCtrls, ACBrBase, ACBrValidador;

type
  TrptDoctoFiscalEmitido = class(TfrmRelatorio_Padrao)
    edtEmpresa: TMaskEdit;
    edtLoja: TMaskEdit;
    edtTerceiro: TMaskEdit;
    edtTipoDocto: TMaskEdit;
    edtCPFCNPJ: TMaskEdit;
    edtDataInicial: TMaskEdit;
    edtDataFinal: TMaskEdit;
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    qryTipoDocto: TADOQuery;
    qryTerceiro: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    dsEmpresa: TDataSource;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    dsLoja: TDataSource;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    dsTerceiro: TDataSource;
    qryTipoDoctonCdTipoDoctoFiscal: TIntegerField;
    qryTipoDoctocNmTipoDoctoFiscal: TStringField;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    dsTipoDocto: TDataSource;
    rdgOperacao: TRadioGroup;
    rdgSituacao: TRadioGroup;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    ACBrValidador1: TACBrValidador;
    qryTipoPedido: TADOQuery;
    dsTipoPedido: TDataSource;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    edtTipoPedido: TEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    procedure edtEmpresaExit(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtTerceiroExit(Sender: TObject);
    procedure edtTipoDoctoExit(Sender: TObject);
    procedure edtCPFCNPJExit(Sender: TObject);
    procedure ACBrValidador1MsgErro(Mensagem: String);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTipoDoctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure edtTipoPedidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTipoPedidoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptDoctoFiscalEmitido: TrptDoctoFiscalEmitido;
  cFiltro              : string;

implementation

uses fMenu, rDoctoFiscalEmitido_view, fLookup_Padrao;

{$R *.dfm}

procedure TrptDoctoFiscalEmitido.edtEmpresaExit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text);
end;

procedure TrptDoctoFiscalEmitido.edtLojaExit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  PosicionaQuery(qryLoja,edtLoja.Text);
end;

procedure TrptDoctoFiscalEmitido.edtTerceiroExit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, edtTerceiro.Text);
end;

procedure TrptDoctoFiscalEmitido.edtTipoDoctoExit(Sender: TObject);
begin
  inherited;
  qryTipoDocto.Close;
  PosicionaQuery(qryTipoDocto, edtTipoDocto.Text);
end;

procedure TrptDoctoFiscalEmitido.edtCPFCNPJExit(Sender: TObject);
var
  cCPFCNPJ : string;
begin
  inherited;

  cCPFCNPJ := Trim(edtCPFCNPJ.Text);

  if (cCPFCNPJ <> '') then
  begin
      if (Length(cCPFCNPJ) > 11) then
          ACBrValidador1.TipoDocto := docCNPJ
      else ACBrValidador1.TipoDocto := docCPF;

      ACBrValidador1.Documento := edtCPFCNPJ.Text;
      ACBrValidador1.Validar;
  end;
end;

procedure TrptDoctoFiscalEmitido.ACBrValidador1MsgErro(Mensagem: String);
begin
  inherited;
  MensagemErro('O CPF/ CNPJ digitado � inv�lido.');
  edtCPFCNPJ.Text := '';
  edtCPFCNPJ.SetFocus;
  abort;
end;

procedure TrptDoctoFiscalEmitido.ToolButton1Click(Sender: TObject);
var
  objRel : TrptDoctoFiscalEmitido_view;
begin
  inherited;

  objRel :=  TrptDoctoFiscalEmitido_view.Create(nil);

  Try
      Try
          objRel.SPREL_DOCTO_FISCAL_EMITIDO.Close;
          objRel.SPREL_DOCTO_FISCAL_EMITIDO.Parameters.ParamByName('@nCdEmpresa').Value    := frmMenu.ConvInteiro(edtEmpresa.Text);
          objRel.SPREL_DOCTO_FISCAL_EMITIDO.Parameters.ParamByName('@nCdLoja').Value       := frmMenu.ConvInteiro(edtLoja.Text);
          objRel.SPREL_DOCTO_FISCAL_EMITIDO.Parameters.ParamByName('@nCdTipoDocto').Value  := frmMenu.ConvInteiro(edtTipoDocto.Text);
          objRel.SPREL_DOCTO_FISCAL_EMITIDO.Parameters.ParamByName('@nCdTerceiro').Value   := frmMenu.ConvInteiro(edtTerceiro.Text);
          objRel.SPREL_DOCTO_FISCAL_EMITIDO.Parameters.ParamByName('@cCPF').Value          := edtCPFCNPJ.Text;
          objRel.SPREL_DOCTO_FISCAL_EMITIDO.Parameters.ParamByName('@dDtInicial').Value    := frmMenu.ConvData(edtDataInicial.Text);
          objRel.SPREL_DOCTO_FISCAL_EMITIDO.Parameters.ParamByName('@dDtFinal').Value      := frmMenu.ConvData(edtDataFinal.Text);
          objRel.SPREL_DOCTO_FISCAL_EMITIDO.Parameters.ParamByName('@nTipoOper').Value     := rdgOperacao.ItemIndex;
          objRel.SPREL_DOCTO_FISCAL_EMITIDO.Parameters.ParamByName('@nSituacao').Value     := rdgSituacao.ItemIndex;
          objRel.SPREL_DOCTO_FISCAL_EMITIDO.Parameters.ParamByName('@nCdTipoPedido').Value := frmMenu.ConvInteiro(edtTipoPedido.Text);
          objRel.cmdPreparaTemp.Execute;
          objRel.SPREL_DOCTO_FISCAL_EMITIDO.ExecProc;

          objRel.qryResultado.Close;
          objRel.qryResultado.Open;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          {-- prepara filtros para serem exibidos --}
          objRel.lblFiltro1.Caption := '';
          cFiltro := '';

          if (Trim(edtEmpresa.Text) <> '') then
              cFiltro := cFiltro + 'Empresa: ' + DBEdit1.Text ;

          if (Trim(edtLoja.Text) <> '') then
              cFiltro := cFiltro + '/ Loja: ' + DBEdit2.Text;

          if (Trim(edtTerceiro.Text) <> '') then
              cFiltro := cFiltro + '/ Terceiro: ' + DBEdit3.Text;

          if (Trim(edtTipoDocto.Text) <> '') then
              cFiltro := cFiltro + '/ Tipo Docto: ' + DBEdit4.Text;

          if (Trim(edtTipoPedido.Text) <> '') then
              cFiltro := cFiltro + '/Tipo Pedido: ' + DBEdit5.Text;

          if (Trim(edtCPFCNPJ.Text) <> '') then
              cFiltro := cFiltro + '/ CPF/CNPJ: ' + edtCPFCNPJ.Text;

          if (rdgOperacao.ItemIndex = 0) then
              cFiltro := cFiltro + '/ Opera��o: Entrada'
          else cFiltro := cFiltro + '/ Opera��o: Sa�da';

          if (rdgSituacao.ItemIndex = 0) then
              cFiltro := cFiltro + '/ Situa��o: Todos'
          else if (rdgSituacao.ItemIndex = 1) then
              cFiltro := cFiltro + '/ Situa��o: Ativo'
          else cFiltro := cFiltro + '/ Situa��o: Cancelado';

          cFiltro := cFiltro  + '/ Per�odo: ' + edtDataInicial.Text + ' � ' + edtDataFinal.Text;

          objRel.lblFiltro1.Caption := cFiltro;

          {-- exibe relat�rio --}
          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  Finally;
      FreeAndNil(objRel);
  end;

end;

procedure TrptDoctoFiscalEmitido.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(147);

            If (nPK > 0) then
            begin
                edtLoja.Text := IntToStr(nPK);
                PosicionaQuery(qryLoja, edtLoja.Text);
            end ;

    end ;

  end ;

end;

procedure TrptDoctoFiscalEmitido.edtEmpresaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(25);

            If (nPK > 0) then
            begin
                edtEmpresa.Text := IntToStr(nPK);
                PosicionaQuery(qryEmpresa, edtEmpresa.Text);
            end ;

    end ;

  end ;

end;

procedure TrptDoctoFiscalEmitido.edtTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                edtTerceiro.Text := IntToStr(nPK);
                PosicionaQuery(qryTerceiro, edtTerceiro.Text);
            end ;

    end ;

  end ;

end;

procedure TrptDoctoFiscalEmitido.edtTipoDoctoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(77);

            If (nPK > 0) then
            begin
                edtTipoDocto.Text := IntToStr(nPK);
                PosicionaQuery(qryTipoDocto, edtTipoDocto.Text);
            end ;

    end ;

  end ;

end;

procedure TrptDoctoFiscalEmitido.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
  begin
      edtLoja.ReadOnly   := True ;
      edtLoja.Color      := $00E9E4E4 ;
      edtLoja.Font.Color := clBlack ;
      edtLoja.TabStop    := False ;
  end;

end;

procedure TrptDoctoFiscalEmitido.edtTipoPedidoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : integer;
begin
  inherited;
  case key of
  vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(54);

            If (nPK > 0) then
            begin
                edtTipoPedido.Text := IntToStr(nPK);
                PosicionaQuery(qryTipoPedido,edtTipoPedido.Text);
            end ;

          end ;
  end ;
end;

procedure TrptDoctoFiscalEmitido.edtTipoPedidoExit(Sender: TObject);
begin
  inherited;
  qryTipoPedido.Close;
  PosicionaQuery(qryTipoPedido,edtTipoPedido.Text);
end;

initialization
    RegisterClass(TrptDoctoFiscalEmitido);

end.
