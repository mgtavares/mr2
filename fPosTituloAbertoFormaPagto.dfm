inherited frmPosTituloAbertoFormaPagto: TfrmPosTituloAbertoFormaPagto
  Left = 234
  Top = 120
  Caption = 'Relat'#243'rio - Posi'#231#227'o de T'#237'tulos em aberto Forma de Pagto'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 68
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label2: TLabel [2]
    Left = 29
    Top = 98
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Esp'#233'cie de T'#237'tulo'
  end
  object Label5: TLabel [3]
    Left = 70
    Top = 122
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
  end
  object Label8: TLabel [4]
    Left = 35
    Top = 146
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Moeda Exibi'#231#227'o'
  end
  object Label3: TLabel [5]
    Left = 7
    Top = 199
    Width = 102
    Height = 13
    Alignment = taRightJustify
    Caption = 'Intervalo Vencimento'
  end
  object Label6: TLabel [6]
    Left = 191
    Top = 199
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label4: TLabel [7]
    Left = 22
    Top = 170
    Width = 87
    Height = 13
    Caption = 'Forma Pagamento'
  end
  object Label9: TLabel [8]
    Tag = 1
    Left = 89
    Top = 72
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [10]
    Tag = 1
    Left = 180
    Top = 40
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    Enabled = False
    TabOrder = 1
  end
  object DBEdit3: TDBEdit [11]
    Tag = 1
    Left = 252
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 2
  end
  object DBEdit7: TDBEdit [12]
    Tag = 1
    Left = 180
    Top = 90
    Width = 726
    Height = 21
    DataField = 'cNmEspTit'
    DataSource = DataSource3
    TabOrder = 3
  end
  object DBEdit9: TDBEdit [13]
    Tag = 1
    Left = 180
    Top = 115
    Width = 69
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = DataSource4
    TabOrder = 4
  end
  object DBEdit10: TDBEdit [14]
    Tag = 1
    Left = 252
    Top = 115
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource4
    TabOrder = 5
  end
  object DBEdit12: TDBEdit [15]
    Tag = 1
    Left = 180
    Top = 140
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource5
    TabOrder = 6
  end
  object DBEdit13: TDBEdit [16]
    Tag = 1
    Left = 252
    Top = 140
    Width = 654
    Height = 21
    DataField = 'cNmMoeda'
    DataSource = DataSource5
    TabOrder = 7
  end
  object MaskEdit1: TMaskEdit [17]
    Left = 112
    Top = 191
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 13
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [18]
    Left = 211
    Top = 191
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 14
    Text = '  /  /    '
  end
  object MaskEdit5: TMaskEdit [19]
    Left = 112
    Top = 90
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 9
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object MaskEdit6: TMaskEdit [20]
    Left = 112
    Top = 115
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 10
    Text = '         '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object MaskEdit7: TMaskEdit [21]
    Left = 112
    Top = 140
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 11
    Text = '      '
    OnExit = MaskEdit7Exit
  end
  object RadioGroup1: TRadioGroup [22]
    Left = 112
    Top = 217
    Width = 185
    Height = 65
    Caption = ' Tipo de Lan'#231'amento '
    ItemIndex = 0
    Items.Strings = (
      'T'#237'tulos a Pagar'
      'T'#237'tulos a Receber')
    TabOrder = 15
  end
  object DBEdit4: TDBEdit [23]
    Tag = 1
    Left = 180
    Top = 165
    Width = 726
    Height = 21
    DataField = 'cNmFormaPagto'
    DataSource = DataSource6
    TabOrder = 16
  end
  object MaskEdit4: TMaskEdit [24]
    Left = 112
    Top = 165
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 12
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object RadioGroup2: TRadioGroup [25]
    Left = 304
    Top = 217
    Width = 249
    Height = 65
    Caption = ' Listar somente t'#237'tulos sem forma de pagamento '
    ItemIndex = 1
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 17
  end
  object MaskEdit8: TMaskEdit [26]
    Left = 112
    Top = 65
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 8
    Text = '      '
    OnExit = MaskEdit8Exit
    OnKeyDown = MaskEdit8KeyDown
  end
  object DBEdit1: TDBEdit [27]
    Tag = 1
    Left = 180
    Top = 65
    Width = 726
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 18
  end
  object MaskEdit3: TMaskEdit [28]
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    TabOrder = 19
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  inherited ImageList1: TImageList
    Left = 608
    Top = 248
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 800
    Top = 280
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUnidadeNegocio'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM UNIDADENEGOCIO'
      ' WHERE nCdUnidadeNegocio = :nCdUnidadeNegocio')
    Left = 768
    Top = 280
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object qryEspTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdEspTit int'
      '    SET @nCdEspTit =:nPK'
      ''
      'SELECT *'
      '  FROM ESPTIT'
      ' WHERE nCdEspTit = @nCdEspTit'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioEspTit'
      '               WHERE UsuarioEspTit.nCdEspTit      = @nCdEspTit'
      '                 AND UsuarioEspTit.nCdUsuario     = :nCdUsuario'
      '                 AND UsuarioEspTit.cFlgTipoAcesso = '#39'L'#39')'
      '')
    Left = 736
    Top = 280
    object qryEspTitnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitnCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryEspTitcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryEspTitcTipoMov: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object qryEspTitcFlgPrev: TIntegerField
      FieldName = 'cFlgPrev'
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nCdTerceiro')
    Left = 704
    Top = 280
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryMoeda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMoeda'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '   FROM Moeda'
      ' WHERE nCdMoeda = :nCdMoeda')
    Left = 672
    Top = 280
    object qryMoedanCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMoedacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 10
    end
    object qryMoedacNmMoeda: TStringField
      FieldName = 'cNmMoeda'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 640
    Top = 312
  end
  object DataSource2: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 672
    Top = 312
  end
  object DataSource3: TDataSource
    DataSet = qryEspTit
    Left = 704
    Top = 312
  end
  object DataSource4: TDataSource
    DataSet = qryTerceiro
    Left = 736
    Top = 312
  end
  object DataSource5: TDataSource
    DataSet = qryMoeda
    Left = 768
    Top = 312
  end
  object qryFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdFormaPagto'
      ',cNmFormaPagto'
      'FROM FormaPagto'
      'WHERE nCdFormaPagto = :nPK'
      'AND cFlgPermFinanceiro=1'
      '')
    Left = 608
    Top = 280
    object qryFormaPagtonCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryFormaPagtocNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
  end
  object DataSource6: TDataSource
    DataSet = qryFormaPagto
    Left = 608
    Top = 312
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK'
      
        '      AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja =' +
        ' Loja.nCdLoja AND UL.nCdUsuario = :nCdUsuario)')
    Left = 640
    Top = 280
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 800
    Top = 312
  end
end
