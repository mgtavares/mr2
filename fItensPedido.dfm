inherited frmItensPedido: TfrmItensPedido
  Left = 102
  Top = 171
  Width = 987
  Height = 536
  Caption = 'Itens do Pedido'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 971
    Height = 471
  end
  inherited ToolBar1: TToolBar
    Width = 971
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 971
    Height = 471
    Align = alClient
    AllowedOperations = []
    DataGrouping.GroupLevels = <>
    DataSource = dsItens
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        ReadOnly = True
        Width = 80
      end
      item
        EditButtons = <>
        FieldName = 'cNmItem'
        Footers = <>
        ReadOnly = True
        Width = 275
      end
      item
        EditButtons = <>
        FieldName = 'nQtdePed'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'nValUnitario'
        Footers = <>
        ReadOnly = True
        Width = 77
      end
      item
        EditButtons = <>
        FieldName = 'nPercDescontoItem'
        Footers = <>
        ReadOnly = True
        Width = 72
      end
      item
        EditButtons = <>
        FieldName = 'nPercIPI'
        Footers = <>
        ReadOnly = True
        Width = 46
      end
      item
        EditButtons = <>
        FieldName = 'nValCustoUnit'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'nValTotal'
        Footers = <>
        Width = 78
      end
      item
        EditButtons = <>
        FieldName = 'nValSugVenda'
        Footers = <>
        ReadOnly = True
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryItens: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdPedido'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmItem'
      '      ,nQtdePed'
      '      ,nValUnitario'
      '      ,nPercDescontoItem'
      '      ,nPercIPI'
      '      ,nValCustoUnit'
      '      ,(nValCustoUnit * nQtdePed) nValTotal'
      '      ,nValSugVenda '
      '  FROM ItemPedido '
      ' WHERE nCdPedido = :nCdPedido'
      '   AND ItemPedido.nCdTipoItemPed IN (1,2,3,5)')
    Left = 264
    Top = 200
    object qryItensnCdProduto: TIntegerField
      DisplayLabel = 'C'#243'd. Produto'
      FieldName = 'nCdProduto'
    end
    object qryItenscNmItem: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItensnQtdePed: TBCDField
      DisplayLabel = 'Qtde.'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItensnValUnitario: TBCDField
      DisplayLabel = 'Valor Unit'#225'rio'
      FieldName = 'nValUnitario'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryItensnPercDescontoItem: TBCDField
      DisplayLabel = '% Desconto'
      FieldName = 'nPercDescontoItem'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItensnPercIPI: TBCDField
      DisplayLabel = '% IPI'
      FieldName = 'nPercIPI'
      DisplayFormat = '#,##0.00'
      Precision = 5
      Size = 2
    end
    object qryItensnValCustoUnit: TBCDField
      DisplayLabel = 'Val. Unit. Final'
      FieldName = 'nValCustoUnit'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryItensnValTotal: TBCDField
      DisplayLabel = 'Valor Total'
      FieldName = 'nValTotal'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 27
      Size = 10
    end
    object qryItensnValSugVenda: TBCDField
      DisplayLabel = 'Valor Venda'
      FieldName = 'nValSugVenda'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsItens: TDataSource
    DataSet = qryItens
    Left = 280
    Top = 232
  end
end
