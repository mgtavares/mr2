unit rPossibilidadeFaturamento_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPossibilidadeFaturamento_view = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_POSSIB_FATURAMENTO: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText6: TQRDBText;
    QRBand5: TQRBand;
    QRShape2: TQRShape;
    QRDBText8: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRLabel12: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    SPREL_POSSIB_FATURAMENTOnCdPedido: TIntegerField;
    SPREL_POSSIB_FATURAMENTOdDtAutorizacao: TDateTimeField;
    SPREL_POSSIB_FATURAMENTOcNmDestinatario: TStringField;
    SPREL_POSSIB_FATURAMENTOcNmTerceiroPagador: TStringField;
    SPREL_POSSIB_FATURAMENTOnCdItemPedido: TIntegerField;
    SPREL_POSSIB_FATURAMENTOnCdProduto: TIntegerField;
    SPREL_POSSIB_FATURAMENTOcNmProduto: TStringField;
    SPREL_POSSIB_FATURAMENTOnQtdePed: TIntegerField;
    SPREL_POSSIB_FATURAMENTOnQtdeExpRec: TIntegerField;
    SPREL_POSSIB_FATURAMENTOnQtdePossivel: TIntegerField;
    SPREL_POSSIB_FATURAMENTOnQtdeEstoque: TFloatField;
    SPREL_POSSIB_FATURAMENTOnQtdeEmpenhado: TFloatField;
    SPREL_POSSIB_FATURAMENTOnQtdeDisponivel: TFloatField;
    SPREL_POSSIB_FATURAMENTOCOLUMN1: TFloatField;
    QRLabel14: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel16: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel6: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPossibilidadeFaturamento_view: TrptPossibilidadeFaturamento_view;

implementation

{$R *.dfm}

end.
