inherited frmContabiliza: TfrmContabiliza
  Left = 24
  Top = 155
  Width = 1024
  Height = 497
  Caption = 'Contabilizar Lan'#231'amentos'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 126
    Width = 1008
    Height = 335
  end
  inherited ToolBar1: TToolBar
    Width = 1008
    Transparent = True
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 126
    Width = 1008
    Height = 335
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 331
    ClientRectLeft = 4
    ClientRectRight = 1004
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Log de Processamentos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1000
        Height = 307
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsLogProcesso
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'dDtLog'
            Footers = <>
            Width = 138
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cObserv'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1008
    Height = 97
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 5
      Top = 72
      Width = 141
      Height = 13
      Alignment = taRightJustify
      Caption = 'Contabilizar Lan'#231'amentos at'#233
    end
    object Label2: TLabel
      Tag = 1
      Left = 47
      Top = 24
      Width = 99
      Height = 13
      Alignment = taRightJustify
      Caption = #218'ltima contabiliza'#231#227'o'
    end
    object Label3: TLabel
      Tag = 1
      Left = 93
      Top = 48
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Limite'
    end
    object MaskEdit1: TMaskEdit
      Tag = 1
      Left = 152
      Top = 16
      Width = 115
      Height = 21
      EditMask = '99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 0
      Text = '  /  /    '
    end
    object MaskEdit2: TMaskEdit
      Left = 152
      Top = 64
      Width = 115
      Height = 21
      EditMask = '99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object MaskEdit3: TMaskEdit
      Tag = 1
      Left = 152
      Top = 40
      Width = 115
      Height = 21
      EditMask = '99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
  end
  object qryParametro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cParametro'
        DataType = ftString
        Precision = 15
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '   FROM PARAMETRO'
      '  WHERE cParametro = :cParametro')
    Left = 424
    Top = 64
    object qryParametrocParametro: TStringField
      FieldName = 'cParametro'
      Size = 15
    end
    object qryParametrocValor: TStringField
      FieldName = 'cValor'
      Size = 15
    end
    object qryParametrocDescricao: TStringField
      FieldName = 'cDescricao'
      Size = 50
    end
  end
  object usp_Contabiliza: TADOStoredProc
    AutoCalcFields = False
    Connection = frmMenu.Connection
    CursorType = ctStatic
    CommandTimeout = 0
    EnableBCD = False
    ProcedureName = 'SP_CONTABILIZA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtFechamento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 472
    Top = 64
  end
  object dsLogProcesso: TDataSource
    DataSet = qryLogProcesso
    Left = 536
    Top = 64
  end
  object qryLogProcesso: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 30 dDtLog'
      '      ,LogProcesso.nCdUsuario'
      '      ,cNmUsuario'
      '      ,cObserv '
      '  FROM LogProcesso'
      
        '       INNER JOIN Usuario ON Usuario.nCdUsuario = LogProcesso.nC' +
        'dUsuario'
      ' WHERE nCdProcesso = 1'
      ' ORDER BY dDtLog DESC')
    Left = 504
    Top = 64
    object qryLogProcessodDtLog: TDateTimeField
      DisplayLabel = 'Data Processo'
      FieldName = 'dDtLog'
    end
    object qryLogProcessonCdUsuario: TIntegerField
      DisplayLabel = 'Usu'#225'rio|C'#243'digo'
      FieldName = 'nCdUsuario'
    end
    object qryLogProcessocNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rio|Nome'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryLogProcessocObserv: TStringField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'cObserv'
      Size = 50
    end
  end
end
