unit fQueryExecute;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxLookAndFeelPainters, StdCtrls, cxButtons, DB, ADODB,
  PrnDbgeh, GridsEh, DBGridEh, ComCtrls, ComObj, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  cxFilterControl, cxGridCustomPopupMenu, cxGridPopupMenu;

type
  TfrmQueryExecute = class(TForm)
    Panel1: TPanel;
    PrintDBGridEh1: TPrintDBGridEh;
    qryResultado: TADOQuery;
    dsResultado: TDataSource;
    cxButton1: TcxButton;
    Edit1: TRichEdit;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxStyleRepository1: TcxStyleRepository;
    cxStyle1: TcxStyle;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxTV: TcxGridDBTableView;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmQueryExecute: TfrmQueryExecute;

implementation

{$R *.dfm}

procedure TfrmQueryExecute.cxButton1Click(Sender: TObject);
var
  csg     : TcxDataSummaryGroup;
  csglink : TcxDataSummaryGroupItemLink;
  csgItem : TcxDataSummaryItem;
  i       : Integer;
  s       : string;
begin

  if (pos('UPDATE',uppercase(Edit1.Text)) > 0) and (pos('WHERE',uppercase(edit1.Text)) = 0) then
  begin
      ShowMessage('N�o � poss�vel executar um UPDATE sem WHERE.') ;
      abort ;
  end ;

  if (pos('DELETE',uppercase(Edit1.Text)) > 0) and (pos('WHERE',uppercase(Edit1.Text)) = 0) then
  begin
      ShowMessage('N�o � poss�vel executar um DELETE sem WHERE.') ;
      abort ;
  end ;

  cxTV.ClearItems;

  with qryResultado,cxTV do
  begin
    close;
    SQL.Clear;
    SQL.Add(Edit1.Text);
    open;
    first;

    FieldList.Update;

    cxTV.DataController.DataSource := dsResultado ;
    cxTV.DataController.CreateAllItems();

    csg := DataController.Summary.SummaryGroups.Add;

    csg.Links.Clear;

    for i:= 0 to ColumnCount -1 do

    begin

      s:= Fields[i].FieldName;

      with Columns[i] do
      begin

        if Pos('cNm',s) > 0 then
        begin
            Width := 180;
        end;

        if Pos('dDt',s) > 0 then
        begin
            Width := 80;
        end;

        if (Fields[i] is TNumericField) and (Pos('nCd',s) = 0) then
        begin
          (Fields[i] as TNumericField).DisplayFormat := '#,0.00;-#,0.00;#';

          Columns[i].Summary.FooterFormat :='#,0.00';
          Columns[i].Summary.FooterKind   := skSum;
          Columns[i].Width                := 80 ;

          csgitem          := csg.SummaryItems.Add;
          csgitem.ItemLink := Columns[i];
          csgitem.Kind     := skSum;
          csgItem.Format   := '#,0.00;-#,0.00; #';
          csgItem.Position := spGroup ;
          Width            := 80;

        end
        else begin

            csglink          := csg.Links.Add;
            csglink.ItemLink := Columns[i];

        end;

     end;

    end;

    OptionsData.Deleting        := False ;
    OptionsData.Inserting       := False ;
    OptionsData.Editing         := False ;

    OptionsView.GridLines       := glboth ;
    OptionsView.GroupByBox      := True ;
    OptionsView.Header          := True ;
    OptionsView.Footer          := True ;
    OptionsView.HeaderEndEllipsis := True ;

    OptionsBehavior.PullFocusing := True ;

    OptionsCustomize.ColumnHidingOnGrouping := False ;
    OptionsCustomize.ColumnHiding := True;
    OptionsCustomize.ColumnMoving := True ;

  end;

//  cxTV.Columns[0].G

  cxGrid1Level1.GridView := cxTV ;

end;

procedure TfrmQueryExecute.cxButton2Click(Sender: TObject);
var linha, coluna : integer;
    var planilha : variant;
    var valorcampo : string;
begin

    if qryResultado.eof then
    begin
        ShowMessage('Nenhum resultado para exportar.') ;
        exit ;
    end ;

    planilha:= CreateoleObject('Excel.Application');
    planilha.WorkBooks.add(1);
    planilha.caption := 'Posi��o de t�tulos em aberto';
    planilha.visible := true;

    for linha := 0 to qryResultado.RecordCount - 1 do
    begin
       for coluna := 1 to qryResultado.FieldCount do
       begin
         valorcampo := qryResultado.Fields[coluna - 1].AsString;
         planilha.cells[linha + 2,coluna] := valorCampo;
       end;
       qryResultado.Next;
    end;
    for coluna := 1 to qryResultado.FieldCount do
    begin
       valorcampo := qryResultado.Fields[coluna - 1].DisplayLabel;
       planilha.cells[1,coluna] := valorcampo;
    end;
    planilha.columns.Autofit;



end;

procedure TfrmQueryExecute.cxButton3Click(Sender: TObject);
begin
    Close ;
end;

initialization
    RegisterClass(TfrmQueryExecute) ;
    
end.
