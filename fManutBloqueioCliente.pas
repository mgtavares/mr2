unit fManutBloqueioCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons,
  GridsEh, DBGridEh, cxPC, cxControls, Menus, DBGridEhGrouping;

type
  TfrmManutBloqueioCliente = class(TfrmCadastro_Padrao)
    qryMasternCdTerceiro: TIntegerField;
    qryMastercIDExterno: TStringField;
    qryMastercNmTerceiro: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMastercRG: TStringField;
    qryMasterdDtCadastro: TDateTimeField;
    qryMasterdDtNasc: TDateTimeField;
    qryMasternValLimiteCred: TBCDField;
    qryMasternValCreditoUtil: TBCDField;
    qryMasternValPedidoAberto: TBCDField;
    qryMasternValRecAtraso: TBCDField;
    qryMasternValChequePendComp: TBCDField;
    qryMasternPercEntrada: TBCDField;
    qryMasternSaldoLimiteCredito: TBCDField;
    qryMastercNmTabTipoSituacao: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    qryMasterdDtNegativacao: TDateTimeField;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    qryMastercFlgBloqAtuLimAutom: TIntegerField;
    btCalcular: TcxButton;
    qryMasternCdTabTipoSituacao: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh5: TDBGridEh;
    DBGridEh4: TDBGridEh;
    qryBloqueios: TADOQuery;
    qryBloqueiosnCdRestricaoVendaTerceiro: TAutoIncField;
    qryBloqueiosnCdTabTipoRestricaoVenda: TIntegerField;
    qryBloqueioscNmTabTipoRestricaoVenda: TStringField;
    qryBloqueiosnCdUsuarioBloqueio: TIntegerField;
    qryBloqueioscNmUsuario: TStringField;
    qryBloqueiosnCdLojaBloqueio: TIntegerField;
    qryBloqueiosdDtBloqueio: TDateTimeField;
    qryBloqueioscFlgAutomatico: TIntegerField;
    qryBloqueioscObs: TStringField;
    qryBloqueioscFlgLiberar: TIntegerField;
    dsBloqueios: TDataSource;
    dsDesbloqueados: TDataSource;
    qryDesbloqueados: TADOQuery;
    AutoIncField1: TAutoIncField;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    IntegerField2: TIntegerField;
    StringField2: TStringField;
    IntegerField3: TIntegerField;
    DateTimeField1: TDateTimeField;
    IntegerField4: TIntegerField;
    StringField3: TStringField;
    qryDesbloqueadosnCdLojaDesbloqueio: TIntegerField;
    qryDesbloqueadosdDtDesbloqueio: TDateTimeField;
    qryDesbloqueadosnCdUsuarioDesbloqueio: TIntegerField;
    qryDesbloqueadoscNmUsuarioDesb: TStringField;
    qryDesbloqueadoscObsDesbloqueio: TStringField;
    PopupMenu1: TPopupMenu;
    Desbloquear1: TMenuItem;
    SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO: TADOStoredProc;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit16Change(Sender: TObject);
    procedure btnAuditoriaClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure cxTabSheet2Show(Sender: TObject);
    procedure btCalcularClick(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure Desbloquear1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmManutBloqueioCliente: TfrmManutBloqueioCliente;

implementation

uses fAprovacaoClienteVarejo_LimiteInicial, fTrilhaClienteVarejo,
  fHistoricoLimiteCredito, fMenu, fManutBloqueioCliente_Inserir;

{$R *.dfm}

procedure TfrmManutBloqueioCliente.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster   := 'CLIENTECREDIARIO' ;
  nCdTabelaSistema  := 13 ;
  nCdConsultaPadrao := 180 ;
  bCodigoAutomatico := False ;
  bAutoEdit         := False ;

end;

procedure TfrmManutBloqueioCliente.DBEdit16Change(Sender: TObject);
begin
  inherited;
  if (DBedit16.Text <> '') then
  begin
      DBEdit16.Color      := clRed ;
      DBEdit16.Font.Color := clWhite ;
  end
  else
  begin
    DBEDit16.Color      := $00E9E4E4  ;
    DBEdit16.Font.Color := clBlack ;
  end ;

end;

procedure TfrmManutBloqueioCliente.btnAuditoriaClick(Sender: TObject);
var
  objForm : TfrmTrilhaClienteVarejo;
begin

  if (not qryMaster.Active or qryMaster.eof) then
  begin
      MensagemAlerta('Nenhum cliente selecionado.') ;
      abort ;
  end ;

  objForm := TfrmTrilhaClienteVarejo.Create(nil);

  PosicionaQuery(objForm.qryLog, qryMasternCdTerceiro.asString);

  if (objForm.qryLog.Eof) then
  begin
      MensagemAlerta('Nenhum registro de alteração para este cliente.') ;
      exit ;
  end ;

  objForm.qryLog.Last;
  showForm(objForm,true);

end;

procedure TfrmManutBloqueioCliente.cxButton1Click(Sender: TObject);
begin
  {inherited;}

end;

procedure TfrmManutBloqueioCliente.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  
end;

procedure TfrmManutBloqueioCliente.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

  if (qryMaster.eof) then
      exit ;

  qryBloqueios.Close ;
  qryBloqueios.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryBloqueios, qryMasternCdTerceiro.AsString) ;

  qryDesbloqueados.Close ;
  
end;

procedure TfrmManutBloqueioCliente.cxTabSheet2Show(Sender: TObject);
begin
  inherited;

  if (qryMaster.eof) then
      exit ;

  PosicionaQuery(qryDesbloqueados, qryMasternCdTerceiro.AsString) ;

end;

procedure TfrmManutBloqueioCliente.btCalcularClick(Sender: TObject);
var
  objForm : TfrmManutBloqueioCliente_Inserir;
begin
  inherited;
  
  if (not qryMaster.Active or qryMaster.eof) then
  begin
      MensagemAlerta('Nenhum cliente selecionado.') ;
      abort ;
  end ;

  objForm := TfrmManutBloqueioCliente_Inserir.Create(nil);
  
  objForm.nCdTerceiro := qryMasternCdTerceiro.Value ;
  showForm(objForm,true) ;

  qryBloqueios.Close ;
  qryBloqueios.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryBloqueios, qryMasternCdTerceiro.AsString) ;

end;

procedure TfrmManutBloqueioCliente.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryBloqueios.Close ;
  qryDesbloqueados.Close ;
  
end;

procedure TfrmManutBloqueioCliente.btSalvarClick(Sender: TObject);
var
  bAlteracao : boolean ;
begin
  {inherited;}

  bAlteracao := True ;

  if (qryBloqueios.Active) then
  begin
      qryBloqueios.First ;

      while not qryBloqueios.Eof do
      begin

          if (qryBloqueios.RecordStatus = [rsModified]) or (qryBloqueios.RecordStatus = [rsNew]) then
          begin
              bAlteracao := True ;
              break ;
          end ;

          qryBloqueios.Next;
      end ;

      qryBloqueios.First ;
  end ;

  if not (bAlteracao) then
  begin
      MensagemAlerta('Nenhum novo bloqueio adicionado.') ;
      abort ;
  end ;

  case MessageDlg('Confirma a atualização da lista de bloqueios ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:abort ;
  end ;

  cxPageControl1.ActivePageIndex := 0 ;

  frmMenu.Connection.BeginTrans;

  try
      if (qryBloqueios.State <> dsInactive) then
          if (qryBloqueios.State <> dsBrowse) then
              qryBloqueios.Post ;

      qryBloqueios.UpdateBatch();

  except
      frmMenu.Connection.RollbackTrans;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Informações Atualizadas com Sucesso!') ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmManutBloqueioCliente.Desbloquear1Click(Sender: TObject);
var
    cObsDesbloqueio : string ;
begin
  inherited;

  InputQuery('Desbloqueio','Motivo Desbloqueio',cObsDesbloqueio) ;

  if (cObsDesbloqueio = '') then
  begin
      MensagemErro('Informe o motivo do desbloqueio.') ;
      exit ;
  end ;

  if (MessageDlg('Confirma desbloqueio ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  try
      SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO.Close ;
      SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdRestricaoVendaTerceiro').Value := qryBloqueiosnCdRestricaoVendaTerceiro.Value ;
      SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdUsuarioDesbloqueio').Value     := frmMenu.nCdUsuarioLogado;
      SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdLojaDesbloqueio').Value        := frmMenu.nCdLojaAtiva;
      SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@cOBSDesbloqueio').Value           := cObsDesbloqueio;
      SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO.ExecProc;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  qryBloqueios.Close ;
  qryBloqueios.Open ;

end;

procedure TfrmManutBloqueioCliente.PopupMenu1Popup(Sender: TObject);
begin
  inherited;
  Desbloquear1.Enabled := ((qryBloqueios.Active) and (qryBloqueioscFlgLiberar.Value = 1))

end;

initialization
    RegisterClass(TfrmManutBloqueioCliente) ;

end.
