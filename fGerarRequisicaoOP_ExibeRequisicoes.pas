unit fGerarRequisicaoOP_ExibeRequisicoes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, cxPC,
  cxControls, DBGridEhGrouping, GridsEh, DBGridEh, DB, ADODB;

type
  TfrmGerarRequisicaoOP_ExibeRequisicoes = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxPageControl2: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryRequisicoes: TADOQuery;
    qryRequisicoesnCdRequisicao: TIntegerField;
    qryRequisicoesnCdEmpresa: TIntegerField;
    qryRequisicoesnCdLoja: TIntegerField;
    qryRequisicoesnCdTipoRequisicao: TIntegerField;
    qryRequisicoesdDtRequisicao: TDateTimeField;
    qryRequisicoesdDtAutor: TDateTimeField;
    qryRequisicoesnCdUsuarioAutor: TIntegerField;
    qryRequisicoescNmSolicitante: TStringField;
    qryRequisicoesnCdTabTipoRequis: TIntegerField;
    qryRequisicoescNrFormulario: TStringField;
    qryRequisicoesnCdTabStatusRequis: TIntegerField;
    qryRequisicoescOBS: TMemoField;
    qryRequisicoesnCdSetor: TIntegerField;
    qryRequisicoesnCdCC: TIntegerField;
    qryRequisicoescFlgAutomatica: TIntegerField;
    qryRequisicoesnValRequisicao: TBCDField;
    qryRequisicoesdDtFinalizacao: TDateTimeField;
    qryRequisicoesnCdServidorOrigem: TIntegerField;
    qryRequisicoesdDtReplicacao: TDateTimeField;
    qryRequisicoesnCdOrdemProducao: TIntegerField;
    qryRequisicoesnCdEtapaOrdemProducao: TIntegerField;
    dsRequisicoes: TDataSource;
    qryRequisicoescNmTabStatusRequis: TStringField;
    qryItemRequisicao: TADOQuery;
    dsItemRequisicao: TDataSource;
    qryItemRequisicaonCdProduto: TIntegerField;
    qryItemRequisicaocNmItem: TStringField;
    qryItemRequisicaocReferencia: TStringField;
    qryItemRequisicaonQtdeReq: TBCDField;
    qryItemRequisicaocUnidadeMedida: TStringField;
    qryItemRequisicaonQtdeAtend: TBCDField;
    qryItemRequisicaonQtdeCanc: TBCDField;
    qryItemRequisicaonSaldoReq: TBCDField;
    qryItemRequisicaonValUnitario: TBCDField;
    qryItemRequisicaonValTotal: TBCDField;
    DBGridEh2: TDBGridEh;
    procedure qryRequisicoesAfterScroll(DataSet: TDataSet);
    procedure exibeRequisicoes( nCdOrdemProducao  , nCdEtapaOrdemProducao : integer );
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGerarRequisicaoOP_ExibeRequisicoes: TfrmGerarRequisicaoOP_ExibeRequisicoes;

implementation

uses rRequisicaoOP ;

{$R *.dfm}

procedure TfrmGerarRequisicaoOP_ExibeRequisicoes.exibeRequisicoes(
  nCdOrdemProducao, nCdEtapaOrdemProducao: integer);
begin

    qryRequisicoes.Close;
    qryRequisicoes.Parameters.ParamByName('nCdOrdemProducao').Value      := nCdOrdemProducao ;
    qryRequisicoes.Parameters.ParamByName('nCdEtapaOrdemProducao').Value := nCdEtapaOrdemProducao ;
    qryRequisicoes.Open;

    Self.ShowModal ;

end;

procedure TfrmGerarRequisicaoOP_ExibeRequisicoes.qryRequisicoesAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery( qryItemRequisicao , qryRequisicoesnCdRequisicao.AsString ) ;

end;

procedure TfrmGerarRequisicaoOP_ExibeRequisicoes.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.SetFocus ;

  DBGridEh1.Font.Size      := 8 ;
  DBGridEh2.Font.Size      := 8 ;
  DBGridEh1.TitleFont.Size := 8 ;
  DBGridEh2.TitleFont.Size := 8 ;

end;

procedure TfrmGerarRequisicaoOP_ExibeRequisicoes.ToolButton1Click(
  Sender: TObject);
var
  objRel : TrptRequisicaoOP ;
begin
  inherited;

  if (not qryRequisicoes.Active) or (qryRequisicoes.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhuma requisição selecionada.') ;
      abort ;
  end ;

  objRel := TrptRequisicaoOP.Create( Self ) ;

  PosicionaQuery( objRel.qryRequisicaoOP , qryRequisicoesnCdRequisicao.asString ) ;

  try

      objRel.QuickRep1.PreviewModal;

  finally

      freeAndNil( objRel ) ;

  end ;

end;

end.
