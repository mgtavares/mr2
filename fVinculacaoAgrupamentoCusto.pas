unit fVinculacaoAgrupamentoCusto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, DB, ADODB, GridsEh, DBGridEh;

type
  TfrmVinculacaoAgrupamentoCusto = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryAgrupamento: TADOQuery;
    dsAgrupamento: TDataSource;
    qryAgrupamentonCdAgrupamentoCusto: TIntegerField;
    qryAgrupamentocNmAgrupamentoCusto: TStringField;
    qryAgrupamentonCdEmpresa: TIntegerField;
    qryAgrupamentocNmEmpresa: TStringField;
    qryAgrupamentocMascara: TStringField;
    qryAgrupamentoiMaxNiveis: TIntegerField;
    qryAgrupamentocFlgAtivo: TStringField;
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVinculacaoAgrupamentoCusto: TfrmVinculacaoAgrupamentoCusto;

implementation

uses fAgrupamentoCusto_Estrutura;

{$R *.dfm}

procedure TfrmVinculacaoAgrupamentoCusto.FormShow(Sender: TObject);
begin
  inherited;

  qryAgrupamento.Close;
  qryAgrupamento.Open;

  DBGridEh1.Align := alClient ;
  DBGridEh1.SetFocus;
  
end;

procedure TfrmVinculacaoAgrupamentoCusto.DBGridEh1DblClick(
  Sender: TObject);
var
  objForm : TfrmAgrupamentoCusto_Estrutura ;
begin
  inherited;

  if not qryAgrupamento.IsEmpty then
  begin

      objForm := TfrmAgrupamentoCusto_Estrutura.Create(Self) ;
      objForm.nCdAgrupamentoCusto := qryAgrupamentonCdAgrupamentoCusto.Value;
      objForm.cNmAgrupamentoCusto := qryAgrupamentocNmAgrupamentoCusto.Value;
      objForm.cFormatoMascara     := qryAgrupamentocMascara.Value;

      showForm( objForm, TRUE ) ;

  end ;


end;

initialization
    RegisterClass( TfrmVinculacaoAgrupamentoCusto ) ;

end.
