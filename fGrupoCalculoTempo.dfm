inherited frmGrupoCalculoTempo: TfrmGrupoCalculoTempo
  Left = 207
  Top = 187
  Width = 942
  Height = 530
  Caption = 'Grupo Calculo Tempo'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 926
    Height = 469
  end
  object Label1: TLabel [1]
    Left = 24
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 13
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  inherited ToolBar2: TToolBar
    Width = 926
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 64
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdGrupoCalculoTempo'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 64
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmGrupoCalculoTempo'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM GrupoCalculoTempo'
      ' WHERE nCdGrupoCalculoTempo = :nPK')
    object qryMasternCdGrupoCalculoTempo: TIntegerField
      FieldName = 'nCdGrupoCalculoTempo'
    end
    object qryMastercNmGrupoCalculoTempo: TStringField
      FieldName = 'cNmGrupoCalculoTempo'
      Size = 50
    end
  end
end
