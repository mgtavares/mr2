�
 TRPTREMESSANUM_VIEW 0�  TPF0TrptRemessaNum_viewrptRemessaNum_viewLeft Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSet
qryRemessaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameTahoma
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinAutoPrintIfEmpty	
SnapToGrid	UnitsMMZoomd TQRBandQRBand1LeftTopWidthHeight� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values������Z�@��������	@ BandTyperbPageHeader TQRLabelQRLabel1LeftTop"Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@�������@������r�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption%   Comprovante de Remessa de NumeráriosColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel
lblEmpresaLeftTopWidth3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@UUUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
lblEmpresaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData1Left�Top"Width8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      H�	@�������@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	ColorclWhiteDataqrsDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFontSize  TQRLabelQRLabel3Left�TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������	@UUUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   Pág.:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData2Left�TopWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@TUUUUU��	@UUUUUUU�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	ColorclWhiteDataqrsPageNumberFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFontSize  TQRLabelQRLabel2LeftKTopXWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      p�@VUUUUU��@     @�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionLojaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel4Left#TophWidth=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU5�@UUUUUU��@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionConta OrigemColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel25LeftTop� WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      ��@UUUUUUE�@      P�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Valor em EspécieColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText16LefthTopXWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU��@VUUUUU��@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryRemessa	DataFieldcNmLojaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText17LefthTophWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU��@UUUUUU��@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryRemessa	DataFieldnCdContaOrigemFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText18LefthTopvWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU��@�������@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryRemessa	DataFieldnCdContaDestinoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel6LeftTopvWidthBHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      ��@�������@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionConta DestinoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel27LeftTophWidth=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU1�@UUUUUU��@UUUUUUe�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionData RemessaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText19Left`TophWidthaHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@VUUUUU��@UUUUUU��@������R�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryRemessa	DataField
dDtRemessaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText20Left�TophWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      Ж	@UUUUUU��@UUUUUUi�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryRemessa	DataFieldcNmUsuarioCadFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel28LeftTopvWidthBHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@�������@�������@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionProcessamentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText21Left`TopvWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@VUUUUU��@�������@UUUUUU=�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryRemessa	DataFielddDtFechFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel29Left-Top� Width3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@�������@      p�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   ObservaçãoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText22LefthTop� WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU��@      p�@UUUUUU�	@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryRemessa	DataFieldcOBSFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText23LefthTop� WidthaHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU��@UUUUUUE�@������R�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryRemessa	DataFieldnValDinheiroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold Mask#,##0.00
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel19LeftTopHWidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������J�@      ��@������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Código RemessaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText11LefthTopHWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU��@      ��@UUUUUU%�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryRemessa	DataFieldnCdRemessaNumFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel20LeftTop� WidthyHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������*�@UUUUUU��@�������@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption* Documentos ExtraviadosColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape6Left Top Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@                    ������t�	@ 	Pen.WidthShape
qrsHorLine  TQRShapeQRShape1Left Top0Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@                 �@������t�	@ 	Pen.WidthShape
qrsHorLine   TQRSubDetailQRSubDetail1LeftTop� WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values       �@��������	@ MasterOwnerDataSet
qryCheques
FooterBandQRBand2PrintBeforePrintIfEmpty	 	TQRDBText	QRDBText1LeftTop Width)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@       �@          UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSet
qryCheques	DataFieldnCdBancoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText2Left�Top Width3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUUa�	@                ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSet
qryCheques	DataField
nValChequeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask#,##0.00OnPrintQRDBText2Print
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText24LeftXTop Width)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@VUUUUU��@          UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSet
qryCheques	DataFieldcAgenciaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText25Left� Top WidthqHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU�@          UUUUUU}�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryCheques	DataFieldcContaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText26LeftTop WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@�������@          �������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryCheques	DataFieldcDigitoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText27Left(Top Width9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@��������@                Ж@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryCheques	DataField	iNrChequeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask,0
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText28LefthTop WidthiHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@�������@                �@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryCheques	DataFieldcCNPJCPFFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText29Left0Top Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU5�	@                �@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryCheques	DataFieldcNmTerceiroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText8Left Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@                    �������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet
qryCheques	DataFieldcStatusFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize   TQRGroupQRGroup1LeftTop� WidthHeight(Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values��������@��������	@ MasterQRSubDetail1ReprintOnNewPage	 TQRLabelQRLabel7LeftTopWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@UUUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionCHEQUESColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel35LeftTopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@       �@       �@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionBANCOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel36LeftXTopWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@VUUUUU��@       �@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   AGÊNCIAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel37Left� TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU�@       �@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionCONTAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel38LeftTopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@�������@       �@UUUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionDIGColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel39Left�TopWidth=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      ��	@       �@UUUUUUe�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionVALOR CHEQUEColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel40Left@TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@��������@       �@������
�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionCHEQUEColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel41LefthTopWidth)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@�������@       �@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionCNPJ/CPFColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel42Left3TopWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU3�	@       �@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionCLIENTEColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand2LeftTop� WidthHeight Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageLinkBandQRGroup1Size.ValuesUUUUUUU�@��������	@ BandTyperbGroupFooter TQRLabelQRLabel8LefteTopWidthVHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@      $�@UUUUUUU�@��������@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionTotal de Cheques:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellblTotChequeLeft�TopWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUe�	@UUUUUUU�@UUUUUU%�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption0ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRSubDetailQRSubDetail2LeftTopEWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values       �@��������	@ MasterOwnerDataSet	qryCartao
FooterBandQRBand3PrintBeforePrintIfEmpty	 	TQRDBText	QRDBText3Left�Top WidthBHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      ��	@                ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSet	qryCartao	DataFieldnValTransacaoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask#,##0.00OnPrintQRDBText3Print
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText4LeftTop Width=HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@       �@          UUUUUUe�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSet	qryCartao	DataFielddDtTransacaoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText30Left� Top Width[HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUUU�@          TUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSet	qryCartao	DataFieldcNmOperadoraCartaoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText31Left2Top WidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������g�@          ������ڻ@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSet	qryCartao	DataFieldiNrAutorizacaoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText32Left�Top Width)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      ��	@          UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSet	qryCartao	DataFieldiNrDoctoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask,0
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText10Left�Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUUU� �                ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSet	qryCartao	DataFieldcStatusFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand3LeftTopQWidthHeight Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRight
AfterPrintQRBand3AfterPrintAlignToBottomColorclWhiteForceNewColumnForceNewPageLinkBandQRGroup2Size.ValuesUUUUUUU�@��������	@ BandTyperbGroupFooter TQRLabel	QRLabel10LeftTopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@������޹@UUUUUUU�@TUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption!   Total de Comprovantes de Cartão:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabellblTotCartaoLeft�TopWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUe�	@UUUUUUU�@UUUUUU%�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption0ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRGroupQRGroup2LeftTopWidthHeight(Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values��������@��������	@ MasterQRSubDetail2ReprintOnNewPage	 TQRLabelQRLabel9LeftTopWidthoHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@UUUUUUU�@      ؒ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   COMPROVANTES DE CARTÃOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel30LeftTopWidthGHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@       �@       �@������ڻ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   DATA TRANSAÇÃOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel31Left� TopWidth.HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUUU�@       �@������j�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption	OPERADORAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel32Left TopWidthYHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      ��@       �@������z�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption   NÚM. AUTORIZAÇÃOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel33Left�TopWidth1HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������J�	@       �@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption	DOCUMENTOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel34Left�TopWidthVHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@�������	@       �@��������@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption   VALOR TRANSAÇÃOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRSubDetailQRSubDetail3LeftTop�WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values       �@��������	@ MasterOwnerDataSetqryCrediario
FooterBandQRBand4PrintBeforePrintIfEmpty	 	TQRDBText	QRDBText5LeftTop WidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@       �@          UUUUUU%�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryCrediario	DataFieldnCdCrediarioFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText6LefthTop WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU��@                P�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryCrediario	DataFielddDtVendaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText7Left� Top Width	HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@       �@          UUUUUUI�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryCrediario	DataFieldcNmTerceiroFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText9Left�Top WidthBHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      ��	@                ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetqryCrediario	DataFieldnValCrediarioFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style Mask#,##0.00OnPrintQRDBText9Print
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText12Left Top WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@                    �������@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetqryCrediario	DataFieldcStatusFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize   TQRGroupQRGroup3LeftTopqWidthHeight0Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values       �@��������	@ MasterQRSubDetail3ReprintOnNewPage	 TQRLabel	QRLabel11LeftTopWidthoHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@UUUUUUU�@      ؒ@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   COMPROVANTES CREDIÁRIOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel12LeftTop Width.HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@       �@UUUUUUU�@������j�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
   CREDIÁRIOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel13LefthTop Width3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU��@UUUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
DATA VENDAColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel14Left� Top WidthaHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@       �@UUUUUUU�@������R�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchCaptionCLIENTEColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel15Left�Top WidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      ��	@UUUUUUU�@      P�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption   VALOR CREDIÁRIOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand4LeftTop�WidthHeight Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageLinkBandQRGroup3Size.ValuesUUUUUUU�@��������	@ BandTyperbGroupFooter TQRLabellblTotCrediarioLeft�TopWidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUe�	@UUUUUUU�@UUUUUU%�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchCaption0ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel18LeftTopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUE�@UUUUUUU�@VUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption$   Total de Comprovantes de Crediário:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand6LeftTop�WidthHeight� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUUU�@��������	@ BandTyperbPageFooter TQRLabel	QRLabel16LeftTop(WidthyHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@UUUUUUU�@��������@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   RESPONSÁVEL CONTA ORIGEMColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape2LeftTopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@       �@      �@ Shape
qrsHorLine  TQRShapeQRShape3LeftTopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@��������	@       �@      �@ Shape
qrsHorLine  TQRLabel	QRLabel17LeftTop(Width~HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@��������	@��������@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   RESPONSÁVEL CONTA DESTINOColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRImageQRImage1LeftTopBWidth)Height)Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUUU�@      ��@UUUUUU��@ Picture.Data
�  
TJPEGImage�  ���� JFIF  ` `  �� C 		
 $.' ",#(7),01444'9=82<.342�� C			2!!22222222222222222222222222222222222222222222222222��  #  " ��           	
�� �   } !1AQa"q2���#B��R��$3br�	
%&'()*456789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz���������������������������������������������������������������������������        	
�� �  w !1AQaq"2�B����	#3R�br�
$4�%�&'()*56789:CDEFGHIJSTUVWXYZcdefghijstuvwxyz��������������������������������������������������������������������������   ? ��&H�',z(䚭q8D�s �>˟��ϵWO5�����͸�3������o2��{��h��|��\��� �����4���hX���g2G	K@3��������r����n8�PkS#+j3y�y[x�����MMq���%E%z���(�����߈IE� _����eC#��[�		*�g����肗K��N�s��~�C�-���guo(��'>��iwז�%ջ�|�c�d��P��YUt�8N6V�����Щ)�R���v5��h���nG�<��� 3P��Ƴ�--�츖\��?��
�%��[[�gt�`�o>��*�[��D��m����u	˯��z�Ly⿯Էګ^E��YA;HϵWT���x�ĳ�V�v�&�QE�PKv��  TQRLabel	QRLabel21Left9TopXWidth[HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      Ж@VUUUUU��@TUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionwww.er2soft.com.brColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclOliveFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel22Left8TopHWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@������*�@      ��@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaptionER2SoftColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclOliveFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape4Left TophWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@          UUUUUU��@������t�	@ 	Pen.WidthShape
qrsHorLine  TQRShapeQRShape5Left Top8Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@          ������*�@������t�	@ 	Pen.WidthShape
qrsHorLine   	TADOQuery
qryCheques
ConnectionfrmMenu.Connection
ParametersNamenPKDataType	ftInteger	Precision
SizeValue   SQL.StringsSELECT Cheque.nCdBanco      ,Cheque.cAgencia      ,Cheque.cConta      ,Cheque.cDigito      ,Cheque.cCNPJCPF      ,Cheque.iNrCheque      ,Cheque.nValCheque      ,cNmTerceiro,      ,CASE WHEN nCdStatusDocto = 2 THEN '*'            ELSE ' '       END cStatus  FROM ChequeRemessaNumO       INNER JOIN Cheque   ON Cheque.nCdCheque     = ChequeRemessaNum.nCdChequeK       LEFT  JOIN Terceiro ON Terceiro.nCdTerceiro = Cheque.nCdTerceiroResp WHERE nCdRemessaNum = :nPK8 ORDER BY nCdBanco, cAgencia, cConta, cDigito, iNrCheque LeftPTop�  TIntegerFieldqryChequesnCdBanco	FieldNamenCdBanco  TStringFieldqryChequescAgencia	FieldNamecAgencia	FixedChar	Size  TStringFieldqryChequescConta	FieldNamecConta	FixedChar	Size  TStringFieldqryChequescDigito	FieldNamecDigito	FixedChar	Size  TStringFieldqryChequescCNPJCPF	FieldNamecCNPJCPFSize  TIntegerFieldqryChequesiNrCheque	FieldName	iNrCheque  	TBCDFieldqryChequesnValCheque	FieldName
nValCheque	PrecisionSize  TStringFieldqryChequescNmTerceiro	FieldNamecNmTerceiroSize2  TStringFieldqryChequescStatus	FieldNamecStatusReadOnly	Size   	TADOQuery	qryCartao
ConnectionfrmMenu.Connection
ParametersNamenPKDataType	ftInteger	Precision
SizeValue   SQL.StringsSELECT dDtTransacao   "      ,cNmOperadoraCartao               ,iNrDocto          ,iNrAutorizacao /      ,nValTransacao                           ,      ,CASE WHEN nCdStatusDocto = 2 THEN '*'            ELSE ' '       END cStatus  FROM CartaoRemessaNumm       INNER JOIN TransacaoCartao ON TransacaoCartao.nCdTransacaoCartao = CartaoRemessaNum.nCdTransacaoCartaol       INNER JOIN OperadoraCartao ON OperadoraCartao.nCdOperadoraCartao = TransacaoCartao.nCdOperadoraCartao WHERE nCdRemessaNum = :nPK ORDER BY dDtTransacao Left�Top� TDateTimeFieldqryCartaodDtTransacao	FieldNamedDtTransacao  TStringFieldqryCartaocNmOperadoraCartao	FieldNamecNmOperadoraCartaoSize2  TIntegerFieldqryCartaoiNrDocto	FieldNameiNrDocto  TIntegerFieldqryCartaoiNrAutorizacao	FieldNameiNrAutorizacao  	TBCDFieldqryCartaonValTransacao	FieldNamenValTransacao	PrecisionSize  TStringFieldqryCartaocStatus	FieldNamecStatusReadOnly	Size   	TADOQueryqryCrediario
ConnectionfrmMenu.Connection
ParametersNamenPKDataType	ftInteger	Precision
SizeValue   SQL.StringsSELECT Crediario.nCdCrediario      ,dDtVenda                      ,cNmTerceiro      ,nValCrediario,      ,CASE WHEN nCdStatusDocto = 2 THEN '*'            ELSE ' '       END cStatus  FROM CrediarioRemessaNumX       INNER JOIN Crediario ON Crediario.nCdCrediario = CrediarioRemessaNum.nCdCrediarioM       INNER JOIN Terceiro  ON Terceiro.nCdTerceiro   = Crediario.nCdTerceiro WHERE nCdRemessaNum = :nPK ORDER BY nCdCrediario LeftTop� TAutoIncFieldqryCrediarionCdCrediario	FieldNamenCdCrediarioReadOnly	  TDateTimeFieldqryCrediariodDtVenda	FieldNamedDtVenda  TStringFieldqryCrediariocNmTerceiro	FieldNamecNmTerceiroSize2  	TBCDFieldqryCrediarionValCrediario	FieldNamenValCrediario	PrecisionSize  TStringFieldqryCrediariocStatus	FieldNamecStatusReadOnly	Size   	TADOQuery
qryRemessa
ConnectionfrmMenu.Connection
ParametersNamenPKDataType	ftInteger	Precision
SizeValue   SQL.StringsSELECT nCdRemessaNum       ,cNmEmpresa      ,cNmLoja%      ,Conta1.nCdConta nCdContaOrigem'      ,Conta2.nCdConta nCdContaDestino /      ,nValDinheiro                            $      ,Usu1.cNmUsuario cNmUsuarioCad      ,dDtRemessa                    ,dDtFech                       ,dDtCancel               :      ,cOBS                                                     ,dDtConfirma             )      ,Usu2.cNmUsuario cNmUsuarioConfirma  FROM RemessaNumY       INNER JOIN Empresa              ON Empresa.nCdEmpresa      = RemessaNum.nCdEmpresaV       LEFT  JOIN Loja                 ON Loja.nCdLoja            = RemessaNum.nCdLoja]       INNER JOIN ContaBancaria Conta1 ON Conta1.nCdContaBancaria = RemessaNum.nCdContaOrigem^       INNER JOIN ContaBancaria Conta2 ON Conta2.nCdContaBancaria = RemessaNum.nCdContaDestino\       INNER JOIN Usuario       Usu1   ON Usu1.nCdUsuario         = RemessaNum.nCdUsuarioCada       LEFT  JOIN Usuario       Usu2   ON Usu2.nCdUsuario         = RemessaNum.nCdUsuarioConfirma WHERE nCdRemessaNum = :nPK LeftpTop�  TIntegerFieldqryRemessanCdRemessaNum	FieldNamenCdRemessaNum  TStringFieldqryRemessacNmEmpresa	FieldName
cNmEmpresaSize2  TStringFieldqryRemessacNmLoja	FieldNamecNmLojaSize2  TStringFieldqryRemessanCdContaOrigem	FieldNamenCdContaOrigem	FixedChar	Size  TStringFieldqryRemessanCdContaDestino	FieldNamenCdContaDestino	FixedChar	Size  	TBCDFieldqryRemessanValDinheiro	FieldNamenValDinheiro	PrecisionSize  TStringFieldqryRemessacNmUsuarioCad	FieldNamecNmUsuarioCadSize2  TDateTimeFieldqryRemessadDtRemessa	FieldName
dDtRemessa  TDateTimeFieldqryRemessadDtFech	FieldNamedDtFech  TDateTimeFieldqryRemessadDtCancel	FieldName	dDtCancel  TStringFieldqryRemessacOBS	FieldNamecOBSSize2  TDateTimeFieldqryRemessadDtConfirma	FieldNamedDtConfirma  TStringFieldqryRemessacNmUsuarioConfirma	FieldNamecNmUsuarioConfirmaSize2    