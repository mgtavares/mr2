inherited frmSetor: TfrmSetor
  Left = 220
  Top = 121
  Width = 999
  Height = 451
  Caption = 'Setor'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 983
    Height = 388
  end
  object Label1: TLabel [1]
    Left = 37
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 48
    Top = 62
    Width = 27
    Height = 13
    Alignment = taRightJustify
    Caption = 'Setor'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 7
    Top = 86
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro Custo'
    FocusControl = DBEdit3
  end
  inherited ToolBar2: TToolBar
    Width = 983
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 80
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdSetor'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 80
    Top = 56
    Width = 645
    Height = 19
    DataField = 'cNmSetor'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [7]
    Left = 80
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdCC'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [8]
    Tag = 1
    Left = 148
    Top = 80
    Width = 577
    Height = 19
    DataField = 'cNmCC'
    DataSource = dsCC
    TabOrder = 4
  end
  object cxPageControl1: TcxPageControl [9]
    Left = 24
    Top = 113
    Width = 697
    Height = 272
    ActivePage = cxTabSheet2
    LookAndFeel.NativeStyle = True
    TabOrder = 5
    ClientRectBottom = 268
    ClientRectLeft = 4
    ClientRectRight = 693
    ClientRectTop = 24
    object cxTabSheet2: TcxTabSheet
      Caption = 'Usu'#225'rios x Autoriza'#231#227'o'
      ImageIndex = 1
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 689
        Height = 244
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuarioAutorSetor
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdSetor'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Title.Caption = 'Usu'#225'rios que podem Autorizar Requisi'#231#245'es neste Setor||C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Title.Caption = 
              'Usu'#225'rios que podem Autorizar Requisi'#231#245'es neste Setor| Nome Usu'#225'r' +
              'io'
            Width = 580
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet1: TcxTabSheet
      Caption = 'Usu'#225'rios x Utiliza'#231#227'o'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 689
        Height = 244
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuarioSetor
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh2Enter
        OnKeyUp = DBGridEh2KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdSetor'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Title.Caption = 'Usu'#225'rios que podem Gerar Requisi'#231#245'es neste Setor|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Usu'#225'rios que podem Gerar Requisi'#231#245'es neste Setor|Nome Usu'#225'rio'
            Width = 580
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSetor, cNmSetor, nCdCC'
      'FROM Setor'
      'WHERE nCdSetor = :nPK')
    Left = 736
    Top = 40
    object qryMasternCdSetor: TIntegerField
      FieldName = 'nCdSetor'
    end
    object qryMastercNmSetor: TStringField
      FieldName = 'cNmSetor'
      Size = 50
    end
    object qryMasternCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
  end
  inherited dsMaster: TDataSource
    Left = 736
    Top = 72
  end
  inherited qryID: TADOQuery
    Left = 768
    Top = 40
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 800
    Top = 40
  end
  inherited qryStat: TADOQuery
    Left = 768
    Top = 72
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 800
    Top = 72
  end
  inherited ImageList1: TImageList
    Left = 832
    Top = 40
  end
  object qryUsuarioAutorSetor: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioAutorSetorBeforePost
    OnCalcFields = qryUsuarioAutorSetorCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM UsuarioAutorSetor'
      'WHERE nCdSetor = :nPK')
    Left = 872
    Top = 72
    object qryUsuarioAutorSetornCdSetor: TIntegerField
      FieldName = 'nCdSetor'
    end
    object qryUsuarioAutorSetornCdUsuario: TIntegerField
      DisplayLabel = 'Usuarios que podem autorizar requisi'#231#245'es neste setor|C'#243'd'
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioAutorSetorcNmUsuario: TStringField
      DisplayLabel = 
        'Usuarios que podem autorizar requisi'#231#245'es neste setor|Nome Usu'#225'ri' +
        'o'
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 50
      Calculated = True
    end
    object qryUsuarioAutorSetornCdUsuarioAutorSetor: TIntegerField
      FieldName = 'nCdUsuarioAutorSetor'
    end
  end
  object dsUsuarioAutorSetor: TDataSource
    DataSet = qryUsuarioAutorSetor
    Left = 872
    Top = 40
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario, cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 904
    Top = 72
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC'
      '      ,cCdCC'
      '      ,cNmCC'
      '  FROM CentroCusto'
      ' WHERE nCdCC     = :nPK'
      '   AND iNivel    = 3'
      '   AND cFlgLanc  = 1'
      '   AND nCdStatus = 1')
    Left = 904
    Top = 40
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object qryUsuarioSetor: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioSetorBeforePost
    OnCalcFields = qryUsuarioSetorCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM UsuarioSetor'
      'WHERE nCdSetor = :nPK')
    Left = 936
    Top = 72
    object qryUsuarioSetornCdSetor: TIntegerField
      FieldName = 'nCdSetor'
    end
    object qryUsuarioSetornCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioSetorcNmUsuario: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Calculated = True
    end
    object qryUsuarioSetornCdUsuarioSetor: TIntegerField
      FieldName = 'nCdUsuarioSetor'
    end
  end
  object dsUsuarioSetor: TDataSource
    DataSet = qryUsuarioSetor
    Left = 736
    Top = 104
  end
  object dsCC: TDataSource
    DataSet = qryCC
    Left = 840
    Top = 72
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 936
    Top = 40
  end
end
