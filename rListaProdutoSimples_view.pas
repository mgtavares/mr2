unit rListaProdutoSimples_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptListaProdutoSimples_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    QRDBText14: TQRDBText;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutonCdProdutoPai: TIntegerField;
    qryProdutocReferencia: TStringField;
    qryProdutocNmProduto: TStringField;
    qryProdutocUnidadeMedida: TStringField;
    qryProdutocNmGrupoProduto: TStringField;
    qryProdutocNmMarca: TStringField;
    qryProdutocNmStatus: TStringField;
    qryProdutonCdTabTipoProduto: TIntegerField;
    qryProdutonCdDepartamento: TIntegerField;
    qryProdutocNmDepartamento: TStringField;
    qryProdutonCdCategoria: TIntegerField;
    qryProdutocNmCategoria: TStringField;
    qryProdutonCdSubCategoria: TIntegerField;
    qryProdutocNmSubCategoria: TStringField;
    qryProdutonCdSegmento: TIntegerField;
    qryProdutocNmSegmento: TStringField;
    qryProdutocFlgProdWeb: TStringField;
    QRGroup1: TQRGroup;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel11: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel17: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel14: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel13: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel12: TQRLabel;
    QRBand2: TQRBand;
    QRShape2: TQRShape;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptListaProdutoSimples_view: TrptListaProdutoSimples_view;

implementation

{$R *.dfm}

end.
