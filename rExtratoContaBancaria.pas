unit rExtratoContaBancaria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, StdCtrls, Mask, DBCtrls, DB, ADODB, ImgList,
  ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping, ToolCtrlsEh, GridsEh,
  DBGridEh, ER2Excel, cxPC, cxControls;

type
  TrptExtratoContaBancaria = class(TfrmRelatorio_Padrao)
    DataSource1: TDataSource;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancariaDadosConta: TStringField;
    qryContaBancarianValLimiteCredito: TBCDField;
    DBEdit1: TDBEdit;
    edtContaBancaria: TMaskEdit;
    Label1: TLabel;
    Label3: TLabel;
    edtDtExtrato: TMaskEdit;
    edtDtExtratoFinal: TMaskEdit;
    Label2: TLabel;
    ER2Excel1: TER2Excel;
    rgModeloImp: TRadioGroup;
    SPREL_EXTRATO_BANCARIO: TADOStoredProc;
    SPREL_EXTRATO_BANCARIOnCdItemExtrato: TAutoIncField;
    SPREL_EXTRATO_BANCARIOnCdContaBancaria: TIntegerField;
    SPREL_EXTRATO_BANCARIOnCdBanco: TIntegerField;
    SPREL_EXTRATO_BANCARIOcAgencia: TIntegerField;
    SPREL_EXTRATO_BANCARIOnCdConta: TStringField;
    SPREL_EXTRATO_BANCARIOdDtLancto: TDateTimeField;
    SPREL_EXTRATO_BANCARIOcHistorico: TStringField;
    SPREL_EXTRATO_BANCARIOcDocumento: TStringField;
    SPREL_EXTRATO_BANCARIOnValLancto: TBCDField;
    SPREL_EXTRATO_BANCARIOnSaldoFinal: TBCDField;
    SPREL_EXTRATO_BANCARIOcNmTitular: TStringField;
    SPREL_EXTRATO_BANCARIOcNmTipoLancto: TStringField;
    SPREL_EXTRATO_BANCARIOnSaldoLancto: TBCDField;
    DataSource2: TDataSource;
    procedure edtContaBancariaExit(Sender: TObject);
    procedure edtContaBancariaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptExtratoContaBancaria: TrptExtratoContaBancaria;

implementation

uses fMenu, fLookup_Padrao, rExtratoContaBancaria_view, QuickRpt;

{$R *.dfm}

procedure TrptExtratoContaBancaria.edtContaBancariaExit(Sender: TObject);
begin
  inherited;
  qryContaBancaria.Close ;
  PosicionaQuery(qryContaBancaria, edtContaBancaria.Text) ;

end;

procedure TrptExtratoContaBancaria.edtContaBancariaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(37,'EXISTS(SELECT 1 FROM UsuarioContaBancaria UCB WHERE UCB.nCdContaBancaria = ContaBancaria.nCdContaBancaria AND UCB.nCdUsuario = '+ intToStr(frmMenu.nCdUsuarioLogado)+') AND ContaBancaria.nCdEmpresa = @@Empresa AND ((ContaBancaria.nCdLoja IS NULL) OR EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdUsuario = @@Usuario AND UL.nCdLoja = ContaBancaria.nCdLoja))'); // AND (cFlgCofre = 1 OR cFlgCaixa = 0)') ;

        If (nPK > 0) then
        begin
            qryContaBancaria.Close ;
            {qryContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;}
            PosicionaQuery(qryContaBancaria, IntToStr(nPK)) ;
            edtContaBancaria.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptExtratoContaBancaria.ToolButton1Click(Sender: TObject);
var
  objRel : TrptExtratoContaBancaria_view;
  iLinha : Integer;
begin
  inherited;

  if (qryContaBancaria.Eof) then
  begin
      MensagemAlerta('Selecione a conta banc�ria.') ;
      exit ;
  end ;

  if (trim(edtDtExtrato.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data inicial.') ;
      edtDtExtrato.SetFocus ;
      exit ;
  end ;

  if (trim(edtDtExtratoFinal.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data Final.') ;
      edtDtExtratoFinal.SetFocus ;
      exit ;
  end ;


  objRel := TrptExtratoContaBancaria_view.Create(nil);

  try
      try
          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblPeriodo.Caption := edtDtExtrato.Text + ' at� ' + edtDtExtratoFinal.Text ;

          objRel.SPREL_EXTRATO_BANCARIO.Close ;
          objRel.SPREL_EXTRATO_BANCARIO.Parameters.ParamByName('@nCdContaBancaria').Value := qryContaBancarianCdContaBancaria.Value ;
          objRel.SPREL_EXTRATO_BANCARIO.Parameters.ParamByName('@cDtInicial').Value       := edtDtExtrato.Text ;
          objRel.SPREL_EXTRATO_BANCARIO.Parameters.ParamByName('@cDtFinal').Value         := edtDtExtratoFinal.Text ;
          objRel.SPREL_EXTRATO_BANCARIO.Open;

          case (rgModeloImp.ItemIndex) of
              0 : objRel.PreviewModal;
              1 : begin
                      frmMenu.mensagemUsuario('Exportando Planilha...');

                      { -- formata c�lulas -- }
                      ER2Excel1.Celula['A1'].FontName := 'Calibri';
                      ER2Excel1.Celula['A2'].FontName := 'Calibri';
                      ER2Excel1.Celula['A1'].FontSize := 10;
                      ER2Excel1.Celula['A2'].FontSize := 10;

                      ER2Excel1.Celula['A1'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['A2'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['A3'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['A4'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['I4'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['A5'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['A6'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['A7'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['C7'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['F7'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['M7'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['Q7'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['S7'].Background := RGB(221, 221, 221);

                      ER2Excel1.Celula['A1'].Mesclar('T1');
                      ER2Excel1.Celula['A2'].Mesclar('T2');
                      ER2Excel1.Celula['A3'].Mesclar('T3');
                      ER2Excel1.Celula['A4'].Mesclar('H4');
                      ER2Excel1.Celula['I4'].Mesclar('T4');
                      ER2Excel1.Celula['A5'].Mesclar('T5');
                      ER2Excel1.Celula['A6'].Mesclar('T6');
                      ER2Excel1.Celula['A7'].Mesclar('B7');
                      ER2Excel1.Celula['C7'].Mesclar('E7');
                      ER2Excel1.Celula['F7'].Mesclar('L7');
                      ER2Excel1.Celula['M7'].Mesclar('P7');
                      ER2Excel1.Celula['Q7'].Mesclar('R7');
                      ER2Excel1.Celula['S7'].Mesclar('T7');

                      { -- inseri informa��es do cabe�alho -- }
                      ER2Excel1.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                      ER2Excel1.Celula['A2'].Text := 'Extrato Conta/Caixa.';
                      ER2Excel1.Celula['A4'].Text := 'Banco: ' + objRel.SPREL_EXTRATO_BANCARIOnCdBanco.AsString;
                      ER2Excel1.Celula['I4'].Text := 'Conta: ' + objRel.SPREL_EXTRATO_BANCARIOnCdConta.AsString;
                      ER2Excel1.Celula['A4'].Text := 'Titular: ' + objRel.SPREL_EXTRATO_BANCARIOcNmTitular.Value;
                      ER2Excel1.Celula['A5'].Text := 'Per�odo de Lan�amentos: ' + edtDtExtrato.Text + ' at� ' + edtDtExtratoFinal.Text;
                      ER2Excel1.Celula['A7'].Text := 'DATA';
                      ER2Excel1.Celula['C7'].Text := 'TIPO DE LAN�AMENTO';
                      ER2Excel1.Celula['F7'].Text := 'HIST�RICO';
                      ER2Excel1.Celula['M7'].Text := 'DOCUMENTO';
                      ER2Excel1.Celula['Q7'].Text := 'VALOR';
                      ER2Excel1.Celula['S7'].Text := 'SALDO';

                      iLinha := 8;
                      
                      objRel.SPREL_EXTRATO_BANCARIO.First;

                      while (not objRel.SPREL_EXTRATO_BANCARIO.Eof) do
                      begin
                          ER2Excel1.Celula['A' + IntToStr(iLinha)].Text := objRel.SPREL_EXTRATO_BANCARIOdDtLancto.Value;
                          ER2Excel1.Celula['C' + IntToStr(iLinha)].Text := objRel.SPREL_EXTRATO_BANCARIOcNmTipoLancto.Value;
                          ER2Excel1.Celula['F' + IntToStr(iLinha)].Text := objRel.SPREL_EXTRATO_BANCARIOcHistorico.Value;
                          ER2Excel1.Celula['M' + IntToStr(iLinha)].Text := objRel.SPREL_EXTRATO_BANCARIOcDocumento.Value;
                          ER2Excel1.Celula['Q' + IntToStr(iLinha)].Text := objRel.SPREL_EXTRATO_BANCARIOnValLancto.Value;
                          ER2Excel1.Celula['S' + IntToStr(iLinha)].Text := objRel.SPREL_EXTRATO_BANCARIOnSaldoLancto.Value;

                          ER2Excel1.Celula['Q' + IntToStr(iLinha)].Mascara := '#.##0,00';
                          ER2Excel1.Celula['S' + IntToStr(iLinha)].Mascara := '#.##0,00';

                          Inc(iLinha);

                          objRel.SPREL_EXTRATO_BANCARIO.Next;
                      end;

                      ER2Excel1.Celula['Q8'].Text := '';
                      ER2Excel1.Celula['Q' + IntToStr(iLinha - 1)].Text := '';

                      ER2Excel1.ExportXLS;
                      ER2Excel1.CleanupInstance;

                      frmMenu.mensagemUsuario('');
                  end;
          end;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TrptExtratoContaBancaria.ToolButton5Click(Sender: TObject);
begin
  inherited;

  SPREL_EXTRATO_BANCARIO.Close ;
  SPREL_EXTRATO_BANCARIO.Parameters.ParamByName('@nCdContaBancaria').Value := qryContaBancarianCdContaBancaria.Value ;
  SPREL_EXTRATO_BANCARIO.Parameters.ParamByName('@cDtInicial').Value       := edtDtExtrato.Text ;
  SPREL_EXTRATO_BANCARIO.Parameters.ParamByName('@cDtFinal').Value         := edtDtExtratoFinal.Text ;
  SPREL_EXTRATO_BANCARIO.Open;

end;

initialization
    RegisterClass(TrptExtratoContaBancaria) ;

end.
