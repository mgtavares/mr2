inherited frmCartaCorrecaoNFe: TfrmCartaCorrecaoNFe
  Left = 207
  Top = 117
  Width = 1097
  Height = 439
  Caption = 'Carta de Corre'#231#227'o Eletr'#244'nica CC-e'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 145
    Width = 1081
    Height = 256
  end
  inherited ToolBar1: TToolBar
    Width = 1081
    ButtonWidth = 117
    inherited ToolButton1: TToolButton
      Caption = 'Executar &Consulta'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 117
    end
    inherited ToolButton2: TToolButton
      Left = 125
    end
  end
  object GroupBox2: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1081
    Height = 116
    Align = alTop
    Caption = ' Filtros de Consulta '
    Color = clWhite
    ParentColor = False
    TabOrder = 1
    object Label9: TLabel
      Tag = 1
      Left = 16
      Top = 32
      Width = 100
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro Destinat'#225'rio'
    end
    object Label10: TLabel
      Tag = 1
      Left = 24
      Top = 56
      Width = 92
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo de Emiss'#227'o'
    end
    object Label3: TLabel
      Tag = 1
      Left = 200
      Top = 56
      Width = 16
      Height = 13
      Alignment = taCenter
      Caption = 'at'#233
    end
    object Label2: TLabel
      Tag = 1
      Left = 200
      Top = 80
      Width = 16
      Height = 13
      Alignment = taCenter
      Caption = 'at'#233
    end
    object Label4: TLabel
      Tag = 1
      Left = 21
      Top = 80
      Width = 95
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Nota Fiscal de'
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 186
      Top = 24
      Width = 551
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = dsTerceiro
      TabOrder = 1
    end
    object er2LkpTerceiro: TER2LookupMaskEdit
      Left = 120
      Top = 24
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      CodigoLookup = 17
      QueryLookup = qryTerceiro
    end
    object dDtInicial: TMaskEdit
      Left = 120
      Top = 50
      Width = 65
      Height = 21
      EditMask = '##/##/####;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object dDtFinal: TMaskEdit
      Left = 232
      Top = 50
      Width = 65
      Height = 21
      EditMask = '##/##/####;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
    object DoctoFiscalInicial: TMaskEdit
      Left = 120
      Top = 76
      Width = 65
      Height = 21
      EditMask = '#######;1; '
      MaxLength = 7
      TabOrder = 4
      Text = '       '
    end
    object DoctoFiscalFinal: TMaskEdit
      Left = 232
      Top = 76
      Width = 65
      Height = 21
      EditMask = '#######;1; '
      MaxLength = 7
      TabOrder = 5
      Text = '       '
    end
  end
  object cxGrid2: TcxGrid [3]
    Left = 0
    Top = 145
    Width = 1081
    Height = 256
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    LookAndFeel.NativeStyle = True
    object cxGridDBTableView1: TcxGridDBTableView
      PopupMenu = Popup
      OnDblClick = cxGridDBTableView1DblClick
      DataController.DataSource = dsDoctoFiscal
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTotal'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#'
          Kind = skCount
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnGrouping = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.FonteSomenteLeitura
      object cxGridDBTableView1nCdDoctoFiscal: TcxGridDBColumn
        Caption = 'C'#243'd.'
        DataBinding.FieldName = 'nCdDoctoFiscal'
        Width = 65
      end
      object cxGridDBTableView1dDtEmissao: TcxGridDBColumn
        Caption = 'Data Emiss'#227'o'
        DataBinding.FieldName = 'dDtEmissao'
        Width = 136
      end
      object cxGridDBTableView1iNrDocto: TcxGridDBColumn
        Caption = 'Nr. Docto'
        DataBinding.FieldName = 'iNrDocto'
        Width = 87
      end
      object cxGridDBTableView1nCdSerieFiscal: TcxGridDBColumn
        Caption = 'S'#233'rie'
        DataBinding.FieldName = 'nCdSerieFiscal'
        Width = 63
      end
      object cxGridDBTableView1cCFOP: TcxGridDBColumn
        Caption = 'CFOP'
        DataBinding.FieldName = 'cCFOP'
        Width = 60
      end
      object cxGridDBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro'
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 417
      end
      object cxGridDBTableView1nValTotal: TcxGridDBColumn
        Caption = 'Valor Total'
        DataBinding.FieldName = 'nValTotal'
        Width = 114
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 536
    Top = 104
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000999999009999990099999900999999009999990000000000000000000000
      0000808080008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000009999
      9900000000000000000000000000000000009999990000000000000000008080
      8000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000E6E6E600E6E6E600E6E6E600E6E6E6000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000000000000E6E6E600E6E6
      E600E6E6E6000000000000000000E6E6E600E6E6E600E6E6E600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000E6E6E6000000
      000000000000D9D9D900D9D9D9000000000000000000E6E6E600000000009999
      9900999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000099999900D9D9D900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E60000000000E6E6E600E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000099999900E6E6E600E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E60000000000E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000000000009999990099999900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600D9D9D900E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000009999990099999900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000999999009999
      9900E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600000000009999
      9900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000999999009999
      99009999990099999900E6E6E600E6E6E600E6E6E600E6E6E600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999009999990099999900999999000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFF30000
      FE00C003FFE9000000008001F051000000008001E023000000008001C0270000
      00008001860F0000000080019987000000008001008700000000800100470000
      0000800100070000000080010007000000018001800F000000038001801F0000
      0077C003C03F0000007FFFFFF0FF000000000000000000000000000000000000
      000000000000}
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK')
    Left = 496
    Top = 69
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object Popup: TPopupMenu
    Images = ImageList1
    Left = 536
    Top = 69
    object ExibirDocumentoFiscal1: TMenuItem
      Caption = 'Exibir Documento Fiscal'
      ImageIndex = 2
      OnClick = ExibirDocumentoFiscal1Click
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 496
    Top = 104
  end
  object qryDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'iNrInicial'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'iNrFinal'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro           int'
      '       ,@dDtInicial            varchar(10)'
      '       ,@dDtFinal              varchar(10)'
      '       ,@nCdDoctoFiscalInicial int'
      '       ,@nCdDoctoFiscalFinal   int'
      ''
      'SET @nCdTerceiro           = :nCdTerceiro'
      'SET @dDtInicial            = :dDtInicial'
      'SET @dDtFinal              = :dDtFinal'
      'SET @nCdDoctoFiscalInicial = :iNrInicial'
      'SET @nCdDoctoFiscalFinal   = :iNrFinal'
      ''
      ''
      'SELECT nCdDoctoFiscal'
      '      ,nCdSerieFiscal'
      '      ,cCFOP'
      '      ,iNrDocto'
      '      ,dDtEmissao'
      '      ,dDtImpressao'
      '      ,nCdTerceiro'
      '      ,cNmTerceiro'
      '      ,nValTotal '
      '  FROM DoctoFiscal'
      
        ' WHERE ((@nCdTerceiro    = 0)                                  O' +
        'R (nCdTerceiro            = @nCdTerceiro))'
      
        '   AND ((dDtEmissao     >= Convert(DATETIME,@dDtInicial,103))  O' +
        'R (@dDtInicial            = '#39'01/01/1900'#39'))'
      
        '   AND ((dDtEmissao      < Convert(DATETIME,@dDtFinal,103)+1)  O' +
        'R (@dDtFinal              = '#39'01/01/1900'#39'))'
      
        '   AND ((iNrDocto       >= @nCdDoctoFiscalInicial)             O' +
        'R (@nCdDoctoFiscalInicial = 0))'
      
        '   AND ((iNrDocto       <= @nCdDoctoFiscalFinal)               O' +
        'R (@nCdDoctoFiscalFinal   = 0))'
      '   AND dDtImpressao      IS NOT NULL'
      '   AND dDtCancel         IS NULL'
      '   AND nCdEmpresa        = :nCdEmpresa'
      '   AND cFlgComplementar  = 0'
      '')
    Left = 456
    Top = 72
    object qryDoctoFiscalnCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryDoctoFiscalnCdSerieFiscal: TIntegerField
      FieldName = 'nCdSerieFiscal'
    end
    object qryDoctoFiscalcCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryDoctoFiscaliNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryDoctoFiscaldDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryDoctoFiscalnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryDoctoFiscalcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryDoctoFiscalnValTotal: TBCDField
      FieldName = 'nValTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscaldDtImpressao: TDateTimeField
      FieldName = 'dDtImpressao'
    end
  end
  object dsDoctoFiscal: TDataSource
    DataSet = qryDoctoFiscal
    Left = 456
    Top = 104
  end
end
