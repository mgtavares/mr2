unit fTituloProv;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ImgList, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons;

type
  TfrmTituloProv = class(TfrmCadastro_Padrao)
    qryMasternCdTitulo: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdEspTit: TIntegerField;
    qryMastercNrTit: TStringField;
    qryMasteriParcela: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdCategFinanc: TIntegerField;
    qryMasternCdMoeda: TIntegerField;
    qryMastercSenso: TStringField;
    qryMasterdDtEmissao: TDateTimeField;
    qryMasterdDtReceb: TDateTimeField;
    qryMasterdDtVenc: TDateTimeField;
    qryMasterdDtLiq: TDateTimeField;
    qryMasterdDtCad: TDateTimeField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternValTit: TBCDField;
    qryMasternValLiq: TBCDField;
    qryMasternSaldoTit: TBCDField;
    qryMasternValJuro: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMastercObsTit: TMemoField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitnCdGrupoEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitcFlgPrev: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceironCdStatus: TIntegerField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    qryMastercCNPJCPF: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    Label24: TLabel;
    DBMemo1: TDBMemo;
    qryTerceironCdMoeda: TIntegerField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryMasternCdUnidadeNegocio: TIntegerField;
    Label27: TLabel;
    DBEdit34: TDBEdit;
    qryMasternCdCC: TIntegerField;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    qryCCnCdCC1: TIntegerField;
    qryCCnCdCC2: TIntegerField;
    qryCCcCdCC: TStringField;
    qryCCcCdHie: TStringField;
    qryCCiNivel: TSmallintField;
    qryCCcFlgLanc: TIntegerField;
    qryCCnCdStatus: TIntegerField;
    Label28: TLabel;
    DBEdit36: TDBEdit;
    qryAux: TADOQuery;
    DBEdit4: TDBEdit;
    dsCC: TDataSource;
    qryMastercFlgDA: TIntegerField;
    qryMasternCdContaBancariaDA: TIntegerField;
    qryMasternCdTipoLanctoDA: TIntegerField;
    qryMasternCdLojaTit: TIntegerField;
    Label4: TLabel;
    DBEdit5: TDBEdit;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit10: TDBEdit;
    dsLoja: TDataSource;
    DBCheckBox1: TDBCheckBox;
    Label5: TLabel;
    DBEdit11: TDBEdit;
    Label10: TLabel;
    DBEdit13: TDBEdit;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancariacAgencia: TIntegerField;
    qryContaBancarianCdCOnta: TStringField;
    qryContaBancariacNmTitular: TStringField;
    DBEdit14: TDBEdit;
    dsContaBancaria: TDataSource;
    DBEdit15: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    qryTipoLancto: TADOQuery;
    qryTipoLanctonCdTipoLancto: TIntegerField;
    qryTipoLanctocNmTipoLancto: TStringField;
    DBEdit19: TDBEdit;
    dsTipoLancto: TDataSource;
    DBEdit20: TDBEdit;
    dsMoeda: TDataSource;
    DBEdit22: TDBEdit;
    dsEmpresa: TDataSource;
    DBEdit23: TDBEdit;
    dsUnidadeNegocio: TDataSource;
    DBEdit24: TDBEdit;
    dsTerceiro: TDataSource;
    DBEdit25: TDBEdit;
    dsEspTit: TDataSource;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    Label11: TLabel;
    DBEdit26: TDBEdit;
    qryCategFinanc: TADOQuery;
    dsCategFinanc: TDataSource;
    qryCategFinancnCdCategFinanc: TIntegerField;
    qryCategFinanccNmCategFinanc: TStringField;
    qryCategFinancnCdGrupoCategFinanc: TIntegerField;
    qryCategFinanccFlgRecDes: TStringField;
    qryCategFinancnCdPlanoConta: TIntegerField;
    qryCategFinancnCdServidorOrigem: TIntegerField;
    qryCategFinancdDtReplicacao: TDateTimeField;
    DBEdit21: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryTerceiroAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBEdit34KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit36KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit11Enter(Sender: TObject);
    procedure DBEdit36Enter(Sender: TObject);
    procedure DBEdit36Exit(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit13Exit(Sender: TObject);
    procedure DBEdit11Exit(Sender: TObject);
    procedure DBCheckBox1Click(Sender: TObject);
    procedure DBEdit34Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryEspTitBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTituloProv: TfrmTituloProv;

implementation

uses fMenu, fLookup_Padrao, fConvTitSP, fTituloProv_Duplicar;

{$R *.dfm}

procedure TfrmTituloProv.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TITULO' ;
  nCdTabelaSistema  := 21 ;
  nCdConsultaPadrao := 115 ;
  bLimpaAposSalvar  := False ;
end;

procedure TfrmTituloProv.btIncluirClick(Sender: TObject);
begin

  inherited;

  DBEdit34.Enabled := True ;
  DBEdit2.Enabled  := True ;
  DBEdit7.Enabled  := True ;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;

  qryMasternCdMoeda.Value := 1 ;
  PosicionaQuery(qryMoeda, qryMasternCdMoeda.AsString) ;

  DBEdit34.SetFocus ;


end;

procedure TfrmTituloProv.FormShow(Sender: TObject);
begin
  inherited;
  qryEspTit.Close ;
  qryEspTit.Open ;

  if (frmMenu.LeParametro('VAREJO') <> 'S') then
      DBEdit5.Enabled := False ;

end;

procedure TfrmTituloProv.qryTerceiroAfterScroll(DataSet: TDataSet);
begin
  inherited;
//  if (qryMaster.State = dsInsert) then
//      qryMasternCdMoeda.Value := qryTerceironCdMoeda.Value ;
      
end;

procedure TfrmTituloProv.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
    MensagemAlerta('Esta t�tulo n�o pertence a esta empresa.') ;
    btCancelar.Click;
    exit ;
  end ;

  qryCC.Close ;
  qryCC.Parameters.ParamByName('nCdCategFinanc').Value := qryMasternCdCategFinanc.Value ;
  PosicionaQuery(qryCC, qryMasternCdCC.asString) ;

  qryLoja.Close ;
  qryContaBancaria.Close ;
  qryTipoLancto.Close ;
  qryCategFInanc.Close ;
  qryTerceiro.Close ;
  qryEspTit.Close ;
  qryUnidadeNegocio.Close ;
  qryEmpresa.Close ;
  qryMoeda.Close ;

  qryUnidadeNegocio.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;

  PosicionaQuery(qryLoja, qryMasternCdLojaTit.AsString) ;
  PosicionaQuery(qryContaBancaria, qryMasternCdContaBancariaDA.AsString) ;
  PosicionaQuery(qryTipoLancto, qryMasternCdTipoLanctoDA.AsString) ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.AsString) ;
  PosicionaQuery(qryEspTit, qryMasternCdEspTit.AsString) ;
  PosicionaQuery(qryUnidadeNegocio, qryMasternCdUnidadeNegocio.AsString) ;
  PosicionaQuery(qryCategFinanc, qryMasternCdCategFinanc.AsString) ;
  PosicionaQuery(qryMoeda, qryMasternCdMoeda.AsString) ;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;

end;

procedure TfrmTituloProv.DBEdit34KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(35,'EXISTS(SELECT 1 FROM EmpresaUnidadeNegocio EUN WHERE EUN.nCdUnidadeNegocio = UnidadeNegocio.nCdUnidadeNegocio AND EUN.nCdEmpresa = ' + DBEdit2.Text + ')');

            If (nPK > 0) then
            begin
                qryMasternCdUnidadeNegocio.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTituloProv.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(114);

            If (nPK > 0) then
            begin
                qryMasternCdEspTit.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTituloProv.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTituloProv.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (qryMasternCdEspTit.Value = 0) then
            begin
                ShowMessage('Informe a esp�cie de t�tulo.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta(109);

            If (nPK > 0) then
            begin
                qryMasternCdCategFinanc.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTituloProv.DBEdit36KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (qryMasternCdCategFinanc.asString = '') then
            begin
                ShowMessage('Informe a Categoria Financeira.') ;
                DBEdit7.SetFocus ;
                Exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(33,'EXISTS(SELECT 1 FROM CentroCustoCategFinanc CCCF WHERE CCCF.nCdCC = CentroCusto.nCdCC AND CCCF.nCdCategFinanc = ' + qryMasternCdCategFinanc.asString + ')');

            If (nPK > 0) then
            begin
                qryMasternCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTituloProv.DBEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(16);

            If (nPK > 0) then
            begin
                qryMasternCdMoeda.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTituloProv.DBEdit11Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMasterdDtReceb.Value := qryMasterdDtEmissao.Value ;

end;

procedure TfrmTituloProv.DBEdit36Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInactive) then
      Exit;

  qryCC.Parameters.ParamByName('nCdCategFinanc').Value := qryMasternCdCategFinanc.Value ;
end;

procedure TfrmTituloProv.DBEdit36Exit(Sender: TObject);
begin
  inherited;
  qryCC.Close ;
  PosicionaQuery(qryCC, DBEdit36.Text) ;

end;

procedure TfrmTituloProv.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryCC.Close ;
  qryLoja.Close ;
  qryContaBancaria.Close ;
  qryTipoLancto.Close ;
  qryCategFInanc.Close ;
  qryTerceiro.Close ;
  qryEspTit.Close ;
  qryUnidadeNegocio.Close ;
  qryEmpresa.Close ;
  qryMoeda.Close ;

end;

procedure TfrmTituloProv.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, DBEdit5.Text) ;
  
end;

procedure TfrmTituloProv.DBEdit13Exit(Sender: TObject);
begin
  inherited;

  qryTipoLancto.Close ;
  PosicionaQuery(qryTipoLancto, DBEdit13.Text) ;
  
end;

procedure TfrmTituloProv.DBEdit11Exit(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close ;
  PosicionaQuery(qryContaBancaria, DBEdit11.Text) ;
  
end;

procedure TfrmTituloProv.DBCheckBox1Click(Sender: TObject);
begin
  inherited;

  DBEdit11.Enabled := DBCheckBox1.Checked ;
  DBEdit13.Enabled := DBCheckBox1.Checked ;

end;

procedure TfrmTituloProv.DBEdit34Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInactive) then
      Exit;

  qryUnidadeNegocio.Close ;
  qryUnidadeNegocio.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;
  PosicionaQuery(qryUnidadeNegocio, DBEdit34.Text) ;
end;

procedure TfrmTituloProv.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryEspTit.Close ;
  PosicionaQuery(qryEspTit, DBEdit3.Text) ;
  
end;

procedure TfrmTituloProv.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DBEdit6.Text) ;
  
end;

procedure TfrmTituloProv.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInactive) then
      Exit;

  qryCategFinanc.Close ;
  PosicionaQuery(qryCategFinanc, DBEdit7.Text) ;
  
end;

procedure TfrmTituloProv.DBEdit8Exit(Sender: TObject);
begin
  inherited;

  qryMoeda.Close ;
  PosicionaQuery(qryMoeda, DBEdit8.Text) ;
  
end;

procedure TfrmTituloProv.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(86,'Loja.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva)) ;

            If (nPK > 0) then
            begin
                qryMasternCdLojaTit.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTituloProv.DBEdit11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'cFlgCaixa = 0 AND cFlgCofre = 0 AND ContaBancaria.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva)) ;

            If (nPK > 0) then
            begin
                qryMasternCdContaBancariaDA.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTituloProv.DBEdit13KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(130,'cFlgPermDA = 1') ;

            If (nPK > 0) then
            begin
                qryMasternCdTipoLanctoDA.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTituloProv.cxButton1Click(Sender: TObject);
var
  objForm : TfrmConvTitSP ;
begin
  if not qryMaster.Active or (Trim(dbEdit1.Text) = '') then
  begin
      MensagemAlerta('Nenhuma provis�o selecionada.') ;
      exit ;
  end ;

  objForm := TfrmConvTitSP.Create(Self) ;
  objForm.nCdTitulo := qryMasternCdTitulo.Value ;
  showForm( objForm , TRUE ) ;

  btCancelar.Click;

end;

procedure TfrmTituloProv.cxButton2Click(Sender: TObject);
begin
  if not qryMaster.Active or (Trim(dbEdit1.Text) = '') then
  begin
      MensagemAlerta('Nenhuma provis�o selecionada.') ;
      exit ;
  end ;

  case MessageDlg('Confirma o cancelamento desta provis�o ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      qryAux.Close;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM Titulo WHERE nCdTitulo = ' + qryMasternCdTitulo.AsString) ;
      qryAux.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Provis�o cancelada com sucesso.') ;
  btCancelar.Click;

end;

procedure TfrmTituloProv.cxButton3Click(Sender: TObject);
var
  objForm : TfrmTituloProv_Duplicar ;
begin
  if not qryMaster.Active or (Trim(dbEdit1.Text) = '') then
  begin
      MensagemAlerta('Nenhuma provis�o selecionada.') ;
      exit ;
  end ;

  objForm := TfrmTituloProv_Duplicar.Create(Self) ;

  objForm.nCdTitulo := qryMasternCdTitulo.Value ;
  objForm.nValTit   := qryMasternValTit.Value   ;
  showForm( objForm , TRUE ) ;

end;

procedure TfrmTituloProv.qryMasterBeforePost(DataSet: TDataSet);
var
    qryAux : TADOQuery ;
begin

  if (qryMaster.State <> dsInsert) then
  begin
      if (qryMasterdDtCancel.AsString <> '') then
      begin
          MensagemAlerta('T�tulo cancelado n�o pode ser alterado.') ;
          exit ;
      end ;

      if (qryMasternValLiq.Value > 0) or (qryMasternValDesconto.Value > 0) or (qryMasternValJuro.Value > 0) then
      begin
          MensagemAlerta('T�tulo tem movimentos e n�o pode ser alterado.') ;
          exit ;
      end ;

  end ;

  if (qryMasternCdEmpresa.Value = 0) or (dbEdit22.Text = '') then
  begin
      MensagemAlerta('Informe a Empresa') ;
      DBEdit2.SetFocus;
      abort ;
  end ;

  if (qryMasternCdUnidadeNegocio.Value = 0) or (dbEdit23.Text = '') then
  begin
      MensagemAlerta('Informe a unidade de neg�cio') ;
      DBEdit34.SetFocus;
      abort ;
  end ;

  if (DBEdit5.Enabled) and (DBEdit10.Text = '') then
  begin
      MensagemAlerta('Informe a loja.') ;
      DbEdit5.SetFocus;
      abort ;
  end ;

  if (qryMasternCdEspTit.Value = 0) or (dbEdit25.Text = '') then
  begin
      MensagemAlerta('Informe a esp�cie de t�tulo') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (qryMasternCdTerceiro.Value = 0) or (dbEdit24.Text = '') then
  begin
      MensagemAlerta('Informe o terceiro') ;
      DBEdit6.SetFocus;
      abort ;
  end ;

  if (qryMasternCdCategFinanc.Value = 0) or (dbEdit21.Text = '') then
  begin
      MensagemAlerta('Informe a categoria financeira') ;
      DBEdit7.SetFocus;
      abort ;
  end ;

  if (qryMasternCdCC.Value = 0) or (dbEdit4.Text = '') then
  begin
      MensagemAlerta('Informe o centro de custo') ;
      DBEdit7.SetFocus;
      abort ;
  end ;

  if (qryMasternCdMoeda.Value = 0) or (dbEdit20.Text = '') then
  begin
      MensagemAlerta('Informe a moeda') ;
      DBEdit8.SetFocus;
      abort ;
  end ;

  if (qryMasterdDtVenc.AsString = '') then
  begin
      MensagemAlerta('Informe a data de vencimento') ;
      DBEdit12.SetFocus;
      abort ;
  end ;

  if (qryMasternValTit.Value <= 0) then
  begin
      MensagemAlerta('Valor do t�tulo inv�lido ou n�o informado') ;
      DBEdit16.SetFocus;
      abort ;
  end ;

  if (qryMastercFlgDA.Value = 1) then
  begin

      if (qryContaBancaria.Eof) or (qryContaBancarianCdConta.Value = '') then
      begin
          MensagemAlerta('Selecione a conta banc�ria de d�bito.') ;
          DBEdit11.SetFocus;
          abort ;
      end ;

      if (qryTipoLancto.Eof) or (qryTipoLanctocNmTipoLancto.Value = '') then
      begin
          MensagemAlerta('Selecione o tipo de lan�amento de d�bito para o extrato banc�rio.') ;
          DBEdit13.SetFocus;
          abort ;
      end ;

  end ;


  if not (qryEspTit.Active) then
      qryEspTit.open ;

  if    ((qryCategFinanccFlgRecDes.Value = 'D') and (qryEspTitcTipoMov.Value = 'R'))
     or ((qryCategFinanccFlgRecDes.Value = 'R') and (qryEspTitcTipoMov.Value = 'P')) then
  begin
      MensagemAlerta('Esta esp�cie de t�tulo n�o pode ser utilizada com esta categoria financeira') ;
      DbEdit3.SetFocus ;
      Abort ;
  end ;

  qryAux := TADOQuery.Create(Self);

  qryAux.Connection := frmMenu.Connection ;

  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM CentroCustoCategFinanc CCCF WHERE CCCF.nCdCC = :nCdCC AND CCCF.nCdCategFinanc = :nCdCategFinanc') ;
  qryAux.Parameters.ParamByName('nCdCC').Value := qryMasternCdCC.Value ;
  qryAux.Parameters.ParamByName('nCdCategFinanc').Value := qryMasternCdCategFinanc.Value ;
  qryAux.Open ;

  if (qryAux.eof) then
  begin
      qryAux := nil ;
      MensagemAlerta('N�o existe v�nculo entre esta categoria financeira e este centro de custo.') ;
      DbEdit36.SetFocus ;
      Abort ;
  end ;

  qryAux := nil ;

  inherited;

  if (qryMaster.State = dsInsert) then
  begin
    if (qryCategFinanccFlgRecDes.Value = 'R') then
      qryMastercSenso.Value := 'C' ;

    if (qryCategFinanccFlgRecDes.Value = 'D') then
      qryMastercSenso.Value := 'D' ;

    qryMasterdDtCad.Value := Now() ;
    qryMasternSaldoTit.Value := qryMasternValTit.Value ;

    If (qryMasteriParcela.Value <= 0) then
        qryMasteriParcela.Value := 1 ;

    if (qryMasternCdMoeda.Value = 0) then
        qryMasternCdMoeda.Value := 1 ;
  end ;

  if (qryMastercFlgDA.Value = 0) then
  begin
      qryMasternCdContaBancariaDA.asString := '' ;
      qryMasternCdTipoLanctoDA.asString    := '' ;

      qryContaBancaria.Close;
      qryTipoLancto.Close;
  end ;
end;

procedure TfrmTituloProv.qryEspTitBeforeOpen(DataSet: TDataSet);
begin
  inherited;

  qryEspTit.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

initialization
    RegisterClass(TfrmTituloProv) ;


end.
