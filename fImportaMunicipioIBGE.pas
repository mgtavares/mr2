unit fImportaMunicipioIBGE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, StdCtrls, cxButtons, DB, ADODB, StrUtils, Buttons,
  Mask, ER2Lookup, DBCtrls;

type
  TfrmImportaMunicipioIBGE = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    Label2: TLabel;
    edtCaminho: TEdit;
    OpenDialog1: TOpenDialog;
    qryMunicipio: TADOQuery;
    qryMunicipionCdMunicipio: TIntegerField;
    qryMunicipionCdEstado: TIntegerField;
    qryMunicipiocNmMunicipio: TStringField;
    qryMunicipionCdMunicipioIBGE: TIntegerField;
    qryChecaMunicipioCodigo: TADOQuery;
    qryChecaMunicipioCodigoCOLUMN1: TIntegerField;
    qryChecaMunicipioNome: TADOQuery;
    IntegerField1: TIntegerField;
    cxButton1: TcxButton;
    qryAtualizaMunicipio: TADOQuery;
    er2LkpEstado: TER2LookupMaskEdit;
    qryEstado: TADOQuery;
    qryEstadonCdEstado: TIntegerField;
    qryEstadonCdPais: TIntegerField;
    qryEstadocUF: TStringField;
    qryEstadocNmEstado: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    procedure cxButton1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImportaMunicipioIBGE: TfrmImportaMunicipioIBGE;

implementation

uses
  fMenu;

{$R *.dfm}

procedure TfrmImportaMunicipioIBGE.cxButton1Click(Sender: TObject);
begin
  inherited;

  if (not DirectoryExists('.\Arquivos\Municipios')) then
      CreateDir('.\Arquivos\Municipios');

  OpenDialog1.Title      := 'Selecione o Arquivo';
  OpenDialog1.DefaultExt := '*.txt';
  OpenDialog1.Filter     := 'Arquivos de Munic�pios (*.txt)';
  OpenDialog1.InitialDir := '.\Arquivos\Municipios';

  if OpenDialog1.Execute then
      edtCaminho.Text := OpenDialog1.FileName;

end;

procedure TfrmImportaMunicipioIBGE.ToolButton1Click(Sender: TObject);
var
  F : TextFile;
  cCdMunicipio , cNmMunicipio , cLinha : widestring   ;
begin
  inherited;

  if (qryEstado.IsEmpty) then
  begin
      MensagemAlerta('Informe o c�digo do estado.');
      er2LkpEstado.SetFocus;
      Abort;
  end;

  if (Trim(edtCaminho.Text) = '') then
  begin
      MensagemAlerta('Informe o caminho do arquivo.') ;
      edtCaminho.SetFocus;
      exit ;
  end ;

  if not FileExists(Trim(edtCaminho.Text)) then
  begin
      MensagemErro('Arquivo inexistente.') ;
      edtCaminho.SetFocus;
      exit ;
  end ;

  if (MessageDlg('Confirma a importa��o dos munic�pios ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
      exit ;

  {-- inicia a importa��o do arquivo --}
  AssignFile(F, edtCaminho.Text);
  Reset(F) ;

  qryMunicipio.Close;
  qryMunicipio.Open;
  
  while not Eof(F) do
  begin
    ReadLn(F, cLinha);
    cCdMunicipio := Copy(cLinha,1,7) ;
    cNmMunicipio := Copy(cLinha,9,50) ;

    qryChecaMunicipioCodigo.Close;
    qryChecaMunicipioCodigo.Parameters.ParamByName('nCdEstado').Value    := strToint(Trim(er2LkpEstado.Text));
    qryChecaMunicipioCodigo.Parameters.ParamByName('nCdMunicipio').Value := strToint(Trim(cCdMunicipio));
    qryChecaMunicipioCodigo.Open;

    if qryChecaMunicipioCodigo.Eof then
    begin

        qryMunicipio.Insert;
        qryMunicipionCdEstado.Value        := strToint(Trim(er2LkpEstado.Text));
        qryMunicipionCdMunicipio.Value     := strToInt(Trim(cCdMunicipio)) ;
        qryMunicipionCdMunicipioIBGE.Value := strToint(Trim(cCdMunicipio)) ;
        qryMunicipiocNmMunicipio.Value     := AnsiUpperCase(cNmMunicipio) ;
        qryMunicipio.Post;

    end
    else
    begin
        qryAtualizaMunicipio.Close;
        qryAtualizaMunicipio.Parameters.ParamByName('nCdMunicipio').Value := StrToInt(Trim(cCdMunicipio));
        qryAtualizaMunicipio.Parameters.ParamByName('cNmMunicipio').Value := AnsiUpperCase(cNmMunicipio);
        qryAtualizaMunicipio.ExecSQL;
    end;
  end;

  CloseFile(F);

  ShowMessage('Importa��o Conclu�da.') ;

  er2LkpEstado.Text := '' ;
  edtCaminho.Text  := '' ;
  er2LkpEstado.SetFocus;

end;

initialization
    RegisterClass(TfrmImportaMunicipioIBGE) ;

end.
