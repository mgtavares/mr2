inherited rptRecibo: TrptRecibo
  Left = 12
  Top = 163
  Caption = 'Recibo'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 68
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label6: TLabel [2]
    Left = 198
    Top = 147
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label3: TLabel [3]
    Left = 7
    Top = 147
    Width = 102
    Height = 13
    Alignment = taRightJustify
    Caption = 'Intervalo Vencimento'
  end
  object Label2: TLabel [4]
    Left = 76
    Top = 73
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
  end
  object Label4: TLabel [5]
    Left = 62
    Top = 98
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Esp.T'#237'tulo'
  end
  object Label5: TLabel [6]
    Left = 36
    Top = 123
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Parcela'
  end
  inherited ToolBar1: TToolBar
    TabOrder = 5
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit3: TMaskEdit [8]
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    Enabled = False
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object DBEdit2: TDBEdit [9]
    Tag = 1
    Left = 181
    Top = 40
    Width = 66
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    Enabled = False
    TabOrder = 7
  end
  object DBEdit3: TDBEdit [10]
    Tag = 1
    Left = 250
    Top = 40
    Width = 651
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 8
  end
  object MaskEdit1: TMaskEdit [11]
    Left = 112
    Top = 139
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [12]
    Left = 224
    Top = 139
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object MaskEdit4: TMaskEdit [13]
    Left = 112
    Top = 65
    Width = 65
    Height = 21
    TabOrder = 0
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object DBEdit1: TDBEdit [14]
    Tag = 1
    Left = 181
    Top = 65
    Width = 113
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = DataSource2
    TabOrder = 9
  end
  object DBEdit4: TDBEdit [15]
    Tag = 1
    Left = 297
    Top = 65
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource2
    TabOrder = 10
  end
  object MaskEdit6: TMaskEdit [16]
    Left = 112
    Top = 90
    Width = 65
    Height = 21
    TabOrder = 1
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object DBEdit5: TDBEdit [17]
    Tag = 1
    Left = 181
    Top = 90
    Width = 654
    Height = 21
    DataField = 'cNmEspTit'
    DataSource = DataSource3
    TabOrder = 11
  end
  object MaskEdit7: TMaskEdit [18]
    Left = 112
    Top = 115
    Width = 65
    Height = 21
    TabOrder = 2
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object DBEdit6: TDBEdit [19]
    Tag = 1
    Left = 181
    Top = 115
    Width = 654
    Height = 21
    DataField = 'cNmTabTipoParcContratoEmpImobiliario'
    DataSource = DataSource4
    TabOrder = 12
  end
  inherited ImageList1: TImageList
    Left = 944
    Top = 192
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 576
    Top = 144
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 600
    Top = 176
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nCdTerceiro')
    Left = 736
    Top = 144
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryTerceiro
    Left = 768
    Top = 168
  end
  object qryEspTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEspTit'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '   FROM ESPTIT'
      'WHERE nCdEspTit = :nCdEspTit')
    Left = 640
    Top = 144
    object qryEspTitnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitnCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryEspTitcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryEspTitcTipoMov: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object qryEspTitcFlgPrev: TIntegerField
      FieldName = 'cFlgPrev'
    end
  end
  object DataSource3: TDataSource
    DataSet = qryEspTit
    Left = 672
    Top = 168
  end
  object qryTipoParcela: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'select * from TabTipoParcContratoEmpImobiliario  where nCdTabTip' +
        'oParcContratoEmpImobiliario =:nPK'
      'order by nCdTabTipoParcContratoEmpImobiliario')
    Left = 504
    Top = 144
    object qryTipoParcelanCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryTipoParcelacNmTabTipoParcContratoEmpImobiliario: TStringField
      FieldName = 'cNmTabTipoParcContratoEmpImobiliario'
      Size = 50
    end
  end
  object DataSource4: TDataSource
    DataSet = qryTipoParcela
    Left = 528
    Top = 176
  end
  object usp_CorrecaoMonetaria: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CORRECAO_MONETARIA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEspTit'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTipoParcela'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@dDtFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@cSenso'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 648
    Top = 248
  end
end
