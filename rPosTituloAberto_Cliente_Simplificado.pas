unit rPosTituloAberto_Cliente_Simplificado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosTituloAberto_cliente_simplificado = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText10: TQRDBText;
    QRExpr3: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText1: TQRDBText;
    QRLabel22: TQRLabel;
    QRImage1: TQRImage;
    QRLabel14: TQRLabel;
    QRLabel42: TQRLabel;
    QRShape5: TQRShape;
    QRShape3: TQRShape;
    QRShape1: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText16: TQRDBText;
    QRExpr8: TQRExpr;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    QRShape4: TQRShape;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRLabel9: TQRLabel;
    QRDBText19: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel15: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape2: TQRShape;
    QRShape6: TQRShape;
    usp_RelatorionCdTitulo: TIntegerField;
    usp_RelatorionCdEspTit: TIntegerField;
    usp_RelatoriocNmEspTit: TStringField;
    usp_RelatorionCdSP: TIntegerField;
    usp_RelatoriocNrTit: TStringField;
    usp_RelatorionCdTerceiro: TIntegerField;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatorionCdMoeda: TIntegerField;
    usp_RelatoriocSigla: TStringField;
    usp_RelatoriocSenso: TStringField;
    usp_RelatoriodDtVenc: TDateTimeField;
    usp_RelatorionValTit: TFloatField;
    usp_RelatorionSaldoTit: TFloatField;
    usp_RelatoriodDtCancel: TDateTimeField;
    usp_RelatorionValJuro: TFloatField;
    usp_RelatorioiDias: TIntegerField;
    usp_RelatorionCdBancoPortador: TIntegerField;
    usp_RelatorionValVencido: TFloatField;
    usp_RelatorionValVencer: TFloatField;
    usp_RelatoriocFlgPrev: TIntegerField;
    usp_RelatoriodDtEmissao: TDateTimeField;
    usp_RelatorioiParcela: TIntegerField;
    usp_RelatorionCdLojaTit: TIntegerField;
    usp_RelatorionCdEmpresa: TIntegerField;
    usp_RelatoriocNrNf: TStringField;
    QRDBText4: TQRDBText;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosTituloAberto_cliente_simplificado: TrptPosTituloAberto_cliente_simplificado;

implementation

{$R *.dfm}

end.
