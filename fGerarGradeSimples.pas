unit fGerarGradeSimples;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, Mask, ImgList, ComCtrls, ToolWin,
  ExtCtrls, DB, ADODB;

type
  TfrmGerarGradeSimples = class(TfrmProcesso_Padrao)
    MaskEdit4: TMaskEdit;
    Label4: TLabel;
    MaskEdit3: TMaskEdit;
    Label3: TLabel;
    SP_GERA_GRADE_SIMPLES: TADOStoredProc;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdProduto : integer ;
  end;

var
  frmGerarGradeSimples: TfrmGerarGradeSimples;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmGerarGradeSimples.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(MaskEdit3.Text) = '') then
      MaskEdit3.Text := '0' ;

  if (Trim(MaskEdit4.Text) = '') then
      MaskEdit4.Text := '0' ;

  try
      if (StrToFloat(MaskEdit3.Text) < 0) then
      begin
          MensagemAlerta('Valor de custo inv�lido.') ;
          exit ;
      end ;
  except
          MensagemAlerta('Valor de custo inv�lido.') ;
          exit ;
  end ;

  try
      if (StrToFloat(MaskEdit4.Text) < 0) then
      begin
          MensagemAlerta('Valor de venda inv�lido.') ;
          exit ;
      end ;
  except
          MensagemAlerta('Valor de venda inv�lido.') ;
          exit ;
  end ;

  case MessageDlg('Confirma a gera��o da grade ?',mtConfirmation,[mbYes,mbNo],0) of
    mrNo:exit ;
  end ;

  frmMenu.Connection.Begintrans;

  try
      SP_GERA_GRADE_SIMPLES.Close ;
      SP_GERA_GRADE_SIMPLES.Parameters.ParamByName('@nCdProduto').Value := nCdProduto ;
      SP_GERA_GRADE_SIMPLES.Parameters.ParamByName('@nCdusuario').Value := frmMenu.nCdUsuarioLogado ;
      SP_GERA_GRADE_SIMPLES.Parameters.ParamByName('@nValCusto').Value  := StrToFloat(Trim(MaskEdit3.Text)) ;
      SP_GERA_GRADE_SIMPLES.Parameters.ParamByName('@nValVenda').Value  := StrToFloat(Trim(MaskEdit4.Text)) ;
      SP_GERA_GRADE_SIMPLES.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  close ;

end;

end.
