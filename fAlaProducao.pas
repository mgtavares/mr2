unit fAlaProducao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmAlaProducao = class(TfrmCadastro_Padrao)
    qryMasternCdAlaProducao: TIntegerField;
    qryMastercNmAlaProducao: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAlaProducao: TfrmAlaProducao;

implementation

{$R *.dfm}

procedure TfrmAlaProducao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'ALAPRODUCAO' ;
  nCdTabelaSistema  := 62 ;
  nCdConsultaPadrao := 140 ;

end;

procedure TfrmAlaProducao.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

initialization
    RegisterClass(TfrmAlaProducao) ;
    
end.
