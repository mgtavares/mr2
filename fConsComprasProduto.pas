unit fConsComprasProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBCtrls, StdCtrls, GridsEh, DBGridEh, ADODB, Mask, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGrid;

type
  TfrmConsComprasProduto = class(TfrmProcesso_Padrao)
    qryResultado: TADOQuery;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DataSource1: TDataSource;
    dsResultado: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    MaskEdit1: TMaskEdit;
    DBEdit1: TDBEdit;
    qryResultadonCdPedido: TIntegerField;
    qryResultadodDtPedido: TDateTimeField;
    qryResultadodDtAutor: TDateTimeField;
    qryResultadodDtPrevEntIni: TDateTimeField;
    qryResultadodDtPrevEntFim: TDateTimeField;
    qryResultadocNmTerceiro: TStringField;
    qryResultadonQtdePed: TBCDField;
    qryResultadonQtdeExpRec: TBCDField;
    qryResultadonSaldo: TBCDField;
    qryResultadocNrPedTerceiro: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtAutor: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPrevEntIni: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPrevEntFim: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdePed: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeExpRec: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldo: TcxGridDBColumn;
    cxGrid1DBTableView1cNrPedTerceiro: TcxGridDBColumn;
    Label3: TLabel;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    procedure MaskEdit1Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsComprasProduto: TfrmConsComprasProduto;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmConsComprasProduto.MaskEdit1Exit(Sender: TObject);
begin
  inherited;

  qryProduto.Close ;
  PosicionaQuery(qryProduto, MAskEdit1.Text) ;
  
  qryResultado.Close ;
  
end;

procedure TfrmConsComprasProduto.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione o produto.') ;
      MaskEdit1.SetFocus ;
      exit ;
  end ;

  if (trim(MaskEdit2.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data inicial.') ;
      MaskEdit2.Setfocus ;
      exit ;
  end ;

  if (trim(MaskEdit3.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data final.') ;
      MaskEdit2.Setfocus ;
      exit ;
  end ;

  qryResultado.Close ;
  qryResultado.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryResultado.Parameters.ParamByName('nCdProduto').Value := frmMenu.ConvInteiro(Trim(Maskedit1.Text)) ;
  qryResultado.Parameters.ParamByName('dDtInicial').Value := frmMenu.ConvData(Trim(Maskedit2.Text)) ;
  qryResultado.Parameters.ParamByName('dDtFinal').Value   := frmMenu.ConvData(Trim(Maskedit3.Text)) ;
  qryResultado.Open ;

  if (qryResultado.eof) then
  begin
      MensagemAlerta('Nenhum registro encontrado para o crit�rio utilizado.') ;
  end ;

end;

procedure TfrmConsComprasProduto.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(76);

        If (nPK > 0) then
        begin
            MaskEdit1.Text := IntToStr(nPK) ;
            PosicionaQuery(qryProduto, MaskEdit1.Text);
        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmConsComprasProduto) ;
    
end.
