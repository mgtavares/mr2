unit rOrcamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, QuickRpt, QRCtrls, jpeg;

type
  TrptOrcamento = class(TForm)
    QuickRep1: TQuickRep;
    qryPedido: TADOQuery;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape1: TQRShape;
    QRGroup1: TQRGroup;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel11: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel13: TQRLabel;
    QRLabel7: TQRLabel;
    QRBand2: TQRBand;
    QRLabel16: TQRLabel;
    QRSubDetail1: TQRSubDetail;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRLabel27: TQRLabel;
    QRDBText21: TQRDBText;
    QRImage1: TQRImage;
    QRLabel36: TQRLabel;
    SummaryBand1: TQRBand;
    QRLabel12: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel17: TQRLabel;
    QRShape2: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRShape5: TQRShape;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidodDtPrevEntIni: TDateTimeField;
    qryPedidodDtPrevEntFim: TDateTimeField;
    qryPedidonCdEmpresa: TIntegerField;
    qryPedidocNmEmpresa: TStringField;
    qryPedidonCdLoja: TIntegerField;
    qryPedidocNmLoja: TStringField;
    qryPedidonCdTipoPedido: TIntegerField;
    qryPedidocNmTipoPedido: TStringField;
    qryPedidonCdTerceiro: TIntegerField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidocNrPedTerceiro: TStringField;
    qryPedidonCdCondPagto: TIntegerField;
    qryPedidocNmCondPagto: TStringField;
    qryPedidonCdTabStatusPed: TIntegerField;
    qryPedidocNmTabStatusPed: TStringField;
    qryPedidonPercDesconto: TBCDField;
    qryPedidonPercAcrescimo: TBCDField;
    qryPedidonValPedido: TBCDField;
    qryPedidocNmContato: TStringField;
    qryPedidocNmTerceiroRepres: TStringField;
    qryPedidonCdProduto: TIntegerField;
    qryPedidocReferencia: TStringField;
    qryPedidocNmItem: TStringField;
    qryPedidonAltura: TBCDField;
    qryPedidonLargura: TBCDField;
    qryPedidonDimensao: TBCDField;
    qryPedidonQtdePed: TBCDField;
    qryPedidonValUnitario: TBCDField;
    qryPedidonValTotalItem: TBCDField;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText8: TQRDBText;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel28: TQRLabel;
    qryPedidocOBS: TMemoField;
    QRDBText13: TQRDBText;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptOrcamento: TrptOrcamento;

implementation

{$R *.dfm}

end.
