unit fAtribuiMarca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, DB, ADODB, GridsEh, DBGridEh, StdCtrls, Mask, DBCtrls,
  Menus;

type
  TfrmAtribuiMarca = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qrySubcategoria: TADOQuery;
    qryMarca: TADOQuery;
    dsMarca: TDataSource;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qrySubcategorianCdSubCategoria: TIntegerField;
    qrySubcategoriaSubcategoriaNome: TStringField;
    cmdPreparaTemp: TADOCommand;
    qryPreparaTemp: TADOQuery;
    qryPopulaTemp: TADOQuery;
    qryTempAtribuiMarca: TADOQuery;
    dsTempAtribuiMarca: TDataSource;
    qryTempAtribuiMarcaDepartamentoCd: TIntegerField;
    qryTempAtribuiMarcaCategoriaCd: TIntegerField;
    qryTempAtribuiMarcaSubcategoriaCd: TIntegerField;
    qryTempAtribuiMarcanCdMarcaSubCategoria: TIntegerField;
    qryTempAtribuiMarcacFlgAcao: TStringField;
    qryTempAtribuiMarcacNmDepartamento: TStringField;
    qryTempAtribuiMarcacNmCategoria: TStringField;
    qryTempAtribuiMarcacNmSubCategoria: TStringField;
    qryDepartamento: TADOQuery;
    qryCategoria: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryCategorianCdCategoria: TIntegerField;
    qryCategoriacNmCategoria: TStringField;
    PopupMenu1: TPopupMenu;
    btnExcluirRegistro: TMenuItem;
    qryTempAtribuiMarcanCdMarca: TIntegerField;
    SP_ATRIBUI_MARCA: TADOStoredProc;
    qryVerificaRegistro: TADOQuery;
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTempAtribuiMarcaCalcFields(DataSet: TDataSet);
    procedure qryTempAtribuiMarcaBeforePost(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure btnExcluirRegistroClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure ToolButton2Click(Sender: TObject);
    procedure DBGridEh1EditButtonClick(Sender: TObject);
    procedure qryTempAtribuiMarcaBeforeDelete(DataSet: TDataSet);
    procedure qryTempAtribuiMarcaAfterScroll(DataSet: TDataSet);
    procedure DBGridEh1Columns2UpdateData(Sender: TObject;
      var Text: String; var Value: Variant; var UseText, Handled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGridEh1Columns4UpdateData(Sender: TObject;
      var Text: String; var Value: Variant; var UseText, Handled: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAtribuiMarca: TfrmAtribuiMarca;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmAtribuiMarca.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : integer;
begin
    {Ao pressionar a tecla F4, � poss�vel consultar departamentos, categorias e subcategorias}

    if (Key = VK_F4) then

        if (qryTempAtribuiMarca.State = dsInsert) or (qryTempAtribuiMarca.IsEmpty) then

            case (DBGridEh1.Col) of

            1: begin
                   qryTempAtribuiMarca.Edit;

                   nPK := frmLookup_Padrao.ExecutaConsulta(129);

                   if (nPK > 0) then
                   begin
                       if (nPK <> qryTempAtribuiMarcaDepartamentoCd.Value) then
                       begin
                           qryTempAtribuiMarcaCategoriaCd.Clear;
                           qryTempAtribuiMarcaSubcategoriaCd.Clear;
                       end;

                       qryTempAtribuiMarcaDepartamentoCd.Value := nPK;
                   end;
               end;

            3: begin
                   qryTempAtribuiMarca.Edit;

                   if (qryTempAtribuiMarcaDepartamentoCd.Value > 0) then
                   begin
                       nPK := frmLookup_Padrao.ExecutaConsulta2(46, 'nCdDepartamento = ' + qryTempAtribuiMarcaDepartamentoCd.AsString);

                       if (nPK > 0) then
                       begin
                           if (nPK <> qryTempAtribuiMarcaCategoriaCd.Value) then
                               qryTempAtribuiMarcaSubcategoriaCd.Clear;

                           qryTempAtribuiMarcaCategoriaCd.Value := nPK;
                       end;
                   end

                   else
                   begin
                       MensagemAlerta('Informe um departamento!');
                       Abort;
                   end;
               end;

            5: begin
                   qryTempAtribuiMarca.Edit;


                   if (qryTempAtribuiMarcaCategoriaCd.Value > 0) then
                   begin
                       nPK := frmLookup_Padrao.ExecutaConsulta2(47, 'nCdCategoria = ' + qryTempAtribuiMarcaCategoriaCd.AsString);

                       if (nPK > 0) then
                           qryTempAtribuiMarcaSubcategoriaCd.Value := nPK;
                   end

                   else
                       MensagemAlerta('Informe uma categoria!');
               end;
            end;
end;

procedure TfrmAtribuiMarca.qryTempAtribuiMarcaCalcFields(
  DataSet: TDataSet);
begin
    {Traz o nome de cada departamento, categoria e subcategoria da tabela temporaria}

    qryDepartamento.Close;
    qryDepartamento.Parameters.ParamByName('nPK').Value := qryTempAtribuiMarcaDepartamentoCd.Value;
    qryDepartamento.Open;

    qryTempAtribuiMarcacNmDepartamento.Value := qryDepartamentocNmDepartamento.Value;

    qryCategoria.Close;
    qryCategoria.Parameters.ParamByName('nPK').Value             := qryTempAtribuiMarcaCategoriaCd.Value;
    qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryTempAtribuiMarcaDepartamentoCd.Value;
    qryCategoria.Open;

    qryTempAtribuiMarcacNmCategoria.Value := qryCategoriacNmCategoria.Value;

    qrySubcategoria.Close;
    qrySubcategoria.Parameters.ParamByName('nPK').Value          := qryTempAtribuiMarcaSubcategoriaCd.Value;
    qrySubcategoria.Parameters.ParamByName('nCdCategoria').Value := qryTempAtribuiMarcaCategoriaCd.Value;
    qrySubcategoria.Open;

    qryTempAtribuiMarcacNmSubCategoria.Value := qrySubcategoriaSubcategoriaNome.Value;
end;

procedure TfrmAtribuiMarca.qryTempAtribuiMarcaBeforePost(
  DataSet: TDataSet);
begin
    {Antes do post, valida todas as informa��es do grid}

    if (qryTempAtribuiMarcaDepartamentoCd.Value < 1) then
    begin
        MensagemAlerta('Informe um departamento!');

        Abort;
    end;

    if (qryTempAtribuiMarcaDepartamentoCd.IsNull = False) and (Trim(qryTempAtribuiMarcacNmDepartamento.Value) = '') then
    begin
        MensagemAlerta('Departamento inv�lido!');

        DBGridEh1.Col := 1;

        Abort;
    end;

    if (qryTempAtribuiMarcaCategoriaCd.IsNull = False) and (Trim(qryTempAtribuiMarcacNmCategoria.Value) = '') then
    begin
        MensagemAlerta('Categoria inv�lida!');

        DBGridEh1.Col := 3;

        Abort;
    end;

    if (qryTempAtribuiMarcaSubcategoriaCd.IsNull = False) and (Trim(qryTempAtribuiMarcacNmSubCategoria.Value) = '') then
    begin
        MensagemAlerta('Subcategoria inv�lida!');

        DBGridEh1.Col := 5;

        Abort;
    end;

    {Atribui o valor 'I' para o campo cFlgAcao, indicando que o registro deve ser inserido na tabela MarcaSubCategoria}

    if (qryTempAtribuiMarca.State = dsInsert) then
    begin
        qryTempAtribuiMarcacFlgAcao.Value := 'I';
        qryTempAtribuiMarcanCdMarca.Value := qryMarcanCdMarca.Value;
    end;
end;

procedure TfrmAtribuiMarca.ToolButton1Click(Sender: TObject);
begin
    {Processa todas as informa��es da tabela tempor�ria e atualiza a tabela MarcaSubCategoria}

    if (qryTempAtribuiMarca.State = dsInsert) then
        if (Trim(qryTempAtribuiMarcaDepartamentoCd.AsString) = '') then
        begin
            MensagemAlerta('N�o h� nenhum registro na tabela!');
            Abort;
        end

        else
            qryTempAtribuiMarca.Post;

    qryTempAtribuiMarca.First;

    frmMenu.Connection.BeginTrans;

    try
        SP_ATRIBUI_MARCA.ExecProc;
    except
        frmMenu.Connection.RollbackTrans;

        qryTempAtribuiMarca.Close;
        qryTempAtribuiMarca.Open;

        MensagemErro('Erro no processamento!');
        raise;
    end;

    qryTempAtribuiMarca.Close;
    qryPreparaTemp.ExecSQL;
    qryPopulaTemp.ExecSQL;
    qryTempAtribuiMarca.Open;

    frmMenu.Connection.CommitTrans;

    ShowMessage('Atribui��o realizada com sucesso!');
end;

procedure TfrmAtribuiMarca.btnExcluirRegistroClick(Sender: TObject);
begin
    {Atualiza o campo cFlgAcao do registro para 'D', indicando que o registro deve ser deletado da tabela MarcaSubCategoria}

    qryTempAtribuiMarca.Edit;

    qryTempAtribuiMarcacFlgAcao.Value := 'D';

    qryTempAtribuiMarca.Post;
    qryTempAtribuiMarca.Close;
    qryTempAtribuiMarca.Open;

    if (qryTempAtribuiMarca.IsEmpty) then
        qryTempAtribuiMarca.Insert;
end;

procedure TfrmAtribuiMarca.PopupMenu1Popup(Sender: TObject);
begin
    {Se a query estiver em modo 'Insert', habilita o bot�o de exclus�o de registro}

    if (qryTempAtribuiMarca.State = dsInsert) then
        btnExcluirRegistro.Visible := False
    else
        btnExcluirRegistro.Visible := True;
end;

procedure TfrmAtribuiMarca.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmAtribuiMarca.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmAtribuiMarca.FormKeyPress(Sender: TObject; var Key: Char);
begin
{  inherited;}

end;

procedure TfrmAtribuiMarca.ToolButton2Click(Sender: TObject);
begin
    Close;
end;

procedure TfrmAtribuiMarca.DBGridEh1EditButtonClick(Sender: TObject);
begin
    if (qryTempAtribuiMarca.IsEmpty) then
        qryTempAtribuiMarca.Insert;
end;

procedure TfrmAtribuiMarca.qryTempAtribuiMarcaBeforeDelete(
  DataSet: TDataSet);
begin
    {Exclui o registro da tabela tempor�ria}

    if not (qryTempAtribuiMarcanCdMarcaSubCategoria.IsNull) then
    begin
        qryTempAtribuiMarca.Edit;

        qryTempAtribuiMarcacFlgAcao.Value := 'D';

        qryTempAtribuiMarca.Post;

        qryTempAtribuiMarca.Close;
        qryTempAtribuiMarca.Open;

        Abort;
    end;
end;

procedure TfrmAtribuiMarca.qryTempAtribuiMarcaAfterScroll(
  DataSet: TDataSet);
begin
    if (qryTempAtribuiMarca.IsEmpty) then
        qryTempAtribuiMarca.Insert;
end;

procedure TfrmAtribuiMarca.DBGridEh1Columns2UpdateData(Sender: TObject;
  var Text: String; var Value: Variant; var UseText, Handled: Boolean);
begin
    {Valida o departamento antes de atualizar a coluna departamento}

    if (qryTempAtribuiMarcaDepartamentoCd.Value < 1) then
    begin
        MensagemAlerta('Informe um departamento!');

        qryTempAtribuiMarcaCategoriaCd.Clear;

        DBGridEh1.Col := 1;

        Abort;
    end;
end;

procedure TfrmAtribuiMarca.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    {Verifica se foram realizadas altera��es na tabela}

    qryVerificaRegistro.Close;
    qryVerificaRegistro.Open;

    if not (qryVerificaRegistro.IsEmpty) then
        if (MessageDLG('Foram realizadas altera��es na tabela. Deseja realmente sair?', mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
            Abort;

    qryVerificaRegistro.Close;
end;

procedure TfrmAtribuiMarca.DBGridEh1Columns4UpdateData(Sender: TObject;
  var Text: String; var Value: Variant; var UseText, Handled: Boolean);
begin
    {Valida a categoria e o departamento ao atualizar a coluna categoria}

    if (qryTempAtribuiMarcaDepartamentoCd.Value < 1) then
    begin
        MensagemAlerta('Informe um departamento!');

        qryTempAtribuiMarcaCategoriaCd.Clear;

        DBGridEh1.Col := 1;

        Abort;
    end;

    if (qryTempAtribuiMarcaCategoriaCd.Value < 1) then
    begin
        MensagemAlerta('Informe uma categoria!');

        DBGridEh1.Col := 3;

        Abort;
    end;
end;

end.
