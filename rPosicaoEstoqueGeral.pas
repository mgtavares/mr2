unit rPosicaoEstoqueGeral;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, ER2Lookup, ER2Excel;

type
  TrptPosicaoEstoqueGeral = class(TfrmRelatorio_Padrao)
    qryGrupoProdutos: TADOQuery;
    qryGrupoInventario: TADOQuery;
    qryDepartamento: TADOQuery;
    qryLocalEstoque: TADOQuery;
    qryCategoria: TADOQuery;
    qrySubCategoria: TADOQuery;
    qrySegmento: TADOQuery;
    qryEmpresa: TADOQuery;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentocNmSegmento: TStringField;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategoriacNmSubCategoria: TStringField;
    qryCategorianCdCategoria: TAutoIncField;
    qryCategoriacNmCategoria: TStringField;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryGrupoInventarionCdGrupoInventario: TIntegerField;
    qryGrupoInventariocNmGrupoInventario: TStringField;
    qryGrupoProdutosnCdGrupoProduto: TIntegerField;
    qryGrupoProdutoscNmGrupoProduto: TStringField;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryProduto: TADOQuery;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    dsEmpresa: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    dsLocalEstoque: TDataSource;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    dsGrupoProduto: TDataSource;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    dsGrupoInventario: TDataSource;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    dsDepartamento: TDataSource;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    dsCategoria: TDataSource;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    dsSubCategoria: TDataSource;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    dsSegmento: TDataSource;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    dsProduto: TDataSource;
    edtEmpresa: TER2LookupMaskEdit;
    edtLocalEstoque: TER2LookupMaskEdit;
    edtGrupoProduto: TER2LookupMaskEdit;
    edtGrupoInventario: TER2LookupMaskEdit;
    edtDepartamento: TER2LookupMaskEdit;
    edtCategoria: TER2LookupMaskEdit;
    edtSubCategoria: TER2LookupMaskEdit;
    edtSegmento: TER2LookupMaskEdit;
    edtProduto: TER2LookupMaskEdit;
    RadioGroup3: TRadioGroup;
    RadioGroup4: TRadioGroup;
    RadioGroup5: TRadioGroup;
    ER2Excel1: TER2Excel;
    RadioGroup6: TRadioGroup;
    procedure edtCategoriaBeforeLookup(Sender: TObject);
    procedure edtSubCategoriaBeforeLookup(Sender: TObject);
    procedure edtSegmentoBeforeLookup(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure exportaRelatorio;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoEstoqueGeral: TrptPosicaoEstoqueGeral;

implementation

uses
  rPosicaoEstoqueGeral_view, fMenu;

var
  objRel : TrptPosicaoEstoqueGeral_view;

{$R *.dfm}

procedure TrptPosicaoEstoqueGeral.edtCategoriaBeforeLookup(Sender: TObject);
begin
  inherited;
  if (Trim(edtDepartamento.Text) <> '') then
  begin
      edtCategoria.WhereAdicional.Text := 'nCdDepartamento = ' + edtDepartamento.Text + ' ORDER BY cNmCategoria';
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := edtDepartamento.Text;
  end
  else
  begin
      MensagemAlerta('Selecione o Departamento.');
      abort;
  end;

end;

procedure TrptPosicaoEstoqueGeral.edtSubCategoriaBeforeLookup(Sender: TObject);
begin
  inherited;
  if (Trim(edtCategoria.Text) <> '') then
  begin
      edtSubCategoria.WhereAdicional.Text := 'nCdCategoria = ' + edtCategoria.Text + ' ORDER BY cNmSubCategoria';
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := edtCategoria.Text;
  end
  else
  begin
      MensagemAlerta('Selecione a Categoria.');
      abort;
  end;
end;

procedure TrptPosicaoEstoqueGeral.edtSegmentoBeforeLookup(Sender: TObject);
begin
  inherited;
  if (Trim(edtSubCategoria.Text) <> '') then
  begin
      edtSegmento.WhereAdicional.Text := 'nCdSubCategoria = ' + edtSubCategoria.Text + ' ORDER BY cNmSegmento';
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := edtSubCategoria.Text;
  end
  else
  begin
      MensagemAlerta('Selecione a SubCategoria.');
      abort;
  end;
end;

procedure TrptPosicaoEstoqueGeral.ToolButton1Click(Sender: TObject);
var
  cFiltro : String;
begin
  inherited;

  objRel := TrptPosicaoEstoqueGeral_view.Create(nil);

  try
      try
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Close;
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@nCdEmpresa').Value                 := frmMenu.ConvInteiro(edtEmpresa.Text);
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@nCdLocalEstoque').Value            := frmMenu.ConvInteiro(edtLocalEstoque.Text);
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@nCdGrupoProduto').Value            := frmMenu.ConvInteiro(edtGrupoProduto.Text);
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@nCdGrupoInventario').Value         := frmMenu.ConvInteiro(edtGrupoInventario.Text);
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@nCdDepartamento').Value            := frmMenu.ConvInteiro(edtDepartamento.Text);
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@nCdCategoria').Value               := frmMenu.ConvInteiro(edtCategoria.Text);
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@nCdSubCategoria').Value            := frmMenu.ConvInteiro(edtSubCategoria.Text);
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@nCdSegmento').Value                := frmMenu.ConvInteiro(edtSegmento.Text);
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@nCdProduto').Value                 := frmMenu.ConvInteiro(edtProduto.Text);
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@cFlgExibeValores').Value           := RadioGroup1.ItemIndex;
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@cFlgMetodoValorizacao').Value      := RadioGroup2.ItemIndex;
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@cFlgSomenteEstoqueNegativo').Value := RadioGroup3.ItemIndex;
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@cFlgSomenteProdutoAtivo').Value    := RadioGroup4.ItemIndex;
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Parameters.ParamByName('@nCdTabTipoProduto').Value          := RadioGroup6.ItemIndex + 1;
          objRel.SPREL_POSICAO_ESTOQUE_GERAL.Open;

          if (RadioGroup5.ItemIndex = 0) then
          begin

              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              cFiltro := '';

              if (Trim(edtEmpresa.Text) <> '') then
                  cFiltro := cFiltro + 'Empresa: ' + DBEdit1.Text;

              if (Trim(edtLocalEstoque.Text) <> '') then
                  cFiltro := cFiltro + '/ ' + 'Local Estoque: ' + DBEdit2.Text;

              if (Trim(edtGrupoProduto.Text) <> '') then
                  cFiltro := cFiltro + '/ ' + 'Grupo Produto: ' + DBEdit3.Text;

              if (Trim(edtGrupoInventario.Text) <> '') then
                  cFiltro := cFiltro + '/ ' + 'Grupo Inventario: ' + DBEdit4.Text;

              if (Trim(edtDepartamento.Text) <> '') then
                  cFiltro := cFiltro + '/ ' + 'Departamento: ' + DBEdit5.Text;

              if (Trim(edtCategoria.Text) <> '') then
                  cFiltro := cFiltro + '/ ' + 'Categoria: ' + DBEdit6.Text;

              if (Trim(edtSubCategoria.Text) <> '') then
                  cFiltro := cFiltro + '/ ' + 'SubCategoria: ' + DBEdit7.Text;

              if (Trim(edtSegmento.Text) <> '') then
                  cFiltro := cFiltro + '/ ' + 'Segmento: ' + DBEdit8.Text;

              if (Trim(edtProduto.Text) <> '') then
                  cFiltro := cFiltro + '/ ' + 'Produto: ' + DBEdit9.Text;

              if (RadioGroup1.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ Exibir Valores: N�o '
              else cFiltro := cFiltro + '/ Exibir Valores: Sim';

              if (RadioGroup2.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ M�todo Valoriza��o: Ultimo Pre�o Custo'
              else cFiltro := cFiltro + '/ M�todo Valoriza��o: Pre�o M�dio';

              if (RadioGroup3.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ Somente Estoque Negativo: N�o'
              else cFiltro := cFiltro + '/ Somente Estoque Negativo: Sim';

              if (RadioGroup4.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ Somente Produto Ativo: N�o'
              else cFiltro := cFiltro + '/ Somente Produto Ativo: Sim';

              cFiltro := cFiltro + '/ Nivel Produto:' + IntToStr(RadioGroup6.ItemIndex + 1);
              
              objRel.lblFiltro1.Caption := cFiltro;
              objRel.QuickRep1.PreviewModal ;

          end
          else {-- relat�rio em planilha --}
          begin
              exportaRelatorio;
          end ;
          
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TrptPosicaoEstoqueGeral.exportaRelatorio;
var
  cNmFonte : string ;
  iLinha   : integer ;
begin
  inherited;

  frmMenu.mensagemUsuario('Exportando Planilha...');

  cNmFonte := 'Calibri' ;

  ER2Excel1.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
  ER2Excel1.Celula['A2'].Text := 'Relat�rio de Posi��o de Estoque - Geral' ;
  
  ER2Excel1.Celula['A1'].FontName   := cNmFonte;
  ER2Excel1.Celula['A1'].FontSize   := 10 ;

  ER2Excel1.Celula['A2'].FontName   := cNmFonte;
  ER2Excel1.Celula['A2'].FontSize   := 10 ;


  ER2Excel1.Celula['A5'].Text := 'Produto/Item' ;
  ER2Excel1.Celula['A6'].Text := 'C�digo' ;
  ER2Excel1.Celula['B6'].Text := 'Descri��o' ;
  ER2Excel1.Celula['C6'].Text := 'U.M' ;

  ER2Excel1.Celula['A5'].Mesclar('C5');
  ER2Excel1.Celula['A5'].HorizontalAlign := haCenter ;

  ER2Excel1.Celula['D6'].Text := '�lt. Compra';;
  ER2Excel1.Celula['E6'].Text := '�lt. Invent�rio' ;
  ER2Excel1.Celula['F6'].Text := 'Qtde. Estoque' ;
  ER2Excel1.Celula['G6'].Text := 'Qtde. Tr�nsito' ;
  ER2Excel1.Celula['H6'].Text := 'Custo Unit.';
  ER2Excel1.Celula['I6'].Text := 'Total';
  ER2Excel1.Celula['J6'].Text := 'Grupo Produto';

  ER2Excel1.Celula['A4'].Background := RGB(216,216,216);
  ER2Excel1.Celula['A4'].FontName   := cNmFonte;
  ER2Excel1.Celula['A4'].FontSize   := 9 ;

  ER2Excel1.Celula['A5'].Background := RGB(216,216,216);
  ER2Excel1.Celula['A5'].FontName   := cNmFonte;
  ER2Excel1.Celula['A5'].FontSize   := 9 ;

  ER2Excel1.Celula['A6'].Background := RGB(216,216,216);
  ER2Excel1.Celula['A6'].FontName   := cNmFonte;
  ER2Excel1.Celula['A6'].FontSize   := 9 ;
  ER2Excel1.Celula['A6'].Border     := [EdgeBottom];

  ER2Excel1.Celula['A4'].Range('J4');
  ER2Excel1.Celula['A5'].Range('J5');
  ER2Excel1.Celula['A6'].Range('J6');

  iLinha := 7 ; {-- Linha Inicial --}

  objRel.SPREL_POSICAO_ESTOQUE_GERAL.First;

  while not objRel.SPREL_POSICAO_ESTOQUE_GERAL.Eof do
  begin

      ER2Excel1.Celula['A' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_ESTOQUE_GERALnCdProduto.Value ;
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_ESTOQUE_GERALcNmProduto.Value;
      ER2Excel1.Celula['C' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_ESTOQUE_GERALcUnidadeMedida.Value;
      ER2Excel1.Celula['D' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_ESTOQUE_GERALdDtUltReceb.Value;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_ESTOQUE_GERALdDtUltInventario.Value;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_ESTOQUE_GERALnQtdeDisponivel.Value;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_ESTOQUE_GERALnQtdeTransito.Value;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_ESTOQUE_GERALnValCustoUnitario.Value;
      ER2Excel1.Celula['I' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_ESTOQUE_GERALnValCustoTotal.Value;
      ER2Excel1.Celula['J' + IntToStr(iLinha)].Text := objRel.SPREL_POSICAO_ESTOQUE_GERALcNmGrupoProduto.Value;

      ER2Excel1.Celula['D' + IntToStr(iLinha)].Mascara := 'dd/mm/aaaa' ;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].Mascara := 'dd/mm/aaaa' ;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].Mascara := '#.##0,000000' ;
      ER2Excel1.Celula['I' + IntToStr(iLinha)].Mascara := '#.##0,00' ;

      ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['I' + IntToStr(iLinha)].HorizontalAlign := haRight ;

      inc( iLinha ) ;

      objRel.SPREL_POSICAO_ESTOQUE_GERAL.Next;
  end ;

  objRel.SPREL_POSICAO_ESTOQUE_GERAL.First;

  { -- exporta planilha e limpa result do componente -- }
  ER2Excel1.ExportXLS;
  ER2Excel1.CleanupInstance;

  frmMenu.mensagemUsuario('');
end;

initialization
    RegisterClass(TrptPosicaoEstoqueGeral);

end.
