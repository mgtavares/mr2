inherited frmTituloProv_Duplicar: TfrmTituloProv_Duplicar
  Left = 421
  Top = 227
  Width = 332
  Height = 449
  BorderIcons = [biSystemMenu]
  Caption = 'Duplica'#231#227'o de Provis'#227'o'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 316
    Height = 384
  end
  inherited ToolBar1: TToolBar
    Width = 316
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 316
    Height = 384
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsVenctos
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object cdsVenctos: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    AfterInsert = cdsVenctosAfterInsert
    BeforePost = cdsVenctosBeforePost
    Left = 216
    Top = 96
    Data = {
      3A0000009619E0BD0100000018000000020000000000030000003A0007644474
      56656E630400060000000000076E56616C54697408000400000000000000}
    object cdsVenctosdDtVenc: TDateField
      DisplayLabel = 'Vencimentos Previstos|Data'
      DisplayWidth = 16
      FieldName = 'dDtVenc'
      EditMask = '!99/99/9999;1;_'
    end
    object cdsVenctosnValTit: TFloatField
      DisplayLabel = 'Vencimentos Previstos|Valor'
      DisplayWidth = 16
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
    end
  end
  object dsVenctos: TDataSource
    DataSet = cdsVenctos
    Left = 256
    Top = 96
  end
  object SP_DUPLICA_TITULO_PROV: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_DUPLICA_TITULO_PROV;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtVenc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@nValTit'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end>
    Left = 216
    Top = 168
  end
end
