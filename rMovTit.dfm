inherited rptMovTit: TrptMovTit
  Left = 12
  Top = 143
  Caption = 'Relat'#243'rio - Movimenta'#231#227'o de T'#237'tulos'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 89
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label2: TLabel [2]
    Left = 50
    Top = 96
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Esp'#233'cie de T'#237'tulo'
  end
  object Label5: TLabel [3]
    Left = 91
    Top = 120
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
  end
  object Label3: TLabel [4]
    Left = 7
    Top = 144
    Width = 123
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo de Movimenta'#231#227'o'
  end
  object Label6: TLabel [5]
    Left = 215
    Top = 144
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label4: TLabel [6]
    Left = 53
    Top = 216
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Cheque'
  end
  object Label7: TLabel [7]
    Left = 202
    Top = 216
    Width = 149
    Height = 13
    Alignment = taRightJustify
    Caption = '(Somente para T'#237'tulos a Pagar)'
  end
  object Label9: TLabel [8]
    Tag = 1
    Left = 110
    Top = 70
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label8: TLabel [9]
    Left = 83
    Top = 240
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Opera'#231#227'o'
  end
  object Label10: TLabel [10]
    Left = 24
    Top = 168
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Emiss'#227'o T'#237'tulo'
  end
  object Label11: TLabel [11]
    Left = 215
    Top = 168
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label12: TLabel [12]
    Left = 7
    Top = 192
    Width = 123
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Vencimento T'#237'tulo'
  end
  object Label13: TLabel [13]
    Left = 215
    Top = 192
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [15]
    Tag = 1
    Left = 205
    Top = 40
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 1
  end
  object DBEdit3: TDBEdit [16]
    Tag = 1
    Left = 277
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 2
  end
  object DBEdit7: TDBEdit [17]
    Tag = 1
    Left = 205
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmEspTit'
    DataSource = DataSource3
    TabOrder = 3
  end
  object DBEdit9: TDBEdit [18]
    Tag = 1
    Left = 205
    Top = 112
    Width = 129
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = DataSource4
    TabOrder = 4
  end
  object DBEdit10: TDBEdit [19]
    Tag = 1
    Left = 341
    Top = 112
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource4
    TabOrder = 5
  end
  object edtDtMovIni: TMaskEdit [20]
    Left = 133
    Top = 136
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 10
    Text = '  /  /    '
  end
  object edtDtMovFim: TMaskEdit [21]
    Left = 237
    Top = 136
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 11
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [22]
    Left = 133
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object MaskEdit5: TMaskEdit [23]
    Left = 133
    Top = 88
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 8
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object MaskEdit6: TMaskEdit [24]
    Left = 133
    Top = 112
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 9
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object RadioGroup1: TRadioGroup [25]
    Left = 133
    Top = 262
    Width = 185
    Height = 105
    Caption = 'Tipo de Movimenta'#231#227'o'
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'Liquida'#231#227'o'
      'Juro'
      'Desconto'
      'Abatimento')
    TabOrder = 18
  end
  object RadioGroup2: TRadioGroup [26]
    Left = 325
    Top = 262
    Width = 185
    Height = 105
    Caption = 'Tipo de T'#237'tulo'
    ItemIndex = 0
    Items.Strings = (
      'T'#237'tulos a Pagar'
      'T'#237'tulos a Receber')
    TabOrder = 19
  end
  object MaskEdit4: TMaskEdit [27]
    Left = 133
    Top = 208
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 16
    Text = '      '
  end
  object MaskEdit7: TMaskEdit [28]
    Left = 133
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 7
    Text = '      '
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object DBEdit1: TDBEdit [29]
    Tag = 1
    Left = 205
    Top = 64
    Width = 649
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 20
  end
  object DBEdit4: TDBEdit [30]
    Tag = 1
    Left = 205
    Top = 232
    Width = 654
    Height = 21
    DataField = 'cNmOperacao'
    DataSource = DataSource6
    TabOrder = 21
  end
  object MaskEdit8: TMaskEdit [31]
    Left = 133
    Top = 232
    Width = 65
    Height = 21
    TabOrder = 17
    OnExit = MaskEdit8Exit
    OnKeyDown = MaskEdit8KeyDown
  end
  object edtDtEmiIni: TMaskEdit [32]
    Left = 133
    Top = 160
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 12
    Text = '  /  /    '
  end
  object edtDtEmiFim: TMaskEdit [33]
    Left = 237
    Top = 160
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 13
    Text = '  /  /    '
  end
  object edtDtVencIni: TMaskEdit [34]
    Left = 133
    Top = 184
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 14
    Text = '  /  /    '
  end
  object edtDtVencFim: TMaskEdit [35]
    Left = 237
    Top = 184
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 15
    Text = '  /  /    '
  end
  inherited ImageList1: TImageList
    Left = 832
    Top = 80
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 485
    Top = 112
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUnidadeNegocio'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM UNIDADENEGOCIO'
      ' WHERE nCdUnidadeNegocio = :nCdUnidadeNegocio')
    Left = 533
    Top = 112
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object qryEspTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEspTit'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM ESPTIT'
      ' WHERE cFlgPrev = 0'
      '   AND nCdEspTit = :nCdEspTit'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioEspTit UE'
      '               WHERE UE.nCdEspTit      = EspTit.nCdEspTit'
      '                 AND UE.nCdUsuario     = :nCdUsuario'
      '                 AND UE.cFlgTipoAcesso = '#39'L'#39')'
      ''
      '')
    Left = 573
    Top = 112
    object qryEspTitnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitnCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryEspTitcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryEspTitcTipoMov: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object qryEspTitcFlgPrev: TIntegerField
      FieldName = 'cFlgPrev'
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nCdTerceiro')
    Left = 616
    Top = 104
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryMoeda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMoeda'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '   FROM Moeda'
      ' WHERE nCdMoeda = :nCdMoeda')
    Left = 664
    Top = 104
    object qryMoedanCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMoedacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 10
    end
    object qryMoedacNmMoeda: TStringField
      FieldName = 'cNmMoeda'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 304
  end
  object DataSource2: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 632
    Top = 312
  end
  object DataSource3: TDataSource
    DataSet = qryEspTit
    Left = 640
    Top = 320
  end
  object DataSource4: TDataSource
    DataSet = qryTerceiro
    Left = 648
    Top = 328
  end
  object DataSource5: TDataSource
    DataSet = qryMoeda
    Left = 656
    Top = 336
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK'
      
        '      AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja =' +
        ' Loja.nCdLoja AND UL.nCdUsuario = :nCdUsuario)')
    Left = 936
    Top = 80
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 976
    Top = 77
  end
  object qryOperacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '   FROM Operacao'
      'WHERE nCdOperacao = :nPK')
    Left = 776
    Top = 240
    object qryOperacaonCdOperacao: TIntegerField
      FieldName = 'nCdOperacao'
    end
    object qryOperacaocNmOperacao: TStringField
      FieldName = 'cNmOperacao'
      Size = 50
    end
    object qryOperacaocTipoOper: TStringField
      FieldName = 'cTipoOper'
      FixedChar = True
      Size = 1
    end
    object qryOperacaocSinalOper: TStringField
      FieldName = 'cSinalOper'
      FixedChar = True
      Size = 1
    end
    object qryOperacaonCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryOperacaonCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryOperacaodDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
  end
  object DataSource6: TDataSource
    DataSet = qryOperacao
    Left = 624
    Top = 272
  end
end
