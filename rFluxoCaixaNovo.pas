unit rFluxoCaixaNovo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, ER2Lookup, DBCtrls, DB, ADODB, RDprint, ER2Excel,
  cxControls, cxPC, DBGridEhGrouping, GridsEh, DBGridEh, ToolCtrlsEh;

type
  TrptFluxoCaixaNovo = class(TfrmRelatorio_Padrao)
    edtEmpresa: TER2LookupMaskEdit;
    edtLoja: TER2LookupMaskEdit;
    edtMoeda: TER2LookupMaskEdit;
    edtQtdPeriodos: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    GroupBox1: TGroupBox;
    CheckBox1: TCheckBox;
    CheckBox2: TCheckBox;
    CheckBox3: TCheckBox;
    CheckBox4: TCheckBox;
    CheckBox5: TCheckBox;
    CheckBox6: TCheckBox;
    CheckBox7: TCheckBox;
    CheckBox8: TCheckBox;
    RadioGroup2: TRadioGroup;
    RadioGroup3: TRadioGroup;
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    qryMoeda: TADOQuery;
    dsEmpresa: TDataSource;
    dsLoja: TDataSource;
    dsMoeda: TDataSource;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit1: TDBEdit;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit2: TDBEdit;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    RDprint1: TRDprint;
    SPREL_FLUXO_CAIXA: TADOStoredProc;
    cmdPreparaTemp: TADOCommand;
    qryPosCaixaAtual: TADOQuery;
    qryDemonstraMovFutura: TADOQuery;
    qryExpectativaCaixa: TADOQuery;
    qryMovAnalitico: TADOQuery;
    qryPosCaixaAtualcEmpresaLoja: TStringField;
    qryPosCaixaAtualcBanco: TStringField;
    qryPosCaixaAtualcAgencia: TStringField;
    qryPosCaixaAtualnCdConta: TStringField;
    qryPosCaixaAtualnSaldoConta: TBCDField;
    qryPosCaixaAtualdDtSaldo: TDateTimeField;
    qryPosCaixaAtualnValLimite: TBCDField;
    qryPosCaixaAtualnSaldoTotal: TBCDField;
    qryPosCaixaAtualnValLimiteTotal: TBCDField;
    qryDemonstraMovFuturadDtInicial: TDateTimeField;
    qryDemonstraMovFuturadDtFinal: TDateTimeField;
    qryDemonstraMovFuturanValTotalReceber: TBCDField;
    qryDemonstraMovFuturanValTotalPagar: TBCDField;
    qryDemonstraMovFuturanValSaldoAcumulado: TBCDField;
    qryExpectativaCaixanValTotalAtrasReceber: TBCDField;
    qryExpectativaCaixanValTotalAtrasPagar: TBCDField;
    qryExpectativaCaixanValDispTotal: TBCDField;
    qryExpectativaCaixanValDispImediata: TBCDField;
    qryExpectativaCaixanValTotalReceber: TBCDField;
    qryExpectativaCaixanValTotalPagar: TBCDField;
    qryExpectativaCaixanValSaldoSemLimCred: TBCDField;
    qryExpectativaCaixanValLimiteCred: TBCDField;
    qryExpectativaCaixanSaldoTotal: TBCDField;
    qryMovAnaliticodDtInicial: TDateTimeField;
    qryMovAnaliticodDtFinal: TDateTimeField;
    qryMovAnaliticoiPeriodo: TIntegerField;
    qryMovAnaliticodDtVenc: TDateTimeField;
    qryMovAnaliticonCdTitulo: TIntegerField;
    qryMovAnaliticocNrTit: TStringField;
    qryMovAnaliticocNmEspTit: TStringField;
    qryMovAnaliticocNmTerceiro: TStringField;
    qryMovAnaliticocNmCategFinanc: TStringField;
    qryMovAnaliticonSaldoTit: TBCDField;
    qryMovAnaliticonValTotalPagar: TBCDField;
    qryMovAnaliticonValTotalReceber: TBCDField;
    qryMovAnaliticocSenso: TStringField;
    qryDemonstraMovFuturanValTotalReceberPeriodo: TBCDField;
    qryDemonstraMovFuturanValTotalPagarPeriodo: TBCDField;
    ER2Excel1: TER2Excel;
    qryCotacao: TADOQuery;
    qryCotacaonCdMoeda: TIntegerField;
    qryCotacaodDtCotacao: TDateTimeField;
    qryCotacaonValCotacao: TBCDField;
    chkChequeReceber: TCheckBox;
    chkChequePagar: TCheckBox;
    chkPrevisaoPagar: TCheckBox;
    GroupBox2: TGroupBox;
    chkDepositar: TCheckBox;
    chkCustodiado: TCheckBox;
    qryExpectativaCaixanValChequeDepositar: TBCDField;
    qryExpectativaCaixanValChequeDepositado: TBCDField;
    qryExpectativaCaixanValChequeCustodia: TBCDField;
    chkDepositado: TCheckBox;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qrySaldoInicial: TADOQuery;
    qrySaldoInicialnCdContaBancaria: TIntegerField;
    qrySaldoInicialnCdEmpresa: TStringField;
    qrySaldoInicialnCdLoja: TStringField;
    qrySaldoInicialnCdBanco: TStringField;
    qrySaldoInicialcAgencia: TIntegerField;
    qrySaldoInicialnCdConta: TStringField;
    qrySaldoInicialcNmTitular: TStringField;
    qrySaldoInicialnSaldoAtual: TBCDField;
    qrySaldoInicialddDtSaldo: TDateTimeField;
    dsSaldoInicial: TDataSource;
    qryPopulaSaldoInicial: TADOQuery;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure RDprint1NewPage(Sender: TObject; Pagina: Integer);
    procedure ER2Excel1BeforeExport(Sender: TObject);
    procedure ER2Excel1AfterExport(Sender: TObject);
    procedure edtEmpresaExit(Sender: TObject);
    procedure atualizaSaldoInicial;
    procedure edtLojaExit(Sender: TObject);
  private
    { Private declarations }
    iPagina : integer;
    iLinha  : integer;
    cFiltro : String;
    function BoolToInt(Value : Boolean) : integer;
    procedure ExibeFluxoCaixa;
    procedure TelaFluxoCaixa;
    procedure GeraPlanilha;
    procedure PrintHeader(cNmHeader : String);
  public
    { Public declarations }
  end;

var
  rptFluxoCaixaNovo: TrptFluxoCaixaNovo;

implementation

uses fMenu, rFluxoCaixaNovo_Tela;

function TrptFluxoCaixaNovo.BoolToInt(Value: Boolean): integer;
begin
    if (Value) then
        Result := 1
    else Result := 0;
end;

procedure TrptFluxoCaixaNovo.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
  edtEmpresa.PosicionaQuery;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      edtLoja.Text := IntToStr(frmMenu.nCdLojaAtiva);
      edtLoja.PosicionaQuery;
  end else
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
      edtLoja.TabStop  := False ;
  end;

  edtMoeda.Text := '1';
  edtMoeda.PosicionaQuery;

  atualizaSaldoInicial;

end;

procedure TrptFluxoCaixaNovo.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(edtMoeda.Text) = '') then
  begin
      MensagemAlerta('Selecione a moeda de refer�ncia.');
      edtMoeda.SetFocus;
      abort;
  end;

  if (frmMenu.ConvInteiro(edtQtdPeriodos.Text) <= 0) then
  begin
      MensagemAlerta('Digite a quatidade de per�odos.');
      edtQtdPeriodos.SetFocus;
      abort;
  end;

  cmdPreparaTemp.Execute;

  SPREL_FLUXO_CAIXA.Close;
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@nCdEmpresa').Value                 := frmMenu.ConvInteiro(edtEmpresa.Text);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@nCdLoja').Value                    := frmMenu.ConvInteiro(edtLoja.Text);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@nCdMoeda').Value                   := frmMenu.ConvInteiro(edtMoeda.Text);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@nPeriodos').Value                  := frmMenu.ConvInteiro(edtQtdPeriodos.Text);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgAtraso').Value                 := BoolToInt(CheckBox1.Checked);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgReceber').Value                := BoolToInt(CheckBox2.Checked);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgPagar').Value                  := BoolToInt(CheckBox3.Checked);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgExibMovAnaliReceber').Value    := BoolToInt(CheckBox7.Checked);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgExibMovAnaliPagar').Value      := BoolToInt(CheckBox8.Checked);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgPeriodicidade').Value          := RadioGroup2.ItemIndex;
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgPedidoVendaAutorizado').Value  := BoolToInt(CheckBox4.Checked);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgPedidoCompraAutorizado').Value := BoolToInt(CheckBox5.Checked);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgChequeReceber').Value          := BoolToInt(chkChequeReceber.Checked);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgChequePagar').Value            := BoolToInt(chkChequePagar.Checked);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgPrevPagar').Value              := BoolToInt(chkPrevisaoPagar.Checked);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgChkDepositar').Value           := BoolToInt(chkDepositar.Checked);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgChkDepositado').Value          := BoolToInt(chkDepositado.Checked);
  SPREL_FLUXO_CAIXA.Parameters.ParamByName('@cFlgChkCustodiado').Value          := BoolToInt(chkCustodiado.Checked);
  SPREL_FLUXO_CAIXA.ExecProc;

  qryPosCaixaAtual.Close;
  qryPosCaixaAtual.Open;

  qryDemonstraMovFutura.Close;
  qryDemonstraMovFutura.Open;

  qryExpectativaCaixa.Close;
  qryExpectativaCaixa.Open;

  qryMovAnalitico.Close;
  qryMovAnalitico.Open;

  qryCotacao.Close;
  qryCotacao.Parameters.ParamByName('nCdMoeda').Value := qryMoedanCdMoeda.Value;
  qryCotacao.Open;

  if (RadioGroup3.ItemIndex = 0) then  {-- quando modo de exibi��o for relat�rio --}
      ExibeFluxoCaixa;

  if (RadioGroup3.ItemIndex = 1) then {-- quando modo de exibi��o for em tela --}
      TelaFluxoCaixa;

  if (RadioGroup3.ItemIndex = 2) then  {-- quando modo de exibi��o for Planilha --}
      GeraPlanilha;

end;

procedure TrptFluxoCaixaNovo.RDprint1NewPage(Sender: TObject;
  Pagina: Integer);
begin
  inherited;

  iPagina := iPagina+ 1 ;

  PrintHeader('CABE�ALHO');

end;

procedure TrptFluxoCaixaNovo.ExibeFluxoCaixa;
var
  iPeriodo         : integer;
  nPeriodo         : integer;
  nValTotalPagar   : Double;
  nValTotalReceber : Double;
begin
  iPagina          := 0;
  nValTotalPagar   := 0;
  nValTotalReceber := 0;


  cFiltro := 'Periodos: ' + Trim(edtQtdPeriodos.Text) + '/ EMP: ' + frmMenu.ZeroEsquerda(Trim(edtEmpresa.Text),3);

  if ((frmMenu.LeParametro('VAREJO') = 'S') and (qryLojanCdLoja.AsString <> '')) then
      cFiltro := cFiltro + '/ LOJA: ' + frmMenu.ZeroEsquerda(Trim(edtLoja.Text),3);

  if (CheckBox1.Checked) then
      cFiltro := cFiltro + '/ TTA: SIM'
  else cFiltro := cFiltro + '/ TTA: N�O';

  if (CheckBox2.Checked) then
      cFiltro := cFiltro + '/ TTR: SIM'
  else cFiltro := cFiltro + '/ TTR: N�O';

  if (CheckBox3.Checked) then
      cFiltro := cFiltro + '/ TTP: SIM'
  else cFiltro := cFiltro + '/ TTP: N�O';

  if (CheckBox4.Checked) then
      cFiltro := cFiltro + '/ PDV: SIM'
  else cFiltro := cFiltro + '/ PDV: N�O';

  if (CheckBox5.Checked) then
      cFiltro := cFiltro + '/ PDC: SIM'
  else cFiltro := cFiltro + '/ PDC: N�O';

  RDPrint1.Abrir;

  PrintHeader('CABE�ALHO');

  qryPosCaixaAtual.First;

  if (CheckBox6.Checked) then
  begin

      RDPrint1.ImpF(iLinha,1,'Empresa/Loja',[negrito,comp12]);
      RDPrint1.ImpF(iLinha,14,'Banco',[negrito,comp12]);
      RDPrint1.ImpF(iLinha,32,'Ag�ncia',[negrito,comp12]);
      RDPrint1.ImpF(iLinha,40,'Conta',[negrito,comp12]);
      RDPrint1.ImpF(iLinha,53,'Saldo',[negrito,comp12]);
      RDPrint1.ImpF(iLinha,60,'Dt. Saldo',[negrito,comp12]);
      RDPrint1.ImpF(iLinha,69,'Limite Cr�dito',[negrito,comp12]);

      while not qryPosCaixaAtual.Eof do
      begin
          RDPrint1.ImpF(iLinha + 1,1,qryPosCaixaAtualcEmpresaLoja.Value,[comp17]);
          RDPrint1.ImpF(iLinha + 1,14,qryPosCaixaAtualcBanco.Value,[comp17]);
          RDPrint1.ImpF(iLinha + 1,32,qryPosCaixaAtualcAgencia.Value,[comp17]);
          RDPrint1.ImpF(iLinha + 1,40,qryPosCaixaAtualnCdConta.Value,[comp17]);
          RDPrint1.ImpVal(iLinha + 1,48,'#########,##0.00',qryPosCaixaAtualnSaldoConta.Value,[comp17]);
          RDPrint1.ImpF(iLinha + 1,60,qryPosCaixaAtualdDtSaldo.AsString,[comp17]);
          RDPrint1.ImpVal(iLinha + 1,71,'#########,##0.00',qryPosCaixaAtualnValLimite.Value,[comp17]);
          iLinha := iLinha + 1;

          qryPosCaixaAtual.Next;
      end;

      RDPrint1.ImpF(iLinha + 1,40,'TOTAL',[comp17]);
      RDPrint1.ImpVal(iLinha + 1,48,'#########,##0.00',qryPosCaixaAtualnSaldoTotal.Value,[comp17]);
      RDPrint1.ImpVal(iLinha + 1,71,'#########,##0.00',qryPosCaixaAtualnValLimiteTotal.Value,[comp17]);
  end
  else begin
      RDPrint1.ImpF(iLinha,1,'Saldo Total',[negrito,comp12]);
      RDPrint1.ImpF(iLinha,13,'Limite Cr�dito Total',[negrito,comp12]);

      RDPrint1.ImpVal(iLinha + 1,1,'#########,##0.00',qryPosCaixaAtualnSaldoTotal.Value,[comp17]);
      RDPrint1.ImpVal(iLinha + 1,20,'#########,##0.00',qryPosCaixaAtualnValLimiteTotal.Value,[comp17]);
  end;

  iLinha := iLinha + 1;

  PrintHeader('MOVFUTURA');

  qryDemonstraMovFutura.First;

  while not qryDemonstraMovFutura.Eof do
  begin

      if (iLinha > 61) then
      begin
          RDPrint1.Novapagina;
          PrintHeader('MOVFUTURA');
      end;

      RDPrint1.ImpF(iLinha + 1,1,qryDemonstraMovFuturadDtInicial.AsString,[comp12]);

      if (RadioGroup2.ItemIndex <> 0) then
      begin
          RDPrint1.ImpF(iLinha + 1,10,'a',[comp12]);
          RDPrint1.ImpF(iLinha + 1,12,qryDemonstraMovFuturadDtFinal.AsString,[comp12]);
      end;

      RDPrint1.ImpVal(iLinha + 1,21,'#########,##0.00',qryDemonstraMovFuturanValTotalReceber.Value,[comp12]);
      RDPrint1.ImpVal(iLinha + 1,35,'#########,##0.00',qryDemonstraMovFuturanValTotalPagar.Value,[comp12]);
      RDPrint1.ImpVal(iLinha + 1,50,'#########,##0.00',(qryDemonstraMovFuturanValTotalReceber.Value-qryDemonstraMovFuturanValTotalPagar.Value),[comp12]);
      RDPrint1.ImpVal(iLinha + 1,67,'#########,##0.00',qryDemonstraMovFuturanValSaldoAcumulado.Value,[comp12]);

      iLinha := iLinha + 1;
      qryDemonstraMovFutura.Next;
  end;

  iLinha := iLinha + 1;
  
  RDPrint1.ImpF(iLinha,1,'Total do Per�odo',[comp12,negrito]);
  RDPrint1.ImpVal(iLinha,21,'#########,##0.00',qryDemonstraMovFuturanValTotalReceberPeriodo.Value,[comp12,negrito]);
  RDPrint1.ImpVal(iLinha,35,'#########,##0.00',qryDemonstraMovFuturanValTotalPagarPeriodo.Value,[comp12,negrito]);
  RDPrint1.ImpVal(iLinha,50,'#########,##0.00',(qryDemonstraMovFuturanValTotalReceberPeriodo.Value-qryDemonstraMovFuturanValTotalPagarPeriodo.Value),[comp12,negrito]);

  iLinha := iLinha + 2;

  if ((iLinha + 10) >= 61) then
  begin
      RDPrint1.Novapagina;
  end;

  RDPrint1.ImpF(iLinha,1,'RESUMO',[negrito,comp12,sublinhado]);

  iLinha := iLinha + 2;

  RDPrint1.ImpF(iLinha,1,'Total de Atrasados a Receber',[comp12]);
  RDPrint1.ImpF(iLinha + 1,1,'Total de Atrasados a Pagar',[comp12]);
  RDPrint1.ImpF(iLinha + 2,1,'Disponibilidade Total',[comp12]);
  RDPrint1.ImpF(iLinha + 3,1,'Disponibilidade Imediata',[comp12]);
  RDPrint1.ImpF(iLinha + 4,1,'Total a Receber',[comp12]);
  RDPrint1.ImpF(iLinha + 5,1,'Total a Pagar',[comp12]);
  RDPrint1.ImpF(iLinha + 6,1,'Saldo sem Limite Cr�dito',[comp12]);
  RDPrint1.ImpF(iLinha + 7,1,'Limite de Cr�dito',[comp12]);
  RDPrint1.ImpF(iLinha + 8,1,'Saldo Total',[comp12]);

  qryExpectativaCaixa.First;
  
  RDPrint1.ImpVal(iLinha,30,'#########,##0.00',qryExpectativaCaixanValTotalAtrasReceber.Value,[comp12,negrito]);
  RDPrint1.ImpVal(iLinha + 1,30,'#########,##0.00',qryExpectativaCaixanValTotalAtrasPagar.Value,[comp12,negrito]);
  RDPrint1.ImpVal(iLinha + 2,30,'#########,##0.00',qryExpectativaCaixanValDispTotal.Value,[comp12,negrito]);
  RDPrint1.ImpVal(iLinha + 3,30,'#########,##0.00',qryExpectativaCaixanValDispImediata.Value,[comp12,negrito]);
  RDPrint1.ImpVal(iLinha + 4,30,'#########,##0.00',qryExpectativaCaixanValTotalReceber.Value,[comp12,negrito]);
  RDPrint1.ImpVal(iLinha + 5,30,'#########,##0.00',qryExpectativaCaixanValTotalPagar.Value,[comp12,negrito]);
  RDPrint1.ImpVal(iLinha + 6,30,'#########,##0.00',qryExpectativaCaixanValSaldoSemLimCred.Value,[comp12,negrito]);
  RDPrint1.ImpVal(iLinha + 7,30,'#########,##0.00',qryExpectativaCaixanValLimiteCred.Value,[comp12,negrito]);
  RDPrint1.ImpVal(iLinha + 8,30,'#########,##0.00',qryExpectativaCaixanSaldoTotal.Value,[comp12,negrito]);

  iLinha := iLinha + 10;

  RDPrint1.ImpF(iLinha,1,'RESUMO DE CHEQUES A RECEBER',[comp12]);
  RDPrint1.ImpF(iLinha + 1,1,'Dispon�vel Dep�sito',[comp12]);
  RDPrint1.ImpF(iLinha + 2,1,'Depositado',[comp12]);
  RDPrint1.ImpF(iLinha + 3,1,'Depositado Custodia',[comp12]);

  RDPrint1.ImpVal(iLinha + 1,30,'#########,##0.00',qryExpectativaCaixanValChequeDepositar.Value,[comp12,negrito]);
  RDPrint1.ImpVal(iLinha + 2,30,'#########,##0.00',qryExpectativaCaixanValChequeDepositado.Value,[comp12,negrito]);
  RDPrint1.ImpVal(iLinha + 3,30,'#########,##0.00',qryExpectativaCaixanValChequeCustodia.Value,[comp12,negrito]);

  iLinha := iLinha + 5;


  if ((CheckBox7.Checked) or (CheckBox8.Checked)) then
  begin

      RDPrint1.ImpF(iLinha,1,'MOVIMENTA��O ANAL�TICA',[negrito,comp12,sublinhado]);

      qryMovAnalitico.First;

      while not qryMovAnalitico.Eof do
      begin

          if (iPeriodo <> qryMovAnaliticoiPeriodo.Value) then
          begin

              nPeriodo         := qryMovAnaliticoiPeriodo.Value;
              nValTotalPagar   := qryMovAnaliticonValTotalPagar.Value;
              nValTotalReceber := qryMovAnaliticonValTotalReceber.Value;

              iLinha := iLinha + 2;

              if (iLinha + 2 >= 61) then
              begin
                  RDPrint1.Novapagina;
              end;

              PrintHeader('ANALITICO');

          end;

          RDPrint1.ImpF(iLinha + 1,1,qryMovAnaliticodDtVenc.AsString,[comp17]);
          RDPrint1.ImpF(iLinha + 1,9,qryMovAnaliticonCdTitulo.AsString,[comp17]);
          RDPrint1.ImpF(iLinha + 1,17,qryMovAnaliticocNrTit.AsString,[comp17]);
          RDPrint1.ImpF(iLinha + 1,25,qryMovAnaliticocNmEspTit.Value,[comp17]);
          RDPrint1.ImpF(iLinha + 1,39,qryMovAnaliticocNmTerceiro.Value,[comp17]);
          RDPrint1.ImpF(iLinha + 1,55,qryMovAnaliticocNmCategFinanc.Value,[comp17]);
          RDPrint1.ImpVal(iLinha + 1,69,'######,##0.00',qryMovAnaliticonSaldoTit.Value,[comp17]);
          RDPrint1.ImpF(iLinha + 1,78,qryMovAnaliticocSenso.Value,[comp17]);

          iPeriodo := qryMovAnaliticoiPeriodo.Value;
          iLinha   := iLinha + 1;

          if (iLinha >= 61) then
          begin
              RDPrint1.Novapagina;
              PrintHeader('ANALITICO');
          end;

          qryMovAnalitico.Next;

          if ((iPeriodo <> qryMovAnaliticoiPeriodo.Value) or (qryMovAnalitico.Eof)) then
          begin
              iLinha := iLinha + 1;

              if (iLinha >= 61) then
              begin
                  RDPrint1.Novapagina;
              end;
              
              RDPrint1.ImpF(iLinha,55,'TOTAL A PAGAR',[negrito,comp17]);
              RDPrint1.ImpVal(iLinha,69,'######,##0.00',nValTotalPagar,[negrito,comp17]);
              RDPrint1.ImpF(iLinha + 1,55,'TOTAL A RECEBER',[negrito,comp17]);
              RDPrint1.ImpVal(iLinha + 1,69,'######,##0.00',nValTotalReceber,[negrito,comp17]);

              iLinha := iLinha + 1;

          end;

      end;

  end;


  RDprint1.Fechar;

end;

procedure TrptFluxoCaixaNovo.PrintHeader(cNmHeader: String);
begin

    if ((cNmHeader = 'ANALITICO') and (RadioGroup3.ItemIndex = 0))then
    begin
        RDPrint1.ImpF(iLinha,1,'Per�odo de Movimenta��o: ' + qryMovAnaliticodDtInicial.AsString + ' a ' + qryMovAnaliticodDtFinal.AsString ,[negrito,comp17]);

        iLinha := iLinha + 1;

        RDPrint1.ImpF(iLinha,1,'Data Vencto',[negrito,comp17]);
        RDPrint1.ImpF(iLinha,9,'ID T�tulo',[negrito,comp17]);
        RDPrint1.ImpF(iLinha,17,'N�m. T�tulo',[negrito,comp17]);
        RDPrint1.ImpF(iLinha,25,'Esp�cie de T�tulo',[negrito,comp17]);
        RDPrint1.ImpF(iLinha,39,'Terceiro',[negrito,comp17]);
        RDPrint1.ImpF(iLinha,55,'Categ. Financeira',[negrito,comp17]);
        RDPrint1.ImpF(iLinha,74,'Saldo',[negrito,comp17]);
        RDPrint1.ImpF(iLinha,78,'Senso',[negrito,comp17]);
    end;

    if((cNmHeader = 'ANALITICO') and (RadioGroup3.ItemIndex = 2)) then
    begin
        ER2Excel1.Celula['B' + IntToStr(iLinha)].Text  := 'Per�odo de Movimenta��o: ' + qryMovAnaliticodDtInicial.AsString + ' a ' + qryMovAnaliticodDtFinal.AsString;
        ER2Excel1.Celula['B' + IntToStr(iLinha)].Width := 16;
        ER2Excel1.Celula['B' + IntToStr(iLinha)].Negrito;
        ER2Excel1.Celula['B' + IntToStr(iLinha)].Background := RGB(216,216,216);

        ER2Excel1.Celula['B' + IntToStr(iLinha)].Range('I' + IntToStr(iLinha + 1));

        iLinha := iLinha + 1;

        ER2Excel1.Celula['B' + IntToStr(iLinha)].Text  := 'Data Vencto';
        ER2Excel1.Celula['B' + IntToStr(iLinha)].Width := 16;
        ER2Excel1.Celula['B' + IntToStr(iLinha)].Negrito;

        ER2Excel1.Celula['C' + IntToStr(iLinha)].Text  := 'ID T�tulo';
        ER2Excel1.Celula['C' + IntToStr(iLinha)].Width := 13;
        ER2Excel1.Celula['C' + IntToStr(iLinha)].Negrito;

        ER2Excel1.Celula['D' + IntToStr(iLinha)].Text  := 'N�m. T�tulo';
        ER2Excel1.Celula['D' + IntToStr(iLinha)].Width := 17;
        ER2Excel1.Celula['D' + IntToStr(iLinha)].Negrito;

        ER2Excel1.Celula['E' + IntToStr(iLinha)].Text  := 'Esp�cie de T�tulo';
        ER2Excel1.Celula['E' + IntToStr(iLinha)].Width := 18;
        ER2Excel1.Celula['E' + IntToStr(iLinha)].Negrito;

        ER2Excel1.Celula['F' + IntToStr(iLinha)].Text  := 'Terceiro';
        ER2Excel1.Celula['F' + IntToStr(iLinha)].Width := 35;
        ER2Excel1.Celula['F' + IntToStr(iLinha)].Negrito;

        ER2Excel1.Celula['G' + IntToStr(iLinha)].Text  := 'Categ. Financeira';
        ER2Excel1.Celula['G' + IntToStr(iLinha)].Width := 16;
        ER2Excel1.Celula['G' + IntToStr(iLinha)].Negrito;

        ER2Excel1.Celula['H' + IntToStr(iLinha)].Text            := 'Saldo';
        ER2Excel1.Celula['H' + IntToStr(iLinha)].Width           := 14;
        ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haRight;
        ER2Excel1.Celula['H' + IntToStr(iLinha)].Negrito;

        ER2Excel1.Celula['I' + IntToStr(iLinha)].Text  := 'Senso';
        ER2Excel1.Celula['I' + IntToStr(iLinha)].Width := 16;
        ER2Excel1.Celula['I' + IntToStr(iLinha)].Negrito;

        iLinha := iLinha + 1;
    end;

    if (cNmHeader = 'CABE�ALHO') then
    begin
        RDPrint1.ImpF(1,1,frmMenu.cNmFantasiaEmpresa,[negrito]);
        RDPrint1.ImpD(1,80,'Data Emiss�o: ' + DateTimeToStr(Now()),[comp17]);
        RDPrint1.ImpF(2,1,'FLUXO DE CAIXA EM ' + qryMoedacSigla.Value,[negrito])  ;
        RDPrint1.ImpD(2,80,'P�gina: ' + frmMenu.ZeroEsquerda(intToStr(iPagina),3),[comp17]);

        iLinha := 3;

        if (qryMoedanCdMoeda.Value <> 1) then
        begin
            RDPrint1.ImpF(iLinha,1,'COTA��O ' + qryCotacaonValCotacao.AsString + ' EM ' + qryCotacaodDtCotacao.AsString,[negrito]);
            iLinha := iLinha + 1;
        end;

        RDPrint1.ImpF(iLinha,1,cFiltro,[negrito,comp12]);

        RDPrint1.impBox(iLinha + 1,01,'--------------------------------------------------------------------------------');

        iLinha := iLinha + 2;
    end;

    if (cNmHeader = 'MOVFUTURA') then
    begin
        iLinha := iLinha + 2;
        
        RDPrint1.ImpF(iLinha,1,'Per�odo',[negrito,comp12]);
        RDPrint1.ImpF(iLinha,27,'A Receber',[negrito,comp12]);
        RDPrint1.ImpF(iLinha,42,'A Pagar',[negrito,comp12]);
        RDPrint1.ImpF(iLinha,56,'Resultado',[negrito,comp12]);
        RDPrint1.ImpF(iLinha,73,'Acumulado',[negrito,comp12]);
   end;

end;

procedure TrptFluxoCaixaNovo.GeraPlanilha;
var
  iLinhaAux : integer;
  nPeriodo         : integer;
  iPeriodo         : integer;
  nValTotalPagar   : Double;
  nValTotalReceber : Double;
begin

  cFiltro := 'Periodos: ' + Trim(edtQtdPeriodos.Text) + '/ EMP: ' + frmMenu.ZeroEsquerda(Trim(edtEmpresa.Text),3);

  if ((frmMenu.LeParametro('VAREJO') = 'S') and (qryLojanCdLoja.AsString <> '')) then
      cFiltro := cFiltro + '/ LOJA: ' + frmMenu.ZeroEsquerda(Trim(edtLoja.Text),3);

  if (CheckBox1.Checked) then
      cFiltro := cFiltro + '/ TTA: SIM'
  else cFiltro := cFiltro + '/ TTA: N�O';

  if (CheckBox2.Checked) then
      cFiltro := cFiltro + '/ TTR: SIM'
  else cFiltro := cFiltro + '/ TTR: N�O';

  if (CheckBox3.Checked) then
      cFiltro := cFiltro + '/ TTP: SIM'
  else cFiltro := cFiltro + '/ TTP: N�O';

  if (CheckBox4.Checked) then
      cFiltro := cFiltro + '/ PDV: SIM'
  else cFiltro := cFiltro + '/ PDV: N�O';

  if (CheckBox5.Checked) then
      cFiltro := cFiltro + '/ PDC: SIM'
  else cFiltro := cFiltro + '/ PDC: N�O';

  {inicia a gera��o da planilha}

  ER2Excel1.Celula['A1'].Width := 2;
  ER2Excel1.Celula['B2'].Mesclar('D2');
  ER2Excel1.Celula['B2'].HorizontalAlign := haLeft;
  ER2Excel1.Celula['B2'].Text := 'FLUXO DE CAIXA EM ' + qryMoedacSigla.Value;

  iLinha := 3;

  if (qryMoedanCdMoeda.Value <> 1) then
  begin
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := 'COTA��O ' + qryCotacaonValCotacao.AsString + ' EM ' + qryCotacaodDtCotacao.AsString;
      ER2Excel1.Celula['B' + IntToStr(iLinha)].HorizontalAlign := haLeft;
      iLinha := iLinha + 2;
  end;

  ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := cFiltro;

  iLinha := iLinha + 2;

  ER2Excel1.Celula['B2'].Negrito;
  ER2Excel1.Celula['B' + IntToStr(iLinha)].Negrito;
  ER2Excel1.Celula['C' + IntToStr(iLinha)].Negrito;
  ER2Excel1.Celula['E' + IntToStr(iLinha)].Negrito;
  ER2Excel1.Celula['F' + IntToStr(iLinha)].Negrito;
  ER2Excel1.Celula['G' + IntToStr(iLinha)].Negrito;
  ER2Excel1.Celula['H' + IntToStr(iLinha)].Negrito;

  ER2Excel1.Celula['B' + IntToStr(iLinha)].Border     := [EdgeBottom];
  ER2Excel1.Celula['B' + IntToStr(iLinha)].Background := RGB(216,216,216);

  ER2Excel1.Celula['B' + IntToStr(iLinha)].Range('H' + IntToStr(iLinha));

  ER2Excel1.Celula['B' + IntToStr(iLinha)].Text  := 'Empresa/Loja';
  ER2Excel1.Celula['B' + IntToStr(iLinha)].Width := 16;

  ER2Excel1.Celula['C' + IntToStr(iLinha)].Text  := 'Banco';
  ER2Excel1.Celula['C' + IntToStr(iLinha)].Width := 13;

  ER2Excel1.Celula['E' + IntToStr(iLinha)].Text            := 'Ag�ncia/Conta';
  ER2Excel1.Celula['E' + IntToStr(iLinha)].Width           := 18;
  ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight;

  ER2Excel1.Celula['F' + IntToStr(iLinha)].Text            := 'Saldo';
  ER2Excel1.Celula['F' + IntToStr(iLinha)].Width           := 35;
  ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight;

  ER2Excel1.Celula['G' + IntToStr(iLinha)].Text            := 'Dt. Saldo';
  ER2Excel1.Celula['G' + IntToStr(iLinha)].Width           := 16;
  ER2Excel1.Celula['G' + IntToStr(iLinha)].HorizontalAlign := haRight;

  ER2Excel1.Celula['H' + IntToStr(iLinha)].Text            := 'Limite Cr�dito';
  ER2Excel1.Celula['H' + IntToStr(iLinha)].Width           := 14;
  ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haRight;

  iLinha := iLinha + 1;

  iLinhaAux := iLinha;

  while not qryPosCaixaAtual.Eof do
  begin
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Text       := qryPosCaixaAtualcEmpresaLoja.Value;
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Width      := 16;
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Background := RGB(242,242,242);

      ER2Excel1.Celula['C' + IntToStr(iLinha)].Text       := qryPosCaixaAtualcBanco.Value;
      ER2Excel1.Celula['C' + IntToStr(iLinha)].Width      := 13;

      ER2Excel1.Celula['E' + IntToStr(iLinha)].Text            := Trim(qryPosCaixaAtualcAgencia.AsString) + '/' + Trim(qryPosCaixaAtualnCdConta.AsString);
      ER2Excel1.Celula['E' + IntToStr(iLinha)].Width           := 18;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['F' + IntToStr(iLinha)].Text            := qryPosCaixaAtualnSaldoConta.Value;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Mascara         := '#.##0,00';
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Width           := 35;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['G' + IntToStr(iLinha)].Text            := qryPosCaixaAtualdDtSaldo.AsString;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].Width           := 16;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['H' + IntToStr(iLinha)].Text            := qryPosCaixaAtualnValLimite.Value;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].Mascara         := '#.##0,00';
      ER2Excel1.Celula['H' + IntToStr(iLinha)].Width           := 14;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haRight;

      iLinha := iLinha + 1;

      qryPosCaixaAtual.Next;
  end;

  ER2Excel1.Celula['B' + IntToStr(iLinhaAux)].Range('H' + IntToStr(iLinha - 1));

  ER2Excel1.Celula['F' + IntToStr(iLinha)].AutoSoma('F' + IntToStr(iLinhaAux),'F' + IntToStr(iLinha - 1));
  ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight;
  ER2Excel1.Celula['F' + IntToStr(iLinha)].Width           := 35;

  ER2Excel1.Celula['H' + IntToStr(iLinha)].AutoSoma('H' + IntToStr(iLinhaAux),'H' + IntToStr(iLinha - 1));
  ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haRight;
  ER2Excel1.Celula['H' + IntToStr(iLinha)].Width           := 14;

  iLinha := iLinha + 2;

  ER2Excel1.Celula['B' + IntToStr(iLinha)].Text  := 'Per�odo';
  ER2Excel1.Celula['B' + IntToStr(iLinha)].Width := 16;
  ER2Excel1.Celula['B' + IntToStr(iLinha)].Negrito;

  ER2Excel1.Celula['D' + IntToStr(iLinha)].Text  := 'A Receber';
  ER2Excel1.Celula['D' + IntToStr(iLinha)].Width := 17;
  ER2Excel1.Celula['D' + IntToStr(iLinha)].HorizontalAlign := haRight;
  ER2Excel1.Celula['D' + IntToStr(iLinha)].Negrito;

  ER2Excel1.Celula['E' + IntToStr(iLinha)].Text            := 'A Pagar';
  ER2Excel1.Celula['E' + IntToStr(iLinha)].Width           := 18;
  ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight;
  ER2Excel1.Celula['E' + IntToStr(iLinha)].Negrito;

  ER2Excel1.Celula['F' + IntToStr(iLinha)].Text            := 'Acumulado';
  ER2Excel1.Celula['F' + IntToStr(iLinha)].Width           := 35;
  ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight;
  ER2Excel1.Celula['F' + IntToStr(iLinha)].Negrito;

  iLinha    := iLinha + 1;
  iLinhaAux := iLinha;

  {-- Movimenta��o futura --}

  while not qryDemonstraMovFutura.Eof do
  begin

      ER2Excel1.Celula['B' + IntToStr(iLinha)].Text  := qryDemonstraMovFuturadDtInicial.AsString;
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Width := 16;

      if (RadioGroup2.ItemIndex <> 0) then
      begin
          ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := ER2Excel1.Celula['B' + IntToStr(iLinha)].Text + ' a ' + qryDemonstraMovFuturadDtFinal.AsString;
      end;

      ER2Excel1.Celula['D' + IntToStr(iLinha)].Text  := qryDemonstraMovFuturanValTotalReceber.Value;
      ER2Excel1.Celula['D' + IntToStr(iLinha)].Mascara         := '#.##0,00';
      ER2Excel1.Celula['D' + IntToStr(iLinha)].Width := 17;
      ER2Excel1.Celula['D' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['E' + IntToStr(iLinha)].Text            := qryDemonstraMovFuturanValTotalPagar.Value;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].Mascara         := '#.##0,00';
      ER2Excel1.Celula['E' + IntToStr(iLinha)].Width           := 18;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight;

      ER2Excel1.Celula['F' + IntToStr(iLinha)].Text            := qryDemonstraMovFuturanValSaldoAcumulado.Value;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Mascara         := '#.##0,00';
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Width           := 35;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight;

      iLinha := iLinha + 1;
      qryDemonstraMovFutura.Next;
  end;

  ER2Excel1.Celula['B' + IntToStr(iLinha)].Text  := 'Total do Per�odo';
  ER2Excel1.Celula['B' + IntToStr(iLinha)].Width := 16;
  ER2Excel1.Celula['B' + IntToStr(iLinha)].Negrito;

  ER2Excel1.Celula['D' + IntToStr(iLinha)].AutoSoma('D' + IntToStr(iLinhaAux),'D' + IntToStr(iLinha - 1));
  ER2Excel1.Celula['D' + IntToStr(iLinha)].Negrito;
  ER2Excel1.Celula['D' + IntToStr(iLinha)].Width           := 17;
  ER2Excel1.Celula['D' + IntToStr(iLinha)].HorizontalAlign := haRight;

  ER2Excel1.Celula['E' + IntToStr(iLinha)].AutoSoma('E' + IntToStr(iLinhaAux),'E' + IntToStr(iLinha - 1));
  ER2Excel1.Celula['E' + IntToStr(iLinha)].Negrito;
  ER2Excel1.Celula['E' + IntToStr(iLinha)].Width           := 18;
  ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight;

  iLinha := iLinha + 2;

  ER2Excel1.Celula['B' + IntToStr(iLinha)].Text  := 'RESUMO';
  ER2Excel1.Celula['B' + IntToStr(iLinha)].Width := 16;
  ER2Excel1.Celula['B' + IntToStr(iLinha)].Negrito;

  iLinha := iLinha + 1;

  ER2Excel1.Celula['B' + IntToStr(iLinha)].Text  := 'Total de Atrasados a Receber';
  ER2Excel1.Celula['B' + IntToStr(iLinha)].Width := 16;
  ER2Excel1.Celula['B' + IntToStr(iLinha)].HorizontalAlign := haRight;
  ER2Excel1.Celula['B' + IntToStr(iLinha)].Mesclar('C' + IntToStr(iLinha));

  ER2Excel1.Celula['B' + IntToStr(iLinha + 1)].Text  := 'Total de Atrasados a Pagar';
  ER2Excel1.Celula['B' + IntToStr(iLinha + 1)].Width := 16;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 1)].HorizontalAlign := haRight;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 1)].Mesclar('C' + IntToStr(iLinha + 1));

  ER2Excel1.Celula['B' + IntToStr(iLinha + 2)].Text  := 'Disponibilidade Total';
  ER2Excel1.Celula['B' + IntToStr(iLinha + 2)].Width := 16;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 2)].HorizontalAlign := haRight;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 2)].Mesclar('C' + IntToStr(iLinha + 2));

  ER2Excel1.Celula['B' + IntToStr(iLinha + 3)].Text  := 'Disponibilidade Imediata';
  ER2Excel1.Celula['B' + IntToStr(iLinha + 3)].Width := 16;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 3)].HorizontalAlign := haRight;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 3)].Mesclar('C' + IntToStr(iLinha + 3));

  ER2Excel1.Celula['B' + IntToStr(iLinha + 4)].Text  := 'Total a Receber';
  ER2Excel1.Celula['B' + IntToStr(iLinha + 4)].Width := 16;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 4)].HorizontalAlign := haRight;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 4)].Mesclar('C' + IntToStr(iLinha + 4));

  ER2Excel1.Celula['B' + IntToStr(iLinha + 5)].Text  := 'Total a Pagar';
  ER2Excel1.Celula['B' + IntToStr(iLinha + 5)].Width := 16;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 5)].HorizontalAlign := haRight;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 5)].Mesclar('C' + IntToStr(iLinha + 5));

  ER2Excel1.Celula['B' + IntToStr(iLinha + 6)].Text  := 'Saldo Sem Limite Cr�dito';
  ER2Excel1.Celula['B' + IntToStr(iLinha + 6)].Width := 16;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 6)].HorizontalAlign := haRight;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 6)].Mesclar('C' + IntToStr(iLinha + 6));

  ER2Excel1.Celula['B' + IntToStr(iLinha + 7)].Text  := 'Limite de Cr�dito';
  ER2Excel1.Celula['B' + IntToStr(iLinha + 7)].Width := 16;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 7)].HorizontalAlign := haRight;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 7)].Mesclar('C' + IntToStr(iLinha + 7));

  ER2Excel1.Celula['B' + IntToStr(iLinha + 8)].Text  := 'Saldo Total';
  ER2Excel1.Celula['B' + IntToStr(iLinha + 8)].Width := 16;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 8)].HorizontalAlign := haRight;
  ER2Excel1.Celula['B' + IntToStr(iLinha + 8)].Mesclar('C' + IntToStr(iLinha + 8));

  qryExpectativaCaixa.First;

  ER2Excel1.Celula['D' + IntToStr(iLinha)].Text            := qryExpectativaCaixanValTotalAtrasReceber.Value;
  ER2Excel1.Celula['D' + IntToStr(iLinha)].Mascara         := '#.##0,00';
  ER2Excel1.Celula['D' + IntToStr(iLinha)].Width           := 17;
  ER2Excel1.Celula['D' + IntToStr(iLinha)].HorizontalAlign := haRight;
  ER2Excel1.Celula['D' + IntToStr(iLinha)].Negrito;

  ER2Excel1.Celula['D' + IntToStr(iLinha + 1)].Text            := qryExpectativaCaixanValTotalAtrasPagar.Value;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 1)].Mascara         := '#.##0,00';
  ER2Excel1.Celula['D' + IntToStr(iLinha + 1)].Width           := 17;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 1)].HorizontalAlign := haRight;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 1)].Negrito;

  ER2Excel1.Celula['D' + IntToStr(iLinha + 2)].Text            := qryExpectativaCaixanValDispTotal.Value;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 2)].Mascara         := '#.##0,00';
  ER2Excel1.Celula['D' + IntToStr(iLinha + 2)].Width           := 17;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 2)].HorizontalAlign := haRight;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 2)].Negrito;

  ER2Excel1.Celula['D' + IntToStr(iLinha + 3)].Text            := qryExpectativaCaixanValDispImediata.Value;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 3)].Mascara         := '#.##0,00';
  ER2Excel1.Celula['D' + IntToStr(iLinha + 3)].Width           := 17;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 3)].HorizontalAlign := haRight;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 3)].Negrito;

  ER2Excel1.Celula['D' + IntToStr(iLinha + 4)].Text            := qryExpectativaCaixanValTotalReceber.Value;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 4)].Mascara         := '#.##0,00';
  ER2Excel1.Celula['D' + IntToStr(iLinha + 4)].Width           := 17;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 4)].HorizontalAlign := haRight;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 4)].Negrito;

  ER2Excel1.Celula['D' + IntToStr(iLinha + 5)].Text            := qryExpectativaCaixanValTotalPagar.Value;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 5)].Mascara         := '#.##0,00';
  ER2Excel1.Celula['D' + IntToStr(iLinha + 5)].Width           := 17;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 5)].HorizontalAlign := haRight;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 5)].Negrito;

  ER2Excel1.Celula['D' + IntToStr(iLinha + 6)].Text            := qryExpectativaCaixanValSaldoSemLimCred.Value;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 6)].Mascara         := '#.##0,00';
  ER2Excel1.Celula['D' + IntToStr(iLinha + 6)].Width           := 17;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 6)].HorizontalAlign := haRight;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 6)].Negrito;

  ER2Excel1.Celula['D' + IntToStr(iLinha + 7)].Text            := qryExpectativaCaixanValLimiteCred.Value;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 7)].Mascara         := '#.##0,00';
  ER2Excel1.Celula['D' + IntToStr(iLinha + 7)].Width           := 17;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 7)].HorizontalAlign := haRight;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 7)].Negrito;

  ER2Excel1.Celula['D' + IntToStr(iLinha + 8)].Text            := qryExpectativaCaixanSaldoTotal.Value;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 8)].Mascara         := '#.##0,00';
  ER2Excel1.Celula['D' + IntToStr(iLinha + 8)].Width           := 17;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 8)].HorizontalAlign := haRight;
  ER2Excel1.Celula['D' + IntToStr(iLinha + 8)].Negrito;

  iLinha := iLinha + 10;

  if ((CheckBox7.Checked) or (CheckBox8.Checked)) then
  begin

      ER2Excel1.Celula['B' + IntToStr(iLinha)].Text  := 'MOVIMENTA��O ANAL�TICA';
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Width := 16;
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Negrito;

      qryMovAnalitico.First;

      while not qryMovAnalitico.Eof do
      begin

          if (iPeriodo <> qryMovAnaliticoiPeriodo.Value) then
          begin

              nPeriodo         := qryMovAnaliticoiPeriodo.Value;
              nValTotalPagar   := qryMovAnaliticonValTotalPagar.Value;
              nValTotalReceber := qryMovAnaliticonValTotalReceber.Value;

              iLinha := iLinha + 2;

              PrintHeader('ANALITICO');

          end;

          ER2Excel1.Celula['B' + IntToStr(iLinha)].Text  := qryMovAnaliticodDtVenc.asDateTime;
          ER2Excel1.Celula['B' + IntToStr(iLinha)].Width := 16;

          ER2Excel1.Celula['C' + IntToStr(iLinha)].Text  := qryMovAnaliticonCdTitulo.AsString;
          ER2Excel1.Celula['C' + IntToStr(iLinha)].Width := 13;

          ER2Excel1.Celula['D' + IntToStr(iLinha)].Text  := qryMovAnaliticocNrTit.AsString;
          ER2Excel1.Celula['D' + IntToStr(iLinha)].Width := 17;

          ER2Excel1.Celula['E' + IntToStr(iLinha)].Text  := qryMovAnaliticocNmEspTit.Value;
          ER2Excel1.Celula['E' + IntToStr(iLinha)].Width := 24;

          ER2Excel1.Celula['F' + IntToStr(iLinha)].Text  := qryMovAnaliticocNmTerceiro.Value;
          ER2Excel1.Celula['F' + IntToStr(iLinha)].Width := 35;

          ER2Excel1.Celula['G' + IntToStr(iLinha)].Text  := qryMovAnaliticocNmCategFinanc.Value;
          ER2Excel1.Celula['G' + IntToStr(iLinha)].Width := 19;

          ER2Excel1.Celula['H' + IntToStr(iLinha)].Text            := qryMovAnaliticonSaldoTit.AsFloat;
          ER2Excel1.Celula['H' + IntToStr(iLinha)].Mascara         := '#.##0,00';
          ER2Excel1.Celula['H' + IntToStr(iLinha)].Width           := 19;
          ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haRight;

          ER2Excel1.Celula['I' + IntToStr(iLinha)].Text  := qryMovAnaliticocSenso.Value;
          ER2Excel1.Celula['I' + IntToStr(iLinha)].Width := 16;

          iPeriodo := qryMovAnaliticoiPeriodo.Value;
          iLinha   := iLinha + 1;

          qryMovAnalitico.Next;

          if ((nPeriodo <> qryMovAnaliticoiPeriodo.Value) or (qryMovAnalitico.Eof)) then
          begin

              ER2Excel1.Celula['G' + IntToStr(iLinha)].Text  := 'TOTAL A PAGAR';
              ER2Excel1.Celula['G' + IntToStr(iLinha)].Width := 19;
              ER2Excel1.Celula['G' + IntToStr(iLinha)].Negrito;

              ER2Excel1.Celula['H' + IntToStr(iLinha)].Text            := nValTotalPagar;
              ER2Excel1.Celula['H' + IntToStr(iLinha)].Mascara         := '#.##0,00';
              ER2Excel1.Celula['H' + IntToStr(iLinha)].Width           := 19;
              ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haRight;
              ER2Excel1.Celula['H' + IntToStr(iLinha)].Negrito;

              ER2Excel1.Celula['G' + IntToStr(iLinha + 1)].Text  := 'TOTAL A RECEBER';
              ER2Excel1.Celula['G' + IntToStr(iLinha + 1)].Width := 19;
              ER2Excel1.Celula['G' + IntToStr(iLinha + 1)].Negrito;

              ER2Excel1.Celula['H' + IntToStr(iLinha + 1)].Text            := nValTotalReceber;
              ER2Excel1.Celula['H' + IntToStr(iLinha + 1)].Mascara         := '#.##0,00';
              ER2Excel1.Celula['H' + IntToStr(iLinha + 1)].Width           := 19;
              ER2Excel1.Celula['H' + IntToStr(iLinha + 1)].HorizontalAlign := haRight;
              ER2Excel1.Celula['H' + IntToStr(iLinha + 1)].Negrito;

              iLinha := iLinha + 1;

          end;

      end;
  end;

  { -- exporta planilha e limpa result do componente -- }
  ER2Excel1.ExportXLS;
  ER2Excel1.CleanupInstance;
end;

procedure TrptFluxoCaixaNovo.ER2Excel1BeforeExport(Sender: TObject);
begin
  inherited;

  frmMenu.mensagemUsuario('Exportando planilha...');
end;

procedure TrptFluxoCaixaNovo.ER2Excel1AfterExport(Sender: TObject);
begin
  inherited;

  frmMenu.mensagemUsuario('');
end;

procedure TrptFluxoCaixaNovo.TelaFluxoCaixa;
var
  objForm : TrptFluxoCaixaNovo_Tela;
begin

  objForm := TrptFluxoCaixaNovo_Tela.Create(nil);

  showForm(objForm,True);
  
end;

procedure TrptFluxoCaixaNovo.edtEmpresaExit(Sender: TObject);
begin
  inherited;

  atualizaSaldoInicial;

end;

procedure TrptFluxoCaixaNovo.atualizaSaldoInicial ;
begin

    qryPopulaSaldoInicial.Close;
    qryPopulaSaldoInicial.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro( edtEmpresa.Text );
    qryPopulaSaldoInicial.Parameters.ParamByName('nCdLoja').Value    := frmMenu.ConvInteiro( edtLoja.Text );
    qryPopulaSaldoInicial.ExecSQL;

    qrySaldoInicial.Close;
    qrySaldoInicial.Open;

end;

procedure TrptFluxoCaixaNovo.edtLojaExit(Sender: TObject);
begin
  inherited;
  atualizaSaldoInicial;
end;

initialization
  RegisterClass(TrptFluxoCaixaNovo);

{$R *.dfm}

end.
