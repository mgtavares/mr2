unit rRankingCompraCliente_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, DB, ADODB, QuickRpt, ExtCtrls;

type
  TrptRankingCompraCliente_view = class(TForm)
    QuickRep1: TQuickRep;
    PageHeaderBand1: TQRBand;
    DetailBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel4: TQRLabel;
    SPREL_RANKING_COMPRA_CLIENTES: TADOStoredProc;
    PageFooterBand1: TQRBand;
    QRLabel14: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel36: TQRLabel;
    QRDBText3: TQRDBText;
    QRLabel12: TQRLabel;
    QRShape2: TQRShape;
    QRLabel25: TQRLabel;
    SPREL_RANKING_COMPRA_CLIENTEScNmLoja: TStringField;
    SPREL_RANKING_COMPRA_CLIENTESnCdTerceiro: TIntegerField;
    SPREL_RANKING_COMPRA_CLIENTEScNmTerceiro: TStringField;
    SPREL_RANKING_COMPRA_CLIENTESnValTotalPedido: TBCDField;
    QRShape1: TQRShape;
    lblEmpresa: TQRLabel;
    QRLabel1: TQRLabel;
    QRShape3: TQRShape;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRLabel5: TQRLabel;
    QRLabel22: TQRLabel;
    lblFiltro: TQRLabel;
    QRShape6: TQRShape;
    QRLabel8: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRBand1: TQRBand;
    QRLabel9: TQRLabel;
    QRBand2: TQRBand;
    QRExpr1: TQRExpr;
    lblPosicao: TQRLabel;
    QRLabel13: TQRLabel;
    SPREL_RANKING_COMPRA_CLIENTESnCdLoja: TStringField;
    QRLabel11: TQRLabel;
    QRDBText4: TQRDBText;
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    iPosicao : Integer;
  end;

var
  rptRankingCompraCliente_view: TrptRankingCompraCliente_view;

implementation

{$R *.dfm}
uses
  fMenu;

procedure TrptRankingCompraCliente_view.DetailBand1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  lblPosicao.Caption := IntToStr(iPosicao) + '�';
  
  Inc(iPosicao);
end;

procedure TrptRankingCompraCliente_view.QuickRep1BeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  Inc(iPosicao);
end;

end.
