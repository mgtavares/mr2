unit fAjusteEmpenho_IncluirEditar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, ER2Lookup, DBCtrls;

type
  TfrmAjusteEmpenho_IncluirEditar = class(TfrmProcesso_Padrao)
    qryEstoqueEmpenhado: TADOQuery;
    qryEstoqueEmpenhadonCdEstoqueEmpenhado: TAutoIncField;
    qryEstoqueEmpenhadonCdProduto: TIntegerField;
    qryEstoqueEmpenhadonCdEmpresa: TIntegerField;
    qryEstoqueEmpenhadonQtdeEstoqueEmpenhado: TBCDField;
    qryEstoqueEmpenhadonCdLocalEstoque: TIntegerField;
    qryEstoqueEmpenhadodDtEmpenho: TDateTimeField;
    qryEstoqueEmpenhadonCdItemPedido: TIntegerField;
    qryEstoqueEmpenhadonCdOrdemProducao: TIntegerField;
    qryEstoqueEmpenhadocFlgInclusoManual: TIntegerField;
    qryEstoqueEmpenhadonCdUsuarioAltManual: TIntegerField;
    qryEstoqueEmpenhadodDtAltManual: TDateTimeField;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    Label7: TLabel;
    qryBuscaOP: TADOQuery;
    qryBuscaOPnCdOrdemProducao: TIntegerField;
    qryBuscaPedido: TADOQuery;
    qryBuscaPedidonCdItemPedido: TIntegerField;
    edtOP: TMaskEdit;
    edtPedido: TMaskEdit;
    qryBuscaOPcNumeroOP: TStringField;
    qryBuscaPedidonCdPedido: TIntegerField;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmLogin: TStringField;
    DBEdit8: TDBEdit;
    DataSource4: TDataSource;
    edtProduto: TER2LookupDBEdit;
    edtLocalEstoque: TER2LookupDBEdit;
    edtUsuarioAlt: TER2LookupDBEdit;
    procedure qryEstoqueEmpenhadoAfterScroll(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure qryEstoqueEmpenhadoBeforePost(DataSet: TDataSet);
    procedure edtPedidoChange(Sender: TObject);
    procedure edtOPChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAjusteEmpenho_IncluirEditar: TfrmAjusteEmpenho_IncluirEditar;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAjusteEmpenho_IncluirEditar.qryEstoqueEmpenhadoAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryBuscaOP.Close;
  qryBuscaOP.Parameters.ParamByName('cNumeroOP').Value := '';
  qryBuscaOP.Parameters.ParamByName('nCdOP').Value     := qryEstoqueEmpenhadonCdOrdemProducao.Value;
  qryBuscaOP.Open;

  qryBuscaPedido.Close;
  qryBuscaPedido.Parameters.ParamByName('nCdProduto').Value    := qryEstoqueEmpenhadonCdProduto.Value;
  qryBuscaPedido.Parameters.ParamByName('nCdItemPedido').Value := qryEstoqueEmpenhadonCdItemPedido.Value;
  qryBuscaPedido.Parameters.ParamByName('nCdPedido').Value     := 0;
  qryBuscaPedido.Open;

  edtOP.Text     := qryBuscaOPcNumeroOP.AsString;
  edtPedido.Text := qryBuscaPedidonCdPedido.AsString;

  edtProduto.PosicionaQuery;
  edtLocalEstoque.PosicionaQuery;
  edtUsuarioAlt.PosicionaQuery;
end;

procedure TfrmAjusteEmpenho_IncluirEditar.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (qryEstoqueEmpenhado.State = dsEdit) then
      if (MessageDlg('Confirma a altera��o do registro ?  ',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
          abort;

  if (qryEstoqueEmpenhado.State = dsInsert) then
      if (MessageDlg('Confirma a inclus�o deste registro ?  ',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
          abort;

  if ((qryEstoqueEmpenhado.State = dsEdit) or (qryEstoqueEmpenhado.State = dsInsert)) then
      qryEstoqueEmpenhado.Post;
      
  Close;
end;

procedure TfrmAjusteEmpenho_IncluirEditar.qryEstoqueEmpenhadoBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  qryBuscaOP.Close;
  qryBuscaOP.Parameters.ParamByName('cNumeroOP').Value := Trim(edtOP.Text);
  qryBuscaOP.Parameters.ParamByName('nCdOP').Value     := 0;
  qryBuscaOP.Open;

  if ((qryBuscaOP.RecordCount = 0) and (Trim(edtOp.Text) <> '')) then
  begin
      MensagemAlerta('Ordem de Produ��o n�mero: ' + Trim(edtOp.Text) + ' n�o encontrada. Digite uma OP v�lida.');
      abort;
  end;

  qryBuscaPedido.Close;
  qryBuscaPedido.Parameters.ParamByName('nCdProduto').Value    := qryEstoqueEmpenhadonCdProduto.Value;
  qryBuscaPedido.Parameters.ParamByName('nCdItemPedido').Value := 0;
  qryBuscaPedido.Parameters.ParamByName('nCdPedido').Value     := frmMenu.ConvInteiro(edtPedido.Text);
  qryBuscaPedido.Open;

  if ((qryBuscaPedido.RecordCount = 0) and (Trim(edtPedido.Text) <> '')) then
      if (MessageDlg('Este produto n�o pertence ao pedido digitado. Deseja continuar sem incluir o n�mero de pedido ?  ',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
          abort;

  if (qryEstoqueEmpenhadonQtdeEstoqueEmpenhado.Value <= 0) then
  begin
      MensagemAlerta('A quantidade a ser empenhada deve ser maior do que zero.');
      abort;
  end;

  qryEstoqueEmpenhadonCdItemPedido.AsString    := qryBuscaPedidonCdItemPedido.AsString;
  qryEstoqueEmpenhadonCdOrdemProducao.AsString := qryBuscaOPnCdOrdemProducao.AsString;
  qryEstoqueEmpenhadodDtAltManual.Value        := Now();
  qryEstoqueEmpenhadonCdUsuarioAltManual.Value := frmMenu.nCdUsuarioLogado;

  if (qryEstoqueEmpenhado.State = dsInsert) then
  begin
      qryEstoqueEmpenhadocFlgInclusoManual.Value := 1;
      qryEstoqueEmpenhadonCdEmpresa.Value        := frmMenu.nCdEmpresaAtiva;
      qryEstoqueEmpenhadodDtEmpenho.Value        := Now();
  end;
  
end;

procedure TfrmAjusteEmpenho_IncluirEditar.edtPedidoChange(Sender: TObject);
begin
  inherited;

  if ((qryEstoqueEmpenhado.State <> dsEdit) or (qryEstoqueEmpenhado.State <> dsInsert)) then
      qryEstoqueEmpenhado.Edit;
end;

procedure TfrmAjusteEmpenho_IncluirEditar.edtOPChange(Sender: TObject);
begin
  inherited;
  
  if ((qryEstoqueEmpenhado.State <> dsEdit) or (qryEstoqueEmpenhado.State <> dsInsert)) then
      qryEstoqueEmpenhado.Edit;
end;

end.
