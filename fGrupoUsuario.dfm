inherited frmGrupoUsuario_Cad: TfrmGrupoUsuario_Cad
  Caption = 'frmGrupoUsuario_Cad'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 856
    Height = 552
  end
  object Label1: TLabel [1]
    Left = 27
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 16
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 33
    Top = 94
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit3
  end
  inherited ToolBar2: TToolBar
    Width = 856
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 72
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdGrupoUsuario'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 72
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmGrupoUsuario'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [7]
    Left = 72
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [8]
    Tag = 1
    Left = 140
    Top = 88
    Width = 229
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsMaster
    TabOrder = 4
  end
  object cxPageControl1: TcxPageControl [9]
    Left = 16
    Top = 128
    Width = 785
    Height = 409
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 5
    ClientRectBottom = 405
    ClientRectLeft = 4
    ClientRectRight = 781
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Usu'#225'rios Vinculados'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 777
        Height = 381
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuarioGrupoUsuario
        FixedColor = clMoneyGreen
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        OddRowColor = clWhite
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghDialogFind, dghColumnResize, dghColumnMove]
        ParentFont = False
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdGrupoUsuario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Width = 74
          end
          item
            EditButtons = <>
            FieldName = 'cNmLogin'
            Footers = <>
            Width = 135
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'dDataAcesso'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuarioAcesso'
            Footers = <>
            ReadOnly = True
            Width = 103
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Acessos Permitidos'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 777
        Height = 381
        Align = alClient
        Color = clWhite
        DataGrouping.GroupLevels = <>
        DataSource = dsAplicacaoGrupoUsuario
        FixedColor = clMoneyGreen
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        OddRowColor = clWhite
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh2KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdGrupoUsuario'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdAplicacao'
            Footers = <>
            Width = 90
          end
          item
            EditButtons = <>
            FieldName = 'cNmAplicacao'
            Footers = <>
            ReadOnly = True
            Width = 322
          end
          item
            Checkboxes = True
            EditButtons = <>
            FieldName = 'cFlgAlterar'
            Footers = <>
            Width = 98
          end
          item
            EditButtons = <>
            FieldName = 'dDataAcesso'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuarioAcesso'
            Footers = <>
            ReadOnly = True
            Width = 109
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object btMenuAninhado: TcxButton [10]
    Left = 16
    Top = 544
    Width = 137
    Height = 33
    Caption = 'Lista de Permiss'#245'es'
    TabOrder = 6
    OnClick = btMenuAninhadoClick
    Glyph.Data = {
      06030000424D06030000000000003600000028000000100000000F0000000100
      180000000000D002000000000000000000000000000000000000C6D6DE8C9C9C
      8C9C9C8C9C9C8C9C9C8C9C9C8C9C9CC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6
      DEC6D6DEC6D6DEC6D6DE0000000000000000000000000000000000008C9C9CC6
      D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6000000009CFF
      00639C00639C00639C0000008C9C9CC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
      DEC6D6DEC6D6DEC6D6DE0000009CF7FF009CFF009CFF00639C0000008C9C9CC6
      D6DEC6D6DEC6D6DE8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C8C9C9C0000009CF7FF
      009CFF009CFF00639C0000008C9C9CC6D6DEC6D6DE0000000000000000000000
      000000000000008C9C9C0000009CF7FF9CF7FF9CF7FF009CFF0000008C9C9CC6
      D6DEC6D6DE0000006BD6BD007B5A007B5A007B5A0000008C9C9C000000000000
      000000000000000000000000C6D6DEC6D6DEC6D6DE0000009CDED66BD6BD6BD6
      BD007B5A0000008C9C9CC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6
      D6DEC6D6DE0000009CDED66BD6BD6BD6BD007B5A0000008C9C9CC6DEC6C6D6DE
      000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE0000009CDED69CDED69CDE
      D66BD6BD0000008C9C9CC6D6DEC6D6DE000000C6D6DEC6D6DEC6D6DEC6DEC6C6
      D6DEC6D6DE000000000000000000000000000000000000C6D6DEC6D6DEC6D6DE
      000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6
      DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000C6D6DEC6D6DEC6
      D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DE
      C6D6DEC6D6DEC6D6DE000000000000C6D6DEC6D6DEC6D6DEC6D6DE000000C6D6
      DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE00000000
      0000000000C6D6DEC6D6DE000000C6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
      C6D6DEC6D6DEC6DEC6C6D6DE000000C6D6DEC6D6DE000000000000000000C6D6
      DEC6D6DEC6D6DEC6D6DE}
    LookAndFeel.NativeStyle = True
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 *'
      'FROM GRUPOUSUARIO'
      'WHERE nCdGrupoUsuario = :nPK')
    object qryMasternCdGrupoUsuario: TIntegerField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdGrupoUsuario'
    end
    object qryMastercNmGrupoUsuario: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'cNmGrupoUsuario'
      Size = 50
    end
    object qryMasternCdStatus: TIntegerField
      DisplayLabel = 'Status'
      FieldName = 'nCdStatus'
    end
    object qryMastercNmStatus: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmStatus'
      LookupDataSet = qryStat
      LookupKeyFields = 'nCdStatus'
      LookupResultField = 'cNmStatus'
      KeyFields = 'nCdStatus'
      LookupCache = True
      Size = 50
      Lookup = True
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdUsuario'
      ',cNmUsuario'
      ',cNmLogin'
      'FROM USUARIO')
    Left = 616
    Top = 296
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryUsuariocNmLogin: TStringField
      FieldName = 'cNmLogin'
    end
  end
  object qryUsuarioGrupoUsuario: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    BeforePost = qryUsuarioGrupoUsuarioBeforePost
    Parameters = <
      item
        Name = 'nCdGrupoUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM USUARIOGRUPOUSUARIO'
      'WHERE nCdGrupoUsuario = :nCdGrupoUsuario')
    Left = 816
    Top = 120
    object qryUsuarioGrupoUsuarionCdGrupoUsuario: TIntegerField
      FieldName = 'nCdGrupoUsuario'
    end
    object qryUsuarioGrupoUsuarionCdUsuario: TIntegerField
      DisplayLabel = 'C'#243'd. Usu'#225'rio'
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioGrupoUsuariodDataAcesso: TDateTimeField
      DisplayLabel = 'Data Permiss'#227'o'
      FieldName = 'dDataAcesso'
    end
    object qryUsuarioGrupoUsuarionCdUsuarioAcesso: TIntegerField
      DisplayLabel = 'Usu'#225'rio Permiss'#227'o'
      FieldName = 'nCdUsuarioAcesso'
    end
    object qryUsuarioGrupoUsuariocNmUsuario: TStringField
      DisplayLabel = 'Nome Usu'#225'rio'
      FieldKind = fkLookup
      FieldName = 'cNmUsuario'
      LookupDataSet = qryUsuario
      LookupKeyFields = 'nCdUsuario'
      LookupResultField = 'cNmUsuario'
      KeyFields = 'nCdUsuario'
      LookupCache = True
      ReadOnly = True
      Size = 50
      Lookup = True
    end
    object qryUsuarioGrupoUsuariocNmLogin: TStringField
      DisplayLabel = 'Login Acesso'
      FieldKind = fkLookup
      FieldName = 'cNmLogin'
      LookupDataSet = qryUsuario
      LookupKeyFields = 'nCdUsuario'
      LookupResultField = 'cNmLogin'
      KeyFields = 'nCdUsuario'
      LookupCache = True
      ReadOnly = True
      Lookup = True
    end
    object qryUsuarioGrupoUsuarionCdUsuarioGrupoUsuario: TIntegerField
      FieldName = 'nCdUsuarioGrupoUsuario'
    end
  end
  object dsUsuarioGrupoUsuario: TDataSource
    DataSet = qryUsuarioGrupoUsuario
    Left = 824
    Top = 184
  end
  object qryAplicacaoGrupoUsuario: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    BeforePost = qryAplicacaoGrupoUsuarioBeforePost
    AfterPost = qryAplicacaoGrupoUsuarioAfterPost
    BeforeDelete = qryAplicacaoGrupoUsuarioBeforeDelete
    Parameters = <
      item
        Name = 'nCdGrupoUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM AplicacaoGRUPOUSUARIO'
      'WHERE nCdGrupoUsuario = :nCdGrupoUsuario')
    Left = 848
    Top = 320
    object qryAplicacaoGrupoUsuarionCdGrupoUsuario: TIntegerField
      FieldName = 'nCdGrupoUsuario'
    end
    object qryAplicacaoGrupoUsuarionCdAplicacao: TIntegerField
      DisplayLabel = 'C'#243'd. Aplica'#231#227'o'
      FieldName = 'nCdAplicacao'
    end
    object qryAplicacaoGrupoUsuariocNmAplicacao: TStringField
      DisplayLabel = 'Descri'#231#227'o Aplica'#231#227'o'
      DisplayWidth = 100
      FieldKind = fkLookup
      FieldName = 'cNmAplicacao'
      LookupDataSet = qryAplicacao
      LookupKeyFields = 'nCdAplicacao'
      LookupResultField = 'cNmAplicacao'
      KeyFields = 'nCdAplicacao'
      Size = 100
      Lookup = True
    end
    object qryAplicacaoGrupoUsuariodDataAcesso: TDateTimeField
      DisplayLabel = 'Data Permiss'#227'o'
      FieldName = 'dDataAcesso'
    end
    object qryAplicacaoGrupoUsuarionCdUsuarioAcesso: TIntegerField
      DisplayLabel = 'Usu'#225'rio Permiss'#227'o'
      FieldName = 'nCdUsuarioAcesso'
    end
    object qryAplicacaoGrupoUsuariocFlgAlterar: TIntegerField
      DisplayLabel = 'Permiss'#227'o Alterar'
      FieldName = 'cFlgAlterar'
    end
    object qryAplicacaoGrupoUsuarionCdAplicacaoGrupoUsuario: TIntegerField
      FieldName = 'nCdAplicacaoGrupoUsuario'
    end
  end
  object dsAplicacaoGrupoUsuario: TDataSource
    DataSet = qryAplicacaoGrupoUsuario
    Left = 880
    Top = 328
  end
  object qryAplicacao: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM APLICACAO')
    Left = 920
    Top = 280
    object qryAplicacaonCdAplicacao: TIntegerField
      FieldName = 'nCdAplicacao'
    end
    object qryAplicacaocNmAplicacao: TStringField
      FieldName = 'cNmAplicacao'
      Size = 50
    end
    object qryAplicacaonCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryAplicacaonCdModulo: TIntegerField
      FieldName = 'nCdModulo'
    end
    object qryAplicacaocNmObjeto: TStringField
      FieldName = 'cNmObjeto'
      Size = 50
    end
  end
  object qryParticulAplicacao: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM PARTICULAPLICACAO')
    Left = 816
    Top = 248
    object qryParticulAplicacaocSiglaParticul: TStringField
      FieldName = 'cSiglaParticul'
      FixedChar = True
      Size = 5
    end
    object qryParticulAplicacaonCdAplicacao: TIntegerField
      FieldName = 'nCdAplicacao'
    end
    object qryParticulAplicacaocNmParticul: TStringField
      FieldName = 'cNmParticul'
      Size = 50
    end
  end
  object qryParticulAplicacaoGrupoUsuario: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    BeforePost = qryParticulAplicacaoGrupoUsuarioBeforePost
    Parameters = <
      item
        Name = 'nCdGrupoUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdAplicacao'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM ParticulAplicacaoGrupoUsuario'
      'WHERE nCdGrupoUsuario = :nCdGrupoUsuario'
      'AND EXISTS(SELECT nCdAplicacao'
      '       FROM ParticulAplicacao'
      
        '      WHERE ParticulAplicacao.cSiglaParticul = ParticulAplicacao' +
        'GrupoUsuario.cSiglaParticul'
      '        AND ParticulAplicacao.nCdAplicacao   = :nCdAplicacao)')
    Left = 888
    Top = 192
    object qryParticulAplicacaoGrupoUsuarionCdGrupoUsuario: TIntegerField
      FieldName = 'nCdGrupoUsuario'
    end
    object qryParticulAplicacaoGrupoUsuariocSiglaParticul: TStringField
      DisplayLabel = 'Sigla'
      FieldName = 'cSiglaParticul'
      FixedChar = True
      Size = 5
    end
    object qryParticulAplicacaoGrupoUsuariocFlgPermissao: TIntegerField
      DisplayLabel = 'Permitido'
      FieldName = 'cFlgPermissao'
    end
    object qryParticulAplicacaoGrupoUsuariocNmParticul: TStringField
      DisplayLabel = 'Descri'#231#227'o Particularidade'
      FieldKind = fkLookup
      FieldName = 'cNmParticul'
      LookupDataSet = qryParticulAplicacao
      LookupKeyFields = 'cSiglaParticul'
      LookupResultField = 'cNmParticul'
      KeyFields = 'cSiglaParticul'
      LookupCache = True
      Size = 50
      Lookup = True
    end
  end
  object dsParticulAplicacaoGrupoUsuario: TDataSource
    DataSet = qryParticulAplicacaoGrupoUsuario
    Left = 928
    Top = 192
  end
end
