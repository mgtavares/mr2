unit rOscilacaoEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBCtrls, ADODB, StdCtrls, Mask, ER2Lookup,comObj, ExcelXP, ER2Excel;

type
  TrptOscilacaoEstoque = class(TfrmRelatorio_Padrao)
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    edtEmpresa: TER2LookupMaskEdit;
    edtLocalEstoque: TER2LookupMaskEdit;
    edtDepartamento: TER2LookupMaskEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit1: TDBEdit;
    dsEmpresa: TDataSource;
    DBEdit2: TDBEdit;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryCategoria: TADOQuery;
    qryCategorianCdCategoria: TAutoIncField;
    qryCategorianCdDepartamento: TIntegerField;
    qryCategoriacNmCategoria: TStringField;
    qryCategorianCdStatus: TIntegerField;
    qrySubCategoria: TADOQuery;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategorianCdCategoria: TIntegerField;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySubCategorianCdStatus: TIntegerField;
    qrySegmento: TADOQuery;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentonCdSubCategoria: TIntegerField;
    qrySegmentocNmSegmento: TStringField;
    qrySegmentonCdStatus: TIntegerField;
    qryDepartamento: TADOQuery;
    AutoIncField3: TAutoIncField;
    StringField3: TStringField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    DBEdit3: TDBEdit;
    dsLocalEstoque: TDataSource;
    DBEdit4: TDBEdit;
    dsDepartamento: TDataSource;
    DBEdit5: TDBEdit;
    dsCategoria: TDataSource;
    edtCategoria: TER2LookupMaskEdit;
    DBEdit6: TDBEdit;
    dsSegmento: TDataSource;
    edtSubCategoria: TER2LookupMaskEdit;
    qryGrupoInventario: TADOQuery;
    DBEdit7: TDBEdit;
    edtSegmento: TER2LookupMaskEdit;
    qryGrupoInventarionCdGrupoInventario: TIntegerField;
    qryGrupoInventariocNmGrupoInventario: TStringField;
    DBEdit8: TDBEdit;
    dsGrupoInventario: TDataSource;
    edtGrupoInventario: TER2LookupMaskEdit;
    edtProduto: TER2LookupMaskEdit;
    edtGrupoProduto: TER2LookupMaskEdit;
    MaskEditDataInicial: TMaskEdit;
    Label11: TLabel;
    MaskEditDataFinal: TMaskEdit;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DBEdit9: TDBEdit;
    dsProduto: TDataSource;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBEdit10: TDBEdit;
    dsGrupoProduto: TDataSource;
    dsSubCategoria: TDataSource;
    RadioGroupExibirValores: TRadioGroup;
    RadioGroupValorizacao: TRadioGroup;
    RadioGroupProdutoAtivo: TRadioGroup;
    RadioGroupSaida: TRadioGroup;
    RadioGroupTipoSaida: TRadioGroup;
    Label12: TLabel;
    MaskEditDtRecInicial: TMaskEdit;
    Label13: TLabel;
    MaskEditDtRecFinal: TMaskEdit;
    Label14: TLabel;
    ER2Excel1: TER2Excel;
    RadioGroupAgrupamento: TRadioGroup;
    procedure edtEmpresaBeforePosicionaQry(Sender: TObject);
    procedure edtCategoriaBeforePosicionaQry(Sender: TObject);
    procedure edtSubCategoriaBeforePosicionaQry(Sender: TObject);
    procedure edtSegmentoBeforePosicionaQry(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtCategoriaBeforeLookup(Sender: TObject);
    procedure edtSubCategoriaBeforeLookup(Sender: TObject);
    procedure edtSegmentoBeforeLookup(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtProdutoBeforeLookup(Sender: TObject);
    procedure ER2Excel1BeforeExport(Sender: TObject);
    procedure ER2Excel1AfterExport(Sender: TObject);
  private
    { Private declarations }
    iCursor : integer;
  public
    { Public declarations }
  end;

var
  rptOscilacaoEstoque: TrptOscilacaoEstoque;

implementation

uses fMenu, rOscilacaoEstoque_View, fOscilacaoEstoque_View;

{$R *.dfm}

 procedure TrptOscilacaoEstoque.edtEmpresaBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

procedure TrptOscilacaoEstoque.edtCategoriaBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;
  if (Trim(edtCategoria.Text) <> '') then
  begin
      edtCategoria.WhereAdicional.Text := 'nCdDepartamento = ' + edtDepartamento.Text ;
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := edtDepartamento.Text ;
  end;
end;

procedure TrptOscilacaoEstoque.edtSubCategoriaBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;
  if (Trim(edtSubCategoria.Text) <> '') then
  begin
      edtSubCategoria.WhereAdicional.Text := 'nCdCategoria = ' + edtCategoria.Text;
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := edtCategoria.Text;
  end;
end;

procedure TrptOscilacaoEstoque.edtSegmentoBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;
  if (Trim(edtSegmento.Text) <> '') then
  begin
      edtSegmento.WhereAdicional.Text := 'nCdSubCategoria = ' + edtSubCategoria.Text;
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := edtSubCategoria.Text;
  end;
end;

procedure TrptOscilacaoEstoque.FormShow(Sender: TObject);
begin
  inherited;
  edtEmpresa.Text := intToStr(frmMenu.nCdEmpresaAtiva);
  edtEmpresa.PosicionaQuery;
  edtLocalEstoque.SetFocus;
end;

procedure TrptOscilacaoEstoque.edtCategoriaBeforeLookup(Sender: TObject);
begin
  inherited;
  if (Trim(edtDepartamento.Text) <> '') then
  begin
     edtCategoria.WhereAdicional.Text := 'nCdDepartamento = ' + edtDepartamento.Text;
     qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := edtDepartamento.Text;
  end
  else
  begin
     ShowMessage('Departamento n�o selecionado');
     abort;
  end;
end;

procedure TrptOscilacaoEstoque.edtSubCategoriaBeforeLookup(
  Sender: TObject);
begin
  inherited;
  if (Trim(edtCategoria.Text) <> '') then
  begin
      edtSubCategoria.WhereAdicional.Text := 'nCdCategoria = ' + edtCategoria.Text;
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := edtCategoria.Text;
  end
  else
  begin
     ShowMessage('Categoria n�o selecionado');
     abort;
  end;

end;

procedure TrptOscilacaoEstoque.edtSegmentoBeforeLookup(Sender: TObject);
begin
  inherited;
  if (Trim(edtSubCategoria.Text) <> '') then
  begin
      edtSegmento.WhereAdicional.Text := 'nCdSubCategoria = ' + edtSubCategoria.Text;
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := edtSubCategoria.Text;
  end
  else
  begin
     ShowMessage('SubCategoria n�o selecionado');
     abort;
  end;

end;

procedure TrptOscilacaoEstoque.ToolButton1Click(Sender: TObject);
var
  objRel  : TrptOscilacaoEstoque_View;
  objForm : TfrmOscilacaoEstoque_View;

//  valorcampo     : string;
  cNmAgrupamento : String;
  iLinha         : integer;
  iLinhaAux      : integer;

begin

  objRel := TrptOscilacaoEstoque_View.Create(nil) ;

  try
      try
          // Saida Relatorio ou planilha
          if (RadioGroupSaida.ItemIndex <> 2) then
          begin 

              objRel.uspRelatorio.Close ;
              objRel.uspRelatorio.Parameters.ParamByName('@nCdEmpresa').Value               := frmMenu.ConvInteiro(edtEmpresa.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@nCdLocalEstoque').Value          := frmMenu.ConvInteiro(edtLocalEstoque.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@nCdGrupoProduto').Value          := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@nCdDepartamento').Value          := frmMenu.ConvInteiro(edtDepartamento.Text) ;

              objRel.uspRelatorio.Parameters.ParamByName('@nCdCategoria').Value             := frmMenu.ConvInteiro(edtCategoria.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@nCdSubCategoria').Value          := frmMenu.ConvInteiro(edtSubCategoria.Text) ;

              objRel.uspRelatorio.Parameters.ParamByName('@nCdSegmento').Value              := frmMenu.ConvInteiro(edtSegmento.Text) ;

              objRel.uspRelatorio.Parameters.ParamByName('@nCdGrupoInventario').Value       := frmMenu.ConvInteiro(edtGrupoInventario.Text) ;

              objRel.uspRelatorio.Parameters.ParamByName('@nCdProduto').Value               := frmMenu.ConvInteiro(edtProduto.Text) ;

              objRel.uspRelatorio.Parameters.ParamByName('@dDtInicial').Value               := frmMenu.ConvData(MaskEditDataInicial.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@dDtFinal').Value                 := frmMenu.ConvData(MaskEditDataFinal.Text) ;

              objRel.uspRelatorio.Parameters.ParamByName('@dDtRecebInicial').Value          := frmMenu.ConvData(MaskEditDtRecInicial.Text) ;
              objRel.uspRelatorio.Parameters.ParamByName('@dDtRecebFinal').Value            := frmMenu.ConvData(MaskEditDtRecFinal.Text) ;

              if (RadioGroupExibirValores.ItemIndex = 0) then
                  objRel.uspRelatorio.Parameters.ParamByName('@ExibirValores').Value        := 1
              else objRel.uspRelatorio.Parameters.ParamByName('@ExibirValores').Value       := 0 ;

              if (RadioGroupProdutoAtivo.ItemIndex = 0) then
                  objRel.uspRelatorio.Parameters.ParamByName('@SomenteProdutoAtivo').Value  := 1
              else objRel.uspRelatorio.Parameters.ParamByName('@SomenteProdutoAtivo').Value := 0 ;

              objRel.uspRelatorio.Parameters.ParamByName('@Valorizacao').Value         := RadioGroupValorizacao.ItemIndex;
              objRel.uspRelatorio.Parameters.ParamByName('@cFlgModoAgrupamento').Value := RadioGroupAgrupamento.ItemIndex;

              objRel.uspRelatorio.Open  ;

              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              objRel.lblFiltro1.Caption := '';

              if (Trim(edtEmpresa.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + 'Empresa : ' + Trim(EdtEmpresa.Text) + '-' + DbEdit1.Text ;

              if (Trim(EdtLocalEstoque.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Local Estoque : ' + Trim(EdtLocalEstoque.Text) + '-' + DbEdit3.Text ;

              if (Trim(edtGrupoProduto.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Grupo Produto: ' + Trim(edtGrupoProduto.Text) + '-' + DbEdit10.Text ;

              if (Trim(edtDepartamento.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Departamento: ' + Trim(edtDepartamento.Text) + '-' + DbEdit4.Text ;

              if (Trim(edtCategoria.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Categoria: ' + Trim(edtCategoria.Text) + '-' + DbEdit5.Text ;

              if (Trim(edtSubCategoria.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / SubCategoria: ' + Trim(edtSubCategoria.Text) + '-' + DbEdit6.Text ;

              if (Trim(edtSegmento.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Segmento: ' + Trim(edtSegmento.Text) + '-' + DbEdit7.Text ;

              if (Trim(edtGrupoInventario.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Grupo Invent�rio: ' + Trim(edtGrupoInventario.Text) + '-' + DbEdit8.Text ;

              if (Trim(edtProduto.Text) <> '') then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Produto: ' + Trim(edtProduto.Text) + '-' + DbEdit9.Text ;

              if ((Trim(MaskEditDataInicial.Text) <> '/  /') or (Trim(MaskEditDataFinal.Text) <> '/  /')) then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo : ' + Trim(MaskEditDataInicial.Text) + '-' + String(MaskEditDataFinal.Text) ;

              if ((Trim(MaskEditDtRecInicial.Text) <> '/  /') or (Trim(MaskEditDtRecFinal.Text) <> '/  /')) then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo �lt. Recebimento : ' + Trim(MaskEditDtRecInicial.Text) + '-' + String(MaskEditDtRecFinal.Text) ;

              if (RadioGroupExibirValores.ItemIndex = 0) then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Exibir Valores : Sim'
              else objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Exibir Valores : N�o' ;

              if (RadioGroupProdutoAtivo.ItemIndex = 0) then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Somente Produto Ativo : Sim'
              else objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Somente Produto Ativo: N�o' ;

              if (RadioGroupValorizacao.ItemIndex = 0) then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / M�todo de Valoriza��o : �ltimo Pre�o Custo';

              if (RadioGroupValorizacao.ItemIndex = 1) then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / M�todo de Valoriza��o : Pre�o M�dio' ;

              if (RadioGroupValorizacao.ItemIndex = 2) then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / M�todo de Valoriza��o : �ltimo Pre�o Recebimento';

              if (RadioGroupTipoSaida.ItemIndex = 0) then
                  objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo de Sa�da: Anal�tico'
              else objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Tipo de Sa�da: Sint�tico' ;

              //Oculta itens - modelo saida sintetico
              objRel.QRBand1.Enabled := True;
              if RadioGroupTipoSaida.ItemIndex = 1 then
                  objRel.QRBand3.Enabled := false;

          end;

          {--visualiza o relat�rio--}
          // saida em relat�rio
          if (RadioGroupSaida.ItemIndex = 0) then
              objRel.QuickRep1.PreviewModal;

          // sa�da em planilha
          if (RadioGroupSaida.ItemIndex = 1) then
          begin
              if (objRel.uspRelatorio.Eof) then
              begin
                  ShowMessage('Nenhum registro encontrado para o crit�rio selecionado.') ;
                  edtLocalEstoque.SetFocus;
                  abort ;
              end;

              objRel.uspRelatorio.First;
              iLinha := 1;

              ER2Excel1.Celula['A1'].Text := objRel.lblFiltro1.Caption;
              ER2Excel1.Celula['A1'].Mesclar('Q1');

              while not objRel.uspRelatorio.Eof do
              begin

                  if (cNmAgrupamento <> objRel.uspRelatoriocNmGrupoProduto.Value) then
                  begin
                      ER2Excel1.Celula['A' + intToStr(iLinha + 2)].Text := 'Grupo de Produto:';
                      ER2Excel1.Celula['A' + intToStr(iLinha + 2)].Negrito;
                      ER2Excel1.Celula['A' + intToStr(iLinha + 2)].Range('T' + intToStr(iLinha + 3));
                      ER2Excel1.Celula['A' + intToStr(iLinha + 2)].Background := clSilver;
                      cNmAgrupamento := objRel.uspRelatoriocNmGrupoProduto.Value;

                      ER2Excel1.Celula['B' + intToStr(iLinha + 2)].Text := cNmAgrupamento;
                      ER2Excel1.Celula['B' + intToStr(iLinha + 2)].Negrito;

                      iLinha         := iLinha + 3;

                      ER2Excel1.Celula['A' + intToStr(iLinha)].Text := 'ID';
                      ER2Excel1.Celula['A' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['B' + intToStr(iLinha)].Text := 'Descri��o';
                      ER2Excel1.Celula['B' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['C' + intToStr(iLinha)].Text := 'U.M.';
                      ER2Excel1.Celula['C' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['D' + intToStr(iLinha)].Text := 'Local de Estoque';
                      ER2Excel1.Celula['D' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['E' + intToStr(iLinha)].Text := 'Ult. Rec';
                      ER2Excel1.Celula['E' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['F' + intToStr(iLinha-1)].Text            := '*Estoque Inicial*';
                      ER2Excel1.Celula['F' + intToStr(iLinha-1)].HorizontalAlign := haCenter;
                      ER2Excel1.Celula['F' + intToStr(iLinha-1)].Negrito;
                      ER2Excel1.Celula['F' + intToStr(iLinha-1)].Mesclar('G' + intToStr(iLinha-1));

                      ER2Excel1.Celula['F' + intToStr(iLinha)].Text := 'Qtde.';
                      ER2Excel1.Celula['F' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['G' + intToStr(iLinha)].Text := 'Valor';
                      ER2Excel1.Celula['G' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['H' + intToStr(iLinha-1)].Text            := '*----Entradas----*';
                      ER2Excel1.Celula['H' + intToStr(iLinha-1)].HorizontalAlign := haCenter;
                      ER2Excel1.Celula['H' + intToStr(iLinha-1)].Negrito;
                      ER2Excel1.Celula['H' + intToStr(iLinha-1)].Mesclar('I' + intToStr(iLinha-1));

                      ER2Excel1.Celula['H' + intToStr(iLinha)].Text := 'Qtde.';
                      ER2Excel1.Celula['H' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['I' + intToStr(iLinha)].Text := 'Valor';
                      ER2Excel1.Celula['I' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['J' + intToStr(iLinha-1)].Text            := '*-----Sa�das-----*';
                      ER2Excel1.Celula['J' + intToStr(iLinha-1)].HorizontalAlign := haCenter;
                      ER2Excel1.Celula['J' + intToStr(iLinha-1)].Negrito;
                      ER2Excel1.Celula['J' + intToStr(iLinha-1)].Mesclar('K' + intToStr(iLinha-1));

                      ER2Excel1.Celula['J' + intToStr(iLinha)].Text := 'Qtde.';
                      ER2Excel1.Celula['J' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['K' + intToStr(iLinha)].Text := 'Valor';
                      ER2Excel1.Celula['K' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['L' + intToStr(iLinha-1)].Text            := '*-Estoque Final-*';
                      ER2Excel1.Celula['L' + intToStr(iLinha-1)].HorizontalAlign := haCenter;
                      ER2Excel1.Celula['L' + intToStr(iLinha-1)].Negrito;
                      ER2Excel1.Celula['L' + intToStr(iLinha-1)].Mesclar('M' + intToStr(iLinha-1));

                      ER2Excel1.Celula['L' + intToStr(iLinha)].Text := 'Qtde.';
                      ER2Excel1.Celula['L' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['M' + intToStr(iLinha)].Text := 'Valor';
                      ER2Excel1.Celula['M' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['N' + intToStr(iLinha-1)].Text            := '*---Oscila��o---*';
                      ER2Excel1.Celula['N' + intToStr(iLinha-1)].HorizontalAlign := haCenter;
                      ER2Excel1.Celula['N' + intToStr(iLinha-1)].Negrito;
                      ER2Excel1.Celula['N' + intToStr(iLinha-1)].Mesclar('O' + intToStr(iLinha-1));

                      ER2Excel1.Celula['N' + intToStr(iLinha)].Text := 'Qtde.';
                      ER2Excel1.Celula['N' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['O' + intToStr(iLinha)].Text := 'Valor';
                      ER2Excel1.Celula['O' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['P' + intToStr(iLinha)].Text := 'Custo Unit.';
                      ER2Excel1.Celula['P' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['Q' + intToStr(iLinha)].Text := 'Marca';
                      ER2Excel1.Celula['Q' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['R' + intToStr(iLinha)].Text := 'Departamento';
                      ER2Excel1.Celula['R' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['S' + intToStr(iLinha)].Text := 'Categoria';
                      ER2Excel1.Celula['S' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['T' + intToStr(iLinha)].Text := 'Subcategoria';
                      ER2Excel1.Celula['T' + intToStr(iLinha)].Negrito;

                      iLinhaAux := iLinha + 1;
                  end;

                  iLinha := iLinha + 1;

                  ER2Excel1.Celula['A' + intToStr(iLinha)].Text            := objRel.uspRelatorionCdProduto.Value;
                  ER2Excel1.Celula['A' + intToStr(iLinha)].HorizontalAlign := haRight;

                  ER2Excel1.Celula['B' + intToStr(iLinha)].Text            := objRel.uspRelatoriocNmProduto.Value;
                  ER2Excel1.Celula['C' + intToStr(iLinha)].Text            := objRel.uspRelatoriocUnidadeMedida.Value;

                  ER2Excel1.Celula['D' + intToStr(iLinha)].Text            := objRel.uspRelatoriocNmLocalEstoque.Value;

                  ER2Excel1.Celula['E' + intToStr(iLinha)].Text            := strToDate( dateToStr( objRel.uspRelatoriodDtUltReceb.Value ) ) ;
                  ER2Excel1.Celula['E' + intToStr(iLinha)].Mascara         := 'dd/mm/aaaa';

                  {-- estoque inicial --}
                  ER2Excel1.Celula['F' + intToStr(iLinha)].Text            := objRel.uspRelatorionSaldoAnt.Value;
                  ER2Excel1.Celula['F' + intToStr(iLinha)].Mascara         := '0';
                  ER2Excel1.Celula['F' + intToStr(iLinha)].HorizontalAlign := haRight;

                  ER2Excel1.Celula['G' + intToStr(iLinha)].Text            := objRel.uspRelatorionValorAnt.Value;
                  ER2Excel1.Celula['G' + intToStr(iLinha)].Mascara         := '0,00';
                  ER2Excel1.Celula['G' + intToStr(iLinha)].HorizontalAlign := haRight;

                  {-- entradas --}
                  ER2Excel1.Celula['H' + intToStr(iLinha)].Text            := objRel.uspRelatorionQtdeEnt.Value;
                  ER2Excel1.Celula['H' + intToStr(iLinha)].Mascara         := '0';
                  ER2Excel1.Celula['H' + intToStr(iLinha)].HorizontalAlign := haRight;

                  ER2Excel1.Celula['I' + intToStr(iLinha)].Text            := objRel.uspRelatorionValorEnt.Value;
                  ER2Excel1.Celula['I' + intToStr(iLinha)].Mascara         := '0,00';
                  ER2Excel1.Celula['I' + intToStr(iLinha)].HorizontalAlign := haRight;

                  {-- saidas --}
                  ER2Excel1.Celula['J' + intToStr(iLinha)].Text            := objRel.uspRelatorionQtdeSai.Value;
                  ER2Excel1.Celula['J' + intToStr(iLinha)].Mascara         := '0';
                  ER2Excel1.Celula['J' + intToStr(iLinha)].HorizontalAlign := haRight;

                  ER2Excel1.Celula['K' + intToStr(iLinha)].Text            := objRel.uspRelatorionValorSai.Value;
                  ER2Excel1.Celula['K' + intToStr(iLinha)].Mascara         := '0,00';
                  ER2Excel1.Celula['K' + intToStr(iLinha)].HorizontalAlign := haRight;

                  {-- estoque final --}
                  ER2Excel1.Celula['L' + intToStr(iLinha)].Text            := objRel.uspRelatorionSaldoPos.Value;
                  ER2Excel1.Celula['L' + intToStr(iLinha)].Mascara         := '0';
                  ER2Excel1.Celula['L' + intToStr(iLinha)].HorizontalAlign := haRight;

                  ER2Excel1.Celula['M' + intToStr(iLinha)].Text            := objRel.uspRelatorionValorPos.Value;
                  ER2Excel1.Celula['M' + intToStr(iLinha)].Mascara         := '0,00';
                  ER2Excel1.Celula['M' + intToStr(iLinha)].HorizontalAlign := haRight;

                  {-- oscila��o --}
                  ER2Excel1.Celula['N' + intToStr(iLinha)].Text            := objRel.uspRelatorionQtdeOsc.Value;
                  ER2Excel1.Celula['N' + intToStr(iLinha)].Mascara         := '0';
                  ER2Excel1.Celula['N' + intToStr(iLinha)].HorizontalAlign := haRight;

                  ER2Excel1.Celula['O' + intToStr(iLinha)].Text            := objRel.uspRelatorionValorOsc.Value;
                  ER2Excel1.Celula['O' + intToStr(iLinha)].Mascara         := '0,00';
                  ER2Excel1.Celula['O' + intToStr(iLinha)].HorizontalAlign := haRight;

                  if (RadioGroupValorizacao.ItemIndex = 0) then
                      ER2Excel1.Celula['P' + intToStr(iLinha)].Text := objRel.uspRelatorionValCusto.Value;

                  if (RadioGroupValorizacao.ItemIndex = 1) then
                      ER2Excel1.Celula['P' + intToStr(iLinha)].Text := objRel.uspRelatorionValMedio.Value;

                  if (RadioGroupValorizacao.ItemIndex = 2) then
                      ER2Excel1.Celula['P' + intToStr(iLinha)].Text := objRel.uspRelatorionValPrecoUltRec.Value;

                  ER2Excel1.Celula['P' + intToStr(iLinha)].Mascara         := '0,00';
                  ER2Excel1.Celula['P' + intToStr(iLinha)].HorizontalAlign := haRight;

                  {-- marca,departamento,categoria e subcategoria --}
                  ER2Excel1.Celula['Q' + intToStr(iLinha)].Text := objRel.uspRelatoriocNmMarcaProduto.Value;
                  ER2Excel1.Celula['R' + intToStr(iLinha)].Text := objRel.uspRelatoriocNmDepartamento.Value;
                  ER2Excel1.Celula['S' + intToStr(iLinha)].Text := objRel.uspRelatoriocNmCategoria.Value;
                  ER2Excel1.Celula['T' + intToStr(iLinha)].Text := objRel.uspRelatoriocNmSubCategoria.Value;

                  objRel.uspRelatorio.Next;

                  if ((cNmAgrupamento <> objRel.uspRelatoriocNmGrupoProduto.Value) or (objRel.uspRelatorio.Eof))then
                  begin
                      iLinha := iLinha + 1;

                      ER2Excel1.Celula['E' + intToStr(iLinha)].Text := 'Subtotal Grupo de Produtos:';
                      ER2Excel1.Celula['E' + intToStr(iLinha)].Negrito;
                      ER2Excel1.Celula['E' + intToStr(iLinha)].HorizontalAlign := haRight;

                      ER2Excel1.Celula['F' + intToStr(iLinha)].Border          := [EdgeTop];
                      ER2Excel1.Celula['F' + intToStr(iLinha)].Range('O' + intToStr(iLinha));

                      ER2Excel1.Celula['G' + intToStr(iLinha)].AutoSoma('G' + intToStr(iLinhaAux),'G' + intToStr(iLinha - 1));
                      ER2Excel1.Celula['G' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['G' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['I' + intToStr(iLinha)].AutoSoma('I' + intToStr(iLinhaAux),'I' + intToStr(iLinha - 1));
                      ER2Excel1.Celula['I' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['I' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['K' + intToStr(iLinha)].AutoSoma('K' + intToStr(iLinhaAux),'K' + intToStr(iLinha - 1));
                      ER2Excel1.Celula['K' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['K' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['M' + intToStr(iLinha)].AutoSoma('M' + intToStr(iLinhaAux),'M' + intToStr(iLinha - 1));
                      ER2Excel1.Celula['M' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['M' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['O' + intToStr(iLinha)].AutoSoma('O' + intToStr(iLinhaAux),'O' + intToStr(iLinha - 1));
                      ER2Excel1.Celula['O' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['O' + intToStr(iLinha)].Negrito;

                  end;
              end;

              { -- exporta planilha e limpa result do componente -- }
              ER2Excel1.ExportXLS;
              ER2Excel1.CleanupInstance;
          end ;

          //saida em tela
          if (RadioGroupSaida.ItemIndex = 2) then
          begin

              objForm := TfrmOscilacaoEstoque_View.Create(nil) ;

              objForm.uspRelatorio.Close ;
              objForm.uspRelatorio.Parameters.ParamByName('@nCdEmpresa').Value               := frmMenu.ConvInteiro(edtEmpresa.Text) ;
              objForm.uspRelatorio.Parameters.ParamByName('@nCdLocalEstoque').Value          := frmMenu.ConvInteiro(edtLocalEstoque.Text) ;
              objForm.uspRelatorio.Parameters.ParamByName('@nCdGrupoProduto').Value          := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
              objForm.uspRelatorio.Parameters.ParamByName('@nCdDepartamento').Value          := frmMenu.ConvInteiro(edtDepartamento.Text) ;

              objForm.uspRelatorio.Parameters.ParamByName('@nCdCategoria').Value             := frmMenu.ConvInteiro(edtCategoria.Text) ;
              objForm.uspRelatorio.Parameters.ParamByName('@nCdSubCategoria').Value          := frmMenu.ConvInteiro(edtSubCategoria.Text) ;
              objForm.uspRelatorio.Parameters.ParamByName('@nCdSegmento').Value              := frmMenu.ConvInteiro(edtSegmento.Text) ;

              objForm.uspRelatorio.Parameters.ParamByName('@nCdGrupoInventario').Value       := frmMenu.ConvInteiro(edtGrupoInventario.Text) ;

              objForm.uspRelatorio.Parameters.ParamByName('@nCdProduto').Value               := frmMenu.ConvInteiro(edtProduto.Text) ;

              objForm.uspRelatorio.Parameters.ParamByName('@dDtInicial').Value               := frmMenu.ConvData(MaskEditDataInicial.Text) ;
              objForm.uspRelatorio.Parameters.ParamByName('@dDtFinal').Value                 := frmMenu.ConvData(MaskEditDataFinal.Text) ;

              objForm.uspRelatorio.Parameters.ParamByName('@dDtRecebInicial').Value          := frmMenu.ConvData(MaskEditDtRecInicial.Text) ;
              objForm.uspRelatorio.Parameters.ParamByName('@dDtRecebFinal').Value            := frmMenu.ConvData(MaskEditDtRecFinal.Text) ;

              if (RadioGroupExibirValores.ItemIndex = 0) then
                  objForm.uspRelatorio.Parameters.ParamByName('@ExibirValores').Value        := 1
              else objForm.uspRelatorio.Parameters.ParamByName('@ExibirValores').Value       := 0 ;

              if (RadioGroupProdutoAtivo.ItemIndex = 0) then
                  objForm.uspRelatorio.Parameters.ParamByName('@SomenteProdutoAtivo').Value  := 1
              else objForm.uspRelatorio.Parameters.ParamByName('@SomenteProdutoAtivo').Value := 0 ;

              objRel.uspRelatorio.Parameters.ParamByName('@Valorizacao').Value  := RadioGroupValorizacao.ItemIndex ;

              objForm.uspRelatorio.Open  ;

              objForm.cxGrid1.Align := alClient ;
              showForm(objForm,true) ;

          end;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
      FreeAndNil(objForm);
  end;

end;

procedure TrptOscilacaoEstoque.edtProdutoBeforeLookup(Sender: TObject);
begin
  inherited;
    edtProduto.WhereAdicional.Text := 'NOT EXISTS(SELECT 1 FROM Produto Filho WHERE Filho.nCdProdutoPai = Produto.nCdProduto)'
end;

procedure TrptOscilacaoEstoque.ER2Excel1BeforeExport(Sender: TObject);
begin
  inherited;

  frmMenu.mensagemUsuario('Exportando planilha...');

  iCursor       := Screen.Cursor;
  Screen.Cursor := crSQLWait;
end;

procedure TrptOscilacaoEstoque.ER2Excel1AfterExport(Sender: TObject);
begin
  inherited;

  frmMenu.mensagemUsuario('');

  Screen.Cursor := iCursor;
end;

initialization
     RegisterClass(TrptOscilacaoEstoque) ;
end.
