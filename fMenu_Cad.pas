unit fMenu_Cad;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DBCtrls, StdCtrls, Mask, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, ImgList, cxPC, cxControls,
  DBGridEhGrouping, Menus, ToolCtrlsEh;

type
  TfrmMenu_Cad = class(TfrmCadastro_Padrao)
    qryMasternCdMenu: TIntegerField;
    qryMasternCdMenuPai: TIntegerField;
    qryMastercNmMenu: TStringField;
    qryMasternCdStatus: TIntegerField;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    qryMastercNmStatus: TStringField;
    qryMenuPai: TADOQuery;
    qryMenuPainCdMenu: TIntegerField;
    qryMenuPaicNmMenu: TStringField;
    qryMastercNmMenuPai: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    qryItemMenu: TADOQuery;
    dsItemMenu: TDataSource;
    qryItemMenunCdMenu: TIntegerField;
    qryItemMenunCdAplicacao: TIntegerField;
    qryItemMenunCdStatus: TIntegerField;
    qryAplicacao: TADOQuery;
    qryAplicacaonCdAplicacao: TIntegerField;
    qryAplicacaocNmAplicacao: TStringField;
    qryAplicacaonCdStatus: TIntegerField;
    qryAplicacaonCdModulo: TIntegerField;
    qryAplicacaocNmObjeto: TStringField;
    qryItemMenucNmAplicacao: TStringField;
    qryItemMenunCdItemMenu: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    PopupMenu1: TPopupMenu;
    GerarScript1: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure qryItemMenuBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure GerarScript1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
  private
    { Private declarations }
    procedure GerarScript(nCdTipoScript : integer);
  public
    { Public declarations }
  end;

var
  frmMenu_Cad: TfrmMenu_Cad;

implementation

uses fLookup_Padrao, fMenu;


{$R *.dfm}

procedure TfrmMenu_Cad.FormShow(Sender: TObject);
begin
  inherited;
  qryStatus.Close ;
  qryMenuPai.Close ;
  qryStatus.Open ;
  qryMenuPai.Open;

  DBEdit3.CharCase := ecNormal ;


end;

procedure TfrmMenu_Cad.qryItemMenuBeforePost(DataSet: TDataSet);
begin

  if (qryItemMenucNmAplicacao.Value = '') then
  begin
      ShowMessage('C�digo de aplica��o inv�lido.') ;
      abort ;
  end ;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  inherited;

  qryItemMenunCdMenu.Value := qryMaster.FieldList[0].Value ;

  if (qryItemMenu.State = dsInsert) then
      qryItemMenunCdItemMenu.Value := frmMenu.fnProximoCodigo('ITEMMENU') ;

end;

procedure TfrmMenu_Cad.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryItemMenu.Close ;
  qryItemMenu.Parameters.ParamByName('nCdMenu').Value := qryMasternCdMenu.Value ;
  qryItemMenu.Open ;

end;

procedure TfrmMenu_Cad.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryItemMenu.Close ;
end;

procedure TfrmMenu_Cad.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmMenu_Cad.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemMenu.State = dsBrowse) then
            qryItemMenu.Edit ;
            
        if (qryItemMenu.State = dsInsert) or (qryItemMenu.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(1);

            If (nPK > 0) then
            begin
                qryItemMenunCdAplicacao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmMenu_Cad.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(4,'Menu.nCdMenuPai IS NULL');

            If (nPK > 0) then
            begin
                qryMasternCdMenuPai.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmMenu_Cad.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'MENU' ;
  nCdTabelaSistema  := 2 ;
  nCdConsultaPadrao := 4 ;

end;

procedure TfrmMenu_Cad.ToolButton10Click(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      Abort;
      
  GerarScript(1);
end;

procedure TfrmMenu_Cad.GerarScript(nCdTipoScript: integer);
var
  objForm : TForm;
  cScript : TMemo;
begin
  inherited;

  if not (qryMaster.Active) then
      exit;

   objForm := TForm.Create(nil);

   objForm.Width    := 650;
   objForm.Height   := 350;
   objForm.Position := poDesktopCenter;
   objForm.Caption  := 'Script SQL';

   cScript            := TMemo.Create(nil);
   cScript.Parent     := objForm;
   cScript.Align      := alClient;
   cScript.ScrollBars := ssVertical;
   cScript.Font.Name  := 'Consolas';

   if (nCdTipoScript = 1) then {-- se for gera��o de menu --}
   begin

       cScript.Lines.Add('INSERT INTO Menu (nCdMenu');

       if (qryMasternCdMenuPai.Value > 0) then
           cScript.Lines.Add('                 ,nCdMenuPai');

       cScript.Lines.Add('                 ,cNmMenu');
       cScript.Lines.Add('                 ,nCdStatus)');
       cScript.Lines.Add('          VALUES (' + qryMasternCdMenu.AsString);

       if (qryMasternCdMenuPai.Value > 0) then
           cScript.Lines.Add('                 ,' + qryMasternCdMenuPai.AsString);

       cScript.Lines.Add('                 ,' + #39 + qryMastercNmMenu.AsString + #39);
       cScript.Lines.Add('                 ,' + qryMasternCdStatus.AsString + ')');
   end;

   if (nCdTipoScript = 2) then {-- se for cadastro de aplica��o do menu --}
   begin

       cScript.Lines.Add('INSERT INTO ItemMenu (nCdItemMenu');
       cScript.Lines.Add('                     ,nCdMenu');
       cScript.Lines.Add('                     ,nCdAplicacao');
       cScript.Lines.Add('                     ,nCdStatus)');
       cScript.Lines.Add('              VALUES (' + qryItemMenunCdItemMenu.AsString);
       cScript.Lines.Add('                     ,' + qryItemMenunCdMenu.AsString);
       cScript.Lines.Add('                     ,' + qryItemMenunCdAplicacao.AsString);
       cScript.Lines.Add('                     ,' + qryItemMenunCdStatus.AsString + ')');

   end;

   showForm(objForm,True);
end;

procedure TfrmMenu_Cad.GerarScript1Click(Sender: TObject);
begin
  inherited;

  GerarScript(2);
end;

procedure TfrmMenu_Cad.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      Abort;
end;

initialization
  RegisterClass(TfrmMenu_Cad) ;

end.
