unit fSetorBemPatrim;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  DBGridEhGrouping;

type
  TfrmSetorBemPatrim = class(TfrmCadastro_Padrao)
    qryMasternCdSetorBemPatrim: TIntegerField;
    qryMastercNmSetorBemPatrim: TStringField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label2: TLabel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryLocalBemPatrim: TADOQuery;
    dsLocalBemPatrim: TDataSource;
    qryLocalBemPatrimnCdLocalBemPatrim: TAutoIncField;
    qryLocalBemPatrimcNmLocalBemPatrim: TStringField;
    qryLocalBemPatrimnCdSetorBemPatrim: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryLocalBemPatrimBeforePost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSetorBemPatrim: TfrmSetorBemPatrim;

implementation

{$R *.dfm}

procedure TfrmSetorBemPatrim.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'SETORBEMPATRIM' ;
  nCdTabelaSistema  := 503 ;
  nCdConsultaPadrao := 1004 ;

end;

procedure TfrmSetorBemPatrim.btIncluirClick(Sender: TObject);
begin
  inherited;
    DBEdit2.SetFocus ;
end;

procedure TfrmSetorBemPatrim.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;
  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a Descri��o do Setor.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

end;
procedure TfrmSetorBemPatrim.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryLocalBemPatrim , qryMasternCdSetorBemPatrim.AsString) ;

end;

procedure TfrmSetorBemPatrim.qryLocalBemPatrimBeforePost(
  DataSet: TDataSet);
begin
  inherited;
  qryLocalBemPatrimcNmLocalBemPatrim.Value := UpperCase(qryLocalBemPatrimcNmLocalBemPatrim.Value);
  qryLocalBemPatrimnCdSetorBemPatrim.Value := qryMasternCdSetorBemPatrim.Value;
end;

procedure TfrmSetorBemPatrim.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  if (qryMaster.State = dsInsert) then
  qryMaster.Post ;
end;

procedure TfrmSetorBemPatrim.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryLocalBemPatrim.Close;
end;

procedure TfrmSetorBemPatrim.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryLocalBemPatrim.Close;
  PosicionaQuery(qryLocalBemPatrim , qryMasternCdSetorBemPatrim.AsString) ;

end;

initialization
    RegisterClass(TfrmSetorBemPatrim) ;

end.
