inherited frmGeraFaturamento: TfrmGeraFaturamento
  Left = 178
  Top = 130
  Width = 1109
  Height = 505
  Caption = 'Gera'#231#227'o de Faturamento'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 134
    Width = 1101
    Height = 344
  end
  inherited ToolBar1: TToolBar
    Width = 1101
    ButtonWidth = 92
    inherited ToolButton1: TToolButton
      Caption = 'Exibir Pedidos'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 92
    end
    inherited ToolButton2: TToolButton
      Left = 100
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 134
    Width = 1101
    Height = 344
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = Me
      DataController.DataSource = dsPedido
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GroupByBox = False
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1PedidoNmero: TcxGridDBColumn
        Caption = 'Pedido'
        DataBinding.FieldName = 'Pedido|N'#250'mero'
        Width = 85
      end
      object cxGrid1DBTableView1PedidoData: TcxGridDBColumn
        Caption = 'Data Emiss'#227'o'
        DataBinding.FieldName = 'Pedido|Data'
        Width = 132
      end
      object cxGrid1DBTableView1PedidoTerceiroEntrega: TcxGridDBColumn
        Caption = 'Terceiro de Entrega'
        DataBinding.FieldName = 'Pedido|Terceiro Entrega'
        Width = 333
      end
      object cxGrid1DBTableView1PedidoStatus: TcxGridDBColumn
        Caption = 'Status'
        DataBinding.FieldName = 'Pedido|Status'
        Width = 157
      end
      object cxGrid1DBTableView1PedidoSaldoFaturar: TcxGridDBColumn
        Caption = 'Saldo Pendente'
        DataBinding.FieldName = 'Pedido|Saldo Faturar'
        Width = 143
      end
      object cxGrid1DBTableView1PedidoTipo: TcxGridDBColumn
        Caption = 'Tipo de Pedido'
        DataBinding.FieldName = 'Pedido|Tipo'
        Width = 216
      end
      object cxGrid1DBTableView1PrevisodeEntregaIncio: TcxGridDBColumn
        Caption = 'Dt. Entrega Inicial'
        DataBinding.FieldName = 'Previs'#227'o de Entrega|In'#237'cio'
        Visible = False
      end
      object cxGrid1DBTableView1PrevisodeEntregaFim: TcxGridDBColumn
        DataBinding.FieldName = 'Previs'#227'o de Entrega|Fim'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1101
    Height = 105
    Align = alTop
    Caption = 'Filtros'
    TabOrder = 1
    object Label3: TLabel
      Tag = 1
      Left = 13
      Top = 76
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Faturamento'
    end
    object Label2: TLabel
      Tag = 1
      Left = 21
      Top = 48
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro Entrega'
    end
    object Label1: TLabel
      Tag = 1
      Left = 17
      Top = 24
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Grupo de Pedidos'
    end
    object MaskEdit2: TMaskEdit
      Left = 112
      Top = 68
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
      OnExit = MaskEdit2Exit
    end
    object MaskEdit3: TMaskEdit
      Left = 112
      Top = 40
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 1
      Text = '      '
      OnExit = MaskEdit3Exit
      OnKeyDown = MaskEdit3KeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 180
      Top = 40
      Width = 653
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = dsTerceiro
      TabOrder = 3
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 180
      Top = 16
      Width = 653
      Height = 21
      DataField = 'cNmGrupoPedido'
      DataSource = dsGrupoPedido
      TabOrder = 4
    end
    object MaskEdit1: TMaskEdit
      Left = 112
      Top = 16
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = MaskEdit1Exit
      OnKeyDown = MaskEdit1KeyDown
    end
  end
  inherited ImageList1: TImageList
    Top = 240
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A210000000000000000003E39340039343000332F
      2B002C29250027242100201D1B00E7E7E700333130000B0A0900070706000404
      0300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000046413B00857A7000C3B8
      AE007C7268007F756B0036322D00F2F2F1004C4A470095897D00BAAEA2007C72
      68007F756B000101010000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A210000000000000000004D47410083786F00CCC3
      BA00786F65007B71670034302D00FEFEFE002C2A270095897D00C2B8AD00786F
      65007C7268000605050000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000554E480083786F00CCC3
      BA007970660071685F00585550000000000049464500857A7000C2B8AD00786F
      65007B7167000D0C0B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000817B76009F928600CCC3
      BA00C0B4AA00A6988B00807D79000000000074726F0090847900C2B8AD00C0B4
      AA00A89B8E004947470000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A210000000000FCFCFC0060595200423D38005851
      4A003D383300332F2B0039373400D3D3D3005F5E5C001A181600252220001917
      15000F0E0D0012121200FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A210000000000FDFDFD009D918500B1A396007F75
      6B007C726800776D64006C635B002E2A2600564F480080766C007C726800776D
      640070675E0001010100FAFAFA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A210000000000FEFDFD00B8ACA100BAAEA2008277
      6D0082776D00AA917B00BAA79400B8A69000B09781009F8D7D00836D5B007163
      570095897D0023232200FCFCFC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A210000000000FDFCFC00DDDAD7009B8E82009D91
      8500867B7100564F4800504A440080766C006E665D00826C5800A6917D009484
      7400564F48008B8A8A00FEFEFE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A003163630031636300000000000000000000000000746B6200A497
      8A0095897D009F9286003E393400000000004C4640007E746A00857A70003E39
      340085817E00F5F5F500FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B5003163630000000000000000000000000000000000000000000000
      00009B918700C3B8AE00655D5500000000007C726800A89B8E00A69B90000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A79C9100BCB0A4009D91850000000000AEA093009D9185007B756E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF00000000FE00FFFF00000000
      FE00800300000000000080030000000000008003000000000000810300000000
      0000810300000000000000010000000000000001000000000000000100000000
      00000001000000000000C101000000000001F11F000000000003F11F00000000
      0077FFFF00000000007FFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object qryGrupoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM GrupoPedido WHERE cFlgFaturar = 1'
      'AND nCdGrupoPedido = :nPK')
    Left = 280
    Top = 208
    object qryGrupoPedidonCdGrupoPedido: TIntegerField
      FieldName = 'nCdGrupoPedido'
    end
    object qryGrupoPedidocNmGrupoPedido: TStringField
      FieldName = 'cNmGrupoPedido'
      Size = 50
    end
    object qryGrupoPedidocFlgFaturar: TIntegerField
      FieldName = 'cFlgFaturar'
    end
  end
  object dsGrupoPedido: TDataSource
    DataSet = qryGrupoPedido
    Left = 280
    Top = 240
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Terceiro,nPerTaxa1'
      '      ,Terceiro,nPerTaxa2'
      '  FROM Terceiro'
      
        '       INNER JOIN TerceiroTipoTerceiro TTT ON TTT.nCdTerceiro = ' +
        'Terceiro.nCdTerceiro'
      ' WHERE nCdTipoTerceiro IN (1,2)'
      '      AND Terceiro.nCdTerceiro = :nPK')
    Left = 312
    Top = 208
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceironPerTaxa1: TFloatField
      FieldName = 'nPerTaxa1'
    end
    object qryTerceironPerTaxa2: TFloatField
      FieldName = 'nPerTaxa2'
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 312
    Top = 240
  end
  object qryPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Size = -1
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro int'
      ''
      'SET @nCdTerceiro = :nCdTerceiro'
      ''
      'SELECT nCdPedido       "Pedido|N'#250'mero"'
      '      ,dDtPedido       "Pedido|Data"'
      '      ,cNmTipoPedido   "Pedido|Tipo"'
      '      ,dDtPrevEntIni   "Previs'#227'o de Entrega|In'#237'cio"'
      '      ,dDtPrevEntFim   "Previs'#227'o de Entrega|Fim"'
      '      ,cNmTabStatusPed "Pedido|Status"'
      '      ,nSaldoFat       "Pedido|Saldo Faturar"'
      '      ,cNmTerceiro     "Pedido|Terceiro Entrega"'
      '      ,Pedido.nCdTerceiro     "Pedido|Codigo Terceiro"'
      '      ,Terceiro.nPerTaxa1 "Terceiro|nPerTaxa1"'
      '      ,Terceiro.nPerTaxa2 "Terceiro|nPerTaxa2"'
      
        '      ,CASE WHEN (TabStatusPed.cFlgFaturar = 1) OR (TipoPedido.c' +
        'FlgDevolucVenda = 1 AND Pedido.nCdTabStatusPed = 2) THEN 1'
      '            ELSE 0'
      '       END cFlgFaturar'
      '  FROM Pedido'
      
        '       INNER JOIN Terceiro     ON Terceiro.nCdTerceiro         =' +
        ' Pedido.nCdTerceiro'
      
        '       INNER JOIN TipoPedido   ON TipoPedido.nCdTipoPedido     =' +
        ' Pedido.nCdTipoPedido'
      
        '       INNER JOIN GrupoPedido  ON GrupoPedido.nCdGrupoPedido   =' +
        ' TipoPedido.nCdGrupoPedido'
      
        '       INNER JOIN TabStatusPed ON TabStatusPed.nCdTabStatusPed =' +
        ' Pedido.nCdTabStatusPed'
      ' WHERE Pedido.nSaldoFat            > 0'
      '   AND GrupoPedido.nCdGrupoPedido = :nPK'
      '   AND Pedido.nCdEmpresa          = :nCdEmpresa'
      '   AND TipoPedido.cFlgOrcamento   = 0'
      
        '   AND ((@nCdTerceiro             = 0) OR (Pedido.nCdTerceiro = ' +
        '@nCdTerceiro))'
      '   AND ((ISNULL(Pedido.nCdLoja,0) = 0) OR (EXISTS(SELECT 1'
      
        '                                                    FROM Usuario' +
        'Loja'
      
        '                                                   WHERE Usuario' +
        'Loja.nCdUsuario = :nCdUsuario'
      
        '                                                     AND Usuario' +
        'Loja.nCdLoja    = Pedido.nCdLoja)))'
      '   AND EXISTS(SELECT 1'
      '                FROM ItemPedido'
      '               WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      '                 AND nQtdeLibFat > 0)'
      ' ORDER BY Pedido.nCdPedido'
      '')
    Left = 344
    Top = 208
    object qryPedidoPedidoNmero: TIntegerField
      FieldName = 'Pedido|N'#250'mero'
    end
    object qryPedidoPedidoData: TDateTimeField
      FieldName = 'Pedido|Data'
    end
    object qryPedidoPedidoTipo: TStringField
      FieldName = 'Pedido|Tipo'
      Size = 50
    end
    object qryPedidoPrevisodeEntregaIncio: TDateTimeField
      FieldName = 'Previs'#227'o de Entrega|In'#237'cio'
    end
    object qryPedidoPrevisodeEntregaFim: TDateTimeField
      FieldName = 'Previs'#227'o de Entrega|Fim'
    end
    object qryPedidoPedidoStatus: TStringField
      FieldName = 'Pedido|Status'
      Size = 50
    end
    object qryPedidoPedidoSaldoFaturar: TBCDField
      FieldName = 'Pedido|Saldo Faturar'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryPedidoPedidoTerceiroEntrega: TStringField
      FieldName = 'Pedido|Terceiro Entrega'
      Size = 50
    end
    object qryPedidocFlgFaturar: TIntegerField
      FieldName = 'cFlgFaturar'
      ReadOnly = True
    end
    object qryPedidoPedidoCodigoTerceiro: TIntegerField
      FieldName = 'Pedido|Codigo Terceiro'
    end
    object qryPedidoTerceironPerTaxa1: TFloatField
      FieldName = 'Terceiro|nPerTaxa1'
    end
    object qryPedidoTerceironPerTaxa2: TFloatField
      FieldName = 'Terceiro|nPerTaxa2'
    end
  end
  object dsPedido: TDataSource
    DataSet = qryPedido
    Left = 344
    Top = 240
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 376
    Top = 208
  end
end
