unit rFaturamentoAnual_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptFaturamentoAnual_view = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    QRHTMLFilter1: TQRHTMLFilter;
    QRCSVFilter1: TQRCSVFilter;
    QRTextFilter1: TQRTextFilter;
    QRCompositeReport1: TQRCompositeReport;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabel1: TQRLabel;
    lblAno1: TQRLabel;
    lblAno2: TQRLabel;
    lblmes4: TQRLabel;
    lblAno3: TQRLabel;
    QRLabel18: TQRLabel;
    lblmes2: TQRLabel;
    SummaryBand1: TQRBand;
    QRShape2: TQRShape;
    QRExpr13: TQRExpr;
    usp_RelatoriocMes: TStringField;
    usp_RelatorionValAno1: TFloatField;
    usp_RelatorionPercVar1: TFloatField;
    usp_RelatorionValAno2: TFloatField;
    usp_RelatorionPercVar2: TFloatField;
    usp_RelatorionValAno3: TFloatField;
    QRShape3: TQRShape;
    QRExpr7: TQRExpr;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    lblFiltro1: TQRLabel;
    QRExpr5: TQRExpr;
    QRExpr4: TQRExpr;
    QRShape6: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    procedure QRExpr2Print(sender: TObject; var Value: String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFaturamentoAnual_view: TrptFaturamentoAnual_view;

implementation

{$R *.dfm}

procedure TrptFaturamentoAnual_view.QRExpr2Print(sender: TObject;
  var Value: String);
begin
    //QRExpr5.Caption := FormatFloat('#,##0.00%',(StrToFloat(QRExpr2.Caption) / StrToFloat(QRExpr1.Caption)) *100 - 100) ;
end;

end.
