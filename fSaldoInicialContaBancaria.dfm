inherited frmSaldoInicialContaBancaria: TfrmSaldoInicialContaBancaria
  Width = 1280
  Height = 770
  Caption = 'Saldo Inicial Conta Banc'#225'ria'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1264
    Height = 709
  end
  object Label1: TLabel [1]
    Left = 33
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 28
    Top = 70
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 39
    Top = 94
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Banco'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 30
    Top = 118
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ag'#234'ncia'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 156
    Top = 118
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Conta'
    FocusControl = DBEdit5
  end
  object Label10: TLabel [6]
    Left = 39
    Top = 142
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Titular'
    FocusControl = DBEdit14
  end
  object Label6: TLabel [7]
    Left = 9
    Top = 166
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo Inicial'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [8]
    Left = 263
    Top = 166
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Saldo'
    FocusControl = DBEdit7
  end
  inherited ToolBar2: TToolBar
    Width = 1264
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [10]
    Tag = 1
    Left = 80
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdContaBancaria'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [11]
    Tag = 1
    Left = 80
    Top = 64
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [12]
    Tag = 1
    Left = 80
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdBanco'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [13]
    Tag = 1
    Left = 80
    Top = 112
    Width = 52
    Height = 19
    DataField = 'cAgencia'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [14]
    Tag = 1
    Left = 240
    Top = 112
    Width = 177
    Height = 19
    DataField = 'nCdConta'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit9: TDBEdit [15]
    Tag = 1
    Left = 152
    Top = 64
    Width = 57
    Height = 19
    DataField = 'cSiglaEmpresa'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit10: TDBEdit [16]
    Tag = 1
    Left = 216
    Top = 64
    Width = 201
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit11: TDBEdit [17]
    Tag = 1
    Left = 152
    Top = 88
    Width = 265
    Height = 19
    DataField = 'cNmBanco'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit14: TDBEdit [18]
    Tag = 1
    Left = 80
    Top = 136
    Width = 473
    Height = 19
    DataField = 'cNmTitular'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit6: TDBEdit [19]
    Left = 80
    Top = 160
    Width = 169
    Height = 19
    DataField = 'nSaldoInicial'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit7: TDBEdit [20]
    Left = 328
    Top = 160
    Width = 89
    Height = 19
    DataField = 'dDtSaldoInicial'
    DataSource = dsMaster
    TabOrder = 11
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ContaBancaria'
      'WHERE nCdContaBancaria = :nPK')
    Top = 216
    object qryMasternCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryMasternCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryMasternCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMastercFlgFluxo: TIntegerField
      FieldName = 'cFlgFluxo'
    end
    object qryMasteriUltimoCheque: TIntegerField
      FieldName = 'iUltimoCheque'
    end
    object qryMasteriUltimoBordero: TIntegerField
      FieldName = 'iUltimoBordero'
    end
    object qryMastercSiglaEmpresa: TStringField
      FieldKind = fkLookup
      FieldName = 'cSiglaEmpresa'
      LookupDataSet = qryEmpresa
      LookupKeyFields = 'nCdEmpresa'
      LookupResultField = 'cSigla'
      KeyFields = 'nCdEmpresa'
      LookupCache = True
      Size = 5
      Lookup = True
    end
    object qryMastercNmEmpresa: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmEmpresa'
      LookupDataSet = qryEmpresa
      LookupKeyFields = 'nCdEmpresa'
      LookupResultField = 'cNmEmpresa'
      KeyFields = 'nCdEmpresa'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMastercNmBanco: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmBanco'
      LookupDataSet = qryBanco
      LookupKeyFields = 'nCdBanco'
      LookupResultField = 'cNmBanco'
      KeyFields = 'nCdBanco'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMastercNmCC: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmCC'
      LookupKeyFields = 'nCdCC'
      LookupResultField = 'cNmCC'
      KeyFields = 'nCdCC'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMasternValLimiteCredito: TBCDField
      FieldName = 'nValLimiteCredito'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgDeposito: TIntegerField
      FieldName = 'cFlgDeposito'
    end
    object qryMastercFlgEmiteCheque: TIntegerField
      FieldName = 'cFlgEmiteCheque'
    end
    object qryMastercAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryMastercFlgEmiteBoleto: TIntegerField
      FieldName = 'cFlgEmiteBoleto'
    end
    object qryMastercFlgCaixa: TIntegerField
      FieldName = 'cFlgCaixa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasternCdUsuarioOperador: TIntegerField
      FieldName = 'nCdUsuarioOperador'
    end
    object qryMasternSaldoConta: TBCDField
      FieldName = 'nSaldoConta'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgCofre: TIntegerField
      FieldName = 'cFlgCofre'
    end
    object qryMasterdDtUltConciliacao: TDateTimeField
      FieldName = 'dDtUltConciliacao'
    end
    object qryMastercNmTitular: TStringField
      FieldName = 'cNmTitular'
      Size = 50
    end
    object qryMastercFlgProvTit: TIntegerField
      FieldName = 'cFlgProvTit'
    end
    object qryMasternSaldoInicial: TBCDField
      FieldName = 'nSaldoInicial'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtSaldoInicial: TDateTimeField
      FieldName = 'dDtSaldoInicial'
      EditMask = '!99/99/9999;1;_'
    end
  end
  inherited dsMaster: TDataSource
    Top = 216
  end
  inherited qryID: TADOQuery
    Top = 216
  end
  inherited usp_ProximoID: TADOStoredProc
    Top = 272
  end
  inherited qryStat: TADOQuery
    Top = 288
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Top = 328
  end
  inherited ImageList1: TImageList
    Top = 312
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM Empresa'
      'WHERE nCdStatus = 1')
    Left = 608
    Top = 408
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresanCdTerceiroEmp: TIntegerField
      FieldName = 'nCdTerceiroEmp'
    end
    object qryEmpresanCdTerceiroMatriz: TIntegerField
      FieldName = 'nCdTerceiroMatriz'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresanCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryBanco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM Banco')
    Left = 656
    Top = 408
    object qryBanconCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryBancocNmBanco: TStringField
      FieldName = 'cNmBanco'
      Size = 50
    end
  end
  object SP_SALDO_INICIAL_CONTA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_SALDO_INICIAL_CONTA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdContaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 440
    Top = 392
  end
end
