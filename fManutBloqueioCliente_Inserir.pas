unit fManutBloqueioCliente_Inserir;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, DBCtrls, ADODB;

type
  TfrmManutBloqueioCliente_Inserir = class(TfrmProcesso_Padrao)
    Label5: TLabel;
    edtnCdTabTipoRestricaoVenda: TMaskEdit;
    Edit1: TEdit;
    Label1: TLabel;
    qryTabTipoRestricaoVenda: TADOQuery;
    qryTabTipoRestricaoVendanCdTabTipoRestricaoVenda: TIntegerField;
    qryTabTipoRestricaoVendacNmTabTipoRestricaoVenda: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    SP_INSERI_BLOQUEIO_TERCEIRO: TADOStoredProc;
    qryVerificaDup: TADOQuery;
    procedure edtnCdTabTipoRestricaoVendaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtnCdTabTipoRestricaoVendaExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTerceiro : integer ;
  end;

var
  frmManutBloqueioCliente_Inserir: TfrmManutBloqueioCliente_Inserir;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmManutBloqueioCliente_Inserir.edtnCdTabTipoRestricaoVendaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(189);

        If (nPK > 0) then
            edtnCdTabTipoRestricaoVenda.Text := intToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmManutBloqueioCliente_Inserir.edtnCdTabTipoRestricaoVendaExit(
  Sender: TObject);
begin
  inherited;

  qryTabTipoRestricaoVenda.Close ;
  PosicionaQuery(qryTabTipoRestricaoVenda, edtnCdTabTipoRestricaoVenda.Text) ;
  
end;

procedure TfrmManutBloqueioCliente_Inserir.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de bloqueio.') ;
      edtnCdTabTipoRestricaoVenda.Setfocus ;
      exit ;
  end ;

  if (Edit1.Text = '') then
  begin
      MensagemAlerta('Informe o motivo/observa��o do bloqueio.') ;
      Edit1.Setfocus ;
      exit ;
  end ;

  if (MessageDlg('Confirma a inclus�o deste bloqueio ?'+#13#13+'N�o ser� poss�vel excluir este bloqueio, somente o desbloqueio ser� permitido.',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
      exit ;

  qryVerificaDup.Close;
  qryVerificaDup.Parameters.ParamByName('nCdTerceiro').Value              := nCdTerceiro;
  qryVerificaDup.Parameters.ParamByName('nCdTabTipoRestricaoVenda').Value := qryTabTipoRestricaoVendanCdTabTipoRestricaoVenda.Value;
  qryVerificaDup.Open ;

  if not qryVerificaDup.eof then
  begin
      MensagemErro('J� existe um bloqueio deste tipo pendente de desbloqueio neste cliente.') ;
      close ;
      exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try

      SP_INSERI_BLOQUEIO_TERCEIRO.Close ;
      SP_INSERI_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdTerceiro').Value              := nCdTerceiro ;
      SP_INSERI_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdTabTipoRestricaoVenda').Value := qryTabTipoRestricaoVendanCdTabTipoRestricaoVenda.Value;
      SP_INSERI_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdUsuarioBloqueio').Value       := frmMenu.nCdUsuarioLogado ;
      SP_INSERI_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdLojaBloqueio').Value          := frmMenu.nCdLojaAtiva ;
      SP_INSERI_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@cObs').Value                     := edit1.Text ;
      SP_INSERI_BLOQUEIO_TERCEIRO.ExecProc;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryTabTipoRestricaoVenda.Close ;

  ShowMessage('Bloqueio adicionado com sucesso!') ;

  close ;


end;

procedure TfrmManutBloqueioCliente_Inserir.FormShow(Sender: TObject);
begin
  inherited;

  qryTabTipoRestricaoVenda.Close ;
  edit1.Text                       := '' ;
  edtnCdTabTipoRestricaoVenda.Text := '' ;
  edtnCdTabTipoRestricaoVenda.Setfocus ;

end;

procedure TfrmManutBloqueioCliente_Inserir.Edit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = vk_return) then
      ToolButton1.Click;

end;

end.
