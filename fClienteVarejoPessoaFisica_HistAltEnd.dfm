inherited frmClienteVarejoPessoaFisica_HistAltEnd: TfrmClienteVarejoPessoaFisica_HistAltEnd
  Left = 55
  Top = 168
  Width = 1065
  Caption = 'Hist'#243'rico de Altera'#231#227'o de Endere'#231'o'
  OldCreateOrder = True
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1049
  end
  inherited ToolBar1: TToolBar
    Width = 1049
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1049
    Height = 435
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsEndereco
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnGrouping = False
      OptionsCustomize.ColumnMoving = False
      OptionsCustomize.ColumnSorting = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLineColor = clSilver
      OptionsView.GridLines = glNone
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.FonteSomenteLeitura
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1cEndereco: TcxGridDBColumn
        Caption = 'Endere'#231'o'
        DataBinding.FieldName = 'cEndereco'
        Width = 231
      end
      object cxGrid1DBTableView1cComplemento: TcxGridDBColumn
        Caption = 'Complemento'
        DataBinding.FieldName = 'cComplemento'
        Width = 107
      end
      object cxGrid1DBTableView1cBairro: TcxGridDBColumn
        Caption = 'Bairro'
        DataBinding.FieldName = 'cBairro'
        Width = 164
      end
      object cxGrid1DBTableView1cCidade: TcxGridDBColumn
        Caption = 'Cidade'
        DataBinding.FieldName = 'cCidade'
        Width = 181
      end
      object cxGrid1DBTableView1cTelefone: TcxGridDBColumn
        Caption = 'Telefone'
        DataBinding.FieldName = 'cTelefone'
      end
      object cxGrid1DBTableView1cFax: TcxGridDBColumn
        Caption = 'Telefone 2'
        DataBinding.FieldName = 'cFax'
      end
      object cxGrid1DBTableView1cCEP: TcxGridDBColumn
        Caption = 'CEP'
        DataBinding.FieldName = 'cCEP'
        Width = 88
      end
      object cxGrid1DBTableView1cNmTabTipoResidencia: TcxGridDBColumn
        Caption = 'Tipo Resid'#234'ncia'
        DataBinding.FieldName = 'cNmTabTipoResidencia'
        Width = 166
      end
      object cxGrid1DBTableView1cTempoResid: TcxGridDBColumn
        Caption = 'Temp Resid.'
        DataBinding.FieldName = 'cTempoResid'
        Width = 144
      end
      object cxGrid1DBTableView1nValAluguel: TcxGridDBColumn
        Caption = 'Valor Aluguel'
        DataBinding.FieldName = 'nValAluguel'
        Width = 96
      end
      object cxGrid1DBTableView1cNmTabTipoComprov: TcxGridDBColumn
        Caption = 'Comprovante'
        DataBinding.FieldName = 'cNmTabTipoComprov'
        Width = 141
      end
      object cxGrid1DBTableView1cOBSComprov: TcxGridDBColumn
        Caption = 'Observa'#231#227'o'
        DataBinding.FieldName = 'cOBSComprov'
      end
      object cxGrid1DBTableView1dDtCadastro: TcxGridDBColumn
        Caption = 'Data Cadastro'
        DataBinding.FieldName = 'dDtCadastro'
      end
      object cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn
        Caption = 'Usu'#225'rio Cadastro'
        DataBinding.FieldName = 'cNmUsuario'
        Width = 176
      end
      object cxGrid1DBTableView1cNmLoja: TcxGridDBColumn
        Caption = 'Loja Cadastro'
        DataBinding.FieldName = 'cNmLoja'
        Width = 190
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryEndereco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT Endereco.cEndereco + '#39' ,'#39' + dbo.fn_zeroEsquerda(Endereco.' +
        'iNumero,6) cEndereco'
      '      ,cComplemento'
      '      ,cBairro'
      '      ,cCidade + '#39'/'#39' + cUF cCidade'
      '      ,cTelefone'
      '      ,cFax'
      '      ,cCEP'
      '      ,cNmTabTipoResidencia'
      
        '      ,dbo.fn_ZeroEsquerda(iTempoResidAno,2) + '#39'a '#39' + dbo.fn_Zer' +
        'oEsquerda(iTempoResidMes,2) + '#39'm'#39' cTempoResid'
      '      ,nValAluguel'
      '      ,cNmTabTipoComprov'
      '      ,cOBSComprov'
      '      ,cNmUsuario'
      '      ,dDtCadastro'
      '      ,cNmLoja'
      '  FROM Endereco'
      
        '       LEFT JOIN TabTipoResidencia ON TabTipoResidencia.nCdTabTi' +
        'poResidencia = Endereco.nCdTabTipoResidencia'
      
        '       LEFT JOIN TabTipoComprov    ON TabTipoComprov.nCdTabTipoC' +
        'omprov       = Endereco.nCdTabTipoComprovEnd'
      
        '       LEFT JOIN Usuario           ON Usuario.nCdUsuario        ' +
        '             = Endereco.nCdUsuarioCadastro'
      
        '       LEFT JOIN Loja              ON Loja.nCdLoja              ' +
        '             = Endereco.nCdLojaCadastro'
      ' WHERE Endereco.nCdTerceiro = :nPK'
      ' ORDER BY dDtCadastro DESC')
    Left = 192
    Top = 72
    object qryEnderecocEndereco: TStringField
      FieldName = 'cEndereco'
      ReadOnly = True
      Size = 67
    end
    object qryEnderecocComplemento: TStringField
      FieldName = 'cComplemento'
      Size = 35
    end
    object qryEnderecocBairro: TStringField
      FieldName = 'cBairro'
      Size = 35
    end
    object qryEnderecocCidade: TStringField
      FieldName = 'cCidade'
      ReadOnly = True
      Size = 38
    end
    object qryEnderecocTelefone: TStringField
      FieldName = 'cTelefone'
    end
    object qryEnderecocFax: TStringField
      FieldName = 'cFax'
    end
    object qryEnderecocCEP: TStringField
      FieldName = 'cCEP'
      FixedChar = True
      Size = 8
    end
    object qryEnderecocNmTabTipoResidencia: TStringField
      FieldName = 'cNmTabTipoResidencia'
      Size = 50
    end
    object qryEnderecocTempoResid: TStringField
      FieldName = 'cTempoResid'
      ReadOnly = True
      Size = 49
    end
    object qryEndereconValAluguel: TBCDField
      FieldName = 'nValAluguel'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryEnderecocNmTabTipoComprov: TStringField
      FieldName = 'cNmTabTipoComprov'
      Size = 50
    end
    object qryEnderecocOBSComprov: TStringField
      FieldName = 'cOBSComprov'
      Size = 30
    end
    object qryEnderecocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryEnderecodDtCadastro: TDateTimeField
      FieldName = 'dDtCadastro'
    end
    object qryEnderecocNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsEndereco: TDataSource
    DataSet = qryEndereco
    Left = 232
    Top = 72
  end
end
