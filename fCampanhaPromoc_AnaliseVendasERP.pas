unit fCampanhaPromoc_AnaliseVendasERP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ADODB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmCampanhaPromoc_AnaliseVendasERP = class(TfrmProcesso_Padrao)
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryVendas: TADOQuery;
    dsVendas: TDataSource;
    qryVendasnCdItemPedido: TIntegerField;
    qryVendasnCdPedido: TIntegerField;
    qryVendasdDtPedido: TDateTimeField;
    qryVendasnCdProduto: TIntegerField;
    qryVendascNmItem: TStringField;
    qryVendasnQtdeExpRec: TBCDField;
    qryVendasnValUnitario: TBCDField;
    qryVendasnValDesconto: TBCDField;
    qryVendasnValAcrescimo: TBCDField;
    qryVendasnValCustoUnit: TBCDField;
    qryVendasnValTotalItem: TBCDField;
    qryVendasnValCMV: TBCDField;
    qryVendasnMargemBruta: TBCDField;
    qryVendascNmDepartamento: TStringField;
    qryVendascNmCategoria: TStringField;
    qryVendascNmSubCategoria: TStringField;
    qryVendascNmSegmento: TStringField;
    qryVendascNmLinha: TStringField;
    qryVendascNmMarca: TStringField;
    qryVendascNmTerceiro: TStringField;
    cxGridDBTableView1nCdItemPedido: TcxGridDBColumn;
    cxGridDBTableView1nCdPedido: TcxGridDBColumn;
    cxGridDBTableView1dDtPedido: TcxGridDBColumn;
    cxGridDBTableView1nCdProduto: TcxGridDBColumn;
    cxGridDBTableView1cNmItem: TcxGridDBColumn;
    cxGridDBTableView1nQtdeExpRec: TcxGridDBColumn;
    cxGridDBTableView1nValUnitario: TcxGridDBColumn;
    cxGridDBTableView1nValTotalItem: TcxGridDBColumn;
    cxGridDBTableView1nValCMV: TcxGridDBColumn;
    cxGridDBTableView1nMargemBruta: TcxGridDBColumn;
    cxGridDBTableView1cNmDepartamento: TcxGridDBColumn;
    cxGridDBTableView1cNmCategoria: TcxGridDBColumn;
    cxGridDBTableView1cNmSubCategoria: TcxGridDBColumn;
    cxGridDBTableView1cNmSegmento: TcxGridDBColumn;
    cxGridDBTableView1cNmLinha: TcxGridDBColumn;
    cxGridDBTableView1cNmMarca: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCampanhaPromoc_AnaliseVendasERP: TfrmCampanhaPromoc_AnaliseVendasERP;

implementation

{$R *.dfm}

end.
