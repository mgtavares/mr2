unit fMetaDescontoMensal_Vendedor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, cxPC, cxControls, DB,
  ADODB;

type
  TfrmMetaDescontoMensal_vendedor = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryMaster: TADOQuery;
    qryMasternCdMetaVendedorDesconto: TIntegerField;
    qryMasternCdMetaDescontoMes: TIntegerField;
    qryMasternCdTerceiroColab: TIntegerField;
    dsMaster: TDataSource;
    qryMastercNmTerceiro: TStringField;
    qryMasternValCotaDesconto: TBCDField;
    qryTotalSaldo: TADOQuery;
    qryTotalSaldonValMetaDesconto: TBCDField;
    qryTotalSaldonDescontoUtilizado: TBCDField;
    qryTotalSaldonSaldo: TBCDField;
    procedure FormShow(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure prCalcFooter();
    procedure prValidaCalcMeta();
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    cCdMetaDescontoMes : String;
    iAno : Integer;
    iMes : Integer;
  end;

var
  frmMetaDescontoMensal_vendedor : TfrmMetaDescontoMensal_vendedor;

implementation
uses
  fMenu, fAcompanhaMetaDescVendedor;
{$R *.dfm}

procedure TfrmMetaDescontoMensal_vendedor.prCalcFooter();
begin
    PosicionaQuery(qryTotalSaldo, cCdMetaDescontoMes);
    DBGridEh1.Columns[2].Footers[0].Value := FormatCurr('#,##0.00', qryTotalSaldonValMetaDesconto.Value);
    DBGridEh1.Columns[2].Footers[1].Value := FormatCurr('#,##0.00', qryTotalSaldonDescontoUtilizado.Value);
    DBGridEh1.Columns[2].Footers[2].Value := FormatCurr('#,##0.00', qryTotalSaldonSaldo.Value);
end;

procedure TfrmMetaDescontoMensal_vendedor.prValidaCalcMeta();
var
  nValAntigo : Double;
  nDesconto  : Double;
begin
  inherited;
  PosicionaQuery(qryTotalSaldo, cCdMetaDescontoMes);

  nValAntigo := qryMasternValCotaDesconto.OldValue;
  nDesconto  := qryTotalSaldonDescontoUtilizado.Value;
  nDesconto  := nDesconto - nValAntigo;

  if ((nDesconto + qryMasternValCotaDesconto.Value) > qryTotalSaldonValMetaDesconto.Value) then
  begin
      MensagemAlerta('Voc� ultrapassou o valor permitido, informe um valor de at� R$ ' + FormatFloat('#,##0.00', (qryTotalSaldonValMetaDesconto.Value - qryTotalSaldonDescontoUtilizado.Value)) + '.');
      Abort;
  end;
end;

procedure TfrmMetaDescontoMensal_vendedor.FormShow(Sender: TObject);
begin
  inherited;

  prCalcFooter;
end;

procedure TfrmMetaDescontoMensal_vendedor.qryMasterBeforePost(
  DataSet: TDataSet);
var
  nValUtilizado   : Double;
  nValCotaDescOld : Double;
  objAcompMeta    : TfrmAcompanhaMetaDescVendedor;
begin
  inherited;

  PosicionaQuery(qryTotalSaldo, cCdMetaDescontoMes);

  if (qryMasternValCotaDesconto.Value > qryTotalSaldonValMetaDesconto.Value) then
  begin
      MensagemAlerta('O valor informado � maior que a meta vigente.' + #13 + 'Informe um valor de at� R$ ' + FormatCurr('#,##0.00', qryTotalSaldonValMetaDesconto.Value));
      Abort;
  end;

  if (qryMasternValCotaDesconto.Value < 0) then
  begin
      MensagemAlerta('O valor da meta n�o pode ser negativo.');
      Abort;
  end;

  objAcompMeta := TfrmAcompanhaMetaDescVendedor.Create(Nil);
  objAcompMeta.qryMaster.Close;
  objAcompMeta.qryMaster.Parameters.ParamByName('iMes').Value        := iMes;
  objAcompMeta.qryMaster.Parameters.ParamByName('iAno').Value        := iAno;
  objAcompMeta.qryMaster.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiroColab.Value;
  objAcompMeta.qryMaster.Open;

  { -- se valor for atualizado para menor, verifica se o valor da meta n�o � inferior ao desconto j� utilizado -- }
  if (not objAcompMeta.qryMaster.IsEmpty) then
  begin
      nValUtilizado   := 0;
      nValCotaDescOld := qryMasternValCotaDesconto.OldValue;

      if (qryMasternValCotaDesconto.Value < nValCotaDescOld) then
      begin
          while not(objAcompMeta.qryMaster.Eof) do
          begin
              nValUtilizado := nValUtilizado + objAcompMeta.qryMasternValDescontoUtil.Value;
              objAcompMeta.qryMaster.Next;
          end;

          if (qryMasternValCotaDesconto.Value < nValUtilizado) then
          begin
              MensagemAlerta('O valor informado � inferior ao desconto j� utilizado.' + #13 + 'Valor m�nimo para meta R$ ' + FormatCurr('#,##0.00', nValUtilizado));
              Abort;
          end;

          objAcompMeta.qryMaster.First;

          while not(objAcompMeta.qryMaster.Eof) do
          begin
              objAcompMeta.qryMaster.Edit;
              objAcompMeta.qryMasternValDescontoAnt.Value  := objAcompMeta.qryMasternValDescontoAnt.Value - (Abs(qryMasternValCotaDesconto.Value - nValCotaDescOld));
              objAcompMeta.qryMasternValDescontoPost.Value := objAcompMeta.qryMasternValDescontoPost.Value - (Abs(qryMasternValCotaDesconto.Value - nValCotaDescOld));
              objAcompMeta.qryMaster.Post;
              objAcompMeta.qryMaster.Next;
          end;
      end
      else
      begin
          objAcompMeta.qryMaster.First;

          while not(objAcompMeta.qryMaster.Eof) do
          begin
              objAcompMeta.qryMaster.Edit;
              objAcompMeta.qryMasternValDescontoAnt.Value  := objAcompMeta.qryMasternValDescontoAnt.Value + (Abs(qryMasternValCotaDesconto.Value - nValCotaDescOld));
              objAcompMeta.qryMasternValDescontoPost.Value := objAcompMeta.qryMasternValDescontoPost.Value + (Abs(qryMasternValCotaDesconto.Value - nValCotaDescOld));
              objAcompMeta.qryMaster.Post;
              objAcompMeta.qryMaster.Next;
          end;
      end;
  end;

  prValidaCalcMeta;
end;

procedure TfrmMetaDescontoMensal_vendedor.qryMasterAfterPost(
  DataSet: TDataSet);
begin
  inherited;
  prCalcFooter;
end;

procedure TfrmMetaDescontoMensal_vendedor.ToolButton2Click(
  Sender: TObject);
begin
  prValidaCalcMeta;
  inherited;
end;

end.
