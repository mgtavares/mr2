unit rAniversariantesMes;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DB, Mask, DBCtrls, ADODB, ER2Excel;

type
  TrptAniversariantesMes = class(TfrmRelatorio_Padrao)
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    dsLoja: TDataSource;
    DBEdit2: TDBEdit;
    GroupMes: TRadioGroup;
    GroupSexo: TRadioGroup;
    rgModeloImp: TRadioGroup;
    ER2Excel: TER2Excel;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit1Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptAniversariantesMes: TrptAniversariantesMes;

implementation

uses fLookup_Padrao,fMenu,rAniversariantesMes_view;

{$R *.dfm}

procedure TrptAniversariantesMes.FormCreate(Sender: TObject);
var
    iDia,iMes,iAno : word;
begin
  inherited;
  DecodeDate(Now,iAno,iMes,iDia);
  GroupMes.ItemIndex := (iMes - 1);

  if (frmMenu.nCdLojaAtiva > 0) then
      PosicionaQuery(qryLoja, IntToStr(frmMenu.nCdLojaAtiva));

end;

procedure TrptAniversariantesMes.DBEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(147);

            If (nPK > 0) then
            begin
                DBEdit1.Text := IntToStr(nPK);
                PosicionaQuery(qryLoja, DBEdit1.Text);
            end ;

    end ;

  end ;

end;

procedure TrptAniversariantesMes.DBEdit1Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryLoja, DBEdit1.Text);
end;

procedure TrptAniversariantesMes.ToolButton1Click(Sender: TObject);
var
    iMes,iSexo : integer;
    iLinha     : Integer;
    cSexo      : String;
    objRel     : TrptAniversariantesMes_view;
begin
  inherited;

  {-- verifica se a loja est� selecionada --}

  if ((Trim(DBEdit1.Text) = '') or (DBEdit2.Text = '')) then
  begin
      MensagemAlerta('Selecione uma loja');
      Abort;
  end;

  {-- prepara os filtros para procedure --}

  iMes  := (GroupMes.ItemIndex + 1);
  iSexo := GroupSexo.ItemIndex;

  {-- verifica sexo selecionado --}
  case (GroupSexo.ItemIndex) of
      0 : cSexo := 'TODOS';
      1 : cSexo := 'MASCULINO';
      2 : cSexo := 'FEMININO';
  end;

  {-- prepara e executa procedure --}

  objRel := TrptAniversariantesMes_view.Create(nil);

  try
      try

          objRel.SPREL_ANIVERSARIANTES_MES.Close;
          objRel.SPREL_ANIVERSARIANTES_MES.Parameters.ParamByName('@iMes').Value            := iMes;
          objRel.SPREL_ANIVERSARIANTES_MES.Parameters.ParamByName('@nCdTabTipoSexo').Value  := iSexo;
          objRel.SPREL_ANIVERSARIANTES_MES.Parameters.ParamByName('@nCdLojaCadastro').Value := DBEdit1.Text;

          objRel.cmdPreparaTemp.Execute;
          objRel.SPREL_ANIVERSARIANTES_MES.ExecProc;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblTitle.Caption   := 'RELAT�RIO DE ANIVERSARIANTES DO M�S DE ' + UpperCase(GroupMes.Items.Strings[iMes-1]) + '    LOJA CADASTRO: ' + DBEdit2.Text + '    SEXO: ' + cSexo;

          objRel.qryResultado.Close;
          objRel.qryResultado.Open;

          {-- exibe relat�rio --}
          case (rgModeloImp.ItemIndex) of
              0 : objRel.QuickRep1.PreviewModal;
              1 : begin
                      frmMenu.mensagemUsuario('Exportando Planilha...');

                      ER2Excel.Celula['A1'].Mesclar('W1');
                      ER2Excel.Celula['A2'].Mesclar('W2');
                      ER2Excel.Celula['A3'].Mesclar('W3');
                      ER2Excel.Celula['A4'].Mesclar('D4');
                      ER2Excel.Celula['E4'].Mesclar('G4');
                      ER2Excel.Celula['H4'].Mesclar('J4');
                      ER2Excel.Celula['K4'].Mesclar('W4');
                      ER2Excel.Celula['A5'].Mesclar('W5');
                      ER2Excel.Celula['B6'].Mesclar('F6');
                      ER2Excel.Celula['G6'].Mesclar('H6');
                      ER2Excel.Celula['J6'].Mesclar('K6');
                      ER2Excel.Celula['L6'].Mesclar('M6');
                      ER2Excel.Celula['N6'].Mesclar('O6');
                      ER2Excel.Celula['P6'].Mesclar('Q6');
                      ER2Excel.Celula['R6'].Mesclar('S6');
                      ER2Excel.Celula['T6'].Mesclar('W6');

                      ER2Excel.Celula['A1'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A2'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A3'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A4'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['E4'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['H4'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['K4'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A5'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['A6'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['B6'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['G6'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['I6'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['J6'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['L6'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['N6'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['P6'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['R6'].Background := RGB(221, 221, 221);
                      ER2Excel.Celula['T6'].Background := RGB(221, 221, 221);

                      { -- inseri informa��es do cabe�alho -- }
                      ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                      ER2Excel.Celula['A2'].Text := 'Rel. Aniversariantes do M�s.';
                      ER2Excel.Celula['A4'].Text := 'Loja Cadastro: ' + DBEdit2.Text;
                      ER2Excel.Celula['E4'].Text := 'Sexo: ' + cSexo;
                      ER2Excel.Celula['H4'].Text := 'M�s: ' + GroupMes.Items.Strings[iMes-1];
                      ER2Excel.Celula['A6'].Text := 'C�d.';
                      ER2Excel.Celula['B6'].Text := 'Cliente';
                      ER2Excel.Celula['G6'].Text := 'Dt. Nascimento';
                      ER2Excel.Celula['I6'].Text := 'Idade';
                      ER2Excel.Celula['J6'].Text := 'Dt. Cadastro';
                      ER2Excel.Celula['L6'].Text := 'Telefone 1';
                      ER2Excel.Celula['N6'].Text := 'Telefone 2';
                      ER2Excel.Celula['P6'].Text := 'Celular';
                      ER2Excel.Celula['R6'].Text := 'Comercial';
                      ER2Excel.Celula['T6'].Text := 'E-mail';

                      iLinha := 7;

                      objRel.qryResultado.First;

                      while (not objRel.qryResultado.Eof) do
                      begin
                          ER2Excel.Celula['A' + IntToStr(iLinha)].Text := objRel.qryResultadonCdTerceiro.Value;
                          ER2Excel.Celula['B' + IntToStr(iLinha)].Text := objRel.qryResultadocNmTerceiro.Value;
                          ER2Excel.Celula['G' + IntToStr(iLinha)].Text := objRel.qryResultadodDtNasc.Value;
                          ER2Excel.Celula['I' + IntToStr(iLinha)].Text := objRel.qryResultadonIdade.Value;
                          ER2Excel.Celula['J' + IntToStr(iLinha)].Text := objRel.qryResultadodDtCadastro.Value;
                          ER2Excel.Celula['L' + IntToStr(iLinha)].Text := objRel.qryResultadocTelefone1.Value;
                          ER2Excel.Celula['N' + IntToStr(iLinha)].Text := objRel.qryResultadocTelefone2.Value;
                          ER2Excel.Celula['P' + IntToStr(iLinha)].Text := objRel.qryResultadocTelefoneMovel.Value;
                          ER2Excel.Celula['R' + IntToStr(iLinha)].Text := objRel.qryResultadocTelefoneEmpTrab.Value;
                          ER2Excel.Celula['T' + IntToStr(iLinha)].Text := objRel.qryResultadocEmail.Value;

                          objRel.qryResultado.Next;

                          Inc(iLinha);
                      end;

                      { -- exporta planilha e limpa result do componente -- }
                      ER2Excel.ExportXLS;
                      ER2Excel.CleanupInstance;

                      frmMenu.mensagemUsuario('');
                  end;
          end;
          
      except
          MensagemErro('Erro na cria��o do relat�rio. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel) ;
  end;
end;

initialization
    RegisterClass(TrptAniversariantesMes) ;

end.
