unit fCancelaDepositoBancario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DBCtrls, StdCtrls, Mask, ADODB, DB, ImgList,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmCancelaDepositoBancario = class(TfrmProcesso_Padrao)
    qryDepositoBancario: TADOQuery;
    SP_CANCELA_DEPOSITO_BANCARIO: TADOStoredProc;
    MaskEdit1: TMaskEdit;
    Label7: TLabel;
    Label1: TLabel;
    DBEdit7: TDBEdit;
    DBEdit1: TDBEdit;
    qryDepositoBancarionCdDepositoBancario: TIntegerField;
    qryDepositoBancarionCdEmpresa: TIntegerField;
    qryDepositoBancarionCdLoja: TIntegerField;
    qryDepositoBancariodDtDeposito: TDateTimeField;
    qryDepositoBancarionCdContaBancaria: TIntegerField;
    qryDepositoBancarionValDinheiro: TBCDField;
    qryDepositoBancarionValChequeNumerario: TBCDField;
    qryDepositoBancarionValChequePreDatado: TBCDField;
    qryDepositoBancarionValTotalDeposito: TBCDField;
    qryDepositoBancarioiQtdeCheques: TIntegerField;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    DataSource1: TDataSource;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    Label9: TLabel;
    DBEdit8: TDBEdit;
    Label10: TLabel;
    DBEdit9: TDBEdit;
    qryDepositoBancariocNmEmpresa: TStringField;
    qryDepositoBancariocNmLoja: TStringField;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label11: TLabel;
    DBEdit12: TDBEdit;
    qryDepositoBancariocNmConta: TStringField;
    qryDepositoBancariodDtCancel: TDateTimeField;
    qryDepositoBancarionCdUsuarioCancel: TIntegerField;
    Label2: TLabel;
    DBEdit13: TDBEdit;
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCancelaDepositoBancario: TfrmCancelaDepositoBancario;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmCancelaDepositoBancario.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(166,'dDtProcesso IS NOT NULL');

            If (nPK > 0) then
            begin
                MaskEdit1.Text := IntToStr(nPK);
                PosicionaQuery(qryDepositoBancario,MaskEdit1.Text);
            end ;

    end;
    VK_RETURN: begin
        PosicionaQuery(qryDepositoBancario,MaskEdit1.Text);
    end;

  end ;

end;

procedure TfrmCancelaDepositoBancario.MaskEdit1Exit(Sender: TObject);
begin
  inherited;
  qryDepositoBancario.Close;
  PosicionaQuery(qryDepositoBancario,MaskEdit1.Text);
end;

procedure TfrmCancelaDepositoBancario.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryDepositoBancario,MaskEdit1.Text);

  if(Trim(MaskEdit1.Text)='') then
  begin
      MensagemAlerta('Antes de Processar Selecione o Dep�sito Banc�rio.');
      MaskEdit1.SetFocus;
      abort;
  end;

  case MessageDlg('Confirma o cancelamento do dep�sito ? ' +#13#13+ 'Ap�s cancelado esse processo n�o poder� ser desfeito.',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  if(qryDepositoBancariodDtCancel.AsString <> '') then
  begin
      MensagemAlerta('O Dep�sito Banc�rio ' + qryDepositoBancarionCdDepositoBancario.AsString + ' j� foi cancelado.');
      MaskEdit1.SetFocus;
      qryDepositoBancario.Close;
      MaskEdit1.Text := '';
      abort;
  end;

  frmMenu.Connection.BeginTrans;
  
  try
      SP_CANCELA_DEPOSITO_BANCARIO.Close;
      SP_CANCELA_DEPOSITO_BANCARIO.Parameters.ParamByName('@nCdDepositoBancario').Value := qryDepositoBancarionCdDepositoBancario.Value ;
      SP_CANCELA_DEPOSITO_BANCARIO.Parameters.ParamByName('@nCdUsuario').Value          := frmMenu.nCdUsuarioLogado;
      SP_CANCELA_DEPOSITO_BANCARIO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Dep�sito Cancelado com Sucesso.');

  qryDepositoBancario.Close;

  MaskEdit1.Text := '';
  MaskEdit1.SetFocus;


end;

initialization
    RegisterClass(TfrmCancelaDepositoBancario) ;

end.
