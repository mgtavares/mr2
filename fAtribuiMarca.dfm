inherited frmAtribuiMarca: TfrmAtribuiMarca
  Left = 51
  Top = 185
  VertScrollBar.Range = 0
  BorderStyle = bsDialog
  Caption = 'Atribui'#231#227'o de Marca'
  ClientHeight = 428
  ClientWidth = 1164
  OldCreateOrder = True
  Position = poDesktopCenter
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1164
    Height = 399
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 40
    Width = 59
    Height = 13
    Caption = 'C'#243'd. Marca:'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 88
    Top = 40
    Width = 50
    Height = 13
    Caption = 'Descri'#231#227'o:'
    FocusControl = DBEdit2
  end
  inherited ToolBar1: TToolBar
    Width = 1164
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBGridEh1: TDBGridEh [4]
    Left = 8
    Top = 88
    Width = 1145
    Height = 329
    AllowedOperations = [alopInsertEh, alopDeleteEh, alopAppendEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsTempAtribuiMarca
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    PopupMenu = PopupMenu1
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnEditButtonClick = DBGridEh1EditButtonClick
    OnKeyDown = DBGridEh1KeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'Departamento|C'#243'd.'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmDepartamento'
        Footers = <>
        ReadOnly = True
        Tag = 1
      end
      item
        EditButtons = <>
        FieldName = 'Categoria|C'#243'd.'
        Footers = <>
        OnUpdateData = DBGridEh1Columns2UpdateData
      end
      item
        EditButtons = <>
        FieldName = 'cNmCategoria'
        Footers = <>
        ReadOnly = True
        Tag = 1
      end
      item
        EditButtons = <>
        FieldName = 'Subcategoria|C'#243'd.'
        Footers = <>
        OnUpdateData = DBGridEh1Columns4UpdateData
      end
      item
        EditButtons = <>
        FieldName = 'cNmSubCategoria'
        Footers = <>
        ReadOnly = True
        Tag = 1
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 8
    Top = 56
    Width = 75
    Height = 21
    DataField = 'nCdMarca'
    DataSource = dsMarca
    TabOrder = 2
  end
  object DBEdit2: TDBEdit [6]
    Tag = 1
    Left = 88
    Top = 56
    Width = 377
    Height = 21
    DataField = 'cNmMarca'
    DataSource = dsMarca
    TabOrder = 3
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000EFFFFF00000000000000840000008400000000000000
      0000EFFFFF0000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      84000000840000000000EFFFFF00EFFFFF00000000000000840000000000EFFF
      FF00EFFFFF0000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000084002100
      C6002100C6000000000000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000000000002100C6000000
      84002100C6002100C6002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000000084002100C6002100
      C6002100C6000000FF002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      0000000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000002100C6000000FF002100
      C6000000FF000000FF0000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF00000084000000000000000000000000000000FF002100C6000000
      FF000000FF0000000000EFFFFF00EFFFFF002100C6000000FF0000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000000000002100C6000000FF000000
      FF000000FF0000000000EFFFFF00000000000000FF002100C6000000FF000000
      0000EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000000000002100C6000000FF000000
      FF000000FF0000000000000000000000FF000000FF000000FF002100C6000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF000000FF002100
      C6002100C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF0000008400000000000000000000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF002100C6000000
      FF002100C6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF002100
      C600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFF0030000
      FE00C003E001000000008001C001000000008001800100000000800180010000
      0000800100010000000080010001000000008001000100000000800100010000
      00008001000100000000800180030000000180018003000000038001C0070000
      0077C003E00F0000007FFFFFF83F000000000000000000000000000000000000
      000000000000}
  end
  object qrySubcategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSubCategoria'
      '      ,cNmSubCategoria AS "Subcategoria|Nome"'
      '  FROM SubCategoria'
      ' WHERE nCdSubCategoria = :nPK'
      '   AND nCdCategoria    = :nCdCategoria')
    Left = 704
    Top = 32
    object qrySubcategorianCdSubCategoria: TIntegerField
      FieldName = 'nCdSubCategoria'
    end
    object qrySubcategoriaSubcategoriaNome: TStringField
      FieldName = 'Subcategoria|Nome'
      Size = 50
    end
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarca'
      '      ,cNmMarca'
      '  FROM Marca'
      ' WHERE nCdMarca = :nPK')
    Left = 144
    Top = 32
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 176
    Top = 32
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempAtribuiMarca'#39') IS NULL)'#13#10'BEGIN'#13#10'    ' +
      'CREATE TABLE #TempAtribuiMarca(nCdTempAtribuiMarca  int Primary ' +
      'key identity(1,1)'#13#10'                                  ,nCdDeparta' +
      'mento      int'#13#10'                                  ,nCdCategoria ' +
      '        int'#13#10'                                  ,nCdSubCategoria ' +
      '     int'#13#10'                                  ,nCdMarca           ' +
      '  int'#13#10'                                  ,nCdMarcaSubCategoria i' +
      'nt'#13#10'                                  ,cFlgAcao             char' +
      '(1))'#13#10'END '#13#10
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 488
    Top = 32
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempAtribuiMarca'#39') IS NULL)'
      'BEGIN'
      
        '    CREATE TABLE #TempAtribuiMarca(nCdTempAtribuiMarca  int Prim' +
        'ary key identity(1,1)'
      '                                  ,nCdDepartamento      int'
      '                                  ,nCdCategoria         int'
      '                                  ,nCdSubCategoria      int'
      '                                  ,nCdMarca             int'
      '                                  ,nCdMarcaSubCategoria int'
      '                                  ,cFlgAcao             char(1))'
      'END '
      ''
      'TRUNCATE TABLE #TempAtribuiMarca')
    Left = 520
    Top = 32
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMarca'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdMarca int'
      ''
      'SET @nCdMarca = :nCdMarca'
      ''
      'INSERT INTO #TempAtribuiMarca(nCdDepartamento'
      '                             ,nCdCategoria         '
      '                             ,nCdSubCategoria'
      '                             ,nCdMarca'
      '                             ,nCdMarcaSubCategoria)'
      '                       SELECT Cat.nCdDepartamento'
      '                             ,SubCat.nCdCategoria'
      '                             ,MarcaSubCategoria.nCdSubCategoria'
      '                             ,MarcaSubCategoria.nCdMarca'
      
        '                             ,MarcaSubCategoria.nCdMarcaSubCateg' +
        'oria'
      '                         FROM MarcaSubCategoria'
      
        '                              INNER JOIN SubCategoria SubCat ON ' +
        'SubCat.nCdSubCategoria = MarcaSubCategoria.nCdSubCategoria'
      
        '                              INNER JOIN Categoria Cat       ON ' +
        'Cat.nCdCategoria       = SubCat.nCdCategoria'
      
        '                              INNER JOIN Departamento Dep    ON ' +
        'Dep.nCdDepartamento    = Cat.nCdDepartamento'
      '                        WHERE nCdMarca = @nCdMarca')
    Left = 552
    Top = 32
  end
  object qryTempAtribuiMarca: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTempAtribuiMarcaBeforePost
    BeforeDelete = qryTempAtribuiMarcaBeforeDelete
    AfterScroll = qryTempAtribuiMarcaAfterScroll
    OnCalcFields = qryTempAtribuiMarcaCalcFields
    Parameters = <>
    SQL.Strings = (
      '   SELECT nCdDepartamento AS "Departamento|C'#243'd."'
      '         ,nCdCategoria    AS "Categoria|C'#243'd."'
      '         ,nCdSubCategoria AS "Subcategoria|C'#243'd."'
      '         ,nCdMarca'
      '         ,nCdMarcaSubCategoria'
      '         ,cFlgAcao'
      '     FROM #TempAtribuiMarca'
      '    WHERE cFlgAcao IS NULL'
      '       OR cFlgAcao = '#39'I'#39
      ' ORDER BY nCdDepartamento'
      '         ,nCdCategoria'
      '         ,nCdSubCategoria')
    Left = 584
    Top = 32
    object qryTempAtribuiMarcaDepartamentoCd: TIntegerField
      FieldName = 'Departamento|C'#243'd.'
    end
    object qryTempAtribuiMarcaCategoriaCd: TIntegerField
      FieldName = 'Categoria|C'#243'd.'
    end
    object qryTempAtribuiMarcaSubcategoriaCd: TIntegerField
      FieldName = 'Subcategoria|C'#243'd.'
    end
    object qryTempAtribuiMarcanCdMarcaSubCategoria: TIntegerField
      FieldName = 'nCdMarcaSubCategoria'
    end
    object qryTempAtribuiMarcacFlgAcao: TStringField
      FieldName = 'cFlgAcao'
      FixedChar = True
      Size = 1
    end
    object qryTempAtribuiMarcacNmDepartamento: TStringField
      DisplayLabel = 'Departamento|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmDepartamento'
      Size = 50
      Calculated = True
    end
    object qryTempAtribuiMarcacNmCategoria: TStringField
      DisplayLabel = 'Categoria|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCategoria'
      Size = 50
      Calculated = True
    end
    object qryTempAtribuiMarcacNmSubCategoria: TStringField
      DisplayLabel = 'Subcategoria|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmSubCategoria'
      Size = 50
      Calculated = True
    end
    object qryTempAtribuiMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
  end
  object dsTempAtribuiMarca: TDataSource
    DataSet = qryTempAtribuiMarca
    Left = 584
    Top = 64
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento'
      '      ,cNmDepartamento'
      '  FROM Departamento'
      ' WHERE nCdDepartamento = :nPK')
    Left = 640
    Top = 32
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdDepartamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCategoria'
      '      ,cNmCategoria'
      '  FROM Categoria'
      ' WHERE nCdCategoria    = :nPK'
      '   AND nCdDepartamento = :nCdDepartamento')
    Left = 672
    Top = 32
    object qryCategorianCdCategoria: TIntegerField
      FieldName = 'nCdCategoria'
    end
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 1120
    Top = 96
    object btnExcluirRegistro: TMenuItem
      Caption = 'Excluir Registro'
      OnClick = btnExcluirRegistroClick
    end
  end
  object SP_ATRIBUI_MARCA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ATRIBUI_MARCA;1'
    Parameters = <>
    Left = 760
    Top = 32
  end
  object qryVerificaRegistro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdTempAtribuiMarca'
      '      ,nCdDepartamento'
      '      ,nCdCategoria'
      '      ,nCdSubCategoria'
      '      ,nCdMarca'
      '      ,nCdMarcaSubCategoria'
      '      ,cFlgAcao'
      '  FROM #TempAtribuiMarca'
      ' WHERE cFlgAcao = '#39'I'#39
      '    OR cFlgAcao = '#39'D'#39)
    Left = 208
    Top = 32
  end
end
