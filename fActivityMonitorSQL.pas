unit fActivityMonitorSQL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, StdCtrls;

type
  TfrmActivityMonitorSQL = class(TfrmProcesso_Padrao)
    Connection: TADOConnection;
    qrySysProcesses: TADOQuery;
    dsSysProcesses: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    btEncerrarProc: TToolButton;
    btVisualizarProc: TToolButton;
    ToolButton4: TToolButton;
    ToolButton7: TToolButton;
    qryKill: TADOQuery;
    qrySysProcessesnCdIDProcesso: TSmallintField;
    qrySysProcessescNmLogin: TWideStringField;
    qrySysProcessesdDtLogin: TDateTimeField;
    qrySysProcessescHostName: TWideStringField;
    qrySysProcessesdDtInicioExec: TDateTimeField;
    qrySysProcessescStatusExec: TWideStringField;
    qrySysProcessesiTransacoes: TSmallintField;
    qrySysProcessesnCdBlockID: TSmallintField;
    qrySysProcessescFlgHeadBlocker: TIntegerField;
    qrySysProcessescComandoSQL: TMemoField;
    cxGrid1DBTableView1nCdIDProcesso: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLogin: TcxGridDBColumn;
    cxGrid1DBTableView1dDtLogin: TcxGridDBColumn;
    cxGrid1DBTableView1cHostName: TcxGridDBColumn;
    cxGrid1DBTableView1dDtInicioExec: TcxGridDBColumn;
    cxGrid1DBTableView1cStatusExec: TcxGridDBColumn;
    cxGrid1DBTableView1iTransacoes: TcxGridDBColumn;
    cxGrid1DBTableView1nCdBlockID: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgHeadBlocker: TcxGridDBColumn;
    cxGrid1DBTableView1cComandoSQL: TcxGridDBColumn;
    Panel1: TPanel;
    imgDivergencia: TImage;
    Label6: TLabel;
    qryRegistraProcesso: TADOQuery;
    qryRegistraProcessonCdIDProcesso: TIntegerField;
    qryRegistraProcessocNmDatabase: TStringField;
    qryRegistraProcessocNmLogin: TStringField;
    qryRegistraProcessodDtLogin: TDateTimeField;
    qryRegistraProcessocNmComputador: TStringField;
    qryRegistraProcessodDtInicioExec: TDateTimeField;
    qryRegistraProcessocStatusExec: TStringField;
    qryRegistraProcessocFlgTransacaoAtiva: TIntegerField;
    qryRegistraProcessodDtCadastro: TDateTimeField;
    qryRegistraProcessocComandoSQL: TMemoField;
    function fnConectarDatabase : Boolean;
    procedure btVisualizarProcClick(Sender: TObject);
    procedure btEncerrarProcClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmActivityMonitorSQL: TfrmActivityMonitorSQL;

implementation

uses
  fMenu;

{$R *.dfm}

function TfrmActivityMonitorSQL.fnConectarDatabase : Boolean;
var
  bStatus : Boolean;
begin
  bStatus := False;

  try
      try
          frmMenu.mensagemUsuario('Estabelecendo conex�o...');
          
          Connection.Close;
          Connection.ConnectionString := 'provider=MSDASQL.1;Network Library=DBMSSOCN;driver={SQL Server};Server=' + frmMenu.cIPServidor + ';uid=sa;pwd=admsoft101580;database=' + frmMenu.cInstancia;
          Connection.LoginPrompt      := False;
          Screen.Cursor               := crSQLWait;

          Connection.Open();
          bStatus := True;
      except on E : Exception do
          MensagemErro(E.Message);
      end;
  finally
      frmMenu.mensagemUsuario('');
      Screen.Cursor := crDefault;
      Result        := bStatus;
  end;
end;

procedure TfrmActivityMonitorSQL.btVisualizarProcClick(Sender: TObject);
begin
  inherited;

  if (not fnConectarDatabase) then
      Exit;

  qrySysProcesses.Close;
  qrySysProcesses.Parameters.ParamByName('cHostLocal').Value := frmMenu.cNomeComputador;
  qrySysProcesses.Open;
end;

procedure TfrmActivityMonitorSQL.btEncerrarProcClick(Sender: TObject);
begin
  inherited;

  if (qrySysProcesses.IsEmpty) then
      Exit;

  case MessageDlg('O processo ID ' + qrySysProcessesnCdIDProcesso.AsString + ' ser� encerrado e o HOST ser� desconectado, '
                 +'este procedimento pode originar instabilidade no sistema ou afetar a integridade de seus dados.'
                 +#13 + 'Deseja realmente encerrar este processo ?', mtConfirmation, [mbYes,mbNo] ,0) of
      mrNo : Abort;
  end;

  try
      try
          frmMenu.mensagemUsuario('Registrando processo...');

          qryRegistraProcesso.Close;
          qryRegistraProcesso.Open;
          qryRegistraProcesso.Insert;
          qryRegistraProcessonCdIDProcesso.Value      := qrySysProcessesnCdIDProcesso.Value;
          qryRegistraProcessocNmDatabase.Value        := frmMenu.cInstancia;
          qryRegistraProcessocNmLogin.Value           := qrySysProcessescNmLogin.Value;
          qryRegistraProcessodDtLogin.Value           := qrySysProcessesdDtLogin.Value;
          qryRegistraProcessocNmComputador.Value      := qrySysProcessescHostName.Value;
          qryRegistraProcessodDtInicioExec.Value      := qrySysProcessesdDtInicioExec.Value;
          qryRegistraProcessocStatusExec.Value        := qrySysProcessescStatusExec.Value;
          qryRegistraProcessocFlgTransacaoAtiva.Value := qrySysProcessesiTransacoes.Value;
          qryRegistraProcessodDtCadastro.Value        := Now();
          qryRegistraProcessocComandoSQL.Value        := qrySysProcessescComandoSQL.Value;
          qryRegistraProcesso.Post;

          frmMenu.mensagemUsuario('Encerrando processo ' + qrySysProcessesnCdIDProcesso.AsString + '...');

          qryKill.Close;
          qryKill.SQL.Clear;
          qryKill.SQL.Add('KILL ' + qrySysProcessesnCdIDProcesso.AsString);
          qryKill.ExecSQL;

          ShowMessage('Processo encerrado com sucesso.'); 
      except on E : Exception do
          MensagemErro(E.Message);
      end;
  finally
      btVisualizarProc.Click;
      frmMenu.mensagemUsuario('');
  end;
end;

initialization
  RegisterClass(TfrmActivityMonitorSQL);

end.
