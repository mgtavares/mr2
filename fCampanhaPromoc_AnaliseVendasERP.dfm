inherited frmCampanhaPromoc_AnaliseVendasERP: TfrmCampanhaPromoc_AnaliseVendasERP
  Left = -8
  Top = -8
  Width = 1456
  Height = 886
  Caption = 'Campanha Promocional - An'#225'lise de Vendas - ERP'
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1440
    Height = 821
  end
  inherited ToolBar1: TToolBar
    Width = 1440
    ButtonWidth = 93
    inherited ToolButton1: TToolButton
      Caption = 'Atualizar Tela'
    end
    inherited ToolButton3: TToolButton
      Left = 93
    end
    inherited ToolButton2: TToolButton
      Left = 101
    end
  end
  object cxGrid2: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1440
    Height = 821
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGridDBTableView1: TcxGridDBTableView
      DataController.DataSource = dsVendas
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'nValTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'nSaldoTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skAverage
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          Column = cxGridDBTableView1nValTotalItem
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          Column = cxGridDBTableView1nValCMV
        end
        item
          Format = '#,##0.00'
          Kind = skAverage
          Position = spFooter
          Column = cxGridDBTableView1nMargemBruta
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nSaldoTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skAverage
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValTotalItem
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValCMV
        end
        item
          Format = '#,##0.00'
          Kind = skAverage
          Column = cxGridDBTableView1nMargemBruta
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      OptionsView.GroupFooters = gfAlwaysVisible
      object cxGridDBTableView1nCdItemPedido: TcxGridDBColumn
        DataBinding.FieldName = 'nCdItemPedido'
        Visible = False
      end
      object cxGridDBTableView1nCdPedido: TcxGridDBColumn
        Caption = 'Pedido'
        DataBinding.FieldName = 'nCdPedido'
      end
      object cxGridDBTableView1dDtPedido: TcxGridDBColumn
        Caption = 'Data Faturam.'
        DataBinding.FieldName = 'dDtPedido'
      end
      object cxGridDBTableView1nCdProduto: TcxGridDBColumn
        Caption = 'Produto'
        DataBinding.FieldName = 'nCdProduto'
      end
      object cxGridDBTableView1cNmItem: TcxGridDBColumn
        Caption = 'Descri'#231#227'o Produto'
        DataBinding.FieldName = 'cNmItem'
        Width = 403
      end
      object cxGridDBTableView1nQtdeExpRec: TcxGridDBColumn
        Caption = 'Quant.'
        DataBinding.FieldName = 'nQtdeExpRec'
        Width = 68
      end
      object cxGridDBTableView1nValUnitario: TcxGridDBColumn
        Caption = 'Val. Unit'#225'rio'
        DataBinding.FieldName = 'nValUnitario'
        Width = 131
      end
      object cxGridDBTableView1nValTotalItem: TcxGridDBColumn
        Caption = 'Val. Total'
        DataBinding.FieldName = 'nValTotalItem'
      end
      object cxGridDBTableView1nValCMV: TcxGridDBColumn
        Caption = 'Custo Venda'
        DataBinding.FieldName = 'nValCMV'
      end
      object cxGridDBTableView1nMargemBruta: TcxGridDBColumn
        Caption = '% Lucro Bruto'
        DataBinding.FieldName = 'nMargemBruta'
      end
      object cxGridDBTableView1cNmDepartamento: TcxGridDBColumn
        Caption = 'Departamento'
        DataBinding.FieldName = 'cNmDepartamento'
        Width = 150
      end
      object cxGridDBTableView1cNmCategoria: TcxGridDBColumn
        Caption = 'Categoria'
        DataBinding.FieldName = 'cNmCategoria'
        Width = 150
      end
      object cxGridDBTableView1cNmSubCategoria: TcxGridDBColumn
        Caption = 'Sub Categoria'
        DataBinding.FieldName = 'cNmSubCategoria'
        Width = 150
      end
      object cxGridDBTableView1cNmSegmento: TcxGridDBColumn
        Caption = 'Segmento'
        DataBinding.FieldName = 'cNmSegmento'
        Width = 150
      end
      object cxGridDBTableView1cNmLinha: TcxGridDBColumn
        Caption = 'Linha'
        DataBinding.FieldName = 'cNmLinha'
        Width = 150
      end
      object cxGridDBTableView1cNmMarca: TcxGridDBColumn
        Caption = 'Marca'
        DataBinding.FieldName = 'cNmMarca'
        Width = 150
      end
      object cxGridDBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro'
        DataBinding.FieldName = 'cNmTerceiro'
        Visible = False
        GroupIndex = 0
        Width = 150
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 336
    Top = 240
  end
  object qryVendas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdCampanhaPromoc int'
      ''
      'Set @nCdCampanhaPromoc = :nPK'
      ''
      'SELECT ItemPedido.nCdItemPedido'
      '      ,Pedido.nCdPedido'
      '      ,Pedido.dDtPedido'
      '      ,ItemPedido.nCdProduto'
      '      ,ItemPedido.cNmItem'
      '      ,ItemPedido.nQtdeExpRec'
      '      ,ItemPedido.nValUnitario'
      '      ,ItemPedido.nValDesconto'
      '      ,ItemPedido.nValAcrescimo'
      '      ,ItemPedido.nValCustoUnit'
      '      ,ItemPedido.nValTotalItem'
      '      ,ItemPedido.nValCMV'
      
        '      ,Convert(DECIMAL(12,2),(CASE WHEN ItemPedido.nValTotalItem' +
        ' = 0 AND ItemPedido.nValCMV = 0 THEN 0'
      
        '                                   WHEN ItemPedido.nValTotalItem' +
        ' > 0 AND ItemPedido.nValCMV = 0 THEN 100'
      
        '                                   ELSE (((ItemPedido.nValTotalI' +
        'tem / ItemPedido.nValCMV)*100)-100)'
      '                              END)) nMargemBruta'
      '      ,Departamento.cNmDepartamento'
      '      ,Categoria.cNmCategoria'
      '      ,SubCategoria.cNmSubCategoria'
      '      ,Segmento.cNmSegmento'
      '      ,Linha.cNmLinha'
      '      ,Marca.cNmMarca'
      '      ,Terceiro.cNmTerceiro'
      '  FROM ItemPedidoAtendido'
      
        '       INNER JOIN ItemPedido   ON ItemPedido.nCdItemPedido     =' +
        ' ItemPedidoAtendido.nCdItemPedido'
      
        '       INNER JOIN Pedido       ON Pedido.nCdPedido             =' +
        ' ItemPedido.nCdPedido'
      
        '       LEFT  JOIN Terceiro     ON Terceiro.nCdTerceiro         =' +
        ' Pedido.nCdTerceiro'
      
        '       LEFT  JOIN Produto      ON Produto.nCdProduto           =' +
        ' ItemPedido.nCdProduto'
      
        '       LEFT  JOIN Departamento ON Departamento.nCdDepartamento =' +
        ' Produto.nCdDepartamento'
      
        '       LEFT  JOIN Categoria    ON Categoria.nCdCategoria       =' +
        ' Produto.nCdCategoria'
      
        '       LEFT  JOIN SubCategoria ON SubCategoria.nCdSubCategoria =' +
        ' Produto.nCdSubCategoria'
      
        '       LEFT  JOIN Segmento     ON Segmento.nCdSegmento         =' +
        ' Produto.nCdSegmento'
      
        '       LEFT  JOIN Linha        ON Linha.nCdLinha               =' +
        ' Produto.nCdLinha'
      
        '       LEFT  JOIN Marca        ON Marca.nCdMarca               =' +
        ' Produto.nCdMarca'
      ' WHERE Pedido.nCdTabStatusPed       IN (3,4,5)'
      
        '   AND nCdTipoItemPed <> 6  -- N'#227'o incluir os itens do kit formu' +
        'lado'
      '   AND ItemPedido.nCdCampanhaPromoc = @nCdCampanhaPromoc'
      'UNION ALL'
      'SELECT IPF.nCdItemPedido'
      '      ,Pedido.nCdPedido'
      '      ,Pedido.dDtPedido'
      '      ,IPF.nCdProduto'
      '      ,IPF.cNmItem'
      
        '      ,((IPF.nQtdePed / IP.nQtdePed) * nQtdeAtendida) * IPF.nVal' +
        'CustoUnit nValor'
      '      ,((IPF.nQtdePed / IP.nQtdePed) * nQtdeAtendida) as nQtde'
      '      ,IPF.nValDesconto'
      '      ,IPF.nValAcrescimo'
      '      ,IPF.nValCustoUnit'
      '      ,IPF.nValTotalItem'
      '      ,IPF.nValCMV'
      '      ,0 nMargemBruta'
      '      ,Departamento.cNmDepartamento'
      '      ,Categoria.cNmCategoria'
      '      ,SubCategoria.cNmSubCategoria'
      '      ,Segmento.cNmSegmento'
      '      ,Linha.cNmLinha'
      '      ,Marca.cNmMarca'
      '      ,Terceiro.cNmTerceiro'
      '  FROM ItemPedidoAtendido'
      
        '       INNER JOIN ItemPedido IP  ON IP.nCdItemPedido            ' +
        ' = ItemPedidoAtendido.nCdItemPedido'
      
        '       INNER JOIN ItemPedido IPF ON IPF.nCdItemPedidoPai        ' +
        ' = IP.nCdItemPedido'
      
        '       INNER JOIN Pedido         ON Pedido.nCdPedido            ' +
        ' = IP.nCdPedido'
      
        '       LEFT  JOIN Terceiro       ON Terceiro.nCdTerceiro        ' +
        ' = Pedido.nCdTerceiro'
      
        #9'   INNER JOIN Produto        ON Produto.nCdProduto           = ' +
        'IPF.nCdProduto'
      
        '       LEFT  JOIN Departamento   ON Departamento.nCdDepartamento' +
        ' = Produto.nCdDepartamento'
      
        '       LEFT  JOIN Categoria      ON Categoria.nCdCategoria      ' +
        ' = Produto.nCdCategoria'
      
        '       LEFT  JOIN SubCategoria   ON SubCategoria.nCdSubCategoria' +
        ' = Produto.nCdSubCategoria'
      
        '       LEFT  JOIN Segmento       ON Segmento.nCdSegmento        ' +
        ' = Produto.nCdSegmento'
      
        '       LEFT  JOIN Linha          ON Linha.nCdLinha              ' +
        ' = Produto.nCdLinha'
      
        '       LEFT  JOIN Marca          ON Marca.nCdMarca              ' +
        ' = Produto.nCdMarca'
      ' WHERE Pedido.nCdTabStatusPed       IN (3,4,5)'
      '   AND IPF.nCdCampanhaPromoc = @nCdCampanhaPromoc'
      
        '   AND IP.nCdTipoItemPed     = 6  -- N'#227'o incluir os itens do kit' +
        ' formulado'
      '')
    Left = 248
    Top = 184
    object qryVendasnCdItemPedido: TIntegerField
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryVendasnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
      ReadOnly = True
    end
    object qryVendasdDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
      ReadOnly = True
    end
    object qryVendasnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
      ReadOnly = True
    end
    object qryVendascNmItem: TStringField
      FieldName = 'cNmItem'
      ReadOnly = True
      Size = 150
    end
    object qryVendasnQtdeExpRec: TBCDField
      FieldName = 'nQtdeExpRec'
      ReadOnly = True
      Precision = 38
      Size = 6
    end
    object qryVendasnValUnitario: TBCDField
      FieldName = 'nValUnitario'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 38
      Size = 15
    end
    object qryVendasnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryVendasnValAcrescimo: TBCDField
      FieldName = 'nValAcrescimo'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryVendasnValCustoUnit: TBCDField
      FieldName = 'nValCustoUnit'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryVendasnValTotalItem: TBCDField
      FieldName = 'nValTotalItem'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryVendasnValCMV: TBCDField
      FieldName = 'nValCMV'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryVendasnMargemBruta: TBCDField
      FieldName = 'nMargemBruta'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryVendascNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      ReadOnly = True
      Size = 50
    end
    object qryVendascNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      ReadOnly = True
      Size = 50
    end
    object qryVendascNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      ReadOnly = True
      Size = 50
    end
    object qryVendascNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      ReadOnly = True
      Size = 50
    end
    object qryVendascNmLinha: TStringField
      FieldName = 'cNmLinha'
      ReadOnly = True
      Size = 50
    end
    object qryVendascNmMarca: TStringField
      FieldName = 'cNmMarca'
      ReadOnly = True
      Size = 50
    end
    object qryVendascNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      ReadOnly = True
      Size = 50
    end
  end
  object dsVendas: TDataSource
    DataSet = qryVendas
    Left = 296
    Top = 184
  end
end
