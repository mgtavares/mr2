unit fPedidoVendaWeb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, Menus, StdCtrls, Mask, DBCtrls, cxPC, cxControls,
  DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, cxLookAndFeelPainters,
  cxButtons, cxContainer, cxEdit, cxTextEdit, ACBrBase, ACBrValidador,
  ER2Lookup, ACBrSocket, ACBrSedex, DBClient;

type
  TfrmPedidoVendaWeb = class(TfrmCadastro_Padrao)
    ToolButton3: TToolButton;
    btOpcao: TToolButton;
    tabPedido: TcxPageControl;
    tabItemPedido: TcxTabSheet;
    tabParcela: TcxTabSheet;
    tabTransportadora: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    Image2: TImage;
    Image6: TImage;
    menuOpcao: TPopupMenu;
    qryMasternCdPedido: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    qryEmpresa: TADOQuery;
    dsEmpresa: TDataSource;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit5: TDBEdit;
    qryTipoPedido: TADOQuery;
    dsTipoPedido: TDataSource;
    qryTipoPedidocNmTipoPedido: TStringField;
    DBEdit6: TDBEdit;
    qryTerceiro: TADOQuery;
    dsTerceiro: TDataSource;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternCdTabStatusPed: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdUsuarioAutor: TIntegerField;
    Label5: TLabel;
    DBEdit9: TDBEdit;
    dsLoja: TDataSource;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit12: TDBEdit;
    Label8: TLabel;
    DBEdit13: TDBEdit;
    qryMastercNrPedTerceiro: TStringField;
    Label9: TLabel;
    DBEdit14: TDBEdit;
    qryDadoAutorz: TADOQuery;
    dsDadoAutorz: TDataSource;
    qryDadoAutorzcDadoAutoriz: TStringField;
    DBEdit15: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    DBEdit17: TDBEdit;
    qryCondPagto: TADOQuery;
    dsCondPagto: TDataSource;
    qryCondPagtocNmCondPagto: TStringField;
    DBEdit18: TDBEdit;
    qryItemEstoque: TADOQuery;
    dsItemPedido: TDataSource;
    qryItemEstoquenCdItemPedido: TIntegerField;
    qryItemEstoquenCdPedido: TIntegerField;
    qryItemEstoquenCdProduto: TIntegerField;
    qryItemEstoquecNmItem: TStringField;
    qryItemEstoquenQtdePed: TBCDField;
    qryItemEstoquenQtdeExpRec: TBCDField;
    qryItemEstoquenQtdeCanc: TBCDField;
    qryItemEstoquecCdProduto: TStringField;
    qryItemEstoquenValUnitario: TBCDField;
    qryItemEstoquenValDesconto: TBCDField;
    qryItemEstoquenValCustoUnit: TBCDField;
    qryItemEstoquenValTotalItem: TBCDField;
    qryMastercCupom: TStringField;
    qryMasternValAcrescimo: TBCDField;
    qryMasternValProdutos: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMasternValPedido: TBCDField;
    qryMastercOBS: TMemoField;
    qryMasternCdEnderecoEntrega: TIntegerField;
    qryMasternCdEnderecoCobranca: TIntegerField;
    qryEnderecos: TADOQuery;
    dsEnderecos: TDataSource;
    qryEnderecoscEndereco: TStringField;
    qryEnderecoscNmContato: TStringField;
    qryEnderecosiNumero: TIntegerField;
    qryEnderecoscBairro: TStringField;
    qryEnderecoscCidade: TStringField;
    qryEnderecoscUF: TStringField;
    qryEnderecoscCep: TStringField;
    DBGridEh3: TDBGridEh;
    btGeraParcela: TcxButton;
    qryPrazoPedido: TADOQuery;
    dsSugereParcela: TDataSource;
    qryPrazoPedidodVencto: TDateTimeField;
    qryPrazoPedidoiDias: TIntegerField;
    qryPrazoPedidonValPagto: TBCDField;
    Label37: TLabel;
    Label12: TLabel;
    DBEdit19: TDBEdit;
    Label17: TLabel;
    DBEdit20: TDBEdit;
    Label18: TLabel;
    DBEdit23: TDBEdit;
    Label19: TLabel;
    DBEdit32: TDBEdit;
    Label36: TLabel;
    SP_SUGERE_PARCELA_PEDIDO_WEB: TADOStoredProc;
    Label20: TLabel;
    qryEnderecosTipoEnd: TStringField;
    qryTipoEnd: TADOQuery;
    dsTipoEnd: TDataSource;
    qryTipoEndcNmTipoEnd: TStringField;
    qryEnderecosnCdTipoEnd: TIntegerField;
    qryTabStatusPed: TADOQuery;
    TabStatusPed: TDataSource;
    qryTabStatusPedcNmTabStatusPed: TStringField;
    DBEdit22: TDBEdit;
    qryMasternValDescFrete: TBCDField;
    qryMasternValTotalFrete: TBCDField;
    qryMasternValJurosCartao: TBCDField;
    DBEdit25: TDBEdit;
    btCliente: TMenuItem;
    btTipoPedido: TMenuItem;
    btCondPagto: TMenuItem;
    qryMasteriQtdeParcela: TIntegerField;
    qryMasteriQtdeDiasFrete: TIntegerField;
    DBEdit21: TDBEdit;
    cxTextEdit1: TcxTextEdit;
    Label23: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label24: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label29: TLabel;
    DBEdit16: TDBEdit;
    DBEdit24: TDBEdit;
    Label38: TLabel;
    ACBrValidador: TACBrValidador;
    SP_FINALIZA_PEDIDO: TADOStoredProc;
    qryTipoPedidocGerarFinanc: TIntegerField;
    qryTipoPedidocFlgVenda: TIntegerField;
    qryGrupoCredito: TADOQuery;
    qryGrupoCreditonCdGrupoCredito: TIntegerField;
    qryGrupoCreditocNmGrupoCredito: TStringField;
    qryGrupoCreditocFlgAlertaTitAtrasado: TIntegerField;
    dsGrupoCredito: TDataSource;
    DBEdit28: TDBEdit;
    Label11: TLabel;
    DBEdit29: TDBEdit;
    qryTerceironCdGrupoCredito: TIntegerField;
    qryAux: TADOQuery;
    btTerceiro: TMenuItem;
    N2: TMenuItem;
    tabAnotacao: TcxTabSheet;
    Image3: TImage;
    dbMemoOBS: TDBMemo;
    qryTerceironCdTerceiro: TIntegerField;
    SP_EMPENHA_ESTOQUE_PEDIDO: TADOStoredProc;
    qryMasterdDtAutor: TDateTimeField;
    qryMastercStatusPed: TStringField;
    qryMasternCdTabStatusPedWeb: TIntegerField;
    qryGrupoCreditocFlgPedidosAnaliseFin: TIntegerField;
    SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB: TADOStoredProc;
    N3: TMenuItem;
    btGerarPicking: TMenuItem;
    btSeparaoEmbalagem: TMenuItem;
    qryTipoPedidonCdTabTipoPedido: TIntegerField;
    ToolButton10: TToolButton;
    btFinalizarPedido: TToolButton;
    ToolButton13: TToolButton;
    btImprimirPedido: TToolButton;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocExigeAutor: TIntegerField;
    qryTipoPedidonCdEspTit: TIntegerField;
    qryCondPagtoiQtdeParcelas: TIntegerField;
    qryTerceiroTransp: TADOQuery;
    qryTerceiroTranspnCdTerceiro: TIntegerField;
    qryTerceiroTranspcNmTerceiro: TStringField;
    Label21: TLabel;
    DBEdit30: TDBEdit;
    dsTerceiroTransp: TDataSource;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMastercTipoEntrega: TStringField;
    DBEdit33: TDBEdit;
    Label22: TLabel;
    btImpEtiqueta: TcxButton;
    qryMastercTrackingCode: TStringField;
    DBEdit34: TDBEdit;
    qryAtualizaTrackingCode: TADOQuery;
    DBEdit31: TDBEdit;
    qryItemEstoquenValFrete: TBCDField;
    tabInfoFinanc: TcxTabSheet;
    dbMemoOBSFinanc: TDBMemo;
    Image4: TImage;
    qryMastercOBSFinanc: TMemoField;
    qryMastercNrDocto: TStringField;
    qryMastercNrAutorizacao: TStringField;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label25: TLabel;
    DBEdit27: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    N1: TMenuItem;
    btGerarRegTransmissao: TMenuItem;
    SP_ENVIA_PEDIDOWEB_TEMPREGISTROWEB: TADOStoredProc;
    Label26: TLabel;
    qryTabTipoModFrete: TADOQuery;
    qryTabTipoModFretenCdTabTipoModFrete: TIntegerField;
    qryTabTipoModFretecNmTabTipoModFrete: TStringField;
    DBEdit37: TDBEdit;
    dsTabTipoModFrete: TDataSource;
    qryMasternCdTabTipoModFrete: TIntegerField;
    btModoFrete: TcxButton;
    ACBrSedex1: TACBrSedex;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    cxPageControl2: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    DBGridEh4: TDBGridEh;
    btAtualizarCodRastreio: TcxButton;
    btAtualizarRastreio: TcxButton;
    dsRastreio: TDataSource;
    cdsRastreio: TClientDataSet;
    cdsRastreiodDtAcao: TDateTimeField;
    cdsRastreiocLocal: TStringField;
    cdsRastreiocSituacao: TStringField;
    cdsRastreiocObs: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btFinalizarPedidoClick(Sender: TObject);
    procedure btGeraParcelaClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryEnderecosCalcFields(DataSet: TDataSet);
    procedure DBEdit17KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit17Exit(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure btClienteClick(Sender: TObject);
    procedure btTipoPedidoClick(Sender: TObject);
    procedure btCondPagtoClick(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btTerceiroClick(Sender: TObject);
    procedure btImprimirPedidoClick(Sender: TObject);
    procedure btGerarPickingClick(Sender: TObject);
    procedure btSeparaoEmbalagemClick(Sender: TObject);
    procedure btImpEtiquetaClick(Sender: TObject);
    procedure btAtualizarCodRastreioClick(Sender: TObject);
    procedure btGerarRegTransmissaoClick(Sender: TObject);
    procedure btModoFreteClick(Sender: TObject);
    procedure btAtualizarRastreioClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPedidoVendaWeb: TfrmPedidoVendaWeb;

implementation

{$R *.dfm}

uses
  fMenu, fLookup_Padrao, fClienteVarejoPessoaFisica, fTipoPedido, fCondPagtoPDV,
  fTerceiro, rPedidoVenda,fMonitorIntegracaoWeb,fMotivoCancelaSaldoPedido,
  fGerarPickingList, fSeparaEmbalaPedido, fModImpETP, Math;

procedure TfrmPedidoVendaWeb.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster   := 'PEDIDO';
  nCdTabelaSistema  := 30;
  nCdConsultaPadrao := 96;

  tabPedido.ActivePage := tabItemPedido;
end;

procedure TfrmPedidoVendaWeb.btFinalizarPedidoClick(Sender: TObject);
var
  nValProdutos   : Double;
  nValDesconto   : Double;
  objFormTrans   : TfrmMonitorIntegracaoWeb;
  objFormCancela : TfrmMotivoCancelaSaldoPedido;
begin
  inherited;

  nValProdutos := 0;
  nValDesconto := 0;

  if (qryMaster.Eof) then
      Exit;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post;
  end;

  if (Trim(qryTerceirocCNPJCPF.Value) = '') then
  begin
      MensagemErro('Cliente n�o possui CPF/CNPJ preenchido.');
      Abort;
  end
  else
  begin
      { -- valida CNPJ/CPF -- }
      ACBrValidador.Documento   := qryTerceirocCNPJCPF.Value;
      ACBrValidador.IgnorarChar := '';

      case (Length(qryTerceirocCNPJCPF.Value)) of
          11 : ACBrValidador.TipoDocto := docCPF;
          14 : ACBrValidador.TipoDocto := docCNPJ;
      end;

      if not (ACBrValidador.Validar) then
      begin
          MensagemAlerta('CNPJ/CPF inv�lido. N�o informe pontos ou hifens, apenas os n�meros.');
          DBEdit7.SetFocus;
          Abort;
      end;
  end;

  { -- verifica tipo pedido -- }
  if ((qryTipoPedidonCdTabTipoPedido.IsNull) or (qryTipoPedidonCdTabTipoPedido.Value <> 1)) then
  begin
      MensagemAlerta('Tipo de pedido deve possuir a a��o de SA�DA no estoque.');
      DBEdit3.SetFocus;
      Abort;
  end;

  if (qryTipoPedidocGerarFinanc.Value <> 1) then
  begin
      MensagemAlerta('Tipo de pedido deve estar configurado para gerar t�tulo financeiro.');
      DBEdit3.SetFocus;
      Abort;
  end;

  if (qryTipoPedidocFlgVenda.Value <> 1) then
  begin
      MensagemAlerta('Tipo de pedido deve estar configurado para ser considerado como venda.');
      DBEdit3.SetFocus;
      Abort;
  end;

  { -- verifica grupo de cr�dito do cliente -- }
  if ((qryTipoPedidocGerarFinanc.Value = 1) and (qryTipoPedidocFlgVenda.Value = 1)) then
  begin
      {if ((frmMenu.LeParametro('OBRIGAGRPCRDPED') = 'S') and (qryGrupoCredito.IsEmpty)) then}
      if (qryGrupoCredito.IsEmpty) then
      begin
          MensagemAlerta('Grupo de Cr�dito n�o informado para este cliente.');
          DBEdit28.SetFocus;
          Abort;
      end;

      if (qryGrupoCreditocFlgPedidosAnaliseFin.Value <> 1) then
      begin
          MensagemAlerta('Grupo de Cr�dito n�o configurado para an�lise de pedidos.');
          DBEdit28.SetFocus;
          Abort;
      end;

      { -- verifica se cliente possui t�tulos em atraso -- }
      if (qryGrupoCreditocFlgAlertaTitAtrasado.Value = 1) then
      begin
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('SELECT 1                                                                            ');
          qryAux.SQL.Add('  FROM Titulo                                                                       ');
          qryAux.SQL.Add(' WHERE nCdTerceiro = ' + qryTerceironCdTerceiro.AsString                             );
          qryAux.SQL.Add('   AND nCdEmpresa  = ' + IntToStr(frmMenu.nCdEmpresaAtiva)                           );
          qryAux.SQL.Add('   AND nCdEspTit   = CAST(dbo.fn_LeParametro('+ #39 +'CDESPTIT-WEB'+ #39 +') as int)');
          qryAux.SQL.Add('   AND dDtCancel   IS NULL                                                          ');
          qryAux.SQL.Add('   AND nSaldoTit   > 0                                                              ');
          qryAux.SQL.Add('   AND dDtVenc     < dbo.fn_OnlyDate(GETDATE())                                     ');
          qryAux.Open;

          if (not qryAux.IsEmpty) then
          begin
              case MessageDlg('Cliente ' + qryTerceirocNmTerceiro.Value + ' possui t�tulos em atraso. Deseja continuar ?', mtConfirmation,[mbYes,mbNo],0) of
                  mrNo : Abort;
              end;
          end;
      end;
  end;

  { -- calcula total dos itens -- }
  //qryItemEstoque.First;
  //qryItemEstoque.DisableControls;

  //while (not qryItemEstoque.Eof) do
  //begin
      { -- total itens -- }
      //nValProdutos := nValProdutos + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value);
      { -- total desconto itens -- }
      //nValDesconto := nValDesconto + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDesconto.Value);

      //qryItemEstoque.Next;
  //end;

  //qryItemEstoque.First;
  //qryItemEstoque.EnableControls;

  { -- calcula total desconto -- }
  //nValDesconto := nValDesconto + qryMasternValDescFrete.Value + qryMasternValDesconto.Value;

  { -- atualiza total pedido -- }
  //qryMaster.Edit;
  //qryMasternValProdutos.Value := nValProdutos;
  //qryMasternValDesconto.Value := nValDesconto;
  //qryMasternValPedido.Value   := (nValProdutos + qryMasternValAcrescimo.Value + qryMasternValFrete.Value) - nValDesconto;
  //qryMaster.Post;

  { -- verifica informa��es de pagamento do pedido -- }
  btGeraParcela.Click;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT SUM(nValPagto)                            ');
  qryAux.SQL.Add('  FROM PrazoPedido                               ');
  qryAux.SQL.Add(' WHERE nCdPedido = ' + qryMasternCdPedido.asString);
  qryAux.Open;

  if ((qryAux.Eof and (qryMasternValPedido.Value > 0)) or (qryAux.FieldList[0].Value <> qryMasternValPedido.Value)) then
  begin
      MensagemAlerta('Valor da soma das parcelas � diferente do valor total do pedido.');
      tabPedido.ActivePage := tabParcela;
      btGeraParcela.SetFocus;
      Abort;
  end;

  case MessageDlg('O pedido ser� finalizado e n�o poder� sofrer altera��es. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Abort;
  end;

  { -- finaliza pedido -- }
  try
      frmMenu.Connection.BeginTrans;

      //SP_FINALIZA_PEDIDO.Close;
      //SP_FINALIZA_PEDIDO.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value;
      //SP_FINALIZA_PEDIDO.ExecProc;

      { -- veririca se status do pedido web difere do pedido efetivo e chama processo correspondente -- }
      case (qryMasternCdTabStatusPedWeb.Value) of
          { -- atualiza status (2 : An�lise Financeiro) -- }
          //1 : begin
          //        qryAux.Close;
          //        qryAux.SQL.Clear;
          //        qryAux.SQL.Add('UPDATE PedidoWeb                                         ');
          //        qryAux.SQL.Add('   SET cCdTabStatusPed = ' + #39 + '2' + #39              );
          //        qryAux.SQL.Add(' WHERE cCdPedidoWeb    = ' + qryMastercNrPedTerceiro.Value);
          //        qryAux.ExecSQL;
          //    end;

          { -- autoriza pedido (3	: Autorizado) -- }
          3 : begin
                  try
                      objFormTrans := TfrmMonitorIntegracaoWeb.Create(nil);

                      { -- se tipo de pedido exige autoriza��o, verifica se usu�rio possui permiss�o para autorizar este tipo de pedido -- }
                      if (qryTipoPedidocExigeAutor.Value = 1) then
                      begin
                          objFormTrans.qryVerificaAutPedido.Close;
                          objFormTrans.qryVerificaAutPedido.Parameters.ParamByName('nPK').Value        := qryMasternCdTipoPedido.Value;
                          objFormTrans.qryVerificaAutPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
                          objFormTrans.qryVerificaAutPedido.Open;

                          if (objFormTrans.qryVerificaAutPedido.IsEmpty) then
                          begin
                              MensagemAlerta('Usu�rio n�o possui permiss�o para autorizar este tipo de pedido.' + #13 + 'Tipo de Pedido: ' + UpperCase(qryMasternCdTipoPedido.AsString + ' - ' + qryTipoPedidocNmTipoPedido.Value));

                              { -- cancela transa��o -- }
                              if (frmMenu.Connection.InTransaction) then
                                  frmMenu.Connection.RollbackTrans;

                              Abort;
                          end;
                      end;

                      { -- chama processo de autoriza��o de pedido -- }
                      objFormTrans.AutorizarPedido(qryMasternCdPedido.Value);

                      { -- verifica se pedido foi aprovado -- }
                      qryAux.Close;
                      qryAux.SQL.Clear;
                      qryAux.SQL.Add('SELECT 1 FROM Pedido WHERE nCdTabStatusPed = 3 AND nCdPedido = ' + qryMasternCdPedido.asString);
                      qryAux.Open;

                      if (not qryAux.IsEmpty) then
                      begin
                          { -- liquida adiantamento de cr�dito do cliente e gera t�tulos para concilia��o no financeiro -- }
                          SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB.Close;
                          SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB.Parameters.ParamByName('@nCdPedido').Value  := qryMasternCdPedido.Value;
                          SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
                          SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB.ExecProc;
                      end;
                  finally
                      FreeAndNil(objFormTrans);
                  end;
              end;
          { -- cancela pedido (10	: Cancelado) -- }
          10 : begin
                   try
                       objFormCancela := TfrmMotivoCancelaSaldoPedido.Create( Self );

                       { -- chama processo de cancelamento de pedido -- }
                       objFormCancela.cancelaSaldoPedido(qryMasternCdPedido.Value, 0);

                       { -- verifica se pedido foi cancelado -- }
                       //qryAux.Close;
                       //qryAux.SQL.Clear;
                       //qryAux.SQL.Add('SELECT 1 FROM Pedido WHERE nCdTabStatusPed = 10 AND nCdPedido = ' + qryMasternCdPedido.AsString);
                       //qryAux.Open;

                       { -- removida bloco abaixo devido adto cr�dito ser gerado apenas quando pedido for aprovado no sistema -- }
                       { -- se houver cancelamento de pedido ap�s aprova��o, ser� necess�rio gerar o provisionamento de liq. de cr�dito do cliente -- }
                       //if (not qryAux.IsEmpty) then
                       //begin
                       //    { -- cancela adiantamento de cr�dito do cliente -- }
                       //    SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB.Close;
                       //    SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB.Parameters.ParamByName('@nCdPedido').Value  := qryMasternCdPedido.Value;
                       //    SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
                       //    SP_PROCESSA_ADIANTAMENTO_PEDIDOWEB.ExecProc;
                       //end;
                   finally
                       FreeAndNil(objFormCancela);
                   end;
               end;
      end;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Pedido finalizado com sucesso.');
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      Raise;
      Exit;
  end;

  { -- da um refresh no pedido  -- }
  PosicionaQuery(qryMaster, DBEdit1.Text);

  { -- imprime pedido finalizado -- }
  case MessageDlg('Deseja imprimir o pedido ?', mtConfirmation,[mbYes,mbNo],0) of
      mrYes : btImprimirPedido.Click;
  end;
end;

procedure TfrmPedidoVendaWeb.btGeraParcelaClick(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      Exit;

  if (qryMasternValPedido.Value = 0) then
  begin
      ShowMessage('Pedido sem valor.');
      Exit;
  end;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post;
  end;

  { -- verifica se qtde. parcelas do pedido � igual a qtde. de parcelas configurado na codi��o de pagamento -- }
  if (qryMasternCdCondPagto.IsNull) then
  begin
      MensagemAlerta('Informe a condi��o de pagamento.');
      DBEdit17.SetFocus;
      Abort;
  end;

  if (qryMasteriQtdeParcela.Value <> qryCondPagtoiQtdeParcelas.Value) then
  begin
      MensagemAlerta('Quantidade de parcelas do Pedido (' + qryMasteriQtdeParcela.AsString + ' parc.) difere da quantidade de parcelas cadastradas na Condi��o de Pagamento (' + qryCondPagtoiQtdeParcelas.AsString + ' parc.).');
      DBEdit17.SetFocus;
      Abort;
  end;
    
  frmMenu.Connection.BeginTrans;

  try
      SP_SUGERE_PARCELA_PEDIDO_WEB.Close;
      SP_SUGERE_PARCELA_PEDIDO_WEB.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value;
      SP_SUGERE_PARCELA_PEDIDO_WEB.ExecProc;
  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      Raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.AsString);
end;

procedure TfrmPedidoVendaWeb.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.Eof) then
      Exit;

  tabPedido.ActivePage := tabItemPedido;

  qryEmpresa.Close;
  qryLoja.Close;
  qryTipoPedido.Close;
  qryTerceiro.Close;
  qryTerceiro.Close;
  qryGrupoCredito.Close;
  qryCondPagto.Close;
  qryPrazoPedido.Close;
  qryDadoAutorz.Close;
  qryItemEstoque.Close;
  qryTabStatusPed.Close;
  qryEnderecos.Close;
  qryTabTipoModFrete.Close;

  PosicionaQuery(qryEmpresa,qryMasternCdEmpresa.AsString);
  PosicionaQuery(qryLoja,qryMasternCdLoja.AsString);
  PosicionaQuery(qryTipoPedido,qryMasternCdTipoPedido.AsString);
  PosicionaQuery(qryTerceiro,qryMasternCdTerceiro.AsString);
  PosicionaQuery(qryTerceiroTransp,qryMasternCdTerceiroTransp.AsString);
  PosicionaQuery(qryGrupoCredito, qryTerceironCdGrupoCredito.AsString);
  PosicionaQuery(qryCondPagto,qryMasternCdCondPagto.AsString);
  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.AsString);
  PosicionaQuery(qryDadoAutorz,qryMasternCdPedido.AsString);
  PosicionaQuery(qryItemEstoque,qryMasternCdPedido.AsString);
  PosicionaQuery(qryTabStatusPed,qryMasternCdTabStatusPed.AsString);
  PosicionaQuery(qryTabTipoModFrete, qryMasternCdTabTipoModFrete.AsString);

  qryEnderecos.Close;
  qryEnderecos.Parameters.ParamByName('nPKCob').Value := qryMasternCdEnderecoCobranca.Value;
  qryEnderecos.Parameters.ParamByName('nPKEnt').Value := qryMasternCdEnderecoEntrega.Value;
  qryEnderecos.Open;
end;

procedure TfrmPedidoVendaWeb.qryEnderecosCalcFields(DataSet: TDataSet);
begin
  inherited;
  
  PosicionaQuery(qryTipoEnd,qryEnderecosnCdTipoEnd.AsString);
  qryEnderecosTipoEnd.Value := qryTipoEndcNmTipoEnd.Value;
end;

procedure TfrmPedidoVendaWeb.DBEdit17KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : integer;
begin
  inherited;

  if (qryMaster.State <> dsEdit) then
      Exit;

  case (Key) of
      vk_F4 : begin
                  if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
                  begin
                      nPK := frmLookup_Padrao.ExecutaConsulta(138);

                      if (nPK > 0) then
                      begin
                          qryMasternCdCondPagto.Value := nPK;
                      end;
                  end;
              end;
  end;
end;

procedure TfrmPedidoVendaWeb.DBEdit17Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active = False) then
      Exit;

  qryCondPagto.Close;
  PosicionaQuery(qryCondPagto,qryMasternCdCondPagto.AsString);
end;

procedure TfrmPedidoVendaWeb.btConsultarClick(Sender: TObject);
var
  nPK : Integer;
begin
  //inherited;

  nPK := 0;
  nPK := frmLookup_Padrao.ExecutaConsulta2(96,'cFlgPedidoWeb = 1');

  PosicionaPK(nPK);
end;

procedure TfrmPedidoVendaWeb.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (dataCol = 4) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) < qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clYellow;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) >= qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clBlue;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  if (dataCol = 5) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemEstoquenQtdeCanc.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clRed;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;
end;

procedure TfrmPedidoVendaWeb.btClienteClick(Sender: TObject);
var
  objForm : TfrmClienteVarejoPessoaFisica;
begin
  inherited;

  if (not frmMenu.fnValidaUsuarioAPL('FRMCLIENTEVAREJOPESSOAFISICA')) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Abort;
  end;

  { -- carrega aplica��o -- }
  objForm := TfrmClienteVarejoPessoaFisica.Create(nil);

  if (not qryMaster.IsEmpty) then
      PosicionaQuery(objForm.qryMaster, qryMasternCdTerceiro.AsString);

  showForm(objForm,True);

  { -- da refresh no registro  -- }
  if (not qryMaster.IsEmpty) then
      PosicionaQuery(qryTerceiro,qryMasternCdTerceiro.AsString);
end;

procedure TfrmPedidoVendaWeb.btTerceiroClick(Sender: TObject);
var
  objForm : TfrmTerceiro;
begin
  inherited;

  if (not frmMenu.fnValidaUsuarioAPL('FRMTERCEIRO')) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Abort;
  end;

  { -- carrega aplica��o -- }
  objForm := TfrmTerceiro.Create(nil);

  if (not qryMaster.IsEmpty) then
      PosicionaQuery(objForm.qryMaster, qryMasternCdTerceiro.AsString);

  showForm(objForm,True);

  { -- da refresh no registro  -- }
  if (not qryMaster.IsEmpty) then
  begin
      PosicionaQuery(qryTerceiro,qryMasternCdTerceiro.AsString);
      PosicionaQuery(qryGrupoCredito, qryTerceironCdGrupoCredito.AsString);
  end;
end;

procedure TfrmPedidoVendaWeb.btTipoPedidoClick(Sender: TObject);
var
  objForm : TfrmTipoPedido;
begin
  inherited;

  if (not frmMenu.fnValidaUsuarioAPL('FRMTIPOPEDIDO')) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Abort;
  end;

  { -- carrega aplica��o -- }
  objForm := TfrmTipoPedido.Create(nil);

  if (not qryMaster.IsEmpty) then
      PosicionaQuery(objForm.qryMaster, qryMasternCdTipoPedido.AsString);

  showForm(objForm,True);

  { -- da refresh no registro  -- }
  if (not qryMaster.IsEmpty) then
      PosicionaQuery(qryTipoPedido,qryMasternCdTipoPedido.AsString);
end;

procedure TfrmPedidoVendaWeb.btCondPagtoClick(Sender: TObject);
var
  objForm : TfrmCondPagtoPDV;
begin
  inherited;

  if (not frmMenu.fnValidaUsuarioAPL('FRMCONDPAGTO')) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Abort;
  end;

  { -- carrega aplica��o -- }
  objForm := TfrmCondPagtoPDV.Create(nil);

  if (not qryMaster.IsEmpty) then
      PosicionaQuery(objForm.qryMaster, qryMasternCdCondPagto.AsString);

  showForm(objForm,True);

  { -- da refresh no registro  -- }
  if (not qryMaster.IsEmpty) then
  begin
      PosicionaQuery(qryCondPagto,qryMasternCdCondPagto.AsString);
  end;
end;

procedure TfrmPedidoVendaWeb.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  qryLoja.Close;
  qryTipoPedido.Close;
  qryTerceiro.Close;
  qryTerceiroTransp.Close;
  qryGrupoCredito.Close;
  qryCondPagto.Close;
  qryPrazoPedido.Close;
  qryDadoAutorz.Close;
  qryItemEstoque.Close;
  qryTabStatusPed.Close;
  qryTabTipoModFrete.Close;
  qryEnderecos.Close;

  tabPedido.ActivePage := tabItemPedido;
end;

procedure TfrmPedidoVendaWeb.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      MensagemAlerta('Pedido finalizado n�o pode ser alterado.') ;
      PosicionaQuery(qryMaster, DBEdit1.Text);
      Abort;
  end;

  inherited;
end;

procedure TfrmPedidoVendaWeb.btImprimirPedidoClick(Sender: TObject);
var
  objRel : TrptPedidoVenda;

begin
  if (qryMaster.IsEmpty) then
      Exit;

  objRel := TrptPedidoVenda.Create(nil);

  try
      try
          PosicionaQuery(objRel.qryPedido,qryMasternCdPedido.AsString);
          PosicionaQuery(objRel.qryItemEstoque_Grade,qryMasternCdPedido.AsString);
          
          objRel.QRSubDetail1.Enabled := True;
          objRel.QRSubDetail2.Enabled := False;
          objRel.QRSubDetail3.Enabled := False;

          if (objRel.qryItemEstoque_Grade.eof) then
              objRel.QRSubDetail1.Enabled := False;

         objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

         objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do Relat�rio');
          Raise;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TfrmPedidoVendaWeb.btGerarPickingClick(Sender: TObject);
var
  objForm : TfrmGerarPickingList;
begin
  inherited;

  if (qryMaster.IsEmpty) then
  begin
      MensagemAlerta('Nenhum pedido selecionado.');
      Abort;
  end;

  if (   (qryMasternCdTabStatusPed.Value <> 3)
      and(qryMasternCdTabStatusPed.Value <> 4)) then
  begin
      MensagemAlerta('O Status do pedido n�o permite gera��o de picking list.');
      Abort;
  end;

  objForm := TfrmGerarPickingList.Create(Self);
  objForm.nCdPedido         := qryMasternCdPedido.Value;
  objForm.cFlgOrdemProducao := 0; {-- informa que o pedido n�o tem OP Vinculada --}
  objForm.nCdOrdemProducao  := 0;
  objForm.cOBS              := qryMastercOBS.Value;

  showForm(objForm,True);
end;

procedure TfrmPedidoVendaWeb.btSeparaoEmbalagemClick(Sender: TObject);
var
  objForm : TfrmSeparaEmbalaPedido;
begin
  inherited;

  if (not frmMenu.fnValidaUsuarioAPL('FRMSEPARAEMBALAPEDIDO')) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Abort;
  end;

  { -- carrega aplica��o -- }
  objForm := TfrmSeparaEmbalaPedido.Create(Self);
  showForm(objForm, True);

  { -- da refresh no registro  -- }
  if (not qryMaster.IsEmpty) then
      PosicionaPK(qryMasternCdPedido.Value);
end;

procedure TfrmPedidoVendaWeb.btImpEtiquetaClick(Sender: TObject);
var
    objForm : TfrmModImpETP;
    arrPed  : array of integer;
begin
    inherited;

    if (not qryMaster.Active) then
        Exit;

    try
        objForm := TfrmModImpETP.Create(nil);

        SetLength(arrPed,1);

        arrPed[0] := qryMasternCdPedido.Value;

        objForm.emitirEtiquetas(arrPed,1);
    finally
        FreeAndNil(objForm);
    end;
end;

procedure TfrmPedidoVendaWeb.btAtualizarCodRastreioClick(Sender: TObject);
var
  cTrackingCode     : String;
  cTrackingCodeConf : String;
label
  solicitaTrackingCode;
begin
  //inherited

  if (not qryMaster.Active) then
      Exit;

  { -- permite atualizar c�digo de rastreio enquanto o pedido ainda n�o foi transmitido para a plataforma -- }
  { -- obs: no ato do faturamento do pedido o usu�rio pode atualizar tamb�m o c�digo de rastreio          -- }
  if (qryMasternCdTabStatusPed.Value = 10) then
  begin
      MensagemAlerta('Pedido cancelado n�o permite altera��o do C�digo de Rastreio');
      Exit;
  end;

  if ((qryMasternCdTabStatusPed.Value > 3) and (Trim(qryMastercTrackingCode.Value) <> '')) then
  begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT iIDRegistro'                                                                );
      qryAux.SQL.Add('  FROM TempRegistroWeb'                                                            );
      qryAux.SQL.Add(' WHERE iIDRegistro = ' + qryMastercNrPedTerceiro.Value                             );
      qryAux.SQL.Add('   AND nCdTabelaSistema = dbo.fn_TabelaSistema(' + #39 + 'PEDIDOWEB' + #39 + ')'   );
      qryAux.SQL.Add('   AND ((TempRegistroWeb.dDtIntegracao IS NULL) OR (TempRegistroWeb.cFlgErro = 1))');
      qryAux.Open;

      if (qryAux.Eof) then
      begin
          MensagemAlerta('Pedido transmitido para web n�o permite altera��o do C�digo de Rastreio.');
          Exit;
      end;
  end;

  solicitaTrackingCode:
  
  InputQuery('C�digo de Rastreamento','Informe o C�digo de Rastreamento',cTrackingCode);

  if (Trim(cTrackingCode) = '') then
      ShowMessage('C�digo de Rastreio n�o informado.')
  else
  begin
      InputQuery('C�digo de Rastreamento','Confirme o C�digo de Rastreamento',cTrackingCodeConf);

      if (Trim(cTrackingCode) <> Trim(cTrackingCodeConf)) then
      begin
          MensagemAlerta('Confirma��o do c�digo de rastreio inv�lida.');

          cTrackingCode     := '';
          cTrackingCodeConf := '';

          goto solicitaTrackingCode;
      end
      else
      begin
          qryAtualizaTrackingCode.Close;
          qryAtualizaTrackingCode.Parameters.ParamByName('nCdPedido').Value     := qryMasternCdPedido.Value;
          qryAtualizaTrackingCode.Parameters.ParamByName('cTrackingCode').Value := cTrackingCode;
          qryAtualizaTrackingCode.ExecSQL;

          ShowMessage('C�digo de Rastreio atualizado com sucesso.');

          PosicionaPK(qryMasternCdPedido.Value);
      end;
  end;
end;

procedure TfrmPedidoVendaWeb.btGerarRegTransmissaoClick(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMasternCdTabStatusPed.Value <= 3) then
  begin
      MensagemAlerta('Status do Pedido n�o permite gera��o de arquivo para transmiss�o.');
      Exit;
  end;

  { -- verifica se pedido j� foi enviado para o monitor -- }
  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT iIDRegistro'                                                             );
  qryAux.SQL.Add('  FROM TempRegistroWeb'                                                         );
  qryAux.SQL.Add(' WHERE iIDRegistro = ' + qryMastercNrPedTerceiro.Value                          );
  qryAux.SQL.Add('   AND nCdTabelaSistema = dbo.fn_TabelaSistema(' + #39 + 'PEDIDOWEB' + #39 + ')');
  qryAux.Open;

  if (not qryAux.IsEmpty) then
  begin
      MensagemAlerta('Registro de transmiss�o j� processado anteriormente.');
      Abort;
  end;

  case MessageDlg('Confirma gera��o do registro para transmiss�o ?', mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Abort;
  end;

  try
      SP_ENVIA_PEDIDOWEB_TEMPREGISTROWEB.Close;
      SP_ENVIA_PEDIDOWEB_TEMPREGISTROWEB.Parameters.ParamByName('@nCdPedidoWeb').Value := frmMenu.ConvInteiro(qryMastercNrPedTerceiro.Value);
      SP_ENVIA_PEDIDOWEB_TEMPREGISTROWEB.ExecProc;

      ShowMessage('Registro de transmiss�o gerado com sucesso.');
  except
      MensagemErro('Erro no processamento');
      Raise;
  end;
end;

procedure TfrmPedidoVendaWeb.btModoFreteClick(Sender: TObject);
var
  nPK : Integer;
begin
  //inherited;

  if (not qryMaster.Active) then
      Exit;

  if (qryMasternCdTabStatusPed.Value > 3) then
  begin
      MensagemAlerta('Status do pedido n�o permite alterar seu modo de frete.');
      Exit;
  end;

  nPK := 0;
  nPK := frmLookup_Padrao.ExecutaConsulta(776);

  if ((qryMasternValFrete.Value = 0) and (nPK <> 9)) then
  begin
      MensagemAlerta('Modo de Frete inv�lido devido pedido n�o possuir valor de frete informado.');
      Abort;
  end;

  if ((qryMasternValFrete.Value > 0) and (nPK = 9)) then
  begin
      MensagemAlerta('Modo de Frete inv�lido devido pedido possuir valor de frete informado.');
      Abort;
  end;

  case MessageDlg('Confirma altera��o do modo de frete para este pedido ?', mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Abort;
  end;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE Pedido'                                             );
  qryAux.SQL.Add('   SET nCdTabTipoModFrete = ' + IntToStr(nPK)              );
  qryAux.SQL.Add(' WHERE nCdPedido          = ' + qryMasternCdPedido.AsString);
  qryAux.ExecSQL;

  PosicionaQuery(qryTabTipoModFrete, IntToStr(nPK));
end;

procedure TfrmPedidoVendaWeb.btAtualizarRastreioClick(Sender: TObject);
var
  i : Integer;
begin
  if (qryMaster.IsEmpty) then
      Exit;

  if (Trim(qryMastercTrackingCode.Value) = '') then
      Exit;

  try
      ACBrSedex1.Rastrear(qryMastercTrackingCode.Value);
      cdsRastreio.Close;
      cdsRastreio.CreateDataSet;
      cdsRastreio.EmptyDataSet;
  except
      MensagemErro('Falha ao consultar os dados do rastreio.');
      Raise
  end;

  for i := 0 to ACBrSedex1.retRastreio.Count -1 do
  begin
      cdsRastreio.Append;
      cdsRastreiodDtAcao.Value   := ACBrSedex1.retRastreio[i].DataHora;
      cdsRastreiocLocal.Value    := ACBrSedex1.retRastreio[i].Local;
      cdsRastreiocSituacao.Value := ACBrSedex1.retRastreio[i].Situacao;
      cdsRastreiocObs.Value      := ACBrSedex1.retRastreio[i].Observacao;
      cdsRastreio.Post;
  end;
end;

initialization
    RegisterClass(TfrmPedidoVendaWeb);

end.
