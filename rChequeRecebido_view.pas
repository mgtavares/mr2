unit rChequeRecebido_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptChequeRecebido_view = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    lblFiltro: TQRLabel;
    QRShape2: TQRShape;
    QRLabel10: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRLabel12: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText6: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText4: TQRDBText;
    usp_RelatorionCdCheque: TIntegerField;
    usp_RelatorionCdEmpresa: TStringField;
    usp_RelatoriocNmContaBancaria: TStringField;
    usp_RelatorioiNrCheque: TIntegerField;
    usp_RelatorionValCheque: TFloatField;
    usp_RelatoriodDtDeposito: TDateTimeField;
    usp_RelatoriodDtEmissao: TDateTimeField;
    usp_RelatoriocFlgCompensado: TStringField;
    usp_RelatorionCdLanctoFin: TIntegerField;
    usp_RelatorionCdTerceiro: TIntegerField;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatoriocNmTabStatusCheque: TStringField;
    QRLabel6: TQRLabel;
    QRDBText8: TQRDBText;
    QRLabel14: TQRLabel;
    usp_RelatorionCdBanco: TStringField;
    QRLabel16: TQRLabel;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptChequeRecebido_view: TrptChequeRecebido_view;

implementation

{$R *.dfm}

end.
