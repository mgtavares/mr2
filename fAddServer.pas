unit fAddServer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, StdCtrls, cxLookAndFeelPainters, cxButtons;

type
  TfrmAddServer = class(TForm)
    Image1: TImage;
    edtDescricao: TEdit;
    edtIP: TEdit;
    edtInstancia: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    btAdd: TcxButton;
    cxButton1: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btAddClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    iIdConn : Integer ;
  end;

var
  frmAddServer: TfrmAddServer;

implementation

uses fMenu, inifiles;

{$R *.dfm}

procedure TfrmAddServer.FormCreate(Sender: TObject);
var
    i : Integer;
begin

  Screen.OnActiveControlChange := frmMenu.ScreenActiveControlChange;

  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TEdit) then
    begin
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      (Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 15 ;
      (Components [I] as TEdit).Font.Name   := 'Tahoma' ;
      (Components [I] as TEdit).Font.Size   := 7 ;
      (Components [I] as TEdit).Font.Style  := [] ;
      (Components [I] as TEdit).OnKeyPress  := frmMenu.NavegaEnter ;

    end;

    if (Components [I] is TLabel) then
    begin
       (Components [I] as TLabel).Font.Name    := 'Tahoma' ;
       (Components [I] as TLabel).Font.Size    := 8 ;
       (Components [I] as TLabel).Font.Style   := [] ;
       (Components [I] as TLabel).Font.Color   := clBlack ;
       (Components [I] as TLabel).Transparent  := True ;
       (Components [I] as TLabel).BringToFront;
    end ;

  end ;

end;

procedure TfrmAddServer.FormShow(Sender: TObject);
begin

    edtInstancia.Text := 'ADMSoft' ;
    edtDescricao.SetFocus ;

end;

procedure TfrmAddServer.btAddClick(Sender: TObject);
var
  arquivo : Tinifile;
  cDiretorio : string;
begin

    if (Trim(edtDescricao.Text) = '') then
    begin
        ShowMessage('Informe a descri��o da conex�o.') ;
        edtDescricao.SetFocus ;
        exit ;
    end ;

    if (Trim(edtIP.Text) = '') then
    begin
        ShowMessage('Informe o endere�o IP da conex�o.') ;
        edtIP.SetFocus ;
        exit ;
    end ;

    if (Trim(edtInstancia.Text) = '') then
    begin
        edtInstancia.Text := 'ADMSoft';
    end ;

    cDiretorio := extractfilepath(application.exename);
    arquivo   := Tinifile.Create(cDiretorio + 'Servidores.ini');

    Arquivo.WriteString(intToStr(iIdConn),'D', edtDescricao.Text);
    Arquivo.WriteString(intToStr(iIdConn),'I', edtIP.Text);
    Arquivo.WriteString(intToStr(iIdConn),'N', edtInstancia.Text);
    Arquivo.Free;

    close ;
end;

procedure TfrmAddServer.cxButton1Click(Sender: TObject);
begin
    close ;
end;

end.
