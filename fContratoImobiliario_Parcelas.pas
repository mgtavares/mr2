unit fContratoImobiliario_Parcelas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB;

type
  TfrmContratoImobiliario_Parcelas = class(TfrmProcesso_Padrao)
    qryParcelas: TADOQuery;
    dsParcelas: TDataSource;
    qryParcelasnCdParcContratoEmpImobiliario: TAutoIncField;
    qryParcelasnCdContratoEmpImobiliario: TIntegerField;
    qryParcelasnCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryParcelascNmTabTipoParcContratoEmpImobiliario: TStringField;
    qryParcelasiParcela: TIntegerField;
    qryParcelasiParcelaTotal: TIntegerField;
    qryParcelasdDtVencto: TDateTimeField;
    qryParcelasiDiasAtraso: TIntegerField;
    qryParcelascParcelaChaves: TStringField;
    qryParcelasnValorParcelaOriginal: TBCDField;
    qryParcelasnValMulta: TBCDField;
    qryParcelasnValJuro: TBCDField;
    qryParcelasnValDesconto: TBCDField;
    qryParcelasnValAbatimento: TBCDField;
    qryParcelasnSaldoTit: TBCDField;
    qryParcelasnValLiq: TBCDField;
    qryParcelasdDtLiq: TDateTimeField;
    DBGridEh1: TDBGridEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmContratoImobiliario_Parcelas: TfrmContratoImobiliario_Parcelas;

implementation

{$R *.dfm}

end.
