inherited frmContratoImobiliario_Parcelas: TfrmContratoImobiliario_Parcelas
  BorderIcons = [biSystemMenu]
  Caption = 'Parcelas'
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    Align = alClient
    DataSource = dsParcelas
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdParcContratoEmpImobiliario'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdContratoEmpImobiliario'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmTabTipoParcContratoEmpImobiliario'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iParcela'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iParcelaTotal'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtVencto'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iDiasAtraso'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cParcelaChaves'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValorParcelaOriginal'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValMulta'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValJuro'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValDesconto'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValAbatimento'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoTit'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValLiq'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtLiq'
        Footers = <>
      end>
  end
  object qryParcelas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      ''
      'SELECT Parc.nCdParcContratoEmpImobiliario '
      '      ,nCdContratoEmpImobiliario '
      
        '      ,TabTipoParcContratoEmpImobiliario.nCdTabTipoParcContratoE' +
        'mpImobiliario '
      
        '      ,TabTipoParcContratoEmpImobiliario.cNmTabTipoParcContratoE' +
        'mpImobiliario'
      '      ,Parc.iParcela    '
      '      ,iParcelaTotal '
      '      ,dDtVencto     '
      
        '      ,CASE WHEN dDtLiq IS NULL AND dDtVencto < dbo.fn_OnlyDate(' +
        'GetDate()) THEN (Convert(int,(dbo.fn_OnlyDate(GetDate()) - dDtVe' +
        'ncto)))'
      '            ELSE NULL'
      '       END iDiasAtraso'
      '      ,CASE WHEN Tit.cFlgCartaoConciliado = 1 THEN '#39'Sim'#39
      '            ELSE NULL'
      '       END cParcelaChaves'
      '      ,nValorParcela as nValorParcelaOriginal'
      '      ,Tit.nValMulta'
      '      ,Tit.nValJuro'
      '      ,Tit.nValDesconto'
      '      ,Tit.nValAbatimento'
      '      ,Tit.nSaldoTit'
      '      ,Tit.nValLiq'
      '      ,Tit.dDtLiq'
      '  FROM ParcContratoEmpImobiliario Parc'
      
        '       LEFT JOIN TabTipoParcContratoEmpImobiliario ON TabTipoPar' +
        'cContratoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario = P' +
        'arc.nCdTabTipoParcContratoEmpImobiliario '
      
        '       LEFT JOIN Titulo Tit                        ON Tit.nCdPar' +
        'cContratoEmpImobiliario                                      = P' +
        'arc.nCdParcContratoEmpImobiliario'
      ' WHERE nCdContratoEmpImobiliario = :nPK'
      ' ORDER BY dDtVencto')
    Left = 216
    Top = 136
    object qryParcelasnCdParcContratoEmpImobiliario: TAutoIncField
      FieldName = 'nCdParcContratoEmpImobiliario'
      ReadOnly = True
    end
    object qryParcelasnCdContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdContratoEmpImobiliario'
    end
    object qryParcelasnCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryParcelascNmTabTipoParcContratoEmpImobiliario: TStringField
      FieldName = 'cNmTabTipoParcContratoEmpImobiliario'
      Size = 50
    end
    object qryParcelasiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryParcelasiParcelaTotal: TIntegerField
      FieldName = 'iParcelaTotal'
    end
    object qryParcelasdDtVencto: TDateTimeField
      FieldName = 'dDtVencto'
    end
    object qryParcelasiDiasAtraso: TIntegerField
      FieldName = 'iDiasAtraso'
      ReadOnly = True
    end
    object qryParcelascParcelaChaves: TStringField
      FieldName = 'cParcelaChaves'
      ReadOnly = True
      Size = 3
    end
    object qryParcelasnValorParcelaOriginal: TBCDField
      FieldName = 'nValorParcelaOriginal'
      Precision = 12
      Size = 2
    end
    object qryParcelasnValMulta: TBCDField
      FieldName = 'nValMulta'
      Precision = 12
      Size = 2
    end
    object qryParcelasnValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryParcelasnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryParcelasnValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object qryParcelasnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryParcelasnValLiq: TBCDField
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryParcelasdDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
  end
  object dsParcelas: TDataSource
    DataSet = qryParcelas
    Left = 256
    Top = 136
  end
end
