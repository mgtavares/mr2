unit fTransfEst;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, DBClient, DBGridEhGrouping, fTransfEst_Itens,
  ToolCtrlsEh, cxContainer, cxEdit, cxTextEdit, Menus;

type
  TfrmTransfEst = class(TfrmCadastro_Padrao)
    qryMasternCdTransfEst: TIntegerField;
    qryMasternCdEmpresaOrigem: TIntegerField;
    qryMasternCdLojaOrigem: TIntegerField;
    qryMasternCdLocalEstoqueOrigem: TIntegerField;
    qryMasternCdTabTipoTransf: TIntegerField;
    qryMasternCdEmpresaDestino: TIntegerField;
    qryMasternCdLojaDestino: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdLocalEstoqueDestino: TIntegerField;
    qryMasterdDtCad: TDateTimeField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasterdDtFinalizacao: TDateTimeField;
    qryMasternCdUsuarioFinal: TIntegerField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternCdUsuarioCancel: TIntegerField;
    qryMasterdDtConfirmacao: TDateTimeField;
    qryEmpresaOrigem: TADOQuery;
    qryEmpresaOrigemnCdEmpresa: TIntegerField;
    qryEmpresaOrigemcNmEmpresa: TStringField;
    qryEmpresaDestino: TADOQuery;
    qryEmpresaDestinonCdEmpresa: TIntegerField;
    qryEmpresaDestinocNmEmpresa: TStringField;
    qryLojaOrigem: TADOQuery;
    qryLojaOrigemnCdLoja: TAutoIncField;
    qryLojaOrigemcNmLoja: TStringField;
    qryLojaDestino: TADOQuery;
    qryLojaDestinonCdLoja: TAutoIncField;
    qryLojaDestinocNmLoja: TStringField;
    qryLocalEstoqueOrigem: TADOQuery;
    qryLocalEstoqueOrigemnCdLocalEstoque: TIntegerField;
    qryLocalEstoqueOrigemcNmLocalEstoque: TStringField;
    qryLocalEstoqueDestino: TADOQuery;
    qryLocalEstoqueDestinonCdLocalEstoque: TIntegerField;
    qryLocalEstoqueDestinocNmLocalEstoque: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    GroupBox1: TGroupBox;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit16: TDBEdit;
    dsEmpresaOrigem: TDataSource;
    DBEdit17: TDBEdit;
    dsLojaOrigem: TDataSource;
    DBEdit18: TDBEdit;
    dsLocalEstoqueOrigem: TDataSource;
    GroupBox2: TGroupBox;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit19: TDBEdit;
    dsEmpresaDestino: TDataSource;
    DBEdit20: TDBEdit;
    dsLojaDestino: TDataSource;
    DBEdit21: TDBEdit;
    dsLocalEstoqueDestino: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit22: TDBEdit;
    dsTerceiro: TDataSource;
    qryAux: TADOQuery;
    qryItem: TADOQuery;
    qryItemnQtde: TBCDField;
    qryItemnValUnitario: TBCDField;
    qryItemnValTotal: TBCDField;
    qryItemcNmItem: TStringField;
    qryItemnCdProduto: TIntegerField;
    dsItem: TDataSource;
    qryItemnCdTransfEst: TIntegerField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryItemnCdItemTransfEst: TAutoIncField;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    qryProdutoEAN: TADOQuery;
    qryProdutoEANnCdProduto: TIntegerField;
    qryProdutoEANnValCusto: TBCDField;
    qryProdutonValCusto: TBCDField;
    qryProdutoEANcNmProduto: TStringField;
    SP_PROCESSA_TRANSFERENCIA_ESTOQUE: TADOStoredProc;
    qryMasternCdUsuarioConfirm: TIntegerField;
    qryItemnQtdeConf: TBCDField;
    qryTrataProduto: TADOQuery;
    qryPreparaTemp: TADOQuery;
    qryTempNaoLidos: TADOQuery;
    qryTempNaoLidosiPosicao: TIntegerField;
    qryTempNaoLidoscCodigo: TStringField;
    dsTempNaoLidos: TDataSource;
    qryMasternValTransferencia: TBCDField;
    qryMastercFlgModoManual: TIntegerField;
    qryItemnQtdeDiv: TBCDField;
    qryMastercFlgAutomaticaCD: TIntegerField;
    qryPedidoCD: TADOQuery;
    qryPedidoCDnCdPedido: TIntegerField;
    DBEdit9: TDBEdit;
    Label9: TLabel;
    Edit1: TEdit;
    DBEdit11: TDBEdit;
    Edit2: TEdit;
    DBEdit15: TDBEdit;
    Label15: TLabel;
    Edit4: TEdit;
    Label11: TLabel;
    DBEdit13: TDBEdit;
    Edit3: TEdit;
    Label13: TLabel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    Panel1: TPanel;
    Label5: TLabel;
    cxTextEdit3: TcxTextEdit;
    cxTextEdit4: TcxTextEdit;
    cxButton8: TcxButton;
    btnConferencia: TcxButton;
    btnVisualizaConfe: TcxButton;
    Panel2: TPanel;
    btExcluirItens: TcxButton;
    cxTabSheet2: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    btFinalizar: TToolButton;
    ToolButton10: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    PopupMenu1: TPopupMenu;
    btProtocolo: TMenuItem;
    EmitirEtiqueta1: TMenuItem;
    qryMastercFlgFaturaTransfEst: TIntegerField;
    btAddItem: TcxButton;
    qryProdutoSel: TADOQuery;
    qryProdutoSelcFlgAux: TIntegerField;
    qryProdutoSelnCdProduto: TIntegerField;
    qryProdutoSelcReferencia: TStringField;
    qryProdutoSelcNmProduto: TStringField;
    qryProdutoSelnValCusto: TBCDField;
    qryProdutoSelnValVenda: TBCDField;
    qryProdutoSelnMarkUp: TBCDField;
    qryProdutoSeldDtUltVenda: TDateTimeField;
    qryProdutoSeldDtUltReceb: TDateTimeField;
    qryProdutoSeliDiasEstoque: TIntegerField;
    qryProdutoSelnQtdeEstoque: TBCDField;
    qryProdutoSelcNmDepartamento: TStringField;
    qryProdutoSelcNmCategoria: TStringField;
    qryProdutoSelcNmSubCategoria: TStringField;
    qryProdutoSelcNmSegmento: TStringField;
    qryProdutoSelcNmLinha: TStringField;
    qryProdutoSelcNmMarca: TStringField;
    qryProdutoSelcNmGrupoProduto: TStringField;
    qryProdutoSelcNmClasseProduto: TStringField;
    qryProdutoSelcNmStatus: TStringField;
    qryIncluiProdutos: TADOQuery;
    btConferir: TcxButton;
    qryPopulaConfPadrao: TADOQuery;
    qryPopulaConfPadraonCdProduto: TIntegerField;
    qryPopulaConfPadraocNmProduto: TStringField;
    qryPopulaConfPadraocEAN: TStringField;
    qryPopulaConfPadraocEANFornec: TStringField;
    qryGravaConfPadrao: TADOQuery;
    btLeitura: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GroupBox2Enter(Sender: TObject);
    procedure qryItemBeforePost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryItemCalcFields(DataSet: TDataSet);
    procedure DBEdit7Exit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DBRadioGroup1Change(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBGridEh2Enter(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure btLeitura_bkClick(Sender: TObject);
    procedure TrataProdutos();
    procedure btConferir_bkClick(Sender: TObject);
    procedure btExcluirItensClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure qryItemBeforeDelete(DataSet: TDataSet);
    procedure btFinalizarClick(Sender: TObject);
    procedure Protocolo1Click(Sender: TObject);
    procedure EmitirEtiqueta1Click(Sender: TObject);
    procedure btAddItemClick(Sender: TObject);
    procedure btConferirClick(Sender: TObject);
    procedure btLeituraClick(Sender: TObject);
  private
    { Private declarations }
    objfrmTransfEst_Itens : TfrmTransfEst_Itens;
  public
    { Public declarations }
    cVarejo          : String ;
    nCdTransfEst     : integer;
    nCdEmpresaOrigem : integer;
  end;

var
  frmTransfEst: TfrmTransfEst;

implementation

uses fMenu, fLookup_Padrao, rTransferencia_view, fTransfEst_ItensNaoLidos,
     fModImpETP, fCampanhaPromoc_IncluiProduto, fConferenciaProdutoPadrao;

{$R *.dfm}

procedure TfrmTransfEst.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TRANSFEST' ;
  nCdTabelaSistema  := 52 ;
  nCdConsultaPadrao := 117 ;
  bLimpaAposSalvar  := false ;
end;

procedure TfrmTransfEst.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBRadioGroup1.Enabled := True ;
  DBRadioGroup1.SetFocus;
  DBRadioGroup1.ItemIndex := 0 ;

  qryMasternCdEmpresaOrigem.Value  := frmMenu.nCdEmpresaAtiva ;
  qryMasternCdEmpresaDestino.Value := frmMenu.nCdEmpresaAtiva ;

  PosicionaQuery(qryEmpresaOrigem, IntToStr(frmMenu.nCdEmpresaAtiva)) ;
  PosicionaQuery(qryEmpresaDestino, IntToStr(frmMenu.nCdEmpresaAtiva)) ;

  if (dbEdit3.Enabled) then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT nCdLoja FROM UsuarioLoja WHERE nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado)) ;
      qryAux.Open ;

      if (qryAux.RecordCount = 1) then
      begin
          PosicionaQuery(qryLojaOrigem, qryAux.FieldList[0].AsString) ;
          qryMasternCdLojaOrigem.Value := qryLojaOrigemnCdLoja.Value ;
          DbEdit4.SetFocus ;
          DbEdit3.Enabled := False ;
      end ;

      qryAux.Close ;
  end
  else begin
      DbEdit4.SetFocus ;
  end ;

  DBGridEh1.ReadOnly  := False ;
  DBGridEh2.ReadOnly  := False ;

end;

procedure TfrmTransfEst.FormShow(Sender: TObject);
begin
  inherited;

  if (nCdTransfEst <> 0) and (nCdEmpresaOrigem <> 0) then
  begin

      qryMaster.Close;
      qryMaster.Parameters.ParamByName('nPK').Value        := nCdTransfEst;
      qryMaster.Parameters.ParamByName('nCdEmpresa').Value := nCdEmpresaOrigem;
      qryMaster.Open;

  end;


  cVarejo := frmMenu.LeParametro('VAREJO') ;

  if (cVarejo <> 'S') then
  begin
      DbEdit3.Enabled := False ;
      DbEdit6.Enabled := False ;
  end ;

  cxTabSheet2.Enabled := True ;

  if (frmMenu.LeParametro('VERVALTRANSF') = 'N') then
  begin
      cxTabSheet2.Enabled := False ;
      {rptTransferencia_view.QRDBText2.Enabled := false ;
      rptTransferencia_view.QRLabel16.Enabled := false ;}

  end ;

  Edit1.Enabled := False ;
  Edit2.Enabled := False ;
  Edit3.Enabled := False ;

  DbEdit5.Enabled := False ;
  DbEdit6.Enabled := False ;
  DbEdit7.Enabled := False ;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;

  qryLojaOrigem.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryLojaOrigem.Parameters.ParamByName('nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
  qryLojaDestino.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmTransfEst.GroupBox2Enter(Sender: TObject);
begin
  inherited;
{  DbEdit7.Enabled := False ;
  DbEdit6.Enabled := False ;

  qryLojaOrigem.Close ;
  qryLojaDestino.Close ;
  qryLocalEstoqueOrigem.Close ;
  qryLocalEstoqueDestino.Close ;

  dbEdit3.Text := '' ;
  dbEdit4.Text := '' ;
  DbEdit6.Text := '' ;
  dbEdit7.Text := '' ;
  dbEdit8.Text := '' ;

  if (dbRadioGroup1.ItemIndex = 0) then
  begin

      qryMasternCdEmpresaOrigem.Value  := frmMenu.nCdEmpresaAtiva ;
      qryMasternCdEmpresaDestino.Value := frmMenu.nCdEmpresaAtiva ;

      DbEdit19.Text := DbEdit16.Text ;

      DbEdit5.Enabled := False ;
      DbEdit6.Enabled := False ;
      DbEdit7.Enabled := False ;

      if (dbedit3.Text <> '') then
      begin
          qryMasternCdLojaDestino.Value := qryMasternCdLojaOrigem.Value ;
          PosicionaQuery(qryLojaDestino, qryMasternCdLojaDestino.AsString) ;
      end ;

  end ;

  if (dbRadioGroup1.ItemIndex = 1) then
  begin

      DbEdit5.Enabled := True ;

      if (cVarejo = 'S') then
          DbEdit6.Enabled := True ;

      DbEdit7.Enabled := False ;

  end ;

  if (dbRadioGroup1.ItemIndex = 2) then
  begin

      DbEdit5.Enabled := True ;

      if (cVarejo = 'S') then
          DbEdit6.Enabled := True ;

      DbEdit7.Enabled := True ;

  end ;    }

end;

procedure TfrmTransfEst.qryItemBeforePost(DataSet: TDataSet);
begin

  if (qryItemnQtde.Value <= 0) then
  begin
      MensagemAlerta('Valor inv�lido.') ;
      abort ;
  end ;

  qryItemnValTotal.Value    := qryItemnValUnitario.Value * qryItemnQtde.Value ;

  if (qryItemnValTotal.Value < 0) then
  begin
      MensagemAlerta('Valor inv�lido.') ;
      abort ;
  end ;

  if (qryMasternCdTabTipoTransf.Value = 3) and (qryItemnValTotal.Value = 0) then
  begin
      MensagemAlerta('Para remessa de consigna��o � necess�rio digita��o de valores.') ;
      abort ;
  end ;

  inherited;

  qryItemnCdTransfEst.Value := qryMasternCdTransfEst.Value ;

  if (qryItem.State = dsInsert) then
      qryItemnCdItemTransfEst.Value := frmMenu.fnProximoCodigo('ITEMTRANSFEST') ;

end;

procedure TfrmTransfEst.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInactive) then
      Abort;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post;

  qryItem.Close;
  PosicionaQuery(qryItem, qryMasternCdTransfEst.AsString);
end;

procedure TfrmTransfEst.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  DBEdit3.ReadOnly       := False ;
  btLeitura.Enabled      := True ;
  btConferir.Enabled     := True ;
  btExcluirItens.Enabled := True ;
  btAddItem.Enabled      := True ;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdLojaOrigem.Value > 0) then
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ' AND UL.nCdLoja = ' + qryMasternCdLojaOrigem.AsString) ;
      qryAux.Open ;

      if (qryAux.Eof) then
      begin
          qryAux.Close ;
          MensagemAlerta('Voc� n�o tem permiss�o para ver transfer�ncias desta loja.') ;
          btCancelar.Click;
          exit ;
      end ;

      qryAux.Close ;

  end ;

  if qryMastercFlgAutomaticaCD.Value = 1 then
  begin
      DBRadioGroup1.Enabled  := False;
      btLeitura.Enabled      := False;
      btExcluirItens.Enabled := False;
      btAddItem.Enabled      := False;
  end
  else
  begin
      DBRadioGroup1.Enabled := True;
      btLeitura.Enabled      := False;
      btExcluirItens.Enabled := False;
  end;

  PosicionaQuery(qryEmpresaOrigem, qryMasternCdEmpresaOrigem.asString) ;
  PosicionaQuery(qryEmpresaDestino, qryMasternCdEmpresaDestino.asString) ;

  PosicionaQuery(qryLojaOrigem, qryMasternCdLojaOrigem.AsString) ;
  PosicionaQuery(qryLojaDestino, qryMasternCdLojaDestino.asString) ;

  qryLocalEstoqueOrigem.Close ;
  qryLocalEstoqueDestino.Close ;

  qryLocalEstoqueOrigem.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLojaOrigem.Value ;
  qryLocalEstoqueOrigem.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresaOrigem.Value ;
  PosicionaQuery(qryLocalEstoqueOrigem, qryMasternCdLocalEstoqueOrigem.AsString) ;

  qryLocalEstoqueDestino.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLojaDestino.Value ;
  qryLocalEstoqueDestino.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresaDestino.Value ;
  qryLocalEstoqueDestino.Parameters.ParamByName('nCdTerceiro').Value:= qryMasternCdTerceiro.Value ;
  PosicionaQuery(qryLocalEstoqueDestino, qryMasternCdLocalEstoqueDestino.AsString) ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  PosicionaQuery(qryItem, qryMasternCdTransfEst.AsString) ;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuarioCad.AsString) ;

  Edit1.Text := '' ;
  Edit2.Text := '' ;
  Edit3.Text := '' ;
  Edit4.Text := '' ;

  if not qryUsuario.eof then
      Edit1.Text := qryUsuariocNmUsuario.Value ;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuarioFinal.AsString) ;

  if not qryUsuario.eof then
      Edit2.Text := qryUsuariocNmUsuario.Value ;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuarioCancel.AsString) ;

  if not qryUsuario.eof then
      Edit3.Text := qryUsuariocNmUsuario.Value ;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuarioConfirm.AsString) ;

  if not qryUsuario.eof then
      Edit4.Text := qryUsuariocNmUsuario.Value ;

  DBGridEh1.ReadOnly  := False ;
  DBGridEh2.ReadOnly  := False ;

  if (qryMasterdDtFinalizacao.AsString <> '') then
  begin
      DBGridEh1.ReadOnly     := True;
      DBGridEh2.ReadOnly     := True;
      btLeitura.Enabled      := False;
      btConferir.Enabled     := False;
      btExcluirItens.Enabled := False;
      btAddItem.Enabled      := False;
  end;

end;

procedure TfrmTransfEst.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  Edit1.Text := '' ;
  Edit2.Text := '' ;
  Edit3.Text := '' ;
  EDit4.Text := '' ;

  qryTerceiro.Close ;
  qryEmpresaOrigem.Close ;
  qryEmpresaDestino.Close ;
  qryLojaOrigem.Close ;
  qryLojaDestino.Close ;
  qryLocalEstoqueOrigem.Close ;
  qryLocalEstoqueDestino.Close ;

  qryItem.Close ;

end;

procedure TfrmTransfEst.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (dbEdit18.Text = '') then
  begin
      MensagemAlerta('Informe o estoque de origem.') ;
      dbEdit4.SetFocus ;
      abort ;
  end ;

  if (dbEdit21.Text = '') and (DBRadioGroup1.ItemIndex <> 2) then
  begin
      MensagemAlerta('Informe o estoque de destino.') ;
      dbEdit8.SetFocus ;
      abort ;
  end ;

  if (qryLocalEstoqueOrigemnCdLocalEstoque.Value = qryLocalEstoqueDestinonCdLocalEstoque.Value) then
  begin
      MensagemAlerta('Os estoques de origem e destino devem ser diferentes.') ;
      dbEdit4.SetFocus ;
      abort ;
  end ;

  inherited;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasterdDtCad.Value            := Now() ;
      qryMasternCdUsuarioCad.Value     := frmMenu.nCdUsuarioLogado;
      qryMastercFlgModoManual.Value    := 0;
      qryMastercFlgAutomaticaCD.Value  := 0;

      if (DBRadioGroup1.ItemIndex = 3) then
          qryMastercFlgFaturaTransfEst.Value := 1;
  end;

  if ((qryMaster.State = dsEdit) and (qryMastercFlgAutomaticaCD.Value <> 1)) then
  begin
      if (DBRadioGroup1.ItemIndex = 3) then
          qryMastercFlgFaturaTransfEst.Value := 1
      else
          qryMastercFlgFaturaTransfEst.Value := 0;
  end;

  if (not qryItem.IsEmpty) then
      qryMasternValTransferencia.Value := StrToFloat(DBGridEh2.FieldColumns['nValTotal'].Footer.SumValue);
end;

procedure TfrmTransfEst.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (cVarejo = 'S') then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(87,'Loja.nCdLoja = ' + DbEdit3.Text);
            end
            else begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + DbEdit2.Text);
            end ;

            If (nPK > 0) then
            begin
                qryMasternCdLocalEstoqueOrigem.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfEst.DBEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
    cWhere : String ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            If (dbEdit5.Text = '') then
            begin
                MensagemAlerta('Informe a empresa de destino.') ;
                DbEdit5.SetFocus ;
                exit ;
            end ;

            If (dbEdit6.Text = '') and (cVarejo = 'S') then
            begin
                MensagemAlerta('Informe a loja de destino.') ;
                DbEdit6.SetFocus ;
                exit ;
            end ;

            if (DbRadioGroup1.ItemIndex = 2) and (dbEdit7.Text = '') then
            begin
                MensagemAlerta('Informe o Terceiro de Destino.') ;
                DbEdit7.SetFocus ;
                exit ;
            end ;

            cWhere := ' ' ;
            if (DbEdit5.Text <> '') then cWhere := cWhere + 'Empresa.nCdEmpresa = ' + dbEdit5.Text ;
            If (dbEdit6.Text <> '') then cWhere := cWhere + ' AND Loja.nCdLoja = ' + dbEdit6.Text  ;
            if (dbEdit7.Text <> '') then cWhere := cWhere + ' AND LocalEstoque.nCdTerceiro = ' + dbEdit7.Text ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(87,cWhere);

            If (nPK > 0) then
            begin
                qryMasternCdLocalEstoqueDestino.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfEst.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            If (dbEdit5.Text = '') then
            begin
                MensagemAlerta('Informe a empresa de destino.') ;
                DbEdit5.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(86,'Loja.nCdEmpresa = ' + dbEdit5.Text);

            If (nPK > 0) then
            begin
                qryMasternCdLojaDestino.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfEst.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(25);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresaDestino.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfEst.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  qryLocalEstoqueOrigem.Close ;

  qryLocalEstoqueOrigem.Parameters.ParamByName('nCdEmpresa').Value := DbEdit2.text ;

  if (dbEdit3.Enabled) then
  begin
      if (dbEdit3.Text = '') then
      begin
          MensagemAlerta('Informe a loja de origem.') ;

          if (DBEdit3.Visible) then
               DbEdit3.SetFocus;
              
          exit ;
      end ;

      qryLocalEstoqueOrigem.Parameters.ParamByName('nCdLoja').Value := frmMenu.ConvInteiro(DbEdit3.text) ;
  end
  else begin
      qryLocalEstoqueOrigem.Parameters.ParamByName('nCdLoja').Value := 0 ;
  end ;

  PosicionaQuery(qryLocalEstoqueOrigem, DbEdit4.Text) ;

end;

procedure TfrmTransfEst.DBEdit8Exit(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  qryLocalEstoqueDestino.Close ;

  qryLocalEstoqueDestino.Parameters.ParamByName('nCdEmpresa').Value := DbEdit5.text ;

  if (dbEdit6.Enabled) then
  begin
      if (dbEdit6.Text = '') then
      begin
          MensagemAlerta('Informe a loja de destino.') ;

          if (DBEdit6.Visible) then
               DbEdit6.SetFocus;

          exit ;
      end ;

      qryLocalEstoqueDestino.Parameters.ParamByName('nCdLoja').Value := frmMenu.ConvInteiro(DbEdit6.text) ;
  end
  else begin
      qryLocalEstoqueDestino.Parameters.ParamByName('nCdLoja').Value := 0 ;
  end ;

  if (dbEdit7.Enabled) and (DBEdit7.Visible) and (qryMasternCdTabTipoTransf.Value = 3) then
  begin
      if (dbEdit7.Text = '') then
      begin
          MensagemAlerta('Informe o terceiro de destino.') ;
          DbEdit7.SetFocus;
          exit ;
      end ;

      qryLocalEstoqueDestino.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(DbEdit7.text) ;
  end
  else begin
      qryLocalEstoqueDestino.Parameters.ParamByName('nCdTerceiro').Value := 0 ;
  end ;

  PosicionaQuery(qryLocalEstoqueDestino, DbEdit8.Text) ;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

end;

procedure TfrmTransfEst.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(17,'EXISTS(SELECT 1 FROM LocalEstoque WHERE LocalEstoque.nCdTerceiro = vTerceiros.nCdTerceiro)');

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfEst.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Refresh;
  {qryItem.Close;
  PosicionaQuery(qryItem, qryMasternCdTransfEst.AsString);}
end;

procedure TfrmTransfEst.qryItemCalcFields(DataSet: TDataSet);
begin
 {-- inherited; --}

  try

      qryProduto.Close;
      PosicionaQuery(qryProduto, qryItemnCdProduto.AsString) ;

      if not qryProduto.eof then
      begin
          if (qryItemcNmItem.Value = '') then
              qryItemcNmItem.Value      := qryProdutocNmProduto.Value ;

          if (qryItem.State = dsInsert) and (qryProdutonValCusto.Value > 0) and (qryItemnValUnitario.Value = 0) then
              qryItemnValUnitario.Value := qryProdutonValCusto.Value ;

      end ;

      qryItemnQtdeDiv.Value := qryItemnQtde.Value - qryItemnQtdeConf.Value ;
  except
      //nothing;
  end;

end;

procedure TfrmTransfEst.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  Posicionaquery(qryTerceiro, DbEdit7.Text) ;

end;

procedure TfrmTransfEst.FormActivate(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePage := cxTabSheet1 ;

end;

procedure TfrmTransfEst.DBRadioGroup1Change(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if DBEdit11.Text <> '' then
      exit ;

  DBEdit4.Enabled := True ;
  DbEdit5.Enabled := True ;

  DbEdit7.Enabled := False ;
  DbEdit6.Enabled := False ;

  btLeitura.Enabled      := True;
  btExcluirItens.Enabled := True;

  dbEdit3.Text := '' ;
  dbEdit4.Text := '' ;
  DbEdit6.Text := '' ;
  dbEdit7.Text := '' ;
  dbEdit8.Text := '' ;

  DBEdit3.ReadOnly := False ;

  if (qryMaster.State <> dsBrowse) then
  begin

      qryLojaOrigem.Close ;
      qryLojaDestino.Close ;
      qryLocalEstoqueOrigem.Close ;
      qryLocalEstoqueDestino.Close ;

      if (cVarejo = 'S') then
      begin
          PosicionaQuery(qryLojaOrigem, IntToStr(frmMenu.nCdLojaAtiva)) ;
          qryMasternCdLojaOrigem.Value := frmMenu.nCdLojaAtiva;
          DBEdit3.ReadOnly := True ;
      End ;

  end ;


  if (dbRadioGroup1.ItemIndex = 0) then
  begin

      Label17.Caption  := 'Loja' ;
      DBEdit6.Visible  := True ;
      DBEdit20.Visible := True ;
      DBEdit7.Visible  := False ;
      DBEdit22.Visible := False ;

      if (qryMaster.State <> dsBrowse) then
      begin

          qryMasternCdEmpresaOrigem.Value  := frmMenu.nCdEmpresaAtiva ;
          qryMasternCdEmpresaDestino.Value := frmMenu.nCdEmpresaAtiva ;

          if (cVarejo = 'S') then
          begin
              PosicionaQuery(qryLojaDestino, IntToStr(frmMenu.nCdLojaAtiva)) ;
              qryMasternCdLojaDestino.Value := frmMenu.nCdLojaAtiva;
          end ;

      end ;

      DbEdit19.Text := DbEdit16.Text ;

      DbEdit5.Enabled := False ;
      DbEdit6.Enabled := False ;
      DbEdit7.Enabled := False ;
      DBEdit8.Enabled := True  ;

      if (DBEdit3.Enabled) then
          DBEdit3.SetFocus ;

  end ;

  if (dbRadioGroup1.ItemIndex = 1) or (DBRadioGroup1.ItemIndex = 3) then
  begin

      Label17.Caption  := 'Loja' ;
      DBEdit6.Visible  := True ;
      DBEdit20.Visible := True ;
      DBEdit7.Visible  := False ;
      DBEdit22.Visible := False ;

      if qryMastercFlgAutomaticaCD.Value = 1 then
      begin
          DbEdit2.Enabled := False;
          DbEdit3.Enabled := False;
          DbEdit4.Enabled := False;
          DbEdit5.Enabled := False;
          DbEdit7.Enabled := False;
          DbEdit8.Enabled := False;
      end
      else
      begin
          DbEdit2.Enabled := True;
          DbEdit3.Enabled := True;
          DbEdit4.Enabled := True;
          DbEdit5.Enabled := True;
          DbEdit7.Enabled := True;
          DbEdit8.Enabled := True;
      end;

      if (cVarejo = 'S') and (qryMastercFlgAutomaticaCD.Value = 0) then
          DbEdit6.Enabled := True
      else
          DbEdit6.Enabled := False;

      if (qryMaster.State <> dsBrowse) then
      begin
          qryMasternCdLojaDestino.asString := '' ;
      end ;

      if (DBEdit3.Enabled) then
          DBEdit3.SetFocus ;
  end ;

  if (dbRadioGroup1.ItemIndex = 2) then
  begin

      Label17.Caption  := 'Terceiro' ;
      DBEdit6.Visible  := False ;
      DBEdit20.Visible := False ;
      DBEdit7.Visible  := True ;
      DBEdit22.Visible := True ;

      DbEdit5.Enabled := True ;

      if (cVarejo = 'S') then
          DbEdit6.Enabled := True ;

      DbEdit7.Enabled := True ;

      if (qryMaster.State <> dsBrowse) then
      begin

          qryMasternCdLojaDestino.asString := '' ;

      end ;

      if (DBEdit3.Enabled) then
          DBEdit3.SetFocus ;

  end ;


end;

procedure TfrmTransfEst.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryLojaDestino, DbEdit6.Text) ;

end;

procedure TfrmTransfEst.DBGridEh2Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

end;

procedure TfrmTransfEst.cxButton1Click(Sender: TObject);
begin

    if (qryMaster.Active) and (qryMasternCdTransfEst.Value > 0) then
        PosicionaQuery(qryItem, qryMasternCdTransfEst.AsString) ;

end;

procedure TfrmTransfEst.btLeitura_bkClick(Sender: TObject);
begin
    {
    if not (qryMaster.Active) then
        exit ;

    if (qryMasterdDtCancel.AsString <> '') then
    begin
        MensagemAlerta('Transfer�ncia cancelada n�o permite altera��es.');
        PosicionaPK(qryMasternCdTransfEst.Value);
        Exit;
    end;

    if (qryMastercFlgAutomaticaCD.Value = 1) then
    begin
        MensagemAlerta('Transfer�ncia gerada pelo centro de distribui��o n�o permite altera��es.');
        Exit;
    end;

    if (qryMasternCdTransfEst.Value = 0) then
    begin
        MensagemAlerta('Salve a transfer�ncia antes de iniciar a leitura.') ;
        exit ;
    end ;

    objfrmTransfEst_Itens := TfrmTransfEst_Itens.Create(nil);

    objfrmTransfEst_Itens.RadioGroup1.Enabled   := False ;
    objfrmTransfEst_Itens.RadioGroup1.ItemIndex := 0 ;
    objfrmTransfEst_Itens.Caption               := 'Itens da Transfer�ncia' ;

    showForm(objfrmTransfEst_Itens,false);

    case MessageDlg('Confirma a grava��o dos itens ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo:exit ;
    end ;

    TrataProdutos;
    }
end;

procedure TfrmTransfEst.TrataProdutos();
var
    iPosicao : integer ;
    formAux  : TfrmTransfEst_ItensNaoLidos;
begin

    qryPreparaTemp.Close ;
    qryPreparaTemp.ExecSQL ;

    objfrmTransfEst_Itens.cdsProduto.First ;

    frmMenu.Connection.BeginTrans;

    iPosicao := 1 ;

    while not objfrmTransfEst_Itens.cdsProduto.Eof do
    begin

      qryTrataProduto.Close ;
      qryTrataProduto.Parameters.ParamByName('nCdTransfEst').Value := qryMasternCdTransfEst.Value ;
      qryTrataProduto.Parameters.ParamByName('nQtde').Value        := objfrmTransfEst_Itens.cdsProdutonQtde.Value ;
      qryTrataProduto.Parameters.ParamByName('cCodigo').Value      := objfrmTransfEst_Itens.cdsProdutocCodigo.Value ;
      qryTrataProduto.Parameters.ParamByName('iPosicao').Value     := objfrmTransfEst_Itens.cdsProdutoiPosicao.Value ;

      if (objfrmTransfEst_Itens.RadioGroup1.ItemIndex = 0) then
          qryTrataProduto.Parameters.ParamByName('cModo').Value := 'D'
      else qryTrataProduto.Parameters.ParamByName('cModo').Value := 'C' ;

      try
          qryTrataProduto.ExecSQL;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      objfrmTransfEst_Itens.cdsProduto.Delete;

      objfrmTransfEst_Itens.cdsProduto.First;
      iPosicao := iPosicao+ 1 ;

    end ;

    qryAux.Close ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('DELETE FROM ItemTransfEst WHERE nCdTransfEst = ' + qryMasternCdTransfEst.AsString + ' AND (nQtde + nQtdeConf) = 0 ') ;

    try
        qryAux.ExecSQL;
    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento.') ;
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    objfrmTransfEst_Itens.Close ;

    PosicionaQuery(qryItem, qryMasternCdTransfEst.AsString) ;

    qryTempNaoLidos.Close ;
    qryTempNaoLidos.Open ;

    if not qryTempNaoLidos.Eof then
    begin

       MensagemAlerta('Alguns c�digo n�o foram encontrados. Clique em OK para ver a lista de c�digos.') ;

       formAux := TfrmTransfEst_ItensNaoLidos.Create(nil) ;
       try
         formAux.DataSource1.DataSet := qryTempNaoLidos;
         formAux.ShowModal;
       finally
         formAux.Free;
       end;

    end ;

    FreeAndNil(objfrmTransfEst_Itens);

end ;

procedure TfrmTransfEst.btConferir_bkClick(Sender: TObject);
begin
{
    if not (qryMaster.Active) then
        exit ;

    if (qryMasterdDtCancel.AsString <> '') then
    begin
        MensagemAlerta('Transfer�ncia cancelada n�o permite altera��es.');
        PosicionaPK(qryMasternCdTransfEst.Value);
        Exit;
    end;

    if (qryMasternCdTransfEst.Value = 0) then
    begin
        MensagemAlerta('Salve a transfer�ncia antes de iniciar a confer�ncia.') ;
        exit ;
    end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE ItemTransfEst Set nQtdeConf = 0 WHERE nCdTransfEst = ' + qryMasternCdTransfEst.AsString) ;

  frmMenu.Connection.BeginTrans;

  try
      qryAux.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  objfrmTransfEst_Itens := TfrmTransfEst_Itens.Create(nil);

  objfrmTransfEst_Itens.RadioGroup1.Enabled   := False ;
  objfrmTransfEst_Itens.RadioGroup1.ItemIndex := 1 ;

  showForm(objfrmTransfEst_Itens,false);

  case MessageDlg('Confirma a grava��o da confer�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  TrataProdutos;

    {qryTempNaoLidos.Close ;
    qryTempNaoLidos.Open ;

    if not qryTempNaoLidos.Eof then
    begin

        MensagemAlerta('Alguns c�digo n�o foram encontrados. Clique em OK para ver a lista de c�digos.') ;

        //qryItensqryTempNaoLidos.Clone(frmTransfEst_ItensNaoLidos.qryItens) ;
        //frmTransfEst_ItensNaoLidos.qryItens.Recordset := qryTempNaoLidos.Recordset;
        //frmTransfEst_ItensNaoLidos.DBGridEh1.DataSource := frmTransfEst.dsTempNaoLidos ;
        frmTransfEst_ItensNaoLidos.ShowModal;
    end ;
    
     }
end;

procedure TfrmTransfEst.btExcluirItensClick(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      exit ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Transfer�ncia cancelada n�o permite altera��es.');
      PosicionaPK(qryMasternCdTransfEst.Value);
      Exit;
  end;

  if (qryMastercFlgAutomaticaCD.Value = 1) then
  begin
      MensagemAlerta('Transfer�ncia gerada pelo centro de distribui��o n�o permite altera��es.');
      Exit;
  end;

  if (qryItem.Active) and (qryItem.RecordCount > 0) then
  begin

      case MessageDlg('Confirma a exclus�o dos itens desta transfer�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM ItemTransfEst WHERE nCdTransfEst = ' + qryMasternCdTransfEst.AsString) ;

      frmMenu.Connection.BeginTrans;

      try
          qryAux.ExecSQL;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      PosicionaQuery(qryItem, qryMasternCdTransfEst.AsString) ;
      
  end ;

end;

procedure TfrmTransfEst.btCancelarClick(Sender: TObject);
begin
  inherited;
  btLeitura.Enabled      := False ;
  btConferir.Enabled     := False ;
  btExcluirItens.Enabled := False ;

end;

procedure TfrmTransfEst.btSalvarClick(Sender: TObject);
begin
  if (qryMaster.Active) and (qryMasterdDtFinalizacao.AsString <> '') then
  begin
      MensagemErro('N�o � poss�vel alterar uma transfer�ncia j� processada.') ;
      abort ;
  end ;

  if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
      inherited;

end;

procedure TfrmTransfEst.btExcluirClick(Sender: TObject);
begin
  if (qryMaster.Active) and (qryMasterdDtFinalizacao.AsString <> '') then
  begin
      MensagemErro('N�o � poss�vel alterar uma transfer�ncia j� processada.') ;
      abort ;
  end ;

  inherited;

  qryTerceiro.Close ;
  qryEmpresaOrigem.Close ;
  qryEmpresaDestino.Close ;
  qryLojaOrigem.Close ;
  qryLojaDestino.Close ;
  qryLocalEstoqueOrigem.Close ;
  qryLocalEstoqueDestino.Close ;

  qryItem.Close ;

end;



procedure TfrmTransfEst.qryItemBeforeDelete(DataSet: TDataSet);
begin
  inherited;

  if (qryMastercFlgAutomaticaCD.Value = 1) then
  begin
      MensagemAlerta('Transfer�ncia gerada pelo Centro de Distribui��o n�o permite altera��es.');
      Abort;
  end;
end;

procedure TfrmTransfEst.btFinalizarClick(Sender: TObject);
begin

  if not qryMaster.Active then
  begin
      MensagemAlerta('Selecione uma transfer�ncia.') ;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Transfer�ncia cancelada n�o permite altera��es.');
      PosicionaPK(qryMasternCdTransfEst.Value);
      Exit;
  end;

  if (qryMasterdDtFinalizacao.AsString <> '') then
  begin
      MensagemAlerta('Transfer�ncia j� processada.') ;
      exit ;
  end ;

  if (qryItem.isEmpty) then
  begin
      MensagemAlerta('Nenhum item digitado.') ;
      exit ;
  end ;

  qryItem.First ;

  while not qryItem.eof do
  begin
      if (qryItemnQtdeDiv.Value <> 0) then
      begin
          MensagemAlerta('Existe produto(s) com diverg�ncia de quantidade.');
          exit ;
      end ;

      qryItem.Next ;
  end ;

  qryItem.First ;

  case MessageDlg('Confirma a finaliza��o desta transfer�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_PROCESSA_TRANSFERENCIA_ESTOQUE.Close ;
      SP_PROCESSA_TRANSFERENCIA_ESTOQUE.Parameters.ParamByName('@nCdTransfEst').Value := qryMasternCdTransfEst.Value ;
      SP_PROCESSA_TRANSFERENCIA_ESTOQUE.Parameters.ParamByName('@nCdUsuario').Value   := frmMenu.nCdUsuarioLogado;
      SP_PROCESSA_TRANSFERENCIA_ESTOQUE.ExecProc;

      if (qryMasternCdTabTipoTransf.Value = 4) then
      begin
           PosicionaQuery(qryPedidoCD,qryMasternCdTransfEst.AsString);
           ShowMessage('Transfer�ncia finalizada com sucesso.' + chr(13) + chr(13) + 'C�digo Pedido: ' + IntToStr(qryPedidoCDnCdPedido.Value) + '.');
      end
      else begin
          ShowMessage('Transfer�ncia finalizada com sucesso.');
      end;

  except
      frmMenu.Connection.RollbackTrans;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  PosicionaQuery(qryMaster, qryMasternCdTransfEst.AsString) ;

  case MessageDlg('Deseja imprimir o protocolo ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes: btProtocolo.Click ;
  end ;

  btCancelar.Click;

end;

procedure TfrmTransfEst.Protocolo1Click(Sender: TObject);
var
  objRel : TrptTransferencia_view;
begin

  if not qryMaster.Active then
  begin
      MensagemAlerta('Selecione uma transfer�ncia.') ;
      exit ;
  end ;

  if (qryMasterdDtFinalizacao.AsString = '') then
  begin
      MensagemAlerta('Transfer�ncia n�o processada.') ;
      exit ;
  end ;

  objRel := TrptTransferencia_view.Create(nil);

  try
      try
          if (frmMenu.LeParametro('VERVALTRANSF') = 'N') then
          begin
              objRel.QRDBText2.Enabled := false ;
              objRel.QRLabel16.Enabled := false ;
          end ;
          
          PosicionaQuery(objRel.qryMaster, qryMasternCdTransfEst.AsString) ;
          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;


end;

procedure TfrmTransfEst.EmitirEtiqueta1Click(Sender: TObject);
var
  nQtdeEtiquetas : integer;
  objForm : TfrmModImpETP;
  iIDTransf : Array of integer;
  iAux : integer;
begin
  inherited;

  if ((qryMaster.State <> dsEdit) or (qryMaster.RecordCount = 0)) then
  begin
      MensagemAlerta('Nenhuma transfer�ncia selecionada.') ;
      abort ;
  end ;
  
  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Esta transfer�ncia foi cancelada e n�o permite a impress�o de etiqueta.');
      abort;
  end;

  if (qryMasterdDtFinalizacao.AsString = '') then
  begin
      MensagemAlerta('Esta transfer�ncia ainda n�o foi processada.');
      abort;
  end;

  SetLength(iIDTransf,0);

  nQtdeEtiquetas := frmMenu.ConvInteiro( InputBox('Emiss�o de Etiqueta','Digite a quantidade de etiquetas para imprimir',''));

  if (nQtdeEtiquetas > 0) then
  begin

      SetLength(iIDTransf,nQtdeEtiquetas);

      for iAux := 0 to nQtdeEtiquetas - 1 do
      begin
          iIDTransf[iAux] := qryMasternCdTransfEst.AsInteger;
      end;

      objForm := TfrmModImpETP.Create(nil);

      objForm.emitirEtiquetas(iIDTransf,2) ;  // Etiqueta de Transferencia

  end else
      MensagemAlerta('Nenhuma Quantidade selecionada');

end;

procedure TfrmTransfEst.btAddItemClick(Sender: TObject);
var
  objForm : TfrmCampanhaPromoc_IncluiProduto;
begin
  inherited;

  if not(qryMaster.Active) then
      exit;

  objForm := TfrmCampanhaPromoc_IncluiProduto.Create(nil);
  objForm.nCdTabTipoProd := 3;

  try
      showForm(objForm, true);
  except
      MensagemErro('Erro na cria��o do formul�rio');
      raise;
  end;

  qryProdutoSel.Close;
  qryProdutoSel.Open;

  frmMenu.Connection.BeginTrans;

  try
      qryIncluiProdutos.Close;
      qryIncluiProdutos.Parameters.ParamByName('nCdTransfEst').Value := qryMasternCdTransfEst.Value;
      qryIncluiProdutos.ExecSQL;

      frmMenu.Connection.CommitTrans;

      qryItem.Requery();
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro na inclus�o dos produtos.');
      raise;
  end;
end;

procedure TfrmTransfEst.btConferirClick(Sender: TObject);
var
  objConfProdPadrao : TfrmConferenciaProdutoPadrao;
begin
  inherited;

  if not (qryMaster.Active) then
      Exit;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Transfer�ncia cancelada n�o permite altera��es.');
      PosicionaPK(qryMasternCdTransfEst.Value);
      Exit;
  end;

  if (qryMasternCdTransfEst.Value = 0) then
  begin
      MensagemAlerta('Salve a transfer�ncia antes de iniciar a confer�ncia.');
      Exit;
  end;

  try
      try
          objConfProdPadrao := TfrmConferenciaProdutoPadrao.Create(nil);
          frmMenu.mensagemUsuario('Buscando produtos para conf...');

          { -- popula dataset dos produtos -- }
          PosicionaQuery(qryPopulaConfPadrao, qryMasternCdTransfEst.AsString);

          if (qryPopulaConfPadrao.IsEmpty) then
          begin
              MensagemAlerta('N�o existe produtos para confer�ncia.');
              Exit;
          end;

          qryPopulaConfPadrao.First;
          qryPopulaConfPadrao.DisableControls;

          while (not qryPopulaConfPadrao.Eof) do
          begin
              objConfProdPadrao.cdsProdutosConfere.Insert;
              objConfProdPadrao.cdsProdutosConferecCdProduto.Value := qryPopulaConfPadraonCdProduto.AsString;
              objConfProdPadrao.cdsProdutosConferecNmProduto.Value := qryPopulaConfPadraocNmProduto.Value;
              objConfProdPadrao.cdsProdutosConferecEAN.Value       := Trim(qryPopulaConfPadraocEAN.Value);
              objConfProdPadrao.cdsProdutosConferecEANFornec.Value := Trim(qryPopulaConfPadraocEANFornec.Value);
              objConfProdPadrao.cdsProdutosConfere.Post;

              qryPopulaConfPadrao.Next;
          end;

          qryPopulaConfPadrao.First;
          qryPopulaConfPadrao.EnableControls;

          if (objConfProdPadrao.cdsProdutosConfere.IsEmpty) then
          begin
              MensagemErro('Falha ao popular produtos para confer�ncia.');
              Exit;
          end;

          { -- configura tela de confer�ncia padr�o -- }
          objConfProdPadrao.Caption               := 'Confer�ncia de Produtos';
          objConfProdPadrao.rbConferencia.Checked := True;

          showForm(objConfProdPadrao,False);

          if (objConfProdPadrao.bProcessa) then
              case MessageDlg('Confirma a grava��o da confer�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
                  mrNo:Exit;
              end
          else
              Exit;

          try
              frmMenu.Connection.BeginTrans;

              { -- processa confer�ncia dos itens -- }
              qryGravaConfPadrao.Close;
              qryGravaConfPadrao.Parameters.ParamByName('nCdTransfEst').Value := qryMasternCdTransfEst.Value;
              qryGravaConfPadrao.Parameters.ParamByName('cFlgModo').Value     := 'C';
              qryGravaConfPadrao.ExecSQL;

              frmMenu.Connection.CommitTrans;

              qryItem.Close;
              PosicionaQuery(qryItem, qryMasternCdTransfEst.AsString);
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.');
              Raise;
          end;

          { -- verificar se todos foram conferidos -- }
          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('SELECT TOP 1 1 FROM ItemTransfEst WHERE nCdTransfEst = ' + qryMasternCdTransfEst.AsString + ' AND nQtde <> nQtdeConf');
          qryAux.Open;

          if (qryAux.IsEmpty) then
              ShowMessage('Transfer�ncia conferida com sucesso.')
          else
              MensagemAlerta('Existe produto(s) com diverg�ncia de quantidade.');
      except
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      FreeAndNil(objConfProdPadrao);
      frmMenu.mensagemUsuario('');
  end;
end;

procedure TfrmTransfEst.btLeituraClick(Sender: TObject);
var
  objConfProdPadrao : TfrmConferenciaProdutoPadrao;
begin
  inherited;

  if not (qryMaster.Active) then
      Exit;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Transfer�ncia cancelada n�o permite altera��es.');
      PosicionaPK(qryMasternCdTransfEst.Value);
      Exit;
  end;

  if (qryMastercFlgAutomaticaCD.Value = 1) then
  begin
      MensagemAlerta('Transfer�ncia gerada pelo centro de distribui��o n�o permite altera��es.');
      Exit;
  end;

  if (qryMasternCdTransfEst.Value = 0) then
  begin
      MensagemAlerta('Salve a transfer�ncia antes de iniciar o processo de digita��o/leitura.');
      exit;
  end;

  try
      try
          { -- configura tela de digit���o padr�o -- }
          objConfProdPadrao := TfrmConferenciaProdutoPadrao.Create(nil);
          objConfProdPadrao.Caption                    := 'Digita��o/Leitura de Produtos';
          objConfProdPadrao.rbDigitacaoLeitura.Checked := True;

          showForm(objConfProdPadrao,False);

          if (objConfProdPadrao.bProcessa) then
              case MessageDlg('Confirma a grava��o dos itens ?',mtConfirmation,[mbYes,mbNo],0) of
                  mrNo:Exit;
              end
          else
              Exit;

          frmMenu.Connection.BeginTrans;

          { -- processa inclus�o dos itens -- }
          qryGravaConfPadrao.Close;
          qryGravaConfPadrao.Parameters.ParamByName('nCdTransfEst').Value := qryMasternCdTransfEst.Value;
          qryGravaConfPadrao.Parameters.ParamByName('cFlgModo').Value     := 'D';
          qryGravaConfPadrao.ExecSQL;

          qryItem.Close;
          PosicionaQuery(qryItem, qryMasternCdTransfEst.AsString);

          frmMenu.Connection.CommitTrans;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      FreeAndNil(objConfProdPadrao);
      frmMenu.mensagemUsuario('');
  end;
end;

Initialization
    RegisterClass(TfrmTransfEst) ;
end.
