unit fMetaDescontoMensal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, cxPC, cxControls, DB,
  ADODB, StdCtrls, Mask, ER2Lookup, cxLookAndFeelPainters, cxButtons,
  DBCtrls;

type
  TfrmMetaDescontoMensal = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    GroupBox1: TGroupBox;
    edtEmpresa: TER2LookupMaskEdit;
    Label3: TLabel;
    qryMaster: TADOQuery;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit1: TDBEdit;
    dsEmpresa: TDataSource;
    dsMaster: TDataSource;
    edtMesAno: TMaskEdit;
    qryMasternCdMetaDescontoMes: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMastercNmLoja: TStringField;
    qryMasternValMetaDesconto: TBCDField;
    qryMasterdDtInicial: TDateTimeField;
    qryMasterdDtFinal: TDateTimeField;
    qryMasternCdUsuarioUltAlt: TIntegerField;
    qryMasterdDtUltAlt: TDateTimeField;
    qryAux: TADOQuery;
    qryInsereSemana: TADOQuery;
    Label1: TLabel;
    qryMasternValDescontoUtil: TBCDField;
    qryMasternValDescontoDistr: TBCDField;
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure edtEmpresaExit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    iMes : Integer;
    iAno : Integer;
  end;

var
  frmMetaDescontoMensal: TfrmMetaDescontoMensal;

implementation

uses fMenu, fMetaDescontoMensal_Vendedor;

{$R *.dfm}

procedure TfrmMetaDescontoMensal.DBGridEh1DblClick(Sender: TObject);
var
  objDescontoVendedor : TfrmMetaDescontoMensal_vendedor;
begin
  inherited;

  if (qryMasternValMetaDesconto.Value = 0) then
  begin
      MensagemAlerta('Informe o saldo da meta de desconto a ser distribuido entre os vendedores.');
      Abort;
  end;

  objDescontoVendedor := TfrmMetaDescontoMensal_vendedor.Create(nil);

  objDescontoVendedor.qryMaster.Close;
  objDescontoVendedor.qryMaster.Parameters.ParamByName('nCdUsuario').Value         := frmMenu.nCdUsuarioLogado;
  objDescontoVendedor.qryMaster.Parameters.ParamByName('nCdLoja').Value            := qryMasternCdLoja.Value;
  objDescontoVendedor.qryMaster.Parameters.ParamByName('nCdMetaDescontoMes').Value := qryMasternCdMetaDescontoMes.Value;
  objDescontoVendedor.qryMaster.Open;

  if(objDescontoVendedor.qryMaster.RecordCount > 0) then
  begin
      objDescontoVendedor.iMes := iMes;
      objDescontoVendedor.iAno := iAno;
      objDescontoVendedor.cCdMetaDescontoMes := qryMasternCdMetaDescontoMes.AsString;
      showForm(objDescontoVendedor, true);

      qryMaster.Requery;
  end
  else
      ShowMessage('N�o existe vendedores vinculados para a loja selecionada.');
end;

procedure TfrmMetaDescontoMensal.edtEmpresaExit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryEmpresa, edtEmpresa.Text);

  if (Trim(DBEdit1.Text) = '') then
  begin
      MensagemAlerta('Informe a empresa.');
      edtEmpresa.SetFocus;
      Abort;
  end;
  
end;

procedure TfrmMetaDescontoMensal.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (qryMasternValMetaDesconto.Value < 0) then
  begin
      MensagemAlerta('O valor da meta n�o pode ser negativo.');
      Abort;
  end;

  if (qryMasternValMetaDesconto.Value < qryMasternValDescontoDistr.Value) then
  begin
      MensagemAlerta('O valor informado � menor que a meta j� distribu�da.' + #13 + 'Informe um valor igual ou superior a R$ ' + FormatCurr('#,##0.00', qryMasternValDescontoDistr.Value));
      Abort;
  end;

  inherited;
end;

procedure TfrmMetaDescontoMensal.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (trim(edtMesAno.Text) = '/') then
      edtMesAno.Text := Copy(DateToStr(Date),4,7) ;

  edtMesAno.Text := Trim(frmMenu.ZeroEsquerda(edtMesAno.Text,6)) ;

  try
      StrToDate('01/' + edtMesAno.Text) ;
  except
      MensagemErro('M�s/Ano inv�lido.'+#13#13+'Utilize: mm/aaaa') ;
      edtMesAno.SetFocus;
      abort ;
  end;

  iMes := StrToInt(Trim(Copy(edtMesAno.Text,1,2)));
  iAno := StrToInt(Trim(Copy(edtMesAno.Text,4,4)));

  {-- se o campo da empresa estiver vazio preenche com a empresa ativa --}
  
  if (Trim(edtEmpresa.Text) = '') then
  begin
      edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
      qryEmpresa.Close;
      PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
  end;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Close;
      qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro(Trim(edtEmpresa.Text));
      qryMaster.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryMaster.Parameters.ParamByName('iMes').Value       := iMes;
      qryMaster.Parameters.ParamByName('iAno').Value       := iAno;
      qryMaster.Open;

      {-- Caso n�o seja encontrado nenhum registro --}
      if (qryMaster.IsEmpty) then
      begin
          MensagemAlerta('Nenhum registro encontrado.');
          Abort;
      end;
      
  except
      frmMenu.Connection.RollbackTrans;        
      MensagemErro('Erro no Processamento.');
      raise;
  end;
  frmMenu.Connection.CommitTrans;
  
  qryMaster.First;
  DBGridEh1.SetFocus;
end;

initialization
  RegisterClass(TfrmMetaDescontoMensal) ;

end.
