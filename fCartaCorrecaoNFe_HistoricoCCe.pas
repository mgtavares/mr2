unit fCartaCorrecaoNFe_HistoricoCCe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, DBCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxPC;

type
  TfrmCartaCorrecaoNFe_HistoricoCCe = class(TfrmProcesso_Padrao)
    qryCartaCorrecaoNFe: TADOQuery;
    dsCartaCorrecaoNFe: TDataSource;
    cxPageControl1: TcxPageControl;
    Tab1: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdCartaCorrecaoNFe: TcxGridDBColumn;
    cxGridDBTableView1dDtEmissao: TcxGridDBColumn;
    cxGridDBTableView1cCorrecao: TcxGridDBColumn;
    cxGridDBTableView1iSeq: TcxGridDBColumn;
    cxGridDBTableView1iLote: TcxGridDBColumn;
    cxGridDBTableView1cChaveNFe: TcxGridDBColumn;
    cxGridDBTableView1cNrProtocoloNFe: TcxGridDBColumn;
    qryCartaCorrecaoNFenCdCartaCorrecaoNFe: TIntegerField;
    qryCartaCorrecaoNFenCdDoctoFiscal: TIntegerField;
    qryCartaCorrecaoNFedDtEmissao: TDateTimeField;
    qryCartaCorrecaoNFecCorrecao: TStringField;
    qryCartaCorrecaoNFeiSeq: TIntegerField;
    qryCartaCorrecaoNFeiLote: TIntegerField;
    qryCartaCorrecaoNFecChaveNFe: TStringField;
    qryCartaCorrecaoNFecCaminhoXML: TStringField;
    qryCartaCorrecaoNFecNrProtocoloCCe: TStringField;
    qryCartaCorrecaoNFecArquivoXML: TStringField;
    qryCartaCorrecaoNFecXMLCCe: TMemoField;
    qryCartaCorrecaoNFenCdServidorOrigem: TIntegerField;
    qryCartaCorrecaoNFedDtReplicacao: TDateTimeField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCartaCorrecaoNFe_HistoricoCCe: TfrmCartaCorrecaoNFe_HistoricoCCe;

implementation

uses
  fMenu;

{$R *.dfm}

end.
