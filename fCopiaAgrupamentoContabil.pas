unit fCopiaAgrupamentoContabil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, ER2Lookup;

type
  TfrmCopiaAgrupamentoContabil = class(TfrmProcesso_Padrao)
    qryConsultaTipo: TADOQuery;
    dsConsultaTipo: TDataSource;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtDescricao: TEdit;
    edtExercicio: TEdit;
    ER2LookupMaskEdit1: TER2LookupMaskEdit;
    qryConsultaTiponCdTabTipoAgrupamentoContabil: TIntegerField;
    qryConsultaTipocNmTabTipoAgrupamentoContabil: TStringField;
    DBEdit1: TDBEdit;
    SP_DUPLICA_AGRUPAMENTO_CONTABIL: TADOStoredProc;
    qryVerificaTipo: TADOQuery;
    dsVerificaTipo: TDataSource;
    qryVerificaTiponcdAgrupamentoContabil: TIntegerField;
    qryVerificaTipoiAno: TIntegerField;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdAgrupamento : integer;
  end;

var
  frmCopiaAgrupamentoContabil: TfrmCopiaAgrupamentoContabil;

implementation

uses fMenu,fAgrupamentoContabil, DateUtils;

{$R *.dfm}

procedure TfrmCopiaAgrupamentoContabil.ToolButton1Click(Sender: TObject);
begin
    if (Trim(edtDescricao.Text) = '') or (Trim(edtExercicio.Text) = '') or (Trim(ER2LookupMaskEdit1.Text) = '') then
    begin
        ShowMessage('Preencha todos os campos');
        abort;
    end;

    qryVerificaTipo.Close;
    qryVerificaTipo.Parameters.ParamByName('nPKTipo').Value:= qryConsultaTiponCdTabTipoAgrupamentoContabil.Value;
    qryVerificaTipo.Parameters.ParamByName('nPKAno').Value := edtExercicio.text;
    qryVerificaTipo.open;

    if (StrToInt(edtExercicio.text) < 2000) or (StrToInt(edtExercicio.text) > (YearOf(Now)+1))then
    begin
        ShowMessage('Exerc�cio inv�lido');
        Abort;
    end;

    if not(qryVerificaTipo.IsEmpty) then
    begin
        if (qryVerificaTipo.Parameters.ParamByName('nPKTipo').Value = 1) then
        begin
            ShowMessage('Tipo Agrupamento n�o pode ser duplicado');
            Abort;
        end;
    end;

    try
        frmMenu.Connection.BeginTrans;

        SP_DUPLICA_AGRUPAMENTO_CONTABIL.Close;

        SP_DUPLICA_AGRUPAMENTO_CONTABIL.Parameters.ParamByName('@nCdAgrupamentoContabil').Value := nCdAgrupamento;
        SP_DUPLICA_AGRUPAMENTO_CONTABIL.Parameters.ParamByName('@cNmAgrupamentoContabil').Value := edtDescricao.Text;
        SP_DUPLICA_AGRUPAMENTO_CONTABIL.Parameters.ParamByName('@nCdTabTipoAgrupamentoContabil').Value := qryConsultaTiponCdTabTipoAgrupamentoContabil.Value;
        SP_DUPLICA_AGRUPAMENTO_CONTABIL.Parameters.ParamByName('@iAno').Value := edtExercicio.Text;

        SP_DUPLICA_AGRUPAMENTO_CONTABIL.ExecProc;

        frmMenu.Connection.CommitTrans;

        ShowMessage('Duplica��o Realizada');

    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamneto');
        raise;
        abort;
    end;
        close;
  inherited;
end;

end.
