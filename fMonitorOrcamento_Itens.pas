unit fMonitorOrcamento_Itens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  StdCtrls, DBCtrls, ADODB, GridsEh, DBGridEh;

type
  TfrmMonitorOrcamento_Itens = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryItemPedido: TADOQuery;
    qryItemPedidonCdItemPedido: TAutoIncField;
    qryItemPedidonCdProduto: TIntegerField;
    qryItemPedidocNmItem: TStringField;
    qryItemPedidonQtdePed: TBCDField;
    qryItemPedidocDescricaoTecnica: TMemoField;
    Label1: TLabel;
    DBMemo1: TDBMemo;
    DataSource1: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMonitorOrcamento_Itens: TfrmMonitorOrcamento_Itens;

implementation

{$R *.dfm}

end.
