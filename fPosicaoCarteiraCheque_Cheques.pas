unit fPosicaoCarteiraCheque_Cheques;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping;

type
  TfrmPosicaoCarteiraCheque_Cheques = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryCheques: TADOQuery;
    dsCheques: TDataSource;
    qryChequesnCdCheque: TAutoIncField;
    qryChequesnCdBanco: TIntegerField;
    qryChequesiNrCheque: TIntegerField;
    qryChequesnValCheque: TBCDField;
    qryChequescCNPJCPF: TStringField;
    qryChequescNmTerceiro: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPosicaoCarteiraCheque_Cheques: TfrmPosicaoCarteiraCheque_Cheques;

implementation

{$R *.dfm}

end.
