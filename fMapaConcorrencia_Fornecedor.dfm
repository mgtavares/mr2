inherited frmMapaConcorrencia_Fornecedor: TfrmMapaConcorrencia_Fornecedor
  Left = 115
  Top = 206
  Width = 942
  Height = 242
  BorderIcons = [biSystemMenu]
  Caption = 'Fornecedores'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 926
    Height = 177
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 96
    Width = 85
    Height = 13
    Caption = '1o Fornecedor ->'
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 136
    Width = 85
    Height = 13
    Caption = '2o Fornecedor ->'
  end
  object Label3: TLabel [3]
    Left = 8
    Top = 176
    Width = 85
    Height = 13
    Caption = '3o Fornecedor ->'
  end
  object Label4: TLabel [4]
    Left = 128
    Top = 72
    Width = 27
    Height = 13
    Caption = 'Nome'
  end
  object Label5: TLabel [5]
    Left = 336
    Top = 72
    Width = 42
    Height = 13
    Caption = 'Telefone'
  end
  object Label6: TLabel [6]
    Left = 512
    Top = 72
    Width = 39
    Height = 13
    Caption = 'Contato'
  end
  object Label7: TLabel [7]
    Left = 688
    Top = 72
    Width = 28
    Height = 13
    Caption = 'e-mail'
  end
  object Label8: TLabel [8]
    Left = 64
    Top = 48
    Width = 56
    Height = 13
    Caption = 'Nome Mapa'
  end
  inherited ToolBar1: TToolBar
    Width = 926
    ButtonWidth = 86
    inherited ToolButton1: TToolButton
      Caption = '&Criar Mapa'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 86
    end
    inherited ToolButton2: TToolButton
      Left = 94
    end
  end
  object edtNome1: TEdit [10]
    Left = 128
    Top = 88
    Width = 200
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 2
  end
  object edtFone1: TEdit [11]
    Left = 336
    Top = 88
    Width = 170
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 3
  end
  object edtContato1: TEdit [12]
    Left = 512
    Top = 88
    Width = 170
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 4
  end
  object edtEmail1: TEdit [13]
    Left = 688
    Top = 88
    Width = 170
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 5
  end
  object btSel1: TcxButton [14]
    Left = 96
    Top = 84
    Width = 33
    Height = 25
    Hint = 'Consultar Cadastro de Fornecedores'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 17
    TabStop = False
    OnClick = btSel1Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000C6D6DEC6948C
      C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
      8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
      D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
      00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
      FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
      948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
      FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
      C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
      FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
      948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
      8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
      D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
    LookAndFeel.NativeStyle = True
  end
  object edtNome2: TEdit [15]
    Left = 128
    Top = 128
    Width = 200
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 6
  end
  object edtFone2: TEdit [16]
    Left = 336
    Top = 128
    Width = 170
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 7
  end
  object edtContato2: TEdit [17]
    Left = 512
    Top = 128
    Width = 170
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 8
  end
  object edtEmail2: TEdit [18]
    Left = 688
    Top = 128
    Width = 170
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 9
  end
  object btSel2: TcxButton [19]
    Left = 96
    Top = 124
    Width = 33
    Height = 25
    Hint = 'Consultar Cadastro de Fornecedores'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 18
    TabStop = False
    OnClick = btSel2Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000C6D6DEC6948C
      C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
      8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
      D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
      00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
      FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
      948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
      FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
      C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
      FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
      948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
      8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
      D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
    LookAndFeel.NativeStyle = True
  end
  object edtNome3: TEdit [20]
    Left = 128
    Top = 168
    Width = 200
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 10
  end
  object edtFone3: TEdit [21]
    Left = 336
    Top = 168
    Width = 170
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 11
  end
  object edtContato3: TEdit [22]
    Left = 512
    Top = 168
    Width = 170
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 12
  end
  object edtEmail3: TEdit [23]
    Left = 688
    Top = 168
    Width = 170
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 13
  end
  object btSel3: TcxButton [24]
    Left = 96
    Top = 164
    Width = 33
    Height = 25
    Hint = 'Consultar Cadastro de Fornecedores'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 19
    TabStop = False
    OnClick = btSel3Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000C6D6DEC6948C
      C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
      8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
      D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
      00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
      FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
      948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
      FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
      C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
      FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
      948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
      8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
      D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
    LookAndFeel.NativeStyle = True
  end
  object edtID1: TEdit [25]
    Left = 872
    Top = 88
    Width = 49
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 14
  end
  object edtID2: TEdit [26]
    Left = 872
    Top = 128
    Width = 49
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 15
  end
  object edtID3: TEdit [27]
    Left = 872
    Top = 168
    Width = 49
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 16
  end
  object edtNmMapa: TEdit [28]
    Left = 128
    Top = 40
    Width = 433
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 50
    TabOrder = 1
  end
  inherited ImageList1: TImageList
    Left = 560
    Top = 16
  end
  object SP_ADD_FORNEC_MAPA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ADD_FORNEC_MAPA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdMapaCompra'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNmFornecedor'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@cNmContato'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@cTelefone'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end
      item
        Name = '@cEmail'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end>
    Left = 568
    Top = 64
  end
  object SP_GERA_MAPA_COMPRA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_MAPA_COMPRA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNmMapaCompra'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@nCdMapaCompra'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 632
    Top = 48
  end
  object SP_EXPLODE_ITENS_MAPA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_EXPLODE_ITENS_MAPA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdMapaCompra'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 624
    Top = 112
  end
  object qryFornecedor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @cNmFantasia varchar(50)'
      '       ,@cNmContato  varchar(50)'
      '       ,@cTelefone   varchar(20)'
      '       ,@cEmail      varchar(50)'
      '       ,@nCdTerceiro int'
      ''
      'Set @nCdTerceiro = :nPK'
      ''
      
        'SELECT @cNmFantasia = CASE WHEN cNmFantasia IS NOT NULL THEN cNm' +
        'Fantasia'
      '                           ELSE cNmTerceiro'
      '                      END'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = @nCdTerceiro'
      ''
      'SELECT @cNmContato = cNmContato'
      '      ,@cTelefone  = cTelefone'
      '      ,@cEmail     = cEmail'
      '  FROM Endereco'
      ' WHERE nCdTerceiro = @nCdTerceiro'
      '   AND cNmContato IS NOT NULL'
      ''
      'SELECT @cNmFantasia cNmFantasia'
      '      ,@cNmContato  cNmContato'
      '      ,@cTelefone   cTelefone'
      '      ,@cEmail      cEmail')
    Left = 448
    Top = 72
    object qryFornecedorcNmFantasia: TStringField
      FieldName = 'cNmFantasia'
      ReadOnly = True
      Size = 50
    end
    object qryFornecedorcNmContato: TStringField
      FieldName = 'cNmContato'
      ReadOnly = True
      Size = 50
    end
    object qryFornecedorcTelefone: TStringField
      FieldName = 'cTelefone'
      ReadOnly = True
    end
    object qryFornecedorcEmail: TStringField
      FieldName = 'cEmail'
      ReadOnly = True
      Size = 50
    end
  end
end
