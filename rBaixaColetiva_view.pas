unit rBaixaColetiva_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptBaixaColetiva_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRBand5: TQRBand;
    QRShape2: TQRShape;
    QRDBText8: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText10: TQRDBText;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    qryLoteLiq: TADOQuery;
    qryLoteLiqnCdLoteLiq: TStringField;
    qryLoteLiqcTipoLiq: TStringField;
    qryLoteLiqcNmTerceiro: TStringField;
    qryLoteLiqnValDinheiro: TBCDField;
    qryLoteLiqnValChTerceiro: TBCDField;
    qryLoteLiqnValRecursoProp: TBCDField;
    qryLoteLiqnValCredAnterior: TBCDField;
    qryLoteLiqnValTotalLote: TBCDField;
    qryLoteLiqnValTitLiq: TBCDField;
    qryLoteLiqcNmUsuario: TStringField;
    qryLoteLiqnCdTitulo: TIntegerField;
    qryLoteLiqdDtEmissao: TDateTimeField;
    qryLoteLiqdDtVenc: TDateTimeField;
    qryLoteLiqcNrTit: TStringField;
    qryLoteLiqiParcela: TIntegerField;
    qryLoteLiqcNrNF: TStringField;
    qryLoteLiqnValLiq: TBCDField;
    qryLoteLiqcNmEspTit: TStringField;
    QRLabel14: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel16: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabel20: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QuickRep2: TQuickRep;
    QRLabel34: TQRLabel;
    QRDBText9: TQRDBText;
    TitleBand1: TQRBand;
    DetailBand1: TQRBand;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRShape6: TQRShape;
    QRLabel31: TQRLabel;
    SummaryBand1: TQRBand;
    QRLabel8: TQRLabel;
    qryChequeLoteLiq: TADOQuery;
    qryChequeLoteLiqnCdChequeLoteLiq: TAutoIncField;
    qryChequeLoteLiqnCdLoteLiq: TIntegerField;
    qryChequeLoteLiqnCdBanco: TIntegerField;
    qryChequeLoteLiqcAgencia: TStringField;
    qryChequeLoteLiqcConta: TStringField;
    qryChequeLoteLiqcDigito: TStringField;
    qryChequeLoteLiqcCNPJCPF: TStringField;
    qryChequeLoteLiqnValCheque: TBCDField;
    qryChequeLoteLiqdDtDeposito: TDateTimeField;
    qryChequeLoteLiqiNrCheque: TIntegerField;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRCompositeReport1: TQRCompositeReport;
    QRDBText26: TQRDBText;
    QRLabel12: TQRLabel;
    qryLoteLiqnSaldoLote: TBCDField;
    PageFooterBand1: TQRBand;
    TitleBand2: TQRBand;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRShape9: TQRShape;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRSysData3: TQRSysData;
    QRSysData4: TQRSysData;
    QRLabel47: TQRLabel;
    QRShape10: TQRShape;
    QRShape11: TQRShape;
    QRDBText27: TQRDBText;
    QRLabel48: TQRLabel;
    qryLoteLiqnValTaxaAdm: TBCDField;
    qryChequeLoteLiqnValTaxaAdm: TBCDField;
    qryChequeLoteLiqiDiasVenc: TIntegerField;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    qryLoteLiqnValJuro: TBCDField;
    qryLoteLiqnValDesconto: TBCDField;
    qryLoteLiqnSaldoTit: TBCDField;
    qryLoteLiqnSaldoResid: TBCDField;
    QRDBText30: TQRDBText;
    QRLabel53: TQRLabel;
    QRDBText31: TQRDBText;
    QRLabel54: TQRLabel;
    QRDBText32: TQRDBText;
    QRLabel33: TQRLabel;
    QRLabel55: TQRLabel;
    QRDBText6: TQRDBText;
    QRShape7: TQRShape;
    QRLabel32: TQRLabel;
    QRLabel35: TQRLabel;
    procedure QRCompositeReport1AddReports(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptBaixaColetiva_view: TrptBaixaColetiva_view;

implementation

{$R *.dfm}

procedure TrptBaixaColetiva_view.QRCompositeReport1AddReports(
  Sender: TObject);
begin

  QRCompositeReport1.Reports.Add(QuickRep1) ;
  QRCompositeReport1.Reports.Add(QuickRep2) ;

end;

end.
