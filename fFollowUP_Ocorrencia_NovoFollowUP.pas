unit fFollowUP_Ocorrencia_NovoFollowUP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB;

type
  TfrmFollowUP_Ocorrencia_NovoFollowUP = class(TfrmProcesso_Padrao)
    qryInsereFollow: TADOQuery;
    qryAlteraPrevisaoRequis: TADOQuery;
    memoOcorrencia: TMemo;
    mskDtAtendFinal: TMaskEdit;
    mskDtAtendInicial: TMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    Label1: TLabel;
    edtDescResumida: TEdit;
    mskDtProximaAcao: TMaskEdit;
    Label5: TLabel;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdOcorrencia : integer;
  end;

var
  frmFollowUP_Ocorrencia_NovoFollowUP: TfrmFollowUP_Ocorrencia_NovoFollowUP;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmFollowUP_Ocorrencia_NovoFollowUP.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (Trim(edtDescResumida.Text) = '') then
  begin
      ShowMessage('Informe a descri��o resumida.');
      edtDescResumida.SetFocus;
      Abort;
  end;

  if (Trim(mskDtProximaAcao.Text) <> '/  /') then
  begin
      if (frmMenu.ConvData(mskDtProximaAcao.Text) = StrToDate('01/01/1900')) then
      begin
          mskDtProximaAcao.SetFocus;
          Abort;
      end;
  end;

  if ((Trim(mskDtAtendInicial.Text) <> '/  /') and (Trim(mskDtAtendFinal.Text) = '/  /'))then
  begin
      ShowMessage('Para informar uma nova previs�o de atendimento, informe a data final.');
      mskDtAtendFinal.SetFocus;
      Abort;
  end;

  if ((Trim(mskDtAtendInicial.Text) = '/  /') and (Trim(mskDtAtendFinal.Text) <> '/  /'))then
  begin
      ShowMessage('Para informar uma nova previs�o de atendimento, informe a data inicial.') ;
      mskDtAtendInicial.SetFocus;
      Abort;
  end;

  if ((Trim(mskDtAtendInicial.Text) <> '/  /') and (Trim(mskDtAtendFinal.Text) <> '/  /'))then
  begin
      { -- valida datas -- }
      if (frmMenu.ConvData(mskDtAtendInicial.Text) = StrToDate('01/01/1900')) then
      begin
          mskDtAtendInicial.SetFocus;
          Abort;
      end;

      if (frmMenu.ConvData(mskDtAtendFinal.Text) = StrToDate('01/01/1900')) then
      begin
          mskDtAtendFinal.SetFocus;
          Abort;
      end;

      if (not frmMenu.fnValidaPeriodo(frmMenu.ConvData(mskDtAtendInicial.Text),frmMenu.ConvData(mskDtAtendFinal.Text))) then
      begin
          mskDtAtendInicial.SetFocus;
          Abort;
      end;

      { -- atualiza previs�o de atendimento -- }
      try
          frmMenu.Connection.BeginTrans;

          qryAlteraPrevisaoRequis.Close;
          qryAlteraPrevisaoRequis.Parameters.ParamByName('nCdOcorrencia').Value   := nCdOcorrencia;
          qryAlteraPrevisaoRequis.Parameters.ParamByName('dDtPrevAtendIni').Value := mskDtAtendInicial.Text;
          qryAlteraPrevisaoRequis.Parameters.ParamByName('dDtPrevAtendFim').Value := mskDtAtendFinal.Text;
          qryAlteraPrevisaoRequis.ExecSQL;

          frmMenu.Connection.CommitTrans;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento');
          Raise;
          Exit;
      end;
  end;

  if (MessageDlg('Confirma a gera��o desta ocorr�ncia ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      Exit;

  try
      frmMenu.Connection.BeginTrans;

      { -- gera registro follow up -- }
      qryInsereFollow.Close;
      qryInsereFollow.Parameters.ParamByName('nCdOcorrencia').Value    := nCdOcorrencia;
      qryInsereFollow.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
      qryInsereFollow.Parameters.ParamByName('cOcorrenciaResum').Value := edtDescResumida.Text;
      qryInsereFollow.Parameters.ParamByName('cOcorrencia').Value      := AnsiUpperCase(memoOcorrencia.Text);

      if (Trim(mskDtProximaAcao.Text) = '/  /') then
          qryInsereFollow.Parameters.ParamByName('dDtProxAcao').Value := Null
      else
          qryInsereFollow.Parameters.ParamByName('dDtProxAcao').Value := mskDtProximaAcao.Text;

      qryInsereFollow.ExecSQL;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Ocorr�ncia registrada com sucesso.');
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento');
      Raise;
  end;

  Close;

end;

end.
