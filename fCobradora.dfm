inherited frmCobradora: TfrmCobradora
  Left = 177
  Top = 178
  Caption = 'Cobradora'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 54
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 62
    Top = 70
    Width = 30
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 9
    Top = 94
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Fun'#231#227'o Encargo'
    FocusControl = DBEdit3
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 98
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdCobradora'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 98
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmCobradora'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBCheckBox1: TDBCheckBox [7]
    Left = 98
    Top = 114
    Width = 97
    Height = 17
    Caption = 'Ativo'
    DataField = 'cFlgAtivo'
    DataSource = dsMaster
    TabOrder = 4
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBEdit3: TDBEdit [8]
    Left = 98
    Top = 88
    Width = 650
    Height = 19
    DataField = 'cNmFuncaoValEncargoSQL'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBCheckBox2: TDBCheckBox [9]
    Left = 202
    Top = 114
    Width = 543
    Height = 17
    Caption = 'Permite Liquida'#231#227'o por recebimento na Loja.'
    DataField = 'cFlgPermiteLiqLoja'
    DataSource = dsMaster
    TabOrder = 5
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Cobradora'
      'WHERE nCdCobradora = :nPK')
    object qryMasternCdCobradora: TIntegerField
      FieldName = 'nCdCobradora'
    end
    object qryMastercNmCobradora: TStringField
      FieldName = 'cNmCobradora'
      Size = 50
    end
    object qryMastercFlgAtivo: TIntegerField
      FieldName = 'cFlgAtivo'
    end
    object qryMastercNmFuncaoValEncargoSQL: TStringField
      FieldName = 'cNmFuncaoValEncargoSQL'
      Size = 50
    end
    object qryMastercFlgPermiteLiqLoja: TIntegerField
      FieldName = 'cFlgPermiteLiqLoja'
    end
  end
end
