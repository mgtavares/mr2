inherited frmImpBoletoAgrupamento: TfrmImpBoletoAgrupamento
  Left = 139
  Top = 121
  Width = 907
  Height = 432
  Caption = 'Agrupamento de T'#237'tulos'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 126
    Width = 891
    Height = 270
  end
  inherited ToolBar1: TToolBar
    Width = 891
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 126
    Width = 891
    Height = 270
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 266
    ClientRectLeft = 4
    ClientRectRight = 887
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'T'#237'tulos'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 883
        Height = 242
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          DataController.DataSource = dsTempTitSelecioando
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '##,#00.00'
              Kind = skSum
              FieldName = 'nSaldoTit'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              Column = cxGrid1DBTableView1nSaldoTit
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnHiding = True
          OptionsCustomize.ColumnMoving = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GridLines = glVertical
          OptionsView.GroupByBox = False
          Styles.Selection = frmMenu.LinhaAzul
          Styles.Header = frmMenu.Header
          object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
            Caption = 'ID'
            DataBinding.FieldName = 'nCdTitulo'
          end
          object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
            Caption = 'N'#250'm. T'#237'tulo'
            DataBinding.FieldName = 'cNrTit'
          end
          object cxGrid1DBTableView1cNrNF: TcxGridDBColumn
            Caption = 'N'#250'm. NF'
            DataBinding.FieldName = 'cNrNF'
            Width = 73
          end
          object cxGrid1DBTableView1iParcela: TcxGridDBColumn
            Caption = 'Parc.'
            DataBinding.FieldName = 'iParcela'
            Width = 51
          end
          object cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn
            Caption = 'Dt. Emiss'#227'o'
            DataBinding.FieldName = 'dDtEmissao'
            Width = 89
          end
          object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
            Caption = 'Dt. Vencimento'
            DataBinding.FieldName = 'dDtVenc'
          end
          object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
            Caption = 'Saldo T'#237'tulo'
            DataBinding.FieldName = 'nSaldoTit'
            HeaderAlignmentHorz = taRightJustify
            Width = 99
          end
          object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
            Caption = 'Terceiro'
            DataBinding.FieldName = 'cNmTerceiro'
            Width = 145
          end
          object cxGrid1DBTableView1cNmFormaPagto: TcxGridDBColumn
            Caption = 'Forma Pagamento'
            DataBinding.FieldName = 'cNmFormaPagto'
            Width = 128
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 891
    Height = 97
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Tag = 1
      Left = 15
      Top = 23
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Vencimento'
    end
    object Label6: TLabel
      Tag = 1
      Left = 28
      Top = 48
      Width = 68
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero Docto'
    end
    object Label2: TLabel
      Tag = 1
      Left = 38
      Top = 74
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = 'Observa'#231#227'o'
    end
    object MaskEditVencto: TMaskEdit
      Left = 104
      Top = 15
      Width = 69
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 0
      Text = '  /  /    '
    end
    object MaskEditNumeroDocto: TMaskEdit
      Left = 104
      Top = 40
      Width = 113
      Height = 21
      TabOrder = 1
    end
    object MaskEditObs: TMaskEdit
      Left = 104
      Top = 66
      Width = 641
      Height = 21
      TabOrder = 2
    end
  end
  object qryTempTitSelecionado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_TitulosBoleto'#39') IS NULL)'
      'BEGIN'
      #9'CREATE TABLE #Temp_TitulosBoleto(nCdTitulo      int PRIMARY KEY'
      #9#9#9#9#9#9#9'   ,cFlgOK         int default 0 not null'
      ' '#9#9#9#9#9#9#9'   ,cNrTit         char(17)'
      #9#9#9#9#9#9#9'   ,cNrNF          char(15)'
      #9#9#9#9#9#9#9'   ,iParcela       int'
      #9#9#9#9#9#9#9'   ,dDtEmissao     datetime'
      #9#9#9#9#9#9#9'   ,dDtVenc        datetime'
      #9#9#9#9#9#9#9'   ,nSaldoTit      decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,nCdTerceiro    int'
      #9#9#9#9#9#9#9'   ,cNmTerceiro    varchar(50)'
      '                 ,nCdFormaPagto  int'
      '                 ,cNmFormaPagto  varchar(50))'
      'END'
      ''
      'SELECT *'
      '  FROM #Temp_TitulosBoleto'
      ' WHERE cFlgOK = 1'
      ' ORDER BY cNmTerceiro,dDtEmissao,cNrNf,iParcela')
    Left = 520
    Top = 65
    object qryTempTitSelecionadonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTempTitSelecionadocFlgOK: TIntegerField
      FieldName = 'cFlgOK'
    end
    object qryTempTitSelecionadocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTempTitSelecionadocNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTempTitSelecionadoiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTempTitSelecionadodDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTempTitSelecionadodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTempTitSelecionadonSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempTitSelecionadonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTempTitSelecionadocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTempTitSelecionadonCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryTempTitSelecionadocNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
  end
  object dsTempTitSelecioando: TDataSource
    DataSet = qryTempTitSelecionado
    Left = 560
    Top = 64
  end
  object qryMTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      '   FROM MTitulo')
    Left = 624
    Top = 64
    object qryMTitulonCdMTitulo: TAutoIncField
      FieldName = 'nCdMTitulo'
    end
    object qryMTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryMTitulonCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryMTitulonCdOperacao: TIntegerField
      FieldName = 'nCdOperacao'
    end
    object qryMTitulonCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryMTituloiNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryMTitulonValMov: TBCDField
      FieldName = 'nValMov'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMTitulodDtMov: TDateTimeField
      FieldName = 'dDtMov'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMTitulodDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryMTitulodDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryMTitulocNrDoc: TStringField
      FieldName = 'cNrDoc'
      FixedChar = True
      Size = 15
    end
    object qryMTitulodDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryMTitulonCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qryMTitulonCdUsuarioCancel: TIntegerField
      FieldName = 'nCdUsuarioCancel'
    end
    object qryMTitulocObsMov: TMemoField
      FieldName = 'cObsMov'
      BlobType = ftMemo
    end
    object qryMTitulonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMTitulocFlgMovAutomatico: TIntegerField
      FieldName = 'cFlgMovAutomatico'
    end
    object qryMTitulodDtBomPara: TDateTimeField
      FieldName = 'dDtBomPara'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMTitulonCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMTitulocFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
  end
  object qryTituloAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM'
      '  Titulo'
      '  WHERE nCdTitulo = :nPk')
    Left = 664
    Top = 64
    object qryTituloAuxnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTituloAuxnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryTituloAuxnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryTituloAuxcNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTituloAuxiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTituloAuxnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTituloAuxnCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryTituloAuxnCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryTituloAuxcSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryTituloAuxdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTituloAuxdDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
    end
    object qryTituloAuxdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTituloAuxdDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
    object qryTituloAuxdDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryTituloAuxdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryTituloAuxnValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTituloAuxnValLiq: TBCDField
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryTituloAuxnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTituloAuxnValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTituloAuxnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryTituloAuxcObsTit: TMemoField
      FieldName = 'cObsTit'
      BlobType = ftMemo
    end
    object qryTituloAuxnCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryTituloAuxnCdBancoPortador: TIntegerField
      FieldName = 'nCdBancoPortador'
    end
    object qryTituloAuxdDtRemPortador: TDateTimeField
      FieldName = 'dDtRemPortador'
    end
    object qryTituloAuxdDtBloqTit: TDateTimeField
      FieldName = 'dDtBloqTit'
    end
    object qryTituloAuxcObsBloqTit: TStringField
      FieldName = 'cObsBloqTit'
      Size = 50
    end
    object qryTituloAuxnCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryTituloAuxnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryTituloAuxdDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryTituloAuxnCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryTituloAuxdDtCalcJuro: TDateTimeField
      FieldName = 'dDtCalcJuro'
    end
    object qryTituloAuxnCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryTituloAuxnCdCrediario: TIntegerField
      FieldName = 'nCdCrediario'
    end
    object qryTituloAuxnCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTituloAuxnCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryTituloAuxcFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryTituloAuxcFlgDocCobranca: TIntegerField
      FieldName = 'cFlgDocCobranca'
    end
    object qryTituloAuxcNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTituloAuxnCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
    object qryTituloAuxcFlgDA: TIntegerField
      FieldName = 'cFlgDA'
    end
    object qryTituloAuxnCdContaBancariaDA: TIntegerField
      FieldName = 'nCdContaBancariaDA'
    end
    object qryTituloAuxnCdTipoLanctoDA: TIntegerField
      FieldName = 'nCdTipoLanctoDA'
    end
    object qryTituloAuxnCdLojaTit: TIntegerField
      FieldName = 'nCdLojaTit'
    end
    object qryTituloAuxcFlgInclusaoManual: TIntegerField
      FieldName = 'cFlgInclusaoManual'
    end
    object qryTituloAuxdDtVencOriginal: TDateTimeField
      FieldName = 'dDtVencOriginal'
    end
    object qryTituloAuxcFlgCartaoConciliado: TIntegerField
      FieldName = 'cFlgCartaoConciliado'
    end
    object qryTituloAuxnCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryTituloAuxnCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryTituloAuxnValTaxaOperadora: TBCDField
      FieldName = 'nValTaxaOperadora'
      Precision = 12
      Size = 2
    end
    object qryTituloAuxnCdLoteConciliacaoCartao: TIntegerField
      FieldName = 'nCdLoteConciliacaoCartao'
    end
    object qryTituloAuxdDtIntegracao: TDateTimeField
      FieldName = 'dDtIntegracao'
    end
    object qryTituloAuxnCdParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdParcContratoEmpImobiliario'
    end
    object qryTituloAuxnValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object qryTituloAuxnValMulta: TBCDField
      FieldName = 'nValMulta'
      Precision = 12
      Size = 2
    end
    object qryTituloAuxdDtNegativacao: TDateTimeField
      FieldName = 'dDtNegativacao'
    end
    object qryTituloAuxnCdUsuarioNeg: TIntegerField
      FieldName = 'nCdUsuarioNeg'
    end
    object qryTituloAuxnCdItemListaCobrancaNeg: TIntegerField
      FieldName = 'nCdItemListaCobrancaNeg'
    end
    object qryTituloAuxnCdLojaNeg: TIntegerField
      FieldName = 'nCdLojaNeg'
    end
    object qryTituloAuxdDtReabNeg: TDateTimeField
      FieldName = 'dDtReabNeg'
    end
    object qryTituloAuxcFlgCobradora: TIntegerField
      FieldName = 'cFlgCobradora'
    end
    object qryTituloAuxnCdCobradora: TIntegerField
      FieldName = 'nCdCobradora'
    end
    object qryTituloAuxdDtRemCobradora: TDateTimeField
      FieldName = 'dDtRemCobradora'
    end
    object qryTituloAuxnCdUsuarioRemCobradora: TIntegerField
      FieldName = 'nCdUsuarioRemCobradora'
    end
    object qryTituloAuxnCdItemListaCobRemCobradora: TIntegerField
      FieldName = 'nCdItemListaCobRemCobradora'
    end
    object qryTituloAuxcFlgReabManual: TIntegerField
      FieldName = 'cFlgReabManual'
    end
    object qryTituloAuxnCdUsuarioReabManual: TIntegerField
      FieldName = 'nCdUsuarioReabManual'
    end
    object qryTituloAuxnCdFormaPagtoTit: TIntegerField
      FieldName = 'nCdFormaPagtoTit'
    end
    object qryTituloAuxnPercTaxaJurosDia: TBCDField
      FieldName = 'nPercTaxaJurosDia'
      Precision = 12
    end
    object qryTituloAuxnPercTaxaMulta: TBCDField
      FieldName = 'nPercTaxaMulta'
      Precision = 12
    end
    object qryTituloAuxiDiaCarenciaJuros: TIntegerField
      FieldName = 'iDiaCarenciaJuros'
    end
    object qryTituloAuxcFlgRenegociado: TIntegerField
      FieldName = 'cFlgRenegociado'
    end
    object qryTituloAuxcCodBarra: TStringField
      FieldName = 'cCodBarra'
      Size = 100
    end
    object qryTituloAuxnValEncargoCobradora: TBCDField
      FieldName = 'nValEncargoCobradora'
      Precision = 12
      Size = 2
    end
    object qryTituloAuxcFlgRetCobradoraManual: TIntegerField
      FieldName = 'cFlgRetCobradoraManual'
    end
    object qryTituloAuxnCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryTituloAuxdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryTituloAuxnValIndiceCorrecao: TBCDField
      FieldName = 'nValIndiceCorrecao'
      Precision = 12
      Size = 8
    end
    object qryTituloAuxnValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      Precision = 12
      Size = 2
    end
    object qryTituloAuxcFlgNegativadoManual: TIntegerField
      FieldName = 'cFlgNegativadoManual'
    end
    object qryTituloAuxcIDExternoTit: TStringField
      FieldName = 'cIDExternoTit'
    end
    object qryTituloAuxnCdTipoAdiantamento: TIntegerField
      FieldName = 'nCdTipoAdiantamento'
    end
    object qryTituloAuxcFlgAdiantamento: TIntegerField
      FieldName = 'cFlgAdiantamento'
    end
    object qryTituloAuxnCdMTituloGerador: TIntegerField
      FieldName = 'nCdMTituloGerador'
    end
    object qryTituloAuxnValBaseCorrecao: TBCDField
      FieldName = 'nValBaseCorrecao'
      Precision = 12
      Size = 2
    end
    object qryTituloAuxcFlgBoletoImpresso: TIntegerField
      FieldName = 'cFlgBoletoImpresso'
    end
    object qryTituloAuxcFlgTituloAgrupado: TIntegerField
      FieldName = 'cFlgTituloAgrupado'
    end
  end
  object qryTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Titulo'
      'WHERE nCdTitulo = :nPk       ')
    Left = 720
    Top = 64
    object qryTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTitulonCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryTitulonCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryTitulocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTituloiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTitulonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTitulonCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryTitulonCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryTitulocSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryTitulodDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTitulodDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
    end
    object qryTitulodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulodDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
    object qryTitulodDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryTitulodDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryTitulonValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTitulonValLiq: TBCDField
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryTitulonSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulonValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTitulonValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryTitulocObsTit: TMemoField
      FieldName = 'cObsTit'
      BlobType = ftMemo
    end
    object qryTitulonCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryTitulonCdBancoPortador: TIntegerField
      FieldName = 'nCdBancoPortador'
    end
    object qryTitulodDtRemPortador: TDateTimeField
      FieldName = 'dDtRemPortador'
    end
    object qryTitulodDtBloqTit: TDateTimeField
      FieldName = 'dDtBloqTit'
    end
    object qryTitulocObsBloqTit: TStringField
      FieldName = 'cObsBloqTit'
      Size = 50
    end
    object qryTitulonCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryTitulonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryTitulodDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryTitulonCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryTitulodDtCalcJuro: TDateTimeField
      FieldName = 'dDtCalcJuro'
    end
    object qryTitulonCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryTitulonCdCrediario: TIntegerField
      FieldName = 'nCdCrediario'
    end
    object qryTitulonCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTitulonCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryTitulocFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryTitulocFlgDocCobranca: TIntegerField
      FieldName = 'cFlgDocCobranca'
    end
    object qryTitulocNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTitulonCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
    object qryTitulocFlgDA: TIntegerField
      FieldName = 'cFlgDA'
    end
    object qryTitulonCdContaBancariaDA: TIntegerField
      FieldName = 'nCdContaBancariaDA'
    end
    object qryTitulonCdTipoLanctoDA: TIntegerField
      FieldName = 'nCdTipoLanctoDA'
    end
    object qryTitulonCdLojaTit: TIntegerField
      FieldName = 'nCdLojaTit'
    end
    object qryTitulocFlgInclusaoManual: TIntegerField
      FieldName = 'cFlgInclusaoManual'
    end
    object qryTitulodDtVencOriginal: TDateTimeField
      FieldName = 'dDtVencOriginal'
    end
    object qryTitulocFlgCartaoConciliado: TIntegerField
      FieldName = 'cFlgCartaoConciliado'
    end
    object qryTitulonCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryTitulonCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryTitulonValTaxaOperadora: TBCDField
      FieldName = 'nValTaxaOperadora'
      Precision = 12
      Size = 2
    end
    object qryTitulonCdLoteConciliacaoCartao: TIntegerField
      FieldName = 'nCdLoteConciliacaoCartao'
    end
    object qryTitulodDtIntegracao: TDateTimeField
      FieldName = 'dDtIntegracao'
    end
    object qryTitulonCdParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdParcContratoEmpImobiliario'
    end
    object qryTitulonValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object qryTitulonValMulta: TBCDField
      FieldName = 'nValMulta'
      Precision = 12
      Size = 2
    end
    object qryTitulodDtNegativacao: TDateTimeField
      FieldName = 'dDtNegativacao'
    end
    object qryTitulonCdUsuarioNeg: TIntegerField
      FieldName = 'nCdUsuarioNeg'
    end
    object qryTitulonCdItemListaCobrancaNeg: TIntegerField
      FieldName = 'nCdItemListaCobrancaNeg'
    end
    object qryTitulonCdLojaNeg: TIntegerField
      FieldName = 'nCdLojaNeg'
    end
    object qryTitulodDtReabNeg: TDateTimeField
      FieldName = 'dDtReabNeg'
    end
    object qryTitulocFlgCobradora: TIntegerField
      FieldName = 'cFlgCobradora'
    end
    object qryTitulonCdCobradora: TIntegerField
      FieldName = 'nCdCobradora'
    end
    object qryTitulodDtRemCobradora: TDateTimeField
      FieldName = 'dDtRemCobradora'
    end
    object qryTitulonCdUsuarioRemCobradora: TIntegerField
      FieldName = 'nCdUsuarioRemCobradora'
    end
    object qryTitulonCdItemListaCobRemCobradora: TIntegerField
      FieldName = 'nCdItemListaCobRemCobradora'
    end
    object qryTitulocFlgReabManual: TIntegerField
      FieldName = 'cFlgReabManual'
    end
    object qryTitulonCdUsuarioReabManual: TIntegerField
      FieldName = 'nCdUsuarioReabManual'
    end
    object qryTitulonCdFormaPagtoTit: TIntegerField
      FieldName = 'nCdFormaPagtoTit'
    end
    object qryTitulonPercTaxaJurosDia: TBCDField
      FieldName = 'nPercTaxaJurosDia'
      Precision = 12
    end
    object qryTitulonPercTaxaMulta: TBCDField
      FieldName = 'nPercTaxaMulta'
      Precision = 12
    end
    object qryTituloiDiaCarenciaJuros: TIntegerField
      FieldName = 'iDiaCarenciaJuros'
    end
    object qryTitulocFlgRenegociado: TIntegerField
      FieldName = 'cFlgRenegociado'
    end
    object qryTitulocCodBarra: TStringField
      FieldName = 'cCodBarra'
      Size = 100
    end
    object qryTitulonValEncargoCobradora: TBCDField
      FieldName = 'nValEncargoCobradora'
      Precision = 12
      Size = 2
    end
    object qryTitulocFlgRetCobradoraManual: TIntegerField
      FieldName = 'cFlgRetCobradoraManual'
    end
    object qryTitulonCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryTitulodDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryTitulonValIndiceCorrecao: TBCDField
      FieldName = 'nValIndiceCorrecao'
      Precision = 12
      Size = 8
    end
    object qryTitulonValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      Precision = 12
      Size = 2
    end
    object qryTitulocFlgNegativadoManual: TIntegerField
      FieldName = 'cFlgNegativadoManual'
    end
    object qryTitulocIDExternoTit: TStringField
      FieldName = 'cIDExternoTit'
    end
    object qryTitulonCdTipoAdiantamento: TIntegerField
      FieldName = 'nCdTipoAdiantamento'
    end
    object qryTitulocFlgAdiantamento: TIntegerField
      FieldName = 'cFlgAdiantamento'
    end
    object qryTitulonCdMTituloGerador: TIntegerField
      FieldName = 'nCdMTituloGerador'
    end
    object qryTitulonValBaseCorrecao: TBCDField
      FieldName = 'nValBaseCorrecao'
      Precision = 12
      Size = 2
    end
    object qryTitulocFlgBoletoImpresso: TIntegerField
      FieldName = 'cFlgBoletoImpresso'
    end
  end
  object SP_AGRUPA_BOLETO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_AGRUPA_BOLETO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cDtVenc'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@cNrTit'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@cOBS'
        Attributes = [paNullable]
        DataType = ftString
        Size = 100
        Value = Null
      end
      item
        Name = '@nCdTituloAgrupado'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 792
    Top = 61
  end
end
