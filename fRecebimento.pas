unit fRecebimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons, DBCtrlsEh, DBLookupEh, Menus,
  cxContainer, cxEdit, cxTextEdit, GIFImage, DBGridEhGrouping, ACBrNFe,pcnConversao,
  fConferenciaProdutoPadrao,ToolCtrlsEh, ER2Lookup, cxLabel, ACBrBase,
  ACBrDFe;

type
  TfrmRecebimento = class(TfrmCadastro_Padrao)
    qryMasternCdRecebimento: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTipoReceb: TIntegerField;
    qryMasterdDtReceb: TDateTimeField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNrDocto: TStringField;
    qryMastercSerieDocto: TStringField;
    qryMasterdDtDocto: TDateTimeField;
    qryMasternValDocto: TBCDField;
    qryMasternCdTabStatusReceb: TIntegerField;
    qryMasternCdUsuarioFech: TIntegerField;
    qryMasterdDtFech: TDateTimeField;
    qryMastercOBS: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    DBEdit15: TDBEdit;
    DataSource1: TDataSource;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    DBEdit16: TDBEdit;
    DataSource2: TDataSource;
    qryTipoReceb: TADOQuery;
    qryTipoRecebnCdTipoReceb: TIntegerField;
    qryTipoRecebcNmTipoReceb: TStringField;
    qryTipoRecebcFlgExigeNF: TIntegerField;
    DBEdit17: TDBEdit;
    DataSource3: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit18: TDBEdit;
    DataSource4: TDataSource;
    qryTabStatusReceb: TADOQuery;
    qryTabStatusRecebnCdTabStatusReceb: TIntegerField;
    qryTabStatusRecebcNmTabStatusReceb: TStringField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DBEdit19: TDBEdit;
    DataSource5: TDataSource;
    DBEdit11: TDBEdit;
    DataSource6: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    qryItemRecebimento: TADOQuery;
    qryItemRecebimentonCdItemRecebimento: TIntegerField;
    qryItemRecebimentonCdRecebimento: TIntegerField;
    qryItemRecebimentonCdItemRecebimentoPai: TIntegerField;
    qryItemRecebimentonCdTipoItemPed: TIntegerField;
    qryItemRecebimentonCdProduto: TIntegerField;
    qryItemRecebimentocNmItem: TStringField;
    qryItemRecebimentonCdItemPedido: TIntegerField;
    qryItemRecebimentonQtde: TBCDField;
    qryItemRecebimentonValTotal: TBCDField;
    qryItemRecebimentocEANFornec: TStringField;
    qryItemRecebimentonCdTabStatusItemPed: TIntegerField;
    qryItemRecebimentocFlgDiverg: TIntegerField;
    qryItemRecebimentonCdTabTipoDiverg: TIntegerField;
    qryItemRecebimentonCdUsuarioAutorDiverg: TIntegerField;
    qryItemRecebimentodDtAutorDiverg: TDateTimeField;
    qryItemRecebimentonCdPedido: TIntegerField;
    dsItemRecebimento: TDataSource;
    DBGridEh1: TDBGridEh;
    qryAux: TADOQuery;
    cxButton1: TcxButton;
    qryConsultaPedido: TADOQuery;
    qryTabTipoDiverg: TADOQuery;
    qryTabTipoDivergnCdTabTipoDiverg: TIntegerField;
    qryTabTipoDivergcNmTabTipoDiverg: TStringField;
    qryTabStatusItemPed: TADOQuery;
    qryTabStatusItemPednCdTabStatusItemPed: TIntegerField;
    qryTabStatusItemPedcNmTabStatusItemPed: TStringField;
    qryTabStatusItemPedcFlgFaturar: TIntegerField;
    qryItemRecebimentocNmTabStatusItemPed: TStringField;
    qryItemRecebimentocNmTabTipoDiverg: TStringField;
    cxTabSheet4: TcxTabSheet;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryItemBarra: TADOQuery;
    qryItemBarranCdItemRecebimento: TAutoIncField;
    qryItemBarranCdRecebimento: TIntegerField;
    qryItemBarranCdItemRecebimentoPai: TIntegerField;
    qryItemBarranCdTipoItemPed: TIntegerField;
    qryItemBarranCdProduto: TIntegerField;
    qryItemBarracNmItem: TStringField;
    qryItemBarranCdItemPedido: TIntegerField;
    qryItemBarranQtde: TBCDField;
    qryItemBarranValUnitario: TBCDField;
    qryItemBarranValTotal: TBCDField;
    qryItemBarranValUnitarioEsp: TBCDField;
    qryItemBarranValUnitarioPed: TBCDField;
    qryItemBarracEANFornec: TStringField;
    qryItemBarranCdTabStatusItemPed: TIntegerField;
    qryItemBarracFlgDiverg: TIntegerField;
    qryItemBarranCdTabTipoDiverg: TIntegerField;
    qryItemBarranCdUsuarioAutorDiverg: TIntegerField;
    qryItemBarradDtAutorDiverg: TDateTimeField;
    DBGridEh2: TDBGridEh;
    qryItemBarracNmTabStatusItemPed: TStringField;
    qryItemBarracNmTabTipoDiverg: TStringField;
    dsItemBarra: TDataSource;
    qryItemBarranCdPedido: TIntegerField;
    qryPrazoRecebimento: TADOQuery;
    dsPrazoRecebimento: TDataSource;
    qryPrazoRecebimentonCdRecebimento: TIntegerField;
    qryPrazoRecebimentodDtVenc: TDateTimeField;
    qryPrazoRecebimentonValPagto: TBCDField;
    DBGridEh3: TDBGridEh;
    usp_Gera_SubItem: TADOStoredProc;
    usp_Grade: TADOStoredProc;
    qryTemp: TADOQuery;
    dsTemp: TDataSource;
    cmdExcluiSubItem: TADOCommand;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryProdutonCdGrade: TIntegerField;
    qryProdutonCdTabTipoProduto: TIntegerField;
    qryProdutonCdProdutoPai: TIntegerField;
    SP_FINALIZA_RECEBIMENTO: TADOStoredProc;
    qryMasternValTotalNF: TBCDField;
    qryMasternValTitulo: TBCDField;
    qryMasternPercDescontoVencto: TBCDField;
    qryMasternPercAcrescimoVendor: TBCDField;
    qryMasternCdUsuarioAutorFinanc: TIntegerField;
    qryMasterdDtAutorFinanc: TDateTimeField;
    qryMastercCFOP: TStringField;
    qryMastercCNPJEmissor: TStringField;
    qryMastercIEEmissor: TStringField;
    qryMastercUFEmissor: TStringField;
    qryMastercModeloNF: TStringField;
    qryMasternValBaseICMS: TBCDField;
    qryMasternValICMS: TBCDField;
    qryMasternValIsenta: TBCDField;
    qryMasternValOutras: TBCDField;
    qryMasternValIPI: TBCDField;
    TabCentroCusto: TcxTabSheet;
    DBComboItens: TDBLookupComboboxEh;
    Label26: TLabel;
    DBGridEh8: TDBGridEh;
    Image3: TImage;
    cxButton4: TcxButton;
    usp_copia_cc: TADOStoredProc;
    qryItemCC: TADOQuery;
    dsItemCC: TDataSource;
    qryCCItemRecebimento: TADOQuery;
    dsCCItemRecebimento: TDataSource;
    qryCentroCusto: TADOQuery;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocNmCC: TStringField;
    qryCentroCustonCdCC1: TIntegerField;
    qryCentroCustonCdCC2: TIntegerField;
    qryCentroCustocCdCC: TStringField;
    qryCentroCustocCdHie: TStringField;
    qryCentroCustoiNivel: TSmallintField;
    qryCentroCustocFlgLanc: TIntegerField;
    qryCentroCustonCdStatus: TIntegerField;
    qryItemCCnCdItemRecebimento: TAutoIncField;
    qryItemCCcNmItem: TStringField;
    qryCCItemRecebimentonCdItemRecebimento: TIntegerField;
    qryCCItemRecebimentonCdCC: TIntegerField;
    qryCCItemRecebimentonPercent: TBCDField;
    qryCCItemRecebimentocNmCC: TStringField;
    qryMasternValTotalItensPag: TBCDField;
    qryMasternValDescontoNF: TBCDField;
    TabLote: TcxTabSheet;
    Image4: TImage;
    qryLoteItemRecebimento: TADOQuery;
    qryLoteItemRecebimentonCdLoteItemRecebimento: TAutoIncField;
    qryLoteItemRecebimentocNrLote: TStringField;
    qryLoteItemRecebimentodDtFabricacao: TDateTimeField;
    qryLoteItemRecebimentodDtValidade: TDateTimeField;
    qryLoteItemRecebimentonQtde: TBCDField;
    dsLoteItemRecebimento: TDataSource;
    qryProdLote: TADOQuery;
    qryProdLotenCdProduto: TIntegerField;
    qryProdLotecNmProduto: TStringField;
    qryProdLotenQtde: TBCDField;
    dsProdLote: TDataSource;
    qryLoteItemRecebimentonCdRecebimento: TIntegerField;
    qryLoteItemRecebimentonCdProduto: TIntegerField;
    qryItemPagavel: TADOQuery;
    SP_GERA_PARCELA_RECEBIMENTO: TADOStoredProc;
    qryMasterdDtContab: TDateTimeField;
    qryMastercFlgIntegrado: TIntegerField;
    qryMastercFlgParcelaAutom: TIntegerField;
    qryMasternValICMSSub: TBCDField;
    qryMasternValDespesas: TBCDField;
    qryMasternValFrete: TBCDField;
    qryItemRecebimentonPercIPI: TBCDField;
    qryItemRecebimentonValIPI: TBCDField;
    qryItemRecebimentonPercICMSSub: TBCDField;
    qryItemRecebimentonValICMSSub: TBCDField;
    qryItemRecebimentonPercDesconto: TBCDField;
    qryItemRecebimentonValDesconto: TBCDField;
    qryItemBarranPercIPI: TBCDField;
    qryItemBarranValIPI: TBCDField;
    qryItemBarranPercICMSSub: TBCDField;
    qryItemBarranValICMSSub: TBCDField;
    qryItemBarranPercDesconto: TBCDField;
    qryItemBarranValDesconto: TBCDField;
    qryPrazoRecebimentonCdTitulo: TIntegerField;
    qryPrazoRecebimentocNrTit: TStringField;
    qryPrazoRecebimentoiParcela: TIntegerField;
    qryPrazoRecebimentocFlgDocCobranca: TIntegerField;
    Label29: TLabel;
    DBEdit31: TDBEdit;
    Label30: TLabel;
    DBEdit32: TDBEdit;
    Label32: TLabel;
    DBEdit34: TDBEdit;
    Label34: TLabel;
    DBEdit36: TDBEdit;
    qryMasternPercDescProduto: TBCDField;
    qryConsultaPedidonCdItemPedido: TAutoIncField;
    qryConsultaPedidonValCustoUnit: TBCDField;
    qryConsultaPedidonValUnitarioEsp: TBCDField;
    qryConsultaPedidonPercIPI: TBCDField;
    qryConsultaPedidonPercICMSSub: TBCDField;
    SP_INSERI_DIVERGENCIA_RECEB: TADOStoredProc;
    qryItemRecebimentonPercIPIPed: TBCDField;
    qryItemRecebimentonPercICMSSubPed: TBCDField;
    PopupMenu1: TPopupMenu;
    ExibirDivergncias1: TMenuItem;
    ExcluirItem1: TMenuItem;
    cxButton2: TcxButton;
    qryPrazoRecebimentonValParcela: TBCDField;
    qryPrazoRecebimentonValOperacaoFin: TBCDField;
    cxTabSheet5: TcxTabSheet;
    Image5: TImage;
    qryMastercFlgAnaliseQualidade: TIntegerField;
    qryMastercFlgAnaliseQualidadeOK: TIntegerField;
    qryMasterdDtAnaliseQualidade: TDateTimeField;
    qryMasternCdUsuarioAnaliseQualidade: TIntegerField;
    qryMastercOBSQualidade: TStringField;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    Label38: TLabel;
    DBEdit38: TDBEdit;
    Label39: TLabel;
    DBEdit39: TDBEdit;
    Label40: TLabel;
    DBEdit40: TDBEdit;
    qryUsuarioQualidade: TADOQuery;
    qryUsuarioQualidadenCdUsuario: TIntegerField;
    qryUsuarioQualidadecNmUsuario: TStringField;
    DBEdit41: TDBEdit;
    DataSource7: TDataSource;
    qryTipoRecebcFlgPrecoMedio: TIntegerField;
    qryTipoRecebcFlgRecebCego: TIntegerField;
    qryTipoRecebcFlgAguardConfirm: TIntegerField;
    qryGradeRecebimento: TADOQuery;
    qryInseriItemGrade: TADOQuery;
    qryItemRecebimentonValTotalLiq: TFloatField;
    cxButton3: TcxButton;
    qryPrazoRecebimentoiQtdeDias: TIntegerField;
    qryPrazoRecebimentocFlgDiverg: TIntegerField;
    SP_INSERI_DIVERGENCIA_PARCELA_RECEB: TADOStoredProc;
    qryTesteDupNF: TADOQuery;
    qryTesteDupNFnCdRecebimento: TIntegerField;
    qryProdutocReferencia: TStringField;
    qryConsultaPedidonValUnitario: TBCDField;
    qryItemRecebimentocFlgDescAbatUnitario: TIntegerField;
    qryItemRecebimentonValDescAbatUnit: TBCDField;
    qryCalculaST: TADOQuery;
    qryCalculaSTnValICMSSub: TBCDField;
    qryCalculaSTnPercICMSSub: TBCDField;
    qryItemRecebimentonValUnitario: TFloatField;
    qryItemRecebimentonValUnitarioEsp: TFloatField;
    qryItemRecebimentonValUnitarioPed: TFloatField;
    qryItemRecebimentonValCustoFinal: TFloatField;
    qryItemPagavelnValTotal: TFloatField;
    qryTipoRecebnCdQuestionarioCQM: TIntegerField;
    qryTipoRecebcFlgExigeCQM: TIntegerField;
    qryTipoRecebcFlgDivValor: TIntegerField;
    qryTipoRecebcFlgDivIPI: TIntegerField;
    qryTipoRecebcFlgDivICMSSub: TIntegerField;
    qryTipoRecebcFlgDivPrazoEntrega: TIntegerField;
    qryTipoRecebcFlgDivPrazoPagto: TIntegerField;
    qryTipoRecebcFlgDivGrade: TIntegerField;
    qryPrazoRecebimentocFlgAutomatica: TIntegerField;
    qryPrazoRecebimentocCodBarra: TStringField;
    qryItemEtiqueta: TADOQuery;
    qryItemEtiquetanCdProduto: TIntegerField;
    qryItemEtiquetanQtde: TBCDField;
    qryPrazoRecebimentonCdPrazoRecebimento: TIntegerField;
    qryCCItemRecebimentonCdCentroCustoItemRecebimento: TAutoIncField;
    DBEdit24: TDBEdit;
    Label25: TLabel;
    VincularPedido1: TMenuItem;
    SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO: TADOStoredProc;
    qryTipoRecebcFlgExigeNFe: TIntegerField;
    qryMastercChaveNFe: TStringField;
    qryItemRecebimentonCdServidorOrigem: TIntegerField;
    qryItemRecebimentodDtReplicacao: TDateTimeField;
    qryItemRecebimentonPercRedBaseCalcICMS: TFloatField;
    qryItemRecebimentonBaseCalcICMS: TFloatField;
    qryItemRecebimentonPercRedBaseCalcSubTrib: TFloatField;
    qryItemRecebimentonPercAliqICMS: TFloatField;
    qryItemRecebimentonValSeguro: TFloatField;
    qryItemRecebimentonValICMS: TFloatField;
    qryItemRecebimentonValDifAliqICMS: TFloatField;
    qryItemRecebimentonValFrete: TFloatField;
    qryItemRecebimentonValAcessorias: TFloatField;
    qryItemRecebimentocCdProduto: TStringField;
    qryItemRecebimentocCdST: TStringField;
    qryItemRecebimentonCdTipoICMS: TIntegerField;
    qryItemRecebimentonCdTipoIPI: TIntegerField;
    qryItemRecebimentocFlgImportacao: TIntegerField;
    qryCFOP: TADOQuery;
    qryCFOPnCdCFOP: TAutoIncField;
    qryCFOPcCFOP: TStringField;
    qryCFOPcCFOPPai: TStringField;
    qryCFOPcNmCFOP: TStringField;
    qryCFOPcFlgGeraLivroFiscal: TIntegerField;
    qryCFOPcFlgGeraCreditoICMS: TIntegerField;
    qryCFOPcFlgGeraCreditoIPI: TIntegerField;
    qryCFOPcFlgGeraCreditoPIS: TIntegerField;
    qryCFOPcFlgGeraCreditoCOFINS: TIntegerField;
    qryCFOPcFlgImportacao: TIntegerField;
    qryCFOPnCdTipoICMS: TIntegerField;
    qryCFOPnCdTipoIPI: TIntegerField;
    qryItemRecebimentocFlgGeraLivroFiscal: TIntegerField;
    qryItemRecebimentocCFOP: TStringField;
    qryItemRecebimentonValBaseCalcSubTrib: TFloatField;
    qryTipoRecebcFlgAtuPrecoVenda: TIntegerField;
    qryTipoRecebnCdServidorOrigem: TIntegerField;
    qryTipoRecebdDtReplicacao: TDateTimeField;
    qryTipoRecebcFlgPermiteItemAD: TIntegerField;
    qryTipoRecebcFlgExigeInformacoesFiscais: TIntegerField;
    qryEnderecoEmpresa: TADOQuery;
    qryEmpresanCdTerceiroEmp: TIntegerField;
    qryEnderecoEmpresanCdTerceiro: TIntegerField;
    qryEnderecoEmpresacUF: TStringField;
    qryTipoTributacaoICMS: TADOQuery;
    qryItemRecebimentocFlgGeraCreditoICMS: TIntegerField;
    qryItemRecebimentocFlgGeraCreditoIPI: TIntegerField;
    qryItemRecebimentonValAliquotaII: TFloatField;
    Label33: TLabel;
    DBEdit33: TDBEdit;
    Label35: TLabel;
    DBEdit35: TDBEdit;
    Label36: TLabel;
    DBEdit37: TDBEdit;
    Label41: TLabel;
    DBEdit42: TDBEdit;
    Label42: TLabel;
    DBEdit43: TDBEdit;
    qryMasternValSeguroNF: TBCDField;
    qryMasternPercFreteNF: TBCDField;
    qryMasternPercSeguroNF: TBCDField;
    qryMasternPercDespesaNF: TBCDField;
    qryMasternValBaseCalcICMSSub: TBCDField;
    qryItemRecebimentocNmTipoICMS: TStringField;
    qryItemRecebimentocNmTipoIPI: TStringField;
    Label19: TLabel;
    DBEdit23: TDBEdit;
    Label27: TLabel;
    DBEdit29: TDBEdit;
    qryTipoTributacaoICMSnCdTipoTributacaoICMS: TAutoIncField;
    qryTipoTributacaoICMScCdST: TStringField;
    qryTipoTributacaoICMScNmTipoTributacaoICMS: TStringField;
    qryTipoTributacaoICMS_Aux: TADOQuery;
    qryTipoTributacaoICMS_AuxnCdTipoTributacaoICMS: TAutoIncField;
    qryTipoTributacaoICMS_AuxcCdST: TStringField;
    qryTipoTributacaoICMS_AuxcNmTipoTributacaoICMS: TStringField;
    DBGridEh4: TDBGridEh;
    qryProdLotenCdItemRecebimento: TIntegerField;
    cxButton7: TcxButton;
    OpenDialog1: TOpenDialog;
    qryTerceirocCnpjCpf: TStringField;
    btRegistroLoteSerial: TcxButton;
    qryProdutoFornecedor: TADOQuery;
    qryProdutoFornecedornPercIPI: TBCDField;
    qryProdutoFornecedornPercIVA: TBCDField;
    qryProdutoFornecedornPercBCICMSSub: TBCDField;
    qryProdutoFornecedornPercICMSSub: TBCDField;
    qryTerceirocFlgOptSimples: TIntegerField;
    qryMastercUFOrigemNF: TStringField;
    Label15: TLabel;
    DBEdit12: TDBEdit;
    qryItemRecebimentonAliqICMSInterna: TFloatField;
    qryItemRecebimentonPercIVA: TFloatField;
    qryChecaEnderecoUFTerceiro: TADOQuery;
    qryItemRecebimentonPercBaseCalcIPI: TFloatField;
    qryItemRecebimentonValBaseIPI: TFloatField;
    qryItemRecebimentocNCM: TStringField;
    qryItemRecebimentocCFOPNF: TStringField;
    qryItemRecebimentocCdSTIPI: TStringField;
    qryGrupoImposto: TADOQuery;
    qryGrupoImpostocNCM: TStringField;
    btCadProduto: TcxButton;
    btCadProdutoGrade: TcxButton;
    qryMastercFlgDocEntregue: TIntegerField;
    CheckcFlgDocEntregue: TDBCheckBox;
    qryVerificaMovEstoque: TADOQuery;
    qryNaturezaOperacao: TADOQuery;
    qryNaturezaOperacaocCFOP: TStringField;
    Panel1: TPanel;
    Label16: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label17: TLabel;
    cxTextEdit4: TcxTextEdit;
    cxButton8: TcxButton;
    qryDeletaReferencias: TADOQuery;
    cxButton9: TcxButton;
    SP_GERA_RECEBIMENTO_CD: TADOStoredProc;
    qryMastercFlgRecebTransfEst: TIntegerField;
    dsDoctoRecebido: TDataSource;
    qryDoctoRecebido: TADOQuery;
    qryDoctoRecebidonCdRecebimento: TIntegerField;
    ToolButton14: TToolButton;
    btnOpcao: TToolButton;
    menuOpcao: TPopupMenu;
    btnImpProtocoloRecebimento: TMenuItem;
    btnImpListaPreSelecao: TMenuItem;
    N1: TMenuItem;
    btnEmitirEtqTermica: TMenuItem;
    btnEmitirEtqMatricial: TMenuItem;
    qryTipoRecebcFlgExigeImportacaoXMLNFe: TIntegerField;
    qryTipoRecebcFlgEscriturarEntrada: TIntegerField;
    qryTipoRecebcFlgDivSemPedido: TIntegerField;
    qryTipoRecebcFlgAtuPrecoWeb: TIntegerField;
    qryTipoRecebcFlgConfereItem: TIntegerField;
    qryItemRecebimentonQtdeConf: TFloatField;
    btnConferencia: TcxButton;
    btnVisualizaConfe: TcxButton;
    qryVerificaItemRecebDivergencia: TADOQuery;
    tabItemSemPedido: TcxTabSheet;
    DBGridEh5: TDBGridEh;
    qryItemRecebidoSemPedido: TADOQuery;
    qryItemRecebidoSemPedidonCdItemRecebimento: TIntegerField;
    qryItemRecebidoSemPedidocCdProduto: TStringField;
    qryItemRecebidoSemPedidocNmItem: TStringField;
    qryItemRecebidoSemPedidonQtde: TBCDField;
    qryItemRecebidoSemPedidonCdTipoPedidoRecebido: TIntegerField;
    qryItemRecebidoSemPedidocNmTipoPedido: TStringField;
    dsItemRecebidoSemPedido: TDataSource;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    Label20: TLabel;
    DBEdit20: TDBEdit;
    dsTipoPedido: TDataSource;
    qryTipoPedidoAux: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    dsTipoPedidoAux: TDataSource;
    Image2: TImage;
    Label18: TLabel;
    ER2LookupMaskEdit1: TER2LookupMaskEdit;
    DBEdit21: TDBEdit;
    btAplicaTipoPedido: TcxButton;
    ACBrNFe1: TACBrNFe;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasterdDtCad: TDateTimeField;
    qryUsuarioCad: TADOQuery;
    qryUsuarioCadnCdUsuario: TIntegerField;
    qryUsuarioCadcNmUsuario: TStringField;
    DBEdit22: TDBEdit;
    dsUsuarioCad: TDataSource;
    Label21: TLabel;
    DBEdit25: TDBEdit;
    Label22: TLabel;
    qryPopulaConfPadrao: TADOQuery;
    qryPopulaConfPadraonCdProduto: TIntegerField;
    qryPopulaConfPadraocNmProduto: TStringField;
    qryPopulaConfPadraocEAN: TStringField;
    qryPopulaConfPadraocEANFornec: TStringField;
    qryGravaConfPadrao: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure qryItemRecebimentoCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryItemRecebimentoBeforePost(DataSet: TDataSet);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure qryItemBarraBeforePost(DataSet: TDataSet);
    procedure DBGridEh2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure btCancelarClick(Sender: TObject);
    procedure DBGridEh2Enter(Sender: TObject);
    procedure qryPrazoRecebimentoBeforePost(DataSet: TDataSet);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure qryTempAfterCancel(DataSet: TDataSet);
    procedure qryItemRecebimentoAfterPost(DataSet: TDataSet);
    procedure qryItemRecebimentoBeforeDelete(DataSet: TDataSet);
    procedure btnImpProtocoloRecebimentoClick(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryCCItemRecebimentoBeforePost(DataSet: TDataSet);
    procedure qryCCItemRecebimentoCalcFields(DataSet: TDataSet);
    procedure cxButton4Click(Sender: TObject);
    procedure DBGridEh8KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxPageControl1Change(Sender: TObject);
    procedure DBComboItensChange(Sender: TObject);
    procedure qryLoteItemRecebimentoBeforePost(DataSet: TDataSet);
    procedure DBComboProdutosChange(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ExibirDivergncias1Click(Sender: TObject);
    procedure ExcluirItem1Click(Sender: TObject);
    procedure qryTipoRecebAfterScroll(DataSet: TDataSet);
    procedure usp_GradeAfterPost(DataSet: TDataSet);
    procedure usp_GradeAfterCancel(DataSet: TDataSet);
    procedure qryGradeRecebimentoAfterCancel(DataSet: TDataSet);
    procedure qryGradeRecebimentoAfterPost(DataSet: TDataSet);
    procedure cxButton3Click(Sender: TObject);
    procedure DBGridEh3DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure btnEmitirEtqMatricialClick(Sender: TObject);
    procedure btnEmitirEtqTermicaClick(Sender: TObject);
    function gerarProduto() : integer;
    procedure DBGridEh1KeyPress(Sender: TObject; var Key: Char);
    procedure qryItemRecebimentoAfterScroll(DataSet: TDataSet);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure vinculaPedidoitemRecebimento (nCdItemRecebimento:integer; nCdProduto:integer);
    procedure VincularPedido1Click(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure verificaControlesICMS ( cCST : string );
    procedure qryItemRecebimentocCdSTChange(Sender: TField);
    procedure qryItemRecebimentocCFOPChange(Sender: TField);
    procedure DBGridEh4DblClick(Sender: TObject);
    procedure btRegistroLoteSerialClick(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure atualizaColunasInformacoesFiscais();
    procedure sugerirCalculoST (bCalcularIPI : boolean);
    procedure DBEdit12Exit(Sender: TObject);
    procedure checaValidadeCFOP( cCFOP : string ; bCFOPEntrada : boolean );
    procedure qryItemRecebimentocCFOPNFChange(Sender: TField);
    procedure calculaTotaisItem( bGravar : boolean );
    procedure btCadProdutoClick(Sender: TObject);
    procedure btCadProdutoGradeClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure btnImpListaPreSelecaoClick(Sender: TObject);
    procedure btnConferenciaClick(Sender: TObject);
    procedure btnVisualizaConfeClick(Sender: TObject);
    procedure tabItemSemPedidoShow(Sender: TObject);
    procedure DBGridEh5KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryItemRecebidoSemPedidonCdTipoPedidoRecebidoChange(
      Sender: TField);
    procedure qryItemRecebidoSemPedidoCalcFields(DataSet: TDataSet);
    procedure btAplicaTipoPedidoClick(Sender: TObject);
    procedure qryItemRecebidoSemPedidoBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    cTipoEAN          : string;
    iDiasMinVenc      : integer ;
    nCdPedidoConsulta : integer ;
    cSugerirST        : string ;
    bReadOnly         : Boolean;
    procedure VisulizaItensConferidos();
  public
    { Public declarations }
  end;

var
  frmRecebimento: TfrmRecebimento;

implementation

uses fMenu, fLookup_Padrao, fConsultaItemPedidoAberto, rRecebimento_view,
  fRecebimento_Divergencias, fMontaGrade, fRecebimento_Etiqueta,
  fModImpETP, fRecebimento_CadProduto,fRegistroMovLote,fRecebimentoLeituraXML, strUtils,
  fProdutoERP, fProduto, rItensPreDistribuicao_View,fRecebimento_Coferencia,rItensSeparacaoDistribuicao;

{$R *.dfm}

var
   objConsultaItemPedidoAberto : TfrmConsultaItemPedidoAberto ;
   objGrade                    : TfrmMontaGrade ;
   objGeraProduto              : TfrmRecebimento_CadProduto ;

procedure TfrmRecebimento.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'RECEBIMENTO' ;
  nCdTabelaSistema  := 41 ;
  nCdConsultaPadrao := 89 ;
  bLimpaAposSalvar  := False ;
end;

procedure TfrmRecebimento.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryEmpresa,DbEdit2.Text) ;
  if (qryEmpresa.Active) and (qryEmpresanCdTerceiroEmp.Value <> 0) then
        PosicionaQuery(qryEnderecoEmpresa,qryEmpresanCdTerceiroEmp.AsString);
end;

procedure TfrmRecebimento.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  if qryEmpresa.Active then
      qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, DbEdit3.Text) ;
end;

procedure TfrmRecebimento.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTipoReceb,DBEdit4.Text) ;

  atualizaColunasInformacoesFiscais() ;

end;

procedure TfrmRecebimento.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  if (trim(DBEdit4.Text) = '') then
  begin
      MensagemAlerta('Informe o tipo de recebimento.') ;
      DBEdit4.SetFocus;
      exit ;
  end ;

  qryTerceiro.Parameters.ParamByName('nCdTipoReceb').Value := qryTipoRecebnCdTipoReceb.Value ;

  PosicionaQuery(qryTerceiro, DBEdit6.Text) ;

  if (DBEdit18.Text <> '') and (qryTipoRecebcFlgExigeNF.Value = 1) then
  begin
      qryTesteDupNF.Close ;
      qryTesteDupNF.Parameters.ParamByName('cNrDocto').Value    := DBEdit7.Text ;
      qryTesteDupNF.Parameters.ParamByName('cSerieDocto').Value := DBEdit8.Text ;
      qryTesteDupNF.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value ;
      qryTesteDupNF.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdRecebimento.Value ;
      qryTesteDupNF.Open ;

      if not qryTesteDUPnf.Eof then
      begin
          MensagemAlerta('Esta nota fiscal j� foi recebida no recebimento No : ' + qryTesteDupNFnCdRecebimento.AsString) ;
      end ;

      qryTesteDupNf.Close ;

  end ;

end;

procedure TfrmRecebimento.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (qryMastercFlgRecebTransfEst.Value = 1) then
  begin
      MensagemAlerta('Recebimento gerado de forma autom�tica n�o permite altera��es.');
      Abort;
  end;

  if DBEdit3.Enabled and (Trim(DbEdit16.Text) = '') then
  begin
      MensagemAlerta('Informe a Loja.') ;
      DbEdit3.SetFocus;
      abort ;
  end ;

  if (trim(DbEdit17.Text) = '') then
  begin
      MensagemAlerta('Informe o Tipo de Recebimento.') ;
      dbEdit4.SetFocus;
      abort ;
  end ;

  if (qryTipoRecebcFlgRecebCego.Value = 0) then
  begin

      if (trim(DbEdit18.Text) = '') then
      begin
          MensagemAlerta('Informe o Terceiro.') ;
          DBEdit6.SetFocus;
          abort ;
      end ;

      if (qryTipoRecebcFlgExigeNF.Value = 1) then
      begin

          if (trim(DbEdit7.Text) = '') then
          begin
              MensagemAlerta('Informe o N�mero da Nota Fiscal.') ;
              DbEdit7.SetFocus;
              abort ;
          end ;

          if (frmMenu.LeParametro('PERMNUMCAMPONF') = 'S') then
          begin
              qryMastercNrDocto.Value := IntToStr(frmMenu.ConvInteiro(TrimLeft(TrimRight(qryMastercNrDocto.Value))));
          end;

          if (trim(DbEdit8.Text) = '') then
          begin
              qryMastercSerieDocto.Value := 'UN' ;
          end ;

          if (trim(qryMasterdDtDocto.AsString) = '/  /') then
          begin
              MensagemAlerta('Informe a data da nota fiscal.') ;
              DbEdit9.SetFocus;
              abort ;
          end ;

          if (qryMasterdDtDocto.Value > Now()) then
          begin
              MensagemAlerta('A data de emiss�o n�o pode ser maior que hoje.') ;
              DbEdit9.SetFocus;
              abort ;
          end ;

          if (qryMasternValTotalNF.Value <= 0) then
          begin
              MensagemAlerta('Informe o total da nota fiscal.') ;
              DBEdit36.SetFocus;
              abort ;
          end ;

      end ;

      if (qryMasternValDocto.Value <= 0) then
      begin
          MensagemAlerta('Informe o valor total dos produtos.') ;
          DbEdit10.SetFocus;
          abort ;
      end ;

  end ;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabStatusReceb.Value := 1 ;
      qryMasterdDtReceb.Value          := StrToDate(DateToStr(Now())) ;
      qryMasternCdUsuarioCad.Value     := frmMenu.nCdUsuarioLogado;
      qryMasterdDtCad.Value            := Now();
  end ;

  if (qryMasternValBaseCalcICMSSub.asString = '') then
      qryMasternValBaseCalcICMSSub.Value := 0 ;

  if (qryMasternValICMSSub.asString = '') then
      qryMasternValICMSSub.Value := 0 ;

  if (qryMasternValICMS.asString = '') then
      qryMasternValICMS.Value := 0 ;

  if (qryMasternValDocto.asString = '') then
      qryMasternValDocto.Value := 0 ;

  if (qryMasternValSeguroNF.asString = '') then
      qryMasternValSeguroNF.Value := 0 ;

  if (qryMasternValFrete.asString = '') then
      qryMasternValFrete.Value := 0 ;

  if (qryMasternValDespesas.asString = '') then
      qryMasternValDespesas.Value := 0 ;

  if (qryMasternValIPI.asString = '') then
      qryMasternValIPI.Value := 0 ;

  if (qryMasternValDescontoNF.asString = '') then
      qryMasternValDescontoNF.Value := 0 ;

  if (qryMasternValTotalNF.asString = '') then
      qryMasternValTotalNF.Value := 0 ;


  if (qryTipoRecebcFlgExigeNF.Value = 1) and (qryTipoRecebcFlgRecebCego.Value = 0) then
  begin

      if (ABS(qryMasternValTotalNF.Value - (qryMasternValDocto.Value - qryMasternValDescontoNF.Value + qryMasternValIPI.Value + qryMasternValICMSSub.Value + qryMasternValDespesas.Value + qryMasternValSeguroNF.Value + qryMasternValFrete.Value)) > 1) then
      begin
          MensagemAlerta('Total dos valores digitados n�o confere com o total da nota fiscal.') ;
          DBEdit42.SetFocus;
          abort ;
      end ;

      qryMasternPercDescProduto.Value := 0 ;
      qryMasternPercDespesaNF.Value   := 0 ;
      qryMasternPercFreteNF.Value     := 0 ;
      qryMasternPercSeguroNF.Value    := 0 ;

      if (qryMasternValDescontoNF.Value > 0) then
          qryMasternPercDescProduto.Value := (qryMasternValDescontoNF.Value / qryMasternValDocto.Value) *100;

      if (qryMasternValDespesas.Value > 0) then
          qryMasternPercDespesaNF.Value := (qryMasternValDespesas.Value / qryMasternValDocto.Value) *100;

      if (qryMasternValFrete.Value > 0) then
          qryMasternPercFreteNF.Value := (qryMasternValFrete.Value / qryMasternValDocto.Value) *100;

      if (qryMasternValSeguroNF.Value > 0) then
         qryMasternPercSeguroNF.Value := (qryMasternValSeguroNF.Value / qryMasternValDocto.Value) *100;

  end ;

  inherited;

end;

procedure TfrmRecebimento.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

  if (qryMaster.State = dsInsert) then
      exit ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;

  PosicionaQuery(qryTipoReceb,qryMasternCdTipoReceb.AsString) ;

  qryTerceiro.Parameters.ParamByName('nCdTipoReceb').Value := qryTipoRecebnCdTipoReceb.Value ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  PosicionaQuery(qryItemRecebimento, qryMasternCdRecebimento.AsString) ;
  PosicionaQuery(qryItemBarra, qryMasternCdRecebimento.AsString) ;
  PosicionaQuery(qryPrazoRecebimento, qryMasternCdRecebimento.AsString) ;
  PosicionaQuery(qryUsuario,qryMasternCdUsuarioFech.asString) ;
  PosicionaQuery(qryUsuarioCad,qryMasternCdUsuarioCad.asString) ;

  PosicionaQuery(qryUsuarioQualidade, qryMasternCdUsuarioAnaliseQualidade.AsString) ;

  PosicionaQuery(qryTabStatusReceb,qryMasternCdTabStatusReceb.asString) ;

  DBGridEh1.ReadOnly := False ;
  DBGridEh2.ReadOnly := False ;
  DBGridEh3.ReadOnly := False ;

  if (qryMastercFlgRecebTransfEst.Value = 1) then
  begin
      DBGridEh1.ReadOnly := True ;
      DBGridEh2.ReadOnly := True ;
  end;

  if qryMaster.Active and (qryMasternCdTabStatusReceb.Value > 2) then
  begin
      DBGridEh1.ReadOnly := True ;
      DBGridEh2.ReadOnly := True ;
      DBGridEh3.ReadOnly := True ;
  end ;

  PosicionaQuery(qryItemCC, qryMasternCdRecebimento.AsString) ;

  atualizaColunasInformacoesFiscais() ;

end;

procedure TfrmRecebimento.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryItemRecebimento, qryMasternCdRecebimento.AsString) ;
  PosicionaQuery(qryItemBarra, qryMasternCdRecebimento.AsString) ;
  PosicionaQuery(qryPrazoRecebimento, qryMasternCdRecebimento.AsString) ;
  PosicionaQuery(qryUsuarioCad, qryMasternCdUsuarioCad.AsString) ;
end;

procedure TfrmRecebimento.btIncluirClick(Sender: TObject);
begin
  inherited;

  nCdPedidoConsulta := 0 ;
  
  DBGridEh1.ReadOnly := False ;
  DBGridEh2.ReadOnly := False ;
  DBGridEh3.ReadOnly := False ;

  qryMasternCdEmpresa.Value                          := frmMenu.nCdEmpresaAtiva;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  PosicionaQuery(qryEmpresa, DbEdit2.Text) ;

  If not DBEdit3.Enabled then
      DBEdit4.SetFocus
  else
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryaux.Fields.Clear ;
      qryAux.SQL.Add('SELECT nCdLoja FROM UsuarioLoja WHERE nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado)) ;
      qryAux.Fields.Clear;
      qryAux.Open ;

      if qryAux.eof then
      begin
          MensagemAlerta('Nenhuma loja vinculada para este usu�rio.') ;
          btcancelar.Click;
          abort ;
      end ;

      DbEdit3.SetFocus ;

      if (qryAux.RecordCount = 1) then
      begin
          qryMasternCdLoja.Value := qryAux.FieldList[0].Value ;
          PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;
          DBEdit4.SetFocus;
      end ;

      qryAux.Close ;

  end ;

end;

procedure TfrmRecebimento.FormShow(Sender: TObject);
begin
  inherited;

  objConsultaItemPedidoAberto := TfrmConsultaItemPedidoAberto.Create( Self ) ;
  objGrade                    := TfrmMontaGrade.Create( Self ) ;
  objGeraProduto              := TfrmRecebimento_CadProduto.Create( Self ) ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value          := frmMenu.nCdUsuarioLogado;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value          := frmMenu.nCdEmpresaAtiva;
  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value    := frmMenu.nCdUsuarioLogado;
  qryTipoPedidoAux.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  cTipoEAN     := frmMenu.LeParametro('EANRECEB') ;
  iDiasMinVenc := StrToint(frmMenu.LeParametro('DIAMINVENC')) ;

  btCadProduto.Enabled      := (frmMenu.fnValidaUsuarioAPL('FRMPRODUTOERP')) ;
  btCadProdutoGrade.Enabled := (frmMenu.fnValidaUsuarioAPL('FRMPRODUTO')) ;

  cxPageControl1.ActivePageIndex := 0 ;

  TabLote.Enabled     := True ;
  cxTabSheet5.Enabled := True ;

  if (frmMenu.LeParametroEmpresa('LOTEPROD') <> 'S') then
  begin
      TabLote.Enabled     := False ;
      cxTabSheet5.Enabled := False ;
  end ;

  DBEdit3.Enabled := False ;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      DBEdit3.Enabled := True ;
  end ;

  cSugerirST := frmMenu.LeParametro('SUGERIRSTREC') ;

  {if (cSugerirST = 'S') then
  begin
      DBGridEh1.Columns[13].ReadOnly         := True ;
      DBGridEh1.Columns[13].Title.Font.Color := clRed ;
  end ;}

  {-- formata a casa da quantidade sem decimal --}
  DBGridEh1.FieldColumns['nQtde'].DisplayFormat := ',0';

  qryItemRecebimentonValUnitario.DisplayFormat    := frmMenu.cMascaraCompras;
  qryItemRecebimentonValUnitarioEsp.DisplayFormat := frmMenu.cMascaraCompras;
  qryItemRecebimentonValUnitarioPed.DisplayFormat := frmMenu.cMascaraCompras;
  qryItemRecebimentonValCustoFinal.DisplayFormat  := frmMenu.cMascaraCompras;

  if (qryEmpresa.Active) and (qryEmpresanCdTerceiroEmp.Value <> 0) then
          PosicionaQuery(qryEnderecoEmpresa,qryEmpresanCdTerceiroEmp.AsString);

  if (frmMenu.LeParametro('PERMNUMCAMPONF') = 'S') then
  begin
      qryMastercNrDocto.EditMask := '!99999999999999999;1;';
  end;
end;

procedure TfrmRecebimento.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(59,'Loja.nCdEmpresa = ' + intToStr(frmMenu.nCdEmpresaAtiva));

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRecebimento.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(173,'EXISTS(SELECT 1 FROM TipoPedido INNER JOIN TipoPedidoTipoReceb TPTR ON TPTR.nCdTipoPedido = TipoPedido.nCdTipoPedido WHERE TPTR.nCdTipoReceb = ' + qryTipoRecebnCdTipoReceb.AsString + ')');

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRecebimento.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(88);

            If (nPK > 0) then
            begin
                qryMasternCdTipoReceb.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRecebimento.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      abort ;

  if (qryMasternCdRecebimento.Value = 0) then
  begin
      qryMaster.Post ;
  end ;

end;

procedure TfrmRecebimento.cxButton1Click(Sender: TObject);
begin

    if (not qryMaster.Active) or (qryMasternCdRecebimento.Value = 0) then
    begin
        MensagemAlerta('Salve os dados recebimento antes de consultar os pedidos.') ;
        abort ;
    end ;

    if qryMaster.Active and (qryMasternCdTabStatusReceb.Value > 2) then
    begin
       MensagemAlerta('Recebimento j� finalizado, imposs�vel alterar.') ;
       abort ;
    end ;

    if (qryMaster.State <> dsBrowse) then
    begin
      qryMaster.Post ;
    end ;

    cxPageControl1.ActivePageIndex := 0 ;

    objConsultaItemPedidoAberto.nCdTerceiro    := qryMasternCdTerceiro.Value ;
    objConsultaItemPedidoAberto.nCdTipoReceb   := qryMasternCdTipoReceb.Value ;
    objConsultaItemPedidoAberto.nCdLoja        := qryMasternCdLoja.Value ;
    objConsultaItemPedidoAberto.nCdRecebimento := qryMasternCdRecebimento.Value ;
    objConsultaItemPedidoAberto.nCdPedido      := nCdPedidoConsulta ;
    objConsultaItemPedidoAberto.nCdProduto     := 0 ;

    showForm( objConsultaItemPedidoAberto , FALSE ) ;

    if (objConsultaItemPedidoAberto.qryItemPedido.Active) and (objConsultaItemPedidoAberto.bSelecionado) and (objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.Value > 0) then
    begin

        qryItemRecebimento.Insert ;
        qryItemRecebimentonCdRecebimento.Value := qryMasternCdRecebimento.Value ;

        nCdPedidoConsulta := objConsultaItemPedidoAberto.qryPedidonCdPedido.Value ;

        if (objConsultaItemPedidoAberto.qryItemPedidonCdProduto.Value > 0) then
        begin
            qryItemRecebimentonCdProduto.Value := objConsultaItemPedidoAberto.qryItemPedidonCdProduto.Value ;
            qryItemRecebimentocCdProduto.Value := qryItemRecebimentonCdProduto.AsString;
        end;

        qryItemRecebimentocCdProduto.Value     := objConsultaItemPedidoAberto.qryItemPedidonCdProduto.asString ;

        if (objConsultaItemPedidoAberto.qryItemPedidocCdProduto.Value <> '') then
            qryItemRecebimentocCdProduto.Value     := objConsultaItemPedidoAberto.qryItemPedidocCdProduto.Value ;

        qryItemRecebimentocNmItem.Value        := objConsultaItemPedidoAberto.qryItemPedidocNmItem.Value ;
        qryItemRecebimentonQtde.Value          := objConsultaItemPedidoAberto.qryItemPedidonQtde.Value ;
        qryItemRecebimentonValUnitario.Value   := 0 ;
        qryItemRecebimentonValTotal.Value      := 0 ;

        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.Fields.Clear;
        qryAux.SQL.Add('SELECT ItemPedido.nCdTipoItemPed') ;
        qryAux.SQL.Add('      ,CASE WHEN ItemPedido.nCdTipoItemPed = 4 THEN (ItemPedido.nValUnitario - Pai.nValDesconto + Pai.nValAcrescimo)') ;
        qryAux.SQL.Add('            ELSE (ItemPedido.nValUnitario - ItemPedido.nValDesconto + ItemPedido.nValAcrescimo)') ;
        qryAux.SQL.Add('       END nValUnitario') ;
        qryAux.SQL.Add('      ,ItemPedido.nValUnitarioEsp') ;
        qryAux.SQL.Add('      ,ItemPedido.nCdPedido') ;
        qryAux.SQL.Add('      ,ItemPedido.nPercIPI') ;
        qryAux.SQL.Add('      ,ItemPedido.nPercICMSSub') ;
        qryAux.SQL.Add('  FROM ItemPedido') ;
        qryAux.SQL.Add('       LEFT JOIN ItemPedido Pai ON Pai.nCdItemPedido = ItemPedido.nCdItemPedidoPai') ;
        qryAux.SQL.Add(' WHERE ItemPedido.nCdItemPedido = ' + objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.AsString) ;
        qryAux.Open ;

        if not qryAux.eof then
        begin
            qryItemRecebimentonCdTipoItemPed.Value  := qryAux.FieldList[0].Value ;
            qryItemRecebimentonValUnitarioPed.Value := qryAux.FieldList[1].AsFloat;
            qryItemRecebimentonValUnitarioEsp.Value := qryAux.FieldList[2].AsFloat;
            qryItemRecebimentonCdPedido.Value       := qryAux.FieldList[3].Value ;
            qryItemRecebimentonPercIPIPed.Value     := qryAux.FieldList[4].AsFloat;
            qryItemRecebimentonPercICMSSubPed.Value := qryAux.FieldList[5].AsFloat;
        end
        else begin
            qryItemRecebimentonValUnitarioPed.Value := 0 ;
            qryItemRecebimentonValUnitarioEsp.Value := 0 ;
        end ;

        qryAux.Close ;

        qryItemRecebimentonCdItemPedido.Value  := objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.Value ;

        if (qryItemRecebimentonCdProduto.asString <> '') then
        begin

              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT nCdGrade FROM Produto WHERE nCdTabTipoProduto = 2 AND nCdProduto = ' + qryItemRecebimentonCdProduto.asString) ;
              qryAux.Fields.Clear ;
              qryAux.Open ;

              if (not qryAux.eof) and (qryAux.FieldList[0].AsString <> '') then
              begin

                objGrade.qryValorAnterior.SQL.Text := '' ;
                objGrade.PreparaDataSet(qryItemRecebimentonCdProduto.Value);
                objGrade.Renderiza;

                qryItemRecebimentonQtde.Value := objGrade.nQtdeTotal ;

                DBGridEh1.SetFocus;
                DBGridEh1.Col := 5 ;

            end
            else begin
                DBGridEh1.SetFocus ;
                DBGridEh1.Col := 5 ;
            end ;
        end
        else begin
            //qryItemRecebimento.Post;
            DBGridEh1.SetFocus ;
        end ;


        DBGridEh1.Col := 5 ;
    end ;

end;

procedure TfrmRecebimento.qryItemRecebimentoCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryItemRecebimento.State = dsCalcFields) then
  begin

      if (qryItemRecebimento.Active) then
          qryItemRecebimentonValTotalLiq.Value := qryItemRecebimentonValTotal.Value - qryItemRecebimentonValDesconto.Value ;

      if qryItemRecebimentonCdItemPedido.asString = '' then
          exit ;

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT nCdPedido FROM ItemPedido WHERE nCdItemPedido = ' + qryItemRecebimentonCdItemPedido.AsString) ;
      qryAux.Fields.Clear;
      qryAux.Open ;

      if not qryAux.eof then
          qryItemRecebimentonCdPedido.Value := qryAux.FieldList[0].Value ;

      qryAux.Close ;

  end ;

end;

procedure TfrmRecebimento.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTipoReceb.Close ;
  qryTerceiro.Close ;
  qryItemRecebimento.Close ;
  qryItemBarra.Close ;
  qryUsuario.Close ;
  qryUsuarioCad.Close;
  qryTabStatusReceb.Close ;
  qryPrazoRecebimento.Close ;
  qryItemCC.Close ;
  qryUsuario.Close ;
  qryUsuarioQualidade.Close ;

end;

procedure TfrmRecebimento.qryItemRecebimentoBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryItemRecebimentocCdProduto.Value <> 'AD') and (qryItemRecebimentonCdProduto.Value = 0) and (qryItemRecebimentonCdTipoItemPed.Value <> 5) then
  begin
      MensagemAlerta('Informe o produto ou pressione ESC.') ;
      DbGridEh1.Col := 1 ;
      abort ;
  end ;

  if (qryItemRecebimentocCdProduto.Value = 'AD') and (qryItemRecebimentonCdTipoItemPed.Value = 5) and (Trim(qryItemRecebimentocNmItem.Value) = '') then
  begin
      MensagemAlerta('Informe a Descri��o do produto AD.') ;
      DbGridEh1.Col := 4 ;
      abort ;
  end ;

  if (qryItemRecebimentonQtde.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade recebida.') ;
      abort ;
  end ;

  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
  begin

      if Trim(qryItemRecebimentocCdSt.Value) = '' then
      begin
          MensagemAlerta('CST do item n�o informado.');
          abort;
      end;

      if Trim(qryItemRecebimentocCfopNF.Value) = '' then
      begin
          MensagemAlerta('Informe o CFOP da NF.');
          abort;
      end;

      checaValidadeCFOP( qryItemRecebimentocCFOPNF.Value , FALSE ) ;

      if Trim(qryItemRecebimentocCfop.Value) = '' then
      begin
          MensagemAlerta('Informe o CFOP de entrada.');
          abort;
      end;

      checaValidadeCFOP( qryItemRecebimentocCFOP.Value , TRUE ) ;

      if     (length(trim(qryItemRecebimentocNCM.Value)) <> 2)
         and (length(trim(qryItemRecebimentocNCM.Value)) <> 8) then
      begin
          MensagemAlerta('NCM/SH Inv�lido. O NCM deve ter 02 ou 08 d�gitos.') ;
          abort;
      end ;

      { -- valida NCM -- }
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM TabIBPT WHERE cNCM = ' + #39 + qryItemRecebimentocNCM.Value + #39);
      qryAux.Open;

      if (qryAux.IsEmpty) then
      begin
          MensagemAlerta('NCM ' + qryItemRecebimentocNCM.Value + ' n�o cadastrado.');
          DBGridEh1.SelectedField := qryItemRecebimentocNCM;
          DBGridEh1.SetFocus;
          Abort;
      end;

      if (qryTerceirocFlgOptSimples.Value = 1) then
      begin

          if (qryItemRecebimentocFlgGeraCreditoICMS.Value = 1) then
          begin
              MensagemAlerta('O Terceiro fornecedor � optante pelo simples e n�o pode gerar cr�dito de ICMS.') ;
              abort ;
          end ;

          if (qryItemRecebimentocFlgGeraCreditoIPI.Value = 1) then
          begin
              MensagemAlerta('O Terceiro fornecedor � optante pelo simples e n�o pode gerar cr�dito de IPI.') ;
              abort ;
          end ;

      end ;

  end;

  if (qryItemRecebimentonValUnitario.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor unit�rio.') ;
      abort ;
  end ;

  {-- RECALCULA OS TOTAIS DO ITEM E SEUS IMPOSTOS --}
  calculaTotaisItem( TRUE ) ;

  if (qryItemRecebimentonValICMS.Value < 0) then
  begin
      MensagemAlerta('Valor do ICMS n�o pode ser negativo.') ;
      abort ;
  end ;

  if (qryItemRecebimentonValIPI.Value < 0) then
  begin
      MensagemAlerta('Valor do IPI n�o pode ser negativo.') ;
      abort ;
  end ;

  if (qryItemRecebimentonValICMSSub.Value < 0) then
  begin
      MensagemAlerta('Valor do ICMS de Substitui��o Tribut�ria n�o pode ser negativo.') ;
      abort ;
  end ;

  qryItemRecebimentonCdTabStatusItemPed.Value  := 1 ;
  qryItemRecebimentonCdRecebimento.Value       := qryMasternCdRecebimento.Value ;
  qryItemRecebimentocFlgDiverg.Value           := 0 ;
  qryItemRecebimentonCdTabTipoDiverg.AsVariant := Null ;

  // calculo custo final unitario
  qryItemRecebimentonValCustoFinal.Value := ((qryItemRecebimentonValTotal.Value - qryItemRecebimentonValDesconto.Value   + qryItemRecebimentonValIPI.Value   + qryItemRecebimentonValICMSSub.Value  +
                                              qryItemRecebimentonValAcessorias.Value + qryItemRecebimentonValFrete.Value + qryItemRecebimentonValSeguro.Value) / qryItemRecebimentonQtde.Value) ;

  if (qryItemRecebimentonCdItemPedido.Value > 0) and (qryItemRecebimento.State = dsInsert) then
  begin

    qryAux.Close ;
    qryAux.Fields.Clear ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('SELECT 1 FROM ItemRecebimento WHERE nCdItemPedido = ' + qryItemRecebimentonCdItemPedido.asString + ' AND nCdRecebimento = ' + qryMasternCdRecebimento.asString) ;
    qryAux.Open ;

    if not qryAux.Eof then
    begin
        MensagemAlerta('Este Item j� est� digitado neste recebimento.') ;
        qryAux.Close ;
        qryItemRecebimento.Cancel ;
        abort ;
    end ;

    qryAux.Close ;

  end ;

  if (qryItemRecebimentonCdItemPedido.Value = 0) then
  begin

      qryConsultaPedido.Close ;
      qryConsultaPedido.Parameters.ParamByName('nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva            ;

      if (qryLoja.Active) then
          qryConsultaPedido.Parameters.ParamByName('nCdLoja').Value    := qryLojanCdLoja.Value               ;

      if (qryTerceiro.Active) and (not qryTerceiro.Eof) then
          qryConsultaPedido.Parameters.ParamByName('nCdTerceiro').Value    := qryTerceironCdTerceiro.Value
      else qryConsultaPedido.Parameters.ParamByName('nCdTerceiro').Value := 0 ;

      qryConsultaPedido.Parameters.ParamByName('nCdTipoReceb').Value   := qryTipoRecebnCdTipoReceb.Value     ;
      qryConsultaPedido.Parameters.ParamByName('nCdProduto').Value     := qryItemRecebimentonCdProduto.Value ;
      qryConsultaPedido.Parameters.ParamByName('nCdRecebimento').Value := qryMasternCdRecebimento.Value      ;
      qryConsultaPedido.Open ;

      if not qryConsultaPedido.eof then
      begin

          if (qryConsultaPedido.FieldList[0].Value <> null) then
          begin
              //qryItemRecebimentonCdItemPedido.Value   := qryConsultaPedido.FieldValues['nCdItemPedido'];
              qryItemRecebimentonCdItemPedido.Value   := qryConsultaPedido.FieldList[0].Value ;
              qryItemRecebimentonValUnitarioPed.Value := qryConsultaPedido.FieldList[5].Value ;
              qryItemRecebimentonValUnitarioEsp.Value := qryConsultaPedido.FieldList[2].Value ;
          end ;

      end ;

      qryConsultaPedido.Close ;
  end ;

  if (qryItemRecebimento.State = dsInsert) then
      qryItemRecebimentonCdItemRecebimento.Value := frmMenu.fnProximoCodigo('ITEMRECEBIMENTO') ;

end;

procedure TfrmRecebimento.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (qryItemRecebimentocFlgDescAbatUnitario.Value = 0) AND (qryItemRecebimentonValDescAbatUnit.Value > 0) then
  begin

    if (Column = DBGridEh1.Columns[3]) then
    begin
      DBGridEh1.Canvas.Brush.Color := clWhite;
      DBGridEh1.Canvas.Font.Color  := clRed ;
      DBGridEh1.Canvas.FillRect(Rect);
      DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
    end;

  end ;

  if (qryItemRecebimentonCdTabStatusItemPed.Value = 4) then
  begin

      if (Column = DBGridEh1.Columns[3]) then
      begin
          DBGridEh1.Canvas.Brush.Color := $0080FFFF ; //$00A6FFFF;
          DBGridEh1.Canvas.Font.Color  := clBlue ;
          DBGridEh1.Canvas.FillRect(Rect);
          DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end;
    
  end ;

end;



procedure TfrmRecebimento.DBGridEh1ColExit(Sender: TObject);
var
  bGrade: boolean ;
label
  montaGrade;
begin
  inherited;

  bGrade := false ;

  if (DbGridEh1.Col = 1) and (qryItemRecebimento.State <> dsBrowse) then
  begin

      DBGridEh1.Columns[3].Title.Font.Color := clBlack ;
      DBGridEh1.Columns[3].ReadOnly         := False ;
      DBGridEh1.Columns[3].Font.Color       := clBlack ;

      //Produto AD
      qryItemRecebimentocCdProduto.Value := Uppercase(qryItemRecebimentocCdProduto.Value);
      qryItemRecebimentocCdProduto.Value := Trim(qryItemRecebimentocCdProduto.Value);

      if (qryTipoRecebcFlgPermiteItemAD.Value = 0) and (qryItemRecebimentocCdProduto.Value = 'AD') then
      begin
          MensagemAlerta('Tipo de Recebimento n�o permite item AD.') ;
          DbGridEh1.Col := 1 ;
          abort ;
      end
      else
      begin
          if qryItemRecebimentocCdProduto.Value <> 'AD' then
          begin
              DBGridEh1.Columns[3].ReadOnly         := True ;
              DBGridEh1.Columns[3].Title.Font.Color := clRed ;
              DBGridEh1.Columns[3].ReadOnly         := True ;
          end;

          if qryItemRecebimentocCdProduto.Value = 'AD' then
          begin
              DBGridEh1.Columns[3].Title.Font.Color := clBlack ;
              DBGridEh1.Columns[3].ReadOnly         := False ;
              DbGridEh1.Col                         := 4 ;

              qryItemRecebimentonCdTipoItemPed.Value := 5;
          end;
      end;

      if ((qryItemRecebimentocCdProduto.Value <> '') and (qryItemRecebimentocCdProduto.Value <> 'AD')) then
      begin

          PosicionaQuery(qryProduto,qryItemRecebimentocCdProduto.Value); //  asString) ;

          if (qryProduto.eof) then
          begin
              MensagemAlerta('C�digo de produto inv�lido.') ;
              abort ;
          end ;

          qryItemRecebimentonCdProduto.Value     := qryProdutonCdProduto.Value;
          qryItemRecebimentocNmItem.Value        := qryProdutocNmProduto.Value ;
          qryItemRecebimentonCdTipoItemPed.Value := 2 ;

          qryGrupoImposto.Close;
          PosicionaQuery( qryGrupoImposto , qryProdutonCdProduto.asString ) ;

          if not qryGrupoImposto.eof then
              qryItemRecebimentocNCM.Value := qryGrupoImpostocNCM.Value ;

          qryGrupoImposto.Close;

          if (qryProdutonCdGrade.Value > 0) then
          begin
              { -- verifica se a grade ao menos tem um item informado cadastrado -- }
              qryAux.Close;
              qryAux.SQL.Clear;
              qryAux.SQL.Add('SELECT 1 FROM ItemGrade WHERE nCdGrade = ' + qryProdutonCdGrade.AsString);
              qryAux.Open;

              if (qryAux.IsEmpty) then
              begin
                  MensagemAlerta('Grade No ' + qryProdutonCdGrade.AsString + ' do produto ' + qryProdutocNmProduto.Value + ' n�o configurado corretamente.');
                  Abort;
              end;

              bGrade:= true ;

              if (qryProdutonCdTabTipoProduto.Value = 3) then
              begin
                  MensagemAlerta('Produto em grade s� pode receber entrada pelo c�digo intermedi�rio. Utilize o C�digo : ' + qryProdutonCdProdutoPai.asString) ;
                  abort ;
              end ;

              if (qryProdutonCdTabTipoProduto.Value = 1) then
              begin
                  MensagemAlerta('O produto estruturado n�o pode receber entradas. Utilize o produto intermedi�rio.') ;
                  abort ;
              end ;

              qryItemRecebimentonCdTipoItemPed.Value := 1 ;

          end ;

          qryProduto.Close ;

          qryAux.Close ;
          qryAux.Fields.Clear;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('SELECT 1                                                                                            ') ;
          qryAux.SQL.Add('  FROM Produto                                                                                      ') ;
          qryAux.SQL.Add(' WHERE nCdProduto = ' + qryItemRecebimentonCdProduto.asString                                        );
          qryAux.SQL.Add('   AND EXISTS(SELECT 1                                                                              ');
          qryAux.SQL.Add('                FROM GrupoProdutoTipoPedido GPTP                                                    ');
          qryAux.SQL.Add('                     INNER JOIN TipoPedidoTipoReceb TPTR ON TPTR.nCdTipoPedido = GPTP.nCdTipoPedido ');
          qryAux.SQL.Add('               WHERE GPTP.nCdGrupoProduto = Produto.nCdGrupo_Produto AND nCdTipoReceb = ' + qryMasternCdTipoReceb.asString + ')                         ') ;
          qryAux.Open ;

          if qryAux.Eof and (qryTipoRecebcFlgRecebCego.Value = 0) then
          begin
              qryAux.Close ;
              MensagemAlerta('Este produto pertence a um grupo de produto que n�o � permitido neste tipo de recebimento.') ;
              qryItemRecebimento.Cancel;
              abort ;
          end ;

          qryAux.Close ;

          if bGrade then
          begin

            montaGrade:
            objGrade.qryValorAnterior.SQL.Text := '' ;

            if (qryItemRecebimentonCdItemRecebimento.Value > 0) then
                objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtde FROM ItemRecebimento WHERE nCdTipoItemPed = 4 AND nCdItemRecebimentoPai = ' + qryItemRecebimentonCdItemRecebimento.AsString + ' ORDER BY ncdProduto') ;

            objGrade.PreparaDataSet(qryItemRecebimentonCdProduto.Value);
            objGrade.Renderiza;

            if (objGrade.nQtdeTotal <= 0) then
            begin
                MensagemErro('Quantidade digitada na grade inv�lida ou n�o informada.') ;
                goto montaGrade;
            end ;

            qryItemRecebimentonQtde.Value := objGrade.nQtdeTotal ;

            DBGridEh1.Columns[4].Title.Font.Color := clRed ;
            DBGridEh1.Columns[4].ReadOnly         := True ;
            {DBGridEh1.Columns[4].Font.Color       := clTeal ;}
            DBGridEh1.Columns[4].Color            := clSilver ;

            DBGridEh1.SetFocus;

            if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
                DBGridEh1.Col  := 6
            else
                DBGridEh1.Col  := 11 ;                  

          end
          else
          begin
              DBGridEh1.Columns[4].Title.Font.Color := clBlack ;
              DBGridEh1.Columns[4].ReadOnly         := False ;
              DBGridEh1.Columns[4].Color            := clWhite ;
              DBGridEh1.Col                         := 5 ;
          end;
      end;
  end ;

  if (DBGridEh1.Col = 4)  and (qryItemRecebimentocCdProduto.Value = 'AD') and (qryItemRecebimento.State <> dsBrowse) then
      qryItemRecebimentocNmItem.Value := Uppercase(qryItemRecebimentocNmItem.Value);

  { -- inclui sugest�o de CFOP de entrada caso a natureza de opera��o esteja configurada (entrada x sa�da) -- }
  if ((DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cCFOPNF') and (qryItemRecebimento.State <> dsBrowse)) then
  begin
      qryNaturezaOperacao.Close;
      qryNaturezaOperacao.Parameters.ParamByName('nCdRecebimento').Value := qryMasternCdRecebimento.Value;
      qryNaturezaOperacao.Parameters.ParamByName('cCFOPPedido').Value    := qryItemRecebimentocCFOPNF.Value;
      qryNaturezaOperacao.Parameters.ParamByName('cFlgEntSai').Value     := 'S';
      qryNaturezaOperacao.Open;

      if (not qryNaturezaOperacao.IsEmpty) then
          qryItemRecebimentocCFOP.Value := qryNaturezaOperacaocCFOP.Value;
  end;

  calculaTotaisItem( FALSE ) ;
end;

procedure TfrmRecebimento.qryItemBarraBeforePost(DataSet: TDataSet);
begin

      if (qryItemBarracEANFornec.Value <> '') then
      begin

          qryAux.Close ;
          qryAux.Fields.Clear ;
          qryAux.SQL.Clear ;

          if (cTipoEAN = 'P') then
          begin
              qryAux.SQL.Add('SELECT nCdProduto, cNmProduto, nCdGrade, nCdTabTipoProduto, nCdProdutoPai FROM Produto WHERE nCdStatus = 1 cEAN = ' + CHR(39) + Trim(qryItemBarracEANFornec.Value) + CHR(39)) ;
          end
          else
          begin
              qryAux.SQL.Add('SELECT nCdProduto, cNmProduto, nCdGrade, nCdTabTipoProduto, nCdProdutoPai FROM Produto WHERE nCdStatus = 1 AND cEANFornec = ' + CHR(39) + Trim(qryItemBarracEANFornec.Value) + CHR(39)) ;
          end ;

          qryAux.Open ;

          if (qryAux.eof) then
          begin
              MensagemAlerta('C�digo de barra inv�lido.') ;
              abort ;
          end ;

          if (qryAux.RecordCount > 1) then
          begin
              MensagemAlerta('Mais de um produto ativo com este c�digo de barra.') ;
              abort ;
          end ;

          qryItemBarranCdProduto.Value := qryAux.FieldList[0].Value ;
          qryItemBarracNmItem.Value    := qryAux.FieldList[1].Value ;

          qryAux.Close ;

          qryItemBarranCdTipoItemPed.Value := 9 ;

          qryAux.SQL.Clear;
          qryAux.SQL.Add('SELECT 1                                                                                            ') ;
          qryAux.SQL.Add('  FROM Produto                                                                                      ') ;
          qryAux.SQL.Add('       INNER JOIN Departamento ON Departamento.nCdDepartamento = Produto.nCdDepartamento            ');
          qryAux.SQL.Add(' WHERE nCdProduto = ' + qryItemBarranCdProduto.asString                                        );
          qryAux.SQL.Add('   AND EXISTS(SELECT 1                                                                              ');
          qryAux.SQL.Add('                FROM GrupoProdutoTipoPedido GPTP                                                    ');
          qryAux.SQL.Add('                     INNER JOIN TipoPedidoTipoReceb TPTR ON TPTR.nCdTipoPedido = GPTP.nCdTipoPedido ');
          qryAux.SQL.Add('               WHERE GPTP.nCdGrupoProduto = Departamento.nCdGrupoProduto AND nCdTipoReceb = ' + qryMasternCdTipoReceb.asString + ')                         ') ;
          qryAux.Open ;

          if qryAux.Eof and (qryTipoRecebcFlgRecebCego.Value = 0) then
          begin
              qryAux.Close ;
              MensagemAlerta('Este produto pertence a um grupo de produto que n�o � permitido neste tipo de recebimento.') ;
              qryItemBarra.Cancel;
              abort ;
          end ;

          qryAux.Close ;

      end ;

  inherited;

  qryItemBarranCdTabStatusItemPed.Value := 1 ;
  qryItemBarranCdRecebimento.Value      := qryMasternCdRecebimento.Value ;
  qryItemBarranQtde.Value               := 1 ;

end;

procedure TfrmRecebimento.DBGridEh2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (qryItemBarracNmTabTipoDiverg.Value <> '') then
  begin

    if not(gdSelected in State) then
    begin
      DBGridEh2.Canvas.Brush.Color := clRed;
      DBGridEh2.Canvas.Font.Color  := clWhite ;
      DBGridEh2.Canvas.FillRect(Rect);
      DBGridEh2.DefaultDrawDataCell(Rect,Column.Field,State);
    end;

  end ;

end;

procedure TfrmRecebimento.btCancelarClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex        := 0 ;

end;

procedure TfrmRecebimento.DBGridEh2Enter(Sender: TObject);
begin
  inherited;
  if not qryMaster.Active then
      abort ;

  if (qryMaster.State <> dsBrowse) then
  begin
      qryMaster.Post ;
  end ;

end;

procedure TfrmRecebimento.qryPrazoRecebimentoBeforePost(DataSet: TDataSet);
begin
  inherited;

  //if (qryPrazoRecebimentodDtVenc.Value < StrToDate(DateToStr(Now() + iDiasMinVenc))) then
  //begin
  //    ShowMessage('Data de vencimento inferior a pol�cita de pagamento utilizada. Prazo m�nimo para vencimento: ' + IntToStr(iDiasMinVenc) + ' dia(s).') ;
  //    abort ;
  //end ;

  if (qryPrazoRecebimentonValParcela.Value <= 0) then
  begin
      MensagemAlerta('Valor da parcela inv�lido ou n�o informado.') ;
      abort ;
  end ;

  qryPrazoRecebimentonValPagto.Value := qryPrazoRecebimentonValParcela.Value ;

  if (qryPrazoRecebimentocFlgDocCobranca.Value = 1) and (qryPrazoRecebimentocNrTit.Value = '') then
  begin
      MensagemAlerta('Informe o n�mero do t�tulo a pagar.') ;
      abort ;
  end ;

  qryPrazoRecebimentonCdRecebimento.Value := qryMasternCdRecebimento.Value ;
  qryPrazoRecebimentoiQtdeDias.Value      := Trunc(qryPrazoRecebimentodDtVenc.Value - qryMasterdDtDocto.Value) ;

  if (qryPrazoRecebimento.State = dsInsert) then
      qryPrazoRecebimentonCdPrazoRecebimento.Value := frmMenu.fnProximoCodigo('PRAZORECEBIMENTO') ;

end;

procedure TfrmRecebimento.qryTempAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (qryItemRecebimento.State = dsBrowse) then
          qryItemRecebimento.Edit ;

      qryItemRecebimentonQtde.Value := iQtde ;

  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRecebimento.qryTempAfterCancel(DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;

  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (qryItemRecebimento.State = dsBrowse) then
          qryItemRecebimento.Edit ;

      qryItemRecebimentonQtde.Value := iQtde ;

      DBGridEh1.Col := 5 ;
  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRecebimento.qryItemRecebimentoAfterPost(DataSet: TDataSet);
var
 i : integer ;
 bGrade : boolean ;
begin

  bGrade := False ;

  if (qryItemRecebimentonCdProduto.Value > 0) then
  begin

      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT nCdGrade FROM Produto WHERE nCdTabTipoProduto = 2 AND nCdProduto = ' + qryItemRecebimentonCdProduto.asString) ;
      qryAux.Fields.Clear ;
      qryAux.Open ;


      if (not qryAux.eof) and (qryAux.FieldList[0].AsString <> '') then
          bGrade := true ;

      qryAux.Close ;
  end ;


  if (not objGrade.bMontada) and (bGrade) then
  begin

      objGrade.qryValorAnterior.SQL.Text := '' ;

      if (qryItemRecebimentonCdItemRecebimento.Value > 0) then
          objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtde FROM ItemRecebimento WHERE nCdTipoItemPed = 4 AND nCdItemRecebimentoPai = ' + qryItemRecebimentonCdItemRecebimento.AsString + ' ORDER BY ncdProduto') ;

      objGrade.PreparaDataSet(qryItemRecebimentonCdProduto.Value);

  end ;

  // exclui as diverg�ncias
  try
      cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemRecebimentonCdItemRecebimento.Value ;
      cmdExcluiSubItem.Execute;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  if (qryItemRecebimentonCdItemPedido.Value > 0) then
  begin

      if (qryItemRecebimentonPercICMSSub.Value > qryItemRecebimentonPercICMSSubPed.Value) and (qryTipoRecebcFlgDivICMSSub.Value = 1) then
      begin
          SP_INSERI_DIVERGENCIA_RECEB.Close ;
          SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
          SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdTabTipoDiverg').Value   := 9 ;
          SP_INSERI_DIVERGENCIA_RECEB.ExecProc;
      end ;

      if (qryItemRecebimentonPercIPI.Value > qryItemRecebimentonPercIPIPed.Value) and (qryTipoRecebcFlgDivIPI.Value = 1) then
      begin
          SP_INSERI_DIVERGENCIA_RECEB.Close ;
          SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
          SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdTabTipoDiverg').Value   := 8 ;
          SP_INSERI_DIVERGENCIA_RECEB.ExecProc;
      end ;

      qryAux.Close ;
      qryAux.Fields.Clear ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT (nQtdePed-nQtdeExpRec-nQtdeCanc) FROM ItemPedido WHERE nCdItemPedido = ' + qryItemRecebimentonCdItemPedido.asString) ;
      qryAux.Open ;

      if not qryAux.Eof then
      begin
          if (qryItemRecebimentonQtde.Value > qryAux.FieldList[0].Value) and (qryItemRecebimentonCdTipoItemPed.Value <> 1) then
          begin
              SP_INSERI_DIVERGENCIA_RECEB.Close ;
              SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
              SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdTabTipoDiverg').Value   := 4 ;
              SP_INSERI_DIVERGENCIA_RECEB.ExecProc;
          end ;
      end ;

      //inseri os itens da grade
      if (bGrade) then
      begin
          objGrade.cdsQuantidade.First;
          objGrade.cdsCodigo.First ;
      end ;

      for i := 0 to objGrade.cdsQuantidade.FieldList.Count-1 do
      begin
          if (objGrade.cdsQuantidade.Fields[i].Value <> null) and (StrToInt(objGrade.cdsQuantidade.Fields[i].Value) > 0) then
          begin
              qryInseriItemGrade.Close ;
              qryInseriItemGrade.Parameters.ParamByName('nCdRecebimento').Value     := qryMasternCdRecebimento.Value ;
              qryInseriItemGrade.Parameters.ParamByName('nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
              qryInseriItemGrade.Parameters.ParamByName('nCdProduto').Value         := StrToInt(objGrade.cdsCodigo.Fields[i].Value) ;
              qryInseriItemGrade.Parameters.ParamByName('iQtde').Value              := StrToInt(objGrade.cdsQuantidade.Fields[i].Value) ;
              qryInseriItemGrade.ExecSQL;
          end ;
      end ;

      if (bGrade) then
          objGrade.LiberaGrade;

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT 1 WHERE IsNull((SELECT Sum(nCdProduto*nQtdePed) FROM ItemPedido WHERE nCdItemPedidoPai = ' + qryItemRecebimentonCdItemPedido.AsString +' AND nCdTipoItemPed = 4),0) <> IsNull((SELECT Sum(nCdProduto*nQtde) FROM ItemRecebimento WHERE nCdItemRecebimentoPai = ' + qryItemRecebimentonCdItemRecebimento.asString +' AND nCdTipoItemPed = 4),0)') ;
      qryAux.Open ;

      if not qryAux.Eof and (qryTipoRecebcFlgDivGrade.Value = 1) then
      begin
        SP_INSERI_DIVERGENCIA_RECEB.Close ;
        SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
        SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdTabTipoDiverg').Value   := 5 ;
        SP_INSERI_DIVERGENCIA_RECEB.ExecProc;
      end ;

  end
  else begin
      // sem pedido
      //inseri os itens da grade
      if (bGrade) then
      begin
          objGrade.cdsQuantidade.First;
          objGrade.cdsCodigo.First ;
      end ;

      for i := 0 to objGrade.cdsQuantidade.FieldList.Count-1 do
      begin
          if (StrToInt(objGrade.cdsQuantidade.Fields[i].Value) > 0) then
          begin
              qryInseriItemGrade.Close ;
              qryInseriItemGrade.Parameters.ParamByName('nCdRecebimento').Value     := qryMasternCdRecebimento.Value ;
              qryInseriItemGrade.Parameters.ParamByName('nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
              qryInseriItemGrade.Parameters.ParamByName('nCdProduto').Value         := StrToInt(objGrade.cdsCodigo.Fields[i].Value) ;
              qryInseriItemGrade.Parameters.ParamByName('iQtde').Value              := StrToInt(objGrade.cdsQuantidade.Fields[i].Value) ;
              qryInseriItemGrade.ExecSQL;
          end ;
      end ;

      if (bGrade) then
          objGrade.LiberaGrade;
          
      if (qryTipoRecebcFlgDivSemPedido.Value = 1) then
      begin
          SP_INSERI_DIVERGENCIA_RECEB.Close ;
          SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemRecebimentonCdItemRecebimento.Value ;
          SP_INSERI_DIVERGENCIA_RECEB.Parameters.ParamByName('@nCdTabTipoDiverg').Value   := 1 ;
          SP_INSERI_DIVERGENCIA_RECEB.ExecProc;
      end;
  end ;

  inherited;

  qryItemRecebimento.Requery();
  qryItemRecebimento.Last;

end;

procedure TfrmRecebimento.qryItemRecebimentoBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin
      { -- verifica se recebimento j� foi movimentado e n�o permite exclus�o do registro devido j� haver sido movimentado anteriormente -- }
      { -- este problema ocorre quando o recebimento ainda n�o foi atualizado o status entre os servidores -- }
      qryVerificaMovEstoque.Close;
      qryVerificaMovEstoque.Parameters.ParamByName('nPK').Value        := IntToStr(qryMasternCdRecebimento.Value);
      qryVerificaMovEstoque.Parameters.ParamByName('nCdProduto').Value := IntToStr(qryItemRecebimentonCdProduto.Value);
      qryVerificaMovEstoque.Open;

      if (not qryVerificaMovEstoque.IsEmpty) then
      begin
          MensagemAlerta('N�o � possivel excluir os itens devido este recebimento j� ter sido movimentado anteriormente.');
          Abort;
      end;
  end ;

  frmMenu.Connection.BeginTrans;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemRecebimentonCdItemRecebimento.Value ;
    cmdExcluiSubItem.Execute;

    qryAux.Close ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('DELETE FROM HistAutorDiverg WHERE nCdItemRecebimento = ' + qryItemRecebimentonCdItemRecebimento.asString);
    qryAux.ExecSQL;

    qryAux.Close ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('DELETE FROM CentroCustoItemRecebimento WHERE nCdItemRecebimento = ' + qryItemRecebimentonCdItemRecebimento.asString);
    qryAux.ExecSQL;

  except
    frmMenu.Connection.RollbackTrans;
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  frmMenu.Connection.CommitTrans;

end;

procedure TfrmRecebimento.btnImpProtocoloRecebimentoClick(Sender: TObject);
var
    objRel : TrptRecebimento_view ;
begin

  if not qryMaster.Active then
      exit ;

  objRel := TrptRecebimento_view.Create( Self ) ;

  objRel.qryMaster.Close ;
  PosicionaQuery(objRel.qryMaster, qryMasternCdRecebimento.asString) ;
  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmRecebimento.ToolButton10Click(Sender: TObject);
var
    iLimiteDias : integer ;
    strEstados  : TStringList ;
    objForm     : TfrmRegistroMovLote ;
    nQtdeLote   : double ;

label
    PulaConsistencias ;
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMasternCdRecebimento.Value = 0) then
  begin
      MensagemAlerta('Nenhum recebimento ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusReceb.Value > 2) then
  begin
      MensagemAlerta('Recebimento j� finalizado.') ;
      exit ;
  end ;

  if ((qryItemRecebimento.RecordCount = 0) and (qryItemBarra.RecordCount = 0)) then
  begin
      MensagemAlerta('Informe no m�nimo um item a receber para efetuar a finaliza��o do recebimento');
      Exit;      
  end;

  if (qryPrazoRecebimento.Active) and (qryPrazoRecebimento.State <> dsBrowse) then
      qryPrazoRecebimento.Post ;

  If (qryMaster.State <> dsBrowse) and (qryMastercFlgRecebTransfEst.Value = 0) then
      qryMaster.Post ;

  iLimiteDias := 0 ;

  // se for recebimento CEGO n�o confere NADA!
  if (qryTipoRecebcFlgRecebCego.Value = 1) then
      goto PulaConsistencias ;

  if ((qryTipoRecebcFlgExigeNfe.Value = 1) and (DBEdit29.Text = '')) then
  begin
      MensagemAlerta('� obrigat�ria a informa��o da Chave da NFe.') ;
      DBEdit29.SetFocus;
      abort;
  end;

  // verifica o estado de origem da NF
  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
  begin

       if (Trim(DBEdit12.Text) = '') then
       begin
           MensagemAlerta('Informe a unidade federativa de origem da nota fiscal.');
           DBEdit12.SetFocus;
           Abort;
       end;

       qryChecaEnderecoUFTerceiro.Close;
       qryChecaEnderecoUFTerceiro.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
       qryChecaEnderecoUFTerceiro.Parameters.ParamByName('cUF').Value         := DBEdit12.Text ;
       qryChecaEnderecoUFTerceiro.Open;

       if (qryChecaEnderecoUFTerceiro.eof) then
       begin
           MensagemAlerta('Nenhum endere�o ativo encontrado para este terceiro no estado de origem da nota fiscal. Verifique.') ;
           abort ;
       end ;

       qryChecaEnderecoUFTerceiro.Close;

  end;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT IsNull(Sum(nValTotal),0)          ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValDesconto),0)       ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValIPI),0)            ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValICMSSub),0)        ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValSeguro),0)         ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValAcessorias),0)     ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValFrete),0)          ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValBaseCalcSubTrib),0)');
  qryAux.SQL.Add('      ,IsNull(Sum(nBaseCalcICMS),0)      ');
  qryAux.SQL.Add('      ,IsNull(Sum(nValICMS),0)           ');
  qryAux.SQL.Add('  FROM ItemRecebimento                   ');
  qryAux.SQL.Add(' WHERE nCdTipoItemPed IN (1,2,3,5,6,9)   ');
  qryAux.SQL.Add('   AND nCdRecebimento = ' + qryMasternCdRecebimento.asString) ;
  qryAux.Open ;

  if (qryTipoRecebcFlgExigeNfe.Value = 1) then
  begin
  if (ABS(qryMasternValTotalNF.Value - (qryMasternValDocto.Value - qryMasternValDescontoNF.Value + qryMasternValIPI.Value + qryMasternValICMSSub.Value + qryMasternValDespesas.Value + qryMasternValSeguroNF.Value + qryMasternValFrete.Value)) > 1) then
      begin
          MensagemAlerta('Total dos valores digitados n�o confere com o total da nota fiscal.') ;
          DBEdit42.SetFocus;
          abort ;
      end;
  end;

  {-- s� checa os dados de ICMS comum quando o tipo de recebimento exige os dados fiscais --}
  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
  begin

      // Valor da base de calculo de icms
      if qryAux.Eof or (ABS((qryAux.FieldList[8].Value) -  qryMasternValBaseICMS.Value) > 1) then
      begin
          qryAux.Close ;
          MensagemAlerta('Valor da Base de C�lculo do ICMS da Nota Fiscal n�o confere com o valor da BC dos itens digitados.') ;
          cxPageControl1.ActivePage := cxTabSheet1 ;
          DBGridEh1.SetFocus;
          abort ;
      end ;

      // Valor do icms
      if qryAux.Eof or (ABS((qryAux.FieldList[9].Value) -  qryMasternValICMS.Value) > 1) then
      begin
          qryAux.Close ;
          MensagemAlerta('Valor do ICMS da Nota Fiscal n�o confere com o valor do ICMS dos itens digitados.') ;
          abort ;
          cxPageControl1.ActivePage := cxTabSheet1 ;
          DBGridEh1.SetFocus;
      end ;

  end ;

  // Valor da base de calculo substituicao
  if qryAux.Eof or (ABS((qryAux.FieldList[7].Value) -  qryMasternValBaseCalcICMSSub.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor da Base de C�lculo Substititui��o ICMS da Nota Fiscal n�o confere com o valor B.Calc. Sub. Total dos itens digitados.') ;
      abort ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
  end ;

  // Valor da icms substituicao
  if qryAux.Eof or (ABS((qryAux.FieldList[3].Value) -  qryMasternValICMSSub.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor da ICMS Substitui��o da Nota Fiscal n�o confere com o valor ICMS Subs. total dos itens digitados.') ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
      abort ;
  end ;

  // Valor do Seguro
  if qryAux.Eof or (ABS((qryAux.FieldList[4].Value) -  qryMasternValSeguroNF.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor do Seguro da Nota Fiscal n�o confere com o seguro total dos itens digitados.') ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
      abort ;
  end ;

  // Valor do Frete
  if qryAux.Eof or (ABS((qryAux.FieldList[6].Value) -  qryMasternValFrete.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor do Frete da Nota Fiscal n�o confere com o Frete total dos itens digitados.') ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
      abort ;
  end ;

  // Valor das Despesas Acessorias
  if qryAux.Eof or (ABS((qryAux.FieldList[5].Value) -  qryMasternValDespesas.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor da Despesa Acess�ria da Nota Fiscal n�o confere com o Valor das Despesas Acess�rias Total dos itens digitados.') ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
      abort ;
  end ;


  if qryAux.Eof or (ABS((qryAux.FieldList[0].Value-qryAux.FieldList[1].Value) - (qryMasternValDocto.Value - qryMasternValDescontoNF.Value)) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor Total dos produtos n�o confere com o total dos itens digitados.') ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
      abort ;
  end ;

  {if qryAux.Eof or (ABS(qryAux.FieldList[1].Value - qryMasternValDescontoNF.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor do desconto da NF n�o confere com o total de desconto dos itens digitados.') ;
      abort ;
  end ;}

  {-- verifica se o total do IPI dos itens confere com o total do IPI digitado na capa do recebimento --}
  if qryAux.Eof or (ABS(qryAux.FieldList[2].Value - qryMasternValIPI.Value) > 1) then
  begin
      qryAux.Close ;
      MensagemAlerta('Valor do IPI da NF n�o confere com o total de IPI dos itens digitados.') ;
      cxPageControl1.ActivePage := cxTabSheet1 ;
      DBGridEh1.SetFocus;
      abort ;
  end ;

  qryAux.Close ;

  if (qryTipoRecebcFlgExigeNF.Value = 1) then
  begin

      try
          iLimiteDias := StrToInt(frmMenu.LeParametro('LIMDIAEMINFREC')) ;
      except
          MensagemAlerta('Parametro LIMDIAEMINFREC configurado incorretamente.') ;
          iLimiteDias := 0 ;
      end;

      if (iLimiteDias > 0) AND ((StrToDate(DateToStr(Now())) - qryMasterdDtDocto.Value) > iLimiteDias) then
      begin
          MensagemAlerta('A data de emiss�o da nota fiscal � superior ao limite de dias tolerado para recebimento.') ;
          exit ;
      end ;

  end ;

  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
  begin
      if (qryMasternValBaseICMS.Value < 0) then
      begin
          MensagemAlerta('Base de c�lculo de ICMS inv�lida') ;
          DbEdit42.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseICMS.Value > 0) and (qryMasternValICMS.Value <= 0) then
      begin
          MensagemAlerta('Informe o Valor do ICMS') ;
          DbEdit37.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseICMS.Value <= 0) and (qryMasternValICMS.Value > 0) then
      begin
          MensagemAlerta('Informe a Base de C�lculo do ICMS') ;
          DbEdit42.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseCalcICMSSub.Value > 0) and (qryMasternValICMSSub.Value <= 0) then
      begin
          MensagemAlerta('Informe o Valor do ICMS Substitui��o') ;
          DbEdit31.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseCalcICMSSub.Value <= 0) and (qryMasternValICMSSub.Value > 0) then
      begin
          MensagemAlerta('Informe a Base de C�lculo do ICMS Substitui��o') ;
          DbEdit43.SetFocus ;
          exit ;
      end ;

  end;

  if (qryTipoRecebcFlgExigeNF.Value = 1) then
  begin

      if (Trim(DBEdit23.Text) = '') then
      begin
          MensagemAlerta('Informe o Modelo da Nota Fiscal.') ;
          DbEdit23.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseICMS.Value < 0) then
      begin
          MensagemAlerta('Base de c�lculo de ICMS inv�lida') ;
          DbEdit42.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseICMS.Value > 0) and (qryMasternValICMS.Value <= 0) then
      begin
          MensagemAlerta('Informe o Valor do ICMS') ;
          DbEdit37.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseICMS.Value <= 0) and (qryMasternValICMS.Value > 0) then
      begin
          MensagemAlerta('Informe a Base de C�lculo do ICMS') ;
          DbEdit42.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseCalcICMSSub.Value > 0) and (qryMasternValICMSSub.Value <= 0) then
      begin
          MensagemAlerta('Informe o Valor do ICMS Substitui��o') ;
          DbEdit31.SetFocus ;
          exit ;
      end ;

      if (qryMasternValBaseCalcICMSSub.Value <= 0) and (qryMasternValICMSSub.Value > 0) then
      begin
          MensagemAlerta('Informe a Base de C�lculo do ICMS Substitui��o') ;
          DbEdit43.SetFocus ;
          exit ;
      end ;

      cxPageControl1.ActivePage := cxTabSheet1 ;

  end ;

  // Confere os centros de custo
  if (frmMenu.LeParametroEmpresa('USACCCOMPRA') = 'S') and (qryMasternValDocto.Value > 0) then
  begin

      cxPageControl1.ActivePage := TabCentroCusto ;

      qryItemCC.First;

      while not qryItemCC.Eof do
      begin

          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('SELECT Sum(nPercent) FROM CentroCustoItemRecebimento WHERE nCdItemRecebimento = ' + qryItemCCnCdItemRecebimento.AsString);
          qryAux.Open ;

          if qryAux.Eof or (qryAux.FieldList[0].Value <> 100) then
          begin
              qryAux.Close ;
              MensagemAlerta('O rateio de centro de custo do item � diferente de 100%.' + #13#13 + 'Item: ' + qryItemCCcNmItem.Value) ;
              exit ;
          end ;

          qryItemCC.Next;
      end ;

      qryItemCC.First;

  end ;

  // Confere os lotes de fabrica��o
  qryProdLote.Close;
  PosicionaQuery(qryProdLote, qryMasternCdRecebimento.AsString) ;

  if not qryProdLote.eof then
  begin

      cxPageControl1.ActivePage := TabLote ;

      objForm := TfrmRegistroMovLote.Create( Self ) ;

      try

          qryProdLote.First;

          while not qryProdLote.Eof do
          begin

              nQtdeLote := objForm.retornaQuantidadeRegistradaTemporaria( qryProdLotenCdProduto.Value
                                                                        , 2
                                                                        , qryProdLotenCdItemRecebimento.Value ) ;

              {-- quando o produto n�o tem lote/serial controlados a fun��o retorna -1 --}
              if (nQtdeLote <> -1) then
              begin

                  if ( nQtdeLote <> qryProdLotenQtde.Value ) then
                  begin
                      MensagemAlerta('A quantidade especificada no Registro de Lotes/Seriais para o produto: ' + qryProdLotenCdProduto.AsString + ' - ' + qryProdLotecNmProduto.Value + ' n�o confere com a quantidade recebida.') ;
                      btRegistroLoteSerial.Click;
                      abort ;
                  end ;

              end ;

              qryProdLote.Next;
          end ;

      finally
          freeAndNil( objForm ) ;
      end ;

      qryProdLote.First;

  end ;

  // confere as parcelas
  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT IsNull(Sum(nValParcela),0) FROM PrazoRecebimento WHERE nCdRecebimento = ' + qryMasternCdRecebimento.AsString) ;
  qryAux.Open ;

  qryItemPagavel.Close ;
  PosicionaQuery(qryItemPagavel, qryMasternCdRecebimento.AsString) ;

  if (qryItemPagavel.Eof or (qryItemPagavelnValTotal.Value = 0)) and (qryAux.FieldList[0].Value > 0) then
  begin
      qryAux.Close ;
      qryItemPagavel.Close ;
      MensagemAlerta('Este recebimento n�o tem nenhum item pag�vel, exclua as parcelas.') ;
      cxPageControl1.ActivePage := cxTabSheet4;
      DBGridEh3.SetFocus;
      exit ;
  end ;

  if ((not qryItemPagavel.Eof) and (qryItemPagavelnValTotal.Value > 0) and (qryAux.FieldList[0].Value = 0)) then
  begin
      qryAux.Close ;
      qryItemPagavel.Close ;
      MensagemAlerta('Informe as parcelas do pagamento.') ;
      cxPageControl1.ActivePage := cxTabSheet4;
      DBGridEh3.SetFocus;
      exit ;
  end ;

  if (qryPrazoRecebimento.Active) and (qryPrazoRecebimento.RecordCount = 0) and (qryItemPagavelnValTotal.Value > 0) then
  begin
      MensagemAlerta('Gere as parcelas antes de finalizar o recebimento.') ;
      cxPageControl1.ActivePage := cxTabSheet4;
      DBGridEh3.SetFocus;
      exit ;
  end ;

  if (abs((qryItemPagavelnValTotal.Value+qryMasternValIPI.Value+qryMasternValDespesas.Value+qryMasternValIcmsSub.Value+qryMasternValFrete.Value+qryMasternValSeguroNF.Value) - (qryAux.FieldList[0].Value )) > 1) then
  begin
      qryAux.Close ;
      qryItemPagavel.Close ;
      MensagemAlerta('O valor das parcelas � diferente da soma dos itens pag�veis do recebimento.') ;
      cxPageControl1.ActivePage := cxTabSheet4;
      DBGridEh3.SetFocus;
      exit ;
  end ;

  PulaConsistencias:

  {-- Verifica se os itens foram conferidos. --}
  if (qryTipoRecebcFlgConfereItem.Value = 1) then
  begin
      PosicionaQuery(qryVerificaItemRecebDivergencia, qryMasternCdRecebimento.AsString);

      if not qryVerificaItemRecebDivergencia.IsEmpty then
      begin
          MensagemAlerta('Itens do recebimento com diverg�ncia de quantidade conferida. Favor verificar.');
          Abort;
      end;
  end;

  qryItemRecebimento.First;

  while not (qryItemRecebimento.Eof) do
  begin

      qryProduto.Close;
      PosicionaQuery(qryProduto, qryItemRecebimentonCdProduto.AsString);

      if (qryProdutonCdGrade.Value > 0) then
      begin
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('SELECT Sum(nQtde) as nQtde FROM ItemRecebimento WHERE nCdItemRecebimentoPai = ' + qryItemRecebimentonCdItemRecebimento.AsString);
          qryAux.Open;

          if (qryAux.FieldByName('nQtde').Value <> qryItemRecebimentonQtde.Value) then
          begin
              MensagemAlerta('Problemas no recebimento do item "' + qryProdutonCdProduto.AsString + ' - ' + qryProdutocNmProduto.Value + '", por favor exclua e reprocesse esse item.');

              Abort;
          end;
      end;

      qryItemRecebimento.Next;
  end;

  cxPageControl1.ActivePageIndex := 0 ;

  case MessageDlg('Confirma a finaliza��o deste recebimento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_FINALIZA_RECEBIMENTO.Close ;
      SP_FINALIZA_RECEBIMENTO.Parameters.ParamByName('@nCdRecebimento').Value := qryMasternCdRecebimento.Value ;
      SP_FINALIZA_RECEBIMENTO.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
      SP_FINALIZA_RECEBIMENTO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryMaster, qryMasternCdRecebimento.asString) ;

  if (qryMasternCdTabStatusReceb.Value = 2) and (frmMenu.LeParametro('FRECDIV') = 'N') then
  begin
      MensagemAlerta('Recebimento com diverg�ncias.                    ' + #13#13 + 'Solicite libera��o.') ;
  end
  else
  begin
      ShowMessage('Recebimento finalizado com sucesso.') ;
  end ;

end;

procedure TfrmRecebimento.btSalvarClick(Sender: TObject);
begin

  if qryMaster.Active and (qryMasternCdTabStatusReceb.Value > 2) then
  begin
      MensagemAlerta('Recebimento j� finalizado, imposs�vel alterar.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmRecebimento.qryCCItemRecebimentoBeforePost(
  DataSet: TDataSet);
begin

  if (qryCCItemRecebimentonCdCC.Value > 0) and (qryCCItemRecebimentocNmCC.Value = '') then
  begin
      MensagemAlerta('Informe um centro de custo.') ;
      abort ;
  end ;

  qryCCItemRecebimentonCdItemRecebimento.Value := qryItemCCnCdItemRecebimento.Value ;

  if (qryCCItemRecebimento.State = dsInsert) then
      qryCCItemRecebimentonCdCentroCustoItemRecebimento.Value := frmMenu.fnProximoCodigo('CENTROCUSTOITEMRECEBIMENTO') ; 

  inherited;

end;

procedure TfrmRecebimento.qryCCItemRecebimentoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryCentroCusto, qryCCItemRecebimentonCdCC.AsString) ;

  if not qryCentroCusto.Eof then
      qryCCItemRecebimentocNmCC.Value := qryCentroCustocNmCC.Value ;

end;

procedure TfrmRecebimento.cxButton4Click(Sender: TObject);
begin
  inherited;

  if qryItemCC.Eof or (DBComboItens.Text = '') then
  begin
      MensagemAlerta('Selecione o item a ser copiado.') ;
      exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_copia_cc.Close;
      usp_copia_cc.Parameters.ParamByName('@nCdItemRecebimento').Value := qryItemCCnCdItemRecebimento.Value ;
      usp_copia_cc.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

end;

procedure TfrmRecebimento.DBGridEh8KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryCCItemRecebimento.State = dsBrowse) then
             qryCCItemRecebimento.Edit ;

        if (qryCCItemRecebimento.State = dsInsert) or (qryCCItemRecebimento.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(29);

            If (nPK > 0) then
            begin
                qryCCItemRecebimentonCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRecebimento.cxPageControl1Change(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) then
  begin

    if (cxPageControl1.ActivePage = TabCentroCusto) then
    begin
      PosicionaQuery(qryItemCC, qryMasternCdRecebimento.AsString) ;

      if not qryItemCC.Eof then
          qryItemCC.First;
    end ;

    if (cxPageControl1.ActivePage = TabLote) then
    begin
      qryProdLote.Close;
      PosicionaQuery(qryProdLote, qryMasternCdRecebimento.AsString) ;

      qryProdLote.First;

      bReadOnly := False ;

      if (qryMasternCdTabStatusReceb.Value > 2) then
      begin
          bReadOnly := True ;
      end ;

    end ;

  end ;

end;

procedure TfrmRecebimento.DBComboItensChange(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCCItemRecebimento, qryItemCCnCdItemRecebimento.AsString) ;

end;

procedure TfrmRecebimento.qryLoteItemRecebimentoBeforePost(
  DataSet: TDataSet);
begin

  if (qryLoteItemRecebimentocNrLote.Value = '') then
  begin
      MensagemAlerta('Informe o N�mero do Lote.') ;
      abort ;
  end ;

  if (qryLoteItemRecebimentodDtFabricacao.Value > qryLoteItemRecebimentodDtValidade.Value) or (qryLoteItemRecebimentodDtFabricacao.asString = '') or (qryLoteItemRecebimentodDtValidade.asString = '') then
  begin
      MensagemAlerta('Data de Validade/Fabrica��o Inv�lida.') ;
      abort ;
  end ;

  if (qryLoteItemRecebimentodDtFabricacao.Value > qryMasterdDtReceb.Value) then
  begin
      MensagemAlerta('A data de fabrica��o do lote n�o pode ser maior que a data de recebimento.') ;
      abort ;
  end ;

  if (qryLoteItemRecebimentodDtValidade.Value < StrToDate(DateToStr(Now()))) then
  begin
      MensagemAlerta('Lote Vencido! Imposs�vel receber.') ;
      abort ;
  end ;

  if (qryLoteItemRecebimentonQtde.Value <= 0) then
  begin
      MensagemAlerta('Quantidade inv�lida.') ;
      abort ;
  end ;

  inherited;

  qryLoteItemRecebimentocNrLote.Value        := Uppercase(qryLoteItemRecebimentocNrLote.Value) ;
  qryLoteItemRecebimentonCdRecebimento.Value := qryMasternCdRecebimento.Value ;
  qryLoteItemRecebimentonCdProduto.Value     := qryProdLotenCdProduto.Value ;

end;

procedure TfrmRecebimento.DBComboProdutosChange(Sender: TObject);
begin
  inherited;

  qryLoteItemRecebimento.Close ;
  qryLoteItemRecebimento.Parameters.ParamByName('nCdRecebimento').Value := qryMasternCdRecebimento.Value ;
  PosicionaQuery(qryLoteItemRecebimento, qryProdLotenCdProduto.AsString) ;

end;

procedure TfrmRecebimento.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Col = 1) then
        begin

            if (qryItemRecebimento.State = dsBrowse) then
                 qryItemRecebimento.Edit ;


            if ((qryItemRecebimento.State = dsInsert) or (qryItemRecebimento.State = dsEdit)) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(68);

                If (nPK > 0) then
                begin
                    qryItemRecebimentonCdProduto.Value := nPK ;
                    qryItemRecebimentocCdProduto.Value := qryItemRecebimentonCdProduto.AsString;
                end ;

            end ;

        end ;

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cCFOP') then
        begin
            if (qryItemRecebimento.State = dsBrowse) then
            qryItemRecebimento.Edit ;

            if (((qryItemRecebimento.State = dsInsert) or (qryItemRecebimento.State = dsEdit))  and (qryMasternCdTabStatusReceb.Value= 1)) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(1011,'Convert(INTEGER,cCFOP) < 4000');
                If (nPK > 0) then
                begin
                    qryItemRecebimentocCFOP.Value := intToStr(nPk);
                end ;
            end ;

        end;

    end ;

  end ;

end;

procedure TfrmRecebimento.ExibirDivergncias1Click(Sender: TObject);
var
  objForm : TfrmRecebimento_Divergencias ;
begin
  inherited;

  if (qryItemRecebimentonCdItemRecebimento.Value = 0) then
  begin
      MensagemAlerta('Nenhum item selecionado.') ;
      exit ;
  end ;

  objForm := TfrmRecebimento_Divergencias.Create( Self ) ;

  PosicionaQuery(objForm.qryDivergencias, qryItemRecebimentonCdItemRecebimento.AsString) ;

  showForm( objForm , TRUE ) ;

end;

procedure TfrmRecebimento.ExcluirItem1Click(Sender: TObject);
begin
  inherited;

  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin
      if (MessageDlg('Confirma a exclus�o deste item?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      qryItemRecebimento.Delete;
  end ;
end;

procedure TfrmRecebimento.qryTipoRecebAfterScroll(DataSet: TDataSet);
begin
  inherited;

  cxPageControl1.Pages[1].Enabled := True ;
  cxPageControl1.Pages[2].Enabled := True ;
  cxPageControl1.Pages[3].Enabled := True ;
  cxPageControl1.Pages[4].Enabled := True ;
  cxPageControl1.Pages[5].Enabled := True ;

  if not qryTipoReceb.Eof and (qryTipoRecebcFlgRecebCego.Value = 1) then
  begin
      cxPageControl1.Pages[1].Enabled := False ;
      cxPageControl1.Pages[2].Enabled := False ;
      cxPageControl1.Pages[3].Enabled := False ;
      cxPageControl1.Pages[4].Enabled := False ;
      cxPageControl1.Pages[5].Enabled := False ;
  end ;

  if (frmMenu.LeParametroEmpresa('LOTEPROD') <> 'S') then
  begin
      TabLote.Enabled     := False ;
      cxTabSheet5.Enabled := False ;
  end ;

  if (qryTipoRecebcFlgConfereItem.Value = 1) then
  begin
     btnConferencia.Enabled    := True;
     btnVisualizaConfe.Enabled := True;
  end
  else
  begin
     btnConferencia.Enabled    := False;
     btnVisualizaConfe.Enabled := False;
  end;
end;

procedure TfrmRecebimento.usp_GradeAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to usp_Grade.FieldList.Count-1 do
  begin
      iQtde := iQtde + usp_Grade.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (qryItemRecebimento.State = dsBrowse) then
          qryItemRecebimento.Edit ;

      qryItemRecebimentonQtde.Value := iQtde ;

  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRecebimento.usp_GradeAfterCancel(DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to usp_Grade.FieldList.Count-1 do
  begin
      iQtde := iQtde + usp_Grade.Fields[i].Value ;
  end ;

  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (qryItemRecebimento.State = dsBrowse) then
          qryItemRecebimento.Edit ;

      qryItemRecebimentonQtde.Value := iQtde ;

      DBGridEh1.Col := 5 ;
  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRecebimento.qryGradeRecebimentoAfterCancel(
  DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to qryGradeRecebimento.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryGradeRecebimento.Fields[i].Value ;
  end ;

  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (qryItemRecebimento.State = dsBrowse) then
          qryItemRecebimento.Edit ;

      qryItemRecebimentonQtde.Value := iQtde ;

      DBGridEh1.Col := 5 ;
  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRecebimento.qryGradeRecebimentoAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to qryGradeRecebimento.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryGradeRecebimento.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin

      if (qryItemRecebimento.State = dsBrowse) then
          qryItemRecebimento.Edit ;

      qryItemRecebimentonQtde.Value := iQtde ;

  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmRecebimento.cxButton3Click(Sender: TObject);
begin
  inherited;

  case MessageDlg('Confirma a exclus�o das parcelas?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  qryPrazoRecebimento.Close ;

  frmMenu.Connection.BeginTrans;

  try
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM PrazoRecebimento WHERE nCdRecebimento = ' + qryMasternCdRecebimento.AsString) ;
      qryAux.ExecSQL;

      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM HistAutorDiverg WHERE nCdTabTipoDiverg = 10 AND nCdRecebimento = ' + qryMasternCdRecebimento.AsString) ;
      qryAux.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryPrazoRecebimento.Open ;

end;

procedure TfrmRecebimento.DBGridEh3DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (qryPrazoRecebimentocFlgDiverg.Value = 1) and ((DataCol >= 1) and (DataCol <= 2)) then
  begin

      DBGridEh3.Canvas.Brush.Color := $00A6FFFF;
      DBGridEh3.Canvas.Font.Color  := clBlack ;
      DBGridEh3.Canvas.FillRect(Rect);
      DBGridEh3.DefaultDrawDataCell(Rect,Column.Field,State);

  end ;

end;

procedure TfrmRecebimento.btnEmitirEtqMatricialClick(Sender: TObject);
var
  objRecebimento_Etiqueta : TfrmRecebimento_Etiqueta ;
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMasternCdRecebimento.Value = 0) then
  begin
      MensagemAlerta('Nenhum recebimento ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusReceb.Value <= 2) then
  begin
      MensagemAlerta('Finalize o recebimento antes de imprimir as etiquetas.') ;
      exit ;
  end ;

  objRecebimento_Etiqueta := TfrmRecebimento_Etiqueta.Create( Self ) ;

  objRecebimento_Etiqueta.cdsProduto.Close ;
  objRecebimento_Etiqueta.cdsProduto.Open ;

  while not objRecebimento_Etiqueta.cdsProduto.eof do
  begin
      objRecebimento_Etiqueta.cdsProduto.Delete;
      objRecebimento_Etiqueta.cdsProduto.Next ;
  end ;

  if not qryItemRecebimento.eof then
  begin

      qryItemRecebimento.First ;

      while not qryItemRecebimento.Eof do
      begin

          if (qryItemRecebimentonCdProduto.Value > 0) then
          begin
              PosicionaQuery(qryProduto, qryItemRecebimentonCdProduto.asString) ;

              objRecebimento_Etiqueta.cdsProduto.Insert;
              objRecebimento_Etiqueta.cdsProdutonCdProduto.Value         := qryItemRecebimentonCdProduto.Value ;
              objRecebimento_Etiqueta.cdsProdutocNmProduto.Value         := qryItemRecebimentocNmItem.Value    ;
              objRecebimento_Etiqueta.cdsProdutocReferencia.Value        := qryProdutocReferencia.Value        ;
              objRecebimento_Etiqueta.cdsProdutocFlgOK.Value             := 1 ;
              objRecebimento_Etiqueta.cdsProdutonCdItemRecebimento.Value := qryItemRecebimentonCdItemRecebimento.Value ;
              objRecebimento_Etiqueta.cdsProduto.Post ;
          end ;

          qryItemRecebimento.Next ;

      end ;

      qryItemRecebimento.First ;

      showForm( objRecebimento_Etiqueta , FALSE ) ;

  end ;

  freeAndNil( objRecebimento_Etiqueta ) ;

end;

procedure TfrmRecebimento.btnEmitirEtqTermicaClick(Sender: TObject);
var
    arrID : array of integer ;
    iAux,iAuxTotal  : integer ;
    objModImpETP : TfrmModImpETP ;
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMasternCdRecebimento.Value = 0) then
  begin
      MensagemAlerta('Nenhum recebimento ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusReceb.Value = 1) then
  begin
      MensagemAlerta('Finalize o recebimento antes de imprimir as etiquetas.') ;
      exit ;
  end ;

  { -- verifica se algum item foi inserido/alterado ap�s a �ltima finaliza��o -- }

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT TOP 1 1                    ');
  qryAux.SQL.Add('  FROM ItemRecebimento            ');
  qryAux.SQL.Add(' WHERE nCdRecebimento      = ' + qryMasternCdRecebimento.AsString) ;
  qryAux.SQL.Add('   AND nCdTabStatusItemPed = 1');
  qryAux.Open;

  if not qryAux.Eof then
  begin
      qryAux.Close;
      MensagemErro('O recebimento foi alterado. Finalize o recebimento novamente.') ;
      abort ;
  end ;

  qryAux.Close;

  { -- s� impede a impress�o das etiquetas caso algum item do recebimento tenha uma restria��o -- }
  { -- de grade/quantidade pedido/fora pedido sem autoriza��o.                                 -- }
  { -- se for s� divergencia de pre�o libera as etiquetas                                      -- }
  { -- se n�o for recebimento cego                                                             -- }
  if (qryTipoRecebcFlgRecebCego.Value = 0) then
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT TOP 1 1                    ');
      qryAux.SQL.Add('  FROM HistAutorDiverg            ');
      qryAux.SQL.Add(' WHERE nCdRecebimento    = ' + qryMasternCdRecebimento.AsString) ;
      qryAux.SQL.Add('   AND nCdTabTipoDiverg IN (1,4,5)');
      qryAux.SQL.Add('   AND dDtAutorizacao   IS NULL   ');
      qryAux.Open;

      if not qryAux.Eof then
      begin
          qryAux.Close;
          if (frmMenu.LeParametro('VAREJO') = 'S') then
              MensagemErro('Produto com diverg�ncia de grade ou fora de pedido. Emiss�o de etiquetas n�o permitida.')
          else
              MensagemErro('Produto com diverg�ncia de quantidade ou fora de pedido. Emiss�o de etiquetas n�o permitida.');
          abort ;
      end ;

      qryAux.Close;

  end ;

  {-- posiciona o dataset com os produtos para emitir as etiquetas --}
  PosicionaQuery(qryItemEtiqueta, qryMasternCdRecebimento.AsString) ;

  if qryItemEtiqueta.eof then
  begin
      MensagemAlerta('Nenhum produto para emitir etiqueta.');
      abort ;
  end ;

  SetLength(arrID,0) ;

  qryItemEtiqueta.First;

  iAuxTotal := 0 ;

  while not qryItemEtiqueta.Eof do
  begin
      for iAux := 0 to (qryItemEtiquetanQtde.AsInteger-1) do
      begin
          SetLength(arrID,Length(arrID)+1) ;
          arrID[iAuxTotal] := qryItemEtiquetanCdProduto.Value ;
          inc(iAuxTotal) ;
      end ;

      qryItemEtiqueta.Next;
  end ;

  qryItemEtiqueta.Close;

  objModImpETP := TfrmModImpETP.Create( Self ) ;

  objModImpETP.emitirEtiquetas(arrID,1) ;

  freeAndNil( objModImpETP ) ;

end;

function TfrmRecebimento.gerarProduto: integer;
begin

    Sleep(100) ;
    frmMenu.limpaBufferTeclado;

    showForm( objGeraProduto , FALSE ) ;

    if (objGeraProduto.nCdProdutoGerado > 0) then
    begin

        if (qryItemRecebimento.State = dsBrowse) then
            qryItemRecebimento.Insert ;

        qryItemRecebimentonCdProduto.Value   := objGeraProduto.nCdProdutoGerado ;
        qryItemRecebimentocCdProduto.Value   := intToStr( objGeraProduto.nCdProdutoGerado );
        qryItemRecebimentonValUnitario.Value := objGeraProduto.nValCustoUnitario ;

        DBGridEh1.Col := 1 ;
        DBGridEh1.SetFocus;

        exit ;

    end ;

    Result := objGeraProduto.nCdProdutoGerado ;

end;

procedure TfrmRecebimento.DBGridEh1KeyPress(Sender: TObject;
  var Key: Char);
var
    nValPedido : double ;
    State : TKeyboardState ;
    nPk   : integer;
begin
  inherited;

    GetKeyboardState(State);

    if (((State[vk_Control] And 128) <> 0)) and (Key = #10) then
    begin

        if (qryMaster.Active) then
            if (qryMasternCdRecebimento.Value > 0) then
                if not (DBGridEh1.ReadOnly) then
                    if (frmMenu.LeParametro('CADPRODUTORECEB') = 'S') then
                        GerarProduto() ;

    end ;


end;

procedure TfrmRecebimento.qryItemRecebimentoAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryItemRecebimento.State = dsBrowse) then
  begin

      with DBGridEh1 do
      begin

          {-- desativa as colunas para edi��o --}
          Columns[FieldColumns['cNmItem'].Index].Title.Font.Color := clRed ;
          Columns[FieldColumns['cNmItem'].Index].ReadOnly         := True ;
          Columns[FieldColumns['cNmItem'].Index].Color            := clSilver ;

          {-- se n�o for item AD, desativa a edi��o do nome do item --}
          if (qryItemRecebimentonCdTipoItemPed.Value <> 5) then
              PosicionaQuery(qryProduto,qryItemRecebimentonCdProduto.asString) ;

          {-- item Ad --}
          if qryItemRecebimentonCdTipoItemPed.Value = 5 then
          begin

              {-- se o status do recebimento estiver DIGITADO ou com DIVERGENCIA, libera o nome do item para edi��o --}

              if (qryMasternCdTabStatusReceb.Value <= 2) then
              begin
                  Columns[FieldColumns['cNmItem'].Index].Title.Font.Color := clBlack ;
                  Columns[FieldColumns['cNmItem'].Index].ReadOnly         := False ;
                  Columns[FieldColumns['cNmItem'].Index].Color            := clWhite ;
              end;

              Visible := True;
          end

      end ;

      verificaControlesICMS( qryItemRecebimentocCdST.Value ) ;

  end ;

end;

procedure TfrmRecebimento.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  ExibirDivergncias1.Enabled := ((qryItemRecebimento.Active) and (qryItemRecebimentonCdItemRecebimento.Value > 0)) ;
  ExcluirItem1.Enabled       := ((qryItemRecebimento.Active) and (qryItemRecebimentonCdItemRecebimento.Value > 0) and (qryMasternCdTabStatusReceb.Value <= 2)) ;
  VincularPedido1.Enabled    := ((qryItemRecebimento.Active) and (qryItemRecebimentonCdItemRecebimento.Value > 0) and (qryMasternCdTabStatusReceb.Value <= 2)) ;

end;

procedure TfrmRecebimento.vinculaPedidoitemRecebimento(nCdItemRecebimento:integer;nCdProduto:integer);
begin
    cxPageControl1.ActivePageIndex := 0 ;

    objConsultaItemPedidoAberto.nCdTerceiro    := qryMasternCdTerceiro.Value ;
    objConsultaItemPedidoAberto.nCdTipoReceb   := qryMasternCdTipoReceb.Value ;
    objConsultaItemPedidoAberto.nCdLoja        := qryMasternCdLoja.Value ;
    objConsultaItemPedidoAberto.nCdRecebimento := qryMasternCdRecebimento.Value ;
    objConsultaItemPedidoAberto.nCdPedido      := 0 ;
    objConsultaItemPedidoAberto.nCdProduto     := nCdProduto ;

    showForm( objConsultaItemPedidoAberto , FALSE ) ;

    {-- se foi selecionado algum item do pedido , vincula ao item do recebimento --}
    if (objConsultaItemPedidoAberto.qryItemPedido.Active) and (objConsultaItemPedidoAberto.bSelecionado) and (objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.Value > 0) then
    begin

        if (MessageDlg('Confirma a vincula��o deste item de recebimento com o pedido ' + objConsultaItemPedidoAberto.qryPedidonCdPedido.asString + ' ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
            exit ;

        frmMenu.Connection.BeginTrans;

        try
            SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.Close;
            SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.Parameters.ParamByName('@nCdItemRecebimento').Value := nCdItemRecebimento;
            SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.Parameters.ParamByName('@nCdItemPedido').Value      := objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.Value;
            SP_VINCULA_ITEMRECEBIMENTO_ITEMPEDIDO.ExecProc;
        except
            frmMenu.Connection.RollbackTrans;
            MensagemErro('Erro no processamento.') ;
            raise ;
        end ;

        frmMenu.Connection.CommitTrans ;

        ShowMessage('Vincula��o processada com sucesso.') ;

        qryItemRecebimento.Requery();

    end ;

end;

procedure TfrmRecebimento.VincularPedido1Click(Sender: TObject);
begin

    vinculaPedidoitemRecebimento(qryItemRecebimentonCdItemRecebimento.Value, qryItemRecebimentonCdProduto.Value) ;

end;

procedure TfrmRecebimento.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
  begin

      {-- se for um inteiro, retira os zeros da esquerda e manter somente os numeros --}
      try
          qryMastercNrDocto.Value := intToStr(StrToInt(qryMastercNrDocto.Value)) ;
      except
      end ;

  end ;

end;

procedure TfrmRecebimento.verificaControlesICMS(cCST: string);
var
    bICMS   , bICMSST    : boolean ;
    bICMSRed, bICMSSTRed : boolean ;
    i                    : integer ;

begin

    if (not qryMaster.Active) then
        exit ;

    if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 0) then
    begin

        if (qryItemRecebimentonPercICMSSub.Value = 0) and (qryItemRecebimento.State in [dsInsert,dsEdit]) then
        begin
             qryItemRecebimentonValBaseCalcSubTrib.Value     := 0;
             qryItemRecebimentonValICMSSub.Value             := 0;
             qryItemRecebimentonPercRedBaseCalcSubTrib.Value := 0;
        end ;

        exit ;
    end ;

    bICMS   := true ;
    bICMSST := false ;

    if (trim(RightStr(cCST,2)) = '60') or (trim(RightStr(cCST,2)) = '70') then
        bICMS := false ;

    if (trim(RightStr(cCST,2)) = '10') or (trim(RightStr(cCST,2)) = '20') or (trim(RightStr(cCST,2)) = '30') or (trim(RightStr(cCST,2)) = '70') then
    begin
        bICMSST := true ;
    end ;

    bICMSRed   := (trim(RightStr(cCST,2)) = '20') or (trim(RightStr(cCST,2)) = '70') ;
    bICMSSTRed := (trim(RightStr(cCST,2)) = '70') ;

    // formata as colunas do grid
    with DBGridEh1 do
    begin

        {-- ICMS NORMAL --}
        // % reducao base calc icms
        Columns[FieldColumns['nPercRedBaseCalcICMS'].Index].ReadOnly := (not bICMSRed) ;

        if (bICMSRed)  then
            Columns[FieldColumns['nPercRedBaseCalcICMS'].Index].Title.Font.Color := clBlack
        else
            Columns[FieldColumns['nPercRedBaseCalcICMS'].Index].Title.Font.Color := clRed ;


        {-- as colunas de base de c�lculo do ICMS de Valor do ICMS SEMPRE ficam somente leitura --}
        Columns[FieldColumns['nBaseCalcICMS'].Index].ReadOnly := TRUE ;
        Columns[FieldColumns['nValICMS'].Index].ReadOnly      := TRUE ;

        Columns[FieldColumns['nBaseCalcICMS'].Index].Title.Font.Color := clRed  ;
        Columns[FieldColumns['nValICMS'].Index].Title.Font.Color      := clRed ;

        {-- *** Substitui��o Tributaria *** --}
        {-- se bICMSST for TRUE, indica que o CST do item tem substitui��o tribut�ria --}
        {-- quando bICMSST for TRUE, ele precisa deixar o ReadOnly como false, por isso � atribuido o valor de (not bICMSST) --}

        Columns[FieldColumns['nPercIVA'].Index].ReadOnly            := (not bICMSST) ;
        Columns[FieldColumns['nAliqICMSInterna'].Index].ReadOnly    := (not bICMSST) ;
        Columns[FieldColumns['nValBaseCalcSubTrib'].Index].ReadOnly := (not bICMSST) ;
        Columns[FieldColumns['nValICMSSub'].Index].ReadOnly         := (not bICMSST) ;

        if (bICMSST)  then
        begin

            Columns[FieldColumns['nPercIVA'].Index].Title.Font.Color            := clBlack ;
            Columns[FieldColumns['nAliqICMSInterna'].Index].Title.Font.Color    := clBlack ;
            Columns[FieldColumns['nValBaseCalcSubTrib'].Index].Title.Font.Color := clBlack ;
            Columns[FieldColumns['nValICMSSub'].Index].Title.Font.Color         := clBlack ;

        end
        else
        begin

            Columns[FieldColumns['nPercIVA'].Index].Title.Font.Color            := clRed ;
            Columns[FieldColumns['nAliqICMSInterna'].Index].Title.Font.Color    := clRed ;
            Columns[FieldColumns['nValBaseCalcSubTrib'].Index].Title.Font.Color := clRed ;
            Columns[FieldColumns['nValICMSSub'].Index].Title.Font.Color         := clRed ;

        end ;

    end ;

    if (qryItemRecebimento.State in [dsInsert,dsEdit]) then
    begin
         if not (bICMS) Then
         Begin
             qryItemRecebimentonPercAliqICMS.Value        := 0;
             qryItemRecebimentonBaseCalcICMS.Value        := 0;
             qryItemRecebimentonValICMS.Value             := 0;
             qryItemRecebimentonPercRedBaseCalcICMS.Value := 0;
         end;

         if not (bICMSRed) then
             qryItemRecebimentonPercRedBaseCalcICMS.Value := 0;

         if not (bICMSST) then
         begin
             qryItemRecebimentonPercIVA.Value                := 0;
             qryItemRecebimentonAliqICMSInterna.Value        := 0;
             qryItemRecebimentonValBaseCalcSubTrib.Value     := 0;
             qryItemRecebimentonPercICMSSub.Value            := 0;
             qryItemRecebimentonValICMSSub.Value             := 0;
             qryItemRecebimentonPercRedBaseCalcSubTrib.Value := 0;
         end ;

         if not (bICMSSTRed) then
             qryItemRecebimentonPercRedBaseCalcSubTrib.Value := 0;
    end;

    calculaTotaisItem( FALSE ) ;

end;

procedure TfrmRecebimento.qryItemRecebimentocCdSTChange(Sender: TField);
begin
  inherited;

  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) and (qryItemRecebimentocCdST.Value <> '') then
  begin

      if NOT(Length(qryItemRecebimentocCdST.Value) IN [3, 4]) then
      begin
          MensagemAlerta('O CST/CSOSN do item deve ter de 3 � 4 d�gitos. Ex: 010, 0100');
          abort ;
      end ;

      PosicionaQuery(qryTipoTributacaoICMS_Aux,RightStr(qryItemRecebimentocCdSt.AsString,2));

      if qryTipoTributacaoICMS_Aux.eof then
      begin
          MensagemAlerta('C�digo de Situa��o Tribut�ria Inv�lido');
          abort;
      end;

      verificaControlesICMS( qryItemRecebimentocCdST.Value ) ;

      {-- verifica os par�metros do produto x fornecedor para j� sugerir algumas informa��es --}
      if (qryItemRecebimentonCdTipoItemPed.Value <> 5) then  {-- se n�o for item AD --}
      begin

        if (qryItemRecebimento.State = dsBrowse) then
            exit ;

        qryProdutoFornecedor.Close;

        qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value;
        PosicionaQuery( qryProdutoFornecedor , qryItemRecebimentonCdProduto.asString ) ;

        if not qryProdutoFornecedor.eof then
        begin

            {-- traz os valores pre-definidos no cadastro do fornecedor --}
            qryItemRecebimentonPercIPI.Value         := qryProdutoFornecedornPercIPI.Value ;
            qryItemRecebimentonPercIVA.Value         := qryProdutoFornecedornPercIVA.Value ;
            qryItemRecebimentonAliqICMSInterna.Value := qryProdutoFornecedornPercICMSSub.Value ;

            if (qryItemRecebimentonPercIPI.Value > 0) then
                qryItemRecebimentonValIPI.Value  := ((qryItemRecebimentonValTotal.Value-qryItemRecebimentonValDesconto.Value) * (qryItemRecebimentonPercIPI.Value/100)) ;

            {-- calcula a base do ICMS --}
            if (qryProdutoFornecedornPercBCICMSSub.Value > 0) and (DBGridEh1.Columns[13].ReadOnly) then
            begin
                MensagemAlerta('O produto est� vinculado ao fornecedor com redu��o na base de c�lculo do ICMS. O CST digitado para o item n�o permite redu��o na base. Verifique.') ;
                DBGridEh1.Col := 6;
                abort ;
            end ;

            if (qryProdutoFornecedornPercBCICMSSub.Value > 0) and (not DBGridEh1.Columns[13].ReadOnly) then
                qryItemRecebimentonPercRedBaseCalcICMS.Value := qryProdutoFornecedornPercBCICMSSub.Value ;

            qryItemRecebimentonBaseCalcICMS.Value := (qryItemRecebimentonValTotal.Value * (qryItemRecebimentonPercRedBaseCalcICMS.Value / 100)) ;

        end ;

      end;

  end;

end;

procedure TfrmRecebimento.qryItemRecebimentocCFOPChange(Sender: TField);
begin
  inherited;

  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) and (qryItemRecebimentocCFOP.Value <> '') then
  begin

      checaValidadeCFOP( qryItemRecebimentocCFOP.Value , TRUE ) ;

      PosicionaQuery(qryCfop,qryItemRecebimentocCfop.AsString);

      qryItemRecebimentocFlgGeraLivroFiscal.Value    := qryCFOPcFlgGeraLivroFiscal.Value;
      qryItemRecebimentocFlgGeraCreditoICMS.Value    := qryCFOPcFlgGeraCreditoICMS.Value;
      qryItemRecebimentocFlgGeraCreditoIPI.Value     := qryCFOPcFlgGeraCreditoIPI.Value;
      qryItemRecebimentocFlgImportacao.Value         := qryCFOPcFlgImportacao.Value;

      {-- se o terceiro for optante pelo simples, n�o gera credito de ICMS nem IPI --}
      if (qryTerceirocFlgOptSimples.Value = 1) then
      begin
          qryItemRecebimentocFlgGeraCreditoICMS.Value := 0;
          qryItemRecebimentocFlgGeraCreditoIPI.Value  := 0;
      end ;

      {-- verifica os par�metros do produto x fornecedor para j� sugerir algumas informa��es --}
      if (qryItemRecebimentonCdTipoItemPed.Value <> 5) then  {-- se n�o for item AD --}
      begin

        if (qryItemRecebimento.State = dsBrowse) then
            exit ;

        qryProdutoFornecedor.Close;

        qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value;
        PosicionaQuery( qryProdutoFornecedor , qryItemRecebimentonCdProduto.asString ) ;

        if not qryProdutoFornecedor.eof then
        begin

            qryItemRecebimentonPercIPI.Value := qryProdutoFornecedornPercIPI.Value ;

            if (qryItemRecebimentonPercIPI.Value > 0) then
                qryItemRecebimentonValIPI.Value  := ((qryItemRecebimentonValTotal.Value-qryItemRecebimentonValDesconto.Value) * (qryItemRecebimentonPercIPI.Value/100)) ;


            {-- calcula a base do ICMS --}
            if (qryProdutoFornecedornPercBCICMSSub.Value > 0) and (DBGridEh1.Columns[13].ReadOnly) then
            begin
                MensagemAlerta('O produto est� vinculado ao fornecedor com redu��o na base de c�lculo do ICMS. O CST digitado para o item n�o permite redu��o na base. Verifique.') ;
                DBGridEh1.Col := 6;
                abort ;
            end ;

            if (qryProdutoFornecedornPercBCICMSSub.Value > 0) and (not DBGridEh1.Columns[13].ReadOnly) then
                qryItemRecebimentonPercRedBaseCalcICMS.Value := qryProdutoFornecedornPercBCICMSSub.Value ;

            qryItemRecebimentonBaseCalcICMS.Value := (qryItemRecebimentonValTotal.Value * (qryItemRecebimentonPercRedBaseCalcICMS.Value / 100)) ;

        end ;

      end;

  end;

end;

procedure TfrmRecebimento.DBGridEh4DblClick(Sender: TObject);
var
    objForm : TfrmRegistroMovLote ;
begin

  objForm := TfrmRegistroMovLote.Create( Self ) ;

  objForm.registraMovLote( qryProdLotenCdProduto.Value
                         , 0
                         , 2 {-- Tipo de movimenta��o que esta sendo realizada --}
                         , qryProdLotenCdItemRecebimento.Value
                         , 0 {-- o local de estoque sera ajustado atraves da procedure --}
                         , qryProdLotenQtde.Value
                         , 'RECEBIMENTO DE MERCADORIAS - ITEM No. ' + Trim( qryProdLotenCdItemRecebimento.AsString )
                         , True
                         , bReadOnly
                         , False) ;

  freeAndNil( objForm ) ;
end;

procedure TfrmRecebimento.btRegistroLoteSerialClick(Sender: TObject);
var
    objForm : TfrmRegistroMovLote ;
begin

  if (qryProdLote.Active) and (qryProdLote.RecordCount > 0) then
  begin

      objForm := TfrmRegistroMovLote.Create( Self ) ;

      objForm.registraMovLote( qryProdLotenCdProduto.Value
                             , 0
                             , 2 {-- Tipo de movimenta��o que esta sendo realizada --}
                             , qryProdLotenCdItemRecebimento.Value
                             , 0 {-- o local de estoque sera ajustado atraves da procedure --}
                             , qryProdLotenQtde.Value
                             , 'RECEBIMENTO DE MERCADORIAS - ITEM No. ' + Trim( qryProdLotenCdItemRecebimento.AsString )
                             , True
                             , bReadOnly
                             , False) ;

      freeAndNil( objForm ) ;

  end ;

end;

procedure TfrmRecebimento.cxButton7Click(Sender: TObject);
var
   objLerXML : TfrmRecebimentoLeituraXML ;
   i : integer;
begin
  inherited;

  if (not qryMaster.Active) or (qryMasternCdRecebimento.Value = 0) then
  begin
      MensagemAlerta('Salve o Recebimento antes de importar XML.');
      abort;
  end;

  if (qryMasternCdTabStatusReceb.Value <> 1) then
  begin
      MensagemAlerta('Status do Recebimento n�o permite importar XML.');
      abort;
  end;

  if (qryTipoReceb.Eof) then
  begin
      MensagemAlerta('Informe o Tipo de Recebimento.');
      abort;
  end;

  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 0) then
  begin
      MensagemAlerta('Tipo de Recebimento n�o permite importar o XML.');
      abort;
  end;

  if Trim(DBEdit7.Text) = '' then
  begin
      MensagemAlerta('Informe o N�mero da NFe antes de importar o XML.');
      DBEdit7.setFocus;
      abort;
  end;

  if Trim(DBEdit18.Text) = '' then
  begin
      MensagemAlerta('Informe o Terceiro da NFe antes de importar o XML.');
      DBEdit6.setFocus;
      abort;
  end;

  if (not qryItemRecebimento.Eof) then
  begin
      MensagemAlerta('Exclua os itens do recebimento antes de processar a leitura do arquivo XML.');
      DBGridEh1.SetFocus;
      Exit;
  end;

  objLerXML := TfrmRecebimentoLeituraXML.Create( Self ) ;

  OpenDialog1.FileName  :=  '';
  OpenDialog1.Title := 'Selecione um arquivo NFE XML';
  OpenDialog1.DefaultExt := '*-nfe.XML';
  OpenDialog1.Filter := 'Arquivos NFE (*-nfe.XML)|*-nfe.XML|Arquivos XML (*.XML)|*.XML|Arquivos TXT (*.TXT)|*.TXT|Todos os Arquivos (*.*)|*.*';

  if OpenDialog1.Execute then
  begin

      if (OpenDialog1.FileName <> '') then
      begin

          //tenta XML
          ACBrNFe1.NotasFiscais.Clear;
          try
              ACBrNFe1.NotasFiscais.LoadFromFile(OpenDialog1.FileName);
          except
              MensagemAlerta('Arquivo NFe Inv�lido.');
              exit;
          end;

      end
      else
      begin
          exit ;
      end ;

  end
  else
  begin

      exit ;

  end ;

  with ACBrNFe1.NotasFiscais.Items[0].NFe do
  begin

     objLerXML.qryTerceiro.Close;
     objLerXML.qryTerceiro.Parameters.ParamByName('cCnpjCpf').Value := Trim(emit.CNPJCPF);
     objLerXML.qryTerceiro.Open;

     if Trim(DBEdit7.Text) <> Trim(intToStr(ide.nNF)) then
     begin
         MensagemAlerta('O N�mero da NF do XML � diferente do N�mero da NF informado neste recebimento. NF Selecionada: ' + intToStr(ide.nNF));
         DBEdit7.setFocus;
         abort;
     end;

     //verificar cnpj
     if qryTerceirocCnpjCpf.Value <> Trim(emit.CNPJCPF) then
     begin
         MensagemAlerta('O CNPJ do Emitente da NFe � diferente do CNPJ do Terceiro selecionado.');
         DBEdit6.setFocus;
         abort;
     end;

     objLerXML.qryNFeXML.Close;
     objLerXML.qryNFeXML.ExecSQL;
     objLerXML.qryNFeXML.Open;

     with ACBrNFe1.NotasFiscais.Items[0].NFe do
     begin

         for I := 0 to Det.Count-1 do
         begin

             with Det.Items[I] do
             begin

                 objLerXML.qryNFeXML.Insert;
                 objLerXML.qryNFeXMLnCdRecebimento.Value       := qryMasternCdRecebimento.Value;
                 objLerXML.qryNFeXMLcCdProdutoFornecedor.Value := Prod.cProd;
                 objLerXML.qryNFeXMLcNmProdutoFornecedor.Value := Prod.xProd;
                 objLerXML.qryNFeXMLnQtdeFornecedor.Value      := Prod.qCom;
                 objLerXML.qryNFeXMLcUnMedidaFornecedor.Value  := Prod.uCom;
                 objLerXML.qryNFeXMLnValUnitario.Value         := Prod.vUnCom;
                 objLerXML.qryNFeXMLnValTotal.Value            := Prod.vProd;
                 objLerXML.qryNFeXMLnValDesconto.Value         := Prod.vDesc;
                 objLerXML.qryNFeXMLcNCM.Value                 := Prod.NCM;

                 objLerXML.qryNFeXMLnValSeguro.Value           := Prod.vSeg;
//                 objLerXML.qryNFeXMLnValDespAcessorias.Value   := Prod.vOutro;
                 objLerXML.qryNFeXMLnValFrete.Value            := Prod.vFrete;

                 objLerXML.qryNFeXMLcOrigemMercadoria.Value    := OrigToStr( Imposto.ICMS.orig ); 
                 objLerXML.qryNFeXMLcCdST.Value                := CSTICMSToStrTagPos( Imposto.ICMS.CST );
                 objLerXML.qryNFeXMLnValBaseICMS.Value         := Imposto.ICMS.vBC;
                 objLerXML.qryNFeXMLnValICMS.Value             := Imposto.ICMS.vICMS;
                 objLerXML.qryNFeXMLnPercAliqICMS.Value        := Imposto.ICMS.pICMS;

                 if (Imposto.ICMS.pRedBC > 0) then
                     objLerXML.qryNFeXMLnPercRedBaseIcms.Value := (100 - Imposto.ICMS.pRedBC) ;

                 if (Imposto.ICMS.pRedBCST > 0) then
                     objLerXML.qryNFeXMLnPercRedBaseICMSSt.Value := (100 - Imposto.ICMS.pRedBCST) ;

                 objLerXML.qryNFeXMLnValBaseICMSST.Value       := Imposto.ICMS.vBCST;
                 objLerXML.qryNFeXMLnValICMSST.Value           := Imposto.ICMS.vICMSST;
                 objLerXML.qryNFeXMLnpercAliqICMSST.Value      := Imposto.ICMS.pICMSST;
                 objLerXML.qryNFeXMLnPercIVA.Value             := Imposto.ICMS.pMVAST;

                 objLerXML.qryNFeXMLnValBaseIPI.Value          := Imposto.IPI.vBC ;
                 objLerXML.qryNFeXMLnPercAliqIPI.Value         := Imposto.IPI.pIPI;
                 objLerXML.qryNFeXMLnValIPI.Value              := Imposto.IPI.vIPI;
                 objLerXML.qryNFeXMLcCSTIPI.Value              := CSTIPIToStr( Imposto.IPI.CST) ;

                 objLerXML.qryNFeXMLcCdSt.Value                := CSTICMSToStr(Imposto.ICMS.CST);
                 objLerXML.qryNFeXMLcCFOP.Value                := Prod.CFOP;

                 objLerXML.qryProdutoFornecedor.Close;
                 objLerXML.qryProdutoFornecedor.Parameters.ParamByName('cProduto').Value    := Prod.cProd;
                 objLerXML.qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value;
                 objLerXML.qryProdutoFornecedor.Open;

                 if not objLerXML.qryProdutoFornecedor.eof then
                 begin

                     objLerXML.qryProduto.Close;
                     objLerXML.qryProduto.Parameters.ParamByName('nPk').Value := objLerXML.qryProdutoFornecedornCdProduto.Value;
                     objLerXML.qryProduto.Open;

                     if not objLerXML.qryProduto.eof then
                     begin

                         objLerXML.qryNFeXMLnCdProduto.Value           := objLerXML.qryProdutonCdProduto.Value;
                         objLerXML.qryNFeXMLcNmProduto.Value           := objLerXML.qryProdutocNmProduto.Value;
                         objLerXML.qryNFeXMLcUnMedida.Value            := objLerXML.qryProdutocUnidadeMedida.Value;

                     end ;

                 end ;

                 objLerXML.qryNFeXML.Post;

             end;

         end;

     end;

     objLerXML.qryNFeXML.Open;

     showForm( objLerXML , FALSE ) ;

     PosicionaQuery(qryItemRecebimento,qryMasternCdRecebimento.AsString);
 end;

end;

procedure TfrmRecebimento.atualizaColunasInformacoesFiscais;
begin

  if qryTipoReceb.Eof then
      exit;

  {-- quando o tipo de recebimento n�o exigir informa��es fiscais, oculta v�rias colunas --}
  with DBGridEh1 do
  begin

        Columns[FieldColumns['nBaseCalcICMS'].Index].Visible        := (qryTipoRecebcFlgExigeNF.Value = 1) ;
        Columns[FieldColumns['nPercAliqICMS'].Index].Visible        := (qryTipoRecebcFlgExigeNF.Value = 1) ;
        Columns[FieldColumns['nValICMS'].Index].Visible             := (qryTipoRecebcFlgExigeNF.Value = 1) ;
        Columns[FieldColumns['nPercIVA'].Index].Visible             := (qryTipoRecebcFlgExigeNF.Value = 1) ;
        Columns[FieldColumns['nAliqICMSInterna'].Index].Visible     := (qryTipoRecebcFlgExigeNF.Value = 1) ;
        Columns[FieldColumns['nPercRedBaseCalcICMS'].Index].Visible := (qryTipoRecebcFlgExigeNF.Value = 1) ;

        Columns[FieldColumns['cCdST'].Index].Visible                := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cCFOP'].Index].Visible                := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cCFOPNF'].Index].Visible              := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cNCM'].Index].Visible                 := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cFlgGeraLivroFiscal'].Index].Visible  := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cFlgGeraCreditoICMS'].Index].Visible  := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cFlgGeraCreditoIPI'].Index].Visible   := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['cFlgImportacao'].Index].Visible       := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
//        Columns[FieldColumns['nValFrete'].Index].Visible            := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['nValSeguro'].Index].Visible           := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['nValAliquotaII'].Index].Visible       := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;
        Columns[FieldColumns['nValAcessorias'].Index].Visible       := (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) ;

  end ;

end;

procedure TfrmRecebimento.sugerirCalculoST (bCalcularIPI : boolean) ;
begin


    if (qryItemRecebimento.Active) and (qryItemRecebimentonCdProduto.Value > 0) then
    begin

        if (qryItemRecebimento.State = dsBrowse) then
            exit ;

        qryProdutoFornecedor.Close;

        qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value;
        PosicionaQuery( qryProdutoFornecedor , qryItemRecebimentonCdProduto.asString ) ;

        if not qryProdutoFornecedor.eof then
        begin

            if (bCalcularIPI) then
            begin
                qryItemRecebimentonPercIPI.Value := qryProdutoFornecedornPercIPI.Value ;

                if (qryItemRecebimentonPercIPI.Value > 0) then
                begin
                    qryItemRecebimentonValIPI.Value  := ((qryItemRecebimentonValTotal.Value-qryItemRecebimentonValDesconto.Value) * (qryItemRecebimentonPercIPI.Value/100)) ;
                end ;
            end ;

            {-- calcula a base da ST --}

            qryItemRecebimentonValBaseCalcSubTrib.Value := (qryItemRecebimentonValTotal.Value + qryItemRecebimentonValIPI.Value) * (1 + (qryItemRecebimentonPercIVA.Value/100)) ;
            qryItemRecebimentonValICMSSub.Value         := (qryItemRecebimentonValBaseCalcSubTrib.Value * (qryItemRecebimentonAliqICMSInterna.Value / 100)) - qryItemRecebimentonValICMS.Value ;
            qryItemRecebimentonPercICMSSub.Value        := qryItemRecebimentonPercAliqICMS.Value ;

        end ;

    end ;

end;

procedure TfrmRecebimento.DBEdit12Exit(Sender: TObject);
begin
  inherited;

  {-- se o tipo de recebimento exige os dados fiscais, obrigar informar a UF de origem da NF de entrada --}
  if (qryMaster.Active) and (DBEdit17.Text <> '') then
  begin

      if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) then
      begin

          if (pos('#',DBEdit12.Text) > 0) or (pos( trim(DBEdit12.Text) , frmMenu.estadosBrasileiros ) = 0) then
          begin
              MensagemAlerta('Unidade federativa inv�lida. Ex: SP / MG / RJ. Utilize EX para notas de outros pa�ses.') ;
              DBEdit12.SetFocus;
              abort ;
          end;

      end ;

  end ;

end;

procedure TfrmRecebimento.checaValidadeCFOP( cCFOP : string ; bCFOPEntrada : boolean );
begin
    PosicionaQuery( qryCFOP , cCFOP );

    if qryCFOP.Eof then
    begin
        MensagemAlerta('CFOP ' + cCFOP + ' n�o cadastrado.');
        abort;
    end;

    //Os CFOP�s de Entrada Iniciam com 1,2,3 e os CFOP�s de sa�da iniciam com 5,6,7.

    if (Pos(Copy(cCFOP,1,1),'#1#2#3#') = 0) and (bCFOPEntrada) then
    begin
        MensagemAlerta('CFOP inv�lido para a opera��o. Os CFOP�s de Entrada iniciam-se com 1,2 ou 3.');
       abort;
    end;

    if (Pos(Copy(cCFOP,1,1),'#5#6#7#') = 0) and (not bCFOPEntrada) then
    begin
        MensagemAlerta('CFOP inv�lido para a opera��o. Os CFOP�s de Sa�da (NF) iniciam-se com 5,6 ou 7.');
       abort;
    end;

    PosicionaQuery(qryEnderecoEmpresa,qryEmpresanCdTerceiroEmp.AsString);

    if not qryEnderecoEmpresa.Eof then
    begin

        {-- se for uma opera��o interestadual, verifica se o CFOP come�a com 5 ou 1, caso comece avisa que est� errado --}
        {-- todo CFOP de opera��o fora do estado deve iniciar com 2,3 para entrada e 6,7 para sa�da                    --}
        if (DBEdit12.Text <> qryEnderecoEmpresacUf.Value) and ( ( Copy(cCFOP,1,1) = '1' ) or ( Copy(cCFOP,1,1) = '5' ) ) then
        begin
            MensagemAlerta('O CFOP informado � um CFOP para opera��es dentro do estado. Utilize um CFOP para opera��es de fora do estado.');
            abort;
        end;

        {-- se for uma opera��o interestadual, verifica se o CFOP come�a com 5 ou 1, caso comece avisa que est� errado --}
        {-- todo CFOP de opera��o fora do estado deve iniciar com 2,3 para entrada e 6,7 para sa�da                    --}
        if (DBEdit12.Text = qryEnderecoEmpresacUf.Value) and ( ( Copy(cCFOP,1,1) <> '1' ) and ( Copy(cCFOP,1,1) <> '5' ) ) then
        begin
            MensagemAlerta('O CFOP informado � um CFOP para opera��es fora do estado. Utilize um CFOP para opera��es de dentro do estado.');
            abort;
        end;

    end;

end;

procedure TfrmRecebimento.qryItemRecebimentocCFOPNFChange(Sender: TField);
begin
  inherited;

  if (qryTipoRecebcFlgExigeInformacoesFiscais.Value = 1) and (qryItemRecebimentocCFOP.Value <> '') then
      checaValidadeCFOP( qryItemRecebimentocCFOPNF.Value , FALSE ) ;

end;

procedure TfrmRecebimento.calculaTotaisItem(bGravar: boolean);
begin

  if (qryItemRecebimento.State in ([dsInsert,dsEdit])) then
  begin

      {-- TOTAL DO ITEM --}
      qryItemRecebimentonValTotal.Value := qryItemRecebimentonValUnitario.Value * qryItemRecebimentonQtde.Value ;
      qryItemRecebimentonValTotal.Value := frmMenu.TBRound(qryItemRecebimentonValTotal.Value,2);

      //Calcula o desconto no item conforme percentual do desconto no valor dos produtos da nota
      if (qryItemRecebimentonPercDesconto.Value = 0) and (qryMasternPercDescProduto.Value > 0) then
      begin
          qryItemRecebimentonPercDesconto.Value := qryMasternPercDescProduto.Value ;
          qryItemRecebimentonValDesconto.Value  := (qryItemRecebimentonValTotal.Value * (qryItemRecebimentonPercDesconto.Value/100)) ;
      end ;

      // desconto
      qryItemRecebimentonValDesconto.Value := 0 ;

      if (qryItemRecebimentonPercDesconto.Value > 0) then
          qryItemRecebimentonValDesconto.Value := (qryItemRecebimentonValTotal.Value * (qryItemRecebimentonPercDesconto.Value/100)) ;

      {-- ICMS NORMAL --}
      if (qryItemRecebimentonPercAliqICMS.Value <> 0) then
      begin

          if (qryItemRecebimentonPercRedBaseCalcICMS.Value <> 0) then
              qryItemRecebimentonBaseCalcICMS.Value  := ( qryItemRecebimentonValTotal.Value - qryItemRecebimentonValDesconto.Value ) * (qryItemRecebimentonPercRedBaseCalcICMS.Value / 100)
          else qryItemRecebimentonBaseCalcICMS.Value := ( qryItemRecebimentonValTotal.Value - qryItemRecebimentonValDesconto.Value ) ;

          qryItemRecebimentonValICMS.Value := qryItemRecebimentonBaseCalcICMS.Value * (qryItemRecebimentonPercAliqICMS.Value / 100);

      end
      else
      begin

          qryItemRecebimentonValICMS.Value := 0 ;

          if (bGravar) then
          begin
              qryItemRecebimentonBaseCalcICMS.Value        := 0 ;
              qryItemRecebimentonPercRedBaseCalcICMS.Value := 0 ;
          end ;

      end ;


      {-- IPI --}
      if (qryItemRecebimentonPercIPI.Value > 0) then
      begin

          if (qryItemRecebimentonPercBaseCalcIPI.Value > 0) then
              qryItemRecebimentonValBaseIPI.Value := (qryItemRecebimentonValTotal.Value * (qryItemRecebimentonPercBaseCalcIPI.Value / 100))
          else qryItemRecebimentonValBaseIPI.Value := qryItemRecebimentonValTotal.Value ;

          qryItemRecebimentonValIPI.Value  := (qryItemRecebimentonValBaseIPI.Value * (qryItemRecebimentonPercIPI.Value/100)) ;

      end
      else
      begin
          qryItemRecebimentonValIPI.Value := 0 ;

          if (bGravar) then
          begin
              qryItemRecebimentonPercBaseCalcIPI.Value := 0 ;
              qryItemRecebimentonValBaseIPI.Value      := 0 ;
          end ;

      end ;

      {-- ICMS ST --}
      if not DBGridEh1.Columns[DBGridEh1.FieldColumns['nValBaseCalcSubTrib'].Index].ReadOnly then
      begin
          qryItemRecebimentonValBaseCalcSubTrib.Value := (qryItemRecebimentonValTotal.Value + qryItemRecebimentonValIPI.Value) * (1 + (qryItemRecebimentonPercIVA.Value/100)) ;
          qryItemRecebimentonValICMSSub.Value         := (qryItemRecebimentonValBaseCalcSubTrib.Value * (qryItemRecebimentonAliqICMSInterna.Value / 100)) - qryItemRecebimentonValICMS.Value ;
          qryItemRecebimentonPercICMSSub.Value        := qryItemRecebimentonPercAliqICMS.Value ;

          if (qryItemRecebimentonAliqICMSInterna.Value = 0) and (qryItemRecebimentonPercIVA.Value = 0) and (bGravar) then
          begin
              qryItemRecebimentonValBaseCalcSubTrib.Value := 0 ;
              qryItemRecebimentonValICMSSub.Value         := 0 ;
              qryItemRecebimentonPercICMSSub.Value        := 0 ;
          end ;

      end ;

  end ;

end;

procedure TfrmRecebimento.btCadProdutoClick(Sender: TObject);
var
  objForm : TfrmProdutoERP;
begin
  inherited;

  objForm := TfrmProdutoERP.Create( Self );
  showForm( objForm , TRUE ) ;

end;

procedure TfrmRecebimento.btCadProdutoGradeClick(Sender: TObject);
var
  objForm : TfrmProduto;
begin
  inherited;

  objForm := TfrmProduto.Create( Self );
  showForm( objForm , TRUE ) ;

end;

procedure TfrmRecebimento.btExcluirClick(Sender: TObject);
begin
  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMasternCdTabStatusReceb.Value > 2) then
  begin
      MensagemAlerta('Status do recebimento n�o permite exclus�o.');
      Exit;
  end;

  { -- verifica se recebimento j� foi movimentado e n�o permite exclus�o do registro devido j� haver sido movimentado anteriormente -- }
  { -- este problema ocorre quando o recebimento ainda n�o foi atualizado o status entre os servidores -- }
  qryVerificaMovEstoque.Close;
  qryVerificaMovEstoque.Parameters.ParamByName('nPK').Value := IntToStr(qryMasternCdRecebimento.Value);
  qryVerificaMovEstoque.Open;

  if (not qryVerificaMovEstoque.IsEmpty) then
  begin
      MensagemAlerta('N�o � possivel excluir este recebimento devido j� ter sido movimentado anteriormente.');
      Abort;
  end;

  {-- Deleta as referencias como: centro de custo, historico de divergencia e itens do recebimento --}
  qryDeletaReferencias.Close;
  qryDeletaReferencias.Parameters.ParamByName('nCdRecebimento').Value := qryMasternCdRecebimento.Value;
  qryDeletaReferencias.ExecSQL;

  inherited;
end;

procedure TfrmRecebimento.cxButton8Click(Sender: TObject);
begin

  if (not qryItemRecebimento.Active) or (qryItemRecebimentonCdItemRecebimento.Value = 0) then
  begin
      MensagemAlerta('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  qryProduto.Parameters.ParamByName('nPK').Value := qryItemRecebimentocCdProduto.Value ;
  PosicionaQuery(qryProduto, qryItemRecebimentonCdProduto.asString) ;

  if qryProduto.eof then
  begin
      MensagemAlerta('Selecione um item.') ;
      exit ;
  end ;

  if (qryProdutonCdGrade.Value = 0) then
  begin
      MensagemAlerta('Este item n�o est� configurado para trabalhar em grade.') ;
      exit ;
  end ;

  objGrade.qryValorAnterior.SQL.Text := '' ;

  frmMenu.mensagemUsuario('Consultando grades...');

  if (qryItemRecebimentonCdItemRecebimento.Value > 0) then
      objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtde FROM ItemRecebimento WITH(INDEX=IN05_ItemRecebimento) WHERE nCdItemRecebimentoPai = ' + qryItemRecebimentonCdItemRecebimento.AsString + ' AND nCdTipoItemPed = 4 ORDER BY nCdProduto');

  objGrade.PreparaDataSet(qryItemRecebimentonCdProduto.Value);

  if (qryMasternCdTabStatusReceb.Value >= 3) or (qryMastercFlgRecebTransfEst.Value = 1) then
      objGrade.DBGridEh1.ReadOnly := True ;

  frmMenu.mensagemUsuario('Renderizando layout...');

  objGrade.Renderiza;
  objGrade.DBGridEh1.ReadOnly := False ;

  if (qryMasternCdTabStatusReceb.Value < 3) then
  begin
      frmMenu.mensagemUsuario('Habilitando edi��o...');

      qryItemRecebimento.Edit ;
      qryItemRecebimentonQtde.Value := objGrade.nQtdeTotal ;      //retorna valor zero e quantidade do item pedido fica zerado.

      DBGridEh1.Col := DBGridEh1.FieldColumns['nValUnitario'].Index;
      DBGridEh1.SetFocus;
  end ;

end;

procedure TfrmRecebimento.cxButton9Click(Sender: TObject);
var
  cCdRecebimento : String;
  cChaveNFeReceb : String;
begin
  inherited;

  if (frmMenu.LeParametro('CENTRODISTATIVO') <> 'S') then
  begin
      MensagemAlerta('Empresa n�o parametrizada para utiliza��o do Centro de Distribui��o.');
      Abort;
  end;

  if (qryMaster.State in [dsInsert,dsEdit])  then
  begin
      MensagemAlerta('Recebimento em aberto, feche este registro para continuar.');
      Abort;
  end;

  if qryMaster.Active then
      Abort;

  InputQuery('Gera��o de Recebimento Transf.','Informe a chave do documento fiscal.',cChaveNFeReceb);

  if (Trim(cChaveNFeReceb) = '') then
  begin
      ShowMessage('Chave do documento fiscal n�o informado.');
      Abort;
  end;

  {-- Valida se o documento fiscal j� foi recebido. --}
  qryDoctoRecebido.Close;
  qryDoctoRecebido.Parameters.ParamByName('cChaveNFe').Value := cChaveNFeReceb;
  qryDoctoRecebido.Open;

  if not qryDoctoRecebido.IsEmpty then
  begin
      case MessageDlg('Documento fiscal j� foi recebido anteriormente, deseja visualizar ?',mtConfirmation,[mbYes,mbNo],0) of
          mrYes: begin
                     PosicionaQuery(qryMaster, qryDoctoRecebidonCdRecebimento.AsString);
                     Abort;
                 end;
          mrNo:  begin
                     Abort;
                 end;
      end;
  end;

  try
      frmMenu.Connection.BeginTrans;

      SP_GERA_RECEBIMENTO_CD.Close;
      SP_GERA_RECEBIMENTO_CD.Parameters.ParamByName('@nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva;
      SP_GERA_RECEBIMENTO_CD.Parameters.ParamByName('@cChaveNFeReceb').Value := cChaveNFeReceb;
      SP_GERA_RECEBIMENTO_CD.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
      SP_GERA_RECEBIMENTO_CD.Parameters.ParamByName('@nCdRecebimento').Value := 0;
      SP_GERA_RECEBIMENTO_CD.ExecProc;

      frmMenu.Connection.CommitTrans;

      cCdRecebimento := IntToStr(SP_GERA_RECEBIMENTO_CD.Parameters.ParamByName('@nCdRecebimento').Value);

      PosicionaQuery(qryMaster,cCdRecebimento);
   except
      MensagemErro('Erro no processamento.');
      frmMenu.Connection.RollbackTrans;
      raise;
   end;
end;

procedure TfrmRecebimento.btnImpListaPreSelecaoClick(Sender: TObject);
var
  ObjRel : TrelItensSeparacaoDistribuicao_view;
begin
  if (qryMaster.IsEmpty) then
     Exit;

  try
      ObjRel := TrelItensSeparacaoDistribuicao_view.Create(nil);

      ObjRel.qryResultado.Close;
      ObjRel.qryResultado.Parameters.ParamByName('nCdRecebimento').Value := qryMasternCdRecebimento.Value;
      ObjRel.qryResultado.Open;

      ObjRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
      ObjRel.QuickRep1.PreviewModal;

      FreeAndNil(ObjRel);

  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end;
end;

procedure TfrmRecebimento.btnConferenciaClick(Sender: TObject);
var
  objConfProdPadrao : TfrmConferenciaProdutoPadrao;
begin

  if not (qryMaster.Active) then
      exit ;

  if (qryTipoRecebcFlgConfereItem.Value <> 1) then
  begin
      MensagemAlerta('Tipo de recebimento n�o exige confer�ncia.');
      Exit;
  end;

  if (qryMasternCdRecebimento.Value = 0) then
  begin
      MensagemAlerta('Salve o recebimento antes de iniciar a confer�ncia.') ;
      exit ;
  end ;

  if qryMasternCdTabStatusReceb.Value >= 3 then
  begin
      MensagemAlerta('Status n�o permite confer�ncia.');
      Abort;
  end;

  if (qryItemRecebimento.State in [dsInsert,dsEdit]) then
     qryItemRecebimento.Post;

  if (qryItemRecebimento.RecordCount = 0) then
      Exit;

  {-- Verifica se a confer�ncia j� foi realizada. --}
  PosicionaQuery(qryVerificaItemRecebDivergencia,qryMasternCdRecebimento.AsString);

  if qryVerificaItemRecebDivergencia.IsEmpty then
  begin
      case MessageDlg('Os itens j� foram conferidos. Deseja continuar?',mtConfirmation,[mbYes,mbNo],0) of
            mrNo: exit;
      end ;
  end;

  try
      try
          objConfProdPadrao := TfrmConferenciaProdutoPadrao.Create(nil);
          frmMenu.mensagemUsuario('Buscando produtos para conf...');

          { -- popula dataset dos produtos -- }
          PosicionaQuery(qryPopulaConfPadrao, qryMasternCdRecebimento.AsString);

          if (qryPopulaConfPadrao.IsEmpty) then
          begin
              MensagemAlerta('N�o existe produtos para confer�ncia.');
              Exit;
          end;

          qryPopulaConfPadrao.First;
          qryPopulaConfPadrao.DisableControls;

          while (not qryPopulaConfPadrao.Eof) do
          begin
              objConfProdPadrao.cdsProdutosConfere.Insert;
              objConfProdPadrao.cdsProdutosConferecCdProduto.Value := qryPopulaConfPadraonCdProduto.AsString;
              objConfProdPadrao.cdsProdutosConferecNmProduto.Value := qryPopulaConfPadraocNmProduto.Value;
              objConfProdPadrao.cdsProdutosConferecEAN.Value       := Trim(qryPopulaConfPadraocEAN.Value);
              objConfProdPadrao.cdsProdutosConferecEANFornec.Value := Trim(qryPopulaConfPadraocEANFornec.Value);
              objConfProdPadrao.cdsProdutosConfere.Post;

              qryPopulaConfPadrao.Next;
          end;

          qryPopulaConfPadrao.First;
          qryPopulaConfPadrao.EnableControls;

          if (objConfProdPadrao.cdsProdutosConfere.IsEmpty) then
          begin
              MensagemErro('Falha ao popular produtos para confer�ncia.');
              Exit;
          end;

          { -- configura tela de confer�ncia padr�o -- }
          objConfProdPadrao.Caption               := 'Confer�ncia de Produtos';
          objConfProdPadrao.rbConferencia.Checked := True;

          showForm(objConfProdPadrao,False);

          if (objConfProdPadrao.bProcessa) then
              case MessageDlg('Confirma a grava��o da confer�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
                  mrNo:Exit;
              end
          else
              Exit;

          try
              frmMenu.Connection.BeginTrans;

              { -- processa confer�ncia dos itens -- }
              qryGravaConfPadrao.Close;
              qryGravaConfPadrao.Parameters.ParamByName('nCdRecebimento').Value := qryMasternCdRecebimento.Value;
              qryGravaConfPadrao.ExecSQL;

              frmMenu.Connection.CommitTrans;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.');
              Raise;
          end;
      except
          MensagemErro('Erro no processamento.');
          Raise;
      end;

      PosicionaQuery(qryVerificaItemRecebDivergencia,qryMasternCdRecebimento.AsString);

      if (not qryVerificaItemRecebDivergencia.IsEmpty) then
          MensagemAlerta('Existem itens com saldo divergente.');
  finally
      FreeAndNil(objConfProdPadrao);
      frmMenu.mensagemUsuario('');
  end;

  case MessageDlg('Deseja visualizar os itens conferidos?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit;
  end;

  VisulizaItensConferidos;
end;

procedure TfrmRecebimento.VisulizaItensConferidos();
var
  ObjForm  : TfrmRecebimento_Conferencia;
begin
  {-- Exibe os itens conferidos --}
  try
      ObjForm := TfrmRecebimento_Conferencia.Create(nil);
      ObjForm.PosicionaQuery(ObjForm.qryProdutoGrade,qryMasternCdRecebimento.AsString);
      ObjForm.PosicionaQuery(ObjForm.qryProdutoGrade,qryMasternCdRecebimento.AsString);

      if ObjForm.qryProdutoGrade.IsEmpty then
          ObjForm.cxTabSheet1.Enabled := False;

      if ObjForm.qryProdutoSemGrade.IsEmpty then
          ObjForm.cxTabSheet2.Enabled := False;

      showForm(ObjForm,true);
  except
      MensagemErro('Erro no processamento.');
      raise;
  end;

end;

procedure TfrmRecebimento.btnVisualizaConfeClick(Sender: TObject);
var
  ObjForm : TfrmRecebimento_Conferencia;
begin

  if (not qryMaster.Active) then
      Abort;

  if qryMasternCdRecebimento.Value = 0 then
      Abort;

  if (qryTipoRecebcFlgConfereItem.Value <> 1) then
  begin
      MensagemAlerta('Tipo de recebimento n�o exige confer�ncia.');
      Exit;
  end;

  if (qryItemRecebimento.State in [dsInsert,dsEdit]) then
     qryItemRecebimento.Post;

  if (qryItemRecebimento.RecordCount = 0) then
      Exit;

  {-- Exibe os itens conferidos --}
  try
      ObjForm := TfrmRecebimento_Conferencia.Create(nil);
      ObjForm.PosicionaQuery(ObjForm.qryProdutoGrade,qryMasternCdRecebimento.AsString);
      ObjForm.PosicionaQuery(ObjForm.qryProdutoGrade,qryMasternCdRecebimento.AsString);

      if ObjForm.qryProdutoGrade.IsEmpty then
          ObjForm.cxTabSheet1.Enabled := False;

      if ObjForm.qryProdutoSemGrade.IsEmpty then
          ObjForm.cxTabSheet2.Enabled := False;

      showForm(ObjForm,true);
  except
      MensagemErro('Erro no processamento.');
      raise;
  end;

end;

procedure TfrmRecebimento.tabItemSemPedidoShow(Sender: TObject);
begin
  inherited;

  qryItemRecebidoSemPedido.Close;
  qryTipoPedido.Close;

  if not(qryMaster.IsEmpty) then
      PosicionaQuery( qryItemRecebidoSemPedido , qryMasternCdRecebimento.AsString ) ;
      
end;

procedure TfrmRecebimento.DBGridEh5KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh5.Columns[4].FieldName = 'nCdTipoPedidoRecebido') then
        begin
            if (qryItemRecebidoSemPedido.State = dsBrowse) then
                qryItemRecebidoSemPedido.Edit ;

            if ((qryItemRecebidoSemPedido.State = dsInsert) or (qryItemRecebidoSemPedido.State = dsEdit)) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(102,'TipoPedido.nCdTabTipoPedido = 2');

                If (nPK > 0) then
                    qryItemRecebidoSemPedidonCdTipoPedidoRecebido.Value := nPK;

            end ;

        end;

    end ;

  end ;
  
end;

procedure TfrmRecebimento.qryItemRecebidoSemPedidonCdTipoPedidoRecebidoChange(
  Sender: TField);
begin
  inherited;

  {qryTipoPedido.Close;

  if (qryItemRecebidoSemPedidonCdTipoPedidoRecebido.Value > 0) then
  begin

      PosicionaQuery( qryTipoPedido, qryItemRecebidoSemPedidonCdTipoPedidoRecebido.AsString );

      if not (qryTipoPedido.IsEmpty) then
          qryItemRecebidoSemPedidocNmTipoPedido.Value := qryTipoPedidocNmTipoPedido.Value;
  end;}
end;

procedure TfrmRecebimento.qryItemRecebidoSemPedidoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryTipoPedido.Close;

  if (qryItemRecebidoSemPedidonCdTipoPedidoRecebido.Value > 0) then // AND (qryItemRecebidoSemPedidocNmTipoPedido.Value = '') then
  begin

      PosicionaQuery( qryTipoPedido, qryItemRecebidoSemPedidonCdTipoPedidoRecebido.AsString );

      if not (qryTipoPedido.IsEmpty) then
          qryItemRecebidoSemPedidocNmTipoPedido.Value := qryTipoPedidocNmTipoPedido.Value; 
  end;
end;

procedure TfrmRecebimento.btAplicaTipoPedidoClick(Sender: TObject);
var
  nCodigo : String;
begin
  inherited;

  if (qryItemRecebidoSemPedido.IsEmpty) then
      Exit;

  if(Trim(ER2LookupMaskEdit1.Text) <> '') AND (DBEdit21.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de pedido a ser aplicado para todos os itens sem pedido.');
      ER2LookupMaskEdit1.SetFocus;
      Abort;
  end;

  if(DBEdit21.Text = '') then
      nCodigo := 'NULL'
  else
      nCodigo := Trim(ER2LookupMaskEdit1.Text);

  try
      try
          frmMenu.Connection.BeginTrans;

          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('UPDATE ItemRecebimento                                                      ');
          qryAux.SQL.Add('   SET nCdTipoPedidoRecebido = ' + nCodigo                                   );
          qryAux.SQL.Add(' WHERE nCdRecebimento        = ' + qryItemRecebimentonCdRecebimento.AsString );
          qryAux.SQL.Add('   AND nCdItemPedido         IS NULL                                        ');
          qryAux.SQL.Add('   AND nCdItemRecebimentoPai IS NULL                                        ');
          qryAux.ExecSQL;

          frmMenu.Connection.CommitTrans;

          ShowMessage('Tipo de pedido para itens sem v�nculo ao pedido de compra alterados com sucesso.');
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      ER2LookupMaskEdit1.Text := '';
      qryTipoPedidoAux.Close;
      qryItemRecebidoSemPedido.Requery;
  end;
end;

procedure TfrmRecebimento.qryItemRecebidoSemPedidoBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.IsEmpty) then
      Exit;

  if ((qryItemRecebidoSemPedidonCdTipoPedidoRecebido.Value > 0) and (qryItemRecebidoSemPedidocNmTipoPedido.Value = '')) then
  begin
      MensagemAlerta('Informe o tipo de pedido de entrada.');
      DBGridEh5.SelectedField := qryItemRecebidoSemPedidonCdTipoPedidoRecebido;
      DBGridEh5.SetFocus;
      Abort;
  end;

  inherited;
end;

initialization
    RegisterClass(TfrmRecebimento) ;

end.
