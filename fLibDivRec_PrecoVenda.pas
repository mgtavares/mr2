unit fLibDivRec_PrecoVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit;

type
  TfrmLibDivRec_PrecoVenda = class(TfrmProcesso_Padrao)
    edtPrecoVenda: TcxCurrencyEdit;
    Label1: TLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    procedure FormShow(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure oVendaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLibDivRec_PrecoVenda: TfrmLibDivRec_PrecoVenda;

implementation

{$R *.dfm}

procedure TfrmLibDivRec_PrecoVenda.FormShow(Sender: TObject);
begin
  inherited;

  edtPrecoVenda.Value := 0 ;

end;

procedure TfrmLibDivRec_PrecoVenda.ToolButton2Click(Sender: TObject);
begin

  if (edtPrecoVenda.Value < 0) then
  begin
      MensagemAlerta('Valor de venda inv�lido.') ;
      edtPrecoVenda.SetFocus;
  end ;

  if (edtPrecoVenda.Value = 0) then
  begin
      case MessageDlg('O Valor de venda n�o foi informado, o valor de venda no cadastro de produtos n�o ser� alterado. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo: exit ;
      end ;
  end ;

  inherited;

end;

procedure TfrmLibDivRec_PrecoVenda.oVendaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = vk_Return) then
      ToolButton2.Click;
      
end;

end.
