unit fSelPortaEtiqueta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DBCtrls, DB, ADODB, Registry, Mask, cxLookAndFeelPainters,
  cxButtons;

type
  TfrmSelPortaEtiqueta = class(TfrmProcesso_Padrao)
    qryModImpETP: TADOQuery;
    qryModImpETPnCdModImpETP: TIntegerField;
    qryModImpETPcNmModImpETP: TStringField;
    qryModImpETPiQtdeEtiquetas: TIntegerField;
    qryModImpETPiAvancoEtiqueta: TIntegerField;
    qryModImpETPiModelo: TIntegerField;
    qryModImpETPiTemperatura: TIntegerField;
    qryModImpETPcFlgLimpaMemoria: TIntegerField;
    qryModImpETPcQuery: TMemoField;
    qryModImpETPiEspacoEntreEtiquetas: TIntegerField;
    dsModImpETP: TDataSource;
    GroupBox1: TGroupBox;
    DBLookupListBox1: TDBLookupListBox;
    GroupBox2: TGroupBox;
    Label1: TLabel;
    cmbPorta: TComboBox;
    qryConfigAmbiente: TADOQuery;
    qryConfigAmbientecPortaEtiqueta: TStringField;
    Label2: TLabel;
    edtQtdeCopias: TMaskEdit;
    cxButton1: TcxButton;
    qryConfigAmbientecNmComputador: TStringField;
    qryConfigAmbientecPortaMatricial: TStringField;
    qryConfigAmbientecFlgUsaECF: TIntegerField;
    qryConfigAmbientecModeloECF: TStringField;
    qryConfigAmbientecPortaECF: TStringField;
    qryConfigAmbientenCdTipoImpressoraPDV: TIntegerField;
    qryConfigAmbienteiViasECF: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    bImprimir : boolean;
  end;

var
  frmSelPortaEtiqueta: TfrmSelPortaEtiqueta;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmSelPortaEtiqueta }

procedure TfrmSelPortaEtiqueta.FormShow(Sender: TObject);
begin
  inherited;

  bImprimir := false ;
  
  qryConfigAmbiente.Close;
  qryConfigAmbiente.Parameters.ParamByName('cNmComputador').Value := frmMenu.cNomeComputador;
  qryConfigAmbiente.Open;

  edtQtdeCopias.Text := '1' ;

  cmbPorta.Clear;
  
  if (qryConfigAmbientecPortaEtiqueta.Value <> '') then
      cmbPorta.Items.Add(qryConfigAmbientecPortaEtiqueta.Value) ;

  if (qryConfigAmbientecPortaEtiqueta.Value <> 'LPT1') then
      cmbPorta.Items.Add('LPT1') ;

  if (qryConfigAmbientecPortaEtiqueta.Value <> 'LPT2') then
      cmbPorta.Items.Add('LPT2') ;

  if (qryConfigAmbientecPortaEtiqueta.Value <> 'LPT3') then
      cmbPorta.Items.Add('LPT3') ;

  if (qryConfigAmbientecPortaEtiqueta.Value <> 'COM1') then
      cmbPorta.Items.Add('COM1') ;

  if (qryConfigAmbientecPortaEtiqueta.Value <> 'COM2') then
      cmbPorta.Items.Add('COM2') ;

  if (qryConfigAmbientecPortaEtiqueta.Value <> 'COM3') then
      cmbPorta.Items.Add('COM3') ;

  cmbPorta.ItemIndex := 0 ;

  cmbPorta.SetFocus;

end;


procedure TfrmSelPortaEtiqueta.cxButton1Click(Sender: TObject);
begin
  inherited;

  if (trim(cmbPorta.Text) = '') then
  begin
      MensagemAlerta('Informe a porta de impress�o.');
      cmbPorta.SetFocus;
      abort;
  end ;

  if (trim(edtQtdeCopias.Text) = '') then
      edtQtdeCopias.Text := '1' ;

  if (strToInt(trim(edtQtdeCopias.Text)) <= 0) then
  begin
      MensagemAlerta('Informe a quantidade de c�pias.') ;
      edtQtdeCopias.SetFocus;
      exit ;
  end ;

  if (qryModImpETP.isEmpty) then
  begin
      MensagemAlerta('Nenhum modelo de etiqueta encontrado.') ;
      abort ;
  end ;

  bImprimir := True ;
  
  Close;

end;

end.
