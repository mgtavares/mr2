inherited frmLocalEstoque: TfrmLocalEstoque
  Left = 130
  Top = 98
  Caption = 'Local de Estoque'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 62
    Top = 40
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 13
    Top = 62
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'Local de Estoque'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 57
    Top = 86
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 79
    Top = 110
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 6
    Top = 134
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo de Estoque'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 60
    Top = 158
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit9
  end
  object Label7: TLabel [7]
    Left = 15
    Top = 182
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Local de Entrega'
    FocusControl = DBEdit11
  end
  object Label8: TLabel [8]
    Left = 16
    Top = 206
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro de Custo'
    FocusControl = DBEdit12
  end
  object DBEdit1: TDBEdit [10]
    Tag = 1
    Left = 104
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdLocalEstoque'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [11]
    Left = 104
    Top = 56
    Width = 369
    Height = 19
    DataField = 'cNmLocalEstoque'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [12]
    Left = 104
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 3
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [13]
    Left = 104
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 4
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [14]
    Left = 104
    Top = 128
    Width = 65
    Height = 19
    DataField = 'nCdGrupoEstoque'
    DataSource = dsMaster
    TabOrder = 5
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [15]
    Tag = 1
    Left = 176
    Top = 80
    Width = 369
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit7: TDBEdit [16]
    Tag = 1
    Left = 176
    Top = 104
    Width = 369
    Height = 19
    DataField = 'cNmLoja'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit8: TDBEdit [17]
    Tag = 1
    Left = 176
    Top = 128
    Width = 369
    Height = 19
    DataField = 'cNmGrupoEstoque'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit9: TDBEdit [18]
    Left = 104
    Top = 152
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 6
    OnExit = DBEdit9Exit
    OnKeyDown = DBEdit9KeyDown
  end
  object DBEdit10: TDBEdit [19]
    Tag = 1
    Left = 176
    Top = 152
    Width = 369
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = DataSource1
    TabOrder = 15
  end
  object DBCheckBox1: TDBCheckBox [20]
    Left = 104
    Top = 232
    Width = 241
    Height = 17
    Caption = 'Considerar como estoque dispon'#237'vel'
    DataField = 'cFlgEstoqueDisp'
    DataSource = dsMaster
    TabOrder = 9
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBEdit11: TDBEdit [21]
    Left = 104
    Top = 176
    Width = 808
    Height = 19
    DataField = 'cEnderecoLocalEntrega'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit12: TDBEdit [22]
    Left = 104
    Top = 200
    Width = 65
    Height = 19
    DataField = 'nCdCC'
    DataSource = dsMaster
    TabOrder = 8
    OnExit = DBEdit12Exit
    OnKeyDown = DBEdit12KeyDown
  end
  object DBEdit13: TDBEdit [23]
    Tag = 1
    Left = 176
    Top = 200
    Width = 369
    Height = 19
    DataField = 'cNmCC'
    DataSource = DataSource2
    TabOrder = 16
  end
  object DBCheckBox2: TDBCheckBox [24]
    Left = 520
    Top = 232
    Width = 161
    Height = 17
    Caption = 'Bloquear Movimenta'#231#245'es'
    DataField = 'cFlgEstoqueBloqueado'
    DataSource = dsMaster
    TabOrder = 11
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object cxPageControl1: TcxPageControl [25]
    Left = 16
    Top = 272
    Width = 705
    Height = 337
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 17
    ClientRectBottom = 333
    ClientRectLeft = 4
    ClientRectRight = 701
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Grupo de Produtos Permitidos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 697
        Height = 309
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsGrupoProdutoLocalEstoque
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdGrupoProdutoLocalEstoque'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdLocalEstoque'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdGrupoProduto'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmGrupoProduto'
            Footers = <>
            ReadOnly = True
            Width = 403
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Usu'#225'rios Permitidos Movimentar'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 697
        Height = 309
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuarioLocalEstoque
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh2KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdLocalEstoque'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Width = 398
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBCheckBox3: TDBCheckBox [26]
    Left = 352
    Top = 232
    Width = 161
    Height = 17
    Caption = 'Influenciar C'#225'lculo MRP'
    DataField = 'cFlgInfluenciaMRP'
    DataSource = dsMaster
    TabOrder = 10
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM LocalEstoque'
      'WHERE nCdLocalEstoque = :nPK')
    Left = 528
    Top = 272
    object qryMasternCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryMastercNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasternCdGrupoEstoque: TIntegerField
      FieldName = 'nCdGrupoEstoque'
    end
    object qryMastercNmEmpresa: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmEmpresa'
      LookupDataSet = qryEmpresa
      LookupKeyFields = 'nCdEmpresa'
      LookupResultField = 'cNmEmpresa'
      KeyFields = 'nCdEmpresa'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMastercNmLoja: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmLoja'
      LookupDataSet = qryLoja
      LookupKeyFields = 'nCdLoja'
      LookupResultField = 'cNmLoja'
      KeyFields = 'nCdLoja'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMastercNmGrupoEstoque: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmGrupoEstoque'
      LookupDataSet = qryGrupoEstoque
      LookupKeyFields = 'nCdGrupoEstoque'
      LookupResultField = 'cNmGrupoEstoque'
      KeyFields = 'nCdGrupoEstoque'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMastercFlgEstoqueDisp: TIntegerField
      FieldName = 'cFlgEstoqueDisp'
    end
    object qryMastercEnderecoLocalEntrega: TStringField
      FieldName = 'cEnderecoLocalEntrega'
      Size = 200
    end
    object qryMasternCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMastercFlgEstoqueBloqueado: TIntegerField
      FieldName = 'cFlgEstoqueBloqueado'
    end
    object qryMastercFlgInfluenciaMRP: TIntegerField
      FieldName = 'cFlgInfluenciaMRP'
    end
  end
  inherited dsMaster: TDataSource
    Top = 232
  end
  inherited qryID: TADOQuery
    Left = 600
    Top = 232
  end
  inherited usp_ProximoID: TADOStoredProc
    Top = 288
  end
  inherited qryStat: TADOQuery
    Top = 304
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Top = 344
  end
  inherited ImageList1: TImageList
    Top = 296
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      ',cNmEmpresa'
      'FROM Empresa')
    Left = 584
    Top = 392
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryGrupoEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdGrupoEstoque'
      ',cNmGrupoEstoque'
      'FROM GrupoEstoque')
    Left = 616
    Top = 392
    object qryGrupoEstoquenCdGrupoEstoque: TIntegerField
      FieldName = 'nCdGrupoEstoque'
    end
    object qryGrupoEstoquecNmGrupoEstoque: TStringField
      FieldName = 'cNmGrupoEstoque'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja')
    Left = 648
    Top = 392
    object qryLojanCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryGrupoProdutoLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryGrupoProdutoLocalEstoqueBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM GrupoProdutoLocalEstoque'
      'WHERE nCdLocalEstoque = :nPK')
    Left = 136
    Top = 376
    object qryGrupoProdutoLocalEstoquenCdGrupoProdutoLocalEstoque: TAutoIncField
      FieldName = 'nCdGrupoProdutoLocalEstoque'
    end
    object qryGrupoProdutoLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryGrupoProdutoLocalEstoquenCdGrupoProduto: TIntegerField
      DisplayLabel = 'Grupo de Produtos|C'#243'd'
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutoLocalEstoquecNmGrupoProduto: TStringField
      DisplayLabel = 'Grupo de Produtos|Descri'#231#227'o'
      FieldKind = fkLookup
      FieldName = 'cNmGrupoProduto'
      LookupDataSet = qryGrupoProduto
      LookupKeyFields = 'nCdGrupoProduto'
      LookupResultField = 'cNmGrupoProduto'
      KeyFields = 'nCdGrupoProduto'
      LookupCache = True
      Size = 50
      Lookup = True
    end
  end
  object dsGrupoProdutoLocalEstoque: TDataSource
    DataSet = qryGrupoProdutoLocalEstoque
    Left = 736
    Top = 416
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoProduto')
    Left = 784
    Top = 408
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
    object qryGrupoProdutocFlgExpPDV: TIntegerField
      FieldName = 'cFlgExpPDV'
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 648
    Top = 232
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 624
    Top = 368
  end
  object qryUsuarioLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioLocalEstoqueBeforePost
    OnCalcFields = qryUsuarioLocalEstoqueCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM UsuarioLocalEstoque'
      'WHERE nCdLocalEstoque = :nPK')
    Left = 168
    Top = 376
    object qryUsuarioLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryUsuarioLocalEstoquenCdUsuario: TIntegerField
      DisplayLabel = 'Usu'#225'rio|C'#243'd'
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioLocalEstoquecNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rio|Nome do Usu'#225'rio'
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 50
      Calculated = True
    end
    object qryUsuarioLocalEstoquenCdUsuarioLocalEstoque: TAutoIncField
      FieldName = 'nCdUsuarioLocalEstoque'
    end
  end
  object dsUsuarioLocalEstoque: TDataSource
    DataSet = qryUsuarioLocalEstoque
    Left = 904
    Top = 304
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 912
    Top = 352
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cNmCC'
      'FROM CentroCusto'
      'WHERE nCdCC = :nPK'
      'AND cFlgLanc = 1'
      'AND iNivel = 3')
    Left = 728
    Top = 72
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryCC
    Left = 592
    Top = 272
  end
end
