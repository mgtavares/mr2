unit fProvisaoPagto_TitulosAdiantamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxPC;

type
  TfrmProvisaoPagtoTituloAdiantamento = class(TfrmProcesso_Padrao)
    qryTitulosAdiantamento: TADOQuery;
    DataSource1: TDataSource;
    qryTitulosAdiantamentonCdTitulo: TIntegerField;
    qryTitulosAdiantamentonCdEmpresa: TIntegerField;
    qryTitulosAdiantamentonCdEspTit: TIntegerField;
    qryTitulosAdiantamentocNrTit: TStringField;
    qryTitulosAdiantamentoiParcela: TIntegerField;
    qryTitulosAdiantamentonCdTerceiro: TIntegerField;
    qryTitulosAdiantamentonCdCategFinanc: TIntegerField;
    qryTitulosAdiantamentonCdMoeda: TIntegerField;
    qryTitulosAdiantamentocSenso: TStringField;
    qryTitulosAdiantamentodDtEmissao: TDateTimeField;
    qryTitulosAdiantamentodDtReceb: TDateTimeField;
    qryTitulosAdiantamentodDtVenc: TDateTimeField;
    qryTitulosAdiantamentodDtLiq: TDateTimeField;
    qryTitulosAdiantamentodDtCad: TDateTimeField;
    qryTitulosAdiantamentodDtCancel: TDateTimeField;
    qryTitulosAdiantamentonValTit: TBCDField;
    qryTitulosAdiantamentonValLiq: TBCDField;
    qryTitulosAdiantamentonSaldoTit: TBCDField;
    qryTitulosAdiantamentonValJuro: TBCDField;
    qryTitulosAdiantamentonValDesconto: TBCDField;
    qryTitulosAdiantamentocObsTit: TMemoField;
    qryTitulosAdiantamentonCdSP: TIntegerField;
    qryTitulosAdiantamentonCdBancoPortador: TIntegerField;
    qryTitulosAdiantamentodDtRemPortador: TDateTimeField;
    qryTitulosAdiantamentodDtBloqTit: TDateTimeField;
    qryTitulosAdiantamentocObsBloqTit: TStringField;
    qryTitulosAdiantamentonCdUnidadeNegocio: TIntegerField;
    qryTitulosAdiantamentonCdCC: TIntegerField;
    qryTitulosAdiantamentodDtContab: TDateTimeField;
    qryTitulosAdiantamentonCdDoctoFiscal: TIntegerField;
    qryTitulosAdiantamentodDtCalcJuro: TDateTimeField;
    qryTitulosAdiantamentonCdRecebimento: TIntegerField;
    qryTitulosAdiantamentonCdCrediario: TIntegerField;
    qryTitulosAdiantamentonCdTransacaoCartao: TIntegerField;
    qryTitulosAdiantamentonCdLanctoFin: TIntegerField;
    qryTitulosAdiantamentocFlgIntegrado: TIntegerField;
    qryTitulosAdiantamentocFlgDocCobranca: TIntegerField;
    qryTitulosAdiantamentocNrNF: TStringField;
    qryTitulosAdiantamentonCdProvisaoTit: TIntegerField;
    qryTitulosAdiantamentocFlgDA: TIntegerField;
    qryTitulosAdiantamentonCdContaBancariaDA: TIntegerField;
    qryTitulosAdiantamentonCdTipoLanctoDA: TIntegerField;
    qryTitulosAdiantamentonCdLojaTit: TIntegerField;
    qryTitulosAdiantamentocFlgInclusaoManual: TIntegerField;
    qryTitulosAdiantamentodDtVencOriginal: TDateTimeField;
    qryTitulosAdiantamentocFlgCartaoConciliado: TIntegerField;
    qryTitulosAdiantamentonCdGrupoOperadoraCartao: TIntegerField;
    qryTitulosAdiantamentonCdOperadoraCartao: TIntegerField;
    qryTitulosAdiantamentonValTaxaOperadora: TBCDField;
    qryTitulosAdiantamentonCdLoteConciliacaoCartao: TIntegerField;
    qryTitulosAdiantamentodDtIntegracao: TDateTimeField;
    qryTitulosAdiantamentonCdParcContratoEmpImobiliario: TIntegerField;
    qryTitulosAdiantamentonValAbatimento: TBCDField;
    qryTitulosAdiantamentonValMulta: TBCDField;
    qryTitulosAdiantamentodDtNegativacao: TDateTimeField;
    qryTitulosAdiantamentonCdUsuarioNeg: TIntegerField;
    qryTitulosAdiantamentonCdItemListaCobrancaNeg: TIntegerField;
    qryTitulosAdiantamentonCdLojaNeg: TIntegerField;
    qryTitulosAdiantamentodDtReabNeg: TDateTimeField;
    qryTitulosAdiantamentocFlgCobradora: TIntegerField;
    qryTitulosAdiantamentonCdCobradora: TIntegerField;
    qryTitulosAdiantamentodDtRemCobradora: TDateTimeField;
    qryTitulosAdiantamentonCdUsuarioRemCobradora: TIntegerField;
    qryTitulosAdiantamentonCdItemListaCobRemCobradora: TIntegerField;
    qryTitulosAdiantamentocFlgReabManual: TIntegerField;
    qryTitulosAdiantamentonCdUsuarioReabManual: TIntegerField;
    qryTitulosAdiantamentonCdFormaPagtoTit: TIntegerField;
    qryTitulosAdiantamentonPercTaxaJurosDia: TBCDField;
    qryTitulosAdiantamentonPercTaxaMulta: TBCDField;
    qryTitulosAdiantamentoiDiaCarenciaJuros: TIntegerField;
    qryTitulosAdiantamentocFlgRenegociado: TIntegerField;
    qryTitulosAdiantamentocCodBarra: TStringField;
    qryTitulosAdiantamentonValEncargoCobradora: TBCDField;
    qryTitulosAdiantamentocFlgRetCobradoraManual: TIntegerField;
    qryTitulosAdiantamentonCdServidorOrigem: TIntegerField;
    qryTitulosAdiantamentodDtReplicacao: TDateTimeField;
    qryTitulosAdiantamentonValIndiceCorrecao: TBCDField;
    qryTitulosAdiantamentonValCorrecao: TBCDField;
    qryTitulosAdiantamentocFlgNegativadoManual: TIntegerField;
    qryTitulosAdiantamentocIDExternoTit: TStringField;
    qryTitulosAdiantamentonCdTipoAdiantamento: TIntegerField;
    qryTitulosAdiantamentocFlgAdiantamento: TIntegerField;
    qryTitulosAdiantamentonCdMTituloGerador: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cSenso: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1nCdSP: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLojaTit: TcxGridDBColumn;
    qryTitulosAdiantamentocNmEspTit: TStringField;
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProvisaoPagtoTituloAdiantamento: TfrmProvisaoPagtoTituloAdiantamento;

implementation

uses fTitulo;

{$R *.dfm}

procedure TfrmProvisaoPagtoTituloAdiantamento.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmTitulo ;
begin
  inherited;

  if (qryTitulosAdiantamento.Active) then
  begin

      objForm := TfrmTitulo.Create(nil) ;

      frmTitulo.PosicionaQuery(objForm.qryMaster,qryTitulosAdiantamentonCdTitulo.AsString);

      objForm.btIncluir.Visible   := False ;
      objForm.btSalvar.Visible    := False ;
      objForm.btConsultar.Visible := False ;
      objForm.btExcluir.Visible   := False ;
      objForm.ToolButton1.Visible := False ;
      objForm.btCancelar.Visible  := False ;

      showForm( objForm , TRUE ) ;

  end ;

end;

end.
