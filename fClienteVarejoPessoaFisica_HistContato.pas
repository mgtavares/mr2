unit fClienteVarejoPessoaFisica_HistContato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, cxLookAndFeelPainters, StdCtrls, cxButtons,
  Mask, GridsEh, DBGridEh, cxPC, cxControls, ImgList, ComCtrls, ToolWin,
  ExtCtrls, DB, ADODB, DBGridEhGrouping;

type
  TfrmClienteVarejoPessoaFisica_HistContato = class(TfrmProcesso_Padrao)
    qryContatos: TADOQuery;
    qryContatosnCdContatoCobranca: TAutoIncField;
    qryContatosdDtContato: TDateTimeField;
    qryContatoscHoraContato: TStringField;
    qryContatoscNmPessoaContato: TStringField;
    qryContatoscTelefoneContato: TStringField;
    qryContatoscNmTipoResultadoContato: TStringField;
    qryContatosnCdLoja: TStringField;
    qryContatoscNmUsuario: TStringField;
    qryContatoscTextoContato: TMemoField;
    qryContatosdDtReagendado: TDateTimeField;
    qryContatoscFlgCarta: TIntegerField;
    qryContatosnCdItemListaCobranca: TIntegerField;
    dsContatos: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh4: TDBGridEh;
    Label5: TLabel;
    edtDtIniContato: TMaskEdit;
    Label7: TLabel;
    edtDtFimContato: TMaskEdit;
    cxButton5: TcxButton;
    procedure cxButton5Click(Sender: TObject);
    procedure DBGridEh4DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmClienteVarejoPessoaFisica_HistContato: TfrmClienteVarejoPessoaFisica_HistContato;
  nCdTerceiroHisContato : integer;
implementation

uses fMenu, fCobranca_VisaoContato_Contato;

{$R *.dfm}

procedure TfrmClienteVarejoPessoaFisica_HistContato.cxButton5Click(
  Sender: TObject);
begin
  inherited;
  if (trim(edtDtFimContato.Text) = '/  /') then
      edtDtFimContato.Text := dateToStr(Date) ;

  if (trim(edtDtIniContato.Text) = '/  /') then
      edtDtIniContato.Text := dateToStr(Date-30) ;

  qryContatos.Close;
  qryContatos.Parameters.ParamByName('dDtInicial').Value := frmMenu.ConvData(edtDtIniContato.Text) ;
  qryContatos.Parameters.ParamByName('dDtFinal').Value   := frmMenu.ConvData(edtDtFimContato.Text) ;
  PosicionaQuery(qryContatos,IntToStr(nCdTerceiroHisContato));

  if qryContatos.Eof then
      ShowMessage('Nenhum contato registrado para o per�odo selecionado.') ;
end;

procedure TfrmClienteVarejoPessoaFisica_HistContato.DBGridEh4DblClick(
  Sender: TObject);
var
  objForm : TfrmCobranca_VisaoContato_Contato ;
begin
  inherited;
  if qryContatos.Active then
  begin

      objForm := TfrmCobranca_VisaoContato_Contato.Create( Self ) ;

      objForm.exibeContato(qryContatosnCdContatoCobranca.Value);

      freeAndNil( objForm ) ;

  end ;

end;


procedure TfrmClienteVarejoPessoaFisica_HistContato.FormShow(
  Sender: TObject);
begin
  inherited;
  qryContatos.Close;
  edtDtIniContato.Text := '';
  edtDtFimContato.Text := '';
  edtDtIniContato.SetFocus;
end;

end.
