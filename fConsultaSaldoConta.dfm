inherited frmConsultaSaldoConta: TfrmConsultaSaldoConta
  Left = 191
  Top = 185
  Caption = 'Consulta Saldo Conta'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    ButtonWidth = 89
    inherited ToolButton1: TToolButton
      Caption = 'Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 89
    end
    inherited ToolButton2: TToolButton
      Left = 97
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 433
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsContaBancaria
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nSaldoConta
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1cNmLoja: TcxGridDBColumn
        Caption = 'Loja'
        DataBinding.FieldName = 'cNmLoja'
      end
      object cxGrid1DBTableView1nCdContaBancaria: TcxGridDBColumn
        Caption = 'C'#243'd'
        DataBinding.FieldName = 'nCdContaBancaria'
      end
      object cxGrid1DBTableView1nCdConta: TcxGridDBColumn
        Caption = 'Conta'
        DataBinding.FieldName = 'nCdConta'
      end
      object cxGrid1DBTableView1nSaldoConta: TcxGridDBColumn
        Caption = 'Saldo Atual'
        DataBinding.FieldName = 'nSaldoConta'
        Width = 127
      end
      object cxGrid1DBTableView1dDtUltConciliacao: TcxGridDBColumn
        Caption = 'Data Concilia'#231#227'o'
        DataBinding.FieldName = 'dDtUltConciliacao'
        Width = 135
      end
      object cxGrid1DBTableView1cTipoConta: TcxGridDBColumn
        Caption = 'Tipo'
        DataBinding.FieldName = 'cTipoConta'
        Width = 82
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '      ,nCdContaBancaria'
      
        '      ,CASE WHEN (cFlgCaixa = 0 AND cFlgCofre = 0) THEN (Convert' +
        '(VARCHAR(5),nCdBanco) + '#39' - '#39' + Convert(VARCHAR(5),cAgencia) + '#39 +
        ' - '#39' + nCdConta + '#39' - '#39' + IsNull(cNmTitular,'#39' '#39'))'
      '            ELSE nCdConta'
      '       END nCdConta'
      '      ,nSaldoConta'
      '      ,dDtUltConciliacao'
      '      ,CASE WHEN  cFlgCofre = 1 THEN '#39'COFRE'#39
      '            WHEN cFlgCaixa = 1 AND cFlgCofre = 0 THEN '#39'CAIXA'#39
      '            ELSE '#39'BANCO'#39
      '       END cTipoConta'
      '  FROM ContaBancaria'
      '       LEFT JOIN Loja ON Loja.nCdLoja = ContaBancaria.nCdLoja'
      ' WHERE ContaBancaria.nCdEmpresa       = :nPK'
      '   AND ContaBancaria.nCdStatus        = 1'
      ' ORDER BY cNmLoja')
    Left = 240
    Top = 56
    object qryContaBancariacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      ReadOnly = True
      Size = 84
    end
    object qryContaBancarianSaldoConta: TBCDField
      FieldName = 'nSaldoConta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryContaBancariadDtUltConciliacao: TDateTimeField
      FieldName = 'dDtUltConciliacao'
    end
    object qryContaBancariacTipoConta: TStringField
      FieldName = 'cTipoConta'
      ReadOnly = True
      Size = 5
    end
  end
  object dsContaBancaria: TDataSource
    DataSet = qryContaBancaria
    Left = 272
    Top = 56
  end
end
