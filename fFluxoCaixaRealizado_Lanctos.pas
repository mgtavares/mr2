unit fFluxoCaixaRealizado_Lanctos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid;

type
  TfrmFluxoCaixaRealizado_Lanctos = class(TfrmProcesso_Padrao)
    qryLanctos: TADOQuery;
    dsLanctos: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryLanctosdDtLancto: TDateTimeField;
    qryLanctosnCdLojaPG: TStringField;
    qryLanctosnCdLojaTit: TStringField;
    qryLanctosnCdLanctoFin: TIntegerField;
    qryLanctoscNrDocto: TStringField;
    qryLanctoscHistorico: TStringField;
    qryLanctosnValor: TBCDField;
    qryLanctosiNrCheque: TIntegerField;
    qryLanctoscNmUsuario: TStringField;
    qryLanctosnCdTitulo: TIntegerField;
    qryLanctosnCdSP: TIntegerField;
    qryLanctosnCdProvisaoTit: TIntegerField;
    qryLanctoscNrNF: TStringField;
    qryLanctoscNmTerceiro: TStringField;
    qryLanctoscFlgManual: TStringField;
    qryLanctosnCdConta: TStringField;
    qryLanctoscNmCC: TStringField;
    cxGrid1DBTableView1dDtLancto: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLojaPG: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLojaTit: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLanctoFin: TcxGridDBColumn;
    cxGrid1DBTableView1cNrDocto: TcxGridDBColumn;
    cxGrid1DBTableView1cHistorico: TcxGridDBColumn;
    cxGrid1DBTableView1nValor: TcxGridDBColumn;
    cxGrid1DBTableView1iNrCheque: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1nCdSP: TcxGridDBColumn;
    cxGrid1DBTableView1nCdProvisaoTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgManual: TcxGridDBColumn;
    cxGrid1DBTableView1nCdConta: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCC: TcxGridDBColumn;
    qryLanctoscNmColunaGrupoTotalCC: TStringField;
    cxGrid1DBTableView1cNmColunaGrupoTotalCC: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFluxoCaixaRealizado_Lanctos: TfrmFluxoCaixaRealizado_Lanctos;

implementation

{$R *.dfm}

end.
