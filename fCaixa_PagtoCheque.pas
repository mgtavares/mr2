unit fCaixa_PagtoCheque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, GridsEh, DBGridEh, ImgList,
  ComCtrls, ToolWin, ExtCtrls, StdCtrls, Mask, cxControls, cxContainer,
  cxEdit, cxTextEdit, cxCurrencyEdit, cxLookAndFeelPainters, cxButtons,
  DBCtrls, jpeg, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmCaixa_PagtoCheque = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    edtLimiteCreditoTotal: TcxCurrencyEdit;
    Label33: TLabel;
    Label4: TLabel;
    edtParcelasAberto: TcxCurrencyEdit;
    Label6: TLabel;
    edtCreditoDisponivel: TcxCurrencyEdit;
    Label7: TLabel;
    edtSaldoLimite: TcxCurrencyEdit;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    GroupBox3: TGroupBox;
    listRestricoes: TListBox;
    edtValCompra: TcxCurrencyEdit;
    Label11: TLabel;
    btConsParcAberto: TcxButton;
    Timer1: TTimer;
    Image2: TImage;
    qryTemp_RestricaoVenda: TADOQuery;
    qryTemp_RestricaoVendanCdTemp_Pagto: TIntegerField;
    qryTemp_RestricaoVendanCdTabTipoRestricaoVenda: TIntegerField;
    qryTemp_RestricaoVendanCdUsuarioAutor: TIntegerField;
    qryTemp_RestricaoVendadDtAutor: TDateTimeField;
    qryTemp_RestricaoVendacJustificativa: TMemoField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceironValLimiteCred: TBCDField;
    qryTerceironValChequePendente: TBCDField;
    qryTerceironValChequeDevolvidoAberto: TBCDField;
    qryTerceironValParcelaAberto: TBCDField;
    qryTerceironValParcelaAtraso: TBCDField;
    qryTerceironValLimiteDisp: TBCDField;
    qryTerceironCdStatus: TIntegerField;
    qryTerceironCdTabTipoSituacao: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocRG: TStringField;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label12: TLabel;
    cmd: TADOCommand;
    qryPrepara_Temp: TADOQuery;
    imgAlerta: TImage;
    qryTemp_RestricaoVendacFlgLiberado: TIntegerField;
    qryAux: TADOQuery;
    dsTemp_Cheque: TDataSource;
    qryPrepara_Temp_Cheque: TADOQuery;
    qryPrepara_Temp_ChequenCdBanco: TIntegerField;
    qryPrepara_Temp_ChequecDigito: TStringField;
    qryPrepara_Temp_ChequeiNrCheque: TIntegerField;
    qryPrepara_Temp_ChequecCNPJCPF: TStringField;
    qryPrepara_Temp_ChequenValCheque: TBCDField;
    qryPrepara_Temp_ChequedDtDeposito: TDateTimeField;
    qryPrepara_Temp_ChequedDtLimite: TDateTimeField;
    qryPrepara_Temp_ChequeiAutorizacao: TIntegerField;
    qryPrepara_Temp_ChequenCdTemp_Pagto: TIntegerField;
    DBGridEh3: TDBGridEh;
    edtValChequeDevolvido: TcxCurrencyEdit;
    Label1: TLabel;
    btGerarCheque: TcxButton;
    qryPrepara_Temp_ChequenCdCheque: TAutoIncField;
    qryPrepara_Temp_ChequenCdAgencia: TStringField;
    qryPrepara_Temp_ChequenCdConta: TStringField;
    Label9: TLabel;
    edtCondPagto: TEdit;
    Label3: TLabel;
    Label14: TLabel;
    edtPercEntrCliente: TcxCurrencyEdit;
    edtPercEntrCondicao: TcxCurrencyEdit;
    qryTerceirodDtProxSPC: TDateTimeField;
    qryTerceirodDtNegativacao: TDateTimeField;
    qryTerceironPercEntrada: TBCDField;
    qryTerceirocFlgRestricao: TIntegerField;
    qryBloqueioCadastro: TADOQuery;
    qryBloqueioCadastronCdTabTipoRestricaoVenda: TIntegerField;
    qryBloqueioCadastrocNmTabTipoRestricaoVenda: TStringField;
    qryPrepara_Temp_ChequecFlgEntrada: TIntegerField;
    qryPrepara_Temp_ChequenValEntrada: TBCDField;
    ToolButton4: TToolButton;
    btCliente: TToolButton;
    procedure ToolButton1Click(Sender: TObject);
    procedure qryTemp_CrediarioBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure VerificaRestricoes(bRestricaoEntrada:boolean);
    procedure Timer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btConsParcAbertoClick(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure InseriRestricao(nCdTabTipoRestricaoVenda: integer);
    procedure FormCreate(Sender: TObject);
    procedure btGerarChequeClick(Sender: TObject);
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryPrepara_Temp_ChequeBeforePost(DataSet: TDataSet);
    procedure qryPrepara_Temp_ChequeAfterScroll(DataSet: TDataSet);
    procedure qryPrepara_Temp_ChequeAfterPost(DataSet: TDataSet);
    procedure btClienteClick(Sender: TObject);
    procedure btGerarChequeKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nValorEntradaMinima : double ;
    nCdTerceiro         : integer ;
    nCdTemp_Pagto       : integer ;
    iParcelaTotal       : integer ;
    cFlgLiqCrediario    : integer ;

  end;

var
  frmCaixa_PagtoCheque: TfrmCaixa_PagtoCheque;
  nValEntradaOld : double ;

implementation

uses fMenu, fCaixa_Justificativa, fPdvSupervisor, fCaixa_ParcelaAberto,
  fCaixa_Recebimento, fCaixa_LiberaRestricao, fCaixa_DadosCheque,
  fClienteVarejoPessoaFisica;

{$R *.dfm}

procedure TfrmCaixa_PagtoCheque.ToolButton1Click(Sender: TObject);
var
  nValTotalCheques         : double ;
  nValorFinanciar          : double ;
  objCaixa_LiberaRestricao : TfrmCaixa_LiberaRestricao ;
  objCaixa                 : TfrmCaixa_Recebimento ;
begin
  inherited;

  nValTotalCheques := 0 ;

  qryPrepara_Temp_Cheque.First ;

  while not qryPrepara_Temp_Cheque.Eof do
  begin
      if (qryPrepara_Temp_ChequenCdBanco.AsString = '') then
      begin
          MensagemErro('Informe o c�digo do banco.') ;
          exit ;
      end ;

      if (qryPrepara_Temp_ChequenCdConta.AsString = '') then
      begin
          MensagemErro('Informe o n�mero da conta.') ;
          exit ;
      end ;

      if (qryPrepara_Temp_ChequeiNrCheque.AsString = '') then
      begin
          MensagemErro('Informe o n�mero do cheque.') ;
          exit ;
      end ;

      nValTotalCheques := nValTotalCheques + qryPrepara_Temp_ChequenValCheque.Value ;
      qryPrepara_Temp_Cheque.Next ;
  end ;

  qryPrepara_Temp_Cheque.First ;

  nValorFinanciar  := frmMenu.TBRound(edtValCompra.Value,2) ;
  nValTotalCheques := frmMenu.TBRound(nValTotalCheques,2) ;

  if (nValTotalCheques <> nValorFinanciar) then
  begin
      MensagemAlerta('O Valor da soma dos cheques � diferente do valor da compra.') ;
      abort ;
  end ;

  if (listRestricoes.Items.Count > 1) then
  begin

      if (MessageDlg('O cliente tem restri��es para esta venda. Deseja continuar mesmo assim ?',mtConfirmation,[mbYes,mbNo],0) = MRYES) then
      begin

            if (frmMenu.LeParametro('JUSTIFVDAREST') = 'S') then
            begin

                objCaixa_LiberaRestricao := TfrmCaixa_LiberaRestricao.Create( Self ) ;
                objCaixa_LiberaRestricao.nCdTemp_Pagto   := nCdTemp_Pagto ;

                {-- solicita autoriza��o para liberar as diverg�ncias --}
                objCaixa := TfrmCaixa_Recebimento.Create(Self) ;

                try

                    if not objCaixa.AutorizacaoGerente(33) then
                    begin
                        MensagemAlerta('Autentica��o falhou.') ;
                        abort ;
                    end ;

                    {-- armazena o c�digo do usu�rio que autorizou a libera��o das diverg�ncias --}
                    objCaixa_LiberaRestricao.nCdUsuarioAutor := objCaixa.nCdUsuarioSupervisor;
                    
                finally

                    freeAndNil( objCaixa ) ;

                end ;

                // Atualiza lista de permiss�es
                qryAux.Close ;
                qryAux.SQL.Clear ;
                qryAux.SQL.Add('UPDATE #Temp_RestricaoVenda                                                                                      ');
                qryAux.SQL.Add('   SET cFlgGerenteLibera = (SELECT cFlgGerenteLibera                                                             ');
                qryAux.SQL.Add('                              FROM TabTipoRestricaoVenda Tipo                                                    ');
                qryAux.SQL.Add('                             WHERE Tipo.nCdTabTipoRestricaoVenda = #Temp_RestricaoVenda.nCdTabTipoRestricaoVenda)');
                qryAux.ExecSQL;

                if not (objCaixa_LiberaRestricao.LiberaRestricoes()) then
                begin
                    MensagemErro('As restri��es n�o foram liberadas.') ;
                    abort ;
                end
                else ShowMessage('Restri��es liberadas com sucesso.') ;

            end ;

      end
      else abort ;

  end ;

  close ;
  
end;

procedure TfrmCaixa_PagtoCheque.qryTemp_CrediarioBeforePost(
  DataSet: TDataSet);
begin

  qryPrepara_Temp_ChequedDtDeposito.Value := StrToDate(DateToStr(qryPrepara_Temp_ChequedDtDeposito.Value)) ;

  if (qryPrepara_Temp_ChequedDtDeposito.Value > qryPrepara_Temp_ChequedDtLimite.Value) then
  begin
      MensagemAlerta('Data de dep�sito maior que a data limite.') ;
      abort ;
  end ;

  if (qryPrepara_Temp_ChequedDtDeposito.Value < Date) then
  begin
      MensagemAlerta('Data de dep�sito menor que hoje.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmCaixa_PagtoCheque.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  case key of
      vk_F4: ToolButton1.Click;
      vk_F9: btCliente.Click;
  end ;

end;

procedure TfrmCaixa_PagtoCheque.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      vk_F4: ToolButton1.Click;
      vk_F9: btCliente.Click;
  end ;

end;

procedure TfrmCaixa_PagtoCheque.FormShow(Sender: TObject);
begin
  inherited;

  imgAlerta.Visible := False ;

  qryPrepara_Temp_Cheque.EnableControls;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro,intToStr(nCdTerceiro)) ;

  edtPercEntrCliente.Value    := qryTerceironPercEntrada.Value ;
  edtLimiteCreditoTotal.Value := qryTerceironValLimiteCred.Value ;
  edtParcelasAberto.Value     := qryTerceironValParcelaAberto.Value + qryTerceironValChequePendente.Value + qryTerceironValChequeDevolvidoAberto.Value;
  edtCreditoDisponivel.Value  := qryTerceironValLimiteDisp.Value ;
  edtValChequeDevolvido.Value := qryTerceironValChequeDevolvidoAberto.Value ;

  VerificaRestricoes(false);

  Label3.Visible              := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;
  edtPercEntrCliente.Visible  := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;

  Label14.Visible             := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;
  edtPercEntrCondicao.Visible := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;

  edtPercEntrCliente.StyleDisabled.Color     := clBtnFace ;
  edtPercentrCliente.StyleDisabled.TextColor := clTeal ;

  edtPercEntrCondicao.StyleDisabled.Color     := clBtnFace ;
  edtPercentrCondicao.StyleDisabled.TextColor := clTeal ;

  if (edtPercEntrCliente.Value >  edtPercEntrCondicao.Value) then
  begin
      edtPercEntrCliente.StyleDisabled.Color     := clYellow ;
      edtPercentrCliente.StyleDisabled.TextColor := clBlack ;
  end ;

  if (edtPercEntrCliente.Value <= edtPercEntrCondicao.Value) then
  begin
      edtPercEntrCondicao.StyleDisabled.Color     := clYellow ;
      edtPercEntrCondicao.StyleDisabled.TextColor := clBlack ;
  end ;

  btGerarCheque.SetFocus;

end;

procedure TfrmCaixa_PagtoCheque.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmCaixa_PagtoCheque.VerificaRestricoes(bRestricaoEntrada:boolean);
var
    iRestricoes : integer ;
begin

  {-- Exclui as restri��es n�o liberadas --}
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('DELETE #Temp_RestricaoVenda WHERE nCdTemp_Pagto = 999999 AND cFlgLiberado = 0') ;
  qryAux.ExecSQL;

  listRestricoes.Clear;
  iRestricoes := 0 ;

  edtSaldoLimite.Value := edtCreditoDisponivel.Value - edtValCompra.Value ;

  if (bRestricaoEntrada) then
  begin
      listRestricoes.Items.Add('-> ENTRARA MENOR EXIGIDO') ;
      InseriRestricao(9);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (edtSaldoLimite.Value < 0) and (cFlgLiqCrediario = 0) then
  begin
      listRestricoes.Items.Add('-> LIMITE DE CR�DITO EXCEDIDO') ;
      InseriRestricao(1);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceironValParcelaAtraso.Value > 0) and (cFlgLiqCrediario = 0) then
  begin
      listRestricoes.Items.Add('-> PARCELA EM ATRASO') ;
      InseriRestricao(2);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceironValChequeDevolvidoAberto.Value > 0) then
  begin
      listRestricoes.Items.Add('-> CHEQUE DEVOLVIDO PENDENTE') ;
      InseriRestricao(7);
      iRestricoes := iRestricoes +1 ;
  end ;

  Application.ProcessMessages;

  if (qryTerceironCdTabTipoSituacao.Value <> 2) and (qryTerceironCdTabTipoSituacao.Value < 4) then
  begin
      listRestricoes.Items.Add('-> CADASTRO PENDENTE DE APROVA��O') ;
      InseriRestricao(3);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceironCdTabTipoSituacao.Value = 4) and (cFlgLiqCrediario = 0) then
  begin
      listRestricoes.Items.Add('-> CADASTRO BLOQUEADO') ;
      InseriRestricao(4);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceirocFlgRestricao.Value = 1) and (cFlgLiqCrediario = 0) then
  begin
      listRestricoes.Items.Add('-> RESTRI��O SPC') ;
      InseriRestricao(5);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceirodDtNegativacao.AsString <> '') and (cFlgLiqCrediario = 0) then
  begin
      listRestricoes.Items.Add('-> CLIENTE NEGATIVADO PELA EMPRESA') ;
      InseriRestricao(6);
      iRestricoes := iRestricoes +1 ;
  end ;

  if (qryTerceirodDtProxSPC.Value < Date) and (cFlgLiqCrediario = 0) then
  begin
      listRestricoes.Items.Add('-> CONSULTA SPC VENCIDA') ;
      InseriRestricao(8);
      iRestricoes := iRestricoes +1 ;
  end ;

  qryBloqueioCadastro.Close;
  PosicionaQuery(qryBloqueioCadastro, qryTerceironCdTerceiro.AsString) ;

  if not qryBloqueioCadastro.eof then
  begin
      qryBloqueioCadastro.First ;

      while not qryBloqueioCadastro.eof do
      begin
          listRestricoes.Items.Add('-> ' + Trim(qryBloqueioCadastrocNmTabTipoRestricaoVenda.Value)) ;
          InseriRestricao(qryBloqueioCadastronCdTabTipoRestricaoVenda.Value);
          iRestricoes := iRestricoes +1 ;

          qryBloqueioCadastro.Next ;
      end ;

  end ;

  qryBloqueioCadastro.Close ;

  imgAlerta.Visible    := False ;
  Timer1.Enabled       := False ;
  listRestricoes.Color := clWhite ;

  if (iRestricoes = 0) then
  begin
      if (cFlgLiqCrediario = 0) then
          listRestricoes.Items.Add('Nenhuma restri��o para esta venda.')
      else listRestricoes.Items.Add('Nenhuma restri��o para este recebimento.') ;
  end
  else begin
      listRestricoes.Items.Add('Total de Restri��es : ' + intToStr(iRestricoes)) ;
      Timer1.Enabled := True ;
  end ;

end;

procedure TfrmCaixa_PagtoCheque.Timer1Timer(Sender: TObject);
begin
  inherited;

  if (listRestricoes.Count > 0) then
  begin
      if (imgAlerta.Visible) then
          imgAlerta.Visible := false
      else imgAlerta.Visible := true ;
  end ;
  
end;

procedure TfrmCaixa_PagtoCheque.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  Timer1.Enabled := false ;
end;

procedure TfrmCaixa_PagtoCheque.btConsParcAbertoClick(Sender: TObject);
var
    objForm : TfrmCaixa_ParcelaAberto ;
begin
  inherited;

  if (edtParcelasAberto.Value > 0) then
  begin


      objForm := TfrmCaixa_ParcelaAberto.Create( Self ) ;
      PosicionaQuery(objForm.qryTituloAberto,intToStr(nCdTerceiro)) ;

      objForm.ShowModal ;

      freeAndNil( objForm ) ;

  end ;

  DBGridEh3.SetFocus;

end;

procedure TfrmCaixa_PagtoCheque.ToolButton2Click(Sender: TObject);
begin

  if (MessageDlg('Os cheques n�o ser�o registrados. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = MRNO) then
      exit ;

  qryPrepara_Temp_Cheque.DisableControls;

  qryPrepara_Temp_Cheque.First ;

  while not qryPrepara_Temp_Cheque.Eof do
  begin
      qryPrepara_Temp_Cheque.Delete ;
      qryPrepara_Temp_Cheque.First ;
  end ;

  qryPrepara_Temp_Cheque.EnableControls;

  Close ;

end;

procedure TfrmCaixa_PagtoCheque.InseriRestricao(
  nCdTabTipoRestricaoVenda: integer);
begin

    if not qryTemp_RestricaoVenda.Active then
        qryTemp_RestricaoVenda.Open;

    cmd.CommandText := 'INSERT INTO #Temp_RestricaoVenda (nCdTemp_Pagto, nCdTabTipoRestricaoVenda, cTemporario) VALUES(' + intToStr(nCdTemp_Pagto) + ',' + intToStr(nCdTabTipoRestricaoVenda) + ',' + Chr(39) + 'Liberar' + Chr(39) + ')' ;
    cmd.Execute;

    imgAlerta.Visible := True ;

end;

procedure TfrmCaixa_PagtoCheque.FormCreate(Sender: TObject);
begin
{  inherited;}

end;

procedure TfrmCaixa_PagtoCheque.btGerarChequeClick(Sender: TObject);
var
    iNumCheque           : integer ;
    objCaixa_DadosCheque : TfrmCaixa_DadosCheque ;
begin

  objCaixa_DadosCheque := TfrmCaixa_DadosCheque.Create( Self );

  objCaixa_DadosCheque.edtCodBanco.Text   := '' ;
  objCaixa_DadosCheque.edtCodAgencia.Text := '' ;
  objCaixa_DadosCheque.edtNumConta.Text   := '' ;
  objCaixa_DadosCheque.edtDV.Text         := '' ;
  objCaixa_DadosCheque.edtNumCheque.Text  := '' ;
  objCaixa_DadosCheque.edtCPFCNPJ.Text    := '' ;
  objCaixa_DadosCheque.ShowModal;

  if (objCaixa_DadosCheque.edtCodBanco.Text = '') then
    exit ;

  if (frmMenu.LeParametro('CHEQUETERCPDV') = 'N') then
  begin

      if (objCaixa_DadosCheque.edtCPFCNPJ.Text <> qryTerceirocCNPJCPF.Value) then
      begin
          MensagemErro('O CPF/CNPJ do emissor do cheque � diferente do CPF/CNPJ do cadastro do cliente.') ;
          abort ;
      end ;

  end ;

  {-- Atualiza os cheques deste pagamentos --}
  qryPrepara_Temp_Cheque.DisableControls;
  qryPrepara_Temp_Cheque.First;

  iNumCheque := frmMenu.ConvInteiro(objCaixa_DadosCheque.edtNumCheque.Text) ;

  while not qryPrepara_Temp_Cheque.Eof do
  begin
      qryPrepara_Temp_Cheque.Edit;
      qryPrepara_Temp_ChequenCdBanco.Value   := frmMenu.ConvInteiro(objCaixa_DadosCheque.edtCodBanco.Text) ;
      qryPrepara_Temp_ChequenCdAgencia.Value := objCaixa_DadosCheque.edtCodAgencia.Text ;
      qryPrepara_Temp_ChequenCdConta.Value   := objCaixa_DadosCheque.edtNumConta.Text ;
      qryPrepara_Temp_ChequecDigito.Value    := objCaixa_DadosCheque.edtDV.Text;
      qryPrepara_Temp_ChequeiNrCheque.Value  := iNumCheque ;
      qryPrepara_Temp_ChequecCNPJCPF.Value   := objCaixa_DadosCheque.edtCPFCNPJ.Text ;
      qryPrepara_Temp_Cheque.Post;

      qryPrepara_Temp_Cheque.Next;
      iNumCheque := iNumCheque + 1;
  end ;

  qryPrepara_Temp_Cheque.First;
  qryPrepara_Temp_Cheque.EnableControls;

  freeAndNil( objCaixa_DadosCheque ) ;

  DBGridEh3.Col := 5 ;
  DBGridEh3.SetFocus;

end;

procedure TfrmCaixa_PagtoCheque.DBGridEh3KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  case key of
      vk_F4: ToolButton1.Click;
      vk_F9: btCliente.Click;
  end ;

end;

procedure TfrmCaixa_PagtoCheque.qryPrepara_Temp_ChequeBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryPrepara_Temp_ChequenValCheque.Value <= 0) then
  begin
      MensagemErro('Valor do cheque inv�lido.') ;
      abort ;
  end ;

  if (qryPrepara_Temp_ChequedDtDeposito.Value > qryPrepara_Temp_ChequedDtLimite.Value) then
  begin
      MensagemErro('A data de dep�sito n�o pode ser maior que a data de limite.') ;
      abort ;
  end ;

end;

procedure TfrmCaixa_PagtoCheque.qryPrepara_Temp_ChequeAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  DBGridEh3.Columns[7].Title.Font.Color := clBlack ;
  DBGridEh3.Columns[7].ReadOnly         := False   ;
  DBGridEh3.Columns[7].Color            := clWhite ;
  DBGridEh3.Columns[7].Font.Color       := clBlue  ;

  if (qryPrepara_Temp_ChequecFlgEntrada.Value = 1) then
  begin
      DBGridEh3.Columns[7].Title.Font.Color := clRed     ;
      DBGridEh3.Columns[7].ReadOnly         := True      ;
      DBGridEh3.Columns[7].Color            := $00E6E6E6 ;
      DBGridEh3.Columns[7].Font.Color       := clBlack   ;
  end ;

end;

procedure TfrmCaixa_PagtoCheque.qryPrepara_Temp_ChequeAfterPost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryPrepara_Temp_ChequecFlgEntrada.Value = 1) then
  begin

      if (qryPrepara_Temp_ChequenValCheque.Value < qryPrepara_Temp_ChequenValEntrada.Value) then
          VerificaRestricoes(true)
      else VerificaRestricoes(false) ;

  end ;

end;

procedure TfrmCaixa_PagtoCheque.btClienteClick(Sender: TObject);
var
  objClientePessoaFisica : TfrmClienteVarejoPessoaFisica ;
begin
  inherited;

  objClientePessoaFisica := TfrmClienteVarejoPessoaFisica.Create( Self ) ;

  objClientePessoaFisica.PosicionaQuery(objClientePessoaFisica.qryMaster, intToStr(nCdTerceiro));
  objClientePessoaFisica.btIncluir.Visible   := False ;
  objClientePessoaFisica.btExcluir.Visible   := False ;
  objClientePessoaFisica.ToolButton1.Visible := False ;
  objClientePessoaFisica.btCancelar.Visible  := False ;
  objClientePessoaFisica.btConsultar.Visible := False ;
  objClientePessoaFisica.bChamadaExterna     := True ;
  objClientePessoaFisica.ShowModal;

  freeAndNil( objClientePessoaFisica ) ;

  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, intToStr(ncdTerceiro)) ;

  {-- na volta do cadastro do cliente revisa se ficaram restri��es --}
  
  edtLimiteCreditoTotal.Value := qryTerceironValLimiteCred.Value ;
  edtPercEntrCliente.Value    := qryTerceironPercEntrada.Value ;
  edtParcelasAberto.Value     := qryTerceironValParcelaAberto.Value + qryTerceironValChequePendente.Value + qryTerceironValChequeDevolvidoAberto.Value;
  edtCreditoDisponivel.Value  := qryTerceironValLimiteDisp.Value ;

  Label3.Visible              := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;
  edtPercEntrCliente.Visible  := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;

  Label14.Visible             := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;
  edtPercEntrCondicao.Visible := ((edtPercEntrCliente.Value > 0) or (edtPercEntrCondicao.Value > 0)) ;

  edtPercEntrCliente.StyleDisabled.Color     := clBtnFace ;
  edtPercentrCliente.StyleDisabled.TextColor := clTeal ;

  edtPercEntrCondicao.StyleDisabled.Color     := clBtnFace ;
  edtPercentrCondicao.StyleDisabled.TextColor := clTeal ;

  if (edtPercEntrCliente.Value >  edtPercEntrCondicao.Value) then
  begin
      edtPercEntrCliente.StyleDisabled.Color     := clYellow ;
      edtPercentrCliente.StyleDisabled.TextColor := clBlack ;
  end ;

  if (edtPercEntrCliente.Value <= edtPercEntrCondicao.Value) then
  begin
      edtPercEntrCondicao.StyleDisabled.Color     := clYellow ;
      edtPercEntrCondicao.StyleDisabled.TextColor := clBlack ;
  end ;
  
  VerificaRestricoes(false);

end;

procedure TfrmCaixa_PagtoCheque.btGerarChequeKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  case key of
      vk_F4: ToolButton1.Click;
      vk_F9: btCliente.Click;
  end ;
  
end;

end.
