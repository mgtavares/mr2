unit rMapaCompra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptMapaCompra = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand4: TQRBand;
    QRBand5: TQRBand;
    SPREL_MAPA_HEADER: TADOStoredProc;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText10: TQRDBText;
    QRShape2: TQRShape;
    QRShape4: TQRShape;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape9: TQRShape;
    QRShape10: TQRShape;
    QRLabel16: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    SPREL_MAPA_HEADERcNmFornecedor1: TStringField;
    SPREL_MAPA_HEADERcNmContato1: TStringField;
    SPREL_MAPA_HEADERcTelefone1: TStringField;
    SPREL_MAPA_HEADERcEmail1: TStringField;
    SPREL_MAPA_HEADERcNmFornecedor2: TStringField;
    SPREL_MAPA_HEADERcNmContato2: TStringField;
    SPREL_MAPA_HEADERcTelefone2: TStringField;
    SPREL_MAPA_HEADERcEmail2: TStringField;
    SPREL_MAPA_HEADERcNmFornecedor3: TStringField;
    SPREL_MAPA_HEADERcNmContato3: TStringField;
    SPREL_MAPA_HEADERcTelefone3: TStringField;
    SPREL_MAPA_HEADERcEmail3: TStringField;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRShape12: TQRShape;
    QRShape13: TQRShape;
    QRDBText26: TQRDBText;
    SPREL_MAPA_HEADERnCdMapaCompra: TIntegerField;
    SPREL_MAPA_HEADERcOBS: TMemoField;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRLabel24: TQRLabel;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRLabel25: TQRLabel;
    SPREL_MAPA_HEADERcNmMapaCompra: TStringField;
    SPREL_MAPA_HEADERdDtGeracao: TDateTimeField;
    QRDBText30: TQRDBText;
    QRLabel13: TQRLabel;
    QRLabel26: TQRLabel;
    QRDBText31: TQRDBText;
    QRLabel29: TQRLabel;
    QRDBText34: TQRDBText;
    QRDBText36: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText33: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText32: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText35: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText1: TQRDBText;
    QRLabel31: TQRLabel;
    QRDBText27: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText37: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText38: TQRDBText;
    QRDBText39: TQRDBText;
    QRDBText40: TQRDBText;
    usp_RelatorionCdItemMapaCompra: TIntegerField;
    usp_RelatorioiItem: TIntegerField;
    usp_RelatoriocCdProduto: TStringField;
    usp_RelatoriocNmItem: TStringField;
    usp_RelatorionQtde: TFloatField;
    usp_RelatorionCdFornVenc: TIntegerField;
    usp_RelatorionCdTipoRazaoEscolha: TIntegerField;
    usp_RelatorionValUnitarioFinal: TFloatField;
    usp_RelatorionValTotalFinal: TFloatField;
    usp_RelatorionValFinal_Forn1: TFloatField;
    usp_RelatorionValFinal_Forn2: TFloatField;
    usp_RelatorionValFinal_Forn3: TFloatField;
    usp_RelatoriocCondPagto_Forn1: TStringField;
    usp_RelatoriocCondPagto_Forn2: TStringField;
    usp_RelatoriocCondPagto_Forn3: TStringField;
    usp_RelatoriocPrazoEntrega_Forn1: TStringField;
    usp_RelatoriocPrazoEntrega_Forn2: TStringField;
    usp_RelatoriocPrazoEntrega_Forn3: TStringField;
    usp_RelatoriodValidade_Forn1: TDateTimeField;
    usp_RelatoriodValidade_Forn2: TDateTimeField;
    usp_RelatoriodValidade_Forn3: TDateTimeField;
    usp_RelatorionPercIPI_Forn1: TFloatField;
    usp_RelatorionPercIPI_Forn2: TFloatField;
    usp_RelatorionPercIPI_Forn3: TFloatField;
    usp_RelatorionPercICMSSub_Forn1: TFloatField;
    usp_RelatorionPercICMSSub_Forn2: TFloatField;
    usp_RelatorionPercICMSSub_Forn3: TFloatField;
    usp_RelatorionValFrete_Forn1: TFloatField;
    usp_RelatorionValFrete_Forn2: TFloatField;
    usp_RelatorionValFrete_Forn3: TFloatField;
    usp_RelatorionValProdutos_Forn1: TFloatField;
    usp_RelatorionValProdutos_Forn2: TFloatField;
    usp_RelatorionValProdutos_Forn3: TFloatField;
    usp_RelatorionValImposto_Forn1: TFloatField;
    usp_RelatorionValImposto_Forn2: TFloatField;
    usp_RelatorionValImposto_Forn3: TFloatField;
    usp_RelatorionValUnitario_Forn1: TFloatField;
    usp_RelatorionValUnitario_Forn2: TFloatField;
    usp_RelatorionValUnitario_Forn3: TFloatField;
    QRDBText41: TQRDBText;
    QRDBText42: TQRDBText;
    QRDBText43: TQRDBText;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRDBText44: TQRDBText;
    QRDBText45: TQRDBText;
    QRDBText46: TQRDBText;
    QRDBText47: TQRDBText;
    QRDBText48: TQRDBText;
    QRDBText49: TQRDBText;
    QRShape24: TQRShape;
    QRShape1: TQRShape;
    QRLabel5: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    usp_RelatorionCdItemRecebimento: TIntegerField;
    usp_RelatoriodDtUltReceb: TDateTimeField;
    usp_RelatorionUltValUnitario: TFloatField;
    usp_RelatorionValMelhorPreco: TFloatField;
    usp_RelatorionCdProduto: TIntegerField;
    QRDBText50: TQRDBText;
    QRDBText51: TQRDBText;
    QRDBText52: TQRDBText;
    SPREL_MAPA_HEADERdDtCancel: TDateTimeField;
    SPREL_MAPA_HEADERcStatusMapa: TStringField;
    QRDBText53: TQRDBText;
    QRShape3: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel14: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptMapaCompra: TrptMapaCompra;

implementation

{$R *.dfm}

end.
