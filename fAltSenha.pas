unit fAltSenha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, ExtCtrls, StdCtrls, cxButtons, DB, ADODB;

type
  TfrmAltSenha = class(TForm)
    btGravar: TcxButton;
    Label3: TLabel;
    Label1: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Image1: TImage;
    qryAltSenha: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure btGravarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAltSenha: TfrmAltSenha;

implementation

uses fMenu, fCadastro_Template;

{$R *.dfm}

procedure TfrmAltSenha.FormCreate(Sender: TObject);
var
    i : Integer;
begin

  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TEdit) then
    begin
      (Components [I] as TEdit).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      (Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 15 ;
      (Components [I] as TEdit).Font.Name   := 'Consolas' ;
      (Components [I] as TEdit).Font.Size   := 7 ;
      (Components [I] as TEdit).Font.Style  := [] ;

    end;

    if (Components [I] is TLabel) then
    begin
       (Components [I] as TLabel).Font.Name    := 'Tahoma' ;
       (Components [I] as TLabel).Font.Size    := 8 ;
       (Components [I] as TLabel).Font.Style   := [] ;
       (Components [I] as TLabel).Font.Color   := clWhite ;
       (Components [I] as TLabel).Transparent  := True ;
       (Components [I] as TLabel).BringToFront;
    end ;

    if (Components [I] is TButton) then
    begin
       (Components [I] as TButton).ParentFont := False ;
       (Components [I] as TButton).Font.Name  := 'Arial' ;
       (Components [I] as TButton).Font.Size  := 8 ;
       (Components [I] as TButton).Font.Color := clBlue ;
       (Components [I] as TButton).Default    := False ;
       (Components [I] as TButton).Update ;
    end ;

  end ;

end;

procedure TfrmAltSenha.btGravarClick(Sender: TObject);
begin

    If (Trim(Edit1.Text) = '') then
    begin
        frmMenu.MensagemAlerta('Informe a nova senha.') ;
        edit1.setFocus;
        exit ;
    end ;

    if (Edit1.Text <> Edit2.Text) then
    begin
        frmMenu.MensagemErro('Senha n�o confere!') ;
        Edit1.Text := '' ;
        Edit2.Text := '' ;
        edit1.setFocus;
        exit ;
    end ;

    frmMenu.Connection.BeginTrans ;

    try

        qryAltSenha.Close ;
        qryAltSenha.Parameters.ParamByName('cSenha').Value     := frmMenu.Cripto(Edit1.Text) ;
        qryAltSenha.Parameters.ParamByName('nCdUsuario').Value := frmMenu.qryValidaLoginnCdUsuario.Value ;
        qryAltSenha.ExecSQL;

    except
        frmMenu.Connection.RollbackTrans;
        frmMenu.MensagemErro('Erro no processamento.') ;
        raise ;
        exit ;
    end ;

    frmMenu.Connection.CommitTrans;

    frmMenu.ShowMessage('Senha alterada com sucesso.') ;

    close ;

end;

procedure TfrmAltSenha.FormShow(Sender: TObject);
begin
    Edit1.SetFocus ;
end;

procedure TfrmAltSenha.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = 13) then
        Edit2.SetFocus ;

end;

procedure TfrmAltSenha.Edit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if (Key = 13) then
        btGravar.SetFocus ;

end;

end.
