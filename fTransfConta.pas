unit fTransfConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask;

type
  TfrmTransfConta = class(TfrmCadastro_Padrao)
    qryMasternCdTransfConta: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasterdDtTransfConta: TDateTimeField;
    qryMasterdDtProcesso: TDateTimeField;
    qryMasternCdContaBancariaDeb: TIntegerField;
    qryMasternCdContaBancariaCred: TIntegerField;
    qryMasternValor: TBCDField;
    qryMasternCdUsuario: TIntegerField;
    qryMastercOBSTransfConta: TMemoField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBMemo1: TDBMemo;
    qryMasternCdLoja: TIntegerField;
    Label10: TLabel;
    DBEdit9: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit10: TDBEdit;
    DataSource1: TDataSource;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit11: TDBEdit;
    DataSource2: TDataSource;
    qryContaDebito: TADOQuery;
    qryContaDebitonCdContaBancaria: TIntegerField;
    qryContaDebitocNmConta: TStringField;
    DBEdit12: TDBEdit;
    DataSource3: TDataSource;
    qryContaCredito: TADOQuery;
    qryContaCreditonCdContaBancaria: TIntegerField;
    qryContaCreditocNmConta: TStringField;
    DBEdit13: TDBEdit;
    DataSource4: TDataSource;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DBEdit14: TDBEdit;
    DataSource5: TDataSource;
    Label12: TLabel;
    Label11: TLabel;
    qryContaDebitocFlgCofre: TIntegerField;
    qryContaDebitonCdUsuarioOperador: TIntegerField;
    qryContaCreditocFlgCofre: TIntegerField;
    qryContaCreditonCdUsuarioOperador: TIntegerField;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    SP_PROCESSA_TRANSFERENCIA_CONTA: TADOStoredProc;
    qryMasteriNrCheque: TIntegerField;
    qryMastercHistorico: TStringField;
    Label13: TLabel;
    DBEdit15: TDBEdit;
    Label14: TLabel;
    DBEdit16: TDBEdit;
    qryContaDebitocFlgEmiteCheque: TIntegerField;
    qryUsuarioContaBancaria: TADOQuery;
    qryUsuarioContaBancarianCdContaBancaria: TIntegerField;
    qryUsuarioContaBancarianCdUsuario: TIntegerField;
    qryConciliacao: TADOQuery;
    qryContaCreditocFlgCaixa: TIntegerField;
    qryContaDebitocFlgCaixa: TIntegerField;
    qryConciliacaodDtExtrato: TDateTimeField;
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit9Exit(Sender: TObject);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton10Click(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTransfConta: TfrmTransfConta;

implementation

uses fMenu, fLookup_Padrao, fConfereUsuario;

{$R *.dfm}

procedure TfrmTransfConta.btIncluirClick(Sender: TObject);
begin
  qryMaster.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;

  inherited;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;

  qryMasternCdUsuario.Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryUsuario, qryMasternCdUsuario.AsString) ;

  if (DBEdit9.Enabled) then
  begin
      qryMasternCdLoja.Value := frmMenu.nCdLojaAtiva;
      PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;
      DBEdit9.SetFocus;
  end
  else DBEdit5.SetFocus;

  qryMasterdDtTransfConta.Value := Date();

  btSalvar.Visible  := True ;
  btExcluir.Visible := True ;
  
end;

procedure TfrmTransfConta.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      DBEdit9.Enabled := False ;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  
  qryContaDebito.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryContaDebito.Parameters.ParamByName('nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
  qryContaCredito.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryContaCredito.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

end;

procedure TfrmTransfConta.DBEdit9Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, DBEdit9.Text) ;
  
end;

procedure TfrmTransfConta.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  qryContaDebito.Close ;
  qryContaDebito.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;
  PosicionaQuery(qryContaDebito, DBEdit5.Text) ;

end;

procedure TfrmTransfConta.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  qryContaCredito.Close ;
  qryContaCredito.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;
  PosicionaQuery(qryContaCredito, DBEdit6.Text) ;


end;

procedure TfrmTransfConta.DBEdit8Exit(Sender: TObject);
begin
  inherited;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, DBEdit8.Text) ;

end;

procedure TfrmTransfConta.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBEdit9.Enabled) and (DBEdit11.Text = '') then
  begin
      MensagemAlerta('Informe a loja.') ;
      DBEdit9.SetFocus ;
      abort ;
  end ;

  if (DBEdit12.Text = '') then
  begin
      MensagemAlerta('Informe a conta de d�bito.') ;
      DBEdit5.SetFocus ;
      abort ;
  end ;

  if (qryContaDebitocFlgEmiteCheque.Value = 0) and (qryMasteriNrCheque.Value > 0) then
  begin
      MensagemAlerta('A Conta de d�bito n�o est� configurada para emitir Cheques. Remova o n�mero do Cheque.') ;
      DBEdit15.SetFocus;
      abort ;
  end ;

  if (DBEdit13.Text = '') then
  begin
      MensagemAlerta('Informe a conta de cr�dito.') ;
      DBEdit6.SetFocus ;
      abort ;
  end ;

  if (trim(DBEdit3.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data da transfer�ncia.') ;
      DBEdit3.SetFocus ;
      abort ;
  end ;

  if (StrToDateTime(frmMenu.LeParametro('DTCONTAB')) >= qryMasterdDtTransfConta.Value) then
  begin
      MensagemAlerta('A data da transfer�ncia n�o pode ser inferior a data da contabiliza��o financeira.') ;
      Abort ;
  end ;

  if (qryMasternValor.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da transfer�ncia.') ;
      DBEdit7.SetFocus ;
      abort ;
  end ;

  if (qryContaDebitonCdContaBancaria.Value = qryContaCreditonCdCOntaBancaria.Value) then
  begin
      MensagemAlerta('A conta de cr�dito e d�bito devem ser diferentes.') ;
      abort ;
  end ;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  qryMasternCdUsuario.Value := frmMenu.nCdUsuarioLogado;

  inherited;

end;

procedure TfrmTransfConta.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TRANSFCONTA' ;
  nCdTabelaSistema  := 73 ;
  nCdConsultaPadrao := 168 ;
  bLimpaAposSalvar  := false ;

end;

procedure TfrmTransfConta.DBEdit9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfConta.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(169);

            If (nPK > 0) then
            begin
                qryMasternCdContaBancariaDeb.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfConta.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(169);

            If (nPK > 0) then
            begin
                qryMasternCdContaBancariaCred.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfConta.ToolButton10Click(Sender: TObject);
var
    nCdUsuarioDigitador : integer ;
begin
  inherited;

  if not qryMaster.Active then
  begin
      MensagemAlerta('Nenhuma transfer�ncia ativa.') ;
      exit ;
  end ;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

  if (qryMasterdDtProcesso.AsString <> '') then
  begin
      MensagemAlerta('Transfer�ncia j� processada.') ;
      exit ;
  end ;

  if (frmMenu.nCdUsuarioLogado <> qryMasternCdUsuario.Value) then
  begin
      MensagemAlerta('Somente o usu�rio que cadastrou a transfer�ncia pode processar.') ;
      exit ;
  end ;

  {-- verifica conta d�bito--}

  qryUsuarioContaBancaria.Close;
  qryUsuarioContaBancaria.Parameters.ParamByName('nCdContaBancaria').Value := qryMasternCdContaBancariaDeb.Value;
  qryUsuarioContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryUsuarioContaBancaria.Open;

  if (qryUsuarioContaBancaria.Eof) then
  begin
      case MessageDlg('Voc� n�o tem permiss�o para movimentar esta conta de d�bito.' +#13#13+ 'Deseja identificar outro usu�rio ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;
      nCdUsuarioDigitador := frmSenhaAcesso.Usuario() ;

      if (nCdUsuarioDigitador = -1) then
      begin
          MensagemAlerta('Autentica��o falhou.') ;
          exit ;
      end ;

      qryUsuarioContaBancaria.Close;
      qryUsuarioContaBancaria.Parameters.ParamByName('nCdContaBancaria').Value := qryMasternCdContaBancariaDeb.Value;
      qryUsuarioContaBancaria.Parameters.ParamByName('nCdUsuario').Value := nCdUsuarioDigitador;
      qryUsuarioContaBancaria.Open;

      if (qryUsuarioContaBancaria.Eof) then
      begin
          MensagemAlerta('Usu�rio Identificado n�o tem permiss�o para movimentar esta conta de d�bito.') ;
          exit ;
      end ;
  end;

  {-- verifica conta cr�dito--}

  qryUsuarioContaBancaria.Close;
  qryUsuarioContaBancaria.Parameters.ParamByName('nCdContaBancaria').Value := qryMasternCdContaBancariaCred.Value;
  qryUsuarioContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryUsuarioContaBancaria.Open;

  if (qryUsuarioContaBancaria.Eof) then
  begin
      case MessageDlg('Voc� n�o tem permiss�o para movimentar esta conta de cr�dito.' +#13#13+ 'Deseja identificar outro usu�rio ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;
      nCdUsuarioDigitador := frmSenhaAcesso.Usuario() ;

      if (nCdUsuarioDigitador = -1) then
      begin
          MensagemAlerta('Autentica��o falhou.') ;
          exit ;
      end ;

      qryUsuarioContaBancaria.Close;
      qryUsuarioContaBancaria.Parameters.ParamByName('nCdContaBancaria').Value := qryMasternCdContaBancariaCred.Value;
      qryUsuarioContaBancaria.Parameters.ParamByName('nCdUsuario').Value := nCdUsuarioDigitador;
      qryUsuarioContaBancaria.Open;

      if (qryUsuarioContaBancaria.Eof) then
      begin
          MensagemAlerta('Usu�rio Identificado n�o tem permiss�o para movimentar esta conta de Cr�dito.') ;
          exit ;
      end ;
  end;

  if ((qryContaDebitocFlgCaixa.Value = 0) and (qryContaCreditocFlgCaixa.Value = 0)) then
  begin
      qryConciliacao.Close;
      PosicionaQuery(qryConciliacao,qryContaDebitonCdContaBancaria.AsString);
      if (qryConciliacaodDtExtrato.Value >= qryMasterdDtTransfConta.Value) then
      begin
          MensagemAlerta('Data de transfer�ncia em per�odo j� conciliado na Conta Debito.');
          DBEdit3.SetFocus;
          exit;
      end;

      qryConciliacao.Close;
      PosicionaQuery(qryConciliacao,qryContaCreditonCdContaBancaria.AsString);
      if (qryConciliacaodDtExtrato.Value >= qryMasterdDtTransfConta.Value) then
      begin
          MensagemAlerta('Data de transfer�ncia em per�odo j� conciliado na Conta Cr�dito.');
          DBEdit3.SetFocus;
          exit;
      end;

  end
  else
  Begin
      if (qryMasterdDtTransfConta.Value < Date()) then
      begin
          MensagemAlerta('Tranfer�ncia com Conta Caixa somente na data do dia.');
          qryMasterdDtTransfConta.Value := Date();
          DBEdit3.SetFocus;
          exit;
      end;
  end;

  case MessageDlg('Confirma o processamento desta transfer�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_PROCESSA_TRANSFERENCIA_CONTA.Close ;
      SP_PROCESSA_TRANSFERENCIA_CONTA.Parameters.ParamByName('@nCdTransfConta').Value := qryMasternCdTransfConta.Value ;
      SP_PROCESSA_TRANSFERENCIA_CONTA.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  btCancelar.Click ;


end;

procedure TfrmTransfConta.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;

  qryEmpresa.Close ;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;

  qryContaDebito.Close ;
  qryContaDebito.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;
  PosicionaQuery(qryContaDebito, qryMasternCdContaBancariaDeb.AsString) ;

  qryContaCredito.Close ;
  qryContaCredito.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;
  PosicionaQuery(qryContaCredito, qryMasternCdContaBancariaCred.AsString) ;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuario.AsString) ;

  btSalvar.Visible  := True ;
  btExcluir.Visible := True ;

  if (qryMasterdDtProcesso.AsString <> '') then
  begin
      btSalvar.Visible  := False ;
      btExcluir.Visible := False ;
  end ;

end;

procedure TfrmTransfConta.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryLoja.Close ;
  qryContaDebito.Close ;
  qryContaCredito.Close ;
  qryUsuario.Close ;
  qryConciliacao.Close;

  btSalvar.Visible  := True ;
  btExcluir.Visible := True ;

end;

initialization
    RegisterClass(TfrmTransfConta) ;

end.

