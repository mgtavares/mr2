unit fProvisaoPagto_ContaBancaria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, StdCtrls, DBClient, Menus, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmProvisaoPagto_ContaBancaria = class(TfrmProcesso_Padrao)
    qryFormaPagto: TADOQuery;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    qryFormaPagtocFlgOK: TIntegerField;
    dsFormaPagto: TDataSource;
    DBGridEh1: TDBGridEh;
    Panel1: TPanel;
    Panel2: TPanel;
    DBGridEh2: TDBGridEh;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancariacAgencia: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancariacNmTitular: TStringField;
    qryContaBancariacFlgOK: TIntegerField;
    qryContaBancarianSaldoInicial: TBCDField;
    qryContaBancarianProvDebito: TBCDField;
    qryContaBancarianProvCredito: TBCDField;
    qryContaBancarianSaldoFinal: TBCDField;
    dsContaBancaria: TDataSource;
    qryProvisaoTit: TADOQuery;
    qryProvisaoTitnCdProvisaoTit: TAutoIncField;
    qryProvisaoTitnCdEmpresa: TIntegerField;
    qryProvisaoTitnCdContaBancaria: TIntegerField;
    qryProvisaoTitdDtPagto: TDateTimeField;
    qryProvisaoTitnCdFormaPagto: TIntegerField;
    qryProvisaoTitnCdUsuarioPrev: TIntegerField;
    qryProvisaoTitdDtPrev: TDateTimeField;
    qryProvisaoTitnCdUsuarioBaixa: TIntegerField;
    qryProvisaoTitdDtBaixa: TDateTimeField;
    qryProvisaoTitcFlgBaixado: TIntegerField;
    qryProvisaoTitdDtCancel: TDateTimeField;
    qryProvisaoTitnCdUsuarioCancel: TIntegerField;
    qryProvisaoTitnValProvisao: TBCDField;
    qryAux: TADOQuery;
    cdsTitulos: TClientDataSet;
    cdsTitulosnCdTitulo: TIntegerField;
    cmdPreparaTemp: TADOCommand;
    qryFormaPagtonCdTabTipoFormaPagto: TIntegerField;
    qryContaBancariacFlgCaixa: TIntegerField;
    PopupMenu1: TPopupMenu;
    SaldoProjetado: TMenuItem;
    qryContaBancariadDtUltConciliacao: TDateTimeField;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SaldoProjetadoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    dDtPagto : TDateTime ;
    nValProvisao : double ;
  end;

var
  frmProvisaoPagto_ContaBancaria: TfrmProvisaoPagto_ContaBancaria;

implementation

uses fMenu, fProvisaoPagto_Titulos, fProvisaoPagto_SaldoProjetado;

{$R *.dfm}

procedure TfrmProvisaoPagto_ContaBancaria.ToolButton1Click(
  Sender: TObject);
var
  iAux : integer ;
  nCdFormaPagto, nCdContaBancaria : integer ;
  cFlgCaixa, nCdTabTipoFormaPagto : integer ;
  nCdTerceiro : integer ;
  objFormPai  : TfrmProvisaoPagto_Titulos ;
begin
  inherited;

  if (qryFormaPagto.State <> dsBrowse) then
      qryFormaPagto.Post ;

  if (qryContaBancaria.State <> dsBrowse) then
      qryContaBancaria.Post ;
      
  qryFormaPagto.DisableControls;
  qryFormaPagto.First ;

  iAux                 := 0 ;
  cFlgCaixa            := 0 ;
  nCdTabTipoFormaPagto := 0 ;

  while not qryFormaPagto.Eof do
  begin
      if (qryFormaPagtocFlgOK.Value = 1) then
      begin
          iAux                 := iAux + 1 ;
          nCdFormaPagto        := qryFormaPagtonCdFormaPagto.Value ;
          nCdTabTipoFormaPagto := qryFormaPagtonCdTabTipoFormaPagto.Value ;
      end ;

      qryFormaPagto.Next ;
  end ;

  qryFormaPagto.First ;
  qryFormaPagto.EnableControls;

  if (iAux <> 1) then
  begin
      MensagemAlerta('Selecione uma forma de pagamento.') ;
      exit ;
  end ;

  qryContaBancaria.DisableControls;
  qryContaBancaria.First ;

  iAux := 0 ;

  while not qryContaBancaria.Eof do
  begin
      if (qryContaBancariacFlgOK.Value = 1) then
      begin
          iAux             := iAux + 1 ;
          nCdContaBancaria := qryContaBancarianCdContaBancaria.Value ;
          cFlgCaixa        := qryContaBancariacFlgCaixa.Value ;
      end ;

      qryContaBancaria.Next ;
  end ;

  qryContaBancaria.First ;
  qryContaBancaria.EnableControls;

  if (iAux <> 1) then
  begin
      MensagemAlerta('Selecione uma conta banc�ria.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a inclus�o desta provis�o ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  cdsTitulos.Close ;
  cdsTitulos.Open ;

  while not cdsTitulos.eof do
  begin
      cdsTitulos.Delete ;
      cdsTitulos.First ;
  end ;

  {-- cria um objeto para referenciar ao FORM pai e poder acessar os objetos desse FORM pai --}
  objFormPai := (Self.Owner as TfrmProvisaoPagto_Titulos) ;

  objFormPai.qryTemp.First ;
  
  nCdTerceiro := 0 ;

  while not objFormPai.qryTemp.eof do
  begin
      if (objFormPai.qryTempcFlgOK.Value = 1) then
      begin
          if (nCdTerceiro > 0) and (nCdTerceiro <> objFormPai.qryTempnCdTerceiro.Value) and (qryFormaPagtonCdTabTipoFormaPagto.Value = 11) then
          begin
              MensagemAlerta('Para provis�o de pagamento com cheque de terceiro n�o � permitido ' +#13#13 + ' selecionar ao mesmo tempo t�tulos de mais de um fornecedor.') ;
              close ;
              exit ;
          end ;

          nCdTerceiro := objFormPai.qryTempnCdTerceiro.Value ;

          cdsTitulos.Insert ;
          cdsTitulosnCdTitulo.Value := objFormPai.qryTempnCdTitulo.Value ;
          cdsTitulos.Post ;
      end ;
      objFormPai.qryTemp.Next ;
  end;

  objFormPai.qryTemp.First ;

  frmMenu.Connection.BeginTrans;

  try
      qryProvisaoTit.Close ;
      qryProvisaoTit.Open ;

      qryProvisaoTit.Insert ;
      qryProvisaoTitnCdProvisaoTit.Value   := frmMenu.fnProximoCodigo('PROVISAOTIT') ;
      qryProvisaoTitnCdEmpresa.Value       := frmMenu.nCdEmpresaAtiva;
      qryProvisaoTitnCdContaBancaria.Value := nCdContaBancaria ;
      qryProvisaoTitdDtPagto.Value         := dDtPagto         ;
      qryProvisaoTitnCdFormaPagto.Value    := nCdFormaPagto    ;
      qryProvisaoTitnCdUsuarioPrev.Value   := frmMenu.nCdUsuarioLogado;
      qryProvisaoTitdDtPrev.Value          := Now() ;
      qryProvisaoTitcFlgBaixado.Value      := 0 ;
      qryProvisaoTitnValProvisao.Value     := nValProvisao ;
      qryProvisaoTit.Post ;

      cdsTitulos.First ;

      while not cdsTitulos.eof do
      begin

          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('UPDATE Titulo Set nCdProvisaoTit = ' + qryProvisaoTitnCdProvisaoTit.AsString + ' WHERE nCdTitulo = ' + cdsTitulosnCdTitulo.AsString) ;
          qryAux.ExecSQL ;

          cdsTitulos.Next ;
      end;

      cdsTitulos.First ;

  except
      frmMenu.Connection.CommitTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  objFormPai := nil ;
  
  Close ;

end;

procedure TfrmProvisaoPagto_ContaBancaria.FormShow(Sender: TObject);
begin
  inherited;

  if (qryContaBancaria.RecordCount = 1) then
  begin
      qryContaBancaria.First ;
      qryContaBancaria.Edit ;
      qryContaBancariacFlgOK.Value := 1 ;
      qryContaBancaria.Post ;
  end ;

end;

procedure TfrmProvisaoPagto_ContaBancaria.SaldoProjetadoClick(
  Sender: TObject);
var
  objForm : TfrmProvisaoPagtoSaldoProjetado ;
begin
  inherited;

  objForm := TfrmProvisaoPagtoSaldoProjetado.Create(nil);

  objForm.uspConsulta.Close;
  objForm.uspConsulta.Parameters.ParamByName('@nCdEmpresaAtiva').Value := 1; //frmMenu.nCdEmpresaAtiva;
  objForm.uspConsulta.Parameters.ParamByName('@nCdContaBancaria').Value := qryContaBancarianCdContaBancaria.Value;
  objForm.uspConsulta.open;

  showForm( objForm , TRUE ) ;

end;

end.
