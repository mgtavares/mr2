unit fProvisaoPagto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, cxLookAndFeelPainters, cxButtons, DB, DBCtrls, ADODB, ER2Lookup;

type
  TfrmProvisaoPagto = class(TfrmProcesso_Padrao)
    edtDtPagto: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    edtDtVencIni: TMaskEdit;
    edtDtVencFim: TMaskEdit;
    Label3: TLabel;
    Label4: TLabel;
    edtNrSP: TMaskEdit;
    edtNrTit: TMaskEdit;
    Label5: TLabel;
    edtnCdTerceiro: TMaskEdit;
    Label6: TLabel;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    cxButton1: TcxButton;
    edtEmpresa: TER2LookupMaskEdit;
    procedure edtnCdTerceiroExit(Sender: TObject);
    procedure edtnCdTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProvisaoPagto: TfrmProvisaoPagto;

implementation

uses fLookup_Padrao, fProvisaoPagto_Titulos, fMenu;

{$R *.dfm}

procedure TfrmProvisaoPagto.edtnCdTerceiroExit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, edtnCdTerceiro.Text) ;
  
end;

procedure TfrmProvisaoPagto.edtnCdTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            edtnCdTerceiro.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmProvisaoPagto.cxButton1Click(Sender: TObject);
var
  objForm : TfrmProvisaoPagto_Titulos ;
begin
  inherited;

  if (DayOfWeek(edtDtPagto.Date) = 1) or (DayOfWeek(edtDtPagto.Date) = 7) then
  begin

      case MessageDlg('A data de pagamento selecionada n�o � um dia �til, continuar ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

  end ;

  objForm := TfrmProvisaoPagto_Titulos.Create(nil) ;

  objForm.qryPreparaTemp.Close ;
  objForm.qryPreparaTemp.ExecSQL;

  objForm.cmdPreparaTemp.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
  objForm.cmdPreparaTemp.Parameters.ParamByName('nCdSP').Value       := frmMenu.ConvInteiro(edtNrSP.Text);
  objForm.cmdPreparaTemp.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(edtnCdTerceiro.Text) ;
  objForm.cmdPreparaTemp.Parameters.ParamByName('dDtVencIni').Value  := frmMenu.ConvData(edtDtVencIni.Text) ;
  objForm.cmdPreparaTemp.Parameters.ParamByName('dDtVencFim').Value  := frmMenu.ConvData(edtDtVencFim.Text);
  objForm.cmdPreparaTemp.Parameters.ParamByName('dDtPagto').Value    := frmMenu.ConvData(DateToStr(edtDtPagto.Date)) ;
  objForm.cmdPreparaTemp.Parameters.ParamByName('cNrTit').Value      := Trim(edtNrTit.Text) ;
  objForm.cmdPreparaTemp.Execute;

  objForm.qryTemp.Close ;
  objForm.qryTemp.Open  ;

  objForm.edtTotalSelec.Value := 0 ;
  objForm.dDtPagto            := StrToDate(DateToStr(edtDtPagto.Date));

  if (edtDtPagto.Date < StrToDate(DateToStr(Now()))) then
  begin
      MensagemAlerta('A data de pagamento n�o pode ser menor que hoje.') ;
      exit ;
  end ;

  showForm( objForm , TRUE ) ;
  
end;

procedure TfrmProvisaoPagto.FormShow(Sender: TObject);
begin
  inherited;

  edtDtPagto.DateTime := Now() ;
  
end;

initialization
    RegisterClass(TfrmProvisaoPagto) ;
    
end.
