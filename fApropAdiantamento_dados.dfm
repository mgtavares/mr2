inherited frmApropAdiantamento_dados: TfrmApropAdiantamento_dados
  Left = 318
  Top = 256
  Width = 579
  Height = 366
  BorderIcons = [biSystemMenu]
  Caption = 'Liquida'#231#227'o do Adiantamento'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 563
    Height = 301
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 40
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor em Esp'#233'cie'
  end
  object Label5: TLabel [2]
    Left = 15
    Top = 64
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Banc'#225'ria'
  end
  inherited ToolBar1: TToolBar
    Width = 563
    TabOrder = 3
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtDinheiro: TcxCurrencyEdit [4]
    Left = 96
    Top = 32
    Width = 121
    Height = 21
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    TabOrder = 0
  end
  object MaskEdit6: TMaskEdit [5]
    Left = 96
    Top = 56
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit6Exit
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 168
    Top = 56
    Width = 385
    Height = 21
    DataField = 'nCdConta'
    DataSource = DataSource1
    TabOrder = 2
  end
  object StaticText2: TStaticText [7]
    Left = 8
    Top = 88
    Width = 545
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Rela'#231#227'o de Cheques'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 4
  end
  object DBGridEh3: TDBGridEh [8]
    Left = 8
    Top = 104
    Width = 545
    Height = 217
    DataGrouping.GroupLevels = <>
    DataSource = dsTemp_Cheque
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -11
    Font.Name = 'Arial'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clBlue
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = [fsBold]
    OptionsEh = [dghFixed3D, dghFrozen3D, dghFooter3D, dghData3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghDialogFind, dghColumnResize, dghColumnMove]
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clBlack
    TitleFont.Height = -11
    TitleFont.Name = 'Arial'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdBanco'
        Footers = <>
        Width = 37
      end
      item
        EditButtons = <>
        FieldName = 'nCdAgencia'
        Footers = <>
        Width = 47
      end
      item
        EditButtons = <>
        FieldName = 'nCdConta'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'cDigito'
        Footers = <>
        Width = 22
      end
      item
        EditButtons = <>
        FieldName = 'cCNPJCPF'
        Footers = <>
        Width = 91
      end
      item
        EditButtons = <>
        FieldName = 'iNrCheque'
        Footers = <>
        Width = 81
      end
      item
        EditButtons = <>
        FieldName = 'nValCheque'
        Footers = <>
        Width = 74
      end
      item
        EditButtons = <>
        FieldName = 'dDtDeposito'
        Footers = <>
        Width = 80
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdContaBancaria, (dbo.fn_ZeroEsquerda(nCdBanco,3) + '#39' - ' +
        #39' + nCdConta) nCdConta'
      '  FROM ContaBancaria'
      ' WHERE nCdEmpresa       = :nCdEmpresa'
      '   AND cFlgCaixa        = 0'
      '   AND cFlgCofre        = 0'
      '   AND nCdContaBancaria = :nPK')
    Left = 352
    Top = 160
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      ReadOnly = True
      Size = 33
    end
  end
  object DataSource1: TDataSource
    DataSet = qryContaBancaria
    Left = 256
    Top = 184
  end
  object qryPrepara_Temp_Cheque: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryPrepara_Temp_ChequeBeforePost
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..##Temp_Cheque'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE ##Temp_Cheque (nCdBanco     int'
      '                               ,nCdAgencia   int'
      '                               ,nCdConta     int'
      '                               ,cDigito      CHAR(1)'
      '                               ,iNrCheque    int'
      
        '                               ,nValCheque   DECIMAL(12,2) defau' +
        'lt 0'
      '                               ,cCNPJCPF     CHAR(14)'
      '                               ,dDtDeposito  DATETIME'
      
        '                               ,CONSTRAINT PK_Temp_Cheque PRIMAR' +
        'Y KEY (nCdBanco, nCdAgencia, nCdConta, iNrCheque))'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM ##Temp_Cheque'
      ' ORDER BY dDtDeposito')
    Left = 264
    Top = 240
    object qryPrepara_Temp_ChequenCdBanco: TIntegerField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Banco'
      FieldName = 'nCdBanco'
    end
    object qryPrepara_Temp_ChequenCdAgencia: TIntegerField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Ag'#234'ncia'
      FieldName = 'nCdAgencia'
    end
    object qryPrepara_Temp_ChequenCdConta: TIntegerField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Conta'
      FieldName = 'nCdConta'
    end
    object qryPrepara_Temp_ChequecDigito: TStringField
      DisplayLabel = 'Rela'#231#227'o de Cheques|DV'
      FieldName = 'cDigito'
      FixedChar = True
      Size = 1
    end
    object qryPrepara_Temp_ChequeiNrCheque: TIntegerField
      DisplayLabel = 'Rela'#231#227'o de Cheques|N'#250'm. Cheque'
      FieldName = 'iNrCheque'
    end
    object qryPrepara_Temp_ChequecCNPJCPF: TStringField
      DisplayLabel = 'Rela'#231#227'o de Cheques|CPF'
      FieldName = 'cCNPJCPF'
      FixedChar = True
      Size = 14
    end
    object qryPrepara_Temp_ChequenValCheque: TBCDField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Valor Cheque'
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryPrepara_Temp_ChequedDtDeposito: TDateTimeField
      DisplayLabel = 'Rela'#231#227'o de Cheques|Data Dep'#243'sito'
      FieldName = 'dDtDeposito'
      EditMask = '!99/99/9999;1;_'
    end
  end
  object dsTemp_Cheque: TDataSource
    DataSet = qryPrepara_Temp_Cheque
    Left = 320
    Top = 240
  end
  object SP_LIQUIDA_ADIANTAMENTO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_LIQUIDA_ADIANTAMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nValAdiantamento'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nValDinheiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 392
    Top = 208
  end
end
