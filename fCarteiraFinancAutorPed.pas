unit fCarteiraFinancAutorPed;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  StdCtrls, Mask, DBCtrls, ADODB, cxLookAndFeelPainters, cxButtons;

type
  TfrmCarteiraFinancAutorPed = class(TfrmProcesso_Padrao)
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceironValLimiteCred: TBCDField;
    qryTerceironValCreditoUtil: TBCDField;
    qryTerceironValPedidoAberto: TBCDField;
    qryTerceironValRecAtraso: TBCDField;
    qryTerceirodMaiorRecAtraso: TDateTimeField;
    DataSource1: TDataSource;
    qryTerceironValCredFinanc: TBCDField;
    GroupBox1: TGroupBox;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    DBEdit54: TDBEdit;
    DBEdit55: TDBEdit;
    DBEdit56: TDBEdit;
    DBEdit57: TDBEdit;
    DBEdit58: TDBEdit;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    DBEdit61: TDBEdit;
    DBEdit62: TDBEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    cxButton6: TcxButton;
    cxButton7: TcxButton;
    qryPosicaoFinanceira: TADOQuery;
    qryPosicaoFinanceiranValLimiteCred: TBCDField;
    qryPosicaoFinanceiranValPendenciaFinanceiraReceber: TBCDField;
    qryPosicaoFinanceiranValPendenciaFinanceiraPagar: TBCDField;
    qryPosicaoFinanceiranValPendenciaFinanceiraAtrasado: TBCDField;
    qryPosicaoFinanceiranValPedidoVendaAberto: TBCDField;
    qryPosicaoFinanceiranValPedidoCompraAberto: TBCDField;
    qryPosicaoFinanceiranValChequePendente: TBCDField;
    qryPosicaoFinanceiranValChequeRisco: TBCDField;
    qryPosicaoFinanceiranSaldoLimite: TBCDField;
    qryMediaVenda: TADOQuery;
    qryMediaVendanValorMedio: TBCDField;
    qryMediaVendanValorTotal: TBCDField;
    DataSource11: TDataSource;
    DataSource12: TDataSource;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    procedure FormShow(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTerceiro : integer ;
  end;

var
  frmCarteiraFinancAutorPed: TfrmCarteiraFinancAutorPed;

implementation

uses fTerceiro_PosicaoReceber, fTerceiro_VendaAutorizada,
  fTerceiro_ChequePendente, dcVendaProdutoCliente_view, fMenu,
  fTerceiro_MediaVenda;

{$R *.dfm}

procedure TfrmCarteiraFinancAutorPed.FormShow(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTerceiro, IntToStr(nCdTerceiro)) ;
  Posicionaquery(qryPosicaoFinanceira, qryTerceironCdTerceiro.asString) ;
  Posicionaquery(qryMediaVenda, qryTerceironCdTerceiro.asString) ;

end;

procedure TfrmCarteiraFinancAutorPed.cxButton6Click(Sender: TObject);
var
    objForm : TfrmTerceiro_PosicaoReceber ;
begin
  inherited;
  if (qryTerceiro.Active) and (qryPosicaoFinanceiranValPendenciaFinanceiraReceber.Value > 0 ) then
  begin

      objForm := TfrmTerceiro_PosicaoReceber.Create(nil) ;

      objForm.exibeTitulos(qryTerceironCdTerceiro.Value
                          ,FALSE);

      FreeAndNil(objForm) ;

  end ;

end;

procedure TfrmCarteiraFinancAutorPed.cxButton5Click(Sender: TObject);
var
    objForm : TfrmTerceiro_PosicaoReceber ;
begin
  inherited;
  if (qryTerceiro.Active) and (qryPosicaoFinanceiranValPendenciaFinanceiraAtrasado.Value > 0) then
  begin

      objForm := TfrmTerceiro_PosicaoReceber.Create(nil) ;

      objForm.exibeTitulos(qryTerceironCdTerceiro.Value
                          ,TRUE);

      FreeAndNil(objForm) ;

  end ;


end;

procedure TfrmCarteiraFinancAutorPed.cxButton4Click(Sender: TObject);
var
    objForm : TfrmTerceiro_VendaAutorizada ;
begin
  inherited;

  objForm := TfrmTerceiro_VendaAutorizada.Create(nil) ;

  if (qryTerceiro.Active) and (qryPosicaoFinanceiranValPedidoVendaAberto.Value > 0) then
  begin

      objForm.exibeValores(qryTerceironCdTerceiro.Value);

  end ;

end;

procedure TfrmCarteiraFinancAutorPed.cxButton3Click(Sender: TObject);
var
  objForm : TfrmTerceiro_ChequePendente ;
begin
  inherited;
  if (qryTerceiro.Active) and (qryPosicaoFinanceiranValChequePendente.Value > 0) then
  begin

      objForm := TfrmTerceiro_ChequePendente.Create(nil) ;

      objForm.exibeValores(qryTerceironCdTerceiro.Value,0);

      FreeAndNil(objForm) ;

  end ;

end;

procedure TfrmCarteiraFinancAutorPed.cxButton1Click(Sender: TObject);
var
  dataInicial, dataFinal : String ;
  objForm : TdcmVendaProdutoCliente_view ;
begin

  if qryTerceiro.Active then
  begin

      dataInicial := DateToStr(date()-365);
      dataFinal   := DateToStr(date());

      objForm := TdcmVendaProdutoCliente_view.Create(nil) ;

      objForm.ADODataSet1.Close ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdGrupoEconomico').Value := 0 ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdTerceiro').Value       := qryTerceironCdTerceiro.Value;
      objForm.ADODataSet1.Parameters.ParamByName('nCdDepartamento').Value   := 0 ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdMarca').Value          := 0 ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdLinha').Value          := 0 ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdClasseProduto').Value  := 0 ;
      objForm.ADODataSet1.Parameters.ParamByName('dDtInicial').Value        := frmMenu.ConvData(dataInicial) ;
      objForm.ADODataSet1.Parameters.ParamByName('dDtFinal').Value          := frmMenu.ConvData(dataFinal) ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdRamoAtividade').Value  := 0 ;
      objForm.ADODataSet1.Parameters.ParamByName('nCdTerceiroRepres').Value := 0 ;
      objForm.ADODataSet1.Open ;

      if (objForm.ADODataSet1.eof) then
      begin
          ShowMessage('Nenhuma informa��o encontrada.') ;
          FreeAndNil(objForm) ;
          exit ;
      end ;

      frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

      if objForm.PivotCube1.Active then
      begin
          objForm.PivotCube1.Active := False;
      end;

      objForm.PivotCube1.ExtendedMode := True;
      objForm.PVMeasureToolBar1.HideButtons := False;
      objForm.PivotCube1.Active := True;

      objForm.PivotMap1.Measures[2].ColumnPercent := True;
      objForm.PivotMap1.Measures[2].Value := False;

      objForm.PivotMap1.Measures[3].Rank := True ;
      objForm.PivotMap1.Measures[3].Value := False;

      objForm.PivotMap1.SortColumn(0,3,False) ;

      objForm.PivotMap1.Title := 'ER2Soft - An�lise de Vendas por Clientes' ;
      objForm.PivotGrid1.RefreshData;

      frmMenu.StatusBar1.Panels[6].Text := '' ;

      objForm.ShowModal ;
      Self.Activate ;

  end;

end;

procedure TfrmCarteiraFinancAutorPed.cxButton7Click(Sender: TObject);
var
  objForm : TfrmTerceiro_MediaVenda;
begin
  inherited;

  if (not qryMediaVenda.Active) or (qryMediaVendanValorMedio.Value = 0) then
  begin
      MensagemAlerta('Nenhuma venda nos �ltimos 12 meses para esse terceiro.') ;
      abort ;
  end ;

  objForm := TfrmTerceiro_MediaVenda.Create(nil) ;
  objForm.exibeValores(qryTerceironCdTerceiro.Value);

  FreeAndNil( objForm ) ;

end;

procedure TfrmCarteiraFinancAutorPed.cxButton2Click(Sender: TObject);
var
  objForm : TfrmTerceiro_ChequePendente ;
begin
  inherited;

  if (qryTerceiro.Active) and (qryPosicaoFinanceiranValChequeRisco.Value > 0)  then
  begin

      objForm := TfrmTerceiro_ChequePendente.Create(nil) ;

      objForm.exibeValores(qryTerceironCdTerceiro.Value,1);

      FreeAndNil(objForm) ;

  end ;

end;

end.
