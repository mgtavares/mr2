inherited rptRelCredAberto: TrptRelCredAberto
  Left = 205
  Top = 146
  Caption = 'Relat'#243'rio de Credi'#225'rios em Aberto'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel [1]
    Left = 70
    Top = 40
    Width = 20
    Height = 13
    Caption = 'Loja'
    FocusControl = DBEdit1
  end
  object Label3: TLabel [2]
    Left = 19
    Top = 64
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data da Venda'
  end
  object Label6: TLabel [3]
    Left = 180
    Top = 64
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object ER2LookupMaskEdit1: TER2LookupMaskEdit [5]
    Left = 96
    Top = 32
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    CodigoLookup = 59
    QueryLookup = qryTipoLoja
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 168
    Top = 32
    Width = 221
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsTipoLoja
    TabOrder = 2
  end
  object MaskEdit6: TMaskEdit [7]
    Left = 96
    Top = 56
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit7: TMaskEdit [8]
    Left = 200
    Top = 56
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  inherited ImageList1: TImageList
    Left = 840
    Top = 432
  end
  object qryTipoLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '              ,nCdLoja'
      '    FROM Loja'
      '   WHERE nCdLoja = :nPK')
    Left = 440
    Top = 256
    object qryTipoLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryTipoLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
  end
  object dsTipoLoja: TDataSource
    DataSet = qryTipoLoja
    Left = 472
    Top = 256
  end
end
