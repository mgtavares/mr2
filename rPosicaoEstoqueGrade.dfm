inherited rptPosicaoEstoqueGrade: TrptPosicaoEstoqueGrade
  Left = 53
  Top = 73
  Height = 665
  Caption = 'Relat'#243'rio Posi'#231#227'o de Estoque - em Grade'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 605
  end
  object Label1: TLabel [1]
    Left = 81
    Top = 72
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label2: TLabel [2]
    Left = 32
    Top = 96
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
  end
  object Label3: TLabel [3]
    Left = 54
    Top = 120
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria'
  end
  object Label4: TLabel [4]
    Left = 33
    Top = 144
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sub Categoria'
  end
  object Label5: TLabel [5]
    Left = 53
    Top = 168
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Segmento'
  end
  object Label6: TLabel [6]
    Left = 72
    Top = 192
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  object Label7: TLabel [7]
    Left = 31
    Top = 288
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Produto'
  end
  object Label8: TLabel [8]
    Left = 39
    Top = 360
    Width = 62
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dias Estoque'
  end
  object Label9: TLabel [9]
    Left = 191
    Top = 360
    Width = 6
    Height = 13
    Caption = 'a'
  end
  object Label10: TLabel [10]
    Left = 49
    Top = 336
    Width = 52
    Height = 13
    Alignment = taRightJustify
    Caption = 'Refer'#234'ncia'
  end
  object Label11: TLabel [11]
    Left = 76
    Top = 216
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Linha'
  end
  object Label12: TLabel [12]
    Left = 63
    Top = 240
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cole'#231#227'o'
  end
  object Label13: TLabel [13]
    Left = 70
    Top = 264
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Classe'
  end
  object Label14: TLabel [14]
    Left = 8
    Top = 312
    Width = 93
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campanha Promoc.'
  end
  object Label15: TLabel [15]
    Left = 30
    Top = 48
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Estoque'
  end
  inherited ToolBar1: TToolBar
    TabOrder = 35
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtLoja: TMaskEdit [17]
    Left = 104
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
  end
  object edtDepartamento: TMaskEdit [18]
    Left = 104
    Top = 88
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = edtDepartamentoExit
    OnKeyDown = edtDepartamentoKeyDown
  end
  object edtCategoria: TMaskEdit [19]
    Left = 104
    Top = 112
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = edtCategoriaExit
    OnKeyDown = edtCategoriaKeyDown
  end
  object edtSubCategoria: TMaskEdit [20]
    Left = 104
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = edtSubCategoriaExit
    OnKeyDown = edtSubCategoriaKeyDown
  end
  object edtSegmento: TMaskEdit [21]
    Left = 104
    Top = 160
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = edtSegmentoExit
    OnKeyDown = edtSegmentoKeyDown
  end
  object edtMarca: TMaskEdit [22]
    Left = 104
    Top = 184
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = edtMarcaExit
    OnKeyDown = edtMarcaKeyDown
  end
  object RadioGroup1: TRadioGroup [23]
    Left = 8
    Top = 392
    Width = 185
    Height = 41
    Caption = 'Exibir Valores'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 15
  end
  object RadioGroup2: TRadioGroup [24]
    Left = 200
    Top = 392
    Width = 257
    Height = 41
    Caption = 'Agrupamento'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Produto Estruturado'
      'Produto Final')
    TabOrder = 16
  end
  object DBEdit1: TDBEdit [25]
    Tag = 1
    Left = 176
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmLoja'
    DataSource = DataSource1
    TabOrder = 24
  end
  object DBEdit2: TDBEdit [26]
    Tag = 1
    Left = 176
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = DataSource2
    TabOrder = 25
  end
  object DBEdit3: TDBEdit [27]
    Tag = 1
    Left = 176
    Top = 112
    Width = 654
    Height = 21
    DataField = 'cNmCategoria'
    DataSource = DataSource3
    TabOrder = 26
  end
  object DBEdit4: TDBEdit [28]
    Tag = 1
    Left = 176
    Top = 136
    Width = 654
    Height = 21
    DataField = 'cNmSubCategoria'
    DataSource = DataSource4
    TabOrder = 27
  end
  object DBEdit5: TDBEdit [29]
    Tag = 1
    Left = 176
    Top = 160
    Width = 654
    Height = 21
    DataField = 'cNmSegmento'
    DataSource = DataSource5
    TabOrder = 28
  end
  object DBEdit6: TDBEdit [30]
    Tag = 1
    Left = 176
    Top = 184
    Width = 654
    Height = 21
    DataField = 'cNmMarca'
    DataSource = DataSource6
    TabOrder = 29
  end
  object edtGrupoProduto: TMaskEdit [31]
    Left = 104
    Top = 280
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 10
    Text = '      '
    OnExit = edtGrupoProdutoExit
    OnKeyDown = edtGrupoProdutoKeyDown
  end
  object DBEdit7: TDBEdit [32]
    Tag = 1
    Left = 176
    Top = 280
    Width = 654
    Height = 21
    DataField = 'cNmGrupoProduto'
    DataSource = DataSource7
    TabOrder = 30
  end
  object RadioGroup4: TRadioGroup [33]
    Left = 8
    Top = 440
    Width = 185
    Height = 41
    Caption = 'Somente Produtos Ativos'
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 18
  end
  object RadioGroup3: TRadioGroup [34]
    Left = 200
    Top = 440
    Width = 201
    Height = 41
    Caption = 'Detalhar Local de Estoque'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 17
  end
  object RadioGroup5: TRadioGroup [35]
    Left = 464
    Top = 440
    Width = 161
    Height = 41
    Caption = 'Destacar Linha do Produto'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 19
  end
  object edtDiasEstoqueIni: TMaskEdit [36]
    Left = 104
    Top = 352
    Width = 75
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 13
    Text = '         '
    OnExit = edtGrupoProdutoExit
    OnKeyDown = edtGrupoProdutoKeyDown
  end
  object edtDiasEstoqueFim: TMaskEdit [37]
    Left = 205
    Top = 352
    Width = 75
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 14
    Text = '         '
    OnExit = edtGrupoProdutoExit
    OnKeyDown = edtGrupoProdutoKeyDown
  end
  object edtReferencia: TEdit [38]
    Left = 104
    Top = 328
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    MaxLength = 15
    TabOrder = 12
  end
  object edtLinha: TMaskEdit [39]
    Left = 104
    Top = 208
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 7
    Text = '      '
    OnExit = edtLinhaExit
    OnKeyDown = edtLinhaKeyDown
  end
  object edtColecao: TMaskEdit [40]
    Left = 104
    Top = 232
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 8
    Text = '      '
    OnExit = edtColecaoExit
    OnKeyDown = edtColecaoKeyDown
  end
  object edtClasseProduto: TMaskEdit [41]
    Left = 104
    Top = 256
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 9
    Text = '      '
    OnExit = edtClasseProdutoExit
    OnKeyDown = edtClasseProdutoKeyDown
  end
  object DBEdit8: TDBEdit [42]
    Tag = 1
    Left = 176
    Top = 208
    Width = 654
    Height = 21
    DataField = 'cNmLinha'
    DataSource = DataSource8
    TabOrder = 31
  end
  object DBEdit9: TDBEdit [43]
    Tag = 1
    Left = 176
    Top = 232
    Width = 654
    Height = 21
    DataField = 'cNmColecao'
    DataSource = DataSource9
    TabOrder = 32
  end
  object DBEdit10: TDBEdit [44]
    Tag = 1
    Left = 176
    Top = 256
    Width = 654
    Height = 21
    DataField = 'cNmClasseProduto'
    DataSource = DataSource10
    TabOrder = 33
  end
  object GroupBox1: TGroupBox [45]
    Left = 632
    Top = 392
    Width = 185
    Height = 137
    Caption = 'Totalizadores'
    TabOrder = 23
    object chkTotDepartamento: TCheckBox
      Left = 8
      Top = 16
      Width = 97
      Height = 17
      Caption = 'Departamento'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
    object chkTotCategoria: TCheckBox
      Left = 8
      Top = 32
      Width = 97
      Height = 17
      Caption = 'Categoria'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object chkTotSubCategoria: TCheckBox
      Left = 8
      Top = 48
      Width = 97
      Height = 17
      Caption = 'SubCategoria'
      Checked = True
      State = cbChecked
      TabOrder = 2
    end
    object chkTotSegmento: TCheckBox
      Left = 8
      Top = 64
      Width = 97
      Height = 17
      Caption = 'Segmento'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object chkTotGeral: TCheckBox
      Left = 8
      Top = 80
      Width = 97
      Height = 17
      Caption = 'Total Geral'
      Checked = True
      State = cbChecked
      TabOrder = 4
    end
  end
  object RadioGroup6: TRadioGroup [46]
    Left = 464
    Top = 392
    Width = 161
    Height = 41
    Caption = 'Quebrar Estrutura em Linhas'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 20
  end
  object RadioGroup7: TRadioGroup [47]
    Left = 8
    Top = 488
    Width = 185
    Height = 41
    Caption = 'Destacar Produtos em Promo'#231#227'o'
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 21
  end
  object RadioGroup8: TRadioGroup [48]
    Left = 200
    Top = 488
    Width = 201
    Height = 41
    Caption = 'Exibir Somente Produtos em Promo'#231#227'o'
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 22
  end
  object edtCdCampanhaPromoc: TMaskEdit [49]
    Left = 104
    Top = 304
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 11
    Text = '      '
    OnExit = edtCdCampanhaPromocExit
    OnKeyDown = edtCdCampanhaPromocKeyDown
  end
  object DBEdit11: TDBEdit [50]
    Tag = 1
    Left = 176
    Top = 304
    Width = 654
    Height = 21
    DataField = 'cNmCampanhaPromoc'
    DataSource = DataSource11
    TabOrder = 34
  end
  object edtGrupoEstoque: TMaskEdit [51]
    Left = 104
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = edtGrupoEstoqueExit
    OnKeyDown = edtGrupoEstoqueKeyDown
  end
  object DBEdit12: TDBEdit [52]
    Tag = 1
    Left = 176
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmGrupoEstoque'
    DataSource = DataSource12
    TabOrder = 36
  end
  object RadioGroup9: TRadioGroup [53]
    Left = 464
    Top = 488
    Width = 161
    Height = 41
    Caption = 'Forma de Exibi'#231#227'o'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Relatorio'
      'Planilha')
    TabOrder = 37
    Visible = False
  end
  inherited ImageList1: TImageList
    Left = 872
    Top = 144
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK'
      
        '      AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja =' +
        ' Loja.nCdLoja AND UL.nCdUsuario = :nCdUsuario)')
    Left = 664
    Top = 192
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLoja
    Left = 632
    Top = 296
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmDepartamento, nCdDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 712
    Top = 224
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
  end
  object DataSource2: TDataSource
    DataSet = qryDepartamento
    Left = 640
    Top = 304
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdDepartamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmCategoria, nCdCategoria'
      'FROM Categoria'
      'WHERE nCdCategoria = :nPK'
      'AND nCdDepartamento = :nCdDepartamento')
    Left = 824
    Top = 200
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
    object qryCategorianCdCategoria: TAutoIncField
      FieldName = 'nCdCategoria'
      ReadOnly = True
    end
  end
  object DataSource3: TDataSource
    DataSet = qryCategoria
    Left = 648
    Top = 312
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmSubCategoria, nCdSubCategoria'
      'FROM SubCategoria'
      'WHERE nCdSubCategoria = :nPK'
      'AND nCdCategoria = :nCdCategoria')
    Left = 792
    Top = 200
    object qrySubCategoriacNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object qrySubCategorianCdSubCategoria: TAutoIncField
      FieldName = 'nCdSubCategoria'
      ReadOnly = True
    end
  end
  object DataSource4: TDataSource
    DataSet = qrySubCategoria
    Left = 656
    Top = 320
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdSubCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmSegmento, nCdSegmento'
      'FROM Segmento'
      'WHERE nCdSegmento = :nPK'
      'AND nCdSubCategoria = :nCdSubCategoria')
    Left = 712
    Top = 264
    object qrySegmentocNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
    object qrySegmentonCdSegmento: TAutoIncField
      FieldName = 'nCdSegmento'
      ReadOnly = True
    end
  end
  object DataSource5: TDataSource
    DataSet = qrySegmento
    Left = 664
    Top = 328
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmMarca, nCdMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 752
    Top = 224
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
  end
  object DataSource6: TDataSource
    DataSet = qryMarca
    Left = 672
    Top = 336
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto, cNmGrupoProduto'
      'FROM GrupoProduto'
      'WHERE nCdGrupoProduto = :nPK')
    Left = 816
    Top = 264
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
  object DataSource7: TDataSource
    DataSet = qryGrupoProduto
    Left = 680
    Top = 344
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha '
      'WHERE nCdLinha = :nPK')
    Left = 816
    Top = 304
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object DataSource8: TDataSource
    DataSet = qryLinha
    Left = 624
    Top = 296
  end
  object qryColecao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdColecao, cNmColecao'
      'FROM Colecao'
      'WHERE nCdColecao = :nPK')
    Left = 824
    Top = 352
    object qryColecaonCdColecao: TIntegerField
      FieldName = 'nCdColecao'
    end
    object qryColecaocNmColecao: TStringField
      FieldName = 'cNmColecao'
      Size = 50
    end
  end
  object DataSource9: TDataSource
    DataSet = qryColecao
    Left = 632
    Top = 304
  end
  object qryClasseProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdClasseProduto, cNmClasseProduto'
      'FROM ClasseProduto'
      'WHERE nCdClasseProduto = :nPK')
    Left = 800
    Top = 408
    object qryClasseProdutonCdClasseProduto: TIntegerField
      FieldName = 'nCdClasseProduto'
    end
    object qryClasseProdutocNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
  end
  object DataSource10: TDataSource
    DataSet = qryClasseProduto
    Left = 640
    Top = 312
  end
  object qryCampanhaPromoc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdCampanhaPromoc'
      '      ,cNmCampanhaPromoc '
      '      ,cFlgAtivada'
      '      ,cFlgSuspensa'
      '  FROM CampanhaPromoc'
      ' WHERE nCdEmpresa        = :nCdEmpresa'
      '   AND nCdCampanhaPromoc = :nPK')
    Left = 904
    Top = 264
    object qryCampanhaPromocnCdCampanhaPromoc: TIntegerField
      FieldName = 'nCdCampanhaPromoc'
    end
    object qryCampanhaPromoccNmCampanhaPromoc: TStringField
      FieldName = 'cNmCampanhaPromoc'
      Size = 50
    end
    object qryCampanhaPromoccFlgAtivada: TIntegerField
      FieldName = 'cFlgAtivada'
    end
    object qryCampanhaPromoccFlgSuspensa: TIntegerField
      FieldName = 'cFlgSuspensa'
    end
  end
  object DataSource11: TDataSource
    DataSet = qryCampanhaPromoc
    Left = 648
    Top = 320
  end
  object qryGrupoEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoEstoque '
      '      ,cNmGrupoEstoque      '
      '  FROM GrupoEstoque'
      ' WHERE nCdGrupoEstoque = :nPK')
    Left = 776
    Top = 336
    object qryGrupoEstoquenCdGrupoEstoque: TIntegerField
      FieldName = 'nCdGrupoEstoque'
    end
    object qryGrupoEstoquecNmGrupoEstoque: TStringField
      FieldName = 'cNmGrupoEstoque'
      Size = 50
    end
  end
  object DataSource12: TDataSource
    DataSet = qryGrupoEstoque
    Left = 624
    Top = 320
  end
  object ER2Excel1: TER2Excel
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 904
    Top = 448
  end
end
