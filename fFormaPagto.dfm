inherited frmFormaPagto: TfrmFormaPagto
  Left = 349
  Top = 192
  Width = 883
  Height = 417
  Caption = 'Forma de Pagamento'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 867
    Height = 354
  end
  object Label1: TLabel [1]
    Left = 19
    Top = 46
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 70
    Width = 49
    Height = 13
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 35
    Top = 94
    Width = 22
    Height = 13
    Caption = 'Tipo'
    FocusControl = DBEdit3
  end
  inherited ToolBar2: TToolBar
    Width = 867
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 64
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdFormaPagto'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 64
    Top = 64
    Width = 649
    Height = 19
    DataField = 'cNmFormaPagto'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBCheckBox1: TDBCheckBox [7]
    Left = 64
    Top = 116
    Width = 97
    Height = 17
    Caption = 'Ativo'
    DataField = 'cFlgAtivo'
    DataSource = dsMaster
    TabOrder = 5
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBEdit3: TDBEdit [8]
    Left = 64
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoFormaPagto'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [9]
    Tag = 1
    Left = 133
    Top = 88
    Width = 580
    Height = 19
    DataField = 'cNmTabTipoFormaPagto'
    DataSource = DataSource1
    TabOrder = 4
  end
  object DBCheckBox5: TDBCheckBox [10]
    Left = 64
    Top = 140
    Width = 169
    Height = 17
    Caption = 'Permitir Uso no Caixa/Loja'
    DataField = 'cFlgPermCaixa'
    DataSource = dsMaster
    TabOrder = 6
    ValueChecked = '1'
    ValueUnchecked = '0'
    OnClick = DBCheckBox5Click
  end
  object DBCheckBox4: TDBCheckBox [11]
    Left = 413
    Top = 140
    Width = 161
    Height = 17
    Caption = 'Permitir Uso no Financeiro'
    DataField = 'cFlgPermFinanceiro'
    DataSource = dsMaster
    TabOrder = 7
    ValueChecked = '1'
    ValueUnchecked = '0'
    OnClick = DBCheckBox4Click
  end
  object GroupBox1: TGroupBox [12]
    Left = 64
    Top = 165
    Width = 337
    Height = 140
    TabOrder = 8
    object DBCheckBox10: TDBCheckBox
      Left = 16
      Top = 112
      Width = 97
      Height = 17
      Caption = 'Exigir Cliente'
      DataField = 'cFlgExigeCliente'
      DataSource = dsMaster
      TabOrder = 0
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox7: TDBCheckBox
      Left = 16
      Top = 88
      Width = 185
      Height = 17
      Caption = 'TEF'
      DataField = 'cFlgTEF'
      DataSource = dsMaster
      TabOrder = 1
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox6: TDBCheckBox
      Left = 16
      Top = 64
      Width = 257
      Height = 17
      Caption = 'Impacta Saldo Caixa (Fluxo Caixa Realizado)'
      DataField = 'cFlgImpactaCaixa'
      DataSource = dsMaster
      TabOrder = 2
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox2: TDBCheckBox
      Left = 16
      Top = 40
      Width = 177
      Height = 17
      Caption = 'Gerar Comprovante no Caixa'
      DataField = 'cFlgGeraComprov'
      DataSource = dsMaster
      TabOrder = 3
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox3: TDBCheckBox
      Left = 16
      Top = 16
      Width = 249
      Height = 17
      Caption = 'Permitir Liquida'#231#227'o de Parcelas de Credi'#225'rio'
      DataField = 'cFlgLiqCrediario'
      DataSource = dsMaster
      TabOrder = 4
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
  end
  object GroupBox2: TGroupBox [13]
    Left = 413
    Top = 165
    Width = 299
    Height = 140
    TabOrder = 9
    object DBCheckBox12: TDBCheckBox
      Left = 16
      Top = 40
      Width = 185
      Height = 17
      Caption = 'Exigir C'#243'digo de Barras'
      DataField = 'cFlgExigeCB'
      DataSource = dsMaster
      TabOrder = 0
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox11: TDBCheckBox
      Left = 16
      Top = 16
      Width = 185
      Height = 17
      Caption = 'Permitir Telepagamento'
      DataField = 'cFlgTP'
      DataSource = dsMaster
      TabOrder = 1
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
  end
  object rgTipoCad: TRadioGroup [14]
    Left = 64
    Top = 312
    Width = 337
    Height = 49
    Caption = ' Tipo Cad. Cliente PDV '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Cadastro Simples'
      'Cadastro Completo')
    TabOrder = 10
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterEdit = qryMasterAfterEdit
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM FormaPagto'
      ' WHERE nCdFormaPagto = :nPK')
    Left = 648
    Top = 120
    object qryMasternCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryMastercNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryMastercFlgAtivo: TIntegerField
      FieldName = 'cFlgAtivo'
    end
    object qryMastercFlgGeraComprov: TIntegerField
      FieldName = 'cFlgGeraComprov'
    end
    object qryMasternCdTabTipoFormaPagto: TIntegerField
      FieldName = 'nCdTabTipoFormaPagto'
    end
    object qryMastercFlgLiqCrediario: TIntegerField
      FieldName = 'cFlgLiqCrediario'
    end
    object qryMastercFlgPermFinanceiro: TIntegerField
      FieldName = 'cFlgPermFinanceiro'
    end
    object qryMastercFlgPermCaixa: TIntegerField
      FieldName = 'cFlgPermCaixa'
    end
    object qryMastercFlgImpactaCaixa: TIntegerField
      FieldName = 'cFlgImpactaCaixa'
    end
    object qryMastercFlgTEF: TIntegerField
      FieldName = 'cFlgTEF'
    end
    object qryMastercFlgTP: TIntegerField
      FieldName = 'cFlgTP'
    end
    object qryMastercFlgExigeCB: TIntegerField
      FieldName = 'cFlgExigeCB'
    end
    object qryMasternCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMasterdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryMastercFlgExigeCliente: TIntegerField
      FieldName = 'cFlgExigeCliente'
    end
    object qryMastercFlgTipoCadCliente: TIntegerField
      FieldName = 'cFlgTipoCadCliente'
    end
  end
  inherited dsMaster: TDataSource
    Left = 648
    Top = 152
  end
  inherited qryID: TADOQuery
    Left = 680
    Top = 120
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 712
    Top = 152
  end
  inherited qryStat: TADOQuery
    Left = 712
    Top = 120
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 680
    Top = 152
  end
  inherited ImageList1: TImageList
    Left = 776
    Top = 120
  end
  object qryTabTipoFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabTipoFormaPagto'
      'WHERE nCdTabTipoFormaPagto = :nPK')
    Left = 744
    Top = 120
    object qryTabTipoFormaPagtonCdTabTipoFormaPagto: TIntegerField
      FieldName = 'nCdTabTipoFormaPagto'
    end
    object qryTabTipoFormaPagtocNmTabTipoFormaPagto: TStringField
      FieldName = 'cNmTabTipoFormaPagto'
      Size = 50
    end
    object qryTabTipoFormaPagtocFlgExigeCliente: TIntegerField
      FieldName = 'cFlgExigeCliente'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTabTipoFormaPagto
    Left = 744
    Top = 152
  end
end
