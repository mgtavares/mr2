unit fCaixa_NumValePresente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, DB, cxDBData, cxLookAndFeelPainters, cxButtons,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, jpeg, ExtCtrls, ADODB, GridsEh,
  DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmCaixa_NumValePresente = class(TForm)
    edtValorTotal: TcxCurrencyEdit;
    Label1: TLabel;
    Image1: TImage;
    qryTemp_ValePresente: TADOQuery;
    qryTemp_ValePresentecCodigo: TStringField;
    qryTemp_ValePresentenValor: TBCDField;
    dsTemp_ValePresente: TDataSource;
    btnCancelar: TcxButton;
    btnConfirmar: TcxButton;
    DBGridEh1: TDBGridEh;
    qryTestaVale: TADOQuery;
    qryTestaValenCdLoja: TIntegerField;
    qryTestaValedDtVale: TDateTimeField;
    procedure btnConfirmarClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1KeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_NumValePresente: TfrmCaixa_NumValePresente;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmCaixa_NumValePresente.btnConfirmarClick(Sender: TObject);
var
    nValTotal   : double ;
    nValVendido : double ;
begin

    nValTotal := 0 ;

    qryTemp_ValePresente.First ;

    while not qryTemp_ValePresente.Eof do
    begin
        nValTotal := nValTotal + qryTemp_ValePresentenValor.Value ;
        qryTemp_ValePresente.Next;
    end ;

    qryTemp_ValePresente.First ;


    nValTotal   := frmMenu.TBRound(nValTotal,2);
    nValVendido := frmmenu.TBRound(edtValorTotal.Value,2);

    if (nValTotal <> nValVendido) then
    begin
        frmMenu.MensagemErro('A soma do valor dos cart�es n�o confere com o valor total vendido. Verifique.') ;
        abort ;
    end ;

    {-- verifica se a numera��o dos cart�es presentes n�o existem em aberto --}
    qryTemp_ValePresente.First ;

    while not qryTemp_ValePresente.Eof do
    begin
        qryTestaVale.Close ;
        qryTestaVale.Parameters.ParamByName('nCdTabTipoVale').Value := 2 ;
        qryTestaVale.Parameters.ParamByName('cCodVale').Value       := Trim(qryTemp_ValePresentecCodigo.Value);
        qryTestaVale.Open;

        if not qryTestaVale.eof then
        begin
            frmMenu.MensagemErro('O Cart�o N�mero ' + qryTemp_ValePresentecCodigo.Value + ' j� foi vendido e o saldo est� pendente.' + #13#13 + 'Dados da Venda:  Loja: ' + frmMenu.ZeroEsquerda(qryTestaValenCdLoja.AsString,3) + '   Data Venda: ' + qryTestaValedDtVale.AsString) ;
            abort ;
        end ;

        qryTemp_ValePresente.Next;
    end ;

    Self.ModalResult := MrNo ;

    qryTemp_ValePresente.First ;

    qryTestaVale.Close;

    if (frmMenu.MessageDlg('Confirma o n�mero dos Cart�es Presente ?',mtConfirmation,[mbYes,mbNo],0) = MRNO) then
        exit ;

    Self.ModalResult := MrYes ;

end;

procedure TfrmCaixa_NumValePresente.btnCancelarClick(Sender: TObject);
begin

    frmMenu.ZeraTemp('#Temp_ValePresente') ;

end;

procedure TfrmCaixa_NumValePresente.FormShow(Sender: TObject);
begin

    qryTemp_ValePresente.Close ;
    qryTemp_ValePresente.Open ;

    DBGridEh1.SetFocus;

end;

procedure TfrmCaixa_NumValePresente.DBGridEh1KeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) and (qryTemp_ValePresentecCodigo.Value = '') then
    begin
        qryTemp_ValePresente.Cancel;
        btnConfirmar.SetFocus;
    end ;

end;

end.
