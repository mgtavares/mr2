inherited frmHistoricoPadrao: TfrmHistoricoPadrao
  Left = 216
  Top = 19
  Width = 1186
  Caption = 'Hist'#243'rico Padr'#227'o'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1170
  end
  object Label1: TLabel [1]
    Left = 56
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 45
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 96
    Top = 78
    Width = 185
    Height = 13
    Caption = '[Par'#226'metro] - valor a ser substituido.'
  end
  inherited ToolBar2: TToolBar
    Width = 1170
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 96
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdHistoricoPadrao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 96
    Top = 56
    Width = 753
    Height = 19
    DataField = 'cNmHistoricoPadrao'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM HistoricoPadrao'
      'WHERE nCdHistoricoPadrao = :nPK')
    object qryMasternCdHistoricoPadrao: TIntegerField
      FieldName = 'nCdHistoricoPadrao'
    end
    object qryMastercNmHistoricoPadrao: TStringField
      FieldName = 'cNmHistoricoPadrao'
      Size = 100
    end
  end
  inherited dsMaster: TDataSource
    Left = 512
    Top = 224
  end
  inherited qryID: TADOQuery
    Left = 592
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 632
    Top = 192
  end
  inherited qryStat: TADOQuery
    Left = 552
    Top = 192
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 632
    Top = 224
  end
  inherited ImageList1: TImageList
    Left = 672
    Top = 192
  end
end
