unit fCaixa_Sangria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, StdCtrls, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxCurrencyEdit, cxLookAndFeelPainters, cxButtons, DB, ADODB,
  RDprint;

type
  TfrmCaixa_Sangria = class(TForm)
    Image1: TImage;
    edtValor: TcxCurrencyEdit;
    Label3: TLabel;
    btnConfirmar: TcxButton;
    btnCancelar: TcxButton;
    SP_LANCAMENTO_CAIXA: TADOStoredProc;
    qryContaCofre: TADOQuery;
    qryContaCofrenCdContaBancaria: TIntegerField;
    qryContaCofrenCdConta: TStringField;
    qryTipoLancto: TADOQuery;
    qryTipoLanctocFlgEmiteECF: TIntegerField;
    RDprint1: TRDprint;
    qryLanctoFin: TADOQuery;
    qryLanctoFinnCdLanctoFin: TAutoIncField;
    qryLanctoFinnCdContaBancaria: TIntegerField;
    qryLanctoFinnCdConta: TStringField;
    qryLanctoFindDtLancto: TDateTimeField;
    qryLanctoFinnCdTipoLancto: TIntegerField;
    qryLanctoFincNmTipoLancto: TStringField;
    qryLanctoFinnValLancto: TBCDField;
    qryLanctoFinnCdUsuario: TIntegerField;
    qryLanctoFincNmUsuario: TStringField;
    qryLanctoFincHistorico: TStringField;
    qryVerificaSaldo: TADOQuery;
    qryVerificaSaldonSaldoAtualizado: TBCDField;
    procedure btnConfirmarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure EmitirComprovante;
    procedure ImprimirComprovante(nCdLanctoFin : integer) ;
    procedure edtValorKeyPress(Sender: TObject; var Key: Char);
    procedure edtValorExit(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdResumoCaixa  : integer ;
    cFlgUsaECF      : boolean ;
    bSenha          : boolean ;
    nCdContaCofre   : integer ;
    cValorParametro : String  ;
  end;

var
  frmCaixa_Sangria: TfrmCaixa_Sangria;
  nCdLanctoFin : integer ;

implementation

uses fCaixa_Recebimento, fMenu;

{$R *.dfm}

procedure TfrmCaixa_Sangria.btnConfirmarClick(Sender: TObject);
var
    objCaixa : TfrmCaixa_Recebimento ;
begin

    if (cValorParametro = 'S') and (nCdContaCofre = 0) then
    begin
        frmMenu.MensagemErro('Nenhuma conta cofre parametrizada para esta loja.') ;
        btnConfirmar.Enabled := False ;
        close ;
    end;

    if (edtValor.Value <= 0) then
    begin
        frmMenu.MensagemErro('Valor da sangria inv�lido ou n�o informado.') ;
        edtValor.SetFocus ;
        abort ;
    end ;

    if (frmMenu.MessageDlg('Confirma o valor da Sangria ?'+#13#13+#13#13+'Valor ' + Format('%m',[edtValor.Value]),mtConfirmation,[mbYes,mbNo],0) = MRNO) then
    begin
        edtValor.Value := 0 ;
        edtValor.SetFocus;
        abort ;
    end ;

  {-- Verifica se o h� dinheiro dispon�vel no caixa para realizar a sangria --}
  qryVerificaSaldo.Close;
  qryVerificaSaldo.Parameters.ParamByName('nValSangria').Value    := edtValor.Value;
  qryVerificaSaldo.Parameters.ParamByName('nCdResumoCaixa').Value := nCdResumoCaixa;
  qryVerificaSaldo.Open;

  if(qryVerificaSaldonSaldoAtualizado.Value < 0) then
  begin
      frmMenu.MensagemErro('O valor da sangria informado � superior ao saldo presente no caixa.');
      edtValor.SetFocus ;
      abort ;
  end;

  if (bSenha) then
  begin

      try

          objCaixa := TfrmCaixa_Recebimento.Create( Self ) ;

          if (not objCaixa.AutorizacaoGerente(24)) then
          begin
              frmMenu.MensagemAlerta('Esta transa��o requer uma autoriza��o n�vel gerente para que seja conclu�da.') ;
              abort ;
          end ;

      finally

          freeAndNil( objCaixa ) ;

      end ;

  end ;

  frmMenu.Connection.BeginTrans;

  try

      SP_LANCAMENTO_CAIXA.Close ;
      SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
      SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nCdResumoCaixa').Value := nCdResumoCaixa ;
      SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nCdTipoLancto').Value  := StrToInt(frmMenu.LeParametro('TIPOLANCTOSANG')) ;
      SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nValLancto').Value     := edtValor.Value ;

      if (cValorParametro = 'S') and (nCdContaCofre > 0) then
          SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nCdContaCofre').Value  := nCdContaCofre
      else SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nCdContaCofre').Value := 0 ;
      SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@cHistorico').Value     := 'SANGRIA' ;

      SP_LANCAMENTO_CAIXA.ExecProc;

      nCdLanctoFin := SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nCdLanctoFin_Out').Value ;

  except
      frmMenu.Connection.RollbackTrans;
      frmMenu.MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  if (frmMenu.MessageDlg('Deseja emitir o comprovante ?',mtConfirmation,[mbYes,mbNo],0) = MRYES) then
  begin
      EmitirComprovante;
  end ;


  Close ;

end;

procedure TfrmCaixa_Sangria.EmitirComprovante;
begin

  qryTipoLancto.Close ;
  qryTipoLancto.Parameters.ParamByName('nPK').Value := SP_LANCAMENTO_CAIXA.Parameters.ParamByName('@nCdTipoLancto').Value ;
  qryTipoLancto.Open ;

  if (qryTipoLanctocFlgEmiteECF.Value = 1) and (cFlgUsaECF) then
  begin
      if not (frmMenu.ACBrECF1.Ativo) then
          frmMenu.ACBrECF1.Ativar;

      frmMenu.ACBrECF1.Sangria(edtValor.Value, 'SANGRIA' , 'SANGRIA', 'DINHEIRO' ) ;

  end ;

  if (qryTipoLanctocFlgEmiteECF.Value = 0) or not (cFlgUsaECF) then
      ImprimirComprovante(nCdLanctoFin) ;

  Close() ;

end;

procedure TfrmCaixa_Sangria.FormShow(Sender: TObject);
begin
    nCdContaCofre   := 0;
    cValorParametro := frmMenu.LeParametro('USARCOFRE');

    if (nCdResumoCaixa <= 0) then
    begin
        frmMenu.MensagemErro('O parametro do resumo do caixa n�o foi enviado corretamente.');
        exit ;
    end ;

    btnConfirmar.Enabled := True ;

    if (cValorParametro = 'S') then
    begin

        qryContaCofre.Close ;
        qryContaCofre.Parameters.ParamByName('nPK').Value := frmMenu.nCdLojaAtiva ;
        qryContaCofre.Open ;

        if (qryContaCofre.IsEmpty) then
        begin
            frmMenu.MensagemErro('Nenhuma conta cofre parametrizada para esta loja.') ;
            btnConfirmar.Enabled := False ;
            close ;
        end ;

        nCdContaCofre := qryContaCofrenCdContaBancaria.Value;

    end ;

    nCdLanctoFin   := 0 ;
    edtValor.Value := 0 ;
    edtValor.SetFocus ;

end;

procedure TfrmCaixa_Sangria.ImprimirComprovante(nCdLanctoFin: integer);
begin
  frmMenu.ShowMessage('Prepare a impressora e clique em OK para impress�o do comprovante.') ;

  qryLanctoFin.Close ;
  qryLanctoFin.Parameters.ParamByName('nPK').Value := nCdLanctoFin ;
  qryLanctoFin.Open ;

  if (qryLanctoFin.eof) then
  begin
      frmMenu.MensagemErro('O lan�amento n�o foi gerado. Contate o Administrador do ER2Soft.');
      exit ;
  end ;

  try
      rdPrint1.Abrir ;
  except
      frmMenu.MensagemErro('Erro ao tentar abrir o relat�rio. Verifique a configura��o da impressora.') ;
      PostMessage(Self.Handle, WM_CLOSE, 0, 0);
      abort ;
  end ;


  rdprint1.TamanhoQteLPP      := seis ;
  rdprint1.FonteTamanhoPadrao := s10cpp;

  rdprint1.ImpF(01,01,' *** COMPROVANTE CAIXA ***',[Negrito]);
  rdprint1.ImpF(02,01,'- - - - - - - - - - - - - -',[Negrito]);

  rdPrint1.Imp(03,01,'') ;
  rdPrint1.ImpF(04,01,'Lan�amento : ' + qryLanctoFinnCdLanctoFin.AsString,[Normal]) ;
  rdPrint1.Imp(05,01,'') ;
  rdPrint1.ImpF(06,01,'Data Lancto: ' + qryLanctoFindDtLancto.AsString,[Normal]) ;
  rdPrint1.ImpF(07,01,'Tipo       : ' + qryLanctoFincNmTipoLancto.Value,[Normal]) ;
  rdPrint1.ImpF(08,01,'Operador   : ' + qryLanctoFincNmUsuario.Value,[Negrito]) ;
  rdPrint1.ImpF(10,01,'Hist�rico  : ' + qryLanctoFincHistorico.Value,[Normal]) ;
  rdPrint1.Imp(11,01,'') ;
  rdPrint1.ImpF(12,01,'Valor      : ',[Normal]) ;
  rdprint1.IMPD(12,29,formatFloat('###,##0.00',qryLanctoFinnValLancto.Value),[Expandido]);

  rdPrint1.ImpF(14,01,'Hora Atual : ' + DateTimeToStr(Now()),[Normal]) ;
  rdPrint1.ImpF(17,01,'Operador',[Normal]) ;
  rdPrint1.ImpF(19,01,'______________________________',[Normal]) ;
  rdPrint1.ImpF(22,01,'Gerente',[Normal]) ;
  rdPrint1.ImpF(24,01,'______________________________',[Normal]) ;

  if (frmMenu.LeParametro('PREVIEWIMP') = 'S') then
  begin
      rdprint1.OpcoesPreview.Preview := true ;
      rdprint1.OpcoesPreview.Remalina:= true ;
      rdprint1.OpcoesPreview.PaginaZebrada:= true ;
  end ;

  try
      rdPrint1.Fechar ;
  except
      frmMenu.MensagemErro('Erro ao tentar exibir o relat�rio. Verifique a configura��o da impressora.') ;
      PostMessage(Self.Handle, WM_CLOSE, 0, 0);
      abort ;
  end ;

end;

procedure TfrmCaixa_Sangria.edtValorKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (key = #13) then
        btnConfirmar.SetFocus;
        
end;

procedure TfrmCaixa_Sangria.edtValorExit(Sender: TObject);
begin

    edtValor.Update;
    
    if (edtValor.Value = 0) then
        Close() ;

end;

procedure TfrmCaixa_Sangria.btnCancelarClick(Sender: TObject);
begin
    Close ;
end;

end.
