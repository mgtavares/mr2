inherited frmProvisaoPagto_ContaBancaria: TfrmProvisaoPagto_ContaBancaria
  Left = 248
  Top = 93
  Width = 956
  Height = 589
  Caption = 'Sele'#231#227'o de Conta Banc'#225'ria'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 209
    Width = 940
    Height = 342
  end
  inherited ToolBar1: TToolBar
    Width = 940
    ButtonWidth = 94
    inherited ToolButton1: TToolButton
      Caption = 'Gerar Provis'#227'o'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 94
    end
    inherited ToolButton2: TToolButton
      Left = 102
    end
  end
  object TPanel [2]
    Left = 0
    Top = 29
    Width = 940
    Height = 164
    Align = alTop
    TabOrder = 1
    object DBGridEh1: TDBGridEh
      Left = 1
      Top = 1
      Width = 938
      Height = 162
      Align = alClient
      AllowedOperations = [alopUpdateEh]
      DataGrouping.GroupLevels = <>
      DataSource = dsFormaPagto
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'Tahoma'
      FooterFont.Style = []
      IndicatorOptions = [gioShowRowIndicatorEh]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      UseMultiTitle = True
      Columns = <
        item
          Checkboxes = True
          EditButtons = <>
          FieldName = 'cFlgOK'
          Footers = <>
          KeyList.Strings = (
            '1'
            '0')
          Width = 26
        end
        item
          EditButtons = <>
          FieldName = 'nCdFormaPagto'
          Footers = <>
          ReadOnly = True
          Width = 40
        end
        item
          EditButtons = <>
          FieldName = 'cNmFormaPagto'
          Footers = <>
          ReadOnly = True
          Width = 347
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object Panel1: TPanel [3]
    Left = 0
    Top = 193
    Width = 940
    Height = 16
    Align = alTop
    TabOrder = 2
  end
  object Panel2: TPanel [4]
    Left = 0
    Top = 209
    Width = 940
    Height = 342
    Align = alClient
    Caption = 'Panel2'
    TabOrder = 3
    object DBGridEh2: TDBGridEh
      Left = 1
      Top = 1
      Width = 938
      Height = 340
      Align = alClient
      DataGrouping.GroupLevels = <>
      DataSource = dsContaBancaria
      Flat = True
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'Tahoma'
      FooterFont.Style = []
      IndicatorOptions = [gioShowRowIndicatorEh]
      PopupMenu = PopupMenu1
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      UseMultiTitle = True
      Columns = <
        item
          Checkboxes = True
          EditButtons = <>
          FieldName = 'cFlgOK'
          Footers = <>
          KeyList.Strings = (
            '1'
            '0')
          Width = 21
        end
        item
          EditButtons = <>
          FieldName = 'nCdContaBancaria'
          Footers = <>
          ReadOnly = True
          Width = 28
        end
        item
          EditButtons = <>
          FieldName = 'nCdBanco'
          Footers = <>
          ReadOnly = True
          Width = 35
        end
        item
          EditButtons = <>
          FieldName = 'cAgencia'
          Footers = <>
          ReadOnly = True
          Width = 47
        end
        item
          EditButtons = <>
          FieldName = 'nCdConta'
          Footers = <>
          ReadOnly = True
          Width = 81
        end
        item
          EditButtons = <>
          FieldName = 'cNmTitular'
          Footers = <>
          ReadOnly = True
          Width = 292
        end
        item
          EditButtons = <>
          FieldName = 'dDtUltConciliacao'
          Footers = <>
          ReadOnly = True
          Width = 89
        end
        item
          EditButtons = <>
          FieldName = 'nSaldoInicial'
          Footers = <>
          ReadOnly = True
          Width = 98
        end
        item
          EditButtons = <>
          FieldName = 'nProvDebito'
          Footers = <>
          ReadOnly = True
          Width = 101
        end
        item
          EditButtons = <>
          FieldName = 'nSaldoFinal'
          Footers = <>
          ReadOnly = True
          Width = 97
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 472
    Top = 72
  end
  object qryFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'INSERT INTO #FormaPagto'
      '    SELECT nCdFormaPagto'
      '          ,cNmFormaPagto'
      '          ,0 cFlgOK'
      '          ,nCdTabTipoFormaPagto'
      '      FROM FormaPagto'
      '     WHERE cFlgPermFinanceiro = 1'
      ''
      'UPDATE #FormaPagto'
      '   SET cFlgOK = 0'
      ''
      'SELECT *'
      '  FROM #FormaPagto'
      ' ORDER BY cNmFormaPagto')
    Left = 280
    Top = 160
    object qryFormaPagtonCdFormaPagto: TIntegerField
      DisplayLabel = 'Formas de Pagamento|C'#243'd'
      FieldName = 'nCdFormaPagto'
    end
    object qryFormaPagtocNmFormaPagto: TStringField
      DisplayLabel = 'Formas de Pagamento|Descri'#231#227'o'
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryFormaPagtocFlgOK: TIntegerField
      DisplayLabel = 'Formas de Pagamento|OK'
      FieldKind = fkInternalCalc
      FieldName = 'cFlgOK'
    end
    object qryFormaPagtonCdTabTipoFormaPagto: TIntegerField
      FieldName = 'nCdTabTipoFormaPagto'
    end
  end
  object dsFormaPagto: TDataSource
    DataSet = qryFormaPagto
    Left = 320
    Top = 160
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa int'
      '       ,@dHoje      datetime'
      '       ,@nCdUsuario int'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      'Set @nCdUsuario = :nCdUsuario'
      ''
      'SELECT @dHoje = dbo.fn_OnlyDate(GetDate())'
      ''
      'INSERT INTO #ContaBancaria'
      '    SELECT nCdContaBancaria'
      '          ,nCdBanco'
      '          ,cAgencia'
      '          ,nCdConta'
      '          ,cNmTitular'
      '          ,0 cFlgOK'
      '          ,Convert(DECIMAL(12,2),0.00) nSaldoInicial'
      '          ,Convert(DECIMAL(12,2),0.00) nProvDebito'
      '          ,Convert(DECIMAL(12,2),0.00) nProvCredito'
      '          ,Convert(DECIMAL(12,2),0.00) nSaldoFinal'
      '          ,cFlgCaixa'
      '          ,dDtUltConciliacao'
      '      FROM ContaBancaria'
      '     WHERE cFlgProvTit = 1'
      '       AND nCdEmpresa  = @nCdEmpresa'
      '       AND nCdStatus   = 1'
      ' AND (EXISTS(SELECT 1'
      '                FROM UsuarioContaBancaria UCB'
      
        '               Where UCB.nCdContaBancaria = ContaBancaria.nCdCon' +
        'taBancaria'
      
        '                 AND UCB.nCdUsuario = @nCdUsuario) OR (cFlgCofre' +
        ' = 1 AND (   nCdUsuarioOperador IS NULL'
      
        '                                                                ' +
        '          OR nCdUsuarioOperador = @nCdUsuario)))'
      ''
      'UPDATE #ContaBancaria'
      '   SET cFlgOK = 0'
      '      ,nSaldoInicial = (SELECT nSaldoConta'
      '                          FROM ContaBancaria'
      
        '                         WHERE ContaBancaria.nCdContaBancaria = ' +
        '#ContaBancaria.nCdContaBancaria)'
      '      ,nProvDebito = (SELECT Sum(nValProvisao)'
      '                        FROM ProvisaoTit'
      
        '                       WHERE ProvisaoTit.nCdContaBancaria = #Con' +
        'taBancaria.nCdContaBancaria'
      '                         AND cFlgBaixado  = 0'
      '                         AND dDtCancel   IS NULL)'
      ''
      'UPDATE #ContaBancaria'
      
        '   SET nSaldoFinal = IsNull(nSaldoInicial,0) - IsNull(nProvDebit' +
        'o,0) + IsNull(nProvCredito,0)                         '
      ''
      'SELECT *'
      '  FROM #ContaBancaria'
      ''
      '')
    Left = 408
    Top = 242
    object qryContaBancarianCdContaBancaria: TIntegerField
      DisplayLabel = 'Contas autorizadas para voc'#234' provisionar|C'#243'd'
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdBanco: TIntegerField
      DisplayLabel = 'Contas autorizadas para voc'#234' provisionar|Banco'
      FieldName = 'nCdBanco'
    end
    object qryContaBancariacAgencia: TIntegerField
      DisplayLabel = 'Contas autorizadas para voc'#234' provisionar|Ag'#234'ncia'
      FieldName = 'cAgencia'
    end
    object qryContaBancarianCdConta: TStringField
      DisplayLabel = 'Contas autorizadas para voc'#234' provisionar|Conta'
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryContaBancariacNmTitular: TStringField
      DisplayLabel = 'Contas autorizadas para voc'#234' provisionar|Titular'
      FieldName = 'cNmTitular'
      Size = 50
    end
    object qryContaBancariacFlgOK: TIntegerField
      DisplayLabel = 'Contas autorizadas para voc'#234' provisionar|OK'
      FieldName = 'cFlgOK'
    end
    object qryContaBancarianSaldoInicial: TBCDField
      DisplayLabel = 'Saldo Atual'
      FieldName = 'nSaldoInicial'
      Precision = 12
      Size = 12
    end
    object qryContaBancarianProvDebito: TBCDField
      DisplayLabel = 'Provis'#245'es de|Pagamento'
      FieldName = 'nProvDebito'
      Precision = 12
      Size = 12
    end
    object qryContaBancarianProvCredito: TBCDField
      DisplayLabel = 'Contas autorizadas para voc'#234' provisionar|Prov. Cr'#233'dito'
      FieldName = 'nProvCredito'
      Precision = 12
      Size = 12
    end
    object qryContaBancarianSaldoFinal: TBCDField
      DisplayLabel = 'Saldo Final|Projetado'
      FieldName = 'nSaldoFinal'
      Precision = 12
      Size = 12
    end
    object qryContaBancariacFlgCaixa: TIntegerField
      FieldName = 'cFlgCaixa'
    end
    object qryContaBancariadDtUltConciliacao: TDateTimeField
      DisplayLabel = #218'ltima|Concilia'#231#227'o'
      FieldName = 'dDtUltConciliacao'
    end
  end
  object dsContaBancaria: TDataSource
    DataSet = qryContaBancaria
    Left = 448
    Top = 250
  end
  object qryProvisaoTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ProvisaoTit'
      'WHERE nCdProvisaoTit = :nPK')
    Left = 416
    Top = 125
    object qryProvisaoTitnCdProvisaoTit: TAutoIncField
      FieldName = 'nCdProvisaoTit'
    end
    object qryProvisaoTitnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryProvisaoTitnCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryProvisaoTitdDtPagto: TDateTimeField
      FieldName = 'dDtPagto'
    end
    object qryProvisaoTitnCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryProvisaoTitnCdUsuarioPrev: TIntegerField
      FieldName = 'nCdUsuarioPrev'
    end
    object qryProvisaoTitdDtPrev: TDateTimeField
      FieldName = 'dDtPrev'
    end
    object qryProvisaoTitnCdUsuarioBaixa: TIntegerField
      FieldName = 'nCdUsuarioBaixa'
    end
    object qryProvisaoTitdDtBaixa: TDateTimeField
      FieldName = 'dDtBaixa'
    end
    object qryProvisaoTitcFlgBaixado: TIntegerField
      FieldName = 'cFlgBaixado'
    end
    object qryProvisaoTitdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryProvisaoTitnCdUsuarioCancel: TIntegerField
      FieldName = 'nCdUsuarioCancel'
    end
    object qryProvisaoTitnValProvisao: TBCDField
      FieldName = 'nValProvisao'
      Precision = 12
      Size = 2
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 216
    Top = 117
  end
  object cdsTitulos: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    Left = 136
    Top = 141
    Data = {
      2C0000009619E0BD0100000018000000010000000000030000002C00096E4364
      546974756C6F04000100000000000000}
    object cdsTitulosnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#FormaPagto'#39') IS NULL)'#13#10'    SELECT TOP 0 ' +
      'nCdFormaPagto'#13#10'          ,cNmFormaPagto'#13#10'          ,0 cFlgOK'#13#10'  ' +
      '        ,nCdTabTipoFormaPagto'#13#10'      INTO #FormaPagto'#13#10'      FRO' +
      'M FormaPagto'#13#10'     '#13#10'     '#13#10'IF (OBJECT_ID('#39'tempdb..#ContaBancari' +
      'a'#39') IS NULL)'#13#10'    SELECT TOP 0 nCdContaBancaria'#13#10'          ,nCdB' +
      'anco'#13#10'          ,cAgencia'#13#10'          ,nCdConta'#13#10'          ,cNmTi' +
      'tular'#13#10'          ,0 cFlgOK'#13#10'          ,Convert(DECIMAL(12,2),0.0' +
      '0) nSaldoInicial'#13#10'          ,Convert(DECIMAL(12,2),0.00) nProvDe' +
      'bito'#13#10'          ,Convert(DECIMAL(12,2),0.00) nProvCredito'#13#10'     ' +
      '     ,Convert(DECIMAL(12,2),0.00) nSaldoFinal'#13#10'          ,cFlgCa' +
      'ixa'#13#10'          ,dDtUltConciliacao'#13#10'      INTO #ContaBancaria'#13#10'  ' +
      '    FROM ContaBancaria'#13#10#13#10'TRUNCATE TABLE #FormaPagto'#13#10'TRUNCATE T' +
      'ABLE #ContaBancaria'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 480
    Top = 133
  end
  object PopupMenu1: TPopupMenu
    Images = ImageList1
    Left = 620
    Top = 360
    object SaldoProjetado: TMenuItem
      Caption = 'Saldo Projetado'
      OnClick = SaldoProjetadoClick
    end
  end
end
