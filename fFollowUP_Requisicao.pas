unit fFollowUP_Requisicao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ComCtrls, ToolWin, ExtCtrls,
  GridsEh, DBGridEh, StdCtrls, DB, ADODB, cxControls, cxContainer, cxEdit,
  cxTextEdit, fProcesso_Padrao, DBCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk, Menus, ER2Excel,
  dxPSContainerLnk;

type
  TfrmFollowUP_Requisicao = class(TfrmProcesso_Padrao)
    SP_FOLLOWUP_REQUISICAO: TADOStoredProc;
    dsSP_FOLLOWUP_REQUISICAO: TDataSource;
    btMarcarCritico: TToolButton;
    ToolButton5: TToolButton;
    btImprimir: TToolButton;
    ToolButton7: TToolButton;
    btFollowUp: TToolButton;
    qryAux: TADOQuery;
    qryFollowUp: TADOQuery;
    dsFollowUp: TDataSource;
    qryFollowUpdDtFollowUp: TDateTimeField;
    qryFollowUpcNmUsuario: TStringField;
    qryFollowUpcOcorrenciaResum: TStringField;
    qryFollowUpdDtProxAcao: TDateTimeField;
    qryFollowUpcOcorrencia: TMemoField;
    ToolButton9: TToolButton;
    btFiltro: TToolButton;
    ToolButton11: TToolButton;
    GroupBox1: TGroupBox;
    rgModoExibicao: TRadioGroup;
    GroupBox2: TGroupBox;
    cxGridRequisicao: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    GroupBox3: TGroupBox;
    cxGridOcorrencia: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1dDtFollowUp: TcxGridDBColumn;
    cxGridDBTableView1cNmUsuario: TcxGridDBColumn;
    cxGridDBTableView1cOcorrenciaResum: TcxGridDBColumn;
    cxGridDBTableView1dDtProxAcao: TcxGridDBColumn;
    cxGridDBTableView1cOcorrencia: TcxGridDBColumn;
    DBMemo1: TDBMemo;
    dxComponentPrinter1: TdxComponentPrinter;
    rgTipoVisao: TRadioGroup;
    ER2Excel1: TER2Excel;
    ToolButton12: TToolButton;
    btPlanilha: TToolButton;
    dxComponentPrinter1Link1: TdxCustomContainerReportLink;
    GroupBox4: TGroupBox;
    cxTextEdit3: TcxTextEdit;
    Label29: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label24: TLabel;
    rgTabTipoRequis: TRadioGroup;
    cxGridDBTableView3DBColumn_nCdRequisicao: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cNmLoja: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cNmTipoRequisicao: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cNmSituacao: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cNmCC: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cNmSetor: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cNmSolicitante: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cUltimoAcomp: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cNmMarca: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cNmDepartamento: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cNmCategoria: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_nValTotalItem: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_nSaldoAberto: TcxGridDBColumn;
    SP_FOLLOWUP_REQUISICAOnCdRequisicao: TIntegerField;
    SP_FOLLOWUP_REQUISICAOdDtPrevAtendIni: TDateTimeField;
    SP_FOLLOWUP_REQUISICAOdDtPrevAtendFim: TDateTimeField;
    SP_FOLLOWUP_REQUISICAOcNmLoja: TStringField;
    SP_FOLLOWUP_REQUISICAOcNmTipoRequisicao: TStringField;
    SP_FOLLOWUP_REQUISICAOcNmSituacao: TStringField;
    SP_FOLLOWUP_REQUISICAOcNmCC: TStringField;
    SP_FOLLOWUP_REQUISICAOcNmSetor: TStringField;
    SP_FOLLOWUP_REQUISICAOcNmSolicitante: TStringField;
    SP_FOLLOWUP_REQUISICAOcUltimoAcomp: TStringField;
    SP_FOLLOWUP_REQUISICAOcNmMarca: TStringField;
    SP_FOLLOWUP_REQUISICAOcNmDepartamento: TStringField;
    SP_FOLLOWUP_REQUISICAOcNmCategoria: TStringField;
    SP_FOLLOWUP_REQUISICAOnValTotalItem: TBCDField;
    SP_FOLLOWUP_REQUISICAOnSaldoAberto: TBCDField;
    procedure btImprimirClick(Sender: TObject);
    procedure btFiltroClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rgModoExibicaoClick(Sender: TObject);
    procedure rgTabTipoRequisClick(Sender: TObject);
    procedure rgTipoVisaoClick(Sender: TObject);
    procedure btMarcarCriticoClick(Sender: TObject);
    procedure btFollowUpClick(Sender: TObject);
    procedure cxGridDBTableView3CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGridDBTableView3StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
  private
    { Private declarations }
    procedure prConsultaRequisicao();
  public
    { Public declarations }
  end;

var
  frmFollowUP_Requisicao: TfrmFollowUP_Requisicao;

implementation

uses fMenu, fNovo_FollowUp, fFollowUp_Requisicao_Filtro, fFollowUp_Requisicao_NovoFollowUp;

{$R *.dfm}

procedure TfrmFollowUP_Requisicao.btImprimirClick(Sender: TObject);
begin
  inherited;

  dxComponentPrinter1.Preview(True,nil);

end;

procedure TfrmFollowUP_Requisicao.btFiltroClick(Sender: TObject);
var
  objForm : TfrmFollowUP_Requisicao_Filtro;
begin
  inherited;

  try
      objForm := TfrmFollowUP_Requisicao_Filtro.Create(Self);

      showForm(objForm,false);

      { -- modo de exibi��o -- }
      case (rgModoExibicao.ItemIndex) of
          0 : SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@cTipoExibicao').Value := 'T'; //todos
          1 : SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@cTipoExibicao').Value := 'A'; //atraso
          2 : SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@cTipoExibicao').Value := 'C'; //cr�tico
      end;

      SP_FOLLOWUP_REQUISICAO.Close;
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@nCdUsuario').Value        := frmMenu.nCdUsuarioLogado;
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(objForm.edtEmpresa.Text);
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@nCdLoja').Value           := frmMenu.ConvInteiro(objForm.er2LkpLoja.Text);
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@nCdTabTipoRequis').Value  := rgTabTipoRequis.ItemIndex; //0 : todos - 1 : estoque - 2 : compra
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@nCdTipoRequisicao').Value := frmMenu.ConvInteiro(objForm.er2LkpTipoRequisicao.Text);
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@nCdCC').Value             := frmMenu.ConvInteiro(objForm.er2LkpCentroCusto.Text);
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@nCdSetor').Value          := frmMenu.ConvInteiro(objForm.er2LkpSetor.Text);
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@nCdMarca').Value          := frmMenu.ConvInteiro(objForm.er2LkpMarca.Text);
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@nCdProduto').Value        := frmMenu.ConvInteiro(objForm.er2LkpProduto.Text);
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@dDtPrevAtendIni').Value   := frmMenu.ConvData(objForm.mskDtAtendInicial.Text);
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@dDtPrevAtendFim').Value   := frmMenu.ConvData(objForm.mskDtAtendFinal.Text);
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@dDtRequisicaoIni').Value  := frmMenu.ConvData(objForm.mskDtRequisicaoInicial.Text);
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@dDtRequisicaoFim').Value  := frmMenu.ConvData(objForm.mskDtRequisicaoFinal.Text);
      SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@cFlgModoVisao').Value     := rgTipoVisao.ItemIndex; //0 : marca - 1 : marca,departamento,categoria
      SP_FOLLOWUP_REQUISICAO.Open;

      { -- altera exibi��o de colunas -- }
      case (rgTipoVisao.ItemIndex) of
          { -- marca -- }
          0 : begin
                  cxGridDBTableView3DBColumn_cNmDepartamento.Visible := False;
                  cxGridDBTableView3DBColumn_cNmCategoria.Visible    := False;
              end;
          { -- marca / departamento / categoria -- }
          1 : begin
                  cxGridDBTableView3DBColumn_cNmDepartamento.Visible := True;
                  cxGridDBTableView3DBColumn_cNmCategoria.Visible    := True;
              end;
      end;
  finally
      FreeAndNil(objForm);
  end;
end;

procedure TfrmFollowUP_Requisicao.FormShow(Sender: TObject);
begin
  inherited;

  btFiltro.Click;
end;

procedure TfrmFollowUP_Requisicao.prConsultaRequisicao();
begin

  { -- modo de exibi��o -- }
  case (rgModoExibicao.ItemIndex) of
      0 : SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@cTipoExibicao').Value := 'T'; //todos
      1 : SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@cTipoExibicao').Value := 'A'; //atraso
      2 : SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@cTipoExibicao').Value := 'C'; //cr�tico
  end;

  { -- tipo de requisi��o -- }
  SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@nCdTabTipoRequis').Value := rgTabTipoRequis.ItemIndex; //0 : todos - 1 : estoque - 2 : compra

  { -- tipo de vis�o -- }
  SP_FOLLOWUP_REQUISICAO.Parameters.ParamByName('@cFlgModoVisao').Value := rgTipoVisao.ItemIndex; //0 : marca - 1 : marca,departamento,categoria

  { -- atualiza consulta -- }
  SP_FOLLOWUP_REQUISICAO.Requery;

  { -- altera exibi��o de colunas -- }
  case (rgTipoVisao.ItemIndex) of
      { -- marca -- }
      0 : begin
              cxGridDBTableView3DBColumn_cNmDepartamento.Visible := False;
              cxGridDBTableView3DBColumn_cNmCategoria.Visible    := False;
          end;
      { -- marca / departamento / categoria -- }
      1 : begin
              cxGridDBTableView3DBColumn_cNmDepartamento.Visible := True;
              cxGridDBTableView3DBColumn_cNmCategoria.Visible    := True;
          end;
  end;
end;

procedure TfrmFollowUP_Requisicao.rgModoExibicaoClick(Sender: TObject);
begin
  inherited;

  prConsultaRequisicao;
end;

procedure TfrmFollowUP_Requisicao.rgTabTipoRequisClick(Sender: TObject);
begin
  inherited;

  prConsultaRequisicao;
end;

procedure TfrmFollowUP_Requisicao.rgTipoVisaoClick(Sender: TObject);
begin
  inherited;

  prConsultaRequisicao;
end;

procedure TfrmFollowUP_Requisicao.btMarcarCriticoClick(Sender: TObject);
begin
  inherited;

  if (SP_FOLLOWUP_REQUISICAO.Eof) then
      Exit;

  if (SP_FOLLOWUP_REQUISICAOcNmSituacao.Value = 'Cr�tico') then
  begin
      ShowMessage('Requisi��o j� est� em situa��o cr�tica.');
      Exit;
  end;

  case MessageDlg('Confirma a altera��o da situa��o desta requisi��o ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo: Exit;
  end;

  try
      frmMenu.Connection.BeginTrans;
      
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE Requisicao SET cFlgCritico = 1 WHERE nCdRequisicao = ' + SP_FOLLOWUP_REQUISICAOnCdRequisicao.AsString);
      qryAux.ExecSQL;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Situa��o da requisi��o alterada com sucesso.');

      qryAux.Close;
      
      SP_FOLLOWUP_REQUISICAO.Requery();
  except
      frmMenu.Connection.RollbackTrans;
      ShowMessage('Erro no processamento.');
      Raise;
  end;
end;

procedure TfrmFollowUP_Requisicao.btFollowUpClick(Sender: TObject);
var
  objForm : TfrmFollowUP_Requisicao_NovoFollowUp;
begin
  inherited;

  if (SP_FOLLOWUP_REQUISICAO.Eof) then
      Exit;

  objForm               := TfrmFollowUP_Requisicao_NovoFollowUp.Create(Self);
  objForm.nCdRequisicao := SP_FOLLOWUP_REQUISICAOnCdRequisicao.Value;

  showForm(objForm, True);

  PosicionaQuery(qryFollowUp,SP_FOLLOWUP_REQUISICAOnCdRequisicao.AsString);
end;

procedure TfrmFollowUP_Requisicao.cxGridDBTableView3CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;

  qryFollowUp.Close;

  if (SP_FOLLOWUP_REQUISICAOnCdRequisicao.Value > 0) then
      PosicionaQuery(qryFollowUp, SP_FOLLOWUP_REQUISICAOnCdRequisicao.AsString);
end;

procedure TfrmFollowUP_Requisicao.cxGridDBTableView3StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if (AItem = cxGridDBTableView3DBColumn_nCdRequisicao) then
  begin
      try
          if (ARecord.Values[3] <> 'Normal') then
              AStyle := frmMenu.LinhaAmarela;

          if (ARecord.Values[3] = 'Cr�tico') then
              AStyle := frmMenu.LinhaVermelha;
      except
      end;
  end;
end;

initialization
    RegisterClass(TfrmFollowUP_Requisicao) ;

end.
