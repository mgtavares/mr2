inherited frmCadastroMetasDia: TfrmCadastroMetasDia
  Left = 204
  Top = 154
  Width = 928
  Height = 626
  Caption = 'Cadastro e Manuten'#231#227'o de Metas Di'#225'rias'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 920
    Height = 570
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 80
    Width = 36
    Height = 13
    Caption = 'Per'#237'odo'
  end
  object Label2: TLabel [2]
    Left = 128
    Top = 80
    Width = 17
    Height = 13
    Caption = 'At'#233
  end
  object Label3: TLabel [3]
    Left = 24
    Top = 56
    Width = 20
    Height = 13
    Caption = 'Loja'
  end
  inherited ToolBar1: TToolBar
    Width = 920
    TabOrder = 5
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit1: TMaskEdit [5]
    Left = 48
    Top = 72
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [6]
    Left = 152
    Top = 72
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object Edit1: TEdit [7]
    Left = 48
    Top = 48
    Width = 73
    Height = 21
    TabOrder = 0
    OnChange = Edit1Change
    OnKeyDown = Edit1KeyDown
  end
  object DBEdit1: TDBEdit [8]
    Left = 128
    Top = 48
    Width = 273
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    Enabled = False
    TabOrder = 4
  end
  object DBGridEh1: TDBGridEh [9]
    Left = 8
    Top = 112
    Width = 801
    Height = 393
    DataGrouping.GroupLevels = <>
    DataSource = dsMetas
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'data'
        Footers = <>
        Title.Caption = 'Data'
        Width = 96
      end
      item
        EditButtons = <>
        FieldName = 'loja'
        Footers = <>
        Title.Caption = 'Loja'
      end
      item
        EditButtons = <>
        FieldName = 'visitantes'
        Footers = <>
        Title.Caption = 'Visitantes'
      end
      item
        EditButtons = <>
        FieldName = 'tickets'
        Footers = <>
        Title.Caption = 'Tickets'
      end
      item
        EditButtons = <>
        FieldName = 'receita'
        Footers = <>
        Title.Caption = 'Receita de Venda'
        Width = 96
      end
      item
        EditButtons = <>
        FieldName = 'ticketMedio'
        Footers = <>
        Title.Caption = 'Ticket M'#233'dio'
        Width = 75
      end
      item
        EditButtons = <>
        FieldName = 'itensTicket'
        Footers = <>
        Title.Caption = 'Itens p/ Ticket'
        Width = 85
      end
      item
        EditButtons = <>
        FieldName = 'conversao'
        Footers = <>
        Title.Caption = '% Convers'#227'o'
        Width = 77
      end
      item
        EditButtons = <>
        FieldName = 'associacao'
        Footers = <>
        Title.Caption = '% Associa'#231#227'o'
        Width = 83
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object cxButton1: TcxButton [10]
    Left = 8
    Top = 512
    Width = 97
    Height = 33
    Caption = 'Exportar'
    TabOrder = 6
    OnClick = cxButton1Click
    Glyph.Data = {
      66020000424D660200000000000036000000280000000D0000000E0000000100
      18000000000030020000C30E0000C30E00000000000000000000B0B0B03C3C3C
      3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3CB0B0
      B0FF3C3C3CE5E5E5F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1
      F1F1F1E5E5E53C3C3C003C3C3CF1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1
      F1F1F1F1F1F1F1F1F1F1F1F1F1F13C3C3C003C3C3CF1F1F1F1F1F1F1F1F1F1F1
      F1EDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDEDF1F1F13C3C3C003C3C3CF1F1F1
      F1F1F1F1F1F1EDEDEDEDEDEDE8E8E8E8E8E8E8E8E8E8E8E8E8E8E8F1F1F13C3C
      3CFF3C3C3CF1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1F1E8E8E8E5E5E5DCDCDC
      DCDCDCF1F1F13C3C3C003C3C3CF1F1F1F1F1F1F1F1F1F1F1F1F1F1F1EDEDEDE2
      E2E2DCDCDCD3D3D3D1D1D1F1F1F13C3C3C003C3C3CF1F1F1F1F1F1F1F1F1F1F1
      F1EDEDEDE2E2E2D9D9D9D1D1D1CACACAC5C5C5F1F1F13C3C3C003C3C3CF1F1F1
      EDEDEDF1F1F1EDEDEDE2E2E2D9D9D9CECECEC3C3C3BFBFBFB8B8B8F1F1F13C3C
      3CFF3C3C3CF1F1F1E5E5E5E5E5E5DFDFDFDCDCDCD1D1D1C1C1C1F1F1F1F1F1F1
      F1F1F1F1F1F13C3C3C003C3C3CF1F1F1DFDFDFE5E5E5DFDFDFD3D3D3CCCCCCBF
      BFBFF1F1F1DCDCDCD6D6D63C3C3CABABAB003C3C3CEDEDEDD6D6D6D9D9D9D9D9
      D9CECECECACACABDBDBDF1F1F1BABABA3C3C3CABABABF5F5F5003C3C3CE2E2E2
      EDEDEDF1F1F1F1F1F1F1F1F1F1F1F1F1F1F1E2E2E23C3C3CABABABF5F5F5FFFF
      FFFFB3B3B33C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3C3CABABAB
      F5F5F5FFFFFFFFFFFF00}
  end
  inherited ImageList1: TImageList
    Left = 632
    Top = 40
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja INT'
      ''
      'SET @nCdLoja = :nCdLoja'
      ''
      
        'SELECT TOP 1 cNmLoja, nCdLoja  FROM Loja WHERE nCdLoja = @nCdLoj' +
        'a')
    Left = 600
    Top = 40
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 200
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 568
    Top = 40
  end
  object qryMetas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtIni'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtFim'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @loja INT,'
      #9#9'@ini VARCHAR(10),'
      #9#9'@fim VARCHAR(10)'
      ''
      'SET @loja = :nCdLoja'
      ''
      'SET @ini = :dDtIni'
      ''
      'SET @fim = :dDtFim'
      ''
      'SELECT * FROM MetasSeedDia'
      
        'WHERE (([data] > = CONVERT(DATETIME, @ini, 103))OR (@ini = '#39'01/0' +
        '1/1900'#39'))'
      
        'AND(([data] < = CONVERT(DATETIME, @fim, 103))OR (@fim = '#39'01/01/1' +
        '900'#39'))'
      'AND ((loja = @loja) OR (@loja = 0))'
      'ORDER BY [data], loja')
    Left = 600
    Top = 72
    object qryMetasdata: TDateTimeField
      FieldName = 'data'
    end
    object qryMetasloja: TIntegerField
      FieldName = 'loja'
    end
    object qryMetasvisitantes: TIntegerField
      FieldName = 'visitantes'
    end
    object qryMetastickets: TIntegerField
      FieldName = 'tickets'
    end
    object qryMetasreceita: TFloatField
      FieldName = 'receita'
    end
    object qryMetasticketMedio: TFloatField
      FieldName = 'ticketMedio'
    end
    object qryMetasitensTicket: TFloatField
      FieldName = 'itensTicket'
    end
    object qryMetasconversao: TFloatField
      FieldName = 'conversao'
    end
    object qryMetasassociacao: TFloatField
      FieldName = 'associacao'
    end
  end
  object dsMetas: TDataSource
    DataSet = qryMetas
    Left = 568
    Top = 72
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '.csv'
    Filter = 'Arquivo de texto|*.csv'
    InitialDir = 'C:\'
    Left = 632
    Top = 72
  end
end
