unit rTransferencia_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptTransferencia_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText9: TQRDBText;
    QRBand5: TQRBand;
    QRDBText7: TQRDBText;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    qryMaster: TADOQuery;
    QRDBText15: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText19: TQRDBText;
    QRLabel15: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRShape2: TQRShape;
    QRLabel4: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText8: TQRDBText;
    QRLabel7: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel9: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText17: TQRDBText;
    QRLabel12: TQRLabel;
    SummaryBand1: TQRBand;
    QRLabel18: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel17: TQRLabel;
    qryMasternCdTransfEst: TIntegerField;
    qryMasternCdLojaOrigem: TIntegerField;
    qryMastercNmLojaOrigem: TStringField;
    qryMasternCdLojaDestino: TIntegerField;
    qryMastercNmLojaDestino: TStringField;
    qryMasternCdTabTipoTransf: TIntegerField;
    qryMastercNmTabTipoTransf: TStringField;
    qryMasternCdLocalEstoqueOrigem: TIntegerField;
    qryMastercNmLocalOrigem: TStringField;
    qryMasternCdLocalEstoqueDestino: TIntegerField;
    qryMastercNmLocalDestino: TStringField;
    qryMasterdDtFinalizacao: TDateTimeField;
    qryMasternCdProduto: TIntegerField;
    qryMastercEAN: TStringField;
    qryMastercNmProduto: TStringField;
    qryMasternQtde: TBCDField;
    QRExpr1: TQRExpr;
    QRImage1: TQRImage;
    QRLabel14: TQRLabel;
    QRLabel22: TQRLabel;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    qryMasternValTransferencia: TBCDField;
    QRLabel16: TQRLabel;
    QRDBText2: TQRDBText;
    qryMastercUnidadeMedida: TStringField;
    QRDBText6: TQRDBText;
    QRLabel20: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptTransferencia_view: TrptTransferencia_view;

implementation

{$R *.dfm}

end.
