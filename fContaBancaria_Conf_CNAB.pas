unit fContaBancaria_Conf_CNAB;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DBCtrls, ER2Lookup, DB, Mask, ADODB, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, FileCtrl;

type
  TfrmContaBancaria_Conf_CNAB = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    qryCNAB: TADOQuery;
    qryCNABcCedenteAgencia: TStringField;
    qryCNABcCedenteAgenciaDigito: TStringField;
    qryCNABcCedenteConta: TStringField;
    qryCNABcCedenteContaDigito: TStringField;
    qryCNABcCedenteCarteira: TStringField;
    qryCNABcCedenteCNPJCPF: TStringField;
    qryCNABcCedenteNome: TStringField;
    qryCNABcCedenteCodCedente: TStringField;
    qryCNABnCdTabTipoModeloImpBoleto: TIntegerField;
    qryCNABnCdTabTipoRespEmissaoBoleto: TIntegerField;
    qryCNABnCdTabTipoLayOutRemessa: TIntegerField;
    qryCNABiSeqNossoNumero: TIntegerField;
    qryCNABiSeqArquivoRemessa: TIntegerField;
    qryCNABiDiasProtesto: TIntegerField;
    qryCNABcFlgAceite: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    dsCNAB: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    edtModImp: TER2LookupDBEdit;
    edtRespEmissao: TER2LookupDBEdit;
    edtLayoutRem: TER2LookupDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    qryCNABcPrimeiraInstrucaoBoleto: TStringField;
    qryCNABcSegundaInstrucaoBoleto: TStringField;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    cxButton1: TcxButton;
    qryModImp: TADOQuery;
    qryRespEmissao: TADOQuery;
    qryLayoutRem: TADOQuery;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    dsModImp: TDataSource;
    dsRespEmissao: TDataSource;
    dsLayoutRem: TDataSource;
    qryModImpnCdTabTipoModeloImpBoleto: TIntegerField;
    qryModImpcNmTabTipoModeloImpBoleto: TStringField;
    qryRespEmissaonCdTabTipoRespEmissaoBoleto: TIntegerField;
    qryRespEmissaocNmTabTipoRespEmissaoBoleto: TStringField;
    qryLayoutRemnCdTabTipoLayOutRemessa: TIntegerField;
    qryLayoutRemcNmTabTipoLayOutRemessa: TStringField;
    qryCNABnCdContaBancaria: TIntegerField;
    qryCNABcDiretorioArquivoRemessa: TStringField;
    DBEdit18: TDBEdit;
    cxButton2: TcxButton;
    Label19: TLabel;
    qryCNABcDiretorioArquivoLicenca: TStringField;
    OpenDialog1: TOpenDialog;
    procedure cxButton1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure qryCNABBeforePost(DataSet: TDataSet);
    procedure qryCNABAfterScroll(DataSet: TDataSet);
    procedure cxButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ConfigCNAB(nCdContaBancaria : integer);
  end;

var
  frmContaBancaria_Conf_CNAB: TfrmContaBancaria_Conf_CNAB;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmContaBancaria_Conf_CNAB.ConfigCNAB(
  nCdContaBancaria: integer);
begin

    qryCNAB.Close;
    PosicionaQuery(qryCNAB, IntToStr(nCdContaBancaria));

    qryCNAB.Edit;
    Self.ShowModal;
end;

procedure TfrmContaBancaria_Conf_CNAB.cxButton1Click(Sender: TObject);
var
  cDiretorio : String;
begin
  inherited;

  cDiretorio := qryCNABcDiretorioArquivoRemessa.Value;

  SelectDirectory('Selecione o Diret�rio do arquivo de Remessa','',cDiretorio);

  if (cDiretorio <> '') then
  begin

      if (qryCNAB.State <> dsEdit) then
          qryCNAB.Edit;

      qryCNABcDiretorioArquivoRemessa.Value := cDiretorio;
  end;

end;

procedure TfrmContaBancaria_Conf_CNAB.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryCNAB.Post;
  Close;
end;

procedure TfrmContaBancaria_Conf_CNAB.qryCNABBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (Trim(qryCNABcCedenteAgencia.Value) = '') then
  begin
      MensagemAlerta('Digite a ag�ncia.');
      DBEdit1.SetFocus;
      Abort;
  end;

  if (Trim(qryCNABcCedenteAgenciaDigito.Value) = '') then
  begin
      MensagemAlerta('Informe o Digito da ag�ncia.');
      DBEdit2.SetFocus;
      Abort;
  end;

  if (Trim(qryCNABcCedenteConta.Value) = '') then
  begin
      MensagemAlerta('Digite a Conta.');
      DBEdit3.SetFocus;
      Abort;
  end;

  if (Trim(qryCNABcCedenteContaDigito.Value) = '') then
  begin
      MensagemAlerta('Informe o digito da conta.');
      DBEdit4.SetFocus;
      Abort;
  end;

  if (Trim(qryCNABcCedenteCodCedente.Value) = '') then
  begin
      MensagemAlerta('Informe o C�digo do Cedente.');
      DBEdit5.SetFocus;
      Abort;
  end;

  if (Trim(qryCNABcCedenteCarteira.Value) = '') then
  begin
      MensagemAlerta('Inform o C�digo da Carteira.');
      DBEdit6.SetFocus;
      Abort;
  end;

  if (frmMenu.TestaCpfCgc(Trim(qryCNABcCedenteCNPJCPF.Value)) = '') then
  begin
      DBEdit7.SetFocus;
      Abort;
  end;

  if (Trim(qryCNABcCedenteNome.Value) = '') then
  begin
      MensagemAlerta('Digite o Nome do Titular.');
      DBEdit8.SetFocus;
      Abort;
  end;

  if (qryCNABnCdTabTipoModeloImpBoleto.Value = 0) then
  begin
      MensagemAlerta('Informe o tipo do Modelo de Impress�o do Boleto.');
      edtModImp.SetFocus;
      Abort;
  end;

  if (qryCNABnCdTabTipoRespEmissaoBoleto.Value = 0) then
  begin
      MensagemAlerta('Informe o Respons�vel pela Emiss�o do Boleto.');
      edtRespEmissao.SetFocus;
      Abort;
  end;

  if (qryCNABnCdTabTipoLayOutRemessa.Value = 0) then
  begin
      MensagemAlerta('Informe o Tipo de Layout da Remessa.');
      edtLayoutRem.SetFocus;
      Abort;
  end;

  if (   ((qryCNABnCdTabTipoModeloImpBoleto.Value <> 3) and (qryCNABnCdTabTipoRespEmissaoBoleto.Value  = 2))
      or ((qryCNABnCdTabTipoModeloImpBoleto.Value  = 3) and (qryCNABnCdTabTipoRespEmissaoBoleto.Value <> 2))) then
  begin
      MensagemAlerta('Modelo de Impress�o inv�lido para o Respons�vel pela Emiss�o.');
      edtModImp.SetFocus;
      Abort;
  end;

end;

procedure TfrmContaBancaria_Conf_CNAB.qryCNABAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryModImp,qryCNABnCdTabTipoModeloImpBoleto.AsString);
  PosicionaQuery(qryRespEmissao,qryCNABnCdTabTipoRespEmissaoBoleto.AsString);
  PosicionaQuery(qryLayoutRem,qryCNABnCdTabTipoLayOutRemessa.AsString);
end;

procedure TfrmContaBancaria_Conf_CNAB.cxButton2Click(Sender: TObject);
var
  cDiretorio : String;
begin
  inherited;

  cDiretorio := qryCNABcDiretorioArquivoLicenca.Value;

  OpenDialog1.Filter := 'Arquivo de Licen�a(.conf)|*.conf';

  OpenDialog1.Execute;

  cDiretorio := OpenDialog1.FileName;

  if (cDiretorio <> '') then
  begin

      if (qryCNAB.State <> dsEdit) then
          qryCNAB.Edit;

      qryCNABcDiretorioArquivoLicenca.Value := cDiretorio;
  end;

end;

end.
