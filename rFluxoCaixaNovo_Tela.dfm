inherited rptFluxoCaixaNovo_Tela: TrptFluxoCaixaNovo_Tela
  Left = 4
  Top = -1
  Width = 1279
  Height = 769
  Caption = 'Fluxo de Caixa - Visualiza'#231#227'o em Tela'
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 182
    Width = 934
    Height = 234
    Visible = False
  end
  inherited ToolBar1: TToolBar
    Width = 1263
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 934
    Top = 182
    Width = 329
    Height = 234
    Align = alRight
    Caption = 'Resumo'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 15
      Top = 24
      Width = 143
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total de Atrasados a Receber'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Tag = 1
      Left = 27
      Top = 48
      Width = 131
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total de Atrasados a Pagar'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Tag = 1
      Left = 61
      Top = 72
      Width = 97
      Height = 13
      Alignment = taRightJustify
      Caption = 'Disponibilidade Total'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Tag = 1
      Left = 43
      Top = 96
      Width = 115
      Height = 13
      Alignment = taRightJustify
      Caption = 'Disponibilidade Imediata'
      FocusControl = DBEdit4
    end
    object Label5: TLabel
      Tag = 1
      Left = 82
      Top = 120
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total a Receber'
      FocusControl = DBEdit5
    end
    object Label6: TLabel
      Tag = 1
      Left = 94
      Top = 144
      Width = 64
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total a Pagar'
      FocusControl = DBEdit6
    end
    object Label7: TLabel
      Tag = 1
      Left = 27
      Top = 168
      Width = 131
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo sem Limite de Cr'#233'dito'
      FocusControl = DBEdit7
    end
    object Label8: TLabel
      Tag = 1
      Left = 78
      Top = 192
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Limite de Cr'#233'dito'
      FocusControl = DBEdit8
    end
    object Label9: TLabel
      Tag = 1
      Left = 105
      Top = 216
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo Total'
      FocusControl = DBEdit9
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 164
      Top = 16
      Width = 125
      Height = 21
      DataField = 'nValTotalAtrasReceber'
      DataSource = dsExpectativaCaixa
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 164
      Top = 40
      Width = 125
      Height = 21
      DataField = 'nValTotalAtrasPagar'
      DataSource = dsExpectativaCaixa
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 164
      Top = 64
      Width = 125
      Height = 21
      DataField = 'nValDispTotal'
      DataSource = dsExpectativaCaixa
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 164
      Top = 88
      Width = 125
      Height = 21
      DataField = 'nValDispImediata'
      DataSource = dsExpectativaCaixa
      TabOrder = 3
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 164
      Top = 112
      Width = 125
      Height = 21
      DataField = 'nValTotalReceber'
      DataSource = dsExpectativaCaixa
      TabOrder = 4
    end
    object DBEdit6: TDBEdit
      Tag = 1
      Left = 164
      Top = 136
      Width = 125
      Height = 21
      DataField = 'nValTotalPagar'
      DataSource = dsExpectativaCaixa
      TabOrder = 5
    end
    object DBEdit7: TDBEdit
      Tag = 1
      Left = 164
      Top = 160
      Width = 125
      Height = 21
      DataField = 'nValSaldoSemLimCred'
      DataSource = dsExpectativaCaixa
      TabOrder = 6
    end
    object DBEdit8: TDBEdit
      Tag = 1
      Left = 164
      Top = 184
      Width = 125
      Height = 21
      DataField = 'nValLimiteCred'
      DataSource = dsExpectativaCaixa
      TabOrder = 7
    end
    object DBEdit9: TDBEdit
      Tag = 1
      Left = 164
      Top = 208
      Width = 125
      Height = 21
      DataField = 'nSaldoTotal'
      DataSource = dsExpectativaCaixa
      TabOrder = 8
    end
  end
  object GroupBox2: TGroupBox [3]
    Left = 0
    Top = 416
    Width = 1263
    Height = 317
    Align = alBottom
    Caption = 'Movimenta'#231#227'o Anal'#237'tica'
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 2
      Top = 15
      Width = 1259
      Height = 300
      Align = alClient
      TabOrder = 0
      LookAndFeel.NativeStyle = True
      object cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dsMovAnalitico
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        object cxGrid1DBTableView1dDtInicial: TcxGridDBColumn
          DataBinding.FieldName = 'dDtInicial'
          Visible = False
        end
        object cxGrid1DBTableView1dDtFinal: TcxGridDBColumn
          DataBinding.FieldName = 'dDtFinal'
          Visible = False
        end
        object cxGrid1DBTableView1iPeriodo: TcxGridDBColumn
          DataBinding.FieldName = 'iPeriodo'
          Visible = False
        end
        object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
          Caption = 'Dt. Vencimento'
          DataBinding.FieldName = 'dDtVenc'
        end
        object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
          Caption = 'C'#243'd. Titulo'
          DataBinding.FieldName = 'nCdTitulo'
          Width = 83
        end
        object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
          Caption = 'N'#250'm. Titulo'
          DataBinding.FieldName = 'cNrTit'
        end
        object cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn
          Caption = 'Esp. Titulo'
          DataBinding.FieldName = 'cNmEspTit'
          Width = 186
        end
        object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
          Caption = 'Terceiro'
          DataBinding.FieldName = 'cNmTerceiro'
          Width = 221
        end
        object cxGrid1DBTableView1cNmCategFinanc: TcxGridDBColumn
          Caption = 'Categ. Financeira'
          DataBinding.FieldName = 'cNmCategFinanc'
          Width = 187
        end
        object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
          Caption = 'Saldo'
          DataBinding.FieldName = 'nSaldoTit'
        end
        object cxGrid1DBTableView1nValTotalPagar: TcxGridDBColumn
          DataBinding.FieldName = 'nValTotalPagar'
          Visible = False
        end
        object cxGrid1DBTableView1nValTotalReceber: TcxGridDBColumn
          DataBinding.FieldName = 'nValTotalReceber'
          Visible = False
        end
        object cxGrid1DBTableView1cSenso: TcxGridDBColumn
          Caption = 'Senso'
          DataBinding.FieldName = 'cSenso'
          Width = 58
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object GroupBox4: TGroupBox [4]
    Left = 0
    Top = 29
    Width = 1263
    Height = 153
    Align = alTop
    Caption = 'Posi'#231#227'o de Caixa Atual'
    TabOrder = 3
    object DBGridEh2: TDBGridEh
      Left = 2
      Top = 15
      Width = 1259
      Height = 136
      Align = alClient
      AllowedOperations = []
      DataGrouping.GroupLevels = <>
      DataSource = dsPosCaixaAtual
      Flat = False
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'Tahoma'
      FooterFont.Style = []
      FooterRowCount = 1
      ReadOnly = True
      RowDetailPanel.Color = clBtnFace
      SumList.Active = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      UseMultiTitle = True
      Columns = <
        item
          EditButtons = <>
          FieldName = 'cEmpresaLoja'
          Footers = <>
          Title.Caption = 'Empresa/Loja'
          Width = 85
        end
        item
          EditButtons = <>
          FieldName = 'cBanco'
          Footers = <>
          Title.Caption = 'Conta|Banco'
        end
        item
          EditButtons = <>
          FieldName = 'cAgencia'
          Footers = <>
          Title.Caption = 'Conta|Ag'#234'ncia'
          Width = 89
        end
        item
          EditButtons = <>
          FieldName = 'nCdConta'
          Footers = <>
          Title.Caption = 'Conta|Conta'
        end
        item
          EditButtons = <>
          FieldName = 'nSaldoConta'
          Footers = <>
          Title.Caption = 'Total|Saldo'
        end
        item
          EditButtons = <>
          FieldName = 'dDtSaldo'
          Footers = <>
          Title.Caption = 'Total|Dt. Saldo'
        end
        item
          EditButtons = <>
          FieldName = 'nValLimite'
          Footers = <>
          Title.Caption = 'Total|Limite Cr'#233'dito'
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object GroupBox3: TGroupBox [5]
    Left = 0
    Top = 182
    Width = 934
    Height = 234
    Align = alClient
    Caption = 'Per'#237'odos de Movimenta'#231#227'o'
    TabOrder = 4
    object DBGridEh1: TDBGridEh
      Left = 2
      Top = 15
      Width = 930
      Height = 217
      Align = alClient
      AllowedOperations = []
      DataGrouping.GroupLevels = <>
      DataSource = dsDemonstraMovFutura
      Flat = False
      FooterColor = clWindow
      FooterFont.Charset = DEFAULT_CHARSET
      FooterFont.Color = clWindowText
      FooterFont.Height = -11
      FooterFont.Name = 'Tahoma'
      FooterFont.Style = []
      FooterRowCount = 1
      ReadOnly = True
      RowDetailPanel.Color = clBtnFace
      SumList.Active = True
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = []
      UseMultiTitle = True
      Columns = <
        item
          EditButtons = <>
          FieldName = 'cPeriodo'
          Footers = <>
          Title.Caption = 'Periodo'
          Width = 173
        end
        item
          EditButtons = <>
          FieldName = 'nValTotalReceber'
          Footers = <>
          Title.Caption = 'Total|Receber'
          Width = 137
        end
        item
          EditButtons = <>
          FieldName = 'nValTotalPagar'
          Footers = <>
          Title.Caption = 'Total|Pagar'
          Width = 96
        end
        item
          EditButtons = <>
          FieldName = 'nValSaldoAcumulado'
          Footer.Font.Charset = DEFAULT_CHARSET
          Footer.Font.Color = clWindow
          Footer.Font.Height = -11
          Footer.Font.Name = 'Tahoma'
          Footer.Font.Style = []
          Footers = <>
          Title.Caption = 'Total|Saldo Acumulado'
          Width = 135
        end>
      object RowDetailData: TRowDetailPanelControlEh
      end
    end
  end
  object qryMovAnalitico: TADOQuery
    Connection = frmMenu.Connection
    Filtered = True
    Parameters = <>
    SQL.Strings = (
      '    SELECT *'
      '      FROM #TempMovAnalitico'
      '     ORDER BY dDtVenc')
    Left = 288
    Top = 496
    object qryMovAnaliticodDtInicial: TDateTimeField
      FieldName = 'dDtInicial'
    end
    object qryMovAnaliticodDtFinal: TDateTimeField
      FieldName = 'dDtFinal'
    end
    object qryMovAnaliticoiPeriodo: TIntegerField
      FieldName = 'iPeriodo'
    end
    object qryMovAnaliticodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryMovAnaliticonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryMovAnaliticocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryMovAnaliticocNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryMovAnaliticocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMovAnaliticocNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object qryMovAnaliticonSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMovAnaliticonValTotalPagar: TBCDField
      FieldName = 'nValTotalPagar'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMovAnaliticonValTotalReceber: TBCDField
      FieldName = 'nValTotalReceber'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMovAnaliticocSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
  end
  object qryExpectativaCaixa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      '    SELECT *'
      '      FROM #TempExpectativaCaixa')
    Left = 952
    Top = 376
    object qryExpectativaCaixanValTotalAtrasReceber: TBCDField
      FieldName = 'nValTotalAtrasReceber'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValTotalAtrasPagar: TBCDField
      FieldName = 'nValTotalAtrasPagar'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValDispTotal: TBCDField
      FieldName = 'nValDispTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValDispImediata: TBCDField
      FieldName = 'nValDispImediata'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValTotalReceber: TBCDField
      FieldName = 'nValTotalReceber'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValTotalPagar: TBCDField
      FieldName = 'nValTotalPagar'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValSaldoSemLimCred: TBCDField
      FieldName = 'nValSaldoSemLimCred'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanValLimiteCred: TBCDField
      FieldName = 'nValLimiteCred'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryExpectativaCaixanSaldoTotal: TBCDField
      FieldName = 'nSaldoTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object qryDemonstraMovFutura: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryDemonstraMovFuturaAfterScroll
    OnCalcFields = qryDemonstraMovFuturaCalcFields
    Parameters = <>
    SQL.Strings = (
      '    SELECT *'
      '      FROM #TempDemonstraMovFutura')
    Left = 280
    Top = 264
    object qryDemonstraMovFuturadDtInicial: TDateTimeField
      FieldName = 'dDtInicial'
    end
    object qryDemonstraMovFuturadDtFinal: TDateTimeField
      FieldName = 'dDtFinal'
    end
    object qryDemonstraMovFuturanValTotalReceber: TBCDField
      FieldName = 'nValTotalReceber'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDemonstraMovFuturanValTotalPagar: TBCDField
      FieldName = 'nValTotalPagar'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDemonstraMovFuturanValSaldoAcumulado: TBCDField
      FieldName = 'nValSaldoAcumulado'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDemonstraMovFuturanValTotalReceberPeriodo: TBCDField
      FieldName = 'nValTotalReceberPeriodo'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDemonstraMovFuturanValTotalPagarPeriodo: TBCDField
      FieldName = 'nValTotalPagarPeriodo'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDemonstraMovFuturacPeriodo: TStringField
      FieldKind = fkCalculated
      FieldName = 'cPeriodo'
      Size = 25
      Calculated = True
    end
  end
  object qryPosCaixaAtual: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      '    SELECT *'
      '      FROM #TempPosCaixaAtual')
    Left = 288
    Top = 80
    object qryPosCaixaAtualcEmpresaLoja: TStringField
      FieldName = 'cEmpresaLoja'
      Size = 7
    end
    object qryPosCaixaAtualcBanco: TStringField
      FieldName = 'cBanco'
      Size = 54
    end
    object qryPosCaixaAtualcAgencia: TStringField
      FieldName = 'cAgencia'
      Size = 4
    end
    object qryPosCaixaAtualnCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryPosCaixaAtualnSaldoConta: TBCDField
      FieldName = 'nSaldoConta'
      Precision = 12
      Size = 2
    end
    object qryPosCaixaAtualdDtSaldo: TDateTimeField
      FieldName = 'dDtSaldo'
    end
    object qryPosCaixaAtualnValLimite: TBCDField
      FieldName = 'nValLimite'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryPosCaixaAtualnSaldoTotal: TBCDField
      FieldName = 'nSaldoTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryPosCaixaAtualnValLimiteTotal: TBCDField
      FieldName = 'nValLimiteTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsExpectativaCaixa: TDataSource
    DataSet = qryExpectativaCaixa
    Left = 968
    Top = 408
  end
  object dsPosCaixaAtual: TDataSource
    DataSet = qryPosCaixaAtual
    Left = 304
    Top = 112
  end
  object dsDemonstraMovFutura: TDataSource
    DataSet = qryDemonstraMovFutura
    Left = 296
    Top = 296
  end
  object dsMovAnalitico: TDataSource
    DataSet = qryMovAnalitico
    Left = 304
    Top = 528
  end
end
