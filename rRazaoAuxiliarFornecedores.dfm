inherited rptRazaoAuxiliarFornecedores: TrptRazaoAuxiliarFornecedores
  Left = 217
  Top = 174
  Caption = 'Raz'#227'o Auxiliar Fornecedores'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 56
    Top = 54
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label2: TLabel [2]
    Left = 15
    Top = 80
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Econ'#244'mico'
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label3: TLabel [3]
    Left = 58
    Top = 106
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label8: TLabel [4]
    Left = 61
    Top = 133
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo'
  end
  object Label11: TLabel [5]
    Left = 187
    Top = 133
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtEmpresa: TER2LookupMaskEdit [7]
    Left = 104
    Top = 46
    Width = 63
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    OnBeforePosicionaQry = edtEmpresaBeforePosicionaQry
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  object DBEdit2: TDBEdit [8]
    Tag = 1
    Left = 176
    Top = 46
    Width = 67
    Height = 21
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 2
  end
  object DBEdit1: TDBEdit [9]
    Tag = 1
    Left = 248
    Top = 46
    Width = 652
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 3
  end
  object edtGrupoEconomico: TER2LookupMaskEdit [10]
    Left = 104
    Top = 72
    Width = 63
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 4
    Text = '         '
    CodigoLookup = 94
    QueryLookup = qryGrupoEconomico
  end
  object DBEdit3: TDBEdit [11]
    Tag = 1
    Left = 176
    Top = 72
    Width = 652
    Height = 21
    DataField = 'cNmGrupoEconomico'
    DataSource = dsGrupoEconomico
    TabOrder = 5
  end
  object edtTerceiro: TER2LookupMaskEdit [12]
    Left = 104
    Top = 98
    Width = 63
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 6
    Text = '         '
    CodigoLookup = 17
    WhereAdicional.Strings = (
      
        'EXISTS (SELECT nCdTerceiro FROM TerceiroTipoTerceiro WHERE Terce' +
        'iroTipoTerceiro.nCdTerceiro = vTerceiros.nCdTerceiro AND Terceir' +
        'oTipoTerceiro.nCdTipoTerceiro=1)')
    QueryLookup = qryTerceiro
  end
  object DBEdit4: TDBEdit [13]
    Tag = 1
    Left = 176
    Top = 98
    Width = 652
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 7
  end
  object MaskEditDataInicial: TMaskEdit [14]
    Left = 104
    Top = 125
    Width = 70
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
  end
  object MaskEditDataFinal: TMaskEdit [15]
    Left = 215
    Top = 125
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 9
    Text = '  /  /    '
  end
  object RadioGroupTipoSaida: TRadioGroup [16]
    Left = 17
    Top = 154
    Width = 185
    Height = 40
    Caption = 'Tipo de Sa'#237'da'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Anal'#237'tico'
      'Sint'#233'tico')
    TabOrder = 10
  end
  object RadioGroupSaida: TRadioGroup [17]
    Left = 209
    Top = 154
    Width = 265
    Height = 40
    Caption = 'Modo de Exibi'#231#227'o'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Relat'#243'rio'
      'Planilha'
      'Tela')
    TabOrder = 11
  end
  inherited ImageList1: TImageList
    Left = 760
    Top = 168
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nPK')
    Left = 880
    Top = 88
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 912
    Top = 88
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdGrupoEconomico, cNmGrupoEconomico'
      'FROM GrupoEconomico'
      'WHERE nCdGrupoEconomico = :nPK')
    Left = 880
    Top = 124
    object qryGrupoEconomiconCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object dsGrupoEconomico: TDataSource
    DataSet = qryGrupoEconomico
    Left = 912
    Top = 124
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro   '
      'WHERE nCdTerceiro = :nPk'
      'and'
      
        'EXISTS (SELECT nCdTerceiro FROM TerceiroTipoTerceiro WHERE Terce' +
        'iroTipoTerceiro.nCdTerceiro = Terceiro.nCdTerceiro AND TerceiroT' +
        'ipoTerceiro.nCdTipoTerceiro=1)'
      '')
    Left = 880
    Top = 161
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 912
    Top = 161
  end
  object ER2Excel1: TER2Excel
    Titulo = 'Di'#225'rio Auxiliar de Clientes'
    AutoSizeCol = True
    Background = clWhite
    Transparent = False
    Left = 728
    Top = 184
  end
end
