inherited frmLojaOperadoraCartaoTaxaJuros: TfrmLojaOperadoraCartaoTaxaJuros
  Width = 284
  Caption = 'Taxa de Juros por Parcelas'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 268
  end
  inherited ToolBar1: TToolBar
    Width = 268
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 268
    Height = 435
    Align = alClient
    DataSource = dsLojaOperadoraCartaoTaxaJuros
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'iParcelas'
        Footers = <>
        Title.Caption = '(%) Taxa de Juros por Parcelas|Parcelas'
      end
      item
        EditButtons = <>
        FieldName = 'nTaxaJuroOperadora'
        Footers = <>
        Title.Caption = '(%) Taxa de Juros por Parcelas|Taxa de Juros'
        Width = 105
      end>
  end
  inherited ImageList1: TImageList
    Left = 128
    Top = 232
  end
  object qryLojaOperadoraCartaoTaxaJuros: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryLojaOperadoraCartaoTaxaJurosBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM LojaOperadoraCartaoTaxaJuros '
      ' WHERE nCdLojaOperadoraCartao =:nPK')
    Left = 40
    Top = 152
    object qryLojaOperadoraCartaoTaxaJurosnCdLojaOperadoraCartao: TIntegerField
      FieldName = 'nCdLojaOperadoraCartao'
    end
    object qryLojaOperadoraCartaoTaxaJurosiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryLojaOperadoraCartaoTaxaJurosnTaxaJuroOperadora: TBCDField
      FieldName = 'nTaxaJuroOperadora'
      Precision = 12
    end
    object qryLojaOperadoraCartaoTaxaJurosnCdLojaOperadoraCartaoTaxaJuros: TIntegerField
      FieldName = 'nCdLojaOperadoraCartaoTaxaJuros'
    end
  end
  object dsLojaOperadoraCartaoTaxaJuros: TDataSource
    DataSet = qryLojaOperadoraCartaoTaxaJuros
    Left = 88
    Top = 200
  end
end
