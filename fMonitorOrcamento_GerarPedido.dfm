inherited frmMonitorOrcamento_GerarPedido: TfrmMonitorOrcamento_GerarPedido
  Left = 335
  Top = 186
  Width = 515
  Height = 428
  BorderIcons = [biSystemMenu]
  Caption = 'Gera'#231#227'o de Pedido'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 499
    Height = 363
  end
  object Label1: TLabel [1]
    Left = 23
    Top = 48
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Pedido'
  end
  object Label3: TLabel [2]
    Left = 11
    Top = 72
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Previs'#227'o Entrega'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [3]
    Left = 184
    Top = 72
    Width = 6
    Height = 13
    Caption = 'a'
    FocusControl = DBEdit3
  end
  object Label5: TLabel [4]
    Left = 10
    Top = 120
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pedido do Cliente'
    FocusControl = DBEdit4
  end
  object Label6: TLabel [5]
    Left = 35
    Top = 136
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observa'#231#227'o'
    FocusControl = DBMemo1
  end
  object Label7: TLabel [6]
    Left = 8
    Top = 264
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Anota'#231#227'o T'#233'cnica'
    FocusControl = DBMemo2
  end
  object Label2: TLabel [7]
    Left = 45
    Top = 96
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Incoterms'
    FocusControl = DBEdit6
  end
  object Label8: TLabel [8]
    Left = 280
    Top = 120
    Width = 94
    Height = 13
    Caption = 'Valor Adiantamento'
    FocusControl = DBEdit9
  end
  inherited ToolBar1: TToolBar
    Width = 499
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit1: TDBEdit [10]
    Left = 96
    Top = 40
    Width = 65
    Height = 21
    DataField = 'nCdTipoPedido'
    DataSource = DataSource1
    TabOrder = 1
    OnExit = DBEdit1Exit
    OnKeyDown = DBEdit1KeyDown
  end
  object DBEdit2: TDBEdit [11]
    Left = 96
    Top = 64
    Width = 81
    Height = 21
    DataField = 'dDtPrevEntregaIni'
    DataSource = DataSource1
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [12]
    Left = 192
    Top = 64
    Width = 81
    Height = 21
    DataField = 'dDtPrevEntregaFim'
    DataSource = DataSource1
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [13]
    Left = 96
    Top = 112
    Width = 145
    Height = 21
    DataField = 'cNrPedTerceiro'
    DataSource = DataSource1
    TabOrder = 5
  end
  object DBMemo1: TDBMemo [14]
    Left = 96
    Top = 136
    Width = 393
    Height = 121
    DataField = 'cOBS'
    DataSource = DataSource1
    TabOrder = 7
  end
  object DBMemo2: TDBMemo [15]
    Left = 96
    Top = 264
    Width = 393
    Height = 121
    DataField = 'cAnotacao'
    DataSource = DataSource1
    TabOrder = 8
  end
  object DBEdit5: TDBEdit [16]
    Tag = 1
    Left = 168
    Top = 40
    Width = 321
    Height = 21
    DataField = 'cNmTipoPedido'
    DataSource = DataSource2
    TabOrder = 9
  end
  object DBEdit6: TDBEdit [17]
    Left = 96
    Top = 88
    Width = 65
    Height = 21
    DataField = 'nCdIncoterms'
    DataSource = DataSource1
    TabOrder = 4
    OnExit = DBEdit6Exit
    OnKeyDown = DBEdit6KeyDown
  end
  object DBEdit7: TDBEdit [18]
    Tag = 1
    Left = 168
    Top = 88
    Width = 43
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource3
    TabOrder = 10
  end
  object DBEdit8: TDBEdit [19]
    Tag = 1
    Left = 216
    Top = 88
    Width = 273
    Height = 21
    DataField = 'cNmIncoterms'
    DataSource = DataSource3
    TabOrder = 11
  end
  object DBEdit9: TDBEdit [20]
    Left = 376
    Top = 112
    Width = 113
    Height = 21
    DataField = 'nValAdiantamento'
    DataSource = DataSource1
    TabOrder = 6
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoPedido'
      '      ,cNmTipoPedido'
      '  FROM TipoPedido'
      ' WHERE cFlgVenda = 1'
      'AND nCdTipoPedido = :nPK')
    Left = 408
    Top = 144
    object qryTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Aprova_Orcamento'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #Temp_Aprova_Orcamento (nCdTipoPedido     int'
      '                                     ,nCdIncoterms      int'
      '                                     ,dDtPrevEntregaIni datetime'
      '                                     ,dDtPrevEntregaFim datetime'
      
        '                                     ,cNrPedTerceiro    varchar(' +
        '15)'
      '                                     ,cOBS              text'
      '                                     ,cAnotacao         text'
      
        '                                     ,nValAdiantamento  decimal(' +
        '12,2))'
      ''
      'END'
      ''
      'INSERT INTO #Temp_Aprova_Orcamento (dDtPrevEntregaIni'
      '                                    ,dDtPrevEntregaFim'
      '                                    ,cNrPedTerceiro'
      '                                    ,cOBS'
      '                                    ,cAnotacao'
      '                                    ,nCdIncoterms)'
      '                              SELECT dDtPrevEntIni'
      '                                    ,dDtPrevEntFim'
      '                                    ,cNrPedTerceiro'
      '                                    ,cOBS'
      '                                    ,cAnotacao'
      '                                    ,nCdIncoterms'
      '                                FROM Pedido'
      '                               WHERE nCdPedido = :nPK')
    Left = 472
    Top = 144
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Aprova_Orcamento'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #Temp_Aprova_Orcamento (nCdTipoPedido     int'
      '                                         ,nCdIncoterms      int'
      
        '                                         ,dDtPrevEntregaIni date' +
        'time'
      
        '                                         ,dDtPrevEntregaFim date' +
        'time'
      
        '                                         ,cNrPedTerceiro    varc' +
        'har(15)'
      '                                         ,cOBS              text'
      '                                         ,cAnotacao         text'
      
        '                                         ,nValAdiantamento  deci' +
        'mal(12,2))'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #Temp_Aprova_Orcamento')
    Left = 472
    Top = 184
    object qryTempnCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTempdDtPrevEntregaIni: TDateTimeField
      FieldName = 'dDtPrevEntregaIni'
      EditMask = '!99/99/9999;1;_'
    end
    object qryTempdDtPrevEntregaFim: TDateTimeField
      FieldName = 'dDtPrevEntregaFim'
      EditMask = '!99/99/9999;1;_'
    end
    object qryTempcNrPedTerceiro: TStringField
      FieldName = 'cNrPedTerceiro'
      Size = 15
    end
    object qryTempcOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
    object qryTempcAnotacao: TMemoField
      FieldName = 'cAnotacao'
      BlobType = ftMemo
    end
    object qryTempnCdIncoterms: TIntegerField
      FieldName = 'nCdIncoterms'
    end
    object qryTempnValAdiantamento: TBCDField
      FieldName = 'nValAdiantamento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTemp
    Left = 384
    Top = 240
  end
  object DataSource2: TDataSource
    DataSet = qryTipoPedido
    Left = 392
    Top = 248
  end
  object qryIncoterms: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM Incoterms'
      ' WHERE nCdIncoterms = :nPK')
    Left = 344
    Top = 200
    object qryIncotermsnCdIncoterms: TIntegerField
      FieldName = 'nCdIncoterms'
    end
    object qryIncotermscSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 3
    end
    object qryIncotermscNmIncoterms: TStringField
      FieldName = 'cNmIncoterms'
      Size = 50
    end
    object qryIncotermscFlgPagador: TStringField
      FieldName = 'cFlgPagador'
      FixedChar = True
      Size = 1
    end
  end
  object DataSource3: TDataSource
    DataSet = qryIncoterms
    Left = 248
    Top = 216
  end
  object SP_APROVA_ORCAMENTO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_APROVA_ORCAMENTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 48
    Top = 304
  end
  object cmdTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#Temp_Aprova_Orcamento'#39') IS NULL)'#13#10'BEGIN'#13 +
      #10#13#10'    CREATE TABLE #Temp_Aprova_Orcamento (nCdTipoPedido     in' +
      't'#13#10'                                     ,nCdIncoterms      int'#13#10 +
      '                                     ,dDtPrevEntregaIni datetime' +
      #13#10'                                     ,dDtPrevEntregaFim dateti' +
      'me'#13#10'                                     ,cNrPedTerceiro    varc' +
      'har(15)'#13#10'                                     ,cOBS             ' +
      ' text'#13#10'                                     ,cAnotacao         t' +
      'ext'#13#10'                                     ,nValAdiantamento  dec' +
      'imal(12,2))'#13#10#13#10'END'#13#10
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 40
    Top = 184
  end
end
