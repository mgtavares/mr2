unit rPosTituloAberto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosTituloAberto = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText6: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText9: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText11: TQRDBText;
    usp_RelatorionCdTitulo: TIntegerField;
    usp_RelatorionCdEspTit: TIntegerField;
    usp_RelatoriocNmEspTit: TStringField;
    usp_RelatorionCdSP: TIntegerField;
    usp_RelatoriocNrTit: TStringField;
    usp_RelatorionCdTerceiro: TIntegerField;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatorionCdMoeda: TIntegerField;
    usp_RelatoriocSigla: TStringField;
    usp_RelatoriocSenso: TStringField;
    usp_RelatoriodDtVenc: TDateTimeField;
    usp_RelatorionValTit: TFloatField;
    usp_RelatorionSaldoTit: TFloatField;
    usp_RelatoriodDtCancel: TDateTimeField;
    usp_RelatorionValJuro: TFloatField;
    QRDBText12: TQRDBText;
    QRLabel18: TQRLabel;
    usp_RelatorioiDias: TIntegerField;
    QRDBText10: TQRDBText;
    QRLabel15: TQRLabel;
    QRLabel19: TQRLabel;
    QRDBText13: TQRDBText;
    usp_RelatorionCdBancoPortador: TIntegerField;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    usp_RelatorionValVencer: TFloatField;
    usp_RelatorionValVencido: TFloatField;
    QRLabel6: TQRLabel;
    QRDBText4: TQRDBText;
    QRExpr7: TQRExpr;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    usp_RelatoriocFlgPrev: TIntegerField;
    QRShape3: TQRShape;
    QRExpr8: TQRExpr;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    QRShape6: TQRShape;
    usp_RelatorioiParcela: TIntegerField;
    QRDBText14: TQRDBText;
    QRLabel25: TQRLabel;
    QRDBText15: TQRDBText;
    usp_RelatorionCdLojaTit: TIntegerField;
    QRDBText16: TQRDBText;
    QRLabel26: TQRLabel;
    QRDBText17: TQRDBText;
    QRLabel27: TQRLabel;
    usp_RelatorionCdEmpresa: TIntegerField;
    usp_RelatoriocNrNf: TStringField;
    QRLabel28: TQRLabel;
    QRDBText18: TQRDBText;
    QRLabel24: TQRLabel;
    QRShape7: TQRShape;
    QRShape5: TQRShape;
    QRLabel14: TQRLabel;
    QRLabel71: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosTituloAberto: TrptPosTituloAberto;

implementation

{$R *.dfm}

end.
