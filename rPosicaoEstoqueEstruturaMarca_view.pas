unit rPosicaoEstoqueEstruturaMarca_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosicaoEstoqueEstruturaMarca_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCA: TADOStoredProc;
    SummaryBand1: TQRBand;
    QRShape6: TQRShape;
    QRExpr3: TQRExpr;
    QRLabel20: TQRLabel;
    QRBand7: TQRBand;
    QRExpr6: TQRExpr;
    QRDBText30: TQRDBText;
    QRDBText25: TQRDBText;
    QRLabel33: TQRLabel;
    QRDBText26: TQRDBText;
    QRLabel34: TQRLabel;
    QRDBText27: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText28: TQRDBText;
    QRLabel37: TQRLabel;
    QRExpr7: TQRExpr;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRExpr4: TQRExpr;
    QRLabel4: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCAcNmDepartamento: TStringField;
    SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCAcNmCategoria: TStringField;
    SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCAcNmSubCategoria: TStringField;
    SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCAcNmSegmento: TStringField;
    SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCAcNmMarca: TStringField;
    SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCAnQtdeEstoque: TBCDField;
    SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCAnValTotalCusto: TBCDField;
    SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCAnQtdeVenda: TBCDField;
    SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCAnValCMV: TBCDField;
    QRDBText1: TQRDBText;
    QRLabel5: TQRLabel;
    QRExpr5: TQRExpr;
    QRExpr8: TQRExpr;
    QRShape7: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    SPREL_POSICAO_ESTOQUE_ESTRUTURA_MARCAnQtdeTransito: TBCDField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoEstoqueEstruturaMarca_view: TrptPosicaoEstoqueEstruturaMarca_view;

implementation

{$R *.dfm}

end.
