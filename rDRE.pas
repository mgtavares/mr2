unit rDRE;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons, comObj ;

type
  TrptDRE = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    Label2: TLabel;
    DataSource3: TDataSource;
    DBEdit7: TDBEdit;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit5: TMaskEdit;
    Label4: TLabel;
    MaskEdit2: TMaskEdit;
    RadioGroup1: TRadioGroup;
    CheckBox1: TCheckBox;
    RadioGroup2: TRadioGroup;
    Label5: TLabel;
    edtLoja: TMaskEdit;
    DBEdit1: TDBEdit;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    dsQryLoja: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaExit(Sender: TObject);
    procedure FormActivate(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptDRE: TrptDRE;

implementation

uses fMenu, rImpSP, fLookup_Padrao, rMovTit_view, rDRE_view, QRExport ;

{$R *.dfm}

procedure TrptDRE.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin

  iAux := frmMenu.nCdEmpresaAtiva;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  if (iAux > 0) then
  begin

    MaskEdit3.Text := IntToStr(iAux) ;

    qryEmpresa.Close ;
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := iAux ;
    qryEmpresa.Open ;

    qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

    if (frmMenu.nCdLojaAtiva > 0) then
    begin
        edtLoja.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
        PosicionaQuery(qryLoja, edtLoja.text) ;
    end
    else
    begin
        edtLoja.ReadOnly := True ;
        edtLoja.Color    := $00E9E4E4 ;
    end ;

  end ;

  MaskEdit3.SetFocus ;

  inherited;

end;


procedure TrptDRE.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TrptDRE.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryUnidadeNegocio.Close ;

  PosicionaQuery(qryUnidadeNegocio, MaskEdit5.Text) ;
end;

function MesExtenso(dData: TDateTime):string;
begin

    if StrToInt(Copy(DateToStr(dData),4,2)) = 1 then
        result := 'jan/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 2 then
        result := 'fev/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 3 then
        result := 'mar/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 4 then
        result := 'abr/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 5 then
        result := 'mai/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 6 then
        result := 'jun/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 7 then
        result := 'jul/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 8 then
        result := 'ago/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 9 then
        result := 'set/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 10 then
        result := 'out/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 11 then
        result := 'nov/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 12 then
        result := 'dez/' + Copy(DateToStr(dData),7,4) ;

end ;

procedure TrptDRE.ToolButton1Click(Sender: TObject);
var
    dAux : TDateTime ;
    dAux1, dAux2, dAux3, dAux4, dAux5, dAux6 : TDateTime ;
    dAux7, dAux8, dAux9, dAux10, dAux11, dAux12 : TDateTime ;

    linha, coluna : integer;
    planilha      : variant;
    valorcampo    : string;
    objRel        : TrptDRE_view ;

begin

  If (Trim(MaskEdit3.Text) = '') or (DBEDit3.Text = '') Then
  begin
      ShowMessage('Informe a Empresa.') ;
      exit ;
  end ;

  if (Length(Trim(MaskEdit1.Text)) <> 7) then
  begin
      ShowMessage('Compet�ncia Inicial Inv�lida.') ;
      exit ;
  end ;

  try
      dAux := StrToDateTime('01/' + MaskEdit1.Text) ;
  except
      ShowMessage('Compet�ncia Inicial Inv�lida.') ;
      exit ;
  end ;

  if (Length(Trim(MaskEdit2.Text)) <> 7) then
  begin
      ShowMessage('Compet�ncia Final Inv�lida.') ;
      exit ;
  end ;

  try
      dAux := StrToDateTime('01/' + MaskEdit2.Text) ;
  except
      ShowMessage('Compet�ncia Final Inv�lida.') ;
      exit ;
  end ;

  dAux1 := StrToDateTime('01/' + MaskEdit1.Text) ;
  dAux2 := dAux1  + 31 ;
  dAux3 := dAux2  + 31 ;
  dAux4 := dAux3  + 31 ;
  dAux5 := dAux4  + 31 ;
  dAux6 := dAux5  + 31 ;
  dAux7 := dAux6  + 31 ;
  dAux8 := dAux7  + 31 ;
  dAux9 := dAux8  + 31 ;
  dAux10:= dAux9  + 31 ;
  dAux11:= dAux10 + 31 ;
  dAux12:= dAux11 + 31 ;

  objRel := TrptDRE_view.Create(Self) ;

  objRel.lblmes1.Caption  := MesExtenso(dAux1) ;
  objRel.lblmes2.Caption  := MesExtenso(dAux2) ;
  objRel.lblmes3.Caption  := MesExtenso(dAux3) ;
  objRel.lblmes4.Caption  := MesExtenso(dAux4) ;
  objRel.lblmes5.Caption  := MesExtenso(dAux5) ;
  objRel.lblmes6.Caption  := MesExtenso(dAux6) ;
  objRel.lblmes7.Caption  := MesExtenso(dAux7) ;
  objRel.lblmes8.Caption  := MesExtenso(dAux8) ;
  objRel.lblmes9.Caption  := MesExtenso(dAux9) ;
  objRel.lblmes10.Caption := MesExtenso(dAux10) ;
  objRel.lblmes11.Caption := MesExtenso(dAux11) ;
  objRel.lblmes12.Caption := MesExtenso(dAux12) ;

  objRel.usp_Relatorio.Close ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdUnidadeNegocio').Value := frmMenu.ConvInteiro(MaskEdit5.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@cMesAnoIni').Value        := Trim(MaskEdit1.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@cMesAnoFim').Value        := Trim(MaskEdit2.Text) ;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value       := 0
  else objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value      := frmMenu.ConvInteiro(edtLoja.Text) ;

  if (RadioGroup2.ItemIndex = 0) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cTipo').Value             := 'A' ;

  if (RadioGroup2.ItemIndex = 1) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cTipo').Value             := 'S' ;

  if (CheckBox1.Checked) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cFlgContaZerada').Value := 'S'
  else objRel.usp_Relatorio.Parameters.ParamByName('@cFlgContaZerada').Value := 'N' ;

  objRel.usp_Relatorio.Open  ;

  if (RadioGroup1.ItemIndex = 1) then
  begin

    planilha:= CreateoleObject('Excel.Application');
    planilha.WorkBooks.add(1);
    planilha.caption := 'DRE - Demonstrativo de Resultados do Exerc�cio';
    planilha.visible := true;

    for linha := 0 to objRel.usp_Relatorio.RecordCount - 1 do
    begin
       for coluna := 1 to objRel.usp_Relatorio.FieldCount do
       begin
         valorcampo := objRel.usp_Relatorio.Fields[coluna - 1].AsString;
         planilha.cells[linha + 2,coluna] := valorCampo;
       end;
       objRel.usp_Relatorio.Next;
    end;
    for coluna := 1 to objRel.usp_Relatorio.FieldCount do
    begin

       valorcampo := objRel.usp_Relatorio.Fields[coluna - 1].DisplayLabel;
       planilha.cells[1,coluna] := valorcampo;

    end;
    planilha.columns.Autofit;

    FreeAndNil(objRel);

  end
  else
  begin

      If (Trim(MaskEdit5.Text) = '') and (DBEDit7.Text <> '') Then DBEDit7.Text := '';
      If (Trim(edtLoja.Text) = '')   and (DBEDit1.Text <> '') Then DBEDit1.Text := '';

      objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
      objRel.lblFiltro1.Caption := 'Empresa      : ' + DbEdit3.Text ;
      objRel.lblFiltro2.Caption := 'Unid. Negocio: ' + DbEdit7.Text ;

      if (frmMenu.LeParametro('VAREJO') = 'S') then
          objRel.lblFiltro4.Caption := 'Loja         : ' + DbEdit1.Text
      else objRel.lblFiltro4.Caption := '';

      objRel.lblFiltro3.Caption     := 'Per�odo      : ' + MaskEdit1.Text + ' a ' + MaskEdit2.Text;

      try
          try
              {--visualiza o relat�rio--}
              objRel.QuickRep1.PreviewModal;
          except
              MensagemErro('Erro na cria��o do relat�rio');
              raise;
          end;
      finally
          FreeAndNil(objRel);
      end;

  end ;

end;


procedure TrptDRE.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptDRE.cxButton1Click(Sender: TObject);
begin
  inherited;
  //rptDRE_View.QuickRep1.ExportToFilter(TQRXLSFilter.Create('c:\teste.xls'));
end;

procedure TrptDRE.MaskEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(14);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryUnidadeNegocio, MaskEdit5.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptDRE.edtLojaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptDRE.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;

end;

procedure TrptDRE.FormActivate(Sender: TObject);
begin
  inherited;
  if (frmMenu.LeParametro('VAREJO') = 'N') then
      edtLoja.Enabled := False ;

end;

initialization
     RegisterClass(TrptDRE) ;

end.
