inherited frmMotivoHoraImprodutiva: TfrmMotivoHoraImprodutiva
  Left = 111
  Top = 166
  Caption = 'Motivo Hora Improdutiva'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 51
    Top = 54
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 40
    Top = 78
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 94
    Top = 48
    Width = 65
    Height = 19
    DataField = 'nCdMotivoHoraImprodutiva'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 94
    Top = 72
    Width = 650
    Height = 19
    DataField = 'cNmMotivoHoraImprodutiva'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMotivoHoraImprodutiva'
      '      ,cNmMotivoHoraImprodutiva'
      '  FROM MotivoHoraImprodutiva'
      ' WHERE nCdMotivoHoraImprodutiva = :nPk'
      '')
    object qryMasternCdMotivoHoraImprodutiva: TIntegerField
      FieldName = 'nCdMotivoHoraImprodutiva'
    end
    object qryMastercNmMotivoHoraImprodutiva: TStringField
      FieldName = 'cNmMotivoHoraImprodutiva'
      Size = 50
    end
  end
end
