unit fTitulo_DetalheJuros;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, GridsEh, DBGridEh, ImgList,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmTitulo_DetalheJuros = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryTabelaJuro: TADOQuery;
    dsTabelaJuro: TDataSource;
    qryTabelaJuronCdTituloJuroDia: TAutoIncField;
    qryTabelaJuronCdTitulo: TIntegerField;
    qryTabelaJurodDtCalculoJuro: TDateTimeField;
    qryTabelaJurodDtVencTit: TDateTimeField;
    qryTabelaJuronValBaseCalculo: TBCDField;
    qryTabelaJuronPercJuroDia: TBCDField;
    qryTabelaJuronValJuro: TBCDField;
    qryTabelaJuronValJuroAcumulado: TBCDField;
    qryTabelaJuronPercMulta: TBCDField;
    qryTabelaJuronValMulta: TBCDField;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTitulo_DetalheJuros: TfrmTitulo_DetalheJuros;

implementation

{$R *.dfm}

procedure TfrmTitulo_DetalheJuros.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Columns[5].DisplayFormat := '#,##0.0000' ;
end;

end.
