unit fMotivoCancSaldoPed;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, ER2Lookup, StdCtrls, Mask, DBCtrls;

type
  TfrmMotivoCancSaldoPed = class(TfrmCadastro_Padrao)
    qryMasternCdMotivoCancSaldoPed: TIntegerField;
    qryMastercNmMotivoCancSaldoPed: TStringField;
    qryMasternCdTabTipoPedido: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    edtTipoPedido: TER2LookupDBEdit;
    Label3: TLabel;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTabTipoPedido: TIntegerField;
    qryTipoPedidocNmTabTipoPedido: TStringField;
    DBEdit3: TDBEdit;
    dsTipoPedido: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMotivoCancSaldoPed: TfrmMotivoCancSaldoPed;

implementation

{$R *.dfm}

procedure TfrmMotivoCancSaldoPed.FormCreate(Sender: TObject);
begin
  inherited;

  nCdTabelaSistema  := 322;
  cNmTabelaMaster   := 'MOTIVOCANCSALDOPED';
  nCdConsultaPadrao := 760;
end;

procedure TfrmMotivoCancSaldoPed.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(qryMastercNmMotivoCancSaldoPed.Value) = '') then
  begin
      MensagemAlerta('Digite a descri��o do Motivo de Cancelamento.');
      DBEdit2.SetFocus;
      abort;
  end;

  edtTipoPedido.PosicionaQuery;

  if ((qryMasternCdTabTipoPedido.Value = 0) or (DBEdit3.Text = '')) then
  begin
      MensagemAlerta('O Tipo de Pedido n�o foi selecionado ou � inv�lido.');
      edtTipoPedido.SetFocus;
      abort;
  end;

  inherited;
end;

procedure TfrmMotivoCancSaldoPed.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryTipoPedido.Close;
  PosicionaQuery(qryTipoPedido,qryMasternCdTabTipoPedido.AsString);
end;

procedure TfrmMotivoCancSaldoPed.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryTipoPedido.Close;
  
end;

procedure TfrmMotivoCancSaldoPed.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
end;

initialization
  RegisterClass(TfrmMotivoCancSaldoPed);

end.
