unit rRelPedidoVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, DB, ADODB, Mask, StdCtrls, DBCtrls, ImgList,
  ComCtrls, ToolWin, ExtCtrls, ER2Lookup;

type
  TrptRelPedidoVenda = class(TfrmRelatorio_Padrao)
    Label5: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label2: TLabel;
    Label7: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit10: TDBEdit;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
    MaskEdit3: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit5: TMaskEdit;
    DBEdit5: TDBEdit;
    MaskEdit9: TMaskEdit;
    MaskEdit8: TMaskEdit;
    DBEdit4: TDBEdit;
    MaskEdit4: TMaskEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    dsEmpresa: TDataSource;
    dsTipoPedido: TDataSource;
    dsTerceiro: TDataSource;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    dsGrupoEconomico: TDataSource;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    dsMarca: TDataSource;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    RadioGroup3: TRadioGroup;
    RadioGroup4: TRadioGroup;
    MaskEdit1: TMaskEdit;
    Label1: TLabel;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    qryGrupoProdutocFlgExpPDV: TIntegerField;
    DBEdit2: TDBEdit;
    DataSource1: TDataSource;
    MaskEdit10: TMaskEdit;
    Label10: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    RadioGroup5: TRadioGroup;
    RadioGroup6: TRadioGroup;
    edtCdProduto: TER2LookupMaskEdit;
    Label11: TLabel;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DBEdit6: TDBEdit;
    DataSource3: TDataSource;
    RadioGroup7: TRadioGroup;
    procedure MaskEdit2Exit(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
    procedure MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
    procedure MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit10Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRelPedidoVenda: TrptRelPedidoVenda;

implementation

uses fLookup_Padrao,  fMenu, rRelPedidoVenda_view;

{$R *.dfm}

procedure TrptRelPedidoVenda.MaskEdit2Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close;

  PosicionaQuery(qryGrupoEconomico, MaskEdit2.Text) ;

end;

procedure TrptRelPedidoVenda.MaskEdit3Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, MaskEdit3.Text) ;

end;

procedure TrptRelPedidoVenda.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryTipoPedido.Close ;
  PosicionaQuery(qryTipoPedido, MaskEdit4.Text) ;

end;

procedure TrptRelPedidoVenda.MaskEdit5Exit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, MaskEdit5.Text) ;

end;

procedure TrptRelPedidoVenda.MaskEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            MaskEdit2.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, MaskEdit2.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRelPedidoVenda.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(101);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, MaskEdit3.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRelPedidoVenda.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(71,'cFlgCompra = 0 AND cFlgDevolucVenda = 0 AND cFlgDevolucCompra = 0');

        If (nPK > 0) then
        begin
            MaskEdit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTipoPedido, MaskEdit4.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRelPedidoVenda.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            MaskEdit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, MaskEdit5.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRelPedidoVenda.MaskEdit1Exit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, MaskEdit1.Text) ;
  
end;

procedure TrptRelPedidoVenda.MaskEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin
            MaskEdit1.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, MaskEdit1.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRelPedidoVenda.ToolButton1Click(Sender: TObject);
var
    cFiltro : String ;
    objRel  : TrptRelPedidoVenda_view;
begin
  inherited;

  objRel := TrptRelPedidoVenda_view.Create(nil);

  Try
      Try
          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          objRel.usp_Relatorio.Close ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdGrupoEconomico').Value :=  frmMenu.ConvInteiro(MaskEdit2.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdTerceiro').Value       :=  frmMenu.ConvInteiro(MaskEdit3.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdTipoPedido').Value     :=  frmMenu.ConvInteiro(MaskEdit4.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@cFlgCompraVenda').Value   :=  'V';
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdMarca').Value          :=  frmMenu.ConvInteiro(MaskEdit5.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdGrupoProduto').Value   :=  frmMenu.ConvInteiro(MaskEdit1.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@cPrevEntregaIni').Value   :=  frmMenu.ConvData(MaskEdit6.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@cPrevEntregaFim').Value   :=  frmMenu.ConvData(MaskEdit7.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@cDtPedidoIni').Value      :=  frmMenu.ConvData(MaskEdit8.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@cDtPedidoFim').Value      :=  frmMenu.ConvData(MaskEdit9.Text);
          objRel.usp_Relatorio.Parameters.ParamByName('@cSituacaoItem').Value     :=  RadioGroup2.ItemIndex ;
          objRel.usp_Relatorio.Parameters.ParamByName('@cSoDivergencia').Value    :=  1 ;
          objRel.usp_Relatorio.Parameters.ParamByName('@cExibirValor').Value      :=  RadioGroup3.ItemIndex ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value           :=  frmMenu.ConvInteiro(MaskEdit10.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value        :=  frmMenu.nCdEmpresaAtiva;
          objRel.usp_Relatorio.Parameters.ParamByName('@cSoTroca').Value          :=  RadioGroup4.ItemIndex;
          objRel.usp_Relatorio.Parameters.ParamByName('@cExibeBonif').Value       :=  RadioGroup5.ItemIndex;
          objRel.usp_Relatorio.Parameters.ParamByName('@cSoBonif').Value          :=  RadioGroup6.ItemIndex;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdProduto').Value        :=  frmMenu.ConvInteiro(edtCdProduto.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nSituacaoPedido').Value   :=  RadioGroup7.ItemIndex;
          objRel.usp_Relatorio.Open ;

          // oculta os valores
          objRel.QRBand4.Enabled := True ;

          if (RadioGroup3.ItemIndex = 1) then
              objRel.QRBand4.Enabled := False ;

          // oculta os itens
          objRel.QRBand3.Enabled := True ;

          if (RadioGroup1.ItemIndex = 1) then
              objRel.QRBand3.Enabled := False ;

          // Monta os filtros para exibir

          cFiltro := 'Empresa : ' + IntToStr(frmMenu.nCdEmpresaAtiva) + '-' + frmMenu.cNmEmpresaAtiva ;

          IF (DBEdit3.Text <> '') then
              cFiltro := cFiltro + ' / Loja: ' + MaskEdit10.Text + '-' + Trim(DbEdit3.Text) ;

          IF (DBEdit1.Text <> '') then
              cFiltro := cFiltro + ' / Grupo Economico: ' + MaskEdit2.Text + '-' + Trim(DbEdit1.Text) ;

          IF (DBEdit10.Text <> '') then
              cFiltro := cFiltro + ' / Terceiro: ' + MaskEdit3.Text + '-' + Trim(DbEdit10.Text) ;

          IF (DBEdit4.Text <> '') then
              cFiltro := cFiltro + ' / Tipo Pedido: ' + MaskEdit4.Text + '-' + Trim(DbEdit4.Text) ;

          IF (DBEdit5.Text <> '') then
              cFiltro := cFiltro + ' / Marca: ' + MaskEdit5.Text + '-' + Trim(DbEdit5.Text) ;

          IF (DBEdit2.Text <> '') then
              cFiltro := cFiltro + ' / Grupo Produto: ' + MaskEdit1.Text + '-' + Trim(DbEdit2.Text) ;

          IF (DBEdit6.Text <> '') then
              cFiltro := cFiltro + ' / Produto: ' + edtCdProduto.Text + '-' + Trim(DbEdit6.Text) ;

          IF (Trim(MaskEdit6.Text) <> '/  /') or (Trim(MaskEdit7.Text) <> '/  /') then
              cFiltro := cFiltro + ' / Prev. Entrega: ' ;

          IF (Trim(MaskEdit6.Text) <> '/  /') then
              cFiltro := cFiltro + ' de : ' + MaskEdit6.Text ;

          IF (Trim(MaskEdit7.Text) <> '/  /') then
              cFiltro := cFiltro + ' at� : ' + MaskEdit7.Text ;

          IF (Trim(MaskEdit8.Text) <> '/  /') or (Trim(MaskEdit9.Text) <> '/  /') then
              cFiltro := cFiltro + ' / Data Pedido: ' ;

          IF (Trim(MaskEdit8.Text) <> '/  /') then
              cFiltro := cFiltro + ' de : ' + MaskEdit8.Text ;

          IF (Trim(MaskEdit9.Text) <> '/  /') then
              cFiltro := cFiltro + ' at� : ' + MaskEdit9.Text ;

          cFiltro := cFiltro + ' / Situa��o : ' ;

          IF (RadioGroup2.ItemIndex = 0) then
              cFiltro := cFiltro + 'TODOS' ;

          IF (RadioGroup2.ItemIndex = 1) then
              cFiltro := cFiltro + 'S� em aberto' ;

          IF (RadioGroup2.ItemIndex = 2) then
              cFiltro := cFiltro + 'S� em atraso' ;

          IF (RadioGroup2.ItemIndex = 3) then
              cFiltro := cFiltro + 'S� faturados' ;

          IF (RadioGroup2.ItemIndex = 4) then
              cFiltro := cFiltro + 'S� previs�es' ;

          IF (RadioGroup2.ItemIndex = 5) then
              cFiltro := cFiltro + 'S� cancelados' ;

          IF (RadioGroup4.ItemIndex = 0) then
              cFiltro := cFiltro + ' / Somente trocas' ;

          cFiltro := cFiltro + ' / Incluir Bonifica��o : ' ;

          IF (RadioGroup5.ItemIndex = 0) then
              cFiltro := cFiltro + 'Sim' ;

          IF (RadioGroup5.ItemIndex = 1) then
              cFiltro := cFiltro + 'N�o' ;

          IF (RadioGroup6.ItemIndex = 0) then
              cFiltro := cFiltro + ' / Somente Bonif : ' ;

          objRel.lblFiltro1.Caption := cFiltro ;

          objRel.QuickRep1.Preview;
       except
          MensagemErro('Erro na cria��o do Relat�rio');
          raise;
       end;
   finally
      FreeAndNil(objRel);
   end;
end;

procedure TrptRelPedidoVenda.MaskEdit10KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(86);

        If (nPK > 0) then
        begin
            MaskEdit10.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, MaskEdit10.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRelPedidoVenda.MaskEdit10Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, MaskEdit10.Text) ;

end;

initialization
    RegisterClass(TrptRelPedidoVenda) ;

end.
