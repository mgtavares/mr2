inherited frmTabRenegociacao: TfrmTabRenegociacao
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'Tabela Renegocia'#231#227'o'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1152
    Height = 785
  end
  inherited ToolBar1: TToolBar
    Width = 1152
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 8
    Top = 48
    Width = 633
    Height = 369
    DataSource = DataSource1
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTabRenegociacao'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'iDiasAtraso'
        Footers = <>
        Width = 35
      end
      item
        EditButtons = <>
        FieldName = 'iDiasAtrasoFim'
        Footers = <>
        Width = 35
      end
      item
        EditButtons = <>
        FieldName = 'nPercDescPrincVista'
        Footers = <>
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'iParcLimDescPrinc'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'nPercDescPrincParc'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'nPercDescPrincMaiorParc'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'nPercDescJurosVista'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'iParcLimDescJuros'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'nPercDescJurosParc'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'nPercDescJurosMaiorParc'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'iQtdeMaxParc'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'nPercMinEntrada'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'nPercEntradaSug'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'nValParcelaMin'
        Footers = <>
        Width = 70
      end
      item
        EditButtons = <>
        FieldName = 'nCdServidorOrigem'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'dDtReplicacao'
        Footers = <>
        Visible = False
      end>
  end
  object qryTabRenegociacao: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTabRenegociacaoBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM TabRenegociacao'
      ' WHERE nCdEmpresa = :nPK'
      ' ORDER BY iDiasAtraso')
    Left = 240
    Top = 168
    object qryTabRenegociacaonCdTabRenegociacao: TAutoIncField
      FieldName = 'nCdTabRenegociacao'
    end
    object qryTabRenegociacaonCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryTabRenegociacaoiDiasAtraso: TIntegerField
      DisplayLabel = 'Dias Atraso|De'
      FieldName = 'iDiasAtraso'
    end
    object qryTabRenegociacaoiDiasAtrasoFim: TIntegerField
      DisplayLabel = 'Dias Atraso|At'#233
      FieldName = 'iDiasAtrasoFim'
    end
    object qryTabRenegociacaonPercDescPrincVista: TBCDField
      DisplayLabel = 'Valor Principal|A vista|% Descto'
      FieldName = 'nPercDescPrincVista'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaoiParcLimDescPrinc: TIntegerField
      DisplayLabel = 'Valor Principal|Parcelado|Qtde. Parc.'
      FieldName = 'iParcLimDescPrinc'
    end
    object qryTabRenegociacaonPercDescPrincParc: TBCDField
      DisplayLabel = 'Valor Principal|Parcelado|% Descto'
      FieldName = 'nPercDescPrincParc'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaonPercDescPrincMaiorParc: TBCDField
      DisplayLabel = 'Valor Principal|Parcelado|% Descto > Parc.'
      FieldName = 'nPercDescPrincMaiorParc'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaonPercDescJurosVista: TBCDField
      DisplayLabel = 'Juros|A vista|% Descto'
      FieldName = 'nPercDescJurosVista'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaoiParcLimDescJuros: TIntegerField
      DisplayLabel = 'Juros|Parcelado|Qtde. Parc.'
      FieldName = 'iParcLimDescJuros'
    end
    object qryTabRenegociacaonPercDescJurosParc: TBCDField
      DisplayLabel = 'Juros|Parcelado|% Descto'
      FieldName = 'nPercDescJurosParc'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaonPercDescJurosMaiorParc: TBCDField
      DisplayLabel = 'Juros|Parcelado|% Descto > Parc.'
      FieldName = 'nPercDescJurosMaiorParc'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaoiQtdeMaxParc: TIntegerField
      DisplayLabel = 'Dados Parcelamento|Qtde M'#225'x. Parcelas'
      FieldName = 'iQtdeMaxParc'
    end
    object qryTabRenegociacaonPercMinEntrada: TBCDField
      DisplayLabel = 'Dados Parcelamento|% M'#237'nimo Entrada'
      FieldName = 'nPercMinEntrada'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaonPercEntradaSug: TBCDField
      DisplayLabel = 'Dados Parcelamento|% Entrada Sugerido'
      FieldName = 'nPercEntradaSug'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaonValParcelaMin: TBCDField
      DisplayLabel = 'Dados Parcelamento|Val. M'#237'nimo Parcela'
      FieldName = 'nValParcelaMin'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaonCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryTabRenegociacaodDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTabRenegociacao
    Left = 296
    Top = 176
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 368
    Top = 248
  end
end
