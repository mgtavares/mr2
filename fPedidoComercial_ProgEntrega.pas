unit fPedidoComercial_ProgEntrega;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping;

type
  TfrmPedidoComercial_ProgEntrega = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryItemPedido: TADOQuery;
    dsItemPedido: TDataSource;
    qryItemPedidonCdItemPedido: TAutoIncField;
    qryItemPedidonCdPedido: TIntegerField;
    qryItemPedidonCdProduto: TIntegerField;
    qryItemPedidocNmitem: TStringField;
    qryItemPedidonQtdePed: TBCDField;
    qryItemPedidonQtdePrev: TBCDField;
    qryItemPedidodDtEntregaIni: TDateTimeField;
    qryItemPedidodDtEntregaFim: TDateTimeField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPedidoComercial_ProgEntrega: TfrmPedidoComercial_ProgEntrega;

implementation

{$R *.dfm}

end.
