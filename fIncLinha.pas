unit fIncLinha;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DB, ADODB;

type
  TfrmIncLinha = class(TfrmProcesso_Padrao)
    Edit1: TEdit;
    Label1: TLabel;
    usp_Linha: TADOStoredProc;
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GravarLinha();
    procedure ToolButton1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    nCdMarca : Integer ;
    nCdLinha : Integer ;
  end;

var
  frmIncLinha: TfrmIncLinha;

implementation

{$R *.dfm}

procedure TfrmIncLinha.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;

  case key of
    13 : Begin
        GravarLinha() ;
    end ;

  end ;

end;

procedure TfrmIncLinha.GravarLinha();
begin

    if (trim(Edit1.Text) = '') then
        exit ;

    usp_Linha.Close ;
    usp_Linha.Parameters.ParamByName('@nCdMarca').Value := nCdMarca ;
    usp_Linha.Parameters.ParamByName('@cNmLinha').Value := Trim(Edit1.Text) ;
    usp_Linha.ExecProc ;

    nCdLinha := usp_Linha.Parameters.ParamByName('@nCdLinha').Value ;

    Self.Close ;

end ;

procedure TfrmIncLinha.ToolButton1Click(Sender: TObject);
begin
  inherited;
    GravarLinha() ;
end;

end.

