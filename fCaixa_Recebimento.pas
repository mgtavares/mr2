unit fCaixa_Recebimento;

interface

uses
  ACBrECF, ACBrRFD, ACBrBase, ACBrDevice, ACBrECFClass, ACBrConsts,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GridsEh, DBGridEh, StdCtrls, cxLookAndFeelPainters, cxButtons,
  Mask, ExtCtrls, DB, ADODB, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit, DBCtrls, cxPC, Menus, cxLabel,
  ImgList, cxMaskEdit, cxDropDownEdit, cxImageComboBox, cxCheckBox, Buttons,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, jpeg, cxImage, RDprint, xpMan,
  GIFImage, Math, DateUtils, ACBrUtil, TypInfo,
  DBGridEhGrouping, ACBrSocket, ACBrIBPTax, ACBrGAV, fFuncoesSAT, Cappta_Gp_Api_Com_TLB,
  ActiveX, fCapptAPI, fCaixa_ConsOperadora, uPDV_Bematech_Termica,
  ACBrPosPrinter, StrUtils;

type
  TfrmCaixa_Recebimento = class(TForm)
    qryTemp_Pedido: TADOQuery;
    cmd: TADOCommand;
    qryTemp_PedidonCdPedido: TIntegerField;
    dsTemp_Pedido: TDataSource;
    qryPedido: TADOQuery;
    qryTemp_PedidonValPedido: TBCDField;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    DataSource1: TDataSource;
    qryOperadoraCartao: TADOQuery;
    qryOperadoraCartaonCdOperadoraCartao: TIntegerField;
    qryOperadoraCartaocNmOperadoraCartao: TStringField;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    qryTerceiro: TADOQuery;
    dsTerceiro: TDataSource;
    qryParcelas: TADOQuery;
    ADOQuery2: TADOQuery;
    qryCondPagtonCdTabTipoFormaPagto: TIntegerField;
    qryCondPagtonCdFormaPagto: TIntegerField;               
    qryCondPagtocNmFormaPagto: TStringField;
    cmdZeraTemp: TADOCommand;
    btFinalizar: TcxButton;
    btCancelar: TcxButton;
    qryLocalizaCaixa: TADOQuery;
    qryLocalizaCaixanCdContaBancaria: TIntegerField;
    qryLocalizaCaixanCdConta: TStringField;
    qryCaixaAberto: TADOQuery;
    qryCaixaAbertonCdResumoCaixa: TAutoIncField;
    qryCaixaAbertonCdContaBancaria: TIntegerField;
    qryCaixaAbertonCdConta: TStringField;
    qryCaixaAbertodDtAbertura: TDateTimeField;
    SP_ABRE_CAIXA: TADOStoredProc;
    qryLocalizaCaixadDtAbertura: TDateTimeField;
    qryLocalizaCaixanCdResumoCaixa: TAutoIncField;
    SP_FINALIZA: TADOStoredProc;
    dsParcelas: TDataSource;
    SP_BUSCA_PARCELA_CREDIARIO: TADOStoredProc;
    PopupMenu1: TPopupMenu;
    FunesECF1: TMenuItem;
    AtivarECF1: TMenuItem;
    LeituraX1: TMenuItem;
    ReduoZ1: TMenuItem;
    qryAux: TADOQuery;
    qryAmbiente: TADOQuery;
    qryAmbientecFlgUsaECF: TIntegerField;
    SP_GERA_CUPOM_FISCAL: TADOStoredProc;
    SP_GERA_CUPOM_FISCALnCdProduto: TIntegerField;
    SP_GERA_CUPOM_FISCALcNmItem: TStringField;
    SP_GERA_CUPOM_FISCALcUnidadeMedida: TStringField;
    SP_GERA_CUPOM_FISCALnQtde: TBCDField;
    SP_GERA_CUPOM_FISCALnValUnitario: TBCDField;
    SP_GERA_CUPOM_FISCALcIcmsECF: TStringField;
    SP_GRAVA_NUMERO_CUPOM_FISCAL: TADOStoredProc;
    SP_RETORNA_FORMAPAGTO_ECF: TADOStoredProc;
    SP_RETORNA_FORMAPAGTO_ECFnCdTabTipoFormaPagto: TIntegerField;
    SP_RETORNA_FORMAPAGTO_ECFcCodigoECF: TStringField;
    qryAmbientecModeloECF: TStringField;
    qryAmbientecPortaECF: TStringField;
    ImageList1: TImageList;
    qryTemp_Pagtos: TADOQuery;
    qryTemp_PagtosnCdTemp_Pagto: TIntegerField;
    qryTemp_PagtosnCdFormaPagto: TIntegerField;
    qryTemp_PagtoscNmFormaPagto: TStringField;
    qryTemp_PagtosnCdCondPagto: TIntegerField;
    qryTemp_PagtoscNmCondPagto: TStringField;
    qryTemp_PagtosnCdTerceiro: TIntegerField;
    qryTemp_PagtoscNmTerceiro: TStringField;
    qryTemp_PagtosnValPagto: TBCDField;
    qryTemp_PagtosnValEntrada: TBCDField;
    qryTemp_PagtoscFlgTipo: TIntegerField;
    dsTemp_Pagtos: TDataSource;
    qryInseri_Temp_Pagtos: TADOQuery;
    qryInseri_Temp_PagtosnCdTemp_Pagto: TAutoIncField;
    qryInseri_Temp_PagtosnCdFormaPagto: TIntegerField;
    qryInseri_Temp_PagtosnCdCondPagto: TIntegerField;
    qryInseri_Temp_PagtosnCdTerceiro: TIntegerField;
    qryInseri_Temp_PagtosnValPagto: TBCDField;
    qryInseri_Temp_PagtosnValEntrada: TBCDField;
    qryInseri_Temp_PagtoscFlgTipo: TIntegerField;
    qryTemp_Cheque: TADOQuery;
    qryTemp_PagtosnValDesconto: TBCDField;
    qryTemp_PagtosnValAcrescimo: TBCDField;
    qryInseri_Temp_PagtosnValDesconto: TBCDField;
    qryInseri_Temp_PagtosnValAcrescimo: TBCDField;
    qryTemp_PagtosiNrCartao: TLargeintField;
    qryTemp_PagtosiNrDocto: TIntegerField;
    qryTemp_PagtosiNrAutorizacao: TIntegerField;
    qryTemp_PagtosnCdOperadoraCartao: TIntegerField;
    qryInseri_Temp_PagtosiNrCartao: TLargeintField;
    qryInseri_Temp_PagtosiNrDocto: TIntegerField;
    qryInseri_Temp_PagtosiNrAutorizacao: TIntegerField;
    qryInseri_Temp_PagtosnCdOperadoraCartao: TIntegerField;
    qryTemp_PagtoscNmOperadoraCartao: TStringField;
    qryTemp_PagtosnCdCrediario: TIntegerField;
    cxPageControl1: TcxPageControl;
    tabPedidos: TcxTabSheet;
    tabParcelas: TcxTabSheet;
    tabPagamento: TcxTabSheet;
    GroupBox1: TGroupBox;
    Label12: TLabel;
    GroupBox4: TGroupBox;
    Label14: TLabel;
    btIncParcela: TcxButton;
    btExcluirParcelas: TcxButton;
    edtNumCarnet: TMaskEdit;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1nCdTitulo: TcxGridDBColumn;
    cxGridDBTableView1cNrTit: TcxGridDBColumn;
    cxGridDBTableView1iParcela: TcxGridDBColumn;
    cxGridDBTableView1cNmEspTit: TcxGridDBColumn;
    cxGridDBTableView1dDtVenc: TcxGridDBColumn;
    cxGridDBTableView1iDiasAtraso: TcxGridDBColumn;
    cxGridDBTableView1nValTit: TcxGridDBColumn;
    cxGridDBTableView1nValJuro: TcxGridDBColumn;
    cxGridDBTableView1nValDesconto: TcxGridDBColumn;
    cxGridDBTableView1nSaldoTit: TcxGridDBColumn;
    cxGridDBTableView1cNmLoja: TcxGridDBColumn;
    cxGridDBTableView1cFlgEntrada: TcxGridDBColumn;
    cxGridDBTableView1cFlgMarcado: TcxGridDBColumn;
    cxGridDBTableView1nCdCrediario: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1nValPedido: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    SP_VALIDA_TROCA_VALE: TADOStoredProc;
    qryInseri_Temp_PagtosnCdCrediario: TIntegerField;
    qryInseri_Temp_PagtoscFlgVale: TIntegerField;
    tabCheques: TcxTabSheet;
    GroupBox5: TGroupBox;
    btConsultaCheque: TcxButton;
    btExcCheques: TcxButton;
    qryTempChequeResgate: TADOQuery;
    qryTempChequeResgatenCdCheque: TIntegerField;
    qryTempChequeResgatenCdBanco: TIntegerField;
    qryTempChequeResgateiNrCheque: TIntegerField;
    qryTempChequeResgatenValCheque: TBCDField;
    qryTempChequeResgatedDtDeposito: TDateTimeField;
    qryTempChequeResgatecNmTabStatusCheque: TStringField;
    qryTempChequeResgatecNmTerceiro: TStringField;
    cxGrid4: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    dsTempChequeResgate: TDataSource;
    cxGridDBTableView3nCdCheque: TcxGridDBColumn;
    cxGridDBTableView3nCdBanco: TcxGridDBColumn;
    cxGridDBTableView3iNrCheque: TcxGridDBColumn;
    cxGridDBTableView3nValCheque: TcxGridDBColumn;
    cxGridDBTableView3dDtDeposito: TcxGridDBColumn;
    cxGridDBTableView3cNmTabStatusCheque: TcxGridDBColumn;
    cxGridDBTableView3cNmTerceiro: TcxGridDBColumn;
    qryImportaChequeConsulta: TADOQuery;
    qryAmbienteiViasECF: TIntegerField;
    qrySomaDescAcres: TADOQuery;
    qrySomaDescAcresnValDesconto: TBCDField;
    qrySomaDescAcresnValAcrescimo: TBCDField;
    tTimerLabel: TTimer;
    qryTemp_PagtoscFlgVale: TIntegerField;
    qryTemp_PagtoscFlgTEF: TIntegerField;
    qryInseri_Temp_PagtoscFlgTEF: TIntegerField;
    edtTotalPagar: TcxCurrencyEdit;
    cxLabel1: TcxLabel;
    edtEntrada: TcxCurrencyEdit;
    cxLabel3: TcxLabel;
    edtSaldo: TcxCurrencyEdit;
    cxLabel4: TcxLabel;
    edtPedidos: TcxCurrencyEdit;
    cxLabel2: TcxLabel;
    edtParcelas: TcxCurrencyEdit;
    cxLabel5: TcxLabel;
    edtValorPago: TcxCurrencyEdit;
    cxLabel6: TcxLabel;
    edtTroco: TcxCurrencyEdit;
    cxLabel7: TcxLabel;
    edtVale: TcxCurrencyEdit;
    cxLabel8: TcxLabel;
    edtChequeResg: TcxCurrencyEdit;
    cxLabel9: TcxLabel;
    btSair: TcxButton;
    btResgateCheque: TcxButton;
    btNovaParcela: TcxButton;
    btNovoPedido: TcxButton;
    Image1: TImage;
    Shape2: TShape;
    btFuncoes: TcxButton;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    lblConta: TLabel;
    lblOperador: TLabel;
    lblLoja: TLabel;
    lblSerieCF: TLabel;
    lblCF: TLabel;
    lblMensagem: TLabel;
    Label2: TLabel;
    imgCaixaLivre: TcxImage;
    btCliente: TcxButton;
    btIncPagamento: TcxButton;
    btExcluiPagamentos: TcxButton;
    btPgEntrada: TcxButton;
    btUsaVale: TcxButton;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBTableView2nCdTemp_Pagto: TcxGridDBColumn;
    cxGridDBTableView2nCdFormaPagto: TcxGridDBColumn;
    cxGridDBTableView2cNmFormaPagto: TcxGridDBColumn;
    cxGridDBTableView2nCdCondPagto: TcxGridDBColumn;
    cxGridDBTableView2cNmCondPagto: TcxGridDBColumn;
    cxGridDBTableView2nCdTerceiro: TcxGridDBColumn;
    cxGridDBTableView2cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView2nValPagto: TcxGridDBColumn;
    cxGridDBTableView2nValEntrada: TcxGridDBColumn;
    cxGridDBTableView2cFlgTipo: TcxGridDBColumn;
    cxGridDBTableView2nValDesconto: TcxGridDBColumn;
    cxGridDBTableView2nValAcrescimo: TcxGridDBColumn;
    cxGridDBTableView2iNrCartao: TcxGridDBColumn;
    cxGridDBTableView2iNrDocto: TcxGridDBColumn;
    cxGridDBTableView2iNrAutorizacao: TcxGridDBColumn;
    cxGridDBTableView2nCdOperadoraCartao: TcxGridDBColumn;
    cxGridDBTableView2cNmOperadoraCartao: TcxGridDBColumn;
    cxGridDBTableView2nCdCrediario: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    DBEdit7: TDBEdit;
    Label25: TLabel;
    DBEdit9: TDBEdit;
    Label27: TLabel;
    btTrocaCondicao: TcxButton;
    btTrocaCliente: TcxButton;
    DBEdit1: TDBEdit;
    Label1: TLabel;
    edtValor: TcxCurrencyEdit;
    Label3: TLabel;
    edtDC: TcxCurrencyEdit;
    Label4: TLabel;
    edtDM: TcxCurrencyEdit;
    Label5: TLabel;
    Label6: TLabel;
    edtVlrPagto: TcxCurrencyEdit;
    qryTemp_PedidonCdTerceiro: TIntegerField;
    qryTemp_PedidonCdCondPagto: TIntegerField;
    qryTemp_PedidonValProdutos: TBCDField;
    qryTemp_PedidonValDescontoCondPagto: TBCDField;
    qryTemp_PedidonValJurosCondPagto: TBCDField;
    qryPedidonCdPedido: TIntegerField;
    qryPedidonValPedido: TBCDField;
    qryPedidonCdTerceiro: TIntegerField;
    qryPedidonCdCondPagto: TIntegerField;
    qryPedidonValProdutos: TBCDField;
    qryPedidonValDescontoCondPagto: TBCDField;
    qryPedidonValJurosCondPagto: TBCDField;
    qryPedidocNmCondPagto: TStringField;
    qryTemp_PedidocNmCondPagto: TStringField;
    cxGrid1DBTableView1nValProdutos: TcxGridDBColumn;
    cxGrid1DBTableView1nValDescontoCondPagto: TcxGridDBColumn;
    cxGrid1DBTableView1nValJurosCondPagto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCondPagto: TcxGridDBColumn;
    qryPedidonValDesconto: TBCDField;
    qryTemp_PedidonValDesconto: TBCDField;
    cxGrid1DBTableView1nValDesconto: TcxGridDBColumn;
    qryPedidocNmTerceiro: TStringField;
    qryCondPagtonPercAcrescimo: TBCDField;
    qryCondPagtocFlgExigeCliente: TIntegerField;
    qryCondPagtocFlgTEF: TIntegerField;
    qryPedidonValValeMerc: TBCDField;
    qryTemp_PedidonValValeMerc: TBCDField;
    cxLabel10: TcxLabel;
    DBEdit2: TDBEdit;
    Label7: TLabel;
    DBEdit3: TDBEdit;
    Label8: TLabel;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocRG: TStringField;
    qryTerceirocNmTabTipoComprov: TStringField;
    qryTemp_PagtosnValDescontoManual: TBCDField;
    qryInseri_Temp_PagtosnValDescontoManual: TBCDField;
    qryTempChequeResgatenValJuros: TBCDField;
    cxGridDBTableView3DBColumn1: TcxGridDBColumn;
    cxGridDBTableView3DBColumn2: TcxGridDBColumn;
    qryTempChequeResgatenValTotal: TBCDField;
    qryCondPagtonPercEntrada: TBCDField;
    qryCondPagtoiQtdeParcelas: TIntegerField;
    qryCondPagtoiDiasPrimParc: TIntegerField;
    qryCondPagtoiDiasMaxPrimParc: TIntegerField;
    qryCondPagtoiDiasProxParc: TIntegerField;
    qryCondPagtocFlgPermPromocao: TIntegerField;
    qryCondPagtodDtPrimParcela: TDateTimeField;
    qryCondPagtocFlgPrimParcEnt: TIntegerField;
    qryCondPagtocFlgPermDesconto: TIntegerField;
    Image2: TImage;
    imgLogoCaixa: TImage;
    qryLocalizaCaixaOperador: TADOQuery;
    qryLocalizaCaixaOperadornCdContaBancaria: TIntegerField;
    btCartaoPresente: TcxButton;
    qryTemp_PedidonValValePres: TBCDField;
    qryPedidonValValePres: TBCDField;
    imgBloquear: TImage;
    imgDesbloquear: TImage;
    qryCaixaAbertocFlgBloqueado: TIntegerField;
    qryLocalizaCaixacFlgBloqueado: TIntegerField;
    qryAmbientecPortaMatricial: TStringField;
    qryAmbientenCdTipoImpressoraPDV: TIntegerField;
    qryAmbientecNomeDLL: TStringField;
    qryVale: TADOQuery;
    qryValenCdTerceiro: TIntegerField;
    qryValecNmTerceiro: TStringField;
    qryValedDtVale: TDateTimeField;
    qryValenValVale: TBCDField;
    qryValecNmLoja: TStringField;
    qryValenCdVale: TStringField;
    qryValenCdLanctoFin: TStringField;
    qryValecTipoVale: TStringField;
    qryValecRG: TStringField;
    qryPedidocFlgPromocional: TIntegerField;
    qryTemp_PedidocFlgPromocional: TIntegerField;
    cxGrid1DBTableView1cFlgPromocional: TcxGridDBColumn;
    qryPedidonValDevoluc: TBCDField;
    qryTemp_PedidonValDevoluc: TBCDField;
    SP_CANCELA_CUPOM_FISCAL: TADOStoredProc;
    qryPedidonValMercadoria: TBCDField;
    qryTemp_PedidonValMercadoria: TBCDField;
    PopupMenu2: TPopupMenu;
    RemoverJurosdasParcelas1: TMenuItem;
    qryParcelasnCdTitulo: TIntegerField;
    qryParcelascNrTit: TStringField;
    qryParcelascNmEspTit: TStringField;
    qryParcelasdDtEmissao: TDateTimeField;
    qryParcelasdDtVenc: TDateTimeField;
    qryParcelasnValTit: TBCDField;
    qryParcelasnValJuro: TBCDField;
    qryParcelasnValDesconto: TBCDField;
    qryParcelasnSaldoTit: TBCDField;
    qryParcelascNmLoja: TStringField;
    qryParcelasiDiasAtraso: TIntegerField;
    qryParcelascFlgEntrada: TIntegerField;
    qryParcelasnCdCrediario: TIntegerField;
    qryParcelasiParcela: TIntegerField;
    qryParcelascFlgMarcado: TIntegerField;
    qryParcelasnCdTerceiro: TIntegerField;
    qryParcelascFlgDescontoJuros: TIntegerField;
    qryParcelasnValJurosDescontado: TBCDField;
    btRenegociacao: TcxButton;
    qryInseri_Temp_PagtosnCdPropostaReneg: TIntegerField;
    qryInseri_Temp_PagtosiQtdeParcReneg: TIntegerField;
    qryInseri_Temp_PagtosnValParcReneg: TBCDField;
    SP_BUSCA_PARCELA_CREDIARIOnCdTitulo: TIntegerField;
    SP_BUSCA_PARCELA_CREDIARIOiParcela: TIntegerField;
    SP_BUSCA_PARCELA_CREDIARIOcFlgCobradora: TIntegerField;
    qrySomaParcCobradora: TADOQuery;
    qrySomaParcCobradoranSaldoTitAberto: TBCDField;
    qrySomaParcCobradoranSaldoTitAbertoCobradora: TBCDField;
    SP_GERA_CUPOM_FISCALcAliqICMS: TStringField;
    qryTemp_PedidonValAcrescimo: TBCDField;
    qryPedidonValAcrescimo: TBCDField;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    edtJM: TcxCurrencyEdit;
    Label13: TLabel;
    qryLocalizaCaixadDtLimiteFech: TDateTimeField;
    qryVerificaChequeCaixa: TADOQuery;
    qryVerificaCartaoCaixa: TADOQuery;
    qryVerificaCrediarioCaixa: TADOQuery;
    qryVerificaChequeCaixaCOLUMN1: TIntegerField;
    qryVerificaCartaoCaixaCOLUMN1: TIntegerField;
    qryVerificaCrediarioCaixaCOLUMN1: TIntegerField;
    qryConfereDireito: TADOQuery;
    qryConfereDireitonCdUsuarioTipoDireitoGerente: TAutoIncField;
    qryTemp_PagtosnValJurosCondPagto: TBCDField;
    qryInseri_Temp_PagtosnValJurosCondPagto: TBCDField;
    qryAmbientecFlgTEFAtivo: TIntegerField;
    bCancelarResp: TcxButton;
    qryCondPagtonPercValeDesconto: TBCDField;
    qryCondPagtocFlgGeraValeDescontoPromocao: TIntegerField;
    qryTemp_PagtosnValValeDesconto: TBCDField;
    qryInseri_Temp_PagtosnValValeDesconto: TBCDField;
    cxGridDBTableView2nValValeDesconto: TcxGridDBColumn;
    LiquidacaoParcial: TMenuItem;
    RDprint1: TRDprint;
    qryValenSaldoVale: TBCDField;
    qryValecCodigo: TStringField;
    qryAmbientecFlgUsaGaveta: TIntegerField;
    qryAmbientecNmPortaGaveta: TStringField;
    qryAmbientenCdTipoGavetaPDV: TIntegerField;
    qryRenegAvista: TADOQuery;
    qryRenegAvistanCdPropostaReneg: TIntegerField;
    qryVerificaCadCliente: TADOQuery;
    qryVerificaCadClientecFlgCadSimples: TIntegerField;
    qryCondPagtocFlgTipoCadCliente: TIntegerField;
    qryLiqParcial: TADOQuery;
    qryParcelasnValTotal: TBCDField;
    cxGridDBTableView1nValTotal: TcxGridDBColumn;
    edtNumPedido: TMaskEdit;
    qryAmbientecPathLogoCaixa: TStringField;
    qryVerificaSaldoContaBancaria: TADOQuery;
    qryVerificaSaldoContaBancarianSaldoConta: TBCDField;
    qryTabIBPT: TADOQuery;
    qryTabIBPTiDiasValidade: TIntegerField;
    qryTabIBPTcChave: TStringField;
    qryAmbienteiBaudECF: TIntegerField;
    qryAmbientecFlgCFeAtivo: TIntegerField;
    qryAmbientecModoEnvioCFe: TStringField;
    qryAmbientecCdAtivacaoSAT: TStringField;
    qryAmbientecAssinaturaACSAT: TStringField;
    qryAmbientenCdUFEmissaoCFe: TIntegerField;
    qryAmbientecCNPJACSAT: TStringField;
    qryAmbientecFlgTipoAmbienteCFe: TIntegerField;
    qryAmbientecModeloSAT: TStringField;
    qryAmbientecFlgCodSeguranca: TIntegerField;
    lblModoEnvioCFe: TLabel;
    lblModoCFe: TLabel;
    qryAmbientecFlgSATBloqueado: TIntegerField;
    qryAmbientecFlgSATCompartilhado: TIntegerField;
    qryAmbientenCdServidorSAT: TIntegerField;
    SP_GERA_SOLICITACAO_CD: TADOStoredProc;
    imgInfoServidorSAT: TImage;
    qryServidorSAT: TADOQuery;
    qryServidorSATnCdServidorSAT: TIntegerField;
    qryServidorSATcNmServidorSAT: TStringField;
    qryServidorSATnCdStatus: TIntegerField;
    qryServidorSATnCdEmpresa: TIntegerField;
    qryServidorSATnCdLoja: TIntegerField;
    qryLanctoFinPagCred: TADOQuery;
    qryLanctoFinPagCredcFlgLiqCred: TIntegerField;
    qryAtualizaLanctoFinCFe: TADOQuery;
    DBEdit4: TDBEdit;
    Label15: TLabel;
    qryTerceironCdGrupoClienteDesc: TIntegerField;
    qryTerceirocNmGrupoClienteDesc: TStringField;
    qryVerificaAtDados: TADOQuery;
    DBEdit5: TDBEdit;
    qryTemp_PagtosiQtdeParcelas: TIntegerField;
    qryOpCartao: TADOQuery;
    qryOpCartaonCdOperadoraCartao: TIntegerField;
    qryTemp_PagtoscNumeroControle: TStringField;
    qryImpressora: TADOQuery;
    qryTipoParc: TADOQuery;
    qryTipoParccValor: TStringField;
    qryTemp_PagtosnCdTabTipoFormaPagto: TIntegerField;
    qryTemp_PagtoscNrDoctoNSU: TStringField;
    qryImpressoracModeloImpSat: TStringField;
    qryImpressoracPortaImpSat: TStringField;
    qryImpressoracPageCodImpSat: TStringField;
    qryImpressoracFlgTipoImpSAT: TIntegerField;
    qryTemp_PagtoscNrAutorizacao: TStringField;
    qryInseri_Temp_PagtoscNrDoctoNSU: TStringField;
    qryInseri_Temp_PagtoscNrAutorizacao: TStringField;
    SP_BUSCA_PARCELA_CREDIARIOcFlgCobradoraPermiteRecLoja: TIntegerField;
    procedure btSairClick(Sender: TObject);
    function MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
    procedure ShowMessage(cTexto: string);
    procedure MensagemErro(cTexto: string);
    procedure MensagemAlerta(cTexto: string);
    function SimularCheque(bEntrada : boolean): double;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtNumPedidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btTrocaCondicaoClick(Sender: TObject);
    procedure SimularCrediario();
    procedure cxButton4Click(Sender: TObject);
    procedure CalculaSaldo();
    procedure MaskEdit3Exit(Sender: TObject);
    procedure btNovoPedidoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure InativaFuncoes();
    procedure AtivaFuncoes();
    procedure VerificaSaldoCaixa(bContinua : Boolean);
    function AutorizacaoGerente(nCdTipoLiberacao:integer): boolean;
    procedure btFinalizarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btNovaParcelaClick(Sender: TObject);
    procedure btIncParcelaClick(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure edtDinheiroExit(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure btIncPagamentoClick(Sender: TObject);
    procedure btExcluiPagamentosClick(Sender: TObject);
    procedure edtNumCarnetKeyPress(Sender: TObject; var Key: Char);
    procedure btPgEntradaClick(Sender: TObject);
    procedure qryTemp_PagtosCalcFields(DataSet: TDataSet);
    procedure btExcluirParcelasClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure ExcluirPagamentos();
    procedure IdentificaCliente() ;
    procedure btUsaValeClick(Sender: TObject);
    procedure btConsultaChequeClick(Sender: TObject);
    procedure btResgateChequeClick(Sender: TObject);
    procedure btExcChequesClick(Sender: TObject);
    procedure AbreCupomFiscal() ;
    procedure ImprimeItensCupomFiscal();
    procedure FinalizaCupomFiscal() ;
    procedure CancelaCupomFiscal() ;
    procedure EnviaPagamentoECF(cFormaPagto: string; nValorPagto: double; cOBS: string; bVinculado: boolean);
    function EstadoECF() : string ;
    procedure LeituraX;
    procedure ReducaoZ;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tTimerLabelTimer(Sender: TObject);
    procedure EnviaPagamentosECF;
    procedure CancelaRecebimento;
    procedure AcumulaValorInicial(nValor:double; nDC:double; nDM:double; nJM:double; nValorPagar:double);
    procedure edtDMExit(Sender: TObject);
    procedure SelecionaCondicaoPagamento;
    procedure CalculaOscilacaoCondicao;
    procedure SelecionaCliente;
    procedure btTrocaClienteClick(Sender: TObject);
    procedure edtValorPropertiesEditValueChanged(Sender: TObject);
    procedure edtValorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDCKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDMKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtVlrPagtoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DesativaValores;
    procedure AtivaValores;
    procedure edtEntradaPropertiesChange(Sender: TObject);
    procedure SuprimentoCaixa;
    procedure CaixaLivre;
    procedure edtDMEnter(Sender: TObject);
    procedure TrataTeclaFuncao(Key: Word);
    procedure btNovoPedidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btNovaParcelaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btResgateChequeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btFinalizarKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btCancelarKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btFuncoesKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSairKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtNumCarnetKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btTrocaCondicaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btIncPagamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btTrocaClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGridDBTableView2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGridDBTableView3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGridDBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtValorKeyPress(Sender: TObject; var Key: Char);
    procedure edtNumPedidoKeyPress(Sender: TObject; var Key: Char);
    procedure btFuncoesClick(Sender: TObject);
    procedure tabPagamentoEnter(Sender: TObject);
    procedure btCartaoPresenteClick(Sender: TObject);
    procedure imgBloquearClick(Sender: TObject);
    procedure bloquearCaixa(nCdResumoCaixa:integer);
    procedure desbloquearCaixa(nCdResumoCaixa:integer);
    procedure imgDesbloquearClick(Sender: TObject);
    procedure ImprimirVale(nCdVale : integer);
    procedure btClienteClick(Sender: TObject);
    procedure RemoverJurosdasParcelas1Click(Sender: TObject);
    procedure btRenegociacaoClick(Sender: TObject);
    procedure cxLabel2Click(Sender: TObject);
    function calculaTotalPago():double;
    procedure FormActivate(Sender: TObject);
    procedure edtJMKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtJMEnter(Sender: TObject);
    procedure edtJMExit(Sender: TObject);
    procedure retiraDescontoPedidos();
    function processarPagamentosTEF : boolean;
    procedure bCancelarRespClick(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure LiquidacaoParcialClick(Sender: TObject);
    procedure PopupMenu2Popup(Sender: TObject);
    procedure qryTerceiroAfterOpen(DataSet: TDataSet);
    procedure ImprimeFinalizaTEF;
    procedure cxPageControl1Change(Sender: TObject);
    procedure prGeraRemessaNum;
    procedure prGeraCFe;
    procedure DBEdit9Change(Sender: TObject);
    function processaPagtoTEFCappta: Boolean;

  private
    { Private declarations }
    FormaPagtoECF : array[1..50] of String;
    fCancelado    : Boolean;
    cFlgSATAtivo  : Boolean;
  public
    { Public declarations }

    nCdTerceiro            : integer ;
    nCdResumoCaixa         : integer ;
    nCdContaBancaria       : Integer ;
    cFlgLiqCrediario       : integer ;
    cFlgUsaConsig          : integer ;
    cCGCPF                 : string ;
    nCdTerceiroPDV         : integer ;

    nSaldoTit              : Extended;

    iQtdeClientes          : integer ;
    nCdTerceiroPadrao      : integer ;
    nCdUsuarioSupervisor   : integer ;
    nPercMaxDescSupervisor : double ;
    cFlgUsaGaveta          : Boolean ;
    cFlgUsaTEF             : Boolean ;
    cModoEnvioCFe          : String;

    { -- Var processamento TEF -- }
    objTEF : TfrmCapptAPI;
    opAprovada : IRespostaOperacaoAprovada;
    opNegada : IRespostaOperacaoRecusada;
    valorTEF : Double;
    qtParcelasTEF : Integer;
    objConsOperadora : TfrmCaixa_ConsOperadora;
    qtCartoes : Integer;
    respMultiCartoes : Integer;
    nCodigoBandeiraCartao : Integer;
    cFlgTipoOperacao : String;

    { -- Var Itera��o TEF -- }
    iteracaoTef : IIteracaoTef;
    tipoIteracao : Integer;
    mensagem : IMensagem;
    reqParametro : IRequisicaoParametro;
    transacoesPendentes : IRespostaTransacaoPendente;
    parametro: string;
    acaoRequisicao: integer;
    transacoesPendentesSafeArray: PSafeArray;
    lBound, uBound, contadorTransacoesPendentes: LongInt;
    transacaoPendente: ITransacaoPendente;
    Contador : Integer;

    { -- Var Impress�o TEF -- }
    confTEF : Boolean;
    cupomCliente, cupomLojista : TStringList;

  end;

const
  Estados : array[TACBrECFEstado] of string = ('N�o Inicializada', 'Desconhecido', 'Livre', 'Venda', 'Pagamento', 'Relat�rio', 'Bloqueada', 'Requer Z', 'Requer X', 'Nao Fiscal' );

var
  frmCaixa_Recebimento: TfrmCaixa_Recebimento;

  iSubTotal, iConta, iTransacao, iQuantasTransacoes, iValorRestante: integer;

  cFormaPgto     : string;
  cNumeroCupom   : string;
  cValorPago     : TStrings;
  arrFormaPagto  : TStrings;
  cIdentificacao : TDateTime;
  Gerencial      : boolean;
  bCupomAberto   : Boolean ;
  nCdCupomFiscal : integer ;
  nCdDoctoFiscal : integer ;
  cFlgUsaECF     : boolean ;
  cFlgUsaECFAux  : boolean ;
  cFlgUsaCFe     : boolean ;
  cFlgUsaCFeAux  : Boolean ;
  cFlgUsaCFeComp : Boolean ;
  cCGCPF         : string ;
  iQtdeVias      : integer ;
  KBHook         : HHook; {this intercepts keyboard input}

  nValorPGInicial : double ;
  nValorDCInicial : double ;
  nValorDMInicial : double ;
  nValorJMInicial : double ;
  nValorPGFinal   : double ;
  cPermDescontoMP : string ;

  nTotalValePresente : double ;
  bCaixaBloqueado    : boolean;
  bItemPromocao      : boolean;
  nValMercadoria     : double ;
  cApuraEstatistica  : string;
  dDtLimiteFech      : TDateTime;

  // Vari�veis TEF
  sCupom : string;
  sData  : string;
  sHora  : string;

  arrCupomTef : Array of TStrings;

  function KeyBoardHook(code: integer; wParam: word; lParam: longword): longword; stdcall;

implementation

uses fMenu, fCaixa_SelCliente, fCaixa_DadosCheque, fCaixa_Cheques,
  fCaixa_ParcelaCred, fPdvFormaPagto, fPdvCondPagto, fCaixa_DadosCartao,
  fPdvSupervisor, fCaixa_ListaParcelas, fClienteVarejoPessoaFisica_Simples,
  fCaixa_LanctoMan, fFuncoesECF, fPdv, fCaixa_Dinheiro,
  fImpDFPadrao, fCaixa_ReembolsoVale, fCaixa_ResgateCheque, pasModuloTEF,
  fMensagem, fCaixa_Suprimento, fCaixa_Sangria, fCaixa_PagtoCheque,
  fCaixa_Funcoes, fCaixa_NumValePresente, fCaixa_ConfUsuarioCaixa,
  fClienteVarejoPessoaFisica, fCaixa_DescontoJuros, fCaixa_ConsPropReneg,
  fCaixa_EfetivaPropReneg, rCaixa_Recebimento_Comprovante,
  fCaixa_CPFNotaFiscal, fCaixa_ParcelaVencida, fTimedMessage, fPdvDesconto,
  fTransacaoTEF, fCaixa_CredErro, fCaixa_Justificativa, fEmiteNFe2,
  fRemessa_Numerario, fCaixa_AtualizaCliente;

  // Leitura da Fun��o FinalizaTransacaoSiTefInterativo a partir da DLL CliSiTef32I.dll
  procedure FinalizaTransacaoSiTefInterativo (smallint: Word;pNumeroCuponFiscal: PChar;pDataFiscal: PChar;pHorario: PChar); far; stdcall;external 'CliSiTef32I.dll';

type
  TPMsg = ^TMsg;
{$R *.dfm}

{-- declara algumas variaveis de objeto que ser�o utilizadas varias vezes no caixa --}
var
    objCaixa_Funcoes         : TfrmCaixa_Funcoes ;
    objCaixa_PagtoCheque     : TfrmCaixa_PagtoCheque ;
    objCaixa_ParcelaCred     : TfrmCaixa_ParcelaCred ;
    objCaixa_Cheques         : TfrmCaixa_Cheques ;
    objCaixa_DadosCheque     : TfrmCaixa_DadosCheque ;
    objCaixa_NumValePresente : TfrmCaixa_NumValePresente ;
    objCaixa_DadosCartao     : TfrmCaixa_DadosCartao ;
    objCaixa_Dinheiro        : TfrmCaixa_Dinheiro ;
    objCaixa_ListaParcelas   : TfrmCaixa_ListaParcelas ;
    objCaixa_ResgateCheque   : TfrmCaixa_ResgateCheque ;
    objPdvFormaPagto         : TfrmPdvFormaPagto ;
    objPdvCondPagto          : TfrmPdvCondPagto  ;

function TfrmCaixa_Recebimento.MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
var
    Mensagem: TForm;
    Portugues: Boolean;
begin

    frmMensagem.lblMensagem.Caption := Msg ;
    frmMensagem.AType               := AType ;
    Result := frmMensagem.ShowModal;
    exit ;

    Portugues := True ;
    Mensagem  := CreateMessageDialog(Msg, AType, Abuttons);

    Mensagem.Font.Name := 'Tahoma' ;
    Mensagem.Font.Size := 8 ;
    Mensagem.Font.Style := [fsBold] ;

    Mensagem.Ctl3D := True ;

    with Mensagem do
    begin

        if Portugues then
        begin

            if Atype = mtConfirmation then
            begin
                Caption := 'ER2Soft - Confirma��o'
            end
            else
            if AType = mtWarning then
            begin
                Caption := 'ER2Soft - Aviso' ;
                Mensagem.Color := clYellow ;
            end
            else if AType = mtError then
            begin
                Caption := 'ER2Soft - Erro' ;
                Mensagem.Color := clRed ;
            end
            else if AType = mtInformation then
                Caption := 'ER2Soft - Informa��o';

        end;

    end;

    if Portugues then
    begin
        TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
        TButton(Mensagem.FindComponent('NO')).Caption := '&N�o';
        TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
        TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
        TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
        TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
        TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
        TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
    end;


    Result := Mensagem.ShowModal;

    Mensagem.Free;
end;

procedure TfrmCaixa_Recebimento.ShowMessage(cTexto: string);
begin
    MessageDLG(cTexto,mtInformation,[mbOK],0) ;
end;

procedure TfrmCaixa_Recebimento.MensagemErro(cTexto: string);
begin
    MessageDLG(cTexto,mtError,[mbOK],0) ;
end;

procedure TfrmCaixa_Recebimento.MensagemAlerta(cTexto: string);
begin
    MessageDLG(cTexto,mtWarning,[mbOK],0) ;
end;

procedure TfrmCaixa_Recebimento.btSairClick(Sender: TObject);
begin

    if (MessageDlg('Confirma o cancelamento do recebimento ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
        exit;

    if ((frmMenu.LeParametro('SENHACANREC') = 'S') and (not qryPedido.IsEmpty)) then //or (not qryParcelas.IsEmpty) or (not qryTempChequeResgate.IsEmpty))) then
    begin
        if (MessageDlg('Para sair � necess�rio a autoriza��o do gerente.' +#13+#13+ 'Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
            exit ;

        if not (AutorizacaoGerente(34)) then
        begin
            MensagemAlerta('Autentica��o Falhou');
            abort ;
        end;
    end;

    close ;
end;

procedure TfrmCaixa_Recebimento.IdentificaCliente() ;
var
    cCPF, cRG     : string ;
label
    IdentificaClienteCheque;
begin

    if (frmMenu.LeParametro('PEDIRDOCCLICX') = 'S') then
    begin

        IdentificaClienteCheque:

        cCPF := '' ;
        cRG  := '' ;

        DbEdit2.Visible := False ;
        DBEdit3.Visible := False ;

        frmMenu.InputQuery('Identifica��o do Cliente','Informe o N�mero do RG',cRG);

        if (cRG = '') then
            frmMenu.InputQuery('Identifica��o do Cliente','Informe o N�mero do CPF:',cCPF);

        if (cCPF <> '') then
            if (frmMenu.TestaCpfCgc(cCPF) = '') then
                Goto IdentificaClienteCheque ;

        if (cCPF <> '') and (trim(qryTerceirocCNPJCPF.Value) <> trim(cCPF)) then
        begin
            MensagemAlerta('CPF digitado n�o confere com o CPF cadastrado do cliente.') ;
            Goto IdentificaClienteCheque ;
        end ;

        if (cRG <> '') and (trim(qryTerceirocRG.Value) <> trim(cRG)) then
        begin
            MensagemAlerta('RG digitado n�o confere com o RG cadastrado do cliente.') ;
            Goto IdentificaClienteCheque ;
        end ;

        DbEdit2.Visible := True ;
        DBEdit3.Visible := True ;
        
        if (trim(cRG) = '') and (trim(cCPF) = '') then
        begin
            MensagemAlerta('O Cliente n�o foi confirmado, favor solicitar autoriza��o do gerente para continuar.') ;

            if not AutorizacaoGerente(20) then
            begin
                MensagemAlerta('Autentica��o falhou.') ;
                abort ;
            end ;
        end ;

    end ;

end ;

function TfrmCaixa_Recebimento.SimularCheque(bEntrada : boolean): double ;
var
    dDtSugerida   : TDateTime ;
    dDtLimite     : TDateTime ;
    nValAux       : Double    ;
    nValTotal     : Double    ;
    nValParcela   : Double    ;
    iParcela      : integer   ;
    nCdTemp_Pagto : integer   ;
    bSeleciona    : boolean   ;
    dDtPrimData   : TDateTime ;
    dDtVencto     : TDateTime ;
    nValEntrada   : double    ;
    cFlgEntrada   : integer   ;
begin

    {-- Gera um ID temporario para o TEMP_PAGTO, pois o registro s� ser� inserido DEPOIS da gera��o das parcelas --}
    nCdTemp_Pagto := 999999 ;

    if (qryTerceiro.eof) or (qryTerceironCdTerceiro.Value = nCdTerceiroPDV) then
    begin
        SelecionaCliente;
    end ;

    {-- Pede o CPF ou RG do cliente para identifica��o do mesmo--}
    IdentificaCliente();

    objCaixa_PagtoCheque.nCdTerceiro               := qryTerceironCdTerceiro.Value ;
    objCaixa_PagtoCheque.edtValCompra.Value        := 0 ;

    objCaixa_PagtoCheque.qryPrepara_Temp.Close   ;
    objCaixa_PagtoCheque.qryPrepara_Temp.ExecSQL ;

    {-- Exclui os cheques que pode ter ficado pendente --}
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('DELETE FROM #Temp_Cheque_Pagamento  WHERE nCdTemp_Pagto = ' + intToStr(nCdTemp_Pagto)) ;
    qryAux.ExecSQL;

    {-- Abre o dataset das parcelas --}
    objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.Close ;
    objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.Parameters.ParamByName('nPK').Value := nCdTemp_Pagto ;
    objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.Open ;

    {-- Abre o dataset das restricoes --}
    objCaixa_PagtoCheque.qryTemp_RestricaoVenda.Close ;
    objCaixa_PagtoCheque.qryTemp_RestricaoVenda.Open ;

    {-- Abre o dataset com as informa��es do cliente --}
    objCaixa_PagtoCheque.qryTerceiro.Close ;
    objCaixa_PagtoCheque.qryTerceiro.Parameters.ParamByName('nPK').Value := qryTerceironCdTerceiro.Value ;
    objCaixa_PagtoCheque.qryTerceiro.Open ;

    dDtPrimData := 0 ;

    objCaixa_PagtoCheque.edtPercEntrCondicao.Value := qryCondPagtonPercEntrada.Value ;

    if (objCaixa_PagtoCheque.qryTerceironPercEntrada.Value > qryCondPagtonPercEntrada.Value) then
    begin
        qryCondPagto.Edit;
        qryCondPagtocFlgPrimParcEnt.Value := 1 ;

        {-- no caso do cheque, nao assume direto o percentual de entrada do cadastro do cliente igual no credi�rio. --}
        {-- Antes faz um teste para saber se, o valor da parcela estabelecido na condi��o de pagamento n�o ser�     --}
        {-- superior ao percentual de entrada que est� no cadastro                                                  --}

        if (objCaixa_PagtoCheque.qryTerceironPercEntrada.Value > (100 / qryCondPagtoiQtdeParcelas.Value)) then
            qryCondPagtonPercEntrada.Value := objCaixa_PagtoCheque.qryTerceironPercEntrada.Value ;

    end ;

    cFlgEntrada := qryCondPagtocFlgPrimParcEnt.Value ;

    if (objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.RecordCount = 0) then
    begin

        nValAux  := 0 ;

        objCaixa_PagtoCheque.nCdTemp_Pagto      := nCdTemp_Pagto     ;

        if not bEntrada then
        begin
            objCaixa_PagtoCheque.edtValCompra.Value := edtVlrPagto.Value ;
            nValTotal                               := edtVlrPagto.Value ;
        end
        else
        begin
            objCaixa_PagtoCheque.edtValCompra.Value := edtEntrada.Value ;
            nValTotal                               := edtEntrada.Value ;
        end ;

        iParcela := 1 ;

        { -- gera as parcelas de acordo com a condi��o de pagamento -- }
        while (iParcela <= qryCondPagtoiQtdeParcelas.Value) do
        begin

            {-- se a primeira data n�o foi preenchida, gera a primeira data --}
            {-- a primeira data � a data de hoje adicionado os dias de prazo da parcelas --}
            {-- existindo a sugest�o de vencimento no cadastro de prazo, esta data substitiu a data calculada --}
            {-- se a condi��o exigir entrada, a dDtPrimData ja foi preenchida com a data de hoje no c�digo acima --}
            if (dDtPrimData = 0) then
            begin

                {-- se tem percentual fixo definido na condi��o aplica --}
                if (qryCondPagtonPercEntrada.Value > 0) then
                    nValEntrada := frmMenu.TBRound(nValTotal * (qryCondPagtonPercEntrada.Value/100),2)
                else nValEntrada := frmMenu.TBRound(nValTotal / qryCondPagtoiQtdeParcelas.Value,2) ; {-- se n�o tem percentual fixo aplica parcelas iguais --}

                nValParcela := nValEntrada ;

                { -- se a condi��o tiver data sugestiva na primeira parcela aplica -- }
                if (qryCondPagtodDtPrimParcela.AsString <> '') then
                    dDtSugerida := qryCondPagtodDtPrimParcela.Value
                else dDtSugerida := StrToDate(DateToStr(Now())) + qryCondPagtoiDiasPrimParc.Value ; {-- ou ent�o adiciona os dias da primeira parcela na data de hoje --}

                dDtLimite   := StrToDate(DateToStr(Now())) + qryCondPagtoiDiasMaxPrimParc.Value ;

                dDtPrimData := dDtSugerida ;
                dDtVencto   := dDtSugerida ;

                if (qryCondPagtocFlgPrimParcEnt.Value = 1) then
                begin
                    dDtSugerida := Date ;
                    dDtLimite   := Date ;
                    dDtPrimData := Date ;
                    dDtVencto   := Date ;
                end ;

            end
            else {-- quando for a partir da segunda parcela, adiciona a primeira data o numero de dias da parcela corrente --}
            begin
                dDtSugerida := dDtSugerida  + qryCondPagtoiDiasProxParc.Value  ;
                dDtLimite   := (dDtSugerida + (qryCondPagtoiDiasMaxPrimParc.Value - qryCondPagtoiDiasPrimParc.Value)) ;
                dDtVencto   := dDtSugerida ;

                { -- calcula o valor das parcelas -- }
                nValParcela := frmMenu.TBRound(((nValTotal-nValEntrada) / (qryCondPagtoiQtdeParcelas.Value-1)),2) ;

            end ;

            if (dDtVencto > dDtLimite) then
                dDtLimite := dDtVencto ;

            {-- acumula na variavel para comparar no final se precisa de arredondamento --}
            nValAux := nValAux + nValParcela ;

            {-- Gera a parcela fisicamente --}
            if (cFlgEntrada = 1) then
                cmd.CommandText := 'INSERT INTO #Temp_Cheque_Pagamento  (cFlgEntrada, nValEntrada,dDtDeposito, dDtLimite, nValCheque, nCdTemp_Pagto) VALUES(1,' + StringReplace(FloatToStr(nValEntrada),',','.',[rfReplaceAll,rfIgnoreCase]) + ',Convert(DATETIME,' + Chr(39) + DateToStr(dDtVencto) + Chr(39) + ',103),Convert(DATETIME,' + Chr(39) + DateToStr(dDtLimite) + Chr(39) + ',103), ' + StringReplace(FloatToStr(nValParcela),',','.',[rfReplaceAll,rfIgnoreCase]) + ',' + IntToStr(nCdTemp_Pagto) + ')'
            else cmd.CommandText := 'INSERT INTO #Temp_Cheque_Pagamento  (dDtDeposito, dDtLimite, nValCheque, nCdTemp_Pagto) VALUES(Convert(DATETIME,' + Chr(39) + DateToStr(dDtVencto) + Chr(39) + ',103),Convert(DATETIME,' + Chr(39) + DateToStr(dDtLimite) + Chr(39) + ',103), ' + StringReplace(FloatToStr(nValParcela),',','.',[rfReplaceAll,rfIgnoreCase]) + ',' + IntToStr(nCdTemp_Pagto) + ')' ;

            cmd.Execute;

            cFlgEntrada := 0 ;
            iParcela    := iParcela + 1 ;
        end ;

        {-- verifica se a parcela precisa de algum arredondamento -- }
        if (nValAux < nValTotal) then
        begin
            cmd.CommandText := 'UPDATE #Temp_Cheque_Pagamento Set nValCheque = nValCheque + ' + StringReplace(FloatToStr(nValTotal - nValAux),',','.',[rfReplaceAll,rfIgnoreCase]) + ' WHERE dDtDeposito = (SELECT MIN(dDtDeposito) FROM #Temp_Cheque_Pagamento WHERE nCdTemp_Pagto = 999999) AND nCdTemp_Pagto =' + IntToStr(nCdTemp_Pagto) ;
            cmd.Execute ;
        end ;

        if (nValAux > nValTotal) then
        begin
            cmd.CommandText := 'UPDATE #Temp_Cheque_Pagamento Set nValCheque = nValCheque + ' + StringReplace(FloatToStr(nValTotal - nValAux),',','.',[rfReplaceAll,rfIgnoreCase]) + ' WHERE dDtDeposito = (SELECT MAX(dDtDeposito) FROM #Temp_Cheque_Pagamento WHERE nCdTemp_Pagto = 999999) AND nCdTemp_Pagto =' + IntToStr(nCdTemp_Pagto) ;
            cmd.Execute ;
        end ;

        objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.Close ;
        objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.Parameters.ParamByName('nPK').Value := nCdTemp_Pagto ;
        objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.Open ;

    end ;

    if (qryCondPagto.State <> dsBrowse) then
        qryCondPagto.Cancel;

    objCaixa_PagtoCheque.edtCondPagto.Text := qryCondPagtocNmCondPagto.Value ;
    objCaixa_PagtoCheque.cFlgLiqCrediario  := cFlgLiqCrediario ;
    objCaixa_PagtoCheque.ShowModal;

end ;

procedure TfrmCaixa_Recebimento.SimularCrediario();
var
    dDtSugerida   : TDateTime ;
    dDtLimite     : TDateTime ;
    dDtVenctoAux  : TDateTime ;
    nValAux       : Double    ;
    nValTotal     : Double    ;
    nValParcela   : Double    ;
    iParcela      : integer   ;
    nCdTemp_Pagto : integer   ;
    bSeleciona    : boolean   ;
    bTemEntrada   : boolean   ;
    nValEntrada   : double    ;
    dDtPrimData   : TDateTime ;
    dDtVencto     : TDateTime ;
begin

    {-- Gera um ID temporario para o TEMP_PAGTO, pois o registro s� ser� inserido DEPOIS da gera��o das parcelas --}
    nCdTemp_Pagto := 999999 ;

    if (qryTerceiro.eof) or (qryTerceironCdTerceiro.Value = nCdTerceiroPDV) then
    begin
        MensagemErro('N�o � poss�vel gerar um credi�rio sem um cliente selecionado.') ;
        abort ;
    end ;

    {-- Pede o CPF ou RG do cliente para identifica��o do mesmo--}
    IdentificaCliente();

    objCaixa_ParcelaCred.nCdTerceiro               := qryTerceironCdTerceiro.Value ;
    objCaixa_ParcelaCred.edtValEntrada.Value       := 0 ;
    objCaixa_ParcelaCred.edtValEntradaMinima.Value := 0 ;
    objCaixa_ParcelaCred.edtValCompra.Value        := 0 ;
    objCaixa_ParcelaCred.edtValorFinanciar.Value   := 0 ;

    objCaixa_ParcelaCred.qryPrepara_Temp.Close   ;
    objCaixa_ParcelaCred.qryPrepara_Temp.ExecSQL ;

    {-- Exclui as parcelas de um credi�rio que pode ter ficado pendente --}
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('DELETE FROM #Temp_Crediario WHERE nCdTemp_Pagto = ' + intToStr(nCdTemp_Pagto)) ;
    qryAux.ExecSQL;

    {-- Abre o dataset das parcelas --}
    objCaixa_ParcelaCred.qryTemp_Crediario.Close ;
    objCaixa_ParcelaCred.qryTemp_Crediario.Parameters.ParamByName('nCdTemp_Pagto').Value := nCdTemp_Pagto ;
    objCaixa_ParcelaCred.qryTemp_Crediario.Open ;

    {-- Abre o dataset das restricoes --}
    objCaixa_ParcelaCred.qryTemp_RestricaoVenda.Close ;
    objCaixa_ParcelaCred.qryTemp_RestricaoVenda.Open ;

    {-- Abre o dataset com as informa��es do cliente --}
    objCaixa_ParcelaCred.qryTerceiro.Close ;
    objCaixa_ParcelaCred.qryTerceiro.Parameters.ParamByName('nPK').Value := qryTerceironCdTerceiro.Value ;
    objCaixa_ParcelaCred.qryTerceiro.Open ;

    bTemEntrada := False ;
    nValEntrada := 0 ;
    dDtPrimData := 0 ;

    objCaixa_ParcelaCred.edtPercEntrCondicao.Value := qryCondPagtonPercEntrada.Value ;

    if (objCaixa_ParcelaCred.qryTerceironPercEntrada.Value > qryCondPagtonPercEntrada.Value) then
    begin
        {-- altera temporariamente o registro e nao salva, somente para processamento abaixo }
        qryCondPagto.Edit;
        qryCondPagtocFlgPrimParcEnt.Value := 1 ;
        qryCondPagtonPercEntrada.Value    := objCaixa_ParcelaCred.qryTerceironPercEntrada.Value ;

        if (qryCondPagtoiQtdeParcelas.Value <= 1) then
            qryCondPagtoiQtdeParcelas.Value := 2 ;
    end ;

    if (objCaixa_ParcelaCred.qryTemp_Crediario.RecordCount = 0) then
    begin

        nValAux   := 0 ;

        objCaixa_ParcelaCred.nCdTemp_Pagto           := nCdTemp_Pagto     ;
        objCaixa_ParcelaCred.edtValCompra.Value      := edtVlrPagto.Value ;
        objCaixa_ParcelaCred.edtValorFinanciar.Value := edtVlrPagto.Value ;
        nValTotal                                    := edtVlrPagto.Value ;

        iParcela := 1 ;

        {-- se a condi��o tem entrada --}
        if (qryCondPagtocFlgPrimParcEnt.Value = 1) then
        begin

            {-- se a primeira parcela for entrada, coloca a primeira data como hoje --}
            dDtPrimData := Date ;
            dDtVencto   := Date ;
            dDtSugerida := Date ;
            dDtLimite   := Date ;

            {-- se tem percentual fixo definido na condi��o aplica --}
            if (qryCondPagtonPercEntrada.Value > 0) then
                nValEntrada := frmMenu.TBRound(nValTotal * (qryCondPagtonPercEntrada.Value/100),2)
            else nValEntrada := frmMenu.TBRound(nValTotal / qryCondPagtoiQtdeParcelas.Value,2) ; {-- se n�o tem percentual fixo aplica parcelas iguais --}

            {-- retira os centavos da entrada --}
            nValEntrada := nValEntrada - Frac(nValEntrada) ;

            bTemEntrada := True ;

            objCaixa_ParcelaCred.edtValEntrada.Value       := nValEntrada ;
            objCaixa_ParcelaCred.edtValEntradaMinima.Value := nValEntrada ;

            {-- gera o registro da entrada na tabela tempor�ria --}
            cmd.CommandText := 'INSERT INTO #Temp_Crediario (dDtVencto, dDtSugerida, dDtLimite, nValParcela, cFlgEntrada, nCdTemp_Pagto, nPercent, iDiasPrazo) VALUES(Convert(DATETIME,' + Chr(39) + DateToStr(Date) + Chr(39) + ',103),Convert(DATETIME,' + Chr(39) + DateToStr(Date) + Chr(39) + ',103),Convert(DATETIME,' + Chr(39) + DateToStr(Date) + Chr(39) + ',103), ' + StringReplace(FloatToStr(nValEntrada),',','.',[rfReplaceAll,rfIgnoreCase]) + ',1,' + IntToStr(nCdTemp_Pagto) + ',' + StringReplace(qryCondPagtonPercEntrada.asString,',','.',[rfReplaceAll,rfIgnoreCase]) + ',0)' ;
            cmd.Execute;

            iParcela := iParcela + 1 ;
        end ;

        { -- calcula o valor das parcelas -- }
        nValParcela := frmMenu.TBRound(((nValTotal-nValEntrada) / qryCondPagtoiQtdeParcelas.Value),2) ;

        { -- gera as parcelas de acordo com a condi��o de pagamento -- }
        while (iParcela <= qryCondPagtoiQtdeParcelas.Value) do
        begin

            {-- se a primeira data n�o foi preenchida, gera a primeira data --}
            {-- a primeira data � a data de hoje adicionado os dias de prazo da parcelas --}
            {-- existindo a sugest�o de vencimento no cadastro de prazo, esta data substitiu a data calculada --}
            {-- se a condi��o exigir entrada, a dDtPrimData ja foi preenchida com a data de hoje no c�digo acima --}
            if (dDtPrimData = 0) then
            begin

                { -- se a condi��o tiver data sugestiva na primeira parcela aplica -- }
                if (qryCondPagtodDtPrimParcela.AsString <> '') then
                    dDtSugerida := qryCondPagtodDtPrimParcela.Value
                else dDtSugerida := StrToDate(DateToStr(Now())) + qryCondPagtoiDiasPrimParc.Value ; {-- ou ent�o adiciona os dias da primeira parcela na data de hoje --}

                { -- se a data de vencimento da primeira parcela for menor que hoje, sugere a data de hoje na parcela --}
                if (dDtSugerida < Date) then
                    dDtSugerida := Date ;

                { -- erro arrumado-- }
                if (qryCondPagtoiDiasPrimParc.Value > 0) then
                    dDtLimite   := (dDtSugerida + (qryCondPagtoiDiasMaxPrimParc.Value - qryCondPagtoiDiasProxParc.Value)) 
                else
                    dDtLimite   := StrToDate(DateToStr(Now())) + qryCondPagtoiDiasMaxPrimParc.Value ;

                dDtPrimData := dDtSugerida ;
                dDtVencto   := dDtSugerida ;

                {if (DayOfWeek(dDtSugerida) = 1) then // domingo
                    dDtVencto := dDtSugerida + 1 ;

                if (DayOfWeek(dDtLimite) = 1) then // domingo
                    dDtLimite := dDtLimite + 1 ;}



            end
            else {-- quando for a partir da segunda parcela, adiciona a primeira data o numero de dias da parcela corrente --}
            begin

                dDtSugerida := dDtSugerida  + qryCondPagtoiDiasProxParc.Value  ;
                dDtLimite   := (dDtSugerida + (qryCondPagtoiDiasMaxPrimParc.Value - qryCondPagtoiDiasProxParc.Value)) ;
                dDtVencto   := dDtSugerida ;

                {-- se o vencimento for no domingo, soma um dia a mais --}
                {if (DayOfWeek(dDtSugerida) = 1) then // domingo
                    dDtVencto := dDtSugerida + 1 ;}
            end ;

            if (dDtVencto > dDtLimite) then
                dDtLimite := dDtVencto ;

            {-- acumula na variavel para comparar no final se precisa de arredondamento --}
            nValAux := nValAux + nValParcela ;

            {-- Gera a parcela fisicamente --}
            cmd.CommandText := 'INSERT INTO #Temp_Crediario (dDtVencto, dDtSugerida, dDtLimite, nValParcela, cFlgEntrada, nCdTemp_Pagto, nPercent, iDiasPrazo) VALUES(Convert(DATETIME,' + Chr(39) + DateToStr(dDtVencto) + Chr(39) + ',103),Convert(DATETIME,' + Chr(39) + DateToStr(dDtVencto) + Chr(39) + ',103),Convert(DATETIME,' + Chr(39) + DateToStr(dDtLimite) + Chr(39) + ',103), ' + StringReplace(FloatToStr(nValParcela),',','.',[rfReplaceAll,rfIgnoreCase]) + ',0,' + IntToStr(nCdTemp_Pagto) + ',0,' + qryCondPagtoiDiasProxParc.asString + ')' ;
            cmd.Execute;

            iParcela  := iParcela + 1 ;
        end ;

        { -- verifica se a parcela precisa de algum arredondamento -- }
        if (nValAux < (nValTotal - nValEntrada)) then
        begin
            cmd.CommandText := 'UPDATE #Temp_Crediario Set nValParcela = nValParcela + ' + StringReplace(FloatToStr(nValTotal - nValAux),',','.',[rfReplaceAll,rfIgnoreCase]) + ' WHERE dDtVencto = (SELECT MIN(dDtVencto) FROM #Temp_Crediario WHERE cFlgEntrada = 0 AND nCdTemp_Pagto = 999999) AND cFlgEntrada = 0 AND nCdTemp_Pagto =' + IntToStr(nCdTemp_Pagto) ;
            cmd.Execute ;
        end ;

        if (nValAux > (nValTotal - nValEntrada)) then
        begin
            cmd.CommandText := 'UPDATE #Temp_Crediario Set nValParcela = nValParcela - ' + StringReplace(FloatToStr(nValTotal - nValAux),',','.',[rfReplaceAll,rfIgnoreCase]) + ' WHERE dDtVencto = (SELECT MAX(dDtVencto) FROM #Temp_Crediario WHERE cFlgEntrada = 0 AND nCdTemp_Pagto = 999999) AND cFlgEntrada = 0 AND nCdTemp_Pagto =' + IntToStr(nCdTemp_Pagto) ;
            cmd.Execute ;
        end ;

        // quando a condi��o de pagamento n�o obriga entrada, gera uma parcela de entrada zerada, para poder
        // na tela de parcelas do credi�rio digitar o valor de uma entrada para simula��o.
        // isso ser� usado no caso de estouro de limite de cr�dito
        if not (bTemEntrada) then
        begin
            cmd.CommandText := 'INSERT INTO #Temp_Crediario (dDtVencto, dDtSugerida, dDtLimite, nValParcela, cFlgEntrada, nCdTemp_Pagto, nPercent, iDiasPrazo) VALUES(Convert(DATETIME,' + Chr(39) + DateToStr(Date) + Chr(39) + ',103),Convert(DATETIME,' + Chr(39) + DateToStr(Date) + Chr(39) + ',103),Convert(DATETIME,' + Chr(39) + DateToStr(Date) + Chr(39) + ',103),0,1,' + IntToStr(nCdTemp_Pagto) + ',0,0)' ;
            cmd.Execute;
        end ;
        
        objCaixa_ParcelaCred.qryTemp_Crediario.Close ;
        objCaixa_ParcelaCred.qryTemp_Crediario.Parameters.ParamByName('nCdTemp_Pagto').Value := nCdTemp_Pagto ;
        objCaixa_ParcelaCred.qryTemp_Crediario.Open ;

        { -- analisa datas cadastradas no calend�rio para n�o utilizar na sugest�o de vencimento -- }
        objCaixa_ParcelaCred.VerificaCalendario;

        objCaixa_ParcelaCred.edtValorFinanciar.Value := objCaixa_ParcelaCred.edtValorFinanciar.Value - objCaixa_ParcelaCred.edtValEntrada.Value ;

        objCaixa_ParcelaCred.iDiasProxParc := qryCondPagtoiDiasProxParc.Value;
    end ;

    if (qryCondPagto.State = dsEdit) then
        qryCondPagto.Cancel;

    objCaixa_ParcelaCred.edtCondPagto.Text := qryCondPagtocNmCondPagto.Value ;

    objCaixa_ParcelaCred.bProcessado := False ;
    objCaixa_ParcelaCred.ShowModal;

end ;

procedure TfrmCaixa_Recebimento.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  // F2
  if (key = 113) then
  begin
      btNovoPedido.Click;
  end ;

  // F3
  if (key = 114) then
  begin
      btNovaParcela.Click;
  end ;

  if key = 13 then
  begin

    key := 0;
    perform (WM_NextDlgCtl, 0, 0);

  end;

end;

procedure TfrmCaixa_Recebimento.edtNumPedidoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  TrataTeclaFuncao(Key) ;
end;

procedure TfrmCaixa_Recebimento.btTrocaCondicaoClick(Sender: TObject);
var
    nCdFormaPagto, nCdCondPagto : integer ;
begin

    {if (edtSaldo.Value <= 0) then
    begin
        MensagemAlerta('Nenhum saldo dispon�vel para pagamento.') ;
        btFinalizar.SetFocus;
        exit ;
    end ;}

    if ((edtValor.Value + edtJM.Value) <= 0) then
    begin
        MensagemAlerta('Nenhum saldo dispon�vel para pagamento.') ;
        btFinalizar.SetFocus;
        exit ;
    end ;

    cFlgLiqCrediario := 0 ;

    if (qryParcelas.Active) and (qryParcelas.RecordCount > 0) then
        cFlgLiqCrediario := 1 ;

    if (qryTempChequeResgate.Active) and (qryTempChequeResgate.RecordCount > 0) then
        cFlgLiqCrediario := 1 ;

    {-- se for venda, verifica se tem desconto manual, se tiver, avisa o operador --}
    if (nValorDMInicial > 0) and (cFlgLiqCrediario = 0) then
    begin

        if (messageDlg('O(s) pedido(s) tem desconto informado pelo vendedor.' +#13#13 + 'Alterando o pagamento este desconto ser� automaticamente removido. Valor do Desconto:  '+ Format('%m',[nValorDMInicial]) +#13#13 + 'Deseja Continuar ?',mtConfirmation,[mbYes,mbNo],0) = MRNO) then
        begin
            btIncPagamento.SetFocus;
            exit ;
        end
        else
        begin
            retiraDescontoPedidos();
        end ;

    end ;

    {-- se for venda, verifica se tem desconto de condi��o, se tiver, avisa o operador --}
    if (nValorDCInicial < 0) and (cFlgLiqCrediario = 0) then
    begin

        if (messageDlg('O(s) pedido(s) tem desconto autom�tico pela condi��o de pagamento selecionada pelo vendedor.' +#13#13 + 'Alterando o pagamento este desconto ser� automaticamente removido. Valor do Desconto:  '+ Format('%m',[nValorDCInicial]) +#13#13 + 'Deseja Continuar ?',mtConfirmation,[mbYes,mbNo],0) = MRNO) then
        begin
            btIncPagamento.SetFocus;
            exit ;
        end
        else
        begin
            retiraDescontoPedidos();
        end ;

    end ;

    {-- se for venda, verifica se tem juros de condi��o, se tiver, avisa o operador --}
    if (nValorDCInicial > 0) and (cFlgLiqCrediario = 0) then
    begin

        if (messageDlg('O(s) pedido(s) tem acr�scimo autom�tico pela condi��o de pagamento selecionada pelo vendedor.' +#13#13 + 'Alterando o pagamento este desconto ser� automaticamente removido. Valor do Acr�scimo:  '+ Format('%m',[nValorDCInicial]) +#13#13 + 'Deseja Continuar ?',mtConfirmation,[mbYes,mbNo],0) = MRNO) then
        begin
            btIncPagamento.SetFocus;
            exit ;
        end
        else
        begin
            retiraDescontoPedidos();
        end ;

    end ;

    {-- seleciona a forma de pagamento --}
    objPdvFormaPagto.cFlgLiqCrediario := cFlgLiqCrediario ;
    nCdFormaPagto                     := objPdvFormaPagto.SelecionaFormaPagto() ;

    if (nCdFormaPagto <= 0) then
        abort ;

    {-- passa os parametros para a tela de consulta de condi��es --}
    objPdvCondPagto.nCdLoja               := frmMenu.nCdLojaAtiva;
    objPdvCondPagto.nCdFormaPagto         := nCdFormaPagto       ;
    objPdvCondPagto.cFlgLiqCrediario      := cFlgLiqCrediario    ;
    objPdvCondPagto.bItemPromocao         := bItemPromocao       ;
    objPdvCondPagto.edtValorPagar.Value   := frmMenu.TBRound((edtValor.Value + edtJM.Value),2) ;
    objPdvCondPagto.edtSaldo.Value        := frmMenu.TBRound((edtValor.Value + edtJM.Value),2) ;
    objPdvCondPagto.cPermDesconto         := 'S' ;
    objPdvCondPagto.edtValorPagar.Enabled := True ;

    {-- se o valor que est� sendo pago for menor que o valor total a pagar, verifica se permite desconto para multiplos pagamentos --}
    if ((edtValor.Value + edtJM.Value) < edtTotalPagar.Value) then
        objPdvCondPagto.cPermDesconto := cPermDescontoMP ;

    {-- Processa a consulta --}
    nCdCondPagto  := objPdvCondPagto.SelecionaCondPagto() ;

    {-- se o operador desistiu da consulta, retorna por aqui --}
    if (nCdCondPagto <= 0) then
        abort ;

    {-- s� vai no banco de dados se n�o foi selecionada condi��o no pdv
    {-- ou se a condi��o selecionada agora no caixa for diferente do que a selecionada no pdv
    {-- este processo roda online via internet e quanto menos for ao banco de dados melhor --}
    if qryCondPagto.eof or (nCdCondPagto <> qryCondPagtonCdCondPagto.Value) then
    begin
        qryCondPagto.Close ;
        qryCondPagto.Parameters.ParamByName('nCdCondPagto').Value := nCdCondPagto ;
        qryCondPagto.Parameters.ParamByName('nCdLoja').Value      := frmMenu.nCdLojaAtiva;
        qryCondPagto.Open ;
    end ;

    {-- Atualiza os totais --}
    {-- o desconto da condi��o � somando no total porque quando � desconto vem negativo, e quando � juros vem positivo --}

    if (nCdCondPagto <> qryCondPagtonCdCondPagto.Value) or ((nCdCondPagto = qryCondPagtonCdCondPagto.Value) and ((strTofloat(edtVlrPagto.EditValue) + strToFloat(edtDM.EditValue)) <> objPdvCondPagto.qryCondPagtonValorTOtal.Value)) then
    begin
        edtValor.Value    := objPdvCondPagto.edtValorPagar.Value ;
        edtDC.Value       := (objPdvCondPagto.qryCondPagtonValorTotal.Value - (edtValor.Value + edtJM.Value)) ;
        edtDM.Value       := 0 ;
        edtJM.Value       := 0 ;
        edtVlrPagto.Value := edtValor.Value + edtDC.Value - edtDM.Value + edtJM.Value ;

        AtivaValores;
    end ;


    {-- se nenhum cliente foi selecionado e a forma de pagamento exige, obriga selecionar um agora --}
    if (cFlgLiqCrediario = 0) then
        if (qryCondPagtocFlgExigeCliente.Value = 1) and (qryTerceiro.eof or (qryTerceironCdTerceiro.Value = nCdTerceiroPDV)) then
            SelecionaCliente;

    if edtValor.Enabled then
        edtValor.SetFocus
    else btIncPagamento.SetFocus;

end;

procedure TfrmCaixa_Recebimento.cxButton4Click(Sender: TObject);
begin

    if not qryCondPagto.Active then
        exit ;

    if (qryCondPagtonCdTabTipoFormaPagto.Value <> 5) then
    begin
        MensagemAlerta('A forma de pagamento n�o permite credi�rio.') ;
        exit ;
    end ;

    objCaixa_ParcelaCred.qryTemp_Crediario.Close;

    frmMenu.ZeraTemp('#Temp_Crediario');

end;

procedure TfrmCaixa_Recebimento.CalculaSaldo();
var
    nValpagto : double ;
begin

    if ((abs(nValorDMInicial) + abs(nValorDCInicial)) > 0) then
    begin

        try
            qryAux.Close ;
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('IF OBJECT_ID(' + Chr(39) + 'tempdb..#Temp_Pagtos' + Chr(39) + ') IS NOT NULL SELECT Convert(DECIMAL(12,2),IsNull(Sum(nValPagto),0)) FROM #Temp_Pagtos ELSE SELECT 0') ;
            qryAux.Open ;

            nValPagto := qryAux.FieldList[0].Value ;
        except
            nValPagto := 0 ;
        end ;

    end
    else
    begin

        try
            qryAux.Close ;
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('IF OBJECT_ID(' + Chr(39) + 'tempdb..#Temp_Pagtos' + Chr(39) + ') IS NOT NULL SELECT Convert(DECIMAL(12,2),IsNull(Sum(nValPagto - nValJurosCondPagto + nValDesconto),0)) FROM #Temp_Pagtos ELSE SELECT 0') ;
            qryAux.Open ;

            nValPagto := qryAux.FieldList[0].Value ;
        except
            nValPagto := 0 ;
        end ;

    end ;

    edtValorPago.Value  := nValPagto ;

    edtTotalPagar.Value := (edtPedidos.Value + edtParcelas.Value + edtChequeResg.Value) ;
    edtSaldo.Value      := (edtPedidos.Value + edtParcelas.Value + edtChequeResg.Value) - nValPagto ;
    edtSaldo.Value      := frmMenu.TBRound(edtSaldo.Value,2) ;

end ;


procedure TfrmCaixa_Recebimento.MaskEdit3Exit(Sender: TObject);
begin

{    try
        cValor := formatcurr('0.00',StrToFloat(MaskEdit3.Text)) ;
        MaskEdit3.Text := cValor ;
    except
        MensagemAlerta('Valor inv�lido.') ;
        exit ;
    end ; }

    CalculaSaldo() ;

    btFinalizar.SetFocus;

end;

procedure TfrmCaixa_Recebimento.btNovoPedidoClick(Sender: TObject);
var
    nValPedido : double  ;
    cEstadoECF : string  ;
    objPDV     : TfrmPDV ;
begin

    if (dDtLimiteFech < Date) then
    begin
        MensagemErro('Este caixa requer fechamento imediato.');
        Abort;
    end
    else
    begin
        VerificaSaldoCaixa(False);
    end;

    if (cFlgUsaECF) then
    begin

        cEstadoECF := EstadoECF() ;

        if (cEstadoECF <> 'Livre') then
        begin
            MensagemAlerta('Estado da impressora ECF n�o permite abertura de um novo cupom fiscal. Estado : ' + cEstadoECF);
        end ;

    end ;

    nValMercadoria       := 0 ;
    nCdDoctoFiscal       := 0 ;
    bItemPromocao        := False ;
    tabPedidos.Enabled   := True  ;
    tabParcelas.Enabled  := False ;
    tabCheques.Enabled   := False ;
    tabPagamento.Enabled := True  ;
    imgCaixaLivre.Visible:= False ;
    GroupBox1.Enabled    := true ;

    btFinalizar.Enabled     := True ;
    btCancelar.Enabled      := True ;
    btNovoPedido.Enabled    := False ;
    btNovaParcela.Enabled   := False ;
    btResgateCheque.Enabled := False ;
    //cxButton6.Enabled       := False ;
    btFuncoes.Enabled       := False ;

    cFlgLiqCrediario := 0 ;
    nValorPGInicial  := 0 ;
    nValorDCInicial  := 0 ;
    nValorDMInicial  := 0 ;
    nValorJMInicial  := 0 ;
    nValorPGFinal    := 0 ;

    DesativaValores ;

    cxPageControl1.ActivePage := tabPedidos ;

    edtNumPedido.Enabled := True ;
    edtNumPedido.SetFocus ;

    {-- se a loja n�o utiliza pr�-venda, abre neste momento a tela de pr�-venda para bipar os produtos --}
    if ((frmMenu.cFlgUsaPDV = 0) and (cFlgUsaConsig = 0)) then
    begin

        objPDV := TfrmPDV.Create( Self ) ;

        objPDV.iNrPedidoExterno := 0 ;
        objPDV.cOrigemCaixa     := 1 ;
        objPDV.ShowModal ;

        //Atualiza o saldo do pedidos
        if (objPDV.iNrPedidoExterno > 0) then
            edtNumPedido.Text := intToStr(objPDV.iNrPedidoExterno);

        freeAndNil( objPDV ) ;

    end ;

end;

procedure TfrmCaixa_Recebimento.InativaFuncoes();
begin
    btNovoPedido.Enabled    := False ;
    btNovaParcela.Enabled   := False ;
    btResgateCheque.Enabled := False ;
end ;

function TfrmCaixa_Recebimento.AutorizacaoGerente(nCdTipoLiberacao:integer): boolean;
var
    objPDVSupervisor : TfrmPDVSupervisor ;
begin

    nCdUsuarioSupervisor := 0 ;

    objPDVSupervisor := TfrmPDVSupervisor.Create( Self ) ;

    objPDVSupervisor.edtCodigo.Text := ''    ;
    objPDVSupervisor.edtSenha.Text  := ''    ;
    objPDVSupervisor.cResultado     := False ;
    objPDVSupervisor.nCdLoja        := frmMenu.nCdLojaAtiva ;

    objPDVSupervisor.ShowModal;

    if not objPDVSupervisor.cResultado then
    begin
        Result := false ;
        exit ;
    end ;

    {-- verifica se o gerente tem acesso ao tipo de direito solicitado --}
    qryConfereDireito.Close;
    qryConfereDireito.Parameters.ParambyName('nCdUsuario').Value            := objPDVSupervisor.edtCodigo.Value;
    qryConfereDireito.Parameters.ParambyName('nCdTipoDireitoGerente').Value := nCdTipoLiberacao;
    qryConfereDireito.Open ;

    if qryConfereDireito.Eof then
    begin
        freeAndNil( objPDVSupervisor ) ;
        MensagemAlerta('Voc� n�o tem permiss�o para liberar esta solicita��o.' +#13#13+'C�digo do Direito: ' + frmMenu.ZeroEsquerda(intToStr(nCdTipoLiberacao),3)) ;
        Result := False ;
        exit ;
    end ;

    qryConfereDireito.Close ;

    nCdUsuarioSupervisor   := strToInt(objPDVSupervisor.edtCodigo.EditValue) ;
    nPercMaxDescSupervisor := objPDVSupervisor.qryConfereUsuarionPercMaxDesc.Value ;

    freeAndNil( objPDVSupervisor ) ;

    Result := True ;

end ;

procedure TfrmCaixa_Recebimento.FormShow(Sender: TObject);
var
   cArquivo   : TextFile;
   cLinha     : string ;
   cEstadoECF : string ;
begin

    fCancelado := False ;

    {if FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoCaixa.JPG') then
        imgLogoCaixa.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoCaixa.JPG')
    else if FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoCaixa.JPEG') then
        imgLogoCaixa.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoCaixa.JPEG')
    else if FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoCaixa.GIF') then
        imgLogoCaixa.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoCaixa.GIF')
    else if FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoCaixa.BMP') then
        imgLogoCaixa.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoCaixa.BMP')}

    qryAmbiente.Close;
    qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
    qryAmbiente.Open;

    if (Trim(qryAmbientecPathLogoCaixa.Value) = '') then
    begin
        if (FileExists(ExtractFilePath(Application.ExeName) + 'Imagens\LogoCaixaPadrao.JPG')) then
            imgLogoCaixa.Picture.LoadFromFile(ExtractFilePath(Application.ExeName) + 'Imagens\LogoCaixaPadrao.JPG');
    end
    else
    begin
        if (FileExists(qryAmbientecPathLogoCaixa.Value)) then
            imgLogoCaixa.Picture.LoadFromFile(qryAmbientecPathLogoCaixa.Value);
    end;

    Application.ProcessMessages;

end;

procedure TfrmCaixa_Recebimento.btFinalizarClick(Sender: TObject);
var
  nCdLanctoFin           : Integer;
  nCdCrediario_Out       : Integer;
  nCdValeDesconto_Out    : Integer;
  iRetorno               : Integer;
  iVezes                 : Integer;
  nCdVale                : Integer;
  cFlgValePresente       : String;
  cFlgValeMercadoria     : String;
  cFlgValeDesconto       : String;
  cLinha                 : String;
  cArquivo               : TextFile;
  objCaixa_CPFNotaFiscal : TfrmCaixa_CPFNotaFiscal;
  I                      : Integer;
  ppMarcaImpSAT : TACBrPosPrinterModelo;
  pcPagCodigo   : TACBrPosPaginaCodigo;

  Status: TACBrPosPrinterStatus;
  ii: TACBrPosTipoStatus;
  AStr: String;
label
  solicitaCPFCNPJ;
begin


 //   ShowMessage("Entrei em Recebimento");
    if (qryTemp_Pedido.IsEmpty) and (qryParcelas.IsEmpty) and (qryTempChequeResgate.IsEmpty) and (qryTemp_Pagtos.IsEmpty) then
        Abort;

    if (edtSaldo.Value > 0) then
    begin
        MensagemAlerta('Para encerrar o recebimento, o saldo a pagar n�o pode ser maior que zero.');
        cxPageControl1.ActivePage := tabPagamento;
        btIncPagamento.SetFocus;
        Exit;
    end;

    {-- se teve venda de vale presente e o parametro diz para pedir a numera��o, chama a tela de entrada de cart�o presente --}
    if (nTotalValePresente > 0) then
        if (frmMenu.LeParametro('PEDIRNUMVPCX') = 'S') then
        begin
            objCaixa_NumValePresente.edtValorTotal.Value := nTotalValePresente;
            if (objCaixa_NumValePresente.ShowModal = MRNo) then
                exit ;
        end ;

    case MessageDlg('Confirma a finaliza��o do recebimento ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo:exit ;
    end ;

    nCdLanctoFin        := 0   ;
    cFlgValePresente    := 'N' ;
    cFlgValeMercadoria  := 'N' ;
    cFlgValeDesconto    := 'N' ;
    nCdCrediario_Out    := 0   ;
    nCdValeDesconto_Out := 0   ;

    if not qryCondPagto.Active then
        qryCondPagto.Open ;

    if (Trim(objCaixa_DadosCartao.edtnCdOperadoraCartao.Text) = '') then
        objCaixa_DadosCartao.edtnCdOperadoraCartao.Text := '0' ;

    {-- o cupom fiscal ser� aberto na inclus�o do primeiro pagamento, por�m, no caso de trocas sem valor a pagar --}
    {-- nenhum pagamento ser� incluso, ent�o, testa aqui se o cupom est� aberto --}

    if (cFlgUsaECF) and (nValMercadoria > 0) then
    begin

        if (cFlgLiqCrediario = 0) then
        begin

            // Abre o cupom fiscal
            if (cFlgUsaECF) and (EstadoECF() = 'Livre') then
            begin
                Sleep(300);

                btFinalizar.Enabled := false ;

                AbreCupomFiscal() ;

                Sleep(300);

                btFinalizar.Enabled := true ;

                lblMensagem.Caption := 'Totalizando cupom...' ;

                qrySomaDescAcres.Close ;
                qrySomaDescAcres.Open ;

                Sleep(300);
                frmMenu.ACBrECF1.SubtotalizaCupom((qrySomaDescAcresnValAcrescimo.Value - qrySomaDescAcresnValDesconto.Value), frmMenu.cMsgCupomFiscal );
                Sleep(300);
                lblMensagem.Caption := '' ;

            end ;

        end ;

    end ;

    {-- INICIO TEF: Processa os pagamentos com TEF , caso existam --}
    confTEF := False;



    if (cFlgUsaTEF) then
      begin
         frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Antes de processaPagtoTEFCappta' );
         if not (processaPagtoTEFCappta) then
          begin
            btFinalizar.Enabled := True;
            frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Apos de processaPagtoTEFCappta:Abortado' );
            Abort;
          end;
         frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Apos de processaPagtoTEFCappta:OK' );
      end;

    if (Trim(objCaixa_DadosCartao.edtnCdOperadoraCartao.Text) = '') then
        objCaixa_DadosCartao.edtnCdOperadoraCartao.Text := '0';

    if (cFlgUsaECF) and (nValMercadoria > 0) then
    begin

        if (cFlgLiqCrediario = 0) then
        begin

            // Abre o cupom fiscal
            if (cFlgUsaECF) and (EstadoECF() = 'Livre') then
            begin
                Sleep(300);

                AbreCupomFiscal();

                Sleep(300);

                //btFinalizar.Enabled := true ;

                lblMensagem.Caption := 'Totalizando cupom...' ;

                qrySomaDescAcres.Close ;
                qrySomaDescAcres.Open ;

                Sleep(300);
                frmMenu.ACBrECF1.SubtotalizaCupom((qrySomaDescAcresnValAcrescimo.Value - qrySomaDescAcresnValDesconto.Value), frmMenu.cMsgCupomFiscal );
                Sleep(300);
                lblMensagem.Caption := '' ;

            end ;

        end ;

    end ;

    if (confTEF) then
         frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Antes de SP_FINALIZA' );

    { -- ap�s processar as transa��es TEF , envia os dados para o banco de dados --}



    frmMenu.Connection.BeginTrans ;

    try
        SP_FINALIZA.Close ;
        SP_FINALIZA.Parameters.ParamByName('@nCdUsuario').Value         := frmMenu.nCdUsuarioLogado;
        SP_FINALIZA.Parameters.ParamByName('@nCdResumoCaixa').Value     := nCdResumoCaixa ;

        {-- soma o juro manual para caso de ajustes de valor de troca usando a fun��o de acrescimo --}
        {-- evitando assim que seja gerado vale-mercadoria --}

        if (edtSaldo.Value < 0) then
        begin
            SP_FINALIZA.Parameters.ParamByName('@nSaldo').Value := edtSaldo.Value;
        end
        else
        begin
            SP_FINALIZA.Parameters.ParamByName('@nSaldo').Value := edtSaldo.Value + edtJM.Value ;
        end ;

        SP_FINALIZA.Parameters.ParamByName('@nCdDoctoFiscal').Value     := nCdDoctoFiscal ;
        SP_FINALIZA.ExecProc;

        nCdLanctoFin        := SP_FINALIZA.Parameters.ParamByName('@nCdLanctoFin_Out').Value;
        cFlgValePresente    := SP_FINALIZA.Parameters.ParamByName('@cFlgValePresente').Value;
        cFlgValeMercadoria  := SP_FINALIZA.Parameters.ParamByName('@cFlgValeMercadoria').Value;
        cFlgValeDesconto    := SP_FINALIZA.Parameters.ParamByName('@cFlgValeDesconto').Value;
        nCdVale             := SP_FINALIZA.Parameters.ParamByName('@nCdVale_Out').Value;
        nCdValeDesconto_Out := SP_FINALIZA.Parameters.ParamByName('@nCdValeDesconto_Out').Value;

        if (cFlgUsaECF) and (nValMercadoria > 0) then
        begin

            if (cFlgLiqCrediario = 0) then
            begin

                try
                    EnviaPagamentosECF ;
                except
                    MensagemErro('Erro enviando as formas de pagamento para ECF.') ;
                    raise ;
                end ;

                try
                    FinalizaCupomFiscal() ;

                except
                    MensagemErro('Erro finalizando cupom fiscal.') ;
                    raise ;
                end ;

            end ;

        end ;

        { -- se cupom fiscal eletr�nico estivar ativo, substitui uso da ECF -- }
        if (cFlgUsaCFe) and (nValMercadoria > 0) then
        begin
            if (cFlgLiqCrediario = 0) then
            begin
                try
                    lblMensagem.Caption := 'Gerando cupom fiscal...';

                    try
                        if (nCdTerceiro = 0) then
                            nCdTerceiro := nCdTerceiroPDV;

                        objCaixa_CPFNotaFiscal := TfrmCaixa_CPFNotaFiscal.Create(Self);

                        solicitaCPFCNPJ:

                        objCaixa_CPFNotaFiscal.ShowModal;

                        cCGCPF := objCaixa_CPFNotaFiscal.edtCPF.Text;

                        if (cCGCPF <> '') then
                        begin
                            if (frmMenu.TestaCpfCgc(cCGCPF) = '') then
                            begin
                                goto solicitaCPFCNPJ;
                            end;

                            case MessageDlg('Confirma a utiliza��o do CPF/CNPJ n� ' + cCGCPF + ' ?', mtConfirmation,[mbYes,mbNo],0) of
                                mrNo : begin
                                    goto solicitaCPFCNPJ;
                                end;
                            end;
                        end
                        else
                        begin
                            case MessageDlg('Nenhum CPF/CNPJ informado, deseja continuar ?', mtConfirmation,[mbYes,mbNo],0) of
                                mrNo : begin
                                    goto solicitaCPFCNPJ;
                                end;
                            end;
                        end;

                        { -- gera cupom fiscal eletr�nico -- }
                        SP_GERA_CUPOM_FISCAL.Close ;
                        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva  ;
                        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdLoja').Value     := frmMenu.nCdLojaAtiva     ;
                        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdUsuario').Value  := frmMenu.nCdUsuarioLogado ;
                        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdTerceiro').Value := nCdTerceiro              ;
                        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@cCPF').Value        := cCGCPF                   ;
                        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@cCdNumSerie').Value := '';
                        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@cFlgCFe').Value     := 1; //cupom fiscal eletr�nico CFe
                        SP_GERA_CUPOM_FISCAL.Open;

                        nCdDoctoFiscal := SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdDoctoFiscal').Value ;

                        { -- atualiza lan�amento fin. cf-e -- }
                        qryAtualizaLanctoFinCFe.Close;
                        qryAtualizaLanctoFinCFe.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
                        qryAtualizaLanctoFinCFe.Parameters.ParamByName('nCdLanctoFin').Value   := nCdLanctoFin;
                        qryAtualizaLanctoFinCFe.ExecSQL;

                        { -- transmite cupom fiscal eletr�nico -- }
                        if (confTEF) then
                         frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Antes Emite Cupom Fiscal' );

                        prGeraCFe;

                        if (confTEF) then
                         frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Apos Emite Cupom Fiscal' );

                    except
                        if (confTEF) then
                         frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Erro Grave Fechar Cupom Fiscal' );
                        Raise;
                    end;
                finally
                    lblMensagem.Caption := '';

                    FreeAndNil(objCaixa_CPFNotaFiscal);
                end;
            end;
        end;

    except
        frmMenu.Connection.RollbackTrans;
        // Se Erro e TEF, Desfazer Pagamentos
        if (confTEF) then
         frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Apos ROLLBACK de SP_FINALIZA' );

        if (confTEF) then
        begin
          frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Antes de Cappta.DesfazerPagamentos' );
          objTEF.cappta.DesfazerPagamentos;
          frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Apos de Cappta.DesfazerPagamentos' );
        end;

        MensagemErro('Erro no processamento.');
        raise ;
    end ;

    frmMenu.Connection.CommitTrans ;
    if (confTEF) then
         frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Apos COMMIT de SP_FINALIZA' );



    qryTemp_Pagtos.Close ;
    qryTemp_Pagtos.Open ;


    try

 ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      { -- Confirma pagamentos e imprime cupons TEF -- }
      if (confTEF) then
      begin

         frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Antes de Confirmar e Imprimir TEF' );

       try
        //Imprime cupons TEF
          qryImpressora.Close;
          qryImpressora.Parameters.ParamByName('cNmComputador').Value := frmMenu.qryConfigAmbientecNmComputador.Value;
          qryImpressora.Open;

         if qryImpressora.IsEmpty then
         begin
          MensagemErro('Impressora N�o Configurada '+frmMenu.qryConfigAmbientecNmComputador.Value+' , Operacao TEF Cancelada!');
          frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Impressora N�o Configurada '+frmMenu.qryConfigAmbientecNmComputador.Value );

          frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Impressora N�o Configurada Antes Desfazer Pagamentos' );
          objTEF.cappta.DesfazerPagamentos;
          frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Impressora N�o Configurada Apos Desfazer Pagamentos' );
         end else
         begin
  //        if qryImpressoracFlgTipoImpSAT.Value = 1 then  // Tipo ESC POS
  //        begin

             case AnsiIndexStr(qryImpressoracModeloImpSAT.Value,['Epson','Bematech','Daruma','Elgin','Diebold']) of
             0 : ppMarcaImpSAT := ppEscPosEpson;
             1 : ppMarcaImpSAT := ppEscBematech;
             2 : ppMarcaImpSAT := ppEscDaruma;
             3 : ppMarcaImpSAT := ppEscVox;    //ppEscElgin;   Alterado 02/08/18 nova versoa ACBR por Marcelo
             4 : ppMarcaImpSAT := ppEscDiebold;
             end;

             case AnsiIndexStr(qryImpressoracPageCodImpSat.Value,['pcNone','pc437','pc850','pc852','pc860','pcUTF8','pc1252']) of
             0 : pcPagCodigo := pcNone;
             1 : pcPagCodigo := pc437;
             2 : pcPagCodigo := pc850;
             3 : pcPagCodigo := pc852;
             4 : pcPagCodigo := pc860;
             5 : pcPagCodigo := pcUTF8;
             6 : pcPagCodigo := pc1252;
             end;

//                           stImpressora :=  frmMenu.ACBrPosPrinter1.LerStatusImpressora;
               frmMenu.ACBrPosPrinter1.Desativar;

              frmMenu.ACBrPosPrinter1.Modelo             := ppMarcaImpSAT;
              frmMenu.ACBrPosPrinter1.Porta              := qryImpressoracPortaImpSAT.Value;
              frmMenu.ACBrPosPrinter1.CortaPapel         := True;
              frmMenu.ACBrPosPrinter1.PaginaDeCodigo     := pcPagCodigo;
            repeat
              frmMenu.ACBRPosPrinter1AguardarPronta(confTEF);

             try
              frmMenu.ACBrPosPrinter1.Ativar;
              frmMenu.ACBrPosPrinter1.Imprimir(cupomCliente.Text);
              frmMenu.ACBrPosPrinter1.CortarPapel( True );
              frmMenu.ACBrPosPrinter1.Imprimir(cupomLojista.Text);
              frmMenu.ACBrPosPrinter1.CortarPapel( False );  // Corte Total
              frmMenu.ACBrPosPrinter1.Desativar;
              frmMenu.ACBRPosPrinter1FechaImpTermica;

              frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Impressao: Antes de ConfirmarPagamentos' );
              objTEF.cappta.ConfirmarPagamentos;
              frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Impressao: Apos de ConfirmarPagamentos' );
              break;
             except
               frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Impressao: ERRO, Tentar Novamente?' );
               case MessageDlg('Erro Impress�o Comprovante TEF, Tentar Novamente?', mtConfirmation,[mbYes,mbNo],0) of
               mrYes : begin
                       frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Impressao: ERRO, Respondeu SIM' );
                       sleep( 500 );
                       end;
               mrNo  : begin
                       frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Impressao: ERRO, Respondeu NAO' );
                       MensagemErro('Erro ao efetuar a impress�o do cupom TEF'+#13+'TEF Confirmado!'+#13+'Reimprima em Fun��es/Reimpress�o.');
                       frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Impressao: Antes de ConfirmarPagamentos' );
                       objTEF.cappta.ConfirmarPagamentos;
                       frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Impressao: Apos de ConfirmarPagamentos' );
                       { MensagemErro('Erro ao efetuar a impress�o do cupom TEF, OPERA�AO DO CART�O FOI ESTORNADA.'); }
                       { objTEF.cappta.DesfazerPagamentos; }
                       break;
                       end;
               end;
             end;
            until False;
          end;
       finally
        cupomCliente.Free;
        cupomLojista.Free;
        objTEF.Free;
        frmMenu.CapptaLog('fCaixaRecebimento/btFinalizarClick Fim do processamento TEF' );
        frmMenu.CapptaLog('' );
       end;
      end;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        // imprime os carnet�s
        if (qryTemp_Pagtos.Active) then
        begin
            qryTemp_Pagtos.First ;

            while not qryTemp_Pagtos.eof do
            begin

                lblMensagem.Caption := 'Imprimindo carnet�s...' ;

                if (qryTemp_PagtosnCdCrediario.Value > 0) then
                begin
                    ShowMessage('Prepare a impressora para impress�o do carnet.' + #13#13 + 'Cliente : ' + qryTemp_PagtoscNmTerceiro.Value) ;

                    try
                        frmImpDFPadrao.ImprimirCarnet(qryTemp_PagtosnCdCrediario.Value, 0);
                    except
                        MensagemErro('Problemas na impress�o do credi�rio: ' + qryTemp_PagtosnCdCrediario.AsString);
                    end;
                end ;

                qryTemp_pagtos.Next ;

            end ;

            qryTemp_pagtos.Close ;
        end ;

        lblMensagem.Caption := '' ;

        { -- verifica se renegocia��o foi avista para impress�o de comprovante -- }
        qryRenegAvista.Close;
        qryRenegAvista.Parameters.ParamByName('nPK').Value := nCdLanctoFin;
        qryRenegAvista.Open;

        if (not qryRenegAvista.IsEmpty) then
        begin
            case MessageDlg('Deseja imprimir comprovante de renegocia��o ?',mtConfirmation,[mbYes,mbNo],0) of
                mrYes : begin
                            frmImpDFPadrao.ImprimirReneg(qryRenegAvistanCdPropostaReneg.Value);

                            case MessageDlg('Comprovante de renegocia��o foi impresso corretamente?',mtConfirmation,[mbYes,mbNo],0) of
                                mrNo : frmImpDFPadrao.ImprimirReneg(qryRenegAvistanCdPropostaReneg.Value);
                            end;
                        end;
            end;
        end;

        //autentica os carnet�s
        if (frmMenu.LeParametro('TIPOAUTENTCRED') = 'PG') then
        begin
            qryLanctoFinPagCred.Close;
            qryLanctoFinPagCred.Parameters.ParamByName('nPK').Value := nCdLanctoFin;
            qryLanctoFinPagCred.Open;

            if (not qryLanctoFinPagCred.IsEmpty) then
            begin
                lblMensagem.Caption := 'Imp. comprovante pagto...' ;
                frmImpDFPadrao.ImprimirPagtoParcela(nCdLanctoFin);
            end;
        end
        else
        begin
            qryParcelas.Close ;
            qryParcelas.Open ;
            qryParcelas.First ;

            while not qryParcelas.Eof do
            begin
                //              --- SQL ---              \\
                qryAux.Close;
                qryAux.SQL.Clear;
                qryAux.SQL.Add('   SELECT nSaldoTit ');
                qryAux.SQL.Add('     FROM Titulo    ');
                qryAux.SQL.Add('    WHERE nCdTitulo = '+intTostr(qryParcelasnCdTitulo.Value));
                qryAux.Open;

                //Testa se o saldo do titulo � qual a zero caso seja autentica o pagamento da parcela
                if qryAux.FieldList[0].Value = 0 then
                begin
                    lblMensagem.Caption := 'Autenticando parcelas...' ;

                    if ((qryParcelascFlgEntrada.Value = 0) and (qryParcelasnCdCrediario.Value > 0)) then
                    begin
                        ShowMessage('Prepare a impressora para autentica��o impress�o do carnet.' + #13#13 + 'N�mero : ' + qryParcelasnCdCrediario.AsString + '   Parcela: ' + qryParcelasiParcela.asString) ;
                        frmImpDFPadrao.AutenticaParcela(qryParcelasnCdCrediario.Value, qryParcelasiParcela.Value);
                    end
                end;

                qryParcelas.Next;
            end;
        end;

        if (cFlgValeMercadoria = 'S') or ((cFlgValePresente = 'S') and (frmMenu.LeParametro('PEDIRNUMVPCX') = 'N')) then
        begin
            ImprimirVale(nCdVale) ;
        end ;

        if (cFlgValeDesconto = 'S') then
        begin
            ShowMessage('Foi gerado um vale desconto e ser� impresso agora. Clique em OK para continuar.') ;
            ImprimirVale( nCdValeDesconto_Out ) ;
        end ;
    finally
        cmdZeraTemp.Execute;

        frmMenu.ZeraTemp('#Temp_Pedido');
        frmMenu.ZeraTemp('#Temp_Pagtos');
        frmMenu.ZeraTemp('#Temp_ChequeResgate');
        frmMenu.ZeraTemp('#Temp_ValePresente') ;
        frmMenu.ZeraTemp('#Temp_RestricaoVenda');

        qryCondPagto.Close ;
        qryTerceiro.Close ;
        nCdTerceiro := 0 ;

        qryTemp_Pedido.Close ;

        // limpa os dados dos cheques
        objCaixa_Cheques.qryPrepara_Temp_Cheque.Close ;

        objCaixa_DadosCheque.edtCodBanco.Text   := '' ;
        objCaixa_DadosCheque.edtCodAgencia.Text := '' ;
        objCaixa_DadosCheque.edtNumConta.Text   := '' ;
        objCaixa_DadosCheque.edtDV.Text         := '' ;
        objCaixa_DadosCheque.edtNumCheque.Text  := '' ;
        objCaixa_DadosCheque.edtCPFCNPJ.Text    := '' ;

        // limpa os dados do credi�rio
        objCaixa_ParcelaCred.qryTemp_Crediario.Close ;

        // limpa as parcelas recebidas
        qryParcelas.Close ;

        // limpa os cheques resgatados
        qryTempChequeResgate.Close ;

        // zera os saldos da tela
        edtPedidos.Value    := 0 ;
        edtParcelas.Value   := 0 ;
        edtEntrada.Value    := 0 ;
        edtValorPago.Value  := 0 ;
        edtTroco.Value      := 0 ;
        edtVale.Value       := 0 ;
        edtChequeResg.Value := 0 ;
        nTotalValePresente  := 0 ;

        CalculaSaldo() ;

        btNovoPedido.Enabled    := True ;
        btNovaParcela.Enabled   := True ;
        btResgateCheque.Enabled := True ;

        btNovoPedido.SetFocus ;

        btFinalizar.Enabled  := False ;
        btCancelar.Enabled   := False ;

        btIncParcela.Enabled      := False ;
        btExcluirParcelas.Enabled := False ;

        GroupBox1.Enabled    := False ;
        GroupBox4.Enabled    := False ;

        cFlgLiqCrediario     := 0 ;

        iQtdeClientes        := 0 ;
        nCdTerceiroPadrao    := 0 ;

        bCupomAberto         := False ;
        cCGCPF               := '' ;
        nValMercadoria       := 0 ;
        nCdDoctoFiscal       := 0 ;

        freeAndNil(cValorPago);
        freeAndNil(arrFormaPagto);

        lblMensagem.Caption  := '' ;

        CaixaLivre() ;

        VerificaSaldoCaixa(False);
    end;
end;



procedure TfrmCaixa_Recebimento.VerificaSaldoCaixa(bContinua : Boolean);
var
  nValLimiteCaixa : Double;
begin
  { -- valida par�metro -- }
  try
      nValLimiteCaixa := StrToFloat(frmMenu.LeParametro('LIMITEDINCAIXA'));
  except
      frmMenu.MensagemErro('Parametro LIMITEDINCAIXA n�o configurado corretamente.' + #13#13 + 'Exemplo: 0,00');
      Abort;
  end;

  { -- verifica se caixa atingiu o limite de saldo -- }
  if (nValLimiteCaixa > 0) then
  begin
      qryVerificaSaldoContaBancaria.Close;
      qryVerificaSaldoContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryVerificaSaldoContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      qryVerificaSaldoContaBancaria.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
      qryVerificaSaldoContaBancaria.Open;

      if (qryVerificaSaldoContaBancarianSaldoConta.Value >= nValLimiteCaixa) then
      begin
          MensagemAlerta('Caixa atingiu seu limite de saldo m�ximo, suas fun��es foram desabilitadas at� que seja atualizado o saldo.');

          if (bContinua = False) then
              Abort;
      end;
  end;
end;

procedure TfrmCaixa_Recebimento.btCancelarClick(Sender: TObject);
var
   cArquivo   : TextFile;
   cLinha     : string ;
begin

    if (MessageDlg('Confirma o cancelamento do recebimento ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
        exit;

    if ((frmMenu.LeParametro('SENHACANREC') = 'S') and (not qryPedido.IsEmpty)) then //or (not qryParcelas.IsEmpty) or (not qryTempChequeResgate.IsEmpty))) then
    begin
        if (MessageDlg('Para sair � necess�rio a autoriza��o do gerente.' +#13+#13+ 'Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
            exit ;

        if not (AutorizacaoGerente(34)) then
        begin
            MensagemAlerta('Autentica��o Falhou');
            abort ;
        end;
    end;

    CancelaCupomFiscal;
    CancelaRecebimento;
    ExcluirPagamentos;

    cmdZeraTemp.Execute;

    frmMenu.ZeraTemp('#Temp_Pedido');
    frmMenu.ZeraTemp('#Temp_Pagtos');
    frmMenu.ZeraTemp('#Temp_ChequeResgate');
    frmMenu.ZeraTemp('#Temp_ValePresente');
    frmMenu.ZeraTemp('#Temp_RestricaoVenda');

    nCdDoctoFiscal := 0 ;
    nValMercadoria := 0 ;

    qryCondPagto.Close ;
    qryTerceiro.Close ;
    nCdTerceiro := 0 ;

    if (qryTemp_pedido.Active) then
        qryTemp_Pedido.Close ;

    // limpa os dados dos cheques
    objCaixa_Cheques.qryPrepara_Temp_Cheque.Close ;

    objCaixa_DadosCheque.edtCodBanco.Text   := '' ;
    objCaixa_DadosCheque.edtCodAgencia.Text := '' ;
    objCaixa_DadosCheque.edtNumConta.Text   := '' ;
    objCaixa_DadosCheque.edtDV.Text         := '' ;
    objCaixa_DadosCheque.edtNumCheque.Text  := '' ;
    objCaixa_DadosCheque.edtCPFCNPJ.Text    := '' ;

    // limpa os dados do credi�rio
    if (objCaixa_ParcelaCred.qryTemp_Crediario.Active) then
        objCaixa_ParcelaCred.qryTemp_Crediario.Close ;

    // limpa as parcelas recebidas
    if (qryParcelas.Active) then
        qryParcelas.Close ;

    // limpa os cheques resgatados
    if (qryTempChequeResgate.Active) then
        qryTempChequeResgate.Close ;

    // zera os saldos da tela
    edtPedidos.Value    := 0 ;
    edtParcelas.Value   := 0 ;
    edtEntrada.Value    := 0 ;
    edtValorPago.Value  := 0 ;
    edtTroco.Value      := 0 ;
    edtVale.Value       := 0 ;
    edtChequeResg.Value := 0 ;
    nTotalValePresente  := 0 ;
    bItemPromocao       := False ;

    CalculaSaldo() ;

    btNovoPedido.Enabled    := True ;
    btNovaParcela.Enabled   := True ;
    btResgateCheque.Enabled := True ;

    lblMensagem.Caption := '' ;

    CaixaLivre() ;

end;

procedure TfrmCaixa_Recebimento.btNovaParcelaClick(Sender: TObject);
begin
    if (dDtLimiteFech < Date) then
    begin
        MensagemErro('Este caixa requer fechamento imediato.');
        Abort;
    end
    else
    begin
        VerificaSaldoCaixa(False);
    end;

    nValMercadoria       := 0 ;
    nCdDoctoFiscal       := 0 ;
    bItemPromocao        := False ;
    tabPedidos.Enabled   := False ;
    tabParcelas.Enabled  := True  ;
    tabCheques.Enabled   := False ;
    tabPagamento.Enabled := True  ;
    imgCaixaLivre.Visible:= False ;

    if (qryTemp_Pagtos.Active) and (qryTemp_Pagtos.RecordCount > 0) then
    begin

        case MessageDlg('Voc� j� iniciou o processo de recebimento. Se for adicionar parcelas agora todos os pagamentos ser�o exclusos. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) of
            mrNo:exit ;
        end ;

        frmMenu.ZeraTemp('#Temp_Pagtos');
        frmMenu.ZeraTemp('#Temp_Cheque_Pagamento');
        frmMenu.ZeraTemp('#Temp_Crediario');

        qryTemp_Pagtos.Close ;
        qryTemp_Pagtos.Open ;

        CalculaSaldo() ;

    end ;


    GroupBox4.Enabled       := True ;
    btFinalizar.Enabled     := True ;
    btCancelar.Enabled      := True ;
    btNovoPedido.Enabled    := False ;
    btNovaParcela.Enabled   := False ;
    btResgateCheque.Enabled := False ;

    cFlgLiqCrediario := 1 ;
    nValorPGInicial  := 0 ;
    nValorDCInicial  := 0 ;
    nValorDMInicial  := 0 ;
    nValorJMInicial  := 0 ;
    nValorPGFinal    := 0 ;

    DesativaValores;

    btIncParcela.Enabled      := True ;
    btExcluirParcelas.Enabled := True ;
    btRenegociacao.Enabled    := True ;
    btFuncoes.Enabled         := False ;

    cxPageControl1.ActivePage := tabParcelas ;

    edtNumCarnet.SetFocus ;

end;

procedure TfrmCaixa_Recebimento.btIncParcelaClick(Sender: TObject);
var
    nCdTerceiro         : integer ;
    objCaixa_SelCliente : TfrmCaixa_SelCliente ;
begin

    objCaixa_SelCliente := TfrmCaixa_SelCliente.Create( Self ) ;

    nCdTerceiro := objCaixa_SelCliente.SelecionaCliente;

    freeAndNil( objCaixa_SelCliente ) ;

    if (nCdTerceiro = 0) then
    begin
        MensagemAlerta('Nenhum cliente selecionado.') ;
        edtNumCarnet.SetFocus;
        exit ;
    end ;

    qryParcelas.Close ;
    qryParcelas.Open ;

    objCaixa_ListaParcelas.qryParcelas.Close ;
    objCaixa_ListaParcelas.qryParcelas.Parameters.ParamByName('nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva ;
    objCaixa_ListaParcelas.qryParcelas.Parameters.ParamByName('nCdTerceiro').Value  := nCdTerceiro ;
    objCaixa_ListaParcelas.qryParcelas.Parameters.ParamByName('dDtSimulacao').Value := DateToStr(Date) ;
    objCaixa_ListaParcelas.qryParcelas.Parameters.ParamByName('nCdLoja').Value      := frmMenu.nCdLojaAtiva;
    objCaixa_ListaParcelas.qryParcelas.Open ;

    if (objCaixa_ListaParcelas.qryParcelas.Eof) then
    begin
        qrySomaParcCobradora.Close;
        qrySomaParcCobradora.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
        qrySomaParcCobradora.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro ;
        qrySomaParcCobradora.Parameters.ParamByName('nCdLoja').Value     := frmMenu.nCdLojaAtiva;
        qrySomaParcCobradora.Open;

        if (qrySomaParcCobradoranSaldoTitAbertoCobradora.Value > 0) then
            MensagemAlerta('Nenhuma parcela pendente para este cliente.' +#13#13+'Cliente tem parcelas enviadas para cobradora.')
        else MensagemAlerta('Nenhuma parcela pendente para este cliente.') ;

        qrySomaParcCobradora.Close ;
        edtNumCarnet.SetFocus;
        abort ;
    end ;

    objCaixa_ListaParcelas.ShowModal;

    qryParcelas.Close ;
    qryParcelas.Open ;

    qryParcelas.First ;

    edtParcelas.Value := 0 ;
    nValorPGInicial   := 0 ;
    nValorDCInicial   := 0 ;
    nValorDMInicial   := 0 ;
    nValorJMInicial   := 0 ;
    nValorPGFinal     := 0 ;

    while not qryParcelas.Eof do
    begin

        edtParcelas.Value := edtParcelas.Value + qryParcelasnValTotal.Value ;

        AcumulaValorInicial(qryParcelasnValTotal.Value
                           ,0
                           ,0
                           ,0
                           ,qryParcelasnValTotal.Value) ;

        qryParcelas.next ;
    end ;

    qryParcelas.First ;

    CalculaSaldo() ;

    edtNumCarnet.SetFocus;

end;

procedure TfrmCaixa_Recebimento.cxButton6Click(Sender: TObject);
var
    objForm : TfrmCaixa_LanctoMan ;
begin

    objForm := TfrmCaixa_LanctoMan.Create( Self ) ;

    objForm.nCdResumoCaixa := nCdResumoCaixa ;
    objForm.cFlgUsaECF     := cFlgUsaECF     ;
    objForm.ShowModal ;

    freeAndNil( objForm ) ;

end;

procedure TfrmCaixa_Recebimento.edtDinheiroExit(Sender: TObject);
begin
    CalculaSaldo() ;

    btFinalizar.SetFocus;

end;

procedure TfrmCaixa_Recebimento.Button6Click(Sender: TObject);
begin
    frmMenu.ACBrECF1.FechaCupom( '' );

end;

procedure TfrmCaixa_Recebimento.btIncPagamentoClick(Sender: TObject);
var
    nCdFormaPagto, nCdCondPagto : integer ;
    cNmFormaPagto : string  ;
    nValCrediario : double  ;
    nValCheque    : double  ;
    cFlgTEF       : integer ;
    cNSU          : string  ;
    cRede         : string  ;
    cAutorizacao  : string  ;
    nCdOperadora  : integer ;
    nValorPagoOld : double ;
    objJustifica  : TfrmCaixa_Justificativa;
label
    SelecionaFormaPagamento;
begin

    if ((edtValor.Value + edtJM.Value) <= 0) then
    begin
        MensagemAlerta('Nenhum valor informado.') ;
        if (edtValor.Enabled) then
            edtValor.SetFocus;
        exit ;
    end ;

    if (qryCondPagto.eof) then
    begin
        MensagemAlerta('Selecione uma condi��o de pagamento.') ;
        btTrocaCondicao.SetFocus;
        exit ;
    end ;

    if not (qryCondPagto.Eof) and (qryCondPagtocFlgExigeCliente.Value = 1) then
    begin
        if (qryParcelas.IsEmpty) then
        begin
            if ((qryTerceiro.Eof) or (qryTerceironCdTerceiro.Value = nCdTerceiroPDV)) then
            begin
                { -- verifica se forma de pagamento exige informar o cliente, se exigem o usu�rio pode efetuar a libera��o da venda atrav�s
                     da autoriza��o do gerente. Contudo, formas de pagamento que utilizem o tipo 2 e 5 n�o podem ser liberadas devido a
                     necessidade do cadastro do cliente.
                -- }
                if (qryCondPagtonCdTabTipoFormaPagto.Value in [2,5]) then { -- 2 : Cheque / 5 : Credi�rio -- }
                begin
                    MensagemAlerta('Esta condi��o de pagamento exige que seja selecionado um cliente.');

                    btTrocaCliente.Click;

                    if (qryTerceiro.eof) then
                        Exit;
                end
                else
                begin
                    if (MessageDlg('Esta condi��o de pagamento exige que seja selecionado um cliente. ' + #13#13 + 'Deseja solicitar autoriza��o do gerente?',mtConfirmation,[mbYes,mbNo],0) = mrYes) then
                    begin
                        if not (AutorizacaoGerente(35)) then
                        begin
                            MensagemAlerta('Autentica��o Falhou');
                            Abort;
                        end;

                        try
                            { -- inseri justificativa de venda sem cliente -- }
                            objJustifica := TfrmCaixa_Justificativa.Create(nil);

                            objJustifica.MemoJustificativa.Clear;
                            objJustifica.Caption := 'Justificativa - Venda Sem Cliente';

                            objJustifica.ShowModal;

                            if (Trim(objJustifica.MemoJustificativa.Text) = '') then
                                Exit;

                            qryAux.SQL.Clear;
                            qryAux.SQL.Add('UPDATE Pedido                                                                              ');
                            qryAux.SQL.Add('   SET cOBS            = ' + #39 + AnsiUpperCase(objJustifica.MemoJustificativa.Text) + #39 );
                            qryAux.SQL.Add('      ,nCdUsuarioAutor = ' + IntToStr(nCdUsuarioSupervisor)                                 );
                            qryAux.SQL.Add('  FROM Pedido                                                                              ');
                            qryAux.SQL.Add('       INNER JOIN CondPagto  ON CondPagto.nCdCondPagto  = Pedido.nCdCondPagto              ');
                            qryAux.SQL.Add('       INNER JOIN FormaPagto ON FormaPagto.nCdFormaPagto = CondPagto.nCdFormaPagto         ');
                            qryAux.SQL.Add(' WHERE cFlgExigeCliente = 1                                                                ');
                            qryAux.SQL.Add('   AND nCdPedido        IN (SELECT nCdPedido                                               ');
                            qryAux.SQL.Add('                              FROM #Temp_Pedido)                                           ');
                            qryAux.ExecSQL;
                        finally
                            FreeAndNil(objJustifica);
                        end;
                    end
                    else
                    begin
                        btTrocaCliente.Click;
                        Exit;
                    end;
                end;
            end;
        end;
    end;

    if (MessageDlg('Confirma a inclus�o deste pagamento ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
    begin
        if (edtValor.Enabled) then
            edtValor.SetFocus;
        exit ;
    end ;

    if (edtDM.Enabled and (edtDM.Value > 0)) then
    begin
        MensagemAlerta('Solicite a autoriza��o do gerente para o desconto de ' + Format('%m',[edtDM.Value])) ;
        if not AutorizacaoGerente(27) then
        begin
            MensagemErro('Autoriza��o inv�lida. O pagamento n�o foi processado.') ;
            edtDM.SetFocus;
            abort ;
        end ;

        {if (((edtDM.Value / (edtValor.Value + edtDC.Value))*100) > frmPDVSupervisor.qryConfereUsuarionPercMaxDesc.Value) then
        begin
            frmMenu.MensagemErro('Desconto n�o autorizado.') ;
            edtDM.SetFocus;
            abort ;
        end ;}
    end ;

    frmMenu.cCartaoNSU       := '' ;
    frmMenu.cCartaoTransacao := '' ;
    frmMenu.cCartaoOperadora := '' ;

    cFlgTEF := 0 ;

    if not (qryTemp_Pagtos.Active) then
        qryTemp_Pagtos.Open ;

    CalculaSaldo() ;

    {cFlgLiqCrediario := 0 ;}

    if (cValorPago = nil) then
        cValorPago := TStringList.Create;

    if (arrFormaPagto = nil) then
        arrFormaPagto := TStringList.Create;

    {if (cFlgLiqCrediario = 0) and (nValMercadoria > 0) then
    begin

        // Abre o cupom fiscal
        if (cFlgUsaECF) and (EstadoECF() = 'Livre') then
            AbreCupomFiscal() ;

        if (cFlgUsaECF) and (EstadoECF() = 'Venda') then
        begin
            lblMensagem.Caption := 'Totalizando cupom...' ;

            qrySomaDescAcres.Close ;
            qrySomaDescAcres.Open ;

            frmMenu.ACBrECF1.SubtotalizaCupom((qrySomaDescAcresnValAcrescimo.Value - qrySomaDescAcresnValDesconto.Value), '' );

            lblMensagem.Caption := '' ;

        end ;

    end ;}

    SelecionaFormaPagamento:

    {!!!!!!
    PASSAR A ROTINA DE IDENTIFICACAO DO TEF ABAIXO PARA O DATASOURCE DA CONDICAO DE PAGAMENTO, E ATRIBUIR NA VARIAVEL
    SUBTOTALIZAR O CUPOM FISCAL SOMENTE DEPOIS QUE PROCESSAR OS PAGAMENTOS.
    !!!!!!}

    nCdFormaPagto                       := qryCondPagtonCdFormaPagto.Value ;

    {-- se o TEF estiver ativo e a forma de pagamento da condi��o utilizar TEF --}
    if ((cFlgUsaTEF) and (qryCondPagtocFlgTEF.Value = 1)) then
        cFlgTEF := 1
    else
        cFlgTEF := 0;

    cNmFormaPagto                       := qryCondPagtocNmFormaPagto.Value ;

    {-- Faz o processamento do pagamento --}
    // dinheiro
    if (qryCondPagtonCdTabTipoFormaPagto.Value = 1) then
    begin

        objCaixa_Dinheiro.edtSaldo.Value    := edtVlrPagto.Value ;
        objCaixa_Dinheiro.edtDinheiro.Value := edtVlrPagto.Value ;
        objCaixa_Dinheiro.edtDinheiro.SelectAll;
        objCaixa_Dinheiro.ShowModal;

        if (objCaixa_Dinheiro.edtDinheiro.Value <= 0) then
        begin
            MensagemAlerta('Nenhum valor informado, o pagamento n�o foi registrado.') ;
            exit ;
        end ;

        if not (qryInseri_Temp_Pagtos.Active) then
             qryInseri_Temp_Pagtos.Open ;

        qryInseri_Temp_Pagtos.Insert ;
        qryInseri_Temp_PagtosnCdFormaPagto.Value := qryCondPagtonCdFormaPagto.Value     ;
        qryInseri_Temp_PagtosnCdCondPagto.Value  := qryCondPagtonCdCondPagto.Value      ;

        if (qryTerceiro.Active) then
            qryInseri_Temp_PagtosnCdTerceiro.Value   := qryTerceironCdTerceiro.Value        ;

        qryInseri_Temp_PagtosnValPagto.Value     := objCaixa_Dinheiro.edtDinheiro.Value ;
        qryInseri_Temp_PagtosnValEntrada.Value   := 0 ;
        qryInseri_Temp_PagtoscFlgTipo.Value      := 1 ;

        qryInseri_Temp_PagtosnValAcrescimo.Value := 0 ;
        qryInseri_Temp_PagtosnValDesconto.Value  := 0 ;

        if (edtDC.Value > 0) then
        begin
            qryInseri_Temp_PagtosnValAcrescimo.Value      := edtDC.Value ;
            qryInseri_Temp_PagtosnValJurosCondPagto.Value := edtDC.Value ;
        end
        else if (edtDC.Value < 0) then
            qryInseri_Temp_PagtosnValDesconto.Value  := abs(edtDC.Value) ;

        if (edtDM.Value > 0) then
        begin
            qryInseri_Temp_PagtosnValDesconto.Value       := qryInseri_Temp_PagtosnValDesconto.Value + edtDM.Value ;
            qryInseri_Temp_PagtosnValDescontoManual.Value := edtDM.Value ;
        end ;

        if (edtJM.Value > 0) then
            qryInseri_Temp_PagtosnValAcrescimo.Value := qryInseri_Temp_PagtosnValAcrescimo.Value + edtJM.Value ;

        if (objCaixa_Dinheiro.edtDinheiro.Value > objCaixa_Dinheiro.edtSaldo.Value) then
            qryInseri_Temp_PagtosnValPagto.Value := objCaixa_Dinheiro.edtSaldo.Value ;

        if (qryCondPagtonPercValeDesconto.Value > 0) and (cFlgLiqCrediario = 0) then
            if ((not bItemPromocao) or (bItemPromocao and (qryCondPagtocFlgGeraValeDescontoPromocao.Value = 1))) then
                qryInseri_Temp_PagtosnValValeDesconto.Value := (qryInseri_Temp_PagtosnValPagto.Value * (qryCondPagtonPercValeDesconto.Value / 100)) ;

        qryInseri_Temp_Pagtos.Post ;

        qryTemp_Pagtos.Close ;
        qryTemp_Pagtos.Open ;

        if (objCaixa_Dinheiro.edtDinheiro.Value > objCaixa_Dinheiro.edtSaldo.Value) then
        begin
            edtTroco.Value := (objCaixa_Dinheiro.edtDinheiro.Value - objCaixa_Dinheiro.edtSaldo.Value) ;
            ShowMessage('Valor do Troco : ' + edtTroco.Text) ;
        end ;

        { -- abre gaveta -- }
        if (cFlgUsaGaveta) then
            frmMenu.prAbreGaveta;
    end ;

    // cheque
    if (qryCondPagtonCdTabTipoFormaPagto.Value = 2) then
    begin
        qryTemp_Cheque.Close ;
        qryTemp_Cheque.ExecSQL ;

        nValCheque := SimularCheque( FALSE );

        CalculaSaldo() ;

        {-- soma o valor das parcelas do credi�rio --}
        nValCheque := 0 ;

        objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.First ;

        while not objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.eof do
        begin
            nValCheque := nValCheque + objCaixa_PagtoCheque.qryPrepara_Temp_ChequenValCheque.Value ;
            objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.next ;
        end ;

        objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.First ;

        {-- atualiza o saldo a pagar na tela do caixa --}
        CalculaSaldo() ;

        {-- se o valor dos cheques for maior que zero, gera o registro do pagamento --}
        if (nValCheque > 0) then
        begin

            if not (qryInseri_Temp_Pagtos.Active) then
                qryInseri_Temp_Pagtos.Open ;

            qryInseri_Temp_Pagtos.Insert ;
            qryInseri_Temp_PagtosnCdFormaPagto.Value := qryCondPagtonCdFormaPagto.Value     ;
            qryInseri_Temp_PagtosnCdCondPagto.Value  := qryCondPagtonCdCondPagto.Value      ;
            qryInseri_Temp_PagtosnCdTerceiro.Value   := qryTerceironCdTerceiro.Value        ;
            qryInseri_Temp_PagtosnValPagto.Value     := nValCheque ;
            qryInseri_Temp_PagtosnValEntrada.Value   := 0 ; {-- Cheque nao trata entrada igual crediario --}

            qryInseri_Temp_PagtosnValAcrescimo.Value := 0 ;
            qryInseri_Temp_PagtosnValDesconto.Value  := 0 ;

            if (edtDC.Value > 0) then
            begin
                qryInseri_Temp_PagtosnValAcrescimo.Value      := edtDC.Value ;
                qryInseri_Temp_PagtosnValJurosCondPagto.Value := edtDC.Value ;
            end
            else if (edtDC.Value < 0) then
                qryInseri_Temp_PagtosnValDesconto.Value  := abs(edtDC.Value) ;

            if (edtDM.Value > 0) then
            begin
                qryInseri_Temp_PagtosnValDesconto.Value       := qryInseri_Temp_PagtosnValDesconto.Value + edtDM.Value ;
                qryInseri_Temp_PagtosnValDescontoManual.Value := edtDM.Value ;
            end ;

            if (edtJM.Value > 0) then
                qryInseri_Temp_PagtosnValAcrescimo.Value := qryInseri_Temp_PagtosnValAcrescimo.Value + edtJM.Value ;

            qryInseri_Temp_PagtoscFlgTipo.Value      := 1 ;

            if (qryCondPagtonPercValeDesconto.Value > 0) and (cFlgLiqCrediario = 0) then
                if ((not bItemPromocao) or (bItemPromocao and (qryCondPagtocFlgGeraValeDescontoPromocao.Value = 1))) then
                    qryInseri_Temp_PagtosnValValeDesconto.Value := (qryInseri_Temp_PagtosnValPagto.Value * (qryCondPagtonPercValeDesconto.Value / 100)) ;

            qryInseri_Temp_Pagtos.Post ;

            {-- Atualiza o ID do pagamento nas parcelas --}
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('UPDATE #Temp_Cheque_Pagamento SET nCdTemp_Pagto = ' + qryInseri_Temp_PagtosnCdTemp_Pagto.AsString + ' WHERE nCdTemp_Pagto = 999999') ;
            qryAux.ExecSQL;

            {-- Atualiza o ID do pagamento nas parcelas --}
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('UPDATE #Temp_RestricaoVenda SET nCdTemp_Pagto = ' + qryInseri_Temp_PagtosnCdTemp_Pagto.AsString + ' WHERE nCdTemp_Pagto = 999999 AND cFlgLiberado = 1') ;
            qryAux.ExecSQL;

            {-- Exclui as restri��es n�o liberadas --}
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('DELETE #Temp_RestricaoVenda WHERE nCdTemp_Pagto = 999999 AND cFlgLiberado = 0') ;
            qryAux.ExecSQL;

            qryTemp_Pagtos.Close ;
            qryTemp_Pagtos.Open ;

        end ;

       { -- abre gaveta -- }
       if (cFlgUsaGaveta) then
           frmMenu.prAbreGaveta;
    end ;

    // debito
    if (qryCondPagtonCdTabTipoFormaPagto.Value = 3) then
    begin

        // o processamento do TEF � feito no bot�o FINALIZAR do caixa.

        // quando n�o utiliza TEF
        if (cFlgTEF = 0) then
        begin

            objCaixa_DadosCartao.cTipoOperacao       := 'D' ;
            objCaixa_DadosCartao.edtValorPagar.Value := edtVlrPagto.Value ;
            objCaixa_DadosCartao.ShowModal;

            if (Trim(objCaixa_DadosCartao.edtNrDocto.Text) = '') then
                exit ;

            frmMenu.cCartaoNSU       := objCaixa_DadosCartao.edtNrDocto.Text ;
            frmMenu.cCartaoTransacao := objCaixa_DadosCartao.edtNrAutorizacao.Text ;
            nCdOperadora             := StrToInt(Trim(objCaixa_DadosCartao.edtnCdOperadoraCartao.Text)) ;

        end ;

        if not (qryInseri_Temp_Pagtos.Active) then
             qryInseri_Temp_Pagtos.Open ;

        qryInseri_Temp_Pagtos.Insert ;
        qryInseri_Temp_PagtosnCdFormaPagto.Value := qryCondPagtonCdFormaPagto.Value     ;
        qryInseri_Temp_PagtosnCdCondPagto.Value  := qryCondPagtonCdCondPagto.Value      ;

        if (qryTerceiro.Active) then
            qryInseri_Temp_PagtosnCdTerceiro.Value   := qryTerceironCdTerceiro.Value        ;

        qryInseri_Temp_PagtosnValAcrescimo.Value := 0 ;
        qryInseri_Temp_PagtosnValDesconto.Value  := 0 ;

        if (edtDC.Value > 0) then
        begin
            qryInseri_Temp_PagtosnValAcrescimo.Value      := edtDC.Value ;
            qryInseri_Temp_PagtosnValJurosCondPagto.Value := edtDC.Value ;
        end
        else if (edtDC.Value < 0) then
            qryInseri_Temp_PagtosnValDesconto.Value  := abs(edtDC.Value) ;

        if (edtDM.Value > 0) then
        begin
            qryInseri_Temp_PagtosnValDesconto.Value       := qryInseri_Temp_PagtosnValDesconto.Value + edtDM.Value ;
            qryInseri_Temp_PagtosnValDescontoManual.Value := edtDM.Value ;
        end ;

        if (edtJM.Value > 0) then
            qryInseri_Temp_PagtosnValAcrescimo.Value := qryInseri_Temp_PagtosnValAcrescimo.Value + edtJM.Value ;

        qryInseri_Temp_PagtosnValPagto.Value          := edtVlrPagto.Value ;
        qryInseri_Temp_PagtosnValEntrada.Value        := 0 ;
        qryInseri_Temp_PagtoscFlgTipo.Value           := 1 ;
        qryInseri_Temp_PagtoscFlgTEF.Value            := cFlgTEF ;

        if (frmMenu.cCartaoNSU <> '') then
        begin
            qryInseri_Temp_PagtosiNrDocto.Value           := StrToInt(frmMenu.RetNumeros(frmMenu.cCartaoNSU)) ;
            qryInseri_Temp_PagtoscNrDoctoNSU.Value        := frmMenu.cCartaoNSU ;
        end;

        if (frmMenu.cCartaoTransacao <> '') then
        begin
            qryInseri_Temp_PagtosiNrAutorizacao.Value     := StrToInt(frmMenu.RetNumeros(frmMenu.cCartaoTransacao)) ;
            qryInseri_Temp_PagtoscNrAutorizacao.Value     := frmMenu.cCartaoTransacao ;
        end;
        qryInseri_Temp_PagtosnCdOperadoraCartao.Value := nCdOperadora ;

        if (qryCondPagtonPercValeDesconto.Value > 0) and (cFlgLiqCrediario = 0) then
            if ((not bItemPromocao) or (bItemPromocao and (qryCondPagtocFlgGeraValeDescontoPromocao.Value = 1))) then
                qryInseri_Temp_PagtosnValValeDesconto.Value := (qryInseri_Temp_PagtosnValPagto.Value * (qryCondPagtonPercValeDesconto.Value / 100)) ;

        qryInseri_Temp_Pagtos.Post ;

        qryTemp_Pagtos.Close ;
        qryTemp_Pagtos.Open ;

        { -- abre gaveta -- }
        if (cFlgUsaGaveta) then
            frmMenu.prAbreGaveta;
    end ;

    // credito
    if (qryCondPagtonCdTabTipoFormaPagto.Value = 4) then
    begin

        // o processamento do TEF � feito no bot�o FINALIZAR do caixa.

        // quando n�o utiliza TEF
        if (cFlgTEF = 0) then
        begin

            objCaixa_DadosCartao.cTipoOperacao       := 'C' ;
            objCaixa_DadosCartao.edtValorPagar.Value := edtVlrPagto.Value ;
            objCaixa_DadosCartao.ShowModal;

            if (Trim(objCaixa_DadosCartao.edtNrDocto.Text) = '') then
                exit ;

            frmMenu.cCartaoNSU       := objCaixa_DadosCartao.edtNrDocto.Text ;
            frmMenu.cCartaoTransacao := objCaixa_DadosCartao.edtNrAutorizacao.Text ;
            nCdOperadora             := StrToInt(Trim(objCaixa_DadosCartao.edtnCdOperadoraCartao.Text)) ;

        end ;

        if not (qryInseri_Temp_Pagtos.Active) then
            qryInseri_Temp_Pagtos.Open ;

        qryInseri_Temp_Pagtos.Insert ;
        qryInseri_Temp_PagtosnCdFormaPagto.Value := qryCondPagtonCdFormaPagto.Value     ;
        qryInseri_Temp_PagtosnCdCondPagto.Value  := qryCondPagtonCdCondPagto.Value      ;

        if (qryTerceiro.Active) then
            qryInseri_Temp_PagtosnCdTerceiro.Value   := qryTerceironCdTerceiro.Value        ;

        qryInseri_Temp_PagtosnValAcrescimo.Value := 0 ;
        qryInseri_Temp_PagtosnValDesconto.Value  := 0 ;

        if (edtDC.Value > 0) then
        begin
            qryInseri_Temp_PagtosnValAcrescimo.Value      := edtDC.Value ;
            qryInseri_Temp_PagtosnValJurosCondPagto.Value := edtDC.Value ;
        end
        else if (edtDC.Value < 0) then
            qryInseri_Temp_PagtosnValDesconto.Value  := abs(edtDC.Value) ;

        if (edtDM.Value > 0) then
        begin
            qryInseri_Temp_PagtosnValDesconto.Value       := qryInseri_Temp_PagtosnValDesconto.Value + edtDM.Value ;
            qryInseri_Temp_PagtosnValDescontoManual.Value := edtDM.Value ;
        end ;

        if (edtJM.Value > 0) then
            qryInseri_Temp_PagtosnValAcrescimo.Value := qryInseri_Temp_PagtosnValAcrescimo.Value + edtJM.Value ;

        qryInseri_Temp_PagtosnValPagto.Value          := edtVlrPagto.Value ;
        qryInseri_Temp_PagtosnValEntrada.Value        := 0 ;
        qryInseri_Temp_PagtoscFlgTipo.Value           := 1 ;
        qryInseri_Temp_PagtoscFlgTEF.Value            := cFlgTEF ;

        if (frmMenu.cCartaoNSU <> '') then
        begin
            qryInseri_Temp_PagtosiNrDocto.Value           := StrToInt(frmMenu.RetNumeros(frmMenu.cCartaoNSU)) ;
            qryInseri_Temp_PagtoscNrDoctoNSU.Value        := frmMenu.cCartaoNSU ;
        end;

        if (frmMenu.cCartaoTransacao <> '') then
        begin
            qryInseri_Temp_PagtosiNrAutorizacao.Value     := StrToInt(frmMenu.RetNumeros(frmMenu.cCartaoTransacao)) ;
            qryInseri_Temp_PagtoscNrAutorizacao.Value     := frmMenu.cCartaoTransacao ;
        end;


        qryInseri_Temp_PagtosnCdOperadoraCartao.Value := nCdOperadora ;

        if (qryCondPagtonPercValeDesconto.Value > 0) and (cFlgLiqCrediario = 0) then
            if ((not bItemPromocao) or (bItemPromocao and (qryCondPagtocFlgGeraValeDescontoPromocao.Value = 1))) then
                qryInseri_Temp_PagtosnValValeDesconto.Value := (qryInseri_Temp_PagtosnValPagto.Value * (qryCondPagtonPercValeDesconto.Value / 100)) ;

        qryInseri_Temp_Pagtos.Post ;

        qryTemp_Pagtos.Close ;
        qryTemp_Pagtos.Open ;

        { -- abre gaveta -- }
        if (cFlgUsaGaveta) then
            frmMenu.prAbreGaveta;
    end ;

    // credi�rio
    if (qryCondPagtonCdTabTipoFormaPagto.Value = 5) then
    begin
        SimularCrediario() ;

        if not (objCaixa_ParcelaCred.bProcessado) then
        begin
            MensagemAlerta('Opera��o cancelada.') ;
            abort ;
        end ;

        CalculaSaldo() ;

        {-- soma o valor das parcelas do credi�rio --}
        nValCrediario := 0 ;

        objCaixa_ParcelaCred.qryTemp_Crediario.First ;

        while not objCaixa_ParcelaCred.qryTemp_Crediario.eof do
        begin
            nValCrediario := nValCrediario + objCaixa_ParcelaCred.qryTemp_CrediarionValParcela.Value ;
            objCaixa_ParcelaCred.qryTemp_Crediario.next ;
        end ;

        objCaixa_ParcelaCred.qryTemp_Crediario.First ;

        {-- atualiza o saldo a pagar na tela do caixa --}
        CalculaSaldo() ;

        {-- se o valor das parcelas for maior que zero, gera o registro do pagamento --}
        if (nValCrediario > 0) then
        begin

            if not (qryInseri_Temp_Pagtos.Active) then
                qryInseri_Temp_Pagtos.Open ;

            qryInseri_Temp_Pagtos.Insert ;
            qryInseri_Temp_PagtosnCdFormaPagto.Value := qryCondPagtonCdFormaPagto.Value     ;
            qryInseri_Temp_PagtosnCdCondPagto.Value  := qryCondPagtonCdCondPagto.Value      ;
            qryInseri_Temp_PagtosnCdTerceiro.Value   := qryTerceironCdTerceiro.Value        ;
            qryInseri_Temp_PagtosnValPagto.Value     := nValCrediario ;
            qryInseri_Temp_PagtosnValEntrada.Value   := objCaixa_ParcelaCred.edtValEntrada.Value ;

            qryInseri_Temp_PagtosnValAcrescimo.Value := 0 ;
            qryInseri_Temp_PagtosnValDesconto.Value  := 0 ;

            if (edtDC.Value > 0) then
            begin
                qryInseri_Temp_PagtosnValAcrescimo.Value      := edtDC.Value ;
                qryInseri_Temp_PagtosnValJurosCondPagto.Value := edtDC.Value ;
            end
            else if (edtDC.Value < 0) then
                qryInseri_Temp_PagtosnValDesconto.Value  := abs(edtDC.Value) ;

            if (edtDM.Value > 0) then
            begin
                qryInseri_Temp_PagtosnValDesconto.Value       := qryInseri_Temp_PagtosnValDesconto.Value + edtDM.Value ;
                qryInseri_Temp_PagtosnValDescontoManual.Value := edtDM.Value ;
            end ;

            if (edtJM.Value > 0) then
                qryInseri_Temp_PagtosnValAcrescimo.Value := qryInseri_Temp_PagtosnValAcrescimo.Value + edtJM.Value ;

            qryInseri_Temp_PagtoscFlgTipo.Value      := 1 ;

            if (qryCondPagtonPercValeDesconto.Value > 0) and (cFlgLiqCrediario = 0) then
                if ((not bItemPromocao) or (bItemPromocao and (qryCondPagtocFlgGeraValeDescontoPromocao.Value = 1))) then
                    qryInseri_Temp_PagtosnValValeDesconto.Value := (qryInseri_Temp_PagtosnValPagto.Value * (qryCondPagtonPercValeDesconto.Value / 100)) ;

            qryInseri_Temp_Pagtos.Post ;

            edtEntrada.Value := edtEntrada.Value + objCaixa_ParcelaCred.edtValEntrada.Value ;

            {-- Atualiza o ID do pagamento nas parcelas --}
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('UPDATE #Temp_Crediario SET nCdTemp_Pagto = ' + qryInseri_Temp_PagtosnCdTemp_Pagto.AsString + ' WHERE nCdTemp_Pagto = 999999') ;
            qryAux.ExecSQL;

            {-- Atualiza o ID do pagamento nas parcelas --}
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('UPDATE #Temp_RestricaoVenda SET nCdTemp_Pagto = ' + qryInseri_Temp_PagtosnCdTemp_Pagto.AsString + ' WHERE nCdTemp_Pagto = 999999 AND cFlgLiberado = 1') ;
            qryAux.ExecSQL;

            {-- Exclui as restri��es n�o liberadas --}
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('DELETE #Temp_RestricaoVenda WHERE nCdTemp_Pagto = 999999 AND cFlgLiberado = 0') ;
            qryAux.ExecSQL;

            qryTemp_Pagtos.Close ;
            qryTemp_Pagtos.Open ;

            { -- abre gaveta -- }
            if (cFlgUsaGaveta) then
                frmMenu.prAbreGaveta;
        end ;

    end ;

    {-- testa se o pagamento foi realmente efetivado --}
    nValorPagoOld := edtValorPago.Value ;

    CalculaSaldo() ;

    if (nValorPagoOld < edtValorPago.Value) then
    begin

        qryCondPagto.Close ;
        qryTerceiro.Close ;

        {-- Zera os valores da tela --}
        edtValor.Value    := 0 ;
        edtDC.Value       := 0 ;
        edtDM.Value       := 0 ;
        edtJM.Value       := 0 ;
        edtVlrPagto.Value := 0 ;

        {-- Se sobrou algum saldo a pagar, al�m da entrada do credi�rio, posiciona no campo de pagamento novamente --}
        if ((frmMenu.TBRound(edtSaldo.Value,2) - frmMenu.TBRound(edtEntrada.Value,2)) > 0) then
        begin
            edtValor.Value := (edtSaldo.Value - edtEntrada.Value) ;

            if (edtvalor.Enabled) then
                edtValor.SetFocus
            else
                btTrocaCondicao.SetFocus;
                
            exit ;
        end ;

        if (edtEntrada.Value > 0) then
            btPgEntrada.SetFocus ;

        if (frmMenu.TBRound(edtSaldo.Value,2) <= 0) then
        begin
            btFinalizar.SetFocus;
            abort ;
        end ;
    end
    else
        if (edtvalor.Enabled) then
            edtValor.SetFocus
        else btIncPagamento.SetFocus;

end;

procedure TfrmCaixa_Recebimento.ExcluirPagamentos();
begin

    {-- Exclui todos os pagamentos que n�o tenham sido gerados por proposta de renegocia��o --}
    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Add('IF OBJECT_ID(' + Chr(39) + 'tempdb..#Temp_Pagtos' + Chr(39) + ') IS NOT NULL DELETE FROM #Temp_Pagtos WHERE nCdPropostaReneg IS NULL') ;
    qryAux.ExecSQL;

    frmMenu.ZeraTemp('#Temp_Cheque_Pagamento');
    frmMenu.ZeraTemp('#Temp_Crediario');
    frmMenu.ZeraTemp('#Temp_RestricaoVenda');

    qryTemp_Pagtos.Close ;
    qryTemp_Pagtos.Open ;

    edtEntrada.Value := 0 ;
    edtTroco.Value   := 0 ;

    {-- soma para saber se ficou alguma proposta de renegocia��o com valor de entrada --}
    try
        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('IF OBJECT_ID(' + Chr(39) + 'tempdb..#Temp_Pagtos' + Chr(39) + ') IS NOT NULL SELECT IsNull(Sum(nValEntrada),0) FROM #Temp_Pagtos ELSE SELECT 0') ;
        qryAux.Open ;

        edtEntrada.Value := qryAux.FieldList[0].Value ;
    except
        edtEntrada.Value := 0 ;
    end ;

    CalculaSaldo() ;

end ;
procedure TfrmCaixa_Recebimento.btExcluiPagamentosClick(Sender: TObject);
var
   nCdFormaPagto, nCdCondPagto : integer ;
   nValCrediario, nValCheque : double ;
   cArquivo   : TextFile;
   cLinha     : string ;
begin

    if not (qryTemp_Pagtos.Active) then
        qryTemp_Pagtos.Open ;

    if (qryTemp_Pagtos.RecordCount = 0) then
    begin
        MensagemAlerta('Nenhum pagamento registrado.');
        Exit;
    end;

    case MessageDlg('Confirma a exclus�o dos pagamentos ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo:exit ;
    end ;

    ExcluirPagamentos() ;

    {-- posiciona os pagamentos iniciais --}
    edtValor.Value    := nValorPGInicial ;
    edtDC.Value       := nValorDCInicial ;
    edtDM.Value       := nValorDMInicial ;
    edtJM.Value       := nValorJMInicial ;
    edtVlrPagto.Value := nValorPGFinal   ;

    if (cFlgLiqCrediario = 0) then
    begin

        DesativaValores ;

        qryTemp_Pedido.First ;

        if not qryTemp_Pedido.Eof then
        begin
            qryCondPagto.Close ;
            qryCondPagto.Parameters.ParamByName('nCdCondPagto').Value := qryPedidonCdCondPagto.Value ;
            qryCondPagto.Parameters.ParamByName('nCdLoja').Value      := frmMenu.nCdLojaAtiva;
            qryCondPagto.Open ;

        end ;

        if (qryPedidonCdTerceiro.Value > 0) then
        begin

            qryTerceiro.Close ;
            qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := qryPedidonCdTerceiro.Value ;
            qryTerceiro.Open ;

        end ;

        btIncPagamento.SetFocus ;

    end
    else if (edtValor.Enabled) then edtValor.SetFocus
         else if (edtEntrada.Value > 0) then
             btPgEntrada.SetFocus;

end;

procedure TfrmCaixa_Recebimento.edtNumCarnetKeyPress(Sender: TObject;
  var Key: Char);
var
    iValAux                 : integer ;
    nValorAux               : double ;
    objCaixa_ParcelaVencida : TfrmCaixa_ParcelaVencida ;
    objCaixa_CredErro       : TfrmCaixa_CredErro;
begin
  if (key = #13) then
  begin

    if (Trim(edtNumCarnet.Text) = '') then
    begin
        if (edtParcelas.Value = 0) then
        begin
            if (MessageDlg('Nenhuma parcela informada. Deseja consultar as parcelas ?',mtConfirmation,[mbYes,mbNo],0) = MRYES) then
                btIncParcela.Click
        end
        else
        begin

            {--se foram selecionadas parcelas, verifica se o cliente nao tem outras parcelas que estejam vencidas--}

            objCaixa_ParcelaVencida := TfrmCaixa_ParcelaVencida.Create( Self ) ;

            try

                if (objCaixa_ParcelaVencida.verificaParcelasAtraso > 0) then
                begin
                    if (MessageDlg('O Cliente tem parcela vencida que n�o foi selecionada. Deseja Visualizar ?',mtConfirmation,[mbYes,mbNo],0) = MrYes) then
                    begin

                        objCaixa_ParcelaVencida.ShowModal;
                        objCaixa_ParcelaVencida.qryParcelasVencidas.Close;
                        edtNumCarnet.SetFocus;
                        abort;

                    end ;
                end ;

                objCaixa_ParcelaVencida.qryParcelasVencidas.Close;

            finally

                freeAndNil( objCaixa_ParcelaVencida ) ;

            end ;

            cxPageControl1.ActivePage := tabPagamento ;

            AtivaValores;

            edtValor.SetFocus;

        end ;

        exit ;
    end ;

    try
        iValAux := StrToInt(Trim(edtNumCarnet.Text)) ;
    except
        MensagemAlerta('N�mero de Carnet inv�lido.') ;
        edtNumCarnet.SetFocus ;
        edtNumCarnet.Text := '' ;
        exit ;
    end ;

    qryParcelas.Close ;
    qryParcelas.Open  ;

    SP_BUSCA_PARCELA_CREDIARIO.Close ;
    SP_BUSCA_PARCELA_CREDIARIO.Parameters.ParamByName('@nCdCrediario').Value := frmMenu.ConvInteiro(edtNumCarnet.Text) ;
    SP_BUSCA_PARCELA_CREDIARIO.Parameters.ParamByName('@nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva;
    SP_BUSCA_PARCELA_CREDIARIO.Parameters.ParamByName('@nCdLoja').Value      := frmMenu.nCdLojaAtiva;
    SP_BUSCA_PARCELA_CREDIARIO.Open ;

    if not SP_BUSCA_PARCELA_CREDIARIO.Eof then
    begin
        if (SP_BUSCA_PARCELA_CREDIARIOcFlgCobradora.Value = 1 ) and ( SP_BUSCA_PARCELA_CREDIARIOcFlgCobradoraPermiteRecLoja.Value= 0 ) then
        begin
            MensagemErro('T�tulo enviado para cobradora. Imposs�vel receber no caixa.'+#13#13 + 'Parcela : ' + frmMenu.ZeroEsquerda(SP_BUSCA_PARCELA_CREDIARIOiParcela.AsString,3)) ;
            edtNumCarnet.Text := '' ;
            edtNumCarnet.SetFocus;
            abort ;
        end ;
    end ;

    qryParcelas.Close ;
    qryParcelas.Open  ;

    edtNumCarnet.Text := '' ;

    qryParcelas.First ;

    nValorAux         := edtParcelas.Value ;
    edtParcelas.Value := 0 ;

    nValorPGInicial   := 0 ;
    nValorDCInicial   := 0 ;
    nValorDMInicial   := 0 ;
    nValorJMInicial   := 0 ;
    nValorPGFinal     := 0 ;

    while not qryParcelas.Eof do
    begin

        AcumulaValorInicial(qryParcelasnValTotal.Value
                           ,0
                           ,0
                           ,0
                           ,qryParcelasnValTotal.Value) ;
    
        edtParcelas.Value := edtParcelas.Value + qryParcelasnValTotal.Value ;

        qryParcelas.next ;
    end ;

    qryParcelas.First ;

    CalculaSaldo() ;

    if (nValorAux >= edtParcelas.Value) then
    begin
        MensagemAlerta('N�mero de Carnet n�o encontrado ou j� adicionado na lista de parcelas.') ;
        abort ;
    end ;

    edtNumCarnet.Text := '' ;
    edtNumCarnet.SetFocus ;

    { -- verifica se credi�rio possui todas as parcelas no banco de dados, caso n�o exista, � informado quais
         parcelas supostamente ainda n�o foram replicadas devido problemas de sincroniza��o.
         uma forma de alertar o usu�rio a n�o receber a parcela at� que todas tenham sido sincronizadas na loja recebedora -- }
    try
        objCaixa_CredErro := TfrmCaixa_CredErro.Create(nil);

        objCaixa_CredErro.qryVerificaCrediario.Close;
        objCaixa_CredErro.qryVerificaCrediario.Open;

        if (not objCaixa_CredErro.qryVerificaCrediario.IsEmpty) then
        begin
            MensagemAlerta('Existem informa��es inconsistentes no credi�rio selecionado.');
            objCaixa_CredErro.ShowModal;

            if (MessageDlg('Credi�rio ' + Trim(edtNumCarnet.Text) + ' n�o possui todas as parcelas sincronizadas no sistema. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
            begin
                frmMenu.ZeraTemp('#Temp_Titulos');

                qryParcelas.CLose ;
                qryParcelas.Open ;

                cFlgLiqCrediario  := 1 ;
                nValorPGInicial   := 0 ;
                nValorDCInicial   := 0 ;
                nValorDMInicial   := 0 ;
                nValorJMInicial   := 0 ;
                nValorPGFinal     := 0 ;
                edtParcelas.Value := 0 ;

                CalculaSaldo();

                DesativaValores;

                edtNumCarnet.SetFocus ;
            end;
        end;
    finally
        FreeAndNil(objCaixa_CredErro);
    end;
  end;
end;

procedure TfrmCaixa_Recebimento.btPgEntradaClick(Sender: TObject);
var
    nCdFormaPagto, nCdCondPagto : integer ;
    nValCrediario : double ;
    nValCheque    : double ;
    cFlgLiqCrediarioOld : integer ;
    cFlgTEF       : integer ;

label
    SelecionaFormaPagamento;
begin


      cFlgTEF := 0 ;


    if not (qryTemp_Pagtos.Active) then
        qryTemp_Pagtos.Open ;

    CalculaSaldo() ;

    if (edtEntrada.Value <= 0) then
    begin
        MensagemAlerta('Nenhum entrada em aberto para pagamento.') ;
        exit;
    end ;

    cFlgLiqCrediarioOld := cFlgLiqCrediario ;

    cFlgLiqCrediario := 1 ;


    SelecionaFormaPagamento:

    objPdvFormaPagto.nCdFormaPagtoPre := 0 ;
    objPdvFormaPagto.cFlgLiqCrediario := cFlgLiqCrediario ;
    nCdFormaPagto                     := objPdvFormaPagto.SelecionaFormaPagto() ;

    objPdvCondPagto.nCdCondPagtoPre       := 0 ;
    objPdvCondPagto.nCdLoja               := frmMenu.nCdLojaAtiva;
    objPdvCondPagto.nCdFormaPagto         := nCdFormaPagto       ;
    objPdvCondPagto.cFlgLiqCrediario      := cFlgLiqCrediario    ;
    objPdvCondPagto.edtValorPagar.Value   := edtEntrada.Value    ;
    objPdvCondPagto.edtSaldo.Value        := edtEntrada.Value    ;
    objPdvCondPagto.edtValorPagar.Enabled := True ;
    objPdvCondPagto.bItemPromocao         := false;

    nCdCondPagto  := objPdvCondPagto.SelecionaCondPagto() ;

    qryCondPagto.Close ;
    qryCondPagto.Parameters.ParamByName('nCdCondPagto').Value := nCdCondPagto ;
    qryCondPagto.Parameters.ParamByName('nCdLoja').Value      := frmMenu.nCdLojaAtiva;
    qryCondPagto.Open ;

    {-- se o TEF estiver ativo e a forma de pagamento da condi��o utilizar TEF --}
    if ((cFlgUsaTEF) and (qryCondPagtocFlgTEF.Value = 1)) then
        cFlgTEF := 1
    else
        cFlgTEF := 0;


    // dinheiro
    if (qryCondPagtonCdTabTipoFormaPagto.Value = 1) then
    begin

        objCaixa_Dinheiro.edtSaldo.Value    := objPdvCondPagto.nValorFinal;
        objCaixa_Dinheiro.edtDinheiro.Value := objPdvCondPagto.nValorFinal;
        objCaixa_Dinheiro.edtDinheiro.SelectAll;
        objCaixa_Dinheiro.ShowModal;

        if (objCaixa_Dinheiro.edtDinheiro.Value <= 0) then
        begin

            MensagemAlerta('Nenhum valor informado, o pagamento n�o foi registrado.') ;
            exit ;
        end ;

        if not (qryInseri_Temp_Pagtos.Active) then
             qryInseri_Temp_Pagtos.Open ;

        qryInseri_Temp_Pagtos.Insert ;
        qryInseri_Temp_PagtosnCdFormaPagto.Value := nCdFormaPagto ;
        qryInseri_Temp_PagtosnCdCondPagto.Value  := nCdCondPagto  ;
        qryInseri_Temp_PagtosnCdTerceiro.Value   := nCdTerceiro   ;
        qryInseri_Temp_PagtosnValPagto.Value     := objCaixa_Dinheiro.edtDinheiro.Value ;
        qryInseri_Temp_PagtosnValEntrada.Value   := 0 ;
        qryInseri_Temp_PagtoscFlgTipo.Value      := 2 ;

        if (objCaixa_Dinheiro.edtDinheiro.Value > objCaixa_Dinheiro.edtSaldo.Value) then
            qryInseri_Temp_PagtosnValPagto.Value := objCaixa_Dinheiro.edtSaldo.Value ;

        qryInseri_Temp_Pagtos.Post ;

        edtEntrada.Value := edtEntrada.Value - qryInseri_Temp_PagtosnValPagto.Value ;

        qryTemp_Pagtos.Close ;
        qryTemp_Pagtos.Open ;

        if (objCaixa_Dinheiro.edtDinheiro.Value > objCaixa_Dinheiro.edtSaldo.Value) then
        begin
            edtTroco.Value := (objCaixa_Dinheiro.edtDinheiro.Value - objCaixa_Dinheiro.edtSaldo.Value) ;
            ShowMessage('Valor do Troco : ' + edtTroco.Text) ;
        end ;

    end ;

    // cheque
    if (qryCondPagtonCdTabTipoFormaPagto.Value = 2) then
    begin
        qryTemp_Cheque.Close ;
        qryTemp_Cheque.ExecSQL ;

        nValCheque := SimularCheque( TRUE );

        CalculaSaldo() ;

        {-- soma o valor dos cheques --}
        nValCheque := 0 ;

        objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.First ;

        while not objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.eof do
        begin
            nValCheque := nValCheque + objCaixa_PagtoCheque.qryPrepara_Temp_ChequenValCheque.Value ;
            objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.next ;
        end ;

        objCaixa_PagtoCheque.qryPrepara_Temp_Cheque.First ;

        {-- atualiza o saldo a pagar na tela do caixa --}
        CalculaSaldo() ;

        {-- se o valor dos cheques for maior que zero, gera o registro do pagamento --}
        if (nValCheque > 0) then
        begin

            if not (qryInseri_Temp_Pagtos.Active) then
                 qryInseri_Temp_Pagtos.Open ;

            qryInseri_Temp_Pagtos.Insert ;
            qryInseri_Temp_PagtosnCdFormaPagto.Value := qryCondPagtonCdFormaPagto.Value     ;
            qryInseri_Temp_PagtosnCdCondPagto.Value  := qryCondPagtonCdCondPagto.Value      ;
            qryInseri_Temp_PagtosnCdTerceiro.Value   := qryTerceironCdTerceiro.Value        ;
            qryInseri_Temp_PagtosnValPagto.Value     := nValCheque ;
            qryInseri_Temp_PagtosnValEntrada.Value   := 0 ;
            qryInseri_Temp_PagtoscFlgTipo.Value      := 2 ;
            qryInseri_Temp_Pagtos.Post ;

            edtEntrada.Value := edtEntrada.Value - nValCheque ;

            {-- Atualiza o ID do pagamento nas parcelas --}
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('UPDATE #Temp_Cheque_Pagamento SET nCdTemp_Pagto = ' + qryInseri_Temp_PagtosnCdTemp_Pagto.AsString + ' WHERE nCdTemp_Pagto = 999999') ;
            qryAux.ExecSQL;

            {-- Atualiza o ID do pagamento nas parcelas --}
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('UPDATE #Temp_RestricaoVenda SET nCdTemp_Pagto = ' + qryInseri_Temp_PagtosnCdTemp_Pagto.AsString + ' WHERE nCdTemp_Pagto = 999999 AND cFlgLiberado = 1') ;
            qryAux.ExecSQL;

            {-- Exclui as restri��es n�o liberadas --}
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('DELETE #Temp_RestricaoVenda WHERE nCdTemp_Pagto = 999999 AND cFlgLiberado = 0') ;
            qryAux.ExecSQL;
            
            qryTemp_Pagtos.Close ;
            qryTemp_Pagtos.Open ;

        end ;

    end ;

    // debito
    if (qryCondPagtonCdTabTipoFormaPagto.Value = 3) then
    begin
       // quando n�o utiliza TEF
       if (cFlgTEF = 0) then
       begin
        objCaixa_DadosCartao.cTipoOperacao       := 'D' ;
        objCaixa_DadosCartao.edtValorPagar.Value := objPDVCondPagto.nValorFinal ;
        objCaixa_DadosCartao.ShowModal;

        if (Trim(objCaixa_DadosCartao.edtNrDocto.Text) = '') then
            exit ;
       end;
        if not (qryInseri_Temp_Pagtos.Active) then
             qryInseri_Temp_Pagtos.Open ;

        qryInseri_Temp_Pagtos.Insert ;
        qryInseri_Temp_PagtosnCdFormaPagto.Value      := nCdFormaPagto ;
        qryInseri_Temp_PagtosnCdCondPagto.Value       := nCdCondPagto  ;
        qryInseri_Temp_PagtosnCdTerceiro.Value        := nCdTerceiro   ;
        qryInseri_Temp_PagtosnValPagto.Value          := objPdvCondPagto.nValorFinal ;
        qryInseri_Temp_PagtosnValEntrada.Value        := 0 ;
        qryInseri_Temp_PagtoscFlgTipo.Value           := 2 ;
        qryInseri_Temp_PagtoscFlgTEF.Value            := cFlgTEF ;

       // quando n�o utiliza TEF
       if (cFlgTEF = 0) then
       begin
        qryInseri_Temp_PagtosiNrDocto.Value           := StrToInt(frmMenu.RetNumeros(objCaixa_DadosCartao.edtNrDocto.Text)) ;
        qryInseri_Temp_PagtoscNrDoctoNSU.Value        := objCaixa_DadosCartao.edtNrDocto.Text ;
        qryInseri_Temp_PagtosiNrAutorizacao.Value     := StrToInt(frmMenu.RetNumeros(objCaixa_DadosCartao.edtNrAutorizacao.Text)) ;
        qryInseri_Temp_PagtoscNrAutorizacao.Value     := objCaixa_DadosCartao.edtNrAutorizacao.Text ;
        qryInseri_Temp_PagtosnCdOperadoraCartao.Value := StrToInt(Trim(objCaixa_DadosCartao.edtnCdOperadoraCartao.Text)) ;
       end;
        qryInseri_Temp_Pagtos.Post ;

        edtEntrada.Value := edtEntrada.Value - objPdvCondPagto.nValorFinal ;

        qryTemp_Pagtos.Close ;
        qryTemp_Pagtos.Open ;

    end ;

    // credito
    if (qryCondPagtonCdTabTipoFormaPagto.Value = 4) then
    begin
       // quando n�o utiliza TEF
       if (cFlgTEF = 0) then
       begin
        objCaixa_DadosCartao.cTipoOperacao       := 'C' ;
        objCaixa_DadosCartao.edtValorPagar.Value := objPdvCondPagto.nValorFinal ;
        objCaixa_DadosCartao.ShowModal;

        if (Trim(objCaixa_DadosCartao.edtNrDocto.Text) = '') then
            exit ;
       end;
        if not (qryInseri_Temp_Pagtos.Active) then
             qryInseri_Temp_Pagtos.Open ;

        qryInseri_Temp_Pagtos.Insert ;
        qryInseri_Temp_PagtosnCdFormaPagto.Value      := nCdFormaPagto ;
        qryInseri_Temp_PagtosnCdCondPagto.Value       := nCdCondPagto  ;
        qryInseri_Temp_PagtosnCdTerceiro.Value        := nCdTerceiro   ;
        qryInseri_Temp_PagtosnValPagto.Value          := objPdvCondPagto.nValorFinal;
        qryInseri_Temp_PagtosnValEntrada.Value        := 0 ;
        qryInseri_Temp_PagtoscFlgTipo.Value           := 2 ;
        qryInseri_Temp_PagtoscFlgTEF.Value            := cFlgTEF ;
       // quando n�o utiliza TEF
       if (cFlgTEF = 0) then
       begin
        qryInseri_Temp_PagtosiNrDocto.Value           := StrToInt(frmMenu.RetNumeros(objCaixa_DadosCartao.edtNrDocto.Text)) ;
        qryInseri_Temp_PagtoscNrDoctoNSU.Value        := objCaixa_DadosCartao.edtNrDocto.Text ;
        qryInseri_Temp_PagtosiNrAutorizacao.Value     := StrToInt(frmMenu.RetNumeros(objCaixa_DadosCartao.edtNrAutorizacao.Text)) ;
        qryInseri_Temp_PagtoscNrAutorizacao.Value     := objCaixa_DadosCartao.edtNrAutorizacao.Text ;
        qryInseri_Temp_PagtosnCdOperadoraCartao.Value := StrToInt(Trim(objCaixa_DadosCartao.edtnCdOperadoraCartao.Text)) ;
       end;
        qryInseri_Temp_Pagtos.Post ;

        edtEntrada.Value := edtEntrada.Value - objPdvCondPagto.nValorFinal ;
        qryTemp_Pagtos.Close ;
        qryTemp_Pagtos.Open ;

    end ;

    cFlgLiqCrediario := cFlgLiqCrediarioOld ;

    // credi�rio
    if (qryCondPagtonCdTabTipoFormaPagto.Value = 5) then
    begin
        MensagemAlerta('Condi��o n�o permitida para pagamento de entrada.') ;
        exit ;
    end ;

    if (edtEntrada.Value < 0) then
    begin
        edtEntrada.Value := 0 ;
    end ;

    CalculaSaldo() ;

    qryCondPagto.Close ;
    qryTerceiro.Close ;
    
    if (frmMenu.TBRound(edtSaldo.Value,2) <= 0) then
    begin
        btFinalizar.SetFocus;
    end
    else btPgEntrada.SetFocus ;

end;

procedure TfrmCaixa_Recebimento.qryTemp_PagtosCalcFields(
  DataSet: TDataSet);
begin

  if (qryTemp_PagtosnCdOperadoraCartao.Value > 0) then
  begin
{      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT nCdTabTipoFormaPagto FROM FormaPagto WHERE nCdFormaPagto = ' + qryTemp_PagtosnCdFormaPagto.AsString) ;
      qryAux.Open ;

      if not qryAux.Eof then
      begin
          qryTemp_PagtosnCdTabTipoFormaPagto.Value := qryAux.FieldList[0].Value ;
      end;
 }

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT cNmOperadoraCartao FROM OperadoraCartao WHERE nCdOperadoraCartao = ' + qryTemp_PagtosnCdOperadoraCartao.AsString) ;
      qryAux.Open ;

      if not qryAux.Eof then
      begin
          qryTemp_PagtoscNmOperadoraCartao.Value := qryAux.FieldList[0].Value ;
      end;
  end ;

end;

procedure TfrmCaixa_Recebimento.btExcluirParcelasClick(Sender: TObject);
begin

    if not qryParcelas.Active or (qryParcelas.RecordCount = 0) then
    begin
        MensagemAlerta('Nenhuma parcela para remover da tela.') ;
        exit ;
    end ;

    case MessageDlg('Deseja remover da tela as parcelas selecionadas para pagamento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
    end ;

    frmMenu.ZeraTemp('#Temp_Titulos');

    qryParcelas.CLose ;
    qryParcelas.Open ;

    cFlgLiqCrediario  := 1 ;
    nValorPGInicial   := 0 ;
    nValorDCInicial   := 0 ;
    nValorDMInicial   := 0 ;
    nValorJMInicial   := 0 ;
    nValorPGFinal     := 0 ;
    edtParcelas.Value := 0 ;

    CalculaSaldo() ;

    DesativaValores;

    edtNumCarnet.SetFocus ;

end;

procedure TfrmCaixa_Recebimento.Button1Click(Sender: TObject);
begin
    ShowMessage(EstadoECF()) ;
end;

procedure TfrmCaixa_Recebimento.btUsaValeClick(Sender: TObject);
var
    cNrVale : string ;
begin

    frmMenu.InputQuery('Identifica��o do Vale','Informe o N�mero do Vale: ',cNrVale);

    if (Trim(cNrVale) = '') then
        Exit;

    {try
        StrToInt(cNrVale);
    except
        frmMenu.MensagemErro('C�digo do vale inv�lido, informe apenas n�meros.');
        Exit;
    end;}

    case MessageDlg(('Confirma utiliza��o do vale No ' + cNrVale + ' ?'),mtConfirmation,[mbYes,mbNo],0) of
        mrNo:exit ;
    end ;

    try
        SP_VALIDA_TROCA_VALE.Close ;
        SP_VALIDA_TROCA_VALE.Parameters.ParamByName('@nCdEmpresa').Value    := frmMenu.nCdEmpresaAtiva;
        SP_VALIDA_TROCA_VALE.Parameters.ParamByName('@cNrVale').Value       := cNrVale;
        SP_VALIDA_TROCA_VALE.Parameters.ParamByName('@cFlgReembolso').Value := 0;
        SP_VALIDA_TROCA_VALE.ExecProc;
    except
        MensagemErro('Erro no processamento.') ;
        raise ;
    end ;

    if (SP_VALIDA_TROCA_VALE.Parameters.ParamByName('@iOK').Value > 0) then
    begin
        MensagemAlerta(SP_VALIDA_TROCA_VALE.Parameters.ParamByName('@cMsg').Value) ;
        abort ;
    end ;

    // gera o registro da utiliza��o do vale.
    if not (qryTemp_Pagtos.Active) then
        qryTemp_Pagtos.Open ;

    if not (qryInseri_Temp_Pagtos.Active) then
        qryInseri_Temp_Pagtos.Open ;

    qryInseri_Temp_Pagtos.Insert ;
    qryInseri_Temp_PagtosnCdFormaPagto.Value      := StrToInt(frmMenu.LeParametro('FORMAPAGTOVLCX')) ;
    qryInseri_Temp_PagtosnValPagto.Value          := SP_VALIDA_TROCA_VALE.Parameters.ParamByName('@nSaldoVale').Value ;
    qryInseri_Temp_PagtosnValEntrada.Value        := 0       ;
    qryInseri_Temp_PagtoscFlgTipo.Value           := 2       ;
    qryInseri_Temp_PagtosiNrDocto.Value           := StrToInt(cNrVale) ;
    qryInseri_Temp_PagtoscFlgVale.Value           := 1 ;
    qryInseri_Temp_Pagtos.Post ;

    edtVale.Value := edtVale.Value + qryInseri_Temp_PagtosnValPagto.Value ;

    CalculaSaldo() ;

    qryTemp_Pagtos.Close ;
    qryTemp_Pagtos.Open ;

    if (frmMenu.TBRound(edtSaldo.Value,2) <= 0) then
    begin
        btFinalizar.SetFocus;
    end
    else
    begin
        AtivaValores;
        edtValor.Value := edtSaldo.Value ;
        edtValor.SetFocus;
    end ;
    
end;

procedure TfrmCaixa_Recebimento.btConsultaChequeClick(Sender: TObject);
var
    cNmTerceiro : string ;
    objCaixa_SelCliente : TfrmCaixa_SelCliente ;
label
    SelecionaCheques;
begin

    SelecionaCheques:

    objCaixa_SelCliente := TfrmCaixa_SelCliente.Create( Self ) ;

    nCdTerceiro := objCaixa_SelCliente.SelecionaCliente;

    if (nCdTerceiro = 0) then
    begin
        freeAndNil( objCaixa_SelCliente ) ;
        MensagemErro('Nenhum cliente foi selecionado.') ;
    end
    else
    begin

        cNmTerceiro := objCaixa_SelCliente.qryTerceirocNmTerceiro.Value ;

        freeAndNil( objCaixa_SelCliente ) ;
        
        objCaixa_ResgateCheque.nCdTerceiro := nCdTerceiro ;
        objCaixa_ResgateCheque.Edit1.Text  := cNmTerceiro ;
        objCaixa_ResgateCheque.ShowModal;

        qryImportaChequeConsulta.Close ;
        qryImportaChequeConsulta.ExecSQL ;

        qryTempChequeResgate.Close ;
        qryTempChequeResgate.Open  ;

        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('SELECT IsNull(Sum(nValTotal),0) FROM #Temp_ChequeResgate') ;
        qryAux.Open ;

        nValorPGInicial  := 0 ;
        nValorDCInicial  := 0 ;
        nValorDMInicial  := 0 ;
        nValorJMInicial  := 0 ;
        nValorPGFinal    := 0 ;

        if not qryAux.Eof then
        begin

            edtChequeResg.Value := qryAux.FieldList[0].Value ;

            AcumulaValorInicial(qryAux.FieldList[0].Value
                               ,0
                               ,0
                               ,0
                               ,qryAux.FieldList[0].Value) ;

        end ;

        CalculaSaldo() ;

    end ;


    if (edtChequeResg.Value > 0) then
    begin

        Keybd_Event(Vk_Right, 0, 0, 0);
        case MessageDlg('Deseja selecionar mais cheques ?',mtConfirmation,[mbYes,mbNo],0) of
            mrNo: begin
                cxPageControl1.ActivePage := tabPagamento ;
                AtivaValores ;
                edtValor.SetFocus ;
            end ;
            MrYes: Goto selecionaCheques;
        end ;

    end ;

end;

procedure TfrmCaixa_Recebimento.btResgateChequeClick(Sender: TObject);
begin
    if (dDtLimiteFech < Date) then
    begin
        MensagemErro('Este caixa requer fechamento imediato.');
        Abort;
    end
    else
    begin
        VerificaSaldoCaixa(False);
    end;

    nValMercadoria       := 0 ;
    nCdDoctoFiscal       := 0 ;
    bItemPromocao        := False ;
    tabPedidos.Enabled   := False ;
    tabParcelas.Enabled  := False ;
    tabCheques.Enabled   := True  ;
    tabPagamento.Enabled := True  ;

    GroupBox4.Enabled       := True ;
    btFinalizar.Enabled     := True ;
    btCancelar.Enabled      := True ;
    btNovoPedido.Enabled    := False ;
    btNovaParcela.Enabled   := False ;
    btFuncoes.Enabled       := False ;
    imgCaixaLivre.Visible   := False ;
    btResgateCheque.Enabled := False ;

    cFlgLiqCrediario := 1 ;
    nValorPGInicial  := 0 ;
    nValorDCInicial  := 0 ;
    nValorDMInicial  := 0 ;
    nValorJMInicial  := 0 ;
    nValorPGFinal    := 0 ;

    DesativaValores;

    cxPageControl1.ActivePage := tabCheques ;

    btConsultaCheque.Click;

end;

procedure TfrmCaixa_Recebimento.btExcChequesClick(Sender: TObject);
begin

    case MessageDlg('Limpar a lista de cheques ?',mtConfirmation,[mbYes,mbNo],0) of
        mrYes: begin

            frmMenu.ZeraTemp('#Temp_Cheque_Consulta');
            frmMenu.ZeraTemp('#Temp_ChequeResgate') ;

            qryTempChequeResgate.Close ;
            qryTempChequeResgate.Open  ;

            edtChequeResg.Value := 0 ;

            CalculaSaldo() ;
            
            cFlgLiqCrediario := 1 ;
            nValorPGInicial  := 0 ;
            nValorDCInicial  := 0 ;
            nValorDMInicial  := 0 ;
            nValorJMInicial  := 0 ;
            nValorPGFinal    := 0 ;

            DesativaValores;
        end ;
    end ;


end;

procedure TfrmCaixa_Recebimento.AbreCupomFiscal;
label
    SolicitaCPFCNPJ ;
var
    cEstadoECF             : string ;
    objCaixa_CPFNotaFiscal : TfrmCaixa_CPFNotaFiscal;
    cFormaPagtoECF         : string ;
    iDiasIBPT              : Integer;
begin
    if (not cFlgUsaECF) then
        Exit;

    if (cFlgUsaCFe) then
        Exit;

    lblMensagem.Caption := 'Abrindo cupom fiscal...' ;

    cEstadoECF := EstadoECF() ;

    If (cEstadoECF = '') then
    begin
        if ( Application.MessageBox( 'A impressora n�o responde!' + #13 +
                                     'Deseja tentar novamente?', 'Aten��o',
                                     MB_IconInformation + MB_YESNO ) = IDYES ) then
        begin
            AbreCupomFiscal() ;
        end
        else
            abort ;
    end ;

    if (cEstadoECF = 'Venda') or (cEstadoECF = 'Pagamento') then //ja tem um cupom fiscal aberto
    begin
        lblMensagem.Caption := '' ;
        MensagemAlerta('N�o � poss�vel abrir um novo cupom fiscal enquanto outro estiver aberto.') ;
        CancelaRecebimento;
        abort ;
    end ;

    if (cEstadoECF <> 'Livre') then
    begin
        lblMensagem.Caption := '' ;
        MensagemAlerta('O Estado da impressora ECF n�o permite a abertura de um novo cupom fiscal.' +#13#13+ 'Estado: ' + cEstadoECF) ;
        CancelaRecebimento;
        abort ;
    end ;

    if (bCupomAberto) then
        exit ;

    { -- verifica se forma de pagto esta configurada na ECF, caso n�o exista programa forma de pagto antes da abertura do cupom -- }
    try
        qryTemp_Pagtos.Close;
        qryTemp_Pagtos.Open;
        qryTemp_Pagtos.First;

        while (not qryTemp_Pagtos.Eof) do
        begin
            if (frmMenu.ACBrECF1.AchaFPGDescricao(qryTemp_PagtoscNmFormaPagto.Value) = Nil) then
            begin
                Sleep(300);

                lblMensagem.Caption := 'Gravando forma de pagamento na ECF...';
                cFormaPagtoECF      := qryTemp_PagtoscNmFormaPagto.Value;

                frmMenu.ACBrECF1.ProgramaFormaPagamento(cFormaPagtoECF,TRUE,'');
            end;

            qryTemp_Pagtos.Next;
        end;

        qryTemp_Pagtos.First;
    except
        MensagemErro('Erro no processamento');
        Raise;
        Abort;
    end;

    lblMensagem.Caption := '';

    if (frmMenu.ACBrECF1.PoucoPapel) then
        MensagemAlerta('Impressora ECF com pouco papel!') ;

    iConta     := 0 ;
    iTransacao := 0 ;

    if (nCdTerceiro = 0) then
        nCdTerceiro := nCdTerceiroPDV ;


    objCaixa_CPFNotaFiscal := TfrmCaixa_CPFNotaFiscal.Create( Self ) ;

    SolicitaCPFCNPJ:

    objCaixa_CPFNotaFiscal.ShowModal;

    cCGCPF := objCaixa_CPFNotaFiscal.edtCPF.Text;

    if (cCGCPF <> '') then
    begin
        if (frmMenu.TestaCpfCgc(cCGCPF) = '') then
        begin
            Goto SolicitaCPFCNPJ ;
        end ;
    end ;

    freeAndNil( objCaixa_CPFNotaFiscal ) ;

    lblMensagem.Caption  := 'Gerando cupom fiscal...' ;

    qryTabIBPT.Close;
    qryTabIBPT.Open;

    iDiasIBPT := StrToIntDef(frmMenu.LeParametro('DIASVENCTABIBPT'),0);

    if (not qryTabIBPT.IsEmpty) then
    begin
        if ((qryTabIBPTiDiasValidade.Value <= iDiasIBPT) and (iDiasIBPT > 0)) then
            MensagemAlerta('Faltam ' + qryTabIBPTiDiasValidade.AsString + ' dias para o vencimento das al�quotas para atendimento da lei 12.741 da Transpar�ncia Fiscal.'
                          +#13#13
                          +'Caso n�o seja atualizado dentro do prazo, o c�lculo deixar� de aparecer no cupom fiscal.');
    end;

    // grava o cupom fiscal na base de dados
    frmMenu.Connection.BeginTrans ;

    try

        SP_GERA_CUPOM_FISCAL.Close ;
        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva  ;
        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdLoja').Value     := frmMenu.nCdLojaAtiva     ;
        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdUsuario').Value  := frmMenu.nCdUsuarioLogado ;
        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdTerceiro').Value := nCdTerceiro              ;
        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@cCPF').Value        := cCGCPF                   ;
        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@cCdNumSerie').Value := frmMenu.ACBrECF1.NumSerie;
        SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@cFlgCFe').Value     := 0; //cupom fiscal ECF
        SP_GERA_CUPOM_FISCAL.Open;

        nCdDoctoFiscal := SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdDoctoFiscal').Value ;

    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento') ;
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    if (trim(cCGCPF) <> '') then
        frmMenu.ACBrECF1.AbreCupom(Trim(cCGCPF), 'CONSUMIDOR' , '' )
    else frmMenu.ACBrECF1.AbreCupom('', '' , '' );

    nCdCupomFiscal := StrToInt(frmMenu.ACBrECF1.NumCupom) ;

    // Imprime os produtos do cupom
    ImprimeItensCupomFiscal() ;

    // grava o n�mero do cupom na base de dados
    frmMenu.Connection.BeginTrans ;

    try

        SP_GRAVA_NUMERO_CUPOM_FISCAL.Close ;
        SP_GRAVA_NUMERO_CUPOM_FISCAL.Parameters.ParamByName('@nCdDoctoFiscal').Value  := nCdDoctoFiscal ;
        SP_GRAVA_NUMERO_CUPOM_FISCAL.Parameters.ParamByName('@iNrDocto').Value        := nCdCupomFiscal ;
        SP_GRAVA_NUMERO_CUPOM_FISCAL.ExecProc ;

    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento') ;
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    bCupomAberto := True ;
    lblMensagem.Caption := '' ;

    edtNumPedido.Enabled     := false ;

end;

procedure TfrmCaixa_Recebimento.ImprimeItensCupomFiscal;
var
  nValImpAproxFed : Double;
  nValImpAproxEst : Double;
  nValImpAproxMun : Double;
  cChaveIBPT      : String;
begin
    if (not cFlgUsaECF) then
        exit ;

    if (cFlgUsaCFe) then
        Exit;

    if (EstadoECF() <> 'Venda') then
    begin
        MensagemAlerta('Status do cupom fiscal n�o permite incluir itens.') ;
        abort ;
    end ;

    lblMensagem.Caption := 'Imprimindo produtos...' ;

    SP_GERA_CUPOM_FISCAL.First ;

    while not SP_GERA_CUPOM_FISCAL.Eof do
    begin

        frmMenu.ACBrECF1.VendeItem( SP_GERA_CUPOM_FISCALnCdProduto.AsString
                                   ,SP_GERA_CUPOM_FISCALcNmItem.Value
                                   ,SP_GERA_CUPOM_FISCALcAliqICMS.Value
                                   ,SP_GERA_CUPOM_FISCALnQtde.Value
                                   ,SP_GERA_CUPOM_FISCALnValUnitario.Value
                                   ,0
                                   ,SP_GERA_CUPOM_FISCALcUnidadeMedida.Value
                                   ,'%' ) ;

        SP_GERA_CUPOM_FISCAL.Next;

    end ;

    { -- atribui valor do imposto aproximado calculado pela procedure para ser impresso no rodap� do cupom fiscal -- }
    nValImpAproxFed := SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nValTotalImpostoAproxFed').Value;
    nValImpAproxEst := SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nValTotalImpostoAproxEst').Value;
    nValImpAproxMun := SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nValTotalImpostoAproxMun').Value;

    if ((nValImpAproxFed > 0) or (nValImpAproxEst > 0) or (nValImpAproxMun > 0)) then
    begin
        qryTabIBPT.Close;
        qryTabIBPT.Open;

        if (not qryTabIBPT.IsEmpty) then
            cChaveIBPT := qryTabIBPTcChave.Value;

        frmMenu.ACBrECF1.InfoRodapeCupom.Imposto.ValorAproximadoFederal   := nValImpAproxFed;
        frmMenu.ACBrECF1.InfoRodapeCupom.Imposto.ValorAproximadoEstadual  := nValImpAproxEst;
        frmMenu.ACBrECF1.InfoRodapeCupom.Imposto.ValorAproximadoMunicipal := nValImpAproxMun;
        frmMenu.ACBrECF1.InfoRodapeCupom.Imposto.Fonte                    := 'IBPT/FECOMERCIO (' + cChaveIBPT + ')';
        frmMenu.ACBrECF1.InfoRodapeCupom.Imposto.ModoCompacto             := False;
    end;

    SP_GERA_CUPOM_FISCAL.Close ;

    lblMensagem.Caption := '' ;

end;

procedure TfrmCaixa_Recebimento.FinalizaCupomFiscal;
begin

    if (not cFlgUsaECF) then
        exit ;

    if (cFlgUsaCFe) then
        Exit;

    Sleep(300) ;

    lblMensagem.Caption := 'Finalizando Cupom Fiscal...' ;

    frmMenu.ACBrECF1.FechaCupom('');

    try
        frmMenu.ACBrECF1.CortaPapel(TRUE);
    except
    end ;

end;

function TfrmCaixa_Recebimento.EstadoECF: string;
var
    cOldMsg : string ;
begin

  cOldMsg := lblMensagem.Caption ;

  lblMensagem.Caption := 'Checando Estado ECF...' ;

  Application.ProcessMessages;

  if (not cFlgUsaECF) then
  begin
      Result := 'Livre' ;
      lblMensagem.Caption := cOldMsg ;
      exit ;
  end ;

  try
     Result :=  Estados[ frmMenu.ACBrECF1.Estado ] ;
  except
     Result := '' ;
  end ;

  lblMensagem.Caption := cOldMsg ;

  if (Result = 'N�o Inicializada') then
      Exception.Create('Impressora Fiscal Nao Inicializada.') ;

  if (Result = 'Desconhecido') then
      Exception.Create('Impressora Fiscal Nao Inicializada. Status Desconhecido.') ;

  if (Result = 'Bloqueada') then
      Exception.Create('Impressora Fiscal est� Bloqueada.') ;

  if (Result = 'Requer Z') then
      Exception.Create('Impressora Fiscal Requer Redu��o Z.') ;

  if (Result = 'Requer X') then
      Exception.Create('Impressora Fiscal Requer Leitura X.') ;

  if (Result = 'Nao Fiscal') then
      Exception.Create('Impressora encontrada n�o � uma impressora fiscal.') ;
{
  estNaoInicializada  Porta Serial ainda nao foi aberta
  estDesconhecido     Porta aberta, mas estado ainda nao definido
  estLivre            Impressora Livre, sem nenhum cupom aberto,
                      pronta para nova venda, Reducao Z e Leitura X ok,
                      pode ou nao j� ter ocorrido 1� venda no dia...
  estVenda            Cupom de Venda Aberto com ou sem venda do 1� Item
  estPagamento        Iniciado Fechamento de Cupom com Formas Pagto
                      pode ou n�o ter efetuado o 1� pagto. Nao pode
                      mais vender itens, ou alterar Subtotal
  estRelatorio        Imprimindo Cupom Fiscal Vinculado ou Relatorio Gerencial
  estBloqueada        Redu�ao Z j� emitida, bloqueada at� as 00:00
  estRequerZ          Reducao Z dia anterior nao emitida. Emita agora
  estRequerX          Esta impressora requer Leitura X todo inicio de
                          dia. Imprima uma Leitura X para poder vender
  estNaoFiscal        Comprovante Nao Fiscal Aberto
}
end;

procedure TfrmCaixa_Recebimento.CancelaCupomFiscal;
var
    cEstadoECF : string ;
begin

    if (not cFlgUsaECF) then
        exit ;

    cEstadoECF := EstadoECF() ;

    if ((EstadoECF = 'Venda') or (EstadoECF = 'Pagamento')) then
    begin
        lblMensagem.Caption := 'Cancelando cupom fiscal...' ;
        frmMenu.ACBrECF1.CancelaCupom() ;
        lblMensagem.Caption := '' ;
    end ;


end;

procedure TfrmCaixa_Recebimento.EnviaPagamentoECF(cFormaPagto: string; nValorPagto: double; cOBS: string; bVinculado: boolean);
begin

    if (not cFlgUsaECF) then
        exit ;

    if (EstadoECF() <> 'Pagamento') then
    begin
        Exception.Create('O Estado da Impressora ECF n�o � de Pagamento. Estado : ' + EstadoECF());
    end ;

    { -- bloco removido pois programa��o da forma de pagto pode ser efetuada deve ser efetuado antes da abertura do cupom -- }
    { -- processo incluso no momento da abertura do cupom -- }
    {lblMensagem.Caption := 'Enviando Pagamento ECF...' ;

    if (frmMenu.ACBrECF1.AchaFPGDescricao(cFormaPagto) = nil) then
    begin
        lblMensagem.Caption := 'Gravando forma de pagamento na ECF...' ;
        frmMenu.ACBrECF1.ProgramaFormaPagamento(cFormaPagto,TRUE,'') ;
    end ;}

    lblMensagem.Caption := 'Enviando Pagamento ECF...' ;

    frmMenu.ACBrECF1.EfetuaPagamento(frmMenu.ACBrECF1.AchaFPGDescricao(cFormaPagto).Indice, nValorPagto, cOBS, bVinculado) ;

    lblMensagem.Caption := '' ;

end;

procedure TfrmCaixa_Recebimento.LeituraX;
var
    cEstadoECF : string ;
begin

  cEstadoECF :=  Estados[ frmMenu.ACBrECF1.Estado ] ;

  if (cEstadoECF = 'N�o Inicializada') then
  begin
      MensagemAlerta('Impressora Fiscal Nao Inicializada.') ;
      exit ;
  end ;

  if (cEstadoECF = 'Bloqueada') then
  begin
      MensagemAlerta('Impressora Fiscal est� Bloqueada.') ;
      exit ;
  end ;

  if (cEstadoECF = 'Desconhecido') then
  begin
      MensagemAlerta('Impressora Fiscal Nao Inicializada. Status Desconhecido.') ;
      exit ;
  end ;

  if (cEstadoECF = 'Nao Fiscal') then
  begin
      MensagemAlerta('Impressora encontrada n�o � uma impressora fiscal.') ;
      exit ;
  end ;

  if (cEstadoECF <> 'Livre') then
  begin
      MensagemAlerta('Finalize o cupom fiscal em aberto antes de solicitar a Leitura X') ;
      exit ;
  end ;

  frmMenu.MensagemPadrao('AGUARDE A IMPRESS�O...',0,5000) ;

  frmMenu.ACBrECF1.LeituraX;
end;

procedure TfrmCaixa_Recebimento.ReducaoZ;
var
    cEstadoECF : string ;
begin

  cEstadoECF :=  Estados[ frmMenu.ACBrECF1.Estado ] ;

  if (cEstadoECF = 'N�o Inicializada') then
  begin
      MensagemAlerta('Impressora Fiscal Nao Inicializada.') ;
      exit ;
  end ;

  if (cEstadoECF = 'Bloqueada') then
  begin
      MensagemAlerta('Impressora Fiscal est� Bloqueada.' +#13#13 + '�ltima Redu��o Z em: ' + frmMenu.ACBrECF1.DadosUltimaReducaoZ) ;
      exit ;
  end ;

  if (cEstadoECF = 'Desconhecido') then
  begin
      MensagemAlerta('Impressora Fiscal Nao Inicializada. Status Desconhecido.') ;
      exit ;
  end ;

  if (cEstadoECF = 'Nao Fiscal') then
  begin
      MensagemAlerta('Impressora encontrada n�o � uma impressora fiscal.') ;
      exit ;
  end ;

  if (cEstadoECF = 'Venda') or (cEstadoECF = 'Pagamento') then
  begin
      MensagemAlerta('Finalize o cupom fiscal em aberto antes de solicitar a Redu��o Z') ;
      exit ;
  end ;

  if (MessageDlg('Este processo ir� bloquear a impressora ECF e s� permitir� movimentos amanh�.' +#13#13 + 'Confirma ?',mtConfirmation,[mbYes,mbNo],0) = MRNO) then
      exit ;

  frmMenu.MensagemPadrao('AGUARDE A IMPRESS�O...',0,5000) ;

  frmMenu.ACBrECF1.CorrigeEstadoErro(True);
  frmMenu.ACBrECF1.ReducaoZ(0);

  ShowMessage('Redu��o Z realizada com sucesso.' + #13#13 + 'ECF Bloqueada at� as 00:00hrs.') ;

end;

procedure TfrmCaixa_Recebimento.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
    cEstadoECF : string ;
    cArquivo   : TextFile ;
    cLinha     : string ;

begin

    UnHookWindowsHookEx(KBHook) ;

    cEstadoECF := EstadoECF() ;

    lblMensagem.Caption := '' ;

    if (cFlgUsaECF) then
    begin
        // se a impressora n�o responder encerra por aqui.
        if (cEstadoECF = '') then
        begin
            MensagemAlerta('N�o foi poss�vel obter uma resposta do status da impressora ECF.') ;
            lblMensagem.Caption := '' ;
            exit ;
        end ;

        if (cEstadoECF = 'Venda') OR (cEstadoECF = 'Pagamento') then
        begin
            if (MessageDlg('Existe um cupom fiscal em processo de venda na impressora ECF.' +#13#13 +'Se voc� fechar a tela do caixa, o cupom fiscal ser� cancelado. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = MRYES) then
            begin

                try
                    frmMenu.ACBrECF1.CancelaCupom ;

                    {-- cancela o documento fiscal --}
                    SP_CANCELA_CUPOM_FISCAL.Close ;
                    SP_CANCELA_CUPOM_FISCAL.Parameters.ParamByName('@nCdDoctoFiscal').Value := nCdDoctoFiscal;
                    SP_CANCELA_CUPOM_FISCAL.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
                    SP_CANCELA_CUPOM_FISCAL.ExecProc;

                except
                    MensagemErro('Erro no processamento.') ;
                    raise ;
                end ;

            end
            else abort ;
        end ;

        if (cEstadoECF = 'Relatorio') then
            frmMenu.ACBrECF1.FechaRelatorio;

    end ;

end;

procedure TfrmCaixa_Recebimento.tTimerLabelTimer(Sender: TObject);
begin

    if (lblMensagem.Visible) then
        lblMensagem.Visible := False
    else lblMensagem.Visible := True ;

end;

procedure TfrmCaixa_Recebimento.EnviaPagamentosECF;
var
    nValDevoluc, nValVale : double ;
begin

    {-- Se tiver troca, envia primeiro --}
    nValDevoluc := 0;
    nValVale    := 0;

    if qryTemp_Pedido.Active then
    begin
        qryTemp_Pedido.First ;

        while not qryTemp_Pedido.eof do
        begin
            nValDevoluc := nValDevoluc + qryTemp_PedidonValDevoluc.Value  ;
            nValVale    := nValVale    + qryTemp_PedidonValValeMerc.Value ;
            qryTemp_Pedido.Next;
        end ;

        qryTemp_Pedido.First;

        Sleep(300) ;

        if nValDevoluc > 0 then
            EnviaPagamentoECF('CREDITO TROCA', nValDevoluc , '', false) ;

        Sleep(300) ;

        if nValVale > 0 then
            EnviaPagamentoECF('VALE', nValVale , '', false) ;
    end ;

    qryTemp_Pagtos.Close ;
    qryTemp_Pagtos.Open ;

    qryTemp_Pagtos.First ;

    while not qryTemp_Pagtos.Eof do
    begin

        // imprime o valor pago no cupom fiscal, juntamente com a forma de pagamento
        // se for uma transa��o TEF, confirma a transa��o
        Sleep(300) ;
        EnviaPagamentoECF(qryTemp_PagtoscNmFormaPagto.Value, qryTemp_PagtosnValPagto.Value , '', (qryTemp_PagtoscFlgTEF.Value = 1)) ;

        qryTemp_Pagtos.Next ;

    end ;

    qryTemp_Pagtos.First ;

end;

procedure TfrmCaixa_Recebimento.CancelaRecebimento;
begin

    try
        cmdZeraTemp.Execute;
    except
    end ;

    frmMenu.ZeraTemp('#Temp_Pedido');

    qryCondPagto.Close ;
    qryTerceiro.Close ;
    nCdTerceiro := 0 ;

    qryTemp_Pedido.Close ;

    // zera os saldos da tela
    edtPedidos.Value  := 0 ;
    edtParcelas.Value := 0 ;
    edtEntrada.Value  := 0 ;
    edtTroco.Value    := 0 ;
    edtVale.Value     := 0 ;

    CalculaSaldo() ;

    // limpa os dados dos cheques
    objCaixa_Cheques.qryPrepara_Temp_Cheque.Close ;

    // limpa os dados do credi�rio
    objCaixa_ParcelaCred.qryTemp_Crediario.Close ;

    // limpa as parcelas recebidas
    qryParcelas.Close ;

    btNovoPedido.Enabled    := True ;
    btNovaParcela.Enabled   := True ;
    btResgateCheque.Enabled := True ;

    btNovoPedido.SetFocus ;

    btFinalizar.Enabled := False ;
    btCancelar.Enabled  := False ;

    GroupBox1.Enabled := False ;
    GroupBox4.Enabled := False ;

    btIncParcela.Enabled      := False ;
    btExcluirParcelas.Enabled := False ;
    btFuncoes.Enabled         := True ;

    cFlgLiqCrediario  := 0 ;

    iQtdeClientes     := 0 ;
    nCdTerceiroPadrao := 0 ;

    bCupomAberto      := False ;

end;

function KeyBoardHook(code: integer; wParam: word; lParam: longword): longword; stdcall;
begin

  if code<0 then
  begin  //if code is <0 your keyboard hook should always run CallNextHookEx instantly and
    KeyBoardHook := CallNextHookEx(KBHook,code,wParam,lparam); //then return the value from it.
    Exit;
  end;

  if (Pos(Screen.ActiveForm.Name,'frmCaixa_Recebimento') > 0) then
  begin

      {case wParam of
          VK_F2 : begin
              frmCaixa_Recebimento.btNovoPedido.Click;
              exit ;
          end ;

          VK_F3 : begin
              TfrmCaixa_Recebimento.btNovaParcela.Click;
              exit ;
          end ;

          VK_F4 : begin
              frmCaixa_Recebimento.btResgateCheque.Click;
              exit ;
          end ;

          VK_F7 : begin
              frmCaixa_Recebimento.btFinalizar.Click;
              exit ;
          end ;

          VK_F8 : begin
              frmCaixa_Recebimento.btCancelar.Click;
              exit ;
          end ;

          VK_F9 : begin
              frmCaixa_Recebimento.btSair.Click;
              exit ;
          end ;

      end ; }
      
  end ;

  CallNextHookEx(KBHook,code,wParam,lparam);  //call the next hook proc if there is one

  KeyBoardHook:=0; //if KeyBoardHook returns a non-zero value, the window that should get
                     //the keyboard message doesnt get it.
  Exit;

end;


procedure TfrmCaixa_Recebimento.AcumulaValorInicial(nValor, nDC, nDM, nJM,
  nValorPagar: double);
begin

  nValorPGInicial := nValorPGInicial + nValor      ;
  nValorDCInicial := nValorDCInicial + nDC         ;
  nValorDMInicial := nValorDMInicial + nDM         ;
  nValorJMInicial := nValorJMInicial + nJM         ;
  nValorPGFinal   := nValorPGFinal   + nValorPagar ;

  edtValor.Value    := nValorPGInicial ;
  edtDC.Value       := nValorDCInicial ;
  edtDM.Value       := nValorDMInicial ;
  edtJM.Value       := nValorJMInicial ;
  edtVlrPagto.Value := nValorPGFinal ;


end;

procedure TfrmCaixa_Recebimento.edtDMExit(Sender: TObject);
begin

    edtDM.Value       := abs(edtDM.Value) ;

    if (edtDM.Value > 0) and (qryCondPagto.Active) and (qryCondPagtocFlgPermDesconto.Value = 0) then
    begin
        MensagemErro('A condi��o ' + qryCondPagtocNmCondPagto.Value + ' n�o permite desconto manual.') ;
        edtDM.Value := 0 ;
        edtDM.SetFocus;
        exit ;
    end ;

    if (edtDM.Value > edtVlrPagto.Value) then
    begin
        MensagemErro('O valor do desconto n�o pode ser maior que o valor a pagar.') ;
        edtDM.Value := 0 ;
        edtDM.SetFocus ;
        exit ;
    end ;

    edtVlrPagto.Value := edtValor.Value + edtDC.Value - edtDM.Value + edtJM.Value ;

end;

procedure TfrmCaixa_Recebimento.SelecionaCondicaoPagamento;
var
    nCdFormaPagto, nCdCondPagto : integer ;
begin

    cFlgLiqCrediario := 0 ;

    if (qryParcelas.Active) and (qryParcelas.RecordCount > 0) then
        cFlgLiqCrediario := 1 ;

    if (qryTempChequeResgate.Active) and (qryTempChequeResgate.RecordCount > 0) then
        cFlgLiqCrediario := 1 ;

    objPdvFormaPagto.cFlgLiqCrediario := cFlgLiqCrediario ;
    nCdFormaPagto                     := objPdvFormaPagto.SelecionaFormaPagto() ;

    if (nCdFormaPagto <= 0) then
        abort ;

    objPdvCondPagto.nCdLoja               := frmMenu.nCdLojaAtiva;
    objPdvCondPagto.nCdFormaPagto         := nCdFormaPagto       ;
    objPdvCondPagto.cFlgLiqCrediario      := cFlgLiqCrediario    ;
    objPdvCondPagto.bItemPromocao         := bItemPromocao       ;

    objPdvCondPagto.edtValorPagar.Value   := frmMenu.TBRound(edtValor.Value,2) ;
    objPdvCondPagto.edtSaldo.Value        := frmMenu.TBRound(edtValor.Value,2) ;
    objPdvCondPagto.cPermDesconto         := 'S' ;
    objPdvCondPagto.edtValorPagar.Enabled := True ;

    {-- se o valor que est� sendo pago for menor que o valor total a pagar, verifica se permite desconto para multiplos pagamentos --}
    if (edtValor.Value < edtTotalPagar.Value) then
        objPdvCondPagto.cPermDesconto := cPermDescontoMP ;

    {-- Processa a consulta --}
    nCdCondPagto  := objPdvCondPagto.SelecionaCondPagto() ;

    if (nCdCondPagto <= 0) then
        abort ;

    // se for a primeira condi��o de pagamento, considera a padr�o da venda
    qryCondPagto.Close ;
    qryCondPagto.Parameters.ParamByName('nCdCondPagto').Value := nCdCondPagto ;
    qryCondPagto.Parameters.ParamByName('nCdLoja').Value      := frmMenu.nCdLojaAtiva;
    qryCondPagto.Open ;

    {-- Atualiza os totais --}
    {-- o desconto da condi��o � somando no total porque quando � desconto vem negativo, e quando � juros vem positivo --}
    edtValor.Value    := objPdvCondPagto.edtValorPagar.Value ;
    edtDC.Value       := 0 ;
    edtDM.Value       := 0 ;

    if (cFlgLiqCrediario = 0) then
    begin
        edtDC.Value       := (objPdvCondPagto.qryCondPagtonValorTotal.Value - edtValor.Value) ;
        edtDM.Value       := 0 ;
        edtJM.Value       := 0 ;
        edtVlrPagto.Value := edtValor.Value + edtDC.Value - edtDM.Value + edtJM.Value ;
    end
    else edtVlrPagto.Value := edtValor.Value ;

    {-- se nenhum cliente foi selecionado e a forma de pagamento exige, obriga selecionar um agora --}
    if (cFlgLiqCrediario = 0) then
        if (qryCondPagtocFlgExigeCliente.Value = 1) and (qryTerceiro.eof or (qryTerceironCdTerceiro.Value = nCdTerceiroPDV)) then
            SelecionaCliente;

    edtValor.SetFocus;

end;

procedure TfrmCaixa_Recebimento.CalculaOscilacaoCondicao;
begin
    edtDC.Value := 0;

    if (not qryCondPagto.eof) and (qryCondPagtonPercAcrescimo.Value <> 0) then
        edtDC.Value := frmMenu.TBRound((edtValor.Value * (qryCondPagtonPercAcrescimo.Value / 100)),2) ;

end;

procedure TfrmCaixa_Recebimento.SelecionaCliente;
var
    objForm : TfrmCaixa_SelCliente;
begin
    objForm := TfrmCaixa_SelCliente.Create(Self);

    nCdTerceiro := objForm.SelecionaCliente();

    FreeAndNil(objForm);

    if (nCdTerceiro = 0) then
        exit ;

    qryTerceiro.Close;
    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro;
    qryTerceiro.Open;

    { -- remove justificativa de venda sem cliente -- }
    qryAux.SQL.Clear;
    qryAux.SQL.Add('UPDATE Pedido                           ');
    qryAux.SQL.Add('   SET cOBS            = NULL           ');
    qryAux.SQL.Add('      ,nCdUsuarioAutor = NULL           ');
    qryAux.SQL.Add('WHERE nCdPedido IN (SELECT nCdPedido    ');
    qryAux.SQL.Add('                      FROM #Temp_Pedido)');
    qryAux.ExecSQL;

end;

procedure TfrmCaixa_Recebimento.btTrocaClienteClick(Sender: TObject);
begin
    SelecionaCliente;

    if (edtValor.enabled) then
        edtValor.SetFocus
    else btIncPagamento.SetFocus ;

end;

procedure TfrmCaixa_Recebimento.edtValorPropertiesEditValueChanged(
  Sender: TObject);
begin

    edtDC.Value       := 0 ;
    edtDM.Value       := 0 ;
    edtJM.Value       := 0 ;
    edtVlrPagto.Value := 0 ;

end;

procedure TfrmCaixa_Recebimento.edtValorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

    if (Key = vk_return) then
        if (edtDM.Enabled) then
            edtDM.SetFocus
        else btIncPagamento.SetFocus;

end;

procedure TfrmCaixa_Recebimento.edtDCKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if (Key = vk_return) then
        if (edtDM.Enabled) then
            edtDM.SetFocus
        else btIncPagamento.SetFocus;

end;

procedure TfrmCaixa_Recebimento.edtDMKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

    if (Key = vk_return) then
        edtJM.SetFocus ;

end;

procedure TfrmCaixa_Recebimento.edtVlrPagtoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

    if (Key = vk_return) then
        btIncPagamento.SetFocus ;

end;

procedure TfrmCaixa_Recebimento.DesativaValores;
begin
    edtValor.Enabled := false ;
    edtDM.Enabled    := false ;
    edtJM.Enabled    := false ;
end;

procedure TfrmCaixa_Recebimento.AtivaValores;
begin
    edtValor.Enabled := true ;
    edtDM.Enabled    := (cFlgLiqCrediario = 0) ;
    edtJM.Enabled    := (cFlgLiqCrediario = 0) ;
end;

procedure TfrmCaixa_Recebimento.edtEntradaPropertiesChange(
  Sender: TObject);
begin

    btPgEntrada.Enabled := (edtEntrada.Value > 0) ;

end;

procedure TfrmCaixa_Recebimento.SuprimentoCaixa;
begin

    if (nCdResumoCaixa = 0) then
    begin
        MensagemErro('O Caixa n�o est� aberto.') ;
        abort ;
    end ;

end;

procedure TfrmCaixa_Recebimento.CaixaLivre;
begin

    imgCaixaLivre.Visible := True ;
    tabPedidos.Enabled    := False ;
    tabParcelas.Enabled   := False ;
    tabCheques.Enabled    := False ;
    tabPagamento.Enabled  := False ;
    btFuncoes.Enabled     := True ;
    lblMensagem.Caption   := '' ;

    cxPageControl1.ActivePage := tabPedidos ;

    imgCaixaLivre.BringToFront;

end;

procedure TfrmCaixa_Recebimento.edtDMEnter(Sender: TObject);
begin

    edtDM.SelectAll;

end;

procedure TfrmCaixa_Recebimento.TrataTeclaFuncao(Key: Word);
begin

    case Key of
        VK_F2: if (btNovoPedido.Enabled) then btNovoPedido.Click ;
        VK_F3: if (btNovaParcela.Enabled) then btNovaParcela.Click ;
        VK_F4: if (btResgateCheque.Enabled) then btResgateCheque.Click ;
        VK_F7: if (btFinalizar.Enabled) then btFinalizar.Click ;
        VK_F8: if (btCancelar.Enabled) then btCancelar.Click ;
        VK_F9: btCliente.Click;
        VK_F11: btFuncoes.Click;
        VK_F12: btSair.Click;
    end ;

end;

procedure TfrmCaixa_Recebimento.btNovoPedidoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;
end;

procedure TfrmCaixa_Recebimento.btNovaParcelaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.btResgateChequeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.btFinalizarKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.btCancelarKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.btClienteKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.btFuncoesKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.btSairKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.edtNumCarnetKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

    if (Key = VK_F4) then
    begin
        btIncParcela.Click;
        abort ;
    end ;

    if (Key = VK_F6) then
    begin
        btExcluirParcelas.Click;
        exit ;
    end ;

    if (Key = VK_F7) then
    begin
        btRenegociacao.Click;
        exit ;
    end ;

    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.btTrocaCondicaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.btIncPagamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.btTrocaClienteKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.cxGridDBTableView2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.cxGridDBTableView3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.cxGridDBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

end;

procedure TfrmCaixa_Recebimento.edtValorKeyPress(Sender: TObject;
  var Key: Char);
var
    nValorPago, nValorSaldo : Double ;
begin

    if (Key = #13) then
    begin

        nValorPago  := frmMenu.TBRound(edtValor.Value,2) ;
        nValorSaldo := frmMenu.TBRound(edtSaldo.Value,2) ;

        if (nValorPago > nValorSaldo) then
        begin
            MensagemAlerta('O Valor n�o pode ser maior que o saldo a pagar.') ;
            edtValor.Value := 0 ;
            edtValor.SetFocus;
            exit ;
        end ;

        if (edtValor.Value > 0) and (qryCondPagto.eof) then
        begin
            SelecionaCondicaoPagamento;

            if (qryCondPagto.Eof) then
            begin
                MensagemErro('Nenhuma condi��o de pagamento foi selecionada.') ;
                edtValor.SetFocus ;
                exit ;
            end ;
        end ;

        edtDC.Value := 0 ;

        if (cFlgLiqCrediario = 0) then
            CalculaOscilacaoCondicao;

        edtVlrPagto.Value := edtValor.Value + edtDC.Value - edtDM.Value + edtJM.Value ;

        if (frmMenu.TBRound(edtSaldo.Value,2) > 0) then
            if (edtDM.Enabled) then
                edtDM.SetFocus
            else btIncPagamento.SetFocus
        else btFinalizar.SetFocus;

    end ;

end;

procedure TfrmCaixa_Recebimento.edtNumPedidoKeyPress(Sender: TObject;
  var Key: Char);
var
    nValPedido : double ;
    State      : TKeyboardState ;
label
    Questao1;
begin

    GetKeyboardState(State);

    if (((State[vk_Control] And 128) <> 0)) and (Key = #9) then
    begin
        if (cApuraEstatistica = 'S') then
        begin

            if (cxLabel2.Caption = 'Pedidos') then
            begin
                cxLabel2.Caption := 'Pedidos.';
                cFlgUsaECF       := False;
                cFlgUsaCFe       := False;
            end
            else
            begin
                cxLabel2.Caption := 'Pedidos';
                cFlgUsaECF       := cFlgUsaECFAux;
                cFlgUsaCFe       := cFlgUsaCFeAux;
            end ;

        end ;

    end ;


    if (Key <> #13) then
        exit ;

    nValPedido    := 0 ;

    if (Trim(edtNumPedido.Text) <> '') then
    begin

        qryPedido.Close ;
        qryPedido.Parameters.ParamByName('nCdLoja').Value   := frmMenu.nCdLojaAtiva;
        qryPedido.Parameters.ParamByName('nCdPedido').Value := frmMenu.ConvInteiro(edtNumPedido.Text);
        qryPedido.Open ;

        if (qryPedido.eof) then
        begin
            MensagemAlerta('Pedido n�o encontrado.') ;
            edtNumPedido.Text := '' ;
            edtNumPedido.SetFocus ;
            exit ;
        end ;

        if not (qryCondPagto.eof) and (qryCondPagtonCdCondPagto.Value <> qryPedidonCdCondPagto.Value) then
        begin
            MensagemErro('S� � permitido receber mais de um pedido se todos estiverem com a mesma condi��o de pagamento.'+#13#13+'Condi��o deste pedido : ' + qryPedidocNmCondPagto.Value) ;
            qryPedido.Close ;
            edtNumPedido.Text := '' ;
            edtNumPedido.SetFocus ;
            exit ;
        end ;

        { -- permite apenas pedidos que utilizam terceiro pdv padr�o TERCPDV -- }
        { -- obs: se pedido que exige cliente mas possui terceiro pdv padr�o, automaticamente � atualizado o terceiro no ato da sele��o do cliente -- }
        if ((not qryTerceiro.Eof) and (qryTerceironCdTerceiro.Value <> qryPedidonCdTerceiro.Value) and (nCdTerceiroPDV <> qryPedidonCdTerceiro.Value)) then
        begin
            MensagemErro('S� � permitido receber mais de um pedido se todos estiverem com o mesmo cliente.'+#13#13+'Cliente deste pedido : ' + qryPedidocNmTerceiro.Value + '.');
            qryPedido.Close ;
            edtNumPedido.Text := '' ;
            edtNumPedido.SetFocus ;
            exit ;
        end;

        {-- se for o primeiro pedido digitado, conecta o datasource --}
        if not (qryTemp_Pedido.Active) then
            qryTemp_Pedido.Open ;

        {-- testa para ver se o pedido digitado j� n�o foi digitado e adicionado na tabela tempor�ria --}
        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('SELECT 1 FROM #Temp_Pedido WHERE nCdPedido = ' + edtNumPedido.Text) ;
        qryAux.Open ;

        if qryAux.Eof then
        begin
            {-- inseri o pedido --}
            qryTemp_Pedido.Insert;
            qryTemp_PedidonCdPedido.Value             := qryPedidonCdPedido.Value             ;
            qryTemp_PedidonValPedido.Value            := qryPedidonValPedido.Value            ;
            qryTemp_PedidonCdTerceiro.Value           := qryPedidonCdTerceiro.Value           ;
            qryTemp_PedidonCdCondPagto.Value          := qryPedidonCdCondPagto.Value          ;
            qryTemp_PedidonValProdutos.Value          := qryPedidonValProdutos.Value          ;
            qryTemp_PedidonValDescontoCondPagto.Value := qryPedidonValDescontoCondPagto.Value ;
            qryTemp_PedidonValJurosCondPagto.Value    := qryPedidonValJurosCondPagto.Value    ;
            qryTemp_PedidonValDesconto.Value          := qryPedidonValDesconto.Value          ;
            qryTemp_pedidonValValeMerc.Value          := qryPedidonValValeMerc.Value          ;
            qryTemp_pedidocNmCondPagto.Value          := qryPedidocNmCondPagto.Value          ;
            qryTemp_pedidonValValePres.Value          := qryPedidonValValePres.Value          ;
            qryTemp_PedidocFlgPromocional.Value       := qryPedidocFlgPromocional.Value       ;
            qryTemp_PedidonValDevoluc.Value           := qryPedidonValDevoluc.Value           ;
            qryTemp_PedidonValMercadoria.Value        := qryPedidonValMercadoria.Value        ;
            qryTemp_pedidonValAcrescimo.Value         := qryPedidonValAcrescimo.Value         ;
            qryTemp_Pedido.Post;

            bItemPromocao := (qryPedidocFlgPromocional.Value = 1) ;

            nTotalValePresente := nTotalValePresente + qryPedidonValValePres.Value ;
            nValMercadoria     := nValMercadoria     + qryPedidonValMercadoria.Value ;

            qryAux.Close ;

            {-- repagina o datasource --}
            qryTemp_Pedido.Requery();

            {-- se for o primeiro pedido, posiciona o datasource da condi��o de pagamento e do cliente --}
            if (qryPedidonValPedido.Value > 0) then
            begin

                qryCondPagto.Close ;
                qryCondPagto.Parameters.ParamByName('nCdCondPagto').Value := qryPedidonCdCondPagto.Value ;
                qryCondPagto.Parameters.ParamByName('nCdLoja').Value      := frmMenu.nCdLojaAtiva;
                qryCondPagto.Open ;

            end ;

            if ((qryPedidonCdTerceiro.Value > 0) and (qryPedidonCdTerceiro.Value <> nCdTerceiroPDV)) then
            begin

                qryTerceiro.Close ;
                qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := qryPedidonCdTerceiro.Value ;
                qryTerceiro.Open ;

            end;

            {-- atualiza os totais da tela --}
            edtPedidos.Value := edtPedidos.Value + qryPedidonValPedido.Value; //- qryPedidonValValeMerc.Value ;
            CalculaSaldo() ;

            {-- atualiza as variaveis iniciais --}
            AcumulaValorInicial(qryPedidonValProdutos.Value
                               ,(qryPedidonValJurosCondPagto.Value - qryPedidonValDescontoCondPagto.Value)
                               ,qryPedidonValDesconto.Value
                               ,qryPedidonValAcrescimo.Value
                               ,qryPedidonValPedido.Value) ;


        end ;

        edtNumPedido.Text := '' ;
        edtNumPedido.SetFocus ;

        exit ;

    end ;


    if (Trim(edtNumPedido.Text) = '') then
    begin

        if (not qryTemp_Pedido.Active) or (qryTemp_Pedido.RecordCount = 0) then
        begin
            MensagemAlerta('Informe o n�mero do pedido.') ;
            edtNumPedido.setFocus;
            abort ;
        end ;

        cxPageControl1.ActivePage := tabPagamento;

        DesativaValores;

        if (nValorPGFinal <= 0) then
        begin
            {edtSaldo.Value := 0 ;}
            btFinalizar.setFocus ;
        end
        else
        begin
            btIncPagamento.setFocus ;
        end ;

        {if (frmMenu.TBRound(edtSaldo.Value,2) > 0) then
            btIncPagamento.SetFocus
        else btFinalizar.SetFocus ;}

    end ;

end;

procedure TfrmCaixa_Recebimento.btFuncoesClick(Sender: TObject);
begin
    objCaixa_Funcoes.nCdResumoCaixa := nCdResumoCaixa ;
    objCaixa_Funcoes.cFlgUsaECF     := cFlgUsaECF ;
    objCaixa_Funcoes.cFlgUsaGaveta  := cFlgUsaGaveta ;
    objCaixa_Funcoes.cFlgUsaCFe     := cFlgUsaCFe;
    objCaixa_Funcoes.cFlgUsaCFeComp := cFlgUsaCFeComp;

    InativaFuncoes ;

    btCliente.Enabled := false ;
    btFuncoes.Enabled := false ;
    btSair.Enabled    := false ;

    //VerificaSaldoCaixa(True);

    objCaixa_Funcoes.ShowModal;

    btCliente.Enabled       := true  ;
    btFuncoes.Enabled       := true  ;
    btSair.Enabled          := true  ;

    { -- verifica se caixa esta aberto -- }
    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Add('SELECT 1                                                                                        ');
    qryAux.SQL.Add('  FROM ResumoCaixa                                                                              ');
    qryAux.SQL.Add('       INNER JOIN ContaBancaria ON ContaBancaria.nCdContaBancaria = ResumoCaixa.nCdContaBancaria');
    qryAux.SQL.Add(' WHERE nCdUsuarioAber      = ' + IntToStr(frmMenu.nCdUsuarioLogado)                              );
    qryAux.SQL.Add('   AND ResumoCaixa.nCdLoja = ' + IntToStr(frmMenu.nCdLojaAtiva)                                  );
    qryAux.SQL.Add('   AND nCdUsuarioFech IS NULL                                                                   ');
    qryAux.Open;

    if (objCaixa_Funcoes.nCdResumoCaixa <= 0) and (qryAux.IsEmpty) then {-- o caixa foi fechado --}
    begin

        btNovoPedido.Enabled    := false ;
        btNovaParcela.Enabled   := false ;
        btResgateCheque.Enabled := false ;

        CaixaLivre() ;

        if (not qryLocalizaCaixa.eof) then
        begin
            lblConta.Caption := qryLocalizaCaixanCdConta.Value + ' - FECHADO ' ;
            ShowMessage('Caixa inativo at� a pr�xima abertura.') ;
        end ;

        nCdResumoCaixa := -1 ;

    end
    else AtivaFuncoes;

    btFuncoes.SetFocus;
end;

procedure TfrmCaixa_Recebimento.AtivaFuncoes;
begin
    btNovoPedido.Enabled    := True ;
    btNovaParcela.Enabled   := True ;
    btResgateCheque.Enabled := True ;
end;

procedure TfrmCaixa_Recebimento.tabPagamentoEnter(Sender: TObject);
begin

    btCartaoPresente.Enabled := (nTotalValePresente > 0) ;

end;

procedure TfrmCaixa_Recebimento.btCartaoPresenteClick(Sender: TObject);
begin

    objCaixa_NumValePresente.ShowModal;
    
end;

procedure TfrmCaixa_Recebimento.imgBloquearClick(Sender: TObject);
begin

    if (MessageDlg('Confirma o bloqueio deste caixa ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
        exit ;

    bloquearCaixa(nCdResumoCaixa) ;
    
end;

procedure TfrmCaixa_Recebimento.bloquearCaixa(nCdResumoCaixa: integer);
begin

    if (btCancelar.Enabled) then
        btCancelar.Click;

    qryAux.Close ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('UPDATE ResumoCaixa SET cFlgBloqueado = 1 WHERE nCdResumoCaixa = ' + intToStr(nCdResumoCaixa)) ;

    try
        qryAux.ExecSQL;
    except
        MensagemErro('Erro no processamento.');
        raise ;
    end ;

    bCaixaBloqueado        := False ;
    imgBloquear.Visible    := False ;
    imgDesbloquear.Visible := True ;
    inativaFuncoes;
    btFuncoes.Enabled      := False ;

end;

procedure TfrmCaixa_Recebimento.desbloquearCaixa(nCdResumoCaixa: integer);
begin

    qryAux.Close ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('UPDATE ResumoCaixa SET cFlgBloqueado = 0 WHERE nCdResumoCaixa = ' + intToStr(nCdResumoCaixa)) ;

    try
        qryAux.ExecSQL;
    except
        MensagemErro('Erro no processamento.');
        raise ;
    end ;

    bCaixaBloqueado        := True ;
    imgBloquear.Visible    := True ;
    imgDesbloquear.Visible := False ;
    AtivaFuncoes;
    btFuncoes.Enabled   := True ;


end;

procedure TfrmCaixa_Recebimento.imgDesbloquearClick(Sender: TObject);
var
    objForm : TfrmCaixa_ConfUsuarioCaixa ;
begin

    objForm := TfrmCaixa_ConfUsuarioCaixa.Create( Self ) ;

    objForm.edtUsuario.Text := frmMenu.cNmUsuarioLogado;
    objForm.ShowModal;

    if (objForm.qryValidaLogin.Eof) then
        MensagemErro('Usu�rio Inv�lido. O caixa permanece bloqueado!')
    else desbloquearCaixa(nCdResumoCaixa) ;

    freeAndNil( objForm ) ;

end;

procedure TfrmCaixa_Recebimento.ImprimirVale(nCdVale : integer);
var
    iLinha      : integer;
    nValVale    : double;
    cCodigoVale : String;
begin

  if not qryAmbiente.Active then
  begin
    qryAmbiente.Close ;
    qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
    qryAmbiente.Open ;
  end ;

  if (qryAmbientenCdTipoImpressoraPDV.Value <= 1) then
  begin
      MensagemAlerta('Nenhuma impressora configurada para o ambiente deste computador.') ;
      exit ;
  end ;

  qryVale.Close ;
  qryVale.Parameters.ParamByName('nPK').Value := nCdVale ;
  qryVale.Open ;

  if (frmMenu.LeParametro('UTILIZASALDOVP') = 'N') then
  begin
      cCodigoVale := qryValenCdVale.Value;
      nValVale    := qryValenValVale.Value;
  end
  else
  begin
      cCodigoVale := qryValecCodigo.Value;
      nValVale    := qryValenSaldoVale.Value;
  end;

  // termica bematech
  {if (qryAmbientenCdTipoImpressoraPDV.Value = 2) then
  begin

      if (trim(qryAmbientecPortaMatricial.Value) = '') then
      begin
          ShowMessage('Porta de comunica��o da impressora do pdv n�o informada.') ;
          exit ;
      end ;

      if (trim(qryAmbientecNomeDLL.Value) = '') then
      begin
          ShowMessage('Nome da DLL n�o informada para o tipo de impressora ativa.') ;
          exit ;
      end ;

      Imprime_Termica_Bematech_AbrePorta(qryAmbientecNomeDLL.Value , trim(qryAmbientecPortaMatricial.Value));

      Imprime_Termica_Bematech_Negrito     (frmMenu.cNmEmpresaCupom) ;
      Imprime_Termica_Bematech_ImprimeLinha('                  *** PEDIDO ***                 ');
      Imprime_Termica_Bematech_ImprimeLinha('              NAO � DOCUMENTO FISCAL             ');
      Imprime_Termica_Bematech_ImprimeLinha('-------------------------------------------------');

      Imprime_Termica_Bematech_ImprimeLinha('Loja      : ' + frmMenu.cNmLojaAtiva);
      Imprime_Termica_Bematech_ImprimeLinha('No Pedido : ' + qryPedidonCdPedido.AsString);
      Imprime_Termica_Bematech_ImprimeLinha('Data      : ' + qryPedidodDtPedido.AsString);
      Imprime_Termica_Bematech_ImprimeLinha('Vendedor  : ' + qryPedidocNmUsuario.Value);

      qryItemPedido.Close ;
      qryItemPedido.Parameters.ParamByName('nCdPedido').Value := nCdPedido ;
      qryItemPedido.Open ;

      Imprime_Termica_Bematech_ImprimeLinha('                                          ') ;
      Imprime_Termica_Bematech_ImprimeLinha('Produto                     Qt    Unit.     Total') ;
      Imprime_Termica_Bematech_ImprimeLinha('-------------------------------------------------') ;

      while not qryItemPedido.eof do
      begin
          if (qryItemPedidonCdProduto.Value > 0) then
                    Imprime_Termica_Bematech_ImprimeLinha(qryItemPedidonCdProduto.asString + '-' + qryItemPedidocNmItem.Value)
          else       Imprime_Termica_Bematech_ImprimeLinha(qryItemPedidocCdProduto.Value + '-' + qryItemPedidocNmItem.Value) ;

          Imprime_Termica_Bematech_ImprimeLinha('                            ' + qryItemPedidonQtdePed.asString + '    ' + formatFloat('##,##0.00',qryItemPedidonValUnitario.Value) + '    ' + formatFloat('##,##0.00',qryItemPedidonValTotalItem.Value));

          qryItemPedido.Next ;

      end ;

      qryItemPedido.Close ;

      Imprime_Termica_Bematech_ImprimeLinha('-------------------------------------------------') ;
      Imprime_Termica_Bematech_ImprimeLinha('Total do Pedido       : ' + formatFloat('##,##0.00',qryPedidonValPedido.Value));
      Imprime_Termica_Bematech_ImprimeLinha('') ;
      Imprime_Termica_Bematech_ImprimeLinha('') ;
      Imprime_Termica_Bematech_ImprimeLinha('') ;
      Imprime_Termica_Bematech_ImprimeBarra(qryPedidonCdPedido.AsString) ;
      Imprime_Termica_Bematech_FechaPorta ;

  end ;}

  // epson matricial
  if (qryAmbientenCdTipoImpressoraPDV.Value = 3) then
  begin

      ShowMessage('Prepare a impressora e clique em OK para impress�o do vale.') ;

      if (qryAmbientecPortaMatricial.Value <> '') then
          rdPrint1.PortaComunicacao := qryAmbientecPortaMatricial.Value ;

      rdPrint1.Abrir ;

      rdprint1.TamanhoQteLPP      := seis ;
      rdprint1.FonteTamanhoPadrao := s10cpp;

      rdprint1.ImpF(01,01,'             *** VALE ***              ',[Comp12]);
      rdPrint1.ImpF(02,01,'         NAO � DOCUMENTO FISCAL        ',[Comp12]) ;
      rdprint1.ImpF(03,01,'---------------------------------------',[Comp12]);

      rdPrint1.ImpF(04,01,'Loja       : ' + qryValecNmLoja.Value        ,[Comp12]) ;
      rdPrint1.ImpF(05,01,'Data       : ' + qryValedDtVale.AsString     ,[Comp12]) ;
      rdPrint1.ImpF(07,01,'N�m. Vale  : ' + cCodigoVale                 ,[Comp12]) ;
      rdPrint1.ImpF(08,01,'Cliente    : ' + qryValecNmTerceiro.Value    ,[Comp12]) ;
      rdPrint1.ImpF(09,01,'R.G        : ' + qryValecRG.Value            ,[Comp12]) ;
      rdPrint1.ImpF(10,01,'Valor Vale : '                               ,[Comp12]) ;
      rdPrint1.ImpD(10,21,formatFloat('##,##0.00',nValVale)             ,[Comp12]) ;


      rdPrint1.ImpF(13,01,'Tipo       : ' + qryValecTipoVale.Value      ,[Comp12]) ;
      rdPrint1.ImpF(14,01,'Lan�amento : ' + qryValenCdLanctoFin.Value   ,[Comp12]) ;

      rdPrint1.ImpF(19,01,'              ---------------------------------------',[Comp12]) ;
      rdPrint1.ImpF(20,01,'Operador    : ' + frmMenu.cNmCompletoUsuario    ,[Comp12]) ;
      rdPrint1.ImpF(21,01,'Loja Emiss�o: ' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdLojaAtiva),3) + ' - ' + frmMenu.cNmLojaAtiva,[Comp12]) ;
      rdPrint1.ImpF(22,01,'Data Emiss�o: ' + dateTostr(Now()),[Comp12]) ;

      if (frmMenu.LeParametro('PREVIEWIMP') = 'S') then
      begin
          rdprint1.OpcoesPreview.Preview      := true ;
          rdprint1.OpcoesPreview.Remalina     := false ;
          rdprint1.OpcoesPreview.PaginaZebrada:= false ;
      end ;

      rdPrint1.Fechar ;

  end ;

  case MessageDlg('O vale foi impresso corretamente ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo: ImprimirVale(nCdVale) ;
  end ;

  qryVale.Close ;
  
end;

procedure TfrmCaixa_Recebimento.btClienteClick(Sender: TObject);
var
  objSelCliente                 : TfrmCaixa_SelCliente;
  objClientePessoaFisica        : TfrmClienteVarejoPessoaFisica;
  objClientePessoaFisicaSimples : TfrmClienteVarejoPessoaFisica_Simples;
  nCdTerceiroSel                : integer; 
begin

    objSelCliente := TfrmCaixa_SelCliente.Create( Self ) ;

    nCdTerceiroSel := objSelCliente.SelecionaCliente;
    freeAndNil( objSelCliente ) ;

    if (nCdTerceiroSel = 0) then
    begin
        MensagemAlerta('Nenhum cliente selecionado.') ;
        {edtNumCarnet.SetFocus;}
        exit ;
    end ;

    { -- chama tela de cadastro de acordo com o tipo do cliente (Simples/Completo) -- }
    qryVerificaCadCliente.Close;
    qryVerificaCadCliente.Parameters.ParamByName('nPK').Value := nCdTerceiroSel;
    qryVerificaCadCliente.Open;

    case (qryVerificaCadClientecFlgCadSimples.Value) of
        0 : begin
                objClientePessoaFisica := TfrmClienteVarejoPessoaFisica.Create( Self ) ;

                objClientePessoaFisica.PosicionaQuery(objClientePessoaFisica.qryMaster, intToStr(nCdTerceiroSel));
                objClientePessoaFisica.btIncluir.Visible   := False ;
                objClientePessoaFisica.btExcluir.Visible   := False ;
                objClientePessoaFisica.ToolButton1.Visible := False ;
                objClientePessoaFisica.btCancelar.Visible  := False ;
                objClientePessoaFisica.btConsultar.Visible := False ;
                objClientePessoaFisica.bChamadaExterna     := True ;
                objClientePessoaFisica.ShowModal;

                FreeAndNil( objClientePessoaFisica ) ;
            end;

        1 : begin
                objClientePessoaFisicaSimples := TfrmClienteVarejoPessoaFisica_Simples.Create(Self);

                objClientePessoaFisicaSimples.PosicionaQuery(objClientePessoaFisicaSimples.qryMaster, intToStr(nCdTerceiroSel));
                objClientePessoaFisicaSimples.btIncluir.Visible    := False ;
                objClientePessoaFisicaSimples.ToolButton7.Visible  := False ;
                objClientePessoaFisicaSimples.ToolButton2.Visible  := False ;
                objClientePessoaFisicaSimples.ToolButton6.Visible  := False ;
                objClientePessoaFisicaSimples.ToolButton50.Visible := False ;
                objClientePessoaFisicaSimples.ToolButton11.Visible := False ;
                objClientePessoaFisicaSimples.ToolButton3.Visible  := False ;
                objClientePessoaFisicaSimples.ToolButton1.Visible    := False ;
                objClientePessoaFisicaSimples.btExcluir.Visible      := False ;
                objClientePessoaFisicaSimples.btCancelar.Visible     := False ;
                objClientePessoaFisicaSimples.btConsultar.Visible    := False ;
                objClientePessoaFisicaSimples.btConvCadastro.Visible := False ;
                objClientePessoaFisicaSimples.ShowModal;

                FreeAndNil(objClientePessoaFisicaSimples);
            end;
    end;
end;

procedure TfrmCaixa_Recebimento.RemoverJurosdasParcelas1Click(
  Sender: TObject);
var
  objCaixa_DescontoJuros : TfrmCaixa_DescontoJuros ;
begin

    if (edtParcelas.Value <= 0) then
    begin
        MensagemErro('Nenhuma parcela selecionada.') ;
        exit ;
    end ;


    if (qryParcelasnValJuro.Value = 0) then
    begin
        ShowMessage('Nenhuma parcela em atraso selecionada.') ;
        exit ;
    end ;

    if not AutorizacaoGerente(28) then
    begin
        MensagemAlerta('Autentica��o falhou.') ;
        abort ;
    end ;

    {-- volta o valor do juros descontado para o saldo do t�tulo --}
    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Add('UPDATE #Temp_Titulos SET nValTotal = nValTotal + nValJurosDescontado') ;
    qryAux.ExecSQL;

    objCaixa_DescontoJuros := TfrmCaixa_DescontoJuros.Create( Self ) ;

    objCaixa_DescontoJuros.ExibeParcelas();

    freeAndNil( objCaixa_DescontoJuros ) ;

    if (MessageDlg('Confirma o desconto dos juros ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
        exit ;

    edtParcelas.Value := 0 ;
    nValorPGInicial   := 0 ;
    nValorDCInicial   := 0 ;
    nValorDMInicial   := 0 ;
    nValorJMInicial   := 0 ;
    nValorPGFinal     := 0 ;

    {--debita o valor do juros descontado do saldo do t�tulo --}
    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Add('UPDATE #Temp_Titulos SET nValTotal = nValTotal - nValJurosDescontado') ;
    qryAux.ExecSQL;

    qryParcelas.Close;
    qryParcelas.Open;

    qryParcelas.First;

    while not qryParcelas.Eof do
    begin

        edtParcelas.Value := edtParcelas.Value + qryParcelasnValTotal.Value ;

        AcumulaValorInicial(qryParcelasnValTotal.Value
                           ,0
                           ,0
                           ,0
                           ,qryParcelasnValTotal.Value) ;

        qryParcelas.Next;
    end ;

    qryParcelas.First;

    CalculaSaldo() ;

    edtNumCarnet.SetFocus;

end;

procedure TfrmCaixa_Recebimento.btRenegociacaoClick(Sender: TObject);
var
    nCdTerceiro               : integer ;
    objCaixa_SelCliente       : TfrmCaixa_SelCliente ;
    objCaixa_ConsPropReneg    : TfrmCaixa_ConsPropReneg ;
    objCaixa_EfetivaPropReneg : TfrmCaixa_EfetivaPropReneg ;
begin

    objCaixa_SelCliente := TfrmCaixa_SelCliente.Create( Self ) ;

    nCdTerceiro := objCaixa_SelCliente.SelecionaCliente;

    freeAndNil( objCaixa_SelCliente ) ;

    if (nCdTerceiro = 0) then
    begin
        MensagemAlerta('Nenhum cliente selecionado.') ;
        edtNumCarnet.SetFocus;
        exit ;
    end ;

    objCaixa_ConsPropReneg := TfrmCaixa_ConsPropReneg.Create( Self );

    objCaixa_ConsPropReneg.exibePropostas(frmMenu.nCdEmpresaAtiva,nCdTerceiro);

    if (objCaixa_ConsPropReneg.qryPropostaReneg.Active) and (objCaixa_ConsPropReneg.qryPropostaRenegnCdPropostaReneg.Value > 0) then
    begin

        objCaixa_EfetivaPropReneg := TfrmCaixa_EfetivaPropReneg.Create( Self ) ;

        objCaixa_EfetivaPropReneg.exibeProposta(objCaixa_ConsPropReneg.qryPropostaRenegnCdPropostaReneg.Value);

        {-- se a proposta foi efetivada, atualiza os valores da tela --}
        if (objCaixa_EfetivaPropReneg.bEfetivarProposta) then
        begin

            if not (qryTemp_Pagtos.Active) then
                qryTemp_Pagtos.Open ;

            CalculaSaldo() ;

            if not (qryInseri_Temp_Pagtos.Active) then
                qryInseri_Temp_Pagtos.Open ;

            qryInseri_Temp_Pagtos.Insert ;
            qryInseri_Temp_PagtosnCdFormaPagto.Value    := frmMenu.ConvInteiro(frmMenu.LeParametro('FORMAPAGTOREN')) ;
            qryInseri_Temp_PagtosnCdCondPagto.asString  := '' ;
            qryInseri_Temp_PagtosnCdTerceiro.Value      := nCdTerceiro ;
            qryInseri_Temp_PagtosnValPagto.Value        := objCaixa_EfetivaPropReneg.edtValNegociado.Value-objCaixa_EfetivaPropReneg.edtValEntrada.Value;
            qryInseri_Temp_PagtosnValEntrada.Value      := objCaixa_EfetivaPropReneg.edtValEntrada.Value;
            qryInseri_Temp_PagtosnValAcrescimo.Value    := 0 ;
            qryInseri_Temp_PagtosnValDesconto.Value     := 0 ;
            qryInseri_Temp_PagtoscFlgTipo.Value         := 1 ;
            qryInseri_Temp_PagtosnCdPropostaReneg.Value := objCaixa_EfetivaPropReneg.qryPropostaRenegnCdPropostaReneg.Value ;
            qryInseri_Temp_PagtosiQtdeParcReneg.Value   := frmMenu.ConvInteiro(objCaixa_EfetivaPropReneg.edtQtdeParcelas.Text) ;
            qryInseri_Temp_PagtosnValParcReneg.Value    := objCaixa_EfetivaPropReneg.edtValParcela.Value;
            qryInseri_Temp_Pagtos.Post ;

            edtParcelas.Value := edtParcelas.Value + objCaixa_EfetivaPropReneg.edtValNegociado.Value ;
            edtEntrada.Value  := edtEntrada.Value  + objCaixa_EfetivaPropReneg.edtValEntrada.Value;

            qryTemp_Pagtos.Close ;
            qryTemp_Pagtos.Open ;

            cxPageControl1.ActivePage := tabPagamento ;
            tabParcelas.Enabled       := false;

            qryTerceiro.Close ;
            qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro ;
            qryTerceiro.Open ;

            if (btPgEntrada.Enabled) then btPgEntrada.Click() //btPgEntrada.SetFocus
            else btFinalizar.SetFocus;

        end ;

        freeAndNil( objCaixa_EfetivaPropReneg ) ;

    end ;

    freeAndNil( objCaixa_ConsPropReneg ) ;
    
end;

procedure TfrmCaixa_Recebimento.cxLabel2Click(Sender: TObject);
begin

    if (cApuraEstatistica = 'S') then
    begin

        if (cxLabel2.Caption = 'Pedidos') then
        begin
            cxLabel2.Caption := 'Pedidos.';
            cFlgUsaECF       := False;
            cFlgUsaCFe       := False;
        end
        else
        begin
            cxLabel2.Caption := 'Pedidos';
            cFlgUsaECF       := cFlgUsaECFAux;
            cFlgUsaCFe       := cFlgUsaCFeAux;
        end ;

    end ;

end;

function TfrmCaixa_Recebimento.calculaTotalPago: double;
begin

    try
        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('IF OBJECT_ID(' + Chr(39) + 'tempdb..#Temp_Pagtos' + Chr(39) + ') IS NOT NULL SELECT IsNull(Sum(nValPagto),0) FROM #Temp_Pagtos ELSE SELECT 0') ;
        qryAux.Open ;

        Result := qryAux.FieldList[0].Value ;
    except
        Result := 0 ;
    end ;

end;

procedure TfrmCaixa_Recebimento.FormActivate(Sender: TObject);
var
   cArquivo      : TextFile;
   cLinha        : string ;
   cEstadoECF    : string ;
   objSuprimento : TfrmCaixa_Suprimento ;
   objSAT        : TfrmFuncoesSAT;
begin

    {-- inst�ncia alguns objetos que ser�o utilizados v�rias vezes no caixa --}
    objCaixa_Funcoes         := TfrmCaixa_Funcoes.Create( Self ) ;
    objCaixa_PagtoCheque     := TfrmCaixa_PagtoCheque.Create( Self ) ;
    objCaixa_ParcelaCred     := TfrmCaixa_ParcelaCred.Create( Self ) ;
    objCaixa_Cheques         := TfrmCaixa_Cheques.Create( Self ) ;
    objCaixa_DadosCheque     := TfrmCaixa_DadosCheque.Create( Self ) ;
    objCaixa_NumValePresente := TfrmCaixa_NumValePresente.Create( Self ) ;
    objCaixa_DadosCartao     := TfrmCaixa_DadosCartao.Create( Self ) ;
    objCaixa_Dinheiro        := TfrmCaixa_Dinheiro.Create( Self ) ;
    objCaixa_ListaParcelas   := TfrmCaixa_ListaParcelas.Create( Self );
    objPdvFormaPagto         := TfrmPdvFormaPagto.Create( Self ) ;
    objPdvCondPagto          := TfrmPdvCondPagto.Create( Self ) ;
    objCaixa_ResgateCheque   := TfrmCaixa_ResgateCheque.Create( Self ) ;

    imgDesbloquear.Top  := 16 ;
    imgDesbloquear.Left := 472 ;

    {frmMenu.ACBrECF1.MsgAguarde     := 'Mensagem Aguarde' ;}
    frmMenu.ACBrECF1.MsgTrabalhando := 'Processando...' ;
    frmMenu.ACBrECF1.MsgPoucoPapel  := 1 ;
    frmMenu.ACBrECF1MsgAguarde('Aguarde...');
    frmMenu.ACBrECF1.ExibeMensagem  := True ;

    CaixaLivre() ;

    if (frmMenu.LeParametro('VAREJO') <> 'S') then
    begin
        MensagemErro('O ER2Soft n�o est� configurado para funcionamento em modo de varejo.') ;
        InativaFuncoes() ;
        PostMessage(Self.Handle, WM_CLOSE, 0, 0);
        exit ;
    end ;

    lblOperador.Caption := frmMenu.cNmCompletoUsuario;
    lblLoja.Caption     := frmMenu.cNmLojaAtiva;
    lblConta.Caption    := '' ;

    Application.ProcessMessages;

    { -- Verifica se o usu�rio tem algum caixa vinculado -- }
    qryLocalizaCaixaOperador.Close ;
    qryLocalizaCaixaOperador.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
    qryLocalizaCaixaOperador.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
    qryLocalizaCaixaOperador.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
    qryLocalizaCaixaOperador.Open ;

    if (qryLocalizaCaixaOperador.Eof) then
    begin
        lblConta.Caption := 'SOMENTE FUN��ES' ;
        InativaFuncoes() ;
        Application.ProcessMessages;

        {-- Se n�o for gerente --}
        if (frmMenu.cFlgUsuarioGerente = 0) then
        begin
            MensagemErro('Voc� n�o tem acesso para movimentar nenhuma conta de caixa.') ;
            PostMessage(Self.Handle, WM_CLOSE, 0, 0);
            abort ;
        end ;

        {-- Se for um gerente --}
        if (MessageDlg('Nenhum caixa vinculado para este operador. Deseja acessar o menu de fun��es do caixa ?',mtConfirmation,[mbYes,mbNo],0) = MRYes) then
        begin
            if not AutorizacaoGerente(29) then
            begin
                MensagemErro('Autentica��o Falhou.') ;
                InativaFuncoes() ;
                btCliente.Enabled := False ;
                btFuncoes.Enabled := False ;
                PostMessage(Self.Handle, WM_CLOSE, 0, 0);
                abort ;
            end ;
        end
        else
        begin
            PostMessage(Self.Handle, WM_CLOSE, 0, 0);
            abort ;
        end ;

        exit ;
    end ;

    cFlgUsaECF     := False ;
    cFlgUsaECFAux  := False ;
    cFlgUsaCFe     := False ;
    cFlgUsaCFeAux  := False ;
    cFlgUsaCFeComp := False ;
    cFlgUsaGaveta  := False ;
    cFlgUsaTEF     := False ;

    nCdTerceiroPDV    := StrToInt(frmMenu.LeParametro('TERCPDV')) ;
    cPermDescontoMP   := frmMenu.LeParametro('APLDESCMULTPG') ;
    cApuraEstatistica := frmMenu.LeParametro('APURASTATECX') ;

    qryAmbiente.Close ;
    qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
    qryAmbiente.Open ;

    if not qryAmbiente.eof then
    begin

        iQtdeVias := qryAmbienteiViasECF.Value ;

        { -- se CF-e estiver ativo, ignora uso da ECF -- }
        if ((qryAmbientecFlgUsaECF.Value = 1) and (qryAmbientecFlgCFeAtivo.Value = 0)) then
        begin

            lblMensagem.Caption := 'Iniciando impressora ECF...' ;
            Application.ProcessMessages;

            try

                frmMenu.ACBrECF1.Modelo   := TACBrECFModelo(qryAmbientecModeloECF.asInteger) ;

                { -- atribui velocidade da ECF -- }
                if (qryAmbienteiBaudECF.Value > 0) then
                    frmMenu.ACBrECF1.Device.Baud := qryAmbienteiBaudECF.AsInteger;

                frmMenu.ACBrECF1.Porta    := qryAmbientecPortaECF.Value ;
                frmMenu.ACBrECF1.Operador := frmMenu.cNmUsuarioLogado;

                frmMenu.ACBrECF1.Ativar;
                cFlgUsaECF    := True ;
                cFlgUsaECFAux := True ;

            except
                frmMenu.ACBrECF1.Modelo := TACBrECFModelo(0) ;
                frmMenu.ACBrECF1.Porta  := 'Procurar' ;

                cFlgUsaECF    := True ;
                cFlgUsaECFAux := True ;

                if not frmMenu.ACBrECF1.AcharECF(true,False) then
                begin
                    cFlgUsaECF    := False ;
                    cFlgUsaECFAux := False ;
                    MessageDlg('Nenhuma Impressora ECF conectada.',mtInformation,[mbOk],0) ;
                    InativaFuncoes() ;
                    PostMessage(Self.Handle, WM_CLOSE, 0, 0);
                    exit ;
                end
                else frmMenu.ACBrECF1.Ativar ;

            end ;

            qryAux.Close ;
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('UPDATE ConfigAmbiente Set cModeloECF = ' + intToStr(Integer( frmMenu.ACBrECF1.Modelo )) + ', cPortaECF = ' + Chr(39) + frmMenu.ACBrECF1.Porta + Chr(39) + ' WHERE UPPER(cNmComputador) = UPPER(' + Chr(39) + frmMenu.cNomeComputador + Chr(39) + ')') ;
            qryAux.ExecSQL;

            qryAmbiente.Close ;
            qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
            qryAmbiente.Open ;

            frmMenu.cModeloECF := frmMenu.ACBrECF1.ModeloStr;
            lblSerieCF.Caption := 'S�rie ECF :';
            lblCF.Caption      := frmMenu.ACBrECF1.NumSerie;

            // captura os c�digos de pagamento do modelo da impressora fiscal
            if (frmMenu.cModeloECF <> '') then
            begin
                SP_RETORNA_FORMAPAGTO_ECF.Close ;
                SP_RETORNA_FORMAPAGTO_ECF.Parameters.ParamByName('@cModeloECF').Value := frmMenu.cModeloECF ;
                SP_RETORNA_FORMAPAGTO_ECF.Open ;

                SP_RETORNA_FORMAPAGTO_ECF.First ;

                while not SP_RETORNA_FORMAPAGTO_ECF.Eof do
                begin

                    FormaPagtoECF[SP_RETORNA_FORMAPAGTO_ECFnCdTabTipoFormaPagto.Value] := SP_RETORNA_FORMAPAGTO_ECFcCodigoECF.Value ;
                    SP_RETORNA_FORMAPAGTO_ECF.Next ;

                end ;

                SP_RETORNA_FORMAPAGTO_ECF.Close ;

            end ;

            Application.ProcessMessages;

            cEstadoECF := EstadoECF() ;

            if (cEstadoECF = 'Venda') or (cEstadoECF = 'Pagamento') or (cEstadoECF = 'Relatorio') then
            begin

                if (cEstadoECF = 'Venda') or (cEstadoECF = 'Pagamento') then
                    frmMenu.ACBrECF1.CancelaCupom;

                if (cEstadoECF = 'Relatorio') then
                    frmMenu.ACBrECF1.FechaRelatorio;

            end ;

            lblMensagem.Caption := '' ;

        end
        else
            lblCF.Caption := 'N�o Utilizada';

        if ((qryAmbientecFlgCFeAtivo.Value = 1) and (not cFlgSATAtivo)) then
        begin
            cFlgUsaECF              := False;
            cFlgUsaECFAux           := False;
            cFlgUsaCFe              := True;
            cFlgUsaCFeAux           := True;
            lblModoCFe.Visible      := True;
            lblModoEnvioCFe.Visible := True;
            cModoEnvioCFe           := qryAmbientecModoEnvioCFe.Value;

            if (cModoEnvioCFe = 'N') then
            begin
                lblSerieCF.Caption      := 'S�rie     :';
                lblCF.Caption           := 'N�o Utilizada';
                lblModoEnvioCFe.Caption := 'NFC-e'
            end;

            if ((cModoEnvioCFe = 'S') or (cModoEnvioCFe = 'C')) then
            begin
                try
                    InativaFuncoes;
                    btFuncoes.Enabled := False;
                    btCliente.Enabled := False;

                    if (cModoEnvioCFe = 'S') then
                    begin
                        objSAT := TfrmFuncoesSAT.Create(Self);

                        { -- configura sat apenas quando estiver compartilhado -- }
                        if (qryAmbientecFlgSATCompartilhado.Value = 1) then
                        begin
                            cFlgUsaCFeComp := True;

                            qryServidorSAT.Close;
                            qryServidorSAT.Parameters.ParamByName('nPK').Value := qryAmbientenCdServidorSAT.Value;
                            qryServidorSAT.Open;

                            if (qryServidorSAT.IsEmpty) then
                            begin
                                Raise Exception.Create('Servidor SAT No ' + qryAmbientenCdServidorSAT.AsString + ' n�o encontrado ou desativado, verifique as configura��es de ambiente para uso do SAT compartilhado.');
                                Exit;
                            end;

                            if (qryServidorSATnCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
                            begin
                                Raise Exception.Create('Empresa vinculada ao servidor SAT ' + qryServidorSATcNmServidorSAT.Value + ' difere da empresa ativa, verifique as configura��es de ambiente para uso do SAT compartilhado.');
                                Exit;
                            end;

                            if (qryServidorSATnCdLoja.Value <> frmMenu.nCdLojaAtiva) then
                            begin
                                Raise Exception.Create('Loja vinculada ao servidor SAT ' + qryServidorSATcNmServidorSAT.Value + ' difere da loja ativa, verifique as configura��es de ambiente para uso do SAT compartilhado.');
                                Exit;
                            end;

                            lblSerieCF.Caption         := 'S�rie SAT :';
                            lblCF.Caption              := '...';
                            lblModoCFe.Caption         := 'Modo CF-e :';
                            lblModoEnvioCFe.Caption    := 'SAT Compartilhado';
                            imgInfoServidorSAT.Hint    := 'PDV conectado ao servidor sat : ' + qryServidorSATcNmServidorSAT.Value;
                            imgInfoServidorSAT.Visible := True;
                        end
                        else
                        begin
                            lblSerieCF.Caption      := 'S�rie SAT :';
                            lblCF.Caption           := '...';
                            lblModoCFe.Caption      := 'Modo CF-e :';
                            lblModoEnvioCFe.Caption := 'SAT';

                            lblMensagem.Caption := 'Iniciando equipamento SAT...';
                            Application.ProcessMessages;

                            objSAT.btLigarSAT.Click;
                        end;

                        lblMensagem.Caption := 'Consultando informa��es SAT...';
                        Application.ProcessMessages;

                        objSAT.btConsultarInfoSAT.Click;

                        lblCF.Caption := objSAT.cNumSerieSAT;

                        qryAux.Close;
                        qryAux.SQL.Clear;
                        qryAux.SQL.Add('UPDATE ConfigAmbiente SET cNumSerieSAT = ' + #39 + objSAT.cNumSerieSAT + #39 + ' WHERE UPPER(cNmComputador) = UPPER(' + #39 + frmMenu.cNomeComputador + #39 + ')');
                        qryAux.ExecSQL;

                        FreeAndNil(objSAT);
                    end;

                    if (cModoEnvioCFe = 'C') then
                    begin
                        lblModoCFe.Caption      := 'Modo CF-e :';
                        lblModoEnvioCFe.Caption := 'NFCe e SAT';
                    end;

                    lblMensagem.Caption := '';

                    AtivaFuncoes;
                    btFuncoes.Enabled := True;
                    btCliente.Enabled := True;
                    cFlgSATAtivo      := True;
                except on E : Exception do
                begin
                    cFlgUsaCFe          := False;
                    cFlgUsaCFeAux       := False;
                    cFlgUsaCFeComp      := False;
                    cFlgSATAtivo        := False;
                    lblMensagem.Caption := '';

                    if (qryAmbientecFlgSATCompartilhado.Value = 0) then
                    begin
                        objSAT.btDesligarSAT.Click;
                        FreeAndNil(objSAT);
                    end;

                    InativaFuncoes;
                    MensagemErro(E.Message);
                    //PostMessage(Self.Handle, WM_CLOSE, 0, 0);
                    //Raise;
                    Exit;
                end;end;
            end;
        end
        else
            if (qryAmbientecFlgUsaECF.Value <> 1) then
                lblCF.Caption := 'N�o Utilizada';

       { -- inicializando TEF -- }
       if (qryAmbientecFlgTEFAtivo.Value = 1) then
           cFlgUsaTEF := True;

       { -- inicializando configura��o gaveta eletr�nica -- }
       if (qryAmbientecFlgUsaGaveta.Value = 1) then
            cFlgUsaGaveta := True;

       if (cFlgUsaGaveta) then
       begin
           try
               frmMenu.ACBrGAV1.Desativar ;

               { -- busca modelo de gaveta -- }
               frmMenu.ACBrGAV1.Modelo := TACBrGAVModelo(qryAmbientenCdTipoGavetaPDV.value);

               {-- verifica se utiliza gaveta ligada a ECF --}
               {-- funcionalidade ainda n�o dispon�vel (n�o testado!!!) --}
               {if ACBrGAV1.Modelo = gavImpressoraECF then
               begin
                   ACBrGAV1.ECF := frmMenu.ACBrECF1 ;
                   frmMenu.ACBrECF1.Ativar ;
               end ;}

               { -- verifica se n�o utiliza gaveta ligada a ECF -- }
               if frmMenu.ACBrGAV1.Modelo <> gavImpressoraECF then
               begin
                   frmMenu.ACBrGAV1.Desativar ;
                   frmMenu.ACBrGAV1.Porta := qryAmbientecNmPortaGaveta.Value;
               end;

               { -- ativa gaveta eletr�nica -- }
               if (not frmMenu.ACBrGAV1.Ativo) then
                   frmMenu.ACBrGAV1.Ativar
           except
               cFlgUsaGaveta := False;
               Showmessage('Nenhuma Gaveta Eletr�nica conectada.');
               InativaFuncoes();
               PostMessage(Self.Handle, WM_CLOSE, 0, 0);
               Exit;
           end;
       end;
    end;

    {-- Localiza um caixa aberto para este operador/loja --}
    qryLocalizaCaixa.Close ;
    qryLocalizaCaixa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
    qryLocalizaCaixa.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
    qryLocalizaCaixa.Open ;

    {-- Se nenhum caixa estiver aberto --}
    if (qryLocalizaCaixa.eof) then
    begin

        if (MessageDlg('O Caixa est� fechado. Confirma a abertura ?',mtConfirmation,[mbYes,mbNo],0) = MRYes) then
        begin

            {--verifica se permite abrir o caixa com documentos do dia anterior--}
            if (frmMenu.LeParametro('ABRECXDOCDIAA') = 'N') then
            begin

                qryVerificaChequeCaixa.Close;
                qryVerificaChequeCaixa.Parameters.ParamByName('nPK').Value := qryLocalizaCaixaOperadornCdContaBancaria.Value ;
                qryVerificaChequeCaixa.Open;

                if not qryVerificaChequeCaixa.Eof then
                begin
                    MensagemErro('Este caixa possui cheque(s) de movimento anterior. Transfira para o cofre antes da abertura do caixa.') ;
                    InativaFuncoes() ;
                    btCliente.Enabled := True ;
                    btFuncoes.Enabled := True ;
                    prGeraRemessaNum;
                    Abort;
                end ;

                qryVerificaCartaoCaixa.Close;
                qryVerificaCartaoCaixa.Parameters.ParamByName('nPK').Value := qryLocalizaCaixaOperadornCdContaBancaria.Value ;
                qryVerificaCartaoCaixa.Open;

                if not qryVerificaCartaoCaixa.Eof then
                begin
                    MensagemErro('Este caixa possui comprovante(s) de cart�o de movimento anterior. Transfira para o cofre antes da abertura do caixa.') ;
                    InativaFuncoes() ;
                    btCliente.Enabled := True ;
                    btFuncoes.Enabled := True ;
                    prGeraRemessaNum;
                    Abort;
                end ;

                qryVerificaCrediarioCaixa.Close;
                qryVerificaCrediarioCaixa.Parameters.ParamByName('nPK').Value := qryLocalizaCaixaOperadornCdContaBancaria.Value ;
                qryVerificaCrediarioCaixa.Open;

                if not qryVerificaCrediarioCaixa.Eof then
                begin
                    MensagemErro('Este caixa possui comprovante(s) de credi�rio de movimento anterior. Transfira para o cofre antes da abertura do caixa.') ;
                    InativaFuncoes() ;
                    btCliente.Enabled := True ;
                    btFuncoes.Enabled := True ;
                    prGeraRemessaNum;
                    Abort;
                end ;

            end ;

            {--verifica se � necess�rio senha do gerente para abrir o caixa--}
            if ((frmMenu.LeParametro('SENGERAC') = 'S') and (not AutorizacaoGerente(30))) then
            begin
                MensagemAlerta('Autentica��o Falhou.') ;
                InativaFuncoes() ;
                PostMessage(Self.Handle, WM_CLOSE, 0, 0);
                exit ;
            end ;

            frmMenu.Connection.BeginTrans;

            try
                SP_ABRE_CAIXA.Close ;
                SP_ABRE_CAIXA.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
                SP_ABRE_CAIXA.Parameters.ParamByName('@nCdLoja').Value    := frmMenu.nCdLojaAtiva;
                SP_ABRE_CAIXA.ExecProc;
            except
                qryCaixaAberto.Close ;
                InativaFuncoes() ;
                frmMenu.Connection.RollbackTrans;
                MensagemErro('Erro no processamento.') ;
                raise ;
            end ;

            frmMenu.Connection.CommitTrans ;

            qryLocalizaCaixa.Close ;
            qryLocalizaCaixa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
            qryLocalizaCaixa.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
            qryLocalizaCaixa.Open ;

            if (frmMenu.LeParametro('SUGLEITURAX') = 'S') and (cFlgUsaECFAux) then
                case MessageDlg('Deseja Emitir a Leitura X ?',mtConfirmation,[mbYes,mbNo],0) of
                    mrYes: LeituraX ;
                end ;

            objSuprimento := TfrmCaixa_Suprimento.Create( Self ) ;

            objSuprimento.cFlgUsaECF     := cFlgUsaECF     ;
            objSuprimento.nCdResumoCaixa := qryLocalizaCaixanCdResumoCaixa.Value ;

            if (frmMenu.LeParametro('SENHASUPRABRCX') = 'N') then
                objSuprimento.bSenha := False
            else
                objSuprimento.bSenha := True;

            objSuprimento.ShowModal;

            freeAndNil( objSuprimento ) ;

            { -- abre gaveta -- }
            if (cFlgUsaGaveta) then
                frmMenu.prAbreGaveta;
        end
        else
        begin {-- Se n�o desejar abrir o caixa --}
            InativaFuncoes() ;
            lblConta.Caption                  := 'CAIXA FECHADO' ;
            nCdResumoCaixa                    := 0 ;

            objCaixa_Funcoes.nCdContaBancaria  := 0 ;
            objCaixa_Funcoes.dDtAbertura       := 0 ;
            objCaixa_Funcoes.cNmCaixa          := '';
            exit ;
        end ;

    end ;

    {-- Se o caixa estiver aberto... --}
    nCdResumoCaixa      := qryLocalizaCaixanCdResumoCaixa.Value ;
    lblConta.Caption    := qryLocalizaCaixanCdConta.Value + ' Abertura: ' + qryLocalizaCaixadDtAbertura.AsString ;
    dDtLimiteFech       := qryLocalizaCaixadDtLimiteFech.Value ;

    if (dDtLimiteFech < Date) then
    begin
        MensagemErro('Este caixa requer fechamento imediato.') ;
    end
    else
    begin
        VerificaSaldoCaixa(True);
    end;

    objCaixa_Funcoes.nCdContaBancaria := qryLocalizaCaixanCdContaBancaria.Value ;
    objCaixa_Funcoes.dDtAbertura      := qryLocalizaCaixadDtAbertura.Value ;
    objCaixa_Funcoes.cNmCaixa         := qryLocalizaCaixanCdConta.Value ;

    frmMenu.ZeraTemp('#Temp_Pedido');
    frmMenu.ZeraTemp('#Temp_Titulos');
    frmMenu.ZeraTemp('#Temp_Pagtos');
    frmMenu.ZeraTemp('#Temp_Cheque_Pagamento');
    frmMenu.ZeraTemp('#Temp_Crediario');
    frmMenu.ZeraTemp('#Temp_ChequeResgate');
    frmMenu.ZeraTemp('#Temp_RestricaoVenda');

    CalculaSaldo() ;

    bCaixaBloqueado := (qryLocalizaCaixacFlgBloqueado.Value = 1);

    imgBloquear.Visible    := (not bCaixaBloqueado) ;
    imgDesbloquear.Visible := bCaixaBloqueado ;

    if (bCaixaBloqueado) then
    begin
        InativaFuncoes;
        btFuncoes.Enabled := false ;
    end
    else if (btNovoPedido.Enabled) then
        btNovoPedido.SetFocus
        else btCancelar.Click;

    if (cFlgUsaConsig = 1) then
       btNovoPedido.Click;

end;

procedure TfrmCaixa_Recebimento.edtJMKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    TrataTeclaFuncao(Key) ;

    if (Key = vk_return) then
        btIncPagamento.SetFocus ;

end;

procedure TfrmCaixa_Recebimento.edtJMEnter(Sender: TObject);
begin

    edtJM.SelectAll;

end;

procedure TfrmCaixa_Recebimento.edtJMExit(Sender: TObject);
begin

    edtJM.Value       := abs(edtJM.Value) ;
    edtVlrPagto.Value := edtValor.Value + edtDC.Value - edtDM.Value + edtJM.Value ;

end;

procedure TfrmCaixa_Recebimento.retiraDescontoPedidos;
begin

    // retira o desconto dos pedidos

    qryTemp_Pedido.First ;

    while not qryTemp_Pedido.Eof do
    begin

        if ((abs(qryTemp_PedidonValDesconto.Value) + abs(qryTemp_PedidonValDescontoCondPagto.Value) + abs(qryTemp_pedidonValJurosCondPagto.Value)) > 0) then
        begin

            // atualiza o pedido fisicamente na tabela PEDIDO
            qryAux.Close ;
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('UPDATE Pedido                                                                                        ') ;
            qryAux.SQL.Add('   Set nValPedido            = nValPedido + nValDesconto + nValDescontoCondPagto - nValJurosCondPagto') ;
            qryAux.SQL.Add('      ,nValDesconto          = 0                                                                     ') ;
            qryAux.SQL.Add('      ,nValDescontoCondPagto = 0                                                                     ') ;
            qryAux.SQL.Add('      ,nValJurosCondPagto    = 0                                                                     ') ;
            qryAux.SQL.Add(' WHERE nCdPedido             = ' + qryTemp_PedidonCdPedido.AsString) ;
            qryAux.ExecSQL;

            // atualiza o pedido na tabela temporaria do caixa
            qryAux.Close ;
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('UPDATE #Temp_Pedido                                                                                  ') ;
            qryAux.SQL.Add('   Set nValPedido            = nValPedido + nValDesconto + nValDescontoCondPagto - nValJurosCondPagto') ;
            qryAux.SQL.Add('      ,nValDesconto          = 0                                                                     ') ;
            qryAux.SQL.Add('      ,nValDescontoCondPagto = 0                                                                     ') ;
            qryAux.SQL.Add('      ,nValJurosCondPagto    = 0                                                                     ') ;
            qryAux.SQL.Add(' WHERE nCdPedido             = ' + qryTemp_PedidonCdPedido.AsString) ;
            qryAux.ExecSQL;

        end ;

        qryTemp_Pedido.Next;

    end ;

    qryTemp_Pedido.Close ;
    qryTemp_Pedido.Open ;

    // zera os saldos da tela
    edtPedidos.Value    := 0 ;
    edtParcelas.Value   := 0 ;
    edtEntrada.Value    := 0 ;
    edtValorPago.Value  := 0 ;
    edtTroco.Value      := 0 ;
    edtVale.Value       := 0 ;
    edtChequeResg.Value := 0 ;

    CalculaSaldo() ;

    nValorPGInicial  := 0 ;
    nValorDCInicial  := 0 ;
    nValorDMInicial  := 0 ;
    nValorJMInicial  := 0 ;
    nValorPGFinal    := 0 ;
    
    // atualiza os totais da tela
    while not qryTemp_Pedido.Eof do
    begin

        {-- atualiza os totais da tela --}
        edtPedidos.Value := edtPedidos.Value + qryTemp_PedidonValPedido.Value; //- qryPedidonValValeMerc.Value ;
        CalculaSaldo() ;

        {-- atualiza as variaveis iniciais --}
        AcumulaValorInicial(qryTemp_PedidonValProdutos.Value
                           ,(qryTemp_PedidonValJurosCondPagto.Value - qryTemp_PedidonValDescontoCondPagto.Value)
                           ,qryTemp_PedidonValDesconto.Value
                           ,qryTemp_PedidonValAcrescimo.Value
                           ,qryTemp_PedidonValPedido.Value) ;

        qryTemp_Pedido.Next;

    end ;

    qryTemp_Pedido.First ;

end;

procedure TfrmCaixa_Recebimento.bCancelarRespClick(Sender: TObject);
begin
   fCancelado := True ;
end;

function TfrmCaixa_Recebimento.processarPagamentosTEF: boolean;
begin
   if (qryTemp_Pagtos.IsEmpty) then
       Exit;

    qryAux.Close;

    qryAux.SQL.Clear;
    qryAux.SQL.Add('SELECT nCdTemp_Pagto FROM #Temp_Pagtos WHERE cFlgTEF = 1');

    qryAux.Open;

    if (qryAux.IsEmpty) then
        Exit;

    SetLength(arrCupomTef, (qryAux.RecordCount * 2));

    qryTemp_Pagtos.First;

    while not qryTemp_pagtos.eof do
    begin

        // Seleciona somente os pagamentos via TEF

        if (qryTemp_PagtoscFlgTEF.Value = 1) then
        begin

            // Verifica se o PinPad ainda est� conectado

            if (frmTransacaoTEF.TestaPinPad = 0) then
            begin
                MensagemErro('Pin Pad desconectado ou inoperante.');

                Result := False;

                Exit;
            end;

            qryCondPagto.Close;
            qryCondPagto.Parameters.ParamByName('nCdCondPagto').Value := qryTemp_PagtosnCdCondPagto.Value;
            qryCondPagto.Parameters.ParamByName('nCdLoja').Value      := frmMenu.nCdLojaAtiva;
            qryCondPagto.Open;

            frmTransacaoTEF.edtValor.Value       := qryTemp_PagtosnValPagto.Value;
            frmTransacaoTEF.edtDesconto.Value    := qryTemp_PagtosnValDesconto.Value;
            frmTransacaoTEF.edtAcrescimo.Value   := qryTemp_PagtosnValAcrescimo.Value;
            frmTransacaoTEF.edtQtdeParcelas.Text := qryCondPagtoiQtdeParcelas.AsString;

            frmTransacaoTEF.edtValorTotal.Value := (qryTemp_PagtosnValPagto.Value + qryTemp_PagtosnValDesconto.Value + qryTemp_PagtosnValAcrescimo.Value);

            frmTransacaoTEF.qryOperacao.Close;
            frmTransacaoTEF.qryOperacao.Parameters.ParamByName('nPK').Value := qryTemp_PagtosnCdTemp_Pagto.Value;
            frmTransacaoTEF.qryOperacao.Open;

            if (frmTransacaoTEF.ShowTransacao) then
            begin
                qryTemp_Pagtos.Edit;
                qryTemp_PagtosiNrDocto.Value           := StrToInt(frmMenu.RetNumeros(frmTransacaoTEF.edtDocumento.Text));
                qryTemp_PagtosiNrAutorizacao.Value     := StrToInt(frmMenu.RetNumeros(frmTransacaoTEF.edtAutorizacao.Text));
                qryTemp_PagtosnCdOperadoraCartao.Value := frmTransacaoTEF.qryOperadoranCdOperadoraCartao.Value;
                qryTemp_Pagtos.Post;

                sCupom := frmMenu.ACBrECF1.NumCupom;
                sData  := frmTransacaoTEF.sData;
                sHora  := frmTransacaoTEF.sHora;

                arrCupomTef[0] := TStringList.Create;
                arrCupomTef[0].Append(frmTransacaoTEF.memoVia1.Text);

                arrCupomTef[1] := TStringList.Create;
                arrCupomTef[1].Append(frmTransacaoTEF.memoVia2.Text);
            end
            else
            begin
                if (frmTransacaoTEF.Enabled) then
                    frmTransacaoTEF.FinalizaTrans(0);

                MensagemAlerta('Transa��o cancelada.');

                Result := False;

                Exit;
            end;

        end ;

        qryTemp_Pagtos.Next ;
    end ;

    qryTemp_Pagtos.First;

    Result := True;
end;

procedure TfrmCaixa_Recebimento.DBGridEh1DblClick(Sender: TObject);
begin
    qryParcelas.Close ;
    qryParcelas.Open ;

    qryParcelas.First ;

    edtParcelas.Value := 0 ;
    nValorPGInicial   := 0 ;
    nValorDCInicial   := 0 ;
    nValorDMInicial   := 0 ;
    nValorJMInicial   := 0 ;
    nValorPGFinal     := 0 ;

    while not qryParcelas.Eof do
    begin

        edtParcelas.Value := edtParcelas.Value + qryParcelasnValTotal.Value ;

        AcumulaValorInicial(qryParcelasnValTotal.Value
                           ,0
                           ,0
                           ,0
                           ,qryParcelasnValTotal.Value) ;

        qryParcelas.next ;
    end ;

    qryParcelas.First ;

    CalculaSaldo() ;

end;

procedure TfrmCaixa_Recebimento.LiquidacaoParcialClick(Sender: TObject);
var
    objPDVDesconto : TfrmPdvDesconto;
begin
    //Cria a tela de desconto
    objPDVDesconto := TfrmPdvDesconto.Create(self);

    //                FORMATANDO A JANELA          \\

    objPDVDesconto.Caption         := 'Liquida��o Parcial';
    objPDVDesconto.Label1.Caption  := 'Saldo do Titulo';
    objPDVDesconto.Label2.Caption  := '% Restante';
    objPDVDesconto.Label3.Caption  := 'Saldo restante';
    objPDVDesconto.Label4.Caption  := 'Valor do pagto';


    //Retorna o valor do saldo do item
    qryAux.Close;
    qryAux.SQL.Clear;

    //                --- SQL ---                \\
    qryAux.SQL.Add('   SELECT nSaldoTit    ');
    qryAux.SQL.Add('     FROM Titulo       ');
    qryAux.SQL.Add('    WHERE nCdTitulo =  '+intToStr(qryParcelasnCdTitulo.Value));

    qryAux.Open;

    // Define o total
    objPDVDesconto.edtValVenda.Value := qryAux.FieldList[0].value;

    //Define o percentual maximo de desconto
    objPDVDesconto.nPercMaxDesc := 100;

    //Abre a janela de Desconto
    objPDVDesconto.ShowModal;

    // Testa se o total na janela � negativo ou igual a zero
    if objPDVDesconto.edtValliquido.Value <=0 then
    begin
        MessageDLG('O valor do pagamento parcial deve ser maior que zero.',mtWarning,[mbOK],0);
        abort;
    end;

    //Testa se o pagamento parcial � menor que o valor do titulo
    if objPDVDesconto.edtValliquido.Value < qryAux.FieldList[0].value then
    begin
        //Altera o saldo da parcela na temporaria
        qryLiqParcial.Close;
        qryLiqParcial.Parameters.ParamByName('nCdTitulo').Value := qryParcelasnCdTitulo.Value;
        qryLiqParcial.Parameters.ParamByName('nValLiquido').Value := objPDVDesconto.edtValliquido.Value;
        qryLiqParcial.ExecSQL;

        if (qryParcelasnValJurosDescontado.Value > 0) then
            ShowMessage('Desconto Sob Juros foi removido desta parcela.');
    end
    else
    begin
        MessageDLG('O pagamento parcial deve ser menor que o valor da parcela.',mtWarning,[mbOK],0);
    end;

    //===============================================\\

    //Atualiza o grid
    qryParcelas.Close ;
    qryParcelas.Open ;

    // Faz um loop atualizando o total
    qryParcelas.First ;

    edtParcelas.Value := 0 ;
    nValorPGInicial   := 0 ;
    nValorDCInicial   := 0 ;
    nValorDMInicial   := 0 ;
    nValorJMInicial   := 0 ;
    nValorPGFinal     := 0 ;

    while not qryParcelas.Eof do
    begin

        edtParcelas.Value := edtParcelas.Value + qryParcelasnValTotal.Value ;

        AcumulaValorInicial(qryParcelasnValTotal.Value
                           ,0
                           ,0
                           ,0
                           ,qryParcelasnValTotal.Value) ;

        qryParcelas.next ;
    end ;

    qryParcelas.First ;

    CalculaSaldo() ;

    //Manda o foco para o campo numero Carnet.
    edtNumCarnet.SetFocus;

end;

procedure TfrmCaixa_Recebimento.PopupMenu2Popup(Sender: TObject);
begin
    { -- verifica utiliza��o de liquida��o de credi�rio parcial -- }
    if frmMenu.LeParametro('USALIQPARCIAL') = 'S' then
        LiquidacaoParcial.Visible := true
    else
        LiquidacaoParcial.Visible := False;
end;

procedure TfrmCaixa_Recebimento.qryTerceiroAfterOpen(DataSet: TDataSet);
begin
    if (qryTerceiro.IsEmpty) then
        Exit;

    if ((not qryCondPagto.Eof) and (qryCondPagtocFlgExigeCliente.Value = 1)) then
    begin
        qryVerificaCadCliente.Close;
        qryVerificaCadCliente.Parameters.ParamByName('nPK').Value := qryTerceironCdTerceiro.Value;
        qryVerificaCadCliente.Open;

        if ((qryVerificaCadClientecFlgCadSimples.Value = 1) and (qryCondPagtocFlgTipoCadCliente.Value = 1)) then
        begin
            MensagemAlerta('Esta forma de pagamento exige Cadastro Completo do cliente.');
            qryTerceiro.Close;
        end;
    end;
end;

procedure TfrmCaixa_Recebimento.ImprimeFinalizaTEF;
var
    i : integer ;
begin
    // De acordo com as etapas 41, 43 e 45 do roteiro de homologa��o TEF
    // caso haja alguma interrup��o na impress�o o sistema deve reiniciar
    // a impress�o das duas vias do cupom.

    if (qryTemp_PagtoscFlgTEF.Value = 1) then
    begin
        i := 0;

        repeat
            try
                frmMenu.ACBrECF1.RelatorioGerencial(arrCupomTef[0]);
                frmMenu.ACBrECF1.RelatorioGerencial(arrCupomTef[1]);

                i := 0;
            except
                i := 1;
            end;
        until (i = 0);

        frmTransacaoTEF.FinalizaTrans(1);
    end;
end;

procedure TfrmCaixa_Recebimento.cxPageControl1Change(Sender: TObject);
begin
  if (cxPageControl1.ActivePage = tabPagamento) then
  begin
      { -- desativa p�gina pedidos para que algumas valida��es presentes nesta pedidos
           n�o sejam perdidas, se o usu�rio desejar incluir novos pedidos
           � necess�rio efetuar o cancelamento e iniciar o recebimento novamente -- }
      if ((qryTemp_Pedido.Active) and (qryTemp_Pedido.RecordCount > 0)) then
      begin
          edtNumPedido.Enabled := False;
          edtNumPedido.Color   := clMenuBar;
      end;
  end;
end;

procedure TfrmCaixa_Recebimento.prGeraRemessaNum;
var
  objRemessaNum : TfrmRemessa_Numerarios;
begin
  if (MessageDlg('Deseja gerar a remessa de numer�rios agora ?',mtConfirmation,[mbYes,mbNo],0) = mrYes) then
  begin
      objRemessaNum := TfrmRemessa_Numerarios.Create(Nil);
      frmMenu.showForm(objRemessaNum, True);
  end;
end;

procedure TfrmCaixa_Recebimento.prGeraCFe;
var
  objSAT       : TfrmFuncoesSAT;
  objNFCe      : TfrmEmiteNFe2;
  bStatusSEFAZ : Boolean;
begin
  try
      objSAT  := TfrmFuncoesSAT.Create(Self);
      objNFCe := TfrmEmiteNFe2.Create(Self);

      bStatusSEFAZ := objNFCe.StatusWebService(1).nCdStatusSEFAZ <> 107;

      { -- desativa fun��es durante o processo de transmiss�o da venda -- }
      btFinalizar.Enabled := False;
      btCancelar.Enabled  := False;
      btCliente.Enabled   := False;

      { -- NFCe -- }
      if (cModoEnvioCFe = 'N') then
      begin
          if (bStatusSEFAZ = False) then
          begin
              MensagemErro('Webservice SEFAZ de origem est� inoperante no momento. Tente novamente mais tarde.');
              Exit;
          end;

          try
              objNFCe.gerarNFCe(nCdDoctoFiscal);
          except
              Raise;
          end;

          Exit;
      end;

      { -- SAT -- }
      if (cModoEnvioCFe = 'S') then
      begin
          lblMensagem.Caption := 'Transmitindo venda ao SAT...';

          objSAT.prGeraVendaSAT(nCdDoctoFiscal);

          lblMensagem.Caption := 'Imprimindo extrato de venda...';

          try
              objSAT.prPreparaImpressao;
               frmMenu.ACBrSAT1.ImprimirExtrato;
               frmMenu.ACBRPosPrinter1FechaImpTermica;

          except on E : Exception do
              MensagemErro('Erro ao efetuar a impress�o extrato de venda, entre em "Fun��es do SAT" e reimprima o comprovante.'
                          +#13#13
                          +'Mensagem SAT: ' + E.Message);
          end;

          Exit;
      end;

      { -- NFCe e Contig. SAT -- }
      if (cModoEnvioCFe = 'C') then
      begin
          { -- se servi�o estiver em opera��o envia NFC-e, se n�o ativa modo SAT -- }
          if (bStatusSEFAZ) then
          begin
              try
                  objNFCe.gerarNFCe(nCdDoctoFiscal);
              except
                  Raise;
                  Exit;
              end;
          end
          else
          begin
              lblMensagem.Caption := 'Transmitindo venda ao SAT...';

              objSAT.prGeraVendaSAT(nCdDoctoFiscal);

              lblMensagem.Caption := 'Imprimindo extrato de venda...';

              try
                  objSAT.prImpExtratoVenda(nCdDoctoFiscal);
              except on E : Exception do
                  MensagemErro('Erro ao efetuar a impress�o extrato de venda, entre em Fun��es do SAT e reimprima o comprovante.'
                              +#13#13
                              +'Mensagem SAT: ' + E.Message);
              end;
          end;

          Exit;
      end;
  finally
      lblMensagem.Caption := '';
      btFinalizar.Enabled := True;
      btCancelar.Enabled  := True;
      btCliente.Enabled   := True;

      FreeAndNil(objSAT);
      FreeAndNil(objNFCe);
  end;
end;

procedure TfrmCaixa_Recebimento.DBEdit9Change(Sender: TObject);
var
  cliente : String;
  atualizar : Integer;
  NewForm : TForm;
begin
  if (nCdTerceiro <> 0) then
  begin
  try
    qryVerificaAtDados.Close;
    qryVerificaAtDados.SQL.Clear;
    qryVerificaAtDados.SQL.Add('EXEC SP_VERIFICA_DADOS_CLIENTE @cliente = ' +  IntToStr(nCdTerceiro));
    qryVerificaAtDados.Open;
    qryVerificaAtDados.First;
    atualizar := qryVerificaAtDados.FieldByName('Result').AsInteger;
    if ((atualizar = 1) and (DBEdit9.Text <> '')) then
      begin
        NewForm := TfrmCaixa_AtualizaCliente.Create(Self);
        NewForm.ShowModal;
      end;
    except
    end;
    end;
end;

function TfrmCaixa_Recebimento.processaPagtoTEFCappta: Boolean;
  begin
  if (qryTemp_Pagtos.IsEmpty) then
    Exit;

    qryAux.Close;
    qryAux.SQL.Clear;
    qryAux.SQL.Add('SELECT nCdTemp_Pagto FROM #Temp_Pagtos WHERE cFlgTEF = 1');
    qryAux.Open;
    if (qryAux.IsEmpty) then
      begin
        Result := True;
        Exit;
      end;

    frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta Criar Objeto TEF' );

    objTEF := TfrmCapptAPI.Create(Self);

    if (objTEF.GPPAtivo = False) then
    begin
      frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta Gerenciador Cappta n�o est� ativo!' );
      frmMenu.ShowMessage('Gerenciador Cappta n�o est� ativo!');
        Result := False;
      Exit;
    end;
    cupomCliente := TStringList.Create;
    cupomLojista := TStringList.Create;

    qtCartoes := qryTemp_Pagtos.RecordCount;

    { verifica se � multicart�es }
    if (qtCartoes > 1) then
      begin
        frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta Inicio Multiplos Cartoes' );
        respMultiCartoes := objTEF.cappta.IniciarMultiCartoes(qtCartoes);
        if (respMultiCartoes <> 0) then
          begin
            frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta '+Format('N�o foi poss�vel iniciar a opera��o. C�digo de erro %d', [respMultiCartoes]) );
            frmMenu.showMessage(Format('N�o foi poss�vel iniciar a opera��o. C�digo de erro %d', [respMultiCartoes]));
            Exit;
          end;
        end;

      qryTemp_Pagtos.First;
      while not (qryTemp_Pagtos.Eof) do
        begin
            { -- verificar se a forma de pagamento usa TEF --}
            if (qryTemp_PagtoscFlgTEF.Value = 1  ) then
            begin

              if not (( qryTemp_PagtosnCdTabTipoFormaPagto.Value = 3 ) or ( qryTemp_PagtosnCdTabTipoFormaPagto.Value = 4 ) ) then
              begin
                frmMenu.showMessage('Forma '+qryTemp_PagtoscNmFormaPagto.asstring+' n�o associada ao Tipo 3 ou 4 ( D�bito ou Cr�dito )');
                 Result := False;
                 Exit;
              end;

              qryCondPagto.Close;
              qryCondPagto.Parameters.ParamByName('nCdCondPagto').Value := qryTemp_PagtosnCdCondPagto.Value;
              qryCondPagto.Parameters.ParamByName('nCdLoja').Value      := frmMenu.nCdLojaAtiva;
              qryCondPagto.Open;

              valorTEF := qryTemp_PagtosnValPagto.Value; //+ qryTemp_PagtosnValDesconto.Value + qryTemp_PagtosnValAcrescimo.Value);

              { pagto d�bito }
              if (qryTemp_PagtosnCdTabTipoFormaPagto.Value = 3) then
              begin
                cFlgTipoOperacao := 'D';
                frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta Inicio PagtoDebito' );
                objTEF.iniciaPagtoDebito(valorTEF);
              end;

              { pagto cr�dito }
              if (qryTemp_PagtosnCdTabTipoFormaPagto.Value = 4) then
              begin
                  cFlgTipoOperacao := 'C';
                  qryTipoParc.Close;
                  qryTipoParc.Open;

                  qtParcelasTEF := qryCondPagtoiQtdeParcelas.Value;

                  frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta Inicio PagtoCredito' );
                  if (qtParcelasTEF > 1) then
                    objTEF.iniciaPagtoCredito(valorTEF,True,qtParcelasTEF,StrToInt(qryTipoParccValor.Value))
                  else
                    objTEF.iniciaPagtoCredito(valorTEF,False,qtParcelasTEF,StrToInt(qryTipoParccValor.Value));
              end;

              { ---- In�cio da Itera��o TEF ---- }
              frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta Antes Iteracao TEF' );
              Repeat


                iteracaoTef := ObjTEF.cappta.IterarOperacaoTef();
                tipoIteracao := iteracaoTef.Get_TipoIteracao;

              { if (tipoIteracao = 8) then
                begin
                  transacoesPendentes := (iteracaoTef as IRespostaTransacaoPendente);
                  if (MessageDlg(transacoesPendentes.Get_Mensagem,mtConfirmation,[mbYes,mbNo],0) = mrNo) then
                    objTEF.cappta.EnviarParametro('0', 1)
                  else
                    objTEF.cappta.EnviarParametro('1', 1);
                end; }

              Until (iteracaoTef.Get_TipoIteracao = 1) or (iteracaoTef.Get_TipoIteracao = 2);

              { ---- Fim da Itera��o TEF ---- }

              if (tipoIteracao <> 1) then
              begin
                opNegada := (iteracaoTef as IRespostaOperacaoRecusada);
                frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta Iteracao TEF NEGADA:'+ opNegada.Get_Motivo );

                frmMenu.ShowMessage('Opera��o n�o processada. Motivo: ' + opNegada.Get_Motivo);
                if (respMultiCartoes = 0) then
                begin
                  frmMenu.ShowMessage('As transa��o TEF n�o foram finalizadas.');

                  frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta Iteracao TEF NEGADA Antes de DesfazerPagamentos' );
                  objTEF.cappta.DesfazerPagamentos;
                  frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta Iteracao TEF NEGADA Apos de DesfazerPagamentos' );
                end;
                Result := False;
                Exit;
              end;
              opAprovada := (iteracaoTef as IRespostaOperacaoAprovada);

              frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta Iteracao TEF APROVADA' );

              nCodigoBandeiraCartao := opAprovada.Get_CodigoBandeiraCartao;

              qryOpCartao.Close;
              qryOpCartao.SQL.Clear;
              qryOpCartao.SQL.Add('SELECT TOP 1 nCdOperadoraCartao FROM OperadoraCartao ' +
                                  'WHERE cFlgTipoOperacao = ' + chr(39) + cFlgTipoOperacao + chr(39) +
                                  'AND nCodigoBandeiraCartao = ' + IntToStr(nCodigoBandeiraCartao));
              qryOpCartao.Open;


              if (qryOpCartao.IsEmpty) then
              begin
                frmMenu.ShowMessage('Erro - adquirente n�o cadastrada - C�digo Cappta: ' + IntToStr(opAprovada.Get_CodigoBandeiraCartao));
                objTEF.cappta.DesfazerPagamentos;
                Result := False;
                Exit;
              end;
              //frmMenu.ShowMessage('NrDocto: ' + opAprovada.Get_NsuAdquirente);
              //frmMenu.ShowMessage('NrAutorizacao: ' + opAprovada.Get_CodigoAutorizacaoAdquirente);

              qryTemp_Pagtos.Edit;
              // Substituido em 17/06/19 por campo novo widestring.
              //qryTemp_PagtosiNrDocto.Value            := StrToInt(opAprovada.Get_NsuAdquirente);
              qryTemp_PagtosiNrDocto.Value            := 0;
              qryTemp_PagtoscNrDoctoNSU.Value         := opAprovada.Get_NsuAdquirente;
              qryTemp_PagtosiNrAutorizacao.Value      := 0;
              qryTemp_PagtoscNrAutorizacao.Value      := opAprovada.Get_CodigoAutorizacaoAdquirente;
              qryTemp_PagtosnCdOperadoraCartao.Value  := qryOpCartaonCdOperadoraCartao.Value;
              qryTemp_PagtoscNumeroControle.Value     := opAprovada.Get_numeroControle;
              qryTemp_Pagtos.Post;

              frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta NSU:'+opAprovada.Get_NsuAdquirente );
              frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta AUT:'+opAprovada.Get_CodigoAutorizacaoAdquirente );
              cupomCliente.Add(opAprovada.Get_CupomCliente);
              cupomCliente.Add('');
              cupomCliente.Add('');
              cupomCliente.Add('');
              cupomCliente.Add('');

              cupomLojista.Add(opAprovada.Get_CupomLojista);
              cupomLojista.Add('');
              cupomLojista.Add('');
              cupomLojista.Add('');
              cupomLojista.Add('');

              frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta Obteve Comprovante' );

            end;
            frmMenu.CapptaLog('fCaixaRecebimento/processaPagtoTEFCappta Fim do Pagamento' );

            qryTemp_Pagtos.Next;
        end;

    confTEF := True;

    qryTemp_Pagtos.First;

    Result := True;
end;




initialization
    RegisterClass(TfrmCaixa_Recebimento);

end.
