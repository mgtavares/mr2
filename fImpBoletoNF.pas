unit fImpBoletoNF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, StdCtrls, Mask, cxLookAndFeelPainters, cxButtons,
  DBCtrls, DBGridEhGrouping;

type
  TfrmImpBoletoNF = class(TfrmProcesso_Padrao)
    Panel1: TPanel;
    DBGridEh1: TDBGridEh;
    edtDtFaturamento: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtTerceiro: TEdit;
    qryResultado: TADOQuery;
    dsResultado: TDataSource;
    qryResultadonCdDoctoFiscal: TAutoIncField;
    qryResultadocSerie: TStringField;
    qryResultadoiNrDocto: TIntegerField;
    qryResultadodDtEmissao: TDateTimeField;
    qryResultadonCdTerceiro: TIntegerField;
    qryResultadocNmTerceiro: TStringField;
    qryResultadonValTotal: TBCDField;
    qryResultadonValFatura: TBCDField;
    qryResultadoiParcelas: TIntegerField;
    qryResultadodDtImpressao: TDateTimeField;
    cxButton1: TcxButton;
    qryContaBancaria: TADOQuery;
    Label3: TLabel;
    cmd: TADOCommand;
    SP_GERA_LANCTOFIN_EMISSAO_BOLETO: TADOStoredProc;
    qryAux: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancariacNmBanco: TStringField;
    qryContaBancariacAgencia: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancariacNmTitular: TStringField;
    edtContaBancaria: TMaskEdit;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    qryContaBancariaiUltimoBoleto: TIntegerField;
    SP_REGISTRA_NUMBOLETO_NF_TITULO: TADOStoredProc;
    qryResultadocFlgBoletoImpresso: TIntegerField;
    qryResultadocImpresso: TStringField;
    qryResultadonCdBancoPortador: TIntegerField;
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure edtContaBancariaExit(Sender: TObject);
    procedure edtContaBancariaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtContaBancariaChange(Sender: TObject);
    procedure edtTerceiroChange(Sender: TObject);
    procedure edtDtFaturamentoChange(Sender: TObject);
  private
    { Private declarations }
    nCdBanco: integer ;
  public
    { Public declarations }
  end;

var
  frmImpBoletoNF: TfrmImpBoletoNF;

implementation

uses fMenu, fLookup_Padrao, fImpBOLPadrao;

{$R *.dfm}

procedure TfrmImpBoletoNF.cxButton1Click(Sender: TObject);
begin
  inherited;

  if (qryContaBancaria.eof) then
  begin
      MensagemAlerta('Selecione uma conta bancaria para emiss�o.') ;
      edtContaBancaria.SetFocus;
      abort;
  end ;

  if (trim(edtDtFaturamento.Text) = '/  /') then
      edtDtFaturamento.Text := DateToStr(Date) ;

  qryResultado.Close ;
  qryResultado.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryResultado.Parameters.ParamByName('cDtEmissao').Value := edtDtFaturamento.Text ;
  qryResultado.Parameters.ParamByName('cNmTerceiro').Value := edtTerceiro.Text ;
  qryResultado.Open ;

  if qryResultado.eof then
      MensagemAlerta('Nenhum documento encontrado para o crit�rio utilizado.') ;

end;

procedure TfrmImpBoletoNF.FormShow(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  
end;

procedure TfrmImpBoletoNF.DBGridEh1DblClick(Sender: TObject);
var
    cProximoBoleto : string ;
    iProximoBoleto : integer ;
    objForm        : TfrmImpBolPadrao ;
begin
  inherited;

  objForm := TfrmImpBolPadrao.Create(nil) ;

  if not qryResultado.eof then
  begin

      {--se n�o foi impresso ainda --}
      if (qryResultadocFlgBoletoImpresso.Value = 0) then
      begin

          case MessageDlg('Confirma a impress�o dos boletos ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo:exit ;
          end ;

          qryContaBancaria.Requery();

          cProximoBoleto := intToStr(qryContaBancariaiUltimoBoleto.Value +1) ;

          InputQuery('�ltimo Boleto: ' + qryContaBancariaiUltimoBoleto.asString,'N�mero do Pr�ximo Boleto',cProximoBoleto) ;

          try
              strToInt(trim(cProximoBoleto));
          except
              MensagemAlerta('N�mero de boleto inv�lido ou n�o informado.') ;
              abort ;
          end ;

          if (strToInt(trim(cProximoBoleto)) <= 0) then
          begin
              MensagemAlerta('N�mero de boleto inv�lido ou n�o informado.') ;
              abort ;
          end ;

          if (strToint(trim(cProximoBoleto)) <= qryContaBancariaiUltimoBoleto.Value) then
          begin
              MensagemAlerta('O N�mero do pr�ximo boleto n�o pode ser menor ou igual ao �ltimo boleto impresso.') ;
              abort ;
          end ;

          case MessageDlg('Confirma a numera��o informada ?  N�mero Pr�ximo Boleto: ' + cProximoBoleto,mtConfirmation,[mbYes,mbNo],0) of
              mrNo:exit ;
          end ;

          objForm.nCdBanco   := qryContaBancarianCdBanco.Value ;
          objForm.nCdDoctoFiscal := qryResultadonCdDoctoFiscal.Value ;
          objForm.ImprimirBoleto;

          case MessageDlg('Os boletos foram impressos corretamente ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo: begin
                  ShowMessage('Nenhum registro foi atualizado, imprima novamente o boleto.') ;
                  exit ;
              end ;
          end ;

          frmMenu.Connection.BeginTrans;

          try

              // marca o titulo como boleto emitido e atualiza o n�mero do t�tulo
              SP_REGISTRA_NUMBOLETO_NF_TITULO.Close;
              SP_REGISTRA_NUMBOLETO_NF_TITULO.Parameters.ParamByName('@nCdDoctoFiscal').Value   := qryResultadonCdDoctoFiscal.Value;
              SP_REGISTRA_NUMBOLETO_NF_TITULO.Parameters.ParamByName('@nCdBancoPortador').Value := qryContaBancarianCdBanco.Value;
              SP_REGISTRA_NUMBOLETO_NF_TITULO.Parameters.ParamByName('@nCdContaBancaria').Value := qryContaBancarianCdContaBancaria.Value;
              SP_REGISTRA_NUMBOLETO_NF_TITULO.Parameters.ParamByName('@iProximoBoleto').Value   := strToInt(cProximoBoleto);
              SP_REGISTRA_NUMBOLETO_NF_TITULO.ExecProc;

              // gera os lan�amentos financeiros na conta
              SP_GERA_LANCTOFIN_EMISSAO_BOLETO.Close ;
              SP_GERA_LANCTOFIN_EMISSAO_BOLETO.Parameters.ParamByName('@nCdDoctoFiscal').Value   := qryResultadonCdDoctoFiscal.Value ;
              SP_GERA_LANCTOFIN_EMISSAO_BOLETO.Parameters.ParamByName('@nCdBanco').Value         := qryContaBancarianCdBanco.Value;
              SP_GERA_LANCTOFIN_EMISSAO_BOLETO.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado ;
              SP_GERA_LANCTOFIN_EMISSAO_BOLETO.Parameters.ParamByName('@nCdTituloEmissao').Value := 0;
              SP_GERA_LANCTOFIN_EMISSAO_BOLETO.ExecProc;

          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          frmMenu.Connection.CommitTrans;
          qryResultado.Requery(); 
      end
      else
      begin {-- se for reimpress�o --}
      
          case MessageDlg('Deseja reimprimir este(s) boleto(s) ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo:exit ;
          end ;

          objForm.nCdBanco       := qryResultadonCdBancoPortador.Value ;
          objForm.nCdDoctoFiscal := qryResultadonCdDoctoFiscal.Value ;
          objForm.ImprimirBoleto;

      end ;

  end ;

  FreeAndNil(objForm);

end;

procedure TfrmImpBoletoNF.edtContaBancariaExit(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close;
  PosicionaQuery(qryContaBancaria, edtContaBancaria.Text) ;
  
end;

procedure TfrmImpBoletoNF.edtContaBancariaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'cFlgEmiteBoleto = 1 AND ContaBancaria.nCdEmpresa = @@Empresa');

            If (nPK > 0) then
                edtContaBancaria.Text := intToStr(nPK);

    end ;

  end ;

end;

procedure TfrmImpBoletoNF.edtContaBancariaChange(Sender: TObject);
begin
  inherited;

  qryResultado.Close;
end;

procedure TfrmImpBoletoNF.edtTerceiroChange(Sender: TObject);
begin
  inherited;
  qryResultado.Close;

end;

procedure TfrmImpBoletoNF.edtDtFaturamentoChange(Sender: TObject);
begin
  inherited;
  qryResultado.Close;

end;

initialization
    RegisterClass(TfrmImpBoletoNF) ;

end.
