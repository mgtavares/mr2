unit rVendaCrediario_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptVendaCrediario_view = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_RELVENDA_CREDIARIO: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText6: TQRDBText;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    SPREL_RELVENDA_CREDIARIOnCdLoja: TStringField;
    SPREL_RELVENDA_CREDIARIOnCdCrediario: TIntegerField;
    SPREL_RELVENDA_CREDIARIOnCdTerceiro: TIntegerField;
    SPREL_RELVENDA_CREDIARIOcNmTerceiro: TStringField;
    SPREL_RELVENDA_CREDIARIOdDtVenda: TDateTimeField;
    SPREL_RELVENDA_CREDIARIOiParcelas: TIntegerField;
    SPREL_RELVENDA_CREDIARIOnValCrediario: TFloatField;
    SPREL_RELVENDA_CREDIARIOnCdConta: TStringField;
    SPREL_RELVENDA_CREDIARIOcFlgRenegociacao: TStringField;
    SPREL_RELVENDA_CREDIARIOnCdPropostaReneg: TIntegerField;
    SPREL_RELVENDA_CREDIARIOcFlgRestricao: TStringField;
    SPREL_RELVENDA_CREDIARIOnValEntrada: TFloatField;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape2: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel1: TQRLabel;
    QRShape6: TQRShape;
    SPREL_RELVENDA_CREDIARIOcNmTabTipoRestricaoVenda: TStringField;
    SPREL_RELVENDA_CREDIARIOcNmUsuario: TStringField;
    SPREL_RELVENDA_CREDIARIOcJustificativa: TStringField;
    QRLabel13: TQRLabel;
    QRDBText14: TQRDBText;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    SPREL_RELVENDA_CREDIARIOcNmVendedor: TStringField;
    QRDBText15: TQRDBText;
    QRLabel2: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaCrediario_view: TrptVendaCrediario_view;

implementation

{$R *.dfm}

end.
