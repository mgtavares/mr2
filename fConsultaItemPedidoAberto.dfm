inherited frmConsultaItemPedidoAberto: TfrmConsultaItemPedidoAberto
  Left = 221
  Top = 155
  Width = 961
  Height = 517
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Consulta de Item de Pedido '
  KeyPreview = False
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 222
    Width = 945
    Height = 257
  end
  inherited ToolBar1: TToolBar
    Width = 945
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 945
    Height = 193
    ActivePage = cxTabSheet1
    Align = alTop
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 189
    ClientRectLeft = 4
    ClientRectRight = 941
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Pedidos Pendentes'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 937
        Height = 165
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = DataSource1
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh1DblClick
        OnDrawColumnCell = DBGridEh1DrawColumnCell
        OnKeyDown = DBGridEh1KeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <>
            Width = 66
          end
          item
            EditButtons = <>
            FieldName = 'dDtPedido'
            Footers = <>
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoPedido'
            Footers = <>
            Width = 114
          end
          item
            EditButtons = <>
            FieldName = 'dDtPrevEntIni'
            Footers = <>
            Width = 86
          end
          item
            EditButtons = <>
            FieldName = 'dDtPrevEntFim'
            Footers = <>
            Width = 86
          end
          item
            EditButtons = <>
            FieldName = 'cNrPedTerceiro'
            Footers = <>
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'cNmTerceiro'
            Footers = <>
            Width = 83
          end
          item
            EditButtons = <>
            FieldName = 'cNmFantasia'
            Footers = <>
            Width = 91
          end
          item
            EditButtons = <>
            FieldName = 'cNmGrupoEconomico'
            Footers = <>
            Width = 100
          end
          item
            EditButtons = <>
            FieldName = 'cNmContato'
            Footers = <>
            Width = 103
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object cxPageControl2: TcxPageControl [3]
    Left = 0
    Top = 222
    Width = 945
    Height = 257
    ActivePage = cxTabSheet2
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 253
    ClientRectLeft = 4
    ClientRectRight = 941
    ClientRectTop = 24
    object cxTabSheet2: TcxTabSheet
      Caption = 'Itens Pendentes do Pedido Selecionado'
      ImageIndex = 0
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 35
        Width = 937
        Height = 194
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = DataSource2
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh2DblClick
        OnDrawColumnCell = DBGridEh2DrawColumnCell
        OnKeyDown = DBGridEh2KeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            Width = 53
          end
          item
            EditButtons = <>
            FieldName = 'cReferencia'
            Footers = <>
            Width = 92
          end
          item
            EditButtons = <>
            FieldName = 'cNmCor'
            Footers = <>
            Width = 117
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            Width = 399
          end
          item
            EditButtons = <>
            FieldName = 'cSiglaUnidadeMedida'
            Footers = <>
            Width = 26
          end
          item
            EditButtons = <>
            FieldName = 'nQtde'
            Footers = <>
            Width = 89
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeVinculada'
            Footers = <>
            Width = 89
          end
          item
            EditButtons = <>
            FieldName = 'nCdItemPedido'
            Footers = <>
            Width = 48
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoItemPed'
            Footers = <>
            Width = 101
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 937
        Height = 35
        Align = alTop
        TabOrder = 1
        object Label1: TLabel
          Tag = 1
          Left = 5
          Top = 17
          Width = 52
          Height = 13
          Alignment = taRightJustify
          Caption = 'Refer'#234'ncia'
        end
        object Edit1: TEdit
          Left = 60
          Top = 9
          Width = 321
          Height = 21
          TabOrder = 0
          OnChange = Edit1Change
        end
      end
    end
  end
  object qryPedido: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryPedidoAfterScroll
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTipoReceb'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @iDias             int'
      '       ,@cRecebeAtraso     char(1)'
      '       ,@nCdUsuario        int'
      '       ,@nCdLoja           int'
      '       ,@nCdTerceiro       int'
      '       ,@nCdEmpresa        int'
      '       ,@nCdTipoReceb      int'
      '       ,@nCdGrupoEconomico int'
      '       ,@nCdProduto        int'
      ''
      'Set @nCdTerceiro  = :nCdTerceiro'
      'Set @nCdEmpresa   = :nCdEmpresa'
      'Set @nCdTipoReceb = :nCdTipoReceb'
      'Set @nCdUsuario   = :nCdUsuario'
      'Set @nCdLoja      = :nCdLoja'
      'Set @nCdProduto   = :nCdProduto'
      ''
      'SELECT @nCdGrupoEconomico = nCdGrupoEconomico'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = @nCdTerceiro'
      ''
      'SELECT @iDias = Convert(int,IsNull(cValor,'#39'0'#39'))'
      '  FROM Parametro'
      ' WHERE cParametro = '#39'DIAATRASOREC'#39
      ''
      'SELECT @cRecebeAtraso = IsNull(cValor,'#39'S'#39')'
      '  FROM Parametro'
      ' WHERE cParametro = '#39'RECFORAPAZO'#39
      ''
      'IF (@nCdLoja = 0)'
      'BEGIN'
      ''
      #9'SELECT DISTINCT Pedido.nCdPedido'
      #9#9'  ,Pedido.dDtPedido'
      #9#9'  ,CASE WHEN dDtPrevEntIni IS NULL THEN dDtEntregaIni'
      #9#9#9#9'    ELSE dDtPrevEntIni'
      #9#9'   END dDtPrevEntIni'
      #9#9'  ,CASE WHEN dDtPrevEntFim IS NULL THEN dDtEntregaFim'
      #9#9#9#9'    ELSE dDtPrevEntFim'
      #9#9'   END dDtPrevEntFim'
      #9#9'  ,cNrPedTerceiro'
      #9#9'  ,Pedido.cNmContato'
      #9#9'  ,'#39' '#39' cOBS'
      '      ,Terceiro.cNmTerceiro'
      '      ,Terceiro.cNmFantasia'
      '      ,GrupoEconomico.cNmGrupoEconomico'
      '      ,TipoPedido.cNmTipoPedido'
      #9'  FROM Pedido'
      
        '         LEFT  JOIN TipoPedido     ON TipoPedido.nCdTipoPedido  ' +
        '       = Pedido.nCdTipoPedido'
      
        '         LEFT  JOIN Terceiro       ON Terceiro.nCdTerceiro      ' +
        '       = Pedido.nCdTerceiro'
      
        '         LEFT  JOIN GrupoEconomico ON GrupoEconomico.nCdGrupoEco' +
        'nomico = Terceiro.nCdGrupoEconomico'
      
        #9#9'     INNER JOIN ItemPedido     ON ItemPedido.nCdPedido        ' +
        '     = Pedido.nCdPedido'
      #9' WHERE nCdTabStatusPed IN (3,4)'
      #9'   AND (   (Pedido.nCdTerceiro = @nCdTerceiro)'
      '            OR (    @nCdGrupoEconomico IS NOT NULL'
      
        '                AND Terceiro.nCdGrupoEconomico = @nCdGrupoEconom' +
        'ico))'
      #9'   AND nCdEmpresa  = @nCdEmpresa'
      
        #9'   AND (   (@cRecebeAtraso = '#39'N'#39' AND dbo.fn_OnlyDate(GetDate())' +
        ' BETWEEN (dDtPrevEntIni-@iDias) AND (dDtPrevEntFim+@iDias))'
      #9#9#9'OR (@cRecebeAtraso = '#39'S'#39'))'
      #9'   AND EXISTS(SELECT 1'
      #9#9#9#9#9'FROM UsuarioLocalEstoque'
      #9#9#9#9'   WHERE UsuarioLocalEstoque.nCdUsuario      = @nCdUsuario'
      
        #9#9#9#9#9' AND UsuarioLocalEstoque.nCdLocalEstoque = Pedido.nCdEstoqu' +
        'eMov)'
      #9'   AND EXISTS(SELECT 1'
      #9#9#9#9#9'FROM ItemPedido'
      #9#9#9#9'   WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      #9#9#9#9#9' AND (nQtdePed-nQtdeExpRec-nQtdeCanc)>0)'
      #9'   AND EXISTS(SELECT 1'
      #9#9#9#9#9'FROM TipoPedidoTipoReceb TPTR'
      #9#9#9#9'   WHERE TPTR.nCdTipoPedido = Pedido.nCdTipoPedido'
      #9#9#9#9#9' AND TPTR.nCdTipoReceb  = @nCdTipoReceb)'
      '     AND ((@nCdProduto = 0) OR (EXISTS(SELECT 1'
      '                                         FROM ItemPedido'
      
        '                                        WHERE ItemPedido.nCdPedi' +
        'do  = Pedido.nCdPedido'
      
        '                                          AND (ItemPedido.nQtdeP' +
        'ed-ItemPedido.nQtdeExpRec-ItemPedido.nQtdeCanc) > 0'
      
        '                                          AND ItemPedido.nCdProd' +
        'uto = @nCdProduto)))'
      ''
      'END'
      'ELSE'
      'BEGIN'
      ''
      #9'SELECT DISTINCT Pedido.nCdPedido'
      #9#9'  ,Pedido.dDtPedido'
      #9#9'  ,CASE WHEN dDtPrevEntIni IS NULL THEN dDtEntregaIni'
      #9#9#9#9'ELSE dDtPrevEntIni'
      #9#9'   END dDtPrevEntIni'
      #9#9'  ,CASE WHEN dDtPrevEntFim IS NULL THEN dDtEntregaFim'
      #9#9#9#9'ELSE dDtPrevEntFim'
      #9#9'   END dDtPrevEntFim'
      #9#9'  ,cNrPedTerceiro'
      #9#9'  ,Pedido.cNmContato'
      #9#9'  ,'#39' '#39' cOBS'
      '          ,Terceiro.cNmTerceiro'
      '          ,Terceiro.cNmFantasia'
      '          ,GrupoEconomico.cNmGrupoEconomico'
      '          ,TipoPedido.cNmTipoPedido'
      #9'  FROM Pedido'
      
        '           LEFT  JOIN TipoPedido     ON TipoPedido.nCdTipoPedido' +
        '         = Pedido.nCdTipoPedido'
      
        '           LEFT  JOIN Terceiro       ON Terceiro.nCdTerceiro    ' +
        '         = Pedido.nCdTerceiro'
      
        '           LEFT  JOIN GrupoEconomico ON GrupoEconomico.nCdGrupoE' +
        'conomico = Terceiro.nCdGrupoEconomico'
      
        #9#9'   INNER JOIN ItemPedido     ON ItemPedido.nCdPedido          ' +
        '   = Pedido.nCdPedido'
      #9' WHERE nCdTabStatusPed IN (3,4)'
      #9'   AND (   (Pedido.nCdTerceiro = @nCdTerceiro)'
      '            OR (    @nCdGrupoEconomico IS NOT NULL'
      
        '                AND Terceiro.nCdGrupoEconomico = @nCdGrupoEconom' +
        'ico))'
      #9'   AND nCdEmpresa  = @nCdEmpresa'
      
        #9'   AND (   (@cRecebeAtraso = '#39'N'#39' AND dbo.fn_OnlyDate(GetDate())' +
        ' BETWEEN (dDtPrevEntIni-@iDias) AND (dDtPrevEntFim+@iDias))'
      #9#9#9'OR (@cRecebeAtraso = '#39'S'#39'))'
      #9'   AND Pedido.nCdLoja     = @nCdLoja'
      #9'   AND EXISTS(SELECT 1'
      #9#9#9#9#9'FROM ItemPedido'
      #9#9#9#9'   WHERE ItemPedido.nCdPedido = Pedido.nCdPedido'
      #9#9#9#9#9' AND (nQtdePed-nQtdeExpRec-nQtdeCanc)>0)'
      #9'   AND EXISTS(SELECT 1'
      #9#9#9#9#9'FROM TipoPedidoTipoReceb TPTR'
      #9#9#9#9'   WHERE TPTR.nCdTipoPedido = Pedido.nCdTipoPedido'
      #9#9#9#9#9' AND TPTR.nCdTipoReceb  = @nCdTipoReceb)'
      '     AND ((@nCdProduto = 0) OR (EXISTS(SELECT 1'
      '                                         FROM ItemPedido'
      
        '                                        WHERE ItemPedido.nCdPedi' +
        'do  = Pedido.nCdPedido'
      
        '                                          AND (ItemPedido.nQtdeP' +
        'ed-ItemPedido.nQtdeExpRec-ItemPedido.nQtdeCanc) > 0'
      
        '                                          AND ItemPedido.nCdProd' +
        'uto = @nCdProduto)))'
      ''
      'END')
    Left = 592
    Top = 128
    object qryPedidonCdPedido: TIntegerField
      DisplayLabel = 'Pedido'
      FieldName = 'nCdPedido'
    end
    object qryPedidodDtPedido: TDateTimeField
      DisplayLabel = 'Data Pedido'
      FieldName = 'dDtPedido'
    end
    object qryPedidodDtPrevEntIni: TDateTimeField
      DisplayLabel = 'Previs'#227'o Entrega|Inicial'
      FieldName = 'dDtPrevEntIni'
    end
    object qryPedidodDtPrevEntFim: TDateTimeField
      DisplayLabel = 'Previs'#227'o Entrega|Final'
      FieldName = 'dDtPrevEntFim'
    end
    object qryPedidocNrPedTerceiro: TStringField
      DisplayLabel = 'N'#250'mero Pedido|Terceiro'
      FieldName = 'cNrPedTerceiro'
      Size = 15
    end
    object qryPedidocNmContato: TStringField
      DisplayLabel = 'Contato'
      FieldName = 'cNmContato'
      Size = 35
    end
    object qryPedidocOBS: TStringField
      FieldName = 'cOBS'
      ReadOnly = True
      Size = 1
    end
    object qryPedidocNmTerceiro: TStringField
      DisplayLabel = 'Terceiro|Raz'#227'o Social'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryPedidocNmFantasia: TStringField
      DisplayLabel = 'Terceiro|Nome Fantasia'
      FieldName = 'cNmFantasia'
      Size = 50
    end
    object qryPedidocNmGrupoEconomico: TStringField
      DisplayLabel = 'Terceiro|Grupo Econ'#244'mico'
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
    object qryPedidocNmTipoPedido: TStringField
      DisplayLabel = 'Tipo Pedido'
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryPedido
    Left = 560
    Top = 120
  end
  object qryItemPedido: TADOQuery
    Connection = frmMenu.Connection
    Filtered = True
    Parameters = <
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdRecebimento'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdProduto     int'
      '       ,@nCdRecebimento int'
      ''
      'Set @nCdProduto     = :nCdProduto'
      'Set @nCdRecebimento = :nCdRecebimento'
      ''
      'SELECT ItemPedido.nCdItemPedido'
      '      ,TipoItemPed.cNmTipoItemPed'
      '      ,ItemPedido.nCdProduto'
      '      ,ItemPedido.cNmItem'
      
        '      ,(ItemPedido.nQtdePed - ItemPedido.nQtdeExpRec - ItemPedid' +
        'o.nQtdeCanc) nQtde'
      '      ,(IsNull((SELECT Sum(nQtde)'
      '                  FROM ItemRecebimento'
      '                 WHERE nCdRecebimento = @nCdRecebimento'
      
        '                   AND nCdItemPedido  = ItemPedido.nCdItemPedido' +
        '),0)) as nQtdeVinculada'
      '      ,ItemPedido.cSiglaUnidadeMedida'
      '      ,Produto.cReferencia'
      '      ,ItemPedido.cCdProduto'
      '      ,Cor.cNmCor'
      '  FROM ItemPedido'
      
        '       INNER JOIN TipoItemPed ON TipoItemPed.nCdTipoItemPed = It' +
        'emPedido.nCdTipoItemPed'
      
        '       LEFT  JOIN Produto     ON Produto.nCdProduto         = It' +
        'emPedido.nCdProduto'
      
        '       LEFT  JOIN Cor         ON Cor.nCdCor                 = Pr' +
        'oduto.nCdCor'
      
        ' WHERE (ItemPedido.nQtdePed - ItemPedido.nQtdeExpRec - ItemPedid' +
        'o.nQtdeCanc) > 0'
      '   AND ItemPedido.nCdItemPedidoPai IS NULL'
      '   AND ItemPedido.nCdPedido = :nPK'
      
        '   AND ((@nCdProduto = 0) OR (ItemPedido.nCdProduto = @nCdProdut' +
        'o))'
      '')
    Left = 608
    Top = 240
    object qryItemPedidonCdItemPedido: TAutoIncField
      DisplayLabel = 'Item|ID'
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryItemPedidocNmTipoItemPed: TStringField
      DisplayLabel = 'Item|Tipo de Item'
      FieldName = 'cNmTipoItemPed'
      Size = 50
    end
    object qryItemPedidonCdProduto: TIntegerField
      DisplayLabel = 'Item|C'#243'digo'
      FieldName = 'nCdProduto'
    end
    object qryItemPedidocNmItem: TStringField
      DisplayLabel = 'Item|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemPedidonQtde: TBCDField
      DisplayLabel = 'Quantidade|Saldo Pedido'
      FieldName = 'nQtde'
      ReadOnly = True
      DisplayFormat = ',0'
      Precision = 14
    end
    object qryItemPedidocSiglaUnidadeMedida: TStringField
      DisplayLabel = 'Quantidade|UM'
      FieldName = 'cSiglaUnidadeMedida'
      FixedChar = True
      Size = 2
    end
    object qryItemPedidocReferencia: TStringField
      DisplayLabel = 'Item|Refer'#234'ncia'
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object qryItemPedidocCdProduto: TStringField
      FieldName = 'cCdProduto'
      Size = 15
    end
    object qryItemPedidonQtdeVinculada: TBCDField
      DisplayLabel = 'Quantidade|Vinculada'
      FieldName = 'nQtdeVinculada'
      ReadOnly = True
      DisplayFormat = ',0'
      Precision = 32
    end
    object qryItemPedidocNmCor: TStringField
      DisplayLabel = 'Item|Cor'
      FieldName = 'cNmCor'
      Size = 100
    end
  end
  object DataSource2: TDataSource
    DataSet = qryItemPedido
    Left = 568
    Top = 240
  end
end
