inherited frmBanco: TfrmBanco
  Caption = 'frmBanco'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 35
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 62
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome Banco'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Left = 76
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdBanco'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 76
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmBanco'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM BANCO'
      'WHERE nCdBanco = :nPK')
    object qryMasternCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryMastercNmBanco: TStringField
      FieldName = 'cNmBanco'
      Size = 50
    end
  end
end
