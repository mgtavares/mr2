unit fSelecEmpresa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxLookAndFeelPainters, StdCtrls, cxButtons, DB, ADODB,
  GridsEh, DBGridEh, jpeg, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmSelecEmpresa = class(TForm)
    DBGridEh1: TDBGridEh;
    qryEmpresas: TADOQuery;
    dsEmpresas: TDataSource;
    qryEmpresasnCdUsuario: TIntegerField;
    qryEmpresasnCdEmpresa: TIntegerField;
    qryEmpresascSigla: TStringField;
    qryEmpresascNmEmpresa: TStringField;
    qryEmpresascNmFantasia: TStringField;
    Panel1: TPanel;
    cxButton1: TcxButton;
    procedure FormCreate(Sender: TObject);
    function SelecionaEmpresa(nCdUsuario:Integer) : Integer ;
    procedure cxButton1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecEmpresa: TfrmSelecEmpresa;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmSelecEmpresa.FormCreate(Sender: TObject);
var
    i : Integer;
begin
  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TDBGridEh) then
    begin
      (Components [I] as TDBGridEh).Flat := True ;
      (Components [I] as TDBGridEh).FixedColor := clMoneyGreen ;
      (Components [I] as TDBGridEh).OddRowColor := clAqua ;
      (Components [I] as TDBGridEh).UseMultiTitle := True ;

      if ((Components [I] as TDBGridEh).Tag = 1) then
      begin
          (Components [I] as TDBGridEh).ParentFont := False ;
          (Components [I] as TDBGridEh).ReadOnly   := True ;
          (Components [I] as TDBGridEh).Color      := clSilver ;
          (Components [I] as TDBGridEh).Font.Color := clYellow ;
          Update;
      end ;

    end;

    if (Components [I] is TEdit) then
    begin
      (Components [I] as TEdit).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      (Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 15 ;
      (Components [I] as TEdit).Font.Name   := 'Tahoma' ;
      (Components [I] as TEdit).Font.Size   := 7 ;
      (Components [I] as TEdit).Font.Style  := [] ;

    end;

    if (Components [I] is TLabel) then
    begin
       (Components [I] as TLabel).Font.Name    := 'Tahoma' ;
       (Components [I] as TLabel).Font.Size    := 8 ;
       (Components [I] as TLabel).Font.Style   := [] ;
       (Components [I] as TLabel).Font.Color   := clBlack ;
       (Components [I] as TLabel).Transparent  := True ;
       (Components [I] as TLabel).BringToFront;
    end ;

    if (Components [I] is TButton) then
    begin
       (Components [I] as TButton).ParentFont := False ;
       (Components [I] as TButton).Font.Name  := 'Arial' ;
       (Components [I] as TButton).Font.Size  := 8 ;
       (Components [I] as TButton).Font.Color := clBlue ;
       (Components [I] as TButton).Default    := False ;
       (Components [I] as TButton).Update ;
    end ;
  end ;

end;

function TfrmSelecEmpresa.SelecionaEmpresa(nCdUsuario:Integer) : Integer ;
begin

    qryEmpresas.Close ;
    qryEmpresas.Parameters.ParamByName('nCdUsuario').Value := nCdUsuario ;
    qryEmpresas.Open  ;

    if (qryEmpresas.eof) then
    begin
        ShowMessage('Nenhuma empresa ativa vinculada para o usu�rio.') ;
        Result := 0 ;
        exit ;
    end ;

    if (qryEmpresas.RecordCount = 1) then
    begin
        qryEmpresas.First;
        cxButton1.Click;
        Result := qryEmpresasnCdEmpresa.Value ;
        frmMenu.AlterarEmpresa1.Enabled := False ;
        exit ;
    end ;

    frmSelecEmpresa.ShowModal ;

    Result := qryEmpresasnCdEmpresa.Value ;
end ;

procedure TfrmSelecEmpresa.cxButton1Click(Sender: TObject);
begin
    frmMenu.StatusBar1.Panels[1].Text := qryEmpresascSigla.Value ;

    frmMenu.cNmEmpresaAtiva    := qryEmpresascNmEmpresa.Value ;
    frmMenu.cNmFantasiaEmpresa := qryEmpresascNmFantasia.Value ;
    frmMenu.Caption := trim(qryEmpresascNmEmpresa.Value) + '    ___::: www.er2soft.com.br :::___' ;

    close ;
end;

procedure TfrmSelecEmpresa.DBGridEh1DblClick(Sender: TObject);
begin
    frmMenu.StatusBar1.Panels[1].Text := qryEmpresascSigla.Value ;

    frmMenu.cNmEmpresaAtiva    := qryEmpresascNmEmpresa.Value ;
    frmMenu.cNmFantasiaEmpresa := qryEmpresascNmFantasia.Value ;
    frmMenu.Caption := trim(qryEmpresascNmEmpresa.Value) + '    ___::: www.er2soft.com.br :::___' ;

    close ;

end;

procedure TfrmSelecEmpresa.FormShow(Sender: TObject);
begin
    DbGridEh1.SetFocus;
end;

procedure TfrmSelecEmpresa.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case key of
    vk_RETURN : cxButton1.Click;
  end ;

end;

end.
