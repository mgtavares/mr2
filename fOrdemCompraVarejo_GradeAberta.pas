unit fOrdemCompraVarejo_GradeAberta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, ADODB, DB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh,
  cxPC, cxControls;

type
  TfrmOrdemCompraVarejo_GradeAberta = class(TfrmProcesso_Padrao)
    qryLoja: TADOQuery;
    dsLoja: TDataSource;
    qryPreparaTemp: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cmdPreparaTemp: TADOCommand;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nQtdeTotalPares : integer ;
    bGrade : boolean ;
    nCdItemPedido : integer ;
    nCdProduto : integer;
  end;

var
  frmOrdemCompraVarejo_GradeAberta: TfrmOrdemCompraVarejo_GradeAberta;

implementation

uses
    fMontaGrade;

{$R *.dfm}

procedure TfrmOrdemCompraVarejo_GradeAberta.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
//  inherited;

end;

procedure TfrmOrdemCompraVarejo_GradeAberta.ToolButton1Click(Sender: TObject);
var
    iQtde, iQtdeLoja, i : integer ;
begin
  inherited;

  iQtdeLoja := 0 ;

  // conta a quantidade de grades nas lojas
  qryLoja.First;

  while not qryLoja.Eof do
  begin
  //    iQtdeLoja := iQtdeLoja + qryLojanQtdeGrade.Value ;

      qryLoja.Next ;
  end ;

  qryLoja.First ;

  if (iQtdeLoja = 0) then
  begin
      MensagemAlerta('Informe a quantidade de grades por loja.') ;
      abort ;
  end ;

  nQtdeTotalPares := iQtdeLoja ;

  close ;

end;

procedure TfrmOrdemCompraVarejo_GradeAberta.FormShow(Sender: TObject);
var
  i : integer;
  nQtdeColumn : integer;
begin
  inherited;

  qryPreparaTemp.Close ;
  qryPreparaTemp.Parameters.ParamByName('nCdItemPedido').Value := nCdItemPedido ;
  qryPreparaTemp.Parameters.ParamByName('nCdProduto').Value := nCdProduto ;
  qryPreparaTemp.Open;

  nQtdeColumn := qryPreparaTemp.FieldCount;

  qryLoja.Close ;
  //qryLoja.Parameters.ParamByName('nCdItemPedido').Value := nCdItemPedido ;
  //qryLoja.Parameters.ParamByName('nCdProduto').Value := nCdProduto ;
  qryLoja.Open ;

  qryLoja.DisableControls;
  qryLoja.FieldList.Update;

  DBGridEh1.Columns.AddAllColumns(true);

  {-- faz tratamento das colunas --}

  DBGridEh1.Columns[0].Visible := false;
  DBGridEh1.Columns[1].Visible := false;


  DBGridEh1.Columns[2].ReadOnly := true;
  DBGridEh1.Columns[2].Title.Caption := 'Loja|Descri��o';
  DBGridEh1.Columns[2].Color := $00E9E4E4;
  DBGridEh1.Columns[2].Width := 100;

  for i := 3 to DBGridEh1.Columns.Count -1 do
  begin

      DBGridEh1.Columns[i].Title.Caption := 'Grade|' + StringReplace(DBGridEh1.Columns[i].Title.Caption,'grade_','',[rfReplaceAll,rfIgnoreCase]);
  end;
  
  {-- reabilita query --}
  qryLoja.EnableControls;


end;

procedure TfrmOrdemCompraVarejo_GradeAberta.DBGridEh1DblClick(
  Sender: TObject);
var
    objGrade : TFrmMontaGrade;
begin
  inherited;

  (*objGrade := TFrmMontaGrade.Create(nil);
  
  objGrade.qryValorAnterior.SQL.Text := '' ;

  objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtdePed FROM ItemPedido WHERE nCdTipoItemPed = 4 AND nCdItemPedidoPai = ' + IntToStr(nCdItemPedido) + ' ORDER BY ncdProduto') ;

  objGrade.PreparaDataSet(nCdProduto);
  objGrade.Renderiza;
  *)
  

end;

end.
