unit fGerarGrade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBCtrls, ADODB, StdCtrls, Mask, cxControls, cxContainer, cxEdit,
  cxTextEdit, cxCurrencyEdit, GridsEh, DBGridEh, cxLookAndFeelPainters,
  cxButtons, DBGridEhGrouping, ToolCtrlsEh, dxCntner, dxEditor, dxExEdtr,
  dxEdLib;

type
  TfrmGerarGrade = class(TfrmProcesso_Padrao)
    qryCor: TADOQuery;
    qryCornCdCor: TAutoIncField;
    qryCorcNmCor: TStringField;
    qryCorcNmCorPredom: TStringField;
    dsCor: TDataSource;
    qryMaterial: TADOQuery;
    qryMaterialnCdMaterial: TAutoIncField;
    qryMaterialnCdMarca: TIntegerField;
    qryMaterialcNmMaterial: TStringField;
    qryMaterialnCdStatus: TIntegerField;
    dsMaterial: TDataSource;
    usp_Gera_Grade: TADOStoredProc;
    DBGridEh1: TDBGridEh;
    cmdCriarTemp: TADOCommand;
    dsTemp: TDataSource;
    qryTemp: TADOQuery;
    qryTempnCdCor: TIntegerField;
    qryTempcCor: TStringField;
    qryTempcMaterial: TStringField;
    qryTempnValCusto: TBCDField;
    qryTempnValVenda: TBCDField;
    cmdAddItem: TADOCommand;
    qryCorAux: TADOQuery;
    qryMaterialAux: TADOQuery;
    qryCorAuxcNmCor: TStringField;
    qryMaterialAuxcNmMaterial: TStringField;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    btAdd: TcxButton;
    Edit5: TEdit;
    btLimpaCor: TcxButton;
    btLimpaMat: TcxButton;
    Edit6: TEdit;
    Edit7: TEdit;
    dxCurValCusto: TdxCurrencyEdit;
    dxCurValVenda: TdxCurrencyEdit;
    procedure GerarGrade;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PosicionaCor(cCor:String) ;
    procedure PosicionaMaterial(cMaterial:String) ;
    procedure Edit1Exit(Sender: TObject);
    procedure Edit3Exit(Sender: TObject);
    procedure btLimpaMatClick(Sender: TObject);
    procedure btLimpaCorClick(Sender: TObject);
    procedure btAddClick(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
    nCdProduto : integer ;
  end;

var
  frmGerarGrade: TfrmGerarGrade;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmGerarGrade.GerarGrade;
begin

  case MessageDlg('Confirma a gera��o desta grade de itens ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo: Exit;
  end;

  if (dxCurValCusto.Value > dxCurValVenda.Value) then
  begin

    case MessageDlg('O valor de custo � maior que o valor de venda. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo: Exit;
    end;

  end ;

  frmMenu.Connection.BeginTrans ;

  try
      usp_Gera_Grade.Close ;
      usp_Gera_Grade.Parameters.ParamByName('@nCdProduto').Value  := nCdProduto ;
      usp_Gera_Grade.Parameters.ParamByName('@nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
      usp_Gera_Grade.ExecProc;
  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processo de gera��o de grade!') ;
      raise ;
      exit ;
  end ;

  frmMenu.Connection.CommitTrans ;

  ShowMessage('Grade Gerada com Sucesso!') ;

  qryCor.Close ;
  qryMaterial.Close ;

end ;

procedure TfrmGerarGrade.ToolButton1Click(Sender: TObject);
begin
    if (qryTemp.IsEmpty) then
    begin
        MensagemAlerta('Nenhum item configurado na lista de grades.');
        Abort;
    end;

    GerarGrade();
    Close;
end;

procedure TfrmGerarGrade.FormShow(Sender: TObject);
begin
  inherited;

  Edit1.Text := '' ;
  Edit2.Text := '' ;
  Edit3.Text := '' ;
  Edit4.Text := '' ;
  Edit5.Text := '' ;
  Edit6.Text := '0' ;
  Edit7.Text := '0' ;
  
  cmdCriarTemp.Execute ;

  qryTemp.Close ;
  qryTemp.Open ;

  Edit3.SetFocus ;
  
end;

procedure TfrmGerarGrade.PosicionaCor(cCor:String) ;
begin

    if (Trim(cCor) = '') then
        exit ;

    if (frmMenu.LeParametro('CONSISTIRCOR') = 'S') then
    begin

        qryCor.Close ;
        qryCor.Parameters.ParamByName('nPK').Value := cCor ;
        qryCor.Open ;

        if qryCor.Eof then
        begin
            MensagemAlerta('Cor n�o cadastrada. Favor verificar.') ;
            Edit1.SetFocus ;
            abort ;
        end ;

    end ;

    if (Trim(Edit2.Text) <> '') then
        Edit2.Text := Edit2.Text + '/' + Trim(cCor)
    else begin
        Edit2.Text := Trim(cCor) ;

        if (qryCor.Active) then
            Edit5.Text := qryCornCdCor.AsString
        else Edit5.Text := '1' ;

    end ;

    Edit1.Text := '' ;
    Edit7.Text := IntToStr(StrToInt(Edit7.Text) + 1) ;
    Edit1.SetFocus ;

end ;

procedure TfrmGerarGrade.PosicionaMaterial(cMaterial:String) ;
begin

    if (Trim(cMaterial) = '') then
        exit ;

    if (frmMenu.LeParametro('CONSISTIRMAT') = 'S') then
    begin
        qryMaterial.Close ;
        qryMaterial.Parameters.ParamByName('nPK').Value := cMaterial ;
        qryMaterial.Open ;

        if qryMaterial.Eof then
        begin
            MensagemAlerta('Material n�o cadastrado para esta marca. Favor verificar.') ;
            Edit3.SetFocus ;
            abort ;
        end ;
    end ;

    if (Trim(Edit4.Text) <> '') then
        Edit4.Text := Edit4.Text + '/' + TRIM(cMaterial)
    else
        Edit4.Text := TRIM(cMaterial) ;

    Edit3.Text := '' ;
    Edit6.Text := IntToStr(StrToInt(Edit6.Text) + 1) ;
    Edit3.SetFocus ;

end ;

procedure TfrmGerarGrade.Edit1Exit(Sender: TObject);
begin
  inherited;
  PosicionaCor(Edit1.Text)
end;

procedure TfrmGerarGrade.Edit3Exit(Sender: TObject);
begin
  inherited;
  PosicionaMaterial(Edit3.Text) ;
end;

procedure TfrmGerarGrade.btLimpaMatClick(Sender: TObject);
begin
  inherited;
  Edit4.Text := '' ;
  Edit6.Text := '0' ;
  Edit3.SetFocus ;
end;

procedure TfrmGerarGrade.btLimpaCorClick(Sender: TObject);
begin
  inherited;
  Edit2.Text := '' ;
  Edit5.Text := '' ;
  Edit7.Text := '0' ;
  Edit1.SetFocus ;
end;

procedure TfrmGerarGrade.btAddClick(Sender: TObject);
begin
  inherited;

  if (Edit4.Text = '') then
  begin
      MensagemAlerta('Selecione o material.') ;
      Edit3.SetFocus;
      exit ;
  end ;

  if (Edit2.Text = '') then
  begin
      MensagemAlerta('Selecione a cor.') ;
      Edit1.SetFocus;
      exit ;
  end ;

  if (strToInt(Edit6.Text) <> strToInt(Edit7.Text)) then
  begin
      MensagemAlerta('A quantidade de cores e materiais devem ser as mesmas.') ;
      Edit3.SetFocus;
      exit ;
  end ;

  if (dxCurValVenda.Value < dxCurValCusto.Value) then
  begin
      if (MessageDlg('O pre�o de venda est� menor que o pre�o de custo. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      begin
          dxCurValCusto.SetFocus;
          exit ;
      end ;

  end ;

  try
    cmdAddItem.Parameters.ParamByName('nCdCor').Value    := StrToInt(Edit5.Text) ;
    cmdAddItem.Parameters.ParamByName('cCor').Value      := Edit2.Text ;
    cmdAddItem.Parameters.ParamByName('cMaterial').Value := Edit4.Text ;
    cmdAddItem.Parameters.ParamByName('nValCusto').Value := dxCurValCusto.Value;
    cmdAddItem.Parameters.ParamByName('nValVenda').Value := dxCurValVenda.Value;
    cmdAddItem.Execute;
  except
    MensagemErro('Erro no processamento') ;
    raise ;
  end ;

  Edit1.Text := '' ;
  Edit2.Text := '' ;
  Edit3.Text := '' ;
  Edit4.Text := '' ;
  Edit5.Text := '' ;
  Edit6.Text := '0' ;
  Edit7.Text := '0' ;

  qryTemp.Close ;
  qryTemp.Open ;

  Edit3.SetFocus ;

end;

procedure TfrmGerarGrade.Edit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(43);

        If (nPK > 0) then
        begin
            PosicionaQuery(qryCorAux,IntToStr(nPK)) ;
            Edit1.Text := Trim(qryCorAuxcNmCor.Value) ;
        end ;

    end ;

  end ;

end;

procedure TfrmGerarGrade.Edit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(44,'nCdMarca = ' + IntToStr(qryMaterial.Parameters.ParamByName('nCdMarca').Value));

        If (nPK > 0) then
        begin
            PosicionaQuery(qryMaterialAux,IntToStr(nPK)) ;
            Edit3.Text := Trim(qryMaterialAuxcNmMaterial.Value) ;
        end ;

    end ;

  end ;

end;

end.

