inherited frmAcompanhaGiroProduto_view: TfrmAcompanhaGiroProduto_view
  Left = -8
  Top = -8
  Width = 1152
  Height = 864
  Caption = 'Acompanhamento Giro Produto'
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1136
    Height = 799
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    ButtonWidth = 68
    inherited ToolButton1: TToolButton
      Caption = 'Imprimir'
      ImageIndex = 2
      Visible = False
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 68
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Left = 76
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 799
    Align = alClient
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnHiding = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLineColor = clSilver
      OptionsView.GridLines = glVertical
      OptionsView.GroupFooters = gfAlwaysVisible
      Styles.Content = frmMenu.FonteSomenteLeitura
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1cNmMarca: TcxGridDBColumn
        Caption = 'Marca'
        DataBinding.FieldName = 'cNmMarca'
        Width = 100
      end
      object cxGrid1DBTableView1nCdProduto: TcxGridDBColumn
        DataBinding.FieldName = 'nCdProduto'
      end
      object cxGrid1DBTableView1nCdTabTipoProduto: TcxGridDBColumn
        Caption = 'C'#243'd'
        DataBinding.FieldName = 'nCdTabTipoProduto'
        Visible = False
      end
      object cxGrid1DBTableView1cNmProduto: TcxGridDBColumn
        Caption = 'Descri'#231#227'o Produto'
        DataBinding.FieldName = 'cNmProduto'
        Width = 397
      end
      object cxGrid1DBTableView1nQtdeEstoqueAtual: TcxGridDBColumn
        Caption = 'Estoque Atual'
        DataBinding.FieldName = 'nQtdeEstoqueAtual'
        Width = 115
      end
      object cxGrid1DBTableView1iDiasEstoqueMedio: TcxGridDBColumn
        Caption = 'Idade Estoque'
        DataBinding.FieldName = 'iDiasEstoqueMedio'
        Width = 119
      end
      object cxGrid1DBTableView1nQtdeVendaPeriodo: TcxGridDBColumn
        Caption = 'Vda Per'#237'odo'
        DataBinding.FieldName = 'nQtdeVendaPeriodo'
        Width = 102
      end
      object cxGrid1DBTableView1nGiroPeriodo: TcxGridDBColumn
        Caption = 'Giro Per'#237'odo'
        DataBinding.FieldName = 'nGiroPeriodo'
        Width = 108
      end
      object cxGrid1DBTableView1nQtdeVendaTotal: TcxGridDBColumn
        Caption = 'Vda Total'
        DataBinding.FieldName = 'nQtdeVendaTotal'
        Width = 92
      end
      object cxGrid1DBTableView1nGiroTotal: TcxGridDBColumn
        Caption = 'Giro Total'
        DataBinding.FieldName = 'nGiroTotal'
        Width = 98
      end
      object cxGrid1DBTableView1nPercVenda: TcxGridDBColumn
        Caption = '% Vendido'
        DataBinding.FieldName = 'nPercVenda'
        Width = 93
      end
      object cxGrid1DBTableView1nPercVendaIdeal: TcxGridDBColumn
        Caption = '% Vda Ideal'
        DataBinding.FieldName = 'nPercVendaIdeal'
        Width = 104
      end
      object cxGrid1DBTableView1cNmDepartamento: TcxGridDBColumn
        Caption = 'Departamento'
        DataBinding.FieldName = 'cNmDepartamento'
        Width = 113
      end
      object cxGrid1DBTableView1cNmCategoria: TcxGridDBColumn
        Caption = 'Categoria'
        DataBinding.FieldName = 'cNmCategoria'
        Width = 100
      end
      object cxGrid1DBTableView1cNmSubCategoria: TcxGridDBColumn
        Caption = 'SubCategoria'
        DataBinding.FieldName = 'cNmSubCategoria'
        Width = 119
      end
      object cxGrid1DBTableView1cNmSegmento: TcxGridDBColumn
        Caption = 'Segmento'
        DataBinding.FieldName = 'cNmSegmento'
        Width = 100
      end
      object cxGrid1DBTableView1cNmMarca_1: TcxGridDBColumn
        Caption = 'C'#243'd'
        DataBinding.FieldName = 'cNmMarca_1'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 440
    Top = 232
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009E9E
      9E00656464006564640065646400656464006564640065646400656464006564
      6400898A8900B9BABA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000D5D5D5005157
      570031333200484E4E00313332003D4242003D4242003D4242003D424200191D
      230051575700C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000000000008C908F00191D
      230031333200313332003133320031333200313332003133320031333200191D
      230004050600898A890000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      840000008400000084000000000000000000000000000000000065646400898A
      8900DADDD700B9BABA00BEBEBE00BEBEBE00BEBEBE00BEBEBE00BEBABC00CCCC
      CC00515757006564640000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000000000000717272003D42
      4200898A8900777B7B007B8585007B8585007B8585007B8585007B8585009392
      92003D424200777B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000B6B6B6003133
      32007B858500AEAEAE00A4A8A800A4A8A800A4A8A800A4A8A800A4A8A800484E
      4E0031333200B9BABA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000000000000D5D5D500484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00898A
      8900484E4E00C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE007B85
      85003D4242000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF000000840000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00D5D5D5007172
      72003D4242000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE007B858500191D2300191D
      230051575700C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE0071727200484E4E003D42
      420093929200C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000CCCCCC00484E
      4E0031333200575D5E005157570051575700575D5E00191D2300191D23009392
      9200CCCCCC00BEBEBE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B6B6
      B6009E9E9E009E9E9E009E9E9E009E9E9E009E9E9E00A4A8A800AEAEAE00C6C6
      C600BEBEBE00BEBEBE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFFF0000
      FE00C003E003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C003000000008001C003000000008001C0070000
      00008001C007000000008001C003000000018001C003000000038001C0030000
      0077C003E0030000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object SPREL_ACOMPANHA_GIRO_PRODUTO: TADOStoredProc
    Connection = frmMenu.Connection
    CursorType = ctStatic
    CommandTimeout = 0
    EnableBCD = False
    ProcedureName = 'SPREL_ACOMPANHA_GIRO_PRODUTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@dDtFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@iGiroIdeal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iDiasSemEstoque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdDepartamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdCategoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdSubCategoria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdSegmento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdMarca'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLinha'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdColecao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdClasse'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdGrupoProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cReferencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@iDiasEstoqueIni'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iDiasEstoqueFim'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNivelAgrupamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iOrdenacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iTipoOrdenacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 360
    Top = 224
    object SPREL_ACOMPANHA_GIRO_PRODUTOnCdProduto: TIntegerField
      DisplayLabel = 'C'#243'd'
      FieldName = 'nCdProduto'
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOnCdTabTipoProduto: TIntegerField
      DisplayLabel = 'Acompanhamento de Giro de Produtos|'
      FieldName = 'nCdTabTipoProduto'
      Visible = False
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOcNmProduto: TStringField
      DisplayLabel = 'Acompanhamento de Giro de Produtos|Descri'#231#227'o Produto'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOcNmMarca: TStringField
      DisplayLabel = 'Acompanhamento de Giro de Produtos|Marca'
      FieldName = 'cNmMarca'
      Size = 50
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOnQtdeEstoqueAtual: TFloatField
      DisplayLabel = 'Acompanhamento de Giro de Produtos|Dados Estoque|Saldo Atual'
      FieldName = 'nQtdeEstoqueAtual'
      DisplayFormat = ',0'
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOiDiasEstoqueMedio: TIntegerField
      DisplayLabel = 'Acompanhamento de Giro de Produtos|Dados Estoque|Idade M'#233'dia'
      FieldName = 'iDiasEstoqueMedio'
      DisplayFormat = ',0'
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOnQtdeVendaPeriodo: TFloatField
      DisplayLabel = 'Acompanhamento de Giro de Produtos|Dados do Per'#237'odo|Venda'
      FieldName = 'nQtdeVendaPeriodo'
      DisplayFormat = ',0'
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOnGiroPeriodo: TFloatField
      DisplayLabel = 'Acompanhamento de Giro de Produtos|Dados do Per'#237'odo|Giro'
      FieldName = 'nGiroPeriodo'
      DisplayFormat = '#,##0.00'
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOnQtdeVendaTotal: TFloatField
      DisplayLabel = 
        'Acompanhamento de Giro de Produtos|Dados desde '#218'ltima Compra|Ven' +
        'da'
      FieldName = 'nQtdeVendaTotal'
      DisplayFormat = ',0'
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOnGiroTotal: TFloatField
      DisplayLabel = 
        'Acompanhamento de Giro de Produtos|Dados desde '#218'ltima Compra|Gir' +
        'o'
      FieldName = 'nGiroTotal'
      DisplayFormat = '#,##0.00'
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOnPercVenda: TFloatField
      DisplayLabel = 
        'Acompanhamento de Giro de Produtos|Dados desde '#218'ltima Compra|% V' +
        'da'
      FieldName = 'nPercVenda'
      DisplayFormat = '#,##0.00'
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOnPercVendaIdeal: TFloatField
      DisplayLabel = 
        'Acompanhamento de Giro de Produtos|Dados desde '#218'ltima Compra|% V' +
        'da Ideal'
      FieldName = 'nPercVendaIdeal'
      DisplayFormat = '#,##0.00'
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOcNmDepartamento: TStringField
      DisplayLabel = 'Acompanhamento de Giro de Produtos|Departamento'
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOcNmCategoria: TStringField
      DisplayLabel = 'Acompanhamento de Giro de Produtos|Categoria'
      FieldName = 'cNmCategoria'
      Size = 50
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOcNmSubCategoria: TStringField
      DisplayLabel = 'Acompanhamento de Giro de Produtos|SubCategoria'
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOcNmSegmento: TStringField
      DisplayLabel = 'Acompanhamento de Giro de Produtos|Segmento'
      FieldName = 'cNmSegmento'
      Size = 50
    end
    object SPREL_ACOMPANHA_GIRO_PRODUTOcNmMarca_1: TStringField
      DisplayLabel = 'Acompanhamento de Giro de Produtos|'
      FieldName = 'cNmMarca_1'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = SPREL_ACOMPANHA_GIRO_PRODUTO
    Left = 392
    Top = 224
  end
end
