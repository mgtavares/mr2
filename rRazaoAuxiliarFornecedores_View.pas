unit rRazaoAuxiliarFornecedores_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, QRCtrls, jpeg, DB, ADODB;

type
  TrptRazaoAuxiliarFornecedores_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRExpr6: TQRExpr;
    QRShape4: TQRShape;
    QRExpr2: TQRExpr;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRExpr9: TQRExpr;
    QRBand5: TQRBand;
    QRLabel14: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRImage1: TQRImage;
    QRBand3: TQRBand;
    QRDBText16: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText5: TQRDBText;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel15: TQRLabel;
    Cancel: TQRShape;
    QRLabel26: TQRLabel;
    uspRelatorio: TADOStoredProc;
    uspRelatorionCdContador: TAutoIncField;
    uspRelatorionCdTerceiro: TIntegerField;
    uspRelatoriocNmTerceiro: TStringField;
    uspRelatorionCdTitulo: TIntegerField;
    uspRelatoriodDtMovto: TDateTimeField;
    uspRelatoriocNrNF: TStringField;
    uspRelatorionCdParc: TIntegerField;
    uspRelatoriocNrTit: TStringField;
    uspRelatoriocNmOperacao: TStringField;
    uspRelatoriocHistorico: TStringField;
    uspRelatorionValCredito: TBCDField;
    uspRelatorionValDebito: TBCDField;
    uspRelatorionSaldo: TBCDField;
    uspRelatoriocDocumento: TStringField;
    uspRelatoriocNmUsuario: TStringField;
    uspRelatoriocContaBancaria: TStringField;
    uspRelatoriocNmEspTit: TStringField;
    uspRelatoriocFlgExibeRazao: TIntegerField;
    QRDBText3: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRazaoAuxiliarFornecedores_View: TrptRazaoAuxiliarFornecedores_View;

implementation

{$R *.dfm}

end.
