unit rFluxoCaixaRealizado_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptFluxoCaixaRealizado_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    lblFiltro1: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    qryResumoContas: TADOQuery;
    dsResumoContas: TDataSource;
    qryResumoContascTipo: TStringField;
    qryResumoContasiSeqDRE: TIntegerField;
    qryResumoContascNmGrupoPlanoConta: TStringField;
    qryResumoContasnCdPlanoConta: TIntegerField;
    qryResumoContascNmPlanoConta: TStringField;
    qryResumoContascFlgRecDes: TStringField;
    qryResumoContasnValor: TBCDField;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRGroup2: TQRGroup;
    QRDBText2: TQRDBText;
    QRBand2: TQRBand;
    QRBand4: TQRBand;
    QRDBText7: TQRDBText;
    QRExpr1: TQRExpr;
    QRLabel5: TQRLabel;
    QRExpr2: TQRExpr;
    qryResumo: TADOQuery;
    dsResumo: TDataSource;
    qryResumoiOrdem: TIntegerField;
    qryResumocDescricao: TStringField;
    qryResumonValor: TFloatField;
    DetailBand1: TQRBand;
    QRDBText5: TQRDBText;
    QRDBText4: TQRDBText;
    QRCompositeReport1: TQRCompositeReport;
    QuickRep2: TQuickRep;
    DetailBand2: TQRBand;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    TitleBand1: TQRBand;
    QRLabel4: TQRLabel;
    QuickRep3: TQuickRep;
    qrySaldoFinal: TADOQuery;
    qrySaldoFinalnCdContaBancaria: TIntegerField;
    qrySaldoFinalcNmContaBancaria: TStringField;
    qrySaldoFinaldDtSaldoFinal: TDateField;
    qrySaldoFinalnValorFinal: TFloatField;
    TitleBand2: TQRBand;
    DetailBand3: TQRBand;
    QRLabel6: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    SummaryBand1: TQRBand;
    QRExpr3: TQRExpr;
    QRLabel7: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    PageFooterBand1: TQRBand;
    QRShape3: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    QRShape5: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    procedure QRCompositeReport1AddReports(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    imprimeSaldoFinal : boolean;
  end;

var
  rptFluxoCaixaRealizado_view: TrptFluxoCaixaRealizado_view;

implementation

{$R *.dfm}

procedure TrptFluxoCaixaRealizado_view.QRCompositeReport1AddReports(
  Sender: TObject);
begin
  QRCompositeReport1.Reports.Add(QuickRep1) ;
  QRCompositeReport1.Reports.Add(QuickRep2) ;

  if (imprimeSaldoFinal) then
      QRCompositeReport1.Reports.Add(QuickRep3) ;

end;

end.
