unit fGerenciaPreco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmGerenciaPreco = class(TfrmProcesso_Padrao)
    qryTabPrecoAux: TADOQuery;
    qryTabPrecoAuxnCdTabPrecoAux: TAutoIncField;
    qryTabPrecoAuxnCdEmpresa: TIntegerField;
    qryTabPrecoAuxnCdProduto: TIntegerField;
    qryTabPrecoAuxnCdTerceiro: TIntegerField;
    qryTabPrecoAuxnValor: TBCDField;
    qryTabPrecoAuxnPercent: TBCDField;
    qryTabPrecoAuxcNmTerceiro: TStringField;
    qryTabPrecoAuxcNmProduto: TStringField;
    DBGridEh1: TDBGridEh;
    dsTabPrecoAux: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    procedure qryTabPrecoAuxCalcFields(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTabPrecoAuxBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGerenciaPreco: TfrmGerenciaPreco;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmGerenciaPreco.qryTabPrecoAuxCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryTabPrecoAuxnCdProduto.Value > 0) and (qryTabPrecoAuxcNmProduto.Value = '') then
  begin
      qryProduto.Close ;
      PosicionaQuery(qryProduto, qryTabPrecoAuxnCdProduto.AsString) ;

      if not qryProduto.eof then
          qryTabPrecoAuxcNmProduto.Value := qryProdutocNmProduto.Value ;

  end ;

  if (qryTabPrecoAuxnCdTerceiro.Value > 0) and (qryTabPrecoAuxcNmTerceiro.Value = '') then
  begin
      qryTerceiro.Close ;
      PosicionaQuery(qryTerceiro, qryTabPrecoAuxnCdTerceiro.AsString) ;

      if not qryTerceiro.eof then
          qryTabPrecoAuxcNmTerceiro.Value := qryTerceirocNmTerceiro.Value ;

  end ;

end;

procedure TfrmGerenciaPreco.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTabPrecoAux.State = dsBrowse) then
             qryTabPrecoAux.Edit ;
        

        if (qryTabPrecoAux.State = dsInsert) or (qryTabPrecoAux.State = dsEdit) then
        begin

            if (DBGridEh1.Col = 3) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(106);

                If (nPK > 0) then
                begin
                    qryTabPrecoAuxnCdProduto.Value := nPK ;
                end ;
            end ;

            if (DBGridEh1.Col = 5) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(101);

                If (nPK > 0) then
                begin
                    qryTabPrecoAuxnCdTerceiro.Value := nPK ;
                end ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmGerenciaPreco.qryTabPrecoAuxBeforePost(DataSet: TDataSet);
begin

  if (qryTabPrecoAuxcNmProduto.Value = '') then
  begin
      MensagemAlerta('Informe o produto.') ;
      abort ;
  end ;

  if (qryTabPrecoAuxnValor.Value = 0) and (qryTabPrecoAuxnPercent.Value = 0) then
  begin
      MensagemAlerta('Informe o pre�o ou o percentual.') ;
      abort ;
  end ;

  if (qryTabPrecoAuxnValor.Value > 0) and (qryTabPrecoAuxnPercent.Value > 0) then
  begin
      MensagemAlerta('Informe o pre�o ou o percentual.') ;
      abort ;
  end ;

  inherited;

  qryTabPrecoAuxnCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
  
end;

procedure TfrmGerenciaPreco.FormShow(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryTabPrecoAux, IntToStr(frmMenu.nCdEmpresaAtiva)) ;
  
end;

initialization
    RegisterClass(TfrmGerenciaPreco);
end.
