unit fAnaliseContatoCobranca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  cxLookAndFeelPainters, DB, ADODB, cxButtons, Mask, ER2Lookup, DBCtrls,
  cxPC, cxControls, dxCntner, dxCalc, DBGridEhGrouping, ToolCtrlsEh,
  GridsEh, DBGridEh, TeeProcs, TeEngine, Chart, DbChart, Series;

type
  TfrmAnaliseContatoCobranca = class(TfrmProcesso_Padrao)
    gbFiltro: TGroupBox;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    Label9: TLabel;
    er2LkpLoja: TER2LookupMaskEdit;
    DBEdit1: TDBEdit;
    qryTerceiro: TADOQuery;
    dsTerceiro: TDataSource;
    er2LkpCliente: TER2LookupMaskEdit;
    Label7: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmStatus: TStringField;
    DBEdit4: TDBEdit;
    qryTipoResultadoContato: TADOQuery;
    qryTipoResultadoContatonCdTipoResultadoContato: TIntegerField;
    qryTipoResultadoContatocNmTipoResultadoContato: TStringField;
    qryTipoResultadoContatocFlgAtivo: TIntegerField;
    qryTipoResultadoContatocFlgContatado: TIntegerField;
    qryTipoResultadoContatonCdTabTipoRestricaoVenda: TIntegerField;
    er2LkpTipoResultContato: TER2LookupMaskEdit;
    DBEdit5: TDBEdit;
    dsTipoResultadoContato: TDataSource;
    Label1: TLabel;
    gbOpcao: TGroupBox;
    checkUltContato: TCheckBox;
    mskDtInicial: TMaskEdit;
    Label2: TLabel;
    mskDtFinal: TMaskEdit;
    Label3: TLabel;
    rgModoInclusao: TRadioGroup;
    cxPageControl: TcxPageControl;
    tabAnaliseGeral: TcxTabSheet;
    tabAnaliseSemanal: TcxTabSheet;
    er2LkpAnalista: TER2LookupMaskEdit;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DataSource1: TDataSource;
    Label4: TLabel;
    DBEdit6: TDBEdit;
    tabDesempAnalista: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryTempContatoCobranca: TADOQuery;
    qryTempResumoContatoDiario: TADOQuery;
    qryTempResumoDesempAnalista: TADOQuery;
    dsTempContatoCobranca: TDataSource;
    dsTempResumoContatoDiario: TDataSource;
    dsTempResumoDesempAnalista: TDataSource;
    qryTempResumoDesempAnalistanCdUsuarioAnalista: TIntegerField;
    qryTempResumoDesempAnalistacNmUsuario: TStringField;
    qryTempResumoDesempAnalistanCdLoja: TStringField;
    qryTempResumoDesempAnalistacNmLoja: TStringField;
    qryTempResumoDesempAnalistaiQtdeContato: TIntegerField;
    qryTempResumoDesempAnalistaiQtdeContatoManual: TIntegerField;
    qryTempResumoDesempAnalistaiQtdeContatoAuto: TIntegerField;
    qryTempResumoDesempAnalistanPercParticipacao: TBCDField;
    qryTempContatoCobrancanCdContatoCobranca: TIntegerField;
    qryTempContatoCobrancanCdLoja: TStringField;
    qryTempContatoCobrancacNmLoja: TStringField;
    qryTempContatoCobrancacNmUsuario: TStringField;
    qryTempContatoCobrancacNmTerceiro: TStringField;
    qryTempContatoCobrancadDtContato: TDateTimeField;
    qryTempContatoCobrancacHoraContato: TStringField;
    qryTempContatoCobrancacNmPessoaContato: TStringField;
    qryTempContatoCobrancacTelefoneContato: TStringField;
    qryTempContatoCobrancacTextoContato: TStringField;
    qryTempContatoCobrancanCdTipoResultadoContato: TIntegerField;
    qryTempContatoCobrancacNmTipoResultadoContato: TStringField;
    qryTempContatoCobrancacModoInclusao: TStringField;
    DBGridEh2: TDBGridEh;
    DBGridEh3: TDBGridEh;
    Label5: TLabel;
    qryTempResumoContatoDiariodDtContato: TDateTimeField;
    qryTempResumoContatoDiarioiSemana: TIntegerField;
    qryTempResumoContatoDiariocDiaSemana: TStringField;
    qryTempResumoContatoDiarionCdTipoContato: TIntegerField;
    qryTempResumoContatoDiariocNmTipoContato: TStringField;
    qryTempResumoContatoDiarioiQtdeContato: TIntegerField;
    qryTempResumoContatoDiarionPercParticipacao: TBCDField;
    ImageList2: TImageList;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAnaliseContatoCobranca: TfrmAnaliseContatoCobranca;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAnaliseContatoCobranca.FormShow(Sender: TObject);
begin
  inherited;

  er2LkpLoja.SetFocus;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      desativaER2LkpMaskEdit(er2LkpLoja);
      er2LkpCliente.SetFocus;
  end;

  cxPageControl.ActivePage := tabAnaliseGeral;
end;

procedure TfrmAnaliseContatoCobranca.ToolButton1Click(Sender: TObject);
var
  cFlgUltContato : Integer;
begin
  inherited;

  { -- valiad per�odo -- }
  if ((Trim(mskDtInicial.Text) <> '/  /') and (Trim(mskDtFinal.Text) = '/  /')) then
  begin
      MensagemAlerta('Informe o per�odo final.');
      mskDtFinal.SetFocus;
      Abort;
  end;

  if ((Trim(mskDtInicial.Text) = '/  /') and (Trim(mskDtFinal.Text) <> '/  /')) then
  begin
      MensagemAlerta('Informe o per�odo inicial.');
      mskDtInicial.SetFocus;
      Abort;
  end;

  if ((Trim(mskDtInicial.Text) = '/  /') and (Trim(mskDtFinal.Text) = '/  /')) then
  begin
      mskDtInicial.Text := DateToStr(Date-30);
      mskDtFinal.Text   := DateToStr(Date);
  end;

  if (not frmMenu.fnValidaPeriodo(frmMenu.ConvData(mskDtInicial.Text), frmMenu.ConvData(mskDtFinal.Text))) then
  begin
      mskDtInicial.SetFocus;
      Abort;
  end;

  cFlgUltContato := 0;

  if (checkUltContato.Checked) then
      cFlgUltContato := 1;

  { -- rgModoInclusao.ItemIndex -- }
  { -- 0 : manual               -- }
  { -- 1 : autom�tico           -- }
  { -- 2 : todos                -- }

  case (cxPageControl.ActivePageIndex) of
      0 : begin
              qryTempContatoCobranca.Close;
              qryTempContatoCobranca.Parameters.ParamByName('nCdLoja').Value                 := frmMenu.ConvInteiro(er2LkpLoja.Text);
              qryTempContatoCobranca.Parameters.ParamByName('nCdTerceiro').Value             := frmMenu.ConvInteiro(er2LkpCliente.Text);
              qryTempContatoCobranca.Parameters.ParamByName('nCdUsuario').Value              := frmMenu.ConvInteiro(er2LkpAnalista.Text);
              qryTempContatoCobranca.Parameters.ParamByName('nCdTipoResultadoContato').Value := frmMenu.ConvInteiro(er2LkpTipoResultContato.Text);
              qryTempContatoCobranca.Parameters.ParamByName('cDtContatoInicial').Value       := DateToStr(frmMenu.ConvData(mskDtInicial.Text));
              qryTempContatoCobranca.Parameters.ParamByName('cDtContatoFinal').Value         := DateToStr(frmMenu.ConvData(mskDtFinal.Text));
              qryTempContatoCobranca.Parameters.ParamByName('cFlgConsideraUltContato').Value := cFlgUltContato;
              qryTempContatoCobranca.Parameters.ParamByName('cFlgModoInclusao').Value        := rgModoInclusao.ItemIndex;
              qryTempContatoCobranca.Open;
          end;
      1 : begin
              qryTempResumoContatoDiario.Close;
              qryTempResumoContatoDiario.Parameters.ParamByName('nCdLoja').Value                 := frmMenu.ConvInteiro(er2LkpLoja.Text);
              qryTempResumoContatoDiario.Parameters.ParamByName('nCdTerceiro').Value             := frmMenu.ConvInteiro(er2LkpCliente.Text);
              qryTempResumoContatoDiario.Parameters.ParamByName('nCdUsuario').Value              := frmMenu.ConvInteiro(er2LkpAnalista.Text);
              qryTempResumoContatoDiario.Parameters.ParamByName('nCdTipoResultadoContato').Value := frmMenu.ConvInteiro(er2LkpTipoResultContato.Text);
              qryTempResumoContatoDiario.Parameters.ParamByName('cDtContatoInicial').Value       := DateToStr(frmMenu.ConvData(mskDtInicial.Text));
              qryTempResumoContatoDiario.Parameters.ParamByName('cDtContatoFinal').Value         := DateToStr(frmMenu.ConvData(mskDtFinal.Text));
              qryTempResumoContatoDiario.Parameters.ParamByName('cFlgConsideraUltContato').Value := cFlgUltContato;
              qryTempResumoContatoDiario.Parameters.ParamByName('cFlgModoInclusao').Value        := rgModoInclusao.ItemIndex;
              qryTempResumoContatoDiario.Open;
          end;
      2 : begin
              qryTempResumoDesempAnalista.Close;
              qryTempResumoDesempAnalista.Parameters.ParamByName('nCdLoja').Value                 := frmMenu.ConvInteiro(er2LkpLoja.Text);
              qryTempResumoDesempAnalista.Parameters.ParamByName('nCdTerceiro').Value             := frmMenu.ConvInteiro(er2LkpCliente.Text);
              qryTempResumoDesempAnalista.Parameters.ParamByName('nCdUsuario').Value              := frmMenu.ConvInteiro(er2LkpAnalista.Text);
              qryTempResumoDesempAnalista.Parameters.ParamByName('nCdTipoResultadoContato').Value := frmMenu.ConvInteiro(er2LkpTipoResultContato.Text);
              qryTempResumoDesempAnalista.Parameters.ParamByName('cDtContatoInicial').Value       := DateToStr(frmMenu.ConvData(mskDtInicial.Text));
              qryTempResumoDesempAnalista.Parameters.ParamByName('cDtContatoFinal').Value         := DateToStr(frmMenu.ConvData(mskDtFinal.Text));
              qryTempResumoDesempAnalista.Parameters.ParamByName('cFlgConsideraUltContato').Value := cFlgUltContato;
              qryTempResumoDesempAnalista.Parameters.ParamByName('cFlgModoInclusao').Value        := rgModoInclusao.ItemIndex;
              qryTempResumoDesempAnalista.Open;
          end;
  end;
end;

initialization
  RegisterClass(TfrmAnaliseContatoCobranca);

end.
