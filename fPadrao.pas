unit fPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ComCtrls, StdCtrls, Mask, ExtCtrls, DBCtrls, DB, ADODB,
  Grids, DBGrids;

type
  TfrmStatus_CAD = class(TForm)
    ImageList1: TImageList;
    Button1: TButton;
    StatusBar1: TStatusBar;
    Bevel1: TBevel;
    Database: TADOConnection;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    DataSource1: TDataSource;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    Label7: TLabel;
    Bevel2: TBevel;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit1Enter(Sender: TObject);
    procedure DBEdit1Exit(Sender: TObject);
    procedure DBEdit2Enter(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmStatus_CAD: TfrmStatus_CAD;

implementation

{$R *.dfm}


procedure TfrmStatus_CAD.FormCreate(Sender: TObject);
begin

    Database.DefaultDatabase := 'ADMSoft' ;
    Database.LoginPrompt := False;
    Database.Open('ADMSoft','ADMSoft101580');
    Database.Connected := True;

    qryStatus.Open;

    StatusBar1.Panels.Items[0].Text := 'ADMSoft v1.0' ;

end;


procedure TfrmStatus_CAD.DBEdit1Enter(Sender: TObject);
begin
dbEdit1.Color := clYellow;
end;

procedure TfrmStatus_CAD.DBEdit1Exit(Sender: TObject);
begin
dbEdit1.Color := clAqua;
end;

procedure TfrmStatus_CAD.DBEdit2Enter(Sender: TObject);
begin
dbEdit2.Color := clYellow;

end;

procedure TfrmStatus_CAD.DBEdit2Exit(Sender: TObject);
begin
dbEdit2.Color := clAqua;

end;

procedure TfrmStatus_CAD.Button5Click(Sender: TObject);
begin
    frmStatus_CAD.Close;
end;

procedure TfrmStatus_CAD.Button2Click(Sender: TObject);
begin
    qryStatus.Insert;
end;

procedure TfrmStatus_CAD.Button1Click(Sender: TObject);
begin
    qryStatus.Post;
end;

procedure TfrmStatus_CAD.Button3Click(Sender: TObject);
begin
    qryStatus.Cancel;
end;

procedure TfrmStatus_CAD.Button4Click(Sender: TObject);
begin
    qryStatus.Delete;
end;

end.
