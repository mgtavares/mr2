inherited frmConciliaBancoManual: TfrmConciliaBancoManual
  Left = 158
  Top = 23
  Width = 1075
  Height = 671
  BorderIcons = []
  Caption = 'Concilia'#231#227'o Banc'#225'ria - Manual'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 633
    Width = 1059
    Height = 0
  end
  inherited ToolBar1: TToolBar
    Width = 1059
    ButtonWidth = 126
    inherited ToolButton1: TToolButton
      Caption = 'Nova Concilia'#231#227'o'
      ImageIndex = 2
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Left = 126
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Left = 134
    end
    object ToolButton5: TToolButton
      Left = 260
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 268
      Top = 0
      Caption = 'Processar Concilia'#231#227'o'
      ImageIndex = 1
      OnClick = ToolButton4Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1059
    Height = 108
    Align = alTop
    Caption = 'Informa'#231#245'es do Extrato'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 22
      Top = 24
      Width = 73
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta Banc'#225'ria'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Tag = 1
      Left = 551
      Top = 48
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Limite Cr'#233'dito'
      FocusControl = DBEdit2
    end
    object Label4: TLabel
      Tag = 1
      Left = 27
      Top = 48
      Width = 68
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo Anterior'
      FocusControl = DBEdit3
    end
    object Label5: TLabel
      Tag = 1
      Left = 9
      Top = 72
      Width = 86
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cr'#233'dito Conciliado'
      FocusControl = DBEdit3
    end
    object Label6: TLabel
      Tag = 1
      Left = 288
      Top = 72
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = 'D'#233'bito Conciliado'
      FocusControl = DBEdit3
    end
    object Label7: TLabel
      Tag = 1
      Left = 516
      Top = 72
      Width = 100
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo Final Calculado'
      FocusControl = DBEdit3
    end
    object Label8: TLabel
      Tag = 1
      Left = 262
      Top = 48
      Width = 109
      Height = 13
      Caption = 'Data do Saldo Anterior'
      FocusControl = DBEdit4
    end
    object Label9: TLabel
      Tag = 1
      Left = 552
      Top = 24
      Width = 62
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Extrato'
      FocusControl = DBEdit5
    end
    object Label10: TLabel
      Tag = 1
      Left = 713
      Top = 24
      Width = 90
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo Final Extrato'
      FocusControl = DBEdit6
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 172
      Top = 16
      Width = 325
      Height = 21
      DataField = 'DadosConta'
      DataSource = DataSource1
      TabOrder = 3
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 624
      Top = 40
      Width = 81
      Height = 21
      DataField = 'nValLimiteCredito'
      DataSource = DataSource1
      TabOrder = 4
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 104
      Top = 40
      Width = 121
      Height = 21
      DataField = 'nSaldo'
      DataSource = DataSource2
      TabOrder = 5
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 376
      Top = 40
      Width = 121
      Height = 21
      DataField = 'dDtSaldo'
      DataSource = DataSource2
      TabOrder = 6
    end
    object DBEdit5: TDBEdit
      Left = 624
      Top = 16
      Width = 81
      Height = 21
      DataField = 'dDtExtrato'
      DataSource = dsConciliacao
      TabOrder = 0
    end
    object DBEdit6: TDBEdit
      Left = 808
      Top = 16
      Width = 89
      Height = 21
      DataField = 'nSaldoContaBancaria'
      DataSource = dsConciliacao
      TabOrder = 1
    end
    object DBEdit7: TDBEdit
      Tag = 1
      Left = 104
      Top = 64
      Width = 121
      Height = 21
      DataField = 'nValCredito'
      DataSource = dsConciliacao
      TabOrder = 7
    end
    object DBEdit8: TDBEdit
      Tag = 1
      Left = 376
      Top = 64
      Width = 121
      Height = 21
      DataField = 'nValDebito'
      DataSource = dsConciliacao
      TabOrder = 8
    end
    object DBEdit9: TDBEdit
      Tag = 1
      Left = 624
      Top = 64
      Width = 121
      Height = 21
      DataField = 'nSaldoFinal'
      DataSource = dsSaldoCalculado
      TabOrder = 9
    end
    object cxButton1: TcxButton
      Left = 904
      Top = 48
      Width = 129
      Height = 25
      Caption = 'Incluir Lan'#231'amento'
      Enabled = False
      TabOrder = 10
      OnClick = cxButton1Click
      Glyph.Data = {
        2E020000424D2E0200000000000036000000280000000C0000000E0000000100
        180000000000F801000000000000000000000000000000000000EEFEFEEEFEFE
        EEFEFEEEFEFE9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F0000
        000000000000000000000000000000000000000000000000000000000000009C
        8C6F000000FFFFFFC0928FC0928FC0928FC0928FC0928FC0928FC0928FC0928F
        0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9F9EED9F9EED9F9EE
        D9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9F9EED9F9
        EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9
        F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EE
        D9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9
        EED9F9EED9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFF
        F9EED9F9EED9F9EED9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F0000
        00FFFFFFF9EED9F9EED9F9EED9F9EED90000000000000000000000000000009C
        8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED90000009C8C6F9C8C6F9C8C6F
        000000EEFEFE000000FFFFFFF9EED9F9EED9F9EED9F9EED90000009C8C6F9C8C
        6F000000EEFEFEEEFEFE000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000009C
        8C6F000000EEFEFEEEFEFEEEFEFE000000000000000000000000000000000000
        000000000000EEFEFEEEFEFEEEFEFEEEFEFE}
      LookAndFeel.NativeStyle = True
    end
    object cxButton2: TcxButton
      Left = 904
      Top = 76
      Width = 129
      Height = 25
      Caption = 'Cheque Devolvido'
      Enabled = False
      TabOrder = 11
      OnClick = cxButton2Click
      Glyph.Data = {
        06030000424D06030000000000003600000028000000130000000C0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFAD6A
        FFAD6A8989898989898989898989898989898989898989898989898989898989
        89898989898989898989898989898989ABABAB000000FFFFFFFFAD6AFFAD6AFF
        FFFFFFFFFFFFFFFFF4F4F4EBEBEBE3E3E3D7D7D7D7D7D7D7D7D7D7D7D7D7D7D7
        D7D7D7D7D7D7D7D7D7D7D7D7898989000000FFFFFFFFAD6AFFAD6AFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFF4F4F4EBEBEBE3E3E300000000000000000000000000
        0000000000D7D7D7898989000000FFFFFFFFAD6AFFAD6AFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFF4F4F4EBEBEBE3E3E3D7D7D7D7D7D7D7D7D7D7D7D7D7D7
        D7D7D7D7898989000000FFFFFFFFAD6AFFAD6AFFFFFFFFFFFFB4B4B4B4B4B4B4
        B4B4B4B4B4B4B4B4ACACACACACACAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD7D7D7
        898989000000FFFFFFFFAD6AFFAD6AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFF4F4F4EBEBEBE3E3E3D7D7D7D7D7D7D7D7D7D7D7D789898900
        0000FFFFFFFFAD6AFFAD6AFFFFFFFFFFFFB4B4B4B4B4B4B4B4B4B4B4B4B4B4B4
        B4B4B4ACACACACACACAAAAAAAAAAAAAAAAAAAAAAAAD7D7D7898989000000FFFF
        FFFFAD6AFFAD6AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFF4F4F4EBEBEBE3E3E3D7D7D7D7D7D7898989000000FFFFFFFFAD6A
        FFAD6AFFFFFFFFFFFF241CED241CED241CEDFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFF4F4F4EBEBEBE3E3E3D7D7D7898989000000FFFFFFFFAD6AFFAD6AFF
        FFFFFFFFFF241CED241CED241CEDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        4CB122F4F4F44CB122E3E3E3898989000000FFFFFFFFAD6AFFAD6AFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFF4F4F4EBEBEB898989000000FFFFFFFFAD6AFFAD6A898989898989898989
        8989898989898989898989898989898989898989898989898989898989898989
        89898989ABABAB000000}
      LookAndFeel.NativeStyle = True
    end
    object cxButton3: TcxButton
      Left = 904
      Top = 16
      Width = 129
      Height = 29
      Caption = 'Iniciar Concilia'#231#227'o'
      TabOrder = 2
      OnClick = cxButton3Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FAF7F9FBF9FF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFF7FAF837833D347D3AF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FBF8408E4754A35C4F9F5733
        7D39F8FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        F8FBF8499A515BAC6477CA8274C87E51A059347E3AF8FBF9FFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFF8FCF951A65A63B56D7ECE897BCC8776CA8176
        C98152A25A357F3BF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FCFA59B063
        6BBD7684D2907AC98560B26A63B46D78C98378CB8253A35C36803CF9FBF9FFFF
        FFFFFFFFFFFFFFFFFFFFD3ECD66CBD7679C98680CE8D53A75CB2D6B59CC9A05C
        AD677CCC8679CB8554A45D37813DF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD9EFDC
        6CBD756DC079B5DBB9FFFFFFFFFFFF98C79D5EAE687DCD897CCD8756A55F3882
        3EF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD5EDD8BEE2C3FFFFFFFFFFFFFFFFFFFF
        FFFF99C89D5FAF697FCE8A7ECE8957A66039833FF9FBF9FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF99C89E60B06A81CF8D7FCF
        8B58A761398540F9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFF99C99E62B26C82D18F7AC88557A6609FC4A2FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9F63B3
        6D5FAF69A5CBA9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9FA5CEA9FFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      LookAndFeel.NativeStyle = True
    end
    object DBEdit10: TDBEdit
      Tag = 1
      Left = 104
      Top = 16
      Width = 65
      Height = 21
      DataField = 'nCdContaBancaria'
      DataSource = DataSource1
      TabOrder = 12
    end
  end
  object GroupBox2: TGroupBox [3]
    Left = 0
    Top = 137
    Width = 1059
    Height = 248
    Align = alTop
    Caption = ' Lan'#231'amentos N'#227'o Conciliados '
    TabOrder = 2
    object Label3: TLabel
      Tag = 1
      Left = 11
      Top = 32
      Width = 125
      Height = 13
      Alignment = taRightJustify
      Caption = 'Intervalo de Lan'#231'amentos'
    end
    object Label11: TLabel
      Tag = 1
      Left = 248
      Top = 32
      Width = 16
      Height = 13
      Alignment = taRightJustify
      Caption = 'at'#233
    end
    object edtDtInicial: TDateTimePicker
      Left = 144
      Top = 24
      Width = 97
      Height = 21
      Date = 39868.000000000000000000
      Time = 39868.000000000000000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnChange = edtDtInicialChange
    end
    object cxButton4: TcxButton
      Left = 376
      Top = 22
      Width = 129
      Height = 25
      Caption = 'Exibir Lan'#231'amentos'
      Enabled = False
      TabOrder = 2
      OnClick = cxButton4Click
      Glyph.Data = {
        36030000424D360300000000000036000000280000000F000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
        393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
        00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
        F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
        83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
        68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
        FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
        9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
        8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
        D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
        B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
        5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
        A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
        9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
        488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
        FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
        FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
        FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
    object edtDtFinal: TDateTimePicker
      Left = 272
      Top = 24
      Width = 97
      Height = 21
      Date = 39868.000000000000000000
      Time = 39868.000000000000000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnChange = edtDtFinalChange
    end
    object RadioGroup1: TRadioGroup
      Left = 640
      Top = 12
      Width = 257
      Height = 33
      Caption = 'Filtro de Tipo de Lan'#231'amento'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Todos'
        'Cr'#233'dito'
        'D'#233'bito')
      TabOrder = 3
      OnClick = RadioGroup1Click
    end
    object cxGrid1: TcxGrid
      Left = 8
      Top = 56
      Width = 1025
      Height = 177
      PopupMenu = PopupMenu1
      TabOrder = 4
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dsNaoConciliados
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        OptionsCustomize.ColumnMoving = False
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1DBColumn1: TcxGridDBColumn
          Caption = 'Data Lancto'
          DataBinding.FieldName = 'dDtLancto'
          Options.Editing = False
        end
        object cxGrid1DBTableView1DBColumn2: TcxGridDBColumn
          Caption = 'Nr. Doc.'
          DataBinding.FieldName = 'cDocumento'
          Options.Editing = False
        end
        object cxGrid1DBTableView1DBColumn3: TcxGridDBColumn
          Caption = 'Hist'#243'rico'
          DataBinding.FieldName = 'cHistorico'
          Options.Editing = False
          Width = 588
        end
        object cxGrid1DBTableView1DBColumn4: TcxGridDBColumn
          Caption = 'Valor Cr'#233'dito'
          DataBinding.FieldName = 'nValCredito'
          Options.Editing = False
          Width = 109
        end
        object cxGrid1DBTableView1DBColumn5: TcxGridDBColumn
          Caption = 'Valor D'#233'bito'
          DataBinding.FieldName = 'nValDebito'
          Options.Editing = False
          Width = 97
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object GroupBox3: TGroupBox [4]
    Left = 0
    Top = 385
    Width = 1059
    Height = 248
    Align = alTop
    Caption = ' Lan'#231'amentos Conciliados '
    TabOrder = 3
    object cxGrid5: TcxGrid
      Left = 8
      Top = 16
      Width = 1025
      Height = 200
      TabOrder = 0
      object cxGrid5DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid5DBTableView1DblClick
        DataController.DataSource = dsConciliados
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        OptionsCustomize.ColumnMoving = False
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        object cxGrid5DBTableView1DBColumn1: TcxGridDBColumn
          Caption = 'Data Lancto'
          DataBinding.FieldName = 'dDtLancto'
          Options.Editing = False
        end
        object cxGrid5DBTableView1DBColumn2: TcxGridDBColumn
          Caption = 'Nr. Doc.'
          DataBinding.FieldName = 'cDocumento'
          Options.Editing = False
        end
        object cxGrid5DBTableView1DBColumn3: TcxGridDBColumn
          Caption = 'Hist'#243'rico'
          DataBinding.FieldName = 'cHistorico'
          Options.Editing = False
          Width = 588
        end
        object cxGrid5DBTableView1DBColumn4: TcxGridDBColumn
          Caption = 'Valor Cr'#233'dito'
          DataBinding.FieldName = 'nValCredito'
          Options.Editing = False
          Width = 109
        end
        object cxGrid5DBTableView1DBColumn5: TcxGridDBColumn
          Caption = 'Valor D'#233'bito'
          DataBinding.FieldName = 'nValDebito'
          Options.Editing = False
          Width = 97
        end
      end
      object cxGrid5Level1: TcxGridLevel
        GridView = cxGrid5DBTableView1
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 432
    Top = 336
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009C8C6F009C8C6F009C8C6F009C8C6F009C8C6F009C8C
      6F009C8C6F009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      840000008400000084000000000000000000000000000000000000000000FFFF
      FF00C0928F00C0928F00C0928F00C0928F00C0928F00C0928F00C0928F00C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      840000008400000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900F9EED900C092
      8F00000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED9000000000000000000000000000000
      0000000000009C8C6F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900000000009C8C6F009C8C6F009C8C
      6F00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF00000084000000000000000000000000000000000000000000FFFF
      FF00F9EED900F9EED900F9EED900F9EED900000000009C8C6F009C8C6F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00000000009C8C6F00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFC030000
      FE00C003C003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C007000000018001C00F000000038001C01F0000
      0077C003C03F0000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      
        '  ,Convert(VARCHAR,IsNull(nCdBanco,0)) + '#39' - '#39' + Convert(VARCHAR' +
        ',IsNull(cAgencia,0)) + '#39' - '#39' + IsNull(nCdConta,'#39' '#39') + '#39' - '#39' + Is' +
        'Null(cNmTitular,'#39' '#39') as DadosConta'
      '      ,nValLimiteCredito'
      ',cFlgReapresentaCheque'
      '  FROM ContaBancaria '
      ' WHERE nCdContaBancaria = :nPK'
      '')
    Left = 256
    Top = 272
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariaDadosConta: TStringField
      FieldName = 'DadosConta'
      ReadOnly = True
      Size = 81
    end
    object qryContaBancarianValLimiteCredito: TBCDField
      FieldName = 'nValLimiteCredito'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryContaBancariacFlgReapresentaCheque: TIntegerField
      FieldName = 'cFlgReapresentaCheque'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryContaBancaria
    Left = 392
    Top = 304
  end
  object qrySaldoConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdContaBancaria'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cDtSaldo'
        DataType = ftString
        Size = 12
        Value = '01/01/1901'
      end>
    SQL.Strings = (
      'DECLARE @nCdContaBancaria int'
      '       ,@dDtSaldo         datetime'
      '       ,@cDtSaldo         varchar(10)'
      ''
      'Set @nCdContaBancaria = :nCdContaBancaria'
      'Set @cDtSaldo         = :cDtSaldo'
      'Set @dDtSaldo         = Convert(DATETIME,@cDtSaldo,103)'
      ''
      'SELECT dDtSaldo'
      '      ,nSaldo'
      '  FROM SaldoContaBancaria'
      ' WHERE nCdContaBancaria  = @nCdContaBancaria'
      '   AND dDtSaldo          = (SELECT MAX(dDtSaldo)'
      '                             FROM SaldoContaBancaria'
      
        '                            WHERE nCdContaBancaria = @nCdContaBa' +
        'ncaria'
      '                              AND dDtSaldo        <  @dDtSaldo'
      '                              AND cFlgConciliado = 1 )'
      ' ORDER BY dDtSaldo DESC')
    Left = 256
    Top = 232
    object qrySaldoContanSaldo: TBCDField
      FieldName = 'nSaldo'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qrySaldoContadDtSaldo: TDateTimeField
      FieldName = 'dDtSaldo'
    end
  end
  object DataSource2: TDataSource
    DataSet = qrySaldoConta
    Left = 288
    Top = 232
  end
  object qryNaoConciliados: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'iTipo'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtInicial'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtFinal'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @iTipo int'
      ''
      'Set @iTipo = :iTipo'
      ''
      'SELECT nCdLanctoFin'
      '      ,dDtLancto'
      '      ,nValCredito'
      '      ,nValDebito'
      '      ,nCdTipoLancto'
      '      ,cFlgConciliado'
      '      ,cDocumento'
      '      ,cHistorico'
      '      ,cFlgManual'
      '  FROM #Temp_LanctoFin'
      ' WHERE cFlgConciliado   = 0'
      '   AND dDtLancto >=  Convert(DATETIME,:dDtInicial,103)'
      '   AND dDtLancto  < (Convert(DATETIME,:dDtFinal,103) + 1)'
      '   AND (   @iTipo = 0 --Todos'
      
        '        OR ((@iTipo = 1 AND nValCredito > 0) OR (@iTipo <> 1))  ' +
        '--Todos os creditos'
      
        '        OR ((@iTipo = 2 AND nValDebito  > 0) OR (@iTipo <> 2))) ' +
        '--Todos os creditos'
      ' ORDER BY dDtLancto')
    Left = 388
    Top = 232
    object qryNaoConciliadosnCdLanctoFin: TIntegerField
      DisplayLabel = 'Lan'#231'amentos n'#227'o conciliados|'
      FieldName = 'nCdLanctoFin'
    end
    object qryNaoConciliadosdDtLancto: TDateTimeField
      DisplayLabel = 'Lan'#231'amentos n'#227'o conciliados|Data Lancto'
      FieldName = 'dDtLancto'
    end
    object qryNaoConciliadosnValCredito: TBCDField
      DisplayLabel = 'Lan'#231'amentos n'#227'o conciliados|Valor Cr'#233'dito'
      FieldName = 'nValCredito'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryNaoConciliadosnValDebito: TBCDField
      DisplayLabel = 'Lan'#231'amentos n'#227'o conciliados|Valor D'#233'bito'
      FieldName = 'nValDebito'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryNaoConciliadosnCdTipoLancto: TIntegerField
      DisplayLabel = 'Lan'#231'amentos n'#227'o conciliados|'
      FieldName = 'nCdTipoLancto'
    end
    object qryNaoConciliadoscFlgConciliado: TIntegerField
      DisplayLabel = 'Lan'#231'amentos n'#227'o conciliados|OK'
      FieldName = 'cFlgConciliado'
    end
    object qryNaoConciliadoscDocumento: TStringField
      DisplayLabel = 'Lan'#231'amentos n'#227'o conciliados|Nr. Doc.'
      FieldName = 'cDocumento'
      Size = 15
    end
    object qryNaoConciliadoscHistorico: TStringField
      DisplayLabel = 'Lan'#231'amentos n'#227'o conciliados|Hist'#243'rico'
      FieldName = 'cHistorico'
      Size = 150
    end
    object qryNaoConciliadoscFlgManual: TIntegerField
      DisplayLabel = 'Lan'#231'amentos n'#227'o conciliados|'
      FieldName = 'cFlgManual'
    end
  end
  object dsNaoConciliados: TDataSource
    DataSet = qryNaoConciliados
    Left = 420
    Top = 232
  end
  object qryConciliados: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_LanctoFin'#39') IS NULL)'
      'BEGIN'
      ''
      #9#9'CREATE TABLE #Temp_LanctoFin (nCdLanctoFin   int'
      #9#9#9#9#9#9#9#9#9' ,dDtLancto      datetime'
      #9#9#9#9#9#9#9#9#9' ,nValCredito    decimal(12,2)'
      #9#9#9#9#9'         ,nValDebito     decimal(12,2)'
      #9#9#9#9#9#9#9#9#9' ,nCdTipoLancto  int'
      #9#9#9#9#9#9#9#9#9' ,cFlgConciliado int default 0 not null'
      #9#9#9#9#9#9#9#9#9' ,cDocumento     varchar(15)'
      #9#9#9#9#9#9#9#9#9' ,cHistorico     varchar(50)'
      #9#9#9#9#9#9#9#9#9' ,cFlgManual     int default 0 not null'
      '                   ,cFlgAtualizado int default 0 not null)'
      ''
      'END'
      ''
      ''
      'SELECT nCdLanctoFin'
      '      ,dDtLancto'
      '      ,nValCredito'
      '      ,nValDebito'
      '      ,nCdTipoLancto'
      '      ,cFlgConciliado'
      '      ,cDocumento'
      '      ,cHistorico'
      '      ,cFlgManual'
      '  FROM #Temp_LanctoFin'
      ' WHERE cFlgConciliado = 1'
      ' ORDER BY dDtLancto')
    Left = 324
    Top = 232
    object qryConciliadosnCdLanctoFin: TIntegerField
      DisplayLabel = 'Lan'#231'amentos Conciliados|'
      FieldName = 'nCdLanctoFin'
    end
    object qryConciliadosdDtLancto: TDateTimeField
      DisplayLabel = 'Lan'#231'amentos Conciliados|Data Lancto'
      FieldName = 'dDtLancto'
    end
    object qryConciliadosnValCredito: TBCDField
      DisplayLabel = 'Lan'#231'amentos Conciliados|Valor Cr'#233'dito'
      FieldName = 'nValCredito'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryConciliadosnValDebito: TBCDField
      DisplayLabel = 'Lan'#231'amentos Conciliados|Valor D'#233'bito'
      FieldName = 'nValDebito'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryConciliadosnCdTipoLancto: TIntegerField
      DisplayLabel = 'Lan'#231'amentos Conciliados|'
      FieldName = 'nCdTipoLancto'
    end
    object qryConciliadoscFlgConciliado: TIntegerField
      DisplayLabel = 'Lan'#231'amentos Conciliados|'
      FieldName = 'cFlgConciliado'
    end
    object qryConciliadoscDocumento: TStringField
      DisplayLabel = 'Lan'#231'amentos Conciliados|Nr. Doc.'
      FieldName = 'cDocumento'
      Size = 15
    end
    object qryConciliadoscHistorico: TStringField
      DisplayLabel = 'Lan'#231'amentos Conciliados|Hist'#243'rico'
      FieldName = 'cHistorico'
      Size = 150
    end
    object qryConciliadoscFlgManual: TIntegerField
      DisplayLabel = 'Lan'#231'amentos Conciliados|'
      FieldName = 'cFlgManual'
    end
  end
  object dsConciliados: TDataSource
    DataSet = qryConciliados
    Left = 356
    Top = 232
  end
  object SP_PROCESSA_CONCILIACAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_PROCESSA_CONCILIACAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cDtExtrato'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 292
    Top = 336
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#Temp_LanctoFin'#39') IS NULL)'#13#10'BEGIN'#13#10#13#10#9#9'CR' +
      'EATE TABLE #Temp_LanctoFin (nCdLanctoFin   int'#13#10#9#9#9#9#9#9#9#9#9' ,dDtLa' +
      'ncto      datetime'#13#10#9#9#9#9#9#9#9#9#9' ,nValCredito    decimal(12,2)'#13#10#9#9#9 +
      #9#9'         ,nValDebito     decimal(12,2)'#13#10#9#9#9#9#9#9#9#9#9' ,nCdTipoLanc' +
      'to  int'#13#10#9#9#9#9#9#9#9#9#9' ,cFlgConciliado int default 0 not null'#13#10#9#9#9#9#9 +
      #9#9#9#9' ,cDocumento     varchar(15)'#13#10#9#9#9#9#9#9#9#9#9' ,cHistorico     varc' +
      'har(150)'#13#10#9#9#9#9#9#9#9#9#9' ,cFlgManual     int default 0 not null'#13#10'    ' +
      '               ,cFlgAtualizado int default 0 not null)'#13#10#13#10'END'#13#10' ' +
      '     '#13#10
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 396
    Top = 336
  end
  object qryConsultaCheque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdContaBancariaDep'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'iNrCheque'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCheque'
      ',dDtDevol'
      ',dDtSegDevol'
      '  FROM Cheque'
      ' WHERE nCdContaBancariaDep = :nCdContaBancariaDep'
      '   AND iNrCheque           = :iNrCheque'
      '   AND nCdTabStatusCheque  IN (2,3,4,9)')
    Left = 364
    Top = 336
    object qryConsultaChequenCdCheque: TAutoIncField
      FieldName = 'nCdCheque'
      ReadOnly = True
    end
    object qryConsultaChequedDtDevol: TDateTimeField
      FieldName = 'dDtDevol'
    end
    object qryConsultaChequedDtSegDevol: TDateTimeField
      FieldName = 'dDtSegDevol'
    end
  end
  object SP_REGISTRA_CHEQUE_DEVOLVIDO_EXTRATO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_REGISTRA_CHEQUE_DEVOLVIDO_EXTRATO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContaBancariaDep'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdCheque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cDtExtrato'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 360
    Top = 304
  end
  object dsConciliacao: TDataSource
    DataSet = qryConciliacaoBancaria
    Left = 360
    Top = 272
  end
  object qryConciliacaoBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM ConciliacaoBancaria')
    Left = 328
    Top = 272
    object qryConciliacaoBancarianCdConciliacaoBancaria: TAutoIncField
      FieldName = 'nCdConciliacaoBancaria'
      ReadOnly = True
    end
    object qryConciliacaoBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryConciliacaoBancariadDtConciliacaoBancaria: TDateTimeField
      FieldName = 'dDtConciliacaoBancaria'
    end
    object qryConciliacaoBancariadDtExtrato: TDateTimeField
      FieldName = 'dDtExtrato'
      EditMask = '!99/99/9999;1;_'
    end
    object qryConciliacaoBancarianSaldoContaBancaria: TBCDField
      FieldName = 'nSaldoContaBancaria'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryConciliacaoBancarianValCredito: TBCDField
      FieldName = 'nValCredito'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryConciliacaoBancarianValDebito: TBCDField
      FieldName = 'nValDebito'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryConciliacaoBancarianCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
  end
  object cdsSaldoCalculado: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 392
    Top = 272
    object cdsSaldoCalculadonSaldoFinal: TFloatField
      FieldName = 'nSaldoFinal'
      DisplayFormat = '#,##0.00'
    end
  end
  object dsSaldoCalculado: TDataSource
    DataSet = cdsSaldoCalculado
    Left = 424
    Top = 272
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 320
    Top = 305
  end
  object qrySomaLancamentos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_LanctoFin'#39') IS NULL)'
      'BEGIN'
      ''
      #9#9'CREATE TABLE #Temp_LanctoFin (nCdLanctoFin   int'
      #9#9#9#9#9#9#9#9#9' ,dDtLancto      datetime'
      #9#9#9#9#9#9#9#9#9' ,nValCredito    decimal(12,2)'
      #9#9#9#9#9'         ,nValDebito     decimal(12,2)'
      #9#9#9#9#9#9#9#9#9' ,nCdTipoLancto  int'
      #9#9#9#9#9#9#9#9#9' ,cFlgConciliado int default 0 not null'
      #9#9#9#9#9#9#9#9#9' ,cDocumento     varchar(15)'
      #9#9#9#9#9#9#9#9#9' ,cHistorico     varchar(50)'
      #9#9#9#9#9#9#9#9#9' ,cFlgManual     int default 0 not null'
      '                   ,cFlgAtualizado int default 0 not null)'
      ''
      'END'
      ''
      'SELECT Sum(nValCredito) as nValCredito'
      '      ,Sum(nValDebito) as nValDebito'
      '  FROM #Temp_LanctoFin'
      ' WHERE cFlgConciliado = 1')
    Left = 288
    Top = 273
    object qrySomaLancamentosnValCredito: TBCDField
      FieldName = 'nValCredito'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qrySomaLancamentosnValDebito: TBCDField
      FieldName = 'nValDebito'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdContaBancaria'
        Size = -1
        Value = Null
      end
      item
        Name = 'cDtExtrato'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'DECLARE @nCdContaBancaria int'
      '       ,@dDtExtrato       datetime'
      '       ,@cDtExtrato       varchar(10)'
      ''
      'Set @nCdContaBancaria = :nCdContaBancaria'
      'Set @cDtExtrato       = :cDtExtrato'
      'Set @dDtExtrato       = Convert(datetime,@cDtExtrato,103)'
      ''
      'DELETE'
      '  FROM #Temp_LanctoFin'
      ' WHERE cFlgConciliado = 0'
      ''
      'INSERT INTO #Temp_LanctoFin (nCdLanctoFin'
      '                            ,dDtLancto'
      '                            ,nValCredito'
      '                            ,nValDebito'
      '                            ,nCdTipoLancto'
      '                            ,cFlgConciliado'
      '                            ,cDocumento'
      '                            ,cHistorico'
      '                            ,cFlgManual'
      '                            ,cFlgAtualizado)'
      '                      SELECT nCdLanctoFin'
      '                            ,dDtLancto'
      
        '                            ,CASE WHEN nValLancto > 0 THEN nValL' +
        'ancto'
      '                                  ELSE 0'
      '                             END'
      
        '                            ,CASE WHEN nValLancto < 0 THEN ABS(n' +
        'ValLancto)'
      '                                  ELSE 0'
      '                             END'
      '                            ,TipoLancto.nCdTipoLancto'
      '                            ,cFlgConciliado'
      '                            ,cDocumento'
      
        '                            ,CASE WHEN cNmTipoLancto IS NOT NULL' +
        ' THEN IsNull(RTRIM(cNmTipoLancto),'#39' '#39') + CASE WHEN cHistorico IS' +
        ' NOT NULL THEN + '#39' - '#39' + cHistorico ELSE '#39' '#39' END'
      '                                  ELSE cHistorico'
      '                             END'
      '                            ,cFlgManual'
      '                            ,0'
      '                        FROM LanctoFin'
      
        '                             LEFT JOIN TipoLancto       ON TipoL' +
        'ancto.nCdTipoLancto             = LanctoFin.nCdTipoLancto'
      
        '                             LEFT JOIN ProvisaoTit      ON Provi' +
        'saoTit.nCdProvisaoTit           = LanctoFin.nCdProvisaoTit'
      
        '                             LEFT JOIN DepositoBancario ON Depos' +
        'itoBancario.nCdDepositoBancario = LanctoFin.nCdDepositoBancario'
      
        '                       WHERE LanctoFin.nCdContaBancaria = @nCdCo' +
        'ntaBancaria'
      '                         AND cFlgConciliado   = 0'
      '                         AND NOT EXISTS(SELECT 1'
      
        '                                          FROM #Temp_LanctoFin T' +
        'emp'
      
        '                                         WHERE Temp.nCdLanctoFin' +
        ' = LanctoFin.nCdLanctoFin)'
      '                         AND ProvisaoTit.dDtCancel      IS NULL'
      '                         AND DepositoBancario.dDtCancel IS NULL')
    Left = 256
    Top = 337
  end
  object qryLanctoFin: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT LanctoFin.cFlgManual'
      '      ,Usuario.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '      ,cFlgConciliado'
      '  FROM LanctoFin'
      
        '       LEFT JOIN Usuario ON Usuario.nCdUsuario = LanctoFin.nCdUs' +
        'uario'
      ' WHERE nCdLanctoFin = :nPK')
    Left = 288
    Top = 305
    object qryLanctoFincFlgManual: TIntegerField
      FieldName = 'cFlgManual'
    end
    object qryLanctoFinnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryLanctoFincNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryLanctoFincFlgConciliado: TIntegerField
      FieldName = 'cFlgConciliado'
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 424
    Top = 305
    object ExibirTtulosMovimentados1: TMenuItem
      Caption = 'Exibir T'#237'tulos Movimentados'
      OnClick = ExibirTtulosMovimentados1Click
    end
    object ExcluirLanamento1: TMenuItem
      Caption = 'Excluir Lan'#231'amento'
      OnClick = ExcluirLanamento1Click
    end
  end
  object SP_REAPRESENTA_CHEQUE: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_REAPRESENTA_CHEQUE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdCheque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 328
    Top = 337
  end
  object qryLanctoCartao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 1 '
      '  FROM LanctoFIn '
      ' WHERE nCdLanctoFin        = :nCdLanctoFin'
      '   AND nCdOperadoraCartao IS NOT NULL')
    Left = 256
    Top = 305
  end
end
