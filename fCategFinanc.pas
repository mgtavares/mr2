unit fCategFinanc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, ImgList, cxPC, cxControls;

type
  TfrmCategFinanc = class(TfrmCadastro_Padrao)
    qryMasternCdCategFinanc: TIntegerField;
    qryMastercNmCategFinanc: TStringField;
    qryMasternCdGrupoCategFinanc: TIntegerField;
    qryMastercFlgRecDes: TStringField;
    qryGrupoCategFinanc: TADOQuery;
    qryGrupoCategFinancnCdGrupoCategFinanc: TIntegerField;
    qryGrupoCategFinanccNmGrupoCategFinanc: TStringField;
    qryMastercNmGrupoCategFinanc: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    DBEdit5: TDBEdit;
    dsUsuarioCategFinanc: TDataSource;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    qryCentroCusto: TADOQuery;
    dsCentroCusto: TDataSource;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcCdCC: TStringField;
    qryCCcNmCC: TStringField;
    qryCentroCustonCdCategFinanc: TIntegerField;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocCdCC: TStringField;
    qryCentroCustocNmCC: TStringField;
    qryMasternCdPlanoConta: TIntegerField;
    qryContas: TADOQuery;
    qryContasnCdPlanoConta: TAutoIncField;
    qryContascNmPlanoConta: TStringField;
    qryContasnCdGrupoPlanoConta: TIntegerField;
    qryContascFlgRecDes: TStringField;
    qryMastercNmPlanoConta: TStringField;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    DBGridEh1: TDBGridEh;
    qryUsuarioCategFinanc: TADOQuery;
    qryUsuarioCategFinancnCdCategFinanc: TIntegerField;
    qryUsuarioCategFinancnCdUsuario: TIntegerField;
    qryUsuarioCategFinanccNmUsuario: TStringField;
    qryUsuarioCategFinancnCdUsuarioCategFinanc: TIntegerField;
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryUsuarioCategFinancBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryCentroCustoBeforePost(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryUsuarioCategFinancCalcFields(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCategFinanc: TfrmCategFinanc;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmCategFinanc.btIncluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;
  
  DBEdit2.SetFocus;
end;

procedure TfrmCategFinanc.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(12);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoCategFinanc.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmCategFinanc.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasternCdGrupoCategFinanc.Value = 0) or (DBEdit4.Text = '') then
  begin
      ShowMessage('Informe o grupo de categoria') ;
      DBEdit3.SetFocus;
      Abort ;
  end ;

  if (qryMastercFlgRecDes.Value <> 'R') and (qryMastercFlgRecDes.Value <> 'D') then
  begin
      ShowMessage('Flag Indicador de Receita ou Despesa Invalido.') ;
      DBEdit5.SetFocus;
      Abort ;
  end ;

  if (qryMastercNmCategFinanc.Value = '') then
  begin
      ShowMessage('Informe a descri��o') ;
      DBEdit2.SetFocus;
      Abort ;
  end ;

  if (qryMasternCdPlanoConta.Value = 0) or (DbEdit7.Text = '') then
  begin
      ShowMessage('Informe a Conta do Plano de Contas') ;
      DbEdit6.SetFocus ;
      Abort ;
  end ;

  inherited;

end;

procedure TfrmCategFinanc.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryCentroCusto.Close ;
  qryCentroCusto.Parameters.ParamByName('nCdCategFinanc').Value := qryMasternCdCategFinanc.Value ;
  qryCentroCusto.Open ;

  PosicionaQuery(qryUsuarioCategFinanc , qryMasternCdCategFinanc.AsString) ;

end;

procedure TfrmCategFinanc.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryCentroCusto.Close ;
  qryUsuarioCategFinanc.Close;

end;

procedure TfrmCategFinanc.qryUsuarioCategFinancBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  if (qryUsuarioCategFinanccNmUsuario.Value = '') then
  begin
      MensagemAlerta('Selecione um usu�rio.') ;
      abort ;
  end ;

  inherited;

  qryUsuarioCategFinancnCdCategFinanc.Value := qryMasternCdCategFinanc.Value ;

  if (qryUsuarioCategFinanc.State = dsInsert) then
      qryUsuarioCategFinancnCdUsuarioCategFinanc.Value := frmMenu.fnProximoCodigo('USUARIOCATEGFINANC') ;

end;

procedure TfrmCategFinanc.FormShow(Sender: TObject);
begin

  cxPageControl1.ActivePageIndex := 0 ;
  inherited;

end;

procedure TfrmCategFinanc.qryCentroCustoBeforePost(DataSet: TDataSet);
begin
  If (qryCentroCustonCdCC.Value = 0) or (qryCentroCustocNmCC.Value = '') then
  begin
      ShowMessage('Informe um centro de custo v�lido.') ;
      Abort ;
  end ;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryCentroCustonCdCategFinanc.Value := qryMasternCdCategFinanc.Value ;

  inherited;

end;

procedure TfrmCategFinanc.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryCentroCusto.State = dsBrowse) then
             qryCentroCusto.Edit ;


        if (qryCentroCusto.State = dsInsert) or (qryCentroCusto.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(29);

            If (nPK > 0) then
            begin
                qryCentroCustonCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmCategFinanc.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(41);

            If (nPK > 0) then
            begin
                qryMasternCdPlanoConta.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCategFinanc.qryUsuarioCategFinancCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryUsuarioCategFinancnCdUsuario.Value > 0) then
  begin
      PosicionaQuery(qryUsuario, qryUsuarioCategFinancnCdUsuario.AsString) ;

      if not qryUsuario.eof then
          qryUsuarioCategFinanccNmUsuario.Value := qryUsuariocNmUsuario.Value ;
  end ;

end;

procedure TfrmCategFinanc.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioCategFinanc.State = dsBrowse) then
             qryUsuarioCategFinanc.Edit ;

        if (qryUsuarioCategFinanc.State = dsInsert) or (qryUsuarioCategFinanc.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
                qryUsuarioCategFinancnCdUsuario.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmCategFinanc.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryUsuarioCategFinanc , qryMasternCdCategFinanc.AsString) ;
  
end;

procedure TfrmCategFinanc.btSalvarClick(Sender: TObject);
begin
  inherited;

  qryUsuarioCategFinanc.Close;
  qryCentroCusto.Close;
  
end;

procedure TfrmCategFinanc.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CATEGFINANC' ;
  nCdTabelaSistema  := 9 ;
  nCdConsultaPadrao := 13 ;

end;

initialization
    RegisterClass(TfrmCategFinanc);

end.
