unit fSelecLoja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxLookAndFeelPainters, StdCtrls, cxButtons, DB, ADODB,
  GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmSelecLoja = class(TForm)
    Image1: TImage;
    DBGridEh1: TDBGridEh;
    qryLojas: TADOQuery;
    dsLojas: TDataSource;
    qryLojasnCdLoja: TIntegerField;
    qryLojascNmLoja: TStringField;
    qryLojascFlgUsaPDV: TIntegerField;
    Panel1: TPanel;
    cxButton2: TcxButton;
    qryLojascMsgCupomFiscal: TStringField;
    procedure FormCreate(Sender: TObject);
    function SelecionaLoja(nCdUsuario:Integer) : Integer ;
    procedure cxButton1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSelecLoja: TfrmSelecLoja;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmSelecLoja.FormCreate(Sender: TObject);
var
    i : Integer;
begin
  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TDBGridEh) then
    begin
      (Components [I] as TDBGridEh).Flat := True ;
      (Components [I] as TDBGridEh).FixedColor := clMoneyGreen ;
      (Components [I] as TDBGridEh).OddRowColor := clAqua ;
      (Components [I] as TDBGridEh).UseMultiTitle := True ;

      if ((Components [I] as TDBGridEh).Tag = 1) then
      begin
          (Components [I] as TDBGridEh).ParentFont := False ;
          (Components [I] as TDBGridEh).ReadOnly   := True ;
          (Components [I] as TDBGridEh).Color      := clSilver ;
          (Components [I] as TDBGridEh).Font.Color := clYellow ;
          Update;
      end ;

    end;

    if (Components [I] is TEdit) then
    begin
      (Components [I] as TEdit).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      (Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 15 ;
      (Components [I] as TEdit).Font.Name   := 'Tahoma' ;
      (Components [I] as TEdit).Font.Size   := 7 ;
      (Components [I] as TEdit).Font.Style  := [] ;

    end;

    if (Components [I] is TLabel) then
    begin
       (Components [I] as TLabel).Font.Name    := 'Tahoma' ;
       (Components [I] as TLabel).Font.Size    := 8 ;
       (Components [I] as TLabel).Font.Style   := [] ;
       (Components [I] as TLabel).Font.Color   := clBlack ;
       (Components [I] as TLabel).Transparent  := True ;
       (Components [I] as TLabel).BringToFront;
    end ;

    if (Components [I] is TButton) then
    begin
       (Components [I] as TButton).ParentFont := False ;
       (Components [I] as TButton).Font.Name  := 'Arial' ;
       (Components [I] as TButton).Font.Size  := 8 ;
       (Components [I] as TButton).Font.Color := clBlue ;
       (Components [I] as TButton).Default    := False ;
       (Components [I] as TButton).Update ;
    end ;
  end ;

end;

function TfrmSelecLoja.SelecionaLoja(nCdUsuario:Integer) : Integer ;
begin

    qryLojas.Close ;
    qryLojas.Parameters.ParamByName('nCdUsuario').Value := nCdUsuario ;
    qryLojas.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
    qryLojas.Open  ;

    if (qryLojas.eof) then
    begin
        ShowMessage('Nenhuma loja vinculada para o usu�rio.') ;
        Result := 0 ;
        exit ;
    end ;

    if (qryLojas.RecordCount = 1) then
    begin
        qryLojas.First;
        cxButton2.Click;
        Result := qryLojasnCdLoja.Value ;
        exit ;
    end ;

    qryLojas.First ;

    while not qryLojas.eof do
    begin
        if (qryLojasnCdLoja.Value = frmMenu.nCdLojaPadrao) then
        begin
            cxButton2.Click;
            Result := frmMenu.nCdLojaPadrao ;
            exit ;
        end ;
        qryLojas.Next ;
    end ;

    qryLojas.First ;

    frmSelecLoja.ShowModal ;

    Result := qryLojasnCdLoja.Value ;
end ;

procedure TfrmSelecLoja.cxButton1Click(Sender: TObject);
begin
    frmMenu.StatusBar1.Panels[1].Text := qryLojascNmLoja.Value ;

    frmMenu.cNmLojaAtiva    := qryLojascNmLoja.Value    ;
    frmMenu.cFlgUsaPDV      := qryLojascFlgUsaPDV.Value ;
    frmMenu.cMsgCupomFiscal := qryLojascMsgCupomFiscal.Value ;

    close ;
end;

procedure TfrmSelecLoja.DBGridEh1DblClick(Sender: TObject);
begin
    frmMenu.StatusBar1.Panels[1].Text := qryLojascNmLoja.Value ;

    frmMenu.cNmLojaAtiva    := qryLojascNmLoja.Value ;
    frmMenu.cMsgCupomFiscal := qryLojascMsgCupomFiscal.Value ;

    close ;

end;

procedure TfrmSelecLoja.FormShow(Sender: TObject);
begin
    DbGridEh1.SetFocus;
end;

procedure TfrmSelecLoja.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  case key of
    vk_RETURN : cxButton2.Click;
  end ;

end;

end.
