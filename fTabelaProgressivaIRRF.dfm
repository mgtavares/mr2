inherited frmTabelaProgressivaIRRF: TfrmTabelaProgressivaIRRF
  Left = 103
  Top = 179
  Caption = 'Tabela Progressiva IRRF'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel [1]
    Left = 29
    Top = 49
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Mes/Ano'
    FocusControl = DBEdit2
  end
  object DBEdit2: TDBEdit [3]
    Left = 81
    Top = 43
    Width = 91
    Height = 19
    DataField = 'cMesAno'
    DataSource = dsMaster
    TabOrder = 1
    OnExit = DBEdit2Exit
  end
  object cxPageControl1: TcxPageControl [4]
    Left = 24
    Top = 72
    Width = 817
    Height = 200
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 200
    ClientRectRight = 817
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'Tabela IRRF'
      ImageIndex = 0
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 817
        Height = 177
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsIRRF
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nValInicial'
            Footers = <>
            Title.Caption = 'Tabela Progressiva IRRF|Valor Inicial'
          end
          item
            EditButtons = <>
            FieldName = 'nValFinal'
            Footers = <>
            Title.Caption = 'Tabela Progressiva IRRF|Valor Final'
          end
          item
            EditButtons = <>
            FieldName = 'nAliqIRRF'
            Footers = <>
            Title.Caption = 'Tabela Progressiva IRRF|Al'#237'quota'
          end
          item
            EditButtons = <>
            FieldName = 'nValParcDedutivel'
            Footers = <>
            Title.Caption = 'Tabela Progressiva IRRF|Valor Parcela Dedut'#237'vel'
          end
          item
            EditButtons = <>
            FieldName = 'nValParcDedutDep'
            Footers = <>
            Title.Caption = 'Tabela Progressiva IRRF|Valor Parcela Dedut'#237'vel Dependente'
            Width = 149
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM TabelaIRRF'
      'WHERE nCdTabelaIRRF = :nPk'
      '')
    object qryMasternCdTabelaIRRF: TIntegerField
      FieldName = 'nCdTabelaIRRF'
    end
    object qryMastercMesAno: TStringField
      FieldName = 'cMesAno'
      FixedChar = True
      Size = 7
    end
  end
  object qryIRRF: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryIRRFBeforePost
    AfterPost = qryIRRFAfterPost
    AfterScroll = qryIRRFAfterScroll
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftString
        Precision = 7
        Size = 7
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM TabelaIRRF'
      'WHERE cMesAno = :nPk'
      'Order by nValInicial')
    Left = 888
    Top = 304
    object qryIRRFnCdTabelaIRRF: TIntegerField
      FieldName = 'nCdTabelaIRRF'
    end
    object qryIRRFcMesAno: TStringField
      FieldName = 'cMesAno'
      FixedChar = True
      Size = 7
    end
    object qryIRRFnValInicial: TBCDField
      FieldName = 'nValInicial'
      DisplayFormat = '##,#00.00'
      Precision = 12
      Size = 2
    end
    object qryIRRFnValFinal: TBCDField
      FieldName = 'nValFinal'
      DisplayFormat = '##,#00.00'
      Precision = 12
      Size = 2
    end
    object qryIRRFnAliqIRRF: TBCDField
      FieldName = 'nAliqIRRF'
      DisplayFormat = '##,#00.00'
      Precision = 12
      Size = 2
    end
    object qryIRRFnValParcDedutivel: TBCDField
      FieldName = 'nValParcDedutivel'
      DisplayFormat = '##,#00.00'
      Precision = 12
      Size = 2
    end
    object qryIRRFnValParcDedutDep: TBCDField
      FieldName = 'nValParcDedutDep'
      DisplayFormat = '##,#00.00'
      Precision = 12
      Size = 2
    end
  end
  object dsIRRF: TDataSource
    DataSet = qryIRRF
    Left = 928
    Top = 296
  end
end
