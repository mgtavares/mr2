inherited frmContratoFinanc: TfrmContratoFinanc
  Caption = 'Contrato Financeiro'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 78
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 73
    Top = 62
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 76
    Top = 86
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 10
    Top = 110
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria Financeira'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 26
    Top = 134
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Unidade Neg'#243'cio'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 48
    Top = 158
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro Custo'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 79
    Top = 182
    Width = 37
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo SP'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 18
    Top = 200
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Contrato'
    FocusControl = DBMemo1
  end
  object Label9: TLabel [9]
    Left = 28
    Top = 374
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Validade'
    FocusControl = DBEdit8
  end
  object Label10: TLabel [10]
    Left = 240
    Top = 374
    Width = 16
    Height = 13
    Caption = 'at'#233
    FocusControl = DBEdit9
  end
  object Label11: TLabel [11]
    Left = 16
    Top = 398
    Width = 100
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Cancelamento'
    FocusControl = DBEdit10
  end
  object Label12: TLabel [12]
    Left = 49
    Top = 422
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Mensal'
    FocusControl = DBEdit11
  end
  object Label13: TLabel [13]
    Left = 35
    Top = 446
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dia Vencimento'
    FocusControl = DBEdit12
  end
  object Label14: TLabel [14]
    Left = 39
    Top = 470
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = #218'ltima gera'#231#227'o'
    FocusControl = DBEdit13
  end
  object DBEdit1: TDBEdit [16]
    Tag = 1
    Left = 120
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdContratoFinanc'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [17]
    Tag = 1
    Left = 120
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [18]
    Left = 120
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [19]
    Left = 120
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdCategFinanc'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit4Exit
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [20]
    Left = 120
    Top = 128
    Width = 65
    Height = 19
    DataField = 'nCdUnidadeNegocio'
    DataSource = dsMaster
    TabOrder = 5
    OnExit = DBEdit5Exit
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [21]
    Left = 120
    Top = 152
    Width = 65
    Height = 19
    DataField = 'nCdCC'
    DataSource = dsMaster
    TabOrder = 6
    OnExit = DBEdit6Exit
    OnKeyDown = DBEdit6KeyDown
  end
  object DBEdit7: TDBEdit [22]
    Left = 120
    Top = 176
    Width = 65
    Height = 19
    DataField = 'nCdTipoSP'
    DataSource = dsMaster
    TabOrder = 7
    OnExit = DBEdit7Exit
    OnKeyDown = DBEdit7KeyDown
  end
  object DBMemo1: TDBMemo [23]
    Left = 120
    Top = 200
    Width = 681
    Height = 161
    DataField = 'cDescricaoContrato'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit8: TDBEdit [24]
    Left = 120
    Top = 368
    Width = 113
    Height = 19
    DataField = 'dDtInicio'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit9: TDBEdit [25]
    Left = 264
    Top = 368
    Width = 113
    Height = 19
    DataField = 'dDtFim'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit10: TDBEdit [26]
    Left = 120
    Top = 392
    Width = 113
    Height = 19
    DataField = 'dDtCancel'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit11: TDBEdit [27]
    Left = 120
    Top = 416
    Width = 113
    Height = 19
    DataField = 'nValMensal'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit12: TDBEdit [28]
    Left = 120
    Top = 440
    Width = 113
    Height = 19
    DataField = 'iDiaVencto'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit13: TDBEdit [29]
    Tag = 1
    Left = 120
    Top = 464
    Width = 145
    Height = 19
    DataField = 'dDtUltGeracao'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit14: TDBEdit [30]
    Tag = 1
    Left = 192
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 15
  end
  object DBEdit15: TDBEdit [31]
    Tag = 1
    Left = 192
    Top = 80
    Width = 650
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = DataSource2
    TabOrder = 16
  end
  object DBEdit16: TDBEdit [32]
    Tag = 1
    Left = 192
    Top = 104
    Width = 650
    Height = 19
    DataField = 'cNmCategFinanc'
    DataSource = DataSource3
    TabOrder = 17
  end
  object DBEdit17: TDBEdit [33]
    Tag = 1
    Left = 192
    Top = 128
    Width = 650
    Height = 19
    DataField = 'cNmUnidadeNegocio'
    DataSource = DataSource4
    TabOrder = 18
  end
  object DBEdit18: TDBEdit [34]
    Tag = 1
    Left = 192
    Top = 152
    Width = 650
    Height = 19
    DataField = 'cNmCC'
    DataSource = DataSource5
    TabOrder = 19
  end
  object DBEdit19: TDBEdit [35]
    Tag = 1
    Left = 192
    Top = 176
    Width = 650
    Height = 19
    DataField = 'cNmTipoSP'
    DataSource = DataSource6
    TabOrder = 20
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ContratoFinanc'
      'WHERE nCdContratoFinanc = :nPK')
    object qryMasternCdContratoFinanc: TIntegerField
      FieldName = 'nCdContratoFinanc'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasternCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryMasternCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryMasternCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMasternCdTipoSP: TIntegerField
      FieldName = 'nCdTipoSP'
    end
    object qryMastercDescricaoContrato: TMemoField
      FieldName = 'cDescricaoContrato'
      BlobType = ftMemo
    end
    object qryMasterdDtInicio: TDateTimeField
      FieldName = 'dDtInicio'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtFim: TDateTimeField
      FieldName = 'dDtFim'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasternValMensal: TBCDField
      FieldName = 'nValMensal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasteriDiaVencto: TIntegerField
      FieldName = 'iDiaVencto'
    end
    object qryMasterdDtUltGeracao: TDateTimeField
      FieldName = 'dDtUltGeracao'
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 720
    Top = 144
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 328
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 768
    Top = 144
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryTerceiro
    Left = 632
    Top = 336
  end
  object qryCategFinanc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCategFinanc'
      '      ,cNmCategFinanc'
      '  FROM CategFinanc'
      ' WHERE cFlgRecDes = '#39'D'#39
      '   AND nCdCategFinanc = :nPK')
    Left = 664
    Top = 144
    object qryCategFinancnCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryCategFinanccNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qryCategFinanc
    Left = 640
    Top = 344
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM UnidadeNegocio'
      ' WHERE nCdUnidadeNegocio = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM EmpresaUnidadeNegocio'
      
        '               WHERE EmpresaUnidadeNegocio.nCdUnidadeNegocio = U' +
        'nidadeNegocio.nCdUnidadeNegocio'
      
        '                 AND EmpresaUnidadeNegocio.nCdEmpresa = :nCdEmpr' +
        'esa)')
    Left = 624
    Top = 144
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object DataSource4: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 648
    Top = 352
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC'
      '      ,cNmCC'
      '  FROM CentroCusto'
      ' WHERE iNivel   = 3'
      '   AND cFlgLanc = 1'
      '   AND nCdCC    = :nPK')
    Left = 576
    Top = 152
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object DataSource5: TDataSource
    DataSet = qryCC
    Left = 656
    Top = 360
  end
  object qryTipoSP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoSP'
      '      ,cNmTipoSP'
      '  FROM TipoSP'
      ' WHERE nCdTipoSP = :nPK')
    Left = 544
    Top = 152
    object qryTipoSPnCdTipoSP: TIntegerField
      FieldName = 'nCdTipoSP'
    end
    object qryTipoSPcNmTipoSP: TStringField
      FieldName = 'cNmTipoSP'
      Size = 50
    end
  end
  object DataSource6: TDataSource
    DataSet = qryTipoSP
    Left = 664
    Top = 368
  end
end
