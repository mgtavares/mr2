inherited frmCobranca_VisaoContato_MovTit: TfrmCobranca_VisaoContato_MovTit
  Left = 81
  Top = 129
  Width = 810
  BorderIcons = [biSystemMenu]
  Caption = 'Movimentos do T'#237'tulo'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 794
  end
  inherited ToolBar1: TToolBar
    Width = 794
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 794
    Height = 435
    Align = alClient
    DataSource = dsMovimentos
    DrawMemoText = True
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'dDtMov'
        Footers = <>
        Width = 103
      end
      item
        EditButtons = <>
        FieldName = 'cNmOperacao'
        Footers = <>
        Width = 363
      end
      item
        EditButtons = <>
        FieldName = 'nValMov'
        Footers = <>
        Width = 98
      end
      item
        EditButtons = <>
        FieldName = 'cOBSMov'
        Footers = <>
        Width = 179
      end>
  end
  object qryMovimentos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT dbo.fn_ZeroEsquerda(Operacao.nCdOperacao,4) + '#39' - '#39' + cNm' +
        'Operacao as cNmOperacao'
      '      ,dDtMov'
      '      ,nValMov'
      '      ,cOBSMov'
      '  FROM MTitulo'
      
        '       INNER JOIN Operacao ON Operacao.nCdOperacao = MTitulo.nCd' +
        'Operacao'
      ' WHERE nCdTitulo = :nPK'
      '   AND dDtCancel IS NULL'
      ' ORDER BY dDtMov')
    Left = 256
    Top = 144
    object qryMovimentoscNmOperacao: TStringField
      DisplayLabel = 'Movimentos do T'#237'tulo|Opera'#231#227'o Financeira'
      FieldName = 'cNmOperacao'
      ReadOnly = True
      Size = 68
    end
    object qryMovimentosdDtMov: TDateTimeField
      DisplayLabel = 'Movimentos do T'#237'tulo|Data Mov.'
      FieldName = 'dDtMov'
    end
    object qryMovimentosnValMov: TBCDField
      DisplayLabel = 'Movimentos do T'#237'tulo|Valor Mov.'
      FieldName = 'nValMov'
      Precision = 12
      Size = 2
    end
    object qryMovimentoscOBSMov: TMemoField
      DisplayLabel = 'Movimentos do T'#237'tulo|Observa'#231#227'o'
      FieldName = 'cOBSMov'
      BlobType = ftMemo
    end
  end
  object dsMovimentos: TDataSource
    DataSet = qryMovimentos
    Left = 296
    Top = 144
  end
end
