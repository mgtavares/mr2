unit fBuscaPK;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, ExtCtrls, cxLookAndFeelPainters, cxButtons, DBCtrls;

type
  TfrmBuscaPK = class(TForm)
    Image1: TImage;
    MaskEdit1: TMaskEdit;
    Label1: TLabel;
    cxButton1: TcxButton;
    procedure FormCreate(Sender: TObject);
    function AbreFormPK() : Integer;
    procedure cxButton1Click(Sender: TObject);
    procedure MaskEdit1KeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBuscaPK: TfrmBuscaPK;

implementation

uses fMenu;

{$R *.dfm}

function TfrmBuscaPK.AbreFormPK() : Integer;
begin
    MaskEdit1.Text := '' ;
    frmBuscaPK.ShowModal ;

    If (Trim(MaskEdit1.Text) = '') Then
        Result := 0
    else
        Result := StrToInt(Trim(MaskEdit1.Text)) ;

end ;

procedure TfrmBuscaPK.FormCreate(Sender: TObject);
var
    i : Integer;
begin
  for I := 0 to ComponentCount - 1 do
  begin
    if (Components [I] is TDBEdit) then
    begin
      (Components [I] as TDBEdit).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TDBEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBEdit).CharCase    := ecUpperCase ;
      (Components [I] as TDBEdit).Color       := clWhite ;
      (Components [I] as TDBEdit).Height      := 11 ;
      (Components [I] as TDBEdit).BevelInner  := bvSpace ;
      (Components [I] as TDBEdit).BevelKind   := bkNone ;
      (Components [I] as TDBEdit).BevelOuter  := bvLowered ;
      (Components [I] as TDBEdit).Ctl3D       := True    ;
      (Components [I] as TDBEdit).BorderStyle := bsSingle ;
      (Components [I] as TDBEdit).Font.Name   := 'Consolas' ;
      (Components [I] as TDBEdit).Font.Size   := 10 ;
      (Components [I] as TDBEdit).Font.Style  := [] ;

      if ((Components [I] as TDBEdit).Tag = 1) then
      begin
          (Components [I] as TDBEdit).ParentFont := False ;
          (Components [I] as TDBEdit).Enabled := False ;
          (Components [I] as TDBEdit).Color   := clSilver ;
          (Components [I] as TDBEdit).Font.Color := clWhite ;
      end ;

    end;

    if (Components [I] is TEdit) then
    begin
      (Components [I] as TEdit).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      (Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 15 ;
      (Components [I] as TEdit).Font.Name   := 'Consolas' ;
      (Components [I] as TEdit).Font.Size   := 10 ;
      (Components [I] as TEdit).Font.Style  := [] ;

    end;

    if (Components [I] is TMaskEdit) then
    begin
      (Components [I] as TMaskEdit).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TMaskEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TMaskEdit).CharCase    := ecUpperCase ;
      (Components [I] as TMaskEdit).Color       := clWhite ;
      (Components [I] as TMaskEdit).BevelInner  := bvSpace ;
      (Components [I] as TMaskEdit).BevelKind   := bkNone ;
      (Components [I] as TMaskEdit).BevelOuter  := bvLowered ;
      (Components [I] as TMaskEdit).Ctl3D       := True    ;
      (Components [I] as TMaskEdit).BorderStyle := bsSingle ;
      (Components [I] as TMaskEdit).Height      := 15 ;
      (Components [I] as TMaskEdit).Font.Name   := 'Consolas' ;
      (Components [I] as TMaskEdit).Font.Size   := 10 ;
      (Components [I] as TMaskEdit).Font.Style  := [] ;

    end;

    if (Components [I] is TDBLookupComboBox) then
    begin
      (Components [I] as TDBLookupComboBox).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TDBLookupComboBox).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBLookupComboBox).Color       := clWhite ;
      (Components [I] as TDBLookupComboBox).BevelInner  := bvSpace   ;
      (Components [I] as TDBLookupComboBox).BevelKind   := bkNone    ;
      (Components [I] as TDBLookupComboBox).BevelOuter  := bvLowered ;
      (Components [I] as TDBLookupComboBox).Ctl3D       := True      ;
      (Components [I] as TDBLookupComboBox).Font.Name   := 'Arial' ;
      (Components [I] as TDBLookupComboBox).Font.Size   := 8 ;
      (Components [I] as TDBLookupComboBox).Font.Style  := [] ;
      (Components [I] as TDBLookupComboBox).DropDownRows := 30 ;

    end;

    if (Components [I] is TLabel) then
    begin
       (Components [I] as TLabel).Font.Name    := 'Consolas' ;
       (Components [I] as TLabel).Font.Size    := 8 ;
       (Components [I] as TLabel).Font.Style   := [] ;
       (Components [I] as TLabel).Font.Color   := clWhite ;
       (Components [I] as TLabel).Transparent  := True ;
       (Components [I] as TLabel).BringToFront;
    end ;

    if (Components [I] is TButton) then
    begin
       (Components [I] as TButton).ParentFont := False ;
       (Components [I] as TButton).Font.Name  := 'Arial' ;
       (Components [I] as TButton).Font.Size  := 8 ;
       (Components [I] as TButton).Font.Color := clBlue ;
       (Components [I] as TButton).Default    := False ;
       (Components [I] as TButton).Update ;
    end ;
  end ;


end;

procedure TfrmBuscaPK.cxButton1Click(Sender: TObject);
begin
    Close ;
end;

procedure TfrmBuscaPK.MaskEdit1KeyPress(Sender: TObject; var Key: Char);
begin

  if key = #13 then
  begin

    key := #0;
    close;

  end;

end;

procedure TfrmBuscaPK.FormShow(Sender: TObject);
begin
    MaskEdit1.SelStart := 0;
    MaskEdit1.SetFocus;
end;

end.
