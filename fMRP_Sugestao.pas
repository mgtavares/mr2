unit fMRP_Sugestao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ADODB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, StdCtrls, Menus, cxContainer, cxTextEdit,
  ER2Excel;

type
  TfrmMRP_Sugestao = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    SP_CALCULA_MRP: TADOStoredProc;
    SP_CALCULA_MRPnCdItemMRP: TAutoIncField;
    SP_CALCULA_MRPnCdProduto: TIntegerField;
    SP_CALCULA_MRPcNmProduto: TStringField;
    SP_CALCULA_MRPnLoteMultiplo: TBCDField;
    SP_CALCULA_MRPnQtdeMinimaCompra: TBCDField;
    SP_CALCULA_MRPcUnidadeMedidaCompra: TStringField;
    SP_CALCULA_MRPnFatorCompra: TBCDField;
    SP_CALCULA_MRPnEstoqueAtual: TBCDField;
    SP_CALCULA_MRPnEstoqueEmpenhado: TBCDField;
    SP_CALCULA_MRPnCdLoja: TIntegerField;
    SP_CALCULA_MRPcFlgTipoPonto: TStringField;
    SP_CALCULA_MRPnEstoqueSeg: TBCDField;
    SP_CALCULA_MRPnEstoqueRessup: TBCDField;
    SP_CALCULA_MRPnEstoqueMax: TBCDField;
    SP_CALCULA_MRPnConsumoMedDia: TBCDField;
    SP_CALCULA_MRPnCdLocalEstoqueEntrega: TIntegerField;
    SP_CALCULA_MRPiDiaEntrega: TIntegerField;
    SP_CALCULA_MRPnDuracaoEstoque: TBCDField;
    SP_CALCULA_MRPnPrevisaoRecebimento: TBCDField;
    SP_CALCULA_MRPnPrevisaoVendas: TBCDField;
    SP_CALCULA_MRPnQtdeComprar: TBCDField;
    SP_CALCULA_MRPiLeadTimeSubItens: TIntegerField;
    SP_CALCULA_MRPnEstoqueDisponivel: TBCDField;
    SP_CALCULA_MRPiLLC: TIntegerField;
    SP_CALCULA_MRPnQtdeSugestao: TBCDField;
    SP_CALCULA_MRPdDtPrevisaoReceb: TDateTimeField;
    SP_CALCULA_MRPcNmTipoNecessidade: TStringField;
    DataSource1: TDataSource;
    cxGrid1DBTableView1nCdItemMRP: TcxGridDBColumn;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1nLoteMultiplo: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeMinimaCompra: TcxGridDBColumn;
    cxGrid1DBTableView1cUnidadeMedidaCompra: TcxGridDBColumn;
    cxGrid1DBTableView1nFatorCompra: TcxGridDBColumn;
    cxGrid1DBTableView1nEstoqueAtual: TcxGridDBColumn;
    cxGrid1DBTableView1nEstoqueEmpenhado: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLoja: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgTipoPonto: TcxGridDBColumn;
    cxGrid1DBTableView1nEstoqueSeg: TcxGridDBColumn;
    cxGrid1DBTableView1nEstoqueRessup: TcxGridDBColumn;
    cxGrid1DBTableView1nEstoqueMax: TcxGridDBColumn;
    cxGrid1DBTableView1nConsumoMedDia: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLocalEstoqueEntrega: TcxGridDBColumn;
    cxGrid1DBTableView1iDiaEntrega: TcxGridDBColumn;
    cxGrid1DBTableView1nDuracaoEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1nPrevisaoRecebimento: TcxGridDBColumn;
    cxGrid1DBTableView1nPrevisaoVendas: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeComprar: TcxGridDBColumn;
    cxGrid1DBTableView1iLeadTimeSubItens: TcxGridDBColumn;
    cxGrid1DBTableView1nEstoqueDisponivel: TcxGridDBColumn;
    cxGrid1DBTableView1iLLC: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeSugestao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPrevisaoReceb: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTipoNecessidade: TcxGridDBColumn;
    SP_CALCULA_MRPcNmLocalEstoque: TStringField;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    SP_CALCULA_MRPcNmGrupoProduto: TStringField;
    cxGrid1DBTableView1DBColumn2: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    GerarPedidodeCompra1: TMenuItem;
    N1: TMenuItem;
    ExibirPedidos1: TMenuItem;
    ltimosPedidos1: TMenuItem;
    FornecedoresHomologados1: TMenuItem;
    FrmuladoProduto1: TMenuItem;
    N2: TMenuItem;
    DetalharEstoqueAtual1: TMenuItem;
    CadastrodoProduto1: TMenuItem;
    N3: TMenuItem;
    ExibirRequisio1: TMenuItem;
    SP_CALCULA_MRPcNmTipoObtencao: TStringField;
    cmdTempFornecedor: TADOCommand;
    cxGrid1DBTableView1DBColumn3: TcxGridDBColumn;
    Label2: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label5: TLabel;
    cxTextEdit4: TcxTextEdit;
    Label3: TLabel;
    cxTextEdit5: TcxTextEdit;
    qryAux: TADOQuery;
    qryPreparaTemp: TADOQuery;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    SP_CALCULA_MRPnCdTipoRessuprimento: TIntegerField;
    SP_MRP_GERA_REQUISICAO: TADOStoredProc;
    Label4: TLabel;
    Label6: TLabel;
    cxStyleRepository1: TcxStyleRepository;
    EstiloPadrao: TcxStyle;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ER2Excel1: TER2Excel;
    DetalharEmpenho1: TMenuItem;
    N4: TMenuItem;
    VermelhaBranca: TcxStyle;
    PretaBranca: TcxStyle;
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure FormShow(Sender: TObject);
    procedure GerarPedidodeCompra1Click(Sender: TObject);
    procedure ExibirPedidos1Click(Sender: TObject);
    procedure ltimosPedidos1Click(Sender: TObject);
    procedure FornecedoresHomologados1Click(Sender: TObject);
    procedure FrmuladoProduto1Click(Sender: TObject);
    procedure DetalharEstoqueAtual1Click(Sender: TObject);
    procedure CadastrodoProduto1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure DetalharEmpenho1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMRP_Sugestao: TfrmMRP_Sugestao;

implementation

uses fMenu, fMRP_GeraPedido, fPedidoAbertoProduto, fMRP_UltimosPedidos,
  fMRP_FornecHomol, fMRP_FormulaProd, fMRP_DetalheEstoqueAtual,
  fProdutoERP, fMRP_ListaPedidosDia, fMRP_DetalhaEmpenho, fProdutoERP_UltimasCompras;

{$R *.dfm}

procedure TfrmMRP_Sugestao.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
var
  nDuracaoEstoque : double ;
  nPR             : double ;
  nEM             : double ;
  cFlgTipo        : string ;
  nEstoqueDIsp    : double ;
  nConsumoMedio   : double ;
begin
  inherited;

  if (AItem <> cxGrid1DBTableView1nQtdeSugestao) and (AItem <> cxGrid1DBTableView1dDtPrevisaoReceb) then
  begin

      AStyle := frmMenu.LinhaCinza ;

  end ;

  if (AItem = cxGrid1DBTableView1nQtdeSugestao) then
  begin
      cFlgTipo        := ARecord.Values[14] ;
      nDuracaoEstoque := StrToFloat(ARecord.Values[9]) ;
      nEM             := StrToFloat(ARecord.Values[11]) ;
      nPR             := StrToFloat(ARecord.Values[12]) ;
      nEstoqueDisp    := StrToFloat(ARecord.Values[8]) ;
      nConsumoMedio   := StrToFloat(ARecord.Values[15]) ;

      if (nConsumoMedio = 0) then
          AStyle := frmMenu.LinhaAzul ;

      if (nConsumoMedio > 0) then
      begin
          if (cFlgTipo = 'D') and (nDuracaoEstoque <= nPR) then
              AStyle := frmMenu.LinhaAmarela ;

          if (cFlgTipo = 'D') and (nDuracaoEstoque <= nEM) then
              AStyle := VermelhaBranca ;

          if (cFlgTipo = 'Q') and (nEstoqueDisp <= nPR) then
              AStyle := frmMenu.LinhaAmarela ;

          if (cFlgTipo = 'Q') and (nEstoqueDisp <= nEM) then
              AStyle := VermelhaBranca ;
      end ;
  end ;

end;

procedure TfrmMRP_Sugestao.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') <> 'S') then
      cxGrid1DBTableView1nCdLoja.Visible := False ;
      
end;

procedure TfrmMRP_Sugestao.GerarPedidodeCompra1Click(Sender: TObject);
var
  objForm : TfrmMRP_GeraPedido;
begin
  inherited;

  if (SP_CALCULA_MRPcNmTipoObtencao.Value = '') then
  begin
      MensagemAlerta('Nenhum tipo de obten��o do produto foi definido.') ;
      exit ;
  end ;

  if (SP_CALCULA_MRPnCdTipoRessuprimento.Value = 0) then
  begin
      MensagemAlerta('Tipo de ressuprimento n�o configurado no cadastro do produto.') ;
      exit ;
  end ;

  objForm := TfrmMRP_GeraPedido.Create(nil);

  if (SP_CALCULA_MRP.State = dsEdit) then
      SP_CALCULA_MRP.Post ;

  //
  // Compra
  //
  if (SP_CALCULA_MRPcNmTipoObtencao.Value = 'Compra') then
  begin

      // contrato
      if (SP_CALCULA_MRPnCdTipoRessuprimento.Value = 2) then
      begin

          objForm.edtProduto.Text             := SP_CALCULA_MRPcNmProduto.Value ;
          objForm.edtQtdeSugerida.Value       := SP_CALCULA_MRPnQtdeSugestao.Value ;
          objForm.edtQtdeParaPedido.Value     := SP_CALCULA_MRPnQtdeSugestao.Value ;
          objForm.edtPrevEntrega.DateTime     := SP_CALCULA_MRPdDtPrevisaoReceb.Value ;
          objForm.edtFatorCompra.Text         := SP_CALCULA_MRPnFatorCompra.AsString;
          objForm.edtUnidadeMedidaCompra.Text := SP_CALCULA_MRPcUnidadeMedidaCompra.Value ;
          objForm.edtQtdeMinimaCompra.Text    := SP_CALCULA_MRPnQtdeMinimaCompra.AsString ;

          if (SP_CALCULA_MRPdDtPrevisaoReceb.Value < Date) then
              objForm.edtPrevEntrega.DateTime := Date + 1 ;

          objForm.edtPedidoFirme.Text     := frmMenu.LeParametro('QTDEPEDFIRME') ;
          objForm.edtPedidoPrev.Text      := frmMenu.LeParametro('QTDEPEDPREV') ;
          objForm.nCdProduto              := SP_CALCULA_MRPnCdProduto.Value ;
          objForm.edtQtdeItens.Value      := Trunc(SP_CALCULA_MRPnConsumoMedDia.Value * 30) / StrToFloat(objForm.edtFatorCompra.Text) ;

          if (objForm.edtQtdeItens.Value < StrToFloat(objForm.edtQtdeMinimaCompra.Text)) then
              objForm.edtQtdeItens.Value := StrToFloat(objForm.edtQtdeMinimaCompra.Text) ;

          objForm.nCdLoja                 := SP_CALCULA_MRPnCdLoja.Value ;

          cmdTempFornecedor.Execute;
          PosicionaQuery(objForm.qryFornecedores, SP_CALCULA_MRPnCdProduto.asString) ;

          if (objForm.qryFornecedores.Eof) then
          begin
              MensagemAlerta('Nenhum fornecedor homologado para este item.') ;
              exit ;
          end ;

          showForm(objForm,false);

          if (objForm.bPedidoGerado) then
          begin
              SP_CALCULA_MRP.Delete ;
          end ;

      end ;

      // cota��o
      if (SP_CALCULA_MRPnCdTipoRessuprimento.Value = 1) then
      begin
          case MessageDlg('Confirma a gera��o de uma requisi��o de compra para cota��o ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo:exit ;
          end ;

          SP_MRP_GERA_REQUISICAO.Close ;
          SP_MRP_GERA_REQUISICAO.Parameters.ParamByName('@nCdProduto').Value     := SP_CALCULA_MRPnCdProduto.Value ;
          SP_MRP_GERA_REQUISICAO.Parameters.ParamByName('@nCdGrupoMRP').Value    := SP_CALCULA_MRP.Parameters.ParamByName('@nCdGrupoMRP').Value ;
          SP_MRP_GERA_REQUISICAO.Parameters.ParamByName('@nCdEmpresa').Value     := SP_CALCULA_MRP.Parameters.ParamByName('@nCdEmpresa').Value ;
          SP_MRP_GERA_REQUISICAO.Parameters.ParamByName('@nCdLoja').Value        := SP_CALCULA_MRPnCdLoja.Value ;
          SP_MRP_GERA_REQUISICAO.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
          SP_MRP_GERA_REQUISICAO.Parameters.ParamByName('@nQtde').Value          := SP_CALCULA_MRPnQtdeSugestao.Value ;
          SP_MRP_GERA_REQUISICAO.Parameters.ParamByName('@cFlgUrgente').Value    := 0 ;
          SP_MRP_GERA_REQUISICAO.Parameters.ParamByName('@cJustificativa').Value := '' ;

          if (SP_CALCULA_MRPcFlgTipoPonto.Value = 'D') then
              if (SP_CALCULA_MRPnDuracaoEstoque.Value < SP_CALCULA_MRPnEstoqueSeg.Value) then
              begin
                  SP_MRP_GERA_REQUISICAO.Parameters.ParamByName('@cFlgUrgente').Value    := 1 ;
                  SP_MRP_GERA_REQUISICAO.Parameters.ParamByName('@cJustificativa').Value := 'QUANT ABAIXO ESTOQUE SEGURAN�A' ;
              end ;

          if (SP_CALCULA_MRPcFlgTipoPonto.Value = 'Q') then
              if (SP_CALCULA_MRPnEstoqueDisponivel.Value < SP_CALCULA_MRPnEstoqueSeg.Value) then
              begin
                  SP_MRP_GERA_REQUISICAO.Parameters.ParamByName('@cFlgUrgente').Value    := 1 ;
                  SP_MRP_GERA_REQUISICAO.Parameters.ParamByName('@cJustificativa').Value := 'QUANT ABAIXO ESTOQUE SEGURAN�A' ;
              end ;

          frmMenu.Connection.BeginTrans;

          try
              SP_MRP_GERA_REQUISICAO.ExecProc;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          frmMenu.Connection.CommitTrans;
          SP_CALCULA_MRP.Delete ;

      end ;

  end ;

end;

procedure TfrmMRP_Sugestao.ExibirPedidos1Click(Sender: TObject);
var
  objForm : TfrmPedidoAbertoProduto;
begin
  inherited;

  objForm := TfrmPedidoAbertoProduto.Create(nil);

  objForm.qryMaster.Close ;
  objForm.qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  objForm.qryMaster.Parameters.ParamByName('nCdProduto').Value := SP_CALCULA_MRPnCdProduto.Value ;
  objForm.qryMaster.Parameters.ParamByName('nCdLoja').Value    := SP_CALCULA_MRPnCdLoja.Value    ;
  objForm.qryMaster.Open ;

  if not objForm.qryMaster.Eof then
      showForm(objForm,true)
  else MensagemAlerta('Nenhum pedido de compra em aberto para o produto.') ;

end;

procedure TfrmMRP_Sugestao.ltimosPedidos1Click(Sender: TObject);
var
  objForm : TfrmProdutoERP_UltimasCompras;
begin
  inherited;

  objForm := TfrmProdutoERP_UltimasCompras.Create(nil);

  objForm.qryUltimasCompras.Close;
  objForm.qryUltimasCompras.Parameters.ParamByName('nCdProduto').Value := SP_CALCULA_MRPnCdProduto.Value;
  objForm.qryUltimasCompras.Open;

  showForm(objForm, True)

end;

procedure TfrmMRP_Sugestao.FornecedoresHomologados1Click(Sender: TObject);
var
  objForm : TfrmMRP_FornecHomol;
begin
  inherited;

  objForm := TfrmMRP_FornecHomol.Create(nil);

  PosicionaQuery(objForm.qryFornecedores, SP_CALCULA_MRPnCdProduto.AsString) ;
  showForm(objForm,true);

end;

procedure TfrmMRP_Sugestao.FrmuladoProduto1Click(Sender: TObject);
var
  objForm : TfrmMRP_FormulaProd;
begin
  inherited;

  objForm := TfrmMRP_FormulaProd.Create(nil);

  PosicionaQuery(objForm.qryFormula, SP_CALCULA_MRPnCdProduto.AsString);
  showForm(objForm,true);

end;

procedure TfrmMRP_Sugestao.DetalharEstoqueAtual1Click(Sender: TObject);
var
  objForm : TfrmMRP_DetalheEstoqueAtual;
begin
  inherited;

  objForm := TfrmMRP_DetalheEstoqueAtual.Create(nil);

  PosicionaQuery(objForm.qryPosicaoEstoque, SP_CALCULA_MRPnCdProduto.AsString) ;
  showForm(objForm,true);

end;

procedure TfrmMRP_Sugestao.CadastrodoProduto1Click(Sender: TObject);
var
  objForm : TfrmProdutoERP;
begin
  inherited;

  objForm := TfrmProdutoERP.Create(nil);

  qryAux.Close ;
  qryAux.SQL.Text := ('SELECT IsNull(nCdProdutoPai, nCdProduto) FROM Produto WHERE nCdProduto = ' + SP_CALCULA_MRPnCdProduto.AsString) ;
  qryAux.Open ;

  if not qryAux.Eof and (qryAux.FieldList[0].Value > 0) then
  begin

      objForm.PosicionaQuery(objForm.qryMaster, qryAux.FieldList[0].Value) ;

      {objForm.btIncluir.Enabled    := False ;
      objForm.btSalvar.Enabled     := False ;
      objForm.btCancelar.Enabled   := False ;
      objForm.btConsultar.Enabled  := False ;
      objForm.ToolButton1.Enabled  := False ;
      objForm.btGerarGrade.Enabled := False ;
      objForm.cxButton1.Enabled    := False ;
      objForm.btExcluir.Enabled    := False ;}

      showForm(objForm,true);

  end ;

end;

procedure TfrmMRP_Sugestao.ToolButton1Click(Sender: TObject);
begin
  inherited;

  case MessageDlg('Confirma o reprocessamento do MRP ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  SP_CALCULA_MRP.Close ;
  SP_CALCULA_MRP.Open ;

  //frmMRP_Sugestao.cxGrid1DBTableView1.ViewData.Expand(True) ;
  
end;

procedure TfrmMRP_Sugestao.ToolButton2Click(Sender: TObject);
begin
  ToolBar1.Enabled := False ;
  inherited;

end;

procedure TfrmMRP_Sugestao.ToolButton5Click(Sender: TObject);
var
  objForm : TfrmMRP_ListaPedidoDia;
begin
  inherited;

  objForm := TfrmMRP_ListaPedidoDia.Create(nil);

  showForm(objForm,true);
end;

procedure TfrmMRP_Sugestao.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  GerarPedidodeCompra1.Enabled := True ;

  if (SP_CALCULA_MRPnCdTipoRessuprimento.Value = 1) then
      GerarPedidodeCompra1.Caption := 'Gerar Requisi��o de Compra' ;

  if (SP_CALCULA_MRPnCdTipoRessuprimento.Value = 2) then
      GerarPedidodeCompra1.Caption := 'Gerar Pedido de Compra' ;

end;

procedure TfrmMRP_Sugestao.ToolButton7Click(Sender: TObject);
var
  cNmFonte : string ;
  iLinha   : integer ;
begin
  inherited;

  frmMenu.mensagemUsuario('Exportando Planilha...');

  cNmFonte := 'Calibri' ;

  ER2Excel1.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
  ER2Excel1.Celula['A2'].Text := 'MRP - Resultado da Sugest�o' ;
  
  ER2Excel1.Celula['A1'].FontName   := cNmFonte;
  ER2Excel1.Celula['A1'].FontSize   := 10 ;

  ER2Excel1.Celula['A2'].FontName   := cNmFonte;
  ER2Excel1.Celula['A2'].FontSize   := 10 ;

  ER2Excel1.Celula['A5'].Mesclar('C5');

  ER2Excel1.Celula['A5'].Text            := 'Produto' ;
  ER2Excel1.Celula['A5'].HorizontalAlign := haCenter ;

  ER2Excel1.Celula['A6'].Text := 'C�digo' ;
  ER2Excel1.Celula['B6'].Text := 'Descri��o' ;
  ER2Excel1.Celula['C6'].Text := 'U.M' ;

  ER2Excel1.Celula['D5'].Text := 'Quant.' ;
  ER2Excel1.Celula['D6'].Text := 'Sugerida' ;

  ER2Excel1.Celula['E5'].Text := 'Estoque' ;
  ER2Excel1.Celula['E6'].Text := 'F�sico' ;

  ER2Excel1.Celula['F5'].Text := 'Estoque' ;
  ER2Excel1.Celula['F6'].Text := 'Empenho' ;

  ER2Excel1.Celula['G5'].Text := 'Estoque' ;
  ER2Excel1.Celula['G6'].Text := 'Dispon.' ;

  ER2Excel1.Celula['H4'].Text := 'Dias' ;
  ER2Excel1.Celula['H5'].Text := 'Dura��o' ;
  ER2Excel1.Celula['H6'].Text := 'Estoque' ;

  ER2Excel1.Celula['I5'].Text := 'Estoque' ;
  ER2Excel1.Celula['I6'].Text := 'Excedente.' ;

  ER2Excel1.Celula['J5'].Text := 'Previs�o' ;
  ER2Excel1.Celula['J6'].Text := 'Recebto' ;

  ER2Excel1.Celula['K5'].Text := 'Estoque' ;
  ER2Excel1.Celula['K6'].Text := 'M�nimo' ;

  ER2Excel1.Celula['L5'].Text := 'Ponto' ;
  ER2Excel1.Celula['L6'].Text := 'Ressupr.' ;

  ER2Excel1.Celula['M5'].Text := 'Estoque' ;
  ER2Excel1.Celula['M6'].Text := 'M�ximo' ;

  ER2Excel1.Celula['N4'].Text := 'Consumo' ;
  ER2Excel1.Celula['N5'].Text := 'M�dio' ;
  ER2Excel1.Celula['N6'].Text := 'Di�rio' ;

  ER2Excel1.Celula['O4'].Text := 'Consumo' ;
  ER2Excel1.Celula['O5'].Text := 'M�dio' ;
  ER2Excel1.Celula['O6'].Text := 'Mensal' ;

  ER2Excel1.Celula['P4'].Text := 'Lote' ;
  ER2Excel1.Celula['P5'].Text := 'M�nimo' ;
  ER2Excel1.Celula['P6'].Text := 'Compra' ;

  ER2Excel1.Celula['Q5'].Text := 'Lead' ;
  ER2Excel1.Celula['Q6'].Text := 'Time' ;

  ER2Excel1.Celula['R5'].Text := 'Grupo' ;
  ER2Excel1.Celula['R6'].Text := 'Produto' ;

  {ER2Excel1.Celula['A6'].Congelar('Q6');}

  ER2Excel1.Celula['A4'].Background := RGB(216,216,216);
  ER2Excel1.Celula['A4'].FontName   := cNmFonte;
  ER2Excel1.Celula['A4'].FontSize   := 9 ;

  ER2Excel1.Celula['A5'].Background := RGB(216,216,216);
  ER2Excel1.Celula['A5'].FontName   := cNmFonte;
  ER2Excel1.Celula['A5'].FontSize   := 9 ;

  ER2Excel1.Celula['A6'].Background := RGB(216,216,216);
  ER2Excel1.Celula['A6'].FontName   := cNmFonte;
  ER2Excel1.Celula['A6'].FontSize   := 9 ;
  ER2Excel1.Celula['A6'].Border     := [EdgeBottom];

  ER2Excel1.Celula['A4'].Range('R4');

  ER2Excel1.Celula['A5'].Range('R5');

  ER2Excel1.Celula['A6'].Range('R6');

  iLinha := 7 ; {-- Linha Inicial --}

  SP_CALCULA_MRP.First;

  while not SP_CALCULA_MRP.Eof do
  begin

      ER2Excel1.Celula['A' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnCdProduto.Value ;
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := SP_CALCULA_MRPcNmProduto.Value;
      ER2Excel1.Celula['C' + IntToStr(iLinha)].Text := SP_CALCULA_MRPcUnidadeMedidaCompra.Value;
      ER2Excel1.Celula['D' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnQtdeSugestao.Value;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnEstoqueAtual.Value;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnEstoqueEmpenhado.Value;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnEstoqueDisponivel.Value;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnDuracaoEstoque.Value;

      ER2Excel1.Celula['J' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnPrevisaoRecebimento.Value;
      ER2Excel1.Celula['K' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnEstoqueSeg.Value;
      ER2Excel1.Celula['L' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnEstoqueRessup.Value;
      ER2Excel1.Celula['M' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnEstoqueMax.Value;
      ER2Excel1.Celula['N' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnConsumoMedDia.Value;
      ER2Excel1.Celula['O' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnConsumoMedDia.Value * 30;
      ER2Excel1.Celula['P' + IntToStr(iLinha)].Text := SP_CALCULA_MRPnQtdeMinimaCompra.Value;
      ER2Excel1.Celula['Q' + IntToStr(iLinha)].Text := SP_CALCULA_MRPiDiaEntrega.Value;
      ER2Excel1.Celula['R' + IntToStr(iLinha)].Text := SP_CALCULA_MRPcNmGrupoProduto.Value;

      {-- se o tipo de gatilho for em dias --}
      if (SP_CALCULA_MRPnConsumoMedDia.Value = 0) then
          ER2Excel1.Celula['D' + IntToStr(iLinha)].Background := clBlue
      else
      begin

          if ( SP_CALCULA_MRPcFlgTipoPonto.Value = 'D' ) then
          begin

              if (SP_CALCULA_MRPnDuracaoEstoque.Value <= SP_CALCULA_MRPnEstoqueRessup.Value) then
                  ER2Excel1.Celula['D' + IntToStr(iLinha)].Background := clYellow;

              if (SP_CALCULA_MRPnDuracaoEstoque.Value <= SP_CALCULA_MRPnEstoqueSeg.Value) then
              begin
                  ER2Excel1.Celula['D' + IntToStr(iLinha)].Background := clRed;
                  ER2Excel1.Celula['D' + IntToStr(iLinha)].FontColor  := clWhite;
              end ;

              if (SP_CALCULA_MRPnDuracaoEstoque.Value > SP_CALCULA_MRPnEstoqueMax.Value) then
              begin
                  ER2Excel1.Celula['I' + IntToStr(iLinha)].Text := (SP_CALCULA_MRPnEstoqueDisponivel.Value - (SP_CALCULA_MRPnEstoqueMax.Value * SP_CALCULA_MRPnConsumoMedDia.Value)) ;
                  ER2Excel1.Celula['I' + IntToStr(iLinha)].Background := clBlack;
                  ER2Excel1.Celula['I' + IntToStr(iLinha)].FontColor  := clWhite;
              end ;


          end

      end ;

      {-- se o tipo de gatilho for em Quantidade --}

      if ( SP_CALCULA_MRPcFlgTipoPonto.Value = 'D' ) then
      begin

          if (SP_CALCULA_MRPnEstoqueDisponivel.Value <= SP_CALCULA_MRPnEstoqueRessup.Value) then
              ER2Excel1.Celula['D' + IntToStr(iLinha)].Background := clYellow;

          if (SP_CALCULA_MRPnEstoqueDisponivel.Value <= SP_CALCULA_MRPnEstoqueSeg.Value) then
              ER2Excel1.Celula['D' + IntToStr(iLinha)].Background := clRed;

      end ;

      ER2Excel1.Celula['D' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['I' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['J' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['K' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['L' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['M' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['N' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['O' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['P' + IntToStr(iLinha)].Mascara := '#.##0' ;
      ER2Excel1.Celula['Q' + IntToStr(iLinha)].Mascara := '#.##0' ;

      ER2Excel1.Celula['D' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['I' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['J' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['K' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['L' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['M' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['N' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['O' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['P' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['Q' + IntToStr(iLinha)].HorizontalAlign := haRight ;


      inc( iLinha ) ;
      
      SP_CALCULA_MRP.Next;
  end ;

  SP_CALCULA_MRP.First;

  ER2Excel1.Celula['A6'].Width := 7;
  ER2Excel1.Celula['B6'].Width := 37;
  ER2Excel1.Celula['C6'].Width := 3;

  ER2Excel1.Celula['A4'].Height := 11;
  ER2Excel1.Celula['A5'].Height := 11;
  ER2Excel1.Celula['A6'].Height := 11;

  ER2Excel1.ExportXLS;

  frmMenu.mensagemUsuario('');


end;

procedure TfrmMRP_Sugestao.DetalharEmpenho1Click(Sender: TObject);
var
  objForm : TfrmMRP_DetalhaEmpenho ;
begin
  inherited;

  objForm := TfrmMRP_DetalhaEmpenho.Create( Self ) ;

  objForm.exibeEmpenhos( SP_CALCULA_MRPnCdProduto.Value );

  freeAndNil( objForm ) ;

end;

end.
