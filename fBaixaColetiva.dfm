inherited frmBaixaColetiva: TfrmBaixaColetiva
  Caption = 'Baixa Coletiva de T'#237'tulos'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 83
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 205
    Top = 38
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 58
    Top = 62
    Width = 63
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Lote'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 81
    Top = 86
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 2
    Top = 110
    Width = 119
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Banc'#225'ria Cr'#233'dito'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 28
    Top = 134
    Width = 93
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor em Dinheiro'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 264
    Top = 134
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cheque (Terceiro)'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 9
    Top = 158
    Width = 112
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Recurso Pr'#243'prio'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 270
    Top = 158
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cr'#233'dito Anterior'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [10]
    Left = 496
    Top = 158
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Total Lote'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [11]
    Left = 45
    Top = 182
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Utilizado'
    FocusControl = DBEdit11
  end
  object Label12: TLabel [12]
    Left = 259
    Top = 182
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Creditado Terceiro'
    FocusControl = DBEdit12
  end
  object Label13: TLabel [13]
    Left = 522
    Top = 182
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo Lote'
    FocusControl = DBEdit13
  end
  object Label14: TLabel [14]
    Left = 724
    Top = 38
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Fechamento'
    FocusControl = DBEdit14
  end
  object Label15: TLabel [15]
    Left = 708
    Top = 62
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio Fechamento'
    FocusControl = DBEdit15
  end
  object Label16: TLabel [16]
    Left = 148
    Top = 62
    Width = 176
    Height = 13
    Caption = '( P=Pagamento / R=Recebimento )'
    FocusControl = DBEdit3
  end
  object Label17: TLabel [17]
    Left = 636
    Top = 110
    Width = 188
    Height = 13
    Caption = '(Somente para Lote de Recebimento)'
    FocusControl = DBEdit3
  end
  object Label22: TLabel [18]
    Left = 483
    Top = 136
    Width = 93
    Height = 13
    Alignment = taRightJustify
    Caption = 'Taxa Adminitrativa'
    FocusControl = DBEdit19
  end
  inherited ToolBar2: TToolBar
    ButtonWidth = 93
    inherited ToolButton7: TToolButton
      Left = 93
    end
    inherited ToolButton1: TToolButton
      Left = 101
    end
    inherited btAlterar: TToolButton
      Left = 194
    end
    inherited ToolButton2: TToolButton
      Left = 287
    end
    inherited btSalvar: TToolButton
      Left = 291
    end
    inherited ToolButton6: TToolButton
      Left = 384
    end
    inherited btExcluir: TToolButton
      Left = 392
    end
    inherited ToolButton50: TToolButton
      Left = 485
    end
    inherited btCancelar: TToolButton
      Left = 493
    end
    inherited ToolButton11: TToolButton
      Left = 586
    end
    inherited btConsultar: TToolButton
      Left = 594
    end
    inherited ToolButton8: TToolButton
      Left = 687
    end
    inherited ToolButton5: TToolButton
      Left = 695
    end
    inherited ToolButton4: TToolButton
      Left = 788
    end
    inherited ToolButton9: TToolButton
      Left = 796
    end
    inherited btnAuditoria: TToolButton
      Left = 804
    end
    object ToolButton3: TToolButton
      Left = 897
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton10: TToolButton
      Left = 905
      Top = 0
      Caption = 'Encerrar Lote'
      ImageIndex = 7
      OnClick = ToolButton10Click
    end
    object ToolButton12: TToolButton
      Left = 998
      Top = 0
      Caption = 'Comprovante'
      ImageIndex = 8
      OnClick = ToolButton12Click
    end
  end
  object DBEdit1: TDBEdit [20]
    Tag = 1
    Left = 124
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdLoteLiq'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [21]
    Tag = 1
    Left = 252
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [22]
    Left = 124
    Top = 56
    Width = 17
    Height = 19
    DataField = 'cTipoLiq'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
  end
  object DBEdit4: TDBEdit [23]
    Left = 124
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit4Exit
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [24]
    Left = 124
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdContaBancaria'
    DataSource = dsMaster
    TabOrder = 5
    OnExit = DBEdit5Exit
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [25]
    Left = 124
    Top = 128
    Width = 120
    Height = 19
    DataField = 'nValDinheiro'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [26]
    Tag = 1
    Left = 356
    Top = 128
    Width = 120
    Height = 19
    DataField = 'nValChTerceiro'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [27]
    Tag = 1
    Left = 124
    Top = 152
    Width = 120
    Height = 19
    DataField = 'nValRecursoProp'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit9: TDBEdit [28]
    Tag = 1
    Left = 356
    Top = 152
    Width = 120
    Height = 19
    DataField = 'nValCredAnterior'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit10: TDBEdit [29]
    Tag = 1
    Left = 580
    Top = 152
    Width = 120
    Height = 19
    DataField = 'nValTotalLote'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit11: TDBEdit [30]
    Tag = 1
    Left = 124
    Top = 176
    Width = 120
    Height = 19
    DataField = 'nValTitLiq'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit12: TDBEdit [31]
    Tag = 1
    Left = 356
    Top = 176
    Width = 120
    Height = 19
    DataField = 'nValCredTerceiro'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit13: TDBEdit [32]
    Tag = 1
    Left = 580
    Top = 176
    Width = 120
    Height = 19
    DataField = 'nSaldoLote'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit14: TDBEdit [33]
    Tag = 1
    Left = 816
    Top = 32
    Width = 113
    Height = 19
    DataField = 'dDtFecham'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit15: TDBEdit [34]
    Tag = 1
    Left = 816
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdUsuarioFecham'
    DataSource = dsMaster
    TabOrder = 15
  end
  object DBEdit16: TDBEdit [35]
    Tag = 1
    Left = 324
    Top = 32
    Width = 57
    Height = 19
    DataField = 'cSiglaEmp'
    DataSource = dsMaster
    TabOrder = 16
  end
  object DBEdit17: TDBEdit [36]
    Tag = 1
    Left = 388
    Top = 32
    Width = 297
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsMaster
    TabOrder = 17
  end
  object DBEdit22: TDBEdit [37]
    Tag = 1
    Left = 196
    Top = 80
    Width = 489
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = DataSource1
    TabOrder = 18
  end
  object DBEdit18: TDBEdit [38]
    Tag = 1
    Left = 196
    Top = 104
    Width = 429
    Height = 19
    DataField = 'cNmContaBancaria'
    DataSource = DataSource2
    TabOrder = 19
  end
  object cxPageControl1: TcxPageControl [39]
    Left = 16
    Top = 216
    Width = 1081
    Height = 513
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 20
    ClientRectBottom = 509
    ClientRectLeft = 4
    ClientRectRight = 1077
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Rela'#231#227'o de Cheques'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1073
        Height = 455
        DataGrouping.GroupLevels = <>
        DataSource = dsChequeLote
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdChequeLoteLiq'
            Footer.Color = clWhite
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoteLiq'
            Footer.Color = clWhite
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdBanco'
            Footer.Color = clWhite
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 36
          end
          item
            EditButtons = <>
            FieldName = 'cAgencia'
            Footer.Color = clWhite
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 40
          end
          item
            EditButtons = <>
            FieldName = 'cConta'
            Footer.Color = clWhite
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 117
          end
          item
            EditButtons = <>
            FieldName = 'cDigito'
            Footer.Color = clWhite
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 24
          end
          item
            EditButtons = <>
            FieldName = 'iNrCheque'
            Footer.Color = clWhite
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'cCNPJCPF'
            Footer.Color = clWhite
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 107
          end
          item
            EditButtons = <>
            FieldName = 'nValCheque'
            Footer.Color = clWhite
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clBlack
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 120
          end
          item
            EditButtons = <>
            FieldName = 'dDtDeposito'
            Footer.Color = clWhite
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'nValTaxaAdm'
            Footer.Color = clWhite
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clBlack
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 120
          end
          item
            EditButtons = <>
            FieldName = 'iDiasVenc'
            Footer.Color = clWhite
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 42
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object btMaqCheque: TcxButton
        Left = 288
        Top = 457
        Width = 153
        Height = 28
        Caption = 'Usar Leitora Cheque'
        TabOrder = 1
        OnClick = btMaqChequeClick
        Glyph.Data = {
          CA050000424DCA05000000000000360000002800000016000000150000000100
          1800000000009405000000000000000000000000000000000000E5E5E5F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0E7E7
          E7D5D5D5E5E5E5E7E7E7D5D5D5E5E5E5E7E7E7D5D5D5E5E5E5F0F0F00000D5D5
          D5E4E4E4F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0EEEEEEEFECECE0
          DCDCD8D6D5D3D2D1C7C6C6E4E4E4E7E7E7D5D5D5E4E4E4E7E7E7D5D5D5E4E4E4
          0000E7E7E7D5D5D5E4E4E4F0F0F0F0F0F0F0F0F0F0F0F0EBEBEBDAD9D8DBDBDA
          F6F5F5D0CBCABDB9B8ABA8A5959390959290B7B6B4D5D4D3D5D5D5E4E4E4E7E7
          E7D5D5D50000E4E4E4E7E7E7D5D5D5E4E4E4F0F0F0F0F0F0E1E1E0CAC9C9CFCE
          CEE7E7E7F3F3F3F5F5F5F7F7F7F4F3F3E6E6E5BFBDBBA29E9CAFABA8B3AFADC1
          C0BFE4E4E4E7E7E70000D5D5D5E4E4E4E7E7E7D5D5D5E0E0E0CBCAC9BCBBBAC6
          C5C4EBEAEAE1E1E1CEAF9AC4A793BFB7B1DADBDCCECFD0BEBFC0B4B5B5CBCACA
          B6B3B1A09D9BBDBDBCE4E4E40000E7E7E7D5D5D5E4E4E4D9D9D9A2A09FA7A5A4
          C9C8C7EEEEEED3CDC9C6997ACF9268B888679E7E6789786D8E8C8AB1B2B2C3C4
          C5D6D6D6DADADACFCECEACABAAD5D5D50000E4E4E4E8E8E8B7B6B59A98969D9C
          9ABABAB9DEDEDEB8ABA2B1896DC88F68CB9168B28667987B677F706669676666
          6666666666797979959595CFCFCFDDDDDDE8E8E80000D1D0D0AAA9A7908E8C90
          8E8DD5D5D4D3D2D2847971927662B48767CD9268C68E67AC8367927967796E66
          666666666666666666808080BEBEBED7D7D7D5D5D5E4E4E40000A19F9D898785
          A9A7A6DEDEDEA0A0A06D69668170628E705BB18262D09368C08C67A681678D76
          66736B666666666E6E6EAAAAAAD4D4D4E7E7E7F0F0F0E8E8E8D5D5D50000A09D
          9BC5C4C3C8C7C7858585626262655F5A857061A07C63AA7C5BC68C63BA8967A0
          7F678774666E69669E9E9ECECECEDEDEDEEDEDEDF0F0F0F0F0F0F0F0F0E8E8E8
          0000E3E2E2DDDDDDB9B9B98B8B8B878786807871816B5BA37D62BF8A63B6815C
          AB80629A7C679C8F86CBCACAD9D9D9EBEBEBF0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F00000E3E2E2AFACAAC3C2C1BFBFBEADACABC2C1C1D4D4D4BEB7B2D0BE
          B2D6B9A6AA8E79BBB2ACD7D7D7E9E9E9F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F00000F0F0F0C7C5C4CCCBCADFDFDFDCDCDCC2C2C2C6C5C4C7
          C6C5A8A7A6AFAEADB4B2B2BEBDBDE4E4E4F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F00000F0F0F0EBEBEBB4B1B0DCDCDCE4E4E4E1E1E1
          D3D3D3E8E8E8EAEAEAD3D3D3D5D5D4A19F9DD0D0D0E4E4E4F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F00000F0F0F0F0F0F0DEDDDDC2C0BEE5E5
          E5EAEAEAE7E7E7D9D9D9E6E6E6E6E6E6D0D0D0C9C8C8ADACAAD5D5D5E4E4E4F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F00000F0F0F0F0F0F0F0F0F0C4
          C2C0D7D6D5EAEAEAF0F0F0ECECECD6D6D6E0E0E0E1E1E1CBCBCBB8B7B6BDBCBB
          D5D5D5E3E3E3F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F00000F0F0F0F0F0F0
          F0F0F0ECEBEBB6B4B2E7E7E6F0F0F0F4F4F4E9E9E9D2D2D2DDDDDDDDDDDDC8C8
          C8A6A4A3D3D2D2D5D5D5E3E3E3F0F0F0F0F0F0F0F0F0F0F0F0F0F0F00000F0F0
          F0F0F0F0F0F0F0F0F0F0DADAD9C8C6C5F1F1F1F4F4F4F0F0F0E4E4E4CFCFCFD9
          D9D9D9D9D9C2C2C29E9C9BDDDDDCD6D6D6E3E3E3F0F0F0F0F0F0F0F0F0F0F0F0
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0C2BFBDE5E4E4F4F4F4F0F0F0ECECEC
          E0E0E0CBCBCBD5D5D5D7D7D7B9B9B9A5A3A2E8E8E8D6D6D6E3E3E3F0F0F0F0F0
          F0F0F0F00000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0ECEBEBBAB7B4CBC9C8D6D4
          D3DBDAD9E8E8E8DCDCDCC8C8C8D1D1D1D6D6D6ABAAA9B5B4B3E8E8E8D6D6D6E3
          E3E3F0F0F0F0F0F00000E3E3E3F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0DF
          DEDDD1D0CFC8C6C4B9B7B5B7B5B49F9C999E9C9AA6A4A2A19E9C8B8887D4D3D3
          E9E9E9D6D6D6E3E3E3F0F0F00000}
        LookAndFeel.NativeStyle = True
      end
      object cxButton1: TcxButton
        Left = 144
        Top = 457
        Width = 145
        Height = 28
        Caption = 'Excluir Cheques'
        TabOrder = 2
        OnClick = cxButton1Click
        Glyph.Data = {
          32040000424D3204000000000000360000002800000013000000110000000100
          180000000000FC03000000000000000000000000000000000000FAFBFFFFFBFC
          FFFAF6FFFDF5FFFFF5FFFFF4FFFFF4FFFDF1FFFDF1FFFFF5FFFFF7FFFAF4FFF8
          F0FFF6E8FFFDEDFFFBEFFFF5EAFFF6F0FFFEFC000000FFFEFFFFFBF7F1E0D7E5
          D0C1EBD1C0F9DCC7EAC9B5F4D1BDFDD8C4EBC5B3E7C3B1F9D5C5FDD9C7FFE2CE
          FFE1CBF3D3C0E3C6B7F0D7CDFFFAF3000000FFFFFBFFFAF1DFC8B9B193809B77
          5FB0886CA17558A97A5EB9896DB7876BB6856BB7866CAD7C62A7785DA0725A9E
          755EB69280E8CBBDFFFBF3000000FFFFF7FFFAEBE8CBB69F7A60623617572705
          743E1B6E34106327037438147437156629076A2F0F743D1E582406623318A97E
          69F6D4C4FFFDF2000000FFF9F0FFFAEAFDDDC6AA826655240467310EC28760AD
          7048692B036A2B05692B05753713B67A56B17551703815643111B4876CFFDDC9
          FFF7E9000000FFFFFBFFFFF4F4D6C5A57C635C2A0C6F3616C88C68FFCCA8CD93
          6F602805592501A67352E6B28EBA7D556529006A3510AA7C5DEAC8B0FFFFF000
          0000FFFFFCFFFFF4F6D5C5A77B635E2A0C6329067A3C18C48962FCC39CB47F5A
          C69470DEB08EB8866285471E75360A733C15B18463FAD8C0FFFDEB000000FFFF
          F9FFFFF1F9D6C2AB7C60612B0A8A4F2871330B6C2C03CA8C63D2976FE2AB84B4
          805B622E066E300784441B6C3510A87A5BFFDFC7FFFDED000000FFFFF7FFFFF0
          FDD7BFAC7D5E632C0775370F7031056E2D01C48559EBAE82E9AF85B68057672F
          0665270070310B6E3515AA7B5FF8D6BFFFFDEE000000FFFFF5FFFFEFFFD7BEAE
          7D5D632C076224007D3C0FC98659E6A578CA8C5EC68B5EFBC295D2986E74350F
          5617007A4225B98B73F0CCBAFFFBEC000000FFFFF5FFFFEFFDD7BFAB7D5E612B
          0882451DC38458FFC397B7794B7C3F136F3407CB9469FFC89FB87B596024066E
          381FAF826DF0CEBEFFFFF4000000FFFFF7FFFFF1F7D6C2A57C635A2B0B6B3512
          B47B54D1966E7A3F17713910632D046E3A12B9845FBC83646E361D5826109E73
          62F8D6C9FFFEF7000000FFFFFBFFFFF4F0D6C69E7B67532A1150210551200067
          3413461300582806744624491C005526075C290F5D2A16532513A47D6FFCDED3
          FFEFE9000000FFFEFEFFFBF8DCC7BFA88D7F9A7969A3806CA78169A98069A981
          68A98168A78268A58268A78169A37865A87B6DA67F71B39086DFC3BCFFF9F400
          0000FFFEFFFFFEFEF5E6E3E0CDC6E3CDC2EAD0C2EBD1C1EDD0C1EDD1C0EDD1C0
          EDD1C0EBD1C1EDD0C1F3D0C3F4D0C6EDCCC3E9CDC6F7E1DCFFFBFA000000FAFA
          FFFFFDFFFFF8F8FFF7F3FFFFF9FFFFF8FFFFF7FFFFF7FFFFF7FFFFF7FFFFF7FF
          FFF8FFFFF7FFFEF7FFFEF7FFFEF8FFFAF5FFF8F7FFFDFD000000F7FAFFFDFCFE
          FEFCFCFFFCFBFFFFFCFFFFFCFFFFFBFFFFFBFFFFFBFFFFFCFFFFFCFFFFFCFFFE
          FCFFFCF9FFFDFAFFFEFBFFFEFCFFFDFDFFFAFB000000}
        LookAndFeel.Kind = lfOffice11
      end
      object btSugerirCheques: TcxButton
        Left = 0
        Top = 457
        Width = 145
        Height = 28
        Caption = 'Sugerir Cheques'
        TabOrder = 3
        OnClick = btSugerirChequesClick
        Glyph.Data = {
          26040000424D2604000000000000360000002800000012000000120000000100
          180000000000F003000000000000000000000000000000000000FFFFFEFFFFFE
          FFFDFDFFFEFDFBF9F8F7F9F9F7F9F9F5FAF9F7F9F9F7F9FAF5F9FAF5F9FAF9FB
          FBFBFBFBFBFBFBFCFCFCFDFDFDFFFEFE0000FFFEFBFFFCFAFFFCFAFFFCF9FFFE
          FAFAFFFDF4FCFB8D98967C81829AA1A46D797D8794968D9392EAEDEBFFFFFEFF
          FFFEEEECEBFFFFFE0000F4F5F10700009D8B8AFFF1EFFBF9F8F0FEFC6A8D8900
          0604001A1A000507000A0E6A8D9082908E8B8F8AF4F7F5686967110F0EFFFCFB
          0000F1FFFFEDFBF9060B0A7F7E80FAFFFF00030432817E7BECE849D3CC5FE4E1
          6ED5D82A6E73000603888F8A7C817F010402FFFFFEF9F7F60000ECFFFFE9FFFF
          DBECEFE6F9FE678187619EA25DD4D347E9E426E6DF1BDCD959FAFE6FE0E35582
          7F7D8781868D8AF8FDFBF8FBF9FFFFFE0000F1FFFFE3F6FBEAFFFFDCFFFF0006
          0B7AE3E660F8F934F2F11FF7F60EF2F119F6FA31D8DB9CDDDB010F096F7A77F8
          FFFEF5FAF8F7FAF80000F7FDFF000006000008CFF6FF00141C5ACCD25CF2F844
          F6FB21E9EF21F9FF07EAF341F5FA8BD2CF0008019BA9A5000603000200FBFFFE
          0000FFFEFFFFFDFFF5F4FEE6FAFF00081283DCE67AE8F482FFFF7CFFFF50EEFF
          3EF2FF52E6F2B1EFEF000400808F8BECFAF6EEF7F4FAFFFE0000FDFFFFEDECF0
          FDFCFFEFFEFF5180883C89927AE3EE8CFFFF6CF9FF55F0FF43E8FB64ECF8558F
          8E6C7E77E7F6F2F0FEFAF8FFFEF6FDFA0000EEFFF9ECFFFB000908698082DAFF
          FF000B0E7AFCFB3CE8E633FBFA21EFF03BF6FE51DDE300100C83958E71807C00
          0602F7FFFDF7FEFB0000EFFFF9000B016C7A74EFFDFBE0F5F6BDF3F400201C3B
          D3CD3DF1EC3CEFEC4CE0E600171C4F7C79E6F6EFF4FFFE7C8784000200F6FBF9
          0000F8FFF8F6FCF1FFFFF8FEEFEDFFFDFFF8FFFF0005040013123D8A8C2D757C
          57879300010A91A3A2F5FFFBEBF4F1FAFFFEFBFFFEF8FBF90000F0F4E9FFF9F0
          FFF1ECFFFDFBFFEFEEFFF9F96C71709EAAACE5EFF68A8C96837A8708000A787A
          7AFAFFFCEDF2F0EDF0EEF0F1EFF4F5F30000FFFFF9FFF6F1FFFDF9FFEDE8FFFF
          FBF5F9F4191716817678987E84B89BA49E89929C9096A39F9ECECFCBFFFFFEF9
          F7F6FFFFFEFFFEFD0000FFFFFCFFFFFBFCE9E6FFFAF7F6F6F0E4EBE4949893A8
          A19EFFECEEB1939893797F0E00019C9494FFFFFCF6F1F0FFFBFAF6F1F0FFFFFE
          0000F8FFFFE4E9E7FFFFFCFFFFFBFFFFFBFFFFFB4B5D56011911000904777C7B
          240F129D8286FFF8F9FBF6F3FFFAFAFFFCFCFEF6F6FFFEFE0000F1FDFDF4FFFD
          FFFFFEFAF5F2FFF8F6FDFAF6F1FFFCDEFDF6000600000300FFF9FBFFF5F9FFFE
          FEF5F0EFFFFEFEF7EFEFFFFEFEFEF6F60000F9FEFDF9FEFDFCFCFCFDFBFAFEFA
          F9FCFAF9F5FCF9F1FCFAF1FCFAF5FBFAFEF9FAFFF9FAFEF9FAFDFBFBFEFCFCFE
          FCFCFFFDFDFFFEFE0000}
        LookAndFeel.Kind = lfOffice11
      end
      object Panel1: TPanel
        Left = 352
        Top = 196
        Width = 441
        Height = 89
        TabOrder = 4
        Visible = False
        object Label18: TLabel
          Tag = 1
          Left = 8
          Top = 14
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = 'Leitura'
        end
        object Label19: TLabel
          Tag = 1
          Left = 24
          Top = 38
          Width = 19
          Height = 13
          Alignment = taRightJustify
          Caption = 'CPF'
        end
        object Label20: TLabel
          Tag = 1
          Left = 200
          Top = 62
          Width = 100
          Height = 13
          Alignment = taRightJustify
          Caption = 'Data para Dep'#243'sito'
        end
        object Label21: TLabel
          Tag = 1
          Left = 213
          Top = 40
          Width = 87
          Height = 13
          Alignment = taRightJustify
          Caption = 'Valor do Cheque'
        end
        object Edit1: TEdit
          Left = 48
          Top = 8
          Width = 377
          Height = 19
          TabOrder = 0
          OnExit = Edit1Exit
          OnKeyDown = Edit1KeyDown
        end
        object edtCPF: TEdit
          Left = 48
          Top = 32
          Width = 121
          Height = 19
          TabOrder = 1
          OnKeyDown = edtCPFKeyDown
        end
        object edtDtDeposito: TMaskEdit
          Left = 304
          Top = 56
          Width = 120
          Height = 19
          EditMask = '!90/90/0000;1;_'
          MaxLength = 10
          TabOrder = 3
          Text = '  /  /    '
          OnExit = edtDtDepositoExit
          OnKeyDown = edtDtDepositoKeyDown
        end
        object edtValorCheque: TcxCurrencyEdit
          Left = 304
          Top = 32
          Width = 121
          Height = 21
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taBottomJustify
          Properties.DisplayFormat = ',0.00;-,0.00'
          Properties.ValidateOnEnter = True
          Style.BorderStyle = ebsFlat
          TabOrder = 2
          OnKeyDown = edtValorChequeKeyDown
        end
        object cxButton2: TcxButton
          Left = 8
          Top = 56
          Width = 75
          Height = 25
          Caption = 'Fechar'
          TabOrder = 4
          OnClick = cxButton2Click
          LookAndFeel.NativeStyle = True
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'T'#237'tulos Liquidados'
      ImageIndex = 1
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 1073
        Height = 455
        DataGrouping.GroupLevels = <>
        DataSource = dsTitulos
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh3Enter
        OnKeyUp = DBGridEh3KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTituloLoteLiq'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Visible = False
            Width = 74
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoteLiq'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTitulo'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenc'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'cNrTit'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 100
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 37
          end
          item
            EditButtons = <>
            FieldName = 'cNmEspTit'
            Footers = <>
            ReadOnly = True
            Width = 214
          end
          item
            EditButtons = <>
            FieldName = 'nValDesconto'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nValJuro'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nSaldoTit'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nValLiq'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clBlack
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'cNmTerceiro'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 206
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object btSugerirTitulos: TcxButton
        Left = 0
        Top = 456
        Width = 145
        Height = 28
        Caption = 'Sugerir T'#237'tulos'
        TabOrder = 1
        OnClick = btSugerirTitulosClick
        Glyph.Data = {
          26040000424D2604000000000000360000002800000012000000120000000100
          180000000000F003000000000000000000000000000000000000FFFFFEFFFFFE
          FFFDFDFFFEFDFBF9F8F7F9F9F7F9F9F5FAF9F7F9F9F7F9FAF5F9FAF5F9FAF9FB
          FBFBFBFBFBFBFBFCFCFCFDFDFDFFFEFE0000FFFEFBFFFCFAFFFCFAFFFCF9FFFE
          FAFAFFFDF4FCFB8D98967C81829AA1A46D797D8794968D9392EAEDEBFFFFFEFF
          FFFEEEECEBFFFFFE0000F4F5F10700009D8B8AFFF1EFFBF9F8F0FEFC6A8D8900
          0604001A1A000507000A0E6A8D9082908E8B8F8AF4F7F5686967110F0EFFFCFB
          0000F1FFFFEDFBF9060B0A7F7E80FAFFFF00030432817E7BECE849D3CC5FE4E1
          6ED5D82A6E73000603888F8A7C817F010402FFFFFEF9F7F60000ECFFFFE9FFFF
          DBECEFE6F9FE678187619EA25DD4D347E9E426E6DF1BDCD959FAFE6FE0E35582
          7F7D8781868D8AF8FDFBF8FBF9FFFFFE0000F1FFFFE3F6FBEAFFFFDCFFFF0006
          0B7AE3E660F8F934F2F11FF7F60EF2F119F6FA31D8DB9CDDDB010F096F7A77F8
          FFFEF5FAF8F7FAF80000F7FDFF000006000008CFF6FF00141C5ACCD25CF2F844
          F6FB21E9EF21F9FF07EAF341F5FA8BD2CF0008019BA9A5000603000200FBFFFE
          0000FFFEFFFFFDFFF5F4FEE6FAFF00081283DCE67AE8F482FFFF7CFFFF50EEFF
          3EF2FF52E6F2B1EFEF000400808F8BECFAF6EEF7F4FAFFFE0000FDFFFFEDECF0
          FDFCFFEFFEFF5180883C89927AE3EE8CFFFF6CF9FF55F0FF43E8FB64ECF8558F
          8E6C7E77E7F6F2F0FEFAF8FFFEF6FDFA0000EEFFF9ECFFFB000908698082DAFF
          FF000B0E7AFCFB3CE8E633FBFA21EFF03BF6FE51DDE300100C83958E71807C00
          0602F7FFFDF7FEFB0000EFFFF9000B016C7A74EFFDFBE0F5F6BDF3F400201C3B
          D3CD3DF1EC3CEFEC4CE0E600171C4F7C79E6F6EFF4FFFE7C8784000200F6FBF9
          0000F8FFF8F6FCF1FFFFF8FEEFEDFFFDFFF8FFFF0005040013123D8A8C2D757C
          57879300010A91A3A2F5FFFBEBF4F1FAFFFEFBFFFEF8FBF90000F0F4E9FFF9F0
          FFF1ECFFFDFBFFEFEEFFF9F96C71709EAAACE5EFF68A8C96837A8708000A787A
          7AFAFFFCEDF2F0EDF0EEF0F1EFF4F5F30000FFFFF9FFF6F1FFFDF9FFEDE8FFFF
          FBF5F9F4191716817678987E84B89BA49E89929C9096A39F9ECECFCBFFFFFEF9
          F7F6FFFFFEFFFEFD0000FFFFFCFFFFFBFCE9E6FFFAF7F6F6F0E4EBE4949893A8
          A19EFFECEEB1939893797F0E00019C9494FFFFFCF6F1F0FFFBFAF6F1F0FFFFFE
          0000F8FFFFE4E9E7FFFFFCFFFFFBFFFFFBFFFFFB4B5D56011911000904777C7B
          240F129D8286FFF8F9FBF6F3FFFAFAFFFCFCFEF6F6FFFEFE0000F1FDFDF4FFFD
          FFFFFEFAF5F2FFF8F6FDFAF6F1FFFCDEFDF6000600000300FFF9FBFFF5F9FFFE
          FEF5F0EFFFFEFEF7EFEFFFFEFEFEF6F60000F9FEFDF9FEFDFCFCFCFDFBFAFEFA
          F9FCFAF9F5FCF9F1FCFAF1FCFAF5FBFAFEF9FAFFF9FAFEF9FAFDFBFBFEFCFCFE
          FCFCFFFDFDFFFEFE0000}
        LookAndFeel.Kind = lfOffice11
      end
      object btAddTitulo: TcxButton
        Left = 144
        Top = 456
        Width = 145
        Height = 28
        Caption = 'Adicionar T'#237'tulos'
        TabOrder = 2
        OnClick = btAddTituloClick
        Glyph.Data = {
          96030000424D9603000000000000360000002800000010000000120000000100
          1800000000006003000000000000000000000000000000000000C6D6DEC6D6DE
          C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
          DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6
          D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          008C9C9CC6D6DEC6D6DE000000BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD
          BDBDBDBDBD0000006BD6BD6BD6BD0000008C9C9CC6D6DEC6D6DE000000EFFFFF
          C6948CC6948CC6948CC6948CC6948CC6948CC6948C0000006BD6BD6BD6BD0000
          008C9C9C8C9C9C8C9C9C000000EFFFFFC6948CE7E7E7FFEFDEFFEFDE00000000
          00000000000000006BD6BD6BD6BD000000000000000000000000000000EFFFFF
          C6948CC6948CC6948CC6948C0000006BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6
          BD6BD6BD6BD6BD000000000000EFFFFFFFEFDEFFEFDEFFEFDEFFEFDE0000006B
          D6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD6BD000000000000EFFFFF
          007B5A007B5A007B5A00C6940000000000000000000000006BD6BD6BD6BD0000
          00000000000000000000000000EFFFFF007B5A007B5A00C694007B5A00C69400
          7B5A00C6940000006BD6BD6BD6BD0000008C9C9CC6D6DEC6D6DE000000EFFFFF
          007B5A007B5A007B5A00C694007B5A00C69400C6940000006BD6BD6BD6BD0000
          008C9C9CC6D6DEC6D6DE000000EFFFFFFFEFDEFFEFDEFFEFDEFFEFDEFFEFDEFF
          EFDEFFEFDE0000000000000000000000008C9C9CC6D6DEC6D6DE000000EFFFFF
          C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948CBDBDBD0000
          008C9C9CC6D6DEC6D6DE000000EFFFFFC6948CFFEFDEE7E7E7FFEFDEFFEFDEFF
          EFDEFFEFDEFFEFDEC6948CBDBDBD0000008C9C9CC6D6DEC6D6DE000000EFFFFF
          C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948CBDBDBD0000
          008C9C9CC6D6DEC6D6DE000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEF
          FFFFEFFFFFEFFFFFEFFFFFEFFFFF0000008C9C9CC6D6DEC6D6DE000000000000
          0000000000000000000000000000000000000000000000000000000000000000
          00C6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6
          D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE}
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = True
      end
      object cxButton3: TcxButton
        Left = 288
        Top = 456
        Width = 145
        Height = 28
        Caption = 'Excluir T'#237'tulos'
        TabOrder = 3
        OnClick = cxButton3Click
        Glyph.Data = {
          32040000424D3204000000000000360000002800000013000000110000000100
          180000000000FC03000000000000000000000000000000000000FAFBFFFFFBFC
          FFFAF6FFFDF5FFFFF5FFFFF4FFFFF4FFFDF1FFFDF1FFFFF5FFFFF7FFFAF4FFF8
          F0FFF6E8FFFDEDFFFBEFFFF5EAFFF6F0FFFEFC000000FFFEFFFFFBF7F1E0D7E5
          D0C1EBD1C0F9DCC7EAC9B5F4D1BDFDD8C4EBC5B3E7C3B1F9D5C5FDD9C7FFE2CE
          FFE1CBF3D3C0E3C6B7F0D7CDFFFAF3000000FFFFFBFFFAF1DFC8B9B193809B77
          5FB0886CA17558A97A5EB9896DB7876BB6856BB7866CAD7C62A7785DA0725A9E
          755EB69280E8CBBDFFFBF3000000FFFFF7FFFAEBE8CBB69F7A60623617572705
          743E1B6E34106327037438147437156629076A2F0F743D1E582406623318A97E
          69F6D4C4FFFDF2000000FFF9F0FFFAEAFDDDC6AA826655240467310EC28760AD
          7048692B036A2B05692B05753713B67A56B17551703815643111B4876CFFDDC9
          FFF7E9000000FFFFFBFFFFF4F4D6C5A57C635C2A0C6F3616C88C68FFCCA8CD93
          6F602805592501A67352E6B28EBA7D556529006A3510AA7C5DEAC8B0FFFFF000
          0000FFFFFCFFFFF4F6D5C5A77B635E2A0C6329067A3C18C48962FCC39CB47F5A
          C69470DEB08EB8866285471E75360A733C15B18463FAD8C0FFFDEB000000FFFF
          F9FFFFF1F9D6C2AB7C60612B0A8A4F2871330B6C2C03CA8C63D2976FE2AB84B4
          805B622E066E300784441B6C3510A87A5BFFDFC7FFFDED000000FFFFF7FFFFF0
          FDD7BFAC7D5E632C0775370F7031056E2D01C48559EBAE82E9AF85B68057672F
          0665270070310B6E3515AA7B5FF8D6BFFFFDEE000000FFFFF5FFFFEFFFD7BEAE
          7D5D632C076224007D3C0FC98659E6A578CA8C5EC68B5EFBC295D2986E74350F
          5617007A4225B98B73F0CCBAFFFBEC000000FFFFF5FFFFEFFDD7BFAB7D5E612B
          0882451DC38458FFC397B7794B7C3F136F3407CB9469FFC89FB87B596024066E
          381FAF826DF0CEBEFFFFF4000000FFFFF7FFFFF1F7D6C2A57C635A2B0B6B3512
          B47B54D1966E7A3F17713910632D046E3A12B9845FBC83646E361D5826109E73
          62F8D6C9FFFEF7000000FFFFFBFFFFF4F0D6C69E7B67532A1150210551200067
          3413461300582806744624491C005526075C290F5D2A16532513A47D6FFCDED3
          FFEFE9000000FFFEFEFFFBF8DCC7BFA88D7F9A7969A3806CA78169A98069A981
          68A98168A78268A58268A78169A37865A87B6DA67F71B39086DFC3BCFFF9F400
          0000FFFEFFFFFEFEF5E6E3E0CDC6E3CDC2EAD0C2EBD1C1EDD0C1EDD1C0EDD1C0
          EDD1C0EBD1C1EDD0C1F3D0C3F4D0C6EDCCC3E9CDC6F7E1DCFFFBFA000000FAFA
          FFFFFDFFFFF8F8FFF7F3FFFFF9FFFFF8FFFFF7FFFFF7FFFFF7FFFFF7FFFFF7FF
          FFF8FFFFF7FFFEF7FFFEF7FFFEF8FFFAF5FFF8F7FFFDFD000000F7FAFFFDFCFE
          FEFCFCFFFCFBFFFFFCFFFFFCFFFFFBFFFFFBFFFFFBFFFFFCFFFFFCFFFFFCFFFE
          FCFFFCF9FFFDFAFFFEFBFFFEFCFFFDFDFFFAFB000000}
        LookAndFeel.Kind = lfOffice11
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Recurso Pr'#243'prio'
      ImageIndex = 2
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 1073
        Height = 485
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsRecurso
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh2Enter
        OnKeyUp = DBGridEh2KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdRecursoLoteLiq'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoteLiq'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdContaBancaria'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 32
          end
          item
            EditButtons = <>
            FieldName = 'cNmContaBancaria'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 173
          end
          item
            EditButtons = <>
            FieldName = 'nCdFormaPagto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 36
          end
          item
            EditButtons = <>
            FieldName = 'cNmFormaPagto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 207
          end
          item
            EditButtons = <>
            FieldName = 'nValRecurso'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clBlack
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 120
          end
          item
            EditButtons = <>
            FieldName = 'iNrCheque'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'dDtBomPara'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Segoe UI'
            Footer.Font.Style = []
            Footers = <>
            Width = 84
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit19: TDBEdit [40]
    Tag = 1
    Left = 580
    Top = 128
    Width = 120
    Height = 19
    DataField = 'nValTaxaAdm'
    DataSource = dsMaster
    TabOrder = 21
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM LoteLiq'
      ' WHERE nCdLoteLiq = :nPK')
    Left = 480
    Top = 48
    object qryMasternCdLoteLiq: TIntegerField
      FieldName = 'nCdLoteLiq'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMastercTipoLiq: TStringField
      FieldName = 'cTipoLiq'
      FixedChar = True
      Size = 1
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasternCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryMasternValDinheiro: TBCDField
      FieldName = 'nValDinheiro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValChTerceiro: TBCDField
      FieldName = 'nValChTerceiro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValRecursoProp: TBCDField
      FieldName = 'nValRecursoProp'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValCredAnterior: TBCDField
      FieldName = 'nValCredAnterior'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValTotalLote: TBCDField
      FieldName = 'nValTotalLote'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValTitLiq: TBCDField
      FieldName = 'nValTitLiq'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValCredTerceiro: TBCDField
      FieldName = 'nValCredTerceiro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternSaldoLote: TBCDField
      FieldName = 'nSaldoLote'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtFecham: TDateTimeField
      FieldName = 'dDtFecham'
    end
    object qryMasternCdUsuarioFecham: TIntegerField
      FieldName = 'nCdUsuarioFecham'
    end
    object qryMastercSiglaEmp: TStringField
      FieldKind = fkLookup
      FieldName = 'cSiglaEmp'
      LookupDataSet = qryEmpresa
      LookupKeyFields = 'nCdEmpresa'
      LookupResultField = 'cSigla'
      KeyFields = 'nCdEmpresa'
      LookupCache = True
      Size = 5
      Lookup = True
    end
    object qryMastercNmEmpresa: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmEmpresa'
      LookupDataSet = qryEmpresa
      LookupKeyFields = 'nCdEmpresa'
      LookupResultField = 'cNmEmpresa'
      KeyFields = 'nCdEmpresa'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMasternCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
    object qryMasternValTaxaAdm: TBCDField
      FieldName = 'nValTaxaAdm'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtLoteLiq: TDateTimeField
      FieldName = 'dDtLoteLiq'
    end
  end
  inherited dsMaster: TDataSource
    Top = 48
  end
  inherited qryID: TADOQuery
    Left = 1008
    Top = 40
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 1040
    Top = 40
  end
  inherited qryStat: TADOQuery
    Left = 936
    Top = 40
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 976
    Top = 40
  end
  inherited ImageList1: TImageList
    Left = 976
    Top = 200
    Bitmap = {
      494C010109000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009E9E
      9E00656464006564640065646400656464006564640065646400656464006564
      6400898A8900B9BABA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D5D5D5005157
      570031333200484E4E00313332003D4242003D4242003D4242003D424200191D
      230051575700C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008C908F00191D
      230031333200313332003133320031333200313332003133320031333200191D
      230004050600898A890000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000065646400898A
      8900DADDD700B9BABA00BEBEBE00BEBEBE00BEBEBE00BEBEBE00BEBABC00CCCC
      CC00515757006564640000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000717272003D42
      4200898A8900777B7B007B8585007B8585007B8585007B8585007B8585009392
      92003D424200777B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B6B6B6003133
      32007B858500AEAEAE00A4A8A800A4A8A800A4A8A800A4A8A800A4A8A800484E
      4E0031333200B9BABA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D5D5D500484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00898A
      8900484E4E00C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE007B85
      85003D4242000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00D5D5D5007172
      72003D4242000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE007B858500191D2300191D
      230051575700C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE0071727200484E4E003D42
      420093929200C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E0031333200575D5E005157570051575700575D5E00191D2300191D23009392
      9200CCCCCC00BEBEBE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B6B6
      B6009E9E9E009E9E9E009E9E9E009E9E9E009E9E9E00A4A8A800AEAEAE00C6C6
      C600BEBEBE00BEBEBE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      6300636363006363630063636300636363006363630063636300C6948C00C694
      8C000000000000000000C6948C00C6948C000000000000000000000000000000
      0000C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFFFFF008C9C9C0000000000C6948C000000000000000000000000000000
      00000000000000000000EFFFFF00000000000000840000008400000000000000
      0000EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C00000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C6000000000000000000C6DEC60000000000636363000000
      0000C6948C008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C0000000000EFFF
      FF008C9C9C008C9C9C0000000000C6948C000000000000000000000000000000
      84000000840000000000EFFFFF00EFFFFF00000000000000840000000000EFFF
      FF00EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000BDBDBD000000
      000000000000000000000000000000000000C6948C0000000000EFFFFF008C9C
      9C008C9C9C000000000000000000C6948C000000000000000000000084002100
      C6002100C6000000000000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF008C9C
      9C000000000000000000000000006363630000000000000000002100C6000000
      84002100C6002100C6002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF00092369005157570000000000000000000000000000000000000000000000
      000000000000E7F7F700EFFFFF00000000000000000000000000000000000000
      00000000000000000000000000006363630000000000000084002100C6002100
      C6002100C6000000FF002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF0009236900515757000000000000000000000000000000000000000000EFFF
      FF00EFFFFF00DEBDD600DEBDD600EFFFFF00EFFFFF00C6948C00000000008C9C
      9C008C9C9C008C9C9C000000000063636300000000002100C6000000FF002100
      C6000000FF000000FF0000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF0009236900515757000000000000000000000000008C9C9C00000000000000
      000000000000000000000000000000000000EFFFFF0000000000000000000000
      00008C9C9C008C9C9C000000000063636300000000000000FF002100C6000000
      FF000000FF0000000000EFFFFF00EFFFFF002100C6000000FF0000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      00008C9C9C008C9C9C000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000EFFFFF00000000000000FF002100C6000000FF000000
      0000EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100A56300000000
      000000000000000000000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000000000000000FF000000FF000000FF002100C6000000
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C0000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF000000FF002100
      C6002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000000000008C9C9C00FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100000000006B42
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF002100C6000000
      FF002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000000000008C9C9C008C9C
      9C0000000000000000000000000000000000000000000000000000000000A563
      0000A5630000A563000000000000636363000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF002100
      C600000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A80000000000000000000000000000000000000000000000
      00008C9C9C008C9C9C008C9C9C008C9C9C000000000000000000000000000000
      0000000000000000000000000000636363000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      630000000000000000000000000000000000C6948C00C6948C00C6948C006363
      6300636363006363630063636300636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADBBBA0096ADAF009BB6
      BA0091AAAE00A7BABF0096A9AE00A0B0B6009CACB200A0B0B600A0B0B60093A9
      AE0089A2A6009AB5B9009FB3B400C1D3D2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AEBCB800BECCCA00BAD3D500B8D2
      D800C0D8DE00BFD2D700C6D7DA00B9C8CB00B9C8CB00C0CFD200C2D3D600C8DC
      E100C8E0E600BCD6DC00BBCDCE00ADBBB9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A3B3AC00C2D4CD00C3D8D900C2D7
      D900B5C6C9008F9B9B00939B9A00979E9B009095930089908D009BA3A20093A2
      A4008195960097ACAE00CCDAD800A0ABA8000000000000000000BACACE00BACA
      CE00BACACE00BACACE00BACACE00BACACE00BACACE0000000000C2D2D600AEBE
      C2009DABAC00C6D6DA000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A210000000000A2B6B100C5DBD600CFE4E200ADBF
      BE0002100F00000200000002000010100A000D0E050002020000090D08000A15
      1300000201007A898B00C8D4D400A0ABA90000000000C6D6DA00C6DADA00C6DA
      DA00C6DADA00CADADE00CDDEE200CDDEE200CDDEE200CEE2E400BECED200636C
      6C003D4242009AA6AA0000000000000000000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000091AAA600BFDAD600BED1CE00C9D6
      D40000080600C5C7C100C5C3BB00BEB9B000C2BEB300B9B7AD00D8D6CE00A7AC
      AA000F1A18008D9A9800CDD9DB0097A3A50000000000C2D6D600C6D6DA00C6DA
      DA00CDDEE20000000000ABB9BA00ABB9BA00B2C1C200000000005C646500AEAE
      AE00898A89003D42420000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF000923690051575700000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000094B0B000C2E0E100BDCDCC00CDD6
      D300050B0600F8F4E900EFE6D900FFF5E500FFF3E100FFF2E200E3DACD00C9CA
      C100000300008E979400C8D4DA00A2AFB70000000000C2D6D600C6DADA00CDDE
      E200A6B4B60051575700191D230031333200575D5E00484E4E00BEBEBE00898A
      890031333200A2AFB000000000000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000096ACAA00C5DADB00C4D6D700CAD6
      D60000020000F0EADF00F7EADA00FFEFDC00FFEFDA00FCEBD800FFFCEC00C7C8
      BF0000020000A5B1B100C4D7DC0097AAAF0000000000C2D6D600CADEDE00909E
      A0003133320084909100D2DEDF00CEDADE00777B7B003D4242009E9E9E003D42
      42009AA6AA00D6EAEB00000000000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000099ABAA00C6DBD900C4D5D800C9D6
      D80000020000F0EADF00F7EADA00FFEFDA00FFF3DD00F5E2CD00F0E3D300B2B4
      AE000C1512007A878900C3D7DC0096AAAF0000000000C2D6D600C6DADA003D42
      4200BECED200BAC5C600D5D5D500CCD5D500BECACA00ABB9BA00191D23008490
      9100CDDEE200CADEDE00000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000099ABAA00C6DADB00C6D5D800C9D6
      D80000010000EEEADF00F5EBDA00FEEEDD00FDECD900FFFFEE00D8CDBF00BCBE
      B8000002010099A6A800C4D7DC0097AAAD0000000000CDDEE2006D7778005157
      5700313332003133320031333200313332003133320031333200515757005C64
      6500C2D2D200CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000ECEAE000F2EBDC00FBEEDE00FAECDA00FFFBE900E6DED100C1C5
      C0000001000089969800C4D7DC0097AAAD0000000000D3E4E500575D5E00575D
      5E001070820007F0F90007F0F90007F0F90007F0F9003D424200575D5E00484E
      4E00C6D6DA00CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000EBE9E100EEEADF00F7EEE000E3D9C800F6EDDF00FCF8ED00B6BC
      B700070F0E0093A0A200C6D6DC0097AAAD0000000000D3E4E500575D5E00575D
      5E00191D2300107082001070820010708200107082003133320071727200484E
      4E00C2D6D600CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8D9DC00C8D4D800CAD6
      D80000010100E5E9E300E9EAE100F0EEE300E3DFD400F4F2E7000203000099A0
      9D0000020200C3CFD100C6D7DA0099ABAC0000000000C6DADA00A6B4B6003D42
      42006D6C6B00777B7B00898A89008C908F00939292008E9A9A003D42420096A3
      A600C6DADA00CADADE00000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A003163630031636300000000009BACAF00C7D7DD00CDD9DD00C6D2
      D40000020200FBFFFC00F6FAF400EFF2E900FFFFF700F0F3EA00000200000001
      0000CDD7D700C8D4D600C5D7D8009AACAD0000000000C2D6D600CEE2E4005157
      57005C646500909EA00000000000D6E6EA00B2C1C2007B8585005C646500D3E4
      E500C6D6DA00CADADA00000000000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B5003163630000000000000000009AABAE00C3D3D900CFDBDF00C1CC
      D00000010300000200000003000001080100000500000E150E0000020000CFD9
      D900BECACC00DEE9ED00CFDEE0009CAEAF0000000000C2D6D600CADEDE00C2D2
      D2005C646500484E4E003D4242003D424200484E4E00636C6C00CADADE00CADA
      DA00C6DADA00CADADE0000000000000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A800000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      000000000000000000000000000000000000A5B8BD00B7C7CD00C5D1D500BDC8
      CC00CCD8DA00D7E3E300B3C0BE00BFCDC900E0EEEA00B5C3BF00E0EDEB00B4C0
      C200BBC7C900E5F0F400B7C5C400A5B5B40000000000BED2D200C2D2D200C2D6
      D600D6EAEB00A6B4B6007B8585007B858500AFBDBE00D6EAEB00C2D2D600C2D2
      D200C2D2D200C2D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC600000000000000000000000000B1C4C900AEBEC50095A1A500ADB8
      BC00B2BEC200AAB8B70097A8A500A5B6B30094A5A20096A7A40090A19E009EAB
      AD00BBC7CB00939EA200B2C0BF00B8C6C4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF000000000000FFFF000000000000
      E003000000000000C003000000000000C003000000000000C003000000000000
      C003000000000000C003000000000000C003000000000000C007000000000000
      C007000000000000C003000000000000C003000000000000C003000000000000
      E003000000000000FFFF000000000000FFFFFFFF81C0FFFFC000F0030180FFFF
      DFE0E0010000E0035000C0010000C003D00280010000C003CF0680010000C003
      B9CE00010000C003A00200010000C0033F6200010000C003200200010000C003
      200E00010000C003200280030181C003800280038181C0038FC2C0078181C003
      C03EE00F8181FFFFC000F83F8383FFFF8000FFFFFFFFFFFF0000FFFFFFFFFE00
      0000C043E003FE0000008003C003000000008443C003000000008003C0030000
      00008003C003000000008003C003000000008003C003000000008003C0030000
      00008003C003000000008003C003000000008203C003000100008003C0030003
      00008003FFFF00770000FFFFFFFF007F00000000000000000000000000000000
      000000000000}
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cCNPJCPF, cNmTerceiro'
      '  FROM TERCEIRO'
      ' WHERE nCdStatus = 1'
      'AND nCdTerceiro = :nPK')
    Left = 624
    Top = 48
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdUsuario int'
      ''
      'Set @nCdUsuario = :nCdUsuario'
      ''
      'SELECT nCdContaBancaria'
      
        '      ,CASE WHEN (cFlgCaixa = 0 AND cFlgCofre = 0) THEN (Convert' +
        '(VARCHAR(5),nCdBanco) + '#39' - '#39' + Convert(VARCHAR(5),cAgencia) + '#39 +
        ' - '#39' + nCdConta + '#39' - '#39' + IsNull(cNmTitular,'#39' '#39')) '
      '            ELSE nCdConta'
      '       END AS cNmContaBancaria'
      '      ,cFlgDeposito'
      '  FROM ContaBancaria'
      ' WHERE (EXISTS(SELECT 1'
      '                FROM UsuarioContaBancaria UCB'
      
        '               Where UCB.nCdContaBancaria = ContaBancaria.nCdCon' +
        'taBancaria'
      
        '                 AND UCB.nCdUsuario = @nCdUsuario) OR (cFlgCofre' +
        ' = 1 AND (   nCdUsuarioOperador IS NULL'
      
        '                                                                ' +
        '          OR nCdUsuarioOperador = @nCdUsuario)))'
      '   AND ContaBancaria.nCdContaBancaria = :nPK')
    Left = 592
    Top = 48
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariacNmContaBancaria: TStringField
      FieldName = 'cNmContaBancaria'
      ReadOnly = True
      Size = 100
    end
    object qryContaBancariacFlgDeposito: TIntegerField
      FieldName = 'cFlgDeposito'
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      ',cSigla'
      ',cNmEmpresa'
      'FROM EMPRESA')
    Left = 520
    Top = 48
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryChequeLote: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryChequeLoteBeforePost
    AfterPost = qryChequeLoteAfterPost
    BeforeDelete = qryChequeLoteBeforeDelete
    AfterDelete = qryChequeLoteAfterDelete
    Parameters = <
      item
        Name = 'nCdLoteLiq'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM ChequeLoteLiq'
      ' WHERE nCdLoteLiq = :nCdLoteLiq')
    Left = 48
    Top = 296
    object qryChequeLotenCdChequeLoteLiq: TAutoIncField
      FieldName = 'nCdChequeLoteLiq'
    end
    object qryChequeLotenCdLoteLiq: TIntegerField
      FieldName = 'nCdLoteLiq'
    end
    object qryChequeLotenCdBanco: TIntegerField
      DisplayLabel = 'Cheques do Lote|Bco'
      FieldName = 'nCdBanco'
      DisplayFormat = '###0'
    end
    object qryChequeLotecAgencia: TStringField
      DisplayLabel = 'Cheques do Lote|Ag.'
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryChequeLotecConta: TStringField
      DisplayLabel = 'Cheques do Lote|Nr. Conta'
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryChequeLotecDigito: TStringField
      DisplayLabel = 'Cheques do Lote|DV'
      FieldName = 'cDigito'
      FixedChar = True
      Size = 1
    end
    object qryChequeLotecCNPJCPF: TStringField
      DisplayLabel = 'Cheques do Lote|CNPJ/CPF'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequeLotenValCheque: TBCDField
      DisplayLabel = 'Cheques do Lote|Valor Cheque'
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryChequeLotedDtDeposito: TDateTimeField
      DisplayLabel = 'Cheques do Lote|Dt. Dep'#243'sito'
      FieldName = 'dDtDeposito'
    end
    object qryChequeLoteiNrCheque: TIntegerField
      DisplayLabel = 'Cheques do Lote|Nr Cheque'
      FieldName = 'iNrCheque'
    end
    object qryChequeLotenValTaxaAdm: TBCDField
      DisplayLabel = 'Cheques do Lote|Val. Taxa Adm.'
      FieldName = 'nValTaxaAdm'
      Precision = 12
      Size = 2
    end
    object qryChequeLoteiDiasVenc: TIntegerField
      DisplayLabel = 'Cheques do Lote|Dias'
      FieldName = 'iDiasVenc'
    end
  end
  object dsChequeLote: TDataSource
    DataSet = qryChequeLote
    Left = 88
    Top = 296
  end
  object qryRecurso: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryRecursoBeforePost
    AfterPost = qryRecursoAfterPost
    BeforeDelete = qryRecursoBeforeDelete
    AfterDelete = qryRecursoAfterDelete
    OnCalcFields = qryRecursoCalcFields
    Parameters = <
      item
        Name = 'nCdLoteLiq'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM RecursoLoteLiq'
      'WHERE nCdLoteLiq = :nCdLoteLiq')
    Left = 48
    Top = 392
    object qryRecursonCdRecursoLoteLiq: TAutoIncField
      FieldName = 'nCdRecursoLoteLiq'
    end
    object qryRecursonCdLoteLiq: TIntegerField
      FieldName = 'nCdLoteLiq'
    end
    object qryRecursonCdContaBancaria: TIntegerField
      DisplayLabel = 'Conta Banc'#225'ria D'#233'bito|C'#243'd'
      FieldName = 'nCdContaBancaria'
    end
    object qryRecursonCdFormaPagto: TIntegerField
      DisplayLabel = 'Forma de Liquida'#231#227'o|C'#243'd'
      FieldName = 'nCdFormaPagto'
    end
    object qryRecursoiNrCheque: TIntegerField
      DisplayLabel = 'Forma de Liquida'#231#227'o|Nr Cheque'
      FieldName = 'iNrCheque'
    end
    object qryRecursodDtBomPara: TDateTimeField
      DisplayLabel = 'Forma de Liquida'#231#227'o|Data Dep'#243'sito'
      FieldName = 'dDtBomPara'
      EditMask = '!99/99/9999;1;_'
    end
    object qryRecursonValRecurso: TBCDField
      DisplayLabel = 'Forma de Liquida'#231#227'o|Valor'
      FieldName = 'nValRecurso'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryRecursocNmContaBancaria: TStringField
      DisplayLabel = 'Conta Banc'#225'ria D'#233'bito|Dados Conta'
      FieldKind = fkCalculated
      FieldName = 'cNmContaBancaria'
      Size = 50
      Calculated = True
    end
    object qryRecursocNmFormaPagto: TStringField
      DisplayLabel = 'Forma de Liquida'#231#227'o|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmFormaPagto'
      Size = 50
      Calculated = True
    end
  end
  object dsRecurso: TDataSource
    DataSet = qryRecurso
    Left = 712
    Top = 336
  end
  object qryFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdFormaPagto, cNmformaPagto, nCdTabTipoFormaPagto'
      'FROM FORMAPAGTO'
      'WHERE cFlgAtivo = 1'
      'AND nCdFormaPagto = :nPK')
    Left = 648
    Top = 336
    object qryFormaPagtonCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryFormaPagtocNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryFormaPagtonCdTabTipoFormaPagto: TIntegerField
      FieldName = 'nCdTabTipoFormaPagto'
    end
  end
  object qryTituloLoteLiq: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTituloLoteLiqBeforePost
    BeforeDelete = qryTituloLoteLiqBeforeDelete
    AfterDelete = qryTituloLoteLiqAfterDelete
    OnCalcFields = qryTituloLoteLiqCalcFields
    Parameters = <
      item
        Name = 'nCdLoteLiq'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TituloLoteLiq'
      'WHERE nCdLoteLiq = :nCdLoteLiq')
    Left = 48
    Top = 352
    object qryTituloLoteLiqnCdTituloLoteLiq: TAutoIncField
      FieldName = 'nCdTituloLoteLiq'
    end
    object qryTituloLoteLiqnCdLoteLiq: TIntegerField
      FieldName = 'nCdLoteLiq'
    end
    object qryTituloLoteLiqnCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulo|C'#243'd'
      FieldName = 'nCdTitulo'
    end
    object qryTituloLoteLiqcNrTit: TStringField
      DisplayLabel = 'T'#237'tulo|Nr. Tit.'
      FieldKind = fkLookup
      FieldName = 'cNrTit'
      LookupDataSet = qryTitulo
      LookupKeyFields = 'nCdTitulo'
      LookupResultField = 'cNrTit'
      KeyFields = 'nCdTitulo'
      Size = 50
      Lookup = True
    end
    object qryTituloLoteLiqiParcela: TIntegerField
      DisplayLabel = 'T'#237'tulo|Parc.'
      FieldKind = fkCalculated
      FieldName = 'iParcela'
      ReadOnly = True
      Calculated = True
    end
    object qryTituloLoteLiqcNmTerceiro: TStringField
      DisplayLabel = 'T'#237'tulo|Terceiro'
      FieldKind = fkCalculated
      FieldName = 'cNmTerceiro'
      Size = 50
      Calculated = True
    end
    object qryTituloLoteLiqdDtVenc: TDateField
      DisplayLabel = 'T'#237'tulo|Vencto'
      FieldKind = fkCalculated
      FieldName = 'dDtVenc'
      Calculated = True
    end
    object qryTituloLoteLiqnValLiq: TBCDField
      DisplayLabel = 'Valores|Val. Liquidando'
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryTituloLoteLiqnValJuro: TBCDField
      DisplayLabel = 'Valores|Juros'
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTituloLoteLiqnSaldoTit: TBCDField
      DisplayLabel = 'Valores|Saldo Atual'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTituloLoteLiqcNmEspTit: TStringField
      DisplayLabel = 'T'#237'tulo|Esp'#233'cie'
      FieldKind = fkCalculated
      FieldName = 'cNmEspTit'
      Size = 50
      Calculated = True
    end
    object qryTituloLoteLiqnValDesconto: TBCDField
      DisplayLabel = 'Valores|Desconto Antecip.'
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
  end
  object dsTitulos: TDataSource
    DataSet = qryTituloLoteLiq
    Left = 488
    Top = 464
  end
  object qryTitulo: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'dDtMov'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'cSenso'
        DataType = ftString
        Precision = 1
        Size = 1
        Value = 'C'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @dDtMov VARCHAR(10)'
      ''
      'Set @dDtMov = :dDtMov'
      ''
      'SELECT nCdTitulo'
      '      ,nCdSP'
      '      ,cNrTit'
      '      ,iParcela'
      '      ,dDtVenc'
      '      ,Titulo.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,nSaldoTit'
      '      ,nCdEmpresa'
      '      ,EspTit.cNmEspTit'
      
        '      ,(dbo.fn_SimulaJurosTitulo(Titulo.nCdTitulo,Convert(DATETI' +
        'ME,@dDtMov,103)) + dbo.fn_SimulaMultaTitulo(Titulo.nCdTitulo)) a' +
        's nValJuro'
      
        '      ,dbo.fn_SimulaDescontoAntecipTitulo(Titulo.nCdTitulo,Conve' +
        'rt(DATETIME,@dDtMov,103)) as nValDesconto'
      
        '      ,(nSaldoTit + (dbo.fn_SimulaJurosTitulo(Titulo.nCdTitulo,C' +
        'onvert(DATETIME,@dDtMov,103))'
      '                  + dbo.fn_SimulaMultaTitulo(Titulo.nCdTitulo))'
      
        '                  - dbo.fn_SimulaDescontoAntecipTitulo(Titulo.nC' +
        'dTitulo,Convert(DATETIME,@dDtMov,103))) nSaldoTitFinal'
      '  FROM Titulo'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdT' +
        'erceiro'
      
        '       INNER JOIN EspTit   ON EspTit.nCdEspTit     = Titulo.nCdE' +
        'spTit'
      ' WHERE nCdTitulo   = :nPK'
      '   AND cSenso      = :cSenso'
      '   AND nCdEmpresa  = :nCdEmpresa'
      '   AND dDtCancel  IS NULL'
      '   AND dDtBloqTit IS NULL'
      '   AND cFlgPrev    = 0')
    Left = 776
    Top = 520
    object qryTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTitulonCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryTitulocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTituloiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTitulodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTitulonSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTitulonCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryTitulocNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTitulonValJuro: TBCDField
      FieldName = 'nValJuro'
      ReadOnly = True
      Precision = 13
      Size = 2
    end
    object qryTitulonValDesconto: TBCDField
      FieldName = 'nValDesconto'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
  end
  object qryVerificaDup: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 896
    Top = 544
  end
  object usp_Processa_LoteLiq: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_PROCESSA_LOTELIQ;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLoteLiq'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 800
    Top = 96
  end
  object qryLocalizaCheque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdBanco'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 35
        Size = 35
        Value = '0'
      end
      item
        Name = 'cAgencia'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 35
        Size = 35
        Value = '1'
      end
      item
        Name = 'cConta'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 35
        Size = 35
        Value = '1'
      end
      item
        Name = 'cDigito'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 35
        Size = 35
        Value = '1'
      end
      item
        Name = 'iNrCheque'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 35
        Size = 35
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cCNPJCPF'
      '      ,nValCheque'
      '      ,nCdTerceiroPort'
      '      ,nCdEmpresa'
      ',dDtDeposito'
      '  FROM Cheque'
      
        ' WHERE cChave = dbo.fn_ChaveCheque(:nCdBanco, :cAgencia, :cConta' +
        ', :cDigito, :iNrCheque)')
    Left = 320
    Top = 320
    object qryLocalizaChequecCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryLocalizaChequenValCheque: TBCDField
      FieldName = 'nValCheque'
      Precision = 12
      Size = 2
    end
    object qryLocalizaChequenCdTerceiroPort: TIntegerField
      FieldName = 'nCdTerceiroPort'
    end
    object qryLocalizaChequenCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryLocalizaChequedDtDeposito: TDateTimeField
      FieldName = 'dDtDeposito'
    end
  end
  object usp_Sugerir_Titulo: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_SUGERIR_TITULO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLoteLiq'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cTipoLiq'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@cChequeDev'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@dDtMov'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 784
    Top = 632
  end
  object qryLocalizaConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cAgencia'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 4
        Size = 4
        Value = Null
      end
      item
        Name = 'cConta'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 15
        Size = 15
        Value = Null
      end
      item
        Name = 'cDigito'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 1
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 cCNPJCPF'
      '  FROM Cheque'
      ' WHERE nCdBanco = :nCdBanco'
      '   AND cAgencia = :cAgencia'
      '   AND cConta   = :cConta'
      '   AND cDigito  = :cDigito')
    Left = 520
    Top = 336
    object qryLocalizaContacCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 632
    Top = 376
  end
  object DataSource2: TDataSource
    DataSet = qryContaBancaria
    Left = 640
    Top = 384
  end
  object qryContaBancariaRecurso: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      ''
      'Set @nCdLoja = :nCdLoja'
      ''
      'SELECT nCdContaBancaria'
      
        '      ,CASE WHEN (cFlgCaixa = 0 AND cFlgCofre = 0) THEN (Convert' +
        '(VARCHAR(5),nCdBanco) + '#39' - '#39' + Convert(VARCHAR(5),cAgencia) + '#39 +
        ' - '#39' + nCdConta + '#39' - '#39' + IsNull(cNmTitular,'#39' '#39')) '
      '            ELSE nCdConta'
      '       END AS cNmContaBancaria'
      '      ,cFlgDeposito'
      '  FROM ContaBancaria'
      ' WHERE ((@nCdLoja = 0) OR (nCdLoja = @nCdLoja))'
      '   AND ( ((cFlgCofre = 0) AND EXISTS(SELECT 1'
      
        '                                       FROM UsuarioContaBancaria' +
        ' UC'
      
        '                                      WHERE UC.nCdContaBancaria ' +
        '= ContaBancaria.nCdContaBancaria'
      
        '                                        AND UC.nCdUsuario = :nCd' +
        'Usuario))'
      '        OR (cFlgCofre = 1))'
      'AND ContaBancaria.nCdContaBancaria = :nPK')
    Left = 968
    Top = 152
    object qryContaBancariaRecursonCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariaRecursocNmContaBancaria: TStringField
      FieldName = 'cNmContaBancaria'
      ReadOnly = True
      Size = 84
    end
    object qryContaBancariaRecursocFlgDeposito: TIntegerField
      FieldName = 'cFlgDeposito'
    end
  end
  object qryConfereBanco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmBanco'
      'FROM Banco'
      'WHERE nCdBanco = :nPK')
    Left = 460
    Top = 400
    object qryConfereBancocNmBanco: TStringField
      FieldName = 'cNmBanco'
      Size = 50
    end
  end
end
