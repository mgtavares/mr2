unit fRetiraClienteCobradora;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, DBCtrls, ADODB, cxLookAndFeelPainters, cxPC,
  cxControls, cxButtons, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmRetiraClienteCobradora = class(TfrmProcesso_Padrao)
    edtCdTerceiro: TMaskEdit;
    Label5: TLabel;
    edtCdCobradora: TMaskEdit;
    Label1: TLabel;
    qryCobradora: TADOQuery;
    qryCobradoranCdCobradora: TIntegerField;
    qryCobradoracNmCobradora: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryCliente: TADOQuery;
    qryClientenCdTerceiro: TIntegerField;
    qryClientecNmTerceiro: TStringField;
    qryClientecCNPJCPF: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    DBEdit3: TDBEdit;
    cxButton1: TcxButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryTitulos: TADOQuery;
    qryTitulosnCdTitulo: TIntegerField;
    qryTitulosnCdLojaTit: TIntegerField;
    qryTituloscNrTit: TStringField;
    qryTitulosiParcela: TIntegerField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTituloscNmEspTit: TStringField;
    qryTitulosnValTit: TBCDField;
    qryTitulosnValEncargoCobradora: TBCDField;
    qryTitulosnSaldoTit: TBCDField;
    qryTitulosdDtRemCobradora: TDateTimeField;
    dsTitulos: TDataSource;
    qryAux: TADOQuery;
    procedure cxButton1Click(Sender: TObject);
    procedure edtCdTerceiroExit(Sender: TObject);
    procedure edtCdCobradoraExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtCdTerceiroChange(Sender: TObject);
    procedure edtCdCobradoraChange(Sender: TObject);
    procedure edtCdCobradoraKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCdTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRetiraClienteCobradora: TfrmRetiraClienteCobradora;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmRetiraClienteCobradora.cxButton1Click(Sender: TObject);
begin
  inherited;

  qryTitulos.Close;
  qryTitulos.Parameters.ParamByName('nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva ;
  qryTitulos.Parameters.ParamByName('nCdTerceiro').Value  := qryClientenCdTerceiro.Value ;
  qryTitulos.Parameters.ParamByName('nCdCobradora').Value := qryCobradoranCdCobradora.Value ;
  qryTitulos.Open;

  if qryTitulos.eof then
  begin
      MensagemAlerta('Nenhum t�tulo encontrado para este cliente nesta cobradora.') ;
      edtCdTerceiro.SetFocus;
      abort;
  end ;

end;

procedure TfrmRetiraClienteCobradora.edtCdTerceiroExit(Sender: TObject);
begin
  inherited;

  qryCliente.Close;
  PosicionaQuery(qryCliente, edtCdTerceiro.Text) ;

end;

procedure TfrmRetiraClienteCobradora.edtCdCobradoraExit(Sender: TObject);
begin
  inherited;

  qryCobradora.Close;
  PosicionaQuery(qryCobradora, edtCdCobradora.Text) ;

end;

procedure TfrmRetiraClienteCobradora.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (not qryTitulos.Active) or (qryTitulos.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum t�tulo selecionado para retirar da cobradora.') ;
      abort ;
  end ;

  if (MessageDlg('Confirma a retirada destes t�tulos da cobradora ?'+#13#13+'Este processo n�o poder� ser estornado.',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
      exit ;

  qryTitulos.DisableControls;

  frmMenu.Connection.BeginTrans;

  qryTitulos.First ;

  try
      while not qryTitulos.eof do
      begin
          qryAux.SQL.Clear;
          qryAux.SQL.Add('UPDATE Titulo SET nSaldoTit = nSaldoTit - nValEncargoCobradora, nCdCobradora = NULL, cFlgCobradora = 0, dDtRemCobradora = NULL, nCdUsuarioRemCobradora = NULL, nCdItemListaCobRemCobradora = NULL, cFlgRetCobradoraManual = 1 WHERE nCdTitulo = ' + qryTitulosnCdTitulo.AsString) ;
          qryAux.ExecSQL;

          qryAux.SQL.Clear;
          qryAux.SQL.Add('UPDATE Titulo SET nValEncargoCobradora = 0 WHERE nCdTitulo = ' + qryTitulosnCdTitulo.AsString) ;
          qryAux.ExecSQL;

          qryTitulos.Next ;
      end ;
  except
      frmMenu.Connection.RollbackTrans;
      qryTitulos.EnableControls;
      MensagemErro('Erro no processamento.');
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryTitulos.First;

  qryTitulos.EnableControls;

  ShowMessage('T�tulos processados com sucesso.') ;

  qryTitulos.Close;
  edtCdTerceiro.SetFocus;

end;

procedure TfrmRetiraClienteCobradora.edtCdTerceiroChange(Sender: TObject);
begin
  inherited;

  qryTitulos.Close ;

end;

procedure TfrmRetiraClienteCobradora.edtCdCobradoraChange(Sender: TObject);
begin
  inherited;
  qryTitulos.Close ;

end;

procedure TfrmRetiraClienteCobradora.edtCdCobradoraKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(199);

        If (nPK > 0) then
            edtCdCobradora.Text := intToStr(nPK) ;
    end ;

  end ;

end;

procedure TfrmRetiraClienteCobradora.edtCdTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(200);

        If (nPK > 0) then
            edtCdTerceiro.Text := intToStr(nPK) ;
    end ;

  end ;
  
end;

initialization
    RegisterClass(TfrmRetiraClienteCobradora) ;

end.
