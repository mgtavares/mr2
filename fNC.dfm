inherited frmNC: TfrmNC
  Left = 388
  Top = 291
  Width = 201
  Height = 106
  Caption = 'N/C'
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 193
    Height = 50
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 48
    Width = 7
    Height = 13
    Caption = 'N'
  end
  object Label2: TLabel [2]
    Left = 96
    Top = 48
    Width = 7
    Height = 13
    Caption = 'C'
  end
  inherited ToolBar1: TToolBar
    Width = 193
    TabOrder = 2
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit4: TMaskEdit [4]
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    TabOrder = 0
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object MaskEdit3: TMaskEdit [5]
    Left = 24
    Top = 40
    Width = 65
    Height = 21
    BiDiMode = bdRightToLeft
    ParentBiDiMode = False
    TabOrder = 1
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
end
