inherited frmDepositoBancario_ConsultaCheques: TfrmDepositoBancario_ConsultaCheques
  Left = 221
  Top = 176
  Width = 765
  BorderIcons = [biSystemMenu]
  Caption = 'Consulta de Cheques'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 749
  end
  inherited ToolBar1: TToolBar
    Width = 749
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 749
    Height = 435
    Align = alClient
    DataSource = dsCheques
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdBanco'
        Footers = <>
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'iNrCheque'
        Footers = <>
        Width = 85
      end
      item
        EditButtons = <>
        FieldName = 'nValCheque'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cCNPJCPF'
        Footers = <>
        Width = 120
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        Width = 370
      end>
  end
  object qryCheques: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Cheque.nCdCheque'
      '      ,Cheque.nCdBanco'
      '      ,Cheque.iNrCheque'
      '      ,Cheque.nValCheque'
      '      ,Cheque.cCNPJCPF'
      '      ,cNmTerceiro'
      '  FROM ItemDepositoBancario'
      
        '       INNER JOIN ChequeItemDepositoBancario CIDB ON CIDB.nCdIte' +
        'mDepositoBancario = ItemDepositoBancario.nCdItemDepositoBancario'
      
        '       INNER JOIN Cheque                          ON Cheque.nCdC' +
        'heque             = CIDB.nCdCheque'
      
        '       LEFT JOIN Terceiro ON Terceiro.nCdTerceiro = Cheque.nCdTe' +
        'rceiroResp'
      ' WHERE ItemDepositoBancario.nCdItemDepositoBancario = :nPK')
    Left = 184
    Top = 104
    object qryChequesnCdCheque: TAutoIncField
      DisplayLabel = 'Cheques|'
      FieldName = 'nCdCheque'
      ReadOnly = True
    end
    object qryChequesnCdBanco: TIntegerField
      DisplayLabel = 'Cheques|Banco'
      FieldName = 'nCdBanco'
    end
    object qryChequesiNrCheque: TIntegerField
      DisplayLabel = 'Cheques|Nr. Cheque'
      FieldName = 'iNrCheque'
    end
    object qryChequesnValCheque: TBCDField
      DisplayLabel = 'Cheques|Valor'
      FieldName = 'nValCheque'
      Precision = 12
      Size = 2
    end
    object qryChequescCNPJCPF: TStringField
      DisplayLabel = 'Cheques|CNPJ/CPF'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequescNmTerceiro: TStringField
      DisplayLabel = 'Cheques|Cliente Respons'#225'vel'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsCheques: TDataSource
    DataSet = qryCheques
    Left = 232
    Top = 104
  end
end
