unit fTipoResultadoContato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmTipoResultadoContato = class(TfrmCadastro_Padrao)
    qryMasternCdTipoResultadoContato: TIntegerField;
    qryMastercNmTipoResultadoContato: TStringField;
    qryMastercFlgAtivo: TIntegerField;
    qryMastercFlgContatado: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    dsFlgAtivo: TDataSource;
    dsFlgContatado: TDataSource;
    DBLookupComboBox1: TDBLookupComboBox;
    DBLookupComboBox2: TDBLookupComboBox;
    Label3: TLabel;
    Label4: TLabel;
    qryMasternCdTabTipoRestricaoVenda: TIntegerField;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    qryTabTipoRestricaoVenda: TADOQuery;
    qryTabTipoRestricaoVendanCdTabTipoRestricaoVenda: TIntegerField;
    qryTabTipoRestricaoVendacNmTabTipoRestricaoVenda: TStringField;
    DBEdit4: TDBEdit;
    dsTabTipoRestricaoVenda: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoResultadoContato: TfrmTipoResultadoContato;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmTipoResultadoContato.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPORESULTADOCONTATO' ;
  nCdTabelaSistema  := 90 ;
  nCdConsultaPadrao := 196 ;

end;

procedure TfrmTipoResultadoContato.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;

end;

procedure TfrmTipoResultadoContato.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryTabTipoRestricaoVenda.Close;
  PosicionaQuery(qryTabTipoRestricaoVenda, DBEdit3.Text);

end;

procedure TfrmTipoResultadoContato.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryTabTipoRestricaoVenda.Close;
  PosicionaQuery(qryTabTipoRestricaoVenda, qryMasternCdTabTipoRestricaoVenda.AsString);

end;

procedure TfrmTipoResultadoContato.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qryTabTipoRestricaoVenda.Close;

end;

procedure TfrmTipoResultadoContato.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(189);

            If (nPK > 0) then
            begin
                qryMasternCdTabTipoRestricaoVenda.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmTipoResultadoContato) ;

end.
