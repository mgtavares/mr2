unit fProdutoERPInsumos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping, cxPC, cxControls, StdCtrls,
  DBCtrls, cxLookAndFeelPainters, cxButtons;

type
  TfrmProdutoERPInsumos = class(TfrmProcesso_Padrao)
    dsFormulaProduto: TDataSource;
    qryProdutoFormula: TADOQuery;
    qryProdutoFormulanCdProduto: TIntegerField;
    qryProdutoFormulacNmProduto: TStringField;
    qryFormulaProduto: TADOQuery;
    qryFormulaProdutonCdprodutoPai: TIntegerField;
    qryFormulaProdutonCdProduto: TIntegerField;
    qryFormulaProdutocNmProduto: TStringField;
    qryFormulaProdutonQtde: TBCDField;
    qryFormulaProdutonCdFormulaProduto: TAutoIncField;
    qryFormulaProdutoiMetodoUso: TIntegerField;
    qryFormulaProdutocUnidadeMedida: TStringField;
    qryFormulaProdutonFator: TBCDField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    qryFormulaProdutonValCustoUnit: TFloatField;
    qryFormulaProdutonValCustoTotal: TFloatField;
    qryProdutoFormulanValCusto: TBCDField;
    qryProdutoFormulacUnidadeMedida: TStringField;
    qryFormulaProdutonFatorConversaoUM: TBCDField;
    qryFormulaProdutocUnidadeMedidaEstoque: TStringField;
    qryProdutoFormulacFlgFantasma: TIntegerField;
    qryProdutoFormulanCdTipoObtencao: TIntegerField;
    qryAtualizaPrecoCusto: TADOQuery;
    GroupBox1: TGroupBox;
    cxPageControl2: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    cmbTipoOP: TDBLookupComboBox;
    Label1: TLabel;
    GroupBox2: TGroupBox;
    cxButton1: TcxButton;
    DBGridEh1: TDBGridEh;
    qryTipoOP: TADOQuery;
    dsTipoOP: TDataSource;
    qryTipoOPnCdTipoOP: TIntegerField;
    qryTipoOPcNmTipoOP: TStringField;
    qryRoteiroProducao: TADOQuery;
    dsRoteiroProducao: TDataSource;
    qryRoteiroProducaonCdRoteiroProducao: TIntegerField;
    qryRoteiroProducaonCdProduto: TIntegerField;
    qryRoteiroProducaocNmRoteiro: TStringField;
    qryRoteiroProducaonCdTipoOP: TIntegerField;
    qryRoteiroProducaodDtUltAlt: TDateTimeField;
    qryRoteiroProducaonCdUsuarioUltAlt: TIntegerField;
    qryRoteiroProducaonCdStatus: TIntegerField;
    qryRoteiroProducaocNmUsuario: TStringField;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    qryRoteiroProducaocNmStatus: TStringField;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    qryAux: TADOQuery;
    qryFormulaProdutonCdTabTipoMetodoSaidaEstoque: TIntegerField;
    qryFormulaProdutonCdLocalEstoque: TIntegerField;
    qryFormulaProdutonCdRoteiroEtapaProducao: TIntegerField;
    qryFormulaProdutocNmLocalEstoque: TStringField;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryProdutoFormulanCdTabTipoMetodoUso: TIntegerField;
    qryProdutoFormulanCdTabTipoMetodoSaidaEstoque: TIntegerField;
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryFormulaProdutoBeforePost(DataSet: TDataSet);
    procedure qryFormulaProdutoCalcFields(DataSet: TDataSet);
    procedure qryFormulaProdutonCdProdutoChange(Sender: TField);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryFormulaProdutocUnidadeMedidaChange(Sender: TField);
    procedure exibeEstruturaProduto( nCdProdutoAux:integer );
    procedure exibeEstruturaEtapaProducao( nCdProdutoAux, nCdRoteiroEtapaProducaoAux : integer) ;
    procedure ToolButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryTipoOPAfterScroll(DataSet: TDataSet);
    procedure qryRoteiroProducaoCalcFields(DataSet: TDataSet);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure qryFormulaProdutonCdLocalEstoqueChange(Sender: TField);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdProduto              : integer ;
    nCdRoteiroEtapaProducao : integer ;
  end;

var
  frmProdutoERPInsumos: TfrmProdutoERPInsumos;

implementation

uses fLookup_Padrao, fMenu, fRoteiroProducao_Etapas;

{$R *.dfm}

procedure TfrmProdutoERPInsumos.DBGridEh3KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DbGridEh3.Col = 2) then
        begin

            if (qryFormulaProduto.State = dsBrowse) then
                 qryFormulaProduto.Edit ;

            if (qryFormulaProduto.State = dsInsert) or (qryFormulaProduto.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(76);

                If (nPK > 0) then
                begin
                    qryFormulaProdutonCdProduto.Value := nPK ;
                end ;

            end ;

        end ;

        if (DBGridEh3.Columns[DBGridEh3.Col-1].FieldName = 'nCdLocalEstoque') then
        begin

            if (qryFormulaProduto.State = dsBrowse) then
                 qryFormulaProduto.Edit ;

            if (qryFormulaProduto.State = dsInsert) or (qryFormulaProduto.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(87);

                If (nPK > 0) then
                begin
                    qryFormulaProdutonCdLocalEstoque.Value := nPK ;
                end ;

            end ;


        end ;

    end ;

  end ;

end;

procedure TfrmProdutoERPInsumos.qryFormulaProdutoBeforePost(
  DataSet: TDataSet);
begin

  if (qryFormulaProdutocNmProduto.Value = '') then
  begin
      MensagemAlerta('Selecione o produto.') ;
      abort ;
  end ;

  qryFormulaProdutocUnidadeMedida.Value := Uppercase(qryFormulaProdutocUnidadeMedida.Value) ;

  if (trim(qryFormulaProdutocUnidadeMedida.Value) = '') then
  begin
      MensagemAlerta('Informe a unidade de medida.') ;
      abort ;
  end ;

  frmMenu.qryUnidadeMedida.Close ;
  PosicionaQuery(frmMenu.qryUnidadeMedida, qryFormulaProdutocUnidadeMedida.Value) ;

  if (frmMenu.qryUnidadeMedida.eof) then
  begin
      MensagemAlerta('Unidade de medida n�o cadastrada.') ;
      abort ;
  end ;

  if (DBGridEh3.Columns[6].Visible) and (qryFormulaProdutocNmLocalEstoque.Value = '') then
  begin
      MensagemAlerta('Informe o local de estoque.') ;
      abort ;
  end ;

  if (qryFormulaProdutonQtde.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade.') ;
      abort ;
  end ;

  if (qryFormulaProdutonFator.Value < 1) then
  begin
      MensagemAlerta('O fator de convers�o n�o pode ser menor que 1.') ;
      abort ;
  end ;

  inherited;

  qryFormulaProdutonCdProdutoPai.Value := nCdProduto ;

  if (nCdRoteiroEtapaProducao > 0) then
      qryFormulaProdutonCdRoteiroEtapaProducao.Value := nCdRoteiroEtapaProducao ;

end;

procedure TfrmProdutoERPInsumos.qryFormulaProdutoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryFormulaProduto.State in [dsCalcFields,dsBrowse]) then
  begin

      qryProdutoFormula.Close ;
      PosicionaQuery(qryProdutoFormula, qryFormulaProdutonCdProduto.AsString) ;

      if qryProdutoFormula.Active then
      begin
          qryFormulaProdutocNmProduto.Value            := qryProdutoFormulacNmProduto.Value ;
          qryFormulaProdutocUnidadeMedidaEstoque.Value := qryProdutoFormulacUnidadeMedida.Value ;
          qryFormulaProdutonValCustoUnit.Value         := qryProdutoFormulanValCusto.Value     * qryFormulaProdutonFatorConversaoUM.Value ;
          qryFormulaProdutonValCustoTotal.Value        := qryFormulaProdutonValCustoUnit.Value * qryFormulaProdutonQtde.Value ;
      end ;

      qryLocalEstoque.Close ;
      PosicionaQuery(qryLocalEstoque, qryFormulaProdutonCdLocalEstoque.AsString) ;

      if qryLocalEstoque.Active then
          qryFormulaProdutocNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;

  end ;

end;

procedure TfrmProdutoERPInsumos.qryFormulaProdutonCdProdutoChange(
  Sender: TField);
begin
  inherited;

  if (qryFormulaProduto.State in [dsInsert,dsEdit]) then
  begin

      if (qryFormulaProdutonCdProduto.Value > 0) then
      begin

          qryProdutoFormula.Close ;
          PosicionaQuery(qryProdutoFormula, qryFormulaProdutonCdProduto.AsString) ;

          if qryProdutoFormula.Active then
          begin

              qryFormulaProdutocNmProduto.Value            := qryProdutoFormulacNmProduto.Value ;
              qryFormulaProdutocUnidadeMedidaEstoque.Value := qryProdutoFormulacUnidadeMedida.Value ;

              if (qryFormulaProdutocUnidadeMedida.Value = '') then
              begin
                  qryFormulaProdutocUnidadeMedida.Value    := qryProdutoFormulacUnidadeMedida.Value ;
                  qryFormulaProdutonFatorConversaoUM.Value := 1 ;
              end
              else
              begin

                  // se a unidade de medida da f�rmula for diferente da unidade de medida do estoque do produto --
                  // grava o fator de convers�o de uma unidade para outra                                       --
                  if (Uppercase( qryFormulaProdutocUnidadeMedida.Value ) <> Uppercase( qryFormulaProdutocUnidadeMedidaEstoque.Value )) then
                      qryFormulaProdutonFatorConversaoUM.Value := frmMenu.fnFatorConversaoUM( qryFormulaProdutocUnidadeMedidaEstoque.Value
                                                                                             ,qryFormulaProdutocUnidadeMedida.Value)
                  else qryFormulaProdutonFatorConversaoUM.Value := 1 ;

              end ;

              qryFormulaProdutonValCustoUnit.Value  := qryProdutoFormulanValCusto.Value     * qryFormulaProdutonFatorConversaoUM.Value ;
              qryFormulaProdutonValCustoTotal.Value := qryFormulaProdutonValCustoUnit.Value * qryFormulaProdutonQtde.Value ;

          end ;

          if (qryFormulaProduto.State = dsInsert) then
          begin
          
              qryFormulaProdutonFator.Value                       := 1 ;
              qryFormulaProdutoiMetodoUso.Value                   := qryProdutoFormulanCdTabTipoMetodoUso.Value ;
              qryFormulaProdutonCdTabTipoMetodoSaidaEstoque.Value := qryProdutoFormulanCdTabTipoMetodoSaidaEstoque.Value ;

              {-- quando for um item fantasma, sugere automaticamente como backflush --}
              if (qryProdutoFormulacFlgFantasma.Value = 1) then
                  qryFormulaProdutoiMetodoUso.Value := 2 ;

          end ;

      end ;

  end ;

end;

procedure TfrmProdutoERPInsumos.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}
  
end;

procedure TfrmProdutoERPInsumos.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}


end;

procedure TfrmProdutoERPInsumos.qryFormulaProdutocUnidadeMedidaChange(
  Sender: TField);
begin
  inherited;

  // se a unidade de medida da f�rmula for diferente da unidade de medida do estoque do produto --
  // grava o fator de convers�o de uma unidade para outra                                       --
  if (qryFormulaProdutocUnidadeMedida.Value <> qryFormulaProdutocUnidadeMedidaEstoque.Value) then
      qryFormulaProdutonFatorConversaoUM.Value := frmMenu.fnFatorConversaoUM( qryFormulaProdutocUnidadeMedidaEstoque.Value
                                                                             ,qryFormulaProdutocUnidadeMedida.Value)
  else qryFormulaProdutonFatorConversaoUM.Value := 1 ;

end;

procedure TfrmProdutoERPInsumos.exibeEstruturaProduto(nCdProdutoAux: integer);
begin

    Self.nCdProduto := nCdProdutoAux ;

    qryProdutoFormula.Close ;
    PosicionaQuery(qryProdutoFormula, intToStr( nCdProdutoAux)) ;

    if (qryProdutoFormula.eof) then
    begin
        MensagemErro('O c�digo do produto ' + intToStr( nCdProdutoAux) + ' n�o existe no cadastro de produtos.') ;
        abort ;
    end ;

    {-- se n�o for obtido por produ��o --}
    if (qryProdutoFormulanCdTipoObtencao.Value <> 2) then
    begin

        cxTabSheet1.Enabled            := True  ;
        cxTabSheet2.Enabled            := False ;
        cxPageControl1.ActivePageIndex := 0;

        PosicionaQuery(qryFormulaProduto, intToStr( nCdProdutoAux )) ;

    end ;

    {-- obtido por produ��o --}
    if (qryProdutoFormulanCdTipoObtencao.Value = 2) then
    begin

        cxTabSheet1.Enabled            := False  ;
        cxTabSheet2.Enabled            := True ;
        cxPageControl1.ActivePageIndex := 1;

        qryTipoOP.Close ;
        qryTipoOP.Open ;

        {PosicionaQuery(qryFormulaProduto, intToStr( nCdProdutoAux )) ;}

    end ;

    {-- se n�o veio de uma etapa do roteiro da produ��o, inativa os dados do local de estoque --}
    if (nCdRoteiroEtapaProducao = 0) then
    begin

        DBGridEh3.Columns[6].Visible := False ;
        DBGridEh3.Columns[7].Visible := False ;

    end ;

    Self.ShowModal ;

    qryFormulaProduto.Close ;

end;

procedure TfrmProdutoERPInsumos.ToolButton2Click(Sender: TObject);
begin

  {-- s� atualiza o pre�o de custo do produto pai se a tela n�o foi chamada pela tela de etapas da produ��o --}
  if (qryFormulaProduto.Active) and (qryFormulaProduto.RecordCount > 0) and (nCdRoteiroEtapaProducao = 0) then
  begin

      qryProdutoFormula.Close;
      PosicionaQuery(qryProdutoFormula, intToStr( nCdProduto )) ;

      if (qryProdutoFormulanValCusto.Value <> DBGridEh3.SumList.SumCollection.Items[6].SumValue) then
      begin

          if (MessageDlg('Deseja atualizar o pre�o de custo do produto ?',mtConfirmation,[mbYes,mbNo],0) = MrYes) then
          begin

              frmMenu.Connection.BeginTrans;

              try
                  qryAtualizaPrecoCusto.Close ;
                  qryAtualizaPrecoCusto.Parameters.ParamByName('nCdProduto').Value := Self.nCdProduto ;
                  qryAtualizaPrecoCusto.Parameters.ParamByName('nValCusto').Value  := DBGridEh3.SumList.SumCollection.Items[6].SumValue;
                  qryAtualizaPrecoCusto.ExecSQL;
              except
                  frmMenu.Connection.RollbackTrans;
                  MensagemErro('Erro no processamento.') ;
                  raise ;
              end ;

              frmMenu.Connection.CommitTrans ;

          end ;

      end ;

  end ;

  inherited;

end;

procedure TfrmProdutoERPInsumos.FormShow(Sender: TObject);
begin
  inherited;

  if (qryTipoOP.Eof) and (cxTabSheet2.Enabled) then
  begin
      MensagemErro('Nenhum tipo de ordem de produ��o cadastrado.') ;
      abort ;
  end ;

  if (cxPageControl1.ActivePageIndex = 0) then
      DBGridEh3.SetFocus
  else begin
      cmbTipoOP.KeyValue := qryTipoOPnCdTipoOP.Value ; {-- for�a posicionar o combo em um registro --}
      cmbTipoOP.SetFocus;
  end ;

end;

procedure TfrmProdutoERPInsumos.qryTipoOPAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryRoteiroProducao.Close;
  qryRoteiroProducao.Parameters.ParamByName('nCdProduto').Value := nCdProduto ;
  qryRoteiroProducao.Parameters.ParamByName('nCdTipoOP').Value  := qryTipoOPnCdTipoOP.Value ;
  qryRoteiroProducao.Open;

  DBGridEh1.Columns[3].Width := 179;

end;

procedure TfrmProdutoERPInsumos.qryRoteiroProducaoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryUsuario, qryRoteiroProducaonCdUsuarioUltAlt.asString) ;

  if not qryUsuario.Eof then
      qryRoteiroProducaocNmUsuario.Value := qryUsuariocNmUsuario.Value ;

  PosicionaQuery(qryStatus, qryRoteiroProducaonCdStatus.asString) ;

  if not qryStatus.eof then
      qryRoteiroProducaocNmStatus.Value := qryStatuscNmStatus.Value ;
      
end;

procedure TfrmProdutoERPInsumos.cxButton2Click(Sender: TObject);
begin
  inherited;

  if (not qryRoteiroProducao.Active) or (qryRoteiroProducao.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum roteiro de produ��o selecionado.') ;
      exit ;
  end ;

  if (qryRoteiroProducaonCdStatus.Value = 1) then
  begin
      MensagemAlerta('O roteiro de produ��o selecionado j� est� ativo.') ;
      exit ;
  end ;

  qryAux.Close ;
  qryAux.SQL.Add('SELECT 1 ') ;
  qryAux.SQL.Add('  FROM RoteiroProducao ') ;
  qryAux.SQL.Add(' WHERE nCdProduto          = ' + intToStr(nCdProduto)) ;
  qryAux.SQL.Add('   AND nCdTipoOP           = ' + qryTipoOPnCdTipoOP.AsString) ;
  qryAux.SQL.Add('   AND nCdStatus           = 1') ;
  qryAux.SQL.Add('   AND nCdRoteiroProducao <> ' + qryRoteiroProducaonCdRoteiroProducao.asString) ;
  qryAux.Open;

  if not qryAux.eof then
  begin
      MensagemAlerta('J� existe um roteiro de produ��o ativo para este tipo de ordem de produ��o. Inative-o primeiro.') ;
      exit ;
  end ;

  if (MessageDlg('Confirma a ativa��o deste roteiro de produ��o ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE RoteiroProducao Set nCdStatus = 1, dDtUltAlt = GetDate(), nCdUsuarioUltAlt = ' + intToStr( frmMenu.nCdUsuarioLogado ) + ' WHERE nCdRoteiroProducao = ' + qryRoteiroProducaonCdRoteiroProducao.AsString) ;
      qryAux.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Roteiro de produ��o ativado com sucesso!') ;

  qryRoteiroProducao.Close ;
  qryRoteiroProducao.Open ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmProdutoERPInsumos.cxButton3Click(Sender: TObject);
begin
  inherited;

  if (not qryRoteiroProducao.Active) or (qryRoteiroProducao.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum roteiro de produ��o selecionado.') ;
      exit ;
  end ;

  if (qryRoteiroProducaonCdStatus.Value = 2) then
  begin
      MensagemAlerta('O roteiro de produ��o selecionado j� est� inativo.') ;
      exit ;
  end ;

  if (MessageDlg('Confirma a inativa��o deste roteiro de produ��o ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE RoteiroProducao Set nCdStatus = 2, dDtUltAlt = GetDate(), nCdUsuarioUltAlt = ' + intToStr( frmMenu.nCdUsuarioLogado ) + ' WHERE nCdRoteiroProducao = ' + qryRoteiroProducaonCdRoteiroProducao.AsString) ;
      qryAux.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Roteiro de produ��o inativado com sucesso!') ;

  qryRoteiroProducao.Close ;
  qryRoteiroProducao.Open ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmProdutoERPInsumos.cxButton1Click(Sender: TObject);
var
  cNmRoteiro : string[50] ;
begin
  inherited;

  cNmRoteiro := '' ;
  cNmRoteiro := InputBox('Roteiro de Produ��o','Nome Roteiro','ROTEIRO ' + Trim(qryTipoOPcNmTipoOP.Value) + ' - PRODUTO ' + intToStr(nCdProduto)) ;

  if (cNmRoteiro = '') then
      exit ;

  if (MessageDlg('Confirma a inclus�o deste roteiro de produ��o ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  qryRoteiroProducao.Insert;
  qryRoteiroProducaonCdRoteiroProducao.Value := frmMenu.fnProximoCodigo('ROTEIROPRODUCAO') ;
  qryRoteiroProducaonCdProduto.Value         := nCdProduto ;
  qryRoteiroProducaocNmRoteiro.Value         := cNmRoteiro ;
  qryRoteiroProducaonCdTipoOP.Value          := qryTipoOPnCdTipoOP.Value ;
  qryRoteiroProducaodDtUltAlt.Value          := Now() ;
  qryRoteiroProducaonCdUsuarioUltAlt.Value   := frmMenu.nCdUsuarioLogado ;
  qryRoteiroProducaonCdStatus.Value          := 2 ;
  qryRoteiroProducao.Post ;

end;

procedure TfrmProdutoERPInsumos.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmRoteiroProducao_Etapas ;
begin
  inherited;

  if (not cxTabSheet2.Enabled) then
      exit ;
       
  if (not qryRoteiroProducao.Active) or (qryRoteiroProducao.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum roteiro de produ��o selecionado.') ;
      exit ;
  end ;

  objForm := TfrmRoteiroProducao_Etapas.Create( Self ) ;

  objForm.exibeRoteiroProducao( nCdProduto
                               ,qryTipoOPnCdTipoOP.Value
                               ,qryRoteiroProducaonCdRoteiroProducao.Value );

  freeAndNil( objForm ) ;

end;

procedure TfrmProdutoERPInsumos.exibeEstruturaEtapaProducao(nCdProdutoAux,
  nCdRoteiroEtapaProducaoAux: integer);
begin

    nCdRoteiroEtapaProducao := nCdRoteiroEtapaProducaoAux ;
    nCdProduto              := nCdProdutoAux ;

    cxTabSheet1.Caption := 'Produtos da Etapa de Produ��o' ;

    cxTabSheet1.Enabled            := True  ;
    cxTabSheet2.Enabled            := False ;
    cxPageControl1.ActivePageIndex := 0;

    qryFormulaProduto.Close ;
    qryFormulaProduto.Parameters.ParamByName('nPK').Value                     := nCdProdutoAux ;
    qryFormulaProduto.Parameters.ParamByName('nCdRoteiroEtapaProducao').Value := nCdRoteiroEtapaProducaoAux ;
    qryFormulaProduto.Open ;

    {-- se veio de uma etapa do roteiro da produ��o, ativa os dados do local de estoque --}
    if (nCdRoteiroEtapaProducao > 0) then
    begin

        DBGridEh3.Columns[6].Visible := True ;
        DBGridEh3.Columns[7].Visible := True ;

    end ;

    Self.ShowModal;
    
    cxTabSheet1.Caption := 'Estrutura para Compra' ;

end;

procedure TfrmProdutoERPInsumos.qryFormulaProdutonCdLocalEstoqueChange(
  Sender: TField);
begin
  inherited;

  if (qryFormulaProduto.State in [dsInsert,dsEdit]) then
  begin

      qryFormulaProdutocNmLocalEstoque.Value := '' ;

      if (qryFormulaProdutonCdLocalEstoque.Value > 0) then
      begin

          qryLocalEstoque.Close ;
          PosicionaQuery(qryLocalEstoque, qryFormulaProdutonCdLocalEstoque.AsString) ;

          if qryLocalEstoque.Active then
              qryFormulaProdutocNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;

      end ;

  end ;

end;

end.
