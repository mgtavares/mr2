unit fCaixa_EstornoDia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, Menus, DBGridEhGrouping, ToolCtrlsEh, Cappta_Gp_Api_Com_TLB, fCapptAPI,
  ACBrBase, ACBrPosPrinter, StrUtils;

type
  TfrmCaixa_EstornoDia = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryLanctoFin: TADOQuery;
    dsLanctoFin: TDataSource;
    SP_ESTORNA_LANCTO_CAIXA: TADOStoredProc;
    qryLanctoFinnCdLanctoFin: TAutoIncField;
    qryLanctoFindDtLancto: TDateTimeField;
    qryLanctoFinnValLancto: TBCDField;
    qryLanctoFincFlgManual: TIntegerField;
    qryLanctoFincNmTipoLancto: TStringField;
    qryLanctoFincNmFormaPagto: TStringField;
    qryLanctoVenda: TADOQuery;
    qryBuscaDoctoFiscal: TADOQuery;
    qryItensPedido: TADOQuery;
    ReimprimirCupomFiscal1: TMenuItem;
    qryPreparaTemp: TADOQuery;
    qryTempPagto: TADOQuery;
    qryItensPedidonCdPedido: TIntegerField;
    qryItensPedidonValPedido: TBCDField;
    qryItensPedidonCdTerceiro: TIntegerField;
    qryItensPedidonValProdutos: TBCDField;
    qryTempVale: TADOQuery;
    qryTempValenValValeMerc: TBCDField;
    qryTempValenValDevoluc: TBCDField;
    qryAux: TADOQuery;
    PopupMenu1: TPopupMenu;
    qryTempPagtocNmFormaPagto: TStringField;
    qryTempPagtonValPagto: TBCDField;
    qryTempPagtocFlgTEF: TIntegerField;
    qryTempPagtonValPedido: TBCDField;
    qryTempPagtonCdPedido: TIntegerField;
    qryTempPagtonValProdutos: TBCDField;
    qryTempPagtonCdTerceiro: TIntegerField;
    qryTempPagtonValValeMerc: TBCDField;
    qryTempPagtonValDevoluc: TBCDField;
    qryTestaValor: TADOQuery;
    qryTestaValePresente: TADOQuery;
    qryValidaEstCredReneg: TADOQuery;
    qryFormaPagtoLanctoFin: TADOQuery;
    qryFormaPagtoLanctoFincNmFormaPagto: TStringField;
    qryConsultaDoctoFiscalCFe: TADOQuery;
    qryConsultaDoctoFiscalCFenCdDoctoFiscal: TIntegerField;
    qryTempCFe: TADOQuery;
    qryTempCFenCdPedido: TIntegerField;
    qryTempCFenValPedido: TBCDField;
    qryTempCFenCdTerceiro: TIntegerField;
    qryPreparaTempCFe: TADOQuery;
    qryBuscaDoctoFiscalnCdDoctoFiscal: TIntegerField;
    qryTEF: TADOQuery;
    qryTEFcNumeroControle: TStringField;
    qryImpressora: TADOQuery;
    qryImpressoracModeloImpSAT: TStringField;
    qryImpressoracPortaImpSAT: TStringField;
    qryImpressoracPageCodImpSAT: TStringField;
    qryImpressoracFlgTipoImpSAT: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ReimprimirCupomFiscal1Click(Sender: TObject);
    procedure AbreCupomFiscal();
    function EstadoECF() : string ;
    procedure ImprimeItensCupomFiscal();
    procedure FinalizaCupomFiscal();
    procedure CancelaRecebimento;
    procedure CancelaCupomFiscal();
    procedure EnviaPagamentosECF;
    function calculaTotalPago():double;
    procedure EnviaPagamentoECF(cFormaPagto: string; nValorPagto: double; cOBS: string; bVinculado: boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
    nCdLanctoFinOld       : integer ;
  public
    { Public declarations }
    nCdResumoCaixa        : integer ;
    nCdUsuarioAutorizador : integer ;
    nCdTerceiro           : integer ;
    cFlgLiqCrediario      : integer ;
    cCGCPF                : string  ;
    nCdTerceiroPDV        : integer ;
    nValPagto             : Currency;
    iQtdeClientes         : integer ;
    nCdTerceiroPadrao     : integer ;
    cFlgUsaGaveta         : boolean ;
    cFlgUsaCFe            : boolean ;
  end;

var
  frmCaixa_EstornoDia: TfrmCaixa_EstornoDia;

implementation

uses fMenu, fPdvSupervisor, fCaixa_MotivoEstorno, fCaixa_Recebimento,
  fCaixa_CPFNotaFiscal,fCaixa_LanctoMan, fFuncoesSAT, Math, uPDV_Bematech_Termica;

{$R *.dfm}

var
    objCaixa : TfrmCaixa_Recebimento ;

procedure TfrmCaixa_EstornoDia.FormShow(Sender: TObject);
begin

  objCaixa := TfrmCaixa_Recebimento.Create( Self ) ;

  PosicionaQuery(qryLanctoFin, intToStr(nCdResumoCaixa)) ;

  if (qryLanctoFin.Eof) then
  begin
      MensagemAlerta('Nenhum lan�amento dispon�vel para estorno.') ;
      close ;
      exit ;
  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmCaixa_EstornoDia.ToolButton1Click(Sender: TObject);
var
  objMotivoEstorno   : TfrmCaixa_MotivoEstorno ;
  objCaixa_LanctoMan : TfrmCaixa_LanctoMan ;
  objSAT             : TfrmFuncoesSAT;
  objTEF             : TfrmCapptAPI;
  opAprovada         : IRespostaOperacaoAprovada;
  opNegada           : IRespostaOperacaoRecusada;
  iteracaoTef        : IIteracaoTef;
  tipoIteracao       : integer;
  cupomCliente, cupomLoja : TStringList;
  I : Integer;
  lErroTEF : Boolean;
  ppMarcaImpSAT : TACBrPosPrinterModelo;
  pcPagCodigo   : TACBrPosPaginaCodigo;

begin
  inherited;

  if (qryLanctoFin.IsEmpty) then
      Exit;

  if (qryLanctoFinnCdLanctoFin.Value = 0) then
  begin
      MensagemAlerta('Selecione um lan�amento para ser estornado.');
      Exit;
  end;

  objMotivoEstorno := TfrmCaixa_MotivoEstorno.Create( Self ) ;
  objSAT           := TfrmFuncoesSAT.Create(Self);

  objMotivoEstorno.Edit1.Text := '' ;
  objMotivoEstorno.ShowModal ;

  try
      case MessageDlg('Confirma o estorno deste lan�amento ?' + #13#13 + 'Motivo: ' + objMotivoEstorno.Edit1.Text,mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      { -- verifica se esta sendo estornado credi�rio -- }
      qryValidaEstCredReneg.Close;
      qryValidaEstCredReneg.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.Value;
      qryValidaEstCredReneg.Open;

      if not (qryValidaEstCredReneg.IsEmpty) then
      begin
          MensagemAlerta('N�o � poss�vel estornar o lan�amento ' + qryLanctoFinnCdLanctoFin.AsString + ' devido existir uma renegocia��o ativa vinculada ao credi�rio.');
          Exit;
      end;

      PosicionaQuery(qryConsultaDoctoFiscalCFe, qryLanctoFinnCdLanctoFin.AsString);

      if ((not qryConsultaDoctoFiscalCFe.IsEmpty) and (not cFlgUsaCFe)) then
      begin
          frmMenu.MensagemAlerta('M�dulo SAT n�o habilitado para este terminal.');
          Exit;
      end;

          { -- se tiver TEF, processa cancelamento dos pagamentos -- }
          lErroTEF := False;
          qryTEF.Close;
          qryTEF.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.Value;
          qryTEF.Open;

          if not (qryTEF.IsEmpty) then
          begin
           try

            frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Inicio processamento TEF' );

            objTEF := TfrmCapptAPI.Create(Self);
            cupomCliente  := TStringList.Create;
            cupomLoja := TStringList.Create;
            qryTEF.First;
            while not (qryTEF.Eof) do
            begin

             frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Antes de Chamar Cancelamento' );

             objTEF.cancelaPagamento('cappta',qryTEFcNumeroControle.Value);

             { ---- In�cio da Itera��o TEF ---- }

              Repeat

                iteracaoTef := ObjTEF.cappta.IterarOperacaoTef();
                tipoIteracao := iteracaoTef.Get_TipoIteracao;

                if (tipoIteracao = 8) then
                  objTEF.cappta.EnviarParametro('0', 1);

              Until (iteracaoTef.Get_TipoIteracao = 1) or (iteracaoTef.Get_TipoIteracao = 2);

              { ---- Fim da Itera��o TEF ---- }

              if (tipoIteracao <> 1) then
              begin
                opNegada := (iteracaoTef as IRespostaOperacaoRecusada);
                frmMenu.ShowMessage('Opera��o n�o processada. ' + opNegada.Get_Motivo);
                lErroTEF := True;
                frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Iteracao Operacao Nao Processada:'+opNegada.Get_Motivo );

                // Fica no loop para tentar cancelar todas as transacoes TEF
              end else
              begin
               opAprovada := (iteracaoTef as IRespostaOperacaoAprovada);

                frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Iteracao Operacao Processada' );

               cupomCliente.Add(opAprovada.Get_CupomCliente);

               cupomLoja.Add(opAprovada.Get_CupomLojista);

               //Imprime cupons TEF
                frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Comprovante Obtido' );

               qryImpressora.Close;
               qryImpressora.Parameters.ParamByName('cNmComputador').Value := frmMenu.qryConfigAmbientecNmComputador.Value;
               qryImpressora.Open;

               if qryImpressora.IsEmpty then
               begin
                  frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click N�o Encontrada configura��o impressora para '+frmMenu.qryConfigAmbientecNmComputador.Value );
                  MensagemErro('N�o Encontrada configura��o impressora para '+frmMenu.qryConfigAmbientecNmComputador.Value);
                  lErroTef := True;
               end else begin
//        if qryImpressoracFlgTipoImpSAT.Value = 1 then  // Tipo ESC POS
//        begin

                   case AnsiIndexStr(qryImpressoracModeloImpSAT.Value,['Epson','Bematech','Daruma','Elgin','Diebold']) of
                   0 : ppMarcaImpSAT := ppEscPosEpson;
                   1 : ppMarcaImpSAT := ppEscBematech;
                   2 : ppMarcaImpSAT := ppEscDaruma;
                   3 : ppMarcaImpSAT := ppEscVox;    //ppEscElgin;   Alterado 02/08/18 nova versoa ACBR por Marcelo
                   4 : ppMarcaImpSAT := ppEscDiebold;
                   end;

                   case AnsiIndexStr(qryImpressoracPageCodImpSat.Value,['pcNone','pc437','pc850','pc852','pc860','pcUTF8','pc1252']) of
                   0 : pcPagCodigo := pcNone;
                   1 : pcPagCodigo := pc437;
                   2 : pcPagCodigo := pc850;
                   3 : pcPagCodigo := pc852;
                   4 : pcPagCodigo := pc860;
                   5 : pcPagCodigo := pcUTF8;
                   6 : pcPagCodigo := pc1252;
                   end;

                    frmMenu.ACBrPosPrinter1.Desativar;

                    frmMenu.ACBrPosPrinter1.Modelo             := ppMarcaImpSAT;
                    frmMenu.ACBrPosPrinter1.Porta              := qryImpressoracPortaImpSAT.Value;
                    frmMenu.ACBrPosPrinter1.CortaPapel         := True;
                    frmMenu.ACBrPosPrinter1.PaginaDeCodigo     := pcPagCodigo;


                    frmMenu.ACBRPosPrinter1AguardarPronta(True);


                    try
                     frmMenu.ACBrPosPrinter1.Ativar;
                     frmMenu.ACBrPosPrinter1.Imprimir(cupomCliente.Text);
                     frmMenu.ACBrPosPrinter1.CortarPapel( True );
                     frmMenu.ACBrPosPrinter1.Imprimir(cupomLoja.Text);
                     frmMenu.ACBrPosPrinter1.CortarPapel( False );  // Corte Total
                     frmMenu.ACBrPosPrinter1.Desativar;

                     frmMenu.ACBRPosPrinter1FechaImpTermica;
                     frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Impressao OK');
                    except
                        frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Erro ao efetuar a impress�o do cupom TEF de Cancelamento');
                        MensagemErro('Erro ao efetuar a impress�o do cupom TEF de Cancelamento');
                        lErroTEF := True;
                    end;
               end;
              end;

              qryTEF.Next;
            end;
           finally
            cupomCliente.Free;
            cupomLoja.Free;
            objTEF.Free;
            frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Fim Processamento TEF' );
           end;
          end;
          if lErroTEF then
          begin
             case MessageDlg('Erro no Processamento TEF. Continua Estorno Sem estornar o TEF ?',mtConfirmation,[mbYes,mbNo],0) of
                    mrYes: begin
                          frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Erro no processamento TEF, Operador Optou Continuar Estorno');
                          end;
                    mrNo: begin
                          frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Erro no processamento TEF, Operador Optou Nao Estornar Recebimento');
                          exit ;
                          end;
            end ;
          end;


      frmMenu.Connection.BeginTrans;

      try

          frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Executando SP_ESTORNA_LANCTO_CAIXA');

          SP_ESTORNA_LANCTO_CAIXA.Close ;
          SP_ESTORNA_LANCTO_CAIXA.Parameters.ParamByName('@nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.Value ;
          SP_ESTORNA_LANCTO_CAIXA.Parameters.ParamByName('@nCdUsuario').Value   := nCdUsuarioAutorizador ;
          SP_ESTORNA_LANCTO_CAIXA.Parameters.ParamByName('@cMotivo').Value      := objMotivoEstorno.Edit1.Text ;
          SP_ESTORNA_LANCTO_CAIXA.Parameters.ParamByName('@nCdLoja').Value      := frmMenu.nCdLojaAtiva;
          SP_ESTORNA_LANCTO_CAIXA.ExecProc ;

          { -- se for cf-e, envia informa��o de cancelamento para o SAT -- }
          qryConsultaDoctoFiscalCFe.First;

          while (not qryConsultaDoctoFiscalCFe.Eof) do
          begin
              frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click geraCancSAT:'+qryConsultaDoctoFiscalCFenCdDoctoFiscal.AsString);
              objSAT.prGeraCancSAT(qryConsultaDoctoFiscalCFenCdDoctoFiscal.Value);
              qryConsultaDoctoFiscalCFe.Next;
          end;


      except
          frmMenu.Connection.RollbackTrans;
          frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Erro em SP_ESTORNA_LANCTO_CAIXA');
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;





      { -- verifica se utiliza gaveta eletr�nica -- }
      if (cFlgUsaGaveta) then
          frmMenu.prAbreGaveta;

      ShowMessage('Lan�amento estornado com sucesso.') ;

      nCdLanctoFinOld := qryLanctoFinnCdLanctoFin.Value ;

      qryLanctoFin.Close ;
      PosicionaQuery(qryLanctoFin, intToStr(nCdResumoCaixa)) ;

      {-- emite o comprovante --}

      objCaixa_LanctoMan := TfrmCaixa_LanctoMan.Create( Self ) ;

      try

          frmMenu.CapptaLog('fCaixa_EstornoDia/ToolButton1Click Emite Comprovante do Estorno');
          objCaixa_LanctoMan.imprimirComprovanteEstorno(nCdLanctoFinOld);

      finally

          freeAndNil( objCaixa_LanctoMan ) ;

      end ;

      DBGridEh1.SetFocus ;

  finally

      freeAndNil( objMotivoEstorno ) ;
      FreeAndNil(objSAT);

  end ;

end;

procedure TfrmCaixa_EstornoDia.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  ToolButton1.Click ;
  
end;

{-- AQUI COME�AM AS ALTERA��ES PARA REIMPRESS�O DO CUPOM FISCAL --}

procedure TfrmCaixa_EstornoDia.PopupMenu1Popup(Sender: TObject);
begin
  inherited;
  qryLanctoVenda.Close;
  qryLanctoVenda.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.Value;
  qryLanctoVenda.Open;

  qryTestaValor.Close;
  qryTestaValor.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.Value;
  qryTestaValor.Open;

  qryTestaValePresente.Close;
  qryTestaValePresente.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.Value;
  qryTestaValePresente.Open;

  if ((qryLanctoVenda.Eof) or (qryTestaValor.Eof) or (qryTestaValePresente.Eof)) then
  begin
      ReimprimirCupomFiscal1.Enabled := false;
      exit;
  end
  else
      ReimprimirCupomFiscal1.Enabled := true;
end;

procedure TfrmCaixa_EstornoDia.ReimprimirCupomFiscal1Click(Sender: TObject);
var
  nCdItemPedido          : Integer;
  objCaixa_CPFNotaFiscal : TfrmCaixa_CPFNotaFiscal;
label
  solicitaCPFCNPJ;
begin
  inherited;

  if ((not cFlgUsaECFAux) and (not cFlgUsaCFeAux)) then
  begin
      MensagemAlerta('Nenhum equipamento emissor de cupom fiscal ativo.');
      exit;
  end;

  qryBuscaDoctoFiscal.Close;
  qryBuscaDoctoFiscal.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.Value;
  qryBuscaDoctoFiscal.Open;

  if (not qryBuscaDoctoFiscal.IsEmpty) then
  begin
      MensagemAlerta('O cupom fiscal j� foi registrado para este lan�amento e encontra-se vinculado ao documento No ' + qryBuscaDoctoFiscalnCdDoctoFiscal.AsString + '.');
      Exit;
  end;

  { -- cupom fiscal ECF -- }
  if (cFlgUsaECFAux) then
  begin
      case MessageDlg('Deseja emitir o cupom fiscal para o lan�amento n� ' + qryLanctoFinnCdLanctoFin.AsString + ' ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      // Abre o cupom fiscal
      if (EstadoECF() = 'Livre') then
      begin
          Sleep(300);

          cFlgUsaECF := cFlgUsaECFAux;

          AbreCupomFiscal() ;

          {qrySomaDescAcres.Close ;
          qrySomaDescAcres.Open ;}

          {(qrySomaDescAcresnValAcrescimo.Value - qrySomaDescAcresnValDesconto.Value)}

          qryTempPagto.Close;
          qryTempPagto.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.Value;
          qryTempPagto.Open;

          nValPagto := 0;

          while not qryTempPagto.Eof do
          begin
              nValPagto := nValPagto + qryTempPagtonValPagto.Value;
              qryTempPagto.Next;
          end;

          qryTempPagto.First;

          Sleep(300);
          frmMenu.ACBrECF1.SubtotalizaCupom((calculaTotalPago() - qryTempPagtonValProdutos.Value), frmMenu.cMsgCupomFiscal );
          Sleep(300);

          try
              EnviaPagamentosECF ;
          except
              MensagemErro('Erro enviando as formas de pagamento para ECF.') ;
          raise ;
          end ;

          try
              FinalizaCupomFiscal() ;
          except
              MensagemErro('Erro finalizando cupom fiscal.') ;
          raise ;
          end ;

      end ;
  end;

  { -- cupom fiscal eletr�nico CF-e -- }
  if (cFlgUsaCFeAux) then
  begin
      if (qryLanctoFinnValLancto.Value <= 0) then
      begin
          MensagemAlerta('Lan�amento n�o possui valor para emiss�o do cupom fiscal eletr�nico.');
          Exit;
      end;

      case MessageDlg('Deseja emitir o cupom fiscal eletr�nico para o lan�amento n� ' + qryLanctoFinnCdLanctoFin.AsString + ' ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo : Exit;
      end;

      try
          objCaixa.lblMensagem.Caption := 'Gerando cupom fiscal...';
          objCaixa.cModoEnvioCFe       := 'S';
          objCaixa_CPFNotaFiscal       := TfrmCaixa_CPFNotaFiscal.Create(Self);

          cCGCPF         := '';
          nCdTerceiro    := 0;
          nCdDoctoFiscal := 0;

          qryPreparaTempCFe.ExecSQL;

          qryTempCFe.Close;
          qryTempCFe.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.Value;
          qryTempCFe.Open;

          if (qryTempCFe.IsEmpty) then
          begin
              MensagemErro('Erro ao localizar o(s) pedido(s) vinculado(s) ao lan�amento selecionado.');
              Exit;
          end;

          if (nCdTerceiro = 0) then
              nCdTerceiro := qryTempCFenCdTerceiro.Value;

          solicitaCPFCNPJ:

          objCaixa_CPFNotaFiscal.ShowModal;

          cCGCPF := objCaixa_CPFNotaFiscal.edtCPF.Text;

          if (cCGCPF <> '') then
          begin
              if (frmMenu.TestaCpfCgc(cCGCPF) = '') then
              begin
                  goto solicitaCPFCNPJ;
              end;

              case MessageDlg('Confirma a utiliza��o do CPF/CNPJ n� ' + cCGCPF + ' ?', mtConfirmation,[mbYes,mbNo],0) of
                  mrNo : begin
                      goto solicitaCPFCNPJ;
                  end;
              end;
          end
          else
          begin
              case MessageDlg('Nenhum CPF/CNPJ informado, deseja continuar ?', mtConfirmation,[mbYes,mbNo],0) of
                  mrNo : begin
                      goto solicitaCPFCNPJ;
                  end;
              end;
          end;

          try
              frmMenu.Connection.BeginTrans;

              { -- gera cupom fiscal eletr�nico -- }
              objCaixa.SP_GERA_CUPOM_FISCAL.Close ;
              objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
              objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdLoja').Value     := frmMenu.nCdLojaAtiva;
              objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
              objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdTerceiro').Value := nCdTerceiro;
              objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@cCPF').Value        := cCGCPF;
              objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@cCdNumSerie').Value := '';
              objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@cFlgCFe').Value     := 1; //cupom fiscal eletr�nico CFe
              objCaixa.SP_GERA_CUPOM_FISCAL.Open;

              nCdDoctoFiscal := objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdDoctoFiscal').Value;

              { -- atualiza lan�amento fin. cf-e -- }
              objCaixa.qryAtualizaLanctoFinCFe.Close;
              objCaixa.qryAtualizaLanctoFinCFe.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
              objCaixa.qryAtualizaLanctoFinCFe.Parameters.ParamByName('nCdLanctoFin').Value   := qryLanctoFinnCdLanctoFin.Value;
              objCaixa.qryAtualizaLanctoFinCFe.ExecSQL;

              { -- transmite cupom fiscal eletr�nico -- }
              objCaixa.prGeraCFe;

              frmMenu.Connection.CommitTrans;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro na gera��o do cupom fiscal eletr�nico.');
              Raise;
          end;
      finally
          objCaixa.lblMensagem.Caption := '';

          qryBuscaDoctoFiscal.Close;
          qryPreparaTempCFe.Close;
          objCaixa.cmdZeraTemp.Execute;

          frmMenu.ZeraTemp('#Temp_Pedido');
          frmMenu.ZeraTemp('#Temp_PagtoDocto');


          FreeAndNil(objCaixa_CPFNotaFiscal);
      end;

      cFlgUsaCFe := False;
  end;
end;

procedure TfrmCaixa_EstornoDia.AbreCupomFiscal;
label
    SolicitaCPFCNPJ ;
var
    cEstadoECF             : string ;
    objCaixa_CPFNotaFiscal : TfrmCaixa_CPFNotaFiscal ;
    cFormaPagtoECF         : String ;
begin

    qryPreparaTemp.ExecSQL;

    qryItensPedido.Close;
    qryItensPedido.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.Value;
    qryItensPedido.Open;

    cEstadoECF := EstadoECF() ;

    If (cEstadoECF = '') then
    begin
        if ( Application.MessageBox( 'A impressora n�o responde!' + #13 +
                                     'Deseja tentar novamente?', 'Aten��o',
                                     MB_IconInformation + MB_YESNO ) = IDYES ) then
        begin
            AbreCupomFiscal() ;
        end
        else
            abort ;
    end ;

    if (cEstadoECF = 'Venda') or (cEstadoECF = 'Pagamento') then //ja tem um cupom fiscal aberto
    begin
        MensagemAlerta('N�o � poss�vel abrir um novo cupom fiscal enquanto outro estiver aberto.') ;
        CancelaRecebimento;
        abort ;
    end ;

    if (cEstadoECF <> 'Livre') then
    begin
        MensagemAlerta('O Estado da impressora ECF n�o permite a abertura de um novo cupom fiscal.' +#13#13+ 'Estado: ' + cEstadoECF) ;
        CancelaRecebimento;
        abort ;
    end ;

    if (bCupomAberto) then
        exit ;

    { -- verifica se forma de pagto esta configurada na ECF, caso n�o exista programa forma de pagto antes da abertura do cupom -- }
    try
        qryFormaPagtoLanctoFin.Close;
        qryFormaPagtoLanctoFin.Parameters.ParamByName('nPK').Value := qryLanctoFinnCdLanctoFin.Value;
        qryFormaPagtoLanctoFin.Open;

        qryFormaPagtoLanctoFin.First;

        while (not qryFormaPagtoLanctoFin.Eof) do
        begin
            if (frmMenu.ACBrECF1.AchaFPGDescricao(qryFormaPagtoLanctoFincNmFormaPagto.Value) = Nil) then
            begin
                Sleep(300);

                cFormaPagtoECF := qryFormaPagtoLanctoFincNmFormaPagto.Value;

                frmMenu.ACBrECF1.ProgramaFormaPagamento(cFormaPagtoECF,TRUE,'');
            end;

            qryFormaPagtoLanctoFin.Next;
        end;

        qryFormaPagtoLanctoFin.First;
    except
        MensagemErro('Erro no processamento.');
        Raise;
        Abort;
    end;

    if (frmMenu.ACBrECF1.PoucoPapel) then
        MensagemAlerta('Impressora ECF com pouco papel!') ;

    iConta     := 0 ;
    iTransacao := 0 ;

    if (nCdTerceiro = 0) then
        nCdTerceiro := qryItensPedidonCdTerceiro.Value ;


    objCaixa_CPFNotaFiscal := TfrmCaixa_CPFNotaFiscal.Create( Self ) ;

    SolicitaCPFCNPJ:

    objCaixa_CPFNotaFiscal.ShowModal;

    cCGCPF := objCaixa_CPFNotaFiscal.edtCPF.Text;

    if (cCGCPF <> '') then
    begin
        if (frmMenu.TestaCpfCgc(cCGCPF) = '') then
        begin
            Goto SolicitaCPFCNPJ ;
        end ;
    end ;

    freeAndNil( objCaixa_CPFNotaFiscal ) ;

    // grava o cupom fiscal na base de dados
    frmMenu.Connection.BeginTrans ;

    try
        objCaixa.SP_GERA_CUPOM_FISCAL.Close ;
        objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
        objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdLoja').Value     := frmMenu.nCdLojaAtiva;
        objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
        objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdTerceiro').Value := nCdTerceiro;
        objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@cCPF').Value        := cCGCPF;
        objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@cFlgCFe').Value     := 0; //cupom fiscal ECF
        objCaixa.SP_GERA_CUPOM_FISCAL.Open;

        nCdDoctoFiscal := objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nCdDoctoFiscal').Value ;

    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento') ;
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    if (trim(cCGCPF) <> '') then
        frmMenu.ACBrECF1.AbreCupom(Trim(cCGCPF), 'CONSUMIDOR' , '' )
    else frmMenu.ACBrECF1.AbreCupom('', '' , '' );

    nCdCupomFiscal := StrToInt(frmMenu.ACBrECF1.NumCupom) ;

    // Imprime os produtos do cupom
    ImprimeItensCupomFiscal() ;

    // grava o n�mero do cupom na base de dados
    frmMenu.Connection.BeginTrans ;

    try

        objCaixa.SP_GRAVA_NUMERO_CUPOM_FISCAL.Close ;
        objCaixa.SP_GRAVA_NUMERO_CUPOM_FISCAL.Parameters.ParamByName('@nCdDoctoFiscal').Value  := nCdDoctoFiscal ;
        objCaixa.SP_GRAVA_NUMERO_CUPOM_FISCAL.Parameters.ParamByName('@iNrDocto').Value        := nCdCupomFiscal ;
        objCaixa.SP_GRAVA_NUMERO_CUPOM_FISCAL.ExecProc ;

    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento') ;
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    bCupomAberto := True ;
    
end;

function TfrmCaixa_EstornoDia.EstadoECF: string;
begin

    Application.ProcessMessages;

    if (not cFlgUsaECF) then
    begin
        Result := 'Livre' ;
        exit ;
    end ;

    try
        Result :=  Estados[ frmMenu.ACBrECF1.Estado ] ;
    except
        Result := '' ;
    end ;

    if (Result = 'N�o Inicializada') then
        Exception.Create('Impressora Fiscal Nao Inicializada.') ;

    if (Result = 'Desconhecido') then
        Exception.Create('Impressora Fiscal Nao Inicializada. Status Desconhecido.') ;

    if (Result = 'Bloqueada') then
        Exception.Create('Impressora Fiscal est� Bloqueada.') ;

    if (Result = 'Requer Z') then
        Exception.Create('Impressora Fiscal Requer Redu��o Z.') ;

    if (Result = 'Requer X') then
        Exception.Create('Impressora Fiscal Requer Leitura X.') ;

    if (Result = 'Nao Fiscal') then
        Exception.Create('Impressora encontrada n�o � uma impressora fiscal.') ;
{
    estNaoInicializada  Porta Serial ainda nao foi aberta
    estDesconhecido     Porta aberta, mas estado ainda nao definido
    estLivre            Impressora Livre, sem nenhum cupom aberto,
                        pronta para nova venda, Reducao Z e Leitura X ok,
                        pode ou nao j� ter ocorrido 1� venda no dia...
    estVenda            Cupom de Venda Aberto com ou sem venda do 1� Item
    estPagamento        Iniciado Fechamento de Cupom com Formas Pagto
                      pode ou n�o ter efetuado o 1� pagto. Nao pode
                      mais vender itens, ou alterar Subtotal
    estRelatorio        Imprimindo Cupom Fiscal Vinculado ou Relatorio Gerencial
    estBloqueada        Redu�ao Z j� emitida, bloqueada at� as 00:00
    estRequerZ          Reducao Z dia anterior nao emitida. Emita agora
    estRequerX          Esta impressora requer Leitura X todo inicio de
                          dia. Imprima uma Leitura X para poder vender
    estNaoFiscal        Comprovante Nao Fiscal Aberto
}
end;

procedure TfrmCaixa_EstornoDia.ImprimeItensCupomFiscal;
var
  nValImpAproxFed : Double;
  nValImpAproxEst : Double;
  nValImpAproxMun : Double;
  cChaveIBPT      : String;
begin

    if (EstadoECF() <> 'Venda') then
    begin
        MensagemAlerta('Status do cupom fiscal n�o permite incluir itens.') ;
        abort ;
    end ;

    objCaixa.SP_GERA_CUPOM_FISCAL.First ;

    while not objCaixa.SP_GERA_CUPOM_FISCAL.Eof do
    begin

        frmMenu.ACBrECF1.VendeItem( objCaixa.SP_GERA_CUPOM_FISCALnCdProduto.AsString
                                   ,objCaixa.SP_GERA_CUPOM_FISCALcNmItem.Value
                                   ,objCaixa.SP_GERA_CUPOM_FISCALcAliqICMS.Value
                                   ,objCaixa.SP_GERA_CUPOM_FISCALnQtde.Value
                                   ,objCaixa.SP_GERA_CUPOM_FISCALnValUnitario.Value
                                   ,0
                                   ,objCaixa.SP_GERA_CUPOM_FISCALcUnidadeMedida.Value
                                   ,'%' ) ;

        objCaixa.SP_GERA_CUPOM_FISCAL.Next;

    end ;

    { -- atribui valor do imposto aproximado calculado pela procedure para ser impresso no rodap� do cupom fiscal -- }
    nValImpAproxFed := objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nValTotalImpostoAproxFed').Value;
    nValImpAproxEst := objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nValTotalImpostoAproxEst').Value;
    nValImpAproxMun := objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nValTotalImpostoAproxMun').Value;

    if ((nValImpAproxFed > 0) or (nValImpAproxEst > 0) or (nValImpAproxMun > 0)) then
    begin
        objCaixa.qryTabIBPT.Close;
        objCaixa.qryTabIBPT.Open;

        if (not objCaixa.qryTabIBPT.IsEmpty) then
            cChaveIBPT := objCaixa.qryTabIBPTcChave.Value;

        frmMenu.ACBrECF1.InfoRodapeCupom.Imposto.ValorAproximadoFederal   := objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nValTotalImpostoAproxFed').Value;
        frmMenu.ACBrECF1.InfoRodapeCupom.Imposto.ValorAproximadoEstadual  := objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nValTotalImpostoAproxEst').Value;
        frmMenu.ACBrECF1.InfoRodapeCupom.Imposto.ValorAproximadoMunicipal := objCaixa.SP_GERA_CUPOM_FISCAL.Parameters.ParamByName('@nValTotalImpostoAproxMun').Value;

        { -- informa fonte de al�quota para c�lculo do imposto aproximado -- }
        frmMenu.ACBrECF1.InfoRodapeCupom.Imposto.Fonte        := 'IBPT/FECOMERCIO (' + cChaveIBPT + ')';
        frmMenu.ACBrECF1.InfoRodapeCupom.Imposto.ModoCompacto := False;
    end;

    objCaixa.SP_GERA_CUPOM_FISCAL.Close ;

end;

procedure TfrmCaixa_EstornoDia.FinalizaCupomFiscal;
begin

    Sleep(300) ;

    frmMenu.ACBrECF1.FechaCupom('');

    try
        frmMenu.ACBrECF1.CortaPapel(TRUE);
    except
    end ;

    cFlgUsaECF := false;

    nCdTerceiro := 0;
    nValPagto   := 0;

    qryLanctoVenda.Close ;
    qryBuscaDoctoFiscal.Close ;
    qryTempVale.Close;
    qryTempPagto.Close;
    qryItensPedido.Close ;
    qryPreparaTemp.Close;

    bCupomAberto := False ;
    objCaixa.cmdZeraTemp.Execute;

    frmMenu.ZeraTemp('#Temp_Pedido');
    frmMenu.ZeraTemp('#Temp_Pagtos');
    frmMenu.ZeraTemp('#Temp_ChequeResgate');
    frmMenu.ZeraTemp('#Temp_ValePresente') ;
    frmMenu.ZeraTemp('#Temp_RestricaoVenda');

end;

procedure TfrmCaixa_EstornoDia.CancelaRecebimento;
begin

    try
        objCaixa.cmdZeraTemp.Execute;
    except
    end ;

    frmMenu.ZeraTemp('#Temp_Pedido');

    qryLanctoVenda.Close ;
    qryBuscaDoctoFiscal.Close ;
    qryTempVale.Close;
    qryTempPagto.Close;
    qryItensPedido.Close ;
    qryPreparaTemp.Close;

    cFlgLiqCrediario  := 0 ;

    iQtdeClientes     := 0 ;
    nCdTerceiroPadrao := 0 ;

    bCupomAberto      := False ;

end;

procedure TfrmCaixa_EstornoDia.CancelaCupomFiscal;
var
    cEstadoECF : string ;
begin

    if (not cFlgUsaECFAux) then
        exit ;

    cEstadoECF := EstadoECF() ;

    if ((EstadoECF = 'Venda') or (EstadoECF = 'Pagamento')) then
        frmMenu.ACBrECF1.CancelaCupom() ;

end;

procedure TfrmCaixa_EstornoDia.EnviaPagamentosECF;
var
    nValDevoluc, nValVale : double ;
begin

    qryTempVale.Close;
    qryTempVale.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.Value;
    qryTempVale.Open;

    {-- Se tiver troca, envia primeiro --}
    nValDevoluc := 0;
    nValVale    := 0;

    if qryTempVale.Active then
    begin
        qryTempVale.First ;

        while not qryTempVale.eof do
        begin
            nValDevoluc := nValDevoluc + qryTempValenValDevoluc.Value  ;
            nValVale    := nValVale    + qryTempValenValValeMerc.Value ;
            qryTempVale.Next;
        end ;

        qryTempVale.First;

        Sleep(300) ;

        if nValDevoluc > 0 then
            EnviaPagamentoECF('CREDITO TROCA', nValDevoluc , '', false) ;

        Sleep(300) ;

        if nValVale > 0 then
            EnviaPagamentoECF('VALE', nValVale , '', false) ;
    end ;


    {--verifica os pagamentos--}
    qryTempPagto.Close ;
    qryTempPagto.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.Value;
    qryTempPagto.Open ;

    qryTempPagto.First ;

    //Verifica se houve alguma forma de pagamento

    while not qryTempPagto.Eof  do
    begin
        if(Trim(qryTempPagtocNmFormaPagto.Value) <> '') then
        begin
            // imprime o valor pago no cupom fiscal, juntamente com a forma de pagamento
            // se for uma transa��o TEF, confirma a transa��o
            Sleep(300) ;
            EnviaPagamentoECF(qryTempPagtocNmFormaPagto.Value, qryTempPagtonValPagto.Value , '', (qryTempPagtocFlgTEF.Value = 1)) ;

        end;
        qryTempPagto.Next ;

    end ;

    qryTempPagto.First ;

end;

procedure TfrmCaixa_EstornoDia.EnviaPagamentoECF(cFormaPagto: string; nValorPagto: double; cOBS: string; bVinculado: boolean);
begin

    if (not cFlgUsaECF) then
        exit ;

    if (EstadoECF() <> 'Pagamento') then
    begin
        Exception.Create('O Estado da Impressora ECF n�o � de Pagamento. Estado : ' + EstadoECF());
    end ;

    { -- bloco removido pois programa��o da forma de pagto pode ser efetuada deve ser efetuado antes da abertura do cupom -- }
    { -- processo incluso no momento da abertura do cupom -- }
    {if (frmMenu.ACBrECF1.AchaFPGDescricao(cFormaPagto) = nil) then
        frmMenu.ACBrECF1.ProgramaFormaPagamento(cFormaPagto,TRUE,'') ;}

    frmMenu.ACBrECF1.EfetuaPagamento(frmMenu.ACBrECF1.AchaFPGDescricao(cFormaPagto).Indice, nValorPagto, cOBS, bVinculado) ;

end;

function TfrmCaixa_EstornoDia.calculaTotalPago: double;
begin

    try
        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('IF OBJECT_ID(' + Chr(39) + 'tempdb..#Temp_Pagtos_Estorno' + Chr(39) + ') IS NOT NULL SELECT IsNull(Sum(nValPagto),0) FROM #Temp_Pagtos_Estorno ELSE SELECT 0') ;
        qryAux.Open ;

        Result := qryAux.FieldList[0].Value ;
    except
        Result := 0 ;
    end ;

end;

procedure TfrmCaixa_EstornoDia.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

  freeAndNil( objCaixa ) ;

  inherited;

end;

initialization
    RegisterClass(TfrmCaixa_EstornoDia) ;

end.
