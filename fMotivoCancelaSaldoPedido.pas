unit fMotivoCancelaSaldoPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, DBCtrls;

type
  TfrmMotivoCancelaSaldoPedido = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    qryMotivo: TADOQuery;
    qryMotivonCdMotivoCancSaldoPed: TIntegerField;
    qryMotivocNmMotivoCancSaldoPed: TStringField;
    qryMotivonCdTabTipoPedido: TIntegerField;
    dsMotivo: TDataSource;
    comboMotivos: TDBLookupComboBox;
    SP_CANCELA_PEDIDO: TADOStoredProc;
    SP_CANCELA_ITEM_PEDIDO_MOTIVO: TADOStoredProc;
  procedure cancelaSaldoPedido( nCdPedido, nCdItemPedido : integer ) ;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    nCdPedidoAux     : integer ;
    nCdItemPedidoAux : integer ;
  public
    { Public declarations }
  end;

var
  frmMotivoCancelaSaldoPedido: TfrmMotivoCancelaSaldoPedido;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmMotivoCancelaSaldoPedido }

procedure TfrmMotivoCancelaSaldoPedido.cancelaSaldoPedido(nCdPedido,
  nCdItemPedido: integer);
begin

    nCdPedidoAux     := nCdPedido;
    nCdItemPedidoAux := nCdItemPedido ;

    qryMotivo.Close;
    qryMotivo.Parameters.ParamByName('nCdPedido').Value := nCdPedido ;
    qryMotivo.Open;

    Self.ShowModal;

end;

procedure TfrmMotivoCancelaSaldoPedido.FormShow(Sender: TObject);
begin
  inherited;

  comboMotivos.SetFocus;
  
end;

procedure TfrmMotivoCancelaSaldoPedido.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (comboMotivos.Text = '') then
  begin
      MensagemAlerta('Selecione o motivo de cancelamento.') ;
      comboMotivos.setFocus;
      abort;
  end ;

  {-- se o itempedido est� preenchido, indica que deve cancelar somente o saldo deste item, se estiver zerado --}
  {-- indica que deve cancelar TODO o pedido --}
  if (nCdItemPedidoAux > 0) then
  begin

      case MessageDlg('Confirma o cancelamento do saldo deste item ?', mtConfirmation,[mbYes,mbNo],0) of
          mrNo:Exit;
      end;

      frmMenu.Connection.BeginTrans;

      try
          SP_CANCELA_ITEM_PEDIDO_MOTIVO.Close;
          SP_CANCELA_ITEM_PEDIDO_MOTIVO.Parameters.ParamByName('@nCdItemPedido').Value         := nCdItemPedidoAux;
          SP_CANCELA_ITEM_PEDIDO_MOTIVO.Parameters.ParamByName('@nCdMotivoCancSaldoPed').Value := qryMotivonCdMotivoCancSaldoPed.Value ;
          SP_CANCELA_ITEM_PEDIDO_MOTIVO.Parameters.ParamByName('@nCdUsuario').Value            := frmMenu.nCdUsuarioLogado;
          SP_CANCELA_ITEM_PEDIDO_MOTIVO.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

  end ;

  if (nCdItemPedidoAux = 0) then
  begin

      case MessageDlg('Confirma o cancelamento deste pedido ?', mtConfirmation,[mbYes,mbNo],0) of
          mrNo:Exit;
      end;

      frmMenu.Connection.BeginTrans;

      try
          SP_CANCELA_PEDIDO.Close;
          SP_CANCELA_PEDIDO.Parameters.ParamByName('@nCdPedido').Value             := nCdPedidoAux;
          SP_CANCELA_PEDIDO.Parameters.ParamByName('@nCdMotivoCancSaldoPed').Value := qryMotivonCdMotivoCancSaldoPed.Value ;
          SP_CANCELA_PEDIDO.Parameters.ParamByName('@nCdUsuario').Value            := frmMenu.nCdUsuarioLogado;
          SP_CANCELA_PEDIDO.ExecProc;

          frmMenu.LogAuditoria(30,3,nCdPedidoAux,'Pedido Cancelado') ;

      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

  end ;

  Close;

end;

end.
