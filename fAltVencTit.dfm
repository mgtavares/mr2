inherited frmAltVencTit: TfrmAltVencTit
  Caption = 'Altera'#231#227'o Vencimento de T'#237'tulo'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 38
    Top = 38
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo T'#237'tulo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 65
    Top = 62
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 22
    Top = 110
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Esp'#233'cie de T'#237'tulo'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 35
    Top = 134
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero T'#237'tulo'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 304
    Top = 134
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Parcela'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 68
    Top = 158
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit6
  end
  object Label9: TLabel [7]
    Left = 269
    Top = 206
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Senso'
    FocusControl = DBEdit9
  end
  object Label12: TLabel [8]
    Left = 500
    Top = 182
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Vencimento'
    FocusControl = DBEdit12
  end
  object Label16: TLabel [9]
    Left = 49
    Top = 206
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor T'#237'tulo'
    FocusControl = DBEdit16
  end
  object Label21: TLabel [10]
    Left = 184
    Top = 38
    Width = 56
    Height = 13
    Caption = 'N'#250'mero SP'
    FocusControl = DBEdit21
  end
  object Label7: TLabel [11]
    Left = 48
    Top = 232
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observa'#231#227'o'
    FocusControl = DBMemo1
  end
  object Label8: TLabel [12]
    Left = 40
    Top = 182
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Emiss'#227'o'
    FocusControl = DBEdit7
  end
  object Label10: TLabel [13]
    Left = 240
    Top = 182
    Width = 61
    Height = 13
    Caption = 'Data Receb.'
    FocusControl = DBEdit8
  end
  object Label11: TLabel [14]
    Left = 87
    Top = 88
    Width = 21
    Height = 13
    Caption = 'Loja'
    FocusControl = DBEdit10
  end
  object Label13: TLabel [15]
    Left = 530
    Top = 134
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero NF'
    FocusControl = DBEdit11
  end
  inherited ToolBar2: TToolBar
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited ToolButton6: TToolButton
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [17]
    Tag = 1
    Left = 112
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTitulo'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [18]
    Tag = 1
    Left = 112
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [19]
    Tag = 1
    Left = 112
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdEspTit'
    DataSource = dsMaster
    TabOrder = 3
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [20]
    Left = 112
    Top = 128
    Width = 185
    Height = 19
    DataField = 'cNrTit'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [21]
    Tag = 1
    Left = 344
    Top = 128
    Width = 65
    Height = 19
    DataField = 'iParcela'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [22]
    Tag = 1
    Left = 112
    Top = 152
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 11
    OnKeyDown = DBEdit6KeyDown
  end
  object DBEdit9: TDBEdit [23]
    Tag = 1
    Left = 304
    Top = 200
    Width = 25
    Height = 19
    DataField = 'cSenso'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit12: TDBEdit [24]
    Left = 592
    Top = 176
    Width = 81
    Height = 19
    DataField = 'dDtVenc'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit16: TDBEdit [25]
    Tag = 1
    Left = 112
    Top = 200
    Width = 81
    Height = 19
    DataField = 'nValTit'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit21: TDBEdit [26]
    Tag = 1
    Left = 248
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdSP'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit26: TDBEdit [27]
    Tag = 1
    Left = 184
    Top = 56
    Width = 57
    Height = 19
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 15
  end
  object DBEdit27: TDBEdit [28]
    Tag = 1
    Left = 248
    Top = 56
    Width = 425
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 16
  end
  object DBEdit28: TDBEdit [29]
    Tag = 1
    Left = 184
    Top = 104
    Width = 369
    Height = 19
    DataField = 'cNmEspTit'
    DataSource = dsEspTit
    TabOrder = 17
  end
  object DBEdit29: TDBEdit [30]
    Tag = 1
    Left = 184
    Top = 152
    Width = 113
    Height = 19
    DataField = 'cCNPJCPF'
    DataSource = dsTerceiro
    TabOrder = 18
  end
  object DBEdit30: TDBEdit [31]
    Tag = 1
    Left = 304
    Top = 152
    Width = 369
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 19
  end
  object DBMemo1: TDBMemo [32]
    Left = 112
    Top = 232
    Width = 561
    Height = 249
    DataField = 'cObsTit'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit7: TDBEdit [33]
    Left = 112
    Top = 176
    Width = 97
    Height = 19
    DataField = 'dDtEmissao'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [34]
    Left = 304
    Top = 176
    Width = 97
    Height = 19
    DataField = 'dDtReceb'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit10: TDBEdit [35]
    Tag = 1
    Left = 112
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdLojaTit'
    DataSource = dsMaster
    TabOrder = 20
  end
  object DBEdit11: TDBEdit [36]
    Left = 592
    Top = 128
    Width = 81
    Height = 19
    DataField = 'cNrNF'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit13: TDBEdit [37]
    Tag = 1
    Left = 184
    Top = 80
    Width = 489
    Height = 19
    DataField = 'cNmLoja'
    DataSource = DataSource1
    TabOrder = 21
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TITULO'
      'WHERE nCdTitulo = :nPK')
    Left = 200
    Top = 328
    object qryMasternCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryMastercNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryMasteriParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasternCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryMasternCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMastercSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryMasterdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
    object qryMasterdDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryMasterdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryMasternValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValLiq: TBCDField
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryMasternSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryMasternValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryMasternValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryMastercObsTit: TMemoField
      FieldName = 'cObsTit'
      BlobType = ftMemo
    end
    object qryMasternCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryMasternCdBancoPortador: TIntegerField
      FieldName = 'nCdBancoPortador'
    end
    object qryMasterdDtRemPortador: TDateTimeField
      FieldName = 'dDtRemPortador'
    end
    object qryMasterdDtBloqTit: TDateTimeField
      FieldName = 'dDtBloqTit'
    end
    object qryMastercObsBloqTit: TStringField
      FieldName = 'cObsBloqTit'
      Size = 50
    end
    object qryMasternCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryMasternCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMasterdDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryMasternCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryMasterdDtCalcJuro: TDateTimeField
      FieldName = 'dDtCalcJuro'
    end
    object qryMasternCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryMasternCdCrediario: TIntegerField
      FieldName = 'nCdCrediario'
    end
    object qryMasternCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryMasternCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMastercFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryMastercNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryMasternCdLojaTit: TIntegerField
      FieldName = 'nCdLojaTit'
    end
  end
  inherited dsMaster: TDataSource
    Left = 248
    Top = 328
  end
  inherited qryID: TADOQuery
    Left = 296
    Top = 328
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 160
    Top = 328
  end
  inherited qryStat: TADOQuery
    Left = 1144
    Top = 72
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 368
    Top = 328
  end
  inherited ImageList1: TImageList
    Left = 336
    Top = 328
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      ',cSigla'
      ',cNmEmpresa'
      'FROM EMPRESA'
      'WHERE nCdEmpresa = :nPK')
    Left = 160
    Top = 384
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryEspTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM EspTit'
      'WHERE nCdEspTit = :nPK')
    Left = 200
    Top = 384
    object qryEspTitnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitnCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryEspTitcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryEspTitcTipoMov: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object qryEspTitcFlgPrev: TIntegerField
      FieldName = 'cFlgPrev'
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryTerceiroAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      ',nCdStatus'
      ',nCdMoeda'
      'FROM TERCEIRO'
      'WHERE nCdTerceiro = :nPK')
    Left = 280
    Top = 384
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceironCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryTerceironCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
  end
  object dsMovimento: TDataSource
    Left = 280
    Top = 424
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 336
    Top = 440
  end
  object dsEspTit: TDataSource
    DataSet = qryEspTit
    Left = 592
    Top = 320
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 648
    Top = 320
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 592
    Top = 424
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 312
    Top = 288
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLoja
    Left = 560
    Top = 392
  end
end
