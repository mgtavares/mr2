unit fTerceiro_MediaVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, cxPC, cxControls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, Mxstore, MXDB, TeeProcs, TeEngine, Chart,
  MXGRAPH, cxLookAndFeelPainters, cxButtons, Series, DbChart;

type
  TfrmTerceiro_MediaVenda = class(TfrmProcesso_Padrao)
    qryPreparaTemp: TADOQuery;
    qryPopulaTemp: TADOQuery;
    qryFaturamentoMensal: TADOQuery;
    qryFaturamentoSemestral: TADOQuery;
    qryFaturamentoSemestraliAno: TIntegerField;
    qryFaturamentoSemestraliSemestre: TIntegerField;
    qryFaturamentoSemestralcSemestreAno: TStringField;
    qryFaturamentoSemestralnValorTotal: TBCDField;
    qryFaturamentoSemestralnVariacao: TBCDField;
    qryFaturamentoAnual: TADOQuery;
    qryFaturamentoAnualiAno: TIntegerField;
    qryFaturamentoAnualnValorTotal: TBCDField;
    qryFaturamentoAnualnVariacao: TBCDField;
    RadioGroup1: TRadioGroup;
    dsFaturamentoMensal: TDataSource;
    dsFaturamentoSemestral: TDataSource;
    qryFaturamentoMensaliAno: TIntegerField;
    qryFaturamentoMensaliMes: TIntegerField;
    qryFaturamentoMensalcMesAno: TStringField;
    qryFaturamentoMensalnValorTotal: TBCDField;
    qryFaturamentoMensalnVariacao: TBCDField;
    dsFaturamentoAnual: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxPageControl2: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1cMesAno: TcxGridDBColumn;
    cxGridDBTableView1nValorTotal: TcxGridDBColumn;
    cxGridDBTableView1nVariacao: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    cxPageControl3: TcxPageControl;
    cxTabSheet4: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBTableView2cSemestreAno: TcxGridDBColumn;
    cxGridDBTableView2nValorTotal: TcxGridDBColumn;
    cxGridDBTableView2nVariacao: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    cxPageControl4: TcxPageControl;
    cxTabSheet5: TcxTabSheet;
    cxGrid3: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridDBTableView3iAno: TcxGridDBColumn;
    cxGridDBTableView3nValorTotal: TcxGridDBColumn;
    cxGridDBTableView3nVariacao: TcxGridDBColumn;
    cxGridLevel3: TcxGridLevel;
    Panel2: TPanel;
    cxButton1: TcxButton;
    DBChart1: TDBChart;
    Series1: TBarSeries;
    procedure atualizaValores(nCdTerceiro : integer; iIntervalo:integer) ;
    procedure exibeValores(nCdTerceiro : integer) ;
    procedure RadioGroup1Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure exibeCubo(nCdTerceiro : integer; dDtInicial, dDtFinal : TDateTime);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure cxGridDBTableView2DblClick(Sender: TObject);
    procedure cxGridDBTableView3DblClick(Sender: TObject);
  private
    { Private declarations }
    dDtInicialAux, dDtFinalAux : TDateTime;
  public
    { Public declarations }
    nCdTerceiroAux : integer ;
  end;

var
  frmTerceiro_MediaVenda: TfrmTerceiro_MediaVenda;

implementation

uses fMenu, dcVendaProdutoCliente_view;

{$R *.dfm}

{ TfrmTerceiro_MediaVenda }

procedure TfrmTerceiro_MediaVenda.atualizaValores(nCdTerceiro,
  iIntervalo: integer);
var
   dDataAux : TDateTime;
begin

    nCdTerceiroAux := nCdTerceiro ;
    dDataAux       := Date ;

    case RadioGroup1.ItemIndex of
        0 : dDataAux := dDataAux - 180          ; {-- ultimos 6 meses  --}
        1 : dDataAux := dDataAux - 365          ; {-- ultimos 12 meses --}
        2 : dDataAux := dDataAux - 730          ; {-- ultimos 24 meses --}
        3 : dDataAux := dDataAux - 1905         ; {-- ultimos 36 meses --}
        4 : dDataAux := strToDate('01/01/1900') ; {-- todo hist�rico   --}
    end ;

    qryPreparaTemp.ExecSQL;

    qryPopulaTemp.Parameters.ParamByName('nPK').Value           := nCdTerceiro ;
    qryPopulaTemp.Parameters.ParamByName('dDtInicialAux').Value := DateToStr(dDataAux) ;
    qryPopulaTemp.ExecSQL;

    qryFaturamentoMensal.Close;
    qryFaturamentoMensal.Open;

    qryFaturamentoSemestral.Close;
    qryFaturamentoSemestral.Open;

    qryFaturamentoAnual.Close;
    qryFaturamentoAnual.Open;

end;

procedure TfrmTerceiro_MediaVenda.exibeValores(nCdTerceiro: integer);
begin

    RadioGroup1.ItemIndex := 1 ;

    atualizaValores( nCdTerceiro
                   , RadioGroup1.ItemIndex) ;

    Self.ShowModal ;

    qryFaturamentoMensal.Close;
    qryFaturamentoSemestral.Close;
    qryFaturamentoAnual.Close;

end;

procedure TfrmTerceiro_MediaVenda.RadioGroup1Click(Sender: TObject);
begin
  inherited;

  atualizaValores(nCdTerceiroAux
                 ,RadioGroup1.ItemIndex) ;

end;

procedure TfrmTerceiro_MediaVenda.cxButton1Click(Sender: TObject);
begin

  {DecisionGraph1.PrintLandscape;}

end;

procedure TfrmTerceiro_MediaVenda.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmTerceiro_MediaVenda.exibeCubo(nCdTerceiro: integer;
  dDtInicial, dDtFinal: TDateTime);
var
  objForm : TdcmVendaProdutoCliente_view ;
begin

  objForm := TdcmVendaProdutoCliente_view.Create(nil) ;
  
  objForm.ADODataSet1.Close ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value        := 0 ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdGrupoEconomico').Value := 0 ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdTerceiro').Value       := nCdTerceiro ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdDepartamento').Value   := 0 ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdMarca').Value          := 0 ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdLinha').Value          := 0 ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdClasseProduto').Value  := 0 ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtInicial').Value        := DateToStr(dDtInicial) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtFinal').Value          := DateToStr(dDtFinal) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdRamoAtividade').Value  := 0 ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdTerceiroRepres').Value := 0 ;
  objForm.ADODataSet1.Open ;

  if (objForm.ADODataSet1.eof) then
  begin
      ShowMessage('Nenhuma informa��o encontrada.') ;
      exit ;
  end ;

  frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

  if objForm.PivotCube1.Active then
  begin
      objForm.PivotCube1.Active := False;
  end;

  objForm.PivotCube1.ExtendedMode := True;
  objForm.PVMeasureToolBar1.HideButtons := False;
  objForm.PivotCube1.Active := True;

  objForm.PivotMap1.Measures[2].ColumnPercent := True;
  objForm.PivotMap1.Measures[2].Value := False;

  objForm.PivotMap1.Measures[3].Rank := True ;
  objForm.PivotMap1.Measures[3].Value := False;

  objForm.PivotMap1.SortColumn(0,3,False) ;

  objForm.PivotMap1.Title := 'ER2Soft - An�lise de Vendas por Clientes' ;
  objForm.PivotGrid1.RefreshData;

  frmMenu.StatusBar1.Panels[6].Text := '' ;

  showForm(objForm , TRUE) ;
  
  Self.Activate ;


end;

procedure TfrmTerceiro_MediaVenda.cxGridDBTableView1DblClick(
  Sender: TObject);
var
  iMes : Integer;
  iAno : Integer;
begin
  inherited;

  if (qryFaturamentoMensal.Active) and (qryFaturamentoMensal.RecordCount > 0) then
  begin

      {-- forma a data inicial do per�odo --}
      dDtInicialAux := StrToDate('01/' + frmMenu.ZeroEsquerda(qryFaturamentoMensaliMes.AsString,2) + '/' + qryFaturamentoMensaliAno.AsString) ;

      {-- forma a data final do per�odo (apenas soma +1 no m�s) --}
      iMes := StrToInt(frmMenu.ZeroEsquerda(intToStr(qryFaturamentoMensaliMes.Value+1),2));
      iAno := qryFaturamentoMensaliAno.AsInteger;

      if (iMes = 13) then
      begin
          iMes := 1;
          iAno := iAno + 1;
      end;

      //dDtFinalAux   := StrToDate('01/' + frmMenu.ZeroEsquerda(intToStr(qryFaturamentoMensaliMes.Value+1),2) + '/' + qryFaturamentoMensaliAno.AsString) ;
      dDtFinalAux := StrToDate('01/' + IntToStr(iMes) + '/' + IntToStr(iAno));

      {-- como a data acima veio no primeiro dia do pr�ximo mes, debita um dia da data e recupera o ultimo dia do mes anterior --}
      dDtFinalAux := dDtFinalAux -1 ;

      exibeCubo( nCdTerceiroAux
               , dDtInicialAux
               , dDtFinalAux    );

  end ;

end;

procedure TfrmTerceiro_MediaVenda.cxGridDBTableView2DblClick(
  Sender: TObject);
begin
  inherited;

  if (qryFaturamentoSemestral.Active) and (qryFaturamentoSemestral.RecordCount > 0) then
  begin

      {-- primeiro semestre --}
      if (qryFaturamentoSemestraliSemestre.Value = 1) then
      begin

          {-- forma a data inicial do per�odo --}
          dDtInicialAux := StrToDate('01/01/' + qryFaturamentoSemestraliAno.AsString) ;

          {-- forma a data final do semestre --}
          dDtFinalAux   := StrToDate('01/07/' + qryFaturamentoSemestraliAno.AsString) ;

          {-- como a data acima veio no primeiro dia do pr�ximo mes, debita um dia da data e recupera o ultimo dia do mes anterior --}
          dDtFinalAux   := dDtFinalAux -1 ;

      end
      else
      begin
          {-- segundo semestre --}

          {-- forma a data inicial do per�odo --}
          dDtInicialAux := StrToDate('01/07/' + qryFaturamentoSemestraliAno.AsString) ;

          {-- forma a data final do per�odo (apenas soma +1 no m�s) --}
          dDtFinalAux   := StrToDate('31/12/' + qryFaturamentoSemestraliAno.AsString) ;

      end ;


      exibeCubo( nCdTerceiroAux
               , dDtInicialAux
               , dDtFinalAux    );

  end ;

end;

procedure TfrmTerceiro_MediaVenda.cxGridDBTableView3DblClick(
  Sender: TObject);
begin
  inherited;

  if (qryFaturamentoAnual.Active) and (qryFaturamentoAnual.RecordCount > 0) then
  begin

      {-- forma a data inicial do per�odo --}
      dDtInicialAux := StrToDate('01/01/' + qryFaturamentoAnualiAno.AsString) ;
      dDtFinalAux   := StrToDate('31/12/' + qryFaturamentoAnualiAno.AsString) ;

      exibeCubo( nCdTerceiroAux
               , dDtInicialAux
               , dDtFinalAux    );

  end ;

end;

end.
