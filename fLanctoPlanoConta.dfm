inherited frmLanctoPlanoConta: TfrmLanctoPlanoConta
  Height = 643
  Caption = 'Lan'#231'amento Plano Conta'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 582
  end
  object Label1: TLabel [1]
    Left = 62
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 57
    Top = 62
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 10
    Top = 86
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Unidade Neg'#243'cio'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 69
    Top = 110
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 6
    Top = 158
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Compet'#234'ncia'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 27
    Top = 206
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Cadastro'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 8
    Top = 182
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Lan'#231'amento'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 60
    Top = 230
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 40
    Top = 251
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observa'#231#227'o'
    FocusControl = DBMemo1
  end
  object Label11: TLabel [10]
    Left = 44
    Top = 134
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Conta'
    FocusControl = DBEdit12
  end
  object DBEdit1: TDBEdit [12]
    Tag = 1
    Left = 104
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdLanctoPlanoConta'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [13]
    Tag = 1
    Left = 104
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [14]
    Left = 104
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdUnidadeNegocio'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [15]
    Left = 104
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdPlanoConta'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit4Exit
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [16]
    Left = 104
    Top = 152
    Width = 81
    Height = 19
    DataField = 'dDtCompetencia'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [17]
    Tag = 1
    Left = 104
    Top = 200
    Width = 169
    Height = 19
    DataField = 'dDtProcesso'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit7: TDBEdit [18]
    Left = 104
    Top = 176
    Width = 169
    Height = 19
    DataField = 'nValLancto'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit8: TDBEdit [19]
    Tag = 1
    Left = 104
    Top = 224
    Width = 65
    Height = 19
    DataField = 'nCdUsuarioCad'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBMemo1: TDBMemo [20]
    Left = 104
    Top = 249
    Width = 721
    Height = 224
    DataField = 'cOBS'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit9: TDBEdit [21]
    Tag = 1
    Left = 176
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 10
  end
  object DBEdit10: TDBEdit [22]
    Tag = 1
    Left = 176
    Top = 80
    Width = 650
    Height = 19
    DataField = 'cNmUnidadeNegocio'
    DataSource = DataSource2
    TabOrder = 11
  end
  object DBEdit11: TDBEdit [23]
    Tag = 1
    Left = 176
    Top = 104
    Width = 650
    Height = 19
    DataField = 'cNmPlanoConta'
    DataSource = DataSource3
    TabOrder = 12
  end
  object DBEdit12: TDBEdit [24]
    Tag = 1
    Left = 104
    Top = 128
    Width = 121
    Height = 19
    DataField = 'cTipoConta'
    DataSource = DataSource3
    TabOrder = 13
  end
  object DBEdit13: TDBEdit [25]
    Tag = 1
    Left = 176
    Top = 224
    Width = 650
    Height = 19
    DataField = 'cNmUsuario'
    DataSource = DataSource4
    TabOrder = 14
  end
  object GroupBox1: TGroupBox [26]
    Left = 305
    Top = 128
    Width = 521
    Height = 89
    Caption = 'Rastreamento'
    TabOrder = 15
    object Label10: TLabel
      Left = 395
      Top = 62
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. SP'
      FocusControl = DBEdit14
    end
    object Label12: TLabel
      Left = 31
      Top = 38
      Width = 66
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Pedido'
      FocusControl = DBEdit15
    end
    object Label13: TLabel
      Left = 184
      Top = 38
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'd. Item Pedido'
      FocusControl = DBEdit16
    end
    object Label14: TLabel
      Left = 368
      Top = 38
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'd. Produto'
      FocusControl = DBEdit17
    end
    object Label15: TLabel
      Left = 8
      Top = 62
      Width = 89
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Lancto. Fin.'
      FocusControl = DBEdit18
    end
    object Label16: TLabel
      Left = 175
      Top = 62
      Width = 97
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Recebimento'
      FocusControl = DBEdit19
    end
    object DBEdit14: TDBEdit
      Tag = 1
      Left = 440
      Top = 56
      Width = 65
      Height = 19
      DataField = 'nCdSP'
      DataSource = dsMaster
      TabOrder = 0
    end
    object DBEdit15: TDBEdit
      Tag = 1
      Left = 104
      Top = 32
      Width = 65
      Height = 19
      DataField = 'nCdPedido'
      DataSource = dsMaster
      TabOrder = 1
    end
    object DBEdit16: TDBEdit
      Tag = 1
      Left = 280
      Top = 32
      Width = 65
      Height = 19
      DataField = 'nCdItemPedido'
      DataSource = dsMaster
      TabOrder = 2
    end
    object DBEdit17: TDBEdit
      Tag = 1
      Left = 440
      Top = 32
      Width = 65
      Height = 19
      DataField = 'nCdProduto'
      DataSource = dsMaster
      TabOrder = 3
    end
    object DBEdit18: TDBEdit
      Tag = 1
      Left = 104
      Top = 56
      Width = 65
      Height = 19
      DataField = 'nCdLanctoFin'
      DataSource = dsMaster
      TabOrder = 4
    end
    object DBEdit19: TDBEdit
      Tag = 1
      Left = 280
      Top = 56
      Width = 65
      Height = 19
      DataField = 'nCdRecebimento'
      DataSource = dsMaster
      TabOrder = 5
    end
    object DBCheckBox1: TDBCheckBox
      Left = 104
      Top = 8
      Width = 169
      Height = 17
      Caption = 'Lan'#231'amento Manual'
      DataField = 'cFlgManual'
      DataSource = dsMaster
      ReadOnly = True
      TabOrder = 6
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM LanctoPlanoConta'
      'WHERE nCdEmpresa = :nCdEmpresa'
      'AND nCdLanctoPlanoConta = :nPK')
    Top = 216
    object qryMasternCdLanctoPlanoConta: TAutoIncField
      FieldName = 'nCdLanctoPlanoConta'
      ReadOnly = True
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryMasternCdGrupoPlanoConta: TIntegerField
      FieldName = 'nCdGrupoPlanoConta'
    end
    object qryMasternCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryMasterdDtCompetencia: TDateTimeField
      FieldName = 'dDtCompetencia'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasternCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryMasternCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryMasterdDtProcesso: TDateTimeField
      FieldName = 'dDtProcesso'
    end
    object qryMasternValLancto: TBCDField
      FieldName = 'nValLancto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryMasternCdItemPedido: TIntegerField
      FieldName = 'nCdItemPedido'
    end
    object qryMasternCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryMasternCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMastercFlgManual: TIntegerField
      FieldName = 'cFlgManual'
    end
    object qryMasternCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qryMastercOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
    object qryMasternCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
  end
  inherited dsMaster: TDataSource
    Top = 216
  end
  inherited qryID: TADOQuery
    Top = 216
  end
  inherited usp_ProximoID: TADOStoredProc
    Top = 272
  end
  inherited qryStat: TADOQuery
    Top = 288
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Top = 328
  end
  inherited ImageList1: TImageList
    Top = 312
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 864
    Top = 248
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 592
    Top = 328
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUnidadeNegocio, cNmUnidadeNegocio'
      'FROM UnidadeNegocio'
      'WHERE nCdUnidadeNegocio = :nPK')
    Left = 856
    Top = 320
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 600
    Top = 336
  end
  object qryPlanoConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdPlanoConta'
      '      ,cNmPlanoConta'
      '      ,CASE WHEN cFlgRecDes = '#39'D'#39' THEN '#39'DESPESA'#39
      '            ELSE '#39'RECEITA'#39
      '       END cTipoConta'
      ',nCdGrupoPlanoConta'
      '  FROM PlanoConta'
      ' WHERE cFlgLanc = 1'
      '   AND nCdPlanoConta = :nPK')
    Left = 856
    Top = 376
    object qryPlanoContanCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryPlanoContacNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
    object qryPlanoContacTipoConta: TStringField
      FieldName = 'cTipoConta'
      ReadOnly = True
      Size = 7
    end
    object qryPlanoContanCdGrupoPlanoConta: TIntegerField
      FieldName = 'nCdGrupoPlanoConta'
    end
  end
  object DataSource3: TDataSource
    DataSet = qryPlanoConta
    Left = 608
    Top = 344
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario, cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 960
    Top = 240
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object DataSource4: TDataSource
    DataSet = qryUsuario
    Left = 592
    Top = 304
  end
end
