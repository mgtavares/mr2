unit fSyncInfra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls;

type
  TfrmSyncInfra = class(TfrmProcesso_Padrao)
    Edit1: TEdit;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    ConnDestino: TADOConnection;
    ckLookup: TCheckBox;
    ckTabelaSistema: TCheckBox;
    ckUltimoCodigo: TCheckBox;
    ckAplicacao: TCheckBox;
    qryLookup: TADOQuery;
    qryLookupnCdTabelaConsulta: TIntegerField;
    qryLookupcNmTabelaConsulta: TStringField;
    qryLookupComandoSQL: TMemoField;
    qryFiltroTabelaConsulta: TADOQuery;
    qryFiltroTabelaConsultanCdTabelaConsulta: TIntegerField;
    qryFiltroTabelaConsultacNmCampoTabela: TStringField;
    qryFiltroTabelaConsultacTipoDado: TStringField;
    qryFiltroTabelaConsultacNmCampoTela: TStringField;
    qryAux: TADOQuery;
    qryLookupDestino: TADOQuery;
    qryLookupDestinonCdTabelaConsulta: TIntegerField;
    qryLookupDestinocNmTabelaConsulta: TStringField;
    qryLookupDestinoComandoSQL: TMemoField;
    qryFiltroTabelaConsultaDestino: TADOQuery;
    qryFiltroTabelaConsultaDestinonCdTabelaConsulta: TIntegerField;
    qryFiltroTabelaConsultaDestinocNmCampoTabela: TStringField;
    qryFiltroTabelaConsultaDestinocTipoDado: TStringField;
    qryFiltroTabelaConsultaDestinocNmCampoTela: TStringField;
    qryTabelaSistema: TADOQuery;
    qryTabelaSistemanCdTabelaSistema: TIntegerField;
    qryTabelaSistemacTabela: TStringField;
    qryTabelaSistemacNmTabela: TStringField;
    qryTabelaSistemaDestino: TADOQuery;
    qryTabelaSistemaDestinonCdTabelaSistema: TIntegerField;
    qryTabelaSistemaDestinocTabela: TStringField;
    qryTabelaSistemaDestinocNmTabela: TStringField;
    qryUltimoCodigo: TADOQuery;
    qryUltimoCodigocNmTabela: TStringField;
    qryUltimoCodigoiUltimoCodigo: TIntegerField;
    qryUltimoCodigoDestino: TADOQuery;
    qryUltimoCodigoDestinocNmTabela: TStringField;
    qryUltimoCodigoDestinoiUltimoCodigo: TIntegerField;
    qryAplicacao: TADOQuery;
    qryAplicacaonCdAplicacao: TIntegerField;
    qryAplicacaocNmAplicacao: TStringField;
    qryAplicacaonCdStatus: TIntegerField;
    qryAplicacaonCdModulo: TIntegerField;
    qryAplicacaocNmObjeto: TStringField;
    qryAplicacaoDestino: TADOQuery;
    qryAplicacaoDestinonCdAplicacao: TIntegerField;
    qryAplicacaoDestinocNmAplicacao: TStringField;
    qryAplicacaoDestinonCdStatus: TIntegerField;
    qryAplicacaoDestinonCdModulo: TIntegerField;
    qryAplicacaoDestinocNmObjeto: TStringField;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSyncInfra: TfrmSyncInfra;

implementation

{$R *.dfm}

procedure TfrmSyncInfra.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(Edit1.Text) = '') then
  begin
      MensagemAlerta('Informe o servidor de destino.') ;
      Edit1.SetFocus ;
      exit ;
  end ;

  if (Trim(Edit2.Text) = '') then
  begin
      MensagemAlerta('Informe o Database.') ;
      Edit2.SetFocus ;
      exit ;
  end ;

  ConnDestino.Close ;
  ConnDestino.ConnectionString := 'provider=MSDASQL.1;driver={SQL Server};server=' + Edit1.Text + ';uid=ADMSoft;pwd=admsoft101580;database=' + Edit2.Text ;
  ConnDestino.LoginPrompt := false ;
  ConnDestino.Open();

  if not ConnDestino.Connected then
      exit ;


  if (ckLookup.Checked) then
  begin

      qryLookup.Close ;
      qryLookup.Open ;

      if not qryLookup.eof then
      begin

          qryLookupDestino.Close ;
          qryLookupDestino.Connection := connDestino ;
          qryLookupDestino.Open ;

          qryLookup.First ;

          while not qryLookup.Eof do
          begin

              // verifica se a lookup existe no servidor de destino
              qryAux.Close ;
              qryAux.Connection := connDestino ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT 1 FROM TabelaConsulta WHERE nCdTabelaConsulta = ' + qryLookupnCdTabelaConsulta.AsString);
              qryAux.Open ;

              if qryAux.Eof then
              begin

                  connDestino.BeginTrans;

                  try
                      // inseri a lookup
                      qryLookupDestino.Insert ;
                      qryLookupDestinonCdTabelaConsulta.Value := qryLookupnCdTabelaConsulta.Value ;
                      qryLookupDestinocNmTabelaConsulta.Value := qryLookupcNmTabelaConsulta.Value ;
                      qryLookupDestinoComandoSQL.Value        := qryLookupComandoSQL.Value ;
                      qryLookupDestino.Post ;

                      // inseri os campos ;

                      PosicionaQuery(qryFiltroTabelaConsulta, qryLookupnCdTabelaConsulta.AsString) ;

                      if not qryFiltroTabelaConsulta.Eof then
                      begin

                          qryFiltroTabelaConsulta.First ;

                          qryFiltroTabelaConsultaDestino.Close ;
                          qryFiltroTabelaConsultaDestino.Connection := ConnDestino ;
                          qryFiltroTabelaConsultaDestino.Open ;

                          while not qryFiltroTabelaConsulta.Eof do
                          begin
                              qryFiltroTabelaConsultaDestino.Insert ;
                              qryFiltroTabelaConsultaDestinonCdTabelaConsulta.Value := qryFiltroTabelaConsultanCdTabelaConsulta.Value ;
                              qryFiltroTabelaConsultaDestinocNmCampoTabela.Value    := qryFiltroTabelaConsultacNmCampoTabela.Value ;
                              qryFiltroTabelaConsultaDestinocTipoDado.Value         := qryFiltroTabelaConsultacTipoDado.Value ;
                              qryFiltroTabelaConsultaDestinocNmCampoTela.Value      := qryFiltroTabelaConsultacNmCampoTela.Value ;
                              qryFiltroTabelaConsultaDestino.Post ;

                              qryFiltroTabelaConsulta.Next ;
                          end ;

                          qryFiltroTabelaConsulta.Close ;

                      end ;

                  except
                      ConnDestino.RollbackTrans;
                      raise ;
                  end ;

                  ConnDestino.CommitTrans;

              end ;

              qryLookup.Next ;

          end ;

      end ;

  end ;

  if (ckTabelaSistema.Checked) then
  begin

      qryTabelaSistema.Close ;
      qryTabelaSistema.Open ;

      if not qryTabelaSistema.eof then
      begin

          qryTabelaSistemaDestino.Close ;
          qryTabelaSistemaDestino.Connection := connDestino ;
          qryTabelaSistemaDestino.Open ;

          qryTabelaSistema.First ;

          while not qryTabelaSistema.Eof do
          begin

              // verifica se a tabela existe no servidor de destino
              qryAux.Close ;
              qryAux.Connection := connDestino ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT 1 FROM TabelaSistema WHERE nCdTabelaSistema = ' + qryTabelaSistemanCdTabelaSistema.AsString);
              qryAux.Open ;

              if qryAux.Eof then
              begin

                  connDestino.BeginTrans;

                  try
                      // inseri a tabela sistema
                      qryTabelaSistemaDestino.Insert ;
                      qryTabelaSistemaDestinonCdTabelaSistema.Value := qryTabelaSistemanCdTabelaSistema.Value ;
                      qryTabelaSistemaDestinocTabela.Value          := qryTabelaSistemacTabela.Value ;
                      qryTabelaSistemaDestinocNmTabela.Value        := qryTabelaSistemacNmTabela.Value ;
                      qryTabelaSistemaDestino.Post ;

                  except
                      ConnDestino.RollbackTrans;
                      raise ;
                  end ;

                  ConnDestino.CommitTrans;

              end ;

              qryTabelaSistema.Next ;

          end ;

      end ;

  end ;


  if (ckUltimoCodigo.Checked) then
  begin

      qryUltimoCodigo.Close ;
      qryUltimoCodigo.Open ;

      if not qryUltimoCodigo.eof then
      begin

          qryUltimoCodigoDestino.Close ;
          qryUltimoCodigoDestino.Connection := connDestino ;
          qryUltimoCodigoDestino.Open ;

          qryUltimoCodigo.First ;

          while not qryUltimoCodigo.Eof do
          begin

              // verifica se a tabela existe no servidor de destino
              qryAux.Close ;
              qryAux.Connection := connDestino ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT 1 FROM UltimoCodigo WHERE cNmTabela = ' + Chr(39) + Trim(qryUltimoCodigocNmTabela.Value) + Chr(39));
              qryAux.Open ;

              if qryAux.Eof then
              begin

                  connDestino.BeginTrans;

                  try
                      // inseri a tabela sistema
                      qryUltimoCodigoDestino.Insert ;
                      qryUltimoCodigoDestinocNmTabela.Value     := qryUltimoCodigocNmTabela.Value ;
                      qryUltimoCodigoDestinoiUltimoCodigo.Value := qryUltimoCodigoiUltimoCodigo.Value ;
                      qryUltimoCodigoDestino.Post ;

                  except
                      ConnDestino.RollbackTrans;
                      raise ;
                  end ;

                  ConnDestino.CommitTrans;

              end ;

              qryUltimoCodigo.Next ;

          end ;

      end ;

  end ;

  if (ckAplicacao.Checked) then
  begin

      qryAplicacao.Close ;
      qryAplicacao.Open ;

      if not qryAplicacao.eof then
      begin

          qryAplicacaoDestino.Close ;
          qryAplicacaoDestino.Connection := connDestino ;
          qryAplicacaoDestino.Open ;

          qryAplicacao.First ;

          while not qryAplicacao.Eof do
          begin

              // verifica se a tabela existe no servidor de destino
              qryAux.Close ;
              qryAux.Connection := connDestino ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT 1 FROM Aplicacao WHERE nCdAplicacao = ' + qryAplicacaonCdAplicacao.AsString) ;
              qryAux.Open ;

              if qryAux.Eof then
              begin

                  connDestino.BeginTrans;

                  try

                      // inseri a aplicacao

                      qryAplicacaoDestino.Insert ;
                      qryAplicacaoDestinonCdAplicacao.Value := qryAplicacaonCdAplicacao.Value ;
                      qryAplicacaoDestinocNmAplicacao.Value := qryAplicacaocNmAplicacao.Value ;
                      qryAplicacaoDestinonCdStatus.Value    := qryAplicacaonCdStatus.Value    ;
                      qryAplicacaoDestinonCdModulo.Value    := qryAplicacaonCdModulo.Value    ;
                      qryAplicacaoDestinocNmObjeto.Value    := qryAplicacaocNmObjeto.Value    ;
                      qryAplicacaoDestino.Post ;

                  except
                      ConnDestino.RollbackTrans;
                      raise ;
                  end ;

                  ConnDestino.CommitTrans;

              end ;

              qryAplicacao.Next ;

          end ;

      end ;

  end ;

  ConnDestino.Close ;

  ShowMessage('Sincronização concluída.') ;

end;

initialization
    RegisterClass(TfrmSyncInfra) ;

end.
