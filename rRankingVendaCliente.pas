unit rRankingVendaCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls;

type
  TrptRankingVendaCliente = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit4: TMaskEdit;
    Label2: TLabel;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    dsGrupoEconomico: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRankingVendaCliente: TrptRankingVendaCliente;

implementation

uses fMenu, fLookup_Padrao, rRankingVendaCliente_view;

{$R *.dfm}

procedure TrptRankingVendaCliente.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  iAux := frmMenu.UsuarioEmpresaAutorizada;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  MaskEdit3.SetFocus ;

end;


procedure TrptRankingVendaCliente.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TrptRankingVendaCliente.ToolButton1Click(Sender: TObject);
var
   objRel : TrptRankingVendaCliente_view;
begin

  objRel := TrptRankingVendaCliente_view.Create(nil);
  Try
      Try
          objRel.usp_Relatorio.Close ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value  := frmMenu.ConvInteiro(MaskEdit3.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdGrupoEconomico').Value := frmMenu.ConvInteiro(MaskEdit4.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@dDtInicial').Value  := frmMenu.ConvData(MaskEdit1.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@dDtFinal').Value    := frmMenu.ConvData(MaskEdit2.Text) ;
          objRel.usp_Relatorio.Open  ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := 'Empresa : '        + Trim(MaskEdit3.Text) + ' ' + DbEdit3.Text ;
          objRel.lblFiltro3.Caption := 'Grupo Economico: ' + String(MaskEdit4.Text) + ' ' + DbEdit1.Text ;
          objRel.lblFiltro4.Caption := 'Per�odo        : ' + String(MaskEdit1.Text) + ' ' + String(MaskEdit2.Text) ;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  Finally;
      FreeAndNil(objRel);
  end;
end;


procedure TrptRankingVendaCliente.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptRankingVendaCliente.MaskEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptRankingVendaCliente.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close ;
  
  PosicionaQuery(qryGrupoEconomico, MaskEdit4.Text) ;
end;

initialization
     RegisterClass(TrptRankingVendaCliente) ;

end.
