unit dcAnaliseLucratividade_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, PivotMap_SRC, PivotCube_SRC, PivotGrid_SRC, PivotToolBar_SRC;

type
  TdcmAnaliseLucratividade_view = class(TfrmProcesso_Padrao)
    ADODataSet1: TADODataSet;
    PVRowToolBar1: TPVRowToolBar;
    PVMeasureToolBar1: TPVMeasureToolBar;
    PVColToolBar1: TPVColToolBar;
    PivotGrid1: TPivotGrid;
    PivotCube1: TPivotCube;
    PivotMap1: TPivotMap;
    ADODataSet1Ano: TDateTimeField;
    ADODataSet1Mes: TDateTimeField;
    ADODataSet1cNmDepartamento: TStringField;
    ADODataSet1cNmCategoria: TStringField;
    ADODataSet1cNmSubCategoria: TStringField;
    ADODataSet1cNmSegmento: TStringField;
    ADODataSet1cNmMarca: TStringField;
    ADODataSet1cNmLinha: TStringField;
    ADODataSet1cNmClasseProduto: TStringField;
    ADODataSet1cNmProduto: TStringField;
    ADODataSet1cNmRamoAtividade: TStringField;
    ADODataSet1cNmTerceiro: TStringField;
    ADODataSet1nValor: TBCDField;
    ADODataSet1nQtde: TBCDField;
    ADODataSet1nValCustoMedio: TBCDField;
    ADODataSet1nValLucro: TBCDField;
    ADODataSet1nPercLucro: TBCDField;
    procedure PivotGrid1GetCellValue(Sender: TObject; X, Y, RowIndex,
      ColumnIndex, MeasureIndex, ViewIndex: Integer; var Value: Double;
      var CellString: String; Cell: TpvCellDrawing);
    procedure PivotGrid1GetColumnTotal(Sender: TObject; X, Y, RowIndex,
      ColumnIndex, MeasureIndex, ViewIndex: Integer; var Value: Double;
      var CellString: String; Cell: TpvCellDrawing);
  private
    { Private declarations }
    nValorVenda : Double ;
    nValorCusto : Double ;
  public
    { Public declarations }
  end;

var
  dcmAnaliseLucratividade_view: TdcmAnaliseLucratividade_view;

implementation

{$R *.dfm}

procedure TdcmAnaliseLucratividade_view.PivotGrid1GetCellValue(
  Sender: TObject; X, Y, RowIndex, ColumnIndex, MeasureIndex,
  ViewIndex: Integer; var Value: Double; var CellString: String;
  Cell: TpvCellDrawing);
var
tmpMeasure: TMapMeasure;
begin
    tmpMeasure := PivotMap1.MeasureByIndex[MeasureIndex];

    if (tmpMeasure.AliasName = 'nPercLucroPourcentGroupByRow') then
    begin
        nValorCusto := PivotMap1.Cells[ColumnIndex, RowIndex, MeasureIndex-2, 0];
        nValorVenda := PivotMap1.Cells[ColumnIndex, RowIndex, MeasureIndex-3, 0];

        if (nValorCusto > 0) then
            Value := (nValorVenda / nValorCusto) * 100 ;

        if (nValorCusto = 0) or (nValorVenda = 0) then
            Value := 100 ;
    end ;


end;

procedure TdcmAnaliseLucratividade_view.PivotGrid1GetColumnTotal(
  Sender: TObject; X, Y, RowIndex, ColumnIndex, MeasureIndex,
  ViewIndex: Integer; var Value: Double; var CellString: String;
  Cell: TpvCellDrawing);
var
tmpMeasure: TMapMeasure;
begin
    tmpMeasure := PivotMap1.MeasureByIndex[MeasureIndex];

    if tmpMeasure.AliasName = 'nPercLucroPourcentGroupByRow' then
    begin
        nValorCusto := PivotMap1.Cells[ColumnIndex, RowIndex, MeasureIndex-2, 0];
        nValorVenda := PivotMap1.Cells[ColumnIndex, RowIndex, MeasureIndex-3, 0];

        if (nValorCusto > 0) then
            Value := (nValorVenda / nValorCusto) * 100 ;

        if (nValorCusto = 0) or (nValorVenda = 0) then
            Value := 100 ;
    end ;

end;

end.
