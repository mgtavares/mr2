unit fProduto_HistPrecoVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmProduto_HistPrecoVenda = class(TfrmProcesso_Padrao)
    DBGridEh2: TDBGridEh;
    qryHistorico: TADOQuery;
    dsHistorico: TDataSource;
    qryHistoricodDtAltPreco: TDateTimeField;
    qryHistoriconValPrecoAtual: TBCDField;
    qryHistoriconValPrecoAnterior: TBCDField;
    qryHistoricocNmUsuario: TStringField;
    qryHistoricocOBS: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProduto_HistPrecoVenda: TfrmProduto_HistPrecoVenda;

implementation

{$R *.dfm}

end.
