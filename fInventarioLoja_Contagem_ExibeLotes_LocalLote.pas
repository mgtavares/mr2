unit fInventarioLoja_Contagem_ExibeLotes_LocalLote;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, ToolCtrlsEh, DB, ADODB, GridsEh, DBGridEh;

type
  TfrmInventarioLoja_Contagem_ExibeLotes_LocalLote = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryTempLote: TADOQuery;
    dsTempLote: TDataSource;
    qryLoteInventario: TADOQuery;
    qryValidaLote: TADOQuery;
    qryLoteInventarionCdLoteContagemInventario: TIntegerField;
    qryLoteInventarionCdInventario: TIntegerField;
    qryLoteInventariocNrLote: TStringField;
    qryLoteInventarioiQtdeProdutos: TIntegerField;
    qryLoteInventarioiQtdeProdutosInv: TIntegerField;
    qryLoteInventariocLocalLote: TStringField;
    qryTempLotenCdLoteContagemInventario: TIntegerField;
    qryTempLotecNrLote: TStringField;
    qryTempLotecLocalLote: TStringField;
    qryTempLoteiQtdeProdutos: TIntegerField;
    cmdPreparaTemp: TADOCommand;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdInventario : integer;
  end;

var
  frmInventarioLoja_Contagem_ExibeLotes_LocalLote: TfrmInventarioLoja_Contagem_ExibeLotes_LocalLote;

implementation

uses
    fMenu, fInventarioLoja_Contagem_ExibeLotes;

{$R *.dfm}

procedure TfrmInventarioLoja_Contagem_ExibeLotes_LocalLote.ToolButton1Click(
  Sender: TObject);
begin
  if (qryTempLote.RecordCount = 0) then
  begin
      ShowMessage('Nenhum lote informado para valida��o.');
      Abort;
  end;

  qryValidaLote.Close;
  qryValidaLote.Parameters.ParamByName('nPK').Value := nCdInventario;
  qryValidaLote.Open;

  if (not qryValidaLote.IsEmpty) then
  begin
      MessageDLG('Erro ao validar lote(s), verfique se as informa��es est�o corretas.', mtWarning, [mbOk], 0);
      Abort;
  end;

  qryTempLote.Close;
  qryTempLote.Open;

  qryTempLote.First;

  while not qryTempLote.Eof do
  begin
      qryLoteInventario.Close;
      qryLoteInventario.Parameters.ParamByName('nCdLoteContagemInventario').Value := qryTempLotenCdLoteContagemInventario.Value;
      qryLoteInventario.Open;

      qryLoteInventario.Edit;
      qryLoteInventariocLocalLote.Value    := qryTempLotecLocalLote.Value;
      qryLoteInventarioiQtdeProdutos.Value := qryTempLoteiQtdeProdutos.Value;
      qryLoteInventario.Post;

      qryTempLote.Next
  end;

  Close;
end;

procedure TfrmInventarioLoja_Contagem_ExibeLotes_LocalLote.ToolButton2Click(
  Sender: TObject);
begin
  if (MessageDLG('Se for efetuado o cancelamento os registros ser�o perdidos.' + #13#13 + 'Confirma cancelamento ?', mtConfirmation, [mbYes,mbCancel], 0) = mrYes) then
      Close;
end;

initialization
    RegisterClass(TfrmInventarioLoja_Contagem_ExibeLotes_LocalLote);

end.
