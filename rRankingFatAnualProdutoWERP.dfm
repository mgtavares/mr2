inherited rptRankingFatAnualProdutoWERP: TrptRankingFatAnualProdutoWERP
  Left = 51
  Top = 132
  Caption = 'Curva ABC de produtos'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 51
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label3: TLabel [2]
    Left = 20
    Top = 144
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'M'#234's/Ano Inicial'
  end
  object Label5: TLabel [3]
    Left = 54
    Top = 96
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
  end
  object Label6: TLabel [4]
    Left = 11
    Top = 120
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Linha de Produto'
  end
  object Label10: TLabel [5]
    Left = 59
    Top = 72
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
  end
  object Label4: TLabel [6]
    Left = 172
    Top = 144
    Width = 231
    Height = 13
    Caption = '(Ser'#225' projetado 12 meses a partir do m'#234's inicial)'
  end
  inherited ToolBar1: TToolBar
    TabOrder = 6
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [8]
    Tag = 1
    Left = 160
    Top = 40
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 7
  end
  object DBEdit3: TDBEdit [9]
    Tag = 1
    Left = 232
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 8
  end
  object MaskEdit1: TMaskEdit [10]
    Left = 96
    Top = 136
    Width = 63
    Height = 21
    EditMask = '99/9999'
    MaxLength = 7
    TabOrder = 4
    Text = '  /    '
  end
  object MaskEdit3: TMaskEdit [11]
    Tag = 1
    Left = 96
    Top = 40
    Width = 60
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = MaskEdit3Exit
  end
  object MaskEdit4: TMaskEdit [12]
    Left = 96
    Top = 88
    Width = 60
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object MaskEdit6: TMaskEdit [13]
    Left = 96
    Top = 112
    Width = 60
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object RadioGroup1: TRadioGroup [14]
    Left = 96
    Top = 168
    Width = 321
    Height = 49
    Caption = 'Modo Exibi'#231#227'o'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Valores moeda corrente'
      'Quantidade de produtos')
    TabOrder = 5
    Visible = False
  end
  object DBEdit1: TDBEdit [15]
    Tag = 1
    Left = 160
    Top = 88
    Width = 530
    Height = 21
    DataField = 'cNmProduto'
    DataSource = DataSource6
    TabOrder = 9
  end
  object DBEdit4: TDBEdit [16]
    Tag = 1
    Left = 160
    Top = 112
    Width = 529
    Height = 21
    DataField = 'cNmLinha'
    DataSource = DataSource7
    TabOrder = 10
  end
  object MaskEdit9: TMaskEdit [17]
    Tag = 1
    Left = 96
    Top = 64
    Width = 60
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
  end
  object DBEdit8: TDBEdit [18]
    Tag = 1
    Left = 160
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource10
    TabOrder = 11
  end
  inherited ImageList1: TImageList
    Left = 832
    Top = 80
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 496
    Top = 112
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM UNIDADENEGOCIO'
      ' WHERE nCdUnidadeNegocio = :nPK')
    Left = 544
    Top = 112
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 304
  end
  object DataSource2: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 536
    Top = 304
  end
  object DataSource3: TDataSource
    Left = 576
    Top = 352
  end
  object DataSource4: TDataSource
    Left = 648
    Top = 352
  end
  object DataSource5: TDataSource
    Left = 704
    Top = 336
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto, cNmProduto'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 736
    Top = 96
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object DataSource6: TDataSource
    DataSet = qryProduto
    Left = 632
    Top = 312
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha'
      'WHERE nCdLinha = :nPK')
    Left = 592
    Top = 160
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object DataSource7: TDataSource
    DataSet = qryLinha
    Left = 640
    Top = 320
  end
  object qryRegiao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdRegiao, cNmRegiao FROM Regiao WHERE nCdRegiao = :nPK')
    Left = 720
    Top = 200
    object qryRegiaonCdRegiao: TIntegerField
      FieldName = 'nCdRegiao'
    end
    object qryRegiaocNmRegiao: TStringField
      FieldName = 'cNmRegiao'
      Size = 50
    end
  end
  object DataSource8: TDataSource
    DataSet = qryRegiao
    Left = 648
    Top = 328
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 808
    Top = 272
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoEconomico, cNmGrupoEconomico'
      'FROM GrupoEconomico'
      'WHERE nCdGrupoEconomico = :nPK')
    Left = 792
    Top = 192
    object qryGrupoEconomiconCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object DataSource9: TDataSource
    DataSet = qryGrupoEconomico
    Left = 656
    Top = 336
  end
  object DataSource10: TDataSource
    DataSet = qryTerceiro
    Left = 664
    Top = 344
  end
end
