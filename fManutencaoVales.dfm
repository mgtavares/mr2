inherited frmManutencaoVales: TfrmManutencaoVales
  Left = 215
  Top = 77
  Width = 969
  Caption = 'Manuten'#231#227'o de Vales'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 953
  end
  object Label1: TLabel [1]
    Left = 31
    Top = 40
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Vale'
  end
  object Label2: TLabel [2]
    Left = 225
    Top = 64
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor do vale'
  end
  object Label3: TLabel [3]
    Left = 43
    Top = 160
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo Vale'
  end
  object Label4: TLabel [4]
    Left = 227
    Top = 88
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja Emiss'#227'o'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 736
    Top = 160
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Estorno'
    FocusControl = DBEdit5
  end
  object Label7: TLabel [6]
    Left = 34
    Top = 64
    Width = 63
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Vale'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [7]
    Left = 735
    Top = 88
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Emiss'#227'o'
    FocusControl = DBEdit8
  end
  object Label6: TLabel [8]
    Left = 749
    Top = 112
    Width = 54
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Baixa'
    FocusControl = DBEdit6
  end
  object Label9: TLabel [9]
    Left = 19
    Top = 88
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Lancto Emiss'#227'o'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [10]
    Left = 454
    Top = 88
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Caixa Emiss'#227'o'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [11]
    Left = 33
    Top = 112
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Lancto Baixa'
    FocusControl = DBEdit11
  end
  object Label12: TLabel [12]
    Left = 468
    Top = 112
    Width = 57
    Height = 13
    Alignment = taRightJustify
    Caption = 'Caixa Baixa'
    FocusControl = DBEdit12
  end
  object Label13: TLabel [13]
    Left = 241
    Top = 112
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja Baixa'
    FocusControl = DBEdit13
  end
  object Label14: TLabel [14]
    Left = 212
    Top = 40
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo do Vale'
  end
  object Label15: TLabel [15]
    Left = 9
    Top = 136
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Lancto Recompra'
    FocusControl = DBEdit15
  end
  object Label16: TLabel [16]
    Left = 217
    Top = 136
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja Recompra'
    FocusControl = DBEdit16
  end
  object Label17: TLabel [17]
    Left = 444
    Top = 136
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Caixa Recompra'
    FocusControl = DBEdit17
  end
  object Label18: TLabel [18]
    Left = 725
    Top = 136
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Recompra'
    FocusControl = DBEdit18
  end
  inherited ToolBar2: TToolBar
    Width = 953
    inherited btIncluir: TToolButton
      Enabled = False
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
  end
  object DBEdit4: TDBEdit [20]
    Tag = 1
    Left = 295
    Top = 82
    Width = 143
    Height = 19
    DataField = 'cNmLoja'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit5: TDBEdit [21]
    Tag = 1
    Left = 807
    Top = 154
    Width = 137
    Height = 19
    DataField = 'dDtEstorno'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit7: TDBEdit [22]
    Tag = 1
    Left = 100
    Top = 58
    Width = 113
    Height = 19
    DataField = 'cNmTabTipoVale'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit8: TDBEdit [23]
    Tag = 1
    Left = 807
    Top = 82
    Width = 137
    Height = 19
    DataField = 'dDtVale'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBCheckBox1: TDBCheckBox [24]
    Left = 295
    Top = 156
    Width = 297
    Height = 17
    Caption = 'Permitir Liquidar Venda/Abater Parcela Credi'#225'rio'
    DataField = 'cFlgLiqVenda'
    DataSource = dsMaster
    TabOrder = 2
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBEdit6: TDBEdit [25]
    Tag = 1
    Left = 807
    Top = 106
    Width = 137
    Height = 19
    DataField = 'dDtBaixa'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit2: TDBEdit [26]
    Tag = 1
    Left = 295
    Top = 58
    Width = 143
    Height = 19
    DataField = 'nValVale'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit3: TDBEdit [27]
    Left = 100
    Top = 154
    Width = 113
    Height = 19
    DataField = 'nSaldoVale'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit9: TDBEdit [28]
    Tag = 1
    Left = 100
    Top = 82
    Width = 113
    Height = 19
    DataField = 'nCdLanctoFin'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit10: TDBEdit [29]
    Tag = 1
    Left = 527
    Top = 82
    Width = 195
    Height = 19
    DataField = 'nCdConta'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit11: TDBEdit [30]
    Tag = 1
    Left = 100
    Top = 106
    Width = 113
    Height = 19
    DataField = 'nCdLanctoFinBaixa'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit12: TDBEdit [31]
    Tag = 1
    Left = 527
    Top = 106
    Width = 195
    Height = 19
    DataField = 'nCdContaBaixa'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit13: TDBEdit [32]
    Tag = 1
    Left = 295
    Top = 106
    Width = 143
    Height = 19
    DataField = 'cNmLojaBaixa'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit15: TDBEdit [33]
    Tag = 1
    Left = 100
    Top = 130
    Width = 113
    Height = 19
    DataField = 'nCdLanctoFinRecompra'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit16: TDBEdit [34]
    Tag = 1
    Left = 295
    Top = 130
    Width = 143
    Height = 19
    DataField = 'cNmLojaRecompra'
    DataSource = dsMaster
    TabOrder = 15
  end
  object DBEdit17: TDBEdit [35]
    Tag = 1
    Left = 527
    Top = 130
    Width = 195
    Height = 19
    DataField = 'nCdContaRecompra'
    DataSource = dsMaster
    TabOrder = 16
  end
  object DBEdit18: TDBEdit [36]
    Tag = 1
    Left = 807
    Top = 130
    Width = 137
    Height = 19
    DataField = 'dDtRecompra'
    DataSource = dsMaster
    TabOrder = 17
  end
  object DBEdit1: TDBEdit [37]
    Tag = 1
    Left = 100
    Top = 34
    Width = 65
    Height = 19
    DataField = 'nCdVale'
    DataSource = dsMaster
    TabOrder = 18
  end
  object DBEdit14: TDBEdit [38]
    Tag = 1
    Left = 295
    Top = 34
    Width = 390
    Height = 19
    DataField = 'cCodigo'
    DataSource = dsMaster
    TabOrder = 19
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Vale.nCdVale'
      '      ,Vale.cCodigo'
      '      ,TTV.cNmTabTipoVale'
      
        '      ,dbo.fn_ZeroEsquerda(Loja.nCdLoja,3) + '#39' - '#39' + Loja.cNmLoj' +
        'a cNmLoja'
      '      ,Vale.dDtVale'
      '      ,Vale.nValVale'
      '      ,Vale.nSaldoVale'
      '      ,Vale.dDtBaixa'
      '      ,Vale.dDtEstorno'
      '      ,Vale.cFlgLiqVenda'
      '      ,CB.nCdConta'
      '      ,Vale.nCdLanctoFin'
      '      ,Vale.nCdLanctoFinBaixa'
      '      ,CBB.nCdConta nCdContaBaixa'
      
        '      ,dbo.fn_ZeroEsquerda(LJB.nCdLoja,3) + '#39' - '#39' + LJB.cNmLoja ' +
        'cNmLojaBaixa'
      '      ,Vale.nCdLanctoFinRecompra'
      '      ,CBR.nCdConta nCdContaRecompra'
      
        '      ,dbo.fn_ZeroEsquerda(LJR.nCdLoja,3) + '#39' - '#39' + LJR.cNmLoja ' +
        'cNmLojaRecompra'
      '      ,dDtRecompra'
      '  FROM Vale'
      
        '       INNER JOIN TabTipoVale TTV   ON TTV.nCdTabTipoVale   = Va' +
        'le.nCdTabTipoVale'
      
        '       INNER JOIN Loja              ON Loja.nCdLoja         = Va' +
        'le.nCdLoja'
      
        '       LEFT  JOIN LanctoFin     LF  ON LF.nCdLanctoFin      = Va' +
        'le.nCdLanctoFin'
      
        '       LEFT  JOIN ContaBancaria CB  ON CB.nCdContaBancaria  = LF' +
        '.nCdContaBancaria'
      
        '       LEFT  JOIN LanctoFin     LFB ON LFB.nCdLanctoFin     = Va' +
        'le.nCdLanctoFinBaixa'
      
        '       LEFT  JOIN ContaBancaria CBB ON CBB.nCdContaBancaria = LF' +
        'B.nCdContaBancaria'
      
        '       LEFT  JOIN Loja          LJB ON LJB.nCdLoja          = CB' +
        'B.nCdLoja'
      
        '       LEFT  JOIN LanctoFIn     LFR ON LFR.nCdLanctoFin     = Va' +
        'le.nCdLanctoFinRecompra'
      
        '       LEFT  JOIN ContaBancaria CBR ON CBR.nCdContaBancaria = LF' +
        'R.nCdContaBancaria'
      
        '       LEFT  JOIN Loja          LJR ON LJR.nCdLoja          = CB' +
        'R.nCdLoja'
      ' WHERE nCdVale =:nPK')
    object qryMasternCdVale: TAutoIncField
      FieldName = 'nCdVale'
      ReadOnly = True
    end
    object qryMastercCodigo: TStringField
      FieldName = 'cCodigo'
      Size = 30
    end
    object qryMastercNmTabTipoVale: TStringField
      FieldName = 'cNmTabTipoVale'
      Size = 50
    end
    object qryMastercNmLoja: TStringField
      FieldName = 'cNmLoja'
      ReadOnly = True
      Size = 68
    end
    object qryMasterdDtVale: TDateTimeField
      FieldName = 'dDtVale'
    end
    object qryMasternValVale: TBCDField
      FieldName = 'nValVale'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternSaldoVale: TBCDField
      FieldName = 'nSaldoVale'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtBaixa: TDateTimeField
      FieldName = 'dDtBaixa'
    end
    object qryMasterdDtEstorno: TDateTimeField
      FieldName = 'dDtEstorno'
    end
    object qryMastercFlgLiqVenda: TIntegerField
      FieldName = 'cFlgLiqVenda'
    end
    object qryMasternCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryMasternCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMasternCdLanctoFinBaixa: TIntegerField
      FieldName = 'nCdLanctoFinBaixa'
    end
    object qryMasternCdContaBaixa: TStringField
      FieldName = 'nCdContaBaixa'
      FixedChar = True
      Size = 15
    end
    object qryMastercNmLojaBaixa: TStringField
      FieldName = 'cNmLojaBaixa'
      ReadOnly = True
      Size = 68
    end
    object qryMasternCdLanctoFinRecompra: TIntegerField
      FieldName = 'nCdLanctoFinRecompra'
    end
    object qryMasternCdContaRecompra: TStringField
      FieldName = 'nCdContaRecompra'
      FixedChar = True
      Size = 15
    end
    object qryMastercNmLojaRecompra: TStringField
      FieldName = 'cNmLojaRecompra'
      ReadOnly = True
      Size = 68
    end
    object qryMasterdDtRecompra: TDateTimeField
      FieldName = 'dDtRecompra'
    end
  end
end
