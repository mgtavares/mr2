unit fRecebimento_Divergencias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  GridsEh, DBGridEh, ADODB, DBGridEhGrouping;

type
  TfrmRecebimento_Divergencias = class(TfrmProcesso_Padrao)
    qryDivergencias: TADOQuery;
    qryDivergenciasnCdTabTipoDiverg: TIntegerField;
    qryDivergenciascNmTabTipoDiverg: TStringField;
    DBGridEh1: TDBGridEh;
    dsDivergencias: TDataSource;
    qryDivergenciascNmUsuario: TStringField;
    qryDivergenciasdDtautorizacao: TDateTimeField;
    qryDivergenciascFlgAutorizAutomatica: TIntegerField;
    qryDivergenciascOBS: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRecebimento_Divergencias: TfrmRecebimento_Divergencias;

implementation

{$R *.dfm}

end.
