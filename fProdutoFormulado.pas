unit fProdutoFormulado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons, cxPC, cxControls;

type
  TfrmProdutoFormulado = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    dsDepartamento: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TAutoIncField;
    qryDepartamentocNmDepartamento: TStringField;
    qryDepartamentonCdStatus: TIntegerField;
    qryCategoria: TADOQuery;
    qryCategorianCdCategoria: TAutoIncField;
    qryCategorianCdDepartamento: TIntegerField;
    qryCategoriacNmCategoria: TStringField;
    qryCategorianCdStatus: TIntegerField;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DataSource1: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategorianCdCategoria: TIntegerField;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySubCategorianCdStatus: TIntegerField;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DataSource2: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentonCdSubCategoria: TIntegerField;
    qrySegmentocNmSegmento: TStringField;
    qrySegmentonCdStatus: TIntegerField;
    DBEdit9: TDBEdit;
    DataSource3: TDataSource;
    Label11: TLabel;
    DBEdit17: TDBEdit;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    DataSource9: TDataSource;
    qryMasternCdProduto: TIntegerField;
    qryMasternCdDepartamento: TIntegerField;
    qryMasternCdCategoria: TIntegerField;
    qryMasternCdSubCategoria: TIntegerField;
    qryMasternCdSegmento: TIntegerField;
    qryMasternCdMarca: TIntegerField;
    qryMasternCdLinha: TIntegerField;
    qryMastercReferencia: TStringField;
    qryMasternCdTabTipoProduto: TIntegerField;
    qryMasternCdProdutoPai: TIntegerField;
    qryMasternCdGrade: TIntegerField;
    qryMasternCdColecao: TIntegerField;
    qryMastercNmProduto: TStringField;
    qryMasternCdClasseProduto: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMasternCdCor: TIntegerField;
    qryMasternCdMaterial: TIntegerField;
    qryMasteriTamanho: TIntegerField;
    qryMasternQtdeEstoque: TBCDField;
    qryMasternValCusto: TBCDField;
    qryMasternValVenda: TBCDField;
    qryMastercEAN: TStringField;
    qryMastercEANFornec: TStringField;
    qryMasterdDtUltReceb: TDateTimeField;
    qryMasterdDtUltVenda: TDateTimeField;
    cxPageControl1: TcxPageControl;
    TabItens: TcxTabSheet;
    TabConfiguracao: TcxTabSheet;
    StaticText2: TStaticText;
    TabPermissao: TcxTabSheet;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryItemMovimentar: TADOQuery;
    qryItemMovimentarnCdFormulaProduto: TAutoIncField;
    qryItemMovimentarnCdProdutoPai: TIntegerField;
    qryItemMovimentarnCdProduto: TIntegerField;
    qryItemMovimentarnQtde: TBCDField;
    qryItemMovimentarcNmProduto: TStringField;
    dsItemMovimentar: TDataSource;
    DBGridEh1: TDBGridEh;
    qryFormulaColuna: TADOQuery;
    qryFormulaColunanCdFormulaColuna: TAutoIncField;
    qryFormulaColunanCdProdutoPai: TIntegerField;
    qryFormulaColunaiColuna: TIntegerField;
    qryFormulaColunanCdDepartamento: TIntegerField;
    qryFormulaColunanQtdeProduto: TBCDField;
    qryFormulaColunanQtdeAmostra: TBCDField;
    qryFormulaColunacNmDepartamento: TStringField;
    dsFormulaColuna: TDataSource;
    DBGridEh2: TDBGridEh;
    qryDepartamentoAux: TADOQuery;
    qryDepartamentoAuxnCdDepartamento: TIntegerField;
    qryDepartamentoAuxcNmDepartamento: TStringField;
    cxPageControl2: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    DBGridEh4: TDBGridEh;
    qryProdutoColuna: TADOQuery;
    dsProdutoColuna: TDataSource;
    qryProdutoColunanCdFormulaColunaProduto: TAutoIncField;
    qryProdutoColunanCdFormulaColuna: TIntegerField;
    qryProdutoColunacFlgProdAmostra: TStringField;
    qryProdutoColunanCdProduto: TIntegerField;
    qryProdutoColunacApelido: TStringField;
    qryAmostraColuna: TADOQuery;
    dsAmostraColuna: TDataSource;
    qryAmostraColunanCdFormulaColunaProduto: TAutoIncField;
    qryAmostraColunanCdFormulaColuna: TIntegerField;
    qryAmostraColunacFlgProdAmostra: TStringField;
    qryAmostraColunanCdProduto: TIntegerField;
    qryAmostraColunacApelido: TStringField;
    qryProdutoColunacNmProduto: TStringField;
    qryAmostraColunacNmProduto: TStringField;
    qryFormulaTipoPedido: TADOQuery;
    dsFormulaTipoPedido: TDataSource;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    DBGridEh5: TDBGridEh;
    StaticText1: TStaticText;
    qryFormulaTipoPedidonCdProdutoPai: TIntegerField;
    qryFormulaTipoPedidonCdTipoPedido: TIntegerField;
    qryFormulaTipoPedidocNmTipoPedido: TStringField;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    qryMarcanCdStatus: TIntegerField;
    dsMarca: TDataSource;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label3: TLabel;
    qryMastercFlgProdVenda: TIntegerField;
    DBCheckBox1: TDBCheckBox;
    cxTabSheet3: TcxTabSheet;
    Image5: TImage;
    Label30: TLabel;
    DBEdit41: TDBEdit;
    Label31: TLabel;
    DBEdit42: TDBEdit;
    Label32: TLabel;
    DBEdit43: TDBEdit;
    Label33: TLabel;
    DBEdit44: TDBEdit;
    Label34: TLabel;
    DBEdit45: TDBEdit;
    Label35: TLabel;
    DBEdit46: TDBEdit;
    Label36: TLabel;
    DBEdit47: TDBEdit;
    Label37: TLabel;
    DBEdit48: TDBEdit;
    Label38: TLabel;
    qryMasternPesoBruto: TBCDField;
    qryMasternPesoLiquido: TBCDField;
    qryMasternAltura: TBCDField;
    qryMasternLargura: TBCDField;
    qryMasternDimensao: TBCDField;
    qryMasternVolume: TBCDField;
    qryMastercUMVolume: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit21Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit15KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit16KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit21KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryItemMovimentarBeforePost(DataSet: TDataSet);
    procedure qryItemMovimentarCalcFields(DataSet: TDataSet);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure DBEdit17Exit(Sender: TObject);
    procedure qryFormulaColunaBeforePost(DataSet: TDataSet);
    procedure qryFormulaColunaCalcFields(DataSet: TDataSet);
    procedure DBGridEh2ColExit(Sender: TObject);
    procedure qryProdutoColunaBeforePost(DataSet: TDataSet);
    procedure qryAmostraColunaBeforePost(DataSet: TDataSet);
    procedure qryFormulaColunaAfterScroll(DataSet: TDataSet);
    procedure TabConfiguracaoEnter(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure DBGridEh3ColExit(Sender: TObject);
    procedure DBGridEh4ColExit(Sender: TObject);
    procedure qryAmostraColunaCalcFields(DataSet: TDataSet);
    procedure qryProdutoColunaCalcFields(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh4KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh5KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryFormulaTipoPedidoBeforePost(DataSet: TDataSet);
    procedure DBEdit11Exit(Sender: TObject);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProdutoFormulado: TfrmProdutoFormulado;

implementation

uses fGerarGrade, fLookup_Padrao, fIncLinha;

{$R *.dfm}

procedure TfrmProdutoFormulado.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PRODUTO' ;
  nCdTabelaSistema  := 25 ;
  nCdConsultaPadrao := 70 ;
  bLimpaAposSalvar  := False ;
end;

procedure TfrmProdutoFormulado.DBEdit2Exit(Sender: TObject);
begin
  PosicionaQuery(qryDepartamento, DBEdit2.Text) ;

  if (qryDepartamento.Active) then
    qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TfrmProdutoFormulado.PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
begin
    if (Trim(cValor) = '') then
        exit ;

    oQuery.Close ;
    oQuery.Parameters.ParamByName('nPK').Value := cValor ;
    oQuery.Open ;
end ;

procedure TfrmProdutoFormulado.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCategoria, DBEdit4.Text) ;

  if (qryCategoria.Active) then
      qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

end;

procedure TfrmProdutoFormulado.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qrySubCategoria, DBEdit6.Text) ;

  if (qrySubCategoria.Active) then
  begin
    qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  end;

end;

procedure TfrmProdutoFormulado.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qrySegmento, DBEdit7.Text) ;
end;

procedure TfrmProdutoFormulado.DBEdit21Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
  begin

    case MessageDlg('Confirma a inclus�o deste produto ?',mtConfirmation,[mbYes,mbNo],0) of
        mrYes: btSalvar.Click;
    end;

  end ;

end;

procedure TfrmProdutoFormulado.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;

  cxPageControl1.ActivePageIndex := 0 ;

  PosicionaQuery(qryDepartamento, qryMasternCdDepartamento.asString) ;

  qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;


  PosicionaQuery(qryCategoria, qryMasternCdCategoria.asString) ;
  qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryMasternCdCategoria.Value ;

  PosicionaQuery(qrySubCategoria, qryMasternCdSubCategoria.asString) ;
  qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;

  PosicionaQuery(qrySegmento, qryMasternCdSegmento.asString) ;
  PosicionaQuery(qryMarca, qryMasternCdMarca.asString) ;

  PosicionaQuery(qryItemMovimentar, qryMasternCdProduto.asString) ;
  PosicionaQuery(qryFormulaColuna, qryMasternCdProduto.asString) ;
  PosicionaQuery(qryFormulaTipoPedido, qryMasternCdProduto.asString) ;

end;

procedure TfrmProdutoFormulado.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryDepartamento.Close ;
  qryCategoria.Close ;
  qrySubCategoria.Close ;
  qrySegmento.Close ;
  qryMarca.Close ;
  qryFormulaColuna.Close ;
  qryItemMovimentar.Close ;
  qryFormulaTipoPedido.Close ;
  qryAmostraColuna.Close ;
  qryProdutoColuna.Close ;

end;

procedure TfrmProdutoFormulado.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            nPK := frmLookup_Padrao.ExecutaConsulta(45);
            If (nPK > 0) then
            begin
                qryMasternCdDepartamento.Value := nPK ;
                PosicionaQuery(qryDepartamento, qryMasternCdDepartamento.asString) ;
                qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

                // consulta de categoria
                nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);
                If (nPK > 0) then
                begin
                    qryMasternCdCategoria.Value := nPK ;
                    PosicionaQuery(qryCategoria, qryMasternCdCategoria.asString) ;
                    qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

                    // consulta de subcategoria
                    nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
                    If (nPK > 0) then
                    begin
                        qryMasternCdSubCategoria.Value := nPK ;
                        PosicionaQuery(qrySubCategoria,qryMasternCdSubCategoria.asString) ;

                        qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;

                        // Consulta de segmento
                        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
                        If (nPK > 0) then
                        begin
                            qryMasternCdSegmento.Value := nPK ;
                            PosicionaQuery(qrySegmento,qryMasternCdSegmento.AsString) ;

                        end ;



                    end ;

                end ;

            end ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoFormulado.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryDepartamento.active or (qryDepartamentonCdDepartamento.Value = 0) then
            begin
                ShowMessage('Selecione um departamento.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);
            If (nPK > 0) then
                qryMasternCdCategoria.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoFormulado.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryCategoria.active or (qryCategorianCdCategoria.Value = 0) then
            begin
                ShowMessage('Selecione uma Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
            If (nPK > 0) then
                qryMasternCdSubCategoria.Value := nPK ;
        end ;
    end ;
  end ;
end;

procedure TfrmProdutoFormulado.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qrySubCategoria.active or (qrySubCategorianCdCategoria.Value = 0) then
            begin
                ShowMessage('Selecione uma Sub Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
            If (nPK > 0) then
                qryMasternCdSegmento.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoFormulado.DBEdit10KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if not qrySubCategoria.active or (qrySubCategorianCdSubCategoria.Value = 0) then
            begin
                ShowMessage('Selecione uma Sub Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(49,'nCdStatus = 1 AND EXISTS(SELECT 1 FROM MarcaSubcategoria MSC WHERE MSC.nCdMarca = Marca.nCdMarca AND MSC.nCdSubCategoria = ' + Chr(39) + qrySubCategorianCdSubCategoria.asString + Chr(39) + ')');
            If (nPK > 0) then
                qryMasternCdMarca.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoFormulado.DBEdit15KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(51);
            If (nPK > 0) then
                qryMasternCdGrade.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoFormulado.DBEdit16KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(52);
            If (nPK > 0) then
                qryMasternCdColecao.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoFormulado.DBEdit20KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(53);
            If (nPK > 0) then
                qryMasternCdClasseProduto.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoFormulado.DBEdit21KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);
            If (nPK > 0) then
                qryMasternCdStatus.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoFormulado.btIncluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;
  DbEdit2.SetFocus ;
end;

procedure TfrmProdutoFormulado.qryMasterBeforePost(DataSet: TDataSet);
begin

  If (dbEdit2.Text = '') or (dbEdit3.Text = '') then
  begin
      ShowMessage('Informe o departamento.') ;
      dbEdit2.SetFocus ;
      Abort ;
  end ;

  If (dbEdit4.Text = '') or (dbEdit5.Text = '') then
  begin
      ShowMessage('Informe a categoria.') ;
      dbEdit4.SetFocus ;
      Abort ;
  end ;

  If (dbEdit6.Text = '') or (dbEdit8.Text = '') then
  begin
      ShowMessage('Informe a Sub Categoria.') ;
      dbEdit6.SetFocus ;
      Abort ;
  end ;

  If (dbEdit7.Text = '') or (dbEdit9.Text = '') then
  begin
      ShowMessage('Informe o Segmento.') ;
      dbEdit7.SetFocus ;
      Abort ;
  end ;

  If (dbEdit11.Text = '') or (dbEdit10.Text = '') then
  begin
      ShowMessage('Informe a marca.') ;
      dbEdit11.SetFocus ;
      Abort ;
  end ;

  If (dbEdit17.Text = '') then
  begin
      ShowMessage('Informe a Descri��o.') ;
      dbEdit17.SetFocus ;
      Abort ;
  end ;

  inherited;

end;

procedure TfrmProdutoFormulado.qryItemMovimentarBeforePost(
  DataSet: TDataSet);
begin

  if (qryItemMovimentarnCdProduto.Value = 0) or (qryItemMovimentarcNmProduto.Value = '') then
  begin
      ShowMessage('Informe o produto a movimentar.') ;
      abort ;
  end ;

  if (qryItemMovimentarnQtde.Value <= 0) then
  begin
      ShowMessage('Informe a quantidade a movimentar.') ;
      abort ;
  end ;

  qryItemMovimentarnCdProdutoPai.Value := qryMasternCdProduto.Value ;


  inherited;
end;

procedure TfrmProdutoFormulado.qryItemMovimentarCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryProduto, qryItemMovimentarnCdProduto.AsString) ;

  if not qryProduto.eof then
      qryItemMovimentarcNmProduto.Value := qryProdutocNmProduto.Value ;


end;

procedure TfrmProdutoFormulado.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  if (DBGridEh1.Col = 3) and (qryItemMovimentar.State <> dsBrowse) then
  begin

    PosicionaQuery(qryProduto, qryItemMovimentarnCdProduto.AsString) ;

    qryItemMovimentarcNmProduto.Value := qryProdutocNmProduto.Value ;

  end ;
  
end;

procedure TfrmProdutoFormulado.DBEdit17Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
  begin
      qryMastercReferencia.Value       := 'FORMULA' ;
      qryMasternCdTabTipoProduto.Value := 4 ;
      qryMasternCdStatus.Value         := 1 ;

      qryMaster.Post ;
  end ;

end;

procedure TfrmProdutoFormulado.qryFormulaColunaBeforePost(
  DataSet: TDataSet);
begin

  if (qryFormulaColunaiColuna.Value <= 0) then
  begin
      ShowMessage('Informe a coluna da caixa.') ;
      abort ;
  end ;

  if (qryFormulaColunanCdDepartamento.Value = 0) or (qryFormulaColunacNmDepartamento.Value = '') then
  begin
      ShowMessage('Informe o departamento de produtos.') ;
      abort ;
  end ;

  if (qryFormulaColunanQtdeProduto.Value < 0) then
  begin
      ShowMessage('Quantidade de Produtos Inv�lida.') ;
      abort ;
  end ;

  if (qryFormulaColunanQtdeAmostra.Value < 0) then
  begin
      ShowMessage('Quantidade de Amostras Inv�lida.') ;
      abort ;
  end ;

  if (qryFormulaColunanQtdeAmostra.Value+qryFormulaColunanQtdeProduto.Value <= 0) then
  begin
      ShowMessage('Informe a quantidade de produtos e/ou amostras.') ;
      abort ;
  end ;

  qryFormulaColunanCdProdutoPai.Value := qryMasternCdProduto.Value ;

  inherited;

end;

procedure TfrmProdutoFormulado.qryFormulaColunaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryDepartamentoaux.Close ;
  
  PosicionaQuery(qryDepartamentoAux, qryFormulaColunanCdDepartamento.AsString) ;

  if not qryDepartamentoAux.eof then
      qryFormulaColunacNmDepartamento.Value := qryDepartamentoAuxcNmDepartamento.Value ;

end;

procedure TfrmProdutoFormulado.DBGridEh2ColExit(Sender: TObject);
begin
  inherited;

  if (DBGridEh2.Col = 4) and (qryFormulaColuna.State <> dsBrowse) then
  begin

    PosicionaQuery(qryDepartamentoAux, qryFormulaColunanCdDepartamento.AsString) ;

    if not qryDepartamentoAux.eof then
        qryFormulaColunacNmDepartamento.Value := qryDepartamentoAuxcNmDepartamento.Value ;

  end ;
  
end;

procedure TfrmProdutoFormulado.qryProdutoColunaBeforePost(
  DataSet: TDataSet);
begin

  if (qryProdutoColunanCdProduto.Value = 0) or (qryProdutoColunacNmproduto.Value = '') then
  begin
      ShowMessage('Informe o produto.') ;
      abort ;
  end ;

  if (qryProdutoColunacApelido.Value = '') then
  begin
      ShowMessage('Informe o apelido do produto.') ;
      abort ;
  end ;

  qryProdutoColunacApelido.Value         := Uppercase(qryProdutoColunacApelido.Value) ;
  qryProdutoColunanCdFormulaColuna.Value := qryFormulaColunanCdFormulaColuna.Value ;
  qryProdutoColunacFlgProdAmostra.Value  := 'P' ;

  inherited;


end;

procedure TfrmProdutoFormulado.qryAmostraColunaBeforePost(
  DataSet: TDataSet);
begin
  if (qryAmostraColunanCdProduto.Value = 0) or (qryAmostraColunacNmproduto.Value = '') then
  begin
      ShowMessage('Informe o produto.') ;
      abort ;
  end ;

  if (qryAmostraColunacApelido.Value = '') then
  begin
      ShowMessage('Informe o apelido do produto.') ;
      abort ;
  end ;

  qryAmostraColunacApelido.Value         := Uppercase(qryAmostraColunacApelido.Value) ;
  qryAmostraColunanCdFormulaColuna.Value := qryFormulaColunanCdFormulaColuna.Value ;
  qryAmostraColunacFlgProdAmostra.Value  := 'A' ;

  inherited;

end;

procedure TfrmProdutoFormulado.qryFormulaColunaAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryProdutoColuna, qryFormulaColunanCdFormulaColuna.AsString) ;
  PosicionaQuery(qryAmostraColuna, qryFormulaColunanCdFormulaColuna.AsString) ;

end;

procedure TfrmProdutoFormulado.TabConfiguracaoEnter(Sender: TObject);
begin
  inherited;
  cxPageControl2.ActivePageIndex := 0 ;

end;

procedure TfrmProdutoFormulado.btSalvarClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmProdutoFormulado.DBGridEh3ColExit(Sender: TObject);
begin
  inherited;
  if (DBGridEh3.Col = 3) then
  begin

    PosicionaQuery(qryProduto, qryProdutoColunanCdProduto.AsString) ;

    qryProdutoColunacNmProduto.Value := qryProdutocNmProduto.Value ;

  end ;
end;

procedure TfrmProdutoFormulado.DBGridEh4ColExit(Sender: TObject);
begin
  inherited;
  if (DBGridEh3.Col = 3) then
  begin

    PosicionaQuery(qryProduto, qryAmostraColunanCdProduto.AsString) ;

    qryAmostraColunacNmProduto.Value := qryProdutocNmProduto.Value ;

  end ;
end;

procedure TfrmProdutoFormulado.qryAmostraColunaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryProduto, qryAmostraColunanCdProduto.AsString) ;

  if (qryAmostraColuna.State <> dsBrowse) then
      if not qryProduto.eof then
          qryAmostraColunacNmProduto.Value := qryProdutocNmProduto.Value ;

end;

procedure TfrmProdutoFormulado.qryProdutoColunaCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryProduto, qryProdutoColunanCdProduto.AsString) ;

  if (qryProdutoColuna.State <> dsBrowse) then
      if not qryProduto.eof then
          qryProdutoColunacNmProduto.Value := qryProdutocNmProduto.Value ;

end;

procedure TfrmProdutoFormulado.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemMovimentar.State = dsBrowse) then
             qryItemMovimentar.Edit ;

        if (qryItemMovimentar.State = dsInsert) or (qryItemMovimentar.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(42);

            If (nPK > 0) then
            begin
                qryItemMovimentarnCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmProdutoFormulado.DBGridEh3KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryProdutoColuna.State = dsBrowse) then
             qryProdutoColuna.Edit ;

        if (qryProdutoColuna.State = dsInsert) or (qryProdutoColuna.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(76,'nCdDepartamento = ' + qryFormulaColunanCdDepartamento.asString);

            If (nPK > 0) then
            begin
                qryProdutoColunanCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmProdutoFormulado.DBGridEh4KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryAmostraColuna.State = dsBrowse) then
             qryAmostraColuna.Edit ;

        if (qryAmostraColuna.State = dsInsert) or (qryAmostraColuna.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(76,'nCdDepartamento = ' + qryFormulaColunanCdDepartamento.asString);

            If (nPK > 0) then
            begin
                qryAmostraColunanCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmProdutoFormulado.DBGridEh2KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryFormulaColuna.State = dsBrowse) then
             qryFormulaColuna.Edit ;

        if (qryFormulaColuna.State = dsInsert) or (qryFormulaColuna.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(45);

            If (nPK > 0) then
            begin
                qryFormulaColunanCdDepartamento.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmProdutoFormulado.DBGridEh5KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryFormulaTipoPedido.State = dsBrowse) then
             qryFormulaTipoPedido.Edit ;


        if (qryFormulaTipoPedido.State = dsInsert) or (qryFormulaTipoPedido.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(71);

            If (nPK > 0) then
            begin
                qryFormulaTipoPedidonCdTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmProdutoFormulado.qryFormulaTipoPedidoBeforePost(
  DataSet: TDataSet);
begin

  if (qryFormulaTipoPedidonCdTipoPedido.Value = 0) or (qryFormulaTipoPedidocNmTipoPedido.Value = '') then
  begin
      ShowMessage('Informe o tipo de pedido.') ;
      abort ;
  end ;

  qryFormulaTipoPedidonCdProdutoPai.Value := qryMasternCdProduto.Value ;

  inherited;

end;

procedure TfrmProdutoFormulado.DBEdit11Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryMarca, DBEdit11.Text) ;

end;

procedure TfrmProdutoFormulado.DBEdit11KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if not qrySubCategoria.active or (qrySubCategorianCdSubCategoria.Value = 0) then
            begin
                ShowMessage('Selecione uma Sub Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(49,'nCdStatus = 1 AND EXISTS(SELECT 1 FROM MarcaSubcategoria MSC WHERE MSC.nCdMarca = Marca.nCdMarca AND MSC.nCdSubCategoria = ' + Chr(39) + qrySubCategorianCdSubCategoria.asString + Chr(39) + ')');
            If (nPK > 0) then
                qryMasternCdMarca.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoFormulado.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryItemMovimentar, qryMasternCdProduto.asString) ;
  PosicionaQuery(qryFormulaColuna, qryMasternCdProduto.asString) ;
  PosicionaQuery(qryFormulaTipoPedido, qryMasternCdProduto.asString) ;

end;

initialization
    RegisterClass(TfrmProdutoFormulado) ;

end.
