unit fTabRenegociacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh;

type
  TfrmTabRenegociacao = class(TfrmProcesso_Padrao)
    qryTabRenegociacao: TADOQuery;
    qryTabRenegociacaonCdTabRenegociacao: TAutoIncField;
    qryTabRenegociacaonCdEmpresa: TIntegerField;
    qryTabRenegociacaoiDiasAtraso: TIntegerField;
    qryTabRenegociacaonPercDescPrincVista: TBCDField;
    qryTabRenegociacaoiParcLimDescPrinc: TIntegerField;
    qryTabRenegociacaonPercDescPrincParc: TBCDField;
    qryTabRenegociacaonPercDescPrincMaiorParc: TBCDField;
    qryTabRenegociacaonPercDescJurosVista: TBCDField;
    qryTabRenegociacaoiParcLimDescJuros: TIntegerField;
    qryTabRenegociacaonPercDescJurosParc: TBCDField;
    qryTabRenegociacaonPercDescJurosMaiorParc: TBCDField;
    qryTabRenegociacaoiQtdeMaxParc: TIntegerField;
    qryTabRenegociacaonPercMinEntrada: TBCDField;
    qryTabRenegociacaonPercEntradaSug: TBCDField;
    qryTabRenegociacaonValParcelaMin: TBCDField;
    qryTabRenegociacaoiDiasAtrasoFim: TIntegerField;
    qryTabRenegociacaonCdServidorOrigem: TIntegerField;
    qryTabRenegociacaodDtReplicacao: TDateTimeField;
    DBGridEh1: TDBGridEh;
    DataSource1: TDataSource;
    qryAux: TADOQuery;
    procedure FormShow(Sender: TObject);
    procedure qryTabRenegociacaoBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTabRenegociacao: TfrmTabRenegociacao;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmTabRenegociacao.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient ;
  PosicionaQuery(qryTabRenegociacao, intToStr(frmMenu.nCdEmpresaAtiva)) ;

end;

procedure TfrmTabRenegociacao.qryTabRenegociacaoBeforePost(
  DataSet: TDataSet);
begin

  qryTabRenegociacaonCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;

  qryAux.Close;
  qryAux.SQL.Clear;
  if (qryTabRenegociacao.State = dsEdit) then
      qryAux.SQL.Add('SELECT 1 FROM TabRenegociacao WHERE nCdTabRenegociacao <> ' + qryTabRenegociacaonCdTabRenegociacao.AsString + ' AND nCdEmpresa = ' + qryTabRenegociacaonCdEmpresa.AsString + ' AND ' + qryTabRenegociacaoiDiasAtraso.AsString + ' BETWEEN iDiasAtraso AND iDiasAtrasoFim')
  else qryAux.SQL.Add('SELECT 1 FROM TabRenegociacao WHERE nCdEmpresa = ' + qryTabRenegociacaonCdEmpresa.AsString + ' AND ' + qryTabRenegociacaoiDiasAtraso.AsString + ' BETWEEN iDiasAtraso AND iDiasAtrasoFim');
  qryAux.Open;

  if not qryAux.eof then
  begin
      MensagemAlerta('Esse intervalo de dia (dia inicial) j� est� cadastrado.') ;
      abort ;
  end ;

  qryAux.Close;
  qryAux.SQL.Clear;
  if (qryTabRenegociacao.State = dsEdit) then
      qryAux.SQL.Add('SELECT 1 FROM TabRenegociacao WHERE nCdTabRenegociacao <> ' + qryTabRenegociacaonCdTabRenegociacao.AsString + ' AND nCdEmpresa = ' + qryTabRenegociacaonCdEmpresa.AsString + ' AND ' + qryTabRenegociacaoiDiasAtrasoFim.AsString + ' BETWEEN iDiasAtraso AND iDiasAtrasoFim')
  else qryAux.SQL.Add('SELECT 1 FROM TabRenegociacao WHERE nCdEmpresa = ' + qryTabRenegociacaonCdEmpresa.AsString + ' AND ' + qryTabRenegociacaoiDiasAtrasoFim.AsString + ' BETWEEN iDiasAtraso AND iDiasAtrasoFim');
  qryAux.Open;

  if not qryAux.eof then
  begin
      MensagemAlerta('Esse intervalo de dia (dia final) j� est� cadastrado.') ;
      abort ;
  end ;

  if (qryTabRenegociacao.State = dsInsert) then
      qryTabRenegociacaonCdTabRenegociacao.Value := frmMenu.fnProximoCodigo('TABRENEGOCIACAO') ;
      
  inherited;

end;

initialization
    RegisterClass(TfrmTabRenegociacao) ;
    
end.
