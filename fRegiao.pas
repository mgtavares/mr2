unit fRegiao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmRegiao = class(TfrmCadastro_Padrao)
    qryMasternCdRegiao: TIntegerField;
    qryMastercNmRegiao: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRegiao: TfrmRegiao;

implementation

{$R *.dfm}

procedure TfrmRegiao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'REGIAO' ;
  nCdTabelaSistema  := 17 ;
  nCdConsultaPadrao := 21 ;
end;

procedure TfrmRegiao.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

initialization
    RegisterClass(tFrmRegiao);
    
end.
