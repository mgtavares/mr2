unit fManutPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, cxLookAndFeelPainters, GridsEh, DBGridEh, StdCtrls, cxButtons,
  Mask, DBCtrls, DBGridEhGrouping, cxPC, cxControls, ToolCtrlsEh;

type
  TfrmManutPedido = class(TfrmCadastro_Padrao)
    qryMasternCdPedido: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    qryMastercNmTipoPedido: TStringField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNmTerceiro: TStringField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMasternValPedido: TBCDField;
    qryMasternSaldoFat: TBCDField;
    qryMasternCdTabStatusPed: TIntegerField;
    qryMastercNmTabStatusPed: TStringField;
    qryItemEstoque: TADOQuery;
    qryItemEstoquenCdItemPedido: TAutoIncField;
    qryItemEstoquenCdPedido: TIntegerField;
    qryItemEstoquenCdItemPedidoPai: TIntegerField;
    qryItemEstoquenCdProduto: TIntegerField;
    qryItemEstoquecCdProduto: TStringField;
    qryItemEstoquenCdTipoItemPed: TIntegerField;
    qryItemEstoquenCdEstoqueMov: TIntegerField;
    qryItemEstoquecNmItem: TStringField;
    qryItemEstoquenQtdePed: TBCDField;
    qryItemEstoquenQtdeExpRec: TBCDField;
    qryItemEstoquenQtdeCanc: TBCDField;
    qryItemEstoquenValUnitario: TBCDField;
    qryItemEstoquenPercIPI: TBCDField;
    qryItemEstoquenValIPI: TBCDField;
    qryItemEstoquenValDesconto: TBCDField;
    qryItemEstoquenValCustoUnit: TBCDField;
    qryItemEstoquenValSugVenda: TBCDField;
    qryItemEstoquenValTotalItem: TBCDField;
    qryItemEstoquenValAcrescimo: TBCDField;
    qryItemEstoquecSiglaUnidadeMedida: TStringField;
    dsItemEstoque: TDataSource;
    qryItemEstoquenSaldoItem: TBCDField;
    usp_Grade: TADOStoredProc;
    qryTemp: TADOQuery;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutonCdGrade: TIntegerField;
    dsTemp: TDataSource;
    usp_Cancela_Item: TADOStoredProc;
    usp_Altera_data: TADOStoredProc;
    qryAux: TADOQuery;
    qryMastercFlgBaixaManual: TIntegerField;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label3: TLabel;
    DBEdit8: TDBEdit;
    btPrevisao: TcxButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    GroupBox2: TGroupBox;
    btGrade: TcxButton;
    btCancelaSaldo: TcxButton;
    btAlterarKit: TcxButton;
    btBxManual: TcxButton;
    btCancelaSaldoPedido: TcxButton;
    qryMastercFlgPedidoWeb: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btGradeClick(Sender: TObject);
    procedure qryTempAfterCancel(DataSet: TDataSet);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure btCancelaSaldoClick(Sender: TObject);
    procedure btPrevisaoClick(Sender: TObject);
    procedure btAlterarKitClick(Sender: TObject);
    procedure qryItemEstoqueAfterScroll(DataSet: TDataSet);
    procedure DBGridEh2Exit(Sender: TObject);
    procedure DBGridEh2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure btBxManualClick(Sender: TObject);
    procedure btCancelaSaldoPedidoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmManutPedido: TfrmManutPedido;

implementation

uses fMenu, fEmbFormula, fManutPedido_BaixaManual, fMotivoCancelaSaldoPedido, fMontaGrade;

{$R *.dfm}
var
  objGrade      : TfrmMontaGrade ;

procedure TfrmManutPedido.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PEDIDO' ;
  nCdTabelaSistema  := 30 ;
  nCdConsultaPadrao := 58 ;
end;

procedure TfrmManutPedido.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryItemEstoque, qryMasternCdPedido.asString) ;

  btPrevisao.Enabled := True ;
  btBxManual.Enabled := (qryMastercFlgBaixaManual.Value = 1) ;

  { -- desabilita bot�o grade para pedidos web devido utilizar produto final -- }
  if (qryMastercFlgPedidoWeb.Value = 1) then
      btGrade.Enabled := False
  else
      btGrade.Enabled := True;
end;

procedure TfrmManutPedido.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryItemEstoque.Close ;

  btPrevisao.Enabled     := False ;
  btGrade.Enabled        := True  ;
  btCancelaSaldo.Enabled := False ;

end;

procedure TfrmManutPedido.btGradeClick(Sender: TObject);
begin
  inherited;

  if (not qryItemEstoque.Active) or (qryItemEstoquenCdItemPedido.Value = 0) then
  begin
      MensagemAlerta('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

  if qryProduto.eof then
  begin
      MensagemAlerta('Selecione um item.') ;
      exit ;
  end ;

  if (qryProdutonCdGrade.Value = 0) then
  begin
      MensagemAlerta('Este item n�o est� configurado para trabalhar em grade.') ;
      exit ;
  end ;

  objGrade.qryValorAnterior.SQL.Text := '' ;

  if (qryItemEstoquenCdItemPedido.Value > 0) then
      objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtdePed FROM ItemPedido WHERE nCdTipoItemPed = 4 AND nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString + ' ORDER BY ncdProduto') ;

  objGrade.PreparaDataSet(qryItemEstoquenCdProduto.Value);
  objGrade.DBGridEh1.ReadOnly := True ;

  objGrade.Renderiza;
  objGrade.DBGridEh1.ReadOnly := False ;

  DBGridEh1.Col := 7 ;
  DBGridEh1.SetFocus;

end;

procedure TfrmManutPedido.qryTempAfterCancel(DataSet: TDataSet);
begin
  inherited;
  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmManutPedido.qryTempAfterPost(DataSet: TDataSet);
begin
  inherited;
  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmManutPedido.FormShow(Sender: TObject);
begin
  inherited;
  btPrevisao.Enabled     := False ;
  btGrade.Enabled        := True  ;
  btCancelaSaldo.Enabled := False ;

  objGrade := TfrmMontaGrade.Create( Self ) ;

end;

procedure TfrmManutPedido.btCancelaSaldoClick(Sender: TObject);
var
  objForm : TfrmMotivoCancelaSaldoPedido ;
begin
  inherited;

  if (qryItemEstoquenSaldoItem.Value = 0) then
  begin
      MensagemAlerta('Item com saldo zerado.') ;
      exit ;
  end ;

  {-- abre o form que solicita o motivo de cancelamento e PROCESSA o cancelamento do pedido --}
  objForm := TfrmMotivoCancelaSaldoPedido.Create( Self ) ;

  try
      objForm.cancelaSaldoPedido( qryMasternCdPedido.Value , qryItemEstoquenCdItemPedido.Value ) ;
  finally
      freeAndNil( objForm ) ;
  end ;

  PosicionaQuery(qryMaster, qryMasternCdPedido.asString) ;
  PosicionaQuery(qryItemEstoque, qryMasternCdPedido.asString) ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmManutPedido.btPrevisaoClick(Sender: TObject);
var
    cAltera:string ;
begin
  inherited;

  case MessageDlg('Confirma a altera��o do prazo de entrega ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  cAltera := 'S' ;

  case MessageDlg('Ajustar o prazo de pagamento das parcelas ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo: cAltera := 'N' ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try

      usp_Altera_Data.Close ;
      usp_Altera_Data.Parameters.ParamByName('@nCdPedido').Value      := qryMasternCdPedido.Value ;
      usp_Altera_Data.Parameters.ParamByName('@dDtPrevEntIni').Value  := frmMenu.ConvData(DBEdit7.Text) ;
      usp_Altera_Data.Parameters.ParamByName('@dDtPrevEntFim').Value  := frmMenu.ConvData(DBEdit8.Text) ;
      usp_Altera_Data.Parameters.ParamByName('@cAjusteParcela').Value := cAltera ;
      usp_Altera_Data.ExecProc ;

  except
      frmMenu.Connection.RollBackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  ShowMessage('Processamento conclu�do com sucesso.') ;

  PosicionaQuery(qryMaster, qryMasternCdPedido.asString) ;
  PosicionaQuery(qryItemEstoque, qryMasternCdPedido.asString) ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmManutPedido.btAlterarKitClick(Sender: TObject);
begin

  {-- fun��o desativada por exig�ncia da nota fiscal eletr�nica --}

end;

procedure TfrmManutPedido.qryItemEstoqueAfterScroll(DataSet: TDataSet);
begin
  inherited;

  btAlterarKit.Enabled := False ;

  if (qryItemEstoquenCdTipoItemPed.Value = 6) then
      btAlterarKit.Enabled := True ;

  btCancelaSaldo.Enabled := (qryItemEstoquenSaldoItem.Value > 0) ;

end;

procedure TfrmManutPedido.DBGridEh2Exit(Sender: TObject);
begin
  inherited;

  DBGridEh2.Visible := False ;
end;

procedure TfrmManutPedido.DBGridEh2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  // t�tulos vencidos
  if (Column.Field.Value <> '0') then
  begin

    DBGridEh2.Canvas.Brush.Color := clYellow ; //$0080FFFF ;
    DBGridEh2.Canvas.FillRect(Rect);
    DBGridEh2.DefaultDrawDataCell(Rect,Column.Field,State);

  end ;

end;

procedure TfrmManutPedido.btBxManualClick(Sender: TObject);
var
  objForm : TfrmManutPedido_BaixaManual ;

begin
  inherited;

  objForm := TfrmManutPedido_BaixaManual.Create(nil);

  PosicionaQuery(objForm.qryItemPedido, qryMasternCdPedido.asString) ;
  objForm.nCdPedido := qryMasternCdPedido.Value ;

  showForm(objForm,True);

  PosicionaQuery(qryMaster, qryMasternCdPedido.asString) ;

end;

procedure TfrmManutPedido.btCancelaSaldoPedidoClick(Sender: TObject);
var
  objForm      : TfrmMotivoCancelaSaldoPedido ;
  nSaldoPedido : double ;
begin
  inherited;

  if qryMaster.Active and (qryItemEstoque.RecordCount > 0) then
  begin

      nSaldoPedido := 0 ;

      qryItemEstoque.First;

      while not qryItemEstoque.eof do
      begin

          if (qryItemEstoquenSaldoItem.Value > 0) then
              nSaldoPedido := nSaldoPedido + qryItemEstoquenSaldoItem.Value ;

          qryItemEstoque.Next;
      end ;

      qryItemEstoque.First;

      if (nSaldoPedido <= 0) then
      begin
          MensagemAlerta('Nenhum item com saldo pendente para cancelamento.') ;
          abort ;
      end ;


      {-- abre o form que solicita o motivo de cancelamento e PROCESSA o cancelamento do pedido --}
      objForm := TfrmMotivoCancelaSaldoPedido.Create( Self ) ;

      try
          objForm.cancelaSaldoPedido( qryMasternCdPedido.Value , 0 ) ;
      finally
          freeAndNil( objForm ) ;
      end ;

      PosicionaQuery(qryMaster, qryMasternCdPedido.asString) ;
      PosicionaQuery(qryItemEstoque, qryMasternCdPedido.asString) ;

      DBGridEh1.SetFocus ;

  end ;

end;

initialization
    RegisterClass(tFrmManutPedido) ;

end.
