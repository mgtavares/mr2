inherited frmAdmListaCobranca_Listas: TfrmAdmListaCobranca_Listas
  Left = 211
  Top = 0
  Width = 1018
  Height = 728
  BorderIcons = [biSystemMenu]
  Caption = 'Listas'
  OldCreateOrder = True
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1002
    Height = 661
  end
  inherited ToolBar1: TToolBar
    Width = 1002
    ButtonWidth = 97
    inherited ToolButton1: TToolButton
      Caption = '&Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 97
    end
    inherited ToolButton2: TToolButton
      Left = 105
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1002
    Height = 661
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsLista
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    PopupMenu = PopupMenu1
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdListaCobranca'
        Footers = <>
        Width = 50
      end
      item
        EditButtons = <>
        FieldName = 'dDtCobranca'
        Footers = <>
        Width = 77
      end
      item
        EditButtons = <>
        FieldName = 'cNmListaCobranca'
        Footers = <>
        Width = 150
      end
      item
        EditButtons = <>
        FieldName = 'iSeq'
        Footers = <>
        Width = 34
      end
      item
        EditButtons = <>
        FieldName = 'cSituacao'
        Footers = <>
        Width = 80
      end
      item
        EditButtons = <>
        FieldName = 'dDtVencto'
        Footers = <>
        Width = 77
      end
      item
        EditButtons = <>
        FieldName = 'dDtAtribuicao'
        Footers = <>
        Width = 77
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
        Width = 67
      end
      item
        EditButtons = <>
        FieldName = 'iQtdeClienteTotal'
        Footers = <>
        Width = 60
      end
      item
        EditButtons = <>
        FieldName = 'iQtdeClienteRestante'
        Footers = <>
        Width = 68
      end
      item
        EditButtons = <>
        FieldName = 'iQtdeClienteContato'
        Footers = <>
        Width = 77
      end
      item
        EditButtons = <>
        FieldName = 'iQtdeReagendado'
        Footers = <>
        Width = 82
      end
      item
        EditButtons = <>
        FieldName = 'iQtdeSemContato'
        Footers = <>
        Width = 76
      end
      item
        EditButtons = <>
        FieldName = 'iQtdeAcordo'
        Footers = <>
        Width = 87
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryLista: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cFlgSomenteNaoAtrib'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cFlgSomenteNaoEncerrada'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdListaCobranca'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuarioResp'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdCronogramaCob'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtCobranca'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtCobrancaFim'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @cFlgSomenteNaoAtrib     int'
      '       ,@cFlgSomenteNaoEncerrada int'
      '       ,@nCdListaCobranca        int'
      '       ,@nCdUsuarioResp          int'
      '       ,@nCdLoja                 int'
      '       ,@nCdCronogramaCob        int'
      '       ,@dDtCobranca             varchar(10)'
      '       ,@dDtCobrancaFim          varchar(10)'
      ''
      'Set @cFlgSomenteNaoAtrib     = :cFlgSomenteNaoAtrib'
      'Set @cFlgSomenteNaoEncerrada = :cFlgSomenteNaoEncerrada'
      'Set @nCdListaCobranca        = :nCdListaCobranca'
      'Set @nCdUsuarioResp          = :nCdUsuarioResp'
      'Set @nCdLoja                 = :nCdLoja'
      'Set @nCdCronogramaCob        = :nCdCronogramaCob'
      'Set @dDtCobranca             = :dDtCobranca'
      'Set @dDtCobrancaFim          = :dDtCobrancaFim'
      ''
      'IF (@dDtCobrancaFim = '#39#39') Set @dDtCobrancaFim = @dDtCobranca'
      ' '
      'SELECT nCdListaCobranca'
      '      ,ListaCobranca.dDtCobranca'
      '      ,ListaCobranca.dDtVencto'
      '      ,iSeq'
      '      ,cNmListaCobranca'
      '      ,iQtdeClienteTotal'
      
        '      ,(iQtdeClienteTotal - iQtdeClienteContato - iQtdeSemContat' +
        'o) iQtdeClienteRestante'
      '      ,dDtAtribuicao'
      '      ,dDtAbertura'
      '      ,cNmUsuario'
      '      ,nCdUsuarioResp'
      '      ,iQtdeClienteContato'
      '      ,iQtdeReagendado'
      '      ,iQtdeSemContato'
      '      ,iQtdeAcordo'
      '      ,CASE WHEN dDtCancel       IS NOT NULL THEN '#39'Cancelada'#39
      '            WHEN dDtEncerramento IS NOT NULL THEN '#39'Encerrada'#39
      '            ELSE '#39'Aberta'#39
      '       END cSituacao'
      '      ,ListaCobranca.cFlgTelefone'
      '      ,ListaCobranca.dDtEncerramento'
      '      ,ListaCobranca.cFlgNegativar'
      '  FROM ListaCobranca'
      
        '       LEFT JOIN Usuario          ON Usuario.nCdUsuario         ' +
        '          = ListaCobranca.nCdUsuarioResp'
      
        '       LEFT JOIN CronogramaCobDia ON CronogramaCobDia.nCdCronogr' +
        'amaCobDia = ListaCobranca.nCdCronogramaCobDia'
      ' WHERE ListaCobranca.nCdEmpresa      = :nCdEmpresa'
      
        '   AND ((@cFlgSomenteNaoEncerrada = 0) OR (@cFlgSomenteNaoEncerr' +
        'ada = 1 AND dDtEncerramento IS NULL))'
      
        '   AND ((@cFlgSomenteNaoAtrib     = 0) OR (@cFlgSomenteNaoAtrib ' +
        '    = 1 AND nCdUsuarioResp  IS NULL))'
      
        '   AND ((@nCdUsuarioResp          = 0) OR (nCdUsuarioResp       ' +
        '             = @nCdUsuarioResp))'
      
        '   AND ((@nCdListaCobranca        = 0) OR (nCdListaCobranca     ' +
        '             = @nCdListaCobranca))'
      
        '   AND ((@nCdLoja                 = 0) OR (ListaCobranca.nCdLoja' +
        '             = @nCdLoja))'
      
        '   AND ((ListaCobranca.dDtCobranca >= Convert(DATETIME,@dDtCobra' +
        'nca,103))      OR (@dDtCobranca    = '#39'01/01/1900'#39'))'
      
        '   AND ((ListaCobranca.dDtCobranca <  Convert(DATETIME,@dDtCobra' +
        'ncaFim,103)+1) OR (@dDtCobrancaFim = '#39'01/01/1900'#39'))'
      
        '   AND ((@nCdCronogramaCob        = 0) OR (    CronogramaCobDia.' +
        'nCdCronogramaCob = @nCdCronogramaCob'
      
        '                                           AND CronogramaCobDia.' +
        'dDtCobranca      >= Convert(DATETIME,@dDtCobranca,103)'
      
        '                                           AND CronogramaCobDia.' +
        'dDtCobranca      <  Convert(DATETIME,@dDtCobrancaFim,103)+1))'
      ' ORDER BY dDtCobranca, cNmListaCobranca, iSeq'
      '')
    Left = 304
    Top = 168
    object qryListanCdListaCobranca: TIntegerField
      DisplayLabel = 'Listas de Cobran'#231'a|C'#243'd'
      FieldName = 'nCdListaCobranca'
    end
    object qryListadDtCobranca: TDateTimeField
      DisplayLabel = 'Listas de Cobran'#231'a|Data Cobran'#231'a'
      FieldName = 'dDtCobranca'
    end
    object qryListadDtVencto: TDateTimeField
      DisplayLabel = 'Listas de Cobran'#231'a|Data Vencto'
      FieldName = 'dDtVencto'
    end
    object qryListaiSeq: TIntegerField
      DisplayLabel = 'Listas de Cobran'#231'a|Seq.'
      FieldName = 'iSeq'
    end
    object qryListadDtAtribuicao: TDateTimeField
      DisplayLabel = 'Listas de Cobran'#231'a|Data/Hora Atribui'#231#227'o'
      FieldName = 'dDtAtribuicao'
    end
    object qryListadDtAbertura: TDateTimeField
      DisplayLabel = 'Listas de Cobran'#231'a|Data Abertura'
      FieldName = 'dDtAbertura'
    end
    object qryListacNmListaCobranca: TStringField
      DisplayLabel = 'Listas de Cobran'#231'a|Nome Lista'
      FieldName = 'cNmListaCobranca'
      Size = 50
    end
    object qryListacNmUsuario: TStringField
      DisplayLabel = 'Listas de Cobran'#231'a|Analista Cobran'#231'a'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryListanCdUsuarioResp: TIntegerField
      FieldName = 'nCdUsuarioResp'
    end
    object qryListaiQtdeClienteTotal: TIntegerField
      DisplayLabel = 'Listas de Cobran'#231'a|Qtde Clientes'
      FieldName = 'iQtdeClienteTotal'
    end
    object qryListaiQtdeClienteRestante: TIntegerField
      DisplayLabel = 'Listas de Cobran'#231'a|Qtde Restantes'
      FieldName = 'iQtdeClienteRestante'
      ReadOnly = True
    end
    object qryListaiQtdeClienteContato: TIntegerField
      DisplayLabel = 'Listas de Cobran'#231'a|Qtde Clientes Contatados'
      FieldName = 'iQtdeClienteContato'
    end
    object qryListaiQtdeReagendado: TIntegerField
      DisplayLabel = 'Listas de Cobran'#231'a|Qtde Clientes Reagendados'
      FieldName = 'iQtdeReagendado'
    end
    object qryListaiQtdeSemContato: TIntegerField
      DisplayLabel = 'Listas de Cobran'#231'a|Qtde Clientes Sem Contato'
      FieldName = 'iQtdeSemContato'
    end
    object qryListaiQtdeAcordo: TIntegerField
      DisplayLabel = 'Listas de Cobran'#231'a|Qtde Clientes Renegociaram'
      FieldName = 'iQtdeAcordo'
    end
    object qryListacSituacao: TStringField
      DisplayLabel = 'Listas de Cobran'#231'a|Situa'#231#227'o'
      FieldName = 'cSituacao'
      ReadOnly = True
      Size = 9
    end
    object qryListacFlgTelefone: TIntegerField
      FieldName = 'cFlgTelefone'
    end
    object qryListadDtEncerramento: TDateTimeField
      FieldName = 'dDtEncerramento'
    end
    object qryListacFlgNegativar: TIntegerField
      FieldName = 'cFlgNegativar'
    end
  end
  object dsLista: TDataSource
    DataSet = qryLista
    Left = 352
    Top = 168
  end
  object PopupMenu1: TPopupMenu
    Left = 440
    Top = 280
    object GerenciarItens1: TMenuItem
      Caption = 'Atribuir/Alterar Analista'
      OnClick = GerenciarItens1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object EncerrarLista1: TMenuItem
      Caption = 'Encerrar Lista'
      OnClick = EncerrarLista1Click
    end
    object CancelarLista1: TMenuItem
      Caption = 'Cancelar Lista'
      OnClick = CancelarLista1Click
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 560
    Top = 288
  end
end
