unit fGeraRemessaRecebimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, GridsEh, DBGridEh, StdCtrls, DB, ADODB;

type
  TfrmGeraRemessaRecebimento = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    RadioGroup1: TRadioGroup;
    DBGridEh1: TDBGridEh;
    qryRemessas: TADOQuery;
    DataSource1: TDataSource;
    qryRemessasnCdBorderoFinanceiro: TIntegerField;
    qryRemessasdDtGeracao: TDateTimeField;
    qryRemessasnCdUsuarioGeracao: TIntegerField;
    qryRemessasnCdBanco: TIntegerField;
    qryRemessascNmBanco: TStringField;
    qryRemessascAgencia: TIntegerField;
    qryRemessasnCdConta: TStringField;
    qryRemessascNmTitular: TStringField;
    qryRemessasnValTotalTitulo: TBCDField;
    qryRemessascNmArquivoRemessa: TStringField;
    qryRemessascNmComputadorGeracao: TStringField;
    qryRemessasnQtdeTitulo: TIntegerField;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGeraRemessaRecebimento: TfrmGeraRemessaRecebimento;

implementation

uses fGeraRemessaRecebimento_titulo;

{$R *.dfm}

procedure TfrmGeraRemessaRecebimento.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryRemessas.Close;
  qryRemessas.Parameters.ParamByName('cFlgTodasRemessas').Value := RadioGroup1.ItemIndex;
  qryRemessas.Open;
  
end;

procedure TfrmGeraRemessaRecebimento.FormShow(Sender: TObject);
begin
  inherited;

  ToolButton1.Click;
end;

procedure TfrmGeraRemessaRecebimento.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmGeraRemessaRecebimento_titulo;
begin
  inherited;

  if (qryRemessasnCdBorderoFinanceiro.Value = 0) then
      exit;

  objForm := TfrmGeraRemessaRecebimento_titulo.Create(nil);

  objForm.qryTitulosRemessa.Close;
  objForm.PosicionaQuery(objForm.qryTitulosRemessa,qryRemessasnCdBorderoFinanceiro.AsString);

  showForm(objForm, True);

  ToolButton1.Click;
end;

procedure TfrmGeraRemessaRecebimento.RadioGroup1Click(Sender: TObject);
begin
  inherited;

  qryRemessas.Close;
  qryRemessas.Parameters.ParamByName('cFlgTodasRemessas').Value := RadioGroup1.ItemIndex;
  qryRemessas.Open;
end;

initialization
  RegisterClass(TfrmGeraRemessaRecebimento);

end.
