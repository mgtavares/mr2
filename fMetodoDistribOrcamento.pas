unit fMetodoDistribOrcamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, DBGridEhGrouping, GridsEh, DBGridEh,
  cxPC, cxControls;

type
  TfrmMetodoDistribOrcamento = class(TfrmCadastro_Padrao)
    qryMasternCdMetodoDistribOrcamento: TIntegerField;
    qryMastercNmMetodoDistribOrcamento: TStringField;
    qryMastercFlgLinear: TIntegerField;
    qryMastercFlgPadrao: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryFatorDistribOrcam: TADOQuery;
    qryMes: TADOQuery;
    dsFatorMetodoDistribOrcamento: TDataSource;
    qryFatorDistribOrcamcNmMes: TStringField;
    qryDefinePadrao: TADOQuery;
    qryMesnCdTabTipoMes: TIntegerField;
    qryMescNmTabTipoMes: TStringField;
    qryFatorDistribOrcamnCdFatorMetodoDistribOrcamento: TIntegerField;
    qryFatorDistribOrcamnCdMetodoDistribOrcamento: TIntegerField;
    qryFatorDistribOrcamnCdTabTipoMes: TIntegerField;
    qryFatorDistribOrcamnPercent: TBCDField;
    qryInsereMes: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryFatorDistribOrcamCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMetodoDistribOrcamento: TfrmMetodoDistribOrcamento;

implementation

{$R *.dfm}

uses fMenu, fLookup_Padrao;

procedure TfrmMetodoDistribOrcamento.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'METODODISTRIBORCAMENTO' ;
  nCdTabelaSistema  := 427 ;
  nCdConsultaPadrao := 750 ;
  bLimpaAposSalvar  := False;

end;

procedure TfrmMetodoDistribOrcamento.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
end;

procedure TfrmMetodoDistribOrcamento.qryMasterBeforePost(
  DataSet: TDataSet);
begin

  if (Trim(qryMastercNmMetodoDistribOrcamento.Value) = '') then
  begin
      MensagemAlerta('Digite a Descrição para o Método de Distribuição.');
      DBEdit2.SetFocus;
      abort;
  end;

  if ((qryFatorDistribOrcam.Active) and (qryFatorDistribOrcam.RecordCount > 0)) then
  begin
      if ((DBGridEh1.Columns[2].Footers[0].SumValue <> 100) and (DBGridEh1.Columns[2].Footers[0].SumValue <> 0)) then
      begin
          MensagemAlerta('A soma da distribuição mensal deve ser 0 ou igual a 100%.');
          abort;
      end;
  end;

  inherited;

end;

procedure TfrmMetodoDistribOrcamento.qryMasterAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryFatorDistribOrcam.Close;
  qryFatorDistribOrcam.Parameters.ParamByName('nPK').Value := qryMasternCdMetodoDistribOrcamento.Value;
  qryFatorDistribOrcam.Open;
  
end;

procedure TfrmMetodoDistribOrcamento.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;

end;

procedure TfrmMetodoDistribOrcamento.qryFatorDistribOrcamCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryMes.Close;
  qryMes.Parameters.ParamByName('nCdTabTipoMes').Value := qryFatorDistribOrcamnCdTabTipoMes.Value;
  qryMes.Open;

  qryFatorDistribOrcamcNmMes.Value := qryMescNmTabTipoMes.Value;
end;

procedure TfrmMetodoDistribOrcamento.qryMasterAfterClose(
  DataSet: TDataSet);
begin
  inherited;

  qryFatorDistribOrcam.Close;
  qryMes.Close;
  
end;

procedure TfrmMetodoDistribOrcamento.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  if (qryMastercFlgPadrao.Value = 1) then
  begin
      frmMenu.Connection.BeginTrans;

      try
          qryDefinePadrao.Close;
          qryDefinePadrao.Parameters.ParamByName('nCdMetodoDistribOrcamento').Value := qryMasternCdMetodoDistribOrcamento.Value;
          qryDefinePadrao.ExecSQL;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no Processamento.');
          raise;
      end;

      frmMenu.Connection.CommitTrans;
  end;

  if (qryFatorDistribOrcam.RecordCount <= 0) then
  begin

      frmMenu.Connection.BeginTrans;

      try
          qryInsereMes.Close;
          qryInsereMes.Parameters.ParamByName('nCdMetodoDistribOrcamento').Value := qryMasternCdMetodoDistribOrcamento.Value;
          qryInsereMes.ExecSQL;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no Processamento.');
          raise;
      end;

      frmMenu.Connection.CommitTrans;

      qryFatorDistribOrcam.Close;
      qryFatorDistribOrcam.Parameters.ParamByName('nPK').Value := qryMasternCdMetodoDistribOrcamento.Value;
      qryFatorDistribOrcam.Open;
  end;


end;

procedure TfrmMetodoDistribOrcamento.btSalvarClick(Sender: TObject);
begin
  inherited;

  btCancelar.Click;
end;

initialization
  RegisterClass(TfrmMetodoDistribOrcamento);

end.
