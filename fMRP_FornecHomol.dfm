inherited frmMRP_FornecHomol: TfrmMRP_FornecHomol
  Left = 175
  Top = 203
  BorderIcons = [biSystemMenu]
  Caption = 'Fornecedores Homologados para Compra com Contrato'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsFornecedores
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnMoving = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.FonteSomenteLeitura
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn
        Caption = 'C'#243'd'
        DataBinding.FieldName = 'nCdTerceiro'
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro Fonrecedor'
        DataBinding.FieldName = 'cNmTerceiro'
      end
      object cxGrid1DBTableView1nPercentCompra: TcxGridDBColumn
        DataBinding.FieldName = 'nPercentCompra'
      end
      object cxGrid1DBTableView1cNrContrato: TcxGridDBColumn
        Caption = 'N'#250'mero Contrato'
        DataBinding.FieldName = 'cNrContrato'
      end
      object cxGrid1DBTableView1dDtValidadeIni: TcxGridDBColumn
        Caption = 'V'#225'lido de'
        DataBinding.FieldName = 'dDtValidadeIni'
      end
      object cxGrid1DBTableView1dDtValidadeFim: TcxGridDBColumn
        Caption = 'At'#233
        DataBinding.FieldName = 'dDtValidadeFim'
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryFornecedores: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdProdutoPai int'
      '       ,@nCdProduto    int'
      ''
      'Set @nCdProduto = :nPK'
      ''
      'SELECT @nCdProdutoPai = nCdProdutoPai'
      '  FROM Produto'
      ' WHERE nCdProduto = @nCdProduto'
      ''
      'SELECT Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,nPercentCompra'
      '      ,cNrContrato'
      '      ,dDtValidadeIni'
      '      ,dDtValidadeFim'
      '  FROM ProdutoFornecedor'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = ProdutoForn' +
        'ecedor.nCdTerceiro'
      ' WHERE nCdproduto                 = @nCdProduto'
      '   AND nCdTipoRessuprimentoFornec = 2'
      'UNION'
      'SELECT Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,nPercentCompra'
      '      ,cNrContrato'
      '      ,dDtValidadeIni'
      '      ,dDtValidadeFim'
      '  FROM ProdutoFornecedor'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = ProdutoForn' +
        'ecedor.nCdTerceiro'
      ' WHERE nCdproduto                 = @nCdProdutoPai'
      '   AND nCdTipoRessuprimentoFornec = 2')
    Left = 464
    Top = 248
    object qryFornecedoresnCdTerceiro: TIntegerField
      DisplayLabel = 'Fornecedor|C'#243'd'
      FieldName = 'nCdTerceiro'
    end
    object qryFornecedorescNmTerceiro: TStringField
      DisplayLabel = 'Fornecedor|Nome'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryFornecedoresnPercentCompra: TBCDField
      DisplayLabel = '% Compra'
      FieldName = 'nPercentCompra'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryFornecedorescNrContrato: TStringField
      DisplayLabel = 'N'#250'mero|Contrato'
      FieldName = 'cNrContrato'
      Size = 15
    end
    object qryFornecedoresdDtValidadeIni: TDateTimeField
      DisplayLabel = 'Per'#237'odo Validade|In'#237'cio'
      FieldName = 'dDtValidadeIni'
    end
    object qryFornecedoresdDtValidadeFim: TDateTimeField
      DisplayLabel = 'Per'#237'odo Validade|Fim'
      FieldName = 'dDtValidadeFim'
    end
  end
  object dsFornecedores: TDataSource
    DataSet = qryFornecedores
    Left = 496
    Top = 248
  end
end
