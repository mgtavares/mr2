inherited frmDoctoFiscal: TfrmDoctoFiscal
  Left = 180
  Top = 93
  Width = 1118
  Height = 604
  Caption = 'Consulta/Reimpress'#227'o Documento Fiscal'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 337
    Width = 1110
    Height = 240
  end
  inherited ToolBar2: TToolBar
    Width = 1110
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited ToolButton7: TToolButton
      Visible = False
    end
    inherited ToolButton9: TToolButton
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Visible = False
    end
    inherited btSalvar: TToolButton
      Visible = False
    end
    inherited ToolButton6: TToolButton
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
    object ToolButton3: TToolButton
      Left = 856
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton10: TToolButton
      Left = 864
      Top = 0
      Caption = 'Op'#231#245'es'
      DropdownMenu = PopupMenu1
      ImageIndex = 10
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 25
    Width = 1110
    Height = 312
    Align = alTop
    Caption = ' Dados do Documento Fiscal '
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 18
      Top = 24
      Width = 64
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo Doc.'
      FocusControl = DBEdit1
    end
    object Label5: TLabel
      Tag = 1
      Left = 10
      Top = 118
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'Entrada/Sa'#237'da'
      FocusControl = DBEdit5
    end
    object Label7: TLabel
      Tag = 1
      Left = 280
      Top = 22
      Width = 25
      Height = 13
      Caption = 'S'#233'rie'
      FocusControl = DBEdit7
    end
    object Label8: TLabel
      Tag = 1
      Left = 32
      Top = 96
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Opera'#231#227'o'
      FocusControl = DBEdit8
    end
    object Label10: TLabel
      Tag = 1
      Left = 160
      Top = 22
      Width = 41
      Height = 13
      Caption = 'N'#250'mero'
      FocusControl = DBEdit10
    end
    object Label2: TLabel
      Tag = 1
      Left = 361
      Top = 22
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Emiss'#227'o'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Tag = 1
      Left = 549
      Top = 118
      Width = 49
      Height = 13
      Caption = 'Data Imp.'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Tag = 1
      Left = 42
      Top = 166
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro'
      FocusControl = DBEdit4
    end
    object Label9: TLabel
      Tag = 1
      Left = 161
      Top = 118
      Width = 32
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status'
      FocusControl = DBEdit17
    end
    object Label11: TLabel
      Tag = 1
      Left = 19
      Top = 286
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Fatura'
      FocusControl = DBEdit18
    end
    object Label12: TLabel
      Tag = 1
      Left = 699
      Top = 286
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Total Documento'
      FocusControl = DBEdit19
    end
    object Label14: TLabel
      Tag = 1
      Left = 176
      Top = 286
      Width = 66
      Height = 13
      Alignment = taRightJustify
      Caption = 'Incent. Fiscal'
      FocusControl = DBEdit21
    end
    object Label15: TLabel
      Tag = 1
      Left = 710
      Top = 238
      Width = 77
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Produtos'
      FocusControl = DBEdit22
    end
    object Label16: TLabel
      Tag = 1
      Left = 29
      Top = 238
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'Base ICMS'
      FocusControl = DBEdit23
    end
    object Label17: TLabel
      Tag = 1
      Left = 186
      Top = 238
      Width = 56
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor ICMS'
      FocusControl = DBEdit24
    end
    object Label18: TLabel
      Tag = 1
      Left = 345
      Top = 238
      Width = 67
      Height = 13
      Alignment = taRightJustify
      Caption = 'Base ICMS ST'
      FocusControl = DBEdit25
    end
    object Label19: TLabel
      Tag = 1
      Left = 533
      Top = 238
      Width = 70
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor ICMS ST'
      FocusControl = DBEdit26
    end
    object Label22: TLabel
      Tag = 1
      Left = 524
      Top = 22
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Canc.'
      FocusControl = DBEdit29
    end
    object Label6: TLabel
      Tag = 1
      Left = 28
      Top = 46
      Width = 54
      Height = 13
      Alignment = taRightJustify
      Caption = 'Chave NFe'
      FocusControl = DBEdit6
    end
    object Label23: TLabel
      Tag = 1
      Left = 494
      Top = 46
      Width = 85
      Height = 13
      Alignment = taRightJustify
      Caption = 'Proto. Canc. NFe'
      FocusControl = DBEdit30
    end
    object Label13: TLabel
      Tag = 1
      Left = 56
      Top = 262
      Width = 26
      Height = 13
      Alignment = taRightJustify
      Caption = 'Frete'
      FocusControl = DBEdit20
    end
    object Label20: TLabel
      Tag = 1
      Left = 194
      Top = 262
      Width = 48
      Height = 13
      Alignment = taRightJustify
      Caption = 'Despesas'
      FocusControl = DBEdit27
    end
    object Label21: TLabel
      Tag = 1
      Left = 745
      Top = 262
      Width = 42
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor IPI'
      FocusControl = DBEdit28
    end
    object Label24: TLabel
      Tag = 1
      Left = 564
      Top = 262
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Base IPI'
      FocusControl = DBEdit31
    end
    object Label25: TLabel
      Tag = 1
      Left = 367
      Top = 286
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor PIS'
      FocusControl = DBEdit32
    end
    object Label26: TLabel
      Tag = 1
      Left = 534
      Top = 286
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor COFINS'
      FocusControl = DBEdit33
    end
    object Label27: TLabel
      Tag = 1
      Left = 375
      Top = 262
      Width = 37
      Height = 13
      Alignment = taRightJustify
      Caption = 'Seguro'
      FocusControl = DBEdit34
    end
    object Label29: TLabel
      Tag = 1
      Left = 271
      Top = 117
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Usu'#225'rio Imp.'
      FocusControl = DBEdit17
    end
    object Label28: TLabel
      Left = 816
      Top = 46
      Width = 3
      Height = 13
      Alignment = taRightJustify
      FocusControl = DBEdit36
    end
    object Label30: TLabel
      Tag = 1
      Left = 730
      Top = 46
      Width = 54
      Height = 13
      Alignment = taRightJustify
      Caption = 'Just. Canc.'
      FocusControl = DBEdit30
    end
    object Label31: TLabel
      Tag = 1
      Left = 38
      Top = 70
      Width = 44
      Height = 13
      Alignment = taRightJustify
      Caption = 'Situa'#231#227'o'
      FocusControl = DBEdit6
    end
    object Label32: TLabel
      Left = 12
      Top = 141
      Width = 67
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo Doc Fisc'
      FocusControl = DBEdit39
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 88
      Top = 16
      Width = 65
      Height = 19
      DataField = 'nCdDoctoFiscal'
      DataSource = dsMaster
      TabOrder = 0
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 88
      Top = 112
      Width = 65
      Height = 19
      DataField = 'cFlgEntSai'
      DataSource = dsMaster
      TabOrder = 1
    end
    object DBEdit7: TDBEdit
      Tag = 1
      Left = 312
      Top = 16
      Width = 41
      Height = 19
      DataField = 'cSerie'
      DataSource = dsMaster
      TabOrder = 2
    end
    object DBEdit8: TDBEdit
      Tag = 1
      Left = 88
      Top = 88
      Width = 65
      Height = 19
      DataField = 'cCFOP'
      DataSource = dsMaster
      TabOrder = 3
    end
    object DBEdit9: TDBEdit
      Tag = 1
      Left = 157
      Top = 88
      Width = 772
      Height = 19
      DataField = 'cTextoCFOP'
      DataSource = dsMaster
      TabOrder = 4
    end
    object DBEdit10: TDBEdit
      Tag = 1
      Left = 208
      Top = 16
      Width = 65
      Height = 19
      DataField = 'iNrDocto'
      DataSource = dsMaster
      TabOrder = 5
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 408
      Top = 16
      Width = 81
      Height = 19
      DataField = 'dDtEmissao'
      DataSource = dsMaster
      TabOrder = 6
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 601
      Top = 112
      Width = 139
      Height = 19
      DataField = 'dDtImpressao'
      DataSource = dsMaster
      TabOrder = 7
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 88
      Top = 160
      Width = 65
      Height = 19
      DataField = 'nCdTerceiro'
      DataSource = dsMaster
      TabOrder = 8
    end
    object DBEdit11: TDBEdit
      Tag = 1
      Left = 157
      Top = 160
      Width = 716
      Height = 19
      DataField = 'cNmTerceiro'
      DataSource = dsMaster
      TabOrder = 9
    end
    object DBEdit12: TDBEdit
      Tag = 1
      Left = 88
      Top = 184
      Width = 700
      Height = 19
      DataField = 'cEndereco'
      DataSource = dsMaster
      TabOrder = 10
    end
    object DBEdit13: TDBEdit
      Tag = 1
      Left = 792
      Top = 184
      Width = 81
      Height = 19
      DataField = 'iNumero'
      DataSource = dsMaster
      TabOrder = 11
    end
    object DBEdit14: TDBEdit
      Tag = 1
      Left = 88
      Top = 208
      Width = 281
      Height = 19
      DataField = 'cBairro'
      DataSource = dsMaster
      TabOrder = 12
    end
    object DBEdit15: TDBEdit
      Tag = 1
      Left = 373
      Top = 208
      Width = 415
      Height = 19
      DataField = 'cCidade'
      DataSource = dsMaster
      TabOrder = 13
    end
    object DBEdit16: TDBEdit
      Tag = 1
      Left = 792
      Top = 208
      Width = 81
      Height = 19
      DataField = 'cUF'
      DataSource = dsMaster
      TabOrder = 14
    end
    object DBEdit17: TDBEdit
      Tag = 1
      Left = 200
      Top = 112
      Width = 65
      Height = 19
      DataField = 'nCdStatus'
      DataSource = dsMaster
      TabOrder = 15
    end
    object DBEdit18: TDBEdit
      Tag = 1
      Left = 88
      Top = 280
      Width = 81
      Height = 19
      DataField = 'nValFatura'
      DataSource = dsMaster
      TabOrder = 16
    end
    object DBEdit19: TDBEdit
      Tag = 1
      Left = 792
      Top = 280
      Width = 81
      Height = 19
      DataField = 'nValTotal'
      DataSource = dsMaster
      TabOrder = 17
    end
    object DBEdit21: TDBEdit
      Tag = 1
      Left = 248
      Top = 280
      Width = 81
      Height = 19
      DataField = 'nValIcentivoFiscal'
      DataSource = dsMaster
      TabOrder = 18
    end
    object DBEdit22: TDBEdit
      Tag = 1
      Left = 792
      Top = 232
      Width = 81
      Height = 19
      DataField = 'nValProduto'
      DataSource = dsMaster
      TabOrder = 19
    end
    object DBEdit23: TDBEdit
      Tag = 1
      Left = 88
      Top = 232
      Width = 81
      Height = 19
      DataField = 'nValBaseICMS'
      DataSource = dsMaster
      TabOrder = 20
    end
    object DBEdit24: TDBEdit
      Tag = 1
      Left = 248
      Top = 232
      Width = 81
      Height = 19
      DataField = 'nValICMS'
      DataSource = dsMaster
      TabOrder = 21
    end
    object DBEdit25: TDBEdit
      Tag = 1
      Left = 416
      Top = 232
      Width = 81
      Height = 19
      DataField = 'nValBaseICMSSub'
      DataSource = dsMaster
      TabOrder = 22
    end
    object DBEdit26: TDBEdit
      Tag = 1
      Left = 608
      Top = 232
      Width = 81
      Height = 19
      DataField = 'nValICMSSub'
      DataSource = dsMaster
      TabOrder = 23
    end
    object DBEdit29: TDBEdit
      Tag = 1
      Left = 585
      Top = 16
      Width = 139
      Height = 19
      DataField = 'dDtCancel'
      DataSource = dsMaster
      TabOrder = 24
    end
    object cxButton1: TcxButton
      Left = 939
      Top = 48
      Width = 150
      Height = 29
      Caption = 'Imprimir NF-e / CF-e'
      TabOrder = 25
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFF97979764616063605F625F5E615E5E615E5D615E5D615E5D615E5D615E
        5DBAB1A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF999999FFFFFFFEFEFEFEFEFEFE
        FEFEFEFEFEFEFEFEFEFEFEFFFFFF615E5DBAB1A5FFFFFFFFFFFFD5CABCB9B0A4
        B9B0A49B9B9BFFFFFFA5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5FFFFFF615E
        5DB8B0A4B8B0A4D5CABBABA59C6666666464649D9D9DFFFFFFFDFDFDFDFDFDFD
        FDFDFDFDFDFDFDFDFDFDFDFFFFFF615E5D4848484545459D968E6969699B9B9B
        B8B8B88F8F8FE4E4E4949494949494949494949494949494949494E4E4E45B58
        57A5A5A58E918E4343436A6A6AB8B8B8B6B6B67A7A7AC1C1C1C1C1C1C1C1C1C1
        C1C1C1C1C1C1C1C1C1C1C1C1C1C153504FA3A3A335FE354444446C6C6CB8B8B8
        4742410101010202020202020101010101010101010101010101010101010101
        01464241A5A5A54646466D6D6DB8B8B8B8B8B85252526969696565656060605B
        5B5B5656565252524D4D4D484848333333A5A5A5A5A5A54747476E6E6EB8B8B8
        B8B8B85454546B6B6B6666666262625D5D5D5858585353534F4F4F4A4A4A3434
        34A5A5A5A5A5A5484848707070E7E7E7FFFFFFB7B7B7C5C5C5C0C0C0B8B8B8B2
        B2B2ABABABA3A3A39B9B9B9393936F6F6FFFFFFFCCCCCC4A4A4AACA69D6F6F6F
        6C6C6C6969696E6E6E6C6C6C6A6A6A6868686666666666666666666666665353
        535050504E4E4E9E988FFFFFFFFFFFFFFFFFFF9A9A9AF3F3F3EFEFEFEAEAEAE6
        E6E6E1E1E1DFDFDFDFDFDFDFDFDF615E5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9B9B9BF7F7F7F3F3F3EEEEEEEAEAEAE5E5E5E1E1E1DFDFDFDFDFDF615E
        5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9D9D9DFBFBFBF7F7F7F2F2F2EE
        EEEEEAEAEAE5E5E5E1E1E1DFDFDF615E5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF615E
        5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1A1A19F9F9F9C9C9C9A9A9A96
        96969494949191918F8F8F8C8C8C8C8C8CFFFFFFFFFFFFFFFFFF}
      LookAndFeel.NativeStyle = True
    end
    object cxButton2: TcxButton
      Left = 939
      Top = 16
      Width = 150
      Height = 29
      BiDiMode = bdLeftToRight
      Caption = 'Romaneio'
      ParentBiDiMode = False
      TabOrder = 26
      OnClick = cxButton2Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFF97979764616063605F625F5E615E5E615E5D615E5D615E5D615E5D615E
        5DBAB1A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF999999FFFFFFFEFEFEFEFEFEFE
        FEFEFEFEFEFEFEFEFEFEFEFFFFFF615E5DBAB1A5FFFFFFFFFFFFD5CABCB9B0A4
        B9B0A49B9B9BFFFFFFA5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5FFFFFF615E
        5DB8B0A4B8B0A4D5CABBABA59C6666666464649D9D9DFFFFFFFDFDFDFDFDFDFD
        FDFDFDFDFDFDFDFDFDFDFDFFFFFF615E5D4848484545459D968E6969699B9B9B
        B8B8B88F8F8FE4E4E4949494949494949494949494949494949494E4E4E45B58
        57A5A5A58E918E4343436A6A6AB8B8B8B6B6B67A7A7AC1C1C1C1C1C1C1C1C1C1
        C1C1C1C1C1C1C1C1C1C1C1C1C1C153504FA3A3A335FE354444446C6C6CB8B8B8
        4742410101010202020202020101010101010101010101010101010101010101
        01464241A5A5A54646466D6D6DB8B8B8B8B8B85252526969696565656060605B
        5B5B5656565252524D4D4D484848333333A5A5A5A5A5A54747476E6E6EB8B8B8
        B8B8B85454546B6B6B6666666262625D5D5D5858585353534F4F4F4A4A4A3434
        34A5A5A5A5A5A5484848707070E7E7E7FFFFFFB7B7B7C5C5C5C0C0C0B8B8B8B2
        B2B2ABABABA3A3A39B9B9B9393936F6F6FFFFFFFCCCCCC4A4A4AACA69D6F6F6F
        6C6C6C6969696E6E6E6C6C6C6A6A6A6868686666666666666666666666665353
        535050504E4E4E9E988FFFFFFFFFFFFFFFFFFF9A9A9AF3F3F3EFEFEFEAEAEAE6
        E6E6E1E1E1DFDFDFDFDFDFDFDFDF615E5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9B9B9BF7F7F7F3F3F3EEEEEEEAEAEAE5E5E5E1E1E1DFDFDFDFDFDF615E
        5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9D9D9DFBFBFBF7F7F7F2F2F2EE
        EEEEEAEAEAE5E5E5E1E1E1DFDFDF615E5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF615E
        5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1A1A19F9F9F9C9C9C9A9A9A96
        96969494949191918F8F8F8C8C8C8C8C8CFFFFFFFFFFFFFFFFFF}
      LookAndFeel.NativeStyle = True
    end
    object cxButton3: TcxButton
      Left = 939
      Top = 144
      Width = 150
      Height = 29
      Caption = 'Enviar NFe Email'
      TabOrder = 27
      OnClick = cxButton3Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000AD919A93897F
        937F7F93766C7F6C6C7F636C7F63596C59596C59596C59596C59596C59596C59
        596C59596C59596C5959978181EBE0D5FFFFEBFFF6D5FFEBBFFFE0BFFFD5A8FF
        D5A8FFD5A8EBCAA8EBCAA8FFBFA8FFCAA8FFCAA8D5A8914C3434978181A89191
        EBEBEBFFFFFFFFFFFFFFF6EBFFF6EBFFF6EBFFF6D5FFEBD5FFEBD5FFEBD5FFE0
        BFEBBFA891634C4C3434978181EBF6EBA89D91EBE0D5FFFFFFFFFFFFFFFFFFFF
        FFEBFFF6EBFFF6EBFFF6D5FFEBD5EBBFA8916363EBB37A4C3434978C81FFFFFF
        FFF6EBA8867AEBE0D5FFFFFFFFFFFFFFFFFFFFFFFFFFF6EBFFF6D5D5B3A87A57
        4CEBE0BFFFEBBF634034978C81FFFFFFEBE0D5BFA8A8BFA8A8EBCABFEBD5D5EB
        D5D5EBD5BFEBE0D5D5B3A8A8867A917A7AEBBFA8FFEBD563404CAD8C81EBE0D5
        BFA891EBEBEBEBEBEBA89D91A89D91A89191A89191A8867AA89191EBE0BFEBD5
        BF917A63EBB391634C4CAD979791917AFFF6EBD5A8A8BFA891BF917AA8867AA8
        6F63A86F63A86F6391634C914C34914C34EBEBD563574C634C4CAD8C97916F63
        FFFFFFFFF6FFFFF6FFFFF6FFFFF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF342013634C4C938383916F63FFFFFFD5A8A8BFA891BF917AA8867AA8
        6F63A86F63A86F6391634C914C34914C34FFFFFF3420136C5454CECECE916F63
        FFFFFFFFF6FFFFF6FFFFF6FFFFF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF342013CECECECECECE916F63FFFFFF34637A205763204C63134C6313
        4063063463062863002063001963001363FFFFFF342013CECECECECECE916F63
        FFFFFF4C7A91A8E0FF91E0FF91D5FF7ABFFF63B3FF4CA8EB3491EB2086EB0013
        63FFFFFF342013CECECECECECE916F63FFFFFF4C7A914C6F914C6F9134637A34
        577A204C7A13407A133463062863062863FFFFFF342013CECECECECECE917A63
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFF342013CECECECECECE917A63916F63916F63916F63916F63916F6391
        634C91634C91634C91634C91634C91634C91634C91634CCECECE}
      LookAndFeel.NativeStyle = True
    end
    object DBEdit6: TDBEdit
      Tag = 1
      Left = 88
      Top = 40
      Width = 401
      Height = 19
      DataField = 'cChaveNFe'
      DataSource = dsMaster
      TabOrder = 28
    end
    object DBEdit30: TDBEdit
      Tag = 1
      Left = 585
      Top = 40
      Width = 139
      Height = 19
      DataField = 'cNrProtocoloCancNFe'
      DataSource = dsMaster
      TabOrder = 29
    end
    object DBEdit20: TDBEdit
      Tag = 1
      Left = 88
      Top = 256
      Width = 81
      Height = 19
      DataField = 'nValFrete'
      DataSource = dsMaster
      TabOrder = 30
    end
    object DBEdit27: TDBEdit
      Tag = 1
      Left = 248
      Top = 256
      Width = 81
      Height = 19
      DataField = 'nValDespesa'
      DataSource = dsMaster
      TabOrder = 31
    end
    object DBEdit28: TDBEdit
      Tag = 1
      Left = 792
      Top = 256
      Width = 81
      Height = 19
      DataField = 'nValIPI'
      DataSource = dsMaster
      TabOrder = 32
    end
    object DBEdit31: TDBEdit
      Tag = 1
      Left = 608
      Top = 256
      Width = 81
      Height = 19
      DataField = 'nValBaseIPI'
      DataSource = dsMaster
      TabOrder = 33
    end
    object DBEdit32: TDBEdit
      Tag = 1
      Left = 416
      Top = 280
      Width = 81
      Height = 19
      DataField = 'nValPIS'
      DataSource = dsMaster
      TabOrder = 34
    end
    object DBEdit33: TDBEdit
      Tag = 1
      Left = 608
      Top = 280
      Width = 81
      Height = 19
      DataField = 'nValCOFINS'
      DataSource = dsMaster
      TabOrder = 35
    end
    object DBEdit34: TDBEdit
      Tag = 1
      Left = 416
      Top = 256
      Width = 81
      Height = 19
      DataField = 'nValSeguro'
      DataSource = dsMaster
      TabOrder = 36
    end
    object cxButton4: TcxButton
      Left = 939
      Top = 112
      Width = 150
      Height = 29
      Caption = 'Hist'#243'rico CC-e'
      TabOrder = 37
      OnClick = cxButton4Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        E2C0AACC8D66C07140BC6B36BC6B36BC6B36BC6A36BC6A36BB6A35BB6A35BB69
        35BD6E3BCA8B63E3C2AEFFFFFFFFFFFFC57C4DF8F2EBF7ECDFF6EBDEF6EADEF6
        EADCF6EADCFAF3EBFAF3EBFAF2EAFCF7F3FCF8F4FEFEFDC37A4DFFFFFFFFFFFF
        C27740F5EBDFFDBF68FCBD67FBBE65FCBE64FCBE64FCBD62FBBD63FBBC61FCBE
        60FCBC62FDFBF8BC6B37FFFFFFFFFFFFC37C42F7EDE3FDC26EFFD8A0FFD79EFF
        D69BFFD798FFD696FFD695FFD594FFD493FBBE65FBF7F4BD6C375E5A56635E59
        4C3F3258544FB686423D3020413524372B1B3B2C15F7B352F7B352F7B251F7B2
        4FF7B24FFCF9F5C1743C635B54A79A8D70675E453F3AAD9C915C554E968A7F66
        5E561B1918FCE4D1FCE2CEFCE2CCFBE0C9FBE1C8FDFAF7C37A4171685FC1B6AB
        797066625C56BAA99C6E665DBDB1A57E746A332E2BFDE5D3FCE4D1FCE2CDFBE1
        CBFBE1C9FBF7F2C78045797067C4B9AFA79A8D807A73D6C3B46C635CBBAFA4A6
        988B534D46FDE5D3FBE4D0FBE3CCFADFC7FADFC6FAF2EAC88448706861696158
        564F484D49468C8077403B3746423C3D38332C2825FCE4D1FBE1CCFAE0C7F9DD
        C3F8DCC2FAF4EDC8864B9A98955B544DB0A090695D5186746563584EAA98885A
        514A85796EFBE1CCFADFC7F8DCC2F6DABDF6D8BBFAF4EFC8874CF3F2F06D665E
        85786F4A413B95867A564A409283744F4740E4CFBBFAE0C8F8DCC2F5D6BBF3D4
        B5F1D2B3F8F4F0C6864CF7F6F6C5BFB89C8F83524C47CBB7A7544D479085799A
        8C7FF5DDC6F9DDC4F6D9BCF4E9DFF7F2ECFBF7F3F5EFE9C38048FFFFFFF7F7F6
        776D62756D65DDC8B56F665E6C6259FBE2CBF9E0C8F8DCC2F5D6BAFDFBF8FCE6
        CDFAE5C9E2B684D5A884FFFFFFFFFFFFCA925AFAF6F2FAE0C7FBE1C9FBE2C9FB
        E0C8F9DFC5F8DBC1F4D6B8FFFBF8F6D8B4E1B07DDC9669FDFBFAFFFFFFFFFFFF
        D2A274F8F3EDF8F4EEF8F4EDF8F3EDF8F3EDF8F3EDF8F2ECF7F2ECF2E6D7E2B2
        7DDC986BFDFBFAFFFFFFFFFFFFFFFFFFE8CEB9D7AA7CCC945BCA9055CA9055CA
        9055CA9155CB9055C98F55CF9D69DDB190FDFBFAFFFFFFFFFFFF}
      LookAndFeel.NativeStyle = True
    end
    object cxButton5: TcxButton
      Left = 939
      Top = 80
      Width = 150
      Height = 29
      Caption = 'Imprimir CC-e'
      TabOrder = 38
      OnClick = cxButton5Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFF97979764616063605F625F5E615E5E615E5D615E5D615E5D615E5D615E
        5DBAB1A5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF999999FFFFFFFEFEFEFEFEFEFE
        FEFEFEFEFEFEFEFEFEFEFEFFFFFF615E5DBAB1A5FFFFFFFFFFFFD5CABCB9B0A4
        B9B0A49B9B9BFFFFFFA5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5FFFFFF615E
        5DB8B0A4B8B0A4D5CABBABA59C6666666464649D9D9DFFFFFFFDFDFDFDFDFDFD
        FDFDFDFDFDFDFDFDFDFDFDFFFFFF615E5D4848484545459D968E6969699B9B9B
        B8B8B88F8F8FE4E4E4949494949494949494949494949494949494E4E4E45B58
        57A5A5A58E918E4343436A6A6AB8B8B8B6B6B67A7A7AC1C1C1C1C1C1C1C1C1C1
        C1C1C1C1C1C1C1C1C1C1C1C1C1C153504FA3A3A335FE354444446C6C6CB8B8B8
        4742410101010202020202020101010101010101010101010101010101010101
        01464241A5A5A54646466D6D6DB8B8B8B8B8B85252526969696565656060605B
        5B5B5656565252524D4D4D484848333333A5A5A5A5A5A54747476E6E6EB8B8B8
        B8B8B85454546B6B6B6666666262625D5D5D5858585353534F4F4F4A4A4A3434
        34A5A5A5A5A5A5484848707070E7E7E7FFFFFFB7B7B7C5C5C5C0C0C0B8B8B8B2
        B2B2ABABABA3A3A39B9B9B9393936F6F6FFFFFFFCCCCCC4A4A4AACA69D6F6F6F
        6C6C6C6969696E6E6E6C6C6C6A6A6A6868686666666666666666666666665353
        535050504E4E4E9E988FFFFFFFFFFFFFFFFFFF9A9A9AF3F3F3EFEFEFEAEAEAE6
        E6E6E1E1E1DFDFDFDFDFDFDFDFDF615E5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9B9B9BF7F7F7F3F3F3EEEEEEEAEAEAE5E5E5E1E1E1DFDFDFDFDFDF615E
        5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9D9D9DFBFBFBF7F7F7F2F2F2EE
        EEEEEAEAEAE5E5E5E1E1E1DFDFDF615E5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFF9F9F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF615E
        5DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA1A1A19F9F9F9C9C9C9A9A9A96
        96969494949191918F8F8F8C8C8C8C8C8CFFFFFFFFFFFFFFFFFF}
      LookAndFeel.NativeStyle = True
    end
    object DBEdit35: TDBEdit
      Tag = 1
      Left = 339
      Top = 112
      Width = 203
      Height = 19
      DataField = 'cNmUsuario'
      DataSource = dsUsuario
      TabOrder = 39
    end
    object DBEdit36: TDBEdit
      Tag = 1
      Left = 792
      Top = 40
      Width = 137
      Height = 19
      DataField = 'cJustCancelamento'
      DataSource = dsMaster
      TabOrder = 40
    end
    object checkCancForaPrazo: TDBCheckBox
      Left = 791
      Top = 18
      Width = 125
      Height = 17
      Caption = 'Canc. Fora do Prazo'
      DataField = 'cFlgCancForaPrazo'
      DataSource = dsMaster
      ReadOnly = True
      TabOrder = 41
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBEdit37: TDBEdit
      Tag = 1
      Left = 157
      Top = 64
      Width = 772
      Height = 19
      DataField = 'cNmStatusRetorno'
      DataSource = dsMaster
      TabOrder = 42
    end
    object DBEdit38: TDBEdit
      Tag = 1
      Left = 88
      Top = 64
      Width = 65
      Height = 19
      DataField = 'iStatusRetorno'
      DataSource = dsMaster
      TabOrder = 43
    end
    object DBEdit39: TDBEdit
      Tag = 1
      Left = 88
      Top = 136
      Width = 65
      Height = 19
      DataField = 'nCdTipoDoctoFiscal'
      DataSource = dsMaster
      TabOrder = 44
    end
    object DBEdit40: TDBEdit
      Tag = 1
      Left = 158
      Top = 136
      Width = 715
      Height = 19
      DataField = 'cNmTipoDoctoFiscal'
      DataSource = dsTipoDoctoFiscal
      TabOrder = 45
    end
    object cxButton6: TcxButton
      Left = 939
      Top = 176
      Width = 150
      Height = 29
      Caption = 'Imprimir Etiqueta '
      TabOrder = 46
      OnClick = cxButton6Click
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D0020000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEDEDEDB0B0B0B6B6B6F4F4F4FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEEEEB4
        B4B4E0E0E0D8D8D8B6B6B6F4F4F4FFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFEEEEEEB7B7B7E2E2E2FDFDFDFDFDFDD9D9D9B6B6B6F4F4
        F4FFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFEFEFEFBBBBBBE4E4E4FD
        FDFDF9F7F3000000FDFBF7D9D9D9B7B7B7F4F4F4FFFFFF000000FFFFFFFFFFFF
        FFFFFFF0F0F0BEBEBEE5E5E5FDFDFDF9F7F3000000F9F7F3000000F9F7F3D9D9
        D9B7B7B7F4F4F4000000FFFFFFFFFFFFF1F1F1C2C2C2E7E7E7FFFFFFFFFFFF00
        0000F9F7F3000000F9F7F3000000FDFCFBD9D9D9B7B7B7000000FFFFFFF2F2F2
        C5C5C5E8E8E8FDFDFDFFCC83FDCD88FFFFFF000000F9F7F3000000F9F7F3F9F7
        F3D7D7D7B5B5B5000000F2F2F2C9C9C9E9E9E9FDFDFDFFCC83FFCC83FFD498FF
        D79EFFFFFF000000F9F7F3FDFDFDD9D9D9B9B9B9F9F9F9000000CDCDCDEBEBEB
        FDFDFDFAFAFAFBF3E7FECE89FFD496FFD59AFFCF8BFFFFFFFDFDFDDADADABDBD
        BDF9F9F9FFFFFF000000CDCDCDFDFDFDFDFDFDFCFCFCF7F7F7FDF5EAFECF8AFF
        CC83FFCC83FDFDFDDCDCDCC0C0C0F9F9F9FFFFFFFFFFFF000000CECECEFDFDFD
        E0E0E0CBCBCBCECECEF7F7F7FBF3E8FFCC83FDFDFDDEDEDEC3C3C3FAFAFAFFFF
        FFFFFFFFFFFFFF000000D0D0D0FDFDFDCDCDCDFFFFFFD4D4D4F3F3F3FBFBFBFD
        FDFDE0E0E0C7C7C7FAFAFAFFFFFFFFFFFFFFFFFFFFFFFF000000D2D2D2FDFDFD
        E2E2E2CECECEE0E0E0FDFDFDFDFDFDE2E2E2CBCBCBFAFAFAFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000D3D3D3FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDE4E4E4CD
        CDCDFAFAFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000D5D5D5D4D4D4
        D2D2D2D1D1D1D0D0D0CECECECDCDCDFBFBFBFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 337
    Width = 1110
    Height = 240
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 240
    ClientRectRight = 1110
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'Itens '
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1110
        Height = 217
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsItem
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        ParentShowHint = False
        ReadOnly = True
        ShowHint = False
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <
              item
                FieldName = 'nCdPedido'
                ValueType = fvtCount
              end>
            Width = 48
          end
          item
            EditButtons = <>
            FieldName = 'nCdItemPedido'
            Footers = <
              item
                FieldName = 'nCdItemPedido'
              end>
            Width = 46
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <
              item
                FieldName = 'nCdProduto'
              end>
            Width = 41
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            Width = 100
          end
          item
            EditButtons = <>
            FieldName = 'cUnidadeMedida'
            Footers = <>
            Width = 27
          end
          item
            EditButtons = <>
            FieldName = 'cNCMSH'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cCFOP'
            Footers = <>
            Width = 34
          end
          item
            EditButtons = <>
            FieldName = 'nQtde'
            Footers = <
              item
                DisplayFormat = '0'
                FieldName = 'nQtde'
                Value = '0'
                ValueType = fvtSum
              end>
            Width = 44
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footers = <
              item
                DisplayFormat = '0.0000#,######'
                FieldName = 'nValUnitario'
                Value = '0'
                ValueType = fvtSum
              end>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nValDesconto'
            Footers = <>
            Width = 48
          end
          item
            EditButtons = <>
            FieldName = 'nValAcrescimo'
            Footers = <>
            Width = 48
          end
          item
            EditButtons = <>
            FieldName = 'nValTotal'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cCdST'
            Footers = <>
            Title.Caption = 'ICMS Pr'#243'prio|CST CSOSN'
            Width = 40
          end
          item
            EditButtons = <>
            FieldName = 'nValBaseICMS'
            Footers = <>
            Width = 55
          end
          item
            EditButtons = <>
            FieldName = 'nAliqICMS'
            Footers = <>
            Width = 36
          end
          item
            EditButtons = <>
            FieldName = 'nValICMS'
            Footers = <>
            Width = 49
          end
          item
            EditButtons = <>
            FieldName = 'cCdSTIPI'
            Footers = <>
            Width = 25
          end
          item
            EditButtons = <>
            FieldName = 'nValBaseIPI'
            Footers = <>
            Width = 55
          end
          item
            EditButtons = <>
            FieldName = 'nAliqIPI'
            Footers = <>
            Width = 36
          end
          item
            EditButtons = <>
            FieldName = 'nValIPI'
            Footers = <>
            Width = 45
          end
          item
            EditButtons = <>
            FieldName = 'nPercIVA'
            Footers = <>
            Width = 37
          end
          item
            EditButtons = <>
            FieldName = 'nPercCargaMediaST'
            Footers = <>
            Width = 46
          end
          item
            EditButtons = <>
            FieldName = 'nAliqICMSInternaOrigem'
            Footers = <>
            Width = 55
          end
          item
            EditButtons = <>
            FieldName = 'nAliqICMSInternaDestino'
            Footers = <>
            Width = 49
          end
          item
            EditButtons = <>
            FieldName = 'nValBaseICMSSub'
            Footers = <>
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'nValICMSSub'
            Footers = <>
            Width = 44
          end
          item
            EditButtons = <>
            FieldName = 'nValIcentivoFiscal'
            Footers = <>
            Width = 55
          end
          item
            EditButtons = <>
            FieldName = 'nValDespesa'
            Footers = <>
            Width = 48
          end
          item
            EditButtons = <>
            FieldName = 'nValFrete'
            Footers = <>
            Width = 48
          end
          item
            EditButtons = <>
            FieldName = 'nValSeguro'
            Footers = <>
            Width = 48
          end
          item
            EditButtons = <>
            FieldName = 'cCSTPIS'
            Footers = <>
            Width = 25
          end
          item
            EditButtons = <>
            FieldName = 'nValBasePIS'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nAliqPIS'
            Footers = <>
            Width = 36
          end
          item
            EditButtons = <>
            FieldName = 'nValPIS'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cCSTCOFINS'
            Footers = <>
            Width = 25
          end
          item
            EditButtons = <>
            FieldName = 'nValBaseCOFINS'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nAliqCOFINS'
            Footers = <>
            Width = 36
          end
          item
            EditButtons = <>
            FieldName = 'nValCOFINS'
            Footers = <>
            Width = 58
          end
          item
            EditButtons = <>
            FieldName = 'nValImpostoAproxFed'
            Footers = <>
            Title.Caption = 'Imposto Aproximado | Val. Federal'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nValImpostoAproxEst'
            Footers = <>
            Title.Caption = 'Imposto Aproximado | Val. Estadual'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nValImpostoAproxMun'
            Footers = <>
            Title.Caption = 'Imposto Aproximado | Val. Municipal'
            Width = 65
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    CursorType = ctStatic
    AfterOpen = qryMasterAfterOpen
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM DoctoFiscal'
      ' WHERE nCdDoctoFiscal    = :nPK')
    Left = 344
    Top = 392
    object qryMasternCdDoctoFiscal: TAutoIncField
      FieldName = 'nCdDoctoFiscal'
      ReadOnly = True
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasternCdSerieFiscal: TIntegerField
      FieldName = 'nCdSerieFiscal'
    end
    object qryMastercFlgEntSai: TStringField
      FieldName = 'cFlgEntSai'
      FixedChar = True
      Size = 1
    end
    object qryMastercModelo: TStringField
      FieldName = 'cModelo'
      FixedChar = True
      Size = 2
    end
    object qryMastercCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryMastercTextoCFOP: TStringField
      FieldName = 'cTextoCFOP'
      Size = 50
    end
    object qryMasteriNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryMasternCdTipoDoctoFiscal: TIntegerField
      FieldName = 'nCdTipoDoctoFiscal'
    end
    object qryMasterdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryMasterdDtSaida: TDateTimeField
      FieldName = 'dDtSaida'
    end
    object qryMasterdDtImpressao: TDateTimeField
      FieldName = 'dDtImpressao'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMastercNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMastercCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryMastercIE: TStringField
      FieldName = 'cIE'
      Size = 14
    end
    object qryMastercTelefone: TStringField
      FieldName = 'cTelefone'
    end
    object qryMasternCdEndereco: TIntegerField
      FieldName = 'nCdEndereco'
    end
    object qryMastercEndereco: TStringField
      FieldName = 'cEndereco'
      Size = 50
    end
    object qryMasteriNumero: TIntegerField
      Alignment = taLeftJustify
      FieldName = 'iNumero'
    end
    object qryMastercBairro: TStringField
      FieldName = 'cBairro'
      Size = 35
    end
    object qryMastercCidade: TStringField
      FieldName = 'cCidade'
      Size = 35
    end
    object qryMastercCep: TStringField
      FieldName = 'cCep'
      FixedChar = True
      Size = 8
    end
    object qryMastercUF: TStringField
      FieldName = 'cUF'
      FixedChar = True
      Size = 2
    end
    object qryMasternValTotal: TBCDField
      FieldName = 'nValTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValProduto: TBCDField
      FieldName = 'nValProduto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValBaseICMS: TBCDField
      FieldName = 'nValBaseICMS'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValICMS: TBCDField
      FieldName = 'nValICMS'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValBaseICMSSub: TBCDField
      FieldName = 'nValBaseICMSSub'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValICMSSub: TBCDField
      FieldName = 'nValICMSSub'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValIsenta: TBCDField
      FieldName = 'nValIsenta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValOutras: TBCDField
      FieldName = 'nValOutras'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValDespesa: TBCDField
      FieldName = 'nValDespesa'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValFrete: TBCDField
      FieldName = 'nValFrete'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryMasternCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMastercFlgFaturado: TIntegerField
      FieldName = 'cFlgFaturado'
    end
    object qryMasternCdTerceiroTransp: TIntegerField
      FieldName = 'nCdTerceiroTransp'
    end
    object qryMastercNmTransp: TStringField
      FieldName = 'cNmTransp'
      Size = 50
    end
    object qryMastercCNPJTransp: TStringField
      FieldName = 'cCNPJTransp'
      Size = 14
    end
    object qryMastercIETransp: TStringField
      FieldName = 'cIETransp'
      Size = 14
    end
    object qryMastercEnderecoTransp: TStringField
      FieldName = 'cEnderecoTransp'
      Size = 50
    end
    object qryMastercBairroTransp: TStringField
      FieldName = 'cBairroTransp'
      Size = 35
    end
    object qryMastercCidadeTransp: TStringField
      FieldName = 'cCidadeTransp'
      Size = 35
    end
    object qryMastercUFTransp: TStringField
      FieldName = 'cUFTransp'
      FixedChar = True
      Size = 2
    end
    object qryMasteriQtdeVolume: TIntegerField
      FieldName = 'iQtdeVolume'
    end
    object qryMasternPesoBruto: TBCDField
      FieldName = 'nPesoBruto'
      Precision = 12
      Size = 2
    end
    object qryMasternPesoLiq: TBCDField
      FieldName = 'nPesoLiq'
      Precision = 12
      Size = 2
    end
    object qryMastercPlaca: TStringField
      FieldName = 'cPlaca'
      Size = 10
    end
    object qryMastercUFPlaca: TStringField
      FieldName = 'cUFPlaca'
      FixedChar = True
      Size = 2
    end
    object qryMasternValIPI: TBCDField
      FieldName = 'nValIPI'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValFatura: TBCDField
      FieldName = 'nValFatura'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryMasternCdIncoterms: TIntegerField
      FieldName = 'nCdIncoterms'
    end
    object qryMasternCdUsuarioImpr: TIntegerField
      FieldName = 'nCdUsuarioImpr'
    end
    object qryMasternCdTerceiroPagador: TIntegerField
      FieldName = 'nCdTerceiroPagador'
    end
    object qryMasterdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryMasternCdUsuarioCancel: TIntegerField
      FieldName = 'nCdUsuarioCancel'
    end
    object qryMasternCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryMastercFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryMasternValCredAdiant: TBCDField
      FieldName = 'nValCredAdiant'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValIcentivoFiscal: TBCDField
      FieldName = 'nValIcentivoFiscal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercNrProtocoloNFe: TStringField
      FieldName = 'cNrProtocoloNFe'
      Size = 100
    end
    object qryMastercNrReciboNFe: TStringField
      FieldName = 'cNrReciboNFe'
      Size = 100
    end
    object qryMastercChaveNFe: TStringField
      FieldName = 'cChaveNFe'
      Size = 100
    end
    object qryMastercNrProtocoloCancNFe: TStringField
      FieldName = 'cNrProtocoloCancNFe'
      Size = 100
    end
    object qryMastercNrProtocoloDPEC: TStringField
      FieldName = 'cNrProtocoloDPEC'
      Size = 100
    end
    object qryMasternCdTabTipoEmissaoDoctoFiscal: TIntegerField
      FieldName = 'nCdTabTipoEmissaoDoctoFiscal'
    end
    object qryMastercSerie: TStringField
      FieldName = 'cSerie'
      FixedChar = True
      Size = 3
    end
    object qryMasternValBaseIPI: TBCDField
      FieldName = 'nValBaseIPI'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValPIS: TBCDField
      FieldName = 'nValPIS'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValCOFINS: TBCDField
      FieldName = 'nValCOFINS'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValSeguro: TBCDField
      FieldName = 'nValSeguro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercXMLNFe: TMemoField
      FieldName = 'cXMLNFe'
      BlobType = ftMemo
    end
    object qryMastercJustCancelamento: TStringField
      FieldName = 'cJustCancelamento'
      Size = 255
    end
    object qryMastercFlgCancForaPrazo: TIntegerField
      FieldName = 'cFlgCancForaPrazo'
    end
    object qryMasteriStatusRetorno: TIntegerField
      FieldName = 'iStatusRetorno'
    end
    object qryMastercNmStatusRetorno: TStringField
      FieldName = 'cNmStatusRetorno'
      Size = 150
    end
    object qryMastercFlgAmbienteHom: TIntegerField
      FieldName = 'cFlgAmbienteHom'
    end
    object qryMastercArquivoXML: TStringField
      FieldName = 'cArquivoXML'
      Size = 200
    end
    object qryMastercFlgCFe: TIntegerField
      FieldName = 'cFlgCFe'
    end
  end
  inherited dsMaster: TDataSource
    Left = 344
    Top = 424
  end
  inherited qryID: TADOQuery
    Left = 312
    Top = 392
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 280
    Top = 392
  end
  inherited qryStat: TADOQuery
    Left = 312
    Top = 424
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 280
    Top = 424
  end
  inherited ImageList1: TImageList
    Left = 408
    Top = 424
    Bitmap = {
      494C01010E001300040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000005000000001002000000000000050
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E2E2E200CBCB
      CB00C9C9C900C9C9C900C9C9C900C9C9C900C9C9C900C9C9C900C9C9C900C9C9
      C900C9C9C900C9C9C900CCCCCC00E2E2E2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CBCBCB00F9F9
      F900FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFC
      FC00FCFCFC00FCFCFC00F9F9F900CCCCCC000000000061BE6D005DB8680058B1
      620053A95C004DA1560047994F00419149003B88420035803B0035803B003580
      3B0035803B0035803B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006D71DF005055D9008587CA00FCFC
      FC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFC
      FC00FCFCFC00FCFCFC00FCFCFC00C9C9C9000000000065C37100A0D7A9009CD5
      A50098D3A10094D09D0090CE98008BCB930087C98E0082C689007EC384007AC1
      800076BE7C0035803B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C5C7F3008185E5003D42CC00C9CB
      F200FCFCFC00FCFCFC00FCFCFC00FCFCFC00FBFBFB00FBFBFB00FBFBFB00FBFB
      FB00FBFBFB00FBFBFB00FCFCFC00C9C9C9000000000068C77400A5DAAE00A2D8
      AB009ED6A7009AD4A30096D29F0093CF9A008ECC950089CA900085C78B0081C5
      87007DC2820035803B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFF0FB00878ACC004F55
      DB00EDEEF900FCFCFC00FCFCFC00FBFBFB00FBFBFB00FAFAFA00FAFAFA00E8E9
      F700F3F3F900FAFAFA00FCFCFC00C9C9C9000000000068C7740068C7740065C3
      710061BE6D005DB8680058B1620053A95C004DA1560047994F00419149003580
      3B0035803B0035803B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE00C9C9C900777D
      E4007A81E500B4B8EF009FA3EB00C0C3F100D3D5F4008187E600656CE100767C
      E4009BA0E900F8F8F800FCFCFC00C9C9C9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE00C9C9C900E7E9
      F9004C55DF00E4E5F800D8DAF600666EE4002D38DA004650DF005D65E2007279
      E500A7ABED00F8F8F800FCFCFC00C9C9C9000000000000000000000000000000
      0000000000003DA56F0037A36B00319F6300299B5B0022975200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE00C9C9C900FCFC
      FC006D75E700979CED00DBDDF7006069E500DADCF700FCFCFC00FAFAFA00F9F9
      F900F6F6F600F6F6F600FCFCFC00C9C9C9000000000000000000000000000000
      00000000000039A46E0096CEB00094CDAD0090CAA9001C914A00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE00C9C9C900FCFC
      FC00DCDEF800535EE600606AE800D4D6F700FCFCFC00FBFBFB00F8F8F800F6F6
      F600F3F3F300F2F2F200FCFCFC00C9C9C9000000000000000000000000000000
      00000000000035A2680094CDAD006FBA8E008DC8A500168F4400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE00C9C9C900FCFC
      FC00FCFCFC005D68EA006C76EC00FCFCFC00FBFBFB00F8F8F800F5F5F500F2F2
      F200EFEFEF00EDEDED00FCFCFC00C9C9C9000000000000000000000000000000
      000000000000319E620091CBAA006BB889008AC6A100108B3C00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE00C9C9C900FCFC
      FC00FBFBFB00747FEE00A6ADF300FBFBFB00F8F8F800F5F5F500F1F1F100ECEC
      EC00EAEAEA00E6E6E600FCFCFC00C9C9C9000000000000000000000000003FA7
      710039A3690055AF7C0090CBA80066B6850088C59E003A9F5E000B8833000A82
      2A00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE00C9C9C900FCFC
      FC00F9F9F9006976EE009EA6F200F7F7F700F6F6F600F2F2F200EBEBEB00FCFC
      FC00FCFCFC00FCFCFC00FCFCFC00C9C9C900000000000000000000000000319F
      65005AB3810091CBAA0074BC900061B380006AB6850080C1960043A15F00037B
      1E00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FEFEFE00C9C9C900FCFC
      FC00F2F3F700737FF100CCD0F500F7F7F700F3F3F300F0F0F000EAEAEA00FCFC
      FC00F6F6F600F4F4F400C5C5C500DFDFDF000000000000000000000000000000
      0000289857004FAB74008AC7A10067B5820082C2970046A362001B8937000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000C9C9C900FBFB
      FB00F1F1F4007884F100B9BFF300F5F5F500F1F1F100EFEFEF00E9E9E900FCFC
      FC00E7E7E700C3C3C300DFDFDF00FDFDFD000000000000000000000000000000
      0000000000001990460046A5680083C2980048A5660018893400000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00F8F8
      F800FBFBFB00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00F8F8
      F800C2C2C200DFDFDF00FDFDFD00000000000000000000000000000000000000
      00000000000000000000098837003CA05C001588330000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E3E3E300CCCC
      CC00C9C9C900C9C9C900C9C9C900C9C9C900C9C9C900C9C9C900C9C9C900C9C9
      C900DFDFDF00FDFDFD0000000000000000000000000000000000000000000000
      0000000000000000000000000000038128000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DEBDBD00E7CE
      CE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E2E2E200CBCB
      CB00C9C9C900C9C9C900C9C9C900C9C9C900C9C9C900C9C9C900C9C9C900C9C9
      C900C9C9C900C9C9C900CCCCCC00E2E2E2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFBD9C00EFB5
      9C00D6A59C00D6A59C00DEBDBD00E7CECE00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CBCBCB00F9F9
      F900FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFC
      FC00FCFCFC00FCFCFC00F9F9F900CCCCCC000000000000000000000000009E9E
      9E00656464006564640065646400656464006564640065646400656464006564
      6400898A8900B9BABA00000000000000000000000000EFBD9C00D6A59C00FFDE
      AD00FFD6A500FFD6A500F7C69C00EFB59C00D6A59C0000000000DEB5B500E7CE
      CE00FFFFFF00FFFFFF00000000000000000000000000F1F1F100E6E6E600E1E1
      E100E0E0E000DEDEDE00DDDDDD00DBDBDB00DADADA00D9D9D900D7D7D700D6D6
      D600D5D5D500DADADA00E9E9E900FFFFFF0000000000FEFEFE00C9C9C900FCFC
      FC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFC
      FC00FCFCFC00FCFCFC00FCFCFC00C9C9C9000000000000000000D5D5D5005157
      570031333200484E4E00313332003D4242003D4242003D4242003D424200191D
      230051575700C6C6C600000000000000000000000000FFDEAD00E7B5A500EFCE
      AD00FFEFBD00FFEFB500FFEFB500FFDEAD00FFD6A500FFD6A500F7C69C00EFB5
      9C0000000000000000000000000000000000F4F4F4007898B4001C5D95001F60
      98001C5D95001B5C96001B5B95001A5B95001A5994001A5994001A5994001959
      9300195892001958910063819E00FFFFFF0000000000FEFEFE00C9C9C900FCFC
      FC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FBFBFB00FBFBFB00FBFBFB00FBFB
      FB00FBFBFB00FBFBFB00FCFCFC00C9C9C90000000000000000008C908F00191D
      230031333200313332003133320031333200313332003133320031333200191D
      230004050600898A8900000000000000000000000000FFF7C600FFEFBD00D6A5
      9C00F7EFC600FFFFCE00FFFFCE00FFF7C600FFEFBD00FFEFB500FFEFB500EFC6
      A50000000000000000000000000000000000EFEFEF001F6098003AA9D9001F60
      98003AA9D90046C6F30044C4F30042C4F30042C3F30042C3F40041C3F40041C2
      F40040C2F40034A3D9001A5A9200FFFFFF0000000000FEFEFE00C9C2BF00E195
      6500E0905500FCFCFC00E39A5900E3996700F8F1EE00FAFAFA00FAFAFA00FAFA
      FA00FAFAFA00FAFAFA00FCFCFC00C9C9C900000000000000000065646400898A
      8900DADDD700B9BABA00BEBEBE00BEBEBE00BEBEBE00BEBEBE00BEBABC00CCCC
      CC005157570065646400000000000000000000000000FFFFCE00FFFFCE00F7EF
      C600D6ADA500FFFFD600FFFFD600FFFFD600FFFFD600FFFFCE00EFD6B500D6A5
      9C00EFBD9C00000000000000000000000000F1F1F1001F649C0050CBF2001F64
      9C0050CBF2004CCAF30049C9F30047C7F30046C6F20043C5F30043C4F30042C4
      F30042C3F30041C3F3001A599400FFFFFF0000000000FBF3EE00D7905C00E5A3
      6500E1945D00FCFCFC00E5A06100E7A86800E39B6700F8F0EB00FAFAFA00FAFA
      FA00F8F8F800F8F8F800FCFCFC00C9C9C9000000000000000000717272003D42
      4200898A8900777B7B007B8585007B8585007B8585007B8585007B8585009392
      92003D424200777B7B00000000000000000000000000FFFFDE00FFFFDE00FFFF
      D600E7CEB500E7C6BD00FFFFE700FFFFE700FFFFE700F7EFCE00D6ADA500FFEF
      B500EFCEAD00000000000000000000000000F3F3F30021679E0065D4F4002167
      9E0065D4F4005BD1F30051CEF3004ECCF2004BC9F20048C9F20047C7F20045C6
      F20044C5F20043C5F2001B5C9500FFFFFF00F2CFB400E6A25A00E8AA6A00E49C
      5F00F8EBE500FCFCFC00F8EDE500E5A26200E8AA6A00E39C5700EDC5AF00F9F9
      F900F9F9F900F8F8F800FCFCFC00C9C9C9000000000000000000B6B6B6003133
      32007B858500AEAEAE00A4A8A800A4A8A800A4A8A800A4A8A800A4A8A800484E
      4E0031333200B9BABA00000000000000000000000000FFFFE700FFFFE700E7CE
      BD00C6ADAD00BDB5BD00DEBDBD00EFD6D600EFDED60000000000F7EFC600FFFF
      CE00F7E7BD00000000000000000000000000F4F4F400246CA2007FDFF600246C
      A2007FDFF60067D7F4005DD3F30058D1F20052CEF2004ECDF1004CCAF20048C9
      F10046C7F10046C7F1001D5E9800FFFFFF00E8AC6000ECB87900E5A55A00F4DF
      D100FCFCFC00FCFCFC00FCFCFC00F5E1D200E6A65B00E9B27500E49E5A00F8F5
      F400F6F6F600F6F6F600FCFCFC00C9C9C9000000000000000000D5D5D500484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00898A
      8900484E4E00C6C6C600000000000000000000000000F7E7DE00D6ADA500CECE
      CE00EFFFFF00E7FFFF00CEE7EF00C6CED600BDC6CE00BDB5BD00DEBDAD00FFFF
      D600F7E7C600000000000000000000000000F6F6F600276FA6009BE9F800276F
      A6009BE9F80074DEF50069DAF40062D7F3005BD4F20054D0F10051CEF1004ECC
      F10048C9F00049C9F0001E619A00FFFFFF00F4D5B700EAAE6000EAB57100E8A7
      6500F8EDE500FCFCFC00F9EFE600E9AE6600EAB57100E8A85D00EDC9B000F6F6
      F600F3F3F300F2F2F200FCFCFC00C9C9C9000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE007B85
      85003D4242000000000000000000000000000000000000000000BDD6E700FFFF
      FF00FFFFFF00FFFFFF00EFFFFF00EFFFFF00E7FFFF00D6FFFF00BDB5BD00DEBD
      B500F7E7CE00000000000000000000000000F8F8F8002A74AA00B3F1FB0081E4
      F7001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E
      98001D5E98001D5E980091B0C800FFFFFF0000000000FCF5EF00DFA56500EBB6
      7200E8A96500FCFCFC00EBB56A00ECBB7600EAB07000F6EFEA00F5F5F500F2F2
      F200EFEFEF00EDEDED00FCFCFC00C9C9C9000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00D5D5D5007172
      72003D424200000000000000000000000000FFFFFF00CEA5A500C6D6E700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EFFFFF00C6E7F700B5E7F7009C9C
      B500DEB5AD00000000000000000000000000F9F9F9002D7AAE00C6F7FD008EE8
      F80088E7F80080E4F60078E1F50070DEF40067DAF3005ED6F10057D2F0004ECE
      EE0064D6F2002268A000FFFFFF00FFFFFF0000000000FEFEFE00CAC5C000EAB3
      7100E8AF6200FCFCFC00ECB76700EBB77200F7F1EC00F5F5F500F1F1F100ECEC
      EC00EAEAEA00E6E6E600FCFCFC00C9C9C9000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE007B858500191D2300191D
      230051575700C6C6C6000000000000000000FFFFFF00FFFFFF00A5ADCE00CEE7
      FF00ADDEFF00ADDEFF0094CEF7008CC6F7008CADD600B5ADBD00BDA5B500BD9C
      AD0000000000000000000000000000000000FAFAFA00307FB300D4FAFE0099EC
      FA0092EBF9008BE8F800C2F6FC00B9F4FB00AFF1FA00A3EEF90097EAF80081E2
      F50062C2DF00266EA300FFFFFF00FFFFFF0000000000FEFEFE00C9C9C900FAF5
      F000F9F9F900F9F9F900F9F9F900F7F7F700F6F6F600F2F2F200EBEBEB00FCFC
      FC00FCFCFC00FCFCFC00FCFCFC00C9C9C9000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE0071727200484E4E003D42
      420093929200C6C6C6000000000000000000FFFFFF00FFFFFF00FFFFFF00C6AD
      AD009CEFFF0094EFFF0094DEFF0094D6FF0094D6FF0094D6FF008CC6F700ADAD
      C600DEB5B500FFFFFF000000000000000000000000003386B800DFFCFE00A4F0
      FB009DEFFA00D6FBFE002F7EB1002E7BB0002D7BAF002C7AAE002C78AD002974
      AA003076AB0091B0C800FFFFFF00FFFFFF0000000000FEFEFE00C9C9C900FCFC
      FC00F7F7F700F9F9F900F7F7F700F7F7F700F3F3F300F0F0F000EAEAEA00FCFC
      FC00F6F6F600F4F4F400C5C5C500DFDFDF000000000000000000CCCCCC00484E
      4E0031333200575D5E005157570051575700575D5E00191D2300191D23009392
      9200CCCCCC00BEBEBE000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00C6ADAD009CEFFF0094EFFF0094EFFF0094D6FF009CC6EF00C6A5AD00E7CE
      CE00FFFFFF00FFFFFF000000000000000000FDFDFD00388BBD00BADFED00E7FD
      FF00E4FDFF00B3DEED003383B600F3F3F300F7F7F700F6F6F600F5F5F500F3F3
      F300F4F4F400F9F9F900FFFFFF00FFFFFF000000000000000000C9C9C900FBFB
      FB00F4F4F400F5F5F500F5F5F500F5F5F500F1F1F100EFEFEF00E9E9E900FCFC
      FC00E7E7E700C3C3C300DFDFDF00FDFDFD00000000000000000000000000B6B6
      B6009E9E9E009E9E9E009E9E9E009E9E9E009E9E9E00A4A8A800AEAEAE00C6C6
      C600BEBEBE00BEBEBE000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00C6ADAD00A5DEE7009CEFFF00ADC6D600D6ADAD00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00A8CDE2004295C300398F
      C000378DBE003D8EBE00A2C6DB0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000CCCCCC00F8F8
      F800FBFBFB00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00FCFCFC00F8F8
      F800C2C2C200DFDFDF00FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00DEBDBD00DEB5B500E7CECE00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000E3E3E300CCCC
      CC00C9C9C900C9C9C900C9C9C900C9C9C900C9C9C900C9C9C900C9C9C900C9C9
      C900DFDFDF00FDFDFD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      6300636363006363630063636300636363006363630063636300C6948C00C694
      8C000000000000000000C6948C00C6948C000000000000000000000000000000
      0000C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFFFFF008C9C9C0000000000C6948C000000000000000000000000000000
      00000000000000000000EFFFFF00000000000000840000008400000000000000
      0000EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C00000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C6000000000000000000C6DEC60000000000636363000000
      0000C6948C008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C0000000000EFFF
      FF008C9C9C008C9C9C0000000000C6948C000000000000000000000000000000
      84000000840000000000EFFFFF00EFFFFF00000000000000840000000000EFFF
      FF00EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000BDBDBD000000
      000000000000000000000000000000000000C6948C0000000000EFFFFF008C9C
      9C008C9C9C000000000000000000C6948C000000000000000000000084002100
      C6002100C6000000000000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF00092369005157570000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF008C9C
      9C000000000000000000000000006363630000000000000000002100C6000000
      84002100C6002100C6002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF00092369005157570000000000000000000000000000000000000000000000
      000000000000E7F7F700EFFFFF00000000000000000000000000000000000000
      00000000000000000000000000006363630000000000000084002100C6002100
      C6002100C6000000FF002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF0009236900515757000000000000000000000000000000000000000000EFFF
      FF00EFFFFF00DEBDD600DEBDD600EFFFFF00EFFFFF00C6948C00000000008C9C
      9C008C9C9C008C9C9C000000000063636300000000002100C6000000FF002100
      C6000000FF000000FF0000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF0009236900515757000000000000000000000000008C9C9C00000000000000
      000000000000000000000000000000000000EFFFFF0000000000000000000000
      00008C9C9C008C9C9C000000000063636300000000000000FF002100C6000000
      FF000000FF0000000000EFFFFF00EFFFFF002100C6000000FF0000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      00008C9C9C008C9C9C000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000EFFFFF00000000000000FF002100C6000000FF000000
      0000EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C0000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100A56300000000
      000000000000000000000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000000000000000FF000000FF000000FF002100C6000000
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C0000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF0009236900515757000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF000000FF002100
      C6002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000000000008C9C9C00FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100000000006B42
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF002100C6000000
      FF002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000000000008C9C9C008C9C
      9C0000000000000000000000000000000000000000000000000000000000A563
      0000A5630000A563000000000000636363000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF002100
      C600000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A80000000000000000000000000000000000000000000000
      00008C9C9C008C9C9C008C9C9C008C9C9C000000000000000000000000000000
      0000000000000000000000000000636363000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000636363006363
      630000000000000000000000000000000000C6948C00C6948C00C6948C006363
      6300636363006363630063636300636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000ADBBBA0096ADAF009BB6
      BA0091AAAE00A7BABF0096A9AE00A0B0B6009CACB200A0B0B600A0B0B60093A9
      AE0089A2A6009AB5B9009FB3B400C1D3D2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AEBCB800BECCCA00BAD3D500B8D2
      D800C0D8DE00BFD2D700C6D7DA00B9C8CB00B9C8CB00C0CFD200C2D3D600C8DC
      E100C8E0E600BCD6DC00BBCDCE00ADBBB9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A3B3AC00C2D4CD00C3D8D900C2D7
      D900B5C6C9008F9B9B00939B9A00979E9B009095930089908D009BA3A20093A2
      A4008195960097ACAE00CCDAD800A0ABA8000000000000000000BACACE00BACA
      CE00BACACE00BACACE00BACACE00BACACE00BACACE0000000000C2D2D600AEBE
      C2009DABAC00C6D6DA000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A210000000000A2B6B100C5DBD600CFE4E200ADBF
      BE0002100F00000200000002000010100A000D0E050002020000090D08000A15
      1300000201007A898B00C8D4D400A0ABA90000000000C6D6DA00C6DADA00C6DA
      DA00C6DADA00CADADE00CDDEE200CDDEE200CDDEE200CEE2E400BECED200636C
      6C003D4242009AA6AA0000000000000000000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000091AAA600BFDAD600BED1CE00C9D6
      D40000080600C5C7C100C5C3BB00BEB9B000C2BEB300B9B7AD00D8D6CE00A7AC
      AA000F1A18008D9A9800CDD9DB0097A3A50000000000C2D6D600C6D6DA00C6DA
      DA00CDDEE20000000000ABB9BA00ABB9BA00B2C1C200000000005C646500AEAE
      AE00898A89003D42420000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF000923690051575700000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000094B0B000C2E0E100BDCDCC00CDD6
      D300050B0600F8F4E900EFE6D900FFF5E500FFF3E100FFF2E200E3DACD00C9CA
      C100000300008E979400C8D4DA00A2AFB70000000000C2D6D600C6DADA00CDDE
      E200A6B4B60051575700191D230031333200575D5E00484E4E00BEBEBE00898A
      890031333200A2AFB000000000000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000096ACAA00C5DADB00C4D6D700CAD6
      D60000020000F0EADF00F7EADA00FFEFDC00FFEFDA00FCEBD800FFFCEC00C7C8
      BF0000020000A5B1B100C4D7DC0097AAAF0000000000C2D6D600CADEDE00909E
      A0003133320084909100D2DEDF00CEDADE00777B7B003D4242009E9E9E003D42
      42009AA6AA00D6EAEB00000000000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000099ABAA00C6DBD900C4D5D800C9D6
      D80000020000F0EADF00F7EADA00FFEFDA00FFF3DD00F5E2CD00F0E3D300B2B4
      AE000C1512007A878900C3D7DC0096AAAF0000000000C2D6D600C6DADA003D42
      4200BECED200BAC5C600D5D5D500CCD5D500BECACA00ABB9BA00191D23008490
      9100CDDEE200CADEDE00000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000099ABAA00C6DADB00C6D5D800C9D6
      D80000010000EEEADF00F5EBDA00FEEEDD00FDECD900FFFFEE00D8CDBF00BCBE
      B8000002010099A6A800C4D7DC0097AAAD0000000000CDDEE2006D7778005157
      5700313332003133320031333200313332003133320031333200515757005C64
      6500C2D2D200CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000ECEAE000F2EBDC00FBEEDE00FAECDA00FFFBE900E6DED100C1C5
      C0000001000089969800C4D7DC0097AAAD0000000000D3E4E500575D5E00575D
      5E001070820007F0F90007F0F90007F0F90007F0F9003D424200575D5E00484E
      4E00C6D6DA00CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000EBE9E100EEEADF00F7EEE000E3D9C800F6EDDF00FCF8ED00B6BC
      B700070F0E0093A0A200C6D6DC0097AAAD0000000000D3E4E500575D5E00575D
      5E00191D2300107082001070820010708200107082003133320071727200484E
      4E00C2D6D600CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8D9DC00C8D4D800CAD6
      D80000010100E5E9E300E9EAE100F0EEE300E3DFD400F4F2E7000203000099A0
      9D0000020200C3CFD100C6D7DA0099ABAC0000000000C6DADA00A6B4B6003D42
      42006D6C6B00777B7B00898A89008C908F00939292008E9A9A003D42420096A3
      A600C6DADA00CADADE00000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A003163630031636300000000009BACAF00C7D7DD00CDD9DD00C6D2
      D40000020200FBFFFC00F6FAF400EFF2E900FFFFF700F0F3EA00000200000001
      0000CDD7D700C8D4D600C5D7D8009AACAD0000000000C2D6D600CEE2E4005157
      57005C646500909EA00000000000D6E6EA00B2C1C2007B8585005C646500D3E4
      E500C6D6DA00CADADA00000000000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B5003163630000000000000000009AABAE00C3D3D900CFDBDF00C1CC
      D00000010300000200000003000001080100000500000E150E0000020000CFD9
      D900BECACC00DEE9ED00CFDEE0009CAEAF0000000000C2D6D600CADEDE00C2D2
      D2005C646500484E4E003D4242003D424200484E4E00636C6C00CADADE00CADA
      DA00C6DADA00CADADE0000000000000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A800000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      000000000000000000000000000000000000A5B8BD00B7C7CD00C5D1D500BDC8
      CC00CCD8DA00D7E3E300B3C0BE00BFCDC900E0EEEA00B5C3BF00E0EDEB00B4C0
      C200BBC7C900E5F0F400B7C5C400A5B5B40000000000BED2D200C2D2D200C2D6
      D600D6EAEB00A6B4B6007B8585007B858500AFBDBE00D6EAEB00C2D2D600C2D2
      D200C2D2D200C2D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC600000000000000000000000000B1C4C900AEBEC50095A1A500ADB8
      BC00B2BEC200AAB8B70097A8A500A5B6B30094A5A20096A7A40090A19E009EAB
      AD00BBC7CB00939EA200B2C0BF00B8C6C4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000500000000100010000000000800200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000C000FFFF00000000C000800300000000
      0000800300000000000080030000000080008003000000008000FFFF00000000
      8000F83F000000008000F83F000000008000F83F000000008000F83F00000000
      8000E00F000000008000E00F000000008000F01F00000000C000F83F00000000
      C001FC7F00000000C003FEFF00000000FFFFC003FFFFC000FFFFC003FFFFC000
      E003804380008000C003800F00008000C003800F00008000C003800700008000
      C003800700000000C003804700000000C003800700000000C007C00700008000
      C007000700008000C003000F00008000C003000380008000C00300030000C000
      E00300030100C001FFFF00030000C003FFFFFFFF81C0FFFFC000F0030180FFFF
      DFE0E0010000E0035000C0010000C003D00280010000C003CF0680010000C003
      B9CE00010000C003A00200010000C0033F6200010000C003200200010000C003
      200E00010000C003200280030181C003800280038181C0038FC2C0078181C003
      C03EE00F8181FFFFC000F83F8383FFFF8000FFFFFFFFFFFF0000FFFFFFFFFE00
      0000C043E003FE0000008003C003000000008443C003000000008003C0030000
      00008003C003000000008003C003000000008003C003000000008003C0030000
      00008003C003000000008003C003000000008203C003000100008003C0030003
      00008003FFFF00770000FFFFFFFF007F00000000000000000000000000000000
      000000000000}
  end
  object qryItemDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    EnableBCD = False
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ItemDoctoFiscal.*'
      '      ,ItemPedido.nCdPedido'
      '  FROM ItemDoctoFiscal'
      
        '       LEFT JOIN ItemPedido ON ItemPedido.nCdItemPedido = ItemDo' +
        'ctoFiscal.nCdItemPedido'
      ' WHERE nCdDoctoFiscal = :nPK')
    Left = 376
    Top = 392
    object qryItemDoctoFiscalnCdItemDoctoFiscal: TIntegerField
      FieldName = 'nCdItemDoctoFiscal'
    end
    object qryItemDoctoFiscalnCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryItemDoctoFiscalnCdPedido: TIntegerField
      DisplayLabel = 'Pedido|C'#243'digo'
      FieldName = 'nCdPedido'
    end
    object qryItemDoctoFiscalnCdItemPedido: TIntegerField
      DisplayLabel = 'Pedido|Item'
      FieldName = 'nCdItemPedido'
    end
    object qryItemDoctoFiscalnCdProduto: TIntegerField
      DisplayLabel = 'Item|C'#243'd.'
      FieldName = 'nCdProduto'
    end
    object qryItemDoctoFiscalcNmItem: TStringField
      DisplayLabel = 'Item|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemDoctoFiscalcUnidadeMedida: TStringField
      DisplayLabel = 'Item|U.M'
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryItemDoctoFiscalcCFOP: TStringField
      DisplayLabel = 'CFOP'
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryItemDoctoFiscalnQtde: TFloatField
      DisplayLabel = 'Qtde.'
      FieldName = 'nQtde'
    end
    object qryItemDoctoFiscalnValUnitario: TFloatField
      DisplayLabel = 'Valores|Unit'#225'rio'
      FieldName = 'nValUnitario'
    end
    object qryItemDoctoFiscalnValDesconto: TFloatField
      DisplayLabel = 'Valores|Descto'
      FieldName = 'nValDesconto'
    end
    object qryItemDoctoFiscalnValAcrescimo: TFloatField
      DisplayLabel = 'Valores|Acresc.'
      FieldName = 'nValAcrescimo'
    end
    object qryItemDoctoFiscalnValTotal: TFloatField
      DisplayLabel = 'Valores|Total'
      FieldName = 'nValTotal'
    end
    object qryItemDoctoFiscalcCdST: TStringField
      DisplayLabel = 'ICMS Pr'#243'prio|CST'
      DisplayWidth = 3
      FieldName = 'cCdST'
      FixedChar = True
      Size = 3
    end
    object qryItemDoctoFiscalnValBaseICMS: TFloatField
      DisplayLabel = 'ICMS Pr'#243'prio|Base C'#225'lculo'
      FieldName = 'nValBaseICMS'
    end
    object qryItemDoctoFiscalnAliqICMS: TFloatField
      DisplayLabel = 'ICMS Pr'#243'prio|% Al'#237'q.'
      FieldName = 'nAliqICMS'
    end
    object qryItemDoctoFiscalnValICMS: TFloatField
      DisplayLabel = 'ICMS Pr'#243'prio|Valor'
      FieldName = 'nValICMS'
    end
    object qryItemDoctoFiscalnValBaseICMSSub: TFloatField
      DisplayLabel = 'ICMS ST|Base C'#225'lculo'
      FieldName = 'nValBaseICMSSub'
    end
    object qryItemDoctoFiscalnValICMSSub: TFloatField
      DisplayLabel = 'ICMS ST|Valor'
      FieldName = 'nValICMSSub'
    end
    object qryItemDoctoFiscalcCdSTIPI: TStringField
      DisplayLabel = 'IPI|CST'
      FieldName = 'cCdSTIPI'
      FixedChar = True
      Size = 2
    end
    object qryItemDoctoFiscalnValBaseIPI: TFloatField
      DisplayLabel = 'IPI|Base C'#225'lculo'
      FieldName = 'nValBaseIPI'
    end
    object qryItemDoctoFiscalnAliqIPI: TFloatField
      DisplayLabel = 'IPI|% Al'#237'q.'
      FieldName = 'nAliqIPI'
    end
    object qryItemDoctoFiscalnValIPI: TFloatField
      DisplayLabel = 'IPI|Valor'
      FieldName = 'nValIPI'
    end
    object qryItemDoctoFiscalnValIcentivoFiscal: TFloatField
      DisplayLabel = 'Incentivo Fiscal'
      FieldName = 'nValIcentivoFiscal'
    end
    object qryItemDoctoFiscalnValDespesa: TFloatField
      DisplayLabel = 'Outras Depesas|Outras'
      FieldName = 'nValDespesa'
    end
    object qryItemDoctoFiscalnValFrete: TFloatField
      DisplayLabel = 'Outras Depesas|Frete'
      FieldName = 'nValFrete'
    end
    object qryItemDoctoFiscalnValSeguro: TFloatField
      DisplayLabel = 'Outras Depesas|Seguro'
      FieldName = 'nValSeguro'
    end
    object qryItemDoctoFiscalcCSTPIS: TStringField
      DisplayLabel = 'PIS|CST'
      FieldName = 'cCSTPIS'
      FixedChar = True
      Size = 2
    end
    object qryItemDoctoFiscalnValBasePIS: TFloatField
      DisplayLabel = 'PIS|Base C'#225'lculo'
      FieldName = 'nValBasePIS'
    end
    object qryItemDoctoFiscalnAliqPIS: TFloatField
      DisplayLabel = 'PIS|%Al'#237'q.'
      FieldName = 'nAliqPIS'
    end
    object qryItemDoctoFiscalnValPIS: TFloatField
      DisplayLabel = 'PIS|Valor'
      FieldName = 'nValPIS'
    end
    object qryItemDoctoFiscalcCSTCOFINS: TStringField
      DisplayLabel = 'COFINS|CST'
      FieldName = 'cCSTCOFINS'
      FixedChar = True
      Size = 2
    end
    object qryItemDoctoFiscalnValBaseCOFINS: TFloatField
      DisplayLabel = 'COFINS|Base C'#225'lculo'
      FieldName = 'nValBaseCOFINS'
    end
    object qryItemDoctoFiscalnAliqCOFINS: TFloatField
      DisplayLabel = 'COFINS|% Al'#237'q.'
      FieldName = 'nAliqCOFINS'
    end
    object qryItemDoctoFiscalnValCOFINS: TFloatField
      DisplayLabel = 'COFINS|Valor'
      FieldName = 'nValCOFINS'
    end
    object qryItemDoctoFiscalcNCMSH: TStringField
      DisplayLabel = 'NCM/SH'
      FieldName = 'cNCMSH'
      FixedChar = True
      Size = 8
    end
    object qryItemDoctoFiscalnCdGrupoImposto: TIntegerField
      FieldName = 'nCdGrupoImposto'
    end
    object qryItemDoctoFiscalnCdNaturezaOperacao: TIntegerField
      FieldName = 'nCdNaturezaOperacao'
    end
    object qryItemDoctoFiscalnValUnitarioPed: TFloatField
      FieldName = 'nValUnitarioPed'
    end
    object qryItemDoctoFiscalnValCredAdiant: TFloatField
      FieldName = 'nValCredAdiant'
    end
    object qryItemDoctoFiscalnCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryItemDoctoFiscaldDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryItemDoctoFiscalcDescricaoAdicional: TStringField
      FieldName = 'cDescricaoAdicional'
      Size = 100
    end
    object qryItemDoctoFiscalnPercIVA: TFloatField
      DisplayLabel = 'ICMS ST|MVA'
      FieldName = 'nPercIVA'
    end
    object qryItemDoctoFiscalnPercBCICMS: TFloatField
      FieldName = 'nPercBCICMS'
    end
    object qryItemDoctoFiscalnPercBCIVA: TFloatField
      FieldName = 'nPercBCIVA'
    end
    object qryItemDoctoFiscalnPercBCICMSST: TFloatField
      FieldName = 'nPercBCICMSST'
    end
    object qryItemDoctoFiscalnValIsenta: TFloatField
      FieldName = 'nValIsenta'
    end
    object qryItemDoctoFiscalnValOutras: TFloatField
      FieldName = 'nValOutras'
    end
    object qryItemDoctoFiscalnCdTipoICMSECF: TIntegerField
      FieldName = 'nCdTipoICMSECF'
    end
    object qryItemDoctoFiscalnCdTabTipoOrigemMercadoria: TIntegerField
      FieldName = 'nCdTabTipoOrigemMercadoria'
    end
    object qryItemDoctoFiscalcEnqLegalIPI: TStringField
      FieldName = 'cEnqLegalIPI'
      FixedChar = True
      Size = 3
    end
    object qryItemDoctoFiscalcEXTIPI: TStringField
      FieldName = 'cEXTIPI'
      FixedChar = True
      Size = 2
    end
    object qryItemDoctoFiscalnPercCargaMediaST: TFloatField
      DisplayLabel = 'ICMS ST|Carga M'#233'dia'
      FieldName = 'nPercCargaMediaST'
    end
    object qryItemDoctoFiscalnAliqICMSInternaDestino: TFloatField
      DisplayLabel = 'ICMS ST|Aliq. ICMS Interna|Destino'
      FieldName = 'nAliqICMSInternaDestino'
    end
    object qryItemDoctoFiscalnAliqICMSInternaOrigem: TFloatField
      DisplayLabel = 'ICMS ST|Aliq. ICMS Interna|Origem'
      FieldName = 'nAliqICMSInternaOrigem'
    end
    object qryItemDoctoFiscalnValImpostoAproxFed: TFloatField
      FieldName = 'nValImpostoAproxFed'
    end
    object qryItemDoctoFiscalnValImpostoAproxEst: TFloatField
      FieldName = 'nValImpostoAproxEst'
    end
    object qryItemDoctoFiscalnValImpostoAproxMun: TFloatField
      FieldName = 'nValImpostoAproxMun'
    end
    object qryItemDoctoFiscalnAliqFederal: TFloatField
      FieldName = 'nAliqFederal'
    end
    object qryItemDoctoFiscalnAliqEstadual: TFloatField
      FieldName = 'nAliqEstadual'
    end
    object qryItemDoctoFiscalnAliqMunicipal: TFloatField
      FieldName = 'nAliqMunicipal'
    end
  end
  object dsItem: TDataSource
    DataSet = qryItemDoctoFiscal
    Left = 376
    Top = 424
  end
  object qryEnvioNFeEmail: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cFlgEnviaNFeEmail'
      '      ,cEmailNFe'
      '      ,cEmailCopiaNFe'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro =:nPK')
    Left = 408
    Top = 393
    object qryEnvioNFeEmailcFlgEnviaNFeEmail: TIntegerField
      FieldName = 'cFlgEnviaNFeEmail'
    end
    object qryEnvioNFeEmailcEmailNFe: TStringField
      FieldName = 'cEmailNFe'
      Size = 50
    end
    object qryEnvioNFeEmailcEmailCopiaNFe: TStringField
      FieldName = 'cEmailCopiaNFe'
      Size = 50
    end
  end
  object qryRepresentante: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTerceiro'
      '  FROM Terceiro'
      
        '       LEFT JOIN Pedido ON Pedido.nCdTerceiroRepres = Terceiro.n' +
        'CdTerceiro'
      ' WHERE Terceiro.nCdTerceiro = Pedido.nCdTerceiroRepres'
      '   AND nCdPedido            = :nPK')
    Left = 444
    Top = 393
    object qryRepresentantecNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object dsRepresentante: TDataSource
    DataSet = qryRepresentante
    Left = 448
    Top = 424
  end
  object qryCartaCorrecaoNFe: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdDoctoFiscal int'
      ''
      'SET @nCdDoctoFiscal = :nCdDoctoFiscal'
      ''
      'SELECT nCdCartaCorrecaoNFe'
      '  FROM CartaCorrecaoNFe'
      ' WHERE nCdDoctoFiscal = @nCdDoctoFiscal'
      '   AND iSeq = (SELECT MAX(iSeq)'
      '                 FROM CartaCorrecaoNFe CCN'
      '                WHERE CCN.nCdDoctoFiscal      = @nCdDoctoFiscal'
      
        '                  AND CCN.nCdCartaCorrecaoNFe = nCdCartaCorrecao' +
        'NFe) ')
    Left = 484
    Top = 393
    object qryCartaCorrecaoNFenCdCartaCorrecaoNFe: TIntegerField
      FieldName = 'nCdCartaCorrecaoNFe'
    end
  end
  object PopupMenu1: TPopupMenu
    Images = ImageList1
    Left = 524
    Top = 393
    object GerarXML1: TMenuItem
      Caption = 'Gerar XML NF-e'
      ImageIndex = 11
      OnClick = GerarXML1Click
    end
    object GerarPDF1: TMenuItem
      Caption = 'Gerar PDF NF-e'
      ImageIndex = 12
      OnClick = GerarPDF1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object btEnviaDocMonitorWeb: TMenuItem
      Caption = 'Enviar Doc. Monitor Web'
      ImageIndex = 13
      OnClick = btEnviaDocMonitorWebClick
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      
        'SELECT CAST(nCdUsuario as varchar(20)) + '#39' - '#39' + cNmUsuario as c' +
        'NmUsuario'
      '  FROM Usuario '
      ' WHERE nCdUsuario = :nPK')
    Left = 556
    Top = 425
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsUsuario: TDataSource
    DataSet = qryUsuario
    Left = 524
    Top = 425
  end
  object qryDoctoFiscalAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDoctoFiscal'
      '      ,nCdEmpresa'
      '      ,cChaveNFe'
      '      ,nCdStatus'
      '      ,cCaminhoXML'
      '      ,cCaminhoXMLCanc'
      '      ,cArquivoXML'
      '      ,cArquivoXMLCanc'
      '      ,cFlgAmbienteHom'
      '      ,CONVERT(xml,cXMLNFe) as cXMLNFe '
      '  FROM DoctoFiscal'
      ' WHERE nCdDoctoFiscal = :nPK'
      '')
    Left = 484
    Top = 425
    object qryDoctoFiscalAuxnCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryDoctoFiscalAuxnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryDoctoFiscalAuxcChaveNFe: TStringField
      FieldName = 'cChaveNFe'
      Size = 100
    end
    object qryDoctoFiscalAuxnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryDoctoFiscalAuxcCaminhoXML: TStringField
      FieldName = 'cCaminhoXML'
      Size = 200
    end
    object qryDoctoFiscalAuxcCaminhoXMLCanc: TStringField
      FieldName = 'cCaminhoXMLCanc'
      Size = 200
    end
    object qryDoctoFiscalAuxcArquivoXML: TStringField
      FieldName = 'cArquivoXML'
      Size = 200
    end
    object qryDoctoFiscalAuxcArquivoXMLCanc: TStringField
      FieldName = 'cArquivoXMLCanc'
      Size = 200
    end
    object qryDoctoFiscalAuxcFlgAmbienteHom: TIntegerField
      FieldName = 'cFlgAmbienteHom'
    end
    object qryDoctoFiscalAuxcXMLNFe: TMemoField
      FieldName = 'cXMLNFe'
      ReadOnly = True
      BlobType = ftMemo
    end
  end
  object qryEnviaPedidoWebMonitor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      '--'
      '-- consulta pedidos web vinculados ao doc. fiscal impresso'
      '--'
      'DECLARE @nCdPedidoWeb int'
      ''
      'DECLARE curPedidosWeb CURSOR'
      '    FOR SELECT DISTINCT'
      '               nCdPedidoWeb'
      '          FROM ItemDoctoFiscal'
      
        '               INNER JOIN ItemPedido ON ItemPedido.nCdItemPedido' +
        ' = ItemDoctoFiscal.nCdItemPedido'
      
        '               INNER JOIN PedidoWeb  ON PedidoWeb.nCdPedido     ' +
        ' = ItemPedido.nCdPedido'
      '         WHERE nCdDoctoFiscal = :nCdDoctoFiscal'
      ''
      'OPEN curPedidosWeb'
      ''
      'FETCH NEXT'
      ' FROM curPedidosWeb'
      ' INTO @nCdPedidoWeb'
      ''
      'IF (@@CURSOR_ROWS) = 0'
      'BEGIN'
      
        '    RAISERROR('#39'N'#227'o existem pedidos web vinculados ao documento s' +
        'elecionado.'#39',16,1)'
      '    RETURN'
      'END'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      '    EXEC SP_ENVIA_PEDIDOWEB_TEMPREGISTROWEB @nCdPedidoWeb'
      ''
      '    FETCH NEXT'
      '     FROM curPedidosWeb'
      '     INTO @nCdPedidoWeb'
      ''
      'END'
      ''
      'CLOSE curPedidosWeb'
      'DEALLOCATE curPedidosWeb')
    Left = 556
    Top = 393
  end
  object qryTipoDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      '  SELECT cNmTipoDoctoFiscal'
      '    FROM TipoDoctoFiscal'
      '   WHERE nCdTipoDoctoFiscal = :nPK')
    Left = 588
    Top = 393
    object qryTipoDoctoFiscalcNmTipoDoctoFiscal: TStringField
      FieldName = 'cNmTipoDoctoFiscal'
      Size = 50
    end
  end
  object dsTipoDoctoFiscal: TDataSource
    DataSet = qryTipoDoctoFiscal
    Left = 588
    Top = 425
  end
end
