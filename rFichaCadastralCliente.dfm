inherited rptFichaCadastralCliente: TrptFichaCadastralCliente
  Left = 138
  Top = 119
  Caption = 'Ficha Cadastral Cliente'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 16
    Top = 48
    Width = 33
    Height = 13
    Caption = 'Cliente'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtTerceiro: TMaskEdit [3]
    Left = 56
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnKeyDown = edtTerceiroKeyDown
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 128
    Top = 40
    Width = 537
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 2
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '  FROM vwClienteVarejo'
      ' WHERE nCdTerceiro = :nPK')
    Left = 216
    Top = 104
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 264
    Top = 112
  end
end
