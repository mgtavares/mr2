inherited frmSegmentoCobranca: TfrmSegmentoCobranca
  Left = 193
  Top = 114
  Caption = 'Segmento de Cobran'#231'a'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 51
    Top = 40
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 40
    Top = 64
    Width = 49
    Height = 13
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 92
    Top = 34
    Width = 65
    Height = 19
    DataField = 'nCdSegmentoCobranca'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 92
    Top = 58
    Width = 489
    Height = 19
    DataField = 'cNmSegmentoCobranca'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSegmentoCobranca '
      '       ,cNmSegmentoCobranca '
      '  FROM SegmentoCobranca'
      ' WHERE nCdSegmentoCobranca = :nPK')
    object qryMasternCdSegmentoCobranca: TIntegerField
      FieldName = 'nCdSegmentoCobranca'
    end
    object qryMastercNmSegmentoCobranca: TStringField
      FieldName = 'cNmSegmentoCobranca'
      Size = 50
    end
  end
  inherited dsMaster: TDataSource
    Left = 512
    Top = 224
  end
  inherited qryID: TADOQuery
    Left = 576
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 608
    Top = 192
  end
  inherited qryStat: TADOQuery
    Left = 544
    Top = 192
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 608
    Top = 224
  end
  inherited ImageList1: TImageList
    Left = 640
    Top = 192
  end
end
