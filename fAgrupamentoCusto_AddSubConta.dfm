inherited frmAgrupamentoCusto_AddSubConta: TfrmAgrupamentoCusto_AddSubConta
  Left = 257
  Top = 336
  Width = 655
  Height = 215
  Caption = 'Adicionar SubConta'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 179
    Width = 639
    Height = 0
  end
  inherited ToolBar1: TToolBar
    Width = 639
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 639
    Height = 150
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 39
      Top = 24
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta Superior'
    end
    object Label2: TLabel
      Tag = 1
      Left = 78
      Top = 48
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo'
    end
    object Label3: TLabel
      Tag = 1
      Left = 71
      Top = 72
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#225'scara'
    end
    object Label4: TLabel
      Tag = 1
      Left = 65
      Top = 96
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descri'#231#227'o'
    end
    object Label6: TLabel
      Tag = 1
      Left = 88
      Top = 120
      Width = 23
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#237'vel'
    end
    object edtMascaraSuperior: TEdit
      Tag = 1
      Left = 115
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object edtMascara: TEdit
      Tag = 1
      Left = 115
      Top = 64
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object edtDescricao: TEdit
      Left = 115
      Top = 88
      Width = 500
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 3
    end
    object edtNivel: TEdit
      Tag = 1
      Left = 115
      Top = 112
      Width = 65
      Height = 21
      TabOrder = 4
    end
    object edtCodigo: TMaskEdit
      Left = 115
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 1
      OnChange = edtCodigoChange
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 240
      Top = 16
      Width = 377
      Height = 21
      DataField = 'cNmContaAgrupamentoCusto'
      DataSource = DataSource2
      TabOrder = 5
    end
  end
  object qryContaPai: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ContaAgrupamentoCusto'
      'WHERE nCdContaAgrupamentoCusto = :nPK')
    Left = 448
    Top = 69
    object qryContaPainCdContaAgrupamentoCusto: TIntegerField
      FieldName = 'nCdContaAgrupamentoCusto'
    end
    object qryContaPainCdContaAgrupamentoCustoPai: TIntegerField
      FieldName = 'nCdContaAgrupamentoCustoPai'
    end
    object qryContaPainCdAgrupamentoCusto: TIntegerField
      FieldName = 'nCdAgrupamentoCusto'
    end
    object qryContaPaicMascara: TStringField
      FieldName = 'cMascara'
      Size = 30
    end
    object qryContaPaicNmContaAgrupamentoCusto: TStringField
      FieldName = 'cNmContaAgrupamentoCusto'
      Size = 100
    end
    object qryContaPaiiNivel: TIntegerField
      FieldName = 'iNivel'
    end
    object qryContaPaiiSequencia: TIntegerField
      FieldName = 'iSequencia'
    end
    object qryContaPainCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryContaPaicFlgAnalSintetica: TStringField
      FieldName = 'cFlgAnalSintetica'
      FixedChar = True
      Size = 1
    end
  end
  object qryAgrupamentoCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM AgrupamentoCusto'
      'WHERE nCdAgrupamentoCusto = :nPK')
    Left = 528
    Top = 61
    object qryAgrupamentoCustonCdAgrupamentoCusto: TIntegerField
      FieldName = 'nCdAgrupamentoCusto'
    end
    object qryAgrupamentoCustonCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryAgrupamentoCustocNmAgrupamentoCusto: TStringField
      FieldName = 'cNmAgrupamentoCusto'
      Size = 50
    end
    object qryAgrupamentoCustocMascara: TStringField
      FieldName = 'cMascara'
      Size = 30
    end
    object qryAgrupamentoCustonCdTabTipoAgrupamentoContabil: TIntegerField
      FieldName = 'nCdTabTipoAgrupamentoContabil'
    end
    object qryAgrupamentoCustonCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryAgrupamentoCustoiMaxNiveis: TIntegerField
      FieldName = 'iMaxNiveis'
    end
  end
  object qryContaAgrupamentoCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ContaAgrupamentoCusto'
      'WHERE nCdContaAgrupamentoCusto = :nPK')
    Left = 456
    Top = 125
    object qryContaAgrupamentoCustonCdContaAgrupamentoCusto: TIntegerField
      FieldName = 'nCdContaAgrupamentoCusto'
    end
    object qryContaAgrupamentoCustonCdContaAgrupamentoCustoPai: TIntegerField
      FieldName = 'nCdContaAgrupamentoCustoPai'
    end
    object qryContaAgrupamentoCustonCdAgrupamentoCusto: TIntegerField
      FieldName = 'nCdAgrupamentoCusto'
    end
    object qryContaAgrupamentoCustocMascara: TStringField
      FieldName = 'cMascara'
      Size = 30
    end
    object qryContaAgrupamentoCustocNmContaAgrupamentoCusto: TStringField
      FieldName = 'cNmContaAgrupamentoCusto'
      Size = 100
    end
    object qryContaAgrupamentoCustoiNivel: TIntegerField
      FieldName = 'iNivel'
    end
    object qryContaAgrupamentoCustoiSequencia: TIntegerField
      FieldName = 'iSequencia'
    end
    object qryContaAgrupamentoCustonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryContaAgrupamentoCustocFlgAnalSintetica: TStringField
      FieldName = 'cFlgAnalSintetica'
      FixedChar = True
      Size = 1
    end
  end
  object DataSource2: TDataSource
    DataSet = qryContaPai
    Left = 296
    Top = 120
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 496
    Top = 125
  end
end
