inherited frmImpBoletoInstrucaoTitulo: TfrmImpBoletoInstrucaoTitulo
  Left = 193
  Top = 180
  Width = 789
  Height = 404
  Caption = 'Instru'#231#245'es T'#237'tulo Boleto'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 773
    Height = 339
  end
  inherited ToolBar1: TToolBar
    Width = 773
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 773
    Height = 339
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 335
    ClientRectLeft = 4
    ClientRectRight = 769
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Instru'#231#245'es'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 765
        Height = 311
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTituloInstrucaoBoleto
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTituloInstrucaoBoleto'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'cMensagem'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 504
    Top = 104
  end
  object qryTituloInstrucaoBoleto: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTituloInstrucaoBoletoBeforePost
    Parameters = <
      item
        Name = 'nCdTitulo'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM '
      'TituloInstrucaoBoleto'
      'where nCdTitulo =:nCdTitulo')
    Left = 576
    Top = 56
    object qryTituloInstrucaoBoletonCdTituloInstrucaoBoleto: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'nCdTituloInstrucaoBoleto'
    end
    object qryTituloInstrucaoBoletonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTituloInstrucaoBoletocMensagem: TStringField
      DisplayLabel = 'Instru'#231#227'o de Cobran'#231'a '
      FieldName = 'cMensagem'
      Size = 100
    end
    object qryTituloInstrucaoBoletocFlgMensagemAgrupamento: TIntegerField
      FieldName = 'cFlgMensagemAgrupamento'
    end
  end
  object dsTituloInstrucaoBoleto: TDataSource
    DataSet = qryTituloInstrucaoBoleto
    Left = 608
    Top = 56
  end
end
