unit fMRP_BalancoFornecedores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, GridsEh, DBGridEh, DB, ADODB,
  ImgList, ComCtrls, ToolWin, ExtCtrls;

type
  TfrmMRP_BalancoFornecedores = class(TfrmProcesso_Padrao)
    qryFornecedores: TADOQuery;
    qryFornecedoresnCdTerceiro: TIntegerField;
    qryFornecedorescNmTerceiro: TStringField;
    qryFornecedoresnPercentCompra: TBCDField;
    qryFornecedorescNrContrato: TStringField;
    dsFornecedores: TDataSource;
    DBGridEh1: TDBGridEh;
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMRP_BalancoFornecedores: TfrmMRP_BalancoFornecedores;

implementation

{$R *.dfm}

procedure TfrmMRP_BalancoFornecedores.ToolButton2Click(Sender: TObject);
begin

  if (qryFornecedores.State = dsEdit) then
      qryFornecedores.Post ;

  qryFornecedores.UpdateBatch();
  qryFornecedores.Close ;       
      
  inherited;

end;

end.
