unit fContaBancaria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  DBGridEhGrouping, ToolCtrlsEh, ER2Lookup;

type
  TfrmContaBancaria = class(TfrmCadastro_Padrao)
    qryMasternCdContaBancaria: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdBanco: TIntegerField;
    qryMasternCdConta: TStringField;
    qryMasternCdCC: TIntegerField;
    qryMastercFlgFluxo: TIntegerField;
    qryMasteriUltimoCheque: TIntegerField;
    qryMasteriUltimoBordero: TIntegerField;
    qryEmpresa: TADOQuery;
    qryBanco: TADOQuery;
    qryCC: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresanCdTerceiroEmp: TIntegerField;
    qryEmpresanCdTerceiroMatriz: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresanCdStatus: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryBanconCdBanco: TIntegerField;
    qryBancocNmBanco: TStringField;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    qryCCnCdCC1: TIntegerField;
    qryCCnCdCC2: TIntegerField;
    qryCCcCdCC: TStringField;
    qryCCcCdHie: TStringField;
    qryCCiNivel: TSmallintField;
    qryCCcFlgLanc: TIntegerField;
    qryCCnCdStatus: TIntegerField;
    qryMastercSiglaEmpresa: TStringField;
    qryMastercNmEmpresa: TStringField;
    qryMastercNmBanco: TStringField;
    qryMastercNmCC: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    qryUsuarioContaBancaria: TADOQuery;
    qryUsuarioContaBancarianCdContaBancaria: TIntegerField;
    qryUsuarioContaBancarianCdUsuario: TIntegerField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmLogin: TStringField;
    qryUsuariocSenha: TStringField;
    qryUsuariocFlgAtivo: TSmallintField;
    qryUsuariodDtUltAcesso: TDateTimeField;
    qryUsuariodDtUltAltSenha: TDateTimeField;
    qryUsuarionCdEmpPadrao: TIntegerField;
    qryUsuarionCdStatus: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    qryUsuariocCPF: TStringField;
    qryUsuarionCdTerceiroResponsavel: TIntegerField;
    qryUsuariocFlagMaster: TSmallintField;
    qryUsuariocAcessoWERP: TSmallintField;
    qryUsuariocTrocaSenhaProxLogin: TSmallintField;
    dsUsuarioContaBancaria: TDataSource;
    qryUsuarioContaBancariacNmUsuario: TStringField;
    qryMasternValLimiteCredito: TBCDField;
    Label9: TLabel;
    DBEdit13: TDBEdit;
    qrySaldo: TADOQuery;
    dsSaldo: TDataSource;
    qrySaldonCdSaldoContaBancaria: TAutoIncField;
    qrySaldonCdContaBancaria: TIntegerField;
    qrySaldodDtSaldo: TDateTimeField;
    qrySaldonSaldo: TBCDField;
    qrySaldodDtCad: TDateTimeField;
    qrySaldonCdUsuarioCad: TIntegerField;
    qryMastercFlgDeposito: TIntegerField;
    DBCheckBox2: TDBCheckBox;
    qryMastercFlgEmiteCheque: TIntegerField;
    DBCheckBox3: TDBCheckBox;
    qryMastercAgencia: TIntegerField;
    qryMastercFlgEmiteBoleto: TIntegerField;
    DBCheckBox4: TDBCheckBox;
    qryMastercFlgCaixa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdUsuarioOperador: TIntegerField;
    qryMasternSaldoConta: TBCDField;
    qryMastercFlgCofre: TIntegerField;
    qryMasterdDtUltConciliacao: TDateTimeField;
    qryMastercNmTitular: TStringField;
    qryMastercFlgProvTit: TIntegerField;
    Label10: TLabel;
    DBEdit14: TDBEdit;
    DBCheckBox5: TDBCheckBox;
    qrySaldonSaldoPrevisto: TBCDField;
    qryMastercFlgReapresentaCheque: TIntegerField;
    DBCheckBox6: TDBCheckBox;
    qryMasteriUltimoBoleto: TIntegerField;
    Label11: TLabel;
    DBEdit15: TDBEdit;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    Label12: TLabel;
    DBEdit16: TDBEdit;
    DataSource1: TDataSource;
    DBEdit17: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    qryUsuarioContaBancarianCdUsuarioContaBancaria: TIntegerField;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryMasternCdStatus: TIntegerField;
    ER2LkpStatus: TER2LookupDBEdit;
    Label8: TLabel;
    DataSource2: TDataSource;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    DBEdit8: TDBEdit;
    DataSource3: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryUsuarioContaBancariaBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qrySaldoBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure DBEdit17KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit17Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmContaBancaria: TfrmContaBancaria;

implementation

uses fLookup_Padrao, fMenu, fContaBancaria_Conf_CNAB;

{$R *.dfm}

procedure TfrmContaBancaria.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CONTABANCARIA' ;
  nCdTabelaSistema  := 22 ;
  nCdConsultaPadrao := 37 ;
  bLimpaAposSalvar  := False;
end;

procedure TfrmContaBancaria.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  if(frmMenu.LeParametro('VAREJO') = 'N') then
      DBEdit3.SetFocus
  else DBEdit17.SetFocus;
end;

procedure TfrmContaBancaria.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmContaBancaria.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(23);

            If (nPK > 0) then
            begin
                qryMasternCdBanco.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmContaBancaria.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(29);

            If (nPK > 0) then
            begin
                qryMasternCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmContaBancaria.qryUsuarioContaBancariaBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryUsuarioContaBancarianCdContaBancaria.Value := qryMaster.FieldList[0].Value ;

  inherited;

  if (qryUsuarioContaBancaria.State = dsInsert) then
      qryUsuarioContaBancarianCdUsuarioContaBancaria.Value := frmMenu.fnProximoCodigo('USUARIOCONTABANCARIA') ;

end;

procedure TfrmContaBancaria.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryStatus,IntToStr(qryMasternCdStatus.Value));

  if (qryMastercFlgCaixa.Value = 1) then
  begin
      MensagemAlerta('Esta conta n�o pode ser alterada por esta tela.' +#13#13 + 'Utilize a tela de caixas e cofres.') ;
      btCancelar.Click ;
      exit ;
  end ;

  qryUsuarioContaBancaria.Close ;
  qryUsuarioContaBancaria.Parameters.ParamByName('nCdContaBancaria').Value := qryMasternCdContaBancaria.Value ;
  qryUsuarioContaBancaria.Open ;

  qrySaldo.Close ;
  qrySaldo.Parameters.ParamByName('nCdContaBancaria').Value := qryMasternCdContaBancaria.Value ;
  qrySaldo.Open ;

  qryLoja.Close;
  PosicionaQuery(qryLoja,qryMasternCdLoja.asString);

end;

procedure TfrmContaBancaria.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioContaBancaria.State = dsBrowse) then
             qryUsuarioContaBancaria.Edit ;

        if (qryUsuarioContaBancaria.State = dsInsert) or (qryUsuarioContaBancaria.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioContaBancarianCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmContaBancaria.qrySaldoBeforePost(DataSet: TDataSet);
begin

  if (qrySaldodDtSaldo.asString = '') then
  begin
      ShowMessage('Informe a data do Saldo.') ;
      abort ;
  end ;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qrySaldonCdContaBancaria.Value := qryMaster.FieldList[0].Value ;
  qrySaldodDtCad.Value           := Now() ;
  qrySaldonCdUsuarioCad.Value    := frmMenu.nCdUsuarioLogado ;

  inherited;

end;

procedure TfrmContaBancaria.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;

  qryUsuarioContaBancaria.Close ;
  qrySaldo.Close ;
  qryLoja.Close;
  
end;

procedure TfrmContaBancaria.DBEdit17KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;
  case Key of
  vk_F4 : begin

          if (((qryMaster.State = dsInsert) or (qryMaster.State = dsEdit)) and (frmMenu.LeParametro('VAREJO') = 'S')) then
          begin

              nPK := frmLookup_Padrao.ExecutaConsulta(59);

              If (nPK > 0) then
              begin
                  DBEdit17.Text := IntToStr(nPK);
                  PosicionaQuery(qryLoja,DBEdit17.Text) ;
              end ;

          end ;

      end ;

   end ;

end;

procedure TfrmContaBancaria.DBEdit17Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  PosicionaQuery(qryLoja,DBEdit17.Text) ;
end;

procedure TfrmContaBancaria.FormShow(Sender: TObject);
begin
  inherited;
  if(frmMenu.LeParametro('VAREJO') = 'N') then
  begin
      DBEdit17.ReadOnly   := True ;
      DBEdit17.Color      := $00E9E4E4 ;
      DBEdit17.Font.Color := clBlack ;
      DBEdit17.TabStop    := False ;
      Update;
  end;
end;

procedure TfrmContaBancaria.ToolButton10Click(Sender: TObject);
var
  objForm : TfrmContaBancaria_Conf_CNAB;
begin
  inherited;

  if (not qryMaster.Active) then
      abort;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;
      
  objForm := TfrmContaBancaria_Conf_CNAB.Create(nil);

  try
      try
          objForm.ConfigCNAB(qryMasternCdContaBancaria.Value);
      except
          MensagemErro('Erro na cria��o da classe ' + objForm.ClassName);
          raise;
      end;
  finally
      FreeAndNil(objForm);
  end;
  
end;

procedure TfrmContaBancaria.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasternCdEmpresa.Value = 0) or (dbEdit10.Text = '') then
  begin
      ShowMessage('Informe a Empresa') ;
      DBEdit2.SetFocus ;
      Abort ;
  end ;

  if (qryMasternCdBanco.Value = 0) or (dbEdit11.Text = '') then
  begin
      ShowMessage('Informe o Banco') ;
      DBEdit3.SetFocus ;
      Abort ;
  end ;

  if (qryMastercAgencia.Value = 0) then
  begin
      ShowMessage('Informe a Ag�ncia') ;
      DBEdit4.SetFocus ;
      Abort ;
  end ;

  if (qryMasternCdConta.Value = '') then
  begin
      ShowMessage('Informe o N�mero da Conta') ;
      DBEdit5.SetFocus ;
      Abort ;
  end ;

  if (((Trim(DBEdit17.Text)='') or (DBEdit16.Text = '')) and (frmMenu.LeParametro('VAREJO') = 'S')) then
  begin
      MensagemAlerta('Selecione uma loja');
      DBEdit17.SetFocus;
      Abort ;
  end;

  inherited;

end;

procedure TfrmContaBancaria.btSalvarClick(Sender: TObject);
begin
  inherited;

  btCancelar.Click;
end;

procedure TfrmContaBancaria.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryStatus.Close;
end;

initialization
    RegisterClass(tFrmContaBancaria) ;
end.
