inherited frmVerbaPublicidade: TfrmVerbaPublicidade
  Left = -8
  Top = -8
  Height = 786
  Caption = 'Verba Publicidade'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 725
  end
  object Label1: TLabel [1]
    Left = 78
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 76
    Top = 62
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 48
    Top = 88
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Emiss'#227'o'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 236
    Top = 88
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Vencimento'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 28
    Top = 270
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Total Verba'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 34
    Top = 134
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Verba'
    FocusControl = DBMemo1
  end
  object Label7: TLabel [7]
    Left = 10
    Top = 246
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'Unidade de Neg'#243'cio'
    FocusControl = DBEdit6
  end
  object Label8: TLabel [8]
    Left = 242
    Top = 270
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Liquidado'
    FocusControl = DBEdit7
  end
  object Label9: TLabel [9]
    Left = 246
    Top = 294
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo da Verba'
    FocusControl = DBEdit8
  end
  object Label10: TLabel [10]
    Left = 21
    Top = 294
    Width = 95
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor de Desconto'
    FocusControl = DBEdit9
  end
  object Label11: TLabel [11]
    Left = 254
    Top = 38
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Inclus'#227'o'
    FocusControl = DBEdit10
  end
  object Label12: TLabel [12]
    Left = 45
    Top = 110
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo Verba'
    FocusControl = DBEdit13
  end
  object DBEdit1: TDBEdit [14]
    Tag = 1
    Left = 120
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTitulo'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [15]
    Left = 120
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 2
    OnExit = DBEdit2Exit
    OnKeyDown = DBEdit2KeyDown
  end
  object DBEdit3: TDBEdit [16]
    Left = 120
    Top = 82
    Width = 81
    Height = 19
    DataField = 'dDtEmissao'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [17]
    Left = 328
    Top = 82
    Width = 81
    Height = 19
    DataField = 'dDtVenc'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [18]
    Left = 120
    Top = 264
    Width = 97
    Height = 19
    DataField = 'nValTit'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBMemo1: TDBMemo [19]
    Left = 120
    Top = 130
    Width = 465
    Height = 105
    DataField = 'cObsTit'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit6: TDBEdit [20]
    Left = 120
    Top = 240
    Width = 65
    Height = 19
    DataField = 'nCdUnidadeNegocio'
    DataSource = dsMaster
    TabOrder = 7
    OnExit = DBEdit6Exit
    OnKeyDown = DBEdit6KeyDown
  end
  object DBEdit7: TDBEdit [21]
    Tag = 1
    Left = 328
    Top = 264
    Width = 97
    Height = 19
    DataField = 'nValLiq'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit8: TDBEdit [22]
    Tag = 1
    Left = 328
    Top = 288
    Width = 97
    Height = 19
    DataField = 'nSaldoTit'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit9: TDBEdit [23]
    Tag = 1
    Left = 120
    Top = 288
    Width = 97
    Height = 19
    DataField = 'nValDesconto'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit10: TDBEdit [24]
    Tag = 1
    Left = 328
    Top = 32
    Width = 161
    Height = 19
    DataField = 'dDtCad'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit11: TDBEdit [25]
    Tag = 1
    Left = 192
    Top = 56
    Width = 393
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = DataSource1
    TabOrder = 13
  end
  object DBEdit12: TDBEdit [26]
    Tag = 1
    Left = 192
    Top = 240
    Width = 393
    Height = 19
    DataField = 'cNmUnidadeNegocio'
    DataSource = DataSource2
    TabOrder = 14
  end
  object DBEdit13: TDBEdit [27]
    Left = 120
    Top = 106
    Width = 221
    Height = 19
    DataField = 'cNrTit'
    DataSource = dsMaster
    TabOrder = 5
  end
  object cxPageControl1: TcxPageControl [28]
    Left = 16
    Top = 320
    Width = 841
    Height = 305
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 15
    ClientRectBottom = 301
    ClientRectLeft = 4
    ClientRectRight = 837
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Hist'#243'rico Movimenta'#231#227'o'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 833
        Height = 277
        Align = alClient
        DataSource = dsMovimento
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'Movimento|ID'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'Opera'#231#227'o | C'#243'digo'
            Footers = <>
            Width = 50
          end
          item
            EditButtons = <>
            FieldName = 'Opera'#231#227'o | Descri'#231#227'o'
            Footers = <>
            Width = 180
          end
          item
            EditButtons = <>
            FieldName = 'Movimento|Valor'
            Footers = <>
            Width = 125
          end
          item
            EditButtons = <>
            FieldName = 'Movimento|Data'
            Footers = <>
            Width = 119
          end>
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Observa'#231#227'o Movimento'
      ImageIndex = 1
      object DBMemo2: TDBMemo
        Tag = 1
        Left = 0
        Top = 0
        Width = 833
        Height = 277
        Align = alClient
        DataField = 'cOBSMov'
        DataSource = dsMovimento
        TabOrder = 0
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEspTit'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTitulo'
      '      ,nCdEmpresa'
      '      ,nCdEspTit'
      '      ,iParcela'
      '      ,nCdTerceiro'
      '      ,nCdCategFinanc'
      '      ,nCdMoeda'
      '      ,cSenso'
      '      ,dDtEmissao'
      '      ,dDtVenc'
      '      ,dDtLiq'
      '      ,dDtCad'
      '      ,nValTit'
      '      ,nValLiq'
      '      ,nSaldoTit'
      '      ,nValJuro'
      '      ,nValDesconto'
      '      ,cObsTit'
      '      ,nCdUnidadeNegocio'
      ',cNrTit'
      '  FROM Titulo'
      ' WHERE nCdEmpresa = :nCdEmpresa'
      '   AND nCdEspTit  = :nCdEspTit'
      '   AND nCdTitulo  = :nPK')
    Left = 616
    Top = 216
    object qryMasternCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryMasteriParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasternCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryMasternCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMastercSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryMasterdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
    object qryMasterdDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryMasternValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValLiq: TBCDField
      FieldName = 'nValLiq'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValJuro: TBCDField
      FieldName = 'nValJuro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValDesconto: TBCDField
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercObsTit: TMemoField
      FieldName = 'cObsTit'
      BlobType = ftMemo
    end
    object qryMasternCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryMastercNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
  end
  inherited dsMaster: TDataSource
    Left = 664
    Top = 216
  end
  inherited qryID: TADOQuery
    Left = 712
    Top = 216
  end
  inherited usp_ProximoID: TADOStoredProc
    Top = 272
  end
  inherited qryStat: TADOQuery
    Top = 288
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Top = 328
  end
  inherited ImageList1: TImageList
    Top = 312
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 648
    Top = 360
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 592
    Top = 296
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdUnidadeNegocio, cNmUnidadeNegocio'
      'FROM UnidadeNegocio'
      'WHERE nCdUnidadeNegocio = :nPK'
      
        'AND EXISTS(SELECT 1 FROM EmpresaUnidadeNegocio EUN WHERE EUN.nCd' +
        'UnidadeNegocio = UnidadeNegocio.nCdUnidadeNegocio'
      'AND EUN.nCdEmpresa = :nCdEmpresa)')
    Left = 312
    Top = 344
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 600
    Top = 304
  end
  object qryMTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT MT.nCdMTitulo "Movimento|ID"'
      
        '      ,CASE WHEN nCdBanco IS NOT NULL THEN (Convert(VARCHAR,nCdB' +
        'anco) + '#39' - '#39' + Convert(VARCHAR,cAgencia) + '#39' - '#39' + nCdConta)'
      '            ELSE nCdConta'
      '       END as "Opera'#231#227'o | Conta"'
      '      ,MT.nCdOperacao as "Opera'#231#227'o | C'#243'digo"'
      '      ,OP.cNmOperacao as "Opera'#231#227'o | Descri'#231#227'o"'
      '      ,MT.nCdFormaPagto as "Forma Movimento | C'#243'digo"'
      '      ,FP.cNmFormaPagto as "Forma Movimento | Descri'#231#227'o"'
      '      ,iNrCheque as  "Movimento|Nr.Cheque"'
      
        '      ,nValMov   as "Movimento|Valor"                           ' +
        '   '
      '      ,dDtMov    as "Movimento|Data"             '
      '      ,dDtCad    as "Movimento|Cadastro"              '
      '      ,dDtCancel as "Movimento|Cancel."              '
      '      ,cNrDoc    as "Movimento|Nr. Doc"      '
      '      ,dDtContab as "Movimento|Contab."'
      '      ,UC.cNmUsuario as "Usu'#225'rio|Movimento"'
      '      ,UL.cNmUsuario as "Usu'#225'rio|Cancelamento"'
      '      ,cOBSMov'
      '  FROM MTitulo MT'
      
        '       LEFT  JOIN ContaBancaria CB ON CB.nCdContaBancaria = MT.n' +
        'CdContaBancaria'
      
        '       INNER JOIN Operacao      OP ON OP.nCdOperacao      = MT.n' +
        'CdOperacao'
      
        '       LEFT  JOIN FormaPagto    FP ON FP.nCdFormaPagto    = MT.n' +
        'CdFormaPagto'
      
        '       LEFT  JOIN Usuario       UC ON UC.nCdUsuario       = MT.n' +
        'CdUsuarioCad'
      
        '       LEFT  JOIN Usuario       UL ON UL.nCdUsuario       = MT.n' +
        'CdUsuarioCancel'
      ' WHERE MT.nCdTitulo = :nPK'
      ' ORDER BY 1')
    Left = 976
    Top = 168
    object qryMTituloMovimentoID: TAutoIncField
      FieldName = 'Movimento|ID'
      ReadOnly = True
    end
    object qryMTituloOperaoCdigo: TIntegerField
      FieldName = 'Opera'#231#227'o | C'#243'digo'
    end
    object qryMTituloOperaoDescrio: TStringField
      FieldName = 'Opera'#231#227'o | Descri'#231#227'o'
      Size = 50
    end
    object qryMTituloFormaMovimentoCdigo: TIntegerField
      FieldName = 'Forma Movimento | C'#243'digo'
    end
    object qryMTituloFormaMovimentoDescrio: TStringField
      FieldName = 'Forma Movimento | Descri'#231#227'o'
      Size = 50
    end
    object qryMTituloMovimentoNrCheque: TIntegerField
      FieldName = 'Movimento|Nr.Cheque'
    end
    object qryMTituloMovimentoValor: TBCDField
      FieldName = 'Movimento|Valor'
      Precision = 12
      Size = 2
    end
    object qryMTituloMovimentoData: TDateTimeField
      FieldName = 'Movimento|Data'
    end
    object qryMTituloMovimentoCadastro: TDateTimeField
      FieldName = 'Movimento|Cadastro'
    end
    object qryMTituloMovimentoCancel: TDateTimeField
      FieldName = 'Movimento|Cancel.'
    end
    object qryMTituloMovimentoNrDoc: TStringField
      FieldName = 'Movimento|Nr. Doc'
      FixedChar = True
      Size = 15
    end
    object qryMTituloMovimentoContab: TDateTimeField
      FieldName = 'Movimento|Contab.'
    end
    object qryMTituloUsurioMovimento: TStringField
      FieldName = 'Usu'#225'rio|Movimento'
      Size = 50
    end
    object qryMTituloUsurioCancelamento: TStringField
      FieldName = 'Usu'#225'rio|Cancelamento'
      Size = 50
    end
    object qryMTituloOperaoConta: TStringField
      FieldName = 'Opera'#231#227'o | Conta'
      ReadOnly = True
      Size = 81
    end
    object qryMTitulocOBSMov: TMemoField
      FieldName = 'cOBSMov'
      BlobType = ftMemo
    end
  end
  object dsMovimento: TDataSource
    DataSet = qryMTitulo
    Left = 1016
    Top = 168
  end
end
