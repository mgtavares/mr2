unit fSobre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, ShellAPI, JPEG,
  ExtCtrls, cxControls, cxContainer, cxEdit, cxGroupBox;

type
  TfrmSobre = class(TForm)
    lblWebsite: TLabel;
    Image1: TImage;
    Label4: TLabel;
    Label5: TLabel;
    lblVersaoSistema: TLabel;
    Label6: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Bevel1: TBevel;
    procedure lblWebsiteClick(Sender: TObject);
    procedure lblWebsiteMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure lblWebsiteMouseLeave(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSobre: TfrmSobre;

implementation

uses
  fMenu;

{$R *.dfm}

procedure TfrmSobre.lblWebsiteClick(Sender: TObject);
begin
  ShellExecute(Handle,'open','www.er2soft.com.br','',Nil,0);
end;

procedure TfrmSobre.lblWebsiteMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  lblWebsite.Font.Style := [fsUnderline];
end;

procedure TfrmSobre.lblWebsiteMouseLeave(Sender: TObject);
begin
  lblWebsite.Font.Style := [];
end;

procedure TfrmSobre.FormShow(Sender: TObject);
begin
  lblVersaoSistema.Caption := frmMenu.cNmVersaoSistema;
end;

end.
