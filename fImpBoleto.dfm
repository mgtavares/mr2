inherited frmImpBoleto: TfrmImpBoleto
  Left = 0
  Top = 0
  Width = 1152
  Height = 812
  Caption = 'Impress'#227'o de Boleto'
  OldCreateOrder = True
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 217
    Width = 1144
    Height = 568
  end
  inherited ToolBar1: TToolBar
    Width = 1144
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 217
    Width = 1144
    Height = 568
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 568
    ClientRectRight = 1144
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'T'#237'tulos'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 49
        Width = 1144
        Height = 496
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          DataController.DataSource = dsTemp
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '##,#00.00'
              Kind = skSum
              FieldName = 'nSaldoTit'
              Column = cxGrid1DBTableView1nSaldoTit
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnHiding = True
          OptionsCustomize.ColumnMoving = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GridLines = glVertical
          OptionsView.GroupByBox = False
          Styles.Selection = frmMenu.LinhaAzul
          Styles.Header = frmMenu.Header
          object cxGrid1DBTableView1cFlgOK: TcxGridDBColumn
            DataBinding.FieldName = 'cFlgOK'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.DisplayChecked = '1'
            Properties.DisplayUnchecked = '0'
            Properties.ValueChecked = '1'
            Properties.ValueUnchecked = '0'
          end
          object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTitulo'
            Options.Editing = False
          end
          object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
            DataBinding.FieldName = 'cNrTit'
            Options.Editing = False
            Width = 85
          end
          object cxGrid1DBTableView1cNrNF: TcxGridDBColumn
            DataBinding.FieldName = 'cNrNF'
            Options.Editing = False
            Width = 73
          end
          object cxGrid1DBTableView1iParcela: TcxGridDBColumn
            DataBinding.FieldName = 'iParcela'
            Options.Editing = False
          end
          object cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn
            DataBinding.FieldName = 'dDtEmissao'
            Options.Editing = False
            Width = 97
          end
          object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
            DataBinding.FieldName = 'dDtVenc'
            Options.Editing = False
          end
          object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
            DataBinding.FieldName = 'nSaldoTit'
            Options.Editing = False
            Width = 94
          end
          object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
            DataBinding.FieldName = 'cNmTerceiro'
            Options.Editing = False
            Width = 194
          end
          object cxGrid1DBTableView1cNmFormaPagto: TcxGridDBColumn
            DataBinding.FieldName = 'cNmFormaPagto'
            Options.Editing = False
            Width = 219
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1144
        Height = 49
        Align = alTop
        TabOrder = 1
        object btnSelecionarTodosTit: TcxButton
          Left = 6
          Top = 12
          Width = 121
          Height = 29
          Caption = 'Selecionar Todos'
          TabOrder = 0
          OnClick = btnSelecionarTodosTitClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333300000000
            0EEE333377777777777733330FF00FBFB0EE33337F37733F377733330F0BFB0B
            FB0E33337F73FF73337733330FF000BFBFB033337F377733333733330FFF0BFB
            FBF033337FFF733F333733300000BF0FBFB03FF77777F3733F37000FBFB0F0FB
            0BF077733FF7F7FF7337E0FB00000000BF0077F377777777F377E0BFBFBFBFB0
            F0F077F3333FFFF7F737E0FBFB0000000FF077F3337777777337E0BFBFBFBFB0
            FFF077F3333FFFF73FF7E0FBFB00000F000077FF337777737777E00FBFBFB0FF
            0FF07773FFFFF7337F37003000000FFF0F037737777773337F7333330FFFFFFF
            003333337FFFFFFF773333330000000003333333777777777333}
          LookAndFeel.NativeStyle = True
          NumGlyphs = 2
        end
        object btnLimparSelecaoTit: TcxButton
          Left = 134
          Top = 12
          Width = 121
          Height = 29
          Caption = 'Limpar Sele'#231#227'o'
          TabOrder = 1
          OnClick = btnLimparSelecaoTitClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000130B0000130B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
            333333333333333333FF33333333333330003333333333333777333333333333
            300033FFFFFF3333377739999993333333333777777F3333333F399999933333
            3300377777733333337733333333333333003333333333333377333333333333
            3333333333333333333F333333333333330033333F33333333773333C3333333
            330033337F3333333377333CC3333333333333F77FFFFFFF3FF33CCCCCCCCCC3
            993337777777777F77F33CCCCCCCCCC399333777777777737733333CC3333333
            333333377F33333333FF3333C333333330003333733333333777333333333333
            3000333333333333377733333333333333333333333333333333}
          LookAndFeel.NativeStyle = True
          NumGlyphs = 2
        end
        object btnImprimirBoleto: TcxButton
          Left = 262
          Top = 12
          Width = 121
          Height = 29
          Caption = 'Imprimir'
          TabOrder = 2
          OnClick = btnImprimirBoletoClick
          Glyph.Data = {
            0A020000424D0A0200000000000036000000280000000C0000000D0000000100
            180000000000D401000000000000000000000000000000000000C2C2C29E9E9E
            656464656464656464656464656464656464656464656464898A89B9BABAD5D5
            D5515757313332484E4E3133323D42423D42423D42423D4242191D23515757C6
            C6C68C908F191D23313332313332313332313332313332313332313332191D23
            040506898A89656464898A89DADDD7B9BABABEBEBEBEBEBEBEBEBEBEBEBEBEBA
            BCCCCCCC5157576564647172723D4242898A89777B7B7B85857B85857B85857B
            85857B85859392923D4242777B7BB6B6B63133327B8585AEAEAEA4A8A8A4A8A8
            A4A8A8A4A8A8A4A8A8484E4E313332B9BABAD5D5D5484E4EC6C6C6FCFEFEFCFE
            FEFCFEFEFCFEFEFCFEFEFCFEFE898A89484E4EC6C6C6CCCCCC484E4EC6C6C6FC
            FEFEFCFEFEFCFEFEFCFEFEFCFEFEFCFEFE7B85853D4242C2C2C2CCCCCC484E4E
            C6C6C6FCFEFEFCFEFEFCFEFEFCFEFEFCFEFED5D5D57172723D4242C2C2C2CCCC
            CC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE7B8585191D23191D23515757C6
            C6C6CCCCCC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE717272484E4E3D4242
            939292C6C6C6CCCCCC484E4E313332575D5E515757515757575D5E191D23191D
            23939292CCCCCCBEBEBEC2C2C2B6B6B69E9E9E9E9E9E9E9E9E9E9E9E9E9E9EA4
            A8A8AEAEAEC6C6C6BEBEBEBEBEBE}
          LookAndFeel.NativeStyle = True
        end
        object btnAgruparTitulos: TcxButton
          Left = 390
          Top = 12
          Width = 121
          Height = 29
          Caption = 'Agrupar T'#237'tulos'
          TabOrder = 3
          OnClick = btnAgruparTitulosClick
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            2000000000000004000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000000000000000
            0000000000000000000000000000000000000000000000000000FF00FF00FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF0000000000084A2100084A
            2100084A2100084A2100084A2100084A2100084A210000000000000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000018A54A0018A54A0018A54A0018A54A00084A21000000000000000000D6A5
            8C00D6A58C00D6A58C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5
            F7000000000018A54A0018A54A0018A54A00084A21000000000000000000EFFF
            FF00FFEFDE00FFEFDE00000000000000000000000000000000000000000000FF
            FF0052B5F7000000000018A54A0018A54A00084A21000000000000000000EFFF
            FF00FFEFDE00FFEFDE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FF
            FF0000FFFF0052B5F7000000000018A54A00084A21000000000000000000EFFF
            FF00FFEFDE00FFEFDE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FF
            FF0000FFFF00EFFFFF000000000018A54A00084A21000000000000000000EFFF
            FF00FFEFDE00FFEFDE00000000000000000000000000000000000000000000FF
            FF00EFFFFF000000000018A54A0018A54A00084A21000000000000000000EFFF
            FF00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFF
            FF000000000018A54A0018A54A0018A54A00084A21000000000000000000EFFF
            FF00FFEFDE00FFEFDE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000
            000018A54A0018A54A0018A54A0018A54A00084A21000000000000000000EFFF
            FF00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A5
            4A0018A54A0018A54A0018A54A0031636300316363000000000000000000EFFF
            FF00FFEFDE00FFEFDE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6
            B5009CD6B5009CD6B5009CD6B5003163630000000000FF00FF0000000000EFFF
            FF00FFEFDE00FFEFDE00FFEFDE00FFEFDE00FFEFDE00D6A58C00000000000000
            000000000000000000000000000000000000FF00FF00FF00FF0000000000EFFF
            FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000000000FF00
            FF00FF00FF00FF00FF00C6DEC600FF00FF00FF00FF00FF00FF00000000000000
            000000000000000000000000000000000000000000000000000000000000FF00
            FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
          LookAndFeel.NativeStyle = True
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Instru'#231#245'es'
      ImageIndex = 1
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1128
        Height = 555
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsInstrucaoContaBancaria
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTituloInstrucaoBoleto'
            Footers = <>
            Width = 100
          end
          item
            EditButtons = <>
            FieldName = 'cNmMensagem'
            Footers = <>
            Width = 309
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1144
    Height = 188
    Align = alTop
    TabOrder = 1
    object Label3: TLabel
      Tag = 1
      Left = 7
      Top = 91
      Width = 87
      Height = 13
      Alignment = taRightJustify
      Caption = 'Forma Pagamento'
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label8: TLabel
      Tag = 1
      Left = 30
      Top = 115
      Width = 64
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Emiss'#227'o'
    end
    object Label11: TLabel
      Tag = 1
      Left = 178
      Top = 115
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label1: TLabel
      Tag = 1
      Left = 317
      Top = 115
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Vencimento'
    end
    object Label2: TLabel
      Tag = 1
      Left = 482
      Top = 115
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label4: TLabel
      Tag = 1
      Left = 55
      Top = 67
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro'
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label6: TLabel
      Tag = 1
      Left = 599
      Top = 115
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero NF'
    end
    object Label5: TLabel
      Tag = 1
      Left = 14
      Top = 43
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta Portadora'
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label7: TLabel
      Tag = 1
      Left = 53
      Top = 19
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object edtFormaPagto: TER2LookupMaskEdit
      Left = 96
      Top = 83
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 3
      Text = '         '
      CodigoLookup = 15
      QueryLookup = qryFormaPagamento
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 162
      Top = 83
      Width = 557
      Height = 21
      DataField = 'cNmFormaPagto'
      DataSource = dsFormaPagto
      TabOrder = 4
    end
    object MaskEditEmissaoInicial: TMaskEdit
      Left = 96
      Top = 107
      Width = 67
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 5
      Text = '  /  /    '
    end
    object MaskEditEmissaoFinal: TMaskEdit
      Left = 198
      Top = 107
      Width = 71
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 6
      Text = '  /  /    '
    end
    object MaskEditVenctoInicial: TMaskEdit
      Left = 401
      Top = 107
      Width = 68
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 7
      Text = '  /  /    '
    end
    object MaskEditVenctoFinal: TMaskEdit
      Left = 502
      Top = 107
      Width = 71
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 8
      Text = '  /  /    '
    end
    object edtTerceiro: TER2LookupMaskEdit
      Left = 96
      Top = 59
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 2
      Text = '         '
      CodigoLookup = 17
      WhereAdicional.Strings = (
        
          'EXISTS (SELECT nCdTerceiro FROM TerceiroTipoTerceiro WHERE Terce' +
          'iroTipoTerceiro.nCdTerceiro = vTerceiros.nCdTerceiro AND Terceir' +
          'oTipoTerceiro.nCdTipoTerceiro=2)')
      QueryLookup = qryTerceiro
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 162
      Top = 59
      Width = 557
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = dsTerceiro
      TabOrder = 12
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 162
      Top = 35
      Width = 65
      Height = 21
      DataField = 'nCdBanco'
      DataSource = dsContaBancaria
      TabOrder = 13
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 230
      Top = 35
      Width = 100
      Height = 21
      DataField = 'cNmBanco'
      DataSource = dsContaBancaria
      TabOrder = 14
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 333
      Top = 35
      Width = 65
      Height = 21
      DataField = 'cAgencia'
      DataSource = dsContaBancaria
      TabOrder = 15
    end
    object DBEdit6: TDBEdit
      Tag = 1
      Left = 401
      Top = 35
      Width = 77
      Height = 21
      DataField = 'nCdConta'
      DataSource = dsContaBancaria
      TabOrder = 16
    end
    object DBEdit7: TDBEdit
      Tag = 1
      Left = 481
      Top = 35
      Width = 238
      Height = 21
      DataField = 'cNmTitular'
      DataSource = dsContaBancaria
      TabOrder = 17
    end
    object MaskEditNumeroNF: TMaskEdit
      Left = 654
      Top = 107
      Width = 65
      Height = 21
      TabOrder = 9
    end
    object edtContaBancaria: TER2LookupMaskEdit
      Left = 96
      Top = 35
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 1
      Text = '         '
      OnExit = edtContaBancariaExit
      OnBeforeLookup = edtContaBancariaBeforeLookup
      OnBeforePosicionaQry = edtContaBancariaBeforePosicionaQry
      CodigoLookup = 37
      WhereAdicional.Strings = (
        '(cFlgCofre = 0 and cFlgEmiteBoleto = 1)')
      QueryLookup = qryContaBancaria
    end
    object btnExibirTitulos: TcxButton
      Left = 96
      Top = 152
      Width = 121
      Height = 29
      Caption = 'Exibir T'#237'tulos'
      TabOrder = 11
      OnClick = btnExibirTitulosClick
      Glyph.Data = {
        36040000424D3604000000000000360000002800000010000000100000000100
        2000000000000004000000000000000000000000000000000000FF00FF00C694
        8C00C6948C00C6948C00C6948C00C6948C00C6948C00FF00FF00FF00FF00FF00
        FF00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
        000000000000000000000000000000000000C6948C00FF00FF00FF00FF000000
        00000000000000000000000000000000000000000000C6948C0000000000EFFF
        FF00C6948C00C6948C006363630000000000C6948C00C6948C00C6948C000000
        00000000000000000000000000000000000000000000C6948C0000000000EFFF
        FF00C6948C00C6948C0063636300000000000000000000000000000000000000
        000000000000EFFFFF00EFFFFF000000000000000000C6948C0000000000EFFF
        FF00C6948C00C6948C006363630000000000C6948C00C6948C00000000000000
        000000000000EFFFFF00EFFFFF0000000000000000000000000000000000EFFF
        FF00C6948C00C6948C006363630000000000C6948C00C6948C0000000000EFFF
        FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000EFFF
        FF00C6948C00C6948C006363630000000000C6948C00C6948C0000000000EFFF
        FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000EFFF
        FF00C6948C00C6948C0063636300000000000000000000000000000000000000
        000000000000EFFFFF00EFFFFF0000000000000000000000000000000000EFFF
        FF00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00C694
        8C0000000000EFFFFF00EFFFFF000000000000000000C6948C0000000000EFFF
        FF00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00C694
        8C000000000000000000000000000000000000000000C6948C0000000000EFFF
        FF00C6948C00C6948C00C6948C00000000000000000000000000000000000000
        0000C6948C00C6948C00C6948C006363630000000000C6948C00000000000000
        0000EFFFFF00C6948C006363630000000000C6948C00FF00FF00FF00FF000000
        0000EFFFFF00C6948C00636363000000000000000000FF00FF00FF00FF000000
        0000EFFFFF00C6948C006363630000000000C6948C00FF00FF00FF00FF000000
        0000EFFFFF00C6948C006363630000000000C6948C00FF00FF00FF00FF000000
        0000EFFFFF00C6948C006363630000000000C6948C00FF00FF00FF00FF000000
        0000EFFFFF00C6948C006363630000000000C6948C00FF00FF00FF00FF000000
        0000EFFFFF00C6948C006363630000000000C6948C00FF00FF00FF00FF000000
        0000EFFFFF00C6948C006363630000000000C6948C00FF00FF00FF00FF000000
        000000000000000000000000000000000000FF00FF00FF00FF00FF00FF000000
        000000000000000000000000000000000000FF00FF00FF00FF00}
      LookAndFeel.NativeStyle = True
    end
    object CheckBox2: TCheckBox
      Left = 96
      Top = 132
      Width = 137
      Height = 17
      Caption = 'Reemiss'#227'o de boletos'
      TabOrder = 10
    end
    object edtEmpresa: TER2LookupMaskEdit
      Left = 96
      Top = 11
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      OnBeforePosicionaQry = edtEmpresaBeforePosicionaQry
      CodigoLookup = 8
      WhereAdicional.Strings = (
        '')
      QueryLookup = qryEmpresa
    end
    object DBEdit8: TDBEdit
      Tag = 1
      Left = 162
      Top = 11
      Width = 65
      Height = 21
      DataField = 'cSigla'
      DataSource = DataSource1
      TabOrder = 18
    end
    object DBEdit9: TDBEdit
      Tag = 1
      Left = 230
      Top = 11
      Width = 489
      Height = 21
      DataField = 'cNmEmpresa'
      DataSource = DataSource1
      TabOrder = 19
    end
  end
  inherited ImageList1: TImageList
    Left = 912
    Top = 56
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C00636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF81C00000FE00FFFF01800000
      FE00C00300000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010181000000018001818100000003800181810000
      0077C00381810000007FFFFF8383000000000000000000000000000000000000
      000000000000}
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,Banco.nCdBanco'
      '      ,Banco.cNmBanco'
      '      ,ContaBancaria.cAgencia'
      '      ,ContaBancaria.nCdConta'
      '      ,ContaBancaria.cNmTitular'
      '      ,ContaBancaria.iUltimoBoleto'
      '      ,ContaBancaria.cPrimeiraInstrucaoBoleto'
      '      ,ContaBancaria.cSegundaInstrucaoBoleto'
      '      ,ContaBancaria.nCdTabTipoModeloImpBoleto'
      '      ,ContaBancaria.iSeqNossoNumero'
      '      ,ContaBancaria.nCdTabTipoRespEmissaoBoleto'
      '  FROM ContaBancaria'
      
        '       LEFT JOIN Banco ON Banco.nCdBanco = ContaBancaria.nCdBanc' +
        'o'
      ' WHERE nCdContaBancaria         = :nPK'
      '   AND cFlgEmiteBoleto          = 1'
      '   AND ContaBancaria.nCdEmpresa = :nCdEmpresa'
      '')
    Left = 720
    Top = 128
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryContaBancariacNmBanco: TStringField
      FieldName = 'cNmBanco'
      Size = 50
    end
    object qryContaBancariacAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryContaBancariacNmTitular: TStringField
      FieldName = 'cNmTitular'
      Size = 50
    end
    object qryContaBancariaiUltimoBoleto: TIntegerField
      FieldName = 'iUltimoBoleto'
    end
    object qryContaBancariacPrimeiraInstrucaoBoleto: TStringField
      FieldName = 'cPrimeiraInstrucaoBoleto'
      Size = 100
    end
    object qryContaBancariacSegundaInstrucaoBoleto: TStringField
      FieldName = 'cSegundaInstrucaoBoleto'
      Size = 100
    end
    object qryContaBancarianCdTabTipoModeloImpBoleto: TIntegerField
      FieldName = 'nCdTabTipoModeloImpBoleto'
    end
    object qryContaBancariaiSeqNossoNumero: TIntegerField
      FieldName = 'iSeqNossoNumero'
    end
    object qryContaBancarianCdTabTipoRespEmissaoBoleto: TIntegerField
      FieldName = 'nCdTabTipoRespEmissaoBoleto'
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro   '
      'WHERE nCdTerceiro = :nPk')
    Left = 720
    Top = 161
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryFormaPagamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdFormaPagto'
      '       ,cNmFormaPagto'
      '  FROM FormaPagto'
      ' WHERE cFlgAtivo = 1'
      '       AND nCdFormaPagto = :nPk')
    Left = 720
    Top = 192
    object qryFormaPagamentonCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryFormaPagamentocNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 752
    Top = 160
  end
  object dsContaBancaria: TDataSource
    DataSet = qryContaBancaria
    Left = 752
    Top = 128
  end
  object dsFormaPagto: TDataSource
    DataSet = qryFormaPagamento
    Left = 752
    Top = 192
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nPK')
    Left = 1016
    Top = 40
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'nCdFormaPagto'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'dDtEmissaoInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtEmissaoFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtVenctoInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtVenctoFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'cFlgRemissao'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cCFlgTodosPendentes'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cNRNF'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      ''
      'DECLARE @nCdEmpresa           int'
      '       ,@nCdTerceiro          int'
      '       ,@nCdFormaPagto        int'
      '       ,@dDtEmissaoInicial    Datetime'
      '       ,@dDtEmissaoFinal      Datetime'
      '       ,@dDtVenctoInicial     Datetime'
      '       ,@dDtVenctoFinal       Datetime'
      '       ,@cCFlgRemissao        int'
      '       ,@cFlgTodosPendentes  int'
      '       ,@cNRNF                Varchar(15)'
      ''
      'SET @nCdEmpresa         = :nCdEmpresa'
      'SET @nCdTerceiro        = :nCdTerceiro'
      'SET @nCdFormaPagto      = :nCdFormaPagto'
      'SET @dDtEmissaoInicial  = :dDtEmissaoInicial'
      'SET @dDtEmissaoFinal    = :dDtEmissaoFinal'
      'SET @dDtVenctoInicial   = :dDtVenctoInicial'
      'SET @dDtVenctoFinal     = :dDtVenctoFinal'
      'SET @cCFlgRemissao      = :cFlgRemissao'
      'SET @cFlgTodosPendentes = :cCFlgTodosPendentes'
      'SET @cNRNF              = :cNRNF'
      ''
      'IF (OBJECT_ID('#39'tempdb..#Temp_TitulosBoleto'#39') IS NULL)'
      'BEGIN'
      #9'CREATE TABLE #Temp_TitulosBoleto(nCdTitulo      int PRIMARY KEY'
      #9#9#9#9#9#9#9'   ,cFlgOK         int default 0 not null'
      ' '#9#9#9#9#9#9#9'   ,cNrTit         char(17)'
      #9#9#9#9#9#9#9'   ,cNrNF          char(15)'
      #9#9#9#9#9#9#9'   ,iParcela       int'
      #9#9#9#9#9#9#9'   ,dDtEmissao     datetime'
      #9#9#9#9#9#9#9'   ,dDtVenc        datetime'
      #9#9#9#9#9#9#9'   ,nSaldoTit      decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,nCdTerceiro    int'
      #9#9#9#9#9#9#9'   ,cNmTerceiro    varchar(50)'
      '                 ,nCdFormaPagto  int'
      '                 ,cNmFormaPagto  varchar(50))'
      'END'
      ''
      'INSERT INTO #Temp_TitulosBoleto (nCdTitulo'
      #9#9#9#9#9'            ,cNrTit'
      #9#9#9#9#9'            ,cNrNF'
      #9#9#9#9#9'            ,iParcela'
      #9#9#9#9#9'        '#9',dDtEmissao'
      #9#9#9#9#9'      '#9'    ,dDtVenc'
      #9#9#9#9#9'            ,nSaldoTit'
      #9#9#9#9#9'            ,nCdTerceiro'
      #9#9#9#9#9'            ,cNmTerceiro'
      '                      ,nCdFormaPagto'
      '                      ,cNmFormaPagto)'
      'SELECT nCdTitulo'
      '      ,cNrTit'
      '      ,cNrNf'
      '      ,iParcela'
      '      ,dDtEmissao'
      '      ,dDtVenc'
      '      ,nSaldoTit'
      '      ,Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,FormaPagto.nCdFormaPagto'
      '      ,FormaPagto.cNmFormaPagto'
      '  FROM TITULO'
      
        '  INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Titulo.nCd' +
        'Terceiro'
      
        '  INNER JOIN FormaPagto ON FormaPagto.nCdFormaPagto = Titulo.nCd' +
        'FormaPagtoTit'
      
        'WHERE ((nCdEmpresa         = @nCdEmpresa)     OR (@nCdEmpresa   ' +
        ' = 0))'
      
        '  AND ((Titulo.nCdTerceiro = @nCdTerceiro)    OR (@nCdTerceiro  ' +
        ' = 0))'
      
        '  AND ((nCdFormaPagtoTit   = @nCdFormaPagto)  OR (@nCdFormaPagto' +
        ' = 0))'
      
        '  AND ((dDtEmissao >= Convert(DATETIME,@dDtEmissaoInicial,103)) ' +
        'OR (@dDtEmissaoInicial = '#39'01/01/1900'#39'))'
      
        '  AND ((dDtEmissao <  Convert(DATETIME,@dDtEmissaoFinal,103)+1) ' +
        'OR (@dDtEmissaoFinal   = '#39'01/01/1900'#39'))'
      
        '  AND ((dDtVenc    >= Convert(DATETIME,@dDtVenctoInicial,103))  ' +
        'OR (@dDtVenctoInicial  = '#39'01/01/1900'#39'))'
      
        '  AND ((dDtVenc    <  Convert(DATETIME,@dDtVenctoFinal,103)+1)  ' +
        'OR (@dDtVenctoFinal    = '#39'01/01/1900'#39'))'
      '  AND dDtLiq IS NULL'
      '  AND cSenso = '#39'C'#39
      '  AND dDtCancel IS NULL'
      '  AND nSaldoTit > 0'
      
        '  AND (((@cCFlgRemissao = 1)      AND (Titulo.cFlgBoletoImpresso' +
        ' = 1)) OR (@cCFlgRemissao = 0))'
      
        '  AND (((@cFlgTodosPendentes = 1) AND (Titulo.cFlgBoletoImpresso' +
        ' = 1)) OR (@cFlgTodosPendentes = 0))'
      '  AND ((Titulo.cNrNF = @cNRNF) OR (@cNRNF = '#39#39'))'
      ''
      'SELECT *'
      'FROM #Temp_TitulosBoleto'
      'ORDER BY cNmTerceiro,dDtEmissao,cNrNf,iParcela'
      ''
      '')
    Left = 1160
    Top = 360
    object qryTitulosnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTituloscFlgOK: TIntegerField
      FieldName = 'cFlgOK'
    end
    object qryTituloscNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTituloscNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTitulosiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTitulosdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTitulosdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulosnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTituloscNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTitulosnCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryTituloscNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTempBeforePost
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_TitulosBoleto'#39') IS NULL)'
      'BEGIN'
      #9'CREATE TABLE #Temp_TitulosBoleto(nCdTitulo      int PRIMARY KEY'
      #9#9#9#9#9#9#9'   ,cFlgOK         int default 0 not null'
      ' '#9#9#9#9#9#9#9'   ,cNrTit         char(17)'
      #9#9#9#9#9#9#9'   ,cNrNF          char(15)'
      #9#9#9#9#9#9#9'   ,iParcela       int'
      '            '#9'   ,dDtEmissao     datetime'
      #9#9#9#9#9#9#9'   ,dDtVenc        datetime'
      #9#9#9#9#9#9#9'   ,nSaldoTit      decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,nCdTerceiro    int'
      #9#9#9#9#9#9#9'   ,cNmTerceiro    varchar(50)'
      '                 ,nCdFormaPagto  int'
      '                 ,cNmFormaPagto  varchar(50)'
      '                 ,nCdEspTit      int'
      
        '                 ,cFlgPermitirEmissaoBoleto int default 0 not nu' +
        'll)'
      'END'
      ''
      'SELECT *'
      'FROM #Temp_TitulosBoleto'
      'ORDER BY cNmTerceiro,dDtEmissao,cNrNf,iParcela')
    Left = 1024
    Top = 145
    object qryTempnCdTitulo: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'nCdTitulo'
    end
    object qryTempcFlgOK: TIntegerField
      DisplayLabel = 'OK'
      FieldName = 'cFlgOK'
    end
    object qryTempcNrTit: TStringField
      DisplayLabel = 'Nr.T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTempcNrNF: TStringField
      DisplayLabel = 'Nr NF'
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTempiParcela: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'iParcela'
    end
    object qryTempdDtEmissao: TDateTimeField
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryTempdDtVenc: TDateTimeField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'dDtVenc'
    end
    object qryTempnSaldoTit: TBCDField
      DisplayLabel = 'Saldo T'#237'tulo'
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempnCdTerceiro: TIntegerField
      DisplayLabel = 'Cod. Terceiro'
      FieldName = 'nCdTerceiro'
    end
    object qryTempcNmTerceiro: TStringField
      DisplayLabel = 'Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTempnCdFormaPagto: TIntegerField
      DisplayLabel = 'C'#243'd. Forma Pagto'
      FieldName = 'nCdFormaPagto'
    end
    object qryTempcNmFormaPagto: TStringField
      DisplayLabel = 'Forma Pagto'
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryTempnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryTempcFlgPermitirEmissaoBoleto: TIntegerField
      FieldName = 'cFlgPermitirEmissaoBoleto'
    end
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 1192
    Top = 360
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_TitulosBoleto'#39') IS NULL)'
      'BEGIN'
      #9'CREATE TABLE #Temp_TitulosBoleto(nCdTitulo      int PRIMARY KEY'
      #9#9#9#9#9#9#9'   ,cFlgOK         int default 0 not null'
      ' '#9#9#9#9#9#9#9'   ,cNrTit         char(17)'
      #9#9#9#9#9#9#9'   ,cNrNF          char(15)'
      #9#9#9#9#9#9#9'   ,iParcela       int'
      #9#9#9#9#9#9#9'   ,dDtEmissao     datetime'
      #9#9#9#9#9#9#9'   ,dDtVenc        datetime'
      #9#9#9#9#9#9#9'   ,nSaldoTit      decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,nCdTerceiro    int'
      #9#9#9#9#9#9#9'   ,cNmTerceiro    varchar(50)'
      '                 ,nCdFormaPagto  int'
      '                 ,cNmFormaPagto  varchar(50)'
      '                 ,nCdEspTit      int'
      
        '                 ,cFlgPermitirEmissaoBoleto int default 0 not nu' +
        'll)'
      'END'
      '')
    Left = 1056
    Top = 112
  end
  object dsTemp: TDataSource
    DataSet = qryTemp
    Left = 1056
    Top = 144
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      #13#10'DECLARE @nCdEmpresa           int'#13#10'       ,@nCdTerceiro       ' +
      '   int'#13#10'       ,@nCdFormaPagto        int'#13#10'       ,@dDtEmissaoIn' +
      'icial    Varchar(10)'#13#10'       ,@dDtEmissaoFinal      Varchar(10)'#13 +
      #10'       ,@dDtVenctoInicial     Varchar(10)'#13#10'       ,@dDtVenctoFi' +
      'nal       Varchar(10)'#13#10'       ,@cFlgRemissao        int'#13#10'       ' +
      ',@cFlgTodosPendentes  int'#13#10'       ,@cNRNF                Varchar' +
      '(15)'#13#10'       ,@nCdTitulo             int'#13#10#13#10'SET @nCdEmpresa     ' +
      '    = :nCdEmpresa'#13#10'SET @nCdTerceiro        = :nCdTerceiro'#13#10'SET @' +
      'nCdFormaPagto      = :nCdFormaPagto'#13#10'SET @dDtEmissaoInicial  = :' +
      'dDtEmissaoInicial'#13#10'SET @dDtEmissaoFinal    = :dDtEmissaoFinal'#13#10'S' +
      'ET @dDtVenctoInicial   = :dDtVenctoInicial'#13#10'SET @dDtVenctoFinal ' +
      '    = :dDtVenctoFinal'#13#10'SET @cFlgRemissao       = :cFlgRemissao'#13#10 +
      'SET @cNRNF              = :cNRNF'#13#10'SET @nCdTitulo          = :nCd' +
      'Titulo'#13#10#13#10'TRUNCATE TABLE #Temp_TitulosBoleto'#13#10#13#10'IF (OBJECT_ID('#39't' +
      'empdb..#Temp_TitulosBoleto'#39') IS NULL)'#13#10'BEGIN'#13#10#9'CREATE TABLE #Tem' +
      'p_TitulosBoleto(nCdTitulo int PRIMARY KEY'#13#10#9#9#9#9#9#9#9'   ,cFlgOK    ' +
      '     int default 0 not null'#13#10' '#9#9#9#9#9#9#9'   ,cNrTit         char(17)' +
      #13#10#9#9#9#9#9#9#9'   ,cNrNF          char(15)'#13#10#9#9#9#9#9#9#9'   ,iParcela       ' +
      'int'#13#10'                               ,dDtEmissao     datetime'#13#10#9#9 +
      #9#9#9#9#9'   ,dDtVenc        datetime'#13#10#9#9#9#9#9#9#9'   ,nSaldoTit      deci' +
      'mal(12,2) default 0 not null'#13#10#9#9#9#9#9#9#9'   ,nCdTerceiro    int'#13#10#9#9#9 +
      #9#9#9#9'   ,cNmTerceiro    varchar(50)'#13#10'                            ' +
      '   ,nCdFormaPagto  int'#13#10'                               ,cNmForma' +
      'Pagto  varchar(50)'#13#10'                               ,nCdEspTit   ' +
      '   int  '#13#10'                               ,cFlgPermitirEmissaoBol' +
      'eto int default 0 not null)'#13#10'                                   ' +
      '                                                                ' +
      '              '#13#10'END'#13#10#13#10'INSERT INTO #Temp_TitulosBoleto (nCdTitul' +
      'o'#13#10#9#9#9#9#9'            ,cNrTit'#13#10#9#9#9#9#9'            ,cNrNF'#13#10#9#9#9#9#9'     ' +
      '       ,iParcela'#13#10'                                ,dDtEmissao'#13#10#9 +
      #9#9#9#9'      '#9'    ,dDtVenc'#13#10#9#9#9#9#9'            ,nSaldoTit'#13#10#9#9#9#9#9'     ' +
      '       ,nCdTerceiro'#13#10#9#9#9#9#9'            ,cNmTerceiro'#13#10'            ' +
      '                    ,nCdFormaPagto'#13#10'                            ' +
      '    ,cNmFormaPagto'#13#10'                                ,nCdEspTit'#13#10 +
      '                                ,cFlgPermitirEmissaoBoleto )    ' +
      '             '#13#10'SELECT nCdTitulo'#13#10'      ,cNrTit'#13#10'      ,cNrNf'#13#10'  ' +
      '    ,iParcela'#13#10'      ,dDtEmissao'#13#10'      ,dDtVenc'#13#10'      ,nSaldoT' +
      'it'#13#10'      ,Terceiro.nCdTerceiro'#13#10'      ,SUBSTRING(Terceiro.cNmTe' +
      'rceiro,1,50)'#13#10'      ,FormaPagto.nCdFormaPagto'#13#10'      ,FormaPagto' +
      '.cNmFormaPagto'#13#10'      ,nCdEspTit'#13#10'      ,Terceiro.cFlgPermitirEm' +
      'issaoBoleto '#13#10'  FROM TITULO'#13#10'       INNER JOIN Terceiro   ON Ter' +
      'ceiro.nCdTerceiro     = Titulo.nCdTerceiro'#13#10'       LEFT  JOIN Fo' +
      'rmaPagto ON FormaPagto.nCdFormaPagto = Titulo.nCdFormaPagtoTit'#13#10 +
      ' WHERE (   (Titulo.nCdTitulo = @nCdTitulo) '#13#10'        OR (    (@n' +
      'CdTitulo = 0)'#13#10'            AND ((nCdEmpresa         = @nCdEmpres' +
      'a)     OR (@nCdEmpresa    = 0))'#13#10#9#9'    AND ((Titulo.nCdTerceiro ' +
      '= @nCdTerceiro)    OR (@nCdTerceiro   = 0))'#13#10#9#9'    AND ((nCdForm' +
      'aPagtoTit   = @nCdFormaPagto)  OR (@nCdFormaPagto = 0))'#13#10#9#9'    A' +
      'ND ((dDtEmissao >= Convert(DATETIME,@dDtEmissaoInicial,103)) OR ' +
      '(@dDtEmissaoInicial = '#39'01/01/1900'#39'))'#13#10#9#9'    AND ((dDtEmissao <  ' +
      'Convert(DATETIME,@dDtEmissaoFinal,103)+1) OR (@dDtEmissaoFinal  ' +
      ' = '#39'01/01/1900'#39'))'#13#10#9#9'    AND ((dDtVenc    >= Convert(DATETIME,@d' +
      'DtVenctoInicial,103))  OR (@dDtVenctoInicial  = '#39'01/01/1900'#39'))'#13#10 +
      #9#9'    AND ((dDtVenc    <  Convert(DATETIME,@dDtVenctoFinal,103)+' +
      '1)  OR (@dDtVenctoFinal    = '#39'01/01/1900'#39'))'#13#10#9#9'    AND dDtLiq   ' +
      ' IS NULL'#13#10#9#9'    AND cSenso     = '#39'C'#39#13#10#9#9'    AND dDtCancel IS NUL' +
      'L'#13#10#9#9'    AND nSaldoTit  > 0'#13#10#9#9'    AND (   ((@cFlgRemissao = 1) ' +
      'AND (Titulo.cFlgBoletoImpresso = 1)) '#13#10'                 OR ((@cF' +
      'lgRemissao = 0) AND (Titulo.cFlgBoletoImpresso = 0))'#13#10'          ' +
      '      )'#13#10#9#9'    AND ((Titulo.cNrNF = @cNRNF) OR (@cNRNF = '#39#39')))'#13#10 +
      '       )'#13#10'   '#13#10#13#10'SELECT *'#13#10'  FROM #Temp_TitulosBoleto'#13#10' ORDER BY' +
      ' cNmTerceiro,dDtEmissao,cNrNf,iParcela'#13#10#13#10' '
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdFormaPagto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtEmissaoInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtEmissaoFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtVenctoInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtVenctoFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'cFlgRemissao'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cNRNF'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTitulo'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    Left = 1024
    Top = 113
  end
  object qryTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Titulo'
      'WHERE nCdTitulo = :nPk       ')
    Left = 1056
    Top = 40
    object qryTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTitulonCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryTitulonCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryTitulocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTituloiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTitulonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTitulonCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryTitulonCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryTitulocSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryTitulodDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTitulodDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
    end
    object qryTitulodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulodDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
    object qryTitulodDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryTitulodDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryTitulonValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTitulonValLiq: TBCDField
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryTitulonSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulonValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTitulonValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryTitulocObsTit: TMemoField
      FieldName = 'cObsTit'
      BlobType = ftMemo
    end
    object qryTitulonCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryTitulonCdBancoPortador: TIntegerField
      FieldName = 'nCdBancoPortador'
    end
    object qryTitulodDtRemPortador: TDateTimeField
      FieldName = 'dDtRemPortador'
    end
    object qryTitulodDtBloqTit: TDateTimeField
      FieldName = 'dDtBloqTit'
    end
    object qryTitulocObsBloqTit: TStringField
      FieldName = 'cObsBloqTit'
      Size = 50
    end
    object qryTitulonCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryTitulonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryTitulodDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryTitulonCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryTitulodDtCalcJuro: TDateTimeField
      FieldName = 'dDtCalcJuro'
    end
    object qryTitulonCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryTitulonCdCrediario: TIntegerField
      FieldName = 'nCdCrediario'
    end
    object qryTitulonCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTitulonCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryTitulocFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryTitulocFlgDocCobranca: TIntegerField
      FieldName = 'cFlgDocCobranca'
    end
    object qryTitulocNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTitulonCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
    object qryTitulocFlgDA: TIntegerField
      FieldName = 'cFlgDA'
    end
    object qryTitulonCdContaBancariaDA: TIntegerField
      FieldName = 'nCdContaBancariaDA'
    end
    object qryTitulonCdTipoLanctoDA: TIntegerField
      FieldName = 'nCdTipoLanctoDA'
    end
    object qryTitulonCdLojaTit: TIntegerField
      FieldName = 'nCdLojaTit'
    end
    object qryTitulocFlgInclusaoManual: TIntegerField
      FieldName = 'cFlgInclusaoManual'
    end
    object qryTitulodDtVencOriginal: TDateTimeField
      FieldName = 'dDtVencOriginal'
    end
    object qryTitulocFlgCartaoConciliado: TIntegerField
      FieldName = 'cFlgCartaoConciliado'
    end
    object qryTitulonCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryTitulonCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryTitulonValTaxaOperadora: TBCDField
      FieldName = 'nValTaxaOperadora'
      Precision = 12
      Size = 2
    end
    object qryTitulonCdLoteConciliacaoCartao: TIntegerField
      FieldName = 'nCdLoteConciliacaoCartao'
    end
    object qryTitulodDtIntegracao: TDateTimeField
      FieldName = 'dDtIntegracao'
    end
    object qryTitulonCdParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdParcContratoEmpImobiliario'
    end
    object qryTitulonValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object qryTitulonValMulta: TBCDField
      FieldName = 'nValMulta'
      Precision = 12
      Size = 2
    end
    object qryTitulodDtNegativacao: TDateTimeField
      FieldName = 'dDtNegativacao'
    end
    object qryTitulonCdUsuarioNeg: TIntegerField
      FieldName = 'nCdUsuarioNeg'
    end
    object qryTitulonCdItemListaCobrancaNeg: TIntegerField
      FieldName = 'nCdItemListaCobrancaNeg'
    end
    object qryTitulonCdLojaNeg: TIntegerField
      FieldName = 'nCdLojaNeg'
    end
    object qryTitulodDtReabNeg: TDateTimeField
      FieldName = 'dDtReabNeg'
    end
    object qryTitulocFlgCobradora: TIntegerField
      FieldName = 'cFlgCobradora'
    end
    object qryTitulonCdCobradora: TIntegerField
      FieldName = 'nCdCobradora'
    end
    object qryTitulodDtRemCobradora: TDateTimeField
      FieldName = 'dDtRemCobradora'
    end
    object qryTitulonCdUsuarioRemCobradora: TIntegerField
      FieldName = 'nCdUsuarioRemCobradora'
    end
    object qryTitulonCdItemListaCobRemCobradora: TIntegerField
      FieldName = 'nCdItemListaCobRemCobradora'
    end
    object qryTitulocFlgReabManual: TIntegerField
      FieldName = 'cFlgReabManual'
    end
    object qryTitulonCdUsuarioReabManual: TIntegerField
      FieldName = 'nCdUsuarioReabManual'
    end
    object qryTitulonCdFormaPagtoTit: TIntegerField
      FieldName = 'nCdFormaPagtoTit'
    end
    object qryTitulonPercTaxaJurosDia: TBCDField
      FieldName = 'nPercTaxaJurosDia'
      Precision = 12
    end
    object qryTitulonPercTaxaMulta: TBCDField
      FieldName = 'nPercTaxaMulta'
      Precision = 12
    end
    object qryTituloiDiaCarenciaJuros: TIntegerField
      FieldName = 'iDiaCarenciaJuros'
    end
    object qryTitulocFlgRenegociado: TIntegerField
      FieldName = 'cFlgRenegociado'
    end
    object qryTitulocCodBarra: TStringField
      FieldName = 'cCodBarra'
      Size = 100
    end
    object qryTitulonValEncargoCobradora: TBCDField
      FieldName = 'nValEncargoCobradora'
      Precision = 12
      Size = 2
    end
    object qryTitulocFlgRetCobradoraManual: TIntegerField
      FieldName = 'cFlgRetCobradoraManual'
    end
    object qryTitulonCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryTitulodDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryTitulonValIndiceCorrecao: TBCDField
      FieldName = 'nValIndiceCorrecao'
      Precision = 12
      Size = 8
    end
    object qryTitulonValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      Precision = 12
      Size = 2
    end
    object qryTitulocFlgNegativadoManual: TIntegerField
      FieldName = 'cFlgNegativadoManual'
    end
    object qryTitulocIDExternoTit: TStringField
      FieldName = 'cIDExternoTit'
    end
    object qryTitulonCdTipoAdiantamento: TIntegerField
      FieldName = 'nCdTipoAdiantamento'
    end
    object qryTitulocFlgAdiantamento: TIntegerField
      FieldName = 'cFlgAdiantamento'
    end
    object qryTitulonCdMTituloGerador: TIntegerField
      FieldName = 'nCdMTituloGerador'
    end
    object qryTitulocFlgBoletoImpresso: TIntegerField
      FieldName = 'cFlgBoletoImpresso'
    end
  end
  object qryLanctoFin: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM LanctoFin'
      ' WHERE nCdLanctoFin = :nPk')
    Left = 1088
    Top = 40
    object qryLanctoFinnCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryLanctoFinnCdResumoCaixa: TIntegerField
      FieldName = 'nCdResumoCaixa'
    end
    object qryLanctoFinnCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryLanctoFindDtLancto: TDateTimeField
      FieldName = 'dDtLancto'
    end
    object qryLanctoFinnCdTipoLancto: TIntegerField
      FieldName = 'nCdTipoLancto'
    end
    object qryLanctoFinnValLancto: TBCDField
      FieldName = 'nValLancto'
      Precision = 12
      Size = 2
    end
    object qryLanctoFinnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryLanctoFinnCdUsuarioAutor: TIntegerField
      FieldName = 'nCdUsuarioAutor'
    end
    object qryLanctoFincFlgManual: TIntegerField
      FieldName = 'cFlgManual'
    end
    object qryLanctoFindDtEstorno: TDateTimeField
      FieldName = 'dDtEstorno'
    end
    object qryLanctoFinnCdLanctoFinPai: TIntegerField
      FieldName = 'nCdLanctoFinPai'
    end
    object qryLanctoFincMotivoEstorno: TStringField
      FieldName = 'cMotivoEstorno'
      Size = 150
    end
    object qryLanctoFincHistorico: TStringField
      FieldName = 'cHistorico'
      Size = 50
    end
    object qryLanctoFincFlgConciliado: TIntegerField
      FieldName = 'cFlgConciliado'
    end
    object qryLanctoFinnCdUsuarioConciliacao: TIntegerField
      FieldName = 'nCdUsuarioConciliacao'
    end
    object qryLanctoFindDtConciliacao: TDateTimeField
      FieldName = 'dDtConciliacao'
    end
    object qryLanctoFincDocumento: TStringField
      FieldName = 'cDocumento'
      Size = 15
    end
    object qryLanctoFinnCdCheque: TIntegerField
      FieldName = 'nCdCheque'
    end
    object qryLanctoFindDtExtrato: TDateTimeField
      FieldName = 'dDtExtrato'
    end
    object qryLanctoFindDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryLanctoFinnCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
    object qryLanctoFinnCdTituloDA: TIntegerField
      FieldName = 'nCdTituloDA'
    end
    object qryLanctoFinnCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryLanctoFincFlgLiqCrediario: TIntegerField
      FieldName = 'cFlgLiqCrediario'
    end
    object qryLanctoFinnCdTituloCobranca: TIntegerField
      FieldName = 'nCdTituloCobranca'
    end
    object qryLanctoFincFlgCredNaoIdent: TIntegerField
      FieldName = 'cFlgCredNaoIdent'
    end
    object qryLanctoFinnSaldoPendenteIdent: TBCDField
      FieldName = 'nSaldoPendenteIdent'
      Precision = 12
      Size = 2
    end
    object qryLanctoFinnCdTerceiroDep: TIntegerField
      FieldName = 'nCdTerceiroDep'
    end
    object qryLanctoFinnCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryLanctoFindDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryLanctoFinnCdDepositoBancario: TIntegerField
      FieldName = 'nCdDepositoBancario'
    end
    object qryLanctoFinnCdUsuarioEstorno: TIntegerField
      FieldName = 'nCdUsuarioEstorno'
    end
    object qryLanctoFinnCdCCLancto: TIntegerField
      FieldName = 'nCdCCLancto'
    end
  end
  object SP_GERA_LANCTOFIN_EMISSAO_BOLETO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_LANCTOFIN_EMISSAO_BOLETO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdDoctoFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdBanco'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdTituloEmissao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 616
    Top = 168
  end
  object qryTempInstrucaoTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_TituloInstrucaoBoleto'#39') IS NULL)'
      'BEGIN'
      
        #9'CREATE TABLE #Temp_TituloInstrucaoBoleto(nCdTituloInstrucaoBole' +
        'to int PRIMARY KEY identity'
      
        '                                          ,cNmMensagem          ' +
        '    varchar(100)'
      
        '                                          ,cFlgMensagemPadrao   ' +
        '    int default 0 not null )'
      'END'
      ''
      'SELECT *'
      'FROM #Temp_TituloInstrucaoBoleto'
      '')
    Left = 1160
    Top = 392
    object qryTempInstrucaoTitulonCdTituloInstrucaoBoleto: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'nCdTituloInstrucaoBoleto'
      ReadOnly = True
    end
    object qryTempInstrucaoTitulocNmMensagem: TStringField
      DisplayLabel = 'Instru'#231#227'o'
      FieldName = 'cNmMensagem'
      Size = 100
    end
    object qryTempInstrucaoTitulocFlgMensagemPadrao: TIntegerField
      FieldName = 'cFlgMensagemPadrao'
    end
  end
  object dsTempInstrucaoTitulo: TDataSource
    DataSet = qryTempInstrucaoTitulo
    Left = 1192
    Top = 392
  end
  object qryTituloInstrucaoBoleto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM '
      'TituloInstrucaoBoleto')
    Left = 824
    Top = 64
    object qryTituloInstrucaoBoletonCdTituloInstrucaoBoleto: TIntegerField
      FieldName = 'nCdTituloInstrucaoBoleto'
    end
    object qryTituloInstrucaoBoletonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTituloInstrucaoBoletocMensagem: TStringField
      FieldName = 'cMensagem'
      Size = 100
    end
    object qryTituloInstrucaoBoletocFlgMensagemAgrupamento: TIntegerField
      FieldName = 'cFlgMensagemAgrupamento'
    end
  end
  object dsTituloInstrucaoBoleto: TDataSource
    DataSet = qryTituloInstrucaoBoleto
    Left = 856
    Top = 64
  end
  object qryJurosDiaAtraso: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEspTit'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nPercJuroDia'
      '      ,nPercMulta'
      '  FROM TabelaJuros'
      ' WHERE nCdEmpresa  =:nCdEmpresa'
      '   AND nCdEspTit   = :nCdEspTit'
      '   AND dDtValidade > getDate()')
    Left = 848
    Top = 160
    object qryJurosDiaAtrasonPercJuroDia: TBCDField
      FieldName = 'nPercJuroDia'
      Precision = 12
    end
    object qryJurosDiaAtrasonPercMulta: TBCDField
      FieldName = 'nPercMulta'
      DisplayFormat = '##,#00.00'
      EditFormat = '##,#00.00'
      Precision = 12
      Size = 2
    end
  end
  object qryInstrucaoContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryInstrucaoContaBancariaBeforePost
    BeforeDelete = qryInstrucaoContaBancariaBeforeDelete
    Parameters = <
      item
        Name = 'nCdContaBancaria'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @CONTABANCARIA INT'
      '        ,@Mensagem varchar(100)'
      ''
      'SET @ContaBancaria =  :nCdContaBancaria'
      ''
      
        'IF (OBJECT_ID('#39'tempdb..#Temp_TituloInstrucaoBoleto'#39') IS NOT NULL' +
        ')'
      'BEGIN'
      '    TRUNCATE TABLE #Temp_TituloInstrucaoBoleto'
      'END'
      'IF (OBJECT_ID('#39'tempdb..#Temp_TituloInstrucaoBoleto'#39') IS NULL)'
      'BEGIN'
      
        '   '#9'CREATE TABLE #Temp_TituloInstrucaoBoleto(nCdTituloInstrucaoB' +
        'oleto int PRIMARY KEY identity'
      
        '                                            ,cNmMensagem        ' +
        '      varchar(100)'
      
        '                                            ,cFlgMensagemPadrao ' +
        '      int default 0 not null )'
      'END'
      ''
      'SET @Mensagem = (SELECT cPrimeiraInstrucaoBoleto'
      '                   FROM ContaBancaria'
      '                  WHERE nCdContaBancaria = @ContaBancaria)'
      ''
      'IF (@Mensagem IS NOT NULL)'
      'BEGIN'
      
        '    INSERT INTO #Temp_TituloInstrucaoBoleto (cNmMensagem) VALUES' +
        ' (@MENSAGEM )'
      'END'
      ''
      'SET @Mensagem = (SELECT cSegundaInstrucaoBoleto'
      '                   FROM ContaBancaria'
      '                  WHERE nCdContaBancaria = @ContaBancaria)'
      'IF (@Mensagem IS NOT NULL)'
      'BEGIN'
      
        '   INSERT INTO #Temp_TituloInstrucaoBoleto (cNmMensagem) VALUES ' +
        '(@MENSAGEM )'
      'END'
      ''
      'SELECT *'
      '  FROM #Temp_TituloInstrucaoBoleto'
      'WHERE cNmMensagem IS NOT NULL'
      ''
      '')
    Left = 896
    Top = 96
    object qryInstrucaoContaBancarianCdTituloInstrucaoBoleto: TAutoIncField
      DisplayLabel = 'ID'
      FieldName = 'nCdTituloInstrucaoBoleto'
      ReadOnly = True
    end
    object qryInstrucaoContaBancariacNmMensagem: TStringField
      DisplayLabel = 'Instru'#231#227'o Banc'#225'ria'
      FieldName = 'cNmMensagem'
      Size = 100
    end
    object qryInstrucaoContaBancariacFlgMensagemPadrao: TIntegerField
      FieldName = 'cFlgMensagemPadrao'
    end
  end
  object dsInstrucaoContaBancaria: TDataSource
    DataSet = qryInstrucaoContaBancaria
    Left = 928
    Top = 96
  end
  object qryApagaInstrucaoTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 824
    Top = 96
  end
  object PopupMenu1: TPopupMenu
    Left = 737
    Top = 370
    object InstruesdeCobrancadoTtulo1: TMenuItem
      Caption = 'Instru'#231#245'es de Cobranca  do T'#237'tulo'
      OnClick = InstruesdeCobrancadoTtulo1Click
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 560
    Top = 392
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 904
    Top = 165
  end
  object qryDadosTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cInstrucaoBoleto1        '
      '      ,cInstrucaoBoleto2'
      '      ,iDiasProtesto'
      '      ,nPercDesctoBolAteVencto'
      '      ,iDiasCarencia'
      '      ,nPercMultaBoleto'
      '      ,nPercJurosMesBoleto'
      '      ,cFlgPermitirEmissaoBoleto'
      '      ,cFlgNaoEnviarProtesto'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 968
    Top = 181
    object qryDadosTerceirocInstrucaoBoleto1: TStringField
      FieldName = 'cInstrucaoBoleto1'
      Size = 100
    end
    object qryDadosTerceirocInstrucaoBoleto2: TStringField
      FieldName = 'cInstrucaoBoleto2'
      Size = 100
    end
    object qryDadosTerceiroiDiasProtesto: TIntegerField
      FieldName = 'iDiasProtesto'
    end
    object qryDadosTerceironPercDesctoBolAteVencto: TBCDField
      FieldName = 'nPercDesctoBolAteVencto'
      Precision = 12
      Size = 2
    end
    object qryDadosTerceiroiDiasCarencia: TIntegerField
      FieldName = 'iDiasCarencia'
    end
    object qryDadosTerceironPercMultaBoleto: TBCDField
      FieldName = 'nPercMultaBoleto'
      Precision = 12
      Size = 2
    end
    object qryDadosTerceironPercJurosMesBoleto: TBCDField
      FieldName = 'nPercJurosMesBoleto'
      Precision = 12
      Size = 2
    end
    object qryDadosTerceirocFlgPermitirEmissaoBoleto: TIntegerField
      FieldName = 'cFlgPermitirEmissaoBoleto'
    end
    object qryDadosTerceirocFlgNaoEnviarProtesto: TIntegerField
      FieldName = 'cFlgNaoEnviarProtesto'
    end
  end
end
