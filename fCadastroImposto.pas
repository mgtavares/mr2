unit fCadastroImposto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmCadastroImpostos = class(TfrmCadastro_Padrao)
    qryMasternCdTipoImposto: TIntegerField;
    qryMastercNmTipoImposto: TStringField;
    qryMasternCdCategFinanc: TIntegerField;
    qryMasternCdTipoServicoImposto: TIntegerField;
    qryMasternCdTerceiroCredor: TIntegerField;
    qryMastercFlgAbatimento: TIntegerField;
    qryMastercFlgAgrupaPagamento: TIntegerField;
    qryMastercFlgTipoRetencao: TIntegerField;
    qryMastercFlgCooperado: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryCategFinanc: TADOQuery;
    dsCategFinanc: TDataSource;
    qryCategFinancnCdCategFinanc: TIntegerField;
    qryCategFinanccNmCategFinanc: TStringField;
    qryCategFinancnCdGrupoCategFinanc: TIntegerField;
    qryCategFinanccFlgRecDes: TStringField;
    qryCategFinancnCdPlanoConta: TIntegerField;
    qryCategFinancnCdServidorOrigem: TIntegerField;
    qryCategFinancdDtReplicacao: TDateTimeField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceironCdStatus: TIntegerField;
    qryTerceironCdMoeda: TIntegerField;
    dsTerceiro: TDataSource;
    Label5: TLabel;
    DBEdit7: TDBEdit;
    Label6: TLabel;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    DBEdit10: TDBEdit;
    Label9: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit14: TDBEdit;
    Label14: TLabel;
    DBEdit16: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    DBRadioGroup3: TDBRadioGroup;
    DBRadioGroup4: TDBRadioGroup;
    Label7: TLabel;
    DBEdit9: TDBEdit;
    qryEspTitProv: TADOQuery;
    qryEspTitProvnCdEspTit: TIntegerField;
    qryEspTitProvnCdGrupoEspTit: TIntegerField;
    qryEspTitProvcNmEspTit: TStringField;
    qryEspTitProvcTipoMov: TStringField;
    qryEspTitProvcFlgPrev: TIntegerField;
    dsEspTitProv: TDataSource;
    DBEdit12: TDBEdit;
    qryMasternCdEspTitProv: TIntegerField;
    qryMasternCdEspTitPagto: TIntegerField;
    Label10: TLabel;
    DBEdit13: TDBEdit;
    DBEdit15: TDBEdit;
    qryEspTitPagto: TADOQuery;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    StringField3: TStringField;
    StringField4: TStringField;
    IntegerField6: TIntegerField;
    dsEspTitPagto: TDataSource;
    qryTipoServicoImposto: TADOQuery;
    dsTipoServicoImposto: TDataSource;
    qryTipoServicoImpostonCdTipoServicoImposto: TIntegerField;
    qryTipoServicoImpostocNmTipoServicoImposto: TStringField;
    DBEdit17: TDBEdit;
    qryMastercCdCodServMunicipal: TStringField;
    qryMastercFlgPessoaFisica: TIntegerField;
    qryMastercFlgProtegeBase: TIntegerField;
    qryMastercFlgTipoImposto: TIntegerField;
    DBRadioGroup5: TDBRadioGroup;
    DBRadioGroup6: TDBRadioGroup;
    DBRadioGroup7: TDBRadioGroup;
    qryTipoServicoImpostoiNroTipoServicoImposto: TIntegerField;
    DBEdit18: TDBEdit;
    qryMasternPercBaseCalc: TBCDField;
    qryMasternPercAliq: TBCDField;
    qryMasteriNumeroDias: TIntegerField;
    qryMasteriDiaVencto: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit13Exit(Sender: TObject);
    procedure DBEdit13KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadastroImpostos: TfrmCadastroImpostos;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmCadastroImpostos.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOIMPOSTO' ;
  nCdTabelaSistema  := 505 ;
  nCdConsultaPadrao := 1012 ;

end;

procedure TfrmCadastroImpostos.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            // nPK := frmLookup_Padrao.ExecutaConsulta(13);
            nPK := frmLookup_Padrao.ExecutaConsulta2(32,'cFlgRecDes = ' + Chr(39) + 'D' + Chr(39));

            If (nPK > 0) then
            begin
                qryMasternCdCategFinanc.Value := nPK ;
            end ;

        end ;

    end ;

    end;

end;

procedure TfrmCadastroImpostos.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroCredor.Value := nPK ;
            end ;

        end ;

    end ;

  end;

end;

procedure TfrmCadastroImpostos.DBEdit3Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCategFinanc,qryMasternCdCategFinanc.AsString);
end;

procedure TfrmCadastroImpostos.DBEdit5Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTerceiro,qryMasternCdTerceiroCredor.AsString);

end;

procedure TfrmCadastroImpostos.DBEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(10,'cTipoMov = ' + Chr(39) + 'P' + Chr(39) + 'AND cFlgPrev = 1' );

            If (nPK > 0) then
            begin
                qryMasternCdEspTitProv.Value := nPK ;
            end ;

        end ;

    end ;

  end;

end;

procedure TfrmCadastroImpostos.DBEdit7Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery( qryEspTitProv , qryMasternCdEspTitProv.asString );
end;

procedure TfrmCadastroImpostos.DBEdit13Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery( qryEspTitPagto , qryMasternCdEspTitPagto.asString );

end;

procedure TfrmCadastroImpostos.DBEdit13KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(10,'cTipoMov = ' + Chr(39) + 'P' + Chr(39) + 'AND cFlgPrev = 0' );

            If (nPK > 0) then
            begin
                qryMasternCdEspTitPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end;

end;

procedure TfrmCadastroImpostos.DBEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(1013);

            If (nPK > 0) then
            begin
                qryMasternCdTipoServicoImposto.Value := nPK ;
            end ;

        end ;

    end ;

  end;
end;

procedure TfrmCadastroImpostos.btIncluirClick(Sender: TObject);
begin
  inherited;
   DBEdit2.SetFocus;
   qryMastercFlgAbatimento.Value      := 0;
   qryMastercFlgAgrupaPagamento.Value := 0;
   qryMastercFlgTipoRetencao.Value    := 0;
   qryMastercFlgProtegeBase.Value     := 0;
   qryMastercFlgPessoaFisica.Value    := 0;
   qryMastercFlgCooperado.Value       := 0;

 //qryMastercFlgTipoImposto.Value     := 0;

end;

procedure TfrmCadastroImpostos.DBEdit8Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTipoServicoImposto,qryMasternCdTipoServicoImposto.AsString);
end;

procedure TfrmCadastroImpostos.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;

  PosicionaQuery(qryTipoServicoImposto,qryMasternCdTipoServicoImposto.AsString);
  PosicionaQuery(qryEspTitProv,qryMasternCdEspTitProv.AsString);
  PosicionaQuery(qryEspTitPagto,qryMasternCdEspTitPagto.AsString);
  PosicionaQuery(qryTerceiro,qryMasternCdTerceiroCredor.AsString);
  PosicionaQuery(qryCategFinanc,qryMasternCdCategFinanc.AsString);

end;

procedure TfrmCadastroImpostos.qryMasterBeforePost(DataSet: TDataSet);
begin

  if DBEdit2.Text = '' then
  begin
      MensagemAlerta('Informe a descri��o.') ;
      DBEdit2.SetFocus ;
      abort ;
  end;

  if (DBEdit3.Text = '') or (DBEdit4.Text = '') then
  begin
      MensagemAlerta('Informe a categoria financeira.') ;
      DBEdit3.SetFocus ;
      abort ;
  end;

  if (DBEdit5.Text = '') or (DBEdit6.Text = '') then
  begin
      MensagemAlerta('Informe o Terceiro Credor.') ;
      DBEdit5.SetFocus ;
      abort ;
  end;

  if (DBEdit7.Text = '') or (DBEdit12.Text = '') then
  begin
      MensagemAlerta('Informe a esp�cie de t�tulo para provis�o.') ;
      DBEdit7.SetFocus ;
      abort ;
  end;

  if (DBEdit13.Text = '') or (DBEdit15.Text = '') then
  begin
      MensagemAlerta('Informe a esp�cie de t�tulo para pagamento.') ;
      DBEdit13.SetFocus ;
      abort ;
  end;

  if (DBEdit8.Text = '') or (DBEdit17.Text = '' )then
  begin
      MensagemAlerta('Informe o tipo Servi�o Imposto.') ;
      DBEdit8.SetFocus ;
      abort ;
  end;

  if DBEdit10.Text = '' then
  begin
      MensagemAlerta('Informe o percentual da Base C�lculo.') ;
      DBEdit10.SetFocus ;
      abort ;
  end;

  if (qryMasternPercBaseCalc.Value <= 0) or (qryMasternPercBaseCalc.Value > 100) then
  begin
      MensagemAlerta('Percentual de base c�lculo informado menor ou igual a zero ou maior que 100.') ;
      DBEdit10.SetFocus ;
      abort ;
  end;

  if DBEdit11.Text = '' then
  begin
      MensagemAlerta('Informe o percentual da Al�quota.') ;
      DBEdit11.SetFocus ;
      abort ;
  end;

  if (qryMasternPercAliq.Value <= 0) or (qryMasternPercAliq.Value > 100) then
  begin
      MensagemAlerta('Percentual de al�quota informado menor ou igual a zero ou maior que 100.') ;
      DBEdit11.SetFocus ;
      abort ;
  end;

  if DBRadioGroup7.ItemIndex = -1 then
  begin
      MensagemAlerta('Informe o tipo de imposto.') ;
      DBEdit11.SetFocus ;
      abort ;
  end;

  inherited;

end;

procedure TfrmCadastroImpostos.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryCategFinanc.Close;
  qryEspTitProv.Close;
  qryEspTitPagto.Close;
  qryTerceiro.Close;
  qryTipoServicoImposto.Close;
end;

procedure TfrmCadastroImpostos.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryCategFinanc.Close;
  qryEspTitProv.Close;
  qryEspTitPagto.Close;
  qryTerceiro.Close;
  qryTipoServicoImposto.Close;

end;

procedure TfrmCadastroImpostos.btExcluirClick(Sender: TObject);
begin
  inherited;
  qryCategFinanc.Close;
  qryEspTitProv.Close;
  qryEspTitPagto.Close;
  qryTerceiro.Close;
  qryTipoServicoImposto.Close;

end;

procedure TfrmCadastroImpostos.btSalvarClick(Sender: TObject);
begin
  inherited;
  qryCategFinanc.Close;
  qryEspTitProv.Close;
  qryEspTitPagto.Close;
  qryTerceiro.Close;
  qryTipoServicoImposto.Close;

end;

initialization
    RegisterClass(TfrmCadastroImpostos);

end.
