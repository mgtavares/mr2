inherited frmMetaDescontoMensal: TfrmMetaDescontoMensal
  Left = 170
  Top = 112
  Width = 1055
  Height = 568
  Caption = 'Meta Desconto Mensal'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 113
    Width = 1039
    Height = 417
  end
  inherited ToolBar1: TToolBar
    Width = 1039
    ButtonWidth = 97
    TabOrder = 3
    inherited ToolButton1: TToolButton
      Caption = 'Exibir &Distrib.'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 97
    end
    inherited ToolButton2: TToolButton
      Left = 105
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 113
    Width = 1039
    Height = 417
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 413
    ClientRectLeft = 4
    ClientRectRight = 1035
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Meta de Desconto'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1031
        Height = 389
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsMaster
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh1DblClick
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Loja|C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmLoja'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Loja|Nome da Loja'
            Width = 394
          end
          item
            EditButtons = <>
            FieldName = 'nValMetaDesconto'
            Footers = <>
            Title.Caption = 'Meta de Desconto|Total Meta'
            Width = 110
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'nValDescontoDistr'
            Footers = <>
            Title.Caption = 'Meta de Desconto|Saldo Distribu'#237'do'
            Width = 110
          end
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'nValDescontoUtil'
            Footers = <>
            Title.Caption = 'Meta de Desconto|Saldo Utilizado'
            Width = 110
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1039
    Height = 84
    Align = alTop
    TabOrder = 0
    object Label3: TLabel
      Tag = 1
      Left = 13
      Top = 49
      Width = 79
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#234's/Ano Distrib.'
    end
    object Label1: TLabel
      Tag = 1
      Left = 51
      Top = 27
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
    end
    object edtEmpresa: TER2LookupMaskEdit
      Left = 98
      Top = 18
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      OnExit = edtEmpresaExit
      CodigoLookup = 8
      QueryLookup = qryEmpresa
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 165
      Top = 18
      Width = 404
      Height = 21
      DataField = 'cNmEmpresa'
      DataSource = dsEmpresa
      ReadOnly = True
      TabOrder = 1
    end
  end
  object edtMesAno: TMaskEdit [4]
    Left = 98
    Top = 71
    Width = 65
    Height = 21
    EditMask = '99/9999'
    MaxLength = 7
    TabOrder = 2
    Text = '  /    '
  end
  inherited ImageList1: TImageList
    Left = 600
    Top = 304
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003E39340039343000332F
      2B002C29250027242100201D1B00E7E7E700333130000B0A0900070706000404
      0300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000046413B00857A7000C3B8
      AE007C7268007F756B0036322D00F2F2F1004C4A470095897D00BAAEA2007C72
      68007F756B000101010000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF00000084000000000000000000000000004D47410083786F00CCC3
      BA00786F65007B71670034302D00FEFEFE002C2A270095897D00C2B8AD00786F
      65007C7268000605050000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000554E480083786F00CCC3
      BA007970660071685F00585550000000000049464500857A7000C2B8AD00786F
      65007B7167000D0C0B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000817B76009F928600CCC3
      BA00C0B4AA00A6988B00807D79000000000074726F0090847900C2B8AD00C0B4
      AA00A89B8E004947470000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000FCFCFC0060595200423D38005851
      4A003D383300332F2B0039373400D3D3D3005F5E5C001A181600252220001917
      15000F0E0D0012121200FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF00000084000000000000000000FDFDFD009D918500B1A396007F75
      6B007C726800776D64006C635B002E2A2600564F480080766C007C726800776D
      640070675E0001010100FAFAFA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000FEFDFD00B8ACA100BAAEA2008277
      6D0082776D00AA917B00BAA79400B8A69000B09781009F8D7D00836D5B007163
      570095897D0023232200FCFCFC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000FDFCFC00DDDAD7009B8E82009D91
      8500867B7100564F4800504A440080766C006E665D00826C5800A6917D009484
      7400564F48008B8A8A00FEFEFE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000746B6200A497
      8A0095897D009F9286003E393400000000004C4640007E746A00857A70003E39
      340085817E00F5F5F500FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      00009B918700C3B8AE00655D5500000000007C726800A89B8E00A69B90000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000000000000000
      0000A79C9100BCB0A4009D91850000000000AEA093009D9185007B756E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFFF0000
      FE00C00380030000000080018003000000008001800300000000800181030000
      0000800181030000000080010001000000008001000100000000800100010000
      000080010001000000008001C101000000018001F11F000000038001F11F0000
      0077C003FFFF0000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryMasterBeforePost
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'iMes'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'iAno'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Size = 1
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa         int'
      '       ,@nCdLoja            int'
      '       ,@nCdMetaDescontoMes int'
      '       ,@nCdUsuario         int'
      '       ,@nCdMetaSemana      int'
      '       ,@iMes               int'
      '       ,@iAno               int'
      '       ,@dDtInicial         datetime'
      '       ,@dDtFinal           datetime'
      ''
      'SET @nCdEmpresa = :nCdEmpresa'
      'SET @iMes       = :iMes'
      'SET @iAno       = :iAno'
      'SET @nCdUsuario = :nCdUsuario'
      ''
      'IF (@iMes < 12)'
      'BEGIN'
      
        '    SET @dDtInicial = (Convert(datetime,'#39'01/'#39' + dbo.fn_ZeroEsque' +
        'rda(@iMes,2) + '#39'/'#39' + (Convert(char(4),@iAno)),103))'
      
        '    SET @dDtFinal   = (Convert(datetime,'#39'01/'#39' + dbo.fn_ZeroEsque' +
        'rda(@iMes + 1,2) + '#39'/'#39' + (Convert(char(4),@iAno)),103) - 1)'
      'END'
      ''
      'IF (@iMes = 12)'
      'BEGIN'
      
        '    SET @dDtInicial = (Convert(datetime,'#39'01/'#39' + dbo.fn_ZeroEsque' +
        'rda(@iMes,2) + '#39'/'#39' + (Convert(char(4),@iAno)),103))'
      
        '    SET @dDtFinal   = (Convert(datetime,'#39'01/'#39' + dbo.fn_ZeroEsque' +
        'rda(1,2) + '#39'/'#39' + (Convert(char(4),@iAno + 1)),103) - 1)'
      'END'
      ''
      'DECLARE curAux CURSOR'
      '    FOR SELECT nCdLoja'
      '          FROM Loja'
      '         WHERE nCdEmpresa = @nCdEmpresa'
      ''
      'OPEN curAux'
      ''
      'FETCH NEXT'
      ' FROM curAux'
      ' INTO @nCdLoja'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      '    IF (NOT EXISTS(SELECT 1'
      '                     FROM MetaDescontoMes '
      '                    WHERE nCdLoja    = @nCdLoja'
      '                      AND nCdEmpresa = @nCdEmpresa'
      '                      AND iMes       = @iMes'
      '                      AND iAno       = @iAno))'
      '    BEGIN'
      ''
      '        Set @nCdMetaDescontoMes = NULL'
      ''
      #9'    EXEC usp_ProximoID '#39'METADESCONTOMES'#39
      #9'                      ,@nCdMetaDescontoMes OUTPUT'
      ''
      '        INSERT INTO MetaDescontoMes (nCdMetaDescontoMes'
      '                                    ,nCdLoja'
      '                                    ,nCdEmpresa'
      '                                    ,iMes'
      '                                    ,iAno'
      '                                    ,dDtInicial'
      '                                    ,dDtFinal'
      '                                    ,dDtCad'
      '                                    ,nCdUsuarioUltAlt'
      '                                    ,dDtUltAlt)'
      '                             VALUES (@nCdMetaDescontoMes'
      '                                    ,@nCdLoja'
      '                                    ,@nCdEmpresa'
      '                                    ,@iMes '
      '                                    ,@iAno'
      '                                    ,@dDtInicial'
      '                                    ,@dDtFinal'
      '                                    ,GetDate()'
      '                                    ,@nCdUsuario'
      '                                    ,GetDate())'
      ''
      '    END'
      ''
      '    FETCH NEXT'
      '     FROM curAux'
      '     INTO @nCdLoja'
      ''
      'END'
      ''
      'CLOSE curAux'
      'DEALLOCATE curAux'
      ''
      'SELECT nCdMetaDescontoMes'
      '      ,Loja.nCdLoja'
      '      ,Loja.cNmLoja'
      '      ,MetaDescontoMes.nValMetaDesconto'
      '      ,MetaDescontoMes.dDtInicial'
      '      ,MetaDescontoMes.dDtFinal'
      '      ,MetaDescontoMes.nCdUsuarioUltAlt'
      '      ,MetaDescontoMes.dDtUltAlt'
      '      ,ISNULL((SELECT SUM(MVD.nValCotaDesconto)'
      '                 FROM MetaVendedorDesconto MVD'
      
        '                WHERE MetaDescontoMes.nCdMetaDescontoMes = MVD.n' +
        'CdMetaDescontoMes),0) as nValDescontoDistr'
      '      ,ISNULL((SELECT SUM(MDU.nValDescontoUtil)'
      '                 FROM MetaVendedorDesconto MVD'
      
        '                      INNER JOIN MetaDescontoUtil MDU ON MDU.nCd' +
        'MetaVendedorDesconto = MVD.nCdMetaVendedorDesconto'
      
        '                WHERE MetaDescontoMes.nCdMetaDescontoMes = MVD.n' +
        'CdMetaDescontoMes),0) as nValDescontoUtil'
      '  FROM MetaDescontoMes'
      '       INNER JOIN Loja ON Loja.nCdLoja = MetaDescontoMes.nCdLoja'
      ' WHERE MetaDescontoMes.nCdEmpresa = @nCdEmpresa'
      '   AND MetaDescontoMes.iMes       = @iMes'
      '   AND MetaDescontoMes.iAno       = @iAno'
      '')
    Left = 640
    Top = 271
    object qryMasternCdMetaDescontoMes: TIntegerField
      FieldName = 'nCdMetaDescontoMes'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMastercNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryMasternValMetaDesconto: TBCDField
      FieldName = 'nValMetaDesconto'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtInicial: TDateTimeField
      FieldName = 'dDtInicial'
    end
    object qryMasterdDtFinal: TDateTimeField
      FieldName = 'dDtFinal'
    end
    object qryMasternCdUsuarioUltAlt: TIntegerField
      FieldName = 'nCdUsuarioUltAlt'
    end
    object qryMasterdDtUltAlt: TDateTimeField
      FieldName = 'dDtUltAlt'
    end
    object qryMasternValDescontoUtil: TBCDField
      FieldName = 'nValDescontoUtil'
      ReadOnly = True
      Precision = 38
      Size = 2
    end
    object qryMasternValDescontoDistr: TBCDField
      FieldName = 'nValDescontoDistr'
      ReadOnly = True
      Precision = 38
      Size = 2
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT nCdEmpresa, cNmEmpresa FROM Empresa WHERE nCdEmpresa = :n' +
        'PK')
    Left = 680
    Top = 271
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 680
    Top = 304
  end
  object dsMaster: TDataSource
    DataSet = qryMaster
    Left = 640
    Top = 303
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 720
    Top = 271
  end
  object qryInsereSemana: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'iSemana'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtInicial'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtFinal'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdMetaMes'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdMetaSemana int'
      '       ,@iSemana       int'
      '       ,@dDtInicial    varchar(10)'
      '       ,@dDtFinal      varchar(10)'
      '       ,@nCdMetaMes    int'
      ''
      'SET @iSemana    = :iSemana'
      'SET @dDtInicial = :dDtInicial'
      'SET @dDtFinal   = :dDtFinal'
      'SET @nCdMetaMes = :nCdMetaMes'
      ''
      'EXEC usp_ProximoID '#39'METASEMANA'#39
      '                  ,@nCdMetaSemana OUTPUT'
      ''
      'INSERT INTO MetaSemana(nCdMetaSemana'
      '                      ,nCdMetaMes'
      '                      ,iSemana'
      '                      ,dDtInicial'
      '                      ,dDtFinal)'
      '                VALUES(@nCdMetaSemana'
      '                      ,@nCdMetaMes'
      '                      ,@iSemana'
      '                      ,Convert(datetime,@dDtInicial,103)'
      '                      ,Convert(datetime,@dDtFinal,103))')
    Left = 596
    Top = 273
  end
end
