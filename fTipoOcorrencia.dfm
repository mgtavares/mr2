inherited frmTipoOcorrencia: TfrmTipoOcorrencia
  Left = 280
  Top = 103
  Caption = 'Tipo de Ocorrencia'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 45
    Top = 43
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 35
    Top = 67
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 51
    Top = 90
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
  end
  object Label4: TLabel [4]
    Left = 25
    Top = 114
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dias Atend.'
    FocusControl = DBEdit4
  end
  inherited ToolBar2: TToolBar
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object er2LkpStatus: TER2LookupDBEdit [6]
    Left = 88
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 2
    CodigoLookup = 0
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 88
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdTipoOcorrencia'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit2: TDBEdit [8]
    Left = 88
    Top = 64
    Width = 617
    Height = 19
    DataField = 'cNmTipoOcorrencia'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit4: TDBEdit [9]
    Left = 88
    Top = 112
    Width = 65
    Height = 19
    DataField = 'iDiasAtendimento'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBCheckBox1: TDBCheckBox [10]
    Left = 158
    Top = 114
    Width = 129
    Height = 17
    Caption = 'Contar por dias '#250'teis.'
    DataField = 'cFlgContaDiasUteis'
    DataSource = dsMaster
    TabOrder = 5
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object cxPageControl1: TcxPageControl [11]
    Left = 24
    Top = 144
    Width = 689
    Height = 257
    ActivePage = tabUsuarioRegistro
    LookAndFeel.NativeStyle = True
    TabOrder = 6
    ClientRectBottom = 253
    ClientRectLeft = 4
    ClientRectRight = 685
    ClientRectTop = 24
    object tabUsuarioRegistro: TcxTabSheet
      Caption = 'Usu'#225'rio x Registrar Ocorr'#234'ncia'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 681
        Height = 229
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuarioRegTipoOcorrencia
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdSetor'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Title.Caption = 'Usu'#225'rio Permitdo a Registrar Ocorr'#234'ncias|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Usu'#225'rio Permitdo a Registrar Ocorr'#234'ncias|Nome Usu'#225'rio'
            Width = 580
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object tabUsuarioAtendto: TcxTabSheet
      Caption = 'Usu'#225'rio x Atender Ocorr'#234'ncia'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 681
        Height = 229
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuarioAtendTipoOcorrencia
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnColEnter = DBGridEh2ColEnter
        OnEnter = DBGridEh2Enter
        OnKeyUp = DBGridEh2KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdSetor'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Title.Caption = 'Usu'#225'rio Permitido a Atender Ocorr'#234'ncias|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Usu'#225'rio Permitido a Atender Ocorr'#234'ncias|Nome Usu'#225'rio'
            Width = 580
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit3: TDBEdit [12]
    Tag = 1
    Left = 156
    Top = 88
    Width = 549
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsMaster
    TabOrder = 7
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTipoOcorrencia'
      '      ,cNmTipoOcorrencia'
      '      ,nCdStatus'
      '      ,iDiasAtendimento'
      '      ,cFlgContaDiasUteis'
      '  FROM TipoOcorrencia'
      ' WHERE nCdTipoOcorrencia = :nPK')
    object qryMasternCdTipoOcorrencia: TIntegerField
      FieldName = 'nCdTipoOcorrencia'
    end
    object qryMastercNmTipoOcorrencia: TStringField
      FieldName = 'cNmTipoOcorrencia'
      Size = 50
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMastercNmStatus: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmStatus'
      LookupDataSet = qryStat
      LookupKeyFields = 'nCdStatus'
      LookupResultField = 'cNmStatus'
      KeyFields = 'nCdStatus'
      Lookup = True
    end
    object qryMasteriDiasAtendimento: TIntegerField
      FieldName = 'iDiasAtendimento'
    end
    object qryMastercFlgContaDiasUteis: TIntegerField
      FieldName = 'cFlgContaDiasUteis'
    end
  end
  object qryUsuarioRegTipoOcorrencia: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioRegTipoOcorrenciaBeforePost
    OnCalcFields = qryUsuarioRegTipoOcorrenciaCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdUsuarioRegTipoOcorrencia'
      '      ,nCdTipoOcorrencia'
      '      ,nCdUsuario'
      '  FROM UsuarioRegTipoOcorrencia'
      ' WHERE nCdTipoOcorrencia = :nPK')
    Left = 712
    Top = 127
    object qryUsuarioRegTipoOcorrencianCdUsuarioRegTipoOcorrencia: TIntegerField
      FieldName = 'nCdUsuarioRegTipoOcorrencia'
    end
    object qryUsuarioRegTipoOcorrencianCdTipoOcorrencia: TIntegerField
      FieldName = 'nCdTipoOcorrencia'
    end
    object qryUsuarioRegTipoOcorrencianCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioRegTipoOcorrenciacNmUsuario: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Calculated = True
    end
  end
  object dsUsuarioRegTipoOcorrencia: TDataSource
    DataSet = qryUsuarioRegTipoOcorrencia
    Left = 712
    Top = 159
  end
  object dsUsuarioAtendTipoOcorrencia: TDataSource
    DataSet = qryUsuarioAtendTipoOcorrencia
    Left = 648
    Top = 159
  end
  object qryUsuarioAtendTipoOcorrencia: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioAtendTipoOcorrenciaBeforePost
    OnCalcFields = qryUsuarioAtendTipoOcorrenciaCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdUsuarioAtendTipoOcorrencia'
      '      ,nCdTipoOcorrencia'
      '      ,nCdUsuario'
      '  FROM UsuarioAtendTipoOcorrencia'
      ' WHERE nCdTipoOcorrencia = :nPK')
    Left = 680
    Top = 159
    object qryUsuarioAtendTipoOcorrencianCdUsuarioAtendTipoOcorrencia: TIntegerField
      FieldName = 'nCdUsuarioAtendTipoOcorrencia'
    end
    object qryUsuarioAtendTipoOcorrencianCdTipoOcorrencia: TIntegerField
      FieldName = 'nCdTipoOcorrencia'
    end
    object qryUsuarioAtendTipoOcorrencianCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioAtendTipoOcorrenciacNmUsuario: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Calculated = True
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario, cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 680
    Top = 128
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 680
    Top = 96
  end
end
