unit fTipoDiverg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, ImgList, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, cxPC, cxControls;

type
  TfrmTipoDiverg = class(TfrmCadastro_Padrao)
    qryMasternCdTabTipoDiverg: TIntegerField;
    qryMastercNmTabTipoDiverg: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryUsuarioTabTipoDiverg: TADOQuery;
    dsUsuarioTabTipoDiverg: TDataSource;
    qryUsuarioTabTipoDivergnCdTabTipoDiverg: TIntegerField;
    qryUsuarioTabTipoDivergnCdUsuario: TIntegerField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    qryUsuarioTabTipoDivergcNmUsuario: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioTabTipoDivergnCdUsuarioTabTipoDiverg: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryUsuarioTabTipoDivergBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoDiverg: TfrmTipoDiverg;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmTipoDiverg.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'APLICACAO' ;
  nCdTabelaSistema  := 1 ;
  nCdConsultaPadrao := 99 ;
  bLimpaAposSalvar  := false ;
end;

procedure TfrmTipoDiverg.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioTabTipoDiverg.Close ;
end;

procedure TfrmTipoDiverg.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryUsuarioTabTipoDiverg, qryMasternCdTabTipoDiverg.AsString) ;
end;

procedure TfrmTipoDiverg.qryUsuarioTabTipoDivergBeforePost(
  DataSet: TDataSet);
begin

  if (qryUsuarioTabTipoDivergnCdUsuario.Value = 0) or (qryUsuarioTabTipoDivergcNmUsuario.Value = '') then
  begin
      MensagemAlerta('Selecione um usu�rio.') ;
      abort ;
  end ;

  qryUsuarioTabTipoDivergnCdTabTipoDiverg.Value := qryMasternCdTabTipoDiverg.Value ;

  if (qryUsuarioTabTipoDiverg.State = dsInsert) then
      qryUsuarioTabTipoDivergnCdUsuarioTabTipoDiverg.Value := frmMenu.fnProximoCodigo('USUARIOTABTIPODIVERG') ;

  inherited;
end;

procedure TfrmTipoDiverg.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioTabTipoDiverg.State = dsBrowse) then
             qryUsuarioTabTipoDiverg.Edit ;
        

        if (qryUsuarioTabTipoDiverg.State = dsInsert) or (qryUsuarioTabTipoDiverg.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioTabTipoDivergnCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoDiverg.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  if (bInclusao) then
    PosicionaQuery(qryUsuarioTabTipoDiverg, qryMasternCdTabTipoDiverg.AsString) ;

end;

initialization
    RegisterClass(TfrmTipoDiverg) ;
    
end.
