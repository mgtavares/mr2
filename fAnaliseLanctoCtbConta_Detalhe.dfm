inherited frmAnaliseLanctoCtbConta_Detalhe: TfrmAnaliseLanctoCtbConta_Detalhe
  Left = -8
  Top = -8
  Width = 1456
  Height = 886
  BorderIcons = []
  Caption = 'Consulta Anal'#237'tica'
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1440
    Height = 821
  end
  inherited ToolBar1: TToolBar
    Width = 1440
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid2: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1440
    Height = 821
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGridDBTableView1: TcxGridDBTableView
      DataController.DataSource = dsConsultaAnalitica
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'nValTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'nSaldoTit'
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nSaldoTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValLancto
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLineColor = clSilver
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      OptionsView.GroupFooters = gfAlwaysVisible
      object cxGridDBTableView1nCdLanctoPlanoConta: TcxGridDBColumn
        Caption = 'ID'
        DataBinding.FieldName = 'nCdLanctoPlanoConta'
      end
      object cxGridDBTableView1dDtCompetencia: TcxGridDBColumn
        Caption = 'Data Compet'#234'ncia'
        DataBinding.FieldName = 'dDtCompetencia'
        Width = 125
      end
      object cxGridDBTableView1cOrigem: TcxGridDBColumn
        Caption = 'Origem'
        DataBinding.FieldName = 'cOrigem'
        Width = 106
      end
      object cxGridDBTableView1cNrNF: TcxGridDBColumn
        Caption = 'Nr. NF'
        DataBinding.FieldName = 'cNrNF'
        Width = 75
      end
      object cxGridDBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro'
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 205
      end
      object cxGridDBTableView1nValLancto: TcxGridDBColumn
        Caption = 'Valor Lan'#231'amento'
        DataBinding.FieldName = 'nValLancto'
        HeaderAlignmentHorz = taRightJustify
        Width = 130
      end
      object cxGridDBTableView1nCdSP: TcxGridDBColumn
        Caption = 'N'#250'm. SP'
        DataBinding.FieldName = 'nCdSP'
        Width = 68
      end
      object cxGridDBTableView1nCdPedido: TcxGridDBColumn
        Caption = 'N'#250'm. Pedido'
        DataBinding.FieldName = 'nCdPedido'
        Width = 91
      end
      object cxGridDBTableView1nCdRecebimento: TcxGridDBColumn
        Caption = 'N'#250'm. Recebimento'
        DataBinding.FieldName = 'nCdRecebimento'
        Width = 126
      end
      object cxGridDBTableView1nCdLanctoFin: TcxGridDBColumn
        Caption = 'C'#243'd. Lancto. Fin.'
        DataBinding.FieldName = 'nCdLanctoFin'
        Width = 101
      end
      object cxGridDBTableView1nCdTitulo: TcxGridDBColumn
        Caption = 'C'#243'd. T'#237'tulo'
        DataBinding.FieldName = 'nCdTitulo'
        Width = 100
      end
      object cxGridDBTableView1cManual: TcxGridDBColumn
        Caption = 'Manual'
        DataBinding.FieldName = 'cManual'
        Width = 60
      end
      object cxGridDBTableView1cNmUsuario: TcxGridDBColumn
        Caption = 'Usu'#225'rio Lan'#231'amento Manual'
        DataBinding.FieldName = 'cNmUsuario'
      end
      object cxGridDBTableView1dDtProcesso: TcxGridDBColumn
        Caption = 'Data Processo'
        DataBinding.FieldName = 'dDtProcesso'
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 368
    Top = 152
  end
  object qryConsultaAnalitica: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUnidadeNegocio'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdPlanoConta'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa         int'
      '       ,@nCdUnidadeNegocio  int'
      '       ,@nCdGrupoPlanoConta int'
      '       ,@nCdPlanoConta      int'
      '       ,@dDtInicial         varchar(10)'
      '       ,@dDtFinal           varchar(10)'
      ''
      'Set @nCdEmpresa         = :nCdEmpresa'
      'Set @nCdUnidadeNegocio  = :nCdUnidadeNegocio'
      'Set @nCdPlanoConta      = :nCdPlanoConta'
      'Set @dDtInicial         = :dDtInicial'
      'Set @dDtFinal           = :dDtFinal'
      ''
      'SELECT LanctoPlanoConta.nCdLanctoPlanoConta'
      '      ,LanctoPlanoConta.dDtCompetencia'
      '      ,LanctoPlanoConta.dDtProcesso'
      '      ,LanctoPlanoConta.nCdSP'
      '      ,LanctoPlanoConta.nValLancto'
      '      ,LanctoPlanoConta.nCdPedido'
      '      ,LanctoPlanoConta.nCdRecebimento'
      '      ,LanctoPlanoConta.nCdLanctoFin'
      '      ,LanctoPlanoConta.nCdTitulo'
      '      ,CASE WHEN cFlgManual = 1 THEN '#39'Sim'#39
      '            ELSE NULL'
      '       END cManual'
      '      ,Usuario.cNmUsuario'
      
        '      ,CASE WHEN LanctoPlanoConta.nCdSP IS NOT NULL THEN SP.cNrT' +
        'it'
      
        '            WHEN LanctoPlanoConta.nCdRecebimento IS NOT NULL THE' +
        'N Recebimento.cNrDocto'
      
        '            WHEN LanctoPlanoConta.nCdTitulo IS NOT NULL THEN Tit' +
        'ulo.cNrNF'
      '            ELSE NULL'
      '       END cNrNF'
      
        '      ,CASE WHEN LanctoPlanoConta.nCdSP IS NOT NULL THEN Terceir' +
        'oSP.cNmTerceiro'
      
        '            WHEN LanctoPlanoConta.nCdRecebimento IS NOT NULL THE' +
        'N TerceiroReceb.cNmTerceiro'
      
        '            WHEN LanctoPlanoConta.nCdPedido IS NOT NULL THEN Ter' +
        'ceiroPed.cNmTerceiro'
      '            ELSE NULL'
      '       END cNmTerceiro'
      
        '      ,CASE WHEN LanctoPlanoConta.nCdSP          IS NOT NULL THE' +
        'N '#39'SP'#39
      
        '            WHEN LanctoPlanoConta.nCdRecebimento IS NOT NULL THE' +
        'N '#39'COMPRAS'#39
      
        '            WHEN LanctoPlanoConta.nCdPedido      IS NOT NULL THE' +
        'N '#39'PEDIDO'#39
      
        '            WHEN LanctoPlanoconta.nCdLanctoFin   IS NOT NULL THE' +
        'N '#39'CONCILIACAO'#39
      
        '            WHEN LanctoPlanoConta.nCdTitulo      IS NOT NULL THE' +
        'N '#39'MOV. TITULO'#39
      '       END cOrigem'
      '  FROM LanctoPlanoConta'
      
        '       LEFT JOIN Usuario                ON Usuario.nCdUsuario   ' +
        '              = LanctoPlanoConta.nCdUsuarioCad'
      
        '       LEFT JOIN SP                     ON SP.nCdSP             ' +
        '              = LanctoPlanoConta.nCdSP'
      
        '       LEFT JOIN Recebimento            ON Recebimento.nCdRecebi' +
        'mento         = LanctoPlanoConta.nCdRecebimento'
      
        '       LEFT JOIN Pedido                 ON Pedido.nCdPedido     ' +
        '              = LanctoPlanoConta.nCdPedido'
      
        '       LEFT JOIN Titulo                 ON Titulo.nCdTitulo     ' +
        '              = LanctoPlanoConta.nCdTitulo'
      
        '       LEFT JOIN Terceiro TerceiroSP    ON TerceiroSP.nCdTerceir' +
        'o             = SP.nCdTerceiro'
      
        '       LEFT JOIN Terceiro TerceiroReceb ON TerceiroReceb.nCdTerc' +
        'eiro          = Recebimento.nCdTerceiro'
      
        '       LEFT JOIN Terceiro TerceiroPed   ON TerceiroPed.nCdTercei' +
        'ro            = Pedido.nCdTerceiro'
      
        '       LEFT JOIN Terceiro TerceiroTit   ON TerceiroTit.nCdTercei' +
        'ro            = Titulo.nCdTerceiro'
      ' WHERE LanctoPlanoConta.nCdEmpresa = @nCdEmpresa'
      
        '   AND ((@nCdUnidadeNegocio = 0)   OR (LanctoPlanoConta.nCdUnida' +
        'deNegocio  = @nCdUnidadeNegocio))'
      
        '   AND ((@nCdPlanoConta = 0)       OR (LanctoPlanoConta.nCdPlano' +
        'Conta      = @nCdPlanoConta))'
      
        '   AND LanctoPlanoConta.dDtCompetencia >= Convert(DATETIME,@dDtI' +
        'nicial,103)'
      
        '   AND LanctoPlanoConta.dDtCompetencia  < Convert(DATETIME,@dDtF' +
        'inal,103) + 1'
      ' ORDER BY dDtCompetencia')
    Left = 256
    Top = 144
    object qryConsultaAnaliticanCdLanctoPlanoConta: TAutoIncField
      FieldName = 'nCdLanctoPlanoConta'
      ReadOnly = True
    end
    object qryConsultaAnaliticadDtCompetencia: TDateTimeField
      FieldName = 'dDtCompetencia'
    end
    object qryConsultaAnaliticadDtProcesso: TDateTimeField
      FieldName = 'dDtProcesso'
    end
    object qryConsultaAnaliticanCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryConsultaAnaliticanValLancto: TBCDField
      FieldName = 'nValLancto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryConsultaAnaliticanCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryConsultaAnaliticanCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryConsultaAnaliticanCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryConsultaAnaliticanCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryConsultaAnaliticacManual: TStringField
      FieldName = 'cManual'
      ReadOnly = True
      Size = 3
    end
    object qryConsultaAnaliticacNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryConsultaAnaliticacNrNF: TStringField
      FieldName = 'cNrNF'
      ReadOnly = True
      FixedChar = True
      Size = 17
    end
    object qryConsultaAnaliticacNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      ReadOnly = True
      Size = 50
    end
    object qryConsultaAnaliticacOrigem: TStringField
      FieldName = 'cOrigem'
      ReadOnly = True
      Size = 11
    end
  end
  object dsConsultaAnalitica: TDataSource
    DataSet = qryConsultaAnalitica
    Left = 296
    Top = 144
  end
end
