inherited rptVendaEstruturaMarcaLoja: TrptVendaEstruturaMarcaLoja
  Left = 32
  Top = 88
  Width = 1365
  Height = 711
  Caption = 'Relat'#243'rio de Vendas por Estrutura x Marca'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1349
    Height = 646
  end
  object Label1: TLabel [1]
    Left = 81
    Top = 48
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label2: TLabel [2]
    Left = 32
    Top = 72
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
  end
  object Label3: TLabel [3]
    Left = 54
    Top = 96
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria'
  end
  object Label4: TLabel [4]
    Left = 33
    Top = 120
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sub Categoria'
  end
  object Label5: TLabel [5]
    Left = 53
    Top = 144
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Segmento'
  end
  object Label6: TLabel [6]
    Left = 72
    Top = 168
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  object Label7: TLabel [7]
    Left = 31
    Top = 264
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Produto'
  end
  object Label11: TLabel [8]
    Left = 76
    Top = 192
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Linha'
  end
  object Label12: TLabel [9]
    Left = 63
    Top = 216
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cole'#231#227'o'
  end
  object Label13: TLabel [10]
    Left = 70
    Top = 240
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Classe'
  end
  object Label14: TLabel [11]
    Left = 8
    Top = 288
    Width = 93
    Height = 13
    Alignment = taRightJustify
    Caption = 'Campanha Promoc.'
  end
  object Label8: TLabel [12]
    Left = 188
    Top = 312
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label9: TLabel [13]
    Left = 32
    Top = 312
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Venda'
  end
  inherited ToolBar1: TToolBar
    Width = 1349
    TabOrder = 22
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtLoja: TMaskEdit [15]
    Left = 104
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
  end
  object edtDepartamento: TMaskEdit [16]
    Left = 104
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = edtDepartamentoExit
    OnKeyDown = edtDepartamentoKeyDown
  end
  object edtCategoria: TMaskEdit [17]
    Left = 104
    Top = 88
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = edtCategoriaExit
    OnKeyDown = edtCategoriaKeyDown
  end
  object edtSubCategoria: TMaskEdit [18]
    Left = 104
    Top = 112
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = edtSubCategoriaExit
    OnKeyDown = edtSubCategoriaKeyDown
  end
  object edtSegmento: TMaskEdit [19]
    Left = 104
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = edtSegmentoExit
    OnKeyDown = edtSegmentoKeyDown
  end
  object edtMarca: TMaskEdit [20]
    Left = 104
    Top = 160
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = edtMarcaExit
    OnKeyDown = edtMarcaKeyDown
  end
  object DBEdit1: TDBEdit [21]
    Tag = 1
    Left = 176
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmLoja'
    DataSource = DataSource1
    TabOrder = 11
  end
  object DBEdit2: TDBEdit [22]
    Tag = 1
    Left = 176
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = DataSource2
    TabOrder = 12
  end
  object DBEdit3: TDBEdit [23]
    Tag = 1
    Left = 176
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmCategoria'
    DataSource = DataSource3
    TabOrder = 13
  end
  object DBEdit4: TDBEdit [24]
    Tag = 1
    Left = 176
    Top = 112
    Width = 654
    Height = 21
    DataField = 'cNmSubCategoria'
    DataSource = DataSource4
    TabOrder = 14
  end
  object DBEdit5: TDBEdit [25]
    Tag = 1
    Left = 176
    Top = 136
    Width = 654
    Height = 21
    DataField = 'cNmSegmento'
    DataSource = DataSource5
    TabOrder = 15
  end
  object DBEdit6: TDBEdit [26]
    Tag = 1
    Left = 176
    Top = 160
    Width = 654
    Height = 21
    DataField = 'cNmMarca'
    DataSource = DataSource6
    TabOrder = 16
  end
  object edtGrupoProduto: TMaskEdit [27]
    Left = 104
    Top = 256
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 9
    Text = '      '
    OnExit = edtGrupoProdutoExit
    OnKeyDown = edtGrupoProdutoKeyDown
  end
  object DBEdit7: TDBEdit [28]
    Tag = 1
    Left = 176
    Top = 256
    Width = 654
    Height = 21
    DataField = 'cNmGrupoProduto'
    DataSource = DataSource7
    TabOrder = 17
  end
  object edtLinha: TMaskEdit [29]
    Left = 104
    Top = 184
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = edtLinhaExit
    OnKeyDown = edtLinhaKeyDown
  end
  object edtColecao: TMaskEdit [30]
    Left = 104
    Top = 208
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 7
    Text = '      '
    OnExit = edtColecaoExit
    OnKeyDown = edtColecaoKeyDown
  end
  object edtClasseProduto: TMaskEdit [31]
    Left = 104
    Top = 232
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 8
    Text = '      '
    OnExit = edtClasseProdutoExit
    OnKeyDown = edtClasseProdutoKeyDown
  end
  object DBEdit8: TDBEdit [32]
    Tag = 1
    Left = 176
    Top = 184
    Width = 654
    Height = 21
    DataField = 'cNmLinha'
    DataSource = DataSource8
    TabOrder = 18
  end
  object DBEdit9: TDBEdit [33]
    Tag = 1
    Left = 176
    Top = 208
    Width = 654
    Height = 21
    DataField = 'cNmColecao'
    DataSource = DataSource9
    TabOrder = 19
  end
  object DBEdit10: TDBEdit [34]
    Tag = 1
    Left = 176
    Top = 232
    Width = 654
    Height = 21
    DataField = 'cNmClasseProduto'
    DataSource = DataSource10
    TabOrder = 20
  end
  object edtCdCampanhaPromoc: TMaskEdit [35]
    Left = 104
    Top = 280
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 10
    Text = '      '
    OnExit = edtCdCampanhaPromocExit
    OnKeyDown = edtCdCampanhaPromocKeyDown
  end
  object DBEdit11: TDBEdit [36]
    Tag = 1
    Left = 176
    Top = 280
    Width = 654
    Height = 21
    DataField = 'cNmCampanhaPromoc'
    DataSource = DataSource11
    TabOrder = 21
  end
  object edtDtFinal: TMaskEdit [37]
    Left = 216
    Top = 304
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 24
    Text = '  /  /    '
  end
  object edtDtInicial: TMaskEdit [38]
    Left = 104
    Top = 304
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 23
    Text = '  /  /    '
  end
  object RadioGroup1: TRadioGroup [39]
    Left = 104
    Top = 344
    Width = 201
    Height = 41
    Caption = 'Modo Exibi'#231#227'o'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Relat'#243'rio'
      'Planilha')
    TabOrder = 25
  end
  inherited ImageList1: TImageList
    Left = 872
    Top = 144
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009E9E
      9E00656464006564640065646400656464006564640065646400656464006564
      6400898A8900B9BABA0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D5D5D5005157
      570031333200484E4E00313332003D4242003D4242003D4242003D424200191D
      230051575700C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000008C908F00191D
      230031333200313332003133320031333200313332003133320031333200191D
      230004050600898A8900000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000065646400898A
      8900DADDD700B9BABA00BEBEBE00BEBEBE00BEBEBE00BEBEBE00BEBABC00CCCC
      CC005157570065646400000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000717272003D42
      4200898A8900777B7B007B8585007B8585007B8585007B8585007B8585009392
      92003D424200777B7B00000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B6B6B6003133
      32007B858500AEAEAE00A4A8A800A4A8A800A4A8A800A4A8A800A4A8A800484E
      4E0031333200B9BABA00000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D5D5D500484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00898A
      8900484E4E00C6C6C600000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE007B85
      85003D42420000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00D5D5D5007172
      72003D42420000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE007B858500191D2300191D
      230051575700C6C6C600000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE0071727200484E4E003D42
      420093929200C6C6C600000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CCCCCC00484E
      4E0031333200575D5E005157570051575700575D5E00191D2300191D23009392
      9200CCCCCC00BEBEBE00000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B6B6
      B6009E9E9E009E9E9E009E9E9E009E9E9E009E9E9E00A4A8A800AEAEAE00C6C6
      C600BEBEBE00BEBEBE00000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FFFFFE00FFFF0000
      E003FE00C0030000C003000080010000C003000080010000C003000080010000
      C003000080010000C003000080010000C003000080010000C007000080010000
      C007000080010000C003000080010000C003000180010000C003000380010000
      E0030077C0030000FFFF007FFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK'
      
        '      AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja =' +
        ' Loja.nCdLoja AND UL.nCdUsuario = :nCdUsuario)')
    Left = 664
    Top = 168
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLoja
    Left = 632
    Top = 272
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmDepartamento, nCdDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 712
    Top = 200
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
  end
  object DataSource2: TDataSource
    DataSet = qryDepartamento
    Left = 640
    Top = 280
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdDepartamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmCategoria, nCdCategoria'
      'FROM Categoria'
      'WHERE nCdCategoria = :nPK'
      'AND nCdDepartamento = :nCdDepartamento')
    Left = 824
    Top = 200
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
    object qryCategorianCdCategoria: TAutoIncField
      FieldName = 'nCdCategoria'
      ReadOnly = True
    end
  end
  object DataSource3: TDataSource
    DataSet = qryCategoria
    Left = 648
    Top = 288
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmSubCategoria, nCdSubCategoria'
      'FROM SubCategoria'
      'WHERE nCdSubCategoria = :nPK'
      'AND nCdCategoria = :nCdCategoria')
    Left = 792
    Top = 200
    object qrySubCategoriacNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object qrySubCategorianCdSubCategoria: TAutoIncField
      FieldName = 'nCdSubCategoria'
      ReadOnly = True
    end
  end
  object DataSource4: TDataSource
    DataSet = qrySubCategoria
    Left = 656
    Top = 296
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdSubCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmSegmento, nCdSegmento'
      'FROM Segmento'
      'WHERE nCdSegmento = :nPK'
      'AND nCdSubCategoria = :nCdSubCategoria')
    Left = 712
    Top = 240
    object qrySegmentocNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
    object qrySegmentonCdSegmento: TAutoIncField
      FieldName = 'nCdSegmento'
      ReadOnly = True
    end
  end
  object DataSource5: TDataSource
    DataSet = qrySegmento
    Left = 664
    Top = 304
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmMarca, nCdMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 752
    Top = 200
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
  end
  object DataSource6: TDataSource
    DataSet = qryMarca
    Left = 672
    Top = 312
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto, cNmGrupoProduto'
      'FROM GrupoProduto'
      'WHERE nCdGrupoProduto = :nPK')
    Left = 816
    Top = 264
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
  object DataSource7: TDataSource
    DataSet = qryGrupoProduto
    Left = 680
    Top = 320
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha '
      'WHERE nCdLinha = :nPK')
    Left = 816
    Top = 304
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object DataSource8: TDataSource
    DataSet = qryLinha
    Left = 624
    Top = 272
  end
  object qryColecao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdColecao, cNmColecao'
      'FROM Colecao'
      'WHERE nCdColecao = :nPK')
    Left = 824
    Top = 352
    object qryColecaonCdColecao: TIntegerField
      FieldName = 'nCdColecao'
    end
    object qryColecaocNmColecao: TStringField
      FieldName = 'cNmColecao'
      Size = 50
    end
  end
  object DataSource9: TDataSource
    DataSet = qryColecao
    Left = 632
    Top = 280
  end
  object qryClasseProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdClasseProduto, cNmClasseProduto'
      'FROM ClasseProduto'
      'WHERE nCdClasseProduto = :nPK')
    Left = 800
    Top = 408
    object qryClasseProdutonCdClasseProduto: TIntegerField
      FieldName = 'nCdClasseProduto'
    end
    object qryClasseProdutocNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
  end
  object DataSource10: TDataSource
    DataSet = qryClasseProduto
    Left = 640
    Top = 288
  end
  object qryCampanhaPromoc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdCampanhaPromoc'
      '      ,cNmCampanhaPromoc '
      '      ,cFlgAtivada'
      '      ,cFlgSuspensa'
      '  FROM CampanhaPromoc'
      ' WHERE nCdEmpresa        = :nCdEmpresa'
      '   AND nCdCampanhaPromoc = :nPK')
    Left = 904
    Top = 264
    object qryCampanhaPromocnCdCampanhaPromoc: TIntegerField
      FieldName = 'nCdCampanhaPromoc'
    end
    object qryCampanhaPromoccNmCampanhaPromoc: TStringField
      FieldName = 'cNmCampanhaPromoc'
      Size = 50
    end
    object qryCampanhaPromoccFlgAtivada: TIntegerField
      FieldName = 'cFlgAtivada'
    end
    object qryCampanhaPromoccFlgSuspensa: TIntegerField
      FieldName = 'cFlgSuspensa'
    end
  end
  object DataSource11: TDataSource
    DataSet = qryCampanhaPromoc
    Left = 648
    Top = 296
  end
end
