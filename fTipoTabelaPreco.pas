unit fTipoTabelaPreco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmTipoTabelaPreco = class(TfrmCadastro_Padrao)
    qryMasternCdTipoTabPreco: TIntegerField;
    qryMastercNmTipoTabPreco: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoTabelaPreco: TfrmTipoTabelaPreco;

implementation

{$R *.dfm}

procedure TfrmTipoTabelaPreco.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOTABPRECO' ;
  nCdTabelaSistema  := 84 ;
  nCdConsultaPadrao := 192 ;

end;

procedure TfrmTipoTabelaPreco.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
  
end;

procedure TfrmTipoTabelaPreco.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBedit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o do tipo de tabela.') ;
      DBedit2.SetFocus;
      abort ;
  end ;

  inherited;

end;

initialization
    RegisterClass(TfrmTipoTabelaPreco) ;
    
end.
