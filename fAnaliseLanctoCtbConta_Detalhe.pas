unit fAnaliseLanctoCtbConta_Detalhe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, ADODB;

type
  TfrmAnaliseLanctoCtbConta_Detalhe = class(TfrmProcesso_Padrao)
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryConsultaAnalitica: TADOQuery;
    dsConsultaAnalitica: TDataSource;
    qryConsultaAnaliticanCdLanctoPlanoConta: TAutoIncField;
    qryConsultaAnaliticadDtCompetencia: TDateTimeField;
    qryConsultaAnaliticadDtProcesso: TDateTimeField;
    qryConsultaAnaliticanCdSP: TIntegerField;
    qryConsultaAnaliticanValLancto: TBCDField;
    qryConsultaAnaliticanCdPedido: TIntegerField;
    qryConsultaAnaliticanCdRecebimento: TIntegerField;
    qryConsultaAnaliticanCdLanctoFin: TIntegerField;
    qryConsultaAnaliticanCdTitulo: TIntegerField;
    qryConsultaAnaliticacManual: TStringField;
    qryConsultaAnaliticacNmUsuario: TStringField;
    qryConsultaAnaliticacNrNF: TStringField;
    qryConsultaAnaliticacNmTerceiro: TStringField;
    qryConsultaAnaliticacOrigem: TStringField;
    cxGridDBTableView1nCdLanctoPlanoConta: TcxGridDBColumn;
    cxGridDBTableView1dDtCompetencia: TcxGridDBColumn;
    cxGridDBTableView1dDtProcesso: TcxGridDBColumn;
    cxGridDBTableView1nCdSP: TcxGridDBColumn;
    cxGridDBTableView1nValLancto: TcxGridDBColumn;
    cxGridDBTableView1nCdPedido: TcxGridDBColumn;
    cxGridDBTableView1nCdRecebimento: TcxGridDBColumn;
    cxGridDBTableView1nCdLanctoFin: TcxGridDBColumn;
    cxGridDBTableView1nCdTitulo: TcxGridDBColumn;
    cxGridDBTableView1cManual: TcxGridDBColumn;
    cxGridDBTableView1cNmUsuario: TcxGridDBColumn;
    cxGridDBTableView1cNrNF: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView1cOrigem: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAnaliseLanctoCtbConta_Detalhe: TfrmAnaliseLanctoCtbConta_Detalhe;

implementation

{$R *.dfm}

end.
