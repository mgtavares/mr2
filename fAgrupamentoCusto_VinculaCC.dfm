inherited frmAgrupamentoCusto_VinculaCC: TfrmAgrupamentoCusto_VinculaCC
  Left = 234
  Top = 289
  Width = 659
  Height = 184
  Caption = 'Vincular Centro de Custo'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 148
    Width = 643
    Height = 0
  end
  inherited ToolBar1: TToolBar
    Width = 643
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 643
    Height = 119
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 40
      Top = 24
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = 'SubConta'
    end
    object Label2: TLabel
      Tag = 1
      Left = 8
      Top = 48
      Width = 79
      Height = 13
      Alignment = taRightJustify
      Caption = 'Centro de Custo'
    end
    object Label3: TLabel
      Tag = 1
      Left = 38
      Top = 72
      Width = 49
      Height = 13
      Alignment = taRightJustify
      Caption = 'Sequ'#234'ncia'
    end
    object Label4: TLabel
      Tag = 1
      Left = 47
      Top = 96
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#225'scara'
    end
    object edtMascaraSuperior: TEdit
      Tag = 1
      Left = 91
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object edtCC: TER2LookupMaskEdit
      Left = 91
      Top = 40
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 1
      Text = '         '
      CodigoLookup = 223
      QueryLookup = qryCC
    end
    object edtCodigo: TMaskEdit
      Left = 91
      Top = 64
      Width = 65
      Height = 21
      TabOrder = 2
      OnChange = edtCodigoChange
    end
    object edtMascara: TEdit
      Tag = 1
      Left = 91
      Top = 88
      Width = 121
      Height = 21
      TabOrder = 3
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 216
      Top = 16
      Width = 417
      Height = 21
      DataField = 'cNmContaAgrupamentoCusto'
      DataSource = DataSource1
      TabOrder = 4
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 160
      Top = 40
      Width = 473
      Height = 21
      DataField = 'cNmCC'
      DataSource = DataSource2
      TabOrder = 5
    end
  end
  inherited ImageList1: TImageList
    Left = 264
    Top = 104
  end
  object qrySubConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ContaAgrupamentoCusto'
      'WHERE nCdContaAgrupamentoCusto = :nPK')
    Left = 392
    Top = 61
    object qrySubContanCdContaAgrupamentoCusto: TIntegerField
      FieldName = 'nCdContaAgrupamentoCusto'
    end
    object qrySubContanCdContaAgrupamentoCustoPai: TIntegerField
      FieldName = 'nCdContaAgrupamentoCustoPai'
    end
    object qrySubContanCdAgrupamentoCusto: TIntegerField
      FieldName = 'nCdAgrupamentoCusto'
    end
    object qrySubContacMascara: TStringField
      FieldName = 'cMascara'
      Size = 30
    end
    object qrySubContacNmContaAgrupamentoCusto: TStringField
      FieldName = 'cNmContaAgrupamentoCusto'
      Size = 100
    end
    object qrySubContaiNivel: TIntegerField
      FieldName = 'iNivel'
    end
    object qrySubContaiSequencia: TIntegerField
      FieldName = 'iSequencia'
    end
    object qrySubContanCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qrySubContacFlgAnalSintetica: TStringField
      FieldName = 'cFlgAnalSintetica'
      FixedChar = True
      Size = 1
    end
  end
  object DataSource1: TDataSource
    DataSet = qrySubConta
    Left = 312
    Top = 80
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CentroCusto'
      'WHERE nCdCC = :nPK')
    Left = 480
    Top = 93
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
    object qryCCnCdCC1: TIntegerField
      FieldName = 'nCdCC1'
    end
    object qryCCnCdCC2: TIntegerField
      FieldName = 'nCdCC2'
    end
    object qryCCcCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object qryCCcCdHie: TStringField
      FieldName = 'cCdHie'
      FixedChar = True
      Size = 4
    end
    object qryCCiNivel: TSmallintField
      FieldName = 'iNivel'
    end
    object qryCCcFlgLanc: TIntegerField
      FieldName = 'cFlgLanc'
    end
    object qryCCnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryCCnCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryCCdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
  end
  object DataSource2: TDataSource
    DataSet = qryCC
    Left = 320
    Top = 88
  end
  object qryProximaSequencia: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT IsNull(MAX(iSequencia),0) + 1 as iProximaSequencia'
      '  FROM ContaAgrupamentoCusto'
      ' WHERE nCdContaAgrupamentoCustoPai = :nPK')
    Left = 384
    Top = 109
    object qryProximaSequenciaiProximaSequencia: TIntegerField
      FieldName = 'iProximaSequencia'
      ReadOnly = True
    end
  end
  object qryAgrupamentoCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM AgrupamentoCusto'
      'WHERE nCdAgrupamentoCusto = :nPK')
    Left = 528
    Top = 61
    object qryAgrupamentoCustonCdAgrupamentoCusto: TIntegerField
      FieldName = 'nCdAgrupamentoCusto'
    end
    object qryAgrupamentoCustonCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryAgrupamentoCustocNmAgrupamentoCusto: TStringField
      FieldName = 'cNmAgrupamentoCusto'
      Size = 50
    end
    object qryAgrupamentoCustocMascara: TStringField
      FieldName = 'cMascara'
      Size = 30
    end
    object qryAgrupamentoCustonCdTabTipoAgrupamentoContabil: TIntegerField
      FieldName = 'nCdTabTipoAgrupamentoContabil'
    end
    object qryAgrupamentoCustonCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryAgrupamentoCustoiMaxNiveis: TIntegerField
      FieldName = 'iMaxNiveis'
    end
  end
  object qryContaAgrupamentoCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ContaAgrupamentoCusto'
      'WHERE nCdContaAgrupamentoCusto = :nPK')
    Left = 536
    Top = 101
    object qryContaAgrupamentoCustonCdContaAgrupamentoCusto: TIntegerField
      FieldName = 'nCdContaAgrupamentoCusto'
    end
    object qryContaAgrupamentoCustonCdContaAgrupamentoCustoPai: TIntegerField
      FieldName = 'nCdContaAgrupamentoCustoPai'
    end
    object qryContaAgrupamentoCustonCdAgrupamentoCusto: TIntegerField
      FieldName = 'nCdAgrupamentoCusto'
    end
    object qryContaAgrupamentoCustocMascara: TStringField
      FieldName = 'cMascara'
      Size = 30
    end
    object qryContaAgrupamentoCustocNmContaAgrupamentoCusto: TStringField
      FieldName = 'cNmContaAgrupamentoCusto'
      Size = 100
    end
    object qryContaAgrupamentoCustoiNivel: TIntegerField
      FieldName = 'iNivel'
    end
    object qryContaAgrupamentoCustoiSequencia: TIntegerField
      FieldName = 'iSequencia'
    end
    object qryContaAgrupamentoCustonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryContaAgrupamentoCustocFlgAnalSintetica: TStringField
      FieldName = 'cFlgAnalSintetica'
      FixedChar = True
      Size = 1
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 584
    Top = 85
  end
end
