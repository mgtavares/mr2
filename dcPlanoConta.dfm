inherited dcmPlanoConta: TdcmPlanoConta
  Left = 0
  Top = 0
  Width = 1280
  Height = 770
  Caption = 'Data Analysis - Demonstrativo de Resultados'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1264
    Height = 705
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 40
    Width = 88
    Height = 13
    Caption = 'Per'#237'odo de An'#225'lise'
  end
  object Label2: TLabel [2]
    Left = 200
    Top = 40
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    Width = 1264
    TabOrder = 2
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DecisionGrid1: TDecisionGrid [4]
    Left = 0
    Top = 88
    Width = 1233
    Height = 329
    DefaultColWidth = 200
    DefaultRowHeight = 20
    CaptionColor = clAqua
    CaptionFont.Charset = DEFAULT_CHARSET
    CaptionFont.Color = clCaptionText
    CaptionFont.Height = -11
    CaptionFont.Name = 'Tahoma'
    CaptionFont.Style = []
    DataColor = clInfoBk
    DataSumColor = clInactiveCaption
    DataFont.Charset = DEFAULT_CHARSET
    DataFont.Color = clWindowText
    DataFont.Height = -11
    DataFont.Name = 'Tahoma'
    DataFont.Style = []
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clBlue
    LabelFont.Height = -11
    LabelFont.Name = 'Tahoma'
    LabelFont.Style = []
    LabelColor = clBtnFace
    LabelSumColor = clInactiveCaption
    DecisionSource = DecisionSource1
    Dimensions = <>
    Totals = False
    ShowCubeEditor = True
    BorderStyle = bsNone
    Color = clWhite
    Ctl3D = False
    DefaultDrawing = False
    GridLineWidth = 1
    GridLineColor = clAqua
    ParentCtl3D = False
    TabOrder = 3
  end
  object ER2Soft: TDecisionPivot [5]
    Left = 0
    Top = 61
    Width = 1232
    Height = 25
    ButtonAutoSize = True
    DecisionSource = DecisionSource1
    GroupLayout = xtHorizontal
    Groups = [xtRows, xtColumns, xtSummaries]
    ButtonSpacing = 0
    ButtonWidth = 64
    ButtonHeight = 24
    GroupSpacing = 10
    BorderWidth = 0
    BorderStyle = bsNone
    Color = clAqua
    Ctl3D = False
    ParentCtl3D = False
    TabOrder = 4
  end
  object MaskEdit1: TMaskEdit [6]
    Left = 112
    Top = 32
    Width = 81
    Height = 21
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 0
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [7]
    Left = 224
    Top = 32
    Width = 81
    Height = 21
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
  end
  object DecisionGraph1: TDecisionGraph [8]
    Left = 0
    Top = 424
    Width = 1233
    Height = 250
    DecisionSource = DecisionSource1
    Gradient.Direction = gdFromTopLeft
    Gradient.Visible = True
    LeftWall.Brush.Color = clWhite
    LeftWall.Brush.Style = bsClear
    Title.Font.Charset = ANSI_CHARSET
    Title.Font.Color = clBlue
    Title.Font.Height = -11
    Title.Font.Name = 'Tahoma'
    Title.Font.Style = []
    Title.Text.Strings = (
      'ER2Soft - Gr'#225'fico')
    TabOrder = 5
  end
  inherited ImageList1: TImageList
    Left = 520
    Top = 104
  end
  object qryCube: TADOQuery
    AutoCalcFields = False
    Connection = frmMenu.Connection
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'dDtInicial'
        DataType = ftString
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      
        'SELECT (Convert(Varchar, DatePart(year,dDtCompetencia)) + '#39'/'#39' + ' +
        'Convert(Varchar,DatePart(month,dDtCompetencia))) as Mes'
      '      ,(CASE WHEN PlanoConta.cFlgRecDes = '#39'D'#39' THEN '#39'Despesa'#39
      '             ELSE '#39'Receita'#39
      '        END) Tipo'
      '      ,cNmGrupoPlanoConta'
      '      ,cNmPlanoConta'
      '      ,cSigla'
      '      ,cNmUnidadeNegocio'
      
        '      ,Sum(CASE WHEN PlanoConta.cFlgRecDes = '#39'D'#39' THEN nValLancto' +
        '*-1'
      '                ELSE nValLancto'
      '           END) nValLancto'
      '  FROM LanctoPlanoConta LPC'
      '       INNER JOIN Empresa ON Empresa.nCdEmpresa = LPC.nCdEmpresa'
      
        '       INNER JOIN UnidadeNegocio ON UnidadeNegocio.nCdUnidadeNeg' +
        'ocio = LPC.nCdUnidadeNegocio'
      
        '       INNER JOIN PlanoConta ON PlanoConta.nCdPlanoConta = LPC.n' +
        'CdPlanoConta'
      
        '       INNER JOIN GrupoPlanoConta ON GrupoPlanoConta.nCdGrupoPla' +
        'noConta = PlanoConta.nCdGrupoPlanoConta'
      ' WHERE dDtCompetencia >= Convert(DATETIME,:dDtInicial,103)'
      '   AND dDtCompetencia < Convert(DATETIME,:dDtFinal,103) + 1'
      '   AND cFlgExibeDRE = 1'
      '   AND LPC.nCdEmpresa = :nCdEmpresa'
      
        ' GROUP BY (Convert(Varchar, DatePart(year,dDtCompetencia)) + '#39'/'#39 +
        ' + Convert(Varchar,DatePart(month,dDtCompetencia)))'
      '         ,(CASE WHEN PlanoConta.cFlgRecDes = '#39'D'#39' THEN '#39'Despesa'#39
      '                ELSE '#39'Receita'#39
      '           END)'
      '         ,cNmGrupoPlanoConta'
      '         ,cNmPlanoConta'
      '         ,cSigla'
      '         ,cNmUnidadeNegocio')
    Left = 400
    Top = 136
    object qryCubeMes: TStringField
      FieldName = 'Mes'
      ReadOnly = True
      Size = 61
    end
    object qryCubeTipo: TStringField
      FieldName = 'Tipo'
      ReadOnly = True
      Size = 7
    end
    object qryCubecNmGrupoPlanoConta: TStringField
      FieldName = 'cNmGrupoPlanoConta'
      Size = 50
    end
    object qryCubecNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
    object qryCubecSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryCubecNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
    object qryCubenValLancto: TBCDField
      DisplayLabel = 'Valor Acumulado'
      FieldName = 'nValLancto'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM EMPRESA')
    Left = 477
    Top = 145
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresanCdTerceiroEmp: TIntegerField
      FieldName = 'nCdTerceiroEmp'
    end
    object qryEmpresanCdTerceiroMatriz: TIntegerField
      FieldName = 'nCdTerceiroMatriz'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresanCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM UNIDADENEGOCIO')
    Left = 525
    Top = 257
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object qryPlanoConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM PLANOCONTA')
    Left = 613
    Top = 257
    object qryPlanoContanCdPlanoConta: TAutoIncField
      FieldName = 'nCdPlanoConta'
      ReadOnly = True
    end
    object qryPlanoContacNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
    object qryPlanoContanCdGrupoPlanoConta: TIntegerField
      FieldName = 'nCdGrupoPlanoConta'
    end
    object qryPlanoContacFlgRecDes: TStringField
      FieldName = 'cFlgRecDes'
      FixedChar = True
      Size = 1
    end
  end
  object DecisionSource1: TDecisionSource
    DecisionCube = DecisionCube1
    ControlType = xtCheck
    SparseRows = False
    SparseCols = False
    Left = 368
    Top = 184
    DimensionCount = 0
    SummaryCount = 0
    CurrentSummary = 0
    SparseRows = False
    SparseCols = False
    DimensionInfo = ()
  end
  object DecisionCube1: TDecisionCube
    DataSet = qryCube
    DimensionMap = <
      item
        ActiveFlag = diActive
        FieldType = ftString
        Fieldname = 'Mes'
        Name = 'Mes'
        DerivedFrom = -1
        DimensionType = dimDimension
        BinType = binNone
        ValueCount = 0
        Active = True
      end
      item
        ActiveFlag = diActive
        FieldType = ftString
        Fieldname = 'Tipo'
        Name = 'Tipo'
        DerivedFrom = -1
        DimensionType = dimDimension
        BinType = binNone
        ValueCount = 0
        Active = True
      end
      item
        ActiveFlag = diActive
        FieldType = ftString
        Fieldname = 'cNmGrupoPlanoConta'
        Name = 'Grupo Contas'
        DerivedFrom = -1
        DimensionType = dimDimension
        BinType = binNone
        ValueCount = 0
        Active = True
      end
      item
        ActiveFlag = diActive
        FieldType = ftString
        Fieldname = 'cNmPlanoConta'
        Name = 'Conta'
        DerivedFrom = -1
        DimensionType = dimDimension
        BinType = binNone
        ValueCount = 0
        Active = True
      end
      item
        ActiveFlag = diActive
        FieldType = ftString
        Fieldname = 'cSigla'
        Name = 'Empresa'
        DerivedFrom = -1
        DimensionType = dimDimension
        BinType = binNone
        ValueCount = 0
        Active = True
      end
      item
        ActiveFlag = diActive
        FieldType = ftString
        Fieldname = 'cNmUnidadeNegocio'
        Name = 'Unidade Neg'#243'cio'
        DerivedFrom = -1
        DimensionType = dimDimension
        BinType = binNone
        ValueCount = 0
        Active = True
      end
      item
        ActiveFlag = diAsNeeded
        Format = '#,##0.00'
        FieldType = ftBCD
        Fieldname = 'nValLancto'
        Name = 'Valor Acumulado'
        DerivedFrom = -1
        DimensionType = dimSum
        BinType = binNone
        ValueCount = -1
        Active = True
      end>
    ShowProgressDialog = True
    MaxDimensions = 10
    MaxSummaries = 10
    MaxCells = 0
    Left = 392
    Top = 256
  end
  object qryParametro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cParametro'
        DataType = ftString
        Precision = 15
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cValor'
      'FROM Parametro'
      'WHERE cParametro = :cParametro')
    Left = 536
    Top = 344
    object qryParametrocValor: TStringField
      FieldName = 'cValor'
      Size = 15
    end
  end
end
