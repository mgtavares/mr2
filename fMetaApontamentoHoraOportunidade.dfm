inherited frmMetaApontamentoHoraOportunidade: TfrmMetaApontamentoHoraOportunidade
  Left = 181
  Top = 159
  Width = 958
  Height = 552
  Caption = 'Apontamento Hora / Oportunidade'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 118
    Width = 942
    Height = 398
  end
  inherited ToolBar1: TToolBar
    Width = 942
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 942
    Height = 89
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 50
      Top = 20
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
    end
    object Label3: TLabel
      Tag = 1
      Left = 22
      Top = 68
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#234's/Ano Meta'
    end
    object Label2: TLabel
      Tag = 1
      Left = 70
      Top = 44
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
    end
    object edtMesAno: TMaskEdit
      Left = 96
      Top = 60
      Width = 65
      Height = 21
      EditMask = '99/9999'
      MaxLength = 7
      TabOrder = 2
      Text = '  /    '
      OnChange = edtMesAnoChange
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 168
      Top = 36
      Width = 385
      Height = 21
      DataField = 'cNmLoja'
      DataSource = dsLoja
      TabOrder = 5
    end
    object cxButton1: TcxButton
      Left = 168
      Top = 59
      Width = 105
      Height = 25
      Caption = 'Exibir Meta'
      TabOrder = 3
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000C6D6DEC6948C
        C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
        8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
        D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
        00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
        FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
        948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
        FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
        C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
        FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
        948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
        8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
        D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
      LookAndFeel.NativeStyle = True
    end
    object cxButton2: TcxButton
      Left = 280
      Top = 59
      Width = 105
      Height = 25
      Caption = 'Nova Meta'
      TabOrder = 4
      OnClick = cxButton2Click
      Glyph.Data = {
        2E020000424D2E0200000000000036000000280000000C0000000E0000000100
        180000000000F801000000000000000000000000000000000000EEFEFEEEFEFE
        EEFEFEEEFEFE9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F0000
        000000000000000000000000000000000000000000000000000000000000009C
        8C6F000000FFFFFFC0928FC0928FC0928FC0928FC0928FC0928FC0928FC0928F
        0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9F9EED9F9EED9F9EE
        D9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9F9EED9F9
        EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9
        F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EE
        D9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9
        EED9F9EED9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFF
        F9EED9F9EED9F9EED9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F0000
        00FFFFFFF9EED9F9EED9F9EED9F9EED90000000000000000000000000000009C
        8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED90000009C8C6F9C8C6F9C8C6F
        000000EEFEFE000000FFFFFFF9EED9F9EED9F9EED9F9EED90000009C8C6F9C8C
        6F000000EEFEFEEEFEFE000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000009C
        8C6F000000EEFEFEEEFEFEEEFEFE000000000000000000000000000000000000
        000000000000EEFEFEEEFEFEEEFEFEEEFEFE}
      LookAndFeel.NativeStyle = True
    end
    object edtEmpresa: TMaskEdit
      Left = 96
      Top = 12
      Width = 65
      Height = 21
      EditMask = '########;1; '
      MaxLength = 8
      TabOrder = 0
      Text = '        '
      OnChange = edtEmpresaChange
      OnExit = edtEmpresaExit
      OnKeyDown = edtEmpresaKeyDown
    end
    object edtLoja: TMaskEdit
      Left = 96
      Top = 36
      Width = 65
      Height = 21
      EditMask = '########;1; '
      MaxLength = 8
      TabOrder = 1
      Text = '        '
      OnChange = edtLojaChange
      OnExit = edtLojaExit
      OnKeyDown = edtLojaKeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 168
      Top = 12
      Width = 385
      Height = 21
      DataField = 'cNmEmpresa'
      DataSource = dsEmpresa
      TabOrder = 6
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 118
    Width = 942
    Height = 398
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 394
    ClientRectLeft = 4
    ClientRectRight = 938
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Apontamentos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 934
        Height = 370
        Align = alClient
        AllowedOperations = []
        DataGrouping.GroupLevels = <>
        DataSource = dsApontamentoHora
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh1DblClick
        OnDrawColumnCell = DBGridEh1DrawColumnCell
        Columns = <
          item
            EditButtons = <>
            FieldName = 'iSemana'
            Footers = <>
            ReadOnly = True
            Width = 59
          end
          item
            EditButtons = <>
            FieldName = 'dDtSemana'
            Footers = <>
            ReadOnly = True
            Width = 96
          end
          item
            EditButtons = <>
            FieldName = 'cNmSemana'
            Footers = <>
            ReadOnly = True
            Width = 105
          end
          item
            EditButtons = <>
            FieldName = 'iTotalHorasVendaReal'
            Footers = <>
            ReadOnly = True
            Width = 122
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeOportunidades'
            Footers = <>
            ReadOnly = True
            Width = 121
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 7
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa  '
      '  FROM Empresa'
      ' WHERE nCdEmpresa =:nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE '
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa '
      '                 AND UE.nCdUsuario = :nCdUsuario)')
    Left = 360
    Top = 302
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '8'
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS (SELECT 1'
      '                 FROM UsuarioLoja UL'
      '                WHERE UL.nCdUsuario = :nCdUsuario'
      '                  AND UL.nCdLoja    = Loja.nCdLoja) ')
    Left = 404
    Top = 302
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 376
    Top = 336
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 420
    Top = 334
  end
  object qryApontamentoHora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'iMes'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'iAno'
        DataType = ftString
        Size = 4
        Value = '2010'
      end>
    SQL.Strings = (
      'DECLARE @dDtInicial  datetime'
      '       ,@dDtFinal    datetime'
      '       ,@dDtRef      datetime'
      '       ,@iMes        int'
      '       ,@iAno        int'
      '       ,@nCdLoja     int'
      '       ,@nCdEmpresa  int'
      '       ,@nCdMetaMes  int'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempApontamentoHoras'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempApontamentoHoras (iSemana              int'
      
        '                                       ,dDtSemana            dat' +
        'etime'
      
        '                                       ,cNmSemana            var' +
        'char(13)'
      
        '                                       ,iTotalHorasVendaReal dec' +
        'imal(12,2) default 0 not null'
      
        '                                       ,nQtdeOportunidades   int' +
        '           default 0 not null)'
      ''
      'END'
      ''
      'TRUNCATE TABLE #TempApontamentoHoras'
      ''
      ''
      'SET @nCdEmpresa = :nCdEmpresa'
      'SET @nCdLoja    = :nCdLoja'
      'SET @iMes       = :iMes'
      'SET @iAno       = :iAno'
      ''
      'SELECT @dDtInicial = dDtInicial'
      '      ,@dDtFinal   = dDtFinal '
      '      ,@nCdMetaMes = nCdMetaMes'
      '  FROM MetaMes '
      ' WHERE nCdEmpresa = @nCdEmpresa '
      '   AND nCdLoja    = @nCdLoja '
      '   AND iMes       = @iMes'
      '   AND iAno       = @iAno'
      ''
      ''
      'SET @dDtRef = @dDtInicial'
      ''
      'WHILE @dDtRef <= @dDtFinal'
      'BEGIN'
      '    INSERT INTO #TempApontamentoHoras (iSemana'
      '                                      ,dDtSemana'
      '                                      ,cNmSemana'
      '                                      ,iTotalHorasVendaReal'
      '                                      ,nQtdeOportunidades)'
      ''
      '                               SELECT (SELECT MetaSemana.iSemana'
      '                                         FROM MetaSemana'
      
        '                                        WHERE MetaSemana.nCdMeta' +
        'Mes = @nCdMetaMes'
      
        '                                          AND MetaSemana.dDtInic' +
        'ial = @dDtRef)'
      '                                     ,@dDtRef'
      
        '                                     ,(SELECT CASE Datepart(DW,@' +
        'dDtRef) '
      
        '                                                   WHEN 1 THEN '#39 +
        'DOMINGO'#39
      
        '                                                   WHEN 2 THEN '#39 +
        'SEGUNDA-FEIRA'#39
      
        '                                                   WHEN 3 THEN '#39 +
        'TER'#199'A-FEIRA'#39
      
        '                                                   WHEN 4 THEN '#39 +
        'QUARTA-FEIRA'#39
      
        '                                                   WHEN 5 THEN '#39 +
        'QUINTA-FEIRA'#39
      
        '                                                   WHEN 6 THEN '#39 +
        'SEXTA-FEIRA'#39
      
        '                                                   WHEN 7 THEN '#39 +
        'S'#193'BADO'#39
      '                                              END)'
      
        '                                     ,isNull(SUM(MetaVendedorDia' +
        '.iTotalHorasReal),0)'
      
        '                                     ,isNull(SUM(MetaVendedorDia' +
        '.iOportunidades),0)'
      '                                FROM MetaVendedorDia'
      
        '                                     INNER JOIN MetaVendedor ON ' +
        'MetaVendedor.nCdMetaVendedor = MetaVendedorDia.nCdMetaVendedor'
      
        '                                     INNER JOIN MetaSemana   ON ' +
        'MetaSemana.nCdMetaSemana     = MetaVendedor.nCdMetaSemana'
      
        '                               WHERE MetaSemana.nCdMetaMes = @nC' +
        'dMetaMes'
      
        '                                 AND dDtRef                = @dD' +
        'tRef'
      ''
      '    SET @dDtRef = @dDtRef + 1'
      'END'
      ''
      'SELECT iSemana'
      '      ,dDtSemana'
      '      ,cNmSemana'
      '      ,iTotalHorasVendaReal'
      '      ,nQtdeOportunidades'
      '  FROM #TempApontamentoHoras'
      '')
    Left = 452
    Top = 302
    object qryApontamentoHoraiSemana: TIntegerField
      DisplayLabel = 'Semana'
      FieldName = 'iSemana'
    end
    object qryApontamentoHoradDtSemana: TDateTimeField
      DisplayLabel = 'Data'
      FieldName = 'dDtSemana'
    end
    object qryApontamentoHoracNmSemana: TStringField
      DisplayLabel = 'Dia da Semana'
      FieldName = 'cNmSemana'
      Size = 13
    end
    object qryApontamentoHoraiTotalHorasVendaReal: TBCDField
      DisplayLabel = 'Apontamentos|Horas de Venda'
      FieldName = 'iTotalHorasVendaReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryApontamentoHoranQtdeOportunidades: TIntegerField
      DisplayLabel = 'Apontamentos|Qtde. Oportunidades'
      FieldName = 'nQtdeOportunidades'
    end
  end
  object dsApontamentoHora: TDataSource
    DataSet = qryApontamentoHora
    Left = 460
    Top = 334
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempMetaVendedorDia'#39') IS NULL)'#13#10'BEGIN'#13#10' ' +
      #13#10'    CREATE TABLE #TempMetaVendedorDia (nCdTempMetaVendedorDia ' +
      'int PRIMARY KEY IDENTITY(1,1)'#13#10'                                 ' +
      '     ,nCdMetaVendedor        int           default 0 not null'#13#10' ' +
      '                                     ,dDtRef                 dat' +
      'etime                not null'#13#10'                                 ' +
      '     ,nCdUsuario             int           default 0 not null'#13#10' ' +
      '                                     ,iTotalHorasReal        dec' +
      'imal(12,2) default 0 not null'#13#10'                                 ' +
      '     ,iOportunidades         int           default 0 not null)'#13#10 +
      #13#10'END'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 508
    Top = 302
  end
end
