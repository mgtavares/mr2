inherited frmCorPredom: TfrmCorPredom
  Width = 1168
  Caption = 'Cor Predominante'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1152
  end
  object Label1: TLabel [1]
    Left = 63
    Top = 46
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 31
    Top = 70
    Width = 70
    Height = 13
    Caption = 'Descri'#231#227'o Cor'
    FocusControl = DBEdit2
  end
  inherited ToolBar2: TToolBar
    Width = 1152
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 104
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdCor'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 104
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmCor'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM COR'
      'WHERE nCdCor = :nPK'
      'AND iNivel = 1')
    object qryMasternCdCor: TAutoIncField
      FieldName = 'nCdCor'
    end
    object qryMastercNmCor: TStringField
      FieldName = 'cNmCor'
      Size = 50
    end
    object qryMastercNmCorPredom: TStringField
      FieldName = 'cNmCorPredom'
      Size = 50
    end
    object qryMasteriNivel: TIntegerField
      FieldName = 'iNivel'
    end
    object qryMasternCdCorPredom: TIntegerField
      FieldName = 'nCdCorPredom'
    end
  end
  object DataSource1: TDataSource
    Left = 624
    Top = 368
  end
  object qryVerificaDup: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCor'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cNmCor'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 100
        Size = 100
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      'FROM Cor'
      'WHERE nCdCor <> :nCdCor'
      'AND cNmCor = :cNmCor'
      'AND nCdCorPredom IS NULL')
    Left = 400
    Top = 176
  end
end
