unit fInventario_Monitor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ComCtrls, Mask, ER2Lookup, StdCtrls, DBCtrls,
  ImgList, ToolWin, ExtCtrls, DB, ADODB, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Menus;

type
  TfrmInventario_Monitor = class(TfrmProcesso_Padrao)
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    qryLocalEstoque: TADOQuery;
    dsLoja: TDataSource;
    dsLocalEstoque: TDataSource;
    dsEmpresa: TDataSource;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    ER2LookupMaskEdit1: TER2LookupMaskEdit;
    ER2LookupMaskEdit2: TER2LookupMaskEdit;
    edtDtFinal: TMaskEdit;
    edtDtInicial: TMaskEdit;
    ER2LookupMaskEdit3: TER2LookupMaskEdit;
    edtDtFechFim: TMaskEdit;
    edtDtFechIni: TMaskEdit;
    edtDtEncConIni: TMaskEdit;
    edtDtEncConFim: TMaskEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    qryInventario: TADOQuery;
    dsInventario: TDataSource;
    qryInventariocNmInventario: TStringField;
    qryInventariocNmLocalEstoque: TStringField;
    qryInventariodDtAbertura: TDateTimeField;
    qryInventarionCdUsuarioAbertura: TIntegerField;
    qryInventariocNmUsuarioAbertaura: TStringField;
    qryInventariodDtFech: TDateTimeField;
    qryInventarionCdUsuarioFech: TIntegerField;
    qryInventariocNmUsuarioFech: TStringField;
    qryInventariodDtCancel: TDateTimeField;
    qryInventarionCdUsuarioCancel: TIntegerField;
    qryInventariocNmUsuarioCancel: TStringField;
    qryInventariodDtContagemEncerrada: TDateTimeField;
    qryInventariocOBS: TStringField;
    qryInventarionCdLoja: TIntegerField;
    cxGrid1DBTableView1cNmInventario: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLocalEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1dDtAbertura: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuarioAbertaura: TcxGridDBColumn;
    cxGrid1DBTableView1dDtFech: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuarioFech: TcxGridDBColumn;
    cxGrid1DBTableView1dDtCancel: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuarioCancel: TcxGridDBColumn;
    cxGrid1DBTableView1dDtContagemEncerrada: TcxGridDBColumn;
    cxGrid1DBTableView1cOBS: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLoja: TcxGridDBColumn;
    qryItemLoteContagem: TADOQuery;
    dsItemLoteContagem: TDataSource;
    cNrLote: TStringField;
    qryItemLoteContagemnCdProduto: TIntegerField;
    qryItemLoteContagemcNmProduto: TStringField;
    qryItemLoteContagemnQtdeContada: TBCDField;
    cxGrid2DBTableView1cNrLote: TcxGridDBColumn;
    cxGrid2DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid2DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid2DBTableView1nQtdeContada: TcxGridDBColumn;
    qryInventarionCdInventario: TIntegerField;
    RadioGroup1: TRadioGroup;
    PopupMenu1: TPopupMenu;
    ExportarInventario1: TMenuItem;
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure ExportarInventario1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInventario_Monitor: TfrmInventario_Monitor;

implementation

{$R *.dfm}
uses
  fMenu;
procedure TfrmInventario_Monitor.ToolButton1Click(Sender: TObject);
begin

  if Trim(DBEdit2.Text) = '' then
  begin
      MensagemAlerta('Empresa n�o informado.');
      ER2LookupMaskEdit1.Focused;
      Abort;
  end;

  if (edtDtInicial.Text <> '  /  /    ') then
  begin
      if (edtDtInicial.Text  > edtDtFinal.Text) then
      begin
          MensagemAlerta('Data de abertura inicial maior que data final.');
          edtDtInicial.Focused;
          abort;
      end;
  end;

  if (edtDtFechIni.Text <> '  /  /    ') then
  begin
      if (edtDtFechIni.Text  > edtDtFechFim.Text) then
      begin
          MensagemAlerta('Data de abertura inicial maior que data final.');
          ER2LookupMaskEdit2.Focused;
          abort;
      end;
  end;

  if (edtDtEncConIni.Text <> '  /  /    ') then
  begin
      if (edtDtEncConIni.Text  > edtDtEncConFim.Text) then
      begin
          MensagemAlerta('Data de abertura inicial maior que data final.');
          edtDtEncConIni.Focused;
          abort;
      end;
  end;

  try

    qryInventario.Close;
    qryInventario.Parameters.ParamByName('nCdEmpresa').Value              := frmMenu.ConvInteiro(ER2LookupMaskEdit1.Text);
    qryInventario.Parameters.ParamByName('nCdLoja').Value                 := frmMenu.ConvInteiro(ER2LookupMaskEdit2.Text);
    qryInventario.Parameters.ParamByName('nCdLocalEstoque').Value         := frmMenu.ConvInteiro(ER2LookupMaskEdit3.Text);
    qryInventario.Parameters.ParamByName('dDtAberturaIni').Value          := frmMenu.ConvData(edtDtInicial.Text);
    qryInventario.Parameters.ParamByName('dDtAberturaFim').Value          := frmMenu.ConvData(edtDtFinal.Text);
    qryInventario.Parameters.ParamByName('dDtFechIni').Value              := frmMenu.ConvData(edtDtFechIni.Text);
    qryInventario.Parameters.ParamByName('dDtFechFim').Value              := frmMenu.ConvData(edtDtFechFim.Text);
    qryInventario.Parameters.ParamByName('dDtContagemEncerradaIni').Value := frmMenu.ConvData(edtDtEncConIni.Text);
    qryInventario.Parameters.ParamByName('dDtContagemEncerradaFim').Value := frmMenu.ConvData(edtDtEncConFim.Text);
    qryInventario.Parameters.ParamByName('cFlgStatus').Value              := RadioGroup1.ItemIndex;
    qryInventario.Open;

  except
      MensagemErro('Erro no processamento.');
      raise;
      Abort;
  end;
end;

procedure TfrmInventario_Monitor.cxGrid1DBTableView1CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin

  if qryInventario.State = dsBrowse then
      PosicionaQuery(qryItemLoteContagem,qryInventarionCdInventario.AsString);

end;

procedure TfrmInventario_Monitor.ExportarInventario1Click(Sender: TObject);
var
  cFiltro : String;
  cFiltroAux : String;
begin
  try
      if qryInventario.State = dsBrowse then
          PosicionaQuery(qryItemLoteContagem,qryInventarionCdInventario.AsString);

      cFiltro  := 'Inventario: ' + qryInventariocNmInventario.AsString
                + ' Dt. Abertura: ' +  qryInventariodDtAbertura.AsString
                + ' Usuario Abertura: ' + qryInventariocNmUsuarioAbertaura.AsString
                + ' Dt. Enc. Lote: ' + qryInventariodDtContagemEncerrada.AsString  ;
                
      cFiltroAux := '  Dt. Fechamento: ' + qryInventariodDtFech.AsString
                + ' Usuario Fechamento: ' + qryInventariocNmUsuarioFech.AsString
                + ' Dt. Cancelado: ' + qryInventariodDtCancel.AsString
                + ' Usuario Cancelamento: ' + qryInventariocNmUsuarioCancel.AsString;

      frmMenu.ExportExcelDinamic('Rel. Inventario Loja',cFiltro,cFiltroAux,qryItemLoteContagem);
  except
      MensagemErro('Erro no processamento.');
      raise;
      Abort;
  end;

end;

initialization
    RegisterClass(TfrmInventario_Monitor) ;
end.
