unit fCadastroCentroProdutivo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, ER2Lookup, StdCtrls, Mask, DBCtrls, DBGridEhGrouping, GridsEh,
  DBGridEh, cxPC, cxControls;

type
  TfrmCadastroCentroProdutivo = class(TfrmCadastro_Padrao)
    qryMasternCdCentroProdutivo: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNmCentroProdutivo: TStringField;
    qryMasternCdCC: TIntegerField;
    qryMasternCdLocalEstoqueProcesso: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    edtEmpresa: TER2LookupDBEdit;
    Label2: TLabel;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit2: TDBEdit;
    dsEmpresa: TDataSource;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    qryCentroCusto: TADOQuery;
    qryLocalEstoque: TADOQuery;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocNmCC: TStringField;
    DBEdit6: TDBEdit;
    dsCentroCusto: TDataSource;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DBEdit7: TDBEdit;
    dsLocalEstoque: TDataSource;
    edtCC: TER2LookupDBEdit;
    edtLocalEstoque: TER2LookupDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryLinhaProducao: TADOQuery;
    qryLinhaProducaonCdLinhaProducao: TIntegerField;
    qryLinhaProducaonCdCentroProdutivo: TIntegerField;
    qryLinhaProducaocNmLinhaProducao: TStringField;
    dsLinhaProducao: TDataSource;
    edtSetorRequisicao: TER2LookupDBEdit;
    qrySetorRequisicao: TADOQuery;
    qrySetorRequisicaonCdSetor: TIntegerField;
    qrySetorRequisicaocNmSetor: TStringField;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    dsSetorRequisicao: TDataSource;
    qryMasternCdSetorRequisicao: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure qryLinhaProducaoBeforePost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforeClose(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCadastroCentroProdutivo: TfrmCadastroCentroProdutivo;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmCadastroCentroProdutivo.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CENTROPRODUTIVO' ;
  nCdTabelaSistema  := 312 ;
  nCdConsultaPadrao := 738 ;
  bCodigoAutomatico := False ;
end;

procedure TfrmCadastroCentroProdutivo.qryLinhaProducaoBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  qryLinhaProducaonCdLinhaProducao.Value   := frmMenu.fnProximoCodigo('LINHAPRODUCAO');
  qryLinhaProducaonCdCentroProdutivo.Value := qryMasternCdCentroProdutivo.Value;
end;

procedure TfrmCadastroCentroProdutivo.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if ((qryMaster.Active) and (qryLinhaProducao.State <> dsBrowse)) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmCadastroCentroProdutivo.qryMasterAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryLinhaProducao.Close;
  PosicionaQuery(qryLinhaProducao,qryMasternCdCentroProdutivo.AsString);

  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa,qryMasternCdEmpresa.AsString);

  qryLocalEstoque.Close;
  PosicionaQuery(qryLocalEstoque,qryMasternCdLocalEstoqueProcesso.AsString);

  qryCentroCusto.Close;
  PosicionaQuery(qryCentroCusto,qryMasternCdCC.AsString);

  qrySetorRequisicao.Close;
  PosicionaQuery(qrySetorRequisicao,qryMasternCdSetorRequisicao.AsString);

end;

procedure TfrmCadastroCentroProdutivo.qryMasterBeforeClose(
  DataSet: TDataSet);
begin
  inherited;
  qryLinhaProducao.Close;
  qryEmpresa.Close;
  qryLocalEstoque.Close;
  qryCentroCusto.Close;
  qrySetorRequisicao.Close;
end;

procedure TfrmCadastroCentroProdutivo.qryMasterBeforePost(
  DataSet: TDataSet);
begin

  if (qryMasternCdCentroProdutivo.Value = 0) then
  begin
      MensagemAlerta('Indique o C�digo do Centro produtivo antes de Salvar');
      DBEdit1.SetFocus;
      Abort;
  end;

  if (qryMasternCdEmpresa.Value = 0) then
  begin
      MensagemAlerta('Selecione uma empresa para o Centro Produtivo');
      edtEmpresa.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit3.Text) = '') then
  begin
      MensagemAlerta('Digite uma descri��o para o Centro Produtivo');
      DBEdit3.SetFocus;
      Abort;
  end;

  if (qryMasternCdCC.Value = 0) then
  begin
      MensagemAlerta('Informe o Centro de Custo');
      edtCC.SetFocus;
      Abort;
  end;

  if (qryMasternCdLocalEstoqueProcesso.Value = 0) then
  begin
      MensagemAlerta('Informe o Local de Estoque');
      edtLocalEstoque.SetFocus;
      Abort;
  end;

  if (qryMasternCdSetorRequisicao.Value = 0) then
  begin
      MensagemAlerta('Informe o Setor de Requisi��o');
      edtSetorRequisicao.SetFocus;
      Abort;
  end;

  inherited;

end;

procedure TfrmCadastroCentroProdutivo.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

initialization
    RegisterClass(TfrmCadastroCentroProdutivo);

end.
