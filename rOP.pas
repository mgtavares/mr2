unit rOP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, QRCtrls, QuickRpt, ExtCtrls, SvQrBarcode;

type
  TrptOP = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel7: TQRLabel;
    QRLabel11: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel5: TQRLabel;
    qryOP: TADOQuery;
    qryOPnCdOrdemProducao: TIntegerField;
    qryOPcNumeroOP: TStringField;
    qryOPcNmTipoOP: TStringField;
    qryOPdDtPrevConclusao: TDateTimeField;
    qryOPdDtInicioPrevisto: TDateTimeField;
    qryOPiSeq: TIntegerField;
    qryOPcNmCentroProdutivo: TStringField;
    qryOPcNmLinhaProducao: TStringField;
    qryOPcNmEtapaProducao: TStringField;
    qryOPcOBS: TStringField;
    qryOPnCdProduto: TIntegerField;
    qryOPcReferencia: TStringField;
    qryOPcNmProduto: TStringField;
    qryOPnQtdePlanejada: TBCDField;
    qryOPcNmTabTipoOrigemOP: TStringField;
    qryOPcCdEtapaOrdemProducao: TStringField;
    qryOPcInstrucoesProducao: TMemoField;
    QRLabel1: TQRLabel;
    QRDBText2: TQRDBText;
    qryOPnCdPedido: TIntegerField;
    qryOPcNmTerceiro: TStringField;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRDBRichText1: TQRDBRichText;
    QRDBText8: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    qryOPcUnidadeMedida: TStringField;
    QRDBText13: TQRDBText;
    SvQRBarcode1: TSvQRBarcode;
    SvQRBarcode2: TSvQRBarcode;
    QRLabel4: TQRLabel;
    QRDBText14: TQRDBText;
    qryOPcNmTabTipoStatusOP: TStringField;
    QRLabel13: TQRLabel;
    QRDBText15: TQRDBText;
    qryOPcNmTipoMaquinaPCP: TStringField;
    QRLabel19: TQRLabel;
    QRDBText16: TQRDBText;
    procedure QRGroup2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptOP: TrptOP;

implementation

{$R *.dfm}

procedure TrptOP.QRGroup2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

    SvQRBarcode1.Text := qryOPcCdEtapaOrdemProducao.Value ;

end;

procedure TrptOP.QRGroup1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

    SvQRBarcode2.Text := qryOPcNumeroOP.Value ;

end;

end.
