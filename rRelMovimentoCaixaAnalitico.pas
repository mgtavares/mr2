unit rRelMovimentoCaixaAnalitico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  RDprint, DB, ADODB, StdCtrls, Mask, DBCtrls;

type
  TrptRelMovimentoCaixaAnalitico = class(TfrmRelatorio_Padrao)
    qryLanctoFin: TADOQuery;
    qryPedidos: TADOQuery;
    qryFormaPagto: TADOQuery;
    qryParcelas: TADOQuery;
    RDprint1: TRDprint;
    qryPedidosnCdProduto: TIntegerField;
    qryPedidoscNmItem: TStringField;
    qryPedidosnQtdePed: TBCDField;
    qryPedidosnValUnitario: TBCDField;
    qryPedidosnValTotalItem: TBCDField;
    qryPedidosnCdPedido: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    qryFormaPagtocNmCondPagto: TStringField;
    qryFormaPagtonValPagto: TBCDField;
    qryFormaPagtonValDescontoCondPagto: TBCDField;
    qryFormaPagtonValDescontoManual: TBCDField;
    qryFormaPagtonValJurosCondPagto: TBCDField;
    qryFormaPagtocFlgEntrada: TIntegerField;
    qryParcelascNrTit: TStringField;
    qryParcelasnCdTitulo: TIntegerField;
    qryParcelasiParcela: TIntegerField;
    qryParcelasdDtVenc: TDateTimeField;
    qryParcelasnValTit: TBCDField;
    qryParcelascTerceiro: TStringField;
    qryRecebParcelas: TADOQuery;
    qryRecebParcelasnCdTitulo: TIntegerField;
    qryRecebParcelasiParcela: TIntegerField;
    qryRecebParcelasdDtVenc: TDateTimeField;
    qryRecebParcelasnValLiq: TBCDField;
    qryRecebParcelasnValJuro: TBCDField;
    qryRecebParcelascTerceiro: TStringField;
    qryValeGerado: TADOQuery;
    qryValeBaixado: TADOQuery;
    qryValeGeradonCdVale: TAutoIncField;
    qryValeGeradonValVale: TBCDField;
    qryValeBaixadonCdVale: TAutoIncField;
    qryValeBaixadonValVale: TBCDField;
    qryLanctoDiversos: TADOQuery;
    qryLanctoDiversosnValLancto: TBCDField;
    qryLanctoDiversoscHistorico: TStringField;
    qryPagtoCheque: TADOQuery;
    qryPagtoChequenCdBanco: TIntegerField;
    qryPagtoChequecAgencia: TStringField;
    qryPagtoChequecConta: TStringField;
    qryPagtoChequeiNrCheque: TIntegerField;
    qryPagtoChequenValCheque: TBCDField;
    qryPagtoChequecTerceiro: TStringField;
    qryChequeResgate: TADOQuery;
    qryChequeResgatenCdBanco: TIntegerField;
    qryChequeResgatecAgencia: TStringField;
    qryChequeResgatecConta: TStringField;
    qryChequeResgateiNrCheque: TIntegerField;
    qryChequeResgatenValCheque: TBCDField;
    qryChequeResgatecTerceiro: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    Label1: TLabel;
    dsLoja: TDataSource;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    dsContaBancaria: TDataSource;
    DBEdit4: TDBEdit;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Label4: TLabel;
    qryTipoLancto: TADOQuery;
    qryTipoLanctonCdTipoLancto: TIntegerField;
    qryTipoLanctocNmTipoLancto: TStringField;
    DBEdit1: TDBEdit;
    dsTipoLancto: TDataSource;
    RadioGroup1: TRadioGroup;
    qryLanctoFinnCdLanctoFin: TAutoIncField;
    qryLanctoFindDtLancto: TDateTimeField;
    qryLanctoFinnCdConta: TStringField;
    qryLanctoFinTipoLanctoFin: TStringField;
    qryLanctoFincNmUsuario: TStringField;
    qryLanctoFincFlgEstorno: TIntegerField;
    procedure ToolButton1Click(Sender: TObject);
    procedure RDprint1NewPage(Sender: TObject; Pagina: Integer);
    procedure Edit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
    procedure Edit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit3Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRelMovimentoCaixaAnalitico: TrptRelMovimentoCaixaAnalitico;
  iLinha, iPagina              : integer;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure Verifica_Data(Componente: TMaskEdit);
begin
  try
    if Componente.Text <> '  /  /    ' Then
      StrToDate(Componente.Text);
  except
    frmMenu.MensagemAlerta('Data Inv�lida');
    Componente.Clear;
    Componente.SetFocus;
    Abort;
  end;
end;

procedure TrptRelMovimentoCaixaAnalitico.ToolButton1Click(Sender: TObject);
var
  cBancoAgenciaConta : string;
  iLinhaReuso        : integer;

begin
  inherited;

  {-- faz a consist�ncia dos filtros --}
  Verifica_Data(MaskEdit1);
  Verifica_Data(MaskEdit2);

  if (MaskEdit1.Text = '  /  /    ') then
      MaskEdit1.Text := DateToStr(Now());

  if (MaskEdit2.Text = '  /  /    ') then
      MaskEdit2.Text := DateToStr(Now());

  if (frmMenu.ConvData(MaskEdit1.Text) > frmMenu.ConvData(MaskEdit2.Text)) then
  begin
      MensagemAlerta('Data inicial maior do que a data final');
      Abort;
  end;

  {-- inicia modelagem do relat�rio --}

  qryLanctoFin.Close;

  if ((Trim(Edit1.Text) = '') And (DBEdit2.Text = '')) then
  begin
      MensagemAlerta('Selecione uma loja');
      Edit1.SetFocus;
      Abort;
  end
  else qryLanctoFin.Parameters.ParamByName('nCdLoja').Value := Edit1.Text;

  if ((Trim(Edit2.Text) = '') And (DBEdit4.Text = '')) then
      qryLanctoFin.Parameters.ParamByName('nCdContaBancaria').Value  := 0
  else qryLanctoFin.Parameters.ParamByName('nCdContaBancaria').Value := Edit2.Text;

  if ((Trim(Edit3.Text) = '') And (DBEdit1.Text = '')) then
      qryLanctoFin.Parameters.ParamByName('nCdTipoLancto').Value  := 0
  else qryLanctoFin.Parameters.ParamByName('nCdTipoLancto').Value := Edit3.Text;

  qryLanctoFin.Parameters.ParamByName('dDtInicial').Value       := MaskEdit1.Text;
  qryLanctoFin.Parameters.ParamByName('dDtFinal').Value         := MaskEdit2.Text;

  qryLanctoFin.Parameters.ParamByName('cFlgSomenteEstorno').Value := RadioGroup1.ItemIndex;
  qryLanctoFin.Open;

  if not qryLanctoFin.Eof then
  begin

      iLinha  := 6;
      iPagina := 0;

      RDprint1.Abrir;

      while not qryLanctoFin.Eof do
      begin

          if(iLinha > 62) then
          begin
              iLinha := 6;
              RDprint1.Novapagina;
          end;

          RDprint1.ImpF(iLinha,3,'Lan�amento: ',[comp17,negrito]);
          RDprint1.ImpF(iLinha,10,qryLanctoFinnCdLanctoFin.AsString,[comp17]);
          RDprint1.ImpF(iLinha,17,'Data/Hora: ',[comp17,negrito]);
          RDprint1.ImpD(iLinha,35,qryLanctoFindDtLancto.AsString,[comp17]);
          RDprint1.ImpF(iLinha,41,'Caixa   : ',[comp17,negrito]);
          RDprint1.ImpF(iLinha,48,qryLanctoFinnCdConta.AsString,[comp17]);
          RDprint1.ImpF(iLinha + 1,3,'Tipo      : ',[comp17,negrito]);
          RDprint1.ImpF(iLinha + 1,10,qryLanctoFinTipoLanctoFin.AsString,[comp17]);
          RDprint1.ImpF(iLinha + 1,41,'Operador: ',[comp17,negrito]);
          RDprint1.ImpF(iLinha + 1,48,qryLanctoFincNmUsuario.AsString,[comp17]);

          iLinha := iLinha + 3;

          if (qryLanctoFincFlgEstorno.Value = 1) then
          begin
              RDprint1.ImpF(iLinha,33,'*** ESTORNADO ***',[comp12,negrito]);
              iLinha := iLinha + 2;
          end;
          
          {-- produtos que pertencem ao lanctofin --}

          qryPedidos.Close;
          qryPedidos.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.AsString;
          qryPedidos.Open;

          if not qryPedidos.Eof then
          begin
              RDprint1.ImpF(iLinha,3,'Itens da Venda',[comp17,negrito]);

              iLinha := iLinha + 1;

              if(iLinha > 62) then
              begin
                  iLinha := 6;
                  RDprint1.Novapagina;
              end;

              RDprint1.ImpF(iLinha,6,'Pedido',[comp17,negrito]);
              RDprint1.ImpF(iLinha,16,'C�d.',[comp17,negrito]);
              RDprint1.ImpF(iLinha,28,'Descri��o',[comp17,negrito]);
              RDprint1.ImpF(iLinha,51,'Qtde',[comp17,negrito]);
              RDprint1.ImpF(iLinha,56,'Val. Unit�rio',[comp17,negrito]);
              RDprint1.ImpF(iLinha,68,'Val. Total',[comp17,negrito]);

              while not qryPedidos.Eof do
              begin
                  iLinha := iLinha + 1;

                  if(iLinha > 62) then
                  begin
                      iLinha := 6;
                      RDprint1.Novapagina;
                  end;

                  RDprint1.ImpD(iLinha,9,qryPedidosnCdPedido.AsString,[comp17]);
                  RDprint1.ImpD(iLinha,18,qryPedidosnCdProduto.AsString,[comp17]);

                  {-- verifica se a descri��o precisa de quebra de linha --}

                  iLinhaReuso := iLinha;

                  if ((Length(qryPedidoscNmItem.AsString) > 50) and (Length(qryPedidoscNmItem.AsString) <= 100)) then
                  begin
                      RDprint1.ImpF(iLinha,20,Copy(qryPedidoscNmItem.AsString,1,50),[comp17]);
                      iLinha := iLinha + 1;
                      RDprint1.ImpF(iLinha,20,Copy(qryPedidoscNmItem.AsString,51,Length(qryPedidoscNmItem.AsString)),[comp17]);
                  end
                  else if ((Length(qryPedidoscNmItem.AsString) > 100) and (Length(qryPedidoscNmItem.AsString) <= 150)) then
                  begin
                      RDprint1.ImpF(iLinha,20,Copy(qryPedidoscNmItem.AsString,1,50),[comp17]);
                      iLinha := iLinha + 1;
                      RDprint1.ImpF(iLinha,20,Copy(qryPedidoscNmItem.AsString,51,100),[comp17]);
                      iLinha := iLinha + 1;
                      RDprint1.ImpF(iLinha,20,Copy(qryPedidoscNmItem.AsString,101,Length(qryPedidoscNmItem.AsString)),[comp17]);
                  end
                  else
                      RDprint1.ImpF(iLinha,20,qryPedidoscNmItem.AsString,[comp17]);

                  RDprint1.ImpD(iLinhaReuso,54,qryPedidosnQtdePed.AsString,[comp17]);
                  RDprint1.ImpVal(iLinhaReuso,58,'###,##0.00',qryPedidosnValUnitario.Value,[comp17]);
                  RDprint1.ImpVal(iLinhaReuso,68,'###,##0.00',qryPedidosnValTotalItem.Value,[comp17]);
                  qryPedidos.Next;
              end;

              iLinha := iLinha + 2;
          end;

          {-- parcelas que foram geradas no lanctofin --}

          qryParcelas.Close;
          qryParcelas.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.AsString;
          qryParcelas.Open;

          if not qryParcelas.Eof then
          begin
              RDprint1.ImpF(iLinha,3,'Parcelas Geradas',[comp17,negrito]);

              iLinha := iLinha + 1;

              if(iLinha > 62) then
              begin
                  iLinha := 6;
                  RDprint1.Novapagina;
              end;

              RDprint1.ImpF(iLinha,6,'N�m. T�tulo',[comp17,negrito]);
              RDprint1.ImpF(iLinha,16,'Parcela',[comp17,negrito]);
              RDprint1.ImpF(iLinha,23,'Dt. Vencto',[comp17,negrito]);
              RDprint1.ImpF(iLinha,34,'Valor Parc.',[comp17,negrito]);
              RDprint1.ImpF(iLinha,43,'Cliente',[comp17,negrito]);

              while not qryParcelas.Eof do
              begin
                  iLinha := iLinha + 1;

                  if(iLinha > 62) then
                  begin
                      iLinha := 6;
                      RDprint1.Novapagina;
                  end;

                  RDprint1.ImpD(iLinha,12,qryParcelascNrTit.Value,[comp17]);
                  RDprint1.ImpD(iLinha,21,qryParcelasiParcela.AsString,[comp17]);
                  RDprint1.ImpF(iLinha,23,qryParcelasdDtVenc.AsString,[comp17]);
                  RDprint1.ImpVal(iLinha,34,'###,##0.00',qryParcelasnValTit.Value,[comp17]);
                  RDprint1.ImpF(iLinha,43,qryParcelascTerceiro.AsString,[comp17]);
                  qryParcelas.Next;
              end;

              iLinha := iLinha + 2;
          end;

          {-- lista vale mercadoria gerado na movimenta��o --}

          qryValeGerado.Close;
          qryValeGerado.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.AsString;
          qryValeGerado.Open;

          if not qryValeGerado.Eof then
          begin
              RDprint1.ImpF(iLinha,3,'Vale Mercadoria Gerado',[comp17,negrito]);

              iLinha := iLinha + 1;

              if(iLinha > 62) then
              begin
                  iLinha := 6;
                  RDprint1.Novapagina;
              end;
              while not qryValeGerado.Eof do
              begin
                  RDprint1.ImpF(iLinha,6,'C�digo',[comp17,negrito]);
                  RDprint1.ImpF(iLinha,16,'Valor',[comp17,negrito]);

                  iLinha := iLinha + 1;

                  RDprint1.ImpD(iLinha,8,qryValeGeradonCdVale.AsString,[comp17]);
                  RDprint1.ImpVal(iLinha,13,'###,##0.00',qryValeGeradonValVale.Value,[comp17]);

                  qryValeGerado.Next;
              end;
              iLinha := iLinha + 2;
          end;

          {-- se houve baixa de vale mercadoria, lista aqui --}

          qryValeBaixado.Close;
          qryValeBaixado.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.AsString;
          qryValeBaixado.Open;

          if not qryValeBaixado.Eof then
          begin
              RDprint1.ImpF(iLinha,3,'Vale Mercadoria Baixado',[comp17,negrito]);

              iLinha := iLinha + 1;

              if(iLinha > 62) then
              begin
                  iLinha := 6;
                  RDprint1.Novapagina;
              end;
              while not qryValeBaixado.Eof do
              begin
                  RDprint1.ImpF(iLinha,6,'C�digo',[comp17,negrito]);
                  RDprint1.ImpF(iLinha,16,'Valor',[comp17,negrito]);

                  iLinha := iLinha + 1;

                  RDprint1.ImpD(iLinha,8,qryValeBaixadonCdVale.AsString,[comp17]);
                  RDprint1.ImpVal(iLinha,13,'###,##0.00',qryValeBaixadonValVale.Value,[comp17]);

                  qryValeBaixado.Next;
              end;
              iLinha := iLinha + 2;
          end;

          {-- aqui lista as parcelas recebidas --}

          qryRecebParcelas.Close;
          qryRecebParcelas.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.AsString;
          qryRecebParcelas.Open;

          if not qryRecebParcelas.Eof then
          begin
              RDprint1.ImpF(iLinha,3,'Parcelas Recebidas',[comp17,negrito]);
              iLinha := iLinha + 1;

              if(iLinha > 62) then
              begin
                  iLinha := 6;
                  RDprint1.Novapagina;
              end;

              RDprint1.ImpF(iLinha,6,'N�m. T�tulo',[comp17,negrito]);
              RDprint1.ImpF(iLinha,16,'Parcela',[comp17,negrito]);
              RDprint1.ImpF(iLinha,23,'Dt. Vencto',[comp17,negrito]);
              RDprint1.ImpF(iLinha,31,'Valor Pago',[comp17,negrito]);
              RDprint1.ImpF(iLinha,43,'Juros',[comp17,negrito]);
              RDprint1.ImpF(iLinha,48,'Cliente',[comp17,negrito]);

              while not qryRecebParcelas.Eof do
              begin
                  iLinha := iLinha + 1;

                  if(iLinha > 62) then
                  begin
                      iLinha := 6;
                      RDprint1.Novapagina;
                  end;

                  RDprint1.ImpD(iLinha,12,qryRecebParcelasnCdTitulo.AsString,[comp17]);
                  RDprint1.ImpD(iLinha,21,qryRecebParcelasiParcela.AsString,[comp17]);
                  RDprint1.ImpF(iLinha,23,qryRecebParcelasdDtVenc.AsString,[comp17]);
                  RDprint1.ImpVal(iLinha,31,'###,##0.00',qryRecebParcelasnValLiq.Value,[comp17]);
                  RDprint1.ImpVal(iLinha,40,'###,##0.00',qryRecebParcelasnValJuro.Value,[comp17]);
                  RDprint1.ImpF(iLinha,48,qryRecebParcelascTerceiro.AsString,[comp17]);
                  qryRecebParcelas.Next;
              end;

              iLinha := iLinha + 2;

          end;

          { -- Lista lan�amentos manuais,sagria,suprimento --}

          qryLanctoDiversos.Close;
          qryLanctoDiversos.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.AsString;
          qryLanctoDiversos.Open;

          if not qryLanctoDiversos.Eof then
          begin

             RDprint1.ImpF(iLinha + 1,3,'Valor    : ',[comp17,negrito]);
             RDprint1.ImpF(iLinha,3,'Hist�rico: ',[comp17,negrito]);

             RDprint1.ImpVal(iLinha + 1,11,'###,##0.00',qryLanctoDiversosnValLancto.Value,[comp17,negrito]);
             RDprint1.ImpF(iLinha,11,qryLanctoDiversoscHistorico.AsString,[comp17,negrito]);

             iLinha := iLinha + 3;

          end;

          {-- cheques recebidos como forma de pagamento --}

          qryPagtoCheque.Close;
          qryPagtoCheque.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.AsString;
          qryPagtoCheque.Open;

          if not qryPagtoCheque.Eof then
          begin

              RDprint1.ImpF(iLinha,3,'Cheques Gerados',[comp17,negrito]);

              iLinha := iLinha + 1;

              if(iLinha > 62) then
              begin
                  iLinha := 6;
                  RDprint1.Novapagina;
              end;

              RDprint1.ImpF(iLinha,6,'Banco/Ag�ncia/Conta',[comp17,negrito]);
              RDprint1.ImpF(iLinha,27,'Nr. Cheque',[comp17,negrito]);
              RDprint1.ImpF(iLinha,40,'Valor',[comp17,negrito]);
              RDprint1.ImpF(iLinha,48,'Cliente',[comp17,negrito]);

              while not qryPagtoCheque.Eof do
              begin
                  iLinha := iLinha + 1;

                  if(iLinha > 62) then
                  begin
                      iLinha := 6;
                      RDprint1.Novapagina;
                  end;

                  cBancoAgenciaConta := qryPagtoChequenCdBanco.AsString + '/'
                                      + qryPagtoChequecAgencia.AsString + '/'
                                      + qryPagtoChequecConta.AsString;

                  RDprint1.ImpD(iLinha,20,cBancoAgenciaConta,[comp17]);
                  RDprint1.ImpD(iLinha,32,qryPagtoChequeiNrCheque.AsString,[comp17]);
                  RDprint1.ImpVal(iLinha,37,'###,##0.00',qryPagtoChequenValCheque.Value,[comp17]);
                  RDprint1.ImpF(iLinha,48,qryPagtoChequecTerceiro.AsString,[comp17]);
                  qryPagtoCheque.Next;
              end;

              iLinha := iLinha + 2;
          end;

          {-- cheques resgatados --}

          qryChequeResgate.Close;
          qryChequeResgate.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.AsString;
          qryChequeResgate.Open;

          if not qryChequeResgate.Eof then
          begin
              RDprint1.ImpF(iLinha,3,'Cheques Resgatados',[comp17,negrito]);

              iLinha := iLinha + 1;

              if(iLinha > 62) then
              begin
                  iLinha := 6;
                  RDprint1.Novapagina;
              end;

              RDprint1.ImpF(iLinha,6,'Banco/Ag�ncia/Conta',[comp17,negrito]);
              RDprint1.ImpF(iLinha,27,'Nr. Cheque',[comp17,negrito]);
              RDprint1.ImpF(iLinha,40,'Valor',[comp17,negrito]);
              RDprint1.ImpF(iLinha,48,'Cliente',[comp17,negrito]);

              while not qryChequeResgate.Eof do
              begin
                  iLinha := iLinha + 1;

                  if(iLinha > 62) then
                  begin
                      iLinha := 6;
                      RDprint1.Novapagina;
                  end;

                  cBancoAgenciaConta := qryChequeResgatenCdBanco.AsString + '/'
                                      + qryChequeResgatecAgencia.AsString + '/'
                                      + qryChequeResgatecConta.AsString;

                  RDprint1.ImpD(iLinha,20,cBancoAgenciaConta,[comp17]);
                  RDprint1.ImpD(iLinha,32,qryChequeResgateiNrCheque.AsString,[comp17]);
                  RDprint1.ImpVal(iLinha,37,'###,##0.00',qryChequeResgatenValCheque.Value,[comp17]);
                  RDprint1.ImpF(iLinha,48,qryChequeResgatecTerceiro.AsString,[comp17]);
                  qryChequeResgate.Next;
              end;

              iLinha := iLinha + 2;
          end;

          {-- formas de pagamentos utilizadas --}

          qryFormaPagto.Close;
          qryFormaPagto.Parameters.ParamByName('nCdLanctoFin').Value := qryLanctoFinnCdLanctoFin.AsString;
          qryFormaPagto.Open;

          if not qryFormaPagto.Eof then
          begin
              RDprint1.ImpF(iLinha,3,'Forma de Pagamento',[comp17,negrito]);

              iLinha := iLinha + 1;

              if(iLinha > 62) then
              begin
                  iLinha := 6;
                  RDprint1.Novapagina;
              end;

              RDprint1.ImpF(iLinha,6,'Forma',[comp17,negrito]);
              RDprint1.ImpF(iLinha,16,'Condi��o',[comp17,negrito]);
              RDprint1.ImpF(iLinha,28,'Valor Pago',[comp17,negrito]);
              RDprint1.ImpF(iLinha,41,'D.C',[comp17,negrito]);
              RDprint1.ImpF(iLinha,49,'D.M',[comp17,negrito]);
              RDprint1.ImpF(iLinha,57,'J.C',[comp17,negrito]);
              RDprint1.ImpF(iLinha,61,'Entrada',[comp17,negrito]);

              while not qryFormaPagto.Eof do
              begin
                  iLinha := iLinha + 1;

                  if(iLinha > 62) then
                  begin
                      iLinha := 6;
                      RDprint1.Novapagina;
                  end;

                  RDprint1.ImpF(iLinha,6,qryFormaPagtocNmFormaPagto.AsString,[comp17]);
                  RDprint1.ImpF(iLinha,16,qryFormaPagtocNmCondPagto.AsString,[comp17]);
                  RDprint1.ImpVal(iLinha,28,'###,##0.00',qryFormaPagtonValPagto.Value,[comp17]);
                  RDprint1.ImpVal(iLinha,41,'0.00',qryFormaPagtonValDescontoCondPagto.Value,[comp17]);
                  RDprint1.ImpVal(iLinha,49,'0.00',qryFormaPagtonValDescontoManual.Value,[comp17]);
                  RDprint1.ImpVal(iLinha,57,'0.00',qryFormaPagtonValJurosCondPagto.Value,[comp17]);

                  if qryFormaPagtocFlgEntrada.Value = 1 then
                      RDprint1.ImpF(iLinha,61,'SIM',[comp17]);
                  qryFormaPagto.Next;
              end;

              iLinha := iLinha + 2;

          end;

          iLinha := iLinha + 1;

          RDprint1.impBox(iLinha,01,'--------------------------------------------------------------------------------');
          iLinha := iLinha + 2;
          qryLanctoFin.Next;
      end;

      RDprint1.Fechar;
  end
  else
      MensagemAlerta('Nenhum registro encontrado para a consulta realizada.');
end;

procedure TrptRelMovimentoCaixaAnalitico.RDprint1NewPage(Sender: TObject;
  Pagina: Integer);
begin
  inherited;
  iPagina := iPagina + 1 ;

  rdPrint1.ImpF(1,1,frmMenu.cNmFantasiaEmpresa,[negrito]);
  rdPrint1.ImpD(1,80,'Data Emiss�o: ' + DateTimeToStr(Now()),[comp17]);
  rdPrint1.ImpF(2,1,'RELAT�RIO MOVIMENTO CAIXA - ANAL�TICO',[negrito])  ;
  rdPrint1.ImpD(2,80,'P�gina: ' + frmMenu.ZeroEsquerda(intToStr(iPagina),3),[comp17]);
  rdPrint1.ImpD(3,80,'Per�odo   : '    + MaskEdit1.Text + ' � ' + MaskEdit2.Text , [comp17]) ;
  rdPrint1.impBox(4,01,'--------------------------------------------------------------------------------');

end;

procedure TrptRelMovimentoCaixaAnalitico.Edit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        If (Trim(Edit1.Text)='') then
        begin
            MensagemAlerta('Para selecionar uma conta caixa, selecione primeiro uma loja');
            Abort;
        end
        else begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(126,'ContaBancaria.nCdLoja = ' + Edit1.Text);
        end;

        If (nPK > 0) then
        begin
            Edit2.Text := IntToStr(nPK) ;
            qryContaBancaria.Parameters.ParamByName('nCdLoja').Value := Edit1.Text;
            PosicionaQuery(qryContaBancaria, Edit2.Text) ;
        end ;

    end ;
    end;
end;

procedure TrptRelMovimentoCaixaAnalitico.Edit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(147);

            If (nPK > 0) then
            begin
                Edit1.Text := IntToStr(nPK);
                qryLoja.Close;
                qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
                PosicionaQuery(qryLoja, Edit1.Text);
            end ;

    end ;

  end ;
end;

procedure TrptRelMovimentoCaixaAnalitico.Edit1Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  qryContaBancaria.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja,Edit1.Text);
end;

procedure TrptRelMovimentoCaixaAnalitico.Edit2Exit(Sender: TObject);
begin
  inherited;
  if (Trim(Edit2.Text) = '') and (Trim(Edit1.Text)='') then
  begin
  end
  else if (Trim(Edit1.Text)='') then
  begin
      MensagemAlerta('Para selecionar uma conta caixa, selecione primeiro uma loja');
      Abort;
  end
  else begin
      qryContaBancaria.Close;
      qryContaBancaria.Parameters.ParamByName('nCdLoja').Value := Edit1.Text;
      PosicionaQuery(qryContaBancaria, Edit2.Text) ;
  end;
end;

procedure TrptRelMovimentoCaixaAnalitico.Edit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(130,'cFlgContaCaixa = 1');

            If (nPK > 0) then
            begin
                Edit3.Text := IntToStr(nPK);
                PosicionaQuery(qryTipoLancto, Edit3.Text);
            end ;

    end ;

  end ;
end;

procedure TrptRelMovimentoCaixaAnalitico.Edit3Exit(Sender: TObject);
begin
  inherited;
  qryTipoLancto.Close;
  PosicionaQuery(qryTipoLancto, Edit3.Text) ;
end;

procedure TrptRelMovimentoCaixaAnalitico.FormShow(Sender: TObject);
begin
  inherited;
  If (frmMenu.nCdLojaAtiva > 0) then
  begin
    qryLoja.Close;
    qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
    Edit1.Text := IntToStr(frmMenu.nCdLojaAtiva);
    PosicionaQuery(qryLoja, Edit1.Text);
  end;

end;

initialization
    RegisterClass(TrptRelMovimentoCaixaAnalitico) ;

end.
