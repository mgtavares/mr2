inherited frmPosTituloAberto_Cliente: TfrmPosTituloAberto_Cliente
  Left = 12
  Top = 200
  Caption = 'Relat'#243'rio - Posi'#231#227'o de T'#237'tulos em aberto - por terceiro'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 65
    Top = 48
    Width = 41
    Height = 13
    Caption = 'Empresa'
  end
  object Label2: TLabel [2]
    Left = 26
    Top = 96
    Width = 80
    Height = 13
    Caption = 'Esp'#233'cie de T'#237'tulo'
  end
  object Label5: TLabel [3]
    Left = 67
    Top = 120
    Width = 39
    Height = 13
    Caption = 'Terceiro'
  end
  object Label8: TLabel [4]
    Left = 32
    Top = 144
    Width = 74
    Height = 13
    Caption = 'Moeda Exibi'#231#227'o'
  end
  object Label3: TLabel [5]
    Left = 4
    Top = 168
    Width = 102
    Height = 13
    Caption = 'Intervalo Vencimento'
  end
  object Label6: TLabel [6]
    Left = 190
    Top = 168
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label9: TLabel [7]
    Tag = 1
    Left = 86
    Top = 70
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [9]
    Tag = 1
    Left = 184
    Top = 40
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    Enabled = False
    TabOrder = 1
  end
  object DBEdit3: TDBEdit [10]
    Tag = 1
    Left = 258
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 2
  end
  object DBEdit7: TDBEdit [11]
    Tag = 1
    Left = 184
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmEspTit'
    DataSource = DataSource3
    TabOrder = 3
  end
  object DBEdit9: TDBEdit [12]
    Tag = 1
    Left = 184
    Top = 112
    Width = 129
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = DataSource4
    TabOrder = 4
  end
  object DBEdit10: TDBEdit [13]
    Tag = 1
    Left = 320
    Top = 112
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource4
    TabOrder = 5
  end
  object DBEdit12: TDBEdit [14]
    Tag = 1
    Left = 184
    Top = 136
    Width = 73
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource5
    TabOrder = 6
  end
  object DBEdit13: TDBEdit [15]
    Tag = 1
    Left = 264
    Top = 136
    Width = 654
    Height = 21
    DataField = 'cNmMoeda'
    DataSource = DataSource5
    TabOrder = 7
  end
  object MaskEdit1: TMaskEdit [16]
    Left = 112
    Top = 160
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 13
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [17]
    Left = 208
    Top = 160
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 14
    Text = '  /  /    '
  end
  object MaskEdit5: TMaskEdit [18]
    Left = 112
    Top = 88
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 10
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object MaskEdit6: TMaskEdit [19]
    Left = 112
    Top = 112
    Width = 65
    Height = 21
    EditMask = '###############;1; '
    MaxLength = 15
    TabOrder = 11
    Text = '               '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object MaskEdit7: TMaskEdit [20]
    Left = 112
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 12
    Text = '      '
    OnExit = MaskEdit7Exit
  end
  object RadioGroup1: TRadioGroup [21]
    Left = 112
    Top = 192
    Width = 185
    Height = 65
    Caption = 'Tipo de Lan'#231'amento'
    ItemIndex = 0
    Items.Strings = (
      'T'#237'tulos a Pagar'
      'T'#237'tulos a Receber')
    TabOrder = 15
  end
  object MaskEdit4: TMaskEdit [22]
    Left = 112
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 9
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object DBEdit1: TDBEdit [23]
    Tag = 1
    Left = 184
    Top = 64
    Width = 649
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 16
  end
  object MaskEdit3: TMaskEdit [24]
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    TabOrder = 8
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  inherited ImageList1: TImageList
    Left = 808
    Top = 80
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 488
    Top = 112
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUnidadeNegocio'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM UNIDADENEGOCIO'
      ' WHERE nCdUnidadeNegocio = :nCdUnidadeNegocio')
    Left = 536
    Top = 112
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object qryEspTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdEspTit int'
      '    SET @nCdEspTit =:nPK'
      ''
      'SELECT *'
      '  FROM ESPTIT'
      ' WHERE nCdEspTit = @nCdEspTit'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioEspTit'
      '               WHERE UsuarioEspTit.nCdEspTit      = @nCdEspTit'
      '                 AND UsuarioEspTit.nCdUsuario     = :nCdUsuario'
      '                 AND UsuarioEspTit.cFlgTipoAcesso = '#39'L'#39')'
      ''
      '')
    Left = 576
    Top = 112
    object qryEspTitnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitnCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryEspTitcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryEspTitcTipoMov: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object qryEspTitcFlgPrev: TIntegerField
      FieldName = 'cFlgPrev'
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nCdTerceiro')
    Left = 616
    Top = 104
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryMoeda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMoeda'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '   FROM Moeda'
      ' WHERE nCdMoeda = :nCdMoeda')
    Left = 664
    Top = 104
    object qryMoedanCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMoedacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 10
    end
    object qryMoedacNmMoeda: TStringField
      FieldName = 'cNmMoeda'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 304
  end
  object DataSource2: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 632
    Top = 312
  end
  object DataSource3: TDataSource
    DataSet = qryEspTit
    Left = 640
    Top = 320
  end
  object DataSource4: TDataSource
    DataSet = qryTerceiro
    Left = 648
    Top = 328
  end
  object DataSource5: TDataSource
    DataSet = qryMoeda
    Left = 656
    Top = 336
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK'
      
        '      AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja =' +
        ' Loja.nCdLoja AND UL.nCdUsuario = :nCdUsuario)')
    Left = 936
    Top = 80
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 976
    Top = 77
  end
end
