inherited frmCobranca_VisaoContato_Titulos: TfrmCobranca_VisaoContato_Titulos
  Left = 238
  Top = 258
  Width = 974
  Height = 241
  BorderIcons = [biSystemMenu]
  Caption = 'T'#237'tulos Vinculados ao Contato'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 958
    Height = 176
  end
  inherited ToolBar1: TToolBar
    Width = 958
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 958
    Height = 176
    Align = alClient
    DataSource = dsTitulos
    Flat = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footers = <>
        Width = 55
      end
      item
        EditButtons = <>
        FieldName = 'cNmEspTit'
        Footers = <>
        Width = 170
      end
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footers = <>
        Width = 105
      end
      item
        EditButtons = <>
        FieldName = 'iParcela'
        Footers = <>
        Width = 55
      end
      item
        EditButtons = <>
        FieldName = 'dDtEmissao'
        Footers = <>
        Width = 101
      end
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footers = <>
        Width = 102
      end
      item
        EditButtons = <>
        FieldName = 'nValTit'
        Footers = <>
        Width = 104
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoTit'
        Footers = <>
        Width = 108
      end
      item
        EditButtons = <>
        FieldName = 'dDtLiq'
        Footers = <>
        Width = 107
      end
      item
        EditButtons = <>
        FieldName = 'nCdLojaTit'
        Footers = <>
        Width = 66
      end>
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.iParcela'
      '      ,Titulo.dDtEmissao'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValTit'
      '      ,Titulo.nSaldoTit'
      '      ,Titulo.dDtLiq'
      '      ,dbo.fn_ZeroEsquerda(Titulo.nCdLojaTit,3) nCdLojaTit'
      '      ,cNmEspTit'
      '  FROM TituloListaCobranca'
      
        '       INNER JOIN Titulo ON Titulo.nCdTitulo = TituloListaCobran' +
        'ca.nCdTitulo'
      '       INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEspTit'
      ' WHERE nCdItemListaCobranca = :nPK')
    Left = 296
    Top = 64
    object qryTitulosnCdTitulo: TIntegerField
      DisplayLabel = 'C'#243'd.'
      FieldName = 'nCdTitulo'
    end
    object qryTituloscNrTit: TStringField
      DisplayLabel = 'N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosiParcela: TIntegerField
      DisplayLabel = 'Parc.'
      FieldName = 'iParcela'
    end
    object qryTitulosdDtEmissao: TDateTimeField
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryTitulosdDtVenc: TDateTimeField
      DisplayLabel = 'Dt. Vencto.'
      FieldName = 'dDtVenc'
    end
    object qryTitulosnValTit: TBCDField
      DisplayLabel = 'Valor Titulo'
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosnSaldoTit: TBCDField
      DisplayLabel = 'Saldo T'#237'tulo'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosdDtLiq: TDateTimeField
      DisplayLabel = 'Dt. Pagamento'
      FieldName = 'dDtLiq'
    end
    object qryTituloscNmEspTit: TStringField
      DisplayLabel = 'Esp'#233'cie'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTitulosnCdLojaTit: TStringField
      DisplayLabel = 'Loja'
      FieldName = 'nCdLojaTit'
      ReadOnly = True
      Size = 15
    end
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 328
    Top = 64
  end
end
