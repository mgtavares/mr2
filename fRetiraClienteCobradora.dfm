inherited frmRetiraClienteCobradora: TfrmRetiraClienteCobradora
  Left = 139
  Top = 184
  Width = 949
  Height = 579
  Caption = 'Retirar Cliente Cobradora'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1017
    Height = 540
  end
  object Label5: TLabel [1]
    Left = 41
    Top = 48
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
  end
  object Label1: TLabel [2]
    Left = 23
    Top = 72
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cobradora'
  end
  inherited ToolBar1: TToolBar
    Width = 1017
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtCdTerceiro: TMaskEdit [4]
    Left = 80
    Top = 40
    Width = 65
    Height = 21
    EditMask = '##########;1; '
    MaxLength = 10
    TabOrder = 1
    Text = '          '
    OnChange = edtCdTerceiroChange
    OnExit = edtCdTerceiroExit
    OnKeyDown = edtCdTerceiroKeyDown
  end
  object edtCdCobradora: TMaskEdit [5]
    Left = 80
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnChange = edtCdCobradoraChange
    OnExit = edtCdCobradoraExit
    OnKeyDown = edtCdCobradoraKeyDown
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 152
    Top = 64
    Width = 521
    Height = 21
    DataField = 'cNmCobradora'
    DataSource = DataSource1
    TabOrder = 3
  end
  object DBEdit2: TDBEdit [7]
    Tag = 1
    Left = 152
    Top = 40
    Width = 121
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = DataSource2
    TabOrder = 4
  end
  object DBEdit3: TDBEdit [8]
    Tag = 1
    Left = 280
    Top = 40
    Width = 393
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource2
    TabOrder = 5
  end
  object cxButton1: TcxButton [9]
    Left = 80
    Top = 88
    Width = 117
    Height = 29
    Caption = 'Exibir T'#237'tulos'
    TabOrder = 6
    OnClick = cxButton1Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000C6D6DEC6948C
      C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
      8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
      D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
      00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
      FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
      948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
      C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
      FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
      0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
      C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
      FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
      948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
      C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
      8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
      D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
      EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
      63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
      D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
    LookAndFeel.NativeStyle = True
  end
  object cxPageControl1: TcxPageControl [10]
    Left = 8
    Top = 128
    Width = 1009
    Height = 441
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 7
    ClientRectBottom = 441
    ClientRectRight = 1009
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'T'#237'tulos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1009
        Height = 418
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTitulos
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTitulo'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nCdLojaTit'
            Footers = <>
            Width = 35
          end
          item
            EditButtons = <>
            FieldName = 'cNrTit'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footers = <>
            Width = 41
          end
          item
            EditButtons = <>
            FieldName = 'dDtEmissao'
            Footers = <>
            Width = 77
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenc'
            Footers = <>
            Width = 77
          end
          item
            EditButtons = <>
            FieldName = 'cNmEspTit'
            Footers = <>
            Width = 211
          end
          item
            EditButtons = <>
            FieldName = 'nValTit'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValEncargoCobradora'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nSaldoTit'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'dDtRemCobradora'
            Footers = <>
            Width = 77
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object qryCobradora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCobradora, cNmCobradora'
      'FROM Cobradora'
      'WHERE nCdCobradora = :nPK')
    Left = 240
    Top = 224
    object qryCobradoranCdCobradora: TIntegerField
      FieldName = 'nCdCobradora'
    end
    object qryCobradoracNmCobradora: TStringField
      FieldName = 'cNmCobradora'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryCobradora
    Left = 384
    Top = 240
  end
  object qryCliente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro        '
      '      ,Terceiro.cNmTerceiro        '
      '      ,Terceiro.cCNPJCPF        '
      '  FROM Terceiro            '
      ' WHERE EXISTS(SELECT 1                                     '
      
        '                FROM TerceiroTipoTerceiro TTT                   ' +
        '                                '
      
        '               WHERE TTT.nCdTerceiro = Terceiro.nCdTerceiro     ' +
        '                                                    '
      '                 AND TTT.nCdTipoTerceiro = 2)'
      '   AND Terceiro.nCdTerceiro = :nPK')
    Left = 280
    Top = 157
    object qryClientenCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryClientecNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryClientecCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
  end
  object DataSource2: TDataSource
    DataSet = qryCliente
    Left = 392
    Top = 248
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdCobradora'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTitulo '
      '      ,nCdLojaTit'
      '      ,cNrTit'
      '      ,iParcela'
      '      ,dDtEmissao'
      '      ,dDtVenc'
      '      ,cNmEspTit'
      '      ,nValTit'
      '      ,nValEncargoCobradora'
      '      ,nSaldoTit'
      '      ,dbo.fn_OnlyDate(dDtRemCobradora) as dDtRemCobradora'
      '  FROM Titulo'
      '       INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEspTit'
      ' WHERE nSaldoTit     > 0'
      '   AND nCdEmpresa    = :nCdEmpresa'
      '   AND nCdTerceiro   = :nCdTerceiro'
      '   AND nCdCobradora  = :nCdCobradora'
      '   AND cFlgCobradora = 1')
    Left = 292
    Top = 304
    object qryTitulosnCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulos na Cobradora|C'#243'd.'
      FieldName = 'nCdTitulo'
    end
    object qryTitulosnCdLojaTit: TIntegerField
      DisplayLabel = 'T'#237'tulos na Cobradora|Loja'
      FieldName = 'nCdLojaTit'
    end
    object qryTituloscNrTit: TStringField
      DisplayLabel = 'T'#237'tulos na Cobradora|N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosiParcela: TIntegerField
      DisplayLabel = 'T'#237'tulos na Cobradora|Parc.'
      FieldName = 'iParcela'
    end
    object qryTitulosdDtEmissao: TDateTimeField
      DisplayLabel = 'T'#237'tulos na Cobradora|Dt. Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryTitulosdDtVenc: TDateTimeField
      DisplayLabel = 'T'#237'tulos na Cobradora|Dt. Vencto'
      FieldName = 'dDtVenc'
    end
    object qryTituloscNmEspTit: TStringField
      DisplayLabel = 'T'#237'tulos na Cobradora|Esp'#233'cie T'#237'tulo'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTitulosnValTit: TBCDField
      DisplayLabel = 'T'#237'tulos na Cobradora|Valor Original'
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosnValEncargoCobradora: TBCDField
      DisplayLabel = 'T'#237'tulos na Cobradora|Encargo Cobradora'
      FieldName = 'nValEncargoCobradora'
      Precision = 12
      Size = 2
    end
    object qryTitulosnSaldoTit: TBCDField
      DisplayLabel = 'T'#237'tulos na Cobradora|Saldo Atual'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosdDtRemCobradora: TDateTimeField
      DisplayLabel = 'T'#237'tulos na Cobradora|Dt. Rem. Cobradora'
      FieldName = 'dDtRemCobradora'
      ReadOnly = True
    end
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 332
    Top = 304
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 404
    Top = 320
  end
end
