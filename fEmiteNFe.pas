unit fEmiteNFe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ACBrNFeDANFEClass, ACBrNFeDANFeQRClass, ACBrNFe, pcnConversao, ACBrUtil,DB, ADODB, StrUtils;

type
  TfrmEmiteNFe = class(TForm)
    ACBrNFe1: TACBrNFe;
    ACBrNFeDANFEQR1: TACBrNFeDANFEQR;
    qryConfigAmbiente: TADOQuery;
    qryConfigAmbientecUFEmissaoNfe: TStringField;
    qryConfigAmbientecPathLogoNFe: TStringField;
    qryConfigAmbientenCdTipoAmbienteNFe: TIntegerField;
    qryConfigAmbientecFlgEmiteNFe: TIntegerField;
    qryConfigAmbientecNrSerieCertificadoNFe: TStringField;
    qryDoctoFiscal: TADOQuery;
    qryDoctoFiscalnCdDoctoFiscal: TAutoIncField;
    qryDoctoFiscalnCdEmpresa: TIntegerField;
    qryDoctoFiscalnCdLoja: TIntegerField;
    qryDoctoFiscalnCdSerieFiscal: TIntegerField;
    qryDoctoFiscalcFlgEntSai: TStringField;
    qryDoctoFiscalcModelo: TStringField;
    qryDoctoFiscalcSerie: TStringField;
    qryDoctoFiscalcCFOP: TStringField;
    qryDoctoFiscalcTextoCFOP: TStringField;
    qryDoctoFiscaliNrDocto: TIntegerField;
    qryDoctoFiscalnCdTipoDoctoFiscal: TIntegerField;
    qryDoctoFiscaldDtEmissao: TDateTimeField;
    qryDoctoFiscaldDtSaida: TDateTimeField;
    qryDoctoFiscaldDtImpressao: TDateTimeField;
    qryDoctoFiscalnCdTerceiro: TIntegerField;
    qryDoctoFiscalcNmTerceiro: TStringField;
    qryDoctoFiscalcCNPJCPF: TStringField;
    qryDoctoFiscalcIE: TStringField;
    qryDoctoFiscalcTelefone: TStringField;
    qryDoctoFiscalnCdEndereco: TIntegerField;
    qryDoctoFiscalcEndereco: TStringField;
    qryDoctoFiscaliNumero: TIntegerField;
    qryDoctoFiscalcBairro: TStringField;
    qryDoctoFiscalcCidade: TStringField;
    qryDoctoFiscalcCep: TStringField;
    qryDoctoFiscalcUF: TStringField;
    qryDoctoFiscalnValTotal: TBCDField;
    qryDoctoFiscalnValProduto: TBCDField;
    qryDoctoFiscalnValBaseICMS: TBCDField;
    qryDoctoFiscalnValICMS: TBCDField;
    qryDoctoFiscalnValBaseICMSSub: TBCDField;
    qryDoctoFiscalnValICMSSub: TBCDField;
    qryDoctoFiscalnValIsenta: TBCDField;
    qryDoctoFiscalnValOutras: TBCDField;
    qryDoctoFiscalnValDespesa: TBCDField;
    qryDoctoFiscalnValFrete: TBCDField;
    qryDoctoFiscaldDtCad: TDateTimeField;
    qryDoctoFiscalnCdUsuarioCad: TIntegerField;
    qryDoctoFiscalnCdStatus: TIntegerField;
    qryDoctoFiscalcFlgFaturado: TIntegerField;
    qryDoctoFiscalnCdTerceiroTransp: TIntegerField;
    qryDoctoFiscalcNmTransp: TStringField;
    qryDoctoFiscalcCNPJTransp: TStringField;
    qryDoctoFiscalcIETransp: TStringField;
    qryDoctoFiscalcEnderecoTransp: TStringField;
    qryDoctoFiscalcBairroTransp: TStringField;
    qryDoctoFiscalcCidadeTransp: TStringField;
    qryDoctoFiscalcUFTransp: TStringField;
    qryDoctoFiscaliQtdeVolume: TIntegerField;
    qryDoctoFiscalnPesoBruto: TBCDField;
    qryDoctoFiscalnPesoLiq: TBCDField;
    qryDoctoFiscalcPlaca: TStringField;
    qryDoctoFiscalcUFPlaca: TStringField;
    qryDoctoFiscalnValIPI: TBCDField;
    qryDoctoFiscalnValFatura: TBCDField;
    qryDoctoFiscalnCdCondPagto: TIntegerField;
    qryDoctoFiscalnCdIncoterms: TIntegerField;
    qryDoctoFiscalnCdUsuarioImpr: TIntegerField;
    qryDoctoFiscalnCdTerceiroPagador: TIntegerField;
    qryDoctoFiscaldDtCancel: TDateTimeField;
    qryDoctoFiscalnCdUsuarioCancel: TIntegerField;
    qryDoctoFiscalnCdTipoPedido: TIntegerField;
    qryDoctoFiscalcFlgIntegrado: TIntegerField;
    qryDoctoFiscalnValCredAdiant: TBCDField;
    qryDoctoFiscalnValIcentivoFiscal: TBCDField;
    qryDoctoFiscalnCdLanctoFinDoctoFiscal: TIntegerField;
    qryDoctoFiscaldDtContab: TDateTimeField;
    qryDoctoFiscaldDtIntegracao: TDateTimeField;
    qryDoctoFiscalnCdServidorOrigem: TIntegerField;
    qryDoctoFiscaldDtReplicacao: TDateTimeField;
    qryDoctoFiscalcFlgBoletoImpresso: TIntegerField;
    qryDoctoFiscalcNrProtocoloNFe: TStringField;
    qryDoctoFiscalcNrReciboNFe: TStringField;
    qryDoctoFiscalcFlgAPrazo: TIntegerField;
    qryConfigAmbientenCdUFEmissaoNFe: TIntegerField;
    qryConfigAmbientenCdMunicipioEmissaoNFe: TIntegerField;
    qryItemDoctoFiscal: TADOQuery;
    qryDoctoFiscalcComplEnderDestino: TStringField;
    qryDoctoFiscalnCdMunicipioDestinoIBGE: TIntegerField;
    qryDoctoFiscalnCdPaisDestino: TIntegerField;
    qryDoctoFiscalcNmPaisDestino: TStringField;
    qryDoctoFiscalcCNPJCPFEmitente: TStringField;
    qryDoctoFiscalcIEEmitente: TStringField;
    qryDoctoFiscalcNmRazaoSocialEmitente: TStringField;
    qryDoctoFiscalcNmFantasiaEmitente: TStringField;
    qryDoctoFiscalcTelefoneEmitente: TStringField;
    qryDoctoFiscalnCdEnderecoEmitente: TIntegerField;
    qryDoctoFiscalcEnderecoEmitente: TStringField;
    qryDoctoFiscaliNumeroEmitente: TIntegerField;
    qryDoctoFiscalcComplEnderEmitente: TStringField;
    qryDoctoFiscalcBairroEmitente: TStringField;
    qryDoctoFiscalnCdMunicipioEmitenteIBGE: TIntegerField;
    qryDoctoFiscalcCidadeEmitente: TStringField;
    qryDoctoFiscalcUFEmitente: TStringField;
    qryDoctoFiscalcCepEmitente: TStringField;
    qryDoctoFiscalnCdPaisEmitente: TIntegerField;
    qryDoctoFiscalcNmPaisEmitente: TStringField;
    qryDoctoFiscalcSuframa: TStringField;
    qryItemDoctoFiscalnCdProduto: TStringField;
    qryItemDoctoFiscalcNmItem: TStringField;
    qryItemDoctoFiscalcDescricaoAdicional: TStringField;
    qryItemDoctoFiscalcCFOP: TStringField;
    qryItemDoctoFiscalcNCM: TStringField;
    qryItemDoctoFiscalcCdST: TStringField;
    qryItemDoctoFiscalcCdSTIPI: TStringField;
    qryItemDoctoFiscalcUnidadeMedida: TStringField;
    qryItemDoctoFiscalnCdTabTipoOrigemMercadoria: TIntegerField;
    qryItemDoctoFiscalnValUnitario: TBCDField;
    qryItemDoctoFiscalnAliqICMS: TBCDField;
    qryItemDoctoFiscalnAliqIPI: TBCDField;
    qryItemDoctoFiscalnPercIVA: TBCDField;
    qryItemDoctoFiscalnPercBCICMS: TBCDField;
    qryItemDoctoFiscalnPercBCIVA: TBCDField;
    qryItemDoctoFiscalnPercBCICMSST: TBCDField;
    qryItemDoctoFiscalnQtde: TBCDField;
    qryItemDoctoFiscalnValTotal: TBCDField;
    qryItemDoctoFiscalnValBaseICMS: TBCDField;
    qryItemDoctoFiscalnValICMS: TBCDField;
    qryItemDoctoFiscalnValIPI: TBCDField;
    qryItemDoctoFiscalnValBaseIPI: TBCDField;
    qryItemDoctoFiscalnValBASEICMSSub: TBCDField;
    qryItemDoctoFiscalnValICMSSub: TBCDField;
    qryItemDoctoFiscalnValIsenta: TBCDField;
    qryItemDoctoFiscalnValOutras: TBCDField;
    qryItemDoctoFiscalnValDespesa: TBCDField;
    qryItemDoctoFiscalnValFrete: TBCDField;
    qryPrazoDoctoFiscal: TADOQuery;
    qryPrazoDoctoFiscaliParcela: TIntegerField;
    qryPrazoDoctoFiscaldDtVenc: TDateTimeField;
    qryPrazoDoctoFiscalnValPagto: TBCDField;
    qryDoctoFiscalcXMLNFe: TMemoField;
    qryDoctoFiscalcChaveNFe: TStringField;
    qryMsgDoctoFiscal: TADOQuery;
    qryMsgDoctoFiscalcMsg: TStringField;
    qryDoctoFiscalcNrProtocoloCancNFe: TStringField;
    qryDoctoFiscalcCaminhoXML: TStringField;
    qryDoctoFiscalcArquivoXML: TStringField;
    qryConfigAmbientecPathArqNFe: TStringField;
    qryConfigAmbientecServidorSMTP: TStringField;
    qryConfigAmbientenPortaSMTP: TIntegerField;
    qryConfigAmbientecEmail: TStringField;
    qryConfigAmbientecSenhaEmail: TStringField;
    qryConfigAmbientecFlgUsaSSL: TIntegerField;
    qryDescontoTotal: TADOQuery;
    qryDescontoTotalnValDescontoItens: TBCDField;
    qryTerceiro: TADOQuery;
    qryTerceirocFlgTipoDescontoDanfe: TIntegerField;
    qryItemDoctoFiscalnValDesconto: TBCDField;
    qryDoctoFiscalnValSeguro: TBCDField;
    qryDoctoFiscalcFlgComplementar: TIntegerField;
    qryDoctoFiscalnCdDoctoFiscalOrigemCompl: TIntegerField;
    qryDoctoFiscalcNmMotivoComplemento: TStringField;
    qryDoctoFiscalcNFeRef: TStringField;
    procedure preparaNFe();
    procedure gerarNFe(nCdDoctoFiscal:integer);
    procedure imprimirDANFe(nCdDoctoFiscal:integer) ;
    procedure ReimprimirDANFe(nCdDoctoFiscal:integer) ;
    procedure cancelarNFe(nCdDoctoFiscal:integer);
    procedure enviaEmailNFe(nCdDoctoFiscal:integer; cPara, cCopia : String);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmiteNFe: TfrmEmiteNFe;

implementation

uses fMenu, fConfigAmbiente;

{$R *.dfm}

{ TfrmEmiteNFe }

procedure TfrmEmiteNFe.cancelarNFe(nCdDoctoFiscal: integer);
var
   vAux : string ;
label
   pedeJustificativa;
begin
  {-- prepara o ambiente para cancelamento da NFe --}
  preparaNFe() ;

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryDoctoFiscal.Open;

  if (qryDoctoFiscalcNrProtocoloCancNFe.Value <> '') then
      exit ;

  pedeJustificativa:

  if not(frmMenu.InputQuery('Cancelamento de NFe', 'Justificativa', vAux)) then
     exit;

  if (Length(vAux) < 15) then
  begin
      frmMenu.MensagemAlerta('A justificativa deve ter no m�nimo 15 letras.') ;
      goto pedeJustificativa;
  end ;

  try
      frmMenu.mensagemUsuario('Carregando XML NFe...');
      ACBrNFe1.NotasFiscais.Clear;
      ACBrNFe1.NotasFiscais.LoadFromFile(qryDoctoFiscalcCaminhoXML.Value + qryDoctoFiscalcArquivoXML.Value) ;
      frmMenu.mensagemUsuario('Cancelando NFe...');
      ACBrNFe1.Cancelamento(vAux);
  except
      frmMenu.mensagemUsuario('');
      raise ;
  end ;

  frmMenu.mensagemUsuario('');

  qryDoctoFiscal.Edit ;
  qryDoctoFiscalcNrProtocoloCancNFe.Value := ACBrNFe1.WebServices.Cancelamento.Protocolo ;
  qryDoctoFiscal.Post;

end;

procedure TfrmEmiteNFe.gerarNFe(nCdDoctoFiscal: integer);
var
    iItem, iAux : integer ;
begin

  {-- prepara o ambiente para gera��o da NFe --}
  preparaNFe() ;

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryDoctoFiscal.Open;

  qryItemDoctoFiscal.Close;
  qryItemDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryItemDoctoFiscal.Open;

  // Limpas as Notas Fiscais que possam estar na mem�ria do componente
  frmMenu.mensagemUsuario('Gerando NFe...');

  ACBrNFe1.NotasFiscais.Clear;

  iItem := 1 ;

  with ACBrNFe1.NotasFiscais.Add.NFe do
  begin


      {-- trata a identifica��o da nota fiscal --}
      Ide.natOp   := qryDoctoFiscalcTextoCFOP.Value ;
      Ide.nNF     := qryDoctoFiscaliNrDocto.Value;
      Ide.cNF     := qryDoctoFiscaliNrDocto.Value;
      Ide.modelo  := 55;
      Ide.serie   := 1;
      Ide.dEmi    := qryDoctoFiscaldDtEmissao.Value;
      Ide.dSaiEnt := Date;

      if (qryConfigAmbientenCdTipoAmbienteNFe.Value = 0) then
          Ide.tpAmb := taProducao
      else Ide.tpAmb := taHomologacao;

      if (qryDoctoFiscalcFlgEntSai.Value = 'S') then
          Ide.tpNF := tnSaida
      else Ide.tpNF := tnEntrada ;

      if (qryDoctoFiscalnValFatura.Value > 0) then
          if (qryDoctoFiscalcFlgAPrazo.Value = 1) then
              Ide.indPag  := ipPrazo
          else Ide.indPag := ipVista ;

      if (qryDoctoFiscalnValFatura.Value = 0) then
          Ide.indPag := ipOutras ;


      Ide.verProc := '1.0.0.0';
      Ide.cUF     := qryConfigAmbientenCdUFEmissaoNFe.Value ;
      Ide.cMunFG  := qryConfigAmbientenCdMunicipioEmissaoNFe.Value ;

      {-- Verifica se � uma nota complementar --}
      
      if (qryDoctoFiscalcFlgComplementar.Value = 0) then
          Ide.finNFe  := fnNormal
      else
      begin
          Ide.finNFe  := fnComplementar ;
          Ide.NFref.Add.refNFe := qryDoctoFiscalcNFeRef.Value;
      end;

      Ide.tpEmis  := teNormal ;
      Ide.procEmi := peAplicativoContribuinte ;
      ide.tpImp   := tiRetrato ;

      {-- trata a identifica��o do emitente da nota fiscal --}

      Emit.CNPJCPF           := qryDoctoFiscalcCNPJCPFEmitente.Value ;
      Emit.IE                := qryDoctoFiscalcIEEmitente.Value ;
      Emit.xNome             := qryDoctoFiscalcNmRazaoSocialEmitente.Value ;
      Emit.xFant             := qryDoctoFiscalcNmFantasiaEmitente.Value ;
      Emit.EnderEmit.fone    := qryDoctoFiscalcTelefoneEmitente.Value ;
      Emit.EnderEmit.CEP     := qryDoctoFiscalcCepEmitente.asInteger ;
      Emit.EnderEmit.xLgr    := qryDoctoFiscalcEnderecoEmitente.Value ;
      Emit.EnderEmit.nro     := qryDoctoFiscaliNumeroEmitente.asString ;
      Emit.EnderEmit.xCpl    := qryDoctoFiscalcComplEnderEmitente.Value ;
      Emit.EnderEmit.xBairro := qryDoctoFiscalcBairroEmitente.Value ;
      Emit.EnderEmit.cMun    := qryDoctoFiscalnCdMunicipioEmitenteIBGE.Value ;
      Emit.EnderEmit.xMun    := qryDoctoFiscalcCidadeEmitente.Value ;
      Emit.EnderEmit.UF      := qryDoctoFiscalcUFEmitente.Value ;
      Emit.enderEmit.cPais   := qryDoctoFiscalnCdPaisEmitente.Value ;
      Emit.enderEmit.xPais   := qryDoctoFiscalcNmPaisEmitente.Value ;

      {-- trata a identifica��o do destinat�rio da nota fiscal --}
      Dest.CNPJCPF           := qryDoctoFiscalcCNPJCPF.Value ;
      Dest.EnderDest.CEP     := qryDoctoFiscalcCEP.asInteger ;
      Dest.EnderDest.xLgr    := qryDoctoFiscalcEndereco.Value ;
      Dest.EnderDest.nro     := qryDoctoFiscaliNumero.asString ;
      Dest.EnderDest.xCpl    := qryDoctoFiscalcComplEnderDestino.Value;
      Dest.EnderDest.xBairro := qryDoctoFiscalcBairro.Value ;
      Dest.EnderDest.cMun    := qryDoctoFiscalnCdMunicipioDestinoIBGE.Value;
      Dest.EnderDest.xMun    := qryDoctoFiscalcCidade.Value;
      Dest.EnderDest.UF      := qryDoctoFiscalcUF.Value ;
      Dest.EnderDest.Fone    := qryDoctoFiscalcTelefone.Value;
      Dest.IE                := qryDoctoFiscalcIE.Value;
      Dest.xNome             := qryDoctoFiscalcNmTerceiro.Value ;
      Dest.EnderDest.cPais   := qryDoctoFiscalnCdPaisDestino.Value ;
      Dest.EnderDest.xPais   := qryDoctoFiscalcNmPaisDestino.Value ;
      Dest.ISUF              := qryDoctoFiscalcSuframa.Value ;

      // busca tipo de desconto
      qryTerceiro.Close;
      qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := qryDoctoFiscalnCdTerceiro.Value;
      qryTerceiro.Open;

      {-- trata dos itens da nota fiscal --}

      qryItemDoctoFiscal.First;

      while not qryItemDoctoFiscal.Eof do
      begin

          with Det.Add do
          begin
              infAdProd     := qryItemDoctoFiscalcDescricaoAdicional.Value;
              Prod.nItem    := iItem;
              Prod.CFOP     := qryItemDoctoFiscalcCFOP.Value;
              Prod.cProd    := qryItemDoctoFiscalnCdProduto.Value;
              Prod.xProd    := qryItemDoctoFiscalcNmItem.Value;
              Prod.qCom     := qryItemDoctoFiscalnQtde.Value;
              Prod.uCom     := qryItemDoctoFiscalcUnidadeMedida.Value;
              Prod.vProd    := qryItemDoctoFiscalnValTotal.Value;
              Prod.vUnCom   := qryItemDoctoFiscalnValUnitario.Value;
              Prod.qTrib    := qryItemDoctoFiscalnQtde.Value;
              Prod.uTrib    := qryItemDoctoFiscalcUnidadeMedida.Value;
              Prod.vUnTrib  := qryItemDoctoFiscalnValUnitario.Value;
              Prod.NCM      := qryItemDoctoFiscalcNCM.Value;

              // desconto no total da nf, imprime o valor bruto unitario
              if qryTerceirocFlgTipoDescontoDanfe.Value = 1 then
              begin
                  Prod.vProd    := qryItemDoctoFiscalnValTotal.Value + (qryItemDoctoFiscalnQtde.Value * qryItemDoctoFiscalnValDesconto.Value);
                  Prod.vUnCom   := (qryItemDoctoFiscalnValUnitario.Value + qryItemDoctoFiscalnValDesconto.Value);
              end;

              with Imposto do
              begin

                  with ICMS do
                  begin
                      case qryItemDoctoFiscalcCdST.asInteger of
                              0  : CST := cst00 ;
                              10 : CST := cst10 ;
                              20 : CST := cst20 ;
                              30 : CST := cst30 ;
                              40 : CST := cst40 ;
                              41 : CST := cst41 ;
                              45 : CST := cst45 ;
                              50 : CST := cst50 ;
                              51 : CST := cst51 ;
                              60 : CST := cst60 ;
                              70 : CST := cst70 ;
                              80 : CST := cst80 ;
                              81 : CST := cst81 ;
                              90 : CST := cst90 ;
                      end ;
                      ICMS.CST := CST;

                      if (qryItemDoctoFiscalnValICMS.Value > 0) then
                      begin

                          {CST           := TpcnCSTIcms(AnsiIndexStr(qryItemDoctoFiscalcCdST.asString, ['00', '10','20','30','40','41','45','50','51','60','70','80','81','90'])) ;}
                          ICMS.modBC    := dbiValorOperacao;
                          ICMS.pICMS    := qryItemDoctoFiscalnAliqICMS.Value;
                          ICMS.vICMS    := qryItemDoctoFiscalnValICMS.Value;
                          ICMS.vBC      := qryItemDoctoFiscalnValBaseICMS.Value;
                          ICMS.orig     := TpcnOrigemMercadoria(qryItemDoctoFiscalnCdTabTipoOrigemMercadoria.Value) ;

                          if (qryItemDoctoFiscalnValICMSSub.Value > 0) then
                          begin

                              if (qryItemDoctoFiscalnPercIVA.Value > 0) then
                              begin
                                  ICMS.modBC   := dbiMargemValorAgregado ;
                                  ICMS.modBCST := dbisMargemValorAgregado ;
                              end ;

                              ICMS.pMVAST   := qryItemDoctoFiscalnPercIVA.Value ;
                              ICMS.pRedBCST := (100-qryItemDoctoFiscalnPercBCICMSST.Value) ;
                              ICMS.vBCST    := qryItemDoctoFiscalnValBaseICMSSub.Value ;
                              ICMS.vICMSST  := qryItemDoctoFiscalnValICMSSub.Value ;
                              ICMS.pICMSST  := qryItemDoctoFiscalnAliqICMS.Value;

                          end ;

                      end;

                  end;

                  if (qryItemDoctoFiscalnValIPI.Value > 0) then
                  begin

                      IPI.CST   := TpcnCstIpi(AnsiIndexStr(qryItemDoctoFiscalcCdSTIPI.AsString,['00','49','50','99','01','02','03','04','05','51','52','53','54','55'])) ;

                      {--s� informar qunado o calculo do IPI for baseado em al�quota, n�o em quantidade --}
                      IPI.vBC   := qryItemDoctoFiscalnValBaseIPI.Value ;
                      IPI.pIPI  := qryItemDoctoFiscalnAliqIPI.Value ;
                      IPI.vIPI  := qryItemDoctoFiscalnValIPI.Value ;

                      {--s� informar quando o CNPJ do produtor for diferente do CNPJ do emitente --}
                      {IPI.CNPJProd := Emit.CNPJCPF;}

                      {--s� informar qunado o calculo do IPI for baseado em quantidade, n�o em aliquota --}
                      {IPI.vUnid    := qryItemDoctoFiscalnValUnitario.Value ;}


                  end ;

              end;

          end ;

          {-- incrementa a variavel --}
          inc(iItem) ;

          qryItemDoctoFiscal.Next;

      end;

      qryItemDoctoFiscal.Close;


      {-- grava os dados do transporte --}

      if (qryDoctoFiscalnCdIncoterms.Value = 2) then
          Transp.modFrete := TpcnModalidadeFrete(1)
      else Transp.modFrete := TpcnModalidadeFrete(0) ;

      Transp.Transporta.CNPJCPF := qryDoctoFiscalcCNPJTransp.Value ;
      Transp.Transporta.xNome   := qryDoctoFiscalcNmTransp.Value ;
      Transp.Transporta.IE      := qryDoctoFiscalcIETransp.Value ;
      Transp.Transporta.xEnder  := qryDoctoFiscalcEnderecoTransp.Value ;
      Transp.Transporta.xMun    := qryDoctoFiscalcCidadeTransp.Value ;
      Transp.Transporta.UF      := qryDoctoFiscalcUFTransp.Value ;

      Transp.veicTransp.placa   := qryDoctoFiscalcPlaca.Value ;
      Transp.veicTransp.UF      := qryDoctoFiscalcUFPlaca.Value ;

      with Transp.Vol.Add do
      begin
          qVol  := qryDoctoFiscaliQtdeVolume.Value ;
          pesoL := qryDoctoFiscalnPesoLiq.Value ;
          pesoB := qryDoctoFiscalnPesoBruto.Value ;
      end ;

      {-- grava os totais da nota --}
      Total.ICMSTot.vBC    := qryDoctoFiscalnValBaseICMS.Value;
      Total.ICMSTot.vICMS  := qryDoctoFiscalnValICMS.Value;
      Total.ICMSTot.vBCST  := qryDoctoFiscalnValBaseICMSSub.Value;
      Total.ICMSTot.vST    := qryDoctoFiscalnValICMSSub.Value;
      Total.ICMSTot.vFrete := qryDoctoFiscalnValFrete.Value;
      Total.ICMSTot.vIPI   := qryDoctoFiscalnValIPI.Value;
      Total.ICMSTot.vOutro := qryDoctoFiscalnValOutras.Value;
      Total.ICMSTot.vProd  := qryDoctoFiscalnValProduto.Value;
      Total.ICMSTot.vNF    := qryDoctoFiscalnValTotal.Value;
      Total.ICMSTot.vSeg   := qryDoctoFiscalnValSeguro.Value;

      // desconto no total da nf
      if qryTerceirocFlgTipoDescontoDanfe.Value = 1 then
      begin
          qryDescontoTotal.Close;
          qryDescontoTotal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
          qryDescontoTotal.Open;
          Total.ICMSTot.vDesc  := qryDescontoTotalnValDescontoItens.Value;
          Total.ICMSTot.vProd  := qryDoctoFiscalnValProduto.Value + qryDescontoTotalnValDescontoItens.Value;
      end;

      {-- informa os dados da cobran�a --}
      iItem := 0 ;

      qryPrazoDoctoFiscal.Close;
      qryPrazoDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal ;
      qryPrazoDoctoFiscal.Open;

      qryPrazoDoctoFiscal.First ;

      for iAux := 1 to qryPrazoDoctoFiscal.recordCount do
      begin
          with Cobr.Dup.Add do
          begin
              nDup  := frmMenu.ZeroEsquerda(qryDoctoFiscaliNrDocto.AsString,5)  ;
              dVenc := qryPrazoDoctoFiscaldDtVenc.Value ;
              vDup  := qryPrazoDoctoFiscalnValPagto.Value ;
          end ;

          qryPrazoDoctoFiscal.Next;
      end ;


      qryPrazoDoctoFiscal.Close;

      {- gera as observa��es da nota --}
      qryMsgDoctoFiscal.Close;
      qryMsgDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal ;
      qryMsgDoctoFiscal.Open ;

      if not qryMsgDoctoFiscal.eof then
          while not qryMsgDoctoFiscal.Eof do
          begin

              if (InfAdic.infCpl <> '') then
                  infAdic.infCpl := infAdic.infCpl + ';' ;

              infAdic.infCpl := infAdic.infCpl + qryMsgDoctofiscalcMsg.Value ;

              qryMsgDoctoFiscal.Next;
          end ;

      qryMsgDoctoFiscal.Close;

  end;

  {--gera a nfe--}
  ACBrNFe1.NotasFiscais.GerarNFe;

  {-- assina eletronicamente a nfe --}
  frmMenu.mensagemUsuario('Assinando NFe...');
  ACBrNFe1.NotasFiscais.Assinar;

  {-- valida o xml da nota --}
  frmMenu.mensagemUsuario('Validando NFe...');
  ACBrNFe1.NotasFiscais.Valida;

  {-- envia para a srf a nfe --}
  frmMenu.mensagemUsuario('Transmitindo NFe...');
  ACBrNFe1.WebServices.Envia(0) ;

  {-- atualiza o documento fiscal com os dados da NFe --}
  qryDoctoFiscal.Edit;
  qryDoctoFiscalcXMLNfe.Value         := ACBrNFe1.NotasFiscais.Items[0].XML ;
  qryDoctoFiscalcNrProtocoloNFe.Value := ACBrNFe1.WebServices.Retorno.NFeRetorno.ProtNFe.Items[0].nProt;
  qryDoctoFiscalcNrReciboNFe.Value    := ACBrNFe1.WebServices.Retorno.NFeRetorno.nRec;
  qryDoctoFiscalcChaveNFe.Value       := ACBrNFe1.WebServices.Retorno.ChaveNFe;
  qryDoctoFiscalcCaminhoXML.Value     := ACBrNFe1.Configuracoes.Geral.PathSalvar ;
  qryDoctoFiscalcArquivoXML.Value     := ACBrNFe1.WebServices.Retorno.ChaveNFe +'-NFe.xml' ;
  qryDoctoFiscal.Post;
  qryDoctoFiscal.Close;

  frmMenu.mensagemUsuario('');

end;

procedure TfrmEmiteNFe.imprimirDANFe(nCdDoctoFiscal: integer);
begin
  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryDoctoFiscal.Open;

  // Limpas as Notas Fiscais que possam estar na mem�ria do componente
  frmMenu.mensagemUsuario('Carregando XML NFe...');
  ACBrNFe1.NotasFiscais.Clear;
  ACBrNFe1.NotasFiscais.LoadFromFile(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\Envio e Resposta\' + qryDoctoFiscalcArquivoXML.Value) ;

  ACBrNFeDANFEQR1.PathPDF := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\';

  {-- imprime o danfe --}
  frmMenu.mensagemUsuario('Imprimindo DANFe...');
  ACBrNFe1.NotasFiscais.Imprimir;
  frmMenu.mensagemUsuario('');

end;

procedure TfrmEmiteNFe.enviaEmailNFe(nCdDoctoFiscal : integer; cPara, cCopia : String);
var
  Assunto, Servidor, Porta, Usuario, Senha  : String;
  Mensagem, Copia : TStrings;
  UsarSSL : Boolean;
begin

  Mensagem := TStringList.Create;
  Copia    := TStringList.Create;
  
  {-- Prepara qrys para o envio do email --}

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryDoctoFiscal.Open;

  qryConfigAmbiente.Close ;
  qryConfigAmbiente.Parameters.ParamByName('cNmComputador').Value := frmMenu.cNomeComputador;
  qryConfigAmbiente.Open ;

  // Limpas as Notas Fiscais que possam estar na mem�ria do componente
  frmMenu.mensagemUsuario('Carregando XML NFe...');
  ACBrNFe1.NotasFiscais.Clear;

  ACBrNFe1.NotasFiscais.LoadFromFile(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\Envio e Resposta\' + qryDoctoFiscalcArquivoXML.Value) ;

  //Prepara mensagem
  
  frmMenu.mensagemUsuario('Preparando corpo do e-mail...');
  Assunto := 'Entrega da NFe n� ' + qryDoctoFiscaliNrDocto.AsString;

  if(frmMenu.LeParametro('VAREJO') = 'N') then
  begin
      Mensagem.Clear;
      Mensagem.Add(qryDoctoFiscalcNmTerceiro.AsString + #13#13);
      Mensagem.Add('Entrega da NF-e n� ' + qryDoctoFiscaliNrDocto.AsString + ' com a chave ' + qryDoctoFiscalcChaveNFe.Value +'.');
      Mensagem.Add(#13#13 + 'Veja o XML em anexo, ou fa�a a Consulta de autenticidade no portal nacional da NF-e <www.nfe.fazenda.gov.br/portal> ou no site da Sefaz.');
      Mensagem.Add(#13#13#13 + '<http://www.er2soft.com.br> '+ #13#13);
      Mensagem.Add('Conhe�a o Sistema de Gest�o Empresarial que integra sua empresa e seus parceiros de neg�cios com simplicidade.');
      Mensagem.Add(#13#13 +'O ER2SOFT ERP <http://www.er2soft.com.br > � um sistema f�cil de usar que atende totalmente as necessidades das empresas com baixo custo.');
      Mensagem.Add(#13#13 +'Ligue para a ER2Soft e tenha maiores informa��es (11) 2378-3200 ou acesse o site da ER2Soft.');
  end else
  begin
      Mensagem.Clear;
      Mensagem.Add(qryDoctoFiscalcNmTerceiro.AsString + #13#13);
      Mensagem.Add('Entrega da NF-e n� ' + qryDoctoFiscaliNrDocto.AsString + ' com a chave ' + qryDoctoFiscalcChaveNFe.Value +'.');
      Mensagem.Add(#13#13 + 'Veja o XML em anexo, ou fa�a a Consulta de autenticidade no portal nacional da NF-e <www.nfe.fazenda.gov.br/portal> ou no site da Sefaz.');
      Mensagem.Add(#13#13#13 + '<http://www.er2soft.com.br> '+ #13#13);
      Mensagem.Add('Conhe�a o Sistema de Automa��o Comercial que integra suas lojas e seus parceiros de neg�cios com simplicidade.');
      Mensagem.Add(#13#13 +'O ER2SOFT ERP <http://www.er2soft.com.br > � um sistema f�cil de usar que atende totalmente as necessidades dos lojistas com baixo custo.');
      Mensagem.Add(#13#13 +'Ligue para a ER2Soft e tenha maiores informa��es (11) 2378-3200 ou acesse o site da ER2Soft.');
  end;

  frmMenu.mensagemUsuario('Enviando e-mail...');

  {-- Pega as Configura��es de ambiente --}

  Servidor := qryConfigAmbientecServidorSMTP.Value;
  Porta    := qryConfigAmbientenPortaSMTP.AsString;
  Usuario  := qryConfigAmbientecEmail.Value;

  if(qryConfigAmbientecFlgUsaSSL.Value = 0) then
      UsarSSL := False else UsarSSL := True;

  Senha    := qryConfigAmbientecSenhaEmail.Value;

  if (Trim(cCopia) <> '') then
  begin
      Copia.Clear;
      Copia.Add(cCopia);

      ACBrNFe1.NotasFiscais.Items[0].EnviarEmail(  Servidor
                                                 , Porta
                                                 , Usuario
                                                 , Senha
                                                 , Usuario //de
                                                 , cPara
                                                 , Assunto
                                                 , Mensagem
                                                 , UsarSSL  // Usa SSL
                                                 , false //Enviar PDF junto
                                                 , Copia //com copia
                                                 );
  end else
  begin
      ACBrNFe1.NotasFiscais.Items[0].EnviarEmail(  Servidor
                                                 , Porta
                                                 , Usuario
                                                 , Senha
                                                 , Usuario //de
                                                 , cPara
                                                 , Assunto
                                                 , Mensagem
                                                 , UsarSSL //Usa SSL
                                                 , false //Enviar PDF junto
                                                 );
  end;

  frmMenu.mensagemUsuario('');

end;

procedure TfrmEmiteNFe.preparaNFe;
begin

    // seta as propriedades da NFe

    qryConfigAmbiente.Close;
    qryConfigAmbiente.Parameters.ParamByName('cNmComputador').Value := frmMenu.cNomeComputador;
    qryConfigAmbiente.Open;

    frmMenu.mensagemUsuario('Preparando NFe...');

    if (qryConfigAmbiente.Eof) then
    begin
        frmMenu.MensagemErro('Configura��o de ambiente n�o encontrada para emiss�o da NFe') ;
        abort ;
    end ;

    if (qryConfigAmbientecFlgEmiteNFe.Value = 0) then
    begin
        frmMenu.ShowMessage('Esta esta��o de trabalho n�o est� configurada para emiss�o de NFe.');
        abort ;
    end ;

    if (qryConfigAmbientecNrSerieCertificadoNFe.Value = '') then
    begin
        frmMenu.MensagemErro('N�mero de s�rie do certificado digital n�o informado na configura��o do ambiente.') ;
        abort ;
    end ;

    // verifica se os diret�rios de log existem, se n�o existir, cria.
    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value) then
    begin
        frmMenu.MensagemErro('Caminho para grava��o dos arquivos da Nfe n�o encontrado.' +#13#13 + 'Caminho : ' + qryConfigAmbientecPathArqNfe.Value);
        abort ;
    end ;
    
    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\Envio e Resposta\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\Envio e Resposta\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFe\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFe\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeCan\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeCan\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeDPEC\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeDPEC\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeInu\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeInu\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\') then
        CreateDir(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\') ;

    if not DirectoryExists(qryConfigAmbientecPathArqNfe.Value+'NFe\Schemas\') then
    begin
        frmMenu.MensagemErro('Caminho de Schemas da NFe n�o encontrado.' +#13#13 + 'Caminho: ' + qryConfigAmbientecPathArqNfe.Value+'NFe\Schemas\');
        abort ;
    end ;


    ACBrNFe1.Configuracoes.Geral.PathSalvar  := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\Envio e Resposta\';
    ACBrNFe1.Configuracoes.Geral.PathSchemas := qryConfigAmbientecPathArqNfe.Value+'NFe\Schemas\';
    ACBrNFe1.Configuracoes.Geral.Salvar      := True ;

    ACBrNFe1.Configuracoes.Arquivos.PathNFe  := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFe\';
    ACBrNFe1.Configuracoes.Arquivos.PathCan  := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeCan\';
    ACBrNFe1.Configuracoes.Arquivos.PathDPEC := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeDPEC\';
    ACBrNFe1.Configuracoes.Arquivos.PathInu  := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\NFeInu\';
    ACBrNFe1.Configuracoes.Arquivos.Salvar   := True ;

    ACBrNFe1.Configuracoes.WebServices.UF    := qryConfigAmbientecUFEmissaoNFe.Value ;

    if (qryConfigAmbientenCdTipoAmbienteNFe.Value = 0) then
        ACBrNFe1.Configuracoes.WebServices.Ambiente := taProducao
    else ACBrNFe1.Configuracoes.WebServices.Ambiente := taHomologacao ;

    ACBrNFe1.Configuracoes.WebServices.Visualizar   := false;
    ACBrNFe1.Configuracoes.Certificados.NumeroSerie := qryConfigAmbientecNrSerieCertificadoNFe.Value ;

    // configura��es do DANFE
    
    ACBrNFe1.DANFE.Logo     := qryConfigAmbientecPathLogoNFe.Value ;
    ACBrNFeDANFEQR1.PathPDF := qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\PDF\';

end;

procedure TfrmEmiteNFe.ReimprimirDANFe(nCdDoctoFiscal: integer);
begin

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := nCdDoctoFiscal;
  qryDoctoFiscal.Open;

  preparaNFe();

  // Limpas as Notas Fiscais que possam estar na mem�ria do componente
  frmMenu.mensagemUsuario('Carregando XML NFe...');
  ACBrNFe1.NotasFiscais.Clear;
  ACBrNFe1.NotasFiscais.LoadFromFile(qryConfigAmbientecPathArqNfe.Value+'EMP' + frmMenu.ZeroEsquerda(intToStr(frmMenu.nCdEmpresaAtiva),3) +'\Envio e Resposta\' + qryDoctoFiscalcArquivoXML.Value) ;

  {-- imprime o danfe --}
  ACBrNFe1.NotasFiscais.Imprimir;

  frmMenu.mensagemUsuario('');

end;

end.
