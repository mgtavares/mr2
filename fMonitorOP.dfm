inherited frmMonitorOP: TfrmMonitorOP
  Left = -8
  Top = -8
  Width = 1152
  Height = 850
  Caption = 'Monitor Ordem Produ'#231#227'o'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1136
    Height = 785
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 785
    ActivePage = cxTabSheet7
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 781
    ClientRectLeft = 4
    ClientRectRight = 1132
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Ordens Planejadas'
      ImageIndex = 0
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 1128
        Height = 757
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView1: TcxGridDBTableView
          OnDblClick = cxGridDBTableView1DblClick
          DataController.DataSource = dsOPPlanejada
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.Footer = True
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView1cFlgAtraso: TcxGridDBColumn
            Caption = 'Atraso?'
            DataBinding.FieldName = 'cFlgAtraso'
            Width = 51
          end
          object cxGridDBTableView1nCdOrdemProducao: TcxGridDBColumn
            Caption = 'C'#243'd. OP'
            DataBinding.FieldName = 'nCdOrdemProducao'
          end
          object cxGridDBTableView1cNumeroOP: TcxGridDBColumn
            Caption = 'N'#250'mero OP'
            DataBinding.FieldName = 'cNumeroOP'
            Width = 90
          end
          object cxGridDBTableView1cNmTabTipoOrigemOP: TcxGridDBColumn
            Caption = 'Origem'
            DataBinding.FieldName = 'cNmTabTipoOrigemOP'
            Width = 65
          end
          object cxGridDBTableView1dDtAbertura: TcxGridDBColumn
            Caption = 'Dt. Abertura'
            DataBinding.FieldName = 'dDtAbertura'
            Width = 115
          end
          object cxGridDBTableView1dDtPrevConclusao: TcxGridDBColumn
            Caption = 'Prev. Conclus'#227'o'
            DataBinding.FieldName = 'dDtPrevConclusao'
            Width = 122
          end
          object cxGridDBTableView1cNmTipoOP: TcxGridDBColumn
            Caption = 'Tipo OP'
            DataBinding.FieldName = 'cNmTipoOP'
            Visible = False
            GroupIndex = 0
            Width = 84
          end
          object cxGridDBTableView1nCdProduto: TcxGridDBColumn
            Caption = 'C'#243'd. Produto'
            DataBinding.FieldName = 'nCdProduto'
          end
          object cxGridDBTableView1cReferencia: TcxGridDBColumn
            Caption = 'Refer'#234'ncia Produto'
            DataBinding.FieldName = 'cReferencia'
          end
          object cxGridDBTableView1cNmProduto: TcxGridDBColumn
            Caption = 'Produto'
            DataBinding.FieldName = 'cNmProduto'
            Width = 187
          end
          object cxGridDBTableView1nQtdePlanejada: TcxGridDBColumn
            Caption = 'Quantidade'
            DataBinding.FieldName = 'nQtdePlanejada'
            Width = 87
          end
          object cxGridDBTableView1nCdPedido: TcxGridDBColumn
            Caption = 'Pedido Venda'
            DataBinding.FieldName = 'nCdPedido'
            Width = 96
          end
          object cxGridDBTableView1cNmTerceiro: TcxGridDBColumn
            Caption = 'Terceiro Entrega'
            DataBinding.FieldName = 'cNmTerceiro'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Ordens Autorizadas'
      ImageIndex = 1
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1128
        Height = 757
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        PopupMenu = PopupMenu2
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView2: TcxGridDBTableView
          OnDblClick = cxGridDBTableView2DblClick
          DataController.DataSource = dsOPAutorizada
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.Footer = True
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView2cFlgAtraso: TcxGridDBColumn
            Caption = 'Atraso?'
            DataBinding.FieldName = 'cFlgAtraso'
            Width = 51
          end
          object cxGridDBColumn1: TcxGridDBColumn
            Caption = 'C'#243'd. OP'
            DataBinding.FieldName = 'nCdOrdemProducao'
          end
          object cxGridDBColumn2: TcxGridDBColumn
            Caption = 'N'#250'mero OP'
            DataBinding.FieldName = 'cNumeroOP'
            Width = 90
          end
          object cxGridDBColumn3: TcxGridDBColumn
            Caption = 'Origem'
            DataBinding.FieldName = 'cNmTabTipoOrigemOP'
            Width = 65
          end
          object cxGridDBColumn4: TcxGridDBColumn
            Caption = 'Dt. Abertura'
            DataBinding.FieldName = 'dDtAbertura'
            Width = 115
          end
          object cxGridDBTableView2dDtInicioPrevisto: TcxGridDBColumn
            Caption = 'Previs'#227'o de In'#237'cio'
            DataBinding.FieldName = 'dDtInicioPrevisto'
          end
          object cxGridDBColumn5: TcxGridDBColumn
            Caption = 'Prev. Conclus'#227'o'
            DataBinding.FieldName = 'dDtPrevConclusao'
            Width = 122
          end
          object cxGridDBColumn6: TcxGridDBColumn
            Caption = 'Tipo OP'
            DataBinding.FieldName = 'cNmTipoOP'
            Visible = False
            GroupIndex = 0
            Width = 84
          end
          object cxGridDBColumn7: TcxGridDBColumn
            Caption = 'C'#243'd. Produto'
            DataBinding.FieldName = 'nCdProduto'
          end
          object cxGridDBColumn8: TcxGridDBColumn
            Caption = 'Refer'#234'ncia Produto'
            DataBinding.FieldName = 'cReferencia'
          end
          object cxGridDBColumn9: TcxGridDBColumn
            Caption = 'Produto'
            DataBinding.FieldName = 'cNmProduto'
            Width = 187
          end
          object cxGridDBColumn10: TcxGridDBColumn
            Caption = 'Quantidade'
            DataBinding.FieldName = 'nQtdePlanejada'
            Width = 87
          end
          object cxGridDBColumn11: TcxGridDBColumn
            Caption = 'Pedido Venda'
            DataBinding.FieldName = 'nCdPedido'
            Width = 96
          end
          object cxGridDBColumn12: TcxGridDBColumn
            Caption = 'Terceiro Entrega'
            DataBinding.FieldName = 'cNmTerceiro'
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Ordens em Processo'
      ImageIndex = 2
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 1128
        Height = 528
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        PopupMenu = PopupMenu3
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView3: TcxGridDBTableView
          OnDblClick = cxGridDBTableView2DblClick
          DataController.DataSource = dsOPProcesso
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView3cFlgAtraso: TcxGridDBColumn
            Caption = 'Atraso?'
            DataBinding.FieldName = 'cFlgAtraso'
            Width = 55
          end
          object cxGridDBColumn13: TcxGridDBColumn
            Caption = 'C'#243'd. OP'
            DataBinding.FieldName = 'nCdOrdemProducao'
            Width = 67
          end
          object cxGridDBColumn14: TcxGridDBColumn
            Caption = 'N'#250'mero OP'
            DataBinding.FieldName = 'cNumeroOP'
            Width = 79
          end
          object cxGridDBColumn24: TcxGridDBColumn
            Caption = 'Pedido Venda'
            DataBinding.FieldName = 'nCdPedido'
            Width = 96
          end
          object cxGridDBColumn25: TcxGridDBColumn
            Caption = 'Terceiro Entrega'
            DataBinding.FieldName = 'cNmTerceiro'
            Width = 72
          end
          object cxGridDBColumn15: TcxGridDBColumn
            Caption = 'Origem'
            DataBinding.FieldName = 'cNmTabTipoOrigemOP'
            Width = 65
          end
          object cxGridDBColumn16: TcxGridDBColumn
            Caption = 'Dt. Abertura'
            DataBinding.FieldName = 'dDtAbertura'
            Width = 100
          end
          object cxGridDBColumn17: TcxGridDBColumn
            Caption = 'Previs'#227'o de In'#237'cio'
            DataBinding.FieldName = 'dDtInicioPrevisto'
            Width = 72
          end
          object cxGridDBColumn18: TcxGridDBColumn
            Caption = 'Prev. Conclus'#227'o'
            DataBinding.FieldName = 'dDtPrevConclusao'
            Width = 113
          end
          object cxGridDBColumn19: TcxGridDBColumn
            Caption = 'Tipo OP'
            DataBinding.FieldName = 'cNmTipoOP'
            Visible = False
            GroupIndex = 0
            Width = 84
          end
          object cxGridDBColumn20: TcxGridDBColumn
            Caption = 'C'#243'd. Produto'
            DataBinding.FieldName = 'nCdProduto'
          end
          object cxGridDBColumn21: TcxGridDBColumn
            Caption = 'Refer'#234'ncia Produto'
            DataBinding.FieldName = 'cReferencia'
            Width = 85
          end
          object cxGridDBColumn22: TcxGridDBColumn
            Caption = 'Produto'
            DataBinding.FieldName = 'cNmProduto'
            Width = 116
          end
          object cxGridDBColumn23: TcxGridDBColumn
            Caption = 'Qt. Planej.'
            DataBinding.FieldName = 'nQtdePlanejada'
            HeaderAlignmentHorz = taRightJustify
            Width = 87
          end
          object cxGridDBTableView3nQtdeConcluida: TcxGridDBColumn
            Caption = 'Qt. Conclu'#237'da'
            DataBinding.FieldName = 'nQtdeConcluida'
            HeaderAlignmentHorz = taRightJustify
          end
          object cxGridDBTableView3nSaldoProduzir: TcxGridDBColumn
            Caption = 'Saldo Prod.'
            DataBinding.FieldName = 'nSaldoProduzir'
            HeaderAlignmentHorz = taRightJustify
            Width = 90
          end
        end
        object cxGridLevel3: TcxGridLevel
          GridView = cxGridDBTableView3
        end
      end
      object cxPageControl2: TcxPageControl
        Left = 0
        Top = 528
        Width = 1128
        Height = 229
        ActivePage = cxTabSheet6
        Align = alBottom
        LookAndFeel.NativeStyle = True
        TabOrder = 1
        ClientRectBottom = 225
        ClientRectLeft = 4
        ClientRectRight = 1124
        ClientRectTop = 24
        object cxTabSheet6: TcxTabSheet
          Caption = 'Registros de Conclus'#227'o'
          ImageIndex = 2
          object cxGrid4: TcxGrid
            Left = 0
            Top = 0
            Width = 1120
            Height = 201
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Consolas'
            Font.Style = []
            ParentFont = False
            PopupMenu = PopupMenu3
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView4: TcxGridDBTableView
              DataController.DataSource = dsRegistroEntrada
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  FieldName = 'nValTotal'
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsCustomize.ColumnGrouping = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsSelection.CellMultiSelect = True
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              Styles.Content = frmMenu.FonteSomenteLeitura
              object cxGridDBTableView4dDtEntrada: TcxGridDBColumn
                Caption = 'Data Entrada'
                DataBinding.FieldName = 'dDtEntrada'
              end
              object cxGridDBTableView4cNmUsuario: TcxGridDBColumn
                Caption = 'Usu'#225'rio'
                DataBinding.FieldName = 'cNmUsuario'
                Width = 131
              end
              object cxGridDBTableView4cDocumento: TcxGridDBColumn
                Caption = 'Nr. Doc.'
                DataBinding.FieldName = 'cDocumento'
                Width = 108
              end
              object cxGridDBTableView4cNmTabTipoEntradaProducao: TcxGridDBColumn
                Caption = 'Tipo de Registro'
                DataBinding.FieldName = 'cNmTabTipoEntradaProducao'
                Width = 129
              end
              object cxGridDBTableView4nQtdeEntrada: TcxGridDBColumn
                Caption = 'Quantidade'
                DataBinding.FieldName = 'nQtdeEntrada'
                Width = 87
              end
              object cxGridDBTableView4cNmLocalEstoque: TcxGridDBColumn
                Caption = 'Local de Estoque'
                DataBinding.FieldName = 'cNmLocalEstoque'
                Width = 186
              end
              object cxGridDBTableView4cOBSLivro: TcxGridDBColumn
                Caption = 'Observa'#231#227'o'
                DataBinding.FieldName = 'cOBSLivro'
                Width = 336
              end
            end
            object cxGridLevel4: TcxGridLevel
              GridView = cxGridDBTableView4
            end
          end
        end
        object cxTabSheet4: TcxTabSheet
          Caption = 'Resumo da OP'
          ImageIndex = 0
          object Label1: TLabel
            Tag = 1
            Left = 8
            Top = 24
            Width = 84
            Height = 13
            Caption = 'Quant. Planejada'
            FocusControl = DBEdit1
          end
          object Label2: TLabel
            Tag = 1
            Left = 9
            Top = 52
            Width = 83
            Height = 13
            Caption = 'Quant. Conclu'#237'da'
            FocusControl = DBEdit2
          end
          object Label3: TLabel
            Tag = 1
            Left = 5
            Top = 81
            Width = 87
            Height = 13
            Caption = 'Quant. Cancelada'
            FocusControl = DBEdit3
          end
          object Label4: TLabel
            Tag = 1
            Left = 2
            Top = 110
            Width = 90
            Height = 13
            Caption = 'Quant. Retrabalho'
            FocusControl = DBEdit4
          end
          object Label5: TLabel
            Tag = 1
            Left = 20
            Top = 139
            Width = 72
            Height = 13
            Caption = 'Quant. Refugo'
            FocusControl = DBEdit5
          end
          object Label6: TLabel
            Tag = 1
            Left = 15
            Top = 168
            Width = 77
            Height = 13
            Caption = 'Saldo a Produzir'
            FocusControl = DBEdit6
          end
          object DBEdit1: TDBEdit
            Tag = 1
            Left = 96
            Top = 16
            Width = 81
            Height = 21
            DataField = 'nQtdePlanejada'
            DataSource = dsOPProcesso
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Tag = 1
            Left = 96
            Top = 44
            Width = 81
            Height = 21
            DataField = 'nQtdeConcluida'
            DataSource = dsOPProcesso
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Tag = 1
            Left = 96
            Top = 73
            Width = 81
            Height = 21
            DataField = 'nQtdeCancelada'
            DataSource = dsOPProcesso
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Tag = 1
            Left = 96
            Top = 102
            Width = 81
            Height = 21
            DataField = 'nQtdeRetrabalho'
            DataSource = dsOPProcesso
            TabOrder = 3
          end
          object DBEdit5: TDBEdit
            Tag = 1
            Left = 96
            Top = 131
            Width = 81
            Height = 21
            DataField = 'nQtdeRefugo'
            DataSource = dsOPProcesso
            TabOrder = 4
          end
          object DBEdit6: TDBEdit
            Tag = 1
            Left = 96
            Top = 160
            Width = 81
            Height = 21
            DataField = 'nSaldoProduzir'
            DataSource = dsOPProcesso
            TabOrder = 5
          end
        end
        object cxTabSheet5: TcxTabSheet
          Caption = 'Registro de Apontamentos'
          ImageIndex = 1
        end
      end
    end
    object cxTabSheet7: TcxTabSheet
      Caption = 'Todas OPs Abertas'
      ImageIndex = 3
      object cxGrid5: TcxGrid
        Left = 0
        Top = 0
        Width = 1128
        Height = 757
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        PopupMenu = PopupMenu4
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView5: TcxGridDBTableView
          OnDblClick = cxGridDBTableView2DblClick
          DataController.DataSource = dsOPAberta
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView5cNmTabTipoStatusOP: TcxGridDBColumn
            Caption = 'Situa'#231#227'o'
            DataBinding.FieldName = 'cNmTabTipoStatusOP'
            Visible = False
            GroupIndex = 0
            Width = 97
          end
          object cxGridDBColumn26: TcxGridDBColumn
            Caption = 'Atraso?'
            DataBinding.FieldName = 'cFlgAtraso'
            Width = 65
          end
          object cxGridDBColumn27: TcxGridDBColumn
            Caption = 'C'#243'd. OP'
            DataBinding.FieldName = 'nCdOrdemProducao'
            Width = 67
          end
          object cxGridDBColumn28: TcxGridDBColumn
            Caption = 'N'#250'mero OP'
            DataBinding.FieldName = 'cNumeroOP'
            Width = 79
          end
          object cxGridDBColumn29: TcxGridDBColumn
            Caption = 'Pedido Venda'
            DataBinding.FieldName = 'nCdPedido'
            Width = 96
          end
          object cxGridDBColumn30: TcxGridDBColumn
            Caption = 'Terceiro Entrega'
            DataBinding.FieldName = 'cNmTerceiro'
            Width = 72
          end
          object cxGridDBColumn31: TcxGridDBColumn
            Caption = 'Origem'
            DataBinding.FieldName = 'cNmTabTipoOrigemOP'
            Width = 65
          end
          object cxGridDBColumn32: TcxGridDBColumn
            Caption = 'Dt. Abertura'
            DataBinding.FieldName = 'dDtAbertura'
            Width = 100
          end
          object cxGridDBColumn33: TcxGridDBColumn
            Caption = 'Previs'#227'o de In'#237'cio'
            DataBinding.FieldName = 'dDtInicioPrevisto'
            Width = 72
          end
          object cxGridDBColumn34: TcxGridDBColumn
            Caption = 'Prev. Conclus'#227'o'
            DataBinding.FieldName = 'dDtPrevConclusao'
            Width = 113
          end
          object cxGridDBColumn35: TcxGridDBColumn
            Caption = 'Tipo OP'
            DataBinding.FieldName = 'cNmTipoOP'
            Visible = False
            GroupIndex = 1
            Width = 84
          end
          object cxGridDBColumn36: TcxGridDBColumn
            Caption = 'C'#243'd. Produto'
            DataBinding.FieldName = 'nCdProduto'
          end
          object cxGridDBColumn37: TcxGridDBColumn
            Caption = 'Refer'#234'ncia Produto'
            DataBinding.FieldName = 'cReferencia'
            Width = 85
          end
          object cxGridDBColumn38: TcxGridDBColumn
            Caption = 'Produto'
            DataBinding.FieldName = 'cNmProduto'
            Width = 116
          end
          object cxGridDBColumn39: TcxGridDBColumn
            Caption = 'Qt. Planej.'
            DataBinding.FieldName = 'nQtdePlanejada'
            HeaderAlignmentHorz = taRightJustify
            Width = 87
          end
          object cxGridDBColumn40: TcxGridDBColumn
            Caption = 'Qt. Conclu'#237'da'
            DataBinding.FieldName = 'nQtdeConcluida'
            HeaderAlignmentHorz = taRightJustify
          end
          object cxGridDBColumn41: TcxGridDBColumn
            Caption = 'Saldo Prod.'
            DataBinding.FieldName = 'nSaldoProduzir'
            HeaderAlignmentHorz = taRightJustify
            Width = 90
          end
        end
        object cxGridLevel5: TcxGridLevel
          GridView = cxGridDBTableView5
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C01010A000E00040010001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EBFEFB00F0FC
      FC000000000000000000000000000000000000000000000000008C889300F1F1
      F700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EBFEFB00F0FC
      FC000000000000000000000000000000000000000000000000008C889300F1F1
      F700000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFB00F8FAF400F9FAF600F7F8
      F60000000100110E17000000080000000D0000000F000E051F000B031A00140F
      1E000100080016131C0000000400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000007000300A19099008375
      7900E9E9E30000000000FDFFF800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000ECEEE800FAFCF600FDFEFA00FEFE
      FE0017161A00DBD7E200DED8E900F1E9FF00EFE6FF00D1C6E600D1C7E500D0CA
      DD00D4D0DC00CDC9D400100D1600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000023120F002F080600AB86
      8200B8ACA000DADBCB0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000012140E001C1D1900000100000000
      010004020800EFEBF700F7F1FF00F5ECFF00E0D5F500FDF1FF00FFF4FF00FFF8
      FF00F8F4FF00E6E2ED000B081100000000000000000000000000000000000000
      00000000000000000000E7FEE80000000000FFFFF1002A06000060190F004300
      0000926B5C00C3B1A00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000020300008C8F8D00999B9B009EA0
      A10000000600FBFBFF0033314400211D3600302A4900241D40002B2445000E09
      24003D384D00CCC9D90003010D000000000000000000FFFFF400A09B8C008682
      6F00848771008F997B0089967600939270009F805F0045030000E77B63009121
      0D0050030000BD8A7A00A27F7100FFFFF1000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000100EBF0EF00E7ECED00DDE4
      E70000070E00EFF8FF00E7EEFD001C233700E7ECFF00EDF1FF0020224400ECE9
      FF00F9F2FF00CBC7E40000000E0000000000000000002C170F0029000000460F
      000041080000390200003B0D00003D0800004E050000740F0000A31B0000F469
      4E008B1F0E004C000000B2746A00B08A7E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000E111600E0E4E9000F151C000D14
      1D0000000A00E5EDFE00ECF4FF000F183300EDF7FF00D9E2FF00EFF4FF001613
      4000F4F0FF00D4CEF1000B08210000000000000000004D02000083030400BA16
      1700C4100B00C00C0000AD0F0000B01E0000B52C0600960A0000B71F0000B51D
      0000FF8779008A1A140058000000D29690000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000D0F1700C0C2CD00F8FBFF001619
      2E0003082100F1F4FF00191C420021254E00EDF2FF000E124200E5E7FF00ECE9
      FF0019153E00D6D1F1000A081E0000000000000000004F0909008F0D0E00FF6C
      6E00D9141200FF746B00D5240F00FF6E5000A20E0000FF8A6200BB1F0000FF72
      5100980E0200E56E6C00842724003A0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000800EDEEFF00D1D2EC000304
      260000001C00EDEDFF00ECEDFF0026275F00DDDEFF00EAEBFF0012134B001915
      4600EEEBFF00DCD7F60000001100000000000000000005030000B6837900D27D
      7300E37A6C00E0705C00DB795B00EA896700EB866000FC855E00FF876300A412
      0000FF8C80007E1B170045000000FFF1EE000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000001011300E6E4FF000F0C33001310
      3E0018144800CEC9FF00E6E2FF00E3E0FF00ECE8FF00E3E0FF00EAE9FF00EAE6
      FF00EDEBFF00D1CDEA0006061800000000000000000000130600140700003C18
      08003F0D0000431400002B0500003813000040090000620E0000F2755600FF86
      6F005D000000621E1700FFEDE700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000006071C00E5E4FF00D0CDFA000B05
      3C00000031001F185B001F196000221B6400140F5A001D1863001C1859002C29
      5700252444000F0C250001011100000000000000000000000000000000000000
      0000FFFFEE0000000000FFFFEA00000000000000000032050000FFA48C006E0D
      0000410D0000FFEEE20000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000001700E0DEFF00DFDCFF00DDD6
      FF0000003400B3ACEF00272267000E094E00161358000A074C0023215C000000
      2400464763000F0E220002040F0000000000FDFFFC000000000000000000FFFC
      F400FFFFF500000000000000000000000000000000002F1D06009D6151004F12
      0400FFF1E5000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000009092100242044001A1443000F09
      400000002F00000036000000320005013C000000280000002A0000002C000607
      29000000120000000B0002050D0000000000FDFEFC00FFFDFC00000000000000
      00000000000000000000000000000000000000000000120C0500220E09000000
      000000000000FCFBF70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000F001A17370028244D002621
      4E00211E4C001E1A4B001D1B49001D1C460015173F00070B2E0000012000EDF1
      FF00EFF3FF00F3F4FF00F4F8FD0000000000FEFEFE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001615290008051F00000017000000
      1E0000001C0000001C0000001C0000041D0000041C000002150000001000F1F5
      FF00F1F6FF00F5F7FF00F5FAFD00000000000000000000000000FEFEFE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFBFC00FFFAF600FFFF
      F500FFFFF400FFFFF400FFFDF100FFFDF100FFFFF700FFFAF400FFF8F000FFF6
      E800FFFDED00FFF5EA00FFF6F000FFFEFC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C8C6F009C8C
      6F009C8C6F009C8C6F009C8C6F009C8C6F009C8C6F009C8C6F009C8C6F009C8C
      6F009C8C6F009C8C6F000000000000000000FFFEFF00FFFBF700F1E0D700EBD1
      C000F9DCC700EAC9B500F4D1BD00FDD8C400E7C3B100F9D5C500FDD9C700FFE2
      CE00FFE1CB00E3C6B700F0D7CD00FFFAF3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFB00FFFAF100DFC8B9009B77
      5F00B0886C00A1755800A97A5E00B9896D00B6856B00B7866C00AD7C6200A778
      5D00A0725A00B6928000E8CBBD00FFFBF3000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000080808000000000000000
      00000000000000000000000000000000000000000000000000004E7EA6004E7E
      A6004E7EA6004E7EA6004E7EA600000000004E7EA6004E7EA6004E7EA6004E7E
      A6004E7EA600000000000000000000000000FFFFF700FFFAEB00E8CBB6006236
      170057270500743E1B006E3410006327030074371500662907006A2F0F00743D
      1E0058240600A97E6900F6D4C400FFFDF2000000000000000000000000000000
      0000999999009999990099999900999999009999990000000000000000000000
      0000808080008080800000000000000000000000000000000000000000000000
      00008080800000000000DEBED400000000000000000000000000808080000000
      0000000000000000000000000000000000000000000000000000D6FFFF009EF0
      FF009EF0FF009EF0FF004E7EA60000000000D6FFFF009EF0FF009EF0FF009EF0
      FF009EF0FF00000000000000000000000000FFF9F000FFFAEA00FDDDC6005524
      040067310E00C2876000AD704800692B0300692B050075371300B67A5600B175
      510070381500B4876C00FFDDC900FFF7E9000000000000000000000000009999
      9900000000000000000000000000000000009999990000000000000000008080
      8000808080000000000000000000000000000000000000000000000000000000
      000000000000DEBED400DEBED40000000000DEBED40000000000000000000000
      0000000000000000000000000000000000000000000000000000D6FFFF009EF0
      FF009EF0FF009EF0FF004E7EA60000000000D6FFFF009EF0FF009EF0FF009EF0
      FF009EF0FF00000000000000000000000000FFFFFB00FFFFF400F4D6C5005C2A
      0C006F361600C88C6800FFCCA800CD936F0059250100A6735200E6B28E00BA7D
      550065290000AA7C5D00EAC8B000FFFFF0000000000000000000000000000000
      0000E6E6E600E6E6E600E6E6E600E6E6E6000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000999999009999
      9900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D6FFFF009EF0
      FF009EF0FF009EF0FF004E7EA60000000000D6FFFF009EF0FF009EF0FF009EF0
      FF009EF0FF00000000000000000000000000FFFFFC00FFFFF400F6D5C5005E2A
      0C00632906007A3C1800C4896200FCC39C00C6947000DEB08E00B88662008547
      1E0075360A00B1846300FAD8C000FFFDEB000000000000000000E6E6E600E6E6
      E600E6E6E6000000000000000000E6E6E600E6E6E600E6E6E600000000000000
      000000000000000000000000000000000000000000000000000000000000E6E6
      E6000000000000000000DEBED40000000000DEBED400DEBED400000000000000
      0000000000000000000000000000000000000000000000000000D6FFFF00D6FF
      FF00D6FFFF00D6FFFF00D6FFFF0000000000D6FFFF00D6FFFF00D6FFFF00D6FF
      FF00D6FFFF00000000000000000000000000FFFFF900FFFFF100F9D6C200612B
      0A008A4F280071330B006C2C0300CA8C6300E2AB8400B4805B00622E06006E30
      070084441B00A87A5B00FFDFC700FFFDED000000000000000000E6E6E6000000
      000000000000D9D9D900D9D9D9000000000000000000E6E6E600000000009999
      990099999900000000000000000000000000000000000000000000000000E6E6
      E60080808000000000000000000000000000DEBED40000000000808080004E7E
      A6004E7EA6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFF500FFFFEF00FFD7BE00632C
      0700622400007D3C0F00C9865900E6A57800C68B5E00FBC29500D2986E007435
      0F0056170000B98B7300F0CCBA00FFFBEC000000000099999900D9D9D900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E60000000000E6E6E600E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000080
      0000E6E6E600E6E6E6000000000099999900000000004E7EA600000000004E7E
      A6004E7EA600000000004E7EA600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFF500FFFFEF00FDD7BF00612B
      080082451D00C3845800FFC39700B7794B006F340700CB946900FFC89F00B87B
      590060240600AF826D00F0CEBE00FFFFF4000000000099999900E6E6E600E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E60000000000E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000080
      0000E6E6E600E6E6E600E6E6E600999999000000000000000000000000004E7E
      A6004E7EA6000000000000000000000000000000000000000000000000000000
      0000000000004E7EA6004E7EA6004E7EA6004E7EA6004E7EA600000000009C8C
      6F0000000000000000000000000000000000FFFFF700FFFFF100F7D6C2005A2B
      0B006B351200B47B5400D1966E007A3F1700632D04006E3A1200B9845F00BC83
      64006E361D009E736200F8D6C900FFFEF700000000009999990099999900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600D9D9D900E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000080
      000000800000E6E6E600E6E6E60099999900000000004E7EA600000000004E7E
      A6004E7EA600000000004E7EA600000000000000000000000000000000000000
      000000000000D6FFFF009EF0FF009EF0FF009EF0FF004E7EA600000000009C8C
      6F0000000000000000000000000000000000FFFFFB00FFFFF400F0D6C600532A
      11005021050051200000673413004613000074462400491C0000552607005C29
      0F005D2A1600A47D6F00FCDED300FFEFE900000000009999990099999900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000080
      0000E6E6E600E6E6E600E6E6E6009999990000000000000000004E7EA6000000
      0000000000004E7EA60000000000000000000000000000000000000000000000
      000000000000D6FFFF009EF0FF009EF0FF009EF0FF004E7EA600000000009C8C
      6F0000000000000000000000000000000000FFFEFE00FFFBF800DCC7BF009A79
      6900A3806C00A7816900A9806900A9816800A7826800A5826800A7816900A378
      6500A87B6D00B3908600DFC3BC00FFF9F4000000000000000000999999009999
      9900E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600000000009999
      9900000000000000000000000000000000000000000000000000000000000080
      0000E6E6E600E6E6E600E6E6E600999999000000000000000000000000004E7E
      A6004E7EA6000000000000000000000000000000000000000000000000000000
      000000000000D6FFFF009EF0FF009EF0FF009EF0FF004E7EA600000000009C8C
      6F0000000000000000000000000000000000FFFEFF00FFFEFE00F5E6E300E3CD
      C200EAD0C200EBD1C100EDD0C100EDD1C000EDD1C000EBD1C100EDD0C100F3D0
      C300F4D0C600E9CDC600F7E1DC00FFFBFA000000000000000000999999009999
      99009999990099999900E6E6E600E6E6E600E6E6E600E6E6E600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D6FFFF00D6FFFF00D6FFFF00D6FFFF00D6FFFF00000000009C8C
      6F0000000000000000000000000000000000FAFAFF00FFFDFF00FFF8F800FFFF
      F900FFFFF800FFFFF700FFFFF700FFFFF700FFFFF700FFFFF800FFFFF700FFFE
      F700FFFEF700FFFAF500FFF8F700FFFDFD000000000000000000000000000000
      0000999999009999990099999900999999000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F7FAFF00FDFCFE00FEFCFC00FFFF
      FC00FFFFFC00FFFFFB00FFFFFB00FFFFFB00FFFFFC00FFFFFC00FFFEFC00FFFC
      F900FFFDFA00FFFEFC00FFFDFD00FFFAFB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000080808000000000000000000000000000DEBED40000000000999999000000
      00000000000099999900000000000000000000000000D6D9D80068513A006050
      4000604830007050400070605000705040006048300060483000604830006048
      300060483000A6A6A90000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008080
      8000000000000000000000000000000000000000000099999900000000000000
      80000000800000000000999999000000000000000000C0A8A000F0F0F000E0D8
      D000E0D0C000E0C8C0009090B000D0C0B000E0B8A000D0B0A000D0B0A000D0A8
      9000D0A090006048300000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009999990000000000000080000000
      FF000000FF0000008000000000000000000000000000C0A8A000FFF0F000F0F0
      F000F0E8E0003050C0001038B0007078C000E0D0D000F0D0C000E0D0C000E0C8
      B000D0A8900060483000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009999990000000000000080000000
      00000000FF0000008000000000000000000000000000C0A8A000FFF0F000D0D0
      E0003050C0003058F0002048E0001038B000A098C000F0D0C000F0D0C000E0C8
      B000D0A8900060483000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000009999990099999900000000000000
      80000000800000000000999999000000000000000000C0B0A000FFF8F0002040
      C0003058F0006080FF005078F0004060F0002040B000D0C0C000F0D8D000E0C8
      C000D0B0A00060483000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099999900999999000000
      00000000000099999900999999000000000000000000C0B0A000FFF8F00080A0
      FF008098FF008090F000D0D0E0008098F0004060E0004058B000F0D8D000F0D8
      D000D0B8A00060504000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000999999009999
      99009999990099999900000000000000000000000000D0B0A000FFF8FF00E0E8
      FF00C0C8F000F0F0F000F0F0E000E0D8E0008090F0003058E0005068B000F0E0
      D000E0C8B00070584000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000000000000999999000000
      0000000000009999990000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0B8A00000000000FFF8
      FF00FFF8F000FFF8F000FFF0F000F0F0E000F0E0E0007088F0002050D0009090
      C000E0D0C00080706000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000099999900000000000080
      0000008000000000000099999900000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0B8B000000000000000
      0000FFF8FF00FFF8F000FFF8F000F0F0F000F0E0E000F0E8E0008090F0002048
      D000A098C000A0908000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000099999900000000000080000000FF
      000000FF00000080000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0C0B000000000000000
      000080A0B000608890006088900060789000607880007080900090A0B00090A0
      F0003050D000B0989000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000009999990000000000008000000000
      000000FF00000080000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D0C0B000000000000000
      000080A8B00090D8E00090E8F00080D8F00060C8E0005098B00070809000F0E8
      E000E0D8D000A0989000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000009999990099999900000000000080
      0000008000000000000099999900000000000000000000000000000000000000
      00008080800000000000000000000000000000000000D1C2B300000000000000
      0000F0F8FF0080A8B000A0A8A0009586770080C8D00050708000F0F0F000F0E0
      E000F0E0E00080706000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000099999900999999000000
      0000000000009999990099999900000000000000000000000000000000008080
      80000000000000000000000000000000000000000000E2E5E100D1C2B300D0C0
      B000D0C0B00070A8B000A0E8F000A0E8F00090D0E00040687000C0A8A000C0A8
      A000C0A89000D6DAD600000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000999999009999
      9900999999009999990000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000DCE3E10080B0C00080A0B0007090A000D8DDD900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFCFCF00000000FFFFCFCF00000000
      0001FF85000000000001FF83000000000001FD03000000000001800000000000
      0001800000000000000180000000000000018000000000000001800000000000
      00018001000000000001F58300000000000167870000000000013F9B00000000
      00017FBF000000000001DFFF000000008000FFFFFFFFC0030000FFF3FD7F8003
      0000FFE9F01F80030000F051F19F80030000E023C04F80030000C027845F8003
      0000860FA40F800300009987A307800300000087A001800300000047A043F00F
      00000007A001F00F00000007A043F00F0000800FA067F00F0000801FBF7FF00F
      0000C03FC0FFF01F0000F0FFFFFFFFFFFFFFFFFFFFFFFFFFFE00FFFFF0438003
      FE00C003E781800300008001EF01800300008001C711800300008001EF018003
      00008001FF81800300008001FFC3800300008001C3FFA0030000800181F7B003
      0000800101E3B0030000800111F7B0030001800101E7B00300038001810F8003
      0077C003C3FFF83F007FFFFFFFFFFFFF}
  end
  object qryOPPlanejada: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOrdemProducao'
      '      ,cNumeroOP                            '
      
        '      ,SUBSTRING(cNmTabTipoOrigemOP,1,20)   as cNmTabTipoOrigemO' +
        'P'
      '      ,dDtAbertura                          '
      '      ,dDtPrevConclusao                     '
      '      ,SUBSTRING(TipoOP.cNmTipoOP,1,20)     as cNmTipoOP'
      '      ,Produto.nCdProduto                   '
      '      ,Produto.cReferencia                  '
      '      ,SUBSTRING(Produto.cNmProduto,1,50)   as cNmProduto'
      '      ,nQtdePlanejada                       '
      '      ,OP.nCdPedido                         '
      '      ,SUBSTRING(Terceiro.cNmTerceiro,1,30) as cNmTerceiro'
      
        '      ,CASE WHEN dDtPrevConclusao < dbo.fn_OnlyDate(GetDate()) T' +
        'HEN '#39'Sim'#39
      '            ELSE NULL'
      '       END cFlgAtraso'
      '      ,cFlgEstoqueEmpenhado'
      '  FROM OrdemProducao OP'
      
        '       INNER JOIN TipoOP          ON TipoOP.nCdTipoOP           ' +
        '        = OP.nCdTipoOP'
      
        '       INNER JOIN Produto         ON Produto.nCdProduto         ' +
        '        = OP.nCdProduto'
      
        '       INNER JOIN TabTipoOrigemOP ON TabTipoOrigemOP.nCdTabTipoO' +
        'rigemOP = OP.nCdTabTipoOrigemOP'
      
        '       LEFT  JOIN Pedido          ON Pedido.nCdPedido           ' +
        '        = OP.nCdPedido'
      
        '       LEFT  JOIN Terceiro        ON Terceiro.nCdTerceiro       ' +
        '        = Pedido.nCdTerceiro'
      ' WHERE OP.nCdEmpresa         = :nPK'
      '   AND OP.nCdTabTipoStatusOP = 2'
      '')
    Left = 292
    Top = 149
    object qryOPPlanejadanCdOrdemProducao: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object qryOPPlanejadacNumeroOP: TStringField
      FieldName = 'cNumeroOP'
    end
    object qryOPPlanejadacNmTabTipoOrigemOP: TStringField
      FieldName = 'cNmTabTipoOrigemOP'
      ReadOnly = True
    end
    object qryOPPlanejadadDtAbertura: TDateTimeField
      FieldName = 'dDtAbertura'
    end
    object qryOPPlanejadadDtPrevConclusao: TDateTimeField
      FieldName = 'dDtPrevConclusao'
    end
    object qryOPPlanejadacNmTipoOP: TStringField
      FieldName = 'cNmTipoOP'
      ReadOnly = True
    end
    object qryOPPlanejadanCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryOPPlanejadacReferencia: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object qryOPPlanejadacNmProduto: TStringField
      FieldName = 'cNmProduto'
      ReadOnly = True
      Size = 50
    end
    object qryOPPlanejadanQtdePlanejada: TBCDField
      FieldName = 'nQtdePlanejada'
      Precision = 14
      Size = 6
    end
    object qryOPPlanejadanCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryOPPlanejadacNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      ReadOnly = True
      Size = 30
    end
    object qryOPPlanejadacFlgEstoqueEmpenhado: TIntegerField
      FieldName = 'cFlgEstoqueEmpenhado'
    end
    object qryOPPlanejadacFlgAtraso: TStringField
      FieldName = 'cFlgAtraso'
      ReadOnly = True
      Size = 3
    end
  end
  object dsOPPlanejada: TDataSource
    DataSet = qryOPPlanejada
    Left = 332
    Top = 149
  end
  object PopupMenu1: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu1Popup
    Left = 204
    Top = 221
    object AtualizarOrdensPlanejadas1: TMenuItem
      Caption = 'Atualizar Ordens Planejadas'
      ImageIndex = 2
      OnClick = AtualizarOrdensPlanejadas1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object AgendarIncioProduo2: TMenuItem
      Caption = 'Agendar In'#237'cio Produ'#231#227'o'
      ImageIndex = 8
      OnClick = AgendarIncioProduo2Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object AutorizarOP1: TMenuItem
      Caption = 'Autorizar OP'
      ImageIndex = 3
      OnClick = AutorizarOP1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object CancelarOP1: TMenuItem
      Caption = 'Cancelar OP'
      ImageIndex = 4
      OnClick = CancelarOP1Click
    end
    object VisualizarOP1: TMenuItem
      Caption = 'Visualizar OP'
      ImageIndex = 5
      OnClick = VisualizarOP1Click
    end
  end
  object qryOPAutorizada: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOrdemProducao'
      '      ,cNumeroOP                            '
      
        '      ,SUBSTRING(cNmTabTipoOrigemOP,1,20)   as cNmTabTipoOrigemO' +
        'P'
      '      ,dDtAbertura'
      '      ,dDtInicioPrevisto'
      '      ,dDtPrevConclusao'
      '      ,SUBSTRING(TipoOP.cNmTipoOP,1,20)     as cNmTipoOP'
      '      ,Produto.nCdProduto                   '
      '      ,Produto.cReferencia                  '
      '      ,SUBSTRING(Produto.cNmProduto,1,50)   as cNmProduto'
      '      ,nQtdePlanejada                       '
      '      ,OP.nCdPedido                         '
      '      ,SUBSTRING(Terceiro.cNmTerceiro,1,30) as cNmTerceiro'
      
        '      ,CASE WHEN dDtPrevConclusao  < dbo.fn_OnlyDate(GetDate()) ' +
        'THEN '#39'Sim'#39
      
        '            WHEN dDtInicioPrevisto < dbo.fn_OnlyDate(GetDate()) ' +
        'THEN '#39'Sim'#39
      '            ELSE NULL'
      '       END cFlgAtraso'
      '  FROM OrdemProducao OP'
      
        '       INNER JOIN TipoOP          ON TipoOP.nCdTipoOP           ' +
        '        = OP.nCdTipoOP'
      
        '       INNER JOIN Produto         ON Produto.nCdProduto         ' +
        '        = OP.nCdProduto'
      
        '       INNER JOIN TabTipoOrigemOP ON TabTipoOrigemOP.nCdTabTipoO' +
        'rigemOP = OP.nCdTabTipoOrigemOP'
      
        '       LEFT  JOIN Pedido          ON Pedido.nCdPedido           ' +
        '        = OP.nCdPedido'
      
        '       LEFT  JOIN Terceiro        ON Terceiro.nCdTerceiro       ' +
        '        = Pedido.nCdTerceiro'
      ' WHERE OP.nCdEmpresa         = :nPK'
      '   AND OP.nCdTabTipoStatusOP = 3')
    Left = 292
    Top = 189
    object qryOPAutorizadanCdOrdemProducao: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object qryOPAutorizadacNumeroOP: TStringField
      FieldName = 'cNumeroOP'
    end
    object qryOPAutorizadacNmTabTipoOrigemOP: TStringField
      FieldName = 'cNmTabTipoOrigemOP'
      ReadOnly = True
    end
    object qryOPAutorizadadDtAbertura: TDateTimeField
      FieldName = 'dDtAbertura'
    end
    object qryOPAutorizadadDtInicioPrevisto: TDateTimeField
      FieldName = 'dDtInicioPrevisto'
    end
    object qryOPAutorizadadDtPrevConclusao: TDateTimeField
      FieldName = 'dDtPrevConclusao'
    end
    object qryOPAutorizadacNmTipoOP: TStringField
      FieldName = 'cNmTipoOP'
      ReadOnly = True
    end
    object qryOPAutorizadanCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryOPAutorizadacReferencia: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object qryOPAutorizadacNmProduto: TStringField
      FieldName = 'cNmProduto'
      ReadOnly = True
      Size = 50
    end
    object qryOPAutorizadanQtdePlanejada: TBCDField
      FieldName = 'nQtdePlanejada'
      Precision = 14
      Size = 6
    end
    object qryOPAutorizadanCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryOPAutorizadacNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      ReadOnly = True
      Size = 30
    end
    object qryOPAutorizadacFlgAtraso: TStringField
      FieldName = 'cFlgAtraso'
      ReadOnly = True
      Size = 3
    end
  end
  object dsOPAutorizada: TDataSource
    DataSet = qryOPAutorizada
    Left = 332
    Top = 189
  end
  object PopupMenu2: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu2Popup
    Left = 204
    Top = 253
    object MenuItem1: TMenuItem
      Caption = 'Atualizar Ordens Autorizadas'
      ImageIndex = 2
      OnClick = MenuItem1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object AgendarIncioProduo1: TMenuItem
      Caption = 'Agendar In'#237'cio Produ'#231#227'o'
      ImageIndex = 8
      OnClick = AgendarIncioProduo1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object IniciarProcessoProdutivo1: TMenuItem
      Caption = 'Iniciar Processo Produtivo'
      ImageIndex = 6
      OnClick = IniciarProcessoProdutivo1Click
    end
    object RequisitarProdutos1: TMenuItem
      Caption = 'Requisitar Produtos'
      ImageIndex = 7
      OnClick = RequisitarProdutos1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object CancelarOP2: TMenuItem
      Caption = 'Cancelar OP'
      ImageIndex = 4
      OnClick = CancelarOP2Click
    end
    object VisualizarOP2: TMenuItem
      Caption = 'Visualizar OP'
      ImageIndex = 5
      OnClick = VisualizarOP2Click
    end
  end
  object SP_CANCELA_OP: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CANCELA_OP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdOrdemProducao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 468
    Top = 173
  end
  object SP_AUTORIZA_OP: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_AUTORIZA_OP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdOrdemProducao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cDtPrevInicio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 20
        Value = Null
      end>
    Left = 468
    Top = 213
  end
  object qryOPProcesso: TADOQuery
    Connection = frmMenu.Connection
    AfterClose = qryOPProcessoAfterClose
    AfterScroll = qryOPProcessoAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOrdemProducao'
      '      ,cNumeroOP                            '
      
        '      ,SUBSTRING(cNmTabTipoOrigemOP,1,20)   as cNmTabTipoOrigemO' +
        'P'
      '      ,dDtAbertura'
      '      ,dDtInicioPrevisto'
      '      ,dDtPrevConclusao'
      '      ,SUBSTRING(TipoOP.cNmTipoOP,1,20)     as cNmTipoOP'
      '      ,Produto.nCdProduto                   '
      '      ,Produto.cReferencia                  '
      '      ,SUBSTRING(Produto.cNmProduto,1,50)   as cNmProduto'
      '      ,nQtdePlanejada                       '
      '      ,OP.nCdPedido                         '
      '      ,SUBSTRING(Terceiro.cNmTerceiro,1,30) as cNmTerceiro'
      
        '      ,CASE WHEN dDtPrevConclusao < dbo.fn_OnlyDate(GetDate()) T' +
        'HEN '#39'Sim'#39
      '            ELSE NULL'
      '       END cFlgAtraso'
      '      ,nQtdeConcluida'
      '      ,nQtdeCancelada'
      '      ,nQtdeRetrabalho'
      '      ,nQtdeRefugo'
      
        '      ,(nQtdePlanejada - nQtdeConcluida - nQtdeCancelada - nQtde' +
        'Retrabalho - nQtdeRefugo) as nSaldoProduzir'
      '  FROM OrdemProducao OP'
      
        '       INNER JOIN TipoOP          ON TipoOP.nCdTipoOP           ' +
        '        = OP.nCdTipoOP'
      
        '       INNER JOIN Produto         ON Produto.nCdProduto         ' +
        '        = OP.nCdProduto'
      
        '       INNER JOIN TabTipoOrigemOP ON TabTipoOrigemOP.nCdTabTipoO' +
        'rigemOP = OP.nCdTabTipoOrigemOP'
      
        '       LEFT  JOIN Pedido          ON Pedido.nCdPedido           ' +
        '        = OP.nCdPedido'
      
        '       LEFT  JOIN Terceiro        ON Terceiro.nCdTerceiro       ' +
        '        = Pedido.nCdTerceiro'
      ' WHERE OP.nCdEmpresa         = :nPK'
      '   AND OP.nCdTabTipoStatusOP = 4')
    Left = 292
    Top = 229
    object qryOPProcessonCdOrdemProducao: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object qryOPProcessocNumeroOP: TStringField
      FieldName = 'cNumeroOP'
    end
    object qryOPProcessocNmTabTipoOrigemOP: TStringField
      FieldName = 'cNmTabTipoOrigemOP'
      ReadOnly = True
    end
    object qryOPProcessodDtAbertura: TDateTimeField
      FieldName = 'dDtAbertura'
    end
    object qryOPProcessodDtInicioPrevisto: TDateTimeField
      FieldName = 'dDtInicioPrevisto'
    end
    object qryOPProcessodDtPrevConclusao: TDateTimeField
      FieldName = 'dDtPrevConclusao'
    end
    object qryOPProcessocNmTipoOP: TStringField
      FieldName = 'cNmTipoOP'
      ReadOnly = True
    end
    object qryOPProcessonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryOPProcessocReferencia: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object qryOPProcessocNmProduto: TStringField
      FieldName = 'cNmProduto'
      ReadOnly = True
      Size = 50
    end
    object qryOPProcessonQtdePlanejada: TBCDField
      FieldName = 'nQtdePlanejada'
      Precision = 14
      Size = 6
    end
    object qryOPProcessonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryOPProcessocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      ReadOnly = True
      Size = 30
    end
    object qryOPProcessonQtdeCancelada: TIntegerField
      FieldName = 'nQtdeCancelada'
    end
    object qryOPProcessonQtdeRetrabalho: TIntegerField
      FieldName = 'nQtdeRetrabalho'
    end
    object qryOPProcessonQtdeRefugo: TIntegerField
      FieldName = 'nQtdeRefugo'
    end
    object qryOPProcessonSaldoProduzir: TBCDField
      FieldName = 'nSaldoProduzir'
      ReadOnly = True
      Precision = 20
      Size = 6
    end
    object qryOPProcessocFlgAtraso: TStringField
      FieldName = 'cFlgAtraso'
      ReadOnly = True
      Size = 3
    end
    object qryOPProcessonQtdeConcluida: TIntegerField
      FieldName = 'nQtdeConcluida'
    end
  end
  object dsOPProcesso: TDataSource
    DataSet = qryOPProcesso
    Left = 332
    Top = 229
  end
  object PopupMenu3: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu3Popup
    Left = 204
    Top = 285
    object MenuItem2: TMenuItem
      Caption = 'Atualizar Ordens Em Processo'
      ImageIndex = 2
      OnClick = MenuItem2Click
    end
    object MenuItem3: TMenuItem
      Caption = '-'
    end
    object MenuItem4: TMenuItem
      Caption = 'Exibir Registro Apontamento'
      ImageIndex = 5
    end
    object RequisitarProdutos2: TMenuItem
      Caption = 'Requisitar Produtos'
      ImageIndex = 7
      OnClick = RequisitarProdutos2Click
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object RegistrarConcluso1: TMenuItem
      Caption = 'Registrar Conclus'#227'o'
      ImageIndex = 9
      OnClick = RegistrarConcluso1Click
    end
    object RegistrarRetrabalho1: TMenuItem
      Caption = 'Registrar Retrabalho'
      ImageIndex = 9
      OnClick = RegistrarRetrabalho1Click
    end
    object RegistrarPerda1: TMenuItem
      Caption = 'Registrar Perda'
      ImageIndex = 9
      OnClick = RegistrarPerda1Click
    end
    object MenuItem5: TMenuItem
      Caption = '-'
    end
    object MenuItem6: TMenuItem
      Caption = 'Cancelar OP'
      ImageIndex = 4
      OnClick = MenuItem6Click
    end
    object MenuItem7: TMenuItem
      Caption = 'Visualizar OP'
      ImageIndex = 5
      OnClick = MenuItem7Click
    end
  end
  object SP_INICIA_PROCESSO_OP: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_INICIA_PROCESSO_OP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdOrdemProducao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 468
    Top = 317
  end
  object qryRegistroEntrada: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cDocumento'
      '      ,cNmTabTipoEntradaProducao'
      '      ,nQtdeEntrada'
      
        '      ,dbo.fn_ZeroEsquerda(LocalEstoque.nCdLocalEstoque,5) + '#39' -' +
        ' '#39' + LocalEstoque.cNmLocalEstoque as cNmLocalEstoque'
      '      ,dDtEntrada'
      '      ,cNmUsuario'
      '      ,cOBSLivro'
      '  FROM EntradaProducao'
      
        '       LEFT JOIN TabTipoEntradaProducao ON TabTipoEntradaProduca' +
        'o.nCdTabTipoEntradaProducao = EntradaProducao.nCdTabTipoEntradaP' +
        'roducao'
      
        '       LEFT JOIN LocalEstoque           ON LocalEstoque.nCdLocal' +
        'Estoque                     = EntradaProducao.nCdLocalEstoque'
      
        '       LEFT JOIN Usuario                ON Usuario.nCdUsuario   ' +
        '                            = EntradaProducao.nCdUsuario'
      ' WHERE EntradaProducao.nCdOrdemProducao = :nPK'
      ' ORDER BY dDtEntrada DESC'
      '')
    Left = 292
    Top = 277
    object qryRegistroEntradacDocumento: TStringField
      FieldName = 'cDocumento'
      Size = 50
    end
    object qryRegistroEntradacNmTabTipoEntradaProducao: TStringField
      FieldName = 'cNmTabTipoEntradaProducao'
      Size = 50
    end
    object qryRegistroEntradanQtdeEntrada: TBCDField
      FieldName = 'nQtdeEntrada'
      Precision = 14
      Size = 6
    end
    object qryRegistroEntradacNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      ReadOnly = True
      Size = 68
    end
    object qryRegistroEntradadDtEntrada: TDateTimeField
      FieldName = 'dDtEntrada'
    end
    object qryRegistroEntradacNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryRegistroEntradacOBSLivro: TStringField
      FieldName = 'cOBSLivro'
      Size = 100
    end
  end
  object dsRegistroEntrada: TDataSource
    DataSet = qryRegistroEntrada
    Left = 332
    Top = 277
  end
  object SP_AGENDA_INICIO_OP: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_AGENDA_INICIO_OP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdOrdemProducao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cDtPrevInicio'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end>
    Left = 468
    Top = 261
  end
  object qryOPAberta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOrdemProducao'
      '      ,cNumeroOP                            '
      
        '      ,SUBSTRING(cNmTabTipoOrigemOP,1,20)   as cNmTabTipoOrigemO' +
        'P'
      '      ,dDtAbertura'
      '      ,dDtInicioPrevisto'
      '      ,dDtPrevConclusao'
      '      ,SUBSTRING(TipoOP.cNmTipoOP,1,20)     as cNmTipoOP'
      '      ,Produto.nCdProduto                   '
      '      ,Produto.cReferencia                  '
      '      ,SUBSTRING(Produto.cNmProduto,1,50)   as cNmProduto'
      '      ,nQtdePlanejada                       '
      '      ,OP.nCdPedido                         '
      '      ,SUBSTRING(Terceiro.cNmTerceiro,1,30) as cNmTerceiro'
      
        '      ,CASE WHEN dDtPrevConclusao < dbo.fn_OnlyDate(GetDate()) T' +
        'HEN '#39'Sim'#39
      '            ELSE NULL'
      '       END cFlgAtraso'
      '      ,nQtdeConcluida'
      '      ,nQtdeCancelada'
      '      ,nQtdeRetrabalho'
      '      ,nQtdeRefugo'
      
        '      ,(nQtdePlanejada - nQtdeConcluida - nQtdeCancelada - nQtde' +
        'Retrabalho - nQtdeRefugo) as nSaldoProduzir'
      '      ,cNmTabTipoStatusOP'
      '  FROM OrdemProducao OP'
      
        '       INNER JOIN TipoOP          ON TipoOP.nCdTipoOP           ' +
        '        = OP.nCdTipoOP'
      
        '       INNER JOIN Produto         ON Produto.nCdProduto         ' +
        '        = OP.nCdProduto'
      
        '       INNER JOIN TabTipoOrigemOP ON TabTipoOrigemOP.nCdTabTipoO' +
        'rigemOP = OP.nCdTabTipoOrigemOP'
      
        '       LEFT  JOIN Pedido          ON Pedido.nCdPedido           ' +
        '        = OP.nCdPedido'
      
        '       LEFT  JOIN Terceiro        ON Terceiro.nCdTerceiro       ' +
        '        = Pedido.nCdTerceiro'
      
        '       LEFT  JOIN TabTipoStatusOP ON TabTipoStatusOP.nCdTabTipoS' +
        'tatusOP = OP.nCdTabTipoStatusOP'
      ' WHERE OP.nCdEmpresa          = :nPK'
      '   AND OP.nCdTabTipoStatusOP <= 4')
    Left = 292
    Top = 317
    object IntegerField1: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object StringField1: TStringField
      FieldName = 'cNumeroOP'
    end
    object StringField2: TStringField
      FieldName = 'cNmTabTipoOrigemOP'
      ReadOnly = True
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'dDtAbertura'
    end
    object DateTimeField2: TDateTimeField
      FieldName = 'dDtInicioPrevisto'
    end
    object DateTimeField3: TDateTimeField
      FieldName = 'dDtPrevConclusao'
    end
    object StringField3: TStringField
      FieldName = 'cNmTipoOP'
      ReadOnly = True
    end
    object IntegerField2: TIntegerField
      FieldName = 'nCdProduto'
    end
    object StringField4: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object StringField5: TStringField
      FieldName = 'cNmProduto'
      ReadOnly = True
      Size = 50
    end
    object BCDField1: TBCDField
      FieldName = 'nQtdePlanejada'
      Precision = 14
      Size = 6
    end
    object IntegerField3: TIntegerField
      FieldName = 'nCdPedido'
    end
    object StringField6: TStringField
      FieldName = 'cNmTerceiro'
      ReadOnly = True
      Size = 30
    end
    object IntegerField4: TIntegerField
      FieldName = 'nQtdeCancelada'
    end
    object IntegerField5: TIntegerField
      FieldName = 'nQtdeRetrabalho'
    end
    object IntegerField6: TIntegerField
      FieldName = 'nQtdeRefugo'
    end
    object BCDField2: TBCDField
      FieldName = 'nSaldoProduzir'
      ReadOnly = True
      Precision = 20
      Size = 6
    end
    object StringField7: TStringField
      FieldName = 'cFlgAtraso'
      ReadOnly = True
      Size = 3
    end
    object IntegerField7: TIntegerField
      FieldName = 'nQtdeConcluida'
    end
    object qryOPAbertacNmTabTipoStatusOP: TStringField
      FieldName = 'cNmTabTipoStatusOP'
      Size = 50
    end
  end
  object dsOPAberta: TDataSource
    DataSet = qryOPAberta
    Left = 332
    Top = 317
  end
  object PopupMenu4: TPopupMenu
    Left = 212
    Top = 317
    object AtualizarOrdensEmProcesso1: TMenuItem
      Caption = 'Atualizar Ordens em Aberto'
      ImageIndex = 2
      OnClick = AtualizarOrdensEmProcesso1Click
    end
  end
end
