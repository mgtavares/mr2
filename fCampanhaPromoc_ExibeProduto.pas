unit fCampanhaPromoc_ExibeProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxCheckBox, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmCampanhaPromoc_ExibeProduto = class(TfrmProcesso_Padrao)
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    qryProdutos: TADOQuery;
    qryProdutoscFlgAux: TIntegerField;
    qryProdutosnCdProduto: TIntegerField;
    qryProdutoscReferencia: TStringField;
    qryProdutoscNmProduto: TStringField;
    qryProdutosnValCusto: TBCDField;
    qryProdutosnValVenda: TBCDField;
    qryProdutosnQtdeEstoque: TBCDField;
    qryProdutoscNmDepartamento: TStringField;
    qryProdutoscNmCategoria: TStringField;
    qryProdutoscNmSubCategoria: TStringField;
    qryProdutoscNmSegmento: TStringField;
    qryProdutoscNmLinha: TStringField;
    qryProdutoscNmMarca: TStringField;
    qryProdutoscNmGrupoProduto: TStringField;
    qryProdutoscNmClasseProduto: TStringField;
    dsProdutos: TDataSource;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1cFlgAux: TcxGridDBColumn;
    cxGridDBTableView1nCdProduto: TcxGridDBColumn;
    cxGridDBTableView1cReferencia: TcxGridDBColumn;
    cxGridDBTableView1cNmProduto: TcxGridDBColumn;
    cxGridDBTableView1nValCusto: TcxGridDBColumn;
    cxGridDBTableView1nValVenda: TcxGridDBColumn;
    cxGridDBTableView1nQtdeEstoque: TcxGridDBColumn;
    cxGridDBTableView1cNmDepartamento: TcxGridDBColumn;
    cxGridDBTableView1cNmCategoria: TcxGridDBColumn;
    cxGridDBTableView1cNmSubCategoria: TcxGridDBColumn;
    cxGridDBTableView1cNmSegmento: TcxGridDBColumn;
    cxGridDBTableView1cNmLinha: TcxGridDBColumn;
    cxGridDBTableView1cNmMarca: TcxGridDBColumn;
    cxGridDBTableView1cNmGrupoProduto: TcxGridDBColumn;
    cxGridDBTableView1cNmClasseProduto: TcxGridDBColumn;
    qryProdutosnMarkUp: TBCDField;
    qryProdutosdDtUltVenda: TDateTimeField;
    qryProdutosdDtUltReceb: TDateTimeField;
    qryProdutosiDiasEstoque: TIntegerField;
    cxGridDBTableView1nMarkUp: TcxGridDBColumn;
    cxGridDBTableView1dDtUltVenda: TcxGridDBColumn;
    cxGridDBTableView1dDtUltReceb: TcxGridDBColumn;
    cxGridDBTableView1iDiasEstoque: TcxGridDBColumn;
    qryProdutoscNmStatus: TStringField;
    cxGridDBTableView1DBColumn1: TcxGridDBColumn;
    qryAux: TADOQuery;
    qryPreparaTemp: TADOQuery;
    cxGridDBTableView1cNmColecao: TcxGridDBColumn;
    qryProdutoscNmColecao: TStringField;
    qryIncluiProdutos: TADOQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    BCDField3: TBCDField;
    StringField3: TStringField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    StringField7: TStringField;
    StringField8: TStringField;
    StringField9: TStringField;
    StringField10: TStringField;
    BCDField4: TBCDField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    IntegerField3: TIntegerField;
    StringField11: TStringField;
    StringField12: TStringField;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCampanhaPromoc_ExibeProduto: TfrmCampanhaPromoc_ExibeProduto;

implementation

{$R *.dfm}

procedure TfrmCampanhaPromoc_ExibeProduto.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (qryProdutos.State = dsEdit) then
      qryProdutos.Post ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT Count(1) FROM #Produtos_SelCampanha WHERE cFlgAux = 1') ;
  qryAux.Open ;

  if (qryAux.FieldList[0].Value <= 0) then
  begin
      MensagemErro('Nenhum produto selecionado.') ;
      exit ;
  end ;

  if (MessageDlg('Confirma os produtos selecionados ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

 (* try

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM #Produtos_SelCampanha WHERE cFlgAux = 0') ;
      qryAux.ExecSQL;

  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;
   *)
  close ;
  

end;

procedure TfrmCampanhaPromoc_ExibeProduto.ToolButton5Click(
  Sender: TObject);
begin
  inherited;

  qryProdutos.DisableControls;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE #Produtos_SelCampanha ');
  qryAux.SQL.Add('SET cFlgAux = 1 ');
  qryAux.ExecSQL;

  qryProdutos.Close;
  qryProdutos.Open;

  qryProdutos.First ;

  qryProdutos.EnableControls;

  (*while not qryProdutos.Eof do
  begin
      qryProdutos.Edit;
      qryProdutoscFlgAux.Value := 1 ;
      qryProdutos.Post ;
      qryProdutos.Next;
  end ;

  qryProdutos.First ;  *)

end;

procedure TfrmCampanhaPromoc_ExibeProduto.ToolButton6Click(
  Sender: TObject);
begin
  inherited;

  qryProdutos.DisableControls;

  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE #Produtos_SelCampanha ');
  qryAux.SQL.Add('SET cFlgAux = 0 ');
  qryAux.ExecSQL;

  qryProdutos.Close;
  qryProdutos.Open;

  qryProdutos.First ;

  qryProdutos.EnableControls;

  (*qryProdutos.First ;

  while not qryProdutos.Eof do
  begin
      qryProdutos.Edit;
      qryProdutoscFlgAux.Value := 0 ;
      qryProdutos.Post ;
      qryProdutos.Next;
  end ;

  qryProdutos.First ;*)

end;

end.
