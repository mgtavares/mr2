unit fProdutoWeb_AplicaDimensaoProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, dxCntner, dxEditor, dxExEdtr, dxEdLib, DB, ADODB;

type
  TfrmProdutoWeb_AplicaDimensaoProduto = class(TfrmProcesso_Padrao)
    dxCurComprimento: TdxCurrencyEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    dxCurAltura: TdxCurrencyEdit;
    dxCurLargura: TdxCurrencyEdit;
    dxCurPesoLiquido: TdxCurrencyEdit;
    qryAplicaDimensaoProduto: TADOQuery;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdProdutoPai : integer;
  end;

var
  frmProdutoWeb_AplicaDimensaoProduto: TfrmProdutoWeb_AplicaDimensaoProduto;

implementation

uses
    fMenu;

{$R *.dfm}

procedure TfrmProdutoWeb_AplicaDimensaoProduto.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  { -- n�o permite informar dimens�es menores que 0 -- }
  if (dxCurComprimento.Value < 0) then
  begin
      MensagemAlerta('Comprimento (m) informado deve ser maior ou igual a zero.');
      dxCurComprimento.SetFocus;
      Abort;
  end;

  if (dxCurAltura.Value < 0) then
  begin
      MensagemAlerta('Altura (m) informada deve ser maior ou igual a zero.');
      dxCurAltura.SetFocus;
      Abort;
  end;

  if (dxCurLargura.Value < 0) then
  begin
      MensagemAlerta('Largura (m) informada deve ser maior ou igual a zero.');
      dxCurLargura.SetFocus;
      Abort;
  end;

  if (dxCurPesoLiquido.Value < 0) then
  begin
      MensagemAlerta('Peso L�quido (KG) informado deve ser maior ou igual a zero.');
      dxCurPesoLiquido.SetFocus;
      Abort;
  end;

  if (MessageDLG('As dimens�es informadas ser�o aplicadas em todos os sub itens do produto.' + chr(13)
      + 'Confirma atualiza��o das dimens�es?',mtConfirmation,[mbYes,mbNo],0) =  mrYes) then
  begin
      qryAplicaDimensaoProduto.Close;
      qryAplicaDimensaoProduto.Parameters.ParamByName('nPK').Value          := nCdProdutoPai;
      qryAplicaDimensaoProduto.Parameters.ParamByName('nDimensao').Value    := dxCurComprimento.Value;
      qryAplicaDimensaoProduto.Parameters.ParamByName('nAltura').Value      := dxCurAltura.Value;
      qryAplicaDimensaoProduto.Parameters.ParamByName('nLargura').Value     := dxCurLargura.Value;
      qryAplicaDimensaoProduto.Parameters.ParamByName('nPesoLiquido').Value := dxCurPesoLiquido.Value;
      qryAplicaDimensaoProduto.ExecSQL;

      Close;
  end;

end;

end.
