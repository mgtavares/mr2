unit fVinculacaoAgrupamentoContabil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, DB, ADODB, GridsEh, DBGridEh;

type
  TfrmVinculacaoAgrupamentoContabil = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryAgrupamento: TADOQuery;
    qryAgrupamentonCdAgrupamentoContabil: TIntegerField;
    qryAgrupamentocNmAgrupamentoContabil: TStringField;
    qryAgrupamentonCdEmpresa: TIntegerField;
    qryAgrupamentocNmEmpresa: TStringField;
    qryAgrupamentocMascara: TStringField;
    qryAgrupamentoiMaxNiveis: TIntegerField;
    qryAgrupamentocFlgAtivo: TStringField;
    dsAgrupamento: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVinculacaoAgrupamentoContabil: TfrmVinculacaoAgrupamentoContabil;

implementation

uses fAgrupamentoContabil_Estrutura;

{$R *.dfm}

procedure TfrmVinculacaoAgrupamentoContabil.FormShow(Sender: TObject);
begin
  inherited;

  qryAgrupamento.Close;
  qryAgrupamento.Open;

  DBGridEh1.Align := alClient ;
  DBGridEh1.SetFocus;
  
end;

procedure TfrmVinculacaoAgrupamentoContabil.DBGridEh1DblClick(
  Sender: TObject);
var
  objForm : TfrmAgrupamentoContabil_Estrutura ;
begin
  inherited;

  if not qryAgrupamento.IsEmpty then
  begin

      objForm := TfrmAgrupamentoContabil_Estrutura.Create(Self) ;
      objForm.nCdAgrupamentoContabil := qryAgrupamentonCdAgrupamentoContabil.Value;
      objForm.cNmAgrupamentoContabil := qryAgrupamentocNmAgrupamentoContabil.Value;
      objForm.cFormatoMascara        := qryAgrupamentocMascara.Value;

      showForm( objForm, TRUE ) ;

  end ;


end;

initialization
    RegisterClass( TfrmVinculacaoAgrupamentoContabil ) ;

end.
