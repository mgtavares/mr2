unit fImpDFPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RDprint, DB, ADODB, cxLookAndFeelPainters, StdCtrls, cxButtons, Printers,
  Buttons, ExtCtrls, ImgList, ComCtrls, ToolWin, ACBrPosPrinter;

type
  TER2TipoImpDF = (tpNenhum, tpMatricial, tpTermica, tpGrafica);

  TfrmImpDFPadrao = class(TForm)
    qryModImpDF: TADOQuery;
    qryAux: TADOQuery;
    qryModImpDFnCdModImpDF: TIntegerField;
    qryModImpDFcNmModImpDF: TStringField;
    qryModImpDFcQueryHeader: TMemoField;
    qryModImpDFcQueryDetalhe: TMemoField;
    qryModImpDFcQueryFatura: TMemoField;
    qryModImpDFcQueryMensagem: TMemoField;
    qryResultado: TADOQuery;
    qryCampos: TADOQuery;
    qryCamposnCdCampoModImpDF: TAutoIncField;
    qryCamposnCdModImpDF: TIntegerField;
    qryCamposcParte: TStringField;
    qryCamposiSequencia: TIntegerField;
    qryCamposcNmCampo: TStringField;
    qryCamposcDescricao: TStringField;
    qryCamposiLinha: TIntegerField;
    qryCamposiColuna: TIntegerField;
    qryCamposcTipoDado: TStringField;
    qryCamposcTamanhoFonte: TStringField;
    qryCamposcEstiloFonte: TStringField;
    qryCamposcLPP: TStringField;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    RDprint1: TRDprint;
    qryModImpDFcQueryAutent: TMemoField;
    qryModImpDFiLinhaEntreItens: TIntegerField;
    qryResultado2: TADOQuery;
    qryCampos2: TADOQuery;
    qryCampos2nCdCampoModImpDF: TAutoIncField;
    qryCampos2nCdModImpDF: TIntegerField;
    qryCampos2cParte: TStringField;
    qryCampos2iSequencia: TIntegerField;
    qryCampos2cNmCampo: TStringField;
    qryCampos2cDescricao: TStringField;
    qryCampos2iLinha: TIntegerField;
    qryCampos2iColuna: TIntegerField;
    qryCampos2cTipoDado: TStringField;
    qryCampos2cTamanhoFonte: TStringField;
    qryCampos2cEstiloFonte: TStringField;
    qryCampos2cLPP: TStringField;
    qryAmbiente: TADOQuery;
    qryAmbientecNmComputador: TStringField;
    qryAmbientecPortaMatricial: TStringField;
    qryAmbientecFlgUsaECF: TIntegerField;
    qryAmbientecModeloECF: TStringField;
    qryAmbientecPortaECF: TStringField;
    qryAmbientenCdTipoImpressoraPDV: TIntegerField;
    qryAmbienteiViasECF: TIntegerField;
    qryAmbientecPortaEtiqueta: TStringField;
    qryAmbientecFlgEmiteNFe: TIntegerField;
    qryAmbientecNrSerieCertificadoNFe: TStringField;
    qryAmbientedDtUltAcesso: TDateTimeField;
    qryAmbientecNmUsuarioUltAcesso: TStringField;
    qryAmbientecUFEmissaoNfe: TStringField;
    qryAmbientecPathLogoNFe: TStringField;
    qryAmbientenCdTipoAmbienteNFe: TIntegerField;
    qryAmbientenCdUFEmissaoNFe: TIntegerField;
    qryAmbientenCdMunicipioEmissaoNFe: TIntegerField;
    qryAmbientecPathArqNFeProd: TStringField;
    qryAmbientecPathArqNFeHom: TStringField;
    qryAmbientecServidorSMTP: TStringField;
    qryAmbientecEmail: TStringField;
    qryAmbientecSenhaEmail: TStringField;
    qryAmbientenPortaSMTP: TIntegerField;
    qryAmbientecFlgUsaSSL: TIntegerField;
    qryAmbientecTefDialAutoAtivarGP: TIntegerField;
    qryAmbienteiTefDialEsperaSTS: TIntegerField;
    qryAmbienteiTefDialNumVias: TIntegerField;
    qryAmbientecTefDialArqLog: TStringField;
    qryAmbientecTefDialArqReq: TStringField;
    qryAmbientecTefDialArqResp: TStringField;
    qryAmbientecTefDialArqSTS: TStringField;
    qryAmbientecTefDialArqTemp: TStringField;
    qryAmbientecTefDialGPExeName: TStringField;
    qryAmbientecTefDialPatchBackup: TStringField;
    qryAmbientecFlgTEFAtivo: TIntegerField;
    qryAmbientenCdTipoImpressoraPDVCaixa: TIntegerField;
    Edit1: TEdit;
    ToolBar1: TToolBar;
    ToolButton2: TToolButton;
    ImageList1: TImageList;
    Image1: TImage;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    qryUltimaLinha: TADOQuery;
    qryUltimaLinhaiLinha: TIntegerField;
    qryModImpDFiLarguraQRCode: TIntegerField;
    qryModImpDFiColuna: TIntegerField;
    qryModImpDFiEspaco: TIntegerField;
    qryModImpDFiBuffer: TIntegerField;
    qryModImpDFcFlgTraduzirTag: TIntegerField;
    qryModImpDFcFlgIgnorarTag: TIntegerField;
    qryModImpDFcFlgTipoImpDF: TStringField;
    qryModImpDFcPageCodImpDF: TStringField;
    qryModImpDFiLarguraBarras: TIntegerField;
    qryModImpDFiAlturaBarras: TIntegerField;
    qryModImpDFcFlgNumBarras: TIntegerField;
    qryModImpDFiTipoQRCode: TIntegerField;
    qryModImpDFiLevelErroQRCode: TIntegerField;
    qryCamposcTagFormatacao: TStringField;
    qryCampos2cTagFormatacao: TStringField;
    cxButton6: TcxButton;
    procedure ImprimeLinha(cNomeCampo : String; iLinha : Integer; iColuna : Integer; cTipoDado : String; cValorCampo : String; cLPP : String; cEstiloFonte : String; cTamanhoFonte : String; cTags : String);
    procedure ImprimirDocumento();
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure ImprimirCarnet(nCdCrediario : integer; cFlgReimpressao : integer);
    procedure ImprimirReneg(nCdPropostaReneg : integer);
    procedure ImprimirPagtoParcela(nCdLanctoFin : Integer);
    procedure BitBtn1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    function IntToACBrPosPrinterModelo(nCdModPosPrinter : integer) : TACBrPosPrinterModelo;
    function StrToACBrPosPrinterPagCod(cPaginaCodigo : String) : TACBrPosPaginaCodigo;
    procedure cxButton6Click(Sender: TObject);
  private
    { Private declarations }
    tpImpDF   : TER2TipoImpDF;
    slTags    : TStringList;
    iUltLinha : Integer;
    cMsgReimp : String;
    procedure AutenticaParcela (nCdCrediario: integer; iParcela: integer; objPrint: TRDPrint) ; overload ;
    procedure prPreparaImpTermica();
  public
    { Public declarations }
    nCdDoctoFiscal : Integer ;
  published
    procedure  AutenticaParcela (nCdCrediario: integer; iParcela: integer) ; overload ;
  end;

var
  frmImpDFPadrao: TfrmImpDFPadrao;

implementation

uses fMenu;

{$R *.dfm}

procedure  TfrmImpDFPadrao.AutenticaParcela (nCdCrediario: integer; iParcela: integer) ;
begin

    qryAux.Close ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('SELECT cValor FROM Parametro WHERE cParametro = ' + Chr(39) + 'MODIMPAUTPARC' + Chr(39)) ;
    qryAux.Fields.Clear;
    qryAux.Open ;

    if (qryAux.eof) or (qryAux.FieldList[0].Value = '') then
    begin
        qryAux.Close ;
        ShowMessage('Modelo de impress�o de carnet n�o configurado no parametro MODIMPAUTPARC.') ;
        exit ;
    end ;

    // localiza o modelo de impress�o
    qryModImpDF.Close ;
    qryModImpDF.Parameters.ParamByName('nPK').Value := qryAux.FieldList[0].Value ;
    qryModImpDF.Open ;

    qryAmbiente.Close ;
    qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
    qryAmbiente.Open ;

    tpImpDF := tpNenhum;

    case (qryAmbientenCdTipoImpressoraPDVCaixa.Value) of
        2 : tpImpDF := tpTermica;
        3 : tpImpDF := tpMatricial;
        4 : tpImpDF := tpGrafica;
        5 : tpImpDF := tpTermica;
        6 : tpImpDF := tpTermica;
        7 : tpImpDF := tpTermica;
        8 : tpImpDF := tpTermica;
    end;

    if ((frmMenu.LeParametro('TIPOAUTENTCRED') <> 'L') and (frmMenu.LeParametro('TIPOAUTENTCRED') <> 'P')) then
    begin
        frmMenu.MensagemErro('Par�metro TIPOAUTENTCRED n�o configurado corretamente!');
        Exit;
    end;

    { -- config. impressora matricial/gr�fica -- }
    if (tpImpDF in[tpMatricial, tpGrafica]) then
    begin
        RDprint1.Abrir ;
        RDprint1.OpcoesPreview.CaptionPreview := 'Autentica��o Carnet';
        RDprint1.Orientacao                   := poPortrait;

        if (frmMenu.LeParametro('PREVIEWIMP') = 'S') then
        begin
            RDprint1.OpcoesPreview.Preview       := true;
            RDprint1.OpcoesPreview.Remalina      := false;
            RDprint1.OpcoesPreview.PaginaZebrada := false;
        end;

        if (frmMenu.LeParametro('TIPOAUTENTCRED') = 'P') then
            RDprint1.Impressora := Epson;
    end;

    { -- config. impressora t�rmica -- }
    if (tpImpDF = tpTermica) then
    begin
        prPreparaImpTermica;
        slTags    := TStringList.Create;
        iUltLinha := 0;
    end;

    AutenticaParcela(nCdCrediario, iParcela, RDPrint1);

    if (tpImpDF in[tpMatricial, tpGrafica]) then
    begin
        rdPrint1.Fechar;
        Exit;
    end;

    if (tpImpDF = tpTermica) then
    begin
        frmMenu.ACBrPosPrinter1.Imprimir('</zera>' + slTags.Text + '</lf></linha_simples><c><ce>ER2Soft Solucoes Intel. para o seu Negocio</ce></c></lf><c><ce>www.er2soft.com.br</ce></c></lf></corte_total>');
        FreeAndNil(slTags);
        Exit;
    end;
end;

procedure TfrmImpDFPadrao.AutenticaParcela (nCdCrediario: integer; iParcela: integer; objPrint: TRDPrint) ;
begin

    // obtem a query do cabe�alho
    if (qryModImpDFcQueryAutent.Value <> '') then
    begin

        // monta a query dinamica do cabe�alho e recupera o resultado
        qryResultado2.Close ;
        qryResultado2.Fields.Clear ;
        qryResultado2.SQL.Clear ;
        qryResultado2.SQL.Add(qryModImpDFcQueryAutent.AsString) ;
        qryResultado2.Parameters.ParamByName('nCdCrediario').Value := nCdCrediario ;
        qryResultado2.Parameters.ParamByName('iParcela').Value     := iParcela     ;
        qryResultado2.Open ;

        if not qryResultado2.eof then
        begin

            // recupera os campos que dever�o ser impressos na autenticacao
            qryCampos2.Close ;
            qryCampos2.Parameters.ParamByName('cParte').Value := 'A' ;
            qryCampos2.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos2.Open ;

            while not qryCampos2.eof do
            begin

                try
                    if (qryResultado2.FieldValues[qryCampos2cNmCampo.Value] <> null) then
                    begin
                        if ((iParcela = 1) or (frmMenu.LeParametro('TIPOAUTENTCRED') = 'P')) then
                           ImprimeLinha(qryCampos2cNmCampo.Value, qryCampos2iLinha.Value, qryCampos2iColuna.Value , qryCampos2cTipoDado.Value , qryResultado2.FieldValues[qryCampos2cNmCampo.Value], qryCampos2cLPP.Value, qryCampos2cEstiloFonte.Value, qryCampos2cTamanhoFonte.Value, qryCampos2cTagFormatacao.Value)
                        else ImprimeLinha(qryCampos2cNmCampo.Value, qryCampos2iLinha.Value+((iParcela+(qryModImpDFiLinhaEntreItens.Value*(iParcela-1)))-1), qryCampos2iColuna.Value , qryCampos2cTipoDado.Value , qryResultado2.FieldValues[qryCampos2cNmCampo.Value], qryCampos2cLPP.Value, qryCampos2cEstiloFonte.Value, qryCampos2cTamanhoFonte.Value, qryCampos2cTagFormatacao.Value);
                    end ;
                except
                    frmMenu.MensagemErro('Erro no processamento.');
                    Raise;
                end;

                qryCampos2.Next;
            end ;

        end ;

    end ;

    qryCampos2.Close ;
    qryResultado2.Close ;

end ;

procedure TfrmImpDFPadrao.ImprimirPagtoParcela(nCdLanctoFin : Integer);
var
  cSQL   : String;
  i      : Integer;
  iLinha : Integer;
begin
  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT cValor FROM Parametro WHERE cParametro = ' + Chr(39) + 'MODIMPAUTPARC' + Chr(39)) ;
  qryAux.Fields.Clear;
  qryAux.Open ;

  if (qryAux.Eof) or (qryAux.FieldList[0].Value = '') then
  begin
      qryAux.Close ;
      ShowMessage('Modelo de impress�o de carnet n�o configurado no parametro MODIMPAUTPARC.') ;
      exit ;
  end ;

  qryModImpDF.Close ;
  qryModImpDF.Parameters.ParamByName('nPK').Value := qryAux.FieldList[0].Value ;
  qryModImpDF.Open ;

  qryAmbiente.Close ;
  qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
  qryAmbiente.Open ;

  tpImpDF := tpNenhum;

  case (qryAmbientenCdTipoImpressoraPDVCaixa.Value) of
      2 : tpImpDF := tpTermica;
      3 : tpImpDF := tpMatricial;
      4 : tpImpDF := tpGrafica;
      5 : tpImpDF := tpTermica;
      6 : tpImpDF := tpTermica;
      7 : tpImpDF := tpTermica;
      8 : tpImpDF := tpTermica;
  end;

  if (frmMenu.LeParametro('TIPOAUTENTCRED') <> 'PG') then
  begin
      frmMenu.MensagemErro('Par�metro TIPOAUTENTCRED n�o configurado corretamente!');
      Exit;
  end;

  { -- config. impressora matricial/gr�fica -- }
  if (tpImpDF in[tpMatricial, tpGrafica]) then
  begin
      frmMenu.MensagemErro('Impress�o em matricial n�o permitida para este tipo de comprovante.');
      Exit;
  end;

  { -- config. impressora t�rmica -- }
  if (tpImpDF = tpTermica) then
  begin
      prPreparaImpTermica;
      slTags    := TStringList.Create;
      iUltLinha := 0;
  end;

  { -- cabe�alho -- } 
  if (qryModImpDFcQueryHeader.Value <> '') then
  begin
      // Trata a query do cabe�alho
      cSQL := qryModImpDFcQueryHeader.AsString;
      cSQL := StringReplace(cSQL, '@@Usuario', IntToStr(frmMenu.nCdUsuarioLogado), [rfReplaceAll, rfIgnoreCase]);
      cSQL := StringReplace(cSQL, '@@Empresa', IntToStr(frmMenu.nCdEmpresaAtiva),  [rfReplaceAll, rfIgnoreCase]);
      cSQL := StringReplace(cSQL, '@@Loja',    IntToStr(frmMenu.nCdLojaAtiva),     [rfReplaceAll, rfIgnoreCase]);

      // monta a query dinamica do cabe�alho e recupera o resultado
      qryResultado.Close ;
      qryResultado.Fields.Clear ;
      qryResultado.SQL.Clear ;
      qryResultado.SQL.Add(cSQL) ;
      qryResultado.Parameters.ParamByName('nPK').Value := nCdLanctoFin ;
      qryResultado.Open ;

      if (not qryResultado.eof) then
      begin
          // recupera os campos que dever�o ser impressos no cabe�alho
          qryCampos.Close ;
          qryCampos.Parameters.ParamByName('cParte').Value := 'C' ;
          qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
          qryCampos.Open ;

          while (not qryCampos.eof) do
          begin
              try
                  if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                      ImprimeLinha(qryCamposcNmCampo.Value, qryCamposiLinha.Value, qryCamposiColuna.Value, qryCamposcTipoDado.Value, qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
              except
                  frmMenu.MensagemErro('Erro no processamento.');
                  Raise;
              end;

              qryCampos.Next;
          end;
      end;
  end;

  { -- itens -- }
  if (qryModImpDFcQueryDetalhe.Value <> '') then
  begin
      // Trata a query dos itens
      cSQL := qryModImpDFcQueryDetalhe.AsString;
      cSQL := StringReplace(cSQL, '@@Usuario', IntToStr(frmMenu.nCdUsuarioLogado), [rfReplaceAll, rfIgnoreCase]);
      cSQL := StringReplace(cSQL, '@@Empresa', IntToStr(frmMenu.nCdEmpresaAtiva),  [rfReplaceAll, rfIgnoreCase]);
      cSQL := StringReplace(cSQL, '@@Loja',    IntToStr(frmMenu.nCdLojaAtiva),     [rfReplaceAll, rfIgnoreCase]);

      // monta a query dinamica do detalhe e recupera o resultado
      qryResultado.Close ;
      qryResultado.Fields.Clear ;
      qryResultado.SQL.Clear ;
      qryResultado.SQL.Add(cSQL) ;

      for i:= 1 to qryResultado.Parameters.Count do
      begin
          qryResultado.Parameters.Items[i-1].Value := nCdLanctoFin ;
      end ;

      qryResultado.Open ;

      iLinha := 0 ;

      while not qryResultado.eof do
      begin
          // recupera os campos que dever�o ser impressos no cabe�alho
          qryCampos.Close ;
          qryCampos.Parameters.ParamByName('cParte').Value := 'D' ;
          qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
          qryCampos.Open ;

          while not qryCampos.eof do
          begin

              if (iLinha = 0) then
                  iLinha := qryCamposiLinha.Value ;

              try
                  if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                      ImprimeLinha(qryCamposcNmCampo.Value, iLinha, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);

              except
                  frmMenu.MensagemErro('Erro no processamento.');

                  Raise;
              end;

              qryCampos.Next;
          end ;

          Inc(iLinha);
          qryResultado.Next ;
      end ;
  end ;

  { -- mensagem -- }
  if (qryModImpDFcQueryMensagem.Value <> '') then
  begin
      // Trata a query das mensagens
      cSQL := qryModImpDFcQueryMensagem.AsString;
      cSQL := StringReplace(cSQL, '@@Usuario', IntToStr(frmMenu.nCdUsuarioLogado), [rfReplaceAll, rfIgnoreCase]);
      cSQL := StringReplace(cSQL, '@@Empresa', IntToStr(frmMenu.nCdEmpresaAtiva),  [rfReplaceAll, rfIgnoreCase]);
      cSQL := StringReplace(cSQL, '@@Loja',    IntToStr(frmMenu.nCdLojaAtiva),     [rfReplaceAll, rfIgnoreCase]);

      // monta a query dinamica do detalhe e recupera o resultado
      qryResultado.Close ;
      qryResultado.Fields.Clear ;
      qryResultado.SQL.Clear ;
      qryResultado.SQL.Add(cSQL) ;

      for i:= 1 to qryResultado.Parameters.Count do
      begin
          qryResultado.Parameters.Items[i-1].Value := nCdLanctoFin ;
      end ;

      qryResultado.Open ;

      { -- posiciona para pr�xima linha -- }
      if (iLinha = 0) then
          iLinha := iUltLinha;

      if (not qryResultado.eof) then
      begin
          // recupera os campos que dever�o ser impressos no cabe�alho
          qryCampos.Close ;
          qryCampos.Parameters.ParamByName('cParte').Value := 'M' ;
          qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
          qryCampos.Open ;

          while (not qryCampos.Eof) do
          begin
              if (qryCamposiLinha.Value > 0) then
                  iLinha := qryCamposiLinha.Value;

              try
                  if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                      ImprimeLinha(qryCamposcNmCampo.Value, iLinha, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
              except
                  frmMenu.MensagemErro('Erro no processamento.');
                  Raise;
              end;

              Inc(iLinha);
              qryCampos.Next;
          end;
      end;
  end;

  { -- pagto de parcelas -- }
  if (qryModImpDFcQueryAutent.Value <> '') then
  begin
      qryResultado2.Close;
      qryResultado2.Fields.Clear;
      qryResultado2.SQL.Clear;
      qryResultado2.SQL.Add(qryModImpDFcQueryAutent.AsString);
      qryResultado2.Parameters.ParamByName('nPK').Value := nCdLanctoFin;
      qryResultado2.Open;

      { -- posiciona para pr�xima linha -- }
      if (iLinha = 0) then
          iLinha := iUltLinha;

      while (not qryResultado2.Eof) do
      begin
          { -- recupera os campos que dever�o ser impressos na autenticacao -- }
          qryCampos2.Close;
          qryCampos2.Parameters.ParamByName('cParte').Value := 'A';
          qryCampos2.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value;
          qryCampos2.Open;

          while (not qryCampos2.Eof) do
          begin
              if (qryCamposiLinha.Value > 0) then
                  iLinha := qryCamposiLinha.Value;

              try
                  if (qryResultado2.FieldValues[qryCampos2cNmCampo.Value] <> null) then
                      ImprimeLinha(qryCampos2cNmCampo.Value, iLinha, qryCampos2iColuna.Value , qryCampos2cTipoDado.Value , qryResultado2.FieldValues[qryCampos2cNmCampo.Value], qryCampos2cLPP.Value, qryCampos2cEstiloFonte.Value, qryCampos2cTamanhoFonte.Value, qryCampos2cTagFormatacao.Value);
              except
                  frmMenu.MensagemErro('Erro no processamento.');
                  Raise;
              end;

              qryCampos2.Next;
          end;

          Inc(iLinha);
          qryResultado2.Next ;
      end;
  end;

  qryAux.Close ;
  qryModImpDF.Close;
  qryAmbiente.Close;
  qryCampos2.Close;
  qryResultado2.Close;

  if (tpImpDF = tpTermica) then
  begin
      frmMenu.ACBrPosPrinter1.Imprimir('</zera>' + slTags.Text + '</lf></linha_simples><c><ce>ER2Soft Solucoes Intel. para o seu Negocio</ce></c></lf><c><ce>www.er2soft.com.br</ce></c></lf></corte_total>');
      FreeAndNil(slTags);
      Exit;
  end;
end;

procedure TfrmImpDFPadrao.ImprimeLinha(cNomeCampo : String; iLinha : Integer; iColuna : Integer; cTipoDado : String; cValorCampo : String; cLPP : String; cEstiloFonte : String; cTamanhoFonte : String; cTags : String);
var
  cMascara       : String;
  cValorAnterior : String;
  cValorAtual    : String;
  cValorConfig   : String;
  i              : Integer;
begin
  cMascara := '#,##0.00' ;

  if (cNomeCampo = 'nValUnitario') then
      cMascara := frmMenu.cMascaraCompras;

  if (tpImpDF in[tpMatricial, tpGrafica]) then
  begin
      if cLPP = 'SEIS' then
          rdPrint1.TamanhoQteLPP := seis ;

      if cLPP = 'OITO' then
          rdPrint1.TamanhoQteLPP := oito ;

      if cTipoDado = 'C' then
      begin
          if cEstiloFonte = 'NORMAL' then
          begin
              if cTamanhoFonte = 'NORMAL' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[]) ;

              if cTamanhoFonte = 'EXPANDIDO' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[expandido]) ;

              if cTamanhoFonte = 'COMP12' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP12]) ;

              if cTamanhoFonte = 'COMP17' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP17]) ;

              if cTamanhoFonte = 'COMP20' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP20]) ;
          end ;

          if cEstiloFonte = 'ITALICO' then
          begin
              if cTamanhoFonte = 'NORMAL' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[italico]) ;

              if cTamanhoFonte = 'EXPANDIDO' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[expandido,italico]) ;

              if cTamanhoFonte = 'COMP12' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP12,italico]) ;

              if cTamanhoFonte = 'COMP17' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP17,italico]) ;

              if cTamanhoFonte = 'COMP20' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP20,italico]) ;
          end ;

          if cEstiloFonte = 'NEGRITO' then
          begin
              if cTamanhoFonte = 'NORMAL' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[NEGRITO]) ;

              if cTamanhoFonte = 'EXPANDIDO' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[expandido,NEGRITO]) ;

              if cTamanhoFonte = 'COMP12' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP12,NEGRITO]) ;

              if cTamanhoFonte = 'COMP17' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP17,NEGRITO]) ;

              if cTamanhoFonte = 'COMP20' then
                  rdPrint1.ImpF(iLinha, iColuna, cValorCampo,[COMP20,NEGRITO]) ;
          end ;
      end ;

      if cTipoDado = 'I' then
      begin
          if cEstiloFonte = 'NORMAL' then
          begin
              if cTamanhoFonte = 'NORMAL' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[]) ;

              if cTamanhoFonte = 'EXPANDIDO' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[expandido]) ;

              if cTamanhoFonte = 'COMP12' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP12]) ;

              if cTamanhoFonte = 'COMP17' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP17]) ;

              if cTamanhoFonte = 'COMP20' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP20]) ;
          end ;

          if cEstiloFonte = 'ITALICO' then
          begin
              if cTamanhoFonte = 'NORMAL' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[italico]) ;

              if cTamanhoFonte = 'EXPANDIDO' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[expandido,italico]) ;

              if cTamanhoFonte = 'COMP12' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP12,italico]) ;

              if cTamanhoFonte = 'COMP17' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP17,italico]) ;

              if cTamanhoFonte = 'COMP20' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP20,italico]) ;
          end ;

          if cEstiloFonte = 'NEGRITO' then
          begin
              if cTamanhoFonte = 'NORMAL' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[NEGRITO]) ;

              if cTamanhoFonte = 'EXPANDIDO' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[expandido,NEGRITO]) ;

              if cTamanhoFonte = 'COMP12' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP12,NEGRITO]) ;

              if cTamanhoFonte = 'COMP17' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP17,NEGRITO]) ;

              if cTamanhoFonte = 'COMP20' then
                  rdPrint1.ImpD(iLinha, iColuna, cValorCampo,[COMP20,NEGRITO]) ;
          end ;
      end ;

      if cTipoDado = 'V' then
      begin
          if cEstiloFonte = 'NORMAL' then
          begin
              if cTamanhoFonte = 'NORMAL' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[]) ;

              if cTamanhoFonte = 'EXPANDIDO' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[expandido]) ;

              if cTamanhoFonte = 'COMP12' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[COMP12]) ;

              if cTamanhoFonte = 'COMP17' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[COMP17]) ;

              if cTamanhoFonte = 'COMP20' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[COMP20]) ;
          end ;

          if cEstiloFonte = 'ITALICO' then
          begin
              if cTamanhoFonte = 'NORMAL' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[italico]) ;

              if cTamanhoFonte = 'EXPANDIDO' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[expandido,italico]) ;

              if cTamanhoFonte = 'COMP12' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[COMP12,italico]) ;

              if cTamanhoFonte = 'COMP17' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[COMP17,italico]) ;

              if cTamanhoFonte = 'COMP20' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[COMP20,italico]) ;
          end ;

          if cEstiloFonte = 'NEGRITO' then
          begin
              if cTamanhoFonte = 'NORMAL' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[NEGRITO]) ;

              if cTamanhoFonte = 'EXPANDIDO' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[expandido,NEGRITO]) ;

              if cTamanhoFonte = 'COMP12' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[COMP12,NEGRITO]) ;

              if cTamanhoFonte = 'COMP17' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[COMP17,NEGRITO]) ;

              if cTamanhoFonte = 'COMP20' then
                  rdPrint1.ImpD(iLinha, iColuna, FormatFloat(cMascara,StrToFloat(cValorCampo)),[COMP20,NEGRITO]) ;
          end ;
      end ;
  end ;

  if (tpImpDF = tpTermica) then
  begin
      { -- armazena �ltima linha impressa -- }
      if (iUltLinha < iLinha) then
          iUltLinha := iLinha;

      { -- se linha n�o foi informada, considera a pr�xima linha impressa -- }
      if (iLinha = 0) then
      begin
          iLinha    := iUltLinha + 1;
          iUltLinha := iLinha;
      end;

      { -- se valor, configura m�scara -- }
      if cTipoDado = 'V' then
          cValorCampo := FormatFloat(cMascara, StrToFloat(cValorCampo));

      cValorCampo := StringReplace(cTags, '@@value', cValorCampo, [rfReplaceAll,rfIgnoreCase]);

      { -- config. posicionamento da linha -- }
      while (slTags.Count < iLinha) do
          slTags.Add('');

      cValorAtual    := slTags[iLinha-1];
      cValorAnterior := cValorAtual;

      { -- config. espa�os at� atingir a coluna do novo valor -- }
      while (Length(cValorAtual) < (iColuna-1)) do
          cValorAtual := cValorAtual + ' ';

      cValorCampo := Copy(cValorAtual, 1, (iColuna-1)) + cValorCampo;
      cValorCampo := cValorCampo + Copy(cValorAnterior, (Length(cValorCampo)+1), Length(cValorAnterior));

      slTags[iLinha-1] := cValorCampo;
  end;
end ;

procedure TfrmImpDFPadrao.ImprimirCarnet(nCdCrediario : integer; cFlgReimpressao : integer);
var
  i       : integer ;
  iLinha  : integer ;
  iMaximo : integer ;
  iItens  : integer ;
  cSQL    : String ;
begin
    qryAux.Close ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('SELECT cValor FROM Parametro WHERE cParametro = ' + Chr(39) + 'MODIMPCARNET' + Chr(39)) ;
    qryAux.Fields.Clear;
    qryAux.Open ;

    if (qryAux.eof) or (qryAux.FieldList[0].Value = '') then
    begin
        qryAux.Close ;
        ShowMessage('Modelo de impress�o de carnet n�o configurado no par�metro MODIMPCARNET.') ;
        exit ;
    end ;

    // localiza o modelo de impress�o
    qryModImpDF.Close ;
    qryModImpDF.Parameters.ParamByName('nPK').Value := qryAux.FieldList[0].Value ;
    qryModImpDF.Open ;

    qryAmbiente.Close ;
    qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
    qryAmbiente.Open ;

    tpImpDF := tpNenhum;

    case (qryAmbientenCdTipoImpressoraPDVCaixa.Value) of
        2 : tpImpDF := tpTermica;
        3 : tpImpDF := tpMatricial;
        4 : tpImpDF := tpGrafica;
        5 : tpImpDF := tpTermica;
        6 : tpImpDF := tpTermica;
        7 : tpImpDF := tpTermica;
        8 : tpImpDF := tpTermica;
    end;

    { -- config. impressora matricial/gr�fica -- }
    if (tpImpDF in[tpMatricial, tpGrafica]) then
    begin
        rdPrint1.Abrir ;
        rdPrint1.OpcoesPreview.CaptionPreview := 'Carnet';
        //rdPrint1.SetPrinterbyPorta('LPT1') ;
        //RDprint1.UsaGerenciadorImpr := False ;
        //rdprint1.FonteTamanhoPadrao := s05cpp;

        RDprint1.Orientacao := poPortrait;
        RDPrint1.Impressora := Epson ;

        {-- impressora gr�fica no caixa --}
        if (qryAmbientenCdTipoImpressoraPDVCaixa.Value = 4) then
            RDPrint1.Impressora := Grafico ;

        if (frmMenu.LeParametro('PREVIEWIMP') = 'S') then
        begin

            rdprint1.OpcoesPreview.Preview       := true ;
            rdprint1.OpcoesPreview.Remalina      := false ;
            rdprint1.OpcoesPreview.PaginaZebrada := false ;

        end ;
    end;

    { -- config. impressora t�rmica -- }
    if (tpImpDF = tpTermica) then
    begin
        prPreparaImpTermica;
        slTags    := TStringList.Create;
        iUltLinha := 0;
        cMsgReimp := '';
    end;

    // obtem a query do cabe�alho
    if (qryModImpDFcQueryHeader.Value <> '') then
    begin
        // Trata a query do cabe�alho
        cSQL := qryModImpDFcQueryHeader.AsString;
        cSQL := StringReplace(cSQL, '@@Usuario', IntToStr(frmMenu.nCdUsuarioLogado), [rfReplaceAll, rfIgnoreCase]);
        cSQL := StringReplace(cSQL, '@@Empresa', IntToStr(frmMenu.nCdEmpresaAtiva),  [rfReplaceAll, rfIgnoreCase]);
        cSQL := StringReplace(cSQL, '@@Loja',    IntToStr(frmMenu.nCdLojaAtiva),     [rfReplaceAll, rfIgnoreCase]);

        // monta a query dinamica do cabe�alho e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(cSQL) ;
        qryResultado.Parameters.ParamByName('nPK').Value := nCdCrediario ;
        qryResultado.Open ;

        if not qryResultado.eof then
        begin

            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('cParte').Value := 'C' ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos.Open ;

            while not qryCampos.eof do
            begin

                try
                    if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                        ImprimeLinha(qryCamposcNmCampo.Value, qryCamposiLinha.Value, qryCamposiColuna.Value, qryCamposcTipoDado.Value, qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                except
                    frmMenu.MensagemErro('Erro no processamento.');

                    Raise;
                end;

                qryCampos.Next;
            end ;

            // Destaca a reimpress�o de comprovante no credi�rio
            if ((cFlgReimpressao = 1) and (frmMenu.LeParametro('DESTREIMPCRED') = 'S')) then
            begin
                qryUltimaLinha.Close;
                qryUltimaLinha.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value;
                qryUltimaLinha.Parameters.ParamByName('cParte').Value := 'C'; // C = Cabe�alho
                qryUltimaLinha.Open;

                if (tpImpDF = tpTermica) then
                    cMsgReimp := '</lf><e><c><n>*** REIMP. COMPROVANTE 2� VIA ***</n></c></e></lf>'
                else
                    ImprimeLinha('cReimpressao', qryUltimaLinhaiLinha.Value, 1, 'C', '*****REIMPRESS�O DE COMPROVANTE - 2� VIA*****', 'SEIS', 'NORMAL', 'NORMAL', '');
            end;

        end ;

    end ;

    // obtem a query da fatura
    if (qryModImpDFcQueryFatura.Value <> '') then
    begin

        // Trata a query da fatura
        cSQL := qryModImpDFcQueryFatura.AsString;

        cSQL := StringReplace(cSQL, '@@Usuario', IntToStr(frmMenu.nCdUsuarioLogado), [rfReplaceAll, rfIgnoreCase]);
        cSQL := StringReplace(cSQL, '@@Empresa', IntToStr(frmMenu.nCdEmpresaAtiva),  [rfReplaceAll, rfIgnoreCase]);
        cSQL := StringReplace(cSQL, '@@Loja',    IntToStr(frmMenu.nCdLojaAtiva),     [rfReplaceAll, rfIgnoreCase]);

        // monta a query dinamica do cabe�alho e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(cSQL) ;

        for i:= 1 to qryResultado.Parameters.Count do
        begin
            qryResultado.Parameters.Items[i-1].Value := nCdCrediario ;
        end ;

        qryResultado.Open ;

        if not qryResultado.eof then
        begin

            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('cParte').Value := 'F' ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos.Open ;

            while not qryCampos.eof do
            begin
                try
                    if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                        ImprimeLinha(qryCamposcNmCampo.Value, qryCamposiLinha.Value, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                except
                    frmMenu.MensagemErro('Erro no processamento.');

                    Raise;
                end;

                qryCampos.Next;
            end ;
        end ;

    end ;

    // obtem a query dos itens
    if (qryModImpDFcQueryDetalhe.Value <> '') then
    begin
        // Trata a query dos itens
        cSQL := qryModImpDFcQueryDetalhe.AsString;

        cSQL := StringReplace(cSQL, '@@Usuario', IntToStr(frmMenu.nCdUsuarioLogado), [rfReplaceAll, rfIgnoreCase]);
        cSQL := StringReplace(cSQL, '@@Empresa', IntToStr(frmMenu.nCdEmpresaAtiva),  [rfReplaceAll, rfIgnoreCase]);
        cSQL := StringReplace(cSQL, '@@Loja',    IntToStr(frmMenu.nCdLojaAtiva),     [rfReplaceAll, rfIgnoreCase]);

        // monta a query dinamica do detalhe e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(cSQL) ;

        for i:= 1 to qryResultado.Parameters.Count do
        begin
            qryResultado.Parameters.Items[i-1].Value := nCdCrediario ;
        end ;

        qryResultado.Open ;

        iLinha := 0 ;

        while not qryResultado.eof do
        begin

            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('cParte').Value := 'D' ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos.Open ;

            while not qryCampos.eof do
            begin

                if (iLinha = 0) then
                    iLinha := qryCamposiLinha.Value ;

                try
                    if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                        ImprimeLinha(qryCamposcNmCampo.Value, iLinha, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);

                except
                    frmMenu.MensagemErro('Erro no processamento.');

                    Raise;
                end;

                qryCampos.Next;
            end ;

            iLinha := iLinha + 1 ;

            if (frmMenu.LeParametro('TIPOAUTENTCRED') = 'L') then
            begin
                try
                    AutenticaParcela (nCdCrediario, qryResultado.FieldValues['iParcela'], rdPrint1);
                except
                    frmMenu.MensagemErro('Erro no processamento.');
                    Raise;
                end;
            end;

            if (qryModImpDFiLinhaEntreItens.Value > 0) then
            begin
                for i := 1 to qryModImpDFiLinhaEntreItens.Value do
                begin
                    ImprimeLinha(qryCamposcNmCampo.Value, iLinha, qryCamposiColuna.Value, qryCamposcTipoDado.Value, ' ' , qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                    iLinha := iLinha + 1 ;
                end ;

            end ;

            qryResultado.Next ;

        end ;

    end ;

    // obtem a query das mensagens
    if (qryModImpDFcQueryMensagem.Value <> '') then
    begin

        // Trata a query das mensagens
        cSQL := qryModImpDFcQueryMensagem.AsString;

        cSQL := StringReplace(cSQL, '@@Usuario', IntToStr(frmMenu.nCdUsuarioLogado), [rfReplaceAll, rfIgnoreCase]);
        cSQL := StringReplace(cSQL, '@@Empresa', IntToStr(frmMenu.nCdEmpresaAtiva),  [rfReplaceAll, rfIgnoreCase]);
        cSQL := StringReplace(cSQL, '@@Loja',    IntToStr(frmMenu.nCdLojaAtiva),     [rfReplaceAll, rfIgnoreCase]);

        // monta a query dinamica do detalhe e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(cSQL) ;

        for i:= 1 to qryResultado.Parameters.Count do
        begin
            qryResultado.Parameters.Items[i-1].Value := nCdCrediario ;
        end ;

        qryResultado.Open ;

        iLinha  := 0 ;
        iMaximo := StrToInt(frmMenu.LeParametro('QTDEMSGNF')) ;
        iItens  := 0 ;

        while not qryResultado.eof do
        begin

            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('cParte').Value := 'M' ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos.Open ;

            if (iItens <= iMaximo) then
            begin
                while not qryCampos.eof do
                begin

                    iItens := iItens + 1 ;

                    if (iLinha = 0) then
                        iLinha := qryCamposiLinha.Value ;

                    try
                        if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                            ImprimeLinha(qryCamposcNmCampo.Value, qryCamposiLinha.Value, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                    except
                        frmMenu.MensagemErro('Erro no processamento.');

                        Raise;
                    end;

                    qryCampos.Next;
                end ;

                iLinha := iLinha + 1 ;
            end ;

            qryResultado.Next ;

        end ;

    end ;

    qryCampos.Close ;
    qryResultado.Close ;
    qryAux.Close ;
    qryModImpDF.Close ;

    if (tpImpDF in[tpMatricial, tpGrafica]) then
    begin
        rdPrint1.Fechar;
        Exit;
    end;

    if (tpImpDF = tpTermica) then
    begin
        frmMenu.ACBrPosPrinter1.Imprimir('</zera>' + slTags.Text + '</lf></linha_simples><c><ce>ER2Soft Solucoes Intel. para o seu Negocio</ce></c></lf><c><ce>www.er2soft.com.br</ce></c></lf></corte_total>');
        FreeAndNil(slTags);
        Exit;
    end;
end ;

procedure TfrmImpDFPadrao.ImprimirReneg(nCdPropostaReneg : integer);
var
    i       : integer ;
    iLinha  : integer ;
    iMaximo : integer ;
    iItens  : integer ;
begin
    qryAux.Close ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('SELECT cValor FROM Parametro WHERE cParametro = ' + Chr(39) + 'MODIMPRENEG' + Chr(39)) ;
    qryAux.Fields.Clear;
    qryAux.Open ;

    if (qryAux.eof) or (qryAux.FieldList[0].Value = '') then
    begin
        qryAux.Close ;
        ShowMessage('Modelo de impress�o de carnet n�o configurado no parametro MODIMPRENEG.') ;
        exit ;
    end ;

    // localiza o modelo de impress�o
    qryModImpDF.Close ;
    qryModImpDF.Parameters.ParamByName('nPK').Value := qryAux.FieldList[0].Value ;
    qryModImpDF.Open ;

    // processa as informa��es do cabe�alho

    rdPrint1.Abrir ;
    rdPrint1.OpcoesPreview.CaptionPreview := 'Carnet';
    //rdPrint1.SetPrinterbyPorta('LPT1') ;
    //RDprint1.UsaGerenciadorImpr := False ;
    //rdprint1.FonteTamanhoPadrao := s05cpp;

    RDprint1.Orientacao := poPortrait;
    RDPrint1.Impressora := Epson ;

    qryAmbiente.Close ;
    qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
    qryAmbiente.Open ;

    {-- impressora gr�fica no caixa --}
    if (qryAmbientenCdTipoImpressoraPDVCaixa.Value = 4) then
        RDPrint1.Impressora := Grafico ;

    tpImpDF := tpNenhum;

    case (qryAmbientenCdTipoImpressoraPDVCaixa.Value) of
        2 : tpImpDF := tpTermica;
        3 : tpImpDF := tpMatricial;
        4 : tpImpDF := tpGrafica;
        5 : tpImpDF := tpTermica;
        6 : tpImpDF := tpTermica;
        7 : tpImpDF := tpTermica;
        8 : tpImpDF := tpTermica;
    end;

    { -- liberar condi��o abaixo ap�s config. comprovante em imp. t�rmica -- }
    if (tpImpDF = tpTermica) then
    begin
        frmMenu.MensagemErro('Impress�o em t�rmica n�o permitida para este tipo de comprovante.');
        Exit;
    end;

    if (frmMenu.LeParametro('PREVIEWIMP') = 'S') then
    begin

        rdprint1.OpcoesPreview.Preview       := true ;
        rdprint1.OpcoesPreview.Remalina      := false ;
        rdprint1.OpcoesPreview.PaginaZebrada := false ;

    end ;

   // obtem a query do cabe�alho
    if (qryModImpDFcQueryHeader.Value <> '') then
    begin

        // monta a query dinamica do cabe�alho e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(qryModImpDFcQueryHeader.AsString) ;
        qryResultado.Parameters.ParamByName('nPK').Value := nCdPropostaReneg ;
        qryResultado.Open ;

        if not qryResultado.eof then
        begin

            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('cParte').Value := 'C' ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos.Open ;

            while not qryCampos.eof do
            begin

                try
                    if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                        ImprimeLinha(qryCamposcNmCampo.Value, qryCamposiLinha.Value, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                except
                    frmMenu.MensagemErro('Erro no processamento.');

                    Raise;
                end;

                qryCampos.Next;
            end ;

        end ;

    end ;   

    // obtem a query da fatura
    if (qryModImpDFcQueryFatura.Value <> '') then
    begin

        // monta a query dinamica do cabe�alho e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(qryModImpDFcQueryFatura.AsString) ;

        for i:= 1 to qryResultado.Parameters.Count do
        begin
            qryResultado.Parameters.Items[i-1].Value := nCdPropostaReneg ;
        end ;

        qryResultado.Open ;

        if not qryResultado.eof then
        begin

            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('cParte').Value := 'F' ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos.Open ;

            while not qryCampos.eof do
            begin

                try
                    if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                        ImprimeLinha(qryCamposcNmCampo.Value, qryCamposiLinha.Value, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                except
                    frmMenu.MensagemErro('Erro no processamento.');

                    Raise;
                end;

                qryCampos.Next;
            end ;

        end ;

    end ;

    // obtem a query dos itens
    if (qryModImpDFcQueryDetalhe.Value <> '') then
    begin

        // monta a query dinamica do detalhe e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(qryModImpDFcQueryDetalhe.AsString) ;

        for i:= 1 to qryResultado.Parameters.Count do
        begin
            qryResultado.Parameters.Items[i-1].Value := nCdPropostaReneg ;
        end ;

        qryResultado.Open ;

        //recupera quantidade de itens
        iItens := 0;
        iItens := qryResultado.RecordCount;

        iLinha := 0 ;

        while not qryResultado.eof do
        begin
            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('cParte').Value := 'D' ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos.Open ;

            while not qryCampos.eof do
            begin

                if (iLinha = 0) then
                    iLinha := qryCamposiLinha.Value ;

                try
                    if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                    begin
                       ImprimeLinha(qryCamposcNmCampo.Value, ilinha, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                    end ;
                except
                    frmMenu.MensagemErro('Erro no processamento.');

                    Raise;
                end;

                qryCampos.Next;

            end ;

            ilinha := ilinha + 1;


            //if (frmMenu.LeParametro('TIPOAUTENTCRED') = 'L') then
            //    AutenticaParcela (nCdCrediario, qryResultado.FieldValues['iParcela'], rdPrint1);

            if (qryModImpDFiLinhaEntreItens.Value > 0) then
            begin
                for i := 1 to qryModImpDFiLinhaEntreItens.Value do
                begin
                    ImprimeLinha(qryCamposcNmCampo.Value, iLinha, qryCamposiColuna.Value, qryCamposcTipoDado.Value, ' ' , qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                    iLinha := iLinha + 1 ;
                end ;

            end ;

            qryResultado.Next ;

        end ;

    end ;

    // obtem a query das mensagens
    if (qryModImpDFcQueryMensagem.Value <> '') then
    begin

        ilinha := ilinha + 1;

        // monta a query dinamica do detalhe e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(qryModImpDFcQueryMensagem.AsString) ;

        for i:= 1 to qryResultado.Parameters.Count do
        begin
            qryResultado.Parameters.Items[i-1].Value := nCdPropostaReneg ;
        end ;

        qryResultado.Open ;

        while not qryResultado.eof do
        begin

            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('cParte').Value := 'M' ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos.Open ;

            while not qryCampos.eof do
            begin

                try
                    if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                    begin
                        ImprimeLinha(qryCamposcNmCampo.Value, qryCamposiLinha.Value + iItens, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                    end ;
                except
                    frmMenu.MensagemErro('Erro no processamento.');

                    Raise;
                end;

                qryCampos.Next;

            end ;


            qryResultado.Next ;

        end ;

    end ;      

    qryCampos.Close ;
    qryResultado.Close ;
    qryAux.Close ;
    qryModImpDF.Close ;

    rdPrint1.Fechar ;

end ;

procedure TfrmImpDFPadrao.ImprimirDocumento();
var
    i       : integer ;
    iLinha  : integer ;
    iMaximo : integer ;
    iItens  : integer ;
begin

    qryAux.Close ;
    qryAux.SQL.Clear ;
    qryAux.SQL.Add('SELECT nCdModImpDF FROM DoctoFiscal LEFT JOIN SerieFiscal ON SerieFiscal.nCdSerieFiscal = DoctoFiscal.nCdSerieFiscal WHERE nCdDoctoFiscal = ' + IntToStr(nCdDoctoFiscal)) ;
    qryAux.Fields.Clear;
    qryAux.Open ;

    if (qryAux.FieldList[0].Value = 0) then
    begin
        qryAux.Close ;
        ShowMessage('Nenhum modelo de impress�o vinculado a s�rie fiscal deste documento.') ;
        exit ;
    end ;

    // localiza o modelo de impress�o
    qryModImpDF.Close ;
    qryModImpDF.Parameters.ParamByName('nPK').Value := qryAux.FieldList[0].Value ;
    qryModImpDF.Open ;

    // processa as informa��es do cabe�alho

    rdPrint1.Abrir ;
    rdPrint1.OpcoesPreview.CaptionPreview := 'Documento Fiscal';
    //rdPrint1.SetPrinterbyPorta('LPT1') ;
    //RDprint1.UsaGerenciadorImpr := False ;
    //rdprint1.FonteTamanhoPadrao := s05cpp;

    RDprint1.Orientacao := poPortrait;

    if (frmMenu.LeParametro('PREVIEWIMP') = 'S') then
    begin

        rdprint1.OpcoesPreview.Preview      := true ;
        rdprint1.OpcoesPreview.Remalina     := false ;
        rdprint1.OpcoesPreview.PaginaZebrada:= false ;

    end ;


    // obtem a query do cabe�alho
    if (qryModImpDFcQueryHeader.Value <> '') then
    begin

        // monta a query dinamica do cabe�alho e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(qryModImpDFcQueryHeader.AsString) ;
        qryResultado.Parameters.ParamByName('nPK').Value := nCdDoctoFiscal ;
        qryResultado.Open ;

        if not qryResultado.eof then
        begin

            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('cParte').Value := 'C' ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos.Open ;

            while not qryCampos.eof do
            begin

                try
                    if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                        ImprimeLinha(qryCamposcNmCampo.Value, qryCamposiLinha.Value, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                except
                    frmMenu.MensagemErro('Erro no processamento.');

                    Raise;
                end;

                qryCampos.Next;
            end ;

        end ;

    end ;

    // obtem a query da fatura
    if (qryModImpDFcQueryFatura.Value <> '') then
    begin

        // monta a query dinamica do cabe�alho e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(qryModImpDFcQueryFatura.AsString) ;

        for i:= 1 to qryResultado.Parameters.Count do
        begin
            qryResultado.Parameters.Items[i-1].Value := nCdDoctoFiscal ;
        end ;

        qryResultado.Open ;

        if not qryResultado.eof then
        begin

            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('cParte').Value := 'F' ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos.Open ;

            while not qryCampos.eof do
            begin

                try
                    if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                        ImprimeLinha(qryCamposcNmCampo.Value, qryCamposiLinha.Value, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                except
                    frmMenu.MensagemErro('Erro no processamento.');

                    Raise;
                end;


                qryCampos.Next;
            end ;

        end ;

    end ;

    // obtem a query dos itens
    if (qryModImpDFcQueryDetalhe.Value <> '') then
    begin

        // monta a query dinamica do detalhe e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(qryModImpDFcQueryDetalhe.AsString) ;

        for i:= 1 to qryResultado.Parameters.Count do
        begin
            qryResultado.Parameters.Items[i-1].Value := nCdDoctoFiscal ;
        end ;

        qryResultado.Open ;

        iLinha := 0 ;

        while not qryResultado.eof do
        begin

            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('cParte').Value := 'D' ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos.Open ;

            while not qryCampos.eof do
            begin

                if (iLinha = 0) then
                    iLinha := qryCamposiLinha.Value ;

                try
                    if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                        ImprimeLinha(qryCamposcNmCampo.Value, iLinha, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                except
                    frmMenu.MensagemErro('Erro no processamento.');

                    Raise;
                end;

                qryCampos.Next;
            end ;

            iLinha := iLinha + 1 ;

            if (qryModImpDFiLinhaEntreItens.Value > 0) then
            begin
                for i := 1 to qryModImpDFiLinhaEntreItens.Value do
                begin
                    ImprimeLinha(qryCamposcNmCampo.Value, iLinha, qryCamposiColuna.Value, qryCamposcTipoDado.Value, ' ' , qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                    iLinha := iLinha + 1 ;
                end ;

            end ;

            qryResultado.Next ;

        end ;

    end ;

    // obtem a query das mensagens
    if (qryModImpDFcQueryMensagem.Value <> '') then
    begin

        // monta a query dinamica do detalhe e recupera o resultado
        qryResultado.Close ;
        qryResultado.Fields.Clear ;
        qryResultado.SQL.Clear ;
        qryResultado.SQL.Add(qryModImpDFcQueryMensagem.AsString) ;

        for i:= 1 to qryResultado.Parameters.Count do
        begin
            qryResultado.Parameters.Items[i-1].Value := nCdDoctoFiscal ;
        end ;

        qryResultado.Open ;

        iLinha  := 0 ;
        iMaximo := StrToInt(frmMenu.LeParametro('QTDEMSGNF')) ;
        iItens  := 0 ;

        while not qryResultado.eof do
        begin

            // recupera os campos que dever�o ser impressos no cabe�alho
            qryCampos.Close ;
            qryCampos.Parameters.ParamByName('cParte').Value := 'M' ;
            qryCampos.Parameters.ParamByName('nPK').Value    := qryAux.FieldList[0].Value ;
            qryCampos.Open ;

            if (iItens <= iMaximo) then
            begin
                while not qryCampos.eof do
                begin

                    iItens := iItens + 1 ;

                    if (iLinha = 0) then
                        iLinha := qryCamposiLinha.Value ;

                    try
                        if (qryResultado.FieldValues[qryCamposcNmCampo.Value] <> null) then
                            ImprimeLinha(qryCamposcNmCampo.Value, iLinha, qryCamposiColuna.Value , qryCamposcTipoDado.Value , qryResultado.FieldValues[qryCamposcNmCampo.Value], qryCamposcLPP.Value, qryCamposcEstiloFonte.Value, qryCamposcTamanhoFonte.Value, qryCamposcTagFormatacao.Value);
                    except
                        frmMenu.MensagemErro('Erro no processamento.');

                        Raise;
                    end;

                    qryCampos.Next;
                end ;

                iLinha := iLinha + 1 ;
            end ;

            qryResultado.Next ;

        end ;

    end ;

    qryCampos.Close ;
    qryResultado.Close ;
    qryAux.Close ;
    qryModImpDF.Close ;

    rdPrint1.Fechar ;

end ;

procedure TfrmImpDFPadrao.cxButton1Click(Sender: TObject);
begin

    nCdDoctoFiscal := strtoint(Edit1.Text) ;
    ImprimirDocumento;

end;

procedure TfrmImpDFPadrao.cxButton2Click(Sender: TObject);
begin
  rdprint1.Abrir;
RDprint1.Orientacao := poLandscape;

  rdprint1.TamanhoQteLPP := seis ;
  RDprint1.IMPF(04,01,inttostr(100),[comp12,negrito]);
  rdprint1.FonteTamanhoPadrao := s12cpp;
  rdprint1.ImpF(05,01,'EDGAR DE SOUZA',[]) ;
  rdprint1.FonteTamanhoPadrao := s17cpp;
  rdprint1.IMPF(06,01,'R APRIGIO DE OLIVEIRA, 24    VILA INDUTRIAL    MOGI DAS CRUZES    SP   08770-120',[]) ;
  rdprint1.FonteTamanhoPadrao := s05cpp;
  RDprint1.IMPD(07,15,formatFloat('###,##0.00',10001.34),[]);
  RDprint1.IMPD(08,15,formatFloat('###,##0.00',121001.34),[]);

  rdprint1.TamanhoQteLPP := oito ;
  rdprint1.FonteTamanhoPadrao := s10cpp;
  RDprint1.IMPF(04,31,inttostr(100),[comp12,negrito]);
  rdprint1.ImpF(05,31,'EDGAR DE SOUZA',[]) ;
  rdprint1.IMPF(06,31,'R APRIGIO DE OLIVEIRA, 24    VILA INDUTRIAL    MOGI DAS CRUZES    SP   08770-120',[]) ;
  RDprint1.IMPD(07,55,formatFloat('###,##0.00',10001.34),[]);
  RDprint1.IMPD(08,55,formatFloat('###,##0.00',121001.34),[]);

  //print1.ImpVal(08,15,'###,##0.00',-11455.78,[]);
  rdprint1.FonteTamanhoPadrao := s10cpp;

  rdprint1.TamanhoQteLPP := oito ;
  rdprint1.ImpF(10,01,'Normal - UMBRO',[Normal]);
  rdprint1.ImpF(11,01,'Expandido - UMBRO',[Expandido]);
  rdprint1.ImpF(12,01,'comp12 - UMBRO',[comp12    ]);
  rdprint1.ImpF(13,01,'comp17 - UMBRO',[comp17        ]);
  rdprint1.ImpF(14,01,'comp20 - UMBRO',[comp20            ]);

  rdprint1.TamanhoQteLPP := seis ;
  rdprint1.ImpF(10,21,'Normal - UMBRO',[Normal]);
  rdprint1.ImpF(11,21,'Expandido - UMBRO',[Expandido]);
  rdprint1.ImpF(12,21,'comp12 - UMBRO',[comp12    ]);
  rdprint1.ImpF(13,21,'comp17 - UMBRO',[comp17        ]);
  rdprint1.ImpF(14,21,'comp20 - UMBRO',[comp20            ]);

  rdprint1.Novapagina;

  rdprint1.TamanhoQteLPP := seis ;
  rdprint1.ImpF(10,21,'Normal - UMBRO',[NORMAL]);
  rdprint1.ImpF(11,21,'Expandido - UMBRO',[EXPANDIDO]);
  rdprint1.ImpF(12,21,'comp12 - UMBRO',[comp12    ]);
  rdprint1.ImpF(13,21,'comp17 - UMBRO',[comp17        ]);
  rdprint1.ImpF(14,21,'comp20 - UMBRO',[comp20            ]);

  rdprint1.OpcoesPreview.Preview := true ;
  rdprint1.OpcoesPreview.Remalina:= false ;
  rdprint1.OpcoesPreview.PaginaZebrada:= false ;


rdprint1.impBox(15,01,'a------------------------------------------------------b');
rdprint1.impBox(16,01,'i');
rdprint1.impBox(17,01,'i');
rdprint1.impBox(18,01,'i');
rdprint1.impBox(19,01,'i');
rdprint1.impBox(20,01,'i');
rdprint1.impBox(21,01,'d------------------------------------------------------c');

  rdprint1.Fechar;
end;

procedure TfrmImpDFPadrao.BitBtn1Click(Sender: TObject);
begin
    Close ;
end;

procedure TfrmImpDFPadrao.cxButton3Click(Sender: TObject);
begin
  AutenticaParcela(27899099,1);
end;

procedure TfrmImpDFPadrao.cxButton5Click(Sender: TObject);
begin
  ImprimirReneg(StrToIntDef(Edit1.Text,0));
end;

procedure TfrmImpDFPadrao.cxButton4Click(Sender: TObject);
begin
  ImprimirCarnet(StrToIntDef(Edit1.Text,0),0);
end;

procedure TfrmImpDFPadrao.ToolButton2Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmImpDFPadrao.prPreparaImpTermica();
begin
  frmMenu.ACBrPosPrinter1.Desativar;

  { -- config. gerais -- }
  frmMenu.ACBrPosPrinter1.Modelo             := IntToACBrPosPrinterModelo(qryAmbientenCdTipoImpressoraPDVCaixa.Value);
  frmMenu.ACBrPosPrinter1.Porta              := Trim(qryAmbientecPortaMatricial.Value);
  frmMenu.ACBrPosPrinter1.LinhasBuffer       := qryModImpDFiBuffer.Value;
  frmMenu.ACBrPosPrinter1.LinhasEntreCupons  := qryModImpDFiLinhaEntreItens.Value;
  frmMenu.ACBrPosPrinter1.EspacoEntreLinhas  := qryModImpDFiEspaco.Value;
  frmMenu.ACBrPosPrinter1.ColunasFonteNormal := qryModImpDFiColuna.Value;
  frmMenu.ACBrPosPrinter1.ControlePorta      := False;
  frmMenu.ACBrPosPrinter1.TraduzirTags       := (qryModImpDFcFlgTraduzirTag.Value = 1);
  frmMenu.ACBrPosPrinter1.IgnorarTags        := (qryModImpDFcFlgIgnorarTag.Value = 1);
  frmMenu.ACBrPosPrinter1.PaginaDeCodigo     := StrToACBrPosPrinterPagCod(qryModImpDFcPageCodImpDF.Value);

  { -- cofig. c�digo de barras -- }
  frmMenu.ACBrPosPrinter1.ConfigBarras.MostrarCodigo := (qryModImpDFcFlgNumBarras.Value = 1);
  frmMenu.ACBrPosPrinter1.ConfigBarras.LarguraLinha  := qryModImpDFiLarguraBarras.Value;
  frmMenu.ACBrPosPrinter1.ConfigBarras.Altura        := qryModImpDFiAlturaBarras.Value;

  { -- config. qrcode -- }
  frmMenu.ACBrPosPrinter1.ConfigQRCode.Tipo          := qryModImpDFiTipoQRCode.Value;
  frmMenu.ACBrPosPrinter1.ConfigQRCode.LarguraModulo := qryModImpDFiLarguraQRCode.Value;
  frmMenu.ACBrPosPrinter1.ConfigQRCode.ErrorLevel    := qryModImpDFiLevelErroQRCode.Value;

  { -- config. logo -- }
  {frmMenu.ACBrPosPrinter1.ConfigLogo.KeyCode1 := 0;
  frmMenu.ACBrPosPrinter1.ConfigLogo.KeyCode2  := 0;
  frmMenu.ACBrPosPrinter1.ConfigLogo.FatorX    := 0;
  frmMenu.ACBrPosPrinter1.ConfigLogo.FatorY    := 0;}

  frmMenu.ACBrPosPrinter1.Ativar;
   sleep(500);
end;


function TfrmImpDFPadrao.IntToACBrPosPrinterModelo(nCdModPosPrinter : integer) : TACBrPosPrinterModelo;
begin
  case (nCdModPosPrinter) of
      2 : Result := ppEscBematech;
      5 : Result := ppEscPosEpson;
      6 : Result := ppEscDaruma;
      7 : Result := ppEscVox;    //ppEscElgin;   Alterado 02/08/18 nova versoa ACBR por Marcelo
      8 : Result := ppEscDiebold;
  end;
end;

function TfrmImpDFPadrao.StrToACBrPosPrinterPagCod(cPaginaCodigo : String) : TACBrPosPaginaCodigo;
begin
  if (cPaginaCodigo = 'pcNone') then
      Result := pcNone;

  if (cPaginaCodigo = 'pc437') then
      Result := pc437;

  if (cPaginaCodigo = 'pc850') then
      Result := pc850;

  if (cPaginaCodigo = 'pc852') then
      Result := pc852;

  if (cPaginaCodigo = 'pc860') then
      Result := pc860;

  if (cPaginaCodigo = 'pcUTF8') then
      Result := pcUTF8;

  if (cPaginaCodigo = 'pc1252') then
      Result := pc1252;
end;

procedure TfrmImpDFPadrao.cxButton6Click(Sender: TObject);
begin
  ImprimirPagtoParcela(StrToIntDef(Edit1.Text,0));
end;

initialization
    RegisterClass(TfrmImpDFPadrao) ;

end.
