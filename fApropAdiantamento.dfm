inherited frmApropAdiantamento: TfrmApropAdiantamento
  Left = 244
  Top = 199
  Caption = 'Apropria'#231#227'o Adiantamento'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    ButtonWidth = 93
    inherited ToolButton1: TToolButton
      Caption = 'Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 93
    end
    inherited ToolButton2: TToolButton
      Left = 101
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    DataGrouping.GroupLevels = <>
    DataSource = dsPedido
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdPedido'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtPedido'
        Footers = <>
        Width = 89
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        Width = 350
      end
      item
        EditButtons = <>
        FieldName = 'nValAdiantamento'
        Footers = <>
        Width = 105
      end
      item
        EditButtons = <>
        FieldName = 'nValPedido'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdPedido'
      '      ,dDtPedido'
      '      ,cNmTerceiro'
      '      ,nValAdiantamento'
      '      ,nValPedido'
      '  FROM Pedido'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Pedido.nCdT' +
        'erceiro'
      ' WHERE nCdEmpresa        = :nPK'
      '   AND nValAdiantamento  > 0'
      '   AND cFlgAdiantConfirm = 0')
    Left = 288
    Top = 120
    object qryPedidonCdPedido: TIntegerField
      DisplayLabel = 'Adiantamentos aguardando confirma'#231#227'o|Pedido'
      FieldName = 'nCdPedido'
    end
    object qryPedidodDtPedido: TDateTimeField
      DisplayLabel = 'Adiantamentos aguardando confirma'#231#227'o|Data'
      FieldName = 'dDtPedido'
    end
    object qryPedidocNmTerceiro: TStringField
      DisplayLabel = 'Adiantamentos aguardando confirma'#231#227'o|Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryPedidonValAdiantamento: TBCDField
      DisplayLabel = 'Adiantamentos aguardando confirma'#231#227'o|Valor Adiantamento'
      FieldName = 'nValAdiantamento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryPedidonValPedido: TBCDField
      DisplayLabel = 'Adiantamentos aguardando confirma'#231#227'o|Valor Pedido'
      FieldName = 'nValPedido'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsPedido: TDataSource
    DataSet = qryPedido
    Left = 336
    Top = 120
  end
end
