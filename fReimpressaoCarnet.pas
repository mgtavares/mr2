unit fReimpressaoCarnet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmReimpressaoCarnet = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryCrediario: TADOQuery;
    dsCrediario: TDataSource;
    qryCrediarionCdCrediario: TIntegerField;
    qryCrediarionCdLoja: TIntegerField;
    qryCrediariocNmLoja: TStringField;
    qryCrediariodDtVenda: TDateTimeField;
    qryCrediariodDtUltVencto: TDateTimeField;
    qryCrediariocNmTerceiro: TStringField;
    qryCrediarionValCrediario: TBCDField;
    qryCrediarionSaldo: TBCDField;
    qryCrediariocNmCondPagto: TStringField;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmReimpressaoCarnet: TfrmReimpressaoCarnet;

implementation

uses fCaixa_SelCliente, fImpDFPadrao;

{$R *.dfm}

procedure TfrmReimpressaoCarnet.ToolButton1Click(Sender: TObject);
var
    nCdTerceiro : integer ;
    objForm     : TfrmCaixa_SelCliente;
begin
  inherited;

  objForm := TfrmCaixa_SelCliente.Create(nil);

  nCdTerceiro := 0 ;
  nCdTerceiro := objForm.SelecionaCliente ;

  qryCrediario.Close ;

  if (nCdTerceiro > 0) then
      PosicionaQuery(qryCrediario, IntToStr(nCdTerceiro)) ;

  if (qryCrediario.eof) then
  begin
      MensagemAlerta('Nenhum crediário em aberto.') ;
  end ;


end;

procedure TfrmReimpressaoCarnet.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient ;
  ToolButton1.Click ;
  
end;

procedure TfrmReimpressaoCarnet.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  if (qryCrediario.eof) then
      exit ;

  case MessageDlg('Confirma a reimpressão deste carnet ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  ShowMessage('Prepare a impressora e clique em OK para iniciar a impressão.') ;

  frmImpDFPadrao.ImprimirCarnet(qryCrediarionCdCrediario.Value, 1);


end;

initialization
    RegisterClass(TfrmReimpressaoCarnet) ;
    
end.
