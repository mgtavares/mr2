inherited frmGeraPropostaRenegociacao_Simulacao: TfrmGeraPropostaRenegociacao_Simulacao
  Left = 915
  Top = 150
  Width = 690
  Height = 608
  BorderIcons = [biSystemMenu]
  Caption = 'Simula'#231#227'o Renegocia'#231#227'o '#218'nica'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 682
    Height = 552
  end
  inherited ToolBar1: TToolBar
    Width = 682
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 8
    Top = 40
    Width = 657
    Height = 81
    Caption = 'Parcelas'
    Color = 13086366
    ParentColor = False
    TabOrder = 1
    object Label1: TLabel
      Left = 47
      Top = 56
      Width = 142
      Height = 13
      Alignment = taRightJustify
      Caption = 'Qtde Parcelas para Simula'#231#227'o'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 95
      Top = 24
      Width = 94
      Height = 13
      Alignment = taRightJustify
      Caption = 'Qtde Max. Parcelas'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 300
      Top = 24
      Width = 91
      Height = 13
      Alignment = taRightJustify
      Caption = 'Maior Atraso (dias)'
      FocusControl = DBEdit3
    end
    object DBEdit1: TDBEdit
      Left = 196
      Top = 48
      Width = 65
      Height = 21
      DataField = 'iQtdeParc'
      DataSource = DataSource1
      TabOrder = 1
      OnExit = DBEdit1Exit
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 196
      Top = 16
      Width = 65
      Height = 21
      DataField = 'iQtdeParcMax'
      DataSource = DataSource1
      TabOrder = 2
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 400
      Top = 16
      Width = 65
      Height = 21
      DataField = 'iDiasAtrasoMax'
      DataSource = DataSource1
      TabOrder = 0
    end
  end
  object GroupBox2: TGroupBox [3]
    Left = 8
    Top = 128
    Width = 657
    Height = 129
    Caption = 'Montante'
    Color = 13086366
    ParentColor = False
    TabOrder = 2
    object Label4: TLabel
      Left = 60
      Top = 28
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Original'
      FocusControl = DBEdit4
    end
    object Label5: TLabel
      Left = 39
      Top = 76
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Desconto M'#225'ximo'
      FocusControl = DBEdit5
    end
    object Label6: TLabel
      Left = 26
      Top = 100
      Width = 97
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Desc. Aplicado'
      FocusControl = DBEdit6
    end
    object Label7: TLabel
      Left = 306
      Top = 28
      Width = 87
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Juros+Multa'
      FocusControl = DBEdit7
    end
    object Label8: TLabel
      Left = 309
      Top = 76
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Desconto M'#225'ximo'
      FocusControl = DBEdit8
    end
    object Label9: TLabel
      Left = 296
      Top = 100
      Width = 97
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Desc. Aplicado'
      FocusControl = DBEdit9
    end
    object Label16: TLabel
      Left = 166
      Top = 50
      Width = 11
      Height = 13
      Caption = '%'
      FocusControl = DBEdit16
    end
    object Label17: TLabel
      Left = 254
      Top = 50
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor'
      FocusControl = DBEdit16
    end
    object Label18: TLabel
      Left = 438
      Top = 50
      Width = 11
      Height = 13
      Caption = '%'
      FocusControl = DBEdit18
    end
    object Label19: TLabel
      Left = 526
      Top = 50
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor'
      FocusControl = DBEdit16
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 128
      Top = 20
      Width = 150
      Height = 21
      DataField = 'nValOriginal'
      DataSource = DataSource1
      TabOrder = 4
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 184
      Top = 68
      Width = 94
      Height = 21
      DataField = 'nValDescOriginalMax'
      DataSource = DataSource1
      TabOrder = 5
    end
    object DBEdit6: TDBEdit
      Left = 184
      Top = 92
      Width = 94
      Height = 21
      DataField = 'nValDescOriginal'
      DataSource = DataSource1
      TabOrder = 1
      OnExit = DBEdit6Exit
    end
    object DBEdit7: TDBEdit
      Tag = 1
      Left = 400
      Top = 20
      Width = 150
      Height = 21
      DataField = 'nValJuros'
      DataSource = DataSource1
      TabOrder = 6
    end
    object DBEdit8: TDBEdit
      Tag = 1
      Left = 456
      Top = 68
      Width = 94
      Height = 21
      DataField = 'nValDescJurosMax'
      DataSource = DataSource1
      TabOrder = 7
    end
    object DBEdit9: TDBEdit
      Left = 456
      Top = 92
      Width = 94
      Height = 21
      DataField = 'nValDescJuros'
      DataSource = DataSource1
      TabOrder = 3
      OnExit = DBEdit9Exit
    end
    object DBEdit16: TDBEdit
      Tag = 1
      Left = 128
      Top = 68
      Width = 49
      Height = 21
      DataField = 'nPercDescOriginalMax'
      DataSource = DataSource1
      TabOrder = 8
    end
    object DBEdit17: TDBEdit
      Left = 128
      Top = 92
      Width = 49
      Height = 21
      DataField = 'nPercDescOriginal'
      DataSource = DataSource1
      TabOrder = 0
      OnExit = DBEdit17Exit
    end
    object DBEdit18: TDBEdit
      Tag = 1
      Left = 400
      Top = 68
      Width = 49
      Height = 21
      DataField = 'nPercDescJurosMax'
      DataSource = DataSource1
      TabOrder = 9
    end
    object DBEdit19: TDBEdit
      Left = 400
      Top = 92
      Width = 49
      Height = 21
      DataField = 'nPercDescJuros'
      DataSource = DataSource1
      TabOrder = 2
      OnExit = DBEdit19Exit
    end
  end
  object GroupBox3: TGroupBox [4]
    Left = 8
    Top = 264
    Width = 657
    Height = 129
    Caption = 'Condi'#231#227'o Renegocia'#231#227'o'
    Color = 13086366
    ParentColor = False
    TabOrder = 3
    object Label10: TLabel
      Left = 41
      Top = 29
      Width = 83
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo Renegociar'
      FocusControl = DBEdit10
    end
    object Label11: TLabel
      Left = 293
      Top = 53
      Width = 100
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Entrada M'#237'nima'
      FocusControl = DBEdit11
    end
    object Label12: TLabel
      Left = 16
      Top = 53
      Width = 108
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Entrada Aplicado'
      FocusControl = DBEdit12
    end
    object Label13: TLabel
      Left = 42
      Top = 77
      Width = 82
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor das Parcela'
      FocusControl = DBEdit13
    end
    object Label14: TLabel
      Left = 296
      Top = 77
      Width = 97
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Parcela M'#237'nima'
      FocusControl = DBEdit14
    end
    object Label15: TLabel
      Left = 31
      Top = 101
      Width = 93
      Height = 13
      Alignment = taRightJustify
      Caption = 'Proposta V'#225'lida at'#233
      FocusControl = DBEdit15
    end
    object DBEdit10: TDBEdit
      Tag = 1
      Left = 128
      Top = 21
      Width = 150
      Height = 21
      DataField = 'nValRenegociado'
      DataSource = DataSource1
      TabOrder = 0
      OnChange = DBEdit10Change
    end
    object DBEdit11: TDBEdit
      Tag = 1
      Left = 400
      Top = 45
      Width = 150
      Height = 21
      DataField = 'nValEntradaMin'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBEdit12: TDBEdit
      Left = 128
      Top = 45
      Width = 150
      Height = 21
      DataField = 'nValEntrada'
      DataSource = DataSource1
      TabOrder = 2
      OnChange = DBEdit12Change
    end
    object DBEdit13: TDBEdit
      Tag = 1
      Left = 128
      Top = 69
      Width = 150
      Height = 21
      DataField = 'nValParcela'
      DataSource = DataSource1
      TabOrder = 3
    end
    object DBEdit14: TDBEdit
      Tag = 1
      Left = 400
      Top = 69
      Width = 150
      Height = 21
      DataField = 'nValParcelaMin'
      DataSource = DataSource1
      TabOrder = 4
    end
    object DBEdit15: TDBEdit
      Tag = 1
      Left = 128
      Top = 93
      Width = 77
      Height = 21
      DataField = 'dDtValProposta'
      DataSource = DataSource1
      TabOrder = 5
    end
  end
  object btGerarProposta: TcxButton [5]
    Left = 8
    Top = 546
    Width = 113
    Height = 29
    Caption = 'Gerar Proposta'
    TabOrder = 4
    OnClick = btGerarPropostaClick
    Glyph.Data = {
      EE030000424DEE03000000000000360000002800000012000000110000000100
      180000000000B803000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000080808000
      0000000000000000808080000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080000000DEBED4FFFFFFFFFFFF
      000000808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      000000000000000000000000DEBED4DEBED4000000DEBED4FFFFFF0000000000
      00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF0000009999999999999999
      99000000FFFFFF000000000000000000FFFFFF000000FFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFF0000FFFFFF000000FFFFFFE6E6E6000000000000FFFFFFDE
      BED4000000DEBED4DEBED4000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      0000FFFFFF000000FFFFFFE6E6E6E6E6E6808080000000FFFFFFFFFFFFDEBED4
      0000008080804E7EA6FFFFFF4E7EA6FFFFFFFFFFFFFFFFFF0000FFFFFF000000
      FFFFFFE6E6E6E6E6E60000008080800000000000000000008080804E7EA60000
      000000000000004E7EA6FFFFFFFFFFFF0000FFFFFF000000FFFFFF008000E6E6
      E6E6E6E6E6E6E60000009999990000004E7EA60000004E7EA64E7EA64E7EA600
      00004E7EA6FFFFFF0000FFFFFF000000FFFFFF008000008000E6E6E6E6E6E6E6
      E6E6999999000000FFFFFF0000004E7EA60000004E7EA6000000FFFFFFFFFFFF
      0000FFFFFF000000FFFFFF008000008000008000E6E6E6E6E6E6999999000000
      4E7EA60000004E7EA64E7EA64E7EA60000004E7EA6FFFFFF0000FFFFFF000000
      FFFFFF008000008000E6E6E6E6E6E6E6E6E6999999000000FFFFFF4E7EA60000
      000000000000004E7EA6FFFFFFFFFFFF0000FFFFFF000000FFFFFF008000E6E6
      E6E6E6E6E6E6E6E6E6E6999999000000FFFFFFFFFFFF4E7EA6FFFFFF4E7EA6FF
      FFFFFFFFFFFFFFFF0000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      0000FFFFFFFFFFFF000000000000000000000000000000000000000000FFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
    LookAndFeel.NativeStyle = True
  end
  object GroupBox4: TGroupBox [6]
    Left = 8
    Top = 400
    Width = 657
    Height = 137
    Caption = 'Observa'#231#245'es'
    Color = 13086366
    ParentColor = False
    TabOrder = 5
    object z: TDBMemo
      Left = 2
      Top = 15
      Width = 653
      Height = 120
      Align = alClient
      Color = clWhite
      DataField = 'cObs'
      DataSource = DataSource1
      TabOrder = 0
    end
  end
  inherited ImageList1: TImageList
    Top = 432
  end
  object dsProposta: TADODataSet
    Connection = frmMenu.Connection
    CommandType = cmdTable
    Parameters = <>
    Left = 416
    Top = 432
    object dsPropostanCdPropostaReneg: TIntegerField
      FieldName = 'nCdPropostaReneg'
    end
    object dsPropostaiQtdeParc: TIntegerField
      FieldName = 'iQtdeParc'
    end
    object dsPropostaiQtdeParcMax: TIntegerField
      FieldName = 'iQtdeParcMax'
    end
    object dsPropostaiDiasAtrasoMax: TIntegerField
      FieldName = 'iDiasAtrasoMax'
    end
    object dsPropostanValOriginal: TFloatField
      FieldName = 'nValOriginal'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanValDescOriginalMax: TFloatField
      FieldName = 'nValDescOriginalMax'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanValDescOriginal: TFloatField
      FieldName = 'nValDescOriginal'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanValJuros: TFloatField
      FieldName = 'nValJuros'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanValDescJurosMax: TFloatField
      FieldName = 'nValDescJurosMax'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanValDescJuros: TFloatField
      FieldName = 'nValDescJuros'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanValRenegociado: TFloatField
      FieldName = 'nValRenegociado'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanValEntradaMin: TFloatField
      FieldName = 'nValEntradaMin'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanValEntrada: TFloatField
      FieldName = 'nValEntrada'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanValParcela: TFloatField
      FieldName = 'nValParcela'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanValParcelaMin: TFloatField
      FieldName = 'nValParcelaMin'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostadDtValProposta: TDateField
      FieldName = 'dDtValProposta'
    end
    object dsPropostacObs: TMemoField
      FieldName = 'cObs'
      BlobType = ftMemo
    end
    object dsPropostanPercDescOriginalMax: TFloatField
      FieldName = 'nPercDescOriginalMax'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanPercDescOriginal: TFloatField
      FieldName = 'nPercDescOriginal'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanPercDescJurosMax: TFloatField
      FieldName = 'nPercDescJurosMax'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanPercDescJuros: TFloatField
      FieldName = 'nPercDescJuros'
      DisplayFormat = '#,##0.00'
    end
    object dsPropostanCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object dsPropostanCdItemListaCobranca: TIntegerField
      FieldName = 'nCdItemListaCobranca'
    end
  end
  object DataSource1: TDataSource
    DataSet = dsProposta
    Left = 416
    Top = 464
  end
  object qryTabRenegociacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'iDiasAtraso'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 *'
      '  FROM TabRenegociacao'
      ' WHERE nCdEmpresa   = :nCdEmpresa'
      '   AND :iDiasAtraso BETWEEN iDiasAtraso AND iDiasAtrasoFim'
      ' ORDER BY iDiasAtraso')
    Left = 496
    Top = 432
    object qryTabRenegociacaonCdTabRenegociacao: TAutoIncField
      FieldName = 'nCdTabRenegociacao'
      ReadOnly = True
    end
    object qryTabRenegociacaonCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryTabRenegociacaoiDiasAtraso: TIntegerField
      FieldName = 'iDiasAtraso'
    end
    object qryTabRenegociacaonPercDescPrincVista: TBCDField
      FieldName = 'nPercDescPrincVista'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaoiParcLimDescPrinc: TIntegerField
      FieldName = 'iParcLimDescPrinc'
    end
    object qryTabRenegociacaonPercDescPrincParc: TBCDField
      FieldName = 'nPercDescPrincParc'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaonPercDescPrincMaiorParc: TBCDField
      FieldName = 'nPercDescPrincMaiorParc'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaonPercDescJurosVista: TBCDField
      FieldName = 'nPercDescJurosVista'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaoiParcLimDescJuros: TIntegerField
      FieldName = 'iParcLimDescJuros'
    end
    object qryTabRenegociacaonPercDescJurosParc: TBCDField
      FieldName = 'nPercDescJurosParc'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaonPercDescJurosMaiorParc: TBCDField
      FieldName = 'nPercDescJurosMaiorParc'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaoiQtdeMaxParc: TIntegerField
      FieldName = 'iQtdeMaxParc'
    end
    object qryTabRenegociacaonPercMinEntrada: TBCDField
      FieldName = 'nPercMinEntrada'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaonPercEntradaSug: TBCDField
      FieldName = 'nPercEntradaSug'
      Precision = 12
      Size = 2
    end
    object qryTabRenegociacaonValParcelaMin: TBCDField
      FieldName = 'nValParcelaMin'
      Precision = 12
      Size = 2
    end
  end
  object qryPropostaReneg: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM PropostaReneg')
    Left = 616
    Top = 432
    object qryPropostaRenegnCdPropostaReneg: TAutoIncField
      FieldName = 'nCdPropostaReneg'
    end
    object qryPropostaRenegnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryPropostaRenegnCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryPropostaRenegnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryPropostaRenegdDtProposta: TDateTimeField
      FieldName = 'dDtProposta'
    end
    object qryPropostaRenegnCdUsuarioGeracao: TIntegerField
      FieldName = 'nCdUsuarioGeracao'
    end
    object qryPropostaRenegnCdUsuarioAprov: TIntegerField
      FieldName = 'nCdUsuarioAprov'
    end
    object qryPropostaRenegdDtAprov: TDateTimeField
      FieldName = 'dDtAprov'
    end
    object qryPropostaRenegdDtValidade: TDateTimeField
      FieldName = 'dDtValidade'
    end
    object qryPropostaRenegnCdTabStatusPropostaReneg: TIntegerField
      FieldName = 'nCdTabStatusPropostaReneg'
    end
    object qryPropostaRenegnCdItemListaCobranca: TIntegerField
      FieldName = 'nCdItemListaCobranca'
    end
    object qryPropostaRenegnCdLojaEfetiv: TIntegerField
      FieldName = 'nCdLojaEfetiv'
    end
    object qryPropostaRenegnCdUsuarioEfetiv: TIntegerField
      FieldName = 'nCdUsuarioEfetiv'
    end
    object qryPropostaRenegdDtEfetiv: TDateTimeField
      FieldName = 'dDtEfetiv'
    end
    object qryPropostaRenegnCdUsuarioCancel: TIntegerField
      FieldName = 'nCdUsuarioCancel'
    end
    object qryPropostaRenegdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryPropostaRenegiDiasAtraso: TIntegerField
      FieldName = 'iDiasAtraso'
    end
    object qryPropostaRenegiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryPropostaRenegnValOriginal: TBCDField
      FieldName = 'nValOriginal'
      Precision = 12
      Size = 2
    end
    object qryPropostaRenegnValJuros: TBCDField
      FieldName = 'nValJuros'
      Precision = 12
      Size = 2
    end
    object qryPropostaRenegnValDescMaxOriginal: TBCDField
      FieldName = 'nValDescMaxOriginal'
      Precision = 12
      Size = 2
    end
    object qryPropostaRenegnValDescOriginal: TBCDField
      FieldName = 'nValDescOriginal'
      Precision = 12
      Size = 2
    end
    object qryPropostaRenegnValDescMaxJuros: TBCDField
      FieldName = 'nValDescMaxJuros'
      Precision = 12
      Size = 2
    end
    object qryPropostaRenegnValDescJuros: TBCDField
      FieldName = 'nValDescJuros'
      Precision = 12
      Size = 2
    end
    object qryPropostaRenegnSaldoNegociado: TBCDField
      FieldName = 'nSaldoNegociado'
      Precision = 12
      Size = 2
    end
    object qryPropostaRenegnValEntrada: TBCDField
      FieldName = 'nValEntrada'
      Precision = 12
      Size = 2
    end
    object qryPropostaRenegnValEntradaMin: TBCDField
      FieldName = 'nValEntradaMin'
      Precision = 12
      Size = 2
    end
    object qryPropostaRenegnValParcela: TBCDField
      FieldName = 'nValParcela'
      Precision = 12
      Size = 2
    end
    object qryPropostaRenegnValParcelaMin: TBCDField
      FieldName = 'nValParcelaMin'
      Precision = 12
      Size = 2
    end
    object qryPropostaRenegcOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
    object qryPropostaRenegcFlgRenegMultipla: TIntegerField
      FieldName = 'cFlgRenegMultipla'
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 576
    Top = 432
  end
  object qryTituloPropostaReneg: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM TituloPropostaReneg')
    Left = 456
    Top = 432
    object qryTituloPropostaRenegnCdTituloPropostaReneg: TIntegerField
      FieldName = 'nCdTituloPropostaReneg'
    end
    object qryTituloPropostaRenegnCdPropostaReneg: TIntegerField
      FieldName = 'nCdPropostaReneg'
    end
    object qryTituloPropostaRenegnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTituloPropostaRenegnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempTituloRenegociacao'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #TempTituloRenegociacao (nCdTitulo       int'
      
        '                                         ,cNrTit          char(1' +
        '7)'
      '                                         ,iParcela        int'
      
        '                                         ,cNmEspTit       varcha' +
        'r(50)'
      
        '                                         ,dDtEmissao      dateti' +
        'me'
      
        '                                         ,dDtVenc         dateti' +
        'me'
      '                                         ,iDiasAtraso     int'
      
        '                                         ,nValTit         decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValLiq         decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValJuro        decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValMulta       decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nValDesconto    decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,nSaldoTit       decima' +
        'l(12,2) default 0 not null'
      
        '                                         ,cFlgAux         int   ' +
        '        default 0 not null'
      
        '                                         ,cFlgRenegociado int   ' +
        '        default 0 not null)'
      ''
      'END'
      ''
      ''
      'SELECT Titulo.nCdTitulo'
      '      ,Titulo.nSaldoTit'
      '  FROM #TempTituloRenegociacao Temp'
      '       INNER JOIN Titulo ON Titulo.nCdTitulo = Temp.nCdTitulo'
      ' WHERE Temp.cFlgAux = 1'
      ' ORDER BY Titulo.dDtVenc')
    Left = 536
    Top = 432
    object qryTitulosnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTitulosnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
  end
end
