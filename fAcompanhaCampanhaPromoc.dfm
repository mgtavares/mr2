inherited frmAcompanhaCampanhaPromoc: TfrmAcompanhaCampanhaPromoc
  Caption = 'Acompanhamento Campanha Promocional'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 73
    Width = 1440
    Height = 777
  end
  inherited ToolBar1: TToolBar
    Width = 1440
    ButtonWidth = 93
    inherited ToolButton1: TToolButton
      Caption = 'Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 93
    end
    inherited ToolButton2: TToolButton
      Left = 101
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 73
    Width = 1440
    Height = 777
    DataGrouping.GroupLevels = <>
    DataSource = dsMaster
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    PopupMenu = PopupMenu1
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdCampanhaPromoc'
        Footers = <>
        Width = 51
      end
      item
        EditButtons = <>
        FieldName = 'cChaveCampanha'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmCampanhaPromoc'
        Footers = <>
        Width = 291
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoTabPreco'
        Footers = <>
        Width = 175
      end
      item
        EditButtons = <>
        FieldName = 'dDtValidadeIni'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtValidadeFim'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cSuspensa'
        Footers = <>
        Width = 55
      end
      item
        EditButtons = <>
        FieldName = 'cVencida'
        Footers = <>
        Width = 63
      end
      item
        EditButtons = <>
        FieldName = 'nValVenda'
        Footers = <>
        Width = 127
      end
      item
        EditButtons = <>
        FieldName = 'nValCMV'
        Footers = <>
        Width = 127
      end
      item
        EditButtons = <>
        FieldName = 'nMargemLucro'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1440
    Height = 44
    Align = alTop
    TabOrder = 2
    object CheckBox1: TCheckBox
      Left = 16
      Top = 16
      Width = 169
      Height = 17
      Caption = 'Exibir Campanhas Suspensas'
      TabOrder = 0
    end
    object CheckBox2: TCheckBox
      Left = 200
      Top = 16
      Width = 169
      Height = 17
      Caption = 'Exibir Campanhas Vencidas'
      TabOrder = 1
    end
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cFlgExibeVencida'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cFlgExibeSuspensa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa        int'
      '       ,@cFlgExibeVencida  int'
      '       ,@cFlgExibeSuspensa int'
      ''
      'Set @nCdEmpresa        = :nCdEmpresa'
      'Set @cFlgExibeVencida  = :cFlgExibeVencida'
      'Set @cFlgExibeSuspensa = :cFlgExibeSuspensa'
      ''
      'SELECT nCdCampanhaPromoc'
      '      ,cChaveCampanha'
      '      ,cNmCampanhaPromoc'
      '      ,cNmTipoTabPreco'
      '      ,dDtValidadeIni'
      '      ,dDtValidadeFim'
      '      ,nValVenda'
      '      ,nValCMV'
      '      ,nMargemLucro'
      '      ,cFlgSuspensa'
      '      ,CASE WHEN cFlgSuspensa = 1 THEN '#39'Sim'#39
      '            ELSE NULL'
      '       END cSuspensa'
      
        '      ,CASE WHEN dDtValidadeFim < dbo.fn_OnlyDate(GetDate()) THE' +
        'N '#39'Sim'#39
      '            ELSE NULL'
      '       END cVencida'
      '  FROM CampanhaPromoc'
      
        '       LEFT JOIN TipoTabPreco ON TipoTabPreco.nCdTipoTabPreco = ' +
        'CampanhaPromoc.nCdTipoTabPreco'
      ' WHERE cFlgAtivada  = 1'
      '   AND nCdEmpresa   = @nCdEmpresa'
      '   AND ((@cFlgExibeSuspensa = 1) OR (cFlgSuspensa = 0))'
      
        '   AND ((@cFlgExibeVencida  = 1) OR (dDtValidadeFim >= dbo.fn_On' +
        'lyDate(GetDate())))'
      ' ORDER BY 1')
    Left = 328
    Top = 144
    object qryMasternCdCampanhaPromoc: TIntegerField
      DisplayLabel = 'Campanhas Ativas|C'#243'd'
      FieldName = 'nCdCampanhaPromoc'
    end
    object qryMastercChaveCampanha: TStringField
      DisplayLabel = 'Campanhas Ativas|Chave'
      FieldName = 'cChaveCampanha'
      Size = 15
    end
    object qryMastercNmCampanhaPromoc: TStringField
      DisplayLabel = 'Campanhas Ativas|Descri'#231#227'o Campanha'
      FieldName = 'cNmCampanhaPromoc'
      Size = 50
    end
    object qryMastercNmTipoTabPreco: TStringField
      DisplayLabel = 'Campanhas Ativas|Tipo Tabela Pre'#231'o'
      FieldName = 'cNmTipoTabPreco'
      Size = 50
    end
    object qryMasterdDtValidadeIni: TDateTimeField
      DisplayLabel = 'Campanhas Ativas|V'#225'lida de'
      FieldName = 'dDtValidadeIni'
    end
    object qryMasterdDtValidadeFim: TDateTimeField
      DisplayLabel = 'Campanhas Ativas|V'#225'lida at'#233
      FieldName = 'dDtValidadeFim'
    end
    object qryMasternValVenda: TBCDField
      DisplayLabel = 'Resultado Financeiro|Valor Venda'
      FieldName = 'nValVenda'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValCMV: TBCDField
      DisplayLabel = 'Resultado Financeiro|Custo Venda'
      FieldName = 'nValCMV'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternMargemLucro: TBCDField
      DisplayLabel = 'Resultado Financeiro|% Lucro Bruto'
      FieldName = 'nMargemLucro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgSuspensa: TIntegerField
      FieldName = 'cFlgSuspensa'
    end
    object qryMastercSuspensa: TStringField
      DisplayLabel = 'Campanhas Ativas|Suspensa'
      FieldName = 'cSuspensa'
      ReadOnly = True
      Size = 3
    end
    object qryMastercVencida: TStringField
      DisplayLabel = 'Campanhas Ativas|Vencida'
      FieldName = 'cVencida'
      ReadOnly = True
      Size = 3
    end
  end
  object dsMaster: TDataSource
    DataSet = qryMaster
    Left = 408
    Top = 144
  end
  object PopupMenu1: TPopupMenu
    OnPopup = PopupMenu1Popup
    Left = 352
    Top = 224
    object AtivarCampanha1: TMenuItem
      Caption = 'Ativar Campanha'
      OnClick = AtivarCampanha1Click
    end
    object SuspenderCampanha1: TMenuItem
      Caption = 'Suspender Campanha'
      OnClick = SuspenderCampanha1Click
    end
    object AnaliseFinanceira: TMenuItem
      Caption = 'An'#225'lise Vendas'
      OnClick = AnaliseFinanceiraClick
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 200
    Top = 168
  end
end
