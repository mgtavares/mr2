inherited frmMovTitulo_Abatimento: TfrmMovTitulo_Abatimento
  Left = 368
  Top = 248
  Width = 391
  Height = 180
  Caption = 'Abatimento em T'#237'tulo'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 375
    Height = 115
  end
  object Label5: TLabel [1]
    Left = 17
    Top = 72
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'T'#237'tulo Credit'#243'rio'
  end
  object Label1: TLabel [2]
    Left = 11
    Top = 48
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Abatimento'
  end
  object Label2: TLabel [3]
    Left = 27
    Top = 96
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero T'#237'tulo'
    FocusControl = DBEdit1
  end
  object Label3: TLabel [4]
    Left = 13
    Top = 120
    Width = 80
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Total T'#237'tulo'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [5]
    Left = 216
    Top = 120
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo T'#237'tulo'
    FocusControl = DBEdit3
  end
  object Label6: TLabel [6]
    Left = 175
    Top = 72
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data de Vencimento'
    FocusControl = DBEdit4
  end
  object Label7: TLabel [7]
    Left = 218
    Top = 96
    Width = 53
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero NF'
    FocusControl = DBEdit5
  end
  inherited ToolBar1: TToolBar
    Width = 375
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit6: TMaskEdit [9]
    Left = 96
    Top = 64
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object edtValor: TcxCurrencyEdit [10]
    Left = 96
    Top = 40
    Width = 121
    Height = 21
    ParentFont = False
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ValidateOnEnter = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    Style.LookAndFeel.Kind = lfStandard
    Style.LookAndFeel.NativeStyle = False
    TabOrder = 1
  end
  object DBEdit1: TDBEdit [11]
    Tag = 1
    Left = 96
    Top = 88
    Width = 89
    Height = 21
    DataField = 'cNrTit'
    DataSource = DataSource1
    TabOrder = 3
  end
  object DBEdit2: TDBEdit [12]
    Tag = 1
    Left = 96
    Top = 112
    Width = 89
    Height = 21
    DataField = 'nValTit'
    DataSource = DataSource1
    TabOrder = 4
  end
  object DBEdit3: TDBEdit [13]
    Tag = 1
    Left = 272
    Top = 112
    Width = 89
    Height = 21
    DataField = 'nSaldoTit'
    DataSource = DataSource1
    TabOrder = 5
  end
  object DBEdit4: TDBEdit [14]
    Tag = 1
    Left = 272
    Top = 64
    Width = 89
    Height = 21
    DataField = 'dDtVenc'
    DataSource = DataSource1
    TabOrder = 6
  end
  object DBEdit5: TDBEdit [15]
    Tag = 1
    Left = 272
    Top = 88
    Width = 89
    Height = 21
    DataField = 'cNrNF'
    DataSource = DataSource1
    TabOrder = 7
  end
  inherited ImageList1: TImageList
    Left = 272
    Top = 32
  end
  object qryTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdGrupoEconomico'
        Size = -1
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdGrupoEconomico int'
      ''
      'Set @nCdGrupoEconomico = :nCdGrupoEconomico'
      ''
      'SELECT nCdTitulo'
      '      ,cNrTit'
      '      ,nValTit'
      '      ,nSaldoTit'
      '      ,dDtVenc'
      '      ,cNrNF'
      '  FROM Titulo'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdT' +
        'erceiro'
      ' WHERE Titulo.nCdTitulo   = :nPK'
      '   AND Titulo.nCdEmpresa  = :nCdEmpresa'
      '   AND (   (Titulo.nCdTerceiro = :nCdTerceiro)'
      
        '        OR ((Terceiro.nCdGrupoEconomico = @nCdGrupoEconomico) AN' +
        'D (@nCdGrupoEconomico IS NOT NULL)))'
      '   AND Titulo.cSenso      = '#39'C'#39
      '   AND Titulo.nSaldoTit   > 0')
    Left = 304
    Top = 32
    object qryTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTitulocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulonValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulonSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulocNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTitulo
    Left = 240
    Top = 32
  end
  object SP_ABATIMENTO_TITULO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ABATIMENTO_TITULO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTituloAbat'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTituloCreditorio'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nValAbatimento'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 208
    Top = 32
  end
end
