unit fConsulta_Template;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DB, GridsEh, DBGridEh, ADODB, ComCtrls, ToolWin,
  ImgList, ExtCtrls, DBCtrls, Mask, {System.ImageList,} DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmConsulta_Template = class(TForm)
    Image1: TImage;
    ImageList1: TImageList;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    DBGridEh1: TDBGridEh;
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    Label1: TLabel;
    ToolButton3: TToolButton;
    Label2: TLabel;
    QtdeRegistros: TMaskEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure ExecutaConsulta ;
    procedure Edit1KeyPress(Sender: TObject; var Key: Char);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsulta_Template: TfrmConsulta_Template;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmConsulta_Template.FormCreate(Sender: TObject);
var
    I : Integer ;
    x : Integer ;
begin
  RegisterClass(TfrmConsulta_Template) ;

  for I := 0 to ComponentCount - 1 do
  begin
    if (Components [I] is TDBEdit) then
    begin
      (Components [I] as TDBEdit).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TDBEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBEdit).CharCase    := ecUpperCase ;
      (Components [I] as TDBEdit).Color       := clWhite ;
      (Components [I] as TDBEdit).Height      := 15 ;
      (Components [I] as TDBEdit).BevelInner  := bvSpace ;
      (Components [I] as TDBEdit).BevelKind   := bkNone ;
      (Components [I] as TDBEdit).BevelOuter  := bvLowered ;
      (Components [I] as TDBEdit).Ctl3D       := True    ;
      (Components [I] as TDBEdit).BorderStyle := bsSingle ;
      (Components [I] as TDBEdit).Font.Name   := 'Tahoma' ;
      (Components [I] as TDBEdit).Font.Size   := 8 ;
      (Components [I] as TDBEdit).Font.Style  := [] ;

      if ((Components [I] as TDBEdit).Tag = 1) then
      begin
          (Components [I] as TDBEdit).Enabled := False ;
          (Components [I] as TDBEdit).Color   := clSilver ;
      end ;

    end;

    if (Components [I] is TEdit) then
    begin
      (Components [I] as TEdit).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      (Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 15 ;
      (Components [I] as TEdit).Font.Name   := 'Tahoma' ;
      (Components [I] as TEdit).Font.Size   := 8 ;
      (Components [I] as TEdit).Font.Style  := [] ;

    end;

    if (Components [I] is TMaskEdit) then
    begin
      (Components [I] as TMaskEdit).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TMaskEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TMaskEdit).CharCase    := ecUpperCase ;
      (Components [I] as TMaskEdit).Color       := clWhite ;
      (Components [I] as TMaskEdit).BevelInner  := bvSpace ;
      (Components [I] as TMaskEdit).BevelKind   := bkNone ;
      (Components [I] as TMaskEdit).BevelOuter  := bvLowered ;
      (Components [I] as TMaskEdit).Ctl3D       := True    ;
      (Components [I] as TMaskEdit).BorderStyle := bsSingle ;
      (Components [I] as TMaskEdit).Height      := 15 ;
      (Components [I] as TMaskEdit).Font.Name   := 'Tahoma' ;
      (Components [I] as TMaskEdit).Font.Size   := 8 ;
      (Components [I] as TMaskEdit).Font.Style  := [] ;

    end;

    if (Components [I] is TDBLookupComboBox) then
    begin
      (Components [I] as TDBLookupComboBox).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TDBLookupComboBox).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBLookupComboBox).Color       := clWhite ;
      (Components [I] as TDBLookupComboBox).BevelInner  := bvSpace   ;
      (Components [I] as TDBLookupComboBox).BevelKind   := bkNone    ;
      (Components [I] as TDBLookupComboBox).BevelOuter  := bvLowered ;
      (Components [I] as TDBLookupComboBox).Ctl3D       := True      ;
      (Components [I] as TDBLookupComboBox).Font.Name   := 'Arial' ;
      (Components [I] as TDBLookupComboBox).Font.Size   := 8 ;
      (Components [I] as TDBLookupComboBox).Font.Style  := [] ;

    end;

    if (Components [I] is TLabel) then
    begin
       (Components [I] as TLabel).Font.Name    := 'Courier New' ;
       (Components [I] as TLabel).Font.Size    := 8 ;
       (Components [I] as TLabel).Font.Style   := [] ;
       (Components [I] as TLabel).Font.Color   := clBlack ;
       (Components [I] as TLabel).Transparent  := True ;
       (Components [I] as TLabel).BringToFront;
    end ;

    if (Components [I] is TButton) then
    begin
       (Components [I] as TButton).ParentFont := False ;
       (Components [I] as TButton).Font.Name  := 'Arial' ;
       (Components [I] as TButton).Font.Size  := 8 ;
       (Components [I] as TButton).Font.Color := clBlue ;
       (Components [I] as TButton).Default    := False ;
       (Components [I] as TButton).Update ;
    end ;
  end ;
end;

procedure TfrmConsulta_Template.FormShow(Sender: TObject);
begin

    Edit1.SetFocus;

end;

procedure TfrmConsulta_Template.ExecutaConsulta;
begin
    showMessage('Execucao Consulta') ;
end ;

procedure TfrmConsulta_Template.FormKeyPress(Sender: TObject;
  var Key: Char);
begin

  if key = #13 then
  begin
    key := #0;
    ExecutaConsulta;
  end;

end;

procedure TfrmConsulta_Template.Edit1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if key = #13 then
  begin
    key := #0;
    ExecutaConsulta;
  end;

end;

procedure TfrmConsulta_Template.ToolButton2Click(Sender: TObject);
begin
    close;
end;

end.
