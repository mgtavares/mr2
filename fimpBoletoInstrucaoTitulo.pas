unit fimpBoletoInstrucaoTitulo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DBGridEhGrouping, DB, ADODB, GridsEh,
  DBGridEh, cxPC, cxControls, ImgList, ComCtrls, ToolWin, ExtCtrls;

type
  TfrmImpBoletoInstrucaoTitulo = class(TfrmProcesso_Padrao)
    qryTituloInstrucaoBoleto: TADOQuery;
    qryTituloInstrucaoBoletonCdTituloInstrucaoBoleto: TIntegerField;
    qryTituloInstrucaoBoletonCdTitulo: TIntegerField;
    qryTituloInstrucaoBoletocMensagem: TStringField;
    qryTituloInstrucaoBoletocFlgMensagemAgrupamento: TIntegerField;
    dsTituloInstrucaoBoleto: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    procedure qryTituloInstrucaoBoletoBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTitulo : integer ;
  end;

var
  frmImpBoletoInstrucaoTitulo: TfrmImpBoletoInstrucaoTitulo;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmImpBoletoInstrucaoTitulo.qryTituloInstrucaoBoletoBeforePost(
  DataSet: TDataSet);
begin
  qryTituloInstrucaoBoletonCdTituloInstrucaoBoleto.Value := frmMenu.fnProximoCodigo('TITULOINSTRUCAOBOLETO');
  qryTituloInstrucaoBoletonCdTitulo.Value                := nCdTitulo;
  qryTituloInstrucaoBoletocFlgMensagemAgrupamento.Value  := 1;
  inherited;

end;

end.
