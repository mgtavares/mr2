unit fConsultaChequeEmitido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, DBCtrls, StdCtrls, Mask, GridsEh, DBGridEh, DBGridEhGrouping,
  cxPC, cxControls;

type
  TfrmConsultaChequeEmitido = class(TfrmProcesso_Padrao)
    qryContaBancaria: TADOQuery;
    qryContaBancariacNmContaBancaria: TStringField;
    qryContaBancarianCdContaBancaria: TIntegerField;
    DataSource1: TDataSource;
    qryAux: TADOQuery;
    qryCheque: TADOQuery;
    qryChequedDtDeposito: TDateTimeField;
    qryChequeiNrCheque: TIntegerField;
    qryChequenValCheque: TBCDField;
    dsCheque: TDataSource;
    qryChequecNmFavorecido: TStringField;
    qryChequedDtEmissao: TDateTimeField;
    qryChequeStatus: TStringField;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit2: TMaskEdit;
    RadioGroup1: TRadioGroup;
    MaskEdit5: TMaskEdit;
    MaskEdit4: TMaskEdit;
    Label2: TLabel;
    Label4: TLabel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit1Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaChequeEmitido: TfrmConsultaChequeEmitido;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmConsultaChequeEmitido.FormShow(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT nCdContaBancaria                                                          ') ;
  qryAux.SQL.Add('  FROM ContaBancaria                                                             ') ;
  qryAux.SQL.Add(' WHERE nCdEmpresa       = ' + IntToStr(frmMenu.nCdEmpresaAtiva)                   ) ;
  qryAux.SQL.Add('   AND cFlgEmiteCheque  = 1                                                      ') ;
  qryAux.SQL.Add('   AND EXISTS(SELECT 1                                                           ') ;
  qryAux.SQL.Add('                FROM UsuarioContaBancaria UCB                                    ') ;
  qryAux.SQL.Add('               WHERE UCB.nCdContaBancaria = ContaBancaria.nCdContaBancaria       ') ;
  qryAux.SQL.Add('                 AND UCB.nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ')') ;
  qryAux.Open ;

  if qryAux.Eof then
  begin
      qryAux.Close ;
      MensagemAlerta('Nenhuma conta banc�ria que movimente cheque vinculada para este usu�rio.') ;
      exit ;
  end ;

  if (qryAux.RecordCount = 1) then
  begin

      qryContaBancaria.Close ;
      Posicionaquery(qryContaBancaria, qryAux.FieldList[0].AsString) ;
      MaskEdit1.Text := qryAux.FieldList[0].AsString ;

  end ;

  qryAux.Close ;

  MaskEdit1.SetFocus ;

end;

procedure TfrmConsultaChequeEmitido.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (not qryContaBancaria.Active) or (qryContaBancaria.Eof) then
  begin
      MensagemAlerta('Selecione uma conta banc�ria.') ;
      MaskEdit1.SetFocus;
      exit ;
  end ;

  if (Trim(MaskEdit2.Text) = '/  /') then
  begin
      MaskEdit2.Text := DateToStr(Now()-60) ;
      MaskEdit3.Text := DateToStr(Now()) ;
  end ;

  if (Trim(MaskEdit4.Text) = '/  /') then
  begin
      MaskEdit4.Text := '01/01/1900' ;
      MaskEdit5.Text := '01/01/1900' ;
  end ;

  qryCheque.Close ;
  qryCheque.Parameters.ParamByName('nCdContaBancaria').Value := qryContaBancarianCdContaBancaria.Value ;
  qryCheque.Parameters.ParamByName('dDtInicial').Value       := frmMenu.ConvData(MaskEdit2.Text) ;
  qryCheque.Parameters.ParamByName('dDtFinal').Value         := frmMenu.ConvData(MaskEdit3.Text) ;
  qryCheque.Parameters.ParamByName('dDtInicialDep').Value    := frmMenu.ConvData(MaskEdit4.Text) ;
  qryCheque.Parameters.ParamByName('dDtFinalDep').Value      := frmMenu.ConvData(MaskEdit5.Text) ;

  if (RadioGroup1.ItemIndex = 0) then
      qryCheque.Parameters.ParamByName('cStatus').Value := 'E' ;

  if (RadioGroup1.ItemIndex = 1) then
      qryCheque.Parameters.ParamByName('cStatus').Value := 'C' ;

  if (RadioGroup1.ItemIndex = 2) then
      qryCheque.Parameters.ParamByName('cStatus').Value := 'T' ;

  qryCheque.Open ;

  if (qryCheque.Eof) then
  begin
      ShowMessage('Nenhum cheque encontrado.') ;
  end ;

end;

procedure TfrmConsultaChequeEmitido.ToolButton2Click(Sender: TObject);
begin
  inherited;

  qryCheque.Close ;
  qryContaBancaria.Close ;
  
end;

procedure TfrmConsultaChequeEmitido.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(107);

        If (nPK > 0) then
        begin
            MaskEdit1.Text := IntToStr(nPK) ;
            PosicionaQuery(qryContaBancaria, MaskEdit1.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaChequeEmitido.MaskEdit1Exit(Sender: TObject);
begin
  inherited;
  qryContaBancaria.Close ;
  PosicionaQuery(qryContaBancaria, MaskEdit1.Text) ;

end;

initialization
    RegisterClass(TfrmConsultaChequeEmitido) ;

end.
