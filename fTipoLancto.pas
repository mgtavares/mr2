unit fTipoLancto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask;

type
  TfrmTipoLancto = class(TfrmCadastro_Padrao)
    qryMasternCdTipoLancto: TIntegerField;
    qryMastercNmTipoLancto: TStringField;
    qryMastercSinalOper: TStringField;
    qryMastercFlgPermManual: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    Label4: TLabel;
    qryMastercFlgEmiteECF: TIntegerField;
    DBCheckBox2: TDBCheckBox;
    qryMasternCdPlanoConta: TIntegerField;
    qryMastercFlgContaBancaria: TIntegerField;
    qryMastercFlgContaCaixa: TIntegerField;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    qryPlanoConta: TADOQuery;
    qryPlanoContanCdPlanoConta: TIntegerField;
    qryPlanoContacNmPlanoConta: TStringField;
    DBEdit5: TDBEdit;
    DataSource1: TDataSource;
    qryMastercFlgPermDA: TIntegerField;
    DBCheckBox5: TDBCheckBox;
    qryMastercFlgApurarDRE: TIntegerField;
    DBCheckBox6: TDBCheckBox;
    qryMastercFlgCredNaoIdent: TIntegerField;
    DBCheckBox7: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit4Exit(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoLancto: TfrmTipoLancto;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmTipoLancto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOLANCTO' ;
  nCdTabelaSistema  := 59 ;
  nCdConsultaPadrao := 130 ;
end;

procedure TfrmTipoLancto.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmTipoLancto.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (DbEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o.') ;
      DbEdit2.SetFocus;
      abort ;
  end ;

  if (DbEdit3.Text <> '+') and (DBedit3.Text <> '-') and (DbEdit3.Text <> '#') then
  begin
      MensagemAlerta('Sinal de opera��o inv�lido ou n�o informado.') ;
      DbEdit3.SetFocus;
      abort ;
  end ;

  if (qryMastercFlgApurarDRE.Value = 1) and (DBEdit5.Text = '') then
  begin
      MensagemAlerta('Informe a conta cont�bil para apropria��o no DRE.') ;
      DBEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMastercFlgPermManual.Value = 1) and (DBEdit5.Text = '') then
  begin
      MensagemAlerta('Para lan�amentos permitidos manualmente a informa��o da conta cont�bil � obrigat�ria.') ;
      DBEdit4.SetFocus ;
      abort ;
  end ;

end;

procedure TfrmTipoLancto.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryPlanoConta.Close ;
  PosicionaQuery(qryPlanoConta, DBEdit4.Text) ;
end;

procedure TfrmTipoLancto.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryPlanoConta.Close ;

end;

procedure TfrmTipoLancto.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryPlanoConta.Close ;

end;

procedure TfrmTipoLancto.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryPlanoConta.Close ;
  PosicionaQuery(qryPlanoConta, qryMasternCdPlanoConta.AsString) ;

end;

procedure TfrmTipoLancto.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(41);

            If (nPK > 0) then
            begin
                qryMasternCdPlanoConta.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmTipoLancto) ;
    
end.
