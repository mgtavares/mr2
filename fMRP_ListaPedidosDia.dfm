inherited frmMRP_ListaPedidoDia: TfrmMRP_ListaPedidoDia
  Left = 66
  Top = 193
  Width = 925
  Caption = 'MRP - Listagem dos Pedidos do Dia'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 909
  end
  inherited ToolBar1: TToolBar
    Width = 909
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 909
    Height = 435
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    LookAndFeel.NativeStyle = False
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsPedidos
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OnCellDblClick = cxGrid1DBTableView1CellDblClick
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLineColor = clSilver
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Header = frmMenu.Header
      object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
        Caption = 'Pedido'
        DataBinding.FieldName = 'nCdPedido'
      end
      object cxGrid1DBTableView1dDtPedido: TcxGridDBColumn
        Caption = 'Data Pedido'
        DataBinding.FieldName = 'dDtPedido'
      end
      object cxGrid1DBTableView1cNmLoja: TcxGridDBColumn
        Caption = 'Loja'
        DataBinding.FieldName = 'cNmLoja'
        Width = 117
      end
      object cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn
        Caption = 'Tipo de Pedido'
        DataBinding.FieldName = 'cNmTipoPedido'
        Width = 155
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro Fornecedor'
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 311
      end
      object cxGrid1DBTableView1nValPedido: TcxGridDBColumn
        Caption = 'Valor do Pedido'
        DataBinding.FieldName = 'nValPedido'
        Width = 121
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Pedidos_Novos'#39') IS NULL)'
      'BEGIN'
      ''
      #9#9'SELECT TOP 0 Pedido.nCdPedido'
      '        '#9#9#9'  ,Terceiro.cNmTerceiro'
      '                ,Pedido.nCdLoja'
      #9#9'  INTO #Temp_Pedidos_Novos'
      #9#9'  FROM Pedido'
      
        '  '#9#9#9'   INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Pedido.nCd' +
        'Terceiro'
      ''
      'END'
      ''
      ''
      'SELECT Temp.nCdPedido'
      '      ,Pedido.dDtPedido'
      '      ,Loja.cNmLoja'
      '      ,TipoPedido.cNmTipoPedido'
      '      ,Terceiro.cNmTerceiro'
      '      ,Pedido.nValPedido'
      '  FROM #Temp_Pedidos_Novos Temp'
      
        '       INNER JOIN Pedido     ON Pedido.nCdPedido         = Temp.' +
        'nCdPedido'
      
        '       LEFT  JOIN Loja       ON Loja.nCdLoja             = Pedid' +
        'o.nCdLoja'
      
        '       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedid' +
        'o.nCdTipoPedido'
      
        '       INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Pedid' +
        'o.nCdTerceiro'
      ' ORDER BY Temp.nCdPedido')
    Left = 208
    Top = 184
    object qryPedidonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryPedidodDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object qryPedidocNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryPedidocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryPedidonValPedido: TBCDField
      FieldName = 'nValPedido'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsPedidos: TDataSource
    DataSet = qryPedido
    Left = 256
    Top = 184
  end
end
