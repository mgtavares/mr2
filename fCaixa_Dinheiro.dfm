object frmCaixa_Dinheiro: TfrmCaixa_Dinheiro
  Left = 307
  Top = 225
  Width = 256
  Height = 247
  BorderIcons = [biSystemMenu]
  Caption = 'Pagamento'
  Color = clWhite
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  PixelsPerInch = 96
  TextHeight = 13
  object cxLabel1: TcxLabel
    Left = 108
    Top = 88
    Width = 125
    Height = 21
    Caption = 'Valor do Pagamento'
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clBlue
    Style.Font.Height = -13
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
  end
  object edtDinheiro: TcxCurrencyEdit
    Left = 16
    Top = 112
    Width = 217
    Height = 49
    AutoSize = False
    EditValue = 0.000000000000000000
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.Alignment.Vert = taBottomJustify
    Properties.ValidateOnEnter = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -27
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    TabOrder = 1
    OnKeyPress = edtDinheiroKeyPress
  end
  object cxButton1: TcxButton
    Left = 152
    Top = 168
    Width = 83
    Height = 33
    Caption = 'Confirmar'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = cxButton1Click
    LookAndFeel.NativeStyle = True
  end
  object edtSaldo: TcxCurrencyEdit
    Left = 15
    Top = 32
    Width = 217
    Height = 49
    AutoSize = False
    EditValue = 0.000000000000000000
    Enabled = False
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.Alignment.Vert = taBottomJustify
    Properties.ValidateOnEnter = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -27
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
    TabOrder = 3
    OnKeyPress = edtDinheiroKeyPress
  end
  object cxLabel2: TcxLabel
    Left = 148
    Top = 8
    Width = 84
    Height = 21
    Caption = 'Valor a Pagar'
    ParentFont = False
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clBlue
    Style.Font.Height = -13
    Style.Font.Name = 'Segoe UI'
    Style.Font.Style = []
  end
end
