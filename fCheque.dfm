inherited frmCheque: TfrmCheque
  Left = 57
  Top = 65
  Caption = 'Cheque'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 71
    Top = 36
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label3: TLabel [2]
    Left = 615
    Top = 135
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'm. Cheque'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [3]
    Left = 19
    Top = 160
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'CNPJ/CPF Emissor'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [4]
    Left = 39
    Top = 185
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Cheque'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [5]
    Left = 6
    Top = 110
    Width = 103
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente Respons'#225'vel'
    FocusControl = DBEdit6
  end
  object Label8: TLabel [6]
    Left = 64
    Top = 210
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Portador'
    FocusControl = DBEdit8
  end
  object Label10: TLabel [7]
    Left = 233
    Top = 185
    Width = 100
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data para Dep'#243'sito'
    FocusControl = DBEdit10
  end
  object Label7: TLabel [8]
    Left = 66
    Top = 60
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit12
  end
  object Label13: TLabel [9]
    Left = 88
    Top = 85
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit14
  end
  object Label12: TLabel [10]
    Left = 77
    Top = 135
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Banco'
    FocusControl = DBEdit16
  end
  object Label14: TLabel [11]
    Left = 192
    Top = 135
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ag'#234'ncia'
    FocusControl = DBEdit17
  end
  object Label15: TLabel [12]
    Left = 302
    Top = 135
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta'
    FocusControl = DBEdit18
  end
  object Label16: TLabel [13]
    Left = 464
    Top = 135
    Width = 15
    Height = 13
    Alignment = taRightJustify
    Caption = 'DV'
    FocusControl = DBEdit19
  end
  object Label19: TLabel [14]
    Left = 77
    Top = 235
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit26
  end
  object Label23: TLabel [15]
    Left = 49
    Top = 260
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observa'#231#227'o'
    FocusControl = DBEdit28
  end
  inherited ToolBar2: TToolBar
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited btSalvar: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [17]
    Tag = 1
    Left = 112
    Top = 30
    Width = 65
    Height = 19
    DataField = 'nCdCheque'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit3: TDBEdit [18]
    Tag = 1
    Left = 688
    Top = 129
    Width = 65
    Height = 19
    DataField = 'iNrCheque'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit4: TDBEdit [19]
    Tag = 1
    Left = 112
    Top = 154
    Width = 105
    Height = 19
    DataField = 'cCNPJCPF'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit5: TDBEdit [20]
    Tag = 1
    Left = 112
    Top = 179
    Width = 105
    Height = 19
    DataField = 'nValCheque'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit6: TDBEdit [21]
    Tag = 1
    Left = 112
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdTerceiroResp'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit7: TDBEdit [22]
    Tag = 1
    Left = 184
    Top = 104
    Width = 569
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit8: TDBEdit [23]
    Tag = 1
    Left = 112
    Top = 204
    Width = 641
    Height = 19
    DataField = 'cNmTerceiroPort'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit10: TDBEdit [24]
    Tag = 1
    Left = 336
    Top = 179
    Width = 81
    Height = 19
    DataField = 'dDtDeposito'
    DataSource = dsMaster
    TabOrder = 8
  end
  object btChequeDevolv: TcxButton [25]
    Left = 512
    Top = 160
    Width = 121
    Height = 25
    Caption = 'Registrar Devolu'#231#227'o'
    TabOrder = 9
    Visible = False
    OnClick = btChequeDevolvClick
    LookAndFeel.Kind = lfOffice11
  end
  object DBEdit12: TDBEdit [26]
    Tag = 1
    Left = 112
    Top = 54
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit13: TDBEdit [27]
    Tag = 1
    Left = 184
    Top = 54
    Width = 569
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit14: TDBEdit [28]
    Tag = 1
    Left = 112
    Top = 79
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit15: TDBEdit [29]
    Tag = 1
    Left = 184
    Top = 79
    Width = 569
    Height = 19
    DataField = 'cNmLoja'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit16: TDBEdit [30]
    Tag = 1
    Left = 112
    Top = 129
    Width = 65
    Height = 19
    DataField = 'nCdBanco'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit17: TDBEdit [31]
    Tag = 1
    Left = 240
    Top = 129
    Width = 52
    Height = 19
    DataField = 'cAgencia'
    DataSource = dsMaster
    TabOrder = 15
  end
  object DBEdit18: TDBEdit [32]
    Tag = 1
    Left = 336
    Top = 129
    Width = 121
    Height = 19
    DataField = 'cConta_1'
    DataSource = dsMaster
    TabOrder = 16
  end
  object DBEdit19: TDBEdit [33]
    Tag = 1
    Left = 483
    Top = 129
    Width = 17
    Height = 19
    DataField = 'cDigito'
    DataSource = dsMaster
    TabOrder = 17
  end
  object GroupBox1: TGroupBox [34]
    Left = 16
    Top = 279
    Width = 177
    Height = 97
    Caption = 'Registro de Devolu'#231#227'o'
    TabOrder = 18
    object Label11: TLabel
      Left = 16
      Top = 28
      Width = 68
      Height = 13
      Caption = 'Data Primeira'
      FocusControl = DBEdit11
    end
    object Label2: TLabel
      Left = 11
      Top = 60
      Width = 73
      Height = 13
      Caption = 'Data Segunda'
      FocusControl = DBEdit2
    end
    object DBEdit11: TDBEdit
      Tag = 1
      Left = 88
      Top = 22
      Width = 73
      Height = 19
      DataField = 'dDtDevol'
      DataSource = dsMaster
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 88
      Top = 54
      Width = 73
      Height = 19
      DataField = 'dDtSegDevol'
      DataSource = dsMaster
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox [35]
    Left = 208
    Top = 279
    Width = 545
    Height = 97
    Caption = 'Informa'#231#245'es do Movimento de Caixa'
    TabOrder = 19
    object Label9: TLabel
      Left = 10
      Top = 22
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Movimento Caixa'
      FocusControl = DBEdit9
    end
    object Label17: TLabel
      Left = 192
      Top = 22
      Width = 85
      Height = 13
      Caption = 'Data Movimento'
      FocusControl = DBEdit20
    end
    object Label18: TLabel
      Left = 71
      Top = 46
      Width = 27
      Height = 13
      Alignment = taRightJustify
      Caption = 'Caixa'
      FocusControl = DBEdit21
    end
    object Label20: TLabel
      Left = 388
      Top = 22
      Width = 62
      Height = 13
      Alignment = taRightJustify
      Caption = 'Lan'#231'amento'
      FocusControl = DBEdit23
    end
    object Label21: TLabel
      Left = 48
      Top = 70
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Operador'
      FocusControl = DBEdit24
    end
    object DBEdit9: TDBEdit
      Tag = 1
      Left = 104
      Top = 16
      Width = 65
      Height = 19
      DataField = 'nCdResumoCaixa'
      DataSource = dsMaster
      TabOrder = 0
    end
    object DBEdit20: TDBEdit
      Tag = 1
      Left = 280
      Top = 16
      Width = 89
      Height = 19
      DataField = 'dDtAbertura'
      DataSource = dsMaster
      TabOrder = 1
    end
    object DBEdit21: TDBEdit
      Tag = 1
      Left = 104
      Top = 40
      Width = 65
      Height = 19
      DataField = 'nCdContaBancaria'
      DataSource = dsMaster
      TabOrder = 2
    end
    object DBEdit22: TDBEdit
      Tag = 1
      Left = 176
      Top = 40
      Width = 195
      Height = 19
      DataField = 'nCdConta'
      DataSource = dsMaster
      TabOrder = 3
    end
    object DBEdit23: TDBEdit
      Tag = 1
      Left = 456
      Top = 16
      Width = 81
      Height = 19
      DataField = 'nCdLanctoFin'
      DataSource = dsMaster
      TabOrder = 4
    end
    object DBEdit24: TDBEdit
      Tag = 1
      Left = 104
      Top = 64
      Width = 65
      Height = 19
      DataField = 'nCdUsuario'
      DataSource = dsMaster
      TabOrder = 5
    end
    object DBEdit25: TDBEdit
      Tag = 1
      Left = 176
      Top = 64
      Width = 281
      Height = 19
      DataField = 'cNmUsuario'
      DataSource = dsMaster
      TabOrder = 6
    end
  end
  object DBEdit26: TDBEdit [36]
    Tag = 1
    Left = 112
    Top = 229
    Width = 65
    Height = 19
    DataField = 'nCdTabStatusCheque'
    DataSource = dsMaster
    TabOrder = 20
  end
  object DBEdit27: TDBEdit [37]
    Tag = 1
    Left = 184
    Top = 229
    Width = 569
    Height = 19
    DataField = 'cNmTabStatusCheque'
    DataSource = dsMaster
    TabOrder = 21
  end
  object GroupBox3: TGroupBox [38]
    Left = 760
    Top = 31
    Width = 225
    Height = 345
    Caption = 'Altera'#231#227'o de Status'
    Enabled = False
    TabOrder = 22
    object cxButton1: TcxButton
      Left = 8
      Top = 16
      Width = 201
      Height = 30
      Caption = 'Dispon'#237'vel para Dep'#243'sito'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = cxButton1Click
      LookAndFeel.NativeStyle = True
    end
    object cxButton2: TcxButton
      Left = 8
      Top = 88
      Width = 201
      Height = 30
      Caption = 'Negociado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = cxButton2Click
      LookAndFeel.NativeStyle = True
    end
    object cxButton3: TcxButton
      Left = 8
      Top = 125
      Width = 201
      Height = 30
      Caption = 'Primeira Devolu'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = cxButton3Click
      LookAndFeel.NativeStyle = True
    end
    object cxButton4: TcxButton
      Left = 8
      Top = 161
      Width = 201
      Height = 30
      Caption = 'Segunda Devolu'#231#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = cxButton4Click
      LookAndFeel.NativeStyle = True
    end
    object cxButton5: TcxButton
      Left = 8
      Top = 197
      Width = 201
      Height = 30
      Caption = 'Devolvido para Respons'#225'vel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 4
      OnClick = cxButton5Click
      LookAndFeel.NativeStyle = True
    end
    object cxButton6: TcxButton
      Left = 8
      Top = 234
      Width = 201
      Height = 30
      Caption = 'Enviado para Loja'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 5
      OnClick = cxButton6Click
      LookAndFeel.NativeStyle = True
    end
    object cxButton7: TcxButton
      Left = 8
      Top = 270
      Width = 201
      Height = 30
      Caption = 'Enviado para Protesto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 6
      OnClick = cxButton7Click
      LookAndFeel.NativeStyle = True
    end
    object cxButton8: TcxButton
      Left = 8
      Top = 307
      Width = 201
      Height = 30
      Caption = 'Trocar Portador'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 7
      OnClick = cxButton8Click
      LookAndFeel.NativeStyle = True
    end
    object cxButton10: TcxButton
      Left = 8
      Top = 52
      Width = 201
      Height = 30
      Caption = 'Compensado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 8
      OnClick = cxButton10Click
      LookAndFeel.NativeStyle = True
    end
  end
  object cxPageControl1: TcxPageControl [39]
    Left = 16
    Top = 382
    Width = 969
    Height = 289
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 23
    ClientRectBottom = 285
    ClientRectLeft = 4
    ClientRectRight = 965
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Hist'#243'rico de Status'
      ImageIndex = 0
      object Label22: TLabel
        Tag = 1
        Left = 712
        Top = 13
        Width = 60
        Height = 13
        Caption = 'Observa'#231#227'o'
        FocusControl = DBMemo1
      end
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 1
        Width = 705
        Height = 256
        DataGrouping.GroupLevels = <>
        DataSource = dsHistStatusCheque
        Flat = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Segoe UI'
        Font.Style = []
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        ParentFont = False
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Consolas'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdHistStatusCheque'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtStatus'
            Footers = <>
            Width = 129
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabStatusCheque'
            Footers = <>
            Width = 33
          end
          item
            EditButtons = <>
            FieldName = 'cNmTabStatusCheque'
            Footers = <>
            Width = 260
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Width = 31
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            Width = 214
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBMemo1: TDBMemo
        Left = 712
        Top = 32
        Width = 241
        Height = 225
        DataField = 'cOBS'
        DataSource = dsHistStatusCheque
        TabOrder = 1
        OnKeyPress = DBMemo1KeyPress
      end
      object cxButton9: TcxButton
        Left = 878
        Top = 1
        Width = 75
        Height = 25
        Caption = 'Gravar'
        Enabled = False
        TabOrder = 2
        OnClick = cxButton9Click
        Glyph.Data = {
          E6010000424DE60100000000000036000000280000000C0000000C0000000100
          180000000000B001000000000000000000000000000000000000CCCCCCAAA39F
          6D6C6B656464575D5E6564646564646564646D6C6B656464898A89C6C6C6B6B6
          B63D42420405065157577172726D6C6B575D5E515757191D23191D2331333289
          8A89484E4E0599CF092369DAD2C9FCFEFEFCFEFEEEEEEED5D5D5484E4E0599CF
          0923695157573D424231CDFD09236991846EB3AD9EAAA39F93929291846E3133
          320599CF0923695157573D424231CDFD10708209236909236909236909236909
          23691070820599CF0923695157573D424231CDFD0599CF107082092369092369
          092369092369479EC00599CF0923695157573D424231CDFD092369B7A68ACCC5
          C0C2BEB9C2BEB9DBC8C35157570599CF0923695157573D424231CDFD092369CB
          B9B1E6E6E6DEDEDEDEDEDEEEEEEA5157570599CF0923695157573D424231CDFD
          092369C2B9AEDEDEDEDADDD7DADDD7E6E6E65157570599CF0923695157573D42
          4231CDFD092369B8B1AADEDEDED5D5D5D5D5D5E6E6E6484E4E0599CF09236951
          57573D424231CDFD092369DAD2C9FCFEFEF9FAFAF9FAFAFCFEFE575D5E0599CF
          107082515757777B7B3D4242191D23484E4E575D5E515757515757575D5E191D
          23191D23575D5EA4A8A8}
        LookAndFeel.NativeStyle = True
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Follow Up'
      ImageIndex = 1
      object DBGridEh5: TDBGridEh
        Left = 0
        Top = 0
        Width = 728
        Height = 261
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = DataSource13
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'dDtFollowUp'
            Footers = <>
            Width = 110
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            Width = 128
          end
          item
            EditButtons = <>
            FieldName = 'cOcorrenciaResum'
            Footers = <>
            Width = 323
          end
          item
            EditButtons = <>
            FieldName = 'dDtProxAcao'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBMemo2: TDBMemo
        Left = 728
        Top = 0
        Width = 233
        Height = 261
        Align = alRight
        DataField = 'cOcorrencia'
        DataSource = DataSource13
        Enabled = False
        TabOrder = 1
      end
    end
  end
  object DBEdit28: TDBEdit [40]
    Tag = 1
    Left = 112
    Top = 254
    Width = 641
    Height = 19
    DataField = 'cOBSCheque'
    DataSource = dsMaster
    TabOrder = 24
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Cheque.nCdCheque'
      
        '      ,dbo.fn_ZeroEsquerda(Cheque.nCdBanco,3) + '#39' '#39' + dbo.fn_Zer' +
        'oEsquerda(Convert(VARCHAR,Cheque.cAgencia),4) + '#39' '#39' + Cheque.cCo' +
        'nta + '#39'-'#39' + Cheque.cDigito as cConta'
      '      ,Cheque.iNrCheque     '
      #9'  ,Cheque.cCNPJCPF      '
      #9'  ,Cheque.nValCheque    '
      #9'  ,dbo.fn_OnlyDate(Cheque.dDtDeposito) dDtDeposito   '
      '      ,Cheque.nCdTerceiroResp    '
      #9'  ,Terc1.cNmTerceiro    '
      
        #9'  ,CASE WHEN Terc2.cNmTerceiro IS NULL THEN CASE WHEN ContaBanc' +
        'aria.cFlgCaixa = 0 THEN dbo.fn_ZeroEsquerda(ContaBancaria.nCdBan' +
        'co,3) + '#39'-'#39' + Convert(VARCHAR,ContaBancaria.cAgencia) + '#39' - '#39' + ' +
        'ContaBancaria.nCdConta + '#39'  '#39' + IsNull(ContaBancaria.cNmTitular,' +
        #39' '#39') '
      
        '                                                     ELSE ContaB' +
        'ancaria.nCdConta'
      '                                                END'
      
        #9#9#9'ELSE dbo.fn_ZeroEsquerda(Terc2.nCdTerceiro,6) + '#39' - '#39' + Terc2' +
        '.cNmTerceiro '
      #9'   END cNmTerceiroPort         '
      #9'  ,dbo.fn_OnlyDate(Cheque.dDtRemessaPort) dDtRemessaPort '
      '      ,dDtDevol'
      '      ,nCdTabTipoCheque'
      '      ,Cheque.nCdBanco'
      '      ,Cheque.cAgencia'
      '      ,Cheque.cConta'
      '      ,Cheque.cDigito'
      '      ,Loja.nCdLoja'
      '      ,Loja.cNmLoja'
      '      ,Cheque.nCdLanctoFin'
      '      ,ResumoCaixa.nCdResumoCaixa'
      '      ,ResumoCaixa.dDtAbertura'
      '      ,Caixa.nCdContaBancaria'
      '      ,Caixa.nCdConta'
      '      ,Usuario.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '      ,Empresa.nCdEmpresa'
      '      ,Empresa.cNmEmpresa'
      '      ,Cheque.dDtSegDevol'
      '      ,TabStatusCheque.nCdTabStatusCheque'
      '      ,TabStatusCheque.cNmTabStatusCheque'
      '      ,Cheque.dDtStatus'
      '      ,Cheque.cOBSCheque'
      '  FROM Cheque'
      
        '       INNER JOIN Empresa             ON Empresa.nCdEmpresa     ' +
        '        = Cheque.nCdEmpresa'
      
        '       INNER JOIN TabStatusCheque     ON TabStatusCheque.nCdTabS' +
        'tatusCheque = Cheque.nCdTabStatusCheque'
      
        '       LEFT  JOIN Terceiro Terc1      ON Terc1.nCdTerceiro      ' +
        '        = Cheque.nCdTerceiroResp'
      
        #9'   LEFT  JOIN Terceiro Terc2      ON Terc2.nCdTerceiro         ' +
        '     = Cheque.nCdTerceiroPort'
      
        '       LEFT  JOIN ContaBancaria       ON ContaBancaria.nCdContaB' +
        'ancaria = Cheque.nCdContaBancariaDep'
      
        '       LEFT  JOIN LanctoFin           ON LanctoFin.nCdLanctoFin ' +
        '        = Cheque.nCdLanctoFin'
      
        '       LEFT  JOIN ResumoCaixa         ON ResumoCaixa.nCdResumoCa' +
        'ixa     = LanctoFin.nCdResumoCaixa'
      
        '       LEFT  JOIN Loja                ON Loja.nCdLoja           ' +
        '        = ResumoCaixa.nCdLoja'
      
        '       LEFT  JOIN Usuario             ON Usuario.nCdUsuario     ' +
        '        = LanctoFin.nCdUsuario'
      
        '       LEFT  JOIN ContaBancaria Caixa ON Caixa.nCdContaBancaria ' +
        '        = ResumoCaixa.nCdContaBancaria'
      ' WHERE Cheque.nCdCheque = :nPK'
      '   AND Cheque.nCdTabTipoCheque = 2')
    Left = 1064
    Top = 240
    object qryMasternCdCheque: TAutoIncField
      FieldName = 'nCdCheque'
      ReadOnly = True
    end
    object qryMastercConta: TStringField
      FieldName = 'cConta'
      ReadOnly = True
      Size = 49
    end
    object qryMasteriNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryMastercCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryMasternValCheque: TBCDField
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtDeposito: TDateTimeField
      FieldName = 'dDtDeposito'
      ReadOnly = True
    end
    object qryMasternCdTerceiroResp: TIntegerField
      FieldName = 'nCdTerceiroResp'
    end
    object qryMastercNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMastercNmTerceiroPort: TStringField
      FieldName = 'cNmTerceiroPort'
      ReadOnly = True
      Size = 50
    end
    object qryMasterdDtRemessaPort: TDateTimeField
      FieldName = 'dDtRemessaPort'
      ReadOnly = True
    end
    object qryMasterdDtDevol: TDateTimeField
      FieldName = 'dDtDevol'
    end
    object qryMasternCdTabTipoCheque: TIntegerField
      FieldName = 'nCdTabTipoCheque'
    end
    object qryMasternCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryMastercAgencia: TStringField
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryMastercConta_1: TStringField
      FieldName = 'cConta_1'
      FixedChar = True
      Size = 15
    end
    object qryMastercDigito: TStringField
      FieldName = 'cDigito'
      FixedChar = True
      Size = 1
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMastercNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryMasternCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMasternCdResumoCaixa: TAutoIncField
      FieldName = 'nCdResumoCaixa'
      ReadOnly = True
    end
    object qryMasterdDtAbertura: TDateTimeField
      FieldName = 'dDtAbertura'
    end
    object qryMasternCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryMasternCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryMasternCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryMastercNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMastercNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryMasterdDtSegDevol: TDateTimeField
      FieldName = 'dDtSegDevol'
    end
    object qryMasternCdTabStatusCheque: TIntegerField
      FieldName = 'nCdTabStatusCheque'
    end
    object qryMastercNmTabStatusCheque: TStringField
      FieldName = 'cNmTabStatusCheque'
      Size = 50
    end
    object qryMasterdDtStatus: TDateTimeField
      FieldName = 'dDtStatus'
    end
    object qryMastercOBSCheque: TStringField
      FieldName = 'cOBSCheque'
      Size = 100
    end
  end
  inherited dsMaster: TDataSource
    Left = 1056
    Top = 176
  end
  inherited qryID: TADOQuery
    Left = 1032
    Top = 336
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 1032
    Top = 200
  end
  inherited qryStat: TADOQuery
    Left = 1024
    Top = 128
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 1096
    Top = 256
  end
  inherited ImageList1: TImageList
    Left = 344
    Top = 312
  end
  object qryHistStatusCheque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Hist.nCdHistStatusCheque'
      '      ,TabStatusCheque.nCdTabStatusCheque'
      '      ,TabStatusCheque.cNmTabStatusCheque'
      '      ,Hist.dDtStatus'
      '      ,Usuario.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '      ,Hist.cOBS'
      '  FROM HistStatusCheque Hist'
      
        '       INNER JOIN TabStatusCheque ON TabStatusCheque.nCdTabStatu' +
        'sCheque = Hist.nCdTabStatusCheque'
      
        '       LEFT  JOIN Usuario         ON Usuario.nCdUsuario         ' +
        '        = Hist.nCdUsuario'
      ' WHERE Hist.nCdCheque = :nPK'
      ' ORDER BY dDtStatus DESC')
    Left = 1024
    Top = 88
    object qryHistStatusChequenCdHistStatusCheque: TIntegerField
      FieldName = 'nCdHistStatusCheque'
    end
    object qryHistStatusChequenCdTabStatusCheque: TIntegerField
      DisplayLabel = 'Status|C'#243'd'
      FieldName = 'nCdTabStatusCheque'
    end
    object qryHistStatusChequecNmTabStatusCheque: TStringField
      DisplayLabel = 'Status|Descri'#231#227'o'
      FieldName = 'cNmTabStatusCheque'
      Size = 50
    end
    object qryHistStatusChequedDtStatus: TDateTimeField
      DisplayLabel = 'Data|Status'
      FieldName = 'dDtStatus'
    end
    object qryHistStatusChequenCdUsuario: TIntegerField
      DisplayLabel = 'Usu'#225'rio Respons'#225'vel|C'#243'd'
      FieldName = 'nCdUsuario'
    end
    object qryHistStatusChequecNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rio Respons'#225'vel|Nome Usu'#225'rio'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryHistStatusChequecOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
  end
  object dsHistStatusCheque: TDataSource
    DataSet = qryHistStatusCheque
    Left = 1072
    Top = 96
  end
  object qryInseriHistorico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM HistStatusCheque')
    Left = 1016
    Top = 384
    object qryInseriHistoriconCdHistStatusCheque: TAutoIncField
      FieldName = 'nCdHistStatusCheque'
    end
    object qryInseriHistoriconCdCheque: TIntegerField
      FieldName = 'nCdCheque'
    end
    object qryInseriHistoriconCdTabStatusCheque: TIntegerField
      FieldName = 'nCdTabStatusCheque'
    end
    object qryInseriHistoriconCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryInseriHistoricodDtStatus: TDateTimeField
      FieldName = 'dDtStatus'
    end
    object qryInseriHistoricocOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
  end
  object DataSource13: TDataSource
    DataSet = qryFollowUp
    Left = 1024
    Top = 456
  end
  object qryFollowUp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT dDtFollowUp'
      '      ,cNmUsuario'
      '      ,cOcorrenciaResum'
      '      ,dDtProxAcao'
      ',cOcorrencia'
      '  FROM FollowUp'
      
        '       INNER JOIN Usuario ON Usuario.nCdUsuario = FollowUp.nCdUs' +
        'uario'
      ' WHERE nCdCheque = :nPK'
      'ORDER BY dDtFollowUp DESC')
    Left = 1040
    Top = 488
    object qryFollowUpdDtFollowUp: TDateTimeField
      DisplayLabel = 'Ocorr'#234'ncia|Data'
      FieldName = 'dDtFollowUp'
      ReadOnly = True
    end
    object qryFollowUpcNmUsuario: TStringField
      DisplayLabel = 'Ocorr'#234'ncia|Usu'#225'rio'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryFollowUpcOcorrenciaResum: TStringField
      DisplayLabel = 'Ocorr'#234'ncia|Descri'#231#227'o Resumida'
      FieldName = 'cOcorrenciaResum'
      Size = 50
    end
    object qryFollowUpdDtProxAcao: TDateTimeField
      DisplayLabel = 'Ocorr'#234'ncia|Data Pr'#243'x. A'#231#227'o'
      FieldName = 'dDtProxAcao'
    end
    object qryFollowUpcOcorrencia: TMemoField
      FieldName = 'cOcorrencia'
      BlobType = ftMemo
    end
  end
  object SP_FIN_ATUALIZA_CARTEIRA_TERCEIRO_INDIVIDUAL: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_FIN_ATUALIZA_CARTEIRA_TERCEIRO_INDIVIDUAL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 1016
    Top = 296
  end
end
