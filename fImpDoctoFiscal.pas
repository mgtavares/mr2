unit fImpDoctoFiscal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, Mask,
  DB, StdCtrls, DBCtrls, ADODB, GridsEh, DBGridEh, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGrid, cxPC, Menus,
  cxContainer, cxTextEdit, cxCheckBox, cxImage;

type
  TfrmImpDoctoFiscal = class(TfrmProcesso_Padrao)
    qrySerieFiscal: TADOQuery;
    qrySerieFiscalnCdSerieFiscal: TIntegerField;
    qrySerieFiscalnCdTipoDoctoFiscal: TIntegerField;
    qrySerieFiscalcNmTipoDoctoFiscal: TStringField;
    dsSerieFiscal: TDataSource;
    qryAux: TADOQuery;
    qryDoctoFiscal: TADOQuery;
    dsDoctoFiscal: TDataSource;
    qryDoctoFiscalnCdDoctoFiscal: TAutoIncField;
    qryDoctoFiscaldDtEmissao: TDateTimeField;
    qryDoctoFiscalcCFOP: TStringField;
    qryDoctoFiscalcTextoCFOP: TStringField;
    qryDoctoFiscalcFlgEntSai: TStringField;
    qryDoctoFiscalcNmTerceiro: TStringField;
    qryDoctoFiscalnValTotal: TBCDField;
    qryDoctoFiscalcNmCondPagto: TStringField;
    qryDoctoFiscalcSigla: TStringField;
    sp_imprime_docto: TADOStoredProc;
    qryDoctoFiscalnCdTerceiro: TIntegerField;
    qryDoctoFiscalnCdTerceiroTransp: TIntegerField;
    GroupBox1: TGroupBox;
    MaskEdit3: TMaskEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label1: TLabel;
    Label4: TLabel;
    qryEnvioNFeEmail: TADOQuery;
    qryEnvioNFeEmailcFlgEnviaNFeEmail: TIntegerField;
    qryEnvioNFeEmailcEmailNFe: TStringField;
    qryEnvioNFeEmailcEmailCopiaNFe: TStringField;
    qrySerieFiscalcSerie: TStringField;
    DBEdit3: TDBEdit;
    qrySerieFiscalScan: TADOQuery;
    qrySerieFiscalScannCdSerieFiscal: TIntegerField;
    qryDoctoFiscalcNrProtocoloNFe: TStringField;
    qryDoctoFiscalcNrReciboNFe: TStringField;
    qryDoctoFiscalcChaveNFe: TStringField;
    qryDoctoFiscalAux: TADOQuery;
    qryDoctoFiscalAuxnCdDoctoFiscal: TIntegerField;
    qryDoctoFiscalAuxcNrProtocoloNFe: TStringField;
    qryDoctoFiscalAuxcNrReciboNFe: TStringField;
    qryDoctoFiscalAuxcChaveNFe: TStringField;
    qryDoctoFiscalAuxiNrDocto: TIntegerField;
    qryAtualizaTrackingCode: TADOQuery;
    pageControlDoctoFiscal: TcxPageControl;
    tabResumido: TcxTabSheet;
    tabDetalhado: TcxTabSheet;
    cxGridResumido: TcxGrid;
    cxGridResumidoDBTableView1: TcxGridDBTableView;
    cxGridResumidoDBTableView1nCdDoctoFiscal: TcxGridDBColumn;
    cxGridResumidoDBTableView1dDtEmissao: TcxGridDBColumn;
    cxGridResumidoDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridResumidoDBTableView1nValTotal: TcxGridDBColumn;
    cxGridResumidoDBTableView1cCFOP: TcxGridDBColumn;
    cxGridResumidoDBTableView1cTextoCFOP: TcxGridDBColumn;
    cxGridResumidoDBTableView1cFlgEntSai: TcxGridDBColumn;
    cxGridResumidoDBTableView1cNmCondPagto: TcxGridDBColumn;
    cxGridResumidoDBTableView1cSigla: TcxGridDBColumn;
    cxGridResumidoDBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGridResumidoDBTableView1nCdTerceiroTransp: TcxGridDBColumn;
    cxGridResumidoDBTableView1cNrReciboNFe: TcxGridDBColumn;
    cxGridResumidoLevel1: TcxGridLevel;
    cxGridDetalhado: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryPedidoDoctoFiscal: TADOQuery;
    qryPedidoDoctoFiscalnCdPedido: TIntegerField;
    qryPedidoDoctoFiscalcNmTipoPedido: TStringField;
    qryPedidoDoctoFiscalcNmLoja: TStringField;
    qryPedidoDoctoFiscalnCdTerceiro: TIntegerField;
    qryPedidoDoctoFiscalcNmTerceiro: TStringField;
    qryPedidoDoctoFiscalnCdTerceiroPagador: TIntegerField;
    qryPedidoDoctoFiscalcNmTerceiroPagador: TStringField;
    qryPedidoDoctoFiscalcNmTabStatusPed: TStringField;
    qryPedidoDoctoFiscaldDtPedido: TDateTimeField;
    qryPedidoDoctoFiscaldDtPrevEntIni: TDateTimeField;
    qryPedidoDoctoFiscaldDtPrevEntFim: TDateTimeField;
    qryPedidoDoctoFiscalcAtraso: TStringField;
    qryPedidoDoctoFiscalnValPedido: TBCDField;
    dsPedidoDoctoFiscal: TDataSource;
    cxGridDBTableView1DBColumn_nCdPedido: TcxGridDBColumn;
    cxGridDBTableView1DBColumn_cNmTipoPedido: TcxGridDBColumn;
    cxGridDBTableView1DBColumn_cNmLoja: TcxGridDBColumn;
    cxGridDBTableView1DBColumn_cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView1DBColumn_cNmTerceiroPagador: TcxGridDBColumn;
    cxGridDBTableView1DBColumn_cNmTabStatusPed: TcxGridDBColumn;
    cxGridDBTableView1DBColumn_dDtPedido: TcxGridDBColumn;
    cxGridDBTableView1DBColumn_dDtPrevEntFim: TcxGridDBColumn;
    cxGridDBTableView1DBColumn_cAtraso: TcxGridDBColumn;
    qryEnviaPedidoWebMonitor: TADOQuery;
    qryDoctoFiscalAuxiStatusRetorno: TIntegerField;
    qryDoctoFiscalAuxcNmStatusRetorno: TStringField;
    qryDoctoFiscaliStatusRetorno: TIntegerField;
    qryDoctoFiscalcNmStatusRetorno: TStringField;
    qrySerieFiscalnCdLoja: TIntegerField;
    qryDoctoFiscalnCdLoja: TIntegerField;
    PopupMenu1: TPopupMenu;
    btImpEspelho: TMenuItem;
    qryDoctoEspelho: TADOQuery;
    qryAtualizaDoctoEspenho: TADOQuery;
    qrySerieFiscalLoja: TADOQuery;
    qrySerieFiscalcNmSerieFiscal: TStringField;
    DBEdit4: TDBEdit;
    qryDoctoFiscaliNrDocto: TIntegerField;
    cxGridResumidoDBTableView1iStatusRetorno: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    PreProcessado: TcxStyle;
    Normal: TcxStyle;
    Panel2: TPanel;
    Label2: TLabel;
    qryPedidoDoctoFiscalcFlgEmitirETQFat: TIntegerField;
    qryPedidoDoctoFiscalcTrackingCode: TStringField;
    qryPedidoDoctoFiscalcFlgPedidoWeb: TIntegerField;
    SP_PROCESSA_NUMDOCTOFISCAL: TADOStoredProc;
    qryDoctoFiscalcFlgDocPendente: TIntegerField;
    cxGridResumidoDBTableView1cFlgDocPendente: TcxGridDBColumn;
    Image2: TImage;
    qryDoctoFiscalAuxnCdStatus: TIntegerField;
    qryDoctoFiscaliNrDoctoReserva: TIntegerField;
    cxGridResumidoDBTableView1iNrDoctoReserva: TcxGridDBColumn;
    btGerarArqXML: TMenuItem;
    SaveDialog1: TSaveDialog;
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ImprimeNF();
    procedure EnviaEmail();
    procedure ProcessaNumDocto(nCdSerieFiscal, nCdDoctoFiscal : Integer; cAcao : String);
    procedure Master(Sender: TObject);
    procedure qryDoctoFiscalAfterScroll(DataSet: TDataSet);
    procedure pageControlDoctoFiscalChange(Sender: TObject);
    procedure btImpEspelhoClick(Sender: TObject);
    procedure cxGridResumidoDBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure btGerarArqXMLClick(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  frmImpDoctoFiscal : TfrmImpDoctoFiscal;

implementation

uses fLookup_Padrao, fMenu, fDadoComplDoctoFiscal, fImpDFPadrao, fEmiteNFe2, fModImpETP;

{$R *.dfm}

var
  objImpNFe : TfrmEmiteNfe2;
  objImpNF  : TfrmImpDFPadrao;

procedure TfrmImpDoctoFiscal.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qrySerieFiscal, MaskEdit3.Text) ;
end;

procedure TfrmImpDoctoFiscal.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(82);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySerieFiscal, MaskEdit3.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmImpDoctoFiscal.FormShow(Sender: TObject);
begin
  inherited;

  MaskEdit3.SetFocus ;

  qrySerieFiscal.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;

  qryAux.Close ;
  qryAux.SQL.Add('SELECT nCdSerieFiscal FROM SerieFiscal WHERE cSerie < 900 AND nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva)) ;
  qryAux.Open ;

  if (qryAux.RecordCount = 1) then
  begin
      MaskEdit3.Text := qryAux.FieldList[0].AsString;
      PosicionaQuery(qrySerieFiscal, MaskEdit3.Text) ;

      ToolButton1.Click;
  end ;

  if (qryAux.eof) then
  begin
      ShowMessage('Nenhuma s�rie fiscal configurada para esta empresa.');
  end ;

  frmMenu.qryParametroEmpresa.Close ;
  frmMenu.qryParametroEmpresa.Parameters.ParamByName('cParametro').Value := 'EMITENFE' ;
  frmMenu.qryParametroEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  frmMenu.qryParametroEmpresa.Open ;

  if (not frmMenu.qryParametroEmpresa.eof) then
  begin
      if (frmMenu.LeParametroEmpresa('EMITENFE') = 'S') then
          ShowMessage('Verifique se a impressora padr�o do Windows � a impressora que deseja emitir o DANFE.') ;
  end
  else
  begin
      if (frmMenu.LeParametro('EMITENFE') = 'S') then
          ShowMessage('Verifique se a impressora padr�o do Windows � a impressora que deseja emitir o DANFE.') ;
  end;

  qryAux.Close ;

  pageControlDoctoFiscal.ActivePage := tabResumido;

  MaskEdit3.SetFocus;  
end;

procedure TfrmImpDoctoFiscal.ToolButton1Click(Sender: TObject);
var
  nCdSerieFiscal : Integer;
begin
  inherited;

  if (not qrySerieFiscal.Active) or (qrySerieFiscalnCdSerieFiscal.Value = 0) then
  begin
      ShowMessage('Selecione uma s�rie fiscal.') ;
      MaskEdit3.SetFocus;
      exit ;
  end ;

  nCdSerieFiscal := 0;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
      nCdSerieFiscal := qrySerieFiscalnCdSerieFiscal.Value;

  qryDoctoFiscal.Parameters.ParamByName('nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva;
  qryDoctoFiscal.Parameters.ParamByName('nCdSerieFiscal').Value := nCdSerieFiscal;

  PosicionaQuery(qryDoctoFiscal, qrySerieFiscalnCdTipoDoctoFiscal.AsString) ;
end;

procedure TfrmImpDoctoFiscal.ImprimeNF();
var
    bNotaEletronica : boolean ;
begin

    objImpNFe := TfrmEmiteNfe2.Create( Self );
    objImpNF  := TfrmImpDFPadrao.Create( Self );

    try

        bNotaEletronica := FALSE ;

        if (frmMenu.LeParametro('EMITENFE') = 'S') then
            bNotaEletronica := TRUE ;

        if (not frmMenu.qryParametroEmpresa.eof) then
        begin
            if (frmMenu.LeParametroEmpresa('EMITENFE') = 'N') then
                bNotaEletronica := FALSE ;
        end ;

        try
            {-- agora processa a nota fiscal eletr�nica --}
            if (bNotaEletronica) then
            begin
                objImpNFe.gerarNFe(qryDoctoFiscalnCdDoctoFiscal.Value);

                qryDoctoFiscalAux.Close;
                PosicionaQuery( qryDoctoFiscalAux, qryDoctoFiscalnCdDoctoFiscal.asString ) ;

                if ( qryDoctoFiscalAuxcChaveNFe.Value <> '' ) then
                begin

                    objImpNFe.imprimirDANFe(qryDoctoFiscalnCdDoctoFiscal.Value);

                    { -- ### processo de envio de e-mail ser� enviado atrav�s do m�todo EnviaEmailNF() ### -- }
                    {qryEnvioNFeEmail.Close;
                    qryEnvioNFeEmail.Parameters.ParamByName('nPK').Value := qryDoctoFiscalnCdTerceiro.AsString;
                    qryEnvioNFeEmail.Open;

                    if (qryEnvioNFeEmailcFlgEnviaNFeEmail.Value = 1) then
                    begin
                        case MessageDlg('Desejar enviar a NF-e por e-mail ?',mtConfirmation,[mbYes,mbNo],0) of
                            mrNo:exit ;
                        end ;

                        try
                            objImpNFe.enviaEmailNFe(qryDoctoFiscalnCdDoctoFiscal.Value,qryEnvioNFeEmailcEmailNFe.Value,qryEnvioNFeEmailcEmailCopiaNFe.Value);
                        except
                            MensagemErro('N�o foi poss�vel enviar a NF-e por e-mail, verifique as configura��es de email e/ou sua conex�o.');
                            raise;
                        end;

                        if not(Trim(qryEnvioNFeEmailcEmailCopiaNFe.Value) = '') then
                           ShowMessage('NF-e enviada por e-mail com sucesso'+#13#13+'Email de destino: '+qryEnvioNFeEmailcEmailNFe.Value + ' e ' + qryEnvioNFeEmailcEmailCopiaNFe.Value)
                        else ShowMessage('NF-e enviada por e-mail com sucesso'+#13#13+'Email de destino: '+qryEnvioNFeEmailcEmailNFe.Value) ;

                    end;}
                end;
            end
            else
            begin
                ShowMessage('Posicione o formul�rio na impressora e clique em OK para iniciar a impress�o.') ;

                objImpNF.nCdDoctoFiscal := qryDoctoFiscalnCdDoctoFiscal.Value ;
                objImpNF.ImprimirDocumento;
            end;

            //qryDoctoFiscal.Close ;
            //qryDoctoFiscal.Open ;

        except
            raise;
        end;

    finally

        freeAndNil( objImpNFe ) ;
        freeAndNil( objImpNF ) ;

    end ;

end ;

procedure TfrmImpDoctoFiscal.EnviaEmail();
begin
  objImpNFe := TfrmEmiteNfe2.Create( Self );

  try
      if ( qryDoctoFiscalAuxcChaveNFe.Value <> '' ) then
      begin
          qryEnvioNFeEmail.Close;
          qryEnvioNFeEmail.Parameters.ParamByName('nPK').Value := qryDoctoFiscalnCdTerceiro.AsString;
          qryEnvioNFeEmail.Open;

          if (qryEnvioNFeEmailcFlgEnviaNFeEmail.Value = 1) then
          begin
              case MessageDlg('Desejar enviar a NF-e por e-mail ?',mtConfirmation,[mbYes,mbNo],0) of
                  mrNo:exit ;
              end ;

              try
                  objImpNFe.enviaEmailNFe(qryDoctoFiscalnCdDoctoFiscal.Value,qryEnvioNFeEmailcEmailNFe.Value,qryEnvioNFeEmailcEmailCopiaNFe.Value);
              except
                  MensagemErro('N�o foi poss�vel enviar a NF-e por e-mail, verifique as configura��es de email e/ou sua conex�o.');
                  Raise;
              end;

              if not(Trim(qryEnvioNFeEmailcEmailCopiaNFe.Value) = '') then
                   ShowMessage('NF-e enviada por e-mail com sucesso'+#13#13+'Email de destino: '+qryEnvioNFeEmailcEmailNFe.Value + ' e ' + qryEnvioNFeEmailcEmailCopiaNFe.Value)
              else ShowMessage('NF-e enviada por e-mail com sucesso'+#13#13+'Email de destino: '+qryEnvioNFeEmailcEmailNFe.Value) ;
          end;
      end;
  finally
      freeAndNil( objImpNFe );
  end;
end;

procedure TfrmImpDoctoFiscal.Master(Sender: TObject);
var
  objDadoCompNf     : TfrmDadoComplDoctoFiscal;
  objModImpETP      : TfrmModImpETP;
  arrPed            : Array of Integer;
  cTrackingCode     : String;
  cTrackingCodeConf : String;
  nCdSerieFiscal    : Integer;
  bNotaEletronica   : Boolean;
label
  solicitaTrackingCode;
begin
  inherited;

  bNotaEletronica := False;

  if (frmMenu.LeParametro('EMITENFE') = 'S') then
      bNotaEletronica := True;

  if (not frmMenu.qryParametroEmpresa.eof) then
  begin
      if (frmMenu.LeParametroEmpresa('EMITENFE') = 'N') then
          bNotaEletronica := False;
  end;

  if (qryDoctoFiscal.Active) and (qryDoctoFiscalnCdDoctoFiscal.Value > 0) then
  begin
      qrySerieFiscalLoja.Close;
      qrySerieFiscalLoja.Parameters.ParamByName('nCdLoja').Value        := qryDoctoFiscalnCdLoja.Value;
      qrySerieFiscalLoja.Parameters.ParamByName('nCdSerieFiscal').Value := qrySerieFiscalnCdSerieFiscal.Value;
      qrySerieFiscalLoja.Open;

      if ((qryDoctoFiscalnCdLoja.Value > 0) and (qrySerieFiscalLoja.IsEmpty)) then
      begin
          MensagemAlerta('Loja do documento fiscal n�o vinculado ao cadastro da s�rie fiscal.');
          MaskEdit3.SetFocus;
          Abort;
      end;

      qryDoctoFiscalAux.Close;
      PosicionaQuery( qryDoctoFiscalAux, qryDoctoFiscalnCdDoctoFiscal.asString ) ;

      if (qryDoctoFiscalAuxnCdStatus.Value <> 1) then
      begin
          MensagemErro('Documento fiscal No ' + qryDoctoFiscalAuxnCdDoctoFiscal.AsString + ' foi cancelado por outro usu�rio impedindo seu processamento.');
          ToolButton1.Click;
          Exit;
      end;

      try
          objImpNFe     := TfrmEmiteNfe2.Create(Self);
          objDadoCompNf := TfrmDadoComplDoctoFiscal.Create(nil);
          objModImpETP  := TfrmModImpETP.Create(nil);

          try
              { -- se o documento fiscal n�o tiver n�mero de recibo, faz o processo de gera��o completo -- }
              if (qryDoctoFiscalAuxcNrReciboNFe.Value = '') then
              begin
                  { -- se n�o existir tentativa de impress�o, exibi edi��o de dados complementares -- }
                  if (qryDoctoFiscaliNrDoctoReserva.Value = 0) then
                  begin
                      objDadoCompNf.nCdTerceiro    := qryDoctoFiscalnCdTerceiroTransp.Value ;
                      objDadoCompNf.nCdDoctoFiscal := qryDoctoFiscalnCdDoctoFiscal.Value ;

                      objDadoCompNf.edtVolume.Text      := '0' ;
                      objDadoCompNf.edtPesoBruto.Text   := '0' ;
                      objDadoCompNf.edtPesoLiquido.Text := '0' ;

                      objDadoCompNf.edtnCdTransp.Text := '0'  ;
                      objDadoCompNf.edtcNmTransp.Text := '' ;
                      objDadoCompNf.edtCNPJ.Text      := '' ;
                      objDadoCompNf.edtIE.Text        := '' ;
                      objDadoCompNf.edtEndereco.Text  := '' ;
                      objDadoCompNf.edtBairro.Text    := '' ;
                      objDadoCompNf.edtCidade.Text    := '' ;
                      objDadoCompNf.edtUF.Text        := '' ;
                      objDadoCompNf.edtPlacaVeic.Text := '' ;
                      objDadoCompNf.edtUFVeic.Text    := '' ;
                      objDadoCompNf.edtDtSaida.Text   := DateToStr(Date());

                      showForm(objDadoCompNf,false);

                      if(not objDadoCompNf.bProcessado) then
                          Exit;
                  end
                  else
                  begin
                      case MessageDlg('Confirma a nova tentativa de impress�o deste documento ?', mtConfirmation, [mbYes,mbNo], 0) of
                          mrNo : Exit;
                      end;
                  end;

                  if ((objDadoCompNf.bResultado) or (qryDoctoFiscaliNrDoctoReserva.Value > 0)) then
                  begin
                      frmMenu.qryParametroEmpresa.Close ;
                      frmMenu.qryParametroEmpresa.Parameters.ParamByName('cParametro').Value := 'EMITENFE';
                      frmMenu.qryParametroEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
                      frmMenu.qryParametroEmpresa.Open ;

                      { -- consulta status do servi�o -- }
                      if (bNotaEletronica) then
                          objImpNFe.preparaNFe;

                      {-- quando est� em modo de SCAN pega o numero de s�rie do parametro --}
                      if (objImpNFe.StatusWebService(1).nCdStatusSEFAZ <> 107) then
                      begin
                          qrySerieFiscalScan.Close;
                          PosicionaQuery(qrySerieFiscalScan,IntToStr(frmMenu.nCdEmpresaAtiva));

                          if (qrySerieFiscalScan.RecordCount = 0) then
                          begin
                              MensagemAlerta('S�rie fiscal para o modo de Conting�ncia SCAN n�o cadastrada.');
                              Exit;
                          end;

                          nCdSerieFiscal := qrySerieFiscalScannCdSerieFiscal.Value;
                      end
                      else
                      begin
                          nCdSerieFiscal := qrySerieFiscalnCdSerieFiscal.Value;
                      end;

                      { -- inclui n�m documento fiscal tempor�rio -- }
                      if (bNotaEletronica) then
                          ProcessaNumDocto(nCdSerieFiscal, qryDoctoFiscalnCdDoctoFiscal.Value, 'I');

                      try
                          frmMenu.Connection.BeginTrans;
                          frmMenu.mensagemUsuario('Imprimindo doc...');

                          SP_IMPRIME_DOCTO.Close;
                          SP_IMPRIME_DOCTO.Parameters.ParamByName('@nCdDoctoFiscal').Value := qryDoctoFiscalnCdDoctoFiscal.Value;
                          SP_IMPRIME_DOCTO.Parameters.ParamByName('@nCdSerieFiscal').Value := nCdSerieFiscal;
                          SP_IMPRIME_DOCTO.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
                          SP_IMPRIME_DOCTO.ExecProc;

                          { -- efetiva n�m documento fiscal tempor�rio -- }
                          if (bNotaEletronica) then
                              ProcessaNumDocto(nCdSerieFiscal, qryDoctoFiscalnCdDoctoFiscal.Value, 'E');

                          { -- libera objeto da mem�ria devido m�todo abaixo recriar o mesmo -- }
                          FreeAndNil(objImpNFe);

                          ImprimeNF;

                          frmMenu.Connection.CommitTrans;
                          frmMenu.mensagemUsuario('');
                      except
                          frmMenu.Connection.RollbackTrans;
                          Raise;
                      end;
                  end;
              end;

              if (qryDoctoFiscalAuxcChaveNFe.Value <> '') then
              begin
                  qryPedidoDoctoFiscal.Close;
                  PosicionaQuery(qryPedidoDoctoFiscal, qryDoctoFiscalAuxnCdDoctoFiscal.AsString);

                  qryPedidoDoctoFiscal.First;

                  while (not qryPedidoDoctoFiscal.Eof) do
                  begin

                      if (qryPedidoDoctoFiscalcFlgPedidoWeb.Value = 1) then
                      begin
                          { -- c�digo de rastreio -- }
                          if (not qryPedidoDoctoFiscalcTrackingCode.IsNull) then
                          begin
                              case MessageDlg('Deseja atualizar o C�digo de Rastreio atual ?' + #13#13 + 'C�digo: ' + qryPedidoDoctoFiscalcTrackingCode.Value + '.', mtConfirmation,[mbYes,mbNo],0) of
                                  mrNo  : ShowMessage('C�digo de Rastreio n�o atualizado.');
                                  mrYes : goto solicitaTrackingCode;
                              end;
                          end
                          else
                          begin
                              solicitaTrackingCode:

                              InputQuery('C�digo de Rastreamento','Informe o C�digo de Rastreamento',cTrackingCode);

                              if (Trim(cTrackingCode) = '') then
                              begin
                                  case MessageDlg('C�digo de Rastreio n�o informado.' + #13#13 + 'Deseja continuar ?', mtConfirmation,[mbYes,mbNo],0) of
                                      mrNo  : goto solicitaTrackingCode;
                                      mrYes : ShowMessage('C�digo de Rastreio n�o informado.');
                                  end;
                              end
                              else
                              begin
                                  InputQuery('C�digo de Rastreamento','Confirme o C�digo de Rastreamento',cTrackingCodeConf);

                                  if (Trim(cTrackingCode) <> Trim(cTrackingCodeConf)) then
                                  begin
                                      MensagemAlerta('Confirma��o do C�digo de Rastreio inv�lida.');

                                      cTrackingCode     := '';
                                      cTrackingCodeConf := '';

                                      goto solicitaTrackingCode;
                                  end
                                  else
                                  begin
                                      frmMenu.mensagemUsuario('Atualizando C�digo de Rastreio...');

                                      { -- atualiza c�digo de rastreio apenas para os pedidos ativos na web -- }
                                      try
                                          qryAtualizaTrackingCode.Close;
                                          qryAtualizaTrackingCode.Parameters.ParamByName('nCdDoctoFiscal').Value := qryDoctoFiscalAuxnCdDoctoFiscal.Value;
                                          qryAtualizaTrackingCode.Parameters.ParamByName('cTrackingCode').Value  := cTrackingCode;
                                          qryAtualizaTrackingCode.ExecSQL;

                                          ShowMessage('C�digo de Rastreio atualizado com sucesso.' + #13#13 + 'C�digo: ' + cTrackingCode + '.');
                                      except
                                          Raise;
                                      end;

                                      frmMenu.mensagemUsuario('');
                                  end;
                              end;
                          end;
                      end;

                      { -- se tipo de pedido emitir etiqueta, chama processo de emiss�o -- }
                      if (qryPedidoDoctoFiscalcFlgEmitirETQFat.Value = 1) then
                      begin
                          { -- impress�o de etiqueta -- }
                          if (MessageDlg('Deseja imprimir a etiqueta do pedido No ' + qryPedidoDoctoFiscalnCdPedido.AsString + ' ?', mtConfirmation,[mbYes,mbNo],0) = mrYes) then
                          begin
                              SetLength(arrPed,1);

                              arrPed[0] := qryPedidoDoctoFiscalnCdPedido.Value;

                              objModImpETP.emitirEtiquetas(arrPed,1);
                          end;
                      end;

                      qryPedidoDoctoFiscal.Next;
                  end;

                  { -- atualiza inf. do pedido web e envia para monitor de transmiss�o -- }
                  if (frmMenu.LeParametro('REPLWEBATIVA') = 'S') then
                  begin
                      frmMenu.mensagemUsuario('Enviando p/ WEB');

                      qryEnviaPedidoWebMonitor.Close;
                      qryEnviaPedidoWebMonitor.Parameters.ParamByName('nCdDoctoFiscal').Value := qryDoctoFiscalAuxnCdDoctoFiscal.Value;
                      qryEnviaPedidoWebMonitor.ExecSQL;

                      frmMenu.mensagemUsuario('');
                  end;

                  EnviaEmail;

                  { -- nota com uso denegado -- }
                  if (qryDoctoFiscalAuxiStatusRetorno.Value = 302) then
                  begin
                      ShowMessage('Documento fiscal impresso com situa��o de USO DENEGADO, efetue o cancelamento deste documento.' + #13#13
                                 +'Documento: ' + qryDoctoFiscalAuxnCdDoctoFiscal.AsString + #13
                                 +'NF-e: ' + qryDoctoFiscalAuxiNrDocto.AsString + #13
                                 +'Chave: ' + qryDoctoFiscalAuxcChaveNFe.Value);
                  end;
              end;
          except
              MensagemErro('Erro no processamento.');
              Raise;
              Exit;
          end;
      finally
          frmMenu.mensagemUsuario('');

          FreeAndNil(objDadoCompNf);
          FreeAndNil(objImpNFe);
          FreeAndNil(objModImpETP);

          qryAux.Close;
          qryPedidoDoctoFiscal.Close;
          ToolButton1.Click;
      end;
  end;
end;

procedure TfrmImpDoctoFiscal.ProcessaNumDocto(nCdSerieFiscal, nCdDoctoFiscal : Integer; cAcao : String);
begin
  try
      if (cAcao = 'I') then
          frmMenu.Connection.BeginTrans;

      SP_PROCESSA_NUMDOCTOFISCAL.Close;
      SP_PROCESSA_NUMDOCTOFISCAL.Parameters.ParamByName('@nCdSerieFiscal').Value := nCdSerieFiscal;
      SP_PROCESSA_NUMDOCTOFISCAL.Parameters.ParamByName('@nCdDoctoFiscal').Value := nCdDoctoFiscal;
      SP_PROCESSA_NUMDOCTOFISCAL.Parameters.ParamByName('@cAcao').Value          := cAcao;
      SP_PROCESSA_NUMDOCTOFISCAL.ExecProc;

      if (cAcao = 'I') then
          frmMenu.Connection.CommitTrans;
  except
      if (cAcao = 'I') then
          frmMenu.Connection.RollbackTrans;
      Raise;
  end;
end;

procedure TfrmImpDoctoFiscal.qryDoctoFiscalAfterScroll(DataSet: TDataSet);
begin
  inherited;

  pageControlDoctoFiscal.ActivePage := tabResumido;
end;

procedure TfrmImpDoctoFiscal.pageControlDoctoFiscalChange(Sender: TObject);
begin
  inherited;

  if (qryDoctoFiscal.IsEmpty) then
      Exit;

  qryPedidoDoctoFiscal.Close;

  if (pageControlDoctoFiscal.ActivePage = tabDetalhado) then
  begin
      PosicionaQuery(qryPedidoDoctoFiscal, qryDoctoFiscalnCdDoctoFiscal.AsString);
  end;
end;

procedure TfrmImpDoctoFiscal.btImpEspelhoClick(Sender: TObject);
begin
  if (qryDoctoFiscal.IsEmpty) then
      Exit;

  case MessageDlg('Confirma impress�o do espelho do documento fiscal selecionado ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  try
      frmMenu.Connection.BeginTrans;
      
      try
          { -- atualiza informa��es do documento fiscal temporariamente para impress�o do espelho -- }
          qryDoctoEspelho.Close;
          qryDoctoEspelho.Parameters.ParamByName('nPK').Value := qryDoctoFiscalnCdDoctoFiscal.Value;
          qryDoctoEspelho.ExecSQL;

          objImpNFe := TfrmEmiteNFe2.Create(Self);
          objImpNFe.gerarEspelhoNFe(qryDoctoFiscalnCdDoctoFiscal.Value);
      except
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      frmMenu.Connection.RollbackTrans;
      frmMenu.mensagemUsuario('');
      FreeAndNil(objImpNFe);
  end;
end;

procedure TfrmImpDoctoFiscal.btGerarArqXMLClick(Sender: TObject);
var
  cCaminhoXML : String;
begin
  if (qryDoctoFiscal.IsEmpty) then
      Exit;

  case MessageDlg('Confirma a gera��o do arquivo xml sem assinatura ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  SaveDialog1.FileName := qryDoctoFiscalnCdDoctoFiscal.AsString + '-invalid.xml';
  SaveDialog1.Filter   := '*.xml';

  if (SaveDialog1.Execute) then
      cCaminhoXML := SaveDialog1.FileName;

  if (Trim(cCaminhoXML) = '') then
  begin
      MensagemAlerta('Caminho do arquivo n�o informado.');
      Exit;
  end;

  try
      frmMenu.Connection.BeginTrans;
      
      try
          { -- atualiza informa��es do documento fiscal temporariamente para impress�o do espelho -- }
          qryDoctoEspelho.Close;
          qryDoctoEspelho.Parameters.ParamByName('nPK').Value := qryDoctoFiscalnCdDoctoFiscal.Value;
          qryDoctoEspelho.ExecSQL;

          objImpNFe := TfrmEmiteNFe2.Create(Self);
          objImpNFe.gerarArqXMLNFe(qryDoctoFiscalnCdDoctoFiscal.Value, cCaminhoXML);

          ShowMessage('Arquivo xml gerado com sucesso.' + #13#13 + 'Dir.: ' + cCaminhoXML);
      except
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      frmMenu.Connection.RollbackTrans;
      frmMenu.mensagemUsuario('');
      FreeAndNil(objImpNFe);
  end;
end;

procedure TfrmImpDoctoFiscal.cxGridResumidoDBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  if (AItem <> cxGridResumidoDBTableView1cFlgDocPendente) then
      if (StrToInt(ARecord.Values[0]) = 1) then
          AStyle := PreProcessado;
end;

initialization
  RegisterClass(TfrmImpDoctoFiscal);

end.
