unit fDepositoBancario_SelecaoCheque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  cxLookAndFeelPainters, cxButtons, Mask, GridsEh, DBGridEh, DB, ADODB,
  DBCtrls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid;

type
  TfrmDepositoBancario_SelecaoCheque = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    MaskEdit2: TMaskEdit;
    Label6: TLabel;
    MaskEdit1: TMaskEdit;
    Label3: TLabel;
    MaskEdit6: TMaskEdit;
    Label5: TLabel;
    cxButton1: TcxButton;
    cmdPreparaTemp: TADOCommand;
    qryChequesPend: TADOQuery;
    qryChequesPenddDtDeposito: TDateTimeField;
    qryChequesPendiNrCheque: TIntegerField;
    qryChequesPendnValCheque: TBCDField;
    qryChequesPendcCNPJCPF: TStringField;
    qryChequesPendnCdBanco: TIntegerField;
    qryChequesPendcNmTerceiro: TStringField;
    qryChequesPendcFlgAux: TIntegerField;
    dsChequesPend: TDataSource;
    MaskEdit3: TMaskEdit;
    Label1: TLabel;
    qryContaBancariaDeb: TADOQuery;
    qryContaBancariaDebnCdContaBancaria: TIntegerField;
    qryContaBancariaDebnCdConta: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryItemDepositoBancario: TADOQuery;
    qryItemDepositoBancarionCdItemDepositoBancario: TAutoIncField;
    qryItemDepositoBancarionCdDepositoBancario: TIntegerField;
    qryItemDepositoBancarioiTipo: TIntegerField;
    qryItemDepositoBancarionCdContaDebito: TIntegerField;
    qryItemDepositoBancarioiQtdeCheques: TIntegerField;
    qryItemDepositoBancarionValor: TBCDField;
    qryChequeItemDepositoBancario: TADOQuery;
    qryChequeItemDepositoBancarionCdChequeItemDepositoBancario: TAutoIncField;
    qryChequeItemDepositoBancarionCdItemDepositoBancario: TIntegerField;
    qryChequeItemDepositoBancarionCdCheque: TIntegerField;
    qryChequesPendnCdCheque: TIntegerField;
    qryAux: TADOQuery;
    qryPopulaTemp: TADOQuery;
    qryChequesSEL: TADOQuery;
    DateTimeField1: TDateTimeField;
    IntegerField1: TIntegerField;
    qryChequesSELnValCheque: TBCDField;
    StringField1: TStringField;
    IntegerField2: TIntegerField;
    StringField2: TStringField;
    qryChequesSELcFlgAux: TIntegerField;
    qryChequesSELnCdCheque: TIntegerField;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1dDtDeposito: TcxGridDBColumn;
    cxGrid1DBTableView1iNrCheque: TcxGridDBColumn;
    cxGrid1DBTableView1nValCheque: TcxGridDBColumn;
    cxGrid1DBTableView1cCNPJCPF: TcxGridDBColumn;
    cxGrid1DBTableView1nCdBanco: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgAux: TcxGridDBColumn;
    cxGrid1DBTableView1nCdCheque: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    dsChequesSEL: TDataSource;
    cxGridDBTableView1dDtDeposito: TcxGridDBColumn;
    cxGridDBTableView1iNrCheque: TcxGridDBColumn;
    cxGridDBTableView1nValCheque: TcxGridDBColumn;
    cxGridDBTableView1cCNPJCPF: TcxGridDBColumn;
    cxGridDBTableView1nCdBanco: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView1cFlgAux: TcxGridDBColumn;
    cxGridDBTableView1nCdCheque: TcxGridDBColumn;
    procedure ChequeNumerario(cDtDeposito : string) ;
    procedure ChequePreDatado(cDtDeposito : string) ;
    procedure cxButton1Click(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    iTipo : integer ;
    nCdContaBancariaDep : integer ;
    nCdEmpresa          : integer ;
    nCdLoja             : integer ;
    nCdDepositoBancario : integer ;
  end;

var
  frmDepositoBancario_SelecaoCheque: TfrmDepositoBancario_SelecaoCheque;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmDepositoBancario_SelecaoCheque.ChequeNumerario(cDtDeposito : string) ;
begin
    iTipo               := 2 ;
    Label3.Caption      := 'Data de Dep�sito At�' ;
    Label6.Visible      := False ;
    MaskEdit2.Visible   := False ;
    MaskEdit1.Text      := cDtDeposito ;
    MaskEdit2.Text      := '' ;
    MaskEdit6.Text      := '' ;

    Self.ShowModal;
end ;

procedure TfrmDepositoBancario_SelecaoCheque.ChequePreDatado(cDtDeposito : string) ;
begin
    iTipo               := 3 ;
    Label3.Caption      := 'Data de Dep�sito de' ;
    Label6.Visible      := true ;
    MaskEdit2.Visible   := true ;
    MaskEdit1.Text      := cDtDeposito ;
    MaskEdit2.Text      := '31/12/' + Copy(MaskEdit1.Text,7,4) ;
    MaskEdit6.Text      := '' ;

    Self.ShowModal;
end ;

procedure TfrmDepositoBancario_SelecaoCheque.cxButton1Click(
  Sender: TObject);
begin
  inherited;

  if (DbEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione a conta portadora.') ;
      Maskedit3.SetFocus ;
      exit ;
  end ;

  if (iTipo = 2) then
  begin
      if (Trim(MaskEdit1.Text) = '/  /') then
      begin
          MensagemAlerta('Informe a data limite de dep�sito.') ;
          MaskEdit1.SetFocus ;
          exit ;
      end ;
  end ;

  if (iTipo = 3) then
  begin
      if (Trim(MaskEdit1.Text) = '/  /') then
      begin
          MensagemAlerta('Informe a data inicial de dep�sito.') ;
          MaskEdit1.SetFocus ;
          exit ;
      end ;

      if (Trim(MaskEdit2.Text) = '/  /') then
      begin
          MensagemAlerta('Informe a data final de dep�sito.') ;
          MaskEdit2.SetFocus ;
          exit ;
      end ;
  end ;

  cmdPreparaTemp.Execute;

  try
      qryAux.Close;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM #Temp_Cheques WHERE cFlgAux = 0') ;
      qryAux.ExecSQL;
  except
  end ;

  qryPopulaTemp.Close ;
  qryPopulaTemp.Parameters.ParamByName('iTipo').Value               := iTipo ;
  qryPopulaTemp.Parameters.ParamByName('dDtDepositoIni').Value      := DateToStr(frmMenu.ConvData(MaskEdit1.Text)) ;
  qryPopulaTemp.Parameters.ParamByName('dDtDepositoFim').Value      := DateToStr(frmMenu.ConvData(MaskEdit2.Text)) ;
  qryPopulaTemp.Parameters.ParamByName('iNrCheque').Value           := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  qryPopulaTemp.Parameters.ParamByName('nCdEmpresa').Value          := frmMenu.nCdEmpresaAtiva ;
  qryPopulaTemp.Parameters.ParamByName('nCdContaBancaria').Value    := qryContaBancariaDebnCdContaBancaria.Value ;
  qryPopulaTemp.Parameters.ParamByName('nCdDepositoBancario').Value := nCdDepositoBancario ;
  qryPopulaTemp.ExecSQL;

  qryChequesPend.Close ;
  qryChequesPend.Open ;

  if (qryChequesPend.eof) then
  begin
      MensagemAlerta('Nenhum cheque encontrado para o crit�rio informado.') ;
      MaskEdit3.SetFocus ;
      exit ;
  end ;


end;

procedure TfrmDepositoBancario_SelecaoCheque.MaskEdit3Exit(
  Sender: TObject);
begin
  inherited;

  qryContaBancariaDeb.Close ;
  qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := nCdEmpresa ;
  qryContaBancariaDeb.Parameters.ParamByName('nCdLoja').Value    := nCdLoja ;
  PosicionaQuery(qryContaBancariaDeb, MaskEdit3.Text) ;

end;

procedure TfrmDepositoBancario_SelecaoCheque.MaskEdit3KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = @@Empresa AND ((@@Loja = 0) OR (@@Loja = ContaBancaria.nCdLoja)) AND ((cFlgCaixa = 1 OR cFlgCofre = 1))');

            If (nPK > 0) then
            begin
                MaskEdit3.Text := IntToStr(nPK) ;

                qryContaBancariaDeb.Close ;
                qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := nCdEmpresa ;
                qryContaBancariaDeb.Parameters.ParamByName('nCdLoja').Value    := nCdLoja ;
                PosicionaQuery(qryContaBancariaDeb, MaskEdit3.Text) ;
            end ;

    end ;

  end ;

end;

procedure TfrmDepositoBancario_SelecaoCheque.FormShow(Sender: TObject);
begin
  inherited;

  qryChequesPend.Close ;
  qryChequesSEL.Close ;

  try
      qryAux.Close;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('TRUNCATE TABLE #Temp_Cheques') ;
      qryAux.ExecSQL;
  except
  end ;

  qryContaBancariaDeb.Close ;
  MaskEdit3.Text := '' ;
  Maskedit6.Text := '' ;
  
  MaskEdit3.SetFocus ;

end;

procedure TfrmDepositoBancario_SelecaoCheque.ToolButton1Click(
  Sender: TObject);
var
    nValCheques  : double ;
    iQtdeCheques : integer ;
begin
  inherited;

  if (qryChequesSEL.eof) then
  begin
      MensagemAlerta('Nenhum cheque selecionado.') ;
      exit ;
  end ;

  qryChequesSEL.DisableControls;
  qryChequesSEL.First ;

  iQtdeCheques := 0 ;
  nValCheques  := 0 ;

  while not qryChequesSEL.Eof do
  begin
      nValCheques  := nValCheques + qryChequesSELnValCheque.Value ;
      iQtdeCheques := iQtdeCheques + 1 ;

      qryChequesSEL.Next ;
  end ;

  qryChequesSEL.First ;
  qryChequesSEL.EnableControls ;

  if (iQtdeCheques = 0) then
  begin
      MensagemAlerta('Nenhum cheque selecionado.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a inclus�o dos cheques selecionados no dep�sito ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  qryItemDepositoBancario.Close  ;
  qryItemDepositoBancario.Open   ;

  frmMenu.Connection.BeginTrans;

  try
      qryItemDepositoBancario.Insert ;
      qryItemDepositoBancarionCdItemDepositoBancario.Value := frmMenu.fnProximoCodigo('ITEMDEPOSITOBANCARIO') ;
      qryItemDepositoBancarionCdDepositoBancario.Value     := nCdDepositoBancario ;
      qryItemDepositoBancarioiTipo.Value                   := iTipo ;
      qryItemDepositoBancarionCdContaDebito.Value          := frmMenu.ConvInteiro(Maskedit3.Text) ;
      qryItemDepositoBancarioiQtdeCheques.Value            := iQtdeCheques ;
      qryItemDepositoBancarionValor.Value                  := nValCheques ;
      qryItemDepositoBancario.Post   ;

      qryAux.Close ;
      qryAux.SQL.Clear ;

      if (iTipo = 2) then
      begin
          qryAux.SQL.Add('UPDATE DepositoBancario Set nValChequeNumerario = nValChequeNumerario + ' + StringReplace(qryItemDepositoBancarionValor.AsString,',','.',[rfReplaceAll, rfIgnoreCase])) ;
          qryAux.SQL.Add(', nValTotalDeposito = nValTotalDeposito + ' + StringReplace(qryItemDepositoBancarionValor.AsString,',','.',[rfReplaceAll, rfIgnoreCase])) ;
          qryAux.SQL.Add(', iQtdeCheques = iQtdeCheques + ' + IntToStr(iQtdeCheques)) ;
          qryAux.SQL.Add(' WHERE nCdDepositoBancario = ' + IntToStr(nCdDepositoBancario)) ;
      end ;

      if (iTipo = 3) then
      begin
          qryAux.SQL.Add('UPDATE DepositoBancario Set nValChequePreDatado = nValChequePreDatado + ' + StringReplace(qryItemDepositoBancarionValor.AsString,',','.',[rfReplaceAll, rfIgnoreCase])) ;
          qryAux.SQL.Add(', nValTotalDeposito = nValTotalDeposito + ' + StringReplace(qryItemDepositoBancarionValor.AsString,',','.',[rfReplaceAll, rfIgnoreCase])) ;
          qryAux.SQL.Add(', iQtdeCheques = iQtdeCheques + ' + IntToStr(iQtdeCheques)) ;
          qryAux.SQL.Add(' WHERE nCdDepositoBancario = ' + IntToStr(nCdDepositoBancario)) ;
      end ;

      qryAux.ExecSQL;

      qryChequesSEL.DisableControls;
      qryChequesSEL.First ;

      while not qryChequesSEL.Eof do
      begin

          if (qryChequesSELcFlgAux.Value = 1) then
          begin
              qryChequeItemDepositoBancario.Close ;
              qryChequeItemDepositoBancario.Open  ;

              qryChequeItemDepositoBancario.Insert ;
              qryChequeItemDepositoBancarionCdChequeItemDepositoBancario.Value := frmMenu.fnProximoCodigo('CHEQUEITEMDEPOSITOBANCARIO') ;
              qryChequeItemDepositoBancarionCdItemDepositoBancario.Value       := qryItemDepositoBancarionCdItemDepositoBancario.Value ;
              qryChequeItemDepositoBancarionCdCheque.Value                     := qryChequesSELnCdCheque.Value ;
              qryChequeItemDepositoBancario.Post ;
          end ;

          qryChequesSEL.Next ;
      end ;

      qryChequesSEL.First ;
      qryChequesSEL.EnableControls ;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('TRUNCATE TABLE #Temp_Cheques') ;
  qryAux.ExecSQL;

  ShowMessage('Cheques Adicionados com Sucesso.') ;

  close ;

end;

procedure TfrmDepositoBancario_SelecaoCheque.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  if not qryChequesPend.Active or (qryChequesPendnCdCheque.Value = 0) then
      exit ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE #Temp_Cheques Set cFlgAux = 1 WHERE nCdCheque = ' + qryChequesPendnCdCheque.AsString) ;
  qryAux.ExecSQL ;

  qryChequesPend.Requery();

  qryChequesSEL.Close ;
  qryChequesSEL.Open ;

end;

procedure TfrmDepositoBancario_SelecaoCheque.cxGridDBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  if not qryChequesSEL.Active or (qryChequesSELnCdCheque.Value = 0) then
      exit ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE #Temp_Cheques Set cFlgAux = 0 WHERE nCdCheque = ' + qryChequesSELnCdCheque.AsString) ;
  qryAux.ExecSQL ;

  qryChequesPend.Requery();
  qryChequesSEL.Requery();

end;

end.
