unit fParametro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, cxPC, cxControls, DB, ADODB, DBGridEhGrouping, ToolCtrlsEh,
  StdCtrls, DBCtrls, cxLookAndFeelPainters, cxButtons, cxContainer, cxEdit,
  cxTextEdit, cxMaskEdit, cxDropDownEdit;

type
  TfrmParametro = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    qryParametro: TADOQuery;
    dsParametro: TDataSource;
    qryParametrocParametro: TStringField;
    qryParametrocValor: TStringField;
    qryParametrocDescricao: TStringField;
    qryEmpresa: TADOQuery;
    dsEmpresa: TDataSource;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacParametro: TStringField;
    qryEmpresacValor: TStringField;
    qryEmpresacDescricao: TStringField;
    GroupBox1: TGroupBox;
    edtFiltro: TEdit;
    btBuscar: TcxButton;
    cmbFiltro: TComboBox;
    cxTabSheet3: TcxTabSheet;
    dsTabelaSistema: TDataSource;
    qryTabelaSistema: TADOQuery;
    qryTabelaSistemacNmTabela: TStringField;
    qryTabelaSistemanCdTabelaSistema: TIntegerField;
    qryTabelaSistemacTabela: TStringField;
    qryParametroWeb: TADOQuery;
    qryParametroWebcParametro: TStringField;
    qryParametroWebcURLWebService: TStringField;
    qryParametroWebnCdTabelaSistema: TIntegerField;
    qryParametroWebcNmTabela: TStringField;
    dsParametroWeb: TDataSource;
    GroupBox2: TGroupBox;
    edtFiltroWeb: TEdit;
    btBuscarWeb: TcxButton;
    cmbFiltroWeb: TComboBox;
    DBGridEh3: TDBGridEh;
    procedure FormShow(Sender: TObject);
    procedure cmbFiltroChange(Sender: TObject);
    procedure btBuscarClick(Sender: TObject);
    procedure cmbFiltroPropertiesChange(Sender: TObject);
    procedure btBuscarWebClick(Sender: TObject);
    procedure qryParametroWebCalcFields(DataSet: TDataSet);
    procedure cmbFiltroWebChange(Sender: TObject);
    procedure DBGridEh3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmParametro: TfrmParametro;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmParametro.FormShow(Sender: TObject);
begin
  inherited;
  qryParametroWeb.Close;
  qryParametroWeb.Open;

  qryParametro.Close ;
  qryParametro.Parameters.ParamByName('cParametro').Value := '';
  qryParametro.Open ;

  PosicionaQuery(qryEmpresa, IntToStr(frmMenu.nCdEmpresaAtiva)) ;

  cxPageControl1.ActivePage := cxTabSheet2 ;
  cxPageControl1.ActivePage := cxTabSheet1 ;

end;

procedure TfrmParametro.cmbFiltroChange(Sender: TObject);
begin
  edtFiltro.Clear;
end;

procedure TfrmParametro.btBuscarClick(Sender: TObject);
begin
  case (cmbFiltro.ItemIndex) of
      0:begin
            qryParametro.Close;
            qryParametro.SQL.Clear;
            qryParametro.SQL.Add('SELECT * FROM Parametro');
            qryParametro.Open;
        end;
      1:begin
           qryParametro.Close;
           qryParametro.SQL.Clear;
           qryParametro.SQL.Add('SELECT * FROM Parametro WHERE cParametro LIKE''%' + edtFiltro.Text + '%''');
           qryParametro.Open;
       end;
      2:begin
           qryParametro.Close;
           qryParametro.SQL.Clear;
           qryParametro.SQL.Add('SELECT * FROM Parametro WHERE cDescricao LIKE UPPER(''%' + edtFiltro.Text + '%'')');
           qryParametro.Open;
       end;
  end;
end;

procedure TfrmParametro.cmbFiltroPropertiesChange(Sender: TObject);
begin
  edtFiltro.Clear;
end;

procedure TfrmParametro.btBuscarWebClick(Sender: TObject);
begin
  inherited;
  case (cmbFiltroWeb.ItemIndex) of
    0:begin
          qryParametroWeb.Close;
          qryParametroWeb.SQL.Clear;
          qryParametroWeb.SQL.Add('SELECT * FROM ParametroWeb');
          qryParametroWeb.Open;
      end;
    1:begin
         qryParametroWeb.Close;
         qryParametroWeb.SQL.Clear;
         qryParametroWeb.SQL.Add('SELECT * FROM ParametroWeb WHERE cParametro LIKE''%' + edtFiltroWeb.Text + '%''');
         qryParametroWeb.Open;
     end;
    2:begin
         qryParametroWeb.Close;
         qryParametroWeb.SQL.Clear;
         qryParametroWeb.SQL.Add('SELECT * FROM ParametroWeb WHERE cURLWebService LIKE UPPER(''%' + edtFiltroWeb.Text + '%'')');
         qryParametroWeb.Open;
     end;
   end;
end;

procedure TfrmParametro.qryParametroWebCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (frmMenu.ConvInteiro(qryParametroWebnCdTabelaSistema.AsString) > 0) then
  begin
      PosicionaQuery(qryTabelaSistema,qryParametroWebnCdTabelaSistema.AsString);
      qryParametroWebcNmTabela.Value := qryTabelaSistemacNmTabela.Value;
  end;
end;

procedure TfrmParametro.cmbFiltroWebChange(Sender: TObject);
begin
  inherited;
  
  edtFiltroWeb.Clear;

  if cmbFiltroWeb.ItemIndex = 0 then
  begin
      btBuscarWeb.Click;
  end;
end;

procedure TfrmParametro.DBGridEh3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : integer;
begin
  inherited;
  case key of  VK_F4 : begin
        if (qryParametroWeb.State = dsBrowse) then
             qryParametroWeb.Edit ;

        if (qryParametroWeb.State in [dsInsert,dsEdit]) then
        begin
            if (DBGridEh3.Columns[DBGridEh3.Col-1].FieldName = 'nCdTabelaSistema') then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(770);

                If (nPK > 0) then
                begin
                    qryParametroWebnCdTabelaSistema.Value := nPK ;
                end ;
            end;                               
        end ;
      end;
  end;
end;

initialization
    RegisterClass(tFRMParametro) ;
    
end.
