unit rPickList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, QuickRpt, QRCtrls, jpeg;

type
  TrptPickList = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRGroup1: TQRGroup;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel14: TQRLabel;
    QRLabel7: TQRLabel;
    QRBand2: TQRBand;
    QRShape5: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRBand3: TQRBand;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText27: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel11: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRTotalPagina: TQRLabel;
    QRDBText7: TQRDBText;
    SPREL_PICK_LIST: TADOStoredProc;
    SPREL_PICK_LISTnCdPedido: TIntegerField;
    SPREL_PICK_LISTnCdPickList: TIntegerField;
    SPREL_PICK_LISTdDtPedido: TDateTimeField;
    SPREL_PICK_LISTnCdTerceiro: TIntegerField;
    SPREL_PICK_LISTcNmTerceiro: TStringField;
    SPREL_PICK_LISTcEnderecoEntrega: TStringField;
    SPREL_PICK_LISTdDtPrevEntIni: TDateTimeField;
    SPREL_PICK_LISTdDtPrevEntFim: TDateTimeField;
    SPREL_PICK_LISTnCdItemPedido: TIntegerField;
    SPREL_PICK_LISTcOBS: TStringField;
    SPREL_PICK_LISTnCdProduto: TIntegerField;
    SPREL_PICK_LISTcNmProduto: TStringField;
    SPREL_PICK_LISTnQtdePick: TBCDField;
    SPREL_PICK_LISTnCdTipoItemPed: TIntegerField;
    SPREL_PICK_LISTnCdAlaProducao: TIntegerField;
    SPREL_PICK_LISTcNmAlaProducao: TStringField;
    SPREL_PICK_LISTcComposicao: TStringField;
    QRDBText8: TQRDBText;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPickList: TrptPickList;

implementation

{$R *.dfm}

end.
