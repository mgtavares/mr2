inherited frmMTitulo_Movimenta: TfrmMTitulo_Movimenta
  Left = 166
  Top = 141
  Width = 877
  Height = 505
  BorderIcons = [biSystemMenu]
  Caption = 'Movimenta'#231#227'o Individual de T'#237'tulos'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 861
    Height = 438
  end
  object Label1: TLabel [1]
    Left = 45
    Top = 96
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Banc'#225'ria'
    Color = clWhite
    FocusControl = DBEdit1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
  end
  object Label2: TLabel [2]
    Left = 71
    Top = 48
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Opera'#231#227'o'
    Color = clWhite
    FocusControl = DBEdit2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
  end
  object Label3: TLabel [3]
    Left = 31
    Top = 72
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'Forma Pagamento'
    Color = clWhite
    FocusControl = DBEdit3
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
  end
  object Label4: TLabel [4]
    Left = 41
    Top = 144
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Cheque'
    Color = clWhite
    FocusControl = DBEdit4
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
  end
  object Label5: TLabel [5]
    Left = 24
    Top = 168
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor do Movimento'
    Color = clWhite
    FocusControl = DBEdit5
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
  end
  object Label6: TLabel [6]
    Left = 25
    Top = 192
    Width = 93
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data do Movimento'
    Color = clWhite
    FocusControl = DBEdit6
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
  end
  object Label7: TLabel [7]
    Left = 24
    Top = 216
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Documento'
    Color = clWhite
    FocusControl = DBEdit7
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
  end
  object Label8: TLabel [8]
    Left = 60
    Top = 233
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observa'#231#227'o'
    Color = clWhite
    FocusControl = DBMemo1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    Transparent = False
  end
  object Label9: TLabel [9]
    Left = 311
    Top = 145
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Bom para'
    FocusControl = DBEdit11
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    Transparent = False
  end
  object Label10: TLabel [10]
    Left = 39
    Top = 120
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro de Custo'
    FocusControl = DBEdit12
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  inherited ToolBar1: TToolBar
    Width = 861
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit1: TDBEdit [12]
    Tag = 1
    Left = 120
    Top = 88
    Width = 65
    Height = 22
    DataField = 'nCdContaBancaria'
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    OnExit = DBEdit1Exit
    OnKeyDown = DBEdit1KeyDown
  end
  object DBEdit2: TDBEdit [13]
    Left = 120
    Top = 40
    Width = 65
    Height = 22
    DataField = 'nCdOperacao'
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    OnExit = DBEdit2Exit
    OnKeyDown = DBEdit2KeyDown
  end
  object DBEdit3: TDBEdit [14]
    Tag = 1
    Left = 120
    Top = 64
    Width = 65
    Height = 22
    DataField = 'nCdFormaPagto'
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [15]
    Tag = 1
    Left = 120
    Top = 136
    Width = 134
    Height = 22
    DataField = 'iNrCheque'
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    OnExit = DBEdit4Exit
  end
  object DBEdit5: TDBEdit [16]
    Left = 120
    Top = 160
    Width = 173
    Height = 22
    DataField = 'nValMov'
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
  end
  object DBEdit6: TDBEdit [17]
    Left = 120
    Top = 184
    Width = 89
    Height = 22
    DataField = 'dDtMov'
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 7
    OnEnter = DBEdit6Enter
  end
  object DBEdit7: TDBEdit [18]
    Left = 120
    Top = 208
    Width = 199
    Height = 22
    DataField = 'cNrDoc'
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
  end
  object DBMemo1: TDBMemo [19]
    Left = 120
    Top = 232
    Width = 729
    Height = 225
    DataField = 'cObsMov'
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 9
  end
  object DBEdit11: TDBEdit [20]
    Tag = 1
    Left = 360
    Top = 136
    Width = 81
    Height = 22
    DataField = 'dDtBomPara'
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 5
  end
  object DBEdit8: TDBEdit [21]
    Tag = 1
    Left = 192
    Top = 40
    Width = 654
    Height = 22
    DataField = 'cNmOperacao'
    DataSource = DataSource2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 10
  end
  object DBEdit9: TDBEdit [22]
    Tag = 1
    Left = 192
    Top = 64
    Width = 654
    Height = 22
    DataField = 'cNmFormaPagto'
    DataSource = DataSource3
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 11
  end
  object DBEdit10: TDBEdit [23]
    Tag = 1
    Left = 192
    Top = 88
    Width = 654
    Height = 22
    DataField = 'DadosConta'
    DataSource = DataSource4
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 12
  end
  object DBEdit12: TDBEdit [24]
    Left = 120
    Top = 112
    Width = 65
    Height = 22
    DataField = 'nCdCC'
    DataSource = DataSource1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 13
    OnExit = DBEdit12Exit
    OnKeyDown = DBEdit12KeyDown
  end
  object DBEdit13: TDBEdit [25]
    Tag = 1
    Left = 192
    Top = 112
    Width = 654
    Height = 22
    DataField = 'cNmCC'
    DataSource = DataSource5
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Calibri'
    Font.Style = []
    ParentFont = False
    TabOrder = 14
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT CB.*'
      
        '      ,CASE WHEN cFlgCaixa = 0 THEN '#39'Banco: '#39' + RTRIM(Convert(VA' +
        'RCHAR,nCdBanco)) + '#39' Ag'#234'ncia: '#39' + RTRIM(Convert(VARCHAR,cAgencia' +
        ')) + '#39' Conta: '#39' + RTRIM(nCdConta) '
      '            ELSE nCdConta'
      '       END as DadosConta'
      '  FROM ContaBancaria CB'
      ' WHERE EXISTS(SELECT 1 '
      '                FROM UsuarioContaBancaria UCB '
      '               WHERE UCB.nCdContaBancaria = CB.nCdContaBancaria'
      '                 AND UCB.nCdUsuario       = :nCdUsuario)'
      '   AND nCdEmpresa = :nCdEmpresa'
      'AND CB.nCdContaBancaria = :nPK')
    Left = 368
    Top = 152
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryContaBancarianCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryContaBancarianCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryContaBancariacFlgFluxo: TIntegerField
      FieldName = 'cFlgFluxo'
    end
    object qryContaBancariaiUltimoCheque: TIntegerField
      FieldName = 'iUltimoCheque'
    end
    object qryContaBancariaiUltimoBordero: TIntegerField
      FieldName = 'iUltimoBordero'
    end
    object qryContaBancariaDadosConta: TStringField
      FieldName = 'DadosConta'
      ReadOnly = True
      Size = 56
    end
    object qryContaBancarianValLimiteCredito: TBCDField
      FieldName = 'nValLimiteCredito'
      Precision = 12
      Size = 2
    end
    object qryContaBancariacFlgDeposito: TIntegerField
      FieldName = 'cFlgDeposito'
    end
    object qryContaBancariacFlgEmiteCheque: TIntegerField
      FieldName = 'cFlgEmiteCheque'
    end
    object qryContaBancariacAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryContaBancariacFlgCaixa: TIntegerField
      FieldName = 'cFlgCaixa'
    end
  end
  object qryOperacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '   FROM OPERACAO'
      'WHERE nCdOperacao = :nPK')
    Left = 408
    Top = 152
    object qryOperacaonCdOperacao: TIntegerField
      FieldName = 'nCdOperacao'
    end
    object qryOperacaocNmOperacao: TStringField
      FieldName = 'cNmOperacao'
      Size = 50
    end
    object qryOperacaocTipoOper: TStringField
      FieldName = 'cTipoOper'
      FixedChar = True
      Size = 1
    end
    object qryOperacaocSinalOper: TStringField
      FieldName = 'cSinalOper'
      FixedChar = True
      Size = 1
    end
    object qryOperacaocFlgApuraDRE: TIntegerField
      FieldName = 'cFlgApuraDRE'
    end
  end
  object qryFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM FORMAPAGTO'
      'WHERE cFlgPermFinanceiro = 1'
      'AND nCdFormaPagto = :nPK')
    Left = 448
    Top = 152
    object qryFormaPagtonCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryFormaPagtocNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
  end
  object qryTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTitulo'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM TITULO'
      ' WHERE nCdTitulo = :nCdTitulo')
    Left = 488
    Top = 152
    object qryTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTitulonCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryTitulonCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryTitulocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTituloiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTitulonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTitulonCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryTitulonCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryTitulocSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryTitulodDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTitulodDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
    end
    object qryTitulodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulodDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
    object qryTitulodDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryTitulodDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryTitulonValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTitulonValLiq: TBCDField
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryTitulonSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulonValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTitulonValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryTitulocObsTit: TMemoField
      FieldName = 'cObsTit'
      BlobType = ftMemo
    end
    object qryTitulonCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryTitulonCdBancoPortador: TIntegerField
      FieldName = 'nCdBancoPortador'
    end
    object qryTitulodDtRemPortador: TDateTimeField
      FieldName = 'dDtRemPortador'
    end
    object qryTitulodDtBloqTit: TDateTimeField
      FieldName = 'dDtBloqTit'
    end
    object qryTitulocObsBloqTit: TStringField
      FieldName = 'cObsBloqTit'
      Size = 50
    end
    object qryTitulonCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryTitulonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryTitulodDtIntegracao: TDateTimeField
      FieldName = 'dDtIntegracao'
    end
    object qryTitulonValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
  end
  object qryParametro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cParametro'
        DataType = ftString
        Precision = 15
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cValor'
      'FROM Parametro'
      'WHERE cParametro = :cParametro')
    Left = 528
    Top = 152
    object qryParametrocValor: TStringField
      FieldName = 'cValor'
      Size = 15
    end
  end
  object qryCheque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM Cheque')
    Left = 528
    Top = 184
    object qryChequenCdCheque: TAutoIncField
      FieldName = 'nCdCheque'
    end
    object qryChequenCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryChequenCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryChequecAgencia: TStringField
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryChequecConta: TStringField
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryChequecDigito: TStringField
      FieldName = 'cDigito'
      FixedChar = True
      Size = 1
    end
    object qryChequecCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequeiNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryChequenValCheque: TBCDField
      FieldName = 'nValCheque'
      Precision = 12
      Size = 2
    end
    object qryChequedDtDeposito: TDateTimeField
      FieldName = 'dDtDeposito'
    end
    object qryChequenCdTerceiroResp: TIntegerField
      FieldName = 'nCdTerceiroResp'
    end
    object qryChequenCdTerceiroPort: TIntegerField
      FieldName = 'nCdTerceiroPort'
    end
    object qryChequedDtRemessaPort: TDateTimeField
      FieldName = 'dDtRemessaPort'
    end
    object qryChequenCdTabTipoCheque: TIntegerField
      FieldName = 'nCdTabTipoCheque'
    end
    object qryChequecChave: TStringField
      FieldName = 'cChave'
      FixedChar = True
      Size = 35
    end
    object qryChequenCdContaBancariaDep: TIntegerField
      FieldName = 'nCdContaBancariaDep'
    end
    object qryChequedDtDevol: TDateTimeField
      FieldName = 'dDtDevol'
    end
    object qryChequecFlgCompensado: TIntegerField
      FieldName = 'cFlgCompensado'
    end
    object qryChequenCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryChequedDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryChequecNmFavorecido: TStringField
      FieldName = 'cNmFavorecido'
      Size = 50
    end
  end
  object qryMTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      '   FROM MTitulo')
    Left = 488
    Top = 112
    object qryMTitulonCdMTitulo: TAutoIncField
      FieldName = 'nCdMTitulo'
    end
    object qryMTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryMTitulonCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryMTitulonCdOperacao: TIntegerField
      FieldName = 'nCdOperacao'
    end
    object qryMTitulonCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryMTituloiNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryMTitulonValMov: TBCDField
      FieldName = 'nValMov'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMTitulodDtMov: TDateTimeField
      FieldName = 'dDtMov'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMTitulodDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryMTitulodDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryMTitulocNrDoc: TStringField
      FieldName = 'cNrDoc'
      FixedChar = True
      Size = 15
    end
    object qryMTitulodDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryMTitulonCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qryMTitulonCdUsuarioCancel: TIntegerField
      FieldName = 'nCdUsuarioCancel'
    end
    object qryMTitulocObsMov: TMemoField
      FieldName = 'cObsMov'
      BlobType = ftMemo
    end
    object qryMTitulonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMTitulocFlgMovAutomatico: TIntegerField
      FieldName = 'cFlgMovAutomatico'
    end
    object qryMTitulodDtBomPara: TDateTimeField
      FieldName = 'dDtBomPara'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMTitulonCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMTitulocFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 376
    Top = 184
  end
  object qryLanctoFin: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM LanctoFin')
    Left = 304
    Top = 272
    object qryLanctoFinnCdLanctoFin: TAutoIncField
      FieldName = 'nCdLanctoFin'
    end
    object qryLanctoFinnCdResumoCaixa: TIntegerField
      FieldName = 'nCdResumoCaixa'
    end
    object qryLanctoFinnCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryLanctoFindDtLancto: TDateTimeField
      FieldName = 'dDtLancto'
    end
    object qryLanctoFinnCdTipoLancto: TIntegerField
      FieldName = 'nCdTipoLancto'
    end
    object qryLanctoFinnValLancto: TBCDField
      FieldName = 'nValLancto'
      Precision = 12
      Size = 2
    end
    object qryLanctoFinnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryLanctoFinnCdUsuarioAutor: TIntegerField
      FieldName = 'nCdUsuarioAutor'
    end
    object qryLanctoFincFlgManual: TIntegerField
      FieldName = 'cFlgManual'
    end
    object qryLanctoFindDtEstorno: TDateTimeField
      FieldName = 'dDtEstorno'
    end
    object qryLanctoFinnCdLanctoFinPai: TIntegerField
      FieldName = 'nCdLanctoFinPai'
    end
    object qryLanctoFincMotivoEstorno: TStringField
      FieldName = 'cMotivoEstorno'
      Size = 30
    end
    object qryLanctoFincHistorico: TStringField
      FieldName = 'cHistorico'
      Size = 50
    end
    object qryLanctoFincFlgConciliado: TIntegerField
      FieldName = 'cFlgConciliado'
    end
    object qryLanctoFinnCdUsuarioConciliacao: TIntegerField
      FieldName = 'nCdUsuarioConciliacao'
    end
    object qryLanctoFindDtConciliacao: TDateTimeField
      FieldName = 'dDtConciliacao'
    end
    object qryLanctoFincDocumento: TStringField
      FieldName = 'cDocumento'
      Size = 15
    end
    object qryLanctoFinnCdCheque: TIntegerField
      FieldName = 'nCdCheque'
    end
    object qryLanctoFinnCdUsuarioEstorno: TIntegerField
      FieldName = 'nCdUsuarioEstorno'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryMTitulo
    Left = 424
    Top = 272
  end
  object DataSource2: TDataSource
    DataSet = qryOperacao
    Left = 288
    Top = 216
  end
  object DataSource3: TDataSource
    DataSet = qryFormaPagto
    Left = 440
    Top = 240
  end
  object DataSource4: TDataSource
    DataSet = qryContaBancaria
    Left = 448
    Top = 248
  end
  object qryAtualizaSaldoConta_Debito: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nValMov'
        DataType = ftFloat
        Size = -1
        Value = 0.000000000000000000
      end
      item
        Name = 'nCdContaBancaria'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nValMov DECIMAL(12,2)'
      ''
      'Set @nValMov = :nValMov'
      ''
      'UPDATE ContaBancaria'
      '   SET nSaldoConta = nSaldoConta - @nValMov'
      ' WHERE nCdContaBancaria = :nCdContaBancaria')
    Left = 592
    Top = 152
  end
  object qryAtualizaSaldoConta_Credito: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nValMov'
        DataType = ftFloat
        Size = -1
        Value = 0.000000000000000000
      end
      item
        Name = 'nCdContaBancaria'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nValMov DECIMAL(12,2)'
      ''
      'Set @nValMov = :nValMov'
      ''
      'UPDATE ContaBancaria'
      '   SET nSaldoConta = nSaldoConta + @nValMov'
      ' WHERE nCdContaBancaria = :nCdContaBancaria')
    Left = 632
    Top = 152
  end
  object SP_CONTABILIZA_MTITULO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CONTABILIZA_MTITULO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdMTitulo'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 696
    Top = 128
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmCC'
      '  FROM CentroCusto'
      ' WHERE nCdCC    = :nPK'
      '   AND iNivel   = 3'
      '   AND cFlgLanc = 1')
    Left = 696
    Top = 176
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object DataSource5: TDataSource
    DataSet = qryCC
    Left = 424
    Top = 240
  end
end
