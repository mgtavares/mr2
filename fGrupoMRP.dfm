inherited frmGrupoMRP: TfrmGrupoMRP
  Left = 21
  Top = 130
  Caption = 'Grupo de MRP'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 64
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 16
    Top = 62
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Grupo'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 20
    Top = 86
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Requisi'#231#227'o'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 15
    Top = 110
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Setor Requisi'#231#227'o'
    FocusControl = DBEdit5
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 104
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdGrupoMRP'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [7]
    Left = 104
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmGrupoMRP'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBGridEh1: TDBGridEh [8]
    Left = 8
    Top = 136
    Width = 929
    Height = 377
    DataSource = dsUsuarioGrupoMRP
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    OnEnter = DBGridEh1Enter
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdUsuarioGrupoMRP'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdGrupoMRP'
        Footers = <>
        Visible = False
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'cNmEmpresa'
        Footers = <>
        ReadOnly = True
        Tag = 1
        Width = 250
      end
      item
        EditButtons = <>
        FieldName = 'nCdLoja'
        Footers = <>
        Width = 45
      end
      item
        EditButtons = <>
        FieldName = 'cNmLoja'
        Footers = <>
        ReadOnly = True
        Tag = 1
        Width = 250
      end
      item
        EditButtons = <>
        FieldName = 'nCdUsuario'
        Footers = <>
        Width = 44
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
        ReadOnly = True
        Tag = 1
        Width = 250
      end>
  end
  object DBEdit3: TDBEdit [9]
    Left = 104
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdTipoRequisicao'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [10]
    Tag = 1
    Left = 176
    Top = 80
    Width = 650
    Height = 19
    DataField = 'cNmTipoRequisicao'
    DataSource = DataSource1
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [11]
    Left = 104
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdSetor'
    DataSource = dsMaster
    TabOrder = 5
    OnExit = DBEdit5Exit
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [12]
    Tag = 1
    Left = 176
    Top = 104
    Width = 650
    Height = 19
    DataField = 'cNmSetor'
    DataSource = DataSource2
    TabOrder = 6
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoMRP'
      'WHERE nCdGrupoMRP = :nPK')
    object qryMasternCdGrupoMRP: TIntegerField
      FieldName = 'nCdGrupoMRP'
    end
    object qryMastercNmGrupoMRP: TStringField
      FieldName = 'cNmGrupoMRP'
      Size = 50
    end
    object qryMasternCdTipoRequisicao: TIntegerField
      FieldName = 'nCdTipoRequisicao'
    end
    object qryMasternCdSetor: TIntegerField
      FieldName = 'nCdSetor'
    end
  end
  object qryUsuarioGrupoMRP: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioGrupoMRPBeforePost
    OnCalcFields = qryUsuarioGrupoMRPCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM UsuarioGrupoMRP'
      'WHERE nCdGrupoMRP = :nPK')
    Left = 728
    Top = 208
    object qryUsuarioGrupoMRPnCdUsuarioGrupoMRP: TAutoIncField
      FieldName = 'nCdUsuarioGrupoMRP'
      ReadOnly = True
    end
    object qryUsuarioGrupoMRPnCdGrupoMRP: TIntegerField
      FieldName = 'nCdGrupoMRP'
    end
    object qryUsuarioGrupoMRPnCdUsuario: TIntegerField
      DisplayLabel = 'Usu'#225'rio Autorizado|C'#243'd'
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioGrupoMRPcNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rio Autorizado|Nome Usu'#225'rio'
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 50
      Calculated = True
    end
    object qryUsuarioGrupoMRPnCdEmpresa: TIntegerField
      DisplayLabel = 'Empresa|C'#243'd'
      FieldName = 'nCdEmpresa'
    end
    object qryUsuarioGrupoMRPcNmEmpresa: TStringField
      DisplayLabel = 'Empresa|Nome Empresa'
      FieldKind = fkCalculated
      FieldName = 'cNmEmpresa'
      Size = 50
      Calculated = True
    end
    object qryUsuarioGrupoMRPnCdLoja: TIntegerField
      DisplayLabel = 'Loja|C'#243'd'
      FieldName = 'nCdLoja'
    end
    object qryUsuarioGrupoMRPcNmLoja: TStringField
      DisplayLabel = 'Loja|Nome Loja'
      FieldKind = fkCalculated
      FieldName = 'cNmLoja'
      Size = 50
      Calculated = True
    end
  end
  object dsUsuarioGrupoMRP: TDataSource
    DataSet = qryUsuarioGrupoMRP
    Left = 768
    Top = 208
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario, cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 728
    Top = 352
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object qryTipoRequisicao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoRequisicao, cNmTipoRequisicao'
      'FROM TipoRequisicao'
      'WHERE nCdTipoRequisicao = :nPK'
      'AND nCdTabTipoRequis = 2')
    Left = 752
    Top = 128
    object qryTipoRequisicaonCdTipoRequisicao: TIntegerField
      FieldName = 'nCdTipoRequisicao'
    end
    object qryTipoRequisicaocNmTipoRequisicao: TStringField
      FieldName = 'cNmTipoRequisicao'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTipoRequisicao
    Left = 592
    Top = 272
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 464
    Top = 368
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 512
    Top = 368
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qrySetor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSetor, cNmSetor'
      'FROM Setor'
      'WHERE nCdSetor = :nPK')
    Left = 824
    Top = 120
    object qrySetornCdSetor: TIntegerField
      FieldName = 'nCdSetor'
    end
    object qrySetorcNmSetor: TStringField
      FieldName = 'cNmSetor'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qrySetor
    Left = 600
    Top = 280
  end
end
