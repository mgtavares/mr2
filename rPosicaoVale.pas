unit rPosicaoVale;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls,comObj, ExcelXP, ER2Lookup;

type
  TrptPosicaoVale = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    edtTerceiro: TMaskEdit;
    edtCliente: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryTerceiro: TADOQuery;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    edtDtFinal: TMaskEdit;
    Label8: TLabel;
    edtDtInicial: TMaskEdit;
    Label9: TLabel;
    RadioGroup2: TRadioGroup;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    Label2: TLabel;
    Label3: TLabel;
    edtDtBaixaFinal: TMaskEdit;
    edtDtBaixaInicial: TMaskEdit;
    edtTipoVale: TER2LookupMaskEdit;
    Label4: TLabel;
    qryTipoVale: TADOQuery;
    dsTipoVale: TDataSource;
    qryTipoValenCdTabTipoVale: TIntegerField;
    qryTipoValecNmTabTipoVale: TStringField;
    DBEdit3: TDBEdit;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtTerceiroExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoVale: TrptPosicaoVale;

implementation

uses fMenu, fLookup_Padrao, rVendaCrediario_view, rPosicaoVale_view;

{$R *.dfm}

procedure TrptPosicaoVale.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

end;

procedure TrptPosicaoVale.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TrptPosicaoVale.edtTerceiroExit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;

end;

procedure TrptPosicaoVale.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoVale.edtTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(180);

        If (nPK > 0) then
        begin

            edtTerceiro.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoVale.ToolButton1Click(Sender: TObject);
var
  cFiltro       : string ;
  linha, coluna,LCID : integer;
  planilha      : variant;
  valorcampo    : string;
  objRel        : TrptPosicaoVale_view;
begin
  inherited;


  if (trim(edtDtInicial.Text) = '/  /') and (trim(edtDtBaixaInicial.Text) = '/  /') then
  begin
      MensagemAlerta('Selecione um per�odo.') ;
      edtDtInicial.SetFocus;
      exit;
  end ;

  objRel := TrptPosicaoVale_view.Create(nil);

  try
      try

          objRel.SPREL_POSICAO_VALE.Close;
          objRel.SPREL_POSICAO_VALE.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva ;
          objRel.SPREL_POSICAO_VALE.Parameters.ParamByName('@nCdLoja').Value           := frmMenu.ConvInteiro(edtLoja.Text) ;
          objRel.SPREL_POSICAO_VALE.Parameters.ParamByName('@nCdTerceiro').Value       := frmMenu.ConvInteiro(edtTerceiro.Text) ;
          objRel.SPREL_POSICAO_VALE.Parameters.ParamByName('@dDtEmissaoInicial').Value := frmMenu.ConvData(edtDtInicial.Text) ;
          objRel.SPREL_POSICAO_VALE.Parameters.ParamByName('@dDtEmissaoFinal').Value   := frmMenu.ConvData(edtDtFinal.Text) ;
          objRel.SPREL_POSICAO_VALE.Parameters.ParamByName('@dDtBaixaInicial').Value   := frmMenu.ConvData(edtDtBaixaInicial.Text) ;
          objRel.SPREL_POSICAO_VALE.Parameters.ParamByName('@dDtBaixaFinal').Value     := frmMenu.ConvData(edtDtBaixaFinal.Text) ;
          objRel.SPREL_POSICAO_VALE.Parameters.ParamByName('@cFlgSoValeAberto').Value  := RadioGroup2.ItemIndex ;
          objRel.SPREL_POSICAO_VALE.Parameters.ParamByName('@nCdTabTipoVale').Value    := frmMenu.ConvInteiro(edtTipoVale.Text) ;
          objRel.SPREL_POSICAO_VALE.Open;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          cFiltro := '';

          if (DBEdit1.Text <> '') then
              cFiltro := cFiltro + '/ Loja: ' + trim(edtLoja.Text) + '-' + DbEdit1.Text ;

          if (DBEdit2.Text <> '') then
              cFiltro := cFiltro + '/ Cliente: ' + trim(edtTerceiro.Text) + '-' + DbEdit2.Text ;

          cFiltro := cFiltro + ' / Per�odo Venda: ' + edtDtInicial.Text + ' a ' + edtDtFinal.Text;

          if (trim(edtDtBaixaInicial.Text) <> '/  /') then
          begin
              cFiltro := cFiltro + ' / Per�odo Baixa: ' + edtDtBaixaInicial.Text + ' a ' + edtDtBaixaFinal.Text;
          end ;

          if (qryTipoVale.IsEmpty) then
              cFiltro := cFiltro + ' / Tipo Vale: TODOS'

          else if (qryTipoValenCdTabTipoVale.Value = 1) then
              cFiltro := cFiltro + ' / Tipo Vale: MERCADORIA'

          else if  (qryTipoValenCdTabTipoVale.Value = 2) then
              cFiltro := cFiltro + ' / Tipo Vale: PRESENTE'

          else if  (qryTipoValenCdTabTipoVale.Value = 3) then
              cFiltro := cFiltro + ' / Tipo Vale: DESCONTO'

          else if  (qryTipoValenCdTabTipoVale.Value = 4) then
              cFiltro := cFiltro + ' / Tipo Vale: COMPRA WEB';

          if (RadioGroup2.ItemIndex = 0) then
              cFiltro := cFiltro + ' / Somente Vale em Aberto: SIM ' ;

          if (RadioGroup2.ItemIndex = 1) then
              cFiltro := cFiltro + ' / Somente Vale em Aberto: N�O ' ;

          objRel.lblFiltro1.Caption := cFiltro ;
          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel) ;
  end;

end;

initialization
    RegisterClass(TrptPosicaoVale) ;

end.
