unit fBanco;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, ImgList;

type
  TfrmBanco = class(TfrmCadastro_Padrao)
    qryMasternCdBanco: TIntegerField;
    qryMastercNmBanco: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBanco: TfrmBanco;

implementation

{$R *.dfm}

procedure TfrmBanco.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'BANCO' ;
  nCdTabelaSistema  := 14 ;
  nCdConsultaPadrao := 23 ;

  bCodigoAutomatico := False ;

end;

procedure TfrmBanco.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

procedure TfrmBanco.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (dbEdit1.Text = '') then
  begin
      MensagemAlerta('Informe o c�digo do banco.') ;
      dbEdit1.setFocus ;
      abort ;
  end ;

  if (dbEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o do banco.') ;
      dbEdit2.Setfocus ;
      abort ;
  end ;

  inherited;

end;

initialization
    RegisterClass(tFrmBanco) ;

end.
