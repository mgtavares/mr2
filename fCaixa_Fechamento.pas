unit fCaixa_Fechamento;

interface

uses
  ACBrECF, ACBrRFD, ACBrBase, ACBrDevice, ACBrECFClass, ACBrConsts,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, StdCtrls, cxPC, cxControls, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmCaixa_Fechamento = class(TfrmProcesso_Padrao)
    qryAtualizaTemp: TADOQuery;
    qryTemp: TADOQuery;
    dsTemp: TDataSource;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryTempnCdFormaPagto: TIntegerField;
    qryTempcNmFormaPagto: TStringField;
    qryTempnValCalculado: TBCDField;
    qryTempnValInformado: TBCDField;
    qryTempnValDiferenca: TBCDField;
    SP_FECHA_CAIXA: TADOStoredProc;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    qryTempnValExtraviado: TBCDField;
    cmdTemp: TADOCommand;
    qryTemp_Resumo_Caixa_Operadora: TADOQuery;
    qryTemp_Resumo_Caixa_OperadoranCdOperadoraCartao: TIntegerField;
    qryTemp_Resumo_Caixa_OperadoracNmOperadoraCartao: TStringField;
    qryTemp_Resumo_Caixa_OperadoranValCalculado: TBCDField;
    qryTemp_Resumo_Caixa_OperadoranValInformado: TBCDField;
    qryTemp_Resumo_Caixa_Condicao: TADOQuery;
    qryTemp_Resumo_Caixa_CondicaonCdOperadoraCartao: TIntegerField;
    qryTemp_Resumo_Caixa_CondicaocNmCondicao: TStringField;
    qryTemp_Resumo_Caixa_CondicaoiParcelas: TIntegerField;
    qryTemp_Resumo_Caixa_CondicaonValCalculado: TBCDField;
    qryTemp_Resumo_Caixa_CondicaonValInformado: TBCDField;
    dsTemp_Resumo_Caixa_Operadora: TDataSource;
    dsTemp_Resumo_Caixa_Condicao: TDataSource;
    qryTemp_Resumo_Caixa_OperadoracFlgTipoOperacao: TStringField;
    qryAmbiente: TADOQuery;
    qryAmbientecFlgUsaECF: TIntegerField;
    qryAmbientecModeloECF: TStringField;
    qryAmbientecPortaECF: TStringField;
    qryAmbienteiViasECF: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxPageControl2: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cxPageControl3: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    DBGridEh3: TDBGridEh;
    qryTempnValDifReal: TBCDField;
    qryValidaLanctos: TADOQuery;
    qryValidaLanctosiRetorno: TIntegerField;
    qryValidaTipoLanctoAuto: TADOQuery;
    qryValidaTipoLanctoAutocSinalOper: TStringField;
    procedure FormShow(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure qryTempBeforePost(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure qryTemp_Resumo_Caixa_OperadoraAfterScroll(DataSet: TDataSet);
    procedure qryTemp_Resumo_Caixa_CondicaoAfterPost(DataSet: TDataSet);
    procedure qryTemp_Resumo_Caixa_CondicaoBeforePost(DataSet: TDataSet);
    procedure qryTempAfterScroll(DataSet: TDataSet);
    procedure ReducaoZ;
    procedure BorderoCaixa;
    procedure ToolButton2Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
  private
    { Private declarations }
    iContaTentativa : Integer;
  public
    { Public declarations }
    cFlgUsaECF       : boolean ;
    nCdResumoCaixa   : integer ;
    nCdContaBancaria : integer ;
    dDtAbertura      : TDateTime;
    cNmCaixa         : string ;
  end;

const
  Estados : array[TACBrECFEstado] of string = ('N�o Inicializada', 'Desconhecido', 'Livre', 'Venda', 'Pagamento', 'Relat�rio', 'Bloqueada', 'Requer Z', 'Requer X', 'Nao Fiscal' );

var
  frmCaixa_Fechamento        : TfrmCaixa_Fechamento;
  nValSobraDif               : double ;
  nValFaltaDif               : double ;
  cPermiteSobraCaixaDinheiro : string ;

implementation

uses fMenu, fPdvSupervisor, rBorderoCaixa_view, fCaixa_StatusDocto,
  fCaixa_Recebimento, rBorderoCaixaNovo, fCaixa_LanctoMan, fRemessa_Numerario;

{$R *.dfm}

procedure TfrmCaixa_Fechamento.FormShow(Sender: TObject);
begin
  inherited;

    iContaTentativa := 1; // tentativas de fechar o caixa

    ToolButton1.Enabled := True ;
    ToolButton5.Enabled := True ;
    ToolButton7.Enabled := True ;
    DbGridEh1.Enabled   := True ;


    DbGridEh1.Columns[3].Visible := False ;
    DbGridEh1.Columns[4].Visible := False ;
    DbGridEh1.Columns[5].Visible := False ;

    DbGridEh2.Columns[3].Visible := False ;
    DbGridEh3.Columns[4].Visible := False ;

    if (nCdResumoCaixa = 0) then
    begin

        ToolButton1.Enabled := False ;
        ToolButton5.Enabled := False ;
        ToolButton7.Enabled := False ;
        DbGridEh1.Enabled   := False ;

        MensagemAlerta('Nenhum caixa aberto.') ;
        close ;
        exit ;
    end ;

    if (cFlgUsaECF) then
    begin

        try
            frmMenu.ACBrECF1.Ativar;
        except
            frmMenu.ACBrECF1.Modelo := TACBrECFModelo(0) ;
            frmMenu.ACBrECF1.Porta  := 'Procurar' ;

            cFlgUsaECF := True ;

            if not frmMenu.ACBrECF1.AcharECF(true,False) then
            begin
                cFlgUsaECF := False ;
                MessageDlg('Nenhuma Impressora ECF conectada.',mtInformation,[mbOk],0) ;
                exit ;
            end
            else frmMenu.ACBrECF1.Ativar ;

        end ;

    end ;

    cmdTemp.Execute;

    qryAtualizaTemp.Close ;
    qryAtualizaTemp.Parameters.ParamByName('nPK').Value := nCdContaBancaria ;
    qryAtualizaTemp.ExecSQL;

    qryTemp.Close ;
    qryTemp.Open ;

    qryTemp_Resumo_Caixa_Operadora.Close;
    qryTemp_Resumo_Caixa_Operadora.Open ;

    qryTemp_Resumo_Caixa_Condicao.Close ;
    qryTemp_Resumo_Caixa_Condicao.Open ;

    { -- processo retirado devido nova forma de valida��o de diferen�a no fechamento -- }
    //nValTolerancia := StrToFloat(frmMenu.LeParametro('LIMITEDIFCXDIN')) ;
    //cPermiteSobraCaixaDinheiro := frmMenu.LeParametro('PERMSOBRACXDIN');
    //if (cPermiteSobraCaixaDinheiro = '') Then cPermiteSobraCaixaDinheiro := 'N';

    { -- busca valores sobre diferen�a no caixa -- }
    nValFaltaDif := StrToFloat(frmMenu.LeParametro('PERMFALTACXDIN'));
    nValSobraDif := StrToFloat(frmMenu.LeParametro('PERMSOBRACXDIN'));

    DBGridEh1.SetFocus;
end;

procedure TfrmCaixa_Fechamento.ToolButton5Click(Sender: TObject);
var
  objCaixa : TfrmCaixa_Recebimento ;
begin
  inherited;

  objCaixa := TfrmCaixa_Recebimento.Create( Self ) ;

  try
      if not objCaixa.AutorizacaoGerente(31) then
      begin
          MensagemAlerta('Autentica��o falhou.') ;
          exit ;
      end ;
  finally

      freeAndNil( objCaixa ) ;

  end ;

  DbGridEh1.Columns[3].Visible := True ;
  DbGridEh1.Columns[4].Visible := True ;
  DbGridEh1.Columns[5].Visible := True ;

  DbGridEh2.Columns[3].Visible := True ;
  DbGridEh3.Columns[4].Visible := True ;

  DBGridEh1.SetFocus ;

end;


procedure TfrmCaixa_Fechamento.qryTempBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryTempnValDiferenca.Value := qryTempnValCalculado.Value - qryTempnValInformado.Value;
  qryTempnValDifReal.Value   := qryTempnValInformado.Value - qryTempnValCalculado.Value;
end;

procedure TfrmCaixa_Fechamento.ToolButton1Click(Sender: TObject);
var
  nValDiferenca       : Double; //vari�vel utilizada para finaliza��o do fechamento (com altera��o no sinal)
  nValDifReal         : Double; //vari�vel utilizada para valida��o do fechamento   (sem altera��o no sinal)
  bGerarLancAjuste    : Boolean;
  objCaixa_Receb      : TfrmCaixa_Recebimento;
  objCaixa_LanctoMan  : TfrmCaixa_LanctoMan;
  nCdTipoLancAjustDeb : Integer;
  cMsgFechamento      : String;
  objRemessaNum       : TfrmRemessa_Numerarios;
begin
  inherited;

  nValDiferenca       := 0;
  nValDifReal         := 0;
  nCdTipoLancAjustDeb := StrToIntDef(frmMenu.LeParametro('TIPOLANCAUTCX'),0);
  bGerarLancAjuste    := False;
  cMsgFechamento      := 'Confirma o fechamento do caixa ?';
  
  // Verifica se houve algum lan�amento no caixa ap�s o in�cio do processo de fechamento
  qryValidaLanctos.Close;
  qryValidaLanctos.Parameters.ParamByName('nCdContaBancaria').Value := nCdContaBancaria;
  qryValidaLanctos.Open;

  if not (qryValidaLanctos.IsEmpty) then
  begin
      MensagemAlerta('O caixa possui lan�amentos contabilizados ap�s o in�cio do processo de fechamento.');

      if ((MessageDlg('Deseja recalcular os totais ? Todos os valores informados ser�o apagados.',mtConfirmation,[mbYes,mbNo],0)) = mrYes) then
      begin
          qryAtualizaTemp.Close ;
          qryAtualizaTemp.Parameters.ParamByName('nPK').Value := nCdContaBancaria ;
          qryAtualizaTemp.ExecSQL;

          qryTemp.Close ;
          qryTemp.Open ;

          qryTemp_Resumo_Caixa_Operadora.Close;
          qryTemp_Resumo_Caixa_Operadora.Open ;

          qryTemp_Resumo_Caixa_Condicao.Close ;
          qryTemp_Resumo_Caixa_Condicao.Open ;
      end;

      Abort;

  end;

  // Realiza as consist�ncias
  qryTemp.First;

  while (not qryTemp.Eof) do
  begin

      qryTemp.Edit;
      qryTempnValDiferenca.Value := qryTempnValCalculado.Value - qryTempnValInformado.Value;
      qryTempnValDifReal.Value   := qryTempnValInformado.Value - qryTempnValCalculado.Value;
      qryTemp.Post;

      if (qryTempnCdFormaPagto.Value = 1) then
      begin
          nValDiferenca := nValDiferenca + qryTempnValDiferenca.Value;
          nValDifReal   := nValDifReal   + qryTempnValDifReal.Value;
      end ;

      {-- se for diferente de dinhero, cartao de credito ou debito, faz a consistencia --}
      if (qryTempnCdFormaPagto.Value <> 1) and (qryTempnCdFormaPagto.Value <> 3) and (qryTempnCdFormaPagto.Value <> 4) and (qryTempnValDiferenca.Value <> 0) then
      begin
          MensagemAlerta('N�o � permitido nenhum tipo de diferen�a nos documentos.') ;
          exit ;
      end ;

      qryTemp.Next;
  end ;

  qryTemp.First;

  { -- processo retirado devido nova forma de valida��o de diferen�a no fechamento -- }
  //if (abs(nValDiferenca) > nValTolerancia) then
  //begin
  //    if ( ((nValDiferenca < 0) and (cPermiteSobraCaixaDinheiro = 'N')) OR (nValDiferenca >=0) ) Then
  //    begin
  //        MensagemAlerta('Caixa com diferen�a no dinheiro, imposs�vel fechar.') ;
  //        exit ;
  //    end;

  qryTemp_Resumo_Caixa_Operadora.First ;

  while not (qryTemp_Resumo_Caixa_Operadora.Eof) do
  begin

      if (qryTemp_Resumo_Caixa_OperadoranValInformado.Value <> qryTemp_Resumo_Caixa_OperadoranValCalculado.Value) then
      begin
          MensagemAlerta('Diferen�a no total de comprovantes por Operadora.' + #13#13 + 'Operadora : ' + qryTemp_Resumo_Caixa_OperadoracNmOperadoraCartao.Value) ;
          abort ;
      end ;

      qryTemp_Resumo_Caixa_Condicao.First ;

      while not qryTemp_Resumo_Caixa_Condicao.Eof do
      begin

          if (qryTemp_Resumo_Caixa_CondicaonValInformado.Value <> qryTemp_Resumo_Caixa_CondicaonValCalculado.Value) then
          begin
              MensagemAlerta('Diferen�a no total de comprovantes por Condi��o.' + #13#13 + 'Operadora : ' + qryTemp_Resumo_Caixa_OperadoracNmOperadoraCartao.Value + '    Condi��o : ' + qryTemp_Resumo_Caixa_CondicaocNmCondicao.Value + '  N�m. de Parcelas: ' + qryTemp_Resumo_Caixa_CondicaoiParcelas.AsString) ;
              abort ;
          end ;

          qryTemp_Resumo_Caixa_Condicao.Next ;
      end ;

      qryTemp_Resumo_Caixa_Condicao.First ;

      qryTemp_Resumo_Caixa_Operadora.Next ;
  end ;

  qryTemp_Resumo_Caixa_Operadora.First ;

  { -- verifica se valor da diferen�a real se enquadra nos par�metros de Falta/Sobra no caixa -- }
  if (nValDifReal <> 0) then
  begin
      { -- valida sobra no caixa -- }
      if ((nValDifReal > Abs(nValSobraDif)) and (nValDifReal > 0)) then
      begin
          MensagemAlerta('Caixa com diferen�a no dinheiro, imposs�vel fechar.');
          Exit;
      end;

      { -- valida falta no caixa -- }
      if (Abs(nValDifReal) > Abs(nValFaltaDif)) and (nValDifReal < 0) then
      begin
          { -- se n�o gera ajuste de d�b. autom�tico, finaliza processo -- }
          if (nCdTipoLancAjustDeb = 0) then
          begin
              MensagemAlerta('Caixa com diferen�a no dinheiro, imposs�vel fechar.');
              Exit;
          end
          else
          begin
              { -- verifica opera��o do tipo de lan�amento -- }
              qryValidaTipoLanctoAuto.Close;
              PosicionaQuery(qryValidaTipoLanctoAuto, IntToStr(nCdTipoLancAjustDeb));

              if (qryValidaTipoLanctoAuto.IsEmpty) then
              begin
                  MensagemErro('Parametro TIPOLANCAUTCX n�o configurado corretamente.');
                  Exit;
              end;

              if (qryValidaTipoLanctoAutocSinalOper.Value <> '-') then
              begin
                  MensagemErro('Sinal de opera��o do tipo de lan�amento de ajuste de d�bito autom�tico deve ser classificado como subtra��o (-).');
                  Exit;
              end;

              if (iContaTentativa < 4) then
              begin
                  MensagemAlerta('Caixa com diferen�a no dinheiro, imposs�vel fechar.' + #13 + IntToStr(iContaTentativa) + '� tentativa.');

                  if (iContaTentativa = 3) then
                      MensagemAlerta('Na pr�xima tentativa ser� realizado um lan�amento autom�tico de ajuste de d�bito em seu caixa no valor de R$ ' + FormatFloat('#,##0.00', Abs(nValDiferenca)) + '.');

                  Inc(iContaTentativa);
                  Exit;
              end;

              if (iContaTentativa = 4) then
              begin
                  cMsgFechamento   := 'Confirma o fechamento do caixa com o lan�amento autom�tico de ajuste de d�bito no valor de R$ ' + FormatFloat('#,##0.00', Abs(nValDiferenca)) + ' ?';
                  bGerarLancAjuste := True;
              end;
          end;
      end;
  end;

  if (MessageDlg(cMsgFechamento, mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      Exit;

  if (bGerarLancAjuste) then
  begin
      try
          objCaixa_Receb := TfrmCaixa_Recebimento.Create(nil);

          if (not objCaixa_Receb.AutorizacaoGerente(37)) then
          begin
              MensagemAlerta('Autentica��o falhou.');
              Exit;
          end;

          iContaTentativa := 1;
      finally
          FreeAndNil(objCaixa_Receb);
      end;
  end;

  frmMenu.Connection.BeginTrans;

  try
      { -- se gera lan�amento de ajuste, contabiliza o mesmo e zera valor de diferen�a -- }
      if (bGerarLancAjuste) then
      begin
          try
              objCaixa_LanctoMan := TfrmCaixa_LanctoMan.Create(nil);

              objCaixa_LanctoMan.prProcessaLanctoManual(True, nCdResumoCaixa, nCdTipoLancAjustDeb, Abs(nValDiferenca), 'AJUSTE DE D�BITO AUTOM�TICO FECH. CAIXA');
          finally
              FreeAndNil(objCaixa_LanctoMan);
          end;
          
          nValDiferenca := 0;
      end;

      SP_FECHA_CAIXA.Close ;
      SP_FECHA_CAIXA.Parameters.ParamByName('@nCdResumoCaixa').Value := nCdResumoCaixa ;
      SP_FECHA_CAIXA.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
      SP_FECHA_CAIXA.Parameters.ParamByName('@nValDiferenca').Value  := nValDiferenca ;
      SP_FECHA_CAIXA.ExecProc ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  ToolButton1.Enabled := False ;

  if (MessageDlg('Deseja emitir o border� do caixa ?',mtConfirmation,[mbYes,mbNo],0) = mrYes) then
     BorderoCaixa;

  if (frmMenu.LeParametro('SUGREDUCAOZ') = 'S') and (cFlgUsaECF) then
      if (MessageDlg('Deseja emitir a redu��o Z ?',mtConfirmation,[mbYes,mbNo],0) = MrYes) then
          ReducaoZ;

  {-- Verifica se o parametro esta settado para vir autom�tico --}
  if (frmMenu.LeParametro('GERAREMAUTO') = 'S') then
  begin
      objRemessaNum := TfrmRemessa_Numerarios.Create(Nil);
      objRemessaNum.prGeraRemessAauto(nCdContaBancaria);
      frmMenu.showForm(objRemessaNum, true);
  end;

  nCdResumoCaixa := -1 ;
  Close ;

end;

procedure TfrmCaixa_Fechamento.ToolButton7Click(Sender: TObject);
var
  objForm  : TfrmCaixa_StatusDocto ;
  objCaixa : TfrmCaixa_Recebimento ;
begin
  inherited;

  objCaixa := TfrmCaixa_Recebimento.Create( Self ) ;

  try
      if not objCaixa.AutorizacaoGerente(32) then
      begin
          MensagemAlerta('Autentica��o falhou.') ;
          exit ;
      end ;
  finally

      freeAndNil( objCaixa )

  end ;

  objForm := TfrmCaixa_StatusDocto.Create( Self ) ;

  PosicionaQuery(objForm.qryCheque   , intToStr(nCdContaBancaria)) ;
  PosicionaQuery(objForm.qryCartao   , intToStr(nCdContaBancaria)) ;
  PosicionaQuery(objForm.qryCrediario, intToStr(nCdContaBancaria)) ;
  PosicionaQuery(objForm.qryLanctoMan, intToStr(nCdResumoCaixa)) ;

  objForm.bAlteracao := False ;

  showForm( objForm , FALSE ) ;

  if (objForm.bAlteracao) then
  begin
  
      qryAtualizaTemp.Close ;
      qryAtualizaTemp.Parameters.ParamByName('nPK').Value := nCdContaBancaria ;
      qryAtualizaTemp.ExecSQL;

      qryTemp.Close ;
      qryTemp.Open ;

      qryTemp_Resumo_Caixa_Operadora.Close;
      qryTemp_Resumo_Caixa_Operadora.Open ;

      qryTemp_Resumo_Caixa_Condicao.Close ;
      qryTemp_Resumo_Caixa_Condicao.Open ;

  end ;

  freeAndNil( objForm ) ;
  
  DBGridEh1.SetFocus;

end;

procedure TfrmCaixa_Fechamento.qryTemp_Resumo_Caixa_OperadoraAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryTemp_Resumo_Caixa_Condicao, qryTemp_Resumo_Caixa_OperadoranCdOperadoraCartao.AsString) ;
  
end;

procedure TfrmCaixa_Fechamento.qryTemp_Resumo_Caixa_CondicaoAfterPost(
  DataSet: TDataSet);
begin
  inherited;

  qryTemp_Resumo_Caixa_Operadora.Post ;
  qryTemp.Post ;

end;

procedure TfrmCaixa_Fechamento.qryTemp_Resumo_Caixa_CondicaoBeforePost(
  DataSet: TDataSet);
begin
  inherited;
  qryTemp_Resumo_Caixa_Operadora.Edit ;
  qryTemp_Resumo_Caixa_OperadoranValInformado.Value := qryTemp_Resumo_Caixa_OperadoranValInformado.Value - StrToFloat(qryTemp_Resumo_Caixa_CondicaonValInformado.OldValue) ;
  qryTemp_Resumo_Caixa_OperadoranValInformado.Value := qryTemp_Resumo_Caixa_OperadoranValInformado.Value + qryTemp_Resumo_Caixa_CondicaonValInformado.Value ;

  if (qryTemp_Resumo_Caixa_OperadoracFlgTipoOperacao.Value = 'C') then
      qryTemp.Locate('nCdFormaPagto','4',[loCaseInsensitive,loPartialKey]);

  if (qryTemp_Resumo_Caixa_OperadoracFlgTipoOperacao.Value = 'D') then
      qryTemp.Locate('nCdFormaPagto','3',[loCaseInsensitive,loPartialKey]);

  qryTemp.Edit ;
  qryTempnValInformado.Value := qryTempnValInformado.Value + qryTemp_Resumo_Caixa_CondicaonValInformado.Value ;
  qryTempnValInformado.Value := qryTempnValInformado.Value - StrToFloat(qryTemp_Resumo_Caixa_CondicaonValInformado.OldValue) ;

end;

procedure TfrmCaixa_Fechamento.qryTempAfterScroll(DataSet: TDataSet);
begin
  inherited;

  DBGridEh1.Columns[2].ReadOnly         := False ;
  DBGridEh1.Columns[2].Title.Font.Color := clBlack ;

  if ((qryTempnCdFormaPagto.Value = 3) or (qryTempnCdFormaPagto.Value = 4)) then
  begin
      DBGridEh1.Columns[2].ReadOnly         := True ;
      DBGridEh1.Columns[2].Title.Font.Color := clRed ;
  end ;

end;

procedure TfrmCaixa_Fechamento.ReducaoZ;
var
    cEstadoECF : string ;
begin

  cEstadoECF :=  Estados[ frmMenu.ACBrECF1.Estado ] ;

  if (cEstadoECF = 'N�o Inicializada') then
  begin
      MensagemAlerta('Impressora Fiscal Nao Inicializada.') ;
      exit ;
  end ;

  if (cEstadoECF = 'Bloqueada') then
  begin
      MensagemAlerta('Impressora Fiscal est� Bloqueada.' +#13#13 + '�ltima Redu��o Z em: ' + frmMenu.ACBrECF1.DadosUltimaReducaoZ) ;
      exit ;
  end ;

  if (cEstadoECF = 'Desconhecido') then
  begin
      MensagemAlerta('Impressora Fiscal Nao Inicializada. Status Desconhecido.') ;
      exit ;
  end ;

  if (cEstadoECF = 'Nao Fiscal') then
  begin
      MensagemAlerta('Impressora encontrada n�o � uma impressora fiscal.') ;
      exit ;
  end ;

  if (MessageDlg('Este processo ir� bloquear a impressora ECF e s� permitir� movimentos amanh�.' +#13#13 + 'Confirma ?',mtConfirmation,[mbYes,mbNo],0) = MRNO) then
      exit ;

  frmMenu.ACBrECF1.CorrigeEstadoErro(True);
  frmMenu.ACBrECF1.ReducaoZ(Now());

  ShowMessage('Redu��o Z realizada com sucesso.' + #13#13 + 'ECF Bloqueada at� as 00:00hrs.') ;
end;

procedure TfrmCaixa_Fechamento.BorderoCaixa;
var
    objRel : TrptBorderoCaixaNovo ;
begin

  objRel := TrptBorderoCaixaNovo.Create( Self ) ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.BorderoIndividual(nCdResumoCaixa)

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmCaixa_Fechamento.ToolButton2Click(Sender: TObject);
begin
  DbGridEh1.Columns[3].Visible := False ;
  DbGridEh1.Columns[4].Visible := False ;
  DbGridEh1.Columns[5].Visible := False ;

  DbGridEh2.Columns[3].Visible := False ;
  DbGridEh3.Columns[4].Visible := False ;

  inherited;


end;

procedure TfrmCaixa_Fechamento.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmCaixa_Fechamento.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmCaixa_Fechamento.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if ((qryTempnCdFormaPagto.Value = 3) or (qryTempnCdFormaPagto.Value = 4)) and (DataCol = 2) then
  begin

     DBGridEh1.Canvas.Font.Color := clTeal;
     {DBGridEh1.Canvas.Font.Color  := clWhite ;}

     DBGridEh1.Canvas.FillRect(Rect);
     DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);

  end ;

end;

initialization
    RegisterClass(TfrmCaixa_Fechamento) ;

end.
