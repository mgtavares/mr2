unit fModulo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, Grids, DBGrids, ADODB, StdCtrls, DBCtrls, Buttons, GridsEh,
  DBGridEh, Mask, DBCtrlsEh, DBLookupEh, ComCtrls, ToolWin, ExtCtrls,
  ImgList;

type
  TfrmModulo = class(TForm)
    qryModulo: TADOQuery;
    dsModulo: TDataSource;
    qryModulonCdModulo: TIntegerField;
    qryModulocNmModulo: TStringField;
    qryModulonCdStatus: TIntegerField;
    Image1: TImage;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    ImageList1: TImageList;
    DBLookupComboboxEh1: TDBLookupComboboxEh;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    dsStatus: TDataSource;
    Panel1: TPanel;
    ToolBar1: TToolBar;
    ToolButton5: TToolButton;
    btexit: TToolButton;
    DBNavigator1: TDBNavigator;
    procedure FormCreate(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure btexitClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmModulo: TfrmModulo;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmModulo.FormCreate(Sender: TObject);
var
    I : Integer ;
    x : Integer ;
begin
    RegisterClass(TfrmModulo) ;

  for I := 0 to ComponentCount - 1 do
  begin
    if (Components [I] is TDBEdit) then
    begin
      (Components [I] as TDBEdit).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TDBEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBEdit).CharCase    := ecUpperCase ;
      (Components [I] as TDBEdit).Color       := clWhite ;
      (Components [I] as TDBEdit).Height      := 15 ;
      (Components [I] as TDBEdit).BevelInner  := bvSpace ;
      (Components [I] as TDBEdit).BevelKind   := bkNone ;
      (Components [I] as TDBEdit).BevelOuter  := bvLowered ;
      (Components [I] as TDBEdit).Ctl3D       := True    ;
      (Components [I] as TDBEdit).BorderStyle := bsSingle ;
      (Components [I] as TDBEdit).Font.Name   := 'Tahoma' ;
      (Components [I] as TDBEdit).Font.Size   := 8 ;

      if ((Components [I] as TDBEdit).Tag = 1) then
      begin
          (Components [I] as TDBEdit).Enabled := False ;
          (Components [I] as TDBEdit).Color   := clSilver ;
      end ;

    end;

    if (Components [I] is TEdit) then
    begin
      (Components [I] as TEdit).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      (Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 15 ;
      (Components [I] as TEdit).Font.Name   := 'Tahoma' ;
      (Components [I] as TEdit).Font.Size   := 8 ;

    end;

    if (Components [I] is TDBLookupComboBox) then
    begin
      (Components [I] as TDBLookupComboBox).OnEnter  := frmMenu.emFoco ;
      (Components [I] as TDBLookupComboBox).OnExit   := frmMenu.semFoco ;
      (Components [I] as TDBLookupComboBox).Color    := clWhite ;
      (Components [I] as TDBLookupComboBox).BevelInner := bvSpace   ;
      (Components [I] as TDBLookupComboBox).BevelKind  := bkNone    ;
      (Components [I] as TDBLookupComboBox).BevelOuter := bvLowered ;
      (Components [I] as TDBLookupComboBox).Ctl3D      := True      ;
      (Components [I] as TDBLookupComboBox).Font.Name   := 'Arial' ;
      (Components [I] as TDBLookupComboBox).Font.Size   := 8 ;

    end;

    if (Components [I] is TLabel) then
    begin
       (Components [I] as TLabel).Font.Name := 'Courier New' ;
       (Components [I] as TLabel).Font.Size := 8 ;
       (Components [I] as TLabel).Transparent  := True ;
    end ;

    if (Components [I] is TButton) then
    begin
       (Components [I] as TButton).ParentFont := False ;
       (Components [I] as TButton).Font.Name  := 'Arial' ;
       (Components [I] as TButton).Font.Size  := 8 ;
       (Components [I] as TButton).Font.Color := clBlue ;
       (Components [I] as TButton).Default    := False ;
       (Components [I] as TButton).Update ;
    end ;

    if (Components [I] is TBitBtn) then
    begin
       (Components [I] as TBitBtn).ParentFont := False ;
       (Components [I] as TBitBtn).Font.Name  := 'Arial' ;
       (Components [I] as TBitBtn).Font.Size  := 8 ;
       (Components [I] as TBitBtn).Font.Color := clBlue ;
       (Components [I] as TBitBtn).Default    := False ;
       (Components [I] as TBitBtn).Update ;
    end ;

    //if (Components [I] is TDBGrid) then
   // begin
    //   (Components [I] as TDBGrid).ParentFont := False ;
  //     (Components [I] as TDBGrid).Font.Name  := 'Tahoma' ;
    //   (Components [I] as TDBGrid).Font.Size  := 8 ;
      // (Components [I] as TDBGrid).Font.Color := clBlack ;
 //      (Components [I] as TDBGrid).Color      := clWhite ;
   //    (Components [I] as TDBGrid).TitleFont.Name := 'Tahoma' ;
     //  (Components [I] as TDBGrid).TitleFont.Size := 10 ;
       //(Components [I] as TDBGrid).TitleFont.Color := clBlue ;

       // grid de consulta
       //if ((Components [I] as TDBGrid).Tag = 0) then
       //    (Components [I] as TDBGrid).Options := [dgTitles,dgColLines,dgRowLines,dgTabs,dgRowSelect,dgConfirmDelete,dgCancelOnExit] ;

       //for x := 0 to ((Components [I] as TDBGrid).Columns.Count  - 1) do
       //    (Components [I] as TDBGrid).Columns.Items[x].Title.Color := clAqua ;

//       (Components [I] as TDBGrid).Update ;
  //  end ;

  end;

  qryModulo.Close ;
  qryModulo.Open ;
//frmMenu.GridZebrado (TDBGrid(Sender).DataSource.DataSet.RecNo, TDBGrid(Sender), Rect, Column, State);
end;

procedure TfrmModulo.Button5Click(Sender: TObject);
begin
Close;
end;

procedure TfrmModulo.BitBtn5Click(Sender: TObject);
begin
    frmModulo.Close ;
end;

procedure TfrmModulo.btexitClick(Sender: TObject);
begin
    Close ;
end;

end.
