inherited rptItensPorVenda: TrptItensPorVenda
  Left = 358
  Top = 256
  Width = 748
  Height = 235
  Caption = 'Rel. Itens por Venda - Vendedor'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 732
    Height = 173
  end
  object Label5: TLabel [1]
    Left = 80
    Top = 48
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label1: TLabel [2]
    Left = 54
    Top = 71
    Width = 46
    Height = 13
    Alignment = taRightJustify
    Caption = 'Vendedor'
  end
  object Label3: TLabel [3]
    Left = 9
    Top = 96
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Movimento'
  end
  object Label6: TLabel [4]
    Left = 188
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    Width = 732
    TabOrder = 4
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 172
    Top = 40
    Width = 541
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 5
  end
  object DBEdit2: TDBEdit [7]
    Tag = 1
    Left = 172
    Top = 64
    Width = 541
    Height = 21
    DataField = 'cNmUsuario'
    DataSource = dsVendedor
    TabOrder = 6
  end
  object MaskEdit1: TMaskEdit [8]
    Left = 104
    Top = 88
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [9]
    Left = 216
    Top = 88
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object er2LkpLoja: TER2LookupMaskEdit [10]
    Left = 104
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  object er2LkpVendedor: TER2LookupMaskEdit [11]
    Left = 104
    Top = 64
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    OnBeforeLookup = er2LkpVendedorBeforeLookup
    CodigoLookup = 703
    QueryLookup = qryVendedor
  end
  object RadioGroup1: TRadioGroup [12]
    Left = 104
    Top = 120
    Width = 185
    Height = 41
    Caption = ' Exibir Somente Vendedores Ativos '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'N'#227'o'
      'Sim')
    ParentColor = False
    TabOrder = 7
  end
  object rgModeloImp: TRadioGroup [13]
    Left = 295
    Top = 120
    Width = 185
    Height = 41
    Caption = ' Modelo '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Planilha')
    ParentColor = False
    TabOrder = 8
  end
  inherited ImageList1: TImageList
    Left = 416
    Top = 104
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryLojaBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja'
      '               WHERE nCdLoja    = Loja.nCdLoja'
      '                 AND nCdUsuario = :nCdUsuario)'
      '')
    Left = 352
    Top = 104
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 352
    Top = 136
  end
  object qryVendedor: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryVendedorBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '1'
      end>
    SQL.Strings = (
      'SELECT Usuario.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '  FROM Usuario'
      ' WHERE nCdUsuario               = :nPK'
      '   AND Usuario.nCdStatus        = 1'
      '   AND (Usuario.cFlgVendedor    = 1 OR Usuario.cFlgGerente  = 1)'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdUsuario = Usuario.nCdUsuario'
      '                 AND UL.nCdLoja    = :nCdLoja)'
      ' ORDER BY Usuario.cNmUsuario')
    Left = 384
    Top = 104
    object qryVendedornCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryVendedorcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsVendedor: TDataSource
    DataSet = qryVendedor
    Left = 384
    Top = 136
  end
  object ER2Excel: TER2Excel
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 424
    Top = 136
  end
end
