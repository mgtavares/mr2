unit rPosicaoEstoqueSimples_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosicaoEstoqueSimples_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRLabel4: TQRLabel;
    QRShape2: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel11: TQRLabel;
    SPREL_POSICAO_ESTOQUE_RESUMIDO: TADOStoredProc;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    SummaryBand1: TQRBand;
    QRShape6: TQRShape;
    QRExpr3: TQRExpr;
    QRLabel20: TQRLabel;
    QRBand2: TQRBand;
    QRLabel9: TQRLabel;
    QRExpr2: TQRExpr;
    QRDBText25: TQRDBText;
    QRLabel33: TQRLabel;
    QRDBText26: TQRDBText;
    QRLabel34: TQRLabel;
    QRDBText27: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText28: TQRDBText;
    QRLabel37: TQRLabel;
    QRDBText20: TQRDBText;
    QRLabel28: TQRLabel;
    QRDBText22: TQRDBText;
    QRLabel30: TQRLabel;
    QRDBText21: TQRDBText;
    QRLabel29: TQRLabel;
    QRDBText24: TQRDBText;
    QRLabel32: TQRLabel;
    QRDBText23: TQRDBText;
    QRLabel31: TQRLabel;
    QRDBText11: TQRDBText;
    QRBand4: TQRBand;
    QRLabel10: TQRLabel;
    QRExpr1: TQRExpr;
    QRDBText12: TQRDBText;
    QRBand6: TQRBand;
    QRLabel12: TQRLabel;
    QRExpr4: TQRExpr;
    QRDBText13: TQRDBText;
    QRBand7: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr5: TQRExpr;
    QRDBText29: TQRDBText;
    QRGroup1: TQRGroup;
    QRGroup5: TQRGroup;
    QRGroup6: TQRGroup;
    QRLabel21: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel38: TQRLabel;
    QRExpr6: TQRExpr;
    QRLabel39: TQRLabel;
    SPREL_POSICAO_ESTOQUE_RESUMIDOnCdTempResultado: TAutoIncField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOcNmProduto: TStringField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOnCdProduto: TIntegerField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOcReferencia: TStringField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOcNmMarca: TStringField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOnQtdeEstoque: TBCDField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOnValCusto: TBCDField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOnValVenda: TBCDField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOcNmDepartamento: TStringField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOcNmCategoria: TStringField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOcNmSubCategoria: TStringField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOcNmSegmento: TStringField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOnCdCampanhaPromoc: TIntegerField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOdDtUltReceb: TDateTimeField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOdDtUltVenda: TDateTimeField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOnValTotalCusto: TBCDField;
    SPREL_POSICAO_ESTOQUE_RESUMIDOnValTotalVenda: TBCDField;
    DetailBand1: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText17: TQRDBText;
    QRLabel7: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoEstoqueSimples_view: TrptPosicaoEstoqueSimples_view;

implementation

{$R *.dfm}

end.
