unit fPdvValeMerc;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit, DB, ADODB, jpeg, ExtCtrls;

type
  TfrmPDVValeMerc = class(TForm)
    Image1: TImage;
    qryVale: TADOQuery;
    qryValenValVale: TBCDField;
    qryValenSaldoVale: TBCDField;
    qryValedDtEstorno: TDateTimeField;
    DataSource1: TDataSource;
    Label2: TLabel;
    edtTipo: TcxCurrencyEdit;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    qryValenCdVale: TAutoIncField;
    edtNumVale: TcxTextEdit;
    Label5: TLabel;
    procedure ValidaVale;
    procedure edtNumValeKeyPress(Sender: TObject; var Key: Char);
    procedure edtTipoKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPDVValeMerc: TfrmPDVValeMerc;

implementation

uses fPdv;

{$R *.dfm}

procedure TfrmPDVValeMerc.edtNumValeKeyPress(Sender: TObject; var Key: Char);
begin

    if (key = #13) then
        ValidaVale;

end;

procedure TfrmPDVValeMerc.ValidaVale;
var
    bTemVale  : boolean ;
    bTemSaldo : boolean ;
begin

  if (edtTipo.Value < 1) or (edtTipo.Value > 3) then
  begin
      frmPDV.MensagemErro('Tipo inv�lido.') ;
      edtTipo.SetFocus;
      exit ;
  end ;

  if (edtNumVale.Text <> '') then
  begin

      qryVale.Close;
      qryVale.Parameters.ParambyName('cCodVale').Value       := edtNumVale.Text ;
      qryVale.Parameters.ParamByName('nCdTabTipoVale').Value := edtTipo.Value ;
      qryVale.Open ;

      bTemVale  := false ;
      bTemSaldo := false ;

      while not qryVale.eof do
      begin
      
          bTemVale  := True ;

          if (qryValenSaldoVale.Value > 0) then
          begin
              bTemSaldo := True ;
              break ;
          end ;

          qryVale.Next ;

      end ;

      if not bTemVale then
      begin
          frmPDV.MensagemAlerta('N�mero de Vale/Cart�o n�o encontrado.') ;
          qryVale.Close ;
          close;
      end
      else
      begin

          if not bTemSAldo then
          begin
              frmPDV.MensagemAlerta('Vale/Cart�o n�o tem saldo dispon�vel para uso.') ;
              qryVale.Close ;
              close ;
          end ;

      end ;

      close ;

  end
  else close ;

end;

procedure TfrmPDVValeMerc.edtTipoKeyPress(Sender: TObject; var Key: Char);
begin

    if (key = #13) then
        edtNumVale.SetFocus;

end;

procedure TfrmPDVValeMerc.FormShow(Sender: TObject);
begin

    edtTipo.SetFocus;

end;

end.
