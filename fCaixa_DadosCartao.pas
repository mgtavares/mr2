unit fCaixa_DadosCartao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, jpeg, ExtCtrls, StdCtrls, cxButtons,
  cxControls, cxContainer, cxEdit, cxTextEdit, DB, ADODB, cxCurrencyEdit;

type
  TfrmCaixa_DadosCartao = class(TForm)
    Label2: TLabel;
    edtnCdOperadoraCartao: TcxTextEdit;
    edtNmOperadora: TcxTextEdit;
    btnConfirmar: TcxButton;
    btnCancelar: TcxButton;
    Image2: TImage;
    edtOperacao: TcxTextEdit;
    Label1: TLabel;
    Label8: TLabel;
    edtNrDocto: TcxTextEdit;
    Label9: TLabel;
    edtNrAutorizacao: TcxTextEdit;
    qryOperadora: TADOQuery;
    qryOperadoranCdOperadoraCartao: TIntegerField;
    qryOperadoracNmOperadoraCartao: TStringField;
    edtValorPagar: TcxCurrencyEdit;
    Label3: TLabel;
    procedure edtnCdOperadoraCartaoExit(Sender: TObject);
    procedure btnConfirmarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtNrAutorizacaoExit(Sender: TObject);
    procedure edtNrDoctoExit(Sender: TObject);
    procedure edtnCdOperadoraCartaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnCancelarClick(Sender: TObject);
    procedure edtNrDoctoKeyPress(Sender: TObject; var Key: Char);
    procedure edtNrAutorizacaoKeyPress(Sender: TObject; var Key: Char);
    procedure edtnCdOperadoraCartaoKeyPress(Sender: TObject;
      var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    cTipoOperacao : string ;
    nCdLoja       : integer ;
  end;

var
  frmCaixa_DadosCartao: TfrmCaixa_DadosCartao;

implementation

uses fMenu, fLookup_Padrao, fCaixa_ConsOperadora;

{$R *.dfm}

procedure TfrmCaixa_DadosCartao.edtnCdOperadoraCartaoExit(Sender: TObject);
begin
  if (nCdLoja = 0) then
      qryOperadora.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva
  else qryOperadora.Parameters.ParamByName('nCdLoja').Value := nCdLoja ;

  qryOperadora.Close ;
  qryOperadora.Parameters.ParamByName('nPK').Value           := frmMenu.ConvInteiro(edtnCdOperadoraCartao.Text) ;
  qryOperadora.Parameters.ParamByName('cTipoOperacao').Value := cTipoOperacao;
  qryOperadora.Open ;

  if not qryOperadora.eof then
      edtNmOperadora.Text := qryOperadoracNmOperadoraCartao.Value
   else
      edtNmOperadora.Clear;
end;

procedure TfrmCaixa_DadosCartao.btnConfirmarClick(Sender: TObject);
begin
  if ((not qryOperadora.Active) or (edtNmOperadora.Text = '')) then
  begin
      frmMenu.MensagemAlerta('Selecione uma operadora.') ;
      edtnCdOperadoraCartao.SetFocus;
      exit ;
  end ;

  if (Trim(edtNrAutorizacao.Text) = '') then
  begin
      frmMenu.MensagemAlerta('Informe o c�digo de autoriza��o.') ;
      edtNrAutorizacao.SetFocus ;
      exit ;
  end ;

  if (Trim(edtNrDocto.Text) = '') then
  begin
      frmMenu.MensagemAlerta('Informe o n�mero do documento.') ;
      edtNrDocto.SetFocus ;
      exit ;
  end ;

  case frmMenu.MessageDlg('Confirma os dados do cart�o ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  close ;

end;

procedure TfrmCaixa_DadosCartao.FormShow(Sender: TObject);
var
    nPK : integer ;
    objConsOperadora : TfrmCaixa_ConsOperadora ;
begin

  if (cTipoOperacao = 'D') then
      edtOperacao.Text := 'OPERA��O DE D�BITO' ;

  if (cTipoOperacao = 'C') then
      edtOperacao.Text := 'OPERA��O DE CR�DITO' ;

  qryOperadora.Close ;

  edtnCdOperadoraCartao.Text := '' ;
  edtNrAutorizacao.Text      := '' ;
  edtNrDocto.Text            := '' ;
  edtNmOperadora.Text        := '' ;

  if (nCdLoja = 0) then
      qryOperadora.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva
  else qryOperadora.Parameters.ParamByName('nCdLoja').Value := nCdLoja ;

  objConsOperadora := TfrmCaixa_ConsOperadora.Create(Self) ;

  objConsOperadora.cTipoOperacao := cTipoOperacao ;
  objConsOperadora.nCdLoja       := nCdLoja       ;
  objConsOperadora.ConsultaOperadora;

  if (objConsOperadora.qryOperadora.Active) then
      edtnCdOperadoraCartao.Text := objConsOperadora.qryOperadoranCdOperadoraCartao.AsString ;

  freeAndNil( objConsOperadora ) ;

  edtnCdOperadoraCartao.SetFocus ;

end;

procedure TfrmCaixa_DadosCartao.edtNrAutorizacaoExit(Sender: TObject);
begin
  if (edtNrAutorizacao.Text <> '') then
  begin
      try
          StrToInt(edtNrAutorizacao.Text) ;
      except
             case MessageDlg('Aten��o! Confirma Autorizacao de documento? '+edtNrAutorizacao.Text,mtConfirmation,[mbYes,mbNo],0) of
                    mrNo: begin
                          edtNrAutorizacao.Text := '' ;
                          edtNrAutorizacao.SetFocus ;
                          exit ;
                          end;
             end;
      end ;
  end ;

end;

procedure TfrmCaixa_DadosCartao.edtNrDoctoExit(Sender: TObject);
begin
  if (edtNrDocto.Text <> '') then
  begin
      try
          StrToInt(edtNrDocto.Text) ;
      except
             case MessageDlg('Aten��o! Confirma N�mero de documento? '+edtNrDocto.Text,mtConfirmation,[mbYes,mbNo],0) of
                    mrNo: begin
                          edtNrDocto.Text := '' ;
                          edtNrDocto.SetFocus ;
                          exit ;
                          end;
             end;
      end ;
  end ;

end;

procedure TfrmCaixa_DadosCartao.edtnCdOperadoraCartaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;

    objConsOperadora : TfrmCaixa_ConsOperadora;
begin
  inherited;

  case key of
    vk_F4 : begin

        objConsOperadora := TfrmCaixa_ConsOperadora.Create( Self );

        objConsOperadora.cTipoOperacao := cTipoOperacao ;
        objConsOperadora.nCdLoja       := nCdLoja       ;
        objConsOperadora.ConsultaOperadora;

        if (objConsOperadora.qryOperadora.Active) then
            edtnCdOperadoraCartao.Text := objConsOperadora.qryOperadoranCdOperadoraCartao.AsString ;

        freeAndNil( objConsOperadora ) ;

    end ;

  end ;

end;

procedure TfrmCaixa_DadosCartao.btnCancelarClick(Sender: TObject);
begin

  edtnCdOperadoraCartao.Text := '' ;
  edtNmOperadora.Text        := '' ;
  edtNrAutorizacao.Text      := '' ;
  edtNrDocto.Text            := '' ;

  qryOperadora.Close ;

  close ;

end;

procedure TfrmCaixa_DadosCartao.edtNrDoctoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (key = #13) then
        edtNrAutorizacao.SetFocus;

end;

procedure TfrmCaixa_DadosCartao.edtNrAutorizacaoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (key = #13) then
        btnConfirmar.SetFocus;

end;

procedure TfrmCaixa_DadosCartao.edtnCdOperadoraCartaoKeyPress(
  Sender: TObject; var Key: Char);
begin
    if (key = #13) then
        edtNrDocto.SetFocus;
end;

procedure TfrmCaixa_DadosCartao.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    nCdLoja := 0 ;
end;


end.


