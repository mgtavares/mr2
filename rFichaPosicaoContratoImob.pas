unit rFichaPosicaoContratoImob;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptFichaPosicaoContratoImob = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRDBText8: TQRDBText;
    QRDBText10: TQRDBText;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    qryContrato: TADOQuery;
    qryContratonCdContratoEmpImobiliario: TIntegerField;
    qryContratocNumContrato: TStringField;
    qryContratodDtContrato: TDateTimeField;
    qryContratodDtVenc: TDateTimeField;
    qryContratonValTit: TBCDField;
    qryContratonValJuro: TBCDField;
    qryContratonValDesconto: TBCDField;
    qryContratonValAbatimento: TBCDField;
    qryContratonValLiq: TBCDField;
    qryContratodDtLiq: TDateTimeField;
    qryContratonSaldoTit: TBCDField;
    qryContratocNmTerceiro: TStringField;
    qryContratocCNPJCPF: TStringField;
    qryContratocRG: TStringField;
    qryContratocTelefone1: TStringField;
    qryContratocTelefoneMovel: TStringField;
    qryContratocFax: TStringField;
    qryContratocEmail: TStringField;
    qryContratocNmcjg: TStringField;
    qryContratonValContrato: TBCDField;
    qryContratocNmEmpImobiliario: TStringField;
    qryContratocNmBlocoEmpImobiliario: TStringField;
    qryContratocNrUnidade: TStringField;
    QRDBText1: TQRDBText;
    QRLabel14: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabel21: TQRLabel;
    QRDBText17: TQRDBText;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRDBText18: TQRDBText;
    qryContratocTelefoneEmpTrab: TStringField;
    QRLabel4: TQRLabel;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText21: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    qryContratoiParcela: TStringField;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel24: TQRLabel;
    QRDBText22: TQRDBText;
    QRLabel27: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRLabel26: TQRLabel;
    QRLabel25: TQRLabel;
    qryContratonValCorrecao: TBCDField;
    qryContratonValIndiceCorrecao: TBCDField;
    QRDBText9: TQRDBText;
    QRLabel13: TQRLabel;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFichaPosicaoContratoImob: TrptFichaPosicaoContratoImob;

implementation

{$R *.dfm}

procedure TrptFichaPosicaoContratoImob.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin

    if (QRBand3.Color = clWhite) then
    begin
        QRBand3.Color    := $00F0F0F0 ;
        QRDBText3.Color  := $00F0F0F0 ;
        QRDBText10.Color := $00F0F0F0 ;
        QRDBText2.Color  := $00F0F0F0 ;
        QRDBText4.Color  := $00F0F0F0 ;
        QRDBText7.Color  := $00F0F0F0 ;
        QRDBText5.Color  := $00F0F0F0 ;
        QRDBText6.Color  := $00F0F0F0 ;
    end
    else
    begin
        QRBand3.Color    := clWhite ;
        QRDBText3.Color  := clWhite ;
        QRDBText10.Color := clWhite ;
        QRDBText2.Color  := clWhite ;
        QRDBText4.Color  := clWhite ;
        QRDBText7.Color  := clWhite ;
        QRDBText5.Color  := clWhite ;
        QRDBText6.Color  := clWhite ;
    end ;

end;

end.
