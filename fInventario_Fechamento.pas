unit fInventario_Fechamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, StdCtrls, Mask, DBCtrls, ADODB, cxLookAndFeelPainters,
  cxButtons, cxPC, cxControls, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmInventario_Fechamento = class(TfrmProcesso_Padrao)
    ToolButton4: TToolButton;
    qryInventario: TADOQuery;
    qryInventarionCdInventario: TIntegerField;
    qryInventarionCdEmpresa: TIntegerField;
    qryInventariocNmEmpresa: TStringField;
    qryInventariodDtAbertura: TDateTimeField;
    qryInventariodDtFech: TDateTimeField;
    qryInventarionCdGrupoEstoque: TIntegerField;
    qryInventariocNmGrupoEstoque: TStringField;
    qryInventarionCdLocalEstoque: TIntegerField;
    qryInventariocNmLocalEstoque: TStringField;
    qryInventariocResponsavel: TStringField;
    qryInventariocOBS: TStringField;
    DataSource1: TDataSource;
    qryItemInventario: TADOQuery;
    qryItemInventarionCdInventario: TIntegerField;
    qryItemInventarioiItem: TIntegerField;
    qryItemInventarionCdProduto: TIntegerField;
    qryItemInventarionCdLocalEstoque: TIntegerField;
    qryItemInventarionQtdeFisica: TBCDField;
    qryItemInventarionQtdeLogica: TBCDField;
    qryItemInventarionDiferenca: TBCDField;
    qryItemInventarionPercDif: TBCDField;
    dsItemInventario: TDataSource;
    qryItemInventariocNmProduto: TStringField;
    qryItemInventariocNmLocalEstoque: TStringField;
    qryLocalEstoque: TADOQuery;
    qryProduto: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    SP_FECHA_INVENTARIO: TADOStoredProc;
    ToolButton7: TToolButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    ToolButton5: TToolButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    qryInventariocFlgApuradoDivergencia: TIntegerField;
    SP_APURA_CONTAGEM_INVENTARIO: TADOStoredProc;
    qryInventariodDtCancel: TDateTimeField;
    ToolButton6: TToolButton;
    qryItemInventarionCdItemInventario: TLargeintField;
    procedure qryInventarioAfterScroll(DataSet: TDataSet);
    procedure qryInventarioAfterClose(DataSet: TDataSet);
    procedure qryItemInventarioCalcFields(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInventario_Fechamento: TfrmInventario_Fechamento;

implementation

uses fMenu, rInventario_Resumo, fLookup_Padrao, rInventario;

{$R *.dfm}

procedure TfrmInventario_Fechamento.qryInventarioAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryItemInventario, qryInventarionCdInventario.AsString) ;
  
end;

procedure TfrmInventario_Fechamento.qryInventarioAfterClose(
  DataSet: TDataSet);
begin
  inherited;

  qryItemInventario.Close ;
  
end;

procedure TfrmInventario_Fechamento.qryItemInventarioCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryLocalEstoque.Close;
  qryProduto.Close ;

  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryInventarionCdEmpresa.Value ;

  PosicionaQuery(qryProduto, qryItemInventarionCdProduto.AsString) ;
  PosicionaQuery(qryLocalEstoque, qryItemInventarionCdLocalEstoque.AsString) ;

  if not qryProduto.eof then
      qryItemInventariocNmProduto.Value := qryProdutocNmProduto.Value ;

  if not qryLocalEstoque.Eof then
      qryItemInventariocNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;
      
end;

procedure TfrmInventario_Fechamento.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if not qryInventario.Active or (qryInventarionCdInventario.Value = 0) then
  begin
      MensagemAlerta('Selecione um invent�rio.') ;
      exit ;
  end ;

  if (qryInventariodDtFech.AsString <> '') then
  begin
      MensagemAlerta('Invent�rio j� foi encerrado.') ;
      exit ;
  end ;

  if (qryInventariocFlgApuradoDivergencia.Value = 0) then
  begin

      if (MessageDlg('� necess�rio apurar as diverg�ncias antes de fechar o invent�rio. Deseja apurar agora ?',mtConfirmation,[mbYes,mbNo],0) = MRYES) then
      begin

          frmMenu.Connection.BeginTrans;

          try
              SP_APURA_CONTAGEM_INVENTARIO.Close ;
              SP_APURA_CONTAGEM_INVENTARIO.Parameters.ParamByName('@nCdInventario').Value  := qryInventarionCdInventario.Value ;
              SP_APURA_CONTAGEM_INVENTARIO.Parameters.ParamByName('@cFlgAtuEstoque').Value := 'N' ;
              SP_APURA_CONTAGEM_INVENTARIO.ExecProc;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          frmMenu.Connection.CommitTrans;

          PosicionaQuery(qryInventario, qryInventarionCdInventario.AsString) ;

      end ;

      exit ;
  end ;

  case MessageDlg('Confirma o fechamento deste invent�rio ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_FECHA_INVENTARIO.Close ;
      SP_FECHA_INVENTARIO.Parameters.ParamByName('@nCdInventario').Value := qryInventarionCdInventario.Value ;
      SP_FECHA_INVENTARIO.Parameters.ParamByName('@nCdUsuario').Value    := frmMenu.nCdUsuarioLogado;
      SP_FECHA_INVENTARIO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Invent�rio Fechado! Estoques Atualizados.') ;

  case MessageDlg('Deseja imprimir o resumo do invent�rio ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes: ToolButton7.Click;
  end ;

  qryInventario.Close ;
  qryItemInventario.Close ;

end;

procedure TfrmInventario_Fechamento.ToolButton7Click(Sender: TObject);
var
  objRel : TrptInventario_Resumo;
begin
  inherited;

  if not qryInventario.Active or (qryInventarionCdInventario.Value = 0) then
  begin
      MensagemAlerta('Selecione um invent�rio.') ;
      exit ;
  end ;

  if (qryInventariodDtFech.asString = '') then
  begin
      MensagemAlerta('� necess�rio fechar o invent�rio antes de imprimir o resumo.') ;
      abort ;
  end ;

  objRel := TrptInventario_Resumo.Create(nil);

  try
      try
          objRel.qryPreparaTemp.Execute;

          objRel.SP_PREPARA_RESUMO_INVENTARIO.Close ;
          objRel.SP_PREPARA_RESUMO_INVENTARIO.Parameters.ParamByName('@nCdInventario').Value := qryInventarionCdInventario.Value ;
          objRel.SP_PREPARA_RESUMO_INVENTARIO.ExecProc ;

          objRel.qryTemp_Resumo1.Close ;
          objRel.qryTemp_Resumo2.Close ;
          objRel.qryTemp_Resumo3.Close ;

          objRel.qryTemp_Resumo1.Open ;
          objRel.qryTemp_Resumo2.Open ;
          objRel.qryTemp_Resumo3.Open ;

          objRel.lblAbertura.Caption     := qryInventariodDtAbertura.AsString;
          objRel.lblFechamento.Caption   := qryInventariodDtFech.AsString;
          objRel.lblEmpresa.Caption      := frmMenu.cNmEmpresaAtiva;
          objRel.lblEmpresa2.Caption     := qryInventarionCdEmpresa.AsString + ' - ' + qryInventariocNmEmpresa.Value ;
          objRel.lblInventario.Caption   := qryInventarionCdInventario.AsString;
          objRel.lblLocalEstoque.Caption := qryInventarionCdLocalEstoque.asString + ' - ' + qryInventariocNmLocalEstoque.AsString ;

          objRel.lblDifCusto.Caption := FormatCurr('#,##0.00',objRel.qryTemp_Resumo1nValPrecoCustoFisico.Value - objRel.qryTemp_Resumo1nValPrecoCustoLogico.Value) ;
          objRel.lblDifPecas.Caption := FormatCurr(',0',objRel.qryTemp_Resumo1nQtdePecasFisico.Value - objRel.qryTemp_Resumo1nQtdePecasLogico.Value) ;

          if (objRel.qryTemp_Resumo1nValPrecoCustoFisico.Value > 0) then
              objRel.lblPercDifCusto.Caption      := FormatCurr('#,##0.00',((objRel.qryTemp_Resumo1nValPrecoCustoFisico.Value / objRel.qryTemp_Resumo1nValPrecoCustoLogico.Value)*100)-100) ;

          if (objRel.qryTemp_Resumo1nQtdePecasLogico.Value > 0) then
              objRel.lblPercDifQuantidade.Caption := FormatCurr('#,##0.00',((objRel.qryTemp_Resumo1nQtdePecasFisico.Value / objRel.qryTemp_Resumo1nQtdePecasLogico.Value)*100)-100) ;

          objRel.lblPercOK.Caption          := '0,00' ;
          objRel.lblPercLogicoMaior.Caption := '0,00' ;
          objRel.lblPercLogicoMenor.Caption := '0,00' ;

          if (objRel.qryTemp_Resumo1nQtdePecasOK.Value > 0) then
              objRel.lblPercOK.Caption := FormatCurr('#,##0.00',(objRel.qryTemp_Resumo1nQtdePecasOK.Value / objRel.qryTemp_Resumo1nQtdeItensContados.Value)*100) ;

          if (objRel.qryTemp_Resumo1nQtdePecasLogicoMenor.Value > 0) then
              objRel.lblPercLogicoMenor.Caption := FormatCurr('#,##0.00',(objRel.qryTemp_Resumo1nQtdePecasLogicoMenor.Value / objRel.qryTemp_Resumo1nQtdeItensContados.Value)*100) ;

          if (objRel.qryTemp_Resumo1nQtdePecasLogicoMaior.Value > 0) then
              objRel.lblPercLogicoMaior.Caption := FormatCurr('#,##0.00',(objRel.qryTemp_Resumo1nQtdePecasLogicoMaior.Value / objRel.qryTemp_Resumo1nQtdeItensContados.Value)*100) ;

          objRel.Preview;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmInventario_Fechamento.cxButton1Click(Sender: TObject);
var
  i:Integer ;
begin

    i := frmMenu.BuscaPK() ;

    If (i > 0) then
    begin
        PosicionaQuery(qryInventario, IntToStr(i)) ;

        ToolButton1.Enabled := True ;

        if not qryInventario.eof then
        begin
            if (qryInventariodDtFech.asString <> '') then
            begin
                MensagemAlerta('Invent�rio j� foi encerrado, somente consulta � permitida.') ;
                ToolButton1.Enabled := False ;
            end ;
        end ;

        DBGridEh1.SetFocus;

    end ;

end;

procedure TfrmInventario_Fechamento.cxButton2Click(Sender: TObject);
var
  nCdPK       : Integer ;
begin

  nCdPK := frmLookup_Padrao.ExecutaConsulta(217) ;

  If (nCdPK > 0) then
  begin

    PosicionaQuery(qryInventario, IntToStr(nCdPK)) ;

    ToolButton1.Enabled := True ;

    if ( not qryInventario.isEmpty ) then
    begin
        if not (qryInventariodDtFech.IsNull) then
        begin
            MensagemAlerta('Invent�rio j� foi encerrado, somente consulta � permitida.') ;

            ToolButton1.Enabled := False ;
        end ;

        if not (qryInventariodDtCancel.IsNull) then
        begin
            MensagemAlerta('Invent�rio cancelado, somente consulta � permitida.') ;

            ToolButton1.Enabled := False ;
        end ;

        DBGridEh1.SetFocus;

    end ;

  end ;

end;

procedure TfrmInventario_Fechamento.ToolButton5Click(Sender: TObject);
var
  objRel : TrptInventario_view;
begin
  inherited;

  if (qryInventario.isEmpty) then
  begin
      MensagemAlerta('Selecione um invent�rio.') ;
      abort;
  end ;

  objRel := TrptInventario_view.Create(nil);

  try
      try
          objRel.SPREL_INVENTARIO.Close ;
          objRel.SPREL_INVENTARIO.Parameters.ParamByName('@nCdInventario').Value := qryInventarionCdInventario.Value ;
          objRel.SPREL_INVENTARIO.Open ;

          objRel.Prepare;
          objRel.QRTotalPagina.Caption := '/' + IntToStr(objRel.QRPrinter.PageCount) ;
          objRel.QRPrinter.Free;
          objRel.QRPrinter := nil ;
          
          objRel.lblEmpresa.Caption      := frmMenu.cNmEmpresaAtiva;
          objRel.lblEmpresa2.Caption     := frmMenu.cNmEmpresaAtiva;
          objRel.lblInventario.Caption   := qryInventarionCdInventario.asString;
          objRel.Preview;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;

end;

initialization
    RegisterClass(TfrmInventario_Fechamento) ;

end.
