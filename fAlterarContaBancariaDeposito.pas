unit fAlterarContaBancariaDeposito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DBCtrls, StdCtrls, Mask, ADODB, DB, ImgList,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmAlteraContaBancariaDeposito = class(TfrmProcesso_Padrao)
    qryContaBancaria: TADOQuery;
    qryDepositoBancario: TADOQuery;
    SP_ALTERA_CONTA_DEPOSITO_BANCARIO: TADOStoredProc;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Label7: TLabel;
    DBEdit16: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    DBEdit7: TDBEdit;
    DBEdit1: TDBEdit;
    qryDepositoBancarionCdDepositoBancario: TIntegerField;
    qryDepositoBancarionCdEmpresa: TIntegerField;
    qryDepositoBancarionCdLoja: TIntegerField;
    qryDepositoBancariodDtDeposito: TDateTimeField;
    qryDepositoBancarionCdContaBancaria: TIntegerField;
    qryDepositoBancarionValDinheiro: TBCDField;
    qryDepositoBancarionValChequeNumerario: TBCDField;
    qryDepositoBancarionValChequePreDatado: TBCDField;
    qryDepositoBancarionValTotalDeposito: TBCDField;
    qryDepositoBancarioiQtdeCheques: TIntegerField;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    DataSource1: TDataSource;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    Label9: TLabel;
    DBEdit8: TDBEdit;
    Label10: TLabel;
    DBEdit9: TDBEdit;
    qryDepositoBancariocNmEmpresa: TStringField;
    qryDepositoBancariocNmLoja: TStringField;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label11: TLabel;
    DBEdit12: TDBEdit;
    qryDepositoBancariocNmConta: TStringField;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancariacNmConta: TStringField;
    DataSource2: TDataSource;
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit2Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAlteraContaBancariaDeposito: TfrmAlteraContaBancariaDeposito;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmAlteraContaBancariaDeposito.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(166,'nCdContaBancaria <> ' + qryDepositoBancarionCdContaBancaria.AsString + ' AND dDtProcesso IS NOT NULL AND dDtCancel IS NULL');

            If (nPK > 0) then
            begin
                MaskEdit1.Text := IntToStr(nPK);
                PosicionaQuery(qryDepositoBancario,MaskEdit1.Text);
            end ;

    end ;

  end ;

end;

procedure TfrmAlteraContaBancariaDeposito.MaskEdit1Exit(Sender: TObject);
begin
  inherited;
  qryDepositoBancario.Close;
  PosicionaQuery(qryDepositoBancario,MaskEdit1.Text);
end;

procedure TfrmAlteraContaBancariaDeposito.MaskEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
        if((Trim(MaskEdit1.Text) <> '')) then
        begin
            if (frmMenu.LeParametro('VAREJO') = 'S') then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = ' + qryDepositoBancarionCdEmpresa.AsString + ' AND ((@@Loja = 0) OR (' + qryDepositoBancarionCdLoja.AsString +' = ContaBancaria.nCdLoja)) AND cFlgDeposito = 1');
            end
            else
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = ' + qryDepositoBancarionCdEmpresa.AsString + ' AND cFlgDeposito = 1');
            end ;

        end
        else
        begin
            MaskEdit2.Text := '';
            MensagemAlerta('Selecione o Dep�sito Banc�rio que deseja alterar antes de selecionar a conta Banc�ria de Cr�dito.');
            MaskEdit1.SetFocus;
            abort;
        end;

        If (nPK > 0) then
        begin
            MaskEdit2.Text := IntToStr(nPK);
            qryContaBancaria.Close;
            qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := qryDepositoBancarionCdEmpresa.Value;
            qryContaBancaria.Parameters.ParamByName('nCdLoja').Value := qryDepositoBancarionCdLoja.Value;
            PosicionaQuery(qryContaBancaria,MaskEdit2.Text);
        end ;

    end ;

  end ;

end;

procedure TfrmAlteraContaBancariaDeposito.MaskEdit2Exit(Sender: TObject);
begin
  inherited;
  if((Trim(MaskEdit1.Text) <> '')) then
  begin
      qryContaBancaria.Close;
      qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := qryDepositoBancarionCdEmpresa.Value;
      qryContaBancaria.Parameters.ParamByName('nCdLoja').Value := qryDepositoBancarionCdLoja.Value;
      PosicionaQuery(qryContaBancaria,MaskEdit2.Text);
  end
  else if((Trim(MaskEdit2.Text) <> '')) then
  begin
      MaskEdit2.Text := '';
      MensagemAlerta('Selecione o Dep�sito Banc�rio que deseja alterar antes de selecionar a conta Banc�ria de Cr�dito.');
      MaskEdit1.SetFocus;
      abort;
  end;
end;

procedure TfrmAlteraContaBancariaDeposito.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryDepositoBancario,MaskEdit1.Text);
  PosicionaQuery(qryContaBancaria,MaskEdit2.Text);

  if(Trim(MaskEdit1.Text)='') then
  begin
      MensagemAlerta('Selecione o Dep�sito Banc�rio que deseja alterar.');
      MaskEdit1.SetFocus;
      abort;
  end;

  if(Trim(MaskEdit2.Text)='') then
  begin
      MensagemAlerta('Selecione a Conta Banc�ria de Cr�dito.');
      MaskEdit2.SetFocus;
      abort;
  end;

  if (MessageDlg('Confirma a altera��o da conta banc�ria deste dep�sito ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      SP_ALTERA_CONTA_DEPOSITO_BANCARIO.Close;
      SP_ALTERA_CONTA_DEPOSITO_BANCARIO.Parameters.ParamByName('@nCdDepositoBancario').Value := qryDepositoBancarionCdDepositoBancario.Value ;
      SP_ALTERA_CONTA_DEPOSITO_BANCARIO.Parameters.ParamByName('@nCdContaBancaria').Value    := qryContaBancarianCdContaBancaria.Value;
      SP_ALTERA_CONTA_DEPOSITO_BANCARIO.Parameters.ParamByName('@nCdUsuario').Value          := frmMenu.nCdUsuarioLogado;
      SP_ALTERA_CONTA_DEPOSITO_BANCARIO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Altera��o realizada com sucesso.') ;

  qryDepositoBancario.Close;
  qryContaBancaria.Close;

  MaskEdit1.Text := '';
  MaskEdit2.Text := '';

  MaskEdit1.SetFocus;


end;

initialization
    RegisterClass(TfrmAlteraContaBancariaDeposito) ;

end.
