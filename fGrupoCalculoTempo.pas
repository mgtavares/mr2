unit fGrupoCalculoTempo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmGrupoCalculoTempo = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoCalculoTempo: TIntegerField;
    qryMastercNmGrupoCalculoTempo: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoCalculoTempo: TfrmGrupoCalculoTempo;

implementation

{$R *.dfm}

procedure TfrmGrupoCalculoTempo.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOCALCULOTEMPO' ;
  nCdTabelaSistema  := 314 ;
  nCdConsultaPadrao := 740 ;
  bCodigoAutomatico := True ;
end;

procedure TfrmGrupoCalculoTempo.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DbEdit2.Text) = '') then
  begin
      ShowMessage('Informe a descri��o do grupo de Calculo de Tempo.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmGrupoCalculoTempo.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

initialization
    RegisterClass(TfrmGrupoCalculoTempo);

end.
