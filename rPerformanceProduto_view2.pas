unit rPerformanceProduto_view2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPerformanceProduto_view2 = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_PERFORMANCE_PRODUTO: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel11: TQRLabel;
    SPREL_PERFORMANCE_PRODUTOnCdProduto: TIntegerField;
    SPREL_PERFORMANCE_PRODUTOcReferencia: TStringField;
    SPREL_PERFORMANCE_PRODUTOcNmMarca: TStringField;
    SPREL_PERFORMANCE_PRODUTOcNmProduto: TStringField;
    SPREL_PERFORMANCE_PRODUTOcNmDepartamento: TStringField;
    SPREL_PERFORMANCE_PRODUTOcNmCategoria: TStringField;
    SPREL_PERFORMANCE_PRODUTOcNmSubCategoria: TStringField;
    SPREL_PERFORMANCE_PRODUTOcNmSegmento: TStringField;
    SPREL_PERFORMANCE_PRODUTOnCdGrade: TIntegerField;
    SPREL_PERFORMANCE_PRODUTOdDtUltReceb: TDateTimeField;
    SPREL_PERFORMANCE_PRODUTOcTituloGrade: TStringField;
    SPREL_PERFORMANCE_PRODUTOcTituloGradeEI: TStringField;
    SPREL_PERFORMANCE_PRODUTOcTituloGradeMR: TStringField;
    SPREL_PERFORMANCE_PRODUTOcTituloGradeMV: TStringField;
    SPREL_PERFORMANCE_PRODUTOcTituloGradeEF: TStringField;
    SPREL_PERFORMANCE_PRODUTOnSaldoEI: TIntegerField;
    SPREL_PERFORMANCE_PRODUTOnSaldoMR: TIntegerField;
    SPREL_PERFORMANCE_PRODUTOnSaldoMV: TIntegerField;
    SPREL_PERFORMANCE_PRODUTOnSaldoEF: TIntegerField;
    SPREL_PERFORMANCE_PRODUTOnPerformance: TFloatField;
    SPREL_PERFORMANCE_PRODUTOnCdTerceiroComprador: TIntegerField;
    QRGroup1: TQRGroup;
    QRDBText5: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel2: TQRLabel;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    bZebrado : boolean ;
  end;

var
  rptPerformanceProduto_view2: TrptPerformanceProduto_view2;

implementation

{$R *.dfm}

procedure TrptPerformanceProduto_view2.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin

    if (bZebrado) then
        if (QRBand3.Color = clSilver) then QRBand3.Color := clWhite
        else QRBand3.Color := clSilver ;

end;

end.
