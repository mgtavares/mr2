unit dcCompraCentroCusto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, Mask, StdCtrls, DBCtrls, ImgList,
  ComCtrls, ToolWin, ExtCtrls;

type
  TdcmCompraCentroCusto = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource5: TDataSource;
    DBEdit8: TDBEdit;
    MaskEdit10: TMaskEdit;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    qryCentroCusto: TADOQuery;
    dsCentroCusto: TDataSource;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocCdCC: TStringField;
    qryCentroCustocNmCC: TStringField;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit10Exit(Sender: TObject);
    procedure MaskEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmCompraCentroCusto: TdcmCompraCentroCusto;

implementation

uses fMenu, fLookup_Padrao, dcCompraCentroCusto_view;

{$R *.dfm}

procedure TdcmCompraCentroCusto.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  MaskEdit3.SetFocus ;

end;

procedure TdcmCompraCentroCusto.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TdcmCompraCentroCusto.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TdcmCompraCentroCusto.ToolButton1Click(Sender: TObject);
var
  objForm : TdcmCompraCentroCusto_view;
begin
  inherited;

  if (trim(MaskEdit1.Text) = '/  /') or (Trim(MaskEdit2.Text) = '/  /') then
  begin
      MaskEdit2.Text := DateToStr(Now()) ;
      MaskEdit1.Text := DateToStr(Now()-180) ;
  end ;

  objForm := TdcmCompraCentroCusto_view.Create(nil);

  objForm.ADODataSet1.Close ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdCC').Value      := frmMenu.ConvInteiro(MaskEdit10.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtInicial').Value := frmMenu.ConvData(MaskEdit1.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtFinal').Value   := frmMenu.ConvData(MaskEdit2.Text) ;
  objForm.ADODataSet1.Open ;

  if (objForm.ADODataSet1.eof) then
  begin
      ShowMessage('Nenhuma informação encontrada.') ;
      exit ;
  end ;

  frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

  if objForm.PivotCube1.Active then
  begin
      objForm.PivotCube1.Active := False;
  end;

  objForm.PivotCube1.ExtendedMode := True;
  objForm.PVMeasureToolBar1.HideButtons := False;
  objForm.PivotCube1.Active := True;

  frmMenu.StatusBar1.Panels[6].Text := '' ;

  showForm(objForm,true) ;

end;

procedure TdcmCompraCentroCusto.MaskEdit10Exit(Sender: TObject);
begin
  inherited;

  qryCentroCusto.Close ;
  PosicionaQuery(qryCentroCusto, MaskEdit10.Text) ;
  
end;

procedure TdcmCompraCentroCusto.MaskEdit10KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(29);

        If (nPK > 0) then
        begin
            Maskedit10.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCentroCusto, MaskEdit10.Text) ;
        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TdcmCompraCentroCusto) ;
    
end.

