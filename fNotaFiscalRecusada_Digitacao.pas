unit fNotaFiscalRecusada_Digitacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask;

type
  TfrmNotaFiscalRecusada_Digitacao = class(TfrmCadastro_Padrao)
    qryMasternCdNFRecusada: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasterdDtReceb: TDateTimeField;
    qryMastercNrNF: TStringField;
    qryMastercSerieNF: TStringField;
    qryMasternValTotal: TBCDField;
    qryMasternCdUsuario: TIntegerField;
    qryMastercNrTit1: TStringField;
    qryMastercNrTit2: TStringField;
    qryMastercNrTit3: TStringField;
    qryMastercNrTit4: TStringField;
    qryMastercNrTit5: TStringField;
    qryMastercNrTit6: TStringField;
    qryMastercOBS: TMemoField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    DBMemo1: TDBMemo;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit15: TDBEdit;
    DataSource1: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit16: TDBEdit;
    DataSource2: TDataSource;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DBEdit17: TDBEdit;
    DataSource3: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNotaFiscalRecusada_Digitacao: TfrmNotaFiscalRecusada_Digitacao;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmNotaFiscalRecusada_Digitacao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'NFRECUSADA' ;
  nCdTabelaSistema  := 74 ;
  nCdConsultaPadrao := 171 ;

end;

procedure TfrmNotaFiscalRecusada_Digitacao.btIncluirClick(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      qryMasternCdLoja.Value := frmMenu.nCdLojaAtiva;
      PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;
  end ;

  qryMasternCdUsuario.Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryUsuario, qryMasternCdUsuario.AsString) ;
  
  DBEdit3.SetFocus ;

end;

procedure TfrmNotaFiscalRecusada_Digitacao.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DBEdit3.Text) ;
  
end;

procedure TfrmNotaFiscalRecusada_Digitacao.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmNotaFiscalRecusada_Digitacao.qryMasterBeforePost(
  DataSet: TDataSet);
begin


  if (DBEdit16.Text = '') then
  begin
      MensagemAlerta('Informe o terceiro.') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (trim(DBEdit4.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data de recebimento da NF.') ;
      DBEdit4.SetFocus;
      abort ;
  end ;

  if (trim(DBEdit5.Text) = '') then
  begin
      MensagemAlerta('Informe o n�mero da NF.') ;
      DBEdit5.SetFocus;
      abort ;
  end ;

  if (trim(DBEdit6.Text) = '') then
  begin
      MensagemAlerta('Informe a s�rie da NF.') ;
      DBEdit6.SetFocus;
      abort ;
  end ;

  if (qryMasternValTotal.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da nota fiscal.') ;
      DBEdit7.SetFocus;
      abort ;
  end ;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  inherited;

end;

procedure TfrmNotaFiscalRecusada_Digitacao.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  
end;

procedure TfrmNotaFiscalRecusada_Digitacao.qryMasterAfterCancel(
  DataSet: TDataSet);
begin
  inherited;

  qryLoja.Close ;
  qryTerceiro.Close ;
  qryUsuario.Close ;

end;

procedure TfrmNotaFiscalRecusada_Digitacao.qryMasterAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, qryMasternCdLoja.asString) ;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuario.AsString) ;

end;

initialization
    RegisterClass(TfrmNotaFiscalRecusada_Digitacao) ;

end.
