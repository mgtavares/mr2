unit fCaixa_CredErro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ImgList, ComCtrls, ToolWin, ExtCtrls, ADODB,
  StdCtrls, Mask, DBCtrls, cxCheckBox, cxContainer, cxTextEdit;

type
  TfrmCaixa_CredErro = class(TfrmProcesso_Padrao)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1DBColumn_nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn_cCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn_iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn_dDtVenc: TcxGridDBColumn;
    cxStyleRepository1: TcxStyleRepository;
    FaltaParcela: TcxStyle;
    GroupBox1: TGroupBox;
    qryLoja: TADOQuery;
    dsLoja: TDataSource;
    qryLojacNmLoja: TStringField;
    qryVerificaCrediario: TADOQuery;
    dsVerificaCrediario: TDataSource;
    cxGrid1DBTableView1DBColumn_cFlgParcFaltando: TcxGridDBColumn;
    qryVerificaCrediarionCdTerceiro: TIntegerField;
    qryVerificaCrediariocNmTerceiro: TStringField;
    qryVerificaCrediarionCdLoja: TIntegerField;
    qryVerificaCrediarionCdCrediario: TIntegerField;
    qryVerificaCrediariodDtVenda: TDateTimeField;
    qryVerificaCrediarioiTotalParcCred: TIntegerField;
    qryVerificaCrediarionValCrediario: TBCDField;
    qryVerificaCrediariocCdTitulo: TStringField;
    qryVerificaCrediarioiParcela: TIntegerField;
    qryVerificaCrediariodDtVenc: TDateTimeField;
    qryVerificaCrediarionValTit: TBCDField;
    qryVerificaCrediariocFlgParcFaltando: TIntegerField;
    cxGrid1DBTableView1DBColumn_nCdCrediario: TcxGridDBColumn;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label7: TLabel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit4: TDBEdit;
    Label1: TLabel;
    DBEdit10: TDBEdit;
    Label9: TLabel;
    GroupBox2: TGroupBox;
    Label3: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label4: TLabel;
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure FormShow(Sender: TObject);
    procedure qryVerificaCrediarioAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_CredErro: TfrmCaixa_CredErro;

implementation

{$R *.dfm}

uses
  fCaixa_Recebimento, fMenu;

procedure TfrmCaixa_CredErro.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;
  if (AItem <> cxGrid1DBTableView1DBColumn_cFlgParcFaltando) then
     if (Strtofloat(ARecord.Values[5]) = 1) then
         AStyle := FaltaParcela;

end;

procedure TfrmCaixa_CredErro.FormShow(Sender: TObject);
begin
  inherited;
  cxGrid1DBTableView1.Columns[0].GroupIndex := 0;
end;

procedure TfrmCaixa_CredErro.qryVerificaCrediarioAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryLoja,qryVerificaCrediarionCdLoja.AsString);
end;


end.
