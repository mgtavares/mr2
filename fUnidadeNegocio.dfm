inherited frmUnidadeNegocio: TfrmUnidadeNegocio
  Caption = 'Unidade de Neg'#243'cio'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 60
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 8
    Top = 70
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Unidade Neg'#243'cio'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 104
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdUnidadeNegocio'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 104
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmUnidadeNegocio'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBGridEh1: TDBGridEh [6]
    Left = 8
    Top = 120
    Width = 745
    Height = 249
    DataSource = dsEmpresaUnidadeNegocio
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdUnidadeNegocio'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Width = 103
      end
      item
        EditButtons = <>
        FieldName = 'cSigla'
        Footers = <>
        ReadOnly = True
        Width = 81
      end
      item
        EditButtons = <>
        FieldName = 'cNmEmpresa'
        Footers = <>
        ReadOnly = True
        Width = 521
      end>
  end
  object StaticText1: TStaticText [7]
    Left = 8
    Top = 104
    Width = 745
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Empresas que atuam nesta unidade de neg'#243'cio'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 4
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM UNIDADENEGOCIO'
      'WHERE nCdUnidadeNegocio = :nPK')
    object qryMasternCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryMastercNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object dsEmpresaUnidadeNegocio: TDataSource
    DataSet = qryEmpresaUnidadeNegocio
    Left = 856
    Top = 360
  end
  object qryEmpresaUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryEmpresaUnidadeNegocioBeforePost
    Parameters = <
      item
        Name = 'nCdUnidadeNegocio'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM EMPRESAUNIDADENEGOCIO'
      'WHERE nCdUnidadeNegocio = :nCdUnidadeNegocio')
    Left = 904
    Top = 368
    object qryEmpresaUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryEmpresaUnidadeNegocionCdEmpresa: TIntegerField
      DisplayLabel = 'C'#243'd. Empresa'
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresaUnidadeNegociocNmEmpresa: TStringField
      DisplayLabel = 'Nome Empresa'
      FieldKind = fkLookup
      FieldName = 'cNmEmpresa'
      LookupDataSet = qryEmpresa
      LookupKeyFields = 'nCdEmpresa'
      LookupResultField = 'cNmEmpresa'
      KeyFields = 'nCdEmpresa'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryEmpresaUnidadeNegociocSigla: TStringField
      DisplayLabel = 'Sigla'
      FieldKind = fkLookup
      FieldName = 'cSigla'
      LookupDataSet = qryEmpresa
      LookupKeyFields = 'nCdEmpresa'
      LookupResultField = 'cSigla'
      KeyFields = 'nCdEmpresa'
      Size = 50
      Lookup = True
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM EMPRESA')
    Left = 872
    Top = 408
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresanCdTerceiroEmp: TIntegerField
      FieldName = 'nCdTerceiroEmp'
    end
    object qryEmpresanCdTerceiroMatriz: TIntegerField
      FieldName = 'nCdTerceiroMatriz'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresanCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
end
