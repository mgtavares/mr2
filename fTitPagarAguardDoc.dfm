inherited frmTitPagarAguardDoc: TfrmTitPagarAguardDoc
  Left = 108
  Top = 0
  Width = 1036
  Height = 770
  Caption = 'Consulta de T'#237'tulo a Pagar aguardando Documento'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 97
    Width = 1028
    Height = 642
  end
  inherited ToolBar1: TToolBar
    Width = 1028
    ButtonWidth = 109
    TabOrder = 2
    inherited ToolButton1: TToolButton
      Caption = 'Executar Consulta'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 109
    end
    inherited ToolButton2: TToolButton
      Left = 117
    end
    object ToolButton4: TToolButton
      Left = 226
      Top = 0
      Width = 7
      Caption = 'ToolButton4'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object cxTextEdit3: TcxTextEdit
      Left = 233
      Top = 0
      Width = 25
      Height = 21
      Hint = 'T'#237'tulos Vencidos'
      Enabled = False
      ParentShowHint = False
      Properties.ReadOnly = True
      ShowHint = True
      Style.BorderColor = clRed
      Style.Color = clRed
      StyleDisabled.Color = clRed
      TabOrder = 2
    end
    object cxTextEdit5: TcxTextEdit
      Left = 258
      Top = 0
      Width = 87
      Height = 21
      Hint = 'T'#237'tulos Vencidos'
      Enabled = False
      ParentShowHint = False
      Properties.ReadOnly = True
      ShowHint = True
      Style.BorderColor = clRed
      Style.Color = clRed
      StyleDisabled.BorderStyle = ebsNone
      StyleDisabled.Color = clWhite
      StyleDisabled.TextColor = clBlack
      TabOrder = 4
      Text = 'T'#237'tulos Vencidos'
    end
    object cxTextEdit2: TcxTextEdit
      Left = 345
      Top = 0
      Width = 25
      Height = 21
      Hint = 'T'#237'tulos Vencendo nos pr'#243'ximos 15 dias'
      Enabled = False
      ParentShowHint = False
      Properties.ReadOnly = True
      ShowHint = True
      Style.Color = clYellow
      StyleDisabled.Color = clYellow
      TabOrder = 0
    end
    object cxTextEdit7: TcxTextEdit
      Left = 370
      Top = 0
      Width = 119
      Height = 21
      Hint = 'T'#237'tulos Vencidos'
      Enabled = False
      ParentShowHint = False
      Properties.ReadOnly = True
      ShowHint = True
      Style.BorderColor = clRed
      Style.Color = clRed
      StyleDisabled.BorderStyle = ebsNone
      StyleDisabled.Color = clWhite
      StyleDisabled.TextColor = clBlack
      TabOrder = 6
      Text = 'T'#237'tulos Vencendo Hoje'
    end
    object cxTextEdit1: TcxTextEdit
      Left = 489
      Top = 0
      Width = 25
      Height = 21
      Hint = 'T'#237'tulos Vencendo Hoje'
      Enabled = False
      ParentShowHint = False
      Properties.ReadOnly = True
      ShowHint = True
      Style.Color = clBlue
      StyleDisabled.Color = clBlue
      TabOrder = 1
    end
    object cxTextEdit6: TcxTextEdit
      Left = 514
      Top = 0
      Width = 207
      Height = 21
      Hint = 'T'#237'tulos Vencidos'
      Enabled = False
      ParentShowHint = False
      Properties.ReadOnly = True
      ShowHint = True
      Style.BorderColor = clRed
      Style.Color = clRed
      StyleDisabled.BorderStyle = ebsNone
      StyleDisabled.Color = clWhite
      StyleDisabled.TextColor = clBlack
      TabOrder = 5
      Text = 'T'#237'tulos que Vencer'#227'o nos pr'#243'ximos 15 dias'
    end
    object cxTextEdit4: TcxTextEdit
      Left = 721
      Top = 0
      Width = 25
      Height = 21
      Hint = 'T'#237'tulos Vencendo Hoje'
      Enabled = False
      ParentShowHint = False
      Properties.ReadOnly = True
      ShowHint = True
      Style.Color = clGradientInactiveCaption
      StyleDisabled.Color = clInfoBk
      TabOrder = 3
    end
    object cxTextEdit8: TcxTextEdit
      Left = 746
      Top = 0
      Width = 167
      Height = 21
      Hint = 'T'#237'tulos Vencidos'
      Enabled = False
      ParentShowHint = False
      Properties.ReadOnly = True
      ShowHint = True
      Style.BorderColor = clRed
      Style.Color = clRed
      StyleDisabled.BorderStyle = ebsNone
      StyleDisabled.Color = clWhite
      StyleDisabled.TextColor = clBlack
      TabOrder = 7
      Text = 'T'#237'tulos que Vencer'#227'o ap'#243's 15 dias'
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 97
    Width = 1028
    Height = 642
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsResultado
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghRowHighlight, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    OnDrawColumnCell = DBGridEh1DrawColumnCell
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdLojaTit'
        Footers = <>
        Width = 48
      end
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footers = <>
        Width = 87
      end
      item
        EditButtons = <>
        FieldName = 'cNrNF'
        Footers = <>
        Width = 67
      end
      item
        EditButtons = <>
        FieldName = 'dDtEmissao'
        Footers = <>
        Visible = False
        Width = 95
      end
      item
        EditButtons = <>
        FieldName = 'dDtReceb'
        Footers = <>
        Visible = False
        Width = 94
      end
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footers = <>
        Width = 92
      end
      item
        EditButtons = <>
        FieldName = 'nValTit'
        Footers = <>
        Width = 87
      end
      item
        EditButtons = <>
        FieldName = 'nCdSP'
        Footers = <>
        Width = 55
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        Width = 248
      end
      item
        EditButtons = <>
        FieldName = 'cNmContato'
        Footers = <>
        Width = 160
      end
      item
        EditButtons = <>
        FieldName = 'cTelefone'
        Footers = <>
        Width = 96
      end
      item
        EditButtons = <>
        FieldName = 'cFax'
        Footers = <>
        Width = 97
      end
      item
        EditButtons = <>
        FieldName = 'cEmail'
        Footers = <>
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'cNmFormaPagto'
        Footers = <>
        Width = 100
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1028
    Height = 68
    Align = alTop
    Caption = 'Filtros da Consulta'
    TabOrder = 0
    object Label4: TLabel
      Tag = 2
      Left = 157
      Top = 47
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label3: TLabel
      Tag = 2
      Left = 18
      Top = 47
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Vencimento'
    end
    object Label5: TLabel
      Tag = 2
      Left = 34
      Top = 23
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro'
    end
    object Label1: TLabel
      Tag = 2
      Left = 277
      Top = 48
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero NF'
    end
    object Label2: TLabel
      Tag = 2
      Left = 437
      Top = 47
      Width = 66
      Height = 13
      Caption = 'N'#250'mero T'#237'tulo'
    end
    object edtDtInicial: TMaskEdit
      Left = 77
      Top = 39
      Width = 74
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object edtDtFinal: TMaskEdit
      Left = 176
      Top = 39
      Width = 72
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
    object edtnCdTerceiro: TMaskEdit
      Left = 77
      Top = 15
      Width = 60
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = edtnCdTerceiroExit
      OnKeyDown = edtnCdTerceiroKeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 144
      Top = 15
      Width = 449
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = DataSource1
      TabOrder = 1
    end
    object edtNrNF: TEdit
      Left = 333
      Top = 40
      Width = 84
      Height = 21
      TabOrder = 4
    end
    object edtNrTit: TEdit
      Left = 512
      Top = 39
      Width = 81
      Height = 21
      TabOrder = 5
    end
  end
  inherited ImageList1: TImageList
    Left = 392
    Top = 192
  end
  object qryResultado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNrNF'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 7
        Value = '1'
      end
      item
        Name = 'cNrTit'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    SQL.Strings = (
      'DECLARE @cNrNF       varchar(15)'
      '       ,@nCdTerceiro int'
      '       ,@cNrTit      char(17)'
      '       ,@dDtInicial  varchar(10)'
      '       ,@dDtFinal    varchar(10)'
      ''
      'Set @cNrNF       = RTRIM(LTRIM(:cNrNF))'
      'Set @nCdTerceiro = :nCdTerceiro'
      'Set @cNrTit      = RTRIM(LTRIM(:cNrTit))'
      'Set @dDtInicial  = :dDtInicial'
      'Set @dDtFinal    = :dDtFinal'
      ''
      'SELECT Titulo.nCdTitulo'
      '      ,Titulo.cNrNF'
      '      ,Titulo.dDtEmissao'
      '      ,Titulo.dDtReceb'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValTit'
      '      ,Titulo.nCdSP'
      '      ,Titulo.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,(SELECT TOP 1 cNmContato'
      '          FROM Endereco'
      
        '         WHERE Endereco.nCdTerceiro = Titulo.nCdTerceiro) cNmCon' +
        'tato'
      '      ,(SELECT TOP 1 cTelefone'
      '          FROM Endereco'
      
        '         WHERE Endereco.nCdTerceiro = Titulo.nCdTerceiro) cTelef' +
        'one'
      '      ,(SELECT TOP 1 cFax'
      '          FROM Endereco'
      '         WHERE Endereco.nCdTerceiro = Titulo.nCdTerceiro) cFax'
      '      ,(SELECT TOP 1 cEmail'
      '          FROM Endereco'
      '         WHERE Endereco.nCdTerceiro = Titulo.nCdTerceiro) cEmail'
      '      ,cNmFormaPagto'
      '      ,Titulo.cNrTit'
      '      ,Titulo.cFlgDocCobranca'
      '      ,dbo.fn_ZeroEsquerda(Titulo.nCdLojaTit,3) nCdLojaTit'
      '  FROM Titulo'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Titulo.nCdT' +
        'erceiro'
      
        '       LEFT  JOIN FormaPagto ON FormaPagto.nCdFormaPagto = Terce' +
        'iro.nCdFormaPagto'
      ' WHERE cSenso = '#39'D'#39
      '   AND nSaldoTit > 0'
      '   AND cFlgDocCobranca = 0'
      '   AND ((cNrNF  = @cNrNF)  OR (@cNrNF  = '#39#39'))'
      '   AND ((cNrTit = @cNrTit) OR (@cNrTit = '#39#39'))'
      
        '   AND ((@dDtInicial = '#39'01/01/1900'#39') OR (Titulo.dDtVenc >= Conve' +
        'rt(DATETIME,@dDtInicial,103)))'
      
        '   AND ((@dDtFinal   = '#39'01/01/1900'#39') OR (Titulo.dDtVenc  < (Conv' +
        'ert(DATETIME,@dDtFinal,103)+1)))'
      
        '   AND ((Titulo.nCdTerceiro = @nCdTerceiro) OR (@nCdTerceiro = 0' +
        '))'
      ' ORDER BY dDtVenc')
    Left = 472
    Top = 224
    object qryResultadonCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|ID'
      FieldName = 'nCdTitulo'
    end
    object qryResultadocNrNF: TStringField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Nr NF'
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryResultadodDtEmissao: TDateTimeField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Data Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryResultadodDtReceb: TDateTimeField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Data Receb.'
      FieldName = 'dDtReceb'
    end
    object qryResultadodDtVenc: TDateTimeField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Data Vencto'
      FieldName = 'dDtVenc'
    end
    object qryResultadonValTit: TBCDField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Valor T'#237'tulo'
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResultadonCdSP: TIntegerField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Nr SP'
      FieldName = 'nCdSP'
    end
    object qryResultadonCdTerceiro: TIntegerField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|'
      FieldName = 'nCdTerceiro'
    end
    object qryResultadocNmTerceiro: TStringField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryResultadocNmContato: TStringField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Contato'
      FieldName = 'cNmContato'
      ReadOnly = True
      Size = 35
    end
    object qryResultadocTelefone: TStringField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Telefone'
      FieldName = 'cTelefone'
      ReadOnly = True
    end
    object qryResultadocFax: TStringField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Fax'
      FieldName = 'cFax'
      ReadOnly = True
    end
    object qryResultadocEmail: TStringField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Email'
      FieldName = 'cEmail'
      ReadOnly = True
    end
    object qryResultadocNmFormaPagto: TStringField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Forma Pagto'
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryResultadocNrTit: TStringField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Nr T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryResultadocFlgDocCobranca: TIntegerField
      FieldName = 'cFlgDocCobranca'
    end
    object qryResultadonCdLojaTit: TStringField
      DisplayLabel = 'T'#237'tulos aguardando documento de cobran'#231'a|Loja'
      FieldName = 'nCdLojaTit'
      ReadOnly = True
      Size = 15
    end
  end
  object dsResultado: TDataSource
    DataSet = qryResultado
    Left = 512
    Top = 224
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 744
    Top = 53
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 632
    Top = 376
  end
end
