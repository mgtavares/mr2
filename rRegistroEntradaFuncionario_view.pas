unit rRegistroEntradaFuncionario_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptRegistroEntradaFuncionario_view = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIO: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText4: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRBand5: TQRBand;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel7: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText6: TQRDBText;
    lblPeriodo: TQRLabel;
    QRDBText7: TQRDBText;
    QRGroup2: TQRGroup;
    QRDBText9: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText10: TQRDBText;
    QRLabel1: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOnCdRegistro: TIntegerField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOnCdEmpresa: TIntegerField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOcNmEmpresa: TStringField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOnCdLoja: TIntegerField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOcNmLoja: TStringField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOnCdUsuario: TIntegerField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOnCdTerceiro: TIntegerField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOcNmTerceiro: TStringField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOdDtRegistro: TDateTimeField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOcDiaSemana: TStringField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOcEntrada: TStringField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOcSaida: TStringField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOcSubTotal: TStringField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOcTotalDia: TStringField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOcTotalSemana: TStringField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOcTotalMes: TStringField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOcTotal: TStringField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOiSemana: TIntegerField;
    SPREL_REGISTRO_ENTRADA_FUNCIONARIOiAno: TIntegerField;
    QRGroup3: TQRGroup;
    QRLabel6: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    QRShape3: TQRShape;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRegistroEntradaFuncionario_view: TrptRegistroEntradaFuncionario_view;

implementation

{$R *.dfm}

end.
