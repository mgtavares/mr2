unit rVendaEstruturaMarcaLoja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls,comObj, ExcelXP;

type
  TrptVendaEstruturaMarcaLoja = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    edtDepartamento: TMaskEdit;
    Label2: TLabel;
    edtCategoria: TMaskEdit;
    Label3: TLabel;
    edtSubCategoria: TMaskEdit;
    Label4: TLabel;
    edtSegmento: TMaskEdit;
    Label5: TLabel;
    edtMarca: TMaskEdit;
    Label6: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentocNmDepartamento: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryCategoria: TADOQuery;
    qryCategoriacNmCategoria: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategoriacNmSubCategoria: TStringField;
    DBEdit4: TDBEdit;
    DataSource4: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentocNmSegmento: TStringField;
    DBEdit5: TDBEdit;
    DataSource5: TDataSource;
    qryMarca: TADOQuery;
    qryMarcacNmMarca: TStringField;
    DBEdit6: TDBEdit;
    DataSource6: TDataSource;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryMarcanCdMarca: TIntegerField;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qryCategorianCdCategoria: TAutoIncField;
    qrySegmentonCdSegmento: TAutoIncField;
    edtGrupoProduto: TMaskEdit;
    Label7: TLabel;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBEdit7: TDBEdit;
    DataSource7: TDataSource;
    edtLinha: TMaskEdit;
    Label11: TLabel;
    edtColecao: TMaskEdit;
    Label12: TLabel;
    edtClasseProduto: TMaskEdit;
    Label13: TLabel;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    DBEdit8: TDBEdit;
    DataSource8: TDataSource;
    qryColecao: TADOQuery;
    qryColecaonCdColecao: TIntegerField;
    qryColecaocNmColecao: TStringField;
    DBEdit9: TDBEdit;
    DataSource9: TDataSource;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TIntegerField;
    qryClasseProdutocNmClasseProduto: TStringField;
    DBEdit10: TDBEdit;
    DataSource10: TDataSource;
    edtCdCampanhaPromoc: TMaskEdit;
    Label14: TLabel;
    qryCampanhaPromoc: TADOQuery;
    qryCampanhaPromocnCdCampanhaPromoc: TIntegerField;
    qryCampanhaPromoccNmCampanhaPromoc: TStringField;
    qryCampanhaPromoccFlgAtivada: TIntegerField;
    qryCampanhaPromoccFlgSuspensa: TIntegerField;
    DBEdit11: TDBEdit;
    DataSource11: TDataSource;
    edtDtFinal: TMaskEdit;
    Label8: TLabel;
    edtDtInicial: TMaskEdit;
    Label9: TLabel;
    RadioGroup1: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtDepartamentoExit(Sender: TObject);
    procedure edtCategoriaExit(Sender: TObject);
    procedure edtSubCategoriaExit(Sender: TObject);
    procedure edtSegmentoExit(Sender: TObject);
    procedure edtMarcaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDepartamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSubCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSegmentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoProdutoExit(Sender: TObject);
    procedure edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLinhaExit(Sender: TObject);
    procedure edtColecaoExit(Sender: TObject);
    procedure edtClasseProdutoExit(Sender: TObject);
    procedure edtCdCampanhaPromocExit(Sender: TObject);
    procedure edtCdCampanhaPromocKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLinhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtColecaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClasseProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaEstruturaMarcaLoja: TrptVendaEstruturaMarcaLoja;

implementation

uses fMenu, fLookup_Padrao, rVendaEstruturaMarcaLoja_view;

{$R *.dfm}

procedure TrptVendaEstruturaMarcaLoja.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value           := frmMenu.nCdUsuarioLogado ;
  qryCampanhaPromoc.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

end;

procedure TrptVendaEstruturaMarcaLoja.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TrptVendaEstruturaMarcaLoja.edtDepartamentoExit(Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

  if not qryDepartamento.eof then
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TrptVendaEstruturaMarcaLoja.edtCategoriaExit(Sender: TObject);
begin
  inherited;

  qryCategoria.Close ;
  PosicionaQuery(qryCategoria, edtCategoria.Text) ;

  if not qryCategoria.eof then
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value ;


end;

procedure TrptVendaEstruturaMarcaLoja.edtSubCategoriaExit(Sender: TObject);
begin
  inherited;

  qrySubCategoria.Close ;
  PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

  if not qrySubCategoria.eof then
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  
end;

procedure TrptVendaEstruturaMarcaLoja.edtSegmentoExit(Sender: TObject);
begin
  inherited;

  qrySegmento.Close ;
  PosicionaQuery(qrySegmento, edtSegmento.Text) ;
  
end;

procedure TrptVendaEstruturaMarcaLoja.edtMarcaExit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, edtMarca.Text) ;
  
end;

procedure TrptVendaEstruturaMarcaLoja.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptVendaEstruturaMarcaLoja.edtDepartamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(129);

        If (nPK > 0) then
        begin

            edtDepartamento.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptVendaEstruturaMarcaLoja.edtCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit2.Text = '') then
        begin
            MensagemAlerta('Selecione um departamento.') ;
            edtDepartamento.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(46,'Categoria.nCdDepartamento = ' + edtDepartamento.Text);

        If (nPK > 0) then
        begin

            edtCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCategoria, edtCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptVendaEstruturaMarcaLoja.edtSubCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma Categoria.') ;
            edtCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(47,'SubCategoria.nCdCategoria = ' + edtCategoria.Text);

        If (nPK > 0) then
        begin

            edtSubCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptVendaEstruturaMarcaLoja.edtSegmentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit4.Text = '') then
        begin
            MensagemAlerta('Selecione uma SubCategoria.') ;
            edtSubCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'Segmento.nCdSubCategoria = ' + edtSubCategoria.Text);

        If (nPK > 0) then
        begin

            edtSegmento.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySegmento, edtSegmento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptVendaEstruturaMarcaLoja.edtMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin

            edtMarca.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, edtMarca.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptVendaEstruturaMarcaLoja.edtGrupoProdutoExit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
  
end;

procedure TrptVendaEstruturaMarcaLoja.edtGrupoProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin

            edtGrupoProduto.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;

        end ;

    end ;

  end ;


end;

procedure TrptVendaEstruturaMarcaLoja.ToolButton1Click(Sender: TObject);
var
  cFiltro       : string ;
  linha, coluna,LCID : integer;
  planilha      : variant;
  valorcampo    : string;
  objRel        : TrptVendaEstruturaMarcaLoja_view;
begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') then
      edtDtInicial.Text := DateToStr(Date) ;

  if (trim(edtDtFinal.Text) = '/  /') then
      edtDtFinal.Text := DateToStr(Date) ;

  objRel := TrptVendaEstruturaMarcaLoja_view.Create(nil);

  try
      try
          objRel.qryResultado.Close;
          objRel.qryResultado.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
          objRel.qryResultado.Parameters.ParamByName('nCdLoja').Value          := frmMenu.ConvInteiro(edtLoja.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdDepartamento').Value  := frmMenu.ConvInteiro(edtDepartamento.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdCategoria').Value     := frmMenu.ConvInteiro(edtCategoria.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdSubCategoria').Value  := frmMenu.ConvInteiro(edtSubCategoria.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdSegmento').Value      := frmMenu.ConvInteiro(edtSegmento.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdMarca').Value         := frmMenu.ConvInteiro(edtMarca.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdLinha').Value         := frmMenu.ConvInteiro(edtLinha.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdColecao').Value       := frmMenu.ConvInteiro(edtColecao.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdClasseProduto').Value := frmMenu.ConvInteiro(edtClasseProduto.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdGrupoProduto').Value  := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdCampanhaPromoc').Value:= frmMenu.ConvInteiro(edtCdCampanhaPromoc.Text) ;
          objRel.qryResultado.Parameters.ParamByName('dDtInicial').Value       := frmMenu.ConvData(edtDtInicial.Text) ;
          objRel.qryResultado.Parameters.ParamByName('dDtFinal').Value         := frmMenu.ConvData(edtDtFinal.Text) ;
          objRel.qryResultado.Open;

          if (RadioGroup1.ItemIndex = 0) then
          begin

              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              cFiltro := '' ;

              if (DBEdit1.Text <> '') then
                  cFiltro := cFiltro + '/ Loja: ' + trim(edtLoja.Text) + '-' + DbEdit1.Text ;

              if (DBEdit2.Text <> '') then
                  cFiltro := cFiltro + '/ Departamento: ' + trim(edtDepartamento.Text) + '-' + DbEdit2.Text ;

              if (DBEdit3.Text <> '') then
                  cFiltro := cFiltro + '/ Categoria: ' + trim(edtCategoria.Text) + '-' + DbEdit3.Text ;

              if (DBEdit4.Text <> '') then
                  cFiltro := cFiltro + '/ SubCategoria: ' + trim(edtSubCategoria.Text) + '-' + DbEdit4.Text ;

              if (DBEdit5.Text <> '') then
                  cFiltro := cFiltro + '/ Segmento: ' + trim(edtSegmento.Text) + '-' + DbEdit5.Text ;

              if (DBEdit6.Text <> '') then
                  cFiltro := cFiltro + '/ Marca: ' + trim(edtMarca.Text) + '-' + DbEdit6.Text ;

              if (DBEdit8.Text <> '') then
                  cFiltro := cFiltro + '/ Linha: ' + trim(edtLinha.Text) + '-' + DbEdit8.Text ;

              if (DBEdit9.Text <> '') then
                  cFiltro := cFiltro + '/ Cole��o: ' + trim(edtColecao.Text) + '-' + DbEdit9.Text ;

              if (DBEdit7.Text <> '') then
                  cFiltro := cFiltro + '/ Grupo Produto: ' + trim(edtGrupoProduto.Text) + '-' + DbEdit7.Text ;

              if (DBEdit10.Text <> '') then
                  cFiltro := cFiltro + '/ Classe Produto: ' + trim(edtClasseProduto.Text) + '-' + DbEdit10.Text ;

              if (DBEdit11.Text <> '') then
                  cFiltro := cFiltro + '/ CampanhaPromoc: ' + trim(edtCdCampanhaPromoc.Text) + '-' + DbEdit11.Text ;

             cFiltro := cFiltro + ' / Per�odo Venda: ' + edtDtInicial.Text + ' a ' + edtDtFinal.Text;

             objRel.lblFiltro1.Caption := cFiltro ;
             objRel.QRLabel8.Caption   := '* Posi��o Estoque em ' + edtDtFinal.Text ;

             objRel.QuickRep1.PreviewModal;

          end
          else
          begin

            try
                planilha:= CreateoleObject('Excel.Application');
            except
                MensagemErro('Ocorreu um erro no processamento da planilha, talvez o aplicativo de planilhas n�o esteja instalado corretamente.') ;
                exit ;
            end ;

            LCID := GetUserDefaultLCID;
            planilha.DisplayAlerts  := False;
            planilha.WorkBooks.add(1);
            planilha.ScreenUpdating := true;

            planilha.caption := 'Posi��o de Estoque - Estrutura x Marca';

            for linha := 0 to objRel.qryResultado.RecordCount - 1 do
            begin

               for coluna := 1 to objRel.qryResultado.FieldCount do
               begin

                 valorcampo := objRel.qryResultado.Fields[coluna - 1].AsString;
                 planilha.cells[linha + 2,coluna] := valorCampo;

                 if (objRel.qryResultado.Fields[coluna - 1].DataType = ftBCD) then
                 begin
                     planilha.Cells[Linha+2,coluna].NumberFormat        := '#.##0,00' ;
                     planilha.Cells[Linha+2,Coluna].HorizontalAlignment := xlRight ;
                 end ;

                 {Range[,'G'+LineString].NumberFormat := '$#,##0.00';}

               end;

               objRel.qryResultado.Next;

            end;

            for coluna := 1 to objRel.qryResultado.FieldCount do
            begin

               valorcampo := objRel.qryResultado.Fields[coluna - 1].DisplayLabel;
               planilha.cells[1,coluna] := valorcampo;

            end;

            planilha.columns.Autofit;
            planilha.Visible := True;

          end ;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel) ;
  end;
end;

procedure TrptVendaEstruturaMarcaLoja.edtLinhaExit(Sender: TObject);
begin
  inherited;

  qryLinha.Close;
  PosicionaQuery(qryLinha, edtLinha.Text) ;
  
end;

procedure TrptVendaEstruturaMarcaLoja.edtColecaoExit(Sender: TObject);
begin
  inherited;

  qryColecao.Close;
  PosicionaQuery(qryColecao, edtColecao.Text) ;
  
end;

procedure TrptVendaEstruturaMarcaLoja.edtClasseProdutoExit(Sender: TObject);
begin
  inherited;

  qryClasseProduto.Close;
  PosicionaQuery(qryClasseProduto, edtClasseProduto.Text) ;
  
end;

procedure TrptVendaEstruturaMarcaLoja.edtCdCampanhaPromocExit(Sender: TObject);
begin
  inherited;

  qryCampanhaPromoc.Close;
  PosicionaQuery(qryCampanhaPromoc, edtCdCampanhaPromoc.Text) ;
  
end;

procedure TrptVendaEstruturaMarcaLoja.edtCdCampanhaPromocKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(194);

        If (nPK > 0) then
            edtCdCampanhaPromoc.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptVendaEstruturaMarcaLoja.edtLinhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit6.Text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdMarca = ' + edtMarca.Text)
        else nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
            edtLinha.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptVendaEstruturaMarcaLoja.edtColecaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(52);

        If (nPK > 0) then
            edtColecao.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptVendaEstruturaMarcaLoja.edtClasseProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
            edtClasseProduto.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

initialization
    RegisterClass(TrptVendaEstruturaMarcaLoja) ;

end.
