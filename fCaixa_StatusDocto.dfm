inherited frmCaixa_StatusDocto: TfrmCaixa_StatusDocto
  Left = 16
  Top = 43
  Width = 1021
  Height = 736
  BorderIcons = [biSystemMenu]
  Caption = 'Status de Documento'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1013
    Height = 680
  end
  inherited ToolBar1: TToolBar
    Width = 1013
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1013
    Height = 680
    ActivePage = cxTabSheet3
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 680
    ClientRectRight = 1013
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'Comprovante Carnet'
      ImageIndex = 0
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 1013
        Height = 657
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsCrediario
        Flat = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Consolas'
        Font.Style = []
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Consolas'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdLanctoFin'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdCrediario'
            Footers = <>
            Width = 99
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenda'
            Footers = <>
            ReadOnly = True
            Visible = False
            Width = 72
          end
          item
            EditButtons = <>
            FieldName = 'cNmTerceiro'
            Footers = <>
            ReadOnly = True
            Width = 338
          end
          item
            EditButtons = <>
            FieldName = 'nValCrediario'
            Footers = <>
            ReadOnly = True
            Width = 136
          end
          item
            EditButtons = <>
            FieldName = 'nCdStatusDocto'
            Footers = <>
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'cNmStatusDocto'
            Footers = <>
            ReadOnly = True
            Width = 175
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Cheques'
      ImageIndex = 1
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 997
        Height = 643
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsCheque
        Flat = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Consolas'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDrawColumnCell = DBGridEh1DrawColumnCell
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdLanctoFin'
            Footers = <>
            ReadOnly = True
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdCheque'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtDeposito'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'iNrCheque'
            Footers = <>
            ReadOnly = True
            Width = 76
          end
          item
            EditButtons = <>
            FieldName = 'nValCheque'
            Footers = <>
            ReadOnly = True
            Width = 116
          end
          item
            EditButtons = <>
            FieldName = 'nCdBanco'
            Footers = <>
            ReadOnly = True
            Width = 54
          end
          item
            EditButtons = <>
            FieldName = 'cAgencia'
            Footers = <>
            ReadOnly = True
            Width = 53
          end
          item
            EditButtons = <>
            FieldName = 'cConta'
            Footers = <>
            ReadOnly = True
            Width = 111
          end
          item
            EditButtons = <>
            FieldName = 'cDigito'
            Footers = <>
            ReadOnly = True
            Width = 24
          end
          item
            EditButtons = <>
            FieldName = 'cCNPJCPF'
            Footers = <>
            ReadOnly = True
            Width = 125
          end
          item
            EditButtons = <>
            FieldName = 'dDtEmissao'
            Footers = <>
            ReadOnly = True
            Visible = False
            Width = 72
          end
          item
            EditButtons = <>
            FieldName = 'nCdStatusDocto'
            Footers = <>
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'cNmStatusDocto'
            Footers = <>
            ReadOnly = True
            Width = 175
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Comprovante Cart'#227'o'
      ImageIndex = 2
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 1013
        Height = 657
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsCartao
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        PopupMenu = PopupMenu2
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTransacaoCartao'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdLanctoFin'
            Footers = <>
            ReadOnly = True
            Visible = False
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'iNrDocto'
            Footers = <>
            ReadOnly = True
            Width = 113
          end
          item
            EditButtons = <>
            FieldName = 'dDtTransacao'
            Footers = <>
            ReadOnly = True
            Width = 59
          end
          item
            EditButtons = <>
            FieldName = 'iNrCartao'
            Footers = <>
            ReadOnly = True
            Visible = False
            Width = 84
          end
          item
            EditButtons = <>
            FieldName = 'cNmOperadora'
            Footers = <>
            ReadOnly = True
            Width = 143
          end
          item
            EditButtons = <>
            FieldName = 'iParcelas'
            Footers = <>
            ReadOnly = True
            Width = 49
          end
          item
            EditButtons = <>
            FieldName = 'nValTransacao'
            Footers = <>
            ReadOnly = True
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'nCdStatusDocto'
            Footers = <>
            Width = 57
          end
          item
            EditButtons = <>
            FieldName = 'cNmStatusDocto'
            Footers = <>
            ReadOnly = True
            Width = 175
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet4: TcxTabSheet
      Caption = 'Comprovante Lan'#231'amento Manual (Caixa em Aberto)'
      ImageIndex = 3
      object DBGridEh4: TDBGridEh
        Left = 0
        Top = 0
        Width = 1013
        Height = 657
        Align = alClient
        AllowedOperations = []
        DataGrouping.GroupLevels = <>
        DataSource = dsLanctoMan
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        PopupMenu = PopupMenu1
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdLanctoFin'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtLancto'
            Footers = <>
            Width = 88
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoLancto'
            Footers = <>
            Visible = False
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoLancto'
            Footers = <>
            Width = 272
          end
          item
            EditButtons = <>
            FieldName = 'nValLancto'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object qryCheque: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryChequeBeforePost
    OnCalcFields = qryChequeCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCheque'
      '      ,nCdBanco'
      '      ,cConta'
      '      ,cCNPJCPF'
      '      ,iNrCheque'
      '      ,cDigito'
      '      ,cAgencia'
      '      ,dDtEmissao'
      '      ,nCdStatusDocto'
      '      ,nValCheque'
      '      ,nCdLanctoFin'
      '      ,dDtDeposito'
      '  FROM Cheque'
      ' WHERE nCdContaBancariaDep = :nPK'
      'ORDER BY dDtDeposito,iNrCheque')
    Left = 344
    Top = 200
    object qryChequenCdCheque: TAutoIncField
      DisplayLabel = 'Dados do Cheque'
      FieldName = 'nCdCheque'
      ReadOnly = True
    end
    object qryChequenCdBanco: TIntegerField
      DisplayLabel = 'Dados do Cheque|Bco'
      FieldName = 'nCdBanco'
    end
    object qryChequecAgencia: TStringField
      DisplayLabel = 'Dados do Cheque|Ag.'
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryChequecConta: TStringField
      DisplayLabel = 'Dados do Cheque|Conta'
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryChequecDigito: TStringField
      DisplayLabel = 'Dados do Cheque|D'
      FieldName = 'cDigito'
      FixedChar = True
      Size = 1
    end
    object qryChequecCNPJCPF: TStringField
      DisplayLabel = 'Dados do Cheque|CNPJ/CPF'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequeiNrCheque: TIntegerField
      DisplayLabel = 'Dados do Cheque|Cheque'
      FieldName = 'iNrCheque'
    end
    object qryChequedDtEmissao: TDateTimeField
      DisplayLabel = 'Dados do Cheque|Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryChequenValCheque: TBCDField
      DisplayLabel = 'Dados do Cheque|Valor'
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryChequenCdStatusDocto: TIntegerField
      DisplayLabel = 'Status Documento|C'#243'd'
      FieldName = 'nCdStatusDocto'
    end
    object qryChequecNmStatusDocto: TStringField
      DisplayLabel = 'Status Documento|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmStatusDocto'
      Size = 50
      Calculated = True
    end
    object qryChequenCdLanctoFin: TIntegerField
      DisplayLabel = 'Lan'#231'amento|Financeiro'
      FieldName = 'nCdLanctoFin'
    end
    object qryChequedDtDeposito: TDateTimeField
      DisplayLabel = 'Dados do Cheque|Data Dep'#243'sito'
      FieldName = 'dDtDeposito'
    end
  end
  object qryStatusDocto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM StatusDocto'
      'WHERE nCdStatusDocto = :nPK')
    Left = 352
    Top = 256
    object qryStatusDoctonCdStatusDocto: TIntegerField
      FieldName = 'nCdStatusDocto'
    end
    object qryStatusDoctocNmStatusDocto: TStringField
      FieldName = 'cNmStatusDocto'
      Size = 50
    end
  end
  object qryCartao: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCartaoBeforePost
    OnCalcFields = qryCartaoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTransacaoCartao'
      '      ,dDtTransacao'
      '      ,iNrCartao'
      '      ,iNrDocto'
      '      ,nCdOperadoraCartao'
      '      ,nValTransacao '
      '  ,nCdStatusDocto'
      '      ,nCdLanctoFin'
      ',iParcelas'
      ',nCdCondPagtoCartao'
      ',iNrAutorizacao'
      '      ,cNrDoctoNSU'
      '      ,cNrAutorizacao'
      '  FROM TransacaoCartao'
      ' WHERE nCdContaBancaria = :nPK'
      'Order BY iNrDocto')
    Left = 416
    Top = 200
    object qryCartaonCdTransacaoCartao: TAutoIncField
      FieldName = 'nCdTransacaoCartao'
      ReadOnly = True
    end
    object qryCartaodDtTransacao: TDateTimeField
      DisplayLabel = 'Dados do Comprovante|Data'
      FieldName = 'dDtTransacao'
    end
    object qryCartaoiNrCartao: TLargeintField
      DisplayLabel = 'Dados do Comprovante|Cart'#227'o'
      FieldName = 'iNrCartao'
    end
    object qryCartaoiNrDocto: TIntegerField
      DisplayLabel = 'Dados do Comprovante|Documento'
      FieldName = 'iNrDocto'
    end
    object qryCartaonCdOperadoraCartao: TIntegerField
      DisplayLabel = 'Dados do Comprovante|'
      FieldName = 'nCdOperadoraCartao'
      Visible = False
    end
    object qryCartaocNmOperadora: TStringField
      DisplayLabel = 'Dados do Comprovante|Operadora'
      FieldKind = fkCalculated
      FieldName = 'cNmOperadora'
      Size = 50
      Calculated = True
    end
    object qryCartaonValTransacao: TBCDField
      DisplayLabel = 'Dados do Comprovante|Valor'
      FieldName = 'nValTransacao'
      Precision = 12
      Size = 2
    end
    object qryCartaonCdStatusDocto: TIntegerField
      DisplayLabel = 'Status Documento|C'#243'd'
      FieldName = 'nCdStatusDocto'
    end
    object qryCartaocNmStatusDocto: TStringField
      DisplayLabel = 'Status Documento|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmStatusDocto'
      Size = 50
      Calculated = True
    end
    object qryCartaonCdLanctoFin: TIntegerField
      DisplayLabel = 'Lan'#231'amento|Financeiro'
      FieldName = 'nCdLanctoFin'
    end
    object qryCartaoiParcelas: TIntegerField
      DisplayLabel = 'Dados do Comprovante|Parc.'
      FieldName = 'iParcelas'
    end
    object qryCartaonCdCondPagtoCartao: TIntegerField
      FieldName = 'nCdCondPagtoCartao'
    end
    object qryCartaoiNrAutorizacao: TIntegerField
      FieldName = 'iNrAutorizacao'
    end
    object qryCartaocNrDoctoNSU: TStringField
      FieldName = 'cNrDoctoNSU'
      Size = 50
    end
    object qryCartaocNrAutorizacao: TStringField
      FieldName = 'cNrAutorizacao'
      Size = 50
    end
  end
  object qryCrediario: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCrediarioBeforePost
    OnCalcFields = qryCrediarioCalcFields
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCrediario'
      '      ,dDtVenda'
      '      ,nCdTerceiro'
      '      ,nValCrediario'
      '      ,nCdStatusDocto'
      '      ,nCdLanctoFin'
      '  FROM Crediario'
      ' WHERE nCdContaBancaria = :nPK'
      '   AND dDtEstorno       IS NULL'
      'ORDER BY nCdCrediario')
    Left = 488
    Top = 200
    object qryCrediarionCdCrediario: TAutoIncField
      DisplayLabel = 'Dados da Opera'#231#227'o de Venda|Carnet'
      FieldName = 'nCdCrediario'
      ReadOnly = True
    end
    object qryCrediariodDtVenda: TDateTimeField
      DisplayLabel = 'Dados da Opera'#231#227'o de Venda|Data'
      FieldName = 'dDtVenda'
    end
    object qryCrediariocNmTerceiro: TStringField
      DisplayLabel = 'Dados da Opera'#231#227'o de Venda|Cliente Respons'#225'vel'
      FieldKind = fkCalculated
      FieldName = 'cNmTerceiro'
      Size = 50
      Calculated = True
    end
    object qryCrediarionCdTerceiro: TIntegerField
      DisplayLabel = 'Dados da Opera'#231#227'o de Venda|'
      FieldName = 'nCdTerceiro'
    end
    object qryCrediarionValCrediario: TBCDField
      DisplayLabel = 'Dados da Opera'#231#227'o de Venda|Valor Credi'#225'rio'
      FieldName = 'nValCrediario'
      Precision = 12
      Size = 2
    end
    object qryCrediarionCdStatusDocto: TIntegerField
      DisplayLabel = 'Status Documento|C'#243'd'
      FieldName = 'nCdStatusDocto'
    end
    object qryCrediariocNmStatusDocto: TStringField
      DisplayLabel = 'Status Documento|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmStatusDocto'
      Size = 50
      Calculated = True
    end
    object qryCrediarionCdLanctoFin: TIntegerField
      DisplayLabel = 'Lan'#231'amento|Financeiro'
      FieldName = 'nCdLanctoFin'
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 408
    Top = 264
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsCheque: TDataSource
    DataSet = qryCheque
    Left = 376
    Top = 200
  end
  object dsCartao: TDataSource
    DataSet = qryCartao
    Left = 448
    Top = 200
  end
  object dsCrediario: TDataSource
    DataSet = qryCrediario
    Left = 520
    Top = 200
  end
  object qryOperadora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmOperadoraCartao'
      'FROM OperadoraCartao'
      'WHERE nCdOperadoraCartao = :nPK')
    Left = 520
    Top = 256
    object qryOperadoracNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
  end
  object qryLanctoMan: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdResumoCaixa int'
      ''
      'SELECT @nCdResumoCaixa = nCdResumoCaixa'
      '  FROM ResumoCaixa'
      ' WHERE nCdContaBancaria = :nPK'
      '   AND dDtAbertura     <= GetDate()'
      '   AND nCdUsuarioFech  IS NULL'
      ' ORDER BY dDtAbertura DESC'
      ''
      'SELECT nCdLanctoFin'
      '      ,dDtLancto'
      '      ,LanctoFin.nCdTipoLancto'
      '      ,cNmTipoLancto'
      '      ,nValLancto'
      '  FROM LanctoFin'
      
        '       LEFT JOIN TipoLancto ON TipoLancto.nCdTipoLancto = Lancto' +
        'Fin.nCdTipoLancto'
      ' WHERE cFlgManual      = 1'
      '   AND dDtEstorno     IS NULL'
      '   ANd nCdResumoCaixa  = @nCdResumoCaixa'
      'ORDER BY nCdLanctoFin')
    Left = 680
    Top = 392
    object qryLanctoMannCdLanctoFin: TAutoIncField
      DisplayLabel = 'Lan'#231'amentos|Lancto'
      FieldName = 'nCdLanctoFin'
      ReadOnly = True
    end
    object qryLanctoMandDtLancto: TDateTimeField
      DisplayLabel = 'Lan'#231'amentos|Data'
      FieldName = 'dDtLancto'
    end
    object qryLanctoMannCdTipoLancto: TIntegerField
      DisplayLabel = 'Lan'#231'amentos|'
      FieldName = 'nCdTipoLancto'
    end
    object qryLanctoMancNmTipoLancto: TStringField
      DisplayLabel = 'Lan'#231'amentos|Tipo Lan'#231'amento'
      FieldName = 'cNmTipoLancto'
      Size = 50
    end
    object qryLanctoMannValLancto: TBCDField
      DisplayLabel = 'Lan'#231'amentos|Valor'
      FieldName = 'nValLancto'
      Precision = 12
      Size = 2
    end
  end
  object dsLanctoMan: TDataSource
    DataSet = qryLanctoMan
    Left = 720
    Top = 392
  end
  object PopupMenu1: TPopupMenu
    Left = 792
    Top = 432
    object ImprimirComprovante1: TMenuItem
      Caption = 'Imprimir Comprovante'
      OnClick = ImprimirComprovante1Click
    end
  end
  object PopupMenu2: TPopupMenu
    Left = 744
    Top = 176
    object AlterarOperadora1: TMenuItem
      Caption = 'Alterar Dados do Pagamento'
      OnClick = AlterarOperadora1Click
    end
  end
  object qryCondPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nPercAcrescimo'
      ',nCdTabTipoFormaPagto'
      'FROM CondPagto'
      
        'LEFT JOIN FormaPagto ON FormaPagto.nCdFormaPagto = CondPagto.nCd' +
        'FormaPagto'
      'WHERE nCdCondPagto = :nPK')
    Left = 568
    Top = 232
    object qryCondPagtonPercAcrescimo: TBCDField
      FieldName = 'nPercAcrescimo'
      Precision = 12
      Size = 2
    end
    object qryCondPagtonCdTabTipoFormaPagto: TIntegerField
      FieldName = 'nCdTabTipoFormaPagto'
    end
  end
  object qryLanctoFin: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cFlgLiqCrediario'
      'FROM LanctoFin'
      'WHERE nCdLanctoFin = :nPK')
    Left = 648
    Top = 240
    object qryLanctoFincFlgLiqCrediario: TIntegerField
      FieldName = 'cFlgLiqCrediario'
    end
  end
  object SP_ALTERA_CONDICAO_TRANSACAO_CARTAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ALTERA_CONDICAO_TRANSACAO_CARTAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTransacaoCartao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdCondPagto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdOperadoraCartao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNrDocto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNrAutorizacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNrDoctoNSU'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@cNrAutorizacao'
        Attributes = [paNullable]
        Size = 50
        Value = Null
      end>
    Left = 208
    Top = 216
  end
  object qryTipoOperacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cFlgTipoOperacao'
      'FROM OperadoraCartao'
      'WHERE nCdOperadoraCartao = :nPK')
    Left = 720
    Top = 240
    object qryTipoOperacaocFlgTipoOperacao: TStringField
      FieldName = 'cFlgTipoOperacao'
      FixedChar = True
      Size = 1
    end
  end
  object qryCartaoSituacaoAtual: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmOperadoraCartao'
      '      ,cNmCondPagto'
      '      ,cNmFormaPagto'
      '  FROM TransacaoCartao'
      
        '       LEFT JOIN OperadoraCartao ON OperadoraCartao.nCdOperadora' +
        'Cartao = TransacaoCartao.nCdOperadoraCartao'
      
        '       LEFT JOIN CondPagto       ON CondPagto.nCdCondPagto      ' +
        '       = TransacaoCartao.nCdCondPagtoCartao'
      
        '       LEFT JOIN FormaPagto      ON FormaPagto.nCdFormaPagto    ' +
        '       = CondPagto.nCdFormaPagto'
      ' WHERE nCdTransacaoCartao = :nPK')
    Left = 800
    Top = 224
    object qryCartaoSituacaoAtualcNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryCartaoSituacaoAtualcNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryCartaoSituacaoAtualcNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
  end
end
