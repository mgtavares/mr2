inherited frmPedidoComercial: TfrmPedidoComercial
  Left = 131
  Top = 13
  Width = 1178
  Height = 714
  Caption = 'Pedido Comercial'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 24
    Width = 1162
    Height = 652
    AutoSize = True
  end
  object Label1: TLabel [1]
    Left = 48
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 43
    Top = 62
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 65
    Top = 86
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 25
    Top = 110
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Pedido'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 46
    Top = 158
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 23
    Top = 182
    Width = 63
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Pedido'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 652
    Top = 158
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
  end
  object Label8: TLabel [8]
    Left = 623
    Top = 62
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = '% Desconto'
    FocusControl = DBEdit8
  end
  object Label10: TLabel [9]
    Left = 588
    Top = 86
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Pedido do Terceiro'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [10]
    Left = 896
    Top = 86
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'Contato'
    FocusControl = DBEdit11
  end
  object Label12: TLabel [11]
    Left = 172
    Top = 182
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Previs'#227'o Entrega'
    FocusControl = DBEdit7
  end
  object Label13: TLabel [12]
    Left = 344
    Top = 182
    Width = 16
    Height = 13
    Caption = 'at'#233
    FocusControl = DBEdit18
  end
  object Label17: TLabel [13]
    Left = 574
    Top = 38
    Width = 110
    Height = 13
    Alignment = taRightJustify
    Caption = 'Condi'#231#227'o Pagamento'
    FocusControl = DBEdit24
  end
  object Label19: TLabel [14]
    Left = 367
    Top = 614
    Width = 67
    Height = 13
    Caption = 'Total L'#237'quido'
    FocusControl = DBEdit30
  end
  object Label20: TLabel [15]
    Left = 531
    Top = 638
    Width = 91
    Height = 13
    Caption = 'Total dos Servi'#231'os'
    FocusControl = DBEdit31
  end
  object Label21: TLabel [16]
    Left = 723
    Top = 614
    Width = 97
    Height = 13
    Alignment = taRightJustify
    Caption = 'Total dos Impostos'
    FocusControl = DBEdit32
  end
  object Label22: TLabel [17]
    Left = 356
    Top = 638
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Total do Frete'
    FocusControl = DBEdit33
  end
  object Label25: TLabel [18]
    Left = 752
    Top = 636
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Outros'
    FocusControl = DBEdit36
  end
  object Label26: TLabel [19]
    Left = 936
    Top = 637
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Total do Pedido'
    FocusControl = DBEdit37
  end
  object Label27: TLabel [20]
    Left = 626
    Top = 134
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Comprador'
  end
  object Label36: TLabel [21]
    Left = 838
    Top = 62
    Width = 100
    Height = 13
    Alignment = taRightJustify
    Caption = '% Desconto Vencto'
    FocusControl = DBEdit35
  end
  object Label37: TLabel [22]
    Left = 1017
    Top = 62
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = '% Vendor'
    FocusControl = DBEdit40
  end
  object Label15: TLabel [23]
    Left = 609
    Top = 110
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Representante'
    FocusControl = DBEdit19
  end
  object Label9: TLabel [24]
    Left = 17
    Top = 134
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Local Entrega'
    FocusControl = DBEdit9
  end
  object Label16: TLabel [25]
    Left = 930
    Top = 613
    Width = 94
    Height = 13
    Caption = 'Total do Desconto'
  end
  object Label18: TLabel [26]
    Left = 526
    Top = 613
    Width = 97
    Height = 13
    Caption = 'Total dos Produtos'
  end
  inherited ToolBar2: TToolBar
    Width = 1162
    Height = 24
    inherited ToolButton9: TToolButton
      Visible = False
    end
    inherited btSalvar: TToolButton
      ImageIndex = 2
    end
    object ToolButton14: TToolButton
      Left = 856
      Top = 0
      Width = 8
      Caption = 'ToolButton14'
      ImageIndex = 12
      Style = tbsSeparator
    end
    object btFinalizarPed: TToolButton
      Left = 864
      Top = 0
      Caption = '&Finalizar'
      ImageIndex = 7
      OnClick = btFinalizarPedClick
    end
    object ToolButton3: TToolButton
      Left = 952
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object btCancelarPed: TToolButton
      Left = 960
      Top = 0
      Caption = 'Cancela&r'
      ImageIndex = 9
      OnClick = btCancelarPedClick
    end
    object ToolButton15: TToolButton
      Left = 1048
      Top = 0
      Width = 8
      Caption = 'ToolButton15'
      ImageIndex = 12
      Style = tbsSeparator
    end
    object ToolButton13: TToolButton
      Left = 1056
      Top = 0
      Caption = '&Op'#231#245'es'
      DropdownMenu = menuOpcao
      ImageIndex = 11
      PopupMenu = menuOpcao
    end
  end
  object DBEdit1: TDBEdit [28]
    Tag = 1
    Left = 88
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdPedido'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [29]
    Tag = 1
    Left = 88
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
    OnExit = DBEdit2Exit
    OnKeyDown = DBEdit2KeyDown
  end
  object DBEdit3: TDBEdit [30]
    Left = 88
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [31]
    Left = 88
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdTipoPedido'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit4Exit
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [32]
    Left = 88
    Top = 152
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 6
    OnExit = DBEdit5Exit
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [33]
    Left = 88
    Top = 176
    Width = 73
    Height = 19
    DataField = 'dDtPedido'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [34]
    Left = 686
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nPercDesconto'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit10: TDBEdit [35]
    Left = 686
    Top = 80
    Width = 195
    Height = 19
    DataField = 'cNrPedTerceiro'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit11: TDBEdit [36]
    Left = 942
    Top = 80
    Width = 177
    Height = 19
    DataField = 'cNmContato'
    DataSource = dsMaster
    TabOrder = 15
    OnEnter = DBEdit11Enter
  end
  object DBEdit12: TDBEdit [37]
    Tag = 1
    Left = 156
    Top = 56
    Width = 405
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 18
  end
  object DBEdit13: TDBEdit [38]
    Tag = 1
    Left = 156
    Top = 80
    Width = 405
    Height = 19
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 19
  end
  object DBEdit14: TDBEdit [39]
    Tag = 1
    Left = 156
    Top = 104
    Width = 405
    Height = 19
    DataField = 'cNmTipoPedido'
    DataSource = dsTipoPedido
    TabOrder = 20
  end
  object DBEdit15: TDBEdit [40]
    Tag = 1
    Left = 260
    Top = 152
    Width = 301
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 21
  end
  object DBEdit16: TDBEdit [41]
    Tag = 1
    Left = 156
    Top = 152
    Width = 101
    Height = 19
    DataField = 'cCNPJCPF'
    DataSource = dsTerceiro
    TabOrder = 22
  end
  object DBEdit17: TDBEdit [42]
    Tag = 1
    Left = 686
    Top = 152
    Width = 137
    Height = 19
    DataField = 'cNmTabStatusPed'
    DataSource = dsStatusPed
    TabOrder = 23
  end
  object DBEdit7: TDBEdit [43]
    Left = 264
    Top = 176
    Width = 73
    Height = 19
    DataField = 'dDtPrevEntIni'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit18: TDBEdit [44]
    Left = 368
    Top = 176
    Width = 73
    Height = 19
    DataField = 'dDtPrevEntFim'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit24: TDBEdit [45]
    Left = 686
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdCondPagto'
    DataSource = dsMaster
    TabOrder = 10
    OnExit = DBEdit24Exit
    OnKeyDown = DBEdit24KeyDown
  end
  object DBEdit27: TDBEdit [46]
    Tag = 1
    Left = 754
    Top = 32
    Width = 365
    Height = 19
    DataField = 'cNmCondPagto'
    DataSource = dsCondPagto
    TabOrder = 24
  end
  object cxPageControl1: TcxPageControl [47]
    Left = 8
    Top = 216
    Width = 1137
    Height = 385
    ActivePage = TabItemEstoque
    Focusable = False
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = True
    TabOrder = 25
    ClientRectBottom = 381
    ClientRectLeft = 4
    ClientRectRight = 1133
    ClientRectTop = 24
    object TabItemEstoque: TcxTabSheet
      Caption = 'Itens de Estoque - Grade'
      Enabled = False
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 41
        Width = 1129
        Height = 316
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsItemEstoque
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        PopupMenu = PopupMenu1
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnColEnter = DBGridEh1ColEnter
        OnColExit = DBGridEh1ColExit
        OnDrawColumnCell = DBGridEh1DrawColumnCell
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 46
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 254
          end
          item
            EditButtons = <>
            FieldName = 'cReferencia'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Produto|Referencia'
            Width = 92
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePed'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 46
          end
          item
            EditButtons = <>
            FieldName = 'cSiglaUnidadeMedida'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 27
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeExpRec'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 52
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeCanc'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footer.DisplayFormat = '#,##'
            Footer.FieldName = 'nValUnitario'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footer.Value = '0'
            Footer.ValueType = fvtSum
            Footers = <>
            Tag = 1
          end
          item
            EditButtons = <>
            FieldName = 'nPercDescontoItem'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 42
          end
          item
            EditButtons = <>
            FieldName = 'nValDesconto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <
              item
                DisplayFormat = '0.00#,######'
                FieldName = 'nValDesconto'
                ValueType = fvtSum
              end>
            Tag = 1
            Width = 63
          end
          item
            EditButtons = <>
            FieldName = 'nValAcrescimo'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Tag = 1
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nValTotalProduto'
            Footers = <
              item
                DisplayFormat = '0.00##,##'
                FieldName = 'nValTotalProduto'
                ValueType = fvtSum
              end>
            ReadOnly = True
            Title.Caption = 'Valores|Total Produto'
            Width = 84
          end
          item
            EditButtons = <>
            FieldName = 'nValTotalItem'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <
              item
                DisplayFormat = '0.00#,##'
                FieldName = 'nValTotalItem'
                ValueType = fvtSum
              end>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nPercIPI'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Title.Caption = 'IPI|% '
            Width = 42
          end
          item
            EditButtons = <>
            FieldName = 'nValIPI'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Title.Caption = 'IPI|Valor'
            Width = 53
          end
          item
            EditButtons = <>
            FieldName = 'nPercICMSSub'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Title.Caption = 'Subs. Tribut'#225'ria|% '
            Width = 44
          end
          item
            EditButtons = <>
            FieldName = 'nValICMSSub'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Subs. Tribut'#225'ria|Valor'
          end
          item
            EditButtons = <>
            FieldName = 'nValCustoUnit'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Tag = 1
            Width = 72
          end
          item
            EditButtons = <>
            FieldName = 'nValSugVenda'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Width = 74
          end
          item
            EditButtons = <>
            FieldName = 'dDtEntregaIni'
            Footers = <>
            Width = 89
          end
          item
            EditButtons = <>
            FieldName = 'dDtEntregaFim'
            Footers = <>
            Width = 89
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePrev'
            Footers = <>
            Width = 73
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1129
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        object Label23: TLabel
          Tag = 2
          Left = 728
          Top = 16
          Width = 68
          Height = 13
          Caption = 'Entrega Total'
          FocusControl = DBEdit30
        end
        object Label24: TLabel
          Tag = 2
          Left = 872
          Top = 16
          Width = 76
          Height = 13
          Caption = 'Entrega Parcial'
          FocusControl = DBEdit30
        end
        object Label29: TLabel
          Tag = 2
          Left = 1032
          Top = 16
          Width = 86
          Height = 13
          Caption = 'Saldo Cancelado'
          FocusControl = DBEdit30
        end
        object btnVisualizarGrade: TcxButton
          Left = 2
          Top = 4
          Width = 175
          Height = 33
          Caption = 'Visualizar / Alterar Grade'
          TabOrder = 0
          OnClick = btnVisualizarGradeClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF2F2F2E2E2E2E2E2
            E2E2E2E2E2E2E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFF2F2F2E2E2E27F7F7F4444445555559B9B9BFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFF2F2F2F2F2F2F2F2F2F2F2F2F2F2F2E2E2E27D7D7D4E4E4E9393
            938E8E8E636363FFFFFFFFFFFFFFFFFFF2F2F2F2F2F2E2E2E2E2E2E2E2E2E2E2
            E2E2E2E2E27B7B7B4F4F4F9191918F8F8FACACAC565656FFFFFFFFFFFFF2F2F2
            E2E2E27171714141415C5C5C5C5C5C3E3E3E3B3B3B3D3D3D949494969696ACAC
            AC636363949494FFFFFFF2F2F2E2E2E2494949A0A0A0E3E3E3EEEEEEEEEEEEE2
            E2E2ABABAB3A3A3A6A6A6AAFAFAF646464949494FFFFFFFFFFFFE2E2E24C4C4C
            CECECEECEAE6E3D9BCDCCC9BDCCC9BE3D9BCECEAE6CBCBCB4141415252529494
            94FFFFFFFFFFFFFFFFFF8787879A9A9AEDEBE7DBCA95DAC586DBC78ADCC88BDB
            C78ADDCC99ECEAE6ABABAB505050FFFFFFFFFFFFFFFFFFFFFFFF4F4F4FE8E8E8
            E4DBBEDBC688DECC91E1D099E2D29CE1D099DECC91E4D9BAF2F2F2474747FFFF
            FFFFFFFFFFFFFFFFFFFF5F5F5FF2F2F2DFCFA0DECC91E2D4A0E6DAACE7DDB1E6
            DAACE2D4A0E2D4A8F1F1F1737373FFFFFFFFFFFFFFFFFFFFFFFF606060F4F4F4
            E0D1A3E1D099E6DAACEBE5C0EEEAC9EBE5C0E6DAACE4D8AEF2F2F2777777FFFF
            FFFFFFFFFFFFFFFFFFFF555555EDEDEDE8DFC4E2D29CE7DDB1EEEAC9F3F3DBEE
            EAC9E7DDB1E9E1C6F9F9F94D4D4DFFFFFFFFFFFFFFFFFFFFFFFF8D8D8DB0B0B0
            F6F6F6E5D8AFE6DAACEBE5C0EEEAC9EBE5C0E8DEB9F4F3EFBABABA838383FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFF5A5A5ADDDDDDF9F9F9EDE8D4ECE5CBECE7CEEE
            EAD8F8F8F8DBDBDB5A5A5AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            5B5B5BBCBCBCF4F4F4FDFDFDFDFDFDFFFFFFBCBCBC5B5B5BFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8686865A5A5A7C7C7C7C7C7C51
            5151868686FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.Kind = lfOffice11
        end
        object cxTextEdit1: TcxTextEdit
          Left = 696
          Top = 8
          Width = 25
          Height = 21
          Style.Color = clBlue
          StyleDisabled.Color = clBlue
          TabOrder = 3
        end
        object cxTextEdit2: TcxTextEdit
          Left = 840
          Top = 8
          Width = 25
          Height = 21
          Enabled = False
          Style.Color = clBlue
          StyleDisabled.Color = clYellow
          TabOrder = 4
        end
        object cxTextEdit3: TcxTextEdit
          Left = 1000
          Top = 8
          Width = 25
          Height = 21
          Enabled = False
          Style.Color = clBlue
          StyleDisabled.Color = clRed
          TabOrder = 5
        end
        object cxButton3: TcxButton
          Left = 177
          Top = 4
          Width = 128
          Height = 33
          Caption = 'Pre'#231'o Esperado'
          TabOrder = 1
          TabStop = False
          OnClick = cxButton3Click
          Glyph.Data = {
            06030000424D06030000000000003600000028000000100000000F0000000100
            180000000000D002000000000000000000000000000000000000C6DEC6C6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE8C9C9C0000000000008C9C9C8C9C9CC6D6
            DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE8C9C9C00000000
            0000D6A58CD6A58C0000008C9C9CC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
            C6D6DE8C9C9C000000000000D6A58CFFEFDEFFEFDEFFEFDE0000008C9C9C8C9C
            9CC6D6DEC6D6DEC6D6DEC6D6DE8C9C9C000000000000D6A58CFFEFDEFFEFDEFF
            EFDEFFEFDEFFEFDED6A58C0000008C9C9CC6D6DEC6D6DEC6D6DEC6D6DE000000
            EFFFFFFFEFDEFFEFDEFFEFDEFFEFDEFFEFDEFFEFDEFFEFDEFFEFDE0000008C9C
            9C8C9C9CC6D6DEC6D6DEC6D6DE000000EFFFFFBDBDBDBDBDBDBDBDBDFFEFDEFF
            EFDEFFEFDEFFEFDEFFEFDED6A58C0000008C9C9CC6D6DEC6D6DEC6D6DE8C9C9C
            000000000000000000000000BDBDBDBDBDBDFFEFDEFFEFDEFFEFDEFFEFDE0000
            008C9C9C8C9C9CC6D6DEC6D6DE8C9C9C000000EFFFFFEFFFFFEFFFFF0000008C
            9C9CBDBDBDFFEFDEFFEFDEFFEFDED6A58C0000008C9C9CC6D6DEC6D6DE000000
            EFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDFFEFDEFFEFDEFFEFDEFFEF
            DE0000008C9C9C8C9C9C000000DEBDD6EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEF
            FFFF000000BDBDBDFFEFDEFFEFDEFFEFDED6A58C0000008C9C9C000000DEBDD6
            EFFFFFEFFFFF000000000000EFFFFFEFFFFF000000BDBDBDFFEFDEFFEFDEFFEF
            DED6A58C000000C6D6DE000000DEBDD6DEBDD6EFFFFF000000EFFFFFEFFFFFEF
            FFFF000000BDBDBDFFEFDEEFFFFF0000000000008C9C9CC6D6DEC6D6DE000000
            DEBDD6DEBDD6000000EFFFFFEFFFFF000000D6A58CEFFFFF0000000000008C9C
            9CC6D6DEC6D6DEC6D6DEC6DEC68C9C9C000000DEBDD6DEBDD6DEBDD60000008C
            9C9C0000000000008C9C9CC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
            C6DEC60000000000000000000000000000008C9C9CC6D6DEC6D6DEC6DEC6C6D6
            DEC6D6DEC6D6DEC6DEC6}
          LookAndFeel.NativeStyle = True
        end
        object cxButton5: TcxButton
          Left = 305
          Top = 4
          Width = 152
          Height = 33
          Caption = 'Programa'#231#227'o Entrega'
          TabOrder = 2
          TabStop = False
          OnClick = cxButton5Click
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            20000000000000040000C30E0000C30E00000000000000000000FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00C586
            5D00B6684100B0613800AB5C3400A7583100A4542F00A1522B009B4F28009B4F
            28009B4F28009B4F28009B4F28009B4F2800AE6D4900FFFFFF00FFFFFF00B96D
            4200F0E4DC00EEE1D700ECDCD000E8D7CA00E6D2C300E3CDBF00E1CABA00E1CA
            BA00E1CABA00E1CABA00E1CABA00E1CABA009E553100FFFFFF00FFFFFF00BC70
            4600F2E6DD00D8AE9100D2987000D2956D00D0946B00D0906500CB8B5F00C887
            5900C3805400C0794E00BC7D5400E1CABA00A0563100FFFFFF00FFFFFF00C276
            4A00F2E6DF00D8AE8E00FFFFFF00FCFCFC00FCFFFC00FCFFFC00FFFFFF00FCFC
            FC00FFFFFF00FFFFFF00BF805600E1CABA00A0542F00FFFFFF00FFFFFF00C578
            4D00F2E6DF00D7AB8A00FFFFFF00367C3B0034793900367C3B00367C3B00387D
            3D00367C3B00FFFFFF00C2865B00E1CABA00A2573100FFFFFF00FFFFFF00C87D
            5200F4E8E100D7A78300FFFFFF00FFFFFF0034793900FFFFFF00367C3B00FFFF
            FF0034793900FFFFFF00C58B6100E1CABA009D512A00FFFFFF00FFFFFF00CB82
            5600F4ECE300D7A47D00FFFFFF00FFFFFF0034793900FFFFFF00367C3B00FFFF
            FF003B824100FFFFFF00C8936800E1CABA00A0512A00FFFFFF00FFFFFF00CF86
            5900F4ECE400D5A07800FFFFFF00367C3B003B824100FFFFFF00367C3B00367C
            3B00367C3B00FFFFFF00CD987000E4D2C300A2542D00FFFFFF00FFFFFF00D28A
            5E00F6EEE800D59B7300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00CFA07900EEE3D800A7593100FFFFFF00FFFFFF00D58E
            6300F6EEE8003670FF00356FFF00346EFF00336DFF00316BFF002F68FF002E65
            FF002C63FF002A5FFF00285EFF00F6EEE800AB5D3500FFFFFF00FFFFFF00D893
            6600F6EEE8003570FF004693FF004290FF003F8DFF003C8BFF003884FF003380
            FF002E79FF002A74FF001A49FF00F6EEE800B0623A00FFFFFF00FFFFFF00DA95
            6900F6EEE8003670FF00356FFF00326CFF003069FF002C64FF002A61FF00255A
            FF002155FF001E4FFF001B4BFF00F6EEE800B4663D00FFFFFF00FFFFFF00DFA0
            7600F6EEE800F6EEE800F6EEE800F6EEE800F6EEE800F6EEE800F6EEE800F6EE
            E800F6EEE800F6EEE800F6EEE800F6EEE800BA6F4500FFFFFF00FFFFFF00EEC8
            B100E3A27B00DC976B00D8946800D8916500D58E6100D28A5D00D0875A00CD83
            5600CA7F5300C77B4F00C2774B00C0724900CA896100FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00}
          LookAndFeel.NativeStyle = True
        end
      end
    end
    object TabAD: TcxTabSheet
      Caption = 'Itens AD / Servi'#231'os'
      Enabled = False
      ImageIndex = 1
      object Label30: TLabel
        Tag = 2
        Left = 40
        Top = 342
        Width = 68
        Height = 13
        Caption = 'Entrega Total'
        FocusControl = DBEdit30
      end
      object Label31: TLabel
        Tag = 2
        Left = 184
        Top = 342
        Width = 76
        Height = 13
        Caption = 'Entrega Parcial'
        FocusControl = DBEdit30
      end
      object Label32: TLabel
        Tag = 2
        Left = 344
        Top = 342
        Width = 86
        Height = 13
        Caption = 'Saldo Cancelado'
        FocusControl = DBEdit30
      end
      object DBGridEh4: TDBGridEh
        Left = 0
        Top = 0
        Width = 1193
        Height = 329
        DataGrouping.GroupLevels = <>
        DataSource = dsItemAD
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        PopupMenu = PopupMenu1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDrawColumnCell = DBGridEh4DrawColumnCell
        OnEnter = DBGridEh4Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'cCdProduto'
            Footers = <>
            ReadOnly = True
            Width = 46
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            Width = 254
          end
          item
            EditButtons = <>
            FieldName = 'cSiglaUnidadeMedida'
            Footers = <>
            Width = 31
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePed'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeExpRec'
            Footers = <>
            ReadOnly = True
            Width = 73
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeCanc'
            Footers = <>
            ReadOnly = True
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footers = <>
            Tag = 1
            Width = 95
          end
          item
            EditButtons = <>
            FieldName = 'nValTotalItem'
            Footers = <>
            ReadOnly = True
            Width = 95
          end
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoItemPed'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdItemPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nPercIPI'
            Footers = <>
            Title.Caption = 'IPI|% '
          end
          item
            EditButtons = <>
            FieldName = 'nValIPI'
            Footers = <>
            ReadOnly = True
            Tag = 1
            Title.Caption = 'IPI|Valor'
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'nPercICMSSub'
            Footers = <>
            Title.Caption = 'Subs. Tribut'#225'ria|%'
            Width = 46
          end
          item
            EditButtons = <>
            FieldName = 'nValICMSSub'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Subs. Tribut'#225'ria|Valor'
            Width = 66
          end
          item
            EditButtons = <>
            FieldName = 'nValCustoUnit'
            Footers = <>
            ReadOnly = True
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object cxTextEdit4: TcxTextEdit
        Left = 8
        Top = 336
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clBlue
        TabOrder = 1
      end
      object cxTextEdit5: TcxTextEdit
        Left = 152
        Top = 336
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clYellow
        TabOrder = 2
      end
      object cxTextEdit6: TcxTextEdit
        Left = 312
        Top = 336
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clRed
        TabOrder = 3
      end
    end
    object TabFormula: TcxTabSheet
      Caption = 'Item Formulado'
      Enabled = False
      ImageIndex = 4
      object Label33: TLabel
        Tag = 2
        Left = 608
        Top = 302
        Width = 86
        Height = 13
        Caption = 'Saldo Cancelado'
        FocusControl = DBEdit30
      end
      object Label34: TLabel
        Tag = 2
        Left = 448
        Top = 302
        Width = 76
        Height = 13
        Caption = 'Entrega Parcial'
        FocusControl = DBEdit30
      end
      object Label35: TLabel
        Tag = 2
        Left = 304
        Top = 302
        Width = 68
        Height = 13
        Caption = 'Entrega Total'
        FocusControl = DBEdit30
      end
      object DBGridEh6: TDBGridEh
        Left = 0
        Top = 0
        Width = 1193
        Height = 321
        DataGrouping.GroupLevels = <>
        DataSource = dsItemFormula
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        PopupMenu = PopupMenu1
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnColExit = DBGridEh6ColExit
        OnDrawColumnCell = DBGridEh6DrawColumnCell
        OnEnter = DBGridEh6Enter
        OnKeyUp = DBGridEh6KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            Width = 88
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            Width = 430
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePed'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeExpRec'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeCanc'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nValCustoUnit'
            Footers = <>
            Width = 97
          end
          item
            EditButtons = <>
            FieldName = 'nValTotalItem'
            Footers = <>
            ReadOnly = True
            Width = 103
          end
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoItemPed'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdItemPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cSiglaUnidadeMedida'
            Footers = <>
            Visible = False
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object cxTextEdit7: TcxTextEdit
        Left = 576
        Top = 296
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clRed
        TabOrder = 1
      end
      object cxTextEdit8: TcxTextEdit
        Left = 416
        Top = 296
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clYellow
        TabOrder = 2
      end
      object cxTextEdit9: TcxTextEdit
        Left = 272
        Top = 296
        Width = 25
        Height = 21
        Enabled = False
        Style.Color = clBlue
        StyleDisabled.Color = clBlue
        TabOrder = 3
      end
      object cxButton2: TcxButton
        Left = 0
        Top = 328
        Width = 225
        Height = 25
        Caption = 'Visualizar/Alterar Composi'#231#227'o da Caixa'
        TabOrder = 4
        OnClick = cxButton2Click
        LookAndFeel.Kind = lfOffice11
        LookAndFeel.NativeStyle = True
      end
    end
    object TabCentroCusto: TcxTabSheet
      Caption = 'Centro Custo'
      Enabled = False
      ImageIndex = 6
      OnEnter = TabCentroCustoEnter
      object Image2: TImage
        Left = 0
        Top = 0
        Width = 1129
        Height = 357
        Align = alClient
        Picture.Data = {
          07544269746D6170A6290000424DA62900000000000036000000280000005A00
          0000270000000100180000000000702900000000000000000000000000000000
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000}
        Stretch = True
      end
      object Label14: TLabel
        Left = 16
        Top = 14
        Width = 61
        Height = 13
        Caption = 'Item Pedido'
      end
      object DBGridEh8: TDBGridEh
        Left = 8
        Top = 40
        Width = 721
        Height = 265
        DataGrouping.GroupLevels = <>
        DataSource = dsCCItemPedido
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh8KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItemPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdCC'
            Footers = <>
            Width = 91
          end
          item
            EditButtons = <>
            FieldName = 'cNmCC'
            Footers = <>
            ReadOnly = True
            Width = 491
          end
          item
            EditButtons = <>
            FieldName = 'nPercent'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBComboItens: TDBLookupComboboxEh
        Left = 80
        Top = 8
        Width = 649
        Height = 19
        AutoSelect = False
        EditButtons = <>
        KeyField = 'nCdItemPedido'
        ListField = 'cNmItem'
        ListSource = dsItemCC
        TabOrder = 1
        Visible = True
        OnChange = DBComboItensChange
      end
      object cxButton4: TcxButton
        Left = 733
        Top = 5
        Width = 25
        Height = 25
        Hint = 'Copiar o centro de custo deste item para todos os itens'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = cxButton4Click
        Glyph.Data = {
          66060000424D6606000000000000360000002800000017000000160000000100
          1800000000003006000000000000000000000000000000000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0CB8C44A654006336006336
          00A65400F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0A37600
          D9A77DCB8C44CB8C44CB8C44633600F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0A37600D9A77DD9A77DCB8C44CB8C44A37600A65400F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0A37600D9A77DD9A77DD9A77DCB8C44A37600A6
          5400F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0A37600D9A77DD9A77DD9A7
          7DD9A77DA37600A65400F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          A37600FFFFCCFFFFCCFFFFCCFFFFCCA37600F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0A37600A37600A37600A37600CB8C44F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0001FFFF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0001FFF001FFF001FFFF0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFF001FFF001FFF001FFF001FFFF0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFFF0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFF
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFFF0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0001FFFF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFF
          F0F0F0F0F0F0001FFFF0F0F0001FFFF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000}
        LookAndFeel.NativeStyle = True
      end
    end
    object TabParcela: TcxTabSheet
      Caption = 'Parcelas'
      Enabled = False
      ImageIndex = 3
      object Image3: TImage
        Left = 0
        Top = 0
        Width = 1129
        Height = 357
        Align = alClient
        Picture.Data = {
          07544269746D6170A6290000424DA62900000000000036000000280000005A00
          0000270000000100180000000000702900000000000000000000000000000000
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000}
        Stretch = True
      end
      object DBGridEh3: TDBGridEh
        Left = 8
        Top = 40
        Width = 465
        Height = 241
        DataGrouping.GroupLevels = <>
        DataSource = dsPrazoPedido
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdPrazoPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdPedido'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iDias'
            Footers = <>
            Width = 96
          end
          item
            EditButtons = <>
            FieldName = 'dVencto'
            Footers = <>
            Width = 138
          end
          item
            EditButtons = <>
            FieldName = 'nValPagto'
            Footers = <>
            Width = 139
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object btSugParcela: TcxButton
        Left = 8
        Top = 4
        Width = 121
        Height = 33
        Caption = 'Sugerir Parcelas'
        TabOrder = 1
        OnClick = btSugParcelaClick
        Glyph.Data = {
          36030000424D3603000000000000360000002800000010000000100000000100
          18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF91919189888887868687878788888789
          8988898888888888888887878686878685979797FFFFFFFFFFFFFFFFFFFFFFFF
          888888C2C2C1BCBCBCBCBCBCBCBCBBBCBCBBBCBCBBBCBCBBBCBCBBBCBCBBC2C2
          C18B8B8AFFFFFFFFFFFFFFFFFFFFFFFF8C8C8BFFFFFFEBEBEBEBEBEBE9E9E9E8
          E8E8E7E7E7E7E7E7E6E6E6E6E6E6FFFFFF8E8D8CFFFFFFFFFFFFFFFFFFFFFFFF
          929191FFFFFFB4B4B4949494E7E7E7B2B2B2939393E3E3E3B0B0B0919191FBFB
          FB929291FFFFFFFFFFFFFFFFFFFFFFFF959595FFFFFFE8E8E8E7E7E7E5E5E5E3
          E3E3E2E2E2E0E0E0DFDFDFDCDCDCFFFFFF959595FFFFFFFFFFFFFFFFFFFFFFFF
          9A9A99FFFFFFB1B1B1919191E2E2E2ADADAD8F8F8FDCDCDCA9A9A98D8D8DFBFB
          FB9A9999FFFFFFFFFFFFFFFFFFFFFFFF9E9D9DFFFFFFE3E3E3E1E1E1DCDCDCDB
          DBDBD7D7D7D3D3D3D3D3D3D1D1D1FFFFFF9E9D9CFFFFFFFFFFFFFFFFFFFFFFFF
          A0A0A0FFFFFFADADAD8E8E8ED8D8D8A5A5A58A8A8ACECECE7374E85258DBFBFB
          FB9E9E9EFFFFFFFFFFFFFFFFFFFFFFFFA3A3A3FCFCFCDADADAD7D7D7D2D2D2CE
          CECEC9C9C9C5C5C5C2C2C2BFBFBFFFFFFFA0A0A0FFFFFFFFFFFFFFFFFFFFFFFF
          A3A3A3FFFFFFB07B56C38D67C58F68C69069C8926BCA946CCA956EB07B56FFFF
          FFA1A09FFFFFFFFFFFFFFFFFFFFFFFFFA4A4A4FFFFFFB07B56C18B64C38D66C5
          8F67C69069C8926BCA946CB07B56FFFFFFA0A0A0FFFFFFFFFFFFFFFFFFFFFFFF
          A3A3A3FFFFFFA7724DA7724DA7724DA7724DA7724DA7724DA7724DA7724DFFFF
          FF9E9E9EFFFFFFFFFFFFFFFFFFFFFFFFAAAAAAFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA3A3A3FFFFFFFFFFFFFFFFFFFFFFFF
          B3B3B3A9A9A9A8A8A8ABABABACACACADADADACACACABABABA8A8A8A4A4A4A3A3
          A3B5B5B5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
        LookAndFeel.Kind = lfOffice11
      end
    end
    object TabAbatimentos: TcxTabSheet
      Caption = 'Abatimentos'
      ImageIndex = 6
      object DBGridEh7: TDBGridEh
        Left = 0
        Top = 0
        Width = 1129
        Height = 357
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTituloAbatPedido
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyDown = DBGridEh7KeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTitulo'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenc'
            Footers = <>
            ReadOnly = True
            Width = 97
          end
          item
            EditButtons = <>
            FieldName = 'cNmEspTit'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'cObsTit'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nPercAbat'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object TabFollowUp: TcxTabSheet
      Caption = 'Follow Up'
      ImageIndex = 3
      object Image4: TImage
        Left = 0
        Top = 0
        Width = 1129
        Height = 357
        Align = alClient
        Picture.Data = {
          07544269746D6170A6290000424DA62900000000000036000000280000005A00
          0000270000000100180000000000702900000000000000000000000000000000
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          0000C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2
          B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296
          C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B296C2B2
          96C2B296C2B296C2B296C2B296C2B2960000}
        Stretch = True
      end
      object DBGridEh5: TDBGridEh
        Left = 0
        Top = 4
        Width = 721
        Height = 309
        DataGrouping.GroupLevels = <>
        DataSource = dsFollow
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'dDtFollowUp'
            Footers = <>
            Width = 141
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            Width = 172
          end
          item
            EditButtons = <>
            FieldName = 'cOcorrenciaResum'
            Footers = <>
            Width = 338
          end
          item
            EditButtons = <>
            FieldName = 'dDtProxAcao'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object DBMemo1: TDBMemo
        Left = 728
        Top = 4
        Width = 409
        Height = 309
        DataField = 'cOcorrencia'
        DataSource = dsFollow
        Enabled = False
        TabOrder = 1
      end
    end
    object TabObservacao: TcxTabSheet
      Caption = 'Observa'#231#227'o'
      ImageIndex = 7
      object DBMemo2: TDBMemo
        Left = 0
        Top = 0
        Width = 1129
        Height = 357
        Align = alClient
        DataField = 'cOBS'
        DataSource = dsMaster
        TabOrder = 0
      end
    end
    object tabPreDistrib: TcxTabSheet
      Caption = 'Itens Pr'#233'-Distribuidos'
      Enabled = False
      ImageIndex = 8
      object gridPreDistrib: TcxGrid
        Left = 0
        Top = 0
        Width = 1129
        Height = 357
        Align = alClient
        TabOrder = 0
        object gridPreDistribDBTableView1: TcxGridDBTableView
          PopupMenu = popupDistrib
          DataController.DataSource = dsItemPedidoPreDistribuido
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          object gridPreDistribDBTableView1cNmProdutoPai: TcxGridDBColumn
            Caption = 'Produto'
            DataBinding.FieldName = 'cNmProdutoPai'
            Visible = False
            GroupIndex = 0
            Width = 321
          end
          object gridPreDistribDBTableView1cNmProduto: TcxGridDBColumn
            Caption = 'Grade'
            DataBinding.FieldName = 'cNmProduto'
            Visible = False
            GroupIndex = 1
            Width = 289
          end
          object gridPreDistribDBTableView1nCdLoja: TcxGridDBColumn
            Caption = 'C'#243'd. Loja'
            DataBinding.FieldName = 'nCdLoja'
            Width = 44
          end
          object gridPreDistribDBTableView1cNmLoja: TcxGridDBColumn
            Caption = 'Descri'#231#227'o da Loja'
            DataBinding.FieldName = 'cNmLoja'
          end
          object gridPreDistribDBTableView1nQtdePed: TcxGridDBColumn
            Caption = 'Qtd. Ped.'
            DataBinding.FieldName = 'nQtdePed'
          end
          object gridPreDistribDBTableView1nQtdeDistrib: TcxGridDBColumn
            Caption = 'Qtd. Distrib.'
            DataBinding.FieldName = 'nQtdeDistrib'
            Width = 87
          end
          object gridPreDistribDBTableView1nSaldo: TcxGridDBColumn
            Caption = 'Saldo'
            DataBinding.FieldName = 'nSaldo'
            Width = 71
          end
        end
        object gridPreDistribLevel1: TcxGridLevel
          GridView = gridPreDistribDBTableView1
        end
      end
    end
  end
  object DBEdit30: TDBEdit [48]
    Tag = 1
    Left = 432
    Top = 608
    Width = 89
    Height = 19
    DataField = 'nValProdutos'
    DataSource = dsMaster
    TabOrder = 26
  end
  object DBEdit31: TDBEdit [49]
    Tag = 1
    Left = 624
    Top = 632
    Width = 89
    Height = 19
    DataField = 'nValServicos'
    DataSource = dsMaster
    TabOrder = 27
  end
  object DBEdit32: TDBEdit [50]
    Tag = 1
    Left = 824
    Top = 608
    Width = 89
    Height = 19
    DataField = 'nValImposto'
    DataSource = dsMaster
    TabOrder = 28
  end
  object DBEdit33: TDBEdit [51]
    Left = 432
    Top = 632
    Width = 89
    Height = 19
    DataField = 'nValFrete'
    DataSource = dsMaster
    TabOrder = 29
  end
  object DBEdit36: TDBEdit [52]
    Left = 824
    Top = 632
    Width = 89
    Height = 19
    DataField = 'nValOutros'
    DataSource = dsMaster
    TabOrder = 30
  end
  object DBEdit37: TDBEdit [53]
    Tag = 1
    Left = 1024
    Top = 632
    Width = 89
    Height = 19
    DataField = 'nValPedido'
    DataSource = dsMaster
    TabOrder = 31
  end
  object DBGridEh2: TDBGridEh [54]
    Left = 248
    Top = 432
    Width = 633
    Height = 41
    DataGrouping.GroupLevels = <>
    DataSource = dsGrade
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 32
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    Visible = False
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object DBEdit34: TDBEdit [55]
    Tag = 1
    Left = 826
    Top = 152
    Width = 293
    Height = 19
    DataField = 'cDadoAutoriz'
    DataSource = dsDadoAutorz
    TabOrder = 33
  end
  object DBEdit35: TDBEdit [56]
    Left = 942
    Top = 56
    Width = 49
    Height = 19
    DataField = 'nPercDescontoVencto'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit40: TDBEdit [57]
    Left = 1070
    Top = 56
    Width = 49
    Height = 19
    DataField = 'nPercAcrescimoVendor'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit19: TDBEdit [58]
    Left = 686
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdTerceiroRepres'
    DataSource = dsMaster
    TabOrder = 16
    OnExit = DBEdit19Exit
    OnKeyDown = DBEdit19KeyDown
  end
  object DBEdit20: TDBEdit [59]
    Tag = 1
    Left = 754
    Top = 104
    Width = 365
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiroRepres
    TabOrder = 34
  end
  object DBEdit21: TDBEdit [60]
    Left = 686
    Top = 128
    Width = 65
    Height = 19
    DataField = 'nCdTerceiroColab'
    DataSource = dsMaster
    TabOrder = 17
    OnExit = DBEdit21Exit
    OnKeyDown = DBEdit21KeyDown
  end
  object DBEdit9: TDBEdit [61]
    Left = 88
    Top = 128
    Width = 65
    Height = 19
    DataField = 'nCdEstoqueMov'
    DataSource = dsMaster
    TabOrder = 5
    OnEnter = DBEdit9Enter
    OnExit = DBEdit9Exit
    OnKeyDown = DBEdit9KeyDown
  end
  object DBEdit23: TDBEdit [62]
    Tag = 1
    Left = 156
    Top = 128
    Width = 405
    Height = 19
    DataField = 'cNmLocalEstoque'
    DataSource = dsLocalEstoqueEntrega
    TabOrder = 35
  end
  object DBEdit22: TDBEdit [63]
    Tag = 1
    Left = 754
    Top = 128
    Width = 365
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiroColab
    TabOrder = 36
  end
  object btContatoPadrao: TcxButton [64]
    Left = 1122
    Top = 79
    Width = 25
    Height = 25
    Hint = 'Sugerir Contato Padr'#227'o'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 37
    TabStop = False
    OnClick = btContatoPadraoClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD4DFE64F575C45
      4646404242464D50C9D6DFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF838E95BCBCBBEBEAEACDCCCCA3A19F5A656DFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF79868CA6A5A2A8
      A2A29D9998948F8B525960FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFDBE5EC6683977A95A33A8A98357F8C606E762E4458DFE8EFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3F1FB70AFE1469DE64BBEF747
      E6FD41E5FD51C3FB167CDE3D88D3D5E8F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      E6F3FB69B3E9A6D3F365AEF074E1F673E1F672E0F671E0F64CA3EC9CC3EF2C81
      D7C9E0F5FFFFFFFFFFFFFFFFFFFDFEFF7FC0ECA5D4F3DCFAFE38A1EB74E1F66A
      E4F65DE2F572E0F61691E8C0F5FDACCEF12D83D7EAF3FBFFFFFFFFFFFFB7DDF5
      8BC8EFECFCFE77E1F72F99EA75E1F674E1F668DEF573E1F60986E646D5F3DCFE
      FE6FAAE579B3E6FFFFFFFFFFFF7FC5EEC9E9F9D4F9FD7CE3F786E5F860B1EF68
      B5EF63B4EF4CA6EC82E4F759DCF58AEBFACBE2F7398FDAFFFFFFFFFFFF7EC6EE
      DFF6FDC8F5FCCDF6FCD6F7FDD3F4FCCFF2FCCAF1FBC4F0FCBAF2FB96EAF872E5
      F7E2F4FD3289D8FFFFFFFFFFFF95D2F2D2EFFBDBF9FEDFF9FDECFBFEEEFCFEEF
      FCFEEFFCFEEBFBFEE0F9FEB8F1FBA8F1FBCBE5F83E95DDFFFFFFFFFFFFC8E9F9
      B4E3F8E5FAFEDBF8FDE4FAFEF0FCFEF9FEFFF9FEFFEFFCFED2F6FDB4F1FBEDFD
      FF6BB3EA88C2ECFFFFFFFFFFFFFCFEFFB5E2F6C3EBFAE2F9FDE0F9FDD5F7FDCF
      F6FDC9F4FCC7F4FCD6F9FDEBFAFE90CAF250A9E6F3F9FDFFFFFFFFFFFFFFFFFF
      F7FCFEC4E7F9B7E4F8C7ECFBD7F3FCE1F7FDE2F8FED8F0FCB6DFF86BBBED6CB9
      EBE8F4FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2F5FBC1E6F8A8DDF694
      D2F185CCF080C8F087CBF0BBE1F6FFFFFFFFFFFFFFFFFFFFFFFF}
    LookAndFeel.NativeStyle = True
  end
  object edtTotalLiq: TcxTextEdit [65]
    Left = 623
    Top = 608
    Width = 90
    Height = 21
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    Style.BorderStyle = ebsFlat
    Style.Color = cl3DLight
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    StyleFocused.BorderStyle = ebsFlat
    StyleFocused.Color = clCream
    StyleHot.BorderStyle = ebsFlat
    TabOrder = 38
  end
  object edtTotalDesc: TcxTextEdit [66]
    Left = 1023
    Top = 608
    Width = 90
    Height = 21
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.ReadOnly = True
    Style.BorderColor = clWhite
    Style.BorderStyle = ebsFlat
    Style.Color = cl3DLight
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Tahoma'
    Style.Font.Style = []
    StyleDisabled.BorderStyle = ebsFlat
    StyleFocused.BorderStyle = ebsFlat
    StyleFocused.Color = clCream
    StyleHot.BorderStyle = ebsFlat
    TabOrder = 39
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Pedido'
      'WHERE nCdPedido = :nPK')
    Left = 32
    Top = 352
    object qryMasternCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasternCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryMasternCdTabTipoPedido: TIntegerField
      FieldName = 'nCdTabTipoPedido'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasternCdTerceiroColab: TIntegerField
      FieldName = 'nCdTerceiroColab'
    end
    object qryMasternCdTerceiroTransp: TIntegerField
      FieldName = 'nCdTerceiroTransp'
    end
    object qryMasternCdTerceiroPagador: TIntegerField
      FieldName = 'nCdTerceiroPagador'
    end
    object qryMasterdDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtPrevEntIni: TDateTimeField
      FieldName = 'dDtPrevEntIni'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtPrevEntFim: TDateTimeField
      FieldName = 'dDtPrevEntFim'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasternCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryMasternCdTabStatusPed: TIntegerField
      FieldName = 'nCdTabStatusPed'
    end
    object qryMasternCdIncoterms: TIntegerField
      FieldName = 'nCdIncoterms'
    end
    object qryMasternPercDesconto: TBCDField
      FieldName = 'nPercDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 5
      Size = 2
    end
    object qryMasternPercAcrescimo: TBCDField
      FieldName = 'nPercAcrescimo'
      DisplayFormat = '#,##0.00'
      Precision = 5
      Size = 2
    end
    object qryMasternValProdutos: TBCDField
      FieldName = 'nValProdutos'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValServicos: TBCDField
      FieldName = 'nValServicos'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValImposto: TBCDField
      FieldName = 'nValImposto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValDesconto: TBCDField
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValAcrescimo: TBCDField
      FieldName = 'nValAcrescimo'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValFrete: TBCDField
      FieldName = 'nValFrete'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValOutros: TBCDField
      FieldName = 'nValOutros'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValPedido: TBCDField
      FieldName = 'nValPedido'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercNrPedTerceiro: TStringField
      FieldName = 'cNrPedTerceiro'
      Size = 15
    end
    object qryMastercOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
    object qryMasterdDtAutor: TDateTimeField
      FieldName = 'dDtAutor'
    end
    object qryMasternCdUsuarioAutor: TIntegerField
      FieldName = 'nCdUsuarioAutor'
    end
    object qryMasterdDtRejeicao: TDateTimeField
      FieldName = 'dDtRejeicao'
    end
    object qryMasternCdUsuarioRejeicao: TIntegerField
      FieldName = 'nCdUsuarioRejeicao'
    end
    object qryMastercMotivoRejeicao: TMemoField
      FieldName = 'cMotivoRejeicao'
      BlobType = ftMemo
    end
    object qryMastercNmContato: TStringField
      FieldName = 'cNmContato'
      Size = 35
    end
    object qryMasternCdEstoqueMov: TIntegerField
      FieldName = 'nCdEstoqueMov'
    end
    object qryMastercFlgCritico: TIntegerField
      FieldName = 'cFlgCritico'
    end
    object qryMasternSaldoFat: TBCDField
      FieldName = 'nSaldoFat'
      Precision = 12
      Size = 2
    end
    object qryMastercOBSFinanc: TMemoField
      FieldName = 'cOBSFinanc'
      BlobType = ftMemo
    end
    object qryMasternPercDescontoVencto: TBCDField
      FieldName = 'nPercDescontoVencto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternPercAcrescimoVendor: TBCDField
      FieldName = 'nPercAcrescimoVendor'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternCdMotBloqPed: TIntegerField
      FieldName = 'nCdMotBloqPed'
    end
    object qryMasternCdTerceiroRepres: TIntegerField
      FieldName = 'nCdTerceiroRepres'
    end
    object qryMasternCdUsuarioComprador: TIntegerField
      FieldName = 'nCdUsuarioComprador'
    end
  end
  inherited dsMaster: TDataSource
    Left = 32
    Top = 384
  end
  inherited qryID: TADOQuery
    Left = 152
    Top = 520
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 399
    Top = 480
  end
  inherited qryStat: TADOQuery
    Left = 320
    Top = 517
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 399
    Top = 448
  end
  inherited ImageList1: TImageList
    Left = 359
    Top = 520
    Bitmap = {
      494C01010C000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E3E7F7009AA7E3004C62CC00364FC500344DC300465DC70095A1DE00E1E5
      F600000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C6000000000000000000000000000000000000000000BFC7
      EF004C63D1005264D4008490E70095A0EE00959FED00838EE5004C5DCE003D54
      C300B8C1E9000000000000000000000000000000000000000000000000000000
      000000000000C2C2C200717272006D6C6B0071727200777B7B00A4A8A800B6B6
      B6000000000000000000000000000000000000000000F1F1F100E6E6E600E1E1
      E100E0E0E000DEDEDE00DDDDDD00DBDBDB00DADADA00D9D9D900D7D7D700D6D6
      D600D5D5D500DADADA00E9E9E900FFFFFF000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000C1CAF1004760
      D5007584E300A1ACF4007F8BEC005C67E4005B66E3007D87EA009FA8F1006F7C
      DD00324BC200B9C1EA000000000000000000000000000000000000000000CCCC
      CC009E9E9E003D424200191D23000101010001010100040506003D4242003D42
      420051575700B2B2B2000000000000000000F4F4F4007898B4001C5D95001F60
      98001C5D95001B5C96001B5B95001A5B95001A5994001A5994001A5994001959
      9300195892001958910063819E00FFFFFF000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF000923690051575700000000000000000000000000E7EAFA005970DE007888
      E600A3B0F5005767E7005665E6008992ED008892EC00535FE200525DE1009FA9
      F2006F7DDD004157C600E2E6F600000000000000000000000000C6C6C6009E9E
      9E0031333200092369003D424200CCCCCC00C6C6C600B9BABA00B9BABA00C2C2
      C200313332008C908F00B6B6B60000000000EFEFEF001F6098003AA9D9001F60
      98003AA9D90046C6F30044C4F30042C4F30042C3F30042C3F40041C3F40041C2
      F40040C2F40034A3D9001A5A9200FFFFFF0000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF000923690051575700000000000000000000000000A8B4F0006073E000A4B3
      F7005A6EEB00596CEA005869E800FCFCFC00FCFCFC005562E5005461E300535F
      E2009FA9F2005061D10097A4E100000000000000000000000000CCC5C0003133
      3200107082001070820031333200D3C1B500EEEEEE00FCFEFE00EEEEEE00CCC5
      C0003133320093929200AEAEAE0000000000F1F1F1001F649C0050CBF2001F64
      9C0050CBF2004CCAF30049C9F30047C7F30046C6F20043C5F30043C4F30042C4
      F30042C3F30041C3F3001A599400FFFFFF0000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF0009236900515757000000000000000000000000006A82E9008E9FF0008499
      F4005C73EE005B70EC005A6EEB00909DF100A6AFF3005767E7005665E6005562
      E5007D89EB008591E7004E64CE000000000000000000000000007B8585001070
      82000599CF000599CF0010708200191D23009B9C8F00FCFEFE00AAA39F000405
      06000923690065646400B2B2B20000000000F3F3F30021679E0065D4F4002167
      9E0065D4F4005BD1F30051CEF3004ECCF2004BC9F20048C9F20047C7F20045C6
      F20044C5F20043C5F2001B5C9500FFFFFF0000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF0009236900515757000000000000000000000000005D76EA00A0B3F7006580
      F2005F78F0005D76EF005C73EE00FCFCFC00FCFCFC00596CEA005869E8005767
      E7005D6CE70099A5F1003C55CC000000000000000000000000003D4242001070
      82000599CF0007F0F90010708200191D23009E9E9E00FCFEFE00A4A8A800191D
      2300191D23003D424200C6C6C60000000000F4F4F400246CA2007FDFF600246C
      A2007FDFF60067D7F4005DD3F30058D1F20052CEF2004ECDF1004CCAF20048C9
      F10046C7F10046C7F1001D5E9800FFFFFF0000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF000923690051575700000000000000000000000000617BEE00A1B6F8006784
      F400607CF3005F7AF1005F78F000FCFCFC00FCFCFC005B70EC005A6EEB00596C
      EA005F6FE9009BA8F1004159D0000000000000000000000000003D4242000599
      CF0007F0F90007F0F9003D424200CBB9B100FCFEFE00FCFEFE00FCFEFE00DBC8
      C300040506003D424200CCCCCC0000000000F6F6F600276FA6009BE9F800276F
      A6009BE9F80074DEF50069DAF40062D7F3005BD4F20054D0F10051CEF1004ECC
      F10048C9F00049C9F0001E619A00FFFFFF0000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF000923690051575700000000000000000000000000768DF30091A6F30088A1
      F8006280F400617EF300607CF300FCFCFC00FCFCFC005D76EF005C73EE005B70
      EC008293F1008998EC005970D8000000000000000000000000003D4242000599
      CF0007F0F90007F0F90007F0F90010708200DAD2C900FCFEFE00DADDD7005157
      5700107082003D424200CCCCCC0000000000F8F8F8002A74AA00B3F1FB0081E4
      F7001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E98001D5E
      98001D5E98001D5E980091B0C800FFFFFF0000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF000923690051575700000000000000000000000000B2BFFA006C81EC00A9BD
      FB006382F5006281F5006280F400FCFCFC00FCFCFC005F7AF1005F78F0005D76
      EF00A5B5F8005D70DD00A2AFEB00000000000000000000000000939292001070
      820007F0F90007F0F900B0F4FA007EE1DC0010708200CBB9B100515757000599
      CF0010708200939292000000000000000000F9F9F9002D7AAE00C6F7FD008EE8
      F80088E7F80080E4F60078E1F50070DEF40067DAF3005ED6F10057D2F0004ECE
      EE0064D6F2002268A000FFFFFF00FFFFFF0000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000EBEEFE00758CF7008397
      F000A9BDFB006382F5006382F500FCFCFC00FCFCFC00617EF300607CF300A6B9
      F9007B8DEA005C74E100E7EAFA00000000000000000000000000CCC5C0003D42
      42000599CF0007F0F900C4FDFD008BF6F90007F0F900107082000599CF000599
      CF003D424200CCC5C0000000000000000000FAFAFA00307FB300D4FAFE0099EC
      FA0092EBF9008BE8F800C2F6FC00B9F4FB00AFF1FA00A3EEF90097EAF80081E2
      F50062C2DF00266EA300FFFFFF00FFFFFF0000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF00107082005157570000000000000000000000000000000000CED7FD006D86
      F8008497F100A9BDFB008AA3F8006B89F6006B89F60089A2F800A8BCFA007F92
      EC005972E500C6CEF60000000000000000000000000000000000C6C6C600B8B1
      AA00484E4E001070820007F0F90007F0F90007F0F90007F0F900107082003D42
      4200B8B1AA00C6C6C6000000000000000000000000003386B800DFFCFE00A4F0
      FB009DEFFA00D6FBFE002F7EB1002E7BB0002D7BAF002C7AAE002C78AD002974
      AA003076AB0091B0C800FFFFFF00FFFFFF000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A8000000000000000000000000000000000000000000CED7
      FD00778EFA006E83EE0092A6F400A0B4F800A0B4F80091A6F300687DE9006981
      ED00C8D1F800000000000000000000000000000000000000000000000000C6CA
      C600AEAEAE007A7363003D42420009236900092369003D4242007A736300AEAE
      AE00C6CAC600000000000000000000000000FDFDFD00388BBD00BADFED00E7FD
      FF00E4FDFF00B3DEED003383B600F3F3F300F7F7F700F6F6F600F5F5F500F3F3
      F300F4F4F400F9F9F900FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EBEEFF00B5C3FD007C94FA006C86F7006A84F500778EF500B1BEF800E9EC
      FD00000000000000000000000000000000000000000000000000000000000000
      000000000000CCCCCC00AAA39F00AAA39F00AAA39F00AAA39F00CCCCCC000000
      000000000000000000000000000000000000FFFFFF00A8CDE2004295C300398F
      C000378DBE003D8EBE00A2C6DB0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C0000000000369DD9003199D8002C94
      D7002890D600238CD5001E88D4001A84D3001580D200117CD1000E79D1000A76
      D0000773CF000470CF00016ECE00000000000000000000000000636363006363
      6300636363006363630063636300636363006363630063636300C6948C00C694
      8C000000000000000000C6948C00C6948C000000000000000000000000000000
      0000C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C00000000003DA3DA00BCEBFA00BCEB
      FC00BFEEFE00C6F4FF00CEF8FF00D3FAFF00D0F8FF00C7F2FF00BAE9FC00B3E4
      F900B0E2F800B0E2F8000571CF00000000000000000000000000636363000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EFFFFF008C9C9C0000000000C6948C000000000000000000000000000000
      00000000000000000000EFFFFF00000000000000840000008400000000000000
      0000EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C000000000043A8DB00BFECFB0059CF
      F50041B0EC004EBAEF005AC2EF0060C6EF005CC4EF004CB6EF0037A5E6002A9A
      E10038B8EE00B1E3F8000975D00000000000C6DEC60000000000636363000000
      0000C6948C008C9C9C008C9C9C008C9C9C008C9C9C008C9C9C0000000000EFFF
      FF008C9C9C008C9C9C0000000000C6948C000000000000000000000000000000
      84000000840000000000EFFFFF00EFFFFF00000000000000840000000000EFFF
      FF00EFFFFF0000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000049ADDC00C1EEFB005FD3
      F7006CDBFC007FE5FF008FEDFF0097F2FF0093EDFF007CDFFF005BCCF80046BE
      EF003CBAEE00B3E3F9000E79D100000000000000000000000000BDBDBD000000
      000000000000000000000000000000000000C6948C0000000000EFFFFF008C9C
      9C008C9C9C000000000000000000C6948C000000000000000000000084002100
      C6002100C6000000000000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF00000000000000000000000000000000004EB2DD00C3EFFB0065D6
      F8004CB6EC005ABDEF0095EBFF003097DD004D82AB0084E1FF0041A9E900329F
      E10042BEEF00B4E5F900137ED200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF008C9C
      9C000000000000000000000000006363630000000000000000002100C6000000
      84002100C6002100C6002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000000000000000C6948C000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000053B7DE00C6F0FC006AD9
      F8007CE2FD0090E8FF0099E9FF00329FDF00548BB2008AE2FF006AD0F90050C5
      F10046C1F000B6E7F9001883D300000000000000000000000000000000000000
      000000000000E7F7F700EFFFFF00000000000000000000000000000000000000
      00000000000000000000000000006363630000000000000084002100C6002100
      C6002100C6000000FF002100C60000000000EFFFFF00EFFFFF00EFFFFF000000
      00000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000058BBDF00C7F1FC006FDC
      F90056BBED0061BDEF009BE7FF0035A6E2004BA4E10090E2FF0049ADE90038A4
      E30049C4F000B8E8F9001E88D40000000000000000000000000000000000EFFF
      FF00EFFFFF00DEBDD600DEBDD600EFFFFF00EFFFFF00C6948C00000000008C9C
      9C008C9C9C008C9C9C000000000063636300000000002100C6000000FF002100
      C6000000FF000000FF0000000000EFFFFF00EFFFFF0000000000EFFFFF00EFFF
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF00000000000000000000000000000000005CBFE000C8F3FC0075DF
      F90089E6FD0095E7FF009AE5FF00AAEEFF00A8EDFF0099E3FF0074D5F90059CC
      F3004FC8F100BBE9FA00248DD50000000000000000008C9C9C00000000000000
      000000000000000000000000000000000000EFFFFF0000000000000000000000
      00008C9C9C008C9C9C000000000063636300000000000000FF002100C6000000
      FF000000FF0000000000EFFFFF00EFFFFF002100C6000000FF0000000000EFFF
      FF00EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000060C2E100C9F3FC00CBF3
      FD00D4F6FE00D7F6FF00D8F4FF00E0F8FF00DFF8FF00DAF5FF00CDF1FC00C2ED
      FA00BDEBFA00BDEBFA002B93D60000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      00008C9C9C008C9C9C000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000EFFFFF00000000000000FF002100C6000000FF000000
      0000EFFFFF0000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C000000000061C3E10088A0A8009191
      91008E8E8E005AB9DC0055B8DF0051B5DE004DB1DD0049ADDC0046A8D7007878
      780076767600657E8D003199D80000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100A56300000000
      000000000000000000000000000063636300000000002100C6000000FF000000
      FF000000FF0000000000000000000000FF000000FF000000FF002100C6000000
      FF000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C000000000000000000B1B1B100C6C6
      C60094949400FBFBFB0000000000000000000000000000000000FBFBFB007D7D
      7D00ABABAB00969696000000000000000000000000008C9C9C0000000000FF9C
      3100FF9C3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C31000000
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF000000FF002100
      C6002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C00636363000000000000000000000000000000000000000000BCBCBC00C4C4
      C400A1A1A100EEEEEE0000000000000000000000000000000000EBEBEB008989
      8900A9A9A900A4A4A400000000000000000000000000000000008C9C9C00FF9C
      3100FF9C3100FF9C3100A5630000FF9C3100A5630000FF9C3100000000006B42
      0000A5630000A5630000000000006363630000000000000000000000FF000000
      FF000000FF00EFFFFF00EFFFFF000000FF000000FF000000FF002100C6000000
      FF002100C6000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000D4D4D400BABA
      BA00BFBFBF00A6A6A600F2F2F200FDFDFD00FDFDFD00F1F1F10093939300A8A8
      A8009E9E9E00C3C3C300000000000000000000000000000000008C9C9C008C9C
      9C0000000000000000000000000000000000000000000000000000000000A563
      0000A5630000A563000000000000636363000000000000000000000000000000
      FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF002100
      C600000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000FBFBFB00AEAE
      AE00C4C4C400BEBEBE00A1A1A100969696009393930097979700AEAEAE00AEAE
      AE0095959500FBFBFB0000000000000000000000000000000000000000000000
      00008C9C9C008C9C9C008C9C9C008C9C9C000000000000000000000000000000
      0000000000000000000000000000636363000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C0000000000000000000000000000000000EEEE
      EE00AEAEAE00BCBCBC00CACACA00CCCCCC00CACACA00C2C2C200ADADAD009B9B
      9B00E9E9E9000000000000000000000000000000000000000000636363006363
      630000000000000000000000000000000000C6948C00C6948C00C6948C006363
      6300636363006363630063636300636363000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FBFBFB00D0D0D000BABABA00B1B1B100AEAEAE00B3B3B300C9C9C900FAFA
      FA000000000000000000000000000000000000000000ADBBBA0096ADAF009BB6
      BA0091AAAE00A7BABF0096A9AE00A0B0B6009CACB200A0B0B600A0B0B60093A9
      AE0089A2A6009AB5B9009FB3B400C1D3D2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000AEBCB800BECCCA00BAD3D500B8D2
      D800C0D8DE00BFD2D700C6D7DA00B9C8CB00B9C8CB00C0CFD200C2D3D600C8DC
      E100C8E0E600BCD6DC00BBCDCE00ADBBB9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A3B3AC00C2D4CD00C3D8D900C2D7
      D900B5C6C9008F9B9B00939B9A00979E9B009095930089908D009BA3A20093A2
      A4008195960097ACAE00CCDAD800A0ABA8000000000000000000BACACE00BACA
      CE00BACACE00BACACE00BACACE00BACACE00BACACE0000000000C2D2D600AEBE
      C2009DABAC00C6D6DA000000000000000000000000000000000000000000AAA3
      9F006D6C6B0065646400575D5E006564640065646400656464006D6C6B006564
      6400898A8900C6C6C60000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A210000000000A2B6B100C5DBD600CFE4E200ADBF
      BE0002100F00000200000002000010100A000D0E050002020000090D08000A15
      1300000201007A898B00C8D4D400A0ABA90000000000C6D6DA00C6DADA00C6DA
      DA00C6DADA00CADADE00CDDEE200CDDEE200CDDEE200CEE2E400BECED200636C
      6C003D4242009AA6AA0000000000000000000000000000000000B6B6B6003D42
      42000405060051575700717272006D6C6B00575D5E0051575700191D2300191D
      230031333200898A890000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000091AAA600BFDAD600BED1CE00C9D6
      D40000080600C5C7C100C5C3BB00BEB9B000C2BEB300B9B7AD00D8D6CE00A7AC
      AA000F1A18008D9A9800CDD9DB0097A3A50000000000C2D6D600C6D6DA00C6DA
      DA00CDDEE20000000000ABB9BA00ABB9BA00B2C1C200000000005C646500AEAE
      AE00898A89003D42420000000000000000000000000000000000484E4E000599
      CF0009236900DAD2C900FCFEFE00FCFEFE00EEEEEE00D5D5D500484E4E000599
      CF000923690051575700000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000094B0B000C2E0E100BDCDCC00CDD6
      D300050B0600F8F4E900EFE6D900FFF5E500FFF3E100FFF2E200E3DACD00C9CA
      C100000300008E979400C8D4DA00A2AFB70000000000C2D6D600C6DADA00CDDE
      E200A6B4B60051575700191D230031333200575D5E00484E4E00BEBEBE00898A
      890031333200A2AFB000000000000000000000000000000000003D42420031CD
      FD000923690091846E00B3AD9E00AAA39F009392920091846E00313332000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000096ACAA00C5DADB00C4D6D700CAD6
      D60000020000F0EADF00F7EADA00FFEFDC00FFEFDA00FCEBD800FFFCEC00C7C8
      BF0000020000A5B1B100C4D7DC0097AAAF0000000000C2D6D600CADEDE00909E
      A0003133320084909100D2DEDF00CEDADE00777B7B003D4242009E9E9E003D42
      42009AA6AA00D6EAEB00000000000000000000000000000000003D42420031CD
      FD00107082000923690009236900092369000923690009236900107082000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000099ABAA00C6DBD900C4D5D800C9D6
      D80000020000F0EADF00F7EADA00FFEFDA00FFF3DD00F5E2CD00F0E3D300B2B4
      AE000C1512007A878900C3D7DC0096AAAF0000000000C2D6D600C6DADA003D42
      4200BECED200BAC5C600D5D5D500CCD5D500BECACA00ABB9BA00191D23008490
      9100CDDEE200CADEDE00000000000000000000000000000000003D42420031CD
      FD000599CF001070820009236900092369000923690009236900479EC0000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000099ABAA00C6DADB00C6D5D800C9D6
      D80000010000EEEADF00F5EBDA00FEEEDD00FDECD900FFFFEE00D8CDBF00BCBE
      B8000002010099A6A800C4D7DC0097AAAD0000000000CDDEE2006D7778005157
      5700313332003133320031333200313332003133320031333200515757005C64
      6500C2D2D200CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900B7A68A00CCC5C000C2BEB900C2BEB900DBC8C300515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000ECEAE000F2EBDC00FBEEDE00FAECDA00FFFBE900E6DED100C1C5
      C0000001000089969800C4D7DC0097AAAD0000000000D3E4E500575D5E00575D
      5E001070820007F0F90007F0F90007F0F90007F0F9003D424200575D5E00484E
      4E00C6D6DA00CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900CBB9B100E6E6E600DEDEDE00DEDEDE00EEEEEA00515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8DADB00C6D5D800C9D6
      D80000010000EBE9E100EEEADF00F7EEE000E3D9C800F6EDDF00FCF8ED00B6BC
      B700070F0E0093A0A200C6D6DC0097AAAD0000000000D3E4E500575D5E00575D
      5E00191D2300107082001070820010708200107082003133320071727200484E
      4E00C2D6D600CDDEE200000000000000000000000000000000003D42420031CD
      FD0009236900C2B9AE00DEDEDE00DADDD700DADDD700E6E6E600515757000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000099ABAC00C8D9DC00C8D4D800CAD6
      D80000010100E5E9E300E9EAE100F0EEE300E3DFD400F4F2E7000203000099A0
      9D0000020200C3CFD100C6D7DA0099ABAC0000000000C6DADA00A6B4B6003D42
      42006D6C6B00777B7B00898A89008C908F00939292008E9A9A003D42420096A3
      A600C6DADA00CADADE00000000000000000000000000000000003D42420031CD
      FD0009236900B8B1AA00DEDEDE00D5D5D500D5D5D500E6E6E600484E4E000599
      CF000923690051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A003163630031636300000000009BACAF00C7D7DD00CDD9DD00C6D2
      D40000020200FBFFFC00F6FAF400EFF2E900FFFFF700F0F3EA00000200000001
      0000CDD7D700C8D4D600C5D7D8009AACAD0000000000C2D6D600CEE2E4005157
      57005C646500909EA00000000000D6E6EA00B2C1C2007B8585005C646500D3E4
      E500C6D6DA00CADADA00000000000000000000000000000000003D42420031CD
      FD0009236900DAD2C900FCFEFE00F9FAFA00F9FAFA00FCFEFE00575D5E000599
      CF001070820051575700000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B5003163630000000000000000009AABAE00C3D3D900CFDBDF00C1CC
      D00000010300000200000003000001080100000500000E150E0000020000CFD9
      D900BECACC00DEE9ED00CFDEE0009CAEAF0000000000C2D6D600CADEDE00C2D2
      D2005C646500484E4E003D4242003D424200484E4E00636C6C00CADADE00CADA
      DA00C6DADA00CADADE0000000000000000000000000000000000777B7B003D42
      4200191D2300484E4E00575D5E005157570051575700575D5E00191D2300191D
      2300575D5E00A4A8A800000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      000000000000000000000000000000000000A5B8BD00B7C7CD00C5D1D500BDC8
      CC00CCD8DA00D7E3E300B3C0BE00BFCDC900E0EEEA00B5C3BF00E0EDEB00B4C0
      C200BBC7C900E5F0F400B7C5C400A5B5B40000000000BED2D200C2D2D200C2D6
      D600D6EAEB00A6B4B6007B8585007B858500AFBDBE00D6EAEB00C2D2D600C2D2
      D200C2D2D200C2D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC600000000000000000000000000B1C4C900AEBEC50095A1A500ADB8
      BC00B2BEC200AAB8B70097A8A500A5B6B30094A5A20096A7A40090A19E009EAB
      AD00BBC7CB00939EA200B2C0BF00B8C6C4000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFFFFFFFFFFFFFFFFFFF00FFFFFFFFF
      E003E007F80F8000C003C003E0030000C0038001C0010000C0038001C0010000
      C0038001C0010000C0038001C0010000C0038001C0010000C0038001C0010000
      C0038001C0030000C0038001C0030000C003C003C0038000C003E007E0070000
      FFFFF00FF81F0100FFFFFFFFFFFF0000FFFFFFFF81C08001C000F00301808001
      DFE0E001000080015000C00100008001D002800100008001CF06800100008001
      B9CE000100008001A0020001000080013F620001000080012002000100008001
      200E00010000C3C3200280030181C3C3800280038181C0038FC2C0078181C003
      C03EE00F8181E007C000F83F8383F00F8000FFFFFFFFFFFF0000FFFFFFFFFE00
      0000C043E003FE0000008003C003000000008443C003000000008003C0030000
      00008003C003000000008003C003000000008003C003000000008003C0030000
      00008003C003000000008003C003000000008203C003000100008003C0030003
      00008003FFFF00770000FFFFFFFF007F00000000000000000000000000000000
      000000000000}
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Empresa.nCdEmpresa'
      '      ,Empresa.cNmEmpresa'
      'FROM UsuarioEmpresa UE'
      'INNER JOIN Empresa ON Empresa.nCdEmpresa = UE.nCdEmpresa'
      'WHERE UE.nCdUsuario = :nCdUsuario'
      'AND UE.nCdEmpresa = :nPK')
    Left = 64
    Top = 352
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Loja.nCdLoja'
      '      ,Loja.cNmLoja'
      ',Loja.nCdLocalEstoquePadrao'
      'FROM UsuarioLoja UL'
      'INNER JOIN Loja ON Loja.nCdLoja = UL.nCdLoja'
      'WHERE UL.nCdUsuario = :nCdUsuario'
      'AND UL.nCdLoja = :nPK'
      'AND Loja.nCdEmpresa = :nCdEmpresa')
    Left = 96
    Top = 352
    object qryLojanCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryLojanCdLocalEstoquePadrao: TIntegerField
      FieldName = 'nCdLocalEstoquePadrao'
    end
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoPedido'
      
        'INNER JOIN UsuarioTipoPedido UTP ON UTP.nCdTipoPedido = TipoPedi' +
        'do.nCdTipoPedido'
      'WHERE TipoPedido.nCdTipoPedido = :nPK'
      'AND nCdUsuario = :nCdUsuario'
      'AND nCdTabTipoPedido = 2')
    Left = 128
    Top = 352
    object qryTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryTipoPedidonCdTabTipoPedido: TIntegerField
      FieldName = 'nCdTabTipoPedido'
    end
    object qryTipoPedidocFlgCalcImposto: TIntegerField
      FieldName = 'cFlgCalcImposto'
    end
    object qryTipoPedidocFlgComporRanking: TIntegerField
      FieldName = 'cFlgComporRanking'
    end
    object qryTipoPedidocFlgVenda: TIntegerField
      FieldName = 'cFlgVenda'
    end
    object qryTipoPedidocGerarFinanc: TIntegerField
      FieldName = 'cGerarFinanc'
    end
    object qryTipoPedidocExigeAutor: TIntegerField
      FieldName = 'cExigeAutor'
    end
    object qryTipoPedidonCdEspTitPrev: TIntegerField
      FieldName = 'nCdEspTitPrev'
    end
    object qryTipoPedidonCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryTipoPedidonCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryTipoPedidocFlgAtuPreco: TIntegerField
      FieldName = 'cFlgAtuPreco'
    end
    object qryTipoPedidocFlgItemEstoque: TIntegerField
      FieldName = 'cFlgItemEstoque'
    end
    object qryTipoPedidocFlgItemAD: TIntegerField
      FieldName = 'cFlgItemAD'
    end
    object qryTipoPedidonCdTipoPedido_1: TIntegerField
      FieldName = 'nCdTipoPedido_1'
    end
    object qryTipoPedidonCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryTipoPedidocFlgExibeAcomp: TIntegerField
      FieldName = 'cFlgExibeAcomp'
    end
    object qryTipoPedidocFlgCompra: TIntegerField
      FieldName = 'cFlgCompra'
    end
    object qryTipoPedidocFlgItemFormula: TIntegerField
      FieldName = 'cFlgItemFormula'
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryTerceiroAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTipoPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cCNPJCPF'
      '      ,cNmTerceiro'
      '      ,nCdTerceiroPagador'
      '      ,nCdTerceiroTransp'
      '      ,nCdIncoterms'
      '      ,nCdCondPagto'
      '      ,nPercDesconto'
      '      ,nPercAcrescimo'
      '  FROM Terceiro'
      ' WHERE nCdStatus = 1'
      '   AND nCdTerceiro = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM TerceiroTipoTerceiro TTT'
      
        '                     INNER JOIN TipoPedidoTipoTerceiro TTTP  ON ' +
        'TTTP.nCdTipoTerceiro = TTT.nCdTipoTerceiro'
      
        '                                                            AND ' +
        'TTTp.nCdTipoPedido   = :nCdTipoPedido'
      '               WHERE TTT.nCdTerceiro = Terceiro.nCdTerceiro)')
    Left = 192
    Top = 352
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceironCdTerceiroPagador: TIntegerField
      FieldName = 'nCdTerceiroPagador'
    end
    object qryTerceironCdTerceiroTransp: TIntegerField
      FieldName = 'nCdTerceiroTransp'
    end
    object qryTerceironCdIncoterms: TIntegerField
      FieldName = 'nCdIncoterms'
    end
    object qryTerceironCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryTerceironPercDesconto: TBCDField
      FieldName = 'nPercDesconto'
      Precision = 5
      Size = 2
    end
    object qryTerceironPercAcrescimo: TBCDField
      FieldName = 'nPercAcrescimo'
      Precision = 5
      Size = 2
    end
  end
  object qryTerceiroColab: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro'
      
        'INNER JOIN Usuario ON Usuario.nCdTerceiroResponsavel = Terceiro.' +
        'nCdTerceiro'
      'WHERE Terceiro.nCdTerceiro = :nPK'
      'AND Usuario.cFlgComprador = 1'
      'AND Usuario.nCdStatus = 1'
      '')
    Left = 256
    Top = 352
    object qryTerceiroColabnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroColabcCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceiroColabcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryTerceiroTransp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro'
      
        '     INNER JOIN TerceiroTipoTerceiro ON TerceiroTipoTerceiro.nCd' +
        'Terceiro = Terceiro.nCdTerceiro'
      '                                    AND nCdTipoTerceiro = 3'
      'WHERE nCdStatus = 1'
      'AND Terceiro.nCdTerceiro = :nPK'
      '')
    Left = 648
    Top = 352
    object qryTerceiroTranspnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroTranspcCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceiroTranspcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryIncoterms: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM INCOTERMS'
      'WHERE nCdIncoterms = :nPK')
    Left = 584
    Top = 352
    object qryIncotermsnCdIncoterms: TIntegerField
      FieldName = 'nCdIncoterms'
    end
    object qryIncotermscSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 3
    end
    object qryIncotermscNmIncoterms: TStringField
      FieldName = 'cNmIncoterms'
      Size = 50
    end
    object qryIncotermscFlgPagador: TStringField
      FieldName = 'cFlgPagador'
      FixedChar = True
      Size = 1
    end
  end
  object qryCondPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CondPagto'
      'WHERE nCdCondPagto = :nPK'
      'AND cFlgPDV = 0')
    Left = 224
    Top = 352
    object qryCondPagtonCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryCondPagtocNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
  end
  object qryMoeda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Moeda'
      'WHERE nCdMoeda = :nPK ')
    Left = 88
    Top = 488
    object qryMoedanCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMoedacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 10
    end
    object qryMoedacNmMoeda: TStringField
      FieldName = 'cNmMoeda'
      Size = 50
    end
  end
  object qryEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdLoja'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM Estoque'
      'WHERE nCdEmpresa = :nCdEmpresa'
      'AND (nCdLoja = :nCdLoja OR :nCdLoja = 0)'
      'AND nCdEstoque = :nPK')
    Left = 808
    Top = 352
    object qryEstoquenCdEstoque: TAutoIncField
      FieldName = 'nCdEstoque'
      ReadOnly = True
    end
    object qryEstoquenCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEstoquenCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryEstoquenCdTipoEstoque: TIntegerField
      FieldName = 'nCdTipoEstoque'
    end
    object qryEstoquecNmEstoque: TStringField
      FieldName = 'cNmEstoque'
      Size = 50
    end
    object qryEstoquenCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object qryStatusPed: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabStatusPed'
      'WHERE nCdTabStatusPed = :nPK')
    Left = 552
    Top = 352
    object qryStatusPednCdTabStatusPed: TIntegerField
      FieldName = 'nCdTabStatusPed'
    end
    object qryStatusPedcNmTabStatusPed: TStringField
      FieldName = 'cNmTabStatusPed'
      Size = 50
    end
  end
  object qryTerceiroPagador: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdStatus = 1'
      'AND nCdTerceiro = :nPK')
    Left = 680
    Top = 352
    object qryTerceiroPagadornCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroPagadorcCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceiroPagadorcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 64
    Top = 384
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 96
    Top = 384
  end
  object dsTipoPedido: TDataSource
    DataSet = qryTipoPedido
    Left = 127
    Top = 384
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 191
    Top = 384
  end
  object dsStatusPed: TDataSource
    DataSet = qryStatusPed
    Left = 551
    Top = 384
  end
  object dsTerceiroTransp: TDataSource
    DataSet = qryTerceiroTransp
    Left = 648
    Top = 384
  end
  object dsEstoque: TDataSource
    DataSet = qryEstoque
    Left = 816
    Top = 384
  end
  object dsTerceiroPagador: TDataSource
    DataSet = qryTerceiroPagador
    Left = 679
    Top = 384
  end
  object dsCondPagto: TDataSource
    DataSet = qryCondPagto
    Left = 223
    Top = 384
  end
  object dsIncoterms: TDataSource
    DataSet = qryIncoterms
    Left = 583
    Top = 384
  end
  object qryUsuarioTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoPedido'
      'FROM UsuarioTipoPedido'
      'WHERE nCdUsuario = :nCdUsuario')
    Left = 120
    Top = 488
    object qryUsuarioTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
  end
  object qryUsuarioLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      'FROM UsuarioLoja'
      'WHERE nCdUsuario = :nCdUsuario')
    Left = 120
    Top = 520
    object qryUsuarioLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
  end
  object dsTerceiroColab: TDataSource
    DataSet = qryTerceiroColab
    Left = 256
    Top = 384
  end
  object qryItemEstoque: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemEstoqueBeforePost
    AfterPost = qryItemEstoqueAfterPost
    BeforeDelete = qryItemEstoqueBeforeDelete
    AfterDelete = qryItemEstoqueAfterDelete
    AfterScroll = qryItemEstoqueAfterScroll
    OnCalcFields = qryItemEstoqueCalcFields
    EnableBCD = False
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmItem'
      '      ,nQtdePed'
      '      ,nQtdeExpRec'
      '      ,nQtdeCanc'
      '      ,nValUnitario'
      '      ,nValDesconto'
      '      ,nValAcrescimo'
      '      ,nValSugVenda'
      '      ,nValTotalItem'
      '      ,nPercIPI'
      '      ,nValIPI'
      '      ,nValCustoUnit'
      '      ,nCdPedido'
      '      ,nCdTipoItemPed'
      '      ,nCdItemPedido'
      '      ,nPercICMSSub'
      '      ,nValICMSSub'
      '      ,cSiglaUnidadeMedida'
      '      ,nPercDescontoItem'
      ',nValUnitarioEsp'
      ',dDtEntregaIni'
      ',dDtEntregaFim'
      ',nQtdePrev'
      ',nFatorConvUnidadeEstoque'
      '   FROM ItemPedido WITH(INDEX=IN02_ItemPedido)'
      ' WHERE nCdPedido = :nPK'
      '   AND (   (nCdTipoItemPed = 1)'
      '        OR (nCdTipoItemPed = 2))')
    Left = 288
    Top = 352
    object qryItemEstoquenCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryItemEstoquecNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 50
    end
    object qryItemEstoquenQtdePed: TBCDField
      DisplayLabel = 'Quantidades|Pedido'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemEstoquenQtdeExpRec: TBCDField
      DisplayLabel = 'Quantidades|Receb.'
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object qryItemEstoquenQtdeCanc: TBCDField
      DisplayLabel = 'Quantidades|Cancel.'
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
    object qryItemEstoquenPercDescontoItem: TBCDField
      DisplayLabel = 'Desconto|%'
      FieldName = 'nPercDescontoItem'
      Precision = 12
      Size = 2
    end
    object qryItemEstoquenValSugVenda: TBCDField
      DisplayLabel = 'Valores|Sug.Venda'
      FieldName = 'nValSugVenda'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemEstoquenValTotalItem: TBCDField
      DisplayLabel = 'Valores|Total L'#237'q'
      FieldName = 'nValTotalItem'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemEstoquenPercIPI: TBCDField
      DisplayLabel = 'Impostos|% IPI'
      FieldName = 'nPercIPI'
      DisplayFormat = '#,##0.00'
      Precision = 5
      Size = 2
    end
    object qryItemEstoquenValIPI: TBCDField
      DisplayLabel = 'Impostos|Valor IPI'
      FieldName = 'nValIPI'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryItemEstoquenCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryItemEstoquenCdTipoItemPed: TIntegerField
      FieldName = 'nCdTipoItemPed'
    end
    object qryItemEstoquecCalc: TStringField
      FieldKind = fkCalculated
      FieldName = 'cCalc'
      Size = 50
      Calculated = True
    end
    object qryItemEstoquenCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
    end
    object qryItemEstoquenPercICMSSub: TBCDField
      DisplayLabel = 'Impostos|% ICMS Sub.'
      FieldName = 'nPercICMSSub'
      Precision = 12
      Size = 2
    end
    object qryItemEstoquenValICMSSub: TBCDField
      FieldName = 'nValICMSSub'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemEstoquecSiglaUnidadeMedida: TStringField
      DisplayLabel = 'Quantidades|UM'
      DisplayWidth = 3
      FieldName = 'cSiglaUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryItemEstoquenValUnitario: TFloatField
      DisplayLabel = 'Valor|Unit'#225'rio'
      FieldName = 'nValUnitario'
    end
    object qryItemEstoquenValDesconto: TFloatField
      DisplayLabel = 'Desconto|Valor'
      FieldName = 'nValDesconto'
    end
    object qryItemEstoquenValAcrescimo: TFloatField
      DisplayLabel = 'Valores|Acr'#233'scimo'
      FieldName = 'nValAcrescimo'
      OnValidate = qryItemEstoquenValAcrescimoValidate
    end
    object qryItemEstoquenValCustoUnit: TFloatField
      DisplayLabel = 'Valores|Custo Final'
      FieldName = 'nValCustoUnit'
    end
    object qryItemEstoquenValUnitarioEsp: TFloatField
      FieldName = 'nValUnitarioEsp'
    end
    object qryItemEstoquedDtEntregaIni: TDateTimeField
      DisplayLabel = 'Previs'#227'o Entrega|Inicial'
      FieldName = 'dDtEntregaIni'
    end
    object qryItemEstoquedDtEntregaFim: TDateTimeField
      DisplayLabel = 'Previs'#227'o Entrega|Final'
      FieldName = 'dDtEntregaFim'
    end
    object qryItemEstoquenQtdePrev: TFloatField
      DisplayLabel = 'Quantidade|Prevista'
      FieldName = 'nQtdePrev'
    end
    object qryItemEstoquenFatorConvUnidadeEstoque: TFloatField
      FieldName = 'nFatorConvUnidadeEstoque'
    end
    object qryItemEstoquecReferencia: TStringField
      FieldKind = fkCalculated
      FieldName = 'cReferencia'
      Size = 15
      Calculated = True
    end
    object qryItemEstoquenValTotalProduto: TFloatField
      FieldKind = fkCalculated
      FieldName = 'nValTotalProduto'
      Calculated = True
    end
  end
  object dsItemEstoque: TDataSource
    DataSet = qryItemEstoque
    Left = 292
    Top = 384
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTipoPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,CASE WHEN nFatorCompra = 1 THEN cNmProduto'
      
        '            ELSE cNmProduto + '#39' - '#39' + cUnidadeMedidaCompra + '#39' c' +
        '/ '#39' + Convert(VARCHAR(10),Convert(int,nFatorCompra)) + '#39' '#39' + cUn' +
        'idadeMedida'
      '       END as cNmProduto'
      '      ,nCdGrade'
      
        '      ,CASE WHEN cUnidadeMedidaCompra IS NULL THEN cUnidadeMedid' +
        'a'
      '            ELSE cUnidadeMedidaCompra'
      '       END cUnidadeMedida'
      '      ,nQtdeMinimaCompra'
      '      ,nValVenda'
      '      ,cFlgProdVenda'
      '      ,nFatorCompra'
      '      ,cReferencia'
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK'
      '   AND Produto.cFlgProdCompra = 1'
      '   AND (   (nCdTabTipoProduto = 2 AND nCdGrade > 0)'
      '        OR (nCdGrade IS NULL))'
      '   AND EXISTS(SELECT 1'
      '                FROM GrupoProdutoTipoPedido GPTP'
      '               WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto'
      '                 AND GPTP.nCdTipoPedido   = :nCdTipoPedido)'
      '   AND (NOT EXISTS(SELECT 1'
      '                     FROM Produto Produto2'
      
        '                    WHERE Produto2.nCdProdutoPai = Produto.nCdPr' +
        'oduto) OR (Produto.nCdGrade IS NOT NULL))          ')
    Left = 152
    Top = 488
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 100
    end
    object qryProdutonCdGrade: TIntegerField
      FieldName = 'nCdGrade'
    end
    object qryProdutocUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      ReadOnly = True
      FixedChar = True
      Size = 3
    end
    object qryProdutonQtdeMinimaCompra: TBCDField
      FieldName = 'nQtdeMinimaCompra'
      Precision = 12
      Size = 2
    end
    object qryProdutonValVenda: TBCDField
      FieldName = 'nValVenda'
      Precision = 12
      Size = 2
    end
    object qryProdutocFlgProdVenda: TIntegerField
      FieldName = 'cFlgProdVenda'
    end
    object qryProdutonFatorCompra: TBCDField
      FieldName = 'nFatorCompra'
      Precision = 12
      Size = 2
    end
    object qryProdutocReferencia: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
  end
  object usp_Grade: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_PREPARA_GRADE_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cSQLRetorno'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 1000
        Value = Null
      end
      item
        Name = '@nCdItemPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 304
    Top = 480
  end
  object dsGrade: TDataSource
    DataSet = qryTemp
    Left = 322
    Top = 384
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    AfterPost = qryTempAfterPost
    AfterCancel = qryTempAfterCancel
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM ##Temp_Grade_Qtde')
    Left = 320
    Top = 352
  end
  object usp_Gera_SubItem: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_SUBITEM_GRADE_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdItemPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 432
    Top = 448
  end
  object cmdExcluiSubItem: TADOCommand
    CommandText = 
      'DECLARE @nCdItemPedidoPai int'#13#10'       ,@nCdItemPedido    int'#13#10#13#10 +
      'Set @nCdItemPedidoPai = :nPK'#13#10#13#10'--'#13#10'-- Exclui a pr'#233'-distribui'#231#227'o' +
      ' do item do pedido'#13#10'--'#13#10'DELETE '#13#10'  FROM ItemPedidoPreDistribuido' +
      #13#10' WHERE EXISTS (SELECT 1 '#13#10'                 FROM ItemPedido IP ' +
      #13#10'                WHERE IP.nCdItemPedido = ItemPedidoPreDistribu' +
      'ido.nCdItemPedido'#13#10'                  AND IP.nCdItemPedidoPai    ' +
      ' = @nCdItemPedidoPai)'#13#10'--'#13#10'-- Exclui o empenho da pr'#233'-distribuic' +
      'ao'#13#10'--'#13#10'DELETE '#13#10'  FROM EstoqueEmpenhado'#13#10' WHERE EXISTS (SELECT ' +
      '1 '#13#10'                 FROM ItemPedido IP '#13#10'                WHERE ' +
      'IP.nCdItemPedido = EstoqueEmpenhado.nCdItemPedido'#13#10'             ' +
      '     AND IP.nCdItemPedidoPai     = @nCdItemPedidoPai)'#13#10#13#10'--'#13#10'-- ' +
      'Exclui o centro de custo do item do pedido'#13#10'--'#13#10'DELETE '#13#10'  FROM ' +
      'CentroCustoItemPedido'#13#10' WHERE nCdItemPedido = @nCdItemPedidoPai'#13 +
      #10#13#10'DECLARE curSubItens CURSOR'#13#10'    FOR SELECT nCdItemPedido'#13#10'   ' +
      '       FROM ItemPedido'#13#10'         WHERE nCdItemPedidoPai = @nCdIt' +
      'emPedidoPai'#13#10#13#10'OPEN curSubItens'#13#10#13#10'FETCH NEXT '#13#10' FROM curSubIten' +
      's'#13#10' INTO @nCdItemPedido'#13#10#13#10'WHILE (@@FETCH_STATUS = 0)'#13#10'BEGIN'#13#10#9'-' +
      '-'#13#10#9'-- Exclui o centro de custo dos subitems do item do pedido'#13#10 +
      #9'--'#13#10#9'DELETE '#13#10#9'  FROM CentroCustoItemPedido'#13#10#9' WHERE nCdItemPed' +
      'ido = @nCdItemPedido'#13#10'    '#13#10#13#10#9'FETCH NEXT '#13#10#9' FROM curSubItens'#13#10 +
      #9' INTO @nCdItemPedido'#13#10'END'#13#10#13#10'CLOSE curSubItens'#13#10'DEALLOCATE curS' +
      'ubItens'#13#10#13#10'DELETE '#13#10'  FROM ItemPedido'#13#10' WHERE nCdItemPedidoPai =' +
      ' @nCdItemPedidoPai'#13#10'   AND nCdTipoItemPed   IN (4,7,8)'
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 504
    Top = 480
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    EnableBCD = False
    Parameters = <>
    Left = 184
    Top = 488
  end
  object qryPrazoPedido: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryPrazoPedidoBeforePost
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM PrazoPedido'
      'WHERE nCdPedido = :nPK')
    Left = 775
    Top = 351
    object qryPrazoPedidonCdPrazoPedido: TAutoIncField
      DisplayLabel = 'Parcela|C'#243'd'
      FieldName = 'nCdPrazoPedido'
    end
    object qryPrazoPedidonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryPrazoPedidoiDias: TIntegerField
      DisplayLabel = 'Parcela|Dias Vencimento'
      FieldName = 'iDias'
    end
    object qryPrazoPedidodVencto: TDateTimeField
      DisplayLabel = 'Parcela|Data Vencimento'
      FieldName = 'dVencto'
    end
    object qryPrazoPedidonValPagto: TBCDField
      DisplayLabel = 'Parcela|Valor Parcela'
      FieldName = 'nValPagto'
      Precision = 12
      Size = 2
    end
  end
  object dsPrazoPedido: TDataSource
    DataSet = qryPrazoPedido
    Left = 780
    Top = 384
  end
  object usp_Sugere_Parcela: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_SUGERE_PARCELA_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 368
    Top = 480
  end
  object qryItemAD: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemADBeforePost
    AfterPost = qryItemADAfterPost
    BeforeDelete = qryItemADBeforeDelete
    AfterDelete = qryItemADAfterDelete
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cCdProduto'
      '      ,cNmItem'
      '      ,nQtdePed'
      '      ,nQtdeExpRec'
      '      ,nQtdeCanc'
      '      ,nValTotalItem'
      '      ,nValUnitario'
      '     ,nValCustoUnit'
      '      ,nCdPedido'
      '      ,nCdTipoItemPed'
      '      ,nCdItemPedido'
      '      ,cSiglaUnidadeMedida'
      '      ,nPercIPI'
      '      ,nPercICMSSub'
      ',nValIPI'
      ',nValICMSSub'
      '  FROM ItemPedido'
      ' WHERE nCdPedido      = :nPK'
      '   AND nCdTipoItemPed = 5')
    Left = 484
    Top = 352
    object qryItemADcCdProduto: TStringField
      DisplayLabel = 'Produto|C'#243'd.'
      FieldName = 'cCdProduto'
      Size = 15
    end
    object qryItemADcNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemADcSiglaUnidadeMedida: TStringField
      DisplayLabel = 'Produto|UM'
      FieldName = 'cSiglaUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryItemADnQtdePed: TBCDField
      DisplayLabel = 'Quantidade|Pedida'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemADnQtdeExpRec: TBCDField
      DisplayLabel = 'Quantidade|Atendida'
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object qryItemADnQtdeCanc: TBCDField
      DisplayLabel = 'Quantidade|Cancel.'
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
    object qryItemADnValTotalItem: TBCDField
      DisplayLabel = 'Valor|Total L'#237'q.'
      FieldName = 'nValTotalItem'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryItemADnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryItemADnCdTipoItemPed: TIntegerField
      FieldName = 'nCdTipoItemPed'
    end
    object qryItemADnCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
    end
    object qryItemADnPercIPI: TBCDField
      DisplayLabel = 'Impostos|% IPI'
      FieldName = 'nPercIPI'
      Precision = 5
      Size = 2
    end
    object qryItemADnValIPI: TBCDField
      DisplayLabel = 'Impostos|Valor IPI'
      FieldName = 'nValIPI'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryItemADnPercICMSSub: TBCDField
      DisplayLabel = 'Impostos|% ICMS Sub.'
      FieldName = 'nPercICMSSub'
      Precision = 12
      Size = 2
    end
    object qryItemADnValUnitario: TBCDField
      DisplayLabel = 'Valor|Unit'#225'rio'
      FieldName = 'nValUnitario'
      Precision = 12
    end
    object qryItemADnValICMSSub: TBCDField
      FieldName = 'nValICMSSub'
      Precision = 12
      Size = 2
    end
    object qryItemADnValCustoUnit: TBCDField
      DisplayLabel = 'Custo Unit'#225'rio|Final'
      FieldName = 'nValCustoUnit'
      Precision = 14
      Size = 6
    end
  end
  object dsItemAD: TDataSource
    DataSet = qryItemAD
    Left = 484
    Top = 384
  end
  object usp_Finaliza: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_FINALIZA_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 304
    Top = 448
  end
  object qryDadoAutorz: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT '#39'em : '#39' + Convert(VARCHAR(20),dDtAutor,103) + '#39' por : '#39' +' +
        ' cNmUsuario  as cDadoAutoriz'
      '  FROM Pedido'
      
        '       INNER JOIN Usuario ON Usuario.nCdUsuario = Pedido.nCdUsua' +
        'rioAutor'
      ' WHERE nCdPedido = :nPK')
    Left = 615
    Top = 351
    object qryDadoAutorzcDadoAutoriz: TStringField
      FieldName = 'cDadoAutoriz'
      ReadOnly = True
      Size = 82
    end
  end
  object dsDadoAutorz: TDataSource
    DataSet = qryDadoAutorz
    Left = 616
    Top = 384
  end
  object qryFollow: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT dDtFollowUp'
      '      ,cNmUsuario'
      '      ,cOcorrenciaResum'
      '      ,dDtProxAcao'
      ',cOcorrencia'
      '  FROM FollowUp'
      
        '       INNER JOIN Usuario ON Usuario.nCdUsuario = FollowUp.nCdUs' +
        'uario'
      ' WHERE nCdPedido = :nPK'
      'ORDER BY dDtFollowUp DESC')
    Left = 522
    Top = 352
    object qryFollowdDtFollowUp: TDateTimeField
      DisplayLabel = 'Ocorr'#234'ncia|Data'
      FieldName = 'dDtFollowUp'
      ReadOnly = True
    end
    object qryFollowcNmUsuario: TStringField
      DisplayLabel = 'Ocorr'#234'ncia|Usu'#225'rio'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryFollowcOcorrenciaResum: TStringField
      DisplayLabel = 'Ocorr'#234'ncia|Descri'#231#227'o Resumida'
      FieldName = 'cOcorrenciaResum'
      Size = 50
    end
    object qryFollowdDtProxAcao: TDateTimeField
      DisplayLabel = 'Ocorr'#234'ncia|Data Pr'#243'x. A'#231#227'o'
      FieldName = 'dDtProxAcao'
    end
    object qryFollowcOcorrencia: TMemoField
      FieldName = 'cOcorrencia'
      BlobType = ftMemo
    end
  end
  object dsFollow: TDataSource
    DataSet = qryFollow
    Left = 520
    Top = 384
  end
  object qryItemFormula: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemFormulaBeforePost
    AfterPost = qryItemFormulaAfterPost
    BeforeDelete = qryItemFormulaBeforeDelete
    AfterDelete = qryItemFormulaAfterDelete
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmItem'
      '      ,nQtdePed'
      '      ,nQtdeExpRec'
      '      ,nQtdeCanc'
      '      ,nValTotalItem'
      '      ,nValCustoUnit'
      '      ,nCdPedido'
      '      ,nCdTipoItemPed'
      '      ,nCdItemPedido'
      ',cSiglaUnidadeMedida'
      '  FROM ItemPedido'
      ' WHERE nCdPedido      = :nPK'
      '   AND nCdTipoItemPed = 6'
      '')
    Left = 418
    Top = 352
    object qryItemFormulacNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemFormulanQtdePed: TBCDField
      DisplayLabel = 'Quantidade|Pedida'
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemFormulanQtdeExpRec: TBCDField
      DisplayLabel = 'Quantidade|Atendida'
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object qryItemFormulanQtdeCanc: TBCDField
      DisplayLabel = 'Quantidade|Cancel.'
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
    object qryItemFormulanValTotalItem: TBCDField
      DisplayLabel = 'Valores|Total'
      FieldName = 'nValTotalItem'
      Precision = 12
      Size = 2
    end
    object qryItemFormulanValCustoUnit: TBCDField
      DisplayLabel = 'Valores|Unit'#225'rio'
      FieldName = 'nValCustoUnit'
      Precision = 12
      Size = 2
    end
    object qryItemFormulanCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryItemFormulanCdTipoItemPed: TIntegerField
      FieldName = 'nCdTipoItemPed'
    end
    object qryItemFormulanCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
    end
    object qryItemFormulacSiglaUnidadeMedida: TStringField
      DisplayWidth = 3
      FieldName = 'cSiglaUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryItemFormulanCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
  end
  object dsItemFormula: TDataSource
    DataSet = qryItemFormula
    Left = 418
    Top = 384
  end
  object qryProdutoFormulado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto,cNmProduto'
      'FROM Produto'
      'WHERE nCdTabTipoProduto = 4'
      'AND nCdProduto = :nPK')
    Left = 218
    Top = 488
    object qryProdutoFormuladonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutoFormuladocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object usp_gera_item_embalagem: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_SUBITEM_GRADE_EMBALAGEM;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdItemPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 448
  end
  object qryItemLocalEntrega: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemLocalEntregaBeforePost
    OnCalcFields = qryItemLocalEntregaCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ItemPedido'
      'WHERE nCdPedido = :nPK'
      'AND nCdTipoItemPed IN (1,2,9)')
    Left = 384
    Top = 352
    object qryItemLocalEntreganCdItemPedido: TAutoIncField
      DisplayLabel = 'Item|ID'
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryItemLocalEntreganCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryItemLocalEntreganCdItemPedidoPai: TIntegerField
      FieldName = 'nCdItemPedidoPai'
    end
    object qryItemLocalEntreganCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'digo'
      FieldName = 'nCdProduto'
    end
    object qryItemLocalEntregacCdProduto: TStringField
      FieldName = 'cCdProduto'
      Size = 15
    end
    object qryItemLocalEntreganCdTipoItemPed: TIntegerField
      FieldName = 'nCdTipoItemPed'
    end
    object qryItemLocalEntreganCdEstoqueMov: TIntegerField
      DisplayLabel = 'Estoque de Entrega|C'#243'd'
      FieldName = 'nCdEstoqueMov'
    end
    object qryItemLocalEntregacNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemLocalEntreganQtdePed: TBCDField
      FieldName = 'nQtdePed'
      Precision = 12
    end
    object qryItemLocalEntreganQtdeExpRec: TBCDField
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object qryItemLocalEntreganQtdeCanc: TBCDField
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
    object qryItemLocalEntreganValUnitario: TBCDField
      FieldName = 'nValUnitario'
      Precision = 12
    end
    object qryItemLocalEntreganPercIPI: TBCDField
      FieldName = 'nPercIPI'
      Precision = 5
      Size = 2
    end
    object qryItemLocalEntreganValIPI: TBCDField
      FieldName = 'nValIPI'
      Precision = 12
    end
    object qryItemLocalEntreganValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
    end
    object qryItemLocalEntreganValCustoUnit: TBCDField
      FieldName = 'nValCustoUnit'
      Precision = 12
      Size = 2
    end
    object qryItemLocalEntreganValSugVenda: TBCDField
      FieldName = 'nValSugVenda'
      Precision = 12
      Size = 2
    end
    object qryItemLocalEntreganValTotalItem: TBCDField
      FieldName = 'nValTotalItem'
      Precision = 12
      Size = 2
    end
    object qryItemLocalEntreganValAcrescimo: TBCDField
      FieldName = 'nValAcrescimo'
      Precision = 12
    end
    object qryItemLocalEntregacSiglaUnidadeMedida: TStringField
      FieldName = 'cSiglaUnidadeMedida'
      FixedChar = True
      Size = 2
    end
    object qryItemLocalEntregaiColuna: TIntegerField
      FieldName = 'iColuna'
    end
    object qryItemLocalEntreganCdTabStatusItemPed: TIntegerField
      FieldName = 'nCdTabStatusItemPed'
    end
    object qryItemLocalEntreganValUnitarioEsp: TBCDField
      FieldName = 'nValUnitarioEsp'
      Precision = 12
      Size = 2
    end
    object qryItemLocalEntregacNmLocalEstoque: TStringField
      DisplayLabel = 'Estoque de Entrega|Local'
      FieldKind = fkCalculated
      FieldName = 'cNmLocalEstoque'
      Size = 50
      Calculated = True
    end
    object qryItemLocalEntreganQtdeLibFat: TBCDField
      FieldName = 'nQtdeLibFat'
      Precision = 12
      Size = 2
    end
  end
  object dsItemLocalEntrega: TDataSource
    DataSet = qryItemLocalEntrega
    Left = 386
    Top = 384
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdproduto'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque'
      '      ,cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE EXISTS(SELECT 1 '
      '                FROM GrupoProdutoLocalEstoque'
      
        '               WHERE GrupoProdutoLocalEstoque.nCdLocalEstoque = ' +
        'LocalEstoque.nCdLocalEstoque'
      
        '                 AND GrupoProdutoLocalEstoque.nCdGrupoProduto = ' +
        '(SELECT nCdGrupoProduto'
      
        '                                                                ' +
        '   FROM Produto'
      
        '                                                                ' +
        '        INNER JOIN Departamento ON Departamento.nCdDepartamento ' +
        '= Produto.nCdDepartamento'
      
        '                                                                ' +
        '  WHERE nCdProduto = :nCdproduto))'
      '   AND nCdEmpresa      = :nCdEmpresa'
      '   AND nCdLocalEstoque = :nPK')
    Left = 218
    Top = 520
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object qryItemCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdItemPedido'
      
        '      ,Convert(VARCHAR,nCdItemPedido) + '#39'          '#39' + IsNull(RI' +
        'GHT(REPLICATE('#39'0'#39',10) + Convert(VARCHAR,nCdProduto),10) + '#39' - '#39',' +
        #39#39') + cNmItem as cNmItem'
      '  FROM ItemPedido'
      ' WHERE nCdTipoItemPed NOT IN (4,7,8)'
      '   AND nCdPedido     = :nPK'
      '   --AND nValTotalItem > 0'
      ' ORDER BY nCdTipoItemPed')
    Left = 746
    Top = 352
    object qryItemCCnCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryItemCCcNmItem: TStringField
      FieldName = 'cNmItem'
      ReadOnly = True
      Size = 203
    end
  end
  object dsItemCC: TDataSource
    DataSet = qryItemCC
    Left = 744
    Top = 384
  end
  object qryCCItemPedido: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCCItemPedidoBeforePost
    OnCalcFields = qryCCItemPedidoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM CentroCustoItemPedido'
      ' WHERE nCdItemPedido = :nPK')
    Left = 712
    Top = 352
    object qryCCItemPedidonCdItemPedido: TIntegerField
      FieldName = 'nCdItemPedido'
    end
    object qryCCItemPedidonCdCC: TIntegerField
      DisplayLabel = 'Centro de Custo|C'#243'd'
      FieldName = 'nCdCC'
    end
    object qryCCItemPedidocNmCC: TStringField
      DisplayLabel = 'Centro de Custo|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCC'
      Size = 50
      Calculated = True
    end
    object qryCCItemPedidonPercent: TBCDField
      DisplayLabel = '% Particip.'
      FieldName = 'nPercent'
      Precision = 12
      Size = 2
    end
    object qryCCItemPedidonCdCentroCustoItemPedido: TIntegerField
      FieldName = 'nCdCentroCustoItemPedido'
    end
  end
  object dsCCItemPedido: TDataSource
    DataSet = qryCCItemPedido
    Left = 714
    Top = 384
  end
  object qryCentroCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdTipoPedido'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM CentroCusto'
      ' WHERE iNivel    = 3'
      '   AND cFlgLanc  = 1'
      '   AND nCdStatus = 1'
      '   AND nCdCC     = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM CentroCustoTipoPedido CCTP'
      '               WHERE CCTP.nCdCC         = CentroCusto.nCdCC'
      '                 AND CCTP.nCdTipoPedido = :nCdTipoPedido)')
    Left = 250
    Top = 520
    object qryCentroCustonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCentroCustocNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
    object qryCentroCustonCdCC1: TIntegerField
      FieldName = 'nCdCC1'
    end
    object qryCentroCustonCdCC2: TIntegerField
      FieldName = 'nCdCC2'
    end
    object qryCentroCustocCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object qryCentroCustocCdHie: TStringField
      FieldName = 'cCdHie'
      FixedChar = True
      Size = 4
    end
    object qryCentroCustoiNivel: TSmallintField
      FieldName = 'iNivel'
    end
    object qryCentroCustocFlgLanc: TIntegerField
      FieldName = 'cFlgLanc'
    end
    object qryCentroCustonCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object usp_copia_cc: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_COPIA_CC_ITEMPEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdItemPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 364
    Top = 448
  end
  object qryTerceiroRepres: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE EXISTS(SELECT 1'
      '                FROM TerceiroTipoTerceiro TTT'
      '               WHERE TTT.nCdTerceiro     = Terceiro.nCdTerceiro'
      '                 AND TTT.nCdTipoTerceiro = 4)'
      '   AND nCdStatus = 1'
      '   AND nCdTerceiro = :nPK')
    Left = 450
    Top = 352
    object qryTerceiroRepresnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroReprescNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsTerceiroRepres: TDataSource
    DataSet = qryTerceiroRepres
    Left = 450
    Top = 384
  end
  object PopupMenu1: TPopupMenu
    Left = 468
    Top = 448
    object ExibirRecebimentosdoItem1: TMenuItem
      Caption = 'Exibir Recebimentos do Item'
      OnClick = ExibirRecebimentosdoItem1Click
    end
    object btnPreDistribuir: TMenuItem
      Caption = 'Pr'#233'-Distribuir Item'
      OnClick = btnPreDistribuirClick
    end
  end
  object qryLocalEstoqueEntrega: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      ''
      'Set @nCdLoja = :nCdLoja'
      ''
      'IF (@nCdLoja = 0) Set @nCdLoja = NULL'
      ''
      'SELECT nCdLocalEstoque, cNmLocalEstoque'
      '    FROM LocalEstoque'
      'WHERE nCdEmpresa = :nCdEmpresa'
      '   AND nCdLocalEstoque = :nPK'
      '   AND ((@nCdLoja IS NULL) OR (@nCdLoja = nCdLoja))')
    Left = 162
    Top = 352
    object qryLocalEstoqueEntreganCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoqueEntregacNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object dsLocalEstoqueEntrega: TDataSource
    DataSet = qryLocalEstoqueEntrega
    Left = 160
    Top = 384
  end
  object qryTituloAbatPedido: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTituloAbatPedidoBeforePost
    OnCalcFields = qryTituloAbatPedidoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TituloAbatPedido'
      'WHERE nCdPedido = :nPK')
    Left = 350
    Top = 352
    object qryTituloAbatPedidonCdTituloAbatPedido: TAutoIncField
      FieldName = 'nCdTituloAbatPedido'
    end
    object qryTituloAbatPedidonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryTituloAbatPedidonCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulos Abatidos|C'#243'd'
      FieldName = 'nCdTitulo'
    end
    object qryTituloAbatPedidonPercAbat: TBCDField
      DisplayLabel = 'T'#237'tulos Abatidos|% Desconto'
      FieldName = 'nPercAbat'
      Precision = 12
      Size = 2
    end
    object qryTituloAbatPedidocNmEspTit: TStringField
      DisplayLabel = 'T'#237'tulos Abatidos|Esp'#233'cie do T'#237'tulo'
      FieldKind = fkCalculated
      FieldName = 'cNmEspTit'
      Size = 50
      Calculated = True
    end
    object qryTituloAbatPedidocObsTit: TStringField
      DisplayLabel = 'T'#237'tulos Abatidos|Observa'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cObsTit'
      Size = 50
      Calculated = True
    end
    object qryTituloAbatPedidodDtVenc: TDateField
      DisplayLabel = 'T'#237'tulos Abatidos|Data Venc.'
      FieldKind = fkCalculated
      FieldName = 'dDtVenc'
      Calculated = True
    end
  end
  object dsTituloAbatPedido: TDataSource
    DataSet = qryTituloAbatPedido
    Left = 351
    Top = 385
  end
  object qryTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT dDtVenc'
      '      ,cObsTit'
      '      ,cNmEspTit'
      '      ,dDtBloqTit'
      '      ,dDtCancel'
      '      ,nSaldoTit'
      '  FROM Titulo'
      '       INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEspTit'
      ' WHERE Titulo.nCdTitulo   = :nPK'
      '   AND Titulo.cSenso      = '#39'C'#39)
    Left = 286
    Top = 488
    object qryTitulodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTitulocObsTit: TMemoField
      FieldName = 'cObsTit'
      BlobType = ftMemo
    end
    object qryTitulocNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTitulodDtBloqTit: TDateTimeField
      FieldName = 'dDtBloqTit'
    end
    object qryTitulodDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryTitulonSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
  end
  object qryProdutoFornecedor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProduto'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdProduto  int'
      '       ,@nCdTerceiro int'
      ''
      'Set @nCdProduto  = :nCdProduto'
      'Set @nCdTerceiro = :nCdTerceiro'
      ''
      'IF EXISTS(SELECT 1'
      '            FROM ProdutoFornecedor'
      '           WHERE nCdProduto  = @nCdProduto'
      '             AND nCdTerceiro = @nCdTerceiro)'
      'BEGIN'
      ''
      #9'SELECT nPrecoUnit'
      #9#9'  ,nCdMoeda'
      #9#9'  ,nPercIPI'
      #9#9'  ,nPercICMSSub'
      #9#9'  ,nPrecoUnitEsp'
      #9'  FROM ProdutoFornecedor'
      #9' WHERE nCdProduto  = @nCdProduto'
      #9'   AND nCdTerceiro = @nCdTerceiro'
      ''
      'END'
      'ELSE'
      'BEGIN'
      ''
      '    SELECT nPrecoUnit'
      '          ,nCdMoeda'
      #9#9'  ,nPercIPI'
      #9#9'  ,nPercICMSSub'
      #9#9'  ,nPrecoUnitEsp'
      #9'  FROM ProdutoFornecedor'
      
        #9' WHERE nCdProduto  = (SELECT nCdProdutoPai FROM Produto WHERE n' +
        'CdProduto = @nCdProduto)'
      #9'   AND nCdTerceiro = @nCdTerceiro'
      ''
      'END')
    Left = 252
    Top = 488
    object qryProdutoFornecedornPrecoUnit: TBCDField
      FieldName = 'nPrecoUnit'
      Precision = 14
      Size = 6
    end
    object qryProdutoFornecedornCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryProdutoFornecedornPercIPI: TBCDField
      FieldName = 'nPercIPI'
      Precision = 12
      Size = 2
    end
    object qryProdutoFornecedornPercICMSSub: TBCDField
      FieldName = 'nPercICMSSub'
      Precision = 12
      Size = 2
    end
    object qryProdutoFornecedornPrecoUnitEsp: TBCDField
      FieldName = 'nPrecoUnitEsp'
      Precision = 14
      Size = 6
    end
  end
  object qryContatoPadrao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro int'
      ''
      'Set @nCdTerceiro = :nPK'
      ''
      'IF EXISTS(SELECT 1 '
      '             FROM ContatoTerceiro'
      '            WHERE nCdTerceiro = @nCdTerceiro'
      '              AND cFlgContatoPadrao = 1)'
      'BEGIN'
      
        #9'SELECT SUBSTRING(cNmContato,1,15) + '#39' - '#39' + cTelefone1 cNmConta' +
        'to'
      #9'  FROM ContatoTerceiro'
      '     WHERE nCdTerceiro       = @nCdTerceiro'
      '       AND cFlgContatoPadrao = 1'
      'END'
      'ELSE'
      'BEGIN'
      ''
      '    IF EXISTS(SELECT cNmContato'
      '                FROM Terceiro'
      '               WHERE nCdTerceiro = @nCdTerceiro'
      '                 AND cNmContato IS NOT NULL)'
      '    BEGIN'
      
        #9#9'SELECT SUBSTRING(cNmContato,1,15) + '#39' - '#39' + cTelefone1 cNmCont' +
        'ato'
      #9#9'  FROM Terceiro'
      #9#9' WHERE nCdTerceiro = @nCdTerceiro'
      '    END'
      '    ELSE'
      '    BEGIN'
      
        #9#9'SELECT TOP 1 SUBSTRING(cNmContato,1,15) + '#39' - '#39' + cTelefone1  ' +
        'cNmContato'
      #9#9'  FROM ContatoTerceiro'
      #9#9' WHERE nCdTerceiro = @nCdTerceiro'
      '     ORDER BY cNmContato'
      '    END'
      ''
      'END')
    Left = 284
    Top = 520
    object qryContatoPadraocNmContato: TStringField
      FieldName = 'cNmContato'
      ReadOnly = True
      Size = 43
    end
  end
  object SP_ENVIA_PEDIDO_TEMPREGISTRO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ENVIA_PEDIDO_TEMPREGISTRO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 480
  end
  object qryInseriItemGrade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdItemPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdProduto'
        DataType = ftInteger
        Size = 1
        Value = 0
      end
      item
        Name = 'iQtde'
        DataType = ftInteger
        Size = 1
        Value = 0
      end>
    SQL.Strings = (
      ''
      'DECLARE @nCdItemPedido      int'
      '       ,@nCdProdutoDetalhe  int'
      '       ,@iQtde              int'
      '       ,@cNmProduto         varchar(150)'
      '       ,@nCdPedido          int'
      '       ,@iID                int'
      ''
      'Set @nCdItemPedido      = :nCdItemPedido'
      'Set @nCdProdutoDetalhe  = :nCdProduto'
      'Set @iQtde              = :iQtde'
      ''
      'SELECT @nCdPedido = nCdPedido'
      '  FROM ItemPedido'
      ' WHERE nCdItemPedido = @nCdItemPedido'
      ' '
      'IF EXISTS(SELECT 1'
      #9#9#9#9#9#9'FROM ItemPedido'
      #9#9#9#9'   WHERE nCdItemPedidoPai = @nCdItemPedido'
      #9#9#9#9#9#9' AND nCdTipoItemPed   = 4'
      #9#9#9#9#9#9' AND nCdProduto       = @nCdProdutoDetalhe)'
      'BEGIN'
      ''
      '    UPDATE ItemPedido'
      #9#9'   SET nQtdePed         = @iQtde'
      #9#9' WHERE nCdItemPedidoPai = @nCdItemPedido'
      #9#9'   AND nCdTipoItemPed   = 4'
      #9#9'   AND nCdProduto       = @nCdProdutoDetalhe'
      ''
      'END'
      'ELSE BEGIN'
      ''
      '    SELECT @cNmProduto = cNmProduto'
      '      FROM Produto'
      '     WHERE nCdProduto = @nCdProdutoDetalhe'
      ''
      '    Set @iID = NULL'
      ''
      '    EXEC usp_ProximoID  '#39'ITEMPEDIDO'#39
      '                       ,@iID OUTPUT'
      ''
      '    INSERT INTO ItemPedido (nCdItemPedido'
      '                           ,nCdPedido'
      #9#9'      '#9#9#9#9#9#9#9'   ,nCdItemPedidoPai'
      #9#9#9#9#9'      '#9#9#9#9'   ,nCdProduto'
      '                           ,cNmItem'
      #9#9#9#9#9#9#9#9'           ,nCdTipoItemPed'
      '       '#9#9#9#9#9#9#9#9#9'   ,nQtdePed)'
      #9#9#9'    '#9#9#9#9#9' VALUES(@iID'
      '                           ,@nCdPedido'
      #9#9#9#9#9'      '#9#9#9#9'   ,@nCdItemPedido'
      #9#9#9#9#9#9#9#9'       '#9'   ,@nCdProdutoDetalhe'
      '                           ,@cNmProduto'
      #9#9#9#9#9#9#9#9#9'         ,4   --SubItem'
      #9#9#9#9#9#9#9#9#9'         ,@iQtde)'
      ''
      'END'
      ''
      '')
    Left = 88
    Top = 519
  end
  object qryTerceiroRepresentante: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nCdTerceiroRepres'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdTerceiro'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiroRepres'
      '  FROM TerceiroRepresentante '
      ' WHERE nCdTerceiroRepres = :nCdTerceiroRepres'
      '   AND nCdTerceiro       = :nCdTerceiro')
    Left = 182
    Top = 519
    object qryTerceiroRepresentantenCdTerceiroRepres: TIntegerField
      FieldName = 'nCdTerceiroRepres'
    end
  end
  object menuOpcao: TPopupMenu
    Left = 468
    Top = 480
    object btImpPedido: TMenuItem
      Caption = 'Imprimir Pedido'
      OnClick = btImpPedidoClick
    end
    object btImpPedidoProgr: TMenuItem
      Caption = 'Imprimir Pedido com Programa'#231#227'o'
      OnClick = btImpPedidoProgrClick
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object btUploadArquivo: TMenuItem
      Caption = 'Upload de Arquivos'
      OnClick = btUploadArquivoClick
    end
    object btDuplicaPedido: TMenuItem
      Caption = 'Duplicar Pedido'
      Visible = False
      OnClick = btDuplicaPedidoClick
    end
  end
  object SP_DUPLICA_PEDIDO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_DUPLICA_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedidoDuplicar'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 430
    Top = 480
  end
  object qryBuscaCD: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLocalEstoque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdLoja'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 1'
      '  FROM CentroDistribuicao CD'
      
        '       INNER JOIN LojaCentroDistribuicao       LJ ON LJ.nCdCentr' +
        'oDistribuicao = CD.nCdCentroDistribuicao'
      
        '       INNER JOIN UsuarioCentroDistribuicao    U  ON U.nCdCentro' +
        'Distribuicao  = CD.nCdCentroDistribuicao'
      
        '       INNER JOIN PrioridadeCentroDistribuicao P  ON P.nCdCentro' +
        'Distribuicao  = CD.nCdCentroDistribuicao'
      ' WHERE CD.nCdLocalEstEmpenhoPadrao = :nCdLocalEstoque'
      '   AND U.nCdUsuario                = :nCdUsuario'
      '   AND CD.nCdLoja                  = :nCdLoja')
    Left = 60
    Top = 488
  end
  object popupDistrib: TPopupMenu
    Left = 500
    Top = 448
    object btVisualizaAlteraDistrib: TMenuItem
      Caption = '&Alterar Pr'#233'-Distribui'#231#227'o'
      OnClick = btVisualizaAlteraDistribClick
    end
    object btExcluirDistrib: TMenuItem
      Caption = '&Excluir Pr'#233'-Distribui'#231#227'o'
      OnClick = btExcluirDistribClick
    end
  end
  object qryItemPedidoPreDistribuido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT I2.nCdItemPedido'
      '      ,I2.cNmItem                        as cNmProdutoPai'
      '      ,I.cNmItem                         as cNmProduto'
      '      ,IP.nQtdeDistrib'
      '      ,I.nQtdePed'
      '      ,I.nQtdePed - (SELECT SUM(IPPD.nQtdeDistrib)'
      '                       FROM ItemPedidoPreDistribuido IPPD'
      
        '                      WHERE IPPD.nCdItemPedido = I.nCdItemPedido' +
        ') as nSaldo'
      '      ,L.nCdLoja'
      '      ,cNmLoja'
      '  FROM ItemPedido I'
      
        '       INNER JOIN ItemPedido               I2 ON I2.nCdItemPedid' +
        'o   = I.nCdItemPedidoPai'
      
        '       INNER JOIN ItemPedidoPreDistribuido IP ON IP.nCdItemPedid' +
        'o   = I.nCdItemPedido'
      
        '       INNER JOIN Loja                     L  ON L.nCdLoja      ' +
        '    = IP.nCdLoja'
      ' WHERE I.nCdPedido = :nPK')
    Left = 28
    Top = 488
    object qryItemPedidoPreDistribuidonCdItemPedido: TIntegerField
      FieldName = 'nCdItemPedido'
    end
    object qryItemPedidoPreDistribuidocNmProdutoPai: TStringField
      FieldName = 'cNmProdutoPai'
      Size = 150
    end
    object qryItemPedidoPreDistribuidocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryItemPedidoPreDistribuidonQtdeDistrib: TIntegerField
      FieldName = 'nQtdeDistrib'
      DisplayFormat = '#,##0.00'
    end
    object qryItemPedidoPreDistribuidocNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryItemPedidoPreDistribuidonCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryItemPedidoPreDistribuidonQtdePed: TBCDField
      FieldName = 'nQtdePed'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object qryItemPedidoPreDistribuidonSaldo: TBCDField
      FieldName = 'nSaldo'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 15
    end
  end
  object dsItemPedidoPreDistribuido: TDataSource
    DataSet = qryItemPedidoPreDistribuido
    Left = 28
    Top = 520
  end
  object qryValidaEmpenho: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ItemPedidoAtendido.nCdItemPedido'
      '      ,ItemPedidoAtendido.nQtdeAtendida'
      '      ,ItemPedidoAtendido.dDtAtendimento'
      '      ,ItemPedidoAtendido.nCdRecebimento'
      '  FROM ItemPedidoAtendido'
      
        '       INNER JOIN ItemPedido ON ItemPedido.nCdItemPedido = ItemP' +
        'edidoAtendido.nCdItemPedido'
      ' WHERE nCdItemPedidoPai = :nPK')
    Left = 60
    Top = 520
    object qryValidaEmpenhonCdItemPedido: TIntegerField
      FieldName = 'nCdItemPedido'
    end
    object qryValidaEmpenhonQtdeAtendida: TBCDField
      FieldName = 'nQtdeAtendida'
      Precision = 12
      Size = 2
    end
    object qryValidaEmpenhodDtAtendimento: TDateTimeField
      FieldName = 'dDtAtendimento'
    end
    object qryValidaEmpenhonCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
  end
end
