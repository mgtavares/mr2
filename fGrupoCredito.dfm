inherited frmGrupoCredito: TfrmGrupoCredito
  Left = 92
  Caption = 'Grupo de Cr'#233'dito'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 22
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 12
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 29
    Top = 86
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 27
    Top = 110
    Width = 155
    Height = 13
    Alignment = taRightJustify
    Caption = 'Permitir Exceder Limite Cr'#233'dito'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 305
    Top = 110
    Width = 145
    Height = 13
    Alignment = taRightJustify
    Caption = 'Toler'#226'ncia Dias Atraso T'#237'tulo'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 8
    Top = 134
    Width = 173
    Height = 13
    Alignment = taRightJustify
    Caption = 'Limite Cheque Risco Concentrado'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 285
    Top = 110
    Width = 9
    Height = 13
    Alignment = taRightJustify
    Caption = '%'
    FocusControl = DBEdit4
  end
  object DBEdit1: TDBEdit [9]
    Tag = 1
    Left = 64
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdGrupoCredito'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [10]
    Left = 64
    Top = 56
    Width = 513
    Height = 19
    DataField = 'cNmGrupoCredito'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [11]
    Left = 64
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
  end
  object DBEdit4: TDBEdit [12]
    Left = 189
    Top = 104
    Width = 89
    Height = 19
    DataField = 'nPercLimiteExcesso'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [13]
    Left = 453
    Top = 104
    Width = 89
    Height = 19
    DataField = 'iDiasToleraAtrasoTit'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [14]
    Left = 189
    Top = 128
    Width = 89
    Height = 19
    DataField = 'nValLimiteChequeRisco'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBCheckBox1: TDBCheckBox [15]
    Left = 48
    Top = 168
    Width = 273
    Height = 17
    Caption = 'Alertar T'#237'tulo em Atraso na Digita'#231#227'o do Pedido'
    DataField = 'cFlgAlertaTitAtrasado'
    DataSource = dsMaster
    TabOrder = 7
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox2: TDBCheckBox [16]
    Left = 48
    Top = 184
    Width = 273
    Height = 17
    Caption = 'Alertar Cheque Devolvido Pendente'
    DataField = 'cFlgAlertaChequeDevolv'
    DataSource = dsMaster
    TabOrder = 8
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox3: TDBCheckBox [17]
    Left = 48
    Top = 200
    Width = 273
    Height = 17
    Caption = 'Alertar Cheque Risco Concentrado Acima Limite'
    DataField = 'cFlgAlertaChequeRisco'
    DataSource = dsMaster
    TabOrder = 9
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox4: TDBCheckBox [18]
    Left = 48
    Top = 216
    Width = 273
    Height = 17
    Caption = 'Alertar Limite Cr'#233'dito Excedido'
    DataField = 'cFlgAlertaLimiteCredito'
    DataSource = dsMaster
    TabOrder = 10
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox8: TDBCheckBox [19]
    Left = 328
    Top = 168
    Width = 321
    Height = 17
    Caption = 'Bloquear Pedido T'#237'tulo em Atraso'
    DataField = 'cFlgBloquearPedTitAtraso'
    DataSource = dsMaster
    TabOrder = 11
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox9: TDBCheckBox [20]
    Left = 328
    Top = 184
    Width = 321
    Height = 17
    Caption = 'Bloquear Pedido Cheque Devolvido Pendente'
    DataField = 'cFlgBloquearPedCheqDevolv'
    DataSource = dsMaster
    TabOrder = 12
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox10: TDBCheckBox [21]
    Left = 328
    Top = 200
    Width = 321
    Height = 17
    Caption = 'Bloquear Pedido Cheque Risco Concentrado Acima Limite'
    DataField = 'cFlgBloquearPedCheqRisco'
    DataSource = dsMaster
    TabOrder = 13
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox11: TDBCheckBox [22]
    Left = 328
    Top = 216
    Width = 321
    Height = 17
    Caption = 'Bloquear Pedido Limite Cr'#233'dito Excedido'
    DataField = 'cFlgBloquearPedLimitCred'
    DataSource = dsMaster
    TabOrder = 14
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBEdit7: TDBEdit [23]
    Tag = 1
    Left = 133
    Top = 80
    Width = 145
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsStatus
    TabOrder = 16
  end
  object GroupBox1: TGroupBox [24]
    Left = 48
    Top = 248
    Width = 449
    Height = 73
    Caption = 'A'#231#227'o no Pedido'
    TabOrder = 15
    object DBCheckBox5: TDBCheckBox
      Left = 8
      Top = 16
      Width = 425
      Height = 17
      Caption = 'Colocar TODOS os pedidos em An'#225'lise do Financeiro'
      DataField = 'cFlgPedidosAnaliseFin'
      DataSource = dsMaster
      TabOrder = 0
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox6: TDBCheckBox
      Left = 8
      Top = 32
      Width = 425
      Height = 17
      Caption = 
        'Autorizar Automaticamente os Pedidos Quando n'#227'o Existir Restri'#231#227 +
        'o'
      DataField = 'cFlgAutorAutoPedSemRestri'
      DataSource = dsMaster
      TabOrder = 1
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox7: TDBCheckBox
      Left = 8
      Top = 48
      Width = 425
      Height = 17
      Caption = 
        'Autorizar Automaticamente TODOS os pedidos, independente das res' +
        'tri'#231#245'es'
      DataField = 'cFlgAutorAutoPedidos'
      DataSource = dsMaster
      TabOrder = 2
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM GrupoCredito'
      ' WHERE nCdGrupoCredito = :nPK')
    Left = 792
    Top = 104
    object qryMasternCdGrupoCredito: TIntegerField
      FieldName = 'nCdGrupoCredito'
    end
    object qryMastercNmGrupoCredito: TStringField
      FieldName = 'cNmGrupoCredito'
      Size = 50
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMasternPercLimiteExcesso: TBCDField
      FieldName = 'nPercLimiteExcesso'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasteriDiasToleraAtrasoTit: TIntegerField
      FieldName = 'iDiasToleraAtrasoTit'
    end
    object qryMasternValLimiteChequeRisco: TBCDField
      FieldName = 'nValLimiteChequeRisco'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgAlertaTitAtrasado: TIntegerField
      FieldName = 'cFlgAlertaTitAtrasado'
    end
    object qryMastercFlgAlertaChequeDevolv: TIntegerField
      FieldName = 'cFlgAlertaChequeDevolv'
    end
    object qryMastercFlgAlertaChequeRisco: TIntegerField
      FieldName = 'cFlgAlertaChequeRisco'
    end
    object qryMastercFlgAlertaLimiteCredito: TIntegerField
      FieldName = 'cFlgAlertaLimiteCredito'
    end
    object qryMastercFlgBloquearPedTitAtraso: TIntegerField
      FieldName = 'cFlgBloquearPedTitAtraso'
    end
    object qryMastercFlgBloquearPedCheqDevolv: TIntegerField
      FieldName = 'cFlgBloquearPedCheqDevolv'
    end
    object qryMastercFlgBloquearPedCheqRisco: TIntegerField
      FieldName = 'cFlgBloquearPedCheqRisco'
    end
    object qryMastercFlgBloquearPedLimitCred: TIntegerField
      FieldName = 'cFlgBloquearPedLimitCred'
    end
    object qryMastercFlgPedidosAnaliseFin: TIntegerField
      FieldName = 'cFlgPedidosAnaliseFin'
    end
    object qryMastercFlgAutorAutoPedSemRestri: TIntegerField
      FieldName = 'cFlgAutorAutoPedSemRestri'
    end
    object qryMastercFlgAutorAutoPedidos: TIntegerField
      FieldName = 'cFlgAutorAutoPedidos'
    end
  end
  inherited dsMaster: TDataSource
    Left = 840
    Top = 104
  end
  inherited qryID: TADOQuery
    Left = 888
    Top = 104
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 896
    Top = 160
  end
  inherited qryStat: TADOQuery
    Left = 800
    Top = 176
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 864
    Top = 216
  end
  inherited ImageList1: TImageList
    Left = 1000
    Top = 200
  end
  object qryStatus: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdStatus'
      '      ,cNmStatus'
      '  FROM Status'
      ' WHERE nCdStatus = :nPK')
    Left = 712
    Top = 168
    object qryStatusnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryStatuscNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object dsStatus: TDataSource
    DataSet = qryStatus
    Left = 728
    Top = 200
  end
end
