unit fPdvConsProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, ADODB, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, StdCtrls, ImgList, ComCtrls,
  ToolWin, cxLookAndFeelPainters, cxButtons, jpeg, ExtCtrls, cxCheckBox;

type
  TfrmPdvConsProduto = class(TForm)
    ImageList1: TImageList;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1Descrio: TcxGridDBColumn;
    cxGrid1DBTableView1Cdigo: TcxGridDBColumn;
    cxGrid1DBTableView1nCdGrade: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    qryProduto: TADOQuery;
    qryProdutoDescrio: TStringField;
    qryProdutoCdigo: TIntegerField;
    qryProdutonCdGrade: TIntegerField;
    dsProduto: TDataSource;
    btProxima: TcxButton;
    btSeleciona: TcxButton;
    btFechar: TcxButton;
    Image1: TImage;
    function ConsultaProduto():integer;
    procedure FormShow(Sender: TObject);
    procedure Edit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1Exit(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSelecionaClick(Sender: TObject);
    procedure btProximaClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdProduto : integer ;
  end;

var
  frmPdvConsProduto: TfrmPdvConsProduto;

implementation

uses fPDVItemGrade, fPdv, fMenu;

{$R *.dfm}

{ TForm1 }

function TfrmPdvConsProduto.ConsultaProduto: integer;
begin

    qryProduto.Close ;
    Edit1.Text := '' ;
    Edit2.Text := '' ;
    Self.ShowModal;

    Result := nCdProduto ;

end;

procedure TfrmPdvConsProduto.FormShow(Sender: TObject);
begin
  qryProduto.Close ;
  Edit1.Text        := '' ;
  Edit2.Text        := '' ;
  nCdProduto        := 0  ;
  btProxima.Enabled := False ;

  Edit2.SetFocus ;

end;

procedure TfrmPdvConsProduto.Edit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

    if (Key = VK_RETURN) then
        edit1.SetFocus;

    if (Key = VK_ESCAPE) then
        btFechar.Click;

end;

procedure TfrmPdvConsProduto.Edit1KeyDown(Sender: TObject; var Key: Word;  Shift: TShiftState);
begin

    if (Key = VK_RETURN) then
        edit2.SetFocus;

    if (Key = VK_ESCAPE) then
        btFechar.Click;

end;

procedure TfrmPdvConsProduto.Edit1Exit(Sender: TObject);
begin

  if (Edit1.text = '') and (Edit2.text = '') then
  begin
      frmMenu.MensagemAlerta('Nenhum filtro informado.');
      Edit2.SetFocus;
      exit ;
  end ;

  if (trim(edit1.text) <> '') and (Length(trim(edit1.Text)) < 3) then
  begin
      frmMenu.MensagemAlerta('Informe no m�nimo 3 caracteres da descri��o do produto.');
      Edit1.SetFocus;
      abort ;
  end ;

  if (trim(edit2.text) <> '') and (Length(trim(edit2.Text)) < 3) then
  begin
      frmMenu.MensagemAlerta('Informe no m�nimo 3 caracteres da refer�ncia.');
      Edit2.SetFocus;
      abort ;
  end ;

  qryProduto.Close ;
  qryProduto.Parameters.ParamByName('cNmProdutoAnterior').Value := '' ;
  qryProduto.Parameters.ParamByName('cNmProduto').Value         := Trim(Edit1.Text) ;
  qryProduto.Parameters.ParamByName('iLinhas').Value            := 20 ;
  qryProduto.Parameters.ParamByName('cReferencia').Value        := Trim(Edit2.Text) ;
  qryProduto.Open ;

  btProxima.Enabled := True ;

  if qryProduto.eof then
  begin
      frmMenu.MensagemAlerta('Nenhum produto encontrado para o crit�rio utilizado.') ;
      Edit1.Text := '' ;
      Edit2.Text := '' ;
      Edit2.SetFocus;
      exit ;
  end ;

  if (qryProduto.RecordCount < 20) then
      btProxima.Enabled := false ;

  cxGrid1.SetFocus;

end;

procedure TfrmPdvConsProduto.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  case key of

      vk_return : btSeleciona.Click;
      VK_ESCAPE : btFechar.Click;
      vk_F8     : btProxima.Click;

  end


end;

procedure TfrmPdvConsProduto.btSelecionaClick(Sender: TObject);
var
    objPDVItemGrade : TfrmPDVItemGrade ;
begin


    if (qryProdutonCdGrade.Value > 0) then
    begin
        objPDVItemGrade := TfrmPDVItemGrade.Create( Self ) ;

        objPDVItemGrade.qryItemGrade.Close ;
        objPDVItemGrade.qryItemGrade.Parameters.ParamByName('nCdLoja').Value := frmMenu.nCdLojaAtiva ;
        objPDVItemGrade.qryItemGrade.Parameters.ParamByName('nPK').Value     := qryProdutoCdigo.Value ;
        objPDVItemGrade.qryItemGrade.Open ;
        objPDVItemGrade.ShowModal;

        nCdProduto := objPDVItemGrade.qryItemGradenCdProduto.Value ;

        freeAndNil( objPDVItemGrade ) ;

        Self.Close() ;

    end
    else begin
        nCdProduto := qryProdutoCdigo.Value ;

        Self.Close() ;

    end ;

end;

procedure TfrmPdvConsProduto.btProximaClick(Sender: TObject);
begin

  if (Edit1.text = '') and (Edit2.text = '') then
  begin
      frmPDV.MensagemAlerta('Nenhum filtro informado.');
      Edit2.SetFocus;
      exit ;
  end ;

  if not (btProxima.Enabled) then
      exit ;

  qryProduto.Last;
  
  qryProduto.Parameters.ParamByName('cNmProduto').Value         := Trim(Edit1.Text) ;
  qryProduto.Parameters.ParamByName('iLinhas').Value            := 20 ;
  qryProduto.Parameters.ParamByName('cReferencia').Value        := Trim(Edit2.Text) ;
  qryProduto.Parameters.ParamByName('cNmProdutoAnterior').Value := qryProdutoDescrio.Value ;

  btProxima.Enabled := False ;
  
  qryProduto.Close ;
  qryProduto.Open ;

  if not qryProduto.eof then
      btProxima.Enabled := True ;

  if (qryProduto.RecordCount < 20) then
      btProxima.Enabled := false ;

  cxGrid1.SetFocus;

end;

procedure TfrmPdvConsProduto.btFecharClick(Sender: TObject);
begin
    qryProduto.Close ;
    close() ;
end;

end.
