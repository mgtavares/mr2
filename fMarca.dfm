inherited frmMarca: TfrmMarca
  Left = 202
  Top = 90
  Width = 942
  Height = 586
  Caption = 'Marca'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 926
    Height = 523
  end
  object Label1: TLabel [1]
    Left = 35
    Top = 46
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 24
    Top = 70
    Width = 49
    Height = 13
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 41
    Top = 94
    Width = 32
    Height = 13
    Caption = 'Status'
    FocusControl = DBEdit3
  end
  object Label7: TLabel [4]
    Left = 168
    Top = 45
    Width = 107
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo Externo Web'
  end
  inherited ToolBar2: TToolBar
    Width = 926
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 80
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdMarca'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [7]
    Left = 80
    Top = 64
    Width = 665
    Height = 19
    DataField = 'cNmMarca'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [8]
    Left = 80
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 3
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [9]
    Tag = 1
    Left = 148
    Top = 88
    Width = 200
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsMaster
    TabOrder = 4
  end
  object StaticText2: TStaticText [10]
    Left = 8
    Top = 120
    Width = 441
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Marca x Material'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 5
  end
  object DBGridEh1: TDBGridEh [11]
    Left = 8
    Top = 136
    Width = 441
    Height = 321
    DataGrouping.GroupLevels = <>
    DataSource = dsMaterial
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 6
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    OnEnter = DBGridEh1Enter
    OnKeyDown = DBGridEh1KeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdMarca'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdMaterial'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'cNmMaterial'
        Footers = <>
        Width = 280
      end
      item
        EditButtons = <>
        FieldName = 'nCdStatus'
        Footers = <>
        Width = 45
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StaticText1: TStaticText [12]
    Left = 456
    Top = 120
    Width = 441
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Marca x Linha'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 7
  end
  object DBGridEh2: TDBGridEh [13]
    Left = 456
    Top = 136
    Width = 441
    Height = 321
    DataGrouping.GroupLevels = <>
    DataSource = dsLinha
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 8
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    OnEnter = DBGridEh2Enter
    OnKeyDown = DBGridEh2KeyDown
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdMarca'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdLinha'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'cNmLinha'
        Footers = <>
        Width = 280
      end
      item
        EditButtons = <>
        FieldName = 'nCdStatus'
        Footers = <>
        Width = 45
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object btnAtribuirMarca: TcxButton [14]
    Left = 8
    Top = 464
    Width = 137
    Height = 41
    Caption = 'Atribuir Marca'
    Enabled = False
    TabOrder = 9
    OnClick = btnAtribuirMarcaClick
    Glyph.Data = {
      36060000424D3606000000000000360400002800000020000000100000000100
      0800000000000002000000000000000000000001000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000C0DCC000F0CA
      A6000020400000206000002080000020A0000020C0000020E000004000000040
      20000040400000406000004080000040A0000040C0000040E000006000000060
      20000060400000606000006080000060A0000060C0000060E000008000000080
      20000080400000806000008080000080A0000080C0000080E00000A0000000A0
      200000A0400000A0600000A0800000A0A00000A0C00000A0E00000C0000000C0
      200000C0400000C0600000C0800000C0A00000C0C00000C0E00000E0000000E0
      200000E0400000E0600000E0800000E0A00000E0C00000E0E000400000004000
      20004000400040006000400080004000A0004000C0004000E000402000004020
      20004020400040206000402080004020A0004020C0004020E000404000004040
      20004040400040406000404080004040A0004040C0004040E000406000004060
      20004060400040606000406080004060A0004060C0004060E000408000004080
      20004080400040806000408080004080A0004080C0004080E00040A0000040A0
      200040A0400040A0600040A0800040A0A00040A0C00040A0E00040C0000040C0
      200040C0400040C0600040C0800040C0A00040C0C00040C0E00040E0000040E0
      200040E0400040E0600040E0800040E0A00040E0C00040E0E000800000008000
      20008000400080006000800080008000A0008000C0008000E000802000008020
      20008020400080206000802080008020A0008020C0008020E000804000008040
      20008040400080406000804080008040A0008040C0008040E000806000008060
      20008060400080606000806080008060A0008060C0008060E000808000008080
      20008080400080806000808080008080A0008080C0008080E00080A0000080A0
      200080A0400080A0600080A0800080A0A00080A0C00080A0E00080C0000080C0
      200080C0400080C0600080C0800080C0A00080C0C00080C0E00080E0000080E0
      200080E0400080E0600080E0800080E0A00080E0C00080E0E000C0000000C000
      2000C0004000C0006000C0008000C000A000C000C000C000E000C0200000C020
      2000C0204000C0206000C0208000C020A000C020C000C020E000C0400000C040
      2000C0404000C0406000C0408000C040A000C040C000C040E000C0600000C060
      2000C0604000C0606000C0608000C060A000C060C000C060E000C0800000C080
      2000C0804000C0806000C0808000C080A000C080C000C080E000C0A00000C0A0
      2000C0A04000C0A06000C0A08000C0A0A000C0A0C000C0A0E000C0C00000C0C0
      2000C0C04000C0C06000C0C08000C0C0A000F0FBFF00A4A0A000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FDFDFDFDFD10
      1018181010FDFDFDFDFDFDFDFDFDFDA4A4A4A4A4A4FDFDFDFDFDFDFDFD101002
      28282828021810FDFDFDFDFDFDA4A4F7F7F7F7F7F7A4A4FDFDFDFDFD10182828
      2828282828281810FDFDFDFDA4A4F7F7F7F7F7F7F7F7F7A4FDFDFD1019292929
      282828282828281810FDFDA4F7F7F7F7F7F7F7F7F7F7F7F7A4FDFD102A29BC29
      282828282828282810FDFDA4F7F708F7F7F7F7F7F7F7F7F7A4FD182132290829
      28282828282828280210A4F7F7F7F6F7F7F7F7F7F7F7F7F7F7A4182A322AF672
      28282872282828282810A4F707F7F607F7F7F707F7F7F7F7F7A419733373F608
      722A2A08BD2828282810F707F707F6F607F7F7F608F7F7F7F7A421737373BEFF
      F6BEBDF6FF0829282818F707070708FFF60808F6FFF6F7F7F7A4027474327308
      FFFFFFFFFFFF08292810F70707F707F6FFFFFFFFFFFFF6F7F7A40273BD733373
      BD08FFFFFFF673290210F7070807F70708F6FFFFFFF607F7F7A4FD6A08BC2A32
      737374FF0872292919FDFDF7F607F7F7070707FFF607F7F7F7FDFD6A7408B473
      327373BE322A312919FDFDF707F60707F7070708F7F7F7F7A4FDFDFD29B4F6BE
      7473737373722A18FDFDFDFDF708F608070707070707F7F7FDFDFDFDFD7373BE
      0808BEBD742A2AFDFDFDFDFDFD070708F6F6080807F7F7FDFDFDFDFDFDFDFD2A
      7373736A21FDFDFDFDFDFDFDFDFDFDF707070707F7FDFDFDFDFD}
    LookAndFeel.NativeStyle = True
    Margin = 20
    NumGlyphs = 2
  end
  object DBEdit5: TDBEdit [15]
    Left = 283
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdIdExternoWeb'
    DataSource = dsMaster
    TabOrder = 10
  end
  inherited qryMaster: TADOQuery
    AfterOpen = qryMasterAfterOpen
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM MARCA'
      'WHERE nCdMarca =:nPK')
    object qryMasternCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMastercNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMastercNmStatus: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmStatus'
      LookupDataSet = qryStat
      LookupKeyFields = 'nCdStatus'
      LookupResultField = 'cNmStatus'
      KeyFields = 'nCdStatus'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMasternCdIdExternoWeb: TIntegerField
      FieldName = 'nCdIdExternoWeb'
    end
  end
  inherited dsMaster: TDataSource
    Left = 512
    Top = 224
  end
  inherited qryID: TADOQuery
    Left = 544
    Top = 224
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 640
    Top = 192
  end
  inherited qryStat: TADOQuery
    Left = 544
    Top = 192
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 640
    Top = 224
  end
  inherited ImageList1: TImageList
    Left = 672
    Top = 192
  end
  object qryMaterial: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryMaterialBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Material'
      'WHERE nCdMarca = :nPK'
      'AND iNivel = 1')
    Left = 576
    Top = 192
    object qryMaterialnCdMaterial: TAutoIncField
      DisplayLabel = 'Material|C'#243'd'
      FieldName = 'nCdMaterial'
    end
    object qryMaterialnCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMaterialcNmMaterial: TStringField
      DisplayLabel = 'Material|Descri'#231#227'o'
      FieldName = 'cNmMaterial'
      Size = 50
    end
    object qryMaterialnCdStatus: TIntegerField
      DisplayLabel = 'Material|Status'
      FieldName = 'nCdStatus'
    end
    object qryMaterialiNivel: TIntegerField
      FieldName = 'iNivel'
    end
  end
  object dsMaterial: TDataSource
    DataSet = qryMaterial
    Left = 576
    Top = 224
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryLinhaBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Linha'
      'WHERE nCdMarca = :nPK')
    Left = 608
    Top = 192
    object qryLinhanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryLinhanCdLinha: TAutoIncField
      DisplayLabel = 'Linha|C'#243'd'
      FieldName = 'nCdLinha'
    end
    object qryLinhacNmLinha: TStringField
      DisplayLabel = 'Linha|Descri'#231#227'o'
      FieldName = 'cNmLinha'
      Size = 50
    end
    object qryLinhanCdStatus: TIntegerField
      DisplayLabel = 'Linha|Status'
      FieldName = 'nCdStatus'
    end
  end
  object dsLinha: TDataSource
    DataSet = qryLinha
    Left = 608
    Top = 224
  end
end
