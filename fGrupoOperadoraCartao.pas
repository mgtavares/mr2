unit fGrupoOperadoraCartao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmGrupoOperadoraCartao = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoOperadoraCartao: TIntegerField;
    qryMastercNmGrupoOperadoraCartao: TStringField;
    qryMastercFlgDataCredFechPOS: TIntegerField;
    qryMastercFlgTotalLiqFechPOS: TIntegerField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoOperadoraCartao: TfrmGrupoOperadoraCartao;

implementation

{$R *.dfm}

procedure TfrmGrupoOperadoraCartao.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster   := 'GRUPOOPERADORACARTAO' ;
  nCdTabelaSistema  := 75 ;
  nCdConsultaPadrao := 172 ;

end;

procedure TfrmGrupoOperadoraCartao.qryMasterBeforePost(DataSet: TDataSet);
begin

  if Trim(DBEdit2.Text) = '' then
  begin
      MensagemAlerta('Informe a descri��o do grupo de operadora');
      DBEdit2.SetFocus;
      Abort;
  end;
  inherited;

end;

procedure TfrmGrupoOperadoraCartao.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

initialization
    RegisterClass(TfrmGrupoOperadoraCartao);

end.
