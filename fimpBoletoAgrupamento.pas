unit fimpBoletoAgrupamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxCheckBox, StdCtrls, Mask, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, cxPC, ADODB, DBGridEhGrouping,
  GridsEh, DBGridEh;

type
  TfrmImpBoletoAgrupamento = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    qryTempTitSelecionado: TADOQuery;
    dsTempTitSelecioando: TDataSource;
    qryMTitulo: TADOQuery;
    qryMTitulonCdMTitulo: TAutoIncField;
    qryMTitulonCdTitulo: TIntegerField;
    qryMTitulonCdContaBancaria: TIntegerField;
    qryMTitulonCdOperacao: TIntegerField;
    qryMTitulonCdFormaPagto: TIntegerField;
    qryMTituloiNrCheque: TIntegerField;
    qryMTitulonValMov: TBCDField;
    qryMTitulodDtMov: TDateTimeField;
    qryMTitulodDtCad: TDateTimeField;
    qryMTitulodDtCancel: TDateTimeField;
    qryMTitulocNrDoc: TStringField;
    qryMTitulodDtContab: TDateTimeField;
    qryMTitulonCdUsuarioCad: TIntegerField;
    qryMTitulonCdUsuarioCancel: TIntegerField;
    qryMTitulocObsMov: TMemoField;
    qryMTitulonCdCC: TIntegerField;
    qryMTitulocFlgMovAutomatico: TIntegerField;
    qryMTitulodDtBomPara: TDateTimeField;
    qryMTitulonCdLanctoFin: TIntegerField;
    qryMTitulocFlgIntegrado: TIntegerField;
    qryTituloAux: TADOQuery;
    qryTituloAuxnCdTitulo: TIntegerField;
    qryTituloAuxnCdEmpresa: TIntegerField;
    qryTituloAuxnCdEspTit: TIntegerField;
    qryTituloAuxcNrTit: TStringField;
    qryTituloAuxiParcela: TIntegerField;
    qryTituloAuxnCdTerceiro: TIntegerField;
    qryTituloAuxnCdCategFinanc: TIntegerField;
    qryTituloAuxnCdMoeda: TIntegerField;
    qryTituloAuxcSenso: TStringField;
    qryTituloAuxdDtEmissao: TDateTimeField;
    qryTituloAuxdDtReceb: TDateTimeField;
    qryTituloAuxdDtVenc: TDateTimeField;
    qryTituloAuxdDtLiq: TDateTimeField;
    qryTituloAuxdDtCad: TDateTimeField;
    qryTituloAuxdDtCancel: TDateTimeField;
    qryTituloAuxnValTit: TBCDField;
    qryTituloAuxnValLiq: TBCDField;
    qryTituloAuxnSaldoTit: TBCDField;
    qryTituloAuxnValJuro: TBCDField;
    qryTituloAuxnValDesconto: TBCDField;
    qryTituloAuxcObsTit: TMemoField;
    qryTituloAuxnCdSP: TIntegerField;
    qryTituloAuxnCdBancoPortador: TIntegerField;
    qryTituloAuxdDtRemPortador: TDateTimeField;
    qryTituloAuxdDtBloqTit: TDateTimeField;
    qryTituloAuxcObsBloqTit: TStringField;
    qryTituloAuxnCdUnidadeNegocio: TIntegerField;
    qryTituloAuxnCdCC: TIntegerField;
    qryTituloAuxdDtContab: TDateTimeField;
    qryTituloAuxnCdDoctoFiscal: TIntegerField;
    qryTituloAuxdDtCalcJuro: TDateTimeField;
    qryTituloAuxnCdRecebimento: TIntegerField;
    qryTituloAuxnCdCrediario: TIntegerField;
    qryTituloAuxnCdTransacaoCartao: TIntegerField;
    qryTituloAuxnCdLanctoFin: TIntegerField;
    qryTituloAuxcFlgIntegrado: TIntegerField;
    qryTituloAuxcFlgDocCobranca: TIntegerField;
    qryTituloAuxcNrNF: TStringField;
    qryTituloAuxnCdProvisaoTit: TIntegerField;
    qryTituloAuxcFlgDA: TIntegerField;
    qryTituloAuxnCdContaBancariaDA: TIntegerField;
    qryTituloAuxnCdTipoLanctoDA: TIntegerField;
    qryTituloAuxnCdLojaTit: TIntegerField;
    qryTituloAuxcFlgInclusaoManual: TIntegerField;
    qryTituloAuxdDtVencOriginal: TDateTimeField;
    qryTituloAuxcFlgCartaoConciliado: TIntegerField;
    qryTituloAuxnCdGrupoOperadoraCartao: TIntegerField;
    qryTituloAuxnCdOperadoraCartao: TIntegerField;
    qryTituloAuxnValTaxaOperadora: TBCDField;
    qryTituloAuxnCdLoteConciliacaoCartao: TIntegerField;
    qryTituloAuxdDtIntegracao: TDateTimeField;
    qryTituloAuxnCdParcContratoEmpImobiliario: TIntegerField;
    qryTituloAuxnValAbatimento: TBCDField;
    qryTituloAuxnValMulta: TBCDField;
    qryTituloAuxdDtNegativacao: TDateTimeField;
    qryTituloAuxnCdUsuarioNeg: TIntegerField;
    qryTituloAuxnCdItemListaCobrancaNeg: TIntegerField;
    qryTituloAuxnCdLojaNeg: TIntegerField;
    qryTituloAuxdDtReabNeg: TDateTimeField;
    qryTituloAuxcFlgCobradora: TIntegerField;
    qryTituloAuxnCdCobradora: TIntegerField;
    qryTituloAuxdDtRemCobradora: TDateTimeField;
    qryTituloAuxnCdUsuarioRemCobradora: TIntegerField;
    qryTituloAuxnCdItemListaCobRemCobradora: TIntegerField;
    qryTituloAuxcFlgReabManual: TIntegerField;
    qryTituloAuxnCdUsuarioReabManual: TIntegerField;
    qryTituloAuxnCdFormaPagtoTit: TIntegerField;
    qryTituloAuxnPercTaxaJurosDia: TBCDField;
    qryTituloAuxnPercTaxaMulta: TBCDField;
    qryTituloAuxiDiaCarenciaJuros: TIntegerField;
    qryTituloAuxcFlgRenegociado: TIntegerField;
    qryTituloAuxcCodBarra: TStringField;
    qryTituloAuxnValEncargoCobradora: TBCDField;
    qryTituloAuxcFlgRetCobradoraManual: TIntegerField;
    qryTituloAuxnCdServidorOrigem: TIntegerField;
    qryTituloAuxdDtReplicacao: TDateTimeField;
    qryTituloAuxnValIndiceCorrecao: TBCDField;
    qryTituloAuxnValCorrecao: TBCDField;
    qryTituloAuxcFlgNegativadoManual: TIntegerField;
    qryTituloAuxcIDExternoTit: TStringField;
    qryTituloAuxnCdTipoAdiantamento: TIntegerField;
    qryTituloAuxcFlgAdiantamento: TIntegerField;
    qryTituloAuxnCdMTituloGerador: TIntegerField;
    qryTituloAuxnValBaseCorrecao: TBCDField;
    qryTituloAuxcFlgBoletoImpresso: TIntegerField;
    qryTituloAuxcFlgTituloAgrupado: TIntegerField;
    qryTitulo: TADOQuery;
    qryTitulonCdTitulo: TIntegerField;
    qryTitulonCdEmpresa: TIntegerField;
    qryTitulonCdEspTit: TIntegerField;
    qryTitulocNrTit: TStringField;
    qryTituloiParcela: TIntegerField;
    qryTitulonCdTerceiro: TIntegerField;
    qryTitulonCdCategFinanc: TIntegerField;
    qryTitulonCdMoeda: TIntegerField;
    qryTitulocSenso: TStringField;
    qryTitulodDtEmissao: TDateTimeField;
    qryTitulodDtReceb: TDateTimeField;
    qryTitulodDtVenc: TDateTimeField;
    qryTitulodDtLiq: TDateTimeField;
    qryTitulodDtCad: TDateTimeField;
    qryTitulodDtCancel: TDateTimeField;
    qryTitulonValTit: TBCDField;
    qryTitulonValLiq: TBCDField;
    qryTitulonSaldoTit: TBCDField;
    qryTitulonValJuro: TBCDField;
    qryTitulonValDesconto: TBCDField;
    qryTitulocObsTit: TMemoField;
    qryTitulonCdSP: TIntegerField;
    qryTitulonCdBancoPortador: TIntegerField;
    qryTitulodDtRemPortador: TDateTimeField;
    qryTitulodDtBloqTit: TDateTimeField;
    qryTitulocObsBloqTit: TStringField;
    qryTitulonCdUnidadeNegocio: TIntegerField;
    qryTitulonCdCC: TIntegerField;
    qryTitulodDtContab: TDateTimeField;
    qryTitulonCdDoctoFiscal: TIntegerField;
    qryTitulodDtCalcJuro: TDateTimeField;
    qryTitulonCdRecebimento: TIntegerField;
    qryTitulonCdCrediario: TIntegerField;
    qryTitulonCdTransacaoCartao: TIntegerField;
    qryTitulonCdLanctoFin: TIntegerField;
    qryTitulocFlgIntegrado: TIntegerField;
    qryTitulocFlgDocCobranca: TIntegerField;
    qryTitulocNrNF: TStringField;
    qryTitulonCdProvisaoTit: TIntegerField;
    qryTitulocFlgDA: TIntegerField;
    qryTitulonCdContaBancariaDA: TIntegerField;
    qryTitulonCdTipoLanctoDA: TIntegerField;
    qryTitulonCdLojaTit: TIntegerField;
    qryTitulocFlgInclusaoManual: TIntegerField;
    qryTitulodDtVencOriginal: TDateTimeField;
    qryTitulocFlgCartaoConciliado: TIntegerField;
    qryTitulonCdGrupoOperadoraCartao: TIntegerField;
    qryTitulonCdOperadoraCartao: TIntegerField;
    qryTitulonValTaxaOperadora: TBCDField;
    qryTitulonCdLoteConciliacaoCartao: TIntegerField;
    qryTitulodDtIntegracao: TDateTimeField;
    qryTitulonCdParcContratoEmpImobiliario: TIntegerField;
    qryTitulonValAbatimento: TBCDField;
    qryTitulonValMulta: TBCDField;
    qryTitulodDtNegativacao: TDateTimeField;
    qryTitulonCdUsuarioNeg: TIntegerField;
    qryTitulonCdItemListaCobrancaNeg: TIntegerField;
    qryTitulonCdLojaNeg: TIntegerField;
    qryTitulodDtReabNeg: TDateTimeField;
    qryTitulocFlgCobradora: TIntegerField;
    qryTitulonCdCobradora: TIntegerField;
    qryTitulodDtRemCobradora: TDateTimeField;
    qryTitulonCdUsuarioRemCobradora: TIntegerField;
    qryTitulonCdItemListaCobRemCobradora: TIntegerField;
    qryTitulocFlgReabManual: TIntegerField;
    qryTitulonCdUsuarioReabManual: TIntegerField;
    qryTitulonCdFormaPagtoTit: TIntegerField;
    qryTitulonPercTaxaJurosDia: TBCDField;
    qryTitulonPercTaxaMulta: TBCDField;
    qryTituloiDiaCarenciaJuros: TIntegerField;
    qryTitulocFlgRenegociado: TIntegerField;
    qryTitulocCodBarra: TStringField;
    qryTitulonValEncargoCobradora: TBCDField;
    qryTitulocFlgRetCobradoraManual: TIntegerField;
    qryTitulonCdServidorOrigem: TIntegerField;
    qryTitulodDtReplicacao: TDateTimeField;
    qryTitulonValIndiceCorrecao: TBCDField;
    qryTitulonValCorrecao: TBCDField;
    qryTitulocFlgNegativadoManual: TIntegerField;
    qryTitulocIDExternoTit: TStringField;
    qryTitulonCdTipoAdiantamento: TIntegerField;
    qryTitulocFlgAdiantamento: TIntegerField;
    qryTitulonCdMTituloGerador: TIntegerField;
    qryTitulonValBaseCorrecao: TBCDField;
    qryTitulocFlgBoletoImpresso: TIntegerField;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    MaskEditVencto: TMaskEdit;
    Label6: TLabel;
    MaskEditNumeroDocto: TMaskEdit;
    Label2: TLabel;
    MaskEditObs: TMaskEdit;
    qryTempTitSelecionadonCdTitulo: TIntegerField;
    qryTempTitSelecionadocFlgOK: TIntegerField;
    qryTempTitSelecionadocNrTit: TStringField;
    qryTempTitSelecionadocNrNF: TStringField;
    qryTempTitSelecionadoiParcela: TIntegerField;
    qryTempTitSelecionadodDtEmissao: TDateTimeField;
    qryTempTitSelecionadodDtVenc: TDateTimeField;
    qryTempTitSelecionadonSaldoTit: TBCDField;
    qryTempTitSelecionadonCdTerceiro: TIntegerField;
    qryTempTitSelecionadocNmTerceiro: TStringField;
    qryTempTitSelecionadonCdFormaPagto: TIntegerField;
    qryTempTitSelecionadocNmFormaPagto: TStringField;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmFormaPagto: TcxGridDBColumn;
    SP_AGRUPA_BOLETO: TADOStoredProc;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    bAgrupado          : Boolean;
    nCdTituloAgrupado  : Integer;
  end;

var
  frmImpBoletoAgrupamento: TfrmImpBoletoAgrupamento;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmImpBoletoAgrupamento.ToolButton1Click(Sender: TObject);
var
    inCdTitulo         : integer ;
    nValTituloAgrupado : Double ;
    cObsTit            : String;

begin
  inherited;


  if (Trim(MaskEditVencto.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data de vencimento do novo boleto.') ;
      MaskEditVencto.SetFocus;
      abort;
  end;

  if (Trim(MaskEditNumeroDocto.Text) = '') then
  begin
      MensagemAlerta('Informe o n�mero do documento.');
      MaskEditNumeroDocto.SetFocus;
      abort;
  end;

  // pede confirma��o para o agrupamento
  case MessageDlg('Confirma o agrupamento dos t�tulos selecionados ? Este processo n�o poder� ser revertido.',mtConfirmation,[mbYes,mbNo],0) of
      mrNo: begin
          exit ;
      end ;
  end ;


  
  // faz o agrupamento
  // abate os titulos a agrupar e gera o titulo de agrupamento

  frmMenu.Connection.BeginTrans;

  try
      SP_AGRUPA_BOLETO.Close ;
      SP_AGRUPA_BOLETO.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva;
      SP_AGRUPA_BOLETO.Parameters.ParamByName('@cDtVenc').Value           := MaskEditVencto.Text ;
      SP_AGRUPA_BOLETO.Parameters.ParamByName('@cNrTit').Value            := MaskEditNumeroDocto.Text;
      SP_AGRUPA_BOLETO.Parameters.ParamByName('@nCdUsuario').Value        := frmMenu.nCdUsuarioLogado ;
      SP_AGRUPA_BOLETO.Parameters.ParamByName('@cOBS').Value              := MaskEditObs.Text ;
      SP_AGRUPA_BOLETO.Parameters.ParamByName('@nCdTituloAgrupado').Value := 0 ;

      SP_AGRUPA_BOLETO.ExecProc;

      nCdTituloAgrupado := SP_AGRUPA_BOLETO.Parameters.ParamByName('@nCdTituloAgrupado').Value ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise;
  end ;

  frmMenu.Connection.CommitTrans;

  bAgrupado := True;

  ShowMessage('T�tulos agrupados com sucesso!') ;
  
  Close;

end;

procedure TfrmImpBoletoAgrupamento.FormShow(Sender: TObject);
begin
  inherited;

  MaskEditVencto.SetFocus;
  
end;

end.
