unit rEvolucaoVenda_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptEvolucaoVenda_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText4: TQRDBText;
    QRBand2: TQRBand;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    lblFiltro3: TQRLabel;
    lblFiltro2: TQRLabel;
    usp_Relatorio: TADOStoredProc;
    lblFiltro4: TQRLabel;
    QRLabel13: TQRLabel;
    QRShape3: TQRShape;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatorionMes1: TBCDField;
    usp_RelatorionMes2: TBCDField;
    usp_RelatorionMes3: TBCDField;
    usp_RelatorionMes4: TBCDField;
    usp_RelatorionMes5: TBCDField;
    usp_RelatorionMes6: TBCDField;
    usp_RelatorionTotal: TBCDField;
    usp_RelatorionVar1: TBCDField;
    usp_RelatorionVar2: TBCDField;
    usp_RelatorionVar3: TBCDField;
    usp_RelatorionVar4: TBCDField;
    usp_RelatorionVar5: TBCDField;
    QRDBText5: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText1: TQRDBText;
    QRExpr1: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRShape2: TQRShape;
    QRShape6: TQRShape;
    QRShape7: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptEvolucaoVenda_view: TrptEvolucaoVenda_view;

implementation

{$R *.dfm}

end.
