unit rProdutoMenosVendido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls, comObj, ExcelXP, ER2Excel;

type
  TrptProdutoMenosVendido = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    edtDepartamento: TMaskEdit;
    Label2: TLabel;
    edtCategoria: TMaskEdit;
    Label3: TLabel;
    edtSubCategoria: TMaskEdit;
    Label4: TLabel;
    edtSegmento: TMaskEdit;
    Label5: TLabel;
    edtMarca: TMaskEdit;
    Label6: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentocNmDepartamento: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryCategoria: TADOQuery;
    qryCategoriacNmCategoria: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategoriacNmSubCategoria: TStringField;
    DBEdit4: TDBEdit;
    DataSource4: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentocNmSegmento: TStringField;
    DBEdit5: TDBEdit;
    DataSource5: TDataSource;
    qryMarca: TADOQuery;
    qryMarcacNmMarca: TStringField;
    DBEdit6: TDBEdit;
    DataSource6: TDataSource;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryMarcanCdMarca: TIntegerField;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qryCategorianCdCategoria: TAutoIncField;
    qrySegmentonCdSegmento: TAutoIncField;
    edtGrupoProduto: TMaskEdit;
    Label7: TLabel;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBEdit7: TDBEdit;
    DataSource7: TDataSource;
    edtLinha: TMaskEdit;
    Label11: TLabel;
    edtColecao: TMaskEdit;
    Label12: TLabel;
    edtClasseProduto: TMaskEdit;
    Label13: TLabel;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    DBEdit8: TDBEdit;
    DataSource8: TDataSource;
    qryColecao: TADOQuery;
    qryColecaonCdColecao: TIntegerField;
    qryColecaocNmColecao: TStringField;
    DBEdit9: TDBEdit;
    DataSource9: TDataSource;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TIntegerField;
    qryClasseProdutocNmClasseProduto: TStringField;
    DBEdit10: TDBEdit;
    DataSource10: TDataSource;
    RadioGroup7: TRadioGroup;
    RadioGroup8: TRadioGroup;
    edtCdCampanhaPromoc: TMaskEdit;
    Label14: TLabel;
    qryCampanhaPromoc: TADOQuery;
    qryCampanhaPromocnCdCampanhaPromoc: TIntegerField;
    qryCampanhaPromoccNmCampanhaPromoc: TStringField;
    qryCampanhaPromoccFlgAtivada: TIntegerField;
    qryCampanhaPromoccFlgSuspensa: TIntegerField;
    DBEdit11: TDBEdit;
    DataSource11: TDataSource;
    edtDtInicial: TMaskEdit;
    Label10: TLabel;
    RadioGroup1: TRadioGroup;
    edtDtFinal: TMaskEdit;
    Label8: TLabel;
    RadioGroup2: TRadioGroup;
    ER2Excel1: TER2Excel;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtDepartamentoExit(Sender: TObject);
    procedure edtCategoriaExit(Sender: TObject);
    procedure edtSubCategoriaExit(Sender: TObject);
    procedure edtSegmentoExit(Sender: TObject);
    procedure edtMarcaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDepartamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSubCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSegmentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoProdutoExit(Sender: TObject);
    procedure edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLinhaExit(Sender: TObject);
    procedure edtColecaoExit(Sender: TObject);
    procedure edtClasseProdutoExit(Sender: TObject);
    procedure edtCdCampanhaPromocExit(Sender: TObject);
    procedure edtCdCampanhaPromocKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLinhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtColecaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClasseProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ER2Excel1BeforeExport(Sender: TObject);
    procedure ER2Excel1AfterExport(Sender: TObject);
  private
    { Private declarations }
    iCursor : integer;
  public
    { Public declarations }
  end;

var
  rptProdutoMenosVendido: TrptProdutoMenosVendido;

implementation

uses fMenu, fLookup_Padrao, rProdutoMenosVendido_view;

{$R *.dfm}

procedure TrptProdutoMenosVendido.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value           := frmMenu.nCdUsuarioLogado ;
  qryCampanhaPromoc.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      edtLoja.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLoja, edtLoja.text) ;
  end
  else
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

end;

procedure TrptProdutoMenosVendido.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TrptProdutoMenosVendido.edtDepartamentoExit(Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

  if not qryDepartamento.eof then
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TrptProdutoMenosVendido.edtCategoriaExit(Sender: TObject);
begin
  inherited;

  qryCategoria.Close ;
  PosicionaQuery(qryCategoria, edtCategoria.Text) ;

  if not qryCategoria.eof then
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value ;


end;

procedure TrptProdutoMenosVendido.edtSubCategoriaExit(Sender: TObject);
begin
  inherited;

  qrySubCategoria.Close ;
  PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

  if not qrySubCategoria.eof then
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  
end;

procedure TrptProdutoMenosVendido.edtSegmentoExit(Sender: TObject);
begin
  inherited;

  qrySegmento.Close ;
  PosicionaQuery(qrySegmento, edtSegmento.Text) ;
  
end;

procedure TrptProdutoMenosVendido.edtMarcaExit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, edtMarca.Text) ;
  
end;

procedure TrptProdutoMenosVendido.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptProdutoMenosVendido.edtDepartamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(129);

        If (nPK > 0) then
        begin

            edtDepartamento.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptProdutoMenosVendido.edtCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit2.Text = '') then
        begin
            MensagemAlerta('Selecione um departamento.') ;
            edtDepartamento.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(46,'Categoria.nCdDepartamento = ' + edtDepartamento.Text);

        If (nPK > 0) then
        begin

            edtCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCategoria, edtCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptProdutoMenosVendido.edtSubCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma Categoria.') ;
            edtCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(47,'SubCategoria.nCdCategoria = ' + edtCategoria.Text);

        If (nPK > 0) then
        begin

            edtSubCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptProdutoMenosVendido.edtSegmentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit4.Text = '') then
        begin
            MensagemAlerta('Selecione uma SubCategoria.') ;
            edtSubCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'Segmento.nCdSubCategoria = ' + edtSubCategoria.Text);

        If (nPK > 0) then
        begin

            edtSegmento.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySegmento, edtSegmento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptProdutoMenosVendido.edtMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin

            edtMarca.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, edtMarca.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptProdutoMenosVendido.edtGrupoProdutoExit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
  
end;

procedure TrptProdutoMenosVendido.edtGrupoProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin

            edtGrupoProduto.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;

        end ;

    end ;

  end ;


end;

procedure TrptProdutoMenosVendido.ToolButton1Click(Sender: TObject);
var
  cFiltro            : string ;
  linha, coluna,LCID : integer;
  objRel             : TrptProdutoMenosVendido_view;
begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') then
      edtDtInicial.Text := DateToStr(Date) ;

  if (trim(edtDtFinal.Text) = '/  /') then
      edtDtFinal.Text := DateToStr(Date) ;

  if (StrToDate(edtDtInicial.Text) > StrToDate(edtDtFinal.Text)) then
  begin
      MensagemAlerta('Per�odo inv�lido.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  objRel := TrptProdutoMenosVendido_view.Create(nil);

  try
      try
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Close ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.ConvInteiro(edtLoja.Text) ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdDepartamento').Value  := frmMenu.ConvInteiro(edtDepartamento.Text) ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdCategoria').Value     := frmMenu.ConvInteiro(edtCategoria.Text) ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdSubCategoria').Value  := frmMenu.ConvInteiro(edtSubCategoria.Text) ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdSegmento').Value      := frmMenu.ConvInteiro(edtSegmento.Text) ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdMarca').Value         := frmMenu.ConvInteiro(edtMarca.Text) ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdLinha').Value         := frmMenu.ConvInteiro(edtLinha.Text) ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdColecao').Value       := frmMenu.ConvInteiro(edtColecao.Text) ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdClasseProduto').Value := frmMenu.ConvInteiro(edtClasseProduto.Text) ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdGrupoProduto').Value  := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@dDtInicial').Value       := frmMenu.ConvData(edtDtInicial.Text) ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@dDtFinal').Value         := frmMenu.ConvData(edtDtFinal.Text) ;
          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@nCdCampanhaPromoc').Value:= frmMenu.ConvInteiro(edtCdCampanhaPromoc.Text) ;

          if (RadioGroup7.ItemIndex = 0) then
              objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@cFlgDestPromocao').Value := 1
          else objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@cFlgDestPromocao').Value := 0 ;

          if (RadioGroup8.ItemIndex = 0) then
              objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@cFlgProdSoPromocao').Value   := 1
          else objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@cFlgProdSoPromocao').Value   := 0 ;

          if (RadioGroup2.ItemIndex = 0) then
              objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@cFlgSoProdSemVenda').Value   := 1
          else objRel.SPREL_PRODUTO_MENOS_VENDIDO.Parameters.ParamByName('@cFlgSoProdSemVenda').Value   := 0 ;

          objRel.SPREL_PRODUTO_MENOS_VENDIDO.Open ;

          if (RadioGroup1.ItemIndex = 0) then
          begin
              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              cFiltro := '' ;

              if (DBEdit1.Text <> '') then
                  cFiltro := cFiltro + '/ Loja: ' + trim(edtLoja.Text) + '-' + DbEdit1.Text ;

              if (DBEdit2.Text <> '') then
                  cFiltro := cFiltro + '/ Departamento: ' + trim(edtDepartamento.Text) + '-' + DbEdit2.Text ;

              if (DBEdit3.Text <> '') then
                  cFiltro := cFiltro + '/ Categoria: ' + trim(edtCategoria.Text) + '-' + DbEdit3.Text ;

              if (DBEdit4.Text <> '') then
                  cFiltro := cFiltro + '/ SubCategoria: ' + trim(edtSubCategoria.Text) + '-' + DbEdit4.Text ;

              if (DBEdit5.Text <> '') then
                  cFiltro := cFiltro + '/ Segmento: ' + trim(edtSegmento.Text) + '-' + DbEdit5.Text ;

              if (DBEdit6.Text <> '') then
                  cFiltro := cFiltro + '/ Marca: ' + trim(edtMarca.Text) + '-' + DbEdit6.Text ;

              if (DBEdit8.Text <> '') then
                  cFiltro := cFiltro + '/ Linha: ' + trim(edtLinha.Text) + '-' + DbEdit8.Text ;

              if (DBEdit9.Text <> '') then
                  cFiltro := cFiltro + '/ Cole��o: ' + trim(edtColecao.Text) + '-' + DbEdit9.Text ;

              if (DBEdit7.Text <> '') then
                  cFiltro := cFiltro + '/ Grupo Produto: ' + trim(edtGrupoProduto.Text) + '-' + DbEdit7.Text ;

              if (DBEdit10.Text <> '') then
                  cFiltro := cFiltro + '/ Classe Produto: ' + trim(edtClasseProduto.Text) + '-' + DbEdit10.Text ;

              if (DBEdit11.Text <> '') then
                  cFiltro := cFiltro + '/ CampanhaPromoc: ' + trim(edtCdCampanhaPromoc.Text) + '-' + DbEdit11.Text ;

              cFiltro := cFiltro + '/ Per�odo Venda : ' + trim(edtDtInicial.Text) + ' - ' + trim(edtDtFinal.Text) ;

              if (RadioGroup7.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ Dest. Promo��o: Sim'
              else cFiltro := cFiltro + '/ Dest. Promo��o: N�o' ;

              if (RadioGroup8.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ S� em Promo��o: Sim'
              else cFiltro := cFiltro + '/ S� em Promo��o: N�o' ;

              objRel.lblFiltro1.Caption := cFiltro ;

              objRel.QRLabel17.Caption := '*** Posi��o de Estoque em ' + edtDtFinal.Text;

              objRel.QuickRep1.PreviewModal;
          end
          else
          begin

              {-- celulas de cabe�alho --}

              ER2Excel1.Celula['A1'].Text := 'Departamento';
              ER2Excel1.Celula['B1'].Text := 'Categoria';
              ER2Excel1.Celula['C1'].Text := 'C�digo';
              ER2Excel1.Celula['D1'].Text := 'Descri��o';
              ER2Excel1.Celula['E1'].Text := 'Marca';
              ER2Excel1.Celula['F1'].Text := 'Refer�ncia';
              ER2Excel1.Celula['G1'].Text := 'Qt. Vda.';
              ER2Excel1.Celula['H1'].Text := 'Qt. Est.';
              ER2Excel1.Celula['I1'].Text := 'Qt. Tr�ns.';
              ER2Excel1.Celula['J1'].Text := 'Custo Unit.';
              ER2Excel1.Celula['K1'].Text := 'Venda Unit.';
              ER2Excel1.Celula['L1'].Text := '�lt. Rec.';
              ER2Excel1.Celula['M1'].Text := 'C�d. Promo��o';

              ER2Excel1.Celula['A1'].Negrito;
              ER2Excel1.Celula['B1'].Negrito;
              ER2Excel1.Celula['C1'].Negrito;
              ER2Excel1.Celula['D1'].Negrito;
              ER2Excel1.Celula['E1'].Negrito;
              ER2Excel1.Celula['F1'].Negrito;
              ER2Excel1.Celula['G1'].Negrito;
              ER2Excel1.Celula['H1'].Negrito;
              ER2Excel1.Celula['I1'].Negrito;
              ER2Excel1.Celula['J1'].Negrito;
              ER2Excel1.Celula['K1'].Negrito;
              ER2Excel1.Celula['L1'].Negrito;
              ER2Excel1.Celula['M1'].Negrito;

              ER2Excel1.Celula['A1'].Background := RGB(216,216,216);

              ER2Excel1.Celula['A1'].Range('M1');

              {-- grava as celulas de valor --}

              for linha := 0 to objRel.SPREL_PRODUTO_MENOS_VENDIDO.RecordCount - 1 do
              begin

                  ER2Excel1.Celula['A' + IntToStr(linha + 2)].Text := objRel.SPREL_PRODUTO_MENOS_VENDIDOcNmDepartamento.Value;
                  ER2Excel1.Celula['B' + IntToStr(linha + 2)].Text := objRel.SPREL_PRODUTO_MENOS_VENDIDOcNmCategoria.Value;
                  ER2Excel1.Celula['C' + IntToStr(linha + 2)].Text := objRel.SPREL_PRODUTO_MENOS_VENDIDOnCdProduto.Value;
                  ER2Excel1.Celula['D' + IntToStr(linha + 2)].Text := objRel.SPREL_PRODUTO_MENOS_VENDIDOcNmProduto.Value;
                  ER2Excel1.Celula['E' + IntToStr(linha + 2)].Text := objRel.SPREL_PRODUTO_MENOS_VENDIDOcNmMarca.Value;
                  ER2Excel1.Celula['F' + IntToStr(linha + 2)].Text := objRel.SPREL_PRODUTO_MENOS_VENDIDOcReferencia.Value;
                  ER2Excel1.Celula['G' + IntToStr(linha + 2)].Text := objRel.SPREL_PRODUTO_MENOS_VENDIDOnQtdeVenda.Value;
                  ER2Excel1.Celula['H' + IntToStr(linha + 2)].Text := objRel.SPREL_PRODUTO_MENOS_VENDIDOnQtdeEstoque.Value;
                  ER2Excel1.Celula['I' + IntToStr(linha + 2)].Text := objRel.SPREL_PRODUTO_MENOS_VENDIDOnQtdeTransito.Value;
                  ER2Excel1.Celula['J' + IntToStr(linha + 2)].Text := objRel.SPREL_PRODUTO_MENOS_VENDIDOnValCusto.Value;
                  ER2Excel1.Celula['K' + IntToStr(linha + 2)].Text := objRel.SPREL_PRODUTO_MENOS_VENDIDOnValVenda.Value;

                  if ( objRel.SPREL_PRODUTO_MENOS_VENDIDOdDtUltReceb.Value <> '' ) then
                      ER2Excel1.Celula['L' + IntToStr(linha + 2)].Text := StrToDate( objRel.SPREL_PRODUTO_MENOS_VENDIDOdDtUltReceb.Value );

                  ER2Excel1.Celula['M' + IntToStr(linha + 2)].Text := objRel.SPREL_PRODUTO_MENOS_VENDIDOcChaveCampanha.Value;

                  {-- acrescenta mascara as celulas --}

                  ER2Excel1.Celula['L' + IntToStr(linha + 2)].Mascara         := 'dd/mm/aaaa';

                  ER2Excel1.Celula['G' + IntToStr(linha + 2)].Mascara         := '0,00';

                  ER2Excel1.Celula['H' + IntToStr(linha + 2)].Mascara         := '0,00';

                  ER2Excel1.Celula['I' + IntToStr(linha + 2)].Mascara         := '0,00';

                  ER2Excel1.Celula['J' + IntToStr(linha + 2)].Mascara         := '0,00';

                  ER2Excel1.Celula['K' + IntToStr(linha + 2)].Mascara         := '0,00';

                  objRel.SPREL_PRODUTO_MENOS_VENDIDO.Next;

              end;

              { -- exporta planilha e limpa result do componente -- }
              ER2Excel1.ExportXLS;
              ER2Excel1.CleanupInstance;
          end ;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TrptProdutoMenosVendido.edtLinhaExit(Sender: TObject);
begin
  inherited;

  qryLinha.Close;
  PosicionaQuery(qryLinha, edtLinha.Text) ;
  
end;

procedure TrptProdutoMenosVendido.edtColecaoExit(Sender: TObject);
begin
  inherited;

  qryColecao.Close;
  PosicionaQuery(qryColecao, edtColecao.Text) ;
  
end;

procedure TrptProdutoMenosVendido.edtClasseProdutoExit(Sender: TObject);
begin
  inherited;

  qryClasseProduto.Close;
  PosicionaQuery(qryClasseProduto, edtClasseProduto.Text) ;
  
end;

procedure TrptProdutoMenosVendido.edtCdCampanhaPromocExit(Sender: TObject);
begin
  inherited;

  qryCampanhaPromoc.Close;
  PosicionaQuery(qryCampanhaPromoc, edtCdCampanhaPromoc.Text) ;
  
end;

procedure TrptProdutoMenosVendido.edtCdCampanhaPromocKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(194);

        If (nPK > 0) then
            edtCdCampanhaPromoc.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptProdutoMenosVendido.edtLinhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit6.Text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdMarca = ' + edtMarca.Text)
        else nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
            edtLinha.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptProdutoMenosVendido.edtColecaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(52);

        If (nPK > 0) then
            edtColecao.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptProdutoMenosVendido.edtClasseProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
            edtClasseProduto.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptProdutoMenosVendido.ER2Excel1BeforeExport(Sender: TObject);
begin
  inherited;

  iCursor       := Screen.Cursor ;
  Screen.Cursor := crSQLWait ;

  frmMenu.mensagemUsuario('Exportando planilha...');
end;

procedure TrptProdutoMenosVendido.ER2Excel1AfterExport(Sender: TObject);
begin
  inherited;

  Screen.Cursor := iCursor ;
  frmMenu.mensagemUsuario('');
  
end;

initialization
    RegisterClass(TrptProdutoMenosVendido) ;

end.
