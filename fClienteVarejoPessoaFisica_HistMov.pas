unit fClienteVarejoPessoaFisica_HistMov;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxControls, cxPC, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, ADODB, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, StdCtrls, cxContainer, cxTextEdit;

type
  TfrmClienteVarejoPessoaFisica_HistMov = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    SP_CONSULTA_VENDA_PRODUTO: TADOStoredProc;
    SP_CONSULTA_VENDA_PRODUTOnCdPedido: TIntegerField;
    SP_CONSULTA_VENDA_PRODUTOItem: TAutoIncField;
    SP_CONSULTA_VENDA_PRODUTOdDtPedido: TDateTimeField;
    SP_CONSULTA_VENDA_PRODUTOcNmTerceiro: TStringField;
    SP_CONSULTA_VENDA_PRODUTOcNmLoja: TStringField;
    SP_CONSULTA_VENDA_PRODUTOcNmTipoPedido: TStringField;
    SP_CONSULTA_VENDA_PRODUTOnQtdeExpRec: TBCDField;
    SP_CONSULTA_VENDA_PRODUTOnValUnitario: TBCDField;
    SP_CONSULTA_VENDA_PRODUTOnValDesconto: TBCDField;
    SP_CONSULTA_VENDA_PRODUTOnValAcrescimo: TBCDField;
    SP_CONSULTA_VENDA_PRODUTOnValTotal: TBCDField;
    SP_CONSULTA_VENDA_PRODUTOcNmProduto: TStringField;
    SP_CONSULTA_VENDA_PRODUTOcReferencia: TStringField;
    SP_CONSULTA_VENDA_PRODUTOnCdLanctoFin: TIntegerField;
    dsResultado: TDataSource;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1Item: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLoja: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeExpRec: TcxGridDBColumn;
    cxGrid1DBTableView1nValUnitario: TcxGridDBColumn;
    cxGrid1DBTableView1nValDesconto: TcxGridDBColumn;
    cxGrid1DBTableView1nValAcrescimo: TcxGridDBColumn;
    cxGrid1DBTableView1nValTotal: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cReferencia: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLanctoFin: TcxGridDBColumn;
    SP_CONSULTA_VENDA_PRODUTOnCdLoja: TIntegerField;
    cxGrid1DBTableView1nCdLoja: TcxGridDBColumn;
    qryParcelas: TADOQuery;
    dsParcelas: TDataSource;
    qryParcelascSituacao: TStringField;
    qryParcelascNrTit: TStringField;
    qryParcelasiParcela: TIntegerField;
    qryParcelasdDtEmissao: TDateTimeField;
    qryParcelasdDtVenc: TDateTimeField;
    qryParcelasdDtLiq: TDateTimeField;
    qryParcelasnValTit: TBCDField;
    qryParcelasnSaldoTit: TBCDField;
    qryParcelasnValLiq: TBCDField;
    qryParcelasiDiasAtraso: TIntegerField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1cSituacao: TcxGridDBColumn;
    cxGridDBTableView1cNrTit: TcxGridDBColumn;
    cxGridDBTableView1iParcela: TcxGridDBColumn;
    cxGridDBTableView1dDtEmissao: TcxGridDBColumn;
    cxGridDBTableView1dDtVenc: TcxGridDBColumn;
    cxGridDBTableView1dDtLiq: TcxGridDBColumn;
    cxGridDBTableView1nValTit: TcxGridDBColumn;
    cxGridDBTableView1nSaldoTit: TcxGridDBColumn;
    cxGridDBTableView1nValLiq: TcxGridDBColumn;
    cxGridDBTableView1iDiasAtraso: TcxGridDBColumn;
    cxGridDBTableView1nCdLojaTit: TcxGridDBColumn;
    qryCheque: TADOQuery;
    qryChequedDtCompra: TDateTimeField;
    qryChequenCdLoja: TStringField;
    qryChequenCdBanco: TStringField;
    qryChequeiNrCheque: TStringField;
    qryChequedDtVencto: TDateTimeField;
    qryChequenValCheque: TBCDField;
    qryChequeiDiasAtraso: TIntegerField;
    qryChequecNmTabStatusCheque: TStringField;
    qryChequecMsgResgatado: TStringField;
    qryChequedDtResgate: TDateTimeField;
    qryChequenCdLojaResgate: TStringField;
    dsCheque: TDataSource;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView2dDtCompra: TcxGridDBColumn;
    cxGridDBTableView2nCdLoja: TcxGridDBColumn;
    cxGridDBTableView2nCdBanco: TcxGridDBColumn;
    cxGridDBTableView2iNrCheque: TcxGridDBColumn;
    cxGridDBTableView2dDtVencto: TcxGridDBColumn;
    cxGridDBTableView2nValCheque: TcxGridDBColumn;
    cxGridDBTableView2iDiasAtraso: TcxGridDBColumn;
    cxGridDBTableView2cNmTabStatusCheque: TcxGridDBColumn;
    cxGridDBTableView2cMsgResgatado: TcxGridDBColumn;
    cxGridDBTableView2dDtResgate: TcxGridDBColumn;
    cxGridDBTableView2nCdLojaResgate: TcxGridDBColumn;
    qryVale: TADOQuery;
    qryValedDtVale: TDateTimeField;
    qryValenCdLoja: TStringField;
    qryValecCodigo: TStringField;
    qryValecNmTipoVale: TStringField;
    qryValenValVale: TBCDField;
    qryValenSaldoVale: TBCDField;
    qryValedDtBaixa: TDateTimeField;
    qryValecMsgRecompra: TStringField;
    dsVale: TDataSource;
    cxGrid4: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    cxGridDBTableView3dDtVale: TcxGridDBColumn;
    cxGridDBTableView3nCdLoja: TcxGridDBColumn;
    cxGridDBTableView3cCodigo: TcxGridDBColumn;
    cxGridDBTableView3cNmTipoVale: TcxGridDBColumn;
    cxGridDBTableView3nValVale: TcxGridDBColumn;
    cxGridDBTableView3nSaldoVale: TcxGridDBColumn;
    cxGridDBTableView3dDtBaixa: TcxGridDBColumn;
    cxGridDBTableView3cMsgRecompra: TcxGridDBColumn;
    qryParcelascSituacao2: TStringField;
    cxGridDBTableView1cSituacao2: TcxGridDBColumn;
    GroupBox1: TGroupBox;
    cxTextEdit2: TcxTextEdit;
    Label37: TLabel;
    cxTextEdit1: TcxTextEdit;
    Label1: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label2: TLabel;
    Label3: TLabel;
    cxTextEdit4: TcxTextEdit;
    qryParcelascFlgNegativado: TStringField;
    cxGridDBTableView1cFlgNegativado: TcxGridDBColumn;
    qryParcelascFlgRenegociacao: TStringField;
    cxGridDBTableView1DBColumn1: TcxGridDBColumn;
    qryParcelasnCdLoja: TStringField;
    qryParcelasnCdConta: TStringField;
    cxGridDBTableView1DBColumn2: TcxGridDBColumn;
    cxGridDBTableView1DBColumn3: TcxGridDBColumn;
    qryParcelasnCdLojaTit: TStringField;
    qryParcelasnCdContaEmissao: TStringField;
    cxGridDBTableView1DBColumn4: TcxGridDBColumn;
    qryParcelasnCdTitulo: TIntegerField;
    tabConsignacao: TcxTabSheet;
    qryConsignacao: TADOQuery;
    dsConsignacao: TDataSource;
    qryConsignacaonCdConsignacao: TIntegerField;
    qryConsignacaocNmLoja: TStringField;
    qryConsignacaocNmLogin: TStringField;
    qryConsignacaonCdPedido: TIntegerField;
    qryConsignacaocTipoStatusConsig: TStringField;
    qryConsignacaocPedido: TStringField;
    qryConsignacaocDevolucao: TStringField;
    qryConsignacaocFinalizacao: TStringField;
    qryConsignacaocCancelamento: TStringField;
    cxGrid6: TcxGrid;
    cxGridDBTableView5: TcxGridDBTableView;
    cxGridDBTableView5DBColumn1: TcxGridDBColumn;
    cxGridDBTableView5DBColumn2: TcxGridDBColumn;
    cxGridDBTableView5DBColumn3: TcxGridDBColumn;
    cxGridDBTableView5DBColumn4: TcxGridDBColumn;
    cxGridDBTableView5DBColumn5: TcxGridDBColumn;
    cxGridDBTableView5DBColumn6: TcxGridDBColumn;
    cxGridDBTableView5DBColumn7: TcxGridDBColumn;
    cxGridDBTableView5DBColumn8: TcxGridDBColumn;
    cxGridDBTableView5DBColumn9: TcxGridDBColumn;
    cxGridLevel5: TcxGridLevel;
    qryValenCdVale: TIntegerField;
    cxTabSheet5: TcxTabSheet;
    qryCompraWeb: TADOQuery;
    dsCompraWeb: TDataSource;
    cxGrid5: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridDBColumn_dDtPedido: TcxGridDBColumn;
    cxGridDBColumn_nCdPedido: TcxGridDBColumn;
    cxGridDBColumn_nQtdePed: TcxGridDBColumn;
    cxGridDBColumn_nValPedido: TcxGridDBColumn;
    cxGridLevel4: TcxGridLevel;
    qryCompraWebnCdPedido: TIntegerField;
    qryCompraWebdDtPedido: TStringField;
    qryCompraWebcNmTerceiro: TStringField;
    qryCompraWebcNmTipoPedido: TStringField;
    qryCompraWebnQtdePed: TBCDField;
    qryCompraWebnValDesconto: TBCDField;
    qryCompraWebnValAcrescimo: TBCDField;
    qryCompraWebnValPedido: TBCDField;
    qryCompraWebcNmTabStatusPed: TStringField;
    cxGridDBColumn_cNmTabStatusPed: TcxGridDBColumn;
    qryCompraWebnCdLoja: TIntegerField;
    qryCompraWebnValProdutos: TBCDField;
    qryCompraWebnValFrete: TBCDField;
    cxGridDBColumn_nCdLoja: TcxGridDBColumn;
    cxGridDBColumn_nValProdutos: TcxGridDBColumn;
    cxGridDBColumn_nValFrete: TcxGridDBColumn;
    cxGridDBColumn_nValAcrescimo: TcxGridDBColumn;
    cxGridDBColumn_nValDesconto: TcxGridDBColumn;
    procedure exibeMovimentos(nCdTerceiro:integer);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGridDBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure FormShow(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure cxGridDBTableView5DblClick(Sender: TObject);
    procedure cxGridDBTableView3DblClick(Sender: TObject);
  private
    { Private declarations }
    cDestacaSPC : string ;
  public
    { Public declarations }
  end;

var
  frmClienteVarejoPessoaFisica_HistMov: TfrmClienteVarejoPessoaFisica_HistMov;

implementation

uses fMenu, fConsultaVendaProduto_Dados, fHistMov_HistPagto, fConsignacao,fCaixa_Recebimento;

{$R *.dfm}

{ TfrmClienteVarejoPessoaFisica_HistMov }

procedure TfrmClienteVarejoPessoaFisica_HistMov.exibeMovimentos(
  nCdTerceiro: integer);
begin

  SP_CONSULTA_VENDA_PRODUTO.Close ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@nCdLoja').Value        := 0 ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@nCdCliente').Value     := nCdTerceiro ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@cCNPJCPF').Value       := '' ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@nCdPedido').Value      := 0 ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@iNrDoctoFiscal').Value := 0 ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@dDtInicial').Value     := '01/01/1900' ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@dDtFinal').Value       := '01/01/1900' ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@nCdCrediario').Value   := 0 ;
  SP_CONSULTA_VENDA_PRODUTO.Parameters.ParamByName('@iNrCartao').Value      := '';
  SP_CONSULTA_VENDA_PRODUTO.Open ;

  qryParcelas.Close;
  qryParcelas.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
  qryParcelas.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro ;
  qryParcelas.Open ;

  qryCheque.Close;
  qryCheque.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
  qryCheque.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro ;
  qryCheque.Open ;

  qryVale.Close;
  qryVale.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro ;
  qryVale.Open ;

  qryCompraWeb.Close;
  qryCompraWeb.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryCompraWeb.Parameters.ParamByName('nCdCliente').Value := nCdTerceiro ;
  qryCompraWeb.Open;

  qryConsignacao.Close;
  PosicionaQuery(qryConsignacao, IntToStr(nCdTerceiro));

  cxPageControl1.ActivePageIndex := 0 ;

  Self.ShowModal;

end;

procedure TfrmClienteVarejoPessoaFisica_HistMov.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objConsultaVendaProduto_Dados : TfrmConsultaVendaProduto_Dados ;
begin
  inherited;

  if not SP_CONSULTA_VENDA_PRODUTO.Eof then
  begin

      objConsultaVendaProduto_Dados := TfrmConsultaVendaProduto_Dados.Create( Self ) ;

      PosicionaQuery(objConsultaVendaProduto_Dados.qryItemPedido, SP_CONSULTA_VENDA_PRODUTOnCdLanctoFin.AsString) ;
      PosicionaQuery(objConsultaVendaProduto_Dados.qryCondicao  , SP_CONSULTA_VENDA_PRODUTOnCdLanctoFin.AsString) ;
      PosicionaQuery(objConsultaVendaProduto_Dados.qryPrestacoes, SP_CONSULTA_VENDA_PRODUTOnCdLanctoFin.AsString) ;
      PosicionaQuery(objConsultaVendaProduto_Dados.qryCheques   , SP_CONSULTA_VENDA_PRODUTOnCdLanctoFin.AsString) ;
      objConsultaVendaProduto_Dados.ShowModal;

      freeAndNil( objConsultaVendaProduto_Dados ) ;

  end ;

end;

procedure TfrmClienteVarejoPessoaFisica_HistMov.cxGridDBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if (ARecord.Values[1] = 'ATRASO') then
      if (strToInt(ARecord.Values[11]) > 0) then
          AStyle := frmMenu.LinhaVermelha ;
 
  if (ARecord.Values[1] = 'NORMAL') then
          AStyle := frmMenu.LinhaAmarela ;
  
  if (ARecord.Values[0] = 'RENEGOCIADO') then
          AStyle := frmMenu.LinhaAzul ;

  if ((ARecord.Values[15] = 'SIM') and (cDestacaSPC = 'S')) then
          AStyle := frmMenu.LinhaPreta ;

end;

procedure TfrmClienteVarejoPessoaFisica_HistMov.FormShow(Sender: TObject);
begin
  inherited;

  cDestacaSPC := frmMenu.LeParametro('MARCASPCHMOVC') ;

  if (frmMenu.LeParametro('USACONSIGNCAO') = 'S') then
      tabConsignacao.TabVisible := True
  else
      tabConsignacao.TabVisible := False;
end;

procedure TfrmClienteVarejoPessoaFisica_HistMov.cxGridDBTableView1DblClick(
  Sender: TObject);
var
    objHistMov_HistPagto : TfrmHistMov_HistPagto;
begin
    inherited;
    {Cria o Objeto, passa a PK do titulo e abre a tela}
    objHistMov_HistPagto := TfrmHistMov_HistPagto.Create(self);
    PosicionaQuery(objHistMov_HistPagto.qryInfPag,qryParcelasnCdTitulo.AsString);

    {Testa se a query est� vazia, s� abre janela caso n�o esteja }
    if not(objHistMov_HistPagto.qryInfPag.IsEmpty) then
        showForm(objHistMov_HistPagto,true)
    else
        freeandnil( objHistMov_HistPagto );
end;

procedure TfrmClienteVarejoPessoaFisica_HistMov.cxGridDBTableView5DblClick(
  Sender: TObject);
var
    objForm : TfrmConsignacao;
begin
    if not (qryConsignacao.IsEmpty) then
    begin
        objForm := TfrmConsignacao.Create(nil);

        objForm.qryMaster.Close;

        PosicionaQuery(objForm.qryMaster, IntToStr(qryConsignacaonCdConsignacao.Value));

        objForm.ShowModal;

        FreeAndNil(objForm);
    end;
end;

procedure TfrmClienteVarejoPessoaFisica_HistMov.cxGridDBTableView3DblClick(
  Sender: TObject);
var
  objForm : TfrmCaixa_Recebimento;
begin

  if not qryVale.eof then
  begin

      if (frmMenu.MessageDlg('Confirma a impress�o deste vale ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
          exit ;
          
      if (qryValenSaldoVale.Value <= 0) then
      begin
           MensagemAlerta('Este vale n�o possui saldo, imposs�vel prosseguir.');
           abort;
      end else
      begin

          try
              try
                  objForm := TfrmCaixa_Recebimento.Create(nil);

                  objForm.ImprimirVale(qryValenCdVale.Value);
              except
                  MensagemErro('Erro no processamento.');
                  raise;
              end;
          finally
              FreeAndNil(objForm);
          end;
      end ;
    end;

end;

end.
