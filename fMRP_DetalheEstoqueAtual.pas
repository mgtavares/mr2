unit fMRP_DetalheEstoqueAtual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmMRP_DetalheEstoqueAtual = class(TfrmProcesso_Padrao)
    qryPosicaoEstoque: TADOQuery;
    qryPosicaoEstoquenCdLocalEstoque: TIntegerField;
    qryPosicaoEstoquecNmLocalEstoque: TStringField;
    qryPosicaoEstoquenQtdeDisponivel: TBCDField;
    qryPosicaoEstoquecLocalizacao: TStringField;
    DataSource1: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1nCdLocalEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLocalEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeDisponivel: TcxGridDBColumn;
    cxGrid1DBTableView1cLocalizacao: TcxGridDBColumn;
    qryPosicaoEstoquedDtUltInventario: TDateTimeField;
    cxGrid1DBTableView1dDtUltInventario: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMRP_DetalheEstoqueAtual: TfrmMRP_DetalheEstoqueAtual;

implementation

{$R *.dfm}

end.
