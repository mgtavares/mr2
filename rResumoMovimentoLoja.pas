unit rResumoMovimentoLoja;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, RDprint, DBCtrls, StdCtrls, Mask;

type
  TrptResumoMovimentoLoja = class(TfrmRelatorio_Padrao)
    RDprint1: TRDprint;
    Image2: TImage;
    Image3: TImage;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    MaskEdit2: TMaskEdit;
    Label6: TLabel;
    MaskEdit1: TMaskEdit;
    Label3: TLabel;
    Label1: TLabel;
    Label5: TLabel;
    qryTotalContaBancaria: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    SPREL_RESUMO_MOVTO_CAIXA: TADOStoredProc;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    qrySaldoConta: TADOQuery;
    qrySaldoContanCdLoja: TStringField;
    qrySaldoContanCdConta: TStringField;
    qrySaldoContanSaldoConta: TBCDField;
    qryTotalContaBancariacNmFormaPagto: TStringField;
    qryTotalContaBancariaTotalValPagto: TBCDField;
    qryTotalContaBancariaiQtdVendas: TIntegerField;
    qryRecebParcFormaPagto: TADOQuery;
    qryLanctoManual: TADOQuery;
    qryRecebParcFormaPagtocNmFormaPagto: TStringField;
    qryRecebParcFormaPagtoTotalValPagto: TBCDField;
    qryRecebParcFormaPagtoiQtdeParcPagaTotal: TIntegerField;
    qryLanctoManualdDtLancto: TDateTimeField;
    qryLanctoManualcHistorico: TStringField;
    qryLanctoManualnValLancto: TBCDField;
    qryLanctoManualnCdConta: TStringField;
    Edit1: TEdit;
    Edit2: TEdit;
    dsLoja: TDataSource;
    dsContaBancaria: TDataSource;
    dsSaldoConta: TDataSource;
    dsTotalContaBancaria: TDataSource;
    dsRecebParcFormaPagto: TDataSource;
    dsLanctoManual: TDataSource;
    CheckBox1: TCheckBox;
    procedure ToolButton1Click(Sender: TObject);
    procedure Edit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Edit1Exit(Sender: TObject);
    procedure Edit2Exit(Sender: TObject);
    procedure RDprint1NewPage(Sender: TObject; Pagina: Integer);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptResumoMovimentoLoja: TrptResumoMovimentoLoja;
  iLinha : integer ;
  iPagina: integer ;
  
implementation

uses fMenu, fLookup_Padrao, Math;

{$R *.dfm}

procedure TrptResumoMovimentoLoja.Edit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(147);

            If (nPK > 0) then
            begin
                Edit1.Text := IntToStr(nPK);
                PosicionaQuery(qryLoja, Edit1.Text);
            end ;

    end ;

  end ;
end;

procedure TrptResumoMovimentoLoja.Edit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        If (Trim(Edit1.Text)='') then
        begin
            MensagemAlerta('Para selecionar uma conta caixa, selecione primeiro uma loja');
            Abort;
        end
        else begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(126,'ContaBancaria.nCdLoja = ' + Edit1.Text);
        end;

        If (nPK > 0) then
        begin
            Edit2.Text := IntToStr(nPK) ;
            qryContaBancaria.Parameters.ParamByName('nCdLoja').Value := Edit1.Text;
            PosicionaQuery(qryContaBancaria, Edit2.Text) ;
        end ;

    end ;
    end;

end;

procedure TrptResumoMovimentoLoja.Edit1Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  qryContaBancaria.Close;
  PosicionaQuery(qryLoja,Edit1.Text);
end;

procedure TrptResumoMovimentoLoja.Edit2Exit(Sender: TObject);
begin
  inherited;
  if (Trim(Edit2.Text) = '') and (Trim(Edit1.Text)='') then
  begin
  end
  else if (Trim(Edit1.Text)='') then
  begin
      MensagemAlerta('Para selecionar uma conta caixa, selecione primeiro uma loja');
      Abort;
  end
  else begin
      qryContaBancaria.Close;
      qryContaBancaria.Parameters.ParamByName('nCdLoja').Value := Edit1.Text;
      PosicionaQuery(qryContaBancaria, Edit2.Text) ;
  end;
end;

procedure Verifica_Data(Componente: TMaskEdit);
begin
  try
    if Componente.Text <> '  /  /    ' Then
      StrToDate(Componente.Text);
  except
    frmMenu.MensagemAlerta('Data Inv�lida');
    Componente.Clear;
    Componente.SetFocus;
    Abort;
  end;
end;

procedure TrptResumoMovimentoLoja.ToolButton1Click(Sender: TObject);
var
    iSomaPositivo : currency;
    iSomaNegativo : currency;
    iLinhaReuso   : integer;
    iPercent      : double;
    dtInicial     : string;
    dtFinal       : string;
begin
    inherited;
    SPREL_RESUMO_MOVTO_CAIXA.Close ;
    SPREL_RESUMO_MOVTO_CAIXA.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
    SPREL_RESUMO_MOVTO_CAIXA.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado ;

    if (DBEdit2.Text = '') then
        SPREL_RESUMO_MOVTO_CAIXA.Parameters.ParamByName('@nCdLoja').Value   := 0
    else SPREL_RESUMO_MOVTO_CAIXA.Parameters.ParamByName('@nCdLoja').Value  := Edit1.Text ;

    if (DBEdit4.Text = '') then
        SPREL_RESUMO_MOVTO_CAIXA.Parameters.ParamByName('@nCdContaBancaria').Value := 0
    else SPREL_RESUMO_MOVTO_CAIXA.Parameters.ParamByName('@nCdContaBancaria').Value := Edit2.Text ;

    Verifica_Data(MaskEdit1);
    Verifica_Data(MaskEdit2);

    if (MaskEdit1.Text = '  /  /    ') then
        MaskEdit1.Text := DateToStr(Now());

    if (MaskEdit2.Text = '  /  /    ') then
        MaskEdit2.Text := DateToStr(Now());

    if (frmMenu.ConvData(MaskEdit1.Text) > frmMenu.ConvData(MaskEdit2.Text)) then
    begin
        MensagemAlerta('Data inicial maior do que a data final');
        Abort;
    end;

    SPREL_RESUMO_MOVTO_CAIXA.Parameters.ParamByName('@dDtInicial').Value := frmMenu.ConvData(MaskEdit1.Text);
    SPREL_RESUMO_MOVTO_CAIXA.Parameters.ParamByName('@dDtFinal').Value := frmMenu.ConvData(MaskEdit2.Text);

    dtInicial := SPREL_RESUMO_MOVTO_CAIXA.Parameters.ParamByName('@dDtInicial').Value;
    dtFinal   := SPREL_RESUMO_MOVTO_CAIXA.Parameters.ParamByName('@dDtFinal').Value;

    {-- prepara as tabelas tempor�rias, vari�veis e executa a procedure --}
    cmdPreparaTemp.Execute;
    SPREL_RESUMO_MOVTO_CAIXA.ExecProc ;

    qrySaldoConta.Close;
    qrySaldoConta.Open;

    qryTotalContaBancaria.Close;
    qryTotalContaBancaria.Open;

    qryRecebParcFormaPagto.Close;
    qryRecebParcFormaPagto.Open;

    {--fecha a qry para garantir que ela estara vazia na hora da verifica��o--}
    qryLanctoManual.Close;

    {--quando a flag estiver marcada mostra os lan�amentos manuais--}
    if(CheckBox1.Checked = True) then
        qryLanctoManual.Open;

    iPagina := 0;
    iLinha  := 8;

    {-- inicia o design do relat�rio --}
    RDprint1.Abrir;
    rdPrint1.ImpD(6,0,'Posi��o dos Caixas',[normal,negrito]);
    rdPrint1.ImpF(iLinha,1,'Caixa / Cofre',[normal,negrito]);
    rdPrint1.ImpF(iLinha,58,'Saldo Atual',[normal,negrito]);

    {-- imprime a posi��o dos caixas --}
    qrySaldoConta.First;
    While not qrySaldoConta.Eof do
    begin
        iLinha := iLinha + 1;

        rdPrint1.ImpF(iLinha,1,qrySaldoContanCdConta.Value ,[normal]);
        rdPrint1.Imp(iLinha,58,'R$');
        rdPrint1.ImpVal(iLinha,63,'###,##0.00',qrySaldoContanSaldoConta.Value,[normal]);

        iSomaPositivo := iSomaPositivo + qrySaldoContanSaldoConta.Value;

        qrySaldoConta.Next;
   end;

   {-- imprime o resumo de vendas --}

   rdPrint1.ImpF(6,58,'R$',[negrito]);
   rdPrint1.ImpVal(6,63,'###,##0.00',iSomaPositivo,[normal,negrito]);
   iLinha := iLinha + 4;

   rdprint1.ImpF(iLinha,58,'Total',[normal,negrito]) ;
   iLinha := iLinha + 1 ;
   rdPrint1.ImpD(iLinha,0,'Resumo de Vendas',[normal,negrito]);

   rdPrint1.ImpD(iLinha,56,qryTotalContaBancariaiQtdVendas.AsString,[normal,negrito]);
   iLinhaReuso := iLinha;

   qryTotalContaBancaria.First;

   iSomaPositivo := 0;

   While not qryTotalContaBancaria.Eof do
   begin
       iLinha := iLinha + 1;

       rdPrint1.ImpD(iLinha,56,qryTotalContaBancariacNmFormaPagto.Value,[normal]);
       rdPrint1.Imp(iLinha,58,'R$');
       rdPrint1.ImpVal(iLinha,63,'###,##0.00',qryTotalContaBancariaTotalValPagto.Value,[normal]);

       iSomaPositivo := iSomaPositivo + qryTotalContaBancariaTotalValPagto.Value;

       qryTotalContaBancaria.Next;

   end;

   iLinha := iLinhaReuso;

   rdPrint1.ImpF(iLinha,58,'R$',[negrito]);
   rdPrint1.ImpVal(iLinha,63,'###,##0.00',iSomaPositivo,[normal,negrito]);

   qryTotalContaBancaria.First;

   While not qryTotalContaBancaria.Eof do
   begin
       iLinha := iLinha + 1;
       iPercent := frmMenu.TBRound((qryRecebParcFormaPagtoTotalValPagto.Value/iSomaPositivo)*100,2);
       rdPrint1.ImpVal(iLinha,75,'0.00%',((qryTotalContaBancariaTotalValPagto.Value/iSomaPositivo)*100),[normal]);
       qryTotalContaBancaria.Next;
   end;

   {-- imprime o recebimento de parcelas --}

   iLinha := iLinha + 4;
   rdprint1.ImpF(iLinha,58,'Total',[normal,negrito]) ;
   iLinha := iLinha + 1 ;
   rdPrint1.ImpD(iLinha,0,'Recebimento de Parcelas',[normal,negrito]);
   rdPrint1.ImpD(iLinha,56, qryRecebParcFormaPagtoiQtdeParcPagaTotal.AsString,[normal,negrito]);

   iLinhaReuso := iLinha;

   qryRecebParcFormaPagto.First;

   iSomaPositivo := 0;

   while not qryRecebParcFormaPagto.Eof do
   begin

       iLinha := iLinha + 1;

       rdPrint1.ImpD(iLinha,56,qryRecebParcFormaPagtocNmFormaPagto.Value,[normal]);
       rdPrint1.Imp(iLinha,58,'R$');
       rdPrint1.ImpVal(iLinha,63,'###,##0.00',qryRecebParcFormaPagtoTotalValPagto.Value,[normal]);

       iSomaPositivo := iSomaPositivo + qryRecebParcFormaPagtoTotalValPagto.Value;

       qryRecebParcFormaPagto.Next;

   end;

   iLinha := iLinhaReuso;

   rdPrint1.ImpF(iLinha,58,'R$',[negrito]);
   rdPrint1.ImpVal(iLinha,63,'###,##0.00',iSomaPositivo,[normal,negrito]);

   qryRecebParcFormaPagto.First;

   While not qryRecebParcFormaPagto.Eof do
   begin

       iLinha   := iLinha + 1;
       iPercent := frmMenu.TBRound((qryRecebParcFormaPagtoTotalValPagto.Value/iSomaPositivo)*100,2);

       rdPrint1.ImpVal(iLinha,75,'0.00%',iPercent,[normal]);

       qryRecebParcFormaPagto.Next;
   end;

   iLinha := iLinha + 4;

   {-- imprime os lan�amentos avulsos --}

   if not(qryLanctoManual.Eof) then
   begin
       rdPrint1.ImpD(iLinha,0,'Movimenta��o Manual',[normal,negrito]);
       iLinha := iLinha + 1;
       rdPrint1.ImpD(iLinha,60,'Data        Tipo Lan�amento - Hist�rico                  Caixa            Valor',[normal,negrito]);

       iSomaPositivo := 0;
       iSomaPositivo := 0;

       while not qryLanctoManual.Eof do
       begin

           iLinha := iLinha + 1;

           rdPrint1.ImpF(iLinha,1,DateToStr(qryLanctoManualdDtLancto.Value),[normal]);
           rdPrint1.ImpF(iLinha,13,qryLanctoManualcHistorico.Value,[normal]);
           rdPrint1.ImpF(iLinha,51,qryLanctoManualnCdConta.Value,[normal]);
           rdPrint1.Imp(iLinha,68,'R$');
           rdPrint1.ImpVal(iLinha,71,'###,##0.00',qryLanctoManualnValLancto.Value,[normal]);

           if (qryLanctoManualnValLancto.Value < 0) then
               iSomaNegativo := iSomaNegativo + qryLanctoManualnValLancto.Value
           else iSomaPositivo := iSomaPositivo + qryLanctoManualnValLancto.Value;

           if(iLinha > 62) then
           begin
               iLinha := 6;
               RDprint1.Novapagina;
               RDprint1.ImpD(iLinha,60,'Data        Tipo Lan�amento - Hist�rico                  Caixa            Valor',[normal,negrito]);
           end;

           qryLanctoManual.Next;
       end;

       rdPrint1.ImpD(iLinha + 2,63,'Sa�das',[normal,negrito]);
       rdPrint1.ImpD(iLinha + 3,63,'Entradas',[normal,negrito]);

       rdPrint1.ImpF(iLinha + 2,68,'R$',[negrito]);
       rdPrint1.ImpF(iLinha + 3,68,'R$',[negrito]);

       rdPrint1.ImpVal(iLinha + 2,71,'###,##0.00',Abs(iSomaNegativo),[normal,negrito]);
       rdPrint1.ImpVal(iLinha + 3,71,'###,##0.00',iSomaPositivo,[normal,negrito]);
   end;
   rdPrint1.Fechar;
end;

procedure TrptResumoMovimentoLoja.RDprint1NewPage(Sender: TObject;
  Pagina: Integer);
begin
  inherited;
  iPagina := iPagina + 1 ;

  rdPrint1.ImpF(1,1,frmMenu.cNmFantasiaEmpresa,[negrito]);
  rdPrint1.ImpD(1,80,'Data Emiss�o: ' + DateTimeToStr(Now()),[comp17]);
  rdPrint1.ImpF(2,1,'RESUMO MOVIMENTO LOJA',[negrito])  ;
  rdPrint1.ImpD(2,80,'P�gina: ' + frmMenu.ZeroEsquerda(intToStr(iPagina),3),[comp17]);
  rdPrint1.ImpD(3,80,'Per�odo   : '    + MaskEdit1.Text + ' � ' + MaskEdit2.Text , [comp17]) ;
  rdPrint1.impBox(4,01,'--------------------------------------------------------------------------------');

end;

procedure TrptResumoMovimentoLoja.FormShow(Sender: TObject);
begin
  inherited;
  CheckBox1.Checked := True;
end;

initialization
    RegisterClass(TrptResumoMovimentoLoja) ;

end.
