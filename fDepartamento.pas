unit fDepartamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmDepartamento = class(TfrmCadastro_Padrao)
    qryMasternCdDepartamento: TAutoIncField;
    qryMastercNmDepartamento: TStringField;
    qryMasternCdStatus: TIntegerField;
    qryMastercNmStatus: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    qryCategoria: TADOQuery;
    qryCategorianCdCategoria: TAutoIncField;
    qryCategorianCdDepartamento: TIntegerField;
    qryCategoriacNmCategoria: TStringField;
    qryCategorianCdStatus: TIntegerField;
    dsCategoria: TDataSource;
    DBGridEh1: TDBGridEh;
    StaticText2: TStaticText;
    qrySubCategoria: TADOQuery;
    dsSubCategoria: TDataSource;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategorianCdCategoria: TIntegerField;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySubCategorianCdStatus: TIntegerField;
    DBGridEh2: TDBGridEh;
    StaticText1: TStaticText;
    qrySegmento: TADOQuery;
    dsSegmento: TDataSource;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentonCdSubCategoria: TIntegerField;
    qrySegmentocNmSegmento: TStringField;
    qrySegmentonCdStatus: TIntegerField;
    DBGridEh3: TDBGridEh;
    StaticText3: TStaticText;
    qryMarcaSub: TADOQuery;
    dsMarcaSub: TDataSource;
    qryMarcaSubnCdMarcaSubCategoria: TAutoIncField;
    qryMarcaSubnCdMarca: TIntegerField;
    qryMarcaSubnCdSubCategoria: TIntegerField;
    DBGridEh4: TDBGridEh;
    StaticText4: TStaticText;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    qryMarcaSubcNmMarca: TStringField;
    qryMasternCdGrupoProduto: TIntegerField;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    qryGrupoProdutocFlgExpPDV: TIntegerField;
    Label4: TLabel;
    DBEdit5: TDBEdit;
    qryMastercNmGrupoProduto: TStringField;
    DBEdit6: TDBEdit;
    qryCategorianCdIdExternoWeb: TIntegerField;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    qryMasternCdIdExternoWeb: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryCategoriaBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryCategoriaAfterScroll(DataSet: TDataSet);
    procedure qrySubCategoriaBeforePost(DataSet: TDataSet);
    procedure qryCategoriaAfterClose(DataSet: TDataSet);
    procedure qrySegmentoBeforePost(DataSet: TDataSet);
    procedure qrySubCategoriaAfterScroll(DataSet: TDataSet);
    procedure qrySubCategoriaAfterClose(DataSet: TDataSet);
    procedure qryMarcaSubBeforePost(DataSet: TDataSet);
    procedure DBGridEh4KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDepartamento: TfrmDepartamento;

implementation

uses fLookup_Padrao, fDepartamento_SubCategoria, fMenu;

{$R *.dfm}

procedure TfrmDepartamento.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'DEPARTAMENTO' ;
  nCdTabelaSistema  := 26 ;
  nCdConsultaPadrao := 45 ;

end;

procedure TfrmDepartamento.btIncluirClick(Sender: TObject);
begin
  inherited;
  DbEdit2.SetFocus;
end;

procedure TfrmDepartamento.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryCategoria.Close ;
  qryCategoria.Parameters.ParamByName('nPK').Value := qryMasternCdDepartamento.Value ;
  qryCategoria.Open ;
  
end;

procedure TfrmDepartamento.qryCategoriaBeforePost(DataSet: TDataSet);
begin

  if (qryCategoriacNmCategoria.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o da categoria.') ;
      abort ;
  end ;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryCategorianCdDepartamento.Value := qryMasternCdDepartamento.Value ;
  qryCategoriacNmCategoria.Value    := Uppercase(qryCategoriacNmCategoria.Value) ; 

  if (qryCategorianCdStatus.Value = 0) then
      qryCategorianCdStatus.Value := 1 ;

  if (qryCategoria.State = dsInsert) then
      qryCategorianCdCategoria.Value := frmMenu.fnProximoCodigo('CATEGORIA') ;
  inherited;

end;

procedure TfrmDepartamento.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryCategoria.Close ;

end;

procedure TfrmDepartamento.qryCategoriaAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qrySubCategoria.Close ;
  qrySubCategoria.Parameters.ParamByName('nPK').Value := qryCategorianCdCategoria.Value ;
  qrySubCategoria.Open ;

end;

procedure TfrmDepartamento.qrySubCategoriaBeforePost(DataSet: TDataSet);
begin
  inherited;

  if not qryCategoria.Active or (qryCategorianCdCategoria.Value = 0) then
  begin
      ShowMessage('Selecione uma categoria antes de gravar a sub categoria.') ;
      abort ;
  end ;

  qrySubCategorianCdCategoria.Value := qryCategorianCdCategoria.Value ;

  if (qrySubCategorianCdStatus.Value = 0) then
      qrySubCategorianCdStatus.Value := 1 ;

end;

procedure TfrmDepartamento.qryCategoriaAfterClose(DataSet: TDataSet);
begin
  inherited;
  qrySubCategoria.Close ;
  qryMarcaSub.Close ;

end;

procedure TfrmDepartamento.qrySegmentoBeforePost(DataSet: TDataSet);
begin
  inherited;

  if not qrySubCategoria.Active or (qrySubCategorianCdSubCategoria.Value = 0) then
  begin
      ShowMessage('Selecione uma sub categoria antes de gravar o segmento.') ;
      abort ;
  end ;

  qrySegmentonCdSubCategoria.Value := qrySubCategorianCdSubCategoria.Value ;

  if (qrySegmentonCdStatus.Value = 0) then
      qrySegmentonCdStatus.Value := 1 ;

end;

procedure TfrmDepartamento.qrySubCategoriaAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qrySegmento.Close ;
  qrySegmento.Parameters.ParamByName('nPK').Value := qrySubCategorianCdSubCategoria.Value ;
  qrySegmento.Open ;

  qryMarcaSub.Close ;
  qryMarcaSub.Parameters.ParamByName('nPK').Value := qrySubCategorianCdSubCategoria.Value ;
  qryMarcaSub.Open ;
end;

procedure TfrmDepartamento.qrySubCategoriaAfterClose(DataSet: TDataSet);
begin
  inherited;
  qrySegmento.Close ;

end;

procedure TfrmDepartamento.qryMarcaSubBeforePost(DataSet: TDataSet);
begin
  inherited;
  if not qrySubCategoria.Active or (qrySubCategorianCdSubCategoria.Value = 0) then
  begin
      ShowMessage('Selecione uma sub categoria antes de gravar o segmento.') ;
      abort ;
  end ;

  qryMarcaSubnCdSubCategoria.Value := qrySubCategorianCdSubCategoria.Value ;

end;

procedure TfrmDepartamento.DBGridEh4KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMarcaSub.State = dsBrowse) then
             qryMarcaSub.Insert ;


        if (qryMarcaSub.State = dsInsert) or (qryMarcaSub.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(49);

            If (nPK > 0) then
            begin
                qryMarcaSubnCdMarca.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDepartamento.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);

            If (nPK > 0) then
            begin
                qryMasternCdStatus.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDepartamento.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMastercNmDepartamento.Value = '') then
  begin
      ShowMessage('Informe a descri��o do departamento.') ;
      DBEdit2.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdGrupoProduto.Value = 0) then
  begin
      ShowMessage('Informe o grupo de produtos.') ;
      DBEdit5.SetFocus ;
      abort ;
  end ;

  if (qryMaster.State = dsInsert) then
      qryMasternCdStatus.Value := 1 ;

  inherited;

end;

procedure TfrmDepartamento.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(69);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDepartamento.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryCategoria.Close ;
  qryCategoria.Parameters.ParamByName('nPK').Value := qryMasternCdDepartamento.Value ;
  qryCategoria.Open ;

end;

procedure TfrmDepartamento.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

end;

procedure TfrmDepartamento.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmDepartamento_SubCategoria;
begin
  inherited;

  if (qryCategoria.Active) and (qryCategorianCdCategoria.Value > 0) then
  begin

      objForm := TfrmDepartamento_SubCategoria.Create(nil);

      objForm.nCdCategoria := qryCategorianCdCategoria.Value ;
      showForm(objForm,true);
  end ;
end;

initialization
    RegisterClass(tFrmDepartamento) ;

end.
