inherited frmSugerirCheques: TfrmSugerirCheques
  Left = 310
  Top = 248
  Width = 609
  Height = 147
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Sugest'#227'o de Cheques'
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 593
    Height = 82
  end
  object Label4: TLabel [1]
    Left = 201
    Top = 56
    Width = 85
    Height = 13
    Caption = 'Tipo de Simula'#231#227'o'
  end
  object Label5: TLabel [2]
    Left = 320
    Top = 56
    Width = 260
    Height = 13
    Caption = '1= do menor para o maior / 2= do maior para o menor'
  end
  object Label6: TLabel [3]
    Left = 208
    Top = 80
    Width = 81
    Height = 13
    Caption = 'Valor Pagamento'
  end
  inherited ToolBar1: TToolBar
    Width = 593
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object RadioGroup1: TRadioGroup [5]
    Left = 8
    Top = 40
    Width = 185
    Height = 57
    Caption = 'Filtro'
    Enabled = False
    ItemIndex = 0
    Items.Strings = (
      'Todos disponiveis para dep'#243'sito'
      'Simula'#231#227'o de Pagamento')
    TabOrder = 1
  end
  object edtTipoSimulacao: TMaskEdit [6]
    Left = 296
    Top = 48
    Width = 17
    Height = 21
    Enabled = False
    EditMask = '!#;0;_'
    MaxLength = 1
    TabOrder = 2
  end
  object edtValorPagamento: TMaskEdit [7]
    Left = 296
    Top = 72
    Width = 97
    Height = 21
    Enabled = False
    TabOrder = 3
    OnExit = edtValorPagamentoExit
  end
  inherited ImageList1: TImageList
    Left = 448
    Top = 24
  end
  object usp_Resultado: TADOStoredProc
    Connection = frmMenu.Connection
    CursorType = ctStatic
    ProcedureName = 'SP_CONSULTA_CHEQUE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@cTipoConsulta'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cCNPJCPF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 14
        Value = Null
      end
      item
        Name = '@iNrCheque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtDeposito'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@cTpSimulacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end
      item
        Name = '@nValPagamento'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nCdLoteLiq'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 416
    Top = 24
    object usp_ResultadocConta: TStringField
      DisplayLabel = 'Dados Conta Cheque'
      FieldName = 'cConta'
      ReadOnly = True
      Size = 49
    end
    object usp_ResultadoiNrCheque: TIntegerField
      DisplayLabel = 'Nr. Cheque'
      FieldName = 'iNrCheque'
    end
    object usp_ResultadocCNPJCPF: TStringField
      DisplayLabel = 'CNPJ/CPF Emissor'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object usp_ResultadonValCheque: TBCDField
      DisplayLabel = 'Valor'
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object usp_ResultadodDtDeposito: TDateTimeField
      DisplayLabel = 'Data Dep'#243'sito'
      FieldName = 'dDtDeposito'
      ReadOnly = True
    end
    object usp_ResultadocNmTerceiro: TStringField
      DisplayLabel = 'Terceiro Respons'#225'vel'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object usp_ResultadocNmTerceiroPort: TStringField
      DisplayLabel = 'Portador'
      FieldName = 'cNmTerceiroPort'
      ReadOnly = True
      Size = 50
    end
    object usp_ResultadodDtRemessaPort: TDateTimeField
      DisplayLabel = 'Data Rem. Portador'
      FieldName = 'dDtRemessaPort'
      ReadOnly = True
    end
  end
  object qryChequeLote: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM ChequeLoteLiq')
    Left = 488
    Top = 24
    object qryChequeLotenCdChequeLoteLiq: TAutoIncField
      FieldName = 'nCdChequeLoteLiq'
      ReadOnly = True
    end
    object qryChequeLotenCdLoteLiq: TIntegerField
      FieldName = 'nCdLoteLiq'
    end
    object qryChequeLotenCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryChequeLotecAgencia: TStringField
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryChequeLotecConta: TStringField
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryChequeLotecDigito: TStringField
      FieldName = 'cDigito'
      FixedChar = True
      Size = 1
    end
    object qryChequeLotecCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequeLotenValCheque: TBCDField
      FieldName = 'nValCheque'
      Precision = 12
      Size = 2
    end
    object qryChequeLotedDtDeposito: TDateTimeField
      FieldName = 'dDtDeposito'
    end
    object qryChequeLoteiNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
  end
end
