inherited frmMetaMensal: TfrmMetaMensal
  Left = 39
  Top = 32
  Width = 1167
  Height = 650
  Caption = 'Distribui'#231#227'o de Meta - Mensal'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 97
    Width = 1151
    Height = 517
  end
  inherited ToolBar1: TToolBar
    Width = 1151
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1151
    Height = 68
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 50
      Top = 20
      Width = 41
      Height = 13
      Alignment = taRightJustify
      Caption = 'Empresa'
    end
    object Label3: TLabel
      Tag = 1
      Left = 22
      Top = 48
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#234's/Ano Meta'
    end
    object edtMesAno: TMaskEdit
      Left = 96
      Top = 40
      Width = 65
      Height = 21
      EditMask = '99/9999'
      MaxLength = 7
      TabOrder = 1
      Text = '  /    '
      OnChange = edtMesAnoChange
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 168
      Top = 12
      Width = 385
      Height = 21
      DataField = 'cNmEmpresa'
      DataSource = dsEmpresa
      TabOrder = 4
    end
    object cxButton1: TcxButton
      Left = 168
      Top = 36
      Width = 105
      Height = 25
      Caption = 'Exibir Meta'
      TabOrder = 2
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000C6D6DEC6948C
        C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
        8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
        D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
        00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
        FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
        948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
        FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
        C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
        FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
        948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
        8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
        D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
      LookAndFeel.NativeStyle = True
    end
    object cxButton2: TcxButton
      Left = 288
      Top = 36
      Width = 105
      Height = 25
      Caption = 'Nova Meta'
      TabOrder = 3
      OnClick = cxButton2Click
      Glyph.Data = {
        2E020000424D2E0200000000000036000000280000000C0000000E0000000100
        180000000000F801000000000000000000000000000000000000EEFEFEEEFEFE
        EEFEFEEEFEFE9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F0000
        000000000000000000000000000000000000000000000000000000000000009C
        8C6F000000FFFFFFC0928FC0928FC0928FC0928FC0928FC0928FC0928FC0928F
        0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9F9EED9F9EED9F9EE
        D9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9F9EED9F9
        EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED9
        F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9EED9F9EE
        D9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFFF9EED9F9
        EED9F9EED9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F000000FFFFFF
        F9EED9F9EED9F9EED9F9EED9F9EED9F9EED9F9EED9C0928F0000009C8C6F0000
        00FFFFFFF9EED9F9EED9F9EED9F9EED90000000000000000000000000000009C
        8C6F000000FFFFFFF9EED9F9EED9F9EED9F9EED90000009C8C6F9C8C6F9C8C6F
        000000EEFEFE000000FFFFFFF9EED9F9EED9F9EED9F9EED90000009C8C6F9C8C
        6F000000EEFEFEEEFEFE000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000009C
        8C6F000000EEFEFEEEFEFEEEFEFE000000000000000000000000000000000000
        000000000000EEFEFEEEFEFEEEFEFEEEFEFE}
      LookAndFeel.NativeStyle = True
    end
    object edtEmpresa: TMaskEdit
      Left = 96
      Top = 12
      Width = 65
      Height = 21
      EditMask = '########;1; '
      MaxLength = 8
      TabOrder = 0
      Text = '        '
      OnChange = edtEmpresaChange
      OnExit = edtEmpresaExit
      OnKeyDown = edtEmpresaKeyDown
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 97
    Width = 1151
    Height = 517
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 513
    ClientRectLeft = 4
    ClientRectRight = 1147
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Meta Mensal'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1143
        Height = 489
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsMetaMes
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        STFilter.InstantApply = False
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh1DblClick
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdMetaMes'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'cNmLoja'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nValMetaMinima'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValMeta'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValVendaMediaPrev'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nItensPorVendaPrev'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValItemMedioPrev'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValVendaHoraPrev'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nTxConversaoPrev'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'iTotalHorasPrev'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 656
    Top = 136
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 7
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa  '
      '  FROM Empresa'
      ' WHERE nCdEmpresa =:nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE '
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa '
      '                 AND UE.nCdUsuario = :nCdUsuario)')
    Left = 360
    Top = 304
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 376
    Top = 336
  end
  object qryMetaMes: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryMetaMesBeforePost
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'iMes'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 1
      end
      item
        Name = 'iAno'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 2010
      end
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Size = -1
        Value = 8
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa       int'
      '       ,@nCdLoja          int'
      '       ,@nCdMetaMes       int'
      '       ,@nCdUsuario       int'
      '       ,@nCdMetaSemana    int'
      ''
      '       ,@iMes             int'
      '       ,@iAno             int'
      ''
      '       ,@dDtInicial       datetime'
      '       ,@dDtFinal         datetime'
      ''
      'SET @nCdEmpresa       = :nCdEmpresa'
      'SET @iMes             = :iMes'
      'SET @iAno             = :iAno'
      'SET @nCdUsuario       = :nCdUsuario'
      ''
      'IF (@iMes < 12)'
      'BEGIN'
      
        '    SET @dDtInicial = (Convert(datetime,'#39'01/'#39' + dbo.fn_ZeroEsque' +
        'rda(@iMes,2) + '#39'/'#39' + (Convert(char(4),@iAno)),103))'
      
        '    SET @dDtFinal   = (Convert(datetime,'#39'01/'#39' + dbo.fn_ZeroEsque' +
        'rda(@iMes + 1,2) + '#39'/'#39' + (Convert(char(4),@iAno)),103) - 1)'
      'END'
      ''
      'IF (@iMes = 12)'
      'BEGIN'
      
        '    SET @dDtInicial = (Convert(datetime,'#39'01/'#39' + dbo.fn_ZeroEsque' +
        'rda(@iMes,2) + '#39'/'#39' + (Convert(char(4),@iAno)),103))'
      
        '    SET @dDtFinal   = (Convert(datetime,'#39'01/'#39' + dbo.fn_ZeroEsque' +
        'rda(1,2) + '#39'/'#39' + (Convert(char(4),@iAno + 1)),103) - 1)'
      'END'
      ''
      'DECLARE curAux CURSOR'
      '        FOR SELECT nCdLoja'
      '              FROM Loja'
      '             WHERE nCdEmpresa = @nCdEmpresa'
      ''
      '    OPEN curAux'
      ''
      '    FETCH NEXT'
      '     FROM curAux'
      '     INTO @nCdLoja'
      ''
      '    WHILE (@@FETCH_STATUS = 0)'
      '    BEGIN'
      '        IF (NOT EXISTS(SELECT 1'
      '                         FROM MetaMes'
      '                        WHERE nCdLoja    = @nCdLoja'
      '                          AND nCdEmpresa = @nCdEmpresa'
      '                          AND iMes       = @iMes'
      '                          AND iAno       = @iAno))'
      '        BEGIN'
      ''
      '            Set @nCdMetaMes = NULL'
      ''
      #9#9'    EXEC usp_ProximoID '#39'METAMES'#39
      #9#9'                      ,@nCdMetaMes OUTPUT'
      ''
      '            INSERT INTO MetaMes (nCdMetaMes'
      '                                ,nCdLoja'
      '                                ,nCdEmpresa'
      '                                ,iMes'
      '                                ,iAno'
      '                                ,dDtInicial'
      '                                ,dDtFinal'
      '                                ,dDtCad'
      '                                ,nCdUsuarioUltAlt'
      '                                ,dDtUltAlt)'
      '                 VALUES (@nCdMetaMes'
      '                        ,@nCdLoja'
      '                        ,@nCdEmpresa'
      '                        ,@iMes'
      '                        ,@iAno'
      '                        ,@dDtInicial'
      '                        ,@dDtFinal'
      '                        ,GetDate()'
      '                        ,@nCdUsuario'
      '                        ,GetDate())'
      ''
      '        END'
      ''
      '    FETCH NEXT'
      '     FROM curAux'
      '     INTO @nCdLoja'
      ''
      'END'
      ''
      'CLOSE curAux'
      'DEALLOCATE curAux'
      ''
      'SELECT nCdMetaMes'
      '      ,Loja.nCdLoja'
      '      ,Loja.cNmLoja'
      '      ,MetaMes.nValMetaMinima'
      '      ,MetaMes.nValMeta'
      '      ,MetaMes.nValVendaMediaPrev'
      '      ,MetaMes.nItensPorVendaPrev'
      '      ,MetaMes.nValItemMedioPrev'
      '      ,MetaMes.nValVendaHoraPrev'
      '      ,MetaMes.nTxConversaoPrev'
      '      ,MetaMes.iTotalHorasPrev'
      '      ,MetaMes.dDtInicial'
      '      ,MetaMes.dDtFinal'
      '      ,MetaMes.nCdUsuarioUltAlt'
      '      ,MetaMes.dDtUltAlt'
      '  FROM MetaMes'
      '       INNER JOIN Loja ON Loja.nCdLoja = MetaMes.nCdLoja'
      ' WHERE MetaMes.nCdEmpresa = @nCdEmpresa'
      '   AND iMes               = @iMes'
      '   AND iAno               = @iAno')
    Left = 408
    Top = 304
    object qryMetaMesnCdMetaMes: TIntegerField
      FieldName = 'nCdMetaMes'
    end
    object qryMetaMesnCdLoja: TIntegerField
      DisplayLabel = 'Loja|C'#243'd.'
      FieldName = 'nCdLoja'
    end
    object qryMetaMescNmLoja: TStringField
      DisplayLabel = 'Loja|Nome Loja'
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryMetaMesnValMetaMinima: TBCDField
      DisplayLabel = 'Meta em Valor|Minima'
      FieldName = 'nValMetaMinima'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaMesnValMeta: TBCDField
      DisplayLabel = 'Meta em Valor|Meta'
      FieldName = 'nValMeta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaMesnValVendaMediaPrev: TBCDField
      DisplayLabel = 'Indicadores de Meta|Venda M'#233'dia'
      FieldName = 'nValVendaMediaPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaMesnItensPorVendaPrev: TBCDField
      DisplayLabel = 'Indicadores de Meta|Itens por Venda'
      FieldName = 'nItensPorVendaPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaMesnValItemMedioPrev: TBCDField
      DisplayLabel = 'Indicadores de Meta|Valor m'#233'dio Item'
      FieldName = 'nValItemMedioPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaMesnValVendaHoraPrev: TBCDField
      DisplayLabel = 'Indicadores de Meta|Venda p/ Hora'
      FieldName = 'nValVendaHoraPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaMesnTxConversaoPrev: TBCDField
      DisplayLabel = 'Indicadores de Meta|Taxa Convers'#227'o'
      FieldName = 'nTxConversaoPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMetaMesdDtInicial: TDateTimeField
      FieldName = 'dDtInicial'
    end
    object qryMetaMesdDtFinal: TDateTimeField
      FieldName = 'dDtFinal'
    end
    object qryMetaMesnCdUsuarioUltAlt: TIntegerField
      FieldName = 'nCdUsuarioUltAlt'
    end
    object qryMetaMesdDtUltAlt: TDateTimeField
      FieldName = 'dDtUltAlt'
    end
    object qryMetaMesiTotalHorasPrev: TBCDField
      DisplayLabel = 'Indicadores de Meta|Horas de Venda'
      FieldName = 'iTotalHorasPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsMetaMes: TDataSource
    DataSet = qryMetaMes
    Left = 424
    Top = 336
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 492
    Top = 305
  end
  object qryInsereSemana: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'iSemana'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtInicial'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtFinal'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdMetaMes'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdMetaSemana int'
      '       ,@iSemana       int'
      '       ,@dDtInicial    varchar(10)'
      '       ,@dDtFinal      varchar(10)'
      '       ,@nCdMetaMes    int'
      ''
      'SET @iSemana    = :iSemana'
      'SET @dDtInicial = :dDtInicial'
      'SET @dDtFinal   = :dDtFinal'
      'SET @nCdMetaMes = :nCdMetaMes'
      ''
      'EXEC usp_ProximoID '#39'METASEMANA'#39
      '                  ,@nCdMetaSemana OUTPUT'
      ''
      'INSERT INTO MetaSemana(nCdMetaSemana'
      '                      ,nCdMetaMes'
      '                      ,iSemana'
      '                      ,dDtInicial'
      '                      ,dDtFinal)'
      '                VALUES(@nCdMetaSemana'
      '                      ,@nCdMetaMes'
      '                      ,@iSemana'
      '                      ,Convert(datetime,@dDtInicial,103)'
      '                      ,Convert(datetime,@dDtFinal,103))')
    Left = 452
    Top = 305
  end
end
