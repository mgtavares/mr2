unit fGrupoTotalCC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DBGridEhGrouping, cxLookAndFeelPainters,
  StdCtrls, cxButtons, GridsEh, DBGridEh, cxPC, cxControls, Mask, DBCtrls,
  ImgList, DB, ADODB, ComCtrls, ToolWin, ExtCtrls;

type
  TfrmGrupoTotalCC = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoTotalCC: TIntegerField;
    qryMastercNmGrupoTotalCC: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    btInsereTotal: TcxButton;
    btExcluiTotal: TcxButton;
    qryColunaGrupoTotalCC: TADOQuery;
    qryColunaGrupoTotalCCnCdColunaGrupoTotalCC: TIntegerField;
    qryColunaGrupoTotalCCnCdGrupoTotalCC: TIntegerField;
    qryColunaGrupoTotalCCcNmColunaGrupoTotalCC: TStringField;
    DataSource1: TDataSource;
    procedure btInsereTotalClick(Sender: TObject);
    procedure btExcluiTotalClick(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoTotalCC: TfrmGrupoTotalCC;

implementation

uses fColunaGrupoTotalCC, fMenu;

{$R *.dfm}

procedure TfrmGrupoTotalCC.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmColunaGrupoTotalCC;
begin
  inherited;

  if not(qryMaster.Active) then
      exit;

  if (qryColunaGrupoTotalCCnCdColunaGrupoTotalCC.Value = 0) then
      exit;

  objForm := TfrmColunaGrupoTotalCC.Create(nil);
  objForm.RegistroTotalizador(qryColunaGrupoTotalCCnCdColunaGrupoTotalCC.Value,0);

  qryColunaGrupoTotalCC.Close;
  PosicionaQuery(qryColunaGrupoTotalCC,qryMasternCdGrupoTotalCC.AsString);

end;

procedure TfrmGrupoTotalCC.btInsereTotalClick(Sender: TObject);
var
  objForm : TfrmColunaGrupoTotalCC;
begin
  inherited;

  if not(qryMaster.Active) then
      exit;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;

  try
      try

          objForm := TfrmColunaGrupoTotalCC.Create(nil);
          objForm.nCdGrupoTotalCC := qryMasternCdGrupoTotalCC.Value;
          objForm.RegistroTotalizador(0,1); {-- passa valor 0 pois ele ir� inserir o totalizador --}

      except
          MensagemErro('Erro no processamento.');
          raise;
      end;
  finally
      FreeAndNil(objForm);
  end;

  qryColunaGrupoTotalCC.Close;
  PosicionaQuery(qryColunaGrupoTotalCC,qryMasternCdGrupoTotalCC.AsString);
end;

procedure TfrmGrupoTotalCC.btExcluiTotalClick(Sender: TObject);
var
  objForm : TfrmColunaGrupoTotalCC;
begin
  inherited;

  if not(qryMaster.Active) then
      exit;

  if (qryColunaGrupoTotalCCnCdColunaGrupoTotalCC.Value = 0) then
  begin
      MensagemAlerta('Nenhum totalizador para excluir.');
      exit;
  end;

  frmMenu.Connection.BeginTrans;
  
  try
      try

          objForm := TfrmColunaGrupoTotalCC.Create(nil);
          objForm.RegistroTotalizador(qryColunaGrupoTotalCCnCdColunaGrupoTotalCC.Value,2);

      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          raise;
      end;
  finally
      FreeAndNil(objForm);
  end;

  frmMenu.Connection.CommitTrans;

  qryColunaGrupoTotalCC.Close;
  PosicionaQuery(qryColunaGrupoTotalCC,qryMasternCdGrupoTotalCC.AsString);
end;

procedure TfrmGrupoTotalCC.FormCreate(Sender: TObject);
begin
  inherited;

  nCdTabelaSistema  := 319;
  cNmTabelaMaster   := 'GRUPOTOTALCC';
  nCdConsultaPadrao := 212;
  bLimpaAposSalvar  := False;
end;

procedure TfrmGrupoTotalCC.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryColunaGrupoTotalCC.Close;
end;

procedure TfrmGrupoTotalCC.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryColunaGrupoTotalCC.Close;
  PosicionaQuery(qryColunaGrupoTotalCC,qryMasternCdGrupoTotalCC.AsString);
end;

procedure TfrmGrupoTotalCC.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
end;

procedure TfrmGrupoTotalCC.btSalvarClick(Sender: TObject);
begin
  inherited;

  btCancelar.Click;
end;

procedure TfrmGrupoTotalCC.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;
end;

procedure TfrmGrupoTotalCC.btExcluirClick(Sender: TObject);
var
  objForm : TfrmColunaGrupoTotalCC;
begin

  {inherited;}

  if (MessageDlg('Confirma a exclus�o deste registro ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;
  
  try
      try

          objForm := TfrmColunaGrupoTotalCC.Create(nil);

          qryColunaGrupoTotalCC.First;

          while not qryColunaGrupoTotalCC.Eof do
          begin

              objForm.qryExcluirDpCC.Close;
              objForm.qryExcluirDpCC.Parameters.ParamByName('nCdColunaGrupoTotalCC').Value := qryColunaGrupoTotalCCnCdColunaGrupoTotalCC.Value;
              objForm.qryExcluirDpCC.ExecSQL;

              qryColunaGrupoTotalCC.Delete;
          end;

          qryMaster.Delete;

          btCancelar.Click;

      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          raise;
      end;
  finally
      FreeAndNil(objForm);
  end;

  frmMenu.Connection.CommitTrans;

end;

initialization
  RegisterClass(TfrmGrupoTotalCC);

end.
