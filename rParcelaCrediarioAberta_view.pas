unit rParcelaCrediarioAberta_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptParcelaCrediarioAberta_view = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape1: TQRShape;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText6: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel18: TQRLabel;
    QRShape3: TQRShape;
    QRExpr9: TQRExpr;
    QRDBText14: TQRDBText;
    QRLabel25: TQRLabel;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRLabel26: TQRLabel;
    QRDBText17: TQRDBText;
    QRLabel27: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape7: TQRShape;
    QRLabel4: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText3: TQRDBText;
    usp_RelatorionCdEmpresa: TStringField;
    usp_RelatorionCdLojaTit: TStringField;
    usp_RelatoriocNrTit: TStringField;
    usp_RelatorioiParcela: TIntegerField;
    usp_RelatorionCdTerceiro: TIntegerField;
    usp_RelatoriocNmCliente: TStringField;
    usp_RelatoriodDtEmissao: TDateTimeField;
    usp_RelatoriodDtVenc: TDateTimeField;
    usp_RelatorionValTit: TFloatField;
    usp_RelatorionValJuro: TFloatField;
    usp_RelatorionSaldoTit: TFloatField;
    usp_RelatoriocRenegociacao: TStringField;
    QRDBText4: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr5: TQRExpr;
    QRShape2: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel6: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptParcelaCrediarioAberta_view: TrptParcelaCrediarioAberta_view;

implementation

{$R *.dfm}

end.
