unit fTerceiro_PosicaoReceber;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DB, ADODB, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, cxPC;

type
  TfrmTerceiro_PosicaoReceber = class(TfrmProcesso_Padrao)
    qryTitulosReceber: TADOQuery;
    DataSource1: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    qryTitulosRecebernCdTitulo: TIntegerField;
    qryTitulosRecebernCdEmpresa: TIntegerField;
    qryTitulosRecebercNrTit: TStringField;
    qryTitulosReceberiParcela: TIntegerField;
    qryTitulosReceberdDtEmissao: TDateTimeField;
    qryTitulosReceberdDtVenc: TDateTimeField;
    qryTitulosRecebernValTit: TBCDField;
    qryTitulosRecebernSaldoTit: TBCDField;
    qryTitulosRecebernValJuro: TBCDField;
    qryTitulosRecebernValDesconto: TBCDField;
    qryTitulosReceberdDtCalcJuro: TDateTimeField;
    qryTitulosRecebercNrNf: TStringField;
    qryTitulosRecebernCdLojaTit: TIntegerField;
    qryTitulosRecebercNmEspTit: TStringField;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1nValJuro: TcxGridDBColumn;
    cxGrid1DBTableView1nValDesconto: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNf: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLojaTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    cxTabSheet2: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridDBColumn13: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    procedure exibeTitulos(nCdTerceiro:integer; bSomenteAtrasados:boolean);
    procedure atualizaDataSet(nCdTerceiro:integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cxPageControl1Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTerceiro_PosicaoReceber: TfrmTerceiro_PosicaoReceber;

implementation

{$R *.dfm}

procedure TfrmTerceiro_PosicaoReceber.exibeTitulos(nCdTerceiro: integer;
  bSomenteAtrasados: boolean);
begin

    qryTitulosReceber.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro ;

    if (bSomenteAtrasados) then
        cxPageControl1.ActivePageIndex := 1
    else cxPageControl1.ActivePageIndex := 0;

    atualizaDataSet(qryTitulosReceber.Parameters.ParamByName('nCdTerceiro').Value);

    if not (Self.Showing) then
        Self.ShowModal ;

end;

procedure TfrmTerceiro_PosicaoReceber.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

  qryTitulosReceber.Close;

  inherited;

end;

procedure TfrmTerceiro_PosicaoReceber.atualizaDataSet(
  nCdTerceiro: integer);
begin

    qryTitulosReceber.Close;
    qryTitulosReceber.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro;
    qryTitulosReceber.Parameters.ParamByName('nSituacao').Value   := cxPageControl1.ActivePageIndex;
    qryTitulosReceber.Open;

end;

procedure TfrmTerceiro_PosicaoReceber.cxPageControl1Change(
  Sender: TObject);
begin
    inherited;

    qryTitulosReceber.Close;
    qryTitulosReceber.Parameters.ParamByName('nCdTerceiro').Value := qryTitulosReceber.Parameters.ParamByName('nCdTerceiro').Value;
    qryTitulosReceber.Parameters.ParamByName('nSituacao').Value   := cxPageControl1.ActivePageIndex;
    qryTitulosReceber.Open;

end;

end.
