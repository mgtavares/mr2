unit fGrupoEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmGrupoEstoque = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoEstoque: TIntegerField;
    qryMastercNmGrupoEstoque: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoEstoque: TfrmGrupoEstoque;

implementation

{$R *.dfm}

procedure TfrmGrupoEstoque.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOESTOQUE' ;
  nCdTabelaSistema  := 39 ;
  nCdConsultaPadrao := 85 ;
end;

procedure TfrmGrupoEstoque.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmGrupoEstoque.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DbEdit2.Text) = '') then
  begin
      ShowMessage('Informe a descri��o do grupo de estoque.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  inherited;

end;

initialization
    RegisterClass(TfrmGrupoEstoque) ;

end.
