inherited frmGerarRequisicaoOP_ExibeRequisicoes: TfrmGerarRequisicaoOP_ExibeRequisicoes
  Left = 162
  Width = 819
  BorderIcons = [biSystemMenu]
  Caption = 'Requisi'#231#245'es da Etapa da Ordem de Produ'#231#227'o'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 193
    Width = 803
    Height = 271
  end
  inherited ToolBar1: TToolBar
    Width = 803
    ButtonWidth = 119
    inherited ToolButton1: TToolButton
      Caption = 'Imprimir Requisi'#231#227'o'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 119
    end
    inherited ToolButton2: TToolButton
      Left = 127
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 803
    Height = 164
    ActivePage = cxTabSheet1
    Align = alTop
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 160
    ClientRectLeft = 4
    ClientRectRight = 799
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Requisi'#231#245'es'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 795
        Height = 136
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsRequisicoes
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdRequisicao'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'dDtRequisicao'
            Footers = <>
            ReadOnly = True
            Width = 171
          end
          item
            EditButtons = <>
            FieldName = 'cNmSolicitante'
            Footers = <>
            ReadOnly = True
            Width = 212
          end
          item
            EditButtons = <>
            FieldName = 'cNmTabStatusRequis'
            Footers = <>
            ReadOnly = True
            Width = 131
          end
          item
            EditButtons = <>
            FieldName = 'nValRequisicao'
            Footers = <>
            ReadOnly = True
            Width = 93
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object cxPageControl2: TcxPageControl [3]
    Left = 0
    Top = 193
    Width = 803
    Height = 271
    ActivePage = cxTabSheet2
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 267
    ClientRectLeft = 4
    ClientRectRight = 799
    ClientRectTop = 24
    object cxTabSheet2: TcxTabSheet
      Caption = 'Itens da Requisi'#231#227'o'
      ImageIndex = 0
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 795
        Height = 243
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsItemRequisicao
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            ReadOnly = True
            Width = 46
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            ReadOnly = True
            Width = 177
          end
          item
            EditButtons = <>
            FieldName = 'cReferencia'
            Footers = <>
            ReadOnly = True
            Width = 66
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeReq'
            Footers = <>
            ReadOnly = True
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cUnidadeMedida'
            Footers = <>
            ReadOnly = True
            Width = 27
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeAtend'
            Footers = <>
            ReadOnly = True
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeCanc'
            Footers = <>
            ReadOnly = True
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nSaldoReq'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footers = <>
            ReadOnly = True
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nValTotal'
            Footers = <>
            ReadOnly = True
            Width = 65
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF00FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009E9E
      9E00656464006564640065646400656464006564640065646400656464006564
      6400898A8900B9BABA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000D5D5D5005157
      570031333200484E4E00313332003D4242003D4242003D4242003D424200191D
      230051575700C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000000000008C908F00191D
      230031333200313332003133320031333200313332003133320031333200191D
      230004050600898A890000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      840000008400000084000000000000000000000000000000000065646400898A
      8900DADDD700B9BABA00BEBEBE00BEBEBE00BEBEBE00BEBEBE00BEBABC00CCCC
      CC00515757006564640000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000000000000717272003D42
      4200898A8900777B7B007B8585007B8585007B8585007B8585007B8585009392
      92003D424200777B7B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000B6B6B6003133
      32007B858500AEAEAE00A4A8A800A4A8A800A4A8A800A4A8A800A4A8A800484E
      4E0031333200B9BABA0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000000000000D5D5D500484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00898A
      8900484E4E00C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE007B85
      85003D4242000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF000000840000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE00FCFEFE00D5D5D5007172
      72003D4242000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE007B858500191D2300191D
      230051575700C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000CCCCCC00484E
      4E00C6C6C600FCFEFE00FCFEFE00FCFEFE00FCFEFE0071727200484E4E003D42
      420093929200C6C6C60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000CCCCCC00484E
      4E0031333200575D5E005157570051575700575D5E00191D2300191D23009392
      9200CCCCCC00BEBEBE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B6B6
      B6009E9E9E009E9E9E009E9E9E009E9E9E009E9E9E00A4A8A800AEAEAE00C6C6
      C600BEBEBE00BEBEBE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFFF0000
      FE00C003E003000000008001C003000000008001C003000000008001C0030000
      00008001C003000000008001C003000000008001C003000000008001C0070000
      00008001C007000000008001C003000000018001C003000000038001C0030000
      0077C003E0030000007FFFFFFFFF0000}
  end
  object qryRequisicoes: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryRequisicoesAfterScroll
    Parameters = <
      item
        Name = 'nCdOrdemProducao'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEtapaOrdemProducao'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdOrdemProducao      int'
      '       ,@nCdEtapaOrdemProducao int'
      ''
      'Set @nCdOrdemProducao      = :nCdOrdemProducao'
      'Set @nCdEtapaOrdemProducao = :nCdEtapaOrdemProducao'
      ''
      'SELECT *'
      '  FROM Requisicao'
      
        '       INNER JOIN TabStatusRequis ON TabStatusRequis.nCdTabStatu' +
        'sRequis = Requisicao.nCdTabStatusRequis'
      ' WHERE nCdOrdemProducao      = @nCdOrdemProducao'
      
        '   AND ( ( @nCdEtapaOrdemProducao = 0 ) OR ( nCdEtapaOrdemProduc' +
        'ao = @nCdEtapaOrdemProducao ) )')
    Left = 332
    Top = 117
    object qryRequisicoesnCdRequisicao: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|N'#250'm. Req.'
      FieldName = 'nCdRequisicao'
    end
    object qryRequisicoesnCdEmpresa: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'nCdEmpresa'
    end
    object qryRequisicoesnCdLoja: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'nCdLoja'
    end
    object qryRequisicoesnCdTipoRequisicao: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'nCdTipoRequisicao'
    end
    object qryRequisicoesdDtRequisicao: TDateTimeField
      DisplayLabel = 'Requisi'#231#245'es da OP|Data Requisi'#231#227'o'
      FieldName = 'dDtRequisicao'
    end
    object qryRequisicoesdDtAutor: TDateTimeField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'dDtAutor'
    end
    object qryRequisicoesnCdUsuarioAutor: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'nCdUsuarioAutor'
    end
    object qryRequisicoescNmSolicitante: TStringField
      DisplayLabel = 'Requisi'#231#245'es da OP|Solicitante'
      FieldName = 'cNmSolicitante'
      Size = 30
    end
    object qryRequisicoesnCdTabTipoRequis: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'nCdTabTipoRequis'
    end
    object qryRequisicoescNrFormulario: TStringField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'cNrFormulario'
      Size = 10
    end
    object qryRequisicoesnCdTabStatusRequis: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'nCdTabStatusRequis'
    end
    object qryRequisicoescOBS: TMemoField
      DisplayLabel = 'Requisi'#231#245'es da OP|Observa'#231#227'o'
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
    object qryRequisicoesnCdSetor: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'nCdSetor'
    end
    object qryRequisicoesnCdCC: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'nCdCC'
    end
    object qryRequisicoescFlgAutomatica: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'cFlgAutomatica'
    end
    object qryRequisicoesnValRequisicao: TBCDField
      DisplayLabel = 'Requisi'#231#245'es da OP|Valor Requisi'#231#227'o'
      FieldName = 'nValRequisicao'
      Precision = 12
      Size = 2
    end
    object qryRequisicoesdDtFinalizacao: TDateTimeField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'dDtFinalizacao'
    end
    object qryRequisicoesnCdServidorOrigem: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'nCdServidorOrigem'
    end
    object qryRequisicoesdDtReplicacao: TDateTimeField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'dDtReplicacao'
    end
    object qryRequisicoesnCdOrdemProducao: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'nCdOrdemProducao'
    end
    object qryRequisicoesnCdEtapaOrdemProducao: TIntegerField
      DisplayLabel = 'Requisi'#231#245'es da OP|'
      FieldName = 'nCdEtapaOrdemProducao'
    end
    object qryRequisicoescNmTabStatusRequis: TStringField
      DisplayLabel = 'Requisi'#231#245'es da OP|Status'
      FieldName = 'cNmTabStatusRequis'
      Size = 50
    end
  end
  object dsRequisicoes: TDataSource
    DataSet = qryRequisicoes
    Left = 372
    Top = 117
  end
  object qryItemRequisicao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Produto.nCdProduto'
      '      ,cNmItem '
      '      ,Produto.cReferencia'
      '      ,nQtdeReq'
      '      ,ItemRequisicao.cUnidadeMedida'
      '      ,nQtdeAtend'
      '      ,nQtdeCanc'
      '      ,(nQtdeReq - nQtdeAtend - nQtdeCanc) as nSaldoReq'
      '      ,nValUnitario'
      '      ,nValTotal'
      '  FROM ItemRequisicao'
      
        '       LEFT JOIN Produto ON Produto.nCdProduto = ItemRequisicao.' +
        'nCdProduto'
      ' WHERE ItemRequisicao.nCdRequisicao = :nPK'
      ' ORDER BY cNmItem ')
    Left = 356
    Top = 305
    object qryItemRequisicaonCdProduto: TIntegerField
      DisplayLabel = 'Item|C'#243'd.'
      FieldName = 'nCdProduto'
    end
    object qryItemRequisicaocNmItem: TStringField
      DisplayLabel = 'Item|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 100
    end
    object qryItemRequisicaocReferencia: TStringField
      DisplayLabel = 'Item|Refer'#234'ncia'
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object qryItemRequisicaonQtdeReq: TBCDField
      DisplayLabel = 'Quantidades|Requisitada'
      FieldName = 'nQtdeReq'
      Precision = 12
    end
    object qryItemRequisicaocUnidadeMedida: TStringField
      DisplayLabel = 'Quantidades|U.M'
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryItemRequisicaonQtdeAtend: TBCDField
      DisplayLabel = 'Quantidades|Atendida'
      FieldName = 'nQtdeAtend'
      Precision = 12
    end
    object qryItemRequisicaonQtdeCanc: TBCDField
      DisplayLabel = 'Quantidades|Cancelada'
      FieldName = 'nQtdeCanc'
      Precision = 12
    end
    object qryItemRequisicaonSaldoReq: TBCDField
      DisplayLabel = 'Quantidades|Saldo'
      FieldName = 'nSaldoReq'
      ReadOnly = True
      Precision = 14
    end
    object qryItemRequisicaonValUnitario: TBCDField
      DisplayLabel = 'Custo|Unit'#225'rio'
      FieldName = 'nValUnitario'
      Precision = 14
      Size = 6
    end
    object qryItemRequisicaonValTotal: TBCDField
      DisplayLabel = 'Custo|Total'
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
  end
  object dsItemRequisicao: TDataSource
    DataSet = qryItemRequisicao
    Left = 396
    Top = 305
  end
end
