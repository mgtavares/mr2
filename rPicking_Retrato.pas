unit rPicking_Retrato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, QuickRpt, QRCtrls, jpeg;

type
  TrptPicking_Retrato = class(TForm)
    QuickRep1: TQuickRep;
    qryPedido: TADOQuery;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRGroup1: TQRGroup;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel7: TQRLabel;
    QRBand2: TQRBand;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidonCdTerceiro: TIntegerField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidonCdItemPedido: TIntegerField;
    qryPedidonCdProduto: TIntegerField;
    qryPedidocNmItem: TStringField;
    qryPedidonQtdePed: TBCDField;
    qryPedidonCdTipoItemPed: TIntegerField;
    qryPedidonCdAlaProducao: TIntegerField;
    qryPedidocNmAlaProducao: TStringField;
    qryPedidocComposicao: TStringField;
    QRBand3: TQRBand;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText27: TQRDBText;
    QRLabel4: TQRLabel;
    qryPedidodDtPrevEntIni: TDateTimeField;
    qryPedidodDtPrevEntFim: TDateTimeField;
    QRLabel11: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel13: TQRLabel;
    QRShape4: TQRShape;
    QRTotalPagina: TQRLabel;
    qryPedidocEnderecoEntrega: TStringField;
    QRDBText7: TQRDBText;
    QRLabel1: TQRLabel;
    qryPedidocAnotacao: TMemoField;
    QRLabel10: TQRLabel;
    QRDBText3: TQRDBText;
    QRGroup2: TQRGroup;
    QRDBText11: TQRDBText;
    QRLabel15: TQRLabel;
    QRShape1: TQRShape;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRBand4: TQRBand;
    QRLabel5: TQRLabel;
    QRExpr1: TQRExpr;
    QRShape2: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    qryPedidocNmFantasia: TStringField;
    qryPedidocNmTerceiroRepres: TStringField;
    QRDBText8: TQRDBText;
    QRLabel9: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPicking_Retrato: TrptPicking_Retrato;

implementation

{$R *.dfm}

end.
