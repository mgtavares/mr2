inherited frmMetaApontamentoHora_Vendedor: TfrmMetaApontamentoHora_Vendedor
  Width = 863
  Caption = 'Apontamento de Horas/ Oportunidades'
  Position = poDesktopCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 847
  end
  inherited ToolBar1: TToolBar
    Width = 847
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 847
    Height = 435
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 431
    ClientRectLeft = 4
    ClientRectRight = 843
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Apontamentos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 839
        Height = 407
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsApontamentos
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'cNmVendedor'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 236
          end
          item
            EditButtons = <>
            FieldName = 'iTotalHorasReal'
            Footers = <>
            Width = 110
          end
          item
            EditButtons = <>
            FieldName = 'iOportunidades'
            Footers = <>
            Width = 113
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 504
    Top = 72
  end
  object qryApontamentos: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryApontamentosBeforePost
    OnCalcFields = qryApontamentosCalcFields
    Parameters = <
      item
        Name = 'dDtRef'
        DataType = ftString
        Size = 10
        Value = '10/10/2010'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'iMes'
        DataType = ftString
        Size = 2
        Value = '10'
      end
      item
        Name = 'iAno'
        DataType = ftString
        Size = 4
        Value = '2010'
      end>
    SQL.Strings = (
      'DECLARE @nCdMetaMes         int'
      '       ,@nCdMetaSemana      int'
      '       ,@nCdMetaVendedorDia int'
      '       ,@dDtRef             varchar(10)'
      '       ,@nCdEmpresa         int'
      '       ,@nCdLoja            int'
      '       ,@iMes               int'
      '       ,@iAno               int'
      '       ,@nCdUsuario         int'
      '       ,@nCdMetaVendedor    int'
      ''
      'IF (OBJECT_ID('#39'tempdb..#TempMetaVendedorDia'#39') IS NULL)'
      'BEGIN'
      ' '
      
        '    CREATE TABLE #TempMetaVendedorDia (nCdTempMetaVendedorDia in' +
        't PRIMARY KEY IDENTITY(1,1)'
      
        '                                      ,nCdMetaVendedor        in' +
        't                     not null'
      
        '                                      ,dDtRef                 da' +
        'tetime                not null'
      
        '                                      ,nCdUsuario             in' +
        't                     not null'
      
        '                                      ,iTotalHorasReal        de' +
        'cimal(12,2) default 0 not null'
      
        '                                      ,iOportunidades         in' +
        't           default 0 not null)'
      ''
      'END'
      ''
      'TRUNCATE TABLE #TempMetaVendedorDia'
      ''
      'SET @dDtRef     = :dDtRef'
      'SET @nCdEmpresa = :nCdEmpresa'
      'SET @nCdLoja    = :nCdLoja'
      'SET @iMes       = :iMes'
      'SET @iAno       = :iAno'
      ''
      ''
      'SELECT @nCdMetaMes = nCdMetaMes'
      '  FROM MetaMes '
      ' WHERE nCdEmpresa = @nCdEmpresa '
      '   AND nCdLoja    = @nCdLoja '
      '   AND iMes       = @iMes'
      '   AND iAno       = @iAno'
      ''
      'SELECT @nCdMetaSemana = nCdMetaSemana '
      '  FROM MetaSemana '
      ' WHERE Convert(datetime,@dDtRef,103) >= dDtInicial'
      '   AND Convert(datetime,@dDtRef,103) <= dDtFinal'
      '   AND nCdMetaMes = @nCdMetaMes'
      ''
      'DECLARE curVendedor CURSOR'
      '    FOR SELECT nCdMetaVendedor'
      '              ,nCdUsuario'
      '          FROM MetaVendedor'
      '         WHERE nCdMetaSemana = @nCdMetaSemana'
      ''
      'OPEN curVendedor'
      ''
      'FETCH NEXT'
      ' FROM curVendedor'
      ' INTO @nCdMetaVendedor'
      '     ,@nCdUsuario'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      '        INSERT INTO #TempMetaVendedorDia (nCdMetaVendedor'
      '                                         ,dDtRef'
      '                                         ,nCdUsuario'
      '                                         ,iTotalHorasReal'
      '                                         ,iOportunidades)'
      '                                  SELECT nCdMetaVendedor'
      
        '                                        ,Convert(datetime,@dDtRe' +
        'f,103)'
      '                                        ,@nCdUsuario'
      
        '                                        ,isNull((SELECT iTotalHo' +
        'rasReal FROM MetaVendedorDia WHERE nCdMetaVendedor = @nCdMetaVen' +
        'dedor AND dDtRef = Convert(datetime,@dDtRef,103)),0)'
      
        '                                        ,isNull((SELECT iOportun' +
        'idades  FROM MetaVendedorDia WHERE nCdMetaVendedor = @nCdMetaVen' +
        'dedor AND dDtRef = Convert(datetime,@dDtRef,103)),0)'
      '                                    FROM MetaVendedor'
      
        '                                   WHERE nCdMetaVendedor = @nCdM' +
        'etaVendedor'
      ''
      '    FETCH NEXT'
      '     FROM curVendedor'
      '     INTO @nCdMetaVendedor'
      '         ,@nCdUsuario'
      ''
      'END'
      ''
      'CLOSE curVendedor'
      'DEALLOCATE curVendedor'
      ''
      'SELECT nCdUsuario'
      '      ,iTotalHorasReal'
      '      ,iOportunidades'
      '      ,dDtRef'
      '      ,nCdMetaVendedor'
      '  FROM #TempMetaVendedorDia'
      ' ORDER BY nCdUsuario ASC')
    Left = 268
    Top = 181
    object qryApontamentosnCdUsuario: TIntegerField
      DisplayLabel = 'Vendedor|C'#243'd.'
      FieldName = 'nCdUsuario'
    end
    object qryApontamentoscNmVendedor: TStringField
      DisplayLabel = 'Vendedor|Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmVendedor'
      Size = 50
      Calculated = True
    end
    object qryApontamentosiTotalHorasReal: TBCDField
      DisplayLabel = 'Apontamento|Horas de Venda'
      FieldName = 'iTotalHorasReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryApontamentosiOportunidades: TIntegerField
      DisplayLabel = 'Apontamento|Oportunidades'
      FieldName = 'iOportunidades'
    end
    object qryApontamentosdDtRef: TDateTimeField
      FieldName = 'dDtRef'
    end
    object qryApontamentosnCdMetaVendedor: TIntegerField
      FieldName = 'nCdMetaVendedor'
    end
  end
  object dsApontamentos: TDataSource
    DataSet = qryApontamentos
    Left = 284
    Top = 213
  end
  object qryVendedor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      '             ,cNmUsuario'
      '   FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 332
    Top = 181
    object qryVendedornCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryVendedorcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object qryInsereApontamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMetaVendedor'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtRef'
        Size = -1
        Value = Null
      end
      item
        Name = 'iTotalHorasReal'
        Size = -1
        Value = Null
      end
      item
        Name = 'iOportunidades'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdMetaVendedorDia int'
      '       ,@nCdMetaVendedor    int'
      '       ,@dDtRef             varchar(10)'
      '       ,@iTotalHorasReal    decimal(12,2)'
      '       ,@iTotalHorasPrev    decimal(12,2)'
      '       ,@nPercHorasReal     decimal(12,2)'
      '       ,@iOportunidades     int'
      ''
      'SET @nCdMetaVendedor    = :nCdMetaVendedor'
      'SET @dDtRef             = :dDtRef'
      'SET @iTotalHorasReal    = :iTotalHorasReal'
      'SET @iOportunidades     = :iOportunidades'
      'SET @nPercHorasReal     = 0'
      ''
      
        'IF EXISTS(SELECT 1 FROM MetaVendedorDia WHERE nCdMetaVendedor = ' +
        '@nCdMetaVendedor AND dDtRef = Convert(datetime,@dDtRef,103))'
      'BEGIN'
      ''
      '    UPDATE MetaVendedorDia'
      '       SET iTotalHorasReal = @iTotalHorasReal'
      '          ,iOportunidades  = @iOportunidades'
      '     WHERE nCdMetaVendedor = @nCdMetaVendedor'
      '       AND dDtRef          = Convert(datetime,@dDtRef,103)'
      ''
      'END'
      ''
      
        'IF NOT EXISTS(SELECT 1 FROM MetaVendedorDia WHERE nCdMetaVendedo' +
        'r = @nCdMetaVendedor AND dDtRef = Convert(datetime,@dDtRef,103))'
      'BEGIN'
      ''
      '    EXEC usp_ProximoID '#39'METAVENDEDORDIA'#39
      #9'    '#9'           ,@nCdMetaVendedorDia OUTPUT'
      ''
      '    INSERT INTO MetaVendedorDia (nCdMetaVendedorDia'
      '                                ,nCdMetaVendedor'
      '                                ,dDtRef'
      '                                ,iTotalHorasReal'
      '                                ,iOportunidades)'
      '                          SELECT @nCdMetaVendedorDia'
      '                                ,@nCdMetaVendedor'
      '                                ,Convert(datetime,@dDtRef,103)'
      '                                ,@iTotalHorasReal'
      '                                ,@iOportunidades'
      'END'
      ''
      
        '(SELECT @iTotalHorasPrev = isNull(iTotalHorasPrev,0) FROM MetaVe' +
        'ndedor WHERE nCdMetaVendedor = @nCdMetaVendedor)'
      ''
      'IF (@iTotalHorasPrev > 0)'
      'BEGIN'
      ''
      '    SET @nPercHorasReal = isNull((SELECT SUM(iTotalHorasReal)'
      '                                    FROM MetaVendedorDia'
      
        '                                   WHERE MetaVendedorDia.nCdMeta' +
        'Vendedor = @nCdMetaVendedor),0)/ @iTotalHorasPrev * 100'
      ''
      'END'
      ''
      'UPDATE MetaVendedor'
      '   SET iTotalHorasReal   = (SELECT SUM(iTotalHorasReal)'
      '                              FROM MetaVendedorDia'
      
        '                             WHERE MetaVendedorDia.nCdMetaVended' +
        'or = @nCdMetaVendedor)'
      '      ,nPercHoraLojaReal = @nPercHorasReal'
      ' WHERE nCdMetaVendedor = @nCdMetaVendedor')
    Left = 380
    Top = 181
  end
end
