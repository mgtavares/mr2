unit fConfigCustoCompra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB;

type
  TfrmConfigCustoCompra = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryMaster: TADOQuery;
    dsMaster: TDataSource;
    qryMasternCdClasseCustoCompra: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdGrupoProduto: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdCC: TIntegerField;
    qryMastercNmGrupoProduto: TStringField;
    qryMastercNmLoja: TStringField;
    qryMastercNmCC: TStringField;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    procedure qryMasterCalcFields(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfigCustoCompra: TfrmConfigCustoCompra;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmConfigCustoCompra.qryMasterCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryMasternCdGrupoProduto.Value > 0) and (qryMastercNmGrupoProduto.Value = '') then
  begin
      qryGrupoProduto.Close ;
      PosicionaQuery(qryGrupoProduto, qryMasternCdGrupoProduto.AsString) ;
      qryMastercNmGrupoProduto.Value := qryGrupoProdutocNmGrupoProduto.Value ;
  end ;

  if (qryMasternCdLoja.Value > 0) and (qryMastercNmLoja.Value = '') then
  begin
      qryLoja.Close ;
      PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;
      qryMastercNmLoja.Value := qryLojacNmLoja.Value ;
  end ;

  if (qryMasternCdCC.Value > 0) and (qryMastercNmCC.Value = '') then
  begin
      qryCC.Close ;
      PosicionaQuery(qryCC, qryMasternCdCC.AsString) ;
      qryMastercNmCC.Value := qryCCcNmCC.Value ;
  end ;


end;

procedure TfrmConfigCustoCompra.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryMastercNmGrupoProduto.Value = '') then
  begin
      MensagemAlerta('Selecione o Grupo de Produto.') ;
      abort ;
  end ;

  if (qryMastercNmLoja.Value = '') and (DBGridEh1.Columns[4].Visible) then
  begin
      MensagemAlerta('Selecione a Loja.') ;
      abort ;
  end ;

  if (qryMastercNmCC.Value = '') then
  begin
      MensagemAlerta('Selecione o Centro de Custo.') ;
      abort ;
  end ;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmConfigCustoCompra.FormShow(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryMaster, IntToStr(frmMenu.nCdEmpresaAtiva)) ;

  if (frmMenu.LeParametro('VAREJO') <> 'S') then
  begin
      DBGridEh1.Columns[4].Visible := False ;
      DBGridEh1.Columns[5].Visible := False ;
  end ;

  DBGridEh1.Align := alClient ;

end;

procedure TfrmConfigCustoCompra.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsBrowse) then
             qryMaster.Edit ;
        

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (DBGridEh1.Col = 3) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(69);

                If (nPK > 0) then
                begin
                    qryMasternCdGrupoProduto.Value := nPK ;
                end ;
            end ;

            if (DBGridEh1.Col = 5) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(86,'Loja.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva));

                If (nPK > 0) then
                begin
                    qryMasternCdLoja.Value := nPK ;
                end ;
            end ;

            if (DBGridEh1.Col = 7) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(28,'cFlgLanc = 1') ;

                If (nPK > 0) then
                begin
                    qryMasternCdCC.Value := nPK ;
                end ;
            end ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmConfigCustoCompra) ;

end.
