unit rRankingCompraCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls, ER2Excel, ER2Lookup;

type
  TrptRankingCompraCliente = class(TfrmRelatorio_Padrao)
    Label1: TLabel;
    Label2: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Label3: TLabel;
    ER2Excel: TER2Excel;
    dsloja: TDataSource;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    ER2lpkLoja: TER2LookupMaskEdit;
    rgModeloImp: TRadioGroup;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRankingCompraCliente: TrptRankingCompraCliente;

implementation

{$R *.dfm}
uses
  fMenu,rRankingCompraCliente_view;

procedure TrptRankingCompraCliente.ToolButton1Click(Sender: TObject);
var
  objRel   : TrptRankingCompraCliente_view;
  iLinha   : integer;
  iPosicao : Integer;
begin
  inherited;

  objRel := TrptRankingCompraCliente_view.Create(nil);

  objRel.SPREL_RANKING_COMPRA_CLIENTES.Close;
  objRel.SPREL_RANKING_COMPRA_CLIENTES.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  objRel.SPREL_RANKING_COMPRA_CLIENTES.Parameters.ParamByName('@nCdLoja').Value    := frmMenu.ConvInteiro(ER2lpkLoja.Text);
  objRel.SPREL_RANKING_COMPRA_CLIENTES.Parameters.ParamByName('@dDtInicial').Value := frmMenu.ConvData(MaskEdit1.Text);
  objRel.SPREL_RANKING_COMPRA_CLIENTES.Parameters.ParamByName('@dDtFinal').Value   := frmMenu.ConvData(MaskEdit2.Text);
  objRel.SPREL_RANKING_COMPRA_CLIENTES.Open;

  case (rgModeloImp.ItemIndex) of
      0:begin
            { -- Preenchendo a label filtros do relatorio -- }
            objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
            objRel.lblFiltro.Caption  := 'Loja: ' + DBEdit1.Text + ' / ';
            objRel.lblFiltro.Caption  := objRel.lblFiltro.Caption + 'Per�odo: ' + MaskEdit1.Text + ' At� ';
            objRel.lblFiltro.Caption  := objRel.lblFiltro.Caption + MaskEdit2.Text;
            objRel.QuickRep1.Preview;
        end;
      1:begin
            frmMenu.mensagemUsuario('Exportando Planilha...');

            { -- formata c�lulas -- }
            ER2Excel.Celula['A1'].Background := RGB(221, 221, 221);
            ER2Excel.Celula['A1'].Range('N1');
            ER2Excel.Celula['A2'].Background := RGB(221, 221, 221);
            ER2Excel.Celula['A2'].Range('N2');
            ER2Excel.Celula['A3'].Background := RGB(221, 221, 221);
            ER2Excel.Celula['A3'].Range('N3');
            ER2Excel.Celula['A4'].Background := RGB(221, 221, 221);
            ER2Excel.Celula['A4'].Range('N4');
            ER2Excel.Celula['A5'].Background := RGB(221, 221, 221);
            ER2Excel.Celula['A5'].Range('N5');
            ER2Excel.Celula['A6'].Background := RGB(221, 221, 221);
            ER2Excel.Celula['A6'].Range('N6');

            { -- inseri informa��es do cabe�alho -- }
            ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
            ER2Excel.Celula['A2'].Text := 'Rel. Ranking Compra Cliente.';
            ER2Excel.Celula['A4'].Text := 'Loja: ' + DBEdit1.Text;
            ER2Excel.Celula['D4'].Text := 'Per�odo: ' + MaskEdit1.Text + ' at� ' + MaskEdit2.Text;
            ER2Excel.Celula['A6'].Text := 'Loja';
            ER2Excel.Celula['B6'].Text := 'Posi��o';
            ER2Excel.Celula['C6'].Text := 'Cliente';
            ER2Excel.Celula['M6'].Text := 'Total de Compras';

            ER2Excel.Celula['A1'].Congelar('A7');

            objRel.SPREL_RANKING_COMPRA_CLIENTES.First;

            iLinha   := 7;
            iPosicao := 1;

            while (not objRel.SPREL_RANKING_COMPRA_CLIENTES.Eof) do
            begin
                ER2Excel.Celula['A' + IntToStr(iLinha)].Text := objRel.SPREL_RANKING_COMPRA_CLIENTESnCdLoja.Value;
                ER2Excel.Celula['B' + IntToStr(iLinha)].Text := iPosicao;
                ER2Excel.Celula['C' + IntToStr(iLinha)].Text := objRel.SPREL_RANKING_COMPRA_CLIENTESnCdTerceiro.Value;
                ER2Excel.Celula['D' + IntToStr(iLinha)].Text := objRel.SPREL_RANKING_COMPRA_CLIENTEScNmTerceiro.Value;
                ER2Excel.Celula['M' + IntToStr(iLinha)].Text := FormatFloat('',objRel.SPREL_RANKING_COMPRA_CLIENTESnValTotalPedido.Value);

                ER2Excel.Celula['M' + IntToStr(iLinha)].Mascara         := '#.##0,00';
                ER2Excel.Celula['M' + IntToStr(iLinha)].HorizontalAlign := haRight ;

                objRel.SPREL_RANKING_COMPRA_CLIENTES.Next;

                Inc(iLinha);
                Inc(iPosicao);
            end;

            { -- exporta planilha e limpa result do componente -- }
            ER2Excel.ExportXLS;
            ER2Excel.CleanupInstance;

            frmMenu.mensagemUsuario('');
        end;
    end;
end;

procedure TrptRankingCompraCliente.FormActivate(Sender: TObject);
begin
  inherited;

  ER2lpkLoja.SetFocus;
end;

initialization
    RegisterClass(TrptRankingCompraCliente) ;
end.
