inherited frmCancDoctoFiscal: TfrmCancDoctoFiscal
  Left = 220
  Top = 127
  Width = 900
  Height = 444
  Caption = 'Cancelamento Documento Fiscal'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 884
    Height = 381
  end
  object Label1: TLabel [1]
    Left = 76
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 89
    Top = 70
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'S'#233'rie'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 10
    Top = 94
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Documento'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 46
    Top = 118
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Emiss'#227'o'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 74
    Top = 142
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 24
    Top = 166
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Documento'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 64
    Top = 214
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Opera'#231#227'o'
    FocusControl = DBEdit7
  end
  object Label9: TLabel [8]
    Left = 29
    Top = 238
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Documento'
    FocusControl = DBEdit9
  end
  object Label8: TLabel [9]
    Left = 41
    Top = 262
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Protocolo NFe'
    FocusControl = DBEdit11
  end
  object Label10: TLabel [10]
    Left = 56
    Top = 286
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'Recibo NFe'
    FocusControl = DBEdit12
  end
  object Label11: TLabel [11]
    Left = 70
    Top = 190
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'Situa'#231#227'o'
    FocusControl = DBEdit6
  end
  inherited ToolBar2: TToolBar
    Width = 884
    inherited btIncluir: TToolButton
      Enabled = False
      Visible = False
    end
    inherited ToolButton7: TToolButton
      Visible = False
    end
    inherited ToolButton9: TToolButton
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Visible = False
    end
    inherited btSalvar: TToolButton
      Visible = False
    end
    inherited ToolButton6: TToolButton
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [13]
    Tag = 1
    Left = 120
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdDoctoFiscal'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [14]
    Tag = 1
    Left = 120
    Top = 64
    Width = 26
    Height = 19
    DataField = 'cSerie'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [15]
    Tag = 1
    Left = 120
    Top = 88
    Width = 65
    Height = 19
    DataField = 'iNrDocto'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [16]
    Tag = 1
    Left = 120
    Top = 112
    Width = 89
    Height = 19
    DataField = 'dDtEmissao'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [17]
    Tag = 1
    Left = 120
    Top = 136
    Width = 721
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [18]
    Tag = 1
    Left = 120
    Top = 160
    Width = 121
    Height = 19
    DataField = 'nValTotal'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [19]
    Tag = 1
    Left = 120
    Top = 208
    Width = 65
    Height = 19
    DataField = 'cCFOP'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [20]
    Tag = 1
    Left = 188
    Top = 208
    Width = 653
    Height = 19
    DataField = 'cTextoCFOP'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit9: TDBEdit [21]
    Tag = 1
    Left = 120
    Top = 232
    Width = 65
    Height = 19
    DataField = 'nCdTipoDoctoFiscal'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit10: TDBEdit [22]
    Tag = 1
    Left = 188
    Top = 232
    Width = 653
    Height = 19
    DataField = 'cNmTipoDoctoFiscal'
    DataSource = dsMaster
    TabOrder = 10
  end
  object cxButton1: TcxButton [23]
    Left = 119
    Top = 312
    Width = 145
    Height = 33
    Caption = 'Cancelar Documento'
    TabOrder = 11
    OnClick = cxButton1Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE3E7F79AA7E34C62CC36
      4FC5344DC3465DC795A1DEE1E5F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFBFC7EF4C63D15264D48490E795A0EE959FED838EE54C5DCE3D54C3B8C1
      E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC1CAF14760D57584E3A1ACF47F8BEC5C
      67E45B66E37D87EA9FA8F16F7CDD324BC2B9C1EAFFFFFFFFFFFFFFFFFFE7EAFA
      5970DE7888E6A3B0F55767E75665E68992ED8892EC535FE2525DE19FA9F26F7D
      DD4157C6E2E6F6FFFFFFFFFFFFA8B4F06073E0A4B3F75A6EEB596CEA5869E8FC
      FCFCFCFCFC5562E55461E3535FE29FA9F25061D197A4E1FFFFFFFFFFFF6A82E9
      8E9FF08499F45C73EE5B70EC5A6EEB909DF1A6AFF35767E75665E65562E57D89
      EB8591E74E64CEFFFFFFFFFFFF5D76EAA0B3F76580F25F78F05D76EF5C73EEFC
      FCFCFCFCFC596CEA5869E85767E75D6CE799A5F13C55CCFFFFFFFFFFFF617BEE
      A1B6F86784F4607CF35F7AF15F78F0FCFCFCFCFCFC5B70EC5A6EEB596CEA5F6F
      E99BA8F14159D0FFFFFFFFFFFF768DF391A6F388A1F86280F4617EF3607CF3FC
      FCFCFCFCFC5D76EF5C73EE5B70EC8293F18998EC5970D8FFFFFFFFFFFFB2BFFA
      6C81ECA9BDFB6382F56281F56280F4FCFCFCFCFCFC5F7AF15F78F05D76EFA5B5
      F85D70DDA2AFEBFFFFFFFFFFFFEBEEFE758CF78397F0A9BDFB6382F56382F5FC
      FCFCFCFCFC617EF3607CF3A6B9F97B8DEA5C74E1E7EAFAFFFFFFFFFFFFFFFFFF
      CED7FD6D86F88497F1A9BDFB8AA3F86B89F66B89F689A2F8A8BCFA7F92EC5972
      E5C6CEF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCED7FD778EFA6E83EE92A6F4A0
      B4F8A0B4F891A6F3687DE96981EDC8D1F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFEBEEFFB5C3FD7C94FA6C86F76A84F5778EF5B1BEF8E9ECFDFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    LookAndFeel.NativeStyle = True
  end
  object DBEdit11: TDBEdit [24]
    Tag = 1
    Left = 120
    Top = 256
    Width = 721
    Height = 19
    DataField = 'cNrProtocoloNFe'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit12: TDBEdit [25]
    Tag = 1
    Left = 120
    Top = 280
    Width = 721
    Height = 19
    DataField = 'cNrReciboNFe'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit13: TDBEdit [26]
    Tag = 1
    Left = 120
    Top = 184
    Width = 65
    Height = 19
    DataField = 'iStatusRetorno'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit14: TDBEdit [27]
    Tag = 1
    Left = 188
    Top = 184
    Width = 653
    Height = 19
    DataField = 'cNmStatusRetorno'
    DataSource = dsMaster
    TabOrder = 15
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT DoctoFiscal.nCdDoctoFiscal'
      '      ,cSerie         '
      '      ,iNrDocto       '
      '      ,dDtEmissao     '
      '      ,cNmTerceiro    '
      '      ,nValTotal      '
      '      ,cCFOP          '
      '      ,cTextoCFOP     '
      '      ,TipoDoctoFiscal.nCdTipoDoctoFiscal '
      '      ,TipoDoctoFiscal.cNmTipoDoctoFiscal'
      '      ,iStatusRetorno'
      '      ,cNmStatusRetorno'
      '      ,cNrProtocoloNFe'
      '      ,cNrReciboNFe'
      '      ,DoctoFiscal.nCdTerceiro'
      '      ,DoctoFiscal.nCdSerieFiscal'
      '      ,CASE WHEN iStatusRetorno IN(110  -- uso denegado'
      
        '                                  ,205  -- nf-e esta denegada na' +
        ' base de dados da SEFAZ'
      
        '                                  ,233  -- IE do destinatario na' +
        'o cadastrada'
      
        '                                  ,234  -- IE do destinatario na' +
        'o vinculada ao CNPJ'
      
        '                                  ,301  -- irregularidade fiscal' +
        ' do emitente'
      
        '                                  ,302) -- irregularidade fiscal' +
        ' do destinat'#225'rio'
      '            THEN 1'
      '            ELSE 0'
      '       END cFlgUsoDenegado'
      '      ,ISNULL((SELECT 1'
      '                 FROM NumDoctoFiscal'
      
        '                WHERE NumDoctoFiscal.nCdSerieFiscal = DoctoFisca' +
        'l.nCdSerieFiscal'
      
        '                  AND NumDoctoFiscal.nCdDoctoFiscal = DoctoFisca' +
        'l.nCdDoctoFiscal),0) as cFlgDocPendente'
      '  FROM DoctoFiscal'
      
        '       INNER JOIN TipoDoctoFiscal ON TipoDoctoFiscal.nCdTipoDoct' +
        'oFiscal = DoctoFiscal.nCdTipoDoctoFiscal'
      ' WHERE DoctoFiscal.nCdStatus      = 1'
      '   AND DoctoFiscal.nCdEmpresa     = :nCdEmpresa'
      '   AND DoctoFiscal.nCdDoctoFiscal = :nPK')
    Left = 416
    Top = 320
    object qryMasternCdDoctoFiscal: TAutoIncField
      FieldName = 'nCdDoctoFiscal'
      ReadOnly = True
    end
    object qryMastercSerie: TStringField
      FieldName = 'cSerie'
      FixedChar = True
      Size = 2
    end
    object qryMasteriNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryMasterdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryMastercNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMasternValTotal: TBCDField
      FieldName = 'nValTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercCFOP: TStringField
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryMastercTextoCFOP: TStringField
      FieldName = 'cTextoCFOP'
      Size = 50
    end
    object qryMasternCdTipoDoctoFiscal: TIntegerField
      FieldName = 'nCdTipoDoctoFiscal'
    end
    object qryMastercNmTipoDoctoFiscal: TStringField
      FieldName = 'cNmTipoDoctoFiscal'
      Size = 50
    end
    object qryMastercNrProtocoloNFe: TStringField
      FieldName = 'cNrProtocoloNFe'
      Size = 100
    end
    object qryMastercNrReciboNFe: TStringField
      FieldName = 'cNrReciboNFe'
      Size = 100
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasteriStatusRetorno: TIntegerField
      FieldName = 'iStatusRetorno'
    end
    object qryMastercNmStatusRetorno: TStringField
      FieldName = 'cNmStatusRetorno'
      Size = 150
    end
    object qryMasternCdSerieFiscal: TIntegerField
      FieldName = 'nCdSerieFiscal'
    end
    object qryMastercFlgDocPendente: TIntegerField
      FieldName = 'cFlgDocPendente'
      ReadOnly = True
    end
    object qryMastercFlgUsoDenegado: TIntegerField
      FieldName = 'cFlgUsoDenegado'
      ReadOnly = True
    end
  end
  inherited dsMaster: TDataSource
    Left = 416
    Top = 352
  end
  inherited qryID: TADOQuery
    Left = 480
    Top = 320
  end
  inherited usp_ProximoID: TADOStoredProc
    CommandTimeout = 0
    Left = 512
    Top = 352
  end
  inherited qryStat: TADOQuery
    Left = 448
    Top = 320
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 512
    Top = 320
  end
  inherited ImageList1: TImageList
    Left = 544
    Top = 352
  end
  object usp_cancela: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_CANCELA_DOCTO_FISCAL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdDoctoFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 544
    Top = 320
  end
  object qryVerificaComplemento: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT TOP 1 nCdDoctoFiscal'
      '  FROM DoctoFiscal '
      ' WHERE nCdDoctoFiscalOrigemCompl =:nCdDoctoFiscal'
      '   AND cFlgComplementar          = 1'
      '   AND dDtCancel                 IS NULL')
    Left = 448
    Top = 352
    object qryVerificaComplementonCdDoctoFiscal: TAutoIncField
      FieldName = 'nCdDoctoFiscal'
      ReadOnly = True
    end
  end
  object qryEnvioNFeEmail: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT cFlgEnviaNFeEmail'
      '      ,cEmailNFe'
      '      ,cEmailCopiaNFe'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro =:nPK')
    Left = 480
    Top = 352
    object qryEnvioNFeEmailcFlgEnviaNFeEmail: TIntegerField
      FieldName = 'cFlgEnviaNFeEmail'
    end
    object qryEnvioNFeEmailcEmailNFe: TStringField
      FieldName = 'cEmailNFe'
      Size = 50
    end
    object qryEnvioNFeEmailcEmailCopiaNFe: TStringField
      FieldName = 'cEmailCopiaNFe'
      Size = 50
    end
  end
  object SP_PROCESSA_NUMDOCTOFISCAL: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_PROCESSA_NUMDOCTOFISCAL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdSerieFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdDoctoFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cAcao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 580
    Top = 321
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 576
    Top = 352
  end
end
