unit rRelacaoTerceiroRepresentante;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, DB, ADODB, StdCtrls, Mask, ER2Lookup,
  ImgList, ComCtrls, ToolWin, ExtCtrls, DBCtrls, cxControls, cxContainer,
  cxEdit, cxLabel, ER2Excel;

type
  TrptRelacaoTerceiroRepresentante = class(TfrmRelatorio_Padrao)
    ER2LookupRepres: TER2LookupMaskEdit;
    qryRepresentante: TADOQuery;
    dsRepresentante: TDataSource;
    qryRepresentantenCdTerceiro: TIntegerField;
    qryRepresentantecNmTerceiro: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    edtCidade: TEdit;
    Label4: TLabel;
    edtBairro: TEdit;
    ComboUF: TComboBox;
    ER2Excel: TER2Excel;
    rgModeloImp: TRadioGroup;
    qryTerceiro: TADOQuery;
    dsTerceiro: TDataSource;
    rgAgrupamento: TRadioGroup;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    ER2LookupTerceiro: TER2LookupMaskEdit;
    Label9: TLabel;
    edtDtInicial: TMaskEdit;
    Label8: TLabel;
    edtDtFinal: TMaskEdit;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRelacaoTerceiroRepresentante: TrptRelacaoTerceiroRepresentante;

implementation

uses
  fMenu, rRelacaoTerceiroRepresentante_View;

{$R *.dfm}

procedure TrptRelacaoTerceiroRepresentante.ToolButton1Click(
  Sender: TObject);
var
  objRel          : TrptRelacaoTerceiroRepresentante_View;
  cNmRepres       : String;
  cNmTerceiro     : String;
  cBairro         : String;
  cCidade         : String;
  cUF             : String;
  cModAgrupamento : String;
  cNmOrdenacao1   : String;
  cNmOrdenacao2   : String;
  cNmTotal        : String;
  iLinha          : Integer;
begin
  { -- formata filtros -- }
  if (Trim(DBEdit1.Text) = '') then
      cNmRepres := 'Todos'
  else
      cNmRepres := DBEdit1.Text;

  if (Trim(DBEdit2.Text) = '') then
      cNmTerceiro := 'Todos'
  else
      cNmTerceiro := DBEdit2.Text;

  if (Trim(edtBairro.Text) = '') then
      cBairro := 'Todos'
  else
      cBairro := edtBairro.Text;

  if (Trim(edtCidade.Text) = '') then
      cCidade := 'Todos'
  else
      cCidade := edtCidade.Text;

  if (ComboUF.ItemIndex = -1) then
      cUF := 'Todos'
  else
      cUF := ComboUF.Text;

  if (rgAgrupamento.ItemIndex = 0) then
  begin
      cModAgrupamento := 'T';
      cNmOrdenacao1   := 'Grupo Terceiro';
      cNmOrdenacao2   := 'Representantes';
      cNmTotal        := 'Total Representantes..:';
  end
  else
  begin
      cModAgrupamento := 'R';
      cNmOrdenacao1   := 'Grupo Representante';
      cNmOrdenacao2   := 'Terceiros';
      cNmTotal        := 'Total Terceiros..:';
  end;

  try
      try
          objRel := TrptRelacaoTerceiroRepresentante_View.Create(Self);

          objRel.SPREL_TERCEIRO_X_REPRESENTANTE.Close;
          objRel.SPREL_TERCEIRO_X_REPRESENTANTE.Parameters.ParamByName('@CodTerceiro').Value      := frmMenu.ConvInteiro(ER2LookupTerceiro.Text);
          objRel.SPREL_TERCEIRO_X_REPRESENTANTE.Parameters.ParamByName('@CodRepresentante').Value := frmMenu.ConvInteiro(ER2LookupRepres.Text);
          objRel.SPREL_TERCEIRO_X_REPRESENTANTE.Parameters.ParamByName('@cBairro').Value          := Trim(edtBairro.Text);
          objRel.SPREL_TERCEIRO_X_REPRESENTANTE.Parameters.ParamByName('@cCidade').Value          := Trim(edtCidade.Text);
          objRel.SPREL_TERCEIRO_X_REPRESENTANTE.Parameters.ParamByName('@cUF').Value              := ComboUF.Text;
          objRel.SPREL_TERCEIRO_X_REPRESENTANTE.Parameters.ParamByName('@dDtInicial').Value       := frmMenu.ConvData(edtDtInicial.Text);
          objRel.SPREL_TERCEIRO_X_REPRESENTANTE.Parameters.ParamByName('@dDtFinal').Value         := frmMenu.ConvData(edtDtFinal.Text);
          objRel.SPREL_TERCEIRO_X_REPRESENTANTE.Parameters.ParamByName('@cModAgrupamento').Value  := cModAgrupamento;
          objRel.SPREL_TERCEIRO_X_REPRESENTANTE.Open;

          case (rgModeloImp.ItemIndex) of
              { -- modelo impresso -- }
              0: begin
                     objRel.lblEmpresa.Caption       := frmMenu.cNmEmpresaAtiva;
                     objRel.lblFiltros.Caption       := 'Representante: ' + cNmRepres + ' / Terceiro: ' + cNmTerceiro + ' / Agrupar por: ' + cNmOrdenacao1 + #13 + 'Bairro: ' + cBairro + ' / Cidade: ' + cCidade + ' / UF: ' + cUF;
                     objRel.lblOrdenacao1.Caption    := cNmOrdenacao1;
                     objRel.lblOrdenacao2.Caption    := '*------------ ' + cNmOrdenacao2 + ' ------------*';
                     objRel.lblTotalRegistro.Caption := cNmTotal;

                     { -- formata exibi��o -- }
                     if (cModAgrupamento = 'T') then
                     begin
                         objRel.nCdTerceiroRepres.DataField := 'nCdRepresentante';
                         objRel.cNmTerceiroRepres.DataField := 'cNmRepresentante';
                     end
                     else
                     begin
                         objRel.nCdTerceiroRepres.DataField := 'nCdTerceiro';
                         objRel.cNmTerceiroRepres.DataField := 'cNmTerceiro';
                     end;

                     objRel.QuickRep1.PreviewModal;
                 end;

                 { -- modelo planilha -- }
              1: begin
                     frmMenu.mensagemUsuario('Exportando Planilha...');

                     { -- formata c�lulas -- }
                     ER2Excel.Celula['A1'].Background := RGB(221,221,221);
                     ER2Excel.Celula['A1'].Range('AM1');
                     ER2Excel.Celula['A2'].Background := RGB(221,221,221);
                     ER2Excel.Celula['A2'].Range('AM2');
                     ER2Excel.Celula['A3'].Background := RGB(221,221,221);
                     ER2Excel.Celula['A3'].Range('AM3');
                     ER2Excel.Celula['A4'].Background := RGB(221,221,221);
                     ER2Excel.Celula['A4'].Range('AM4');
                     ER2Excel.Celula['A5'].Background := RGB(221,221,221);
                     ER2Excel.Celula['A5'].Range('AM5');
                     ER2Excel.Celula['A6'].Background := RGB(221,221,221);
                     ER2Excel.Celula['A6'].Range('AM6');

                     { -- inseri informa��es do cabe�alho -- }
                     ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                     ER2Excel.Celula['A2'].Text := 'Rel. Rela��o Terceiro x Representante.';
                     ER2Excel.Celula['A4'].Text := 'Terceiro: ' + cNmTerceiro;
                     ER2Excel.Celula['E4'].Text := 'Representante: ' + cNmRepres;
                     ER2Excel.Celula['I4'].Text := 'Bairro: ' + cBairro;
                     ER2Excel.Celula['M4'].Text := 'Cidade: ' + cCidade;
                     ER2Excel.Celula['Q4'].Text := 'UF: ' + cUF;
                     ER2Excel.Celula['A6'].Text := cNmOrdenacao1;
                     ER2Excel.Celula['F6'].Text := 'Nome Fantasia';
                     ER2Excel.Celula['H6'].Text := 'CNPJ/CPF';
                     ER2Excel.Celula['J6'].Text := cNmOrdenacao2;
                     ER2Excel.Celula['O6'].Text := 'Contato';
                     ER2Excel.Celula['Q6'].Text := 'Dt. Cadastro';
                     ER2Excel.Celula['R6'].Text := 'Tel. 01';
                     ER2Excel.Celula['S6'].Text := 'Tel. 02';
                     ER2Excel.Celula['T6'].Text := 'Tel. M�vel';
                     ER2Excel.Celula['U6'].Text := 'E-mail';
                     ER2Excel.Celula['Y6'].Text := 'Endere�o';

                     ER2Excel.Celula['A1'].Congelar('A7');

                     iLinha := 7;

                     objRel.SPREL_TERCEIRO_X_REPRESENTANTE.First;

                     while (not objRel.SPREL_TERCEIRO_X_REPRESENTANTE.Eof) do
                     begin
                         ER2Excel.Celula['A' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEnCdOrdenacao.Value;
                         ER2Excel.Celula['B' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcOrdenacao.Value;
                         ER2Excel.Celula['F' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcNmFantasia.Value;
                         ER2Excel.Celula['H' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcCNPJCPF.Value;

                         if (rgAgrupamento.ItemIndex = 0) then
                         begin
                             ER2Excel.Celula['J' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEnCdRepresentante.Value;
                             ER2Excel.Celula['K' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcNmRepresentante.Value;
                         end
                         else
                         begin
                             ER2Excel.Celula['J' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEnCdTerceiro.Value;
                             ER2Excel.Celula['K' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcNmTerceiro.Value;
                         end;

                         ER2Excel.Celula['O' + IntToStr(iLinha)].Text  := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcNmContato.Value;
                         ER2Excel.Celula['Q' + IntToStr(iLinha)].Text  := objRel.SPREL_TERCEIRO_X_REPRESENTANTEdDtCadastro.Value;
                         ER2Excel.Celula['R' + IntToStr(iLinha)].Text  := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcTelefone1.Value;
                         ER2Excel.Celula['S' + IntToStr(iLinha)].Text  := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcTelefone2.Value;
                         ER2Excel.Celula['T' + IntToStr(iLinha)].Text  := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcTelefoneMovel.Value;
                         ER2Excel.Celula['U' + IntToStr(iLinha)].Text  := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcEmail.Value;
                         ER2Excel.Celula['Y' + IntToStr(iLinha)].Text  := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcEndereco.Value;
                         ER2Excel.Celula['AD' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEiNumero.Value;
                         ER2Excel.Celula['AE' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcBairro.Value;
                         ER2Excel.Celula['AG' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcCidade.Value;
                         ER2Excel.Celula['AI' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcUF.Value;
                         ER2Excel.Celula['AJ' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcCep.Value;
                         ER2Excel.Celula['AK' + IntToStr(iLinha)].Text := objRel.SPREL_TERCEIRO_X_REPRESENTANTEcComplemento.Value;

                         objRel.SPREL_TERCEIRO_X_REPRESENTANTE.Next;

                         Inc(iLinha);
                     end;

                     { -- exporta planilha e limpa result do componente -- }
                     ER2Excel.ExportXLS;
                     ER2Excel.CleanupInstance;

                     frmMenu.mensagemUsuario('');
                 end;
          end;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          Raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

  ER2LookupTerceiro.SetFocus;
end;

procedure TrptRelacaoTerceiroRepresentante.FormActivate(Sender: TObject);
begin
  inherited;

  ER2LookupTerceiro.SetFocus;
end;

initialization
     RegisterClass(TrptRelacaoTerceiroRepresentante) ;

end.
