inherited frmManutPedido: TfrmManutPedido
  Left = 73
  Top = 48
  Width = 1095
  Height = 666
  Caption = 'Manuten'#231#227'o de Pedidos'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 169
    Width = 1079
    Height = 459
  end
  inherited ToolBar2: TToolBar
    Width = 1079
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited ToolButton7: TToolButton
      Visible = False
    end
    inherited ToolButton9: TToolButton
      Visible = False
    end
    inherited btSalvar: TToolButton
      Visible = False
    end
    inherited ToolButton6: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 25
    Width = 1079
    Height = 144
    Align = alTop
    Caption = 'Dados do Pedido'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 19
      Top = 22
      Width = 80
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero Pedido'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Tag = 1
      Left = 38
      Top = 46
      Width = 61
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo Pedido'
      FocusControl = DBEdit2
    end
    object Label4: TLabel
      Tag = 1
      Left = 59
      Top = 70
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro'
      FocusControl = DBEdit4
    end
    object Label11: TLabel
      Tag = 1
      Left = 11
      Top = 94
      Width = 88
      Height = 13
      Alignment = taRightJustify
      Caption = 'Status do Pedido'
      FocusControl = DBEdit11
    end
    object Label6: TLabel
      Tag = 1
      Left = 36
      Top = 118
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Pedido'
      FocusControl = DBEdit6
    end
    object Label7: TLabel
      Tag = 1
      Left = 180
      Top = 118
      Width = 85
      Height = 13
      Alignment = taRightJustify
      Caption = 'Previs'#227'o Entrega'
      FocusControl = DBEdit7
    end
    object Label3: TLabel
      Tag = 1
      Left = 364
      Top = 118
      Width = 16
      Height = 13
      Alignment = taRightJustify
      Caption = 'at'#233
      FocusControl = DBEdit7
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 104
      Top = 16
      Width = 65
      Height = 19
      DataField = 'nCdPedido'
      DataSource = dsMaster
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 104
      Top = 40
      Width = 65
      Height = 19
      DataField = 'nCdTipoPedido'
      DataSource = dsMaster
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 172
      Top = 40
      Width = 650
      Height = 19
      DataField = 'cNmTipoPedido'
      DataSource = dsMaster
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 104
      Top = 64
      Width = 65
      Height = 19
      DataField = 'nCdTerceiro'
      DataSource = dsMaster
      TabOrder = 3
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 172
      Top = 64
      Width = 650
      Height = 19
      DataField = 'cNmTerceiro'
      DataSource = dsMaster
      TabOrder = 4
    end
    object DBEdit11: TDBEdit
      Tag = 1
      Left = 104
      Top = 88
      Width = 65
      Height = 19
      DataField = 'nCdTabStatusPed'
      DataSource = dsMaster
      TabOrder = 5
    end
    object DBEdit12: TDBEdit
      Tag = 1
      Left = 172
      Top = 88
      Width = 650
      Height = 19
      DataField = 'cNmTabStatusPed'
      DataSource = dsMaster
      TabOrder = 6
    end
    object DBEdit6: TDBEdit
      Tag = 1
      Left = 104
      Top = 112
      Width = 65
      Height = 19
      DataField = 'dDtPedido'
      DataSource = dsMaster
      TabOrder = 7
    end
    object DBEdit7: TDBEdit
      Left = 272
      Top = 112
      Width = 73
      Height = 19
      DataField = 'dDtPrevEntIni'
      DataSource = dsMaster
      TabOrder = 8
    end
    object DBEdit8: TDBEdit
      Left = 392
      Top = 112
      Width = 73
      Height = 19
      DataField = 'dDtPrevEntFim'
      DataSource = dsMaster
      TabOrder = 9
    end
    object btPrevisao: TcxButton
      Left = 472
      Top = 111
      Width = 25
      Height = 25
      Hint = 'Informe a nova previs'#227'o de entrega e clique aqui para salvar.'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 10
      OnClick = btPrevisaoClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFCDCDCD595959606060B3B3B3595959606060DE
        DEDEFF00FFFF00FFFF00FFCDCDCD595959606060DEDEDEFF00FFFF00FF454545
        6464646C6C6C3131316F6F6F5C5C5C606060FF00FFFF00FFFF00FF4545456464
        645C5C5C606060FF00FF3044562E43552C41532A4052283E51263D50253C4E23
        3B4D223A4C21394C523B2A513A295038284F37274E3626FF00FF4680B895C0DE
        90BDDC8CBADA87B6D983B3D77FB1D67BAED478ABD375A9D3AA6732ECD4C0EBD2
        BDEAD1BA9C5624FF00FF4E86BE9BC4E179B0D673ACD36EA8D168A4CF63A0CD5D
        9CCB5899CA7AADD4AF6D36EDD8C5E1BC9BEBD4BFA05B28FF00FF558BC3A1C9E3
        82B6D97CB2D777AED571AAD26BA6D065A2CE609ECC7FB1D6B4733BEFDCCAEEDA
        C8FFF6EFA5612DFF00FF5C90C8DDB47CD09A54CE9451CB8E4DC8884AC58146C3
        7B43C0753FCB8F67B97940EED3BCFFF2E7FFF6F0AA6732FF00FF6294CCFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBD7E44EFD7C2EED5
        BFE2C0A3B57744FF00FF6898D0B2D5E898C6E194C3E08FBFDE8ABCDC85B8DA7F
        B4D879B0D692BEDDC18347BE8045BB7C42B97A41E1C7B1FF00FF6D9CD4B6D7EA
        B3D6E9B1D3E8ADD1E7AACFE5A6CCE4A1C9E39DC6E199C3E03776AEFF00FFFF00
        FFFF00FFFF00FFFF00FF7FA9DB6D9CD46A9AD26697CF6194CC5C90C8568CC451
        88C04B84BC457FB7749FC8FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
        FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
        FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
        00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
      LookAndFeel.Kind = lfOffice11
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 169
    Width = 1079
    Height = 459
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 455
    ClientRectLeft = 4
    ClientRectRight = 1075
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Itens do Pedido'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 44
        Width = 1071
        Height = 387
        Align = alClient
        AllowedOperations = []
        DataGrouping.GroupLevels = <>
        DataSource = dsItemEstoque
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItemPedido'
            Footers = <>
            Width = 60
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cCdProduto'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cNmItem'
            Footers = <>
            Width = 561
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePed'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeExpRec'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeCanc'
            Footers = <>
            Width = 85
          end
          item
            EditButtons = <>
            FieldName = 'nSaldoItem'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
          object DBGridEh2: TDBGridEh
            Left = 0
            Top = 0
            Width = 0
            Height = 0
            Align = alClient
            DataGrouping.GroupLevels = <>
            DataSource = dsTemp
            Flat = False
            FooterColor = clWindow
            FooterFont.Charset = DEFAULT_CHARSET
            FooterFont.Color = clWindowText
            FooterFont.Height = -11
            FooterFont.Name = 'Segoe UI'
            FooterFont.Style = []
            IndicatorOptions = [gioShowRowIndicatorEh]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Segoe UI'
            TitleFont.Style = []
            OnDrawColumnCell = DBGridEh2DrawColumnCell
            OnExit = DBGridEh2Exit
            object RowDetailData: TRowDetailPanelControlEh
            end
          end
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1071
        Height = 44
        Align = alTop
        TabOrder = 1
        object btGrade: TcxButton
          Left = 4
          Top = 8
          Width = 145
          Height = 33
          Caption = 'Visualizar Grade'
          Enabled = False
          TabOrder = 0
          OnClick = btGradeClick
          Glyph.Data = {
            7E030000424D7E030000000000003600000028000000120000000F0000000100
            1800000000004803000000000000000000000000000000000000FFFEFFFFFDFE
            FFFCFDFFFCFBFEFAF9FFFFFFFFFFFFFFFFFFFFF7F3FFFEF8FFF8F5FFF9F7A187
            872005081E0306FFFCFEFFF8FAFFFFFF0000FFFEFFFFFDFEFFFCFDFFFBFCFDFB
            FBFFFFFFFFFFFFFFFFFFFFFFFFFFFBF5FFFEF9937C7A110200FFFAFA7B6F6F0D
            0101FFFDFDFFFFFF0000FFFEFFFFFDFFFBFDFEF7FDFCF6FFFFE0EFEB89989476
            847E868E877A7E7889877F000400E5FBF6718F8A819F9A000400E7FCF9FFFFFF
            0000FFFEFFFEFDFFF8FDFFF3FCFF71818000110F0009060009040003008B938C
            000200F4FCF5718D87597D77000600E4FFFFFFFFFFFFFFFF0000FFFEFFFFFFFF
            FAFEFF000004000004E6F5F8F1FFFFECF8F8FBFFFF090706090000FFFFFC8083
            81000100FBFFFFFFFFFFFFFFFFFFFFFF0000FAFBFFFAFEFF00080FE0F2F9E8FF
            FFE6FFFFDDF8FCEAFFFFD6E8E9EEF8F8FBFFFF020000080001FFF9FBFFFFFFFF
            FFFFFFFFFFFFFFFF0000FAFEFFF1FDFF000A1200141A00030A001318000B0D00
            0607000E0E000604000B0A000404948F919B878CAB959A816B708D777CFFF7FC
            0000FFFFFF00070DA5CFD600080EA3EFF571CCD000131581E9E87EE5E2001A15
            9FFFFC94E0DC000D0C000101000303030809070B0CF5F4F60000EDFFFF000E13
            86C8CD00141968DBDE62E9EB0029283BDFDA42EAE3001B124AF3EA66FAF00018
            0C00130799E8DD8CD3C9002018D4FFFF0000E8FEFF0003088BCFD687E2E974E6
            ED4CD0D74EE0E545E1E155F6F449EAE841DEDA4CDAD500170D81E0D280D8CBA5
            F4E996D9D0CFFFFF0000E4F8FD00121B9ECBD8AEE9F8A9EFFFA2F0FF9BEDFEA7
            F7FF99E4F4AEF3FFB5F1FF000B16000407001614000403000606000203F0FDFF
            0000EBFAFDE3F9FF00000B000F1E000515000011000D1F00041700031500000F
            00051900000CF5FBFFF7FCFDF4F9FCF7F9FAFFFDFFFFFFFF0000F6FBFCF0F7FA
            081115EEF9FDEBF7FDF3FFFFE4F0F6E9F3FAF2FAFFF6FCFFFAFDFF000004F9FB
            FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFEFEFEFFFFFF0000000000
            00F4F4F4F7F7F7FFFFFFF6F6F6000000000000F7F7F7FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFF7F7F702020206060600
            00000E0E0EFEFEFEFFFFFFFFFFFFFDFDFDFDFDFDFDFDFDFEFEFEFEFEFEFEFEFE
            0000}
          LookAndFeel.Kind = lfOffice11
        end
        object btCancelaSaldo: TcxButton
          Left = 150
          Top = 8
          Width = 145
          Height = 33
          Caption = 'Cancelar Saldo Item'
          Enabled = False
          TabOrder = 1
          OnClick = btCancelaSaldoClick
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            20000000000000040000120B0000120B00000000000000000000FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00C4C4F900F8F8FE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00F7F7FE00C0C0F500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00C6C5FA002321EE001B1AE600F8F8FE00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00F7F7FE000E0DDA001110E200C0C0F500FFFFFF00FFFFFF00FFFFFF00C8C7
            FB002928F2003230F9002927F3001D1BE600F8F8FE00FFFFFF00FFFFFF00F8F7
            FE001110DC001A18EC00211FF3001110E200C0C0F500FFFFFF00FFFFFF00DBDB
            FD002C29F3003431F9003E3DFF002A28F3001D1CE800F8F8FE00F8F8FE001614
            E1001D1CEE003231FF001F1EF000100FE100D6D6F800FFFFFF00FFFFFF00FFFF
            FF00DBDBFD002C2AF3003532F900413EFF002B29F3001E1DE8001A19E5002321
            F0003635FF002322F2001312E400D6D6F900FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00DBDBFD002C2BF3003533F900413FFF00403DFF003D3CFF003C3A
            FF002827F4001817E800D7D7FA00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00DBDBFD002D2CF4004643FF002A28FF002826FF003F3D
            FF001E1DEB00D8D8FA00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00F9F8FF002F2DF3004A46FF002E2CFF002C29FF004341
            FF001F1DE900F8F8FE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00F9F9FF003734F7003D3BFA004D4BFF004B48FF004946FF004644
            FF002E2CF400201EE900F8F8FE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00F9F9FF003E3BFB004441FC00534FFF004340FB003331F600302DF4003B38
            F9004846FF00302DF400211FE900F8F8FE00FFFFFF00FFFFFF00FFFFFF00F9F9
            FF004240FE004A47FE005854FF004946FC003A37FA00DCDCFE00DCDBFD00302E
            F6003C3AF9004A47FF00302FF4002220EB00FBFBFF00FFFFFF00FFFFFF00E8E8
            FF004744FF004E4BFF004E4BFE00413EFC00DEDDFE00FFFFFF00FFFFFF00DCDC
            FD00312FF6003C3BFA004B49FF00312FF6009D9DF600FDFDFF00FFFFFF00FFFF
            FF00E8E8FF004744FF004643FE00DFDEFE00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00DCDCFD003330F6003836F700706FF400DCDBFC00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00E8E8FF00DFDFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00DCDCFD00A4A3FA00CBCBFC00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00F7F7FF00FFFFFF00FFFFFF00FFFFFF00}
          LookAndFeel.Kind = lfOffice11
        end
        object btAlterarKit: TcxButton
          Left = 652
          Top = 8
          Width = 145
          Height = 33
          Caption = 'Alterar Kit'
          Enabled = False
          TabOrder = 2
          Visible = False
          OnClick = btAlterarKitClick
          Glyph.Data = {
            A6020000424DA60200000000000036000000280000000F0000000D0000000100
            1800000000007002000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFF0000000000000000000000000000000000000000000000000000000000
            00000000000000000000FFFFFFFFFFFFFFFFFF000000FF9C31000000BDBDBDEF
            FFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000000000000000
            000000000000F7CE8CFF9C31000000A56300FF9C31FF9C31FF9C31FF9C31FF9C
            31FF9C31000000000000000000F7CE8CF7CE8CF7CE8CF7CE8CF7CE8CFF9C3100
            0000A56300EFFFFFEFFFFFEFFFFFEFFFFFFF9C31000000000000000000FFEFDE
            FFEFDEF7CE8CF7CE8CF7CE8CF7CE8CFF9C31000000FF9C31FF9C31FF9C31FF9C
            31FF9C31000000000000000000EFFFFFEFFFFFEFDED6F7CE8CF7CE8CFFEFDE00
            0000A56300EFFFFFEFFFFFEFFFFFEFFFFFFF9C31000000000000000000000000
            000000000000F7CE8CEFDED6000000A56300FF9C31FF9C31FF9C31FF9C31FF9C
            31FF9C31000000000000FFFFFFFFFFFFFFFFFF000000FFEFDE000000BDBDBDEF
            FFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000FFFFFFFFFFFF
            FFFFFF000000000000000000EFFFFFA5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5
            A5EFFFFF000000000000FFFFFFFFFFFFFFFFFF0000008C9C9C000000EFFFFFEF
            FFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFF000000BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD
            BDBDBDBD000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000BDBDBDBD
            BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000
            00000000000000000000}
          LookAndFeel.NativeStyle = True
        end
        object btBxManual: TcxButton
          Left = 444
          Top = 8
          Width = 145
          Height = 33
          Caption = 'Baixa Manual'
          Enabled = False
          TabOrder = 3
          OnClick = btBxManualClick
          Glyph.Data = {
            4E030000424D4E0300000000000036000000280000000E000000120000000100
            1800000000001803000000000000000000000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FF0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFC7C7C78686867272
            72727272727272727272929292FF00FF0000FF00FFFF00FFFF00FFFF00FFFF00
            FFD4D4D4747474727272727272727272727272727272727272FF00FF0000FF00
            FFFF00FFFF00FFFF00FFE1E1E177777772727272727272727272727272727272
            7272727272FF00FF0000FF00FFFF00FFFF00FFEBEBEB7E7E7E72727272727272
            7272727272727272727272727272727272FF00FF0000FF00FFFF00FFF3F3F386
            8686727272727272727272727272727272727272727272727272959595FF00FF
            0000FF00FFFF00FF9090907272727A7A7AC0C0C07A7A7A727272727272727272
            727272868686FF00FFFF00FF0000FF00FFFF00FFCFCFCFBEBEBEFF00FFFF00FF
            7A7A7A7272727272727E7E7E969696FF00FFFF00FFFF00FF0000FF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FF7A7A7A727272DBDBDBFF00FFFF00FFFF00FFFF00
            FFFF00FF0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FF7A7A7A727272FF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FF0000FF00FFFF00FFFF00FFFF00FFDBC3
            AAFF00FF747474727272FF00FFDBC3AAFF00FFFF00FFFF00FFFF00FF0000FF00
            FFFF00FFFF00FFE4D3C1D8BEA4FF00FF727272727272FF00FFD8BEA4E4D3C1FF
            00FFFF00FFFF00FF0000FF00FFFF00FFFF00FFDAC2A9D8BEA4FF00FF89898989
            8989FF00FFD8BEA4DAC1A8FF00FFFF00FFFF00FF0000FF00FFFF00FFFF00FFDA
            C2A9D8BEA4E3D1BFFF00FFFF00FFE3D1BFD8BEA4DAC1A8FF00FFFF00FFFF00FF
            0000FF00FFFF00FFFF00FFE5D3C2D8BEA4D8BEA4D8BEA4D8BEA4D8BEA4D8BEA4
            E4D3C1FF00FFFF00FFFF00FF0000FF00FFFF00FFFF00FFFF00FFDBC3AAD8BEA4
            D8BEA4D8BEA4D8BEA4DBC3AAFF00FFFF00FFFF00FFFF00FF0000FF00FFFF00FF
            FF00FFFF00FFFF00FFE5D4C3DCC5AEDCC5AEE5D4C3FF00FFFF00FFFF00FFFF00
            FFFF00FF0000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FF0000}
          LookAndFeel.NativeStyle = True
        end
        object btCancelaSaldoPedido: TcxButton
          Left = 297
          Top = 8
          Width = 145
          Height = 33
          Caption = 'Cancelar Saldo Pedido'
          TabOrder = 4
          OnClick = btCancelaSaldoPedidoClick
          Glyph.Data = {
            36040000424D3604000000000000360000002800000010000000100000000100
            20000000000000040000120B0000120B00000000000000000000FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00C4C4F900F8F8FE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00F7F7FE00C0C0F500FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00C6C5FA002321EE001B1AE600F8F8FE00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00F7F7FE000E0DDA001110E200C0C0F500FFFFFF00FFFFFF00FFFFFF00C8C7
            FB002928F2003230F9002927F3001D1BE600F8F8FE00FFFFFF00FFFFFF00F8F7
            FE001110DC001A18EC00211FF3001110E200C0C0F500FFFFFF00FFFFFF00DBDB
            FD002C29F3003431F9003E3DFF002A28F3001D1CE800F8F8FE00F8F8FE001614
            E1001D1CEE003231FF001F1EF000100FE100D6D6F800FFFFFF00FFFFFF00FFFF
            FF00DBDBFD002C2AF3003532F900413EFF002B29F3001E1DE8001A19E5002321
            F0003635FF002322F2001312E400D6D6F900FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00DBDBFD002C2BF3003533F900413FFF00403DFF003D3CFF003C3A
            FF002827F4001817E800D7D7FA00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00DBDBFD002D2CF4004643FF002A28FF002826FF003F3D
            FF001E1DEB00D8D8FA00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00F9F8FF002F2DF3004A46FF002E2CFF002C29FF004341
            FF001F1DE900F8F8FE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00F9F9FF003734F7003D3BFA004D4BFF004B48FF004946FF004644
            FF002E2CF400201EE900F8F8FE00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00F9F9FF003E3BFB004441FC00534FFF004340FB003331F600302DF4003B38
            F9004846FF00302DF400211FE900F8F8FE00FFFFFF00FFFFFF00FFFFFF00F9F9
            FF004240FE004A47FE005854FF004946FC003A37FA00DCDCFE00DCDBFD00302E
            F6003C3AF9004A47FF00302FF4002220EB00FBFBFF00FFFFFF00FFFFFF00E8E8
            FF004744FF004E4BFF004E4BFE00413EFC00DEDDFE00FFFFFF00FFFFFF00DCDC
            FD00312FF6003C3BFA004B49FF00312FF6009D9DF600FDFDFF00FFFFFF00FFFF
            FF00E8E8FF004744FF004643FE00DFDEFE00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00DCDCFD003330F6003836F700706FF400DCDBFC00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00E8E8FF00DFDFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00DCDCFD00A4A3FA00CBCBFC00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
            FF00FFFFFF00FFFFFF00F7F7FF00FFFFFF00FFFFFF00FFFFFF00}
          LookAndFeel.Kind = lfOffice11
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdPedido'
      '      ,Ped.nCdTipoPedido'
      '      ,cNmTipoPedido'
      '      ,Ped.nCdTerceiro'
      '      ,cNmTerceiro'
      '      ,Ped.dDtPedido'
      '      ,Ped.dDtPrevEntIni'
      '      ,Ped.dDtPrevEntFim'
      '      ,Ped.nValPedido'
      '      ,Ped.nSaldoFat'
      '      ,Ped.nCdTabStatusPed'
      '      ,cNmTabStatusPed'
      '      ,TipoPedido.cFlgBaixaManual'
      '      ,cFlgPedidoWeb'
      '  FROM Pedido Ped'
      
        '       INNER JOIN TipoPedido   ON TipoPedido.nCdTipoPedido     =' +
        ' Ped.nCdTipoPedido'
      
        '       INNER JOIN Terceiro     ON Terceiro.nCdTerceiro         =' +
        ' Ped.nCdTerceiro'
      
        '       INNER JOIN TabStatusPed ON TabStatusPed.nCdTabStatusPed =' +
        ' Ped.nCdTabStatusPed'
      ' WHERE Ped.nCdPedido            = :nPK'
      '   AND Ped.cFlgPedidoTransfEst <> 1')
    Left = 408
    Top = 392
    object qryMasternCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryMasternCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryMastercNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMastercNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMasterdDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object qryMasterdDtPrevEntIni: TDateTimeField
      FieldName = 'dDtPrevEntIni'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtPrevEntFim: TDateTimeField
      FieldName = 'dDtPrevEntFim'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasternValPedido: TBCDField
      FieldName = 'nValPedido'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternSaldoFat: TBCDField
      FieldName = 'nSaldoFat'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternCdTabStatusPed: TIntegerField
      FieldName = 'nCdTabStatusPed'
    end
    object qryMastercNmTabStatusPed: TStringField
      FieldName = 'cNmTabStatusPed'
      Size = 50
    end
    object qryMastercFlgBaixaManual: TIntegerField
      FieldName = 'cFlgBaixaManual'
    end
    object qryMastercFlgPedidoWeb: TIntegerField
      FieldName = 'cFlgPedidoWeb'
    end
  end
  inherited dsMaster: TDataSource
    Left = 408
    Top = 424
  end
  inherited qryID: TADOQuery
    Left = 536
    Top = 424
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 568
    Top = 392
  end
  inherited qryStat: TADOQuery
    Left = 504
    Top = 392
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 632
    Top = 392
  end
  inherited ImageList1: TImageList
    Left = 632
    Top = 424
  end
  object qryItemEstoque: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryItemEstoqueAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * , (nQtdePed - nQtdeExpRec - nQtdeCanc) as nSaldoItem'
      '  FROM ItemPedido '
      ' WHERE nCdPedido = :nPK'
      '   AND nCdTipoItemPed IN (1,2,3,5,6)')
    Left = 440
    Top = 392
    object qryItemEstoquenCdItemPedido: TAutoIncField
      DisplayLabel = 'ID'
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryItemEstoquenCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryItemEstoquenCdItemPedidoPai: TIntegerField
      FieldName = 'nCdItemPedidoPai'
    end
    object qryItemEstoquenCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
      ReadOnly = True
    end
    object qryItemEstoquecCdProduto: TStringField
      DisplayLabel = 'Produto|C'#243'd AD'
      FieldName = 'cCdProduto'
      ReadOnly = True
      Size = 15
    end
    object qryItemEstoquenCdTipoItemPed: TIntegerField
      FieldName = 'nCdTipoItemPed'
    end
    object qryItemEstoquenCdEstoqueMov: TIntegerField
      FieldName = 'nCdEstoqueMov'
    end
    object qryItemEstoquecNmItem: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmItem'
      ReadOnly = True
      Size = 150
    end
    object qryItemEstoquenQtdePed: TBCDField
      DisplayLabel = 'Quantidade|Pedida'
      FieldName = 'nQtdePed'
      ReadOnly = True
      Precision = 12
    end
    object qryItemEstoquenQtdeExpRec: TBCDField
      DisplayLabel = 'Quantidade|Atendida'
      FieldName = 'nQtdeExpRec'
      ReadOnly = True
      Precision = 12
    end
    object qryItemEstoquenQtdeCanc: TBCDField
      DisplayLabel = 'Quantidade|Cancelada'
      FieldName = 'nQtdeCanc'
      ReadOnly = True
      Precision = 12
    end
    object qryItemEstoquenValUnitario: TBCDField
      FieldName = 'nValUnitario'
      Precision = 12
    end
    object qryItemEstoquenPercIPI: TBCDField
      FieldName = 'nPercIPI'
      Precision = 5
      Size = 2
    end
    object qryItemEstoquenValIPI: TBCDField
      FieldName = 'nValIPI'
      Precision = 12
    end
    object qryItemEstoquenValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
    end
    object qryItemEstoquenValCustoUnit: TBCDField
      FieldName = 'nValCustoUnit'
      Precision = 12
      Size = 2
    end
    object qryItemEstoquenValSugVenda: TBCDField
      FieldName = 'nValSugVenda'
      Precision = 12
      Size = 2
    end
    object qryItemEstoquenValTotalItem: TBCDField
      FieldName = 'nValTotalItem'
      Precision = 12
      Size = 2
    end
    object qryItemEstoquenValAcrescimo: TBCDField
      FieldName = 'nValAcrescimo'
      Precision = 12
    end
    object qryItemEstoquecSiglaUnidadeMedida: TStringField
      FieldName = 'cSiglaUnidadeMedida'
      FixedChar = True
      Size = 2
    end
    object qryItemEstoquenSaldoItem: TBCDField
      DisplayLabel = 'Quantidade|Saldo'
      FieldName = 'nSaldoItem'
      ReadOnly = True
      Precision = 14
    end
  end
  object dsItemEstoque: TDataSource
    DataSet = qryItemEstoque
    Left = 440
    Top = 424
  end
  object usp_Grade: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_PREPARA_GRADE_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cSQLRetorno'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 1000
        Value = Null
      end
      item
        Name = '@nCdItemPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 568
    Top = 424
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    AfterPost = qryTempAfterPost
    AfterCancel = qryTempAfterCancel
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM ##Temp_Grade_Qtde')
    Left = 472
    Top = 392
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto, nCdGrade'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 504
    Top = 424
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutonCdGrade: TIntegerField
      FieldName = 'nCdGrade'
    end
  end
  object dsTemp: TDataSource
    DataSet = qryTemp
    Left = 472
    Top = 424
  end
  object usp_Cancela_Item: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CANCELA_ITEM_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdItemPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 600
    Top = 392
  end
  object usp_Altera_data: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ALTERA_ENTREGA_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@dDtPrevEntIni'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@dDtPrevEntFim'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = Null
      end
      item
        Name = '@cAjusteParcela'
        Attributes = [paNullable]
        DataType = ftString
        Size = 1
        Value = Null
      end>
    Left = 600
    Top = 424
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 536
    Top = 392
  end
end
