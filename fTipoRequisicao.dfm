inherited frmTipoRequisicao: TfrmTipoRequisicao
  Left = 278
  Top = 77
  Width = 882
  Height = 511
  Caption = 'Tipo de Requisi'#231#227'o'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 866
    Height = 448
  end
  object Label1: TLabel [1]
    Left = 27
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 16
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 43
    Top = 86
    Width = 22
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo'
    FocusControl = DBEdit3
  end
  inherited ToolBar2: TToolBar
    Width = 866
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 72
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTipoRequisicao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 72
    Top = 56
    Width = 625
    Height = 19
    DataField = 'cNmTipoRequisicao'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [7]
    Left = 72
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoRequis'
    DataSource = dsMaster
    TabOrder = 3
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [8]
    Tag = 1
    Left = 140
    Top = 80
    Width = 557
    Height = 19
    DataField = 'cNmTabTipoRequis'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBCheckBox1: TDBCheckBox [9]
    Left = 72
    Top = 112
    Width = 121
    Height = 17
    Caption = 'Exigir Autoriza'#231#227'o'
    DataField = 'cExigeAutor'
    DataSource = dsMaster
    TabOrder = 5
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBCheckBox2: TDBCheckBox [10]
    Left = 216
    Top = 112
    Width = 145
    Height = 17
    Caption = 'Exigir Centro de Custo'
    DataField = 'cFlgExigeCC'
    DataSource = dsMaster
    TabOrder = 6
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object cxPageControl1: TcxPageControl [11]
    Left = 8
    Top = 144
    Width = 689
    Height = 321
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 7
    ClientRectBottom = 317
    ClientRectLeft = 4
    ClientRectRight = 685
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Grupo de Produto'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 681
        Height = 293
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsGrupoProdutoTipoRequisicao
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTipoRequisicao'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdGrupoProduto'
            Footers = <>
            Title.Caption = 'Grupos de Produtos Permitidos|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmGrupoProduto'
            Footers = <>
            ReadOnly = True
            Width = 580
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Centro de Custo'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 681
        Height = 293
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsCentroCustoTipoRequisicao
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh2KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTipoRequisicao'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdCC'
            Footers = <>
            Title.Caption = 'Centro de Custo|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmCC'
            Footers = <>
            ReadOnly = True
            Width = 580
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Permiss'#245'es'
      ImageIndex = 2
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 681
        Height = 293
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsAutorizaTipoRequisicao
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh3KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Title.Caption = 'Usu'#225'rios que podem autorizar este tipo de requisi'#231#227'o|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Usu'#225'rios que podem autorizar este tipo de requisi'#231#227'o|Usu'#225'rio'
            Width = 580
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoRequisicao'
      'WHERE nCdTipoRequisicao = :nPK')
    Left = 264
    Top = 328
    object qryMasternCdTipoRequisicao: TIntegerField
      FieldName = 'nCdTipoRequisicao'
    end
    object qryMastercNmTipoRequisicao: TStringField
      FieldName = 'cNmTipoRequisicao'
      Size = 50
    end
    object qryMasternCdTabTipoRequis: TIntegerField
      FieldName = 'nCdTabTipoRequis'
    end
    object qryMastercNmTabTipoRequis: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmTabTipoRequis'
      LookupDataSet = qryTabTipoRequis
      LookupKeyFields = 'nCdTabTipoRequis'
      LookupResultField = 'cNmTabTipoRequis'
      KeyFields = 'nCdTabTipoRequis'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMastercFlgExigeCC: TIntegerField
      FieldName = 'cFlgExigeCC'
    end
    object qryMastercExigeAutor: TIntegerField
      FieldName = 'cExigeAutor'
    end
  end
  inherited dsMaster: TDataSource
    Left = 264
    Top = 360
  end
  inherited qryID: TADOQuery
    Left = 424
    Top = 328
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 456
    Top = 328
  end
  inherited qryStat: TADOQuery
    Left = 424
    Top = 360
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 456
    Top = 360
  end
  inherited ImageList1: TImageList
    Left = 488
    Top = 328
  end
  object qryTabTipoRequis: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM TabTipoRequis')
    Left = 392
    Top = 360
    object qryTabTipoRequisnCdTabTipoRequis: TIntegerField
      FieldName = 'nCdTabTipoRequis'
    end
    object qryTabTipoRequiscNmTabTipoRequis: TStringField
      FieldName = 'cNmTabTipoRequis'
      Size = 50
    end
  end
  object qryGrupoProdutoTipoRequisicao: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryGrupoProdutoTipoRequisicaoBeforePost
    OnCalcFields = qryGrupoProdutoTipoRequisicaoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoProdutoTipoRequisicao'
      'WHERE nCdTipoRequisicao = :nPK')
    Left = 296
    Top = 328
    object qryGrupoProdutoTipoRequisicaonCdTipoRequisicao: TIntegerField
      FieldName = 'nCdTipoRequisicao'
    end
    object qryGrupoProdutoTipoRequisicaonCdGrupoProduto: TIntegerField
      DisplayLabel = 'Grupos de Produtos Permitidos|C'#243'd'
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutoTipoRequisicaocNmGrupoProduto: TStringField
      DisplayLabel = 'Grupos de Produtos Permitidos|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmGrupoProduto'
      Size = 50
      Calculated = True
    end
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmGrupoProduto, nCdGrupoProduto'
      'FROM GrupoProduto'
      'WHERE nCdGrupoProduto = :nPK')
    Left = 360
    Top = 328
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
  end
  object dsGrupoProdutoTipoRequisicao: TDataSource
    DataSet = qryGrupoProdutoTipoRequisicao
    Left = 296
    Top = 360
  end
  object qryCentroCustoTipoRequisicao: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCentroCustoTipoRequisicaoBeforePost
    OnCalcFields = qryCentroCustoTipoRequisicaoCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CentroCustoTipoRequisicao'
      'WHERE nCdTipoRequisicao = :nPK')
    Left = 328
    Top = 328
    object qryCentroCustoTipoRequisicaonCdTipoRequisicao: TIntegerField
      FieldName = 'nCdTipoRequisicao'
    end
    object qryCentroCustoTipoRequisicaonCdCC: TIntegerField
      DisplayLabel = 'Centro de Custo|C'#243'd'
      FieldName = 'nCdCC'
    end
    object qryCentroCustoTipoRequisicaocNmCC: TStringField
      DisplayLabel = 'Centro de Custo|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCC'
      Size = 50
      Calculated = True
    end
  end
  object dsCentroCustoTipoRequisicao: TDataSource
    DataSet = qryCentroCustoTipoRequisicao
    Left = 328
    Top = 360
  end
  object qryCentroCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmCC'
      'FROM CentroCusto'
      'WHERE cFlgLanc = 1'
      'AND nCdCC = :nPK')
    Left = 360
    Top = 360
    object qryCentroCustocNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object qryAutorizaTipoReq: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryAutorizaTipoReqBeforePost
    OnCalcFields = qryAutorizaTipoReqCalcFields
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from usuarioTipoRequisicaoAutor'
      'where nCdTipoRequisicao= :nPk')
    Left = 488
    Top = 360
    object qryAutorizaTipoReqnCdUsuarioTipoRequisicaoAutor: TAutoIncField
      FieldName = 'nCdUsuarioTipoRequisicaoAutor'
      ReadOnly = True
    end
    object qryAutorizaTipoReqnCdTipoRequisicao: TIntegerField
      FieldName = 'nCdTipoRequisicao'
    end
    object qryAutorizaTipoReqnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryAutorizaTipoReqcNmUsuario: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 25
      Calculated = True
    end
  end
  object dsAutorizaTipoRequisicao: TDataSource
    DataSet = qryAutorizaTipoReq
    Left = 524
    Top = 360
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario, cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 392
    Top = 328
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
end
