unit fListaCobranca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, cxLookAndFeelPainters, StdCtrls, cxButtons,
  ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, DB, ADODB,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmListaCobranca = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryLista: TADOQuery;
    dsLista: TDataSource;
    qryListanCdListaCobranca: TIntegerField;
    qryListadDtCobranca: TDateTimeField;
    qryListadDtVencto: TDateTimeField;
    qryListaiSeq: TIntegerField;
    qryListacNmEtapacobranca: TStringField;
    qryListaiQtdeClienteTotal: TIntegerField;
    qryListaiQtdeClienteRestante: TIntegerField;
    qryListadDtAtribuicao: TDateTimeField;
    qryListadDtAbertura: TDateTimeField;
    lblAnalista: TLabel;
    qryListacFlgTelefone: TIntegerField;
    qryListadDtEncerramento: TDateTimeField;
    qryListacFlgNegativar: TIntegerField;
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmListaCobranca: TfrmListaCobranca;

implementation

uses fCobranca_VisaoContato, fMenu, fListaCobranca_Itens;

{$R *.dfm}

procedure TfrmListaCobranca.cxButton1Click(Sender: TObject);
var
  objForm : TfrmCobranca_VisaoContato;
begin
  inherited;

  objForm := TfrmCobranca_VisaoContato.Create(nil);

  objForm.ExibeDados(2);

end;

procedure TfrmListaCobranca.FormShow(Sender: TObject);
begin
  inherited;
  DBGridEh1.Align := alClient;

  DBGridEh1.Columns[7].Width := 154 ;
  DBGridEh1.Columns[8].Width := 154 ;

  lblAnalista.Caption    := 'Analista de Cobran�a: ' + frmMenu.cNmCompletoUsuario;
  lblAnalista.Font.Size  := 12 ;
  lblAnalista.Font.Color := clBlue ;

  qrylista.Close ;
  qryLista.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qrylista.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  PosicionaQuery(qryLista, intToStr(frmMenu.nCdUsuarioLogado)) ;

  if (qryLista.eof) then
      ShowMessage('Nenhuma lista pendende de contatos atribuida a voc�.') ;

end;

procedure TfrmListaCobranca.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qrylista.Close ;
  qryLista.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qrylista.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  PosicionaQuery(qryLista, intToStr(frmMenu.nCdUsuarioLogado)) ;

end;

procedure TfrmListaCobranca.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmListaCobranca_Itens;
begin
  inherited;

  if (qryLista.Active) then
  begin
      objForm := TfrmListaCobranca_Itens.Create(Self);
      objForm.ExibeItensLista(qryListanCdListaCobranca.Value, false,((qryListacFlgTelefone.Value=1) or (qryListadDtEncerramento.asString <> '')), (qryListacFlgNegativar.Value=1)) ;
  end ;

end;

initialization
    RegisterClass(TfrmListaCobranca) ;

end.
