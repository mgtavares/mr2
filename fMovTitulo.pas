unit fMovTitulo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ImgList, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons, cxPC, cxControls, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmMovTitulo = class(TfrmCadastro_Padrao)
    qryMasternCdTitulo: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdEspTit: TIntegerField;
    qryMastercNrTit: TStringField;
    qryMasteriParcela: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdCategFinanc: TIntegerField;
    qryMasternCdMoeda: TIntegerField;
    qryMastercSenso: TStringField;
    qryMasterdDtEmissao: TDateTimeField;
    qryMasterdDtReceb: TDateTimeField;
    qryMasterdDtVenc: TDateTimeField;
    qryMasterdDtLiq: TDateTimeField;
    qryMasterdDtCad: TDateTimeField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternValTit: TBCDField;
    qryMasternValLiq: TBCDField;
    qryMasternSaldoTit: TBCDField;
    qryMasternValJuro: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMastercObsTit: TMemoField;
    qryMasternCdSP: TIntegerField;
    qryMasternCdBancoPortador: TIntegerField;
    qryMasterdDtRemPortador: TDateTimeField;
    qryMasterdDtBloqTit: TDateTimeField;
    qryMastercObsBloqTit: TStringField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitnCdGrupoEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitcFlgPrev: TIntegerField;
    qryCategFinanc: TADOQuery;
    qryCategFinancnCdCategFinanc: TIntegerField;
    qryCategFinanccNmCategFinanc: TStringField;
    qryCategFinancnCdGrupoCategFinanc: TIntegerField;
    qryCategFinanccFlgRecDes: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceironCdStatus: TIntegerField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    Label17: TLabel;
    DBEdit17: TDBEdit;
    Label18: TLabel;
    DBEdit18: TDBEdit;
    Label19: TLabel;
    DBEdit19: TDBEdit;
    Label20: TLabel;
    DBEdit20: TDBEdit;
    Label21: TLabel;
    DBEdit21: TDBEdit;
    Label24: TLabel;
    DBMemo1: TDBMemo;
    qryTerceironCdMoeda: TIntegerField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryMasternCdUnidadeNegocio: TIntegerField;
    Label27: TLabel;
    DBEdit34: TDBEdit;
    qryMasternCdCC: TIntegerField;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    qryCCnCdCC1: TIntegerField;
    qryCCnCdCC2: TIntegerField;
    qryCCcCdCC: TStringField;
    qryCCcCdHie: TStringField;
    qryCCiNivel: TSmallintField;
    qryCCcFlgLanc: TIntegerField;
    qryCCnCdStatus: TIntegerField;
    qryMTitulo: TADOQuery;
    qryMTituloMovimentoID: TAutoIncField;
    qryMTituloOperaoConta: TStringField;
    qryMTituloOperaoCdigo: TIntegerField;
    qryMTituloOperaoDescrio: TStringField;
    qryMTituloFormaMovimentoCdigo: TIntegerField;
    qryMTituloFormaMovimentoDescrio: TStringField;
    qryMTituloMovimentoNrCheque: TIntegerField;
    qryMTituloMovimentoValor: TBCDField;
    qryMTituloMovimentoData: TDateTimeField;
    qryMTituloMovimentoCadastro: TDateTimeField;
    qryMTituloMovimentoCancel: TDateTimeField;
    qryMTituloMovimentoNrDoc: TStringField;
    qryMTituloMovimentoContab: TDateTimeField;
    dsMovimento: TDataSource;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    qryOperacao: TADOQuery;
    qryOperacaonCdOperacao: TIntegerField;
    qryOperacaocNmOperacao: TStringField;
    qryOperacaocTipoOper: TStringField;
    qryOperacaocSinalOper: TStringField;
    qryMTitulonCdUsuarioCancel: TIntegerField;
    qryMTitulocFlgMovAutomatico: TIntegerField;
    qryAux: TADOQuery;
    qryMTitulonCdContaBancaria: TIntegerField;
    qryMTitulonCdLanctoFin: TIntegerField;
    qryMastercNrNF: TStringField;
    qryMasternCdProvisaoTit: TIntegerField;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    qryMTitulocObsMov: TMemoField;
    btAbatimento: TcxButton;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancariacAgencia: TIntegerField;
    qryContaBancarianCdCOnta: TStringField;
    qryContaBancariacNmTitular: TStringField;
    DBEdit10: TDBEdit;
    dsContaBancaria: TDataSource;
    DBEdit11: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label11: TLabel;
    qryMasternCdContaBancariaDA: TIntegerField;
    qryMasternCdTipoLanctoDA: TIntegerField;
    qryMasternCdLojaTit: TIntegerField;
    Label13: TLabel;
    DBEdit22: TDBEdit;
    dsLoja: TDataSource;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    dsEmpresa: TDataSource;
    DBEdit25: TDBEdit;
    dsUnidadeNegocio: TDataSource;
    DBEdit26: TDBEdit;
    dsEspTit: TDataSource;
    DBEdit27: TDBEdit;
    dsTerceiro: TDataSource;
    qryTerceironCdGrupoEconomico: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryMasternValAbatimento: TBCDField;
    Label10: TLabel;
    DBEdit28: TDBEdit;
    qryContaBancariaMovto: TADOQuery;
    qryContaBancariaMovtonCdContaBancaria: TIntegerField;
    qryContaBancariaMovtonCdEmpresa: TIntegerField;
    qryContaBancariaMovtonCdBanco: TIntegerField;
    qryContaBancariaMovtocAgencia: TIntegerField;
    qryContaBancariaMovtonCdConta: TStringField;
    qryContaBancariaMovtonCdCC: TIntegerField;
    qryContaBancariaMovtocFlgFluxo: TIntegerField;
    qryContaBancariaMovtoiUltimoCheque: TIntegerField;
    qryContaBancariaMovtoiUltimoBordero: TIntegerField;
    qryContaBancariaMovtonValLimiteCredito: TBCDField;
    qryContaBancariaMovtocFlgDeposito: TIntegerField;
    qryContaBancariaMovtocFlgEmiteCheque: TIntegerField;
    qryContaBancariaMovtocFlgCaixa: TIntegerField;
    qryContaBancariaMovtonCdLoja: TIntegerField;
    qryContaBancariaMovtonCdUsuarioOperador: TIntegerField;
    qryContaBancariaMovtonSaldoConta: TBCDField;
    qryContaBancariaMovtocFlgCofre: TIntegerField;
    qryContaBancariaMovtodDtUltConciliacao: TDateTimeField;
    qryContaBancariaMovtocFlgEmiteBoleto: TIntegerField;
    qryContaBancariaMovtocNmTitular: TStringField;
    qryContaBancariaMovtocFlgProvTit: TIntegerField;
    qryContaBancariaMovtonSaldoInicial: TBCDField;
    qryContaBancariaMovtodDtSaldoInicial: TDateTimeField;
    qryContaBancariaMovtocFlgReapresentaCheque: TIntegerField;
    qryContaBancariaMovtonCdServidorOrigem: TIntegerField;
    qryContaBancariaMovtodDtReplicacao: TDateTimeField;
    qryContaBancariaMovtoiUltimoBoleto: TIntegerField;
    qryTituloAdiantReceber: TADOQuery;
    qryMTituloAdiantReceber: TADOQuery;
    qryMTituloAdiantRecebernCdMTitulo: TIntegerField;
    qryMTituloAdiantRecebernCdTitulo: TIntegerField;
    qryMTituloAdiantRecebernCdContaBancaria: TIntegerField;
    qryMTituloAdiantRecebernCdOperacao: TIntegerField;
    qryMTituloAdiantRecebernCdFormaPagto: TIntegerField;
    qryMTituloAdiantReceberiNrCheque: TIntegerField;
    qryMTituloAdiantRecebernValMov: TBCDField;
    qryMTituloAdiantReceberdDtMov: TDateTimeField;
    qryMTituloAdiantReceberdDtCad: TDateTimeField;
    qryMTituloAdiantReceberdDtCancel: TDateTimeField;
    qryMTituloAdiantRecebercNrDoc: TStringField;
    qryMTituloAdiantReceberdDtContab: TDateTimeField;
    qryMTituloAdiantRecebernCdUsuarioCad: TIntegerField;
    qryMTituloAdiantRecebernCdUsuarioCancel: TIntegerField;
    qryMTituloAdiantRecebercObsMov: TMemoField;
    qryMTituloAdiantRecebernCdCC: TIntegerField;
    qryMTituloAdiantRecebercFlgMovAutomatico: TIntegerField;
    qryMTituloAdiantReceberdDtBomPara: TDateTimeField;
    qryMTituloAdiantRecebernCdLanctoFin: TIntegerField;
    qryMTituloAdiantRecebercFlgIntegrado: TIntegerField;
    qryMTituloAdiantRecebernCdProvisaoTit: TIntegerField;
    qryMTituloAdiantRecebercFlgMovDA: TIntegerField;
    qryMTituloAdiantReceberdDtIntegracao: TDateTimeField;
    qryMTituloAdiantRecebernCdServidorOrigem: TIntegerField;
    qryMTituloAdiantReceberdDtReplicacao: TDateTimeField;
    qryTituloAdiantRecebernCdTitulo: TIntegerField;
    qryTituloAdiantRecebernCdEmpresa: TIntegerField;
    qryTituloAdiantRecebernCdEspTit: TIntegerField;
    qryTituloAdiantRecebercNrTit: TStringField;
    qryTituloAdiantReceberiParcela: TIntegerField;
    qryTituloAdiantRecebernCdTerceiro: TIntegerField;
    qryTituloAdiantRecebernCdCategFinanc: TIntegerField;
    qryTituloAdiantRecebernCdMoeda: TIntegerField;
    qryTituloAdiantRecebercSenso: TStringField;
    qryTituloAdiantReceberdDtEmissao: TDateTimeField;
    qryTituloAdiantReceberdDtReceb: TDateTimeField;
    qryTituloAdiantReceberdDtVenc: TDateTimeField;
    qryTituloAdiantReceberdDtLiq: TDateTimeField;
    qryTituloAdiantReceberdDtCad: TDateTimeField;
    qryTituloAdiantReceberdDtCancel: TDateTimeField;
    qryTituloAdiantRecebernValTit: TBCDField;
    qryTituloAdiantRecebernValLiq: TBCDField;
    qryTituloAdiantRecebernSaldoTit: TBCDField;
    qryTituloAdiantRecebernValJuro: TBCDField;
    qryTituloAdiantRecebernValDesconto: TBCDField;
    qryTituloAdiantRecebercObsTit: TMemoField;
    qryTituloAdiantRecebernCdSP: TIntegerField;
    qryTituloAdiantRecebernCdBancoPortador: TIntegerField;
    qryTituloAdiantReceberdDtRemPortador: TDateTimeField;
    qryTituloAdiantReceberdDtBloqTit: TDateTimeField;
    qryTituloAdiantRecebercObsBloqTit: TStringField;
    qryTituloAdiantRecebernCdUnidadeNegocio: TIntegerField;
    qryTituloAdiantRecebernCdCC: TIntegerField;
    qryTituloAdiantReceberdDtContab: TDateTimeField;
    qryTituloAdiantRecebernCdDoctoFiscal: TIntegerField;
    qryTituloAdiantReceberdDtCalcJuro: TDateTimeField;
    qryTituloAdiantRecebernCdRecebimento: TIntegerField;
    qryTituloAdiantRecebernCdCrediario: TIntegerField;
    qryTituloAdiantRecebernCdTransacaoCartao: TIntegerField;
    qryTituloAdiantRecebernCdLanctoFin: TIntegerField;
    qryTituloAdiantRecebercFlgIntegrado: TIntegerField;
    qryTituloAdiantRecebercFlgDocCobranca: TIntegerField;
    qryTituloAdiantRecebercNrNF: TStringField;
    qryTituloAdiantRecebernCdProvisaoTit: TIntegerField;
    qryTituloAdiantRecebercFlgDA: TIntegerField;
    qryTituloAdiantRecebernCdContaBancariaDA: TIntegerField;
    qryTituloAdiantRecebernCdTipoLanctoDA: TIntegerField;
    qryTituloAdiantRecebernCdLojaTit: TIntegerField;
    qryTituloAdiantRecebercFlgInclusaoManual: TIntegerField;
    qryTituloAdiantReceberdDtVencOriginal: TDateTimeField;
    qryTituloAdiantRecebercFlgCartaoConciliado: TIntegerField;
    qryTituloAdiantRecebernCdGrupoOperadoraCartao: TIntegerField;
    qryTituloAdiantRecebernCdOperadoraCartao: TIntegerField;
    qryTituloAdiantRecebernValTaxaOperadora: TBCDField;
    qryTituloAdiantRecebernCdLoteConciliacaoCartao: TIntegerField;
    qryTituloAdiantReceberdDtIntegracao: TDateTimeField;
    qryTituloAdiantRecebernCdParcContratoEmpImobiliario: TIntegerField;
    qryTituloAdiantRecebernValAbatimento: TBCDField;
    qryTituloAdiantRecebernValMulta: TBCDField;
    qryTituloAdiantReceberdDtNegativacao: TDateTimeField;
    qryTituloAdiantRecebernCdUsuarioNeg: TIntegerField;
    qryTituloAdiantRecebernCdItemListaCobrancaNeg: TIntegerField;
    qryTituloAdiantRecebernCdLojaNeg: TIntegerField;
    qryTituloAdiantReceberdDtReabNeg: TDateTimeField;
    qryTituloAdiantRecebercFlgCobradora: TIntegerField;
    qryTituloAdiantRecebernCdCobradora: TIntegerField;
    qryTituloAdiantReceberdDtRemCobradora: TDateTimeField;
    qryTituloAdiantRecebernCdUsuarioRemCobradora: TIntegerField;
    qryTituloAdiantRecebernCdItemListaCobRemCobradora: TIntegerField;
    qryTituloAdiantRecebercFlgReabManual: TIntegerField;
    qryTituloAdiantRecebernCdUsuarioReabManual: TIntegerField;
    qryTituloAdiantRecebernCdFormaPagtoTit: TIntegerField;
    qryTituloAdiantRecebernPercTaxaJurosDia: TBCDField;
    qryTituloAdiantRecebernPercTaxaMulta: TBCDField;
    qryTituloAdiantReceberiDiaCarenciaJuros: TIntegerField;
    qryTituloAdiantRecebercFlgRenegociado: TIntegerField;
    qryTituloAdiantRecebercCodBarra: TStringField;
    qryTituloAdiantRecebernValEncargoCobradora: TBCDField;
    qryTituloAdiantRecebercFlgRetCobradoraManual: TIntegerField;
    qryTituloAdiantRecebernCdServidorOrigem: TIntegerField;
    qryTituloAdiantReceberdDtReplicacao: TDateTimeField;
    qryTituloAdiantRecebernValIndiceCorrecao: TBCDField;
    qryTituloAdiantRecebernValCorrecao: TBCDField;
    qryTituloAdiantRecebercFlgNegativadoManual: TIntegerField;
    qryTituloAdiantRecebercIDExternoTit: TStringField;
    qryTituloAdiantRecebernCdTipoAdiantamento: TIntegerField;
    qryTituloAdiantRecebercFlgAdiantamento: TIntegerField;
    qryTituloAdiantRecebernCdMTituloGerador: TIntegerField;
    qryMTitulonCdMTitulo: TIntegerField;
    qryMTitulo1: TADOQuery;
    qryMTitulo1nCdMTitulo: TIntegerField;
    qryMTitulonCdTitulo: TIntegerField;
    qryAjustaSaldoCaixa: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBEdit34KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit22KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure btAbatimentoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMovTitulo: TfrmMovTitulo;

implementation

uses fMenu, fLookup_Padrao, fMovTitulo_Abatimento, fMTitulo_Movimenta;

{$R *.dfm}

procedure TfrmMovTitulo.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TITULO' ;
  nCdTabelaSistema  := 21 ;
  nCdConsultaPadrao := 36 ;
end;

procedure TfrmMovTitulo.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
      MensagemAlerta('Esta t�tulo n�o pertence a esta empresa.') ;
      btCancelar.Click;
      exit ;
  end ;

  If (qryMasterdDtCancel.AsString <> '') then
  begin
    qryMaster.Close ;
    MensagemAlerta('T�tulo cancelado, imposs�vel movimentar.') ;
    btCancelar.Click;
    exit ;
  end;

  If (qryMasterdDtBloqTit.AsString <> '') then
  begin
    qryMaster.Close ;
    MensagemAlerta('T�tulo bloqueado para movimenta��es.') ;
    btCancelar.Click;
    exit ;
  end;

  qryMTitulo.Close ;
  qryMTitulo.Parameters.ParamByName('nCdTitulo').Value := qryMasternCdTitulo.Value ;
  qryMTitulo.Open ;

  qryContaBancaria.Close ;
  PosicionaQuery(qryContaBancaria, qryMasternCdContaBancariaDA.asString) ;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, qryMasternCdLojaTit.AsString) ;

  qryEmpresa.Close ;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;

  qryUnidadeNegocio.Close ;
  PosicionaQuery(qryUnidadeNegocio, qryMasternCdUnidadeNegocio.AsString) ;

  qryEspTit.Close ;
  PosicionaQuery(qryEspTit, qryMasternCdEspTit.AsString) ;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.AsString) ;

  btAbatimento.Enabled := False ;

  if (qryMasternSaldoTit.Value > 0) and (qryMasterdDtCancel.AsString = '') and (qryMastercSenso.Value = 'D') then
  begin
      btAbatimento.Enabled := True ;
  end ;

end;

procedure TfrmMovTitulo.DBEdit34KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(35,'EXISTS(SELECT 1 FROM EmpresaUnidadeNegocio EUN WHERE EUN.nCdUnidadeNegocio = UnidadeNegocio.nCdUnidadeNegocio AND EUN.nCdEmpresa = ' + DBEdit2.Text + ')');

            If (nPK > 0) then
            begin
                qryMasternCdUnidadeNegocio.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmMovTitulo.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(38);

            If (nPK > 0) then
            begin
                qryMasternCdEspTit.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmMovTitulo.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmMovTitulo.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(13,'cFlgRecDes = ' + Chr(39) + 'R' + Chr(39));

            If (nPK > 0) then
            begin
                qryMasternCdCategFinanc.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmMovTitulo.DBEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(16);

            If (nPK > 0) then
            begin
                qryMasternCdMoeda.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmMovTitulo.DBEdit22KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(23);

            If (nPK > 0) then
            begin
                qryMasternCdBancoPortador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmMovTitulo.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryMTitulo.Close ;
  qryLoja.Close ;
  qryContaBancaria.Close ;

  qryEmpresa.Close ;
  qryUnidadeNegocio.Close ;
  qryEspTit.Close ;
  qryTerceiro.Close ;

end;

procedure TfrmMovTitulo.cxButton2Click(Sender: TObject);
var
    nCdLanctoFin : integer ;
    nPk          : integer ;

begin
  inherited;

  if (qryMaster.Eof) then
      Exit;

  if not (qryMTitulo.Active) then
  begin
      MensagemAlerta('Nenhum movimento ativo.') ;
      exit;
  end ;

  if (qryMTituloMovimentoID.Value = 0) then
  begin
      MensagemAlerta('Nenhum movimento selecionado.') ;
      exit ;
  end ;

  if (qryMTituloMovimentoContab.AsString <> '') then
  begin
      MensagemAlerta('Movimento j� foi contabilidado, imposs�vel cancelar.') ;
      exit ;
  end ;

  if (qryMTituloMovimentoCancel.AsString <> '') then
  begin
      MensagemAlerta('Movimento j� foi cancelado.') ;
      exit ;
  end ;

  if (qryMTitulocFlgMovAutomatico.Value = 1) then
  begin
      MensagemErro('Movimento autom�tico n�o pode ser cancelado.') ;
      exit ;
  end ;

  if (qryEspTitcFlgPrev.Value = 1) then
  begin
      MensagemAlerta('Este t�tulo � somente uma provis�o e n�o pode ser movimentado.') ;
      exit ;
  end ;

  if (qryMasternCdProvisaoTit.Value > 0) then
  begin
      MensagemAlerta('Este t�tulo est� provisionado e n�o pode ser movimentado.') ;
      exit ;
  end ;

  if (qryMasternSaldoTit.Value <= 0) then
  begin
      //
      // Se o titulo n�o tiver saldo e o movimento de liquida��o foi autom�tico, n�o permite altera��es.
      //
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT 1 FROM MTitulo INNER JOIN Operacao ON Operacao.nCdOperacao = MTitulo.nCdOperacao WHERE cTipoOper = ' + Chr(39) + 'L' + Chr(39) + ' AND MTitulo.nCdTitulo = ' + qryMasternCdTitulo.AsString + ' AND cFlgMovAutomatico = 1') ;
      qryaux.Open ;

      if not qryAux.eof then
      begin
          qryAux.Close ;
          MensagemAlerta('Este t�tulo foi liquidado por movimenta��o autom�tica e n�o pode sofrer altera��es.') ;
          exit ;
      end ;

      qryAux.Close ;

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT 1 FROM MTitulo WHERE MTitulo.nCdTitulo = ' + qryMasternCdTitulo.AsString + ' AND cFlgMovAutomatico = 1 AND MTitulo.nCdMTitulo > ' + qryMTituloMovimentoID.asString) ;
      qryaux.Open ;

      if not qryAux.eof then
      begin
          qryAux.Close ;
          MensagemAlerta('Este t�tulo temp movimenta��o autom�tica posterior a este lan�amento que voc� est� tentando cancelar.' +#13#13 + 'O t�tulo n�o pode sofrer altera��es.') ;
          exit ;
      end ;

      qryAux.Close ;

  end ;


  if (qryMTituloFormaMovimentoCdigo.Value = 3) then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT 1 FROM Cheque WHERE nCdTabTipoCheque = 1 AND cFlgCompensado = 1 AND iNrCheque = ' + qryMTituloMovimentoNrCheque.AsString + ' AND nCdContaBancaria = ' + qryMTitulonCdContaBancaria.AsString) ;
      qryAux.Open ;

      if not qryAux.Eof then
      begin
          qryAux.Close ;
          MensagemAlerta('Cheque j� foi compensado. Imposs�vel cancelar o movimento.') ;
          exit ;
      end ;

      qryAux.Close ;

  end ;

  if (qryMTitulonCdLanctoFin.Value > 0) then
  begin

      PosicionaQuery(qryContaBancariaMovto,qryMTitulonCdContaBancaria.AsString);

      if (qryContaBancariaMovtocFlgCofre.Value <> 1) then
      begin

          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('SELECT cFlgConciliado FROM LanctoFin WHERE nCdLanctoFin = ' + qryMTitulonCdLanctoFin.AsString) ;
          qryAux.Open ;

          if not qryAux.Eof then
              if (qryAux.FieldList[0].Value = 1) then
              begin
                  qryAux.Close ;
                  MensagemAlerta('Lan�amento financeiro j� foi conciliado, imposs�vel cancelar o movimento.') ;
                  exit ;
              end ;

          qryAux.Close ;
      end;

  end ;

  PosicionaQuery(qryTituloAdiantReceber,qryMTitulonCdMTitulo.AsString);
  PosicionaQuery(qryMTituloAdiantReceber,qryTituloAdiantRecebernCdTitulo.AsString);

  if not qryMTituloAdiantReceber.Eof then qryMTituloAdiantReceber.First;
  while not qryMTituloAdiantReceber.Eof do
  begin
      if (qryMTituloAdiantReceberdDtCancel.AsString = '') then
      begin
          MensagemAlerta('T�tulo a Receber de Adiantamento j� foi movimentado, imposs�vel cancelar o movimento.') ;
          exit ;
      end;
      qryMTituloAdiantReceber.Next;
  end;

  case MessageDlg('Confirma o cancelamento deste movimento ?',mtConfirmation,mbYesNoCancel,0) of
        mrNo:exit;
        mrCancel: exit;
  end ;

  qryOperacao.Close ;
  qryOperacao.Parameters.ParamByName('nCdOperacao').Value := qryMTituloOperaoCdigo.Value ;
  qryOperacao.Open ;

  frmMenu.Connection.BeginTrans ;

  try

      qryMaster.Edit ;

      if (qryOperacaocTipoOper.Value = 'P') then
      begin
          qryMasternValLiq.Value := qryMasternValLiq.Value - qryMTituloMovimentoValor.Value ;
      end ;

      if (qryOperacaocTipoOper.Value = 'J') then
      begin
          qryMasternValJuro.Value := qryMasternValJuro.Value - qryMTituloMovimentoValor.Value ;
      end ;

      if (qryOperacaocTipoOper.Value = 'D') then
      begin
          qryMasternValDesconto.Value := qryMasternValDesconto.Value - qryMTituloMovimentoValor.Value ;
      end ;

      if (qryOperacaocSinalOper.Value = '+') then
          qryMasternSaldoTit.Value := qryMasternSaldoTit.Value - qryMTituloMovimentoValor.Value ;

      if (qryOperacaocSinalOper.Value = '-') then
          qryMasternSaldoTit.Value := qryMasternSaldoTit.Value + qryMTituloMovimentoValor.Value ;

      if (qryMasternSaldoTit.Value < 0) then
      begin
          qryMaster.Cancel;
          MensagemAlerta('O Saldo do t�tulo n�o pode ser inferior a zero.') ;
          abort ;
          exit ;
      end ;

      if (qryMasternSaldoTit.Value > 0) then
          qryMasterdDtLiq.asString := '' ;

      qryMaster.Post ;

      nCdLanctoFin := qryMTitulonCdLanctoFin.Value ;

      qryMTitulo.Edit ;
      qryMTituloMovimentoCancel.Value  := Now() ;
      qryMTitulonCdUsuarioCancel.Value := frmMenu.nCdUsuarioLogado ;
      qryMTitulonCdLanctoFin.asString  := '' ;
      qryMTitulo.Post ;

      // cancela o lan�amento
      if (nCdLanctoFin > 0) then
      begin

          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('DELETE FROM LanctoFin WHERE nCdLanctoFin = ' + IntToStr(nCdLanctoFin)) ;
          qryAux.ExecSQL;

          {-- se for uma conta caixa/cofre, ajusta o saldo do caixa. --}
          if (qryContaBancariaMovtocFlgCaixa.Value = 1) then
          begin

              qryAjustaSaldoCaixa.Close;
              qryAjustaSaldoCaixa.Parameters.ParamByName('nCdContaBancaria').Value := qryContaBancariaMovtonCdContaBancaria.Value;
              qryAjustaSaldoCaixa.Parameters.ParamByName('dDtLancto').Value        := dateToStr( qryMTituloMovimentoCadastro.Value );
              qryAjustaSaldoCaixa.Parameters.ParamByName('nValLancto').Value       := qryMTituloMovimentoValor.Value;
              qryAjustaSaldoCaixa.Parameters.ParamByName('cSenso').Value           := qryMastercSenso.Value;
              qryAjustaSaldoCaixa.ExecSQL;

          end ;

      end ;

      // cancela o cheque
      if (qryMTituloFormaMovimentoCdigo.Value = 3) then
      begin

          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('DELETE FROM Cheque WHERE nCdTabTipoCheque = 1 AND cFlgCompensado = 0 AND iNrCheque = ' + qryMTituloMovimentoNrCheque.AsString + ' AND nCdContaBancaria = ' + qryMTitulonCdContaBancaria.AsString) ;
          qryAux.ExecSQL;
          
      end ;

      // cancela a contabiliza��o
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM LanctoCentroCusto WHERE nCdMTitulo = ' + qryMTitulonCdMtitulo.asString) ;
      qryAux.ExecSQL;

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM LanctoPlanoConta WHERE nCdMTitulo = ' + qryMTitulonCdMtitulo.asString) ;
      qryAux.ExecSQL;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no cancelamento do movimento.') ;
      raise ;
      exit ;
  end ;

  frmMenu.Connection.CommitTrans ;

  ShowMessage('Movimento cancelado com sucesso.') ;

  {qryMTitulo.Close ;
  qryMTitulo.Open ;}

end;

procedure TfrmMovTitulo.cxButton1Click(Sender: TObject);
var
  objMTitulo : TfrmMTitulo_Movimenta ;
begin
  if (qryMaster.Eof) then
      Exit;

  if not qryMaster.Active then
  begin
      MensagemAlerta('Selecione um t�tulo.') ;
      exit ;
  end ;

  if (qryEspTitcFlgPrev.Value = 1) then
  begin
      MensagemAlerta('Este t�tulo � somente uma previs�o e n�o pode ser movimentado.') ;
      exit ;
  end ;

  if (qryMasternCdProvisaoTit.Value > 0) then
  begin
      MensagemAlerta('Este t�tulo est� provisionado e n�o pode ser movimentado.') ;
      exit ;
  end ;

  if (qryMasternSaldoTit.Value <= 0) then
  begin
      MensagemAlerta('Este t�tulo est� liquidado e n�o pode ser movimentado.') ;
      exit ;
  end ;

  if (qryMasternCdContaBancariaDA.Value > 0) then
  begin
      MensagemAlerta('Este t�tulo est� em d�bito autom�tico') ;
  end ;

  objMTitulo := TfrmMTitulo_Movimenta.Create(Self) ;

  objMTitulo.nCdTitulo   := qryMasternCdTitulo.Value ;
  objMTitulo.cNmTerceiro := qryTerceirocNmTerceiro.Value ;

  showForm( objMTitulo , TRUE ) ;

  qryMaster.Close ;
  qryMaster.Open  ;
  
  qryMTitulo.Close ;
  qryMTitulo.Open ;
end;

procedure TfrmMovTitulo.btAbatimentoClick(Sender: TObject);
var
  objForm : TfrmMovTitulo_Abatimento ;
begin
  if (qryMaster.Eof) then
      Exit;

  if not (qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum t�tulo ativo.') ;
      exit;
  end ;

  if (qryMastercSenso.Value <> 'D') then
  begin
      MensagemAlerta('O abatimento s� pode ser realizado em t�tulos a pagar.') ;
      exit ;
  end ;

  if (qryMasternSaldoTit.Value <= 0) then
  begin
      MensagemAlerta('N�o existe saldo no t�tulo para abater.') ;
      exit ;
  end ;

  objForm := TfrmMovTitulo_Abatimento.Create(Self) ;

  objForm.nCdTerceiro       := qryMasternCdTerceiro.Value         ;
  objForm.nCdTitulo         := qryMasternCdTitulo.Value           ;
  objForm.nSaldoTit         := qryMasternSaldoTit.Value           ;
  objForm.nCdGrupoEconomico := qryTerceironCdGrupoEconomico.Value ;
  showForm( objForm , TRUE ) ;

  PosicionaQuery(qryMaster, qryMasternCdTitulo.AsString) ;

end;

procedure TfrmMovTitulo.FormShow(Sender: TObject);
begin
  inherited;
  qryMaster.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

procedure TfrmMovTitulo.btConsultarClick(Sender: TObject);
var
  nPK : integer;
  TipoAcesso : string;
begin
  {inherited;}
  TipoAcesso := #39 + 'M' + #39;
  nPK := frmLookup_Padrao.ExecutaConsulta2(36,'Titulo.dDtCancel is null AND Exists(SELECT 1 FROM UsuarioEspTit WHERE UsuarioEspTit.nCdEspTit = EspTit.nCdEspTit AND UsuarioEspTit.nCdUsuario = '+ IntToStr(frmMenu.nCdUsuarioLogado) +' AND UsuarioEspTit.cFlgTipoAcesso = ' + TipoAcesso + ')');

  If (nPK > 0) then
  begin
      PosicionaQuery(qryMaster, IntToStr(nPK));
  end ;
end;

initialization
    RegisterClass(TfrmMovTitulo) ;


end.
