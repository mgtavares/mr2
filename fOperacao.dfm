inherited frmOperacao: TfrmOperacao
  Caption = 'Opera'#231#227'o Financeira'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 64
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 52
    Top = 70
    Width = 50
    Height = 13
    Alignment = taRightJustify
    Caption = 'Opera'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 27
    Top = 94
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Opera'#231#227'o'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 8
    Top = 118
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Efeito Saldo T'#237'tulo'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 136
    Top = 94
    Width = 313
    Height = 13
    Caption = 
      '(P) - Pagamento / (A) - Abatimento / (J) - Juros / (D) - Descont' +
      'o'
    FocusControl = DBEdit4
  end
  object Label6: TLabel [6]
    Left = 136
    Top = 118
    Width = 354
    Height = 13
    Caption = 
      '(+) - Somar no Saldo / (-) Subtrair no Saldo / (#) - Sem Efeito ' +
      'no Saldo'
    FocusControl = DBEdit4
  end
  object Label7: TLabel [7]
    Left = 71
    Top = 142
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta'
    FocusControl = DBEdit5
  end
  object DBEdit1: TDBEdit [9]
    Tag = 1
    Left = 104
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdOperacao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [10]
    Left = 104
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmOperacao'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [11]
    Left = 104
    Top = 88
    Width = 25
    Height = 19
    DataField = 'cTipoOper'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [12]
    Left = 104
    Top = 112
    Width = 25
    Height = 19
    DataField = 'cSinalOper'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [13]
    Left = 104
    Top = 136
    Width = 65
    Height = 19
    DataField = 'nCdPlanoConta'
    DataSource = dsMaster
    TabOrder = 5
    OnExit = DBEdit5Exit
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [14]
    Tag = 1
    Left = 176
    Top = 136
    Width = 497
    Height = 19
    DataField = 'cNmPlanoConta'
    DataSource = DataSource1
    TabOrder = 6
  end
  object DBCheckBox1: TDBCheckBox [15]
    Left = 104
    Top = 160
    Width = 169
    Height = 17
    Caption = 'Apurar Movimento no DRE'
    DataField = 'cFlgApuraDRE'
    DataSource = dsMaster
    TabOrder = 7
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Operacao'
      'WHERE nCdOperacao = :nPK')
    object qryMasternCdOperacao: TIntegerField
      FieldName = 'nCdOperacao'
    end
    object qryMastercNmOperacao: TStringField
      FieldName = 'cNmOperacao'
      Size = 50
    end
    object qryMastercTipoOper: TStringField
      FieldName = 'cTipoOper'
      FixedChar = True
      Size = 1
    end
    object qryMastercSinalOper: TStringField
      FieldName = 'cSinalOper'
      FixedChar = True
      Size = 1
    end
    object qryMasternCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryMastercFlgApuraDRE: TIntegerField
      FieldName = 'cFlgApuraDRE'
    end
  end
  object qryPlanoConta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdPlanoConta, cNmPlanoConta'
      'FROM PlanoConta'
      'WHERE nCdPlanoConta = :nPK')
    Left = 624
    Top = 144
    object qryPlanoContanCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryPlanoContacNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryPlanoConta
    Left = 592
    Top = 328
  end
end
