inherited frmConciliaLoteLiqCobradora: TfrmConciliaLoteLiqCobradora
  Caption = 'Concilia'#231#227'o Lote Liq. Cobradora'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    ButtonWidth = 100
    inherited ToolButton1: TToolButton
      Caption = '&Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 100
    end
    inherited ToolButton2: TToolButton
      Left = 108
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 8
    Top = 40
    Width = 641
    Height = 321
    DataGrouping.GroupLevels = <>
    DataSource = dsLoteLiqCobradora
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdLoteLiqCobradora'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNrBorderoCob'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtFecham'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValTotal'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdCobradora'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmCobradora'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryLoteLiqCobradora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdLojaCobradora'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT LoteLiqCobradora.nCdLoteLiqCobradora'
      '      ,LoteLiqCobradora.cNrBorderoCob'
      '      ,LoteLiqCobradora.dDtFecham'
      '      ,LoteLiqCobradora.nValTotal'
      '      ,LoteLiqCobradora.nCdCobradora'
      '      ,Cobradora.cNmCobradora'
      '      ,Usuario.cNmUsuario'
      '  FROM LoteLiqCobradora'
      
        '       INNER JOIN Cobradora ON LoteLiqCobradora.nCdCobradora  = ' +
        'Cobradora.nCdCobradora'
      
        '       INNER JOIN Usuario   ON LoteLiqCobradora.nCdUsuarioCad = ' +
        'Usuario.nCdUsuario'
      ' WHERE nCdEmpresa       = :nCdEmpresa'
      '   AND nCdLojaCobradora = :nCdLojaCobradora'
      '   AND dDtFecham IS NOT NULL'
      '   AND dDtCancel IS NULL'
      '   AND dDtFinanc IS NULL'
      ' ORDER BY dDtCad'
      '')
    Left = 376
    Top = 160
    object qryLoteLiqCobradoranCdLoteLiqCobradora: TIntegerField
      DisplayLabel = 'Border'#244's Pendentes de Concilia'#231#227'o|ID'
      FieldName = 'nCdLoteLiqCobradora'
    end
    object qryLoteLiqCobradoracNrBorderoCob: TStringField
      DisplayLabel = 'Border'#244's Pendentes de Concilia'#231#227'o|N'#250'm. Border'#244
      FieldName = 'cNrBorderoCob'
      Size = 10
    end
    object qryLoteLiqCobradoradDtFecham: TDateTimeField
      DisplayLabel = 'Border'#244's Pendentes de Concilia'#231#227'o|Dt. Processo'
      FieldName = 'dDtFecham'
    end
    object qryLoteLiqCobradoranValTotal: TBCDField
      DisplayLabel = 'Border'#244's Pendentes de Concilia'#231#227'o|Val. Border'#244
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryLoteLiqCobradoranCdCobradora: TIntegerField
      DisplayLabel = 'Border'#244's Pendentes de Concilia'#231#227'o|Cobradora|C'#243'd.'
      FieldName = 'nCdCobradora'
    end
    object qryLoteLiqCobradoracNmCobradora: TStringField
      DisplayLabel = 'Border'#244's Pendentes de Concilia'#231#227'o|Cobradora|Nome Cobradora'
      FieldName = 'cNmCobradora'
      Size = 50
    end
    object qryLoteLiqCobradoracNmUsuario: TStringField
      DisplayLabel = 'Border'#244's Pendentes de Concilia'#231#227'o|Usu'#225'rio Respons'#225'vel Digita'#231#227'o'
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsLoteLiqCobradora: TDataSource
    DataSet = qryLoteLiqCobradora
    Left = 416
    Top = 160
  end
end
