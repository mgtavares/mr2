unit fMTitulo_Movimenta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask;

type
  TfrmMTitulo_Movimenta = class(TfrmProcesso_Padrao)
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdEmpresa: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancarianCdCC: TIntegerField;
    qryContaBancariacFlgFluxo: TIntegerField;
    qryContaBancariaiUltimoCheque: TIntegerField;
    qryContaBancariaiUltimoBordero: TIntegerField;
    qryContaBancariaDadosConta: TStringField;
    qryContaBancarianValLimiteCredito: TBCDField;
    qryContaBancariacFlgDeposito: TIntegerField;
    qryContaBancariacFlgEmiteCheque: TIntegerField;
    qryContaBancariacAgencia: TIntegerField;
    qryOperacao: TADOQuery;
    qryOperacaonCdOperacao: TIntegerField;
    qryOperacaocNmOperacao: TStringField;
    qryOperacaocTipoOper: TStringField;
    qryOperacaocSinalOper: TStringField;
    qryFormaPagto: TADOQuery;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    qryTitulo: TADOQuery;
    qryTitulonCdTitulo: TIntegerField;
    qryTitulonCdEmpresa: TIntegerField;
    qryTitulonCdEspTit: TIntegerField;
    qryTitulocNrTit: TStringField;
    qryTituloiParcela: TIntegerField;
    qryTitulonCdTerceiro: TIntegerField;
    qryTitulonCdCategFinanc: TIntegerField;
    qryTitulonCdMoeda: TIntegerField;
    qryTitulocSenso: TStringField;
    qryTitulodDtEmissao: TDateTimeField;
    qryTitulodDtReceb: TDateTimeField;
    qryTitulodDtVenc: TDateTimeField;
    qryTitulodDtLiq: TDateTimeField;
    qryTitulodDtCad: TDateTimeField;
    qryTitulodDtCancel: TDateTimeField;
    qryTitulonValTit: TBCDField;
    qryTitulonValLiq: TBCDField;
    qryTitulonSaldoTit: TBCDField;
    qryTitulonValJuro: TBCDField;
    qryTitulonValDesconto: TBCDField;
    qryTitulocObsTit: TMemoField;
    qryTitulonCdSP: TIntegerField;
    qryTitulonCdBancoPortador: TIntegerField;
    qryTitulodDtRemPortador: TDateTimeField;
    qryTitulodDtBloqTit: TDateTimeField;
    qryTitulocObsBloqTit: TStringField;
    qryTitulonCdUnidadeNegocio: TIntegerField;
    qryTitulonCdCC: TIntegerField;
    qryTitulodDtIntegracao: TDateTimeField;
    qryParametro: TADOQuery;
    qryParametrocValor: TStringField;
    qryCheque: TADOQuery;
    qryChequenCdCheque: TAutoIncField;
    qryChequenCdEmpresa: TIntegerField;
    qryChequenCdBanco: TIntegerField;
    qryChequecAgencia: TStringField;
    qryChequecConta: TStringField;
    qryChequecDigito: TStringField;
    qryChequecCNPJCPF: TStringField;
    qryChequeiNrCheque: TIntegerField;
    qryChequenValCheque: TBCDField;
    qryChequedDtDeposito: TDateTimeField;
    qryChequenCdTerceiroResp: TIntegerField;
    qryChequenCdTerceiroPort: TIntegerField;
    qryChequedDtRemessaPort: TDateTimeField;
    qryChequenCdTabTipoCheque: TIntegerField;
    qryChequecChave: TStringField;
    qryChequenCdContaBancariaDep: TIntegerField;
    qryChequedDtDevol: TDateTimeField;
    qryChequecFlgCompensado: TIntegerField;
    qryChequenCdContaBancaria: TIntegerField;
    qryChequedDtEmissao: TDateTimeField;
    qryChequecNmFavorecido: TStringField;
    qryMTitulo: TADOQuery;
    qryMTitulonCdMTitulo: TAutoIncField;
    qryMTitulonCdTitulo: TIntegerField;
    qryMTitulonCdContaBancaria: TIntegerField;
    qryMTitulonCdOperacao: TIntegerField;
    qryMTitulonCdFormaPagto: TIntegerField;
    qryMTituloiNrCheque: TIntegerField;
    qryMTitulonValMov: TBCDField;
    qryMTitulodDtMov: TDateTimeField;
    qryMTitulodDtCad: TDateTimeField;
    qryMTitulodDtCancel: TDateTimeField;
    qryMTitulocNrDoc: TStringField;
    qryMTitulodDtContab: TDateTimeField;
    qryMTitulonCdUsuarioCad: TIntegerField;
    qryMTitulonCdUsuarioCancel: TIntegerField;
    qryMTitulocObsMov: TMemoField;
    qryMTitulonCdCC: TIntegerField;
    qryMTitulocFlgMovAutomatico: TIntegerField;
    qryMTitulodDtBomPara: TDateTimeField;
    qryMTitulonCdLanctoFin: TIntegerField;
    qryMTitulocFlgIntegrado: TIntegerField;
    qryAux: TADOQuery;
    qryLanctoFin: TADOQuery;
    qryLanctoFinnCdLanctoFin: TAutoIncField;
    qryLanctoFinnCdResumoCaixa: TIntegerField;
    qryLanctoFinnCdContaBancaria: TIntegerField;
    qryLanctoFindDtLancto: TDateTimeField;
    qryLanctoFinnCdTipoLancto: TIntegerField;
    qryLanctoFinnValLancto: TBCDField;
    qryLanctoFinnCdUsuario: TIntegerField;
    qryLanctoFinnCdUsuarioAutor: TIntegerField;
    qryLanctoFincFlgManual: TIntegerField;
    qryLanctoFindDtEstorno: TDateTimeField;
    qryLanctoFinnCdLanctoFinPai: TIntegerField;
    qryLanctoFincMotivoEstorno: TStringField;
    qryLanctoFincHistorico: TStringField;
    qryLanctoFincFlgConciliado: TIntegerField;
    qryLanctoFinnCdUsuarioConciliacao: TIntegerField;
    qryLanctoFindDtConciliacao: TDateTimeField;
    qryLanctoFincDocumento: TStringField;
    qryLanctoFinnCdCheque: TIntegerField;
    DataSource1: TDataSource;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBMemo1: TDBMemo;
    DBEdit11: TDBEdit;
    qryTitulonValAbatimento: TBCDField;
    DBEdit8: TDBEdit;
    DataSource2: TDataSource;
    DBEdit9: TDBEdit;
    DataSource3: TDataSource;
    DBEdit10: TDBEdit;
    DataSource4: TDataSource;
    qryContaBancariacFlgCaixa: TIntegerField;
    qryAtualizaSaldoConta_Debito: TADOQuery;
    qryAtualizaSaldoConta_Credito: TADOQuery;
    qryLanctoFinnCdUsuarioEstorno: TIntegerField;
    SP_CONTABILIZA_MTITULO: TADOStoredProc;
    Label10: TLabel;
    DBEdit12: TDBEdit;
    qryCC: TADOQuery;
    qryCCcNmCC: TStringField;
    DBEdit13: TDBEdit;
    DataSource5: TDataSource;
    qryOperacaocFlgApuraDRE: TIntegerField;
    procedure ToolButton1Click(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit1Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit6Enter(Sender: TObject);
    procedure DBEdit12Exit(Sender: TObject);
    procedure DBEdit12KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTitulo : integer ;
    cNmTerceiro : string ;
  end;

var
  frmMTitulo_Movimenta: TfrmMTitulo_Movimenta;
  nCdFormaPagtoCheque: integer ;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmMTitulo_Movimenta.ToolButton1Click(Sender: TObject);
var
    nCdCheque, nCdMovimento : integer ;
begin

    nCdCheque := 0 ;

    If (qryMTitulonCdOperacao.Value = 0) or (DBEdit8.Text = '') then
    begin
        MensagemAlerta('Informe a opera��o do movimento.') ;
        DBEDit2.SetFocus ;
        abort ;
    end ;

    If ((qryMTitulonCdFormaPagto.Value = 0) or (DBEdit9.Text = ''))
      and (qryOperacaocTipoOper.Value = 'P') then
    begin
        MensagemAlerta('Informe a forma de liquida��o.') ;
        DBEDit3.SetFocus ;
        abort ;
    end ;

    If ((qryMTitulonCdContaBancaria.Value = 0) or (DBEdit10.Text = ''))
      and (qryOperacaocTipoOper.Value = 'P') then
    begin
        MensagemAlerta('Informe a conta banc�ria.') ;
        DBEDit1.SetFocus ;
        abort ;
    end ;

    if not qryContaBancaria.eof then
        if (qryContaBancariacFlgEmiteCheque.Value = 0) and (qryMTitulonCdFormaPagto.Value = nCdFormaPagtoCheque) then
        begin
            MensagemAlerta('Esta conta banc�ria n�o movimenta cheque.') ;
            DbEdit1.SetFocus;
            abort ;
        end ;

    if (qryOperacaocFlgApuraDRE.Value = 1) and (DBEdit13.Text = '') then
    begin
        MensagemAlerta('Este tipo de opera��o de t�tulo exige um centro de custo.') ;
        DBEdit12.SetFocus;
        abort ;
    end ;

    if (qryMTitulonCdFormaPagto.Value = nCdFormaPagtoCheque) and (qryMTituloiNrCheque.Value = 0) then
    begin
        MensagemAlerta('Informe o n�mero do cheque.') ;
        DbEdit4.SetFocus;
        Exit ;
    end ;

    if (qryMTitulonCdFormaPagto.Value = nCdFormaPagtoCheque) and (qryMTitulodDtBomPara.AsString = '') then
    begin
        MensagemAlerta('Informe a data de dep�sito do cheque.') ;
        DbEdit11.SetFocus;
        Exit ;
    end ;

    if (qryMTitulonValMov.Value <= 0) then
    begin
        MensagemAlerta('Informe o valor do movimento ou valor inv�lido.') ;
        DbEdit5.SetFocus;
        Exit ;
    end ;

    if (qryMTitulodDtMov.AsString = '') then
    begin
        MensagemAlerta('Informe a data do movimento.') ;
        DbEdit6.SetFocus;
        Exit ;
    end ;

    qryParametro.Close ;
    qryParametro.Parameters.ParamByName('cParametro').Value := 'DTCONTAB' ;
    qryParametro.Open ;

    If (qryMTitulodDtMov.Value <= StrToDateTime(qryParametrocValor.Value)) then
    begin
        MensagemAlerta('A data do movimento n�o pode ser inferior a data da contabiliza��o financeira.') ;
        Abort ;
    end ;

    If (qryMTitulonCdFormaPagto.Value = nCdFormaPagtoCheque) and (qryMTitulodDtBomPara.Value <= StrToDateTime(qryParametrocValor.Value)) then
    begin
        MensagemAlerta('A data de dep�sito do cheque n�o pode ser inferior a data da contabiliza��o financeira.') ;
        Abort ;
    end ;

    if (qryMTitulodDtMov.Value > Now()) then
    begin
        MensagemAlerta('Data do Movimento inv�lida.') ;
        Abort ;
    end ;

    if (qryMTitulonCdFormaPagto.Value = nCdFormaPagtoCheque) then
    begin

        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('SELECT 1 FROM Cheque WHERE nCdTabTipoCheque = 1 AND iNrCheque = ' + DbEdit4.Text + ' AND nCdContaBancaria = ' + qryMTitulonCdContaBancaria.AsString) ;
        qryAux.Open ;

        if not qryAux.Eof then
        begin
            qryAux.Close ;
            MensagemAlerta('Cheque j� Emitido. Imposs�vel registrar o movimento.') ;
            exit ;
        end ;

        qryAux.Close ;
    end ;

    case MessageDlg('Confirma a inclus�o deste movimento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
    end ;

    qryTitulo.Close ;
    qryTitulo.Parameters.ParamByName('nCdTitulo').Value := nCdTitulo;
    qryTitulo.Open  ;

    qryTitulo.Edit ;

    nCdMovimento := frmMenu.fnProximoCodigo('MTITULO') ;

    frmMenu.Connection.BeginTrans;

    try
        if (qryMTitulo.State = dsBrowse) then
            qryMTitulo.Edit ;

        qryMTitulonCdMTitulo.Value    := nCdMovimento ;

        qryMTitulonCdTitulo.Value     := nCdTitulo ;
        qryMTitulodDtCad.Value        := Now() ;
        qryMTitulonCdUsuarioCad.Value := frmMenu.nCdUsuarioLogado;

        if (qryOperacaocTipoOper.Value = 'P') then
            qryTitulonValLiq.Value := qryTitulonValLiq.Value + qryMTitulonValMov.Value ;

        if (qryOperacaocTipoOper.Value = 'J') then
            qryTitulonValJuro.Value := qryTitulonValJuro.Value + qryMTitulonValMov.Value ;

        if (qryOperacaocTipoOper.Value = 'D') then
            qryTitulonValDesconto.Value := qryTitulonValDesconto.Value + qryMTitulonValMov.Value ;

        if (qryOperacaocTipoOper.Value = 'A') then
            qryTitulonValAbatimento.Value := qryTitulonValAbatimento.Value + qryMTitulonValMov.Value ;

        if (qryOperacaocSinalOper.Value = '+') then
            qryTitulonSaldoTit.Value := qryTitulonSaldoTit.Value + qryMTitulonValMov.Value ;

        if (qryOperacaocSinalOper.Value = '-') then
            qryTitulonSaldoTit.Value := qryTitulonSaldoTit.Value - qryMTitulonValMov.Value ;

        if (qryTitulonSaldoTit.Value < 0) then
        begin
            frmMenu.Connection.RollbackTrans;
            MensagemAlerta('Valor do Movimento maior que o saldo � liquidar.') ;
            Exit ;
        end ;

        if (qryTitulonSaldoTit.Value = 0) then
            qryTitulodDtLiq.Value := qryMTitulodDtMov.Value ;

        qryTitulodDtIntegracao.AsString := '' ;

        // inseri o cheque
        if (qryMTitulonCdFormaPagto.Value = nCdFormaPagtoCheque) then
        begin

            qryCheque.Open ;
            qryCheque.Insert ;
            qryChequenCdCheque.Value        := frmMenu.fnProximoCodigo('CHEQUE') ;
            qryChequenCdEmpresa.Value       := frmMenu.nCdEmpresaAtiva;
            qryChequeiNrCheque.Value        := qryMTituloiNrCheque.Value ;
            qryChequenValCheque.Value       := qryMTitulonValMov.Value ;
            qryChequedDtDeposito.Value      := qryMTitulodDtBomPara.Value ;
            qryChequedDtEmissao.Value       := qryMTitulodDtMov.Value ;
            qryChequecNmFavorecido.Value    := cNmTerceiro ;
            qryChequenCdTabTipoCheque.Value := 1 ;
            qryChequecFlgCompensado.Value   := 0 ;
            qryChequenCdContaBancaria.Value := qryMTitulonCdContaBancaria.Value ;
            qryChequecChave.Value           := 'PROPRIO:' + qryMTitulonCdContaBancaria.AsString + ':' + qryMTituloiNrCheque.AsString;
            qryCheque.Post ;

            nCdCheque := qryChequenCdCheque.Value ;

            qryCheque.Close ;

        end ;
        
        // gera o lan�amento da movimenta��o da conta
        if (qryOperacaocTipoOper.Value = 'P') then
        begin

            qryLanctoFin.Close ;
            qryLanctoFin.Open ;

            qryLanctoFin.Insert ;
            qryLanctoFinnCdLanctoFin.Value     := frmMenu.fnProximoCodigo('LANCTOFIN');
            qryLanctoFinnCdContaBancaria.Value := qryMTitulonCdContaBancaria.Value ;
            qryLanctoFindDtLancto.Value        := qryMTitulodDtMov.Value ;
            qryLanctoFinnCdUsuario.Value       := frmMenu.nCdUsuarioLogado;
            qryLanctoFincFlgManual.Value       := 0 ;

            if (qryTitulocSenso.Value = 'C') then
            begin
                qryLanctoFincHistorico.Value       := 'BAIXA RECEBIMENTO MANUAL' ;
                qryLanctoFinnValLancto.Value       := qryMTitulonValMov.Value ;
            end ;

            qryLanctoFincDocumento.Value := qryMTitulocNrDoc.Value ;

            if (qryTitulocSenso.Value = 'D') then
            begin
                qryLanctoFincHistorico.Value       := 'BAIXA PAGAMENTO MANUAL' ;
                qryLanctoFinnValLancto.Value       := (qryMTitulonValMov.Value * -1) ;

                if (qryMTitulonCdFormaPagto.Value = nCdFormaPagtoCheque) then
                begin
                    qryLanctoFincHistorico.Value := 'BAIXA PAGAMENTO MANUAL - CHEQUE' ;
                    qryLanctoFincDocumento.Value := qryMTituloiNrCheque.asString ;
                end

            end ;

            if (qryMTitulodDtBomPara.asString <> '') then
                qryLanctoFindDtLancto.Value := qryMTitulodDtBomPara.Value ;

            {-- se for uma conta caixa, a data do lan�amento � HOJE --}
            if (qryContaBancariacFlgCaixa.Value = 1) then
                qryLanctoFindDtLancto.Value := Now() ;

            if (nCdCheque > 0) then
                qryLanctoFinnCdCheque.Value := nCdCheque ;

            qryLanctoFin.Post ;

            {--descobre o ID do lan�amento financeiro para vincular no movimento--}
            {qryAux.Close;
            qryAux.SQL.Clear;
            qryAux.SQL.Add('SELECT MAX(nCdLanctoFin) FROM LanctoFin WHERE nCdContaBancaria = ' + qryMTitulonCdContaBancaria.AsString + ' AND nCdUsuario = ' + intToStr(frmMenu.nCdUsuarioLogado)) ;
            qryAux.Open;}

            {-- atualiza o numero do lan�amento financeiro no movimento do t�tulo --}
            qryMTitulonCdLanctoFin.Value := qryLanctoFinnCdLanctoFin.Value;

            {-- se for uma conta caixa , um titulo a pagar e pago em dinheiro, gera o d�bito do saldo --}
            if (qryTitulocSenso.Value = 'D') and (qryContaBancariacFlgCaixa.Value = 1) and (qryMTitulonCdFormaPagto.Value = 1) then
            begin

                qryAtualizaSaldoConta_Debito.Close;
                qryAtualizaSaldoConta_Debito.Parameters.ParamByName('nValMov').Value          := qryMTitulonValMov.Value;
                qryAtualizaSaldoConta_Debito.Parameters.ParamByName('nCdContaBancaria').Value := qryMTitulonCdContaBancaria.Value;
                qryAtualizaSaldoConta_Debito.ExecSQL;

            end ;

            {-- se for uma conta caixa , um titulo a receber e pago em dinheiro, gera o d�bito do saldo --}
            if (qryTitulocSenso.Value = 'C') and (qryContaBancariacFlgCaixa.Value = 1) and (qryMTitulonCdFormaPagto.Value = 1) then
            begin

                qryAtualizaSaldoConta_Credito.Close;
                qryAtualizaSaldoConta_Credito.Parameters.ParamByName('nValMov').Value          := qryMTitulonValMov.Value;
                qryAtualizaSaldoConta_Credito.Parameters.ParamByName('nCdContaBancaria').Value := qryMTitulonCdContaBancaria.Value;
                qryAtualizaSaldoConta_Credito.ExecSQL;

            end ;

        end ;


        qryTitulo.Post ;
        qryMTitulo.Post ;

        {--Contabiliza o movimento --}
        SP_CONTABILIZA_MTITULO.Close;
        SP_CONTABILIZA_MTITULO.Parameters.ParamByName('@nCdMTitulo').Value := qryMTitulonCdMTitulo.Value ;
        SP_CONTABILIZA_MTITULO.ExecProc;

        qryTitulo.Close ;

    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro na inclus�o do movimento.') ;
        Raise ;
        exit ;
    end ;

    frmMenu.Connection.CommitTrans;

    Close ;

end;

procedure TfrmMTitulo_Movimenta.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryFormaPagto.Close;
  PosicionaQuery(qryFormaPagto, DBEdit3.Text) ;

  DBEdit4.ReadOnly  := True ;
  DBEdit11.ReadOnly := True ;

  DBEdit4.Color     := $00E9E4E4 ;
  DBEdit11.Color    := $00E9E4E4 ;

  if not qryFormaPagto.Eof then
      if (qryFormaPagtonCdFormaPagto.Value = nCdFormaPagtoCheque) then
      begin
          DBEdit4.ReadOnly  := False ;
          DBEdit11.ReadOnly := False ;

          DBEdit4.Color     := clWhite ;
          DBEdit11.Color    := clWhite ;
      end ;

  DBEdit1.SetFocus;
  
end;

procedure TfrmMTitulo_Movimenta.DBEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMtitulo.State = dsInsert) or (qryMTitulo.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(39);

            If (nPK > 0) then
            begin
                qryMTitulonCdContaBancaria.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmMTitulo_Movimenta.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMtitulo.State = dsInsert) or (qryMTitulo.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(15,'cFlgPermFinanceiro = 1');

            If (nPK > 0) then
            begin
                qryMTitulonCdFormaPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;


end;

procedure TfrmMTitulo_Movimenta.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMtitulo.State = dsInsert) or (qryMTitulo.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(30);

            If (nPK > 0) then
            begin
                qryMTitulonCdOperacao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmMTitulo_Movimenta.FormShow(Sender: TObject);
begin
  inherited;

  nCdFormaPagtoCheque := strToint(frmMenu.LeParametro('FORMAPAGTOCHE')) ;

  qryContaBancaria.Close ;
  qryContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;

  qryOperacao.Close;
  qryFormaPagto.Close;

  DBEdit3.ReadOnly  := True ;
  DBEdit1.ReadOnly  := True ;
  DBEdit4.ReadOnly  := True ;
  DBEdit11.ReadOnly := True ;

  DBEdit3.Color     := $00E9E4E4 ;
  DBEdit1.Color     := $00E9E4E4 ;
  DBEdit4.Color     := $00E9E4E4 ;
  DBEdit11.Color    := $00E9E4E4 ;

  qryMTitulo.Close ;
  qryMTitulo.Open ;
  qryMTitulo.Insert ;

  DBEdit2.SetFocus ;

end;

procedure TfrmMTitulo_Movimenta.DBEdit2Exit(Sender: TObject);
begin
  inherited;

  qryOperacao.Close;
  PosicionaQuery(qryOperacao, DBEdit2.Text) ;

  DBEdit3.ReadOnly  := True ;
  DBEdit1.ReadOnly  := True ;
  DBEdit12.ReadOnly := True ;

  DBEdit3.Color   := $00E9E4E4 ;
  DBEdit1.Color   := $00E9E4E4 ;
  DBEdit12.Color  := $00E9E4E4 ;

  if not qryOperacao.Eof then
  begin

      if (qryOperacaocTipoOper.Value = 'P') then
      begin
          DBEdit3.ReadOnly := False ;
          DBEdit1.ReadOnly := False ;

          DBEdit3.Color    := clWhite ;
          DBEdit1.Color    := clWhite ;

          DBEdit3.SetFocus;
      end ;

      if (qryOperacaocFlgApuraDRE.Value = 1) then
      begin
      
          DBEdit12.ReadOnly := False ;
          DBEdit12.Color    := clWhite;
          DBEdit12.SetFocus;

      end ;

  end ;

end;

procedure TfrmMTitulo_Movimenta.DBEdit1Exit(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close;
  PosicionaQuery(qryContaBancaria, DBEdit1.Text) ;

  if (not DBEdit4.ReadOnly) then
      DBEdit4.SetFocus;
  
end;

procedure TfrmMTitulo_Movimenta.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  DBEdit11.SetFocus;
  
end;

procedure TfrmMTitulo_Movimenta.DBEdit6Enter(Sender: TObject);
begin
  inherited;

  qryMTitulodDtMov.Value := Date;
  
end;

procedure TfrmMTitulo_Movimenta.DBEdit12Exit(Sender: TObject);
begin
  inherited;

  qryCC.Close;
  PosicionaQuery(qryCC, DBEdit12.Text) ;

  DBEdit5.SetFocus;
  
end;

procedure TfrmMTitulo_Movimenta.DBEdit12KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMtitulo.State = dsInsert) or (qryMTitulo.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(29);

            If (nPK > 0) then
            begin
                qryMTitulonCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

end.
