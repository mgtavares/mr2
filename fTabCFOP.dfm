inherited frmTabCFOP: TfrmTabCFOP
  Left = 120
  Top = 128
  Width = 1062
  Height = 541
  Caption = 'Tabela CFOP'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1046
    Height = 480
  end
  object Label1: TLabel [1]
    Left = 39
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 49
    Top = 62
    Width = 28
    Height = 13
    Alignment = taRightJustify
    Caption = 'CFOP'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [3]
    Left = 28
    Top = 86
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit4
  end
  inherited ToolBar2: TToolBar
    Width = 1046
    TabOrder = 4
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 80
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdCFOP'
    DataSource = dsMaster
    TabOrder = 0
  end
  object DBEdit2: TDBEdit [6]
    Left = 80
    Top = 56
    Width = 65
    Height = 19
    DataField = 'cCFOP'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit4: TDBEdit [7]
    Left = 80
    Top = 80
    Width = 769
    Height = 19
    DataField = 'cNmCFOP'
    DataSource = dsMaster
    TabOrder = 2
  end
  object GroupBox1: TGroupBox [8]
    Left = 80
    Top = 112
    Width = 417
    Height = 81
    TabOrder = 3
    object DBCheckBox4: TDBCheckBox
      Left = 280
      Top = 16
      Width = 113
      Height = 17
      Caption = 'Gera Credito IPI'
      DataField = 'cFlgGeraCreditoIPI'
      DataSource = dsMaster
      TabOrder = 4
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox2: TDBCheckBox
      Left = 280
      Top = 48
      Width = 105
      Height = 17
      Caption = 'Importa'#231#227'o'
      DataField = 'cFlgImportacao'
      DataSource = dsMaster
      TabOrder = 5
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox6: TDBCheckBox
      Left = 136
      Top = 16
      Width = 129
      Height = 17
      Caption = 'Gera Credito COFINS'
      DataField = 'cFlgGeraCreditoCOFINS'
      DataSource = dsMaster
      TabOrder = 2
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox3: TDBCheckBox
      Left = 136
      Top = 48
      Width = 121
      Height = 17
      Caption = 'Gera Credito ICMS'
      DataField = 'cFlgGeraCreditoICMS'
      DataSource = dsMaster
      TabOrder = 3
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox1: TDBCheckBox
      Left = 8
      Top = 16
      Width = 121
      Height = 17
      Caption = 'Gera Livro Fiscal'
      DataField = 'cFlgGeraLivroFiscal'
      DataSource = dsMaster
      TabOrder = 0
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox5: TDBCheckBox
      Left = 8
      Top = 48
      Width = 113
      Height = 17
      Caption = 'Gera Credito PIS'
      DataField = 'cFlgGeraCreditoPIS'
      DataSource = dsMaster
      TabOrder = 1
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM TabCFOP'
      ' WHERE nCdCFOP = :nPK')
    object qryMasternCdCFOP: TIntegerField
      FieldName = 'nCdCFOP'
    end
    object qryMastercCFOP: TStringField
      FieldName = 'cCFOP'
      Size = 5
    end
    object qryMastercCFOPPai: TStringField
      FieldName = 'cCFOPPai'
      Size = 5
    end
    object qryMastercNmCFOP: TStringField
      FieldName = 'cNmCFOP'
      Size = 150
    end
    object qryMastercFlgGeraLivroFiscal: TIntegerField
      FieldName = 'cFlgGeraLivroFiscal'
    end
    object qryMastercFlgGeraCreditoICMS: TIntegerField
      FieldName = 'cFlgGeraCreditoICMS'
    end
    object qryMastercFlgGeraCreditoIPI: TIntegerField
      FieldName = 'cFlgGeraCreditoIPI'
    end
    object qryMastercFlgGeraCreditoPIS: TIntegerField
      FieldName = 'cFlgGeraCreditoPIS'
    end
    object qryMastercFlgGeraCreditoCOFINS: TIntegerField
      FieldName = 'cFlgGeraCreditoCOFINS'
    end
    object qryMastercFlgImportacao: TIntegerField
      FieldName = 'cFlgImportacao'
    end
    object qryMasternCdTipoICMS: TIntegerField
      FieldName = 'nCdTipoICMS'
    end
    object qryMasternCdTipoIPI: TIntegerField
      FieldName = 'nCdTipoIPI'
    end
  end
end
