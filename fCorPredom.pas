unit fCorPredom;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmCorPredom = class(TfrmCadastro_Padrao)
    qryMasternCdCor: TAutoIncField;
    qryMastercNmCor: TStringField;
    qryMastercNmCorPredom: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryMasteriNivel: TIntegerField;
    qryMasternCdCorPredom: TIntegerField;
    DataSource1: TDataSource;
    qryVerificaDup: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCorPredom: TfrmCorPredom;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmCorPredom.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'COR' ;
  nCdTabelaSistema  := 27 ;
  nCdConsultaPadrao := 160 ;
end;

procedure TfrmCorPredom.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmCorPredom.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMastercNmCor.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o da cor.') ;
      abort ;
  end ;

  qryVerificaDup.Close ;
  qryVerificaDup.Parameters.ParamByName('cNmCor').Value := DBEdit2.Text ;
  qryVerificaDup.Parameters.ParamByName('nCdCor').Value := qryMasternCdCor.Value ;
  qryVerificaDup.Open ;

  if not qryVerificaDup.Eof then
  begin
      qryVerificaDup.Close ;
      MensagemAlerta('Cor j� cadastrada.') ;
      abort ;
  end ;

  qryVerificaDup.Close ;

  qryMastercNmCor.Value := Uppercase(qryMastercNmCor.Value) ;
  qryMasteriNivel.Value := 1 ;

  inherited;

end;

procedure TfrmCorPredom.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(43);

            If (nPK > 0) then
            begin
                qryMasternCdCorPredom.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmCorPredom) ;
    
end.
