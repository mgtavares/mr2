unit fMRP_DetalhaEmpenho;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, StdCtrls, Mask, DBCtrls, ADODB, ImgList,
  ComCtrls, ToolWin, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxPC;

type
  TfrmMRP_DetalhaEmpenho = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    dsProduto: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryEstoqueEmpenhado: TADOQuery;
    qryEstoqueEmpenhadonCdEmpresa: TIntegerField;
    qryEstoqueEmpenhadocOrigemEmpenhao: TStringField;
    qryEstoqueEmpenhadodDtEmpenho: TDateTimeField;
    qryEstoqueEmpenhadonCdPedido: TIntegerField;
    qryEstoqueEmpenhadoNmeroOP: TStringField;
    qryEstoqueEmpenhadocNmTerceiro: TStringField;
    qryEstoqueEmpenhadonQtdeEstoqueEmpenhado: TBCDField;
    dsEstoqueEmpenhado: TDataSource;
    cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn;
    cxGrid1DBTableView1cOrigemEmpenhao: TcxGridDBColumn;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1NmeroOP: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeEstoqueEmpenhado: TcxGridDBColumn;
    qryEstoqueEmpenhadodDtPedido: TDateTimeField;
    cxGrid1DBTableView1dDtPedido: TcxGridDBColumn;
    qryEstoqueEmpenhadocNmLocalEstoqueEmp: TStringField;
    qryEstoqueEmpenhadocNmLocalEstoqueReq: TStringField;
    cxGrid1DBTableView1cNmLocalEstoqueEmp: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLocalEstoqueReq: TcxGridDBColumn;
    procedure exibeEmpenhos( nCdProduto : integer ) ;
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMRP_DetalhaEmpenho: TfrmMRP_DetalhaEmpenho;

implementation

uses fPedidoVendaSimples , fMenu;
{$R *.dfm}

{ TfrmMRP_DetalhaEmpenho }

procedure TfrmMRP_DetalhaEmpenho.exibeEmpenhos(nCdProduto: integer);
begin

    PosicionaQuery( qryProduto , intToStr( nCdProduto ) ) ;
    PosicionaQuery( qryEstoqueEmpenhado , intToStr( nCdProduto ) ) ;

    Self.ShowModal;
    
end;

procedure TfrmMRP_DetalhaEmpenho.FormShow(Sender: TObject);
begin
  inherited;

  cxGrid1.SetFocus;
  
end;

procedure TfrmMRP_DetalhaEmpenho.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmPedidoVendaSimples ;
begin
  inherited;

  if not qryEstoqueEmpenhado.isEmpty and (qryEstoqueEmpenhadonCdPedido.Value > 0) then
  begin

      objForm := TfrmPedidoVendaSimples.Create( Self ) ;

      objForm.qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      objForm.qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
      objForm.qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

      objForm.PosicionaQuery( objForm.qryMaster , qryEstoqueEmpenhadonCdPedido.AsString );
      objForm.btIncluir.Visible    := False;
      objForm.btConsultar.Visible  := False;
      objform.btSalvar.Visible     := False;
      objForm.bFinalizar.Visible := False;

      showForm( objForm, TRUE ) ;

  end ;

end;

end.
