unit rRequisicao_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptRequisicao_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand4: TQRBand;
    QRBand5: TQRBand;
    qryRequisicao: TADOQuery;
    qryRequisicaonCdRequisicao: TIntegerField;
    qryRequisicaocNmEmpresa: TStringField;
    qryRequisicaocNmLoja: TStringField;
    qryRequisicaocNmTipoRequisicao: TStringField;
    qryRequisicaodDtRequisicao: TDateTimeField;
    qryRequisicaocNmSetor: TStringField;
    qryRequisicaocNmSolicitante: TStringField;
    qryRequisicaocOBS: TMemoField;
    qryRequisicaonCdProduto: TIntegerField;
    qryRequisicaocNmItem: TStringField;
    qryRequisicaonQtdeReq: TBCDField;
    QRDBText11: TQRDBText;
    QRLabel15: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel16: TQRLabel;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRLabel17: TQRLabel;
    QRDBText15: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRDBText4: TQRDBText;
    QRShape4: TQRShape;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel12: TQRLabel;
    QRShape5: TQRShape;
    QRImage1: TQRImage;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    qryRequisicaonCdLoja: TIntegerField;
    qryRequisicaonCdTipoRequisicao: TIntegerField;
    qryRequisicaonCdItemRequisicao: TAutoIncField;
    qryRequisicaocReferencia: TStringField;
    qryRequisicaocUnidadeMedida: TStringField;
    qryRequisicaocNmCC: TStringField;
    qryRequisicaocNotaComprador: TStringField;
    qryRequisicaocFlgUrgente: TIntegerField;
    qryRequisicaocJustificativa: TStringField;
    qryRequisicaocDescricaoTecnica: TMemoField;
    QRDBText6: TQRDBText;
    QRLabel7: TQRLabel;
    qryRequisicaonCdCC: TIntegerField;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRLabel18: TQRLabel;
    QRDBText18: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText19: TQRDBText;
    QRLabel20: TQRLabel;
    qryRequisicaocIndicUrgencia: TStringField;
    QRDBText20: TQRDBText;
    qryRequisicaonValRequisicao: TBCDField;
    qryRequisicaonValUnitario: TBCDField;
    qryRequisicaonValTotal: TBCDField;
    QRLabel23: TQRLabel;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRLabel24: TQRLabel;
    QRDBText23: TQRDBText;
    QRLabel25: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRequisicao_view: TrptRequisicao_view;

implementation

{$R *.dfm}

end.
