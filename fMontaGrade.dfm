object frmMontaGrade: TfrmMontaGrade
  Left = 366
  Top = 349
  Width = 644
  Height = 86
  BorderIcons = [biSystemMenu]
  Caption = 'Grade'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Consolas'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object DBGridEh1: TDBGridEh
    Left = 0
    Top = 0
    Width = 611
    Height = 49
    Align = alTop
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = DataSource1
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghDialogFind, dghColumnResize, dghColumnMove]
    ParentFont = False
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object DBGridEh2: TDBGridEh
    Left = 0
    Top = 49
    Width = 611
    Height = 120
    Align = alTop
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = DataSource2
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'MS Sans Serif'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    ParentFont = False
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    Visible = False
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object DataSource1: TDataSource
    DataSet = cdsQuantidade
    Left = 240
    Top = 8
  end
  object cdsQuantidade: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterPost = cdsQuantidadeAfterPost
    AfterCancel = cdsQuantidadeAfterCancel
    Left = 112
    Top = 8
  end
  object qryGrade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProduto'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'DECLARE @nCdProduto int'
      ''
      'Set @nCdProduto = :nCdProduto'
      ''
      'SELECT ItemGrade.nCdItemGrade'
      '      ,ItemGrade.nCdGrade'
      '      ,ItemGrade.iTamanho'
      '      ,ItemGrade.cNmTamanho'
      '  FROM ItemGrade'
      
        '       INNER JOIN Produto WITH(INDEX=IN03_Produto) ON Produto.nC' +
        'dProdutoPai = @nCdProduto'
      
        '                          AND Produto.iTamanho      = ItemGrade.' +
        'iTamanho'
      
        ' WHERE ItemGrade.nCdGrade = (SELECT nCdGrade FROM Produto WHERE ' +
        'nCdProduto = @nCdProduto)'
      ' ORDER BY iTamanho')
    Left = 144
    Top = 8
    object qryGradenCdItemGrade: TAutoIncField
      FieldName = 'nCdItemGrade'
      ReadOnly = True
    end
    object qryGradenCdGrade: TIntegerField
      FieldName = 'nCdGrade'
    end
    object qryGradeiTamanho: TIntegerField
      FieldName = 'iTamanho'
    end
    object qryGradecNmTamanho: TStringField
      FieldName = 'cNmTamanho'
      FixedChar = True
      Size = 5
    end
  end
  object cdsCodigo: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 80
    Top = 8
  end
  object DataSource2: TDataSource
    DataSet = cdsCodigo
    Left = 208
    Top = 8
  end
  object qryCodigoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Produto.nCdProduto'
      '      ,Produto.iTamanho'
      '  FROM Produto WITH(INDEX=IN03_Produto)'
      
        '       INNER JOIN ItemGrade ON ItemGrade.nCdGrade = Produto.nCdG' +
        'rade'
      
        '                           AND ItemGrade.iTamanho = Produto.iTam' +
        'anho'
      ' WHERE nCdProdutoPai = :nCdProduto'
      ' ORDER BY iTamanho')
    Left = 176
    Top = 8
    object qryCodigoProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryCodigoProdutoiTamanho: TIntegerField
      FieldName = 'iTamanho'
    end
  end
  object qryValorAnterior: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 280
    Top = 8
  end
end
