unit fTitulo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ImgList, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, Menus, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmTitulo = class(TfrmCadastro_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitnCdGrupoEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitcFlgPrev: TIntegerField;
    qryCategFinanc: TADOQuery;
    qryCategFinancnCdCategFinanc: TIntegerField;
    qryCategFinanccNmCategFinanc: TStringField;
    qryCategFinancnCdGrupoCategFinanc: TIntegerField;
    qryCategFinanccFlgRecDes: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceironCdStatus: TIntegerField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    qryBancoPortador: TADOQuery;
    qryBancoPortadornCdBanco: TIntegerField;
    qryBancoPortadorcNmBanco: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    Label17: TLabel;
    DBEdit17: TDBEdit;
    Label18: TLabel;
    DBEdit18: TDBEdit;
    Label19: TLabel;
    DBEdit19: TDBEdit;
    Label20: TLabel;
    DBEdit20: TDBEdit;
    Label21: TLabel;
    DBEdit21: TDBEdit;
    Label22: TLabel;
    DBEdit22: TDBEdit;
    Label23: TLabel;
    DBEdit23: TDBEdit;
    Label25: TLabel;
    DBEdit24: TDBEdit;
    Label26: TLabel;
    DBEdit25: TDBEdit;
    qryTerceironCdMoeda: TIntegerField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    Label27: TLabel;
    DBEdit34: TDBEdit;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    qryCCnCdCC1: TIntegerField;
    qryCCnCdCC2: TIntegerField;
    qryCCcCdCC: TStringField;
    qryCCcCdHie: TStringField;
    qryCCiNivel: TSmallintField;
    qryCCcFlgLanc: TIntegerField;
    qryCCnCdStatus: TIntegerField;
    Label28: TLabel;
    DBEdit36: TDBEdit;
    qryMTitulo: TADOQuery;
    qryMTituloMovimentoID: TAutoIncField;
    qryMTituloOperaoCdigo: TIntegerField;
    qryMTituloOperaoDescrio: TStringField;
    qryMTituloFormaMovimentoCdigo: TIntegerField;
    qryMTituloFormaMovimentoDescrio: TStringField;
    qryMTituloMovimentoNrCheque: TIntegerField;
    qryMTituloMovimentoValor: TBCDField;
    qryMTituloMovimentoData: TDateTimeField;
    qryMTituloMovimentoCadastro: TDateTimeField;
    qryMTituloMovimentoCancel: TDateTimeField;
    qryMTituloMovimentoNrDoc: TStringField;
    qryMTituloMovimentoContab: TDateTimeField;
    dsMovimento: TDataSource;
    qryMTituloUsurioMovimento: TStringField;
    qryMTituloUsurioCancelamento: TStringField;
    Label30: TLabel;
    DBEdit39: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    qryMTituloOperaoConta: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryCategFinancTitulo: TADOQuery;
    dsCategFinancTitulo: TDataSource;
    qryCategFinancTitulonCdCategFinanc: TIntegerField;
    qryCategFinancTitulocNmCategFinanc: TStringField;
    qryCategFinancTitulonPercent: TBCDField;
    qryCategFinancTitulonValor: TBCDField;
    qryCentroCustoTitulo: TADOQuery;
    dsCentroCustoTitulo: TDataSource;
    DBGridEh3: TDBGridEh;
    DBEdit26: TDBEdit;
    DataSource1: TDataSource;
    DBEdit27: TDBEdit;
    DataSource2: TDataSource;
    DBEdit28: TDBEdit;
    DataSource3: TDataSource;
    DBEdit29: TDBEdit;
    DataSource4: TDataSource;
    DBEdit30: TDBEdit;
    DataSource5: TDataSource;
    DBEdit31: TDBEdit;
    DataSource6: TDataSource;
    DBEdit32: TDBEdit;
    DataSource7: TDataSource;
    DBEdit33: TDBEdit;
    DataSource8: TDataSource;
    qryCategFinancTitulonCdTitulo: TIntegerField;
    qryCCTituloAux: TADOQuery;
    qryCCTituloAuxnCdCentroCustoTitulo: TAutoIncField;
    qryCCTituloAuxnCdTitulo: TIntegerField;
    qryCCTituloAuxnCdCC: TIntegerField;
    qryCCTituloAuxnPercent: TBCDField;
    qryCCTituloAuxnValor: TBCDField;
    qryCategFinancTituloAux: TADOQuery;
    qryCategFinancTituloAuxnCdCategFinancTitulo: TAutoIncField;
    qryCategFinancTituloAuxnCdTitulo: TIntegerField;
    qryCategFinancTituloAuxnCdCategFinanc: TIntegerField;
    qryCategFinancTituloAuxnPercent: TBCDField;
    qryCategFinancTituloAuxnValor: TBCDField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    Label31: TLabel;
    DBEdit35: TDBEdit;
    DBEdit37: TDBEdit;
    DataSource9: TDataSource;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancariacAgencia: TIntegerField;
    qryContaBancarianCdCOnta: TStringField;
    qryContaBancariacNmTitular: TStringField;
    Label32: TLabel;
    DBEdit40: TDBEdit;
    DataSource10: TDataSource;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    Label33: TLabel;
    DBEdit45: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    Label34: TLabel;
    DBEdit46: TDBEdit;
    Label35: TLabel;
    DBEdit47: TDBEdit;
    cxTabSheet4: TcxTabSheet;
    Image2: TImage;
    Label36: TLabel;
    DBEdit48: TDBEdit;
    Label37: TLabel;
    DBEdit49: TDBEdit;
    Label38: TLabel;
    DBEdit50: TDBEdit;
    Label39: TLabel;
    DBEdit51: TDBEdit;
    Label40: TLabel;
    DBEdit52: TDBEdit;
    Label41: TLabel;
    DBEdit53: TDBEdit;
    Label42: TLabel;
    DBEdit54: TDBEdit;
    Label43: TLabel;
    DBEdit55: TDBEdit;
    Label45: TLabel;
    DBEdit57: TDBEdit;
    DBCheckBox3: TDBCheckBox;
    Label46: TLabel;
    DBEdit56: TDBEdit;
    cxButton2: TcxButton;
    Label44: TLabel;
    DBEdit58: TDBEdit;
    Label47: TLabel;
    DBEdit59: TDBEdit;
    qryFormaPagto: TADOQuery;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    DBEdit61: TDBEdit;
    DataSource11: TDataSource;
    qryMasternCdTitulo: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdEspTit: TIntegerField;
    qryMastercNrTit: TStringField;
    qryMasteriParcela: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdCategFinanc: TIntegerField;
    qryMasternCdMoeda: TIntegerField;
    qryMastercSenso: TStringField;
    qryMasterdDtEmissao: TDateTimeField;
    qryMasterdDtReceb: TDateTimeField;
    qryMasterdDtVenc: TDateTimeField;
    qryMasterdDtLiq: TDateTimeField;
    qryMasterdDtCad: TDateTimeField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternValTit: TBCDField;
    qryMasternValLiq: TBCDField;
    qryMasternSaldoTit: TBCDField;
    qryMasternValJuro: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMastercObsTit: TMemoField;
    qryMasternCdSP: TIntegerField;
    qryMasternCdBancoPortador: TIntegerField;
    qryMasterdDtBloqTit: TDateTimeField;
    qryMastercObsBloqTit: TStringField;
    qryMasternCdUnidadeNegocio: TIntegerField;
    qryMasternCdCC: TIntegerField;
    qryMasterdDtContab: TDateTimeField;
    qryMasternCdDoctoFiscal: TIntegerField;
    qryMasterdDtCalcJuro: TDateTimeField;
    qryMasternCdRecebimento: TIntegerField;
    qryMasternCdCrediario: TIntegerField;
    qryMasternCdTransacaoCartao: TIntegerField;
    qryMasternCdLanctoFin: TIntegerField;
    qryMastercFlgIntegrado: TIntegerField;
    qryMastercFlgDocCobranca: TIntegerField;
    qryMastercNrNF: TStringField;
    qryMasternCdProvisaoTit: TIntegerField;
    qryMastercFlgDA: TIntegerField;
    qryMasternCdContaBancariaDA: TIntegerField;
    qryMasternCdTipoLanctoDA: TIntegerField;
    qryMasternCdLojaTit: TIntegerField;
    qryMastercFlgInclusaoManual: TIntegerField;
    qryMasterdDtVencOriginal: TDateTimeField;
    qryMastercFlgCartaoConciliado: TIntegerField;
    qryMasternCdGrupoOperadoraCartao: TIntegerField;
    qryMasternCdOperadoraCartao: TIntegerField;
    qryMasternValTaxaOperadora: TBCDField;
    qryMasternCdLoteConciliacaoCartao: TIntegerField;
    qryMasterdDtIntegracao: TDateTimeField;
    qryMasternCdParcContratoEmpImobiliario: TIntegerField;
    qryMasternValAbatimento: TBCDField;
    qryMasternValMulta: TBCDField;
    qryMasterdDtNegativacao: TDateTimeField;
    qryMasternCdUsuarioNeg: TIntegerField;
    qryMasternCdItemListaCobrancaNeg: TIntegerField;
    qryMasternCdLojaNeg: TIntegerField;
    qryMasterdDtReabNeg: TDateTimeField;
    qryMastercFlgCobradora: TIntegerField;
    qryMasternCdCobradora: TIntegerField;
    qryMasterdDtRemCobradora: TDateTimeField;
    qryMasternCdUsuarioRemCobradora: TIntegerField;
    qryMasternCdItemListaCobRemCobradora: TIntegerField;
    qryMastercFlgReabManual: TIntegerField;
    qryMasternCdUsuarioReabManual: TIntegerField;
    qryMasternCdFormaPagtoTit: TIntegerField;
    qryTerceironCdFormaPagto: TIntegerField;
    Label48: TLabel;
    DBEdit60: TDBEdit;
    qryMasterdDtRemPortador: TDateTimeField;
    qryMasternPercTaxaJurosDia: TBCDField;
    qryMasternPercTaxaMulta: TBCDField;
    qryMasteriDiaCarenciaJuros: TIntegerField;
    Label49: TLabel;
    DBEdit62: TDBEdit;
    Label50: TLabel;
    DBEdit63: TDBEdit;
    Label51: TLabel;
    DBEdit64: TDBEdit;
    DBEdit38: TDBEdit;
    Label29: TLabel;
    DBEdit14: TDBEdit;
    Label14: TLabel;
    cxTabSheet5: TcxTabSheet;
    DBMemo1: TDBMemo;
    qryMTitulocOBSMov: TMemoField;
    qryMastercFlgRenegociado: TIntegerField;
    qryMastercCodBarra: TStringField;
    DBCheckBox4: TDBCheckBox;
    Label24: TLabel;
    DBEdit65: TDBEdit;
    DBEdit13: TDBEdit;
    Label13: TLabel;
    DBEdit66: TDBEdit;
    Label52: TLabel;
    qryMasternValEncargoCobradora: TBCDField;
    qryMastercFlgRetCobradoraManual: TIntegerField;
    DBCheckBox5: TDBCheckBox;
    popFuncoes: TPopupMenu;
    NegativarSPC1: TMenuItem;
    qryMastercFlgNegativadoManual: TIntegerField;
    DBCheckBox6: TDBCheckBox;
    qryNegativaTitulo: TADOQuery;
    btNegativarManual: TcxButton;
    qryCentroCustoTitulonCdCentroCustoTitulo: TIntegerField;
    qryCentroCustoTitulonCdTitulo: TIntegerField;
    qryCentroCustoTitulonCdCC: TIntegerField;
    qryCentroCustoTitulonPercent: TBCDField;
    qryCentroCustoTitulonValor: TBCDField;
    qryCentroCustoTitulonCdServidorOrigem: TIntegerField;
    qryCentroCustoTitulodDtReplicacao: TDateTimeField;
    qryCentroCustoTitulocNmCC: TStringField;
    qryCentroCusto: TADOQuery;
    qryCentroCustocNmCC: TStringField;
    qryTituloNegativado: TADOQuery;
    qryTituloNegativadodDtNegativacao: TDateTimeField;
    qryTituloNegativadodDtReabNeg: TDateTimeField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBEdit34KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit36KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit22KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBEdit11Enter(Sender: TObject);
    procedure DBEdit34Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit36Exit(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure DBEdit22Exit(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBEdit35Exit(Sender: TObject);
    procedure DBEdit35KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton2Click(Sender: TObject);
    procedure DBEdit60KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit60Exit(Sender: TObject);
    procedure DBEdit65Exit(Sender: TObject);
    procedure btNegativarManualClick(Sender: TObject);
    procedure qryCentroCustoTituloBeforePost(DataSet: TDataSet);
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryCentroCustoTituloCalcFields(DataSet: TDataSet);
    procedure qryCentroCustoTitulonCdCCChange(Sender: TField);
  private
    { Private declarations }
      nCdUnidadeNegocioOld : integer ;
      nCdEspTitOld         : integer ;
      nCdTerceiroOld       : integer ;
      nCdCategFinancOld    : integer ;
      nCdCCOld             : integer ;

  public
    { Public declarations }
  end;

var
  frmTitulo: TfrmTitulo;

implementation

uses fMenu, fLookup_Padrao, fTitulo_DetalheJuros;

{$R *.dfm}

procedure TfrmTitulo.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TITULO' ;
  nCdTabelaSistema  := 21 ;
  nCdConsultaPadrao := 36 ;
end;

procedure TfrmTitulo.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;

  qryMasternCdMoeda.Value := 1 ;
  qryMasteriParcela.Value := 1 ;
  PosicionaQuery(qryMoeda, qryMasternCdMoeda.AsString) ;

  DBEdit34.SetFocus ;

end;

procedure TfrmTitulo.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePage := cxTabSheet1 ;

  qryMasternPercTaxaJurosDia.DisplayFormat := '#,##0.0000' ;
  qryMasternPercTaxaMulta.DisplayFormat    := '#,##0.0000' ;
  
  if (frmMenu.LeParametro('VAREJO') = 'N') then
      desativaDBEdit( DBedit35 ) ;

  btNegativarManual.Enabled := (frmMenu.LeParametro('NEGATIVTITMAN')='S') ;

end;

procedure TfrmTitulo.btSalvarClick(Sender: TObject);
var
    qryAux : TADOQuery ;
begin

  if (qryMaster.State = dsEdit) and (qryMastercFlgInclusaoManual.Value = 0) then
  begin
      MensagemAlerta('T�tulo gerado autom�tico n�o pode ser alterado.') ;
      exit ;
  end ;

  if (qryMasternCdCategFinanc.Value = 0) or (dbEdit30.Text = '') then
  begin
      showMessage('Informe a categoria financeira') ;
      DBEdit7.SetFocus;
      abort ;
  end ;

  if (qryMasternCdFormaPagtoTit.Value > 0) and (dbEdit61.Text = '') then
  begin
      showMessage('Forma de Pagto Invalida') ;
      DBEdit60.SetFocus;
      abort ;
  end ;

  if (frmMenu.LeParametro('USARSP') = 'S') and (DBEdit9.Text = 'D') then
  begin
      if (qryMaster.State = dsInsert) then
      begin
          MensagemAlerta('N�o � permitida a inclus�o de t�tulos a pagar. Utilize a Solicita��o de Pagamento.') ;
          abort ;
      end ;

      if (qryMaster.State <> dsInsert) then
      begin
          MensagemAlerta('N�o � permitida a altera��o de t�tulos a pagar.') ;
          abort ;
      end ;
  end ;

  if (qryMaster.State <> dsInsert) then
  begin
      if (qryMasterdDtCancel.AsString <> '') then
      begin
          ShowMessage('T�tulo cancelado n�o pode ser alterado.') ;
          exit ;
      end ;

      if (qryMasternValLiq.Value > 0) or (qryMasternValDesconto.Value > 0) or (qryMasternValJuro.Value > 0) then
      begin
          ShowMessage('T�tulo tem movimentos e n�o pode ser alterado.') ;
          exit ;
      end ;

      if (qryMasternCdSP.Value > 0) then
      begin
          ShowMessage('T�tulo oriundo de SP n�o pode ser alterado.') ;
          exit ;
      end ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
    if (qryCategFinanccFlgRecDes.Value = 'R') then
      qryMastercSenso.Value := 'C' ;

    if (qryCategFinanccFlgRecDes.Value = 'D') then
      qryMastercSenso.Value := 'D' ;

    qryMasterdDtCad.Value := Now() ;

    If (qryMasteriParcela.Value <= 0) then
        qryMasteriParcela.Value := 1 ;

    if (qryMasternCdMoeda.Value = 0) then
        qryMasternCdMoeda.Value := 1 ;

    qryMastercFlgInclusaoManual.Value := 1 ;

  end ;

  qryMasternSaldoTit.Value := qryMasternValTit.Value ;
  
  if (qryMasternCdUnidadeNegocio.Value = 0) or (dbEdit27.Text = '') then
  begin
      showMessage('Informe a unidade de neg�cio') ;
      DBEdit34.SetFocus;
      abort ;
  end ;

  if (not DBEdit35.Readonly) and (DBEdit37.Text = '') then
  begin
      MensagemAlerta('Informe a loja.') ;
      DBEdit35.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdEspTit.Value = 0) or (dbEdit28.Text = '') then
  begin
      showMessage('Informe a esp�cie de t�tulo') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (qryMastercNrTit.Value = '') then
  begin
      showMessage('Informe o n�mero do t�tulo') ;
      DBEdit4.SetFocus;
      abort ;
  end ;

  if (qryMasternCdTerceiro.Value = 0) or (dbEdit29.Text = '') then
  begin
      showMessage('Informe o terceiro') ;
      DBEdit6.SetFocus;
      abort ;
  end ;

  if (qryMasternCdCC.Value = 0) or (dbEdit31.Text = '') then
  begin
      showMessage('Informe o centro de custo') ;
      DBEdit7.SetFocus;
      abort ;
  end ;

  if (qryMasternCdMoeda.Value = 0) or (dbEdit32.Text = '') then
  begin
      showMessage('Informe a moeda') ;
      DBEdit8.SetFocus;
      abort ;
  end ;

  if (qryMasterdDtEmissao.AsString = '') then
  begin
      showMessage('Informe a data de emiss�o') ;
      DBEdit10.SetFocus;
      abort ;
  end ;

  if (qryMasterdDtReceb.AsString = '') and (qryMastercSenso.Value = 'D') then
  begin
      showMessage('Para t�tulos � pagar, a data de recebimento do t�tulo � obr�gat�ria') ;
      DBEdit11.SetFocus;
      abort ;
  end ;

  if (qryMasterdDtVenc.AsString = '') then
  begin
      showMessage('Informe a data de vencimento') ;
      DBEdit12.SetFocus;
      abort ;
  end ;

  if (qryMasternValTit.Value <= 0) then
  begin
      showMessage('Valor do t�tulo inv�lido ou n�o informado') ;
      DBEdit16.SetFocus;
      abort ;
  end ;

  if    ((qryCategFinanccFlgRecDes.Value = 'D') and (qryEspTitcTipoMov.Value = 'R'))
     or ((qryCategFinanccFlgRecDes.Value = 'R') and (qryEspTitcTipoMov.Value = 'P')) then
  begin
      ShowMessage('Esta esp�cie de t�tulo n�o pode ser utilizada com esta categoria financeira') ;
      DbEdit3.SetFocus ;
      Abort ;
  end ;

  qryAux := TADOQuery.Create(Self);

  qryAux.Connection := frmMenu.Connection ;

  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM CentroCustoCategFinanc CCCF WHERE CCCF.nCdCC = :nCdCC AND CCCF.nCdCategFinanc = :nCdCategFinanc') ;
  qryAux.Parameters.ParamByName('nCdCC').Value := qryMasternCdCC.Value ;
  qryAux.Parameters.ParamByName('nCdCategFinanc').Value := qryMasternCdCategFinanc.Value ;
  qryAux.Open ;

  if (qryAux.eof) then
  begin
      qryAux := nil ;
      ShowMessage('N�o existe v�nculo entre este centro de custo e esta categoria financeira.') ;
      DbEdit36.SetFocus ;
      Abort ;
  end ;

  qryAux := nil ;

  inherited;

end;

procedure TfrmTitulo.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
     qryMaster.Close ;
     ShowMessage('Esta t�tulo n�o pertence a esta empresa.') ;
     btCancelar.Click;
     exit ;
  end ;

  qryEmpresa.Close ;
  qryUnidadeNegocio.Close ;
  qryEspTit.Close ;
  qryTerceiro.Close ;
  qryCategFinanc.Close ;
  qryCC.Close ;
  qryMoeda.Close ;
  qryBancoPortador.Close ;
  qryLoja.Close ;
  qryContaBancaria.Close ;
  qryFormaPagto.Close ;

  qryUnidadeNegocio.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  PosicionaQuery(qryUnidadeNegocio, qryMasternCdUnidadeNegocio.AsString) ;
  PosicionaQuery(qryEspTit, qryMasternCdEspTit.AsString) ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.AsString) ;
  PosicionaQuery(qryCategFinanc, qryMasternCdCategFinanc.AsString) ;
  PosicionaQuery(qryCC, qryMasternCdCC.AsString) ;
  PosicionaQuery(qryMoeda, qryMasternCdMoeda.AsString) ;
  PosicionaQuery(qryBancoPortador, qryMasternCdBancoPortador.AsString) ;
  PosicionaQuery(qryLoja, qryMasternCdLojaTit.asString) ;
  PosicionaQuery(qryContaBancaria, qryMasternCdContaBancariaDA.AsString) ;
  //PosicionaQuery(qryFormaPagto, DBEdit61.Text) ;
  PosicionaQuery(qryFormaPagto, qryMasternCdFormaPagtoTit.asString) ;

  qryMTitulo.Close ;
  qryMTitulo.Parameters.ParamByName('nCdTitulo').Value := qryMasternCdTitulo.Value ;
  qryMTitulo.Open ;

  PosicionaQuery(qryCategFinancTitulo, qryMasternCdTitulo.AsString) ;
  PosicionaQuery(qryCentroCustoTitulo, qryMasternCdTitulo.AsString) ;

end;

procedure TfrmTitulo.DBEdit34KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(35,'EXISTS(SELECT 1 FROM EmpresaUnidadeNegocio EUN WHERE EUN.nCdUnidadeNegocio = UnidadeNegocio.nCdUnidadeNegocio AND EUN.nCdEmpresa = ' + DBEdit2.Text + ')');

            If (nPK > 0) then
            begin
                qryMasternCdUnidadeNegocio.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTitulo.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (frmMenu.LeParametro('USARSP') = 'S') then
                nPK := frmLookup_Padrao.ExecutaConsulta(38)
            else
                nPK := frmLookup_Padrao.ExecutaConsulta(10);

            If (nPK > 0) then
            begin
                qryMasternCdEspTit.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTitulo.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTitulo.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (qryMasternCdEspTit.Value = 0) then
            begin
                ShowMessage('Informe a esp�cie de t�tulo.') ;
                exit ;
            end ;

            if (qryEspTitcTipoMov.Value = 'R') then
                nPK := frmLookup_Padrao.ExecutaConsulta2(32,'CategFinanc.cFlgRecDes = ' + Chr(39) + 'R' + Chr(39));

            if (qryEspTitcTipoMov.Value = 'P') then
                nPK := frmLookup_Padrao.ExecutaConsulta2(32,'CategFinanc.cFlgRecDes = ' + Chr(39) + 'D' + Chr(39));

            If (nPK > 0) then
            begin
                qryMasternCdCategFinanc.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTitulo.DBEdit36KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (qryMasternCdCategFinanc.asString = '') then
            begin
                ShowMessage('Informe a Categoria Financeira.') ;
                DBEdit7.SetFocus ;
                Exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(33,'EXISTS(SELECT 1 FROM CentroCustoCategFinanc CCCF WHERE CCCF.nCdCC = CentroCusto.nCdCC AND CCCF.nCdCategFinanc = ' + qryMasternCdCategFinanc.asString + ')');

            If (nPK > 0) then
            begin
                qryMasternCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTitulo.DBEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(16);

            If (nPK > 0) then
            begin
                qryMasternCdMoeda.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTitulo.DBEdit22KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(23);

            If (nPK > 0) then
            begin
                qryMasternCdBancoPortador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmTitulo.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryUnidadeNegocio.Close ;
  qryEspTit.Close ;
  qryTerceiro.Close ;
  qryCategFinanc.Close ;
  qryCC.Close ;
  qryMoeda.Close ;
  qryBancoPortador.Close ;
  qryLoja.Close ;
  qryContaBancaria.Close ;

  qryCategFinancTitulo.Close ;
  qryCentroCustoTitulo.Close ;

  qryMTitulo.Close ;
  qryFormaPagto.Close ;

end;

procedure TfrmTitulo.DBEdit11Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMasterdDtReceb.Value := qryMasterdDtEmissao.Value ;

end;

procedure TfrmTitulo.DBEdit34Exit(Sender: TObject);
begin
  inherited;

  qryUnidadeNegocio.Close ;
  qryUnidadeNegocio.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  PosicionaQuery(qryUnidadeNegocio, DBEdit34.Text) ;
end;

procedure TfrmTitulo.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryEspTit.Close ;
  PosicionaQuery(qryEspTit, DBEdit3.Text) ;

end;

procedure TfrmTitulo.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, DBEdit6.Text) ;
  If (qryMaster.State = dsInsert) and (qryTerceironCdFormaPagto.Value > 0) then
  begin
       qryFormaPagto.Close;
       qryMasternCdFormaPagtoTit.Value := qryTerceironCdFormaPagto.Value;
       PosicionaQuery(qryFormaPagto, qryMasternCdFormaPagtoTit.asString);
  end;
end;

procedure TfrmTitulo.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  qryCategFinanc.Close ;
  PosicionaQuery(qryCategFinanc, DBEdit7.Text) ;

  if (qryMaster.State <> dsBrowse) then
  begin
    if (qryCategFinanccFlgRecDes.Value = 'R') then
      qryMastercSenso.Value := 'C' ;

    if (qryCategFinanccFlgRecDes.Value = 'D') then
      qryMastercSenso.Value := 'D' ;

    If (qryMasteriParcela.Value <= 0) then
        qryMasteriParcela.Value := 1 ;

  end ;

end;

procedure TfrmTitulo.DBEdit36Exit(Sender: TObject);
begin
  inherited;

  qryCC.Close ;
  PosicionaQuery(qryCC, DBEdit36.Text) ;

end;

procedure TfrmTitulo.DBEdit8Exit(Sender: TObject);
begin
  inherited;

  qryMoeda.Close ;
  PosicionaQuery(qryMoeda, DBEdit8.Text) ;

end;

procedure TfrmTitulo.DBEdit22Exit(Sender: TObject);
begin
  inherited;

  qryBancoPortador.Close ;
  PosicionaQuery(qryBancoPortador, DBEdit22.Text) ;

end;

procedure TfrmTitulo.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryCentroCustoTitulo.Close ;
  PosicionaQuery(qryCentroCustoTitulo, qryMasternCdTitulo.asString) ;

  qryCategFinancTitulo.Close ;
  PosicionaQuery(qryCategFinancTitulo, qryMasternCdTitulo.asString) ;

  if (qryCentroCustoTitulo.RecordCount = 0) then
  begin

      qryCCTituloAux.Close ;
      qryCCTituloAux.Open ;

      qryCategFinancTituloAux.Close ;
      qryCategFinancTituloAux.Open  ;

      qryCCTituloAux.Insert ;
      qryCCTituloAuxnCdCentroCustoTitulo.Value := frmMenu.fnProximoCodigo('CENTROCUSTOTITULO') ;
      qryCCTituloAuxnCdTitulo.Value            := qryMasternCdTitulo.Value ;
      qryCCTituloAuxnCdCC.Value                := qryMasternCdCC.Value     ;
      qryCCTituloAuxnPercent.Value             := 100                      ;
      qryCCTituloAuxnValor.Value               := qryMasternValTit.Value   ;
      qryCCTituloAux.Post ;

      qryCategFinancTituloAux.Insert ;
      qryCategFinancTituloAuxnCdCategFinancTitulo.Value := frmMenu.fnProximoCodigo('CATEGFINANCTITULO') ;
      qryCategFinancTituloAuxnCdTitulo.Value            := qryMasternCdTitulo.Value      ;
      qryCategFinancTituloAuxnCdCategFinanc.Value       := qryMasternCdCategFinanc.Value ;
      qryCategFinancTituloAuxnPercent.Value             := 100                           ;
      qryCategFinancTituloAuxnValor.Value               := qryMasternValTit.Value        ;
      qryCategFinancTituloAux.Post ;
  end ;

  qryEmpresa.Close ;
  qryUnidadeNegocio.Close ;
  qryEspTit.Close ;
  qryTerceiro.Close ;
  qryCategFinanc.Close ;
  qryCC.Close ;
  qryMoeda.Close ;
  qryBancoPortador.Close ;
  qryLoja.Close ;
  qryContaBancaria.Close ;

  qryCategFinancTitulo.Close ;
  qryCentroCustoTitulo.Close ;

  qryMTitulo.Close ;
  qryFormaPagto.Close ;

end;

procedure TfrmTitulo.DBEdit35Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, DBEdit35.Text) ;

end;

procedure TfrmTitulo.DBEdit35KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(86,'Loja.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva)) ;

            If (nPK > 0) then
            begin
                qryMasternCdLojaTit.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTitulo.cxButton2Click(Sender: TObject);
begin

    {if (not qryMaster.Active) then
        exit ;

    PosicionaQuery(frmTitulo_DetalheJuros.qryTabelaJuro, qryMasternCdTitulo.AsString) ;
    frmTitulo_DetalheJuros.ShowModal ;}

end;

procedure TfrmTitulo.DBEdit60KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(15,'cFlgPermFinanceiro=1');

            //nPK := frmLookup_Padrao.ExecutaConsulta(15);

            If (nPK > 0) then
            begin
                qryMasternCdFormaPagtoTit.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTitulo.DBEdit60Exit(Sender: TObject);
begin
  inherited;
  qryFormaPagto.Close ;
  PosicionaQuery(qryFormaPagto, DBEdit60.Text) ;


end;

procedure TfrmTitulo.DBEdit65Exit(Sender: TObject);
begin
  inherited;
  if (qryMaster.State = dsInsert) then
  begin
      nCdUnidadeNegocioOld := qryMasternCdUnidadeNegocio.Value ;
      nCdEspTitOld         := qryMasternCdEspTit.Value ;
      nCdTerceiroOld       := qryMasternCdTerceiro.Value ;
      nCdCategFinancOld    := qryMasternCdCategFinanc.Value ;
      nCdCCOld             := qryMasternCdCC.Value ;

      btSalvar.Click;
      btIncluir.Click;

      qryMasternCdUnidadeNegocio.Value := nCdUnidadeNegocioOld ;
      qryMasternCdEspTit.Value         := nCdEspTitOld ;
      qryMasternCdTerceiro.Value       := nCdTerceiroOld ;
      qryMasternCdCategFinanc.Value    := nCdCategFinancOld ;
      qryMasternCdCC.Value             := nCdCCOld ;

  end ;

end;

procedure TfrmTitulo.btNegativarManualClick(Sender: TObject);
begin
  if (not qryMaster.Active) or (qryMasternCdTitulo.Value = 0) then
  begin
      MensagemErro('Nenhum t�tulo selecionado para negativa��o.') ;
      abort ;
  end ;

  if (qryMastercSenso.Value = 'D') then
  begin
      MensagemErro('S� � permitido a negativa��o de t�tulos a receber.') ;
      abort ;
  end ;

  if (qryMasternSaldoTit.Value <= 0) then
  begin
      MensagemErro('T�tulo sem saldo n�o pode ser negativado.');
      abort ;
  end ;

  if (qryMasterdDtCancel.asString <> '') then
  begin
      MensagemErro('N�o � permitido a negativa��o de um t�tulo cancelado.') ;
      abort ;
  end ;

  PosicionaQuery(qryTituloNegativado, qryMasternCdTitulo.asString) ;

  if ((qryTituloNegativadodDtNegativacao.AsString <> '') and (qryTituloNegativadodDtReabNeg.AsString = '')) then
//  if (qryMasterdDtNegativacao.AsString <> '') then     ==> Chamado 315   
  begin
      MensagemErro('T�tulo j� est� negativado.') ;
      abort ;
  end ;

  if (MessageDlg('Confirma a negativa��o deste t�tulo ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryNegativaTitulo.Close;
      qryNegativaTitulo.Parameters.ParamByName('nCdTitulo').Value  := qryMasternCdTitulo.Value;
      qryNegativaTitulo.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
      qryNegativaTitulo.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryNegativaTitulo.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryMaster.Requery();

  ShowMessage('T�tulo negativado com sucesso.') ;

end;

procedure TfrmTitulo.qryCentroCustoTituloBeforePost(DataSet: TDataSet);
begin

  if (qryCentroCustoTitulocNmCC.Value = '') then
  begin
      MensagemAlerta('Selecione o centro de custo.') ;
      abort ;
  end;

  if (qryCentroCustoTitulonPercent.Value <= 0) then
  begin
      MensagemAlerta('Informe o percentual de rateio.') ;
      abort ;
  end ;

  if (qryCentroCustoTitulo.State = dsInsert) then
      qryCentroCustoTitulonCdCentroCustoTitulo.Value := frmMenu.fnProximoCodigo('CENTROCUSTOTITULO') ;

  qryCentroCustoTitulonValor.Value    := (qryMasternValTit.Value * ( qryCentroCustoTitulonPercent.Value / 100 ) );
  qryCentroCustoTitulonCdTitulo.Value := qryMasternCdTitulo.Value ;

  inherited;

end;

procedure TfrmTitulo.DBGridEh3KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh3.Columns[DBGridEh3.Col-1].FieldName = 'nCdCC') then
        begin
            if (qryCentroCustoTitulo.State = dsBrowse) then
                qryCentroCustoTitulo.Edit ;

            nPK := frmLookup_Padrao.ExecutaConsulta(29) ;

            if (nPK > 0) then
            begin
                qryCentroCustoTitulonCdCC.Value := nPk;
            end ;

        end;

    end ;

  end ;

end;

procedure TfrmTitulo.qryCentroCustoTituloCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryCentroCusto.Close;
  PosicionaQuery( qryCentroCusto , qryCentroCustoTitulonCdCC.AsString ) ;

  if not qryCentroCusto.eof then
      qryCentroCustoTitulocNmCC.Value := qryCentroCustocNmCC.Value ;

end;

procedure TfrmTitulo.qryCentroCustoTitulonCdCCChange(Sender: TField);
begin
  inherited;

  qryCentroCusto.Close;
  PosicionaQuery( qryCentroCusto , qryCentroCustoTitulonCdCC.AsString ) ;

  if not qryCentroCusto.eof then
      qryCentroCustoTitulocNmCC.Value := qryCentroCustocNmCC.Value ;

end;

initialization
    RegisterClass(TfrmTitulo) ;


end.
