inherited frmCriterioSolicitacaoCD: TfrmCriterioSolicitacaoCD
  Width = 980
  Caption = 'Crit'#233'rio de Solicita'#231#227'o p/ Centro de Distribui'#231#227'o'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 964
  end
  object Label1: TLabel [1]
    Left = 32
    Top = 45
    Width = 24
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'd.'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 7
    Top = 69
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit1
  end
  object Label3: TLabel [3]
    Left = 24
    Top = 93
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit1
  end
  inherited ToolBar2: TToolBar
    Width = 964
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 60
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdCriterioSolicitacaoCD'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 60
    Top = 64
    Width = 433
    Height = 19
    DataField = 'cNmCriterioSolicitacaoCD'
    DataSource = dsMaster
    TabOrder = 2
  end
  object edtStatus: TER2LookupDBEdit [7]
    Left = 60
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 3
    CodigoLookup = 2
    QueryLookup = qryStat
  end
  object DBEdit3: TDBEdit [8]
    Tag = 1
    Left = 128
    Top = 88
    Width = 169
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsStat
    TabOrder = 4
  end
  object pageSolicitacao: TcxPageControl [9]
    Left = 8
    Top = 120
    Width = 873
    Height = 281
    ActivePage = tabDeptoMarca
    LookAndFeel.NativeStyle = True
    TabOrder = 5
    ClientRectBottom = 277
    ClientRectLeft = 4
    ClientRectRight = 869
    ClientRectTop = 24
    object tabDeptoMarca: TcxTabSheet
      Caption = 'Estrutura de Produtos'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 865
        Height = 253
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsProdutoCriterioSolicitacaoCD
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdDepartamento'
            Footers = <>
            Title.Caption = 'Departamento|C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmDepartamento'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Departamento|Descri'#231#227'o'
            Width = 200
          end
          item
            EditButtons = <>
            FieldName = 'nCdCategoria'
            Footers = <>
            Title.Caption = 'Categoria|C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmCategoria'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Categoria|Descri'#231#227'o'
            Width = 200
          end
          item
            EditButtons = <>
            FieldName = 'nCdSubCategoria'
            Footers = <>
            Title.Caption = 'Sub Categoria|C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmSubCategoria'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Sub Categoria|Descri'#231#227'o'
            Width = 200
          end
          item
            EditButtons = <>
            FieldName = 'nCdSegmento'
            Footers = <>
            Title.Caption = 'Segmento|C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmSegmento'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Segmento|Descri'#231#227'o'
            Width = 200
          end
          item
            EditButtons = <>
            FieldName = 'nCdMarca'
            Footers = <>
            Title.Caption = 'Marca|C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmMarca'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Marca|Descri'#231#227'o'
            Width = 186
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeMinEstoque'
            Footers = <>
            Title.Caption = 'Qtde. M'#237'n. em Estoque da Loja'
            Width = 100
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeMinSolicitar'
            Footers = <>
            Title.Caption = 'Qtde. M'#237'n. '#224' Solicitar'
            Width = 100
          end
          item
            Checkboxes = True
            EditButtons = <>
            FieldName = 'cFlgIgnorarEstNegativo'
            Footers = <>
            Title.Caption = 'Ignorar Est. Negativo/Zerado do CD?'
            Width = 100
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object tabProduto: TcxTabSheet
      Caption = 'Produtos'
      ImageIndex = 1
      object gridProduto: TDBGridEh
        Left = 0
        Top = 0
        Width = 865
        Height = 253
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsProdutoCD
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = gridProdutoEnter
        OnKeyUp = gridProdutoKeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            Title.Caption = 'Produto|C'#243'd.'
          end
          item
            EditButtons = <>
            FieldName = 'cNmProduto'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Produto|Descri'#231#227'o'
            Width = 458
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeMinEstoque'
            Footers = <>
            Title.Caption = 'Qtde. M'#237'n. em Estoque da Loja'
            Width = 100
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeMinSolicitar'
            Footers = <>
            Title.Caption = 'Qtde. M'#237'n. '#224' Solicitar'
            Width = 100
          end
          item
            Checkboxes = True
            EditButtons = <>
            FieldName = 'cFlgIgnorarEstNegativo'
            Footers = <>
            Title.Caption = 'Ignorar Est. Negativo/Zerado do CD?'
            Width = 100
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterEdit = qryMasterAfterEdit
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCriterioSolicitacaoCD'
      '      ,cNmCriterioSolicitacaoCD'
      '      ,dDtCadastro'
      '      ,nCdUsuarioCadastro'
      '      ,nCdStatus'
      '  FROM CriterioSolicitacaoCD'
      ' WHERE nCdCriterioSolicitacaoCD = :nPK')
    Left = 336
    Top = 304
    object qryMasternCdCriterioSolicitacaoCD: TIntegerField
      FieldName = 'nCdCriterioSolicitacaoCD'
    end
    object qryMastercNmCriterioSolicitacaoCD: TStringField
      FieldName = 'cNmCriterioSolicitacaoCD'
      Size = 50
    end
    object qryMasterdDtCadastro: TDateTimeField
      FieldName = 'dDtCadastro'
    end
    object qryMasternCdUsuarioCadastro: TIntegerField
      FieldName = 'nCdUsuarioCadastro'
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  inherited dsMaster: TDataSource
    Left = 336
    Top = 336
  end
  inherited qryID: TADOQuery
    Left = 624
    Top = 336
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 400
    Top = 304
  end
  inherited qryStat: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Status'
      ' WHERE nCdStatus = :nPK ')
    Left = 368
    Top = 304
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 400
    Top = 336
  end
  inherited ImageList1: TImageList
    Left = 432
    Top = 304
  end
  object ProdutoCriterioSolicitacaoCD: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = ProdutoCriterioSolicitacaoCDBeforePost
    OnCalcFields = ProdutoCriterioSolicitacaoCDCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProdutoCriterioSolicitacaoCD'
      '      ,nCdCriterioSolicitacaoCD'
      '      ,nCdDepartamento'
      '      ,nCdCategoria'
      '      ,nCdSubCategoria'
      '      ,nCdSegmento'
      '      ,nCdMarca'
      '      ,nQtdeMinEstoque'
      '      ,nQtdeMinSolicitar'
      '      ,cFlgIgnorarEstNegativo'
      '  FROM ProdutoCriterioSolicitacaoCD'
      ' WHERE nCdCriterioSolicitacaoCD = :nPK'
      '   AND nCdProduto               IS NULL')
    Left = 464
    Top = 304
    object ProdutoCriterioSolicitacaoCDnCdProdutoCriterioSolicitacaoCD: TIntegerField
      FieldName = 'nCdProdutoCriterioSolicitacaoCD'
    end
    object ProdutoCriterioSolicitacaoCDnCdCriterioSolicitacaoCD: TIntegerField
      FieldName = 'nCdCriterioSolicitacaoCD'
    end
    object ProdutoCriterioSolicitacaoCDnCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object ProdutoCriterioSolicitacaoCDnCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object ProdutoCriterioSolicitacaoCDnQtdeMinEstoque: TIntegerField
      FieldName = 'nQtdeMinEstoque'
    end
    object ProdutoCriterioSolicitacaoCDnQtdeMinSolicitar: TIntegerField
      FieldName = 'nQtdeMinSolicitar'
    end
    object ProdutoCriterioSolicitacaoCDcNmDepartamento: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmDepartamento'
      LookupDataSet = qryDepartamento
      LookupKeyFields = 'nCdDepartamento'
      LookupResultField = 'cNmDepartamento'
      KeyFields = 'nCdDepartamento'
      LookupCache = True
      Lookup = True
    end
    object ProdutoCriterioSolicitacaoCDcNmMarca: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmMarca'
      LookupDataSet = qryMarca
      LookupKeyFields = 'nCdMarca'
      LookupResultField = 'cNmMarca'
      KeyFields = 'nCdMarca'
      LookupCache = True
      Lookup = True
    end
    object ProdutoCriterioSolicitacaoCDcFlgIgnorarEstNegativo: TIntegerField
      FieldName = 'cFlgIgnorarEstNegativo'
    end
    object ProdutoCriterioSolicitacaoCDnCdCategoria: TIntegerField
      FieldName = 'nCdCategoria'
    end
    object ProdutoCriterioSolicitacaoCDcNmCategoria: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmCategoria'
      Size = 50
      Calculated = True
    end
    object ProdutoCriterioSolicitacaoCDnCdSubCategoria: TIntegerField
      FieldName = 'nCdSubCategoria'
    end
    object ProdutoCriterioSolicitacaoCDcNmSubCategoria: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmSubCategoria'
      Size = 50
      Calculated = True
    end
    object ProdutoCriterioSolicitacaoCDnCdSegmento: TIntegerField
      FieldName = 'nCdSegmento'
    end
    object ProdutoCriterioSolicitacaoCDcNmSegmento: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmSegmento'
      Size = 50
      Calculated = True
    end
  end
  object dsProdutoCriterioSolicitacaoCD: TDataSource
    DataSet = ProdutoCriterioSolicitacaoCD
    Left = 464
    Top = 336
  end
  object dsProdutoCD: TDataSource
    DataSet = qryProdutoCD
    Left = 496
    Top = 336
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdDepartamento'
      '      ,cNmDepartamento'
      '  FROM Departamento')
    Left = 528
    Top = 304
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdMarca'
      '      ,cNmMarca'
      '  FROM Marca')
    Left = 592
    Top = 304
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmProduto'
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK')
    Left = 592
    Top = 336
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmUsuario'
      '  FROM Usuario'
      ' WHERE nCdUsuario = :nPK')
    Left = 624
    Top = 304
  end
  object dsStat: TDataSource
    DataSet = qryStat
    Left = 368
    Top = 336
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 432
    Top = 336
  end
  object qryProdutoCD: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryProdutoCDBeforePost
    OnCalcFields = qryProdutoCDCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProdutoCriterioSolicitacaoCD'
      '      ,nCdCriterioSolicitacaoCD'
      '      ,nCdProduto'
      '      ,nQtdeMinEstoque'
      '      ,nQtdeMinSolicitar'
      '      ,cFlgIgnorarEstNegativo'
      '  FROM ProdutoCriterioSolicitacaoCD'
      ' WHERE nCdCriterioSolicitacaoCD = :nPK'
      '   AND nCdProduto IS NOT NULL')
    Left = 496
    Top = 304
    object qryProdutoCDnCdProdutoCriterioSolicitacaoCD: TIntegerField
      FieldName = 'nCdProdutoCriterioSolicitacaoCD'
    end
    object qryProdutoCDnCdCriterioSolicitacaoCD: TIntegerField
      FieldName = 'nCdCriterioSolicitacaoCD'
    end
    object qryProdutoCDnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutoCDnQtdeMinEstoque: TIntegerField
      FieldName = 'nQtdeMinEstoque'
    end
    object qryProdutoCDnQtdeMinSolicitar: TIntegerField
      FieldName = 'nQtdeMinSolicitar'
    end
    object qryProdutoCDcFlgIgnorarEstNegativo: TIntegerField
      FieldName = 'cFlgIgnorarEstNegativo'
    end
    object qryProdutoCDcNmProduto: TStringField
      DisplayWidth = 150
      FieldKind = fkCalculated
      FieldName = 'cNmProduto'
      Size = 150
      Calculated = True
    end
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSubCategoria'
      ',cNmSubCategoria'
      'FROM SubCategoria'
      'WHERE nCdCategoria = :nCdCategoria'
      'AND nCdSubCategoria = :nPK')
    Left = 560
    Top = 304
    object qrySubCategorianCdSubCategoria: TAutoIncField
      FieldName = 'nCdSubCategoria'
      ReadOnly = True
    end
    object qrySubCategoriacNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdSubCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSegmento'
      ',cNmSegmento'
      'FROM Segmento'
      'WHERE nCdSubCategoria = :nCdSubCategoria'
      'AND nCdSegmento = :nPK')
    Left = 560
    Top = 336
    object qrySegmentonCdSegmento: TAutoIncField
      FieldName = 'nCdSegmento'
      ReadOnly = True
    end
    object qrySegmentocNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDepartamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCategoria'
      '      ,cNmCategoria'
      '  FROM Categoria'
      ' WHERE nCdDepartamento = :nCdDepartamento'
      '   AND nCdCategoria    = :nPK')
    Left = 528
    Top = 336
    object qryCategorianCdCategoria: TAutoIncField
      FieldName = 'nCdCategoria'
      ReadOnly = True
    end
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
  end
end
