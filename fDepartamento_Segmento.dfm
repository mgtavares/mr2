inherited frmDepartamento_Segmento: TfrmDepartamento_Segmento
  Left = 394
  Top = 225
  Width = 574
  Height = 409
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Segmento'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 558
    Height = 342
  end
  inherited ToolBar1: TToolBar
    Width = 558
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 558
    Height = 342
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsSegmento
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'nCdSegmento'
        Footers = <>
        ReadOnly = True
        Tag = 1
      end
      item
        EditButtons = <>
        FieldName = 'nCdSubCategoria'
        Footers = <>
        Visible = False
        Width = 61
      end
      item
        EditButtons = <>
        FieldName = 'cNmSegmento'
        Footers = <>
        Width = 323
      end
      item
        EditButtons = <>
        FieldName = 'nCdStatus'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdIdExternoWeb'
        Footers = <>
        Title.Caption = 'Segmentos|C'#243'd. Externo Web'
        Width = 65
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 336
    Top = 208
  end
  object dsSegmento: TDataSource
    DataSet = qrySegmento
    Left = 304
    Top = 208
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNmSegmento'
        DataType = ftString
        Precision = 50
        Size = 50
        Value = Null
      end
      item
        Name = 'nCdSubCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      'FROM Segmento'
      'WHERE cNmSegmento = :cNmSegmento'
      'AND nCdSubCategoria = :nCdSubCategoria')
    Left = 336
    Top = 176
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qrySegmentoBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Segmento'
      'WHERE nCdSubCategoria = :nPK')
    Left = 304
    Top = 176
    object qrySegmentonCdSegmento: TAutoIncField
      DisplayLabel = 'Segmentos|C'#243'd'
      FieldName = 'nCdSegmento'
    end
    object qrySegmentonCdSubCategoria: TIntegerField
      DisplayLabel = 'Segmentos|'
      FieldName = 'nCdSubCategoria'
    end
    object qrySegmentocNmSegmento: TStringField
      DisplayLabel = 'Segmentos|Descri'#231#227'o'
      FieldName = 'cNmSegmento'
      Size = 50
    end
    object qrySegmentonCdStatus: TIntegerField
      DisplayLabel = 'Segmentos|Status'
      FieldName = 'nCdStatus'
    end
    object qrySegmentonCdIdExternoWeb: TIntegerField
      FieldName = 'nCdIdExternoWeb'
    end
  end
end
