object frmPdvFormaPagto: TfrmPdvFormaPagto
  Left = 291
  Top = 172
  Width = 508
  Height = 374
  BorderIcons = []
  Caption = 'Sele'#231#227'o da Forma de Pagamento'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = '@Microsoft JhengHei'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 15
  object cxGrid1: TcxGrid
    Left = 0
    Top = 0
    Width = 492
    Height = 338
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      OnKeyDown = cxGrid1DBTableView1KeyDown
      DataController.DataSource = dsFormaPagto
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnFiltering = False
      OptionsCustomize.ColumnMoving = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLineColor = clSilver
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.ConteudoCaixaGrande
      Styles.Header = frmMenu.HeaderPDV
      object cxGrid1DBTableView1nCdFormaPagto: TcxGridDBColumn
        Caption = 'C'#243'digo'
        DataBinding.FieldName = 'nCdFormaPagto'
      end
      object cxGrid1DBTableView1cNmFormaPagto: TcxGridDBColumn
        Caption = 'Descri'#231#227'o'
        DataBinding.FieldName = 'cNmFormaPagto'
        Width = 371
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cFlgLiqCrediario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cFlgSomenteCartao'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @cFlgLiqCrediario  int'
      '       ,@cFlgSomenteCartao int'
      ''
      'Set @cFlgLiqCrediario  = :cFlgLiqCrediario'
      'Set @cFlgSomenteCartao = :cFlgSomenteCartao'
      ''
      'IF (@cFlgSomenteCartao = 0)'
      'BEGIN'
      '    IF (@cFlgLiqCrediario = 1)'
      '    BEGIN'
      '        SELECT nCdFormaPagto'
      '              ,cNmFormaPagto'
      '              ,cFlgTEF'
      '          FROM FormaPagto'
      '         WHERE cFlgAtivo        = 1'
      '           AND cFlgLiqCrediario = 1'
      '           AND cFlgPermCaixa    = 1'
      '         ORDER BY cNmFormaPagto'
      '    END'
      '    ELSE'
      '    BEGIN'
      '        SELECT nCdFormaPagto'
      '              ,cNmFormaPagto'
      '              ,cFlgTEF'
      '          FROM FormaPagto'
      '         WHERE cFlgAtivo     = 1'
      '           AND cFlgPermCaixa = 1'
      '         ORDER BY cNmFormaPagto'
      '    END'
      'END'
      'ELSE'
      'BEGIN'
      '    SELECT nCdFormaPagto'
      '          ,cNmFormaPagto'
      '          ,cFlgTEF'
      '      FROM FormaPagto'
      '     WHERE cFlgAtivo     = 1'
      '       AND cFlgPermCaixa = 1'
      '       AND nCdTabTipoFormaPagto IN (3,4)'
      '     ORDER BY cNmFormaPagto'
      'END')
    Left = 392
    Top = 168
    object qryFormaPagtonCdFormaPagto: TIntegerField
      DisplayLabel = 'Formas de Pagamento|C'#243'digo'
      FieldName = 'nCdFormaPagto'
    end
    object qryFormaPagtocNmFormaPagto: TStringField
      DisplayLabel = 'Formas de Pagamento|Descri'#231#227'o'
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryFormaPagtocFlgTEF: TIntegerField
      FieldName = 'cFlgTEF'
    end
  end
  object dsFormaPagto: TDataSource
    DataSet = qryFormaPagto
    Left = 360
    Top = 176
  end
end
