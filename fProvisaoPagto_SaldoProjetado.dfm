inherited frmProvisaoPagtoSaldoProjetado: TfrmProvisaoPagtoSaldoProjetado
  Left = 197
  Top = 168
  Width = 528
  Caption = 'Saldo Projetado'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 512
  end
  inherited ToolBar1: TToolBar
    Width = 512
    ButtonWidth = 100
    inherited ToolButton1: TToolButton
      Caption = '&Atualizar Tela'
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Left = 100
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Left = 108
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 512
    Height = 435
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataSource = DataSource1
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'dDiaSaldo'
        Footers = <>
        Width = 105
      end
      item
        EditButtons = <>
        FieldName = 'nValProvisaoPagto'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValReceitaPrevista'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoDia'
        Footers = <>
      end>
  end
  object DataSource1: TDataSource
    DataSet = uspConsulta
    Left = 224
    Top = 128
  end
  object uspConsulta: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_SALDO_PROJETADO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdEmpresaAtiva'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdContaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 184
    Top = 128
    object uspConsultadDiaSaldo: TDateTimeField
      DisplayLabel = 'Saldo Futuro Projetado|Data Saldo'
      FieldName = 'dDiaSaldo'
    end
    object uspConsultanValProvisaoPagto: TBCDField
      DisplayLabel = 'Saldo Futuro Projetado|Prov. Pagto'
      FieldName = 'nValProvisaoPagto'
      Precision = 16
      Size = 2
    end
    object uspConsultanValReceitaPrevista: TBCDField
      DisplayLabel = 'Saldo Futuro Projetado|Prev. Receita Cart'#227'o'
      FieldName = 'nValReceitaPrevista'
      Precision = 16
      Size = 2
    end
    object uspConsultanSaldoDia: TBCDField
      DisplayLabel = 'Saldo Futuro Projetado|Saldo Projetado'
      FieldName = 'nSaldoDia'
      Precision = 16
      Size = 2
    end
  end
end
