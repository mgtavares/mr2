unit fConsultaRemessaNumerario_Lancamentos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DBGridEhGrouping, ToolCtrlsEh, GridsEh,
  DBGridEh, cxPC, cxControls, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, MemTableDataEh, MemTableEh;

type
  TfrmConsultaRemessaNumerario_Lancamentos = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cxTabSheet2: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    cxTabSheet3: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    dsCrediarioRemessaNum: TDataSource;
    dsCartaoRemessaNum: TDataSource;
    dsChequeRemessaNum: TDataSource;
    qryChequeRemessaNum: TADOQuery;
    qryChequeRemessaNumnCdChequeRemessaNum: TIntegerField;
    qryChequeRemessaNumnCdBanco: TIntegerField;
    qryChequeRemessaNumcAgencia: TStringField;
    qryChequeRemessaNumiNrCheque: TIntegerField;
    qryChequeRemessaNumnValCheque: TBCDField;
    qryChequeRemessaNumcNmTerceiro: TStringField;
    qryCartaoRemessaNum: TADOQuery;
    qryCartaoRemessaNumdDtTransacao: TDateTimeField;
    qryCartaoRemessaNumcNmOperadoraCartao: TStringField;
    qryCartaoRemessaNumiParcelas: TIntegerField;
    qryCartaoRemessaNumiNrDocto: TIntegerField;
    qryCartaoRemessaNumiNrAutorizacao: TIntegerField;
    qryCartaoRemessaNumnValTransacao: TBCDField;
    qryCrediarioRemessaNum: TADOQuery;
    qryCrediarioRemessaNumnCdRemessaNum: TIntegerField;
    qryCrediarioRemessaNumnCdCrediario: TIntegerField;
    qryCrediarioRemessaNumdDtVenda: TDateTimeField;
    qryCrediarioRemessaNumcNmTerceiro: TStringField;
    qryCrediarioRemessaNumiParcelas: TIntegerField;
    qryCrediarioRemessaNumnValCrediario: TBCDField;
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaRemessaNumerario_Lancamentos: TfrmConsultaRemessaNumerario_Lancamentos;

implementation

{$R *.dfm}

procedure TfrmConsultaRemessaNumerario_Lancamentos.FormActivate(
  Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0;
  DBGridEh1.SetFocus;
end;

end.
