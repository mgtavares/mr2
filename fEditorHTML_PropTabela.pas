unit fEditorHTML_PropTabela;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Buttons, cxLookAndFeelPainters, cxButtons;

type
  TfrmEditorHTML_PropTabela = class(TfrmProcesso_Padrao)
    edtLinhas: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtCelulas: TEdit;
    Label3: TLabel;
    edtEspacamento: TEdit;
    Label4: TLabel;
    edtLargura: TEdit;
    Largura: TLabel;
    Label5: TLabel;
    edtAltura: TEdit;
    Label6: TLabel;
    ckBorda: TComboBox;
    imgBorda: TImageList;
    ColorDialog1: TColorDialog;
    edtRGB: TEdit;
    Label7: TLabel;
    ckAlinhamento: TComboBox;
    btCores: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
    procedure ckBordaDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure btCoresClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
    bProcesso : Boolean;
    corFundo  : TColor;
  end;

var
  frmEditorHTML_PropTabela: TfrmEditorHTML_PropTabela;

implementation

uses
  Types;

{$R *.dfm}

procedure TfrmEditorHTML_PropTabela.FormCreate(Sender: TObject);
begin
  inherited;
  bProcesso := false;
  edtRGB.ReadOnly := true;
  
  {-- Combobox Borda --}
  ckBorda.Style := csOwnerDrawVariable;
end;

procedure TfrmEditorHTML_PropTabela.ToolButton1Click(Sender: TObject);
begin
  inherited;
  bProcesso := true;
  close;
end;

procedure TfrmEditorHTML_PropTabela.ToolButton2Click(Sender: TObject);
begin
  inherited;
  bProcesso := false;
end;

procedure TfrmEditorHTML_PropTabela.ckBordaDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  CenterText : Integer;
begin
  inherited;

  ckBorda.Canvas.FillRect(Rect);
  imgBorda.Draw(ckBorda.Canvas, Rect.Left + 4, Rect.Top + 4, Index);
  CenterText := (Rect.Bottom - Rect.Top - ckBorda.Canvas.TextHeight(text)) div 2;
  ckBorda.Canvas.TextOut(Rect.Left + imgBorda.Width + 8 , Rect.Top + CenterText, ckBorda.Items.Strings[index]);
end;

procedure TfrmEditorHTML_PropTabela.btCoresClick(Sender: TObject);
begin
  inherited;

  if (ColorDialog1.Execute) then
  begin
      corFundo := ColorDialog1.Color;
      try
          edtRGB.ReadOnly := false;
          edtRGB.Text := IntToStr(GetRValue(ColorToRGB(corFundo))) + ','
                       + IntToStr(GetGValue(ColorToRGB(corFundo))) + ','
                       + IntToStr(GetGValue(ColorToRGB(corFundo)));
      finally
          edtRGB.ReadOnly := true;
      end;
  end;
end;

end.
