inherited frmGeraDoctoFiscalCompl: TfrmGeraDoctoFiscalCompl
  Left = 129
  Top = 95
  Width = 1006
  Height = 612
  Caption = 'Gera'#231#227'o Doc. Fiscal Complementar'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 121
    Width = 990
    Height = 455
  end
  inherited ToolBar1: TToolBar
    Width = 990
    inherited ToolButton1: TToolButton
      Caption = '&Pesquisar'
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 990
    Height = 92
    Align = alTop
    Caption = 'Filtros'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 13
      Top = 24
      Width = 100
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro Destinat'#225'rio'
    end
    object Label2: TLabel
      Tag = 1
      Left = 21
      Top = 48
      Width = 92
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo de Emiss'#227'o'
    end
    object Label3: TLabel
      Tag = 1
      Left = 18
      Top = 72
      Width = 95
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Nota Fiscal de'
    end
    object Label4: TLabel
      Tag = 1
      Left = 200
      Top = 72
      Width = 16
      Height = 13
      Alignment = taRightJustify
      Caption = 'at'#233
    end
    object Label5: TLabel
      Tag = 1
      Left = 205
      Top = 48
      Width = 6
      Height = 13
      Alignment = taRightJustify
      Caption = #224
    end
    object edtTerceiro: TMaskEdit
      Left = 118
      Top = 16
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      OnExit = edtTerceiroExit
      OnKeyDown = edtTerceiroKeyDown
    end
    object edtEmissaoFinal: TMaskEdit
      Left = 224
      Top = 40
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object edtNotaInicial: TMaskEdit
      Left = 118
      Top = 64
      Width = 74
      Height = 21
      EditMask = '#######;1; '
      MaxLength = 7
      TabOrder = 3
      Text = '       '
    end
    object edtNotaFinal: TMaskEdit
      Left = 224
      Top = 64
      Width = 74
      Height = 21
      EditMask = '#######;1; '
      MaxLength = 7
      TabOrder = 4
      Text = '       '
    end
    object edtEmissaoInicial: TMaskEdit
      Left = 118
      Top = 40
      Width = 74
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 192
      Top = 16
      Width = 430
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = dsTerceiro
      TabOrder = 5
    end
  end
  object cxGrid2: TcxGrid [3]
    Left = 0
    Top = 121
    Width = 990
    Height = 455
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 2
    LookAndFeel.NativeStyle = True
    object cxGridDBTableView1: TcxGridDBTableView
      OnDblClick = cxGridDBTableView1DblClick
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTotal'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#'
          Kind = skCount
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnGrouping = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellMultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.FonteSomenteLeitura
      object cxGridDBTableView1nCdDoctoFiscal: TcxGridDBColumn
        DataBinding.FieldName = 'nCdDoctoFiscal'
        Width = 53
      end
      object cxGridDBTableView1dDtEmissao: TcxGridDBColumn
        DataBinding.FieldName = 'dDtEmissao'
      end
      object cxGridDBTableView1iNrDocto: TcxGridDBColumn
        DataBinding.FieldName = 'iNrDocto'
        Width = 87
      end
      object cxGridDBTableView1nCdSerieFiscal: TcxGridDBColumn
        Caption = 'S'#233'rie'
        DataBinding.FieldName = 'nCdSerieFiscal'
        Width = 63
      end
      object cxGridDBTableView1cCFOP: TcxGridDBColumn
        DataBinding.FieldName = 'cCFOP'
        Width = 60
      end
      object cxGridDBTableView1nCdTerceiro: TcxGridDBColumn
        DataBinding.FieldName = 'nCdTerceiro'
        Width = 107
      end
      object cxGridDBTableView1cNmTerceiro: TcxGridDBColumn
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 316
      end
      object cxGridDBTableView1nValTotal: TcxGridDBColumn
        DataBinding.FieldName = 'nValTotal'
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 456
    Top = 32
  end
  object qryNotasFiscais: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'iNrInicial'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'iNrFinal'
        DataType = ftString
        Size = 2
        Value = '10'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro           int'
      '       ,@dDtInicial            varchar(10)'
      '       ,@dDtFinal              varchar(10)'
      '       ,@nCdDoctoFiscalInicial int'
      '       ,@nCdDoctoFiscalFinal   int'
      ''
      'SET @nCdTerceiro           = :nCdTerceiro'
      'SET @dDtInicial            = :dDtInicial'
      'SET @dDtFinal              = :dDtFinal'
      'SET @nCdDoctoFiscalInicial = :iNrInicial'
      'SET @nCdDoctoFiscalFinal   = :iNrFinal'
      ''
      ''
      'SELECT nCdDoctoFiscal'
      '      ,nCdSerieFiscal'
      '      ,cCFOP'
      '      ,iNrDocto'
      '      ,dDtEmissao'
      '      ,nCdTerceiro'
      '      ,cNmTerceiro'
      '      ,nValTotal '
      '  FROM DoctoFiscal'
      
        ' WHERE ((@nCdTerceiro    = 0)                                  O' +
        'R (nCdTerceiro            = @nCdTerceiro))'
      
        '   AND ((dDtEmissao     >= Convert(DATETIME,@dDtInicial,103))  O' +
        'R (@dDtInicial            = '#39'01/01/1900'#39'))'
      
        '   AND ((dDtEmissao      < Convert(DATETIME,@dDtFinal,103)+1)  O' +
        'R (@dDtFinal              = '#39'01/01/1900'#39'))'
      
        '   AND ((iNrDocto       >= @nCdDoctoFiscalInicial)             O' +
        'R (@nCdDoctoFiscalInicial = 0))'
      
        '   AND ((iNrDocto       <= @nCdDoctoFiscalFinal)               O' +
        'R (@nCdDoctoFiscalFinal   = 0))'
      '   AND dDtImpressao      is not null'
      '   AND dDtCancel         is null'
      '   AND nCdEmpresa        = :nCdEmpresa'
      '   AND cFlgComplementar  = 0'
      '   AND EXISTS (SELECT 1 '
      '                 FROM TipoDoctoFiscal '
      
        '                WHERE nCdTipoDoctoFiscal    = DoctoFiscal.nCdTip' +
        'oDoctoFiscal'
      '                  AND cFlgAceitaComplemento = 1)')
    Left = 476
    Top = 232
    object qryNotasFiscaisnCdDoctoFiscal: TAutoIncField
      DisplayLabel = 'C'#243'd.'
      FieldName = 'nCdDoctoFiscal'
      ReadOnly = True
    end
    object qryNotasFiscaisnCdSerieFiscal: TIntegerField
      DisplayLabel = 'Nr. S'#233'rie Fiscal'
      FieldName = 'nCdSerieFiscal'
    end
    object qryNotasFiscaiscCFOP: TStringField
      DisplayLabel = 'CFOP'
      FieldName = 'cCFOP'
      FixedChar = True
      Size = 4
    end
    object qryNotasFiscaisiNrDocto: TIntegerField
      DisplayLabel = 'Nr. Docto'
      FieldName = 'iNrDocto'
    end
    object qryNotasFiscaisdDtEmissao: TDateTimeField
      DisplayLabel = 'Data Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryNotasFiscaisnCdTerceiro: TIntegerField
      DisplayLabel = 'C'#243'd. Terceiro'
      FieldName = 'nCdTerceiro'
    end
    object qryNotasFiscaiscNmTerceiro: TStringField
      DisplayLabel = 'Terceiro'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryNotasFiscaisnValTotal: TBCDField
      DisplayLabel = 'Valor Total'
      FieldName = 'nValTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object PopupMenu1: TPopupMenu
    Left = 516
    Top = 232
    object ExibirDocumentoFiscal1: TMenuItem
      Bitmap.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
        FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
        80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
        0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
        000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
        00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
        E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
        99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
        99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
        99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
        9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      Caption = 'Exibir Documento Fiscal'
      OnClick = ExibirDocumentoFiscal1Click
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '             ,cNmTerceiro'
      '   FROM Terceiro'
      ' WHERE nCdTerceiro =:nPK')
    Left = 640
    Top = 64
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 656
    Top = 96
  end
  object DataSource1: TDataSource
    DataSet = qryNotasFiscais
    Left = 488
    Top = 264
  end
end
