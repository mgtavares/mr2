unit rConsultasSCPC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, ImgList, ComCtrls, ToolWin, DBCtrls, StdCtrls, Mask, ADODB, cxGridDBTableView, cxGrid, cxGraphics, cxLookAndFeels, cxCurrencyEdit, cxPC,
  cxLookAndFeelPainters, cxButtons, DB, FileCtrl;

type
  TrptConsultasSCPC = class(TForm)
    ToolBar1: TToolBar;
    ToolButtonImp: TToolButton;
    ToolButtonFechar: TToolButton;
    ImageList1: TImageList;
    Image1: TImage;
    ToolButton3: TToolButton;
    ProgressBar: TProgressBar;
    MaskEditIni: TMaskEdit;
    MaskEditFim: TMaskEdit;
    EditLoja: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    qryLoja: TADOQuery;
    qrySP: TADOQuery;
    EditNmLoja: TEdit;
    SaveDialog1: TSaveDialog;
    procedure FormKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure ToolButtonFecharClick(Sender: TObject);
    procedure PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
    function MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
    procedure ShowMessage(cTexto: string);
    procedure MensagemErro(cTexto: string);
    procedure MensagemAlerta(cTexto: string);
    procedure FormActivate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDeactivate(Sender: TObject);
    procedure showForm(objForm: TForm; bDestroirObjeto : boolean);
    procedure desativaDBEdit (obj : TDBEdit) ;
    procedure ativaDBEdit (obj : TDBEdit) ;
    procedure desativaMaskEdit (obj : TMaskEdit);
    procedure ativaMaskEdit (obj : TMaskEdit);
    procedure ToolButtonImpClick(Sender: TObject);
    procedure EditLojaChange(Sender: TObject);
    procedure EditLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptConsultasSCPC: TrptConsultasSCPC;

implementation

uses fMenu, fCadastro_Template, fMensagem, fLookup_Padrao;

{$R *.dfm}

function TrptConsultasSCPC.MessageDLG(Msg: string; AType: TMsgDlgType; AButtons:TMsgDlgButtons; iHelp: Integer): Word;
var
    Mensagem: TForm;
    Portugues: Boolean;
begin

    frmMensagem.lblMensagem.Caption := Msg ;
    frmMensagem.AType               := AType ;
    Result := frmMensagem.ShowModal;
    exit ;

    Portugues := True ;
    Mensagem  := CreateMessageDialog(Msg, AType, Abuttons);

    Mensagem.Font.Name := 'Tahoma' ;
    Mensagem.Font.Size := 8 ;
    Mensagem.Font.Style := [fsBold] ;

    Mensagem.Ctl3D := True ;

    with Mensagem do
    begin

        if Portugues then
        begin

            if Atype = mtConfirmation then
            begin
                Caption := 'ER2Soft - Confirma��o'
            end
            else
            if AType = mtWarning then
            begin
                Caption := 'ER2Soft - Aviso' ;
                Mensagem.Color := clYellow ;
            end
            else if AType = mtError then
            begin
                Caption := 'ER2Soft - Erro' ;
                Mensagem.Color := clRed ;
            end
            else if AType = mtInformation then
                Caption := 'ER2Soft - Informa��o';

        end;

    end;

    if Portugues then
    begin
        TButton(Mensagem.FindComponent('YES')).Caption := '&Sim';
        TButton(Mensagem.FindComponent('NO')).Caption := '&N�o';
        TButton(Mensagem.FindComponent('CANCEL')).Caption := '&Cancelar';
        TButton(Mensagem.FindComponent('ABORT')).Caption := '&Abortar';
        TButton(Mensagem.FindComponent('RETRY')).Caption := '&Repetir';
        TButton(Mensagem.FindComponent('IGNORE')).Caption := '&Ignorar';
        TButton(Mensagem.FindComponent('ALL')).Caption := '&Todos';
        TButton(Mensagem.FindComponent('HELP')).Caption := 'A&juda';
    end;


    Result := Mensagem.ShowModal;

    Mensagem.Free;
end;


procedure TrptConsultasSCPC.ShowMessage(cTexto: string);
begin
    MessageDLG(cTexto,mtInformation,[mbOK],0) ;
    {frmMenu.nShowMessage(cTexto);}
end;


procedure TrptConsultasSCPC.MensagemErro(cTexto: string);
begin
    MessageDLG(cTexto,mtError,[mbOK],0) ;
    {frmMenu.nMensagemErro(cTexto);}
end;

procedure TrptConsultasSCPC.MensagemAlerta(cTexto: string);
begin
    MessageDLG(cTexto,mtWarning,[mbOK],0) ;
    {frmMenu.nMensagemAlerta(cTexto);}
end;

procedure TrptConsultasSCPC.PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
begin
    if (Trim(cValor) = '') then
        exit ;

    oQuery.Close ;
    oQuery.Parameters.ParamByName('nPK').Value := cValor ;
    oQuery.Open ;
end ;

procedure TrptConsultasSCPC.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
i:integer ;  
begin

  if key = 13 then
  begin

    key := 0;
    perform (WM_NextDlgCtl, 0, 0);

  end;
end;

procedure TrptConsultasSCPC.FormCreate(Sender: TObject);
var
    i : Integer;
    i2 : integer ;
begin

  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TcxPageControl) then
    begin

        (Components [I] as TcxPageControl).Font.Name  := 'Calibri' ;
        (Components [I] as TcxPageControl).Font.Size  := 9 ;
        (Components [I] as TcxPageControl).Font.Style := [] ;
        (Components [I] as TcxPageControl).LookAndFeel.NativeStyle := True ;

    end ;

    if (Components [I] is TGroupBox) then
    begin
       (Components [I] as TGroupBox).Font.Name    := 'Calibri' ;
       (Components [I] as TGroupBox).Font.Size    := 8 ;
       (Components [I] as TGroupBox).Font.Style   := [] ;
    end ;

    if (Components [I] is TCheckBox) then
    begin
       (Components [I] as TCheckBox).Font.Name    := 'Calibri' ;
       (Components [I] as TCheckBox).Font.Size    := 8 ;
       (Components [I] as TCheckBox).Font.Style   := [fsBold] ;
    end ;

    if (Components [I] is TDBCheckBox) then
    begin
       (Components [I] as TDBCheckBox).Font.Name    := 'Calibri' ;
       (Components [I] as TDBCheckBox).Font.Size    := 8 ;
       (Components [I] as TDBCheckBox).Font.Style   := [fsBold] ;
    end ;

    if (Components [I] is TADOStoredProc) then
    begin
        (Components [I] as TADOStoredProc).CommandTimeOut := 600 ;
    end ;

    if (Components [I] is TDBEdit) then
    begin
      //(Components [I] as TDBEdit).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TDBEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBEdit).CharCase    := ecUpperCase ;
      (Components [I] as TDBEdit).Color       := clWhite ;
      (Components [I] as TDBEdit).Height      := 11 ;
      (Components [I] as TDBEdit).BevelInner  := bvSpace ;
      (Components [I] as TDBEdit).BevelKind   := bkNone ;
      (Components [I] as TDBEdit).BevelOuter  := bvLowered ;
      (Components [I] as TDBEdit).Ctl3D       := true    ;
      (Components [I] as TDBEdit).BorderStyle := bsSingle ;
      (Components [I] as TDBEdit).Font.Name   := 'Calibri' ;
      (Components [I] as TDBEdit).Font.Size   := 9 ;
      (Components [I] as TDBEdit).Font.Style  := [] ;
      (Components [I] as TDBEdit).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TDBEdit).OnKeyPress  := frmMenu.NavegaEnter ;

      if ((Components [I] as TDBEdit).Tag = 1) then
      begin
          (Components [I] as TDBEdit).ReadOnly := True ;
          (Components [I] as TDBEdit).Color      := $00E9E4E4 ;
          (Components [I] as TDBEdit).Font.Color := clBlack ;
          (Components [I] as TDBEdit).TabStop  := False ;
          Update;
      end;

    end;

    if (Components [I] is TDBMemo) then
    begin
      (Components [I] as TDBMemo).OnEnter     := frmMenu.emFoco ;
      (Components [I] as TDBMemo).OnExit      := frmMenu.semFoco ;
      (Components [I] as TDBMemo).Color       := clWhite ;
      (Components [I] as TDBMemo).BevelInner  := bvSpace ;
      (Components [I] as TDBMemo).BevelKind   := bkNone ;
      (Components [I] as TDBMemo).BevelOuter  := bvLowered ;
      (Components [I] as TDBMemo).Ctl3D       := True    ;
      (Components [I] as TDBMemo).BorderStyle := bsSingle ;
      (Components [I] as TDBMemo).Font.Name   := 'Calibri' ;
      (Components [I] as TDBMemo).Font.Size   := 9 ;
      (Components [I] as TDBMemo).Font.Style  := [] ;
      (Components [I] as TDBMemo).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if ((Components [I] as TDBMemo).Tag = 1) then
      begin
          (Components [I] as TDBMemo).ParentFont := False ;
          (Components [I] as TDBMemo).Enabled := False ;
          (Components [I] as TDBMemo).Color   := clSilver ;
          (Components [I] as TDBMemo).Font.Color := clYellow ;
          Update;
      end ;

      if ((Components [I] as TDBEdit).Tag = 0) then
      begin
          if (Copy((Components [I] as TDBEdit).DataField,1,3) = 'dDt') then
             (Components [I] as TDBEdit).Width := 76 ;
      end ;

    end;

    if (Components [I] is TEdit) then
    begin
      //(Components [I] as TEdit).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TEdit).CharCase    := ecUpperCase ;
      (Components [I] as TEdit).Color       := clWhite ;
      (Components [I] as TEdit).BevelInner  := bvSpace ;
      (Components [I] as TEdit).BevelKind   := bkNone ;
      (Components [I] as TEdit).BevelOuter  := bvLowered ;
      (Components [I] as TEdit).Ctl3D       := True    ;
      (Components [I] as TEdit).BorderStyle := bsSingle ;
      (Components [I] as TEdit).Height      := 15 ;
      (Components [I] as TEdit).Font.Name   := 'Calibri' ;
      (Components [I] as TEdit).Font.Size   := 9 ;
      (Components [I] as TEdit).Font.Style  := [] ;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TEdit).OnKeyPress  := frmMenu.NavegaEnter ;

    end;

    if (Components [I] is TMaskEdit) then
    begin
      //(Components [I] as TMaskEdit).OnEnter     := frmMenu.emFoco ;
      //(Components [I] as TMaskEdit).OnExit      := frmMenu.semFoco ;
      (Components [I] as TMaskEdit).CharCase    := ecUpperCase ;
      (Components [I] as TMaskEdit).Color       := clWhite ;
      (Components [I] as TMaskEdit).BevelInner  := bvSpace ;
      (Components [I] as TMaskEdit).BevelKind   := bkNone ;
      (Components [I] as TMaskEdit).BevelOuter  := bvLowered ;
      (Components [I] as TMaskEdit).Ctl3D       := True    ;
      (Components [I] as TMaskEdit).BorderStyle := bsSingle ;
      (Components [I] as TMaskEdit).Height      := 15 ;
      (Components [I] as TMaskEdit).Font.Name   := 'Calibri' ;
      (Components [I] as TMaskEdit).Font.Size   := 9 ;
      (Components [I] as TMaskEdit).Font.Style  := [] ;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TMaskEdit).OnKeyPress  := frmMenu.NavegaEnter ;

      if Trim((Components[I] as TMaskEdit).EditMask) = '######;1;' then
          (Components[I] as TMaskEdit).EditMask := '##########;1; ' ;
          
      if (Components[I] as TMaskEdit).EditMask = '!99/99/9999;1;_' then
          (Components[I] as TMaskEdit).Width := 76 ;

      if ((Components [I] as TMaskEdit).Tag = 1) then
      begin
          (Components [I] as TMaskEdit).ReadOnly := True ;
          (Components [I] as TMaskEdit).Color      := $00E9E4E4 ;
          (Components [I] as TMaskEdit).Font.Color := clBlack ;
          (Components [I] as TMaskEdit).TabStop  := False ;
          Update;
      end;


    end;

    if (Components [I] is TLabel) then
    begin
       (Components [I] as TLabel).Font.Name    := 'Tahoma' ;
       (Components [I] as TLabel).Font.Size    := 8 ;
       (Components [I] as TLabel).Font.Style   := [] ;
       (Components [I] as TLabel).Font.Color   := clWhite ;
       (Components [I] as TLabel).Transparent  := True ;
       (Components [I] as TLabel).BringToFront;
    end ;

    if (Components [I] is TButton) then
    begin
       (Components [I] as TButton).ParentFont := False ;
       (Components [I] as TButton).Font.Name  := 'Calibri' ;
       (Components [I] as TButton).Font.Size  := 8 ;
       (Components [I] as TButton).Font.Color := clBlue ;
       (Components [I] as TButton).Default    := False ;
       (Components [I] as TButton).Update ;
    end ;

    If (Components [I] is TDBCheckBox) then
      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
        (Components [I] as TDBCheckBox).OnKeyPress  := frmMenu.NavegaEnter ;

    If (Components [I] is TcxGridDBTableView) then
    begin
        (Components [I] as TcxGridDBTableView).OptionsView.GridLineColor := clAqua ;
        (Components [I] as TcxGridDBTableView).OptionsView.GridLines     := glVertical ;
        (Components [I] as TcxGridDBTableView).Styles.Header             := frmMenu.Header ;
    end ;

    If (Components [I] is TcxGrid) then
    begin
        (Components [I] as TcxGrid).LookAndFeel.Kind        := lfFlat ;
        (Components [I] as TcxGrid).LookAndFeel.NativeStyle := True ;
        (Components [I] as TcxGrid).Font.Name               := 'Calibri' ;
    end ;

    if (Components [I] is TDBLookupComboBox) then
    begin
      (Components [I] as TDBLookupComboBox).OnKeyUp     := frmCadastro_Padrao.OnKeyUp;

      if (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2SOFT.EXE') or (Uppercase(ExtractFileName(Application.ExeName)) = 'ER2WERP.EXE') then
          (Components [I] as TDBLookupComboBox).OnKeyPress  := frmMenu.NavegaEnter ;

      (Components [I] as TDBLookupComboBox).Height      := 12 ;
      (Components [I] as TDBLookupComboBox).BevelInner  := bvSpace ;
      (Components [I] as TDBLookupComboBox).BevelKind   := bkNone ;
      (Components [I] as TDBLookupComboBox).BevelOuter  := bvLowered ;
      (Components [I] as TDBLookupComboBox).Ctl3D       := true    ;
    end;

    if (Components [I] is TcxCurrencyEdit) then
    begin
        (Components [I] as TcxCurrencyEdit).Style.Font.Name := 'Calibri' ;
        (Components [I] as TcxCurrencyEdit).Style.Font.Size := 9 ;
    end ;
        
  end ;

end;

procedure TrptConsultasSCPC.ToolButtonFecharClick(Sender: TObject);
begin
    Close ;
end;



procedure TrptConsultasSCPC.FormActivate(Sender: TObject);
begin
    ToolBar1.Enabled := True ;
    frmMenu.StatusBar1.Panels[5].Text := Self.ClassName ;
end;

procedure TrptConsultasSCPC.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    frmMenu.StatusBar1.Panels[5].Text := '' ;
end;

procedure TrptConsultasSCPC.FormDeactivate(Sender: TObject);
begin
    ToolBar1.Enabled := False ;
end;

procedure TrptConsultasSCPC.showForm(objForm: TForm;
  bDestroirObjeto: boolean);
begin

    frmMenu.showForm(objForm , bDestroirObjeto);

end;

procedure TrptConsultasSCPC.ativaDBEdit(obj: TDBEdit);
begin

    obj.ReadOnly   := False ;
    obj.Color      := clWhite ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := True ;

end;

procedure TrptConsultasSCPC.desativaDBEdit(obj: TDBEdit);
begin

    obj.ReadOnly   := True ;
    obj.Color      := $00E9E4E4 ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := False ;

end;

procedure TrptConsultasSCPC.ativaMaskEdit(obj: TMaskEdit);
begin

    obj.ReadOnly   := False ;
    obj.Color      := clWhite ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := True ;

end;

procedure TrptConsultasSCPC.desativaMaskEdit(obj: TMaskEdit);
begin

    obj.ReadOnly   := True ;
    obj.Color      := $00E9E4E4 ;
    obj.Font.Color := clBlack ;
    obj.TabStop    := False ;

end;

procedure TrptConsultasSCPC.EditLojaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

          nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
            EditLoja.Text := IntToStr(nPK);

    end ;

  end ;

end;

procedure TrptConsultasSCPC.ToolButtonImpClick(Sender: TObject);
var
  loja : integer;
  de, ate, path, arq, linha : String;
  F : TextFile;
begin
   if SaveDialog1.Execute then
   begin
   ProgressBar.Max := 100;
   ProgressBar.Position := 20;
   try
   if EditLoja.Text = '' then loja := 0 else loja := StrToInt(EditLoja.Text);
   if MaskEditIni.Text = '  /  /    ' then MaskEditIni.Text := DateToStr(Date-31);
   de := MaskEditIni.Text;
   if MaskEditFim.Text = '  /  /    ' then MaskEditFim.Text := DateToStr(Date-1);
   ate := MaskEditFim.Text;
   qrySP.Close;
   qrySP.SQL.Clear;
   qrySP.SQL.Add('EXEC SPREL_CONSULTAS_SCPC @loja = ' + IntToStr(loja) + ' ,@ini = ' + chr(39) + de + chr(39) + ' ,@fim = ' + chr(39) + ate + chr(39));
   qrySP.Open;
   ProgressBar.Max := qrySP.RecordCount;
   ProgressBar.Position := Round(qrySP.RecordCount * 0.2);
   AssignFile(F, SaveDialog1.FileName);
   ReWrite(F);
   Writeln(F, 'C�d. Cliente;CPF;Loja Consulta;Data Consulta;Resposta;');
   qrySP.First;
   While not qrySP.Eof do
    begin
      linha := qrySP.FieldByName('C�d. Cliente').Text + ';' +
               qrySP.FieldByName('CPF').Text + ';' +
               qrySP.FieldByName('Loja Consulta').Text + ';' +
               qrySP.FieldByName('Data Consulta').Text + ';' +
               qrySP.FieldByName('Resposta').Text + ';';
      Writeln(F, linha);
      qrySP.Next;
      ProgressBar.Position := ProgressBar.Position + 1;
    end;
      CloseFile(F);
      ShowMessage('O Relat�rio foi criado com sucesso no diret�rio escolhido!');
      ProgressBar.Position := 0;
   except
      ShowMessage('Algo deu errado :( Tente novamente. Se o erro persistir, contate o desenvolvedor.');
      ProgressBar.Position := 0;
   end;
   end;
end;

procedure TrptConsultasSCPC.EditLojaChange(Sender: TObject);
begin
  EditNmLoja.Text := '';
  if EditLoja.Text <> '' then
  begin
    try
      StrToInt(EditLoja.Text);
      qryLoja.Close;
      qryLoja.SQL.Clear;
      qryLoja.SQL.Add('SELECT TOP 1 * FROM Loja WHERE nCdLoja = ' + EditLoja.Text);
      qryLoja.Open;
      EditNmLoja.Text := qryLoja.FieldByName('cNmLoja').Text;
    except
     ShowMessage('C�digo da loja inv�lido!');
    end;
  end;
end;

initialization
  RegisterClass(TrptConsultasSCPC);

end.
