inherited frmMoeda: TfrmMoeda
  Caption = 'Moeda'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 35
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 9
    Top = 70
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sigla Moeda'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 24
    Top = 94
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit3
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 77
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdMoeda'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 77
    Top = 64
    Width = 89
    Height = 19
    DataField = 'cSigla'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [7]
    Left = 77
    Top = 88
    Width = 650
    Height = 19
    DataField = 'cNmMoeda'
    DataSource = dsMaster
    TabOrder = 3
  end
  object cxPageControl1: TcxPageControl [8]
    Left = 16
    Top = 128
    Width = 713
    Height = 473
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 4
    ClientRectBottom = 469
    ClientRectLeft = 4
    ClientRectRight = 709
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Hist'#243'rico de Cota'#231#227'o'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 705
        Height = 445
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsCotacaoMoeda
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghDialogFind, dghColumnResize, dghColumnMove]
        ReadOnly = True
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdMoeda'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtCotacao'
            Footers = <>
          end
          item
            DisplayFormat = '#,##0.00'
            EditButtons = <>
            FieldName = 'nValCotacao'
            Footers = <>
            Width = 115
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM MOEDA'
      'WHERE nCdMoeda = :nPK')
    object qryMasternCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMastercSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 10
    end
    object qryMastercNmMoeda: TStringField
      FieldName = 'cNmMoeda'
      Size = 50
    end
  end
  object qryCotacaoMoeda: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCotacaoMoedaBeforePost
    Parameters = <
      item
        Name = 'nCdMoeda'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM COTACAOMOEDA'
      'WHERE nCdMoeda = :nCdMoeda'
      'ORDER BY dDtCotacao DESC')
    Left = 944
    Top = 416
    object qryCotacaoMoedanCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryCotacaoMoedadDtCotacao: TDateTimeField
      DisplayLabel = 'Data Cota'#231#227'o'
      DisplayWidth = 18
      FieldName = 'dDtCotacao'
      Required = True
    end
    object qryCotacaoMoedanValCotacao: TBCDField
      DisplayLabel = 'Valor Cota'#231#227'o'
      FieldName = 'nValCotacao'
      Required = True
      Precision = 12
    end
  end
  object dsCotacaoMoeda: TDataSource
    DataSet = qryCotacaoMoeda
    Left = 984
    Top = 416
  end
end
