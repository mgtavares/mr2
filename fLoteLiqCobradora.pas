unit fLoteLiqCobradora;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, DBGridEhGrouping, ToolCtrlsEh,
  ER2Lookup;

type
  TfrmLoteLiqCobradora = class(TfrmCadastro_Padrao)
    qryMasternCdLoteLiqCobradora: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdCobradora: TIntegerField;
    qryMastercNrBorderoCob: TStringField;
    qryMasternCdLojaCobradora: TIntegerField;
    qryMasternValTotal: TBCDField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasternCdUsuarioFecham: TIntegerField;
    qryMasternCdUsuarioCancel: TIntegerField;
    qryMasterdDtCad: TDateTimeField;
    qryMasterdDtFecham: TDateTimeField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternCdContaBancaria: TIntegerField;
    qryMasternCdUsuarioFinanc: TIntegerField;
    qryMasterdDtFinanc: TDateTimeField;
    qryMastercOBS: TMemoField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label16: TLabel;
    DBMemo1: TDBMemo;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit16: TDBEdit;
    DataSource1: TDataSource;
    qryLojaCobradora: TADOQuery;
    qryLojaCobradoranCdLoja: TIntegerField;
    qryLojaCobradoracNmLoja: TStringField;
    DBEdit17: TDBEdit;
    DataSource2: TDataSource;
    qryCobradora: TADOQuery;
    qryCobradoranCdCobradora: TIntegerField;
    qryCobradoracNmCobradora: TStringField;
    DBEdit18: TDBEdit;
    DataSource3: TDataSource;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    DBEdit19: TDBEdit;
    DataSource4: TDataSource;
    qryUsuarioCad: TADOQuery;
    qryUsuarioCadcNmUsuario: TStringField;
    DBEdit20: TDBEdit;
    DataSource5: TDataSource;
    qryUsuarioFech: TADOQuery;
    qryUsuarioFechcNmUsuario: TStringField;
    DBEdit21: TDBEdit;
    DataSource6: TDataSource;
    qryUsuarioCancel: TADOQuery;
    qryUsuarioCancelcNmUsuario: TStringField;
    DBEdit22: TDBEdit;
    DataSource7: TDataSource;
    qryUsuarioFinanc: TADOQuery;
    qryUsuarioFinanccNmUsuario: TStringField;
    DBEdit23: TDBEdit;
    DataSource8: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryItemLote: TADOQuery;
    dsItemLote: TDataSource;
    Panel1: TPanel;
    addBaixa: TcxButton;
    cxButton2: TcxButton;
    qryItemLoteLiqCobradora: TADOQuery;
    qryItemLoteLiqCobradoranCdItemLoteLiqCobradora: TAutoIncField;
    qryItemLoteLiqCobradoranCdLoteLiqCobradora: TIntegerField;
    qryItemLoteLiqCobradoranCdTerceiro: TIntegerField;
    qryItemLoteLiqCobradoracFlgTipoLiq: TStringField;
    qryItemLoteLiqCobradoranValLiq: TBCDField;
    qryItemLotenCdItemLoteLiqCobradora: TAutoIncField;
    qryItemLotenCdLoteLiqCobradora: TIntegerField;
    qryItemLotenCdTerceiro: TIntegerField;
    qryItemLotecCNPJCPF: TStringField;
    qryItemLotecNmTerceiro: TStringField;
    qryItemLotecFlgTipoLiq: TStringField;
    qryItemLotenValLiq: TBCDField;
    qryItemLotecFlgDiverg: TIntegerField;
    qryItemLoteLiqCobradoracFlgDiverg: TIntegerField;
    qrySomaTitCobradora: TADOQuery;
    qrySomaTitCobradoranSaldoTit: TBCDField;
    qryAux: TADOQuery;
    qryTitulosBaixa: TADOQuery;
    qryTitulosBaixanCdTitulo: TIntegerField;
    qryTitulosBaixanSaldoTit: TBCDField;
    qryTituloLoteLiqCobradora: TADOQuery;
    qryTituloLoteLiqCobradoranCdTituloLoteLiqCobradora: TAutoIncField;
    qryTituloLoteLiqCobradoranCdItemLoteLiqCobradora: TIntegerField;
    qryTituloLoteLiqCobradoranCdTitulo: TIntegerField;
    qryTituloLoteLiqCobradoracFlgTipoLiq: TStringField;
    qryTituloLoteLiqCobradoranSaldoAnterior: TBCDField;
    qryTituloLoteLiqCobradoranValLiq: TBCDField;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    ToolButton12: TToolButton;
    SP_FINALIZA_LOTELIQ_COBRADORA: TADOStoredProc;
    qryItemLotenSaldoTerceiro: TBCDField;
    qryItemLoteLiqCobradoranSaldoTerceiro: TBCDField;
    btnCancelar: TToolButton;
    SP_CANCELA_LOTE_LIQ: TADOStoredProc;
    DBEdit9: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure addBaixaClick(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure ToolButton12Click(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLoteLiqCobradora: TfrmLoteLiqCobradora;

implementation

uses fMenu, fLookup_Padrao, fLoteLiqCobradora_Cliente,
  fLoteLiqCobradora_Titulos, rLoteLiqCobradora_view;

{$R *.dfm}

procedure TfrmLoteLiqCobradora.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'LOTELIQCOBRADORA' ;
  nCdTabelaSistema  := 93 ;
  nCdConsultaPadrao := 201 ;
  bLimpaAposSalvar  := False ;

end;

procedure TfrmLoteLiqCobradora.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      qryMasternCdLojaCobradora.Value := frmMenu.nCdLojaAtiva;
      PosicionaQuery(qryLojaCobradora, qryMasternCdLojaCobradora.AsString) ;
  end ;

  DBEdit3.SetFocus;
end;

procedure TfrmLoteLiqCobradora.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdUsuarioCad.Value := frmMenu.nCdUsuarioLogado;
      qryMasterdDtCad.Value        := Date ;

      PosicionaQuery(qryUsuarioCad, qryMasternCdUsuarioCad.AsString) ;
  end ;

  if (DBEdit18.Text = '') then
  begin
      MensagemAlerta('Selecione a cobradora.') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (DBEdit4.Text = '') then
  begin
      MensagemAlerta('Informe o n�mero do border�.') ;
      DBEdit4.SetFocus;
      abort ;
  end ;

  inherited;

end;

procedure TfrmLoteLiqCobradora.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryCobradora.Close;
  PosicionaQuery(qryCobradora, DBEdit3.Text) ;
  
end;

procedure TfrmLoteLiqCobradora.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(199);

            If (nPK > 0) then
            begin
                qryMasternCdCobradora.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmLoteLiqCobradora.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmLoteLiqCobradora.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  qryLojaCobradora.Close;
  qryCobradora.Close;
  qryUsuarioCad.Close;
  qryUsuarioFech.Close;
  qryUsuarioCancel.Close;
  qryUsuarioFinanc.Close;
  qryContaBancaria.Close;
  qryItemLote.Close;

  Posicionaquery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  Posicionaquery(qryLojaCobradora, qryMasternCdLojaCobradora.AsString) ;
  Posicionaquery(qryCobradora, qryMasternCdCobradora.AsString) ;
  Posicionaquery(qryUsuarioCad, qryMasternCdUsuarioCad.AsString) ;
  Posicionaquery(qryUsuarioFech, qryMasternCdUsuarioFecham.AsString) ;
  Posicionaquery(qryUsuarioCancel, qryMasternCdUsuariocancel.AsString) ;
  Posicionaquery(qryUsuarioFinanc, qryMasternCdUsuarioFinanc.AsString) ;
  PosicionaQuery(qryContaBancaria, qryMasternCdContaBancaria.AsString) ;
  Posicionaquery(qryItemLote, qryMasternCdLoteLiqCobradora.AsString) ;

end;

procedure TfrmLoteLiqCobradora.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qryEmpresa.Close;
  qryLojaCobradora.Close;
  qryCobradora.Close;
  qryUsuarioCad.Close;
  qryUsuarioFech.Close;
  qryUsuarioCancel.Close;
  qryUsuarioFinanc.Close;
  qryContaBancaria.Close;
  qryItemLote.Close;
  
end;

procedure TfrmLoteLiqCobradora.addBaixaClick(Sender: TObject);
var
    nValLiq, nValResid : double ;
    iTotalTitulos, iTitulosBaixados : integer ;
    objForm : TfrmLoteLiqCobradora_Cliente ;
begin

  if (not qryMaster.Active) then
      exit ;

  if (qryMasternCdUsuarioFecham.Value > 0) then
  begin
      MensagemAlerta('Lote j� foi finalizado e n�o pode ser alterado.') ;
      exit ;
  end ;

  if (qryMasternCdUsuarioCancel.Value > 0) then
  begin
      MensagemAlerta('Lote j� foi cancelado e n�o pode ser alterado.') ;
      exit ;
  end ;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

  objForm := TfrmLoteLiqCobradora_Cliente.Create(nil) ;

  objForm.nCdCobradora        := qryMasternCdCobradora.Value;
  objForm.nCdLoteLiqCobradora := qryMasternCdLoteLiqCobradora.Value;

  showForm ( objForm , FALSE ) ;

  if not (objForm.bValidado) then
      abort;

  {-- Verifica se existe titulos do cliente na cobradora --}
  qrySomaTitCobradora.Close;
  qrySomaTitCobradora.Parameters.ParamByName('nCdTerceiro').Value  := objForm.qryClientenCdTerceiro.Value;
  qrySomaTitCobradora.Parameters.ParamByName('nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva;
  qrySomaTitCobradora.Parameters.ParamByName('nCdCobradora').Value := qryMasternCdCobradora.Value;
  qrySomaTitCobradora.Open;

  if (qrySomaTitCobradoranSaldoTit.Value <= 0) then
  begin
      MensagemAlerta('Nenhum t�tulo na cobradora para o Cliente: ' + objForm.qryClientecNmTerceiro.Value);
      abort;
  end;

  {-- Gera o registro da baixa do cliente --}
  if (objForm.bValidado) then
  begin
      if not qryItemLoteLiqCobradora.Active then
      begin
          qryItemLoteLiqCobradora.Close;
          qryItemLoteLiqCobradora.Open;
      end ;

      frmMenu.Connection.BeginTrans;

      try
          qryItemLoteLiqCobradora.Insert;
          qryItemLoteLiqCobradoranCdItemLoteLiqCobradora.Value := frmMenu.fnProximoCodigo('ITEMLOTELIQCOBRADORA') ;
          qryItemLoteLiqCobradoranCdLoteLiqCobradora.Value     := qryMasternCdLoteLiqCobradora.Value ;
          qryItemLoteLiqCobradoranCdTerceiro.Value             := objForm.qryClientenCdTerceiro.Value;

          if (objForm.cmbTipoBaixa.Text = 'PARCIAL') then
              qryItemLoteLiqCobradoracFlgTipoLiq.Value := 'P'
          else qryItemLoteLiqCobradoracFlgTipoLiq.Value := 'T' ;

          qryItemLoteLiqCobradoranValLiq.Value := objForm.edtValorBaixa.Value ;

          qrySomaTitCobradora.Close;
          qrySomaTitCobradora.Parameters.ParamByName('nCdTerceiro').Value  := objForm.qryClientenCdTerceiro.Value;
          qrySomaTitCobradora.Parameters.ParamByName('nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva;
          qrySomaTitCobradora.Parameters.ParamByName('nCdCobradora').Value := qryMasternCdCobradora.Value;
          qrySomaTitCobradora.Open;

          if (qrySomaTitCobradoranSaldoTit.Value > qryItemLoteLiqCobradoranValLiq.Value) then
              qryItemLoteLiqCobradoranSaldoTerceiro.Value := (qrySomaTitCobradoranSaldoTit.Value - qryItemLoteLiqCobradoranValLiq.Value)
          else qryItemLoteLiqCobradoranSaldoTerceiro.Value := 0 ;

          {-- quando for baixa total, verifica se o montante enviado pela cobradora � igual ou maior que o saldo em aberto do cliente --}
          if (qryItemLoteLiqCobradoracFlgTipoLiq.Value = 'T') then
          begin
              {-- gera divergencia --}
              if qrySomaTitCobradora.Eof or (qrySomaTitCobradoranSaldoTit.Value > qryItemLoteLiqCobradoranValLiq.Value) then
                  qryItemLoteLiqCobradoracFlgDiverg.Value := 1 ;
          end ;

          qryItemLoteLiqCobradora.Post;

          qryTitulosBaixa.Close;
          qryTitulosBaixa.Parameters.ParamByName('nCdTerceiro').Value  := objForm.qryClientenCdTerceiro.Value;
          qryTitulosBaixa.Parameters.ParamByName('nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva;
          qryTitulosBaixa.Parameters.ParamByName('nCdCobradora').Value := qryMasternCdCobradora.Value;
          qryTitulosBaixa.Open;

          qryTitulosBaixa.First ;

          {-- vincula os titulos ao lote --}
          qryTituloLoteLiqCobradora.Close;
          qryTituloLoteLiqCobradora.Open;

          iTotalTitulos    := qryTitulosBaixa.RecordCount;
          iTitulosBaixados := 0 ;
          nValResid        := qryItemLoteLiqCobradoranValLiq.Value ;

          while not qryTitulosBaixa.Eof do
          begin
              nValLiq          := 0 ;
              iTitulosBaixados := iTitulosBaixados + 1 ;

              if (nValResid > 0) then
              begin
                  nValLiq := qryTitulosBaixanSaldoTit.Value ;

                  {-- se o saldo do titulo for maior que o saldo residual do lote, baixa parcial --}
                  if (nValLiq > nValResid) then
                  begin
                      nValLiq   := nValResid ;
                      nValResid := 0 ;
                  end
                  else
                  begin

                      {-- se o t�tulo for o �ltimo titulo em aberto do cliente, e o saldo residual              --}
                      {-- for maior que o saldo do t�tulo, considera o saldo residual como valor pago do titulo --}

                      if (iTitulosBaixados < iTotalTitulos) then
                          nValResid := nValResid - nValLiq
                      else
                      begin
                          nValLiq   := nValResid ;
                          nValResid := 0 ;
                      end ;

                  end ;

              end ;

              {-- quando for baixa total inseri todos os titulos do cliente, quando for parcial, inseri somente se for receber baixa --}
              if (qryItemLoteLiqCobradoracFlgTipoLiq.Value = 'T') or (nValLiq > 0) then
              begin
                  qryTituloLoteLiqCobradora.Insert;
                  qryTituloLoteLiqCobradoranCdTituloLoteLiqCobradora.Value := frmMenu.fnProximoCodigo('TITULOLOTELIQCOBRADORA') ;
                  qryTituloLoteLiqCobradoranCdItemLoteLiqCobradora.Value   := qryItemLoteLiqCobradoranCdItemLoteLiqCobradora.Value ;
                  qryTituloLoteLiqCobradoranCdTitulo.Value                 := qryTitulosBaixanCdTitulo.Value ;
                  qryTituloLoteLiqCobradoracFlgTipoLiq.Value               := qryItemLoteLiqCobradoracFlgTipoLiq.Value;
                  qryTituloLoteLiqCobradoranSaldoAnterior.Value            := qryTitulosBaixanSaldoTit.Value;
                  qryTituloLoteLiqCobradoranValLiq.Value                   := nValLiq ;
                  qryTituloLoteLiqCobradora.Post;
              end ;

              qryTitulosBaixa.Next ;

          end ;

          qryTitulosBaixa.Close;
          qryTituloLoteLiqCobradora.Close;
          qrySomaTitCobradora.Close;

          qryMaster.Edit ;
          qryMasternValTotal.Value := qryMasternValTotal.Value + qryItemLoteLiqCobradoranValLiq.Value ;
          qryMaster.Post ;

      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      Posicionaquery(qryItemLote, qryMasternCdLoteLiqCobradora.AsString) ;

      addBaixa.SetFocus;
      
  end ;

  FreeAndNil(objForm) ;

end;

procedure TfrmLoteLiqCobradora.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post ;

end;

procedure TfrmLoteLiqCobradora.cxButton2Click(Sender: TObject);
begin
    if (qryMaster.Active) and (qryMasternCdUsuarioFecham.Value > 0) then
    begin
        MensagemAlerta('Lote j� foi finalizado e n�o pode ser alterado.') ;
        exit ;
    end ;

    if (qryMasternCdUsuarioCancel.Value > 0) then
    begin
        MensagemAlerta('Lote j� foi cancelado e n�o pode ser alterado.') ;
        exit ;
    end ;

    if (qryItemLote.Active) and (qryItemLote.RecordCount > 0) then
    begin

        if (MessageDlg('Confirma a exclus�o do cliente deste lote ?' + #13#13 + 'Cliente : ' + qryItemLotecNmTerceiro.Value,mtConfirmation,[mbYes,mbNo],0)=MrNo) then
            exit ;

        try

            qryMaster.Edit ;
            qryMasternValTotal.Value := qryMasternValTotal.Value - qryItemLotenValLiq.Value ;
            qryMaster.Post ;

            qryAux.Close;
            qryAux.SQL.Clear;
            qryAux.SQL.Add('DELETE FROM TituloLoteLiqCobradora WHERE nCdItemLoteLiqCobradora = ' + qryItemLotenCdItemLoteLiqCobradora.AsString) ;
            qryAux.ExecSQL;

            qryAux.Close;
            qryAux.SQL.Clear;
            qryAux.SQL.Add('DELETE FROM ItemLoteLiqCobradora WHERE nCdItemLoteLiqCobradora = ' + qryItemLotenCdItemLoteLiqCobradora.AsString) ;
            qryAux.ExecSQL;

        except
            MensagemErro('Erro no processamento.');
            raise ;
        end ;

        Posicionaquery(qryItemLote, qryMasternCdLoteLiqCobradora.AsString) ;

    end ;
end;

procedure TfrmLoteLiqCobradora.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmLoteLiqCobradora_Titulos ;
begin
  inherited;

  if (qryItemLote.Active) and (qryItemLote.RecordCount > 0) then
  begin

      objForm := TfrmLoteLiqCobradora_Titulos.Create(nil) ;

      PosicionaQuery(objForm.qryTitulos, qryItemLotenCdItemLoteLiqCobradora.AsString) ;

      showForm( objForm , TRUE) ;

  end ;
end;

procedure TfrmLoteLiqCobradora.ToolButton12Click(Sender: TObject);
var
  objRel : TrptLoteLiqCobradora_view ;
begin
  inherited;

  if (qryMaster.Active) then
  begin
      if (qryMasternCdUsuarioFecham.Value = 0) then
      begin
          MensagemAlerta('Finalize o lote antes de imprimir o protocolo.') ;
          exit ;
      end ;

      objRel := TrptLoteLiqCobradora_view.Create(nil) ;

      try

          try
              PosicionaQuery(objRel.qryLoteLiqCobradora, qryMasternCdLoteLiqCobradora.AsString) ;
              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              {--visualiza o relat�rio--}

              objRel.QuickRep1.PreviewModal;

          except
              MensagemErro('Erro na cria��o do relat�rio');
              raise;
          end;

      finally
          FreeAndNil(objRel);
      end;

  end ;

end;

procedure TfrmLoteLiqCobradora.ToolButton10Click(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit ;

  if (qryMaster.Active) and (qryMasternCdUsuarioCancel.Value > 0) then
  begin
      MensagemAlerta('Lote j� foi cancelado e n�o pode ser alterado.');

      Exit;
  end

  else if (qryMaster.Active) and (qryMasternCdUsuarioFecham.Value > 0) then
  begin
      MensagemAlerta('Lote j� foi finalizado e n�o pode ser alterado.');

      Exit;
  end

  else if (qryItemLote.IsEmpty) then
  begin
      MensagemAlerta('Nenhuma baixa registrada. Lote n�o pode ser finalizado.');

      Exit;
  end;

  if (MessageDlg('Confirma a finaliza��o deste lote ?' +#13#13 + 'Este lote n�o poder� ser alterado.' ,mtConfirmation,[mbYes,mbNo],0)=MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      SP_FINALIZA_LOTELIQ_COBRADORA.Close;
      SP_FINALIZA_LOTELIQ_COBRADORA.Parameters.ParamByName('@nCdLoteLiqCobradora').Value := qryMasternCdLoteLiqCobradora.Value ;
      SP_FINALIZA_LOTELIQ_COBRADORA.Parameters.ParamByName('@nCdUsuario').Value          := frmMenu.nCdUsuarioLogado ;
      SP_FINALIZA_LOTELIQ_COBRADORA.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  PosicionaQuery(qryMaster, qryMasternCdLoteLiqCobradora.AsString) ;

  if (MessageDlg('Lote processado com sucesso. Deseja emitir o protocolo ?',mtConfirmation,[mbYes,mbNo],0)=MrYes) then
      ToolButton12.Click;

  btCancelar.Click;

end;

procedure TfrmLoteLiqCobradora.btSalvarClick(Sender: TObject);
begin
  if (qryMaster.Active) and (qryMasternCdUsuarioFecham.Value > 0) then
  begin
      MensagemAlerta('Lote j� foi finalizado e n�o pode ser alterado.') ;
      exit ;
  end ;

  inherited;

end;

procedure TfrmLoteLiqCobradora.btExcluirClick(Sender: TObject);
begin
  if (qryMaster.Active) and (qryMasternCdUsuarioFecham.Value > 0) then
  begin
      MensagemAlerta('Lote j� foi finalizado e n�o pode ser alterado.') ;
      exit ;
  end ;

  inherited;

end;

procedure TfrmLoteLiqCobradora.btnCancelarClick(Sender: TObject);
begin
  inherited;

  if ((not (qryMaster.IsEmpty)) and (qryMaster.State <> dsInsert)) then
      if ((qryMasternCdUsuarioFecham.IsNull) and (qryMasternCdUsuarioCancel.IsNull)) then
      begin
          qryMasternCdUsuarioCancel.Value := frmMenu.nCdUsuarioLogado;
          qryMasterdDtCancel.Value        := Now;
          qryMaster.Post;

          ShowMessage('Lote cancelado com sucesso.');

          qryMaster.Requery();
      end

      else if ((qryMasternCdUsuarioFinanc.IsNull) and (qryMasternCdUsuarioCancel.IsNull)) then
      begin
          if (MessageDLG('Confirma o cancelamento do lote?', mtConfirmation, [mbYes, mbNo], 0) = mrYes) then
          begin

              frmMenu.Connection.BeginTrans;

              try
                  SP_CANCELA_LOTE_LIQ.Close;
                  SP_CANCELA_LOTE_LIQ.Parameters.ParamByName('@nCdLoteLiqCobradora').Value := qryMasternCdLoteLiqCobradora.Value;
                  SP_CANCELA_LOTE_LIQ.Parameters.ParamByName('@nCdUsuarioCancel').Value    := frmMenu.nCdUsuarioLogado;
                  SP_CANCELA_LOTE_LIQ.ExecProc;
              except
                  frmMenu.Connection.RollbackTrans;

                  MensagemErro('Erro no processamento.');

                  Raise;
              end;

              frmMenu.Connection.CommitTrans;

              ShowMessage('Lote cancelado com sucesso.');

              qryMaster.Requery();
          end
      end

      else if not (qryMasternCdUsuarioFinanc.IsNull) then
          MensagemAlerta('Lote j� conciliado. N�o � possivel cancelar.')

      else if not (qryMasternCdUsuarioCancel.IsNull) then
          MensagemAlerta('Lote j� cancelado.');
end;

initialization
    RegisterClass(TfrmLoteLiqCobradora) ;

end.
