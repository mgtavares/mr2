unit fContratoImobiliario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons, Menus, DBGridEhGrouping;

type
  TfrmContratoImobiliario = class(TfrmCadastro_Padrao)
    qryMasternCdContratoEmpImobiliario: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNumContrato: TStringField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdBlocoEmpImobiliario: TIntegerField;
    qryMastercNrUnidade: TStringField;
    qryMasterdDtContrato: TDateTimeField;
    qryMasterdDtChaves: TDateTimeField;
    qryMasterdDtQuitacao: TIntegerField;
    qryMasternValContrato: TBCDField;
    qryMasteriParcelas: TIntegerField;
    qryMasteriParcelaInicial: TIntegerField;
    qryMasternValParcela: TBCDField;
    qryMasteriParcelaChaves: TIntegerField;
    qryMasterdDtVenctoPrimParc: TDateTimeField;
    qryMasteriDiasProxParc: TIntegerField;
    qryMasteriDiaVencto: TIntegerField;
    qryMasternValSinal: TBCDField;
    qryMasterdDtVenctoSinal: TDateTimeField;
    qryMasterdDtContab: TDateTimeField;
    qryMasterdDtCancel: TDateTimeField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    GroupBox1: TGroupBox;
    DBEdit17: TDBEdit;
    Label17: TLabel;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    DBEdit15: TDBEdit;
    Label15: TLabel;
    DBEdit14: TDBEdit;
    Label14: TLabel;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit22: TDBEdit;
    DataSource1: TDataSource;
    qryCliente: TADOQuery;
    qryClientenCdTerceiro: TIntegerField;
    qryClientecCNPJCPF: TStringField;
    qryClientecNmTerceiro: TStringField;
    DBEdit23: TDBEdit;
    DataSource2: TDataSource;
    DBEdit24: TDBEdit;
    qryBloco: TADOQuery;
    qryBloconCdBlocoEmpImobiliario: TAutoIncField;
    qryBlococNmBlocoEmpImobiliario: TStringField;
    DBEdit25: TDBEdit;
    DataSource3: TDataSource;
    GroupBox3: TGroupBox;
    DBEdit20: TDBEdit;
    Label20: TLabel;
    DBEdit19: TDBEdit;
    Label19: TLabel;
    Label18: TLabel;
    DBEdit18: TDBEdit;
    pageParcelasIntermediarias: TcxPageControl;
    pageIntermediaria: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    btnGerarParcela: TcxButton;
    qryIntermediarias: TADOQuery;
    qryIntermediariasnCdParcContratoEmpImobiliario: TIntegerField;
    qryIntermediariasnCdContratoEmpImobiliario: TIntegerField;
    qryIntermediariasnCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryIntermediariasiParcela: TIntegerField;
    qryIntermediariasiParcelaTotal: TIntegerField;
    qryIntermediariasdDtVencto: TDateTimeField;
    qryIntermediariasnValorParcela: TBCDField;
    dsIntermediarias: TDataSource;
    cxPageControl2: TcxPageControl;
    cxTabSheet4: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    qryParcelas: TADOQuery;
    dsParcelas: TDataSource;
    qryIntermediariascFlgLiquidada: TStringField;
    qryConsultaSaldoTit: TADOQuery;
    qryConsultaSaldoTitnSaldoTit: TBCDField;
    qryParcelasnCdParcContratoEmpImobiliario: TIntegerField;
    qryParcelasnCdContratoEmpImobiliario: TIntegerField;
    qryParcelasnCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryParcelasiParcela: TIntegerField;
    qryParcelasiParcelaTotal: TIntegerField;
    qryParcelasdDtVencto: TDateTimeField;
    qryParcelasnValorParcela: TBCDField;
    qryParcelascFlgLiquidada: TStringField;
    qryParcelascNmtabTipoParcContrato: TStringField;
    qryAux: TADOQuery;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryMasterdDtGeracao: TDateTimeField;
    SP_GERA_CONTRATO_IMOBILIARIO: TADOStoredProc;
    btCancelarContrato: TcxButton;
    cxButton1: TcxButton;
    SP_CANCELA_CONTRATO_IMOBILIARIO: TADOStoredProc;
    qryTabTipoParcContratoEmpImobiliario: TADOQuery;
    qryTabTipoParcContratoEmpImobiliarionCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryTabTipoParcContratoEmpImobiliariocNmTabTipoParcContratoEmpImobiliario: TStringField;
    cxButton2: TcxButton;
    DBEdit12: TDBEdit;
    Label12: TLabel;
    qryMasternCdIndiceReajuste: TIntegerField;
    Label21: TLabel;
    DBEdit21: TDBEdit;
    DBEdit26: TDBEdit;
    DataSource4: TDataSource;
    cxButton3: TcxButton;
    qryIndiceReajuste: TADOQuery;
    qryIndiceReajustenCdIndiceReajuste: TIntegerField;
    qryIndiceReajustecNmIndiceReajuste: TStringField;
    cxButton4: TcxButton;
    PopupMenu1: TPopupMenu;
    ImprimeRecibo: TMenuItem;
    qryConsultaParcelaTitulo: TADOQuery;
    qryConsultaParcelaTitulonCdTitulo: TIntegerField;
    qryConsultaParcelaTitulonCdEspTit: TIntegerField;
    PopupMenu2: TPopupMenu;
    ImprimeRecibo1: TMenuItem;
    pageSinal: TcxTabSheet;
    cxAnual: TcxTabSheet;
    cxChaves: TcxTabSheet;
    qrySinais: TADOQuery;
    qrySinaisnCdParcContratoEmpImobiliario: TAutoIncField;
    qrySinaisnCdContratoEmpImobiliario: TIntegerField;
    qrySinaisnCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qrySinaisiParcela: TIntegerField;
    qrySinaisiParcelaTotal: TIntegerField;
    qrySinaisdDtVencto: TDateTimeField;
    qrySinaisnValorParcela: TBCDField;
    DBGridEh2: TDBGridEh;
    dsSinais: TDataSource;
    qryAnuais: TADOQuery;
    dsAnuais: TDataSource;
    qryAnuaisnCdParcContratoEmpImobiliario: TAutoIncField;
    qryAnuaisnCdContratoEmpImobiliario: TIntegerField;
    qryAnuaisnCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryAnuaisiParcela: TIntegerField;
    qryAnuaisiParcelaTotal: TIntegerField;
    qryAnuaisdDtVencto: TDateTimeField;
    qryAnuaisnValorParcela: TBCDField;
    DBGridEh4: TDBGridEh;
    qryChaves: TADOQuery;
    dsChaves: TDataSource;
    DBGridEh5: TDBGridEh;
    qryChavesnCdParcContratoEmpImobiliario: TAutoIncField;
    qryChavesnCdContratoEmpImobiliario: TIntegerField;
    qryChavesnCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryChavesiParcela: TIntegerField;
    qryChavesiParcelaTotal: TIntegerField;
    qryChavesdDtVencto: TDateTimeField;
    qryChavesnValorParcela: TBCDField;
    cxTabSheet1: TcxTabSheet;
    qryFinBancario: TADOQuery;
    dsFinBancario: TDataSource;
    qryFinBancarionCdParcContratoEmpImobiliario: TAutoIncField;
    qryFinBancarionCdContratoEmpImobiliario: TIntegerField;
    qryFinBancarionCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryFinBancarioiParcela: TIntegerField;
    qryFinBancarioiParcelaTotal: TIntegerField;
    qryFinBancariodDtVencto: TDateTimeField;
    qryFinBancarionValorParcela: TBCDField;
    DBGridEh6: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btnGerarParcelaClick(Sender: TObject);
    procedure qryIntermediariasBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure qryParcelasCalcFields(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBEdit15Enter(Sender: TObject);
    procedure GeraParcelas;
    procedure ExcluiParcelasAberto;
    procedure FormShow(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btCancelarContratoClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure DBEdit21Exit(Sender: TObject);
    procedure DBEdit21KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure qryIntermediariasCalcFields(DataSet: TDataSet);
    procedure ImprimeReciboClick(Sender: TObject);
    procedure ImprimeRecibo1Click(Sender: TObject);
    procedure qrySinaisBeforePost(DataSet: TDataSet);
    procedure qryAnuaisBeforePost(DataSet: TDataSet);
    procedure qryChavesBeforePost(DataSet: TDataSet);
    procedure qryFinBancarioBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmContratoImobiliario: TfrmContratoImobiliario;

implementation

uses fMenu, fLookup_Padrao, rFichaPosicaoContratoImob,
  rFichaPosicaoContratoImobManual, fContratoImobiliario_Parcelas,
  rFichaContratoImobiliario, fLiquidaParcela;

{$R *.dfm}

procedure TfrmContratoImobiliario.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CONTRATOEMPIMOBILIARIO' ;
  nCdTabelaSistema  := 81 ;
  nCdConsultaPadrao := 186 ;
  bLimpaAposSalvar  := false ;
end;

procedure TfrmContratoImobiliario.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;

  qryMasteriParcelaInicial.Value := 1 ;
  qryMasteriDiasProxParc.Value   := 30 ;
  qryMasterdDtContrato.Value     := Date ;  

  btnGerarParcela.Enabled        := True ;
  DBGridEh1.ReadOnly             := False ;

  DBEdit3.SetFocus;
  
end;

procedure TfrmContratoImobiliario.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryCliente.Close ;
  PosicionaQuery(qryCliente, DBEdit3.Text) ;
  
end;

procedure TfrmContratoImobiliario.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryBloco.Close ;
  PosicionaQuery(qryBloco, DBedit4.Text) ;

end;

procedure TfrmContratoImobiliario.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (DBEdit24.Text = '') then
  begin
      MensagemAlerta('Informe o cliente.') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (DBEdit25.Text = '') then
  begin
      MensagemAlerta('Informe o empreendimento.') ;
      DBEdit4.SetFocus;
      abort ;
  end ;

  if (DBEdit5.Text = '') then
  begin
      MensagemAlerta('Informe�o n�mero da unidade.') ;
      DBedit5.SetFocus;
      abort ;
  end ;

  if (DBedit7.Text = '') then
  begin
      MensagemAlerta('Informe a data do contrato.') ;
      DBEdit7.SetFocus ;
      abort ;
  end ;

  if (qryMasternValContrato.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor total do contrato.') ;
      DBedit8.SetFocus;
      abort ;
  end ;

  if (DBEdit26.Text = '') then
  begin
      MensagemAlerta('Informe o �ndice de reajuste.') ;
      DBEdit21.SetFocus;
      abort;
  end ;

  if (qryMasternValParcela.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      DBedit11.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtVenctoPrimParc.asString = '') then
  begin
      MensagemAlerta('Informe a data de vencimento da primeira parcela.') ;
      DBedit13.SetFocus ;
      abort ;
  end ;

  if (qryMasteriParcelas.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade total de parcelas.') ;
      DBedit9.SetFocus ;
      abort ;
  end ;

  if (qryMasteriDiaVencto.Value <= 0) or (qryMasteriDiaVencto.Value > 31) then
  begin
      MensagemAlerta('Dia de vencimento inv�lido ou n�o informado.') ;
      DBedit15.SetFocus ;
      abort ;
  end ;

  if (qryMasteriParcelaInicial.Value <= 0) then
  begin
      MensagemAlerta('Informe o n�mero da parcela inicial.') ;
      DBedit10.SetFocus ;
      abort ;
  end ;

end;

procedure TfrmContratoImobiliario.btnGerarParcelaClick(Sender: TObject);
var
    iAno, iMes, iDia : Word ;
    iParcela  : integer ;
    dDtVencto : TDateTime ;
begin

    if (qryMaster.State <> dsBrowse) then
    begin

        btSalvar.Click;

    end ;

    try

        qryParcelas.DisableControls;

        try

            frmMenu.Connection.BeginTrans;

            {-- Exclui as parcelas em aberto --}
            ExcluiParcelasAberto;

            qryParcelas.Close ;
            qryParcelas.Open ;

            {-- Gera as parcelas --}
            iParcela := qryMasteriParcelaInicial.Value ;

            DecodeDate(qryMasterdDtVenctoPrimParc.Value, iAno, iMes, iDia) ;
            dDtVencto := EncodeDate(iAno, iMes, qryMasteriDiaVencto.Value) ;

            while (iParcela <= qryMasteriParcelas.Value) do
            begin

                if not qryParcelas.Active then
                    qryParcelas.Open ;

                dDtVencto := IncMonth(qryMasterdDtVenctoPrimParc.Value, (iParcela - 1)) ;

                qryAux.Close ;
                qryAux.SQL.Clear ;
                qryAux.SQL.Add('SELECT 1 FROM ParcContratoEmpImobiliario WHERE nCdContratoEmpImobiliario = ' + qryMasternCdContratoEmpImobiliario.AsString + ' AND nCdTabTipoParcContratoEmpImobiliario = 2 AND iParcela = ' + intToStr(iParcela)) ;
                qryAux.Open ;

                if (qryAux.Eof) then
                begin

                    try
                        qryParcelas.Insert ;
                        qryParcelasnCdContratoEmpImobiliario.Value            := qryMasternCdContratoEmpImobiliario.Value ;
                        qryParcelasnCdTabTipoParcContratoEmpImobiliario.Value := 2 ;
                        qryParcelasiParcela.Value                             := iParcela ;
                        qryParcelasiParcelaTotal.Value                        := qryMasteriParcelas.Value ;

                        if (iParcela = 1) then
                            qryParcelasdDtVencto.Value := incMonth(qryMasterdDtVenctoPrimParc.Value, (iParcela-1))
                        else qryParcelasdDtVencto.Value := dDtVencto ;

                        qryParcelasnValorParcela.Value                        := qryMasternValParcela.Value ;
                        qryParcelas.Post;
                    except
                        MensagemErro('Problema na gera��o da parcela n�mero : ' + intTostr(iParcela)) ;
                        raise ;
                    end ;
                end
                else
                begin

                    qryParcelas.Locate('iParcela;nCdTabTipoParcContratoEmpImobiliario',VarArrayOf([iParcela,2]),[loCaseInsensitive,loPartialKey]);

                    if (qryParcelascFlgLiquidada.Value <> 'Sim') then
                    begin
                        try
                            qryParcelas.Edit ;
                            qryParcelasiParcelaTotal.Value                        := qryMasteriParcelas.Value ;

                            if (iParcela = 1) then
                                qryParcelasdDtVencto.Value := incMonth(qryMasterdDtVenctoPrimParc.Value, (iParcela-1))
                            else qryParcelasdDtVencto.Value := dDtVencto ;

                            qryParcelasnValorParcela.Value                        := qryMasternValParcela.Value ;
                            qryParcelas.Post;
                        except
                            MensagemErro('Problema na gera��o da parcela n�mero : ' + intTostr(iParcela)) ;
                            raise ;
                        end ;
                    end ;
                end ;


                iParcela := iParcela + 1 ;

            end ;

            frmMenu.Connection.CommitTrans;

        except

            frmMenu.Connection.RollbackTrans;
            raise;

        end ;

    finally

        qryParcelas.First;
        qryParcelas.EnableControls;

    end ;

    ShowMessage('Parcelas geradas com sucesso.') ;

    pageParcelasIntermediarias.ActivePageIndex := 0 ;
    DBGridEh2.SetFocus;

end;

procedure TfrmContratoImobiliario.qryIntermediariasBeforePost(
  DataSet: TDataSet);
begin

  if (qryIntermediariasiParcela.Value <= 0) then
  begin
      MensagemAlerta('Informe o n�mero da parcela.') ;
      abort ;
  end ;

  if (qryIntermediariasdDtVencto.Value < qryMasterdDtContrato.Value) then
  begin
      MensagemAlerta('Data de vencimento menor que a data do contrato ou data n�o informada.');
      abort ;
  end ;

  if (qryIntermediariasnValorParcela.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

  qryIntermediariasnCdContratoEmpImobiliario.Value            := qryMasternCdContratoEmpImobiliario.Value ;
  qryIntermediariasnCdTabTipoParcContratoEmpImobiliario.Value := 3 ;
  
  inherited;

end;

procedure TfrmContratoImobiliario.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  if (bInclusao) and (qryMastercNumContrato.Value = '') then
      qryMastercNumContrato.Value := frmMenu.ZeroEsquerda(qryMasternCdContratoEmpImobiliario.asString,8) ;

  if (not qryIntermediarias.Active) then
  begin
      qryIntermediarias.Close ;
      PosicionaQuery(qryIntermediarias, qryMasternCdContratoEmpImobiliario.AsString) ;
  end ;

  if (not qrySinais.Active) then
  begin
      qrySinais.Close ;
      PosicionaQuery(qrySinais, qryMasternCdContratoEmpImobiliario.AsString) ;
  end ;

  if (not qryAnuais.Active) then
  begin
      qryAnuais.Close ;
      PosicionaQuery(qryAnuais, qryMasternCdContratoEmpImobiliario.AsString) ;
  end ;

  if (not qryChaves.Active) then
  begin
      qryChaves.Close ;
      PosicionaQuery(qryChaves, qryMasternCdContratoEmpImobiliario.AsString) ;
  end ;

  if (not qryFinBancario.Active) then
  begin
      qryFinBancario.Close ;
      PosicionaQuery(qryFinBancario, qryMasternCdContratoEmpImobiliario.AsString) ;
  end ;

  if (not qryParcelas.Active) then
  begin
      qryParcelas.Close ;
      PosicionaQuery(qryParcelas, qryMasternCdContratoEmpImobiliario.AsString) ;
  end ;

end;

procedure TfrmContratoImobiliario.btSalvarClick(Sender: TObject);
begin

  if (qryMaster.Active) and (qryMasterdDtGeracao.AsString <> '') then
  begin
      MensagemErro('Contrato j� gerado n�o pode ser alterado.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmContratoImobiliario.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryCliente.Close ;
  qryBloco.Close ;
  qryIntermediarias.Close ;
  qrySinais.Close ;
  qryAnuais.Close ;
  qryChaves.Close ;
  qryFinBancario.Close ;
  qryParcelas.Close ;
  qryIndiceReajuste.Close ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  PosicionaQuery(qryCliente, qryMasternCdTerceiro.AsString) ;
  PosicionaQuery(qryBloco, qryMasternCdBlocoEmpImobiliario.AsString) ;
  PosicionaQuery(qryIntermediarias, qryMasternCdContratoEmpImobiliario.AsString) ;
  PosicionaQuery(qryChaves, qryMasternCdContratoEmpImobiliario.AsString) ;
  PosicionaQuery(qryFinBancario, qryMasternCdContratoEmpImobiliario.AsString) ;
  PosicionaQuery(qryAnuais, qryMasternCdContratoEmpImobiliario.AsString) ;
  PosicionaQuery(qrySinais, qryMasternCdContratoEmpImobiliario.AsString) ;
  PosicionaQuery(qryParcelas, qryMasternCdContratoEmpImobiliario.AsString) ;
  PosicionaQuery(qryIndiceReajuste, qryMasternCdIndiceReajuste.AsString) ;

  btnGerarParcela.Enabled := (qryMasterdDtGeracao.AsString = '') ;
  DBGridEh1.ReadOnly      := (qryMasterdDtGeracao.AsString <> '') ;
  DBGridEh2.ReadOnly      := (qryMasterdDtGeracao.AsString <> '') ;
  DBGridEh4.ReadOnly      := (qryMasterdDtGeracao.AsString <> '') ;
  DBGridEh5.ReadOnly      := (qryMasterdDtGeracao.AsString <> '') ;

end;

procedure TfrmContratoImobiliario.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryCliente.Close ;
  qryBloco.Close ;
  qryIntermediarias.Close ;
  qrySinais.Close ;
  qryAnuais.Close ;
  qryChaves.Close ;
  qryFinBancario.Close ;
  qryParcelas.Close ;
  qryIndiceReajuste.Close ;
  
end;

procedure TfrmContratoImobiliario.qryParcelasCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryParcelascFlgLiquidada.Value = '') then
  begin
      qryConsultaSaldoTit.Close ;
      PosicionaQuery(qryConsultaSaldoTit, qryParcelasnCdParcContratoEmpImobiliario.asString) ;

      if not qryConsultaSaldoTit.Eof and (qryConsultaSaldoTitnSaldoTit.Value <= 0) then
          qryParcelascFlgLiquidada.Value := 'Sim' ;

      if (qryParcelascNmtabTipoParcContrato.Value = '') then
      begin
          qryTabTipoParcContratoEmpImobiliario.Close ;
          PosicionaQuery(qryTabTipoParcContratoEmpImobiliario, qryParcelasnCdTabTipoParcContratoEmpImobiliario.AsString) ;


          if not qryTabTipoParcContratoEmpImobiliario.eof then
              qryParcelascNmtabTipoParcContrato.Value := qryTabTipoParcContratoEmpImobiliariocNmTabTipoParcContratoEmpImobiliario.Value ;
      end ;
  end ;

end;

procedure TfrmContratoImobiliario.DBGridEh1Enter(Sender: TObject);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;
      
  inherited;

end;

procedure TfrmContratoImobiliario.DBEdit15Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
  begin
      if (qryMasterdDtVenctoPrimParc.AsString <> '') then
          qrymasteriDiaVencto.Value := strToint(FormatDateTime('dd',qryMasterdDtVenctoPrimParc.Value)) ;
  end ;

end;

procedure TfrmContratoImobiliario.GeraParcelas;
begin

  if (not qryMaster.Active) then
      exit ;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

  if MessageDlg('Confirma a gera��o das parcelas ?',mtConfirmation,[mbYes,mbNo],0) = MRNO then
      exit ;

  GeraParcelas;

end;

procedure TfrmContratoImobiliario.ExcluiParcelasAberto;
begin

    if (not qryParcelas.Active) then
        PosicionaQuery(qryParcelas, qryMasternCdContratoEmpImobiliario.AsString) ;

    qryParcelas.DisableControls;
    qryParcelas.First ;

    while not qryParcelas.eof do
    begin
        qryParcelas.Delete;
        qryParcelas.First ;
    end ;

    qryParcelas.First ;
    qryParcelas.EnableControls;

end;

procedure TfrmContratoImobiliario.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  pageParcelasIntermediarias.ActivePageIndex := 0 ;
  
end;

procedure TfrmContratoImobiliario.ToolButton10Click(Sender: TObject);
var
    nValIntermediarias, nValParcelas : double ;
begin
  inherited;

  if not (qryMaster.Active) then
      exit ;

  if (qryMasterdDtGeracao.AsString <> '') then
  begin
      MensagemAlerta('Este contrato j� foi gerado.') ;
      exit ;
  end ;

  nValIntermediarias := 0 ;
  nValParcelas       := 0 ;

  try
      qryIntermediarias.DisableControls;
      qrySinais.DisableControls;
      qryAnuais.DisableControls;
      qryChaves.DisableControls;
      qryFinBancario.DisableControls;

      qryIntermediarias.First;
      qrySinais.First;
      qryAnuais.First;
      qryChaves.First;
      qryFinBancario.First;

      while not qryIntermediarias.Eof do
      begin
          nValIntermediarias := nValIntermediarias + qryIntermediariasnValorParcela.Value ;
          qryIntermediarias.Next ;
      end ;

      while not qrySinais.Eof do
      begin
          nValIntermediarias := nValIntermediarias + qrySinaisnValorParcela.Value ;
          qrySinais.Next ;
      end ;

      while not qryAnuais.Eof do
      begin
          nValIntermediarias := nValIntermediarias + qryAnuaisnValorParcela.Value ;
          qryAnuais.Next ;
      end ;

      while not qryChaves.Eof do
      begin
          nValIntermediarias := nValIntermediarias + qryChavesnValorParcela.Value ;
          qryChaves.Next ;
      end ;

      while not qryFinBancario.Eof do
      begin
          nValIntermediarias := nValIntermediarias + qryFinBancarionValorParcela.Value ;
          qryFinBancario.Next ;
      end ;

      qryIntermediarias.First;
      qrySinais.First;
      qryAnuais.First;
      qryChaves.First;
      qryFinBancario.First;

  finally
      qryIntermediarias.EnableControls;
      qrySinais.EnableControls;
      qryAnuais.EnableControls;
      qryChaves.EnableControls;
      qryFinBancario.EnableControls;
  end ;

  nValParcelas := (qryMasternValParcela.Value * qryMasteriParcelas.Value) ;

  if ( abs( frmMenu.TBRound(qryMasternValContrato.Value,2) - (frmMenu.TBRound(nValIntermediarias,2) + frmMenu.TBRound(nValParcelas,2)) ) > 1 ) then
  begin

      MensagemAlerta('A Soma das parcelas � diferente do valor total do contrato.') ;
      abort ;

  end ;

  if (MessageDlg('Confirma a gera��o deste contrato?'+#13#13+'Ap�s este passo o contrato n�o poder� sofre mais altera��es.',mtConfirmation,[mbYes,mbNo],0)=MRNO) then
      exit ;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;
      
  frmMenu.Connection.BeginTrans;

  try

      SP_GERA_CONTRATO_IMOBILIARIO.Close ;
      SP_GERA_CONTRATO_IMOBILIARIO.Parameters.ParamByName('@nCdContratoEmpImobiliario').Value := qryMasternCdContratoEmpImobiliario.Value ;
      SP_GERA_CONTRATO_IMOBILIARIO.ExecProc;

      qryMaster.Edit;
      qryMasterdDtGeracao.Value := Now();
      qryMaster.Post;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Contrato gerado com sucesso!'+#13#13+'As parcelas foram geradas no sistema financeiro.') ;

  if (MessageDlg('Deseja imprimir a ficha do contrato ?',mtConfirmation,[mbYes,mbNo],0)=MRYES) then
      cxButton4.Click;

  btCancelar.Click;

end;

procedure TfrmContratoImobiliario.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(101);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmContratoImobiliario.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(187);

            If (nPK > 0) then
            begin
                qryMasternCdBlocoEmpImobiliario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmContratoImobiliario.btCancelarContratoClick(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      exit ;

  if (qryMasterdDtGeracao.AsString = '') then
  begin
      MensagemAlerta('Este contrato n�o foi gerado.') ;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Este contrato j� foi cancelado.') ;
      exit ;
  end ;

  if MessageDlg('Ao cancelar este contrato, todas as parcelas pendentes ser�o canceladas no financeiro.' +#13#13 + 'Tem certeza que deseja confirmar ?',mtConfirmation,[mbYes,mbNo],0) = MRNO then
      exit;

  frmMenu.Connection.BeginTrans;

  try

      SP_CANCELA_CONTRATO_IMOBILIARIO.Close ;
      SP_CANCELA_CONTRATO_IMOBILIARIO.Parameters.ParamByName('@nCdContratoEmpImobiliario').Value := qryMasternCdContratoEmpImobiliario.Value;
      SP_CANCELA_CONTRATO_IMOBILIARIO.Parameters.ParamByName('@nCdUsuario').Value                := frmMenu.nCdUsuarioLogado ;
      SP_CANCELA_CONTRATO_IMOBILIARIO.ExecProc;

      frmMenu.LogAuditoria(81,4,qryMasternCdContratoEmpImobiliario.Value,'Contrato Cancelado.') ;

      qryMaster.Edit ;
      qryMasterdDtcancel.Value := Now() ;
      qryMaster.Post ;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Contrato cancelado com sucesso. Os t�tulos financeiros foram cancelados.') ;

  btcancelar.Click;

end;

procedure TfrmContratoImobiliario.cxButton1Click(Sender: TObject);
var
  objRel : TrptFichaPosicaoContratoImob ;
begin
  if not (qryMaster.Active) then
      exit ;

  if (qryMasterdDtGeracao.AsString = '') then
  begin
      MensagemAlerta('Este contrato n�o foi gerado.') ;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Este contrato est� cancelado.') ;
  end ;

  objRel := TrptFichaPosicaoContratoImob.Create(nil) ;

  PosicionaQuery(objRel.qryContrato, qryMasternCdContratoEmpImobiliario.AsString) ;

  objRel.lblEmpresa.Caption := frmMenu.cNmFantasiaEmpresa;
  objRel.QRLabel7.Caption   := 'P A R C E L A S     D O     C O N T R A T O' ;

  if (qryMasterdDtCancel.AsString <> '') then
      objRel.QRLabel7.Caption := objRel.QRLabel7.Caption + ' *** CANCELADO EM ' + qryMasterdDtcancel.AsString + ' ***' ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;


procedure TfrmContratoImobiliario.cxButton2Click(Sender: TObject);
var
  objRel : TrptFichaPosicaoContratoImobManual ;
begin
  inherited;
  if not (qryMaster.Active) then
      exit ;

  if (qryMasterdDtGeracao.AsString = '') then
  begin
      MensagemAlerta('Este contrato n�o foi gerado.') ;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Este contrato est� cancelado.') ;
  end ;

  objRel := TrptFichaPosicaoContratoImobManual.Create(nil) ;

  PosicionaQuery(objRel.qryContrato, qryMasternCdContratoEmpImobiliario.AsString) ;
  objRel.lblEmpresa.Caption := frmMenu.cNmFantasiaEmpresa;

  objRel.QRLabel7.Caption := 'P A R C E L A S     D O     C O N T R A T O' ;

  if (qryMasterdDtCancel.AsString <> '') then
      objRel.QRLabel7.Caption := objRel.QRLabel7.Caption + ' *** CANCELADO EM ' + qryMasterdDtcancel.AsString + ' ***' ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmContratoImobiliario.DBEdit21Exit(Sender: TObject);
begin
  inherited;

  qryIndiceReajuste.Close ;
  PosicionaQuery(qryIndiceReajuste, DBEdit21.Text) ;

end;

procedure TfrmContratoImobiliario.DBEdit21KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(188);

            If (nPK > 0) then
            begin
                qryMasternCdIndiceReajuste.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmContratoImobiliario.cxButton3Click(Sender: TObject);
var
  objForm : TfrmContratoImobiliario_Parcelas ;
begin
  inherited;

  if (qryMaster.Active) and (qryParcelas.Active) and (qryParcelas.RecordCount > 0) then
  begin

      objForm := TfrmContratoImobiliario_Parcelas.Create(nil) ;

      PosicionaQuery(objForm.qryParcelas, qryMasternCdContratoEmpImobiliario.asString) ;

      showForm( objForm , TRUE ) ;

  end ;

end;

procedure TfrmContratoImobiliario.cxButton4Click(Sender: TObject);
var
  objRel : TrptFichaContratoImobiliario ;
begin
  inherited;
  // buttom 4
  if not (qryMaster.Active) then
      exit ;

  if (qryMasterdDtGeracao.AsString = '') then
  begin
      MensagemAlerta('Este contrato n�o foi gerado.') ;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Este contrato est� cancelado.') ;
      exit;
  end ;

  objRel := TrptFichaContratoImobiliario.Create(nil) ;

  objRel.lblEmpresa.Caption := frmMenu.cNmFantasiaEmpresa;

  objRel.uspRelatorio.Close;
  objRel.uspRelatorio.Parameters.ParamByName('@nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
  objRel.uspRelatorio.Parameters.ParamByName('@nCdContrato').Value := qryMasternCdContratoEmpImobiliario.Value ;
  objRel.uspRelatorio.Parameters.ParamByName('@nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  objRel.uspRelatorio.Open;

  PosicionaQuery(objRel.qryTerceiro      , qryMasternCdTerceiro.AsString) ;
  PosicionaQuery(objRel.qryPessoaFisica  , qryMasternCdTerceiro.AsString) ;
  PosicionaQuery(objRel.qryIndiceReajuste, qryMasternCdIndiceReajuste.AsString) ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;


end;

procedure TfrmContratoImobiliario.btCancelarClick(Sender: TObject);
begin
  inherited;
  DBEdit26.Clear;
end;

procedure TfrmContratoImobiliario.qryIntermediariasCalcFields(
  DataSet: TDataSet);
begin
  inherited;


end;
//imprime recibo parcela financiamentos/sinal
procedure TfrmContratoImobiliario.ImprimeReciboClick(Sender: TObject);
begin
  inherited;
  if (MessageDlg('Imprime Recibo ?' + #13#13 ,mtConfirmation,[mbYes,mbNo],0) = MRYES) then
    if (qryParcelascFlgLiquidada.Value = 'Sim') then
    begin
      qryConsultaParcelaTitulo.Close ;
      PosicionaQuery(qryConsultaParcelaTitulo, qryParcelasnCdParcContratoEmpImobiliario.asString) ;

      FrmLiquidaParcela.imprimeRecibo(qryConsultaParcelaTitulonCdTitulo.Value,qryMasternCdTerceiro.Value,qryConsultaParcelaTitulonCdEspTit.Value);
    end;
end;

//imprime recibo parcela intermediarias
procedure TfrmContratoImobiliario.ImprimeRecibo1Click(Sender: TObject);
begin
  inherited;
 if (MessageDlg('Imprime Recibo ?' + #13#13 ,mtConfirmation,[mbYes,mbNo],0) = MRYES) then
    if (qryIntermediariascFlgLiquidada.Value = 'Sim') then
    begin
      qryConsultaParcelaTitulo.Close ;
      PosicionaQuery(qryConsultaParcelaTitulo, qryIntermediariasnCdParcContratoEmpImobiliario.asString) ;

      FrmLiquidaParcela.imprimeRecibo(qryConsultaParcelaTitulonCdTitulo.Value,qryMasternCdTerceiro.Value,qryConsultaParcelaTitulonCdEspTit.Value);
    end;
end;

procedure TfrmContratoImobiliario.qrySinaisBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qrySinaisiParcela.Value <= 0) then
  begin
      MensagemAlerta('Informe o n�mero da parcela.') ;
      abort ;
  end ;

  if (qrySinaisdDtVencto.Value < qryMasterdDtContrato.Value) then
  begin
      MensagemAlerta('Data de vencimento menor que a data do contrato ou data n�o informada.');
      abort ;
  end ;

  if (qrySinaisnValorParcela.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

  qrySinaisnCdContratoEmpImobiliario.Value            := qryMasternCdContratoEmpImobiliario.Value ;
  qrySinaisnCdTabTipoParcContratoEmpImobiliario.Value := 1 ;

end;

procedure TfrmContratoImobiliario.qryAnuaisBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryAnuaisiParcela.Value <= 0) then
  begin
      MensagemAlerta('Informe o n�mero da parcela.') ;
      abort ;
  end ;

  if (qryAnuaisdDtVencto.Value < qryMasterdDtContrato.Value) then
  begin
      MensagemAlerta('Data de vencimento menor que a data do contrato ou data n�o informada.');
      abort ;
  end ;

  if (qryAnuaisnValorParcela.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

  qryAnuaisnCdContratoEmpImobiliario.Value            := qryMasternCdContratoEmpImobiliario.Value ;
  qryAnuaisnCdTabTipoParcContratoEmpImobiliario.Value := 5 ;

end;

procedure TfrmContratoImobiliario.qryChavesBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryChavesiParcela.Value <= 0) then
  begin
      MensagemAlerta('Informe o n�mero da parcela.') ;
      abort ;
  end ;

  if (qryChavesdDtVencto.Value < qryMasterdDtContrato.Value) then
  begin
      MensagemAlerta('Data de vencimento menor que a data do contrato ou data n�o informada.');
      abort ;
  end ;

  if (qryChavesnValorParcela.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

  qryChavesnCdContratoEmpImobiliario.Value            := qryMasternCdContratoEmpImobiliario.Value ;
  qryChavesnCdTabTipoParcContratoEmpImobiliario.Value := 4 ;

end;

procedure TfrmContratoImobiliario.qryFinBancarioBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryFinBancarioiParcela.Value <= 0) then
  begin
      MensagemAlerta('Informe o n�mero da parcela.') ;
      abort ;
  end ;

  if (qryFinBancariodDtVencto.Value < qryMasterdDtContrato.Value) then
  begin
      MensagemAlerta('Data de vencimento menor que a data do contrato ou data n�o informada.');
      abort ;
  end ;

  if (qryFinBancarionValorParcela.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

  qryFinBancarionCdContratoEmpImobiliario.Value            := qryMasternCdContratoEmpImobiliario.Value ;
  qryFinBancarionCdTabTipoParcContratoEmpImobiliario.Value := 6 ;

end;

initialization
    RegisterClass(TfrmContratoImobiliario) ;

end.
