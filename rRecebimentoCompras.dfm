inherited rptRecebimentoCompras: TrptRecebimentoCompras
  Left = 299
  Top = 235
  Width = 814
  Height = 327
  Caption = 'Rel. Recebimento de Mercadorias'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 798
    Height = 265
  end
  object Label5: TLabel [1]
    Left = 9
    Top = 67
    Width = 97
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro Fornecedor'
  end
  object Label3: TLabel [2]
    Left = 5
    Top = 163
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Recebimento'
  end
  object Label6: TLabel [3]
    Left = 188
    Top = 163
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label1: TLabel [4]
    Left = 68
    Top = 91
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
  end
  object Label4: TLabel [5]
    Left = 34
    Top = 139
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Pedido'
  end
  object Label7: TLabel [6]
    Left = 21
    Top = 115
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Recebimento'
  end
  object Label2: TLabel [7]
    Left = 86
    Top = 43
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label8: TLabel [8]
    Left = 290
    Top = 162
    Width = 189
    Height = 13
    Caption = '(Data da Confirma'#231#227'o do Recebimento)'
  end
  inherited ToolBar1: TToolBar
    Width = 798
    TabOrder = 14
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit6: TMaskEdit [10]
    Left = 109
    Top = 59
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object MaskEdit1: TMaskEdit [11]
    Left = 109
    Top = 155
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 10
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [12]
    Left = 209
    Top = 155
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 11
    Text = '  /  /    '
  end
  object MaskEdit4: TMaskEdit [13]
    Left = 109
    Top = 83
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object MaskEdit5: TMaskEdit [14]
    Left = 109
    Top = 131
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 9
    Text = '      '
  end
  object MaskEdit7: TMaskEdit [15]
    Left = 109
    Top = 107
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 7
    Text = '      '
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object RadioGroup5: TRadioGroup [16]
    Left = 107
    Top = 187
    Width = 198
    Height = 41
    Caption = ' Somente Recebimentos Finalizados ? '
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'N'#227'o'
      'Sim')
    TabOrder = 12
  end
  object DBEdit3: TDBEdit [17]
    Tag = 1
    Left = 177
    Top = 83
    Width = 605
    Height = 21
    DataField = 'cNmProduto'
    DataSource = dsProduto
    TabOrder = 6
  end
  object DBEdit4: TDBEdit [18]
    Tag = 1
    Left = 177
    Top = 107
    Width = 605
    Height = 21
    DataField = 'cNmTipoReceb'
    DataSource = dsTipoReceb
    TabOrder = 8
  end
  object DBEdit1: TDBEdit [19]
    Tag = 1
    Left = 177
    Top = 59
    Width = 186
    Height = 21
    DataField = 'cCnpjCpf'
    DataSource = dsTerceiro
    TabOrder = 3
  end
  object DBEdit2: TDBEdit [20]
    Tag = 1
    Left = 366
    Top = 59
    Width = 416
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 4
  end
  object DBEdit6: TDBEdit [21]
    Tag = 1
    Left = 177
    Top = 35
    Width = 605
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 1
  end
  object MaskEdit3: TMaskEdit [22]
    Left = 109
    Top = 35
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object RadioGroup1: TRadioGroup [23]
    Left = 523
    Top = 187
    Width = 198
    Height = 41
    Caption = ' Modo Exibi'#231#227'o '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Relat'#243'rio'
      'Planilha')
    TabOrder = 13
  end
  object rgExibeItem: TRadioGroup [24]
    Left = 315
    Top = 187
    Width = 198
    Height = 41
    Caption = ' Exibir Itens do Recebimento ? '
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'N'#227'o'
      'Sim')
    TabOrder = 15
  end
  inherited ImageList1: TImageList
    Left = 512
    Top = 208
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 544
    Top = 208
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'select nCdProduto, cNmProduto  from produto'
      'WHERE nCdProduto = :nCdProduto')
    Left = 640
    Top = 208
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object qryTipoReceb: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTipoReceb'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'select nCdTipoReceb, cNmTipoReceb from tipoReceb'
      'where nCdTipoReceb = :nCdTipoReceb')
    Left = 672
    Top = 208
    object qryTipoRecebnCdTipoReceb: TIntegerField
      FieldName = 'nCdTipoReceb'
    end
    object qryTipoRecebcNmTipoReceb: TStringField
      FieldName = 'cNmTipoReceb'
      Size = 50
    end
  end
  object dsProduto: TDataSource
    DataSet = qryProduto
    Left = 640
    Top = 240
  end
  object dsTipoReceb: TDataSource
    DataSet = qryTipoReceb
    Left = 672
    Top = 240
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nPK')
    Left = 608
    Top = 208
    object qryTerceironcdterceiro: TIntegerField
      FieldName = 'ncdterceiro'
    end
    object qryTerceirocCnpjCpf: TStringField
      FieldName = 'cCnpjCpf'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 608
    Top = 240
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 544
    Top = 240
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT  nCdLoja'
      ',cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK'
      'AND Exists(SELECT 1 '
      'FROM UsuarioLoja '
      'WHERE UsuarioLoja.nCdLoja = Loja.nCdLoja '
      'AND UsuarioLoja.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa'
      ''
      '')
    Left = 576
    Top = 208
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 576
    Top = 240
  end
  object ER2Excel: TER2Excel
    Titulo = 'Rel. Recebimento de Mercadorias'
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 512
    Top = 240
  end
end
