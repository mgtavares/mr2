unit fAdmListaCobranca_Listas_SelLista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, GridsEh, DBGridEh, ImgList,
  ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmAdmListaCobranca_Listas_SelLista = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryListas: TADOQuery;
    dsListas: TDataSource;
    qryListasnCdListaCobranca: TIntegerField;
    qryListasdDtCobranca: TDateTimeField;
    qryListasdDtVencto: TDateTimeField;
    qryListasiSeq: TIntegerField;
    qryListascNmListaCobranca: TStringField;
    qryListasiQtdeClienteTotal: TIntegerField;
    qryListasiQtdeClienteRestante: TIntegerField;
    qryListascNmUsuario: TStringField;
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdListaCobrancaAtual : integer ;
  end;

var
  frmAdmListaCobranca_Listas_SelLista: TfrmAdmListaCobranca_Listas_SelLista;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAdmListaCobranca_Listas_SelLista.DBGridEh1KeyUp(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;
  if (Key = VK_RETURN) then
      close ;

end;

procedure TfrmAdmListaCobranca_Listas_SelLista.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  {}
end;

procedure TfrmAdmListaCobranca_Listas_SelLista.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  {}
end;

procedure TfrmAdmListaCobranca_Listas_SelLista.ToolButton2Click(
  Sender: TObject);
begin
  qryListas.Close;
  inherited;

end;

procedure TfrmAdmListaCobranca_Listas_SelLista.FormShow(Sender: TObject);
begin
  inherited;

  qrylistas.Close;
  qryListas.Parameters.ParamByName('nCdLoja').Value               := frmMenu.nCdLojaAtiva ;
  qryListas.Parameters.ParamByName('nCdListaCobrancaAtual').Value := nCdListaCobrancaAtual;
  PosicionaQuery(qryListas, intToStr(frmMenu.nCdEmpresaAtiva)) ;

  if qryListas.eof then
      ShowMessage('Nenhuma lista de cobranša disponÝvel.') ;
      
end;

procedure TfrmAdmListaCobranca_Listas_SelLista.DBGridEh1DblClick(
  Sender: TObject);
begin
  inherited;
  Close;

end;

end.
