unit fModulo_CAD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ComCtrls, StdCtrls, Mask, ExtCtrls, DBCtrls, DB, ADODB,
  Grids, DBGrids;

type
  TfrmModulo_CAD = class(TForm)
    ImageList1: TImageList;
    Button1: TButton;
    StatusBar1: TStatusBar;
    Bevel1: TBevel;
    Database: TADOConnection;
    qryStatus: TADOQuery;
    dsStatus: TDataSource;
    Label7: TLabel;
    Bevel2: TBevel;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    Button5: TButton;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    qryModulo: TADOQuery;
    qryModulonCdModulo: TIntegerField;
    qryModulocNmModulo: TStringField;
    qryModulonCdStatus: TIntegerField;
    Label1: TLabel;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    Label2: TLabel;
    DBEdit4: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit1Enter(Sender: TObject);
    procedure DBEdit1Exit(Sender: TObject);
    procedure DBEdit2Enter(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmModulo_CAD: TfrmModulo_CAD;

implementation

{$R *.dfm}


procedure TfrmModulo_CAD.FormCreate(Sender: TObject);
begin

    Database.DefaultDatabase := 'ADMSoft' ;
    Database.LoginPrompt := False;
    Database.Open('ADMSoft','ADMSoft101580');
    Database.Connected := True;

    qryStatus.Close;
    qryStatus.Open;

    qryModulo.Close;
    qryModulo.Open;

    StatusBar1.Panels.Items[0].Text := 'ADMSoft v1.0' ;

end;


procedure TfrmModulo_CAD.DBEdit1Enter(Sender: TObject);
begin
dbEdit1.Color := clYellow;
end;

procedure TfrmModulo_CAD.DBEdit1Exit(Sender: TObject);
begin
dbEdit1.Color := clAqua;
end;

procedure TfrmModulo_CAD.DBEdit2Enter(Sender: TObject);
begin
dbEdit2.Color := clYellow;

end;

procedure TfrmModulo_CAD.DBEdit2Exit(Sender: TObject);
begin
dbEdit2.Color := clAqua;

end;

procedure TfrmModulo_CAD.Button5Click(Sender: TObject);
begin
    frmStatus_CAD.Close;
end;

procedure TfrmModulo_CAD.Button2Click(Sender: TObject);
begin
    qryStatus.Insert;
end;

procedure TfrmModulo_CAD.Button1Click(Sender: TObject);
begin
    qryStatus.Post;
end;

procedure TfrmModulo_CAD.Button3Click(Sender: TObject);
begin
    qryStatus.Cancel;
end;

procedure TfrmModulo_CAD.Button4Click(Sender: TObject);
begin
    qryStatus.Delete;
end;

end.
