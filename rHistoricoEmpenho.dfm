inherited rRelHistoricoEmpenho: TrRelHistoricoEmpenho
  Left = 319
  Top = 202
  Width = 762
  Height = 288
  Caption = 'Rel. Hist'#243'rico de Empenho'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 746
    Height = 226
  end
  object Label1: TLabel [1]
    Left = 19
    Top = 46
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Estoque Empenhado'
  end
  object Label2: TLabel [2]
    Left = 15
    Top = 70
    Width = 102
    Height = 13
    Alignment = taRightJustify
    Caption = 'Estoque Requisitante'
  end
  object Label3: TLabel [3]
    Left = 86
    Top = 94
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
  end
  object Label6: TLabel [4]
    Left = 212
    Top = 120
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label4: TLabel [5]
    Left = 16
    Top = 120
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo de Empenho'
  end
  inherited ToolBar1: TToolBar
    Width = 746
    TabOrder = 9
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object er2LkpEstoqueOrig: TER2LookupMaskEdit [7]
    Left = 120
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    OnExit = er2LkpEstoqueOrigExit
    CodigoLookup = 87
  end
  object er2LkpEstoqueDest: TER2LookupMaskEdit [8]
    Left = 120
    Top = 64
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    OnExit = er2LkpEstoqueDestExit
    CodigoLookup = 87
  end
  object er2LkpStatus: TER2LookupMaskEdit [9]
    Left = 120
    Top = 88
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 4
    Text = '         '
    OnExit = er2LkpStatusExit
    CodigoLookup = 2
  end
  object RadioGroup1: TRadioGroup [10]
    Left = 120
    Top = 141
    Width = 409
    Height = 49
    Caption = ' Origem do Empenho '
    Color = 13086366
    Columns = 5
    ItemIndex = 0
    Items.Strings = (
      ' Todos'
      ' Venda '
      ' Comra'
      ' Manual'
      ' Venda Web')
    ParentColor = False
    TabOrder = 8
  end
  object DBEdit1: TDBEdit [11]
    Tag = 1
    Left = 188
    Top = 40
    Width = 549
    Height = 21
    DataField = 'cNmLocalEstoque'
    DataSource = dsLocalOrigem
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [12]
    Tag = 1
    Left = 188
    Top = 64
    Width = 549
    Height = 21
    DataField = 'cNmLocalEstoque'
    DataSource = dsLocalDestino
    TabOrder = 3
  end
  object DBEdit3: TDBEdit [13]
    Tag = 1
    Left = 188
    Top = 88
    Width = 245
    Height = 21
    DataField = 'cNmStatus'
    DataSource = DataSource1
    TabOrder = 5
  end
  object edtDtFinal: TMaskEdit [14]
    Left = 240
    Top = 112
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object edtDtInicial: TMaskEdit [15]
    Left = 120
    Top = 112
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
  end
  object rgModeloImp: TRadioGroup [16]
    Left = 120
    Top = 192
    Width = 185
    Height = 41
    Caption = ' Modelo '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Planilha')
    ParentColor = False
    TabOrder = 10
  end
  inherited ImageList1: TImageList
    Left = 536
    Top = 72
  end
  object qryLocalOrigem: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK ')
    Left = 600
    Top = 72
    object qryLocalOrigemcNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object qryLocalDestino: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK')
    Left = 632
    Top = 72
    object qryLocalDestinocNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object dsLocalOrigem: TDataSource
    DataSet = qryLocalOrigem
    Left = 600
    Top = 104
  end
  object dsLocalDestino: TDataSource
    DataSet = qryLocalDestino
    Left = 632
    Top = 104
  end
  object qryStatus: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmStatus'
      '  FROM Status'
      ' WHERE nCdStatus = :nPK')
    Left = 568
    Top = 72
    object qryStatuscNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryStatus
    Left = 568
    Top = 104
  end
  object ER2Excel1: TER2Excel
    Titulo = 'Aniversariantes do M'#234's'
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 536
    Top = 104
  end
end
