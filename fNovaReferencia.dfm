inherited frmNovaReferencia: TfrmNovaReferencia
  Left = 465
  Top = 314
  Width = 214
  Height = 107
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Incluir nova refer'#234'ncia'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 198
    Height = 42
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 48
    Width = 52
    Height = 13
    Caption = 'Refer'#234'ncia'
  end
  inherited ToolBar1: TToolBar
    Width = 198
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object Edit1: TEdit [3]
    Left = 64
    Top = 40
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 1
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 8
    Top = 32
  end
  object usp_processo: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_PRODUTO_REF;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@cReferencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 64
    Top = 32
  end
end
