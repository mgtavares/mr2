inherited frmCaixa_EstornoDia: TfrmCaixa_EstornoDia
  Left = 288
  Top = 137
  VertScrollBar.Range = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Estorno Lan'#231'amentos Caixa - Dia'
  ClientHeight = 504
  ClientWidth = 950
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 950
    Height = 475
  end
  inherited ToolBar1: TToolBar
    Width = 950
    ButtonWidth = 108
    inherited ToolButton1: TToolButton
      Caption = 'Processar Estorno'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 108
    end
    inherited ToolButton2: TToolButton
      Left = 116
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 950
    Height = 475
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsLanctoFin
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    PopupMenu = PopupMenu1
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdLanctoFin'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtLancto'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoLancto'
        Footers = <>
        Width = 271
      end
      item
        EditButtons = <>
        FieldName = 'nValLancto'
        Footers = <>
        Width = 96
      end
      item
        EditButtons = <>
        FieldName = 'cNmFormaPagto'
        Footers = <>
        Width = 243
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'cFlgManual'
        Footers = <>
        Width = 47
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 136
    Top = 320
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084D6FF0084D6FF0084D6
      FF0084D6FF0084D6FF0084D6FF0084D6FF0084D6FF0084D6FF0084D6FF0084D6
      FF0084D6FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084D6FF00A5F7FF00A5F7
      FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7
      FF00A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084D6FF00A5F7FF00A5F7
      FF00A5F7FF00A5F7FF00A5F7FF00313131003131310031313100313131003131
      3100A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000084D6FF00A5F7FF00A5F7
      FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7
      FF00A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000084D6FF00A5F7FF00A5F7
      FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF007B7B7B007B7B
      7B00A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      8400000084000000840000000000000000000000000084D6FF00A5F7FF00A5F7
      FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7
      FF00A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000084D6FF00A5F7FF007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B00A5F7FF007B7B7B007B7B
      7B00A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000084D6FF00A5F7FF00A5F7
      FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7
      FF00A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000084D6FF00A5F7FF007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B00A5F7FF007B7B7B007B7B
      7B00A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000084D6FF00A5F7FF00A5F7
      FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7
      FF00A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF000000840000000000000000000000000084D6FF00A5F7FF007B7B
      7B007B7B7B007B7B7B007B7B7B007B7B7B007B7B7B00A5F7FF007B7B7B007B7B
      7B00A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000084D6FF00A5F7FF00A5F7
      FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7
      FF00A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000084D6FF00A5F7FF00A5F7
      FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7
      FF00A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000084D6FF00A5F7FF003131
      310031313100313131003131310031313100A5F7FF0031313100A5F7FF003131
      3100A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084D6FF00A5F7FF00A5F7
      FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7FF00A5F7
      FF00A5F7FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084D6FF0084D6FF0084D6
      FF0084D6FF0084D6FF0084D6FF0084D6FF0084D6FF0084D6FF0084D6FF0084D6
      FF0084D6FF0084D6FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF80030000FE00FFFF80030000
      FE00C00380030000000080018003000000008001800300000000800180030000
      0000800180030000000080018003000000008001800300000000800180030000
      0000800180030000000080018003000000018001800300000003800180030000
      0077C00380030000007FFFFF8003000000000000000000000000000000000000
      000000000000}
  end
  object qryLanctoFin: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT LanctoFin.nCdLanctoFin'
      '      ,LanctoFin.dDtLancto'
      '      ,LanctoFin.nValLancto'
      '      ,cFlgManual'
      '      ,cNmTipoLancto'
      '      ,(SELECT TOP 1 cNmFormaPagto '
      '          FROM FormaPagtoLanctoFin'
      
        '               INNER JOIN FormaPagto ON FormaPagto.nCdFormaPagto' +
        ' = FormaPagtoLanctoFin.nCdFormaPagto '
      '         WHERE nCdLanctoFin = LanctoFin.nCdLanctoFin'
      '         ORDER BY nValPagto DESC) cNmFormaPagto'
      '  FROM LanctoFin'
      
        '       INNER JOIN TipoLancto ON TipoLancto.nCdTipoLancto = Lanct' +
        'oFin.nCdTipoLancto'
      ' WHERE nCdResumoCaixa  = :nPK'
      '   AND dDtEstorno      IS NULL'
      '   AND nCdLanctoFinPai IS NULL')
    Left = 232
    Top = 224
    object qryLanctoFinnCdLanctoFin: TAutoIncField
      DisplayLabel = 'Lan'#231'amento'
      FieldName = 'nCdLanctoFin'
      ReadOnly = True
    end
    object qryLanctoFindDtLancto: TDateTimeField
      DisplayLabel = 'Data'
      FieldName = 'dDtLancto'
    end
    object qryLanctoFinnValLancto: TBCDField
      DisplayLabel = 'Valor Lancto.'
      FieldName = 'nValLancto'
      Precision = 12
      Size = 2
    end
    object qryLanctoFincFlgManual: TIntegerField
      DisplayLabel = 'Manual'
      FieldName = 'cFlgManual'
    end
    object qryLanctoFincNmTipoLancto: TStringField
      DisplayLabel = 'Tipo Lan'#231'amento'
      FieldName = 'cNmTipoLancto'
      Size = 50
    end
    object qryLanctoFincNmFormaPagto: TStringField
      DisplayLabel = 'Forma Liquida'#231#227'o'
      FieldName = 'cNmFormaPagto'
      ReadOnly = True
      Size = 50
    end
  end
  object dsLanctoFin: TDataSource
    DataSet = qryLanctoFin
    Left = 200
    Top = 320
  end
  object SP_ESTORNA_LANCTO_CAIXA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ESTORNA_LANCTO_CAIXA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLanctoFin'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cMotivo'
        Attributes = [paNullable]
        DataType = ftString
        Size = 30
        Value = Null
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 232
    Top = 256
  end
  object PopupMenu1: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu1Popup
    Left = 136
    Top = 224
    object ReimprimirCupomFiscal1: TMenuItem
      Caption = 'Emitir Cupom Fiscal'
      ImageIndex = 2
      OnClick = ReimprimirCupomFiscal1Click
    end
  end
  object qryLanctoVenda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1'
      '  FROM LanctoFin'
      ' WHERE nCdLanctoFin  = :nCdLanctoFin'
      '   AND nCdTipoLancto = dbo.fn_LeParametro('#39'TIPOLANCVDCX'#39')')
    Left = 168
    Top = 224
  end
  object qryBuscaDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLanctoFin  int'
      '       ,@nCdPedido     int'
      '       ,@nCdItemPedido int'
      ''
      'SET @nCdLanctoFin = :nCdLanctoFin'
      ''
      'SELECT @nCdPedido = nCdPedido'
      '  FROM Pedido'
      ' WHERE nCdLanctoFin = @nCdLanctoFin'
      ''
      'SELECT TOP 1 nCdDoctoFiscal'
      '  FROM ItemPedido'
      
        '       INNER JOIN ItemDoctoFiscal ON ItemDoctoFiscal.nCdItemPedi' +
        'do = ItemPedido.nCdItemPedido'
      ' WHERE nCdPedido       = @nCdPedido'
      '   AND nCdTipoItemPed IN (1,2,3,4,5,6,7,8,9)'
      ' ORDER BY nCdDoctoFiscal')
    Left = 136
    Top = 288
    object qryBuscaDoctoFiscalnCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
  end
  object qryItensPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLanctoFin int'
      '       ,@nCdPedido     int'
      ''
      'SET @nCdLanctoFin = :nCdLanctoFin'
      ''
      'TRUNCATE TABLE #Temp_Pedido'
      ''
      'INSERT INTO #Temp_Pedido(nCdPedido'
      '                        ,nValPedido'
      '                        ,nCdTerceiro'
      '                        ,nValProdutos)'
      '      SELECT nCdPedido'
      '            ,nValPedido'
      '            ,nCdTerceiro'
      '            ,nValProdutos'
      '        FROM Pedido'
      '       WHERE ncdlanctofin = @nCdLanctoFin'
      ''
      'SELECT *'
      '  FROM #Temp_Pedido'
      '')
    Left = 168
    Top = 256
    object qryItensPedidonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryItensPedidonValPedido: TBCDField
      FieldName = 'nValPedido'
      Precision = 12
      Size = 2
    end
    object qryItensPedidonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryItensPedidonValProdutos: TBCDField
      FieldName = 'nValProdutos'
      Precision = 12
      Size = 2
    end
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      '  IF (OBJECT_ID('#39'tempdb..#Temp_Pedido'#39') IS NULL)'
      '  BEGIN'
      ''
      '      CREATE TABLE #Temp_Pedido (nCdPedido     int'
      
        '                                ,nValPedido    decimal(12,2) def' +
        'ault 0'
      '                                ,nCdTerceiro   int'
      
        '                                ,nValProdutos  decimal(12,2) def' +
        'ault 0)'
      ''
      '  END'
      ''
      '  TRUNCATE TABLE #Temp_Pedido'
      ''
      '  IF (OBJECT_ID('#39'tempdb..#Temp_Pagtos_Estorno'#39') IS NULL)'
      '  BEGIN'
      
        '      CREATE TABLE #Temp_Pagtos_Estorno (cNmFormaPagto varchar(5' +
        '0)'
      
        '                                        ,nValPagto     decimal(1' +
        '2,2) default 0 not null'
      
        '                                        ,cFlgTEF       int      ' +
        '     default 0 not null'
      
        '                                        ,nValPedido    decimal(1' +
        '2,2) default 0 not null'
      
        '                                        ,nCdPedido     int      ' +
        '     default 0 not null'
      
        '                                        ,nValProdutos  decimal(1' +
        '2,2) default 0 not null'
      
        '                                        ,nCdTerceiro   int      ' +
        '     default 0 not null'
      
        '                                        ,nValValeMerc  decimal(1' +
        '2,2) default 0 not null'
      
        '                                        ,nValDevoluc   decimal(1' +
        '2,2) default 0 not null)'
      '  END')
    Left = 136
    Top = 256
  end
  object qryTempPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      '  TRUNCATE TABLE #Temp_Pagtos_Estorno'
      ''
      '  DECLARE @nCdLanctoFin int'
      ''
      '  SET @nCdLanctoFin =:nCdLanctoFin'
      ''
      '  INSERT INTO #Temp_Pagtos_Estorno (cNmFormaPagto'
      '                                   ,nValPagto'
      '                                   ,cFlgTEF'
      '                                   ,nValPedido'
      '                                   ,nCdPedido'
      '                                   ,nValProdutos'
      '                                   ,nCdTerceiro'
      '                                   ,nValValeMerc'
      '                                   ,nValDevoluc)'
      '  SELECT FormaPagto.cNmFormaPagto'
      '        ,isNull(FPLF.nValPagto,0) nValPagto'
      '        ,isNull(FormaPagto.cFlgTEF,0) cFlgTEF'
      '        ,Pedido.nValPedido'
      '        ,Pedido.nCdPedido'
      '        ,Pedido.nValProdutos'
      '        ,Pedido.nCdTerceiro'
      '        ,Pedido.nValValeMerc'
      '        ,Pedido.nValDevoluc'
      '    FROM FormaPagtoLanctoFin FPLF'
      
        '         INNER JOIN FormaPagto ON FormaPagto.nCdFormaPagto = FPL' +
        'F.nCdFormaPagto'
      
        '         RIGHT JOIN Pedido     ON Pedido.nCdLanctoFin      = FPL' +
        'F.nCdLanctoFin'
      '   WHERE Pedido.nCdLanctoFin = @nCdLanctoFin'
      ''
      '   SELECT *'
      '     FROM #Temp_Pagtos_Estorno')
    Left = 168
    Top = 288
    object qryTempPagtocNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryTempPagtonValPagto: TBCDField
      FieldName = 'nValPagto'
      Precision = 12
      Size = 2
    end
    object qryTempPagtocFlgTEF: TIntegerField
      FieldName = 'cFlgTEF'
    end
    object qryTempPagtonValPedido: TBCDField
      FieldName = 'nValPedido'
      Precision = 12
      Size = 2
    end
    object qryTempPagtonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryTempPagtonValProdutos: TBCDField
      FieldName = 'nValProdutos'
      Precision = 12
      Size = 2
    end
    object qryTempPagtonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTempPagtonValValeMerc: TBCDField
      FieldName = 'nValValeMerc'
      Precision = 12
      Size = 2
    end
    object qryTempPagtonValDevoluc: TBCDField
      FieldName = 'nValDevoluc'
      Precision = 12
      Size = 2
    end
  end
  object qryTempVale: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      '  DECLARE @nCdLanctoFin int'
      ''
      '  SET @nCdLanctoFin = :nCdLanctoFin'
      ''
      '  SELECT nValValeMerc'
      '        ,nValDevoluc'
      '    FROM Pedido'
      '   WHERE ncdlanctofin = @nCdLanctoFin')
    Left = 168
    Top = 320
    object qryTempValenValValeMerc: TBCDField
      FieldName = 'nValValeMerc'
      Precision = 12
      Size = 2
    end
    object qryTempValenValDevoluc: TBCDField
      FieldName = 'nValDevoluc'
      Precision = 12
      Size = 2
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 200
    Top = 256
  end
  object qryTestaValor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      '   SELECT Pedido.nValProdutos'
      '     FROM Pedido'
      '    WHERE Pedido.nCdLanctoFin = :nCdLanctoFin'
      '      AND nValProdutos       >= 0')
    Left = 200
    Top = 288
  end
  object qryTestaValePresente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cCdProduto '
      '  FROM ItemPedido'
      
        '       INNER JOIN Pedido    ON Pedido.nCdPedido       = ItemPedi' +
        'do.nCdPedido'
      
        '       INNER JOIN LanctoFin ON LanctoFin.nCdLanctoFin = Pedido.n' +
        'CdLanctoFin'
      ' WHERE LanctoFin.nCdLanctoFin = :nCdLanctoFin'
      '   AND cCdProduto <> '#39'VP'#39)
    Left = 232
    Top = 288
  end
  object qryValidaEstCredReneg: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'SELECT TOP 1 1'
      '  FROM Titulo T'
      ' WHERE nCdLanctoFin = :nCdLanctoFin'
      '   AND EXISTS (SELECT 1'
      '                 FROM TituloPropostaReneg TPR'
      
        '                      INNER JOIN PropostaReneg PR ON PR.nCdPropo' +
        'staReneg = TPR.nCdPropostaReneg '
      '                WHERE TPR.nCdTitulo = T.nCdTitulo'
      '                  AND PR.dDtCancel  IS NULL)')
    Left = 200
    Top = 224
  end
  object qryFormaPagtoLanctoFin: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT DISTINCT'
      '       cNmFormaPagto '
      '  FROM FormaPagtoLanctoFin'
      
        '       INNER JOIN FormaPagto ON FormaPagto.nCdFormaPagto = Forma' +
        'PagtoLanctoFin.nCdFormaPagto '
      ' WHERE nCdLanctoFin = :nPK')
    Left = 200
    Top = 320
    object qryFormaPagtoLanctoFincNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
  end
  object qryConsultaDoctoFiscalCFe: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT DISTINCT '
      '       DoctoFiscal.nCdDoctoFiscal'
      '  FROM ItemPedido'
      
        '       INNER JOIN Pedido          ON Pedido.nCdPedido           ' +
        '   = ItemPedido.nCdPedido'
      
        '       INNER JOIN ItemDoctoFiscal ON ItemDoctoFiscal.nCdItemPedi' +
        'do = ItemPedido.nCdItemPedido'
      
        '       INNER JOIN DoctoFiscal     ON DoctoFiscal.nCdDoctoFiscal ' +
        '   = ItemDoctoFiscal.nCdDoctoFiscal'
      ' WHERE nCdLanctoFin        = :nPK'
      '   AND DoctoFiscal.cFlgCFe = 1')
    Left = 232
    Top = 320
    object qryConsultaDoctoFiscalCFenCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
  end
  object qryTempCFe: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdLanctoFin int'
      ''
      'SET @nCdLanctoFin = :nCdLanctoFin'
      ''
      'TRUNCATE TABLE #Temp_Pedido'
      '                          '
      'INSERT INTO #Temp_Pedido (nCdPedido'
      '                         ,nValPedido'
      '                         ,nCdTerceiro)'
      '                   SELECT nCdPedido'
      '                         ,nValPedido'
      '                         ,nCdTerceiro'
      '                     FROM Pedido'
      '                    WHERE nCdLanctoFin = @nCdLanctoFin'
      '                    ORDER BY nCdPedido'
      ''
      'SELECT nCdPedido'
      '      ,nValPedido'
      '      ,nCdTerceiro'
      '  FROM #Temp_Pedido')
    Left = 264
    Top = 320
    object qryTempCFenCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryTempCFenValPedido: TBCDField
      FieldName = 'nValPedido'
      Precision = 12
      Size = 2
    end
    object qryTempCFenCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
  end
  object qryPreparaTempCFe: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Pedido'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #Temp_Pedido (nCdPedido             int'
      '                              ,nCdTerceiro           int'
      '                              ,nCdCondPagto          int'
      
        '                              ,nValProdutos          decimal(12,' +
        '2) default 0'
      
        '                              ,nValDescontoCondPagto decimal(12,' +
        '2) default 0'
      
        '                              ,nValJurosCondPagto    decimal(12,' +
        '2) default 0'
      
        '                              ,nValPedido            decimal(12,' +
        '2) default 0'
      '                              ,cNmCondPagto          varchar(50)'
      
        '                              ,nValDesconto          decimal(12,' +
        '2) default 0'
      
        '                              ,nValValeMerc          decimal(12,' +
        '2) default 0'
      
        '                              ,nValValePres          decimal(12,' +
        '2) default 0'
      
        '                              ,cFlgPromocional       int        ' +
        '   default 0'
      
        '                              ,nValDevoluc           decimal(12,' +
        '2) default 0'
      
        '                              ,nValMercadoria        decimal(12,' +
        '2) default 0'
      
        '                              ,nValAcrescimo         decimal(12,' +
        '2) default 0)'
      ''
      'END')
    Left = 264
    Top = 288
  end
  object qryTEF: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_TEF'#39') IS NULL)'
      'BEGIN'
      #9'CREATE TABLE #Temp_TEF('
      #9'cNumeroControle VARCHAR(50) NOT NULL)'
      'END'
      ''
      'TRUNCATE TABLE #Temp_TEF'
      ''
      'INSERT INTO #Temp_TEF'
      'SELECT'
      #9'cNumeroControle'
      'FROM'
      #9'TransacaoCartao'
      'WHERE nCdLanctoFin = :nCdLanctoFin'
      'AND cNumeroControle IS NOT NULL'
      ''
      'SELECT * FROM #Temp_TEF')
    Left = 136
    Top = 368
    object qryTEFcNumeroControle: TStringField
      FieldName = 'cNumeroControle'
      Size = 50
    end
  end
  object qryImpressora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNmComputador'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cModeloImpSAT'
      '      ,cPortaImpSAT'
      '      ,cPageCodImpSAT'
      '      ,cFlgTipoImpSAT'
      '  FROM ConfigAmbiente'
      '  WHERE cNmComputador = :cNmComputador'
      '')
    Left = 169
    Top = 367
    object qryImpressoracModeloImpSAT: TStringField
      FieldName = 'cModeloImpSAT'
    end
    object qryImpressoracPortaImpSAT: TStringField
      FieldName = 'cPortaImpSAT'
    end
    object qryImpressoracPageCodImpSAT: TStringField
      FieldName = 'cPageCodImpSAT'
    end
    object qryImpressoracFlgTipoImpSAT: TIntegerField
      FieldName = 'cFlgTipoImpSAT'
    end
  end
end
