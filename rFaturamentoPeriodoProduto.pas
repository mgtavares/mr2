unit rFaturamentoPeriodoProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls;

type
  TrptFaturamentoPeriodoProduto = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    Label5: TLabel;
    DataSource4: TDataSource;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DataSource5: TDataSource;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit6: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit4: TMaskEdit;
    Label2: TLabel;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    dsGrupoEconomico: TDataSource;
    MaskEdit5: TMaskEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    MaskEdit7: TMaskEdit;
    Label7: TLabel;
    DBEdit5: TDBEdit;
    MaskEdit8: TMaskEdit;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    MaskEdit9: TMaskEdit;
    Label9: TLabel;
    DBEdit7: TDBEdit;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TAutoIncField;
    qryClasseProdutocNmClasseProduto: TStringField;
    dsDepartamento: TDataSource;
    dsMarca: TDataSource;
    dsLinha: TDataSource;
    dsClasseProduto: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure MaskEdit9Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFaturamentoPeriodoProduto: TrptFaturamentoPeriodoProduto;

implementation

uses fMenu, fLookup_Padrao, rFaturamentoPeriodoProduto_view;

{$R *.dfm}

procedure TrptFaturamentoPeriodoProduto.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  iAux := frmMenu.UsuarioEmpresaAutorizada;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  MaskEdit3.SetFocus ;

end;


procedure TrptFaturamentoPeriodoProduto.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TrptFaturamentoPeriodoProduto.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TrptFaturamentoPeriodoProduto.ToolButton1Click(Sender: TObject);
var
  objRel : TrptFaturamentoPeriodoProduto_view;
begin

  objRel := TrptFaturamentoPeriodoProduto_view.Create(nil);
  Try
      Try
          objRel.usp_Relatorio.Close ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdGrupoEconomico').Value := frmMenu.ConvInteiro(MaskEdit4.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdTerceiro').Value       := frmMenu.ConvInteiro(MaskEdit6.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdDepartamento').Value   := frmMenu.ConvInteiro(MaskEdit5.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdMarca').Value          := frmMenu.ConvInteiro(MaskEdit7.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdLinha').Value          := frmMenu.ConvInteiro(MaskEdit8.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdClasse').Value         := frmMenu.ConvInteiro(MaskEdit9.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@dDtInicial').Value        := frmMenu.ConvData(MaskEdit1.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@dDtFinal').Value          := frmMenu.ConvData(MaskEdit2.Text) ;
          objRel.usp_Relatorio.Open  ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := 'Empresa : '        + Trim(MaskEdit3.Text) + ' ' + DbEdit3.Text ;
          objRel.lblFiltro2.Caption := 'Terceiro: '        + String(MaskEdit6.Text) + ' ' + DbEdit10.Text ;
          objRel.lblFiltro3.Caption := 'Grupo Economico: ' + String(MaskEdit4.Text) + ' ' + DbEdit1.Text ;
          objRel.lblFiltro4.Caption := 'Per�odo        : ' + String(MaskEdit1.Text) + ' ' + String(MaskEdit2.Text) ;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro ao criar o relat�rio');
          raise;
      end
  Finally;
      FreeAndNil(objRel);
  end;
end;


procedure TrptFaturamentoPeriodoProduto.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptFaturamentoPeriodoProduto.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptFaturamentoPeriodoProduto.MaskEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptFaturamentoPeriodoProduto.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close ;
  
  PosicionaQuery(qryGrupoEconomico, MaskEdit4.Text) ;
end;

procedure TrptFaturamentoPeriodoProduto.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryDepartamento.Close ;

  If (Trim(MaskEdit5.Text) <> '') then
  begin

    PosicionaQuery(qryDepartamento, Maskedit5.Text) ;

  end ;

end;

procedure TrptFaturamentoPeriodoProduto.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryMarca.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin

    PosicionaQuery(qryMarca, Maskedit7.Text) ;

  end ;

end;

procedure TrptFaturamentoPeriodoProduto.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(45);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptFaturamentoPeriodoProduto.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptFaturamentoPeriodoProduto.MaskEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLinha, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptFaturamentoPeriodoProduto.MaskEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
        begin
            Maskedit9.Text := IntToStr(nPK) ;
            PosicionaQuery(qryClasseProduto, IntToStr(nPK));
        end ;

    end ;

  end ;

end;


procedure TrptFaturamentoPeriodoProduto.MaskEdit8Exit(Sender: TObject);
begin
  inherited;
  qryLinha.Close ;

  If (Trim(MaskEdit8.Text) <> '') then
  begin

    PosicionaQuery(qryLinha, Maskedit8.Text) ;

  end ;

end;

procedure TrptFaturamentoPeriodoProduto.MaskEdit9Exit(Sender: TObject);
begin
  inherited;
  qryClasseProduto.Close ;

  If (Trim(MaskEdit9.Text) <> '') then
  begin

    PosicionaQuery(qryClasseProduto, Maskedit9.Text) ;

  end ;

end;

initialization
     RegisterClass(TrptFaturamentoPeriodoProduto) ;

end.
