unit fColecaoProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmColecaoProduto = class(TfrmCadastro_Padrao)
    qryMasternCdColecao: TIntegerField;
    qryMastercNmColecao: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmColecaoProduto: TfrmColecaoProduto;

implementation

{$R *.dfm}

procedure TfrmColecaoProduto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'COLECAO' ;
  nCdTabelaSistema  := 46 ;
  nCdConsultaPadrao := 52 ;
end;

procedure TfrmColecaoProduto.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

initialization
    RegisterClass(TfrmColecaoProduto) ;
    
end.
