unit fTabelaJuros;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB;

type
  TfrmTabelaJuros = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryMaster: TADOQuery;
    qryMasternCdTabelaJuros: TAutoIncField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdEspTit: TIntegerField;
    qryMasterdDtValidade: TDateTimeField;
    qryMasteriDiasCarencia: TIntegerField;
    qryMasternPercMulta: TBCDField;
    qryMasternPercJuroDia: TBCDField;
    qryMastercNmEspTit: TStringField;
    dsMaster: TDataSource;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryMasternPercDescAntecipDia: TBCDField;
    qryMasteriDiasMinDescAntecip: TIntegerField;
    qryMasternPercMaxDescAntecip: TBCDField;
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTabelaJuros: TfrmTabelaJuros;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmTabelaJuros.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMastercNmEspTit.Value = '') then
  begin
      MensagemAlerta('Informe a esp�cie de t�tulo.') ;
      abort ;
  end ;

  if (qryMasterdDtValidade.asString = '') then
  begin
      MensagemAlerta('Informe a data final da vig�ncia da tabela.') ;
      abort ;
  end ;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;

  if (qryMaster.State = dsInsert) then
      qryMasternCdTabelaJuros.Value := frmMenu.fnProximoCodigo('TABELAJUROS');
      
  inherited;


end;

procedure TfrmTabelaJuros.qryMasterCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryMastercNmEspTit.Value = '') then
  begin
  
      qryEspTit.Close ;
      PosicionaQuery(qryEspTit, qryMasternCdEspTit.AsString) ;

      if not qryEspTit.Eof then
          qryMastercNmEspTit.Value := qryEspTitcNmEspTit.Value ;

  end ;
  
end;

procedure TfrmTabelaJuros.FormShow(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryMaster, IntToStr(frmMenu.nCdEmpresaAtiva)) ;

  DBGridEh1.Align := alClient ;
  
end;

procedure TfrmTabelaJuros.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsBrowse) then
             qryMaster.Edit ;

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(10,'cTipoMov = ' + Chr(39) + 'R' + Chr(39));

            If (nPK > 0) then
            begin
                qryMasternCdEspTit.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmTabelaJuros) ;

end.
