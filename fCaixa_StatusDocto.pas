unit fCaixa_StatusDocto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, GridsEh, DBGridEh, Menus, cxPC, cxControls,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmCaixa_StatusDocto = class(TfrmProcesso_Padrao)
    qryCheque: TADOQuery;
    qryStatusDocto: TADOQuery;
    qryStatusDoctonCdStatusDocto: TIntegerField;
    qryStatusDoctocNmStatusDocto: TStringField;
    qryCartao: TADOQuery;
    qryCartaonCdTransacaoCartao: TAutoIncField;
    qryCartaodDtTransacao: TDateTimeField;
    qryCartaoiNrCartao: TLargeintField;
    qryCartaoiNrDocto: TIntegerField;
    qryCartaonCdOperadoraCartao: TIntegerField;
    qryCartaonValTransacao: TBCDField;
    qryCrediario: TADOQuery;
    qryCrediarionCdCrediario: TAutoIncField;
    qryCrediariodDtVenda: TDateTimeField;
    qryCrediarionCdTerceiro: TIntegerField;
    qryCrediarionValCrediario: TBCDField;
    qryCrediarionCdStatusDocto: TIntegerField;
    qryCartaonCdStatusDocto: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceirocNmTerceiro: TStringField;
    dsCheque: TDataSource;
    dsCartao: TDataSource;
    dsCrediario: TDataSource;
    qryChequecNmStatusDocto: TStringField;
    qryCartaocNmStatusDocto: TStringField;
    qryCartaocNmOperadora: TStringField;
    qryCrediariocNmTerceiro: TStringField;
    qryCrediariocNmStatusDocto: TStringField;
    qryChequenCdCheque: TAutoIncField;
    qryChequenCdBanco: TIntegerField;
    qryChequecConta: TStringField;
    qryChequecCNPJCPF: TStringField;
    qryChequeiNrCheque: TIntegerField;
    qryChequecDigito: TStringField;
    qryChequecAgencia: TStringField;
    qryChequedDtEmissao: TDateTimeField;
    qryChequenCdStatusDocto: TIntegerField;
    qryChequenValCheque: TBCDField;
    qryOperadora: TADOQuery;
    qryOperadoracNmOperadoraCartao: TStringField;
    qryChequenCdLanctoFin: TIntegerField;
    qryCartaonCdLanctoFin: TIntegerField;
    qryCrediarionCdLanctoFin: TIntegerField;
    qryLanctoMan: TADOQuery;
    dsLanctoMan: TDataSource;
    qryLanctoMannCdLanctoFin: TAutoIncField;
    qryLanctoMandDtLancto: TDateTimeField;
    qryLanctoMannCdTipoLancto: TIntegerField;
    qryLanctoMancNmTipoLancto: TStringField;
    qryLanctoMannValLancto: TBCDField;
    PopupMenu1: TPopupMenu;
    ImprimirComprovante1: TMenuItem;
    qryCartaoiParcelas: TIntegerField;
    PopupMenu2: TPopupMenu;
    AlterarOperadora1: TMenuItem;
    qryCartaonCdCondPagtoCartao: TIntegerField;
    qryCondPagto: TADOQuery;
    qryCondPagtonPercAcrescimo: TBCDField;
    qryLanctoFin: TADOQuery;
    qryLanctoFincFlgLiqCrediario: TIntegerField;
    SP_ALTERA_CONDICAO_TRANSACAO_CARTAO: TADOStoredProc;
    qryTipoOperacao: TADOQuery;
    qryTipoOperacaocFlgTipoOperacao: TStringField;
    qryCondPagtonCdTabTipoFormaPagto: TIntegerField;
    qryCartaoSituacaoAtual: TADOQuery;
    qryCartaoSituacaoAtualcNmOperadoraCartao: TStringField;
    qryCartaoSituacaoAtualcNmCondPagto: TStringField;
    qryCartaoSituacaoAtualcNmFormaPagto: TStringField;
    qryCartaoiNrAutorizacao: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    cxTabSheet4: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    DBGridEh4: TDBGridEh;
    qryChequedDtDeposito: TDateTimeField;
    qryCartaocNrDoctoNSU: TStringField;
    qryCartaocNrAutorizacao: TStringField;
    procedure qryCrediarioCalcFields(DataSet: TDataSet);
    procedure qryCartaoCalcFields(DataSet: TDataSet);
    procedure qryChequeCalcFields(DataSet: TDataSet);
    procedure qryChequeBeforePost(DataSet: TDataSet);
    procedure qryCartaoBeforePost(DataSet: TDataSet);
    procedure qryCrediarioBeforePost(DataSet: TDataSet);
    procedure ImprimirComprovante1Click(Sender: TObject);
    procedure AlterarOperadora1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
    bAlteracao : boolean ;
  end;

var
  frmCaixa_StatusDocto: TfrmCaixa_StatusDocto;

implementation

uses fCaixa_LanctoMan, fLookup_Padrao, fMenu, fPdvFormaPagto,
  fPdvCondPagto, fCaixa_DadosCartao, fCaixa_AlteraCartao;

{$R *.dfm}

procedure TfrmCaixa_StatusDocto.qryCrediarioCalcFields(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryTerceiro, qryCrediarionCdTerceiro.AsString) ;
  qryCrediariocNmTerceiro.Value := qryTerceirocNmTerceiro.Value ;

  PosicionaQuery(qryStatusDocto, qryCrediarionCdStatusDocto.AsString) ;
  qryCrediariocNmStatusDocto.Value := qryStatusDoctocNmStatusDocto.Value ;

end;

procedure TfrmCaixa_StatusDocto.qryCartaoCalcFields(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryOperadora, qryCartaonCdOperadoraCartao.AsString) ;
  qryCartaocNmOperadora.Value := qryOperadoracNmOperadoraCartao.Value ;

  PosicionaQuery(qryStatusDocto, qryCartaonCdStatusDocto.AsString) ;
  qryCartaocNmStatusDocto.Value := qryStatusDoctocNmStatusDocto.Value ;

end;

procedure TfrmCaixa_StatusDocto.qryChequeCalcFields(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryStatusDocto, qryChequenCdStatusDocto.AsString) ;
  qryChequecNmStatusDocto.Value := qryStatusDoctocNmStatusDocto.Value ;

end;

procedure TfrmCaixa_StatusDocto.qryChequeBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryChequenCdStatusDocto.Value <> 1) and (qryChequenCdStatusDocto.Value <> 2) then
  begin
      MensagemAlerta('Status inv�lido. Utilize : ' + #13#13 +'1 - OK'+#13#13 + '2 - EXTRAVIADO'+#13#13);
      abort ;
  end ;

  bAlteracao := true ;

end;

procedure TfrmCaixa_StatusDocto.qryCartaoBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryCartaonCdStatusDocto.Value <> 1) and (qryCartaonCdStatusDocto.Value <> 2) then
  begin
      MensagemAlerta('Status inv�lido. Utilize : ' + #13#13 +'1 - OK'+#13#13 + '2 - EXTRAVIADO'+#13#13);
      abort ;
  end ;

  bAlteracao := true ;

end;

procedure TfrmCaixa_StatusDocto.qryCrediarioBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryCrediarionCdStatusDocto.Value <> 1) and (qryCrediarionCdStatusDocto.Value <> 2) then
  begin
      MensagemAlerta('Status inv�lido. Utilize : ' + #13#13 +'1 - OK'+#13#13 + '2 - EXTRAVIADO'+#13#13);
      abort ;
  end ;

  bAlteracao := true ;

end;

procedure TfrmCaixa_StatusDocto.ImprimirComprovante1Click(Sender: TObject);
begin
  inherited;

  if not qryLanctoMan.eof then
  begin

      case MessageDlg('Confirma a impress�o do comprovante ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo:exit ;
      end ;

      frmCaixa_LanctoMan.ImprimirComprovante(qryLanctoMannCdLanctoFin.Value);

  end ;

end;

procedure TfrmCaixa_StatusDocto.AlterarOperadora1Click(Sender: TObject);
var
    nPK : Integer ;
    nCdFormapagto, nCdCondPagto : integer ;

    objPDVFormaPagto      : TfrmPdvFormaPagto ;
    objPDVCondPagto       : TfrmPdvCondPagto ;
    objCaixa_DadosCartao  : TfrmCaixa_DadosCartao ;
    objCaixa_AlteraCartao : TfrmCaixa_AlteraCartao ;

begin
  inherited;

  

  if not qryCartao.Eof then
  begin

    objPDVFormaPagto      := TfrmPdvFormaPagto.Create( Self ) ;
    objPDVCondPagto       := TfrmPdvCondPagto.Create( Self ) ;
    objCaixa_DadosCartao  := TfrmCaixa_DadosCartao.Create( Self ) ;
    objCaixa_AlteraCartao := TfrmCaixa_AlteraCartao.Create( Self ) ;

    objPDVFormaPagto.cFlgLiqCrediario := 0 ;
    objPDVFormaPagto.bSomenteCartao   := True ;
    nCdFormaPagto                     := objPDVFormaPagto.SelecionaFormaPagto() ;
    objPDVFormaPagto.bSomenteCartao   := False ;

    try

        if (nCdFormaPagto <= 0) then
            abort ;

        {-- passa os parametros para a tela de consulta de condi��es --}
        objPDVCondPagto.nCdLoja               := frmMenu.nCdLojaAtiva;
        objPDVCondPagto.nCdFormaPagto         := nCdFormaPagto       ;
        objPDVCondPagto.cFlgLiqCrediario      := 0    ;

        objPDVCondPagto.edtValorPagar.Value   := qryCartaonValTransacao.Value ;
        objPDVCondPagto.edtSaldo.Value        := qryCartaonValTransacao.Value ;
        objPDVCondPagto.cPermDesconto         := 'N' ;
        objPDVCondPagto.edtValorPagar.Enabled := False ;

        {-- Processa a consulta --}
        nCdCondPagto  := objPDVCondPagto.SelecionaCondPagto() ;

        {-- se o operador desistiu da consulta, retorna por aqui --}
        if (nCdCondPagto <= 0) then
            abort ;

        qryCondPagto.Close ;
        PosicionaQuery(qryCondPagto, IntToStr(nCdCondPagto)) ;

        // debito
        if (qryCondPagtonCdTabTipoFormaPagto.Value = 3) then
            objCaixa_DadosCartao.cTipoOperacao       := 'D'
        else if (qryCondPagtonCdTabTipoFormaPagto.Value = 4) then
            objCaixa_DadosCartao.cTipoOperacao       := 'C' ;

        objCaixa_DadosCartao.edtValorPagar.Value := qryCartaonValTransacao.Value ;
        objCaixa_DadosCartao.ShowModal;

        if (Trim(objCaixa_DadosCartao.edtNrDocto.Text) = '') then
        begin
            MensagemErro('Processo cancelado.') ;
            exit ;
        end ;

        qryCartaoSituacaoAtual.Close ;
        PosicionaQuery(qryCartaoSituacaoAtual, qryCartaonCdTransacaoCartao.asString) ;

        objCaixa_AlteraCartao.edtFormaPagto_Atual.Text  := qryCartaoSituacaoAtualcNmFormaPagto.Value;
        objCaixa_AlteraCartao.edtCondPagto_Atual.Text   := qryCartaoSituacaoAtualcNmCondPagto.Value;
        objCaixa_AlteraCartao.edtOperadora_Atual.Text   := qryCartaoSituacaoAtualcNmOperadoraCartao.Value ;
        objCaixa_AlteraCartao.edtDocto_Atual.Text       := qryCartaocNrDoctoNSU.Text;
        objCaixa_AlteraCartao.edtAutorizacao_Atual.Text := qryCartaocNrAutorizacao.Text ;

        objCaixa_AlteraCartao.edtFormaPagto_Nova.Text  := objPDVFormaPagto.qryFormaPagtocNmFormaPagto.Value;
        objCaixa_AlteraCartao.edtCondPagto_Nova.Text   := objPDVCondPagto.qryCondPagtocNmCondPagto.Value;
        objCaixa_AlteraCartao.edtOperadora_Nova.Text   := objCaixa_DadosCartao.edtNmOperadora.Text ;
        objCaixa_AlteraCartao.edtDocto_Novo.Text       := objCaixa_DadosCartao.edtNrDocto.Text;
        objCaixa_AlteraCartao.edtAutorizacao_Nova.Text := objCaixa_DadosCartao.edtNrAutorizacao.Text;

        if (objCaixa_AlteraCartao.ShowModal = MrNo) then
            exit ;

        frmMenu.Connection.BeginTrans;

        try
            SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Close ;
            SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@nCdTransacaoCartao').Value := qryCartaonCdTransacaoCartao.Value ;
            SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@nCdCondPagto').Value       := nCdCondPagto ;
            SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@nCdOperadoraCartao').Value := strToint(objCaixa_DadosCartao.edtnCdOperadoraCartao.Text) ;
            SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@iNrDocto').Value           := strToint(frmMenu.RetNumeros(objCaixa_DadosCartao.edtNrDocto.Text)) ;
            SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@iNrAutorizacao').Value     := strToint(frmMenu.RetNumeros(objCaixa_DadosCartao.edtNrAutorizacao.Text)) ;
            SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@cNrDoctoNSU').Value        := objCaixa_DadosCartao.edtNrDocto.Text ;
            SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.Parameters.ParamByName('@cNrAutorizacao').Value     := objCaixa_DadosCartao.edtNrAutorizacao.Text ;
            SP_ALTERA_CONDICAO_TRANSACAO_CARTAO.ExecProc;
        except
            frmMenu.Connection.RollbackTrans;
            MensagemErro('Erro no processamento.') ;
            raise ;
        end ;

        frmMenu.Connection.CommitTrans;

        bAlteracao := True ;

        ShowMessage('Processamento realizado com sucesso.') ;

        qryCartao.Close ;
        qryCartao.Open ;

    finally

        freeAndNil( objPDVFormaPagto ) ;
        freeAndNil( objPDVCondPagto ) ;
        freeAndNil( objCaixa_DadosCartao ) ;
        freeAndNil( objCaixa_AlteraCartao ) ;

    end ;

  end ;
  
end;

procedure TfrmCaixa_StatusDocto.FormShow(Sender: TObject);
begin
  inherited;

  cxPagecontrol1.ActivePageIndex := 0 ;
end;

procedure TfrmCaixa_StatusDocto.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (DataCol = 2) and (qryChequedDtDeposito.Value < Date) then
  begin
      DBGridEh1.Canvas.Brush.Color := clRed;
      DBGridEh1.Canvas.Font.Color  := clWhite ;
  end ;

  DBGridEh1.Canvas.FillRect(Rect);
  DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);

end;

end.
