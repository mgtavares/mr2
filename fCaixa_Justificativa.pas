unit fCaixa_Justificativa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls;

type
  TfrmCaixa_Justificativa = class(TfrmProcesso_Padrao)
    MemoJustificativa: TMemo;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_Justificativa: TfrmCaixa_Justificativa;
  iTamanhoMinimo : integer ;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmCaixa_Justificativa.FormShow(Sender: TObject);
begin
  inherited;

  iTamanhoMinimo := strToInt(frmMenu.LeParametro('MINCARJUSVDARES')) ;
  
end;

procedure TfrmCaixa_Justificativa.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Length(StringReplace(memoJustificativa.Text,' ','',[rfReplaceAll,rfIgnoreCase])) < iTamanhoMinimo) then
  begin
      MensagemAlerta('O texto da justificativa � pequeno demais, favor detalhar mais a justificativa.') ;
      Abort ;
  end ;

  if (MessageDlg('Confirma a justificativa ?',mtConfirmation,[mbYes,mbNo],0) = MRYES) then
      close ;
end;

procedure TfrmCaixa_Justificativa.ToolButton2Click(Sender: TObject);
begin
  MemoJustificativa.Clear;
  inherited;

end;

end.
