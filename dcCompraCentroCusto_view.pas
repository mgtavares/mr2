unit dcCompraCentroCusto_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, PivotMap_SRC, PivotCube_SRC, PivotGrid_SRC, PivotToolBar_SRC;

type
  TdcmCompraCentroCusto_view = class(TfrmProcesso_Padrao)
    ADODataSet1: TADODataSet;
    PVRowToolBar1: TPVRowToolBar;
    PVMeasureToolBar1: TPVMeasureToolBar;
    PVColToolBar1: TPVColToolBar;
    PivotCube1: TPivotCube;
    PivotMap1: TPivotMap;
    ADODataSet1Ano: TDateTimeField;
    ADODataSet1Mes: TDateTimeField;
    ADODataSet1cNmCC1: TStringField;
    ADODataSet1cNmCC2: TStringField;
    ADODataSet1cNmCC: TStringField;
    ADODataSet1nValor: TBCDField;
    PivotGrid1: TPivotGrid;
    ADODataSet1cNmTerceiro: TStringField;
    ADODataSet1cNmProduto: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmCompraCentroCusto_view: TdcmCompraCentroCusto_view;

implementation

{$R *.dfm}

end.
