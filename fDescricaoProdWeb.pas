unit fDescricaoProdWeb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, cxPC, cxControls, OleCtrls, SHDocVw,Activex, MsHtml, Buttons,  ShellAPI,
  DB, ADODB, Mask, ER2Lookup, DBCtrls, DBGridEhGrouping, ToolCtrlsEh,
  GridsEh, DBGridEh;
const
  IDM_MARCADOR = 2184;
  IDM_MARCADOR_LISTA = 2185;
  IDM_OUTDENT = 2187;
  IDM_INDENT = 2186;
  IDM_ALINHARESQ = 59;
  IDM_CENTRALIZAR = 57;
  IDM_ALINHADIR = 60;
  IDM_IMAGEM = 2168;
  IDM_LINHAHORIZ = 2150;
  IDM_RECORTAR = 16;
  IDM_COPIAR = 15;
  IDM_COLAR = 26;
  IDM_HYPERLINK = 2124;
  IDM_DESFAZER = 43;

type
  TfrmDescricaoProdWeb = class(TfrmProcesso_Padrao)
    cdColor: TColorDialog;
    GroupBox1: TGroupBox;
    qryProduto: TADOQuery;
    er2LkpProduto: TER2LookupMaskEdit;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryProdutonCdIdExternoWeb: TIntegerField;
    qryProdutocDescricaoWeb: TMemoField;
    DBEdit1: TDBEdit;
    dsProduto: TDataSource;
    DBEdit2: TDBEdit;
    cxPageControl1: TcxPageControl;
    Label3: TLabel;
    Label1: TLabel;
    cxPageControl2: TcxPageControl;
    cxTabSheet3: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryTabFichaTecnicaProduto: TADOQuery;
    qryTabFichaTecnicaProdutonCdTabFichaTecnicaProduto: TIntegerField;
    qryTabFichaTecnicaProdutonCdProduto: TIntegerField;
    qryTabFichaTecnicaProdutonCdTabFichaTecnica: TIntegerField;
    qryTabFichaTecnicaProdutocDescricao: TStringField;
    qryTabFichaTecnicaProdutocNmTabFichaTecnica: TStringField;
    dsTabFichaTecnicaProduto: TDataSource;
    qryTabFichaTecnica: TADOQuery;
    qryTabFichaTecnicacNmTabFichaTecnica: TStringField;
    qryTabFichaTecnicanCdTabFichaTecnica: TIntegerField;
    dsTabFichaTecnica: TDataSource;
    tabDescricaoWeb: TcxTabSheet;
    DBMemo1: TDBMemo;
    procedure FormCreate(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryProdutoAfterScroll(DataSet: TDataSet);
    procedure qryTabFichaTecnicaProdutoBeforePost(DataSet: TDataSet);
    procedure qryTabFichaTecnicaProdutoCalcFields(DataSet: TDataSet);
    procedure qryProdutoAfterClose(DataSet: TDataSet);

  private
    { Private declarations }
  public
    { Public declarations }
    cHTMLOriginal : String;
    BtFechar      : Integer;
  end;

var
  frmDescricaoProdWeb : TfrmDescricaoProdWeb;
  HTMLDocumento       : IHTMLDocument2;

implementation

{$R *.dfm}

uses
  fMenu, fLookup_Padrao;

function GetIEHandle(WebBrowser: TWebbrowser; ClassName: string): HWND;
var
  hwndChild, hwndTmp: HWND;
  oleCtrl: TOleControl;
  szClass: array [0..255] of char;
begin
  oleCtrl := WebBrowser;
  hwndTmp := oleCtrl.Handle;

  while (true) do
  begin
    hwndChild := GetWindow(hwndTmp, GW_CHILD);
    GetClassName(hwndChild, szClass, SizeOf(szClass));
    if (string(szClass)=ClassName) then
    begin
      Result :=hwndChild;
      Exit;
    end;
    hwndTmp := hwndChild;
  end;

  Result := 0;
end;

procedure TfrmDescricaoProdWeb.FormCreate(Sender: TObject);
begin
  inherited;

  BtFechar := 0;
  cxPageControl1.ActivePage := tabDescricaoWeb;
end;

procedure TfrmDescricaoProdWeb.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (qryProduto.IsEmpty) then
      Exit;

  if (MessageDLG('Confirma atualiza��o do cadastro ?',mtConfirmation,[mbYes,mbNo],0) =  mrYes) then
  begin
      qryProduto.Post;
      qryProduto.Close;

      er2LkpProduto.Clear;
      er2LkpProduto.SetFocus;
  end;
end;

procedure TfrmDescricaoProdWeb.DBGridEh2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

    case (Key) of
        VK_F4 : begin
                    if (qryTabFichaTecnicaProduto.State = dsBrowse) then
                        qryTabFichaTecnicaProduto.Edit ;

                    if (qryTabFichaTecnicaProduto.State in [dsInsert,dsEdit]) then
                    begin
                        if (DBGridEh2.Columns[DBGridEh2.Col-1].FieldName = 'nCdTabFichaTecnica') then
                        begin
                            nPK := frmLookup_Padrao.ExecutaConsulta(768);

                            if (nPK > 0) then
                                qryTabFichaTecnicaProdutonCdTabFichaTecnica.Value := nPK ;
                        end;
                    end;
                end;
    end;
end;

procedure TfrmDescricaoProdWeb.qryProdutoAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryTabFichaTecnicaProduto.Close;

  PosicionaQuery(qryTabFichaTecnicaProduto,qryProdutonCdProduto.AsString);
end;

procedure TfrmDescricaoProdWeb.qryTabFichaTecnicaProdutoBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryTabFichaTecnica.IsEmpty) then
  begin
      MensagemAlerta('C�digo da ficha t�cnica inv�lida.');
      Abort;
  end;

  if (Trim(qryTabFichaTecnicaProdutocDescricao.Value) = '') then
  begin
      MensagemAlerta('Descri��o da ficha t�cnica deve ser preenchido.');
      Abort;
  end;

  inherited;

  qryTabFichaTecnicaProdutonCdTabFichaTecnicaProduto.Value := frmMenu.fnProximoCodigo('TABFICHATECNICAPRODUTO');
  qryTabFichaTecnicaProdutonCdProduto.Value                := qryProdutonCdProduto.Value;
end;

procedure TfrmDescricaoProdWeb.qryTabFichaTecnicaProdutoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryTabFichaTecnica,qryTabFichaTecnicaProdutonCdTabFichaTecnica.AsString);
  qryTabFichaTecnicaProdutocNmTabFichaTecnica.Value := qryTabFichaTecnicacNmTabFichaTecnica.Value;
end;

procedure TfrmDescricaoProdWeb.qryProdutoAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryTabFichaTecnicaProduto.Close;
end;

initialization
  RegisterClass(TfrmDescricaoProdWeb);
end.
