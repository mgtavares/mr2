inherited frmPedidoComercial_PreDistribuicao: TfrmPedidoComercial_PreDistribuicao
  Left = 285
  Top = 201
  Width = 813
  Height = 240
  BorderIcons = [biSystemMenu]
  Caption = 'Pr'#233'-Distribui'#231#227'o de Produtos'
  OldCreateOrder = True
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 797
    Height = 173
    Visible = False
  end
  inherited ToolBar1: TToolBar
    Width = 797
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object gridItemGrade: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 797
    Height = 173
    Align = alClient
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsGrade
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghDialogFind, dghColumnResize, dghColumnMove, dghExtendVertLines]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    VertScrollBar.VisibleMode = sbNeverShowEh
    OnCellClick = gridItemGradeCellClick
    OnColEnter = gridItemGradeColEnter
    OnColExit = gridItemGradeColExit
    OnDrawColumnCell = gridItemGradeDrawColumnCell
    OnKeyDown = gridItemGradeKeyDown
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 336
    Top = 48
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      
        '     ,'#39'LOJA '#39' + dbo.fn_ZeroEsquerda(convert(varchar(10),nCdLoja)' +
        ',3) as cNmLoja'
      ' FROM LojaCentroDistribuicao L'
      'WHERE EXISTS (SELECT 1'
      '                FROM CentroDistribuicao CD'
      
        '               WHERE L.nCdCentroDistribuicao  = CD.nCdCentroDist' +
        'ribuicao'
      '                 AND nCdLocalEstEmpenhoPadrao = :nPK)'
      'ORDER BY nCdLoja')
    Left = 240
    Top = 80
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      ReadOnly = True
      Size = 15
    end
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
  end
  object dsGrade: TDataSource
    DataSet = cdsGrade
    Left = 304
    Top = 80
  end
  object qryBuscaQtdeDistrib: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdItemPedido'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdLoja'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ISNULL(nQtdeDistrib,0) nQtdeDistrib'
      '  FROM ItemPedidoPreDistribuido'
      ' WHERE nCdItemPedido = :nCdItemPedido'
      '   AND nCdLoja       = :nCdLoja')
    Left = 272
    Top = 80
    object qryBuscaQtdeDistribnQtdeDistrib: TIntegerField
      FieldName = 'nQtdeDistrib'
    end
  end
  object qryInsUpDel: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdItemPedido'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdLoja'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'nQtdeDistrib'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdItemPedidoPreDistribuido int'
      '       ,@nCdItemPedido               int'
      '       ,@nCdLoja                     int'
      '       ,@nQtdeDistrib                int'
      '       '
      'SET @nCdItemPedido = :nCdItemPedido'
      'SET @nCdLoja       = :nCdLoja'
      'SET @nQtdeDistrib  = :nQtdeDistrib'
      ''
      'IF (@nQtdeDistrib = 0)'
      'BEGIN'
      '    DELETE'
      '      FROM ItemPedidoPreDistribuido'
      '     WHERE nCdItemPedido = @nCdItemPedido'
      '       AND nCdLoja       = @nCdLoja'
      'END'
      ''
      'ELSE'
      'BEGIN'
      '    UPDATE ItemPedidoPreDistribuido'
      '       SET nQtdeDistrib  = @nQtdeDistrib'
      '     WHERE nCdItemPedido = @nCdItemPedido'
      '       AND nCdLoja       = @nCdLoja'
      'END'
      ''
      ''
      'IF (@@ROWCOUNT = 0)'
      'BEGIN'
      ''
      '    IF (@nQtdeDistrib = 0)'
      '        RETURN'
      ''
      '    EXEC usp_ProximoID '#39'ITEMPEDIDOPREDISTRIBUIDO'#39
      '                      ,@nCdItemPedidoPreDistribuido OUTPUT'
      ''
      
        '    INSERT INTO ItemPedidoPreDistribuido(nCdItemPedidoPreDistrib' +
        'uido'
      '                                        ,nCdItemPedido'
      '                                        ,nCdLoja'
      '                                        ,nQtdeDistrib)'
      '         VALUES(@nCdItemPedidoPreDistribuido'
      '               ,@nCdItemPedido'
      '               ,@nCdLoja'
      '               ,@nQtdeDistrib)'
      'END')
    Left = 208
    Top = 48
  end
  object cdsGrade: TClientDataSet
    Aggregates = <>
    Params = <>
    BeforePost = cdsGradeBeforePost
    BeforeCancel = cdsGradeBeforeCancel
    Left = 304
    Top = 48
  end
  object qryGrade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdItemPedido'
      '      ,iTamanho'
      '  FROM ItemPedido IP (NOLOCK)'
      '       INNER JOIN Produto P ON P.nCdProduto = IP.nCdProduto'
      ' WHERE IP.nCdItemPedidoPai = :nPK'
      ' ORDER BY iTamanho')
    Left = 240
    Top = 48
    object qryGradeiTamanho: TIntegerField
      FieldName = 'iTamanho'
    end
    object qryGradenCdItemPedido: TIntegerField
      FieldName = 'nCdItemPedido'
    end
  end
  object qryBuscaTotais: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'iTipoValor'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdItemPedido'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @iTipoValor int'
      ''
      
        'SET @iTipoValor = :iTipoValor -- 1 = Qtd. Ped | 2 = Qtd. Distrib' +
        ' | 3 = Saldo'
      ''
      'SELECT CASE WHEN (@iTipoValor = 1) THEN nQtdePed'
      
        '            WHEN (@iTipoValor = 2) THEN ISNULL((SELECT SUM(nQtde' +
        'Distrib)'
      
        '                                                  FROM ItemPedid' +
        'oPreDistribuido IPPD '
      
        '                                                 WHERE IPPD.nCdI' +
        'temPedido = IP.nCdItemPedido),0)'
      
        '            WHEN (@iTipoValor = 3) THEN nQtdePed - ISNULL((SELEC' +
        'T SUM(nQtdeDistrib)'
      
        '                                                             FRO' +
        'M ItemPedidoPreDistribuido IPPD '
      
        '                                                            WHER' +
        'E IPPD.nCdItemPedido = IP.nCdItemPedido),0)'
      '       END as nQtde'
      '  FROM ItemPedido IP (NOLOCK)'
      ' WHERE nCdItemPedido = :nCdItemPedido')
    Left = 272
    Top = 48
    object qryBuscaTotaisnQtde: TBCDField
      FieldName = 'nQtde'
      ReadOnly = True
      Precision = 15
    end
  end
end
