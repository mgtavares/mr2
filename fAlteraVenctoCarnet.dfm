inherited frmAlteraVenctoCarnet: TfrmAlteraVenctoCarnet
  Left = 31
  Top = 168
  Caption = 'Altera'#231#227'o Vencimento Carnet'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 32
    Top = 40
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Carnet'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 89
    Top = 64
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [3]
    Left = 43
    Top = 112
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Compra'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [4]
    Left = 243
    Top = 112
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data '#218'lt. Vencto'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [5]
    Left = 74
    Top = 88
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
    FocusControl = DBEdit6
  end
  object Label9: TLabel [6]
    Left = 234
    Top = 136
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data de Quita'#231#227'o'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [7]
    Left = 18
    Top = 160
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cond. Pagamento'
    FocusControl = DBEdit10
  end
  object Label12: TLabel [8]
    Left = 12
    Top = 136
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Total Compra'
    FocusControl = DBEdit12
  end
  inherited ToolBar2: TToolBar
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
    inherited ToolButton50: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [10]
    Tag = 1
    Left = 112
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdCrediario'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [11]
    Tag = 1
    Left = 112
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [12]
    Tag = 1
    Left = 184
    Top = 56
    Width = 481
    Height = 19
    DataField = 'cNmLoja'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [13]
    Tag = 1
    Left = 112
    Top = 104
    Width = 113
    Height = 19
    DataField = 'dDtVenda'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [14]
    Tag = 1
    Left = 328
    Top = 104
    Width = 113
    Height = 19
    DataField = 'dDtUltVencto'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [15]
    Tag = 1
    Left = 112
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [16]
    Tag = 1
    Left = 184
    Top = 80
    Width = 481
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit9: TDBEdit [17]
    Tag = 1
    Left = 328
    Top = 128
    Width = 113
    Height = 19
    DataField = 'dDtLiq'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit10: TDBEdit [18]
    Tag = 1
    Left = 112
    Top = 152
    Width = 65
    Height = 19
    DataField = 'nCdCondPagto'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit11: TDBEdit [19]
    Tag = 1
    Left = 184
    Top = 152
    Width = 481
    Height = 19
    DataField = 'cNmCondPagto'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit12: TDBEdit [20]
    Tag = 1
    Left = 112
    Top = 128
    Width = 89
    Height = 19
    DataField = 'nValCrediario'
    DataSource = dsMaster
    TabOrder = 11
  end
  object cxPageControl1: TcxPageControl [21]
    Left = 16
    Top = 184
    Width = 649
    Height = 321
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 12
    ClientRectBottom = 317
    ClientRectLeft = 4
    ClientRectRight = 645
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Parcelas'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 641
        Height = 293
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataSource = dsTitulo
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        FooterRowCount = 1
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDrawColumnCell = DBGridEh1DrawColumnCell
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTitulo'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iParcela'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 56
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenc'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'dDtVencOriginal'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 121
          end
          item
            EditButtons = <>
            FieldName = 'nValTit'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 98
          end
          item
            EditButtons = <>
            FieldName = 'nValJuro'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 99
          end
          item
            EditButtons = <>
            FieldName = 'nSaldoTit'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 108
          end
          item
            EditButtons = <>
            FieldName = 'dDtLiq'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindowText
            Footer.Font.Height = -11
            Footer.Font.Name = 'Consolas'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Visible = False
            Width = 122
          end>
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Crediario.nCdCrediario'
      '      ,Crediario.nCdEmpresa'
      '      ,Empresa.cNmEmpresa'
      '      ,Crediario.nCdLoja'
      '      ,Loja.cNmLoja'
      '      ,Crediario.dDtVenda'
      '      ,Crediario.dDtUltVencto'
      '      ,Crediario.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Crediario.nValCrediario      '
      '      ,Crediario.nSaldo'
      '      ,Crediario.dDtLiq'
      '      ,Crediario.nCdCondPagto'
      '      ,CondPagto.cNmCondPagto'
      '  FROM Crediario'
      
        '       INNER JOIN Empresa   ON Empresa.nCdEmpresa     = Crediari' +
        'o.nCdEmpresa'
      
        '       INNER JOIN Loja      ON Loja.nCdLoja           = Crediari' +
        'o.nCdLoja'
      
        '       INNER JOIN Terceiro  ON Terceiro.nCdTerceiro   = Crediari' +
        'o.nCdTerceiro'
      
        '       LEFT  JOIN CondPagto ON CondPagto.nCdCondPagto = Crediari' +
        'o.nCdCondPagto'
      ' WHERE Crediario.nCdEmpresa   = :nCdEmpresa'
      '   AND Crediario.nCdCrediario = :nPK')
    Left = 520
    object qryMasternCdCrediario: TIntegerField
      FieldName = 'nCdCrediario'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMastercNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMastercNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryMasterdDtVenda: TDateTimeField
      FieldName = 'dDtVenda'
    end
    object qryMasterdDtUltVencto: TDateTimeField
      FieldName = 'dDtUltVencto'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMastercNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMasternValCrediario: TBCDField
      FieldName = 'nValCrediario'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternSaldo: TBCDField
      FieldName = 'nSaldo'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
    object qryMasternCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryMastercNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
  end
  inherited dsMaster: TDataSource
    Left = 568
  end
  inherited qryID: TADOQuery
    Left = 616
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 624
  end
  inherited qryStat: TADOQuery
    Left = 528
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 592
  end
  inherited ImageList1: TImageList
    Left = 728
  end
  object qryTitulo: TADOQuery
    Connection = frmMenu.Connection
    LockType = ltBatchOptimistic
    BeforeEdit = qryTituloBeforeEdit
    BeforePost = qryTituloBeforePost
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTitulo'
      '      ,iParcela'
      '      ,dDtVenc'
      '      ,dDtVencOriginal'
      '      ,nValTit'
      '      ,nValJuro'
      '      ,nSaldoTit'
      ',dDtLiq'
      '  FROM Titulo'
      ' WHERE nCdCrediario = :nPK'
      'AND dDtLiq IS NULL'
      ' ORDER BY iParcela')
    Left = 688
    Top = 288
    object qryTitulonCdTitulo: TIntegerField
      DisplayLabel = 'Presta'#231#245'es do Carnet|'
      FieldName = 'nCdTitulo'
    end
    object qryTituloiParcela: TIntegerField
      DisplayLabel = 'Presta'#231#245'es do Carnet|Parcela'
      FieldName = 'iParcela'
    end
    object qryTitulodDtVenc: TDateTimeField
      DisplayLabel = 'Presta'#231#245'es do Carnet|Vencimento'
      FieldName = 'dDtVenc'
    end
    object qryTitulodDtVencOriginal: TDateTimeField
      DisplayLabel = 'Presta'#231#245'es do Carnet|Vencimento Original'
      FieldName = 'dDtVencOriginal'
    end
    object qryTitulonValTit: TBCDField
      DisplayLabel = 'Presta'#231#245'es do Carnet|Valor Parcela'
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTitulonValJuro: TBCDField
      DisplayLabel = 'Presta'#231#245'es do Carnet|Valor Juros'
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTitulonSaldoTit: TBCDField
      DisplayLabel = 'Presta'#231#245'es do Carnet|Saldo a Pagar'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulodDtLiq: TDateTimeField
      DisplayLabel = 'Presta'#231#245'es do Carnet|Data Pagamento'
      FieldName = 'dDtLiq'
    end
  end
  object dsTitulo: TDataSource
    DataSet = qryTitulo
    Left = 728
    Top = 280
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 696
    Top = 200
  end
end
