inherited frmGrupoCliente: TfrmGrupoCliente
  Left = 249
  Top = 175
  Width = 882
  Height = 427
  Caption = 'Grupo de Cliente'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 866
    Height = 364
  end
  object Label1: TLabel [1]
    Left = 42
    Top = 40
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 31
    Top = 64
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 48
    Top = 87
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 19
    Top = 112
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Desconto %'
  end
  inherited ToolBar2: TToolBar
    Width = 866
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 88
    Top = 36
    Width = 65
    Height = 19
    DataField = 'nCdGrupoClienteDesc'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [7]
    Left = 88
    Top = 60
    Width = 576
    Height = 19
    DataField = 'cNmGrupoClienteDesc'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [8]
    Left = 88
    Top = 84
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [9]
    Tag = 1
    Left = 156
    Top = 84
    Width = 276
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsStatus
    TabOrder = 4
  end
  object cxPageControl1: TcxPageControl [10]
    Left = 8
    Top = 144
    Width = 657
    Height = 233
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 6
    ClientRectBottom = 229
    ClientRectLeft = 4
    ClientRectRight = 653
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Desconto por Cond. Pagto'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 649
        Height = 205
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsCondPagtoGrupoClienteDesc
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdCondPagto'
            Footers = <>
            Title.Caption = 'C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmCondPagto'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Descri'#231#227'o'
            Width = 438
          end
          item
            EditButtons = <>
            FieldName = 'nPercDesconto'
            Footers = <>
            Title.Caption = '% Desconto'
            Width = 101
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit5: TDBEdit [11]
    Left = 88
    Top = 108
    Width = 65
    Height = 19
    DataField = 'nPercDesconto'
    DataSource = dsMaster
    TabOrder = 5
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM GrupoClienteDesc'
      ' WHERE nCdGrupoClienteDesc = :nPK')
    Left = 456
    Top = 88
    object qryMasternCdGrupoClienteDesc: TIntegerField
      FieldName = 'nCdGrupoClienteDesc'
    end
    object qryMastercNmGrupoClienteDesc: TStringField
      FieldName = 'cNmGrupoClienteDesc'
      Size = 50
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMasternCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMasterdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryMasternPercDesconto: TBCDField
      FieldName = 'nPercDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  inherited dsMaster: TDataSource
    Left = 456
    Top = 120
  end
  inherited qryID: TADOQuery
    Left = 616
    Top = 120
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 552
    Top = 88
  end
  inherited qryStat: TADOQuery
    SQL.Strings = (
      'SELECT * FROM STATUS'
      '')
    Top = 120
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 552
    Top = 120
  end
  inherited ImageList1: TImageList
    Left = 584
    Top = 88
  end
  object qryStatus: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * FROM STATUS WHERE nCdStatus = :nPK')
    Left = 488
    Top = 88
    object qryStatusnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryStatuscNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object dsStatus: TDataSource
    DataSet = qryStatus
    Left = 488
    Top = 120
  end
  object qryCondPagtoGrupoClienteDesc: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCondPagtoGrupoClienteDescBeforePost
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM CondPagtoGrupoClienteDesc'
      ' WHERE nCdGrupoClienteDesc = :nPK'
      '')
    Left = 520
    Top = 88
    object qryCondPagtoGrupoClienteDescnCdCondPagtoGrupoClienteDesc: TIntegerField
      FieldName = 'nCdCondPagtoGrupoClienteDesc'
    end
    object qryCondPagtoGrupoClienteDescnCdGrupoClienteDesc: TIntegerField
      FieldName = 'nCdGrupoClienteDesc'
    end
    object qryCondPagtoGrupoClienteDescnCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryCondPagtoGrupoClienteDescnPercDesconto: TBCDField
      FieldName = 'nPercDesconto'
      Precision = 12
      Size = 2
    end
    object qryCondPagtoGrupoClienteDescnCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryCondPagtoGrupoClienteDescdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryCondPagtoGrupoClienteDesccNmCondPagto: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmCondPagto'
      LookupDataSet = qryCondPagto
      LookupKeyFields = 'nCdCondPagto'
      LookupResultField = 'cNmCondPagto'
      KeyFields = 'nCdCondPagto'
      LookupCache = True
      Size = 50
      Lookup = True
    end
  end
  object dsCondPagtoGrupoClienteDesc: TDataSource
    DataSet = qryCondPagtoGrupoClienteDesc
    Left = 520
    Top = 120
  end
  object qryCondPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM CondPagto')
    Left = 616
    Top = 88
    object qryCondPagtonCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryCondPagtocNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryCondPagtocFlgAtivo: TIntegerField
      FieldName = 'cFlgAtivo'
    end
  end
end
