inherited frmConciliaLoteLiqCobradora_Efetiva: TfrmConciliaLoteLiqCobradora_Efetiva
  Left = 410
  Top = 270
  Width = 651
  Height = 255
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  Caption = 'Concilia'#231#227'o Border'#244' Cobradora'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 635
    Height = 240
  end
  object Label1: TLabel [1]
    Left = 72
    Top = 48
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Lote'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 27
    Top = 72
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'm. Border'#244
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 24
    Top = 96
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Processo'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 28
    Top = 120
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Border'#244
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 42
    Top = 144
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cobradora'
    FocusControl = DBEdit5
  end
  object Label7: TLabel [6]
    Left = 10
    Top = 168
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio Digita'#231#227'o'
    FocusControl = DBEdit7
  end
  object Label12: TLabel [7]
    Left = 26
    Top = 192
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Cr'#233'dito'
  end
  inherited ToolBar1: TToolBar
    Width = 635
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit1: TDBEdit [9]
    Tag = 1
    Left = 96
    Top = 40
    Width = 81
    Height = 21
    DataField = 'nCdLoteLiqCobradora'
    DataSource = DataSource1
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [10]
    Tag = 1
    Left = 96
    Top = 64
    Width = 137
    Height = 21
    DataField = 'cNrBorderoCob'
    DataSource = DataSource1
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [11]
    Tag = 1
    Left = 96
    Top = 88
    Width = 137
    Height = 21
    DataField = 'dDtFecham'
    DataSource = DataSource1
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [12]
    Tag = 1
    Left = 96
    Top = 112
    Width = 137
    Height = 21
    DataField = 'nValTotal'
    DataSource = DataSource1
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [13]
    Tag = 1
    Left = 96
    Top = 136
    Width = 65
    Height = 21
    DataField = 'nCdCobradora'
    DataSource = DataSource1
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [14]
    Tag = 1
    Left = 168
    Top = 136
    Width = 459
    Height = 21
    DataField = 'cNmCobradora'
    DataSource = DataSource1
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [15]
    Tag = 1
    Left = 96
    Top = 160
    Width = 531
    Height = 21
    DataField = 'cNmUsuario'
    DataSource = DataSource1
    TabOrder = 7
  end
  object edtContaBancaria: TMaskEdit [16]
    Left = 96
    Top = 184
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 8
    Text = '         '
    OnExit = edtContaBancariaExit
    OnKeyDown = edtContaBancariaKeyDown
  end
  object DBEdit8: TDBEdit [17]
    Tag = 1
    Left = 168
    Top = 184
    Width = 199
    Height = 21
    DataField = 'nCdConta'
    DataSource = DataSource2
    TabOrder = 9
  end
  object MaskEdit1: TMaskEdit [18]
    Left = 96
    Top = 248
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 10
    Text = '         '
  end
  inherited ImageList1: TImageList
    Left = 384
  end
  object qryLoteLiqCobradora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT LoteLiqCobradora.nCdLoteLiqCobradora'
      '      ,LoteLiqCobradora.cNrBorderoCob'
      '      ,LoteLiqCobradora.dDtFecham'
      '      ,LoteLiqCobradora.nValTotal'
      '      ,LoteLiqCobradora.nCdCobradora'
      '      ,Cobradora.cNmCobradora'
      '      ,Usuario.cNmUsuario'
      '  FROM LoteLiqCobradora'
      
        '       INNER JOIN Cobradora ON LoteLiqCobradora.nCdCobradora  = ' +
        'Cobradora.nCdCobradora'
      
        '       INNER JOIN Usuario   ON LoteLiqCobradora.nCdUsuarioCad = ' +
        'Usuario.nCdUsuario'
      ' WHERE LoteLiqCobradora.nCdLoteLiqCobradora = :nPK')
    Left = 296
    Top = 96
    object qryLoteLiqCobradoranCdLoteLiqCobradora: TIntegerField
      FieldName = 'nCdLoteLiqCobradora'
    end
    object qryLoteLiqCobradoracNrBorderoCob: TStringField
      FieldName = 'cNrBorderoCob'
      Size = 10
    end
    object qryLoteLiqCobradoradDtFecham: TDateTimeField
      FieldName = 'dDtFecham'
    end
    object qryLoteLiqCobradoranValTotal: TBCDField
      FieldName = 'nValTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryLoteLiqCobradoranCdCobradora: TIntegerField
      FieldName = 'nCdCobradora'
    end
    object qryLoteLiqCobradoracNmCobradora: TStringField
      FieldName = 'cNmCobradora'
      Size = 50
    end
    object qryLoteLiqCobradoracNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLoteLiqCobradora
    Left = 328
    Top = 104
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdConta'
      '  FROM ContaBancaria'
      ' WHERE cFlgCofre        = 1'
      '   AND nCdEmpresa       = :nCdEmpresa'
      '   AND nCdLoja          = :nCdLoja'
      '   AND nCdContaBancaria = :nPK')
    Left = 384
    Top = 104
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
  end
  object DataSource2: TDataSource
    DataSet = qryContaBancaria
    Left = 416
    Top = 104
  end
  object SP_ACEITA_BORDERO_COBRADORA_FINANCEIRO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ACEITA_BORDERO_COBRADORA_FINANCEIRO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLoteliqCobradora'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 312
    Top = 40
  end
end
