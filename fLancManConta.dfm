inherited frmLancManConta: TfrmLancManConta
  Left = 214
  Top = 182
  Caption = 'Lan'#231'amento Manual Conta Banc'#225'ria'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label5: TLabel [1]
    Left = 31
    Top = 64
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Lan'#231'amento'
  end
  object Label1: TLabel [2]
    Left = 8
    Top = 40
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Banc'#225'ria/Cofre'
  end
  object Label21: TLabel [3]
    Left = 27
    Top = 136
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Lan'#231'amento'
  end
  object Label2: TLabel [4]
    Left = 18
    Top = 88
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Documento'
  end
  object Label3: TLabel [5]
    Left = 71
    Top = 112
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Hist'#243'rico'
  end
  inherited ToolBar1: TToolBar
    TabOrder = 4
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit3: TMaskEdit [7]
    Left = 115
    Top = 32
    Width = 61
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object MaskEdit6: TMaskEdit [8]
    Left = 115
    Top = 56
    Width = 62
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object edtNrDocumento: TEdit [9]
    Left = 115
    Top = 80
    Width = 121
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 2
  end
  object edtHistorico: TEdit [10]
    Left = 115
    Top = 104
    Width = 353
    Height = 21
    CharCase = ecUpperCase
    TabOrder = 3
  end
  object DBEdit1: TDBEdit [11]
    Tag = 1
    Left = 179
    Top = 32
    Width = 400
    Height = 21
    DataField = 'nCdConta'
    DataSource = DataSource1
    TabOrder = 5
  end
  object DBEdit2: TDBEdit [12]
    Tag = 1
    Left = 179
    Top = 56
    Width = 401
    Height = 21
    DataField = 'cNmTipoLancto'
    DataSource = DataSource2
    TabOrder = 6
  end
  object edtValorLanc: TcxCurrencyEdit [13]
    Left = 115
    Top = 128
    Width = 121
    Height = 23
    ParentFont = False
    Properties.Alignment.Horz = taRightJustify
    Properties.Alignment.Vert = taBottomJustify
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ValidateOnEnter = True
    Style.BorderStyle = ebsFlat
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -13
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    Style.LookAndFeel.NativeStyle = False
    TabOrder = 7
    OnKeyPress = edtValorLancKeyPress
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      
        '      ,CASE WHEN (cFlgCaixa = 0 AND cFlgCofre = 0) THEN (Convert' +
        '(VARCHAR(5),nCdBanco) + '#39' - '#39' + Convert(VARCHAR(5),cAgencia) + '#39 +
        ' - '#39' + nCdConta + '#39' - '#39' + IsNull(cNmTitular,'#39' '#39'))'
      '            ELSE nCdConta'
      '       END nCdConta'
      '     ,cFlgCofre'
      '  FROM ContaBancaria'
      ' WHERE nCdContaBancaria = :nPK'
      '   AND nCdEmpresa       = :nCdEmpresa'
      '   AND (cFlgCaixa = 0 OR cFlgCofre = 1)')
    Left = 264
    Top = 216
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      ReadOnly = True
      Size = 84
    end
    object qryContaBancariacFlgCofre: TIntegerField
      FieldName = 'cFlgCofre'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryContaBancaria
    Left = 384
    Top = 240
  end
  object qryTipoLancto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTipoLancto'
      '      ,cNmTipoLancto'
      '      ,cSinalOper'
      '  FROM TipoLancto'
      ' WHERE cFlgPermManual = 1'
      '   AND nCdTipoLancto  = :nPK')
    Left = 264
    Top = 264
    object qryTipoLanctonCdTipoLancto: TIntegerField
      FieldName = 'nCdTipoLancto'
    end
    object qryTipoLanctocNmTipoLancto: TStringField
      FieldName = 'cNmTipoLancto'
      Size = 50
    end
    object qryTipoLanctocSinalOper: TStringField
      FieldName = 'cSinalOper'
      FixedChar = True
      Size = 1
    end
  end
  object DataSource2: TDataSource
    DataSet = qryTipoLancto
    Left = 392
    Top = 248
  end
  object SP_LANCTOMANUAL_CONTA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_LANCTOMANUAL_CONTA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdContaBancaria'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTipoLancto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cDocumento'
        Attributes = [paNullable]
        DataType = ftString
        Size = 15
        Value = Null
      end
      item
        Name = '@cHistorico'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@nValLancto'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 384
    Top = 176
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 456
    Top = 208
  end
end
