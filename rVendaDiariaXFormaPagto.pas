unit rVendaDiariaXFormaPagto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBCtrls, StdCtrls, Mask, DB, ADODB, RDprint;

type
  TrptVendaDiariaXFormaPagto = class(TfrmRelatorio_Padrao)
    dsLoja: TDataSource;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    MaskEdit2: TMaskEdit;
    Label6: TLabel;
    MaskEdit1: TMaskEdit;
    Label3: TLabel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    usp_Relatorio: TADOStoredProc;
    usp_RelatoriocNmLoja: TStringField;
    usp_RelatoriodDtVenda: TDateTimeField;
    usp_RelatorionValOper: TBCDField;
    usp_RelatorionValDinheiro: TBCDField;
    usp_RelatorionValCheque: TBCDField;
    usp_RelatorionValCartao: TBCDField;
    usp_RelatorionValTEF: TBCDField;
    usp_RelatorionValCrediario: TBCDField;
    usp_RelatorionValRecPrest: TBCDField;
    usp_RelatorionValDescontoCondPagto: TBCDField;
    edtLoja: TMaskEdit;
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaDiariaXFormaPagto: TrptVendaDiariaXFormaPagto;

implementation

uses fMenu, fLookup_Padrao, rVendaDiariaXFormaPagto_view;

{$R *.dfm}

procedure Verifica_Data(Componente: TMaskEdit);
begin
  try
    if Componente.Text <> '  /  /    ' Then
      StrToDate(Componente.Text);
  except
    frmMenu.MensagemAlerta('Data Inv�lida');
    Componente.Clear;
    Componente.SetFocus;
    Abort;
  end;
end;

procedure TrptVendaDiariaXFormaPagto.ToolButton1Click(Sender: TObject);
var
  cFiltro : string;
  objRel  : TrptVendaDiariaXFormaPagto_view;
begin
  inherited;

  if (DBEdit1.Text = '') and (trim(edtLoja.Text) <> '') then
  begin
      MensagemAlerta('Loja inv�lida.') ;
      edtLoja.Setfocus;
      abort ;
  end ;

  objRel := TrptVendaDiariaXFormaPagto_view.Create(nil);

  try
      try

          objRel.usp_Relatorio.Close;

          if(Trim(edtLoja.Text)='') then
              objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value  := 0
          else objRel.usp_Relatorio.Parameters.ParamByName('@nCdLoja').Value := Trim(edtLoja.Text);

            {--valida a data inserida--}

          Verifica_Data(MaskEdit1);
          Verifica_Data(MaskEdit2);

          if (MaskEdit1.Text = '  /  /    ') then
               MaskEdit1.Text := DateToStr(Now());

          if (MaskEdit2.Text = '  /  /    ') then
               MaskEdit2.Text := DateToStr(Now());

          if (frmMenu.ConvData(MaskEdit1.Text) > frmMenu.ConvData(MaskEdit2.Text)) then
          begin
              MensagemAlerta('Data inicial maior do que a data final');
              Abort;
          end;


          objRel.usp_Relatorio.Parameters.ParamByName('@nCdUsuario').Value   := frmMenu.nCdUsuarioLogado;
          objRel.usp_Relatorio.Parameters.ParamByName('@dDtInicial').Value   := MaskEdit1.Text;
          objRel.usp_Relatorio.Parameters.ParamByName('@dDtFinal').Value     := MaskEdit2.Text;
          objRel.usp_Relatorio.Open;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          cFiltro := 'Per�odo: ' + MaskEdit1.Text + ' � ' + MaskEdit2.Text;

          if (Trim(edtLoja.Text) <> '') then
              cFiltro := cFiltro + ' /Loja: ' + qryLojacNmLoja.AsString;
      
          objRel.lblFiltro.Caption := cFiltro;

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel) ;
  end;

end;

procedure TrptVendaDiariaXFormaPagto.edtLojaExit(Sender: TObject);
begin
  inherited;
  qryLoja.Close;
  PosicionaQuery(qryLoja, edtLoja.Text);
end;

procedure TrptVendaDiariaXFormaPagto.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(147,'EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja = Loja.nCdLoja AND UL.nCdUsuario = @@Usuario)');

            If (nPK > 0) then
            begin
                edtLoja.Text := IntToStr(nPK);
                PosicionaQuery(qryLoja, edtLoja.Text);
            end ;

    end ;

  end ;
end;

procedure TrptVendaDiariaXFormaPagto.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  
end;

initialization
    RegisterClass(TrptVendaDiariaXFormaPagto) ;

end.
