unit fArvorePlanoConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, ADODB;

type
  TfrmArvorePlanoConta = class(TfrmProcesso_Padrao)
    qryPlanoConta: TADOQuery;
    qryPlanoContacNmGrupoPlanoConta: TStringField;
    qryPlanoContanCdPlanoConta: TIntegerField;
    qryPlanoContacNmPlanoConta: TStringField;
    DataSource1: TDataSource;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1cNmGrupoPlanoConta: TcxGridDBColumn;
    cxGrid1DBTableView1nCdPlanoConta: TcxGridDBColumn;
    cxGrid1DBTableView1cNmPlanoConta: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmArvorePlanoConta: TfrmArvorePlanoConta;

implementation

{$R *.dfm}

procedure TfrmArvorePlanoConta.FormShow(Sender: TObject);
begin
  inherited;

  qryPlanoConta.Close ;
  qryPlanoConta.Open ;

    cxGrid1DBTableView1.OptionsData.Deleting        := False ;
    cxGrid1DBTableView1.OptionsData.Inserting       := False ;
    cxGrid1DBTableView1.OptionsData.Editing         := False ;

    cxGrid1DBTableView1.OptionsView.GridLines       := glVertical ;
    cxGrid1DBTableView1.OptionsView.GroupByBox      := False ;
    cxGrid1DBTableView1.OptionsView.Header          := True ;

    cxGrid1DBTableView1.OptionsBehavior.PullFocusing := True ;

    cxGrid1DBTableView1.OptionsCustomize.ColumnHidingOnGrouping := False ;
    cxGrid1DBTableView1.OptionsCustomize.ColumnHiding := True;
    cxGrid1DBTableView1.OptionsCustomize.ColumnMoving := True ;

  cxGrid1.Refresh;

end;

end.
