unit fSituacaoEstoqueProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  cxPC, cxControls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ADODB, Mask, DBCtrls;

type
  TfrmSituacaoEstoqueProduto = class(TfrmProcesso_Padrao)
    RadioGroup1: TRadioGroup;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qrySituacaoEstoqueProduto: TADOQuery;
    dsSituacaoEstoqueProduto: TDataSource;
    qrySituacaoEstoqueProdutoiID: TAutoIncField;
    qrySituacaoEstoqueProdutonCdPedido: TIntegerField;
    qrySituacaoEstoqueProdutocNmTerceiro: TStringField;
    qrySituacaoEstoqueProdutodDtPedido: TDateTimeField;
    qrySituacaoEstoqueProdutodDtEntrega: TDateTimeField;
    qrySituacaoEstoqueProdutocNmTipoPedido: TStringField;
    qrySituacaoEstoqueProdutonQtde: TBCDField;
    qrySituacaoEstoqueProdutonSaldo: TBCDField;
    qrySituacaoEstoqueProdutonCdTabTipoPedido: TIntegerField;
    cxGrid1DBTableView1iID: TcxGridDBColumn;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtEntrega: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGrid1DBTableView1nQtde: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldo: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTabTipoPedido: TcxGridDBColumn;
    qrySituacaoEstoqueProdutocUnidadeMedida: TStringField;
    cxGrid1DBTableView1cUnidadeMedida: TcxGridDBColumn;
    GroupBox1: TGroupBox;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    procedure RadioGroup1Click(Sender: TObject);
    procedure exibeSituacaoProduto(nCdEmpresa:integer;nCdProduto:integer);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSituacaoEstoqueProduto: TfrmSituacaoEstoqueProduto;

implementation

uses fProdutoPosicaoEstoque;

{$R *.dfm}

procedure TfrmSituacaoEstoqueProduto.exibeSituacaoProduto(nCdEmpresa,
  nCdProduto: integer);
begin

    qryProduto.Close;
    PosicionaQuery(qryProduto, intToStr(nCdProduto)) ;
    
    qrySituacaoEstoqueProduto.Close;
    qrySituacaoEstoqueProduto.Parameters.ParamByName('nCdEmpresa').Value := nCdEmpresa;
    qrySituacaoEstoqueProduto.Parameters.ParamByName('nCdProduto').Value := nCdProduto;

    ToolButton1.Click;

    Self.ShowModal;

end;

procedure TfrmSituacaoEstoqueProduto.RadioGroup1Click(Sender: TObject);
begin
  inherited;

  ToolButton1.Click;
  
end;

procedure TfrmSituacaoEstoqueProduto.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qrySituacaoEstoqueProduto.Close;
  qrySituacaoEstoqueProduto.Parameters.ParamByName('cFlgTipoPedido').Value := RadioGroup1.ItemIndex;
  qrySituacaoEstoqueProduto.Open;

end;

procedure TfrmSituacaoEstoqueProduto.ToolButton5Click(Sender: TObject);
var
  objRel : TfrmProdutoPosicaoEstoque;
begin
    objRel := TfrmProdutoPosicaoEstoque.Create(nil);
    Try
        Try
            PosicionaQuery(objRel.qryPosicaoEstoque, qryProdutonCdProduto.AsString) ;
            showForm(objRel,false); 
        except
            MensagemErro('Erro na cria��o do Relat�rio');
            raise;
        end;
    finally
        FreeAndNil(objRel);
    end;
end;

end.
