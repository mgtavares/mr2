unit fConsultaSaldoConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, ADODB;

type
  TfrmConsultaSaldoConta = class(TfrmProcesso_Padrao)
    qryContaBancaria: TADOQuery;
    qryContaBancariacNmLoja: TStringField;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancarianSaldoConta: TBCDField;
    qryContaBancariadDtUltConciliacao: TDateTimeField;
    qryContaBancariacTipoConta: TStringField;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dsContaBancaria: TDataSource;
    cxGrid1DBTableView1cNmLoja: TcxGridDBColumn;
    cxGrid1DBTableView1nCdContaBancaria: TcxGridDBColumn;
    cxGrid1DBTableView1nCdConta: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoConta: TcxGridDBColumn;
    cxGrid1DBTableView1dDtUltConciliacao: TcxGridDBColumn;
    cxGrid1DBTableView1cTipoConta: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaSaldoConta: TfrmConsultaSaldoConta;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmConsultaSaldoConta.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      cxGrid1DBTableView1cNmLoja.Visible := False ;

  ToolButton1.Click ;
  
end;

procedure TfrmConsultaSaldoConta.ToolButton1Click(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryContaBancaria, IntToStr(frmMenu.nCdEmpresaAtiva)) ;

end;

initialization
    RegisterClass(TfrmConsultaSaldoConta) ;

end.
