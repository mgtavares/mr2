unit fPedidoComercial;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, cxContainer, cxEdit, cxTextEdit,
  DBCtrlsEh, DBLookupEh, Menus, DBGridEhGrouping, ToolCtrlsEh,
  cxCurrencyEdit, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid;

type
  TfrmPedidoComercial = class(TfrmCadastro_Padrao)
    qryMasternCdPedido: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    qryMasternCdTabTipoPedido: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdTerceiroColab: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternCdTabStatusPed: TIntegerField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternPercDesconto: TBCDField;
    qryMasternPercAcrescimo: TBCDField;
    qryMasternValProdutos: TBCDField;
    qryMasternValServicos: TBCDField;
    qryMasternValImposto: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMasternValAcrescimo: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMasternValOutros: TBCDField;
    qryMasternValPedido: TBCDField;
    qryMastercNrPedTerceiro: TStringField;
    qryMastercOBS: TMemoField;
    qryMasterdDtAutor: TDateTimeField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMasterdDtRejeicao: TDateTimeField;
    qryMasternCdUsuarioRejeicao: TIntegerField;
    qryMastercMotivoRejeicao: TMemoField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTipoPedidonCdTabTipoPedido: TIntegerField;
    qryTipoPedidocFlgCalcImposto: TIntegerField;
    qryTipoPedidocFlgComporRanking: TIntegerField;
    qryTipoPedidocFlgVenda: TIntegerField;
    qryTipoPedidocGerarFinanc: TIntegerField;
    qryTipoPedidocExigeAutor: TIntegerField;
    qryTipoPedidonCdEspTitPrev: TIntegerField;
    qryTipoPedidonCdEspTit: TIntegerField;
    qryTipoPedidonCdCategFinanc: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceiroColab: TADOQuery;
    qryTerceiroColabnCdTerceiro: TIntegerField;
    qryTerceiroColabcCNPJCPF: TStringField;
    qryTerceiroColabcNmTerceiro: TStringField;
    qryTerceiroTransp: TADOQuery;
    qryTerceiroTranspnCdTerceiro: TIntegerField;
    qryTerceiroTranspcCNPJCPF: TStringField;
    qryTerceiroTranspcNmTerceiro: TStringField;
    qryIncoterms: TADOQuery;
    qryIncotermsnCdIncoterms: TIntegerField;
    qryIncotermscSigla: TStringField;
    qryIncotermscNmIncoterms: TStringField;
    qryIncotermscFlgPagador: TStringField;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    qryEstoque: TADOQuery;
    qryEstoquenCdEstoque: TAutoIncField;
    qryEstoquenCdEmpresa: TIntegerField;
    qryEstoquenCdLoja: TIntegerField;
    qryEstoquenCdTipoEstoque: TIntegerField;
    qryEstoquecNmEstoque: TStringField;
    qryEstoquenCdStatus: TIntegerField;
    qryStatusPed: TADOQuery;
    qryStatusPednCdTabStatusPed: TIntegerField;
    qryStatusPedcNmTabStatusPed: TStringField;
    qryTerceiroPagador: TADOQuery;
    qryTerceiroPagadornCdTerceiro: TIntegerField;
    qryTerceiroPagadorcCNPJCPF: TStringField;
    qryTerceiroPagadorcNmTerceiro: TStringField;
    qryMastercNmContato: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    dsEmpresa: TDataSource;
    DBEdit13: TDBEdit;
    dsLoja: TDataSource;
    DBEdit14: TDBEdit;
    dsTipoPedido: TDataSource;
    DBEdit15: TDBEdit;
    dsTerceiro: TDataSource;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    dsStatusPed: TDataSource;
    Label12: TLabel;
    DBEdit7: TDBEdit;
    Label13: TLabel;
    DBEdit18: TDBEdit;
    qryMasternCdEstoqueMov: TIntegerField;
    dsTerceiroTransp: TDataSource;
    dsEstoque: TDataSource;
    Label17: TLabel;
    DBEdit24: TDBEdit;
    dsTerceiroPagador: TDataSource;
    DBEdit27: TDBEdit;
    dsCondPagto: TDataSource;
    dsIncoterms: TDataSource;
    cxPageControl1: TcxPageControl;
    TabItemEstoque: TcxTabSheet;
    TabAD: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioTipoPedido: TADOQuery;
    qryUsuarioTipoPedidonCdTipoPedido: TIntegerField;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdLoja: TIntegerField;
    qryTerceironCdTerceiroPagador: TIntegerField;
    Label19: TLabel;
    DBEdit30: TDBEdit;
    Label20: TLabel;
    DBEdit31: TDBEdit;
    Label21: TLabel;
    DBEdit32: TDBEdit;
    Label22: TLabel;
    DBEdit33: TDBEdit;
    Label25: TLabel;
    DBEdit36: TDBEdit;
    Label26: TLabel;
    DBEdit37: TDBEdit;
    Label27: TLabel;
    dsTerceiroColab: TDataSource;
    TabParcela: TcxTabSheet;
    qryItemEstoque: TADOQuery;
    qryItemEstoquenCdProduto: TIntegerField;
    qryItemEstoquecNmItem: TStringField;
    qryItemEstoquenQtdePed: TBCDField;
    qryItemEstoquenQtdeExpRec: TBCDField;
    qryItemEstoquenQtdeCanc: TBCDField;
    qryItemEstoquenValSugVenda: TBCDField;
    qryItemEstoquenValTotalItem: TBCDField;
    qryItemEstoquenPercIPI: TBCDField;
    qryItemEstoquenValIPI: TBCDField;
    dsItemEstoque: TDataSource;
    qryItemEstoquenCdPedido: TIntegerField;
    qryItemEstoquenCdTipoItemPed: TIntegerField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryItemEstoquecCalc: TStringField;
    qryTerceironCdTerceiroTransp: TIntegerField;
    qryTerceironCdIncoterms: TIntegerField;
    qryTerceironCdCondPagto: TIntegerField;
    qryTerceironPercDesconto: TBCDField;
    qryTerceironPercAcrescimo: TBCDField;
    DBGridEh2: TDBGridEh;
    usp_Grade: TADOStoredProc;
    dsGrade: TDataSource;
    qryTemp: TADOQuery;
    usp_Gera_SubItem: TADOStoredProc;
    qryItemEstoquenCdItemPedido: TAutoIncField;
    cmdExcluiSubItem: TADOCommand;
    qryAux: TADOQuery;
    DBGridEh3: TDBGridEh;
    qryPrazoPedido: TADOQuery;
    dsPrazoPedido: TDataSource;
    qryPrazoPedidonCdPrazoPedido: TAutoIncField;
    qryPrazoPedidonCdPedido: TIntegerField;
    qryPrazoPedidodVencto: TDateTimeField;
    qryPrazoPedidoiDias: TIntegerField;
    qryPrazoPedidonValPagto: TBCDField;
    btSugParcela: TcxButton;
    usp_Sugere_Parcela: TADOStoredProc;
    Image3: TImage;
    ToolButton3: TToolButton;
    btFinalizarPed: TToolButton;
    qryProdutonCdGrade: TIntegerField;
    DBGridEh4: TDBGridEh;
    qryItemAD: TADOQuery;
    qryItemADcCdProduto: TStringField;
    qryItemADcNmItem: TStringField;
    qryItemADnQtdePed: TBCDField;
    qryItemADnQtdeExpRec: TBCDField;
    qryItemADnQtdeCanc: TBCDField;
    qryItemADnValTotalItem: TBCDField;
    qryItemADnCdPedido: TIntegerField;
    qryItemADnCdTipoItemPed: TIntegerField;
    qryItemADnCdItemPedido: TAutoIncField;
    dsItemAD: TDataSource;
    qryItemADcSiglaUnidadeMedida: TStringField;
    qryTipoPedidocFlgAtuPreco: TIntegerField;
    qryTipoPedidocFlgItemEstoque: TIntegerField;
    qryTipoPedidocFlgItemAD: TIntegerField;
    qryTipoPedidonCdTipoPedido_1: TIntegerField;
    qryTipoPedidonCdUsuario: TIntegerField;
    usp_Finaliza: TADOStoredProc;
    btCancelarPed: TToolButton;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    cxTextEdit4: TcxTextEdit;
    cxTextEdit5: TcxTextEdit;
    cxTextEdit6: TcxTextEdit;
    qryDadoAutorz: TADOQuery;
    qryDadoAutorzcDadoAutoriz: TStringField;
    DBEdit34: TDBEdit;
    dsDadoAutorz: TDataSource;
    qryMastercFlgCritico: TIntegerField;
    qryMasternSaldoFat: TBCDField;
    TabFollowUp: TcxTabSheet;
    DBGridEh5: TDBGridEh;
    DBMemo1: TDBMemo;
    qryFollow: TADOQuery;
    qryFollowdDtFollowUp: TDateTimeField;
    qryFollowcNmUsuario: TStringField;
    qryFollowcOcorrenciaResum: TStringField;
    qryFollowdDtProxAcao: TDateTimeField;
    qryFollowcOcorrencia: TMemoField;
    dsFollow: TDataSource;
    TabFormula: TcxTabSheet;
    qryItemFormula: TADOQuery;
    qryItemFormulacNmItem: TStringField;
    qryItemFormulanQtdePed: TBCDField;
    qryItemFormulanQtdeExpRec: TBCDField;
    qryItemFormulanQtdeCanc: TBCDField;
    qryItemFormulanValTotalItem: TBCDField;
    qryItemFormulanValCustoUnit: TBCDField;
    qryItemFormulanCdPedido: TIntegerField;
    qryItemFormulanCdTipoItemPed: TIntegerField;
    qryItemFormulanCdItemPedido: TAutoIncField;
    qryItemFormulacSiglaUnidadeMedida: TStringField;
    dsItemFormula: TDataSource;
    DBGridEh6: TDBGridEh;
    Label33: TLabel;
    cxTextEdit7: TcxTextEdit;
    Label34: TLabel;
    cxTextEdit8: TcxTextEdit;
    Label35: TLabel;
    cxTextEdit9: TcxTextEdit;
    qryItemFormulanCdProduto: TIntegerField;
    qryProdutoFormulado: TADOQuery;
    qryProdutoFormuladonCdProduto: TIntegerField;
    qryProdutoFormuladocNmProduto: TStringField;
    usp_gera_item_embalagem: TADOStoredProc;
    cxButton2: TcxButton;
    qryTipoPedidocFlgExibeAcomp: TIntegerField;
    qryTipoPedidocFlgCompra: TIntegerField;
    qryTipoPedidocFlgItemFormula: TIntegerField;
    qryMastercOBSFinanc: TMemoField;
    qryMasternPercDescontoVencto: TBCDField;
    qryMasternPercAcrescimoVendor: TBCDField;
    Label36: TLabel;
    DBEdit35: TDBEdit;
    Label37: TLabel;
    DBEdit40: TDBEdit;
    TabCentroCusto: TcxTabSheet;
    qryItemLocalEntrega: TADOQuery;
    qryItemLocalEntreganCdItemPedido: TAutoIncField;
    qryItemLocalEntreganCdPedido: TIntegerField;
    qryItemLocalEntreganCdItemPedidoPai: TIntegerField;
    qryItemLocalEntreganCdProduto: TIntegerField;
    qryItemLocalEntregacCdProduto: TStringField;
    qryItemLocalEntreganCdTipoItemPed: TIntegerField;
    qryItemLocalEntreganCdEstoqueMov: TIntegerField;
    qryItemLocalEntregacNmItem: TStringField;
    qryItemLocalEntreganQtdePed: TBCDField;
    qryItemLocalEntreganQtdeExpRec: TBCDField;
    qryItemLocalEntreganQtdeCanc: TBCDField;
    qryItemLocalEntreganValUnitario: TBCDField;
    qryItemLocalEntreganPercIPI: TBCDField;
    qryItemLocalEntreganValIPI: TBCDField;
    qryItemLocalEntreganValDesconto: TBCDField;
    qryItemLocalEntreganValCustoUnit: TBCDField;
    qryItemLocalEntreganValSugVenda: TBCDField;
    qryItemLocalEntreganValTotalItem: TBCDField;
    qryItemLocalEntreganValAcrescimo: TBCDField;
    qryItemLocalEntregacSiglaUnidadeMedida: TStringField;
    qryItemLocalEntregaiColuna: TIntegerField;
    qryItemLocalEntreganCdTabStatusItemPed: TIntegerField;
    qryItemLocalEntreganValUnitarioEsp: TBCDField;
    dsItemLocalEntrega: TDataSource;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryItemLocalEntregacNmLocalEstoque: TStringField;
    Image2: TImage;
    Label14: TLabel;
    DBGridEh8: TDBGridEh;
    qryItemCC: TADOQuery;
    qryItemCCnCdItemPedido: TAutoIncField;
    qryItemCCcNmItem: TStringField;
    dsItemCC: TDataSource;
    qryCCItemPedido: TADOQuery;
    dsCCItemPedido: TDataSource;
    qryCCItemPedidonCdItemPedido: TIntegerField;
    qryCCItemPedidonCdCC: TIntegerField;
    qryCCItemPedidonPercent: TBCDField;
    qryCentroCusto: TADOQuery;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocNmCC: TStringField;
    qryCentroCustonCdCC1: TIntegerField;
    qryCentroCustonCdCC2: TIntegerField;
    qryCentroCustocCdCC: TStringField;
    qryCentroCustocCdHie: TStringField;
    qryCentroCustoiNivel: TSmallintField;
    qryCentroCustocFlgLanc: TIntegerField;
    qryCentroCustonCdStatus: TIntegerField;
    qryCCItemPedidocNmCC: TStringField;
    DBComboItens: TDBLookupComboboxEh;
    cxButton4: TcxButton;
    usp_copia_cc: TADOStoredProc;
    qryTerceiroRepres: TADOQuery;
    dsTerceiroRepres: TDataSource;
    qryTerceiroRepresnCdTerceiro: TIntegerField;
    qryTerceiroReprescNmTerceiro: TStringField;
    qryMasternCdMotBloqPed: TIntegerField;
    qryMasternCdTerceiroRepres: TIntegerField;
    Label15: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    PopupMenu1: TPopupMenu;
    ExibirRecebimentosdoItem1: TMenuItem;
    qryItemEstoquenPercICMSSub: TBCDField;
    qryItemEstoquenValICMSSub: TBCDField;
    qryItemADnPercIPI: TBCDField;
    qryItemADnPercICMSSub: TBCDField;
    qryItemADnValUnitario: TBCDField;
    qryItemADnValIPI: TBCDField;
    qryItemADnValICMSSub: TBCDField;
    qryItemLocalEntreganQtdeLibFat: TBCDField;
    qryMasternCdUsuarioComprador: TIntegerField;
    DBEdit21: TDBEdit;
    qryItemEstoquecSiglaUnidadeMedida: TStringField;
    qryProdutocUnidadeMedida: TStringField;
    qryProdutonQtdeMinimaCompra: TBCDField;
    qryItemEstoquenPercDescontoItem: TBCDField;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    qryLocalEstoqueEntrega: TADOQuery;
    qryLocalEstoqueEntreganCdLocalEstoque: TIntegerField;
    qryLocalEstoqueEntregacNmLocalEstoque: TStringField;
    DBEdit23: TDBEdit;
    dsLocalEstoqueEntrega: TDataSource;
    qryLojanCdLocalEstoquePadrao: TIntegerField;
    TabAbatimentos: TcxTabSheet;
    qryTituloAbatPedido: TADOQuery;
    qryTituloAbatPedidonCdTituloAbatPedido: TAutoIncField;
    qryTituloAbatPedidonCdPedido: TIntegerField;
    qryTituloAbatPedidonCdTitulo: TIntegerField;
    qryTituloAbatPedidonPercAbat: TBCDField;
    qryTituloAbatPedidocNmEspTit: TStringField;
    qryTituloAbatPedidocObsTit: TStringField;
    qryTituloAbatPedidodDtVenc: TDateField;
    DBGridEh7: TDBGridEh;
    dsTituloAbatPedido: TDataSource;
    qryTitulo: TADOQuery;
    qryTitulodDtVenc: TDateTimeField;
    qryTitulocObsTit: TMemoField;
    qryTitulocNmEspTit: TStringField;
    qryTitulodDtBloqTit: TDateTimeField;
    qryTitulodDtCancel: TDateTimeField;
    qryTitulonSaldoTit: TBCDField;
    DBEdit22: TDBEdit;
    qryProdutonValVenda: TBCDField;
    qryProdutoFornecedor: TADOQuery;
    qryProdutoFornecedornPrecoUnit: TBCDField;
    qryProdutoFornecedornCdMoeda: TIntegerField;
    qryProdutoFornecedornPercIPI: TBCDField;
    qryProdutoFornecedornPercICMSSub: TBCDField;
    qryProdutoFornecedornPrecoUnitEsp: TBCDField;
    qryItemEstoquenValUnitario: TFloatField;
    qryItemEstoquenValDesconto: TFloatField;
    qryItemEstoquenValAcrescimo: TFloatField;
    qryItemEstoquenValCustoUnit: TFloatField;
    qryItemEstoquenValUnitarioEsp: TFloatField;
    TabObservacao: TcxTabSheet;
    DBMemo2: TDBMemo;
    btContatoPadrao: TcxButton;
    qryContatoPadrao: TADOQuery;
    qryContatoPadraocNmContato: TStringField;
    qryItemEstoquedDtEntregaIni: TDateTimeField;
    qryItemEstoquedDtEntregaFim: TDateTimeField;
    qryItemEstoquenQtdePrev: TFloatField;
    qryItemADnValCustoUnit: TBCDField;
    SP_ENVIA_PEDIDO_TEMPREGISTRO: TADOStoredProc;
    qryProdutocFlgProdVenda: TIntegerField;
    qryItemEstoquenFatorConvUnidadeEstoque: TFloatField;
    qryProdutonFatorCompra: TBCDField;
    qryCCItemPedidonCdCentroCustoItemPedido: TIntegerField;
    qryInseriItemGrade: TADOQuery;
    qryProdutocReferencia: TStringField;
    qryItemEstoquecReferencia: TStringField;
    Label16: TLabel;
    Label18: TLabel;
    edtTotalLiq: TcxTextEdit;
    edtTotalDesc: TcxTextEdit;
    qryItemEstoquenValTotalProduto: TFloatField;
    qryTerceiroRepresentante: TADOQuery;
    qryTerceiroRepresentantenCdTerceiroRepres: TIntegerField;
    menuOpcao: TPopupMenu;
    btDuplicaPedido: TMenuItem;
    ToolButton13: TToolButton;
    SP_DUPLICA_PEDIDO: TADOStoredProc;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    Image4: TImage;
    tabPreDistrib: TcxTabSheet;
    btnPreDistribuir: TMenuItem;
    qryBuscaCD: TADOQuery;
    popupDistrib: TPopupMenu;
    btVisualizaAlteraDistrib: TMenuItem;
    btExcluirDistrib: TMenuItem;
    gridPreDistribDBTableView1: TcxGridDBTableView;
    gridPreDistribLevel1: TcxGridLevel;
    gridPreDistrib: TcxGrid;
    qryItemPedidoPreDistribuido: TADOQuery;
    dsItemPedidoPreDistribuido: TDataSource;
    qryItemPedidoPreDistribuidonCdItemPedido: TIntegerField;
    qryItemPedidoPreDistribuidocNmProdutoPai: TStringField;
    qryItemPedidoPreDistribuidocNmProduto: TStringField;
    qryItemPedidoPreDistribuidonQtdeDistrib: TIntegerField;
    qryItemPedidoPreDistribuidocNmLoja: TStringField;
    gridPreDistribDBTableView1cNmProdutoPai: TcxGridDBColumn;
    gridPreDistribDBTableView1cNmProduto: TcxGridDBColumn;
    gridPreDistribDBTableView1nQtdeDistrib: TcxGridDBColumn;
    gridPreDistribDBTableView1cNmLoja: TcxGridDBColumn;
    qryItemPedidoPreDistribuidonCdLoja: TIntegerField;
    gridPreDistribDBTableView1nCdLoja: TcxGridDBColumn;
    qryItemPedidoPreDistribuidonQtdePed: TBCDField;
    qryItemPedidoPreDistribuidonSaldo: TBCDField;
    gridPreDistribDBTableView1nQtdePed: TcxGridDBColumn;
    gridPreDistribDBTableView1nSaldo: TcxGridDBColumn;
    qryValidaEmpenho: TADOQuery;
    qryValidaEmpenhonCdItemPedido: TIntegerField;
    qryValidaEmpenhonQtdeAtendida: TBCDField;
    qryValidaEmpenhodDtAtendimento: TDateTimeField;
    qryValidaEmpenhonCdRecebimento: TIntegerField;
    Panel1: TPanel;
    btnVisualizarGrade: TcxButton;
    cxTextEdit1: TcxTextEdit;
    Label23: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label24: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label29: TLabel;
    cxButton3: TcxButton;
    cxButton5: TcxButton;
    btUploadArquivo: TMenuItem;
    btImpPedidoProgr: TMenuItem;
    N2: TMenuItem;
    btImpPedido: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTerceiroAfterScroll(DataSet: TDataSet);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit25KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit24KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit38KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit24Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryItemEstoqueBeforePost(DataSet: TDataSet);
    procedure qryItemEstoqueCalcFields(DataSet: TDataSet);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit19KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueBeforeDelete(DataSet: TDataSet);
    procedure btnVisualizarGradeClick(Sender: TObject);
    procedure qryItemEstoquenValUnitarioValidate(Sender: TField);
    procedure qryItemEstoquenValAcrescimoValidate(Sender: TField);
    procedure qryItemEstoqueAfterDelete(DataSet: TDataSet);
    procedure qryPrazoPedidoBeforePost(DataSet: TDataSet);
    procedure btSugParcelaClick(Sender: TObject);
    procedure btFinalizarPedClick(Sender: TObject);
    procedure qryTempAfterCancel(DataSet: TDataSet);
    procedure qryItemADBeforePost(DataSet: TDataSet);
    procedure qryItemADAfterPost(DataSet: TDataSet);
    procedure qryItemADBeforeDelete(DataSet: TDataSet);
    procedure qryItemADAfterDelete(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh4Enter(Sender: TObject);
    procedure btCancelarPedClick(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh4DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
    procedure qryItemFormulaBeforePost(DataSet: TDataSet);
    procedure qryItemFormulaAfterPost(DataSet: TDataSet);
    procedure qryItemFormulaBeforeDelete(DataSet: TDataSet);
    procedure qryItemFormulaAfterDelete(DataSet: TDataSet);
    procedure DBGridEh6DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure DBGridEh6KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh6ColExit(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure DBGridEh6Enter(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure qryItemLocalEntregaCalcFields(DataSet: TDataSet);
    procedure qryItemLocalEntregaBeforePost(DataSet: TDataSet);
    procedure qryCCItemPedidoBeforePost(DataSet: TDataSet);
    procedure qryCCItemPedidoCalcFields(DataSet: TDataSet);
    procedure DBGridEh8KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBComboItensChange(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure DBEdit19Exit(Sender: TObject);
    procedure ExibirRecebimentosdoItem1Click(Sender: TObject);
    procedure DBEdit21Exit(Sender: TObject);
    procedure DBEdit21KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton5Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure DBEdit9Enter(Sender: TObject);
    procedure DBEdit9Exit(Sender: TObject);
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTituloAbatPedidoBeforePost(DataSet: TDataSet);
    procedure qryTituloAbatPedidoCalcFields(DataSet: TDataSet);
    procedure DBGridEh7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure TabCentroCustoEnter(Sender: TObject);
    procedure DBGridEh1ColEnter(Sender: TObject);
    procedure qryItemEstoqueAfterScroll(DataSet: TDataSet);
    procedure btContatoPadraoClick(Sender: TObject);
    procedure DBEdit11Enter(Sender: TObject);
    procedure enviaPedidoLoja(nCdPedido:integer);
    procedure cxButton7Click(Sender: TObject);
    procedure btDuplicaPedidoClick(Sender: TObject);
    procedure btnPreDistribuirClick(Sender: TObject);
    procedure btVisualizaAlteraDistribClick(Sender: TObject);
    procedure btExcluirDistribClick(Sender: TObject);
    function validaTrocaCabecalhoPreDistrib : boolean;
    function excluiPreDistrib(cCdItemPedidoPai : string) : boolean;
    procedure btUploadArquivoClick(Sender: TObject);
    procedure btImpPedidoProgrClick(Sender: TObject);
    procedure btImpPedidoClick(Sender: TObject);
  private
    { Private declarations }
    nValProdutos : double ;
  public
    { Public declarations }
  end;

var
  frmPedidoComercial: TfrmPedidoComercial;
  cVarejo: string;

implementation

uses Math,fMenu, fLookup_Padrao, rPedidoCom_Simples, fEmbFormula,
  fPrecoEspPedCom, fItemPedidoCompraAtendido, fPedidoComercial_ProgEntrega,
  rPedCom_Contrato, fCentralArquivos, fMontaGrade, fMotivoCancelaSaldoPedido,
  fPedidoComercial_PreDistribuicao;

{$R *.dfm}
var
    objGrade      : TfrmMontaGrade ;
    objEmbFormula : TfrmEmbFormula ;


procedure TfrmPedidoComercial.GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
var
  X:integer;
  dr,dg,db:Extended;
  C1,C2:TColor;
  r1,r2,g1,g2,b1,b2:Byte;
  R,G,B:Byte;
  cnt:integer;
begin
  C1 := FromColor;
  R1 := GetRValue(C1) ;
  G1 := GetGValue(C1) ;
  B1 := GetBValue(C1) ;

  C2 := ToColor;
  R2 := GetRValue(C2) ;
  G2 := GetGValue(C2) ;
  B2 := GetBValue(C2) ;

  dr := (R2-R1) / Rect.Right-Rect.Left;
  dg := (G2-G1) / Rect.Right-Rect.Left;
  db := (B2-B1) / Rect.Right-Rect.Left;

  cnt := 0;
  for X := Rect.Left to Rect.Right-1 do
  begin
    R := R1+Ceil(dr*cnt) ;
    G := G1+Ceil(dg*cnt) ;
    B := B1+Ceil(db*cnt) ;

    Canvas.Pen.Color := RGB(R,G,B) ;
    Canvas.MoveTo(X,Rect.Top) ;
    Canvas.LineTo(X,Rect.Bottom) ;
    inc(cnt) ;
  end;
end;

procedure TfrmPedidoComercial.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PEDIDO' ;
  nCdTabelaSistema  := 30 ;
  nCdConsultaPadrao := 58 ;
  bLimpaAposSalvar  := False ;

  DBGridEh2.Top  := 408;
  DBGridEh2.Left := 80 ;

end;

procedure TfrmPedidoComercial.btIncluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  qryMasterdDtPedido.Value := Date ;
  
  if not qryEmpresa.Active then
  begin
      qryEmpresa.Close ;
      qryEmpresa.Parameters.ParamByName('nPK').Value        := frmMenu.nCdEmpresaAtiva ;
      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryEmpresa.Open ;

      if not qryEmpresa.eof then
      begin
          qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
      end ;
  end ;

  qryUsuarioTipoPedido.Close ;
  qryUsuarioTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryUsuarioTipoPedido.Open ;

  if (qryUsuarioTipoPedido.eof) then
  begin
      ShowMessage('Nenhum tipo de pedido comercial vinculado para este usu�rio.') ;
      btCancelar.Click;
      abort ;
  end ;

  if (qryUsuarioTipoPedido.RecordCount = 1) then
  begin
      PosicionaQuery(qryTipoPedido,qryUsuarioTipoPedidonCdTipoPedido.AsString) ;
      qryMasternCdTipoPedido.Value := qryUsuarioTipoPedidonCdTipoPedido.Value ;
  end ;

  qryUsuarioTipoPedido.Close ;


  If (cVarejo = 'S') then
  begin
      qryUsuarioLoja.Close ;
      qryUsuarioLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryUsuarioLoja.Open ;

      if (qryUsuarioLoja.eof) then
      begin
          ShowMessage('Nenhuma loja vinculada para este usu�rio.') ;
          btCancelar.Click;
          abort ;
      end ;

      if (qryUsuarioLoja.RecordCount = 1) then
      begin
          PosicionaQuery(qryLoja,qryUsuarioLojanCdLoja.AsString) ;
          qryMasternCdLoja.Value := qryUsuarioLojanCdLoja.Value ;
      end ;

      qryUsuarioLoja.Close ;
      DBEdit3.Enabled := True ;

  end
  else
  begin
      desativaDBEdit( DBEdit3 ) ;
  end ;

  if (qryMasternCdTipoPedido.Value = 0) then
      DbEdit4.SetFocus
  Else begin
      DbEdit4.OnExit(nil) ;
      DBEdit5.SetFocus ;
  end ;

  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryEstoque.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value ;

  DBGridEh1.ReadOnly   := False ;
  DBGridEh3.ReadOnly   := False ;
  DBGridEh7.ReadOnly   := False ;
  btSugParcela.Enabled := True ;

  if (DbEdit3.Text = '') and (not DbEdit3.ReadOnly) then
      DBEdit3.SetFocus ;

end;

procedure TfrmPedidoComercial.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTipoPedido.Close ;

end;

procedure TfrmPedidoComercial.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(60,'nCdTabTipoPedido = 2');

            If (nPK > 0) then
            begin
                qryMasternCdTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (Trim(DbEdit4.Text) = '') then
            begin
                ShowMessage('Informe o Tipo de Pedido.') ;
                DBEdit4.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(17,'EXISTS(SELECT 1 FROM TerceiroTipoTerceiro TTT  INNER JOIN TipoPedidoTipoTerceiro TTTP ON TTTP.nCdTipoTerceiro = TTT.nCdTipoTerceiro AND TTTp.nCdTipoPedido = ' + DbEdit4.Text + ' WHERE TTT.nCdTerceiro = vTerceiros.nCdTerceiro)');

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.DBEdit20KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.qryTerceiroAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State <> dsBrowse) then
  begin
    if (qryTerceironCdTerceiroPagador.Value <> 0) and (qryMasternCdTerceiroPagador.Value = 0) then
    begin
      PosicionaQuery(qryTerceiroPagador,qryTerceironCdTerceiroPagador.AsString) ;
      qryMasternCdTerceiroPagador.Value := qryTerceironCdTerceiroPagador.Value ;
    end ;
  end ;

end;

procedure TfrmPedidoComercial.DBEdit23KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.DBEdit25KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.DBEdit24KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(61,'cFlgPdv = 0');

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  if not qryTipoPedido.Active then
  begin
      ShowMessage('Informe o Tipo do Pedido.') ;
      DbEdit4.SetFocus ;
      exit ;
  end ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, dbEdit5.Text) ;

  if not qryTerceiro.active then
      exit ;

  if (qryMaster.State = dsInsert) then
  begin
      if (qryTerceironCdTerceiroTransp.Value > 0) then
      begin
          qryMasternCdTerceiroTransp.Value := qryTerceironCdTerceiroTransp.Value ;
          PosicionaQuery(qryTerceiroTransp,qryTerceironCdTerceiroTransp.asString) ;
      end ;

      if (qryTerceironCdIncoterms.Value > 0) then
      begin
          qryMasternCdIncoterms.Value := qryTerceironCdIncoterms.Value ;
          PosicionaQuery(qryIncoterms,qryTerceironCdIncoterms.asString) ;
      end ;

      if (qryTerceironCdCondPagto.Value > 0) then
      begin
          qryMasternCdCondPagto.Value := qryTerceironCdCondPagto.Value ;
          PosicionaQuery(qryCondPagto, qryTerceironCdCondPagto.asString) ;
      end ;

      qryTerceiroRepresentante.Close;
      qryTerceiroRepresentante.Parameters.ParamByName('nCdTerceiroRepres').Value := frmMenu.nCdTerceiroUsuario;
      qryTerceiroRepresentante.Parameters.ParamByName('nCdTerceiro').Value       := qryTerceironCdTerceiro.Value;
      qryTerceiroRepresentante.Open;

      if (qryTerceiroRepresentantenCdTerceiroRepres.Value > 0) then
      begin
          qryMasternCdTerceiroRepres.Value := qryTerceiroRepresentantenCdTerceiroRepres.Value ;
          PosicionaQuery(qryTerceiroRepres, qryTerceiroRepresentantenCdTerceiroRepres.AsString) ;
      end ;

      qryMasternPercDesconto.Value := qryTerceironPercDesconto.Value ;
      qryMasternPercAcrescimo.Value := qryTerceironPercAcrescimo.Value ;
  end ;

end;

procedure TfrmPedidoComercial.DBEdit38KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(62);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroColab.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmPedidoComercial.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTipoPedido, DBEdit4.Text) ;

  TabItemEstoque.TabVisible := True ;
  TabAD.TabVisible          := False ;
  TabFormula.TabVisible     := False ;
  TabPreDistrib.TabVisible  := False ;

  if not qryTipoPedido.eof then
  begin
    if (qryTipoPedidocFlgItemEstoque.Value = 1) then
    begin
      TabItemEstoque.TabVisible := True ;
      TabItemEstoque.Enabled    := True ;

      TabPreDistrib.TabVisible  := True ;
      TabPreDistrib.Enabled     := True ;
    end ;

    if (qryTipoPedidocFlgItemAD.Value = 1) then
    begin
      TabAD.TabVisible := True ;
      TabAD.Enabled    := True ;
    end ;

    if (qryTipoPedidocFlgItemFormula.Value = 1) then
    begin
      TabFormula.TabVisible := True ;
      TabFormula.Enabled    := True ;
    end ;

    if (qryTipoPedidocGerarFinanc.Value = 1) then
    begin
      TabCentroCusto.Enabled := True ;
      TabParcela.Enabled     := True ;
    end ;

    if (qryTipoPedidocGerarFinanc.Value = 0) then
    begin
      case MessageDlg('Este tipo de pedido n�o gera cobran�a financeira. Confirma o tipo do pedido ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;
    end ;

  end ;

  if not TabItemEstoque.enabled and TabAd.Enabled then
  begin
      cxPageControl1.ActivePageIndex := 1 ;
  end ;

  if not TabItemEstoque.Enabled and not TabAd.Enabled and TabFormula.Enabled then
      cxPageControl1.ActivePageIndex := 2 ;

  if tabItemEstoque.Enabled then
      cxPageControl1.ActivePageIndex := 0 ;

  if (frmMenu.LeParametroEmpresa('USACCCOMPRA') = 'N') then
  begin
      TabCentroCusto.TabVisible := False ;
  end ;

end;

procedure TfrmPedidoComercial.DBEdit3Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  PosicionaQuery(qryLoja, DBEdit3.Text) ;

  if not qryLoja.eof then
      qryEstoque.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value ;

end;

procedure TfrmPedidoComercial.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryEmpresa, DBEdit2.Text) ;
  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;

end;

procedure TfrmPedidoComercial.DBEdit24Exit(Sender: TObject);
begin
  inherited;
  qryCondPagto.Close ;
  PosicionaQuery(qryCondPagto, DBEdit24.Text) ;
end;

procedure TfrmPedidoComercial.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  DBGridEh1.ReadOnly   := False ;
  DBGridEh3.ReadOnly   := False ;
  DBGridEh4.ReadOnly   := False ;
  DBGridEh6.ReadOnly   := False ;
  DBGridEh7.ReadOnly   := False ;

  btSugParcela.Enabled := True ;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
      ShowMessage('Este pedido n�o pertence a esta empresa.') ;
      btCancelar.Click;
      exit ;
  end ;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value    := frmMenu.nCdUsuarioLogado ;
  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;
  PosicionaQuery(qryTipoPedido, qryMasternCdTipoPedido.asString) ;

  if (qryTipoPedido.eof) then
  begin
      ShowMessage('Voc� n�o tem autoriza��o para visualizar este tipo de pedido.') ;
      btCancelar.Click;
      exit;
  end ;

  PosicionaQuery(qryLoja, qryMasternCdLoja.asString) ;

  if not qryLoja.Eof then
      qryEstoque.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value ;

  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;
  PosicionaQuery(qryEstoque, qryMasternCdEstoqueMov.asString) ;
  PosicionaQuery(qryTerceiroColab, qryMasternCdTerceiroColab.asString) ;
  PosicionaQuery(qryTerceiroPagador,qryMasternCdTerceiroPagador.asString) ;
  PosicionaQuery(qryCondPagto, qryMasternCdCondPagto.asString) ;
  PosicionaQuery(qryIncoterms,qryMasternCdIncoterms.asString) ;
  PosicionaQuery(qryTerceiroTransp, qryMasternCdTerceiroTransp.asString) ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryDadoAutorz,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryFollow,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryItemFormula,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryTerceiroRepres, qryMasternCdTerceiroRepres.AsString) ;

  PosicionaQuery(qryItemPedidoPreDistribuido, qryMasternCdPedido.AsString) ;

  qryLocalEstoqueEntrega.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryLocalEstoqueEntrega.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value ;

  PosicionaQuery(qryLocalEstoqueEntrega, qryMasternCdEstoqueMov.AsString) ;

  PosicionaQuery(qryTituloAbatPedido, qryMasternCdPedido.AsString) ;

  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      DBGridEh1.ReadOnly   := True ;
      DBGridEh3.ReadOnly   := True ;
      DBGridEh4.ReadOnly   := True ;
      DBGridEh6.ReadOnly   := True ;
      //DBGridEh7.ReadOnly   := True ;
      btSugParcela.Enabled := False ;
  end ;

  TabItemEstoque.Enabled := False ;
  TabAD.Enabled          := False ;
  TabFormula.Enabled     := False ;
  TabPreDistrib.Enabled  := False ;

  TabItemEstoque.TabStop  := False ;
  TabAD.TabStop           := False ;
  TabParcela.TabStop      := False ;
  TabFollowUp.TabStop     := False ;
  TabFormula.TabStop      := False ;
  TabCentroCusto.TabStop  := False ;
  TabAbatimentos.TabStop  := False ;
  TabPreDistrib.TabStop   := False ;

  TabAd.TabVisible         := False ;
  TabFormula.TabVisible    := False ;
  TabPreDistrib.TabVisible := False ;

  if (qryTipoPedidocFlgItemEstoque.Value = 1) then
  begin
      TabItemEstoque.TabVisible := True ;
      TabItemEstoque.Enabled    := True ;

      TabPreDistrib.TabVisible  := True ;
      TabPreDistrib.Enabled     := True ;
  end ;

  if (qryTipoPedidocFlgItemAD.Value = 1) then
  begin
      TabAD.TabVisible := True ;
      TabAD.Enabled    := True ;
  end ;

  if (qryTipoPedidocFlgItemFormula.Value = 1) then
  begin
      TabFormula.TabVisible := True ;
      TabFormula.Enabled    := True ;
  end ;

  if (qryTipoPedidocGerarFinanc.Value = 1) then
  begin
      TabCentroCusto.Enabled := True ;
      TabParcela.Enabled     := True ;
  end ;

  if not TabItemEstoque.enabled and TabAd.Enabled then
  begin
      cxPageControl1.ActivePageIndex := 1 ;
  end ;

  if not TabItemEstoque.Enabled and not TabAd.Enabled and TabFormula.Enabled then
      cxPageControl1.ActivePageIndex := 2 ;

  if tabItemEstoque.Enabled then
      cxPageControl1.ActivePageIndex := 0 ;

  if (frmMenu.LeParametroEmpresa('USACCCOMPRA') = 'N') then
  begin
      TabCentroCusto.TabVisible := False ;
  end ;

end;

procedure TfrmPedidoComercial.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTipoPedido.Close ;
  qryLoja.Close ;
  qryEstoque.Close ;
  qryTerceiroColab.Close ;
  qryTerceiroPagador.Close ;
  qryCondPagto.Close ;
  qryIncoterms.Close ;
  qryTerceiroTransp.Close ;
  qryTerceiro.Close ;
  qryItemEstoque.Close ;
  qryStatusPed.Close ;
  qryPrazoPedido.Close ;
  qryItemAD.Close ;
  qryDadoAutorz.Close ;
  qryFollow.Close ;
  qryItemFormula.Close ;
  qryTerceiroRepres.Close ;
  qryItemPedidoPreDistribuido.Close ;

  qryItemCC.Close ;
  qryItemLocalEntrega.Close ;
  qryCCItemPedido.Close ;

  qryLocalEstoqueEntrega.Close ;
  qryTituloAbatPedido.Close ;

  TabCentroCusto.Enabled := False ;
  TabParcela.Enabled     := False ;
  TabFormula.Enabled     := False ;
  TabAD.Enabled          := False ;
  TabItemEstoque.Enabled := False ;

  cxPageControl1.ActivePageIndex := 0 ;

  TabItemEstoque.TabStop  := False ;
  TabAD.TabStop           := False ;
  TabParcela.TabStop      := False ;
  TabFollowUp.TabStop     := False ;
  TabFormula.TabStop      := False ;
  TabCentroCusto.TabStop  := False ;
  TabAbatimentos.TabStop  := False ;

  TabAd.TabVisible      := False ;
  TabFormula.TabVisible := False ;

  if (frmMenu.LeParametroEmpresa('USACCCOMPRA') = 'N') then
  begin
      TabCentroCusto.TabVisible := False ;
  end ;

end;

procedure TfrmPedidoComercial.qryItemEstoqueBeforePost(DataSet: TDataSet);
begin

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      ShowMessage('Informe o produto.') ;
      abort ;
  end ;

  qryItemEstoquenCdPedido.Value      := qryMasternCdPedido.Value ;

  if (qryProduto.Active) then
      if (qryProdutonCdGrade.Value = 0) then
          qryItemEstoquenCdTipoItemPed.Value := 2
      else  qryItemEstoquenCdTipoItemPed.Value := 1 ;

  if (qryItemEstoquenQtdePed.Value <= 0) and (qryItemEstoquenQtdePrev.Value <= 0) then
  begin

      if (DBGridEh1.FieldColumns['nQtdePrev'].Visible) then
          MensagemAlerta('Informe a quantidade pedida ou a quantidade prevista.')
      else MensagemAlerta('Informe a quantidade pedida.');

      abort ;
  end ;

  if (cVarejo = 'S') and (qryProdutocFlgProdVenda.Value = 1) and (qryItemEstoquenValSugVenda.Value <= 0) then
  begin
      MensagemAlerta('Informe o pre�o de venda do produto.') ;
      abort ;
  end ;

  inherited;

  // calcula o custo final
  qryItemEstoquenValTotalItem.Value := ((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) * qryItemEstoquenQtdePed.Value) ;

  qryItemEstoquenValIPI.Value     := 0 ;
  qryItemEstoquenValICMSSub.Value := 0 ;

  if (qryItemEstoquenPercIPI.Value > 0) then
      qryItemEstoquenValIPI.Value := (qryItemEstoquenValTotalItem.Value * (qryItemEstoquenPercIPI.Value / 100)) ;

  if (qryItemEstoquenPercICMSSub.Value > 0) then
      qryItemEstoquenValICMSSub.Value := qryItemEstoquenQtdePed.Value * frmMenu.TBRound(((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) * (qryItemEstoquenPercICMSSub.Value / 100)),2) ;

  if (qryItemEstoquenQtdePed.Value > 0) then
  begin
      qryItemEstoquenValCustoUnit.Value := ((((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) * qryItemEstoquenQtdePed.Value) + qryItemEstoquenValIPI.Value + qryItemEstoquenValICMSSub.Value) / qryItemEstoquenQtdePed.Value) ;
      qryItemEstoquenValCustoUnit.Value := frmMenu.TBRound(qryItemEstoquenValCustoUnit.Value,frmMenu.iCasaDecimal) ;
  end ;

  // preco esperado foi zerado , porque estava gerando divergencia indevidamente. 22-12-2010
  qryItemEstoquenValUnitarioEsp.Value := 0 ; // qryItemEstoquenValCustoUnit.Value;
  qryItemEstoquecNmItem.Value         := Uppercase(qryItemEstoquecNmItem.Value) ;

  if (qryItemEstoque.State = dsEdit) then
  begin
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloat(qryItemEstoquenValTotalItem.OldValue) ;
      qryMasternValImposto.Value  := qryMasternValImposto.Value  - StrToFloat(qryItemEstoquenValIPI.OldValue) ;
      qryMasternValImposto.Value  := qryMasternValImposto.Value  - StrToFloat(qryItemEstoquenValICMSSub.OldValue) ;
  end ;

  if (qryItemEstoque.State = dsInsert) then
      qryItemEstoquenCdItemPedido.Value := frmMenu.fnProximoCodigo('ITEMPEDIDO') ;

end;

procedure TfrmPedidoComercial.qryItemEstoqueCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryItemEstoquecNmItem.Value <> '') then
  begin
      qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
      PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

      if not qryProduto.eof then
          qryItemEstoquecReferencia.Value := qryProdutocReferencia.Value;
  end ;

  qryItemEstoquenValTotalProduto.Value := qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value;

end;

procedure TfrmPedidoComercial.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  if (DBGridEh1.Columns[DBGridEh1.SelectedIndex].FieldName = 'nCdProduto') then
  begin
      if (qryItemEstoque.State <> dsBrowse) and (Trim(qryItemEstoquenCdProduto.AsString) <> '') then
      begin

          // Valida a altera��o de quantidade pedida, caso a quantidade anterior j� houver sido pr�-distribuida
          if (qryItemEstoque.State <> dsInsert) then
          begin
              // Valida se existem itens pr�-distribuidos para o recebimento
              qryAux.Close;
              qryAux.SQL.Text := 'SELECT 1'
                               + '  FROM ItemPedidoPreDistribuido I'
                               + ' WHERE EXISTS (SELECT 1'
                               + '                 FROM ItemPedido IP'
                               + '                WHERE IP.nCdItemPedido = I.nCdItemPedido'
                               + '                  AND nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString + ')';
              qryAux.Open;

              if not (qryAux.IsEmpty) then
              begin
                  MensagemAlerta('Para atualizar o produto e quantidade pedida � necess�rio excluir a movimenta��o de pr�-distribui��o.');

                  if not (excluiPreDistrib(qryItemEstoquenCdItemPedido.AsString)) then
                  begin
                      MensagemAlerta('Produto n�o atualizado.');

                      Abort;
                  end
                  else
                  begin
                      qryItemPedidoPreDistribuido.Close;
                      qryItemPedidoPreDistribuido.Open;
                  end;
              end;
          end;

          qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
          PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

          if not qryProduto.eof then
          begin
              qryItemEstoquecNmItem.Value                  := qryProdutocNmProduto.Value ;
              qryItemEstoquecSiglaUnidadeMedida.Value      := qryProdutocUnidadeMedida.Value ;
              qryItemEstoquenFatorConvUnidadeEstoque.Value := qryProdutonFatorCompra.Value ;
          end ;

          if (qryProdutonCdGrade.Value = 0) and (qryItemEstoque.State = dsInsert) then
              qryItemEstoquenQtdePed.Value := qryProdutonQtdeMinimaCompra.Value ;

          if not qryProduto.eof and (qryProdutonCdGrade.Value > 0) then
          begin
              { -- verifica se a grade ao menos tem um item informado cadastrado -- }
              qryAux.Close;
              qryAux.SQL.Clear;
              qryAux.SQL.Add('SELECT 1 FROM ItemGrade WHERE nCdGrade = ' + qryProdutonCdGrade.AsString);
              qryAux.Open;

              if (qryAux.IsEmpty) then
              begin
                  MensagemAlerta('Grade No ' + qryProdutonCdGrade.AsString + ' do produto ' + qryProdutocNmProduto.Value + ' n�o configurado corretamente.');
                  Abort;
              end;

              qryItemEstoquecNmItem.Value             := qryProdutocNmProduto.Value ;
              qryItemEstoquecSiglaUnidadeMedida.Value := qryProdutocUnidadeMedida.Value ;

              objGrade.qryValorAnterior.SQL.Text := '' ;

              if (qryItemEstoquenCdItemPedido.Value > 0) then
                  objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtdePed FROM ItemPedido WITH(INDEX=IN01_ItemPedido) WHERE nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString + ' AND nCdTipoItemPed = 4 ORDER BY ncdProduto') ;

              objGrade.PreparaDataSet(qryItemEstoquenCdProduto.Value);
              objGrade.Renderiza;

              qryItemEstoquenQtdePed.Value := objGrade.nQtdeTotal ;
          end;
      end ;
  end ;

  if (DBGridEh1.Columns[DBGridEh1.SelectedIndex].FieldName = 'nPercDescontoItem') and (qryItemEstoque.State <> dsBrowse) and (qryItemEstoquecNmItem.Value <> '') then
  begin
      qryItemEstoquenValDesconto.Value := 0 ;

      if (qryItemEstoquenPercDescontoItem.Value > 0) then
          qryItemEstoquenValDesconto.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value * (qryItemEstoquenPercDescontoItem.Value / 100))),frmMenu.iCasaDecimal) ;

  end;

end;

procedure TfrmPedidoComercial.FormShow(Sender: TObject);
begin
  inherited;

  objGrade      := TfrmMontaGrade.Create( Self ) ;
  objEmbFormula := TfrmEmbFormula.Create( Self ) ;

  cVarejo  := frmMenu.LeParametro('VAREJO') ;

  cxPageControl1.ActivePageIndex := 0 ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;

  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  If (cVarejo = 'S') then
  begin
      qryUsuarioLoja.Close ;
      qryUsuarioLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryUsuarioLoja.Open ;

      if (qryUsuarioLoja.eof) then
      begin
          ShowMessage('Nenhuma loja vinculada para este usu�rio.') ;
          btCancelar.Click;
          abort ;
      end ;

      qryUsuarioLoja.Close ;
      DBEdit3.Enabled := True ;

  end
  else begin
      desativaDBEdit( DBEdit3 ) ;
  end ;

  // Se utilizar previs�o de entrega nos itens, apaga os campos da tela
  {if (frmMenu.LeParametro('DTPRVENTITEMPED') = 'S') then
  begin
      Label12.Visible  := False ;
      Label13.Visible  := False ;
      DBEdit7.Visible  := False ;
      DBEdit18.Visible := False ;
  end ;}

  TabItemEstoque.TabStop  := False ;
  TabAD.TabStop           := False ;
  TabParcela.TabStop      := False ;
  TabFollowUp.TabStop     := False ;
  TabFormula.TabStop      := False ;
  TabCentroCusto.TabStop  := False ;

  TabAd.TabVisible      := False ;
  TabFormula.TabVisible := False ;

  if (frmMenu.LeParametroEmpresa('USACCCOMPRA') = 'N') then
  begin
      TabCentroCusto.TabVisible := False ;
  end ;

  qryItemEstoquenValUnitario.DisplayFormat  := frmMenu.cMascaraCompras;
  qryItemEstoquenValDesconto.DisplayFormat  := frmMenu.cMascaraCompras;
  qryItemEstoquenValAcrescimo.DisplayFormat := frmMenu.cMascaraCompras;
  qryItemEstoquenValCustoUnit.DisplayFormat := frmMenu.cMascaraCompras;

  qryItemADnValUnitario.DisplayFormat  := frmMenu.cMascaraCompras;
  qryItemADnValCustoUnit.DisplayFormat := frmMenu.cMascaraCompras;

  DBGridEh1.FieldColumns['dDtEntregaIni'].Visible := True;
  DBGridEh1.FieldColumns['dDtEntregaFim'].Visible := True;
  DBGridEh1.FieldColumns['nQtdePrev'].Visible := True;

  if (frmMenu.LeParametro('DTPRVENTITEMPED') = 'N') then
  begin
      DBGridEh1.FieldColumns['dDtEntregaIni'].Visible := False;
      DBGridEh1.FieldColumns['dDtEntregaFim'].Visible := False;
  end ;

  if (frmMenu.LeParametro('QTDEPREVITEMPED') = 'N') then
      DBGridEh1.FieldColumns['nQtdePrev'].Visible := False;

end;

procedure TfrmPedidoComercial.DBEdit19KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(103);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroRepres.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  if not qryItemEstoque.Active then
      PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;

  if not qryItemAD.Active then
      PosicionaQuery(qryItemAD,qryMasternCdPedido.asString) ;

  if not qryItemFormula.Active then
      PosicionaQuery(qryItemFormula,qryMasternCdPedido.asString) ;

  if not qryTituloAbatPedido.Active then
      PosicionaQuery(qryTituloAbatPedido, qryMasternCdPedido.asString) ;

  if not qryItemPedidoPreDistribuido.Active then
      PosicionaQuery(qryItemPedidoPreDistribuido, qryMasternCdPedido.AsString) ;

end;

procedure TfrmPedidoComercial.qryItemEstoqueAfterPost(DataSet: TDataSet);
var
    i : integer ;
begin

  if (objGrade.bMontada) then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM ItemPedido WHERE nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString) ;
      qryAux.ExecSQL;

      objGrade.cdsQuantidade.First;
      objGrade.cdsCodigo.First ;

      for i := 0 to objGrade.cdsQuantidade.FieldList.Count-1 do
      begin
          if (StrToInt(objGrade.cdsQuantidade.Fields[i].Value) > 0) then
          begin
              qryInseriItemGrade.Close ;
              qryInseriItemGrade.Parameters.ParamByName('nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
              qryInseriItemGrade.Parameters.ParamByName('nCdProduto').Value    := StrToInt(objGrade.cdsCodigo.Fields[i].Value) ;
              qryInseriItemGrade.Parameters.ParamByName('iQtde').Value         := StrToInt(objGrade.cdsQuantidade.Fields[i].Value) ;
              qryInseriItemGrade.ExecSQL;
          end ;
      end ;

      objGrade.LiberaGrade;

  end ;

  inherited;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemEstoquenValTotalItem.Value ;
  qryMasternValImposto.Value  := qryMasternValImposto.Value  + qryItemEstoquenValIPI.Value ;
  qryMasternValImposto.Value  := qryMasternValImposto.Value  + qryItemEstoquenValICMSSub.Value ;

  qryMaster.Post ;
end;

procedure TfrmPedidoComercial.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabTipoPedido.Value := qryTipoPedidonCdTabTipoPedido.Value ;
      qryMasternCdTabStatusPed.Value  := 1 ;
  end ;

  if (qryMasternCdEmpresa.Value = 0) then
  begin
      MensagemAlerta('Informe a empresa.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if ((qryMasternCdLoja.Value = 0) or (DbEdit13.Text = '')) and (cVarejo = 'S') then
  begin
      MensagemAlerta('Informe a Loja.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTipoPedido.Value = 0) or (DbEdit14.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de pedido.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdEstoqueMov.Value = 0) or (DBEdit23.Text = '') then
  begin
      MensagemAlerta('Informe o Local de Entrega.') ;
      DbEdit9.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTerceiro.Value = 0) or (DbEdit15.Text = '') then
  begin
      MensagemAlerta('Informe o Terceiro.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  if (qryTipoPedidocFlgCompra.Value = 1) and ((qryMasternCdTerceiroColab.Value = 0) or (DbEdit22.Text = '')) then
  begin
      MensagemAlerta('Informe o Comprador.') ;
      DbEdit21.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPedido.asString = '') then
  begin
      MensagemAlerta('Informe a data do pedido.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;

  if (qryMasternPercDesconto.Value < 0) then
  begin
      MensagemAlerta('Percentual de desconto inv�lido.') ;
      DbEdit8.SetFocus ;
      abort ;
  end ;

  if (qryMasternPercDescontoVencto.Value < 0) then
  begin
      MensagemAlerta('Percentual de desconto inv�lido.') ;
      DbEdit35.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPrevEntIni.asString = '') then
  begin
      MensagemAlerta('Informe a previs�o inicial de entrega.') ;
      DbEdit7.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPrevEntFim.asString = '') then
  begin
      MensagemAlerta('Informe a previs�o final de entrega.') ;
      DbEdit18.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPrevEntFim.Value < qryMasterdDtPrevEntIni.Value) then
  begin
      MensagemAlerta('Previs�o de entrega inv�lida.') ;
      DbEdit7.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPrevEntIni.Value < qryMasterdDtPedido.Value) then
  begin
      MensagemAlerta('A data de previs�o inicial n�o pode ser menor que a data do pedido.') ;
      DBEdit7.SetFocus;
      abort ;
  end ;

  // Valida a troca do cabe�alho do pedido por causa da pr�-distribui��o
  if ((qryMaster.State <> dsInsert) and (frmMenu.LeParametro('CENTRODISTATIVO') = 'S')) then
      if not (validaTrocaCabecalhoPreDistrib) then
      begin
          MensagemAlerta('Efetue a exclus�o dos itens pr�-distribu�dos para salvar as novas altera��es do pedido.');
          Abort;
      end;

  inherited;

  if (qryMaster.State = dsInsert) then
      qryMasternCdTabStatusPed.Value := 1;

  qryMasternValPedido.Value   := qryMasternValOutros.Value + qryMasternValAcrescimo.Value - qryMasternValDesconto.Value + qryMasternValImposto.Value + qryMasternValServicos.Value + qryMasternValProdutos.Value ;

  if (nValProdutos > 0) then
      qryMasternValProdutos.Value := nValProdutos - qryMasternValDesconto.Value;

  nValProdutos := 0 ;
end;

procedure TfrmPedidoComercial.qryTempAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := DBGridEh1.FieldColumns['nQtdePed'].Index;
  end ;

  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmPedidoComercial.qryItemEstoqueBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemEstoquenCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemEstoquenValTotalItem.Value ;
  qryMasternValImposto.Value  := qryMasternValImposto.Value - qryItemEstoquenValIPI.Value - qryItemEstoquenValICMSSub.Value ;

  qryItemPedidoPreDistribuido.Close;
  qryItemPedidoPreDistribuido.Open;
end;

procedure TfrmPedidoComercial.btnVisualizarGradeClick(Sender: TObject);
begin
  if not qryMaster.active then
      Exit;

  if (not qryItemEstoque.Active) or (qryItemEstoquenCdItemPedido.Value = 0) then
  begin
      MensagemAlerta('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
  PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

  if qryProduto.eof then
  begin
      MensagemAlerta('Selecione um item.') ;
      exit ;
  end ;

  if (qryProdutonCdGrade.Value = 0) then
  begin
      MensagemAlerta('Este item n�o est� configurado para trabalhar em grade.') ;
      exit ;
  end ;


  objGrade.qryValorAnterior.SQL.Text := '' ;

  frmMenu.mensagemUsuario('Executando qry Valor anterior');

  if (qryItemEstoquenCdItemPedido.Value > 0) then
      objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtdePed FROM ItemPedido WITH(INDEX=IN01_ItemPedido) WHERE nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString + ' AND nCdTipoItemPed = 4 ORDER BY ncdProduto') ;

  objGrade.PreparaDataSet(qryItemEstoquenCdProduto.Value);

  if (qryMasternCdTabStatusPed.Value >= 3) then
      objGrade.DBGridEh1.ReadOnly := True ;

  frmMenu.mensagemUsuario('Renderiza');

  objGrade.Renderiza;
  objGrade.DBGridEh1.ReadOnly := False ;

  // Valida se existem itens pr�-distribuidos para o recebimento
  qryAux.Close;
  qryAux.SQL.Text := 'SELECT 1'
                   + '  FROM ItemPedidoPreDistribuido I'
                   + ' WHERE EXISTS (SELECT 1'
                   + '                 FROM ItemPedido IP'
                   + '                WHERE IP.nCdItemPedido = I.nCdItemPedido'
                   + '                  AND nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString + ')';
  qryAux.Open;

  if ((not qryAux.IsEmpty) and (qryMasternCdTabStatusPed.Value < 3)) then
  begin
      MensagemAlerta('Para atualizar o produto e quantidade pedida � necess�rio excluir a movimenta��o de pr�-distribui��o.');

      if not (excluiPreDistrib(qryItemEstoquenCdItemPedido.AsString)) then
      begin
          qryItemEstoque.Cancel;

          MensagemAlerta('Produto n�o atualizado.');

          Abort;
      end
      else
      begin
          qryItemPedidoPreDistribuido.Close;
          qryItemPedidoPreDistribuido.Open;
      end;
  end;

  if (qryMasternCdTabStatusPed.Value < 3) then
  begin
      frmMenu.mensagemUsuario('Edita qryItemEstoque');

      qryItemEstoque.Edit ;
      qryItemEstoquenQtdePed.Value := objGrade.nQtdeTotal ;      //retorna valor zero e quantidade do item pedido fica zerado.

      DBGridEh1.Col := DBGridEh1.FieldColumns['nValUnitario'].Index;
      DBGridEh1.SetFocus;
  end ;

end;

procedure TfrmPedidoComercial.qryItemEstoquenValUnitarioValidate(
  Sender: TField);
begin
  inherited;

  if (qryMasternPercDesconto.Value > 0) then
  begin
      qryItemEstoquenPercDescontoItem.Value := qryMasternPercDesconto.Value ;
      qryItemEstoquenValDesconto.Value      := qryItemEstoquenValUnitario.Value * (qryMasternPercDesconto.AsFloat  / 100) ;
  end ;

  if (qryMasternPercAcrescimo.Value > 0) then
      qryItemEstoquenValAcrescimo.Value := qryItemEstoquenValUnitario.Value * (qryMasternPercAcrescimo.AsFloat / 100) ;

end;

procedure TfrmPedidoComercial.qryItemEstoquenValAcrescimoValidate(
  Sender: TField);
begin
  inherited;

  qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) ;
  
  qryItemEstoquenValTotalItem.Value := qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value ;
  
end;

procedure TfrmPedidoComercial.qryItemEstoqueAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmPedidoComercial.qryPrazoPedidoBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryPrazoPedidonCdPedido.Value := qryMasternCdPedido.Value ;

  if (qryPrazoPedidoiDias.Value > 0) then
      qryPrazoPedidodVencto.Value := qryMasterdDtPrevEntIni.Value + qryPrazoPedidoiDias.Value ;

  if (qryPrazoPedidodVencto.asString = '') then
  begin
      MensagemAlerta('Informe a data do vencimento.') ;
      abort ;
  end ;

  if (qryPrazoPedidonValPagto.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

  if (qryPrazoPedido.State = dsInsert) then
      qryPrazoPedidonCdPrazoPedido.Value := frmMenu.fnProximoCodigo('PRAZOPEDIDO') ;

end;

procedure TfrmPedidoComercial.btSugParcelaClick(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternValPedido.Value = 0) then
  begin
      MensagemAlerta('Pedido sem valor.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_Sugere_Parcela.Close ;
      usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Sugere_Parcela.ExecProc ;
  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

end;

procedure TfrmPedidoComercial.btFinalizarPedClick(Sender: TObject);
begin

  nValProdutos := 0 ;

  if not qryMaster.active then
      Exit;
      
  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value <> 1) then
  begin
      MensagemAlerta('Pedido j� finalizado.') ;
      exit ;
  end ;

  if qryItemEstoque.Active then
      qryItemEstoque.First ;

  if qryItemAD.Active then
      qryItemAD.First ;

  if qryItemFormula.Active then
      qryItemformula.First ;
  
  // calcula o total do pedido
  if qryItemEstoque.Active and not qryItemEstoque.eof then
  begin

      qryItemEstoque.First ;

      while not qryItemEstoque.Eof do
      begin
          nValProdutos := nValProdutos + qryItemEstoquenValTotalItem.Value ;
          qryItemEstoque.Next ;
      end ;
      
      qryItemEstoque.First ;

  end ;

  if qryItemAD.Active and not qryItemAD.eof then
  begin

      qryItemAD.First ;

      while not qryItemAD.Eof do
      begin
          nValProdutos := nValProdutos + qryItemADnValTotalItem.Value ;
          qryItemAD.Next ;
      end ;

      qryItemAD.First ;

  end ;

  if qryItemFormula.Active and not qryItemFormula.eof then
  begin

      qryItemFormula.First ;

      while not qryItemFormula.Eof do
      begin
          nValProdutos := nValProdutos + qryItemFormulanValTotalItem.Value ;
          qryItemFormula.Next ;
      end ;

      qryItemFormula.First ;

  end ;

  nValProdutos := nValProdutos + qryMasternValDesconto.Value;

  qryMaster.Edit ;
  qryMasternValProdutos.Value := nValProdutos ;
  qryMaster.Post ;


  if (qryMasternValPedido.Value = 0) then
  begin

      case MessageDlg('Pedido sem valor. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
        mrNo:Abort;
      end;

  end ;

  if (qryTipoPedidocGerarFinanc.Value = 1) then
  begin

      if ((not qryPrazoPedido.Active) or (qryPrazoPedido.RecordCount = 0)) and (qryMasternValPedido.Value = 0) then
      begin
          {}
      end
      else
      begin

          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('SELECT Sum(nValPagto) FROM PrazoPedido WHERE nCdPedido = ' + qryMasternCdPedido.asString) ;
          qryAux.Open ;

          if (qryAux.Eof and (qryMasternValPedido.Value > 0)) or (qryAux.FieldList[0].Value <> qryMasternValPedido.Value) then
          begin
              qryAux.Close ;
              MensagemAlerta('Valor da soma das parcelas � diferente do valor total dos pedidos') ;
              exit ;
          end ;

      end ;

      qryAux.Close ;

      // Confere os centros de custo
      if (frmMenu.LeParametroEmpresa('USACCCOMPRA') = 'S') then
      begin

          cxPageControl1.ActivePage := TabCentroCusto ;

          PosicionaQuery(qryItemCC, qryMasternCdPedido.AsString) ;

          qryItemCC.First;

          while not qryItemCC.Eof do
          begin

              qryAux.Close ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT Sum(nPercent) FROM CentroCustoItemPedido WHERE nCdItemPedido = ' + qryItemCCnCdItemPedido.AsString);
              qryAux.Open ;

              if qryAux.Eof or (qryAux.FieldList[0].Value <> 100) then
              begin
                  MensagemAlerta('O rateio de centro de custo do item � diferente de 100%.' + #13#13 + 'Item: ' + qryItemCCcNmItem.Value) ;
                  exit ;
              end ;

              qryItemCC.Next;
          end ;

          qryItemCC.First;
          
      end ;

  end ;

  case MessageDlg('O pedido ser� finalizado e n�o poder� sofrer altera��es. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Abort;
  end;

  if (qryTipoPedidocGerarFinanc.Value = 0) then
  begin
      case MessageDlg('Este tipo de pedido n�o gera cobran�a financeira. Confirma o tipo do pedido ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      qryMaster.Edit ;
      // verifica se o tipo do pedido exige autoriza��o
      if (qryTipoPedidocExigeAutor.Value = 1) then
          qryMasternCdTabStatusPed.Value := 2  // Aguardando aprova��o
      else begin
          qryMasternCdTabStatusPed.Value := 3 ; // Aprovado
          qryMasterdDtAutor.Value        := Now() ;
          qryMasternCdUsuarioAutor.Value := frmMenu.nCdUsuarioLogado;

      end ;

      qryMasternSaldoFat.Value    := qryMasternValPedido.Value ;
      qryMaster.Post ;

      usp_Finaliza.Close ;
      usp_Finaliza.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Finaliza.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;

      qryMaster.Edit;
      qryMasternCdTabStatusPed.Value := 1 ; // Digitado
      qryMasterdDtAutor.Clear;
      qryMasternCdUsuarioAutor.Clear;
      qryMaster.Post;

      MensagemErro('Erro no processamento.') ;

      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  PosicionaQuery(qryMaster, qryMasternCdPedido.asString) ;

  ShowMessage('Pedido finalizado com sucesso.') ;

end;

procedure TfrmPedidoComercial.qryTempAfterCancel(DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := DBGridEh1.FieldColumns['nQtdePed'].Index;
  end ;

  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmPedidoComercial.qryItemADBeforePost(DataSet: TDataSet);
begin

  if (qryItemADcNmItem.Value = '') then
  begin
      ShowMessage('Informe a descri��o do item.') ;
      abort ;
  end ;

  if (qryItemADcSiglaUnidadeMedida.Value = '') then
  begin
      ShowMessage('Informe a unidade de medida do item.') ;
      abort ;
  end ;

  if (qryItemADnQtdePed.Value <= 0) then
  begin
      ShowMessage('Informe a quantidade do item.') ;
      abort ;
  end ;

  if (qryItemADnValCustoUnit.Value < 0) then
  begin
      ShowMessage('Valor unit�rio inv�lido.') ;
      abort ;
  end ;

  inherited;

  if (qryItemAD.State = dsInsert) then
  begin
  
      qryItemADnCdPedido.Value      := qryMasternCdPedido.Value ;
      qryItemADnCdTipoItemPed.Value := 5 ;
      qryItemADcCdProduto.Value     := 'AD' + qryItemADnCdItemPedido.AsString;
      qryItemADnCdItemPedido.Value  := frmMenu.fnProximoCodigo('ITEMPEDIDO') ;

  end ;

  qryItemADcNmItem.Value             := UpperCase(qryItemADcNmItem.Value) ;
  qryItemADcSiglaUnidadeMedida.Value := UpperCase(qryItemADcSiglaUnidadeMedida.Value) ;

  qryItemADnValTotalItem.Value := qryItemADnQtdePed.Value * qryItemADnValUnitario.Value ;

  qryItemADnValIPI.Value     := 0 ;
  qryItemADnValICMSSub.Value := 0 ;

  if (qryItemADnPercIPI.Value > 0) then
      qryItemADnValIPI.Value := (qryItemADnValTotalItem.Value * (qryItemADnPercIPI.Value / 100)) ;

  if (qryItemADnPercICMSSub.Value > 0) then
      qryItemADnValICMSSub.Value := (qryItemADnValTotalItem.Value * (qryItemADnPercICMSSub.Value / 100)) ;

  qryItemADnValCustoUnit.Value := ((qryItemADnValTotalItem.Value + qryItemADnValIPI.Value + qryItemADnValICMSSub.Value) / qryItemADnQtdePed.Value) ;
  qryItemADnValCustoUnit.Value := frmMenu.TBRound(qryItemADnValCustoUnit.Value,frmMenu.iCasaDecimal) ;

  if (qryItemAD.State = dsEdit) then
  begin
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloat(qryItemADnValTotalItem.OldValue) ;
      qryMasternValImposto.Value  := qryMasternValImposto.Value - StrToFloat(qryItemADnValIPI.OldValue) ;
      qryMasternValImposto.Value  := qryMasternValImposto.Value - StrToFloat(qryItemADnValICMSSub.OldValue) ;
  end ;

end;

procedure TfrmPedidoComercial.qryItemADAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryItemAD.Edit ;
  qryItemADcCdProduto.Value   := 'AD' + qryItemADnCdItemPedido.AsString;
  //qryItemAD.Post ;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemADnValTotalItem.Value ;
  qryMasternValImposto.Value  := qryMasternValImposto.Value + qryItemADnValIPI.Value + qryItemADnValICMSSub.Value ;
  qryMaster.Post ;

end;

procedure TfrmPedidoComercial.qryItemADBeforeDelete(DataSet: TDataSet);
begin
  inherited;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemADnCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemADnValTotalItem.Value ;
  qryMasternValImposto.Value  := qryMasternValImposto.Value  - qryItemADnValIPI.Value - qryItemADnValICMSSub.Value ;

end;

procedure TfrmPedidoComercial.qryItemADAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;
end;

procedure TfrmPedidoComercial.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemEstoque.State = dsBrowse) then
             qryItemEstoque.Edit ;

        if (qryItemEstoque.State = dsInsert) or (qryItemEstoque.State = dsEdit) then
        begin
            If (qryMasternCdTipoPedido.asString = '') then
            begin
                ShowMessage('Selecione um tipo de pedido.') ;
                abort ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(68,'Produto.cFlgProdCompra = 1 AND EXISTS(SELECT 1 FROM GrupoProdutoTipoPedido GPTP WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto AND GPTP.nCdTipoPedido = ' + qryMasternCdTipoPedido.asString + ')');

            If (nPK > 0) then
            begin
                qryItemEstoquenCdProduto.Value := nPK ;

                qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
                PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

                if not qryProduto.eof then
                    qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State = dsInsert) then
  begin
      btSalvar.Click ;
  end ;

  DBGridEh1.FieldColumns['nValDesconto'].ReadOnly := True;

  if (qryTipoPedidocFlgAtuPreco.Value = 1) then
      DBGridEh1.FieldColumns['nValDesconto'].ReadOnly := False;
end;

procedure TfrmPedidoComercial.DBGridEh4Enter(Sender: TObject);
begin
  inherited;
  if (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmPedidoComercial.btCancelarPedClick(Sender: TObject);
var
  objForm : TfrmMotivoCancelaSaldoPedido ;
begin
  if not qryMaster.active then
      Exit;

  if (qryMasternCdTabStatusPed.Value > 2) then
  begin
      MensagemAlerta('O status do pedido n�o permite mais cancelamento.') ;
      exit ;
  end ;

  {-- abre o form que solicita o motivo de cancelamento e PROCESSA o cancelamento do pedido --}
  objForm := TfrmMotivoCancelaSaldoPedido.Create( Self ) ;

  try
      objForm.cancelaSaldoPedido( qryMasternCdPedido.Value , 0 ) ;
  finally
      freeAndNil( objForm ) ;
  end ;

  enviaPedidoLoja(qryMasternCdPedido.Value) ;

  btCancelar.Click;

end;

procedure TfrmPedidoComercial.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (DBGridEh1.Columns[dataCol].FieldName = 'nQtdeExpRec') and not(gdSelected in State) then
  begin
      // recebimento parcial
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) < qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clYellow;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) >= qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clBlue;
        DBGridEh1.Canvas.Font.Color := clYellow ;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  if (DBGridEh1.Columns[dataCol].FieldName = 'nQtdeCanc') and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemEstoquenQtdeCanc.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clRed;
        DBGridEh1.Canvas.Font.Color  := clWhite ;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmPedidoComercial.DBGridEh4DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  If (dataCol = 4) and not(gdSelected in State) then
  begin

      {// recebimento parcial
      if ((qryItemADnQtdeExpRec.Value+qryItemADnQtdeCanc.Value) < qryItemADnQtdePed.Value) and (qryItemADnQtdeExpRec.Value > 0) then
      begin
        DBGridEh4.Canvas.Brush.Color := clYellow;
        DBGridEh4.Canvas.FillRect(Rect);
        DBGridEh4.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemADnQtdeExpRec.Value+qryItemADnQtdeCanc.Value) >= qryItemADnQtdePed.Value) and (qryItemADnQtdeExpRec.Value > 0) then
      begin
        DBGridEh4.Canvas.Brush.Color := clBlue;
        DBGridEh4.Canvas.FillRect(Rect);
        DBGridEh4.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;}

  end ;

  If (dataCol = 5) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemADnQtdeCanc.Value > 0) then
      begin
        DBGridEh4.Canvas.Brush.Color := clRed;
        DBGridEh4.Canvas.FillRect(Rect);
        DBGridEh4.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmPedidoComercial.qryItemFormulaBeforePost(DataSet: TDataSet);
begin

  if (qryItemFormulacNmItem.Value = '') then
  begin
      ShowMessage('Informe a descri��o do item.') ;
      abort ;
  end ;

  if (qryItemFormulanQtdePed.Value <= 0) then
  begin
      ShowMessage('Informe a quantidade do item.') ;
      abort ;
  end ;

  if (qryItemFormulanValCustoUnit.Value < 0) then
  begin
      ShowMessage('Valor unit�rio inv�lido.') ;
      abort ;
  end ;

  inherited;

  if (qryItemFormula.State = dsInsert) then
  begin
      qryItemFormulanCdPedido.Value      := qryMasternCdPedido.Value ;
      qryItemFormulanCdTipoItemPed.Value := 6 ;
      qryItemFormulanCdItemPedido.Value  := frmMenu.fnProximoCodigo('ITEMPEDIDO') ;
  end ;

  qryItemFormulanValTotalItem.Value := qryItemFormulanQtdePed.Value * qryItemFormulanValCustoUnit.Value ;

  if (qryItemFormula.State = dsEdit) then
  begin
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloat(qryItemFormulanValTotalItem.OldValue) ;
  end ;

end;

procedure TfrmPedidoComercial.qryItemFormulaAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemFormulanValTotalItem.Value ;
  qryMaster.Post ;

end;

procedure TfrmPedidoComercial.qryItemFormulaBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;
  qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemFormulanValTotalItem.Value ;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemFormulanCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    ShowMessage('Erro no processamento.') ;
    raise ;
  end ;

end;

procedure TfrmPedidoComercial.qryItemFormulaAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmPedidoComercial.DBGridEh6DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;
  If (dataCol = 4) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemFormulanQtdeExpRec.Value+qryItemFormulanQtdeCanc.Value) < qryItemFormulanQtdePed.Value) and (qryItemFormulanQtdeExpRec.Value > 0) then
      begin
        DBGridEh6.Canvas.Brush.Color := clYellow;
        DBGridEh6.Canvas.FillRect(Rect);
        DBGridEh6.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemFormulanQtdeExpRec.Value+qryItemFormulanQtdeCanc.Value) >= qryItemFormulanQtdePed.Value) and (qryItemFormulanQtdeExpRec.Value > 0) then
      begin
        DBGridEh6.Canvas.Brush.Color := clBlue;
        DBGridEh6.Canvas.FillRect(Rect);
        DBGridEh6.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  If (dataCol = 5) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemFormulanQtdeCanc.Value > 0) then
      begin
        DBGridEh6.Canvas.Brush.Color := clRed;
        DBGridEh6.Canvas.FillRect(Rect);
        DBGridEh6.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmPedidoComercial.DBGridEh6KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemFormula.State = dsBrowse) then
             qryItemFormula.Edit ;


        if (qryItemFormula.State = dsInsert) or (qryItemFormula.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(70,'EXISTS(SELECT 1 FROM FormulaTipoPedido PTP WHERE PTP.nCdProdutoPai = Produto.nCdProduto AND nCdTipoPedido = ' + qryMasternCdTipoPedido.AsString + ')');

            If (nPK > 0) then
            begin
                qryItemFormulanCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmPedidoComercial.DBGridEh6ColExit(Sender: TObject);
begin
  inherited;

  if (DBGridEh6.Col = 1) then
  begin

      if (qryItemFormula.State <> dsBrowse) then
      begin

        PosicionaQuery(qryProdutoFormulado, qryItemFormulanCdProduto.AsString) ;

        if not qryProdutoFormulado.eof then
        begin
            qryItemFormulacNmItem.Value := qryProdutoFormuladocNmProduto.Value ;

            qryAux.Close ;
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('SELECT TOP 1 1 FROM FormulaColuna WHERE nCdProdutoPai = ' + qryItemFormulanCdProduto.asString) ;
            qryAux.Open ;

            if not qryAux.eof then
            begin

                objEmbFormula.nCdPedido       := qryMasternCdPedido.Value          ;
                objEmbFormula.nCdProduto      := qryItemFormulanCdProduto.Value    ;
                objEmbFormula.nCdItemPedido   := qryItemFormulanCdItemPedido.Value ;
                objEmbFormula.nCdTabStatusPed := qryMasternCdTabStatusPed.Value    ;
                objEmbFormula.RenderizaGrade() ;

                // calcula o pre�o
                qryAux.Close ;
                qryAux.SQL.Clear ;
                qryAux.SQL.Add('SELECT Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValUnitProduto,0))') ;
                qryAux.SQL.Add('                ELSE 0       ') ;
                qryAux.SQL.Add('           END)              ') ;
                qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValUnitAmostra,0))') ;
                qryAux.SQL.Add('                ELSE 0       ') ;
                qryAux.SQL.Add('           END)              ') ;
                qryAux.SQL.Add('  FROM ##Temp_Grade_Embalagem') ;
                qryAux.Open ;

                qryItemFormulanValCustoUnit.Value := qryAux.FieldList[0].Value + qryAux.FieldList[1].Value ;

            end ;

            qryAux.Close ;

        end ;

      end ;

  end ;

end;

procedure TfrmPedidoComercial.cxButton2Click(Sender: TObject);
begin

  if not qryItemFormula.Active then
  begin
      ShowMessage('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT TOP 1 1 FROM FormulaColuna WHERE nCdProdutoPai = ' + qryItemFormulanCdProduto.asString) ;
  qryAux.Open ;

  if not qryAux.eof then
  begin

      objEmbFormula.nCdPedido       := qryMasternCdPedido.Value          ;
      objEmbFormula.nCdProduto      := qryItemFormulanCdProduto.Value    ;
      objEmbFormula.nCdItemPedido   := qryItemFormulanCdItemPedido.Value ;
      objEmbFormula.nCdTabStatusPed := qryMasternCdTabStatusPed.Value    ;
      objEmbFormula.RenderizaGrade() ;

      // calcula o pre�o
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValUnitProduto,0))') ;
      qryAux.SQL.Add('                ELSE 0       ') ;
      qryAux.SQL.Add('           END)              ') ;
      qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValUnitAmostra,0))') ;
      qryAux.SQL.Add('                ELSE 0       ') ;
      qryAux.SQL.Add('           END)              ') ;
      qryAux.SQL.Add('  FROM ##Temp_Grade_Embalagem') ;
      qryAux.Open ;

      if (qryMasternCdTabStatusPed.Value <= 1) then
      begin
      
          if (qryItemFormula.State = dsBrowse) then
              qryItemFormula.Edit ;

          qryItemFormulanValCustoUnit.Value := qryAux.FieldList[0].Value + qryAux.FieldList[1].Value ;

          DbGridEh6.SetFocus ;
          DBGridEh6.Col := 3 ;

      end ;

    end
    else begin
        ShowMessage('Produto n�o tem configura��o de embalagem.') ;
    end ;

    qryAux.Close ;


end;

procedure TfrmPedidoComercial.DBGridEh6Enter(Sender: TObject);
begin
  inherited;
  if (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmPedidoComercial.cxButton3Click(Sender: TObject);
var
   objForm : TfrmPrecoEspPedCom ;
begin

    if not qryMaster.active then
        exit ;

    if (qryMasternCdPedido.Value = 0) then
    begin
        ShowMessage('Nenhum pedido ativo.') ;
        exit ;
    end ;

    objForm := TfrmPrecoEspPedCom.Create( Self ) ;

    objForm.nCdPedido := qryMasternCdPedido.Value ;
    showForm( objForm , TRUE ) ;

    enviaPedidoLoja(qryMasternCdPedido.Value) ;
            
end;

procedure TfrmPedidoComercial.qryItemLocalEntregaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryLocalEstoque.Close ;
  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryLocalEstoque.Parameters.ParamByName('nCdProduto').Value := qryItemLocalEntreganCdProduto.Value ;
  PosicionaQuery(qryLocalEstoque, qryItemLocalEntreganCdEstoqueMov.AsString) ;

  if not qryLocalEstoque.eof then
      qryItemLocalEntregacNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;
  
end;

procedure TfrmPedidoComercial.qryItemLocalEntregaBeforePost(
  DataSet: TDataSet);
begin
  if (qryItemLocalEntreganCdEstoqueMov.Value > 0) and (qryItemLocalEntregacNmLocalEstoque.Value = '') then
  begin
      MensagemAlerta('Local de Estoque inv�lido.') ;
      abort ;
  end ;

  inherited;


end;

procedure TfrmPedidoComercial.qryCCItemPedidoBeforePost(DataSet: TDataSet);
begin

  if (qryCCItemPedidonCdCC.Value > 0) and (qryCCItemPedidocNmCC.Value = '') then
  begin
      MensagemAlerta('Informe um centro de custo.') ;
      abort ;
  end ;

  qryCCItemPedidonCdItemPedido.Value := qryItemCCnCdItemPedido.Value ;

  if (qryCCItemPedido.State = dsInsert) then
      qryCCItemPedidonCdCentroCustoItemPedido.Value := frmMenu.fnProximoCodigo('CENTROCUSTOITEMPEDIDO') ;
       
  inherited;

end;

procedure TfrmPedidoComercial.qryCCItemPedidoCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryCentroCusto.Parameters.ParamByName('nCdTipoPedido').Value := qryMasternCdTipoPedido.Value ;
  
  PosicionaQuery(qryCentroCusto, qryCCItemPedidonCdCC.AsString) ;

  if not qryCentroCusto.Eof then
      qryCCItemPedidocNmCC.Value := qryCentroCustocNmCC.Value ;

end;

procedure TfrmPedidoComercial.DBGridEh8KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryCCItemPedido.State = dsBrowse) then
             qryCCItemPedido.Edit ;
        
        if (qryCCItemPedido.State = dsInsert) or (qryCCItemPedido.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(29,'EXISTS(SELECT 1 FROM CentroCustoTipoPedido CCTP WHERE CCTP.nCdCC = CentroCusto.nCdCC AND CCTP.nCdTipoPedido = ' + qryMasternCdTipoPedido.AsString + ')');

            If (nPK > 0) then
            begin
                qryCCItemPedidonCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.DBComboItensChange(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCCItemPedido, qryItemCCnCdItemPedido.AsString) ;

end;

procedure TfrmPedidoComercial.cxButton4Click(Sender: TObject);
begin
  inherited;

  if qryItemCC.Eof or (DBComboItens.Text = '') then
  begin
      MensagemAlerta('Selecione o item a ser copiado.') ;
      exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_copia_cc.Close;
      usp_copia_cc.Parameters.ParamByName('@nCdItemPedido').Value := qryItemCCnCdItemPedido.Value ;
      usp_copia_cc.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

end;

procedure TfrmPedidoComercial.DBEdit19Exit(Sender: TObject);
begin
  inherited;
  qryTerceiroRepres.Close ;
  PosicionaQuery(qryTerceiroRepres, DBEdit19.Text) ;

end;

procedure TfrmPedidoComercial.ExibirRecebimentosdoItem1Click(
  Sender: TObject);
var
  objItemPedidoCompraAtendido : TfrmItemPedidoCompraAtendido ;
begin
  inherited;

  if (cxPageControl1.ActivePage = TabItemEstoque) then
  begin
      if (qryItemEstoque.Active) and (qryItemEstoquenCdItemPedido.Value > 0) then
      begin

          objItemPedidoCompraAtendido := TfrmItemPedidoCompraAtendido.Create( Self ) ;

          PosicionaQuery(objItemPedidoCompraAtendido.qryItem, qryItemEstoquenCdItemPedido.AsString) ;

          showForm( objItemPedidoCompraAtendido , TRUE ) ;

      end ;
  end ;

  if (cxPageControl1.ActivePage = TabAD) then
  begin
      if (qryItemAD.Active) and (qryItemADnCdItemPedido.Value > 0) then
      begin

          objItemPedidoCompraAtendido := TfrmItemPedidoCompraAtendido.Create( Self ) ;

          PosicionaQuery(objItemPedidoCompraAtendido.qryItem, qryItemADnCdItemPedido.AsString) ;

          showForm( objItemPedidoCompraAtendido , TRUE ) ;

      end ;
  end ;

  if (cxPageControl1.ActivePage = TabFormula) then
  begin
      if (qryItemFormula.Active) and (qryItemFormulanCdItemPedido.Value > 0) then
      begin

          objItemPedidoCompraAtendido := TfrmItemPedidoCompraAtendido.Create( Self ) ;

          PosicionaQuery(objItemPedidoCompraAtendido.qryItem, qryItemFormulanCdItemPedido.AsString) ;

          showForm( objItemPedidoCompraAtendido , TRUE ) ;

      end ;
  end ;

end;

procedure TfrmPedidoComercial.DBEdit21Exit(Sender: TObject);
begin
  inherited;

  qryTerceiroColab.Close ;
  PosicionaQuery(qryTerceiroColab, DBEdit21.Text) ;

end;

procedure TfrmPedidoComercial.DBEdit21KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(152);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroColab.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.cxButton5Click(Sender: TObject);
var
  objForm : TfrmPedidoComercial_ProgEntrega ;
begin
  inherited;

  if not qryMaster.eof then
  begin

      objForm := TfrmPedidoComercial_ProgEntrega.Create( Self ) ;

      PosicionaQuery(objForm.qryItemPedido, qryMasternCdPedido.asString) ;

      showForm( objForm , TRUE ) ;

  end ;

end;

procedure TfrmPedidoComercial.cxButton6Click(Sender: TObject);
var
  objRel : TrptPedCom_Contrato ;
begin
  inherited;
  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  objRel := TrptPedCom_Contrato.Create( Self ) ;

  PosicionaQuery(objRel.qryPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(objRel.qryItemEstoque_Grade,qryMasternCdPedido.asString) ;
  PosicionaQuery(objRel.qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(objRel.qryItemFormula,qryMasternCdPedido.asString) ;

  objRel.QRSubDetail1.Enabled := True ;
  objRel.QRSubDetail2.Enabled := True ;
  objRel.QRSubDetail3.Enabled := True ;

  if (objRel.qryItemEstoque_Grade.eof) then
      objRel.QRSubDetail1.Enabled := False ;

  if (objRel.qryItemAD.eof) then
      objRel.QRSubDetail2.Enabled := False ;

  if (objRel.qryItemFormula.eof) then
      objRel.QRSubDetail3.Enabled := False ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmPedidoComercial.DBEdit9Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  IF (not DbEdit3.ReadOnly) and (DbEdit3.Text = '') then
  begin
      MensagemAlerta('Informe a Loja.') ;
      DBEdit3.SetFocus ;
      abort ;
  end ;

  qryLocalEstoqueEntrega.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  if (not DbEdit3.ReadOnly) then
      qryLocalEstoqueEntrega.Parameters.ParamByName('nCdLoja').Value := frmMenu.ConvInteiro(DbEdit3.Text)
  else qryLocalEstoqueEntrega.Parameters.ParamByName('nCdLoja').Value := 0 ;

  if (qryMaster.State = dsInsert) then
      if (DBEdit13.Text <> '') then
      begin
          qryMasternCdEstoqueMov.Value := qryLojanCdLocalEstoquePadrao.Value ;
          PosicionaQuery(qryLocalEstoqueEntrega, qryLojanCdLocalEstoquePadrao.AsString) ;
      end ;

end;

procedure TfrmPedidoComercial.DBEdit9Exit(Sender: TObject);
begin
  inherited;

  qryLocalEstoqueEntrega.Close ;
  PosicionaQuery(qryLocalEstoqueEntrega, DBEdit9.Text) ;

end;

procedure TfrmPedidoComercial.DBEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsBrowse) then
             qryMaster.Edit ;

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva));

            If (nPK > 0) then
            begin
                qryMasternCdEstoqueMov.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.qryTituloAbatPedidoBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryTituloAbatPedidocNmEspTit.Value = '') then
  begin
      MensagemAlerta('Selecione o titulo a ser abatido.') ;
      abort ;
  end ;

  if (qryTituloAbatPedidonPercAbat.Value <= 0) then
  begin
      MensagemAlerta('Informe o percentual de desconto no pedido para o abatimento neste t�tulo.') ;
      abort ;
  end ;

  qryTitulo.Close ;
  qryTitulo.Parameters.ParamByName('nPK').Value         := qryTituloAbatPedidonCdTitulo.Value ;
  qryTitulo.Open ;

  if qryTitulo.eof then
  begin
      MensagemAlerta('T�tulo inv�lido ou n�o pertence a este fornecedor.') ;
      abort ;
  end ;

  if (qryTitulodDtCancel.AsString <> '') then
  begin
      MensagemAlerta('Este t�tulo est� cancelado e n�o pode ser movimentado.') ;
      abort ;
  end ;

  if (qryTitulodDtBloqTit.AsString <> '') then
  begin
      case MessageDlg('Este t�tulo est� bloqueado para movimenta��es. Continuar ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:abort ;
      end ;
  end ;

  if (qryTitulonSaldoTit.Value <= 0) then
  begin
      case MessageDlg('Este t�tulo n�o tem saldo pendente. Continuar ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:abort ;
      end ;
  end ;

  qryTituloAbatPedidonCdPedido.Value := qryMasternCdPedido.Value ;

  if (qryTituloAbatPedido.State = dsInsert) then
      qryTituloAbatPedidonCdTituloAbatPedido.Value := frmMenu.fnProximoCodigo('TITULOABATPEDIDO') ;

end;

procedure TfrmPedidoComercial.qryTituloAbatPedidoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryTituloAbatPedidodDtVenc.asString = '') and (qryTituloAbatPedidonCdTitulo.Value > 0) then
  begin
      qryTitulo.Close ;
      PosicionaQuery(qryTitulo, qryTituloAbatPedidonCdTitulo.AsString) ;

      qryTituloAbatPedidodDtVenc.Value   := qryTitulodDtVenc.Value ;
      qryTituloAbatPedidocNmEspTit.Value := qryTitulocNmEspTit.Value ;
      qryTituloAbatPedidocObsTit.Value   := qryTitulocObsTit.Value ;

  end ;

end;

procedure TfrmPedidoComercial.DBGridEh7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTituloAbatPedido.State = dsBrowse) then
             qryTituloAbatPedido.Edit ;

        if (qryTituloAbatPedido.State = dsInsert) or (qryTituloAbatPedido.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(161,'(Titulo.nCdTerceiro = ' + qryMasternCdTerceiro.AsString + ' OR (Terceiro.nCdGrupoEconomico = (SELECT nCdGrupoEconomico FROM Terceiro Terc2 WHERE Terc2.nCdTerceiro = ' + qryMasternCdTerceiro.AsString + ')))') ; // + ' ORDER BY dDtVenc');

            If (nPK > 0) then
            begin
                qryTituloAbatPedidonCdTitulo.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoComercial.TabCentroCustoEnter(Sender: TObject);
begin
  inherited;
  DBComboItens.SetFocus;

  PosicionaQuery(qryItemCC, qryMasternCdPedido.AsString) ;

  qryItemCC.First;

  DBComboItens.Enabled := True ;

end;

procedure TfrmPedidoComercial.DBGridEh1ColEnter(Sender: TObject);
begin
  inherited;

  if (qryItemEstoque.State = dsInsert) and (DBGridEh1.Col = DBGridEh1.FieldColumns['nPercDescontoItem'].Index) and (qryItemEstoquecNmItem.Value <> '') then
  begin
      qryItemEstoquedDtEntregaIni.Value := qryMasterdDtPrevEntIni.Value ;
      qryItemEstoquedDtEntregaFim.Value := qryMasterdDtPrevEntFim.Value ;

      qryProdutoFornecedor.Close ;
      qryProdutoFornecedor.Parameters.ParamByName('nCdProduto').Value  := qryItemEstoquenCdProduto.Value ;
      qryProdutoFornecedor.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
      qryProdutoFornecedor.Open ;

      if not qryProdutoFornecedor.Eof then
      begin
          qryItemEstoquenValUnitario.Value    := qryProdutoFornecedornPrecoUnit.Value * qryItemEstoquenFatorConvUnidadeEstoque.Value;
          qryItemEstoquenValUnitarioEsp.Value := qryProdutoFornecedornPrecoUnitEsp.Value * qryItemEstoquenFatorConvUnidadeEstoque.Value;
          qryItemEstoquenPercIPI.Value        := qryProdutoFornecedornPercIPI.Value ;
          qryItemEstoquenPercICMSSub.Value    := qryProdutoFornecedornPercICMSSub.Value ;
      end ;

      if (qryItemEstoquenValunitario.Value = 0) and (frmMenu.LeParametro('VAREJO') = 'N') then
      begin
              {-- Localiza o pre�o do �ltimo recebimento --}
              qryAux.Close ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT TOP 1 nValUnitario, nPercIPI');
              qryAux.SQL.Add('  FROM ItemRecebimento');
              qryAux.SQL.Add('       INNER JOIN Recebimento ON Recebimento.nCdRecebimento = ItemRecebimento.nCdRecebimento');
              qryAux.SQL.Add(' WHERE Recebimento.nCdTabStatusReceb >= 4') ;
              qryAux.SQL.Add('   AND Recebimento.nCdEmpresa         = ' + intToStr(frmMenu.nCdEmpresaAtiva)) ;
              qryAux.SQL.Add('   AND nCdProduto                     = ' + qryItemEstoquenCdProduto.AsString) ;
              qryAux.SQL.Add(' ORDER BY dDtReceb DESC ');
              qryAux.Open;

              if not qryAux.Eof then
              begin
                  qryItemEstoquenValUnitario.Value    := qryAux.FieldList[0].Value * qryItemEstoquenFatorConvUnidadeEstoque.Value;
                  qryItemEstoquenValUnitarioEsp.Value := qryAux.FieldList[0].Value * qryItemEstoquenFatorConvUnidadeEstoque.Value;
                  qryItemEstoquenPercIPI.Value        := qryAux.FieldList[1].Value ;
              end
              else
              begin

                  {-- Localiza o pre�o do pedido recebido --}
                  qryAux.Close ;
                  qryAux.SQL.Clear ;
                  qryAux.SQL.Add('SELECT TOP 1 nValUnitario,nValUnitarioEsp, nPercIPI');
                  qryAux.SQL.Add('  FROM ItemPedido');
                  qryAux.SQL.Add('       INNER JOIN Pedido     ON Pedido.nCdPedido = ItemPedido.nCdPedido');
                  qryAux.SQL.Add('       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedido.nCdTipoPedido');
                  qryAux.SQL.Add(' WHERE Pedido.nCdTabStatusPed IN (4,5)');
                  qryAux.SQL.Add('   AND Pedido.nCdEmpresa       = ' + intToStr(frmMenu.nCdEmpresaAtiva)) ;
                  qryAux.SQL.Add('   AND ItemPedido.nCdProduto   = ' + qryItemEstoquenCdProduto.AsString) ;
                  qryAux.SQL.Add('   AND ItemPedido.nQtdeExpRec  > 0');
                  qryAux.SQL.Add('   AND TipoPedido.cFlgCompra   = 1');
                  qryAux.SQL.Add(' ORDER BY dDtPedido DESC');
                  qryAux.Open;

                  if not qryAux.Eof then
                  begin
                      qryItemEstoquenValUnitario.Value    := qryAux.FieldList[0].Value * qryItemEstoquenFatorConvUnidadeEstoque.Value;
                      qryItemEstoquenValUnitarioEsp.Value := qryAux.FieldList[1].Value * qryItemEstoquenFatorConvUnidadeEstoque.Value;
                      qryItemEstoquenPercIPI.Value        := qryAux.FieldList[2].Value ;
                  end ;
              end ;
      end ;

      if (qryItemEstoquenValunitario.Value = 0) and (frmMenu.LeParametro('VAREJO') = 'S') then
      begin

          {-- Localiza o pre�o de custo do produto --}
          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('SELECT TOP 1 nValCusto');
          qryAux.SQL.Add('  FROM Produto');
          qryAux.SQL.Add(' WHERE nCdProdutoPai = ' + qryItemEstoquenCdProduto.AsString) ;
          qryAux.Open;

          if not qryAux.Eof then
          begin
              qryItemEstoquenValUnitario.Value    := qryAux.FieldList[0].Value * qryItemEstoquenFatorConvUnidadeEstoque.Value;
              qryItemEstoquenValUnitarioEsp.Value := qryAux.FieldList[0].Value * qryItemEstoquenFatorConvUnidadeEstoque.Value;
          end ;

      end ;

      qryProdutoFornecedor.Close ;

  end ;

  if (qryItemEstoque.State <> dsBrowse) and (DBGridEh1.Col = DBGridEh1.FieldColumns['nValSugVenda'].Index) and (qryItemEstoquenQtdePed.Value > 0) then
  begin

      qryItemEstoquenValTotalItem.Value := ((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) * qryItemEstoquenQtdePed.Value) ;

      qryItemEstoquenValIPI.Value     := 0 ;
      qryItemEstoquenValICMSSub.Value := 0 ;

      if (qryItemEstoquenPercIPI.Value > 0) then
          qryItemEstoquenValIPI.Value := (qryItemEstoquenValTotalItem.Value * (qryItemEstoquenPercIPI.Value / 100)) ;

      if (qryItemEstoquenPercICMSSub.Value > 0) then
          qryItemEstoquenValICMSSub.Value := qryItemEstoquenQtdePed.Value * frmMenu.TBRound(((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) * (qryItemEstoquenPercICMSSub.Value / 100)),2) ;

      qryItemEstoquenValCustoUnit.Value := ((((qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) * qryItemEstoquenQtdePed.Value) + qryItemEstoquenValIPI.Value + qryItemEstoquenValICMSSub.Value) / qryItemEstoquenQtdePed.Value) ;
      qryItemEstoquenValCustoUnit.Value := frmMenu.TBRound(qryItemEstoquenValCustoUnit.Value,frmMenu.iCasaDecimal) ;

      qryItemEstoquecNmItem.Value := Uppercase(qryItemEstoquecNmItem.Value) ;

      if not qryProduto.eof then
          qryItemEstoquenValSugVenda.Value := qryProdutonValVenda.Value ;

  end ;
end;

procedure TfrmPedidoComercial.qryItemEstoqueAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryItemEstoque.State = dsBrowse) then
  begin

      qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
      PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

      if not qryProduto.eof and (qryProdutonCdGrade.Value = 0) then
      begin
          DBGridEh1.FieldColumns['nQtdePed'].Title.Font.Color := clBlack ;
          DBGridEh1.FieldColumns['nQtdePed'].ReadOnly         := False ;
          DBGridEh1.FieldColumns['nQtdePed'].Font.Color       := clBlack ;

      end ;
      if not qryProduto.eof and (qryProdutonCdGrade.Value > 0) then
      begin

          DBGridEh1.FieldColumns['nQtdePed'].Title.Font.Color := clRed ;
          DBGridEh1.FieldColumns['nQtdePed'].ReadOnly         := True ;
          DBGridEh1.FieldColumns['nQtdePed'].Font.Color       := clTeal ;

      end ;

  end ;

  edtTotalDesc.Text := FormatFloat('###,##0.00',DBGridEh1.FieldColumns['nValDesconto'].Footers[0].SumValue);
  edtTotalLiq.Text  := FormatFloat('###,##0.00',DBGridEh1.FieldColumns['nValTotalProduto'].Footers[0].SumValue);

end;

procedure TfrmPedidoComercial.btContatoPadraoClick(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      exit ;

  if (qryMaster.State = dsBrowse) then
      qryMaster.Edit ;

  qryContatoPadrao.Close ;
  PosicionaQuery(qryContatoPadrao, qryTerceironCdTerceiro.AsString) ;

  if not qryContatoPadrao.Eof then
      qryMastercNmContato.Value := qryContatoPadraocNmContato.Value ;

end;

procedure TfrmPedidoComercial.DBEdit11Enter(Sender: TObject);
begin
  inherited;

  if (DBEdit11.Text = '') and (qryMaster.State = dsInsert) then
      btContatoPadrao.Click;

end;

procedure TfrmPedidoComercial.enviaPedidoLoja(nCdPedido: integer);
begin

    // envia o pedido para as lojas
    SP_ENVIA_PEDIDO_TEMPREGISTRO.Close;
    SP_ENVIA_PEDIDO_TEMPREGISTRO.Parameters.ParamByName('@nCdPedido').Value := nCdPedido ;
    SP_ENVIA_PEDIDO_TEMPREGISTRO.ExecProc;

end;

procedure TfrmPedidoComercial.cxButton7Click(Sender: TObject);
var
  objForm : TfrmMotivoCancelaSaldoPedido ;
begin
  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 2) then
  begin
      MensagemAlerta('O status do pedido n�o permite mais cancelamento.') ;
      exit ;
  end ;

  {-- abre o form que solicita o motivo de cancelamento e PROCESSA o cancelamento do pedido --}
  objForm := TfrmMotivoCancelaSaldoPedido.Create( Self ) ;

  try
      objForm.cancelaSaldoPedido( qryMasternCdPedido.Value , 0 ) ;
  finally
      freeAndNil( objForm ) ;
  end ;

  enviaPedidoLoja(qryMasternCdPedido.Value) ;

  btCancelar.Click;

end;

procedure TfrmPedidoComercial.btDuplicaPedidoClick(Sender: TObject);
var
  nCdPedidoGerado : integer;
begin
  inherited;

  { -- processo removido devido falhas na duplica��o dos itens do pedido -- }
  { -- deve ser analisado os itens e suas refer�ncias antes do uso (Jonathan) -- }
  {if (not qryMaster.Active) then
      abort;

  if (MessageDLG('Confirma a duplica��o deste pedido ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit;

  frmMenu.Connection.BeginTrans;

  try
      SP_DUPLICA_PEDIDO.Close;
      SP_DUPLICA_PEDIDO.Parameters.ParamByName('@nCdPedidoDuplicar').Value := qryMasternCdPedido.Value;
      SP_DUPLICA_PEDIDO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento');
      raise
  end;

  frmMenu.Connection.CommitTrans;

  nCdPedidoGerado := SP_DUPLICA_PEDIDO.Parameters.ParamByName('@nCdPedido').Value;

  btCancelar.Click;

  PosicionaQuery(qryMaster,IntToStr(nCdPedidoGerado));}
end;

procedure TfrmPedidoComercial.btnPreDistribuirClick(Sender: TObject);
var
    objForm : TfrmPedidoComercial_PreDistribuicao;
begin
    inherited;

    if (qryItemEstoque.IsEmpty) then
        Exit;

    if (qryMasternCdTabStatusPed.Value = 10) then
    begin
        MensagemAlerta('Pedido cancelado n�o permite altera��es.');
        Exit;
    end;

    { -- n�o permite altera��es na pr�-distribui��o do item devido o mesmo j� ter sido atendido -- }
    { -- o empenho do item � efetuado no momento da confirma��o do recebimento                  -- }
    qryValidaEmpenho.Close;
    PosicionaQuery(qryValidaEmpenho, qryItemEstoquenCdItemPedido.AsString);

    if (not qryValidaEmpenho.IsEmpty) then
    begin
        MensagemAlerta('Item j� empenhado no estoque n�o permite sua pr�-distribui��o.' + #13#13 + 'Recebimento: ' + qryValidaEmpenhonCdRecebimento.AsString + '.' + #13 + 'Dt. Atend.: ' + DateToStr(qryValidaEmpenhodDtAtendimento.Value) + '.');
        Exit;
    end;

    qryMaster.Post;

    // Trata a pr�-distribui��o de itens do pedido de compra
    if (cxPageControl1.ActivePage = TabItemEstoque) then
    begin

        if (qryItemEstoque.State = dsInsert) then
        begin
            MensagemAlerta('Finalize a inclus�o do item para realizar a pr�-distribui��o.');
            Abort;
        end;

        qryBuscaCD.Close;
        qryBuscaCD.Parameters.ParamByName('nCdLocalEstoque').Value := qryMasternCdEstoqueMov.Value;
        qryBuscaCD.Parameters.ParamByName('nCdUsuario').Value      := frmMenu.nCdUsuarioLogado;
        qryBuscaCD.Parameters.ParamByName('nCdLoja').Value         := qryMasternCdLoja.Value;
        qryBuscaCD.Open;

        if (qryBuscaCD.IsEmpty) then
        begin
            MensagemAlerta('Nenhum centro de distribui��o encontrado para a loja ' + qryMasternCdLoja.AsString + ' - ' + DBEdit13.Text + ' e local de estoque de entrega informados.' + #13 + 'Verifique as configura��es do Centro de Distribui��o e permiss�es de uso para o seu usu�rio.');
            Abort;
        end;

        frmMenu.mensagemUsuario('Montando grade para pr�-distribui��o...');

        objForm := TfrmPedidoComercial_PreDistribuicao.Create(nil);

        objForm.exibeGradePreDistrib(qryMasternCdEstoqueMov.Value, qryItemEstoquenCdItemPedido.Value);

        frmMenu.mensagemUsuario('');

        FreeAndNil(objForm);

        qryItemPedidoPreDistribuido.Close;
        qryItemPedidoPreDistribuido.Open;
    end;
end;

procedure TfrmPedidoComercial.btVisualizaAlteraDistribClick(
  Sender: TObject);
var
  objForm : TfrmPedidoComercial_PreDistribuicao;
begin
  inherited;

  if (qryItemPedidoPreDistribuido.IsEmpty) then
      Exit;

  if (qryMasternCdTabStatusPed.Value = 10) then
  begin
      MensagemAlerta('Pedido cancelado n�o permite altera��es.');
      Exit;
  end;

  { -- n�o permite altera��es na pr�-distribui��o do item devido o mesmo j� ter sido atendido -- }
  { -- o empenho do item � efetuado no momento da confirma��o do recebimento                  -- }
  qryValidaEmpenho.Close;
  PosicionaQuery(qryValidaEmpenho, qryItemEstoquenCdItemPedido.AsString);

  if (not qryValidaEmpenho.IsEmpty) then
  begin
      MensagemAlerta('Item j� empenhado no estoque n�o permite altera��es em sua pr�-distribui��o.' + #13#13 + 'Recebimento: ' + qryValidaEmpenhonCdRecebimento.AsString + '.' + #13 + 'Dt. Atend.: ' + DateToStr(qryValidaEmpenhodDtAtendimento.Value) + '.');
      Exit;
  end;

  if (qryMaster.State = dsEdit) then
      qryMaster.Post;

  qryBuscaCD.Close;
  qryBuscaCD.Parameters.ParamByName('nCdLocalEstoque').Value := qryMasternCdEstoqueMov.Value;
  qryBuscaCD.Parameters.ParamByName('nCdUsuario').Value      := frmMenu.nCdUsuarioLogado;
  qryBuscaCD.Parameters.ParamByName('nCdLoja').Value         := qryMasternCdLoja.Value;
  qryBuscaCD.Open;

  if (qryBuscaCD.IsEmpty) then
  begin
      MensagemAlerta('Nenhum centro de distribui��o encontrado para a loja e local de estoque de entrega informados.');
      Abort;
  end;

  objForm := TfrmPedidoComercial_PreDistribuicao.Create(nil);

  frmMenu.mensagemUsuario('Montando grade para pr�-distribui��o...');

  objForm.exibeGradePreDistrib(qryMasternCdEstoqueMov.Value, qryItemPedidoPreDistribuidonCdItemPedido.Value);

  frmMenu.mensagemUsuario('');

  FreeAndNil(objForm);

  qryItemPedidoPreDistribuido.Close;
  qryItemPedidoPreDistribuido.Open;
end;

procedure TfrmPedidoComercial.btExcluirDistribClick(Sender: TObject);
begin
  inherited;

  if (qryItemPedidoPreDistribuido.IsEmpty) then
      Exit;

  if (qryMasternCdTabStatusPed.Value = 10) then
  begin
      MensagemAlerta('Pedido cancelado n�o permite altera��es.');
      Exit;
  end;

  { -- n�o permite altera��es na pr�-distribui��o do item devido o mesmo j� ter sido atendido -- }
  { -- o empenho do item � efetuado no momento da confirma��o do recebimento                  -- }
  qryValidaEmpenho.Close;
  PosicionaQuery(qryValidaEmpenho, qryItemEstoquenCdItemPedido.AsString);

  if (not qryValidaEmpenho.IsEmpty) then
  begin
      MensagemAlerta('Item j� empenhado no estoque n�o permite a exclus�o de sua pr�-distribui��o.' + #13#13 + 'Recebimento: ' + qryValidaEmpenhonCdRecebimento.AsString + '.' + #13 + 'Dt. Atend.: ' + DateToStr(qryValidaEmpenhodDtAtendimento.Value) + '.');
      Exit;
  end;

  if (excluiPreDistrib(qryItemPedidoPreDistribuidonCdItemPedido.AsString)) then
  begin
      qryItemPedidoPreDistribuido.Close;
      qryItemPedidoPreDistribuido.Open;
  end;
end;

function TfrmPedidoComercial.validaTrocaCabecalhoPreDistrib : boolean;
var
  bRetorno : boolean;
begin
  bRetorno := True;

  // Caso o usu�rio trocar o tipo de pedido, loja ou estoque de entrega
  if (   (qryMasternCdTipoPedido.Value <> qryMasternCdTipoPedido.OldValue)
      or (qryMasternCdLoja.Value       <> qryMasternCdLoja.OldValue)
      or (qryMasternCdEstoqueMov.Value <> qryMasternCdEstoqueMov.OldValue)) then
  begin
      // Valida se existem itens pr�-distribuidos para o recebimento
      qryAux.Close;
      qryAux.SQL.Text := 'SELECT 1'
                       + '  FROM ItemPedidoPreDistribuido I'
                       + ' WHERE EXISTS (SELECT 1'
                       + '                 FROM ItemPedido IP'
                       + '                WHERE IP.nCdItemPedido = I.nCdItemPedido'
                       + '                  AND IP.nCdPedido     = ' + qryMasternCdPedido.AsString + ')';
      qryAux.Open;

      if not (qryAux.IsEmpty) then
      begin
          MensagemAlerta('Existem itens pr�-distribuidos de acordo com o cabe�alho de pedido informado anteriormente.');

          if ((MessageDLG('Para continuar ser� necess�rio excluir as movimenta��es de pr�-distribui��o. Confirma a exclus�o das pr�-distribui��es?',mtConfirmation,[mbYes,mbNo],0)) = mrYes) then
          begin
              qryAux.SQL.Text := 'DELETE FROM ItemPedidoPreDistribuido '
                               + ' WHERE EXISTS (SELECT 1 '
                               + '                 FROM ItemPedido IP '
                               + '                WHERE IP.nCdItemPedido = ItemPedidoPreDistribuido.nCdItemPedido '
                               + '                  AND IP.nCdPedido = ' + qryMasternCdPedido.AsString + ')';

              frmMenu.Connection.BeginTrans;

              try
                  qryAux.Close;
                  qryAux.ExecSQL;
              except
                  frmMenu.Connection.RollbackTrans;
                  MensagemErro('Erro no processamento.');
                  Raise;
                  Abort;
              end;

              frmMenu.Connection.CommitTrans;

              ShowMessage('Movimenta��es de pr�-distribui��o exclu�das com sucesso.');

              qryItemPedidoPreDistribuido.Close;
              qryItemPedidoPreDistribuido.Open;

              bRetorno := True;
          end
          else
              bRetorno := False;
      end;
  end;

  Result := bRetorno;
end;

function TfrmPedidoComercial.excluiPreDistrib(cCdItemPedidoPai : string) : boolean;
var
  bRetorno : boolean;
begin
  if (qryMasternCdTabStatusPed.Value = 10) then
  begin
      MensagemAlerta('Pedido cancelado.');

      Exit;
  end;

  if ((MessageDLG('Confirma a exclus�o da pr�-distribui��o do item?',mtConfirmation,[mbYes,mbNo],0)) = mrYes) then
  begin
      qryAux.SQL.Text := 'DELETE FROM ItemPedidoPreDistribuido '
                       + ' WHERE EXISTS (SELECT 1 '
                       + '                 FROM ItemPedido IP '
                       + '                WHERE IP.nCdItemPedido = ItemPedidoPreDistribuido.nCdItemPedido '
                       + '                  AND nCdItemPedidoPai = ' + cCdItemPedidoPai + ')';

      frmMenu.Connection.BeginTrans;

      try
          qryAux.Close;
          qryAux.ExecSQL;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
          Abort;
      end;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Pr�-distribui��o do item exclu�da com sucesso.');

      bRetorno := True;
  end
  else
      bRetorno := False;

  Result := bRetorno;
end;

procedure TfrmPedidoComercial.btUploadArquivoClick(Sender: TObject);
var
  objForm : TfrmCentralArquivos;
begin
  if not qryMaster.Active then
      exit;

  if (qryMasternCdPedido.Value > 0) then
  begin
      objForm := TfrmCentralArquivos.Create( Self );

      objForm.GerenciaArquivos('PEDIDOCOMPRA',qryMasternCdPedido.Value);

      freeAndNil( objForm );
  end;
end;

procedure TfrmPedidoComercial.btImpPedidoProgrClick(Sender: TObject);
var
  objRel : TrptPedCom_Contrato ;
begin
  inherited;
  
  if not qryMaster.active then
      Exit;

  objRel := TrptPedCom_Contrato.Create( Self ) ;

  PosicionaQuery(objRel.qryPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(objRel.qryItemEstoque_Grade,qryMasternCdPedido.asString) ;
  PosicionaQuery(objRel.qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(objRel.qryItemFormula,qryMasternCdPedido.asString) ;

  objRel.QRSubDetail1.Enabled := True ;
  objRel.QRSubDetail2.Enabled := True ;
  objRel.QRSubDetail3.Enabled := True ;

  if (objRel.qryItemEstoque_Grade.eof) then
      objRel.QRSubDetail1.Enabled := False ;

  if (objRel.qryItemAD.eof) then
      objRel.QRSubDetail2.Enabled := False ;

  if (objRel.qryItemFormula.eof) then
      objRel.QRSubDetail3.Enabled := False ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmPedidoComercial.btImpPedidoClick(Sender: TObject);
var
  objRel : TrptPedidoCom_Simples ;
begin
  if not qryMaster.active then
      Exit;

  if (qryMaster.State = dsInsert) then
  begin
      ShowMessage('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) and (qryMasternCdTabStatusPed.Value = 1) then
  begin
      qryMaster.Post ;
  end ;

  objRel := TrptPedidoCom_Simples.Create( Self ) ;

  objRel.QRLabel2.Enabled   := false ;
  objRel.QRDBText46.Enabled := false ;
  objRel.QRDBText47.Enabled := false ;
  objRel.QRLabel53.Enabled  := false ;


  if (DBGridEh1.FieldColumns['dDtEntregaIni'].Visible) then
  begin
      objRel.QRLabel2.Enabled   := True ;
      objRel.QRDBText46.Enabled := True ;
      objRel.QRDBText47.Enabled := True ;
      objRel.QRLabel53.Enabled  := True ;
  end ;

  objRel.QRLabel36.Enabled  := false ;
  objRel.QRDBText48.Enabled := false ;

  if (DBGridEh1.FieldColumns['nQtdePrev'].Visible) then
  begin
      objRel.QRLabel36.Enabled  := True ;
      objRel.QRDBText48.Enabled := True ;
  end ;

  PosicionaQuery(objRel.qryPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(objRel.qryItemEstoque_Grade,qryMasternCdPedido.asString) ;
  PosicionaQuery(objRel.qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(objRel.qryItemFormula,qryMasternCdPedido.asString) ;

  objRel.QRSubDetail1.Enabled := True ;
  objRel.QRSubDetail2.Enabled := True ;
  objRel.QRSubDetail3.Enabled := True ;

  if (objRel.qryItemEstoque_Grade.eof) then
      objRel.QRSubDetail1.Enabled := False ;

  if (objRel.qryItemAD.eof) then
      objRel.QRSubDetail2.Enabled := False ;

  if (objRel.qryItemFormula.eof) then
      objRel.QRSubDetail3.Enabled := False ;

  objRel.lblEmpresa.Caption := frmMenu.cNmFantasiaEmpresa ;

  if (objRel.qryPedidocRevisaoPedido.Value <> '') then
      objRel.lblTitulo.Caption := 'Pedido de Compra - Revis�o ' + objRel.qryPedidocRevisaoPedido.Value
  else objRel.lblTitulo.Caption := 'Pedido de Compra' ;

  if (objRel.qryPedidocObjetivoPedido.Value <> '') then
      objRel.lblTitulo.Caption := objRel.lblTitulo.Caption + ' - ' + objRel.qryPedidocObjetivoPedido.Value ;

  if (objRel.qryPedidoccodNBR.Value = '') then
  begin
      objRel.QRDBText45.Enabled := false ;
      objRel.QRLabel38.Enabled  := false ;
  end
  else
  begin
      objRel.QRDBText45.Enabled := true ;
      objRel.QRLabel38.Enabled  := true ;
  end ;

  if (objRel.qryPedidocOBS.Value = '') then
      objRel.QRLabel4.Enabled := false
  else objRel.QRLabel4.Enabled := true ;

  if (objRel.qryPedidocCriterioAceite.Value = '') then
      objRel.QRLabel50.Enabled := false
  else objRel.QRLabel50.Enabled := true ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

initialization
    RegisterClass(TfrmPedidoComercial) ;

end.
