unit fGrupoEspTit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmGrupoEspTit = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoEspTit: TIntegerField;
    qryMastercNmGrupoEspTit: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoEspTit: TfrmGrupoEspTit;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmGrupoEspTit.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmGrupoEspTit.FormShow(Sender: TObject);
begin
  cNmTabelaMaster   := 'GRUPOESPTIT' ;
  nCdTabelaSistema  := 6 ;
  nCdConsultaPadrao := 9 ;
  inherited;

end;

initialization
    RegisterClass(tFrmGrupoEspTit) ;

end.
