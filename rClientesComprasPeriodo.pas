unit rClientesComprasPeriodo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DB, Mask, DBCtrls, ADODB, ER2Excel;

type
  TrptClientesComprasPeriodo = class(TfrmRelatorio_Padrao)
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    dsLoja: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    GroupSexo: TRadioGroup;
    GroupModelo: TRadioGroup;
    ER2Excel1: TER2Excel;
    procedure ToolButton1Click(Sender: TObject);
    procedure DBEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure DBEdit1Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptClientesComprasPeriodo: TrptClientesComprasPeriodo;

implementation
uses fMenu, rClientesComprasPeriodo_view, fLookup_Padrao;

procedure Verifica_Data(Componente: TMaskEdit);
begin
  try
    if Componente.Text <> '  /  /    ' Then
      StrToDate(Componente.Text);
  except
    frmMenu.MensagemAlerta('Data Inv�lida');
    Componente.Clear;
    Componente.SetFocus;
    Abort;
  end;
end;


procedure TrptClientesComprasPeriodo.ToolButton1Click(Sender: TObject);
var
    iDia,iMes,iAno  : word;
    cFiltro         : string;
    objRel          : TrptClientesComprasPeriodo_view;
    iLinha          : Integer;
begin
  inherited;

  {-- verifica se a loja est� selecionada --}
  if ((Trim(DBEdit1.Text) = '') or (DBEdit2.Text = '')) then
  begin
      MensagemAlerta('Selecione uma loja');
      Abort;
  end;

  {--valida a data inserida--}

  Verifica_Data(MaskEdit1);
  Verifica_Data(MaskEdit2);

  if (MaskEdit1.Text = '  /  /    ') then
  begin
      DecodeDate(Now,iAno,iMes,iDia);
      MaskEdit1.Text := DateToStr(EncodeDate(iAno,iMes,1));
  end;

  if (MaskEdit2.Text = '  /  /    ') then
      MaskEdit2.Text := DateToStr(Now());

  if (frmMenu.ConvData(MaskEdit1.Text) > frmMenu.ConvData(MaskEdit2.Text)) then
  begin
      MensagemAlerta('Data inicial maior do que a data final');
      Abort;
  end;

  {-- prepara os filtros que ser�o utilizados --}
  cFiltro := '';

  cFiltro := 'Loja de Cadastro: ' + DBEdit2.Text + '/ ';
  cFiltro := cFiltro + 'Per�odo: ' + MaskEdit1.Text + ' � ' + MaskEdit2.Text + '/ ';
  cFiltro := cFiltro + 'Sexo: ' + GroupSexo.Items.Strings[GroupSexo.ItemIndex];

  {-- prepara e executa procedure --}

  objRel := TrptClientesComprasPeriodo_view.Create(nil);

  try
      try
          objRel.SPREL_CLIENTES_COMPRAS_PERIODO.Close;
          objRel.SPREL_CLIENTES_COMPRAS_PERIODO.Parameters.ParamByName('@nCdLojaCadastro').Value := DBEdit1.Text;
          objRel.SPREL_CLIENTES_COMPRAS_PERIODO.Parameters.ParamByName('@nCdTabTipoSexo').Value  := GroupSexo.ItemIndex;
          objRel.SPREL_CLIENTES_COMPRAS_PERIODO.Parameters.ParamByName('@dDtInicial').Value      := MaskEdit1.Text;
          objRel.SPREL_CLIENTES_COMPRAS_PERIODO.Parameters.ParamByName('@dDtFinal').Value        := MaskEdit2.Text;

          objRel.cmdPreparaTemp.Execute;
          objRel.SPREL_CLIENTES_COMPRAS_PERIODO.ExecProc;

          objRel.qryResultado.Close;
          objRel.qryResultado.Open;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro.Caption  := cFiltro;


          {-- Verifica o modelo --}
          case(GroupModelo.ItemIndex) of
              0 : objRel.QuickRep1.PreviewModal; // exibe relat�rio
              1 : begin
                      frmMenu.mensagemUsuario('Exportando Planilha...');

                      {-- Formata��o --}
                      ER2Excel1.Titulo := 'Rel. Cliente Compra Per�odo';

                      ER2Excel1.Celula['A1'].Background := RGB(221, 221, 221);
                      ER2Excel1.Celula['A1'].Negrito;
                      ER2Excel1.Celula['A1'].Range('W6');
                      ER2Excel1.Celula['A1'].Congelar('A7');

                      ER2Excel1.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                      ER2Excel1.Celula['A2'].Text := 'Rel. Cliente Compra Per�odo';
                      ER2Excel1.Celula['A4'].Text := 'Filtro: Loja Cadastro: ' + DBEdit2.Text + ' / Perido: ' + MaskEdit1.Text + ' � ' + MaskEdit2.Text + ' / Sexo: ' + GroupSexo.Items[GroupSexo.ItemIndex];

                      ER2Excel1.Celula['A6'].Text := 'C�d.';
                      ER2Excel1.Celula['B6'].Text := 'Cliente';
                      ER2Excel1.Celula['D6'].Text := 'Dt. Cadastro';
                      ER2Excel1.Celula['F6'].Text := 'Tel. 1';
                      ER2Excel1.Celula['H6'].Text := 'Tel. 2';
                      ER2Excel1.Celula['J6'].Text := 'Celular';
                      ER2Excel1.Celula['L6'].Text := 'Comercial';
                      ER2Excel1.Celula['N6'].Text := 'E-mail';

                      iLinha := 7;

                      while (not objRel.qryResultado.Eof) do
                      begin
                          ER2Excel1.Celula['A' + IntToStr(iLinha)].Text := objRel.qryResultadonCdTerceiro.Value;
                          ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := objRel.qryResultadocNmTerceiro.Value;
                          ER2Excel1.Celula['D' + IntToStr(iLinha)].Text := objRel.qryResultadodDtCadastro.Value;
                          ER2Excel1.Celula['F' + IntToStr(iLinha)].Text := objRel.qryResultadocTelefone1.Value;
                          ER2Excel1.Celula['H' + IntToStr(iLinha)].Text := objRel.qryResultadocTelefone2.Value;
                          ER2Excel1.Celula['J' + IntToStr(iLinha)].Text := objRel.qryResultadocTelefoneMovel.Value;
                          ER2Excel1.Celula['L' + IntToStr(iLinha)].Text := objRel.qryResultadocTelefoneEmpTrab.Value;
                          ER2Excel1.Celula['N' + IntToStr(iLinha)].Text := objRel.qryResultadocEmail.Value;

                          objRel.qryResultado.Next;

                          Inc(iLinha);
                          
                      end;

                      ER2Excel1.ExportXLS;
                      ER2Excel1.CleanupInstance;

                      frmMenu.mensagemUsuario('');

              end;
          end;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel) ;
  end;
end;

procedure TrptClientesComprasPeriodo.DBEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

   case key of
    vk_F4 : begin
            nPK := frmLookup_Padrao.ExecutaConsulta(147);

            If (nPK > 0) then
            begin
                DBEdit1.Text := IntToStr(nPK);
                PosicionaQuery(qryLoja, DBEdit1.Text);
            end ;

    end ;

  end ;
end;

procedure TrptClientesComprasPeriodo.FormCreate(Sender: TObject);
begin
  inherited;

  if(frmMenu.nCdLojaAtiva > 0) then
      PosicionaQuery(qryLoja, IntToStr(frmMenu.nCdLojaAtiva));

end;

procedure TrptClientesComprasPeriodo.DBEdit1Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryLoja,DBEdit1.Text);
end;

initialization
    RegisterClass(TrptClientesComprasPeriodo) ;

{$R *.dfm}

end.
