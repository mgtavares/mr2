inherited frmGrupoPlanoConta: TfrmGrupoPlanoConta
  Left = 1
  Top = 1
  Width = 1022
  Height = 736
  Caption = 'Grupo de Plano de Contas'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1006
    Height = 675
  end
  object Label1: TLabel [1]
    Left = 90
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 79
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 27
    Top = 118
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Resultado DRE'
  end
  object Label4: TLabel [4]
    Left = 51
    Top = 166
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sequ'#234'ncia DRE'
    FocusControl = DBEdit5
  end
  object Label5: TLabel [5]
    Left = 3
    Top = 142
    Width = 125
    Height = 13
    Alignment = taRightJustify
    Caption = 'R - Receita / D - Despesa'
    FocusControl = DBEdit6
  end
  object Label6: TLabel [6]
    Left = 86
    Top = 94
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'M'#225'scara'
    FocusControl = DBEdit7
  end
  inherited ToolBar2: TToolBar
    Width = 1006
  end
  object DBEdit1: TDBEdit [8]
    Tag = 1
    Left = 132
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdGrupoPlanoConta'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [9]
    Left = 132
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmGrupoPlanoConta'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [10]
    Left = 132
    Top = 112
    Width = 65
    Height = 19
    DataField = 'nCdTipoResultadoDRE'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [11]
    Tag = 1
    Left = 204
    Top = 112
    Width = 650
    Height = 19
    DataField = 'cNmTipoResultadoDRE'
    DataSource = DataSource1
    TabOrder = 8
  end
  object DBEdit5: TDBEdit [12]
    Left = 132
    Top = 160
    Width = 65
    Height = 19
    DataField = 'iSeqDRE'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBCheckBox1: TDBCheckBox [13]
    Left = 132
    Top = 184
    Width = 97
    Height = 17
    Caption = 'Exibir DRE'
    DataField = 'cFlgExibeDRE'
    DataSource = dsMaster
    TabOrder = 7
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBEdit6: TDBEdit [14]
    Left = 132
    Top = 136
    Width = 33
    Height = 19
    DataField = 'cFlgRecDes'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit7: TDBEdit [15]
    Left = 132
    Top = 88
    Width = 117
    Height = 19
    DataField = 'cMascaraGrupo'
    DataSource = dsMaster
    TabOrder = 3
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoPlanoConta'
      'WHERE nCdGrupoPlanoConta =:nPK')
    object qryMasternCdGrupoPlanoConta: TIntegerField
      FieldName = 'nCdGrupoPlanoConta'
    end
    object qryMastercNmGrupoPlanoConta: TStringField
      FieldName = 'cNmGrupoPlanoConta'
      Size = 50
    end
    object qryMastercFlgExibeDRE: TIntegerField
      FieldName = 'cFlgExibeDRE'
    end
    object qryMastercFlgRecDes: TStringField
      FieldName = 'cFlgRecDes'
      FixedChar = True
      Size = 1
    end
    object qryMasteriSeqDRE: TIntegerField
      FieldName = 'iSeqDRE'
    end
    object qryMasternCdTipoResultadoDRE: TIntegerField
      FieldName = 'nCdTipoResultadoDRE'
    end
    object qryMasternCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMasterdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryMastercMascaraGrupo: TStringField
      FieldName = 'cMascaraGrupo'
      FixedChar = True
      Size = 12
    end
  end
  object qryTipoResultadoDRE: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoResultadoDRE, cNmTipoResultadoDRE'
      'FROM TipoResultadoDRE'
      'WHERE nCdTipoResultadoDRE = :nPK')
    Left = 520
    Top = 336
    object qryTipoResultadoDREnCdTipoResultadoDRE: TIntegerField
      FieldName = 'nCdTipoResultadoDRE'
    end
    object qryTipoResultadoDREcNmTipoResultadoDRE: TStringField
      FieldName = 'cNmTipoResultadoDRE'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTipoResultadoDRE
    Left = 568
    Top = 392
  end
end
