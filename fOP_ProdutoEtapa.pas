unit fOP_ProdutoEtapa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, cxPC, cxControls, ImgList, ComCtrls, ToolWin,
  ExtCtrls, DBGridEhGrouping, GridsEh, DBGridEh, DB, ADODB;

type
  TfrmOP_ProdutoEtapa = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    qryProdutoEtapa: TADOQuery;
    qryProdutoEtapacNmProduto: TStringField;
    qryProdutoEtapanFatorConversaoUM: TBCDField;
    qryProdutoEtapacUnidadeMedidaEstoque: TStringField;
    qryProdutoEtapacNmLocalEstoque: TStringField;
    qryProdutoFormula: TADOQuery;
    qryProdutoFormulanCdProduto: TIntegerField;
    qryProdutoFormulacNmProduto: TStringField;
    qryProdutoFormulanValCusto: TBCDField;
    qryProdutoFormulacUnidadeMedida: TStringField;
    qryProdutoFormulacFlgFantasma: TIntegerField;
    qryProdutoFormulanCdTipoObtencao: TIntegerField;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    qryProdutoEtapanCdProdutoEtapaOrdemProducao: TIntegerField;
    qryProdutoEtapanCdEtapaOrdemProducao: TIntegerField;
    qryProdutoEtapanCdProduto: TIntegerField;
    qryProdutoEtapanQtdePlanejada: TBCDField;
    qryProdutoEtapanQtdeAtendida: TBCDField;
    qryProdutoEtapaiMetodoUso: TIntegerField;
    qryProdutoEtapacUnidadeMedida: TStringField;
    qryProdutoEtapanCdLocalEstoque: TIntegerField;
    qryProdutoEtapacFlgAltManual: TIntegerField;
    qryProdutoEtapanCdUsuarioAltManual: TIntegerField;
    dsProdutoEtapa: TDataSource;
    qryProdutoEtapacNmUsuario: TStringField;
    qryTabTipoMetodoSaidaEstoque: TADOQuery;
    qryTabTipoMetodoSaidaEstoquecNmTabTipoMetodoSaidaEstoque: TStringField;
    qryProdutoEtapacNmTabTipoMetodoSaidaEstoque: TStringField;
    qryProdutoEtapanCdTabTipoMetodoSaidaEstoque: TIntegerField;
    qryProdutoFormulanCdTabTipoMetodoUso: TIntegerField;
    qryProdutoFormulanCdTabTipoMetodoSaidaEstoque: TIntegerField;
    procedure qryProdutoEtapaCalcFields(DataSet: TDataSet);
    procedure exibeProdutoEtapaProducao( nCdEtapaOrdemProducaoAux : integer ) ;
    procedure qryProdutoEtapaBeforePost(DataSet: TDataSet);
    procedure qryProdutoEtapaBeforeDelete(DataSet: TDataSet);
    procedure qryProdutoEtapaAfterScroll(DataSet: TDataSet);
    procedure qryProdutoEtapaAfterInsert(DataSet: TDataSet);
    procedure qryProdutoEtapacUnidadeMedidaChange(Sender: TField);
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryProdutoEtapanCdLocalEstoqueChange(Sender: TField);
    procedure qryProdutoEtapanCdProdutoChange(Sender: TField);
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    nCdEtapaOrdemProducao : integer ;
  public
    { Public declarations }
  end;

var
  frmOP_ProdutoEtapa: TfrmOP_ProdutoEtapa;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmOP_ProdutoEtapa.exibeProdutoEtapaProducao(
  nCdEtapaOrdemProducaoAux: integer);
begin

    nCdEtapaOrdemProducao := nCdEtapaOrdemProducaoAux ;

    qryProdutoEtapa.Close;
    PosicionaQuery( qryProdutoEtapa , intToStr( nCdEtapaOrdemProducaoAux ) ) ;

    Self.ShowModal ;

end;

procedure TfrmOP_ProdutoEtapa.qryProdutoEtapaCalcFields(DataSet: TDataSet);
begin
  inherited;

  if ( qryProdutoEtapa.State in [dsCalcFields,dsBrowse] ) then
  begin

      qryProdutoFormula.Close;
      PosicionaQuery( qryProdutoFormula , qryProdutoEtapanCdProduto.asString ) ;

      qryLocalEstoque.Close;
      PosicionaQuery( qryLocalEstoque , qryProdutoEtapanCdLocalEstoque.AsString ) ;

      qryTabTipoMetodoSaidaEstoque.Close;
      PosicionaQuery( qryTabTipoMetodoSaidaEstoque , qryProdutoEtapanCdTabTipoMetodoSaidaEstoque.AsString ) ;

      qryUsuario.Close;
      PosicionaQuery( qryUsuario , qryProdutoEtapanCdUsuarioAltManual.AsString ) ;

      if qryProdutoFormula.Active then
      begin
          qryProdutoEtapacNmProduto.Value            := qryProdutoFormulacNmProduto.Value ;
          qryProdutoEtapacUnidadeMedidaEstoque.Value := qryProdutoFormulacUnidadeMedida.Value ;
      end ;

      if qryLocalEstoque.Active then
          qryProdutoEtapacNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;

      if qryTabTipoMetodoSaidaEstoque.Active then
          qryProdutoEtapacNmTabTipoMetodoSaidaEstoque.Value := qryTabTipoMetodoSaidaEstoquecNmTabTipoMetodoSaidaEstoque.Value ;

      if qryUsuario.Active then
          qryProdutoEtapacNmUsuario.Value := qryUsuariocNmUsuario.Value ;

  end ;

end;

procedure TfrmOP_ProdutoEtapa.qryProdutoEtapaBeforePost(DataSet: TDataSet);
begin

  if (qryProdutoEtapacNmProduto.Value = '') then
  begin

      MensagemAlerta('Informe o produto.') ;
      abort

  end ;

  if (qryProdutoEtapanQtdePlanejada.Value < 0) then
  begin

      MensagemAlerta('Quantidade Planejada Inv�lida.') ;
      abort ;

  end ;

  if (qryProdutoEtapacNmLocalEstoque.Value = '') then
  begin

      MensagemAlerta('Informe o local de estoque de sa�da dos produtos.') ;
      abort ;
      
  end ;

  inherited;

  qryProdutoEtapacUnidadeMedida.Value := Uppercase( qryProdutoEtapacUnidadeMedida.Value ) ;

  if (qryProdutoEtapanCdProdutoEtapaOrdemProducao.Value = 0) then
      qryProdutoEtapanCdProdutoEtapaOrdemProducao.Value := frmMenu.fnProximoCodigo('PRODUTOETAPAORDEMPRODUCAO') ;

  if (qryProdutoEtapanCdEtapaOrdemProducao.Value = 0) then
      qryProdutoEtapanCdEtapaOrdemProducao.Value := nCdEtapaOrdemProducao ;

  {-- s� marca esse FLAG quando o produto foi INCLUSO manualmente na etapa da produ��o --}
  if (qryProdutoEtapa.State = dsInsert) then
      qryProdutoEtapacFlgAltManual.Value := 1 ;

  qryProdutoEtapanCdUsuarioAltManual.Value := 1 ;

end;

procedure TfrmOP_ProdutoEtapa.qryProdutoEtapaBeforeDelete(
  DataSet: TDataSet);
begin

  if (qryProdutoEtapanQtdeAtendida.Value > 0) then
  begin

      MensagemAlerta('Imposs�vel excluir um produto que j� teve quantidade atendida.') ;
      abort ;

  end ;

  if (qryProdutoEtapacFlgAltManual.Value = 0) then
  begin

      MensagemAlerta('Imposs�vel excluir um produto autom�tico do roteiro de produ��o.') ;
      abort ;

  end ;

  inherited;

end;

procedure TfrmOP_ProdutoEtapa.qryProdutoEtapaAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  {-- se os dados estiverem para edi��o e o registro n�o foi inclu�do manualmente, desativa as colunas --}
  {-- de unidade de medida da formula e local de estoque de sa�da dos produtos                         --}

  if (not DBGridEh3.Columns[1].ReadOnly) and (qryProdutoEtapacFlgAltManual.Value = 0) and (qryProdutoEtapa.State <> dsInsert) then
  begin

      DBGridEh3.Columns[1].ReadOnly := True ;
      DBGridEh3.Columns[4].ReadOnly := True ;
      DBGridEh3.Columns[8].ReadOnly := True ;

      DBGridEh3.Columns[1].Title.Font.Color := clRed ;
      DBGridEh3.Columns[4].Title.Font.Color := clRed ;
      DBGridEh3.Columns[8].Title.Font.Color := clRed ;

  end ;

end;

procedure TfrmOP_ProdutoEtapa.qryProdutoEtapaAfterInsert(
  DataSet: TDataSet);
begin
  inherited;

  if (DBGridEh3.Columns[1].ReadOnly) then
  begin

      DBGridEh3.Columns[1].ReadOnly := False ;
      DBGridEh3.Columns[4].ReadOnly := False ;
      DBGridEh3.Columns[8].ReadOnly := False ;

      DBGridEh3.Columns[1].Title.Font.Color := clBlack ;
      DBGridEh3.Columns[4].Title.Font.Color := clBlack ;
      DBGridEh3.Columns[8].Title.Font.Color := clBlack ;

  end ;

end;

procedure TfrmOP_ProdutoEtapa.qryProdutoEtapacUnidadeMedidaChange(
  Sender: TField);
begin
  inherited;

  // se a unidade de medida da f�rmula for diferente da unidade de medida do estoque do produto --
  // grava o fator de convers�o de uma unidade para outra                                       --
  if (qryProdutoEtapacUnidadeMedida.Value <> qryProdutoEtapacUnidadeMedidaEstoque.Value) then
      qryProdutoEtapanFatorConversaoUM.Value := frmMenu.fnFatorConversaoUM( qryProdutoEtapacUnidadeMedidaEstoque.Value
                                                                           ,qryProdutoEtapacUnidadeMedida.Value)
  else qryProdutoEtapanFatorConversaoUM.Value := 1 ;

end;

procedure TfrmOP_ProdutoEtapa.DBGridEh3KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        {-- se estiver na coluna de c�digo do produto e a mesma n�o estiver em modo de somente-leitura --}
        if (    (DBGridEh3.Columns[DBGridEh3.Col-1].FieldName = 'nCdProduto')
            and (not DBGridEh3.Columns[DBGridEh3.Col-1].ReadOnly)) then
        begin

            if (qryProdutoEtapa.State = dsBrowse) then
                 qryProdutoEtapa.Edit ;

            if (qryProdutoEtapa.State = dsInsert) or (qryProdutoEtapa.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(76);

                If (nPK > 0) then
                    qryProdutoEtapanCdProduto.Value := nPK ;

            end ;

        end ;

        if (    (DBGridEh3.Columns[DBGridEh3.Col-1].FieldName = 'nCdLocalEstoque')
            and (not DBGridEh3.Columns[DBGridEh3.Col-1].ReadOnly)) then
        begin

            if (qryProdutoEtapa.State = dsBrowse) then
                 qryProdutoEtapa.Edit ;

            if (qryProdutoEtapa.State = dsInsert) or (qryProdutoEtapa.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(87);

                If (nPK > 0) then
                    qryProdutoEtapanCdLocalEstoque.Value := nPK ;

            end ;


        end ;

    end ;

  end ;

end;

procedure TfrmOP_ProdutoEtapa.qryProdutoEtapanCdLocalEstoqueChange(
  Sender: TField);
begin
  inherited;

  if (qryProdutoEtapa.State in [dsInsert,dsEdit]) then
  begin

      qryProdutoEtapacNmLocalEstoque.Value := '' ;

      if (qryProdutoEtapanCdLocalEstoque.Value > 0) then
      begin

          qryLocalEstoque.Close ;
          PosicionaQuery(qryLocalEstoque, qryProdutoEtapanCdLocalEstoque.AsString) ;

          if qryLocalEstoque.Active then
              qryProdutoEtapacNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;

      end ;

  end ;

end;

procedure TfrmOP_ProdutoEtapa.qryProdutoEtapanCdProdutoChange(
  Sender: TField);
begin
  inherited;

  if (qryProdutoEtapa.State in [dsInsert,dsEdit]) then
  begin

      if (qryProdutoEtapanCdProduto.Value > 0) then
      begin

          qryProdutoFormula.Close ;
          PosicionaQuery(qryProdutoFormula, qryProdutoEtapanCdProduto.AsString) ;

          if qryProdutoFormula.Active then
          begin

              qryProdutoEtapacNmProduto.Value            := qryProdutoFormulacNmProduto.Value ;
              qryProdutoEtapacUnidadeMedidaEstoque.Value := qryProdutoFormulacUnidadeMedida.Value ;

              if (qryProdutoEtapacUnidadeMedida.Value = '') then
              begin
                  qryProdutoEtapacUnidadeMedida.Value    := qryProdutoFormulacUnidadeMedida.Value ;
                  qryProdutoEtapanFatorConversaoUM.Value := 1 ;
              end
              else
              begin

                  // se a unidade de medida da f�rmula for diferente da unidade de medida do estoque do produto --
                  // grava o fator de convers�o de uma unidade para outra                                       --
                  if (Uppercase( qryProdutoEtapacUnidadeMedida.Value ) <> Uppercase( qryProdutoEtapacUnidadeMedidaEstoque.Value )) then
                      qryProdutoEtapanFatorConversaoUM.Value := frmMenu.fnFatorConversaoUM( qryProdutoEtapacUnidadeMedidaEstoque.Value
                                                                                           ,qryProdutoEtapacUnidadeMedida.Value)
                  else qryProdutoEtapanFatorConversaoUM.Value := 1 ;

              end ;

          end ;

          if (qryProdutoEtapa.State = dsInsert) then
          begin
              qryProdutoEtapaiMetodoUso.Value                   := qryProdutoFormulanCdTabTipoMetodoUso.Value ;
              qryProdutoEtapanCdTabTipoMetodoSaidaEstoque.Value := qryProdutoFormulanCdTabTipoMetodoSaidaEstoque.Value ;

              {-- quando for um item fantasma, sugere automaticamente como backflush --}
              if (qryProdutoFormulacFlgFantasma.Value = 1) then
                  qryProdutoEtapaiMetodoUso.Value := 2 ;

          end ;

      end ;

  end ;

end;

procedure TfrmOP_ProdutoEtapa.FormShow(Sender: TObject);
begin
  inherited;

  if (DBGridEh3.Columns[1].ReadOnly) and (qryProdutoEtapa.RecordCount = 0) then
  begin

      DBGridEh3.Columns[1].ReadOnly := False ;
      DBGridEh3.Columns[4].ReadOnly := False ;
      DBGridEh3.Columns[8].ReadOnly := False ;

      DBGridEh3.Columns[1].Title.Font.Color := clBlack ;
      DBGridEh3.Columns[4].Title.Font.Color := clBlack ;
      DBGridEh3.Columns[8].Title.Font.Color := clBlack ;

  end ;

  DBGridEh3.SetFocus;
  
end;

procedure TfrmOP_ProdutoEtapa.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmOP_ProdutoEtapa.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

end.
