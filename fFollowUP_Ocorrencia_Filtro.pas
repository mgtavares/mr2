unit fFollowUP_Ocorrencia_Filtro;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, ER2Lookup, DBCtrls, StdCtrls, Mask,fMenu;

type
  TfrmFollowUp_Ocorrencia_Filtro = class(TfrmProcesso_Padrao)
    mskDtOcorrenciaFinal: TMaskEdit;
    mskDtAtendFinal: TMaskEdit;
    Label6: TLabel;
    Label8: TLabel;
    mskDtOcorrenciaInicial: TMaskEdit;
    Label4: TLabel;
    mskDtAtendInicial: TMaskEdit;
    Label3: TLabel;
    edtEmpresa: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    er2LkpLoja: TER2LookupMaskEdit;
    DBEdit1: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    dsEmpresa: TDataSource;
    dsLoja: TDataSource;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojanCdEmpresa: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryTipoOcorrencia: TADOQuery;
    dsTipoOcorrencia: TDataSource;
    qryTipoOcorrencianCdTipoOcorrencia: TIntegerField;
    qryTipoOcorrenciacNmTipoOcorrencia: TStringField;
    Label5: TLabel;
    er2LkpTipoOcorrencia: TER2LookupMaskEdit;
    DBEdit4: TDBEdit;
    procedure qryTipoOcorrenciaBeforeOpen(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFollowUp_Ocorrencia_Filtro: TfrmFollowUp_Ocorrencia_Filtro;

implementation

{$R *.dfm}

procedure TfrmFollowUp_Ocorrencia_Filtro.qryTipoOcorrenciaBeforeOpen(
  DataSet: TDataSet);
begin
  inherited;
  qryTipoOcorrencia.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

procedure TfrmFollowUp_Ocorrencia_Filtro.FormShow(Sender: TObject);
begin
  inherited;
  
  qryEmpresa.Close;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryEmpresa.Parameters.ParamByName('nPK').Value        := frmMenu.nCdEmpresaAtiva;
  qryEmpresa.Open;
end;

end.
