�
 TRPTEXTRATOCONTABANCARIA_VIEW 0AV  TPF0TrptExtratoContaBancaria_viewrptExtratoContaBancaria_viewLeft Top WidthHeightcFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightDataSetSPREL_EXTRATO_BANCARIOFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.Style Functions.Strings
PAGENUMBERCOLUMNNUMBERREPORTTITLE Functions.DATA00'' OptionsFirstPageHeaderLastPageFooter Page.ColumnsPage.Orientation
poPortraitPage.PaperSizeA4Page.Values       �@      ��
@       �@      @�
@       �@       �@           PrinterSettings.CopiesPrinterSettings.DuplexPrinterSettings.FirstPage PrinterSettings.LastPage PrinterSettings.OutputBinAutoPrintIfEmpty	
SnapToGrid	UnitsMMZoomd TQRBandQRBand1Left&Top&Width�Height� Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.ValuesUUUUUUM�@������v�	@ BandTyperbPageHeader TQRShapeQRShape1Left Top$Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values������*�@                ��@������t�	@ 	Pen.WidthShape
qrsHorLine  TQRLabelQRLabel1LeftTopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUUU�@UUUUUU��@�������@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   Rel. Extrato Conta BancáriaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel
lblEmpresaLeftTopWidth3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUUU�@UUUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
lblEmpresaColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData1Left�TopWidth8HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      H�	@UUUUUU��@������*�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	ColorclWhiteDataqrsDateTimeFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFontSize  TQRLabelQRLabel3Left�TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@��������	@UUUUUUU�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   Pág.:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  
TQRSysData
QRSysData2Left�TopWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@TUUUUU��	@UUUUUUU�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	ColorclWhiteDataqrsPageNumberFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentFontSize  TQRLabelQRLabel2Left9Top2WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      Ж@������J�@������
�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionBanco:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel5Left� Top2WidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU��@������J�@������
�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionConta:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText7Left^Top2Width1HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@TUUUUU��@������J�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetSPREL_EXTRATO_BANCARIO	DataFieldnCdBancoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText9Left� Top2Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������"�@������J�@UUUUUU��@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetSPREL_EXTRATO_BANCARIO	DataFieldnCdContaFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel13LeftTopLWidthQHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������*�@UUUUUU�@      P�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Período Lanctos:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel
lblPeriodoLeft^TopLWidth3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@TUUUUU��@UUUUUU�@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
lblPeriodoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel14LeftTopnWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������*�@UUUUUU��@     @�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionDataColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel16LeftJTopnWidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@��������@UUUUUU��@UUUUUU�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption   Tipo LançamentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel17Left�TopnWidth.HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������"�	@UUUUUU��@������j�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption	DocumentoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel18Left!TopnWidth$HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU?�	@UUUUUU��@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   CréditoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRShapeQRShape4Left Top{Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values       �@                ��@������t�	@ Shape
qrsHorLine  TQRShapeQRShape6Left Top�Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@          UUUUUUU� �������t�	@ 	Pen.WidthShape
qrsHorLine  TQRLabelQRLabel6Left/Top?Width)HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@TUUUUU��@      ��@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionTitular:ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText2Left^Top?Width3HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@TUUUUU��@      ��@      ��@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchColorclWhiteDataSetSPREL_EXTRATO_BANCARIO	DataField
cNmTitularFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel7Left� TopnWidth.HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      Ж@UUUUUU��@������j�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption
   HistóricoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel4Left�TopnWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������5�	@UUUUUU��@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionSaldoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabelQRLabel8Left`TopnWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU�	@UUUUUU��@������
�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   DébitoColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand2Left&TopWidth�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values������*�@������v�	@ BandTyperbPageFooter TQRShapeQRShape5Left Top�Width�Height	Frame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values      ��@          UUUUUUU� �������t�	@ 	Pen.WidthShape
qrsHorLine  TQRLabel	QRLabel36LeftTopWidth� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@UUUUUUU�@UUUUUUU�@������Z�@ 	AlignmenttaLeftJustifyAlignToBandAutoSize	AutoStretchCaption6   ER2SOFT - Soluções inteligentes para o seu negócio.ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameCalibri
Font.Style 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel15Left|TopWidthLHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values�������@      X�	@UUUUUUU�@UUUUUU�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionwww.er2soft.com.brColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Height�	Font.NameCalibri
Font.Style 
ParentFontTransparentWordWrap	FontSize   TQRBandDetailBand1Left&Top� Width�HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values       �@������v�	@ BandTyperbDetail 	TQRDBText	QRDBText3LeftTop Width9HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUUU�@                Ж@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetSPREL_EXTRATO_BANCARIO	DataField	dDtLanctoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText4LeftGTop Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������ڻ@          ������g�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretch	ColorclWhiteDataSetSPREL_EXTRATO_BANCARIO	DataFieldcNmTipoLanctoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style OnPrintQRDBText4Print
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText6Left�Top Width:HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@������"�	@          UUUUUUu�@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetSPREL_EXTRATO_BANCARIO	DataField
cDocumentoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText1LeftTop Width:HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@��������	@          UUUUUUu�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetSPREL_EXTRATO_BANCARIO	DataFieldnValCreditoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style OnPrintQRDBText1Print
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText5Left� Top Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      Ж@                Ж@ 	AlignmenttaLeftJustifyAlignToBandAutoSizeAutoStretch	ColorclWhiteDataSetSPREL_EXTRATO_BANCARIO	DataField
cHistoricoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style OnPrintQRDBText4Print
ParentFontTransparentWordWrap	FontSize  	TQRDBText	QRDBText8Left�Top WidthIHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@��������	@          UUUUUU%�@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetSPREL_EXTRATO_BANCARIO	DataFieldnSaldoLanctoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style OnPrintQRDBText8Print
ParentFontTransparentWordWrap	FontSize  	TQRDBText
QRDBText10LeftHTop Width7HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUU%�	@          UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchColorclWhiteDataSetSPREL_EXTRATO_BANCARIO	DataField
nValDebitoFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style OnPrintQRDBText1Print
ParentFontTransparentWordWrap	FontSize   TQRBandQRBand3Left&Top� Width�HeightMFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightAlignToBottomColorclWhiteForceNewColumnForceNewPageSize.Values��������@������v�	@ BandType	rbSummary TQRExprqrexSaldoFinalLeft�Top?WidthHHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@��������	@      ��@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style ColorclWhite
ParentFontResetAfterPrintTransparentWordWrap	
Expression#SPREL_EXTRATO_BANCARIO.nSaldoLanctoMask#,##0.00FontSize  TQRExprqrexSaldoCredLeft�Top%WidthHHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@��������	@��������@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style ColorclWhite
ParentFontResetAfterPrintTransparentWordWrap	
Expression'SUM(SPREL_EXTRATO_BANCARIO.nValCredito)Mask#,##0.00FontSize  TQRExprqrexSaldoAntLeft�TopWidthHHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@��������	@       �@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style ColorclWhite
ParentFontResetAfterPrintTransparentWordWrap	
ExpressionvSPREL_EXTRATO_BANCARIO.nSaldoLancto - SUM(SPREL_EXTRATO_BANCARIO.nValCredito) + SUM(SPREL_EXTRATO_BANCARIO.nValDebito)Mask#,##0.00FontSize  TQRExprqrexSaldoDebLeft�Top2WidthHHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@��������	@������J�@      ��@ 	AlignmenttaRightJustifyAlignToBandAutoSizeAutoStretchFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style ColorclWhite
ParentFontResetAfterPrintTransparentWordWrap	
Expression&SUM(SPREL_EXTRATO_BANCARIO.nValDebito)Mask#,##0.00FontSize  TQRShapeQRShape2Left�TopWidthHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.Values       �@UUUUUUu�	@UUUUUU�@��������@ Shape
qrsHorLine  TQRLabel	QRLabel10Left�Top?Width� HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@�������	@      ��@      `�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption    Saldo Disponível (A + C - D) R$ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel11LeftTop2Width`HeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@�������	@������J�@       �@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Total Débito (D) R$ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel12LeftTop%WidtheHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@UUUUUUE�	@��������@UUUUUU��@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaption   Total Crédito (C) R$ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize  TQRLabel	QRLabel19LeftTopWidthjHeightFrame.ColorclBlackFrame.DrawTopFrame.DrawBottomFrame.DrawLeftFrame.DrawRightSize.ValuesUUUUUU��@      ��	@       �@������:�@ 	AlignmenttaRightJustifyAlignToBandAutoSize	AutoStretchCaptionSaldo Anterior (A) R$ColorclWhiteFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.StylefsBold 
ParentFontTransparentWordWrap	FontSize   TADOStoredProcSPREL_EXTRATO_BANCARIO
ConnectionfrmMenu.ConnectionProcedureNameSPREL_EXTRATO_BANCARIO;1
ParametersName@RETURN_VALUEDataType	ftInteger	DirectionpdReturnValue	Precision
Value  Name@nCdContaBancaria
Attributes
paNullable DataType	ftInteger	Precision
Value  Name@cDtInicial
Attributes
paNullable DataTypeftStringSize
Value
01/01/1900 Name	@cDtFinal
Attributes
paNullable DataTypeftStringSize
Value
01/01/1900  LeftTop` TAutoIncField$SPREL_EXTRATO_BANCARIOnCdItemExtrato	FieldNamenCdItemExtratoReadOnly	  TIntegerField&SPREL_EXTRATO_BANCARIOnCdContaBancaria	FieldNamenCdContaBancaria  TIntegerFieldSPREL_EXTRATO_BANCARIOnCdBanco	FieldNamenCdBanco  TIntegerFieldSPREL_EXTRATO_BANCARIOcAgencia	FieldNamecAgencia  TStringFieldSPREL_EXTRATO_BANCARIOnCdConta	FieldNamenCdConta	FixedChar	Size  TDateTimeFieldSPREL_EXTRATO_BANCARIOdDtLancto	FieldName	dDtLancto  TStringField SPREL_EXTRATO_BANCARIOcHistorico	FieldName
cHistoricoSize2  TStringField SPREL_EXTRATO_BANCARIOcDocumento	FieldName
cDocumentoSize  	TBCDField SPREL_EXTRATO_BANCARIOnValLancto	FieldName
nValLanctoDisplayFormat#,##0.00	PrecisionSize  TStringField SPREL_EXTRATO_BANCARIOcNmTitular	FieldName
cNmTitularSized  TStringField#SPREL_EXTRATO_BANCARIOcNmTipoLancto	FieldNamecNmTipoLanctoSize2  	TBCDField"SPREL_EXTRATO_BANCARIOnSaldoLancto	FieldNamenSaldoLanctoDisplayFormat#,##0.00	PrecisionSize  	TBCDField!SPREL_EXTRATO_BANCARIOnValCredito	FieldNamenValCreditoReadOnly	DisplayFormat#,##0.00	PrecisionSize  	TBCDField SPREL_EXTRATO_BANCARIOnValDebito	FieldName
nValDebitoReadOnly	DisplayFormat#,##0.00	PrecisionSize    