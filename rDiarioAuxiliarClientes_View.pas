unit rDiarioAuxiliarClientes_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, QRCtrls, QuickRpt, ExtCtrls, DB, ADODB;

type
  TrptDiarioAuxiliarClientes_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRGroup1: TQRGroup;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel15: TQRLabel;
    Cancel: TQRShape;
    QRLabel26: TQRLabel;
    QRBand3: TQRBand;
    QRDBText16: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText17: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRExpr6: TQRExpr;
    QRShape4: TQRShape;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRExpr9: TQRExpr;
    QRBand5: TQRBand;
    uspRelatorio: TADOStoredProc;
    uspRelatorionCdTerceiro: TIntegerField;
    uspRelatoriocNmTerceiro: TStringField;
    uspRelatorionCdTitulo: TIntegerField;
    uspRelatoriodDtMovto: TDateTimeField;
    uspRelatoriocNrNF: TStringField;
    uspRelatorionCdParc: TIntegerField;
    uspRelatoriocNrTit: TStringField;
    uspRelatoriocNmOperacao: TStringField;
    uspRelatorionValCredito: TBCDField;
    uspRelatorionValDebito: TBCDField;
    uspRelatorionSaldo: TBCDField;
    uspRelatoriocDocumento: TStringField;
    uspRelatoriocNmUsuario: TStringField;
    uspRelatoriocContaBancaria: TStringField;
    uspRelatoriocNmEspTit: TStringField;
    uspRelatoriocFlgExibeRazao: TIntegerField;
    QRExpr2: TQRExpr;
    uspRelatoriocHistorico: TStringField;
    QRDBText5: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel1: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel5: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptDiarioAuxiliarClientes_View: TrptDiarioAuxiliarClientes_View;

implementation

{$R *.dfm}

end.
