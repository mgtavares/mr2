inherited dcmVendaProdutoClienteWERP: TdcmVendaProdutoClienteWERP
  Left = 0
  Top = 0
  Width = 1280
  Height = 770
  Caption = 'An'#225'lise de Faturamento - WERP'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1264
    Height = 705
  end
  object Label1: TLabel [1]
    Left = 65
    Top = 48
    Width = 41
    Height = 13
    Caption = 'Empresa'
  end
  object Label5: TLabel [2]
    Left = 73
    Top = 72
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
  end
  object Label3: TLabel [3]
    Left = 37
    Top = 192
    Width = 69
    Height = 13
    Caption = 'Per'#237'odo Venda'
  end
  object Label6: TLabel [4]
    Left = 190
    Top = 192
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label4: TLabel [5]
    Left = 37
    Top = 96
    Width = 69
    Height = 13
    Caption = 'Departamento'
  end
  object Label7: TLabel [6]
    Left = 77
    Top = 120
    Width = 29
    Height = 13
    Caption = 'Marca'
  end
  object Label8: TLabel [7]
    Left = 81
    Top = 144
    Width = 25
    Height = 13
    Caption = 'Linha'
  end
  object Label9: TLabel [8]
    Left = 75
    Top = 168
    Width = 31
    Height = 13
    Caption = 'Classe'
  end
  inherited ToolBar1: TToolBar
    Width = 1264
    TabOrder = 8
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [10]
    Tag = 1
    Left = 184
    Top = 40
    Width = 60
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 9
  end
  object DBEdit3: TDBEdit [11]
    Tag = 1
    Left = 248
    Top = 40
    Width = 645
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 10
  end
  object DBEdit9: TDBEdit [12]
    Tag = 1
    Left = 184
    Top = 64
    Width = 120
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = DataSource4
    TabOrder = 12
  end
  object DBEdit10: TDBEdit [13]
    Tag = 1
    Left = 312
    Top = 64
    Width = 645
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource4
    TabOrder = 13
  end
  object MaskEdit1: TMaskEdit [14]
    Left = 112
    Top = 184
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [15]
    Left = 210
    Top = 184
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [16]
    Tag = 1
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
  end
  object MaskEdit6: TMaskEdit [17]
    Tag = 1
    Left = 112
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
  end
  object MaskEdit5: TMaskEdit [18]
    Left = 112
    Top = 88
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object DBEdit4: TDBEdit [19]
    Tag = 1
    Left = 184
    Top = 88
    Width = 650
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = dsDepartamento
    TabOrder = 11
  end
  object MaskEdit7: TMaskEdit [20]
    Left = 112
    Top = 112
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object DBEdit5: TDBEdit [21]
    Tag = 1
    Left = 184
    Top = 112
    Width = 650
    Height = 21
    DataField = 'cNmMarca'
    DataSource = dsMarca
    TabOrder = 14
  end
  object MaskEdit8: TMaskEdit [22]
    Left = 112
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = MaskEdit8Exit
    OnKeyDown = MaskEdit8KeyDown
  end
  object DBEdit6: TDBEdit [23]
    Tag = 1
    Left = 184
    Top = 136
    Width = 650
    Height = 21
    DataField = 'cNmLinha'
    DataSource = dsLinha
    TabOrder = 15
  end
  object MaskEdit9: TMaskEdit [24]
    Left = 112
    Top = 160
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = MaskEdit9Exit
    OnKeyDown = MaskEdit9KeyDown
  end
  object DBEdit7: TDBEdit [25]
    Tag = 1
    Left = 184
    Top = 160
    Width = 650
    Height = 21
    DataField = 'cNmClasseProduto'
    DataSource = dsClasseProduto
    TabOrder = 16
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 304
  end
  object DataSource2: TDataSource
    Left = 632
    Top = 312
  end
  object DataSource3: TDataSource
    Left = 640
    Top = 320
  end
  object DataSource4: TDataSource
    DataSet = qryTerceiro
    Left = 648
    Top = 328
  end
  object DataSource5: TDataSource
    Left = 656
    Top = 336
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoEconomico'
      'WHERE nCdGrupoEconomico = :nPK')
    Left = 424
    Top = 288
    object qryGrupoEconomiconCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object dsGrupoEconomico: TDataSource
    DataSet = qryGrupoEconomico
    Left = 480
    Top = 280
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento, cNmDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 784
    Top = 256
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarca, cNmMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 824
    Top = 256
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'npk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha'
      'WHERE nCdLinha = :npk')
    Left = 864
    Top = 256
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object qryClasseProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM ClasseProduto'
      'WHERE nCdClasseProduto = :nPK')
    Left = 904
    Top = 248
    object qryClasseProdutonCdClasseProduto: TAutoIncField
      FieldName = 'nCdClasseProduto'
      ReadOnly = True
    end
    object qryClasseProdutocNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
  end
  object dsDepartamento: TDataSource
    DataSet = qryDepartamento
    Left = 784
    Top = 288
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 824
    Top = 296
  end
  object dsLinha: TDataSource
    DataSet = qryLinha
    Left = 864
    Top = 296
  end
  object dsClasseProduto: TDataSource
    DataSet = qryClasseProduto
    Left = 912
    Top = 296
  end
  object qryRamoAtividade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM RamoAtividade'
      'WHERE nCdRamoAtividade = :nPK')
    Left = 496
    Top = 360
    object qryRamoAtividadenCdRamoAtividade: TIntegerField
      FieldName = 'nCdRamoAtividade'
    end
    object qryRamoAtividadecNmRamoAtividade: TStringField
      FieldName = 'cNmRamoAtividade'
      Size = 50
    end
  end
  object DataSource6: TDataSource
    DataSet = qryRamoAtividade
    Left = 632
    Top = 376
  end
  object qryTerceiroRepres: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM TerceiroTipoTerceiro TTT'
      '               WHERE TTT.nCdTerceiro     = Terceiro.nCdTerceiro'
      '                 AND TTT.nCdTipoTerceiro = 4)')
    Left = 448
    Top = 400
    object qryTerceiroRepresnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroReprescNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource7: TDataSource
    DataSet = qryTerceiroRepres
    Left = 640
    Top = 384
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 488
    Top = 112
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nCdTerceiro')
    Left = 616
    Top = 104
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
end
