unit fConsignacao_Leitor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DBGridEhGrouping, GridsEh, DBGridEh, ImgList,
  ComCtrls, ToolWin, ExtCtrls, DB, ADODB, ToolCtrlsEh;

type
  TfrmConsignacao_Leitor = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryProduto: TADOQuery;
    dsTempItemConsig: TDataSource;
    qryPreparaTemp: TADOQuery;
    cmdTempItemConsig: TADOCommand;
    qryTempItemConsig: TADOQuery;
    qryTempItemConsignCdProduto: TIntegerField;
    qryTempItemConsigcNmProduto: TStringField;
    qryTempItemConsigcEAN: TStringField;
    qryTempItemConsigcReferencia: TStringField;
    qryTempItemConsignValVenda: TBCDField;
    SP_ITENS_LEITOR_CONSIG: TADOStoredProc;
    SP_DEVOLUCAO_ITEM_CONSIG: TADOStoredProc;
    qryTempItens: TADOQuery;
    qryTempItensnCdProduto: TIntegerField;
    qryTempItensnQtdeRetirada: TIntegerField;
    qryValidaQtde: TADOQuery;
    qryQtdeProdutos: TADOQuery;
    qryValidaQtdenQtdeRetirada: TIntegerField;
    qryQtdeProdutosnQtdeDevolvida: TIntegerField;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryProdutocEAN: TStringField;
    qryProdutocReferencia: TStringField;
    qryProdutonValVenda: TBCDField;
    SP_BUSCAPRODUTO_ITEMPEDIDO: TADOStoredProc;
    SP_BUSCAPRODUTO_ITEMPEDIDOnCdProduto: TIntegerField;
    SP_BUSCAPRODUTO_ITEMPEDIDOcNmProduto: TStringField;
    qryTempItemConsignCdTempItemConsig: TAutoIncField;
    procedure FormCreate(Sender: TObject);
    procedure qryTempItemConsigBeforePost(DataSet: TDataSet);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
     nCdConsignacao  : Integer;
     nCdLocalEstoque : Integer;
     nCdAcao         : Integer;
     iQtdeDev        : Integer;
     bProcessado     : Boolean;
  end;

var
  frmConsignacao_Leitor: TfrmConsignacao_Leitor;

implementation

  uses fMenu, fConsignacao;

{$R *.dfm}

procedure TfrmConsignacao_Leitor.FormCreate(Sender: TObject);
begin
  inherited;

  {Cria e popula as tabelas tempor�rias}

  cmdTempItemConsig.Execute;

  qryPreparaTemp.ExecSQL;

  qryTempItemConsig.Open;
end;

procedure TfrmConsignacao_Leitor.qryTempItemConsigBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  {Busca os dados do produto de acordo com o c�digo de barras}

  SP_BUSCAPRODUTO_ITEMPEDIDO.Close;
  SP_BUSCAPRODUTO_ITEMPEDIDO.Parameters.ParamByName('@cEAN').Value := qryTempItemConsigcEAN.Value;
  SP_BUSCAPRODUTO_ITEMPEDIDO.Open;

  qryProduto.Close;
  PosicionaQuery(qryProduto,SP_BUSCAPRODUTO_ITEMPEDIDOnCdProduto.AsString);

  if (qryProduto.IsEmpty) then
  begin
      MensagemAlerta('Produto n�o encontrado!');

      qryTempItemConsig.Edit;

      qryTempItemConsignCdProduto.Clear;
      qryTempItemConsigcNmProduto.Clear;
      qryTempItemConsigcReferencia.Clear;
      qryTempItemConsigcEAN.Clear;
      qryTempItemConsignValVenda.Clear;

      Abort;
  end;

  qryTempItemConsignCdProduto.Value  := qryProdutonCdProduto.Value;
  qryTempItemConsigcNmProduto.Value  := qryProdutocNmProduto.Value;
  qryTempItemConsigcReferencia.Value := qryProdutocReferencia.Value;
  qryTempItemConsignValVenda.Value   := qryProdutonValVenda.Value;
end;

procedure TfrmConsignacao_Leitor.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmConsignacao_Leitor.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmConsignacao_Leitor.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
    if (Key = VK_RETURN) and (qryTempItemConsig.State = dsInsert) then
        if not (Trim(qryTempItemConsigcEAN.Value) = '') then
            qryTempItemConsig.Post;
end;

procedure TfrmConsignacao_Leitor.ToolButton1Click(Sender: TObject);
var
    cFlgGeraPedido : integer;
begin
    if (qryTempItemConsig.IsEmpty) then
    begin
        MensagemAlerta('N�o h� nenhum produto na tabela!');
        Abort;
    end;

    {Se o c�d da a��o for igual a 1, adiciona produtos na consigna��o. Se o c�digo for igual a 2, atualiza os produtos devolvidos.}

    if (nCdAcao = 1) then
    begin
        if (MessageDLG('Confirma a leitura dos produtos?', mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
            exit;

        frmMenu.Connection.BeginTrans;

        try
            SP_ITENS_LEITOR_CONSIG.Close;
            SP_ITENS_LEITOR_CONSIG.Parameters.ParamByName('@nCdConsignacao').Value := nCdConsignacao;
            SP_ITENS_LEITOR_CONSIG.ExecProc;
        except
            MensagemErro('Erro no processamento.');

            frmMenu.Connection.RollbackTrans;

            Raise;
        end;

        frmMenu.Connection.CommitTrans;

        Close;
    end

    else if (nCdAcao = 2) then
    begin
        if (MessageDLG('Deseja prosseguir com a devolu��o dos produtos? N�o ser� poss�vel desfazer as altera��es ap�s a devolu��o.', mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
            exit;

        cFlgGeraPedido := 1;   //Se todos os produtos forem devolvidos, a gera��o do pedido ser� impossibilitada

        qryTempItemConsig.First;

        while not (qryTempItemConsig.Eof) do
        begin
            qryTempItens.Close;
            PosicionaQuery(qryTempItens,qryTempItemConsignCdProduto.AsString);

            qryQtdeProdutos.Close;
            PosicionaQuery(qryQtdeProdutos,qryTempItemConsignCdProduto.AsString);

            qryValidaQtde.Close;
            PosicionaQuery(qryValidaQtde,qryTempItemConsignCdProduto.AsString);

            if (qryTempItens.IsEmpty) then
            begin
                MensagemAlerta('O produto ' + qryTempItemConsigcNmProduto.Value + ' n�o est� entre os itens dessa consigna��o.');
                Abort;
            end;

            if (qryQtdeProdutosnQtdeDevolvida.Value >= qryValidaQtdenQtdeRetirada.Value) then
                cFlgGeraPedido := 0;

            iQtdeDev := iQtdeDev + 1;

            qryTempItemConsig.Next;
        end;

        if (cFlgGeraPedido = 0) then
            if (MessageDLG('H� produtos com a quantidade devolvida maior ou igual � quantidade retirada. N�o ser� poss�vel gerar um pedido de venda, deseja continuar?', mtConfirmation, [mbYes, mbNo], 0) = mrNo) then
                Exit;

        frmMenu.Connection.BeginTrans;

        try
            SP_DEVOLUCAO_ITEM_CONSIG.Close;
            SP_DEVOLUCAO_ITEM_CONSIG.Parameters.ParamByName('@nCdConsignacao').Value := nCdConsignacao;
            SP_DEVOLUCAO_ITEM_CONSIG.ExecProc;
        except
            frmMenu.Connection.RollbackTrans;

            MensagemErro('Erro no processamento.');

            raise;
        end;

        frmMenu.Connection.CommitTrans;

        ShowMessage('Opera��o realizada com sucesso!');

        { -- marca vari�vel para true para que seja continuado processo de finaliza��o da consigna��o -- }
        bProcessado := True;

        Close;
    end;
end;

procedure TfrmConsignacao_Leitor.FormShow(Sender: TObject);
begin
  inherited;

  iQtdeDev    := 0;
  bProcessado := False;
end;

end.
