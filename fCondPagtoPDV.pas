unit fCondPagtoPDV;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, GridsEh, DBGridEh, StdCtrls, Mask, DBCtrls, cxPC, cxControls,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmCondPagtoPDV = class(TfrmCadastro_Padrao)
    qryMasternCdCondPagto: TIntegerField;
    qryMastercNmCondPagto: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryMasternCdFormaPagto: TIntegerField;
    qryMasterdDtValidadeIni: TDateTimeField;
    qryMasterdDtValidadeFim: TDateTimeField;
    qryMastercFlgAtivo: TIntegerField;
    qryMastercFlgPermDesconto: TIntegerField;
    qryMasternFatorArred: TBCDField;
    qryMasteriParcArred: TIntegerField;
    qryFormaPagto: TADOQuery;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    dsFormaPagto: TDataSource;
    qryAux: TADOQuery;
    qryLojaCondPagto: TADOQuery;
    dsLojaCondPagto: TDataSource;
    qryLojaCondPagtonCdLoja: TIntegerField;
    qryLojaCondPagtonCdCondPagto: TIntegerField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryLojaCondPagtocNmLoja: TStringField;
    qryMastercFlgPrimParcEnt: TIntegerField;
    qryMastercFlgPDV: TIntegerField;
    qryMastercFlgLiqCrediario: TIntegerField;
    qryMasternPercAcrescimo: TBCDField;
    qryMasternPercEntrada: TBCDField;
    qryMasteriDiasPrimParc: TIntegerField;
    qryMasteriDiasMaxPrimParc: TIntegerField;
    qryMasteriDiasProxParc: TIntegerField;
    qryMastercFlgPermPromocao: TIntegerField;
    qryMasterdDtPrimParcela: TDateTimeField;
    qryMasteriQtdeParcelas: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label7: TLabel;
    Label10: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    DBEdit8: TDBEdit;
    DBCheckBox7: TDBCheckBox;
    DBEdit11: TDBEdit;
    GroupBox2: TGroupBox;
    Label9: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    Label12: TLabel;
    DBCheckBox3: TDBCheckBox;
    DBEdit12: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit10: TDBEdit;
    DBGridEh2: TDBGridEh;
    DBEdit13: TDBEdit;
    Label11: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    DBEdit9: TDBEdit;
    Label6: TLabel;
    qryLojaCondPagtonCdLojaCondPagto: TIntegerField;
    Label15: TLabel;
    DBEdit14: TDBEdit;
    qryMasternPercValeDesconto: TBCDField;
    qryMastercFlgGeraValeDescontoPromocao: TIntegerField;
    DBCheckBox5: TDBCheckBox;
    qryMastercFlgArredondaPrecoPDV: TIntegerField;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox8: TDBCheckBox;
    qryMastercFlgCondPagtoWeb: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure qryLojaCondPagtoBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBCheckBox3Click(Sender: TObject);
    procedure DBEdit13Exit(Sender: TObject);
    procedure DBGridEh2Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCondPagtoPDV: TfrmCondPagtoPDV;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmCondPagtoPDV.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CONDPAGTO' ;
  nCdTabelaSistema  := 47 ;
  nCdConsultaPadrao := 138 ;
  bLimpaAposSalvar  := False ;
end;

procedure TfrmCondPagtoPDV.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMasteriQtdeParcelas.Value := 1 ;
  qryMasteriDiasProxParc.Value := 30 ;
  qryMastercFlgAtivo.Value     := 1 ;
  
  DBEdit2.SetFocus ;
end;

procedure TfrmCondPagtoPDV.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryFormaPagto.Close ;
  PosicionaQuery(qryFormaPagto, qryMasternCdFormaPagto.asString) ;
  PosicionaQuery(qryLojaCondPagto, qryMasternCdCondPagto.asString) ;

  DBEdit5.Enabled  := (qryMastercFlgPrimParcEnt.Value = 0) ;
  DBEdit10.Enabled := (qryMastercFlgPrimParcEnt.Value = 0) ;
  {DBEdit9.Enabled  := (qryMastercFlgPrimParcEnt.Value = 0) ;}
  DBEdit12.Enabled := (qryMastercFlgPrimParcEnt.Value = 1) ;

end;

procedure TfrmCondPagtoPDV.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  if not qryLojaCondPagto.Active then
      PosicionaQuery(qryLojaCondPagto, qryMasternCdCondPagto.asString) ;

end;

procedure TfrmCondPagtoPDV.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryFormaPagto.Close ;
  qryLojaCondPagto.Close ;
end;

procedure TfrmCondPagtoPDV.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post ;
      
end;

procedure TfrmCondPagtoPDV.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(15);

            If (nPK > 0) then
            begin
                qryMasternCdFormaPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCondPagtoPDV.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  qryFormaPagto.Close ;
  PosicionaQuery(qryFormaPagto, DbEdit6.Text) ;
  
end;

procedure TfrmCondPagtoPDV.qryMasterBeforePost(DataSet: TDataSet);
begin

  cxPageControl1.ActivePageIndex := 0 ;
  
  if (Trim(DbEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o da condi��o.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtValidadeIni.Value > qryMasterdDtValidadeFim.Value) then
  begin
      MensagemAlerta('Per�odo de validade inv�lido.') ;
      DbEdit3.SetFocus;
      abort ;
  end ;

  if (qryMasternPercEntrada.Value >= 100) then
  begin
      MensagemAlerta('Percentual de entrada inv�lido. Informe um valor entre 0 e 99.') ;
      DBEdit12.SetFocus;
      abort ;
  end ;

  if (qryMasteriQtdeParcelas.Value < 2) and (DBCheckBox3.Checked) then
  begin
      MensagemErro('Para uma condi��o com entrada o n�mero de parcelas deve ser no m�nimo 2.') ;
      qryMastercFlgPrimParcEnt.Value := 0 ;
      DBEdit11.SetFocus ;
      abort ;
  end ;

  if (qryMasteriQtdeParcelas.Value < 1) then
  begin
      MensagemErro('O n�mero de parcelas deve ser no m�nimo 1.') ;
      DBEdit11.SetFocus;
      abort ;
  end ;

  if (qryMasteriDiasProxParc.Value < 0) then
      qryMasteriDiasProxParc.Value := 0 ;

  if (qryMasteriQtdeParcelas.Value > 1) and (qryMasteriDiasProxParc.Value <= 0) then
  begin
      MensagemErro('Informe a quantidade de dias entre as parcelas.') ;
      DBEdit13.SetFocus;
      abort ;
  end ;

  if (qryMasteriDiasPrimParc.Value = 0) and (qryMastercFlgPrimParcEnt.Value = 0) and (qryMasteriQtdeParcelas.Value > 1) then
  begin
      MensagemAlerta('Informe os dias da primeira parcela ou considere a primeira parcela como entrada.') ;

      if (DBEdit5.Enabled) then
          DBEdit5.SetFocus;

      abort ;
  end ;

  if (qryMasteriDiasMaxPrimParc.Value <= 0) and (qryMasteriDiasPrimParc.Value > 0) then
      qryMasteriDiasMaxPrimParc.Value := qryMasteriDiasPrimParc.Value ;

  qryMastercFlgPdv.Value := 1 ;

  inherited;

end;

procedure TfrmCondPagtoPDV.btSalvarClick(Sender: TObject);
begin
  inherited;

  qryMaster.UpdateBatch;
  qryLojaCondPagto.UpdateBatch ;

  btCancelar.Click;

end;

procedure TfrmCondPagtoPDV.qryLojaCondPagtoBeforePost(DataSet: TDataSet);
begin

  if (qryLojaCondPagtonCdLoja.Value = 0) or (qryLojaCondPagtocNmLoja.Text = '') then
  begin
      MensagemAlerta('Informe a loja.') ;
      abort ;
  end ;

  inherited;

  qryLojaCondPagtonCdCondPagto.Value := qryMasternCdCondPagto.Value ;

  if (qryLojaCondPagto.State = dsInsert) then
      qryLojaCondPagtonCdLojaCondPagto.Value := frmMenu.fnProximoCodigo('LOJACONDPAGTO') ;

end;

procedure TfrmCondPagtoPDV.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

  if (frmMenu.LeParametro('VAREJO') <> 'S') then
      DbGridEh2.Enabled := False;


      
end;

procedure TfrmCondPagtoPDV.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryLojaCondPagto.State = dsBrowse) then
             qryLojaCondPagto.Edit ;
        
        if (qryLojaCondPagto.State = dsInsert) or (qryLojaCondPagto.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(86);

            If (nPK > 0) then
            begin
                qryLojaCondPagtonCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCondPagtoPDV.DBCheckBox3Click(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) then
  begin

      if (qryMasteriQtdeParcelas.Value < 2) and (DBCheckBox3.Checked) and (qryMaster.State <> dsBrowse) then
      begin
          MensagemErro('Para uma condi��o com entrada o n�mero de parcelas deve ser no m�nimo 2.') ;
          qryMastercFlgPrimParcEnt.Value := 0 ;
          DBEdit11.SetFocus ;
          abort ;
      end ;
  
      DBEdit5.Enabled  := (not DBCheckBox3.Checked) ;
      DBEdit10.Enabled := (not DBCheckBox3.Checked) ;
      {DBEdit9.Enabled  := (not DBCheckBox3.Checked) ;}
      DBEdit12.Enabled := (DBCheckBox3.Checked) ;

      if (qryMaster.State <> dsBrowse) then
      begin
          qryMasteriDiasPrimParc.Value     := 0  ;
          qryMasteriDiasMaxPrimParc.Value  := 0  ;
          qryMasterdDtPrimParcela.asString := '' ;
          qryMasternPercEntrada.Value      := 0  ;

          if (DBCheckBox3.Checked) then
              DBEdit12.SetFocus
          else DBEdit5.SetFocus;
          
      end ;

  end ;

end;

procedure TfrmCondPagtoPDV.DBEdit13Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) and (not DBCheckBox3.Checked) then
  begin
      qryMasteriDiasPrimParc.Value    := qryMasteriDiasProxParc.Value ;
      qryMasteriDiasMaxPrimParc.Value := qryMasteriDiasProxParc.Value ;
  end ;

end;

procedure TfrmCondPagtoPDV.DBGridEh2Enter(Sender: TObject);
begin
  inherited;
  if qryMaster.Active and (qryMasternCdCondPagto.Value = 0) then
      qryMaster.Post ;

end;

initialization
    RegisterClass(TfrmCondPagtoPDV) ;

end.
