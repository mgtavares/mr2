unit fCor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmCor = class(TfrmCadastro_Padrao)
    qryMasternCdCor: TAutoIncField;
    qryMastercNmCor: TStringField;
    qryMastercNmCorPredom: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryMasteriNivel: TIntegerField;
    qryMasternCdCorPredom: TIntegerField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    qryCorPredom: TADOQuery;
    qryCorPredomcNmCor: TStringField;
    DBEdit4: TDBEdit;
    DataSource1: TDataSource;
    qryVerificaDup: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCor: TfrmCor;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmCor.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'COR' ;
  nCdTabelaSistema  := 27 ;
  nCdConsultaPadrao := 43 ;
end;

procedure TfrmCor.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmCor.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMastercNmCor.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o da cor.') ;
      abort ;
  end ;

  if (DBEdit3.Enabled) and (DBEDit4.Text = '') then
  begin
      MensagemAlerta('Informe a cor predominante') ;
      abort ;
  end ;

  qryVerificaDup.Close ;
  qryVerificaDup.Parameters.ParamByName('cNmCor').Value := DBEdit2.Text ;
  qryVerificaDup.Parameters.ParamByName('nCdCor').Value := qryMasternCdCor.Value ;
  qryVerificaDup.Open ;

  if not qryVerificaDup.Eof then
  begin
      qryVerificaDup.Close ;
      MensagemAlerta('Cor j� cadastrada.') ;
      abort ;
  end ;

  qryVerificaDup.Close ;


  qryMastercNmCor.Value := Uppercase(qryMastercNmCor.Value) ;
  qryMasteriNivel.Value := 1 ;

  inherited;

end;

procedure TfrmCor.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(160);

            If (nPK > 0) then
            begin
                qryMasternCdCorPredom.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCor.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryCorPredom.Close ;
  PosicionaQuery(qryCorPredom,DbEdit3.Text) ;
end;

procedure TfrmCor.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryCorPredom,qryMasternCdCorPredom.asString) ;

  if (DBEdit3.Enabled) and (qryMasternCdCorPredom.Value = 0) and (qryMaster.State <> dsInsert) then
  begin
      MensagemAlerta('Esta � uma cor predominante e n�o pode ser alterada por esta tela.') ;
      btCancelar.Click;
  end ;

end;

procedure TfrmCor.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryCorPredom.Close ;
end;

procedure TfrmCor.btSalvarClick(Sender: TObject);
begin
  inherited;
  qryCorPredom.Close ;
end;

procedure TfrmCor.FormShow(Sender: TObject);
begin
  inherited;

  DBEDit3.Enabled := False;

  if (frmMenu.LeParametro('USACORPREDOM') = 'S') then
      DBEdit3.Enabled := True ;
      
end;

initialization
    RegisterClass(TfrmCor) ;
    
end.
