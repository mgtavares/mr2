unit fSelecServer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, ComCtrls, ExtCtrls,
  ImgList, jpeg;

type
  TfrmSelecServer = class(TForm)
    ListView1: TListView;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ImageList1: TImageList;
    cxButton3: TcxButton;
    Image3: TImage;
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure AtualizaLista(Sender: TObject);
    procedure ListView1DblClick(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);

  private
    { Private declarations }
    iIdConn : Integer ;
    arrIP : Array of String;
    arrInst : Array of String;
  public
    { Public declarations }
  end;

var
  frmSelecServer: TfrmSelecServer;

implementation

uses fMenu, fAddServer, inifiles;

{$R *.dfm}

procedure TfrmSelecServer.AtualizaLista(Sender: TObject);
var
  ListItem : TListItem;
  arquivo : Tinifile;
  cDiretorio, cDescricao, cIP, cInstancia : string;
  iaux: Integer ;
begin

    ListView1.Items.Clear;

    cDiretorio := extractfilepath(application.exename);
    arquivo   := Tinifile.Create(cDiretorio + 'Servidores.ini');

    iIdConn := 0 ;

    SetLength(arrIP,100) ;
    SetLength(arrInst,100) ;

    for iAux := 0 To 100 do
    begin

        cDescricao := Arquivo.ReadString(intToStr(iAux),'D','') ;

        if (cDescricao = '') then
        begin
            Arquivo.Free;
            exit ;
        end ;

        ListItem:= ListView1.Items.Add;
        ListItem.Caption    := cDescricao ;
        ListItem.ImageIndex := 0 ;

        arrIP[ListItem.Index]   := Arquivo.ReadString(intToStr(iAux),'I','') ;
        arrInst[ListItem.Index] := Arquivo.ReadString(intToStr(iAux),'N','') ;

        iIdConn := iIdConn + 1
    end ;

end ;

procedure TfrmSelecServer.cxButton1Click(Sender: TObject);
begin
    try
        frmMenu.cIPServidor := ArrIP[ListView1.Items.IndexOf(ListView1.Selected)] ;
        frmMenu.cInstancia  := ArrInst[ListView1.Items.IndexOf(ListView1.Selected)] ;
        close ;
    except
        ShowMessage('Selecione um servidor.') ;
    end ;
end;

procedure TfrmSelecServer.cxButton2Click(Sender: TObject);
begin
    frmAddServer.iIdConn := iIdConn ;
    frmAddServer.ShowModal ;
    AtualizaLista(Sender);
end;

procedure TfrmSelecServer.FormShow(Sender: TObject);
begin

    AtualizaLista(Sender);

end;

procedure TfrmSelecServer.ListView1DblClick(Sender: TObject);
begin
  cxButton1.Click;
end;

procedure TfrmSelecServer.cxButton3Click(Sender: TObject);
begin
    Application.Terminate;
end;

end.
