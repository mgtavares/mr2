unit fCentroDistribuicao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, DBGridEhGrouping, ToolCtrlsEh,
  GridsEh, DBGridEh, cxPC, cxControls, ER2Lookup;

type
  TfrmCentroDistribuicao = class(TfrmCadastro_Padrao)
    DataSource1: TDataSource;
    qryMasternCdCentroDistribuicao: TIntegerField;
    qryMastercNmCentroDistribuicao: TStringField;
    qryMasternCdLocalEstEmpenhoPadrao: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    qryLocalEstEmpenhadoPadrao: TADOQuery;
    dsLocalEstEmpenhadoPadrao: TDataSource;
    qryLocalEstEmpenhadoPadraocNmLocalEstoque: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryLocalEstoqueCentroDistribuicao: TADOQuery;
    dsLocalEstoqueCentroDistribuicao: TDataSource;
    qryLocalEstoqueCentroDistribuicaonCdLocalEstoqueCentroDistribuicao: TIntegerField;
    qryLocalEstoqueCentroDistribuicaonCdCentroDistribuicao: TIntegerField;
    qryLocalEstoqueCentroDistribuicaonCdLocalEstoque: TIntegerField;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DBEdit5: TDBEdit;
    ER2LookupDBEdit1: TER2LookupDBEdit;
    DBEdit1: TDBEdit;
    qryUsuarioCentroDistribuicao: TADOQuery;
    dsUsuarioCentroDistribuicao: TDataSource;
    DBGridEh2: TDBGridEh;
    qryUsuarioCentroDistribuicaonCdUsuarioCentroDistribuicao: TIntegerField;
    qryUsuarioCentroDistribuicaonCdCentroDistribuicao: TIntegerField;
    qryUsuarioCentroDistribuicaonCdUsuario: TIntegerField;
    DBGridEh3: TDBGridEh;
    qryUsuario: TADOQuery;
    qryAux: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    qryLojaCentroDistribuicao: TADOQuery;
    dsLojaCentroDistribuicao: TDataSource;
    qryTipoFrequenciaDistribuicao: TADOQuery;
    qryTipoPedidoSaida: TADOQuery;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    qryTipoPedidoSaidacNmTipoPedido: TStringField;
    qryTipoFrequenciaDistribuicaocNmTipoFrequenciaDistribuicao: TStringField;
    ER2LookupDBEdit2: TER2LookupDBEdit;
    qryLojanCdLoja: TIntegerField;
    qryTipoPedidoSaidanCdTipoPedido: TIntegerField;
    qryTipoFrequenciaDistribuicaonCdTipoFrequenciaDistribuicao: TIntegerField;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoqueCentroDistribuicaocNmLocalEstoque: TStringField;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuarioCentroDistribuicaocNmUsuario: TStringField;
    qryEmpresa: TADOQuery;
    dsEmpresa: TDataSource;
    qryEmpresacNmEmpresa: TStringField;
    Label1: TLabel;
    DBEdit4: TDBEdit;
    qryMasternCdEmpresa: TIntegerField;
    ER2LookupDBEdit3: TER2LookupDBEdit;
    qryTipoReceb: TADOQuery;
    qryTipoRecebnCdTipoReceb: TIntegerField;
    qryTipoRecebcNmTipoReceb: TStringField;
    qryLojaCentroDistribuicaonCdLojaCentroDistribuicao: TIntegerField;
    qryLojaCentroDistribuicaonCdCentroDistribuicao: TIntegerField;
    qryLojaCentroDistribuicaonCdLoja: TIntegerField;
    qryLojaCentroDistribuicaonCdTipoPedidoSaida: TIntegerField;
    qryLojaCentroDistribuicaonCdTipoPedidoEntrada: TIntegerField;
    qryLojaCentroDistribuicaonCdTipoReceb: TIntegerField;
    qryLojaCentroDistribuicaonCdTipoFrequenciaDistribuicao: TIntegerField;
    qryLojaCentroDistribuicaonCdServidorOrigem: TIntegerField;
    qryLojaCentroDistribuicaodDtReplicacao: TDateTimeField;
    qryLojaCentroDistribuicaocNmLoja: TStringField;
    qryLojaCentroDistribuicaocNmTipoPedidoSaida: TStringField;
    qryLojaCentroDistribuicaocNmTipoPedidoEntrada: TStringField;
    qryLojaCentroDistribuicaocNmTipoFrequenciaDistribuicao: TStringField;
    qryTipoPedidoEntrada: TADOQuery;
    qryTipoPedidoEntradanCdTipoPedido: TIntegerField;
    qryTipoPedidoEntradacNmTipoPedido: TStringField;
    qryLojaCentroDistribuicaocNmTipoReceb: TStringField;
    ER2LookupDBEdit4: TER2LookupDBEdit;
    qryLojaCD: TADOQuery;
    dsLojaCD: TDataSource;
    qryMasternCdLoja: TIntegerField;
    qryLojaCDcNmLoja: TStringField;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    qryTipoPedidoSaidacGerarFinanc: TIntegerField;
    qryTipoRecebcFlgRecebCego: TIntegerField;
    qryVerificaEstoquePadrao: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryLocalEstoqueCentroDistribuicaoBeforePost(
      DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryUsuarioCentroDistribuicaoBeforePost(DataSet: TDataSet);
    procedure DBGridEh3KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSalvarClick(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh2Enter(Sender: TObject);
    procedure DBGridEh3Enter(Sender: TObject);
    procedure qryLojaCentroDistribuicaoBeforePost(DataSet: TDataSet);
    procedure qryLojaCentroDistribuicaoCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCentroDistribuicao: TfrmCentroDistribuicao;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmCentroDistribuicao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CENTRODISTRIBUICAO' ;
  nCdTabelaSistema  := 533 ;
  nCdConsultaPadrao := 782 ;

end;


procedure TfrmCentroDistribuicao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryLocalEstEmpenhadoPadrao.Close;
  qryLocalEstoqueCentroDistribuicao.Close;
  qryStat.Close;
  qryUsuarioCentroDistribuicao.Close;
  qryLojaCentroDistribuicao.Close;
  qryEmpresa.Close;
  qryLojaCD.Close;

  PosicionaQuery(qryLocalEstEmpenhadoPadrao,qryMasternCdLocalEstEmpenhoPadrao.AsString);
  PosicionaQuery(qryEmpresa,qryMasternCdEmpresa.AsString);
  PosicionaQuery(qryLocalEstoqueCentroDistribuicao,qryMasternCdCentroDistribuicao.AsString);
  PosicionaQuery(qryStat,qryMasternCdStatus.AsString);
  PosicionaQuery(qryUsuarioCentroDistribuicao,qryMasternCdCentroDistribuicao.AsString);
  PosicionaQuery(qryLojaCentroDistribuicao,qryMasternCdCentroDistribuicao.AsString);
  PosicionaQuery(qryLojaCD,qryMasternCdLoja.AsString);
end;

procedure TfrmCentroDistribuicao.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryLocalEstEmpenhadoPadrao.Close;
  qryLocalEstoqueCentroDistribuicao.Close;
  qryStat.Close;
  qryUsuarioCentroDistribuicao.Close;
  qryLojaCentroDistribuicao.Close;
  qryEmpresa.Close;
  qryLojaCD.Close;

end;

procedure TfrmCentroDistribuicao.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(87);

            If (nPK > 0) then
            begin
                qryMasternCdLocalEstEmpenhoPadrao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCentroDistribuicao.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  if qryMaster.IsEmpty then
      exit;

  PosicionaQuery(qryLocalEstEmpenhadoPadrao,qryMasternCdLocalEstEmpenhoPadrao.AsString);

end;

procedure TfrmCentroDistribuicao.qryMasterBeforePost(DataSet: TDataSet);
begin

  if Trim(DBEdit4.Text) = '' then
  begin
      MensagemAlerta('Informe a empresa vinculada ao centro de distribui��o.');
      ER2LookupDBEdit3.SetFocus;
      Abort;
  end;

  if Trim(DBEdit6.Text) = '' then
  begin
      MensagemAlerta('Informe a loja vinculada ao centro de distribui��o.');
      ER2LookupDBEdit4.SetFocus;
      Abort;
  end;

  if Trim(DBEdit3.Text) = '' then
  begin
      MensagemAlerta('Descri��o do Centro de Distribui��o n�o informado.');
      DBEdit3.SetFocus;
      Abort;
  end;

  if Trim(DBEdit5.Text) = '' then
  begin
      MensagemAlerta('Local de Estoque Empenhado Padr�o n�o informado.');
      ER2LookupDBEdit2.SetFocus;
      Abort;
  end;

  qryVerificaEstoquePadrao.Close;
  qryVerificaEstoquePadrao.Parameters.ParamByName('nCdLocalEstEmpenho').Value    := qryMasternCdLocalEstEmpenhoPadrao.Value;
  qryVerificaEstoquePadrao.Parameters.ParamByName('nCdCentroDistribuicao').Value := qryMasternCdCentroDistribuicao.Value;
  qryVerificaEstoquePadrao.Open;

  if not qryVerificaEstoquePadrao.IsEmpty then
  begin
      MensagemAlerta('Local de Estoque Empenhado informado, j� est� vinculado a outro centro de distribui��o.');
      ER2LookupDBEdit2.SetFocus;
      Abort;
  end;

  inherited;

end;

procedure TfrmCentroDistribuicao.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryLocalEstoqueCentroDistribuicao.State = dsBrowse) then
             qryLocalEstoqueCentroDistribuicao.Edit ;

        if (qryLocalEstoqueCentroDistribuicao.State = dsInsert) or (qryLocalEstoqueCentroDistribuicao.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(87);

            If (nPK > 0) then
            begin
                qryLocalEstoqueCentroDistribuicaonCdLocalEstoque.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCentroDistribuicao.qryLocalEstoqueCentroDistribuicaoBeforePost(
  DataSet: TDataSet);
begin
  if qryLocalEstoqueCentroDistribuicaocNmLocalEstoque.Value = '' then
  begin
      MensagemErro('Local de estoque n�o informada. ');
      Abort;
  end;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM LocalEstoqueCentroDistribuicao WHERE nCdLocalEstoque = ' + qryLocalEstoqueCentroDistribuicaonCdLocalEstoque.asString + ' AND nCdCentroDistribuicao = ' + qryMasternCdCentroDistribuicao.AsString) ;
  qryAux.Open ;

  if (not qryAux.IsEmpty) then
  begin
      MensagemAlerta('Local de estoque ' + qryLocalEstoquecNmLocalEstoque.Value + ' j� vinculado ao Centro de Distribui��o.');
      Abort;
  end;

  inherited;

  qryLocalEstoqueCentroDistribuicaonCdCentroDistribuicao.Value             := qryMasternCdCentroDistribuicao.Value;
  qryLocalEstoqueCentroDistribuicaonCdLocalEstoqueCentroDistribuicao.Value := frmMenu.fnProximoCodigo('LOCALESTOQUECENTRODISTRIBUICAO');
  
end;

procedure TfrmCentroDistribuicao.DBGridEh2KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioCentroDistribuicao.State = dsBrowse) then
             qryUsuarioCentroDistribuicao.Edit ;

        if (qryUsuarioCentroDistribuicao.State = dsInsert) or (qryUsuarioCentroDistribuicao.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioCentroDistribuicaonCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCentroDistribuicao.qryUsuarioCentroDistribuicaoBeforePost(
  DataSet: TDataSet);
begin

  if (qryUsuarioCentroDistribuicaocNmUsuario.Value = '') then
  begin
      MensagemAlerta('Usu�rio Invalido. ');
      Abort;
  end;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM UsuarioCentroDistribuicao WHERE nCdUsuario = ' + qryUsuarioCentroDistribuicaonCdUsuario.asString + ' AND nCdCentroDistribuicao = ' + qryMasternCdCentroDistribuicao.AsString) ;
  qryAux.Open ;

  if (not qryAux.IsEmpty) then
  begin
      MensagemAlerta('Usu�rio ' + qryUsuarioCentroDistribuicaocNmUsuario.Value + ' j� vinculado ao Centro de Distribui��o.');
      Abort;
  end;

  inherited;

  qryUsuarioCentroDistribuicaonCdCentroDistribuicao.Value        := qryMasternCdCentroDistribuicao.Value;
  qryUsuarioCentroDistribuicaonCdUsuarioCentroDistribuicao.Value := frmMenu.fnProximoCodigo('USUARIOCENTRODISTRIBUICAO');

end;

procedure TfrmCentroDistribuicao.DBGridEh3KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
        {-- Loja --}
        if (DBGridEh3.Columns[DBGridEh3.Col-1].FieldName = 'nCdLoja') then
        begin
          if (qryLojaCentroDistribuicao.State = dsBrowse) then
             qryLojaCentroDistribuicao.Edit ;

          if (qryLojaCentroDistribuicao.State = dsInsert) or (qryLojaCentroDistribuicao.State = dsEdit) then
          begin

            nPK := frmLookup_Padrao.ExecutaConsulta(147);

            If (nPK > 0) then
            begin
                qryLojaCentroDistribuicaonCdLoja.Value := nPK ;
            end ;

          end ;
        end ;

        { -- tipo de pedido sa�da -- }
        if (DBGridEh3.Columns[DBGridEh3.Col-1].FieldName = 'nCdTipoPedidoSaida') then
        begin
          if (qryLojaCentroDistribuicao.State = dsBrowse) then
             qryLojaCentroDistribuicao.Edit ;

          if (qryLojaCentroDistribuicao.State = dsInsert) or (qryLojaCentroDistribuicao.State = dsEdit) then
          begin

            nPK := frmLookup_Padrao.ExecutaConsulta(54);

            If (nPK > 0) then
            begin
                qryLojaCentroDistribuicaonCdTipoPedidoSaida.Value := nPK ;
            end ;

          end ;
        end ;

        { -- tipo de pedido entrada -- }
        if (DBGridEh3.Columns[DBGridEh3.Col-1].FieldName = 'nCdTipoPedidoEntrada') then
        begin
          if (qryLojaCentroDistribuicao.State = dsBrowse) then
             qryLojaCentroDistribuicao.Edit ;

          if (qryLojaCentroDistribuicao.State = dsInsert) or (qryLojaCentroDistribuicao.State = dsEdit) then
          begin

            nPK := frmLookup_Padrao.ExecutaConsulta(98);

            If (nPK > 0) then
            begin
                qryLojaCentroDistribuicaonCdTipoPedidoEntrada.Value := nPK ;
            end ;

          end ;
        end ;

        { -- tipo de recebimento -- }
        if (DBGridEh3.Columns[DBGridEh3.Col-1].FieldName = 'nCdTipoReceb') then
        begin
          if (qryLojaCentroDistribuicao.State = dsBrowse) then
             qryLojaCentroDistribuicao.Edit ;

          if (qryLojaCentroDistribuicao.State = dsInsert) or (qryLojaCentroDistribuicao.State = dsEdit) then
          begin
            if (qryTipoPedidoEntrada.IsEmpty) then
            begin
                MensagemAlerta('Informe o tipo de pedido de entrada.');
                DBGridEh3.SelectedField := qryLojaCentroDistribuicaonCdTipoPedidoEntrada;
                DBGridEh3.SetFocus;
                Abort;
            end;

            nPK := frmLookup_Padrao.ExecutaConsulta2(88,'EXISTS(SELECT TOP 1 1 FROM TipoPedidoTipoReceb TPT WHERE TPT.nCdTipoReceb  = TipoReceb.nCdTipoReceb AND TPT.nCdTipoPedido = ' + qryTipoPedidoEntradanCdTipoPedido.AsString +')');

            If (nPK > 0) then
            begin
                qryLojaCentroDistribuicaonCdTipoReceb.Value := nPK;
            end ;
          end ;
        end ;
        
        {-- Tipo de Frequencia --}
        if (DBGridEh3.Columns[DBGridEh3.Col-1].FieldName = 'nCdTipoFrequenciaDistribuicao') then
        begin
          if (qryLojaCentroDistribuicao.State = dsBrowse) then
             qryLojaCentroDistribuicao.Edit ;

          if (qryLojaCentroDistribuicao.State = dsInsert) or (qryLojaCentroDistribuicao.State = dsEdit) then
          begin

            nPK := frmLookup_Padrao.ExecutaConsulta(781);

            If (nPK > 0) then
            begin
                qryLojaCentroDistribuicaonCdTipoFrequenciaDistribuicao.Value := nPK ;
            end ;

          end ;
        end ;

    end ;

  end ;

end;

procedure TfrmCentroDistribuicao.btSalvarClick(Sender: TObject);
begin
  inherited;
  
  btCancelar.Click;
end;

procedure TfrmCentroDistribuicao.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
     exit ;

  if qryMaster.State = dsInsert then
      qryMaster.Post;

  PosicionaQuery(qryLocalEstoqueCentroDistribuicao,qryMasternCdCentroDistribuicao.AsString);
end;

procedure TfrmCentroDistribuicao.DBGridEh2Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
     exit ;

  if qryMaster.State = dsInsert then
      qryMaster.Post;

  PosicionaQuery(qryUsuarioCentroDistribuicao,qryMasternCdCentroDistribuicao.AsString);
end;

procedure TfrmCentroDistribuicao.DBGridEh3Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
     exit ;

  if qryMaster.State = dsInsert then
      qryMaster.Post;

  PosicionaQuery(qryLojaCentroDistribuicao,qryMasternCdCentroDistribuicao.AsString);
end;

procedure TfrmCentroDistribuicao.qryLojaCentroDistribuicaoBeforePost(
  DataSet: TDataSet);
begin

  if qryLojaCentroDistribuicaocNmLoja.Value = '' then
  begin
      MensagemAlerta('Loja n�o informada. ');
      DBGridEh3.Col := 1;
      Abort;
  end;

  if qryLojaCentroDistribuicao.State = dsInsert then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT 1 FROM LojaCentroDistribuicao WHERE nCdLoja = ' + qryLojaCentroDistribuicaonCdLoja.asString + ' AND nCdCentroDistribuicao = ' + qryMasternCdCentroDistribuicao.AsString) ;
      qryAux.Open ;

      if (not qryAux.IsEmpty) then
      begin
          MensagemAlerta('Loja ' + qryLojaCentroDistribuicaocNmLoja.Value + ' j� vinculada ao Centro de Distribui��o.');
          Abort;
      end;
  end;
  
  if (qryLojaCentroDistribuicaocNmTipoPedidoSaida.Value) = '' then
  begin
      MensagemAlerta('Informe o tipo de pedido de sa�da.');
      DBGridEh3.SelectedField := qryLojaCentroDistribuicaonCdTipoPedidoSaida;
      DBGridEh3.SetFocus;
      Abort;
  end;

  if (qryLojaCentroDistribuicaocNmTipoPedidoEntrada.Value = '') then
  begin
      MensagemAlerta('Informe o tipo de pedido de entrada.');
      DBGridEh3.SelectedField := qryLojaCentroDistribuicaonCdTipoPedidoEntrada;
      DBGridEh3.SetFocus;
      Abort;
  end;

  if (qryLojaCentroDistribuicaocNmTipoReceb.Value = '') then
  begin
      MensagemAlerta('Tipo de recebimento n�o informado.');
      DBGridEh3.SelectedField := qryLojaCentroDistribuicaonCdTipoReceb;
      DBGridEh3.SetFocus;
      Abort;
  end;

  {if (qryTipoPedidoSaidacGerarFinanc.Value = qryTipoRecebcFlgRecebCego.Value) then
  begin
      case (qryTipoPedidoSaidacGerarFinanc.Value) of
          0 : MensagemAlerta('Tipo de recebimento informado n�o deve gerar financeiro.');
          1 : MensagemAlerta('Tipo de recebimento informado deve gerar financeiro.');
      end;

      DBGridEh3.SelectedField := qryLojaCentroDistribuicaonCdTipoReceb;
      DBGridEh3.SetFocus;
      Abort;
  end;}

  if (qryLojaCentroDistribuicaocNmTipoFrequenciaDistribuicao.Value = '') then
  begin
      MensagemAlerta('Informe o tipo de frequ�ncia.');
      DBGridEh3.SelectedField := qryLojaCentroDistribuicaonCdTipoFrequenciaDistribuicao;
      DBGridEh3.SetFocus;
      Abort;
  end;

  inherited;

  qryLojaCentroDistribuicaonCdCentroDistribuicao.Value        := qryMasternCdCentroDistribuicao.Value;
  qryLojaCentroDistribuicaonCdLojaCentroDistribuicao.Value := frmMenu.fnProximoCodigo('LOJACENTRODISTRIBUICAO');
end;

procedure TfrmCentroDistribuicao.qryLojaCentroDistribuicaoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryTipoPedidoEntrada.Close;
  PosicionaQuery(qryTipoPedidoEntrada, qryLojaCentroDistribuicaonCdTipoPedidoEntrada.AsString);

  if (not qryTipoPedidoEntrada.IsEmpty) then
  begin
      qryLojaCentroDistribuicaocNmTipoPedidoEntrada.Value        := qryTipoPedidoEntradacNmTipoPedido.Value;
      qryTipoReceb.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidoEntradanCdTipoPedido.Value;
  end;

  qryTipoReceb.Close;
  PosicionaQuery(qryTipoReceb, qryLojaCentroDistribuicaonCdTipoReceb.AsString);

  if (not qryTipoReceb.IsEmpty) then
  begin
      qryLojaCentroDistribuicaocNmTipoReceb.Value := qryTipoRecebcNmTipoReceb.Value;
  end;
end;

initialization
    RegisterClass(TfrmCentroDistribuicao) ;
end.
