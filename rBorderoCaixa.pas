unit rBorderoCaixa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBCtrls, ADODB, StdCtrls, Mask;

type
  TrptBorderoCaixa = class(TfrmRelatorio_Padrao)
    MaskEdit6: TMaskEdit;
    Label5: TLabel;
    MaskEdit2: TMaskEdit;
    Label6: TLabel;
    MaskEdit1: TMaskEdit;
    Label3: TLabel;
    MaskEdit3: TMaskEdit;
    Label1: TLabel;
    qryLoja: TADOQuery;
    qryContaBancaria: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryAux: TADOQuery;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptBorderoCaixa: TrptBorderoCaixa;

implementation

uses fMenu, fLookup_Padrao, rBorderoCaixa_view;

{$R *.dfm}

procedure TrptBorderoCaixa.FormShow(Sender: TObject);
begin
  inherited;

  MaskEdit1.Text := DateToStr(Now()) ;
  MaskEdit2.Text := MaskEdit1.Text ;

  qryLoja.Close ;
  qryLoja.Parameters.ParamByName('nPK').Value := frmMenu.nCdLojaAtiva;
  qryLoja.Open ;

  MaskEdit6.Text := IntToStr(frmMenu.nCdLojaAtiva) ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT cFlgGerente FROM Usuario WHERE nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado)) ;
  qryAux.Open ;

  MaskEdit3.Enabled := False ;

  // se for gerente
  if not qryAux.Eof and (qryAux.FieldList[0].Value = 1) then
  begin
      MaskEdit3.Enabled := True ;
      MaskEdit3.SetFocus ;

  end ;

  // se n�o for gerente
  if not qryAux.Eof and (qryAux.FieldList[0].Value = 0) then
  begin
      // descobre a conta vinculada ao usuario
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT nCdContaBancaria      ') ;
      qryAux.SQL.Add('  FROM ContaBancaria         ') ;
      qryAux.SQL.Add(' WHERE cFlgCaixa          = 1') ;
      qryAux.SQL.Add('   AND nCdLoja            =  ' + IntToStr(frmMenu.nCdLojaAtiva)) ;
      qryAux.SQL.Add('   AND nCdUsuarioOperador =  ' + IntToStr(frmMenu.nCdUsuarioLogado)) ;
      qryAux.Open ;

      if qryAux.Eof then
      begin
          qryAux.Close ;
          MensagemAlerta('Nenhuma conta vinculada para este usu�rio.') ;
          close ;
      end ;

      MaskEdit3.Text := qryAux.FieldList[0].AsString;

      PosicionaQuery(qryContaBancaria, qryAux.FieldList[0].AsString) ;

      MaskEdit1.SetFocus ;

  end ;

end;

procedure TrptBorderoCaixa.MaskEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(126,'ContaBancaria.nCdLoja = ' + MaskEdit6.Text);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;
            PosicionaQuery(qryContaBancaria, MaskEdit3.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptBorderoCaixa.ToolButton1Click(Sender: TObject);
var
  objRel : TrptBorderoCaixa_view;
begin
  inherited;

  try
      try
          objRel := TrptBorderoCaixa_view.Create( Self ) ;

          objRel.qryPreparaTemp.ExecSQL;

          objRel.SPREL_BORDERO_CAIXA.Close ;
          objRel.SPREL_BORDERO_CAIXA.Parameters.ParamByName('@dDtInicial').Value       := frmMenu.ConvData(MaskEdit1.Text) ;
          objRel.SPREL_BORDERO_CAIXA.Parameters.ParamByName('@dDtFinal').Value         := frmMenu.ConvData(MaskEdit2.Text) ;
          objRel.SPREL_BORDERO_CAIXA.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.ConvInteiro(MaskEdit6.Text) ;
          objRel.SPREL_BORDERO_CAIXA.Parameters.ParamByName('@nCdContaBancaria').Value := frmMenu.ConvInteiro(MaskEdit3.Text) ;
          objRel.SPREL_BORDERO_CAIXA.ExecProc ;

          objRel.qryTemp_Resumo_Vendas.Close ;
          objRel.qryTemp_Resumo_Vendas.Open ;

          objRel.qryTemp_Resumo_Recebimento.Close ;
          objRel.qryTemp_Resumo_Recebimento.Open ;

          objRel.qryTemp_Resumo_Lanctos.Close ;
          objRel.qryTemp_Resumo_Lanctos.Open ;

          objRel.qryTemp_Resumo_Caixa.Close ;
          objRel.qryTemp_Resumo_Caixa.Open ;

          if (DbEdit1.Text <> '') then
              objRel.lblLoja.Caption := DBEdit1.Text
          else objRel.lblLoja.Caption := 'TODAS LOJAS' ;

          if (DbEdit2.Text <> '') then
              objRel.lblCaixa.Caption := DBEdit2.Text
          else objRel.lblCaixa.Caption := 'TODOS CAIXAS' ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          objRel.lblInicial.Caption := MaskEdit1.Text ;
          objRel.lblFinal.Caption   := MaskEdit2.Text ;
          objRel.lblUsuario.Caption := frmMenu.cNmUsuarioLogado;

          objRel.PreviewModal;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          Raise;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TrptBorderoCaixa.MaskEdit3Exit(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close ;
  qryContaBancaria.Parameters.ParamByName('ncdLoja').Value := frmMenu.ConvInteiro(Maskedit6.Text) ;

  PosicionaQuery(qryContaBancaria, MaskEdit3.Text) ;

end;

initialization
    RegisterClass(TrptBorderoCaixa) ;
    
end.
