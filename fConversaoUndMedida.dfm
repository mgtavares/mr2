inherited frmConversaoUndMedida: TfrmConversaoUndMedida
  Left = 270
  Top = 243
  Width = 986
  Height = 527
  Caption = 'Convers'#227'o de Unidade de Medida'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 970
    Height = 462
  end
  inherited ToolBar1: TToolBar
    Width = 970
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 970
    Height = 462
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsConvUnidadeMedida
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'cUnidadeMedidaDe'
        Footers = <>
        Title.Caption = 'Unidade de Medida|De'
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cUnidadeMedidaPara'
        Footers = <>
        Title.Caption = 'Unidade de Medida|Para'
        Width = 73
      end
      item
        EditButtons = <>
        FieldName = 'nFatorConversaoUM'
        Footers = <>
        Tag = 2
        Title.Caption = 'Fator de Convers'#227'o'
        Width = 94
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryConvUnidadeMedida: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryConvUnidadeMedidaBeforePost
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM ConvUnidadeMedida')
    Left = 528
    Top = 184
    object qryConvUnidadeMedidanCdConvUnidadeMedida: TIntegerField
      FieldName = 'nCdConvUnidadeMedida'
    end
    object qryConvUnidadeMedidacUnidadeMedidaDe: TStringField
      FieldName = 'cUnidadeMedidaDe'
      FixedChar = True
      Size = 3
    end
    object qryConvUnidadeMedidacUnidadeMedidaPara: TStringField
      FieldName = 'cUnidadeMedidaPara'
      FixedChar = True
      Size = 3
    end
    object qryConvUnidadeMedidanFatorConversaoUM: TFloatField
      FieldName = 'nFatorConversaoUM'
    end
  end
  object dsConvUnidadeMedida: TDataSource
    DataSet = qryConvUnidadeMedida
    Left = 544
    Top = 216
  end
end
