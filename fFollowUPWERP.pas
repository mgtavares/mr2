unit fFollowUPWERP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ComCtrls, ToolWin, ExtCtrls,
  GridsEh, DBGridEh, StdCtrls, DB, ADODB, cxControls, cxContainer, cxEdit,
  cxTextEdit, fProcesso_Padrao, DBCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid;

type
  TfrmFollowUPWERP = class(TfrmProcesso_Padrao)
    usp_Processo: TADOStoredProc;
    DataSource1: TDataSource;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    qryAux: TADOQuery;
    qryFollow: TADOQuery;
    DataSource2: TDataSource;
    qryFollowdDtFollowUp: TDateTimeField;
    qryFollowcNmUsuario: TStringField;
    qryFollowcOcorrenciaResum: TStringField;
    qryFollowdDtProxAcao: TDateTimeField;
    qryFollowcOcorrencia: TMemoField;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    GroupBox1: TGroupBox;
    RadioGroup1: TRadioGroup;
    cxTextEdit3: TcxTextEdit;
    Label29: TLabel;
    Label24: TLabel;
    cxTextEdit2: TcxTextEdit;
    GroupBox2: TGroupBox;
    cxGrid4: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    GroupBox3: TGroupBox;
    cxGrid1: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1dDtFollowUp: TcxGridDBColumn;
    cxGridDBTableView1cNmUsuario: TcxGridDBColumn;
    cxGridDBTableView1cOcorrenciaResum: TcxGridDBColumn;
    cxGridDBTableView1dDtProxAcao: TcxGridDBColumn;
    cxGridDBTableView1cOcorrencia: TcxGridDBColumn;
    DBMemo1: TDBMemo;
    cxGridDBTableView3nCdPedido: TcxGridDBColumn;
    cxGridDBTableView3cNmTipoPedido: TcxGridDBColumn;
    cxGridDBTableView3cNmSituacao: TcxGridDBColumn;
    cxGridDBTableView3cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView3cNrPedTerceiro: TcxGridDBColumn;
    cxGridDBTableView3cNmContato: TcxGridDBColumn;
    cxGridDBTableView3nSaldoFat: TcxGridDBColumn;
    cxGridDBTableView3nValTotalItem: TcxGridDBColumn;
    usp_ProcessonCdPedido: TIntegerField;
    usp_ProcessocNmTipoPedido: TStringField;
    usp_ProcessocNmSituacao: TStringField;
    usp_ProcessonCdTerceiro: TIntegerField;
    usp_ProcessocNmTerceiro: TStringField;
    usp_ProcessocNrPedTerceiro: TStringField;
    usp_ProcessocNmContato: TStringField;
    usp_ProcessocUltimoAcomp: TStringField;
    usp_ProcessonValTotalItem: TBCDField;
    usp_ProcessonSaldoFat: TBCDField;
    usp_ProcessoStatus: TStringField;
    cxGridDBTableView3Status: TcxGridDBColumn;
    procedure RadioGroup1Click(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure usp_ProcessoAfterScroll(DataSet: TDataSet);
    procedure ToolButton8Click(Sender: TObject);
    procedure RadioGroup2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure cxGridDBTableView3DblClick(Sender: TObject);
    procedure cxGridDBTableView3StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFollowUPWERP: TfrmFollowUPWERP;

implementation

uses fMenu, fNovo_FollowUp, fFiltroFollowUp,
  fConsultaPedComAberTerceiro_Itens, rPedidoVenda;

{$R *.dfm}

procedure TfrmFollowUPWERP.RadioGroup1Click(Sender: TObject);
begin
  inherited;

  usp_Processo.Close ;
  usp_Processo.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (RadioGroup1.ItemIndex = 0) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'T' ;

  if (RadioGroup1.ItemIndex = 1) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'A' ;

  if (RadioGroup1.ItemIndex = 2) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'C' ;


  usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'V' ;

  usp_Processo.Open ;

  //DBGridEh1.Columns[11].DisplayFormat := '#,##0.00' ;
  //DBGridEh1.Columns[10].DisplayFormat := '#,##0.00' ;



end;

procedure TfrmFollowUPWERP.ToolButton6Click(Sender: TObject);
begin
  inherited;

  if not usp_processo.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  PosicionaQuery(rptPedidoVenda.qryPedido,usp_Processo.FieldList[0].asString) ;
  PosicionaQuery(rptPedidoVenda.qryItemEstoque_Grade,usp_Processo.FieldList[0].asString) ;
  PosicionaQuery(rptPedidoVenda.qryItemAD,usp_Processo.FieldList[0].asString) ;
  PosicionaQuery(rptPedidoVenda.qryItemFormula,usp_Processo.FieldList[0].asString) ;

  rptPedidoVenda.QRSubDetail1.Enabled := True ;
  rptPedidoVenda.QRSubDetail2.Enabled := True ;
  rptPedidoVenda.QRSubDetail3.Enabled := True ;

  if (rptPedidoVenda.qryItemEstoque_Grade.eof) then
      rptPedidoVenda.QRSubDetail1.Enabled := False ;

  if (rptPedidoVenda.qryItemAD.eof) then
      rptPedidoVenda.QRSubDetail2.Enabled := False ;

  if (rptPedidoVenda.qryItemFormula.eof) then
      rptPedidoVenda.QRSubDetail3.Enabled := False ;

  rptPedidoVenda.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

  rptPedidoVenda.QuickRep1.PreviewModal;

end;

procedure TfrmFollowUPWERP.ToolButton4Click(Sender: TObject);
begin

  if not usp_processo.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (usp_Processo.FieldList[5].Value = 'Cr�tico') then
  begin
      ShowMessage('Pedido j� est� em situa��o cr�tica.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a altera��o da situa��o deste pedido?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo: exit ;
  end;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE Pedido SET cFlgCritico = 1 WHERE nCdPedido = ' + usp_Processo.FieldList[0].asString) ;

  frmMenu.Connection.BeginTrans;

  try
      qryAux.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans ;
      ShowMessage('Erro no processamento.') ;
      raise ;
  end ;

  qryAux.Close ;
  
  frmMenu.Connection.CommitTrans ;

  ShowMessage('Pedido marcado.') ;

end;

procedure TfrmFollowUPWERP.usp_ProcessoAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryFollow,usp_Processo.FieldList[0].asString) ;
end;

procedure TfrmFollowUPWERP.ToolButton8Click(Sender: TObject);
begin
  inherited;
  if not usp_processo.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;
  

  frmNovo_FollowUp.qryFollow.Close ;
  frmNovo_FollowUp.qryFollow.Open ;
  frmNovo_FollowUp.qryFollow.Insert ;
  frmNovo_FollowUp.qryFollownCdPedido.Value   := usp_Processo.FieldList[0].Value ;
  frmNovo_FollowUp.qryFollownCdUsuario.Value  := frmMenu.nCdUsuarioLogado ;
  frmNovo_FollowUp.qryFollowdDtFollowUp.Value := Now() ;
  frmNovo_FollowUp.ShowModal ;

  PosicionaQuery(qryFollow,usp_Processo.FieldList[0].asString) ;

end;

procedure TfrmFollowUPWERP.RadioGroup2Click(Sender: TObject);
begin
  inherited;
//  DBGridEh1.Columns[11].DisplayFormat := '#,##0.00' ;
//  DBGridEh1.Columns[10].DisplayFormat := '#,##0.00' ;

end;

procedure TfrmFollowUPWERP.FormShow(Sender: TObject);
begin
  inherited;

  usp_processo.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(frmFiltroFollowUp.MaskEdit1.Text) ;
  usp_processo.Parameters.ParamByName('@nCdGrupoEconomico').Value := frmMenu.ConvInteiro(frmFiltroFollowUp.MaskEdit2.Text) ;
  usp_processo.Parameters.ParamByName('@nCdTerceiro').Value       := frmMenu.nCdTerceiroUsuario ;
  usp_processo.Parameters.ParamByName('@nCdTipoPedido').Value     := frmMenu.ConvInteiro(frmFiltroFollowUp.MaskEdit4.Text) ;
  usp_processo.Parameters.ParamByName('@nCdMarca').Value          := frmMenu.ConvInteiro(frmFiltroFollowUp.MaskEdit5.Text) ;
  usp_processo.Parameters.ParamByName('@dDtPrevEntIni').Value     := frmMenu.ConvData(frmFiltroFollowUp.MaskEdit6.Text)    ;
  usp_processo.Parameters.ParamByName('@dDtPrevEntFim').Value     := frmMenu.ConvData(frmFiltroFollowUp.MaskEdit7.Text)    ;
  usp_processo.Parameters.ParamByName('@dDtPedidoIni').Value      := frmMenu.ConvData(frmFiltroFollowUp.MaskEdit8.Text)    ;
  usp_processo.Parameters.ParamByName('@dDtPedidoFim').Value      := frmMenu.ConvData(frmFiltroFollowUp.MaskEdit9.Text)    ;
  usp_processo.Parameters.ParamByName('@nCdCC').Value             := frmMenu.ConvInteiro(frmFiltroFollowUp.MaskEdit10.Text)   ;
  usp_processo.Parameters.ParamByName('@cFlgWERP').Value          := 1 ;

  usp_Processo.Parameters.ParamByName('@nCdUsuario').Value        := frmMenu.nCdUsuarioLogado ;

  if (RadioGroup1.ItemIndex = 0) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'T' ;

  if (RadioGroup1.ItemIndex = 1) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'A' ;

  if (RadioGroup1.ItemIndex = 2) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'C' ;

  usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'V' ;

  usp_Processo.Open ;

end;

procedure TfrmFollowUPWERP.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  if not usp_Processo.Eof then
  begin
      PosicionaQuery(frmConsultaPedComAberTerceiro_Itens.qryItens, usp_ProcessonCdPedido.AsString) ;
      frmConsultaPedComAberTerceiro_Itens.ShowModal;
  end ;

end;

procedure TfrmFollowUPWERP.cxGridDBTableView3DblClick(Sender: TObject);
begin
  inherited;

  if not usp_Processo.Eof then
  begin
      PosicionaQuery(frmConsultaPedComAberTerceiro_Itens.qryItens, usp_ProcessonCdPedido.AsString) ;
      frmConsultaPedComAberTerceiro_Itens.ShowModal;
  end ;

end;

procedure TfrmFollowUPWERP.cxGridDBTableView3StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if (AItem = cxGridDBTableView3nCdPedido) then
  begin

    //showMessage(ARecord.Values[3]);

    if (ARecord.Values[3] <> 'Normal') then
        AStyle := frmMenu.LinhaVermelha ;

    if (ARecord.Values[3] = 'Cr�tico') then
        AStyle := frmMenu.LinhaAmarela ;

  end ;

end;

initialization
    RegisterClass(TfrmFollowUPWERP) ;

end.
