unit fAgrupamentoContabil_Estrutura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxControls, cxContainer, cxTreeView, DB, ADODB, Menus, RDprint;

type
  TfrmAgrupamentoContabil_Estrutura = class(TfrmProcesso_Padrao)
    tvConta: TcxTreeView;
    qryContaAgrupamento: TADOQuery;
    qryContaAgrupamentonCdContaAgrupamentoContabil: TIntegerField;
    qryContaAgrupamentonCdContaAgrupamentoContabilPai: TIntegerField;
    qryContaAgrupamentonCdAgrupamentoContabil: TIntegerField;
    qryContaAgrupamentocMascara: TStringField;
    qryContaAgrupamentocNmContaAgrupamentoContabil: TStringField;
    qryContaAgrupamentocNmContaSegundoIdioma: TStringField;
    qryContaAgrupamentoiNivel: TIntegerField;
    qryContaAgrupamentoiSequencia: TIntegerField;
    qryContaAgrupamentonCdTabTipoNaturezaContaSPED: TIntegerField;
    qryContaAgrupamentonCdPlanoConta: TIntegerField;
    qryContaAgrupamentocFlgAnalSintetica: TStringField;
    qryContaAgrupamentocFlgNaturezaCredDev: TStringField;
    PopupMenu1: TPopupMenu;
    AdicionarSubConta1: TMenuItem;
    ExcluirSubConta1: TMenuItem;
    N1: TMenuItem;
    VincularContaReduzida1: TMenuItem;
    RemoverVinculoContaContbil1: TMenuItem;
    AdicionarContaContbil1: TMenuItem;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    qryAux: TADOQuery;
    AlterarSubConta1: TMenuItem;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    RDprint1: TRDprint;
    qryRelPlanoContas: TADOQuery;
    dsRelPlnoContas: TDataSource;
    qryRelPlanoContasiNivel: TIntegerField;
    qryRelPlanoContascMascara: TStringField;
    qryRelPlanoContascNmContaAgrupamentoContabil: TStringField;
    procedure FormShow(Sender: TObject);
    procedure adicionaFilhos( nCdContaAgrupamentoContabil : integer ; node : TTreeNode) ;
    procedure AdicionarSubConta1Click(Sender: TObject);
    procedure exibeEstrutura;
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure ExcluirSubConta1Click(Sender: TObject);
    procedure AlterarSubConta1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure VincularContaReduzida1Click(Sender: TObject);
    procedure atualizaExpande;
    procedure RemoverVinculoContaContbil1Click(Sender: TObject);
    procedure AdicionarContaContbil1Click(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
  private
    { Private declarations }
    raiz, rootno, no : TTreeNode;
  public
    { Public declarations }
    nCdAgrupamentoContabil : integer ;
    cNmAgrupamentoContabil : string ;
    cFormatoMascara        : string ;
  end;

var
  frmAgrupamentoContabil_Estrutura: TfrmAgrupamentoContabil_Estrutura;

implementation

uses fMenu, fAgrupamentoContabil_AddSubConta, fAgrupamentoContabil_VinculaConta, fContaContabil,fAgrupamentoContabil;

{$R *.dfm}

procedure TfrmAgrupamentoContabil_Estrutura.adicionaFilhos(
  nCdContaAgrupamentoContabil: integer ; node : TTreeNode);
var
  qryContaAgrupamentoFilho : TADOQuery;
begin

  qryContaAgrupamentoFilho := TADOQuery.Create( Self );
  qryContaAgrupamentoFilho.Connection := frmMenu.Connection;
  qryContaAgrupamentoFilho.SQL.Add('SELECT nCdContaAgrupamentoContabil, cMascara, cNmContaAgrupamentoContabil, cFlgAnalSintetica FROM ContaAgrupamentoContabil WHERE nCdContaAgrupamentoContabilPai = :nPK ORDER BY cMascara') ;

  qryContaAgrupamentoFilho.Close;
  PosicionaQuery( qryContaAgrupamentoFilho , intToStr( nCdContaAgrupamentoContabil ) ) ;

  qryContaAgrupamentoFilho.FieldList.Update;

    while not qryContaAgrupamentoFilho.eof do
    begin

      no := tvConta.Items.AddChildObject(node, frmMenu.fnAplicaMascaraContabil( qryContaAgrupamentoFilho.FieldList[1].Value , cFormatoMascara ) + ' - ' + qryContaAgrupamentoFilho.FieldList[2].Value, Pointer( qryContaAgrupamentoFilho.FieldList[0].asInteger )) ;
      no.Data := Pointer( qryContaAgrupamentoFilho.FieldList[0].asInteger ) ;

      if (qryContaAgrupamentoFilho.FieldList[3].Value = 'A') then
      begin
          no.ImageIndex    := 3;
          no.SelectedIndex := 3;
      end
      else
      begin
          no.ImageIndex    := 2;
          no.SelectedIndex := 2;
      end ;

      adicionaFilhos( qryContaAgrupamentoFilho.FieldList[0].asINteger , no ) ;

      qryContaAgrupamentoFilho.Next ;

    end ;

    qryContaAgrupamentoFilho.Close;

    freeAndNil( qryContaAgrupamentoFilho ) ;

end;

procedure TfrmAgrupamentoContabil_Estrutura.FormShow(Sender: TObject);
begin
  inherited;

  AdicionarContaContbil1.Enabled := frmMenu.fnValidaUsuarioAPL('FRMCONTACONTABIL') ;

  ToolButton1.Click;
  ToolButton5.Click;

end;

procedure TfrmAgrupamentoContabil_Estrutura.AdicionarSubConta1Click(
  Sender: TObject);
var
  objForm : TfrmAgrupamentoContabil_AddSubConta;
  i : integer;
begin
  inherited;

  objForm := TfrmAgrupamentoContabil_AddSubConta.Create( Self ) ;

  try

      objForm.cFormatoMascara := cFormatoMascara ;

      objForm.adicionaSubConta( nCdAgrupamentoContabil
                               ,Integer( tvConta.Selected.Data )
                               ,TRUE );

  finally
      freeAndNil( objForm ) ;
  end ;

  atualizaExpande;
  
end;

procedure TfrmAgrupamentoContabil_Estrutura.exibeEstrutura;
begin

  tvConta.Items.Clear;
  
  raiz := tvConta.Items.AddObject(nil, cNmAgrupamentoContabil , Pointer( 0 )) ;
  raiz.ImageIndex    := 2;
  raiz.SelectedIndex := 2;

  qryContaAgrupamento.Close;
  PosicionaQuery( qryContaAgrupamento , intToStr( nCdAgrupamentoContabil ) ) ;

  while not qryContaAgrupamento.eof do
  begin

      rootno := tvConta.Items.AddChildObject(raiz, frmMenu.fnAplicaMascaraContabil( qryContaAgrupamentocMascara.Value , cFormatoMascara ) + ' - ' + qryContaAgrupamentocNmContaAgrupamentoContabil.Value, Pointer( qryContaAgrupamentonCdContaAgrupamentoContabil.Value ) ) ;
      rootno.Data := Pointer( qryContaAgrupamentonCdContaAgrupamentoContabil.Value );

      if (qryContaAgrupamentocFlgAnalSintetica.Value = 'A') then
      begin
          rootno.ImageIndex    := 3;
          rootno.SelectedIndex := 3;
      end
      else
      begin
          rootno.ImageIndex    := 2;
          rootno.SelectedIndex := 2;
      end ;

      adicionaFilhos( qryContaAgrupamentonCdContaAgrupamentoContabil.Value , rootno ) ;

      qryContaAgrupamento.Next;

  end ;

  qryContaAgrupamento.Close;

end;

procedure TfrmAgrupamentoContabil_Estrutura.ToolButton1Click(
  Sender: TObject);
var
  i : integer;
begin
  inherited;

  exibeEstrutura ;

end;

procedure TfrmAgrupamentoContabil_Estrutura.ToolButton5Click(
  Sender: TObject);
var
  x: byte;
begin

  for x := 0 to tvConta.Items.Count - 1 do
      tvConta.Items.Item[x].Expand(True);

end;

procedure TfrmAgrupamentoContabil_Estrutura.ToolButton7Click(
  Sender: TObject);
var
  x: byte;
begin

  for x := 0 to tvConta.Items.Count - 1 do
      tvConta.Items.Item[x].Collapse(True);

end;

procedure TfrmAgrupamentoContabil_Estrutura.ExcluirSubConta1Click(
  Sender: TObject);
var
  nCdContaAgrupamentoContabil, i : integer ;
begin
  inherited;

  if (tvConta.Selected.AbsoluteIndex > 0) then
  begin

      nCdContaAgrupamentoContabil := Integer( tvConta.Selected.Data ) ;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT TOP 1 1 FROM ContaAgrupamentoContabil WHERE nCdContaAgrupamentoContabilPai = ' + intToStr( nCdContaAgrupamentoContabil ) ) ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin
          qryAux.Close;
          MensagemAlerta('Esta subconta tem contas/subcontas dependentes e n�o pode ser exclu�da.') ;
          abort;
      end ;

      qryAux.Close;

      if ( MessageDlg('Confirma a exclus�o desta subconta ?',mtConfirmation,[mbYes,mbNo],0) = MRNO ) then
          exit;

      try
          qryAux.SQL.Clear;
          qryAux.SQL.Add('DELETE FROM ContaAgrupamentoContabil WHERE nCdContaAgrupamentoContabil = ' + intToStr( nCdContaAgrupamentoContabil ) ) ;
          qryAux.ExecSQL;
      except
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

  end ;

  atualizaExpande;

end;

procedure TfrmAgrupamentoContabil_Estrutura.AlterarSubConta1Click(
  Sender: TObject);
var
  objForm : TfrmAgrupamentoContabil_AddSubConta;
  i : integer;
begin
  inherited;

  if (tvConta.Selected.AbsoluteIndex > 0) then
  begin

      objForm := TfrmAgrupamentoContabil_AddSubConta.Create( Self ) ;

      try

          objForm.cFormatoMascara := cFormatoMascara ;

          objForm.adicionaSubConta( nCdAgrupamentoContabil
                                   ,Integer( tvConta.Selected.Data )
                                   ,FALSE );

      finally
          freeAndNil( objForm ) ;
      end ;

  end ;

  atualizaExpande;

end;

procedure TfrmAgrupamentoContabil_Estrutura.PopupMenu1Popup(
  Sender: TObject);
begin
  inherited;

  try
      AdicionarSubConta1.Enabled          := (tvConta.Selected.ImageIndex = 2) ;
      AlterarSubConta1.Enabled            := (tvConta.Selected.ImageIndex = 2) ;
      ExcluirSubConta1.Enabled            := (tvConta.Selected.ImageIndex = 2) ;
      VincularContaReduzida1.Enabled      := (tvConta.Selected.ImageIndex = 2) ;

      RemoverVinculoContaContbil1.Enabled := (tvConta.Selected.ImageIndex = 3) ;
  except
  end;

end;

procedure TfrmAgrupamentoContabil_Estrutura.VincularContaReduzida1Click(
  Sender: TObject);
var
  objForm : TfrmAgrupamentoContabil_VinculaConta ;
begin
  inherited;

  if (tvConta.Selected.AbsoluteIndex > 0) then
  begin

      objForm := TfrmAgrupamentoContabil_VinculaConta.Create( Self ) ;
      objForm.novoVinculo( Integer( tvConta.Selected.Data ) );

  end ;

  atualizaExpande ;

end;

procedure TfrmAgrupamentoContabil_Estrutura.atualizaExpande;
var
    i : integer ;
begin

  i := tvConta.Selected.AbsoluteIndex;
  exibeEstrutura ;
  ToolButton5.Click;

  try
      tvConta.Items.Item[i].Selected := True ;
  except
  end;

end;

procedure TfrmAgrupamentoContabil_Estrutura.RemoverVinculoContaContbil1Click(
  Sender: TObject);
var
  nCdContaAgrupamentoContabil, i : integer ;
begin
  inherited;

  if (tvConta.Selected.AbsoluteIndex > 0) then
  begin

      nCdContaAgrupamentoContabil := Integer( tvConta.Selected.Data ) ;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT TOP 1 1 FROM ContaAgrupamentoContabil WHERE nCdContaAgrupamentoContabilPai = ' + intToStr( nCdContaAgrupamentoContabil ) ) ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin
          qryAux.Close;
          MensagemAlerta('Esta conta tem contas/subcontas dependentes e n�o pode ser exclu�da.') ;
          abort;
      end ;

      qryAux.Close;

      if ( MessageDlg('Confirma a exclus�o do v�nculo desta conta ?',mtConfirmation,[mbYes,mbNo],0) = MRNO ) then
          exit;

      try
          qryAux.SQL.Clear;
          qryAux.SQL.Add('DELETE FROM ContaAgrupamentoContabil WHERE nCdContaAgrupamentoContabil = ' + intToStr( nCdContaAgrupamentoContabil ) ) ;
          qryAux.ExecSQL;
      except
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

  end ;

  atualizaExpande;

end;

procedure TfrmAgrupamentoContabil_Estrutura.AdicionarContaContbil1Click(
  Sender: TObject);
var
  objForm : TfrmContaContabil;
begin
  inherited;

  objForm := TfrmContaContabil.Create( Self ) ;
  showForm( objForm , TRUE ) ;

end;

procedure TfrmAgrupamentoContabil_Estrutura.ToolButton9Click(
  Sender: TObject);
var
    iLinha : integer;
    iCol   : integer;
    iFolha : integer;
begin
    qryRelPlanoContas.Close;
    qryRelPlanoContas.Parameters.ParamByName('nPK').Value := nCdAgrupamentoContabil;
    qryRelPlanoContas.Open;

    iFolha := 1;

    RDprint1.Abrir();

    RDprint1.Imp(1, 2, frmMenu.cNmEmpresaAtiva);
    RDprint1.Imp(3, 30, 'Data/Hora: ' + DateTimeToStr(Now));
    RDprint1.Imp(3, 70, 'Folha: ' + IntToStr(iFolha));
    RDprint1.Imp(3, 2, 'Plano de Contas');
    RDprint1.Imp(4, 2, '______________________________________________________________________________');
    RDprint1.Imp(6, 2,  'Conta');
    RDprint1.Imp(6, 17, 'Descri��o');
    RDprint1.Imp(7,2,  '______________________________________________________________________________');
    RDprint1.Imp(63,2, '______________________________________________________________________________');
    RDprint1.Imp(65,2, 'ER2SOFT - Solu��es inteligentes para o seu neg�cio');
    RDprint1.Imp(65,62,'www.er2soft.com.br');
    RDprint1.Imp(66,2, '______________________________________________________________________________');

    iLinha := 8;

    while not (qryRelPlanoContas.Eof) do
    begin
        iCol := 0;
        iCol := (2 * qryRelPlanoContasiNivel.Value);

        RDprint1.Imp(iLinha,2,qryRelPlanoContascMascara.Value);
        RDprint1.Imp(iLinha,(15 + iCol),qryRelPlanoContascNmContaAgrupamentoContabil.Value);
        inc (iLinha);

        if (iLinha = 61) then
        begin
            iLinha := 8;
            Inc (iFolha);

            RDprint1.Novapagina;

            RDprint1.Imp(1, 2, frmMenu.cNmEmpresaAtiva);
            RDprint1.Imp(3, 30, 'Data/Hora: ' + DateTimeToStr(Now));
            RDprint1.Imp(3, 70, 'Folha: ' + IntToStr(iFolha));
            RDprint1.Imp(3, 2, 'Plano de Contas');
            RDprint1.Imp(4, 2, '______________________________________________________________________________');
            RDprint1.Imp(6, 2,  'Conta');
            RDprint1.Imp(6, 17, 'Descri��o');
            RDprint1.Imp(7,2,  '______________________________________________________________________________');
            RDprint1.Imp(63,2, '______________________________________________________________________________');
            RDprint1.Imp(65,2, 'ER2SOFT - Solu��es inteligentes para o seu neg�cio');
            RDprint1.Imp(65,62,'www.er2soft.com.br');
            RDprint1.Imp(66,2, '______________________________________________________________________________');

        end;
        qryRelPlanoContas.Next;
    end;

    RDprint1.Fechar();
  inherited;

end;

end.
