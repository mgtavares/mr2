unit fInventario_AberturaMod2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBCtrls, ADODB, StdCtrls, Mask, cxPC, cxControls, cxLookAndFeelPainters,
  cxButtons, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmInventario_AberturaMod2 = class(TfrmProcesso_Padrao)
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DataSource1: TDataSource;
    qryGrupoInventario: TADOQuery;
    qryGrupoInventarionCdGrupoInventario: TIntegerField;
    qryGrupoInventariocNmGrupoInventario: TStringField;
    DataSource2: TDataSource;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DataSource3: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    Image2: TImage;
    cmdPreparaTemp: TADOCommand;
    qrySelProduto: TADOQuery;
    qrySelProdutocFlgOK: TIntegerField;
    qrySelProdutonCdProduto: TIntegerField;
    qrySelProdutocReferencia: TStringField;
    qrySelProdutocNmProduto: TStringField;
    qrySelProdutocUnidadeMedida: TStringField;
    Panel1: TPanel;
    DBGridEh1: TDBGridEh;
    dsSelProduto: TDataSource;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    btLimparLista: TcxButton;
    cxButton4: TcxButton;
    SP_ADICIONA_PROD_INVENTARIO: TADOStoredProc;
    qryProdutoFinal: TADOQuery;
    qryProdutoFinalnCdProduto: TIntegerField;
    qryProdutoFinalcReferencia: TStringField;
    qryProdutoFinalcLocalizacao: TStringField;
    qryProdutoFinalcNmProduto: TStringField;
    qryProdutoFinalcUnidadeMedida: TStringField;
    dsProdutoFinal: TDataSource;
    Panel2: TPanel;
    DBGridEh2: TDBGridEh;
    cxButton5: TcxButton;
    qryPopulaTemp: TADOQuery;
    btLimpar2: TcxButton;
    SP_GERA_INVENTARIO_MOD2: TADOStoredProc;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    edtLocalEstoque: TMaskEdit;
    DBEdit1: TDBEdit;
    edtGrupoInventario: TMaskEdit;
    DBEdit2: TDBEdit;
    edtQuadra: TMaskEdit;
    edtRua: TMaskEdit;
    edtPrateleira: TMaskEdit;
    edtProduto: TMaskEdit;
    DBEdit3: TDBEdit;
    btExibir: TcxButton;
    Label6: TLabel;
    edtBox: TMaskEdit;
    procedure btExibirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtLocalEstoqueKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoInventarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtProdutoExit(Sender: TObject);
    procedure edtGrupoInventarioExit(Sender: TObject);
    procedure edtLocalEstoqueExit(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure btLimparListaClick(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure btLimpar2Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInventario_AberturaMod2: TfrmInventario_AberturaMod2;

implementation

uses fMenu, fLookup_Padrao, rInventario;

{$R *.dfm}

procedure TfrmInventario_AberturaMod2.btExibirClick(Sender: TObject);
begin
  inherited;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione um local de estoque.') ;
      edtLocalEstoque.SetFocus;
      exit ;
  end ;

  qryPopulaTemp.Close ;
  qryPopulaTemp.Parameters.ParamByName('nCdLocalEstoque').Value    := frmMenu.ConvInteiro(edtLocalEstoque.Text) ;
  qryPopulaTemp.Parameters.ParamByName('iQuadra').Value            := frmMenu.ConvInteiro(edtQuadra.Text) ;
  qryPopulaTemp.Parameters.ParamByName('iRua').Value               := frmMenu.ConvInteiro(edtRua.Text) ;
  qryPopulaTemp.Parameters.ParamByName('iPrateleira').Value        := frmMenu.ConvInteiro(edtPrateleira.Text) ;
  qryPopulaTemp.Parameters.ParamByName('iBox').Value               := frmMenu.ConvInteiro(edtBox.Text) ;
  qryPopulaTemp.Parameters.ParamByName('nCdGrupoInventario').Value := frmMenu.ConvInteiro(edtGrupoInventario.Text) ;
  qryPopulaTemp.Parameters.ParamByName('nCdProduto').Value         := frmMenu.ConvInteiro(edtProduto.Text) ;
  qryPopulaTemp.ExecSQL ;

  qrySelProduto.Close ;
  qrySelProduto.Open ;

  cxPageControl1.ActivePageIndex := 0 ;
  
  if qrySelProduto.eof then
  begin
      MensagemAlerta('Nenhum produto encontrado para o filtro utilizado') ;
      edtLocalEstoque.SetFocus ;
  end
  else dbGridEh1.SetFocus ;

end;

procedure TfrmInventario_AberturaMod2.FormShow(Sender: TObject);
begin
  inherited;

  cmdPreparaTemp.Execute ;
end;

procedure TfrmInventario_AberturaMod2.edtLocalEstoqueKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva));

        If (nPK > 0) then
        begin
            edtLocalEstoque.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLocalEstoque, edtLocalEstoque.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmInventario_AberturaMod2.edtGrupoInventarioKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(153);

        If (nPK > 0) then
        begin
            edtGrupoInventario.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoInventario, edtGrupoInventario.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmInventario_AberturaMod2.edtProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(76,'NOT EXISTS(SELECT 1 FROM Produto Produto2 WHERE Produto2.nCdProdutoPai = Produto.nCdProduto)');

        If (nPK > 0) then
        begin
            edtProduto.Text := IntToStr(nPK) ;
            PosicionaQuery(qryProduto, edtProduto.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmInventario_AberturaMod2.edtProdutoExit(Sender: TObject);
begin
  inherited;

  qryProduto.Close ;
  PosicionaQuery(qryProduto, edtProduto.Text);
  
end;

procedure TfrmInventario_AberturaMod2.edtGrupoInventarioExit(
  Sender: TObject);
begin
  inherited;

  qryGrupoInventario.Close ;
  PosicionaQuery(qryGrupoInventario, edtGrupoInventario.Text) ;
  
end;

procedure TfrmInventario_AberturaMod2.edtLocalEstoqueExit(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Close ;
  PosicionaQuery(qryLocalEstoque, edtLocalEstoque.Text) ;
  
end;

procedure TfrmInventario_AberturaMod2.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmInventario_AberturaMod2.cxButton1Click(Sender: TObject);
begin
  inherited;

  if not qrySelProduto.eof then
  begin

      qrySelProduto.First ;

      while not qrySelProduto.eof do
      begin
          qrySelProduto.Edit ;
          qrySelProdutocFlgOK.Value := 0 ;
          qrySelProduto.Post ;

          qrySelProduto.Next ;
      end ;

      qrySelProduto.First ;

      qrySelProduto.Close ;
      qrySelProduto.Open ;

  end ;

end;

procedure TfrmInventario_AberturaMod2.cxButton2Click(Sender: TObject);
begin
  inherited;

  if not qrySelProduto.eof then
  begin

      qrySelProduto.First ;

      while not qrySelProduto.eof do
      begin
          qrySelProduto.Edit ;
          qrySelProdutocFlgOK.Value := 1 ;
          qrySelProduto.Post ;

          qrySelProduto.Next ;
      end ;

      qrySelProduto.First ;

      qrySelProduto.Close ;
      qrySelProduto.Open ;

  end ;

end;

procedure TfrmInventario_AberturaMod2.btLimparListaClick(Sender: TObject);
begin
  inherited;

  if not qrySelProduto.eof then
  begin

      qrySelProduto.First ;

      while not qrySelProduto.eof do
      begin
          qrySelProduto.Delete;

          qrySelProduto.First ;
      end ;

      qrySelProduto.First ;

      qrySelProduto.Close ;
      qrySelProduto.Open ;

  end ;

end;

procedure TfrmInventario_AberturaMod2.cxButton4Click(Sender: TObject);
var
    iItens : integer;
begin
  inherited;

  if not qrySelProduto.Active then
  begin
      MensagemAlerta('Nenhum item selecionado.') ;
      exit ;
  end ;

  if (qrySelProduto.State <> dsBrowse) then
      qrySelProduto.Post ;
      
  iItens := 0 ;

  qrySelProduto.First ;

  while not qrySelProduto.eof do
  begin
      if (qrySelProdutocFlgOK.Value = 1) then
          iItens := iItens + 1 ;

      qrySelProduto.Next ;
  end ;

  qrySelProduto.First ;

  if (iItens <= 0) then
  begin
      MensagemAlerta('Nenhum item selecionado.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a grava��o dos itens selecionados na lista do invent�rio ?',mtConfirmation,[mbYes,mbNo],0) of
    mrNo:exit ;
  end ;

  try
      SP_ADICIONA_PROD_INVENTARIO.Close ;
      SP_ADICIONA_PROD_INVENTARIO.Parameters.ParamByName('@nCdLocalEstoque').Value := qryLocalEstoquenCdLocalEstoque.Value ;
      SP_ADICIONA_PROD_INVENTARIO.ExecProc;
  except
      MensagemErro('Erro no processamento') ;
      raise ;
  end ;

  qryProdutoFinal.Close ;
  qryProdutoFinal.Open ;

  btLimparLista.Click ;

  if not edtLocalEstoque.ReadOnly then
  begin
      edtLocalEstoque.ReadOnly := True ;
      edtLocalEstoque.Color    := $00E9E4E4 ;
  end ;

  edtGrupoInventario.SetFocus ;

end;

procedure TfrmInventario_AberturaMod2.btLimpar2Click(Sender: TObject);
begin
  inherited;

  if qryProdutoFinal.Active then
  begin

      qryProdutoFinal.First ;

      while not qryProdutoFinal.eof do
      begin
          qryProdutoFinal.Delete;
          qryProdutoFinal.First;
      end ;

      qryProdutoFinal.Close ;
      qryProdutoFinal.Open ;

  end ;

end;

procedure TfrmInventario_AberturaMod2.cxButton5Click(Sender: TObject);
var
    nCdInventario : integer ;
    objRel : TrptInventario_view;
begin
  inherited;

  if (not qryProdutoFinal.Active) or (qryProdutoFinal.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum produto selecionado para gera��o do invent�rio.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a abertura deste invent�rio ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_GERA_INVENTARIO_MOD2.Close ;
      SP_GERA_INVENTARIO_MOD2.Parameters.ParamByName('@nCdEmpresa').Value      := frmMenu.nCdEmpresaAtiva ;
      SP_GERA_INVENTARIO_MOD2.Parameters.ParamByName('@nCdLocalEstoque').Value := qryLocalEstoquenCdLocalEstoque.Value ;
      SP_GERA_INVENTARIO_MOD2.Parameters.ParamByName('@nCdUsuario').Value      := frmMenu.nCdUsuarioLogado ;
      SP_GERA_INVENTARIO_MOD2.Parameters.ParamByName('@cResponsavel').Value    := '' ;
      SP_GERA_INVENTARIO_MOD2.Parameters.ParamByName('@cOBS').Value            := '' ;
      SP_GERA_INVENTARIO_MOD2.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  nCdInventario := SP_GERA_INVENTARIO_MOD2.Parameters.ParamByName('@nCdInventario').Value ;

  ShowMessage('Invent�rio aberto com sucesso.' + #13#13 + 'N�mero do invent�rio : ' + IntToStr(nCdInventario)) ;

  case MessageDlg('Deseja imprimir o formul�rio de contagem ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes: begin

          objRel := TrptInventario_view.Create(nil);

          try
              try
                  objRel.SPREL_INVENTARIO.Close ;
                  objRel.SPREL_INVENTARIO.Parameters.ParamByName('@nCdInventario').Value := nCdInventario ;
                  objRel.SPREL_INVENTARIO.Open ;

                  objRel.lblEmpresa.Caption      := frmMenu.cNmEmpresaAtiva;
                  objRel.lblEmpresa2.Caption     := frmMenu.cNmEmpresaAtiva;
                  objRel.lblInventario.Caption   := IntToStr(nCdInventario) ;
                  objRel.Preview;
              except
                  MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
                  raise ;
              end ;

          finally
              FreeAndNil(objRel);
          end;

      end ;
  end ;


  btLimpar2.Click ;
  btLimparLista.Click ;

  cxPageControl1.ActivePageIndex := 0 ;

  edtLocalEstoque.SetFocus ;


end;

procedure TfrmInventario_AberturaMod2.ToolButton1Click(Sender: TObject);
begin
  inherited;

  btLimparLista.Click;
  btLimpar2.Click;

  edtLocalEstoque.Color    := clWhite ;
  edtLocalEstoque.ReadOnly := False ;

  qryGrupoInventario.Close;
  qryProduto.Close;
  edtGrupoInventario.Text := '' ;
  edtQuadra.Text          := '' ;
  edtRua.Text             := '' ;
  edtPrateleira.Text      := '' ;
  edtBox.Text             := '' ;
  edtProduto.Text         := '' ;

  edtLocalEstoque.SetFocus;
  
end;

initialization
    RegisterClass(TfrmInventario_AberturaMod2) ;
    
end.
