unit fOperadorPCP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, ER2Lookup;

type
  TfrmOperadorPCP = class(TfrmCadastro_Padrao)
    qryMasternCdOperadorPCP: TIntegerField;
    qryMastercNmOperadorPCP: TStringField;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    qryMasternCdStatus: TIntegerField;
    Label3: TLabel;
    edtStatus: TER2LookupDBEdit;
    DBEdit3: TDBEdit;
    DataSource1: TDataSource;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOperadorPCP: TfrmOperadorPCP;

implementation

{$R *.dfm}

procedure TfrmOperadorPCP.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'OPERADORPCP' ;
  nCdTabelaSistema  := 424 ;
  nCdConsultaPadrao := 1022 ;
end;
procedure TfrmOperadorPCP.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe o Operador PCP.') ;
      DBEdit2.SetFocus;
      abort ;
  end ;
  inherited;

end;

procedure TfrmOperadorPCP.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmOperadorPCP.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryStatus.Close;
end;

procedure TfrmOperadorPCP.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryStatus.Close;
  PosicionaQuery(qryStatus,qryMasternCdStatus.AsString);
end;

initialization
    RegisterClass(TfrmOperadorPCP) ;

end.
