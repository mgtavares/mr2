inherited frmLoteLiqCobradora_Cliente: TfrmLoteLiqCobradora_Cliente
  Left = 332
  Top = 257
  Width = 723
  Height = 182
  BorderIcons = [biSystemMenu]
  Caption = 'Adicionar Baixa'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 144
    Width = 715
    Height = 7
  end
  inherited ToolBar1: TToolBar
    Width = 715
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 715
    Height = 115
    Align = alTop
    Caption = 'Dados da Baixa'
    TabOrder = 1
    object Label5: TLabel
      Tag = 1
      Left = 40
      Top = 24
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cliente'
    end
    object Label1: TLabel
      Tag = 1
      Left = 9
      Top = 56
      Width = 64
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo de Baixa'
    end
    object Label2: TLabel
      Tag = 1
      Left = 20
      Top = 88
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Baixa'
    end
    object edtCliente: TMaskEdit
      Left = 81
      Top = 16
      Width = 70
      Height = 21
      EditMask = '############;1; '
      MaxLength = 12
      TabOrder = 0
      Text = '            '
      OnExit = edtClienteExit
      OnKeyDown = edtClienteKeyDown
    end
    object cmbTipoBaixa: TComboBox
      Left = 81
      Top = 48
      Width = 145
      Height = 21
      Style = csDropDownList
      ItemHeight = 13
      TabOrder = 1
      OnChange = cmbTipoBaixaChange
      Items.Strings = (
        'TOTAL'
        'PARCIAL')
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 160
      Top = 16
      Width = 153
      Height = 21
      DataField = 'cCNPJCPF'
      DataSource = dsCliente
      TabOrder = 2
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 320
      Top = 16
      Width = 377
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = dsCliente
      TabOrder = 3
    end
  end
  object edtValorBaixa: TcxCurrencyEdit [3]
    Left = 80
    Top = 112
    Width = 145
    Height = 21
    ParentFont = False
    Properties.DisplayFormat = ',0.00;-,0.00'
    Properties.ValidateOnEnter = True
    Style.Font.Charset = DEFAULT_CHARSET
    Style.Font.Color = clWindowText
    Style.Font.Height = -11
    Style.Font.Name = 'Consolas'
    Style.Font.Style = []
    Style.LookAndFeel.Kind = lfStandard
    Style.LookAndFeel.NativeStyle = False
    TabOrder = 2
  end
  inherited ImageList1: TImageList
    Top = 80
  end
  object qryCliente: TADOQuery
    Connection = frmMenu.Connection
    AfterOpen = qryClienteAfterOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT Terceiro.nCdTerceiro        '
      '      ,Terceiro.cNmTerceiro        '
      '      ,Terceiro.cCNPJCPF        '
      '  FROM Terceiro            '
      ' WHERE EXISTS(SELECT 1                                     '
      
        '                FROM TerceiroTipoTerceiro TTT                   ' +
        '                                '
      
        '               WHERE TTT.nCdTerceiro = Terceiro.nCdTerceiro     ' +
        '                                                    '
      '                 AND TTT.nCdTipoTerceiro = 2)'
      '   AND Terceiro.nCdTerceiro = :nPK')
    Left = 280
    Top = 77
    object qryClientenCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryClientecNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryClientecCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
  end
  object dsCliente: TDataSource
    DataSet = qryCliente
    Left = 280
    Top = 109
  end
  object qrySomaTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCobradora'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro  int'
      '       ,@nCdEmpresa   int'
      '       ,@nCdCobradora int'
      ''
      'SET @nCdTerceiro  = :nCdTerceiro'
      'SET @nCdEmpresa   = :nCdEmpresa'
      'SET @nCdCobradora = :nCdCobradora'
      ''
      'SELECT Sum(nSaldoTit) as nSaldoTit'
      '  FROM Titulo'
      ' WHERE nCdTerceiro    = @nCdTerceiro'
      '   AND nCdEmpresa     = @nCdEmpresa'
      '   AND nCdCobradora   = @nCdCobradora'
      '   AND nSaldoTit      > 0'
      '   AND cSenso         = '#39'C'#39
      '   AND dDtCancel     IS NULL'
      '   AND cFlgCobradora  = 1')
    Left = 344
    Top = 77
    object qrySomaTitnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object qryValidaCliente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoteLiqCobradora'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdLoteLiqCobradora int'
      '       ,@nCdTerceiro      int'
      ''
      'SET @nCdLoteLiqCobradora = :nCdLoteLiqCobradora'
      'SET @nCdTerceiro         = :nCdTerceiro'
      ''
      'SELECT TOP 1 1'
      '  FROM ItemLoteLiqCobradora'
      ' WHERE nCdLoteLiqCobradora = @nCdLoteLiqCobradora'
      '   AND nCdTerceiro = @nCdTerceiro')
    Left = 312
    Top = 77
    object qryValidaClienteCOLUMN1: TIntegerField
      FieldName = 'COLUMN1'
      ReadOnly = True
    end
  end
end
