unit fInventarioLoja_Contagem;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping, ToolCtrlsEh, Menus;

type
  TfrmInventarioLoja_Contagem = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryInventario: TADOQuery;
    dsInventario: TDataSource;
    qryInventarionCdInventario: TIntegerField;
    qryInventarionCdLoja: TStringField;
    qryInventariodDtAbertura: TDateTimeField;
    qryInventariocNmInventario: TStringField;
    qryInventariocNmLocalEstoque: TStringField;
    qryInventariocNmUsuario: TStringField;
    qryInventariodDtContagemEncerrada: TDateTimeField;
    qryCancelaInventario: TADOQuery;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    qryReabrirInventario: TADOQuery;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure CancelarInventrio1Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInventarioLoja_Contagem: TfrmInventarioLoja_Contagem;

implementation

uses fMenu, fInventarioLoja_Contagem_ExibeLotes;

{$R *.dfm}

procedure TfrmInventarioLoja_Contagem.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align := alClient ;

  ToolButton1.Click;
  
end;

procedure TfrmInventarioLoja_Contagem.ToolButton1Click(Sender: TObject);
begin
  inherited;
  qryInventario.Close;
  qryInventario.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryInventario.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  qryInventario.Open;


end;

procedure TfrmInventarioLoja_Contagem.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmInventarioLoja_Contagem_ExibeLotes;
begin
  inherited;

  if (qryInventarionCdInventario.Value > 0) then
  begin
      objForm := TfrmInventarioLoja_Contagem_ExibeLotes.Create(nil);

      objForm.qryLoteInventario.Parameters.ParamByName('cFlgExibeTodos').Value := 1;
      PosicionaQuery(objForm.qryLoteInventario, qryInventarionCdInventario.AsString) ;

      objForm.cFlgContagemEncerrada := 0 ;

      if (qryInventariodDtContagemEncerrada.AsString <> '') then
          objForm.cFlgContagemEncerrada := 1 ;

      showForm(objForm,true) ;

      qryInventario.Requery();
  end ;

end;

procedure TfrmInventarioLoja_Contagem.CancelarInventrio1Click(
  Sender: TObject);
begin
  inherited;

  if not (qryInventariodDtContagemEncerrada.IsNull) then
  begin
      MensagemAlerta('A contagem do invent�rio j� foi encerrada.');

      Exit;
  end;

  if ((MessageDLG('Confirma o cancelamento do invent�rio selecionado?', mtConfirmation, [mbYes, mbNo], 0)) = mrNo) then
      Exit;

  try
      qryCancelaInventario.Parameters.ParamByName('nPK').Value              := qryInventarionCdInventario.Value;
      qryCancelaInventario.Parameters.ParamByName('nCdUsuarioCancel').Value := frmMenu.nCdUsuarioLogado;
      qryCancelaInventario.ExecSQL;
  except
      MensagemErro('Erro no processamento.');

      Raise;

      Abort;
  end;

  ShowMessage('Invent�rio cancelado com sucesso.');

  qryInventario.Close;
  qryInventario.Open;
end;

procedure TfrmInventarioLoja_Contagem.ToolButton7Click(Sender: TObject);
begin
  inherited;

  if (qryInventario.IsEmpty) then
      Exit;

  if not (qryInventariodDtContagemEncerrada.IsNull) then
  begin
      MensagemAlerta('A contagem do invent�rio j� foi encerrada.');

      Exit;
  end;

  if ((MessageDLG('Confirma o cancelamento do invent�rio selecionado?', mtConfirmation, [mbYes, mbNo], 0)) = mrNo) then
      Exit;

  try
      qryCancelaInventario.Parameters.ParamByName('nPK').Value              := qryInventarionCdInventario.Value;
      qryCancelaInventario.Parameters.ParamByName('nCdUsuarioCancel').Value := frmMenu.nCdUsuarioLogado;
      qryCancelaInventario.ExecSQL;
  except
      MensagemErro('Erro no processamento.');

      Raise;

      Abort;
  end;

  ShowMessage('Invent�rio cancelado com sucesso.');

  qryInventario.Close;
  qryInventario.Open;
end;

procedure TfrmInventarioLoja_Contagem.ToolButton5Click(Sender: TObject);
begin
  inherited;

  if (qryInventario.IsEmpty) then
      Exit;

  if (qryInventariodDtContagemEncerrada.IsNull) then
  begin
      MensagemAlerta('A contagem do invent�rio ainda n�o foi encerrada.');
      Exit;
  end;

  if ((MessageDLG('Confirma a reabertura de contagem do invent�rio selecionado?', mtConfirmation, [mbYes, mbNo], 0)) = mrNo) then
      Exit;

  try
      try
          frmMenu.Connection.BeginTrans;

          qryReabrirInventario.Close;
          qryReabrirInventario.Parameters.ParamByName('nPK').Value := qryInventarionCdInventario.Value;
          qryReabrirInventario.ExecSQL;

          frmMenu.Connection.CommitTrans;

          ShowMessage('Reabertura de contagem efetuada com sucesso.');
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      qryInventario.Requery();
  end;
end;

initialization
    registerClass(TfrmInventarioLoja_Contagem) ;

end.
