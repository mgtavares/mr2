unit fCampanhaPromoc_IncluiProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, DB, DBCtrls, ADODB, cxLookAndFeelPainters, cxButtons;

type
  TfrmCampanhaPromoc_IncluiProduto = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    edtDepartamento: TMaskEdit;
    Label1: TLabel;
    edtCategoria: TMaskEdit;
    Label2: TLabel;
    edtSubCategoria: TMaskEdit;
    Label3: TLabel;
    edtSegmento: TMaskEdit;
    Label4: TLabel;
    edtMarca: TMaskEdit;
    Label5: TLabel;
    edtLinha: TMaskEdit;
    Label6: TLabel;
    edtClasse: TMaskEdit;
    Label7: TLabel;
    edtGrupoProduto: TMaskEdit;
    Label8: TLabel;
    edtReferencia: TEdit;
    Label9: TLabel;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryCategoria: TADOQuery;
    qryCategorianCdCategoria: TAutoIncField;
    qryCategoriacNmCategoria: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategoriacNmSubCategoria: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentocNmSegmento: TStringField;
    DBEdit4: TDBEdit;
    DataSource4: TDataSource;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    DBEdit5: TDBEdit;
    DataSource5: TDataSource;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    DBEdit6: TDBEdit;
    DataSource6: TDataSource;
    qryClasse: TADOQuery;
    qryClassenCdClasseProduto: TIntegerField;
    qryClassecNmClasseProduto: TStringField;
    DBEdit7: TDBEdit;
    DataSource7: TDataSource;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBEdit8: TDBEdit;
    DataSource8: TDataSource;
    cxButton1: TcxButton;
    Label10: TLabel;
    edtColecao: TMaskEdit;
    qryColecao: TADOQuery;
    dsColecao: TDataSource;
    qryColecaonCdColecao: TIntegerField;
    qryColecaocNmColecao: TStringField;
    DBEdit9: TDBEdit;
    procedure cxButton1Click(Sender: TObject);
    procedure edtDepartamentoExit(Sender: TObject);
    procedure edtCategoriaExit(Sender: TObject);
    procedure edtSubCategoriaExit(Sender: TObject);
    procedure edtSegmentoExit(Sender: TObject);
    procedure edtMarcaExit(Sender: TObject);
    procedure edtLinhaExit(Sender: TObject);
    procedure edtClasseExit(Sender: TObject);
    procedure edtGrupoProdutoExit(Sender: TObject);
    procedure edtDepartamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLinhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClasseKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtColecaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtColecaoExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTabTipoProd : Integer;
  end;

var
  frmCampanhaPromoc_IncluiProduto: TfrmCampanhaPromoc_IncluiProduto;

implementation

uses fCampanhaPromoc_ExibeProduto, fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmCampanhaPromoc_IncluiProduto.cxButton1Click(Sender: TObject);
var
  objForm : TfrmCampanhaPromoc_ExibeProduto;
begin

  objForm := TfrmCampanhaPromoc_ExibeProduto.Create(nil);
  try
      try

          objForm.qryPreparaTemp.ExecSQL;

          objForm.qryIncluiProdutos.Close ;
          objForm.qryIncluiProdutos.Parameters.ParamByName('nCdDepartamento').Value := frmMenu.ConvInteiro(edtDepartamento.Text) ;
          objForm.qryIncluiProdutos.Parameters.ParamByName('nCdCategoria').Value    := frmMenu.ConvInteiro(edtCategoria.Text) ;
          objForm.qryIncluiProdutos.Parameters.ParamByName('nCdSubCategoria').Value := frmMenu.ConvInteiro(edtSubCategoria.Text) ;
          objForm.qryIncluiProdutos.Parameters.ParamByName('nCdSegmento').Value     := frmMenu.ConvInteiro(edtSegmento.Text) ;
          objForm.qryIncluiProdutos.Parameters.ParamByName('nCdMarca').Value        := frmMenu.ConvInteiro(edtMarca.Text) ;
          objForm.qryIncluiProdutos.Parameters.ParamByName('nCdLinha').Value        := frmMenu.ConvInteiro(edtLinha.Text) ;
          objForm.qryIncluiProdutos.Parameters.ParamByName('nCdClasse').Value       := frmMenu.ConvInteiro(edtClasse.Text) ;
          objForm.qryIncluiProdutos.Parameters.ParamByName('nCdGrupoProduto').Value := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
          objForm.qryIncluiProdutos.Parameters.ParamByName('nCdColecao').Value      := frmMenu.ConvInteiro(edtColecao.Text) ;
          objForm.qryIncluiProdutos.Parameters.ParamByName('cReferencia').Value     := trim(edtReferencia.Text) ;
          objForm.qryIncluiProdutos.Parameters.ParamByName('nCdTabTipoProd').Value  := nCdTabTipoProd;

          objForm.qryIncluiProdutos.ExecSQL;
          

          objForm.qryProdutos.Close ;
          objForm.qryProdutos.Open ;

          if (objForm.qryProdutos.Eof) then
          begin
              MensagemAlerta('Nenhum produto selecionado para o crit�rio utilizado.') ;
              exit ;
          end ;

          objForm.cxGridDBTableView1.ViewData.Expand(True) ;
          objForm.ShowModal ;
      except
          MensagemErro('Erro na cria��o do formulario');
          raise;
      end;
  finally
      FreeAndNil(objForm);
  end;

  close ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtDepartamentoExit(
  Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtCategoriaExit(
  Sender: TObject);
begin
  inherited;

  if (trim(edtCategoria.Text) <> '') and (qryDepartamento.Eof) then
  begin
      MensagemAlerta('Selecione o departamento.') ;
      edtCategoria.Text := '' ;
      edtDepartamento.SetFocus;
      abort ;
  end ;

  qryCategoria.Close ;
  qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := 0 ;

  if not qryDepartamento.eof then
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

  PosicionaQuery(qryCategoria, edtCategoria.Text) ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtSubCategoriaExit(
  Sender: TObject);
begin
  inherited;

  if (trim(edtSubCategoria.Text) <> '') and (qryCategoria.Eof) then
  begin
      MensagemAlerta('Selecione a categoria.') ;
      edtSubCategoria.Text := '' ;
      edtCategoria.SetFocus;
      abort ;
  end ;

  qrySubCategoria.Close ;
  qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := 0 ;

  if not qryCategoria.eof then
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

  PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtSegmentoExit(
  Sender: TObject);
begin
  inherited;

  if (trim(edtSegmento.Text) <> '') and (qrySubCategoria.Eof) then
  begin
      MensagemAlerta('Selecione a Sub categoria.') ;
      edtSegmento.Text := '' ;
      edtSubCategoria.SetFocus;
      abort ;
  end ;

  qrySegmento.Close ;
  qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := 0 ;

  if not qrySegmento.eof then
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;

  PosicionaQuery(qrySegmento, edtSegmento.Text) ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtMarcaExit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, edtMarca.Text) ;
  
end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtLinhaExit(Sender: TObject);
begin
  inherited;

  if (trim(edtLinha.Text) <> '') and (qryMarca.Eof) then
  begin
      MensagemAlerta('Selecione a marca.') ;
      edtLinha.Text := '' ;
      edtMarca.SetFocus;
      abort ;
  end ;

  qryLinha.Close ;
  qryLinha.Parameters.ParamByName('nCdMarca').Value := 0 ;

  if not qryMarca.eof then
      qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

  PosicionaQuery(qryLinha, edtLinha.Text) ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtClasseExit(Sender: TObject);
begin
  inherited;

  qryClasse.Close ;
  PosicionaQuery(qryClasse, edtClasse.Text) ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtGrupoProdutoExit(
  Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtDepartamentoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(45);
        If (nPK > 0) then
        begin

            edtDepartamento.Text := intTostr(nPK) ;
            PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;
            qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

            // consulta de categoria
            nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);
            If (nPK > 0) then
            begin

                edtCategoria.Text := intTostr(nPK) ;
                PosicionaQuery(qryCategoria, edtCategoria.Text) ;
                qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

                // consulta de subcategoria
                nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
                If (nPK > 0) then
                begin
                    edtSubCategoria.Text := intTostr(nPK) ;
                    PosicionaQuery(qrySubCategoria,edtSubCategoria.Text) ;

                    qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;

                    // Consulta de segmento
                    nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
                    If (nPK > 0) then
                    begin
                        edtSegmento.Text := intTostr(nPK) ;
                        PosicionaQuery(qrySegmento,edtSegmento.Text) ;

                        edtMarca.SetFocus ;

                    end ;

                end ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            edtMarca.Text := intTostr(nPK) ;
            PosicionaQuery(qryMarca, edtMarca.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtLinhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBedit5.Text = '') then
        begin
            MensagemAlerta('Selecione a marca.') ;
            edtLinha.Text := '' ;
            edtMarca.SetFocus;
            abort ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdMarca = ' + edtMarca.Text);

        If (nPK > 0) then
        begin
            edtLinha.Text := intTostr(nPK) ;
            PosicionaQuery(qryLinha, edtLinha.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtClasseKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
        begin
            edtClasse.Text := intTostr(nPK) ;
            PosicionaQuery(qryClasse, edtClasse.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtGrupoProdutoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin
            edtGrupoProduto.Text := intTostr(nPK) ;
            PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  qryCategoria.Close ;
  qrySubCategoria.Close ;
  qrySegmento.Close ;
  qryMarca.Close ;
  qryLinha.Close ;
  qryClasse.Close ;
  qryGrupoProduto.Close ;
  qryColecao.Close ;

  edtDepartamento.Text := '' ;
  edtCategoria.Text    := '' ;
  edtSubCategoria.Text := '' ;
  edtSegmento.Text     := '' ;
  edtMarca.Text        := '' ;
  edtLinha.Text        := '' ;
  edtGrupoProduto.Text := '' ;
  edtClasse.Text       := '' ;
  edtReferencia.Text   := '' ;
  edtColecao.Text      := '' ;

  edtDepartamento.SetFocus ;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.FormShow(Sender: TObject);
begin
  inherited;

  ToolButton1.Click;

end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtColecaoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
    inherited;

    if (Key = vk_F4) then
    begin
        nPK := frmLookup_Padrao.ExecutaConsulta(52);

        if (nPK > 0) then
        begin
            edtColecao.Text := IntToStr(nPK) ;

            PosicionaQuery(qryColecao, edtColecao.Text) ;
        end;
    end;
end;

procedure TfrmCampanhaPromoc_IncluiProduto.edtColecaoExit(Sender: TObject);
begin
    inherited;

    qryColecao.Close ;

    PosicionaQuery(qryColecao, edtColecao.Text) ;
end;

procedure TfrmCampanhaPromoc_IncluiProduto.FormCreate(Sender: TObject);
begin
  inherited;

  nCdTabTipoProd := 2; //marca como padr�o produto inter. para produtos grade
end;

end.
