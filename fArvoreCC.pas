unit fArvoreCC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, ToolWin, ExtCtrls, cxControls, cxContainer, cxTreeView,
  DB, ADODB, ImgList;

type
  TfrmArvoreCC = class(TForm)
    Image1: TImage;
    tvCC: TcxTreeView;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcCdCC: TStringField;
    qryCCcNmCC: TStringField;
    qryCCiNivel: TSmallintField;
    ImageList1: TImageList;
    ToolBar2: TToolBar;
    ToolButton11: TToolButton;
    ToolButton5: TToolButton;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure tvCCExpanded(Sender: TObject; Node: TTreeNode);
    procedure tvCCCollapsed(Sender: TObject; Node: TTreeNode);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
    raiz, rootno, no : TTreeNode;
  public
    { Public declarations }
  end;

var
  frmArvoreCC: TfrmArvoreCC;

implementation

{$R *.dfm}

procedure TfrmArvoreCC.ToolButton1Click(Sender: TObject);
begin
    Close ;
end;

procedure TfrmArvoreCC.FormShow(Sender: TObject);
var
    i: integer ;
begin
    qryCC.Close ;
    qryCC.Open  ;


    tvCC.Items.Clear;

    while not qryCC.Eof do
    begin

      if qryCCiNivel.Value = 1 then
      begin
          raiz := tvCC.Items.AddObject(nil, Trim(qryCCcCdCC.AsString) + ' - ' + qryCCcNmCC.Value + '(' + qryCCnCdCC.AsString + ')', Pointer(qryCCnCdCC.Value)) ;
          raiz.ImageIndex    := 0;
          raiz.SelectedIndex := 0;
      end;
      if qryCCiNivel.Value = 2 then
      begin
          rootno := tvCC.Items.AddChildObject(raiz, Trim(qryCCcCdCC.AsString) + ' - ' + qryCCcNmCC.Value + '(' + qryCCnCdCC.AsString + ')', Pointer(qryCCnCdCC.Value)) ;
          rootno.ImageIndex    := 0;
          rootno.SelectedIndex := 0;
      end;
      if qryCCiNivel.Value = 3 then
          if Assigned(rootno) then
          begin
              no := tvCC.Items.AddChildObject(rootno, Trim(qryCCcCdCC.AsString) + ' - ' + qryCCcNmCC.Value + '(' + qryCCnCdCC.AsString + ')', Pointer(qryCCnCdCC.Value));
              no.ImageIndex    := 2;
              no.SelectedIndex := 2;
          end;

      qryCC.Next;

      i := i + 1 ;

    end;

    qryCC.Close ;

end;

procedure TfrmArvoreCC.tvCCExpanded(Sender: TObject; Node: TTreeNode);
begin
    Node.ImageIndex    := 1;
    Node.SelectedIndex := 1;
end;

procedure TfrmArvoreCC.tvCCCollapsed(Sender: TObject; Node: TTreeNode);
begin
   Node.ImageIndex    := 0;
   Node.SelectedIndex := 0;
end;

procedure TfrmArvoreCC.ToolButton5Click(Sender: TObject);
begin
    Close;
end;

end.
