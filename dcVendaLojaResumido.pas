unit dcVendaLojaResumido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls, ER2Lookup;

type
  TdcmVendaLojaResumido = class(TfrmRelatorio_Padrao)
    Label1: TLabel;
    Label2: TLabel;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    dsLoja: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentocNmDepartamento: TStringField;
    DBEdit2: TDBEdit;
    dsDepartamento: TDataSource;
    qryDepartamentonCdDepartamento: TIntegerField;
    edtReferencia: TEdit;
    Label10: TLabel;
    Label14: TLabel;
    qryCampanhaPromoc: TADOQuery;
    qryCampanhaPromocnCdCampanhaPromoc: TIntegerField;
    qryCampanhaPromoccNmCampanhaPromoc: TStringField;
    qryCampanhaPromoccFlgAtivada: TIntegerField;
    qryCampanhaPromoccFlgSuspensa: TIntegerField;
    DBEdit11: TDBEdit;
    dsCampanhaPromocional: TDataSource;
    edtDtFinal: TMaskEdit;
    Label8: TLabel;
    edtDtInicial: TMaskEdit;
    Label9: TLabel;
    GroupBox1: TGroupBox;
    chkDimAno: TCheckBox;
    chkDimMes: TCheckBox;
    chkDimLoja: TCheckBox;
    chkDimDataPedido: TCheckBox;
    chkDimVendedor: TCheckBox;
    chkDimFormaPagto: TCheckBox;
    chkDimDepartamento: TCheckBox;
    GroupBox2: TGroupBox;
    chkMedQtdeVenda: TCheckBox;
    chkMedValVenda: TCheckBox;
    chkMedMC: TCheckBox;
    edtLoja: TER2LookupMaskEdit;
    edtDepartamento: TER2LookupMaskEdit;
    edtCdCampanhaPromoc: TER2LookupMaskEdit;
    dsTipoPedido: TDataSource;
    qryTipoPedido: TADOQuery;
    qryTipoPedidocNmTipoPedido: TStringField;
    DBEdit12: TDBEdit;
    edtTipoPedido: TMaskEdit;
    Label21: TLabel;
    chkConsidAcresc: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtTipoPedidoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTipoPedidoExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmVendaLojaResumido: TdcmVendaLojaResumido;

implementation

uses fMenu, fLookup_Padrao, dcVendaLojaResumido_view, Math;

{$R *.dfm}

procedure TdcmVendaLojaResumido.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value           := frmMenu.nCdUsuarioLogado ;
  qryCampanhaPromoc.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

end;

procedure TdcmVendaLojaResumido.ToolButton1Click(Sender: TObject);
var
  cFiltro : string ;
  i : integer ;
  objForm : TdcmVendaLojaResumido_view;
begin
  inherited;

  if (trim(edtDtInicial.Text) = '/  /') or (Trim(edtDtFinal.Text) = '/  /') then
  begin
      edtDtFinal.Text   := DateToStr(Date) ;
      edtDtInicial.Text := DateToStr(Date) ;
  end ;

  if (StrToDate(edtDtInicial.Text) > StrToDate(edtDtFinal.Text)) then
  begin
      MensagemAlerta('Per�odo de venda inv�lido.') ;
      edtDtInicial.SetFocus;
      exit ;
  end ;

  objForm := TdcmVendaLojaResumido_view.Create(nil);

  objForm.ADODataSet1.Close;
  objForm.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.nCdEmpresaAtiva ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdLoja').Value           := frmMenu.ConvInteiro(edtLoja.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdDepartamento').Value   := frmMenu.ConvInteiro(edtDepartamento.Text) ;   
  objForm.ADODataSet1.Parameters.ParamByName('nCdCategoria').Value      := frmMenu.ConvInteiro('') ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdSubCategoria').Value   := frmMenu.ConvInteiro('') ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdSegmento').Value       := frmMenu.ConvInteiro('') ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdMarca').Value          := frmMenu.ConvInteiro('') ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdLinha').Value          := frmMenu.ConvInteiro('') ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdColecao').Value        := frmMenu.ConvInteiro('') ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdClasseProduto').Value  := frmMenu.ConvInteiro('') ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdUsuario').Value        := frmMenu.nCdUsuarioLogado;
  objForm.ADODataSet1.Parameters.ParamByName('nCdGrupoProduto').Value   := frmMenu.ConvInteiro('') ;
  objForm.ADODataSet1.Parameters.ParamByName('cReferencia').Value       := edtReferencia.Text ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdCampanhaPromoc').Value := frmMenu.ConvInteiro(edtCdCampanhaPromoc.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtInicial').Value        := frmMenu.ConvData(edtDtInicial.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtFinal').Value          := frmMenu.ConvData(edtDtFinal.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtInicial2').Value       := frmMenu.ConvData(edtDtInicial.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtFinal2').Value         := frmMenu.ConvData(edtDtFinal.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtInicial3').Value       := frmMenu.ConvData(edtDtInicial.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('dDtFinal3').Value         := frmMenu.ConvData(edtDtFinal.Text) ;
  objForm.ADODataSet1.Parameters.ParamByName('nCdTipoPedido').Value     := frmMenu.ConvInteiro(edtTipoPedido.Text);
  objForm.ADODataSet1.Parameters.ParamByName('cFlgConsidAcr').Value     := IfThen(chkConsidAcresc.Checked,1,0);
  objForm.ADODataSet1.Open ;

  if (objForm.ADODataSet1.eof) then
  begin
      ShowMessage('Nenhuma informa��o encontrada.') ;
      exit ;
  end ;

  frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

  if objForm.PivotCube1.Active then
  begin
      objForm.PivotCube1.Active := False;
  end;

  {-- Dimenens�es do cubo --}

  objForm.PivotCube1.Dimensions[0].Enabled  := (chkDimAno.Checked) ;
  objForm.PivotCube1.Dimensions[1].Enabled  := (chkDimMes.Checked) ;
  objForm.PivotCube1.Dimensions[2].Enabled  := (chkDimLoja.Checked) ;
  objForm.PivotCube1.Dimensions[3].Enabled  := (chkDimDataPedido.Checked) ;
  objForm.PivotCube1.Dimensions[4].Enabled  := (chkDimVendedor.Checked) ;
  objForm.PivotCube1.Dimensions[5].Enabled  := (chkDimFormaPagto.Checked) ;
  objForm.PivotCube1.Dimensions[6].Enabled  := (chkDimDepartamento.Checked) ;


  objForm.PivotCube1.ExtendedMode := True;
  objForm.PVMeasureToolBar1.HideButtons := False;
  objForm.PivotCube1.Active := True;

  objForm.PivotMap1.Measures[2].ColumnPercent := True;
  objForm.PivotMap1.Measures[2].Value := False;

  objForm.PivotMap1.Measures[3].Rank := True ;
  objForm.PivotMap1.Measures[3].Value := False;


  objForm.PivotGrid1.ColWidths[0] := 500 ;

  objForm.PivotMap1.SortColumn(0,7,False) ;

  objForm.PivotMap1.Title := 'ER2Soft - An�lise de Vendas por Loja - Resumido' ;


  {-- exibe todas as medidas que ser�o utilizadas para a forma resumida --}
  objForm.PivotMap1.ShowMeasure(0);
  objForm.PivotMap1.ShowMeasure(1);
  objForm.PivotMap1.ShowMeasure(2);
  objForm.PivotMap1.ShowMeasure(3);

  {-- oculta as medidas n�o selecionadas --}
  if (not chkMedQtdeVenda.Checked) then
      objForm.PivotMap1.HideMeasure(0);

  if (not chkMedValVenda.Checked) then
      objForm.PivotMap1.HideMeasure(1);

  if (not chkMedMC.Checked) then
      objForm.PivotMap1.HideMeasure(2);

  objForm.PivotGrid1.RefreshData;

  frmMenu.StatusBar1.Panels[6].Text := '' ;

  showForm(objForm,true) ;
  Self.Activate ;

end;

procedure TdcmVendaLojaResumido.edtTipoPedidoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(71);

        If (nPK > 0) then
            edtTipoPedido.Text := IntToStr(nPK) ;

    end ;

  end ;
end;

procedure TdcmVendaLojaResumido.edtTipoPedidoExit(Sender: TObject);
begin
  qryTipoPedido.Close;
  PosicionaQuery(qryTipoPedido,edtTipoPedido.Text);
end;

initialization
    RegisterClass(TdcmVendaLojaResumido) ;

end.
