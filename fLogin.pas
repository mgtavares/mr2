unit fLogin;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, cxLookAndFeelPainters, StdCtrls, cxButtons,
  cxCheckBox, cxControls, cxContainer, cxEdit, cxTextEdit, cxProgressBar,
  jpeg;

type
  TfrmLogin = class(TForm)
    Image2: TImage;
    cxButton1: TcxButton;
    EdtServidor: TcxTextEdit;
    EdtLogin: TcxTextEdit;
    edtSenha: TcxTextEdit;
    Label3: TLabel;
    cxButton2: TcxButton;
    Bevel1: TBevel;
    Label4: TLabel;
    Label5: TLabel;
    procedure cxButton2Click(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmLogin.cxButton2Click(Sender: TObject);
begin
    Application.Terminate;
end;

procedure TfrmLogin.FormKeyPress(Sender: TObject; var Key: Char);
begin

  if key = #13 then
  begin

    key := #0;
    perform (WM_NextDlgCtl, 0, 0);

  end;

end;

procedure TfrmLogin.cxButton1Click(Sender: TObject);
begin

    cxButton1.Caption := 'Conectando...' ;

    try
      if (frmMenu.ValidaLogin(edtServidor.Text,edtLogin.Text,edtSenha.Text)) then
        close ;
    except
      cxButton1.Caption := 'Logon' ;
      raise ;
    end;

    cxButton1.Caption := 'Logon' ;
    edtLogin.SetFocus;

end;

procedure TfrmLogin.FormShow(Sender: TObject);
begin
    edtServidor.Text := frmMenu.cIPServidor;
    edtLogin.SetFocus;
end;

end.
