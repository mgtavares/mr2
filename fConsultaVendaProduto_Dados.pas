unit fConsultaVendaProduto_Dados;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, ADODB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmConsultaVendaProduto_Dados = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    qryPrestacoes: TADOQuery;
    dsPrestacoes: TDataSource;
    qryCondicao: TADOQuery;
    dsCondicao: TDataSource;
    qryItemPedido: TADOQuery;
    dsItemPedido: TDataSource;
    qryPrestacoesnCdTitulo: TIntegerField;
    qryPrestacoescNmTerceiro: TStringField;
    qryPrestacoesnCdCrediario: TIntegerField;
    qryPrestacoesiParcela: TIntegerField;
    qryPrestacoesdDtVenc: TDateTimeField;
    qryPrestacoesdDtLiq: TDateTimeField;
    qryPrestacoesnValTit: TBCDField;
    qryPrestacoesnValJuro: TBCDField;
    qryPrestacoesnValLiq: TBCDField;
    qryPrestacoesnSaldoTit: TBCDField;
    cxGridDBTableView2nCdTitulo: TcxGridDBColumn;
    cxGridDBTableView2cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView2nCdCrediario: TcxGridDBColumn;
    cxGridDBTableView2iParcela: TcxGridDBColumn;
    cxGridDBTableView2dDtVenc: TcxGridDBColumn;
    cxGridDBTableView2dDtLiq: TcxGridDBColumn;
    cxGridDBTableView2nValTit: TcxGridDBColumn;
    cxGridDBTableView2nValJuro: TcxGridDBColumn;
    cxGridDBTableView2nValLiq: TcxGridDBColumn;
    cxGridDBTableView2nSaldoTit: TcxGridDBColumn;
    qryCondicaonCdFormaPagto: TIntegerField;
    qryCondicaocNmFormaPagto: TStringField;
    qryCondicaonCdCondPagto: TIntegerField;
    qryCondicaocNmCondPagto: TStringField;
    qryCondicaonValPagto: TBCDField;
    qryCondicaonValDescontoCondPagto: TBCDField;
    cxGridDBTableView1nCdFormaPagto: TcxGridDBColumn;
    cxGridDBTableView1cNmFormaPagto: TcxGridDBColumn;
    cxGridDBTableView1nCdCondPagto: TcxGridDBColumn;
    cxGridDBTableView1cNmCondPagto: TcxGridDBColumn;
    cxGridDBTableView1nValPagto: TcxGridDBColumn;
    cxGridDBTableView1nValDescontoCondPagto: TcxGridDBColumn;
    qryItemPedidonCdPedido: TIntegerField;
    qryItemPedidonCdItemPedido: TAutoIncField;
    qryItemPedidonCdProduto: TIntegerField;
    qryItemPedidocNmItem: TStringField;
    qryItemPedidonQtdeExpRec: TBCDField;
    qryItemPedidonValUnitario: TBCDField;
    qryItemPedidonValDesconto: TBCDField;
    qryItemPedidonValAcrescimo: TBCDField;
    qryItemPedidonValTotal: TBCDField;
    qryItemPedidocFlgTrocado: TStringField;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1nCdItemPedido: TcxGridDBColumn;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmItem: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeExpRec: TcxGridDBColumn;
    cxGrid1DBTableView1nValUnitario: TcxGridDBColumn;
    cxGrid1DBTableView1nValDesconto: TcxGridDBColumn;
    cxGrid1DBTableView1nValAcrescimo: TcxGridDBColumn;
    cxGrid1DBTableView1nValTotal: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgTrocado: TcxGridDBColumn;
    GroupBox4: TGroupBox;
    qryCheques: TADOQuery;
    dsCheques: TDataSource;
    qryChequesnCdCheque: TAutoIncField;
    qryChequesnCdBanco: TIntegerField;
    qryChequescAgencia: TStringField;
    qryChequescConta: TStringField;
    qryChequesiNrCheque: TIntegerField;
    qryChequesnValCheque: TBCDField;
    qryChequesdDtDeposito: TDateTimeField;
    qryChequesdDtDevol: TDateTimeField;
    qryChequesdDtSegDevol: TDateTimeField;
    qryChequescNmTabStatusCheque: TStringField;
    cxGrid4: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    cxGridDBTableView3nCdCheque: TcxGridDBColumn;
    cxGridDBTableView3nCdBanco: TcxGridDBColumn;
    cxGridDBTableView3cAgencia: TcxGridDBColumn;
    cxGridDBTableView3cConta: TcxGridDBColumn;
    cxGridDBTableView3iNrCheque: TcxGridDBColumn;
    cxGridDBTableView3nValCheque: TcxGridDBColumn;
    cxGridDBTableView3dDtDeposito: TcxGridDBColumn;
    cxGridDBTableView3dDtDevol: TcxGridDBColumn;
    cxGridDBTableView3dDtSegDevol: TcxGridDBColumn;
    cxGridDBTableView3cNmTabStatusCheque: TcxGridDBColumn;
    qryItemPedidonCdUsuario: TIntegerField;
    qryItemPedidocNmUsuario: TStringField;
    cxGrid1DBTableView1nCdUsuario: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    qryItemPedidonPercDescontoItem: TBCDField;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    procedure cxGridDBTableView2DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaVendaProduto_Dados: TfrmConsultaVendaProduto_Dados;

implementation

uses fHistMov_HistPagto;

{$R *.dfm}

procedure TfrmConsultaVendaProduto_Dados.cxGridDBTableView2DblClick(
  Sender: TObject);
var
    objHistMov_HistPagto : TfrmHistMov_HistPagto;
begin
    inherited;
    {Cria o Objeto, passa a PK do titulo e abre a tela}
    objHistMov_HistPagto := TfrmHistMov_HistPagto.Create(self);
    PosicionaQuery(objHistMov_HistPagto.qryInfPag,qryPrestacoesnCdTitulo.AsString);

    {Testa se a query est� vazia, s� abre janela caso n�o esteja }
    if not(objHistMov_HistPagto.qryInfPag.IsEmpty) then
        showForm(objHistMov_HistPagto,true)
    else
        freeandnil( objHistMov_HistPagto );
end;

end.
