unit fGrupoEconomico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh;

type
  TfrmGrupoEconomico = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoEconomico: TIntegerField;
    qryMastercNmGrupoEconomico: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoEconomico: TfrmGrupoEconomico;

implementation

{$R *.dfm}

procedure TfrmGrupoEconomico.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOECONOMICO' ;
  nCdTabelaSistema  := 44 ;
  nCdConsultaPadrao := 94 ;
end;

procedure TfrmGrupoEconomico.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

initialization
    RegisterClass(TfrmGrupoEconomico) ;
    
end.
