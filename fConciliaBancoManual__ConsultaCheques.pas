unit fConciliaBancoManual__ConsultaCheques;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping;

type
  TfrmConciliaBancoManual__ConsultaCheques = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryCheques: TADOQuery;
    dsCheques: TDataSource;
    qryChequesnCdCheque: TAutoIncField;
    qryChequesnCdBanco: TIntegerField;
    qryChequesiNrCheque: TIntegerField;
    qryChequesnValCheque: TBCDField;
    qryChequescCNPJCPF: TStringField;
    qryChequescNmTerceiro: TStringField;
    qryChequesdDtDeposito: TDateTimeField;
    qryChequesdDtRemessaPort: TDateTimeField;
    function ExibeCheques() : integer ;
    procedure DBGridEh1KeyPress(Sender: TObject; var Key: Char);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConciliaBancoManual__ConsultaCheques: TfrmConciliaBancoManual__ConsultaCheques;

implementation

{$R *.dfm}

function TfrmConciliaBancoManual__ConsultaCheques.ExibeCheques() : integer ;
begin
    Self.ShowModal ;

    if not qryCheques.eof then
    begin
        Result := qryChequesnCdCheque.Value ;
    end
    else Result := 0 ;
end ;

procedure TfrmConciliaBancoManual__ConsultaCheques.DBGridEh1KeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if (key = #9) then
  begin

      case MessageDlg('Confirma o cheque selecionado ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      close ;

  end ;

end;

procedure TfrmConciliaBancoManual__ConsultaCheques.ToolButton2Click(
  Sender: TObject);
begin
  qryCheques.Close ;
  inherited;

end;

end.
