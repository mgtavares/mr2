unit fLibFinRec_Compl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmLibFinRec_Compl = class(TfrmProcesso_Padrao)
    qryRecebimento: TADOQuery;
    qryParcelas: TADOQuery;
    qryPedido: TADOQuery;
    qryPedidonPercDescontoVencto: TBCDField;
    qryPedidonPercAcrescimoVendor: TBCDField;
    dsRecebimento: TDataSource;
    dsParcela: TDataSource;
    qryParcelasnCdRecebimento: TIntegerField;
    qryParcelasdDtVenc: TDateTimeField;
    qryParcelasnValPagto: TBCDField;
    qryParcelasnCdTitulo: TIntegerField;
    qryRecebimentonCdRecebimento: TIntegerField;
    qryRecebimentonCdEmpresa: TIntegerField;
    qryRecebimentonCdLoja: TIntegerField;
    qryRecebimentonCdTipoReceb: TIntegerField;
    qryRecebimentodDtReceb: TDateTimeField;
    qryRecebimentonCdTerceiro: TIntegerField;
    qryRecebimentocNrDocto: TStringField;
    qryRecebimentocSerieDocto: TStringField;
    qryRecebimentodDtDocto: TDateTimeField;
    qryRecebimentonValDocto: TBCDField;
    qryRecebimentonCdTabStatusReceb: TIntegerField;
    qryRecebimentonCdUsuarioFech: TIntegerField;
    qryRecebimentodDtFech: TDateTimeField;
    qryRecebimentocOBS: TStringField;
    qryRecebimentonValTotalNF: TBCDField;
    qryRecebimentonValTitulo: TBCDField;
    qryRecebimentonPercDescontoVencto: TBCDField;
    qryRecebimentonPercAcrescimoVendor: TBCDField;
    qryAux: TADOQuery;
    usp_processo: TADOStoredProc;
    qryRecebimentonValTotalItensPag: TBCDField;
    qryRecebimentonValDescontoNF: TBCDField;
    qryParcelascNrTit: TStringField;
    qryParcelasiParcela: TIntegerField;
    qryParcelascFlgDocCobranca: TIntegerField;
    qryParcelasnValParcela: TBCDField;
    qryParcelasnValOperacaoFin: TBCDField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    SP_GERA_PARCELA_RECEBIMENTO: TADOStoredProc;
    qryParcelascCodBarra: TStringField;
    qryTitulosAdiantamento: TADOQuery;
    qryTitulosAdiantamentonValAdiantamento: TBCDField;
    dsTitulosAdiantamento: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cxPageControl2: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    Memo1: TMemo;
    cxButton6: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure DBEdit4Enter(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure qryParcelasBeforePost(DataSet: TDataSet);
    procedure DBEdit6Exit(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdRecebimento : Integer ;
  end;

var
  frmLibFinRec_Compl: TfrmLibFinRec_Compl;

implementation

uses fMenu, fProvisaoPagto_TitulosAdiantamento;

{$R *.dfm}

procedure TfrmLibFinRec_Compl.FormShow(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryParcelas, IntToStr(nCdRecebimento)) ;
  PosicionaQuery(qryRecebimento, IntToStr(nCdRecebimento)) ;
  PosicionaQuery(qryPedido, IntToStr(nCdRecebimento)) ;
  PosicionaQuery(qryTitulosAdiantamento,qryRecebimentonCdTerceiro.AsString);

  if not qryPedido.eof then
  begin
      qryRecebimento.Edit ;
          if (qryRecebimentonPercDescontoVencto.Value = 0) then
              qryRecebimentonPercDescontoVencto.Value  := qryPedidonPercDescontoVencto.Value ;

          if (qryRecebimentonPercAcrescimoVendor.Value = 0) then
              qryRecebimentonPercAcrescimoVendor.Value := qryPedidonPercAcrescimoVendor.Value ;
      qryRecebimento.Post ;
  end ;

  Memo1.Lines.Clear;

  DBGridEh1.SetFocus ;

end;

procedure TfrmLibFinRec_Compl.DBEdit4Enter(Sender: TObject);
begin
  inherited;

  if (qryRecebimentonPercDescontoVencto.Value > 0) and (qryRecebimentonPercAcrescimoVendor.Value > 0) then
  begin
      MensagemAlerta('N�o � permitido utilizar desconto por atencipa��o de pagamento e juros de prazo no mesmo documento.') ;
      DbEdit1.SetFocus ;
  end ;

  if (qryRecebimento.State = dsBrowse) then
      qryRecebimento.Edit ;

  qryRecebimentonValTitulo.Value := (qryRecebimentonValTotalItensPag.Value - qryRecebimentonValDescontoNF.Value) ;

  if (qryRecebimentonPercDescontoVencto.Value > 0) then
  begin
      qryRecebimentonValTitulo.Value := (qryRecebimentonValTotalItensPag.Value - qryRecebimentonValDescontoNF.Value) * ((100-qryRecebimentonPercDescontoVencto.Value) / 100)
  end ;

  if (qryRecebimentonPercAcrescimoVendor.Value > 0) then
  begin
      qryRecebimentonValTitulo.Value := (qryRecebimentonValTotalItensPag.Value - qryRecebimentonValDescontoNF.Value) * (1+(qryRecebimentonPercAcrescimoVendor.Value / 100))
  end ;

  qryRecebimento.Post ;

end;

procedure TfrmLibFinRec_Compl.ToolButton1Click(Sender: TObject);
begin
  inherited;

  { -- verifica se parcelas possuem dt. vencimento v�lido -- }
  qryParcelas.First;

  while (not qryParcelas.Eof) do
  begin
      if (qryParcelasdDtVenc.Value < (StrToDate(DateToStr(Now() + StrToInt(frmMenu.LeParametro('DIAMINVENC')))))) then
      begin
          MensagemAlerta('Data inferior ao limite m�nimo para pagamento.');
          DBGridEh1.SelectedField := qryParcelasdDtVenc;
          DBGridEh1.SetFocus;
          Abort;
      end;

      qryParcelas.Next;
  end;

  if (qryTitulosAdiantamentonValAdiantamento.Value > 0) then
  begin
     MensagemAlerta('O Terceiro possui t�tulos de adiantamento em aberto. Esta mensagem � apenas para alerta.');
  end;

  if (qryRecebimento.State <> dsBrowse) then
      qryRecebimento.Post ;

  if (qryParcelas.State <> dsBrowse) then
      qryParcelas.Post ;

  case MessageDlg('Confirma o processamento deste recebimento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

      frmMenu.Connection.BeginTrans;

      try
          usp_processo.Close ;
          usp_processo.Parameters.ParamByName('@nCdRecebimento').Value := nCdRecebimento ;
          usp_processo.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
          usp_processo.Parameters.ParamByName('@cOBSTitulo').Value     := Memo1.Lines.Text;
          usp_processo.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Autoriza��o financeira processada com sucesso.');
      Close ;
end;

procedure TfrmLibFinRec_Compl.qryParcelasBeforePost(DataSet: TDataSet);
begin
  if qryParcelasdDtVenc.Value < (StrToDate(DateToStr(Now() + StrToInt(frmMenu.LeParametro('DIAMINVENC')))))  then
  begin
      MensagemAlerta('Data Inferior ao limite m�nimo para pagamento.') ;
      abort ;
  end ;

  if (qryParcelasnValPagto.Value <= 0) then
  begin
      MensagemAlerta('Valor da parcela inv�lido.') ;
      abort ;
  end ;

  if (qryParcelascFlgDocCobranca.Value = 1) and (qryParcelascNrTit.Value = '') then
  begin
      MensagemAlerta('Informe o n�mero do t�tulo a pagar.') ;
      abort ;
  end ;

  inherited;

  qryParcelasnCdRecebimento.Value := nCdRecebimento ;

end;

procedure TfrmLibFinRec_Compl.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  if (qryRecebimento.State = dsBrowse) then
      qryRecebimento.Edit ;

  qryRecebimentonValTitulo.Value := (qryRecebimentonValTotalItensPag.Value - qryRecebimentonValDescontoNF.Value) ;

  if (qryRecebimentonPercDescontoVencto.Value > 0) then
  begin
      qryRecebimentonValTitulo.Value := (qryRecebimentonValTotalItensPag.Value - qryRecebimentonValDescontoNF.Value) * ((100-qryRecebimentonPercDescontoVencto.Value) / 100)
  end ;

  if (qryRecebimentonPercAcrescimoVendor.Value > 0) then
  begin
      qryRecebimentonValTitulo.Value := (qryRecebimentonValTotalItensPag.Value - qryRecebimentonValDescontoNF.Value) * (1+(qryRecebimentonPercAcrescimoVendor.Value / 100))
  end ;

  qryRecebimento.Post ;

end;

procedure TfrmLibFinRec_Compl.ToolButton5Click(Sender: TObject);
begin
  inherited;

  if (qryRecebimento.Active) then
  begin

      case MessageDlg('Confirma o rec�lculo das parcelas ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo:exit ;
      end ;

      frmMenu.Connection.BeginTrans;

      try

          SP_GERA_PARCELA_RECEBIMENTO.Close ;
          SP_GERA_PARCELA_RECEBIMENTO.Parameters.ParamByName('@nCdRecebimento').Value := qryRecebimentonCdRecebimento.Value ;
          SP_GERA_PARCELA_RECEBIMENTO.ExecProc;

      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;
      PosicionaQuery(qryRecebimento, qryRecebimentonCdRecebimento.asString) ;
      PosicionaQuery(qryParcelas, qryRecebimentonCdRecebimento.AsString) ;

  end ;

end;

procedure TfrmLibFinRec_Compl.FormKeyDown(Sender: TObject; var Key: Word;
 Shift: TShiftState);
begin

  {if key = 13 then
  begin

    key := 0;
    perform (WM_NextDlgCtl, 0, 0);

  end;}

end;

procedure TfrmLibFinRec_Compl.cxButton6Click(Sender: TObject);
var
  objForm : TfrmProvisaoPagtoTituloAdiantamento ;
begin
  inherited;

  objForm := TfrmProvisaoPagtoTituloAdiantamento.Create(nil);

  PosicionaQuery(objForm.qryTitulosAdiantamento,qryRecebimentonCdTerceiro.AsString);

  showForm( objForm , TRUE ) ;

end;

end.
