inherited frmAutorizaPropostaReneg_Titulos: TfrmAutorizaPropostaReneg_Titulos
  Left = 113
  Top = 183
  Width = 1197
  BorderIcons = [biSystemMenu]
  Caption = 'T'#237'tulos'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1181
  end
  inherited ToolBar1: TToolBar
    Width = 1181
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1181
    Height = 435
    Align = alClient
    DataSource = dsTitulos
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footers = <>
        Width = 57
      end
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footers = <>
        Width = 80
      end
      item
        EditButtons = <>
        FieldName = 'iParcela'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
        Width = 41
      end
      item
        EditButtons = <>
        FieldName = 'cNmEspTit'
        Footers = <>
        Width = 154
      end
      item
        EditButtons = <>
        FieldName = 'dDtEmissao'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iDiasAtraso'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWhite
        Footer.Font.Height = -11
        Footer.Font.Name = 'Tahoma'
        Footer.Font.Style = []
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValTit'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValLiq'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValJuro'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValMulta'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValDesconto'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoTit'
        Footers = <>
      end>
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'dDtBase'
        Size = -1
        Value = Null
      end
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @dDtBase DATETIME'
      ''
      'Set @dDtBase = Convert(DATETIME,:dDtBase,103)'
      ''
      'SELECT Titulo.nCdTitulo'
      '      ,cNrTit'
      '      ,iParcela'
      '      ,cNmEspTit'
      '      ,dDtEmissao'
      '      ,dDtVenc'
      
        '      ,CASE WHEN dDtVenc < dbo.fn_OnlyDate(GetDate()) THEN Conve' +
        'rt(int,dbo.fn_OnlyDate(GetDate())-dDtVenc)'
      '            ELSE 0'
      '       END  iDiasAtraso'
      '      ,nValTit'
      '      ,nValLiq'
      
        '      ,dbo.fn_SimulaJurosTitulo(Titulo.nCdTitulo,@dDtBase)  nVal' +
        'Juro'
      
        '      ,dbo.fn_SimulaMultaTitulo(Titulo.nCdTitulo)           nVal' +
        'Multa'
      '      ,nValDesconto'
      '      ,TituloPropostaReneg.nSaldoTit +'
      '       dbo.fn_SimulaJurosTitulo(Titulo.nCdTitulo,@dDtBase) +'
      '       dbo.fn_SimulaMultaTitulo(Titulo.nCdTitulo) as nSaldoTit'
      ' FROM TituloPropostaReneg'
      
        '      INNER JOIN Titulo ON Titulo.nCdTitulo = TituloPropostaRene' +
        'g.nCdTitulo'
      '      INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEspTit'
      'WHERE nCdPropostaReneg = :nPK'
      'ORDER BY dDtVenc')
    Left = 376
    Top = 168
    object qryTitulosnCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|C'#243'd'
      FieldName = 'nCdTitulo'
    end
    object qryTituloscNrTit: TStringField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosiParcela: TIntegerField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|Parc.'
      FieldName = 'iParcela'
    end
    object qryTituloscNmEspTit: TStringField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|Esp'#233'cie'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTitulosdDtEmissao: TDateTimeField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|Dt. Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryTitulosdDtVenc: TDateTimeField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|Dt. Vencto'
      FieldName = 'dDtVenc'
    end
    object qryTitulosiDiasAtraso: TIntegerField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|Atraso'
      FieldName = 'iDiasAtraso'
      ReadOnly = True
    end
    object qryTitulosnValTit: TBCDField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|Valor Original'
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosnValLiq: TBCDField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|Valor Pago'
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryTitulosnValJuro: TBCDField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|Valor Juros'
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTitulosnValMulta: TBCDField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|Valor Multa'
      FieldName = 'nValMulta'
      Precision = 12
      Size = 2
    end
    object qryTitulosnValDesconto: TBCDField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|Valor Desconto'
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryTitulosnSaldoTit: TBCDField
      DisplayLabel = 'T'#237'tulos Vinculados nesta Proposta|Saldo Pagar'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 416
    Top = 168
  end
end
