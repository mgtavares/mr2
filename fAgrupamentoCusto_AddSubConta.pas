unit fAgrupamentoCusto_AddSubConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, ER2Lookup, DB, DBCtrls, ADODB;

type
  TfrmAgrupamentoCusto_AddSubConta = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    edtMascaraSuperior: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    edtMascara: TEdit;
    Label3: TLabel;
    edtDescricao: TEdit;
    Label4: TLabel;
    Label6: TLabel;
    edtNivel: TEdit;
    qryContaPai: TADOQuery;
    edtCodigo: TMaskEdit;
    qryAgrupamentoCusto: TADOQuery;
    qryContaAgrupamentoCusto: TADOQuery;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryAux: TADOQuery;
    qryContaPainCdContaAgrupamentoCusto: TIntegerField;
    qryContaPainCdContaAgrupamentoCustoPai: TIntegerField;
    qryContaPainCdAgrupamentoCusto: TIntegerField;
    qryContaPaicMascara: TStringField;
    qryContaPaicNmContaAgrupamentoCusto: TStringField;
    qryContaPaiiNivel: TIntegerField;
    qryContaPaiiSequencia: TIntegerField;
    qryContaPainCdCC: TIntegerField;
    qryContaPaicFlgAnalSintetica: TStringField;
    qryAgrupamentoCustonCdAgrupamentoCusto: TIntegerField;
    qryAgrupamentoCustonCdEmpresa: TIntegerField;
    qryAgrupamentoCustocNmAgrupamentoCusto: TStringField;
    qryAgrupamentoCustocMascara: TStringField;
    qryAgrupamentoCustonCdTabTipoAgrupamentoContabil: TIntegerField;
    qryAgrupamentoCustonCdStatus: TIntegerField;
    qryAgrupamentoCustoiMaxNiveis: TIntegerField;
    qryContaAgrupamentoCustonCdContaAgrupamentoCusto: TIntegerField;
    qryContaAgrupamentoCustonCdContaAgrupamentoCustoPai: TIntegerField;
    qryContaAgrupamentoCustonCdAgrupamentoCusto: TIntegerField;
    qryContaAgrupamentoCustocMascara: TStringField;
    qryContaAgrupamentoCustocNmContaAgrupamentoCusto: TStringField;
    qryContaAgrupamentoCustoiNivel: TIntegerField;
    qryContaAgrupamentoCustoiSequencia: TIntegerField;
    qryContaAgrupamentoCustonCdCC: TIntegerField;
    qryContaAgrupamentoCustocFlgAnalSintetica: TStringField;
    procedure limpaCampos;
    procedure adicionaSubConta( nCdAgrupamentoCusto , nCdContaAgrupamentoCustoPai : integer ; bInclusao : boolean) ;
    procedure FormShow(Sender: TObject);
    procedure edtCodigoChange(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    function fnRemoveMascara( cString : string ) : string;
  private
    { Private declarations }
    bInsert : boolean ;
    nCdContaAgrupamentoCustoPai__ : integer ;
    nCdAgrupamentoCusto__ : integer ;
  public
    { Public declarations }
    cFormatoMascara : string ;
  end;

var
  frmAgrupamentoCusto_AddSubConta: TfrmAgrupamentoCusto_AddSubConta;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmAgrupamentoCusto_AddSubConta }

procedure TfrmAgrupamentoCusto_AddSubConta.adicionaSubConta( nCdAgrupamentoCusto , nCdContaAgrupamentoCustoPai : integer ; bInclusao : boolean) ;
var
  iNivelCorrente, i : integer ;
  cMascaraFormat    : string ;
begin

    bInsert := bInclusao ;

    if (bInsert) then
        ativaMaskEdit( edtCodigo )
    else desativaMaskEdit( edtCodigo ) ;
    
    nCdContaAgrupamentoCustoPai__ := nCdContaAgrupamentoCustoPai ;
    nCdAgrupamentoCusto__         := nCdAgrupamentoCusto ;

    qryAgrupamentoCusto.Close;
    PosicionaQuery( qryAgrupamentoCusto , intToStr( nCdAgrupamentoCusto ) ) ;

    {-- se for a inclus�o de uma subconta, localiza as informa��es da conta pai/superior e posiciona na tela --}
    if ( bInsert ) then
    begin

        qryContaPai.Close;
        PosicionaQuery( qryContaPai , intToStr( nCdContaAgrupamentoCustoPai ) ) ;

        edtNivel.Text := '1' ;

        if not qryContaPai.IsEmpty then
        begin

            if (qryContaPaicFlgAnalSintetica.Value = 'A') then
            begin
                MensagemAlerta('N�o � poss�vel adicionar uma subconta em uma conta anal�tica.') ;
                close;
                abort;
            end ;

            edtMascaraSuperior.Text  := frmMenu.fnAplicaMascaraContabil( qryContaPaicMascara.Value , cFormatoMascara ) ;
            edtNivel.Text            := intToStr( qryContaPaiiNivel.Value + 1 ) ;

            if not qryAgrupamentoCusto.IsEmpty then
            begin

                if (strToInt( edtNivel.Text ) > qryAgrupamentoCustoiMaxNiveis.Value ) then
                begin

                    MensagemAlerta('Limite de n�veis suportados na m�scara do agrupamento de custos excedido.') ;
                    close;
                    abort;

                end ;

            end ;

        end ;

    end
    else
    begin

        qryContaAgrupamentoCusto.Close;
        PosicionaQuery( qryContaAgrupamentoCusto , intToStr( nCdContaAgrupamentoCustoPai ) ) ;

        if not qryContaAgrupamentoCusto.IsEmpty then
        begin

            qryContaPai.Close;
            PosicionaQuery( qryContaPai , qryContaAgrupamentoCustonCdContaAgrupamentoCustoPai.AsString ) ;

            if not qryContaPai.IsEmpty then
                edtMascaraSuperior.Text  := frmMenu.fnAplicaMascaraContabil( qryContaPaicMascara.Value , cFormatoMascara ) ;

            edtCodigo.Text             := qryContaAgrupamentoCustoiSequencia.asString;
            edtDescricao.Text          := qryContaAgrupamentoCustocNmContaAgrupamentoCusto.Value;
            edtNivel.Text              := qryContaAgrupamentoCustoiNivel.AsString;

        end ;

        if (qryContaAgrupamentoCustocFlgAnalSintetica.Value = 'A') then
        begin
            MensagemAlerta('N�o � poss�vel alterar uma conta anal�tica.') ;
            close;
            abort;
        end ;

    end ;

    {-- de acordo com o n�vel da subconta, gera a m�scara no campo c�digo --}
    cMascaraFormat := '' ;
    iNivelCorrente := 1 ;

    for i := 1 to length( qryAgrupamentoCustocMascara.Value ) do
    begin

        if ( Copy( qryAgrupamentoCustocMascara.Value , i , 1 ) = '.' ) then
        begin

            inc( iNivelCorrente ) ;

            if ( iNivelCorrente > strToInt( edtNivel.Text ) ) then
                break ;

        end
        else
        begin

            if ( iNivelCorrente = strToInt( edtNivel.Text ) ) then
                cMascaraFormat := cMascaraFormat + Copy( qryAgrupamentoCustocMascara.Value , i , 1 ) ;

        end ;

    end ;

    edtCodigo.EditMask := trim( cMascaraFormat ) + ';0;_'  ;

    if not bInsert then
        edtCodigo.Text := frmMenu.ZeroEsquerda(edtCodigo.Text,(length(edtCodigo.EditMask)-4)) ;

    if not Self.Showing then
        Self.ShowModal
    else
    begin
        if not edtCodigo.ReadOnly then
            edtCodigo.SetFocus
        else edtDescricao.SetFocus ;
    end ;

end;

procedure TfrmAgrupamentoCusto_AddSubConta.limpaCampos;
begin

    edtMascaraSuperior.Text    := '' ;
    edtCodigo.Text             := '' ;
    edtMascara.Text            := '' ;
    edtDescricao.Text          := '' ;
    edtNivel.Text              := '' ;

end;

procedure TfrmAgrupamentoCusto_AddSubConta.FormShow(Sender: TObject);
begin
  inherited;

  if not edtCodigo.ReadOnly then
      edtCodigo.SetFocus
  else edtDescricao.SetFocus ;

end;

procedure TfrmAgrupamentoCusto_AddSubConta.edtCodigoChange(
  Sender: TObject);
begin
  inherited;

  if (edtMascaraSuperior.Text <> '') then
      edtMascara.Text := edtMascaraSuperior.Text + '.' + edtCodigo.Text
  else edtMascara.Text := edtCodigo.Text ;
  
end;

procedure TfrmAgrupamentoCusto_AddSubConta.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if ( trim( edtCodigo.Text ) = '' ) then
  begin
      MensagemAlerta('Informe o c�digo da subconta.') ;
      edtCodigo.SetFocus;
      abort ;
  end ;

  if ( length( trim( edtCodigo.Text ) ) <> (length( edtCodigo.EditMask ) -4 ) ) then
  begin
      MensagemAlerta('C�digo da subconta incompleto. Preencha todos os d�gitos ou complete com zeros a esquerda.') ;
      edtCodigo.SetFocus;
      abort ;
  end ;

  if ( trim( edtDescricao.Text ) = '' ) then
  begin
      MensagemAlerta('Informe a descri��o da subconta.') ;
      edtDescricao.SetFocus;
      abort ;
  end ;

  if (bInsert) then
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM ContaAgrupamentoCusto WHERE nCdAgrupamentoCusto = ' + intToStr( nCdAgrupamentoCusto__ ) + ' AND cMascara = ' + Chr(39) +  fnRemoveMascara( Trim(edtMascara.Text) ) + Chr(39) ) ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin

          qryAux.Close;
          MensagemAlerta('J� existe uma conta com a m�scara ' + edtMascara.Text + ' neste agrupamento cont�bil. Verifique.') ;
          abort ;

      end ;

  end
  else
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM ContaAgrupamentoCusto WHERE nCdAgrupamentoCusto = ' + intToStr( nCdAgrupamentoCusto__ ) + ' AND nCdContaAgrupamentoCusto != ' + intToStr( nCdContaAgrupamentoCustoPai__ ) + ' AND cMascara = ' + Chr(39) +  fnRemoveMascara( Trim(edtMascara.Text) ) + Chr(39) ) ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin

          qryAux.Close;
          MensagemAlerta('J� existe uma conta com a m�scara ' + edtMascara.Text + ' neste agrupamento cont�bil. Verifique.') ;
          abort ;

      end ;

  end ;

  if ( MessageDlg('Confirma a inclus�o desta subconta ?',mtConfirmation,[mbYes,mbNo],0) = MRNO ) then
  begin
      edtCodigo.SetFocus;
      exit;
  end ;

  try

      if ( bInsert ) then
      begin
          qryContaAgrupamentoCusto.Close;
          PosicionaQuery( qryContaAgrupamentoCusto , '0' ) ;
          
          qryContaAgrupamentoCusto.Insert ;
          qryContaAgrupamentoCustonCdContaAgrupamentoCusto.Value := frmMenu.fnProximoCodigo('CONTAAGRUPAMENTOCUSTO') ;
          qryContaAgrupamentoCustonCdAgrupamentoCusto.Value      := nCdAgrupamentoCusto__ ;

          if ( nCdContaAgrupamentoCustoPai__ > 0 ) then
              qryContaAgrupamentoCustonCdContaAgrupamentoCustoPai.Value := nCdContaAgrupamentoCustoPai__ ;
      end
      else qryContaAgrupamentoCusto.Edit ;

      qryContaAgrupamentoCustocMascara.Value                 := fnRemoveMascara( edtMascara.Text );
      qryContaAgrupamentoCustocNmContaAgrupamentoCusto.Value := edtDescricao.Text ;
      qryContaAgrupamentoCustoiNivel.Value                   := strToInt( edtNivel.Text ) ;
      qryContaAgrupamentoCustoiSequencia.Value               := strToInt( edtCodigo.Text ) ;
      qryContaAgrupamentoCustocFlgAnalSintetica.Value        := 'S' ;
      qryContaAgrupamentoCusto.Post;

  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  if ( not bInsert ) then
      Close
  else
  begin
  
      limpaCampos;

      adicionaSubConta( nCdAgrupamentoCusto__
                      , nCdContaAgrupamentoCustoPai__
                      , TRUE ) ;

  end ;

end;

function TfrmAgrupamentoCusto_AddSubConta.fnRemoveMascara(
  cString: string): string;
var
  x: Integer;
begin

  result := '';

  for x := 1 to length( cString ) do
      if ( Copy( cString , x , 1 ) <> '.' ) then
          result := result + Copy( cString , x , 1 ) ;

end;

end.
