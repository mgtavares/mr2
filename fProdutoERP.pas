unit fProdutoERP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons, cxPC, cxControls, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxGridCustomView, cxGrid, ER2Lookup, Menus,
  ACBrBase, ACBrValidador;

type
  TfrmProdutoERP = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    dsDepartamento: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TAutoIncField;
    qryDepartamentocNmDepartamento: TStringField;
    qryDepartamentonCdStatus: TIntegerField;
    qryCategoria: TADOQuery;
    qryCategorianCdCategoria: TAutoIncField;
    qryCategorianCdDepartamento: TIntegerField;
    qryCategoriacNmCategoria: TStringField;
    qryCategorianCdStatus: TIntegerField;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DataSource1: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategorianCdCategoria: TIntegerField;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySubCategorianCdStatus: TIntegerField;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DataSource2: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentonCdSubCategoria: TIntegerField;
    qrySegmentocNmSegmento: TStringField;
    qrySegmentonCdStatus: TIntegerField;
    DBEdit9: TDBEdit;
    DataSource3: TDataSource;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TAutoIncField;
    qryMarcacNmMarca: TStringField;
    qryMarcanCdStatus: TIntegerField;
    Label3: TLabel;
    DBEdit10: TDBEdit;
    Label5: TLabel;
    DBEdit11: TDBEdit;
    Label8: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DataSource4: TDataSource;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhanCdMarca: TIntegerField;
    qryLinhacNmLinha: TStringField;
    qryLinhanCdStatus: TIntegerField;
    DBEdit14: TDBEdit;
    DataSource5: TDataSource;
    Label11: TLabel;
    DBEdit17: TDBEdit;
    DataSource6: TDataSource;
    DataSource7: TDataSource;
    Label12: TLabel;
    DBEdit20: TDBEdit;
    Label13: TLabel;
    DBEdit21: TDBEdit;
    qryClasse: TADOQuery;
    qryStatus: TADOQuery;
    qryClassenCdClasseProduto: TAutoIncField;
    qryClassecNmClasseProduto: TStringField;
    DBEdit22: TDBEdit;
    DataSource8: TDataSource;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    DBEdit23: TDBEdit;
    DataSource9: TDataSource;
    qryProdutoIntermediario: TADOQuery;
    dsProdutoIntermediario: TDataSource;
    qryMasternCdProduto: TIntegerField;
    qryMasternCdDepartamento: TIntegerField;
    qryMasternCdCategoria: TIntegerField;
    qryMasternCdSubCategoria: TIntegerField;
    qryMasternCdSegmento: TIntegerField;
    qryMasternCdMarca: TIntegerField;
    qryMasternCdLinha: TIntegerField;
    qryMastercReferencia: TStringField;
    qryMasternCdTabTipoProduto: TIntegerField;
    qryMasternCdProdutoPai: TIntegerField;
    qryMasternCdGrade: TIntegerField;
    qryMasternCdColecao: TIntegerField;
    qryMastercNmProduto: TStringField;
    qryMasternCdClasseProduto: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMasternCdCor: TIntegerField;
    qryMasternCdMaterial: TIntegerField;
    qryMasteriTamanho: TIntegerField;
    qryMasternQtdeEstoque: TBCDField;
    qryMastercEAN: TStringField;
    qryMastercEANFornec: TStringField;
    qryMasterdDtUltReceb: TDateTimeField;
    qryMasterdDtUltVenda: TDateTimeField;
    dsProdutoFinal: TDataSource;
    qryConsultaProd: TADOQuery;
    qryConsultaProdnCdProduto: TIntegerField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasterdDtCad: TDateTimeField;
    qryMasternCdUsuarioAtuPreco: TIntegerField;
    qryMasterdDtAtuPreco: TDateTimeField;
    qryMasternQtdeEstoqueSeg: TBCDField;
    qryMasternQtdeEstoqueMin: TBCDField;
    qryMastercFlgExigeInspecao: TIntegerField;
    qryMastercFlgControlaLote: TIntegerField;
    qryMasternQtdeEstoqueMax: TBCDField;
    qryMastercUnidadeMedida: TStringField;
    Label19: TLabel;
    DBEdit19: TDBEdit;
    qryMasternCdCC: TIntegerField;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcCdCC: TStringField;
    qryCCcNmCC: TStringField;
    DataSource10: TDataSource;
    cxPageControl1: TcxPageControl;
    tabSubItens: TcxTabSheet;
    tabDadosGerais: TcxTabSheet;
    cxButton1: TcxButton;
    btGerarGrade: TcxButton;
    DBEdit29: TDBEdit;
    DBEdit28: TDBEdit;
    Label20: TLabel;
    DBEdit25: TDBEdit;
    Label15: TLabel;
    qryMasternQtdeEstoqueEmpenhado: TBCDField;
    qryMasternConsumoMedDia: TBCDField;
    qryMasterdDtUltMRP: TDateTimeField;
    qryMasteriDiasEstoqueSeg: TIntegerField;
    qryMasteriDiasEstoqueRessup: TIntegerField;
    qryMasteriDiasEstoqueMax: TIntegerField;
    qryMasternCdTipoRessuprimento: TIntegerField;
    qryMasternCdPlanoConta: TIntegerField;
    Label26: TLabel;
    DBEdit34: TDBEdit;
    qryPlanoConta: TADOQuery;
    qryPlanoContanCdPlanoConta: TAutoIncField;
    qryPlanoContacNmPlanoConta: TStringField;
    DBEdit35: TDBEdit;
    DataSource11: TDataSource;
    qryTipoRessuprimento: TADOQuery;
    qryTipoRessuprimentonCdTipoRessuprimento: TIntegerField;
    qryTipoRessuprimentocNmTipoRessuprimento: TStringField;
    DataSource12: TDataSource;
    qryProdutoIntermediarionCdProduto: TIntegerField;
    qryProdutoIntermediarionQtdeEstoque: TBCDField;
    qryProdutoIntermediariocReferencia: TStringField;
    qryProdutoIntermediariodDtUltReceb: TDateTimeField;
    qryProdutoIntermediariodDtUltVenda: TDateTimeField;
    qryProdutoIntermediariocNmProduto: TStringField;
    qryProdutoIntermediarioiDiasEstoqueSeg: TIntegerField;
    qryProdutoIntermediarioiDiasEstoqueRessup: TIntegerField;
    qryProdutoIntermediarioiDiasEstoqueMax: TIntegerField;
    qryProdutoIntermediarionConsumoMedDia: TBCDField;
    qryProdutoIntermediarionQtdeEstoqueEmpenhado: TBCDField;
    qryProdutoIntermediarionTempEstoque: TIntegerField;
    qryMasteriDiaEntrega: TIntegerField;
    qryMasteriTipoEntrega: TIntegerField;
    tabDadosPlanejamento: TcxTabSheet;
    dsFormulaProduto: TDataSource;
    qryProdutoFormula: TADOQuery;
    qryProdutoFormulanCdProduto: TIntegerField;
    qryProdutoFormulacNmProduto: TStringField;
    qryMasternCdLocalEstoqueEntrega: TIntegerField;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DataSource13: TDataSource;
    qryMastercFlgProdVenda: TIntegerField;
    tabDimensoes: TcxTabSheet;
    Image5: TImage;
    qryMasternCdAlaProducao: TIntegerField;
    qryMasternPesoBruto: TBCDField;
    qryMasternPesoLiquido: TBCDField;
    qryMasternAltura: TBCDField;
    qryMasternLargura: TBCDField;
    qryMasternDimensao: TBCDField;
    qryMasternVolume: TBCDField;
    qryMastercUMVolume: TStringField;
    Label30: TLabel;
    DBEdit41: TDBEdit;
    Label31: TLabel;
    DBEdit42: TDBEdit;
    Label32: TLabel;
    DBEdit43: TDBEdit;
    Label33: TLabel;
    DBEdit44: TDBEdit;
    Label34: TLabel;
    DBEdit45: TDBEdit;
    Label35: TLabel;
    DBEdit46: TDBEdit;
    Label36: TLabel;
    DBEdit47: TDBEdit;
    qryMastercDescricaoTecnica: TMemoField;
    tabDescricaoTecnica: TcxTabSheet;
    DBMemo1: TDBMemo;
    qryMasternEspessura: TBCDField;
    qryMasternFatorPeso: TBCDField;
    Label39: TLabel;
    DBEdit49: TDBEdit;
    Label40: TLabel;
    DBEdit50: TDBEdit;
    qryMasternCdGrupo_Produto: TIntegerField;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DataSource14: TDataSource;
    qryDepartamentonCdGrupoProduto: TIntegerField;
    cxButton2: TcxButton;
    btPosicaoEstoqueSubItem: TcxButton;
    cxButton7: TcxButton;
    qryMasternCdGrupoInventario: TIntegerField;
    qryGrupoInventario: TADOQuery;
    qryGrupoInventarionCdGrupoInventario: TIntegerField;
    qryGrupoInventariocNmGrupoInventario: TStringField;
    DataSource15: TDataSource;
    qryProdutoIntermediariocEAN: TStringField;
    qryMastercUnidadeMedidaCompra: TStringField;
    qryMasternFatorCompra: TBCDField;
    qryMasternQtdeMinimaCompra: TBCDField;
    cxButton9: TcxButton;
    qryMasternCdTipoPlanejamento: TIntegerField;
    qryMasternCdTipoObtencao: TIntegerField;
    qryMasternLoteMultiplo: TBCDField;
    qryMasteriLLC: TIntegerField;
    qryMastercFlgFantasma: TIntegerField;
    Image3: TImage;
    Label10: TLabel;
    Label14: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label23: TLabel;
    DBEdit18: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    Image2: TImage;
    Label24: TLabel;
    DBEdit33: TDBEdit;
    Label25: TLabel;
    DBEdit38: TDBEdit;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1cReferencia: TcxGridDBColumn;
    cxGrid1DBTableView1dDtUltReceb: TcxGridDBColumn;
    cxGrid1DBTableView1dDtUltVenda: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeEstoqueEmpenhado: TcxGridDBColumn;
    cxGrid1DBTableView1cEAN: TcxGridDBColumn;
    cxGrid1DBTableView1nValCusto: TcxGridDBColumn;
    cxGrid1DBTableView1nValVenda: TcxGridDBColumn;
    qryProdutoIntermediarionCdStatus: TIntegerField;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    qryMasternCdGrupoMRP: TIntegerField;
    qryMasteriLeadTimeInsumos: TIntegerField;
    Label28: TLabel;
    DBEdit39: TDBEdit;
    qryGrupoMRP: TADOQuery;
    qryGrupoMRPnCdGrupoMRP: TIntegerField;
    qryGrupoMRPcNmGrupoMRP: TStringField;
    DBEdit40: TDBEdit;
    DataSource16: TDataSource;
    qryMasternValCusto: TFloatField;
    qryMasternValVenda: TFloatField;
    qryProdutoIntermediarionValCusto: TFloatField;
    qryProdutoIntermediarionValVenda: TFloatField;
    comboMetodoPlanejamento: TDBLookupComboBox;
    qryTipoPlanejamento: TADOQuery;
    qryTipoPlanejamentonCdTipoPlanejamento: TIntegerField;
    qryTipoPlanejamentocNmTipoPlanejamento: TStringField;
    dsTipoPlanejamento: TDataSource;
    Label42: TLabel;
    qryTipoObtencao: TADOQuery;
    qryTipoObtencaonCdTipoObtencao: TIntegerField;
    qryTipoObtencaocNmTipoObtencao: TStringField;
    dsTipoObtencao: TDataSource;
    comboMetodoObtencao: TDBLookupComboBox;
    Label43: TLabel;
    dsTipoRessuprimento: TDataSource;
    comboTipoRessuprimento: TDBLookupComboBox;
    Label44: TLabel;
    qryMasternCdTabTipoModoGestaoProduto: TIntegerField;
    qryMastercFlgProdEstoque: TIntegerField;
    qryMastercFlgProdCompra: TIntegerField;
    qryMastercFlgAtivoFixo: TIntegerField;
    GroupBox1: TGroupBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    comboModoGestao: TDBLookupComboBox;
    Label45: TLabel;
    qryModoGestao: TADOQuery;
    qryModoGestaonCdTabTipoModoGestaoProduto: TIntegerField;
    qryModoGestaocNmTabTipoModoGestaoProduto: TStringField;
    dsModoGestao: TDataSource;
    tabDadosEstoque: TcxTabSheet;
    Image4: TImage;
    qryMetodoUso: TADOQuery;
    dsMetodoUso: TDataSource;
    qryMetodoUsonCdTabTipoMetodoUso: TIntegerField;
    qryMetodoUsocNmTabTipoMetodoUso: TStringField;
    comboMetodoUso: TDBLookupComboBox;
    Label27: TLabel;
    qryMasternCdTabTipoMetodoUso: TIntegerField;
    DBCheckBox4: TDBCheckBox;
    Label46: TLabel;
    comboMetodoBaixaEstoque: TDBLookupComboBox;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMastercIDExterno: TStringField;
    qryMastercNmCorAntiga: TStringField;
    qryMasternCdTabTipoEmpenhoProduto: TIntegerField;
    qryMasteriDiasValidadeLote: TIntegerField;
    qryMasternCdTabTipoMetodoSaidaEstoque: TIntegerField;
    qryTabTipoMetodoSaidaEstoque: TADOQuery;
    dsTabTipoMetodoSaidaEstoque: TDataSource;
    qryTabTipoMetodoSaidaEstoquenCdTabTipoMetodoSaidaEstoque: TIntegerField;
    qryTabTipoMetodoSaidaEstoquecNmTabTipoMetodoSaidaEstoque: TStringField;
    qryMasternCdTabTipoModeloProducao: TIntegerField;
    comboModeloProducao: TDBLookupComboBox;
    Label47: TLabel;
    qryModeloProducao: TADOQuery;
    dsModeloProducao: TDataSource;
    qryModeloProducaonCdTabTipoModeloProducao: TIntegerField;
    qryModeloProducaocNmTabTipoModeloProducao: TStringField;
    qryMasternCdTabTipoApropriacao: TIntegerField;
    qryTipoApropriacao: TADOQuery;
    dsTipoApropriacao: TDataSource;
    qryTipoApropriacaonCdTabTipoApropriacao: TIntegerField;
    qryTipoApropriacaocNmTabTipoApropriacao: TStringField;
    comboTipoApropriacao: TDBLookupComboBox;
    Label48: TLabel;
    qryTabTipoEmpenhoProduto: TADOQuery;
    dsTabTipoEmpenhoProduto: TDataSource;
    qryTabTipoEmpenhoProdutonCdTabTipoEmpenhoProduto: TIntegerField;
    qryTabTipoEmpenhoProdutocNmTabTipoEmpenhoProduto: TStringField;
    comboTipoEmpenho: TDBLookupComboBox;
    Label49: TLabel;
    qryMasternCdLocalEstoquePadrao: TIntegerField;
    Label50: TLabel;
    edtLocalEstoque: TER2LookupDBEdit;
    qryLocalEstoquePadrao: TADOQuery;
    qryLocalEstoquePadraocNmLocalEstoque: TStringField;
    DBEdit36: TDBEdit;
    DataSource17: TDataSource;
    Label51: TLabel;
    DBEdit37: TDBEdit;
    Label52: TLabel;
    qryMastercFlgControlaValidadeLote: TIntegerField;
    DBCheckBox6: TDBCheckBox;
    qryMasteriDiasDisponibilidadeLote: TIntegerField;
    Label53: TLabel;
    DBEdit54: TDBEdit;
    Label54: TLabel;
    cxButton8: TcxButton;
    cxButton10: TcxButton;
    cxButton6: TcxButton;
    cxButton3: TcxButton;
    cxButton11: TcxButton;
    qryPosicaoEstoque: TADOQuery;
    qryPosicaoEstoquenCdEmpresa: TIntegerField;
    qryPosicaoEstoquecNmEmpresa: TStringField;
    qryPosicaoEstoquenCdLoja: TIntegerField;
    qryPosicaoEstoquecNmLoja: TStringField;
    qryPosicaoEstoquenCdLocalEstoque: TIntegerField;
    qryPosicaoEstoquecNmLocalEstoque: TStringField;
    qryPosicaoEstoquenQtdeDisponivel: TBCDField;
    qryPosicaoEstoquenQtdeTransito: TBCDField;
    qryPosicaoEstoquedDtUltInventario: TDateTimeField;
    qryPosicaoEstoquenCdProduto: TIntegerField;
    qryPosicaoEstoquecNmProduto: TStringField;
    qryPosicaoEstoquecDisponivel: TStringField;
    qryPosicaoEstoquecTerceiro: TStringField;
    qryPosicaoEstoquedDtUltRecebEstoque: TDateTimeField;
    qryPosicaoEstoquedDtUltVendaEstoque: TDateTimeField;
    dsPosicaoEstoque: TDataSource;
    cxPageControl2: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEmpresa: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLoja: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLoja: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLocalEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1cNmLocalEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeDisponivel: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeTransito: TcxGridDBColumn;
    cxGrid1DBTableView1dDtUltRecebEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1dDtUltVendaEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1dDtUltInventario: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn2: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    qryPosicaoEstoquenQtdeEmpenhado: TBCDField;
    cxGridDBTableView1nQtdeEmpenhado: TcxGridDBColumn;
    qryPosicaoEstoquenQtdeDisponivelReal: TBCDField;
    cxGridDBTableView1nQtdeDisponivelReal: TcxGridDBColumn;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocFlgExpPDV: TIntegerField;
    qryGrupoProdutonCdServidorOrigem: TIntegerField;
    qryGrupoProdutodDtReplicacao: TDateTimeField;
    qryGrupoProdutonCdGrupoMRPGP: TIntegerField;
    qryGrupoProdutonCdGrupoInventarioGP: TIntegerField;
    qryGrupoProdutonCdTipoPlanejamentoGP: TIntegerField;
    qryGrupoProdutonCdTipoObtencaoGP: TIntegerField;
    qryGrupoProdutoiLeadTimeGP: TIntegerField;
    qryGrupoProdutonLoteMinimoGP: TBCDField;
    qryGrupoProdutonLoteMultiploGP: TBCDField;
    qryGrupoProdutocFlgAlertarInventarioGP: TIntegerField;
    qryGrupoProdutocFlgProdVendaGP: TIntegerField;
    qryGrupoProdutocFlgProdEstoqueGP: TIntegerField;
    qryGrupoProdutocFlgProdCompraGP: TIntegerField;
    qryGrupoProdutocFlgAtivoFixoGP: TIntegerField;
    DBEdit51: TDBEdit;
    Label41: TLabel;
    DBEdit52: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit15: TDBEdit;
    Label9: TLabel;
    DBEdit30: TDBEdit;
    Label21: TLabel;
    qryMastercCdFabricante: TStringField;
    Label29: TLabel;
    DBEdit53: TDBEdit;
    qryProdutoIntermediariocCdFabricante: TStringField;
    cxGrid1DBTableView1cCdFabricante: TcxGridDBColumn;
    qryTabTipoCodProdFat: TADOQuery;
    dsTabTipoCodProdFat: TDataSource;
    qryTabTipoCodProdFatnCdTabTipoCodProdFat: TIntegerField;
    qryTabTipoCodProdFatcNmTabTipoCodProdFat: TStringField;
    comboTipoCodFat: TDBLookupComboBox;
    Label55: TLabel;
    Label56: TLabel;
    qryMasternCdTabTipoCodProdFat: TIntegerField;
    DBEdit48: TDBEdit;
    Label37: TLabel;
    Label38: TLabel;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    popupMenuMais: TPopupMenu;
    GrupodeProdutos1: TMenuItem;
    DepartamentoCategoriaSubCategoria1: TMenuItem;
    Marca1: TMenuItem;
    ClassedeProdutos1: TMenuItem;
    GrupodeInventrio1: TMenuItem;
    GrupodeMRP1: TMenuItem;
    LocaldeEstoque1: TMenuItem;
    OperaoManualnoEstoque1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    ConsultaMovimentaoEstoque1: TMenuItem;
    qryMasternPercTolRecebMenorPedido: TFloatField;
    qryMasternPercTolRecebMaiorPedido: TFloatField;
    qryMastercFlgTolRecebPedIlimitada: TIntegerField;
    Label22: TLabel;
    DBEdit55: TDBEdit;
    DBEdit56: TDBEdit;
    DBCheckBox7: TDBCheckBox;
    Label58: TLabel;
    Label57: TLabel;
    Label59: TLabel;
    ACBrValidador: TACBrValidador;
    qryMastercFCI: TStringField;
    Label60: TLabel;
    DBEdit57: TDBEdit;
    qryProdutoIntermediariocFCI: TStringField;
    cxGrid1DBTableView1cFCI: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit10Exit(Sender: TObject);
    procedure DBEdit11Exit(Sender: TObject);
    procedure DBEdit20Exit(Sender: TObject);
    procedure DBEdit21Exit(Sender: TObject);
    procedure DBEdit17Enter(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btGerarGradeClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit15KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit16KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit21KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit12Exit(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure cxButton1Click(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBEdit28KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit28Exit(Sender: TObject);
    procedure DBEdit34Exit(Sender: TObject);
    procedure DBEdit34KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit39KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh3Enter(Sender: TObject);
    procedure DBGridEh2Enter(Sender: TObject);
    procedure DBEdit38Exit(Sender: TObject);
    procedure DBEdit45Enter(Sender: TObject);
    procedure DBEdit41Enter(Sender: TObject);
    procedure DBEdit51KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit51Exit(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure btPosicaoEstoqueSubItemClick(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure DBEdit15Exit(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure cxButton9Click(Sender: TObject);
    procedure DBEdit39Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryMasternCdTipoObtencaoChange(Sender: TField);
    procedure qryMastercFlgProdEstoqueChange(Sender: TField);
    procedure DBEdit19Exit(Sender: TObject);
    procedure cxButton10Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure atribuiCaracteristicasGrupo(nCdGrupoProduto:integer);
    procedure qryMasternCdGrupo_ProdutoChange(Sender: TField);
    procedure popupMenuMaisPopup(Sender: TObject);
    procedure GrupodeProdutos1Click(Sender: TObject);
    procedure DepartamentoCategoriaSubCategoria1Click(Sender: TObject);
    procedure Marca1Click(Sender: TObject);
    procedure ClassedeProdutos1Click(Sender: TObject);
    procedure GrupodeInventrio1Click(Sender: TObject);
    procedure GrupodeMRP1Click(Sender: TObject);
    procedure LocaldeEstoque1Click(Sender: TObject);
    procedure OperaoManualnoEstoque1Click(Sender: TObject);
    procedure ConsultaMovimentaoEstoque1Click(Sender: TObject);
    procedure qryProdutoIntermediarioBeforePost(DataSet: TDataSet);
    procedure qryProdutoIntermediarioAfterInsert(DataSet: TDataSet);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProdutoERP: TfrmProdutoERP;

implementation

uses fGerarGrade, fLookup_Padrao, fIncLinha, fMenu, fNovaReferencia,
  fProdutoMRP, fProdutoPosicaoEstoque, fProdutoFornecedor,
  fProdutoERPInsumos, fProdutoERP_Identado,fProdutoERP_UltimasCompras,
  fDepartamento, fOperManEstoque, fGrupoProduto, fMarca, fClasseProduto,
  fGrupoInventario, fGrupoMRP, fLocalEstoque, fConsultaMovEstoque;

{$R *.dfm}

procedure TfrmProdutoERP.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster         := 'PRODUTO' ;
  nCdTabelaSistema        := 25 ;
  nCdConsultaPadrao       := 95 ;
  cWhereAddConsultaPadrao := 'nCdGrade IS NULL' ;
  bLimpaAposSalvar        := False ;
end;

procedure TfrmProdutoERP.DBEdit2Exit(Sender: TObject);
begin
  PosicionaQuery(qryDepartamento, DBEdit2.Text) ;

  if (qryDepartamento.Active) then
    qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TfrmProdutoERP.PosicionaQuery(oQuery : TADOQuery; cValor : String) ;
begin
    if (Trim(cValor) = '') then
        exit ;

    oQuery.Close ;
    oQuery.Parameters.ParamByName('nPK').Value := cValor ;
    oQuery.Open ;
end ;

procedure TfrmProdutoERP.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCategoria, DBEdit4.Text) ;

  if (qryCategoria.Active) then
      qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

end;

procedure TfrmProdutoERP.DBEdit6Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qrySubCategoria, DBEdit6.Text) ;

  if (qrySubCategoria.Active) then
  begin
    qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
    qryMarca.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  end;

end;

procedure TfrmProdutoERP.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qrySegmento, DBEdit7.Text) ;
end;

procedure TfrmProdutoERP.DBEdit10Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryMarca, DBEdit10.Text) ;

  if (qryMarca.Active) then
    qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

end;

procedure TfrmProdutoERP.DBEdit11Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryLinha, DBEdit11.Text) ;
end;

procedure TfrmProdutoERP.DBEdit20Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryClasse, DBEdit20.Text) ;

end;

procedure TfrmProdutoERP.DBEdit21Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryStatus, DBEdit21.Text) ;

end;

procedure TfrmProdutoERP.DBEdit17Enter(Sender: TObject);
begin
  inherited;

  If (DbEdit17.Text = '') and (qryMaster.state = dsInsert) then
  begin
      qryMastercNmProduto.Value := Trim(DBEdit13.Text) + ' ' + Trim(DBEdit12.Text) + ' ' + Trim(DBEdit14.Text) ;
  end ;
  
end;

procedure TfrmProdutoERP.btSalvarClick(Sender: TObject);
var
    bInclusao : Boolean ;
begin

  bInclusao := false ;

  if (qryMaster.State = dsInsert) then
  begin
      bInclusao := true ;
      qryMasternCdTabTipoProduto.Value := 1 ;
      qryMasternCdStatus.Value         := 1 ;
      qryMasterdDtCad.Value            := Now();
  end ;

  inherited;

  if (bInclusao) and (qryMasternCdProduto.Value > 0) and (qryMasternCdGrade.Value > 0) then
  begin
  
    case MessageDlg('Deseja gerar grade para deste produto ?',mtConfirmation,[mbYes,mbNo],0) of
        mrYes: btGerarGrade.Click;
    end;

  end ;

end;

procedure TfrmProdutoERP.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;
      
  PosicionaQuery(qryDepartamento, qryMasternCdDepartamento.asString) ;

  qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;


  PosicionaQuery(qryCategoria, qryMasternCdCategoria.asString) ;
  qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryMasternCdCategoria.Value ;

  PosicionaQuery(qrySubCategoria, qryMasternCdSubCategoria.asString) ;
  qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  qryMarca.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;

  PosicionaQuery(qrySegmento, qryMasternCdSegmento.asString) ;

  PosicionaQuery(qryMarca, qryMasternCdMarca.asString) ;

  qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

  PosicionaQuery(qryLinha, qryMasternCdLinha.asString) ;

  PosicionaQuery(qryClasse, qryMasternCdClasseProduto.asString) ;

  PosicionaQuery(qryStatus, qryMasternCdStatus.asString) ;

  PosicionaQuery(qryProdutoIntermediario,qryMasternCdProduto.asString) ;

  PosicionaQuery(qryCC, qryMasternCdCC.AsString) ;

  PosicionaQuery(qryPlanoConta, qryMasternCdPlanoConta.AsString) ;

  PosicionaQuery(qryLocalEstoque, qryMasternCdLocalEstoqueEntrega.AsString) ;

  PosicionaQuery(qryGrupoProduto, qryMasternCdGrupo_Produto.asString) ;

  PosicionaQuery(qryGrupoInventario, qryMasternCdGrupoInventario.AsString) ;

  PosicionaQuery(qryGrupoMRP, qryMasternCdGrupoMRP.AsString) ;

  PosicionaQuery(qryPosicaoEstoque, qryMasternCdProduto.AsString) ;

  tabDadosEstoque.Enabled      := (qryMastercFlgProdEstoque.Value = 1) ;
  tabDadosPlanejamento.Enabled := (qryMastercFlgProdEstoque.Value = 1) ;

  PosicionaQuery( qryLocalEstoquePadrao , qryMasternCdLocalEstoquePadrao.AsString ) ;

end;

procedure TfrmProdutoERP.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryDepartamento.Close ;
  qryCategoria.Close ;
  qrySubCategoria.Close ;
  qrySegmento.Close ;
  qryMarca.Close ;
  qryLinha.Close ;
  qryClasse.Close ;
  qryStatus.Close ;
  qryProdutoIntermediario.Close ;
  qryCC.Close ;
  qryPlanoConta.Close ;
  qryLocalEstoque.Close ;
  qryGrupoProduto.Close ;
  qryGrupoInventario.Close ;
  qryGrupoMRP.Close ;
  qryLocalEstoquePadrao.Close ;
  qryPosicaoEstoque.Close ;

  tabDadosEstoque.Enabled      := false ;
  tabDadosPlanejamento.Enabled := false ;

end;

procedure TfrmProdutoERP.btGerarGradeClick(Sender: TObject);
var
  objForm : TfrmNovaReferencia;
begin
  inherited;

  objForm := TfrmNovaReferencia.Create(nil);

  if not qryMaster.Active then
      exit ;

  if (qryMaster.Active) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

  If (qryMasternCdProduto.Value > 0) then
  begin
      objForm.nCdProduto := qryMasternCdProduto.Value ;
      showForm(objForm,true);

      PosicionaQuery(qryProdutoIntermediario,qryMasternCdProduto.asString) ;

  end ;
end;

procedure TfrmProdutoERP.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            nPK := frmLookup_Padrao.ExecutaConsulta(45);
            If (nPK > 0) then
            begin
                qryMasternCdDepartamento.Value := nPK ;
                PosicionaQuery(qryDepartamento, qryMasternCdDepartamento.asString) ;
                qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

                // consulta de categoria
                nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);
                If (nPK > 0) then
                begin
                    qryMasternCdCategoria.Value := nPK ;
                    PosicionaQuery(qryCategoria, qryMasternCdCategoria.asString) ;
                    qrySubCategoria.Parameters.ParamByname('nCdCategoria').Value := qryCategorianCdCategoria.Value ;

                    // consulta de subcategoria
                    nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
                    If (nPK > 0) then
                    begin
                        qryMasternCdSubCategoria.Value := nPK ;
                        PosicionaQuery(qrySubCategoria,qryMasternCdSubCategoria.asString) ;

                        qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
                        qryMarca.Parameters.ParamByName('nCdSubCategoria').Value    := qrySubCategorianCdSubCategoria.Value ;

                        // Consulta de segmento
                        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
                        If (nPK > 0) then
                        begin
                            qryMasternCdSegmento.Value := nPK ;
                            PosicionaQuery(qrySegmento,qryMasternCdSegmento.AsString) ;

                            // consulta de marca
                            nPK := frmLookup_Padrao.ExecutaConsulta2(49,'nCdStatus = 1 AND EXISTS(SELECT 1 FROM MarcaSubcategoria MSC WHERE MSC.nCdMarca = Marca.nCdMarca AND MSC.nCdSubCategoria = ' + Chr(39) + qrySubCategorianCdSubCategoria.asString + Chr(39) + ')');
                            If (nPK > 0) then
                            begin
                                qryMasternCdMarca.Value := nPK ;
                                PosicionaQuery(qryMarca, qryMasternCdMarca.asString) ;
                                qryLinha.Parameters.ParamByName('nCdMarca').Value := qryMarcanCdMarca.Value ;

                                DBEdit11.SetFocus ;
                            end ;

                        end ;



                    end ;

                end ;

            end ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoERP.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryDepartamento.active or (qryDepartamentonCdDepartamento.Value = 0) then
            begin
                ShowMessage('Selecione um departamento.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);
            If (nPK > 0) then
                qryMasternCdCategoria.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoERP.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryCategoria.active or (qryCategorianCdCategoria.Value = 0) then
            begin
                ShowMessage('Selecione uma Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
            If (nPK > 0) then
                qryMasternCdSubCategoria.Value := nPK ;
        end ;
    end ;
  end ;
end;

procedure TfrmProdutoERP.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qrySubCategoria.active or (qrySubCategorianCdCategoria.Value = 0) then
            begin
                ShowMessage('Selecione uma Sub Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
            If (nPK > 0) then
                qryMasternCdSegmento.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoERP.DBEdit10KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if not qrySubCategoria.active or (qrySubCategorianCdSubCategoria.Value = 0) then
            begin
                ShowMessage('Selecione uma Sub Categoria.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(49,'nCdStatus = 1 AND EXISTS(SELECT 1 FROM MarcaSubcategoria MSC WHERE MSC.nCdMarca = Marca.nCdMarca AND MSC.nCdSubCategoria = ' + Chr(39) + qrySubCategorianCdSubCategoria.asString + Chr(39) + ')');
            If (nPK > 0) then
                qryMasternCdMarca.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoERP.DBEdit11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
    objForm : TfrmIncLinha;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin
            if not qryMarca.active or (qryMarcanCdMarca.Value = 0) then
            begin
                ShowMessage('Selecione uma Marca.') ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdStatus = 1 and nCdMarca = ' + qryMarcanCdMarca.asString);
            If (nPK > 0) then
                qryMasternCdLinha.Value := nPK ;
        end ;
    end ;

    73 : begin
        if (qryMaster.State <> dsInsert) then
        begin
            ShowMessage('Fun��o s� permitida na inclus�o de produtos.') ;
            exit ;
        end ;

        if not qryMarca.active or (qryMarcanCdMarca.Value = 0) then
        begin
            ShowMessage('Selecione uma Marca.') ;
            exit ;
        end ;

        objForm := TfrmIncLinha.Create(nil);

        objForm.nCdMarca   := qryMarcanCdMarca.Value ;
        objForm.Edit1.Text := '' ;
        showForm(objForm,false) ;

        if (objForm.nCdLinha > 0) then
        begin
            qryMasternCdLinha.Value := objForm.nCdLinha ;
            PosicionaQuery(qryLinha, IntToStr(objForm.nCdLinha)) ;
            //DBEdit12.SetFocus ;
        end ;

    end ;

  end ;

end;

procedure TfrmProdutoERP.DBEdit15KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(153);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoInventario.Value := nPK ;
                PosicionaQuery(qryGrupoInventario, IntToStr(nPK)) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmProdutoERP.DBEdit16KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(52);
            If (nPK > 0) then
                qryMasternCdColecao.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoERP.DBEdit20KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(53);
            If (nPK > 0) then
                qryMasternCdClasseProduto.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoERP.DBEdit21KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

      if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);
            If (nPK > 0) then
                qryMasternCdStatus.Value := nPK ;
        end ;
    end ;
  end ;

end;

procedure TfrmProdutoERP.DBEdit12Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsInsert) then
      exit ;

  if (Trim(DBEdit12.Text) <> '') then
  begin

      qryConsultaProd.Close ;

      if (Trim(dbEdit2.Text) <> '') then
        qryConsultaProd.Parameters.ParamByName('nCdDepartamento').Value := StrToInt(Trim(DbEdit2.Text)) ;

      if (Trim(dbEdit4.Text) <> '') then
        qryConsultaProd.Parameters.ParamByName('nCdCategoria').Value    := StrToInt(Trim(DbEdit4.Text)) ;

      if (Trim(dbEdit6.Text) <> '') then
        qryConsultaProd.Parameters.ParamByName('nCdSubCategoria').Value := StrToInt(Trim(DbEdit6.Text)) ;

      if (Trim(dbEdit7.Text) <> '') then
        qryConsultaProd.Parameters.ParamByName('nCdSegmento').Value     := StrToInt(Trim(DbEdit7.Text)) ;

      if (Trim(dbEdit10.Text) <> '') then
        qryConsultaProd.Parameters.ParamByName('nCdMarca').Value        := StrToInt(Trim(DbEdit10.Text)) ;

      if (Trim(dbEdit11.Text) <> '') then
        qryConsultaProd.Parameters.ParamByName('nCdLinha').Value        := StrToInt(Trim(DbEdit11.Text)) ;

      if (Trim(dbEdit12.Text) <> '') then
        qryConsultaProd.Parameters.ParamByName('cReferencia').Value     := Trim(DbEdit12.Text) ;

      qryConsultaProd.Open ;

      if not qryConsultaProd.eof then
      begin
          PosicionaPK(qryConsultaProdnCdProduto.Value) ;
      end ;

  end ;

end;

procedure TfrmProdutoERP.btIncluirClick(Sender: TObject);
var
  nPK : integer ;
begin
  inherited;
  cxPageControl1.ActivePage        := tabDadosGerais ;

  tabDadosEstoque.Enabled          := false ;
  tabDadosPlanejamento.Enabled     := false ;

  qryMasternFatorCompra.Value      := 1 ;
  qryMasternQtdeMinimaCompra.Value := 1 ;
  qryMasternLoteMultiplo.Value     := 1 ;

  nPK := frmLookup_Padrao.ExecutaConsulta(69);

  if (nPK > 0) then
  begin
      qryMasternCdGrupo_Produto.Value := nPK ;
      atribuiCaracteristicasGrupo( nPK ) ;
  end ;

  DbEdit51.SetFocus ;
end;

procedure TfrmProdutoERP.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DbEdit52.Text = '') then
  begin
      MensagemAlerta('Informe o grupo de produtos.') ;
      DbEdit51.SetFocus ;
      abort ;
  end ;

  if     (qryMastercFlgProdEstoque.Value = 0)
     and (qryMastercFlgProdVenda.Value = 0)
     and (qryMastercFlgProdCompra.Value = 0)
     and (qryMastercFlgAtivoFixo.Value = 0) then
  begin

      if (qryMastercFlgFantasma.Value = 0) then
      begin
          MensagemAlerta('Selecione o tipo de Produto.');
          abort ;
      end ;

  end ;

  If (dbEdit2.Text = '') or (dbEdit3.Text = '') then
  begin
      MensagemAlerta('Informe o departamento.') ;
      dbEdit2.SetFocus ;
      Abort ;
  end ;

  If (dbEdit4.Text = '') or (dbEdit5.Text = '') then
  begin
      MensagemAlerta('Informe a categoria.') ;
      dbEdit4.SetFocus ;
      Abort ;
  end ;

  If (dbEdit6.Text = '') or (dbEdit8.Text = '') then
  begin
      MensagemAlerta('Informe a Sub Categoria.') ;
      dbEdit6.SetFocus ;
      Abort ;
  end ;

  If (dbEdit7.Text = '') or (dbEdit9.Text = '') then
  begin
      MensagemAlerta('Informe o Segmento.') ;
      dbEdit7.SetFocus ;
      Abort ;
  end ;

  If (dbEdit10.Text = '') or (dbEdit13.Text = '') then
  begin
      MensagemAlerta('Informe a marca.') ;
      dbEdit10.SetFocus ;
      Abort ;
  end ;

  If (dbEdit11.Text = '') or (dbEdit14.Text = '') then
  begin
      MensagemAlerta('Informe a Linha.') ;
      dbEdit11.SetFocus ;
      Abort ;
  end ;

  If (dbEdit17.Text = '') then
  begin
      MensagemAlerta('Informe a Descri��o.') ;
      dbEdit17.SetFocus ;
      Abort ;
  end ;

  if (DBEdit19.Text = '') then
  begin
      MensagemAlerta('Informe a unidade de medida.') ;
      dbEdit19.SetFocus ;
      abort ;
  end ;

  if not (frmMenu.validaUnidadeMedida(DBEdit19.Text)) then
  begin
      MensagemAlerta('Unidade de medida inv�lida ou n�o cadastrada.') ;
      DBEdit19.SetFocus ;
      abort ;
  end ;

  If (Trim(DbEdit18.Text) = '') then
      qryMastercUnidadeMedidaCompra.Value := qryMastercUnidadeMedida.Value ;

  if (qryMasternFatorCompra.Value < 1) then
      qryMasternFatorCompra.Value := 1 ;

  if (qryMasternQtdeMinimaCompra.Value <= 0) then
      qryMasternQtdeMinimaCompra.Value := 1 ;

  if (qryMasternLoteMultiplo.Value <= 0) then
      qryMasternLoteMultiplo.Value := 1 ;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabTipoProduto.Value := 1 ;
      qryMasternCdStatus.Value         := 1 ;

      if (DBEdit52.Text = '') then
      begin
          qryMasternCdGrupo_Produto.Value := qryDepartamentonCdGrupoProduto.Value ;
          PosicionaQuery(qryGrupoProduto, qryMasternCdGrupo_Produto.AsString) ;
      end ;

  end ;

  if (qryMastercFlgFantasma.Value = 1) and (qryMastercFlgProdEstoque.Value = 1) then
  begin
      MensagemAlerta('Item fantasma n�o pode ser item de estoque.') ;
      abort ;
  end ;

  if (qryMastercFlgFantasma.Value = 1) and (qryMastercFlgProdCompra.Value = 1) then
  begin
      MensagemAlerta('Item fantasma n�o pode ser item de compra.') ;
      abort ;
  end ;

  if (qryMastercFlgFantasma.Value = 1) and (qryMastercFlgProdVenda.Value = 1) then
  begin
      MensagemAlerta('Item fantasma n�o pode ser item de venda.') ;
      abort ;
  end ;

  if (qryMasteriDiaEntrega.Value < 0) then
  begin
      MensagemAlerta('Lead Time inv�lido.') ;
      cxPageControl1.ActivePageIndex := 0 ;
      DBEdit27.SetFocus;
      abort ;
  end ;

  if (qryMasternLoteMultiplo.Value < 0) then
  begin
      MensagemAlerta('M�ltiplo para pedido inv�lido.') ;
      cxPageControl1.ActivePageIndex := 2 ;
      DBEdit31.SetFocus;
      abort ;
  end ;

  if (qryMasternCdTipoPlanejamento.Value = 1) and not (qryMasternCdTipoObtencao.Value IN [1,2]) then
  begin
      MensagemAlerta('Para planejar MRP � obrigat�rio selecionar o m�todo de obten��o.') ;
      cxPageControl1.ActivePageIndex := 2 ;
      comboMetodoObtencao.setFocus;
      abort ;
  end ;

  if (qryMasternCdTipoPlanejamento.Value = 1) and (DBEdit40.Text = '') then
  begin
      MensagemAlerta('Selecione o grupo de MRP.') ;
      cxPageControl1.ActivePageIndex := 2 ;
      DBEdit39.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTipoPlanejamento.Value <> 1) and (DBEdit40.Text <> '') then
  begin
      MensagemAlerta('N�o � permitido informar grupo de MRP para produtos n�o planejados.') ;
      cxPageControl1.ActivePageIndex := 2 ;
      DBEdit39.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTipoObtencao.Value = 2) and (qryMasternCdTabTipoModeloProducao.Value <= 1) then
  begin
      MensagemAlerta('Selecione o modelo de produ��o do produto.') ;
      cxPageControl1.ActivePageIndex := 2 ;
      comboModeloProducao.SetFocus;
      abort ;
  end ;

  if (qryMasternCdTipoObtencao.Value = 2) and (DBEdit36.Text = '') then
  begin
      MensagemAlerta('Selecione o local de estoque padr�o do produto.') ;
      cxPageControl1.ActivePageIndex := 2 ;
      edtLocalEstoque.SetFocus;
      abort ;
  end ;

  if (trim(DBEdit18.Text) <> '') then
      if not (frmMenu.validaUnidadeMedida(DBEdit18.Text)) then
      begin
          MensagemAlerta('Unidade de medida inv�lida ou n�o cadastrada.') ;
          cxPageControl1.ActivePage := tabDadosPlanejamento ;
          DBEdit18.SetFocus ;
          abort ;
      end ;
      
  { -- valida c�digo de barras -- }
  if (frmMenu.LeParametro('VALIDAEAN') = 'S') then
  begin
      ACBrValidador.TipoDocto   := docGTIN;
      ACBrValidador.Documento   := Trim(qryMastercEAN.Value);
      ACBrValidador.IgnorarChar := '';

      if ((Trim(qryMastercEAN.Value) <> '') and (not ACBrValidador.Validar)) then
      begin
          MensagemAlerta('C�digo de barras inv�lido.');
          DBEdit48.SetFocus;
          Abort;
      end;
  end;

  //if (qryMasternCdTipoObtencao.Value = 1) and not (qryMasternCdTipoRessuprimento.Value IN [1,2]) then
  //begin
  //    MensagemAlerta('Para Obten��o por Compra � necess�rio selecionar o tipo de ressuprimento.') ;
  //    abort ;
  //end ;

  inherited;

  {-- se o produto n�o for produzido, for�a o modelo de produ��o para nenhum --}
  if (qryMasternCdTipoObtencao.Value <> 2) then
      qryMasternCdTabTipoModeloProducao.Value := 1;

end;

procedure TfrmProdutoERP.cxButton1Click(Sender: TObject);
begin

  if not qryProdutoIntermediario.Active then
      exit ;

  case MessageDlg('Confirma a exclus�o deste produto ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
     qryProdutoIntermediario.Delete;
  except
      frmMenu.Connection.RollbackTrans ;
      ShowMessage('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryProdutoIntermediario,qryMasternCdProduto.asString) ;

end;

procedure TfrmProdutoERP.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;
      
end;

procedure TfrmProdutoERP.DBEdit28KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(29);

            If (nPK > 0) then
            begin
                qryMasternCdCC.Value := nPK ;
                PosicionaQuery(qryCC, IntToStr(nPK)) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmProdutoERP.DBEdit28Exit(Sender: TObject);
begin
  inherited;
  qryCC.Close ;
  PosicionaQuery(qryCC, DBEdit28.Text) ;

end;

procedure TfrmProdutoERP.DBEdit34Exit(Sender: TObject);
begin
  inherited;

  qryPlanoConta.Close ;
  PosicionaQuery(qryPlanoConta, DbEdit34.Text) ;
  
end;

procedure TfrmProdutoERP.DBEdit34KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(41);

            If (nPK > 0) then
            begin
                qryMasternCdPlanoConta.Value := nPK ;
                PosicionaQuery(qryPlanoConta, IntToStr(nPK)) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmProdutoERP.DBEdit39KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(174);

            If (nPK > 0) then
            begin
                qryMasternCdGrupoMRP.Value := nPK ;
                PosicionaQuery(qryGrupoMRP, IntToStr(nPK)) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmProdutoERP.DBGridEh3Enter(Sender: TObject);
begin

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State <> dsBrowse) then
  begin
      btSalvar.Click ;
  end ;

  inherited;

end;

procedure TfrmProdutoERP.DBGridEh2Enter(Sender: TObject);
begin
  if not qryMaster.Active then
      exit ;

  if (qryMaster.State <> dsBrowse) then
  begin
      btSalvar.Click ;
  end ;

  inherited;

end;

procedure TfrmProdutoERP.DBEdit38Exit(Sender: TObject);
begin
  if (qryMaster.State = dsInsert) then
  begin

    case MessageDlg('Confirma a inclus�o deste produto ?',mtConfirmation,[mbYes,mbNo],0) of
        mrYes: btSalvar.Click;
    end;

  end ;

  inherited;

end;

procedure TfrmProdutoERP.DBEdit45Enter(Sender: TObject);
var
    nAlturaCalc, nLarguraCalc : double ;
    vFrac : integer ;
begin
  inherited;

  if (qryMaster.State = dsBrowse) then
      exit ;

  nAlturaCalc  := frmMenu.TBRound(qryMasternAltura.Value,2) ;
  nLarguraCalc := frmMenu.TBRound(qryMasternLargura.Value,2) ;

  // se for menor que tres, o valor j� foi arredondado.
  if (length(FloatToStr(Frac(nAlturaCalc))) > 3) then
  begin
      vFrac := StrToInt(Copy(FloatToStr(Frac(nAlturaCalc)),4,1));

      if (vFrac > 0) and (vFrac <> 5) then
          nAlturaCalc := nAlturaCalc + ((10 - vFrac)/100) ;
  end ;

  // se for menor que tres, o valor j� foi arredondado.
  if (length(FloatToStr(Frac(nLarguraCalc))) > 3) then
  begin
     vFrac := StrToInt(Copy(FloatToStr(Frac(nLarguraCalc)),4,1));

     if (vFrac > 0) and (vFrac <> 5) then
          nLarguraCalc := nLarguraCalc + ((10 - vFrac)/100) ;
  end ;

  qryMasternDimensao.Value := nAlturaCalc * nLarguraCalc ;

end;

procedure TfrmProdutoERP.DBEdit41Enter(Sender: TObject);
begin
  inherited;

  if (qryMasternDimensao.Value = 0) then
      qryMasternDimensao.Value := 1 ;

  if (qryMasternEspessura.Value = 0) then
      qryMasternEspessura.Value := 1 ;

  if (qryMasternFatorPeso.Value = 0) then
      qryMasternFatorPeso.Value := 1 ;

  if (qryMasternPesoBruto.Value = 0) then
  begin
      qryMasternPesoBruto.Value := qryMasternDimensao.Value * qryMasternEspessura.Value * qryMasternFatorPeso.Value ;
  end ;

  if (qryMasternPesoLiquido.Value = 0) then
  begin
      qryMasternPesoLiquido.Value := qryMasternDimensao.Value * qryMasternEspessura.Value * qryMasternFatorPeso.Value ;
  end ;

end;

procedure TfrmProdutoERP.DBEdit51KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(69);

            If (nPK > 0) then
            begin
                qryMasternCdGrupo_Produto.Value := nPK ;
                PosicionaQuery(qryGrupoProduto, IntToStr(nPK)) ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmProdutoERP.DBEdit51Exit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, DBEdit51.Text) ;

end;

procedure TfrmProdutoERP.cxButton2Click(Sender: TObject);
var
  objForm : TfrmProdutoMRP;
begin
  inherited;

  if (qryProdutoIntermediario.Active) and (qryProdutoIntermediarionCdProduto.Value > 0) then
  begin

      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;
          
      if (qryMasternCdTipoPlanejamento.Value <> 1) then
      begin
          MensagemAlerta('Selecione o tipo de planejamento MRP antes de planejar.') ;
          exit ;
      end ;

      objForm := TfrmProdutoMRP.Create(nil);

      objForm.nCdProduto := qryProdutoIntermediarionCdProduto.Value ;
      objForm.nCdEmpresa := frmMenu.nCdEmpresaAtiva;

      PosicionaQuery(objForm.qryProdutoEmpresa, qryProdutoIntermediarionCdProduto.AsString) ;

      showForm(objForm,true);

  end ;

end;

procedure TfrmProdutoERP.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  cxButton2.Click;
  
end;

procedure TfrmProdutoERP.cxButton3Click(Sender: TObject);
var
  objForm : TfrmProdutoMRP;
begin
  inherited;

  if (qryMaster.Active) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

  if (qryMaster.Active) and (qryMasternCdProduto.Value > 0) then
  begin

      if (qryMasternCdTipoPlanejamento.Value <> 1) then
      begin
          MensagemAlerta('Selecione o tipo de planejamento MRP antes de planejar.') ;
          exit ;
      end ;

      objForm := TfrmProdutoMRP.Create(nil);

      objForm.nCdProduto := qryMasternCdProduto.Value ;
      objForm.nCdEmpresa := frmMenu.nCdEmpresaAtiva;

      PosicionaQuery(objForm.qryProdutoEmpresa, qryMasternCdProduto.AsString) ;

      showForm(objForm,true);

  end ;

end;

procedure TfrmProdutoERP.btPosicaoEstoqueSubItemClick(Sender: TObject);
var
  objForm : TfrmProdutoPosicaoEstoque;
begin
  inherited;

  if (qryProdutoIntermediario.Active) and (qryProdutoIntermediarionCdProduto.Value > 0) then
  begin
      objForm := TfrmProdutoPosicaoEstoque.Create(nil);

      PosicionaQuery(objForm.qryPosicaoEstoque, qryProdutoIntermediarionCdProduto.AsString) ;

      if (objForm.qryPosicaoEstoque.eof) then
      begin
          ShowMessage('Nenhuma posi��o de estoque encontrada para este produto.') ;
          exit ;
      end ;

      showForm(objForm,true);

  end ;

end;

procedure TfrmProdutoERP.cxButton6Click(Sender: TObject);
var
  objForm : TfrmProdutoFornecedor;
begin
  inherited;
  if (qryMaster.Active) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

  if (qryMaster.Active) and (qryMasternCdProduto.Value > 0) then
  begin
      objForm := TfrmProdutoFornecedor.Create(nil);

      PosicionaQuery(objForm.qryProdutoFornecedor, qryMasternCdProduto.AsString) ;

      objForm.nCdProduto := qryMasternCdProduto.Value ;
      showForm(objForm,true);

  end ;


end;

procedure TfrmProdutoERP.cxButton7Click(Sender: TObject);
var
  objForm : TfrmProdutoFornecedor;
begin
  inherited;

  if (qryProdutoIntermediario.Active) and (qryProdutoIntermediarionCdProduto.Value > 0) then
  begin
      objForm := TfrmProdutoFornecedor.Create(nil);

      PosicionaQuery(objForm.qryProdutoFornecedor, qryProdutoIntermediarionCdProduto.AsString) ;

      objForm.nCdProduto := qryProdutoIntermediarionCdProduto.Value ;
      showForm(objForm,true);

  end ;

end;

procedure TfrmProdutoERP.DBEdit15Exit(Sender: TObject);
begin
  inherited;

  qryGrupoInventario.Close ;
  PosicionaQuery(qryGrupoInventario, DBEdit15.Text) ;

end;

procedure TfrmProdutoERP.cxButton8Click(Sender: TObject);
var
  objForm : TfrmProdutoERPInsumos;
begin
  inherited;
  if (qryMaster.Active) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

  if (qryMaster.Active) and (qryMasternCdProduto.Value > 0) then
  begin

      objForm := TfrmProdutoERPInsumos.Create(nil);

      objForm.exibeEstruturaProduto( qryMasternCdProduto.Value );

      freeAndNil( objForm );

  end ;

end;

procedure TfrmProdutoERP.cxButton9Click(Sender: TObject);
var
  objForm : TfrmProdutoERPInsumos;
begin
  inherited;

  if (qryProdutoIntermediario.Active) and (qryProdutoIntermediarionCdProduto.Value > 0) then
  begin
  
      objForm := TfrmProdutoERPInsumos.Create(nil);

      objForm.exibeEstruturaProduto( qryProdutoIntermediarionCdProduto.Value );

      freeAndNil( objForm ) ;

  end ;

end;

procedure TfrmProdutoERP.DBEdit39Exit(Sender: TObject);
begin
  inherited;

  qryGrupoMRP.Close ;
  PosicionaQuery(qryGrupoMRP, DBEdit39.Text) ;
  
end;

procedure TfrmProdutoERP.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  
  qryMasternValCusto.DisplayFormat := frmMenu.cMascaraCompras;
  qryMasternValVenda.DisplayFormat := frmMenu.cMascaraCompras;

  qryProdutoIntermediarionValCusto.DisplayFormat := frmMenu.cMascaraCompras;
  qryProdutoIntermediarionValVenda.DisplayFormat := frmMenu.cMascaraCompras;

  qryTipoPlanejamento.Close;
  qryTipoPlanejamento.Open;

  qryTipoObtencao.Close;
  qryTipoObtencao.Open;

  qryModoGestao.Close;
  qryModoGestao.Open;

  qryMetodoUso.Close;
  qryMetodoUso.Open;

  qryTabTipoMetodoSaidaEstoque.Close;
  qryTabTipoMetodoSaidaEstoque.Open;

  qryModeloProducao.Close;
  qryModeloProducao.Open;

  qryTipoApropriacao.Close;
  qryTipoApropriacao.Open;

  qryTabTipoEmpenhoProduto.Close;
  qryTabTipoEmpenhoProduto.Open;

  qryTabTipoCodProdFat.Close;
  qryTabTipoCodProdFat.Open;

end;

procedure TfrmProdutoERP.qryMasternCdTipoObtencaoChange(Sender: TField);
begin
  inherited;

  comboTipoRessuprimento.Enabled := false ;
  qryTipoRessuprimento.Close;

  {-- compra --}
  if (qryMasternCdTipoObtencao.Value = 1) then
  begin
      qryTipoRessuprimento.Open;
      comboTipoRessuprimento.Enabled := True ;
  end ;

end;

procedure TfrmProdutoERP.qryMastercFlgProdEstoqueChange(Sender: TField);
begin
  inherited;
  tabDadosEstoque.Enabled      := (qryMastercFlgProdEstoque.Value = 1) ;
  tabDadosPlanejamento.Enabled := (qryMastercFlgProdEstoque.Value = 1) ;
end;

procedure TfrmProdutoERP.DBEdit19Exit(Sender: TObject);
begin
  inherited;

  if (trim(DBEdit19.Text) = '') then
  begin
      MensagemAlerta('Informe a unidade de medida padr�o.') ;
  end ;
end;

procedure TfrmProdutoERP.cxButton10Click(Sender: TObject);
var
  objForm : TfrmProdutoERP_Identado ;
begin

  if (qryMaster.Active) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

  if (qryMaster.Active) and (qryMasternCdProduto.Value > 0) then
  begin

      objForm := TfrmProdutoERP_Identado.Create(nil);

      objForm.EstruturaIdentada( qryMasternCdProduto.Value );

      freeAndNil( objForm );

  end ;

end;

procedure TfrmProdutoERP.cxButton11Click(Sender: TObject);
var
  objForm : TfrmProdutoERP_UltimasCompras;
begin
  inherited;

  if (qryMaster.Active) and (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

  if (qryMaster.Active) and (qryMasternCdProduto.Value > 0) then
  begin

      objForm := TfrmProdutoERP_UltimasCompras.Create(nil);

      objForm.qryUltimasCompras.Close;
      objForm.qryUltimasCompras.Parameters.ParamByName('nCdProduto').Value := qryMasternCdProduto.Value;
      objForm.qryUltimasCompras.Open;

      if (objForm.qryUltimasCompras.RecordCount > 0) then
          showForm(objForm, True)
      else MensagemAlerta('Nenhuma compra encontrada para este produto.');

  end;

end;

procedure TfrmProdutoERP.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  inherited;

  btPosicaoEstoqueSubItem.Click;

end;

procedure TfrmProdutoERP.atribuiCaracteristicasGrupo(
  nCdGrupoProduto: integer);
begin

  qryGrupoProduto.Close;
  PosicionaQuery( qryGrupoProduto , intToStr( nCdGrupoProduto ) ) ;

  if not qryGrupoProduto.eof then
  begin

      if not (qryMaster.State in ([dsInsert,dsEdit])) then
          qryMaster.Edit ;

      qryMastercFlgProdEstoque.Value := qryGrupoProdutocFlgProdEstoqueGP.Value ;
      qryMastercFlgProdVenda.Value   := qryGrupoProdutocFlgProdVendaGP.Value ;
      qryMastercFlgProdCompra.Value  := qryGrupoProdutocFlgProdCompraGP.Value ;
      qryMastercFlgAtivoFixo.Value   := qryGrupoProdutocFlgAtivoFixoGP.Value ;

      qryMasternCdGrupoMRP.asString         := qryGrupoProdutonCdGrupoMRPGP.asString ;
      qryMasternCdGrupoInventario.asString  := qryGrupoProdutonCdGrupoInventarioGP.asString ;
      qryMasternCdTipoPlanejamento.asString := qryGrupoProdutonCdTipoPlanejamentoGP.asString ;
      qryMasternCdTipoObtencao.asString     := qryGrupoProdutonCdTipoObtencaoGP.asString ;

      if (qryGrupoProdutoiLeadTimeGP.Value > 0) then
          qryMasteriDiaEntrega.Value := qryGrupoProdutoiLeadTimeGP.Value ;

      if (qryGrupoProdutonLoteMinimoGP.Value > 0) then
          qryMasternQtdeMinimaCompra.Value := qryGrupoProdutonLoteMinimoGP.Value ;

      if (qryGrupoProdutonLoteMultiploGP.Value > 0) then
          qryMasternLoteMultiplo.Value := qryGrupoProdutonLoteMultiploGP.Value ;

      qryGrupoMRP.Close;
      qryGrupoInventario.Close;

      PosicionaQuery( qryGrupoMRP , qryMasternCdGrupoMRP.asString ) ;
      PosicionaQuery( qryGrupoInventario , qryMasternCdGrupoInventario.asString ) ;

  end ;

{}
end;

procedure TfrmProdutoERP.qryMasternCdGrupo_ProdutoChange(Sender: TField);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      atribuiCaracteristicasGrupo( qryMasternCdGrupo_Produto.Value ) 
  else
  begin
      if (qryMaster.State = dsEdit) then
          if (MessageDlg('Voc� alterou o grupo de produtos deste item. Deseja atribuir as caracteristicas padr�es deste grupo no produto ?',mtConfirmation,[mbYes,mbNo],0) = MRYES) then
              atribuiCaracteristicasGrupo( qryMasternCdGrupo_Produto.Value ) ;

  end ;
  
end;

procedure TfrmProdutoERP.popupMenuMaisPopup(Sender: TObject);
begin
  inherited;
  
  DepartamentoCategoriaSubCategoria1.Enabled := frmMenu.fnValidaUsuarioAPL('FRMDEPARTAMENTO') ;
  OperaoManualnoEstoque1.Enabled     := frmMenu.fnValidaUsuarioAPL('FRMOPERMANESTOQUE') ;
  ConsultaMovimentaoEstoque1.Enabled := frmMenu.fnValidaUsuarioAPL('FRMCONSULTAMOVESTOQUE') ;

  GrupodeProdutos1.Enabled  := frmMenu.fnValidaUsuarioAPL('FRMGRUPOPRODUTO') ;
  Marca1.Enabled            := frmMenu.fnValidaUsuarioAPL('FRMMARCA') ;
  ClassedeProdutos1.Enabled := frmMenu.fnValidaUsuarioAPL('FRMCLASSEPRODUTO') ;
  GrupodeInventrio1.Enabled := frmMenu.fnValidaUsuarioAPL('FRMGRUPOINVENTARIO') ;
  GrupodeMRP1.Enabled       := frmMenu.fnValidaUsuarioAPL('FRMGRUPOMRP') ;
  LocaldeEstoque1.Enabled   := frmMenu.fnValidaUsuarioAPL('FRMLOCALESTOQUE') ;

end;

procedure TfrmProdutoERP.GrupodeProdutos1Click(Sender: TObject);
var
  objForm : TfrmGrupoProduto ;
begin
  inherited;

  objForm := TfrmGrupoProduto.Create( Self ) ;
  showForm( objForm , TRUE ) ;

end;

procedure TfrmProdutoERP.DepartamentoCategoriaSubCategoria1Click(
  Sender: TObject);
var
  objForm : TfrmDepartamento ;
begin
  inherited;

  objForm := TfrmDepartamento.Create( Self ) ;
  showForm( objForm , TRUE ) ;

end;

procedure TfrmProdutoERP.Marca1Click(Sender: TObject);
var
  objForm : TfrmMarca ;
begin
  inherited;

  objForm := TfrmMarca.Create( Self ) ;
  showForm( objForm , TRUE ) ;

end;

procedure TfrmProdutoERP.ClassedeProdutos1Click(Sender: TObject);
var
  objForm : TfrmClasseProduto ;
begin
  inherited;

  objForm := TfrmClasseProduto.Create( Self ) ;
  showForm( objForm , TRUE ) ;

end;

procedure TfrmProdutoERP.GrupodeInventrio1Click(Sender: TObject);
var
  objForm : TfrmGrupoInventario ;
begin
  inherited;

  objForm := TfrmGrupoInventario.Create( Self ) ;
  showForm( objForm , TRUE ) ;

end;

procedure TfrmProdutoERP.GrupodeMRP1Click(Sender: TObject);
var
  objForm : TfrmGrupoMRP ;
begin
  inherited;

  objForm := TfrmGrupoMRP.Create( Self ) ;
  showForm( objForm , TRUE ) ;

end;

procedure TfrmProdutoERP.LocaldeEstoque1Click(Sender: TObject);
var
  objForm : TfrmLocalEstoque ;
begin
  inherited;

  objForm := TfrmLocalEstoque.Create( Self ) ;
  showForm( objForm , TRUE ) ;

end;

procedure TfrmProdutoERP.OperaoManualnoEstoque1Click(Sender: TObject);
var
  objForm : TfrmOperManEstoque ;
begin
  inherited;

  objForm := TfrmOperManEstoque.Create( Self ) ;

  {-- se tiver algum produto posicionado na tela, ao carregar a tela de movimenta��o de estoque --}
  {-- ja leva o produto posicionado --}
  if (qryMaster.Active) and (qryMasternCdProduto.Value > 0) then
  begin

      {-- s� faz essa opera��o se o produto n�o tiver subitens --}
      if (not qryProdutoIntermediario.Active) or (qryProdutoIntermediario.RecordCount = 0) then
          objForm.nCdProdutoExterno := qryMasternCdProduto.Value ;

  end ;

  showForm( objForm , TRUE ) ;

end;

procedure TfrmProdutoERP.ConsultaMovimentaoEstoque1Click(Sender: TObject);
var
  objForm : TfrmConsultaMovEstoque ;
begin
  inherited;

  objForm := TfrmConsultaMovEstoque.Create( Self ) ;

  {-- se tiver algum produto posicionado na tela, ao carregar a tela de movimenta��o de estoque --}
  {-- ja leva o produto posicionado --}
  if (qryMaster.Active) and (qryMasternCdProduto.Value > 0) then
  begin

      {-- s� faz essa opera��o se o produto n�o tiver subitens --}
      if (not qryProdutoIntermediario.Active) or (qryProdutoIntermediario.RecordCount = 0) then
      begin
          objForm.edtCdProduto.Text := qryMasternCdProduto.asString ;
          objForm.PosicionaQuery( objForm.qryProduto , objForm.edtCdProduto.Text ) ;
      end ;

  end ;

  showForm( objForm , TRUE ) ;

end;

procedure TfrmProdutoERP.qryProdutoIntermediarioBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  { -- valida c�digo de barras -- }
  if (frmMenu.LeParametro('VALIDAEAN') = 'S') then
  begin
      ACBrValidador.TipoDocto   := docGTIN;
      ACBrValidador.Documento   := Trim(qryProdutoIntermediariocEAN.Value);
      ACBrValidador.IgnorarChar := '';

      if ((Trim(qryProdutoIntermediariocEAN.Value) <> '') and (not ACBrValidador.Validar)) then
      begin
          MensagemAlerta('C�digo de barras inv�lido.');
          cxPageControl1.SetFocus;
          Abort;
      end;
  end;

end;

procedure TfrmProdutoERP.qryProdutoIntermediarioAfterInsert(
  DataSet: TDataSet);
begin
  inherited;

  DataSet.Cancel;
end;

initialization
    RegisterClass(TfrmProdutoERP) ;

end.
