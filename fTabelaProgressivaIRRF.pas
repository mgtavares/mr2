unit fTabelaProgressivaIRRF;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, DBGridEhGrouping, GridsEh, DBGridEh, cxPC, cxControls,
  StdCtrls, Mask, DBCtrls;

type
  TfrmTabelaProgressivaIRRF = class(TfrmCadastro_Padrao)
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryIRRF: TADOQuery;
    dsIRRF: TDataSource;
    qryMasternCdTabelaIRRF: TIntegerField;
    qryMastercMesAno: TStringField;
    qryIRRFnCdTabelaIRRF: TIntegerField;
    qryIRRFcMesAno: TStringField;
    qryIRRFnValInicial: TBCDField;
    qryIRRFnValFinal: TBCDField;
    qryIRRFnAliqIRRF: TBCDField;
    qryIRRFnValParcDedutivel: TBCDField;
    qryIRRFnValParcDedutDep: TBCDField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure qryIRRFBeforePost(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure qryIRRFAfterPost(DataSet: TDataSet);
    procedure qryIRRFAfterScroll(DataSet: TDataSet);
    procedure btExcluirClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTabelaProgressivaIRRF: TfrmTabelaProgressivaIRRF;

implementation

uses fMenu;

{$R *.dfm}
procedure TfrmTabelaProgressivaIRRF.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TABELAIRRF' ;
  nCdTabelaSistema  := 507 ;
  nCdConsultaPadrao := 1020 ;

  btAlterar.Enabled  := False ;
  btExcluir.Enabled  := False ;
  btSalvar.Enabled   := False ;

end;

procedure TfrmTabelaProgressivaIRRF.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBedit2.SetFocus;
  btAlterar.Enabled  := False ;
  btExcluir.Enabled  := False ;
  btSalvar.Enabled   := False ;

end;

procedure TfrmTabelaProgressivaIRRF.FormShow(Sender: TObject);
begin
  inherited;
  if not qryMaster.Eof then
     PosicionaQuery(qryIRRF,qryMastercMesAno.Value);

end;

procedure TfrmTabelaProgressivaIRRF.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  if ((Copy((Trim(DBEdit2.Text)),3,1)) <> '/') then
  begin
      MensagemAlerta('Informe Ano Mes conforme modelo: 01/2010.') ;
      abort ;
  end;

  if (strToInt(Copy((Trim(DBEdit2.Text)),1,2)) > 12) then
  begin
      MensagemAlerta('Ano maior que 12.') ;
      abort ;
  end;

  if (strToInt(Copy((Trim(DBEdit2.Text)),1,2)) < 0) then
  begin
      MensagemAlerta('Ano menou ou igual a 0.') ;
      abort ;
  end;

  if (strToInt(Copy((Trim(DBEdit2.Text)),4,4)) < 2000) then
  begin
      MensagemAlerta('Ano inferior a 2000') ;
      abort ;
  end;

  if (Trim(DBEdit2.Text) <> '') then
      PosicionaQuery(qryIRRF,DBEDIT2.Text);

  btSalvar.Enabled   := False ;
  DBGridEh2.SetFocus;

end;

procedure TfrmTabelaProgressivaIRRF.qryIRRFBeforePost(DataSet: TDataSet);
begin

  if (qryIRRFnValFinal.Value < qryIRRFnValInicial.Value) then
  begin
      MensagemAlerta('Valor Faixa Final menor que Valor Faixa Inicial.') ;
      abort ;
  end;

  if ((qryIRRFnValFinal.Value + qryIRRFnValInicial.Value) <= 0) then
  begin
      MensagemAlerta('Informe o valor inicial e valor final.') ;
      abort ;
  end;

  if (qryIRRFnValFinal.Value < 0) then
  begin
      MensagemAlerta('Valor Faixa Final menor 0.') ;
      abort ;
  end;

  if (qryIRRFnValInicial.Value < 0) then
  begin
      MensagemAlerta('Valor Faixa Inicial menor 0.') ;
      abort ;
  end;

  if (qryIRRFnAliqIRRF.Value < 0) then
  begin
      MensagemAlerta('Valor Al�quota menor 0.') ;
      abort ;
  end;

  if (qryIRRFnAliqIRRF.Value > 100) then
  begin
      MensagemAlerta('Valor Al�quota maior que 100.') ;
      abort ;
  end;

  if (qryIRRFnValParcDedutivel.Value < 0) then
  begin
      MensagemAlerta('Valor Parcela Dedut�vel menor que 0.') ;
      abort ;
  end;

  if (qryIRRFnValParcDedutDep.Value < 0) then
  begin
      MensagemAlerta('Valor Parcela Dedut�vel Dependente menor que 0.') ;
      abort ;
  end;

  qryIRRFcMesAno.Value := qryMastercMesAno.Value;

  if (qryIRRF.State = dsInsert) then
      qryIRRFnCdTabelaIRRF.Value := frmMenu.fnProximoCodigo('TABELAIRRF') ;

  inherited;

end;

procedure TfrmTabelaProgressivaIRRF.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryIRRF.Close;

end;

procedure TfrmTabelaProgressivaIRRF.btConsultarClick(Sender: TObject);
begin
  inherited;
   if not qryMaster.Eof then
       PosicionaQuery(qryIRRF,qryMastercMesAno.Value);
   btSalvar.Enabled   := False ;
   btExcluir.Enabled  := False ;

end;

procedure TfrmTabelaProgressivaIRRF.ToolButton1Click(Sender: TObject);
begin
  inherited;
  qryIRRF.Close;
  if not qryMaster.Eof then
      PosicionaQuery(qryIRRF,qryMastercMesAno.Value);

  btSalvar.Enabled   := False ;
  btExcluir.Enabled := False ;

end;

procedure TfrmTabelaProgressivaIRRF.qryIRRFAfterPost(DataSet: TDataSet);
begin
  inherited;
//  qryMasternCdTabelaIRRF.Value := qryIRRFnCdTabelaIRRF.Value;
  btSalvar.Enabled  := False ;
  btExcluir.Enabled := False ;

end;

procedure TfrmTabelaProgressivaIRRF.qryIRRFAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryMasternCdTabelaIRRF.Value := qryIRRFnCdTabelaIRRF.Value;

end;

procedure TfrmTabelaProgressivaIRRF.btExcluirClick(Sender: TObject);
begin
  inherited;
  btExcluir.Enabled := False ;

end;

procedure TfrmTabelaProgressivaIRRF.btSalvarClick(Sender: TObject);
begin
  inherited;
  btSalvar.Enabled  := False ;

end;

initialization
    RegisterClass(tFrmTabelaProgressivaIRRF) ;

end.
