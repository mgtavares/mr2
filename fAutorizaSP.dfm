inherited frmAutorizaSP: TfrmAutorizaSP
  Left = 52
  Top = 47
  Caption = 'frmAutorizaSP'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 40
    Top = 46
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero SP'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 59
    Top = 94
    Width = 37
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo SP'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [3]
    Left = 56
    Top = 118
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit4
  end
  object Label6: TLabel [4]
    Left = 53
    Top = 70
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [5]
    Left = 38
    Top = 142
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero NF'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [6]
    Left = 54
    Top = 166
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'S'#233'rie NF'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [7]
    Left = 26
    Top = 190
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Total SP'
    FocusControl = DBEdit9
  end
  object Label3: TLabel [8]
    Left = 20
    Top = 262
    Width = 76
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Libera'#231#227'o'
    FocusControl = DBEdit12
  end
  object Label5: TLabel [9]
    Left = 8
    Top = 286
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Autoriza'#231#227'o'
    FocusControl = DBEdit13
  end
  object Label10: TLabel [10]
    Left = 25
    Top = 310
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Cont'#225'bil'
    FocusControl = DBEdit14
  end
  object Label12: TLabel [11]
    Left = 23
    Top = 214
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Provis'#227'o'
    FocusControl = DBEdit15
  end
  object Label13: TLabel [12]
    Left = 30
    Top = 238
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = '% Prev x Real'
    FocusControl = DBEdit16
  end
  inherited ToolBar2: TToolBar
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited btSalvar: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [14]
    Tag = 1
    Left = 100
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdSP'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [15]
    Tag = 1
    Left = 100
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdTipoSP'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [16]
    Tag = 1
    Left = 172
    Top = 88
    Width = 650
    Height = 19
    DataField = 'cNmTipoSP'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [17]
    Tag = 1
    Left = 100
    Top = 112
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [18]
    Tag = 1
    Left = 172
    Top = 112
    Width = 650
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [19]
    Tag = 1
    Left = 100
    Top = 64
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [20]
    Left = 100
    Top = 136
    Width = 89
    Height = 19
    DataField = 'cNrTit'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [21]
    Left = 100
    Top = 160
    Width = 26
    Height = 19
    DataField = 'cSerieTit'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit9: TDBEdit [22]
    Tag = 1
    Left = 100
    Top = 184
    Width = 89
    Height = 19
    DataField = 'nValSP'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit10: TDBEdit [23]
    Tag = 1
    Left = 172
    Top = 64
    Width = 65
    Height = 19
    DataField = 'cSigla'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit11: TDBEdit [24]
    Tag = 1
    Left = 244
    Top = 64
    Width = 578
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsMaster
    TabOrder = 11
  end
  object btAutorizarSP: TcxButton [25]
    Left = 673
    Top = 136
    Width = 149
    Height = 33
    Caption = 'Autorizar SP'
    TabOrder = 12
    TabStop = False
    OnClick = btAutorizarSPClick
    Glyph.Data = {
      6E040000424D6E04000000000000360000002800000013000000120000000100
      1800000000003804000000000000000000000000000000000000C6D6DEC6D6DE
      C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
      DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6
      D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE0000000000000000008C9C9CC6D6DE
      C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DEC6DEC6C6D6DEC6D6
      DEC6D6DEC6D6DEC6D6DE00000018A54A18A54A18A54A0000008C9C9CC6D6DEC6
      D6DEC6D6DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
      00000000000018A54A18A54A18A54A18A54A18A54A0000008C9C9CC6D6DEC6D6
      DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6D6DE0000000000000000006B
      D6BD18A54A6BD6BD0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE
      C6D6DE000000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFF0000006BD6
      BD000000EFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE00
      0000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFFEFFFFF000000EFFFFF
      EFFFFFEFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DE000000C6DE
      C6C6D6DE000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFBDBDBDEFFFFFEF
      FFFFEFFFFF0000006BD6BD18A54A18A54A000000C6D6DE000000C6D6DEC6D6DE
      000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDBDBDBDBDBDBDBDBD
      BDEFFFFF0000006BD6BD18A54A000000C6D6DE000000C6D6DEC6D6DE000000BD
      BDBDEFFFFFEFFFFFEFFFFF000000000000000000000000000000EFFFFFEFFFFF
      000000000000000000C6D6DEC6D6DE000000C6D6DEC6D6DE000000BDBDBDEFFF
      FFEFFFFFEFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6
      D6DEC6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DE000000BDBDBDEFFFFFEFFFFF
      EFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6
      DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DE000000BDBDBDEFFFFFEFFFFFEF
      FFFF000000BDBDBDEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6DEC6D6DEC6D6DE
      C6D6DE000000C6D6DEC6DEC6C6D6DE000000BDBDBDBDBDBDEFFFFFEFFFFF0000
      00EFFFFFEFFFFFDEBDD6DEBDD6000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE00
      0000C6D6DEC6D6DEC6D6DEC6D6DE000000000000BDBDBDBDBDBDBDBDBDBDBDBD
      BDBDBD000000000000C6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DE000000C6D6
      DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000000000000000000000C6
      D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DE
      C6DEC6C6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
      DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6DEC6000000C6DEC6C6D6DEC6D6DEC6
      D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
      C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000}
    LookAndFeel.Kind = lfOffice11
    Margin = 10
  end
  object cxButton2: TcxButton [26]
    Left = 673
    Top = 206
    Width = 149
    Height = 33
    Caption = 'Cancelar Autoriza'#231#227'o'
    TabOrder = 13
    TabStop = False
    OnClick = cxButton2Click
    Glyph.Data = {
      32040000424D3204000000000000360000002800000013000000110000000100
      180000000000FC03000000000000000000000000000000000000FAFBFFFFFBFC
      FFFAF6FFFDF5FFFFF5FFFFF4FFFFF4FFFDF1FFFDF1FFFFF5FFFFF7FFFAF4FFF8
      F0FFF6E8FFFDEDFFFBEFFFF5EAFFF6F0FFFEFC000000FFFEFFFFFBF7F1E0D7E5
      D0C1EBD1C0F9DCC7EAC9B5F4D1BDFDD8C4EBC5B3E7C3B1F9D5C5FDD9C7FFE2CE
      FFE1CBF3D3C0E3C6B7F0D7CDFFFAF3000000FFFFFBFFFAF1DFC8B9B193809B77
      5FB0886CA17558A97A5EB9896DB7876BB6856BB7866CAD7C62A7785DA0725A9E
      755EB69280E8CBBDFFFBF3000000FFFFF7FFFAEBE8CBB69F7A60623617572705
      743E1B6E34106327037438147437156629076A2F0F743D1E582406623318A97E
      69F6D4C4FFFDF2000000FFF9F0FFFAEAFDDDC6AA826655240467310EC28760AD
      7048692B036A2B05692B05753713B67A56B17551703815643111B4876CFFDDC9
      FFF7E9000000FFFFFBFFFFF4F4D6C5A57C635C2A0C6F3616C88C68FFCCA8CD93
      6F602805592501A67352E6B28EBA7D556529006A3510AA7C5DEAC8B0FFFFF000
      0000FFFFFCFFFFF4F6D5C5A77B635E2A0C6329067A3C18C48962FCC39CB47F5A
      C69470DEB08EB8866285471E75360A733C15B18463FAD8C0FFFDEB000000FFFF
      F9FFFFF1F9D6C2AB7C60612B0A8A4F2871330B6C2C03CA8C63D2976FE2AB84B4
      805B622E066E300784441B6C3510A87A5BFFDFC7FFFDED000000FFFFF7FFFFF0
      FDD7BFAC7D5E632C0775370F7031056E2D01C48559EBAE82E9AF85B68057672F
      0665270070310B6E3515AA7B5FF8D6BFFFFDEE000000FFFFF5FFFFEFFFD7BEAE
      7D5D632C076224007D3C0FC98659E6A578CA8C5EC68B5EFBC295D2986E74350F
      5617007A4225B98B73F0CCBAFFFBEC000000FFFFF5FFFFEFFDD7BFAB7D5E612B
      0882451DC38458FFC397B7794B7C3F136F3407CB9469FFC89FB87B596024066E
      381FAF826DF0CEBEFFFFF4000000FFFFF7FFFFF1F7D6C2A57C635A2B0B6B3512
      B47B54D1966E7A3F17713910632D046E3A12B9845FBC83646E361D5826109E73
      62F8D6C9FFFEF7000000FFFFFBFFFFF4F0D6C69E7B67532A1150210551200067
      3413461300582806744624491C005526075C290F5D2A16532513A47D6FFCDED3
      FFEFE9000000FFFEFEFFFBF8DCC7BFA88D7F9A7969A3806CA78169A98069A981
      68A98168A78268A58268A78169A37865A87B6DA67F71B39086DFC3BCFFF9F400
      0000FFFEFFFFFEFEF5E6E3E0CDC6E3CDC2EAD0C2EBD1C1EDD0C1EDD1C0EDD1C0
      EDD1C0EBD1C1EDD0C1F3D0C3F4D0C6EDCCC3E9CDC6F7E1DCFFFBFA000000FAFA
      FFFFFDFFFFF8F8FFF7F3FFFFF9FFFFF8FFFFF7FFFFF7FFFFF7FFFFF7FFFFF7FF
      FFF8FFFFF7FFFEF7FFFEF7FFFEF8FFFAF5FFF8F7FFFDFD000000F7FAFFFDFCFE
      FEFCFCFFFCFBFFFFFCFFFFFCFFFFFBFFFFFBFFFFFBFFFFFCFFFFFCFFFFFCFFFE
      FCFFFCF9FFFDFAFFFEFBFFFEFCFFFDFDFFFAFB000000}
    LookAndFeel.Kind = lfOffice11
    Margin = 10
  end
  object DBEdit12: TDBEdit [27]
    Tag = 1
    Left = 100
    Top = 256
    Width = 129
    Height = 19
    DataField = 'dDtLiber'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit13: TDBEdit [28]
    Tag = 1
    Left = 100
    Top = 280
    Width = 129
    Height = 19
    DataField = 'dDtAutor'
    DataSource = dsMaster
    TabOrder = 15
  end
  object DBEdit14: TDBEdit [29]
    Tag = 1
    Left = 100
    Top = 304
    Width = 129
    Height = 19
    DataField = 'dDtContab'
    DataSource = dsMaster
    TabOrder = 16
  end
  object DBGridEh1: TDBGridEh [30]
    Left = 244
    Top = 136
    Width = 389
    Height = 139
    DataGrouping.GroupLevels = <>
    DataSource = dsParcela
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 17
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdPrazoSP'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdSP'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footers = <>
        Width = 147
      end
      item
        EditButtons = <>
        FieldName = 'nValPagto'
        Footers = <>
        Width = 149
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object btImprimirSP: TcxButton [31]
    Left = 673
    Top = 242
    Width = 149
    Height = 33
    Caption = 'Imprimir SP'
    TabOrder = 18
    OnClick = btImprimirSPClick
    Glyph.Data = {
      0A020000424D0A0200000000000036000000280000000C0000000D0000000100
      180000000000D401000000000000000000000000000000000000C2C2C29E9E9E
      656464656464656464656464656464656464656464656464898A89B9BABAD5D5
      D5515757313332484E4E3133323D42423D42423D42423D4242191D23515757C6
      C6C68C908F191D23313332313332313332313332313332313332313332191D23
      040506898A89656464898A89DADDD7B9BABABEBEBEBEBEBEBEBEBEBEBEBEBEBA
      BCCCCCCC5157576564647172723D4242898A89777B7B7B85857B85857B85857B
      85857B85859392923D4242777B7BB6B6B63133327B8585AEAEAEA4A8A8A4A8A8
      A4A8A8A4A8A8A4A8A8484E4E313332B9BABAD5D5D5484E4EC6C6C6FCFEFEFCFE
      FEFCFEFEFCFEFEFCFEFEFCFEFE898A89484E4EC6C6C6CCCCCC484E4EC6C6C6FC
      FEFEFCFEFEFCFEFEFCFEFEFCFEFEFCFEFE7B85853D4242C2C2C2CCCCCC484E4E
      C6C6C6FCFEFEFCFEFEFCFEFEFCFEFEFCFEFED5D5D57172723D4242C2C2C2CCCC
      CC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE7B8585191D23191D23515757C6
      C6C6CCCCCC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE717272484E4E3D4242
      939292C6C6C6CCCCCC484E4E313332575D5E515757515757575D5E191D23191D
      23939292CCCCCCBEBEBEC2C2C2B6B6B69E9E9E9E9E9E9E9E9E9E9E9E9E9E9EA4
      A8A8AEAEAEC6C6C6BEBEBEBEBEBE}
    LookAndFeel.NativeStyle = True
    Margin = 10
  end
  object cxPageControl1: TcxPageControl [32]
    Left = 4
    Top = 536
    Width = 949
    Height = 185
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 19
    ClientRectBottom = 181
    ClientRectLeft = 4
    ClientRectRight = 945
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Outros T'#237'tulos do Terceiro Vencendo nas Mesmas Datas'
      ImageIndex = 0
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 941
        Height = 157
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsOutrosTitulos
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh2DblClick
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTitulo'
            Footers = <>
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'cNrTit'
            Footers = <>
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'cNrNF'
            Footers = <>
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'dDtEmissao'
            Footers = <>
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'dDtVenc'
            Footers = <>
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'nCdSP'
            Footers = <>
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'nCdRecebimento'
            Footers = <>
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'nValTit'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nSaldoTit'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object cxButton3: TcxButton [33]
    Left = 673
    Top = 171
    Width = 149
    Height = 33
    Caption = 'Alterar SP'
    TabOrder = 20
    TabStop = False
    OnClick = cxButton3Click
    Glyph.Data = {
      A6020000424DA60200000000000036000000280000000F0000000D0000000100
      1800000000007002000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFF0000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFFFFFFFFFFFFFF000000FF9C31000000BDBDBDEF
      FFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000000000000000
      000000000000F7CE8CFF9C31000000A56300FF9C31FF9C31FF9C31FF9C31FF9C
      31FF9C31000000000000000000F7CE8CF7CE8CF7CE8CF7CE8CF7CE8CFF9C3100
      0000A56300EFFFFFEFFFFFEFFFFFEFFFFFFF9C31000000000000000000FFEFDE
      FFEFDEF7CE8CF7CE8CF7CE8CF7CE8CFF9C31000000FF9C31FF9C31FF9C31FF9C
      31FF9C31000000000000000000EFFFFFEFFFFFEFDED6F7CE8CF7CE8CFFEFDE00
      0000A56300EFFFFFEFFFFFEFFFFFEFFFFFFF9C31000000000000000000000000
      000000000000F7CE8CEFDED6000000A56300FF9C31FF9C31FF9C31FF9C31FF9C
      31FF9C31000000000000FFFFFFFFFFFFFFFFFF000000FFEFDE000000BDBDBDEF
      FFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000FFFFFFFFFFFF
      FFFFFF000000000000000000EFFFFFA5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5
      A5EFFFFF000000000000FFFFFFFFFFFFFFFFFF0000008C9C9C000000EFFFFFEF
      FFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF000000BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD
      BDBDBDBD000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000BDBDBDBD
      BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000
      00000000000000000000}
    LookAndFeel.Kind = lfOffice11
    Margin = 10
  end
  object DBEdit15: TDBEdit [34]
    Tag = 1
    Left = 100
    Top = 208
    Width = 89
    Height = 19
    DataField = 'nValProvisao'
    DataSource = dsMaster
    TabOrder = 21
  end
  object DBEdit16: TDBEdit [35]
    Tag = 1
    Left = 100
    Top = 232
    Width = 89
    Height = 19
    DataField = 'nValOscilacao'
    DataSource = dsMaster
    TabOrder = 22
    OnChange = DBEdit16Change
  end
  object cxPageControl2: TcxPageControl [36]
    Left = 4
    Top = 336
    Width = 949
    Height = 193
    ActivePage = cxTabSheet2
    LookAndFeel.NativeStyle = True
    TabOrder = 23
    ClientRectBottom = 189
    ClientRectLeft = 4
    ClientRectRight = 945
    ClientRectTop = 24
    object cxTabSheet2: TcxTabSheet
      Caption = 'Classifica'#231#227'o Despesa'
      ImageIndex = 0
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 941
        Height = 165
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsItemSP
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh3KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdItemSP'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdSP'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'dDtRef'
            Footers = <>
            Width = 79
          end
          item
            EditButtons = <>
            FieldName = 'nCdCategFinanc'
            Footers = <>
            Width = 32
          end
          item
            EditButtons = <>
            FieldName = 'cNmCategFinanc'
            Footers = <>
            ReadOnly = True
            Width = 106
          end
          item
            EditButtons = <>
            FieldName = 'nCdCC'
            Footers = <>
            Width = 34
          end
          item
            EditButtons = <>
            FieldName = 'cNmCC'
            Footers = <>
            ReadOnly = True
            Width = 106
          end
          item
            EditButtons = <>
            FieldName = 'nCdUnidadeNegocio'
            Footers = <>
            Width = 31
          end
          item
            EditButtons = <>
            FieldName = 'cNmUnidadeNegocio'
            Footers = <>
            ReadOnly = True
            Width = 106
          end
          item
            EditButtons = <>
            FieldName = 'nCdProjeto'
            Footers = <>
            Width = 34
          end
          item
            EditButtons = <>
            FieldName = 'cNmProjeto'
            Footers = <>
            Width = 150
          end
          item
            EditButtons = <>
            FieldName = 'nValPagto'
            Footers = <>
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'cObs'
            Footers = <>
            Title.Alignment = taLeftJustify
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Observa'#231#227'o'
      ImageIndex = 1
      object DBMemo1: TDBMemo
        Tag = 1
        Left = 0
        Top = 0
        Width = 941
        Height = 165
        Align = alClient
        DataField = 'cOBSSP'
        DataSource = dsMaster
        TabOrder = 0
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSP'
      '      ,SP.nCdTipoSP'
      '      ,cNmTipoSP'
      '      ,SP.nCdTerceiro'
      '      ,cNmTerceiro'
      '      ,SP.nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '      ,cNrTit'
      '      ,cSerieTit'
      '      ,nValSP'
      '      ,dDtLiber'
      '      ,dDtAutor'
      '      ,dDtContab'
      '      ,dDtCancel'
      '      ,nCdLojaSP'
      '      ,SP.cOBSSP'
      '      ,IsNull((SELECT Sum(nValProvisao)'
      '                 FROM Titulo'
      
        '                WHERE Titulo.nCdSP = SP.nCdSP),0) as nValProvisa' +
        'o'
      '      ,CASE WHEN IsNull((SELECT Sum(nValProvisao)'
      '                           FROM Titulo'
      '                          WHERE Titulo.nCdSP = SP.nCdSP),0) > 0 '
      
        '            THEN ROUND((SP.nValSP / IsNull((SELECT Sum(nValProvi' +
        'sao)'
      '                                              FROM Titulo'
      
        '                                             WHERE Titulo.nCdSP ' +
        '= SP.nCdSP),0))*100-100,2)'
      '            ELSE 0'
      '       END as nValOscilacao'
      '      ,TipoSP.cFlgExigeNF'
      '  FROM SP'
      
        '       INNER JOIN TipoSP   ON TipoSP.nCdTipoSP     = SP.nCdTipoS' +
        'P'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = SP.nCdTerce' +
        'iro'
      
        '       INNER JOIN Empresa  ON Empresa.nCdEmpresa   = SP.nCdEmpre' +
        'sa'
      ' WHERE nCdSP = :nPK')
    Left = 520
    Top = 288
    object qryMasternCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryMasternCdTipoSP: TIntegerField
      FieldName = 'nCdTipoSP'
    end
    object qryMastercNmTipoSP: TStringField
      FieldName = 'cNmTipoSP'
      Size = 50
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMastercNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMastercNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 15
    end
    object qryMastercSerieTit: TStringField
      FieldName = 'cSerieTit'
      FixedChar = True
      Size = 2
    end
    object qryMasternValSP: TBCDField
      FieldName = 'nValSP'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryMastercNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryMasterdDtLiber: TDateTimeField
      FieldName = 'dDtLiber'
    end
    object qryMasterdDtAutor: TDateTimeField
      FieldName = 'dDtAutor'
    end
    object qryMasterdDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryMasterdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryMasternCdLojaSP: TIntegerField
      FieldName = 'nCdLojaSP'
    end
    object qryMastercOBSSP: TMemoField
      FieldName = 'cOBSSP'
      BlobType = ftMemo
    end
    object qryMasternValProvisao: TBCDField
      FieldName = 'nValProvisao'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
    object qryMasternValOscilacao: TBCDField
      FieldName = 'nValOscilacao'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 22
    end
    object qryMastercFlgExigeNF: TIntegerField
      FieldName = 'cFlgExigeNF'
    end
  end
  inherited dsMaster: TDataSource
    Left = 568
    Top = 288
  end
  inherited qryID: TADOQuery
    Left = 616
    Top = 288
  end
  inherited usp_ProximoID: TADOStoredProc
    Top = 344
  end
  inherited qryStat: TADOQuery
    Top = 336
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 568
    Top = 344
  end
  inherited ImageList1: TImageList
    Left = 384
  end
  object usp_AutorizaSP: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_AUTORIZA_SP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdSP'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuarioAutor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 296
    Top = 312
  end
  object usp_Cancela_AutorizaSP: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CANCELA_AUTORIZA_SP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdSP'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuarioCancel'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 336
    Top = 312
  end
  object qryParcela: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT *'
      'FROM PrazoSP'
      'WHERE nCdSP = :nPK')
    Left = 296
    Top = 280
    object qryParcelanCdPrazoSP: TAutoIncField
      FieldName = 'nCdPrazoSP'
      ReadOnly = True
    end
    object qryParcelanCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryParceladDtVenc: TDateTimeField
      DisplayLabel = 'Parcelas|Vencimento'
      FieldName = 'dDtVenc'
    end
    object qryParcelanValPagto: TBCDField
      DisplayLabel = 'Parcelas|Valor'
      FieldName = 'nValPagto'
      Precision = 12
      Size = 2
    end
  end
  object dsParcela: TDataSource
    DataSet = qryParcela
    Left = 328
    Top = 280
  end
  object qryAlcadaSP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresaAtiva'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTipoSP'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nValorSP'
        DataType = ftFloat
        Size = 1
        Value = 0.000000000000000000
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'DECLARE @nCdLoja  int'
      ''
      'SET @nCdLoja = :nCdLoja'
      ''
      'SELECT nCdAlcadaAutorizSP '
      '  FROM AlcadaAutorizSP'
      ' WHERE nCdEmpresa   = :nCdEmpresaAtiva'
      '   AND (nCdLoja     = @nCdLoja  or @nCdLoja = 0)'
      '   AND nCdUsuario   = :nCdUsuario'
      '   AND nCdTipoSP    = :nCdTipoSP'
      '   AND nValAlcada   >= :nValorSP'
      '')
    Left = 384
    Top = 432
    object qryAlcadaSPnCdAlcadaAutorizSP: TIntegerField
      FieldName = 'nCdAlcadaAutorizSP'
    end
  end
  object qryValidaTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNrTit'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 15
        Size = 15
        Value = Null
      end
      item
        Name = 'cSerieTit'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 3
        Size = 3
        Value = Null
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdSP'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT TOP 1 1'
      '  FROM SP'
      ' WHERE cNrTit       = :cNrTit'
      '   AND cSerieTit    = :cSerieTit'
      '   AND nCdTerceiro  = :nCdTerceiro'
      '   AND nCdSP       <> :nCdSP'
      '   AND dDtCancel   IS NULL'
      '   AND dDtAutor    IS NOT NULL')
    Left = 872
    Top = 200
  end
  object qryOutrosTitulos: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 0
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdSP'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'DECLARE @nCdTerceiro int'
      '       ,@nCdSP       int'
      ''
      'Set @nCdTerceiro = :nCdTerceiro'
      'Set @nCdSP       = :nCdSP'
      ''
      'SELECT Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.cNrNF'
      '      ,Titulo.dDtEmissao'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nCdSP'
      '      ,Titulo.nCdRecebimento'
      '      ,Titulo.nValTit'
      '      ,Titulo.nSaldoTit'
      '  FROM Titulo'
      '       INNER JOIN PrazoSP  ON PrazoSP.nCdSP   = @nCdSP'
      
        '                          AND PrazoSP.dDtVenc = Titulo.dDtVenc  ' +
        '                      '
      ' WHERE Titulo.nCdTerceiro  = @nCdTerceiro'
      '   AND Titulo.dDtCancel   IS NULL'
      ' ORDER BY 1'
      ''
      '')
    Left = 472
    Top = 552
    object qryOutrosTitulosnCdTitulo: TIntegerField
      DisplayLabel = 'T'#237'tulos|ID'
      FieldName = 'nCdTitulo'
    end
    object qryOutrosTituloscNrTit: TStringField
      DisplayLabel = 'T'#237'tulos|N'#250'm. T'#237't.'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryOutrosTituloscNrNF: TStringField
      DisplayLabel = 'T'#237'tulos|N'#250'm. NF'
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryOutrosTitulosdDtEmissao: TDateTimeField
      DisplayLabel = 'T'#237'tulos|Dt. Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryOutrosTitulosdDtVenc: TDateTimeField
      DisplayLabel = 'T'#237'tulos|Dt. Vencto'
      FieldName = 'dDtVenc'
    end
    object qryOutrosTitulosnCdSP: TIntegerField
      DisplayLabel = 'T'#237'tulos|N'#250'm. SP'
      FieldName = 'nCdSP'
    end
    object qryOutrosTitulosnCdRecebimento: TIntegerField
      DisplayLabel = 'T'#237'tulos|N'#250'm. Receb.'
      FieldName = 'nCdRecebimento'
    end
    object qryOutrosTitulosnValTit: TBCDField
      DisplayLabel = 'T'#237'tulos|Valor Original'
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryOutrosTitulosnSaldoTit: TBCDField
      DisplayLabel = 'T'#237'tulos|Saldo Aberto'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
  end
  object dsOutrosTitulos: TDataSource
    DataSet = qryOutrosTitulos
    Left = 520
    Top = 552
  end
  object qryProjeto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProjeto, cNmProjeto, nCdStatus'
      'FROM Projeto'
      'WHERE nCdProjeto = :nPK')
    Left = 36
    Top = 336
    object qryProjetonCdProjeto: TIntegerField
      FieldName = 'nCdProjeto'
    end
    object qryProjetocNmProjeto: TStringField
      FieldName = 'cNmProjeto'
      Size = 50
    end
    object qryProjetonCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object qryProjetoCategFinanc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProjeto'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCategFinanc'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT 1 as iRetorno'
      'FROM ProjetoCategFinanc'
      'WHERE nCdProjeto = :nCdProjeto'
      'AND nCdCategFinanc = :nCdCategFinanc')
    Left = 44
    Top = 594
    object qryProjetoCategFinanciRetorno: TIntegerField
      FieldName = 'iRetorno'
      ReadOnly = True
    end
  end
  object qryCategFinanc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CategFinanc'
      'WHERE cFlgRecDes = '#39'D'#39
      'AND nCdCategFinanc = :nPK')
    Left = 144
    Top = 408
    object qryCategFinancnCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryCategFinanccNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object qryCategFinancnCdGrupoCategFinanc: TIntegerField
      FieldName = 'nCdGrupoCategFinanc'
    end
    object qryCategFinanccFlgRecDes: TStringField
      FieldName = 'cFlgRecDes'
      FixedChar = True
      Size = 1
    end
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdCategFinanc'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdCC, cNmCC'
      'FROM CentroCusto'
      'WHERE iNivel = 3'
      'And cFlgLanc = 1'
      'AND nCdStatus = 1'
      'AND nCdCC = :nPK'
      'AND EXISTS(SELECT 1 '
      '   FROM CentroCustoCategFinanc CCCF'
      '  WHERE CCCF.nCdCategFinanc = :nCdCategFinanc'
      '    AND CCCF.nCdCC = CentroCusto.nCdCC) ')
    Left = 112
    Top = 408
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdUnidadeNegocio'
      '      ,cNmUnidadeNegocio'
      '  FROM UnidadeNegocio'
      ' WHERE nCdUnidadeNegocio = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM EmpresaUnidadeNegocio EUN'
      
        '               WHERE EUN.nCdUnidadeNegocio = UnidadeNegocio.nCdU' +
        'nidadeNegocio'
      '                 AND EUN.nCdEmpresa = :nCdEmpresa)')
    Left = 72
    Top = 408
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object qryItemSP: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryItemSPBeforePost
    OnCalcFields = qryItemSPCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ITEMSP'
      'WHERE nCdSP = :nPK')
    Left = 24
    Top = 408
    object qryItemSPnCdItemSP: TAutoIncField
      FieldName = 'nCdItemSP'
    end
    object qryItemSPnCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryItemSPdDtRef: TDateTimeField
      DisplayLabel = 'Refer.**'
      FieldName = 'dDtRef'
    end
    object qryItemSPnValPagto: TBCDField
      DisplayLabel = 'Valor'
      FieldName = 'nValPagto'
      Precision = 12
      Size = 2
    end
    object qryItemSPnCdCategFinanc: TIntegerField
      DisplayLabel = 'Categoria Financeira|C'#243'd'
      FieldName = 'nCdCategFinanc'
    end
    object qryItemSPnCdCC: TIntegerField
      DisplayLabel = 'Centro de Custo|C'#243'd'
      FieldName = 'nCdCC'
    end
    object qryItemSPnCdUnidadeNegocio: TIntegerField
      DisplayLabel = 'Unidade de Neg'#243'cio|C'#243'd'
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryItemSPcNmCategFinanc: TStringField
      DisplayLabel = 'Categoria Financeira|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCategFinanc'
      Size = 50
      Calculated = True
    end
    object qryItemSPcNmCC: TStringField
      DisplayLabel = 'Centro de Custo|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCC'
      Size = 50
      Calculated = True
    end
    object qryItemSPcNmUnidadeNegocio: TStringField
      DisplayLabel = 'Unidade de Neg'#243'cio|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
      Calculated = True
    end
    object qryItemSPcObs: TStringField
      DisplayLabel = 'Hist'#243'rico Despesa'
      FieldName = 'cObs'
      Size = 100
    end
    object qryItemSPnCdProjeto: TIntegerField
      DisplayLabel = 'Projeto|C'#243'd.'
      FieldName = 'nCdProjeto'
    end
    object qryItemSPcNmProjeto: TStringField
      DisplayLabel = 'Projeto|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmProjeto'
      ReadOnly = True
      Size = 50
      Calculated = True
    end
  end
  object dsItemSP: TDataSource
    DataSet = qryItemSP
    Left = 848
    Top = 480
  end
  object qryParametro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cParametro'
        DataType = ftString
        Precision = 15
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cValor'
      'FROM Parametro'
      'WHERE cParametro = :cParametro')
    Left = 536
    Top = 368
    object qryParametrocValor: TStringField
      FieldName = 'cValor'
      Size = 15
    end
  end
end
