inherited frmTerceiro_PosicaoReceber: TfrmTerceiro_PosicaoReceber
  Left = 82
  Top = 138
  Width = 1040
  Height = 538
  Caption = 'Posi'#231#227'o a Receber'
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 25
    Width = 1024
    Height = 477
  end
  inherited ToolBar1: TToolBar
    Width = 1024
    Height = 25
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 25
    Width = 1024
    Height = 477
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    OnChange = cxPageControl1Change
    ClientRectBottom = 473
    ClientRectLeft = 4
    ClientRectRight = 1020
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Todos'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1016
        Height = 449
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGrid1DBTableView1: TcxGridDBTableView
          DataController.DataSource = DataSource1
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Kind = skSum
              Position = spFooter
              Column = cxGrid1DBTableView1nSaldoTit
            end
            item
              Kind = skSum
              Column = cxGrid1DBTableView1nSaldoTit
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '##,##0.00'
              Kind = skSum
              FieldName = 'nSaldoTit'
              Column = cxGrid1DBTableView1nSaldoTit
            end
            item
              Format = '##,#00.00'
              Kind = skSum
              FieldName = 'nValTit'
              Column = cxGrid1DBTableView1nValTit
            end
            item
              Format = '##,#00.00'
              Kind = skSum
              FieldName = 'nValDesconto'
              Column = cxGrid1DBTableView1nValDesconto
            end
            item
              Format = '##,#00.00'
              Kind = skSum
              FieldName = 'nValJuro'
              Column = cxGrid1DBTableView1nValJuro
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnHiding = True
          OptionsCustomize.ColumnMoving = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GridLines = glVertical
          OptionsView.GroupByBox = False
          Styles.Header = frmMenu.Header
          object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTitulo'
          end
          object cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn
            DataBinding.FieldName = 'nCdEmpresa'
          end
          object cxGrid1DBTableView1nCdLojaTit: TcxGridDBColumn
            DataBinding.FieldName = 'nCdLojaTit'
          end
          object cxGrid1DBTableView1cNrNf: TcxGridDBColumn
            DataBinding.FieldName = 'cNrNf'
            Width = 78
          end
          object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
            DataBinding.FieldName = 'cNrTit'
            Width = 95
          end
          object cxGrid1DBTableView1iParcela: TcxGridDBColumn
            DataBinding.FieldName = 'iParcela'
          end
          object cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn
            DataBinding.FieldName = 'cNmEspTit'
            Width = 89
          end
          object cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn
            DataBinding.FieldName = 'dDtEmissao'
          end
          object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
            DataBinding.FieldName = 'dDtVenc'
          end
          object cxGrid1DBTableView1nValTit: TcxGridDBColumn
            DataBinding.FieldName = 'nValTit'
          end
          object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
            DataBinding.FieldName = 'nSaldoTit'
          end
          object cxGrid1DBTableView1nValJuro: TcxGridDBColumn
            DataBinding.FieldName = 'nValJuro'
          end
          object cxGrid1DBTableView1nValDesconto: TcxGridDBColumn
            DataBinding.FieldName = 'nValDesconto'
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Somente em Atraso'
      ImageIndex = 1
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 1016
        Height = 449
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView1: TcxGridDBTableView
          DataController.DataSource = DataSource1
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Kind = skSum
              Position = spFooter
              Column = cxGridDBColumn11
            end
            item
              Kind = skSum
              Column = cxGridDBColumn11
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '##,##0.00'
              Kind = skSum
              FieldName = 'nSaldoTit'
              Column = cxGridDBColumn11
            end
            item
              Format = '##,#00.00'
              Kind = skSum
              FieldName = 'nValTit'
              Column = cxGridDBColumn10
            end
            item
              Format = '##,#00.00'
              Kind = skSum
              FieldName = 'nValDesconto'
              Column = cxGridDBColumn13
            end
            item
              Format = '##,#00.00'
              Kind = skSum
              FieldName = 'nValJuro'
              Column = cxGridDBColumn12
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnHiding = True
          OptionsCustomize.ColumnMoving = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GridLines = glVertical
          OptionsView.GroupByBox = False
          Styles.Header = frmMenu.Header
          object cxGridDBColumn1: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTitulo'
          end
          object cxGridDBColumn2: TcxGridDBColumn
            DataBinding.FieldName = 'nCdEmpresa'
          end
          object cxGridDBColumn3: TcxGridDBColumn
            DataBinding.FieldName = 'nCdLojaTit'
          end
          object cxGridDBColumn4: TcxGridDBColumn
            DataBinding.FieldName = 'cNrNf'
            Width = 78
          end
          object cxGridDBColumn5: TcxGridDBColumn
            DataBinding.FieldName = 'cNrTit'
            Width = 95
          end
          object cxGridDBColumn6: TcxGridDBColumn
            DataBinding.FieldName = 'iParcela'
          end
          object cxGridDBColumn7: TcxGridDBColumn
            DataBinding.FieldName = 'cNmEspTit'
            Width = 89
          end
          object cxGridDBColumn8: TcxGridDBColumn
            DataBinding.FieldName = 'dDtEmissao'
          end
          object cxGridDBColumn9: TcxGridDBColumn
            DataBinding.FieldName = 'dDtVenc'
          end
          object cxGridDBColumn10: TcxGridDBColumn
            DataBinding.FieldName = 'nValTit'
          end
          object cxGridDBColumn11: TcxGridDBColumn
            DataBinding.FieldName = 'nSaldoTit'
          end
          object cxGridDBColumn12: TcxGridDBColumn
            DataBinding.FieldName = 'nValJuro'
          end
          object cxGridDBColumn13: TcxGridDBColumn
            DataBinding.FieldName = 'nValDesconto'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 496
    Top = 136
  end
  object qryTitulosReceber: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nSituacao'
        DataType = ftInteger
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nSituacaoTitulo int'
      ''
      'SET @nSituacaoTitulo = :nSituacao'
      ''
      'SELECT nCdTitulo'
      '      ,nCdEmpresa'
      '      ,cNrTit'
      '      ,iParcela'
      '      ,dDtEmissao'
      '      ,dDtVenc'
      '      ,nValTit'
      '      ,nSaldoTit'
      '      ,nValJuro'
      '      ,nValDesconto'
      '      ,dDtCalcJuro'
      '      ,cNrNf'
      '      ,nCdLojaTit'
      '      ,cNmEspTit'
      '  FROM TITULO'
      ' INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEspTit   '
      
        'WHERE (((dbo.fn_OnlyDate(GetDate()) > dDtVenc) AND (@nSituacaoTi' +
        'tulo = 1)) OR (@nSituacaoTitulo = 0)) '
      '      AND nSaldoTit > 0'
      '      AND cSenso = '#39'C'#39
      
        '      AND Titulo.nCdEspTit   <> Convert(int,dbo.fn_LeParametro('#39 +
        'ET-CDR'#39'))'
      '      AND nCdTerceiro =  :nCdTerceiro'
      '      AND dDtCancel IS NULL'
      'ORDER BY dDtVenc'
      '')
    Left = 600
    Top = 140
    object qryTitulosRecebernCdTitulo: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'nCdTitulo'
    end
    object qryTitulosRecebernCdEmpresa: TIntegerField
      DisplayLabel = 'Empresa'
      FieldName = 'nCdEmpresa'
    end
    object qryTitulosRecebercNrTit: TStringField
      DisplayLabel = 'Nr. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosReceberiParcela: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'iParcela'
    end
    object qryTitulosReceberdDtEmissao: TDateTimeField
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryTitulosReceberdDtVenc: TDateTimeField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'dDtVenc'
    end
    object qryTitulosRecebernValTit: TBCDField
      DisplayLabel = 'Vr. T'#237'tulo'
      FieldName = 'nValTit'
      DisplayFormat = '###,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulosRecebernSaldoTit: TBCDField
      DisplayLabel = 'Saldo T'#237'tulo'
      FieldName = 'nSaldoTit'
      DisplayFormat = '###,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulosRecebernValJuro: TBCDField
      DisplayLabel = 'Vr. Juro'
      FieldName = 'nValJuro'
      DisplayFormat = '###,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulosRecebernValDesconto: TBCDField
      DisplayLabel = 'Vr. Desconto'
      FieldName = 'nValDesconto'
      DisplayFormat = '###,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulosReceberdDtCalcJuro: TDateTimeField
      DisplayLabel = 'Dt. Calc. Juro'
      FieldName = 'dDtCalcJuro'
    end
    object qryTitulosRecebercNrNf: TStringField
      DisplayLabel = 'Nr. Nf.'
      FieldName = 'cNrNf'
      FixedChar = True
      Size = 15
    end
    object qryTitulosRecebernCdLojaTit: TIntegerField
      DisplayLabel = 'Loja'
      FieldName = 'nCdLojaTit'
    end
    object qryTitulosRecebercNmEspTit: TStringField
      DisplayLabel = 'Esp. T'#237'tulo'
      FieldName = 'cNmEspTit'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTitulosReceber
    Left = 640
    Top = 140
  end
end
