unit fGeraDoctoFiscalCompl;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxControls, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxPC, StdCtrls, ImgList, ComCtrls, ToolWin,
  ExtCtrls, Menus, DBCtrls, Mask, ADODB;

type
  TfrmGeraDoctoFiscalCompl = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    qryNotasFiscais: TADOQuery;
    PopupMenu1: TPopupMenu;
    qryTerceiro: TADOQuery;
    edtTerceiro: TMaskEdit;
    edtEmissaoFinal: TMaskEdit;
    edtNotaInicial: TMaskEdit;
    edtNotaFinal: TMaskEdit;
    edtEmissaoInicial: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit1: TDBEdit;
    dsTerceiro: TDataSource;
    ExibirDocumentoFiscal1: TMenuItem;
    qryNotasFiscaisnCdDoctoFiscal: TAutoIncField;
    qryNotasFiscaisnCdSerieFiscal: TIntegerField;
    qryNotasFiscaiscCFOP: TStringField;
    qryNotasFiscaisiNrDocto: TIntegerField;
    qryNotasFiscaisdDtEmissao: TDateTimeField;
    qryNotasFiscaisnCdTerceiro: TIntegerField;
    qryNotasFiscaiscNmTerceiro: TStringField;
    qryNotasFiscaisnValTotal: TBCDField;
    DataSource1: TDataSource;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1nCdDoctoFiscal: TcxGridDBColumn;
    cxGridDBTableView1nCdSerieFiscal: TcxGridDBColumn;
    cxGridDBTableView1cCFOP: TcxGridDBColumn;
    cxGridDBTableView1iNrDocto: TcxGridDBColumn;
    cxGridDBTableView1dDtEmissao: TcxGridDBColumn;
    cxGridDBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView1nValTotal: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    procedure ExibirDocumentoFiscal1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtTerceiroExit(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;
  //to do
  
var
  frmGeraDoctoFiscalCompl: TfrmGeraDoctoFiscalCompl;

implementation

uses fDoctoFiscal, fMenu, fLookup_Padrao, fGeracaoComplemento;

{$R *.dfm}

procedure TfrmGeraDoctoFiscalCompl.ExibirDocumentoFiscal1Click(
  Sender: TObject);
var
  objForm : TfrmDoctoFiscal;
begin
  inherited;
  objForm := TfrmDoctoFiscal.Create(nil);

  objForm.qryMaster.Close;
  objForm.qryMaster.Parameters.ParamByName('nPK').Value := qryNotasFiscaisnCdDoctoFiscal.Value;
  objForm.qryMaster.Open;

  showForm(objForm,True);

end;

procedure TfrmGeraDoctoFiscalCompl.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryNotasFiscais.Close;
  qryNotasFiscais.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(Trim(edtTerceiro.Text));
  qryNotasFiscais.Parameters.ParamByName('dDtInicial').Value  := frmMenu.ConvData(Trim(edtEmissaoInicial.Text));
  qryNotasFiscais.Parameters.ParamByName('dDtFinal').Value    := frmMenu.ConvData(Trim(edtEmissaoFinal.Text));
  qryNotasFiscais.Parameters.ParamByName('iNrInicial').Value  := frmMenu.ConvInteiro(Trim(edtNotaInicial.Text));
  qryNotasFiscais.Parameters.ParamByName('iNrFinal').Value    := frmMenu.ConvInteiro(Trim(edtNotaFinal.Text));
  qryNotasFiscais.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryNotasFiscais.Open;
end;

procedure TfrmGeraDoctoFiscalCompl.edtTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            edtTerceiro.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmGeraDoctoFiscalCompl.edtTerceiroExit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;
end;

procedure TfrmGeraDoctoFiscalCompl.cxGridDBTableView1DblClick(
  Sender: TObject);
var objNFComp : TfrmGeracaoComplemento;
begin
  inherited;

  if ((qryNotasFiscais.Active) and (qryNotasFiscais.RecordCount > 0)) then
  begin

      if (qryNotasFiscaisiNrDocto.Value = 0) then
      begin
          MensagemAlerta('Documento Fiscal n�o emitido n�o pode ser complementado.') ;
          abort ;
      end ;

      objNFComp := TfrmGeracaoComplemento.Create(nil);

      objNFComp.nCdDoctoFiscal := qryNotasFiscaisnCdDoctoFiscal.Value;
      objNFComp.cmdPreparaTemp.Execute;

      objNFComp.qryImportaItens.Close;
      objNFComp.qryImportaItens.Parameters.ParamByName('nCdDoctoFiscal').Value := qryNotasFiscaisnCdDoctoFiscal.Value;
      objNFComp.qryImportaItens.ExecSQL;

      objNFComp.qryTempItemNotaFiscal.Close;
      objNFComp.qryTempItemNotaFiscal.Open;

      objNFComp.qryCalculoImposto.Close;
      objNFComp.qryCalculoImposto.open;
      objNFComp.qryCalculoImposto.Edit;

      showForm(objNFComp,True);

  end;

end;

initialization
    RegisterClass(TfrmGeraDoctoFiscalCompl);

end.
