inherited frmAgrupamentoContabil_AddSubConta: TfrmAgrupamentoContabil_AddSubConta
  Left = 257
  Top = 336
  Width = 641
  Height = 311
  Caption = 'Adicionar SubConta'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 275
    Width = 625
    Height = 0
  end
  inherited ToolBar1: TToolBar
    Width = 625
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 625
    Height = 246
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 39
      Top = 24
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'Conta Superior'
    end
    object Label2: TLabel
      Tag = 1
      Left = 78
      Top = 48
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo'
    end
    object Label3: TLabel
      Tag = 1
      Left = 71
      Top = 72
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#225'scara'
    end
    object Label4: TLabel
      Tag = 1
      Left = 65
      Top = 96
      Width = 46
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descri'#231#227'o'
    end
    object Label5: TLabel
      Tag = 1
      Left = 5
      Top = 120
      Width = 106
      Height = 13
      Alignment = taRightJustify
      Caption = 'Descri'#231#227'o Seg. Idioma'
    end
    object Label6: TLabel
      Tag = 1
      Left = 88
      Top = 144
      Width = 23
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#237'vel'
    end
    object Label7: TLabel
      Tag = 1
      Left = 39
      Top = 224
      Width = 72
      Height = 13
      Alignment = taRightJustify
      Caption = 'Natureza SPED'
    end
    object edtMascaraSuperior: TEdit
      Tag = 1
      Left = 115
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 0
    end
    object edtMascara: TEdit
      Tag = 1
      Left = 115
      Top = 64
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object edtDescricao: TEdit
      Left = 115
      Top = 88
      Width = 500
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 3
    end
    object edtDescricaoSegIdioma: TEdit
      Left = 115
      Top = 112
      Width = 500
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 4
    end
    object edtNivel: TEdit
      Tag = 1
      Left = 115
      Top = 136
      Width = 65
      Height = 21
      TabOrder = 5
    end
    object RadioGroup1: TRadioGroup
      Left = 115
      Top = 160
      Width = 185
      Height = 49
      Caption = 'Natureza Conta'
      Columns = 2
      Items.Strings = (
        'Devedora'
        'Credora')
      TabOrder = 6
    end
    object edtTipoNaturezaSPED: TER2LookupMaskEdit
      Left = 115
      Top = 216
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 7
      Text = '         '
      CodigoLookup = 221
      QueryLookup = qryTipoNaturezaSPED
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 183
      Top = 216
      Width = 432
      Height = 21
      DataField = 'cNmTabTipoNaturezaContaSPED'
      DataSource = DataSource1
      TabOrder = 8
    end
    object edtCodigo: TMaskEdit
      Left = 115
      Top = 40
      Width = 121
      Height = 21
      TabOrder = 1
      OnChange = edtCodigoChange
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 240
      Top = 16
      Width = 377
      Height = 21
      DataField = 'cNmContaAgrupamentoContabil'
      DataSource = DataSource2
      TabOrder = 9
    end
  end
  object qryTipoNaturezaSPED: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabTipoNaturezaContaSPED'
      'WHERE nCdTabTipoNaturezaContaSPED = :nPK')
    Left = 360
    Top = 149
    object qryTipoNaturezaSPEDnCdTabTipoNaturezaContaSPED: TIntegerField
      FieldName = 'nCdTabTipoNaturezaContaSPED'
    end
    object qryTipoNaturezaSPEDcNmTabTipoNaturezaContaSPED: TStringField
      FieldName = 'cNmTabTipoNaturezaContaSPED'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTipoNaturezaSPED
    Left = 320
    Top = 144
  end
  object qryContaPai: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ContaAgrupamentoContabil'
      'WHERE nCdContaAgrupamentoContabil = :nPK')
    Left = 448
    Top = 69
    object qryContaPainCdContaAgrupamentoContabil: TIntegerField
      FieldName = 'nCdContaAgrupamentoContabil'
    end
    object qryContaPainCdContaAgrupamentoContabilPai: TIntegerField
      FieldName = 'nCdContaAgrupamentoContabilPai'
    end
    object qryContaPainCdAgrupamentoContabil: TIntegerField
      FieldName = 'nCdAgrupamentoContabil'
    end
    object qryContaPaicMascara: TStringField
      FieldName = 'cMascara'
      Size = 30
    end
    object qryContaPaicNmContaAgrupamentoContabil: TStringField
      FieldName = 'cNmContaAgrupamentoContabil'
      Size = 100
    end
    object qryContaPaicNmContaSegundoIdioma: TStringField
      FieldName = 'cNmContaSegundoIdioma'
      Size = 100
    end
    object qryContaPaiiNivel: TIntegerField
      FieldName = 'iNivel'
    end
    object qryContaPaiiSequencia: TIntegerField
      FieldName = 'iSequencia'
    end
    object qryContaPainCdTabTipoNaturezaContaSPED: TIntegerField
      FieldName = 'nCdTabTipoNaturezaContaSPED'
    end
    object qryContaPainCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryContaPaicFlgAnalSintetica: TStringField
      FieldName = 'cFlgAnalSintetica'
      FixedChar = True
      Size = 1
    end
    object qryContaPaicFlgNaturezaCredDev: TStringField
      FieldName = 'cFlgNaturezaCredDev'
      FixedChar = True
      Size = 1
    end
  end
  object qryAgrupamentoContabil: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM AgrupamentoContabil'
      'WHERE nCdAgrupamentoContabil = :nPK')
    Left = 528
    Top = 61
    object qryAgrupamentoContabilnCdAgrupamentoContabil: TIntegerField
      FieldName = 'nCdAgrupamentoContabil'
    end
    object qryAgrupamentoContabilnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryAgrupamentoContabilcNmAgrupamentoContabil: TStringField
      FieldName = 'cNmAgrupamentoContabil'
      Size = 50
    end
    object qryAgrupamentoContabilcMascara: TStringField
      FieldName = 'cMascara'
      Size = 30
    end
    object qryAgrupamentoContabilnCdTabTipoAgrupamentoContabil: TIntegerField
      FieldName = 'nCdTabTipoAgrupamentoContabil'
    end
    object qryAgrupamentoContabilnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryAgrupamentoContabilcFlgAnaliseVertical: TIntegerField
      FieldName = 'cFlgAnaliseVertical'
    end
    object qryAgrupamentoContabiliMaxNiveis: TIntegerField
      FieldName = 'iMaxNiveis'
    end
  end
  object qryContaAgrupamentoContabil: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ContaAgrupamentoContabil'
      'WHERE nCdContaAgrupamentoContabil = :nPK')
    Left = 448
    Top = 157
    object qryContaAgrupamentoContabilnCdContaAgrupamentoContabil: TIntegerField
      FieldName = 'nCdContaAgrupamentoContabil'
    end
    object qryContaAgrupamentoContabilnCdContaAgrupamentoContabilPai: TIntegerField
      FieldName = 'nCdContaAgrupamentoContabilPai'
    end
    object qryContaAgrupamentoContabilnCdAgrupamentoContabil: TIntegerField
      FieldName = 'nCdAgrupamentoContabil'
    end
    object qryContaAgrupamentoContabilcMascara: TStringField
      FieldName = 'cMascara'
      Size = 30
    end
    object qryContaAgrupamentoContabilcNmContaAgrupamentoContabil: TStringField
      FieldName = 'cNmContaAgrupamentoContabil'
      Size = 100
    end
    object qryContaAgrupamentoContabilcNmContaSegundoIdioma: TStringField
      FieldName = 'cNmContaSegundoIdioma'
      Size = 100
    end
    object qryContaAgrupamentoContabiliNivel: TIntegerField
      FieldName = 'iNivel'
    end
    object qryContaAgrupamentoContabiliSequencia: TIntegerField
      FieldName = 'iSequencia'
    end
    object qryContaAgrupamentoContabilnCdTabTipoNaturezaContaSPED: TIntegerField
      FieldName = 'nCdTabTipoNaturezaContaSPED'
    end
    object qryContaAgrupamentoContabilnCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryContaAgrupamentoContabilcFlgAnalSintetica: TStringField
      FieldName = 'cFlgAnalSintetica'
      FixedChar = True
      Size = 1
    end
    object qryContaAgrupamentoContabilcFlgNaturezaCredDev: TStringField
      FieldName = 'cFlgNaturezaCredDev'
      FixedChar = True
      Size = 1
    end
  end
  object DataSource2: TDataSource
    DataSet = qryContaPai
    Left = 304
    Top = 144
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 528
    Top = 173
  end
end
