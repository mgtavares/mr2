unit rPosicaoEstoqueSimples;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls, comObj, ExcelXP;

type
  TrptPosicaoEstoqueSimples = class(TfrmRelatorio_Padrao)
    edtLoja: TMaskEdit;
    Label1: TLabel;
    edtDepartamento: TMaskEdit;
    Label2: TLabel;
    edtCategoria: TMaskEdit;
    Label3: TLabel;
    edtSubCategoria: TMaskEdit;
    Label4: TLabel;
    edtSegmento: TMaskEdit;
    Label5: TLabel;
    edtMarca: TMaskEdit;
    Label6: TLabel;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentocNmDepartamento: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryCategoria: TADOQuery;
    qryCategoriacNmCategoria: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    qrySubCategoria: TADOQuery;
    qrySubCategoriacNmSubCategoria: TStringField;
    DBEdit4: TDBEdit;
    DataSource4: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentocNmSegmento: TStringField;
    DBEdit5: TDBEdit;
    DataSource5: TDataSource;
    qryMarca: TADOQuery;
    qryMarcacNmMarca: TStringField;
    DBEdit6: TDBEdit;
    DataSource6: TDataSource;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryMarcanCdMarca: TIntegerField;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qryCategorianCdCategoria: TAutoIncField;
    qrySegmentonCdSegmento: TAutoIncField;
    edtGrupoProduto: TMaskEdit;
    Label7: TLabel;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBEdit7: TDBEdit;
    DataSource7: TDataSource;
    RadioGroup4: TRadioGroup;
    Label8: TLabel;
    edtDiasEstoqueIni: TMaskEdit;
    Label9: TLabel;
    edtDiasEstoqueFim: TMaskEdit;
    edtReferencia: TEdit;
    Label10: TLabel;
    edtLinha: TMaskEdit;
    Label11: TLabel;
    edtColecao: TMaskEdit;
    Label12: TLabel;
    edtClasseProduto: TMaskEdit;
    Label13: TLabel;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    DBEdit8: TDBEdit;
    DataSource8: TDataSource;
    qryColecao: TADOQuery;
    qryColecaonCdColecao: TIntegerField;
    qryColecaocNmColecao: TStringField;
    DBEdit9: TDBEdit;
    DataSource9: TDataSource;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TIntegerField;
    qryClasseProdutocNmClasseProduto: TStringField;
    DBEdit10: TDBEdit;
    DataSource10: TDataSource;
    GroupBox1: TGroupBox;
    chkTotDepartamento: TCheckBox;
    chkTotCategoria: TCheckBox;
    chkTotSubCategoria: TCheckBox;
    chkTotSegmento: TCheckBox;
    chkTotGeral: TCheckBox;
    RadioGroup6: TRadioGroup;
    RadioGroup7: TRadioGroup;
    RadioGroup8: TRadioGroup;
    edtCdCampanhaPromoc: TMaskEdit;
    Label14: TLabel;
    qryCampanhaPromoc: TADOQuery;
    qryCampanhaPromocnCdCampanhaPromoc: TIntegerField;
    qryCampanhaPromoccNmCampanhaPromoc: TStringField;
    qryCampanhaPromoccFlgAtivada: TIntegerField;
    qryCampanhaPromoccFlgSuspensa: TIntegerField;
    DBEdit11: TDBEdit;
    DataSource11: TDataSource;
    RadioGroup3: TRadioGroup;
    edtGrupoEstoque: TMaskEdit;
    Label15: TLabel;
    qryGrupoEstoque: TADOQuery;
    qryGrupoEstoquenCdGrupoEstoque: TIntegerField;
    qryGrupoEstoquecNmGrupoEstoque: TStringField;
    DBEdit12: TDBEdit;
    DataSource12: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure edtLojaExit(Sender: TObject);
    procedure edtDepartamentoExit(Sender: TObject);
    procedure edtCategoriaExit(Sender: TObject);
    procedure edtSubCategoriaExit(Sender: TObject);
    procedure edtSegmentoExit(Sender: TObject);
    procedure edtMarcaExit(Sender: TObject);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDepartamentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSubCategoriaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtSegmentoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtMarcaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoProdutoExit(Sender: TObject);
    procedure edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtLinhaExit(Sender: TObject);
    procedure edtColecaoExit(Sender: TObject);
    procedure edtClasseProdutoExit(Sender: TObject);
    procedure edtCdCampanhaPromocExit(Sender: TObject);
    procedure edtCdCampanhaPromocKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLinhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtColecaoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtClasseProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoEstoqueKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoEstoqueExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoEstoqueSimples: TrptPosicaoEstoqueSimples;

implementation

uses fMenu, fLookup_Padrao, rPosicaoEstoqueSimples_view;

{$R *.dfm}

procedure TrptPosicaoEstoqueSimples.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value           := frmMenu.nCdUsuarioLogado ;
  qryCampanhaPromoc.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      edtLoja.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLoja, edtLoja.text) ;
  end
  else
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

  edtDiasEstoqueIni.Text := '0' ;
  edtDiasEstoqueFim.Text := '999999999' ;

end;

procedure TrptPosicaoEstoqueSimples.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;
  
end;

procedure TrptPosicaoEstoqueSimples.edtDepartamentoExit(Sender: TObject);
begin
  inherited;

  qryDepartamento.Close ;
  PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

  if not qryDepartamento.eof then
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;

end;

procedure TrptPosicaoEstoqueSimples.edtCategoriaExit(Sender: TObject);
begin
  inherited;

  qryCategoria.Close ;
  PosicionaQuery(qryCategoria, edtCategoria.Text) ;

  if not qryCategoria.eof then
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value ;


end;

procedure TrptPosicaoEstoqueSimples.edtSubCategoriaExit(Sender: TObject);
begin
  inherited;

  qrySubCategoria.Close ;
  PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

  if not qrySubCategoria.eof then
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
  
end;

procedure TrptPosicaoEstoqueSimples.edtSegmentoExit(Sender: TObject);
begin
  inherited;

  qrySegmento.Close ;
  PosicionaQuery(qrySegmento, edtSegmento.Text) ;
  
end;

procedure TrptPosicaoEstoqueSimples.edtMarcaExit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, edtMarca.Text) ;
  
end;

procedure TrptPosicaoEstoqueSimples.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin

            edtLoja.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLoja, edtLoja.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueSimples.edtDepartamentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(129);

        If (nPK > 0) then
        begin

            edtDepartamento.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, edtDepartamento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueSimples.edtCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit2.Text = '') then
        begin
            MensagemAlerta('Selecione um departamento.') ;
            edtDepartamento.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(46,'Categoria.nCdDepartamento = ' + edtDepartamento.Text);

        If (nPK > 0) then
        begin

            edtCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCategoria, edtCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueSimples.edtSubCategoriaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma Categoria.') ;
            edtCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(47,'SubCategoria.nCdCategoria = ' + edtCategoria.Text);

        If (nPK > 0) then
        begin

            edtSubCategoria.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySubCategoria, edtSubCategoria.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueSimples.edtSegmentoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit4.Text = '') then
        begin
            MensagemAlerta('Selecione uma SubCategoria.') ;
            edtSubCategoria.SetFocus;
            exit ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(48,'Segmento.nCdSubCategoria = ' + edtSubCategoria.Text);

        If (nPK > 0) then
        begin

            edtSegmento.Text := IntToStr(nPK) ;
            PosicionaQuery(qrySegmento, edtSegmento.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueSimples.edtMarcaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin

            edtMarca.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, edtMarca.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueSimples.edtGrupoProdutoExit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
  
end;

procedure TrptPosicaoEstoqueSimples.edtGrupoProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin

            edtGrupoProduto.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;

        end ;

    end ;

  end ;


end;

procedure TrptPosicaoEstoqueSimples.ToolButton1Click(Sender: TObject);
var
  cFiltro       : string ;
  linha, coluna,LCID, iColuna : integer;
  planilha      : variant;
  valorcampo    : string;
  objRel        : TrptPosicaoEstoqueSimples_view;
begin
  inherited;

  if (trim(edtDiasEstoqueIni.Text) = '') then
      edtDiasEstoqueIni.Text := '0' ;

  if (trim(edtDiasEstoqueFim.Text) = '') then
      edtDiasEstoqueFim.Text := '999999999' ;

  try
      strToInt(trim(edtDiasEstoqueIni.Text)) ;
  except
      MensagemErro('Dias de estoque Inicial inv�lido.') ;
      edtDiasEstoqueIni.Text := '0' ;
      edtDiasEstoqueIni.SetFocus;
      exit ;
  end ;

  try
      strToInt(trim(edtDiasEstoqueFim.Text)) ;
  except
      MensagemErro('Dias de estoque Final inv�lido.') ;
      edtDiasEstoqueFim.Text := '999999999' ;
      edtDiasEstoqueFim.SetFocus;
      exit ;
  end ;

  if (RadioGroup7.ItemIndex = 0) and (RadioGroup2.ItemIndex = 0) then
  begin
      MensagemAlerta('N�o � poss�vel destacar as promo��es com agrupamento em Produto Estruturado. Troque o agrupamento para Produto Final') ;
      exit ;
  end ;

  if (RadioGroup8.ItemIndex = 0) and (RadioGroup2.ItemIndex = 0) then
  begin
      MensagemAlerta('N�o � poss�vel filtrar as promo��es com agrupamento em Produto Estruturado. Troque o agrupamento para Produto Final') ;
      exit ;
  end ;

  objRel := TrptPosicaoEstoqueSimples_view.Create(nil);

  try
      try
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Close ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdGrupoEstoque').Value  := frmMenu.ConvInteiro(edtGrupoEstoque.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.ConvInteiro(edtLoja.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdDepartamento').Value  := frmMenu.ConvInteiro(edtDepartamento.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdCategoria').Value     := frmMenu.ConvInteiro(edtCategoria.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdSubCategoria').Value  := frmMenu.ConvInteiro(edtSubCategoria.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdSegmento').Value      := frmMenu.ConvInteiro(edtSegmento.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdMarca').Value         := frmMenu.ConvInteiro(edtMarca.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdLinha').Value         := frmMenu.ConvInteiro(edtLinha.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdColecao').Value       := frmMenu.ConvInteiro(edtColecao.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdClasseProduto').Value := frmMenu.ConvInteiro(edtClasseProduto.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@cFlgExibePreco').Value   := RadioGroup1.ItemIndex;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@cFlgNivelQuebra').Value  := RadioGroup2.ItemIndex;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdGrupoProduto').Value  := frmMenu.ConvInteiro(edtGrupoProduto.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@cFlgSoProdAtivo').Value  := RadioGroup4.ItemIndex ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@iDiasEstoqueIni').Value  := frmMenu.ConvInteiro(edtDiasEstoqueIni.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@iDiasEstoqueFim').Value  := frmMenu.ConvInteiro(edtDiasEstoqueFim.Text) ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@cReferencia').Value      := edtReferencia.Text ;
          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@nCdCampanhaPromoc').Value:= frmMenu.ConvInteiro(edtCdCampanhaPromoc.Text) ;

          if (RadioGroup7.ItemIndex = 0) then
              objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@cFlgDestPromocao').Value := 1
          else objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@cFlgDestPromocao').Value := 0 ;

          if (RadioGroup8.ItemIndex = 0) then
              objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@cFlgSoPromocao').Value   := 1
          else objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Parameters.ParamByName('@cFlgSoPromocao').Value   := 0 ;

          objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Open ;


          if (RadioGroup3.ItemIndex = 0) then
          begin

              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              objRel.SummaryBand1.Enabled := chkTotGeral.Checked;
              objRel.qrBand2.Enabled := chkTotDepartamento.Checked;
              objRel.qrBand4.Enabled := chkTotCategoria.Checked;
              objRel.qrBand6.Enabled := chkTotSubCategoria.Checked;
              objRel.qrBand7.Enabled := chkTotSegmento.Checked;

              objRel.QRGroup1.Enabled := (RadioGroup6.ItemIndex = 0) ;
              objRel.QRGroup5.Enabled := (RadioGroup6.ItemIndex = 0) ;
              objRel.QRGroup6.Enabled := (RadioGroup6.ItemIndex = 0) ;

              cFiltro := '' ;

              if (DBEdit12.Text <> '') then
                  cFiltro := cFiltro + '/ Grupo Estoque: ' + trim(edtGrupoEstoque.Text) + '-' + DbEdit12.Text ;

              if (DBEdit1.Text <> '') then
                  cFiltro := cFiltro + '/ Loja: ' + trim(edtLoja.Text) + '-' + DbEdit1.Text ;

              if (DBEdit2.Text <> '') then
                  cFiltro := cFiltro + '/ Departamento: ' + trim(edtDepartamento.Text) + '-' + DbEdit2.Text ;

              if (DBEdit3.Text <> '') then
                  cFiltro := cFiltro + '/ Categoria: ' + trim(edtCategoria.Text) + '-' + DbEdit3.Text ;

              if (DBEdit4.Text <> '') then
                  cFiltro := cFiltro + '/ SubCategoria: ' + trim(edtSubCategoria.Text) + '-' + DbEdit4.Text ;

              if (DBEdit5.Text <> '') then
                  cFiltro := cFiltro + '/ Segmento: ' + trim(edtSegmento.Text) + '-' + DbEdit5.Text ;

              if (DBEdit6.Text <> '') then
                  cFiltro := cFiltro + '/ Marca: ' + trim(edtMarca.Text) + '-' + DbEdit6.Text ;

              if (DBEdit8.Text <> '') then
                  cFiltro := cFiltro + '/ Linha: ' + trim(edtLinha.Text) + '-' + DbEdit8.Text ;

              if (DBEdit9.Text <> '') then
                  cFiltro := cFiltro + '/ Cole��o: ' + trim(edtColecao.Text) + '-' + DbEdit9.Text ;

              if (DBEdit7.Text <> '') then
                  cFiltro := cFiltro + '/ Grupo Produto: ' + trim(edtGrupoProduto.Text) + '-' + DbEdit7.Text ;

              if (DBEdit10.Text <> '') then
                  cFiltro := cFiltro + '/ Classe Produto: ' + trim(edtClasseProduto.Text) + '-' + DbEdit10.Text ;

              if (DBEdit11.Text <> '') then
                  cFiltro := cFiltro + '/ CampanhaPromoc: ' + trim(edtCdCampanhaPromoc.Text) + '-' + DbEdit11.Text ;

              if (RadioGroup1.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ Exibir Valores : Sim '
              else cFiltro := cFiltro + '/ Exibir Valores : N�o ';

              if (RadioGroup2.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ Agrupamento : Produto Estruturado '
              else cFiltro := cFiltro + '/ Agrupamento : Produto Final ' ;

              cFiltro := cFiltro + '/ Dias de Estoque: ' + trim(edtDiasEstoqueIni.Text) + ' a ' + trim(edtDiasEstoqueFim.Text) ;

              if (trim(edtReferencia.Text) <> '') then
                  cFiltro := cFiltro + '/ Refer�ncia: ' + trim(edtReferencia.Text);

              if (RadioGroup7.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ Dest. Promo��o: Sim'
              else cFiltro := cFiltro + '/ Dest. Promo��o: N�o' ;

              if (RadioGroup8.ItemIndex = 0) then
                  cFiltro := cFiltro + '/ S� em Promo��o: Sim'
              else cFiltro := cFiltro + '/ S� em Promo��o: N�o' ;

              objRel.lblFiltro1.Caption := cFiltro ;

              objRel.QuickRep1.PreviewModal;

          end
          else
          begin

              try
                  planilha:= CreateoleObject('Excel.Application');
              except
                  MensagemErro('Ocorreu um erro no processamento da planilha, talvez o aplicativo de planilhas n�o esteja instalado corretamente.') ;
                  exit ;
              end ;

              LCID := GetUserDefaultLCID;
              planilha.DisplayAlerts  := False;
              planilha.WorkBooks.add(1);
              planilha.ScreenUpdating := true;

              planilha.caption := 'Posi��o de Estoque - Resumido';

              iColuna := 1 ;

              for linha := 0 to objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.RecordCount - 1 do
              begin

                  for coluna := 1 to objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.FieldCount do
                  begin

                      if (    (objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Fields[coluna - 1].FieldName <> 'dDtUltVenda')
                          and (objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Fields[coluna - 1].FieldName <> 'nCdTempResultado')
                          and (objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Fields[coluna - 1].FieldName <> 'nCdCampanhaPromoc')) then
                      begin

                          valorcampo := objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Fields[coluna - 1].AsString;
                          planilha.cells[linha + 2,iColuna] := valorCampo;

                          if (objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Fields[coluna - 1].DataType = ftBCD) then
                          begin
                              planilha.Cells[Linha+2,iColuna].NumberFormat        := '#.##0,00' ;
                              planilha.Cells[Linha+2,iColuna].HorizontalAlignment := xlRight ;
                          end ;

                          iColuna := iColuna + 1 ;

                      end ;

                     {Range[,'G'+LineString].NumberFormat := '$#,##0.00';}

                  end;

                  iColuna := 1 ;

                  objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Next;

              end;

              iColuna := 1 ;

              for coluna := 1 to objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.FieldCount do
              begin

                  if (    (objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Fields[coluna - 1].FieldName <> 'dDtUltVenda')
                      and (objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Fields[coluna - 1].FieldName <> 'nCdTempResultado')
                      and (objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Fields[coluna - 1].FieldName <> 'nCdCampanhaPromoc')) then
                  begin

                      valorcampo := objRel.SPREL_POSICAO_ESTOQUE_RESUMIDO.Fields[coluna - 1].DisplayLabel;
                      planilha.cells[1,iColuna] := valorcampo;

                      iColuna := iColuna + 1 ;

                  end ;

              end;

              planilha.columns.Autofit;
              planilha.Visible := True;


          end ;

      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TrptPosicaoEstoqueSimples.edtLinhaExit(Sender: TObject);
begin
  inherited;

  qryLinha.Close;
  PosicionaQuery(qryLinha, edtLinha.Text) ;
  
end;

procedure TrptPosicaoEstoqueSimples.edtColecaoExit(Sender: TObject);
begin
  inherited;

  qryColecao.Close;
  PosicionaQuery(qryColecao, edtColecao.Text) ;
  
end;

procedure TrptPosicaoEstoqueSimples.edtClasseProdutoExit(Sender: TObject);
begin
  inherited;

  qryClasseProduto.Close;
  PosicionaQuery(qryClasseProduto, edtClasseProduto.Text) ;
  
end;

procedure TrptPosicaoEstoqueSimples.edtCdCampanhaPromocExit(Sender: TObject);
begin
  inherited;

  qryCampanhaPromoc.Close;
  PosicionaQuery(qryCampanhaPromoc, edtCdCampanhaPromoc.Text) ;
  
end;

procedure TrptPosicaoEstoqueSimples.edtCdCampanhaPromocKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(194);

        If (nPK > 0) then
            edtCdCampanhaPromoc.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueSimples.edtLinhaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit6.Text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(50,'nCdMarca = ' + edtMarca.Text)
        else nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
            edtLinha.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueSimples.edtColecaoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(52);

        If (nPK > 0) then
            edtColecao.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueSimples.edtClasseProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
            edtClasseProduto.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueSimples.edtGrupoEstoqueKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(85);

        If (nPK > 0) then
        begin

            edtGrupoEstoque.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEstoque, edtGrupoEstoque.Text) ;

        end ;

    end ;

  end ;

end;

procedure TrptPosicaoEstoqueSimples.edtGrupoEstoqueExit(Sender: TObject);
begin
  inherited;

  qryGrupoEstoque.Close;
  PosicionaQuery(qryGrupoEstoque, edtGrupoEstoque.Text) ;

end;

initialization
    RegisterClass(TrptPosicaoEstoqueSimples) ;

end.
