unit fConsultaMovEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, Mask, StdCtrls, DBCtrls, ImgList,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, cxLookAndFeelPainters,
  cxButtons, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, comObj, ExcelXP, ER2Excel;

type
  TfrmConsultaMovEstoque = class(TfrmProcesso_Padrao)
    DBEdit10: TDBEdit;
    MaskEdit6: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit4: TMaskEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryGrupoEstoque: TADOQuery;
    dsEmpresa: TDataSource;
    dsGrupoEstoque: TDataSource;
    dsResultado: TDataSource;
    qryGrupoProduto: TADOQuery;
    dsGrupoProduto: TDataSource;
    Label5: TLabel;
    Label2: TLabel;
    qryGrupoEstoquenCdGrupoEstoque: TIntegerField;
    qryGrupoEstoquecNmGrupoEstoque: TStringField;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    qryGrupoProdutocFlgExpPDV: TIntegerField;
    qryResultado: TADOQuery;
    qryProduto: TADOQuery;
    dsProduto: TDataSource;
    qryResultadonCdProduto: TIntegerField;
    qryResultadocNmProduto: TStringField;
    qryResultadodDtMovto: TDateTimeField;
    qryResultadocNmLocalEstoque: TStringField;
    qryResultadonCdOperacaoEstoque: TIntegerField;
    qryResultadocNmOperacaoEstoque: TStringField;
    qryResultadonSaldoAnterior: TBCDField;
    qryResultadonQtde: TBCDField;
    qryResultadonSaldoPosterior: TBCDField;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryOperacaoEstoque: TADOQuery;
    dsOperacaoEstoque: TDataSource;
    qryOperacaoEstoquenCdOperacaoEstoque: TIntegerField;
    qryOperacaoEstoquecNmOperacaoEstoque: TStringField;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DataSource1: TDataSource;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label1: TLabel;
    edtCdProduto: TMaskEdit;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit5: TMaskEdit;
    MaskEdit7: TMaskEdit;
    MaskEdit8: TMaskEdit;
    DBEdit5: TDBEdit;
    CheckBox1: TCheckBox;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1dDtMovto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmGrupoProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmOperacaoEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoAnterior: TcxGridDBColumn;
    cxGrid1DBTableView1nQtde: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoPosterior: TcxGridDBColumn;
    cxGrid1DBTableView1cOBS: TcxGridDBColumn;
    qryResultadocNmGrupoProduto: TStringField;
    edtCdLoja: TMaskEdit;
    Label9: TLabel;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    qryResultadonCdLoja: TStringField;
    cxGrid1DBTableView1DBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView1DBColumn2: TcxGridDBColumn;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    qryResultadoPlanilha: TADOQuery;
    qryResultadoPlanilhanCdProduto: TIntegerField;
    qryResultadoPlanilhacNmProduto: TStringField;
    qryResultadoPlanilhadDtMovto: TDateTimeField;
    qryResultadoPlanilhacNmLocalEstoque: TStringField;
    qryResultadoPlanilhanCdLoja: TStringField;
    qryResultadoPlanilhanCdOperacaoEstoque: TIntegerField;
    qryResultadoPlanilhacNmOperacaoEstoque: TStringField;
    qryResultadoPlanilhanSaldoAnterior: TBCDField;
    qryResultadoPlanilhanQtde: TBCDField;
    qryResultadoPlanilhanSaldoPosterior: TBCDField;
    qryResultadoPlanilhacOBS: TStringField;
    qryResultadoPlanilhacNmGrupoProduto: TStringField;
    qryResultadoPlanilhanTotalSaidas: TBCDField;
    qryResultadoPlanilhanTotalEntradas: TBCDField;
    ER2Excel: TER2Excel;
    qryResultadocOBS: TStringField;
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtCdProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCdProdutoExit(Sender: TObject);
    procedure MaskEdit2Exit(Sender: TObject);
    procedure MaskEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit5KeyPress(Sender: TObject; var Key: Char);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCdLojaExit(Sender: TObject);
    procedure edtCdLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaMovEstoque: TfrmConsultaMovEstoque;

implementation

uses fLookup_Padrao, fMenu, rFichaKardex_view;

{$R *.dfm}

procedure TfrmConsultaMovEstoque.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryGrupoEstoque.Close ;
  PosicionaQuery(qryGrupoEstoque, MaskEdit4.Text) ;

end;

procedure TfrmConsultaMovEstoque.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, MaskEdit6.Text) ;

end;

procedure TfrmConsultaMovEstoque.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(85);

        If (nPK > 0) then
        begin
            MaskEdit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEstoque, MaskEdit4.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaMovEstoque.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin
            MaskEdit6.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, MaskEdit6.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaMovEstoque.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (trim(MaskEdit7.Text) = '/  /') then
  begin
      MaskEdit7.Text := DateToStr(Date-30) ;
  end ;

  if (trim(MaskEdit5.Text) = '/  /') then
  begin
      MaskEdit5.Text := DateToStr(Date) ;
  end ;

  qryResultado.Close ;
  qryResultado.Parameters.ParamByName('nCdEmpresa').Value         := frmMenu.nCdEmpresaAtiva;
  qryResultado.Parameters.ParamByName('nCdGrupoEstoque').Value    := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  qryResultado.Parameters.ParamByName('nCdGrupoProduto').Value    := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  qryResultado.Parameters.ParamByName('nCdProduto').Value         := frmMenu.ConvInteiro(edtCdProduto.Text) ;
  qryResultado.Parameters.ParamByName('nCdOperacaoEstoque').Value := frmMenu.ConvInteiro(MaskEdit2.Text) ;
  qryResultado.Parameters.ParamByName('nCdLocalEstoque').Value    := frmMenu.ConvInteiro(MaskEdit8.Text) ;
  qryResultado.Parameters.ParamByName('dDtInicial').Value         := frmMenu.ConvData(MaskEdit7.Text) ;
  qryResultado.Parameters.ParamByName('dDtFinal').Value           := frmMenu.ConvData(MaskEdit5.Text) ;
  qryResultado.Parameters.ParamByName('nCdLoja').Value            := frmMenu.ConvInteiro(edtCdLoja.Text);
  qryResultado.Parameters.ParamByName('cFlgUltInvent').Value      := 0 ;

  if (checkBox1.Checked) and (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Para verificar as movimenta��es a partir do �ltimo invent�rio, selecione um produto espec�fico.') ;
      edtCdProduto.SetFocus ;
      exit ;
  end ;

  if (checkBox1.Checked) and (DBEdit5.Text = '') then
  begin
      MensagemAlerta('Para verificar as movimenta��es a partir do �ltimo invent�rio, selecione um local de estoque.') ;
      MaskEdit8.SetFocus ;
      exit ;
  end ;

  if (checkBox1.Checked) then
      qryResultado.Parameters.ParamByName('cFlgUltInvent').Value      := 1 ;

  qryResultado.Open ;

  if qryResultado.eof then
  begin
      ShowMessage('Nenhuma movimenta��o para o crit�rio selecionado.') ;
      exit ;
  end ;

  MaskEdit7.Text := DateToStr(qryResultadodDtMovto.Value) ;

end;

procedure TfrmConsultaMovEstoque.edtCdProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(76,'Produto.nCdTabTipoProduto <> 4');

        If (nPK > 0) then
        begin
            edtCdProduto.Text := IntToStr(nPK) ;
            PosicionaQuery(qryProduto, edtCdProduto.Text) ;
        end ;

    end ;

  end ;
end;

procedure TfrmConsultaMovEstoque.edtCdProdutoExit(Sender: TObject);
begin
  inherited;
  qryProduto.Close ;
  PosicionaQuery(qryProduto, edtCdProduto.Text) ;

end;

procedure TfrmConsultaMovEstoque.MaskEdit2Exit(Sender: TObject);
begin
  inherited;
  qryOperacaoEstoque.Close ;
  PosicionaQuery(qryOperacaoEstoque, MaskEdit2.Text) ;
end;

procedure TfrmConsultaMovEstoque.MaskEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(84);

        If (nPK > 0) then
        begin
            MaskEdit2.Text := IntToStr(nPK) ;
            PosicionaQuery(qryOperacaoEstoque, MaskEdit2.Text) ;
        end ;

    end ;

  end ;
end;

procedure TfrmConsultaMovEstoque.MaskEdit8Exit(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Close ;
  PosicionaQuery(qryLocalEstoque, MaskEdit8.Text) ;
  
end;

procedure TfrmConsultaMovEstoque.MaskEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text <> '') then
            nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdLoja = ' + edtCdLoja.Text)
        else nPK := frmLookup_Padrao.ExecutaConsulta(87);

        If (nPK > 0) then
        begin
            MaskEdit8.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLocalEstoque, MaskEdit8.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaMovEstoque.MaskEdit5KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if (key = #13) then
      ToolButton1.Click ;

end;

procedure TfrmConsultaMovEstoque.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

{  if (key = 13) then
      MaskEdit5.Setfocus ;}
end;

procedure TfrmConsultaMovEstoque.edtCdLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, edtCdLoja.Text) ;
  
end;

procedure TfrmConsultaMovEstoque.edtCdLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
            edtCdLoja.Text := IntToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmConsultaMovEstoque.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.nCdLojaAtiva = 0) then
  begin
      edtCdLoja.ReadOnly := True ;
      edtCdLoja.Color    := $00E9E4E4 ;
  end ;

end;

procedure TfrmConsultaMovEstoque.ToolButton5Click(Sender: TObject);
var
  objRel : TrptFichaKardex_view;
begin

  objRel := TrptFichaKardex_view.Create(nil);

  try
      try
          objRel.qryResultado.Close ;
          objRel.qryResultado.Parameters.ParamByName('nCdEmpresa').Value         := frmMenu.nCdEmpresaAtiva;
          objRel.qryResultado.Parameters.ParamByName('nCdGrupoEstoque').Value    := frmMenu.ConvInteiro(MaskEdit4.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdGrupoProduto').Value    := frmMenu.ConvInteiro(MaskEdit6.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdProduto').Value         := frmMenu.ConvInteiro(edtCdProduto.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdOperacaoEstoque').Value := frmMenu.ConvInteiro(MaskEdit2.Text) ;
          objRel.qryResultado.Parameters.ParamByName('nCdLocalEstoque').Value    := frmMenu.ConvInteiro(MaskEdit8.Text) ;
          objRel.qryResultado.Parameters.ParamByName('dDtInicial').Value         := frmMenu.ConvData(MaskEdit7.Text) ;
          objRel.qryResultado.Parameters.ParamByName('dDtFinal').Value           := frmMenu.ConvData(MaskEdit5.Text) ;
          objRel.qryResultado.Parameters.ParamByName('cFlgUltInvent').Value      := 0 ;

          if (checkBox1.Checked) then
              objRel.qryResultado.Parameters.ParamByName('cFlgUltInvent').Value      := 1 ;

          objRel.qryResultado.Open ;

          if objRel.qryResultado.eof then
          begin
              ShowMessage('Nenhuma movimenta��o para o crit�rio selecionado.') ;
              exit ;
          end ;

          objRel.lblEmpresa.Caption   := frmMenu.cNmEmpresaAtiva;
          objRel.lblIntervalo.Caption := MaskEdit7.Text + ' a ' + MaskEdit5.Text ;

          objRel.QuickRep1.PreviewModal ;

      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmConsultaMovEstoque.ToolButton7Click(Sender: TObject);
var
  cNmProduto      : String;
  cNmLoja         : String;
  cNmLocalEstoque : String;
  cNmOpEstoque    : String;
  iLinha          : Integer;

begin
    qryResultadoPlanilha.SQL := qryResultado.SQL ;

    qryResultadoPlanilha.Close ;
    qryResultadoPlanilha.Parameters.ParamByName('nCdEmpresa').Value         := frmMenu.nCdEmpresaAtiva;
    qryResultadoPlanilha.Parameters.ParamByName('nCdGrupoEstoque').Value    := frmMenu.ConvInteiro(MaskEdit4.Text) ;
    qryResultadoPlanilha.Parameters.ParamByName('nCdGrupoProduto').Value    := frmMenu.ConvInteiro(MaskEdit6.Text) ;
    qryResultadoPlanilha.Parameters.ParamByName('nCdProduto').Value         := frmMenu.ConvInteiro(edtCdProduto.Text) ;
    qryResultadoPlanilha.Parameters.ParamByName('nCdOperacaoEstoque').Value := frmMenu.ConvInteiro(MaskEdit2.Text) ;
    qryResultadoPlanilha.Parameters.ParamByName('nCdLocalEstoque').Value    := frmMenu.ConvInteiro(MaskEdit8.Text) ;
    qryResultadoPlanilha.Parameters.ParamByName('dDtInicial').Value         := frmMenu.ConvData(MaskEdit7.Text) ;
    qryResultadoPlanilha.Parameters.ParamByName('dDtFinal').Value           := frmMenu.ConvData(MaskEdit5.Text) ;
    qryResultadoPlanilha.Parameters.ParamByName('nCdLoja').Value            := frmMenu.ConvInteiro(edtCdLoja.Text);
    qryResultadoPlanilha.Parameters.ParamByName('cFlgUltInvent').Value      := 0 ;

    if (checkBox1.Checked) and (DBEdit2.Text = '') then
    begin
        MensagemAlerta('Para verificar as movimenta��es a partir do �ltimo invent�rio, selecione um produto espec�fico.') ;
        edtCdProduto.SetFocus ;
        exit ;
    end ;

    if (checkBox1.Checked) and (DBEdit5.Text = '') then
    begin
        MensagemAlerta('Para verificar as movimenta��es a partir do �ltimo invent�rio, selecione um local de estoque.') ;
        MaskEdit8.SetFocus ;
        exit ;
    end ;

    if (checkBox1.Checked) then
        qryResultadoPlanilha.Parameters.ParamByName('cFlgUltInvent').Value := 1 ;

    qryResultadoPlanilha.Open;

    if qryResultadoPlanilha.eof then
    begin
        MensagemAlerta('Nenhum resultado para o crit�rio utilizado.') ;
        exit;
    end;

    if (Trim(DBEdit2.Text) <> '') then
        cNmProduto := DBEdit2.Text
    else
        cNmProduto := 'Todos';

    if (Trim(DBEdit3.Text) <> '') then
        cNmLoja := DBEdit3.Text
    else
        cNmLoja := 'Todas';

    if (Trim(DBEdit5.Text) <> '') then
        cNmLocalEstoque := DBEdit5.Text
    else
        cNmLocalEstoque := 'Todos';

    if (Trim(DBEdit4.Text) <> '') then
        cNmOpEstoque := DBEdit4.Text
    else
        cNmOpEstoque := 'Todos';

    { -- formata c�lulas -- }
    ER2Excel.Celula['A1'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['A2'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['A3'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['A4'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['A5'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['E5'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['K5'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['A6'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['A7'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['A8'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['I8'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['K8'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['M8'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['N8'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['R8'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['T8'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['U8'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['W8'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['Y8'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['AA8'].Background  := RGB(221, 221, 221);
    ER2Excel.Celula['AC8'].Background := RGB(221, 221, 221);

    ER2Excel.Celula['A1'].Mesclar('AD1');
    ER2Excel.Celula['A2'].Mesclar('AD2');
    ER2Excel.Celula['A3'].Mesclar('AD3');
    ER2Excel.Celula['A4'].Mesclar('AD4');
    ER2Excel.Celula['A5'].Mesclar('D5');
    ER2Excel.Celula['E5'].Mesclar('J5');
    ER2Excel.Celula['K5'].Mesclar('AD5');
    ER2Excel.Celula['A6'].Mesclar('AD6');
    ER2Excel.Celula['A7'].Mesclar('AD7');
    ER2Excel.Celula['A8'].Mesclar('H8');
    ER2Excel.Celula['I8'].Mesclar('J8');
    ER2Excel.Celula['K8'].Mesclar('L8');
    ER2Excel.Celula['N8'].Mesclar('Q8');
    ER2Excel.Celula['R8'].Mesclar('S8');
    ER2Excel.Celula['U8'].Mesclar('V8');
    ER2Excel.Celula['W8'].Mesclar('X8');
    ER2Excel.Celula['Y8'].Mesclar('Z8');
    ER2Excel.Celula['AA8'].Mesclar('AB8');
    ER2Excel.Celula['AC8'].Mesclar('AD8');

    //ER2Excel.Celula['A8'].Congelar('');

    frmMenu.mensagemUsuario('Exportando Planilha...');

    { -- inseri informa��es do cabe�alho -- }
    ER2Excel.Celula['A1'].Text  := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
    ER2Excel.Celula['A2'].Text  := 'Consulta Movimenta��o Estoque.';
    ER2Excel.Celula['A4'].Text  := 'Produto: ' + cNmProduto;
    ER2Excel.Celula['A5'].Text  := 'Loja: ' + cNmLoja;
    ER2Excel.Celula['E5'].Text  := 'Local de Estoque: ' + cNmLocalEstoque;
    ER2Excel.Celula['K5'].Text  := 'Opera��o Estoque: ' + cNmOpEstoque;
    ER2Excel.Celula['A6'].Text  := 'Per�odo de Movimento: ' + MaskEdit7.Text + ' at� ' + MaskEdit5.Text;
    ER2Excel.Celula['A8'].Text  := 'PRODUTO';
    ER2Excel.Celula['I8'].Text  := 'DATA MOVTO';
    ER2Excel.Celula['K8'].Text  := 'LOCAL ESTOQUE';
    ER2Excel.Celula['M8'].Text  := 'LOJA';
    ER2Excel.Celula['N8'].Text  := 'OPERA��O ESTOQUE';
    ER2Excel.Celula['R8'].Text  := 'SALDO ANTERIOR';
    ER2Excel.Celula['T8'].Text  := 'QTD.';
    ER2Excel.Celula['U8'].Text  := 'SALDO POSTERIOR';
    ER2Excel.Celula['W8'].Text  := 'OBSERVA��O';
    ER2Excel.Celula['Y8'].Text  := 'NOME GRUPO PRODUTO';
    ER2Excel.Celula['AA8'].Text := 'TOTAL SA�DAS';
    ER2Excel.Celula['AC8'].Text := 'TOTAL ENTRADAS';

    iLinha := 9;

    qryResultadoPlanilha.First;

    while (not qryResultadoPlanilha.Eof) do
    begin
        ER2Excel.Celula['A' + IntToStr(iLinha)].Text  := qryResultadoPlanilhanCdProduto.Value;
        ER2Excel.Celula['B' + IntToStr(iLinha)].Text  := qryResultadoPlanilhacNmProduto.Value;
        ER2Excel.Celula['I' + IntToStr(iLinha)].Text  := qryResultadoPlanilhadDtMovto.Value;
        ER2Excel.Celula['K' + IntToStr(iLinha)].Text  := qryResultadoPlanilhacNmLocalEstoque.Value;
        ER2Excel.Celula['M' + IntToStr(iLinha)].Text  := qryResultadoPlanilhanCdLoja.Value;
        ER2Excel.Celula['N' + IntToStr(iLinha)].Text  := qryResultadoPlanilhanCdOperacaoEstoque.Value;
        ER2Excel.Celula['O' + IntToStr(iLinha)].Text  := qryResultadoPlanilhacNmOperacaoEstoque.Value;
        ER2Excel.Celula['R' + IntToStr(iLinha)].Text  := qryResultadoPlanilhanSaldoAnterior.AsString;
        ER2Excel.Celula['T' + IntToStr(iLinha)].Text  := qryResultadoPlanilhanQtde.AsString;
        ER2Excel.Celula['U' + IntToStr(iLinha)].Text  := qryResultadoPlanilhanSaldoPosterior.AsString;
        ER2Excel.Celula['W' + IntToStr(iLinha)].Text  := qryResultadoPlanilhacOBS.Value;
        ER2Excel.Celula['Y' + IntToStr(iLinha)].Text  := qryResultadoPlanilhacNmGrupoProduto.Value;
        ER2Excel.Celula['AA' + IntToStr(iLinha)].Text := qryResultadoPlanilhanTotalSaidas.AsString;
        ER2Excel.Celula['AC' + IntToStr(iLinha)].Text := qryResultadoPlanilhanTotalEntradas.AsString;

        Inc(iLinha);

        qryResultadoPlanilha.Next;

    end;

    { -- exporta planilha e limpa result do componente -- }
    ER2Excel.ExportXLS;
    ER2Excel.CleanupInstance;

    frmMenu.mensagemUsuario('');
end;

initialization
    RegisterClass(TfrmConsultaMovEstoque) ;

end.
