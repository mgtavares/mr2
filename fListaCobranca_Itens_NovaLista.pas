unit fListaCobranca_Itens_NovaLista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DBCtrls,
  DB, ADODB, StdCtrls, Mask;

type
  TfrmListaCobranca_Itens_NovaLista = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    edtUsuario: TMaskEdit;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DataSource1: TDataSource;
    DBEdit1: TDBEdit;
    CheckBox1: TCheckBox;
    procedure edtUsuarioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtUsuarioExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CheckBox1Click(Sender: TObject);
    procedure edtUsuarioChange(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdListaCobrancaAtual : integer ;
    cGerarLista           : boolean;
  end;

var
  frmListaCobranca_Itens_NovaLista: TfrmListaCobranca_Itens_NovaLista;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmListaCobranca_Itens_NovaLista.edtUsuarioKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(197);

        If (nPK > 0) then
            edtUsuario.Text := intToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmListaCobranca_Itens_NovaLista.edtUsuarioExit(
  Sender: TObject);
begin
  inherited;
  qryUsuario.Close;
  PosicionaQuery(qryUsuario, edtUsuario.Text) ;

end;

procedure TfrmListaCobranca_Itens_NovaLista.FormShow(Sender: TObject);
begin
  inherited;

  qryUsuario.Close;
  edtUsuario.Text   := '' ;
  CheckBox1.Checked := False ;
  
  edtUsuario.SetFocus;

end;

procedure TfrmListaCobranca_Itens_NovaLista.CheckBox1Click(
  Sender: TObject);
begin
  inherited;

  if (CheckBox1.Checked) then
  begin
      CheckBox1.Checked := True ;
      qryUsuario.Close ;
      edtUsuario.Text := '' ;
  end ;

end;

procedure TfrmListaCobranca_Itens_NovaLista.edtUsuarioChange(
  Sender: TObject);
begin
  inherited;

  CheckBox1.Checked := False ;
  
end;

procedure TfrmListaCobranca_Itens_NovaLista.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (DBEdit1.Text = '') and (not CheckBox1.Checked) then
  begin
      MensagemErro('Selecione o analista de cobran�a.') ;
      edtUsuario.setFocus;
      exit ;
  end ;

  if (MessageDlg('Confirma a gera��o da nova lista de cobran�a ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  cGerarLista := true ;
  close;

end;

procedure TfrmListaCobranca_Itens_NovaLista.ToolButton2Click(
  Sender: TObject);
begin
  cGerarLista := false ;
  inherited;

end;

end.
