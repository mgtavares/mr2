unit fConsTituloAbertoWERP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, fRelatorio_Padrao,
  cxLookAndFeelPainters, cxButtons, ComObj, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGrid;

type
  TfrmConsTituloAbertoWERP = class(TfrmProcesso_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitnCdGrupoEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitcFlgPrev: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    usp_Consulta: TADOStoredProc;
    DataSource6: TDataSource;
    usp_ConsultanCdTitulo: TIntegerField;
    usp_ConsultanCdEspTit: TIntegerField;
    usp_ConsultacNmEspTit: TStringField;
    usp_ConsultanCdSP: TIntegerField;
    usp_ConsultacNrTit: TStringField;
    usp_ConsultaiParcela: TIntegerField;
    usp_ConsultanCdTerceiro: TIntegerField;
    usp_ConsultacNmTerceiro: TStringField;
    usp_ConsultacSenso: TStringField;
    usp_ConsultadDtVenc: TDateTimeField;
    usp_ConsultanValTit: TBCDField;
    usp_ConsultanSaldoTit: TBCDField;
    usp_ConsultacSiglaEmp: TStringField;
    cxButton1: TcxButton;
    usp_ConsultacNrNF: TStringField;
    usp_ConsultanCdMoeda: TIntegerField;
    usp_ConsultacSigla: TStringField;
    usp_ConsultadDtCancel: TDateTimeField;
    usp_ConsultanCdProvisaoTit: TIntegerField;
    usp_ConsultadDtPagto: TDateTimeField;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit5: TMaskEdit;
    MaskEdit6: TMaskEdit;
    Edit1: TEdit;
    Edit2: TEdit;
    usp_ConsultanCdLojaTit: TIntegerField;
    usp_ConsultanValJuro: TBCDField;
    usp_ConsultanValAbatimento: TBCDField;
    usp_ConsultanValDesconto: TBCDField;
    usp_ConsultanValLiq: TBCDField;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1nValLiq: TcxGridDBColumn;
    cxGrid1DBTableView1nValJuro: TcxGridDBColumn;
    cxGrid1DBTableView1nValAbatimento: TcxGridDBColumn;
    cxGrid1DBTableView1nValDesconto: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1cSigla: TcxGridDBColumn;
    cxGrid1DBTableView1cSenso: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsTituloAbertoWERP: TfrmConsTituloAbertoWERP;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmConsTituloAbertoWERP.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  qryMoeda.Close ;

  iAux := frmMenu.nCdEmpresaAtiva;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  if (iAux > 0) then
  begin

    MaskEdit3.Text := IntToStr(iAux) ;

    qryEmpresa.Close ;
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := iAux ;
    qryEmpresa.Open ;

  end ;

  MaskEdit6.Text := intToStr(frmMenu.nCdTerceiroUsuario) ;
  qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
  qryTerceiro.Open ;

  Maskedit5.SetFocus;


end;


procedure TfrmConsTituloAbertoWERP.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TfrmConsTituloAbertoWERP.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryEspTit.Close ;

  If (Trim(MaskEdit5.Text) <> '') then
  begin

    qryEspTit.Parameters.ParamByName('nCdEspTit').Value := MaskEdit5.Text ;
    qryEspTit.Open ;
  end ;

end;

procedure TfrmConsTituloAbertoWERP.ToolButton1Click(Sender: TObject);
begin

  usp_Consulta.Close ;
  usp_Consulta.Parameters.ParamByName('@nCdEmpresa').Value  := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  usp_Consulta.Parameters.ParamByName('@nCdEspTit').Value   := frmMenu.ConvInteiro(MaskEdit5.Text) ;
  usp_Consulta.Parameters.ParamByName('@nCdTerceiro').Value := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  usp_Consulta.Parameters.ParamByName('@cNrTit').Value      := Trim(Edit1.Text) ;
  usp_Consulta.Parameters.ParamByName('@dDtInicial').Value  := frmMenu.ConvData(MaskEdit1.Text) ;
  usp_Consulta.Parameters.ParamByName('@dDtFinal').Value    := frmMenu.ConvData(MaskEdit2.Text) ;
  usp_Consulta.Parameters.ParamByName('@cNrNF').Value       := Trim(Edit2.Text) ;
  usp_Consulta.Parameters.ParamByName('@cSenso').Value      := 'C' ;
  usp_Consulta.Parameters.ParamByName('@cProvisao').Value   := 'T' ;
  usp_Consulta.Open  ;

end;


procedure TfrmConsTituloAbertoWERP.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsTituloAbertoWERP.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(10);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsTituloAbertoWERP.cxButton1Click(Sender: TObject);
var linha, coluna : integer;
    var planilha : variant;
    var valorcampo : string;
begin
    planilha:= CreateoleObject('Excel.Application');
    planilha.WorkBooks.add(1);
    planilha.caption := 'Posi��o de t�tulos em aberto';
    planilha.visible := true;

    for linha := 0 to usp_consulta.RecordCount - 1 do
    begin
       for coluna := 1 to usp_consulta.FieldCount do
       begin
         valorcampo := usp_consulta.Fields[coluna - 1].AsString;
         planilha.cells[linha + 2,coluna] := valorCampo;
       end;
       usp_consulta.Next;
    end;
    for coluna := 1 to usp_consulta.FieldCount do
    begin
       valorcampo := usp_consulta.Fields[coluna - 1].DisplayLabel;
       planilha.cells[1,coluna] := valorcampo;
    end;
    planilha.columns.Autofit;


end;

initialization
     RegisterClass(TfrmConsTituloAbertoWERP) ;

end.
