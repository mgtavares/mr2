unit rRankingFatAnualProduto_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptRankingFatAnualProduto_view = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRDBText13: TQRDBText;
    QRCSVFilter1: TQRCSVFilter;
    QRTextFilter1: TQRTextFilter;
    QRCompositeReport1: TQRCompositeReport;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel1: TQRLabel;
    lblmes1: TQRLabel;
    lblmes3: TQRLabel;
    lblmes4: TQRLabel;
    lblmes5: TQRLabel;
    lblmes6: TQRLabel;
    lblmes7: TQRLabel;
    lblmes8: TQRLabel;
    lblmes9: TQRLabel;
    lblmes10: TQRLabel;
    lblmes11: TQRLabel;
    lblmes12: TQRLabel;
    QRLabel18: TQRLabel;
    lblmes2: TQRLabel;
    usp_RelatorioiPosicao: TIntegerField;
    usp_RelatorionCdTerceiro: TIntegerField;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatoriodDtInicial1: TDateTimeField;
    usp_RelatoriodDtFinal1: TDateTimeField;
    usp_RelatorionValAcumMes1: TFloatField;
    usp_RelatoriodDtInicial2: TDateTimeField;
    usp_RelatoriodDtFinal2: TDateTimeField;
    usp_RelatorionValAcumMes2: TFloatField;
    usp_RelatoriodDtInicial3: TDateTimeField;
    usp_RelatoriodDtFinal3: TDateTimeField;
    usp_RelatorionValAcumMes3: TFloatField;
    usp_RelatoriodDtInicial4: TDateTimeField;
    usp_RelatoriodDtFinal4: TDateTimeField;
    usp_RelatorionValAcumMes4: TFloatField;
    usp_RelatoriodDtInicial5: TDateTimeField;
    usp_RelatoriodDtFinal5: TDateTimeField;
    usp_RelatorionValAcumMes5: TFloatField;
    usp_RelatoriodDtInicial6: TDateTimeField;
    usp_RelatoriodDtFinal6: TDateTimeField;
    usp_RelatorionValAcumMes6: TFloatField;
    usp_RelatoriodDtInicial7: TDateTimeField;
    usp_RelatoriodDtFinal7: TDateTimeField;
    usp_RelatorionValAcumMes7: TFloatField;
    usp_RelatoriodDtInicial8: TDateTimeField;
    usp_RelatoriodDtFinal8: TDateTimeField;
    usp_RelatorionValAcumMes8: TFloatField;
    usp_RelatoriodDtInicial9: TDateTimeField;
    usp_RelatoriodDtFinal9: TDateTimeField;
    usp_RelatorionValAcumMes9: TFloatField;
    usp_RelatoriodDtInicial10: TDateTimeField;
    usp_RelatoriodDtFinal10: TDateTimeField;
    usp_RelatorionValAcumMes10: TFloatField;
    usp_RelatoriodDtInicial11: TDateTimeField;
    usp_RelatoriodDtFinal11: TDateTimeField;
    usp_RelatorionValAcumMes11: TFloatField;
    usp_RelatoriodDtInicial12: TDateTimeField;
    usp_RelatoriodDtFinal12: TDateTimeField;
    usp_RelatorionValAcumMes12: TFloatField;
    usp_RelatorionValTotal: TFloatField;
    usp_RelatorionVariacaoTotal: TFloatField;
    usp_RelatorionMediaMensal: TFloatField;
    usp_RelatorionPercPart: TFloatField;
    usp_RelatorionValAcumulado: TFloatField;
    usp_RelatorionPerc2: TFloatField;
    usp_RelatoriocCurva: TStringField;
    QRDBText1: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel7: TQRLabel;
    QRDBText9: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel9: TQRLabel;
    QRDBText12: TQRDBText;
    SummaryBand1: TQRBand;
    QRExpr1: TQRExpr;
    QRShape2: TQRShape;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    QRExpr11: TQRExpr;
    QRExpr12: TQRExpr;
    QRExpr13: TQRExpr;
    QRExpr14: TQRExpr;
    QRExpr15: TQRExpr;
    QRImage1: TQRImage;
    QRLabel14: TQRLabel;
    QRLabel42: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRankingFatAnualProduto_view: TrptRankingFatAnualProduto_view;

implementation

{$R *.dfm}

end.
