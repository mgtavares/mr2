unit fSaldoInicialContaBancaria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh;

type
  TfrmSaldoInicialContaBancaria = class(TfrmCadastro_Padrao)
    qryMasternCdContaBancaria: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdBanco: TIntegerField;
    qryMasternCdConta: TStringField;
    qryMasternCdCC: TIntegerField;
    qryMastercFlgFluxo: TIntegerField;
    qryMasteriUltimoCheque: TIntegerField;
    qryMasteriUltimoBordero: TIntegerField;
    qryEmpresa: TADOQuery;
    qryBanco: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresanCdTerceiroEmp: TIntegerField;
    qryEmpresanCdTerceiroMatriz: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresanCdStatus: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryBanconCdBanco: TIntegerField;
    qryBancocNmBanco: TStringField;
    qryMastercSiglaEmpresa: TStringField;
    qryMastercNmEmpresa: TStringField;
    qryMastercNmBanco: TStringField;
    qryMastercNmCC: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    qryMasternValLimiteCredito: TBCDField;
    qryMastercFlgDeposito: TIntegerField;
    qryMastercFlgEmiteCheque: TIntegerField;
    qryMastercAgencia: TIntegerField;
    qryMastercFlgEmiteBoleto: TIntegerField;
    qryMastercFlgCaixa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdUsuarioOperador: TIntegerField;
    qryMasternSaldoConta: TBCDField;
    qryMastercFlgCofre: TIntegerField;
    qryMasterdDtUltConciliacao: TDateTimeField;
    qryMastercNmTitular: TStringField;
    qryMastercFlgProvTit: TIntegerField;
    Label10: TLabel;
    DBEdit14: TDBEdit;
    qryMasternSaldoInicial: TBCDField;
    qryMasterdDtSaldoInicial: TDateTimeField;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    SP_SALDO_INICIAL_CONTA: TADOStoredProc;
    procedure FormCreate(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSaldoInicialContaBancaria: TfrmSaldoInicialContaBancaria;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmSaldoInicialContaBancaria.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CONTABANCARIA' ;
  nCdTabelaSistema  := 22 ;
  nCdConsultaPadrao := 37 ;
end;

procedure TfrmSaldoInicialContaBancaria.btSalvarClick(Sender: TObject);
begin

  if (qryMaster.eof) then
  begin
      MensagemAlerta('Nenhuma conta ativa.') ;
      abort ;
  end ;

  if (StrToFloat(qryMasternSaldoInicial.OldValue) <> 0) then
  begin
      MensagemAlerta('Esta conta j� possui saldo inicial e n�o poder� ser alterado.') ;
      abort ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      qryMaster.Post ;
      
      SP_SALDO_INICIAL_CONTA.Close ;
      SP_SALDO_INICIAL_CONTA.Parameters.ParamByName('@nCdContaBancaria').Value := qryMasternCdContaBancaria.Value ;
      SP_SALDO_INICIAL_CONTA.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
      SP_SALDO_INICIAL_CONTA.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      qryMaster.Requery();
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  btCancelar.Click;

end;

procedure TfrmSaldoInicialContaBancaria.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMastercFlgCaixa.Value = 1) and (qryMastercFlgCofre.Value = 0) then
  begin
      MensagemAlerta('N�o � permitido movimentar saldo inicial de caixas.') ;
      btCancelar.Click ;
      exit ;
  end ;

  btSalvar.Enabled := True ;

  if (qryMasternSaldoInicial.Value <> 0) then
  begin
      MensagemAlerta('Esta conta j� possui saldo inicial e n�o poder� ser alterado.') ;
      btSalvar.Enabled := False ;
  end ;

end;

procedure TfrmSaldoInicialContaBancaria.qryMasterBeforePost(
  DataSet: TDataSet);
begin

  if (qryMasternSaldoInicial.Value = 0) then
  begin
      MensagemAlerta('Informe o saldo inicial.') ;
      abort ;
  end ;

  if (qryMasterdDtSaldoInicial.AsString = '') then
  begin
      MensagemAlerta('Inform a data do saldo inicial.') ;
      abort ;
  end ;

  if (qryMasterdDtSaldoInicial.Value <= StrToDateTime(frmMenu.LeParametro('DTCONTAB'))) then
  begin
      MensagemAlerta('A data do saldo inicial n�o pode ser menor que a data da �ltima contabiliza��o.') ;
      abort ;
  end ;

  case MessageDlg('Confirma a gera��o do saldo inicial da conta ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:abort ;
  end ;

  inherited;


end;

initialization
    RegisterClass(tfrmSaldoInicialContaBancaria) ;
end.
