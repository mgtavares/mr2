inherited frmEmbFormula: TfrmEmbFormula
  Left = 90
  Top = 108
  Width = 1152
  Height = 403
  Caption = 'Composi'#231#227'o Embalagem'
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 366
    Width = 1136
    Height = 1
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    ButtonWidth = 80
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 80
    end
    inherited ToolButton2: TToolButton
      Left = 88
    end
    object ToolButton4: TToolButton
      Left = 168
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 176
      Top = 0
      Hint = 'Repete a cor da primeira coluna para todas as  demais'
      Caption = 'Repetir Cor'
      ImageIndex = 2
      ParentShowHint = False
      ShowHint = True
      OnClick = ToolButton5Click
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 337
    Align = alTop
    AllowedOperations = [alopUpdateEh]
    DataGrouping.GroupLevels = <>
    DataSource = dsGrade
    Flat = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    OnColEnter = DBGridEh1ColEnter
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdProdutoFormulado'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'iColuna'
        Footers = <>
        ReadOnly = True
        Width = 44
      end
      item
        EditButtons = <>
        FieldName = 'nCdDepartamento'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cApelidoProduto'
        Footers = <>
        Width = 52
      end
      item
        EditButtons = <>
        FieldName = 'cApelidoAmostra'
        Footers = <>
        Width = 56
      end
      item
        EditButtons = <>
        FieldName = 'nValUnitProduto'
        Footers = <>
        Width = 58
      end
      item
        EditButtons = <>
        FieldName = 'nValUnitAmostra'
        Footers = <>
        Width = 61
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeProduto'
        Footers = <>
        ReadOnly = True
        Width = 60
      end
      item
        EditButtons = <>
        FieldName = 'nQtdeAmostra'
        Footers = <>
        ReadOnly = True
        Width = 62
      end
      item
        EditButtons = <>
        FieldName = 'cNmDepartamento'
        Footers = <>
        ReadOnly = True
        Width = 268
      end
      item
        EditButtons = <>
        FieldName = 'nCdAmostra'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdFormulaColuna'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nEstoqueProduto'
        Footers = <>
        ReadOnly = True
        Width = 76
      end
      item
        EditButtons = <>
        FieldName = 'nEstoqueAmostra'
        Footers = <>
        ReadOnly = True
        Width = 77
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F6F4F400F9F7F700FEF9FA00FFFE
      FF00E7E1E2000D070800050000000A0405000A04050005090E0000000E000017
      2B0000000F0000051300000C1700000006000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFCFC0000000000FEF9FA00F5F0
      F100FFFEFF00130D0E00B2ACAD00B2ACAD00C3BBBC00A4A8B3000F2B4A003D64
      8B00416387005D7A99004C647C00000114000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      840000008400000084000000000000000000FEFCFC00F0EEEE00FFFBFC00F4EF
      F000F9F3F400150F1000E3DBDC00A29A9B00ACA4A500A6ABBA00334F7E00234B
      86001A3E74001634630010295100000322000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF00000084000000000000000000E4DFE000ABA7A600817C7B009691
      90008C84840007000000FFFEFE00FDF4F100F1E8E500D8D8E80040558C00A0BF
      FF00BCD8FF00C0D9FF0055699800000021000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000007000000120B0800090000000900
      0000251B14000C000000EFE1DB00B2A59D00B4A79F00A39CA900383E73003543
      8B00354385002D3973001420500000001D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF00000084000000000000000000201818009F969300CABFBB00C2B8
      B100ACA199001C0F0700FFFDF400FAEBE200FFFBEF00EADFE70056548200D4D9
      FF00CED2FF00D1D8FF0059618900000321000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000160E0E00F7EEEB00AEA39F00ADA3
      9C00B6ABA3000C010000F1E5DB00ACA09600A8999000AA9FA200555272004043
      6F003A3F6600595E7F005358710000000E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000007000000F9F2EF00EEE5E100F7EC
      E800F2E6E00013080000FFFFF700FCF0E600FFF5EB00F9EFEF009F9EAE00E8EA
      FF00EEF2FF00D0D5E400A7ABB60012161B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000009040300F8F1EE00B1A8A400A198
      9400B4AAA3001A110800FFFFF700E5DAD200E2D7CF00F1E9E200B7B5B500D0D2
      D300F3F5F500EDF0EE00B2B3AF00040500000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF000000840000000000000000000D080700F1ECEB00EAE3E000F4EB
      E700DDD4D000291F1800ECE2DB00FFFFF800FFFFF800EEE7DE00E0DED400F8F7
      ED00F8F8EC00F8F8EC00FFFFF500040400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000004000000EFEAE9009F9A9700BDB7
      B200B4AEA9000B0200001A110D00090100000D030000140D0400070300001C1B
      0D00050500000B0B0000060600001C1C0C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF0000008400000000000000000005010000FFFFFE00EAE5E200EBE6
      E300E6E1DE00ACA6A100F2ECE700F6F0EB00F3EDE800C0BAB300120D04007371
      6600FFFFF700FEFDEF00F9F9EB00FAFAEC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF00000000000000000002000000FFFFFE00F4F0EF00E6E2
      E100ECE8E700B5B0AD00E8E3E000E1DCD900F0EBE800BCB7B40007040000928F
      8B00FFFFFC00F9F8F400F5F5EF00FFFFFB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001C1A1900F4F2F100FFFEFD00FEFA
      F900FFFFFE00DEDAD900FFFDFC00FEFBF700FFFFFE00EBE6E700020003007F7E
      8200EDECF000FDFFFF00FDFFFF00FDFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000606060000000000000000000503
      03000E0C0C00020000000705050005030200110F0F00050204001B1A2400EDED
      F900F7F7FF00E0E1EB00FBFDFF00F7FAFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFF00000000
      FE00C00340000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000018001000000000003800100000000
      0077C00300000000007FFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object usp_Grade: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_GRADE_EMBALAGEM;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdItemPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdProdutoFormulado'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 456
    Top = 120
    object usp_GradenCdProdutoFormulado: TIntegerField
      FieldName = 'nCdProdutoFormulado'
    end
    object usp_GradeiColuna: TIntegerField
      DisplayLabel = 'Caixa|Coluna'
      FieldName = 'iColuna'
    end
    object usp_GradenCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object usp_GradecNmDepartamento: TStringField
      DisplayLabel = 'Produto|Departamento'
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object usp_GradenQtdeProduto: TBCDField
      DisplayLabel = 'Quantidades|Produtos'
      FieldName = 'nQtdeProduto'
      Precision = 12
      Size = 2
    end
    object usp_GradenQtdeAmostra: TBCDField
      DisplayLabel = 'Quantidades|Amostras'
      FieldName = 'nQtdeAmostra'
      Precision = 12
      Size = 2
    end
    object usp_GradecApelidoProduto: TStringField
      DisplayLabel = 'Apelidos|Produto'
      FieldName = 'cApelidoProduto'
      FixedChar = True
      Size = 5
    end
    object usp_GradecApelidoAmostra: TStringField
      DisplayLabel = 'Apelidos|Amostra'
      FieldName = 'cApelidoAmostra'
      FixedChar = True
      Size = 5
    end
    object usp_GradenCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object usp_GradenCdAmostra: TIntegerField
      FieldName = 'nCdAmostra'
    end
    object usp_GradenCdFormulaColuna: TIntegerField
      FieldName = 'nCdFormulaColuna'
    end
  end
  object dsGrade: TDataSource
    DataSet = qryTemp
    Left = 496
    Top = 120
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTempBeforePost
    AfterPost = qryTempAfterPost
    AfterScroll = qryTempAfterScroll
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Grade_Embalagem'#39') IS NULL)'
      'BEGIN'
      ''
      #9#9'CREATE TABLE #Temp_Grade_Embalagem (nCdProdutoFormulado int'
      #9#9#9#9#9#9#9#9#9#9'   ,iColuna             int'
      #9#9#9#9#9#9#9#9#9#9'   ,nCdDepartamento     int'
      #9#9#9#9#9#9#9#9#9#9'   ,cNmDepartamento     varchar(50)'
      #9#9#9#9#9#9#9#9#9#9'   ,nQtdeProduto        decimal(12,2)'
      #9#9#9#9#9#9#9#9#9#9'   ,nQtdeAmostra        decimal(12,2)'
      #9#9#9#9#9#9#9#9#9#9'   ,cApelidoProduto     char(5)'
      #9#9#9#9#9#9#9#9#9#9'   ,cApelidoAmostra     char(5)'
      '                       ,nValUnitProduto     decimal(12,2)'
      '                       ,nValUnitAmostra     decimal(12,2)'
      #9#9#9#9#9#9#9#9#9#9'   ,nCdProduto          int'
      #9#9#9#9#9#9#9#9#9#9'   ,nCdAmostra          int'
      #9#9#9#9#9#9#9#9#9#9'   ,nCdFormulaColuna    int'
      '                       ,nEstoqueProduto     decimal(12,2)'
      '                       ,nEstoqueAmostra     decimal(12,2)'
      '                       ,nValDescontoProd    decimal(12,2)'
      '                       ,nValDescontoAmos    decimal(12,2)'
      '                       ,nValAcrescimoProd   decimal(12,2)'
      '                       ,nValAcrescimoAmos   decimal(12,2)'
      '                       ,nValUnitarioProdEsp decimal(12,2))'
      ''
      'END'
      ''
      'SELECT *'
      'FROM #Temp_Grade_Embalagem')
    Left = 464
    Top = 160
    object qryTempnCdProdutoFormulado: TIntegerField
      FieldName = 'nCdProdutoFormulado'
    end
    object qryTempiColuna: TIntegerField
      DisplayLabel = 'Caixa|Coluna'
      FieldName = 'iColuna'
    end
    object qryTempnCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryTempcNmDepartamento: TStringField
      DisplayLabel = 'Produto|Departamento'
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object qryTempnQtdeProduto: TBCDField
      DisplayLabel = 'Quantidades|Produtos'
      FieldName = 'nQtdeProduto'
      Precision = 12
      Size = 2
    end
    object qryTempnQtdeAmostra: TBCDField
      DisplayLabel = 'Quantidades|Amostras'
      FieldName = 'nQtdeAmostra'
      Precision = 12
      Size = 2
    end
    object qryTempcApelidoProduto: TStringField
      DisplayLabel = 'Apelidos|Produto'
      FieldName = 'cApelidoProduto'
      FixedChar = True
      Size = 5
    end
    object qryTempcApelidoAmostra: TStringField
      DisplayLabel = 'Apelidos|Amostra'
      FieldName = 'cApelidoAmostra'
      FixedChar = True
      Size = 5
    end
    object qryTempnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryTempnCdAmostra: TIntegerField
      FieldName = 'nCdAmostra'
    end
    object qryTempnCdFormulaColuna: TIntegerField
      FieldName = 'nCdFormulaColuna'
    end
    object qryTempnValUnitProduto: TBCDField
      DisplayLabel = 'Valor Unit'#225'rio|Produto'
      FieldName = 'nValUnitProduto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempnValUnitAmostra: TBCDField
      DisplayLabel = 'Valor Unit'#225'rio|Amostra'
      FieldName = 'nValUnitAmostra'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempnEstoqueProduto: TBCDField
      DisplayLabel = 'Posi'#231#227'o de Estoque|Produto'
      FieldName = 'nEstoqueProduto'
      Precision = 12
      Size = 2
    end
    object qryTempnEstoqueAmostra: TBCDField
      DisplayLabel = 'Posi'#231#227'o de Estoque|Amostra'
      FieldName = 'nEstoqueAmostra'
      Precision = 12
      Size = 2
    end
    object qryTempnValDescontoProd: TBCDField
      FieldName = 'nValDescontoProd'
      Precision = 12
      Size = 2
    end
    object qryTempnValDescontoAmos: TBCDField
      FieldName = 'nValDescontoAmos'
      Precision = 12
      Size = 2
    end
    object qryTempnValAcrescimoProd: TBCDField
      FieldName = 'nValAcrescimoProd'
      Precision = 12
      Size = 2
    end
    object qryTempnValAcrescimoAmos: TBCDField
      FieldName = 'nValAcrescimoAmos'
      Precision = 12
      Size = 2
    end
    object qryTempnValUnitarioProdEsp: TBCDField
      FieldName = 'nValUnitarioProdEsp'
      Precision = 12
      Size = 2
    end
  end
  object qryFormulaColunaProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdFormulaColuna'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cFlgProdAmostra'
        DataType = ftString
        Precision = 1
        Size = 1
        Value = Null
      end
      item
        Name = 'cApelido'
        DataType = ftString
        Precision = 5
        Size = 5
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto '
      '  FROM FormulaColunaProduto'
      ' WHERE nCdFormulaColuna = :nCdFormulaColuna'
      '   AND cFlgProdAmostra  = :cFlgProdAmostra'
      '   AND cApelido         = :cApelido')
    Left = 464
    Top = 208
    object qryFormulaColunaProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
  end
  object qryZeraCodigo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      '    UPDATE #Temp_Grade_Embalagem'
      '       SET nCdProduto = CASE WHEN nCdProduto = 0 THEN NULL'
      '                             ELSE nCdProduto'
      '                        END'
      '          ,nCdAmostra = CASE WHEN nCdAmostra = 0 THEN NULL'
      '                             ELSE nCdAmostra'
      '                        END')
    Left = 504
    Top = 160
  end
  object sp_tab_preco: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_SUGERE_PRECO_TABELA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTabPrecoAux'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nValor'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nValDesconto'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nValAcrescimo'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        NumericScale = 2
        Precision = 12
        Value = Null
      end>
    Left = 356
    Top = 128
  end
  object qryPosicaoEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'DECLARE @nCdEmpresa int'
      '       ,@nCdProduto int'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      'Set @nCdProduto = :nCdProduto'
      ''
      'SELECT IsNull(Sum(nQtdeDisponivel),0)  nQtdeDisponivel'
      '      ,IsNull((SELECT Sum(nQtdeEstoqueEmpenhado)'
      '                 FROM EstoqueEmpenhado(NOLOCK)'
      '                WHERE nCdEmpresa = @nCdEmpresa'
      
        '                  AND nCdProduto = @nCdProduto),0) nQtdeEstoqueE' +
        'mpenhado'
      '  FROM PosicaoEstoque(NOLOCK)'
      
        '       INNER JOIN LocalEstoque ON LocalEstoque.nCdLocalEstoque =' +
        ' PosicaoEstoque.nCdLocalEstoque'
      ' WHERE nCdProduto      = @nCdProduto'
      '   AND cFlgEstoqueDisp = 1'
      '   AND nCdEmpresa      = @nCdEmpresa')
    Left = 544
    Top = 232
    object qryPosicaoEstoquenQtdeDisponivel: TBCDField
      FieldName = 'nQtdeDisponivel'
      ReadOnly = True
      Precision = 32
    end
    object qryPosicaoEstoquenQtdeEstoqueEmpenhado: TBCDField
      FieldName = 'nQtdeEstoqueEmpenhado'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
  end
  object qryPrecoAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdProduto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nValorInicial'
        DataType = ftFloat
        Size = 5
        Value = 1000.000000000000000000
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa     int'
      '       ,@nCdTerceiro    int'
      '       ,@nCdProduto     int'
      '       ,@nCdTabPrecoAux int'
      '       ,@nValor         decimal(12,2)'
      '       ,@nPercent       decimal(12,2)'
      '       ,@nValorInicial  decimal(12,2)'
      ''
      'Set @nCdEmpresa    = :nCdEmpresa'
      'Set @nCdTerceiro   = :nCdTerceiro'
      'Set @nCdProduto    = :nCdProduto'
      'Set @nValorInicial = :nValorInicial'
      'Set @nValor        = 0'
      'Set @nPercent      = 0'
      ''
      'Set @nCdTabPrecoAux = NULL'
      ''
      'SELECT @nCdTabPrecoAux = nCdTabPrecoAux'
      '      ,@nValor         = nValor'
      '      ,@nPercent       = nPercent'
      '  FROM TabPrecoAux'
      ' WHERE nCdEmpresa  = @nCdEmpresa'
      '   AND nCdProduto  = @nCdProduto'
      '   AND nCdTerceiro = @nCdTerceiro'
      ''
      'IF (@nCdTabPrecoAux IS NULL)'
      'BEGIN'
      ''
      #9'SELECT @nCdTabPrecoAux = nCdTabPrecoAux'
      #9#9'  ,@nValor         = nValor'
      #9#9'  ,@nPercent       = nPercent'
      #9'  FROM TabPrecoAux'
      #9' WHERE nCdEmpresa  = @nCdEmpresa'
      #9'   AND nCdProduto  = @nCdProduto'
      #9'   AND nCdTerceiro IS NULL'
      ''
      'END'
      ''
      'IF (@nCdTabPrecoAux IS NULL)'
      'BEGIN'
      ''
      #9'SELECT @nCdTabPrecoAux = nCdTabPrecoAux'
      #9#9'  ,@nValor         = nValor'
      #9#9'  ,@nPercent       = nPercent'
      #9'  FROM TabPrecoAux'
      #9' WHERE nCdEmpresa  = @nCdEmpresa'
      
        #9'   AND nCdProduto  = (SELECT nCdProdutoPai FROM Produto WHERE n' +
        'CdProduto = @nCdProduto)'
      #9'   AND nCdTerceiro = @nCdTerceiro'
      ''
      'END'
      ''
      ''
      'IF (@nCdTabPrecoAux IS NULL)'
      'BEGIN'
      ''
      #9'SELECT @nCdTabPrecoAux = nCdTabPrecoAux'
      #9#9'  ,@nValor         = nValor'
      #9#9'  ,@nPercent       = nPercent'
      #9'  FROM TabPrecoAux'
      #9' WHERE nCdEmpresa  = @nCdEmpresa'
      
        #9'   AND nCdProduto  = (SELECT nCdProdutoPai FROM Produto WHERE n' +
        'CdProduto = @nCdProduto)'
      #9'   AND nCdTerceiro IS NULL'
      ''
      'END'
      ''
      'IF (@nPercent > 0)'
      'BEGIN'
      '    Set @nValor = ROUND(@nValorInicial * (@nPercent /100),2)'
      'END'
      ''
      'IF (@nValor > @nValorInicial) Set @nValor = @nValorInicial '
      ''
      'SELECT @nValor nPrecoAux')
    Left = 392
    Top = 224
    object qryPrecoAuxnPrecoAux: TBCDField
      FieldName = 'nPrecoAux'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
  end
end
