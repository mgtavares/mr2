unit fClienteVarejoPessoaFisica;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons, Menus, ACBrBase, ACBrValidador,
  DBGridEhGrouping, ToolCtrlsEh, ACBrSocket, ACBrCEP;

type
  TfrmClienteVarejoPessoaFisica = class(TfrmCadastro_Padrao)
    qrySexo: TADOQuery;
    qrySexonCdTabTipoSexo: TIntegerField;
    qrySexocNmTabTipoSexo: TStringField;
    dsTabTipoSexo: TDataSource;
    qryEstadoCivil: TADOQuery;
    qryEstadoCivilnCdTabTipoEstadoCivil: TIntegerField;
    qryEstadoCivilcNmTabTipoEstadoCivil: TStringField;
    dsTabTipoEstadoCivil: TDataSource;
    qryEscolaridade: TADOQuery;
    dsEscolaridade: TDataSource;
    qryEscolaridadenCdTabTipoGrauEscola: TIntegerField;
    qryEscolaridadecNmTabTipoGrauEscola: TStringField;
    qryComprovanteCID: TADOQuery;
    qryComprovanteCIDnCdTabTipoComprov: TIntegerField;
    qryComprovanteCIDcNmTabTipoComprov: TStringField;
    dsComprovanteCID: TDataSource;
    qryEndereco: TADOQuery;
    qryEndereconCdEndereco: TAutoIncField;
    qryEndereconCdTerceiro: TIntegerField;
    qryEnderecocEndereco: TStringField;
    qryEnderecoiNumero: TIntegerField;
    qryEnderecocBairro: TStringField;
    qryEnderecocCidade: TStringField;
    qryEnderecocUF: TStringField;
    qryEndereconCdTipoEnd: TIntegerField;
    qryEndereconCdRegiao: TIntegerField;
    qryEnderecocTelefone: TStringField;
    qryEnderecocFax: TStringField;
    qryEndereconCdPais: TIntegerField;
    qryEndereconCdStatus: TIntegerField;
    qryEnderecocCep: TStringField;
    qryEnderecocComplemento: TStringField;
    qryEndereconCdTabTipoResidencia: TIntegerField;
    qryEnderecoiTempoResidAno: TIntegerField;
    qryEnderecoiTempoResidMes: TIntegerField;
    qryEndereconValAluguel: TBCDField;
    qryEndereconCdTabTipoComprovEnd: TIntegerField;
    qryEnderecocOBSComprov: TStringField;
    dsEndereco: TDataSource;
    qryTipoResidencia: TADOQuery;
    qryTipoResidencianCdTabTipoResidencia: TIntegerField;
    qryTipoResidenciacNmTabTipoResidencia: TStringField;
    dsTipoResidencia: TDataSource;
    qryComprovanteCRS: TADOQuery;
    qryComprovanteCRSnCdTabTipoComprov: TIntegerField;
    qryComprovanteCRScNmTabTipoComprov: TStringField;
    dsComprovanteCRS: TDataSource;
    qryClasseProf: TADOQuery;
    qryClasseProfnCdTabTipoClasseProf: TIntegerField;
    qryClasseProfcNmTabTipoClasseProf: TStringField;
    dsClasseProf: TDataSource;
    qryProfissao: TADOQuery;
    qryProfissaonCdTabTipoProfissao: TIntegerField;
    qryProfissaocNmTabTipoProfissao: TStringField;
    dsProfissao: TDataSource;
    qryComprovanteCTS: TADOQuery;
    qryComprovanteCTSnCdTabTipoComprov: TIntegerField;
    qryComprovanteCTScNmTabTipoComprov: TStringField;
    dsComprovanteCTS: TDataSource;
    qryComprovanteCRD: TADOQuery;
    dsComprovanteCRD: TDataSource;
    qryComprovanteCRDnCdTabTipoComprov: TIntegerField;
    qryComprovanteCRDcNmTabTipoComprov: TStringField;
    qryTipoRenda: TADOQuery;
    dsTipoRenda: TDataSource;
    qryTipoRendanCdTabTipoRenda: TIntegerField;
    qryTipoRendacNmTabTipoRenda: TStringField;
    qryTipoFonteRenda: TADOQuery;
    qryTipoFonteRendanCdTabTipoFonteRenda: TIntegerField;
    qryTipoFonteRendacNmTabTipoFonteRenda: TStringField;
    dsTipoFonteRenda: TDataSource;
    qryPessoaAutorizada: TADOQuery;
    qryPessoaAutorizadanCdPessoaAutorizada: TAutoIncField;
    qryPessoaAutorizadanCdTerceiro: TIntegerField;
    qryPessoaAutorizadacNmPessoaAutorizada: TStringField;
    qryPessoaAutorizadadDtNasc: TDateTimeField;
    qryPessoaAutorizadacCPF: TStringField;
    qryPessoaAutorizadacRG: TStringField;
    qryPessoaAutorizadanCdTabTipoComprovRG: TIntegerField;
    qryPessoaAutorizadacOBSRG: TStringField;
    qryPessoaAutorizadanCdTabTipoGrauParente: TIntegerField;
    qryPessoaAutorizadanCdStatus: TIntegerField;
    dsPessoaAutorizada: TDataSource;
    qryTipoComprovCIDPA: TADOQuery;
    qryPessoaAutorizadacNmTabTipoComprov: TStringField;
    qryGrauParente: TADOQuery;
    qryGrauParentenCdTabTipoGrauParente: TIntegerField;
    qryGrauParentecNmTabTipoGrauParente: TStringField;
    qryPessoaAutorizadacNmTabTipoGrauParente: TStringField;
    qryPessoaAutorizadacNmStatus: TStringField;
    qryTipoComprovCIDPAnCdTabTipoComprov: TIntegerField;
    qryTipoComprovCIDPAcNmTabTipoComprov: TStringField;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    qrySPCPessoaFisica: TADOQuery;
    qrySPCPessoaFisicanCdSPCPessoaFisica: TAutoIncField;
    qrySPCPessoaFisicanCdTerceiro: TIntegerField;
    qrySPCPessoaFisicanCdTabTipoSPC: TIntegerField;
    qrySPCPessoaFisicacFlgRestricao: TIntegerField;
    qrySPCPessoaFisicacDadosAdicionais: TStringField;
    qrySPCPessoaFisicadDtConsulta: TDateTimeField;
    qrySPCPessoaFisicanCdUsuarioConsulta: TIntegerField;
    qrySPCPessoaFisicanCdLojaConsulta: TIntegerField;
    qrySPCPessoaFisicadDtCad: TDateTimeField;
    dsSPCPessoaFisica: TDataSource;
    qrySPCPessoaFisicacNmTabTipoSPC: TStringField;
    qrySPCPessoaFisicacNmUsuarioConsulta: TStringField;
    qryTabTipoSPC: TADOQuery;
    qryTabTipoSPCnCdTabTipoSPC: TIntegerField;
    qryTabTipoSPCcNmTabTipoSPC: TStringField;
    qryUsuarioSPC: TADOQuery;
    qryUsuarioSPCnCdUsuario: TIntegerField;
    qryUsuarioSPCcNmUsuario: TStringField;
    qryUsuarioCad: TADOQuery;
    qryUsuarioCadcNmUsuario: TStringField;
    DataSource1: TDataSource;
    qryUsuarioUltAlt: TADOQuery;
    qryUsuarioUltAltcNmUsuario: TStringField;
    DataSource2: TDataSource;
    qryTabTipoOrigem: TADOQuery;
    qryTabTipoOrigemcNmTabTipoOrigem: TStringField;
    DataSource3: TDataSource;
    qryTabTipoSituacao: TADOQuery;
    qryTabTipoSituacaocNmTabTipoSituacao: TStringField;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    qryAux: TADOQuery;
    DataSource6: TDataSource;
    qryUsuarioAprova: TADOQuery;
    qryUsuarioAprovacNmUsuario: TStringField;
    DataSource7: TDataSource;
    qryTerceiroTipoTerceiro: TADOQuery;
    qryTerceiroTipoTerceironCdTerceiro: TIntegerField;
    qryTerceiroTipoTerceironCdTipoTerceiro: TIntegerField;
    qryBuscaCEP: TADOQuery;
    qryBuscaCEPcUF: TStringField;
    qryBuscaCEPcEndereco: TStringField;
    qryBuscaCEPcBairro: TStringField;
    qryBuscaCEPcCidade: TStringField;
    qryBuscaClienteCPF: TADOQuery;
    qryBuscaClienteCPFnCdTerceiro: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercIDExterno: TStringField;
    qryMastercNmTerceiro: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMasterdDtCadastro: TDateTimeField;
    qryMasternCdStatus: TIntegerField;
    qryMasteriQtdeChequeDev: TIntegerField;
    qryMasterdDtUltChequeDev: TDateTimeField;
    qryMasternValMaiorChequeDev: TBCDField;
    qryMasternValLimiteCred: TBCDField;
    qryMasternValCreditoUtil: TBCDField;
    qryMasternValPedidoAberto: TBCDField;
    qryMasternValRecAtraso: TBCDField;
    qryMasterdMaiorRecAtraso: TDateTimeField;
    qryMastercFlgBloqPedVenda: TIntegerField;
    qryMastercRG: TStringField;
    qryMastercTelefone1: TStringField;
    qryMastercTelefone2: TStringField;
    qryMastercTelefoneMovel: TStringField;
    qryMastercFax: TStringField;
    qryMastercEmail: TStringField;
    qryMasternCdLojaTerceiro: TIntegerField;
    qryMasterdDtUltNegocio: TDateTimeField;
    qryMasternValChequePendComp: TBCDField;
    qryMasternCdTerceiroPessoaFisica: TIntegerField;
    qryMasterdDtNasc: TDateTimeField;
    qryMastercUFRG: TStringField;
    qryMasternCdTabTipoComprovRG: TIntegerField;
    qryMastercOBSRG: TStringField;
    qryMasterdDtEmissaoRG: TDateTimeField;
    qryMasternCdTabTipoSexo: TIntegerField;
    qryMasternCdTabTipoEstadoCivil: TIntegerField;
    qryMasteriNrDependentes: TIntegerField;
    qryMastercNaturalidade: TStringField;
    qryMasternCdTabTipoGrauEscola: TIntegerField;
    qryMasteriDiaVenctoPref: TIntegerField;
    qryMastercNmEmpTrab: TStringField;
    qryMastercTelefoneEmpTrab: TStringField;
    qryMastercRamalEmpTrab: TStringField;
    qryMastercEnderecoEmpTrab: TStringField;
    qryMasteriNrEnderecoEmpTrab: TIntegerField;
    qryMastercBairroEmpTrab: TStringField;
    qryMastercCidadeEmpTrab: TStringField;
    qryMastercUFEmpTrab: TStringField;
    qryMastercCEPEmpTrab: TStringField;
    qryMasternCdTabTipoClasseProf: TIntegerField;
    qryMasternCdTabTipoProfissao: TIntegerField;
    qryMastercCargoTrab: TStringField;
    qryMasterdDtAdmissao: TDateTimeField;
    qryMasternCdTabTipoComprovAdm: TIntegerField;
    qryMastercOBSAdmissao: TStringField;
    qryMasternValRendaBruta: TBCDField;
    qryMasternCdTabTipoComprovRenda: TIntegerField;
    qryMastercOBSRenda: TStringField;
    qryMastercNmCjg: TStringField;
    qryMasterdDtNascCjg: TDateTimeField;
    qryMastercRGCjg: TStringField;
    qryMastercUFRGCjg: TStringField;
    qryMasternCdTabTipoComprovRGCjg: TIntegerField;
    qryMastercOBSRGCjg: TStringField;
    qryMasterdDtEmissaoRGCjg: TDateTimeField;
    qryMastercCPFCjg: TStringField;
    qryMastercTelefoneCelCjg: TStringField;
    qryMastercNmEmpresaTrabCjg: TStringField;
    qryMastercTelefoneEmpTrabCjg: TStringField;
    qryMastercRamalEmpTrabCjg: TStringField;
    qryMasternCdTabTipoClasseProfCjg: TIntegerField;
    qryMasternCdTabTipoProfissaoCjg: TIntegerField;
    qryMastercCargoTrabCjg: TStringField;
    qryMasterdDtAdmissaoCjg: TDateTimeField;
    qryMasternCdTabTipoComprovCjg: TIntegerField;
    qryMastercOBSAdmissaoCjg: TStringField;
    qryMasternValRendaBrutaCjg: TBCDField;
    qryMasternCdTabTipoComprovRendaCjg: TIntegerField;
    qryMastercOBSRendaCjg: TStringField;
    qryMastercNmRendaAdd1: TStringField;
    qryMasternValRendaAdd1: TBCDField;
    qryMasternCdTabTipoRendaAdd1: TIntegerField;
    qryMasternCdTabTipoFonteRendaAdd1: TIntegerField;
    qryMasternCdTabTipoComprovRendaAdd1: TIntegerField;
    qryMastercOBSRendaAdd1: TStringField;
    qryMastercNmRendaAdd2: TStringField;
    qryMasternValRendaAdd2: TBCDField;
    qryMasternCdTabTipoRendaAdd2: TIntegerField;
    qryMasternCdTabTipoFonteRendaAdd2: TIntegerField;
    qryMasternCdTabTipoComprovRendaAdd2: TIntegerField;
    qryMastercOBSRendaAdd2: TStringField;
    qryMastercNmRendaAdd3: TStringField;
    qryMasternValRendaAdd3: TBCDField;
    qryMasternCdTabTipoRendaAdd3: TIntegerField;
    qryMasternCdTabTipoFonteRendaAdd3: TIntegerField;
    qryMasternCdTabTipoComprovRendaAdd3: TIntegerField;
    qryMastercOBSRendaAdd3: TStringField;
    qryMastercNmRefPessoal1: TStringField;
    qryMastercTelefoneRefPessoal1: TStringField;
    qryMastercNmRefPessoal2: TStringField;
    qryMastercTelefoneRefPessoal2: TStringField;
    qryMastercNmRefPessoal3: TStringField;
    qryMastercTelefoneRefPessoal3: TStringField;
    qryMastercNmRefComercial1: TStringField;
    qryMastercRefComercial1: TStringField;
    qryMastercNmRefComercial2: TStringField;
    qryMastercRefComercial2: TStringField;
    qryMastercNmRefComercial3: TStringField;
    qryMastercRefComercial3: TStringField;
    qryMasternCdUsuarioCadastro: TIntegerField;
    qryMasternCdLojaCadastro: TIntegerField;
    qryMasternCdUsuarioUltAlt: TIntegerField;
    qryMasternCdLojaUltAlt: TIntegerField;
    qryMasterdDtUltAlt: TDateTimeField;
    qryMasternCdTabTipoOrigem: TIntegerField;
    qryMasternCdTabTipoSituacao: TIntegerField;
    qryMastercNmMae: TStringField;
    qryMastercNmPai: TStringField;
    qryMasternCdUsuarioAprova: TIntegerField;
    qryMasterdDtUsuarioAprova: TDateTimeField;
    qryMasternSaldoLimiteCredito: TBCDField;
    qryEndereconCdUsuarioCadastro: TIntegerField;
    qryEnderecodDtCadastro: TDateTimeField;
    qryEndereconCdLojaCadastro: TIntegerField;
    qryPessoaAutorizadanCdUsuarioCadastro: TIntegerField;
    qryPessoaAutorizadadDtCadastro: TDateTimeField;
    qryPessoaAutorizadanCdLojaCadastro: TIntegerField;
    qryPessoaAutorizadacNmUsuario: TStringField;
    qryUsuarioPA: TADOQuery;
    qryUsuarioPAnCdUsuario: TIntegerField;
    qryUsuarioPAcNmUsuario: TStringField;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label7: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    ComboSexo: TDBLookupComboBox;
    ComboEstadoCivil: TDBLookupComboBox;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    ComboEscolaridade: TDBLookupComboBox;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    ComboComprovCID: TDBLookupComboBox;
    DBEdit7: TDBEdit;
    DBEdit16: TDBEdit;
    qryMastercCaixaPostal: TStringField;
    qryMastercTelefoneRec1: TStringField;
    qryMastercTelefoneRec2: TStringField;
    qryMastercNmContatoRec1: TStringField;
    qryMastercNmContatoRec2: TStringField;
    qryMastercCNPJEmpTrab: TStringField;
    qryMastercComplementocEndEmpTrab: TStringField;
    qryMastercCNPJEmpTrabCjg: TStringField;
    qryMastercFlgRendaCjgLimite: TIntegerField;
    qryMastercRefComercialGeral: TStringField;
    qryMasternCdBancoRef: TIntegerField;
    qryMastercAgenciaRef: TStringField;
    qryMastercContaBancariaRef: TStringField;
    qryMasternCdTipoContaBancariaRef: TIntegerField;
    qryMastercFlgCartaoCredito: TIntegerField;
    qryMastercFlgTalaoCheque: TIntegerField;
    qryMastercNrDocOutro: TStringField;
    qryMasternCdTabTipoComprovDocOutro: TIntegerField;
    Label106: TLabel;
    DBEdit104: TDBEdit;
    Label107: TLabel;
    DBEdit105: TDBEdit;
    Label108: TLabel;
    DBEdit106: TDBEdit;
    Label109: TLabel;
    DBEdit107: TDBEdit;
    Label110: TLabel;
    DBEdit108: TDBEdit;
    DBEdit115: TDBEdit;
    Label117: TLabel;
    ComboComprovCIDOutroDoc: TDBLookupComboBox;
    Label118: TLabel;
    qryTipoContaBancaria: TADOQuery;
    dsTipoContaBancaria: TDataSource;
    qryTipoContaBancarianCdTipoContaBancaria: TIntegerField;
    qryTipoContaBancariacNmTipoContaBancaria: TStringField;
    qryMastercCepCxPostal: TStringField;
    Label121: TLabel;
    DBEdit117: TDBEdit;
    qryInformacao: TADOQuery;
    qryInformacaonCdInformacaoTerceiro: TAutoIncField;
    qryInformacaonCdTerceiro: TIntegerField;
    qryInformacaonCdLoja: TIntegerField;
    qryInformacaonCdUsuario: TIntegerField;
    qryInformacaodDtCad: TDateTimeField;
    qryInformacaocInformacao: TStringField;
    dsInformacao: TDataSource;
    qryInformacaocNmUsuario: TStringField;
    qryMasterdDtProxSPC: TDateTimeField;
    qryMasterdDtNegativacao: TDateTimeField;
    qryMastercOBSSituacaoCadastro: TStringField;
    qryBloqueios: TADOQuery;
    dsBloqueios: TDataSource;
    qryBloqueiosnCdRestricaoVendaTerceiro: TAutoIncField;
    qryBloqueiosnCdTabTipoRestricaoVenda: TIntegerField;
    qryBloqueioscNmTabTipoRestricaoVenda: TStringField;
    qryBloqueiosnCdUsuarioBloqueio: TIntegerField;
    qryBloqueiosnCdLojaBloqueio: TIntegerField;
    qryBloqueiosdDtBloqueio: TDateTimeField;
    qryBloqueioscFlgAutomatico: TIntegerField;
    qryBloqueioscObs: TStringField;
    qryBloqueioscNmUsuario: TStringField;
    qryDesbloqueados: TADOQuery;
    AutoIncField1: TAutoIncField;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    IntegerField2: TIntegerField;
    StringField2: TStringField;
    IntegerField3: TIntegerField;
    DateTimeField1: TDateTimeField;
    IntegerField4: TIntegerField;
    StringField3: TStringField;
    dsDesbloqueados: TDataSource;
    qryDesbloqueadosdDtDesbloqueio: TDateTimeField;
    qryDesbloqueadosnCdUsuarioDesbloqueio: TIntegerField;
    qryDesbloqueadosnCdLojaDesbloqueio: TIntegerField;
    qryDesbloqueadoscObsDesbloqueio: TStringField;
    qryDesbloqueadoscNmUsuarioDesb: TStringField;
    qryBloqueioscFlgLiberar: TIntegerField;
    PopupMenu1: TPopupMenu;
    Desbloquear1: TMenuItem;
    SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO: TADOStoredProc;
    ValidaDocumento: TACBrValidador;
    qryMasternPercEntrada: TBCDField;
    cxPageControl1: TcxPageControl;
    Tab1: TcxTabSheet;
    Image2: TImage;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    ComboTipoResid: TDBLookupComboBox;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    ComboComprovCRS: TDBLookupComboBox;
    DBEdit40: TDBEdit;
    btAltEndereco: TcxButton;
    btAltTelefone: TcxButton;
    btConsultaEndereco: TcxButton;
    btHistAltEndereco: TcxButton;
    Tab2: TcxTabSheet;
    Image3: TImage;
    GroupBox3: TGroupBox;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label99: TLabel;
    Label68: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    Label72: TLabel;
    Label73: TLabel;
    Label75: TLabel;
    Label71: TLabel;
    Label74: TLabel;
    Label111: TLabel;
    Label112: TLabel;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    ComboClasseProf: TDBLookupComboBox;
    comboProfissao: TDBLookupComboBox;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    DBEdit62: TDBEdit;
    DBEdit63: TDBEdit;
    DBEdit65: TDBEdit;
    ComboTipoComprovCTS: TDBLookupComboBox;
    ComboTipoComprovCRD: TDBLookupComboBox;
    btConsultaEndereco2: TcxButton;
    DBEdit109: TDBEdit;
    DBEdit110: TDBEdit;
    GroupBox4: TGroupBox;
    Label79: TLabel;
    Label80: TLabel;
    Label81: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    ComboTipoRenda1: TDBLookupComboBox;
    DBEdit64: TDBEdit;
    DBEdit70: TDBEdit;
    DBEdit71: TDBEdit;
    DBEdit72: TDBEdit;
    DBEdit73: TDBEdit;
    DBEdit74: TDBEdit;
    DBEdit75: TDBEdit;
    DBEdit76: TDBEdit;
    DBEdit77: TDBEdit;
    ComboTipoRenda2: TDBLookupComboBox;
    ComboTipoRenda3: TDBLookupComboBox;
    ComboFonteRenda1: TDBLookupComboBox;
    ComboFonteRenda2: TDBLookupComboBox;
    ComboFonteRenda3: TDBLookupComboBox;
    cxTabSheet1: TcxTabSheet;
    Image6: TImage;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label25: TLabel;
    Label56: TLabel;
    Label57: TLabel;
    Label61: TLabel;
    Label76: TLabel;
    Label77: TLabel;
    Label58: TLabel;
    Label60: TLabel;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label113: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    ComboComprovCIDCjg: TDBLookupComboBox;
    DBEdit21: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit52: TDBEdit;
    ComboClasseProfCjg: TDBLookupComboBox;
    ComboProfissaoCjg: TDBLookupComboBox;
    DBEdit50: TDBEdit;
    DBEdit51: TDBEdit;
    DBEdit53: TDBEdit;
    DBEdit54: TDBEdit;
    ComboTipoComprovCTSCjg: TDBLookupComboBox;
    ComboTipoComprovCRDCjg: TDBLookupComboBox;
    DBEdit111: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    Tab3: TcxTabSheet;
    Image4: TImage;
    GroupBox8: TGroupBox;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    DBEdit67: TDBEdit;
    DBEdit68: TDBEdit;
    DBEdit69: TDBEdit;
    DBEdit78: TDBEdit;
    DBEdit79: TDBEdit;
    DBEdit80: TDBEdit;
    GroupBox9: TGroupBox;
    Label67: TLabel;
    Label78: TLabel;
    Label119: TLabel;
    DBEdit81: TDBEdit;
    DBEdit82: TDBEdit;
    DBEdit83: TDBEdit;
    DBEdit84: TDBEdit;
    DBEdit85: TDBEdit;
    DBEdit86: TDBEdit;
    DBEdit116: TDBEdit;
    GroupBox2: TGroupBox;
    Label114: TLabel;
    Label115: TLabel;
    Label116: TLabel;
    Label120: TLabel;
    DBEdit112: TDBEdit;
    DBEdit113: TDBEdit;
    DBEdit114: TDBEdit;
    comboTipoContabancaria: TDBLookupComboBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    Tab4: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    cxTabSheet3: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    Tab5: TcxTabSheet;
    Image5: TImage;
    GroupBox11: TGroupBox;
    Label59: TLabel;
    Label66: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Label92: TLabel;
    Label93: TLabel;
    Label101: TLabel;
    Label102: TLabel;
    Label103: TLabel;
    Label104: TLabel;
    Label105: TLabel;
    Label122: TLabel;
    Label123: TLabel;
    Label124: TLabel;
    DBEdit55: TDBEdit;
    DBEdit56: TDBEdit;
    DBEdit57: TDBEdit;
    DBEdit58: TDBEdit;
    DBEdit61: TDBEdit;
    DBEdit66: TDBEdit;
    DBEdit93: TDBEdit;
    DBEdit94: TDBEdit;
    DBEdit95: TDBEdit;
    DBEdit96: TDBEdit;
    DBEdit97: TDBEdit;
    DBEdit98: TDBEdit;
    DBEdit99: TDBEdit;
    DBEdit100: TDBEdit;
    DBEdit101: TDBEdit;
    DBEdit102: TDBEdit;
    DBEdit103: TDBEdit;
    DBEdit118: TDBEdit;
    DBEdit119: TDBEdit;
    DBEdit120: TDBEdit;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    GroupBox12: TGroupBox;
    Label94: TLabel;
    Label95: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    Label98: TLabel;
    Label100: TLabel;
    Label125: TLabel;
    DBEdit87: TDBEdit;
    DBEdit88: TDBEdit;
    DBEdit89: TDBEdit;
    DBEdit90: TDBEdit;
    DBEdit91: TDBEdit;
    DBEdit92: TDBEdit;
    cxButton1: TcxButton;
    btConsParcAberto: TcxButton;
    btAltLimite: TcxButton;
    DBEdit121: TDBEdit;
    cxTabSheet4: TcxTabSheet;
    cxPageControl2: TcxPageControl;
    cxTabSheet5: TcxTabSheet;
    DBGridEh4: TDBGridEh;
    cxTabSheet6: TcxTabSheet;
    DBGridEh5: TDBGridEh;
    cxButton5: TcxButton;
    qryPessoaAutorizadadDtValidadeAutorizado: TDateTimeField;
    qryPessoaAutorizadadDtAutorizacao: TDateTimeField;
    ACBrCEP1: TACBrCEP;
    qryMastercFlgCadSimples: TIntegerField;
    tabCRM: TcxTabSheet;
    Image7: TImage;
    qryMasteriNrCalcado: TIntegerField;
    DBEdit122: TDBEdit;
    Label127: TLabel;
    Label126: TLabel;
    ComboTabTipoLogradouro: TDBLookupComboBox;
    qryTabTipoLogradouro: TADOQuery;
    dsTabTipoLogradouro: TDataSource;
    qryTabTipoLogradouronCdTabTipoLogradouro: TIntegerField;
    qryTabTipoLogradourocNmTabTipoLogradouro: TStringField;
    qryEndereconCdTabTipoLogradouro: TIntegerField;
    Label128: TLabel;
    qryGrupoCliente: TADOQuery;
    qryGrupoClientenCdGrupoClienteDesc: TIntegerField;
    qryGrupoClientecNmGrupoClienteDesc: TStringField;
    DBEdit124: TDBEdit;
    dsGrupoCliente: TDataSource;
    qryMasternCdGrupoClienteDesc: TIntegerField;
    DBEdit123: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryPessoaAutorizadaCalcFields(DataSet: TDataSet);
    procedure qryPessoaAutorizadaBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qrySPCPessoaFisicaBeforePost(DataSet: TDataSet);
    procedure qrySPCPessoaFisicaCalcFields(DataSet: TDataSet);
    procedure DBGridEh2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qrySPCPessoaFisicaBeforeDelete(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure qryEnderecoBeforePost(DataSet: TDataSet);
    procedure Tab5Show(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit28Enter(Sender: TObject);
    procedure DBEdit28Exit(Sender: TObject);
    procedure DBEdit27Exit(Sender: TObject);
    procedure DBEdit27Enter(Sender: TObject);
    procedure DesativaEndereco() ;
    procedure AtivaEndereco() ;
    procedure btAltEnderecoClick(Sender: TObject);
    procedure DBEdit46Exit(Sender: TObject);
    procedure btAltTelefoneClick(Sender: TObject);
    procedure btConsultaEnderecoClick(Sender: TObject);
    procedure btConsultaEndereco2Click(Sender: TObject);
    procedure AtivaLimite() ;
    procedure DesativaLimite() ;
    procedure qryMasterFieldChangeComplete(DataSet: TCustomADODataSet;
      const FieldCount: Integer; const Fields: OleVariant;
      const Error: Error; var EventStatus: TEventStatus);
    procedure btnAuditoriaClick(Sender: TObject);
    procedure btHistAltEnderecoClick(Sender: TObject);
    procedure DBEdit54Exit(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure DBEdit40Exit(Sender: TObject);
    procedure DBEdit77Exit(Sender: TObject);
    procedure DBCheckBox2Exit(Sender: TObject);
    procedure qryInformacaoBeforePost(DataSet: TDataSet);
    procedure cxTabSheet3Show(Sender: TObject);
    procedure qryInformacaoCalcFields(DataSet: TDataSet);
    procedure qryInformacaoBeforeEdit(DataSet: TDataSet);
    procedure qryInformacaoBeforeDelete(DataSet: TDataSet);
    procedure btConsParcAbertoClick(Sender: TObject);
    procedure DBEdit119Change(Sender: TObject);
    procedure DBEdit118Change(Sender: TObject);
    procedure DBEdit90Change(Sender: TObject);
    procedure DBEdit92Change(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure DBEdit99Change(Sender: TObject);
    procedure cxTabSheet4Show(Sender: TObject);
    procedure cxTabSheet6Show(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure Desbloquear1Click(Sender: TObject);
    procedure DBGridEh4DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure cxButton2Click(Sender: TObject);
    procedure cxTabSheet2Show(Sender: TObject);
    procedure DBEdit10Exit(Sender: TObject);
    procedure Tab4Show(Sender: TObject);
    procedure DBEdit28KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton5Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure desativaCPF() ;
    procedure ativaCPF();
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure btAltLimiteClick(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure cxButton5Click(Sender: TObject);
    procedure DBEdit123KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit123Exit(Sender: TObject);
  private
    { Private declarations }
    cCepAux         : string ;
    cEnderecoManual : string ;
    bAlteracao      : boolean ;
  public
    { Public declarations }
    bChamadaExterna : boolean ;
  end;

var
  frmClienteVarejoPessoaFisica: TfrmClienteVarejoPessoaFisica;
  cAltLimCadCli : string ;

implementation

uses fLookup_Padrao, fMenu, fConsultaEndereco, fTrilhaClienteVarejo,
  fClienteVarejoPessoaFisica_HistAltEnd, fImagemDigital,
  fCaixa_ParcelaAberto, fClienteVarejoPessoaFisica_TitulosNeg,
  fClienteVarejoPessoaFisica_HistMov,
  fClienteVarejoPessoaFisica_HistContato,
  fClienteVarejoPessoaFisica_HistoricoNeg,
  fAprovacaoClienteVarejo_LimiteInicial,
  rFichaCadastralCliente_view, DateUtils;


{$R *.dfm}

procedure TfrmClienteVarejoPessoaFisica.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CLIENTECREDIARIO' ;
  nCdTabelaSistema  := 13 ;
  nCdConsultaPadrao := 180 ;
  bCodigoAutomatico := False ;
  bAutoEdit         := False ;

end;

procedure TfrmClienteVarejoPessoaFisica.btIncluirClick(Sender: TObject);
begin
  inherited;

  bAlteracao := True ;

  qryEndereco.Close ;
  qryEndereco.Parameters.ParamByName('nPK').Value := 0 ;
  qryEndereco.Open ;
  qryEndereco.Insert ;

  DesativaEndereco() ;
  AtivaEndereco() ;

  AtivaLimite() ;

  AtivaCPF();

  qryPessoaAutorizada.Close ;
  qryPessoaAutorizada.Parameters.ParamByName('nPK').Value := 0 ;
  qryPessoaAutorizada.Open ;

  qrySPCPessoaFisica.Close ;
  qrySPCPessoaFisica.Parameters.ParamByName('nPK').Value := 0 ;
  qrySPCPessoaFisica.Open ;

  qryInformacao.Close ;
  qryInformacao.Parameters.ParamByName('nPK').Value := 0 ;
  qryInformacao.Open ;

  cxPageControl1.ActivePage := Tab1 ;

  //gera um novo c�digo para o registro
  usp_ProximoID.Close;
  usp_ProximoID.Parameters.ParamByName('@cNmTabela').Value := cNmTabelaMaster ;
  usp_ProximoID.ExecProc ;

  qryMasternCdTerceiro.Value := usp_ProximoID.Parameters.ParamByName('@iUltimoCodigo').Value ;

  if (Trim(DBEdit1.Text) = '') then
  begin
      btCancelar.Click;
      MensagemErro('Erro ao gerar o c�digo deste registro, reinicie o processo de inclus�o.');
      Abort;
  end;

  qryMastercFlgRendaCjgLimite.Value := 0 ;
  qryMastercFlgCartaoCredito.Value  := 0 ;
  qryMastercFlgTalaoCheque.Value    := 0 ;

  btAltEndereco.Visible      := False ;
  btAltTelefone.Visible      := False ;
  btConsultaEndereco.Visible := True  ;
  btHistAltEndereco.Visible  := False ;

  DBEDit3.SetFOcus ;
end;

procedure TfrmClienteVarejoPessoaFisica.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.nCdLojaAtiva = 0) and (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      MensagemAlerta('Nenhuma loja ativa.') ;
  end ;

  cEnderecoManual     := frmMenu.LeParametro('CLIPERMENDMAN') ;
  cAltLimCadCli       := frmMenu.LeParametro('ALTLIMCADCLI') ;
  btAltLimite.Enabled := (frmMenu.LeParametro('ALTLIMCADCLI') = 'S') ;

  qrySexo.Open;
  qryEstadoCivil.Open ;
  qryTabTipoLogradouro.Open;
  qryComprovanteCRS.Open ;
  qryComprovanteCID.Open ;
  qryTipoResidencia.Open ;
  qryEscolaridade.Open ;
  qryComprovanteCRD.Open ;
  qryClasseProf.Open ;
  qryProfissao.Open ;
  qryComprovanteCTS.Open ;
  qryTipoFonteRenda.Open ;
  qryTipoRenda.Open ;
  qryTipoContaBancaria.Open;

  cxPageControl1.ActivePage := Tab1 ;

end;

procedure TfrmClienteVarejoPessoaFisica.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryGrupoCliente.Close;
  PosicionaQuery(qryGrupoCliente, qryMasternCdGrupoClienteDesc.AsString);

  qryEndereco.Close ;
  PosicionaQuery(qryEndereco, qryMasternCdTerceiro.AsString);

  DesativaEndereco() ;

  if (qryMasterdDtUsuarioAprova.AsString <> '') then
  begin

      DesativaEndereco() ;
      DesativaLimite() ;
      DesativaCPF() ;

      btAltEndereco.Visible      := True ;
      btAltTelefone.Visible      := True ;
      btHistAltEndereco.Visible  := True ;

  end
  else
  begin

      AtivaEndereco() ;
      AtivaLimite() ;
      AtivaCPF();

  end ;

  if (frmMenu.LeParametro('PERMITEALTCPF') = 'S') then
      AtivaCPF();

  bAlteracao := False ;

  { -- oculta colunas caso par�metro n�o estiver ativo -- }
  if (StrToInt(frmMenu.LeParametro('DIAVALIDAUT')) <= 0) then
  begin
      DBGridEh1.Columns[10].Visible := false;
      DBGridEh1.Columns[11].Visible := false;
  end
  else
  begin
      DBGridEh1.Columns[10].Visible := true;
      DBGridEh1.Columns[11].Visible := true;
  end;

end;

procedure TfrmClienteVarejoPessoaFisica.qryPessoaAutorizadaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryPessoaAutorizada.State <> dsBrowse) or (qryPessoaAutorizadacNmTabTipoComprov.Value = '') then
  begin

      qryTipoComprovCIDPA.Close ;
      PosicionaQuery(qryTipoComprovCIDPA, qryPessoaAutorizadanCdTabTipoComprovRG.AsString) ;

      if not qryTipoComprovCIDPA.Eof then
          qryPessoaAutorizadacNmTabTipoComprov.Value := qryTipoComprovCIDPAcNmTabTipoComprov.Value ;

  end ;

  if (qryPessoaAutorizada.State <> dsBrowse) or (qryPessoaAutorizadacNmTabTipoGrauParente.Value = '') then
  begin

      qryGrauParente.Close ;
      PosicionaQuery(qryGrauParente, qryPessoaAutorizadanCdTabTipoGrauParente.AsString) ;

      if not qryGrauParente.Eof then
          qryPessoaAutorizadacNmTabTipoGrauParente.Value := qryGrauParentecNmTabTipoGrauParente.Value ;

  end ;

  if (qryPessoaAutorizada.State <> dsBrowse) or (qryPessoaAutorizadacNmStatus.Value = '') then
  begin

      qryStatus.Close ;
      PosicionaQuery(qryStatus, qryPessoaAutorizadanCdStatus.AsString) ;

      if not qryStatus.Eof then
          qryPessoaAutorizadacNmStatus.Value := qryStatuscNmStatus.Value ;

  end ;

  if (qryPessoaAutorizada.State <> dsBrowse) or (qryPessoaAutorizadacNmUsuario.Value = '') then
  begin

      qryUsuarioPA.Close ;
      PosicionaQuery(qryUsuarioPA, qryPessoaAutorizadanCdUsuarioCadastro.AsString) ;

      if not qryUsuarioPA.Eof then
          qryPessoaAutorizadacNmUsuario.Value := qryUsuarioPAcNmUsuario.Value ;

  end ;

  { -- calcula validade pessoa autorizada -- }
  if ((StrToInt(frmMenu.LeParametro('DIAVALIDAUT')) > 0) and (not qryPessoaAutorizadadDtAutorizacao.IsNull)) then
      qryPessoaAutorizadadDtValidadeAutorizado.Value := qryPessoaAutorizadadDtAutorizacao.Value + strtoint(frmMenu.LeParametro('DIAVALIDAUT'));

end;

procedure TfrmClienteVarejoPessoaFisica.qryPessoaAutorizadaBeforePost(
  DataSet: TDataSet);
begin

  if (qryPessoaAutorizadacCPF.Value = qryMastercCNPJCPF.Value) then
  begin
      MensagemAlerta('O CPF da pessoa autorizada n�o pode ser igual o CPF do cliente.') ;
      abort ;
  end ;

  if (qryPessoaAutorizadacNmPessoaAutorizada.Value = '') then
  begin
      MensagemAlerta('Informe o nome completo da pessoa autorizada.') ;
      abort ;
  end ;

  if (qryPessoaAutorizadacRG.Value = '') then
  begin
      MensagemAlerta('Informe o n�mero do documento de identifica��o.') ;
      abort ;
  end ;

  if (qryPessoaAutorizadacNmTabTipoComprov.Value = '') then
  begin
      MensagemAlerta('Informe o tipo do documento de identifica��o.') ;
      abort ;
  end ;

  if (qryPessoaAutorizadacNmTabTipoGrauParente.Value = '') then
  begin
      MensagemAlerta('Informe o grau de parentesco.') ;
      abort ;
  end ;

  if (qryPessoaAutorizadacNmStatus.Value = '') then
  begin
      MensagemAlerta('Informe o status da pessoa.'+#13#13+'1 - Ativo / 2 - Inativo') ;
      abort ;
  end ;

  qryBuscaClienteCPF.Close ;
  PosicionaQuery(qryBuscaClienteCPF, qryPessoaAutorizadacCPF.Value) ;

  if not qryBuscaClienteCPF.eof then
  begin
      MensagemAlerta('Esta pessoa j� est� cadastrada como cliente. Clique em OK para visualizar o c�digo do cliente.') ;
      ShowMessage('C�digo : ' + qryBuscaClienteCPFnCdTerceiro.asString) ;
      abort ;
  end ;

  inherited;

  if (qryPessoaAutorizada.State = dsInsert) then
      qryPessoaAutorizadanCdPessoaAutorizada.Value := frmMenu.fnProximoCodigo('PESSOAAUTORIZADA') ;
       
  qryPessoaAutorizadacNmPessoaAutorizada.Value := Uppercase(qryPessoaAutorizadacNmPessoaAutorizada.Value) ;
  qryPessoaAutorizadanCdTerceiro.Value         := qryMasternCdTerceiro.Value ;
  qryPessoaAutorizadanCdUsuarioCadastro.Value  := frmMenu.nCdUsuarioLogado;

  if (frmMenu.nCdLojaAtiva > 0) then
      qryPessoaAutorizadanCdLojaCadastro.Value     := frmMenu.nCdLojaAtiva;
      
  qryPessoaAutorizadadDtCadastro.Value         := Now() ;

end;

procedure TfrmClienteVarejoPessoaFisica.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryPessoaAutorizada.State = dsBrowse) then
             qryPessoaAutorizada.Edit ;

        if (qryPessoaAutorizada.State = dsInsert) or (qryPessoaAutorizada.State = dsEdit) then
        begin

            if (DBGridEh1.Col = 7) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(177);

                If (nPK > 0) then
                begin
                    qryPessoaAutorizadanCdTabTipoComprovRG.Value := nPK ;
                end ;
            end ;

            if (DBGridEh1.Col = 9) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(178);

                If (nPK > 0) then
                begin
                    qryPessoaAutorizadanCdTabTipoGrauParente.Value := nPK ;
                end ;
            end ;

            if (DBGridEh1.Col = 12) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(2);

                If (nPK > 0) then
                begin
                    qryPessoaAutorizadanCdStatus.Value := nPK ;
                end ;
            end ;

        end ;

    end ;

  end ;


end;

procedure TfrmClienteVarejoPessoaFisica.qrySPCPessoaFisicaBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qrySPCPessoaFisicacNmTabTipoSPC.Value = '') then
  begin
      MensagemAlerta('Informe o Tipo de Consulta') ;
      abort ;
  end ;

  if (qrySPCPessoaFisicadDtConsulta.asString = '') then
  begin
      MensagemAlerta('Informe a data da consulta.') ;
      abort ;
  end ;

  if (qrySPCPessoaFisica.State = dsEdit) then
  begin

      if (qrySPCPessoaFisicadDtCad.Value < Date) then
      begin
          MensagemAlerta('A consulta s� pode ser alterada no dia da inclus�o.') ;
          abort ;
      end ;

      if (qrySPCPessoaFisicanCdUsuarioConsulta.Value <> frmMenu.nCdUsuarioLogado) then
      begin
          MensagemAlerta('Somente o usu�rio que incluiu a consulta pode alterar as informa��es.') ;
          abort ;
      end;

  end ;

  if (qrySPCPessoaFisica.State = dsInsert) then
  begin
      qrySPCPessoaFisicanCdSPCPessoaFisica.Value := frmMenu.fnProximoCodigo('SPCPESSOAFISICA') ;
      qrySPCPessoaFisicadDtCad.Value             := Now() ;
      qrySPCPessoaFisicanCdUsuarioConsulta.Value := frmMenu.nCdUsuarioLogado ;
      qrySPCPessoaFisicanCdTerceiro.Value        := qryMasternCdTerceiro.Value ;

      if (frmMenu.nCdLojaAtiva > 0) then
          qrySPCPessoaFisicanCdLojaConsulta.Value    := frmMenu.nCdLojaAtiva;

  end ;

  qrySPCPessoaFisicacDadosAdicionais.Value := Uppercase(qrySPCPessoaFisicacDadosAdicionais.Value) ;

  if (qryMaster.State = dsBrowse) then
      qryMaster.Edit;

  qryMasterdDtProxSPC.Value := qrySPCPessoaFisicadDtConsulta.Value + strToint(frmMenu.LeParametro('DIAVALIDSPC')) ;
end;

procedure TfrmClienteVarejoPessoaFisica.qrySPCPessoaFisicaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qrySPCPessoaFisica.State <> dsBrowse) or (qrySPCPessoaFisicacNmTabTipoSPC.Value = '') then
  begin

      if (qrySPCPessoaFisicacNmTabTipoSPC.Value = '') then
      begin
          qryTabTipoSPC.Close ;
          PosicionaQuery(qryTabTipoSPC, qrySPCPessoaFisicanCdTabTipoSPC.AsString) ;

          if not qryTabTipoSPC.Eof then
              qrySPCPessoaFisicacNmTabTipoSPC.Value := qryTabTipoSPCcNmTabTipoSPC.Value ;
      end ;

  end ;

  if (qrySPCPessoaFisica.State <> dsBrowse) or (qrySPCPessoaFisicacNmUsuarioConsulta.Value = '') then
  begin

      if (qrySPCPessoaFisicacNmUsuarioConsulta.Value = '') then
      begin
          qryUsuarioSPC.Close ;
          PosicionaQuery(qryUsuarioSPC, qrySPCPessoaFisicanCdUsuarioConsulta.AsString) ;

          if not qryUsuarioSPC.Eof then
              qrySPCPessoaFisicacNmUsuarioConsulta.Value := qryUsuarioSPCcNmUsuario.Value ;
      end ;

  end ;
        

end;

procedure TfrmClienteVarejoPessoaFisica.DBGridEh2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qrySPCPessoaFisica.State = dsBrowse) then
             qrySPCPessoaFisica.Edit ;

        if (qrySPCPessoaFisica.State = dsInsert) or (qrySPCPessoaFisica.State = dsEdit) then
        begin

            if (DBGridEh1.Col = 3) then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta(179);

                If (nPK > 0) then
                begin
                    qrySPCPessoaFisicanCdTabTipoSPC.Value := nPK ;
                end ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.qrySPCPessoaFisicaBeforeDelete(
  DataSet: TDataSet);
begin

  if (qrySPCPessoaFisicadDtCad.Value < Date) then
  begin
      MensagemAlerta('A consulta s� pode ser exclu�da no dia da inclus�o.') ;
      abort ;
  end ;

  if (qrySPCPessoaFisicanCdUsuarioConsulta.Value <> frmMenu.nCdUsuarioLogado) then
  begin
      MensagemAlerta('Somente o usu�rio que incluiu a consulta pode excluir.') ;
      abort ;
  end;

  inherited;

end;

procedure TfrmClienteVarejoPessoaFisica.btSalvarClick(Sender: TObject);
var
  bAlteracao : boolean ;
label
  NovoCodigoEndereco;
begin
  {inherited;}

  bAlteracao := True ;

  if (qryMaster.State = dsBrowse) and ((qryEndereco.State = dsInactive) or (qryEndereco.State = dsBrowse)) then
  begin
      bAlteracao := False ;
  end ;

  if not (bAlteracao) and (qryPessoaAutorizada.Active) then
  begin
      qryPessoaAutorizada.First ;

      while not qryPessoaAutorizada.Eof do
      begin

          if (qryPessoaAutorizada.RecordStatus = [rsModified]) or (qryPessoaAutorizada.RecordStatus = [rsNew]) then
          begin
              bAlteracao := True ;
              break ;
          end ;

          qryPessoaAutorizada.Next;
      end ;

      qryPessoaAutorizada.First ;
  end ;

  if not (bAlteracao) and (qrySPCPessoaFisica.Active) then
  begin
      qrySPCPessoaFisica.First ;

      while not qrySPCPessoaFisica.Eof do
      begin

          if (qrySPCPessoaFisica.RecordStatus = [rsModified]) or (qrySPCPessoaFisica.RecordStatus = [rsNew]) then
          begin
              bAlteracao := True ;
              break ;
          end ;

          qrySPCPessoaFisica.Next;
      end ;

      qrySPCPessoaFisica.First ;
  end ;

  if not (bAlteracao) and (qryInformacao.Active) then
  begin
      qryInformacao.First ;

      while not qryInformacao.Eof do
      begin

          if (qryInformacao.RecordStatus = [rsModified]) or (qryInformacao.RecordStatus = [rsNew]) then
          begin
              bAlteracao := True ;
              break ;
          end ;

          qryInformacao.Next;
      end ;

      qryInformacao.First ;
  end ;

  if not (bAlteracao) then
  begin
      MensagemAlerta('Nenhuma altera��o realizada no cadastro.') ;
      abort ;
  end ;

  if (qryMaster.RecordStatus = [rsNew]) then
  begin

      case MessageDlg('Confirma a inclus�o deste cliente ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:abort ;
      end ;

  end
  else
  begin

      case MessageDlg('Confirma a altera��o deste cliente ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:abort ;
      end ;

  end ;

  cxPageControl1.ActivePage := Tab1 ;

  {-- gera um c�digo para o endere�o, somente quando � inser��o --}
  if (qryEndereco.State <> dsInactive) then
      if (qryEndereco.State <> dsBrowse) then
          if (qryEndereco.State = dsInsert) then
          begin

              NovoCodigoEndereco:

              qryEndereconCdEndereco.Value := frmMenu.fnProximoCodigo('ENDERECO') ;

              qryAux.Close;
              qryAux.SQL.Clear;
              qryAux.SQL.Add('SELECT 1 FROM Endereco WHERE nCdEndereco = ' + qryEndereconCdEndereco.asString) ;
              qryAux.Open;

              if not qryAux.eof then
                  goto NovoCodigoEndereco;

              qryAux.Close ;

          end ;

  frmMenu.Connection.BeginTrans;

  try
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;

      if (qryEndereco.State <> dsInactive) then
          if (qryEndereco.State <> dsBrowse) then
          begin

              qryEndereconCdUsuarioCadastro.Value := frmMenu.nCdUsuarioLogado;
              qryEnderecodDtCadastro.Value        := Now() ;
              
              if (frmMenu.nCdLojaAtiva > 0) then
                  qryEndereconCdLojaCadastro.Value    := frmMenu.nCdLojaAtiva;

              if ((frmMenu.LeParametro('VALIDINFCLIENTE') = 'S') or (Trim(DBEdit28.Text) <> '')) then
                  qryEndereco.Post ;
          end ;

      {if (qryPessoaFisica.State <> dsBrowse) then
          qryPessoaFisica.Post ;}

      if (qrySPCPessoaFisica.State <> dsInactive) then
          if (qrySPCPessoaFisica.State <> dsBrowse) then
              qrySPCPessoaFisica.Post ;

      if (qryInformacao.State <> dsInactive) then
          if (qryInformacao.State <> dsBrowse) then
              qryInformacao.Post ;

      if (qryPessoaAutorizada.State <> dsInactive) then
          if (qryPessoaAutorizada.State <> dsBrowse) then
              qryPessoaAutorizada.Post ;

      // envia tudo que est� no cache para o banco de dados
      qryMaster.UpdateBatch();

      {if (qryTerceiroTipoTerceiro.Active) then
          if (qryTerceiroTipoTerceiro.RecordCount > 0) then
              qryTerceiroTipoTerceiro.UpdateBatch();}

      {-- posta o novo endere�o no banco --}

      if ((frmMenu.LeParametro('VALIDINFCLIENTE') = 'N') or (Trim(DBEdit28.Text) <> '')) then
          qryEndereco.UpdateBatch();
          
      {qryPessoaFisica.UpdateBatch();}

      if (qryPessoaAutorizada.State <> dsInactive) then
          qryPessoaAutorizada.UpdateBatch();

      if (qrySPCPessoaFisica.State <> dsInactive) then
          qrySPCPessoaFisica.UpdateBatch();

      if (qryInformacao.State <> dsInactive) then
          qryInformacao.UpdateBatch();

  except
      frmMenu.Connection.RollbackTrans;
      {qryMaster.CancelBatch();
      qryPessoaFisica.CancelBatch();
      qryPessoaAutorizada.CancelBatch();
      qrySPCPessoaFisica.CancelBatch();
      qryTerceiroTipoTerceiro.CancelBatch();
      qryEndereco.CancelBatch();}
      //MensagemErro('Erro no processamento.') ;
      qryMaster.Edit;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Informa��es Atualizadas com Sucesso!') ;

  cxPageControl1.ActivePage := Tab1 ;

  if (qryMasterdDtUsuarioAprova.AsString <> '') then
  begin

      DesativaEndereco() ;
      DesativaLimite()   ;
      DesativaCPF();

      btAltEndereco.Visible := True ;
      btAltTelefone.Visible := True ;
      
  end ;

  if (bChamadaExterna) then
      close
  else btCancelar.Click;

end;

procedure TfrmClienteVarejoPessoaFisica.qryEnderecoBeforePost(DataSet: TDataSet);
var strEstados : TstringList;
begin
  inherited;

  if (qryEndereconCdStatus.Value <> 2) then
  begin

      if (trim(qryEnderecocCep.Text) = '') then
      begin
          MensagemAlerta('Informe o CEP.') ;
          DBEdit28.SetFocus ;
          abort ;
      end;

      if (qryEndereconCdTabTipoLogradouro.Value = 0) then
      begin
          MensagemAlerta('Selecione o Tipo de Endere�o');
          ComboTabTipoLogradouro.SetFocus;
          Abort;
      end;

      if (qryEnderecocEndereco.Text = '') then
      begin
          MensagemAlerta('Informe o endere�o.') ;
          DBEdit29.SetFocus ;
          abort ;
      end ;

      if (qryEnderecocBairro.Text = '') then
      begin
          MensagemAlerta('Informe o bairro.') ;
          DBEdit31.SetFocus ;
          abort ;
      end ;

      if (qryEnderecocCidade.Text = '') then
      begin
          MensagemAlerta('Informe a cidade.') ;
          DBEdit32.SetFocus ;
          abort ;
      end ;

      strEstados := TStringList.Create;

      strEstados.Add('AC') ;
      strEstados.Add('AL') ;
      strEstados.Add('AM') ;
      strEstados.Add('AP') ;
      strEstados.Add('BA') ;
      strEstados.Add('CE') ;
      strEstados.Add('DF') ;
      strEstados.Add('ES') ;
      strEstados.Add('EX') ;
      strEstados.Add('GO') ;
      strEstados.Add('MA') ;
      strEstados.Add('MG') ;
      strEstados.Add('MS') ;
      strEstados.Add('MT') ;
      strEstados.Add('PA') ;
      strEstados.Add('PB') ;
      strEstados.Add('PE') ;
      strEstados.Add('PI') ;
      strEstados.Add('PR') ;
      strEstados.Add('RJ') ;
      strEstados.Add('RN') ;
      strEstados.Add('RO') ;
      strEstados.Add('RR') ;
      strEstados.Add('RS') ;
      strEstados.Add('SC') ;
      strEstados.Add('SE') ;
      strEstados.Add('SP') ;
      strEstados.Add('TO') ;

      if strEstados.IndexOf(qryEnderecocUF.Value) = -1 then
      begin
          MensagemAlerta('UF inv�lida.') ;
          DBEdit33.SetFocus ;
          Abort ;
      end ;

      if (qryEndereconCdTabTipoResidencia.Value = 0) then
      begin
          MensagemAlerta('Selecione o tipo de resid�ncia.') ;
          ComboTipoResid.SetFocus ;
          abort ;
      end ;

      qryEndereconCdTerceiro.Value := qryMasternCdTerceiro.Value ;
      qryEndereconCdStatus.Value   := 1 ;
      qryEndereconCdTipoEnd.Value  := 1 ;
      qryEndereconCdRegiao.Value   := 1 ;
      qryEndereconCdPais.Value     := strToInt(frmMenu.LeParametro('CDPAISBR'));

  end ;

  if (Length(trim(qryEnderecocTelefone.Value)) > 0) and (Length(trim(qryEnderecocTelefone.Value)) < 10) then
  begin
      MensagemAlerta('Telefone Residencial 1 inv�lido. Informe o DDD e no m�nimo 8 d�gitos.') ;
      DBedit38.SetFocus;
      abort ;
  end;

  if (Length(trim(qryEnderecocFax.Value)) > 0) and (Length(trim(qryEnderecocFax.Value)) < 10) then
  begin
      MensagemAlerta('Telefone Residencial 2 inv�lido. Informe o DDD e no m�nimo 8 d�gitos.') ;
      DBedit39.SetFocus;
      abort ;
  end;

end;

procedure TfrmClienteVarejoPessoaFisica.Tab5Show(Sender: TObject);
begin
  inherited;

  if (qryMaster.eof) then
      exit ;
  
  qryUsuarioCad.Close ;
  qryUsuarioUltAlt.Close ;
  qryTabTipoSituacao.Close ;
  qryTabTipoOrigem.Close ;
  qryStatus.Close ;
  qryUsuarioAprova.Close ;

  PosicionaQuery(qryUsuarioCad, qryMasternCdUsuarioCadastro.AsString) ;
  PosicionaQuery(qryUsuarioUltAlt, qryMasternCdUsuarioUltAlt.AsString) ;
  PosicionaQuery(qryTabTipoSituacao, qryMasternCdTabTipoSituacao.AsString) ;
  PosicionaQuery(qryTabTipoOrigem, qryMasternCdTabTipoOrigem.AsString) ;
  PosicionaQuery(qryStatus, qryMasternCdStatus.AsString) ;
  PosicionaQuery(qryUsuarioAprova, qryMasternCdUsuarioAprova.AsString) ;

end;

procedure TfrmClienteVarejoPessoaFisica.qryMasterAfterPost(DataSet: TDataSet);
begin
  {inherited;}

  {if not qryTerceiroTipoTerceiro.Active then
  begin
      qryTerceiroTipoTerceiro.Close ;
      qryTerceiroTipoTerceiro.Open ;
  end ;

  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM TerceiroTipoTerceiro WHERE nCdTerceiro = ' + qryMasternCdTerceiro.AsString + ' AND nCdTipoTerceiro = 2') ;
  qryAux.Open ;

  if qryAux.Eof then
  begin
      qryTerceiroTipoTerceiro.Insert ;
      qryTerceiroTipoTerceironCdTerceiro.Value     := qryMasternCdTerceiro.Value ;
      qryTerceiroTipoTerceironCdTipoTerceiro.Value := 2 ;
      qryTerceiroTipoTerceiro.Post ;
  end ;

  qryAux.Close ;}

  {if (qryPessoaAutorizada.Active) and (qryPessoaAutorizada.RecordCount > 0) then
      qryPessoaAutorizada.First;

  if (qrySPCPessoaFisica.Active) and (qrySPCPessoaFisica.RecordCount > 0) then
      qrySPCPessoaFisica.First ;

  if (not qryPessoaAutorizada.Active)
      or ((qryPessoaAutorizadanCdTerceiro.Value <> qryMasternCdTerceiro.Value) and (qryPessoaAutorizada.RecordCount > 0)) then
      PosicionaQuery(qryPessoaAutorizada, qryMasternCdTerceiro.AsString) ;

  if (not qrySPCPessoaFisica.Active)
      or ((qrySPCPessoaFisicanCdTerceiro.Value <> qryMasternCdTerceiro.Value) and (qrySPCPessoaFisica.RecordCount > 0)) then
      PosicionaQuery(qrySPCPessoaFisica, qryMasternCdTerceiro.AsString)  ;}

end;

procedure TfrmClienteVarejoPessoaFisica.qryMasterBeforePost(DataSet: TDataSet);
begin
  {inherited;}

  // faz as consist�ncias
  if (qryMastercNmTerceiro.Value = '') then
  begin
      MensagemAlerta('Informe o nome completo do cliente.') ;
      DBEdit3.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtNasc.AsString = '') then
  begin
      MensagemAlerta('Informe a data de nascimento.') ;
      DBEdit4.SetFocus ;
      abort ;
  end ;

  if (frmMenu.LeParametro('PERMCLIMENOR') = 'N') and (YearsBetween(qryMasterdDtNasc.Value, Now) < 18) then
  begin
      MensagemAlerta('N�o permite cadastro de clientes menores de 18 anos.') ;
      DBEdit4.SetFocus ;
      abort ;
  end ;

  if ((qryMastercCNPJCPF.Value = '') and (frmMenu.LeParametro('VALIDINFCLIENTE') = 'S')) then
  begin
      MensagemAlerta('Informe o CPF do cliente.') ;
      DBEdit10.SetFocus ;
      abort ;
  end ;

  if ((qryMastercRG.Value = '') and (frmMenu.LeParametro('VALIDINFCLIENTE') = 'S')) then
  begin
      MensagemAlerta('Informe o documento de identidade.') ;
      DBEdit5.SetFocus ;
      abort ;
  end ;

  if ((qryMastercUFRG.Value = '') and (frmMenu.LeParametro('VALIDINFCLIENTE') = 'S')) then
  begin
      MensagemAlerta('Informe o estado de emiss�o do documento de identidade.') ;
      DBEdit6.SetFocus ;
      abort ;
  end ;

  if ((qryMasterdDtEmissaoRG.asString = '') and (frmMenu.LeParametro('VALIDINFCLIENTE') = 'S')) then
  begin
      MensagemAlerta('Informe a data de emiss�o do documento de identidade.') ;
      DBEdit9.SetFocus ;
      abort ;
  end ;

  if ((qryMasternCdTabTipoComprovRG.Value = 0) and (frmMenu.LeParametro('VALIDINFCLIENTE') = 'S')) then
  begin
      MensagemAlerta('Informe o tipo do documento de identidade.') ;
      ComboComprovCID.SetFocus ;
      abort ;
  end ;

  if ((qryMasternCdTabTipoComprovRG.Value = 99) and (qryMastercOBSRG.Value = '') and (frmMenu.LeParametro('VALIDINFCLIENTE') = 'S')) then
  begin
      MensagemAlerta('Especifique o tipo de documento de identidade.') ;
      DBEdit8.SetFocus ;
      abort ;
  end ;

  if ((qryMasternCdTabTipoSexo.Value = 0) and (frmMenu.LeParametro('VALIDINFCLIENTE') = 'S')) then
  begin
      MensagemAlerta('Selecione o sexo do cliente.') ;
      ComboSexo.SetFocus ;
      abort ;
  end ;

  if ((qryMasternCdTabTipoEstadoCivil.Value = 0) and (frmMenu.LeParametro('VALIDINFCLIENTE') = 'S')) then
  begin
      MensagemAlerta('Selecione o estado civil.') ;
      ComboEstadoCivil.SetFocus;
      abort ;
  end ;

  if (qryMasteriNrDependentes.Value < 0) then
  begin
      MensagemAlerta('N�mero de dependentes inv�lido.') ;
      DBEdit11.SetFocus;
      abort ;
  end ;

  if ((qryMastercNaturalidade.Value = '') and (frmMenu.LeParametro('VALIDINFCLIENTE') = 'S')) then
  begin
      MensagemAlerta('Informe a naturalidade do cliente.') ;
      DBEdit12.SetFocus;
      abort ;
  end ;

  if ((qryMasternCdTabTipoGrauEscola.Value = 0) and (frmMenu.LeParametro('VALIDINFCLIENTE') = 'S')) then
  begin
      MensagemAlerta('Informe o grau de escolaridade do cliente.') ;
      ComboEscolaridade.SetFocus;
      abort ;
  end ;

  if (Length(trim(qryMastercTelefoneMovel.Value)) > 0) and (Length(trim(qryMastercTelefoneMovel.Value)) < 10) then
  begin
      MensagemAlerta('Telefone Celular inv�lido. Informe o DDD e no m�nimo 8 d�gitos.') ;
      DBedit14.SetFocus;
      abort ;
  end ;

  if (Length(trim(qryMastercTelefoneRec1.Value)) > 0) and (Length(trim(qryMastercTelefoneRec1.Value)) < 10) then
  begin
      MensagemAlerta('Telefone para Recado 1 inv�lido. Informe o DDD e no m�nimo 8 d�gitos.');
      DBEdit104.SetFocus;
      abort ;
  end ;

  if (Length(trim(qryMastercTelefoneRec2.Value)) > 0) and (Length(trim(qryMastercTelefoneRec2.Value)) < 10) then
  begin
      MensagemAlerta('Telefone para Recado 2 inv�lido. Informe o DDD e no m�nimo 8 d�gitos.');
      DBEdit106.SetFocus;
      abort ;
  end ;

  if (qryMasteriNrCalcado.Value < 0) then
  begin
      MensagemAlerta('Numera��o do cal�ado inv�lida.');
      cxPageControl1.ActivePage := tabCRM;
      DBEdit122.SetFocus;
      Abort;
  end;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasterdDtCadastro.Value              := Now() ;
      qryMasternCdStatus.Value                := 1 ;
      qryMasternCdUsuarioCadastro.Value       := frmMenu.nCdUsuarioLogado;

      if (frmMenu.nCdLojaAtiva > 0) then
          qryMasternCdLojaCadastro.Value          := frmMenu.nCdLojaAtiva;

      qryMasternCdTabTipoOrigem.Value         := 1 ;
      qryMasternCdTabTipoSituacao.Value       := 1 ;
      qryMastercFlgCadSimples.Value           := 0 ;
      qryMasternCdTerceiroPessoaFisica.Value  := qryMasternCdTerceiro.Value ;
  end ;


  if (qryMaster.State = dsEdit) then
  begin

      qryMasternCdUsuarioUltAlt.Value := frmMenu.nCdUsuarioLogado;
      qryMasterdDtUltAlt.Value        := Now() ;

      if (frmMenu.nCdLojaAtiva > 0) then
          qryMasternCdLojaUltAlt.Value    := frmMenu.nCdLojaAtiva;

      if (qryMasternCdTabTipoOrigem.Value = 0) then
          qryMasternCdTabTipoOrigem.Value := 1 ;

      if (qryMasternCdTabTipoSituacao.Value = 0) then
          qryMasternCdTabTipoSituacao.Value := 1 ;

  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEndereco.Close ;
  qrySPCPessoaFisica.Close ;
  qryPessoaAutorizada.Close ;
  qryUsuarioCad.Close ;
  qryUsuarioUltAlt.Close ;
  qryUsuarioAprova.Close ;
  qryTabTipoSituacao.Close ;
  qryStatus.Close ;
  qryTabTipoOrigem.Close ;
  qryInformacao.Close;
  qryBloqueios.Close ;
  qryDesbloqueados.Close;
  qryGrupoCliente.Close;

  btAltEndereco.Visible      := False ;
  btAltTelefone.Visible      := False ;
  btConsultaEndereco.Visible := False ;
  btHistAltEndereco.Visible  := False ;

end;

procedure TfrmClienteVarejoPessoaFisica.btCancelarClick(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePage := Tab1 ;
  AtivaCPF();
  
end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit28Enter(Sender: TObject);
begin
  inherited;

  cCepAux := DBEdit28.Text ;
  
end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit28Exit(Sender: TObject);
var
  i : integer;
begin
  inherited;

  if (qryEndereco.State <> dsBrowse) and (Trim(DBEdit28.Text) <> '') and (Trim(DBEDit28.Text) <> cCepAUX) then
  begin
      qryBuscaCEP.Close ;
      PosicionaQuery(qryBuscaCEP, DBEdit28.Text) ;

      qryEnderecocEndereco.Value    := '' ;
      qryEnderecocBairro.Value      := '' ;
      qryEnderecocCidade.Value      := '' ;
      qryEnderecocUF.Value          := '' ;
      qryEnderecoiNumero.Value      := 0  ;
      qryEnderecocComplemento.Value := '' ;

      if (not qryBuscaCEP.Eof) then
      begin
          qryEnderecocEndereco.Value := qryBuscaCEPcEndereco.Value ;
          qryEnderecocBairro.Value   := qryBuscaCEPcBairro.Value   ;
          qryEnderecocCidade.Value   := qryBuscaCEPcCidade.Value   ;
          qryEnderecocUF.Value       := qryBuscaCEPcUF.Value       ;

          ComboTabTipoLogradouro.SetFocus ;
      end
      else
      begin
          { -- Efetua consulta atrav�s do componente ACBrCEP caso endere�o n�o exista na base de dados -- }
          { -- WebService: wsByJG / login: admsoft / senha: ed101580 / site: http://www.byjg.com.br -- }
          try
              ACBrCEP1.BuscarPorCEP(DBEdit28.Text);

              if (ACBrCEP1.Enderecos.Count < 1) then
                  MensagemAlerta('CEP n�o localizado em nossa base de endere�os.')
              else
              begin
                  for i := 0 to ACBrCEP1.Enderecos.Count-1 do
                  begin
                      with ACBrCEP1.Enderecos[i] do
                      begin
                          qryEnderecocEndereco.Value    := Logradouro ;
                          qryEnderecocComplemento.Value := Complemento ;
                          qryEnderecocBairro.Value      := Bairro ;
                          qryEnderecocCidade.Value      := Municipio ;
                          qryEnderecocUF.Value          := UF ;
                      end ;
                  end ;

                  ComboTabTipoLogradouro.SetFocus ;
              end ;
          except
              MensagemErro('Erro no processamento.');
              Raise;
          end;
      end;
  end;
end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit27Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsBrowse) and (Trim(DBEdit27.Text) <> '') and (Trim(DBEDit27.Text) <> cCepAUX) then
  begin
      qryBuscaCEP.Close ;
      PosicionaQuery(qryBuscaCEP, DBEdit27.Text) ;

      qryMastercEnderecoEmpTrab.Value   := '' ;
      qryMastercBairroEmpTrab.Value     := '' ;
      qryMastercCidadeEmpTrab.Value     := '' ;
      qryMastercUFEmpTrab.Value         := '' ;
      qryMasteriNrEnderecoEmpTrab.Value := 0 ;

      if not qryBuscaCEP.Eof then
      begin
          qryMastercEnderecoEmpTrab.Value := qryBuscaCEPcEndereco.Value ;
          qryMastercBairroEmpTrab.Value   := qryBuscaCEPcBairro.Value   ;
          qryMastercCidadeEmpTrab.Value   := qryBuscaCEPcCidade.Value   ;
          qryMastercUFEmpTrab.Value       := qryBuscaCEPcUF.Value       ;
          DBEdit42.SetFocus ;
      end ;

  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit27Enter(Sender: TObject);
begin
  inherited;
  cCepAux := DBEdit27.Text ;

end;

procedure TfrmClienteVarejoPessoaFisica.DesativaEndereco;
begin

    DBEDit28.ReadOnly       := True  ;
    DBEdit29.ReadOnly       := true  ;
    DBEdit30.ReadOnly       := True  ;
    DBEdit34.ReadOnly       := True  ;
    DBEdit31.ReadOnly       := True  ;
    DBEdit32.ReadOnly       := True  ;
    DBEdit33.ReadOnly       := True  ;
    DBEdit35.ReadOnly       := True  ;
    DBEdit36.ReadOnly       := True  ;
    DBEdit37.ReadOnly       := True  ;
    DBEdit38.ReadOnly       := True  ;
    DBEdit39.ReadOnly       := True  ;
    DBEdit40.ReadOnly       := True  ;

    DBEDit28.Color          := $00E9E4E4  ;
    DBEdit29.Color          := $00E9E4E4  ;
    DBEdit30.Color          := $00E9E4E4  ;
    DBEdit34.Color          := $00E9E4E4  ;
    DBEdit31.Color          := $00E9E4E4  ;
    DBEdit32.Color          := $00E9E4E4  ;
    DBEdit33.Color          := $00E9E4E4  ;
    DBEdit35.Color          := $00E9E4E4  ;
    DBEdit36.Color          := $00E9E4E4  ;
    DBEdit37.Color          := $00E9E4E4  ;
    DBEdit38.Color          := $00E9E4E4  ;
    DBEdit39.Color          := $00E9E4E4  ;
    DBEdit40.Color          := $00E9E4E4  ;

    ComboTabTipoLogradouro.ReadOnly := True;
    ComboTipoResid.ReadOnly         := True ;
    ComboComprovCRS.ReadOnly        := True ;

end;

procedure TfrmClienteVarejoPessoaFisica.AtivaEndereco;
begin

    DBEDit28.ReadOnly       := False  ;
    DBEdit30.ReadOnly       := False  ;
    DBEdit34.ReadOnly       := False  ;
    DBEdit35.ReadOnly       := False  ;
    DBEdit36.ReadOnly       := False  ;
    DBEdit37.ReadOnly       := False  ;
    DBEdit38.ReadOnly       := False  ;
    DBEdit39.ReadOnly       := False  ;
    DBEdit40.ReadOnly       := False  ;

    DBEDit28.Color          := clWhite  ;
    DBEdit30.Color          := clWhite  ;
    DBEdit34.Color          := clWhite  ;
    DBEdit35.Color          := clWhite  ;
    DBEdit36.Color          := clWhite  ;
    DBEdit37.Color          := clWhite  ;
    DBEdit38.Color          := clWhite  ;
    DBEdit39.Color          := clWhite  ;
    DBEdit40.Color          := clWhite  ;

    if (cEnderecoManual = 'S') then
    begin
        DBEdit29.ReadOnly       := False  ;
        DBEdit31.ReadOnly       := False  ;
        DBEdit32.ReadOnly       := False  ;
        DBEdit33.ReadOnly       := False  ;

        DBEdit29.Color          := clWhite  ;
        DBEdit31.Color          := clWhite  ;
        DBEdit32.Color          := clWhite  ;
        DBEdit33.Color          := clWhite  ;
    end ;

    ComboTabTipoLogradouro.ReadOnly := False;
    ComboTipoResid.ReadOnly         := False ;
    ComboComprovCRS.ReadOnly        := False ;

    btConsultaEndereco.Visible := True ;

end;

procedure TfrmClienteVarejoPessoaFisica.btAltEnderecoClick(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) and (qryMaster.RecordCount > 0) then
  begin
      if not qryEndereco.Eof then
      begin

          qryEndereco.Edit ;
          qryEndereconCdStatus.Value := 2 ;
          qryEndereco.Post ;
          
      end ;

      qryEndereco.Insert ;

      AtivaEndereco() ;

      DBEdit28.SetFocus ;

  end ;



end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit46Exit(Sender: TObject);
begin
  inherited;

  if (DBEdit46.Text = DBEdit10.Text) then
  begin
      MensagemAlerta('CPF do c�njuge n�o pode ser igual CPF do Cliente.') ;
      DBEdit46.SetFocus ;
      abort ;
  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.btAltTelefoneClick(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) and (qryMaster.RecordCount > 0) then
  begin

    DBEdit38.ReadOnly       := False  ;
    DBEdit39.ReadOnly       := False  ;

    DBEdit38.Color          := clWhite  ;
    DBEdit39.Color          := clWhite  ;

    DBEdit38.SetFocus ;

  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.btConsultaEnderecoClick(Sender: TObject);
var
  objConsultaEndereco : TfrmConsultaEndereco ;
begin
  inherited;

  objConsultaEndereco := TfrmConsultaEndereco.Create( Self ) ;
  objConsultaEndereco.ShowModal() ;

  if not objConsultaEndereco.qryConsultaEndereco.Eof then
  begin
      if (qryEndereco.State = dsBrowse) then
          qryEndereco.Edit ;
          
      qryEnderecocEndereco.Value := objConsultaEndereco.qryConsultaEnderecocEndereco.Value ;
      qryEnderecocCEP.Value      := objConsultaEndereco.qryConsultaEnderecocCEP.Value      ;
      qryEnderecocBairro.Value   := objConsultaEndereco.qryConsultaEnderecocBairro.Value   ;
      qryEnderecocCidade.Value   := objConsultaEndereco.qryConsultaEnderecocCidade.Value   ;
      qryEnderecocUF.Value       := objConsultaEndereco.edtUF.Text                         ;

      DBEdit30.SetFocus ;
  end ;

  freeAndNil( objConsultaEndereco ) ;
  
end;

procedure TfrmClienteVarejoPessoaFisica.btConsultaEndereco2Click(Sender: TObject);
var
  objConsultaEndereco : TfrmConsultaEndereco ;
begin
  inherited;

  objConsultaEndereco := TfrmConsultaEndereco.Create( Self ) ;
  objConsultaEndereco.ShowModal() ;

  if not objConsultaEndereco.qryConsultaEndereco.Eof then
  begin
      qryMaster.Edit ;
      qryMastercEnderecoEmpTrab.Value := objConsultaEndereco.qryConsultaEnderecocEndereco.Value ;
      qryMastercCEPEmpTrab.Value      := objConsultaEndereco.qryConsultaEnderecocCEP.Value      ;
      qryMastercBairroEmpTrab.Value   := objConsultaEndereco.qryConsultaEnderecocBairro.Value   ;
      qryMastercCidadeEmpTrab.Value   := objConsultaEndereco.qryConsultaEnderecocCidade.Value   ;
      qryMastercUFEmpTrab.Value       := objConsultaEndereco.edtUF.Text                         ;

      DBEdit42.SetFocus ;
  end ;

  freeAndNil( objConsultaEndereco ) ;
  
end;

procedure TfrmClienteVarejoPessoaFisica.AtivaLimite;
begin

    if (cAltLimCadCli = 'S') then
    begin
        DBEdit87.ReadOnly  := False  ;
        DBEDit87.Color     := clWhite  ;
    end ;

end;

procedure TfrmClienteVarejoPessoaFisica.DesativaLimite;
begin

    DBEdit87.ReadOnly  := True  ;
    DBEDit87.Color     := $00E9E4E4  ;

    DBEdit121.ReadOnly := True  ;
    DBEDit121.Color    := $00E9E4E4  ;

end;

procedure TfrmClienteVarejoPessoaFisica.qryMasterFieldChangeComplete(
  DataSet: TCustomADODataSet; const FieldCount: Integer;
  const Fields: OleVariant; const Error: Error;
  var EventStatus: TEventStatus);
begin
  inherited;
  bAlteracao := True ;
end;

procedure TfrmClienteVarejoPessoaFisica.btnAuditoriaClick(Sender: TObject);
var

  objTrilhaClienteVarejo : TfrmTrilhaClienteVarejo ;

begin
  {inherited;}

  if (not qryMaster.Active or qryMaster.eof) then
  begin
      MensagemAlerta('Nenhum cliente selecionado.') ;
      abort ;
  end ;

  objTrilhaClienteVarejo := TfrmTrilhaClienteVarejo.Create( Self ) ;

  PosicionaQuery(objTrilhaClienteVarejo.qryLog, qryMasternCdTerceiro.asString);

  if (objTrilhaClienteVarejo.qryLog.Eof) then
  begin
      MensagemAlerta('Nenhum registro de altera��o para este cliente.') ;
      exit ;
  end ;

  objTrilhaClienteVarejo.qryLog.Last;
  objTrilhaClienteVarejo.ShowModal;

  freeAndNil( objTrilhaClienteVarejo ) ;

end;

procedure TfrmClienteVarejoPessoaFisica.btHistAltEnderecoClick(
  Sender: TObject);
var
  objClienteVarejoPessoaFisica_HistAltEnd : TfrmClienteVarejoPessoaFisica_HistAltEnd ;
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  objClienteVarejoPessoaFisica_HistAltEnd := TfrmClienteVarejoPessoaFisica_HistAltEnd.Create( Self ) ;

  PosicionaQuery(objClienteVarejoPessoaFisica_HistAltEnd.qryEndereco, qryMasternCdTerceiro.asString) ;
  objClienteVarejoPessoaFisica_HistAltEnd.ShowModal ;

  freeAndNil( objClienteVarejoPessoaFisica_HistAltEnd ) ;
  
end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit54Exit(Sender: TObject);
begin
  inherited;
  
  if (qryMaster.State = dsInsert) then
  begin
      cxPageControl1.ActivePageIndex := 3 ;
      DBEdit67.SetFocus ;
  end ;


end;

procedure TfrmClienteVarejoPessoaFisica.ToolButton10Click(Sender: TObject);
var
  objImagemDigital : TfrmImagemDigital ;
begin
  inherited;

  if not qryMaster.eof then
  begin
      objImagemDigital := TfrmImagemDigital.Create( Self ) ;

      objImagemDigital.DocumentoCliente(qryMasternCdTerceiro.Value) ;

      freeAndNil( objImagemDigital ) ;
  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit40Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
  begin
      cxPageControl1.ActivePageIndex := 1 ;
      DBEdit24.SetFocus ;
  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit77Exit(Sender: TObject);
begin
  inherited;
  if (qryMaster.State = dsInsert) then
  begin
      cxPageControl1.ActivePageIndex := 2 ;
      DBEdit17.SetFocus ;
  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.DBCheckBox2Exit(Sender: TObject);
begin
  inherited;
  if (qryMaster.State = dsInsert) then
  begin
      cxPageControl1.ActivePageIndex := 4 ;
      DBGridEh1.SetFocus ;
  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.qryInformacaoBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryInformacao.State = dsInsert) then
  begin
      qryInformacaonCdInformacaoTerceiro.Value := frmMenu.fnProximoCodigo('INFORMACAOTERCEIRO') ;
      qryInformacaodDtCad.Value                := Now() ;
      qryInformacaonCdUsuario.Value            := frmMenu.nCdUsuarioLogado ;
      qryInformacaonCdTerceiro.Value           := qryMasternCdTerceiro.Value ;

      if (frmMenu.nCdLojaAtiva > 0) then
          qryInformacaonCdLoja.Value    := frmMenu.nCdLojaAtiva;

  end ;

  qryInformacaocInformacao.Value := Uppercase(qryInformacaocInformacao.Value) ;

end;

procedure TfrmClienteVarejoPessoaFisica.cxTabSheet3Show(Sender: TObject);
begin
  inherited;
  if (qryMaster.eof) then
      exit ;

  if (not qryInformacao.Active)
      or ((qryInformacaonCdTerceiro.Value <> qryMasternCdTerceiro.Value) and (qryInformacao.RecordCount > 0)) then
      PosicionaQuery(qryInformacao, qryMasternCdTerceiro.AsString) ;

end;

procedure TfrmClienteVarejoPessoaFisica.qryInformacaoCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  if ((qryInformacao.RecordStatus <> [rsModified]) and (qryInformacao.RecordStatus <> [rsNew])) then
      if (qryInformacaonCdUsuario.Value > 0) then
      begin
          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('SELECT cNmUsuario FROM Usuario WHERE nCdUsuario = ' + qryInformacaonCdUsuario.AsString);
          qryAux.Open ;

          if not qryAux.Eof then
              qryInformacaocNmUsuario.Value := qryAux.FieldList[0].Value ;
      end ;

end;

procedure TfrmClienteVarejoPessoaFisica.qryInformacaoBeforeEdit(
  DataSet: TDataSet);
begin
  inherited;

  if (qryInformacaonCdInformacaoTerceiro.Value > 0) then
  begin
      MensagemErro('N�o � permitido alterar informa��es do cliente, somente adicionar novas informa��es.');
      abort ;
  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.qryInformacaoBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;
  if (qryInformacaonCdInformacaoTerceiro.Value > 0) then
  begin
      MensagemErro('N�o � permitido alterar informa��es do cliente, somente adicionar novas informa��es.');
      abort ;
  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.btConsParcAbertoClick(
  Sender: TObject);
var
  objCaixa_ParcelaAberto : TfrmCaixa_ParcelaAberto ;
begin

  if (qryMasternCdTerceiro.Value > 0) and (qryMasternValCreditoUtil.Value > 0) then
  begin

      objCaixa_ParcelaAberto := TfrmCaixa_ParcelaAberto.Create( Self ) ;

      PosicionaQuery(objCaixa_ParcelaAberto.qryTituloAberto,qryMasternCdTerceiro.asString) ;
      objCaixa_ParcelaAberto.ShowModal ;

      freeAndNil( objCaixa_ParcelaAberto ) ;

  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit119Change(Sender: TObject);
begin
  inherited;

  if (DBedit119.Text <> '') then
  begin
      DBEdit119.Color      := clRed ;
      DBEdit119.Font.Color := clWhite ;
  end
  else
  begin
      DBEDit119.Color      := $00E9E4E4  ;
      DBEdit119.Font.Color := clBlack ;
  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit118Change(Sender: TObject);
begin
  inherited;

  DBEDit118.Color      := $00E9E4E4  ;
  DBEdit118.Font.Color := clBlack ;

  if (DBedit118.Text <> '') then
      if (qryMasterdDtProxSPC.Value < Date) then
      begin
          DBEdit118.Color      := clRed ;
          DBEdit118.Font.Color := clWhite ;
      end


end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit90Change(Sender: TObject);
begin
  inherited;

  DBEDit90.Color      := $00E9E4E4  ;
  DBEdit90.Font.Color := clBlack ;

  if (qryMasternValRecAtraso.Value > 0) then
  begin
      DBEdit90.Color      := clRed ;
      DBEdit90.Font.Color := clWhite ;
  end

end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit92Change(Sender: TObject);
begin
  inherited;

  DBEDit92.Color      := $00E9E4E4  ;
  DBEdit92.Font.Color := clBlack ;

  if (qryMasternSaldoLimiteCredito.Value < 0) then
  begin
      DBEdit92.Color      := clRed ;
      DBEdit92.Font.Color := clWhite ;
  end

end;

procedure TfrmClienteVarejoPessoaFisica.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      btCancelar.Click;

  cxPageControl1.ActivePageIndex := 0 ;
end;

procedure TfrmClienteVarejoPessoaFisica.btConsultarClick(Sender: TObject);
var
  nPK : Integer;
begin
  //inherited;

  nPK := frmLookup_Padrao.ExecutaConsulta2(180,'cFlgCadSimples = 0');
  PosicionaPK(nPK);

  if (qryMaster.IsEmpty) then
      btCancelar.Click;

  cxPageControl1.ActivePageIndex := 0;
end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit99Change(Sender: TObject);
begin
  inherited;

  DBEDit99.Color      := $00E9E4E4  ;
  DBEdit99.Font.Color := clBlack ;

  DBEDit120.Color      := $00E9E4E4  ;
  DBEdit120.Font.Color := clBlack ;

  if (DBEdit93.Text = '4') then
  begin
      DBEdit99.Color      := clRed ;
      DBEdit99.Font.Color := clWhite ;

      DBEdit120.Color      := clRed ;
      DBEdit120.Font.Color := clWhite ;
  end ;

  if (DBEdit93.Text = '3') then
  begin
      DBEdit99.Color      := clYellow ;
      DBEdit99.Font.Color := clBlack ;

      DBEdit120.Color      := clYellow ;
      DBEdit120.Font.Color := clBlack ;
  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.cxTabSheet4Show(Sender: TObject);
begin
  inherited;

  cxPageControl2.ActivePageIndex := 0 ;

  if (qryMaster.eof) then
      exit ;

  qryBloqueios.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryBloqueios, qryMasternCdTerceiro.AsString) ;

end;

procedure TfrmClienteVarejoPessoaFisica.cxTabSheet6Show(Sender: TObject);
begin
  inherited;

  if (qryMaster.eof) then
      exit ;

  PosicionaQuery(qryDesbloqueados, qryMasternCdTerceiro.AsString) ;

end;

procedure TfrmClienteVarejoPessoaFisica.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  Desbloquear1.Enabled := ((qryBloqueios.Active) and (qryBloqueioscFlgLiberar.Value = 1))

end;

procedure TfrmClienteVarejoPessoaFisica.Desbloquear1Click(Sender: TObject);
var
    cObsDesbloqueio : string ;
begin
  inherited;

  InputQuery('Desbloqueio','Motivo Desbloqueio',cObsDesbloqueio) ;

  if (cObsDesbloqueio = '') then
  begin
      MensagemErro('Informe o motivo do desbloqueio.') ;
      exit ;
  end ;

  if (MessageDlg('Confirma desbloqueio ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  try
      SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO.Close ;
      SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdRestricaoVendaTerceiro').Value := qryBloqueiosnCdRestricaoVendaTerceiro.Value ;
      SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdUsuarioDesbloqueio').Value     := frmMenu.nCdUsuarioLogado;
      SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@nCdLojaDesbloqueio').Value        := frmMenu.nCdLojaAtiva;
      SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO.Parameters.ParamByName('@cOBSDesbloqueio').Value           := cObsDesbloqueio;
      SP_DESBLOQUEIA_BLOQUEIO_TERCEIRO.ExecProc;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  qryBloqueios.Close ;
  qryBloqueios.Open ;
  
end;

procedure TfrmClienteVarejoPessoaFisica.DBGridEh4DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;
  
  if (qryBloqueios.RecordCount > 0) then
  begin
      Column.Color      := clRed ;
      Column.Font.Color := clWhite ;
  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.cxButton2Click(Sender: TObject);
var
    objClienteVarejoPessoaFisica_TitulosNeg : TfrmClienteVarejoPessoaFisica_TitulosNeg ;
begin

  if (qryMasternCdTerceiro.Value > 0) then
  begin

      objClienteVarejoPessoaFisica_TitulosNeg := TfrmClienteVarejoPessoaFisica_TitulosNeg.Create( Self ) ;

      PosicionaQuery(objClienteVarejoPessoaFisica_TitulosNeg.qryTitulos,qryMasternCdTerceiro.asString) ;
      objClienteVarejoPessoaFisica_TitulosNeg.ShowModal ;

      freeAndNil( objClienteVarejoPessoaFisica_TitulosNeg ) ;
      
  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.cxTabSheet2Show(Sender: TObject);
begin
  inherited;
  if (qryMaster.eof) then
      exit ;

  //qryPessoaAutorizada.Close ;

  if (not qryPessoaAutorizada.Active)
      or ((qryPessoaAutorizadanCdTerceiro.Value <> qryMasternCdTerceiro.Value) and (qryPessoaAutorizada.RecordCount > 0)) then
      PosicionaQuery(qryPessoaAutorizada, qryMasternCdTerceiro.AsString) ;

end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit10Exit(Sender: TObject);
begin
  inherited;

  ValidaDocumento.Documento := DBEdit10.Text;
  ValidaDocumento.TipoDocto := docCPF ;

  ValidaDocumento.IgnorarChar := '' ;

  if (DBedit10.Text <> '') and (not ValidaDocumento.Validar) then
  begin
      MensagemErro('CPF Inv�lido. N�o informe pontos ou hifens, apenas os n�meros.') ;

      if not DBEdit10.ReadOnly then
          qryMastercCNPJCPF.Value := '' ;

      DBedit10.SetFocus;
      abort ;
  end ;

  if (not DBEdit10.ReadOnly) then
  begin
      qryBuscaClienteCPF.Close ;
      PosicionaQuery(qryBuscaClienteCPF, DBEdit10.Text) ;

      if (not qryBuscaClienteCPF.eof and (qryBuscaClienteCPFnCdTerceiro.Value <> qryMasternCdTerceiro.Value)) then
      begin
          if (MessageDlg('CPF j� cadastrado. Deseja visualizar o cadastro ?',mtConfirmation,[mbYes,mbNo],0) = MrYes) then
          begin
              PosicionaQuery(qryMaster, qryBuscaClienteCPFnCdTerceiro.AsString) ;
              abort ;
          end
          else
          begin
              qryMastercCNPJCPF.Value := '' ;
              DBEdit10.SetFocus;
          end ;

          abort ;
      end ;
  end ;


end;

procedure TfrmClienteVarejoPessoaFisica.Tab4Show(Sender: TObject);
begin
  inherited;
  if (qryMaster.eof) then
      exit ;

  //qryPessoaAutorizada.Close ;

  if (not qrySPCPessoaFisica.Active)
      or ((qrySPCPessoaFisicanCdTerceiro.Value <> qryMasternCdTerceiro.Value) and (qrySPCPessoaFisica.RecordCount > 0)) then
      PosicionaQuery(qrySPCPessoaFisica, qryMasternCdTerceiro.AsString) ;

end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit28KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_F4) and (not DBEdit28.ReadOnly) then
      btConsultaEndereco.Click;

end;

procedure TfrmClienteVarejoPessoaFisica.ToolButton5Click(Sender: TObject);
begin
  if (MessageDlg('Deseja realmente sair do cadastro de clientes?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      abort ;

  inherited;

end;

procedure TfrmClienteVarejoPessoaFisica.cxButton1Click(Sender: TObject);
var
    objClienteVarejoPessoaFisica_HistMov : TfrmClienteVarejoPessoaFisica_HistMov ;
begin

  if (qryMaster.Active) and (qryMasternCdTerceiro.Value > 0) then
  begin

      objClienteVarejoPessoaFisica_HistMov := TfrmClienteVarejoPessoaFisica_HistMov.Create( Self ) ;

      objClienteVarejoPessoaFisica_HistMov.exibeMovimentos( qryMasternCdTerceiro.Value ) ;

      freeAndNil( objClienteVarejoPessoaFisica_HistMov ) ;

  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.desativaCPF;
begin

    DBEdit10.ReadOnly := True  ;
    DBEdit10.Color    := $00E9E4E4  ;

end;

procedure TfrmClienteVarejoPessoaFisica.ativaCPF;
begin

    DBEdit10.ReadOnly := False  ;
    DBEDit10.Color    := clWhite  ;

end;

procedure TfrmClienteVarejoPessoaFisica.cxButton3Click(Sender: TObject);
var
  objClienteVarejoPessoaFisica_HistContato : TfrmClienteVarejoPessoaFisica_HistContato ;
begin
  inherited;

  if (qryMaster.Active) and (qryMasternCdTerceiro.Value > 0) then
  begin

      nCdTerceiroHisContato := qryMasternCdTerceiro.Value;

      objClienteVarejoPessoaFisica_HistContato := TfrmClienteVarejoPessoaFisica_HistContato.Create( Self ) ;
      objClienteVarejoPessoaFisica_HistContato.ShowModal;

      freeAndNil( objClienteVarejoPessoaFisica_HistContato ) ;

  end;
  
end;

procedure TfrmClienteVarejoPessoaFisica.cxButton4Click(Sender: TObject);
var
  objClienteVarejoPessoaFisica_HistoricoNeg : TfrmClienteVarejoPessoaFisica_HistoricoNeg ;
begin
  inherited;
  
  if (qryMaster.Active) and (qryMasternCdTerceiro.Value > 0) then
  begin

      objClienteVarejoPessoaFisica_HistoricoNeg := TfrmClienteVarejoPessoaFisica_HistoricoNeg.Create( Self ) ;

      PosicionaQuery(objClienteVarejoPessoaFisica_HistoricoNeg.qryTitulos,qryMasternCdTerceiro.AsString);
      objClienteVarejoPessoaFisica_HistoricoNeg.ShowModal;

      freeAndNil( objClienteVarejoPessoaFisica_HistoricoNeg ) ;

  end;

end;

procedure TfrmClienteVarejoPessoaFisica.btAltLimiteClick(Sender: TObject);
var
  objAprovacaoClienteVarejo_LimiteInicial : TfrmAprovacaoClienteVarejo_LimiteInicial ;
begin
  inherited;

  if (qryMasternCdTerceiro.Value > 0) then
  begin

      if (qryMasternCdTabTipoSituacao.Value <> 2) then
      begin
          MensagemAlerta('Somente clientes aprovados podem ter limite de cr�dito.') ;
          abort ;
      end ;

      objAprovacaoClienteVarejo_LimiteInicial := TfrmAprovacaoClienteVarejo_LimiteInicial.Create( Self ) ;

      objAprovacaoClienteVarejo_LimiteInicial.nCdTerceiro            := qryMasternCdTerceiro.Value ;
      objAprovacaoClienteVarejo_LimiteInicial.cOBS                   := 'REAJUSTE MANUAL - TELA CLIENTE' ;
      objAprovacaoClienteVarejo_LimiteInicial.edtLimiteCredito.Value := qryMasternValLimiteCred.Value ;
      objAprovacaoClienteVarejo_LimiteInicial.edtPercEntrada.Value   := 0 ;
      objAprovacaoClienteVarejo_LimiteInicial.bAprovacao             := false ;
      objAprovacaoClienteVarejo_LimiteInicial.bLimiteManual          := true ;

      objAprovacaoClienteVarejo_LimiteInicial.ShowModal ;

      freeAndNil( objAprovacaoClienteVarejo_LimiteInicial ) ;

  end ;

end;

procedure TfrmClienteVarejoPessoaFisica.DBGridEh1DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (qryPessoaAutorizadanCdStatus.Value = 2) then
  begin

     DBGridEh1.Canvas.Brush.Color := clRed   ;
     DBGridEh1.Canvas.Font.Color  := clWhite ;
     
  end ;

  DBGridEh1.Canvas.FillRect(Rect);
  DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);

end;

procedure TfrmClienteVarejoPessoaFisica.cxButton5Click(Sender: TObject);
var
  objForm : TrptFichaCadastralCliente_view;
begin
  inherited;

  if qryMaster.IsEmpty then
      MensagemAlerta('Selecione o Cliente.')
  else
  begin
      objForm := TrptFichaCadastralCliente_view.Create(nil);

      objForm.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

      objForm.qryTerceiro.Close;
      PosicionaQuery(objForm.qryTerceiro, qryMasternCdTerceiro.AsString);

      objForm.qrySPCPessoaFisica.Close;
      PosicionaQuery(objForm.qrySPCPessoaFisica, qryMasternCdTerceiro.AsString);

      objForm.QuickRep1.PreviewModal;

      FreeAndNil(objForm);
  end;

end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit123KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  {case Key of
      vk_f4: begin

          if(qryMaster.State = dsBrowse) then
              qryMaster.Edit;

          if(qryMaster.State in[dsInsert, dsEdit]) then
          begin
              nPK := frmLookup_Padrao.ExecutaConsulta(787);

              if(nPK > 0) then
                  qryMasternCdGrupoClienteDesc.Value := nPK;

          end;
      end;
  end;}
end;

procedure TfrmClienteVarejoPessoaFisica.DBEdit123Exit(Sender: TObject);
begin
  inherited;

  {qryGrupoCliente.Close;

  if (qryMaster.IsEmpty) then
      Exit;

  PosicionaQuery(qryGrupoCliente, qryMasternCdGrupoClienteDesc.AsString);}
end;

initialization
    RegisterClass(TfrmClienteVarejoPessoaFisica) ;

end.
