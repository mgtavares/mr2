unit fAdmListaCobranca_Listas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, Menus, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmAdmListaCobranca_Listas = class(TfrmProcesso_Padrao)
    qryLista: TADOQuery;
    qryListanCdListaCobranca: TIntegerField;
    qryListadDtCobranca: TDateTimeField;
    qryListadDtVencto: TDateTimeField;
    qryListaiSeq: TIntegerField;
    qryListaiQtdeClienteTotal: TIntegerField;
    qryListaiQtdeClienteRestante: TIntegerField;
    qryListadDtAtribuicao: TDateTimeField;
    qryListadDtAbertura: TDateTimeField;
    dsLista: TDataSource;
    DBGridEh1: TDBGridEh;
    qryListacNmListaCobranca: TStringField;
    qryListacNmUsuario: TStringField;
    PopupMenu1: TPopupMenu;
    CancelarLista1: TMenuItem;
    EncerrarLista1: TMenuItem;
    GerenciarItens1: TMenuItem;
    N1: TMenuItem;
    qryAux: TADOQuery;
    qryListanCdUsuarioResp: TIntegerField;
    qryListaiQtdeClienteContato: TIntegerField;
    qryListaiQtdeReagendado: TIntegerField;
    qryListaiQtdeSemContato: TIntegerField;
    qryListaiQtdeAcordo: TIntegerField;
    qryListacSituacao: TStringField;
    qryListacFlgTelefone: TIntegerField;
    qryListadDtEncerramento: TDateTimeField;
    qryListacFlgNegativar: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure CancelarLista1Click(Sender: TObject);
    procedure EncerrarLista1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure GerenciarItens1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAdmListaCobranca_Listas: TfrmAdmListaCobranca_Listas;

implementation

uses fMenu, fListaCobranca_Itens, fLookup_Padrao;

{$R *.dfm}

procedure TfrmAdmListaCobranca_Listas.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Align            := alClient;
  DBGridEh1.Columns[6].Width := 154 ;

end;

procedure TfrmAdmListaCobranca_Listas.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryLista.Close ;
  qryLista.Open ;
  
end;

procedure TfrmAdmListaCobranca_Listas.CancelarLista1Click(Sender: TObject);
begin
  inherited;

  if (qryLista.Active) and (qryListanCdListaCobranca.Value > 0) then
  begin
  
      if (MessageDlg('Confirma o cancelamento desta lista de cobran�a?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE ListaCobranca SET dDtCancel = GetDate(), nCdUsuarioCancel = ' + intToStr(frmMenu.nCdUsuarioLogado) + ' WHERE nCdListaCobranca = ' + qryListanCdListaCobranca.asString) ;
      qryAux.ExecSQL;

      qryLista.Requery();

  end ;

end;

procedure TfrmAdmListaCobranca_Listas.EncerrarLista1Click(Sender: TObject);
begin
  inherited;
  if (qryLista.Active) and (qryListanCdListaCobranca.Value > 0) then
  begin
  
      if (MessageDlg('Confirma o encerramento desta lista de cobran�a?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE ListaCobranca SET dDtEncerramento = GetDate() WHERE nCdListaCobranca = ' + qryListanCdListaCobranca.asString) ;
      qryAux.ExecSQL;

      qryLista.Requery();

  end ;

end;

procedure TfrmAdmListaCobranca_Listas.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmListaCobranca_Itens;
begin
  inherited;
  if (qryLista.Active) then
  begin
      objForm := TfrmListaCobranca_Itens.Create(Self);

      objForm.ExibeItensLista(qryListanCdListaCobranca.Value
                             ,True
                             ,((qryListacFlgTelefone.Value=1) or (qryListadDtEncerramento.asString <> ''))
                             ,(qryListacFlgNegativar.Value = 1)) ;
      qryLista.Requery();
      FreeAndNil(objForm);
  end ;

end;

procedure TfrmAdmListaCobranca_Listas.GerenciarItens1Click(
  Sender: TObject);
var
    nPK : integer;
begin
  inherited;

  if (qryLista.Active) then
  begin

      if (qryListanCdUsuarioResp.Value > 0) then
      begin
          if (MessageDlg('Esta lista est� atribuida a(o) analista ' + qryListacNmUsuario.Value + '. Deseja atribuir a outra(o) analista ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
              exit ;
      end ;

      nPK := frmLookup_Padrao.ExecutaConsulta(197);

      If (nPK > 0) and (nPK <> qryListanCdUsuarioResp.Value) then
      begin
        qryAux.Close;
        qryAux.SQL.clear;
        qryAux.SQL.Add('SELECT cNmUsuario FROM Usuario WHERE nCdUsuario = ' + intToStr(nPK)) ;
        qryAux.Open;

        if not qryAux.eof then
        begin
          if (MessageDlg('Confirma a atribui��o desta lista a(o) analista ' + qryAux.FieldList[0].Value + ' ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
              exit ;
        end ;

        qryAux.Close;
        qryAux.SQL.Clear;
        qryAux.SQL.Add('UPDATE ListaCobranca SET dDtAtribuicao = GetDate(), nCdUsuarioresp = ' + intToStr(nPK) + ' WHERE nCdListaCobranca = ' + qryListanCdListaCobranca.asString) ;
        qryAux.ExecSQL;

        qryLista.Requery();

      end ;

  end ;

end;

end.
