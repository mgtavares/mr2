unit fEstornoBaixaProvisao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping;

type
  TfrmEstornoBaixaProvisao = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryProvisaoTit: TADOQuery;
    dsProvisaoTit: TDataSource;
    qryProvisaoTitnCdProvisaoTit: TAutoIncField;
    qryProvisaoTitdDtBaixa: TDateTimeField;
    qryProvisaoTitcNmUsuarioBaixa: TStringField;
    qryProvisaoTitnCdFormaPagto: TIntegerField;
    qryProvisaoTitcNmFormaPagto: TStringField;
    qryProvisaoTitiNrCheque: TIntegerField;
    qryProvisaoTitnValProvisao: TBCDField;
    qryProvisaoTitnCdContaBancaria: TIntegerField;
    qryProvisaoTitnCdBanco: TIntegerField;
    qryProvisaoTitcAgencia: TIntegerField;
    qryProvisaoTitnCdConta: TStringField;
    qryProvisaoTitcNmTitular: TStringField;
    SP_ESTORNA_BAIXA_PROVISAO: TADOStoredProc;
    qryProvisaoTitdDtPagto: TDateTimeField;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEstornoBaixaProvisao: TfrmEstornoBaixaProvisao;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmEstornoBaixaProvisao.FormShow(Sender: TObject);
begin
  inherited;

  qryProvisaoTit.Close ;
  qryProvisaoTit.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryProvisaoTit.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryProvisaoTit.Open ;

  DBGridEh1.Align := alClient ;
  
end;

procedure TfrmEstornoBaixaProvisao.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (qryProvisaoTit.eof) or (qryProvisaoTitnCdProvisaoTit.Value = 0) then
  begin
      MensagemAlerta('Selecione uma provis�o.') ;
      exit ;
  end ;

  case MessageDlg('Confirma o estorno da baixa desta provis�o ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      SP_ESTORNA_BAIXA_PROVISAO.Close ;
      SP_ESTORNA_BAIXA_PROVISAO.Parameters.ParamByName('@nCdProvisaoTit').Value := qryProvisaoTitnCdProvisaoTit.Value ;
      SP_ESTORNA_BAIXA_PROVISAO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryProvisaoTit.Close ;
  qryProvisaoTit.Open ;

  ShowMessage('Baixa da Provisao estornada com sucesso.') ;

end;

initialization
    RegisterClass(TfrmEstornoBaixaProvisao) ;

end.
