unit fClassifcCRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, DBCtrlsEh,
  DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, DBLookupEh, Menus;

type
  TfrmClassifcCRM = class(TfrmCadastro_Padrao)
    qryMasternCdClassificCRM: TIntegerField;
    qryMastercNmClassificCRM: TStringField;
    qryMasterdDtValidadeIni: TDateTimeField;
    qryMasternCdStatus: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    qryMasterdDtValidadeFim: TDateTimeField;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    qryStatus: TADOQuery;
    dsStatus: TDataSource;
    qryStatuscNmStatus: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    qryClassificVar: TADOQuery;
    dsClassificVar: TDataSource;
    Image6: TImage;
    qryInsereClassificVar: TADOQuery;
    qryClassificBrinde: TADOQuery;
    dsClassificBrinde: TDataSource;
    qryClassificBrindenCdClassificBrinde: TIntegerField;
    qryClassificBrindenCdClassificCRM: TIntegerField;
    qryClassificBrindenCdTipoClassificCRM: TIntegerField;
    qryClassificBrindenCdProduto: TIntegerField;
    qryClassificBrindenQtde: TIntegerField;
    qryProduto: TADOQuery;
    qryProdutocNmProduto: TStringField;
    qryClassificBrindecNmProduto: TStringField;
    qryAux: TADOQuery;
    qryClassificVarBonific: TADOQuery;
    dsClassificVarBonific: TDataSource;
    DBCheckBoxEh1: TDBCheckBoxEh;
    DBCheckBoxEh2: TDBCheckBoxEh;
    DBCheckBoxEh4: TDBCheckBoxEh;
    Label14: TLabel;
    DBEdit12: TDBEdit;
    DBEdit8: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    qryClassificVarnCdClassificVarCRM: TIntegerField;
    qryClassificVarcApelidoTipoClassific: TStringField;
    qryClassificVarnCdClassificCRM: TIntegerField;
    qryClassificVarnCdTipoClassificCRM: TIntegerField;
    qryClassificVarnValDinheiro: TBCDField;
    qryClassificVarnValCheque: TBCDField;
    qryClassificVarnValCartaoCredito: TBCDField;
    qryClassificVarnValCartaoDebito: TBCDField;
    qryClassificVarnValCrediario: TBCDField;
    qryClassificVarnMediaCompras: TBCDField;
    qryClassificVarnPercVaricaoFormapPagto: TBCDField;
    qryClassificVarnLimiteCredito: TBCDField;
    qryClassificVarnDescontoBonific: TBCDField;
    qryClassificVarnValeBonific: TBCDField;
    qryClassificVarnLimiteCreditoBonific: TBCDField;
    qryClassificVariDiasValidBonific: TIntegerField;
    qryMastercFlgReabilitado: TIntegerField;
    qryMastercFlgParcelaAtrasada: TIntegerField;
    qryMastercFlgConsiderarVendedor: TIntegerField;
    qryMastercFlgCadastroAtivo: TIntegerField;
    qryMasterdDtPeriodoCadIni: TDateTimeField;
    qryMasterdDtPeriodoCadFim: TDateTimeField;
    qryMasteriDiasMinReabilitado: TIntegerField;
    qryClassificVarcNmTipoClassificCRM: TStringField;
    GroupBox3: TGroupBox;
    DBGridEh2: TDBGridEh;
    GroupBox1: TGroupBox;
    DBGridEh1: TDBGridEh;
    DBGridEh3: TDBGridEh;
    qryClassificVarBonificnLimiteCreditoBonific: TBCDField;
    qryClassificVarBonificnDescontoBonific: TBCDField;
    qryClassificVarBonificnValeBonific: TBCDField;
    qryClassificVarBonificiDiasValidBonific: TIntegerField;
    Label9: TLabel;
    qryMasterdDtPeriodoCompraIni: TDateTimeField;
    qryMasterdDtPeriodoCompraFim: TDateTimeField;
    DBEdit9: TDBEdit;
    Label11: TLabel;
    DBEdit10: TDBEdit;
    Label12: TLabel;
    qryClassificVarnQtdeCliente: TIntegerField;
    qryClassificVarnPercDinheiro: TIntegerField;
    qryClassificVarnPercCrediario: TIntegerField;
    qryClassificVarnPercCheque: TIntegerField;
    qryClassificVarnPercCartaoCredito: TIntegerField;
    qryClassificVarnPercCartaoDebito: TIntegerField;
    qryClassificVarnPercMediaCompra: TIntegerField;
    qryClassificVarnPercDiasAtraso: TIntegerField;
    qryClassificVarnPercLimiteCredito: TIntegerField;
    qryClassificVarnDiasAtrasoTitulo: TIntegerField;
    qryClassificVarnPercVaicacaoAprov: TIntegerField;
    qryTabSatusClassificCRM: TADOQuery;
    dsTabSatusClassificCRM: TDataSource;
    qryTabSatusClassificCRMnCdTabStatusClassificCRM: TIntegerField;
    qryTabSatusClassificCRMcNmTabStatusClassificCRM: TStringField;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    ToolButton3: TToolButton;
    PopupMenu1: TPopupMenu;
    Ativar1: TMenuItem;
    Suspender1: TMenuItem;
    qryMasternCdTabStatusClassificCRM: TIntegerField;
    qryMasterdDtAtivacao: TDateTimeField;
    qryMasternCdUsuarioAtivacao: TIntegerField;
    qryMasterdDtSuspendido: TDateTimeField;
    qryMasternCdUsuarioSuspendido: TIntegerField;
    qryMastercOBSSuspendido: TStringField;
    DBEdit6: TDBEdit;
    Label6: TLabel;
    DBEdit11: TDBEdit;
    Label8: TLabel;
    DBEdit14: TDBEdit;
    qryUsuarioAtivacao: TADOQuery;
    qryUsuarioSuspenso: TADOQuery;
    dsUsuarioAtivacao: TDataSource;
    dsUsuarioSuspenso: TDataSource;
    qryUsuarioAtivacaocNmUsuario: TStringField;
    qryUsuarioSuspensocNmUsuario: TStringField;
    DBEdit13: TDBEdit;
    DBEdit15: TDBEdit;
    Reabertura1: TMenuItem;
    qryVerificaValidade: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure CarregaClassificCRM();
    procedure btIncluirClick(Sender: TObject);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryClassificBrindeBeforePost(DataSet: TDataSet);
    procedure qryClassificBrindeCalcFields(DataSet: TDataSet);
    procedure cxPageControl1Enter(Sender: TObject);
    procedure DBCheckBoxEh4MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGridEh1CellClick(Column: TColumnEh);
    procedure qryClassificVarBonificBeforePost(DataSet: TDataSet);
    procedure qryClassificVarBeforePost(DataSet: TDataSet);
    procedure ValidaVariaveis(nValor : double; nPerc : integer; cTipo : String);
    procedure Ativar1Click(Sender: TObject);
    procedure Suspender1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure qryMasterAfterOpen(DataSet: TDataSet);
    procedure Reabertura1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    bClassific : boolean;
    nPK        : integer;
  end;

var
  frmClassifcCRM: TfrmClassifcCRM;

implementation

uses fLookup_Padrao,fMenu, Math;

{$R *.dfm}

procedure TfrmClassifcCRM.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CLASSIFICCRM' ;
  nCdTabelaSistema  := 544 ;
  nCdConsultaPadrao := 786 ;
  bLimpaAposSalvar  := false;
end;

procedure TfrmClassifcCRM.CarregaClassificCRM();
begin

  {-- Carrega as variaveis --}
  qryClassificVar.Close;
  qryClassificVar.Parameters.ParamByName('nCdClassificCRM').Value     := qryMasternCdClassificCRM.Value;
  qryClassificVar.Open;

    if qryClassificVar.IsEmpty then
        bClassific := true
    else
        bClassific := false;
end;



procedure TfrmClassifcCRM.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);

            If (nPK > 0) then
            begin
                qryMasternCdStatus.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmClassifcCRM.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  qryStatus.Close ;
  PosicionaQuery(qryStatus,qryMasternCdStatus.AsString) ;

end;

procedure TfrmClassifcCRM.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryStatus.Close;
  qryClassificVar.Close;
  qryClassificBrinde.Close;
  qryTabSatusClassificCRM.Close;
  qryUsuarioAtivacao.Close;
  qryUsuarioSuspenso.Close;

  cxPageControl1.ActivePageIndex := 0 ;
end;

procedure TfrmClassifcCRM.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit;

  PosicionaQuery(qryStatus,qryMasternCdStatus.AsString);
  PosicionaQuery(qryTabSatusClassificCRM,qryMasternCdTabStatusClassificCRM.AsString);
  PosicionaQuery(qryUsuarioAtivacao,qryMasternCdUsuarioAtivacao.AsString);
  PosicionaQuery(qryUsuarioSuspenso,qryMasternCdUsuarioSuspendido.AsString);
  CarregaClassificCRM();

  if qryMasternCdTabStatusClassificCRM.Value > 1 then
  begin
      DBGridEh1.AllowedOperations := [];
      DBGridEh2.AllowedOperations := [];
      DBGridEh3.AllowedOperations := [];
  end
  else
  begin
      DBGridEh1.AllowedOperations := [alopUpdateEh];
      DBGridEh2.AllowedOperations := [alopUpdateEh];
      DBGridEh3.AllowedOperations := [alopUpdateEh];
  end;

end;

procedure TfrmClassifcCRM.qryMasterBeforePost(DataSet: TDataSet);
begin

  if qryMasternCdTabStatusClassificCRM.Value > 1 then
  begin
      MensagemAlerta('Status da classifica��o n�o permite altera��o.');
      Abort;
  end;

  if (qryMastercNmClassificCRM.Value = '') then
  begin
      MensagemAlerta('Descri��o da classifica��o CRM n�o informada.');
      DBEdit2.SetFocus;
      Abort;
  end;

  if (DBEdit3.Text = '  /  /    ') or (DBEdit5.Text = '  /  /    ') then
  begin
      MensagemAlerta('Validade da classifica��o n�o informada.');
      DBEdit3.SetFocus;
      Abort;
  end;

  if (qryMasterdDtValidadeIni.Value > qryMasterdDtValidadeFim.Value) then
  begin
      MensagemAlerta('Data inicial informada � maior que data final.');
      DBEdit3.SetFocus;
      Abort;
  end;

  qryVerificaValidade.Close;
  qryVerificaValidade.Parameters.ParamByName('dDtIni').Value := DBEdit3.Text;
  qryVerificaValidade.Parameters.ParamByName('dDtFim').Value := DBEdit5.Text;  
  qryVerificaValidade.Open;

  if not qryVerificaValidade.IsEmpty then
  begin
      MensagemAlerta('Validade informada invalida. Existe outra classifica��o dentro deste periodo informado.');
      DBEdit3.SetFocus;
      Abort;
  end;

  if qryMaster.State = dsInsert then
  begin
      qryMasternCdTabStatusClassificCRM.Value := 1;
  end;

  inherited;

end;

procedure TfrmClassifcCRM.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  try
      {-- Se for igual a true quer dizer que n�o foi inserido as classifica��es --}
      if (bClassific = true) then
      begin
          nPK := qryMasternCdClassificCRM.Value;
          
          qryInsereClassificVar.Close;
          qryInsereClassificVar.Parameters.ParamByName('nPK').Value := nPK;
          qryInsereClassificVar.ExecSQL;

          qryMaster.Close;
          qryMaster.Parameters.ParamByName('nPK').Value := nPK;
          qryMaster.Open;

      end;

  except
      MensagemErro('Erro no processamento.');
      raise;
  end;

end;

procedure TfrmClassifcCRM.btIncluirClick(Sender: TObject);
begin
  inherited;
  bClassific := true;
end;

procedure TfrmClassifcCRM.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh2.Col = 1) then
        begin

            if (qryClassificBrinde.State = dsBrowse) then
                 qryClassificBrinde.Edit ;

            if (qryClassificBrinde.State = dsInsert) or (qryClassificBrinde.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(76);

                If (nPK > 0) then
                begin
                    qryClassificBrindenCdProduto.Value := nPK;
                end ;

            end ;
        end;
    end ;

  end ;
end;

procedure TfrmClassifcCRM.qryClassificBrindeBeforePost(DataSet: TDataSet);
begin

  if not (qryMaster.Active) then
      exit;

  if qryClassificBrinde.State = dsInsert then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT 1 FROM ClassificBrinde WHERE nCdProduto = ' + qryClassificBrindenCdProduto.AsString + ' AND nCdClassificCRM = ' + qryMasternCdClassificCRM.AsString + ' AND nCdTipoClassificCRM = ' + IntToStr(qryClassificVarnCdTipoClassificCRM.Value));
      qryAux.Open ;

      if not qryAux.IsEmpty then
      begin
          MensagemAlerta('Produto j� incluso como brinde.');
          Abort;
      end;
  end;

  if qryClassificBrindenQtde.Value = 0 then
      qryClassificBrindenQtde.Value := 1;

  qryClassificBrindenCdClassificBrinde.Value  := frmMenu.fnProximoCodigo('CLASSIFICBRINDE');
  qryClassificBrindenCdClassificCRM.Value     := qryMasternCdClassificCRM.Value;
  qryClassificBrindenCdTipoClassificCRM.Value := qryClassificVarnCdTipoClassificCRM.Value;

  inherited;
end;


procedure TfrmClassifcCRM.qryClassificBrindeCalcFields(DataSet: TDataSet);
begin
  if not qryMaster.Active then
      exit;

  PosicionaQuery(qryProduto,qryClassificBrindenCdProduto.AsString);

  if not qryProduto.IsEmpty then
      qryClassificBrindecNmProduto.Value := qryProdutocNmProduto.Value; 

end;

procedure TfrmClassifcCRM.cxPageControl1Enter(Sender: TObject);
begin
  if (qryMaster.State = dsInsert) then
  begin
      btSalvar.Click ;
  end ;
end;

procedure TfrmClassifcCRM.DBCheckBoxEh4MouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if DBCheckBoxEh4.Checked = true then
  begin
      ativaDBEdit(DBEdit12);
  end
  else
  begin
      desativaDBEdit(DBEdit12);
      qryMasteriDiasMinReabilitado.Value := 0;
  end;

end;

procedure TfrmClassifcCRM.DBGridEh1CellClick(Column: TColumnEh);
begin

  if not qryMaster.Active then
      Exit;

  qryClassificVarBonific.Close;
  qryClassificVarBonific.Parameters.ParamByName('nCdClassificCRM').Value     := qryMasternCdClassificCRM.Value;
  qryClassificVarBonific.Parameters.ParamByName('nCdTipoClassificCRM').Value := qryClassificVarnCdTipoClassificCRM.Value;
  qryClassificVarBonific.Open;

  qryClassificBrinde.Close;
  qryClassificBrinde.Parameters.ParamByName('nCdClassificCRM').Value         := qryMasternCdClassificCRM.Value;
  qryClassificBrinde.Parameters.ParamByName('nCdTipoClassificCRM').Value     := qryClassificVarnCdTipoClassificCRM.Value;
  qryClassificBrinde.Open;

end;

procedure TfrmClassifcCRM.qryClassificVarBonificBeforePost(
  DataSet: TDataSet);
begin

  if ((qryMasterdDtValidadeIni.Value + qryClassificVarBonificiDiasValidBonific.Value) > qryMasterdDtValidadeFim.Value) then
  begin
      MensagemAlerta('Limite de dias da bonifica��o n�o pode ser superior a validade da classifica��o. ');
      Abort;
  end;

  inherited;

end;

procedure TfrmClassifcCRM.ValidaVariaveis(nValor : double; nPerc : integer; cTipo : String);
begin

  if (nValor > 0) then
  begin
      if (nPerc = 0) then
      begin
         MensagemAlerta('Peso ' + cTipo + ' n�o informado.');
         Abort;
      end;
  end
  else
  begin
      if (nPerc > 0) then
      begin
          MensagemAlerta('Informe o valor ' + cTipo + ' para inserir o peso.');
          Abort;
      end;
  end;

end;

procedure TfrmClassifcCRM.qryClassificVarBeforePost(DataSet: TDataSet);
begin

  ValidaVariaveis(qryClassificVarnValDinheiro.Value      ,qryClassificVarnPercDinheiro.Value      ,'Dinheiro'         );
  ValidaVariaveis(qryClassificVarnValCheque.Value        ,qryClassificVarnPercCheque.Value        ,'Cheque'           );
  ValidaVariaveis(qryClassificVarnValCartaoCredito.Value ,qryClassificVarnPercCartaoCredito.Value ,'Cart�o de Credito');
  ValidaVariaveis(qryClassificVarnValCartaoDebito.Value  ,qryClassificVarnPercCartaoDebito.Value  ,'Cart�o de Debito' );
  ValidaVariaveis(qryClassificVarnValCrediario.Value     ,qryClassificVarnPercCrediario.Value     ,'Cr�dito'          );
  ValidaVariaveis(qryClassificVarnMediaCompras.Value     ,qryClassificVarnPercMediaCompra.Value   ,'M�dia de Compras' );
  ValidaVariaveis(qryClassificVarnLimiteCredito.Value    ,qryClassificVarnPercLimiteCredito.Value ,'Limite de Credito');
  ValidaVariaveis(qryClassificVarnDiasAtrasoTitulo.Value ,qryClassificVarnPercDiasAtraso.Value    ,'Dias atraso'      );

  if qryClassificVarnQtdeCliente.Value = 0 then
  begin
      MensagemAlerta('Quantidade de clientes por tipo de classifica��o n�o informado.');
      Abort;
  end;

  if (qryClassificVarnPercDinheiro.Value + qryClassificVarnPercCheque.Value + qryClassificVarnPercCartaoCredito.Value + qryClassificVarnPercCartaoDebito.Value + qryClassificVarnPercCrediario.Value + qryClassificVarnPercMediaCompra.Value + qryClassificVarnPercDiasAtraso.Value + qryClassificVarnPercLimiteCredito.Value) <> 100 then
  begin
      MensagemAlerta('Somat�ria dos pesos difere de 100%.');
      Abort;
  end;

  inherited;

end;

procedure TfrmClassifcCRM.Ativar1Click(Sender: TObject);
begin

  if not qryMaster.Active then
      Exit;

  if qryMasternCdTabStatusClassificCRM.Value in [2,3,4] then
  begin
      MensagemAlerta('Status n�o permite ativar classifica��o ' + qryMasternCdClassificCRM.AsString + '.');
      Abort;
  end;

  case MessageDlg('Confirma ativa��o da classifica��o?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      frmMenu.Connection.BeginTrans;

      qryAux.Close;
      qryAux.SQL.Add('UPDATE ClassificCRM                                                      ');
      qryAux.SQL.Add('   SET nCdTabStatusClassificCRM = 2                                      ');
      qryAux.SQL.Add('      ,dDtAtivacao              = GETDATE()                              ');
      qryAux.SQL.Add('      ,nCdUsuarioAtivacao       = ' + IntToStr(frmMenu.nCdUsuarioLogado)  );
      qryAux.SQL.Add(' WHERE nCdClassificCRM          = ' + qryMasternCdClassificCRM.AsString   );
      qryAux.ExecSQL;

      frmMenu.Connection.CommitTrans;

      qryMaster.Close;
      qryMaster.Parameters.ParamByName('nPK').Value := nPK;
      qryMaster.Open;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      raise;
  end;
end;

procedure TfrmClassifcCRM.Suspender1Click(Sender: TObject);
begin
  if not qryMaster.Active then
      Exit;

  if qryMasternCdTabStatusClassificCRM.Value = 4 then
  begin
      MensagemAlerta('Classifica��o ' + qryMasternCdClassificCRM.AsString + 'j� suspensa anteriormente.');
      Abort;
  end;

  qryAux.Close;
  qryAux.SQL.Add('SELECT TOP 1 1 FROM HistClassificCRM  WHERE nCdClassificCRM = ' + qryMasternCdClassificCRM.AsString);
  qryAux.Open;

  if not qryAux.IsEmpty then
  begin
        MensagemAlerta('                         **** ATEN��O **** ' + chr(13) + chr(13) + 'Classifica��o processada anteriormente, todos os clientes j� classificados e seus direitos ser�o suspenso.');

  end;

  case MessageDlg('Confirma a suspens�o?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      frmMenu.Connection.BeginTrans;

      qryAux.Close;
      qryAux.SQL.Add('UPDATE ClassificCRM                                                      ');
      qryAux.SQL.Add('   SET nCdTabStatusClassificCRM = 4                                      ');
      qryAux.SQL.Add('      ,dDtSuspendido            = GETDATE()                              ');
      qryAux.SQL.Add('      ,nCdUsuarioSuspendido     = ' + IntToStr(frmMenu.nCdUsuarioLogado)  );
      qryAux.SQL.Add(' WHERE nCdClassificCRM          = ' + qryMasternCdClassificCRM.AsString   );
      qryAux.ExecSQL;

      qryAux.Close;
      qryAux.SQL.Add('UPDATE HistClassificCRM                                              ');
      qryAux.SQL.Add('   SET cFlgSuspendido       = 1                                      ');
      qryAux.SQL.Add('      ,cFlgVigencia         = 0                                      ');
      qryAux.SQL.Add('      ,dDtSuspendido        = GETDATE()                              ');
      qryAux.SQL.Add('      ,nCdUsuarioSuspendido = ' + IntToStr(frmMenu.nCdUsuarioLogado)  );
      qryAux.SQL.Add(' WHERE nCdClassificCRM      = ' + qryMasternCdClassificCRM.AsString   );
      qryAux.ExecSQL;

      qryAux.Close;
      qryAux.SQL.Add('UPDATE Terceiro                                                    ');
      qryAux.SQL.Add('   SET nCdClassificCRM     = NULL                                  ');
      qryAux.SQL.Add('      ,nCdTipoClassificCRM = NULL                                  ');
      qryAux.SQL.Add(' WHERE nCdClassificCRM     = ' + qryMasternCdClassificCRM.AsString  );
      qryAux.ExecSQL;

      frmMenu.Connection.CommitTrans;

      qryMaster.Close;
      qryMaster.Parameters.ParamByName('nPK').Value := nPK;
      qryMaster.Open;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      raise;
  end;

end;

procedure TfrmClassifcCRM.PopupMenu1Popup(Sender: TObject);
begin
  if qryMasternCdTabStatusClassificCRM.Value in [2,3] then
      PopupMenu1.Items.Items[0].Enabled := false
  else
     PopupMenu1.Items.Items[0].Enabled := true;
end;

procedure TfrmClassifcCRM.qryMasterAfterOpen(DataSet: TDataSet);
begin
  nPK := qryMasternCdClassificCRM.Value;
end;

procedure TfrmClassifcCRM.Reabertura1Click(Sender: TObject);
begin

  if not qryMaster.Active then
      Exit;

  if qryMasternCdTabStatusClassificCRM.Value > 2 then
  begin
      MensagemAlerta('Status da classifica��o n�o permite reabertura.');
      Abort;
  end;

  qryAux.Close;
  qryAux.SQL.Add('SELECT TOP 1 1 FROM HistClassificCRM  WHERE nCdClassificCRM = ' + qryMasternCdClassificCRM.AsString);
  qryAux.Open;

  if not qryAux.IsEmpty then
  begin
        MensagemAlerta('                         **** ATEN��O **** ' + chr(13) + chr(13) + 'Classifica��o processada anteriormente, n�o ser� possivel reabrir, suspenda e crie uma nova classifica��o..');
        Abort;
  end;

  case MessageDlg('Confirma reabertura da classifica��o?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      frmMenu.Connection.BeginTrans;

      qryAux.Close;
      qryAux.SQL.Add('UPDATE ClassificCRM                                                    ');
      qryAux.SQL.Add('   SET nCdTabStatusClassificCRM = 1                                    ');
      qryAux.SQL.Add('      ,dDtSuspendido            = NULL                                 ');
      qryAux.SQL.Add('      ,nCdUsuarioSuspendido     = NULL'                                 );
      qryAux.SQL.Add(' WHERE nCdClassificCRM          = ' + qryMasternCdClassificCRM.AsString );
      qryAux.ExecSQL;

      frmMenu.Connection.CommitTrans;

      qryMaster.Close;
      qryMaster.Parameters.ParamByName('nPK').Value := nPK;
      qryMaster.Open;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      raise;
  end;

  ShowMessage('Classifica��o reaberta com sucesso.');


end;

initialization
    RegisterClass(TfrmClassifcCRM);
end.



















