inherited rptLiberacaoDiverReceb: TrptLiberacaoDiverReceb
  Left = 277
  Top = 265
  Width = 948
  Height = 331
  Caption = 'Relat'#243'rio - Libera'#231#227'o Diverg'#234'ncia Recebimento'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 932
    Height = 269
  end
  object Loja: TLabel [1]
    Left = 91
    Top = 48
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label1: TLabel [2]
    Left = 72
    Top = 72
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
  end
  object Label2: TLabel [3]
    Left = 26
    Top = 96
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Recebimento'
    FocusControl = DBEdit4
  end
  object Label3: TLabel [4]
    Left = 32
    Top = 120
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Diverg'#234'ncia'
  end
  object Label4: TLabel [5]
    Left = 15
    Top = 144
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio Autoriza'#231#227'o'
  end
  object Label5: TLabel [6]
    Left = 15
    Top = 192
    Width = 96
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Autoriza'#231#227'o'
  end
  object Label6: TLabel [7]
    Left = 194
    Top = 192
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label7: TLabel [8]
    Left = 10
    Top = 168
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Recebimento'
  end
  object Label8: TLabel [9]
    Left = 194
    Top = 168
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    Width = 932
    TabOrder = 11
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [11]
    Tag = 1
    Left = 184
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 12
  end
  object DBEdit4: TDBEdit [12]
    Tag = 1
    Left = 184
    Top = 88
    Width = 654
    Height = 21
    DataField = 'cNmTipoReceb'
    DataSource = dsTipoReceb
    TabOrder = 13
  end
  object ER2LookupLoja: TER2LookupMaskEdit [13]
    Left = 116
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  object ER2LookupTerceiro: TER2LookupMaskEdit [14]
    Left = 116
    Top = 64
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    CodigoLookup = 173
    QueryLookup = qryTerceiro
  end
  object ER2LookupTipoReceb: TER2LookupMaskEdit [15]
    Left = 116
    Top = 88
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    CodigoLookup = 88
    QueryLookup = qryTipoReceb
  end
  object ER2LookupTipoDiverg: TER2LookupMaskEdit [16]
    Left = 116
    Top = 112
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 3
    Text = '         '
    CodigoLookup = 99
    QueryLookup = qryTabTipoDiverg
  end
  object ER2LookupUsuarioAutori: TER2LookupMaskEdit [17]
    Left = 116
    Top = 136
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 4
    Text = '         '
    CodigoLookup = 5
    QueryLookup = qryUsuario
  end
  object DBEdit3: TDBEdit [18]
    Tag = 1
    Left = 184
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 14
  end
  object DBEdit6: TDBEdit [19]
    Tag = 1
    Left = 184
    Top = 136
    Width = 654
    Height = 21
    DataField = 'cNmUsuario'
    DataSource = dsUsuario
    TabOrder = 15
  end
  object DBEdit5: TDBEdit [20]
    Tag = 1
    Left = 184
    Top = 112
    Width = 654
    Height = 21
    DataField = 'cNmTabTipoDiverg'
    DataSource = dsTabTipoDiverg
    TabOrder = 16
  end
  object edtDtInicialAutorizacao: TMaskEdit [21]
    Left = 116
    Top = 184
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object edtDtFinalAutorizacao: TMaskEdit [22]
    Left = 216
    Top = 184
    Width = 71
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
  end
  object edtDtInicialReceb: TMaskEdit [23]
    Left = 116
    Top = 160
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 5
    Text = '  /  /    '
  end
  object edtDtFinalReceb: TMaskEdit [24]
    Left = 216
    Top = 160
    Width = 71
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 6
    Text = '  /  /    '
  end
  object rgModeloImp: TRadioGroup [25]
    Left = 324
    Top = 216
    Width = 185
    Height = 41
    Caption = ' Modelo '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Planilha')
    ParentColor = False
    TabOrder = 10
  end
  object rgRecebDiv: TRadioGroup [26]
    Left = 116
    Top = 216
    Width = 194
    Height = 41
    Caption = ' Somente Recebimentos Divergentes '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'N'#227'o'
      'Sim')
    ParentColor = False
    TabOrder = 9
  end
  inherited ImageList1: TImageList
    Left = 896
    Top = 40
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 832
    Top = 40
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmEmpresa'
      '  FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 864
    Top = 40
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 864
    Top = 72
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 832
    Top = 72
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTerceiro'
      '   FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 864
    Top = 104
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 832
    Top = 104
  end
  object dsTipoReceb: TDataSource
    DataSet = qryTipoReceb
    Left = 832
    Top = 136
  end
  object qryTipoReceb: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cNmTipoReceb'
      '   FROM TipoReceb'
      'WHERE nCdTipoReceb = :nPK')
    Left = 864
    Top = 136
    object qryTipoRecebcNmTipoReceb: TStringField
      FieldName = 'cNmTipoReceb'
      Size = 50
    end
  end
  object qryTabTipoDiverg: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTabTipoDiverg'
      '  FROM TabTipoDiverg'
      ' WHERE nCdTabTipoDiverg = :nPK')
    Left = 864
    Top = 168
    object qryTabTipoDivergcNmTabTipoDiverg: TStringField
      FieldName = 'cNmTabTipoDiverg'
      Size = 50
    end
  end
  object dsTabTipoDiverg: TDataSource
    DataSet = qryTabTipoDiverg
    Left = 832
    Top = 168
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmUsuario'
      '   FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 864
    Top = 200
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsUsuario: TDataSource
    DataSet = qryUsuario
    Left = 832
    Top = 200
  end
  object ER2Excel: TER2Excel
    Titulo = 'Rel. Libera'#231#227'o de Diverg'#234'ncia no Recebimento'
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 896
    Top = 72
  end
end
