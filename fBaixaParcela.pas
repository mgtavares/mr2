unit fBaixaParcela;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, cxLookAndFeelPainters, DB, DBCtrls, ADODB, cxButtons,
  GridsEh, DBGridEh, cxPC, cxControls, DBGridEhGrouping;

type
  TfrmBaixaParcela = class(TfrmProcesso_Padrao)
    qryParcelaAberto: TADOQuery;
    DataSource2: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    DataSource1: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    MaskEdit6: TMaskEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    cxButton1: TcxButton;
    qryParcelasPendentes: TADOQuery;
    qryParcelasPendentesnCdTitulo: TIntegerField;
    qryParcelasPendentesiparcela: TIntegerField;
    qryParcelasPendentesnValIndiceCorrecao: TBCDField;
    qryParcelasPendentesnValCorrecao: TBCDField;
    qryParcelasPendentesnSaldoTit: TBCDField;
    qryParcelasPendentesnValTit: TBCDField;
    qryParcelaAbertonCdTitulo: TIntegerField;
    qryParcelaAbertonCdEspTit: TIntegerField;
    qryParcelaAbertocNmEspTit: TStringField;
    qryParcelaAbertonCdSP: TIntegerField;
    qryParcelaAbertocNrTit: TStringField;
    qryParcelaAbertoiParcela: TIntegerField;
    qryParcelaAbertonCdTerceiro: TIntegerField;
    qryParcelaAbertocNmTerceiro: TStringField;
    qryParcelaAbertonCdMoeda: TIntegerField;
    qryParcelaAbertocSigla: TStringField;
    qryParcelaAbertocSenso: TStringField;
    qryParcelaAbertodDtVenc: TDateTimeField;
    qryParcelaAbertonValTit: TBCDField;
    qryParcelaAbertonSaldoTit: TBCDField;
    qryParcelaAbertodDtCancel: TDateTimeField;
    qryParcelaAbertocSiglaEmp: TStringField;
    qryParcelaAbertocNrNF: TStringField;
    qryParcelaAbertonCdProvisaoTit: TIntegerField;
    qryParcelaAbertodDtPagto: TDateTimeField;
    qryParcelaAbertonCdLojaTit: TIntegerField;
    qryParcelaAbertonValAbatimento: TBCDField;
    qryParcelaAbertonValDesconto: TBCDField;
    qryParcelaAbertonValLiq: TBCDField;
    qryParcelasPendentesnCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryParcelaAbertonCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBaixaParcela: TfrmBaixaParcela;

implementation

uses fMenu, fLookup_Padrao, fLiquidaParcela;

{$R *.dfm}

procedure TfrmBaixaParcela.MaskEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;
  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            MaskEdit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmBaixaParcela.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TfrmBaixaParcela.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmLiquidaParcela ;
begin
  inherited;

  qryParcelasPendentes.Close;
  qryParcelasPendentes.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(MaskEdit6.Text);
  qryParcelasPendentes.Parameters.ParamByName('cNrTit').Value      := qryParcelaAbertocNrTit.Value;
  qryParcelasPendentes.Parameters.ParamByName('iParcela').Value    := qryParcelaAbertoiParcela.Value;
  qryParcelasPendentes.Open;

  if ((not qryParcelasPendentes.Eof) and (qryParcelasPendentesnCdTabTipoParcContratoEmpImobiliario.Value = qryParcelaAbertonCdTabTipoParcContratoEmpImobiliario.Value)) then
  begin

      if (MessageDlg('Antecipa��o de Pagamento ?' + #13#13 ,mtConfirmation,[mbYes,mbNo],0) = MRNO) then
          exit;

  end;

  objForm := TfrmLiquidaParcela.Create(nil) ;

  PosicionaQuery(objForm.qryLiquidaParcela,qryParcelaAbertonCdTitulo.AsString);

  if not objForm.qryLiquidaParcela.Eof then
  begin

      showForm( objForm , TRUE ) ;

      // repoe a qry com sem a ultima parcela que foi liquidada.

      qryParcelaAberto.Close;
      qryParcelaAberto.Parameters.ParamByName('cSenso').Value      := 'C' ;
      qryParcelaAberto.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
      qryParcelaAberto.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(MaskEdit6.Text) ;
      qryParcelaAberto.Open;
      
  end;

end;


procedure TfrmBaixaParcela.cxButton1Click(Sender: TObject);
begin
  inherited;
  if ((MaskEdit6.Text = '') or (DBEdit3.Text = '')) then
  begin
     MensagemAlerta('Cliente n�o informado.') ;
     MaskEdit6.SetFocus;
     Abort;
  end;

  qryParcelaAberto.Close;
  qryParcelaAberto.Parameters.ParamByName('cSenso').Value := 'C' ;
  qryParcelaAberto.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryParcelaAberto.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  qryParcelaAberto.Open;

end;

initialization
     RegisterClass(TfrmBaixaParcela) ;

end.
