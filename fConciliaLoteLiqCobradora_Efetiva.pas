unit fConciliaLoteLiqCobradora_Efetiva;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls;

type
  TfrmConciliaLoteLiqCobradora_Efetiva = class(TfrmProcesso_Padrao)
    qryLoteLiqCobradora: TADOQuery;
    qryLoteLiqCobradoranCdLoteLiqCobradora: TIntegerField;
    qryLoteLiqCobradoracNrBorderoCob: TStringField;
    qryLoteLiqCobradoradDtFecham: TDateTimeField;
    qryLoteLiqCobradoranValTotal: TBCDField;
    qryLoteLiqCobradoranCdCobradora: TIntegerField;
    qryLoteLiqCobradoracNmCobradora: TStringField;
    qryLoteLiqCobradoracNmUsuario: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    edtContaBancaria: TMaskEdit;
    Label12: TLabel;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    DBEdit8: TDBEdit;
    DataSource2: TDataSource;
    MaskEdit1: TMaskEdit;
    SP_ACEITA_BORDERO_COBRADORA_FINANCEIRO: TADOStoredProc;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtContaBancariaExit(Sender: TObject);
    procedure edtContaBancariaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConciliaLoteLiqCobradora_Efetiva: TfrmConciliaLoteLiqCobradora_Efetiva;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmConciliaLoteLiqCobradora_Efetiva.FormShow(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close ;
  
  edtContaBancaria.Text := '' ;
  edtContaBancaria.SetFocus;

end;

procedure TfrmConciliaLoteLiqCobradora_Efetiva.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (DBedit8.Text = '') then
  begin
      MensagemAlerta('Selecione uma conta de cr�dito.') ;
      edtContaBancaria.SetFocus;
      abort ;
  end ;

  if (MessageDlg('O valor do border� ser� creditado na conta ' + DBEdit8.Text + '.  Confirma ?' +#13#13+'Este processo n�o poder� ser estornado.',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      SP_ACEITA_BORDERO_COBRADORA_FINANCEIRO.Close;
      SP_ACEITA_BORDERO_COBRADORA_FINANCEIRO.Parameters.ParamByName('@nCdLoteliqCobradora').Value := qryLoteLiqCobradoranCdLoteLiqCobradora.Value ;
      SP_ACEITA_BORDERO_COBRADORA_FINANCEIRO.Parameters.ParamByName('@nCdContaBancaria').Value    := frmMenu.ConvInteiro(edtContaBancaria.Text) ;
      SP_ACEITA_BORDERO_COBRADORA_FINANCEIRO.Parameters.ParamByName('@nCdUsuario').Value          := frmMenu.nCdUsuarioLogado;
      SP_ACEITA_BORDERO_COBRADORA_FINANCEIRO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Border� atualizado com sucesso. O saldo da conta foi atualizado.') ;

  Close;

end;

procedure TfrmConciliaLoteLiqCobradora_Efetiva.edtContaBancariaExit(
  Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close;
  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryContaBancaria.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  PosicionaQuery(qryContaBancaria, edtContaBancaria.Text) ;

  if not (qryContaBancaria.eof) then
      ToolButton1.Click;

end;

procedure TfrmConciliaLoteLiqCobradora_Efetiva.edtContaBancariaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
  nPK : integer ;
begin
  inherited;

  nPK := frmLookup_Padrao.ExecutaConsulta2(148,'ContaBancaria.nCdLoja = ' + intToStr(frmMenu.nCdLojaAtiva));

  if (nPK > 0) then
      edtContaBancaria.Text := intToStr(nPK) ;

end;

end.
