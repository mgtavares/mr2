unit rFichaCadastralCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, DB, ADODB, StdCtrls, Mask, ImgList, ComCtrls,
  ToolWin, ExtCtrls, DBCtrls;

type
  TrptFichaCadastralCliente = class(TfrmRelatorio_Padrao)
    edtTerceiro: TMaskEdit;
    Label1: TLabel;
    qryTerceiro: TADOQuery;
    dsTerceiro: TDataSource;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit1: TDBEdit;
    procedure ToolButton1Click(Sender: TObject);
    procedure edtTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFichaCadastralCliente: TrptFichaCadastralCliente;

implementation

    uses fMenu, fLookup_Padrao, rFichaCadastralCliente_view;

{$R *.dfm}

procedure TrptFichaCadastralCliente.ToolButton1Click(Sender: TObject);
var
  objForm : TrptFichaCadastralCliente_view;
begin
  inherited;

  if qryTerceiro.IsEmpty then
      MensagemAlerta('Informe o cliente.')
  else
  begin
      objForm := TrptFichaCadastralCliente_view.Create(nil);

      objForm.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

      objForm.qryTerceiro.Close;
      PosicionaQuery(objForm.qryTerceiro, qryTerceironCdTerceiro.AsString);

      objForm.qrySPCPessoaFisica.Close;
      PosicionaQuery(objForm.qrySPCPessoaFisica, qryTerceironCdTerceiro.AsString);

      objForm.QuickRep1.PreviewModal;

      FreeAndNil(objForm);
  end;
end;

procedure TrptFichaCadastralCliente.edtTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : integer;
begin
  inherited;

  if Key = VK_F4 then
  begin
      nPK := frmLookup_Padrao.ExecutaConsulta(2323);

      if nPK <> 0 then
      begin
          qryTerceiro.Close;
          PosicionaQuery(qryTerceiro, IntToStr(nPK));

          edtTerceiro.Text := IntToStr(nPK);
      end;
  end

  else if Key = VK_RETURN then
  begin
      qryTerceiro.Close;
      PosicionaQuery(qryTerceiro, Trim(edtTerceiro.Text));
  end;
end;

procedure TrptFichaCadastralCliente.FormShow(Sender: TObject);
begin
  inherited;

  edtTerceiro.SetFocus;
end;

initialization
    RegisterClass(TrptFichaCadastralCliente) ;

end.
