unit fEnviaEmailPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdMessageClient, IdMessage, IdSMTP, DB, ADODB, StdCtrls;

type
  TfrmEnviaEmailPadrao = class(TForm)
    Button1: TButton;
    Button2: TButton;
    function fnTestaEmail(cHost, cEmail, cSenha : String; iPorta : Integer) : Boolean;
    procedure prEnviaEmail(cHost, cEmail, cSenha, cEmailDest, cAssunto : String; iPorta : Integer; cMensagem : WideString; arrAnexos : array of string);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    idSMTP       : TIdSMTP;
    idMessage    : TIdMessage;
  end;

var
  frmEnviaEmailPadrao: TfrmEnviaEmailPadrao;

implementation

{$R *.dfm}

uses
  fMenu;
  
procedure TfrmEnviaEmailPadrao.prEnviaEmail(cHost, cEmail, cSenha, cEmailDest, cAssunto : String; iPorta : Integer; cMensagem : WideString; arrAnexos : array of string);
var
  i:integer;
begin
  if (Trim(cHost) = '') then
  begin
      Raise Exception.Create('Informe o host.');
      Exit;
  end;

  if (iPorta = 0) then
  begin
      Raise Exception.Create('Informe a porta do e-mail.');
      Exit;
  end;

  if (Trim(cEmail) = '') then
  begin
      Raise Exception.Create('Informe o endere�o de e-mail do remetente.');
      Exit;
  end;

  if (Trim(cSenha) = '') then
  begin
      Raise Exception.Create('Informe a senha do endere�o de e-mail do remetente.');
      Exit;
  end;

  if (Trim(cEmailDest) = '') then
  begin
      Raise Exception.Create('Informe o endere�o de e-mail do destinat�rio.');
      Exit;
  end;

  if (Trim(cAssunto) = '') then
  begin
      cAssunto := '[Sem Assunto]';
  end;

  frmMenu.mensagemUsuario('Configurando e-mail...');

  { -- configura conex�o para envio atrav�s do INDY -- }
  idSMTP.Host               := cHost;
  idSMTP.Port               := iPorta;
  idSMTP.Username           := cEmail;
  idSMTP.Password           := cSenha;
  idSMTP.AuthenticationType := atLogin;

  { -- prepara os dados da mensagem -- }
  idMessage.From.Address              := cEmail;
  idMessage.Recipients.EMailAddresses := cEmailDest;
  idMessage.Subject                   := cAssunto;
  idMessage.Body.Text                 := cMensagem;

  { -- anexa arquivos no e-mail -- }
  for i := 0 to Length(arrAnexos) - 1 do
  begin
      if (arrAnexos[i] <> '') then
          TIdAttachment.Create(idMessage.MessageParts, arrAnexos[i]);
  end;

  frmMenu.mensagemUsuario('Enviando e-mail...');

  { -- envia o email -- }
  try
      try
          idSMTP.Connect(2000);
          idSMTP.Send(idMessage);
      except
          Raise;
      end;
  finally
      frmMenu.mensagemUsuario('');
      if (idSMTP.Connected) then idSMTP.Disconnect;
  end;
end;

function TfrmEnviaEmailPadrao.fnTestaEmail(cHost, cEmail, cSenha : String; iPorta : Integer) : Boolean;
var
  bAutenticacao : Boolean;
begin
  bAutenticacao := False;
  
  // testa a configura��o do e-mail

  if trim(cHost) = '' then
      frmMenu.MensagemErro('Informe o Host do e-mail.')
  else if iPorta <= 0 then
      frmMenu.MensagemErro('Informe o n�mero da Porta do e-mail.')
  else if trim(cEmail) = '' then
      frmMenu.MensagemErro('Informe o e-mail de acesso!')
  else if trim(cSenha) = '' then
      frmMenu.MensagemErro('Informe a senha do e-mail!');

  try
      IdSMTP.Host               := cHost;
      IdSMTP.Port               := iPorta;
      IdSMTP.Username           := cEmail;
      IdSMTP.Password           := cSenha;
      IdSMTP.AuthenticationType := atLogin;

      // envia o e-mail
      try
          IdSMTP.Connect(0);
          IdSMTP.Authenticate;
          frmMenu.ShowMessage('Teste de e-mail enviado com sucesso!');

          bAutenticacao := True;
      except on E:Exception do
          frmMenu.MensagemErro('Erro ao efetuar teste de conex�o, erro: ' + E.Message);
      end;
  finally
      IdSMTP.Disconnect;
  end;

  Result := bAutenticacao;
end;

procedure TfrmEnviaEmailPadrao.Button2Click(Sender: TObject);
begin
   // prEnviaEmail();
end;

procedure TfrmEnviaEmailPadrao.FormCreate(Sender: TObject);
begin
  idSMTP    := TIdSMTP.Create(Nil);
  idMessage := TIdMessage.Create(Nil);
end;

procedure TfrmEnviaEmailPadrao.FormDestroy(Sender: TObject);
begin
  idSMTP.Free;
  idMessage.Free;
end;

end.
