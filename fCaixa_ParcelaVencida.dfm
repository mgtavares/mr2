inherited frmCaixa_ParcelaVencida: TfrmCaixa_ParcelaVencida
  Width = 843
  Caption = 'Parcelas Vencidas'
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 835
  end
  inherited ToolBar1: TToolBar
    Width = 835
    ButtonWidth = 118
    inherited ToolButton1: TToolButton
      Caption = 'E&xibir Movimento'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 118
    end
    inherited ToolButton2: TToolButton
      Left = 126
    end
  end
  object cxGrid2: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 835
    Height = 440
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    LookAndFeel.Kind = lfFlat
    LookAndFeel.NativeStyle = True
    object cxGridDBTableView1: TcxGridDBTableView
      OnDblClick = cxGridDBTableView1DblClick
      DataController.DataSource = dsParcelasVencidas
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nSaldoTit
        end
        item
          Format = ',0'
          Kind = skCount
          Column = cxGridDBTableView1nCdTitulo
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnFiltering = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLineColor = clSilver
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.ConteudoCaixa
      Styles.Header = frmMenu.HeaderCaixa
      object cxGridDBTableView1nCdTitulo: TcxGridDBColumn
        DataBinding.FieldName = 'nCdTitulo'
        Width = 69
      end
      object cxGridDBTableView1nCdLoja: TcxGridDBColumn
        DataBinding.FieldName = 'nCdLoja'
        Width = 49
      end
      object cxGridDBTableView1cNrTit: TcxGridDBColumn
        DataBinding.FieldName = 'cNrTit'
        Width = 71
      end
      object cxGridDBTableView1iParcela: TcxGridDBColumn
        DataBinding.FieldName = 'iParcela'
        Width = 47
      end
      object cxGridDBTableView1dDtEmissao: TcxGridDBColumn
        DataBinding.FieldName = 'dDtEmissao'
        Width = 85
      end
      object cxGridDBTableView1dDtVenc: TcxGridDBColumn
        DataBinding.FieldName = 'dDtVenc'
        Width = 85
      end
      object cxGridDBTableView1nValTit: TcxGridDBColumn
        DataBinding.FieldName = 'nValTit'
        HeaderAlignmentHorz = taRightJustify
        Width = 65
      end
      object cxGridDBTableView1nValJuro: TcxGridDBColumn
        DataBinding.FieldName = 'nValJuro'
        HeaderAlignmentHorz = taRightJustify
        Width = 65
      end
      object cxGridDBTableView1nValDesconto: TcxGridDBColumn
        DataBinding.FieldName = 'nValDesconto'
        HeaderAlignmentHorz = taRightJustify
        Width = 65
      end
      object cxGridDBTableView1nSaldoTit: TcxGridDBColumn
        DataBinding.FieldName = 'nSaldoTit'
        HeaderAlignmentHorz = taRightJustify
        Width = 65
      end
      object cxGridDBTableView1iDiasAtraso: TcxGridDBColumn
        DataBinding.FieldName = 'iDiasAtraso'
        HeaderAlignmentHorz = taRightJustify
        Width = 52
      end
      object cxGridDBTableView1cNmTerceiro: TcxGridDBColumn
        DataBinding.FieldName = 'cNmTerceiro'
      end
      object cxGridDBTableView1nCdLanctoFin: TcxGridDBColumn
        DataBinding.FieldName = 'nCdLanctoFin'
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000999999009999990099999900999999009999990000000000000000000000
      0000808080008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000009999
      9900000000000000000000000000000000009999990000000000000000008080
      8000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000E6E6E600E6E6E600E6E6E600E6E6E6000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000000000000E6E6E600E6E6
      E600E6E6E6000000000000000000E6E6E600E6E6E600E6E6E600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000E6E6E6000000
      000000000000D9D9D900D9D9D9000000000000000000E6E6E600000000009999
      9900999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000099999900D9D9D900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E60000000000E6E6E600E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000099999900E6E6E600E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E60000000000E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000000000009999990099999900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600D9D9D900E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000009999990099999900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000999999009999
      9900E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600000000009999
      9900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000999999009999
      99009999990099999900E6E6E600E6E6E600E6E6E600E6E6E600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999009999990099999900999999000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFF30000
      FE00C003FFE9000000008001F051000000008001E023000000008001C0270000
      00008001860F0000000080019987000000008001008700000000800100470000
      0000800100070000000080010007000000018001800F000000038001801F0000
      0077C003C03F0000007FFFFFF0FF000000000000000000000000000000000000
      000000000000}
  end
  object qryParcelasVencidas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja int'
      ''
      'SET @nCdLoja = :nCdLoja'
      ''
      '--'
      
        '-- Essa temporaria ja vem preenchida do caixa, s'#243' cria aqui por ' +
        'seguran'#231'a'
      '--'
      'IF (OBJECT_ID('#39'tempdb..#Temp_Titulos'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #Temp_Titulos (nCdTitulo           int'
      #9#9'  '#9'                       ,cNrTit              char(17)'
      #9#9#9'                         ,cNmEspTit           varchar(50)'
      '                               ,dDtEmissao          datetime'
      #9#9#9'                         ,dDtVenc             datetime'
      
        #9#9#9'                         ,nValTit             decimal(12,2) d' +
        'efault 0 not null'
      
        #9#9#9'                         ,nValJuro            decimal(12,2) d' +
        'efault 0 not null'
      
        #9#9#9'                         ,nValDesconto        decimal(12,2) d' +
        'efault 0 not null'
      
        #9#9#9'                         ,nSaldoTit           decimal(12,2) d' +
        'efault 0 not null'
      #9#9#9'                         ,cNmLoja             varchar(50)'
      
        #9#9#9'                         ,iDiasAtraso         int           d' +
        'efault 0 NOT NULL'
      
        '                               ,cFlgEntrada         int         ' +
        '  default 0 NOT NULL'
      '                               ,nCdCrediario        int'
      
        '                               ,iParcela            int         ' +
        '  default 0 NOT NULL'
      
        '                               ,cFlgMarcado         int         ' +
        '  default 0 NOT NULL'
      '                               ,nCdTerceiro         int'
      
        '                               ,cFlgDescontoJuros   int         ' +
        '  default 0 not null'
      
        '                               ,nValJurosDescontado decimal(12,2' +
        ') default 0 not null)'
      ''
      'END'
      ''
      '--'
      
        '-- Cria uma tabela temporaria e inseri os c'#243'digo de cliente das ' +
        'parcelas selecionadas'
      
        '-- Faz isso porque a caixa pode receber parcelas de diferentes c' +
        'lientes em uma mesma transa'#231#227'o.'
      '--'
      'IF (OBJECT_ID('#39'tempdb..#TempTerceiro'#39') IS NOT NULL)'
      '    DROP TABLE #TempTerceiro'
      ''
      'CREATE TABLE #TempTerceiro (nCdTerceiro int)'
      ''
      'INSERT INTO #TempTerceiro (nCdTerceiro)'
      '                    SELECT DISTINCT Titulo.nCdTerceiro'
      '                      FROM #Temp_Titulos Temp'
      
        '                           INNER JOIN Titulo ON Titulo.nCdTitulo' +
        ' = Temp.nCdTitulo'
      ''
      '--'
      
        '-- Seleciona as parcelas de crediario vencidas dos clientes acim' +
        'a filtrados'
      '-- cujas parcelas n'#227'o foram selecionadas pela caixa.'
      '--'
      'SELECT Titulo.nCdTitulo'
      '      ,dbo.fn_ZeroEsquerda(Titulo.nCdLojaTit,3) as nCdLoja'
      '      ,Titulo.cNrTit'
      '      ,Titulo.iParcela'
      '      ,Titulo.dDtEmissao'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValTit'
      
        '      ,dbo.fn_arredondaVarejo(dbo.fn_SimulaJurosTitulo(Titulo.nC' +
        'dTitulo,GetDate())+dbo.fn_SimulaMultaTitulo(Titulo.nCdTitulo)) a' +
        's nValJuro'
      '      ,Titulo.nValDesconto'
      
        '      ,Titulo.nSaldoTit+dbo.fn_arredondaVarejo(dbo.fn_SimulaJuro' +
        'sTitulo(Titulo.nCdTitulo,GetDate())+dbo.fn_SimulaMultaTitulo(Tit' +
        'ulo.nCdTitulo)) as nSaldoTit'
      
        '      ,CASE WHEN dbo.fn_OnlyDate(GetDate()) > dDtVenc THEN Conve' +
        'rt(int,dbo.fn_OnlyDate(GetDate())-dDtVenc)'
      '            ELSE 0'
      '       END iDiasAtraso'
      '      ,Terceiro.cNmTerceiro'
      '      ,Titulo.nCdLanctoFin'
      '  FROM Titulo'
      
        '       INNER JOIN #TempTerceiro Temp ON Temp.nCdTerceiro     = T' +
        'itulo.nCdTerceiro'
      
        '       INNER JOIN Terceiro           ON Terceiro.nCdTerceiro = T' +
        'itulo.nCdTerceiro'
      ' WHERE Titulo.nSaldoTit     > 0'
      '   AND Titulo.dDtCancel    IS NULL'
      '   AND Titulo.nCdCrediario IS NOT NULL'
      '   AND Titulo.cFlgCobradora = 0'
      '   AND Titulo.dDtVenc       < dbo.fn_OnlyDate(GetDate())'
      '   AND NOT EXISTS(SELECT 1'
      '                    FROM #Temp_Titulos'
      
        '                   WHERE #Temp_Titulos.nCdTitulo = Titulo.nCdTit' +
        'ulo)'
      '')
    Left = 216
    Top = 96
    object qryParcelasVencidasnCdTitulo: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'nCdTitulo'
    end
    object qryParcelasVencidasnCdLoja: TStringField
      DisplayLabel = 'Loja'
      FieldName = 'nCdLoja'
      ReadOnly = True
      Size = 15
    end
    object qryParcelasVencidascNrTit: TStringField
      DisplayLabel = 'Carnet'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryParcelasVencidasiParcela: TIntegerField
      DisplayLabel = 'Parc.'
      FieldName = 'iParcela'
    end
    object qryParcelasVencidasdDtEmissao: TDateTimeField
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryParcelasVencidasdDtVenc: TDateTimeField
      DisplayLabel = 'Dt. Vencto'
      FieldName = 'dDtVenc'
    end
    object qryParcelasVencidasnValTit: TBCDField
      DisplayLabel = 'Val. Parcela'
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryParcelasVencidasnValJuro: TBCDField
      DisplayLabel = 'Juros'
      FieldName = 'nValJuro'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryParcelasVencidasnValDesconto: TBCDField
      DisplayLabel = 'Desconto'
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryParcelasVencidasnSaldoTit: TBCDField
      DisplayLabel = 'Saldo'
      FieldName = 'nSaldoTit'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 13
      Size = 2
    end
    object qryParcelasVencidasiDiasAtraso: TIntegerField
      DisplayLabel = 'Atraso'
      FieldName = 'iDiasAtraso'
      ReadOnly = True
    end
    object qryParcelasVencidascNmTerceiro: TStringField
      DisplayLabel = 'Cliente'
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryParcelasVencidasnCdLanctoFin: TIntegerField
      DisplayLabel = 'Lancto Fin.'
      FieldName = 'nCdLanctoFin'
    end
  end
  object dsParcelasVencidas: TDataSource
    DataSet = qryParcelasVencidas
    Left = 216
    Top = 144
  end
end
