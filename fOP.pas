unit fOP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, DBGridEhGrouping,
  cxLookAndFeelPainters, cxButtons, GridsEh, DBGridEh, Shellapi, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid;

type
  TfrmOP = class(TfrmCadastro_Padrao)
    qryMasternCdOrdemProducao: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNumeroOP: TStringField;
    qryMasternCdTipoOP: TIntegerField;
    qryMasterdDtAbertura: TDateTimeField;
    qryMasterdDtPrevConclusao: TDateTimeField;
    qryMasterdDtRealConclusao: TDateTimeField;
    qryMasternCdProduto: TIntegerField;
    qryMasternQtdePlanejada: TBCDField;
    qryMasternCdLocalEstoque: TIntegerField;
    qryMasternCdPedido: TIntegerField;
    qryMasternCdUsuario: TIntegerField;
    qryMasternCdTabTipoStatusOP: TIntegerField;
    qryMasternCdTabTipoOrigemOP: TIntegerField;
    qryMasternValCustoEstimadoProdutos: TBCDField;
    qryMasternValCustoRealProdutos: TBCDField;
    qryMasternPercVarCustoProdutos: TBCDField;
    qryMasternQtdeRetrabalho: TIntegerField;
    qryMasternQtdeConcluida: TIntegerField;
    qryMasternQtdeRefugo: TIntegerField;
    qryMasteriDiasAtraso: TIntegerField;
    qryMastercOBS: TStringField;
    qryMastercOBSLivro: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit7: TDBEdit;
    DataSource1: TDataSource;
    qryTipoOP: TADOQuery;
    qryTipoOPcNmTipoOP: TStringField;
    DBEdit16: TDBEdit;
    DataSource2: TDataSource;
    qryProduto: TADOQuery;
    qryProdutocNmProduto: TStringField;
    qryProdutocUnidadeMedida: TStringField;
    qryProdutonQtdeMinimaCompra: TBCDField;
    qryProdutoiDiaEntrega: TIntegerField;
    DBEdit17: TDBEdit;
    DataSource3: TDataSource;
    Label7: TLabel;
    DBEdit18: TDBEdit;
    Label16: TLabel;
    DBEdit19: TDBEdit;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DBEdit20: TDBEdit;
    DataSource4: TDataSource;
    qryTabTipoStatusOP: TADOQuery;
    qryTabTipoStatusOPcNmTabTipoStatusOP: TStringField;
    DBEdit21: TDBEdit;
    DataSource5: TDataSource;
    qryTabTipoOrigemOP: TADOQuery;
    qryTabTipoOrigemOPcNmTabTipoOrigemOP: TStringField;
    DBEdit22: TDBEdit;
    DataSource6: TDataSource;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    DBEdit23: TDBEdit;
    DataSource7: TDataSource;
    qryPedido: TADOQuery;
    qryPedidonCdTerceiro: TIntegerField;
    qryPedidocNmTerceiro: TStringField;
    Label17: TLabel;
    DBEdit24: TDBEdit;
    DataSource8: TDataSource;
    DBEdit25: TDBEdit;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    Image2: TImage;
    DBGridEh1: TDBGridEh;
    GroupBox2: TGroupBox;
    btExibirProdutoEtapa: TcxButton;
    qryEtapas: TADOQuery;
    qryEtapasnCdEtapaOrdemProducao: TIntegerField;
    qryEtapasnCdOrdemProducao: TIntegerField;
    qryEtapasnCdRoteiroProducao: TIntegerField;
    qryEtapasiSeq: TIntegerField;
    qryEtapasnCdCentroProdutivo: TIntegerField;
    qryEtapasnCdLinhaProducao: TIntegerField;
    qryEtapasnCdEtapaProducao: TIntegerField;
    qryEtapasiTempoPreparacao: TBCDField;
    qryEtapasiTempoSetup: TBCDField;
    qryEtapasiTempoProcesso: TBCDField;
    qryEtapasiTempoEspera: TBCDField;
    qryEtapascInstrucoesProducao: TMemoField;
    qryEtapascArquivoMidia: TStringField;
    qryEtapasdDtInicio: TDateTimeField;
    qryEtapasdDtFim: TDateTimeField;
    dsEtapas: TDataSource;
    qryEtapascNmCentroProdutivo: TStringField;
    qryEtapascNmLinhaProducao: TStringField;
    qryEtapascNmEtapaProducao: TStringField;
    qryCentroProdutivo: TADOQuery;
    qryCentroProdutivocNmCentroProdutivo: TStringField;
    qryLinhaProducao: TADOQuery;
    qryLinhaProducaocNmLinhaProducao: TStringField;
    qryEtapaProducao: TADOQuery;
    qryEtapaProducaocNmEtapaProducao: TStringField;
    qryEtapaProducaocFlgGeraPedidoComercial: TIntegerField;
    qryProcuraRoteiroProducao: TADOQuery;
    SP_GERA_ETAPAS_OP: TADOStoredProc;
    qryEtapascFlgImagem: TIntegerField;
    qryEtapaProducaocFlgServicoExterno: TIntegerField;
    Image3: TImage;
    Label18: TLabel;
    Image4: TImage;
    Label19: TLabel;
    Image5: TImage;
    Label20: TLabel;
    qryProdutonCdTipoObtencao: TIntegerField;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    SP_PLANEJA_OP: TADOStoredProc;
    qryMasternCdOrdemProducaoPai: TIntegerField;
    qryMasterdDtInicioPrevisto: TDateTimeField;
    qryMastercFlgEstoqueEmpenhado: TIntegerField;
    qryMasterdDtEstoqueEmpenhado: TDateTimeField;
    Label21: TLabel;
    DBEdit26: TDBEdit;
    Label22: TLabel;
    DBEdit27: TDBEdit;
    Label23: TLabel;
    DBEdit28: TDBEdit;
    Label24: TLabel;
    DBEdit29: TDBEdit;
    Label25: TLabel;
    DBEdit30: TDBEdit;
    Label26: TLabel;
    DBEdit31: TDBEdit;
    Label27: TLabel;
    DBEdit32: TDBEdit;
    Label29: TLabel;
    Label31: TLabel;
    DBEdit36: TDBEdit;
    Label32: TLabel;
    DBEdit37: TDBEdit;
    DBMemo1: TDBMemo;
    Label28: TLabel;
    DBEdit33: TDBEdit;
    Label33: TLabel;
    DBEdit34: TDBEdit;
    qryMasternValCustoAdicional: TBCDField;
    Label34: TLabel;
    DBEdit38: TDBEdit;
    qryMasternQtdeCancelada: TIntegerField;
    Label35: TLabel;
    DBEdit39: TDBEdit;
    SP_ESTIMA_CUSTO_OP: TADOStoredProc;
    qryVerificaProdutoPedido: TADOQuery;
    qryVerificaProdutoPedidonCdItemPedido: TIntegerField;
    cxTabSheet3: TcxTabSheet;
    cxGrid4: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridDBTableView4dDtEntrada: TcxGridDBColumn;
    cxGridDBTableView4cNmUsuario: TcxGridDBColumn;
    cxGridDBTableView4cDocumento: TcxGridDBColumn;
    cxGridDBTableView4cNmTabTipoEntradaProducao: TcxGridDBColumn;
    cxGridDBTableView4nQtdeEntrada: TcxGridDBColumn;
    cxGridDBTableView4cNmLocalEstoque: TcxGridDBColumn;
    cxGridDBTableView4cOBSLivro: TcxGridDBColumn;
    cxGridLevel4: TcxGridLevel;
    qryRegistroEntrada: TADOQuery;
    qryRegistroEntradacDocumento: TStringField;
    qryRegistroEntradacNmTabTipoEntradaProducao: TStringField;
    qryRegistroEntradanQtdeEntrada: TBCDField;
    qryRegistroEntradacNmLocalEstoque: TStringField;
    qryRegistroEntradadDtEntrada: TDateTimeField;
    qryRegistroEntradacNmUsuario: TStringField;
    qryRegistroEntradacOBSLivro: TStringField;
    dsRegistroEntrada: TDataSource;
    qryMasterdDtAutorizacao: TDateTimeField;
    qryMasternCdUsuarioAutoriz: TIntegerField;
    qryMasterdDtRealInicio: TDateTimeField;
    qryMasteriSeqOPRework: TIntegerField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternCdUsuarioCancel: TIntegerField;
    DBEdit35: TDBEdit;
    Label30: TLabel;
    Label36: TLabel;
    DBEdit40: TDBEdit;
    Label37: TLabel;
    DBEdit41: TDBEdit;
    Label38: TLabel;
    DBEdit42: TDBEdit;
    qryUsuarioAutoriz: TADOQuery;
    qryUsuarioAutorizcNmUsuario: TStringField;
    DBEdit43: TDBEdit;
    DataSource9: TDataSource;
    qryUsuarioCancel: TADOQuery;
    qryUsuarioCancelcNmUsuario: TStringField;
    DBEdit44: TDBEdit;
    DataSource10: TDataSource;
    qryEtapasnCdTipoMaquinaPCP: TIntegerField;
    qryEtapascNmTipoMaquinaPCP: TStringField;
    qryTipoMaquinaPCP: TADOQuery;
    qryTipoMaquinaPCPnCdTipoMaquinaPCP: TIntegerField;
    qryTipoMaquinaPCPcNmTipoMaquinaPCP: TStringField;
    qryTipoMaquinaPCPnCdCentroProdutivo: TIntegerField;
    qryTipoMaquinaPCPnCdLinhaProducao: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure DBEdit10Exit(Sender: TObject);
    procedure DBEdit9Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit15Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryEtapasCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btExibirProdutoEtapaClick(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure desativaCampo( objDBEdit : TDBEdit ; bDesativar : boolean) ;
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton13Click(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit11Exit(Sender: TObject);
    procedure qryEtapasnCdCentroProdutivoChange(Sender: TField);
    procedure qryEtapasnCdLinhaProducaoChange(Sender: TField);
    procedure qryEtapasnCdEtapaProducaoChange(Sender: TField);
    procedure qryEtapasnCdTipoMaquinaPCPChange(Sender: TField);
    procedure qryEtapasBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOP: TfrmOP;

implementation

uses fMenu, fMemoText, fOP_ProdutoEtapa, fLookup_Padrao, rOP;

{$R *.dfm}

procedure TfrmOP.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'ORDEMPRODUCAO' ;
  nCdTabelaSistema  := 415 ;
  nCdConsultaPadrao := 742 ;
  bLimpaAposSalvar  := FALSE ;
end;

procedure TfrmOP.btIncluirClick(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  
  desativaCampo( DBEdit4  , FALSE ) ;
  desativaCampo( DBEdit8  , FALSE ) ;
  desativaCampo( DBEdit9  , FALSE ) ;
  desativaCampo( DBEdit10 , FALSE ) ;
  
  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  PosicionaQuery( qryEmpresa, qryMasternCdEmpresa.AsString ) ;

  qryMasternCdTabTipoStatusOP.Value := 1 ;
  PosicionaQuery( qryTabTipoStatusOP , qryMasternCdTabTipoStatusOP.AsString ) ;

  qryMasternCdTabTipoOrigemOP.Value := 1 ;
  PosicionaQuery( qryTabTipoOrigemOP , qryMasternCdTabTipoOrigemOP.asString ) ;

  qryMasternCdUsuario.Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery( qryUsuario , qryMasternCdUsuario.AsString) ;

  qryMasterdDtAbertura.Value := Now() ;
  DBEdit4.SetFocus ;
  
end;

procedure TfrmOP.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryTipoOP.Close;
  PosicionaQuery( qryTipoOP , qryMasternCdTipoOP.AsString ) ;

end;

procedure TfrmOP.DBEdit8Exit(Sender: TObject);
begin
  inherited;

  qryProduto.Close ;
  PosicionaQuery( qryProduto , qryMasternCdProduto.AsString ) ;

  if ( qryMaster.State = dsInsert ) then
  begin

      if not qryProduto.eof then
      begin
          qryMasternQtdePlanejada.Value := qryProdutonQtdeMinimaCompra.Value ;

          if ( qryProdutonCdTipoObtencao.Value <> 2 ) then
              MensagemAlerta('O m�todo de obten��o deste produto n�o � Produ��o. Verifique.') ;

      end ;

      if (DBEdit11.Text <> '') and (not qryProduto.eof) then
      begin

          qryVerificaProdutoPedido.Close;
          qryVerificaProdutoPedido.Parameters.ParamByName('nCdPedido').Value  := qryMasternCdPedido.Value ;
          qryVerificaProdutoPedido.Parameters.ParamByName('nCdProduto').Value := qryMasternCdProduto.Value ;
          qryVerificaProdutoPedido.Open ;


          try

              if ( qryVerificaProdutoPedido.eof ) then
                  if (MessageDlg('Este produto n�o est� informado no pedido de venda. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
                  begin

                      qryProduto.Close ;
                      qryMasternCdProduto.asString := '' ;
                      DBEdit8.SetFocus;
                      abort ;

                  end ;

          finally

              qryVerificaProdutoPedido.Close;

          end ;

      end ;


  end ;
  
end;

procedure TfrmOP.DBEdit10Exit(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Close ;
  PosicionaQuery( qryLocalEstoque , qryMasternCdLocalEstoque.asString ) ;

end;

procedure TfrmOP.DBEdit9Exit(Sender: TObject);
var
  nFator : Double ;
begin
  inherited;

  if ( qryMaster.State = dsInsert ) then
  begin

      if ( DBEdit17.Text <> '' ) and ( qryMasternQtdePlanejada.Value > 0 ) then
      begin

          {-- converte o lead-time da produ��o para horas --}
          if ( qryProdutoiDiaEntrega.Value > 0 ) and ( qryProdutonQtdeMinimaCompra.Value > 0 ) then
              nFator := ( qryProdutoiDiaEntrega.Value * 24 ) / qryProdutonQtdeMinimaCompra.Value ;

          {-- descobre quantas horas � necess�ria para produzir os produtos --}
          nFator := nFator * qryMasternQtdePlanejada.Value ;

          {-- converte as horas pra dias (per�odo de 24 horas) --}
          nFator := nFator / 24 ;

          qryMasterdDtPrevConclusao.Value := StrToDate( FormatDateTime('DD/MM/YYYY',(qryMasterdDtAbertura.Value + nFator)) ) ;

      end ;

  end ;

end;

procedure TfrmOP.qryMasterBeforePost(DataSet: TDataSet);
begin

  if ( DBEdit16.Text = '' ) then
  begin

      MensagemAlerta('Selecione o Tipo de OP.') ;
      DBEdit4.SetFocus ;
      abort ;

  end ;

  if ( DBEdit17.Text = '' ) then
  begin

      MensagemAlerta('Selecione o produto.') ;
      DBEdit8.SetFocus ;
      abort ;

  end ;

  if ( qryProdutonCdTipoObtencao.Value <> 2 ) then
  begin

      MensagemAlerta('O m�todo de obten��o deste produto n�o � Produ��o. Verifique.') ;
      DBEdit8.Setfocus ;
      abort ;

  end ;

  if ( qryMasternQtdePlanejada.Value <= 0 ) then
  begin

      MensagemAlerta('Quantidade planejada inv�lida ou n�o informada.') ;
      DBEdit9.SetFocus ;
      abort ;

  end ;

  if ( DBEdit20.Text = '' ) then
  begin

      MensagemAlerta('Selecione o local de estoque de entrada da produ��o.') ;
      DBEdit10.SetFocus ;
      abort ;

  end ;

  if ( qryMasterdDtPrevConclusao.AsString = '' ) then
  begin

      MensagemAlerta('Informe a data prevista de conclus�o da produ��o.') ;
      DBEdit6.SetFocus;
      abort ;

  end ;

  if ( qryMasterdDtPrevConclusao.Value < qryMasterdDtAbertura.Value ) then
  begin

      MensagemAlerta('A data prevista de conclus�o n�o pode ser menor que a data de abertura da OP.') ;
      DBEdit6.SetFocus;
      abort ;
      
  end ;

  qryProcuraRoteiroProducao.Close;
  qryProcuraRoteiroProducao.Parameters.ParamByName('nCdProduto').Value := qryMasternCdProduto.Value ;
  qryProcuraRoteiroProducao.Parameters.ParamByName('nCdTipoOP').Value  := qryMasternCdTipoOP.Value ;
  qryProcuraRoteiroProducao.Open;

  if (qryProcuraRoteiroProducao.eof) then
  begin
      MensagemAlerta('Nenhum roteiro de produ��o ativo encontrado para este produto neste tipo de ordem de produ��o.') ;
      DBEdit4.SetFocus;
      abort ;
  end ;

  inherited;

  if ( qryMastercNumeroOP.Value = '' ) then
      qryMastercNumeroOP.Value := '01.' + intToStr( qryMasternCdOrdemProducao.Value ) ;

end;

procedure TfrmOP.DBEdit15Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State in [dsInsert , dsEdit ]) then
      qryMaster.Post ;

end;

procedure TfrmOP.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  PosicionaQuery( qryEmpresa, qryMasternCdEmpresa.AsString ) ;

  qryTipoOP.Close;
  PosicionaQuery( qryTipoOP , qryMasternCdTipoOP.AsString ) ;

  qryProduto.Close ;
  PosicionaQuery( qryProduto , qryMasternCdProduto.AsString ) ;

  qryTabTipoStatusOP.Close ;
  PosicionaQuery( qryTabTipoStatusOP , qryMasternCdTabTipoStatusOP.AsString ) ;

  qryTabTipoOrigemOP.Close ;
  PosicionaQuery( qryTabTipoOrigemOP , qryMasternCdTabTipoOrigemOP.asString ) ;

  qryUsuario.Close ;
  PosicionaQuery( qryUsuario , qryMasternCdUsuario.AsString) ;

  qryLocalEstoque.Close ;
  PosicionaQuery( qryLocalEstoque , qryMasternCdLocalEstoque.asString ) ;

  qryPedido.Close;
  PosicionaQuery( qryPedido , qryMasternCdPedido.asString ) ;

  qryEtapas.Close;
  PosicionaQuery( qryEtapas , qryMasternCdOrdemProducao.asString ) ;

  qryRegistroEntrada.Close;
  PosicionaQuery( qryRegistroEntrada , qryMasternCdOrdemProducao.asString ) ;

  qryUsuarioAutoriz.Close;
  PosicionaQuery( qryUsuarioAutoriz , qryMasternCdUsuarioAutoriz.AsString ) ;

  qryUsuarioCancel.Close;
  PosicionaQuery( qryUsuarioCancel , qryMasternCdUsuarioCancel.AsString ) ;

  desativaCampo( DBEdit4 , TRUE ) ;
  desativaCampo( DBEdit8 , TRUE ) ;
  desativaCampo( DBEdit9 , TRUE ) ;

  {-- se a OP j� foi autorizada n�o deixa alterar --}
  if (qryMasternCdTabTipoStatusOP.Value > 2) then
      desativaCampo( DBEdit11, TRUE ) ;

  {-- se ja tiver alguma quantidade conclu�da, n�o deixa alterar o local de estoque de entrada dos produtos --}
  desativaCampo( DBEdit10 , (qryMasternQtdeConcluida.Value > 0 ) ) ;

end;

procedure TfrmOP.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTipoOP.Close;
  qryProduto.Close ;
  qryTabTipoStatusOP.Close ;
  qryTabTipoOrigemOP.Close ;
  qryUsuario.Close ;
  qryLocalEstoque.Close ;
  qryPedido.Close;
  qryEtapas.Close;
  qryUsuarioAutoriz.Close;
  qryUsuarioCancel.Close;

  desativaCampo( DBEdit4  , FALSE ) ;
  desativaCampo( DBEdit8  , FALSE ) ;
  desativaCampo( DBEdit9  , FALSE ) ;
  desativaCampo( DBEdit10 , FALSE ) ;
  desativaCampo( DBEdit11 , FALSE ) ;

end;

procedure TfrmOP.DBGridEh1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  objMemo : TfrmMemoText ;
begin
  inherited;

  if (Shift = [ssCtrl]) and (key = vk_return) then
  begin

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cInstrucoesProducao') then
      begin

          if (qryEtapas.Active) and (qryEtapas.RecordCount > 0) then
          begin

              objMemo := TfrmMemoText.Create( Self );

              objMemo.campoMemo.Clear;
              objMemo.campoMemo.Lines.Add( qryEtapascInstrucoesProducao.Value ) ;

              objMemo.campoMemo.ReadOnly := TRUE ;

              showForm( objMemo , FALSE ) ;

              objMemo.campoMemo.ReadOnly := FALSE ;

              freeAndNil( objMemo ) ;

          end ;

      end ;

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cArquivoMidia') then
      begin

          if (qryEtapascArquivoMidia.Value <> '') then
              ShellExecute(0, nil, pAnsiChar( Trim( qryEtapascArquivoMidia.Value ) ), nil, nil, SW_NORMAL);

      end ;

  end ;

end;

procedure TfrmOP.qryEtapasCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryEtapas.State in [dsCalcFields,dsBrowse]) then
  begin

      PosicionaQuery(qryCentroProdutivo, qryEtapasnCdCentroProdutivo.AsString) ;

      qryLinhaProducao.Parameters.ParamByName('nCdCentroProdutivo').Value := qryEtapasnCdCentroProdutivo.Value ;

      PosicionaQuery(qryLinhaProducao, qryEtapasnCdLinhaProducao.AsString) ;

      PosicionaQuery(qryEtapaProducao, qryEtapasnCdEtapaProducao.AsString) ;

      qryTipoMaquinaPCP.Close;
      PosicionaQuery(qryTipoMaquinaPCP, qryEtapasnCdTipoMaquinaPCP.asString) ;

      if not qryCentroProdutivo.eof then
          qryEtapascNmCentroProdutivo.Value := qryCentroProdutivocNmCentroProdutivo.Value;

      if not qryLinhaProducao.Eof then
          qryEtapascNmLinhaProducao.Value := qryLinhaProducaocNmLinhaProducao.Value ;

      if not qryTipoMaquinaPCP.Eof then
          qryEtapascNmTipoMaquinaPCP.Value := qryTipoMaquinaPCPcNmTipoMaquinaPCP.Value ;

      if not qryEtapaProducao.Eof then
          qryEtapascNmEtapaProducao.Value := qryEtapaProducaocNmEtapaProducao.Value ;

      if (qryEtapasdDtInicio.AsString = '') then
          qryEtapascFlgImagem.Value := 9
      else qryEtapascFlgImagem.Value := 10 ;

      if (qryEtapaProducaocFlgServicoExterno.Value = 1) then
          qryEtapascFlgImagem.Value := 11 ;


  end ;

end;

procedure TfrmOP.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  if (not qryEtapas.Active) or (qryEtapas.RecordCount = 0) then
  begin

      frmMenu.Connection.BeginTrans;

      try

          SP_GERA_ETAPAS_OP.Close;
          SP_GERA_ETAPAS_OP.Parameters.ParamByName('@nCdOrdemProducao').Value := qryMasternCdOrdemProducao.Value ;
          SP_GERA_ETAPAS_OP.ExecProc;

      except

          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no Processamento.') ;
          raise ;

      end ;

      frmMenu.Connection.CommitTrans;

  end ;

  qryEtapas.Close;
  PosicionaQuery( qryEtapas , qryMasternCdOrdemProducao.AsString ) ;

end;

procedure TfrmOP.btExibirProdutoEtapaClick(Sender: TObject);
var
  objForm : TfrmOP_ProdutoEtapa ;
begin
  inherited;

  if (not qryEtapas.Active) or (qryEtapas.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhuma etapa selecionada.') ;
      abort ;
  end ;

  objForm := TfrmOP_ProdutoEtapa.Create( Self ) ;

  objForm.exibeProdutoEtapaProducao( qryEtapasnCdEtapaOrdemProducao.Value ) ;

  freeAndNil( objForm ) ;

  frmMenu.Connection.BeginTrans;

  try

      SP_ESTIMA_CUSTO_OP.Close ;
      SP_ESTIMA_CUSTO_OP.Parameters.ParamByName('@nCdOrdemProducao').Value := qryMasternCdOrdemProducao.Value ;
      SP_ESTIMA_CUSTO_OP.ExecProc;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans;

  qryMaster.Requery();

end;

procedure TfrmOP.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  if (not qryEtapas.Active) or (qryEtapas.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhuma etapa selecionada.') ;
      abort ;
  end ;

  btExibirProdutoEtapa.Click;
    
end;

procedure TfrmOP.desativaCampo(objDBEdit: TDBEdit; bDesativar: boolean);
begin

    if (bDesativar) then
    begin

        objDBEdit.ReadOnly   := True ;
        objDBEdit.Color      := $00E9E4E4 ;
        objDBEdit.Font.Color := clBlack ;
        objDBEdit.TabStop    := False ;

    end
    else
    begin

        objDBEdit.ReadOnly   := False   ;
        objDBEdit.Color      := clWhite ;
        objDBEdit.Font.Color := clBlack ;
        objDBEdit.TabStop    := True    ;

    end ;

end;

procedure TfrmOP.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if ( DBEdit4.ReadOnly ) then
            exit ;

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(739);

            If (nPK > 0) then
                qryMasternCdTipoOP.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmOP.ToolButton13Click(Sender: TObject);
var
  objRel : TrptOP ;
begin
  inherited;

  if (not qryMaster.Active) or (qryMaster.RecordCount = 0) then
  begin

      MensagemAlerta('Nenhuma ordem de produ��o ativa.') ;
      abort ;

  end ;
  
  objRel := TrptOP.Create( Self ) ;

  PosicionaQuery( objRel.qryOP , qryMasternCdOrdemProducao.AsString ) ;

  try

      try

          objRel.QuickRep1.Preview;

      except

          MensagemErro('Erro no Processamento.') ;
          raise ;
          
      end ;

  finally

      freeAndNil( objRel ) ;

  end ;

end;

procedure TfrmOP.ToolButton10Click(Sender: TObject);
begin
  inherited;

  if (not qryMaster.Active) or (qryMaster.RecordCount = 0) then
  begin

      MensagemAlerta('Nenhuma ordem de produ��o ativa.') ;
      abort ;

  end ;

  {-- se estiver autorizada n�o permite mais altera��es --}
  if (qryMasternCdTabTipoStatusOp.Value > 3) then
  begin
      MensagemAlerta('O Status desta ordem de produ��o n�o permite replanejamento.') ;
      abort ;
  end ;

  if (frmMenu.LeParametro('MOMEMPESTOP') = 'S') then
  begin

      if (MessageDlg('No planejamento os custos da produ��o ser�o estimados e os estoques empenhados. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MRNo) then
          exit ;

  end
  else
  begin

      if (MessageDlg('No planejamento os custos da produ��o ser�o estimados. Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MRNo) then
          exit ;

  end ;

  frmMenu.Connection.BeginTrans ;

  try

      SP_PLANEJA_OP.Close;
      SP_PLANEJA_OP.Parameters.ParamByName('@nCdOrdemProducao').Value := qryMasternCdOrdemProducao.Value ;
      SP_PLANEJA_OP.ExecProc;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans ;

  qryMaster.Requery();
  
  ShowMessage('Ordem de Produ��o Planejada com Sucesso!') ;

end;

procedure TfrmOP.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  
end;

procedure TfrmOP.btCancelarClick(Sender: TObject);
begin
  cxPageControl1.ActivePageIndex := 0 ;
  inherited;

end;

procedure TfrmOP.btConsultarClick(Sender: TObject);
begin

  cxPageControl1.ActivePageIndex := 0 ;
  inherited;

end;

procedure TfrmOP.DBEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if ( DBEdit8.ReadOnly ) then
            exit ;

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(76,'Produto.nCdTipoObtencao = 2');

            If (nPK > 0) then
                qryMasternCdProduto.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmOP.DBEdit10KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if ( DBEdit10.ReadOnly ) then
            exit ;

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(87);

            If (nPK > 0) then
                qryMasternCdLocalEstoque.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmOP.DBEdit11Exit(Sender: TObject);
begin
  inherited;

  if (not DBEdit11.ReadOnly) then
  begin

      qryPedido.Close ;
      PosicionaQuery( qryPedido , DBEdit11.Text ) ;

  end ;

end;

procedure TfrmOP.qryEtapasnCdCentroProdutivoChange(Sender: TField);
begin
  inherited;

  if (qryEtapas.State in [dsInsert, dsEdit]) then
  begin

      qryEtapascNmCentroProdutivo.Value := '' ;

      qryCentroProdutivo.Close;
      PosicionaQuery(qryCentroProdutivo, qryEtapasnCdCentroProdutivo.AsString) ;

      if not qryCentroProdutivo.eof then
          qryEtapascNmCentroProdutivo.Value := qryCentroProdutivocNmCentroProdutivo.Value;

  end ;

end;

procedure TfrmOP.qryEtapasnCdLinhaProducaoChange(Sender: TField);
begin
  inherited;

  if (qryEtapas.State in [dsInsert, dsEdit]) then
  begin

      qryEtapascNmLinhaProducao.Value := '' ;

      qryLinhaProducao.Close;
      qryLinhaProducao.Parameters.ParamByName('nCdCentroProdutivo').Value := qryEtapasnCdCentroProdutivo.Value ;

      PosicionaQuery(qryLinhaProducao, qryEtapasnCdLinhaProducao.AsString) ;

      if not qryLinhaProducao.eof then
          qryEtapascNmLinhaProducao.Value := qryLinhaProducaocNmLinhaProducao.Value;

  end ;

end;

procedure TfrmOP.qryEtapasnCdEtapaProducaoChange(Sender: TField);
begin
  inherited;

  if (qryEtapas.State in [dsInsert, dsEdit]) then
  begin

      qryEtapascNmEtapaProducao.Value := '' ;

      qryEtapaProducao.Close;
      PosicionaQuery(qryEtapaProducao, qryEtapasnCdEtapaProducao.AsString) ;

      if not qryEtapaProducao.eof then
          qryEtapascNmEtapaProducao.Value := qryEtapaProducaocNmEtapaProducao.Value;

  end ;

end;

procedure TfrmOP.qryEtapasnCdTipoMaquinaPCPChange(Sender: TField);
begin
  inherited;

  if (qryEtapas.State in [dsInsert, dsEdit]) then
  begin

      qryEtapascNmTipoMaquinaPCP.Value := '' ;

      qryTipoMaquinaPCP.Close ;
      PosicionaQuery(qryTipoMaquinaPCP, qryEtapasnCdTipoMaquinaPCP.AsString) ;

      if not qryTipoMaquinaPCP.eof then
          qryEtapascNmTipoMaquinaPCP.Value := qryTipoMaquinaPCPcNmTipoMaquinaPCP.Value;

  end ;

end;

procedure TfrmOP.qryEtapasBeforePost(DataSet: TDataSet);
begin

  if (qryEtapasiSeq.Value <= 0) then
  begin
      MensagemAlerta('Informe a sequ�ncia da etapa.') ;
      abort ;
  end ;

  if (qryEtapascNmCentroProdutivo.Value = '') then
  begin
      MensagemAlerta('Informe o centro produtivo.') ;
      abort ;
  end ;

  if (qryEtapascNmLinhaProducao.Value = '') then
  begin
      MensagemAlerta('Informe a linha de produ��o.') ;
      abort ;
  end ;

  if (qryEtapascNmEtapaProducao.Value = '') then
  begin
      MensagemAlerta('Informe a etapa de produ��o.') ;
      abort ;
  end ;

  if (qryEtapasiTempoPreparacao.Value < 0) then
  begin
      MensagemAlerta('Tempo de prepara��o inv�lido. Ex: 0,05 = 05 minutos / 1,30 = 01 hora e meia.') ;
      abort ;
  end ;

  if (qryEtapasiTempoSetup.Value < 0) then
  begin
      MensagemAlerta('Tempo de setup inv�lido. Ex: 0,05 = 05 minutos / 1,30 = 01 hora e meia.') ;
      abort ;
  end ;

  if (qryEtapasiTempoProcesso.Value < 0) then
  begin
      MensagemAlerta('Tempo de processo inv�lido. Ex: 0,05 = 05 minutos / 1,30 = 01 hora e meia.') ;
      abort ;
  end ;

  if (qryEtapasiTempoEspera.Value < 0) then
  begin
      MensagemAlerta('Tempo de espera inv�lido. Ex: 0,05 = 05 minutos / 1,30 = 01 hora e meia.') ;
      abort ;
  end ;

  inherited;


end;

procedure TfrmOP.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    objMemo : TfrmMemoText ;
    nPK     : integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdCentroProdutivo') then
        begin

            if (qryEtapas.State = dsBrowse) then
                 qryEtapas.Edit ;

            if (qryEtapas.State = dsInsert) or (qryEtapas.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(738);

                If (nPK > 0) then
                    qryEtapasnCdCentroProdutivo.Value := nPK ;

            end ;

        end ;

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdLinhaProducao') then
        begin

            if (qryEtapascNmCentroProdutivo.Value = '') then
            begin
                MensagemAlerta('Selecione um centro produtivo.') ;
                abort ;
            end ;

            if (qryEtapas.State = dsBrowse) then
                 qryEtapas.Edit ;

            if (qryEtapas.State = dsInsert) or (qryEtapas.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(207,'LinhaProducao.nCdCentroProdutivo = ' + qryEtapasnCdCentroProdutivo.asString);

                If (nPK > 0) then
                    qryEtapasnCdLinhaProducao.Value := nPK ;

            end ;

        end ;

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdTipoMaquinaPCP') then
        begin

            if (qryEtapas.State = dsBrowse) then
                 qryEtapas.Edit ;

            if (qryEtapas.State = dsInsert) or (qryEtapas.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(209);

                If (nPK > 0) then
                    qryEtapasnCdTipoMaquinaPCP.Value := nPK ;

            end ;

        end ;

        if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdEtapaProducao') then
        begin

            if (qryEtapas.State = dsBrowse) then
                 qryEtapas.Edit ;

            if (qryEtapas.State = dsInsert) or (qryEtapas.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(741);

                If (nPK > 0) then
                    qryEtapasnCdEtapaProducao.Value := nPK ;

            end ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass( TfrmOP ) ;
    
end.
