inherited FrmIndice: TFrmIndice
  Left = 231
  Top = 183
  Caption = 'FrmIndice'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 27
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label3: TLabel [2]
    Left = 16
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit3
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 72
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdIndiceReajuste'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit3: TDBEdit [5]
    Left = 72
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmIndiceReajuste'
    DataSource = dsMaster
    TabOrder = 2
  end
  object cxPageControl1: TcxPageControl [6]
    Left = 8
    Top = 104
    Width = 849
    Height = 385
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 3
    ClientRectBottom = 381
    ClientRectLeft = 4
    ClientRectRight = 845
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Cota'#231#245'es'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 841
        Height = 357
        Align = alClient
        DataSource = DataSource1
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        OptionsEh = [dghFixed3D, dghHighlightFocus, dghClearSelection, dghEnterAsTab, dghDialogFind]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'iAnoReferencia'
            Footers = <>
            Width = 113
          end
          item
            EditButtons = <>
            FieldName = 'iMesReferencia'
            Footers = <>
            Width = 84
          end
          item
            DisplayFormat = 'Valor '#205'ndice'
            EditButtons = <>
            FieldName = 'nIndice'
            Footers = <>
            Width = 137
          end>
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM IndiceReajuste'
      'WHERE nCdIndiceReajuste = :nPK'
      '')
    object qryMasternCdIndiceReajuste: TIntegerField
      FieldName = 'nCdIndiceReajuste'
    end
    object qryMastercNmIndiceReajuste: TStringField
      FieldName = 'cNmIndiceReajuste'
      Size = 50
    end
  end
  object qryCotacao: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCotacaoBeforePost
    Parameters = <
      item
        Name = 'nCdIndiceReajuste'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM COTACAOINDICEREAJUSTE'
      'WHERE nCdIndiceReajuste = :nCdIndiceReajuste'
      'ORDER BY iAnoReferencia DESC, iMesReferencia DESC'
      '')
    Left = 736
    Top = 384
    object qryCotacaonCdCotacaoIndiceReajuste: TAutoIncField
      FieldName = 'nCdCotacaoIndiceReajuste'
      ReadOnly = True
    end
    object qryCotacaonCdIndiceReajuste: TIntegerField
      FieldName = 'nCdIndiceReajuste'
    end
    object qryCotacaoiAnoReferencia: TIntegerField
      DisplayLabel = 'Cota'#231#245'es|Ano Ref.'
      FieldName = 'iAnoReferencia'
    end
    object qryCotacaoiMesReferencia: TIntegerField
      DisplayLabel = 'Cota'#231#245'es|M'#234's Ref.'
      FieldName = 'iMesReferencia'
    end
    object qryCotacaonIndice: TBCDField
      DisplayLabel = 'Cota'#231#245'es|Val. Cota'#231#227'o'
      FieldName = 'nIndice'
      Precision = 12
    end
  end
  object DataSource1: TDataSource
    DataSet = qryCotacao
    Left = 784
    Top = 384
  end
end
