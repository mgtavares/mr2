unit fConsultaRequisAberta_Itens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh;

type
  TfrmConsultaRequisAberta_Itens = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryItens: TADOQuery;
    dsItens: TDataSource;
    qryItensnCdItemRequisicao: TAutoIncField;
    qryItensnCdRequisicao: TIntegerField;
    qryItensnCdProduto: TIntegerField;
    qryItenscNmItem: TStringField;
    qryItensnQtdeReq: TBCDField;
    qryItensnQtdeAtend: TBCDField;
    qryItensnQtdeCanc: TBCDField;
    qryItensnCdMapaCompra: TIntegerField;
    qryItensnCdPedido: TIntegerField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaRequisAberta_Itens: TfrmConsultaRequisAberta_Itens;

implementation

{$R *.dfm}

end.
