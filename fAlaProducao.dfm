inherited frmAlaProducao: TfrmAlaProducao
  Left = 190
  Top = 136
  Caption = 'Ala de Produ'#231#227'o'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 54
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 24
    Top = 62
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ala Produ'#231#227'o'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 96
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdAlaProducao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 96
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmAlaProducao'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM AlaProducao'
      'WHERE nCdAlaProducao = :nPK')
    object qryMasternCdAlaProducao: TIntegerField
      FieldName = 'nCdAlaProducao'
    end
    object qryMastercNmAlaProducao: TStringField
      FieldName = 'cNmAlaProducao'
      Size = 50
    end
  end
end
