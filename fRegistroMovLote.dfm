inherited frmRegistroMovLote: TfrmRegistroMovLote
  Caption = 'Registro Movimenta'#231#227'o Lote'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 100
    Height = 364
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
    object ToolButton4: TToolButton
      Left = 166
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 174
      Top = 0
      Caption = 'Consultar'
      ImageIndex = 2
      OnClick = ToolButton5Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 71
    Align = alTop
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 4
      Top = 24
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'Transa'#231#227'o'
    end
    object Label2: TLabel
      Tag = 1
      Left = 16
      Top = 48
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Produto'
      FocusControl = DBEdit1
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 60
      Top = 16
      Width = 293
      Height = 21
      DataField = 'cNmTabTipoOrigemMovLote'
      DataSource = DataSource1
      TabOrder = 0
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 60
      Top = 40
      Width = 701
      Height = 21
      DataField = 'cNmProduto'
      DataSource = DataSource2
      TabOrder = 1
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 100
    Width = 784
    Height = 364
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 360
    ClientRectLeft = 4
    ClientRectRight = 780
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Dados da Rastreabilidade'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 776
        Height = 336
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTempLote
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdTempMovLote'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdLocalEstoque'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cNrLote'
            Footers = <>
            Width = 215
          end
          item
            EditButtons = <>
            FieldName = 'dDtFabricacao'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'dDtValidade'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'dDtDisponibilidade'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeMov'
            Footers = <>
            Width = 92
          end
          item
            EditButtons = <>
            FieldName = 'nCdTabTipoOrigemMovLote'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'iIDRegistro'
            Footers = <>
            Visible = False
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 416
    Top = 192
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000999999009999990099999900999999009999990000000000000000000000
      0000808080008080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000000000000000000000000000009999
      9900000000000000000000000000000000009999990000000000000000008080
      8000808080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000E6E6E600E6E6E600E6E6E600E6E6E6000000000000000000000000008080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000000000000000000000E6E6E600E6E6
      E600E6E6E6000000000000000000E6E6E600E6E6E600E6E6E600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000E6E6E6000000
      000000000000D9D9D900D9D9D9000000000000000000E6E6E600000000009999
      9900999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000000000000099999900D9D9D900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E60000000000E6E6E600E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF000000840000000000000000000000000099999900E6E6E600E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E60000000000E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000000000009999990099999900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600D9D9D900E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000000000009999990099999900E6E6
      E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E6000000
      0000999999000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000999999009999
      9900E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600E6E6E600000000009999
      9900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000999999009999
      99009999990099999900E6E6E600E6E6E600E6E6E600E6E6E600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999009999990099999900999999000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFF30000
      FE00C003FFE9000000008001F051000000008001E023000000008001C0270000
      00008001860F0000000080019987000000008001008700000000800100470000
      0000800100070000000080010007000000018001800F000000038001801F0000
      0077C003C03F0000007FFFFFF0FF000000000000000000000000000000000000
      000000000000}
  end
  object qryMovLoteTemporario: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryMovLoteTemporarioBeforePost
    Parameters = <
      item
        Name = 'nCdTabTipoOrigemMovLote'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'iIDRegistro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM MovLoteTemporario'
      ' WHERE nCdTabTipoOrigemMovLote = :nCdTabTipoOrigemMovLote'
      '   AND iIDRegistro             = :iIDRegistro'
      ' ORDER BY nCdMovLoteTemporario')
    Left = 288
    Top = 184
    object qryMovLoteTemporarionCdMovLoteTemporario: TAutoIncField
      FieldName = 'nCdMovLoteTemporario'
      ReadOnly = True
    end
    object qryMovLoteTemporarionCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryMovLoteTemporarionCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryMovLoteTemporariocNrLote: TStringField
      DisplayLabel = 'Dados da Rastreabilidade|N'#250'mero Lote'
      FieldName = 'cNrLote'
      OnChange = qryMovLoteTemporariocNrLoteChange
      Size = 30
    end
    object qryMovLoteTemporariodDtFabricacao: TDateTimeField
      DisplayLabel = 'Dados da Rastreabilidade|Data Fabrica'#231#227'o'
      FieldName = 'dDtFabricacao'
    end
    object qryMovLoteTemporariodDtValidade: TDateTimeField
      DisplayLabel = 'Dados da Rastreabilidade|Data Validade'
      FieldName = 'dDtValidade'
    end
    object qryMovLoteTemporariodDtDisponibilidade: TDateTimeField
      DisplayLabel = 'Dados da Rastreabilidade|Data Disponibilidade'
      FieldName = 'dDtDisponibilidade'
    end
    object qryMovLoteTemporarionQtdeMov: TBCDField
      DisplayLabel = 'Dados da Rastreabilidade|Quantidade'
      FieldName = 'nQtdeMov'
      Precision = 12
    end
    object qryMovLoteTemporarionCdTabTipoOrigemMovLote: TIntegerField
      FieldName = 'nCdTabTipoOrigemMovLote'
    end
    object qryMovLoteTemporarioiIDRegistro: TIntegerField
      FieldName = 'iIDRegistro'
    end
    object qryMovLoteTemporariocOBS: TStringField
      FieldName = 'cOBS'
      Size = 50
    end
    object qryMovLoteTemporariocFlgCriarLote: TIntegerField
      FieldName = 'cFlgCriarLote'
    end
    object qryMovLoteTemporariocFlgProcessado: TIntegerField
      FieldName = 'cFlgProcessado'
    end
    object qryMovLoteTemporarionCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
  end
  object qryTabTipoOrigemMovLote: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabTipoOrigemMovLote'
      
        '      ,(dbo.fn_ZeroEsquerda(nCdTabTipoOrigemMovLote,3) + '#39' - '#39' +' +
        ' cNmTabTipoOrigemMovLote) as cNmTabTipoOrigemMovLote'
      '      ,cFlgEntradaSaida'
      'FROM TabTipoOrigemMovLote'
      'WHERE nCdTabTipoOrigemMovLote = :nPK')
    Left = 328
    Top = 184
    object qryTabTipoOrigemMovLotenCdTabTipoOrigemMovLote: TIntegerField
      FieldName = 'nCdTabTipoOrigemMovLote'
    end
    object qryTabTipoOrigemMovLotecNmTabTipoOrigemMovLote: TStringField
      FieldName = 'cNmTabTipoOrigemMovLote'
      Size = 50
    end
    object qryTabTipoOrigemMovLotecFlgEntradaSaida: TStringField
      FieldName = 'cFlgEntradaSaida'
      FixedChar = True
      Size = 1
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTabTipoOrigemMovLote
    Left = 384
    Top = 240
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      
        '      ,Convert(VARCHAR(10),nCdProduto) + '#39' - '#39' + cNmProduto as c' +
        'NmProduto'
      '      ,nCdTabTipoModoGestaoProduto'
      '      ,cFlgControlaValidadeLote'
      '      ,iDiasDisponibilidadeLote'
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK')
    Left = 512
    Top = 208
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      ReadOnly = True
      Size = 163
    end
    object qryProdutonCdTabTipoModoGestaoProduto: TIntegerField
      FieldName = 'nCdTabTipoModoGestaoProduto'
    end
    object qryProdutocFlgControlaValidadeLote: TIntegerField
      FieldName = 'cFlgControlaValidadeLote'
    end
    object qryProdutoiDiasDisponibilidadeLote: TIntegerField
      FieldName = 'iDiasDisponibilidadeLote'
    end
  end
  object DataSource2: TDataSource
    DataSet = qryProduto
    Left = 392
    Top = 248
  end
  object dsTempLote: TDataSource
    DataSet = qryMovLoteTemporario
    Left = 292
    Top = 252
  end
  object qrySomaQtdeMov: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTabTipoOrigemMovLote'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'iIDRegistro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Sum(nQtdeMov) as nQtdeMov'
      '  FROM MovLoteTemporario'
      ' WHERE nCdTabTipoOrigemMovLote = :nCdTabTipoOrigemMovLote'
      '   AND iIDRegistro             = :iIDRegistro')
    Left = 236
    Top = 276
    object qrySomaQtdeMovnQtdeMov: TBCDField
      FieldName = 'nQtdeMov'
      ReadOnly = True
      Precision = 32
    end
  end
  object qryLocalizaLoteEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProduto'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdLocalEstoque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'cNrLote'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 30
        Size = 30
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLoteProduto'
      '      ,dDtFabricacao'
      '      ,dDtValidade'
      '      ,dDtDisponibilidade'
      '      ,nSaldoLote'
      '  FROM LoteProduto'
      ' WHERE nCdProduto      = :nCdProduto'
      '   AND nCdLocalEstoque = :nCdLocalEstoque'
      '   AND cNrLote         = :cNrLote')
    Left = 412
    Top = 324
    object qryLocalizaLoteEstoquenCdLoteProduto: TAutoIncField
      FieldName = 'nCdLoteProduto'
      ReadOnly = True
    end
    object qryLocalizaLoteEstoquedDtFabricacao: TDateTimeField
      FieldName = 'dDtFabricacao'
    end
    object qryLocalizaLoteEstoquedDtValidade: TDateTimeField
      FieldName = 'dDtValidade'
    end
    object qryLocalizaLoteEstoquedDtDisponibilidade: TDateTimeField
      FieldName = 'dDtDisponibilidade'
    end
    object qryLocalizaLoteEstoquenSaldoLote: TBCDField
      FieldName = 'nSaldoLote'
      Precision = 12
      Size = 2
    end
  end
  object qryLocalizaLote: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProduto'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cNrLote'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 30
        Size = 30
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoteProduto'
      '      ,dDtFabricacao'
      '      ,dDtValidade'
      '      ,dDtDisponibilidade'
      '      ,nSaldoLote'
      '  FROM LoteProduto'
      ' WHERE nCdProduto      = :nCdProduto'
      '   AND cNrLote         = :cNrLote'
      '   AND nCdStatus       = 1'
      '   AND nSaldoLote      > 0')
    Left = 460
    Top = 316
    object qryLocalizaLotenCdLoteProduto: TAutoIncField
      FieldName = 'nCdLoteProduto'
      ReadOnly = True
    end
    object qryLocalizaLotedDtFabricacao: TDateTimeField
      FieldName = 'dDtFabricacao'
    end
    object qryLocalizaLotedDtValidade: TDateTimeField
      FieldName = 'dDtValidade'
    end
    object qryLocalizaLotedDtDisponibilidade: TDateTimeField
      FieldName = 'dDtDisponibilidade'
    end
    object qryLocalizaLotenSaldoLote: TBCDField
      FieldName = 'nSaldoLote'
      Precision = 12
      Size = 2
    end
  end
  object qryListaSeriaisSaida: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'iIDRegistro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT LoteProduto.cNrLote'
      '      ,LoteProduto.nCdProduto'
      '      ,SUM(ABS(MovLoteProduto.nQtde)) nQtde'
      '  FROM MovLoteProduto'
      
        '       INNER JOIN LoteProduto ON LoteProduto.nCdLoteProduto = Mo' +
        'vLoteProduto.nCdLoteProduto'
      ' WHERE MovLoteProduto.iIDRegistro             = :iIDRegistro'
      '   AND MovLoteProduto.nCdTabTipoOrigemMovLote = 4'
      ' GROUP BY LoteProduto.cNrLote'
      '         ,LoteProduto.nCdProduto')
    Left = 68
    Top = 220
    object qryListaSeriaisSaidacNrLote: TStringField
      FieldName = 'cNrLote'
      Size = 30
    end
    object qryListaSeriaisSaidanCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryListaSeriaisSaidanQtde: TBCDField
      FieldName = 'nQtde'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
end
