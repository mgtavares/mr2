inherited rptOrcamentoPrevReal_View_Tela: TrptOrcamentoPrevReal_View_Tela
  Caption = 'Gest'#227'o Or'#231'ament'#225'ria - Previsto x Realizado'
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    Align = alClient
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dsSPREL_ORCAMENTO_PREVREAL
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      object cxGrid1DBTableView1cNmGrupoPlanoConta: TcxGridDBColumn
        Caption = 'Grupo'
        DataBinding.FieldName = 'cNmGrupoPlanoConta'
        Visible = False
        GroupIndex = 0
      end
      object cxGrid1DBTableView1cNmPlanoConta: TcxGridDBColumn
        Caption = 'Conta'
        DataBinding.FieldName = 'cNmPlanoConta'
        Width = 164
      end
      object cxGrid1DBTableView1nValPrevJaneiro: TcxGridDBColumn
        Caption = 'Janeiro Prev.'
        DataBinding.FieldName = 'nValPrevJaneiro'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealJaneiro: TcxGridDBColumn
        Caption = 'Janeiro Real.'
        DataBinding.FieldName = 'nValRealJaneiro'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercJaneiro: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercJaneiro'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object cxGrid1DBTableView1nValPrevFevereiro: TcxGridDBColumn
        Caption = 'Fevereiro Prev.'
        DataBinding.FieldName = 'nValPrevFevereiro'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealFevereiro: TcxGridDBColumn
        Caption = 'Fevereiro Real.'
        DataBinding.FieldName = 'nValRealFevereiro'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercFevereiro: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercFevereiro'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object cxGrid1DBTableView1nValPrevMarco: TcxGridDBColumn
        Caption = 'Mar'#231'o Prev.'
        DataBinding.FieldName = 'nValPrevMarco'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealMarco: TcxGridDBColumn
        Caption = 'Mar'#231'o Real.'
        DataBinding.FieldName = 'nValRealMarco'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercMarco: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercMarco'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object cxGrid1DBTableView1nValPrevAbril: TcxGridDBColumn
        Caption = 'Abril Prev.'
        DataBinding.FieldName = 'nValPrevAbril'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealAbril: TcxGridDBColumn
        Caption = 'Abril Real.'
        DataBinding.FieldName = 'nValRealAbril'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercAbril: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercAbril'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object cxGrid1DBTableView1nValPrevMaio: TcxGridDBColumn
        Caption = 'Maio Prev.'
        DataBinding.FieldName = 'nValPrevMaio'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealMaio: TcxGridDBColumn
        Caption = 'Maio Real.'
        DataBinding.FieldName = 'nValRealMaio'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercMaio: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercMaio'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object cxGrid1DBTableView1nValPrevJunho: TcxGridDBColumn
        Caption = 'Junho Prev.'
        DataBinding.FieldName = 'nValPrevJunho'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealJunho: TcxGridDBColumn
        Caption = 'Junho Real.'
        DataBinding.FieldName = 'nValRealJunho'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercJunho: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercJunho'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object cxGrid1DBTableView1nValPrevJulho: TcxGridDBColumn
        Caption = 'Julho Prev.'
        DataBinding.FieldName = 'nValPrevJulho'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealJulho: TcxGridDBColumn
        Caption = 'Julho Real.'
        DataBinding.FieldName = 'nValRealJulho'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercJulho: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercJulho'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object cxGrid1DBTableView1nValPrevAgosto: TcxGridDBColumn
        Caption = 'Agosto Prev.'
        DataBinding.FieldName = 'nValPrevAgosto'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealAgosto: TcxGridDBColumn
        Caption = 'Agosto Real.'
        DataBinding.FieldName = 'nValRealAgosto'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercAgosto: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercAgosto'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object cxGrid1DBTableView1nValPrevSetembro: TcxGridDBColumn
        Caption = 'Setembro Prev.'
        DataBinding.FieldName = 'nValPrevSetembro'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealSetembro: TcxGridDBColumn
        Caption = 'Setembro Real.'
        DataBinding.FieldName = 'nValRealSetembro'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercSetembro: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercSetembro'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object cxGrid1DBTableView1nValPrevOutubro: TcxGridDBColumn
        Caption = 'Outubro Prev.'
        DataBinding.FieldName = 'nValPrevOutubro'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealOutubro: TcxGridDBColumn
        Caption = 'Outubro Real.'
        DataBinding.FieldName = 'nValRealOutubro'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercOutubro: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercOutubro'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object cxGrid1DBTableView1nValPrevNovembro: TcxGridDBColumn
        Caption = 'Novembro Prev.'
        DataBinding.FieldName = 'nValPrevNovembro'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealNovembro: TcxGridDBColumn
        Caption = 'Novembro Real.'
        DataBinding.FieldName = 'nValRealNovembro'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercNovembro: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercNovembro'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object cxGrid1DBTableView1nValPrevDezembro: TcxGridDBColumn
        Caption = 'Dezembro Prev.'
        DataBinding.FieldName = 'nValPrevDezembro'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealDezembro: TcxGridDBColumn
        Caption = 'Dezembro Real.'
        DataBinding.FieldName = 'nValRealDezembro'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercDezembro: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercDezembro'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
      object cxGrid1DBTableView1nValPrevTotal: TcxGridDBColumn
        Caption = 'Total Prev.'
        DataBinding.FieldName = 'nValPrevTotal'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nValRealTotal: TcxGridDBColumn
        Caption = 'Total Real.'
        DataBinding.FieldName = 'nValRealTotal'
        HeaderAlignmentHorz = taRightJustify
      end
      object cxGrid1DBTableView1nPercTotal: TcxGridDBColumn
        Caption = '%'
        DataBinding.FieldName = 'nPercTotal'
        HeaderAlignmentHorz = taRightJustify
        Width = 53
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 424
    Top = 152
  end
  object SPREL_ORCAMENTO_PREVREAL: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SPREL_ORCAMENTO_PREVREAL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdOrcamento'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdGrupoPlanoConta'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdPlanoContaAux'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@cFlgRetornaDataSet'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 320
    Top = 160
    object SPREL_ORCAMENTO_PREVREALcNmOrcamento: TStringField
      FieldName = 'cNmOrcamento'
      ReadOnly = True
      Size = 87
    end
    object SPREL_ORCAMENTO_PREVREALcNmCC: TStringField
      FieldName = 'cNmCC'
      ReadOnly = True
      Size = 63
    end
    object SPREL_ORCAMENTO_PREVREALnCdGrupoPlanoConta: TIntegerField
      FieldName = 'nCdGrupoPlanoConta'
    end
    object SPREL_ORCAMENTO_PREVREALcNmGrupoPlanoConta: TStringField
      FieldName = 'cNmGrupoPlanoConta'
      Size = 50
    end
    object SPREL_ORCAMENTO_PREVREALnCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object SPREL_ORCAMENTO_PREVREALcNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevJaneiro: TBCDField
      FieldName = 'nValPrevJaneiro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealJaneiro: TBCDField
      FieldName = 'nValRealJaneiro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercJaneiro: TBCDField
      FieldName = 'nPercJaneiro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevFevereiro: TBCDField
      FieldName = 'nValPrevFevereiro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealFevereiro: TBCDField
      FieldName = 'nValRealFevereiro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercFevereiro: TBCDField
      FieldName = 'nPercFevereiro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevMarco: TBCDField
      FieldName = 'nValPrevMarco'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealMarco: TBCDField
      FieldName = 'nValRealMarco'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercMarco: TBCDField
      FieldName = 'nPercMarco'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevAbril: TBCDField
      FieldName = 'nValPrevAbril'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealAbril: TBCDField
      FieldName = 'nValRealAbril'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercAbril: TBCDField
      FieldName = 'nPercAbril'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevMaio: TBCDField
      FieldName = 'nValPrevMaio'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealMaio: TBCDField
      FieldName = 'nValRealMaio'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercMaio: TBCDField
      FieldName = 'nPercMaio'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevJunho: TBCDField
      FieldName = 'nValPrevJunho'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealJunho: TBCDField
      FieldName = 'nValRealJunho'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercJunho: TBCDField
      FieldName = 'nPercJunho'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevJulho: TBCDField
      FieldName = 'nValPrevJulho'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealJulho: TBCDField
      FieldName = 'nValRealJulho'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercJulho: TBCDField
      FieldName = 'nPercJulho'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevAgosto: TBCDField
      FieldName = 'nValPrevAgosto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealAgosto: TBCDField
      FieldName = 'nValRealAgosto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercAgosto: TBCDField
      FieldName = 'nPercAgosto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevSetembro: TBCDField
      FieldName = 'nValPrevSetembro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealSetembro: TBCDField
      FieldName = 'nValRealSetembro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercSetembro: TBCDField
      FieldName = 'nPercSetembro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevOutubro: TBCDField
      FieldName = 'nValPrevOutubro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealOutubro: TBCDField
      FieldName = 'nValRealOutubro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercOutubro: TBCDField
      FieldName = 'nPercOutubro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevNovembro: TBCDField
      FieldName = 'nValPrevNovembro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealNovembro: TBCDField
      FieldName = 'nValRealNovembro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercNovembro: TBCDField
      FieldName = 'nPercNovembro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevDezembro: TBCDField
      FieldName = 'nValPrevDezembro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealDezembro: TBCDField
      FieldName = 'nValRealDezembro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercDezembro: TBCDField
      FieldName = 'nPercDezembro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValPrevTotal: TBCDField
      FieldName = 'nValPrevTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnValRealTotal: TBCDField
      FieldName = 'nValRealTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object SPREL_ORCAMENTO_PREVREALnPercTotal: TBCDField
      FieldName = 'nPercTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsSPREL_ORCAMENTO_PREVREAL: TDataSource
    DataSet = SPREL_ORCAMENTO_PREVREAL
    Left = 360
    Top = 152
  end
end
