unit fEstornaContab;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, ImgList, ComCtrls, ToolWin, ExtCtrls,
  Mask, DB, ADODB, cxControls, cxContainer, cxEdit, cxProgressBar, GridsEh,
  DBGridEh, DBGridEhGrouping, cxPC;

type
  TfrmEstornaContab = class(TfrmProcesso_Padrao)
    qryParametro: TADOQuery;
    qryParametrocParametro: TStringField;
    qryParametrocValor: TStringField;
    qryParametrocDescricao: TStringField;
    usp_Contabiliza: TADOStoredProc;
    qryLogProcesso: TADOQuery;
    qryLogProcessodDtLog: TDateTimeField;
    qryLogProcessonCdUsuario: TIntegerField;
    qryLogProcessocNmUsuario: TStringField;
    qryLogProcessocObserv: TStringField;
    dsLogProcesso: TDataSource;
    GroupBox1: TGroupBox;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    Label2: TLabel;
    MaskEdit1: TMaskEdit;
    Label3: TLabel;
    MaskEdit3: TMaskEdit;
    Label1: TLabel;
    MaskEdit2: TMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEstornaContab: TfrmEstornaContab;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmEstornaContab.FormShow(Sender: TObject);
begin
  inherited;

  qryParametro.Close ;
  qryParametro.Parameters.ParamByName('cParametro').Value := 'DTCONTAB' ;
  qryParametro.Open  ;

  MaskEdit3.Text := DateTimeToStr(StrToDateTime(qryParametrocValor.Value)-10) ;

  Maskedit1.Text := qryParametrocValor.Value ;
  MaskEdit2.SetFocus;

  qryLogProcesso.Close ;
  qryLogProcesso.Open ;

end;

procedure TfrmEstornaContab.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(maskedit2.Text) = '/  /') then
  begin
      ShowMessage('Informe a data para o estorno.') ;
      MaskEdit2.SetFocus ;
      exit ;
  end ;

  If (StrToDateTime(MaskEdit2.Text) < StrToDateTime(MaskEdit3.Text)) then
  begin
      ShowMessage('A data para contabilização é inferior a data limite') ;
      MaskEdit2.SetFocus ;
      exit ;
  end ;


  if ((Trim(maskedit1.Text) <> '/  /') and (StrToDateTime(MaskEdit2.Text) > StrToDateTime(MaskEdit1.Text))) then
  begin
      ShowMessage('A data para estorno da contabilização deve ser inferior ou igual a data da última contabilização.') ;
      MaskEdit2.SetFocus ;
      exit ;
  end ;

  case MessageDlg('Confirma o processamento de estorno ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      usp_Contabiliza.Close ;
      usp_Contabiliza.Parameters.ParamByName('@dDtFechamento').Value := MaskEdit2.Text ;
      usp_Contabiliza.Parameters.ParamByName('@nCdUsuario').Value    := frmMenu.nCdUsuarioLogado;
      usp_Contabiliza.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      ShowMessage('Erro no processo de estorno da contabilização') ;
      raise ;
      exit ;
  end ;

  frmMenu.Connection.CommitTrans ;

  ShowMessage('Processo concluído com sucesso.') ;

  qryParametro.Close ;
  qryParametro.Parameters.ParamByName('cParametro').Value := 'DTCONTAB' ;
  qryParametro.Open  ;

  MaskEdit2.Text := '' ;

end;

initialization
    RegisterClass(tfrmEstornaContab) ;
end.
