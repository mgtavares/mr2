unit rLanctoCaixa_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptLanctoCaixa_view = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape1: TQRShape;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText6: TQRDBText;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape3: TQRShape;
    QRDBText16: TQRDBText;
    QRLabel26: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape7: TQRShape;
    QRDBText4: TQRDBText;
    SummaryBand1: TQRBand;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    usp_RelatoriocNmTipoLancto: TStringField;
    usp_RelatoriocHistorico: TStringField;
    usp_RelatorionValLancto: TFloatField;
    usp_RelatorionCdConta: TStringField;
    usp_RelatorionValTotalEntrada: TFloatField;
    usp_RelatorionValTotalSaida: TFloatField;
    usp_RelatoriodDtLancto: TStringField;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel1: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptLanctoCaixa_view: TrptLanctoCaixa_view;

implementation

{$R *.dfm}

end.
