unit fInventarioLoja_Apuracao_ExibeDiverg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ADODB, comObj, ExcelXP, dxPSGlbl, dxPSUtl,
  dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider,
  dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk,
  dxPSContainerLnk;

type
  TfrmInventarioLoja_Apuracao_ExibeDiverg = class(TfrmProcesso_Padrao)
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryDivergencias: TADOQuery;
    dsDivergencias: TDataSource;
    qryDivergenciasnCdProduto: TIntegerField;
    qryDivergenciascReferencia: TStringField;
    qryDivergenciascNmProduto: TStringField;
    qryDivergenciasnQtdeFisica: TBCDField;
    qryDivergenciasnQtdeLogica: TBCDField;
    qryDivergenciasnDiferenca: TBCDField;
    qryDivergenciasnValDiferenca: TBCDField;
    qryDivergenciascNmDepartamento: TStringField;
    qryDivergenciascNmCategoria: TStringField;
    qryDivergenciascNmSubCategoria: TStringField;
    qryDivergenciascNmSegmento: TStringField;
    qryDivergenciascNmMarca: TStringField;
    qryDivergenciascNmLinha: TStringField;
    cxGridDBTableView1nCdProduto: TcxGridDBColumn;
    cxGridDBTableView1cReferencia: TcxGridDBColumn;
    cxGridDBTableView1cNmProduto: TcxGridDBColumn;
    cxGridDBTableView1nQtdeFisica: TcxGridDBColumn;
    cxGridDBTableView1nQtdeLogica: TcxGridDBColumn;
    cxGridDBTableView1nDiferenca: TcxGridDBColumn;
    cxGridDBTableView1nValDiferenca: TcxGridDBColumn;
    cxGridDBTableView1cNmDepartamento: TcxGridDBColumn;
    cxGridDBTableView1cNmCategoria: TcxGridDBColumn;
    cxGridDBTableView1cNmSubCategoria: TcxGridDBColumn;
    cxGridDBTableView1cNmSegmento: TcxGridDBColumn;
    cxGridDBTableView1cNmMarca: TcxGridDBColumn;
    cxGridDBTableView1cNmLinha: TcxGridDBColumn;
    qryDivergenciascEAN: TStringField;
    cxGridDBTableView1DBColumn1: TcxGridDBColumn;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    dxComponentPrinter1: TdxComponentPrinter;
    qryReabreContagem: TADOQuery;
    ToolButton10: TToolButton;
    btFinalizarInventario: TToolButton;
    ToolButton12: TToolButton;
    btCancelarInventario: TToolButton;
    SP_FINALIZA_INVENTARIO_LOJA: TADOStoredProc;
    qryCancelaInventario: TADOQuery;
    procedure ToolButton9Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btFinalizarInventarioClick(Sender: TObject);
    procedure btCancelarInventarioClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    bEncerramento : boolean ;
  end;

var
  frmInventarioLoja_Apuracao_ExibeDiverg: TfrmInventarioLoja_Apuracao_ExibeDiverg;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmInventarioLoja_Apuracao_ExibeDiverg.ToolButton9Click(
  Sender: TObject);
var
  cFiltro       : string ;
  linha, coluna,LCID : integer;
  planilha      : variant;
  valorcampo    : string;
begin

    try
        planilha:= CreateoleObject('Excel.Application');
    except
        MensagemErro('Ocorreu um erro no processamento da planilha, talvez o aplicativo de planilhas n�o esteja instalado corretamente.') ;
        exit ;
    end ;

    LCID := GetUserDefaultLCID;
    
    planilha.DisplayAlerts  := False;
    planilha.WorkBooks.add(1);
    planilha.ScreenUpdating := true;

    planilha.caption := 'Diverg�ncias Invent�rio ' + intToStr(qryDivergencias.Parameters.ParamByName('nPK').Value);

    qryDivergencias.First;
    
    for linha := 0 to qryDivergencias.RecordCount - 1 do
    begin

       for coluna := 1 to qryDivergencias.FieldCount do
       begin

         valorcampo := qryDivergencias.Fields[coluna - 1].AsString;
         planilha.cells[linha + 2,coluna] := valorCampo;

         if (qryDivergencias.Fields[coluna - 1].DataType = ftBCD) then
         begin
             planilha.Cells[Linha+2,coluna].NumberFormat        := '#.##0,00' ;
             planilha.Cells[Linha+2,Coluna].HorizontalAlignment := xlRight ;
         end ;

         {Range[,'G'+LineString].NumberFormat := '$#,##0.00';}

       end;

       qryDivergencias.Next;

    end;

    for coluna := 1 to qryDivergencias.FieldCount do
    begin

       valorcampo := qryDivergencias.Fields[coluna - 1].DisplayLabel;
       planilha.cells[1,coluna] := valorcampo;

    end;
    
    planilha.columns.Autofit;
    planilha.Visible := True;

    qryDivergencias.First;

end;

procedure TfrmInventarioLoja_Apuracao_ExibeDiverg.ToolButton7Click(
  Sender: TObject);
begin
  inherited;

  dxComponentPrinter1.Preview(True,nil);
end;

procedure TfrmInventarioLoja_Apuracao_ExibeDiverg.ToolButton5Click(
  Sender: TObject);
begin
  inherited;

  if (MessageDlg('Confirma a reabertura da contagem deste invent�rio ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryReabreContagem.Close;
      qryReabreContagem.Parameters.ParamByName('nCdInventario').Value := qryDivergencias.Parameters.ParamByName('nPK').Value ;
      qryReabreContagem.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Inventario reaberto para contagem.') ;

  Close ;

end;

procedure TfrmInventarioLoja_Apuracao_ExibeDiverg.FormShow(
  Sender: TObject);
begin
  inherited;

  btFinalizarInventario.Enabled := bEncerramento ;
  btCancelarInventario.Enabled  := bEncerramento ;
  
end;

procedure TfrmInventarioLoja_Apuracao_ExibeDiverg.btFinalizarInventarioClick(
  Sender: TObject);
begin
  inherited;

  if (MessageDlg('Este processo ir� movimentar o estoque e encerr� o invent�rio.' +#13#13 + 'Tem certeza que deseja finalizar ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      SP_FINALIZA_INVENTARIO_LOJA.Close;
      SP_FINALIZA_INVENTARIO_LOJA.Parameters.ParamByName('@nCdInventario').Value := qryDivergencias.Parameters.ParamByName('nPK').Value;
      SP_FINALIZA_INVENTARIO_LOJA.Parameters.ParamByName('@nCdUsuario').Value    := frmMenu.nCdUsuarioLogado;
      SP_FINALIZA_INVENTARIO_LOJA.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Estoque atualizado com Sucesso.') ;
  Close ;

end;

procedure TfrmInventarioLoja_Apuracao_ExibeDiverg.btCancelarInventarioClick(
  Sender: TObject);
begin
  inherited;

  if (MessageDlg('Este processo ir� cancelar o invent�rio e n�o poder� ser desfeito.' +#13#13 + 'Tem certeza que deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryCancelaInventario.Close;
      qryCancelaInventario.Parameters.ParamByName('nCdInventario').Value := qryDivergencias.Parameters.ParamByName('nPK').Value;
      qryCancelaInventario.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Invent�rio cancelado com Sucesso.') ;
  Close ;

end;

end.
