unit fConciliaBancoManual_IncluiLancto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxControls, cxContainer, cxEdit, cxTextEdit, cxCurrencyEdit, StdCtrls,
  DB, DBCtrls, ADODB, Mask;

type
  TfrmConciliaBancoManual_IncluiLancto = class(TfrmProcesso_Padrao)
    edtTipoLancto: TMaskEdit;
    qryTipoLancto: TADOQuery;
    qryTipoLanctonCdTipoLancto: TIntegerField;
    qryTipoLanctocNmTipoLancto: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    edtDocumento: TEdit;
    Label2: TLabel;
    edtHistorico: TEdit;
    Label3: TLabel;
    edtValor: TcxCurrencyEdit;
    Label4: TLabel;
    SP_INCLUI_LANCTO_EXTRATO: TADOStoredProc;
    procedure edtTipoLanctoExit(Sender: TObject);
    procedure edtTipoLanctoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdContaBancaria : integer ;
    dDtExtrato       : string ;
  end;

var
  frmConciliaBancoManual_IncluiLancto: TfrmConciliaBancoManual_IncluiLancto;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmConciliaBancoManual_IncluiLancto.edtTipoLanctoExit(
  Sender: TObject);
begin
  inherited;

  qryTipoLancto.Close ;
  PosicionaQuery(qryTipoLancto, edtTipoLancto.Text) ;

end;

procedure TfrmConciliaBancoManual_IncluiLancto.edtTipoLanctoKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(151);

        If (nPK > 0) then
        begin
            qryTipoLancto.Close ;
            PosicionaQuery(qryTipoLancto, IntToStr(nPK)) ;
            edtTipoLancto.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConciliaBancoManual_IncluiLancto.FormShow(Sender: TObject);
begin
  inherited;

  qryTipoLancto.Close ;
  edtTipoLancto.Text := '' ;
  edtHistorico.Text  := '' ;
  edtDocumento.Text  := '' ;
  edtValor.Value     := 0 ;
  
  edtTipoLancto.SetFocus;

end;

procedure TfrmConciliaBancoManual_IncluiLancto.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (DbEdit1.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de lan�amento.') ;
      exit ;
  end ;

  if (edtValor.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor do lan�amento.') ;
      exit ;
  end ;

  if (TRIM(edtHistorico.Text) = '') then
      edtHistorico.Text := qryTipoLanctocNmTipoLancto.Value ;

  case MessageDlg('Confirma a inclus�o deste lan�amento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      SP_INCLUI_LANCTO_EXTRATO.Close ;
      SP_INCLUI_LANCTO_EXTRATO.Parameters.ParamByName('@nCdContaBancaria').Value := nCdContaBancaria ;
      SP_INCLUI_LANCTO_EXTRATO.Parameters.ParamByName('@cDtExtrato').Value       := dDtExtrato       ;
      SP_INCLUI_LANCTO_EXTRATO.Parameters.ParamByName('@nCdTipoLancto').Value    := qryTipoLanctonCdTipoLancto.Value ;
      SP_INCLUI_LANCTO_EXTRATO.Parameters.ParamByName('@nValLancto').Value       := edtValor.Value   ;
      SP_INCLUI_LANCTO_EXTRATO.Parameters.ParamByName('@cHistorico').Value       := edtHistorico.Text ;
      SP_INCLUI_LANCTO_EXTRATO.Parameters.ParamByName('@cDocumento').Value       := edtDocumento.Text ;
      SP_INCLUI_LANCTO_EXTRATO.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
      SP_INCLUI_LANCTO_EXTRATO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  Close ;

end;

end.
