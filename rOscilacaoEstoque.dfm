inherited rptOscilacaoEstoque: TrptOscilacaoEstoque
  Width = 1152
  Height = 780
  Caption = 'Relat'#243'rio de oscila'#231#227'o de estoque'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1136
    Height = 718
  end
  object Label1: TLabel [1]
    Left = 89
    Top = 46
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    Color = clBtnFace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
  end
  object Label2: TLabel [2]
    Left = 49
    Top = 71
    Width = 81
    Height = 13
    Alignment = taRightJustify
    Caption = 'Local de Estoque'
  end
  object Label3: TLabel [3]
    Left = 45
    Top = 97
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo de Produto'
  end
  object Label4: TLabel [4]
    Left = 33
    Top = 122
    Width = 97
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo de Invent'#225'rio'
  end
  object Label5: TLabel [5]
    Left = 61
    Top = 147
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
  end
  object Label6: TLabel [6]
    Left = 83
    Top = 172
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria'
  end
  object Label7: TLabel [7]
    Left = 65
    Top = 197
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'SubCategoria'
  end
  object Label8: TLabel [8]
    Left = 22
    Top = 272
    Width = 108
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Movimenta'#231#227'o'
  end
  object Label9: TLabel [9]
    Left = 82
    Top = 222
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Segmento'
  end
  object Label10: TLabel [10]
    Left = 92
    Top = 247
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
  end
  object Label11: TLabel [11]
    Left = 216
    Top = 272
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label12: TLabel [12]
    Left = 29
    Top = 297
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Recebimento'
  end
  object Label13: TLabel [13]
    Left = 215
    Top = 297
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label14: TLabel [14]
    Left = 324
    Top = 297
    Width = 158
    Height = 13
    Caption = '('#218'ltimo Recebimento do Produto)'
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtEmpresa: TER2LookupMaskEdit [16]
    Left = 133
    Top = 38
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    OnBeforePosicionaQry = edtEmpresaBeforePosicionaQry
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  object edtLocalEstoque: TER2LookupMaskEdit [17]
    Left = 133
    Top = 63
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    CodigoLookup = 87
    QueryLookup = qryLocalEstoque
  end
  object edtDepartamento: TER2LookupMaskEdit [18]
    Left = 133
    Top = 139
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 5
    Text = '         '
    CodigoLookup = 45
    QueryLookup = qryDepartamento
  end
  object DBEdit1: TDBEdit [19]
    Tag = 1
    Left = 277
    Top = 38
    Width = 654
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 6
  end
  object DBEdit2: TDBEdit [20]
    Tag = 1
    Left = 205
    Top = 38
    Width = 69
    Height = 21
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 7
  end
  object DBEdit3: TDBEdit [21]
    Tag = 1
    Left = 205
    Top = 63
    Width = 654
    Height = 21
    DataField = 'cNmLocalEstoque'
    DataSource = dsLocalEstoque
    TabOrder = 8
  end
  object DBEdit4: TDBEdit [22]
    Tag = 1
    Left = 205
    Top = 139
    Width = 654
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = dsDepartamento
    TabOrder = 9
  end
  object DBEdit5: TDBEdit [23]
    Tag = 1
    Left = 205
    Top = 164
    Width = 654
    Height = 21
    DataField = 'cNmCategoria'
    DataSource = dsCategoria
    TabOrder = 10
  end
  object edtCategoria: TER2LookupMaskEdit [24]
    Left = 133
    Top = 164
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 11
    Text = '         '
    OnBeforeLookup = edtCategoriaBeforeLookup
    OnBeforePosicionaQry = edtCategoriaBeforePosicionaQry
    CodigoLookup = 46
    QueryLookup = qryCategoria
  end
  object DBEdit6: TDBEdit [25]
    Tag = 1
    Left = 205
    Top = 189
    Width = 654
    Height = 21
    DataField = 'cNmSubCategoria'
    DataSource = dsSubCategoria
    TabOrder = 12
  end
  object edtSubCategoria: TER2LookupMaskEdit [26]
    Left = 133
    Top = 189
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 13
    Text = '         '
    OnBeforeLookup = edtSubCategoriaBeforeLookup
    OnBeforePosicionaQry = edtSubCategoriaBeforePosicionaQry
    CodigoLookup = 47
    QueryLookup = qrySubCategoria
  end
  object DBEdit7: TDBEdit [27]
    Tag = 1
    Left = 205
    Top = 214
    Width = 654
    Height = 21
    DataField = 'cNmSegmento'
    DataSource = dsSegmento
    TabOrder = 14
  end
  object edtSegmento: TER2LookupMaskEdit [28]
    Left = 133
    Top = 214
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 15
    Text = '         '
    OnBeforeLookup = edtSegmentoBeforeLookup
    OnBeforePosicionaQry = edtSegmentoBeforePosicionaQry
    CodigoLookup = 48
    QueryLookup = qrySegmento
  end
  object DBEdit8: TDBEdit [29]
    Tag = 1
    Left = 205
    Top = 114
    Width = 654
    Height = 21
    DataField = 'cNmGrupoInventario'
    DataSource = dsGrupoInventario
    TabOrder = 16
  end
  object edtGrupoInventario: TER2LookupMaskEdit [30]
    Left = 133
    Top = 114
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 4
    Text = '         '
    CodigoLookup = 153
    QueryLookup = qryGrupoInventario
  end
  object edtProduto: TER2LookupMaskEdit [31]
    Left = 133
    Top = 239
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 17
    Text = '         '
    OnBeforeLookup = edtProdutoBeforeLookup
    CodigoLookup = 76
    QueryLookup = qryProduto
  end
  object edtGrupoProduto: TER2LookupMaskEdit [32]
    Left = 133
    Top = 89
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 3
    Text = '         '
    CodigoLookup = 69
    QueryLookup = qryGrupoProduto
  end
  object MaskEditDataInicial: TMaskEdit [33]
    Left = 133
    Top = 264
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 18
    Text = '  /  /    '
  end
  object MaskEditDataFinal: TMaskEdit [34]
    Left = 243
    Top = 264
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 19
    Text = '  /  /    '
  end
  object DBEdit9: TDBEdit [35]
    Tag = 1
    Left = 205
    Top = 239
    Width = 654
    Height = 21
    DataField = 'cNmProduto'
    DataSource = dsProduto
    TabOrder = 22
  end
  object DBEdit10: TDBEdit [36]
    Tag = 1
    Left = 205
    Top = 89
    Width = 654
    Height = 21
    DataField = 'cNmGrupoProduto'
    DataSource = dsGrupoProduto
    TabOrder = 23
  end
  object RadioGroupExibirValores: TRadioGroup [37]
    Left = 17
    Top = 324
    Width = 185
    Height = 40
    Caption = 'Exibir Valores'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 24
  end
  object RadioGroupValorizacao: TRadioGroup [38]
    Left = 208
    Top = 324
    Width = 449
    Height = 40
    Caption = 'M'#233'todo de Valoriza'#231#227'o'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      #218'ltimo Pre'#231'o Custo'
      'Pre'#231'o M'#233'dio'
      'Pre'#231'o '#218'ltimo Recebimento')
    TabOrder = 25
  end
  object RadioGroupProdutoAtivo: TRadioGroup [39]
    Left = 17
    Top = 370
    Width = 185
    Height = 40
    Caption = 'Somente Produto Ativo'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 26
  end
  object RadioGroupSaida: TRadioGroup [40]
    Left = 209
    Top = 416
    Width = 305
    Height = 41
    Caption = 'Modo de Exibi'#231#227'o'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Relat'#243'rio'
      'Planilha'
      'Tela')
    TabOrder = 29
  end
  object RadioGroupTipoSaida: TRadioGroup [41]
    Left = 17
    Top = 416
    Width = 185
    Height = 40
    Caption = 'Tipo de Sa'#237'da'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Anal'#237'tico'
      'Sint'#233'tico')
    TabOrder = 28
  end
  object MaskEditDtRecInicial: TMaskEdit [42]
    Left = 133
    Top = 289
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 20
    Text = '  /  /    '
  end
  object MaskEditDtRecFinal: TMaskEdit [43]
    Left = 243
    Top = 289
    Width = 72
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 21
    Text = '  /  /    '
  end
  object RadioGroupAgrupamento: TRadioGroup [44]
    Left = 208
    Top = 369
    Width = 305
    Height = 41
    Caption = 'Agrupamento'
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'Produto Estruturado'
      'Produto Final')
    TabOrder = 27
  end
  inherited ImageList1: TImageList
    Left = 832
    Top = 304
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nPK')
    Left = 880
    Top = 88
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 912
    Top = 88
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque, cNmLocalEstoque'
      'FROM LocalEstoque'
      'WHERE nCdLocalEstoque = :nPK')
    Left = 880
    Top = 124
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdDepartamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CATEGORIA'
      'WHERE nCdCategoria = :nPK'
      'AND nCdDepartamento = :nCdDepartamento')
    Left = 880
    Top = 192
    object qryCategorianCdCategoria: TAutoIncField
      FieldName = 'nCdCategoria'
      ReadOnly = True
    end
    object qryCategorianCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
    object qryCategorianCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM SUBCATEGORIA'
      'WHERE nCdSubCategoria = :nPK'
      'AND nCdCategoria = :nCdCategoria')
    Left = 880
    Top = 224
    object qrySubCategorianCdSubCategoria: TAutoIncField
      FieldName = 'nCdSubCategoria'
      ReadOnly = True
    end
    object qrySubCategorianCdCategoria: TIntegerField
      FieldName = 'nCdCategoria'
    end
    object qrySubCategoriacNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object qrySubCategorianCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdSubCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM SEGMENTO'
      'WHERE nCdSubCategoria = :nCdSubCategoria'
      'AND nCdSegmento = :nPK')
    Left = 886
    Top = 351
    object qrySegmentonCdSegmento: TAutoIncField
      FieldName = 'nCdSegmento'
      ReadOnly = True
    end
    object qrySegmentonCdSubCategoria: TIntegerField
      FieldName = 'nCdSubCategoria'
    end
    object qrySegmentocNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
    object qrySegmentonCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM DEPARTAMENTO'
      'WHERE nCdDepartamento = :nPK')
    Left = 880
    Top = 160
    object AutoIncField3: TAutoIncField
      FieldName = 'nCdDepartamento'
      ReadOnly = True
    end
    object StringField3: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object IntegerField5: TIntegerField
      FieldName = 'nCdStatus'
    end
    object IntegerField6: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
  end
  object dsLocalEstoque: TDataSource
    DataSet = qryLocalEstoque
    Left = 912
    Top = 124
  end
  object dsDepartamento: TDataSource
    DataSet = qryDepartamento
    Left = 914
    Top = 159
  end
  object dsCategoria: TDataSource
    DataSet = qryCategoria
    Left = 912
    Top = 192
  end
  object dsSegmento: TDataSource
    DataSet = qrySegmento
    Left = 918
    Top = 352
  end
  object qryGrupoInventario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoInventario'
      '             ,cNmGrupoInventario '
      'FROM GrupoInventario'
      'WHERE nCdGrupoInventario = :nPK')
    Left = 883
    Top = 256
    object qryGrupoInventarionCdGrupoInventario: TIntegerField
      FieldName = 'nCdGrupoInventario'
    end
    object qryGrupoInventariocNmGrupoInventario: TStringField
      FieldName = 'cNmGrupoInventario'
      Size = 50
    end
  end
  object dsGrupoInventario: TDataSource
    DataSet = qryGrupoInventario
    Left = 915
    Top = 256
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '             ,cNmProduto '
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 884
    Top = 288
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object dsProduto: TDataSource
    DataSet = qryProduto
    Left = 917
    Top = 288
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto'
      '              ,cNmGrupoProduto'
      'FROM GrupoProduto'
      'WHERE nCdGrupoProduto = :nPk')
    Left = 885
    Top = 319
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
  object dsGrupoProduto: TDataSource
    DataSet = qryGrupoProduto
    Left = 917
    Top = 321
  end
  object dsSubCategoria: TDataSource
    DataSet = qrySubCategoria
    Left = 914
    Top = 224
  end
  object ER2Excel1: TER2Excel
    Titulo = 'Rel. Oscila'#231#227'o de Estoque'
    AutoSizeCol = True
    Background = clWhite
    Transparent = False
    OnBeforeExport = ER2Excel1BeforeExport
    OnAfterExport = ER2Excel1AfterExport
    Left = 832
    Top = 272
  end
end
