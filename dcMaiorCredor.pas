unit dcMaiorCredor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, Mxstore, MXDB, DB, ADODB, StdCtrls, Mask,
  ExtCtrls, MXPIVSRC, Grids, MXGRID, ImgList, ComCtrls, ToolWin, GridsEh,
  DBGridEh;

type
  TdcmMaiorCredor = class(TfrmProcesso_Padrao)
    ImageList2: TImageList;
    Label1: TLabel;
    Label2: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    qryCube: TADOQuery;
    qryParametro: TADOQuery;
    qryParametrocValor: TStringField;
    qryCubecTerceiro: TStringField;
    qryCubenValAcumulado: TBCDField;
    qryCubecSigla: TStringField;
    DBGridEh1: TDBGridEh;
    DataSource1: TDataSource;
    RadioGroup1: TRadioGroup;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmMaiorCredor: TdcmMaiorCredor;

implementation

uses fMenu;

{$R *.dfm}

procedure TdcmMaiorCredor.ToolButton1Click(Sender: TObject);
begin
  inherited;
  if (trim(MaskEdit1.Text) = '/  /') or (Trim(MaskEdit2.Text) = '/  /') then
  begin
      qryParametro.Close ;
      qryParametro.Parameters.ParamByName('cParametro').Value := 'DTCONTAB' ;
      qryParametro.Open ;

      if not qryParametro.eof then
      begin
          if (qryParametrocValor.Value <> '') then
          begin
              MaskEdit2.Text := qryParametrocValor.Value ;
              MaskEdit1.Text := DateTimeToStr(StrToDateTime(qryParametrocValor.Value) - 90) ;
          end ;
      end ;

  end ;

  if (trim(MaskEdit1.Text) = '/  /') or (Trim(MaskEdit2.Text) = '/  /') then
  begin
      ShowMessage('Informe o per�odo de an�lise.') ;
      exit ;
  end ;

  If (radioGroup1.ItemIndex = -1) then
  begin
      ShowMessage('Selecione o tipo de visualiza��o') ;
      exit ;
  end ;

  qryCube.Close ;
  qryCube.Parameters.ParamByName('dDtInicial').Value := Trim(MaskEdit1.Text) ;
  qryCube.Parameters.ParamByName('dDtFinal').Value   := Trim(MaskEdit2.Text) ;
  qryCube.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;

  If (radioGroup1.ItemIndex = 0) then
      qryCube.Parameters.ParamByName('cTipo').Value := 'P' ;

  If (radioGroup1.ItemIndex = 1) then
      qryCube.Parameters.ParamByName('cTipo').Value := 'R' ;

  qryCube.Open ;

end;

initialization
    RegisterClass(TdcmMaiorCredor) ;

end.
