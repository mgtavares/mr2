object frmAtualizacaoMonetaria: TfrmAtualizacaoMonetaria
  Left = 192
  Top = 122
  Width = 870
  Height = 500
  Caption = 'frmAtualizacaoMonetaria'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object qryTituloFinanceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdtitulo'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      ' SELECT TIT.nCdTitulo'
      '              ,TIT.nCdEspTit'
      '              ,cNmEspTit'
      '              ,TIT.nCdSP'
      '              ,TIT.cNrTit'
      '              ,TIT.iParcela'
      '              ,TIT.nCdTerceiro'
      '              ,cNmTerceiro'
      '              ,cCNPJCPF'
      '              ,cRG'
      '              ,TIT.nCdMoeda'
      '              ,TIT.cSenso'
      '              ,TIT.dDtEmissao'
      '              ,TIT.dDtLiq'
      '              ,TIT.dDtVenc'
      
        '              ,convert(char(10), DATEADD(D,1-Day(TIT.dDtVenc),TI' +
        'T.dDtVenc),103) as cDtCotacao'
      '              ,TIT.nValTit'
      '              ,TIT.nSaldoTit'
      '              ,TIT.nValIndiceCorrecao'
      '              ,TIT.nValCorrecao'
      '              ,TIT.nValJuro'
      '              ,TIT.nValMulta'
      '              ,TIT.nvalDesconto'
      '              ,TIT.nValAbatimento'
      '              ,Tit.dDtCancel'
      '              ,nValLiq'
      '              ,ContratoEmpImobiliario.iParcelas'
      '              ,ContratoEmpImobiliario.cNrUnidade'
      '              ,ContratoEmpImobiliario.dDtContrato'
      '              ,ContratoEmpImobiliario.nCdIndiceReajuste'
      '              ,BlocoEmpImobiliario.cNmBlocoEmpImobiliario'
      '              ,EmpImobiliario.cNmEmpImobiliario'
      '              ,EmpImobiliario.cEndereco'
      
        '              ,TabTipoParcContratoEmpImobiliario.cNmTabTipoParcC' +
        'ontratoEmpImobiliario'
      '              ,IndiceReajuste.cNmIndiceReajuste'
      
        '              ,ParcContratoEmpImobiliario.nCdTabTipoParcContrato' +
        'EmpImobiliario'
      '          FROM Titulo TIT'
      
        '              LEFT  JOIN EspTit                                 ' +
        '             ON EspTit.nCdEspTit                           = TIT' +
        '.nCdEspTit'
      
        '              LEFT  JOIN Terceiro                               ' +
        '            ON Terceiro.nCdTerceiro                       = TIT.' +
        'nCdTerceiro'
      
        '              LEFT  JOIN ContratoEmpImobiliario                 ' +
        '    ON ContratoEmpImobiliario.nCdTerceiro         = Tit.nCdTerce' +
        'iro AND ContratoEmpImobiliario.cNumContrato = TIT.cNrTit '
      
        '              LEFT  JOIN BlocoEmpImobiliario                    ' +
        '      ON BlocoEmpImobiliario.nCdBlocoEmpImobiliario = ContratoEm' +
        'pImobiliario.nCdBlocoEmpImobiliario'
      
        '              LEFT  JOIN EmpImobiliario                         ' +
        '          ON EmpImobiliario.nCdEmpImobiliario           = BlocoE' +
        'mpImobiliario.nCdEmpImobiliario'
      
        '              LEFT  JOIN ParcContratoEmpImobiliario             ' +
        ' ON ParcContratoEmpImobiliario.nCdParcContratoEmpImobiliario    ' +
        '           = TIT.nCdParcContratoEmpImobiliario'
      
        '              LEFT  JOIN TabTipoParcContratoEmpImobiliario ON Ta' +
        'bTipoParcContratoEmpImobiliario.nCdTabTipoParcContratoEmpImobili' +
        'ario = ParcContratoEmpImobiliario.nCdTabTipoParcContratoEmpImobi' +
        'liario'
      
        '              LEFT  JOIN IndiceReajuste                         ' +
        '         ON IndiceReajuste.nCdIndiceReajuste = ContratoEmpImobil' +
        'iario.nCdIndiceReajuste'
      '         WHERE TIT.nCdTitulo =:nCdTitulo'
      '')
    Left = 584
    Top = 88
    object qryTituloFinanceironCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTituloFinanceironCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryTituloFinanceirocNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTituloFinanceironCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryTituloFinanceirocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTituloFinanceiroiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTituloFinanceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTituloFinanceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTituloFinanceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTituloFinanceirocRG: TStringField
      FieldName = 'cRG'
      Size = 15
    end
    object qryTituloFinanceironCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryTituloFinanceirocSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryTituloFinanceirodDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTituloFinanceirodDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
    object qryTituloFinanceirodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTituloFinanceirocDtCotacao: TStringField
      FieldName = 'cDtCotacao'
      ReadOnly = True
      FixedChar = True
      Size = 10
    end
    object qryTituloFinanceironValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTituloFinanceironSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTituloFinanceironValIndiceCorrecao: TBCDField
      FieldName = 'nValIndiceCorrecao'
      Precision = 12
      Size = 8
    end
    object qryTituloFinanceironValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      Precision = 12
      Size = 2
    end
    object qryTituloFinanceironValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTituloFinanceironValMulta: TBCDField
      FieldName = 'nValMulta'
      Precision = 12
      Size = 2
    end
    object qryTituloFinanceironvalDesconto: TBCDField
      FieldName = 'nvalDesconto'
      Precision = 12
      Size = 2
    end
    object qryTituloFinanceironValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object qryTituloFinanceirodDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryTituloFinanceironValLiq: TBCDField
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryTituloFinanceiroiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryTituloFinanceirocNrUnidade: TStringField
      FieldName = 'cNrUnidade'
      Size = 15
    end
    object qryTituloFinanceirodDtContrato: TDateTimeField
      FieldName = 'dDtContrato'
    end
    object qryTituloFinanceironCdIndiceReajuste: TIntegerField
      FieldName = 'nCdIndiceReajuste'
    end
    object qryTituloFinanceirocNmBlocoEmpImobiliario: TStringField
      FieldName = 'cNmBlocoEmpImobiliario'
      Size = 50
    end
    object qryTituloFinanceirocNmEmpImobiliario: TStringField
      FieldName = 'cNmEmpImobiliario'
      Size = 100
    end
    object qryTituloFinanceirocEndereco: TStringField
      FieldName = 'cEndereco'
      Size = 100
    end
    object qryTituloFinanceirocNmTabTipoParcContratoEmpImobiliario: TStringField
      FieldName = 'cNmTabTipoParcContratoEmpImobiliario'
      Size = 50
    end
    object qryTituloFinanceirocNmIndiceReajuste: TStringField
      FieldName = 'cNmIndiceReajuste'
      Size = 50
    end
    object qryTituloFinanceironCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
  end
  object qryCotacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdIndice'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'iAno'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'Imes'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select * from CotacaoIndiceReajuste'
      'where'
      'nCdIndiceReajuste = :nCdIndice AND'
      'iAnoReferencia =:iAno AND'
      'iMesReferencia =:Imes')
    Left = 632
    Top = 88
    object qryCotacaonCdCotacaoIndiceReajuste: TAutoIncField
      FieldName = 'nCdCotacaoIndiceReajuste'
      ReadOnly = True
    end
    object qryCotacaonCdIndiceReajuste: TIntegerField
      FieldName = 'nCdIndiceReajuste'
    end
    object qryCotacaoiAnoReferencia: TIntegerField
      FieldName = 'iAnoReferencia'
    end
    object qryCotacaoiMesReferencia: TIntegerField
      FieldName = 'iMesReferencia'
    end
    object qryCotacaonIndice: TBCDField
      FieldName = 'nIndice'
      Precision = 12
    end
  end
  object qryJuros: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'select * from tabelajuros')
    Left = 584
    Top = 136
    object qryJurosnCdTabelaJuros: TAutoIncField
      FieldName = 'nCdTabelaJuros'
      ReadOnly = True
    end
    object qryJurosnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryJurosnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryJurosdDtValidade: TDateTimeField
      FieldName = 'dDtValidade'
    end
    object qryJurosiDiasCarencia: TIntegerField
      FieldName = 'iDiasCarencia'
    end
    object qryJurosnPercMulta: TBCDField
      FieldName = 'nPercMulta'
      Precision = 12
      Size = 2
    end
    object qryJurosnPercJuroDia: TBCDField
      FieldName = 'nPercJuroDia'
      Precision = 12
    end
    object qryJurosnPercDescAntecipDia: TBCDField
      FieldName = 'nPercDescAntecipDia'
      Precision = 12
    end
    object qryJurosiDiasMinDescAntecip: TIntegerField
      FieldName = 'iDiasMinDescAntecip'
    end
    object qryJurosnPercMaxDescAntecip: TBCDField
      FieldName = 'nPercMaxDescAntecip'
      Precision = 12
    end
  end
  object qryUltimaCotacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdIndice'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select top 1 *  from cotacaoIndiceReajuste'
      'where nCdIndiceReajuste = :nCdIndice '
      'order by iAnoReferencia desc , iMesReferencia desc'
      ''
      ' ')
    Left = 584
    Top = 184
    object qryUltimaCotacaonCdCotacaoIndiceReajuste: TAutoIncField
      FieldName = 'nCdCotacaoIndiceReajuste'
      ReadOnly = True
    end
    object qryUltimaCotacaonCdIndiceReajuste: TIntegerField
      FieldName = 'nCdIndiceReajuste'
    end
    object qryUltimaCotacaoiAnoReferencia: TIntegerField
      FieldName = 'iAnoReferencia'
    end
    object qryUltimaCotacaoiMesReferencia: TIntegerField
      FieldName = 'iMesReferencia'
    end
    object qryUltimaCotacaonIndice: TBCDField
      FieldName = 'nIndice'
      Precision = 12
    end
  end
  object qryTituloParcelaAnterior: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cNrTit'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 17
        Size = 17
        Value = Null
      end
      item
        Name = 'iParcela'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'select Top 1 titulo.nCdTitulo'
      '            ,titulo.iparcela'
      '            ,titulo.nValCorrecao   '
      
        '            ,parcContratoEmpImobiliario.nCdTabTipoParcContratoEm' +
        'pImobiliario'
      
        '            ,TabTipoParcContratoEmpImobiliario.cNmTabTipoParcCon' +
        'tratoEmpImobiliario'
      'from titulo'
      
        'LEFT  JOIN ContratoEmpImobiliario                     ON Contrat' +
        'oEmpImobiliario.nCdTerceiro                                     ' +
        '= Titulo.nCdTerceiro AND ContratoEmpImobiliario.cNumContrato = T' +
        'itulo.cNrTit '
      
        'LEFT  JOIN ParcContratoEmpImobiliario              ON ParcContra' +
        'toEmpImobiliario.nCdParcContratoEmpImobiliario               = t' +
        'itulo.nCdParcContratoEmpImobiliario AND ParcContratoEmpImobiliar' +
        'io.nCdContratoEmpImobiliario = ContratoEmpImobiliario.nCdContrat' +
        'oEmpImobiliario'
      
        'LEFT  JOIN TabTipoParcContratoEmpImobiliario ON TabTipoParcContr' +
        'atoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario = ParcCon' +
        'tratoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario'
      'where titulo.nValCorrecao > 0 '
      
        '     AND ParcContratoEmpImobiliario.nCdTabTipoParcContratoEmpImo' +
        'biliario = 2'
      '     AND titulo.ncdterceiro =:nCdTerceiro '
      '     AND titulo.cNrTit         =:cNrTit '
      '     AND titulo.iParcela     < :iParcela'
      'order by titulo.iParcela desc')
    Left = 584
    Top = 240
    object qryTituloParcelaAnteriornCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTituloParcelaAnterioriparcela: TIntegerField
      FieldName = 'iparcela'
    end
    object qryTituloParcelaAnteriornValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      Precision = 12
      Size = 2
    end
    object qryTituloParcelaAnteriornCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryTituloParcelaAnteriorcNmTabTipoParcContratoEmpImobiliario: TStringField
      FieldName = 'cNmTabTipoParcContratoEmpImobiliario'
      Size = 50
    end
  end
  object qryParcelasPagas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cNrTit'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 17
        Size = 17
        Value = Null
      end>
    SQL.Strings = (
      'select    titulo.nCdTitulo'
      '            ,titulo.iparcela'
      '            ,titulo.nValIndiceCorrecao'
      '            ,titulo.nValCorrecao   '
      
        '            ,parcContratoEmpImobiliario.nCdTabTipoParcContratoEm' +
        'pImobiliario'
      
        '            ,TabTipoParcContratoEmpImobiliario.cNmTabTipoParcCon' +
        'tratoEmpImobiliario'
      'from titulo'
      
        'LEFT  JOIN ContratoEmpImobiliario                     ON Contrat' +
        'oEmpImobiliario.nCdTerceiro                                     ' +
        '= Titulo.nCdTerceiro AND ContratoEmpImobiliario.cNumContrato = T' +
        'itulo.cNrTit '
      
        'LEFT  JOIN ParcContratoEmpImobiliario              ON ParcContra' +
        'toEmpImobiliario.nCdParcContratoEmpImobiliario               = t' +
        'itulo.nCdParcContratoEmpImobiliario AND ParcContratoEmpImobiliar' +
        'io.nCdContratoEmpImobiliario = ContratoEmpImobiliario.nCdContrat' +
        'oEmpImobiliario'
      
        'LEFT  JOIN TabTipoParcContratoEmpImobiliario ON TabTipoParcContr' +
        'atoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario = ParcCon' +
        'tratoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario'
      'where titulo.nValIndiceCorrecao > 0 AND'
      
        '      ParcContratoEmpImobiliario.nCdTabTipoParcContratoEmpImobil' +
        'iario = 2 AND'
      '      titulo.dDtLiq is not null AND'
      '      titulo.nCdterceiro = :nCdTerceiro AND'
      '      titulo.cNrTit         = :cNrTit '
      'order by titulo.iParcela '
      '')
    Left = 584
    Top = 272
    object qryParcelasPagasnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryParcelasPagasiparcela: TIntegerField
      FieldName = 'iparcela'
    end
    object qryParcelasPagasnValIndiceCorrecao: TBCDField
      FieldName = 'nValIndiceCorrecao'
      Precision = 12
      Size = 8
    end
    object qryParcelasPagasnValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      Precision = 12
      Size = 2
    end
    object qryParcelasPagasnCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryParcelasPagascNmTabTipoParcContratoEmpImobiliario: TStringField
      FieldName = 'cNmTabTipoParcContratoEmpImobiliario'
      Size = 50
    end
  end
  object qryPrimeiraParcela: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cNrTit'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 17
        Size = 17
        Value = Null
      end>
    SQL.Strings = (
      'select Top 1 titulo.nCdTitulo'
      '            ,titulo.iparcela'
      '            ,titulo.nValCorrecao   '
      
        '            ,parcContratoEmpImobiliario.nCdTabTipoParcContratoEm' +
        'pImobiliario'
      
        '            ,TabTipoParcContratoEmpImobiliario.cNmTabTipoParcCon' +
        'tratoEmpImobiliario'
      'from titulo'
      
        'LEFT  JOIN ContratoEmpImobiliario                     ON Contrat' +
        'oEmpImobiliario.nCdTerceiro                                     ' +
        '= Titulo.nCdTerceiro AND ContratoEmpImobiliario.cNumContrato = T' +
        'itulo.cNrTit '
      
        'LEFT  JOIN ParcContratoEmpImobiliario              ON ParcContra' +
        'toEmpImobiliario.nCdParcContratoEmpImobiliario               = t' +
        'itulo.nCdParcContratoEmpImobiliario AND ParcContratoEmpImobiliar' +
        'io.nCdContratoEmpImobiliario = ContratoEmpImobiliario.nCdContrat' +
        'oEmpImobiliario'
      
        'LEFT  JOIN TabTipoParcContratoEmpImobiliario ON TabTipoParcContr' +
        'atoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario = ParcCon' +
        'tratoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario'
      'where titulo.nValCorrecao > 0 '
      
        '     AND ParcContratoEmpImobiliario.nCdTabTipoParcContratoEmpImo' +
        'biliario = 2'
      '     AND titulo.ncdterceiro =:nCdTerceiro '
      '     AND titulo.cNrTit         =:cNrTit '
      'order by titulo.iParcela '
      '')
    Left = 584
    Top = 320
    object qryPrimeiraParcelanCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryPrimeiraParcelaiparcela: TIntegerField
      FieldName = 'iparcela'
    end
    object qryPrimeiraParcelanValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      Precision = 12
      Size = 2
    end
    object qryPrimeiraParcelanCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryPrimeiraParcelacNmTabTipoParcContratoEmpImobiliario: TStringField
      FieldName = 'cNmTabTipoParcContratoEmpImobiliario'
      Size = 50
    end
  end
  object qryUltimaParcelaPaga: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTipoParcela'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cNrTit'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 17
        Size = 17
        Value = Null
      end>
    SQL.Strings = (
      'select   top 1 titulo.nCdTitulo'
      '            ,titulo.iparcela'
      '            ,titulo.nValIndiceCorrecao'
      '            ,titulo.nValCorrecao '
      '            ,titulo.nSaldoTit  '
      '            ,titulo.nValTit'
      '            ,titulo.dDtLiq'
      
        '            ,parcContratoEmpImobiliario.nCdTabTipoParcContratoEm' +
        'pImobiliario'
      
        '            ,TabTipoParcContratoEmpImobiliario.cNmTabTipoParcCon' +
        'tratoEmpImobiliario'
      'from titulo'
      
        'LEFT  JOIN ContratoEmpImobiliario                     ON Contrat' +
        'oEmpImobiliario.nCdTerceiro                                     ' +
        '= Titulo.nCdTerceiro AND ContratoEmpImobiliario.cNumContrato = T' +
        'itulo.cNrTit '
      
        'LEFT  JOIN ParcContratoEmpImobiliario              ON ParcContra' +
        'toEmpImobiliario.nCdParcContratoEmpImobiliario               = t' +
        'itulo.nCdParcContratoEmpImobiliario AND ParcContratoEmpImobiliar' +
        'io.nCdContratoEmpImobiliario = ContratoEmpImobiliario.nCdContrat' +
        'oEmpImobiliario'
      
        'LEFT  JOIN TabTipoParcContratoEmpImobiliario ON TabTipoParcContr' +
        'atoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario = ParcCon' +
        'tratoEmpImobiliario.nCdTabTipoParcContratoEmpImobiliario'
      'where titulo.nValCorrecao > 0 '
      
        '      AND ParcContratoEmpImobiliario.nCdTabTipoParcContratoEmpIm' +
        'obiliario = :nCdTipoParcela '
      '      AND titulo.dDtLiq is not null '
      '      AND titulo.nCdterceiro = :nCdTerceiro '
      '      AND titulo.cNrTit         = :cNrTit '
      'order by titulo.iParcela desc '
      ''
      '')
    Left = 584
    Top = 360
    object qryUltimaParcelaPaganCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryUltimaParcelaPagaiparcela: TIntegerField
      FieldName = 'iparcela'
    end
    object qryUltimaParcelaPaganValIndiceCorrecao: TBCDField
      FieldName = 'nValIndiceCorrecao'
      Precision = 12
      Size = 8
    end
    object qryUltimaParcelaPaganValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      Precision = 12
      Size = 2
    end
    object qryUltimaParcelaPaganCdTabTipoParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdTabTipoParcContratoEmpImobiliario'
    end
    object qryUltimaParcelaPagacNmTabTipoParcContratoEmpImobiliario: TStringField
      FieldName = 'cNmTabTipoParcContratoEmpImobiliario'
      Size = 50
    end
    object qryUltimaParcelaPaganSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryUltimaParcelaPaganValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryUltimaParcelaPagadDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
  end
  object qryCalculaJuros: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTitulo'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtLiquidacao'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nSaldoTit'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdTitulo     int'
      '       ,@cDtLiquidacao varchar(10)'
      '       ,@dDtLiquidacao datetime'
      '       ,@nSaldoTit     decimal(12,2)'
      ''
      'Set @nCdTitulo     = :nCdTitulo'
      'Set @cDtLiquidacao = :dDtLiquidacao'
      'Set @dDtLiquidacao = Convert(DATETIME,@cDtLiquidacao,103)'
      'Set @nSaldoTit     = :nSaldoTit'
      ''
      
        'SELECT IsNull(dbo.fn_SimulaJurosValor(@nCdTitulo,@dDtLiquidacao,' +
        '@nSaldoTit),0)')
    Left = 384
    Top = 120
    object qryCalculaJurosnValJuro: TBCDField
      FieldName = 'COLUMN1'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
  end
  object qryCalculaMulta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTitulo'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nSaldoTit'
        DataType = ftFloat
        Size = 1
        Value = 0.000000000000000000
      end>
    SQL.Strings = (
      'DECLARE @nCdTitulo int'
      '       ,@nSaldoTit decimal(12,2)'
      ''
      'Set @nCdTitulo = :nCdTitulo'
      'Set @nSaldoTit = :nSaldoTit'
      ''
      
        'SELECT IsNull(dbo.fn_SimulaMultaValor(@nCdTitulo,@nSaldoTit),0) ' +
        'as nValMulta'
      '')
    Left = 384
    Top = 168
    object qryCalculaMultanValMulta: TBCDField
      FieldName = 'nValMulta'
      ReadOnly = True
      Precision = 12
      Size = 2
    end
  end
end
