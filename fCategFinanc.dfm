inherited frmCategFinanc: TfrmCategFinanc
  Caption = 'frmCategFinanc'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 56
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 45
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 8
    Top = 94
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Categoria'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 10
    Top = 118
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Receita/Despesa'
    FocusControl = DBEdit5
  end
  object Label5: TLabel [5]
    Left = 31
    Top = 142
    Width = 63
    Height = 13
    Alignment = taRightJustify
    Caption = 'Plano Conta'
    FocusControl = DBEdit6
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 99
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdCategFinanc'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [8]
    Left = 99
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmCategFinanc'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [9]
    Left = 99
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdGrupoCategFinanc'
    DataSource = dsMaster
    TabOrder = 3
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [10]
    Tag = 1
    Left = 171
    Top = 88
    Width = 305
    Height = 19
    DataField = 'cNmGrupoCategFinanc'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [11]
    Left = 99
    Top = 112
    Width = 25
    Height = 19
    DataField = 'cFlgRecDes'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [12]
    Left = 99
    Top = 136
    Width = 65
    Height = 19
    DataField = 'nCdPlanoConta'
    DataSource = dsMaster
    TabOrder = 6
    OnKeyDown = DBEdit6KeyDown
  end
  object DBEdit7: TDBEdit [13]
    Tag = 1
    Left = 171
    Top = 136
    Width = 305
    Height = 19
    DataField = 'cNmPlanoConta'
    DataSource = dsMaster
    TabOrder = 7
  end
  object cxPageControl1: TcxPageControl [14]
    Left = 8
    Top = 176
    Width = 761
    Height = 337
    ActivePage = cxTabSheet2
    LookAndFeel.NativeStyle = True
    TabOrder = 8
    ClientRectBottom = 333
    ClientRectLeft = 4
    ClientRectRight = 757
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Centro de Custo x Categoria'
      ImageIndex = 0
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 753
        Height = 309
        Align = alClient
        DataSource = dsCentroCusto
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh2KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdCategFinanc'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdCC'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cCdCC'
            Footers = <>
            Width = 141
          end
          item
            EditButtons = <>
            FieldName = 'cNmCC'
            Footers = <>
            Width = 484
          end>
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Usu'#225'rio x Categoria'
      ImageIndex = 1
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 753
        Height = 309
        Align = alClient
        DataSource = dsUsuarioCategFinanc
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdCategFinanc'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Width = 572
          end>
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CATEGFINANC'
      'WHERE nCdCategFinanc = :nPK')
    Top = 224
    object qryMasternCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryMastercNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object qryMasternCdGrupoCategFinanc: TIntegerField
      FieldName = 'nCdGrupoCategFinanc'
    end
    object qryMastercFlgRecDes: TStringField
      FieldName = 'cFlgRecDes'
      FixedChar = True
      Size = 1
    end
    object qryMastercNmGrupoCategFinanc: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmGrupoCategFinanc'
      LookupDataSet = qryGrupoCategFinanc
      LookupKeyFields = 'nCdGrupoCategFinanc'
      LookupResultField = 'cNmGrupoCategFinanc'
      KeyFields = 'nCdGrupoCategFinanc'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMasternCdPlanoConta: TIntegerField
      FieldName = 'nCdPlanoConta'
    end
    object qryMastercNmPlanoConta: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmPlanoConta'
      LookupDataSet = qryContas
      LookupKeyFields = 'nCdPlanoConta'
      LookupResultField = 'cNmPlanoConta'
      KeyFields = 'nCdPlanoConta'
      LookupCache = True
      Size = 50
      Lookup = True
    end
  end
  inherited dsMaster: TDataSource
    Top = 224
  end
  inherited qryID: TADOQuery
    Top = 224
  end
  inherited usp_ProximoID: TADOStoredProc
    Top = 280
  end
  inherited qryStat: TADOQuery
    Top = 296
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Top = 336
  end
  inherited ImageList1: TImageList
    Top = 320
  end
  object qryGrupoCategFinanc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM GRUPOCATEGFINANC')
    Left = 696
    Top = 328
    object qryGrupoCategFinancnCdGrupoCategFinanc: TIntegerField
      FieldName = 'nCdGrupoCategFinanc'
    end
    object qryGrupoCategFinanccNmGrupoCategFinanc: TStringField
      FieldName = 'cNmGrupoCategFinanc'
      Size = 50
    end
  end
  object dsUsuarioCategFinanc: TDataSource
    DataSet = qryUsuarioCategFinanc
    Left = 392
    Top = 424
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario, cNmUsuario'
      'FROM USUARIO'
      'WHERE nCdUsuario = :nPK')
    Left = 360
    Top = 424
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object qryCentroCusto: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCentroCustoBeforePost
    Parameters = <
      item
        Name = 'nCdCategFinanc'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CentroCustoCategFinanc Categ'
      'WHERE Categ.nCdCategFinanc = :nCdCategFinanc'
      '')
    Left = 392
    Top = 392
    object qryCentroCustonCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryCentroCustonCdCC: TIntegerField
      DisplayLabel = 'Centro de Custo|C'#243'd'
      FieldName = 'nCdCC'
    end
    object qryCentroCustocCdCC: TStringField
      DisplayLabel = 'Centro de Custo|M'#225'scara'
      FieldKind = fkLookup
      FieldName = 'cCdCC'
      LookupDataSet = qryCC
      LookupKeyFields = 'nCdCC'
      LookupResultField = 'cCdCC'
      KeyFields = 'nCdCC'
      LookupCache = True
      ReadOnly = True
      Size = 8
      Lookup = True
    end
    object qryCentroCustocNmCC: TStringField
      DisplayLabel = 'Centro de Custo|Nome Centro de Custo'
      FieldKind = fkLookup
      FieldName = 'cNmCC'
      LookupDataSet = qryCC
      LookupKeyFields = 'nCdCC'
      LookupResultField = 'cNmCC'
      KeyFields = 'nCdCC'
      LookupCache = True
      ReadOnly = True
      Size = 50
      Lookup = True
    end
  end
  object dsCentroCusto: TDataSource
    DataSet = qryCentroCusto
    Left = 424
    Top = 392
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdCC'
      ',cCdCC'
      ',cNmCC'
      'FROM CentroCusto'
      'WHERE nCdStatus = 1'
      'AND iNivel = 3'
      'AND cFlgLanc = 1')
    Left = 360
    Top = 392
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object qryContas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM PlanoConta')
    Left = 800
    Top = 192
    object qryContasnCdPlanoConta: TAutoIncField
      FieldName = 'nCdPlanoConta'
      ReadOnly = True
    end
    object qryContascNmPlanoConta: TStringField
      FieldName = 'cNmPlanoConta'
      Size = 50
    end
    object qryContasnCdGrupoPlanoConta: TIntegerField
      FieldName = 'nCdGrupoPlanoConta'
    end
    object qryContascFlgRecDes: TStringField
      FieldName = 'cFlgRecDes'
      FixedChar = True
      Size = 1
    end
  end
  object qryUsuarioCategFinanc: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioCategFinancBeforePost
    OnCalcFields = qryUsuarioCategFinancCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM UsuarioCategFinanc'
      'WHERE nCdCategFinanc = :nPK')
    Left = 440
    Top = 424
    object qryUsuarioCategFinancnCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryUsuarioCategFinancnCdUsuario: TIntegerField
      DisplayLabel = 'Usu'#225'rios|C'#243'd'
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioCategFinanccNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rios|Nome Usu'#225'rio'
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 50
      Calculated = True
    end
    object qryUsuarioCategFinancnCdUsuarioCategFinanc: TIntegerField
      FieldName = 'nCdUsuarioCategFinanc'
    end
  end
end
