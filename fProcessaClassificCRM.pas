unit fProcessaClassificCRM;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, StdCtrls, ADODB, Mask, DBCtrls, ER2Lookup,
  cxImageComboBox, cxLookAndFeelPainters, cxButtons, cxCheckBox,
  cxProgressBar, cxTrackBar, cxPC, Menus;

type
  TfrmProcessaClassificCRM = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label5: TLabel;
    cmdPreparaTemp: TADOCommand;
    qryResulCRM: TADOQuery;
    qryClassificCRM: TADOQuery;
    dsResultCRM: TDataSource;
    dsClassificCRM: TDataSource;
    SP_CLASSIFICA_TERCEIRO_CRM: TADOStoredProc;
    qryResulCRMnCdTerceiro: TIntegerField;
    qryResulCRMnCdTipoClassificCRM: TIntegerField;
    qryResulCRMnPontos: TIntegerField;
    qryResulCRMnPercVarFormapPagto: TBCDField;
    qryResulCRMnValTotalCompra: TBCDField;
    C: TBCDField;
    qryResulCRMnValDinheiro: TBCDField;
    qryResulCRMnValDinheiroCRM: TBCDField;
    qryResulCRMnValCartaoCredito: TBCDField;
    qryResulCRMnValCartaoCreditoCRM: TBCDField;
    qryResulCRMnValCartaoDebito: TBCDField;
    qryResulCRMnValCartaoDebitoCRM: TBCDField;
    qryResulCRMnValCrediario: TBCDField;
    qryResulCRMnValCrediarioCRM: TBCDField;
    qryResulCRMnValCheque: TBCDField;
    qryResulCRMnValChequeCRM: TBCDField;
    qryResulCRMcFlgDin: TIntegerField;
    qryResulCRMcFlgCarCred: TIntegerField;
    qryResulCRMcFlgCarDeb: TIntegerField;
    qryResulCRMcFlgCrediario: TIntegerField;
    qryResulCRMcFlgCheque: TIntegerField;
    qryClassificCRMcNmClassificCRM: TStringField;
    ER2LookupMaskEdit1: TER2LookupMaskEdit;
    qryTerceiro: TADOQuery;
    dsTerceiro: TDataSource;
    qryTerceirocNmTerceiro: TStringField;
    Label1: TLabel;
    ER2LookupMaskEdit2: TER2LookupMaskEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    qryResulCRMcNmTerceiro: TStringField;
    qryResulCRMcFlgAprovadoClassific: TStringField;
    qryResulCRMcApelidoTipoClassific: TStringField;
    ToolButton5: TToolButton;
    SP_GERA_CLASSIFIC_CRM: TADOStoredProc;
    qryClassificCRMnCdClassificCRM: TIntegerField;
    qryResulCRMcFlgSelecionado: TIntegerField;
    qryAtualizaSelect: TADOQuery;
    qryResulCRMcNmTipoClassificCRM: TStringField;
    qryHistClassificCRM: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxGrid5: TcxGrid;
    cxGrid5DBTableView1: TcxGridDBTableView;
    cxGrid5DBTableView1cFlgSelecionado: TcxGridDBColumn;
    cxGrid5DBTableView1DBColumn2: TcxGridDBColumn;
    cxGrid5DBTableView1nPontos: TcxGridDBColumn;
    cxGrid5DBTableView1nCdTipoClassificCRM: TcxGridDBColumn;
    cxGrid5DBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGrid5DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid5DBTableView1nValDinheiro: TcxGridDBColumn;
    cxGrid5DBTableView1nValDinheiroCRM: TcxGridDBColumn;
    cxGrid5DBTableView1nValCrediario: TcxGridDBColumn;
    cxGrid5DBTableView1nValCrediarioCRM: TcxGridDBColumn;
    cxGrid5DBTableView1nValCartaoCredito: TcxGridDBColumn;
    cxGrid5DBTableView1nValCartaoCreditoCRM: TcxGridDBColumn;
    cxGrid5DBTableView1nValCartaoDebito: TcxGridDBColumn;
    cxGrid5DBTableView1nValCartaoDebitoCRM: TcxGridDBColumn;
    cxGrid5DBTableView1nValCheque: TcxGridDBColumn;
    cxGrid5DBTableView1nValChequeCRM: TcxGridDBColumn;
    cxGrid5DBTableView1nValTotalCompra: TcxGridDBColumn;
    cxGrid5DBTableView1nValMediaCompraCRM: TcxGridDBColumn;
    cxGrid5Level1: TcxGridLevel;
    panel: TPanel;
    cxButton1: TcxButton;
    cxButton3: TcxButton;
    cxTabSheet2: TcxTabSheet;
    qryAux: TADOQuery;
    cxButton2: TcxButton;
    dsHistClassificCRM: TDataSource;
    cxGrid1: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    Panel1: TPanel;
    qryHistClassificCRMnCdHistClassificCRM: TIntegerField;
    qryHistClassificCRMnCdTerceiro: TIntegerField;
    qryHistClassificCRMcNmTerceiro: TStringField;
    qryHistClassificCRMnCdTipoClassificCRM: TIntegerField;
    qryHistClassificCRMnValDinheiro: TBCDField;
    qryHistClassificCRMnValCartaoDebito: TBCDField;
    qryHistClassificCRMnValCartaoCredito: TBCDField;
    qryHistClassificCRMnValCheque: TBCDField;
    qryHistClassificCRMnValCrediario: TBCDField;
    qryHistClassificCRMnPontos: TIntegerField;
    cxGridDBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView1nCdTipoClassificCRM: TcxGridDBColumn;
    cxGridDBTableView1nValDinheiro: TcxGridDBColumn;
    cxGridDBTableView1nValCartaoDebito: TcxGridDBColumn;
    cxGridDBTableView1nValCartaoCredito: TcxGridDBColumn;
    cxGridDBTableView1nValCheque: TcxGridDBColumn;
    cxGridDBTableView1nValCrediario: TcxGridDBColumn;
    cxGridDBTableView1nPontos: TcxGridDBColumn;
    cxButton7: TcxButton;
    cxButton8: TcxButton;
    PopupMenu1: TPopupMenu;
    CarregarPendentes1: TMenuItem;
    cxButton4: TcxButton;
    qryHistClassificCRMnCdTipoClassific: TIntegerField;
    cxGridDBTableView1nCdTipoClassific: TcxGridDBColumn;
    Excluircliente1: TMenuItem;
    qryCountClassific: TADOQuery;
    qryCountClassificnCdTipoClassificCRM: TIntegerField;
    qryCountClassificnQtde: TIntegerField;
    SP_APROVA_CLASSIFICACAO: TADOStoredProc;
    cxTabSheet3: TcxTabSheet;
    cxGrid2: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    qryClassificAprovados: TADOQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    IntegerField3: TIntegerField;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    BCDField3: TBCDField;
    BCDField4: TBCDField;
    BCDField5: TBCDField;
    IntegerField4: TIntegerField;
    IntegerField5: TIntegerField;
    dsClassificAprovados: TDataSource;
    PopupMenu2: TPopupMenu;
    Atualizar1: TMenuItem;
    Panel2: TPanel;
    lblOuro: TLabel;
    Image2: TImage;
    Image3: TImage;
    lblPrata: TLabel;
    Image4: TImage;
    lblBronze: TLabel;
    Label2: TLabel;
    lbl5: TLabel;
    lblTotal: TLabel;
    Panel3: TPanel;
    lblOuroPendAprov: TLabel;
    Image5: TImage;
    Image6: TImage;
    lblPrataPendAprov: TLabel;
    Image7: TImage;
    lblBronzePendAprov: TLabel;
    lblPendAprov: TLabel;
    lblTotalPendAprovTotal: TLabel;
    lblTotalPendAprov: TLabel;
    Panel4: TPanel;
    lblOuroAnalise: TLabel;
    Image8: TImage;
    Image9: TImage;
    lblPrataAnalise: TLabel;
    Image10: TImage;
    lblBronzeAnalise: TLabel;
    lblAnalise: TLabel;
    lblTotalAnaliseTotal: TLabel;
    lblTotalAnalise: TLabel;
    qryCountPendAprov: TADOQuery;
    qryCountPendAprovnCdTipoClassificCRM: TIntegerField;
    qryCountPendAprovnQtde: TIntegerField;
    qryCountPendAnalise: TADOQuery;
    qryCountPendAnalisenCdTipoClassificCRM: TIntegerField;
    qryCountPendAnalisenQtde: TIntegerField;
    procedure AtualizaSelecao(cAcao : integer);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton7Click(Sender: TObject);
    procedure CarregarPendentes1Click(Sender: TObject);
    procedure ER2LookupMaskEdit1Exit(Sender: TObject);
    procedure cxButton8Click(Sender: TObject);
    procedure Excluircliente1Click(Sender: TObject);
    procedure Atualizar1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProcessaClassificCRM: TfrmProcessaClassificCRM;

implementation

{$R *.dfm}

uses
   fMenu, Math;

procedure TfrmProcessaClassificCRM.AtualizaSelecao(cAcao : integer);
begin

  if qryResulCRM.IsEmpty then
      exit;

  try
      qryAtualizaSelect.Close;
      qryAtualizaSelect.Parameters.ParamByName('cAcao').Value := cAcao;
      qryAtualizaSelect.ExecSQL;

      qryResulCRM.Close;
      qryResulCRM.Open;
      
  except
      MensagemErro('Erro no processamento');
      raise;
  end;

end;

procedure TfrmProcessaClassificCRM.cxButton1Click(Sender: TObject);
begin
  AtualizaSelecao(1);//Marcar todos
end;

procedure TfrmProcessaClassificCRM.cxButton3Click(Sender: TObject);
begin
  AtualizaSelecao(0);//desmarcar todos
end;

procedure TfrmProcessaClassificCRM.cxButton2Click(Sender: TObject);
begin

  if qryResulCRM.IsEmpty then
     exit;

  qryAux.Close;
  qryAux.SQL.Add('SELECT 1 FROM ClassificCRM WHERE nCdClassificCRM = ' + qryClassificCRMnCdClassificCRM.AsString + ' AND nCdTabStatusClassificCRM = 4');
  qryAux.Open;

  if not qryAux.IsEmpty then
  begin
      MensagemAlerta('Classifica��o cancelada anteriormente.');
      Abort;
  end;

  qryAux.Close;
  qryAux.SQL.Add('SELECT 1 FROM #TempClassificTerceiro WHERE cFlgSelecionado = 1');
  qryAux.Open;

  if qryAux.IsEmpty then
  begin
      MensagemAlerta('Nenhum cliente selecionado.');
      Abort;
  end;

  case MessageDlg('Confirma a efetiva��o?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      frmMenu.Connection.BeginTrans;
      
      SP_GERA_CLASSIFIC_CRM.Close;
      SP_GERA_CLASSIFIC_CRM.Parameters.ParamByName('@nCdClassificCRM').Value := qryClassificCRMnCdClassificCRM.Value;
      SP_GERA_CLASSIFIC_CRM.Parameters.ParamByName('@nCdUsuario').Value      := frmMenu.nCdUsuarioLogado;
      SP_GERA_CLASSIFIC_CRM.ExecProc;

      qryResulCRM.Close;
      qryResulCRM.Open;

      frmMenu.Connection.CommitTrans;
  except
      MensagemErro('Erro no processamento.');
      raise;
  end;

end;

procedure TfrmProcessaClassificCRM.cxButton7Click(Sender: TObject);
begin

  if Trim(DBEdit2.Text) = '' then
  begin
      MensagemAlerta('Classifica��o n�o informada.');
      ER2LookupMaskEdit1.SetFocus;
      Abort;
  end;
  
  case MessageDlg('Este processo pode demorar alguns minutos para processar. ' + chr(13) + chr(13) + 'Confirma a analise para classifica��o ' + Trim(ER2LookupMaskEdit1.Text) + '?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      cmdPreparaTemp.Execute;

      SP_CLASSIFICA_TERCEIRO_CRM.Close;
      SP_CLASSIFICA_TERCEIRO_CRM.Parameters.ParamByName('@nCdClassificCRM').Value := ER2LookupMaskEdit1.Text;

      if Trim(ER2LookupMaskEdit2.Text) = '' then
          SP_CLASSIFICA_TERCEIRO_CRM.Parameters.ParamByName('@nCdTerceiroExterno').Value := 0
      else
          SP_CLASSIFICA_TERCEIRO_CRM.Parameters.ParamByName('@nCdTerceiroExterno').Value := ER2LookupMaskEdit2.Text;

      SP_CLASSIFICA_TERCEIRO_CRM.ExecProc;

      qryResulCRM.Close;
      qryResulCRM.Open;
  except
      MensagemErro('Erro no processamento.');
      raise;
  end;

  lblOuroAnalise.Caption   := '0';
  lblPrataAnalise.Caption  := '0';
  lblBronzeAnalise.Caption := '0';
  lblTotalAnalise.Caption  := '0';

  qryCountPendAprov.Close;
  qryCountPendAprov.Parameters.ParamByName('nPK').Value := qryClassificCRMnCdClassificCRM.Value;
  qryCountPendAprov.Open;

  if not qryCountPendAprov.IsEmpty then
  begin
     qryCountPendAprov.First;

     while not qryCountPendAprov.Eof do
     begin
         case qryCountPendAprovnCdTipoClassificCRM.Value of
            1: lblOuro.Caption   := qryCountPendAprovnQtde.AsString;
            2: lblPrata.Caption  := qryCountPendAprovnQtde.AsString;
            3: lblBronze.Caption := qryCountPendAprovnQtde.AsString;
         end;

         lblTotalAnalise.Caption := IntToStr(StrToInt(lblTotalAnalise.Caption) + qryCountPendAprovnQtde.Value); 

         qryCountPendAprov.Next;
     end;
     
  end;

end;

procedure TfrmProcessaClassificCRM.CarregarPendentes1Click(
  Sender: TObject);
begin

  if qryClassificCRM.IsEmpty  then
      exit;

  qryHistClassificCRM.Close;
  qryHistClassificCRM.Parameters.ParamByName('nPK').Value := qryClassificCRMnCdClassificCRM.Value;
  qryHistClassificCRM.Open;

  lblOuroPendAprov.Caption   := '0';
  lblPrataPendAprov.Caption  := '0';
  lblBronzePendAprov.Caption := '0';
  lblTotalPendAprov.Caption  := '0';

  qryCountPendAprov.Close;
  qryCountPendAprov.Parameters.ParamByName('nPK').Value := qryClassificCRMnCdClassificCRM.Value;
  qryCountPendAprov.Open;

  if not qryCountPendAprov.IsEmpty then
  begin
     qryCountPendAprov.First;

     while not qryCountPendAprov.Eof do
     begin
         case qryCountPendAprovnCdTipoClassificCRM.Value of
            1: lblOuro.Caption   := qryCountPendAprovnQtde.AsString;
            2: lblPrata.Caption  := qryCountPendAprovnQtde.AsString;
            3: lblBronze.Caption := qryCountPendAprovnQtde.AsString;
         end;

         lblTotalPendAprov.Caption := IntToStr(StrToInt(lblTotalPendAprov.Caption) + qryCountPendAprovnQtde.Value);

         qryCountPendAprov.Next;
     end;
     
  end;
  
end;

procedure TfrmProcessaClassificCRM.ER2LookupMaskEdit1Exit(Sender: TObject);
begin

  if (ER2LookupMaskEdit1.Text = '') or (qryClassificCRM.IsEmpty) then
  begin
      lblOuro.Caption   := '0';
      lblPrata.Caption  := '0';
      lblBronze.Caption := '0';
      lblTotal.Caption  := '0';

      lblOuroPendAprov.Caption   := '0';
      lblPrataPendAprov.Caption  := '0';
      lblBronzePendAprov.Caption := '0';
      lblTotalPendAprov.Caption  := '0';

      lblOuroAnalise.Caption   := '0';
      lblPrataAnalise.Caption  := '0';
      lblBronzeAnalise.Caption := '0';
      lblTotalAnalise.Caption  := '0';

      qryResulCRM.Close;
      qryHistClassificCRM.Close;
      qryClassificAprovados.Close;
      abort;
  end;

end;

procedure TfrmProcessaClassificCRM.cxButton8Click(Sender: TObject);
begin
  if qryHistClassificCRM.IsEmpty then
      exit;

  qryAux.Close;
  qryAux.SQL.Add('SELECT 1 FROM ClassificCRM WHERE nCdTabStatusClassificCRM = 3 AND nCdClassificCRM <> ' + qryClassificCRMnCdClassificCRM.AsString);
  qryAux.Open;

  if not qryAux.IsEmpty then
  begin
      MensagemAlerta('Existe uma classifica��o em vig�ncia, n�o � possivel ativar.');
      Abort;
  end;

  case MessageDlg('Confirma a aprova��o da classifica��o ' + qryClassificCRMnCdClassificCRM.AsString + '?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      frmMenu.Connection.BeginTrans;

      SP_APROVA_CLASSIFICACAO.Close;
      SP_APROVA_CLASSIFICACAO.Parameters.ParamByName('@nCdClassificCRM').Value := qryClassificCRMnCdClassificCRM.Value;
      SP_APROVA_CLASSIFICACAO.Parameters.ParamByName('@nCdUsuario').Value      := frmMenu.nCdUsuarioLogado;
      SP_APROVA_CLASSIFICACAO.ExecProc;

      frmMenu.Connection.CommitTrans;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      raise;
  end;

  qryHistClassificCRM.Close;
  qryHistClassificCRM.Parameters.ParamByName('nPK').Value := qryClassificCRMnCdClassificCRM.Value;
  qryHistClassificCRM.Open;


end;

procedure TfrmProcessaClassificCRM.Excluircliente1Click(Sender: TObject);
begin
  if (qryHistClassificCRM.Active = False) or (qryHistClassificCRM.IsEmpty = True)then
      exit;

  case MessageDlg('Confirma exclus�o do cliente da classifica��o CRM?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  try
      qryAux.Close;
      qryAux.SQL.Add('DELETE FROM HistClassificCRM WHERE nCdHistClassificCRM = ' + qryHistClassificCRMnCdHistClassificCRM.AsString);
      qryAux.ExecSQL;
  except
      MensagemErro('Erro no processamento.');
      raise;
  end;
end;

procedure TfrmProcessaClassificCRM.Atualizar1Click(Sender: TObject);
begin
  if qryClassificCRM.IsEmpty  then
      exit;

  qryClassificAprovados.Close;
  qryClassificAprovados.Parameters.ParamByName('nPK').Value := qryClassificCRMnCdClassificCRM.Value;
  qryClassificAprovados.Open;

  lblOuro.Caption   := '0';
  lblPrata.Caption  := '0';
  lblBronze.Caption := '0';
  lblTotal.Caption  := '0';

  qryCountClassific.Close;
  qryCountClassific.Parameters.ParamByName('nPK').Value := qryClassificCRMnCdClassificCRM.Value;
  qryCountClassific.Open;

  if not qryCountClassific.IsEmpty then
  begin
     qryCountClassific.First;

     while not qryCountClassific.Eof do
     begin
         case qryCountClassificnCdTipoClassificCRM.Value of
            1: lblOuro.Caption   := qryCountClassificnQtde.AsString;
            2: lblPrata.Caption  := qryCountClassificnQtde.AsString;
            3: lblBronze.Caption := qryCountClassificnQtde.AsString;
         end;

         lblTotal.Caption := IntToStr(StrToInt(lblTotal.Caption) + qryCountClassificnQtde.Value); 

         qryCountClassific.Next;
     end;
     
  end;
end;

procedure TfrmProcessaClassificCRM.FormCreate(Sender: TObject);
begin
  inherited;

  lblOuro.Font.Size   := 10;
  lblPrata.Font.Size  := 10;
  lblBronze.Font.Size := 10;
  lblTotal.Font.Size  := 10;
  lbl5.Font.Size      := 10;
  label2.Font.Size    := 10;

  lblOuroAnalise.Font.Size       := 10;
  lblPrataAnalise.Font.Size      := 10;
  lblBronzeAnalise.Font.Size     := 10;
  lblTotalAnalise.Font.Size      := 10;
  lblTotalAnaliseTotal.Font.Size := 10;
  lblAnalise.Font.Size           := 10;

  lblOuroPendAprov.Font.Size       := 10;
  lblPrataPendAprov.Font.Size      := 10;
  lblBronzePendAprov.Font.Size     := 10;
  lblTotalPendAprov.Font.Size      := 10;
  lblTotalPendAprovTotal.Font.Size := 10;
  lblPendAprov.Font.Size           := 10;







end;

initialization
    RegisterClass(TfrmProcessaClassificCRM);
end.
