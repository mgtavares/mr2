unit fConsultaLanctoCofre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBCtrls, StdCtrls, Mask, DBGridEhGrouping,
  ToolCtrlsEh, ER2Excel;

type
  TfrmConsultaLanctoCofre = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    MaskEdit2: TMaskEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit3: TMaskEdit;
    qryContaBancariaDeb: TADOQuery;
    qryContaBancariaDebnCdContaBancaria: TIntegerField;
    qryContaBancariaDebnCdConta: TStringField;
    qryResultado: TADOQuery;
    dsResultado: TDataSource;
    DataSource1: TDataSource;
    DBGridEh1: TDBGridEh;
    MaskEdit4: TMaskEdit;
    Label4: TLabel;
    MaskEdit5: TMaskEdit;
    Label5: TLabel;
    Label6: TLabel;
    qryTipoLancto: TADOQuery;
    qryTipoLanctonCdTipoLancto: TIntegerField;
    qryTipoLanctocNmTipoLancto: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    qryResultadonCdLanctoFin: TAutoIncField;
    qryResultadodDtLancto: TDateTimeField;
    qryResultadonCdTipoLancto: TIntegerField;
    qryResultadocNmTipoLancto: TStringField;
    qryResultadonValCredito: TBCDField;
    qryResultadonValDebito: TBCDField;
    qryResultadonCdUsuario: TIntegerField;
    qryResultadocNmUsuario: TStringField;
    qryResultadocFlgManual: TStringField;
    qryResultadodDtEstorno: TDateTimeField;
    qryResultadocMotivoEstorno: TStringField;
    qryResultadocHistorico: TStringField;
    qryResultadocDocumento: TStringField;
    qryResultadonCdProvisaoTit: TIntegerField;
    qryContaBancariaDebcFlgCaixa: TIntegerField;
    qryContaBancariaDebcFlgCofre: TIntegerField;
    DBEdit1: TDBEdit;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryResultadoiNrCheque: TIntegerField;
    ToolButton6: TToolButton;
    ER2Excel1: TER2Excel;
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaLanctoCofre: TfrmConsultaLanctoCofre;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmConsultaLanctoCofre.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryTipoLancto.Close ;
  PosicionaQuery(qryTipoLancto, MaskEdit4.Text) ;
  
end;

procedure TfrmConsultaLanctoCofre.MaskEdit3Exit(Sender: TObject);
begin
  inherited;

  qryContaBancariaDeb.Close ;
  qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryContaBancariaDeb.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryContaBancariaDeb, MaskEdit3.Text) ;

end;

procedure TfrmConsultaLanctoCofre.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione a conta.') ;
      MaskEdit3.SetFocus ;
      exit ;
  end ;

  if (trim(MaskEdit1.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data inicial.') ;
      MaskEdit1.SetFocus ;
      exit ;
  end ;

  if (trim(MaskEdit2.Text) = '/  /') then
  begin
      MensagemAlerta('Informe a data final.') ;
      MaskEdit2.SetFocus ;
      exit ;
  end ;

  if (qryContaBancariaDebcFlgCaixa.Value = 1) or (qryContaBancariaDebcFlgCofre.Value = 1) then
  begin
      RadioGroup1.ItemIndex := 1 ;
  end ;

  qryResultado.Close ;
  qryResultado.Parameters.ParamByName('nCdContaBancaria').Value := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  qryResultado.Parameters.ParamByName('cDataInicial').Value     := frmMenu.ConvData(MaskEdit1.Text)    ;
  qryResultado.Parameters.ParamByName('cDataFinal').Value       := frmMenu.ConvData(MaskEdit2.Text)    ;
  qryResultado.Parameters.ParamByName('nCdTipoLancto').Value    := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  qryResultado.Parameters.ParamByName('iNrCheque').Value        := frmMenu.ConvInteiro(MaskEdit5.Text) ;

  if (RadioGroup1.ItemIndex = 1) then
      qryResultado.Parameters.ParamByName('cFlgConciliado').Value   := 'S'
  else qryResultado.Parameters.ParamByName('cFlgConciliado').Value   := 'N' ;

  if (RadioGroup2.ItemIndex = 0) then
      qryResultado.Parameters.ParamByName('cFlgManual').Value       := 'S'
  else qryResultado.Parameters.ParamByName('cFlgManual').Value       := 'N' ;

  qryResultado.Open;

  if (qryResultado.eof) then
  begin
      MensagemAlerta('Nenhum lan�amento encontrado para o crit�rio selecionado.') ;
  end ;

end;

procedure TfrmConsultaLanctoCofre.FormShow(Sender: TObject);
begin
  inherited;

  DBGridEh1.Columns[4].Font.Color := clBlue ;
  DBGridEh1.Columns[5].Font.Color := clRed ;

end;

procedure TfrmConsultaLanctoCofre.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            {nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = @@Empresa AND ((ContaBancaria.nCdLoja IS NULL) OR EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdUsuario = @@Usuario AND UL.nCdLoja = ContaBancaria.nCdLoja))');}
            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'cFlgCofre = 1 AND EXISTS(SELECT 1 FROM UsuarioContaBancaria UCB WHERE UCB.nCdContaBancaria = ContaBancaria.nCdContaBancaria AND UCB.nCdUsuario = '+ intToStr(frmMenu.nCdUsuarioLogado)+') AND ContaBancaria.nCdEmpresa = @@Empresa AND ((@@Loja = 0) OR (@@Loja = ContaBancaria.nCdLoja)) AND (cFlgCofre = 1 OR cFlgCaixa = 0)') ;

            If (nPK > 0) then
            begin
                MaskEdit3.Text := IntToStr(nPK) ;
            end ;

    end ;

  end ;

end;

procedure TfrmConsultaLanctoCofre.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

            nPK := frmLookup_Padrao.ExecutaConsulta(130);

            If (nPK > 0) then
            begin
                MaskEdit4.Text := IntToStr(nPK) ;
            end ;

    end ;

  end ;

end;

procedure TfrmConsultaLanctoCofre.ToolButton5Click(Sender: TObject);
begin
  inherited;

  frmMenu.ImprimeDBGrid(DBGridEh1,'Consulta de Lan�amentos');
end;

procedure TfrmConsultaLanctoCofre.ToolButton6Click(Sender: TObject);
var
  cNmFonte : string ;
  iLinha   : integer ;
begin
  if (qryResultado.State = dsInactive) then
  begin
      MensagemAlerta('Por favor informe os par�metros e realize a consulta antes de Exportar os dados') ;
      MaskEdit3.SetFocus ;
      exit ;
  end;

  inherited;

  frmMenu.mensagemUsuario('Exportando Planilha...');

  cNmFonte := 'Calibri' ;

  ER2Excel1.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
  ER2Excel1.Celula['A2'].Text := 'Loja - Consulta de Lan�amentos do Cofre' ;

  ER2Excel1.Celula['A1'].FontName   := cNmFonte;
  ER2Excel1.Celula['A1'].FontSize   := 10 ;

  ER2Excel1.Celula['A2'].FontName   := cNmFonte;
  ER2Excel1.Celula['A2'].FontSize   := 10 ;

  ER2Excel1.Celula['A5'].Mesclar('C5');

  ER2Excel1.Celula['A5'].Text := 'Lancto' ;
  ER2Excel1.Celula['B5'].Text := 'Data Lancto' ;
  ER2Excel1.Celula['C5'].Text := 'Hist�rico' ;
  ER2Excel1.Celula['D5'].Text := 'Valor Cr�dito' ;
  ER2Excel1.Celula['E5'].Text := 'Valor D�bito' ;
  ER2Excel1.Celula['F5'].Text := 'N�m. Cheque' ;
  ER2Excel1.Celula['G5'].Text := 'Documento' ;
  ER2Excel1.Celula['H5'].Text := 'Manual' ;

  {ER2Excel1.Celula['A6'].Congelar('Q6');}

  ER2Excel1.Celula['A4'].Background := RGB(216,216,216);
  ER2Excel1.Celula['A4'].FontName   := cNmFonte;
  ER2Excel1.Celula['A4'].FontSize   := 9 ;

  ER2Excel1.Celula['A5'].Background := RGB(216,216,216);
  ER2Excel1.Celula['A5'].FontName   := cNmFonte;
  ER2Excel1.Celula['A5'].FontSize   := 9 ;

  ER2Excel1.Celula['A6'].Background := RGB(216,216,216);
  ER2Excel1.Celula['A6'].FontName   := cNmFonte;
  ER2Excel1.Celula['A6'].FontSize   := 9 ;
  ER2Excel1.Celula['A6'].Border     := [EdgeBottom];

  ER2Excel1.Celula['A4'].Range('H4');

  ER2Excel1.Celula['A5'].Range('H5');

  ER2Excel1.Celula['A6'].Range('H6');

  ER2Excel1.Celula['D5'].HorizontalAlign := haRight ;
  ER2Excel1.Celula['E5'].HorizontalAlign := haRight ;


  iLinha := 7 ; {-- Linha Inicial --}

  qryResultado.DisableControls;
  qryResultado.First;

  while not qryResultado.Eof do
  begin

      ER2Excel1.Celula['A' + IntToStr(iLinha)].Text := qryResultadonCdLanctoFin.Value ;
      ER2Excel1.Celula['B' + IntToStr(iLinha)].Text := qryResultadodDtLancto.Value;
      ER2Excel1.Celula['C' + IntToStr(iLinha)].Text := qryResultadocNmTipoLancto.Value;
      ER2Excel1.Celula['D' + IntToStr(iLinha)].Text := qryResultadonValCredito.Value;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].Text := qryResultadonValDebito.Value;
      ER2Excel1.Celula['F' + IntToStr(iLinha)].Text := qryResultadoiNrCheque.Value;
      ER2Excel1.Celula['G' + IntToStr(iLinha)].Text := qryResultadocDocumento.Value;
      ER2Excel1.Celula['H' + IntToStr(iLinha)].Text := qryResultadocFlgManual.Value;


      ER2Excel1.Celula['D' + IntToStr(iLinha)].Mascara := '#.##0,00' ;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].Mascara := '#.##0,00' ;

      ER2Excel1.Celula['D' + IntToStr(iLinha)].HorizontalAlign := haRight ;
      ER2Excel1.Celula['E' + IntToStr(iLinha)].HorizontalAlign := haRight ;

      inc( iLinha ) ;

      qryResultado.Next;
  end ;

  qryResultado.First;
  qryResultado.EnableControls;

  ER2Excel1.Celula['A6'].Width := 7;
  ER2Excel1.Celula['B6'].Width := 15;
  ER2Excel1.Celula['C6'].Width := 40;
  ER2Excel1.Celula['D6'].Width := 14;
  ER2Excel1.Celula['E6'].Width := 14;
  ER2Excel1.Celula['F6'].Width := 17;
  ER2Excel1.Celula['G6'].Width := 30;
  ER2Excel1.Celula['H6'].Width := 7;

  ER2Excel1.Celula['A4'].Height := 5;
  ER2Excel1.Celula['A5'].Height := 10;
  ER2Excel1.Celula['A6'].Height := 5;

  { -- exporta planilha e limpa result do componente -- }
  ER2Excel1.ExportXLS;
  ER2Excel1.CleanupInstance;

  frmMenu.mensagemUsuario('');


end;

initialization
    RegisterClass(TfrmConsultaLanctoCofre) ;

end.
