unit fVinculoProdutoAla;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh;

type
  TfrmVinculoProdutoAla = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryProdutos: TADOQuery;
    dsProdutos: TDataSource;
    qryProdutosnCdProduto: TIntegerField;
    qryProdutoscNmProduto: TStringField;
    qryProdutosnCdAlaProducao: TIntegerField;
    qryProdutoscNmAlaProducao: TStringField;
    qryAlaProducao: TADOQuery;
    qryAlaProducaocNmAlaProducao: TStringField;
    procedure qryProdutosCalcFields(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmVinculoProdutoAla: TfrmVinculoProdutoAla;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmVinculoProdutoAla.qryProdutosCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryAlaProducao.Close ;
  PosicionaQuery(qryAlaProducao, qryProdutosnCdAlaProducao.asString) ;

  if not qryAlaProducao.Eof then
      qryProdutoscNmAlaProducao.Value := qryAlaProducaocNmAlaProducao.Value ;

end;

procedure TfrmVinculoProdutoAla.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
      vk_F4: begin
      
        if (qryProdutos.State = dsBrowse) then
             qryProdutos.Edit ;

        if (qryProdutos.State = dsInsert) or (qryProdutos.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(140);

            If (nPK > 0) then
            begin
                qryProdutosnCdAlaProducao.Value := nPK ;

            end ;

        end ;

      end ;

  end ;

end;

procedure TfrmVinculoProdutoAla.FormShow(Sender: TObject);
begin
  inherited;

  qryProdutos.Close ;
  qryProdutos.Open ;
end;

initialization
    RegisterClass(TfrmVinculoProdutoAla) ;
    
end.
