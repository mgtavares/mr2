unit fCriterioSolicitacaoCD;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, cxPC,
  cxControls, ER2Lookup, StdCtrls, Mask, DBCtrls;

type
  TfrmCriterioSolicitacaoCD = class(TfrmCadastro_Padrao)
    ProdutoCriterioSolicitacaoCD: TADOQuery;
    dsProdutoCriterioSolicitacaoCD: TDataSource;
    dsProdutoCD: TDataSource;
    ProdutoCriterioSolicitacaoCDnCdProdutoCriterioSolicitacaoCD: TIntegerField;
    ProdutoCriterioSolicitacaoCDnCdCriterioSolicitacaoCD: TIntegerField;
    ProdutoCriterioSolicitacaoCDnCdDepartamento: TIntegerField;
    ProdutoCriterioSolicitacaoCDnCdMarca: TIntegerField;
    ProdutoCriterioSolicitacaoCDnQtdeMinEstoque: TIntegerField;
    ProdutoCriterioSolicitacaoCDnQtdeMinSolicitar: TIntegerField;
    qryDepartamento: TADOQuery;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryUsuario: TADOQuery;
    qryMasternCdCriterioSolicitacaoCD: TIntegerField;
    qryMastercNmCriterioSolicitacaoCD: TStringField;
    qryMasterdDtCadastro: TDateTimeField;
    qryMasternCdUsuarioCadastro: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    Label3: TLabel;
    edtStatus: TER2LookupDBEdit;
    dsStat: TDataSource;
    DBEdit3: TDBEdit;
    pageSolicitacao: TcxPageControl;
    tabDeptoMarca: TcxTabSheet;
    tabProduto: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    gridProduto: TDBGridEh;
    ProdutoCriterioSolicitacaoCDcNmDepartamento: TStringField;
    ProdutoCriterioSolicitacaoCDcNmMarca: TStringField;
    ProdutoCriterioSolicitacaoCDcFlgIgnorarEstNegativo: TIntegerField;
    qryAux: TADOQuery;
    qryProdutoCD: TADOQuery;
    qryProdutoCDnCdProdutoCriterioSolicitacaoCD: TIntegerField;
    qryProdutoCDnCdCriterioSolicitacaoCD: TIntegerField;
    qryProdutoCDnCdProduto: TIntegerField;
    qryProdutoCDnQtdeMinEstoque: TIntegerField;
    qryProdutoCDnQtdeMinSolicitar: TIntegerField;
    qryProdutoCDcFlgIgnorarEstNegativo: TIntegerField;
    qryProdutoCDcNmProduto: TStringField;
    qrySubCategoria: TADOQuery;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySegmento: TADOQuery;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentocNmSegmento: TStringField;
    qryCategoria: TADOQuery;
    qryCategorianCdCategoria: TAutoIncField;
    qryCategoriacNmCategoria: TStringField;
    ProdutoCriterioSolicitacaoCDnCdCategoria: TIntegerField;
    ProdutoCriterioSolicitacaoCDnCdSubCategoria: TIntegerField;
    ProdutoCriterioSolicitacaoCDnCdSegmento: TIntegerField;
    ProdutoCriterioSolicitacaoCDcNmCategoria: TStringField;
    ProdutoCriterioSolicitacaoCDcNmSubCategoria: TStringField;
    ProdutoCriterioSolicitacaoCDcNmSegmento: TStringField;
    procedure DBGridEh1Enter(Sender: TObject);
    procedure gridProdutoEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ProdutoCriterioSolicitacaoCDBeforePost(DataSet: TDataSet);
    procedure qryProdutoCDBeforePost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure gridProdutoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryProdutoCDCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterEdit(DataSet: TDataSet);
    procedure ProdutoCriterioSolicitacaoCDCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCriterioSolicitacaoCD: TfrmCriterioSolicitacaoCD;

implementation

uses
    fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmCriterioSolicitacaoCD.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;
end;

procedure TfrmCriterioSolicitacaoCD.gridProdutoEnter(Sender: TObject);
begin
  inherited;
  
  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;
end;

procedure TfrmCriterioSolicitacaoCD.FormCreate(Sender: TObject);
begin
  inherited;

  nCdTabelaSistema  := 539;
  nCdConsultaPadrao := 783;
  cNmTabelaMaster   := 'CRITERIOSOLICITACAOCD';
end;

procedure TfrmCriterioSolicitacaoCD.ProdutoCriterioSolicitacaoCDBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  {if ((ProdutoCriterioSolicitacaoCDcNmDepartamento.IsNull) and (ProdutoCriterioSolicitacaoCDcNmMarca.IsNull)) then
  begin
      MensagemAlerta('Informe um departamento ou uma marca.');

      gridDeptoMarca.Col := 1;

      Abort;
  end;}

  if (ProdutoCriterioSolicitacaoCDnQtdeMinEstoque.Value < 0) then
  begin
      MensagemAlerta('A quantidade m�nima em estoque deve ser maior ou igual a 0.');
      DBGridEh1.SelectedField := ProdutoCriterioSolicitacaoCDnQtdeMinEstoque;
      DBGridEh1.SetFocus;
      Abort;
  end;

  if (ProdutoCriterioSolicitacaoCDnQtdeMinSolicitar.Value < 1) then
  begin
      MensagemAlerta('A quantidade m�nima � solicitar deve ser maior igual a 1.');
      DBGridEh1.SelectedField := ProdutoCriterioSolicitacaoCDnQtdeMinSolicitar;
      DBGridEh1.SetFocus;
      Abort;
  end;

  if (ProdutoCriterioSolicitacaoCDnQtdeMinEstoque.IsNull) then
      ProdutoCriterioSolicitacaoCDnQtdeMinEstoque.Value := 0;

  if (ProdutoCriterioSolicitacaoCD.State = dsInsert) then
  begin

      { -- verifica duplicidade de marcas -- }
      {if (ProdutoCriterioSolicitacaoCDnCdMarca.IsNull = False) and (ProdutoCriterioSolicitacaoCDnCdDepartamento.IsNull = True) then
      begin
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('SELECT nCdProdutoCriterioSolicitacaoCD');
          qryAux.SQL.Add('  FROM ProdutoCriterioSolicitacaoCD');
          qryAux.SQL.Add(' WHERE nCdCriterioSolicitacaoCD        = ' + qryMasternCdCriterioSolicitacaoCD.AsString);
          qryAux.SQL.Add('   AND nCdProdutoCriterioSolicitacaoCD  IS NOT NULL');
          qryAux.SQL.Add('   AND nCdMarca                        = ' + ProdutoCriterioSolicitacaoCDnCdMarca.AsString);
          qryAux.SQL.Add('   AND nCdDepartamento                 IS NULL');
          qryAux.Open;

          if not (qryAux.IsEmpty) then
          begin
              MensagemAlerta('A marca informada j� consta nesse crit�rio de solicita��o.');
              DBGridEh1.SelectedField := ProdutoCriterioSolicitacaoCDnCdMarca;
              DBGridEh1.SetFocus;
              Abort;
          end;
      end;}

      { -- verifica duplicidade de departamentos -- }
      {if (ProdutoCriterioSolicitacaoCDnCdMarca.IsNull = True) and (ProdutoCriterioSolicitacaoCDnCdDepartamento.IsNull = False) then
      begin
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('SELECT nCdProdutoCriterioSolicitacaoCD');
          qryAux.SQL.Add('  FROM ProdutoCriterioSolicitacaoCD');
          qryAux.SQL.Add(' WHERE nCdCriterioSolicitacaoCD         = ' + qryMasternCdCriterioSolicitacaoCD.AsString);
          qryAux.SQL.Add('   AND nCdProdutoCriterioSolicitacaoCD  IS NOT NULL');
          qryAux.SQL.Add('   AND nCdDepartamento                  = ' + ProdutoCriterioSolicitacaoCDnCdDepartamento.AsString);
          qryAux.SQL.Add('   AND nCdMarca                         IS NULL');
          qryAux.Open;

          if not (qryAux.IsEmpty) then
          begin
              MensagemAlerta('O departamento informado j� consta nesse crit�rio de solicita��o.');
              DBGridEh1.SelectedField := ProdutoCriterioSolicitacaoCDnCdDepartamento;
              DBGridEh1.SetFocus;
              Abort;
          end;
      end;}

      // Valida duplicidade de Departamento x Marca
      {if (ProdutoCriterioSolicitacaoCDnCdMarca.IsNull = False) and (ProdutoCriterioSolicitacaoCDnCdDepartamento.IsNull = False) then
      begin
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('SELECT nCdProdutoCriterioSolicitacaoCD');
          qryAux.SQL.Add('  FROM ProdutoCriterioSolicitacaoCD');
          qryAux.SQL.Add(' WHERE nCdCriterioSolicitacaoCD         = ' + qryMasternCdCriterioSolicitacaoCD.AsString);
          qryAux.SQL.Add('   AND nCdProdutoCriterioSolicitacaoCD  IS NOT NULL');
          qryAux.SQL.Add('   AND nCdDepartamento                  = ' + ProdutoCriterioSolicitacaoCDnCdDepartamento.AsString);
          qryAux.SQL.Add('   AND nCdMarca                         = ' + ProdutoCriterioSolicitacaoCDnCdMarca.AsString);
          qryAux.Open;

          if not (qryAux.IsEmpty) then
          begin
              MensagemAlerta('O departamento e marca j� constam nesse crit�rio de solicita��o.');

              gridDeptoMarca.Col := 1;

              Abort;
          end;
      end;}

      ProdutoCriterioSolicitacaoCDnCdProdutoCriterioSolicitacaoCD.Value := frmMenu.fnProximoCodigo('PRODUTOCRITERIOSOLICITACAOCD');
      ProdutoCriterioSolicitacaoCDnCdCriterioSolicitacaoCD.Value        := qryMasternCdCriterioSolicitacaoCD.Value;
  end;
end;

procedure TfrmCriterioSolicitacaoCD.qryProdutoCDBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryProdutoCDcNmProduto.IsNull) then
  begin
      MensagemAlerta('Informe o produto.');

      Abort;
  end;

  if (qryProdutoCDnQtdeMinEstoque.Value < 0) then
  begin
      MensagemAlerta('A quantidade m�nima em estoque maior ou igual a 0.');

      Abort;
  end;

  if (qryProdutoCDnQtdeMinSolicitar.Value < 1) then
  begin
      MensagemAlerta('A quantidade m�nima � solicitar deve ser maior igual a 1.');

      Abort;
  end;

  if (qryProdutoCD.State = dsInsert) then
  begin
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1');
      qryAux.SQL.Add('  FROM ProdutoCriterioSolicitacaoCD');
      qryAux.SQL.Add(' WHERE nCdCriterioSolicitacaoCD = ' + qryMasternCdCriterioSolicitacaoCD.AsString);
      qryAux.SQL.Add('   AND nCdProduto               = ' + qryProdutoCDnCdProduto.AsString);
      qryAux.Open;

      if not (qryAux.IsEmpty) then
      begin
          MensagemAlerta('O produto j� consta no crit�rio de solicita��o.');

          Abort;
      end;
  end;

  if (qryProdutoCDnQtdeMinEstoque.IsNull) then
      qryProdutoCDnQtdeMinEstoque.Value := 0;

  if (qryProdutoCD.State = dsInsert) then
  begin
      qryProdutoCDnCdProdutoCriterioSolicitacaoCD.Value := frmMenu.fnProximoCodigo('PRODUTOCRITERIOSOLICITACAOCD');
      qryProdutoCDnCdCriterioSolicitacaoCD.Value        := qryMasternCdCriterioSolicitacaoCD.Value;
  end;
end;

procedure TfrmCriterioSolicitacaoCD.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (Trim(qryMastercNmCriterioSolicitacaoCD.Value) = '') then
  begin
      MensagemAlerta('Informe a descri��o do crit�rio de solicita��o.');

      DBEdit2.SetFocus;

      Abort;
  end;

  if (qryStat.IsEmpty) then
  begin
      MensagemAlerta('Informe o status do crit�rio de solicita��o.');

      edtStatus.SetFocus;

      Abort;
  end;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasterdDtCadastro.Value        := Now();
      qryMasternCdUsuarioCadastro.Value := frmMenu.nCdUsuarioLogado;
  end;

  inherited;
end;

procedure TfrmCriterioSolicitacaoCD.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryStat.Close;
  ProdutoCriterioSolicitacaoCD.Close;
  qryProdutoCD.Close;
end;

procedure TfrmCriterioSolicitacaoCD.qryMasterAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryStat.Close;
  ProdutoCriterioSolicitacaoCD.Close;
  qryProdutoCD.Close;

  PosicionaQuery(qryStat, qryMasternCdStatus.AsString);
  PosicionaQuery(ProdutoCriterioSolicitacaoCD, qryMasternCdCriterioSolicitacaoCD.AsString);
  PosicionaQuery(qryProdutoCD, qryMasternCdCriterioSolicitacaoCD.AsString);
end;

procedure TfrmCriterioSolicitacaoCD.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (Key = VK_F4) then
  begin
      if (ProdutoCriterioSolicitacaoCD.State = dsBrowse) then
          ProdutoCriterioSolicitacaoCD.Edit;

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdDepartamento') then
      begin
          nPK := frmLookup_Padrao.ExecutaConsulta(45);

          if (nPK > 0) then
              ProdutoCriterioSolicitacaoCDnCdDepartamento.Value := nPK;
      end;

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdCategoria') then
      begin
          if (ProdutoCriterioSolicitacaoCDcNmDepartamento.Value = '') then
          begin
              MensagemAlerta('Informe o departamento.');
              DBGridEh1.SelectedField := ProdutoCriterioSolicitacaoCDnCdDepartamento;
              DBGridEh1.SetFocus;
              Abort;
          end;

          nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + ProdutoCriterioSolicitacaoCDnCdDepartamento.AsString);

          if (nPK > 0) then
          begin
              ProdutoCriterioSolicitacaoCDnCdCategoria.Value               := nPK;
              qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := ProdutoCriterioSolicitacaoCDnCdDepartamento.Value;

              PosicionaQuery(qryCategoria, ProdutoCriterioSolicitacaoCDnCdCategoria.AsString);

              if (not qryCategoria.IsEmpty) then
                  ProdutoCriterioSolicitacaoCDcNmCategoria.Value := qryCategoriacNmCategoria.Value;
          end;
      end;

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdSubCategoria') then
      begin
          if (qryCategoria.IsEmpty) then
          begin
              MensagemAlerta('Informe a categoria.');
              DBGridEh1.SelectedField := ProdutoCriterioSolicitacaoCDnCdCategoria;
              DBGridEh1.SetFocus;
              Abort;
          end;

          nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + ProdutoCriterioSolicitacaoCDnCdCategoria.AsString);

          if (nPK > 0) then
          begin
              ProdutoCriterioSolicitacaoCDnCdSubCategoria.Value            := nPK;
              qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := ProdutoCriterioSolicitacaoCDnCdCategoria.Value;

              PosicionaQuery(qrySubCategoria, ProdutoCriterioSolicitacaoCDnCdSubCategoria.AsString);

              if (not qrySubCategoria.IsEmpty) then
                  ProdutoCriterioSolicitacaoCDcNmSubCategoria.Value := qrySubCategoriacNmSubCategoria.Value;
          end;
      end;

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdSegmento') then
      begin
          if (qrySubCategoria.IsEmpty) then
          begin
              MensagemAlerta('Informe a sub categoria.');
              DBGridEh1.SelectedField := ProdutoCriterioSolicitacaoCDnCdSubCategoria;
              DBGridEh1.SetFocus;
              Abort;
          end;

          nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + ProdutoCriterioSolicitacaoCDnCdSubCategoria.AsString);

          if (nPK > 0) then
          begin
              ProdutoCriterioSolicitacaoCDnCdSegmento.Value               := nPK;
              qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := ProdutoCriterioSolicitacaoCDnCdSubCategoria.Value;

              PosicionaQuery(qrySegmento, ProdutoCriterioSolicitacaoCDnCdSegmento.AsString);

              if (not qrySegmento.IsEmpty) then
                  ProdutoCriterioSolicitacaoCDcNmSegmento.Value := qrySegmentocNmSegmento.Value;
          end;
      end;

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nCdMarca') then
      begin
          nPK := frmLookup_Padrao.ExecutaConsulta(49);

          if (nPK > 0) then
              ProdutoCriterioSolicitacaoCDnCdMarca.Value := nPK;
      end;
  end;

      {case (gridDeptoMarca.Col) of

      1: begin
             if (ProdutoCriterioSolicitacaoCD.State = dsBrowse) then
                 ProdutoCriterioSolicitacaoCD.Edit ;

             nPK := frmLookup_Padrao.ExecutaConsulta(45);

             if (nPK > 0) then
                 ProdutoCriterioSolicitacaoCDnCdDepartamento.Value := nPK ;
         end;

      3: begin
             if (ProdutoCriterioSolicitacaoCD.State = dsBrowse) then
                 ProdutoCriterioSolicitacaoCD.Edit ;

             nPK := frmLookup_Padrao.ExecutaConsulta(49);

             if (nPK > 0) then
                 ProdutoCriterioSolicitacaoCDnCdMarca.Value := nPK ;

         end;

      end;}
end;

procedure TfrmCriterioSolicitacaoCD.gridProdutoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : integer;
begin
  inherited;

  if (Key = VK_F4) then

      if (gridProduto.Col = 1) then
      begin
          if (qryProdutoCD.State = dsBrowse) then
              qryProdutoCD.Edit ;

          nPK := frmLookup_Padrao.ExecutaConsulta(76);

          if (nPK > 0) then
              qryProdutoCDnCdProduto.Value := nPK ;
      end;
end;

procedure TfrmCriterioSolicitacaoCD.qryProdutoCDCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryProduto.Close;
  PosicionaQuery(qryProduto, qryProdutoCDnCdProduto.AsString);

  if not (qryProduto.IsEmpty) then
      qryProdutoCDcNmProduto.Value := qryProdutocNmProduto.Value;

end;

procedure TfrmCriterioSolicitacaoCD.qryMasterAfterEdit(DataSet: TDataSet);
begin
  inherited;

  qryStat.Close;
  ProdutoCriterioSolicitacaoCD.Close;
  qryProdutoCD.Close;

  PosicionaQuery(qryStat, qryMasternCdStatus.AsString);
  PosicionaQuery(ProdutoCriterioSolicitacaoCD, qryMasternCdCriterioSolicitacaoCD.AsString);
  PosicionaQuery(qryProdutoCD, qryMasternCdCriterioSolicitacaoCD.AsString);

end;

procedure TfrmCriterioSolicitacaoCD.ProdutoCriterioSolicitacaoCDCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (ProdutoCriterioSolicitacaoCDnCdCategoria.Value > 0) then
  begin
      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := ProdutoCriterioSolicitacaoCDnCdDepartamento.Value;

      PosicionaQuery(qryCategoria, ProdutoCriterioSolicitacaoCDnCdCategoria.AsString);

      if (not qryCategoria.IsEmpty) then
          ProdutoCriterioSolicitacaoCDcNmCategoria.Value := qryCategoriacNmCategoria.Value;
  end;

  if (ProdutoCriterioSolicitacaoCDnCdSubCategoria.Value > 0) then
  begin
      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := ProdutoCriterioSolicitacaoCDnCdCategoria.Value;

      PosicionaQuery(qrySubCategoria, ProdutoCriterioSolicitacaoCDnCdSubCategoria.AsString);

      if (not qrySubCategoria.IsEmpty) then
          ProdutoCriterioSolicitacaoCDcNmSubCategoria.Value := qrySubCategoriacNmSubCategoria.Value;
  end;

  if (ProdutoCriterioSolicitacaoCDnCdSegmento.Value > 0) then
  begin
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := ProdutoCriterioSolicitacaoCDnCdSubCategoria.Value;

      PosicionaQuery(qrySegmento, ProdutoCriterioSolicitacaoCDnCdSegmento.AsString);

      if (not qrySegmento.IsEmpty) then
          ProdutoCriterioSolicitacaoCDcNmSegmento.Value := qrySegmentocNmSegmento.Value;
  end;
end;

initialization
    RegisterClass(TfrmCriterioSolicitacaoCD);

end.

