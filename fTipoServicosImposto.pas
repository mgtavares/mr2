unit fTipoServicosImposto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmTipoServicoImposto = class(TfrmCadastro_Padrao)
    qryMasternCdTipoServicoImposto: TIntegerField;
    qryMasteriNroTipoServicoImposto: TIntegerField;
    qryMastercNmTipoServicoImposto: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    procedure btIncluirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoServicoImposto: TfrmTipoServicoImposto;

implementation

{$R *.dfm}

procedure TfrmTipoServicoImposto.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit3.SetFocus;
end;

procedure TfrmTipoServicoImposto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOSERVICOIMPOSTO' ;
  nCdTabelaSistema  := 506 ;
  nCdConsultaPadrao := 1013 ;

end;

procedure TfrmTipoServicoImposto.qryMasterBeforePost(DataSet: TDataSet);
begin
  if DBEdit2.Text = '' then
  begin
      MensagemAlerta('Informe a descri��o.') ;
      DBEdit2.SetFocus ;
      abort ;
  end;

  if DBEdit3.Text = '' then
  begin
      MensagemAlerta('Informe a n�mero.') ;
      DBEdit3.SetFocus ;
      abort ;
  end;

  inherited;

end;

initialization
    RegisterClass(TfrmTipoServicoImposto);

end.
