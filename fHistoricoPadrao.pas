unit fHistoricoPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, DBGridEhGrouping, ToolCtrlsEh,
  GridsEh, DBGridEh, cxPC, cxControls;

type
  TfrmHistoricoPadrao = class(TfrmCadastro_Padrao)
    qryMasternCdHistoricoPadrao: TIntegerField;
    qryMastercNmHistoricoPadrao: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmHistoricoPadrao: TfrmHistoricoPadrao;

implementation

uses
  fMenu;

{$R *.dfm}

procedure TfrmHistoricoPadrao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'HISTORICOPADRAO' ;
  nCdTabelaSistema  := 221 ;
  nCdConsultaPadrao := 224 ;

end;

procedure TfrmHistoricoPadrao.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
  
end;

procedure TfrmHistoricoPadrao.qryMasterBeforePost(DataSet: TDataSet);
begin

  if ( trim( DBEdit2.Text ) = '' ) then
  begin
      MensagemAlerta('Informe a descri��o.') ;
      DBEdit2.SetFocus;
      abort;
  end ;

  inherited;

end;

initialization
    RegisterClass( TfrmHistoricoPadrao ) ;
    
end.
