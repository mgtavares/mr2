inherited frmCartaCorrecaoNFe_HistoricoCCe: TfrmCartaCorrecaoNFe_HistoricoCCe
  Left = 288
  Top = 110
  Caption = 'Hist'#243'rico CC-e'
  OldCreateOrder = True
  Position = poDesktopCenter
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 433
    ActivePage = Tab1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 429
    ClientRectLeft = 4
    ClientRectRight = 780
    ClientRectTop = 24
    object Tab1: TcxTabSheet
      Caption = 'Hist'#243'rico de Corre'#231#245'es'
      ImageIndex = 0
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 776
        Height = 405
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView1: TcxGridDBTableView
          DataController.DataSource = dsCartaCorrecaoNFe
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#'
              Kind = skCount
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView1nCdCartaCorrecaoNFe: TcxGridDBColumn
            Caption = 'C'#243'd.'
            DataBinding.FieldName = 'nCdCartaCorrecaoNFe'
            Width = 65
          end
          object cxGridDBTableView1dDtEmissao: TcxGridDBColumn
            Caption = 'Dt. Emiss'#227'o'
            DataBinding.FieldName = 'dDtEmissao'
            Width = 130
          end
          object cxGridDBTableView1cCorrecao: TcxGridDBColumn
            Caption = 'Corre'#231#227'o'
            DataBinding.FieldName = 'cCorrecao'
            Width = 430
          end
          object cxGridDBTableView1iSeq: TcxGridDBColumn
            Caption = 'Sequ'#234'ncia'
            DataBinding.FieldName = 'iSeq'
            Width = 80
          end
          object cxGridDBTableView1iLote: TcxGridDBColumn
            Caption = 'Lote'
            DataBinding.FieldName = 'iLote'
            Width = 80
          end
          object cxGridDBTableView1cChaveNFe: TcxGridDBColumn
            Caption = 'Chave NF-e'
            DataBinding.FieldName = 'cChaveNFe'
            Width = 250
          end
          object cxGridDBTableView1cNrProtocoloNFe: TcxGridDBColumn
            Caption = 'Protocolo NF-e'
            DataBinding.FieldName = 'cNrProtocoloCCe'
            Width = 250
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 16
    Top = 112
  end
  object qryCartaCorrecaoNFe: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM CartaCorrecaoNFe'
      ' WHERE nCdDoctoFiscal = :nPK'
      ' ORDER BY dDtEmissao DESC')
    Left = 48
    Top = 112
    object qryCartaCorrecaoNFenCdCartaCorrecaoNFe: TIntegerField
      FieldName = 'nCdCartaCorrecaoNFe'
    end
    object qryCartaCorrecaoNFenCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryCartaCorrecaoNFedDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryCartaCorrecaoNFecCorrecao: TStringField
      FieldName = 'cCorrecao'
      Size = 1000
    end
    object qryCartaCorrecaoNFeiSeq: TIntegerField
      FieldName = 'iSeq'
    end
    object qryCartaCorrecaoNFeiLote: TIntegerField
      FieldName = 'iLote'
    end
    object qryCartaCorrecaoNFecChaveNFe: TStringField
      FieldName = 'cChaveNFe'
      Size = 200
    end
    object qryCartaCorrecaoNFecCaminhoXML: TStringField
      FieldName = 'cCaminhoXML'
      Size = 200
    end
    object qryCartaCorrecaoNFecNrProtocoloCCe: TStringField
      FieldName = 'cNrProtocoloCCe'
      Size = 100
    end
    object qryCartaCorrecaoNFecArquivoXML: TStringField
      FieldName = 'cArquivoXML'
      Size = 200
    end
    object qryCartaCorrecaoNFecXMLCCe: TMemoField
      FieldName = 'cXMLCCe'
      BlobType = ftMemo
    end
    object qryCartaCorrecaoNFenCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryCartaCorrecaoNFedDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
  end
  object dsCartaCorrecaoNFe: TDataSource
    AutoEdit = False
    DataSet = qryCartaCorrecaoNFe
    Left = 48
    Top = 144
  end
end
