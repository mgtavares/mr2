inherited frmTitulo_DetalheJuros: TfrmTitulo_DetalheJuros
  Width = 936
  BorderIcons = [biSystemMenu]
  Caption = 'Hist'#243'rico C'#225'lculo Juros'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 920
  end
  inherited ToolBar1: TToolBar
    Width = 920
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 920
    Height = 435
    Align = alClient
    DataSource = dsTabelaJuro
    Flat = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTituloJuroDia'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'dDtCalculoJuro'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtVencTit'
        Footers = <>
        Width = 120
      end
      item
        EditButtons = <>
        FieldName = 'nValBaseCalculo'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nPercJuroDia'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValJuro'
        Footers = <>
        Width = 121
      end
      item
        EditButtons = <>
        FieldName = 'nValJuroAcumulado'
        Footers = <>
        Width = 121
      end
      item
        EditButtons = <>
        FieldName = 'nPercMulta'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValMulta'
        Footers = <>
        Width = 121
      end>
  end
  inherited ImageList1: TImageList
    Left = 400
    Top = 152
  end
  object qryTabelaJuro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM TituloJuroDia '
      'WHERE nCdTitulo = :nPK'
      'ORDER BY dDtCalculoJuro DESC')
    Left = 296
    Top = 168
    object qryTabelaJuronCdTituloJuroDia: TAutoIncField
      FieldName = 'nCdTituloJuroDia'
      ReadOnly = True
    end
    object qryTabelaJuronCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTabelaJurodDtCalculoJuro: TDateTimeField
      DisplayLabel = 'Hist'#243'rico C'#225'lculo Juros|Dt. C'#225'lculo'
      FieldName = 'dDtCalculoJuro'
    end
    object qryTabelaJurodDtVencTit: TDateTimeField
      DisplayLabel = 'Hist'#243'rico C'#225'lculo Juros|Dt. Vencto.'
      FieldName = 'dDtVencTit'
    end
    object qryTabelaJuronValBaseCalculo: TBCDField
      DisplayLabel = 'Hist'#243'rico C'#225'lculo Juros|Valor Base'
      FieldName = 'nValBaseCalculo'
      Precision = 12
      Size = 2
    end
    object qryTabelaJuronPercJuroDia: TBCDField
      DisplayLabel = 'Hist'#243'rico C'#225'lculo Juros|% Juro/dia'
      FieldName = 'nPercJuroDia'
      Precision = 12
    end
    object qryTabelaJuronValJuro: TBCDField
      DisplayLabel = 'Hist'#243'rico C'#225'lculo Juros|Valor Juro Dia'
      FieldName = 'nValJuro'
      Precision = 12
    end
    object qryTabelaJuronValJuroAcumulado: TBCDField
      DisplayLabel = 'Hist'#243'rico C'#225'lculo Juros|Valor Juro Acum.'
      FieldName = 'nValJuroAcumulado'
      Precision = 12
      Size = 2
    end
    object qryTabelaJuronPercMulta: TBCDField
      DisplayLabel = 'Hist'#243'rico C'#225'lculo Juros|% Multa'
      FieldName = 'nPercMulta'
      Precision = 12
    end
    object qryTabelaJuronValMulta: TBCDField
      DisplayLabel = 'Hist'#243'rico C'#225'lculo Juros|Valor Multa'
      FieldName = 'nValMulta'
      Precision = 12
      Size = 2
    end
  end
  object dsTabelaJuro: TDataSource
    DataSet = qryTabelaJuro
    Left = 352
    Top = 160
  end
end
