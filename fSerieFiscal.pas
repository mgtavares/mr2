unit fSerieFiscal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, ImgList, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping, ToolCtrlsEh, GridsEh,
  DBGridEh, cxPC, cxControls, cxLookAndFeelPainters, cxButtons;

type
  TfrmSerieFiscal = class(TfrmCadastro_Padrao)
    qryMasternCdSerieFiscal: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdTipoDoctoFiscal: TIntegerField;
    qryMastercModelo: TStringField;
    qryMasteriUltimoNumero: TIntegerField;
    qryTipoDoctoFiscal: TADOQuery;
    qryTipoDoctoFiscalnCdTipoDoctoFiscal: TIntegerField;
    qryTipoDoctoFiscalcNmTipoDoctoFiscal: TStringField;
    qryMastercNmTipoDoctoFiscal: TStringField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryMastercNmEmpresa: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    qryMasternCdModImpDF: TIntegerField;
    qryModImpDF: TADOQuery;
    qryModImpDFnCdModImpDF: TIntegerField;
    qryModImpDFcNmModImpDF: TStringField;
    qryMastercNmModImpDF: TStringField;
    Label7: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    qryMastercSerie: TStringField;
    DBEdit5: TDBEdit;
    qryVerificaSerieFiscal: TADOQuery;
    qrySerieFiscalLoja: TADOQuery;
    qrySerieFiscalLojanCdSerieFiscalLoja: TIntegerField;
    qrySerieFiscalLojanCdLoja: TIntegerField;
    dsSerieFiscalLoja: TDataSource;
    qrySerieFiscalLojanCdSerieFiscal: TIntegerField;
    qrySerieFiscalLojacNmLoja: TStringField;
    cxPageControl1: TcxPageControl;
    tabLoja: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryVerificaSerieFiscalLoja: TADOQuery;
    qryVerificaSerieFiscalLojanCdSerieFiscal: TIntegerField;
    qryMastercNmSerieFiscal: TStringField;
    DBEdit11: TDBEdit;
    Label8: TLabel;
    tabNumDocFiscal: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryNumDoctoFiscal: TADOQuery;
    qryNumDoctoFiscalnCdSerieFiscal: TIntegerField;
    qryNumDoctoFiscalnCdDoctoFiscal: TIntegerField;
    qryNumDoctoFiscaliUltimoNumero: TIntegerField;
    qryNumDoctoFiscaldDtInclusao: TDateTimeField;
    dsNumDoctoFiscal: TDataSource;
    qryNumDoctoFiscalcTextoCFOP: TStringField;
    tabNumInutil: TcxTabSheet;
    qrySerieFiscalInutil: TADOQuery;
    dsSerieFiscalInutil: TDataSource;
    DBGridEh3: TDBGridEh;
    qrySerieFiscalInutilnCdSerieFiscal: TIntegerField;
    qrySerieFiscalInutiliNrInutil: TIntegerField;
    qrySerieFiscalInutilnCdUsuarioCad: TIntegerField;
    qrySerieFiscalInutildDtCad: TDateTimeField;
    qrySerieFiscalInutilcMotivoInutil: TStringField;
    qrySerieFiscalInutilnCdServidorOrigem: TIntegerField;
    qrySerieFiscalInutildDtReplicacao: TDateTimeField;
    qrySerieFiscalInutilcNmUsuario: TStringField;
    Panel1: TPanel;
    btnVisualizarGrade: TcxButton;
    qryVerificaInutilizacao: TADOQuery;
    qryVerificaInutilizacaonCdDoctoFiscal: TIntegerField;
    qryVerificaSerieFiscalInutil: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qrySerieFiscalLojaBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btnVisualizarGradeClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSerieFiscal: TfrmSerieFiscal;

implementation

uses
  fMenu, fLookup_Padrao, StrUtils;

{$R *.dfm}

procedure TfrmSerieFiscal.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'SERIEFISCAL' ;
  nCdTabelaSistema  := 36 ;
  nCdConsultaPadrao := 78 ;
end;

procedure TfrmSerieFiscal.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit11.SetFocus ;
end;

procedure TfrmSerieFiscal.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(25);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmSerieFiscal.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(77);

            If (nPK > 0) then
            begin
                qryMasternCdTipoDoctoFiscal.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmSerieFiscal.DBEdit9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(92);

            If (nPK > 0) then
            begin
                qryMasternCdModImpDF.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmSerieFiscal.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DBEdit11.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o da s�rie fiscal.');
      DBEdit11.SetFocus;
      Abort;
  end;

  try
      strToint(Trim(qryMastercModelo.Value)) ;
  except
      MensagemAlerta('Modelo inv�lido. Informe apenas os n�meros.') ;
      DBEdit4.SetFocus;
      abort ;
  end ;

  try
      strToint(Trim(qryMastercSerie.Value)) ;
  except
      MensagemAlerta('S�rie fiscal inv�lida. Informe apenas os n�meros.') ;
      DBEdit5.SetFocus;
      abort ;
  end ;

  { -- verifica se regra da s�rie fiscal n�o foi cadastrada anteriormente -- }
  qryVerificaSerieFiscal.Close;
  qryVerificaSerieFiscal.Parameters.ParamByName('nPK').Value                := qryMasternCdSerieFiscal.Value;
  qryVerificaSerieFiscal.Parameters.ParamByName('nCdEmpresa').Value         := qryMasternCdEmpresa.Value;
  qryVerificaSerieFiscal.Parameters.ParamByName('nCdTipoDoctoFiscal').Value := qryMasternCdTipoDoctoFiscal.Value;
  qryVerificaSerieFiscal.Parameters.ParamByName('cModelo').Value            := qryMastercModelo.Value;
  qryVerificaSerieFiscal.Parameters.ParamByName('cSerie').Value             := qryMastercSerie.Value;
  qryVerificaSerieFiscal.Open;

  if ((not qryVerificaSerieFiscal.IsEmpty) and (frmMenu.LeParametro('VAREJO') <> 'S')) then
  begin
      MensagemAlerta('Regra de s�rie fiscal j� cadastrado anteriormente.');
      DBEdit2.SetFocus;
      Abort;
  end;

  inherited;
end;

procedure TfrmSerieFiscal.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
          if (qrySerieFiscalLoja.State = dsBrowse) then
              qrySerieFiscalLoja.Edit;

          nPK := frmLookup_Padrao.ExecutaConsulta(59);

          if (nPK > 0) then
              qrySerieFiscalLojanCdLoja.Value := nPK;
      end;
  end;
end;

procedure TfrmSerieFiscal.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;
end;

procedure TfrmSerieFiscal.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qrySerieFiscalLoja.Close;
  qryNumDoctoFiscal.Close;
  qrySerieFiscalInutil.Close;

  if (qryMaster.IsEmpty) then
      Exit;

  PosicionaQuery(qrySerieFiscalLoja, qryMasternCdSerieFiscal.AsString);
  PosicionaQuery(qryNumDoctoFiscal, qryMasternCdSerieFiscal.AsString);
  PosicionaQuery(qrySerieFiscalInutil, qryMasternCdSerieFiscal.AsString);

  if (tabLoja.Visible) then
     cxPageControl1.ActivePage := tabLoja;
end;

procedure TfrmSerieFiscal.qrySerieFiscalLojaBeforePost(DataSet: TDataSet);
begin
  if (qrySerieFiscalLojacNmLoja.Value = '') then
  begin
      MensagemAlerta('Informe a loja da vinculado a s�rie fiscal.');
      DBGridEh1.SelectedField := qrySerieFiscalLojanCdLoja;
      DBGridEh1.SetFocus;
      Abort;
  end;

  qryVerificaSerieFiscalLoja.Close;
  qryVerificaSerieFiscalLoja.Parameters.ParamByName('nCdLoja').Value        := qrySerieFiscalLojanCdLoja.Value;
  qryVerificaSerieFiscalLoja.Parameters.ParamByName('nCdSerieFiscal').Value := qryMasternCdSerieFiscal.Value;
  qryVerificaSerieFiscalLoja.Open;

  if (not qryVerificaSerieFiscalLoja.IsEmpty) then
  begin
      MensagemAlerta('Loja j� vinculada no cadastro da s�rie fiscal No ' + qryVerificaSerieFiscalLojanCdSerieFiscal.AsString + '.');
      DBGridEh1.SelectedField := qrySerieFiscalLojanCdLoja;
      DBGridEh1.SetFocus;
      Abort;
  end;

  inherited;

  if (qrySerieFiscalLoja.State = dsInsert) then
  begin
      qrySerieFiscalLojanCdSerieFiscalLoja.Value := frmMenu.fnProximoCodigo('SERIEFISCALLOJA');
      qrySerieFiscalLojanCdSerieFiscal.Value     := qryMasternCdSerieFiscal.Value;
  end;
end;

procedure TfrmSerieFiscal.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePage := tabLoja;

  if (frmMenu.LeParametro('VAREJO') <> 'S') then
  begin
      cxPageControl1.ActivePage := tabNumDocFiscal;
      tabLoja.Visible           := False;
  end;
end;

procedure TfrmSerieFiscal.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qrySerieFiscalLoja.Close;
  qryNumDoctoFiscal.Close;
  qrySerieFiscalInutil.Close;
end;

procedure TfrmSerieFiscal.btnVisualizarGradeClick(Sender: TObject);
var
  cMotivoInutil : String;
  cNrInutil     : String;
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  case MessageDlg('Ao inutilizar a numera��o da s�rie fiscal o sistema ir� ignorar esta sequ�ncia no faturamento de notas fiscais.' + #13#13 + 'Deseja prosseguir ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end;

  cMotivoInutil := '';
  cNrInutil     := '';

  if (not frmMenu.InputQuery('Motivo de inutiliza��o','Ex: n�m expedida pelo app do fisco:',cMotivoInutil)) then
      Exit;

  if (cMotivoInutil = '') then
  begin
      MensagemAlerta('Motivo de inutiliza��o n�o informado.');
      Exit;
  end;

  if (Length(cMotivoInutil) < 15) then
  begin
      MensagemAlerta('Motivo de inutiliza��o deve possuir no m�nimo 15 caracteres.');
      Exit;
  end;

  if (not frmMenu.InputQuery('N�mera��o Inutilizada','N�mero para inutiliza��o:',cNrInutil)) then
      Exit;

  if (StrToIntDef(Trim(cNrInutil), 0) = 0) then
  begin
      MensagemAlerta('N�mero ' + cNrInutil + ' para inutiliza��o inv�lido.');
      Exit;
  end;

  { -- verifica se numera��o inutil. j� foi registrada -- }
  qryVerificaSerieFiscalInutil.Close;
  qryVerificaSerieFiscalInutil.Parameters.ParamByName('nCdSerieFiscal').Value := qryMasternCdSerieFiscal.Value;
  qryVerificaSerieFiscalInutil.Parameters.ParamByName('iNrInutil').Value      := StrToInt(Trim(cNrInutil));
  qryVerificaSerieFiscalInutil.Open;

  if (not qryVerificaSerieFiscalInutil.IsEmpty) then
  begin
      MensagemErro('Numera��o ' + cNrInutil + ' j� foi inutilizada para esta s�rie fiscal.');
      Exit;
  end;

  { -- verifica se numera��o inutil. n�o possui reserva ou j� foi utilizada -- }
  qryVerificaInutilizacao.Close;
  qryVerificaInutilizacao.Parameters.ParamByName('nCdSerieFiscal').Value := qryMasternCdSerieFiscal.Value;
  qryVerificaInutilizacao.Parameters.ParamByName('iNrInutil').Value      := StrToInt(Trim(cNrInutil));
  qryVerificaInutilizacao.Parameters.ParamByName('nCdTipoDocto').Value   := qryMasternCdTipoDoctoFiscal.Value;
  qryVerificaInutilizacao.Open;

  if (qryVerificaInutilizacaonCdDoctoFiscal.Value > 0) then
  begin
      MensagemErro('Numera��o ' + cNrInutil + ' j� utilizada pelo documento fiscal ' + qryVerificaInutilizacaonCdDoctoFiscal.AsString + '.');
      Exit;
  end;

  try
      try
          case MessageDlg('O procedimento de inutiliza��o n�o poder� ser desfeito.' + #13#13 + 'N�mera��o: ' + cNrInutil + #13 + 'Motivo: ' + AnsiUpperCase(cMotivoInutil) + #13#13 + 'Deseja realmente continuar ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo : Exit;
          end;

          frmMenu.Connection.BeginTrans;

          qrySerieFiscalInutil.Insert;
          qrySerieFiscalInutilnCdSerieFiscal.Value := qryMasternCdSerieFiscal.Value;
          qrySerieFiscalInutiliNrInutil.Value      := StrToInt(Trim(cNrInutil));
          qrySerieFiscalInutilnCdUsuarioCad.Value  := frmMenu.nCdUsuarioLogado;
          qrySerieFiscalInutildDtCad.Value         := Now();
          qrySerieFiscalInutilcMotivoInutil.Value  := AnsiUpperCase(AnsiMidStr(cMotivoInutil, 1, 200));
          qrySerieFiscalInutil.Post;

          frmMenu.Connection.CommitTrans;

          ShowMessage('Numera��o ' + cNrInutil + ' inutilizada com sucesso.');
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      PosicionaQuery(qrySerieFiscalInutil, qryMasternCdSerieFiscal.AsString);
  end;
end;

initialization
    RegisterClass(TfrmSerieFiscal) ;

end.
