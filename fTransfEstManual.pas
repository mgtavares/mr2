unit fTransfEstManual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, DBClient, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmTransfEstManual = class(TfrmCadastro_Padrao)
    qryMasternCdTransfEst: TIntegerField;
    qryMasternCdEmpresaOrigem: TIntegerField;
    qryMasternCdLojaOrigem: TIntegerField;
    qryMasternCdLocalEstoqueOrigem: TIntegerField;
    qryMasternCdTabTipoTransf: TIntegerField;
    qryMasternCdEmpresaDestino: TIntegerField;
    qryMasternCdLojaDestino: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdLocalEstoqueDestino: TIntegerField;
    qryMasterdDtCad: TDateTimeField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasterdDtFinalizacao: TDateTimeField;
    qryMasternCdUsuarioFinal: TIntegerField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternCdUsuarioCancel: TIntegerField;
    qryMasterdDtConfirmacao: TDateTimeField;
    qryEmpresaOrigem: TADOQuery;
    qryEmpresaOrigemnCdEmpresa: TIntegerField;
    qryEmpresaOrigemcNmEmpresa: TStringField;
    qryEmpresaDestino: TADOQuery;
    qryEmpresaDestinonCdEmpresa: TIntegerField;
    qryEmpresaDestinocNmEmpresa: TStringField;
    qryLojaOrigem: TADOQuery;
    qryLojaOrigemnCdLoja: TAutoIncField;
    qryLojaOrigemcNmLoja: TStringField;
    qryLojaDestino: TADOQuery;
    qryLojaDestinonCdLoja: TAutoIncField;
    qryLojaDestinocNmLoja: TStringField;
    qryLocalEstoqueOrigem: TADOQuery;
    qryLocalEstoqueOrigemnCdLocalEstoque: TIntegerField;
    qryLocalEstoqueOrigemcNmLocalEstoque: TStringField;
    qryLocalEstoqueDestino: TADOQuery;
    qryLocalEstoqueDestinonCdLocalEstoque: TIntegerField;
    qryLocalEstoqueDestinocNmLocalEstoque: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    GroupBox1: TGroupBox;
    DBEdit4: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label2: TLabel;
    DBEdit16: TDBEdit;
    dsEmpresaOrigem: TDataSource;
    DBEdit17: TDBEdit;
    dsLojaOrigem: TDataSource;
    DBEdit18: TDBEdit;
    dsLocalEstoqueOrigem: TDataSource;
    GroupBox2: TGroupBox;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    DBEdit19: TDBEdit;
    DataSource4: TDataSource;
    DBEdit20: TDBEdit;
    dsLojaDestino: TDataSource;
    DBEdit21: TDBEdit;
    dsLocalEstoqueDestino: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit22: TDBEdit;
    dsTerceiro: TDataSource;
    qryAux: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryItem: TADOQuery;
    qryItemnQtde: TBCDField;
    qryItemnValUnitario: TBCDField;
    qryItemnValTotal: TBCDField;
    qryItemcNmItem: TStringField;
    qryItemnCdProduto: TIntegerField;
    dsItem: TDataSource;
    qryItemnCdTransfEst: TIntegerField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryItemnCdItemTransfEst: TAutoIncField;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    qryProdutoEAN: TADOQuery;
    qryProdutoEANnCdProduto: TIntegerField;
    qryProdutoEANnValCusto: TBCDField;
    qryProdutonValCusto: TBCDField;
    qryProdutoEANcNmProduto: TStringField;
    SP_PROCESSA_TRANSFERENCIA_ESTOQUE: TADOStoredProc;
    qryMasternCdUsuarioConfirm: TIntegerField;
    qryItemnQtdeConf: TBCDField;
    qryItemnQtdeDiv: TFloatField;
    GroupBox3: TGroupBox;
    Label9: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label15: TLabel;
    DBEdit9: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit15: TDBEdit;
    Edit1: TEdit;
    Edit2: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    qryTrataProduto: TADOQuery;
    qryPreparaTemp: TADOQuery;
    qryTempNaoLidos: TADOQuery;
    qryTempNaoLidosiPosicao: TIntegerField;
    qryTempNaoLidoscCodigo: TStringField;
    dsTempNaoLidos: TDataSource;
    qryMasternValTransferencia: TBCDField;
    qryMastercFlgModoManual: TIntegerField;
    btProcessar: TcxButton;
    btProtocolo: TcxButton;
    btRegistroLoteSerial: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GroupBox2Enter(Sender: TObject);
    procedure qryItemBeforePost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit8Exit(Sender: TObject);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryItemCalcFields(DataSet: TDataSet);
    procedure DBEdit7Exit(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure DBRadioGroup1Change(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBGridEh2Enter(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1ColEnter(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btProcessarClick(Sender: TObject);
    procedure btProtocoloClick(Sender: TObject);
    procedure btRegistroLoteSerialClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    cVarejo : String ;
  end;

var
  frmTransfEstManual: TfrmTransfEstManual;

implementation

uses fMenu, fLookup_Padrao, rTransferencia_view, fTransfEst_Itens,
  fTransfEst_ItensNaoLidos, fTransfEstManual_LoteSerial, fRegistroMovLote;

{$R *.dfm}

procedure TfrmTransfEstManual.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TRANSFEST' ;
  nCdTabelaSistema  := 52 ;
  nCdConsultaPadrao := 117 ;
  bLimpaAposSalvar  := false ;
end;

procedure TfrmTransfEstManual.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBRadioGroup1.Enabled := True ;
  DBRadioGroup1.SetFocus;
  DBRadioGroup1.ItemIndex := 0 ;

  qryMasternCdEmpresaOrigem.Value  := frmMenu.nCdEmpresaAtiva ;
  qryMasternCdEmpresaDestino.Value := frmMenu.nCdEmpresaAtiva ;

  PosicionaQuery(qryEmpresaOrigem, IntToStr(frmMenu.nCdEmpresaAtiva)) ;
  PosicionaQuery(qryEmpresaDestino, IntToStr(frmMenu.nCdEmpresaAtiva)) ;

  if (dbEdit3.Enabled) then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT nCdLoja FROM UsuarioLoja WHERE nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado)) ;
      qryAux.Open ;

      if (qryAux.RecordCount = 1) then
      begin
          PosicionaQuery(qryLojaOrigem, qryAux.FieldList[0].AsString) ;
          qryMasternCdLojaOrigem.Value := qryLojaOrigemnCdLoja.Value ;
          DbEdit4.SetFocus ;
          DbEdit3.Enabled := False ;
      end ;

      qryAux.Close ;
  end
  else begin
      DbEdit4.SetFocus ;
  end ;

  DBGridEh1.ReadOnly  := False ;

end;

procedure TfrmTransfEstManual.FormShow(Sender: TObject);
begin
  inherited;

  cVarejo := frmMenu.LeParametro('VAREJO') ;

  if (cVarejo <> 'S') then
  begin
      DbEdit3.Enabled := False ;
      DbEdit6.Enabled := False ;
  end ;

  if (frmMenu.LeParametro('VERVALTRANSF') = 'N') then
  begin
      DBGridEh1.Columns[5].Visible := false ;
      DBGridEh1.Columns[6].Visible := false ;

      {rptTransferencia_view.QRDBText2.Enabled := false ;
      rptTransferencia_view.QRLabel16.Enabled := false ; }
  end ;

  Edit1.Enabled := False ;
  Edit2.Enabled := False ;
  Edit3.Enabled := False ;

  DbEdit5.Enabled := False ;
  DbEdit6.Enabled := False ;
  DbEdit7.Enabled := False ;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;

  qryLojaOrigem.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryLojaOrigem.Parameters.ParamByName('nCdUsuario').Value  := frmMenu.nCdUsuarioLogado;
  qryLojaDestino.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmTransfEstManual.GroupBox2Enter(Sender: TObject);
begin
  inherited;
{  DbEdit7.Enabled := False ;
  DbEdit6.Enabled := False ;

  qryLojaOrigem.Close ;
  qryLojaDestino.Close ;
  qryLocalEstoqueOrigem.Close ;
  qryLocalEstoqueDestino.Close ;

  dbEdit3.Text := '' ;
  dbEdit4.Text := '' ;
  DbEdit6.Text := '' ;
  dbEdit7.Text := '' ;
  dbEdit8.Text := '' ;

  if (dbRadioGroup1.ItemIndex = 0) then
  begin

      qryMasternCdEmpresaOrigem.Value  := frmMenu.nCdEmpresaAtiva ;
      qryMasternCdEmpresaDestino.Value := frmMenu.nCdEmpresaAtiva ;

      DbEdit19.Text := DbEdit16.Text ;

      DbEdit5.Enabled := False ;
      DbEdit6.Enabled := False ;
      DbEdit7.Enabled := False ;

      if (dbedit3.Text <> '') then
      begin
          qryMasternCdLojaDestino.Value := qryMasternCdLojaOrigem.Value ;
          PosicionaQuery(qryLojaDestino, qryMasternCdLojaDestino.AsString) ;
      end ;

  end ;

  if (dbRadioGroup1.ItemIndex = 1) then
  begin

      DbEdit5.Enabled := True ;

      if (cVarejo = 'S') then
          DbEdit6.Enabled := True ;

      DbEdit7.Enabled := False ;

  end ;

  if (dbRadioGroup1.ItemIndex = 2) then
  begin

      DbEdit5.Enabled := True ;

      if (cVarejo = 'S') then
          DbEdit6.Enabled := True ;

      DbEdit7.Enabled := True ;

  end ;    }

end;

procedure TfrmTransfEstManual.qryItemBeforePost(DataSet: TDataSet);
begin

  if (qryItemnQtde.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade.') ;
      abort ;
  end ;

  if (frmMenu.LeParametro('VERVALTRANSF') = 'N') then
      if (qryItem.State = dsInsert) and (qryItemnCdProduto.Value > 0) and (qryItemnValUnitario.Value = 0) then
      begin
          if (qryProdutonValCusto.Value > 0) and (qryItemnValUnitario.Value = 0) then
              qryItemnValUnitario.Value := qryProdutonValCusto.Value ;

          if (qryItemnValUnitario.Value = 0) then
          begin
              {-- Localiza o pre�o do �ltimo recebimento --}
              qryAux.Close ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT TOP 1 nValCustoFinal');
              qryAux.SQL.Add('  FROM ItemRecebimento');
              qryAux.SQL.Add('       INNER JOIN Recebimento ON Recebimento.nCdRecebimento = ItemRecebimento.nCdRecebimento');
              qryAux.SQL.Add(' WHERE Recebimento.nCdTabStatusReceb >= 4') ;
              qryAux.SQL.Add('   AND Recebimento.nCdEmpresa         = ' + intToStr(frmMenu.nCdEmpresaAtiva)) ;
              qryAux.SQL.Add('   AND nCdProduto                     = ' + qryItemnCdProduto.AsString) ;
              qryAux.SQL.Add(' ORDER BY dDtReceb DESC ');
              qryAux.Open;

              if not qryAux.Eof then
                  qryItemnValUnitario.Value := qryAux.FieldList[0].Value
              else
              begin

                  {-- Localiza o pre�o do pedido recebido --}
                  qryAux.Close ;
                  qryAux.SQL.Clear ;
                  qryAux.SQL.Add('SELECT TOP 1 nValCustoUnit');
                  qryAux.SQL.Add('  FROM ItemPedido');
                  qryAux.SQL.Add('       INNER JOIN Pedido     ON Pedido.nCdPedido = ItemPedido.nCdPedido');
                  qryAux.SQL.Add('       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedido.nCdTipoPedido');
                  qryAux.SQL.Add(' WHERE Pedido.nCdTabStatusPed IN (4,5)');
                  qryAux.SQL.Add('   AND Pedido.nCdEmpresa       = ' + intToStr(frmMenu.nCdEmpresaAtiva)) ;
                  qryAux.SQL.Add('   AND ItemPedido.nCdProduto   = ' + qryItemnCdProduto.AsString) ;
                  qryAux.SQL.Add('   AND ItemPedido.nQtdeExpRec  > 0');
                  qryAux.SQL.Add('   AND TipoPedido.cFlgCompra   = 1');
                  qryAux.SQL.Add(' ORDER BY dDtPedido DESC');
                  qryAux.Open;

                  if not qryAux.Eof then
                      qryItemnValUnitario.Value := qryAux.FieldList[0].Value ;
              end ;
          end ;
      end ;

  qryItemnValTotal.Value    := qryItemnValUnitario.Value * qryItemnQtde.Value ;

  if (qryItemnValTotal.Value <= 0) and (DBGridEh1.Columns[5].Visible) then
  begin
      MensagemAlerta('Informe o valor unit�rio do item.') ;
      abort ;
  end ;

  if (qryMasternCdTabTipoTransf.Value = 3) and (qryItemnValTotal.Value = 0) then
  begin
      MensagemAlerta('Para remessa de consigna��o � necess�rio digita��o de valores.') ;
      abort ;
  end ;

  inherited;

  qryItemnCdTransfEst.Value := qryMasternCdTransfEst.Value ;

  if (qryItem.State = dsInsert) then
      qryItemnCdItemTransfEst.Value := frmMenu.fnProximoCodigo('ITEMTRANSFEST') ;
      
end;

procedure TfrmTransfEstManual.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if ((qryMaster.State <> dsBrowse) and (qryMaster.State <> dsInactive)) then
      qryMaster.Post ;

end;

procedure TfrmTransfEstManual.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  DBEdit3.ReadOnly := False ;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMastercFlgModoManual.Value = 0) then
  begin
      MensagemAlerta('Esta transfer�ncia s� pode ser visualizada na tela de transfer�ncias com leitor.') ;
      btcancelar.Click;
      exit ;
  end ;

  if (qryMasternCdLojaOrigem.Value > 0) then
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ' AND UL.nCdLoja = ' + qryMasternCdLojaOrigem.AsString) ;
      qryAux.Open ;

      if (qryAux.Eof) then
      begin
          qryAux.Close ;
          MensagemAlerta('Voc� n�o tem permiss�o para ver transfer�ncias desta loja.') ;
          btCancelar.Click;
          exit ;
      end ;

      qryAux.Close ;

  end ;

  DBRadioGroup1.Enabled := True ;
      
  PosicionaQuery(qryEmpresaOrigem, qryMasternCdEmpresaOrigem.asString) ;
  PosicionaQuery(qryEmpresaDestino, qryMasternCdEmpresaDestino.asString) ;

  PosicionaQuery(qryLojaOrigem, qryMasternCdLojaOrigem.AsString) ;
  PosicionaQuery(qryLojaDestino, qryMasternCdLojaDestino.asString) ;

  qryLocalEstoqueOrigem.Close ;
  qryLocalEstoqueDestino.Close ;

  qryLocalEstoqueOrigem.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLojaOrigem.Value ;
  qryLocalEstoqueOrigem.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresaOrigem.Value ;
  PosicionaQuery(qryLocalEstoqueOrigem, qryMasternCdLocalEstoqueOrigem.AsString) ;

  qryLocalEstoqueDestino.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLojaDestino.Value ;
  qryLocalEstoqueDestino.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresaDestino.Value ;
  qryLocalEstoqueDestino.Parameters.ParamByName('nCdTerceiro').Value:= qryMasternCdTerceiro.Value ;
  PosicionaQuery(qryLocalEstoqueDestino, qryMasternCdLocalEstoqueDestino.AsString) ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;
  
  PosicionaQuery(qryItem, qryMasternCdTransfEst.AsString) ;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuarioCad.AsString) ;

  Edit1.Text := '' ;
  Edit2.Text := '' ;
  Edit3.Text := '' ;
  Edit4.Text := '' ;

  if not qryUsuario.eof then
      Edit1.Text := qryUsuariocNmUsuario.Value ;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuarioFinal.AsString) ;

  if not qryUsuario.eof then
      Edit2.Text := qryUsuariocNmUsuario.Value ;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuarioCancel.AsString) ;

  if not qryUsuario.eof then
      Edit3.Text := qryUsuariocNmUsuario.Value ;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuarioConfirm.AsString) ;

  if not qryUsuario.eof then
      Edit4.Text := qryUsuariocNmUsuario.Value ;

  DBGridEh1.ReadOnly  := False ;

  if (qryMasterdDtFinalizacao.AsString <> '') then
      DBGridEh1.ReadOnly     := True  ;

end;

procedure TfrmTransfEstManual.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  Edit1.Text := '' ;
  Edit2.Text := '' ;
  Edit3.Text := '' ;
  EDit4.Text := '' ;

  qryTerceiro.Close ;
  qryEmpresaOrigem.Close ;
  qryEmpresaDestino.Close ;
  qryLojaOrigem.Close ;
  qryLojaDestino.Close ;
  qryLocalEstoqueOrigem.Close ;
  qryLocalEstoqueDestino.Close ;

  qryItem.Close ;

end;

procedure TfrmTransfEstManual.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (dbEdit18.Text = '') then
  begin
      MensagemAlerta('Informe o estoque de origem.') ;
      dbEdit4.SetFocus ;
      abort ;
  end ;

  if (dbEdit21.Text = '') then
  begin
      MensagemAlerta('Informe o estoque de destino.') ;
      dbEdit8.SetFocus ;
      abort ;
  end ;

  if (qryLocalEstoqueOrigemnCdLocalEstoque.Value = qryLocalEstoqueDestinonCdLocalEstoque.Value) then
  begin
      MensagemAlerta('Os estoques de origem e destino devem ser diferentes.') ;
      dbEdit4.SetFocus ;
      abort ;
  end ;

  inherited;

  qryMasterdDtCad.Value            := Now() ;
  qryMasternCdUsuarioCad.Value     := frmMenu.nCdUsuarioLogado;
  qryMasternValTransferencia.Value := StrToFloat(DBGridEh1.FieldColumns['nValTotal'].Footer.SumValue);
  qryMastercFlgModoManual.Value    := 1 ;
end;

procedure TfrmTransfEstManual.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (cVarejo = 'S') then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(87,'Loja.nCdLoja = ' + DbEdit3.Text);
            end
            else begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + DbEdit2.Text);
            end ;

            If (nPK > 0) then
            begin
                qryMasternCdLocalEstoqueOrigem.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfEstManual.DBEdit8KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
    cWhere : String ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            If (dbEdit5.Text = '') then
            begin
                MensagemAlerta('Informe a empresa de destino.') ;
                DbEdit5.SetFocus ;
                exit ;
            end ;

            If (dbEdit6.Text = '') and (cVarejo = 'S') then
            begin
                MensagemAlerta('Informe a loja de destino.') ;
                DbEdit6.SetFocus ;
                exit ;
            end ;

            if (DbRadioGroup1.ItemIndex = 2) and (dbEdit7.Text = '') then
            begin
                MensagemAlerta('Informe o Terceiro de Destino.') ;
                DbEdit7.SetFocus ;
                exit ;
            end ;

            cWhere := ' ' ;
            if (DbEdit5.Text <> '') then cWhere := cWhere + 'Empresa.nCdEmpresa = ' + dbEdit5.Text ;
            If (dbEdit6.Text <> '') then cWhere := cWhere + ' AND Loja.nCdLoja = ' + dbEdit6.Text  ;
            if (dbEdit7.Text <> '') then cWhere := cWhere + ' AND LocalEstoque.nCdTerceiro = ' + dbEdit7.Text ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(87,cWhere);

            If (nPK > 0) then
            begin
                qryMasternCdLocalEstoqueDestino.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfEstManual.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            If (dbEdit5.Text = '') then
            begin
                MensagemAlerta('Informe a empresa de destino.') ;
                DbEdit5.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(86,'Loja.nCdEmpresa = ' + dbEdit5.Text);

            If (nPK > 0) then
            begin
                qryMasternCdLojaDestino.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfEstManual.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(25);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresaDestino.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfEstManual.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  qryLocalEstoqueOrigem.Close ;

  qryLocalEstoqueOrigem.Parameters.ParamByName('nCdEmpresa').Value := DbEdit2.text ;

  if (dbEdit3.Enabled) then
  begin
      if (dbEdit3.Text = '') then
      begin
          MensagemAlerta('Informe a loja de origem.') ;
          DbEdit3.SetFocus;
          exit ;
      end ;

      qryLocalEstoqueOrigem.Parameters.ParamByName('nCdLoja').Value := frmMenu.ConvInteiro(DbEdit3.text) ;
  end
  else begin
      qryLocalEstoqueOrigem.Parameters.ParamByName('nCdLoja').Value := 0 ;
  end ;

  PosicionaQuery(qryLocalEstoqueOrigem, DbEdit4.Text) ;

end;

procedure TfrmTransfEstManual.DBEdit8Exit(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;
  
  qryLocalEstoqueDestino.Close ;

  qryLocalEstoqueDestino.Parameters.ParamByName('nCdEmpresa').Value := DbEdit5.text ;

  if (dbEdit6.Enabled) then
  begin
      if (dbEdit6.Text = '') then
      begin
          MensagemAlerta('Informe a loja de destino.') ;
          DbEdit6.SetFocus;
          exit ;
      end ;

      qryLocalEstoqueDestino.Parameters.ParamByName('nCdLoja').Value := frmMenu.ConvInteiro(DbEdit6.text) ;
  end
  else begin
      qryLocalEstoqueDestino.Parameters.ParamByName('nCdLoja').Value := 0 ;
  end ;

  if (dbEdit7.Enabled) and (qryMasternCdTabTipoTransf.Value = 3) then
  begin
      if (dbEdit7.Text = '') then
      begin
          MensagemAlerta('Informe o terceiro de destino.') ;
          DbEdit7.SetFocus;
          exit ;
      end ;

      qryLocalEstoqueDestino.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(DbEdit7.text) ;
  end
  else begin
      qryLocalEstoqueDestino.Parameters.ParamByName('nCdTerceiro').Value := 0 ;
  end ;

  PosicionaQuery(qryLocalEstoqueDestino, DbEdit8.Text) ;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

end;

procedure TfrmTransfEstManual.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(17,'EXISTS(SELECT 1 FROM LocalEstoque WHERE LocalEstoque.nCdTerceiro = vTerceiros.nCdTerceiro)');

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfEstManual.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryItem, qryMasternCdTransfEst.AsString) ;

end;

procedure TfrmTransfEstManual.qryItemCalcFields(DataSet: TDataSet);
begin
  {--inherited;--}

  if (qryItemcNmItem.Value = '') and (qryItemnCdProduto.Value > 0) then
  begin
      qryProduto.Close;
      PosicionaQuery(qryProduto, qryItemnCdProduto.AsString) ;

      if not qryProduto.eof then
          qryItemcNmItem.Value      := qryProdutocNmProduto.Value ;

  end ;

  qryItemnQtdeDiv.Value := qryItemnQtde.Value - qryItemnQtdeConf.Value ;

end;

procedure TfrmTransfEstManual.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  Posicionaquery(qryTerceiro, DbEdit7.Text) ;
  
end;

procedure TfrmTransfEstManual.FormActivate(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePage := cxTabSheet1 ;

end;

procedure TfrmTransfEstManual.DBRadioGroup1Change(Sender: TObject);
begin
  inherited;

  DbEdit7.Enabled := False ;
  DbEdit6.Enabled := False ;

  dbEdit3.Text := '' ;
  dbEdit4.Text := '' ;
  DbEdit6.Text := '' ;
  dbEdit7.Text := '' ;
  dbEdit8.Text := '' ;

  DBEdit3.ReadOnly := False ;

  if not qryMaster.Active then
      exit ;
      
  if (qryMaster.State <> dsBrowse) then
  begin
  
      qryLojaOrigem.Close ;
      qryLojaDestino.Close ;
      qryLocalEstoqueOrigem.Close ;
      qryLocalEstoqueDestino.Close ;

      if (cVarejo = 'S') then
      begin
          PosicionaQuery(qryLojaOrigem, IntToStr(frmMenu.nCdLojaAtiva)) ;
          qryMasternCdLojaOrigem.Value := frmMenu.nCdLojaAtiva;
          DBEdit3.ReadOnly := True ;
      End ;

  end ;


  if (dbRadioGroup1.ItemIndex = 0) then
  begin

      Label17.Caption  := 'Loja' ;
      DBEdit6.Visible  := True ;
      DBEdit20.Visible := True ;
      DBEdit7.Visible  := False ;
      DBEdit22.Visible := False ;

      if (qryMaster.State <> dsBrowse) then
      begin

          qryMasternCdEmpresaOrigem.Value  := frmMenu.nCdEmpresaAtiva ;
          qryMasternCdEmpresaDestino.Value := frmMenu.nCdEmpresaAtiva ;

          if (cVarejo = 'S') then
          begin
              PosicionaQuery(qryLojaDestino, IntToStr(frmMenu.nCdLojaAtiva)) ;
              qryMasternCdLojaDestino.Value := frmMenu.nCdLojaAtiva;
          end ;

      end ;

      DbEdit19.Text := DbEdit16.Text ;

      DbEdit5.Enabled := False ;
      DbEdit6.Enabled := False ;
      DbEdit7.Enabled := False ;

      if (DBEdit3.Enabled) then
          DBEdit3.SetFocus ;

  end ;

  if (dbRadioGroup1.ItemIndex = 1) then
  begin

      Label17.Caption  := 'Loja' ;
      DBEdit6.Visible  := True ;
      DBEdit20.Visible := True ;
      DBEdit7.Visible  := False ;
      DBEdit22.Visible := False ;

      DbEdit5.Enabled := True ;

      if (cVarejo = 'S') then
      begin
          DbEdit6.Enabled := True ;
      end ;

      DbEdit7.Enabled := False ;

      if (qryMaster.State <> dsBrowse) then
      begin

          qryMasternCdLojaDestino.asString := '' ;

      end ;

      if (DBEdit3.Enabled) then
          DBEdit3.SetFocus ;


  end ;

  if (dbRadioGroup1.ItemIndex = 2) then
  begin

      Label17.Caption  := 'Terceiro' ;
      DBEdit6.Visible  := False ;
      DBEdit20.Visible := False ;
      DBEdit7.Visible  := True ;
      DBEdit22.Visible := True ;

      DbEdit5.Enabled := True ;

      if (cVarejo = 'S') then
          DbEdit6.Enabled := True ;

      DbEdit7.Enabled := True ;

      if (qryMaster.State <> dsBrowse) then
      begin

          qryMasternCdLojaDestino.asString := '' ;

      end ;

      if (DBEdit3.Enabled) then
          DBEdit3.SetFocus ;

  end ;

end;

procedure TfrmTransfEstManual.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryLojaDestino, DbEdit6.Text) ;
  
end;

procedure TfrmTransfEstManual.DBGridEh2Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

end;

procedure TfrmTransfEstManual.cxButton1Click(Sender: TObject);
begin

    if (qryMaster.Active) and (qryMasternCdTransfEst.Value > 0) then
        PosicionaQuery(qryItem, qryMasternCdTransfEst.AsString) ;

end;

procedure TfrmTransfEstManual.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItem.State = dsBrowse) then
             qryItem.Edit ;

        if (qryItem.State = dsInsert) or (qryItem.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(76,'NOT EXISTS(SELECT 1 FROM Produto Produto2 WHERE Produto2.nCdProdutoPai = Produto.nCdProduto)');

            If (nPK > 0) then
            begin
                qryItemnCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTransfEstManual.DBGridEh1ColEnter(Sender: TObject);
begin
  inherited;

  if (DBGridEh1.Col = 6) then
  begin
      if (qryItem.State = dsInsert) and (qryItemnCdProduto.Value > 0) and (qryItemnValUnitario.Value = 0) then
      begin
          if (qryProdutonValCusto.Value > 0) and (qryItemnValUnitario.Value = 0) then
              qryItemnValUnitario.Value := qryProdutonValCusto.Value ;

          if (qryItemnValUnitario.Value = 0) then
          begin
              {-- Localiza o pre�o do �ltimo recebimento --}
              qryAux.Close ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT TOP 1 nValCustoFinal');
              qryAux.SQL.Add('  FROM ItemRecebimento');
              qryAux.SQL.Add('       INNER JOIN Recebimento ON Recebimento.nCdRecebimento = ItemRecebimento.nCdRecebimento');
              qryAux.SQL.Add(' WHERE Recebimento.nCdTabStatusReceb >= 4') ;
              qryAux.SQL.Add('   AND Recebimento.nCdEmpresa         = ' + intToStr(frmMenu.nCdEmpresaAtiva)) ;
              qryAux.SQL.Add('   AND nCdProduto                     = ' + qryItemnCdProduto.AsString) ;
              qryAux.SQL.Add(' ORDER BY dDtReceb DESC ');
              qryAux.Open;

              if not qryAux.Eof then
                  qryItemnValUnitario.Value := qryAux.FieldList[0].Value
              else
              begin

                  {-- Localiza o pre�o do pedido recebido --}
                  qryAux.Close ;
                  qryAux.SQL.Clear ;
                  qryAux.SQL.Add('SELECT TOP 1 nValCustoUnit');
                  qryAux.SQL.Add('  FROM ItemPedido');
                  qryAux.SQL.Add('       INNER JOIN Pedido     ON Pedido.nCdPedido = ItemPedido.nCdPedido');
                  qryAux.SQL.Add('       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedido.nCdTipoPedido');
                  qryAux.SQL.Add(' WHERE Pedido.nCdTabStatusPed IN (4,5)');
                  qryAux.SQL.Add('   AND Pedido.nCdEmpresa       = ' + intToStr(frmMenu.nCdEmpresaAtiva)) ;
                  qryAux.SQL.Add('   AND ItemPedido.nCdProduto   = ' + qryItemnCdProduto.AsString) ;
                  qryAux.SQL.Add('   AND ItemPedido.nQtdeExpRec  > 0');
                  qryAux.SQL.Add('   AND TipoPedido.cFlgCompra   = 1');
                  qryAux.SQL.Add(' ORDER BY dDtPedido DESC');
                  qryAux.Open;

                  if not qryAux.Eof then
                      qryItemnValUnitario.Value := qryAux.FieldList[0].Value ;
              end ;

          end ;

      end ;

  end ;

end;

procedure TfrmTransfEstManual.btSalvarClick(Sender: TObject);
begin

  if (qryMaster.Active) and (qryMasterdDtFinalizacao.AsString <> '') then
  begin
      MensagemErro('N�o � poss�vel alterar uma transfer�ncia j� processada.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmTransfEstManual.btExcluirClick(Sender: TObject);
begin

  if (qryMaster.Active) and (qryMasterdDtFinalizacao.AsString <> '') then
  begin
      MensagemErro('N�o � poss�vel alterar uma transfer�ncia j� processada.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmTransfEstManual.btProcessarClick(Sender: TObject);
var
    nValTransferencia : double ;
    objForm           : TfrmRegistroMovLote ;
    nQtdeLote         : double ;
begin

  if not qryMaster.Active then
  begin
      MensagemAlerta('Selecione uma transfer�ncia.') ;
      exit ;
  end ;

  if (qryMasterdDtFinalizacao.AsString <> '') then
  begin
      MensagemAlerta('Transfer�ncia j� processada.') ;
      exit ;
  end ;

  qryItem.First ;

  if (qryItem.isEmpty) then
  begin
      MensagemAlerta('Nenhum item digitado.') ;
      exit ;
  end ;

  objForm := TfrmRegistroMovLote.Create( Self ) ;

  try

      qryItem.First;

      while not (qryItem.Eof) do
      begin

          nQtdeLote := objForm.retornaQuantidadeRegistradaTemporaria( qryItemnCdProduto.Value
                                                                    , 4
                                                                    , qryItemnCdItemTransfEst.Value ) ;

          {-- quando o produto n�o tem lote/serial controlados a fun��o retorna -1 --}
          if (nQtdeLote <> -1) then
          begin

              if ( nQtdeLote <> qryItemnQtde.Value ) then
              begin
                  MensagemAlerta('A quantidade especificada no Registro de Lotes/Seriais para o produto: ' + qryItemnCdProduto.AsString + ' - ' + qryItemcNmItem.Value + ' n�o confere com o total deste movimento.') ;
                  btRegistroLoteSerial.Click;
                  abort ;
              end ;

          end ;

          qryItem.Next;
      end;

  finally
      freeAndNil( objForm ) ;
  end ;

  nValTransferencia := 0 ;

  qryItem.First ;

  while not qryItem.eof do
  begin
      nValTransferencia := qryItemnValTotal.Value + nValTransferencia ;
      qryItem.Next ;
  end ;

  qryItem.First ;

  case MessageDlg('Confirma o processamento da transfer�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  qryMaster.Edit;
  qryMasternValTransferencia.Value := nValTransferencia;
  qryMaster.Post;

  frmMenu.Connection.BeginTrans;

  try
    SP_PROCESSA_TRANSFERENCIA_ESTOQUE.Close ;
    SP_PROCESSA_TRANSFERENCIA_ESTOQUE.Parameters.ParamByName('@nCdTransfEst').Value := qryMasternCdTransfEst.Value ;
    SP_PROCESSA_TRANSFERENCIA_ESTOQUE.Parameters.ParamByName('@nCdUsuario').Value   := frmMenu.nCdUsuarioLogado;
    SP_PROCESSA_TRANSFERENCIA_ESTOQUE.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  PosicionaQuery(qryMaster, qryMasternCdTransfEst.AsString) ;

  ShowMessage('Transfer�ncia Processada com Sucesso.') ;

  case MessageDlg('Deseja imprimir o protocolo ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes: btProtocolo.Click ;
  end ;

  btCancelar.Click;

end;

procedure TfrmTransfEstManual.btProtocoloClick(Sender: TObject);
var
  objRel : TrptTransferencia_view;
begin

  if not qryMaster.Active then
  begin
      MensagemAlerta('Selecione uma transfer�ncia.') ;
      exit ;
  end ;

  if (qryMasterdDtFinalizacao.AsString = '') then
  begin
      MensagemAlerta('Transfer�ncia n�o processada.') ;
      exit;
  end ;

  objRel := TrptTransferencia_view.Create(nil);

  try
      try
          PosicionaQuery(objRel.qryMaster, qryMasternCdTransfEst.AsString) ;
          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmTransfEstManual.btRegistroLoteSerialClick(Sender: TObject);
var
  objForm : TfrmTransfEstManual_LoteSerial;
begin
  inherited;

  if (qryMaster.State = dsInsert) and (qryMasternCdTransfEst.Value = 0) then
  begin
      qryMaster.Post ;
  end ;

  if (not qryMaster.Active) or (qryMaster.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum Registro Ativo.') ;
      abort ;
  end ;

  try
      objForm := TfrmTransfEstManual_LoteSerial.Create( Self ) ;

      objForm.ItemLoteSerial(qryMasternCdTransfEst.Value
                            ,4  {-- Transfer�ncia Entre Estoque - Sa�da --}
                            ,qryMasternCdLocalEstoqueOrigem.Value
                            , False
                            ,(qryMasterdDtFinalizacao.AsString <> ''));
  finally
      freeAndNil( objForm ) ;
  end;

end;

Initialization
    RegisterClass(TfrmTransfEstManual) ;

end.
