unit rFluxoCaixa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons, comObj;

type
  TrptFluxoCaixa = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    MaskEdit3: TMaskEdit;
    RadioGroup1: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFluxoCaixa: TrptFluxoCaixa;

implementation

uses fMenu, rImpSP, fLookup_Padrao, rMovTit_view, rDRE_view, QRExport,
  rFluxoCaixa_view;

{$R *.dfm}

procedure TrptFluxoCaixa.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  iAux := frmMenu.nCdEmpresaAtiva;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  if (iAux > 0) then
  begin

    MaskEdit3.Text := IntToStr(iAux) ;

    qryEmpresa.Close ;
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := iAux ;
    qryEmpresa.Open ;

  end ;

  MaskEdit3.SetFocus ;

end;


procedure TrptFluxoCaixa.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TrptFluxoCaixa.ToolButton1Click(Sender: TObject);
var
    dAux   : TDateTime;
    objRel : TrptFluxoCaixa_view;
    linha, coluna : integer;
    planilha      : variant;
    valorcampo    : string;
begin

  If (Trim(MaskEdit3.Text) = '') or (DBEDit3.Text = '') Then
  begin
      ShowMessage('Informe a Empresa.') ;
      exit ;
  end ;

  objRel := TrptFluxoCaixa_view.Create(Self) ;

  objRel.usp_Relatorio.Close ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value  := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  objRel.usp_Relatorio.Open  ;

  if (RadioGroup1.ItemIndex = 0) then
  begin


      objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
      objRel.lblFiltro1.Caption := 'Empresa : ' + DbEdit3.Text ;

      try
          try
              {--visualiza o relat�rio--}
              objRel.QuickRep1.PreviewModal;

          except
              MensagemErro('Erro na cria��o do relat�rio');
              raise;
          end;
      finally
          FreeAndNil(objRel);
      end;

  end
  else
  begin

    planilha:= CreateoleObject('Excel.Application');
    planilha.WorkBooks.add(1);

    planilha.caption := 'FLUXO DE CAIXA';
    planilha.visible := true;

    for linha := 0 to objRel.usp_Relatorio.RecordCount - 1 do
    begin
       for coluna := 1 to objRel.usp_Relatorio.FieldCount do
       begin
         valorcampo := objRel.usp_Relatorio.Fields[coluna - 1].AsString;
         planilha.cells[linha + 2,coluna] := valorCampo;
       end;
       objRel.usp_Relatorio.Next;
    end;

    planilha.cells[1,1] := 'FLUXO DE CAIXA' ;
    planilha.cells[1,5] := DateTimeToStr(Now());
    planilha.cells[1,6] := 'Empresa : ' + dbEdit3.Text ;

    planilha.columns.Autofit;

    FreeAndNil( objRel ) ;

  end ;

end;


procedure TrptFluxoCaixa.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

initialization
     RegisterClass(TrptFluxoCaixa) ;

end.
