unit rRelCredAberto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, DB, ADODB, DBCtrls, StdCtrls, Mask,
  ER2Lookup, ImgList, ComCtrls, ToolWin, ExtCtrls;

type
  TrptRelCredAberto = class(TfrmRelatorio_Padrao)
    Label5: TLabel;
    ER2LookupMaskEdit1: TER2LookupMaskEdit;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    qryTipoLoja: TADOQuery;
    qryTipoLojacNmLoja: TStringField;
    dsTipoLoja: TDataSource;
    MaskEdit6: TMaskEdit;
    Label6: TLabel;
    MaskEdit7: TMaskEdit;
    qryTipoLojanCdLoja: TIntegerField;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRelCredAberto: TrptRelCredAberto;

implementation

{$R *.dfm}
uses
  fMenu,rRelCredAberto_view;

procedure TrptRelCredAberto.ToolButton1Click(Sender: TObject);
var
  ObjRel : TrptRelCredAberto_view;
  MsgFiltro : string;
begin
  inherited;

  ObjRel := TrptRelCredAberto_view.Create(NIl);
  MsgFiltro := '';

  if (trim(MaskEdit6.Text)= '/  /') or (trim(MaskEdit7.Text)= '/  /')then
  begin
      MensagemAlerta('Preencha a data da venda');
      abort;
  end;

  try
      try
           objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

           MsgFiltro := 'loja ' + DBEdit1.Text + #13 + 'Data de Venda       '+ MaskEdit6.Text + '  At�  ' + MaskEdit7.Text;

           ObjRel.qryRelCredAberto.Close;

           ObjRel.lblFiltro1.Caption := MsgFiltro;

           if (trim(ER2LookupMaskEdit1.text) <> '/  /') then
               ObjRel.qryRelCredAberto.Parameters.ParamByName('Loja').value       := ER2LookupMaskEdit1.Text;

           if (trim(MaskEdit6.Text) <> '/  /') then
               ObjRel.qryRelCredAberto.Parameters.ParamByName('DtVendaIni').value := MaskEdit6.Text;

           if (trim(MaskEdit7.Text) <> '/  /') then
               ObjRel.qryRelCredAberto.Parameters.ParamByName('DtVendaFin').value := MaskEdit7.Text;

           ObjRel.qryRelCredAberto.Open;

           ObjRel.QuickRep1.PreviewModal;
        except
          MensagemErro('Erro na cria��o do Relat�rio');
          raise;
       end;
   finally
      FreeAndNil(objRel);
   end;


end;

initialization
  RegisterClass(TrptRelCredAberto) ;



end.
