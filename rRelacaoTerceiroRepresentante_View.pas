unit rRelacaoTerceiroRepresentante_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, jpeg, QRCtrls, QuickRpt, ExtCtrls, ACBrBase, ACBrGAV;

type
  TrptRelacaoTerceiroRepresentante_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    lblFiltros: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRBand3: TQRBand;
    QRBand5: TQRBand;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    qrcabecalho: TQRGroup;
    cNmTerceiroRepres: TQRDBText;
    qrImgLogo: TQRImage;
    SPREL_TERCEIRO_X_REPRESENTANTE: TADOStoredProc;
    QRLabel22: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    nCdTerceiroRepres: TQRDBText;
    lblOrdenacao1: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel4: TQRLabel;
    SPREL_TERCEIRO_X_REPRESENTANTEnCdOrdenacao: TIntegerField;
    SPREL_TERCEIRO_X_REPRESENTANTEcOrdenacao: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcNmFantasia: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcCNPJCPF: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEnCdTerceiro: TIntegerField;
    SPREL_TERCEIRO_X_REPRESENTANTEcNmTerceiro: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEnCdRepresentante: TIntegerField;
    SPREL_TERCEIRO_X_REPRESENTANTEcNmRepresentante: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcNmContato: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEdDtCadastro: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcEmail: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcTelefone1: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcTelefone2: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcTelefoneMovel: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcEndereco: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEiNumero: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcBairro: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcCidade: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcUF: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcCep: TStringField;
    SPREL_TERCEIRO_X_REPRESENTANTEcComplemento: TStringField;
    lblOrdenacao2: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRBand2: TQRBand;
    QRShape6: TQRShape;
    lblTotalRegistro: TQRLabel;
    qrExTotal: TQRExpr;
    QRShape5: TQRShape;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRelacaoTerceiroRepresentante_View: TrptRelacaoTerceiroRepresentante_View;

implementation

uses
  fMenu;

{$R *.dfm}

procedure TrptRelacaoTerceiroRepresentante_View.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  if (SPREL_TERCEIRO_X_REPRESENTANTE.IsEmpty) then
  begin
      QRLabel4.Caption := '';
      Exit;
  end;

  if ((SPREL_TERCEIRO_X_REPRESENTANTEnCdTerceiro.IsNull) and (SPREL_TERCEIRO_X_REPRESENTANTEnCdRepresentante.IsNull)) then
      PrintBand := False;
end;

procedure TrptRelacaoTerceiroRepresentante_View.QRBand2AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
  qrExTotal.Reset;
end;

end.
