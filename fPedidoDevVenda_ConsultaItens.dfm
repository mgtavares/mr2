inherited frmPedidoDevVenda_ConsultaItens: TfrmPedidoDevVenda_ConsultaItens
  Left = 287
  Top = 89
  Width = 963
  Height = 598
  BorderIcons = []
  Caption = 'Consulta Documentos de Sa'#237'da'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 113
    Width = 947
    Height = 447
  end
  inherited ToolBar1: TToolBar
    Width = 947
    TabOrder = 1
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 947
    Height = 84
    Align = alTop
    Caption = ' Filtros '
    TabOrder = 0
    object Label2: TLabel
      Tag = 1
      Left = 29
      Top = 48
      Width = 86
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo do Pedido'
    end
    object Label1: TLabel
      Tag = 1
      Left = 211
      Top = 48
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label3: TLabel
      Tag = 1
      Left = 18
      Top = 24
      Width = 97
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero NF de Sa'#237'da'
    end
    object mskDtInicial: TMaskEdit
      Left = 120
      Top = 40
      Width = 80
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
    end
    object mskDtFinal: TMaskEdit
      Left = 240
      Top = 40
      Width = 81
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object edtNumNF: TMaskEdit
      Left = 120
      Top = 16
      Width = 76
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
    end
    object btConsultarDoctoFiscal: TcxButton
      Left = 324
      Top = 38
      Width = 89
      Height = 26
      Caption = 'Consultar'
      TabOrder = 3
      OnClick = btConsultarDoctoFiscalClick
      Glyph.Data = {
        36030000424D360300000000000036000000280000000F000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
        393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
        00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
        F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
        83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
        68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
        FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
        9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
        8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
        D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
        B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
        5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
        A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
        9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
        488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
        FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
        FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
        FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
  end
  object cxPageControl1: TcxPageControl [3]
    Left = 0
    Top = 113
    Width = 947
    Height = 447
    ActivePage = tabItemDoctoFiscal
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 443
    ClientRectLeft = 4
    ClientRectRight = 943
    ClientRectTop = 24
    object tabItemDoctoFiscal: TcxTabSheet
      Caption = 'Pedidos'
      ImageIndex = 0
      object cxGridPedido: TcxGrid
        Left = 0
        Top = 0
        Width = 939
        Height = 419
        Align = alClient
        PopupMenu = popupMenu
        TabOrder = 0
        object cxGridPedidoDBTableView1: TcxGridDBTableView
          OnDblClick = cxGridPedidoDBTableView1DblClick
          DataController.DataSource = dsConsultaPedido
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellSelect = False
          object cxGridPedidoDBTableView1DBColumn_nCdPedido: TcxGridDBColumn
            Caption = 'Pedido'
            DataBinding.FieldName = 'nCdPedido'
            Width = 65
          end
          object cxGridPedidoDBTableView1DBColumn_dDtPedido: TcxGridDBColumn
            Caption = 'Dt. Pedido'
            DataBinding.FieldName = 'dDtPedido'
          end
          object cxGridPedidoDBTableView1DBColumn_iNrDocto: TcxGridDBColumn
            Caption = 'Nr. NF'
            DataBinding.FieldName = 'iNrDocto'
            Visible = False
            GroupIndex = 0
            Width = 80
          end
          object cxGridPedidoDBTableView1DBColumn_nCdItemPedido: TcxGridDBColumn
            Caption = 'C'#243'd. Item'
            DataBinding.FieldName = 'nCdItemPedido'
            Width = 75
          end
          object cxGridPedidoDBTableView1DBColumn_cCdProduto: TcxGridDBColumn
            Caption = 'C'#243'd. Prod.'
            DataBinding.FieldName = 'cCdProduto'
            Width = 75
          end
          object cxGridPedidoDBTableView1DBColumn_cNmItem: TcxGridDBColumn
            Caption = 'Descri'#231#227'o Produto'
            DataBinding.FieldName = 'cNmItem'
            Width = 321
          end
          object cxGridPedidoDBTableView1DBColumn_nQtdeAtendida: TcxGridDBColumn
            Caption = 'Qtde Ped.'
            DataBinding.FieldName = 'nQtdeAtendida'
            Width = 90
          end
          object cxGridPedidoDBTableView1DBColumn_nValUnitario: TcxGridDBColumn
            Caption = 'Val. Unit'#225'rio'
            DataBinding.FieldName = 'nValUnitario'
            Width = 95
          end
        end
        object cxGridPedidoLevel1: TcxGridLevel
          GridView = cxGridPedidoDBTableView1
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 576
    Top = 232
  end
  object popupMenu: TPopupMenu
    Left = 576
    Top = 261
    object btVisualizarDoctoFiscal: TMenuItem
      Caption = 'Visualizar Documento Fiscal'
      OnClick = btVisualizarDoctoFiscalClick
    end
  end
  object qryConsultaPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'iNrDocto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa  int'
      '       ,@nCdTerceiro int'
      '       ,@iNrDocto    int'
      '       ,@nCdPedido   int'
      '       ,@cDtInicial  varchar(10)'
      '       ,@cDtFinal    varchar(10)'
      ''
      'SET @nCdEmpresa  = :nCdEmpresa'
      'SET @nCdTerceiro = :nCdTerceiro'
      'SET @iNrDocto    = :iNrDocto'
      'SET @nCdPedido   = :nCdPedido'
      'SET @cDtInicial  = :dDtInicial'
      'SET @cDtFinal    = :dDtFinal'
      ''
      'SELECT Pedido.nCdPedido'
      '      ,Pedido.dDtPedido'
      '      ,Terceiro.cNmTerceiro'
      '      ,DoctoFiscal.iNrDocto'
      '      ,ItemPedido.nCdItemPedido'
      '      ,ItemPedido.cCdProduto'
      '      ,ItemPedido.cNmItem'
      '      ,ItemPedido.nCdTipoItemPed'
      '      ,ItemPedido.nValUnitario'
      '      ,ItemPedidoAtendido.nQtdeAtendida'
      '      ,DoctoFiscal.cUFEmitente'
      '      ,DoctoFiscal.nCdDoctoFiscal'
      '  FROM Pedido'
      
        '       INNER JOIN Terceiro           ON Terceiro.nCdTerceiro    ' +
        '         = Pedido.nCdTerceiro'
      
        '       INNER JOIN ItemPedido         ON ItemPedido.nCdPedido    ' +
        '         = Pedido.nCdPedido'
      
        '       INNER JOIN ItemPedidoAtendido ON ItemPedidoAtendido.nCdIt' +
        'emPedido = ItemPedido.nCdItemPedido'
      
        '       INNER JOIN DoctoFiscal        ON DoctoFiscal.nCdDoctoFisc' +
        'al       = ItemPedidoAtendido.nCdDoctoFiscal'
      ' WHERE Pedido.nCdEmpresa          = @nCdEmpresa'
      '   AND Pedido.nCdTerceiro         = @nCdTerceiro'
      
        '   AND ((DoctoFiscal.iNrDocto     = @iNrDocto)                  ' +
        '         OR (@iNrDocto   = 0))'
      
        '   AND ((Pedido.dDtPedido        >= CONVERT(datetime,@cDtInicial' +
        ',103))   OR (@cDtInicial = '#39'01/01/1900'#39'))'
      
        '   AND ((Pedido.dDtPedido        <  CONVERT(datetime,@cDtFinal,1' +
        '03) + 1) OR (@cDtFinal   = '#39'01/01/1900'#39'))'
      '   AND Pedido.nCdTabStatusPed    IN(4,5)'
      '   AND ItemPedido.nCdTipoItemPed IN(1,2)'
      '   AND DoctoFiscal.dDtImpressao  IS NOT NULL'
      '   AND DoctoFiscal.dDtCancel     IS NULL'
      '   AND DoctoFiscal.nCdStatus      = 1'
      '   AND DoctoFiscal.cFlgEntSai     = '#39'S'#39
      '   AND NOT EXISTS (SELECT 1'
      '                     FROM ItemPedido'
      '                    WHERE nCdPedido            = @nCdPedido'
      
        '                      AND nCdProduto           = ItemPedido.nCdP' +
        'roduto'
      
        '                      AND DoctoFiscal.iNrDocto = CAST(LTRIM(RTRI' +
        'M(cDoctoCompra)) as int))')
    Left = 540
    Top = 233
    object qryConsultaPedidonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryConsultaPedidodDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object qryConsultaPedidocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryConsultaPedidonCdItemPedido: TIntegerField
      FieldName = 'nCdItemPedido'
    end
    object qryConsultaPedidocCdProduto: TStringField
      FieldName = 'cCdProduto'
      Size = 15
    end
    object qryConsultaPedidocNmItem: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryConsultaPedidonCdTipoItemPed: TIntegerField
      FieldName = 'nCdTipoItemPed'
    end
    object qryConsultaPedidonValUnitario: TBCDField
      FieldName = 'nValUnitario'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryConsultaPedidonQtdeAtendida: TBCDField
      FieldName = 'nQtdeAtendida'
      Precision = 12
      Size = 2
    end
    object qryConsultaPedidoiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryConsultaPedidocUFEmitente: TStringField
      FieldName = 'cUFEmitente'
      FixedChar = True
      Size = 2
    end
    object qryConsultaPedidonCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
  end
  object dsConsultaPedido: TDataSource
    DataSet = qryConsultaPedido
    Left = 544
    Top = 264
  end
  object SP_INCLUI_ITEM_DEVOLUCAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_INCLUI_ITEM_DEVOLUCAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNmItem'
        Attributes = [paNullable]
        DataType = ftString
        Size = 150
        Value = Null
      end
      item
        Name = '@cSiglaUnidadeMedida'
        Attributes = [paNullable]
        DataType = ftString
        Size = 3
        Value = Null
      end
      item
        Name = '@nQtdePed'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nValUnitario'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 16
        Value = Null
      end
      item
        Name = '@nValUnitarioEsp'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 4
        Precision = 16
        Value = Null
      end
      item
        Name = '@nPercIPI'
        Attributes = [paNullable]
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Value = Null
      end
      item
        Name = '@nCdItem'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTipoItemPed'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cUFOrigem'
        Attributes = [paNullable]
        DataType = ftString
        Size = 2
        Value = Null
      end>
    Left = 609
    Top = 261
  end
end
