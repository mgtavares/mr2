unit rRankingFatAnual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons, comObj ;

type
  TrptRankingFatAnual = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    Label2: TLabel;
    DataSource3: TDataSource;
    DBEdit7: TDBEdit;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit5: TMaskEdit;
    Label4: TLabel;
    MaskEdit4: TMaskEdit;
    Label5: TLabel;
    MaskEdit6: TMaskEdit;
    Label6: TLabel;
    ComboBox1: TComboBox;
    Label7: TLabel;
    MaskEdit7: TMaskEdit;
    Label8: TLabel;
    RadioGroup1: TRadioGroup;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DBEdit1: TDBEdit;
    DataSource6: TDataSource;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    DBEdit4: TDBEdit;
    DataSource7: TDataSource;
    qryRegiao: TADOQuery;
    qryRegiaonCdRegiao: TIntegerField;
    qryRegiaocNmRegiao: TStringField;
    DBEdit5: TDBEdit;
    DataSource8: TDataSource;
    MaskEdit8: TMaskEdit;
    MaskEdit9: TMaskEdit;
    Label10: TLabel;
    Label9: TLabel;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    DBEdit6: TDBEdit;
    DataSource9: TDataSource;
    DBEdit8: TDBEdit;
    DataSource10: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure MaskEdit9Exit(Sender: TObject);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRankingFatAnual: TrptRankingFatAnual;

implementation

uses fMenu, fLookup_Padrao, QRExport , rRankingFatAnual_view;

{$R *.dfm}

procedure TrptRankingFatAnual.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin

  iAux := frmMenu.nCdEmpresaAtiva;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  if (iAux > 0) then
  begin

    MaskEdit3.Text := IntToStr(iAux) ;

    qryEmpresa.Close ;
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := iAux ;
    qryEmpresa.Open ;

  end ;

  MaskEdit3.SetFocus ;

  inherited;
  
end;


procedure TrptRankingFatAnual.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TrptRankingFatAnual.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryUnidadeNegocio.Close ;

  PosicionaQuery(qryUnidadeNegocio, MaskEdit5.Text) ;

end;

function MesExtenso(dData: TDateTime):string;
begin

    if StrToInt(Copy(DateToStr(dData),4,2)) = 1 then
        result := 'jan/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 2 then
        result := 'fev/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 3 then
        result := 'mar/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 4 then
        result := 'abr/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 5 then
        result := 'mai/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 6 then
        result := 'jun/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 7 then
        result := 'jul/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 8 then
        result := 'ago/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 9 then
        result := 'set/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 10 then
        result := 'out/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 11 then
        result := 'nov/' + Copy(DateToStr(dData),9,2) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 12 then
        result := 'dez/' + Copy(DateToStr(dData),9,2) ;

end ;

procedure TrptRankingFatAnual.ToolButton1Click(Sender: TObject);
var
    objRel : TrptRankingFatAnual_view;
    dAux   : TDateTime ;
    dAux1, dAux2, dAux3, dAux4, dAux5, dAux6 : TDateTime ;
    dAux7, dAux8, dAux9, dAux10, dAux11, dAux12 : TDateTime ;

var linha, coluna : integer;
var planilha : variant;
var valorcampo : string;

begin

  If (Trim(MaskEdit3.Text) = '') or (DBEDit3.Text = '') Then
  begin
      ShowMessage('Informe a Empresa.') ;
      exit ;
  end ;

  if (Length(Trim(MaskEdit1.Text)) <> 7) then
  begin
      ShowMessage('Compet�ncia Inicial Inv�lida.') ;
      exit ;
  end ;

  try
      dAux := StrToDateTime('01/' + MaskEdit1.Text) ;
  except
      ShowMessage('Compet�ncia Inicial Inv�lida.') ;
      exit ;
  end ;

  dAux1 := StrToDateTime('01/' + MaskEdit1.Text) ;
  dAux2 := dAux1  + 31 ;
  dAux3 := dAux2  + 31 ;
  dAux4 := dAux3  + 31 ;
  dAux5 := dAux4  + 31 ;
  dAux6 := dAux5  + 31 ;
  dAux7 := dAux6  + 31 ;
  dAux8 := dAux7  + 31 ;
  dAux9 := dAux8  + 31 ;
  dAux10:= dAux9  + 31 ;
  dAux11:= dAux10 + 31 ;
  dAux12:= dAux11 + 31 ;

  objRel := TrptRankingFatAnual_view.Create(nil);

  Try
      Try
          objRel.lblmes1.Caption  := MesExtenso(dAux1) ;
          objRel.lblmes2.Caption  := MesExtenso(dAux2) ;
          objRel.lblmes3.Caption  := MesExtenso(dAux3) ;
          objRel.lblmes4.Caption  := MesExtenso(dAux4) ;
          objRel.lblmes5.Caption  := MesExtenso(dAux5) ;
          objRel.lblmes6.Caption  := MesExtenso(dAux6) ;
          objRel.lblmes7.Caption  := MesExtenso(dAux7) ;
          objRel.lblmes8.Caption  := MesExtenso(dAux8) ;
          objRel.lblmes9.Caption  := MesExtenso(dAux9) ;
          objRel.lblmes10.Caption := MesExtenso(dAux10) ;
          objRel.lblmes11.Caption := MesExtenso(dAux11) ;
          objRel.lblmes12.Caption := MesExtenso(dAux12) ;

          objRel.usp_Relatorio.Close ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdUnidadeNegocio').Value := frmMenu.ConvInteiro(MaskEdit5.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdProduto').Value        := frmMenu.ConvInteiro(MaskEdit4.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdLinha').Value          := frmMenu.ConvInteiro(MaskEdit6.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdRegiao').Value         := frmMenu.ConvInteiro(MaskEdit7.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@cUF').Value               := Copy(ComboBox1.Text,1,2) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@cMesAnoIni').Value        := Trim(MaskEdit1.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@cMesAnoFim').Value        := Trim(MaskEdit1.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdGrupoEconomico').Value := frmMenu.ConvInteiro(MaskEdit8.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdTerceiroAux').Value    := frmMenu.ConvInteiro(MaskEdit9.Text) ;

          If (RadioGroup1.ItemIndex = 0) then
              objRel.usp_Relatorio.Parameters.ParamByName('@cFlgTipo').Value := 'V' ;

          If (RadioGroup1.ItemIndex = 1) then
              objRel.usp_Relatorio.Parameters.ParamByName('@cFlgTipo').Value := 'Q' ;

          objRel.usp_Relatorio.Open;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := 'Empresa: ' + Trim(DbEdit3.Text) ;

          if (dbEdit7.Text <> '') then objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Unid. Negocio: ' + Trim(DbEdit7.Text) ;

          objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Compet�ncia Inicial: ' + MaskEdit1.Text ;

          if (dbEdit6.Text <> '') then objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Grupo Econ�mico: ' + Trim(DbEdit6.Text) ;
          if (dbEdit8.Text <> '') then objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Cliente: ' + Trim(DbEdit8.Text) ;

          if (dbEdit1.Text <> '') then objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Produto: ' + Trim(DbEdit1.Text) ;
          if (dbEdit4.Text <> '') then objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Linha: ' + Trim(DbEdit4.Text) ;
          if (dbEdit5.Text <> '') then objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Regi�o: ' + Trim(DbEdit5.Text) ;
          if (Trim(ComboBox1.Text) <> '') then objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Estado: ' + Trim(ComboBox1.Text) ;

          If (RadioGroup1.ItemIndex = 0) then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Modo: VALOR ' ;

          If (RadioGroup1.ItemIndex = 1) then
              objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Modo: QUANTIDADE ' ;

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do Relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;


procedure TrptRankingFatAnual.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptRankingFatAnual.cxButton1Click(Sender: TObject);
begin
  inherited;
  //rptDRE_View.QuickRep1.ExportToFilter(TQRXLSFilter.Create('c:\teste.xls'));
end;

procedure TrptRankingFatAnual.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(106);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryProduto, MaskEdit4.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRankingFatAnual.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLinha, MaskEdit6.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRankingFatAnual.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(21);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
            PosicionaQuery(qryRegiao, MaskEdit7.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRankingFatAnual.MaskEdit7Exit(Sender: TObject);
begin
  inherited;

  qryRegiao.Close ;
  PosicionaQuery(qryRegiao, Maskedit7.Text) ;

end;

procedure TrptRankingFatAnual.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryLinha.Close ;
  PosicionaQuery(qryLinha, Maskedit6.Text) ;

end;

procedure TrptRankingFatAnual.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryProduto.Close ;
  PosicionaQuery(qryProduto, Maskedit4.Text) ;

end;

procedure TrptRankingFatAnual.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(14);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryUnidadeNegocio, MaskEdit5.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRankingFatAnual.MaskEdit8Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close ;
  PosicionaQuery(qryGrupoEconomico, MaskEdit8.Text) ;

end;

procedure TrptRankingFatAnual.MaskEdit9Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;
  PosicionaQuery(qryTerceiro, MaskEdit9.Text) ;

end;

procedure TrptRankingFatAnual.MaskEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, MaskEdit8.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptRankingFatAnual.MaskEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(101);

        If (nPK > 0) then
        begin
            Maskedit9.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, MaskEdit9.Text) ;
        end ;

    end ;

  end ;

end;

initialization
     RegisterClass(TrptRankingFatAnual) ;

end.
