inherited rptPosicaoEstoqueGeral: TrptPosicaoEstoqueGeral
  Left = 128
  Top = 123
  Caption = 'Rel. Posi'#231#227'o Estoque'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 46
    Top = 44
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 21
    Top = 68
    Width = 66
    Height = 13
    Alignment = taRightJustify
    Caption = 'Local Estoque'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 17
    Top = 92
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Produto'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 5
    Top = 116
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Invent'#225'rio'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 18
    Top = 140
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 40
    Top = 164
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 22
    Top = 188
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'SubCategoria'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 39
    Top = 212
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Segmento'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 49
    Top = 236
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
    FocusControl = DBEdit9
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object RadioGroup1: TRadioGroup [11]
    Left = 16
    Top = 256
    Width = 161
    Height = 49
    Caption = 'Exibir Valores'
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'N'#227'o'
      'Sim')
    TabOrder = 19
  end
  object RadioGroup2: TRadioGroup [12]
    Left = 184
    Top = 256
    Width = 345
    Height = 49
    Caption = 'M'#233'todo de Valoriza'#231#227'o'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Ultimo Pre'#231'o de Custo'
      'Pre'#231'o M'#233'dio')
    TabOrder = 20
  end
  object DBEdit1: TDBEdit [13]
    Tag = 1
    Left = 161
    Top = 36
    Width = 534
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [14]
    Tag = 1
    Left = 161
    Top = 60
    Width = 534
    Height = 21
    DataField = 'cNmLocalEstoque'
    DataSource = dsLocalEstoque
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [15]
    Tag = 1
    Left = 161
    Top = 84
    Width = 534
    Height = 21
    DataField = 'cNmGrupoProduto'
    DataSource = dsGrupoProduto
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [16]
    Tag = 1
    Left = 161
    Top = 108
    Width = 534
    Height = 21
    DataField = 'cNmGrupoInventario'
    DataSource = dsGrupoInventario
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [17]
    Tag = 1
    Left = 161
    Top = 132
    Width = 534
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = dsDepartamento
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [18]
    Tag = 1
    Left = 161
    Top = 156
    Width = 534
    Height = 21
    DataField = 'cNmCategoria'
    DataSource = dsCategoria
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [19]
    Tag = 1
    Left = 161
    Top = 180
    Width = 534
    Height = 21
    DataField = 'cNmSubCategoria'
    DataSource = dsSubCategoria
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [20]
    Tag = 1
    Left = 161
    Top = 204
    Width = 534
    Height = 21
    DataField = 'cNmSegmento'
    DataSource = dsSegmento
    TabOrder = 8
  end
  object DBEdit9: TDBEdit [21]
    Tag = 1
    Left = 161
    Top = 228
    Width = 534
    Height = 21
    DataField = 'cNmProduto'
    DataSource = dsProduto
    TabOrder = 9
  end
  object edtEmpresa: TER2LookupMaskEdit [22]
    Left = 93
    Top = 36
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 10
    Text = '         '
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  object edtLocalEstoque: TER2LookupMaskEdit [23]
    Left = 93
    Top = 60
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 11
    Text = '         '
    CodigoLookup = 87
    QueryLookup = qryLocalEstoque
  end
  object edtGrupoProduto: TER2LookupMaskEdit [24]
    Left = 93
    Top = 84
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 12
    Text = '         '
    CodigoLookup = 69
    QueryLookup = qryGrupoProdutos
  end
  object edtGrupoInventario: TER2LookupMaskEdit [25]
    Left = 93
    Top = 108
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 13
    Text = '         '
    CodigoLookup = 153
    QueryLookup = qryGrupoInventario
  end
  object edtDepartamento: TER2LookupMaskEdit [26]
    Left = 93
    Top = 132
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 14
    Text = '         '
    CodigoLookup = 129
    QueryLookup = qryDepartamento
  end
  object edtCategoria: TER2LookupMaskEdit [27]
    Left = 93
    Top = 156
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 15
    Text = '         '
    OnBeforeLookup = edtCategoriaBeforeLookup
    CodigoLookup = 46
    QueryLookup = qryCategoria
  end
  object edtSubCategoria: TER2LookupMaskEdit [28]
    Left = 93
    Top = 180
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 16
    Text = '         '
    OnBeforeLookup = edtSubCategoriaBeforeLookup
    CodigoLookup = 47
    QueryLookup = qrySubCategoria
  end
  object edtSegmento: TER2LookupMaskEdit [29]
    Left = 93
    Top = 204
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 17
    Text = '         '
    OnBeforeLookup = edtSegmentoBeforeLookup
    CodigoLookup = 48
    QueryLookup = qrySegmento
  end
  object edtProduto: TER2LookupMaskEdit [30]
    Left = 93
    Top = 228
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 18
    Text = '         '
    CodigoLookup = 76
    WhereAdicional.Strings = (
      
        'NOT EXISTS(SELECT  1 FROM Produto Filho WHERE Filho.nCdProdutoPa' +
        'i = Produto.nCdProduto)')
    QueryLookup = qryProduto
  end
  object RadioGroup3: TRadioGroup [31]
    Left = 536
    Top = 256
    Width = 161
    Height = 49
    Caption = 'Somente Estoque Negativo'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'N'#227'o'
      'Sim')
    TabOrder = 21
  end
  object RadioGroup4: TRadioGroup [32]
    Left = 16
    Top = 312
    Width = 194
    Height = 49
    Caption = 'Status do produto'
    Columns = 3
    ItemIndex = 1
    Items.Strings = (
      'Ambos'
      'Ativo'
      'Inativo')
    TabOrder = 22
  end
  object RadioGroup5: TRadioGroup [33]
    Left = 376
    Top = 312
    Width = 153
    Height = 49
    Caption = 'Modo Exibi'#231#227'o'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Relat'#243'rio'
      'Planilha')
    TabOrder = 23
  end
  object RadioGroup6: TRadioGroup [34]
    Left = 216
    Top = 312
    Width = 153
    Height = 49
    Caption = ' N'#237'vel do Produto '
    Columns = 3
    ItemIndex = 2
    Items.Strings = (
      '1'
      '2'
      '3')
    TabOrder = 24
  end
  inherited ImageList1: TImageList
    Left = 832
    Top = 200
  end
  object qryGrupoProdutos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto'
      '      ,cNmGrupoProduto '
      '  FROM GrupoProduto'
      ' WHERE nCdGrupoProduto = :nPK')
    Left = 104
    Top = 392
    object qryGrupoProdutosnCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutoscNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
  object qryGrupoInventario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdGrupoInventario'
      '      ,cNmGrupoInventario '
      '  FROM GrupoInventario'
      ' WHERE nCdGrupoInventario = :nPK')
    Left = 144
    Top = 392
    object qryGrupoInventarionCdGrupoInventario: TIntegerField
      FieldName = 'nCdGrupoInventario'
    end
    object qryGrupoInventariocNmGrupoInventario: TStringField
      FieldName = 'cNmGrupoInventario'
      Size = 50
    end
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento'
      '      ,cNmDepartamento '
      '  FROM Departamento'
      ' WHERE nCdDepartamento = :nPK')
    Left = 184
    Top = 392
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque'
      '      ,cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK')
    Left = 64
    Top = 392
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdDepartamento'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdCategoria'
      '      ,cNmCategoria '
      '  FROM Categoria'
      ' WHERE nCdCategoria    = :nPK'
      '   AND nCdDepartamento = :nCdDepartamento')
    Left = 224
    Top = 392
    object qryCategorianCdCategoria: TAutoIncField
      FieldName = 'nCdCategoria'
      ReadOnly = True
    end
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdCategoria'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdSubCategoria'
      '      ,cNmSubCategoria '
      '  FROM SubCategoria'
      ' WHERE nCdSubCategoria = :nPK'
      '   AND nCdCategoria    = :nCdCategoria')
    Left = 264
    Top = 392
    object qrySubCategorianCdSubCategoria: TAutoIncField
      FieldName = 'nCdSubCategoria'
      ReadOnly = True
    end
    object qrySubCategoriacNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdSubCategoria'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdSegmento'
      '      ,cNmSegmento '
      '  FROM Segmento'
      ' WHERE nCdSegmento     = :nPK'
      '   AND nCdSubCategoria = :nCdSubCategoria')
    Left = 304
    Top = 392
    object qrySegmentonCdSegmento: TAutoIncField
      FieldName = 'nCdSegmento'
      ReadOnly = True
    end
    object qrySegmentocNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE nCdEmpresa = :nPK'
      '   AND EXISTS(SELECT 1 '
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa)')
    Left = 24
    Top = 392
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto'
      '      ,cNmProduto '
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK')
    Left = 344
    Top = 392
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 40
    Top = 424
  end
  object dsLocalEstoque: TDataSource
    DataSet = qryLocalEstoque
    Left = 80
    Top = 424
  end
  object dsGrupoProduto: TDataSource
    DataSet = qryGrupoProdutos
    Left = 120
    Top = 424
  end
  object dsGrupoInventario: TDataSource
    DataSet = qryGrupoInventario
    Left = 160
    Top = 424
  end
  object dsDepartamento: TDataSource
    DataSet = qryDepartamento
    Left = 200
    Top = 424
  end
  object dsCategoria: TDataSource
    DataSet = qryCategoria
    Left = 240
    Top = 424
  end
  object dsSubCategoria: TDataSource
    DataSet = qrySubCategoria
    Left = 280
    Top = 424
  end
  object dsSegmento: TDataSource
    DataSet = qrySegmento
    Left = 320
    Top = 424
  end
  object dsProduto: TDataSource
    DataSet = qryProduto
    Left = 360
    Top = 424
  end
  object ER2Excel1: TER2Excel
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 560
    Top = 328
  end
end
