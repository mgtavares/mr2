unit fGrupoImposto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  DBGridEhGrouping, ToolCtrlsEh, ER2Lookup;

type
  TfrmGrupoImposto = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoImposto: TIntegerField;
    qryMastercNmGrupoImposto: TStringField;
    qryMastercNCM: TStringField;
    qryMastercClasseFiscal: TStringField;
    qryMastercCdST: TStringField;
    qryMastercUnidadeMedida: TStringField;
    qryMasternCdTipoICMS: TIntegerField;
    qryMasternAliqIPI: TBCDField;
    qryTipoICMS: TADOQuery;
    qryTipoICMSnCdTipoICMS: TIntegerField;
    qryTipoICMScNmTipoICMS: TStringField;
    qryMastercNmTipoICMS: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    DBEdit9: TDBEdit;
    Label9: TLabel;
    Label10: TLabel;
    qryGrupoImpostoEstrutura: TADOQuery;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryCategoria: TADOQuery;
    qryCategorianCdCategoria: TAutoIncField;
    qryCategoriacNmCategoria: TStringField;
    qrySubCategoria: TADOQuery;
    qrySubCategorianCdSubCategoria: TAutoIncField;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySegmento: TADOQuery;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentocNmSegmento: TStringField;
    qryGrupoImpostoEstruturanCdGrupoImpostoProduto: TAutoIncField;
    qryGrupoImpostoEstruturanCdGrupoImposto: TIntegerField;
    qryGrupoImpostoEstruturanCdDepartamento: TIntegerField;
    qryGrupoImpostoEstruturanCdCategoria: TIntegerField;
    qryGrupoImpostoEstruturanCdSubCategoria: TIntegerField;
    qryGrupoImpostoEstruturanCdSegmento: TIntegerField;
    qryGrupoImpostoEstruturanCdProduto: TIntegerField;
    qryGrupoImpostoEstruturacNmDepartamento: TStringField;
    qryGrupoImpostoEstruturacNmCategoria: TStringField;
    qryGrupoImpostoEstruturacNmSubCategoria: TStringField;
    qryGrupoImpostoEstruturacNmSegmento: TStringField;
    dsGrupoImpostoEstrutura: TDataSource;
    DBGridEh2: TDBGridEh;
    qryGrupoImpostoProduto: TADOQuery;
    qryGrupoImpostoProdutonCdGrupoImposto: TIntegerField;
    qryGrupoImpostoProdutonCdProduto: TIntegerField;
    dsGrupoImpostoProduto: TDataSource;
    qryGrupoImpostoProdutocNmProduto: TStringField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryMastercCdSTIPI: TStringField;
    qryMasternCdTabTipoOrigemMercadoria: TIntegerField;
    qryTabTipoOrigemMercadoria: TADOQuery;
    qryTabTipoOrigemMercadorianCdTabTipoOrigemMercadoria: TIntegerField;
    qryTabTipoOrigemMercadoriacNmTabTipoOrigemMercadoria: TStringField;
    ComboTipoMercadoria: TDBLookupComboBox;
    dsTabTipoOrigemMercadoria: TDataSource;
    Label12: TLabel;
    qryGrupoImpostoProdutonCdGrupoImpostoProduto: TIntegerField;
    qryMastercFlgRetemPisCofins: TIntegerField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMastercEXTIPI: TStringField;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    qryTabIBPT: TADOQuery;
    dsTabIBPT: TDataSource;
    qryTabIBPTnCdTabIBPT: TIntegerField;
    qryTabIBPTcNCM: TStringField;
    qryTabIBPTiTabela: TIntegerField;
    qryTabIBPTcDescricao: TStringField;
    qryTabIBPTnAliqNacional: TBCDField;
    qryTabIBPTnAliqImportado: TBCDField;
    qryTabIBPTcFlgManual: TIntegerField;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label13: TLabel;
    DBEdit12: TDBEdit;
    ER2LookupDBEdit1: TER2LookupDBEdit;
    qryMasternCdTabIBPT: TIntegerField;
    Label14: TLabel;
    DBEdit13: TDBEdit;
    Label3: TLabel;
    qryTabIBPTnAliqEstadual: TBCDField;
    qryTabIBPTnAliqMunicipal: TBCDField;
    Label15: TLabel;
    DBEdit3: TDBEdit;
    Label16: TLabel;
    DBEdit14: TDBEdit;
    qryMastercCEST: TStringField;
    Label17: TLabel;
    DBEdit15: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btIncluirClick(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryGrupoImpostoEstruturaBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryGrupoImpostoEstruturaCalcFields(DataSet: TDataSet);
    procedure qryGrupoImpostoProdutoCalcFields(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryGrupoImpostoProdutoBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryTabIBPTAfterOpen(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure ER2LookupDBEdit1Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoImposto: TfrmGrupoImposto;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmGrupoImposto.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOIMPOSTO' ;
  nCdTabelaSistema  := 34 ;
  nCdConsultaPadrao := 73 ;
end;

procedure TfrmGrupoImposto.DBEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(72);

            If (nPK > 0) then
            begin
                qryMasternCdTipoICMS.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmGrupoImposto.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmGrupoImposto.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryGrupoImpostoEstrutura.State = dsBrowse) then
             qryGrupoImpostoEstrutura.Edit ;

        if (qryGrupoImpostoEstrutura.State = dsInsert) or (qryGrupoImpostoEstrutura.State = dsEdit) then
        begin

            // departamento
            if (DBGridEh1.Col = 3) then
            begin
              nPK := frmLookup_Padrao.ExecutaConsulta(45);

              If (nPK > 0) then
              begin
                qryGrupoImpostoEstruturanCdDepartamento.Value := nPK ;

                PosicionaQuery(qryDepartamento,IntToStr(nPK)) ;

                if not qryDepartamento.eof then
                    qryGrupoImpostoEstruturacNmDepartamento.Value := qryDepartamentocNmDepartamento.Value ;

              end ;

            end ;

            // categoria
            if (DBGridEh1.Col = 5) then
            begin

                if not qryDepartamento.active or (qryDepartamentonCdDepartamento.Value = 0) then
                begin
                    ShowMessage('Selecione um departamento.') ;
                    DBGridEh1.Col := 3 ;
                    exit ;
                end ;

                nPK := frmLookup_Padrao.ExecutaConsulta2(46,'nCdStatus = 1 and nCdDepartamento = ' + qryDepartamentonCdDepartamento.asString);

                If (nPK > 0) then
                begin
                    qryGrupoImpostoEstruturanCdCategoria.Value := nPK ;
                    qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryGrupoImpostoEstruturanCdDepartamento.Value ;
                    PosicionaQuery(qryCategoria, IntToStr(nPK)) ;

                    if not qryCategoria.eof then
                        qryGrupoImpostoEstruturacNmCategoria.Value := qryCategoriacNmCategoria.Value ;
                end ;

            end ;

            // subcategoria
            if (DBGridEh1.Col = 7) then
            begin

                if not qryCategoria.active or (qryCategorianCdCategoria.Value = 0) then
                begin
                    ShowMessage('Selecione uma categoria.') ;
                    DBGridEh1.Col := 5 ;
                    exit ;
                end ;

                nPK := frmLookup_Padrao.ExecutaConsulta2(47,'nCdStatus = 1 and nCdCategoria = ' + qryCategorianCdCategoria.asString);
                If (nPK > 0) then
                begin
                    qryGrupoImpostoEstruturanCdSubCategoria.Value := nPK ;
                    qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryGrupoImpostoEstruturanCdCategoria.Value ;
                    PosicionaQuery(qrySubCategoria, IntToStr(nPK)) ;

                    if not qrySubCategoria.eof then
                        qryGrupoImpostoEstruturacNmSubCategoria.Value := qrySubCategoriacNmSubCategoria.Value ;
                end ;

            end ;

            // segmento
            if (DBGridEh1.Col = 9) then
            begin

                if not qrySubCategoria.active or (qrySubCategorianCdSubCategoria.Value = 0) then
                begin
                    ShowMessage('Selecione uma subcategoria.') ;
                    DBGridEh1.Col := 7 ;
                    exit ;
                end ;

                nPK := frmLookup_Padrao.ExecutaConsulta2(48,'nCdStatus = 1 and nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.asString);
                If (nPK > 0) then
                begin
                    qryGrupoImpostoEstruturanCdSegmento.Value := nPK ;
                    qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qryGrupoImpostoEstruturanCdSegmento.Value ;
                    PosicionaQuery(qrySegmento, IntToStr(nPK)) ;

                    if not qrySegmento.eof then
                        qryGrupoImpostoEstruturacNmSegmento.Value := qrySegmentocNmSegmento.Value ;

                end ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmGrupoImposto.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryGrupoImpostoEstrutura, qryMasternCdGrupoImposto.asString) ;
  PosicionaQuery(qryGrupoImpostoProduto, qryMasternCdGrupoImposto.asString) ;
  PosicionaQuery(qryTabIBPT, qryMasternCdTabIBPT.asString) ;
  cxPageControl1.ActivePageIndex := 0 ;
  
end;

procedure TfrmGrupoImposto.qryGrupoImpostoEstruturaBeforePost(
  DataSet: TDataSet);
begin

  if (qryGrupoImpostoEstruturanCdDepartamento.Value = 0) then
  begin
      ShowMessage('Selecione o departamento.') ;
      DBGridEh1.Col := 3 ;
      abort ;
  end ;

  qryGrupoImpostoEstruturanCdGrupoImposto.Value := qryMasternCdGrupoImposto.Value ;

  if (qryGrupoImpostoEstrutura.State = dsInsert) then
      qryGrupoImpostoEstruturanCdGrupoImpostoProduto.Value := frmMenu.fnProximoCodigo('GRUPOIMPOSTOPRODUTO') ;

  inherited;

end;

procedure TfrmGrupoImposto.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
    qryGrupoImpostoEstrutura.Close ;
    qryGrupoImpostoProduto.Close ;
    qryTabIBPT.Close;
end;

procedure TfrmGrupoImposto.qryGrupoImpostoEstruturaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryDepartamento, qryGrupoImpostoEstruturanCdDepartamento.asString) ;

  if not qryDepartamento.eof then
      qryGrupoImpostoEstruturacNmDepartamento.Value := qryDepartamentocNmDepartamento.Value ;

  if qryGrupoImpostoEstruturanCdCategoria.Value > 0 then
  begin

      qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := qryDepartamentonCdDepartamento.Value ;
      PosicionaQuery(qryCategoria,qryGrupoImpostoEstruturanCdCategoria.AsString) ;

      if not qryCategoria.Eof then
          qryGrupoImpostoEstruturacNmCategoria.Value := qryCategoriacNmCategoria.Value ;

  end ;

  if qryGrupoImpostoEstruturanCdSubCategoria.Value > 0 then
  begin

      qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := qryCategorianCdCategoria.Value ;
      PosicionaQuery(qrySubCategoria,qryGrupoImpostoEstruturanCdSubCategoria.AsString) ;

      if not qrySubCategoria.Eof then
          qryGrupoImpostoEstruturacNmSubCategoria.Value := qrySubCategoriacNmSubCategoria.Value ;

  end ;

  if qryGrupoImpostoEstruturanCdSegmento.Value > 0 then
  begin

      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value ;
      PosicionaQuery(qrySegmento,qryGrupoImpostoEstruturanCdSegmento.AsString) ;

      if not qrySegmento.Eof then
          qryGrupoImpostoEstruturacNmSegmento.Value := qrySegmentocNmSegmento.Value ;

  end ;

end;

procedure TfrmGrupoImposto.qryGrupoImpostoProdutoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryProduto, qryGrupoImpostoProdutonCdProduto.AsString) ;

  if not qryProduto.eof then
      qryGrupoImpostoProdutocNmProduto.Value := qryProdutocNmProduto.Value ;

end;

procedure TfrmGrupoImposto.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryGrupoImpostoProduto.State = dsBrowse) then
             qryGrupoImpostoProduto.Edit ;

        if (qryGrupoImpostoProduto.State = dsInsert) or (qryGrupoImpostoProduto.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(76);

            If (nPK > 0) then
            begin
                qryGrupoImpostoProdutonCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmGrupoImposto.qryGrupoImpostoProdutoBeforePost(
  DataSet: TDataSet);
begin
  qryGrupoImpostoProdutonCdGrupoImposto.Value := qryMasternCdGrupoImposto.Value ;

  if (qryGrupoImpostoProduto.State = dsInsert) then
      qryGrupoImpostoProdutonCdGrupoImpostoProduto.Value := frmMenu.fnProximoCodigo('GRUPOIMPOSTOPRODUTO') ;
      
  inherited;

end;

procedure TfrmGrupoImposto.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryGrupoImpostoEstrutura.Close ;
  qryGrupoImpostoProduto.Close ;
  qryTabIBPT.Close;

end;

procedure TfrmGrupoImposto.FormShow(Sender: TObject);
begin
  inherited;

  qryTabTipoOrigemMercadoria.Close;
  qryTabTipoOrigemMercadoria.Open;
  
end;

procedure TfrmGrupoImposto.qryTabIBPTAfterOpen(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State in[dsInsert,dsEdit]) then
      qryMastercNCM.Value := qryTabIBPTcNCM.Value;
end;

procedure TfrmGrupoImposto.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if ((qryTabIBPT.Eof) and (Trim(ER2LookupDBEdit1.Text) <> '')) then
  begin
      MensagemAlerta('C�digo NCM inv�lido.');
      ER2LookupDBEdit1.SetFocus;
      Abort;
  end;

  if (Trim(qryMastercUnidadeMedida.Value) = '') then
  begin
      MensagemAlerta('Informe a Unidade de Medida.');
      DBEdit6.SetFocus;
      Abort;
  end;

  { -- atribui NCM para o campo da tabela GrupoImposto -- }
  if (not qryTabIBPT.IsEmpty) then
      qryMastercNCM.Value := qryTabIBPTcNCM.Value;
end;

procedure TfrmGrupoImposto.ER2LookupDBEdit1Exit(Sender: TObject);
begin
  inherited;

  //if (qryTabIBPT.IsEmpty) then
  //    qryMastercNCM.Value := '';
end;

initialization
    RegisterClass(TfrmGrupoImposto) ;

end.
