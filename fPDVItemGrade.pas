unit fPDVItemGrade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, GridsEh, DBGridEh, ImgList,
  ComCtrls, ToolWin, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, jpeg, Menus;

type
  TfrmPDVItemGrade = class(TfrmProcesso_Padrao)
    qryItemGrade: TADOQuery;
    qryItemGradecNmTamanho: TStringField;
    qryItemGradenCdProduto: TIntegerField;
    dsItemGrade: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1cNmTamanho: TcxGridDBColumn;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    PopupMenu1: TPopupMenu;
    PosioEstoque1: TMenuItem;
    qryItemGradecEAN: TStringField;
    qryItemGradenQtdeEstoque: TIntegerField;
    cxGrid1DBTableView1cEAN: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeEstoque: TcxGridDBColumn;
    procedure FormShow(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure PosioEstoque1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPDVItemGrade: TfrmPDVItemGrade;

implementation

uses fProdutoPosicaoEstoque;

{$R *.dfm}

procedure TfrmPDVItemGrade.FormShow(Sender: TObject);
var
  Msg: TMsg;
begin
  inherited;

end;

procedure TfrmPDVItemGrade.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  inherited; }

end;

procedure TfrmPDVItemGrade.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;
  case key of
      #13 : close() ;
  end ;

end;

procedure TfrmPDVItemGrade.PosioEstoque1Click(Sender: TObject);
begin

  if not (qryItemGrade.eof) then
  begin
      PosicionaQuery(frmProdutoPosicaoEstoque.qryPosicaoEstoque, qryItemGradenCdProduto.AsString) ;

      if (frmProdutoPosicaoEstoque.qryPosicaoEstoque.eof) then
          ShowMessage('Nenhuma posi��o de estoque encontrada para o produto.')
      else frmProdutoPosicaoEstoque.ShowModal;
  end ;

end;

end.
