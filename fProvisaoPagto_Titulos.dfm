inherited frmProvisaoPagto_Titulos: TfrmProvisaoPagto_Titulos
  Left = 7
  Top = 26
  Width = 1152
  Height = 780
  BorderIcons = [biSystemMenu]
  Caption = 'Provis'#227'o de Pagamento - T'#237'tulos'
  OldCreateOrder = True
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 65
    Width = 1136
    Height = 397
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object Panel1: TPanel [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 36
    Align = alTop
    TabOrder = 1
    object cxButton1: TcxButton
      Left = 8
      Top = 0
      Width = 129
      Height = 33
      Caption = 'Gerar Provis'#227'o'
      TabOrder = 0
      OnClick = cxButton1Click
      Glyph.Data = {
        6E040000424D6E04000000000000360000002800000013000000120000000100
        1800000000003804000000000000000000000000000000000000C6D6DEC6D6DE
        C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
        DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6
        D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE0000000000000000008C9C9CC6D6DE
        C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DEC6DEC6C6D6DEC6D6
        DEC6D6DEC6D6DEC6D6DE00000018A54A18A54A18A54A0000008C9C9CC6D6DEC6
        D6DEC6D6DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
        00000000000018A54A18A54A18A54A18A54A18A54A0000008C9C9CC6D6DEC6D6
        DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6D6DE0000000000000000006B
        D6BD18A54A6BD6BD0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE
        C6D6DE000000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFF0000006BD6
        BD000000EFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE00
        0000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFFEFFFFF000000EFFFFF
        EFFFFFEFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DE000000C6DE
        C6C6D6DE000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFBDBDBDEFFFFFEF
        FFFFEFFFFF0000006BD6BD18A54A18A54A000000C6D6DE000000C6D6DEC6D6DE
        000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDBDBDBDBDBDBDBDBD
        BDEFFFFF0000006BD6BD18A54A000000C6D6DE000000C6D6DEC6D6DE000000BD
        BDBDEFFFFFEFFFFFEFFFFF000000000000000000000000000000EFFFFFEFFFFF
        000000000000000000C6D6DEC6D6DE000000C6D6DEC6D6DE000000BDBDBDEFFF
        FFEFFFFFEFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6
        D6DEC6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DE000000BDBDBDEFFFFFEFFFFF
        EFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6
        DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DE000000BDBDBDEFFFFFEFFFFFEF
        FFFF000000BDBDBDEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6DEC6D6DEC6D6DE
        C6D6DE000000C6D6DEC6DEC6C6D6DE000000BDBDBDBDBDBDEFFFFFEFFFFF0000
        00EFFFFFEFFFFFDEBDD6DEBDD6000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE00
        0000C6D6DEC6D6DEC6D6DEC6D6DE000000000000BDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBD000000000000C6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DE000000C6D6
        DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000000000000000000000C6
        D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DE
        C6DEC6C6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
        DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6DEC6000000C6DEC6C6D6DEC6D6DEC6
        D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
        C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000}
      LookAndFeel.NativeStyle = True
    end
    object cxButton2: TcxButton
      Left = 144
      Top = 0
      Width = 129
      Height = 33
      Caption = 'Agregar Provis'#227'o'
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        A6020000424DA60200000000000036000000280000000F0000000D0000000100
        1800000000007002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFF0000000000000000000000000000000000000000000000000000000000
        00000000000000000000FFFFFFFFFFFFFFFFFF000000FF9C31000000BDBDBDEF
        FFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000000000000000
        000000000000F7CE8CFF9C31000000A56300FF9C31FF9C31FF9C31FF9C31FF9C
        31FF9C31000000000000000000F7CE8CF7CE8CF7CE8CF7CE8CF7CE8CFF9C3100
        0000A56300EFFFFFEFFFFFEFFFFFEFFFFFFF9C31000000000000000000FFEFDE
        FFEFDEF7CE8CF7CE8CF7CE8CF7CE8CFF9C31000000FF9C31FF9C31FF9C31FF9C
        31FF9C31000000000000000000EFFFFFEFFFFFEFDED6F7CE8CF7CE8CFFEFDE00
        0000A56300EFFFFFEFFFFFEFFFFFEFFFFFFF9C31000000000000000000000000
        000000000000F7CE8CEFDED6000000A56300FF9C31FF9C31FF9C31FF9C31FF9C
        31FF9C31000000000000FFFFFFFFFFFFFFFFFF000000FFEFDE000000BDBDBDEF
        FFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000FFFFFFFFFFFF
        FFFFFF000000000000000000EFFFFFA5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5A5
        A5EFFFFF000000000000FFFFFFFFFFFFFFFFFF0000008C9C9C000000EFFFFFEF
        FFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF000000BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD
        BDBDBDBD000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000BDBDBDBD
        BDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBDBD000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFF0000000000000000000000000000000000000000000000
        00000000000000000000}
      LookAndFeel.NativeStyle = True
    end
  end
  object Panel2: TPanel [3]
    Left = 0
    Top = 65
    Width = 1136
    Height = 397
    Align = alClient
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 1134
      Height = 395
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dsTemp
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsCustomize.ColumnHiding = True
        OptionsCustomize.ColumnMoving = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsView.GridLines = glVertical
        OptionsView.GroupByBox = False
        Styles.Selection = frmMenu.LinhaAzul
        Styles.Header = frmMenu.Header
        object cxGrid1DBTableView1cFlgOK: TcxGridDBColumn
          Caption = 'OK'
          DataBinding.FieldName = 'cFlgOK'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.DisplayChecked = '1'
          Properties.DisplayUnchecked = '0'
          Properties.NullStyle = nssUnchecked
          Properties.ValueChecked = '1'
          Properties.ValueUnchecked = '0'
          Properties.OnEditValueChanged = cxGrid1DBTableView1cFlgOKPropertiesEditValueChanged
          Width = 37
        end
        object cxGrid1DBTableView1cNmLoja: TcxGridDBColumn
          Caption = 'Loja'
          DataBinding.FieldName = 'cNmLoja'
          Options.Editing = False
          Width = 61
        end
        object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
          Caption = 'Dt. Venc.'
          DataBinding.FieldName = 'dDtVenc'
          Options.Editing = False
          Width = 90
        end
        object cxGrid1DBTableView1cNrNF: TcxGridDBColumn
          Caption = 'N'#250'm. NF'
          DataBinding.FieldName = 'cNrNF'
          Options.Editing = False
          Width = 64
        end
        object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
          Caption = 'N'#250'm. T'#237'tulo'
          DataBinding.FieldName = 'cNrTit'
          Options.Editing = False
          Width = 91
        end
        object cxGrid1DBTableView1iParcela: TcxGridDBColumn
          Caption = 'Parc.'
          DataBinding.FieldName = 'iParcela'
          Options.Editing = False
          Width = 53
        end
        object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
          Caption = 'Saldo T'#237'tulo'
          DataBinding.FieldName = 'nSaldoTit'
          Options.Editing = False
          Width = 99
        end
        object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
          Caption = 'Credor'
          DataBinding.FieldName = 'cNmTerceiro'
          Options.Editing = False
          Styles.OnGetContentStyle = cxGrid1DBTableView1cNmTerceiroStylesGetContentStyle
          Width = 188
        end
        object cxGrid1DBTableView1cNmCategFinanc: TcxGridDBColumn
          Caption = 'Categoria Financeira'
          DataBinding.FieldName = 'cNmCategFinanc'
          Options.Editing = False
          Width = 148
        end
        object cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn
          Caption = 'Esp'#233'cie de T'#237'tulo'
          DataBinding.FieldName = 'cNmEspTit'
          Options.Editing = False
          Width = 129
        end
        object cxGrid1DBTableView1cNmFormaPagto: TcxGridDBColumn
          Caption = 'Forma Pagamento Padr'#227'o'
          DataBinding.FieldName = 'cNmFormaPagto'
          Options.Editing = False
          Width = 114
        end
        object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
          Caption = 'ID'
          DataBinding.FieldName = 'nCdTitulo'
          Options.Editing = False
        end
        object cxGrid1DBTableView1nCdSP: TcxGridDBColumn
          Caption = 'Nr. SP'
          DataBinding.FieldName = 'nCdSP'
          Options.Editing = False
        end
        object cxGrid1DBTableView1nCdRecebimento: TcxGridDBColumn
          Caption = 'Receb.'
          DataBinding.FieldName = 'nCdRecebimento'
          Options.Editing = False
        end
        object cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn
          DataBinding.FieldName = 'nCdTerceiro'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1dDtPagto: TcxGridDBColumn
          Caption = 'Data Prov.'
          DataBinding.FieldName = 'dDtPagto'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1cFlgTituloAdiant: TcxGridDBColumn
          DataBinding.FieldName = 'cFlgTituloAdiant'
          Visible = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object Panel4: TPanel [4]
    Tag = 1
    Left = 0
    Top = 462
    Width = 1136
    Height = 41
    Align = alBottom
    BiDiMode = bdLeftToRight
    Ctl3D = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Consolas'
    Font.Style = []
    ParentBiDiMode = False
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 3
    object Label1: TLabel
      Left = 8
      Top = 10
      Width = 125
      Height = 19
      Caption = 'Total Selecionado'
      Color = clBtnFace
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label20: TLabel
      Tag = 2
      Left = 376
      Top = 10
      Width = 315
      Height = 19
      Caption = 'Terceiro com Adiantamento em aberto'
    end
    object edtTotalSelec: TcxCurrencyEdit
      Left = 152
      Top = 6
      Width = 145
      Height = 23
      ParentFont = False
      Properties.Alignment.Horz = taRightJustify
      Properties.Alignment.Vert = taBottomJustify
      Properties.ReadOnly = True
      Properties.ValidateOnEnter = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Arial'
      Style.Font.Style = [fsBold]
      TabOrder = 0
    end
    object cxTextEdit1: TcxTextEdit
      Left = 344
      Top = 6
      Width = 25
      Height = 27
      Enabled = False
      Style.Color = clBlue
      StyleDisabled.Color = clRed
      TabOrder = 1
    end
  end
  object Panel3: TPanel [5]
    Left = 0
    Top = 544
    Width = 1136
    Height = 198
    Align = alBottom
    Caption = 'Panel3'
    TabOrder = 4
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 1134
      Height = 196
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGridDBTableView1: TcxGridDBTableView
        DataController.DataSource = dsProvisaoTit
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsCustomize.ColumnHiding = True
        OptionsCustomize.ColumnMoving = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GridLines = glVertical
        OptionsView.GroupByBox = False
        Styles.Header = frmMenu.Header
        object cxGridDBTableView1nCdProvisaoTit: TcxGridDBColumn
          DataBinding.FieldName = 'nCdProvisaoTit'
        end
        object cxGridDBTableView1dDtPrev: TcxGridDBColumn
          DataBinding.FieldName = 'dDtPrev'
        end
        object cxGridDBTableView1nCdContaBancaria: TcxGridDBColumn
          DataBinding.FieldName = 'nCdContaBancaria'
          Visible = False
        end
        object cxGridDBTableView1nCdBanco: TcxGridDBColumn
          DataBinding.FieldName = 'nCdBanco'
        end
        object cxGridDBTableView1cAgencia: TcxGridDBColumn
          DataBinding.FieldName = 'cAgencia'
        end
        object cxGridDBTableView1nCdConta: TcxGridDBColumn
          DataBinding.FieldName = 'nCdConta'
        end
        object cxGridDBTableView1dDtPagto: TcxGridDBColumn
          DataBinding.FieldName = 'dDtPagto'
        end
        object cxGridDBTableView1nCdFormaPagto: TcxGridDBColumn
          DataBinding.FieldName = 'nCdFormaPagto'
          Visible = False
        end
        object cxGridDBTableView1cNmFormaPagto: TcxGridDBColumn
          DataBinding.FieldName = 'cNmFormaPagto'
        end
        object cxGridDBTableView1nValProvisao: TcxGridDBColumn
          DataBinding.FieldName = 'nValProvisao'
          Width = 96
        end
        object cxGridDBTableView1cNmTitular: TcxGridDBColumn
          DataBinding.FieldName = 'cNmTitular'
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
  end
  object Panel5: TPanel [6]
    Tag = 1
    Left = 0
    Top = 503
    Width = 1136
    Height = 41
    Align = alBottom
    BiDiMode = bdLeftToRight
    Ctl3D = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Consolas'
    Font.Style = []
    ParentBiDiMode = False
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 5
    object Label2: TLabel
      Left = 536
      Top = 8
      Width = 131
      Height = 13
      Caption = 'Suas Provis'#245'es - em aberto'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButton4: TcxButton
      Left = 0
      Top = 8
      Width = 113
      Height = 25
      Caption = 'Exibir Provis'#245'es'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnClick = cxButton4Click
      Glyph.Data = {
        3E020000424D3E0200000000000036000000280000000D0000000D0000000100
        1800000000000802000000000000000000000000000000000000BACACABACACE
        BACACEBACACEBACACEBACACEBACACEBACACEBACACAC2D2D6AEBEC29DABACC6D6
        DA00C6D6DAC6DADAC6DADAC6DADACADADECDDEE2CDDEE2CDDEE2CEE2E4BECED2
        636C6C3D42429AA6AA00C2D6D6C6D6DAC6DADACDDEE2BACACAABB9BAABB9BAB2
        C1C2BACACA5C6465AEAEAE898A893D424200C2D6D6C6DADACDDEE2A6B4B65157
        57191D23313332575D5E484E4EBEBEBE898A89313332A2AFB000C2D6D6CADEDE
        909EA0313332849091D2DEDFCEDADE777B7B3D42429E9E9E3D42429AA6AAD6EA
        EB00C2D6D6C6DADA3D4242BECED2BAC5C6D5D5D5CCD5D5BECACAABB9BA191D23
        849091CDDEE2CADEDE00CDDEE26D777851575731333231333231333231333231
        33323133325157575C6465C2D2D2CDDEE200D3E4E5575D5E575D5E10708207F0
        F907F0F907F0F907F0F93D4242575D5E484E4EC6D6DACDDEE200D3E4E5575D5E
        575D5E191D23107082107082107082107082313332717272484E4EC2D6D6CDDE
        E200C6DADAA6B4B63D42426D6C6B777B7B898A898C908F9392928E9A9A3D4242
        96A3A6C6DADACADADE00C2D6D6CEE2E45157575C6465909EA0BACACAD6E6EAB2
        C1C27B85855C6465D3E4E5C6D6DACADADA00C2D6D6CADEDEC2D2D25C6465484E
        4E3D42423D4242484E4E636C6CCADADECADADAC6DADACADADE00BED2D2C2D2D2
        C2D6D6D6EAEBA6B4B67B85857B8585AFBDBED6EAEBC2D2D6C2D2D2C2D2D2C2D6
        D600}
      LookAndFeel.NativeStyle = True
    end
    object cxButton3: TcxButton
      Left = 112
      Top = 8
      Width = 121
      Height = 25
      Caption = 'Detalhar Provis'#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      OnClick = cxButton3Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000C6D6DEC6948C
        C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
        8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
        D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
        00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
        FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
        948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
        FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
        C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
        FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
        948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
        8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
        D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
      LookAndFeel.NativeStyle = True
    end
    object cxButton5: TcxButton
      Left = 232
      Top = 8
      Width = 121
      Height = 25
      Caption = 'Excluir Provis'#227'o'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      OnClick = cxButton5Click
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000BDBDBDBDBDBD
        BDBDBDBDBDBDC6948C000000000000000000000000000000C6948CC6948C0000
        00000000BDBDBD000000BDBDBDBDBDBDBDBDBD000000000000000000EFFFFF00
        0000000084000084000000000000EFFFFF000000C6948C000000BDBDBDBDBDBD
        000000000084000084000000EFFFFFEFFFFF000000000084000000EFFFFFEFFF
        FF000000C6948C000000BDBDBD0000000000842100C62100C6000000000000EF
        FFFFEFFFFF000000EFFFFFEFFFFF000000000000C6948C000000BDBDBD000000
        2100C60000842100C62100C62100C6000000EFFFFFEFFFFFEFFFFF0000000000
        00000000C6948C0000000000000000842100C62100C62100C60000FF2100C600
        0000EFFFFFEFFFFFEFFFFF0000000000840000840000000000000000002100C6
        0000FF2100C60000FF0000FF000000EFFFFFEFFFFF000000EFFFFFEFFFFF0000
        000000000000000000000000000000FF2100C60000FF0000FF000000EFFFFFEF
        FFFF2100C60000FF000000EFFFFFEFFFFF0000000000000000000000002100C6
        0000FF0000FF0000FF000000EFFFFF0000000000FF2100C60000FF000000EFFF
        FF0000000000000000000000002100C60000FF0000FF0000FF00000000000000
        00FF0000FF0000FF2100C60000FF000000000000000000000000BDBDBD000000
        0000FF0000FF0000FFEFFFFFEFFFFF0000FF0000FF0000FF0000FF2100C62100
        C6000000BDBDBD000000BDBDBD0000000000FF0000FF0000FFEFFFFFEFFFFF00
        00FF0000FF0000FF2100C60000FF2100C6000000BDBDBD000000BDBDBDBDBDBD
        0000000000FF0000FF0000FF0000FF0000FF0000FF0000FF0000FF2100C60000
        00BDBDBDBDBDBD000000BDBDBDBDBDBDBDBDBD0000000000000000FF0000FF00
        00FF0000FF0000FF000000000000BDBDBDBDBDBDBDBDBD000000BDBDBDBDBDBD
        BDBDBDBDBDBDBDBDBD000000000000000000000000000000BDBDBDBDBDBDBDBD
        BDBDBDBDBDBDBD000000}
      LookAndFeel.NativeStyle = True
    end
  end
  inherited ImageList1: TImageList
    Left = 424
    Top = 200
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    AfterPost = qryTempAfterPost
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_TitulosProvisao'#39') IS NULL) '
      'BEGIN'
      
        #9'CREATE TABLE #Temp_TitulosProvisao(nCdTitulo      int PRIMARY K' +
        'EY'
      #9#9#9#9#9#9#9'   ,cFlgOK         int default 0 not null'
      #9#9#9#9#9#9#9'   ,nCdSP          int'
      #9#9#9#9#9#9#9'   ,nCdRecebimento int'
      #9#9#9#9#9#9#9'   ,dDtVenc        datetime'
      #9#9#9#9#9#9#9'   ,nCdTerceiro    int'
      #9#9#9#9#9#9#9'   ,cNmTerceiro    varchar(100)'
      #9#9#9#9#9#9#9'   ,nSaldoTit      decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,cNmLoja        varchar(50)'
      '                               ,dDtPagto       datetime'
      #9#9#9#9#9#9#9'   ,cNmEspTit      varchar(50)'
      #9#9#9#9#9#9#9'   ,cNrTit         char(17)'
      #9#9#9#9#9#9#9'   ,cNrNF          char(15)'
      #9#9#9#9#9#9#9'   ,iParcela       int'
      '                               ,cNmFormaPagto  varchar(50)'
      '                               ,cNmCategFinanc varchar(50)'
      '                               ,cFlgTituloAdiant  int)'
      ''
      'END'
      ''
      ''
      'SELECT *'
      'FROM #Temp_TitulosProvisao'
      'ORDER BY cNmLoja, dDtVenc')
    Left = 336
    Top = 185
    object qryTempnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTempcFlgOK: TIntegerField
      FieldName = 'cFlgOK'
    end
    object qryTempnCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryTempnCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryTempdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTempnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTempcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTempnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempcNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryTempdDtPagto: TDateTimeField
      FieldName = 'dDtPagto'
    end
    object qryTempcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTempcNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTempcNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTempiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTempcNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryTempcNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object qryTempcFlgTituloAdiant: TIntegerField
      FieldName = 'cFlgTituloAdiant'
    end
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#Temp_TitulosProvisao'#39') IS NULL) '#13#10'BEGIN'#13 +
      #10#9'CREATE TABLE #Temp_TitulosProvisao(nCdTitulo      int PRIMARY ' +
      'KEY'#13#10#9#9#9#9#9#9#9'   ,cFlgOK         int default 0 not null'#13#10#9#9#9#9#9#9#9'  ' +
      ' ,nCdSP          int'#13#10#9#9#9#9#9#9#9'   ,nCdRecebimento int'#13#10#9#9#9#9#9#9#9'   ,' +
      'dDtVenc        datetime'#13#10#9#9#9#9#9#9#9'   ,nCdTerceiro    int'#13#10#9#9#9#9#9#9#9' ' +
      '  ,cNmTerceiro    varchar(100)'#13#10#9#9#9#9#9#9#9'   ,nSaldoTit      decima' +
      'l(12,2) default 0 not null'#13#10#9#9#9#9#9#9#9'   ,cNmLoja        varchar(50' +
      ')'#13#10'                               ,dDtPagto       datetime'#13#10#9#9#9#9 +
      #9#9#9'   ,cNmEspTit      varchar(50)'#13#10#9#9#9#9#9#9#9'   ,cNrTit         cha' +
      'r(17)'#13#10#9#9#9#9#9#9#9'   ,cNrNF          char(15)'#13#10#9#9#9#9#9#9#9'   ,iParcela  ' +
      '     int'#13#10'                               ,cNmFormaPagto  varchar' +
      '(50)'#13#10'                               ,cNmCategFinanc varchar(50)' +
      #13#10'                               ,cFlgTituloAdiant  int)'#13#10#13#10'END'#13 +
      #10#13#10'TRUNCATE TABLE #Temp_TitulosProvisao'#13#10#13#10'DECLARE @cNrTit      ' +
      'CHAR(17)'#13#10'       ,@nCdSP       int'#13#10'       ,@nCdTerceiro int'#13#10'  ' +
      '     ,@dDtVencIni  varchar(10)'#13#10'       ,@dDtVencFim  varchar(10)' +
      #13#10'       ,@dDtPagto    varchar(10)'#13#10'       ,@nCdEmpresa  int'#13#10#13#10 +
      'Set @nCdEmpresa  = :nCdEmpresa'#13#10'Set @nCdSP       = :nCdSP'#13#10'Set @' +
      'nCdTerceiro = :nCdTerceiro'#13#10'Set @dDtVencIni  = :dDtVencIni'#13#10'Set ' +
      '@dDtVencFim  = :dDtVencFim'#13#10'Set @dDtPagto    = :dDtPagto'#13#10'Set @c' +
      'NrTit      = :cNrTit'#13#10#13#10'INSERT INTO #Temp_TitulosProvisao(nCdTit' +
      'ulo     '#13#10'                          ,nCdSP         '#13#10'           ' +
      '               ,nCdRecebimento'#13#10'                          ,dDtVe' +
      'nc    '#13#10'                          ,nCdTerceiro'#13#10'                ' +
      '          ,cNmTerceiro'#13#10'                          ,nSaldoTit'#13#10'  ' +
      '                        ,cNmLoja'#13#10'                          ,dDt' +
      'Pagto'#13#10#9#9#9#9#9#9'  ,cNmEspTit'#13#10#9#9#9#9#9#9'  ,cNrTit   '#13#10#9#9#9#9#9#9'  ,cNrNF   ' +
      ' '#13#10#9#9#9#9#9#9'  ,iParcela'#13#10'                          ,cNmFormaPagto'#13#10 +
      '                          ,cNmCategFinanc'#13#10'                     ' +
      '     ,cFlgTituloAdiant)'#13#10'                    SELECT Tit.nCdTitul' +
      'o'#13#10'                          ,Tit.nCdSP'#13#10'                       ' +
      '   ,Tit.nCdRecebimento'#13#10'                          ,Tit.dDtVenc'#13#10 +
      '                          ,Tit.nCdTerceiro'#13#10'                    ' +
      '      ,Terceiro.cNmTerceiro'#13#10'                          ,Tit.nSal' +
      'doTit'#13#10'                          ,dbo.fn_ZeroEsquerda(Tit.nCdLoj' +
      'aTit,3)'#13#10'                          ,Convert(DATETIME,@dDtPagto,1' +
      '03)'#13#10'                          ,EspTit.cNmEspTit'#13#10'              ' +
      '            ,Tit.cNrTit'#13#10'                          ,Tit.cNrNF'#13#10' ' +
      '                         ,Tit.iParcela'#13#10'                        ' +
      '  ,CASE WHEN FormaPagto2.cNmFormaPagto IS NOT NULL THEN FormaPag' +
      'to2.cNmFormaPagto'#13#10'                                ELSE FormaPag' +
      'to.cNmFormaPagto'#13#10'                           END'#13#10'              ' +
      '            ,cNmCategFinanc'#13#10'                          ,CASE WHE' +
      'N (SELECT TOP 1 Titulo.nCdTitulo'#13#10'                              ' +
      '          FROM Titulo'#13#10'                                       WH' +
      'ERE Titulo.nCdTerceiro       = Tit.nCdTerceiro'#13#10'                ' +
      '                         AND Titulo.nCdEmpresa        = Tit.nCdE' +
      'mpresa'#13#10'                                         AND Titulo.cFlg' +
      'Adiantamento  = 1'#13#10'                                         AND ' +
      'Titulo.nSaldoTit         > 0'#13#10'                                  ' +
      '       AND Titulo.dDtCancel        IS NULL) IS NOT NULL THEN 1'#13#10 +
      '                                ELSE 0'#13#10'                        ' +
      '   END cFlgTituloAdiant'#13#10'                FROM Titulo Tit'#13#10'      ' +
      '                     LEFT JOIN Terceiro       ON Terceiro.nCdTer' +
      'ceiro       = Tit.nCdTerceiro'#13#10'                           LEFT J' +
      'OIN SP             ON SP.nCdSP                   = Tit.nCdSP'#13#10'  ' +
      '                         LEFT JOIN Recebimento    ON Recebimento' +
      '.nCdRecebimento = Tit.nCdRecebimento'#13#10'                          ' +
      ' LEFT JOIN Loja LojaSP    ON LojaSP.nCdLoja             = SP.nCd' +
      'LojaSP'#13#10'                           LEFT JOIN Loja LojaReceb ON L' +
      'ojaReceb.nCdLoja          = Recebimento.nCdLoja'#13#10'               ' +
      '            LEFT JOIN EspTit         ON EspTit.nCdEspTit        ' +
      '   = Tit.nCdEspTit'#13#10'                           LEFT JOIN FormaPa' +
      'gto     ON FormaPagto.nCdFormaPagto   = Terceiro.nCdFormaPagto'#13#10 +
      '                           LEFT JOIN FormaPagto FormaPagto2 ON F' +
      'ormaPagto2.nCdFormaPagto = Tit.nCdFormaPagtoTit'#13#10'               ' +
      '            LEFT JOIN CategFinanc ON CategFinanc.nCdCategFinanc ' +
      '= Tit.nCdCategfinanc'#13#10'                     WHERE Tit.nCdEmpresa ' +
      '     = @nCdEmpresa'#13#10'                       AND ((Tit.dDtVenc    ' +
      '    >= Convert(DATETIME,@dDtVencIni,103)) OR (@dDtVencIni = '#39'01/' +
      '01/1900'#39'))'#13#10'                       AND ((Tit.dDtVenc        <= C' +
      'onvert(DATETIME,@dDtVencFim,103)) OR (@dDtVencFim = '#39'01/01/1900'#39 +
      '))'#13#10'                       AND Tit.cSenso = '#39'D'#39#13#10'               ' +
      '        AND Tit.dDtBloqTit IS NULL'#13#10'                       AND T' +
      'it.dDtCancel  IS NULL'#13#10'                       AND Tit.nSaldoTit ' +
      '      > 0'#13#10'                       AND EspTit.cFlgPrev = 0'#13#10'     ' +
      '                  AND Tit.nCdProvisaoTit IS NULL'#13#10'              ' +
      '         AND ((Tit.nCdTerceiro   = @nCdTerceiro)   OR (@nCdTerce' +
      'iro = 0))'#13#10'                       AND ((Tit.nCdSP         = @nCd' +
      'SP)         OR (@nCdSP       = 0))'#13#10'                       AND (' +
      '(Tit.cNrTit        = RTRIM(@cNrTit)) OR (LTRIM(RTRIM(@cNrTit)) =' +
      ' '#39#39'))'#13#10'                       AND cFlgDA = 0'
    CommandTimeout = 0
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdSP'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtVencIni'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtVencFim'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtPagto'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'cNrTit'
        DataType = ftString
        Size = 1
        Value = 'A'
      end>
    Left = 168
    Top = 177
  end
  object dsTemp: TDataSource
    DataSet = qryTemp
    Left = 368
    Top = 185
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_TitulosProvisao'#39') IS NULL) '
      'BEGIN'
      
        #9'CREATE TABLE #Temp_TitulosProvisao(nCdTitulo      int PRIMARY K' +
        'EY'
      #9#9#9#9#9#9#9'   ,cFlgOK         int default 0 not null'
      #9#9#9#9#9#9#9'   ,nCdSP          int'
      #9#9#9#9#9#9#9'   ,nCdRecebimento int'
      #9#9#9#9#9#9#9'   ,dDtVenc        datetime'
      #9#9#9#9#9#9#9'   ,nCdTerceiro    int'
      #9#9#9#9#9#9#9'   ,cNmTerceiro    varchar(100)'
      #9#9#9#9#9#9#9'   ,nSaldoTit      decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9'   ,cNmLoja        varchar(50)'
      '                               ,dDtPagto       datetime'
      #9#9#9#9#9#9#9'   ,cNmEspTit      varchar(50)'
      #9#9#9#9#9#9#9'   ,cNrTit         char(17)'
      #9#9#9#9#9#9#9'   ,cNrNF          char(15)'
      #9#9#9#9#9#9#9'   ,iParcela       int'
      '                               ,cNmFormaPagto  varchar(50)'
      '                               ,cNmCategFinanc varchar(50)'
      '                               ,cFlgTituloAdiant  int)'
      ''
      'END')
    Left = 336
    Top = 241
  end
  object qryProvisaoTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Prov.nCdProvisaoTit'
      '      ,Prov.nCdContaBancaria'
      '      ,ContaBancaria.nCdBanco'
      '      ,ContaBancaria.cAgencia'
      '      ,ContaBancaria.nCdConta'
      '      ,Prov.dDtPagto'
      '      ,Prov.nCdFormaPagto'
      '      ,cNmFormaPagto'
      '      ,dDtPrev'
      '      ,nValProvisao'
      '      ,cNmTitular'
      '  FROM ProvisaoTit Prov'
      
        '       LEFT JOIN ContaBancaria ON ContaBancaria.nCdContaBancaria' +
        ' = Prov.nCdContaBancaria'
      
        '       LEFT JOIN FormaPagto    ON FormaPagto.nCdFormaPagto      ' +
        ' = Prov.nCdFormaPagto'
      ' WHERE Prov.nCdUsuarioPrev = :nCdUsuario'
      '   AND Prov.nCdEmpresa     = :nCdEmpresa'
      '   AND Prov.cFlgBaixado    = 0'
      '   AND Prov.dDtCancel      IS NULL')
    Left = 240
    Top = 386
    object qryProvisaoTitnCdProvisaoTit: TAutoIncField
      DisplayLabel = 'C'#243'd. Prov.'
      FieldName = 'nCdProvisaoTit'
      ReadOnly = True
    end
    object qryProvisaoTitdDtPrev: TDateTimeField
      DisplayLabel = 'Data Provis'#227'o'
      FieldName = 'dDtPrev'
    end
    object qryProvisaoTitnCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryProvisaoTitnCdBanco: TIntegerField
      DisplayLabel = 'Banco'
      FieldName = 'nCdBanco'
    end
    object qryProvisaoTitcAgencia: TIntegerField
      DisplayLabel = 'Ag'#234'ncia'
      FieldName = 'cAgencia'
    end
    object qryProvisaoTitnCdConta: TStringField
      DisplayLabel = 'N'#250'm. Conta'
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryProvisaoTitdDtPagto: TDateTimeField
      DisplayLabel = 'Data Pagamento'
      FieldName = 'dDtPagto'
    end
    object qryProvisaoTitnCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryProvisaoTitcNmFormaPagto: TStringField
      DisplayLabel = 'Forma Pagamento'
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryProvisaoTitnValProvisao: TBCDField
      DisplayLabel = 'Valor Provis'#227'o'
      FieldName = 'nValProvisao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryProvisaoTitcNmTitular: TStringField
      DisplayLabel = 'Titular Conta'
      FieldName = 'cNmTitular'
      Size = 50
    end
  end
  object dsProvisaoTit: TDataSource
    DataSet = qryProvisaoTit
    Left = 280
    Top = 386
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 344
    Top = 370
  end
  object qryConfereBaixaProvisao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cFlgBaixado'
      '  FROM ProvisaoTit'
      ' WHERE nCdProvisaoTit = :nPK')
    Left = 424
    Top = 527
    object qryConfereBaixaProvisaocFlgBaixado: TIntegerField
      FieldName = 'cFlgBaixado'
    end
  end
  object PopupMenu1: TPopupMenu
    Images = ImageList1
    OnPopup = PopupMenu1Popup
    Left = 720
    Top = 209
    object ExibirAdiantamentos1: TMenuItem
      Caption = 'Exibir Adiantamentos'
      OnClick = ExibirAdiantamentos1Click
    end
    object Atualizar1: TMenuItem
      Caption = 'Atualizar'
      ImageIndex = 1
      OnClick = Atualizar1Click
    end
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 848
    Top = 161
  end
end
