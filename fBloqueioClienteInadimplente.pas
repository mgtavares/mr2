unit fBloqueioClienteInadimplente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, ER2Lookup, DBGridEhGrouping, ToolCtrlsEh, GridsEh,
  DBGridEh;

type
  TfrmBloqueioClienteInadimplente = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    ToolButton4: TToolButton;
    qryMaster: TADOQuery;
    dsMaster: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    ToolButton5: TToolButton;
    PreparaTemp: TADOCommand;
    qryPopulaTemp: TADOQuery;
    qryMasternCdTitulo: TIntegerField;
    qryMasteriParcela: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasterdDtVenc: TDateTimeField;
    qryMasternValTit: TBCDField;
    qryMastercNmTerceiro: TStringField;
    qryPopulaTempnCdTitulo: TIntegerField;
    qryPopulaTempiParcela: TIntegerField;
    qryPopulaTempnCdTerceiro: TIntegerField;
    qryPopulaTempdDtVenc: TDateTimeField;
    qryPopulaTempnValTit: TBCDField;
    qryMastercFlgAux: TIntegerField;
    qryPopulaTempcFlgAux: TIntegerField;
    SP_BLOQUEIA_INADIMPLENTE: TADOStoredProc;
    procedure qryMasterCalcFields(DataSet: TDataSet);
    procedure ToolButton4Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBloqueioClienteInadimplente: TfrmBloqueioClienteInadimplente;

implementation

{$R *.dfm}

uses
  fMenu;

procedure TfrmBloqueioClienteInadimplente.qryMasterCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryTerceiro, IntToStr(qryMasternCdTerceiro.Value));
  qryMastercNmTerceiro.Value := qryTerceirocNmTerceiro.Value;
end;

procedure TfrmBloqueioClienteInadimplente.ToolButton4Click(
  Sender: TObject);
var
  iAno : integer;
  ncdEsptit : integer;

begin
  inherited;

  iAno      := 0;
  ncdEsptit := 0;


  iAno      := StrToInt(frmMenu.LeParametro('ANOSAPOSVENCTIT'));
  ncdEsptit := StrToInt(frmMenu.LeParametro('CDESPTIT-CRED'));

  PreparaTemp.Execute;

  qryPopulaTemp.close;
  qryPopulaTemp.Parameters.ParamByName('iAno').Value      := iAno;
  qryPopulaTemp.Parameters.ParamByName('nCdEsptit').Value := ncdEsptit;
  qryPopulaTemp.ExecSQL;

  qryMaster.Close;
  qryMaster.Open;


  if (qryMaster.IsEmpty) then
  begin
      MensagemAlerta('Nenhum cliente inadimplente a mais de '+IntToStr(iAno)+' ano(s).');
      abort;
  end;

end;

procedure TfrmBloqueioClienteInadimplente.ToolButton1Click(
  Sender: TObject);
var
  i : integer;
begin
  if qryMaster.IsEmpty then
  begin
      MensagemAlerta('Nenhum cliente selecionado para o bloqueio.');
      abort;
  end;

  inherited;

  qryMaster.First;

  i := 0;

  while not qryMaster.Eof do
  begin
      if (qryMastercFlgAux.Value = 1) then
          i := i + 1;

      qryMaster.Next;
  end;

  qryMaster.First;

  if (i=0) then
  begin
      MensagemErro('Nenhum cliente selecionado para bloqueio.') ;
      abort ;
  end ;

  if (MessageDlg('Confirma o bloqueio destes clientes ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit;

  frmMenu.Connection.BeginTrans ;

  try
      SP_BLOQUEIA_INADIMPLENTE.Close;
      SP_BLOQUEIA_INADIMPLENTE.Parameters.ParamByName('@nCdUsuarioBloqueio').Value := frmMenu.nCdUsuarioLogado;
      SP_BLOQUEIA_INADIMPLENTE.Parameters.ParamByName('@nCdLojaBloqueio').Value    := frmMenu.nCdLojaAtiva;
      SP_BLOQUEIA_INADIMPLENTE.ExecProc;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise;

  end;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Clientes bloqueados com sucesso.') ;

  qryMaster.Close;

end;

initialization
  RegisterClass(TfrmBloqueioClienteInadimplente);

end.
