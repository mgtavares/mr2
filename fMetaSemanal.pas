unit fMetaSemanal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, GridsEh, DBGridEh, cxPC, cxControls, ImgList,
  ComCtrls, ToolWin, ExtCtrls, DB, StdCtrls, Mask, DBCtrls, ADODB,
  DBGridEhGrouping;

type
  TfrmMetaSemanal = class(TfrmProcesso_Padrao)
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryMetaSemana: TADOQuery;
    qryMetaSemananCdMetaSemana: TIntegerField;
    qryMetaSemanaiSemana: TIntegerField;
    qryMetaSemananValMetaMinima: TBCDField;
    qryMetaSemananValMeta: TBCDField;
    qryMetaSemananValVendaMediaPrev: TBCDField;
    qryMetaSemananItensPorVendaPrev: TBCDField;
    qryMetaSemananValItemMedioPrev: TBCDField;
    qryMetaSemananValVendaHoraPrev: TBCDField;
    qryMetaSemananTxConversaoPrev: TBCDField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    dsMetaSemana: TDataSource;
    qryCopiarIndicadores: TADOQuery;
    qryMetaSemananCdMetaMes: TIntegerField;
    qryRatearMeta: TADOQuery;
    qryVerificaTotais: TADOQuery;
    qryVerificaTotaisnValMetaMinima: TBCDField;
    qryVerificaTotaisnValMeta: TBCDField;
    qryMetaSemanaiTotalHorasPrev: TBCDField;
    qryVerificaTotaisiTotalHorasPrev: TBCDField;
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton7Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
      nValMetaMinima, nValMeta : Double;
      iTotalHorasPrev          : Double;
  end;

var
  frmMetaSemanal: TfrmMetaSemanal;

implementation

uses fMetaMensal, fMenu;

{$R *.dfm}

procedure TfrmMetaSemanal.ToolButton5Click(Sender: TObject);
begin
  inherited;
  if (MessageDlg('Est� opera��o ir� substituir os valores de indicadores da meta semanal pelos indicadores do m�s. Deseja continuar ? ',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryCopiarIndicadores.Close;
      qryCopiarIndicadores.Parameters.ParamByName('nCdMetaMes').Value := qryMetaSemananCdMetaMes.Value;
      qryCopiarIndicadores.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento');
      raise
  end;

  frmMenu.Connection.CommitTrans;
  
  qryMetaSemana.Close;
  qryMetaSemana.Open;
  
end;

procedure TfrmMetaSemanal.ToolButton7Click(Sender: TObject);
begin
  inherited;

  if (MessageDlg('Deseja ratear o valor da meta por igual para as semanas ? ',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryRatearMeta.Close;
      qryRatearMeta.Parameters.ParamByName('nCdMetaMes').Value := qryMetaSemananCdMetaMes.Value;
      qryRatearMeta.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no Processamento');
      raise
  end;

  frmMenu.Connection.CommitTrans;

  qryMetaSemana.Close;
  qryMetaSemana.Open;
end;

procedure TfrmMetaSemanal.FormShow(Sender: TObject);
begin
  inherited;
  DBGridEh1.SetFocus;
end;

procedure TfrmMetaSemanal.ToolButton1Click(Sender: TObject);
begin
  inherited;

  Close;

end;

procedure TfrmMetaSemanal.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmMetaSemanal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  {inherited;}

  qryVerificaTotais.Close;
  qryVerificaTotais.Parameters.ParamByName('nCdMetaMes').Value := qryMetaSemananCdMetaMes.Value;
  qryVerificaTotais.Open;
  
  if (qryVerificaTotaisnValMetaMinima.AsFloat <> nValMetaMinima) then
  begin
      if (MessageDlg('A soma da meta m�nima das semanas � diferente da meta m�nima do m�s. Deseja continuar ? ',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      begin
          DBGridEh1.SetFocus;
          abort ;
      end;
  end;

  if (qryVerificaTotaisnValMeta.AsFloat <> nValMeta) then
  begin
      if (MessageDlg('A soma da meta das semanas � diferente da meta do m�s. Deseja continuar ? ',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      begin
          DBGridEh1.SetFocus;
          abort ;
      end ;
  end;

  iTotalHorasPrev := qryVerificaTotaisiTotalHorasPrev.Value;

  qryMetaSemana.Close;

end;

end.
