unit fCaixa_ConsPropReneg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, jpeg, ExtCtrls,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxControls, cxGridCustomView, cxGrid, ImgList, ADODB,
  StdCtrls, cxButtons;

type
  TfrmCaixa_ConsPropReneg = class(TForm)
    btFechar: TcxButton;
    btSeleciona: TcxButton;
    dsPropostaReneg: TDataSource;
    qryPropostaReneg: TADOQuery;
    ImageList1: TImageList;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Image1: TImage;
    qryPropostaRenegnCdPropostaReneg: TAutoIncField;
    qryPropostaRenegdDtProposta: TDateTimeField;
    qryPropostaRenegdDtValidade: TDateTimeField;
    qryPropostaRenegnCdTabStatusPropostaReneg: TIntegerField;
    qryPropostaRenegcNmTabStatusPropostaReneg: TStringField;
    qryPropostaRenegnSaldoNegociado: TBCDField;
    qryPropostaRenegiParcelas: TIntegerField;
    cxGrid1DBTableView1nCdPropostaReneg: TcxGridDBColumn;
    cxGrid1DBTableView1dDtProposta: TcxGridDBColumn;
    cxGrid1DBTableView1dDtValidade: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTabStatusPropostaReneg: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoNegociado: TcxGridDBColumn;
    cxGrid1DBTableView1iParcelas: TcxGridDBColumn;
    cxGrid1DBTableView1nValEntrada: TcxGridDBColumn;
    cxGrid1DBTableView1nValParcela: TcxGridDBColumn;
    qryPropostaRenegnValEntrada: TBCDField;
    qryPropostaRenegnValParcela: TBCDField;
    rgFiltros: TRadioGroup;
    qryPropostaRenegcFlgRenegMultipla: TIntegerField;
    qryCancelaReneg: TADOQuery;
    procedure exibePropostas(nCdEmpresa,nCdTerceiro:integer);
    procedure FormShow(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure btSelecionaClick(Sender: TObject);
    procedure rgFiltrosClick(Sender: TObject);
  private
    { Private declarations }
    nCdTerceiroReneg : Integer;
  public
    { Public declarations }
  end;

var
  frmCaixa_ConsPropReneg: TfrmCaixa_ConsPropReneg;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmCaixa_ConsPropReneg }

procedure TfrmCaixa_ConsPropReneg.exibePropostas(nCdEmpresa,
  nCdTerceiro: integer);
begin
    nCdTerceiroReneg := nCdTerceiro;

    qryPropostaReneg.Close;
    qryPropostaReneg.Parameters.ParamByName('nCdEmpresa').Value                := nCdEmpresa;
    qryPropostaReneg.Parameters.ParamByName('nCdTerceiro').Value               := nCdTerceiro;
    qryPropostaReneg.Parameters.ParamByName('nCdTabStatusPropostaReneg').Value := rgFiltros.ItemIndex;
    qryPropostaReneg.Open;

    if (qryPropostaReneg.Eof) then
    begin
        frmMenu.ShowMessage('Nenhuma proposta de renegocia��o encontrada para este cliente.') ;
        PostMessage(Self.Handle, WM_CLOSE, 0, 0);
        exit ;
    end ;

    Self.ShowModal;
end;

procedure TfrmCaixa_ConsPropReneg.FormShow(Sender: TObject);
begin
    cxGrid1.SetFocus;

end;

procedure TfrmCaixa_ConsPropReneg.btFecharClick(Sender: TObject);
begin

    qryPropostaReneg.Close;
    Close;

end;

procedure TfrmCaixa_ConsPropReneg.cxGrid1DBTableView1KeyPress(
  Sender: TObject; var Key: Char);
begin

    if (Key = #13) then
        btSeleciona.Click;

    if (Key = #27) then
        btFechar.Click;

end;

procedure TfrmCaixa_ConsPropReneg.btSelecionaClick(Sender: TObject);
begin

    if (not qryPropostaReneg.IsEmpty) then
    begin
        if (qryPropostaRenegnCdTabStatusPropostaReneg.Value = 1) then
        begin
            frmMenu.MensagemErro('Esta proposta de renegocia��o n�o foi liberada.') ;
            abort ;
        end ;

        if (qryPropostaRenegnCdTabStatusPropostaReneg.Value = 3) then
        begin
            frmMenu.MensagemErro('Esta proposta de renegocia��o j� foi efetivada.') ;
            abort ;
        end ;

        if (qryPropostaRenegnCdTabStatusPropostaReneg.Value = 4) then
        begin
            frmMenu.MensagemErro('Esta proposta de renegocia��o est� cancelada.') ;
            abort ;
        end ;

        if (qryPropostaRenegnCdTabStatusPropostaReneg.Value = 5) then
        begin
            frmMenu.MensagemErro('Esta proposta de renegocia��o est� vencida.') ;
            abort ;
        end ;

        if (qryPropostaRenegnCdTabStatusPropostaReneg.Value = 6) then
        begin
            frmMenu.MensagemErro('Esta proposta de renegocia��o foi estornada no caixa.') ;
            abort ;
        end ;

        case (qryPropostaRenegcFlgRenegMultipla.Value) of
            0 : begin
                    if (frmMenu.MessageDlg('Confirma a proposta de renegocia��o selecionada ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
                        Exit;
                end;

            1 : begin
                    if (frmMenu.MessageDlg('Esta proposta de renegocia��o possui outras propostas vinculadas a ela, ao confirmar todas as outras ser�o canceladas.' + #13#13 + 'Confirma a proposta de renegocia��o selecionada ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
                        Exit
                    else
                    begin
                        { -- cancela renegocia��es m�ltiplas que n�o ser�o utilizadas de acordo com os t�tulos da renegocia��o selecionada -- }
                        qryCancelaReneg.Close;
                        qryCancelaReneg.Parameters.ParamByName('nCdUsuarioCancel').Value := frmMenu.nCdUsuarioLogado;
                        qryCancelaReneg.Parameters.ParamByName('nCdPropostaReneg').Value := qryPropostaRenegnCdPropostaReneg.Value;
                        qryCancelaReneg.ExecSQL;
                    end;
                end;
        end;

        Close;
    end;
end;

procedure TfrmCaixa_ConsPropReneg.rgFiltrosClick(Sender: TObject);
begin
    qryPropostaReneg.Close;
    qryPropostaReneg.Parameters.ParamByName('nCdEmpresa').Value                := frmMenu.nCdEmpresaAtiva;
    qryPropostaReneg.Parameters.ParamByName('nCdTerceiro').Value               := nCdTerceiroReneg;
    qryPropostaReneg.Parameters.ParamByName('nCdTabStatusPropostaReneg').Value := rgFiltros.ItemIndex;
    qryPropostaReneg.Open;
end;

end.
