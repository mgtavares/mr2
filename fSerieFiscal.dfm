inherited frmSerieFiscal: TfrmSerieFiscal
  Left = 274
  Top = 154
  Height = 506
  Caption = 'S'#233'rie Fiscal'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 443
  end
  object Label1: TLabel [1]
    Left = 62
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 65
    Top = 86
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 17
    Top = 134
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Documento Fiscal'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 204
    Top = 158
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modelo'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 316
    Top = 158
    Width = 25
    Height = 13
    Caption = 'S'#233'rie'
  end
  object Label6: TLabel [6]
    Left = 30
    Top = 158
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = #218'ltimo N'#250'mero'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 13
    Top = 110
    Width = 95
    Height = 13
    Alignment = taRightJustify
    Caption = 'Modelo Impress'#227'o'
    FocusControl = DBEdit9
  end
  object Label8: TLabel [8]
    Left = 59
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  inherited ToolBar2: TToolBar
    TabOrder = 12
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [10]
    Tag = 1
    Left = 113
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdSerieFiscal'
    DataSource = dsMaster
    TabOrder = 0
  end
  object DBEdit2: TDBEdit [11]
    Left = 113
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
    OnKeyDown = DBEdit2KeyDown
  end
  object DBEdit3: TDBEdit [12]
    Left = 113
    Top = 128
    Width = 65
    Height = 19
    DataField = 'nCdTipoDoctoFiscal'
    DataSource = dsMaster
    TabOrder = 6
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [13]
    Left = 249
    Top = 152
    Width = 40
    Height = 19
    DataField = 'cModelo'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit6: TDBEdit [14]
    Left = 113
    Top = 152
    Width = 65
    Height = 19
    DataField = 'iUltimoNumero'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit7: TDBEdit [15]
    Tag = 1
    Left = 181
    Top = 80
    Width = 420
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit8: TDBEdit [16]
    Tag = 1
    Left = 181
    Top = 128
    Width = 420
    Height = 19
    DataField = 'cNmTipoDoctoFiscal'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit9: TDBEdit [17]
    Left = 113
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdModImpDF'
    DataSource = dsMaster
    TabOrder = 4
    OnKeyDown = DBEdit9KeyDown
  end
  object DBEdit10: TDBEdit [18]
    Tag = 1
    Left = 181
    Top = 104
    Width = 420
    Height = 19
    DataField = 'cNmModImpDF'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit5: TDBEdit [19]
    Left = 344
    Top = 152
    Width = 40
    Height = 19
    DataField = 'cSerie'
    DataSource = dsMaster
    TabOrder = 10
  end
  object cxPageControl1: TcxPageControl [20]
    Left = 8
    Top = 192
    Width = 625
    Height = 265
    ActivePage = tabLoja
    LookAndFeel.NativeStyle = True
    TabOrder = 11
    ClientRectBottom = 261
    ClientRectLeft = 4
    ClientRectRight = 621
    ClientRectTop = 24
    object tabLoja: TcxTabSheet
      Caption = 'Lojas'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 617
        Height = 237
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsSerieFiscalLoja
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdSerieFiscalLoja'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            Title.Caption = 'Loja|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmLoja'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Loja|Descri'#231#227'o'
            Width = 412
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object tabNumDocFiscal: TcxTabSheet
      Caption = 'Hist. Documentos Pendentes'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 617
        Height = 237
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsNumDoctoFiscal
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdDoctoFiscal'
            Footers = <>
            Title.Caption = 'Doc. Fiscal'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cTextoCFOP'
            Footers = <>
            Title.Caption = 'Destino'
            Width = 282
          end
          item
            EditButtons = <>
            FieldName = 'iUltimoNumero'
            Footers = <>
            Title.Caption = 'N'#250'm. Documento'
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'dDtInclusao'
            Footers = <>
            Title.Caption = 'Dt. Inclus'#227'o'
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object tabNumInutil: TcxTabSheet
      Caption = 'Hist. de N'#250'mera'#231#245'es Inutiliz'#225'veis'
      ImageIndex = 2
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 41
        Width = 617
        Height = 196
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsSerieFiscalInutil
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'iNrInutil'
            Footers = <>
            Title.Caption = 'N'#250'm. Inutilizado'
            Width = 80
          end
          item
            EditButtons = <>
            FieldName = 'dDtCad'
            Footers = <>
            Title.Caption = 'Dt. Cadastro'
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            Title.Caption = 'Usu'#225'rio Resp.'
            Width = 130
          end
          item
            EditButtons = <>
            FieldName = 'cMotivoInutil'
            Footers = <>
            Title.Caption = 'Motivo de Inutiliza'#231#227'o'
            Width = 219
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 617
        Height = 41
        Align = alTop
        BevelOuter = bvNone
        Color = clWhite
        TabOrder = 1
        object btnVisualizarGrade: TcxButton
          Left = 2
          Top = 4
          Width = 151
          Height = 33
          Caption = 'Inutilizar Numera'#231#227'o'
          TabOrder = 0
          OnClick = btnVisualizarGradeClick
          Glyph.Data = {
            36030000424D360300000000000036000000280000000F000000100000000100
            1800000000000003000000000000000000000000000000000000EBEBEBEBEBEB
            EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB
            EBEBEBEBEBEBEB000000EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBB6BABBEBEBEBEB
            EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB000000EBEBEBEBEBEB
            EBEBEBEBEBEBEBEBEBEBEBEB6D7676EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB
            EBEBEBEBEBEBEB000000EBEBEBDBDCDC1E1F1F1E1F1FDBDCDCB6BABB102020EB
            EBEBEBEBEBEBEBEB1E1F1F1E1F1F1E1F1F1E1F1FEBEBEB000000EBEBEB1E1F1F
            EBEBEBEBEBEB1E1F1FEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB1E1F
            1FEBEBEBEBEBEB000000EBEBEB1E1F1FEBEBEBEBEBEB1E1F1FEBEBEBEBEBEBEB
            EBEBEBEBEBEBEBEBEBEBEBEBEBEB1E1F1FEBEBEBEBEBEB000000EBEBEB1E1F1F
            EBEBEBEBEBEB1E1F1FEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB1E1F
            1FEBEBEBEBEBEB000000EBEBEB1E1F1FEBEBEBEBEBEB1E1F1FEBEBEBEBEBEBEB
            EBEBEBEBEBEBEBEB1E1F1F1E1F1F1E1F1FEBEBEBEBEBEB000000EBEBEBDBDCDC
            1E1F1F1E1F1FDBDCDCEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBDBDCDC1E1F
            1FEBEBEBEBEBEB000000EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB
            EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB000000EBEBEBEBEBEB
            EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB
            EBEBEBEBEBEBEB000000EBEBEBEBEBEBEBEBEB009664EBEBEBEBEBEBEBEBEBEB
            EBEBEBEBEBEBEBEB009664009664009664EBEBEBEBEBEB000000EBEBEBEBEBEB
            EBEBEB009664E8F0EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEBE8F0EB0096640096
            64EBEBEBEBEBEB000000EBEBEBEBEBEBEBEBEBCAECE1009664EBF0ECEBEBEBEB
            EBEBEBEBEBEBF0EC009664CAECE1009664EBEBEBEBEBEB000000EBEBEBEBEBEB
            EBEBEBEBEBEBE4F1EB009664009664009664009664009664E4F1EBEBEBEBEBEB
            EBEBEBEBEBEBEB000000EBEBEBEBEBEBEBEBEBEBEBEBEBEBEBEFF0EDE4F1EBE2
            EFE9E4F1EBEFF0EDEBEBEBEBEBEBEBEBEBEBEBEBEBEBEB000000}
          LookAndFeel.Kind = lfOffice11
        end
      end
    end
  end
  object DBEdit11: TDBEdit [21]
    Left = 113
    Top = 56
    Width = 488
    Height = 19
    DataField = 'cNmSerieFiscal'
    DataSource = dsMaster
    TabOrder = 1
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM SerieFiscal'
      'WHERE nCdSerieFiscal = :nPK')
    Left = 592
    Top = 192
    object qryMasternCdSerieFiscal: TIntegerField
      FieldName = 'nCdSerieFiscal'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdTipoDoctoFiscal: TIntegerField
      FieldName = 'nCdTipoDoctoFiscal'
    end
    object qryMastercModelo: TStringField
      FieldName = 'cModelo'
      FixedChar = True
      Size = 2
    end
    object qryMasteriUltimoNumero: TIntegerField
      FieldName = 'iUltimoNumero'
    end
    object qryMastercNmTipoDoctoFiscal: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmTipoDoctoFiscal'
      LookupDataSet = qryTipoDoctoFiscal
      LookupKeyFields = 'nCdTipoDoctoFiscal'
      LookupResultField = 'cNmTipoDoctoFiscal'
      KeyFields = 'nCdTipoDoctoFiscal'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMastercNmEmpresa: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmEmpresa'
      LookupDataSet = qryEmpresa
      LookupKeyFields = 'nCdEmpresa'
      LookupResultField = 'cNmEmpresa'
      KeyFields = 'nCdEmpresa'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMasternCdModImpDF: TIntegerField
      FieldName = 'nCdModImpDF'
    end
    object qryMastercNmModImpDF: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmModImpDF'
      LookupDataSet = qryModImpDF
      LookupKeyFields = 'nCdModImpDF'
      LookupResultField = 'cNmModImpDF'
      KeyFields = 'nCdModImpDF'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMastercSerie: TStringField
      FieldName = 'cSerie'
      FixedChar = True
      Size = 3
    end
    object qryMastercNmSerieFiscal: TStringField
      FieldName = 'cNmSerieFiscal'
      Size = 100
    end
  end
  inherited dsMaster: TDataSource
    Left = 592
    Top = 224
  end
  inherited qryID: TADOQuery
    Left = 720
    Top = 192
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 752
    Top = 192
  end
  inherited qryStat: TADOQuery
    Left = 720
    Top = 224
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 752
    Top = 224
  end
  inherited ImageList1: TImageList
    Left = 752
    Top = 256
  end
  object qryTipoDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoDoctoFiscal')
    Left = 784
    Top = 256
    object qryTipoDoctoFiscalnCdTipoDoctoFiscal: TIntegerField
      FieldName = 'nCdTipoDoctoFiscal'
    end
    object qryTipoDoctoFiscalcNmTipoDoctoFiscal: TStringField
      FieldName = 'cNmTipoDoctoFiscal'
      Size = 50
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      ',cNmEmpresa'
      'FROM Empresa')
    Left = 816
    Top = 192
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryModImpDF: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdModImpDF'
      ',cNmModImpDF'
      'FROM ModImpDF')
    Left = 784
    Top = 192
    object qryModImpDFnCdModImpDF: TIntegerField
      FieldName = 'nCdModImpDF'
    end
    object qryModImpDFcNmModImpDF: TStringField
      FieldName = 'cNmModImpDF'
      Size = 50
    end
  end
  object qryVerificaSerieFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdTipoDoctoFiscal'
        Size = -1
        Value = Null
      end
      item
        Name = 'cModelo'
        Size = -1
        Value = Null
      end
      item
        Name = 'cSerie'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM SerieFiscal'
      ' WHERE ISNULL(nCdSerieFiscal,0)    <> :nPK'
      '   AND ISNULL(nCdEmpresa,0)         = :nCdEmpresa'
      '   AND ISNULL(nCdTipoDoctoFiscal,0) = :nCdTipoDoctoFiscal'
      '   AND ISNULL(cModelo,'#39#39')           = :cModelo'
      '   AND ISNULL(cSerie,'#39#39')            = :cSerie')
    Left = 816
    Top = 256
  end
  object qrySerieFiscalLoja: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qrySerieFiscalLojaBeforePost
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM SerieFiscalLoja'
      ' WHERE nCdSerieFiscal = :nPK')
    Left = 624
    Top = 192
    object qrySerieFiscalLojanCdSerieFiscalLoja: TIntegerField
      FieldName = 'nCdSerieFiscalLoja'
    end
    object qrySerieFiscalLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qrySerieFiscalLojanCdSerieFiscal: TIntegerField
      FieldName = 'nCdSerieFiscal'
    end
    object qrySerieFiscalLojacNmLoja: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmLoja'
      LookupDataSet = qryLoja
      LookupKeyFields = 'nCdLoja'
      LookupResultField = 'cNmLoja'
      KeyFields = 'nCdLoja'
      LookupCache = True
      Size = 50
      Lookup = True
    end
  end
  object dsSerieFiscalLoja: TDataSource
    DataSet = qrySerieFiscalLoja
    Left = 624
    Top = 224
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Loja')
    Left = 816
    Top = 224
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryVerificaSerieFiscalLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdSerieFiscal'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSerieFiscal'
      '  FROM SerieFiscalLoja'
      ' WHERE nCdLoja         = :nCdLoja'
      '   AND nCdSerieFiscal <> :nCdSerieFiscal')
    Left = 784
    Top = 224
    object qryVerificaSerieFiscalLojanCdSerieFiscal: TIntegerField
      FieldName = 'nCdSerieFiscal'
    end
  end
  object qryNumDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT NumDoctoFiscal.nCdSerieFiscal'
      '      ,NumDoctoFiscal.dDtInclusao'
      '      ,NumDoctoFiscal.iUltimoNumero'
      '      ,DoctoFiscal.nCdDoctoFiscal'
      '      ,DoctoFiscal.cTextoCFOP'
      '  FROM NumDoctoFiscal'
      
        '       INNER JOIN DoctoFiscal ON DoctoFiscal.nCdDoctoFiscal = Nu' +
        'mDoctoFiscal.nCdDoctoFiscal'
      ' WHERE NumDoctoFiscal.nCdSerieFiscal = :nPK'
      ' ORDER BY iUltimoNumero')
    Left = 656
    Top = 192
    object qryNumDoctoFiscalnCdSerieFiscal: TIntegerField
      FieldName = 'nCdSerieFiscal'
    end
    object qryNumDoctoFiscalnCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryNumDoctoFiscaliUltimoNumero: TIntegerField
      FieldName = 'iUltimoNumero'
    end
    object qryNumDoctoFiscaldDtInclusao: TDateTimeField
      FieldName = 'dDtInclusao'
    end
    object qryNumDoctoFiscalcTextoCFOP: TStringField
      FieldName = 'cTextoCFOP'
      Size = 50
    end
  end
  object dsNumDoctoFiscal: TDataSource
    AutoEdit = False
    DataSet = qryNumDoctoFiscal
    Left = 656
    Top = 224
  end
  object qrySerieFiscalInutil: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT SerieFiscalInutil.*'
      
        '      ,CAST(Usuario.nCdUsuario as varchar) + '#39' - '#39' + Usuario.cNm' +
        'Usuario as cNmUsuario'
      '  FROM SerieFiscalInutil'
      
        '       INNER JOIN Usuario ON Usuario.nCdUsuario = SerieFiscalInu' +
        'til.nCdUsuarioCad'
      ' WHERE nCdSerieFiscal = :nPK'
      ' ORDER BY iNrInutil DESC')
    Left = 688
    Top = 192
    object qrySerieFiscalInutilnCdSerieFiscal: TIntegerField
      FieldName = 'nCdSerieFiscal'
    end
    object qrySerieFiscalInutiliNrInutil: TIntegerField
      FieldName = 'iNrInutil'
    end
    object qrySerieFiscalInutilnCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qrySerieFiscalInutildDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qrySerieFiscalInutilcMotivoInutil: TStringField
      FieldName = 'cMotivoInutil'
      Size = 300
    end
    object qrySerieFiscalInutilnCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qrySerieFiscalInutildDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qrySerieFiscalInutilcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      ReadOnly = True
      Size = 83
    end
  end
  object dsSerieFiscalInutil: TDataSource
    AutoEdit = False
    DataSet = qrySerieFiscalInutil
    Left = 688
    Top = 224
  end
  object qryVerificaInutilizacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdSerieFiscal'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'iNrInutil'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTipoDocto'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdDoctoFiscal int'
      '       ,@nCdSerieFiscal int'
      '       ,@iNrInutil      int'
      '       ,@nCdTipoDocto   int'
      ''
      'SET @nCdDoctoFiscal = 0'
      'SET @nCdSerieFiscal = :nCdSerieFiscal'
      'SET @iNrInutil      = :iNrInutil'
      'SET @nCdTipoDocto   = :nCdTipoDocto'
      ''
      'SELECT @nCdDoctoFiscal = nCdDoctoFiscal'
      '  FROM DoctoFiscal'
      ' WHERE nCdSerieFiscal     = @nCdSerieFiscal'
      '   AND nCdTipoDoctoFiscal = @nCdTipoDocto'
      '   AND iNrDocto           = @iNrInutil'
      ''
      'IF (ISNULL(@nCdDoctoFiscal,0) = 0)'
      'BEGIN'
      '    SELECT @nCdDoctoFiscal = nCdDoctoFiscal'
      '      FROM NumDoctoFiscal'
      '     WHERE nCdSerieFiscal = @nCdSerieFiscal'
      '       AND iUltimoNumero  = @iNrInutil'
      'END'
      '   '
      'SELECT @nCdDoctoFiscal as nCdDoctoFiscal')
    Left = 688
    Top = 256
    object qryVerificaInutilizacaonCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
      ReadOnly = True
    end
  end
  object qryVerificaSerieFiscalInutil: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdSerieFiscal'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'iNrInutil'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM SerieFiscalInutil'
      ' WHERE nCdSerieFiscal = :nCdSerieFiscal'
      '   AND iNrInutil      = :iNrInutil')
    Left = 688
    Top = 288
  end
end
