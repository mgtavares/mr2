unit fLanctoPlanoConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask;

type
  TfrmLanctoPlanoConta = class(TfrmCadastro_Padrao)
    qryMasternCdLanctoPlanoConta: TAutoIncField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdUnidadeNegocio: TIntegerField;
    qryMasternCdGrupoPlanoConta: TIntegerField;
    qryMasternCdPlanoConta: TIntegerField;
    qryMasterdDtCompetencia: TDateTimeField;
    qryMasternCdSP: TIntegerField;
    qryMasternCdTitulo: TIntegerField;
    qryMasterdDtProcesso: TDateTimeField;
    qryMasternValLancto: TBCDField;
    qryMasternCdPedido: TIntegerField;
    qryMasternCdItemPedido: TIntegerField;
    qryMasternCdProduto: TIntegerField;
    qryMasternCdLanctoFin: TIntegerField;
    qryMastercFlgManual: TIntegerField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMastercOBS: TMemoField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBMemo1: TDBMemo;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit9: TDBEdit;
    DataSource1: TDataSource;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    DBEdit10: TDBEdit;
    DataSource2: TDataSource;
    qryPlanoConta: TADOQuery;
    qryPlanoContanCdPlanoConta: TIntegerField;
    qryPlanoContacNmPlanoConta: TStringField;
    qryPlanoContacTipoConta: TStringField;
    DBEdit11: TDBEdit;
    DataSource3: TDataSource;
    Label11: TLabel;
    DBEdit12: TDBEdit;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DBEdit13: TDBEdit;
    DataSource4: TDataSource;
    qryPlanoContanCdGrupoPlanoConta: TIntegerField;
    qryMasternCdRecebimento: TIntegerField;
    GroupBox1: TGroupBox;
    Label10: TLabel;
    DBEdit14: TDBEdit;
    Label12: TLabel;
    DBEdit15: TDBEdit;
    Label13: TLabel;
    DBEdit16: TDBEdit;
    Label14: TLabel;
    DBEdit17: TDBEdit;
    Label15: TLabel;
    DBEdit18: TDBEdit;
    Label16: TLabel;
    DBEdit19: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLanctoPlanoConta: TfrmLanctoPlanoConta;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmLanctoPlanoConta.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'LANCTOPLANOCONTA' ;
  nCdTabelaSistema  := 79 ;
  nCdConsultaPadrao := 182 ;

  bCodigoAutomatico := false ;
  
end;

procedure TfrmLanctoPlanoConta.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;

end;

procedure TfrmLanctoPlanoConta.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;

  btSalvar.Visible  := True ;
  btExcluir.Visible := True ;

  DBEdit3.SetFocus ;

end;

procedure TfrmLanctoPlanoConta.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (DBEdit10.text = '') then
  begin
      MensagemAlerta('Informe a unidade de neg�cio.') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (DBEdit11.text = '') then
  begin
      MensagemAlerta('Informe a conta.') ;
      DBEdit4.SetFocus;
      abort ;
  end ;

  if (DBEdit5.text = '') then
  begin
      MensagemAlerta('Informe a data de compet�ncia.') ;
      DBEdit5.SetFocus;
      abort ;
  end ;

  if (DBEdit7.Text = '') or (DBEdit7.Text = '0') then
  begin
      MensagemAlerta('Informe o valor do lan�amento.') ;
      DBEdit7.SetFocus;
      abort ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      qryMastercFlgManual.Value    := 1 ;
      qryMasterdDtProcesso.Value   := Now() ;
      qryMasternCdUsuarioCad.Value := frmMenu.nCdUsuarioLogado;
  end ;

  qryMasternCdGrupoPlanoConta.Value := qryPlanoContanCdGrupoPlanoConta.Value ;

end;

procedure TfrmLanctoPlanoConta.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryUnidadeNegocio.Close ;
  PosicionaQuery(qryUnidadeNegocio, DBEdit3.Text) ;

end;

procedure TfrmLanctoPlanoConta.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryPlanoConta.Close ;
  PosicionaQuery(qryPlanoConta, DBedit4.Text) ;

end;

procedure TfrmLanctoPlanoConta.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  btSalvar.Visible  := (qryMastercFlgManual.Value = 1) ;
  btExcluir.Visible := (qryMastercFlgManual.Value = 1) ;

  qryEmpresa.Close ;
  qryUnidadeNegocio.Close ;
  qryPlanoConta.Close ;
  qryUsuario.Close ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  PosicionaQuery(qryUnidadeNegocio, qryMasternCdUnidadeNegocio.asString) ;
  PosicionaQuery(qryPlanoConta, qryMasternCdPlanoConta.AsString) ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuarioCad.AsString) ;

end;

procedure TfrmLanctoPlanoConta.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qryEmpresa.Close ;
  qryUnidadeNegocio.Close ;
  qryPlanoConta.Close ;
  qryUsuario.Close ;

end;

procedure TfrmLanctoPlanoConta.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(110);

            If (nPK > 0) then
                qryMasternCdUnidadeNegocio.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmLanctoPlanoConta.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(41);

            If (nPK > 0) then
                qryMasternCdPlanoConta.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmLanctoPlanoConta.btCancelarClick(Sender: TObject);
begin
  inherited;
  btSalvar.Visible  := True ;
  btExcluir.Visible := True ;

end;

initialization
    RegisterClass(TfrmLanctoPlanoConta) ;

end.
