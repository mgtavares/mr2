inherited rptClientesComprasPeriodo: TrptClientesComprasPeriodo
  Left = 316
  Top = 170
  Width = 848
  Height = 358
  Caption = 'rptClientesComprasPeriodo'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 832
    Height = 296
  end
  object Label2: TLabel [1]
    Left = 31
    Top = 48
    Width = 67
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja Cadastro'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [2]
    Left = 7
    Top = 72
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo de Compra'
  end
  object Label6: TLabel [3]
    Left = 188
    Top = 72
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    Width = 832
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit1: TDBEdit [5]
    Left = 104
    Top = 40
    Width = 65
    Height = 21
    DataField = 'nCdLoja'
    DataSource = dsLoja
    TabOrder = 1
    OnExit = DBEdit1Exit
    OnKeyDown = DBEdit1KeyDown
  end
  object DBEdit2: TDBEdit [6]
    Tag = 1
    Left = 172
    Top = 40
    Width = 654
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 2
  end
  object MaskEdit1: TMaskEdit [7]
    Left = 104
    Top = 64
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [8]
    Left = 216
    Top = 64
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object GroupSexo: TRadioGroup [9]
    Left = 104
    Top = 96
    Width = 297
    Height = 33
    Caption = 'Sexo'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'Masculino'
      'Feminino')
    TabOrder = 5
  end
  object GroupModelo: TRadioGroup [10]
    Left = 104
    Top = 140
    Width = 209
    Height = 33
    Caption = ' Modelo '
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Planilha')
    TabOrder = 6
  end
  inherited ImageList1: TImageList
    Left = 112
    Top = 280
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja =:nPK')
    Left = 144
    Top = 280
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 176
    Top = 280
  end
  object ER2Excel1: TER2Excel
    Titulo = 'Aniversariantes do M'#234's'
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 216
    Top = 280
  end
end
