unit rVendaFatDia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons, comObj ;

type
  TrptVendaFatDia = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    MaskEdit3: TMaskEdit;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptVendaFatDia: TrptVendaFatDia;

implementation

uses fMenu, fLookup_Padrao, QRExport , rVendaFatDia_view;

{$R *.dfm}

procedure TrptVendaFatDia.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin

  iAux := frmMenu.nCdEmpresaAtiva;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  if (iAux > 0) then
  begin

    MaskEdit3.Text := IntToStr(iAux) ;

    qryEmpresa.Close ;
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := iAux ;
    qryEmpresa.Open ;

  end ;

  MaskEdit3.SetFocus ;

  inherited;
  
end;


procedure TrptVendaFatDia.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

function MesExtenso(dData: TDateTime):string;
begin

    if StrToInt(Copy(DateToStr(dData),4,2)) = 1 then
        result := 'jan/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 2 then
        result := 'fev/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 3 then
        result := 'mar/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 4 then
        result := 'abr/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 5 then
        result := 'mai/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 6 then
        result := 'jun/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 7 then
        result := 'jul/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 8 then
        result := 'ago/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 9 then
        result := 'set/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 10 then
        result := 'out/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 11 then
        result := 'nov/' + Copy(DateToStr(dData),7,4) ;

    if StrToInt(Copy(DateToStr(dData),4,2)) = 12 then
        result := 'dez/' + Copy(DateToStr(dData),7,4) ;

end ;

procedure TrptVendaFatDia.ToolButton1Click(Sender: TObject);
var
    objRel : TrptVendaFatDia_view;
    dAux   : TDateTime ;
    dAux1, dAux2, dAux3, dAux4, dAux5, dAux6 : TDateTime ;
    dAux7, dAux8, dAux9, dAux10, dAux11, dAux12 : TDateTime ;

var linha, coluna : integer;
var planilha : variant;
var valorcampo : string;

begin

  If (Trim(MaskEdit3.Text) = '') or (DBEDit3.Text = '') Then
  begin
      ShowMessage('Informe a Empresa.') ;
      exit ;
  end ;

  if (Length(Trim(MaskEdit1.Text)) <> 7) then
  begin
      ShowMessage('Compet�ncia Inicial Inv�lida.') ;
      exit ;
  end ;

  try
      dAux := StrToDateTime('01/' + MaskEdit1.Text) ;
  except
      ShowMessage('Compet�ncia Inicial Inv�lida.') ;
      exit ;
  end ;

  objRel := TrptVendaFatDia_view.Create(nil);

  Try
      Try

          objRel.qryPreparaTemp.ExecSQL ;

          objRel.usp_Relatorio.Close ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value   := frmMenu.ConvInteiro(MaskEdit3.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@cCompetencia').Value := Trim(MaskEdit1.Text) ;
          objRel.usp_Relatorio.ExecProc ;

          objRel.qryTemp.Close ;
          objRel.qryTemp.Open ;

          objRel.qryTemp_Dia.Close ;
          objRel.qryTemp_Dia.Open ;

          {  if (RadioGroup1.ItemIndex = 1) then
          begin

              planilha:= CreateoleObject('Excel.Application');
              planilha.WorkBooks.add(1);
              planilha.caption := 'DRE - Demonstrativo de Resultados do Exerc�cio';
              planilha.visible := true;

              for linha := 0 to rptDRE_view.usp_Relatorio.RecordCount - 1 do
              begin
                  for coluna := 1 to rptDRE_view.usp_Relatorio.FieldCount do
                  begin
                      valorcampo := rptDRE_view.usp_Relatorio.Fields[coluna - 1].AsString;
                      planilha.cells[linha + 2,coluna] := valorCampo;
                  end;
                  rptDRE_view.usp_Relatorio.Next;
              end;
              for coluna := 1 to rptDRE_view.usp_Relatorio.FieldCount do
              begin

              valorcampo := rptDRE_view.usp_Relatorio.Fields[coluna - 1].DisplayLabel;
              planilha.cells[1,coluna] := valorcampo;
          end;
          planilha.columns.Autofit;
          end
          else
          begin   }

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := 'Empresa    : ' + DbEdit3.Text ;
          objRel.lblFiltro3.Caption := 'Compet�ncia: ' + MaskEdit1.Text ;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do Relat�rio');
          raise;
      end;
   finally;
      FreeAndNil(objRel);
   end;

  //end ;

end;


procedure TrptVendaFatDia.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

initialization
     RegisterClass(TrptVendaFatDia) ;

end.
