unit fMonitorProducao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, DBGridEhGrouping, Menus, DBCtrls, StdCtrls,
  Mask, ER2Lookup;

type
  TfrmMonitorProducao = class(TfrmProcesso_Padrao)
    ToolButton4: TToolButton;
    edtEmpresa: TER2LookupMaskEdit;
    edtTerceiro: TER2LookupMaskEdit;
    edtGrupoEconomico: TER2LookupMaskEdit;
    edtGrupoTerceiro: TER2LookupMaskEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    DBEdit3: TDBEdit;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    DBEdit4: TDBEdit;
    DataSource3: TDataSource;
    qryGrupoTerceiro: TADOQuery;
    qryGrupoTerceironCdGrupoTerceiro: TIntegerField;
    qryGrupoTerceirocNmGrupoTerceiro: TStringField;
    DBEdit5: TDBEdit;
    DataSource4: TDataSource;
    chkPedidoNovo: TCheckBox;
    chkPedidoIniciado: TCheckBox;
    chkPedidoParcial: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMonitorProducao: TfrmMonitorProducao;

implementation

uses fMenu, fMonitorProducao_Exibe;

{$R *.dfm}

procedure TfrmMonitorProducao.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  
end;

procedure TfrmMonitorProducao.ToolButton1Click(Sender: TObject);
var
  objForm : TfrmMonitorProducao_Exibe;
begin
  inherited;
  objForm := TfrmMonitorProducao_Exibe.Create( Self );
  objForm.nCdEmpresa         := frmMenu.ConvInteiro( edtEmpresa.Text );
  objForm.nCdTerceiro        := frmMenu.ConvInteiro( edtTerceiro.Text );
  objForm.nCdGrupoEconomico  := frmMenu.ConvInteiro( edtGrupoEconomico.Text );
  objForm.nCdGrupoTerceiro   := frmMenu.ConvInteiro( edtGrupoTerceiro.Text ) ;
  objForm.bFlgPedidoNovo     := chkPedidoNovo.Checked;
  objForm.bFlgPedidoIniciado := chkPedidoIniciado.Checked;
  objForm.bFlgPedidoParcial  := chkPedidoParcial.Checked;
  objForm.WindowState        := wsMaximized;
  showForm( objForm , TRUE ) ;

end;

initialization
    RegisterClass(TfrmMonitorProducao) ;

end.
