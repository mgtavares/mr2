unit fGeraRemessaRecebimento_titulo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DBGridEhGrouping, DB, ADODB, GridsEh,
  DBGridEh, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  cxLookAndFeelPainters, cxButtons, FileCtrl,ComObj;

type
  TfrmGeraRemessaRecebimento_titulo = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryTitulosRemessa: TADOQuery;
    qryTitulosRemessanCdTitulo: TIntegerField;
    qryTitulosRemessacNrTit: TStringField;
    qryTitulosRemessacNrNF: TStringField;
    qryTitulosRemessadDtVenc: TDateTimeField;
    qryTitulosRemessaiParcela: TIntegerField;
    qryTitulosRemessanCdTerceiro: TIntegerField;
    qryTitulosRemessacNmTerceiro: TStringField;
    qryTitulosRemessanSaldoTit: TBCDField;
    dsTitulosRemessa: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    cxButton1: TcxButton;
    edtDiretorio: TEdit;
    qryTitulosRemessacDiretorioArquivoRemessa: TStringField;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancariacCedenteAgencia: TStringField;
    qryContaBancariacCedenteAgenciaDigito: TStringField;
    qryContaBancariacCedenteConta: TStringField;
    qryContaBancariacCedenteContaDigito: TStringField;
    qryContaBancariacCedenteCodCedente: TStringField;
    qryContaBancariacDiretorioArquivoLicenca: TStringField;
    qryContaBancariacPrimeiraInstrucaoBoleto: TStringField;
    qryContaBancariacSegundaInstrucaoBoleto: TStringField;
    qryContaBancariaiUltimoBoleto: TIntegerField;
    qryContaBancariacFlgAceite: TIntegerField;
    qryContaBancariaiDiasProtesto: TIntegerField;
    qryEndereco: TADOQuery;
    qryEndereconCdEndereco: TIntegerField;
    qryEndereconCdTerceiro: TIntegerField;
    qryEnderecocEndereco: TStringField;
    qryEnderecocBairro: TStringField;
    qryEnderecocCidade: TStringField;
    qryEnderecocUF: TStringField;
    qryEnderecocCEP: TStringField;
    qryEnderecocNmTerceiro: TStringField;
    qryEnderecocCNPJCPF: TStringField;
    qryTabelaJuros: TADOQuery;
    qryTabelaJurosnPercMulta: TBCDField;
    qryTabelaJurosnPercJuroDia: TBCDField;
    qryTitulosRemessanCdContaBancaria: TIntegerField;
    qryTitulosRemessadDtEmissao: TDateTimeField;
    qryContaBancariaiSeqArquivoRemessa: TIntegerField;
    qryContaBancarianCdTabTipoLayOutRemessa: TIntegerField;
    qryBorderoFinanceiro: TADOQuery;
    qryBorderoFinanceironCdBorderoFinanceiro: TIntegerField;
    qryBorderoFinanceirocNmArquivoRemessa: TStringField;
    qryBorderoFinanceirocNmComputadorGeracao: TStringField;
    qryTitulosRemessanCdBorderoFinanceiro: TIntegerField;
    qryBorderoFinanceiroiNrSeqArquivoRemessa: TIntegerField;
    procedure cxButton1Click(Sender: TObject);
    procedure qryTitulosRemessaAfterOpen(DataSet: TDataSet);
    procedure GeraArquivoRemessa;
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    cArquivoRemessa : String;
    function TrataMascara(Mascara, Valor: String): String;
  public
    { Public declarations }
  end;

var
  frmGeraRemessaRecebimento_titulo: TfrmGeraRemessaRecebimento_titulo;

implementation

uses fMenu, strUtils;

{$R *.dfm}

procedure TfrmGeraRemessaRecebimento_titulo.cxButton1Click(
  Sender: TObject);
var
  cDiretorio : String;
begin
  inherited;

  cDiretorio := qryTitulosRemessacDiretorioArquivoRemessa.Value;

  SelectDirectory('Selecione o Diret�rio do Arquivo de Remessa','',cDiretorio);

  if (cDiretorio <> '') then
      edtDiretorio.Text := cDiretorio;
  
end;

procedure TfrmGeraRemessaRecebimento_titulo.GeraArquivoRemessa;
var
   CobreBemX      : Variant;
   Boleto         : Variant;
begin

    qryContaBancaria.Close;
    qryContaBancaria.Parameters.ParamByName('nPK').Value := qryTitulosRemessanCdContaBancaria.Value;
    qryContaBancaria.Open;

    qryTabelaJuros.Close;
    qryTabelaJuros.Open;

    if (qryTabelaJuros.Eof) then
    begin
        MensagemAlerta('Tabela de juros n�o cadastrada ou vencida para a esp�cie de Titulo de Boleto, verifique.');
        abort;
    end;

    if not DirectoryExists(ExtractFilePath(Application.ExeName) + 'Boleto\Imagens\') then
    begin
        MensagemAlerta('"' + ExtractFilePath(Application.ExeName) + 'Boleto\Imagens". Diret�rio de imagens n�o encontrado.');
        abort;
    end;

    CobreBemX := CreateOleObject('CobreBemX.ContaCorrente');

    try
        CobreBemX.ArquivoLicenca      := qryContaBancariacDiretorioArquivoLicenca.Value;
        CobreBemX.CodigoAgencia       := TrataMascara(CobreBemX.MascaraCodigoAgencia, qryContaBancariacCedenteAgencia.Value + qryContaBancariacCedenteAgenciaDigito.Value);
        CobreBemX.NumeroContaCorrente := TrataMascara(CobreBemX.MascaraContaCorrente, qryContaBancariacCedenteConta.Value + qryContaBancariacCedenteContaDigito.Value);
        CobreBemX.CodigoCedente       := TrataMascara(CobreBemX.MascaraCodigoCedente, qryContaBancariacCedenteCodCedente.Value);
        CobreBemX.InicioNossoNumero   := '00000000001';
        CobreBemX.FimNossoNumero      := '02147483647';

        CobreBemX.PadroesBoleto.PadroesBoletoImpresso.CaminhoImagensCodigoBarras := ExtractFilePath(Application.ExeName) + 'Boleto\Imagens\';
    except
        MensagemAlerta('Arquivo de Lincen�a inv�lido ou inexistente. Caminho: "' + qryContaBancariacDiretorioArquivoLicenca.Value + '"');
        abort;
    end;

    qryTitulosRemessa.First;

    while not qryTitulosRemessa.Eof do
    begin

        qryEndereco.Close;
        qryEndereco.Parameters.ParamByName('nCdTerceiro').Value := qryTitulosRemessanCdTerceiro.Value;
        qryEndereco.Open;

        Boleto                               := CobreBemX.DocumentosCobranca.Add;
        Boleto.NumeroDocumento               := qryTitulosRemessacNrTit.Value;
        Boleto.NomeSacado                    := qryEnderecocNmTerceiro.Value;
        Boleto.CPFSacado                     := qryEnderecocCNPJCPF.Value;
        Boleto.EnderecoSacado                := qryEnderecocEndereco.Value;
        Boleto.BairroSacado                  := qryEnderecocBairro.Value;
        Boleto.CidadeSacado                  := qryEnderecocCidade.Value;
        Boleto.EstadoSacado                  := qryEnderecocUF.Value;
        Boleto.CepSacado                     := qryEnderecocCEP.Value;
        Boleto.DataDocumento                 := qryTitulosRemessadDtEmissao.AsString;
        Boleto.DataVencimento                := qryTitulosRemessadDtVenc.AsString;
        Boleto.ValorDocumento                := qryTitulosRemessanSaldoTit.Value;
        Boleto.NossoNumero                   := qryTitulosRemessanCdTitulo.Value;
        Boleto.CalculaDacNossoNumero         := True;
        Boleto.PercentualJurosDiaAtraso      := qryTabelaJurosnPercJuroDia.Value;
        Boleto.PercentualMultaAtraso         := qryTabelaJurosnPercMulta.Value;
        Boleto.DiasProtesto                  := qryContaBancariaiDiasProtesto.Value;

        Boleto.PadroesBoleto.InstrucoesCaixa := '<font size="1">'
                                               + qryContaBancariacPrimeiraInstrucaoBoleto.Value + '<br>'
                                               + qryContaBancariacSegundaInstrucaoBoleto.Value + '<br>'
                                               + '</font>';

        Boleto.TipoDocumentoCobranca         := 'DM';

        if (qryContaBancariacFlgAceite.Value = 1) then
            Boleto.Aceite := 'S';

        qryTitulosRemessa.Next;
    end;

    qryBorderoFinanceiro.Close;
    PosicionaQuery(qryBorderoFinanceiro, qryTitulosRemessanCdBorderoFinanceiro.AsString);

    {-- diret�rio onde o arquivo de remessa ser� salvo --}
    CobreBemX.ArquivoRemessa.Diretorio := edtDiretorio.text;

    {-- gera o nome do arquivo de remessa --}
    if (Trim(qryBorderoFinanceirocNmArquivoRemessa.Value) = '') then
    begin
        CobreBemX.ArquivoRemessa.Sequencia := qryContaBancariaiSeqArquivoRemessa.Value + 1;
        cArquivoRemessa                    := 'CB' + frmMenu.ZeroEsquerda(IntToStr(CobreBemX.ArquivoRemessa.Sequencia),6) + '.REM';
    end
    else begin
        CobreBemX.ArquivoRemessa.Sequencia := qryBorderoFinanceiroiNrSeqArquivoRemessa.Value;
        cArquivoRemessa                    := qryBorderoFinanceirocNmArquivoRemessa.Value;
    end;

    CobreBemX.ArquivoRemessa.Arquivo := cArquivoRemessa;

    {-- pega o tipo de layout a ser utilizado --}
    if (qryContaBancarianCdTabTipoLayOutRemessa.Value = 240) then
        CobreBemX.ArquivoRemessa.Layout    := 'FEBRABAN240'
    else if (qryContaBancarianCdTabTipoLayOutRemessa.Value = 400) then
        CobreBemX.ArquivoRemessa.Layout    := 'CNAB400';

    try
        {-- gera o arquivo --}
        CobreBemX.GravaArquivoRemessa;

        {-- atualiza os dados do bordero de remessa --}
        if (Trim(qryBorderoFinanceirocNmArquivoRemessa.Value) = '') then
        begin
            qryContaBancaria.Edit;
            qryContaBancariaiSeqArquivoRemessa.Value := qryContaBancariaiSeqArquivoRemessa.Value + 1;
            qryContaBancaria.Post;

            qryBorderoFinanceiro.Edit;
            qryBorderoFinanceirocNmArquivoRemessa.Value    := cArquivoRemessa;
            qryBorderoFinanceirocNmComputadorGeracao.Value := frmMenu.cNomeComputador;
            qryBorderoFinanceiroiNrSeqArquivoRemessa.Value := qryContaBancariaiSeqArquivoRemessa.Value;
            qryBorderoFinanceiro.Post;
        end;

    except
        raise;
    end ;

    CobreBemX := Unassigned;
end;

procedure TfrmGeraRemessaRecebimento_titulo.qryTitulosRemessaAfterOpen(
  DataSet: TDataSet);
begin
  inherited;

  edtDiretorio.Text := qryTitulosRemessacDiretorioArquivoRemessa.Value;
end;

procedure TfrmGeraRemessaRecebimento_titulo.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (MessageDLG('Confirma a gera��o do arquivo de remessa',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit;

  try
      GeraArquivoRemessa;
  except
      MensagemErro('Erro ao gerar arquivo de remessa.');
      raise;
  end;

  ShowMessage('Arquivo de remessa gerado com sucesso.');

  Close;

end;

function TfrmGeraRemessaRecebimento_titulo.TrataMascara(Mascara, Valor: String): String;
var
    Count        : integer;
    PSeparador   : integer;
    DV           : String;
    ContaAgencia : String;
begin

    if (Trim(Mascara) <> '') then
    begin

        PSeparador := Pos('-',Mascara);

        if (PSeparador > 0) then
        begin

            Count := Length(Mascara) - PSeparador;

            DV           := '-' + Copy(ReverseString(Valor),0, Count);
            ContaAgencia := Copy(Valor,0,Length(Valor) - Count);

            Count := Length(Copy(Mascara,0,PSeparador - 1));

            if (Length(ContaAgencia) < Count) then
                ContaAgencia := frmMenu.ZeroEsquerda(ContaAgencia,Count)
            else if (Length(ContaAgencia) > Count) then
                ContaAgencia := ReverseString(Copy(ReverseString(ContaAgencia),0,Count));

            Result := ContaAgencia + DV;
        end else
        begin

            SetLength(Valor,Length(Mascara));
            
            Result := Valor;

        end;

    end else
        Result := Valor;

end;

end.
