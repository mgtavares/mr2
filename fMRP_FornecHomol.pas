unit fMRP_FornecHomol;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmMRP_FornecHomol = class(TfrmProcesso_Padrao)
    qryFornecedores: TADOQuery;
    qryFornecedoresnCdTerceiro: TIntegerField;
    qryFornecedorescNmTerceiro: TStringField;
    qryFornecedoresnPercentCompra: TBCDField;
    qryFornecedorescNrContrato: TStringField;
    qryFornecedoresdDtValidadeIni: TDateTimeField;
    qryFornecedoresdDtValidadeFim: TDateTimeField;
    dsFornecedores: TDataSource;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nPercentCompra: TcxGridDBColumn;
    cxGrid1DBTableView1cNrContrato: TcxGridDBColumn;
    cxGrid1DBTableView1dDtValidadeIni: TcxGridDBColumn;
    cxGrid1DBTableView1dDtValidadeFim: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMRP_FornecHomol: TfrmMRP_FornecHomol;

implementation

{$R *.dfm}

end.
