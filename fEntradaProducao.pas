unit fEntradaProducao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, ER2Lookup, cxLookAndFeelPainters,
  cxButtons;

type
  TfrmEntradaProducao = class(TfrmCadastro_Padrao)
    qryMasternCdEntradaProducao: TIntegerField;
    qryMasternCdOrdemProducao: TIntegerField;
    qryMastercDocumento: TStringField;
    qryMasternCdProduto: TIntegerField;
    qryMasternCdTabTipoEntradaProducao: TIntegerField;
    qryMasternQtdeEntrada: TBCDField;
    qryMasternCdLocalEstoque: TIntegerField;
    qryMasterdDtEntrada: TDateTimeField;
    qryMasternCdUsuario: TIntegerField;
    qryMastercOBSLivro: TStringField;
    qryMastercOBSAdicional: TMemoField;
    qryMastercNumeroOP: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    qryProduto: TADOQuery;
    qryProdutocNmProduto: TStringField;
    DBEdit8: TDBEdit;
    DataSource1: TDataSource;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DBEdit9: TDBEdit;
    DataSource2: TDataSource;
    qryTabTipoEntradaProducao: TADOQuery;
    qryTabTipoEntradaProducaocNmTabTipoEntradaProducao: TStringField;
    DBEdit10: TDBEdit;
    DataSource3: TDataSource;
    Label8: TLabel;
    DBEdit11: TDBEdit;
    qryOrdemProducao: TADOQuery;
    qryOrdemProducaonCdOrdemProducao: TIntegerField;
    qryOrdemProducaonCdProduto: TIntegerField;
    qryOrdemProducaonCdLocalEstoque: TIntegerField;
    qryOrdemProducaonSaldoQtde: TBCDField;
    qryDescobreNumeroOP: TADOQuery;
    qryDescobreNumeroOPcNumeroOP: TStringField;
    Label9: TLabel;
    DBEdit12: TDBEdit;
    Label10: TLabel;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    DBEdit14: TDBEdit;
    DataSource4: TDataSource;
    DBEdit6: TER2LookupDBEdit;
    DBEdit4: TER2LookupDBEdit;
    Label11: TLabel;
    DBEdit13: TDBEdit;
    DataSource5: TDataSource;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    SP_PROCESSA_CONCLUSAO_OP: TADOStoredProc;
    qryOrdemProducaonCdTabTipoStatusOP: TIntegerField;
    qryOrdemProducaocNmTabTipoStatusOP: TStringField;
    qryOrdemProducaocNmTipoOP: TStringField;
    Label12: TLabel;
    DBEdit15: TDBEdit;
    Label13: TLabel;
    DBEdit16: TDBEdit;
    btRegistroLoteSerial: TcxButton;
    qryPreparaTempMovLote: TADOQuery;
    SP_EFETIVA_TRANSACAO_LOTE: TADOStoredProc;
    qryMasternCdTipoPerdaProducao: TIntegerField;
    qryMasternCdMotivoPerdaProducao: TIntegerField;
    edtCdTipoPerda: TER2LookupDBEdit;
    edtCdMotivoPerda: TER2LookupDBEdit;
    Label14: TLabel;
    Label15: TLabel;
    qryTipoPerda: TADOQuery;
    qryTipoPerdacNmTipoPerdaProducao: TStringField;
    DBEdit17: TDBEdit;
    DataSource6: TDataSource;
    qryMotivoPerdaProducao: TADOQuery;
    qryMotivoPerdaProducaocNmMotivoPerdaProducao: TStringField;
    DBEdit18: TDBEdit;
    DataSource7: TDataSource;
    qryTabTipoEntradaProducaonCdTabTipoEntradaProducao: TIntegerField;
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure DBEdit5Exit(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure desativaCampo(objDBEdit: TDBEdit; bDesativar: boolean);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure btExcluirClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure btRegistroLoteSerialClick(Sender: TObject);
    procedure qryTabTipoEntradaProducaoAfterScroll(DataSet: TDataSet);
    procedure buscaOP;
    procedure registraConclusaoExterna( cNumeroOP : string ; nCdTabTipoEntradaProducao : integer ) ;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    bChamadaExterna : boolean ;
  public
    { Public declarations }
  end;

var
  frmEntradaProducao: TfrmEntradaProducao;

implementation

uses fLookup_Padrao, fMenu, fRegistroMovLote;

{$R *.dfm}

procedure TfrmEntradaProducao.btIncluirClick(Sender: TObject);
begin
  inherited;

  if not bChamadaExterna then
      DBEdit2.SetFocus;
  
end;

procedure TfrmEntradaProducao.DBEdit2Exit(Sender: TObject);
begin
  inherited;

  if not DBEdit2.ReadOnly then
  begin

      buscaOP() ;

  end ;


end;

procedure TfrmEntradaProducao.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  if ( DBEdit2.ReadOnly ) then
      exit ;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsBrowse) then
             qryMaster.Edit ;

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(742);

            If (nPK > 0) then
            begin

                PosicionaQuery( qryDescobreNumeroOP , intToStr( nPK )) ;

                if not ( qryDescobreNumeroOP.eof ) then
                begin

                    qryMastercNumeroOP.Value := qryDescobreNumeroOPcNumeroOP.Value ;
                    PosicionaQuery( qryOrdemProducao , qryMastercNumeroOP.Value ) ;

                end ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmEntradaProducao.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster   := 'ENTRADAPRODUCAO' ;
  nCdTabelaSistema  := 416 ;
  nCdConsultaPadrao := 744 ;
  bLimpaAposSalvar  := FALSE ;

end;

procedure TfrmEntradaProducao.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
  begin

      {-- conclus�o --}
      if (qryMasternCdTabTipoEntradaProducao.Value = 1) then
          qryMastercOBSLivro.Value := 'CONC. PROD. OP: ' + Trim( qryMastercNumeroOP.Value ) + ' EM: ' + DateTimeToStr(Now()) ;

      {-- retrabalho --}
      if (qryMasternCdTabTipoEntradaProducao.Value = 2) then
          qryMastercOBSLivro.Value := 'RETRABALHO PROD. OP: ' + Trim( qryMastercNumeroOP.Value ) + ' EM: ' + DateTimeToStr(Now()) ;

      {-- refugo --}
      if (qryMasternCdTabTipoEntradaProducao.Value = 3) then
          qryMastercOBSLivro.Value := 'REFUGO PROD. OP: ' + Trim( qryMastercNumeroOP.Value ) + ' EM: ' + DateTimeToStr(Now()) ;

      if ( Trim( qryMastercDocumento.Value ) <> '' ) then
          qryMastercOBSLivro.Value := qryMastercOBSLivro.Value + ' DOC: ' + Trim( qryMastercDocumento.Value ) ;

  end ;

end;

procedure TfrmEntradaProducao.ToolButton10Click(Sender: TObject);
var
    objForm        : TfrmRegistroMovLote ;
    nQtdeLote      : double ;
    cFlgGeraRework : integer ;
begin
  inherited;

  if (not qryMaster.Active) then
  begin

      MensagemAlerta('Nenhum registro de entrada ativo.') ;
      abort ;

  end ;

  if ( qryMasterdDtEntrada.asString <> '' ) then
  begin

      MensagemAlerta('Este registro de entrada j� foi processado.') ;
      abort ;

  end ;

  if (qryMaster.State in [dsInsert,dsEdit]) then
      qryMaster.Post ;

  if ( qryMasternQtdeEntrada.Value > qryOrdemProducaonSaldoQtde.Value ) then
      MensagemAlerta('A Quantidade registrada � maior que o saldo pendente da ordem de produ��o!') ;


  {-- verifica se o produto � controlado por lote/serial, caso seja, verifica se a quantidade especificada --}
  {-- nas informa��es do lote foram informadas corretamente                                                --}

  if (qryMasternCdTabTipoEntradaProducao.Value = 1) then {-- produ��o conclu�da --}
  begin

      objForm := TfrmRegistroMovLote.Create( Self ) ;

      try
          nQtdeLote := objForm.retornaQuantidadeRegistradaTemporaria( qryMasternCdProduto.Value
                                                                    , 1
                                                                    , qryMasternCdEntradaProducao.Value ) ;

          {-- quando o produto n�o tem lote/serial controlados a fun��o retorna -1 --}
          if (nQtdeLote <> -1) then
          begin

              if ( nQtdeLote <> qryMasternQtdeEntrada.Value ) then
              begin
                  MensagemAlerta('A quantidade especificada no Registro de Lote/Serial n�o confere com o total deste movimento.') ;
                  btRegistroLoteSerial.Click;
                  abort ;
              end ;

          end ;

      finally
          freeAndNil( objForm ) ;
      end ;

  end ;

  if ( MessageDlg('Confirma este registro da produ��o ? Esta opera��o n�o poder� ser estornada! ',mtConfirmation,[mbYes,mbNo],0) = MrNo ) then
      exit ;

  cFlgGeraRework := 0 ;

  if (qryMasternCdTabTipoEntradaProducao.Value = 2) then
  begin

    if ( MessageDlg('Deseja gerar uma ordem de retrabalho ?',mtConfirmation,[mbYes,mbNo],0) = MrYes ) then
        cFlgGeraRework := 1 ;

  end ;

  frmMenu.Connection.BeginTrans ;

  try

      {-- processa a conclus�o da OP --}
      SP_PROCESSA_CONCLUSAO_OP.Close;
      SP_PROCESSA_CONCLUSAO_OP.Parameters.ParamByName('@nCdOrdemProducao').Value          := qryMasternCdOrdemProducao.Value ;
      SP_PROCESSA_CONCLUSAO_OP.Parameters.ParamByName('@nCdEntradaProducao').Value        := qryMasternCdEntradaProducao.Value ;
      SP_PROCESSA_CONCLUSAO_OP.Parameters.ParamByName('@nCdLocalEstoque').Value           := qryMasternCdLocalEstoque.Value ;
      SP_PROCESSA_CONCLUSAO_OP.Parameters.ParamByName('@nQtdeEntrada').Value              := qryMasternQtdeEntrada.Value ;
      SP_PROCESSA_CONCLUSAO_OP.Parameters.ParamByName('@nCdTabTipoEntradaProducao').Value := qryMasternCdTabTipoEntradaProducao.Value ;
      SP_PROCESSA_CONCLUSAO_OP.Parameters.ParamByName('@nCdUsuario').Value                := frmMenu.nCdUsuarioLogado ;
      SP_PROCESSA_CONCLUSAO_OP.Parameters.ParamByName('@cFlgGeraRework').Value            := cFlgGeraRework;
      SP_PROCESSA_CONCLUSAO_OP.ExecProc;

      {-- processa as movimenta��es de lote --}

      if (qryMasternCdTabTipoEntradaProducao.Value = 1) then {-- produ��o conclu�da --}
      begin

          SP_EFETIVA_TRANSACAO_LOTE.Close;
          SP_EFETIVA_TRANSACAO_LOTE.Parameters.ParamByName('@nCdTabTipoOrigemMovLote').Value := 1 ;  {-- 1 = entrada produ��o --}
          SP_EFETIVA_TRANSACAO_LOTE.Parameters.ParamByName('@iIDRegistro').Value             := qryMasternCdEntradaProducao.Value;
          SP_EFETIVA_TRANSACAO_LOTE.Parameters.ParamByName('@nCdEmpresa').Value              := frmMenu.nCdEmpresaAtiva;
          SP_EFETIVA_TRANSACAO_LOTE.Parameters.ParamByName('@nCdUsuario').Value              := frmMenu.nCdUsuarioLogado;
          SP_EFETIVA_TRANSACAO_LOTE.ExecProc;

      end ;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans ;

  ShowMessage('Registro da produ��o processado com Sucesso.') ;

  btCancelar.Click;

  if (bChamadaExterna) then
      Close ;

end;

procedure TfrmEntradaProducao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryOrdemProducao.Close;
  qryProduto.Close;
  qryLocalEstoque.Close;
  qryTabTipoEntradaProducao.Close;
  qryUsuario.Close;

  PosicionaQuery( qryOrdemProducao          , qryMastercNumeroOP.asString ) ;
  PosicionaQuery( qryProduto                , qryMasternCdProduto.asString ) ;
  PosicionaQuery( qryLocalEstoque           , qryMasternCdLocalEstoque.asString ) ;
  PosicionaQuery( qryTabTipoEntradaProducao , qryMasternCdTabTipoEntradaProducao.asString ) ;
  PosicionaQuery( qryUsuario                , qryMasternCdUsuario.AsString ) ;

  desativaCampo( DBEdit2 , (qryMaster.State <> dsInsert) ) ;

end;

procedure TfrmEntradaProducao.desativaCampo(objDBEdit: TDBEdit;
  bDesativar: boolean);
begin

    if (bDesativar) then
    begin

        objDBEdit.ReadOnly   := True ;
        objDBEdit.Color      := $00E9E4E4 ;
        objDBEdit.Font.Color := clBlack ;
        objDBEdit.TabStop    := False ;

    end
    else
    begin

        objDBEdit.ReadOnly   := False   ;
        objDBEdit.Color      := clWhite ;
        objDBEdit.Font.Color := clBlack ;
        objDBEdit.TabStop    := True    ;

    end ;

end;

procedure TfrmEntradaProducao.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  desativaCampo( DBEdit2 , FALSE ) ;

  qryOrdemProducao.Close ;
  qryProduto.Close ;
  qryTabTipoEntradaProducao.Close ;
  qryLocalEstoque.Close ;
  qryUsuario.Close ;
  qryTipoPerda.Close;
  qryMotivoPerdaProducao.Close;
  
end;

procedure TfrmEntradaProducao.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryOrdemProducao.Eof) then
  begin

      MensagemAlerta('Informe a ordem de produ��o.') ;
      DBEdit2.SetFocus;
      abort ;

  end ;

  if not (qryOrdemProducaonCdTabTipoStatusOP.Value in [3,4]) then
  begin
      MensagemAlerta('Para fazer apontamento de produ��o a OP deve estar com status de autorizada ou em processo.') ;
      abort ;
  end ;

  if (DBEdit10.Text = '') then
  begin

      MensagemAlerta('Informe o tipo de entrada.') ;
      DBEdit4.SetFocus;
      abort ;

  end ;

  if (qryMasternQtdeEntrada.Value <= 0) then
  begin

      MensagemAlerta('Informe a quantidade conclu�da/recusada.') ;
      DBEdit5.SetFocus;
      abort ;

  end ;

  if (DBEdit17.Text = '') and (qryMasternCdTabTipoEntradaProducao.Value = 3) then
  begin

      MensagemAlerta('Informe o tipo de perda.') ;
      edtCdTipoPerda.SetFocus ;
      abort ;

  end ;

  if (DBEdit18.Text = '') and (qryMasternCdTabTipoEntradaProducao.Value = 3) then
  begin

      MensagemAlerta('Informe o motivo da perda.') ;
      edtCdTipoPerda.SetFocus ;
      abort ;

  end ;

  if (DBEdit9.Text = '') and (qryMasternCdTabTipoEntradaProducao.Value = 1) then
  begin

      MensagemAlerta('Informe o local de estoque.') ;
      DBEdit6.SetFocus;
      abort ;

  end ;

  inherited;

  if (qryMaster.State = dsInsert) then
  begin

      qryMasternCdOrdemProducao.Value := qryOrdemProducaonCdOrdemProducao.Value ;
      qryMasternCdProduto.Value       := qryOrdemProducaonCdProduto.Value ;

  end ;

end;

procedure TfrmEntradaProducao.btExcluirClick(Sender: TObject);
begin

  if (not qryMaster.Active) then
  begin

      MensagemAlerta('Nenhum registro de entrada ativo.') ;
      abort ;

  end ;

  if ( qryMasterdDtEntrada.asString <> '' ) then
  begin

      MensagemAlerta('Este registro de entrada j� foi processado.') ;
      abort ;

  end ;

  inherited;

end;

procedure TfrmEntradaProducao.btSalvarClick(Sender: TObject);
begin

  if (not qryMaster.Active) then
  begin

      MensagemAlerta('Nenhum registro de entrada ativo.') ;
      abort ;

  end ;

  if ( qryMasterdDtEntrada.asString <> '' ) then
  begin

      MensagemAlerta('Este registro de entrada j� foi processado.') ;
      abort ;

  end ;

  inherited;

end;

procedure TfrmEntradaProducao.btRegistroLoteSerialClick(Sender: TObject);
var
    objForm : TfrmRegistroMovLote ;
begin

  if (qryMaster.State = dsInsert) and (qryMasternCdEntradaProducao.Value = 0) then
  begin
      qryMaster.Post ;
  end ;

  if (not qryMaster.Active) or (qryMaster.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum Registro de Entrada de Produ��o Ativo.') ;
      abort ;
  end ;

  if (qryMasternCdTabTipoEntradaProducao.Value > 1) then {-- retrabalho/refugo --}
  begin
      MensagemAlerta('Registro de retrabalho/refugo n�o precisa de registro de lote/serial.') ;
      abort ;
  end ;

  objForm := TfrmRegistroMovLote.Create( Self ) ;

  objForm.registraMovLote( qryMasternCdProduto.Value
                         , 0 
                         , 1 {-- Entrada produ��o --}
                         , qryMasternCdEntradaProducao.Value
                         , qryMasternCdLocalEstoque.Value
                         , qryMasternQtdeEntrada.Value
                         , 'ENTRADA PRODU��O No. ' + Trim( qryMasternCdEntradaProducao.asString ) + ' OP: ' + qryMastercNumeroOP.Value
                         , TRUE
                         , (qryMasterdDtEntrada.AsString <> '')
                         , FALSE) ;

  freeAndNil( objForm ) ;

end;

procedure TfrmEntradaProducao.qryTabTipoEntradaProducaoAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  desativaCampo ( edtCdTipoPerda   , (qryTabTipoEntradaProducaonCdTabTipoEntradaProducao.Value <> 3) ) ;
  desativaCampo ( edtCdMotivoPerda , (qryTabTipoEntradaProducaonCdTabTipoEntradaProducao.Value <> 3) ) ;
  desativaCampo ( DBEdit6          , (qryTabTipoEntradaProducaonCdTabTipoEntradaProducao.Value <> 1) ) ;

end;

procedure TfrmEntradaProducao.registraConclusaoExterna(cNumeroOP: string ;
  nCdTabTipoEntradaProducao: integer);
begin

    bChamadaExterna := True ;

    btIncluir.Click;

    qryMastercNumeroOP.Value := cNumeroOP ;
    buscaOP() ;

    Self.ShowModal ;

end;

procedure TfrmEntradaProducao.buscaOP;
begin

      qryOrdemProducao.Close;
      PosicionaQuery( qryOrdemProducao , DBEdit2.Text ) ;

      if not qryOrdemProducao.eof and (qryMaster.State = dsInsert) then
      begin

          qryMasternCdOrdemProducao.Value := qryOrdemProducaonCdOrdemProducao.Value ;
          qryMasternCdProduto.Value       := qryOrdemProducaonCdProduto.Value ;

          qryProduto.Close ;

          PosicionaQuery( qryProduto , qryOrdemProducaonCdProduto.asString ) ;

          qryMasternCdLocalEstoque.Value := qryOrdemProducaonCdLocalEstoque.Value ;
          DBEdit6.PosicionaQuery;

          qryMasternCdTabTipoEntradaProducao.Value := 1 ;
          DBEdit4.PosicionaQuery;

          qryMasternQtdeEntrada.Value := qryOrdemProducaonSaldoQtde.Value ;

      end ;

end;

procedure TfrmEntradaProducao.FormShow(Sender: TObject);
begin
  inherited;

  if (bChamadaExterna) then
      DBEdit5.SetFocus ;
      
end;

initialization
    RegisterClass( TfrmEntradaProducao ) ;

end.
