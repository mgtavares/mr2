inherited frmPais: TfrmPais
  Caption = 'Pais'
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 34
    Top = 46
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 19
    Top = 70
    Width = 53
    Height = 13
    Caption = 'Nome Pais'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 24
    Top = 94
    Width = 48
    Height = 13
    Caption = 'Sigla Pais'
    FocusControl = DBEdit3
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 80
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdPais'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 80
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmPais'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [7]
    Left = 80
    Top = 88
    Width = 39
    Height = 19
    DataField = 'cSigla'
    DataSource = dsMaster
    TabOrder = 3
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM PAIS'
      'WHERE nCdPais = :nPK')
    object qryMasternCdPais: TIntegerField
      FieldName = 'nCdPais'
    end
    object qryMastercNmPais: TStringField
      FieldName = 'cNmPais'
      Size = 50
    end
    object qryMastercSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 3
    end
  end
end
