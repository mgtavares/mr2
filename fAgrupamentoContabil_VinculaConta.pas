unit fAgrupamentoContabil_VinculaConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, ER2Lookup, DB, DBCtrls, ADODB;

type
  TfrmAgrupamentoContabil_VinculaConta = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edtMascaraSuperior: TEdit;
    Label2: TLabel;
    edtPlanoConta: TER2LookupMaskEdit;
    Label3: TLabel;
    edtCodigo: TMaskEdit;
    Label4: TLabel;
    edtMascara: TEdit;
    qrySubConta: TADOQuery;
    qrySubContanCdContaAgrupamentoContabil: TIntegerField;
    qrySubContanCdContaAgrupamentoContabilPai: TIntegerField;
    qrySubContanCdAgrupamentoContabil: TIntegerField;
    qrySubContacMascara: TStringField;
    qrySubContacNmContaAgrupamentoContabil: TStringField;
    qrySubContacNmContaSegundoIdioma: TStringField;
    qrySubContaiNivel: TIntegerField;
    qrySubContaiSequencia: TIntegerField;
    qrySubContanCdTabTipoNaturezaContaSPED: TIntegerField;
    qrySubContanCdPlanoConta: TIntegerField;
    qrySubContacFlgAnalSintetica: TStringField;
    qrySubContacFlgNaturezaCredDev: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryPlanoConta: TADOQuery;
    qryPlanoContanCdPlanoConta: TIntegerField;
    qryPlanoContacNmPlanoConta: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryProximaSequencia: TADOQuery;
    qryProximaSequenciaiProximaSequencia: TIntegerField;
    qryAgrupamentoContabil: TADOQuery;
    qryAgrupamentoContabilnCdAgrupamentoContabil: TIntegerField;
    qryAgrupamentoContabilnCdEmpresa: TIntegerField;
    qryAgrupamentoContabilcNmAgrupamentoContabil: TStringField;
    qryAgrupamentoContabilcMascara: TStringField;
    qryAgrupamentoContabilnCdTabTipoAgrupamentoContabil: TIntegerField;
    qryAgrupamentoContabilnCdStatus: TIntegerField;
    qryAgrupamentoContabilcFlgAnaliseVertical: TIntegerField;
    qryAgrupamentoContabiliMaxNiveis: TIntegerField;
    qryContaAgrupamentoContabil: TADOQuery;
    qryContaAgrupamentoContabilnCdContaAgrupamentoContabil: TIntegerField;
    qryContaAgrupamentoContabilnCdContaAgrupamentoContabilPai: TIntegerField;
    qryContaAgrupamentoContabilnCdAgrupamentoContabil: TIntegerField;
    qryContaAgrupamentoContabilcMascara: TStringField;
    qryContaAgrupamentoContabilcNmContaAgrupamentoContabil: TStringField;
    qryContaAgrupamentoContabilcNmContaSegundoIdioma: TStringField;
    qryContaAgrupamentoContabiliNivel: TIntegerField;
    qryContaAgrupamentoContabiliSequencia: TIntegerField;
    qryContaAgrupamentoContabilnCdTabTipoNaturezaContaSPED: TIntegerField;
    qryContaAgrupamentoContabilnCdPlanoConta: TIntegerField;
    qryContaAgrupamentoContabilcFlgAnalSintetica: TStringField;
    qryContaAgrupamentoContabilcFlgNaturezaCredDev: TStringField;
    qryPlanoContacNmPlanoContaSegundoIdioma: TStringField;
    qryAux: TADOQuery;
    qryPlanoContacFlgNaturezaCredDev: TStringField;
    procedure novoVinculo( nCdContaAgrupamentoContabil : integer ) ;
    procedure FormShow(Sender: TObject);
    procedure edtCodigoChange(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    function fnRemoveMascara( cString : string ) : string;
  private
    { Private declarations }
    bInsert : boolean;
  public
    { Public declarations }
  end;

var
  frmAgrupamentoContabil_VinculaConta: TfrmAgrupamentoContabil_VinculaConta;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmAgrupamentoContabil_VinculaConta }

procedure TfrmAgrupamentoContabil_VinculaConta.novoVinculo(
  nCdContaAgrupamentoContabil: integer);
var
  cMascaraFormat : string ;
  i : integer ;
  iNivelCorrente : integer ;
begin

    qryContaAgrupamentoContabil.Close;
    PosicionaQuery( qryContaAgrupamentoContabil , '0' ) ;

    bInsert := TRUE ;

    qrySubConta.Close;
    PosicionaQuery( qrySubConta , intToStr( nCdContaAgrupamentoContabil ) ) ;

    qryAgrupamentoContabil.Close;
    PosicionaQuery( qryAgrupamentoContabil , qrySubContanCdAgrupamentoContabil.AsString ) ;

    if not qrySubConta.IsEmpty then
        edtMascaraSuperior.Text := frmMenu.fnAplicaMascaraContabil( qrySubContacMascara.Value , qryAgrupamentoContabilcMascara.Value ) ;

    qryProximaSequencia.Close;
    PosicionaQuery( qryProximaSequencia , intToStr( nCdContaAgrupamentoContabil ) ) ;

    if not qryProximaSequencia.IsEmpty then
        edtCodigo.Text := qryProximaSequenciaiProximaSequencia.AsString ;

    {-- de acordo com o n�vel da subconta, gera a m�scara no campo c�digo --}
    cMascaraFormat := '' ;
    iNivelCorrente := 1 ;

    for i := 1 to length( qryAgrupamentoContabilcMascara.Value ) do
    begin

        if ( Copy( qryAgrupamentoContabilcMascara.Value , i , 1 ) = '.' ) then
        begin

            inc( iNivelCorrente ) ;

            if ( iNivelCorrente > ( qrySubContaiNivel.Value + 1 ) ) then
                break ;

        end
        else
        begin

            if ( iNivelCorrente = ( qrySubContaiNivel.Value + 1 ) ) then
                cMascaraFormat := cMascaraFormat + Copy( qryAgrupamentoContabilcMascara.Value , i , 1 ) ;

        end ;

    end ;

    edtCodigo.EditMask := trim( cMascaraFormat ) + ';0;_'  ;
    edtCodigo.Text     := frmMenu.ZeroEsquerda(edtCodigo.Text,(length(edtCodigo.EditMask)-4)) ;

    Self.ShowModal;

end;

procedure TfrmAgrupamentoContabil_VinculaConta.FormShow(Sender: TObject);
begin
  inherited;

  if (not edtPlanoConta.ReadOnly) then
      edtPlanoConta.SetFocus
  else edtCodigo.SetFocus;
  
end;

procedure TfrmAgrupamentoContabil_VinculaConta.edtCodigoChange(
  Sender: TObject);
begin
  inherited;

  if (edtMascaraSuperior.Text <> '') then
      edtMascara.Text := edtMascaraSuperior.Text + '.' + edtCodigo.Text
  else edtMascara.Text := edtCodigo.Text ;
  
end;

procedure TfrmAgrupamentoContabil_VinculaConta.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Selecione uma conta cont�bil.') ;
      edtPlanoConta.SetFocus;
      abort;
  end ;

  if (trim( edtCodigo.Text ) = '') then
  begin
      MensagemAlerta('Informe o c�digo da sequ�ncia.') ;
      edtCodigo.SetFocus;
      abort;
  end ;

  if (bInsert) then
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM ContaAgrupamentoContabil WHERE nCdAgrupamentoContabil = ' + qryAgrupamentoContabilnCdAgrupamentoContabil.AsString + ' AND cMascara = ' + Chr(39) +  fnRemoveMascara( Trim(edtMascara.Text) ) + Chr(39) ) ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin

          qryAux.Close;
          MensagemAlerta('J� existe uma conta com a m�scara ' + edtMascara.Text + ' neste agrupamento cont�bil. Verifique.') ;
          abort ;

      end ;

  end
  else
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM ContaAgrupamentoContabil WHERE nCdAgrupamentoContabil = ' + qryAgrupamentoContabilnCdAgrupamentoContabil.AsString + ' AND nCdContaAgrupamentoContabil != ' + qryContaAgrupamentoContabilnCdContaAgrupamentoContabil.asString + ' AND cMascara = ' + Chr(39) +  fnRemoveMascara( Trim(edtMascara.Text) ) + Chr(39) ) ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin

          qryAux.Close;
          MensagemAlerta('J� existe uma conta com a m�scara ' + edtMascara.Text + ' neste agrupamento cont�bil. Verifique.') ;
          abort ;

      end ;

  end ;

  if ( MessageDlg('Confirma a vincula��o desta conta ?',mtConfirmation,[mbYes,mbNo],0) = MRNO ) then
  begin
      edtCodigo.SetFocus;
      exit;
  end ;

  try
      if ( bInsert ) then
      begin

          qryContaAgrupamentoContabil.Insert ;
          qryContaAgrupamentoContabilnCdContaAgrupamentoContabil.Value    := frmMenu.fnProximoCodigo('CONTAAGRUPAMENTOCONTABIL') ;
          qryContaAgrupamentoContabilnCdAgrupamentoContabil.Value         := qryAgrupamentoContabilnCdAgrupamentoContabil.Value ;
          qryContaAgrupamentoContabilnCdContaAgrupamentoContabilPai.Value := qrySubContanCdContaAgrupamentoContabil.Value ;

      end
      else qryContaAgrupamentoContabil.Edit ;

      qryContaAgrupamentoContabilcMascara.Value                       := fnRemoveMascara( edtMascara.Text );
      qryContaAgrupamentoContabilcNmContaAgrupamentoContabil.Value    := qryPlanoContacNmPlanoConta.Value ;
      qryContaAgrupamentoContabilcNmContaSegundoIdioma.Value          := qryPlanoContacNmPlanoContaSegundoIdioma.Value ;
      qryContaAgrupamentoContabiliNivel.Value                         := ( qrySubContaiNivel.Value + 1 ) ;
      qryContaAgrupamentoContabiliSequencia.Value                     := strToInt( edtCodigo.Text ) ;
      qryContaAgrupamentoContabilnCdTabTipoNaturezaContaSPED.Value    := qrySubContanCdTabTipoNaturezaContaSPED.Value ;
      qryContaAgrupamentoContabilcFlgAnalSintetica.Value              := 'A' ;
      qryContaAgrupamentoContabilcFlgNaturezaCredDev.Value            := qryPlanoContacFlgNaturezaCredDev.Value ;
      qryContaAgrupamentoContabilnCdPlanoConta.Value                  := qryPlanoContanCdPlanoConta.Value ;

      qryContaAgrupamentoContabil.Post;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  Close;
  
end;

function TfrmAgrupamentoContabil_VinculaConta.fnRemoveMascara(
  cString: string): string;
var
  x: Integer;
begin

  result := '';

  for x := 1 to length( cString ) do
      if ( Copy( cString , x , 1 ) <> '.' ) then
          result := result + Copy( cString , x , 1 ) ;

end;

end.
