unit rProvisaoPagamentoBaixada;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBCtrls, StdCtrls, Mask, DB, ADODB;

type
  TrptProvisaoPagamentoBaixada = class(TfrmRelatorio_Padrao)
    Label7: TLabel;
    MaskEdit4: TMaskEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    MaskEdit3: TMaskEdit;
    MaskEdit5: TMaskEdit;
    MaskEdit6: TMaskEdit;
    MaskEdit7: TMaskEdit;
    Image2: TImage;
    qryFormaPagto: TADOQuery;
    qryContaBancaria: TADOQuery;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    dsEmpresa: TDataSource;
    qryFormaPagtocNmFormaPagto: TStringField;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancariacNmTitular: TStringField;
    qryContaBancarianCdConta: TStringField;
    DBEdit4: TDBEdit;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancariacAgencia: TIntegerField;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label8: TLabel;
    edtDataInicialPagto: TMaskEdit;
    Label9: TLabel;
    edtDataFinalPagto: TMaskEdit;
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptProvisaoPagamentoBaixada: TrptProvisaoPagamentoBaixada;

implementation

uses fLookup_Padrao, fMenu, rProvisaoPagamentoBaixada_View;

{$R *.dfm}
procedure TrptProvisaoPagamentoBaixada.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var
   nPk : integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit4.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptProvisaoPagamentoBaixada.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit4.Text) <> '') then
  begin
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit4.Text ;
    qryEmpresa.Open ;
  end ;
  if qryEmpresa.Eof then MaskEdit4.Text := '';

end;

procedure TrptProvisaoPagamentoBaixada.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var
   nPk : integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(37,'cFlgProvTit = 1');

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryContaBancaria.Close ;
            qryContaBancaria.Parameters.ParamByName('nPk').Value := nPK ;
            qryContaBancaria.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptProvisaoPagamentoBaixada.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryContaBancaria.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin
    qryContaBancaria.Parameters.ParamByName('nPk').Value          := MaskEdit3.Text ;
    qryContaBancaria.Open ;
  end ;
  if qryContaBancaria.Eof then MaskEdit3.Text := '';

end;

procedure TrptProvisaoPagamentoBaixada.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;
   case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, MaskEdit6.Text) ;
        end ;

    end ;

  end ;
end;

procedure TrptProvisaoPagamentoBaixada.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, MaskEdit6.Text) ;

end;

procedure TrptProvisaoPagamentoBaixada.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryFormaPagto.Close;
  PosicionaQuery(qryFormaPagto, MaskEdit5.Text) ;
end;

procedure TrptProvisaoPagamentoBaixada.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var
  nPK : Integer ;
begin
  inherited;
   case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(15,'cFlgPermFinanceiro = 1');

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryFormaPagto, MaskEdit5.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptProvisaoPagamentoBaixada.ToolButton1Click(Sender: TObject);
var
  objRel : TrptProvisaoPagamentoBaixada_View ;
begin
  inherited;

  objRel := TrptProvisaoPagamentoBaixada_View.Create(Self) ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  objRel.uspRelatorio.Close ;
  objRel.uspRelatorio.Parameters.ParamByName('@nCdEmpresa').Value         :=  frmMenu.ConvInteiro(MaskEdit4.Text);
  objRel.uspRelatorio.Parameters.ParamByName('@nCdContaBancaria').Value   :=  frmMenu.ConvInteiro(MaskEdit3.Text);
  objRel.uspRelatorio.Parameters.ParamByName('@nCdFormaPagto').Value      :=  frmMenu.ConvInteiro(MaskEdit5.Text);
  objRel.uspRelatorio.Parameters.ParamByName('@iNrCheque').Value          :=  frmMenu.ConvInteiro(MaskEdit7.Text);
  objRel.uspRelatorio.Parameters.ParamByName('@nCdTerceiro').Value        :=  frmMenu.ConvInteiro(MaskEdit6.Text);
  objRel.uspRelatorio.Parameters.ParamByName('@dDtInicialPagto').Value    :=  frmMenu.ConvData(edtDataInicialPagto.Text);
  objRel.uspRelatorio.Parameters.ParamByName('@dDtFinalPagto').Value      :=  frmMenu.ConvData(edtDataFinalPagto.Text);
  objRel.uspRelatorio.Parameters.ParamByName('@dDtInicialBaixa').Value    :=  frmMenu.ConvData(MaskEdit1.Text);
  objRel.uspRelatorio.Parameters.ParamByName('@dDtFinalBaixa').Value      :=  frmMenu.ConvData(MaskEdit2.Text);
  objRel.uspRelatorio.Parameters.ParamByName('@nCdUsuario').Value         :=  frmMenu.nCdUsuarioLogado;
  objRel.uspRelatorio.Open ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  objRel.lblFiltro1.Caption := '';

  if (Trim(MaskEdit4.Text)) <> '' then
     objRel.lblFiltro1.Caption := 'Empresa: ' + ' ' + Trim(MaskEdit4.Text) + ' - ' +  DbEdit14.Text;

  if (((Trim(MaskEdit3.Text)) <> '') and ((Trim(DBEdit1.Text) <> ''))) then
     objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + '  Conta Banc�ria: ' + ' ' + MaskEdit3.Text + ' - ' + Trim(DbEdit5.Text) + ' - ' + Trim(DbEdit6.Text) + ' - ' + Trim(DbEdit4.Text) + ' - ' +  DbEdit1.Text   ;

  if (((Trim(MaskEdit3.Text)) <> '') and ((Trim(DBEdit1.Text) = ''))) then
     objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + '  Conta Banc�ria: ' + ' ' + MaskEdit3.Text + ' - ' + DbEdit4.Text ;

  if (Trim(MaskEdit5.Text)) <> '' then
     objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + '  Forma Pagamento: ' + ' ' + MaskEdit5.Text + ' - ' +  DbEdit2.Text;

  if (((Trim(edtDataInicialPagto.Text)) <> '/  /') or ((Trim(edtDataFinalPagto.Text)) <> '/  /')) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + '  Per�odo Pagto : ' + trim(edtDataInicialPagto.Text) + '-' + trim(edtDataFinalPagto.Text) ;

  if (((Trim(MaskEdit1.Text)) <> '/  /') or ((Trim(MaskEdit2.Text)) <> '/  /')) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + '  Per�odo Baixa : ' + trim(MaskEdit1.Text) + '-' + trim(MaskEdit2.Text) ;

  if (Trim(MaskEdit6.Text)) <> '' then
     objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + '  Terceiro: ' + ' ' + MaskEdit6.Text + ' - ' +  DbEdit3.Text;

  if (Trim(MaskEdit7.Text)) <> '' then
     objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + '  Nro Cheque: ' + ' ' + MaskEdit7.Text;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TrptProvisaoPagamentoBaixada.FormShow(Sender: TObject);
begin
  inherited;
  MaskEdit4.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

end;

initialization
   RegisterClass(TrptProvisaoPagamentoBaixada) ;

end.
