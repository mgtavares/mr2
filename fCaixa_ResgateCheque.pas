unit fCaixa_ResgateCheque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, cxLookAndFeelPainters, cxButtons, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, DB, cxDBData, ADODB,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, jpeg;

type
  TfrmCaixa_ResgateCheque = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Edit1: TEdit;
    Label1: TLabel;
    Label5: TLabel;
    edtBanco: TMaskEdit;
    edtDtDeposito: TMaskEdit;
    Label3: TLabel;
    edtNrCheque: TMaskEdit;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    cxButton1: TcxButton;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryConsultaCheques: TADOQuery;
    qryPreparaTemp: TADOQuery;
    qryChequeNaoSelec: TADOQuery;
    qryChequeNaoSelecnCdCheque: TIntegerField;
    qryChequeNaoSelecdDtDeposito: TDateTimeField;
    qryChequeNaoSeleciNrCheque: TIntegerField;
    qryChequeNaoSelecnValCheque: TBCDField;
    qryChequeNaoSeleccNmTabStatusCheque: TStringField;
    qryChequeNaoSelecnCdLoja: TIntegerField;
    qryChequeNaoSelecnCdBanco: TIntegerField;
    qryChequeNaoSeleccConta: TStringField;
    qryChequeSelec: TADOQuery;
    qryChequeSelecnCdCheque: TIntegerField;
    qryChequeSelecdDtDeposito: TDateTimeField;
    qryChequeSeleciNrCheque: TIntegerField;
    qryChequeSelecnValCheque: TBCDField;
    qryChequeSeleccNmTabStatusCheque: TStringField;
    qryChequeSelecnCdLoja: TIntegerField;
    qryChequeSelecnCdBanco: TIntegerField;
    qryChequeSeleccConta: TStringField;
    dsChequeNaoSelec: TDataSource;
    cxGrid1DBTableView1nCdCheque: TcxGridDBColumn;
    cxGrid1DBTableView1dDtDeposito: TcxGridDBColumn;
    cxGrid1DBTableView1iNrCheque: TcxGridDBColumn;
    cxGrid1DBTableView1nValCheque: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTabStatusCheque: TcxGridDBColumn;
    cxGrid1DBTableView1nCdLoja: TcxGridDBColumn;
    cxGrid1DBTableView1nCdBanco: TcxGridDBColumn;
    cxGrid1DBTableView1cConta: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    dsChequeSelec: TDataSource;
    cxGridDBTableView1nCdCheque: TcxGridDBColumn;
    cxGridDBTableView1dDtDeposito: TcxGridDBColumn;
    cxGridDBTableView1iNrCheque: TcxGridDBColumn;
    cxGridDBTableView1nValCheque: TcxGridDBColumn;
    cxGridDBTableView1cNmTabStatusCheque: TcxGridDBColumn;
    cxGridDBTableView1nCdLoja: TcxGridDBColumn;
    cxGridDBTableView1nCdBanco: TcxGridDBColumn;
    cxGridDBTableView1cConta: TcxGridDBColumn;
    qryAux: TADOQuery;
    Image2: TImage;
    procedure cxButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGridDBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure cxGridDBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtBancoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtNrChequeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtDtDepositoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdTerceiro : integer ;
  end;

var
  frmCaixa_ResgateCheque: TfrmCaixa_ResgateCheque;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmCaixa_ResgateCheque.cxButton1Click(Sender: TObject);
begin
  inherited;

  qryPreparaTemp.Close  ;
  qryPreparaTemp.ExecSQL;

  qryConsultaCheques.Close ;
  qryConsultaCheques.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva               ;
  qryConsultaCheques.Parameters.ParamByName('nCdTerceiro').Value := nCdTerceiro                           ;
  qryConsultaCheques.Parameters.ParamByName('dDtDeposito').Value := frmMenu.ConvData(edtDtDeposito.Text)  ;
  qryConsultaCheques.Parameters.ParamByName('nCdBanco').Value    := frmMenu.ConvInteiro(edtBanco.Text)    ;
  qryConsultaCheques.Parameters.ParamByName('iNrCheque').Value   := frmMenu.ConvInteiro(edtNrCheque.Text) ;
  qryConsultaCheques.ExecSQL ;

  qryChequeNaoSelec.Close ;
  qryChequeNaoSelec.Open  ;

  if qryChequeNaoSelec.Eof then
  begin
      MensagemAlerta('Nenhum cheque encontrado para o crit�rio utilizado.') ;
      exit ;
  end ;

  qryChequeSelec.Close ;
  qryChequeSelec.Open  ;

  cxGrid1.SetFocus ;

end;

procedure TfrmCaixa_ResgateCheque.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  if not qryChequeNaoSelec.Eof then
  begin
      if (qryChequeNaoSelecnCdLoja.Value <> frmMenu.nCdLojaAtiva) then
      begin
          MensagemAlerta('Este cheque n�o est� na conta desta loja.') ;
          abort ;
      end ;

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE #Temp_Cheque_Consulta Set cFlgOK = 1 WHERE nCdCheque = ' + qryChequeNaoSelecnCdCheque.AsString) ;
      qryAux.ExecSQL ;

      qryChequeNaoSelec.Close ;
      qryChequeNaoSelec.Open  ;
      qryChequeSelec.Close    ;
      qryChequeSelec.Open     ;

  end ;

end;

procedure TfrmCaixa_ResgateCheque.cxGridDBTableView1DblClick(
  Sender: TObject);
begin
  inherited;
  if not qryChequeSelec.Eof then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE #Temp_Cheque_Consulta Set cFlgOK = 0 WHERE nCdCheque = ' + qryChequeSelecnCdCheque.AsString) ;
      qryAux.ExecSQL ;

      qryChequeNaoSelec.Close ;
      qryChequeNaoSelec.Open  ;
      qryChequeSelec.Close    ;
      qryChequeSelec.Open     ;

  end ;

end;

procedure TfrmCaixa_ResgateCheque.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (qryChequeSelec.Eof) or (qryChequeSelec.RecordCount <= 0) then
  begin
      MensagemAlerta('Nenhum cheque selecionado para resgate.') ;
      exit ;
  end ;

  close ;

end;

procedure TfrmCaixa_ResgateCheque.cxGrid1DBTableView1KeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if (Key = #13) then
      cxGrid1DBTableView1DBlClick(nil) ;
      
end;

procedure TfrmCaixa_ResgateCheque.cxGrid1DBTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_F4) then
      ToolButton1.Click;

end;

procedure TfrmCaixa_ResgateCheque.cxGridDBTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_F4) then
      ToolButton1.Click;

end;

procedure TfrmCaixa_ResgateCheque.FormShow(Sender: TObject);
begin
  inherited;

  qryChequeNaoSelec.Close ;
  qryChequeSelec.Close ;

  edtBanco.SetFocus;
  
end;

procedure TfrmCaixa_ResgateCheque.cxGridDBTableView1KeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;

  if (Key = #13) then
      cxGridDBTableView1DBlClick(nil) ;

end;

procedure TfrmCaixa_ResgateCheque.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmCaixa_ResgateCheque.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmCaixa_ResgateCheque.edtBancoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  
  if (Key = vk_return) then
      edtNrCheque.SetFocus;

end;

procedure TfrmCaixa_ResgateCheque.edtNrChequeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  
  if (Key = vk_return) then
      edtDtDeposito.SetFocus;

end;

procedure TfrmCaixa_ResgateCheque.edtDtDepositoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = vk_return) then
      cxButton1.Click;

end;

end.
