unit fMetaVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, DBCtrls, Mask, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls;

type
  TfrmMetaVenda = class(TfrmCadastro_Padrao)
    qryMasternCdMetaVenda: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasteriAno: TIntegerField;
    qryMasteriMes: TIntegerField;
    qryMastercFlgSabado: TIntegerField;
    qryMastercFlgDomingo: TIntegerField;
    qryMasternValMetaMes: TBCDField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMetaVenda: TfrmMetaVenda;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmMetaVenda.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'METAVENDA' ;
  nCdTabelaSistema  := 64 ;
  nCdConsultaPadrao := 144 ;

end;

procedure TfrmMetaVenda.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit4.SetFocus ;
end;

procedure TfrmMetaVenda.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasteriAno.Value < 2000) then
  begin
      MensagemAlerta('Ano inv�lido.') ;
      DbEdit4.SetFocus;
      abort ;
  end ;

  if (qryMasteriMes.Value < 1) or (qryMasteriMes.Value > 12) then
  begin
      MensagemAlerta('M�s inv�lido.') ;
      DbEdit5.SetFocus;
      abort ;
  end ;

  if (qryMasternValMetaMes.Value < 0) then
  begin
      MensagemAlerta('Valor da meta inv�lido.') ;
      DbEdit6.Setfocus ;
      abort ;
  end ;

  inherited;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmMetaVenda.FormActivate(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  
end;

initialization
    RegisterClass(TfrmMetaVenda) ;
    
end.
