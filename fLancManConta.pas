unit fLancManConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBCtrls, ADODB, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit, StdCtrls, Mask;

type
  TfrmLancManConta = class(TfrmProcesso_Padrao)
    MaskEdit3: TMaskEdit;
    MaskEdit6: TMaskEdit;
    Label5: TLabel;
    Label1: TLabel;
    edtNrDocumento: TEdit;
    edtHistorico: TEdit;
    Label21: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryTipoLancto: TADOQuery;
    qryTipoLanctonCdTipoLancto: TIntegerField;
    qryTipoLanctocNmTipoLancto: TStringField;
    qryTipoLanctocSinalOper: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    SP_LANCTOMANUAL_CONTA: TADOStoredProc;
    edtValorLanc: TcxCurrencyEdit;
    qryContaBancariacFlgCofre: TIntegerField;
    qryAux: TADOQuery;
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtValorLancKeyPress(Sender: TObject; var Key: Char);
    procedure EmitirComprovante(nCdLanctoFin : integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLancManConta: TfrmLancManConta;

implementation

uses fLookup_Padrao, fMenu, fCaixa_LanctoMan;

{$R *.dfm}

procedure TfrmLancManConta.MaskEdit3Exit(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close ;
  PosicionaQuery(qryContaBancaria, MaskEdit3.Text) ;

end;

procedure TfrmLancManConta.MaskEdit6Exit(Sender: TObject);
begin
  inherited;

  qryTipoLancto.Close ;
  PosicionaQuery(qryTipoLancto, MaskEdit6.Text) ;

end;

procedure TfrmLancManConta.MaskEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        {nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = @@Empresa AND (cFlgCaixa = 0 OR cFlgCofre = 1)');}

        nPK := frmLookup_Padrao.ExecutaConsulta2(37,'EXISTS(SELECT 1 FROM UsuarioContaBancaria UCB WHERE UCB.nCdContaBancaria = ContaBancaria.nCdContaBancaria AND UCB.nCdUsuario = '+ intToStr(frmMenu.nCdUsuarioLogado)+') AND ContaBancaria.nCdEmpresa = @@Empresa AND ((@@Loja = 0) OR (@@Loja = ContaBancaria.nCdLoja)) AND (cFlgCofre = 1 OR cFlgCaixa = 0)') ;

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmLancManConta.MaskEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(130,'cFlgPermManual = 1');

        If (nPK > 0) then
        begin
            MaskEdit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end ;

procedure TfrmLancManConta.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione uma conta banc�ria/cofre.') ;
      MaskEdit3.SetFocus ;
      exit ;
  end ;

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Selecione um tipo de lan�amento') ;
      MaskEdit6.SetFocus ;
      exit ;
  end ;

  if (trim(edtHistorico.Text) = '') then
  begin
      MensagemAlerta('Informe o hist�rico do lan�amento') ;
      edtHistorico.SetFocus ;
      exit ;
  end ;

  if (edtValorLanc.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor do lan�amento') ;
      edtHistorico.SetFocus ;
      exit ;
  end ;

  if (qryTipoLanctocSinalOper.Value = '+') then
  begin
      case MessageDlg('Confirma o lan�amento do cr�dito no saldo da conta ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;
  end ;

  if (qryTipoLanctocSinalOper.Value = '-') then
  begin
      case MessageDlg('Confirma o lan�amento do d�dito no saldo da conta ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      SP_LANCTOMANUAL_CONTA.Close ;
      SP_LANCTOMANUAL_CONTA.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva;
      SP_LANCTOMANUAL_CONTA.Parameters.ParamByName('@nCdContaBancaria').Value := qryContaBancarianCdContaBancaria.Value ;
      SP_LANCTOMANUAL_CONTA.Parameters.ParamByName('@nCdTipoLancto').Value    := qryTipoLanctonCdTipoLancto.Value ;
      SP_LANCTOMANUAL_CONTA.Parameters.ParamByName('@cDocumento').Value       := edtNrDocumento.Text ;
      SP_LANCTOMANUAL_CONTA.Parameters.ParamByName('@cHistorico').Value       := edtHistorico.Text ;
      SP_LANCTOMANUAL_CONTA.Parameters.ParamByName('@nValLancto').Value       := edtValorLanc.Value ;
      SP_LANCTOMANUAL_CONTA.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
      SP_LANCTOMANUAL_CONTA.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryAux.Close ;

  if (qryContaBancariacFlgCofre.Value = 1) then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT MAX(nCdLanctoFin) FROM LanctoFin WHERE nCdusuario = ' + intToStr(frmMenu.nCdUsuarioLogado) + ' AND nCdContaBancaria = ' + qryContaBancarianCdContaBancaria.AsString + ' AND nCdTipoLancto = ' + qryTipoLanctonCdTipoLancto.AsString) ;
      qryAux.Open ;
  end ;

  ShowMessage('Lan�amento processado com sucesso! O saldo da conta foi atualizado.') ;

  if not qryAux.Eof then
  begin
      if (MessageDlg('Deseja emitir comprovante ?',mtConfirmation,[mbYes,mbNo],0) = MRYes) then
          EmitirComprovante(qryAux.FieldList[0].Value) ;
  end ;

  qryContaBancaria.Close ;
  qryTipoLancto.Close    ;

  MaskEdit3.Text      := '' ;
  MaskEdit6.Text      := '' ;
  edtNrDocumento.Text := '' ;
  edtHistorico.Text   := '' ;
  edtValorLanc.Value  := 0 ;

  MaskEdit3.SetFocus ;

end;

procedure TfrmLancManConta.FormShow(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  
end;

procedure TfrmLancManConta.edtValorLancKeyPress(Sender: TObject;
  var Key: Char);
begin
  inherited;

  if (key = #13) then
      MaskEdit3.SetFocus;

end;

procedure TfrmLancManConta.EmitirComprovante(nCdLanctoFin: integer);
var
    objForm : TfrmCaixa_LanctoMan ;
    bOK     : boolean ;
begin

    bOK     := false ;
    objForm := TfrmCaixa_LanctoMan.Create( Self ) ;

    try
        try
            bOK := objForm.ImprimirComprovante(nCdLanctoFin) ;
        except
            MensagemErro('Erro na impress�o do relat�rio.') ;
            raise ;
        end ;
    finally
    
        freeAndNil( objForm ) ;

        if not bOK then
        begin

            if (MessageDlg('O Comprovante n�o foi impresso. Deseja tentar novamente ?',mtConfirmation,[mbYes,mbNo],0) = mrYes) then
                EmitirComprovante( nCdLanctoFin ) ;

        end ;

    end ;

end;

initialization
    RegisterClass(TfrmLancManConta) ;
    
end.
