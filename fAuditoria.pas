unit fAuditoria;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GridsEh, DBGridEh, ExtCtrls, DB, ADODB, ComCtrls, ToolWin,
  ImgList, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmAuditoria = class(TForm)
    ToolBar2: TToolBar;
    ToolButton5: TToolButton;
    qryAuditoria: TADOQuery;
    dsAuditoria: TDataSource;
    Image1: TImage;
    DBGridEh1: TDBGridEh;
    qryAuditoriadDtLog: TDateTimeField;
    qryAuditorianCdUsuario: TIntegerField;
    qryAuditoriacNmLogin: TStringField;
    qryAuditorianCdTipoAcao: TIntegerField;
    qryAuditoriacNmAcao: TStringField;
    qryAuditoriacObserv: TStringField;
    ImageList1: TImageList;
    procedure ToolButton5Click(Sender: TObject);
    procedure ExibeAuditoria(nCdTabelaSistema:Integer; nCdID:Integer) ;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAuditoria: TfrmAuditoria;

implementation

{$R *.dfm}

procedure TfrmAuditoria.ExibeAuditoria(nCdTabelaSistema:Integer; nCdID:Integer) ;
begin
    If (nCdID <= 0) then
        exit ;

    If (nCdTabelaSistema = 0) then
    begin
        ShowMessage('nCdTabelaSistema n�o encontrado no form de auditoria.') ;
        exit ;
    end ;

    qryAuditoria.Close ;
    qryAuditoria.Parameters.ParamByName('nCdTabelaSistema').Value := nCdTabelaSistema ;
    qryAuditoria.Parameters.ParamByName('nCdID').Value            := nCdID ;
    qryAuditoria.Open ;

    frmAuditoria.ShowModal ;

end ;

procedure TfrmAuditoria.ToolButton5Click(Sender: TObject);
begin
    qryAuditoria.Close ;
    Close ;
end;

end.
