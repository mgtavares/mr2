unit fInventarioLoja_Abertura;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  DBCtrls, ADODB, StdCtrls, Mask, cxPC, cxControls, cxLookAndFeelPainters,
  cxButtons, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmInventarioLoja_Abertura = class(TfrmProcesso_Padrao)
    edtLocalEstoque: TMaskEdit;
    Label5: TLabel;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DBEdit1: TDBEdit;
    dsLocalEstoque: TDataSource;
    edtLoja: TMaskEdit;
    Label1: TLabel;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    dsProduto: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    Image2: TImage;
    cmdPreparaTemp: TADOCommand;
    qrySelProduto: TADOQuery;
    qrySelProdutocFlgOK: TIntegerField;
    qrySelProdutonCdProduto: TIntegerField;
    qrySelProdutocReferencia: TStringField;
    qrySelProdutocNmProduto: TStringField;
    qrySelProdutocUnidadeMedida: TStringField;
    Panel1: TPanel;
    DBGridEh1: TDBGridEh;
    dsSelProduto: TDataSource;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    btLimparLista: TcxButton;
    cxButton4: TcxButton;
    qryProdutoFinal: TADOQuery;
    qryProdutoFinalnCdProduto: TIntegerField;
    qryProdutoFinalcReferencia: TStringField;
    qryProdutoFinalcLocalizacao: TStringField;
    qryProdutoFinalcNmProduto: TStringField;
    qryProdutoFinalcUnidadeMedida: TStringField;
    dsProdutoFinal: TDataSource;
    Panel2: TPanel;
    DBGridEh2: TDBGridEh;
    btLimpar2: TcxButton;
    SP_GERA_INVENTARIO_LOJA: TADOStoredProc;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit2: TDBEdit;
    dsLoja: TDataSource;
    Label2: TLabel;
    edtNmInventario: TEdit;
    btAddProduto: TcxButton;
    qryLojanCdLocalEstoquePadrao: TIntegerField;
    qryTransfereProdFinal: TADOQuery;
    cxButton5: TcxButton;
    cxTabSheet3: TcxTabSheet;
    Panel3: TPanel;
    btLimparListaLote: TcxButton;
    DBGridEh3: TDBGridEh;
    qryLoteContagem: TADOQuery;
    qryLoteContagemcNrLote: TStringField;
    qryLoteContagemiQtdeItens: TIntegerField;
    dsLoteContagem: TDataSource;
    qryLimpaSelProduto: TADOQuery;
    qryLimpaProdutoFinal: TADOQuery;
    cxButton3: TcxButton;
    qryAux: TADOQuery;
    procedure FormShow(Sender: TObject);
    procedure edtLocalEstoqueKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaExit(Sender: TObject);
    procedure edtLocalEstoqueExit(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure btLimparListaClick(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure btLimpar2Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure btAddProdutoClick(Sender: TObject);
    procedure btLimparListaLoteClick(Sender: TObject);
    procedure qryLoteContagemBeforePost(DataSet: TDataSet);
    procedure cxButton3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmInventarioLoja_Abertura: TfrmInventarioLoja_Abertura;

implementation

uses fMenu, fLookup_Padrao, rInventario, fInventarioLoja_IncluiProduto;

{$R *.dfm}

procedure TfrmInventarioLoja_Abertura.FormShow(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  edtLoja.Text := intToStr(frmMenu.nCdLojaAtiva) ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;

  if (qryLojanCdLocalEstoquePadrao.Value > 0) then
  begin
      edtLocalEstoque.Text := qryLojanCdLocalEstoquePadrao.AsString ;
      qryLocalEstoque.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value ;
      PosicionaQuery(qryLocalEstoque, edtLocalEstoque.Text) ;
  end ;

  cmdPreparaTemp.Execute ;

  qryLoteContagem.Close;
  qryLoteContagem.Open;

  cxPageControl1.ActivePageIndex := 0 ;
  
  edtLoja.SetFocus;
  
end;

procedure TfrmInventarioLoja_Abertura.edtLocalEstoqueKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if not edtLocalEstoque.ReadOnly then
        begin

            if (qryLoja.Eof) then
            begin
                MensagemAlerta('Selecione a loja.') ;
                edtLoja.SetFocus;
                abort ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva) + ' AND LocalEstoque.nCdLoja = ' + qryLojanCdLoja.AsString);

            If (nPK > 0) then
            begin
                edtLocalEstoque.Text := IntToStr(nPK) ;
                PosicionaQuery(qryLocalEstoque, edtLocalEstoque.Text) ;
            end ;
        end ;

    end ;

  end ;

end;

procedure TfrmInventarioLoja_Abertura.edtLojaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if not edtLoja.ReadOnly then
        begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(147,'EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdUsuario = @@Usuario AND UL.nCdLoja = Loja.nCdLoja)');

            If (nPK > 0) then
            begin
                edtLoja.Text := IntToStr(nPK) ;
                PosicionaQuery(qryLoja, edtLoja.Text) ;
            end ;
        end ;

    end ;

  end ;

end;

procedure TfrmInventarioLoja_Abertura.edtLojaExit(
  Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, edtLoja.Text) ;

end;

procedure TfrmInventarioLoja_Abertura.edtLocalEstoqueExit(Sender: TObject);
begin
  inherited;

  if (qryLoja.eof) then
  begin
      MensagemAlerta('Selecione a loja.') ;
      edtLoja.SetFocus;
      abort ;
  end ;

  qryLocalEstoque.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value ;
  qryLocalEstoque.Close ;
  PosicionaQuery(qryLocalEstoque, edtLocalEstoque.Text) ;
  
end;

procedure TfrmInventarioLoja_Abertura.FormKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
{  inherited;}

end;

procedure TfrmInventarioLoja_Abertura.cxButton1Click(Sender: TObject);
begin
  inherited;

  if not qrySelProduto.eof then
  begin

      {qrySelProduto.First ;

      while not qrySelProduto.eof do
      begin
          qrySelProduto.Edit ;
          qrySelProdutocFlgOK.Value := 0 ;
          qrySelProduto.Post ;

          qrySelProduto.Next ;
      end ;

      qrySelProduto.First ;}

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE #Temp_Produto_Sel SET cFlgOK = 0');
      qryAux.ExecSQL;

      qrySelProduto.Close ;
      qrySelProduto.Open ;

  end ;

end;

procedure TfrmInventarioLoja_Abertura.cxButton2Click(Sender: TObject);
begin
  inherited;

  if not qrySelProduto.eof then
  begin

      {qrySelProduto.First ;

      while not qrySelProduto.eof do
      begin
          qrySelProduto.Edit ;
          qrySelProdutocFlgOK.Value := 1 ;
          qrySelProduto.Post ;

          qrySelProduto.Next ;
      end ;

      qrySelProduto.First ;}

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE #Temp_Produto_Sel SET cFlgOK = 1');
      qryAux.ExecSQL;

      qrySelProduto.Close;
      qrySelProduto.Open;

  end ;

end;

procedure TfrmInventarioLoja_Abertura.btLimparListaClick(Sender: TObject);
begin
  inherited;

  if not qrySelProduto.eof then
  begin

      qryLimpaSelProduto.close;
      qryLimpaSelProduto.ExecSQL;

      qrySelProduto.First ;

      qrySelProduto.Close ;
      qrySelProduto.Open ;

  end ;

end;

procedure TfrmInventarioLoja_Abertura.cxButton4Click(Sender: TObject);
var
    iItens : integer;
begin
  inherited;

  if not qrySelProduto.Active then
  begin
      MensagemAlerta('Nenhum item selecionado.') ;
      exit ;
  end ;

  if (qrySelProduto.State <> dsBrowse) then
      qrySelProduto.Post ;

  qrySelProduto.First ;

  if (MessageDlg('Confirma a grava��o dos itens selecionados na lista do invent�rio ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit;

  try
      qryTransfereProdFinal.Close;
      qryTransfereProdFinal.ExecSQL;
  except
      MensagemErro('Erro no processamento') ;
      raise ;
  end ;

  qryProdutoFinal.Close ;
  qryProdutoFinal.Open ;

  if (qryProdutoFinal.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum item selecionado.') ;
      exit ;
  end ;

  btLimparLista.Click ;

  if not edtLoja.ReadOnly then
  begin
      edtLoja.ReadOnly := True ;
      edtLoja.Color    := $00E9E4E4 ;
  end ;

  if not edtLocalEstoque.ReadOnly then
  begin
      edtLocalEstoque.ReadOnly := True ;
      edtLocalEstoque.Color    := $00E9E4E4 ;
  end ;

  if (MessageDlg('Deseja adicionar mais produtos no invent�rio ?',mtConfirmation,[mbYes,mbNo],0) = MrYes) then
      btAddProduto.Click
  else begin
      cxPageControl1.ActivePageIndex := 2 ;
      DBGridEh3.SetFocus;
  end ;

end;

procedure TfrmInventarioLoja_Abertura.btLimpar2Click(Sender: TObject);
begin
  inherited;

  if qryProdutoFinal.Active then
  begin

      qryLimpaProdutoFinal.close;
      qryLimpaProdutoFinal.ExecSQL;

      qryProdutoFinal.Close ;
      qryProdutoFinal.Open ;

  end ;

end;

procedure TfrmInventarioLoja_Abertura.cxButton5Click(Sender: TObject);
var
    nCdInventario : integer ;
begin
  inherited;

  if (not qryProdutoFinal.Active) or (qryProdutoFinal.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum produto selecionado para gera��o do invent�rio.') ;
      exit ;
  end ;


  if (not qryLoteContagem.Active) or (qryLoteContagem.RecordCount = 0) then
  begin

      MensagemAlerta('Nenhum lote de contagem informado.') ;
      exit ;

  end ;

  case MessageDlg('Confirma a abertura deste invent�rio ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      SP_GERA_INVENTARIO_LOJA.Close ;
      SP_GERA_INVENTARIO_LOJA.Parameters.ParamByName('@nCdEmpresa').Value      := frmMenu.nCdEmpresaAtiva ;
      SP_GERA_INVENTARIO_LOJA.Parameters.ParamByName('@nCdLoja').Value         := qryLojanCdLoja.Value ;
      SP_GERA_INVENTARIO_LOJA.Parameters.ParamByName('@nCdLocalEstoque').Value := qryLocalEstoquenCdLocalEstoque.Value ;
      SP_GERA_INVENTARIO_LOJA.Parameters.ParamByName('@nCdUsuario').Value      := frmMenu.nCdUsuarioLogado ;
      SP_GERA_INVENTARIO_LOJA.Parameters.ParamByName('@cNmInventario').Value   := edtNmInventario.Text;
      SP_GERA_INVENTARIO_LOJA.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  nCdInventario := SP_GERA_INVENTARIO_LOJA.Parameters.ParamByName('@nCdInventario').Value ;

  ShowMessage('Invent�rio aberto com sucesso.' + #13#13 + 'N�mero do invent�rio : ' + IntToStr(nCdInventario)) ;

  btLimpar2.Click ;
  btLimparLista.Click ;
  btLimparListaLote.Click;

  cxPageControl1.ActivePageIndex := 0 ;

  edtLoja.ReadOnly         := False ;
  edtLoja.Color            := clWhite ;
  edtLocalEstoque.ReadOnly := False ;
  edtLocalEstoque.Color    := clWhite ;


  edtNmInventario.Text := '' ;
  edtLoja.SetFocus ;


end;

procedure TfrmInventarioLoja_Abertura.ToolButton1Click(Sender: TObject);
begin
  inherited;

  btLimparLista.Click;
  btLimpar2.Click;

  edtLoja.Color    := clWhite ;
  edtLoja.ReadOnly := False ;

  edtLocalEstoque.Color    := clWhite ;
  edtLocalEstoque.ReadOnly := False ;

  qryLoja.Close;
  qryLocalEstoque.Close;

  edtLoja.Text         := '' ;
  edtLocalEstoque.Text := '' ;
  edtNmInventario.Text := '' ;

  edtLoja.SetFocus;
  
end;

procedure TfrmInventarioLoja_Abertura.btAddProdutoClick(Sender: TObject);
var
  objForm : TfrmInventarioLoja_IncluiProduto;
begin

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Selecione a loja que ser� inventariada.') ;
      edtLoja.SetFocus;
      abort ;
  end ;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione o local de estoque que ser� inventariado.') ;
      edtLoja.SetFocus;
      abort ;
  end ;

  if (trim(edtNmInventario.Text) = '') then
  begin
      MensagemAlerta('Informe o nome do invent�rio.') ;
      edtNmInventario.SetFocus;
      abort ;
  end ;

  {-- chama a tela de filtro de produtos e j� traz com a temp #Temp_Produto_Sel populada --}
  objForm := TfrmInventarioLoja_IncluiProduto.Create(nil);

  showForm(objForm,true);

  qrySelProduto.Close ;
  qrySelProduto.Open ;

  DBGridEh1.SetFocus;

end;

procedure TfrmInventarioLoja_Abertura.btLimparListaLoteClick(Sender: TObject);
begin
  inherited;

  if qryLoteContagem.Active then
  begin

      qryLoteContagem.First ;

      while not qryLoteContagem.eof do
      begin
          qryLoteContagem.Delete;
          qryLoteContagem.First;
      end ;

      qryLoteContagem.Close ;
      qryLoteContagem.Open ;

  end ;


end;

procedure TfrmInventarioLoja_Abertura.qryLoteContagemBeforePost(
  DataSet: TDataSet);
begin

  qryLoteContagemcNrLote.Value := uppercase(qryLoteContagemcNrLote.Value) ;

  if (qryLoteContagemiQtdeItens.Value <= 0) then
  begin
      MensagemAlerta('Quantidade de itens do lote inv�lida ou n�o informada.') ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmInventarioLoja_Abertura.cxButton3Click(Sender: TObject);
var
  nQtdeLote : integer;
  iQtde : string;
  i: integer;
begin
  //inherited;

  InputQuery('Gerar Lote','Quantos lotes deseja criar?',iQtde);

  nQtdeLote := StrToInt(iQtde);

  for i := 1 to nQtdeLote do
  begin

      qryLoteContagem.Insert;
      qryLoteContagemcNrLote.Value    := 'LOT' + frmMenu.ZeroEsquerda(IntToStr(i),4);
      qryLoteContagemiQtdeItens.Value := 1;
      qryLoteContagem.Post;

  end;

end;

initialization
    RegisterClass(TfrmInventarioLoja_Abertura) ;
    
end.
