inherited frmCarteiraFinancAutorPed: TfrmCarteiraFinancAutorPed
  Left = 198
  Top = 172
  Width = 766
  Height = 292
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Carteira Financeira'
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 750
    Height = 227
  end
  inherited ToolBar1: TToolBar
    Width = 750
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 750
    Height = 227
    Align = alClient
    Caption = 'Posi'#231#227'o Financeira'
    TabOrder = 1
    object Label42: TLabel
      Left = 56
      Top = 38
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Limite Cr'#233'dito'
      FocusControl = DBEdit54
    end
    object Label43: TLabel
      Left = 71
      Top = 58
      Width = 50
      Height = 13
      Alignment = taRightJustify
      Caption = 'A Receber'
      FocusControl = DBEdit55
    end
    object Label44: TLabel
      Left = 13
      Top = 78
      Width = 108
      Height = 13
      Alignment = taRightJustify
      Caption = 'A Receber - em atraso'
      FocusControl = DBEdit56
    end
    object Label45: TLabel
      Left = 36
      Top = 98
      Width = 85
      Height = 13
      Alignment = taRightJustify
      Caption = 'Venda Autorizada'
      FocusControl = DBEdit57
    end
    object Label46: TLabel
      Left = 35
      Top = 118
      Width = 86
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cheque Pendente'
      FocusControl = DBEdit58
    end
    object Label47: TLabel
      Left = 56
      Top = 138
      Width = 65
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cheque Risco'
      FocusControl = DBEdit59
    end
    object Label48: TLabel
      Left = 44
      Top = 159
      Width = 77
      Height = 13
      Alignment = taRightJustify
      Caption = 'Saldo Dispon'#237'vel'
      FocusControl = DBEdit60
    end
    object Label49: TLabel
      Left = 14
      Top = 201
      Width = 107
      Height = 13
      Alignment = taRightJustify
      Caption = 'M'#233'dia Venda ('#250'lt 12m)'
      FocusControl = DBEdit61
    end
    object Label50: TLabel
      Left = 20
      Top = 180
      Width = 101
      Height = 13
      Alignment = taRightJustify
      Caption = 'Venda ('#250'lt 12 meses)'
      FocusControl = DBEdit62
    end
    object Label1: TLabel
      Left = 82
      Top = 18
      Width = 39
      Height = 13
      Caption = 'Terceiro'
      FocusControl = DBEdit1
    end
    object DBEdit54: TDBEdit
      Tag = 1
      Left = 128
      Top = 32
      Width = 145
      Height = 21
      DataField = 'nValLimiteCred'
      DataSource = DataSource11
      TabOrder = 0
    end
    object DBEdit55: TDBEdit
      Tag = 1
      Left = 128
      Top = 52
      Width = 145
      Height = 21
      DataField = 'nValPendenciaFinanceiraReceber'
      DataSource = DataSource11
      TabOrder = 1
    end
    object DBEdit56: TDBEdit
      Tag = 1
      Left = 128
      Top = 72
      Width = 145
      Height = 21
      DataField = 'nValPendenciaFinanceiraAtrasado'
      DataSource = DataSource11
      TabOrder = 2
    end
    object DBEdit57: TDBEdit
      Tag = 1
      Left = 128
      Top = 92
      Width = 145
      Height = 21
      DataField = 'nValPedidoVendaAberto'
      DataSource = DataSource11
      TabOrder = 3
    end
    object DBEdit58: TDBEdit
      Tag = 1
      Left = 128
      Top = 112
      Width = 145
      Height = 21
      DataField = 'nValChequePendente'
      DataSource = DataSource11
      TabOrder = 4
    end
    object DBEdit59: TDBEdit
      Tag = 1
      Left = 128
      Top = 132
      Width = 145
      Height = 21
      DataField = 'nValChequeRisco'
      DataSource = DataSource11
      TabOrder = 5
    end
    object DBEdit60: TDBEdit
      Tag = 1
      Left = 128
      Top = 153
      Width = 145
      Height = 21
      DataField = 'nSaldoLimite'
      DataSource = DataSource11
      TabOrder = 6
    end
    object DBEdit61: TDBEdit
      Tag = 1
      Left = 128
      Top = 195
      Width = 145
      Height = 21
      DataField = 'nValorMedio'
      DataSource = DataSource12
      TabOrder = 7
    end
    object DBEdit62: TDBEdit
      Tag = 1
      Left = 128
      Top = 174
      Width = 145
      Height = 21
      DataField = 'nValorTotal'
      DataSource = DataSource12
      TabOrder = 8
    end
    object cxButton1: TcxButton
      Left = 275
      Top = 176
      Width = 22
      Height = 19
      TabOrder = 9
      TabStop = False
      OnClick = cxButton1Click
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
        FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
        80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
        0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
        000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
        00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
        E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
        99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
        99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
        99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
        9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
    end
    object cxButton2: TcxButton
      Left = 275
      Top = 134
      Width = 22
      Height = 19
      TabOrder = 10
      TabStop = False
      OnClick = cxButton2Click
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
        FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
        80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
        0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
        000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
        00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
        E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
        99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
        99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
        99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
        9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
    end
    object cxButton3: TcxButton
      Left = 275
      Top = 114
      Width = 22
      Height = 19
      TabOrder = 11
      TabStop = False
      OnClick = cxButton3Click
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
        FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
        80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
        0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
        000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
        00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
        E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
        99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
        99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
        99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
        9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
    end
    object cxButton4: TcxButton
      Left = 275
      Top = 94
      Width = 22
      Height = 19
      TabOrder = 12
      TabStop = False
      OnClick = cxButton4Click
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
        FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
        80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
        0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
        000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
        00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
        E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
        99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
        99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
        99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
        9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
    end
    object cxButton5: TcxButton
      Left = 275
      Top = 74
      Width = 22
      Height = 19
      TabOrder = 13
      TabStop = False
      OnClick = cxButton5Click
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
        FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
        80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
        0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
        000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
        00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
        E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
        99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
        99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
        99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
        9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
    end
    object cxButton6: TcxButton
      Left = 275
      Top = 54
      Width = 22
      Height = 19
      TabOrder = 14
      TabStop = False
      OnClick = cxButton6Click
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
        FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
        80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
        0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
        000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
        00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
        E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
        99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
        99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
        99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
        9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
    end
    object cxButton7: TcxButton
      Left = 275
      Top = 197
      Width = 22
      Height = 19
      TabOrder = 15
      TabStop = False
      OnClick = cxButton7Click
      Glyph.Data = {
        06030000424D060300000000000036000000280000000F0000000F0000000100
        180000000000D002000000000000000000000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000
        00000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFF000000FFFFFF808080000000000000FFFFFFFFFFFF
        FFFFFFFFFFFF999999999999999999999999999999FFFFFF000000FFFFFF8080
        80808080000000000000FFFFFFFFFFFFFFFFFF99999900000000000000000000
        0000999999000000FFFFFF808080808080000000FFFFFF000000FFFFFFFFFFFF
        000000000000E6E6E6E6E6E6E6E6E6E6E6E6000000000000FFFFFF8080800000
        00FFFFFFFFFFFF000000FFFFFF000000E6E6E6E6E6E6E6E6E6FFFFFFFFFFFFE6
        E6E6E6E6E6E6E6E6000000000000FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        E6E6E6FFFFFFFFFFFFD9D9D9D9D9D9FFFFFFFFFFFFE6E6E60000009999999999
        99FFFFFFFFFFFF000000000000999999D9D9D9E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6FFFFFFE6E6E6E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6FFFFFFE6E6E60000009999
        99FFFFFFFFFFFF000000000000999999999999E6E6E6E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6D9D9D9E6E6E6000000999999FFFFFFFFFFFF000000000000999999
        999999E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E6E60000009999
        99FFFFFFFFFFFF000000FFFFFF000000999999999999E6E6E6E6E6E6E6E6E6E6
        E6E6E6E6E6E6E6E6000000999999FFFFFFFFFFFFFFFFFF000000FFFFFF000000
        999999999999999999999999E6E6E6E6E6E6E6E6E6E6E6E6000000FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFF00000000000099999999999999999999
        9999000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.Kind = lfFlat
      LookAndFeel.NativeStyle = False
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 128
      Top = 10
      Width = 75
      Height = 21
      DataField = 'nCdTerceiro'
      DataSource = DataSource1
      TabOrder = 16
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 208
      Top = 10
      Width = 521
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = DataSource1
      TabOrder = 17
    end
  end
  inherited ImageList1: TImageList
    Left = 424
    Top = 72
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '      ,nValLimiteCred'
      '      ,nValCreditoUtil'
      '      ,nValPedidoAberto'
      '      ,nValRecAtraso'
      '      ,dMaiorRecAtraso'
      '      ,nValCredFinanc'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK')
    Left = 320
    Top = 64
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceironValLimiteCred: TBCDField
      FieldName = 'nValLimiteCred'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTerceironValCreditoUtil: TBCDField
      FieldName = 'nValCreditoUtil'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTerceironValPedidoAberto: TBCDField
      FieldName = 'nValPedidoAberto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTerceironValRecAtraso: TBCDField
      FieldName = 'nValRecAtraso'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTerceirodMaiorRecAtraso: TDateTimeField
      FieldName = 'dMaiorRecAtraso'
    end
    object qryTerceironValCredFinanc: TBCDField
      FieldName = 'nValCredFinanc'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTerceiro
    Left = 352
    Top = 64
  end
  object qryPosicaoFinanceira: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro int'
      ''
      'Set @nCdTerceiro = :nPK'
      ''
      'SELECT Dados.*'
      
        '      ,(Dados.nValLimiteCred - Dados.nValPendenciaFinanceiraRece' +
        'ber - Dados.nValPedidoVendaAberto - Dados.nValChequePendente) as' +
        ' nSaldoLimite'
      '  FROM (SELECT nValLimiteCred'
      #9#9#9'  ,IsNull((SELECT Sum(nSaldoTit)'
      #9#9#9#9#9#9' FROM Titulo (NOLOCK)'
      #9#9#9#9#9#9'WHERE Titulo.nSaldoTit    > 0'
      #9#9#9#9#9#9'  AND Titulo.dDtCancel   IS NULL'
      '              AND Titulo.cSenso       = '#39'C'#39
      
        '              AND Titulo.nCdEspTit   <> Convert(int,dbo.fn_LePar' +
        'ametro('#39'ET-CDR'#39'))'
      
        #9#9#9#9#9#9'  AND Titulo.nCdTerceiro  = Terceiro.nCdTerceiro),0) as nV' +
        'alPendenciaFinanceiraReceber'
      #9#9#9'  ,IsNull((SELECT Sum(nSaldoTit)'
      #9#9#9#9#9#9' FROM Titulo (NOLOCK)'
      #9#9#9#9#9#9'WHERE Titulo.nSaldoTit    > 0'
      #9#9#9#9#9#9'  AND Titulo.dDtCancel   IS NULL'
      '              AND Titulo.cSenso       = '#39'D'#39
      
        '              AND Titulo.nCdEspTit   <> Convert(int,dbo.fn_LePar' +
        'ametro('#39'ET-CDP'#39'))'
      
        #9#9#9#9#9#9'  AND Titulo.nCdTerceiro  = Terceiro.nCdTerceiro),0) as nV' +
        'alPendenciaFinanceiraPagar'
      #9#9#9'  ,IsNull((SELECT Sum(nSaldoTit)'
      #9#9#9#9#9#9' FROM Titulo (NOLOCK)'
      #9#9#9#9#9#9'WHERE Titulo.nSaldoTit    > 0'
      #9#9#9#9#9#9'  AND Titulo.dDtCancel   IS NULL'
      #9#9#9#9#9#9'  AND Titulo.nCdTerceiro  = Terceiro.nCdTerceiro'
      '              AND Titulo.cSenso       = '#39'C'#39
      
        #9#9#9#9#9#9'  AND Titulo.dDtVenc      < dbo.fn_OnlyDate(GetDate())),0)' +
        ' as nValPendenciaFinanceiraAtrasado'
      
        #9#9#9'  ,IsNull((SELECT Sum((ItemPedido.nQtdePed - ItemPedido.nQtde' +
        'ExpRec - ItemPedido.nQtdeCanc) * ItemPedido.nValCustoUnit)'
      #9#9#9#9#9#9' FROM Pedido (NOLOCK)'
      
        #9#9#9#9#9#9#9'  INNER JOIN TipoPedido (NOLOCK) ON TipoPedido.nCdTipoPed' +
        'ido = Pedido.nCdTipoPedido'
      
        #9#9#9#9#9#9#9'  INNER JOIN ItemPedido (NOLOCK) ON ItemPedido.nCdPedido ' +
        '    = Pedido.nCdPedido'
      #9#9#9#9#9#9'WHERE Pedido.nCdTerceiro         = Terceiro.nCdTerceiro'
      #9#9#9#9#9#9'  AND Pedido.nCdTabStatusPed    IN (3,4,5)'
      #9#9#9#9#9#9'  AND ItemPedido.nCdTipoItemPed IN (1,2,6,10)'
      #9#9#9#9#9#9'  AND TipoPedido.cGerarFinanc    = 1'
      '                          AND TipoPedido.cFlgVenda       = 1'
      
        '                          AND (ItemPedido.nQtdePed - ItemPedido.' +
        'nQtdeExpRec - ItemPedido.nQtdeCanc) > 0),0) as nValPedidoVendaAb' +
        'erto '
      
        #9#9#9'  ,IsNull((SELECT Sum((ItemPedido.nQtdePed - ItemPedido.nQtde' +
        'ExpRec - ItemPedido.nQtdeCanc) * ItemPedido.nValCustoUnit)'
      #9#9#9#9#9#9' FROM Pedido (NOLOCK)'
      
        #9#9#9#9#9#9#9'  INNER JOIN TipoPedido (NOLOCK) ON TipoPedido.nCdTipoPed' +
        'ido = Pedido.nCdTipoPedido'
      
        #9#9#9#9#9#9#9'  INNER JOIN ItemPedido (NOLOCK) ON ItemPedido.nCdPedido ' +
        '    = Pedido.nCdPedido'
      #9#9#9#9#9#9'WHERE Pedido.nCdTerceiro         = Terceiro.nCdTerceiro'
      #9#9#9#9#9#9'  AND Pedido.nCdTabStatusPed    IN (3,4,5)'
      #9#9#9#9#9#9'  AND ItemPedido.nCdTipoItemPed IN (1,2,6,10)'
      #9#9#9#9#9#9'  AND TipoPedido.cGerarFinanc    = 1'
      '                          AND TipoPedido.cFlgCompra      = 1'
      
        '                          AND (ItemPedido.nQtdePed - ItemPedido.' +
        'nQtdeExpRec - ItemPedido.nQtdeCanc) > 0),0) as nValPedidoCompraA' +
        'berto '
      #9#9#9'  ,IsNull((SELECT Sum(nValCheque) '
      #9#9#9#9#9#9' FROM Cheque (NOLOCK)'
      #9#9#9#9#9#9'WHERE Cheque.nCdTerceiroResp     = Terceiro.nCdTerceiro '
      
        #9#9#9#9#9#9'  AND ((Cheque.nCdTerceiroPort IS NULL) OR (Cheque.nCdTerc' +
        'eiroPort IS NOT NULL AND Cheque.dDtDeposito >= (dbo.fn_OnlyDate(' +
        'GetDate())+14)))'
      
        #9#9#9#9#9#9'  AND Cheque.nCdTabStatusCheque IN (1,2,4,5,6,8,9,10)),0) ' +
        'as nValChequePendente'
      #9#9#9'  ,IsNull((SELECT Sum(nValCheque) '
      #9#9#9#9#9#9' FROM Cheque (NOLOCK)'
      #9#9#9#9#9#9'WHERE Cheque.nCdTerceiroResp     = Terceiro.nCdTerceiro '
      
        #9#9#9#9#9#9'  AND ((Cheque.nCdTerceiroPort IS NULL) OR (Cheque.nCdTerc' +
        'eiroPort IS NOT NULL AND Cheque.dDtDeposito >= (dbo.fn_OnlyDate(' +
        'GetDate())+14)))'
      #9#9#9#9#9#9'  AND Cheque.nCdTabStatusCheque IN (1,2,4,5,6,8,9,10)'
      #9#9#9#9#9#9'  AND EXISTS(SELECT 1'
      #9#9#9#9#9#9#9#9#9'   FROM TerceiroCPFRisco TCR'
      #9#9#9#9#9#9#9#9#9'  WHERE TCR.nCdTerceiro = Terceiro.nCdTerceiro'
      
        #9#9#9#9#9#9#9#9#9#9'AND TCR.cCNPJCPF    = Cheque.cCNPJCPF)),0) as nValCheq' +
        'ueRisco        '
      #9#9'  FROM Terceiro (NOLOCK)'
      '         WHERE Terceiro.nCdTerceiro = @nCdTerceiro) Dados'
      ''
      '')
    Left = 320
    Top = 104
    object qryPosicaoFinanceiranValLimiteCred: TBCDField
      FieldName = 'nValLimiteCred'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryPosicaoFinanceiranValPendenciaFinanceiraReceber: TBCDField
      FieldName = 'nValPendenciaFinanceiraReceber'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
    object qryPosicaoFinanceiranValPendenciaFinanceiraPagar: TBCDField
      FieldName = 'nValPendenciaFinanceiraPagar'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
    object qryPosicaoFinanceiranValPendenciaFinanceiraAtrasado: TBCDField
      FieldName = 'nValPendenciaFinanceiraAtrasado'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
    object qryPosicaoFinanceiranValPedidoVendaAberto: TBCDField
      FieldName = 'nValPedidoVendaAberto'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 10
    end
    object qryPosicaoFinanceiranValPedidoCompraAberto: TBCDField
      FieldName = 'nValPedidoCompraAberto'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 10
    end
    object qryPosicaoFinanceiranValChequePendente: TBCDField
      FieldName = 'nValChequePendente'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
    object qryPosicaoFinanceiranValChequeRisco: TBCDField
      FieldName = 'nValChequeRisco'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
    object qryPosicaoFinanceiranSaldoLimite: TBCDField
      FieldName = 'nSaldoLimite'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
  end
  object qryMediaVenda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @dDtInicial        DATETIME'
      #9'   ,@dDtFinal          DATETIME'
      '       ,@nCdTerceiro       int'
      ''
      'Set @dDtInicial  = dbo.fn_OnlyDate(GetDate())-365'
      'Set @dDtFinal    = dbo.fn_OnlyDate(GetDate())'
      'Set @nCdTerceiro = :nPK'
      ''
      
        'SELECT Convert(DECIMAL(12,2),Sum(Dados.nValor1))/12 as nValorMed' +
        'io'
      ',Convert(DECIMAL(12,2),Sum(Dados.nValor1)) as nValorTotal'
      '  FROM (SELECT Sum(nQtdeAtendida * nValCustoUnit) nValor1'
      #9#9'  FROM ItemPedidoAtendido'
      
        #9#9#9'   INNER JOIN ItemPedido IP  ON IP.nCdItemPedido = ItemPedido' +
        'Atendido.nCdItemPedido'
      
        #9#9#9'   INNER JOIN Pedido         ON Pedido.nCdPedido             ' +
        '    = IP.nCdPedido'
      
        #9#9#9'   INNER JOIN TipoPedido     ON TipoPedido.nCdTipoPedido     ' +
        '    = Pedido.nCdTipoPedido'
      
        #9#9#9'   INNER JOIN Terceiro       ON Terceiro.nCdTerceiro         ' +
        '    = Pedido.nCdTerceiroPagador'
      #9#9' WHERE cFlgVenda                  = 1'
      #9#9'   AND cGerarFinanc               = 1'
      #9#9'   AND Pedido.nCdTabStatusPed    IN (3,4,5)'
      
        #9#9'   AND IP.nCdTipoItemPed         <> 6  -- N'#227'o incluir os itens' +
        ' do kit formulado'
      
        #9#9'   AND dDtAtendimento             BETWEEN @dDtInicial AND @dDt' +
        'Final'
      #9#9'   AND Pedido.nCdTerceiroPagador  = @nCdTerceiro'
      #9#9'UNION ALL'
      
        #9#9'SELECT Sum(((IPF.nQtdePed / IP.nQtdePed) * nQtdeAtendida) * IP' +
        'F.nValCustoUnit) nValor1'
      #9#9'  FROM ItemPedidoAtendido'
      
        #9#9#9'   INNER JOIN ItemPedido IP  ON IP.nCdItemPedido             ' +
        '    = ItemPedidoAtendido.nCdItemPedido'
      
        #9#9#9'   INNER JOIN ItemPedido IPF ON IPF.nCdItemPedidoPai         ' +
        '    = IP.nCdItemPedido'
      
        #9#9#9'   INNER JOIN Pedido         ON Pedido.nCdPedido             ' +
        '    = IP.nCdPedido'
      
        #9#9#9'   INNER JOIN TipoPedido     ON TipoPedido.nCdTipoPedido     ' +
        '    = Pedido.nCdTipoPedido'
      #9#9' WHERE cFlgVenda                  = 1'
      #9#9'   AND cGerarFinanc               = 1'
      #9#9'   AND Pedido.nCdTabStatusPed    IN (3,4,5)'
      
        #9#9'   AND IP.nCdTipoItemPed          = 6  -- Somente os itens do ' +
        'kit formulado'
      
        #9#9'   AND dDtAtendimento             BETWEEN @dDtInicial AND @dDt' +
        'Final'
      #9#9'   AND Pedido.nCdTerceiroPagador  = @nCdTerceiro) Dados')
    Left = 320
    Top = 136
    object qryMediaVendanValorMedio: TBCDField
      FieldName = 'nValorMedio'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 16
      Size = 6
    end
    object qryMediaVendanValorTotal: TBCDField
      FieldName = 'nValorTotal'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object DataSource11: TDataSource
    DataSet = qryPosicaoFinanceira
    Left = 352
    Top = 104
  end
  object DataSource12: TDataSource
    DataSet = qryMediaVenda
    Left = 352
    Top = 136
  end
end
