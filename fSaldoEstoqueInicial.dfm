inherited frmSaldoEstoqueInicial: TfrmSaldoEstoqueInicial
  Left = -8
  Top = -8
  Width = 1280
  Height = 786
  Caption = 'Saldo Estoque Inicial'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 97
    Width = 1264
    Height = 653
  end
  inherited ToolBar1: TToolBar
    Width = 1264
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 97
    Width = 1264
    Height = 653
    Align = alClient
    DataSource = dsProdutos
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'cNmProduto'
        Footers = <>
        ReadOnly = True
        Width = 407
      end
      item
        EditButtons = <>
        FieldName = 'nQtde'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nValCusto'
        Footers = <>
      end>
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 1264
    Height = 68
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Tag = 1
      Left = 11
      Top = 24
      Width = 81
      Height = 13
      Alignment = taRightJustify
      Caption = 'Local de Estoque'
    end
    object Label2: TLabel
      Tag = 1
      Left = 23
      Top = 48
      Width = 69
      Height = 13
      Alignment = taRightJustify
      Caption = 'Departamento'
    end
    object MaskEdit3: TMaskEdit
      Left = 96
      Top = 16
      Width = 61
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = MaskEdit3Exit
      OnKeyDown = MaskEdit3KeyDown
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 160
      Top = 16
      Width = 280
      Height = 21
      DataField = 'cNmLocalEstoque'
      DataSource = DataSource1
      TabOrder = 1
    end
    object MaskEdit1: TMaskEdit
      Left = 96
      Top = 40
      Width = 62
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 2
      Text = '      '
      OnExit = MaskEdit1Exit
      OnKeyDown = MaskEdit1KeyDown
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 160
      Top = 40
      Width = 281
      Height = 21
      DataField = 'cNmDepartamento'
      DataSource = DataSource2
      TabOrder = 3
    end
    object cxButton1: TcxButton
      Left = 448
      Top = 36
      Width = 113
      Height = 25
      Caption = 'Exibir Produtos'
      TabOrder = 4
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        1800000000000003000000000000000000000000000000000000C6D6DEC6948C
        C6948CC6948CC6948CC6948CC6948CC6D6DEC6D6DEC6D6DEC6948CC6948CC694
        8CC6948CC6948CC6948C000000000000000000000000000000000000C6948CC6
        D6DEC6D6DE000000000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948CC6948C0000000000000000000000
        00000000000000C6948C000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000C6948C000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000000000000000EFFFFFEFFF
        FF000000000000000000000000EFFFFFC6948CC6948C636363000000C6948CC6
        948C000000EFFFFFEFFFFFEFFFFFEFFFFFEFFFFFEFFFFF000000000000EFFFFF
        C6948CC6948C636363000000C6948CC6948C000000EFFFFFEFFFFFEFFFFFEFFF
        FFEFFFFFEFFFFF000000000000EFFFFFC6948CC6948C63636300000000000000
        0000000000000000000000EFFFFFEFFFFF000000000000000000000000EFFFFF
        C6948CC6948CC6948CC6948CC6948CC6948CC6948CC6948C000000EFFFFFEFFF
        FF000000000000C6948C000000EFFFFFC6948CC6948CC6948CC6948CC6948CC6
        948CC6948CC6948C000000000000000000000000000000C6948C000000EFFFFF
        C6948CC6948CC6948C000000000000000000000000000000C6948CC6948CC694
        8C636363000000C6948C000000000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000000000C6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6
        D6DEC6D6DE000000EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000
        EFFFFFC6948C636363000000C6948CC6D6DEC6D6DE000000EFFFFFC6948C6363
        63000000C6948CC6D6DEC6D6DE000000000000000000000000000000C6D6DEC6
        D6DEC6D6DE000000000000000000000000000000C6D6DEC6D6DE}
      LookAndFeel.NativeStyle = True
    end
  end
  inherited ImageList1: TImageList
    Left = 744
    Top = 432
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque, cNmLocalEstoque'
      'FROM LocalEstoque'
      'WHERE nCdEmpresa = :nCdEmpresa'
      'AND nCdLocalEstoque = :nPK')
    Left = 224
    Top = 144
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLocalEstoque
    Left = 384
    Top = 240
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento, cNmDepartamento'
      '  FROM Departamento'
      ' WHERE nCdDepartamento = :nPK'
      '')
    Left = 280
    Top = 144
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryDepartamento
    Left = 392
    Top = 248
  end
  object qryProdutos: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryProdutosBeforePost
    OnCalcFields = qryProdutosCalcFields
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Produtos'#39') IS NULL)'
      'BEGIN'
      ''
      '    CREATE TABLE #Temp_Produtos (nCdProduto int'
      
        '                                 ,nQtde      decimal(12,4) defau' +
        'lt 0 not null'
      
        '                                 ,nValCusto  decimal(12,4) defau' +
        'lt 0 not null)'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #Temp_Produtos')
    Left = 432
    Top = 216
    object qryProdutosnCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd'
      FieldName = 'nCdProduto'
    end
    object qryProdutoscNmProduto: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmProduto'
      Size = 150
      Calculated = True
    end
    object qryProdutosnQtde: TBCDField
      DisplayLabel = 'Saldo Inicial|Quantidade'
      FieldName = 'nQtde'
      Precision = 12
    end
    object qryProdutosnValCusto: TBCDField
      DisplayLabel = 'Saldo Inicial|Valor Custo Unit.'
      FieldName = 'nValCusto'
      Precision = 12
    end
  end
  object dsProdutos: TDataSource
    DataSet = qryProdutos
    Left = 480
    Top = 216
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 720
    Top = 208
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmProduto'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 528
    Top = 216
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object SP_SALDO_INICIAL: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_SALDO_INICIAL;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdLocalEstoque'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 736
    Top = 296
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#Temp_Produtos'#39') IS NULL)'#13#10'BEGIN'#13#10#13#10'    C' +
      'REATE TABLE #Temp_Produtos (nCdProduto int'#13#10'                    ' +
      '             ,nQtde      decimal(12,4) default 0 not null'#13#10'     ' +
      '                            ,nValCusto  decimal(12,4) default 0 ' +
      'not null)'#13#10#13#10'END'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 496
    Top = 288
  end
end
