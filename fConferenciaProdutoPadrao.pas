unit fConferenciaProdutoPadrao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, StdCtrls, Mask, DBCtrls, DB, DBClient, DBGridEhGrouping,
  ToolCtrlsEh, ADODB;

type
  TfrmConferenciaProdutoPadrao = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    cdsProduto: TClientDataSet;
    cdsProdutonQtde: TFloatField;
    cdsProdutocCodigo: TStringField;
    dsClientProduto: TDataSource;
    cdsProdutoiPosicao: TIntegerField;
    cdsProdutocNmProduto: TStringField;
    cdsProdutosConfere: TClientDataSet;
    cdsProdutosConferecCdProduto: TStringField;
    cdsProdutosConferecNmProduto: TStringField;
    cdsProdutosConferecEAN: TStringField;
    cdsProdutosConferecEANFornec: TStringField;
    Panel1: TPanel;
    gbProduto: TGroupBox;
    Label1: TLabel;
    edtEAN: TEdit;
    gbModo: TGroupBox;
    Image2: TImage;
    Image3: TImage;
    rbDigitacaoLeitura: TRadioButton;
    rbConferencia: TRadioButton;
    SP_CONSULTA_PRODUTO: TADOStoredProc;
    qryTempProdutosBip: TADOQuery;
    cmdPreparaTemp: TADOCommand;
    qryTempProdutosBipcCdProduto: TStringField;
    qryTempProdutosBipnQtdeBipada: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtEANKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    iPosicao  : integer ;
  public
    { Public declarations }
    bProcessa     : Boolean;
    dDtInicioConf : TDateTime;
    dDtFimConf    : TDateTime;
  end;

var
  frmConferenciaProdutoPadrao: TfrmConferenciaProdutoPadrao;

implementation

uses fLookup_Padrao, fTransfEst, fMenu;

{$R *.dfm}

procedure TfrmConferenciaProdutoPadrao.FormShow(Sender: TObject);
begin
  inherited;

  if (((not rbDigitacaoLeitura.Checked) and (not rbConferencia.Checked))
     or(rbDigitacaoLeitura.Checked = rbConferencia.Checked)) then
  begin
      MensagemErro('Modo de uso n�o configurado corretamente.');
      ToolButton1.Enabled := False;
  end;

  iPosicao          := 1;
  bProcessa         := False;
  Label1.Font.Color := clBlack;

  rbDigitacaoLeitura.Enabled := rbDigitacaoLeitura.Checked;
  rbConferencia.Enabled      := rbConferencia.Checked;

  edtEAN.MaxLength := StrToint(frmMenu.LeParametro('TMMXBRPRPDV'));
  edtEAN.SetFocus;
end;

procedure TfrmConferenciaProdutoPadrao.edtEANKeyDown(Sender: TObject;  var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key <> 13) then
      Exit;

  edtEAN.Text := Trim(edtEAN.Text);

  if (edtEAN.Text = '') then
      Abort;

   if (dDtInicioConf = 0) then
      dDtInicioConf := Now();

  try
      { -- digita��o/leitura de produtos -- }
      if (rbDigitacaoLeitura.Checked) then
      begin
          SP_CONSULTA_PRODUTO.Close;
          SP_CONSULTA_PRODUTO.Parameters.ParamByName('@cCdProduto').Value        := edtEAN.Text;
          SP_CONSULTA_PRODUTO.Parameters.ParamByName('@nCdTabTipoProduto').Value := 3; //produto final
          SP_CONSULTA_PRODUTO.ExecProc;

          if (SP_CONSULTA_PRODUTO.Parameters.ParamByName('@nCdProduto').Value = Null) then
          begin
              MensagemAlerta('Produto com c�digo ' + edtEAN.Text + ' n�o encontrado.');
              Exit;
          end;

          cdsProduto.Insert;
          cdsProdutocCodigo.Value    := SP_CONSULTA_PRODUTO.Parameters.ParamByName('@nCdProduto').Value;
          cdsProdutocNmProduto.Value := SP_CONSULTA_PRODUTO.Parameters.ParamByName('@cNmProduto').Value;
          cdsProdutonQtde.Value      := 1;
          cdsProdutoiPosicao.Value   := iPosicao;
          cdsProduto.Post;

          Inc(iPosicao);
      end;

      { -- confer�ncia de produtos -- }
      if (rbConferencia.Checked) then
      begin
          if (  (cdsProdutosConfere.Locate('cCdProduto', Trim(edtEAN.Text), []) = True)
              or(cdsProdutosConfere.Locate('cEAN',       Trim(edtEAN.Text), []) = True)
              or(cdsProdutosConfere.Locate('cEANFornec', Trim(edtEAN.Text), []) = True)) then
          begin
              cdsProduto.Insert;
              cdsProdutocCodigo.Value    := cdsProdutosConferecCdProduto.Value;
              cdsProdutocNmProduto.Value := cdsProdutosConferecNmProduto.Value;
              cdsProdutonQtde.Value      := 1;
              cdsProdutoiPosicao.Value   := iPosicao;
              cdsProduto.Post;

              Inc(iPosicao);
          end
          else
          begin
              MensagemAlerta('Produto n�o encontrado.');
              Exit;
          end;
      end;
  finally
      edtEAN.Text := '';
      edtEAN.SetFocus;
  end;
end;

procedure TfrmConferenciaProdutoPadrao.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  //inherited;
end;

procedure TfrmConferenciaProdutoPadrao.ToolButton1Click(Sender: TObject);
begin
  if (dDtFimConf = 0) then
      dDtFimConf := Now();

  if (cdsProduto.State = dsEdit) and (cdsProdutocCodigo.Value <> '') then
      cdsProduto.Post;

  if (cdsProduto.IsEmpty) then
  begin
     MensagemAlerta('Nenhum produto informado.');
     Abort;
  end;

  cmdPreparaTemp.Execute;

  try
      cdsProduto.First;

      while (not cdsProduto.Eof) do
      begin
          qryTempProdutosBip.Close;
          qryTempProdutosBip.Open;
          qryTempProdutosBip.Insert;
          qryTempProdutosBipcCdProduto.Value  := cdsProdutocCodigo.Value;
          qryTempProdutosBipnQtdeBipada.Value := cdsProdutonQtde.AsInteger;
          qryTempProdutosBip.Post;
          cdsProduto.Next;
      end;

      cdsProduto.EmptyDataSet;
  except
      MensagemErro('Falha ao contabilizar os produtos inseridos, tente novamente.');
      Raise;
      Exit;
  end;

  bProcessa := True;
  Close;
end;

procedure TfrmConferenciaProdutoPadrao.FormCreate(Sender: TObject);
begin
  inherited;

  cdsProdutosConfere.EmptyDataSet;
  cdsProduto.EmptyDataSet;
end;

end.

