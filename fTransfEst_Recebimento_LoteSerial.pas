unit fTransfEst_Recebimento_LoteSerial;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, DB, ADODB;

type
  TfrmTransfEst_Recebimento_LoteSerial = class(TfrmProcesso_Padrao)
    ToolButton4: TToolButton;
    btRegistroLoteSerial: TToolButton;
    qryItemLoteSerial: TADOQuery;
    qryItemLoteSerialnCdItemTransfEst: TIntegerField;
    qryItemLoteSerialnCdProduto: TIntegerField;
    qryItemLoteSerialcNmProduto: TStringField;
    qryItemLoteSerialnQtde: TBCDField;
    qryItemLoteSerialnValUnitario: TBCDField;
    qryItemLoteSerialnValTotal: TBCDField;
    qryItemLoteSerialnCdTransfEst: TIntegerField;
    qryItemLoteSerialnQtdeConf: TBCDField;
    dsItemLoteSerial: TDataSource;
    qryItemLoteSerialnCdTabTipoModoGestaoProduto: TIntegerField;
    qryItemLoteSerialcNmTabTipoModoGestaoProduto: TStringField;
    DBGridEh1: TDBGridEh;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    procedure btRegistroLoteSerialClick(Sender: TObject);
    procedure qryItemLoteSerialAfterScroll(DataSet: TDataSet);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    nCdLocalEstoque         : integer;
    bReadOnly               : Boolean;
    nCdTransfEst            : integer;
    nCdTabTipoOrigemMovLote : integer;
    bPermiteCriar           : Boolean;
    bValidaLoteSerial       : Boolean;
  public
    { Public declarations }
    bLoteProcessado : Boolean;
    procedure ItemLoteSerial(nCdTransfEstAux,nCdTabTipoOrigemMovLoteAux,nCdLocalEstoqueAux : integer; bPermiteCriarAux, bReadOnlyAux : boolean; bValidaLoteSerialAux : boolean = False);
  end;

var
  frmTransfEst_Recebimento_LoteSerial: TfrmTransfEst_Recebimento_LoteSerial;

implementation

uses fRegistroMovLote;

{$R *.dfm}

procedure TfrmTransfEst_Recebimento_LoteSerial.btRegistroLoteSerialClick(
  Sender: TObject);
var
    objForm : TfrmRegistroMovLote ;
begin

  objForm := TfrmRegistroMovLote.Create( Self ) ;

  objForm.registraMovLote( qryItemLoteSerialnCdProduto.Value
                         , 0
                         , nCdTabTipoOrigemMovLote {-- Tipo de movimentação que esta sendo realizada --}
                         , qryItemLoteSerialnCdItemTransfEst.Value
                         , nCdLocalEstoque
                         , qryItemLoteSerialnQtde.Value
                         , 'ITEM TRANSFERÊNCIA ENTRE ESTOQUES MANUAL No. ' + Trim( qryItemLoteSerialnCdItemTransfEst.AsString )
                         , bPermiteCriar
                         , bReadOnly
                         , bValidaLoteSerial) ;

  freeAndNil( objForm ) ;
  
end;

procedure TfrmTransfEst_Recebimento_LoteSerial.qryItemLoteSerialAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  if (qryItemLoteSerialnCdTabTipoModoGestaoProduto.Value = 1) then
      btRegistroLoteSerial.Enabled := False
  else btRegistroLoteSerial.Enabled := True;
  
end;

procedure TfrmTransfEst_Recebimento_LoteSerial.DBGridEh1DblClick(
  Sender: TObject);
begin
  inherited;

  btRegistroLoteSerial.Click;

end;

procedure TfrmTransfEst_Recebimento_LoteSerial.ItemLoteSerial(
  nCdTransfEstAux, nCdTabTipoOrigemMovLoteAux, nCdLocalEstoqueAux: integer;
  bPermiteCriarAux, bReadOnlyAux, bValidaLoteSerialAux: boolean);
begin
    nCdTransfEst            := nCdTransfEstAux;
    nCdTabTipoOrigemMovLote := nCdTabTipoOrigemMovLoteAux;
    nCdLocalEstoque         := nCdLocalEstoqueAux;
    bReadOnly               := bReadOnlyAux;
    bPermiteCriar           := bPermiteCriarAux;
    bValidaLoteSerial       := bValidaLoteSerialAux ;
    
    qryItemLoteSerial.Close;
    qryItemLoteSerial.Parameters.ParamByName('nPK').Value := nCdTransfEst;
    qryItemLoteSerial.Open;

    Self.ShowModal;
end;

procedure TfrmTransfEst_Recebimento_LoteSerial.ToolButton1Click(
  Sender: TObject);
var
  objForm : TfrmRegistroMovLote ;
begin

  objForm := TfrmRegistroMovLote.Create( Self ) ;

  qryItemLoteSerial.First;

  while (not qryItemLoteSerial.Eof) do
  begin

      if (not objForm.validaSerialLote(qryItemLoteSerialnCdItemTransfEst.Value,5,qryItemLoteSerialcNmProduto.Value)) then
          abort;

      qryItemLoteSerial.Next;

  end;

  bLoteProcessado := True;

  Close;
end;

end.
