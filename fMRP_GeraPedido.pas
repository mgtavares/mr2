unit fMRP_GeraPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, StdCtrls, Mask, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit, DB, ADODB, cxMaskEdit, cxDropDownEdit, cxCalendar, Menus,
  DBCtrls, DBGridEhGrouping;

type
  TfrmMRP_GeraPedido = class(TfrmProcesso_Padrao)
    edtQtdeSugerida: TcxCurrencyEdit;
    Label1: TLabel;
    Label3: TLabel;
    DBGridEh1: TDBGridEh;
    qryFornecedores: TADOQuery;
    qryFornecedoresnCdTerceiro: TIntegerField;
    qryFornecedorescNmTerceiro: TStringField;
    qryFornecedoresnPercentCompra: TBCDField;
    qryFornecedorescNrContrato: TStringField;
    dsFornecedores: TDataSource;
    StaticText2: TStaticText;
    DBGridEh2: TDBGridEh;
    StaticText1: TStaticText;
    Label7: TLabel;
    qryFormula: TADOQuery;
    qryFormulanCdProduto: TIntegerField;
    qryFormulacNmProduto: TStringField;
    qryFormulanQtdeNecessaria: TBCDField;
    qryFormulanQtdeCompra: TBCDField;
    qryFormulanQtdeEstoque: TBCDField;
    qryFormulanTempEstoque: TIntegerField;
    qryFormulanConsumoMedDia: TBCDField;
    qryFormuladDtPrevEntrega: TDateTimeField;
    qryFormulanQtdePedido: TBCDField;
    dsFormula: TDataSource;
    PopupMenu1: TPopupMenu;
    FornecedoresHomologados1: TMenuItem;
    ltimosPedidos1: TMenuItem;
    DetalharEstoqueAtual1: TMenuItem;
    PedidosemAberto1: TMenuItem;
    BalanceamentoFornecedores1: TMenuItem;
    qryFormulacFlgFornecHom: TIntegerField;
    Label9: TLabel;
    cxTextEdit2: TcxTextEdit;
    qryFornecedoresnCdProduto: TIntegerField;
    qryConferePercentCompra: TADOQuery;
    qryConferePercentCompranCdProduto: TIntegerField;
    qryConferePercentCompranPercentCompra: TBCDField;
    SP_GERA_PEDIDO_MRP: TADOStoredProc;
    qryFormulanQtdeFirme: TBCDField;
    qryFormulanQtdePrev: TBCDField;
    VerCadastrodoProduto1: TMenuItem;
    qryPrepara_Temp_Fornecedores: TADOQuery;
    cmdTempFormula: TADOCommand;
    edtUnidadeMedidaCompra: TEdit;
    edtProduto: TEdit;
    Label10: TLabel;
    edtFatorCompra: TEdit;
    Label11: TLabel;
    edtQtdeMinimaCompra: TEdit;
    Label12: TLabel;
    qryProduto: TADOQuery;
    qryProdutocUnidadeMedidaCompra: TStringField;
    qryProdutonFatorCompra: TBCDField;
    qryProdutonQtdeMinimaCompra: TBCDField;
    edtUnidadeMedida1: TEdit;
    qryProdutocUnidadeMedida: TStringField;
    edtUnidadeMedidaCompra2: TEdit;
    edtQtdeParaPedido: TcxCurrencyEdit;
    Label13: TLabel;
    GroupBox1: TGroupBox;
    edtQtdeItens: TcxCurrencyEdit;
    Label6: TLabel;
    edtPedidoPrev: TcxMaskEdit;
    Label5: TLabel;
    edtPedidoFirme: TcxMaskEdit;
    Label4: TLabel;
    edtUnidadeMedidaCompra3: TEdit;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryFormulacUnidadeMedidaCompra: TStringField;
    qryFormulanFatorCompra: TBCDField;
    qryFormulanQtdeMinimaCompra: TBCDField;
    qryFormulacUnidadeMedida: TStringField;
    MaskEdit6: TMaskEdit;
    Label14: TLabel;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label15: TLabel;
    qryLocalizaEstoqueEntrega: TADOQuery;
    qryLocalizaEstoqueEntreganCdLocalEstoque: TIntegerField;
    qryFormulanCdLocalEstoqueEntrega: TIntegerField;
    GroupBox2: TGroupBox;
    Label8: TLabel;
    DBEdit2: TDBEdit;
    Label16: TLabel;
    DBEdit3: TDBEdit;
    Label17: TLabel;
    DBEdit4: TDBEdit;
    Label18: TLabel;
    DBEdit5: TDBEdit;
    qryFormulacNmLocalEstoque: TStringField;
    qryAux: TADOQuery;
    Label19: TLabel;
    DBEdit6: TDBEdit;
    Label20: TLabel;
    DBEdit7: TDBEdit;
    edtPrevEntrega: TDateTimePicker;
    qryFornecedoresnPrecoUnit: TFloatField;
    procedure edtQtdeRealExit(Sender: TObject);
    procedure DetalharEstoqueAtual1Click(Sender: TObject);
    procedure PedidosemAberto1Click(Sender: TObject);
    procedure FornecedoresHomologados1Click(Sender: TObject);
    procedure BalanceamentoFornecedores1Click(Sender: TObject);
    procedure DBGridEh2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure VerCadastrodoProduto1Click(Sender: TObject);
    procedure qryFormulaBeforePost(DataSet: TDataSet);
    procedure ltimosPedidos1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure MaskEdit6Enter(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryFormulaCalcFields(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdProduto    : integer ;
    nCdLoja       : integer ;
    bPedidoGerado : boolean ;
  end;

var
  frmMRP_GeraPedido: TfrmMRP_GeraPedido;

implementation

uses fMenu, fMRP_DetalheEstoqueAtual, fPedidoAbertoProduto,
  fMRP_FornecHomol, fMRP_BalancoFornecedores, fProdutoERP,
  fMRP_UltimosPedidos, fLookup_Padrao;

{$R *.dfm}

procedure TfrmMRP_GeraPedido.edtQtdeRealExit(Sender: TObject);
begin
  inherited;

{ qryFormula.Close ;
  qryFormula.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
  qryFormula.Parameters.ParamByName('nCdProduto').Value  := nCdProduto ;
  qryFormula.Parameters.ParamByName('nQtdeCompra').Value := edtQtdeReal.Value ;
  qryFormula.Parameters.ParamByName('nCdLoja').Value     := nCdLoja ;
  qryFormula.Open ;

  if (qryFormula.Eof) then
  begin
      GroupBox2.Visible   := False ;
      DBGridEh2.Visible   := False ;
      StaticText1.Visible := False ;
      Label9.Visible      := False ;
      cxTextEdit2.Visible := False ;
  end
  else begin
      GroupBox2.Visible   := True ;
      DBGridEh2.Visible   := True ;
      StaticText1.Visible := True ;
      Label9.Visible      := True ;
      cxTextEdit2.Visible := True ;
  end ;

  // localiza o local de estoque para os insumos
  qryFormula.First ;

  while not qryFormula.eof do
  begin

      qryLocalizaEstoqueEntrega.Close ;
      qryLocalizaEstoqueEntrega.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      qryLocalizaEstoqueEntrega.Parameters.ParamByName('nCdLoja').Value    := nCdLoja ;
      qryLocalizaEstoqueEntrega.Parameters.ParamByName('nCdProduto').Value := qryFormulanCdProduto.Value ;
      qryLocalizaEstoqueEntrega.Open ;

      if (not qryLocalizaEstoqueEntrega.Eof) then
      begin

          qryFormula.Edit ;
          qryFormulanCdLocalEstoqueEntrega.Value := qryLocalizaEstoqueEntreganCdLocalEstoque.Value ;
          qryFormula.Post ;

      end ;

      qryFormula.Next ;
  end ;

  qryFormula.First ;  }

  if (edtFatorCompra.Text = '') then
      edtFatorCompra.Text := '1' ;

  if (edtQtdeMinimaCompra.Text = '') then
      edtQtdeMinimaCompra.Text := '1' ;

  if StrToFloat(edtQtdeParaPedido.Text) < StrToFloat(edtQtdeMinimaCompra.Text) then
  begin

      edtQtdeParaPedido.Text := edtQtdeMinimaCompra.Text ;

  end ;

end;

procedure TfrmMRP_GeraPedido.DetalharEstoqueAtual1Click(Sender: TObject);
var
  objForm : TfrmMRP_DetalheEstoqueAtual;
begin
  inherited;

  objForm := TfrmMRP_DetalheEstoqueAtual.Create(nil);

  PosicionaQuery(objForm.qryPosicaoEstoque, qryFormulanCdProduto.AsString) ;
  showForm(objForm,true);

end;

procedure TfrmMRP_GeraPedido.PedidosemAberto1Click(Sender: TObject);
var
  objForm : TfrmPedidoAbertoProduto;
begin
  inherited;

  objForm := TfrmPedidoAbertoProduto.Create(nil);

  objForm.qryMaster.Close ;
  objForm.qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  objForm.qryMaster.Parameters.ParamByName('nCdProduto').Value := qryFormulanCdProduto.Value ;
  objForm.qryMaster.Parameters.ParamByName('nCdLoja').Value    := nCdLoja    ;
  objForm.qryMaster.Open ;

  if not objForm.qryMaster.Eof then
      showForm(objForm,true);

end;

procedure TfrmMRP_GeraPedido.FornecedoresHomologados1Click(
  Sender: TObject);
var
  objForm : TfrmMRP_FornecHomol;
begin
  inherited;

  objForm := TfrmMRP_FornecHomol.Create(nil);

  PosicionaQuery(objForm.qryFornecedores, qryFormulanCdProduto.AsString) ;
  showForm(objForm,true) ;

end;

procedure TfrmMRP_GeraPedido.BalanceamentoFornecedores1Click(
  Sender: TObject);
var
  objForm : TfrmMRP_BalancoFornecedores;
begin
  inherited;

  objForm := TfrmMRP_BalancoFornecedores.Create(nil);

  PosicionaQuery(objForm.qryFornecedores, qryFormulanCdProduto.AsString) ;

  if (objForm.qryFornecedores.eof) then
      MensagemAlerta('Nenhum fornecedor homologado para este item.') ;

  showForm(objForm,true);

end;

procedure TfrmMRP_GeraPedido.DBGridEh2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  If (dataCol = 1) then
  begin

      if (qryFormulacFlgFornecHom.Value = 0) then
      begin
          DBGridEh2.Canvas.Brush.Color := clYellow;
          DBGridEh2.Canvas.Font.Color  := clBlack;
          DBGridEh2.Canvas.FillRect(Rect);
          DBGridEh2.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmMRP_GeraPedido.FormShow(Sender: TObject);
begin
  inherited;

  qryFornecedoresnPrecoUnit.DisplayFormat := frmMenu.cMascaraCompras;
  
  bPedidoGerado := false ;
  
  PosicionaQuery(qryProduto, IntToStr(nCdProduto)) ;

  edtUnidadeMedidaCompra.Text  := qryProdutocUnidadeMedidaCompra.Value ;
  edtUnidadeMedidaCompra2.Text := qryProdutocUnidadeMedidaCompra.Value ;
  edtUnidadeMedidaCompra3.Text := qryProdutocUnidadeMedidaCompra.Value ;

  edtUnidadeMedida1.Text := qryProdutocUnidadeMedida.Value ;

  edtFatorCompra.Text         := qryProdutonFatorCompra.AsString      ;
  edtQtdeMinimaCompra.Text    := qryProdutonQtdeMinimaCompra.AsString ;
  

  if (edtFatorCompra.Text = '') then
      edtFatorCompra.Text := '1' ;

  if (edtQtdeMinimaCompra.Text = '') then
      edtQtdeMinimaCompra.Text := '1' ;

  if (edtQtdeParaPedido.Value < StrToFloat(edtQtdeMinimaCompra.Text)) then
  begin

      edtQtdeParaPedido.Text := edtQtdeMinimaCompra.Text ;

  end ;
  
  If (edtQtdeItens.Value > 0) then
  begin

      edtQtdeItens.Text := FloatToStr(Round((edtQtdeItens.Value / StrToFloat(edtFatorCompra.Text))+0.5)) ;

      if (edtQtdeItens.Value < StrToFloat(edtQtdeMinimaCompra.Text)) then
          edtQtdeItens.Value := StrToFloat(edtQtdeMinimaCompra.Text) ;

  end ;

  {mdTempFormula.Execute;

  qryFormula.Close ;
  qryFormula.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
  qryFormula.Parameters.ParamByName('nCdProduto').Value  := nCdProduto ;
  qryFormula.Parameters.ParamByName('nQtdeCompra').Value := edtQtdeReal.Value ;
  qryFormula.Parameters.ParamByName('nCdLoja').Value     := nCdLoja ;
  qryFormula.Open ;

  if (qryFormula.Eof) then
  begin
      GroupBox2.Visible   := False ;
      DBGridEh2.Visible   := False ;
      StaticText1.Visible := False ;
      Label9.Visible      := False ;
      cxTextEdit2.Visible := False ;
  end
  else begin
      GroupBox2.Visible   := True ;
      DBGridEh2.Visible   := True ;
      StaticText1.Visible := True ;
      Label9.Visible      := True ;
      cxTextEdit2.Visible := True ;
  end ;

  MaskEdit6.Text := '' ;  }
  qryLocalEstoque.Close ;

  // localiza o local de estoque do itens principal
  qryLocalizaEstoqueEntrega.Close ;
  qryLocalizaEstoqueEntrega.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryLocalizaEstoqueEntrega.Parameters.ParamByName('nCdLoja').Value    := nCdLoja ;
  qryLocalizaEstoqueEntrega.Parameters.ParamByName('nCdProduto').Value := nCdProduto ;
  qryLocalizaEstoqueEntrega.Open ;

  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryLocalEstoque.Parameters.ParamByName('nCdProduto').Value := nCdProduto              ;

  if (not qryLocalizaEstoqueEntrega.Eof) then
  begin
      MaskEdit6.Text := qryLocalizaEstoqueEntreganCdLocalEstoque.AsString;
      PosicionaQuery(qryLocalEstoque, MaskEdit6.Text) ;
  end
  else begin
      MensagemAlerta('Local de Estoque para entrega n�o encontrado.') ;
  end ;

  // localiza o local de estoque para os insumos
  {ryFormula.First ;

  while not qryFormula.eof do
  begin

      qryLocalizaEstoqueEntrega.Close ;
      qryLocalizaEstoqueEntrega.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      qryLocalizaEstoqueEntrega.Parameters.ParamByName('nCdLoja').Value    := nCdLoja ;
      qryLocalizaEstoqueEntrega.Parameters.ParamByName('nCdProduto').Value := qryFormulanCdProduto.Value ;
      qryLocalizaEstoqueEntrega.Open ;

      if (not qryLocalizaEstoqueEntrega.Eof) then
      begin

          qryFormula.Edit ;
          qryFormulanCdLocalEstoqueEntrega.Value := qryLocalizaEstoqueEntreganCdLocalEstoque.Value ;
          qryFormula.Post ;

      end ;

      qryFormula.Next ;
  end ;

  qryFormula.First ;  }

  edtQtdeParaPedido.SetFocus;

end;

procedure TfrmMRP_GeraPedido.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (Trim(edtPedidoFirme.Text) = '') then
      edtPedidoFirme.Text := '0' ;

  if (Trim(edtPedidoPrev.Text) = '') then
      edtPedidoPrev.Text := '0' ;

  if (StrToFloat(edtQtdeParaPedido.Text) <= 0) then
  begin
      MensagemAlerta('Informe a quantidade para o pedido de compra.') ;
      edtQtdeParaPedido.SetFocus;
      exit ;
  end ;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Informe o local de estoque para entrega do produto.') ;
      MaskEdit6.SetFocus;
      exit ;
  end ;

  if (edtPrevEntrega.Date < StrToDate(DateToStr(Now()))) then
  begin
      MensagemAlerta('Previs�o de entrega inv�lida.') ;
      edtPrevEntrega.SetFocus;
      exit ;
  end ;

  if (StrToInt(Trim(edtPedidoFirme.Text)) < 0) then
  begin
      MensagemAlerta('N�mero de pedidos firmes inv�lido.') ;
      edtPedidoFirme.SetFocus;
      exit ;
  end ;

  if (StrToInt(Trim(edtPedidoPrev.Text)) < 0) then
  begin
      MensagemAlerta('N�mero de pedidos previstos inv�lido.') ;
      edtPedidoFirme.SetFocus;
      exit ;
  end ;

  if (edtQtdeItens.Value < 0) then
  begin
      MensagemAlerta('Quantidade de itens inv�lida.') ;
      edtQtdeItens.SetFocus;
      exit ;
  end ;

  if StrToFloat(edtQtdeParaPedido.Text) < StrToFloat(edtQtdeMinimaCompra.Text) then
  begin

      case MessageDlg('A quantidade a ser pedido � menor que a quantidade m�nima para compra. Continuar ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo: abort ;
      end ;

  end ;

  if (qryFornecedores.State = dsEdit) then
      qryFornecedores.Post ;

  // Confere se todos os itens tens fornecedores homologados
  if not qryFormula.Eof then
  begin
      qryFormula.First ;

      while not qryFormula.Eof do
      begin

          if (qryFormulacFlgFornecHom.Value = 0) then
          begin
              MensagemAlerta('O produto ' + qryFormulanCdProduto.AsString + ' n�o tem fornecedor homologado. Imposs�vel gerar pedido.') ;
              exit ;
          end ;

          if (qryFormulanQtdeCompra.Value < 0) then
          begin
              MensagemAlerta('Quantidade a comprar inv�lida.') ;
              exit ;
          end ;

          if (qryFormulanQtdeFirme.Value < 0) then
          begin
              MensagemAlerta('Quantidade para pedido firme inv�lida.') ;
              exit ;
          end ;

          if (qryFormulanQtdePrev.Value < 0) then
          begin
              MensagemAlerta('Quantidade para pedido previsto inv�lida.') ;
              exit ;
          end ;

          if (qryFormulanQtdeCompra.Value > 0) and (qryFormuladDtPrevEntrega.AsString = '') then
          begin
              MensagemAlerta('Informe a data prevista de entrega do item da f�rmula.') ;
              exit ;
          end ;

          if ((qryFormulanQtdeCompra.Value / qryFormulanFatorCompra.Value) < qryFormulanQtdeMinimaCompra.Value) then
          begin
              if (qryFormulanQtdeCompra.Value > 0) then
              begin

                  case MessageDlg('O quantidade do item ' + Trim(qryFormulacNmProduto.Value) + ' est� abaixo do m�nimo para compra. Continua ?',mtConfirmation,[mbYes,mbNo],0) of
                      mrNo:abort ;
                  end ;

              end ;

          end ;

          qryFormula.Next ;
      end ;

      qryFormula.First ;
  end ;


  // Confere o balanceamento dos fornecedores
  qryConferePercentCompra.Close ;
  qryConferePercentCompra.Open ;

  qryConferePercentCompra.First ;

  while not qryConferePercentCompra.Eof do
  begin

      if (qryConferePercentCompranPercentCompra.Value <> 100) then
      begin
          MensagemAlerta('Problemas na divis�o de percentual de compra entre os fornecedores. O total � diferente de 100%') ;
          exit ;
      end ;

      qryConferePercentCompra.Next  ;
  end ;

  qryConferePercentCompra.Close ;

  case MessageDlg('Confirma gera��o do(s) pedido(s) ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  if not qryFormula.eof then
  begin

      qryFormula.First ;

      while not qryFormula.eof do
      begin

          qryFormula.Edit ;

          if (qryFormulacUnidadeMedida.Value <> qryFormulacUnidadeMedidaCompra.Value) then
          begin
              qryFormulacUnidadeMedida.Value := qryFormulacUnidadeMedidaCompra.Value ;

              if (qryFormulanQtdeCompra.Value > 0) then
                  qryFormulanQtdeCompra.Value    := qryFormulanQtdeCompra.Value / qryFormulanFatorCompra.Value ;

              if (qryFormulanQtdeFirme.Value > 0) then
                  qryFormulanQtdeFirme.Value := qryFormulanQtdeFirme.Value / qryFormulanFatorCompra.Value ;
          end ;

          if (qryFormulanQtdeCompra.Value < qryFormulanQtdeMinimaCompra.Value) and (qryFormulanQtdeCompra.Value > 0) then
              qryFormulanQtdeCompra.Value := qryFormulanQtdeMinimaCompra.Value ;

          if (qryFormulanQtdeFirme.Value < qryFormulanQtdeMinimaCompra.Value) and (qryFormulanQtdeCompra.Value > 0) then
              qryFormulanQtdeFirme.Value := qryFormulanQtdeMinimaCompra.Value ;

          if (qryFormulanQtdeCompra.Value = 0) then
              qryFormulanQtdeFirme.Value := 0 ;

          qryFormula.Post;

          qryFormula.Next ;

      end ;

      qryFormula.First ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try

      SP_GERA_PEDIDO_MRP.Close ;
      SP_GERA_PEDIDO_MRP.Parameters.ParamByName('@nCdEmpresa').Value             := frmMenu.nCdEmpresaAtiva;
      SP_GERA_PEDIDO_MRP.Parameters.ParamByName('@nCdProduto').Value             := nCdProduto ;
      SP_GERA_PEDIDO_MRP.Parameters.ParamByName('@nQtdeComprar').Value           := StrToFloat(edtQtdeParaPedido.Text) ;
      SP_GERA_PEDIDO_MRP.Parameters.ParamByName('@dDtEntregaPrev').Value         := DateToStr(edtPrevEntrega.Date);
      SP_GERA_PEDIDO_MRP.Parameters.ParamByName('@iPedidoFirme').Value           := frmMenu.ConvInteiro(edtPedidoFirme.Text) ;
      SP_GERA_PEDIDO_MRP.Parameters.ParamByName('@iPedidoPrev').Value            := frmMenu.ConvInteiro(edtPedidoPrev.Text) ;
      SP_GERA_PEDIDO_MRP.Parameters.ParamByName('@nQtdeFirme').Value             := edtQtdeItens.Value ;
      SP_GERA_PEDIDO_MRP.Parameters.ParamByName('@nCdUsuario').Value             := frmMenu.nCdUsuarioLogado;
      SP_GERA_PEDIDO_MRP.Parameters.ParamByName('@nCdLoja').Value                := nCdLoja ;
      SP_GERA_PEDIDO_MRP.Parameters.ParamByName('@nCdLocalEstoqueEntrega').Value := frmMenu.ConvInteiro(MaskEdit6.Text) ;
      SP_GERA_PEDIDO_MRP.ExecProc ;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Pedido gerado com sucesso.') ;
  bPedidoGerado := True ;
  Close ;

end;

procedure TfrmMRP_GeraPedido.VerCadastrodoProduto1Click(Sender: TObject);
var
  objForm : TfrmProdutoERP;
begin
  inherited;

  if (qryFormulanCdProduto.Value > 0) then
  begin
      objForm := TfrmProdutoERP.Create(nil);

      PosicionaQuery(objForm.qryMaster, qryFormulanCdProduto.AsString) ;

      objForm.btIncluir.Enabled    := False ;
      objForm.btSalvar.Enabled     := False ;
      objForm.btCancelar.Enabled   := False ;
      objForm.btConsultar.Enabled  := False ;
      objForm.ToolButton1.Enabled  := False ;
      objForm.btGerarGrade.Enabled := False ;
      objForm.cxButton1.Enabled    := False ;

      showForm(objForm,true) ;

  end ;

end;

procedure TfrmMRP_GeraPedido.qryFormulaBeforePost(DataSet: TDataSet);
begin
  inherited;

  {f (qryFormulanQtdeCompra.Value <= 0) then
  begin
      qryFormulanQtdeCompra.Value := 0 ;
      qryFormulanQtdeFirme.Value  := 0 ;
      qryFormulanQtdePrev.Value   := 0 ;
  end ;}

end;

procedure TfrmMRP_GeraPedido.ltimosPedidos1Click(Sender: TObject);
var
  objForm : TfrmMRP_UltimosPedidos;
begin
  inherited;

  if (qryFormulanCdProduto.Value > 0) then
  begin

      objForm := TfrmMRP_UltimosPedidos.Create(nil);

      objForm.qryResultado.Close ;
      objForm.qryResultado.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      objForm.qryResultado.Parameters.ParamByName('nCdProduto').Value := qryFormulanCdProduto.Value ;
      objForm.qryResultado.Open ;

      showForm(objForm,true);
  end ;

end;

procedure TfrmMRP_GeraPedido.ToolButton5Click(Sender: TObject);
var
  objForm : TfrmMRP_UltimosPedidos;
begin
  inherited;

  if (nCdProduto > 0) then
  begin
      objForm := TfrmMRP_UltimosPedidos.Create(nil);

      objForm.qryResultado.Close ;
      objForm.qryResultado.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      objForm.qryResultado.Parameters.ParamByName('nCdProduto').Value := nCdProduto ;
      objForm.qryResultado.Open ;

      showForm(objForm,true);
  end ;

end;

procedure TfrmMRP_GeraPedido.MaskEdit6Enter(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryLocalEstoque.Parameters.ParamByName('nCdProduto').Value := nCdProduto             ;
  
end;

procedure TfrmMRP_GeraPedido.MaskEdit6Exit(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryLocalEstoque, MaskEdit6.Text) ;

end;

procedure TfrmMRP_GeraPedido.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
    nCdGrupoProduto : integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('SELECT nCdGrupo_Produto FROM Produto WHERE nCdProduto = ' + IntToStr(nCdProduto)) ;
        qryAux.Open ;

        if qryAux.Eof then
        begin
            nCdGrupoProduto := 0 ;
            MensagemAlerta('Grupo de Produtos n�o definido para este produto.') ;
        end
        else nCdGrupoProduto := qryAux.FieldList[0].Value ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(87,' LocalEstoque.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva) + ' and EXISTS(SELECT 1 FROM GrupoProdutoLocalEstoque GPLE WHERE GPLE.nCdLocalEstoque = LocalEstoque.nCdLocalEstoque AND GPLE.nCdGrupoProduto = ' + IntToStr(nCdGrupoProduto) + ')');

        If (nPK > 0) then
        begin
            MaskEdit6.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLocalEstoque, MaskEdit6.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmMRP_GeraPedido.qryFormulaCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryFormulacNmLocalEstoque.Value = '') and (qryFormulanCdLocalEstoqueEntrega.Value > 0) then
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT cNmLocalEstoque                                                  ') ;
      qryAux.SQL.Add('  FROM LocalEstoque                                                     ') ;
      qryaux.SQL.Add(' WHERE nCdLocalEstoque = ' + qryFormulanCdLocalEstoqueEntrega.AsString   ) ;
      qryaux.SQL.Add('   AND EXISTS(SELECT 1                                                  ') ;
      qryAux.SQL.Add('                FROM GrupoProdutoLocalEstoque GPLE                      ') ;
      qryAux.SQL.Add('               WHERE GPLE.nCdLocalEstoque = LocalEstoque.nCdLocalEstoque') ;
      qryAux.SQL.Add('                 AND GPLE.nCdGrupoProduto = (SELECT nCdGrupo_Produto    ') ;
      qryAux.SQL.Add('                                               FROM Produto             ') ;
      qryAux.SQL.Add('                                              WHERE Produto.nCdProduto = ' + qryFormulanCdProduto.asString + '))') ;
      qryAux.Open ;

      if not qryAux.eof then
          qryFormulacNmLocalEstoque.Value := qryAux.FieldList[0].Value ;

  end ;

  if (qryFormulanCdLocalEstoqueEntrega.Value = 0) then
      qryFormulacNmLocalEstoque.Value := '' ;

end;

procedure TfrmMRP_GeraPedido.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK             : Integer ;
    nCdGrupoProduto : integer ;
begin
  inherited;

  case key of
     vk_f4 : begin

         if (DBGridEh2.Col = 4) then
         begin

             if (qryFormula.State = dsBrowse) then
                 qryFormula.Edit ;

             if (qryFormula.State = dsInsert) or (qryFormula.State = dsEdit) then
             begin

                 qryAux.Close ;
                 qryAux.SQL.Clear ;
                 qryAux.SQL.Add('SELECT nCdGrupo_Produto FROM Produto WHERE nCdProduto = ' + IntToStr(nCdProduto)) ;
                 qryAux.Open ;

                 if qryAux.Eof then
                 begin
                     nCdGrupoProduto := 0 ;
                     MensagemAlerta('Grupo de Produtos n�o definido para este produto.') ;
                 end
                 else nCdGrupoProduto := qryAux.FieldList[0].Value ;

                 nPK := frmLookup_Padrao.ExecutaConsulta2(87,' LocalEstoque.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva) + ' and EXISTS(SELECT 1 FROM GrupoProdutoLocalEstoque GPLE WHERE GPLE.nCdLocalEstoque = LocalEstoque.nCdLocalEstoque AND GPLE.nCdGrupoProduto = ' + IntToStr(nCdGrupoProduto) + ')');

                 If (nPK > 0) then
                 begin
                     qryFormulanCdLocalEstoqueEntrega.Value := nPK ;
                 end ;

             end ;

         end ;

     end ;

  end ;

end;

end.
