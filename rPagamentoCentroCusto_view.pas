unit rPagamentoCentroCusto_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, QRCtrls, jpeg, QuickRpt, ExtCtrls;

type
  TrptPagamentoCentroCusto_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape1: TQRShape;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape3: TQRShape;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel25: TQRLabel;
    QRBand5: TQRBand;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr6: TQRExpr;
    QRBand3: TQRBand;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText14: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr4: TQRExpr;
    QRShape6: TQRShape;
    uspRelatorio: TADOStoredProc;
    QRLabel10: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText4: TQRDBText;
    QRGroup2: TQRGroup;
    QRBand6: TQRBand;
    QRLabel12: TQRLabel;
    QRExpr1: TQRExpr;
    QRLabel15: TQRLabel;
    QRDBText9: TQRDBText;
    qryCentroCusto: TADOQuery;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocNmCC: TStringField;
    qryCentroCustonCdCC1: TIntegerField;
    qryCentroCustonCdCC2: TIntegerField;
    qryCentroCustocCdCC: TStringField;
    qryCentroCustocCdHie: TStringField;
    qryCentroCustoiNivel: TSmallintField;
    qryCentroCustocFlgLanc: TIntegerField;
    qryCentroCustonCdStatus: TIntegerField;
    qryCentroCustonCdServidorOrigem: TIntegerField;
    qryCentroCustodDtReplicacao: TDateTimeField;
    QRDBText6: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText12: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText2: TQRDBText;
    uspRelatorionValMov: TBCDField;
    uspRelatorionCdTitulo: TIntegerField;
    uspRelatorionCdContaBancaria: TIntegerField;
    uspRelatorionCdFormaPagto: TIntegerField;
    uspRelatoriocNmFormaPagto: TStringField;
    uspRelatorioiNrCheque: TIntegerField;
    uspRelatorioContaBancaria: TStringField;
    uspRelatorionCdCC: TIntegerField;
    uspRelatorionCdCC1: TIntegerField;
    uspRelatorionCdCC2: TIntegerField;
    uspRelatoriocCdCC: TStringField;
    uspRelatoriocNmCC: TStringField;
    uspRelatorionPercent: TBCDField;
    uspRelatoriovalorProporcional: TBCDField;
    uspRelatorionValor: TBCDField;
    uspRelatorionCdSP: TIntegerField;
    uspRelatoriocNrTit: TStringField;
    uspRelatorionCdTerceiro: TIntegerField;
    uspRelatoriocNmTerceiro: TStringField;
    uspRelatoriodDtLiq: TDateTimeField;
    uspRelatorionValTit: TBCDField;
    uspRelatorioiParcela: TIntegerField;
    uspRelatoriodDtMov: TDateTimeField;
    QRLabel19: TQRLabel;
    QRLabel18: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel20: TQRLabel;
    uspRelatorionCdLojaTit: TIntegerField;
    QRLabel21: TQRLabel;
    QRDBText15: TQRDBText;
    uspRelatorionCdEmpresa: TIntegerField;
    QRLabel6: TQRLabel;
    QRLabel22: TQRLabel;
    uspRelatoriocNrNf: TStringField;
    QRLabel23: TQRLabel;
    QRDBText16: TQRDBText;
    QRShape2: TQRShape;
    QRDBText17: TQRDBText;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel14: TQRLabel;
    procedure QRGroup1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPagamentoCentroCusto_View: TrptPagamentoCentroCusto_View;

implementation

{$R *.dfm}

procedure TrptPagamentoCentroCusto_View.QRGroup1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  qryCentroCusto.Close;
  qryCentroCusto.Parameters.ParamByName('nCdCC2').Value := uspRelatorionCdCC2.Value;
  qryCentroCusto.Open;
end;

end.
