unit fPedidoAbertoProduto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, Menus, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid, cxContainer, cxTextEdit, StdCtrls;

type
  TfrmPedidoAbertoProduto = class(TfrmProcesso_Padrao)
    qryMaster: TADOQuery;
    qryMasternCdPedido: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtAutor: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMastercNmTerceiro: TStringField;
    qryMasternSaldo: TBCDField;
    qryMastercNrPedTerceiro: TStringField;
    dsMaster: TDataSource;
    qryMasternQtdePrev: TBCDField;
    qryMasternCdProduto: TIntegerField;
    PopupMenu1: TPopupMenu;
    CancelarSaldodoPedido1: TMenuItem;
    qryAux: TADOQuery;
    qryMasternCdItemPedido: TAutoIncField;
    qryMastercSiglaUnidadeMedida: TStringField;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1nCdPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPedido: TcxGridDBColumn;
    cxGrid1DBTableView1dDtAutor: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPrevEntIni: TcxGridDBColumn;
    cxGrid1DBTableView1dDtPrevEntFim: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldo: TcxGridDBColumn;
    cxGrid1DBTableView1cNrPedTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdePrev: TcxGridDBColumn;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1nCdItemPedido: TcxGridDBColumn;
    cxGrid1DBTableView1cSiglaUnidadeMedida: TcxGridDBColumn;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    cxTextEdit3: TcxTextEdit;
    procedure ToolButton2Click(Sender: TObject);
    procedure CancelarSaldodoPedido1Click(Sender: TObject);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPedidoAbertoProduto: TfrmPedidoAbertoProduto;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmPedidoAbertoProduto.ToolButton2Click(Sender: TObject);
begin
  inherited;

  Close ;
end;

procedure TfrmPedidoAbertoProduto.CancelarSaldodoPedido1Click(
  Sender: TObject);
begin
  inherited;

  case MessageDlg('Confirma o cancelamento do saldo deste item?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      qryAux.Close ;
      qryAux.SQL.Text := ('UPDATE ItemPedido Set nQtdeCanc = nQtdeCanc + ' + qryMasternSaldo.AsString + ', nQtdePrev = 0 WHERE nCdItemPedido = ' + qryMasternCdItemPedido.AsString) ;
      qryAux.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      exit ;
  end ;

  frmMenu.Connection.CommitTrans;

  qryMaster.Close ;
  qryMaster.Open ;

  if (qryMaster.Eof) then
      MensagemAlerta('Nenhum pedido encontrado.') ;

end;

procedure TfrmPedidoAbertoProduto.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if (DateToStr(ARecord.Values[4]) <> '') then
      if (ARecord.Values[4] < Date) then
          AStyle := frmMenu.LinhaVermelha;

end;

end.
