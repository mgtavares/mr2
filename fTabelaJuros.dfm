inherited frmTabelaJuros: TfrmTabelaJuros
  Left = -8
  Top = -8
  Width = 1152
  Height = 864
  Caption = 'Tabela de Juros'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1136
    Height = 799
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1136
    Height = 799
    Align = alClient
    DataSource = dsMaster
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdEspTit'
        Footers = <>
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'cNmEspTit'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'dDtValidade'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'iDiasCarencia'
        Footers = <>
        Width = 88
      end
      item
        EditButtons = <>
        FieldName = 'nPercMulta'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nPercJuroDia'
        Footers = <>
        Tag = 2
        Width = 91
      end
      item
        EditButtons = <>
        FieldName = 'nPercDescAntecipDia'
        Footers = <>
        Tag = 2
      end
      item
        EditButtons = <>
        FieldName = 'iDiasMinDescAntecip'
        Footers = <>
        Width = 83
      end
      item
        EditButtons = <>
        FieldName = 'nPercMaxDescAntecip'
        Footers = <>
        Width = 96
      end>
  end
  inherited ImageList1: TImageList
    Left = 392
    Top = 168
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryMasterBeforePost
    OnCalcFields = qryMasterCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TabelaJuros'
      'WHERE nCdEmpresa = :nPK'
      'ORDER BY dDtValidade DESC')
    Left = 272
    Top = 160
    object qryMasternCdTabelaJuros: TAutoIncField
      DisplayLabel = 'Tabela de Juros|'
      FieldName = 'nCdTabelaJuros'
    end
    object qryMasternCdEmpresa: TIntegerField
      DisplayLabel = 'Tabela de Juros|'
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdEspTit: TIntegerField
      DisplayLabel = 'Esp'#233'cie de T'#237'tulo|C'#243'd'
      FieldName = 'nCdEspTit'
    end
    object qryMastercNmEspTit: TStringField
      DisplayLabel = 'Esp'#233'cie de T'#237'tulo|Esp'#233'cie de T'#237'tulo'
      FieldKind = fkCalculated
      FieldName = 'cNmEspTit'
      Size = 50
      Calculated = True
    end
    object qryMasterdDtValidade: TDateTimeField
      DisplayLabel = 'Tabela de Juros|Data Limite Vig'#234'ncia'
      FieldName = 'dDtValidade'
    end
    object qryMasteriDiasCarencia: TIntegerField
      DisplayLabel = 'Tabela de Juros|Dias Car'#234'ncia'
      FieldName = 'iDiasCarencia'
    end
    object qryMasternPercMulta: TBCDField
      DisplayLabel = 'Tabela de Juros|% Multa'
      FieldName = 'nPercMulta'
      Precision = 12
      Size = 2
    end
    object qryMasternPercJuroDia: TBCDField
      DisplayLabel = 'Tabela de Juros|% Juros (dia)'
      FieldName = 'nPercJuroDia'
      DisplayFormat = '#,##0.0000'
      Precision = 12
    end
    object qryMasternPercDescAntecipDia: TBCDField
      DisplayLabel = 'Desconto Antecipa'#231#227'o|% Dia'
      FieldName = 'nPercDescAntecipDia'
      DisplayFormat = '#,##0.0000'
      Precision = 12
    end
    object qryMasteriDiasMinDescAntecip: TIntegerField
      DisplayLabel = 'Desconto Antecipa'#231#227'o|Dias M'#237'nimo'
      FieldName = 'iDiasMinDescAntecip'
    end
    object qryMasternPercMaxDescAntecip: TBCDField
      DisplayLabel = 'Desconto Antecipa'#231#227'o|% Descto M'#225'x.'
      FieldName = 'nPercMaxDescAntecip'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
  end
  object dsMaster: TDataSource
    DataSet = qryMaster
    Left = 312
    Top = 160
  end
  object qryEspTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEspTit, cNmEspTit'
      'FROM EspTit'
      'WHERE nCdEspTit = :nPK'
      'AND cTipoMov = '#39'R'#39)
    Left = 408
    Top = 232
    object qryEspTitnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
  end
end
