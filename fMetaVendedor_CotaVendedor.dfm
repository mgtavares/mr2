inherited frmMetaVendedor_CotaVendedor: TfrmMetaVendedor_CotaVendedor
  Left = 135
  Top = 177
  Width = 1021
  Height = 552
  Caption = 'Distribui'#231#227'o de Cota Vendedor'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1005
    Height = 487
  end
  inherited ToolBar1: TToolBar
    Width = 1005
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1005
    Height = 487
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 483
    ClientRectLeft = 4
    ClientRectRight = 1001
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Cota Vendedor'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 997
        Height = 459
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsCotaVendedores
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        RowDetailPanel.Color = clBtnFace
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyDown = DBGridEh1KeyDown
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdMetaVendedor'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdMetaSemana'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdTerceiroColab'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWindow
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'iTotalHorasPrev'
            Footers = <
              item
                FieldName = 'iTotalHorasPrev'
                ValueType = fvtSum
              end>
          end
          item
            EditButtons = <>
            FieldName = 'nPercHoraCalc'
            Footers = <
              item
              end>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nValMetaMinima'
            Footers = <
              item
              end>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nValMeta'
            Footers = <
              item
              end>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'iTotalHorasReal'
            Footers = <
              item
              end>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'nPercHoraLojaCalc'
            Footers = <
              item
              end>
            ReadOnly = True
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 424
    Top = 128
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F6F4F400F9F7F700FEF9FA00FFFE
      FF00E7E1E2000D070800050000000A0405000A04050005090E0000000E000017
      2B0000000F0000051300000C1700000006000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FEFCFC0000000000FEF9FA00F5F0
      F100FFFEFF00130D0E00B2ACAD00B2ACAD00C3BBBC00A4A8B3000F2B4A003D64
      8B00416387005D7A99004C647C00000114000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      840000008400000084000000000000000000FEFCFC00F0EEEE00FFFBFC00F4EF
      F000F9F3F400150F1000E3DBDC00A29A9B00ACA4A500A6ABBA00334F7E00234B
      86001A3E74001634630010295100000322000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF00000084000000000000000000E4DFE000ABA7A600817C7B009691
      90008C84840007000000FFFEFE00FDF4F100F1E8E500D8D8E80040558C00A0BF
      FF00BCD8FF00C0D9FF0055699800000021000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000007000000120B0800090000000900
      0000251B14000C000000EFE1DB00B2A59D00B4A79F00A39CA900383E73003543
      8B00354385002D3973001420500000001D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF00000084000000000000000000201818009F969300CABFBB00C2B8
      B100ACA199001C0F0700FFFDF400FAEBE200FFFBEF00EADFE70056548200D4D9
      FF00CED2FF00D1D8FF0059618900000321000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000160E0E00F7EEEB00AEA39F00ADA3
      9C00B6ABA3000C010000F1E5DB00ACA09600A8999000AA9FA200555272004043
      6F003A3F6600595E7F005358710000000E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000007000000F9F2EF00EEE5E100F7EC
      E800F2E6E00013080000FFFFF700FCF0E600FFF5EB00F9EFEF009F9EAE00E8EA
      FF00EEF2FF00D0D5E400A7ABB60012161B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000009040300F8F1EE00B1A8A400A198
      9400B4AAA3001A110800FFFFF700E5DAD200E2D7CF00F1E9E200B7B5B500D0D2
      D300F3F5F500EDF0EE00B2B3AF00040500000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF000000840000000000000000000D080700F1ECEB00EAE3E000F4EB
      E700DDD4D000291F1800ECE2DB00FFFFF800FFFFF800EEE7DE00E0DED400F8F7
      ED00F8F8EC00F8F8EC00FFFFF500040400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000004000000EFEAE9009F9A9700BDB7
      B200B4AEA9000B0200001A110D00090100000D030000140D0400070300001C1B
      0D00050500000B0B0000060600001C1C0C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF0000008400000000000000000005010000FFFFFE00EAE5E200EBE6
      E300E6E1DE00ACA6A100F2ECE700F6F0EB00F3EDE800C0BAB300120D04007371
      6600FFFFF700FEFDEF00F9F9EB00FAFAEC000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF00000000000000000002000000FFFFFE00F4F0EF00E6E2
      E100ECE8E700B5B0AD00E8E3E000E1DCD900F0EBE800BCB7B40007040000928F
      8B00FFFFFC00F9F8F400F5F5EF00FFFFFB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000001C1A1900F4F2F100FFFEFD00FEFA
      F900FFFFFE00DEDAD900FFFDFC00FEFBF700FFFFFE00EBE6E700020003007F7E
      8200EDECF000FDFFFF00FDFFFF00FDFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000606060000000000000000000503
      03000E0C0C00020000000705050005030200110F0F00050204001B1A2400EDED
      F900F7F7FF00E0E1EB00FBFDFF00F7FAFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFF00000000
      FE00C00340000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000018001000000000003800100000000
      0077C00300000000007FFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object qryCotaVendedores: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryCotaVendedoresBeforePost
    AfterPost = qryCotaVendedoresAfterPost
    BeforeDelete = qryCotaVendedoresBeforeDelete
    OnCalcFields = qryCotaVendedoresCalcFields
    Parameters = <
      item
        Name = 'nCdMetaSemana'
        DataType = ftString
        Size = 1
        Value = '1'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '2'
      end>
    SQL.Strings = (
      'DECLARE @nCdUsuario      int'
      '       ,@nCdMetaSemana   int'
      '       ,@nCdMetaVendedor int'
      '       ,@PercHorasPrev   decimal(12,2)'
      ' '
      'SET @nCdMetaSemana = :nCdMetaSemana'
      ''
      'IF NOT EXISTS(SELECT 1'
      '                FROM MetaVendedor'
      '               WHERE nCdMetaSemana = @nCdMetaSemana)'
      'BEGIN'
      ''
      '    DECLARE curAux CURSOR'
      '        FOR SELECT nCdUsuario'
      '              FROM Usuario'
      '             WHERE EXISTS(SELECT 1'
      '                            FROM UsuarioLoja UL'
      
        '                           WHERE UL.nCdUsuario = Usuario.nCdUsua' +
        'rio'
      '                             AND UL.nCdLoja    = :nCdLoja)'
      '               AND cFlgVendedor            = 1'
      '               AND nCdStatus               = 1'
      '               AND nCdTerceiroResponsavel IS NOT NULL'
      ''
      '    OPEN curAux'
      ''
      '    FETCH NEXT'
      '     FROM curAux'
      '     INTO @nCdUsuario'
      ''
      '    WHILE (@@FETCH_STATUS = 0)'
      '    BEGIN'
      ''
      
        '        IF ((SELECT 1 FROM MetaVendedor WHERE nCdUsuario = @nCdU' +
        'suario AND nCdMetaSemana = @nCdMetaSemana) IS NULL)'
      '        BEGIN'
      ''
      '            Set @nCdMetaVendedor = NULL'
      ''
      #9#9'        EXEC usp_ProximoID '#39'METAVENDEDOR'#39
      #9#9'                          ,@nCdMetaVendedor OUTPUT'
      ''
      
        '            IF((SELECT iTotalHorasPrev FROM MetaSemana WHERE nCd' +
        'MetaSemana = @nCdMetaSemana) > 0)'
      '            BEGIN'
      
        '                SET @PercHorasPrev = isNull((SELECT (Convert(dec' +
        'imal(12,2),dbo.fn_LeParametro('#39'HRSUGRPVVDMETA'#39'))/MetaSemana.iTot' +
        'alHorasPrev ) * 100'
      '                                               FROM MetaSemana'
      
        '                                              WHERE nCdMetaSeman' +
        'a = @nCdMetaSemana),0)'
      '            END'
      ''
      '            SET @PercHorasPrev = isNull(@PercHorasPrev,0)'
      ''
      '            INSERT INTO MetaVendedor(nCdMetaVendedor'
      '                                    ,nCdMetaSemana'
      '                                    ,nCdUsuario'
      '                                    ,nCdTerceiroColab'
      '                                    ,iTotalHorasPrev'
      '                                    ,nPercHoralojaPrev'
      '                                    ,cFlgAutomatico)'
      '                              SELECT @nCdMetaVendedor'
      '                                    ,@nCdMetaSemana'
      '                                    ,nCdUsuario'
      '                                    ,nCdTerceiroResponsavel'
      
        '                                    ,isNull(Convert(decimal(12,2' +
        '),dbo.fn_LeParametro('#39'HRSUGRPVVDMETA'#39')),0)'
      '                                    ,@PercHorasPrev'
      '                                    ,1'
      '                                FROM Usuario'
      '                               WHERE nCdUsuario   = @nCdUsuario'
      '                                 AND cFlgVendedor = 1'
      '                                 AND nCdStatus    = 1'
      ''
      '        END'
      ''
      '        FETCH NEXT'
      '         FROM curAux'
      '         INTO @nCdUsuario'
      ''
      '    END'
      ''
      '    CLOSE curAux'
      '    DEALLOCATE curAux'
      ''
      'END'
      ''
      'SELECT nCdMetaVendedor'
      '      ,nCdMetaSemana'
      '      ,nCdTerceiroColab'
      '      ,nCdUsuario'
      '      ,iTotalHorasPrev'
      '      ,nPercHoralojaPrev'
      '      ,iTotalHorasReal'
      '      ,nPercHoraLojaReal'
      '      ,cFlgAutomatico'
      '  FROM MetaVendedor'
      ' WHERE nCdMetaSemana = @nCdMetaSemana'
      ' ORDER BY nCdUsuario'
      '')
    Left = 332
    Top = 189
    object qryCotaVendedoresnCdMetaVendedor: TIntegerField
      FieldName = 'nCdMetaVendedor'
    end
    object qryCotaVendedoresnCdMetaSemana: TIntegerField
      FieldName = 'nCdMetaSemana'
    end
    object qryCotaVendedoresnCdTerceiroColab: TIntegerField
      FieldName = 'nCdTerceiroColab'
    end
    object qryCotaVendedoresnCdUsuario: TIntegerField
      DisplayLabel = 'Vendedor|C'#243'd.'
      FieldName = 'nCdUsuario'
    end
    object qryCotaVendedorescNmUsuario: TStringField
      DisplayLabel = 'Vendedor|Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 50
      Calculated = True
    end
    object qryCotaVendedoresnPercHoraCalc: TBCDField
      DisplayLabel = 'Horas Previstas|% Prev.'
      FieldKind = fkCalculated
      FieldName = 'nPercHoraCalc'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
      Calculated = True
    end
    object qryCotaVendedoresnValMetaMinima: TFloatField
      DisplayLabel = 'Meta Prevista|Meta Minima'
      FieldKind = fkCalculated
      FieldName = 'nValMetaMinima'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object qryCotaVendedoresnValMeta: TFloatField
      DisplayLabel = 'Meta Prevista|Meta'
      FieldKind = fkCalculated
      FieldName = 'nValMeta'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object qryCotaVendedoresnPercHoraLojaReal: TBCDField
      DisplayLabel = 'Horas Realizadas|% Loja'
      FieldName = 'nPercHoraLojaReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCotaVendedoresnPercHoralojaPrev: TBCDField
      FieldName = 'nPercHoralojaPrev'
      Visible = False
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCotaVendedoresnPercHoraLojaCalc: TFloatField
      DisplayLabel = 'Horas Realizadas|% Vendedor'
      FieldKind = fkCalculated
      FieldName = 'nPercHoraLojaCalc'
      DisplayFormat = '#,##0.00'
      Calculated = True
    end
    object qryCotaVendedoresiTotalHorasPrev: TBCDField
      DisplayLabel = 'Horas Previstas|Horas Venda'
      FieldName = 'iTotalHorasPrev'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCotaVendedoresiTotalHorasReal: TBCDField
      DisplayLabel = 'Horas Realizadas|Horas Venda'
      FieldName = 'iTotalHorasReal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCotaVendedorescFlgAutomatico: TIntegerField
      FieldName = 'cFlgAutomatico'
    end
  end
  object qryVendedor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '8'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '1'
      end>
    SQL.Strings = (
      'SELECT nCdUsuario'
      '      ,cNmUsuario'
      '      ,nCdTerceiroResponsavel'
      '  FROM Usuario '
      ' WHERE nCdUsuario = :nPK'
      '   AND EXISTS(SELECT 1 '
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdUsuario = nCdUsuario'
      '                 AND UL.nCdLoja    = :nCdLoja)')
    Left = 364
    Top = 189
    object qryVendedornCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryVendedorcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryVendedornCdTerceiroResponsavel: TIntegerField
      FieldName = 'nCdTerceiroResponsavel'
    end
  end
  object dsVendedor: TDataSource
    DataSet = qryVendedor
    Left = 380
    Top = 221
  end
  object dsCotaVendedores: TDataSource
    DataSet = qryCotaVendedores
    Left = 340
    Top = 221
  end
  object qryVerificaDepApontamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMetaVendedor'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT TOP 1 1'
      '  FROM MetaVendedorDia'
      ' WHERE nCdMetaVendedor = :nCdMetaVendedor')
    Left = 428
    Top = 189
  end
  object qryAtualizaHorasPrev: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdMetaSemana'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'iTotalHorasPrev'
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Size = 19
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdMetaSemana int'
      '       ,@nCdMetaMes    int'
      ''
      'SET @nCdMetaSemana = :nCdMetaSemana'
      ''
      'SELECT @nCdMetaMes = nCdMetaMes'
      '  FROM MetaSemana'
      ' WHERE nCdMetaSemana = @nCdMetaSemana'
      ''
      'UPDATE MetaSemana'
      '   SET iTotalHorasPrev = :iTotalHorasPrev'
      ' WHERE nCdMetaSemana   = @nCdMetaSemana'
      ''
      'UPDATE MetaMes'
      '   SET iTotalHorasPrev = (SELECT isNull(SUM(iTotalHorasPrev),0)'
      '                            FROM MetaSemana'
      '                           WHERE nCdMetaMes = @nCdMetaMes)'
      ' WHERE nCdMetaMes = @nCdMetaMes'
      ''
      '')
    Left = 468
    Top = 189
  end
end
