unit fBuscaBorderoData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, StdCtrls, cxButtons, cxControls,
  cxContainer, cxEdit, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  DB, ADODB, jpeg, ExtCtrls;

type
  TfrmBuscaBorderoData = class(TForm)
    cxDateEdit1: TcxDateEdit;
    qryBuscaResumo: TADOQuery;
    qryBuscaResumonCdResumoCaixa: TIntegerField;
    Label2: TLabel;
    btVisualizar: TcxButton;
    btCancelar: TcxButton;
    Image1: TImage;
    procedure FormShow(Sender: TObject);
    procedure btVisualizarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdResumoCaixa : integer;
    nCdContaBancaria : integer;
  end;

var
  frmBuscaBorderoData: TfrmBuscaBorderoData;

implementation
uses
    fMenu;

{$R *.dfm}

procedure TfrmBuscaBorderoData.FormShow(Sender: TObject);
begin
    cxDateEdit1.Date := Date;
    nCdResumoCaixa := 0;
end;

procedure TfrmBuscaBorderoData.btVisualizarClick(Sender: TObject);
begin
    qryBuscaResumo.Close;
    qryBuscaResumo.Parameters.ParamByName('dDtResumo').Value        := cxDateEdit1.Text;
    qryBuscaResumo.Parameters.ParamByName('nCdContaBancaria').Value := nCdContaBancaria;
    qryBuscaResumo.Open;

    if (qryBuscaResumo.IsEmpty) then
    begin
        frmMenu.MensagemAlerta('Nenhum resumo de movimentação localizado para a data.');
        abort;
    end;

    nCdResumoCaixa := qryBuscaResumonCdResumoCaixa.Value;

    Close;
end;

procedure TfrmBuscaBorderoData.btCancelarClick(Sender: TObject);
begin
  Close;
end;

end.
