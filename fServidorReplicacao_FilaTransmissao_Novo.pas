unit fServidorReplicacao_FilaTransmissao_Novo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, cxPC,
  cxControls, DB, ADODB, GridsEh, DBGridEh, Menus, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmServidorReplicacao_FilaTransmissao_Novo = class(TfrmProcesso_Padrao)
    qryTempRegistro: TADOQuery;
    dsTempRegistro: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    qryTempRegistronCdTempRegistro: TAutoIncField;
    qryTempRegistrocTabela: TStringField;
    qryTempRegistroiIDRegistro: TIntegerField;
    qryTempRegistrocFlgAcao: TStringField;
    qryTempRegistrocNmServidor: TStringField;
    PopupMenu1: TPopupMenu;
    Atualizar1: TMenuItem;
    qryTempRegistrodDtInclusao: TDateTimeField;
    cxTabSheet3: TcxTabSheet;
    PopupMenu3: TPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    qryDescartados: TADOQuery;
    dsDescartados: TDataSource;
    Retransmitir1: TMenuItem;
    qryAux: TADOQuery;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdTempRegistro: TcxGridDBColumn;
    cxGridDBTableView1cTabela: TcxGridDBColumn;
    cxGridDBTableView1iIDRegistro: TcxGridDBColumn;
    cxGridDBTableView1cFlgAcao: TcxGridDBColumn;
    cxGridDBTableView1cNmServidor: TcxGridDBColumn;
    cxGridDBTableView1dDtInclusao: TcxGridDBColumn;
    cxGrid1: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView2nCdTempRegistro: TcxGridDBColumn;
    cxGridDBTableView2cTabela: TcxGridDBColumn;
    cxGridDBTableView2iIDRegistro: TcxGridDBColumn;
    cxGridDBTableView2cFlgAcao: TcxGridDBColumn;
    cxGridDBTableView2cNmServidor: TcxGridDBColumn;
    cxGridDBTableView2dDtUltTentativa: TcxGridDBColumn;
    cxGridDBTableView2iQtdeTentativas: TcxGridDBColumn;
    cxGridDBTableView2cMsgErro: TcxGridDBColumn;
    cxGridDBTableView2dDtInclusao: TcxGridDBColumn;
    cxTabSheet4: TcxTabSheet;
    cxGrid3: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    qryPendentesServidor: TADOQuery;
    dsPendentesServidor: TDataSource;
    cxGridDBTableView3nCdServidor: TcxGridDBColumn;
    cxGridDBTableView3cNmServidor: TcxGridDBColumn;
    cxGridDBTableView3iQtdePendente: TcxGridDBColumn;
    PopupMenu4: TPopupMenu;
    MenuItem4: TMenuItem;
    cxTabSheet5: TcxTabSheet;
    qryPendentesTabela: TADOQuery;
    dsPendentesTabela: TDataSource;
    qryPendentesTabelanCdTabelaSistema: TIntegerField;
    qryPendentesTabelacTabela: TStringField;
    qryPendentesTabelacNmTabela: TStringField;
    qryPendentesTabelaiQtdePendente: TIntegerField;
    cxGrid4: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridLevel4: TcxGridLevel;
    cxGridDBTableView4nCdTabelaSistema: TcxGridDBColumn;
    cxGridDBTableView4cTabela: TcxGridDBColumn;
    cxGridDBTableView4cNmTabela: TcxGridDBColumn;
    cxGridDBTableView4iQtdePendente: TcxGridDBColumn;
    PopupMenu5: TPopupMenu;
    MenuItem5: TMenuItem;
    qryPendentesServidornCdServidorDestino: TIntegerField;
    qryPendentesServidorcNmServidor: TStringField;
    qryPendentesServidoriQtdePendenteRetorno: TIntegerField;
    qryPendentesServidoriQtdePendentePacote: TIntegerField;
    cxGridDBTableView3DBColumn1: TcxGridDBColumn;
    qryDescartadosnCdTempRegistro: TIntegerField;
    qryDescartadoscTabela: TStringField;
    qryDescartadosiIDRegistro: TIntegerField;
    qryDescartadoscFlgAcao: TStringField;
    qryDescartadoscNmServidor: TStringField;
    qryDescartadosnCdItemPacoteReplicacao: TAutoIncField;
    qryDescartadosnCdPacoteReplicacao: TIntegerField;
    qryDescartadosnCdTempRegistro_1: TIntegerField;
    qryDescartadosdDtReplicacao: TDateTimeField;
    qryDescartadosdDtUltTentativa: TDateTimeField;
    qryDescartadosiQtdeTentativas: TIntegerField;
    qryDescartadoscFlgIntegrado: TIntegerField;
    qryDescartadoscFlgErro: TIntegerField;
    qryDescartadoscFlgBroken: TIntegerField;
    qryDescartadoscFlgObsoleto: TIntegerField;
    qryDescartadoscFlgNaoEncontrado: TIntegerField;
    qryDescartadoscMsgErro: TMemoField;
    procedure FormShow(Sender: TObject);
    procedure Atualizar1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure Retransmitir1Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmServidorReplicacao_FilaTransmissao_Novo: TfrmServidorReplicacao_FilaTransmissao_Novo;

implementation

{$R *.dfm}

procedure TfrmServidorReplicacao_FilaTransmissao_Novo.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.Align           := alClient ;
  cxPageControl1.ActivePageIndex := 0 ;
  
end;

procedure TfrmServidorReplicacao_FilaTransmissao_Novo.Atualizar1Click(
  Sender: TObject);
begin
  inherited;

  qryTempRegistro.Close;
  qryTempRegistro.Open ;
  
end;

procedure TfrmServidorReplicacao_FilaTransmissao_Novo.MenuItem2Click(
  Sender: TObject);
begin
  inherited;

  qryDescartados.Close;
  qryDescartados.Open ;
  
end;

procedure TfrmServidorReplicacao_FilaTransmissao_Novo.MenuItem3Click(
  Sender: TObject);
begin
  inherited;

  if not qryDescartados.eof then
      ShowMessage(qryDescartadoscMsgErro.AsString) ;

end;

procedure TfrmServidorReplicacao_FilaTransmissao_Novo.Retransmitir1Click(
  Sender: TObject);
begin
  inherited;

  if not qryDescartados.Eof then
  begin

      if (MessageDlg('Confirma a retransmissão deste registro ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Append(' DELETE FROM ItemPacoteReplicacao ');
      qryAux.SQL.Append(' WHERE nCdTempRegistro = ' + qryDescartadosnCdTempRegistro.AsString);
      qryAux.SQL.Append(' DELETE FROM PacoteTempRegistro ');
      qryAux.SQL.Append(' WHERE nCdTempRegistro = ' + qryDescartadosnCdTempRegistro.AsString);
      qryAux.ExecSQL;

      qryDescartados.Requery();

  end ;

end;

procedure TfrmServidorReplicacao_FilaTransmissao_Novo.MenuItem4Click(
  Sender: TObject);
begin
  inherited;

  qryPendentesServidor.Close;
  qryPendentesServidor.Open ;

end;

procedure TfrmServidorReplicacao_FilaTransmissao_Novo.MenuItem5Click(
  Sender: TObject);
begin
  inherited;

  qryPendentesTabela.Close;
  qryPendentesTabela.Open;

end;

end.
