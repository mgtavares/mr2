unit fItemPedidoAtendido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  GridsEh, DBGridEh, ADODB, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmItemPedidoAtendido = class(TfrmProcesso_Padrao)
    qryItem: TADOQuery;
    qryItemiNrDocto: TIntegerField;
    qryItemcSerie: TStringField;
    qryItemnCdRecebimento: TIntegerField;
    qryItemdDtAutor: TDateTimeField;
    qryItemdDtAtendimento: TDateTimeField;
    qryItemnQtdeAtendida: TBCDField;
    qryItemiDias: TIntegerField;
    DBGridEh1: TDBGridEh;
    dsItem: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmItemPedidoAtendido: TfrmItemPedidoAtendido;

implementation

{$R *.dfm}

end.
