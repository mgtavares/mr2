inherited frmModeloDocumento: TfrmModeloDocumento
  Left = 156
  Top = 41
  Width = 1133
  Height = 670
  Caption = 'Modelos de Documento'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1117
    Height = 607
  end
  object Label1: TLabel [1]
    Left = 47
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label5: TLabel [2]
    Left = 36
    Top = 70
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231'ao'
    FocusControl = DBEdit3
  end
  object Label2: TLabel [3]
    Left = 20
    Top = 94
    Width = 65
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Modelo'
    FocusControl = DBEdit2
  end
  object Label6: TLabel [4]
    Left = 53
    Top = 118
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit4
  end
  inherited ToolBar2: TToolBar
    Width = 1117
    TabOrder = 7
    inherited ToolButton9: TToolButton
      Visible = False
    end
    inherited ToolButton4: TToolButton
      Visible = False
    end
    inherited btnAuditoria: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [6]
    Tag = 1
    Left = 90
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdModeloDocumento'
    DataSource = dsMaster
    TabOrder = 0
  end
  object DBEdit3: TDBEdit [7]
    Left = 90
    Top = 64
    Width = 559
    Height = 19
    DataField = 'cNmModeloDocumento'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit4: TDBEdit [8]
    Left = 90
    Top = 112
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 4
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit2: TDBEdit [9]
    Left = 90
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoModeloDoc'
    DataSource = dsMaster
    TabOrder = 2
    OnExit = DBEdit2Exit
    OnKeyDown = DBEdit2KeyDown
  end
  object DBEdit5: TDBEdit [10]
    Tag = 1
    Left = 158
    Top = 88
    Width = 491
    Height = 19
    DataField = 'cNmTabTipoModeloDoc'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit6: TDBEdit [11]
    Tag = 1
    Left = 158
    Top = 112
    Width = 283
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsMaster
    TabOrder = 5
  end
  object cxPageControl1: TcxPageControl [12]
    Left = 16
    Top = 152
    Width = 1089
    Height = 473
    ActivePage = tabMemo
    LookAndFeel.NativeStyle = True
    TabOrder = 6
    ClientRectBottom = 469
    ClientRectLeft = 4
    ClientRectRight = 1085
    ClientRectTop = 24
    object tabMemo: TcxTabSheet
      Caption = 'Texto (TXT)'
      ImageIndex = 1
      object DBMemo1: TDBMemo
        Left = 0
        Top = 0
        Width = 864
        Height = 445
        Align = alClient
        DataField = 'cDescricaoModeloDoc'
        DataSource = dsMaster
        TabOrder = 0
      end
      object Panel1: TPanel
        Left = 864
        Top = 0
        Width = 217
        Height = 445
        Align = alRight
        Color = clWindow
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 8
          Top = 8
          Width = 201
          Height = 417
          Caption = ' Vari'#225'veis do Sistema '
          TabOrder = 0
          object cxTreeView: TTreeView
            Left = 9
            Top = 15
            Width = 185
            Height = 393
            Indent = 19
            ReadOnly = True
            TabOrder = 0
            OnDblClick = cxTreeViewDblClick
            Items.Data = {
              04000000200000000000000000000000FFFFFFFFFFFFFFFF0000000002000000
              07456D7072657361270000000000000001000000FFFFFFFFFFFFFFFF00000000
              000000000E2664657363726963616F456D7026200000000000000002000000FF
              FFFFFFFFFFFFFF000000000000000007267369676C61261D0000000000000000
              000000FFFFFFFFFFFFFFFF0000000001000000044C6F6A612800000000000000
              00000000FFFFFFFFFFFFFFFF00000000000000000F2664657363726963616F4C
              6F6A6126210000000000000000000000FFFFFFFFFFFFFFFF0000000008000000
              08546572636569726F250000000000000001000000FFFFFFFFFFFFFFFF000000
              00000000000C266E6F6D654F726967656D26260000000000000002000000FFFF
              FFFFFFFFFFFF00000000000000000D266E6F6D6544657374696E6F2626000000
              0000000003000000FFFFFFFFFFFFFFFF00000000000000000D26656D61696C4F
              726967656D26270000000000000004000000FFFFFFFFFFFFFFFF000000000000
              00000E26656D61696C44657374696E6F26230000000000000000000000FFFFFF
              FFFFFFFFFF00000000000000000A26656D61696C4E4665262800000000000000
              00000000FFFFFFFFFFFFFFFF00000000000000000F26656D61696C436F706961
              4E466526290000000000000005000000FFFFFFFFFFFFFFFF0000000000000000
              1026656E64657265636F4F726967656D262A0000000000000006000000FFFFFF
              FFFFFFFFFF00000000000000001126656E64657265636F44657374696E6F2624
              0000000000000000000000FFFFFFFFFFFFFFFF00000000070000000B446F6374
              6F46697363616C230000000000000000000000FFFFFFFFFFFFFFFF0000000000
              0000000A2663686176654E464526230000000000000000000000FFFFFFFFFFFF
              FFFF00000000000000000A266E756D446F63746F262600000000000000000000
              00FFFFFFFFFFFFFFFF00000000000000000D2664617461456D697373616F2620
              0000000000000000000000FFFFFFFFFFFFFFFF0000000000000000072676616C
              6F7226250000000000000000000000FFFFFFFFFFFFFFFF00000000000000000C
              26666F726D61506167746F26240000000000000000000000FFFFFFFFFFFFFFFF
              00000000000000000B26636F6E64506167746F26210000000000000000000000
              FFFFFFFFFFFFFFFF0000000000000000082673746174757326}
          end
        end
      end
    end
    object tabDescricao: TcxTabSheet
      Caption = 'Web (HTML)'
      ImageIndex = 2
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1081
        Height = 43
        Align = alTop
        Color = clWindow
        TabOrder = 0
        object btEditorHTML: TcxButton
          Left = 3
          Top = 4
          Width = 153
          Height = 33
          Caption = 'Editor HTML'
          TabOrder = 0
          OnClick = btEditorHTMLClick
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCD6E23C9651B
            C8601AC65918C25317C04E16BD4715BB4115B93E14B73913B63512B43312B433
            12B43312B43312B43312D27735E4AF87E3AB81E1A87BDFA376DEA171DC9D6DDB
            9968DA9763D9945ED7915BD78F57D58D54D58C53D58C53B43312D68443E7B590
            E0A374DE9E6EDC9A67DB9560D9915AD78D53D5894DD38548D28143D07E3ECF7B
            39CE7935D58D54B43312DB8E53EABB99FCF6F2E1A679FCF5F1DD9D6BFCF6F2DA
            945EF7E9DED68C51EFD1BADEA477FDFAF7F7E8DDD78F59B63512E19762ECC1A1
            FCF7F3E5AD84FCF6F2E1A477FCF7F3DD9B69F8EBE0F0D3BEF3DECEE0A97FFAF0
            E9D28344D9945EB73C13E2A06EEEC7A8FEFDFCFDF7F3FEFAF8E3AC81FCF7F4E0
            A374F9ECE3FAF2EBFDF8F4E3AE86FAF1EAD5894DDA9966BD4315E6A779F0CBB0
            FDF8F5EABA98FDF8F4E7B38CFDF8F5E3AA80F9EEE5EECDB4FDF8F5E5B38EFDF9
            F6D88F57DD9E6DC04E16EAAB80F2CFB5FCF4EEECBF9FFBF3EDFDF8F4FDF7F4FC
            F7F3F4DBC9E7B48EF7E6DAE3AD83F6E4D6DB9762DFA376C45918EAAB80F3D0B7
            EFC6A9EFC4A6EEC2A2ECBF9EEBBC98E9B893E8B48EE6B088E3AC81E2A77BE0A3
            74DE9E6EE2AA80C9621AEAAB80F3D0B7F3D0B7F3D0B7F2D0B7F1CEB3F0CBB0EF
            C9ACEEC6A8EDC2A3EBC09EEABB99E8B794E6B48FE4B089CD6E23EAAB80EAAB80
            EAAB80EAAB80EAAB80EAAB80E8A97CE6A477E2A070E29B6BE19762DD9059D98B
            52D88549D6803ED27735FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
      end
      object WebBrowser1: TWebBrowser
        Left = 0
        Top = 43
        Width = 1081
        Height = 402
        Align = alClient
        TabOrder = 1
        ControlData = {
          4C000000B96F00008C2900000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM ModeloDocumento'
      ' WHERE nCdModeloDocumento = :nPK')
    Left = 312
    Top = 368
    object qryMasternCdModeloDocumento: TIntegerField
      FieldName = 'nCdModeloDocumento'
    end
    object qryMastercNmModeloDocumento: TStringField
      FieldName = 'cNmModeloDocumento'
      Size = 30
    end
    object qryMastercArquivoModeloDoc: TStringField
      FieldName = 'cArquivoModeloDoc'
      Size = 100
    end
    object qryMastercDescricaoModeloDoc: TMemoField
      FieldName = 'cDescricaoModeloDoc'
      BlobType = ftMemo
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMasternCdTabTipoModeloDoc: TIntegerField
      FieldName = 'nCdTabTipoModeloDoc'
      LookupDataSet = qryTabTipoMod
      LookupKeyFields = 'nCdTabTipoModeloDoc'
      LookupResultField = 'cNmTabTipoModeloDoc'
      KeyFields = 'nCdTabTipoModeloDoc'
      LookupCache = True
    end
    object qryMastercNmStatus: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmStatus'
      LookupDataSet = qryStat
      LookupKeyFields = 'nCdStatus'
      LookupResultField = 'cNmStatus'
      KeyFields = 'nCdStatus'
      Size = 50
      Lookup = True
    end
    object qryMastercNmTabTipoModeloDoc: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmTabTipoModeloDoc'
      LookupDataSet = qryTabTipoMod
      LookupKeyFields = 'nCdTabTipoModeloDoc'
      LookupResultField = 'cNmTabTipoModeloDoc'
      KeyFields = 'nCdTabTipoModeloDoc'
      Size = 50
      Lookup = True
    end
  end
  inherited dsMaster: TDataSource
    Left = 312
    Top = 400
  end
  inherited qryID: TADOQuery
    Left = 416
    Top = 368
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 408
    Top = 400
  end
  inherited qryStat: TADOQuery
    Left = 376
    Top = 368
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 444
    Top = 400
  end
  inherited ImageList1: TImageList
    Left = 604
    Top = 392
  end
  object qryTabTipoMod: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM TabTipoModeloDoc')
    Left = 343
    Top = 368
    object qryTabTipoModnCdTabTipoModeloDoc: TIntegerField
      FieldName = 'nCdTabTipoModeloDoc'
    end
    object qryTabTipoModcNmTabTipoModeloDoc: TStringField
      FieldName = 'cNmTabTipoModeloDoc'
      Size = 50
    end
  end
  object dsTabTipoModeloDoc: TDataSource
    DataSet = qryTabTipoMod
    Left = 344
    Top = 400
  end
  object dsStat: TDataSource
    DataSet = qryStat
    Left = 376
    Top = 400
  end
  object qryTerceiroOrigem: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro     as nCdTerceiroOrigem'
      '       ,cNmTerceiro    as cNmTerceiroOrigem'
      '       ,cEmail         as cEmailOrigem'
      '       ,cEmailNFe      as cEmailNFeOrigem'
      '   FROM Terceiro'
      '  WHERE nCdTerceiro = :nCdTerceiro')
    Left = 444
    Top = 368
    object qryTerceiroOrigemnCdTerceiroOrigem: TIntegerField
      FieldName = 'nCdTerceiroOrigem'
    end
    object qryTerceiroOrigemcNmTerceiroOrigem: TStringField
      FieldName = 'cNmTerceiroOrigem'
      Size = 100
    end
    object qryTerceiroOrigemcEmailOrigem: TStringField
      FieldName = 'cEmailOrigem'
      Size = 50
    end
    object qryTerceiroOrigemcEmailNFeOrigem: TStringField
      FieldName = 'cEmailNFeOrigem'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdloja INT'
      'SET @nCdLoja = :nCdLoja'
      ''
      'SELECT cNmLoja'
      '  FROM Loja'
      ' WHERE ( (nCdLoja = @nCdLoja) OR (nCdLoja = 0) )')
    Left = 573
    Top = 368
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa INT'
      'SET @nCdEmpresa = :nCdEmpresa'
      ''
      'SELECT cNmEmpresa'
      '      ,cSigla'
      '      ,imgLogoEmpresaGrande'
      '  FROM Empresa'
      ' WHERE ( (nCdEmpresa = @nCdEmpresa) OR (nCdEmpresa = 0) )')
    Left = 605
    Top = 368
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresaimgLogoEmpresaGrande: TBlobField
      FieldName = 'imgLogoEmpresaGrande'
    end
  end
  object qryEnderecoOrigem: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cEndereco as cEnderecoOrigem'
      '  FROM Endereco'
      ' WHERE nCdTerceiro = :nCdTerceiro')
    Left = 508
    Top = 368
    object qryEnderecoOrigemcEnderecoOrigem: TStringField
      FieldName = 'cEnderecoOrigem'
      Size = 50
    end
  end
  object qryTerceiroDestino: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro     as nCdTerceiroDestino'
      '       ,cNmTerceiro    as cNmTerceiroDestino'
      '       ,cEmail         as cEmailDestino'
      '       ,cEmailNFe      as cEmailNFeDestino'
      '   FROM Terceiro'
      '  WHERE nCdTerceiro = :nCdTerceiro')
    Left = 476
    Top = 368
    object qryTerceiroDestinonCdTerceiroDestino: TIntegerField
      FieldName = 'nCdTerceiroDestino'
    end
    object qryTerceiroDestinocNmTerceiroDestino: TStringField
      FieldName = 'cNmTerceiroDestino'
      Size = 100
    end
    object qryTerceiroDestinocEmailDestino: TStringField
      FieldName = 'cEmailDestino'
      Size = 50
    end
    object qryTerceiroDestinocEmailNFeDestino: TStringField
      FieldName = 'cEmailNFeDestino'
      Size = 50
    end
  end
  object qryEnderecoDestino: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT cEndereco as cEnderecoDestino'
      '  FROM Endereco'
      ' WHERE nCdTerceiro = :nCdTerceiro')
    Left = 540
    Top = 368
    object qryEnderecoDestinocEnderecoDestino: TStringField
      FieldName = 'cEnderecoDestino'
      Size = 50
    end
  end
  object qryDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdDoctoFiscal'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT DoctoFiscal.cChaveNFe'
      '      ,DoctoFiscal.iNrDocto'
      '      ,DoctoFiscal.dDtEmissao '
      '      ,DoctoFiscal.nValTotal'
      '      ,CondPagto.cNmCondPagto'
      '      ,FormaPagto.cNmFormaPagto'
      '      ,Status.cNmStatus'
      '  FROM DoctoFiscal '
      
        '       LEFT JOIN CondPagto  ON CondPagto.nCdCondPagto   = DoctoF' +
        'iscal.nCdCondPagto'
      
        '       LEFT JOIN FormaPagto ON FormaPagto.nCdFormaPagto = CondPa' +
        'gto.nCdFormaPagto'
      
        '       LEFT JOIN Status     ON Status.nCdStatus         = DoctoF' +
        'iscal.nCdStatus'
      ' WHERE nCdDoctoFiscal = :nCdDoctoFiscal')
    Left = 476
    Top = 400
    object qryDoctoFiscalcChaveNFe: TStringField
      FieldName = 'cChaveNFe'
      Size = 100
    end
    object qryDoctoFiscaliNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryDoctoFiscaldDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryDoctoFiscalnValTotal: TBCDField
      FieldName = 'nValTotal'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalcNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryDoctoFiscalcNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryDoctoFiscalcNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object qryTitulo: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 60
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTitulo'
      '      ,cNrTit'
      '      ,nValTit'
      '      ,nValDesconto'
      '      ,nValJuro'
      '      ,nValAbatimento'
      '      ,nSaldoTit'
      '      ,dDtVenc'
      '  FROM Titulo'
      ' WHERE nCdTitulo in (select nCdTitulo'
      '                       from #TempTerceiro'
      '                      where nCdTerceiro = :nPK'
      '                        and cFlgAtivo   = 1)')
    Left = 508
    Top = 400
    object qryTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTitulocNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulonValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTitulonValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryTitulonValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTitulonValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object qryTitulonSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from #TempTerceiro')
    Left = 540
    Top = 400
  end
end
