inherited rptDoctoFiscalEmitido: TrptDoctoFiscalEmitido
  Left = 287
  Top = 187
  Width = 745
  Height = 360
  Caption = 'Documento Fiscal Emitido'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 729
    Height = 298
  end
  object Label1: TLabel [1]
    Left = 61
    Top = 45
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 82
    Top = 69
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 63
    Top = 93
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 10
    Top = 117
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Documento'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 51
    Top = 168
    Width = 51
    Height = 13
    Alignment = taRightJustify
    Caption = 'CPF/ CNPJ'
    FocusControl = DBEdit4
  end
  object Label6: TLabel [6]
    Left = 10
    Top = 190
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo de Emiss'#227'o'
    FocusControl = DBEdit4
  end
  object Label7: TLabel [7]
    Left = 188
    Top = 190
    Width = 9
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = #224
    FocusControl = DBEdit4
  end
  object Label8: TLabel [8]
    Left = 30
    Top = 141
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Pedido'
  end
  inherited ToolBar1: TToolBar
    Width = 729
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtEmpresa: TMaskEdit [10]
    Left = 105
    Top = 37
    Width = 65
    Height = 21
    TabOrder = 1
    OnExit = edtEmpresaExit
    OnKeyDown = edtEmpresaKeyDown
  end
  object edtLoja: TMaskEdit [11]
    Left = 105
    Top = 61
    Width = 65
    Height = 21
    TabOrder = 2
    OnExit = edtLojaExit
    OnKeyDown = edtLojaKeyDown
  end
  object edtTerceiro: TMaskEdit [12]
    Left = 105
    Top = 85
    Width = 65
    Height = 21
    TabOrder = 3
    OnExit = edtTerceiroExit
    OnKeyDown = edtTerceiroKeyDown
  end
  object edtTipoDocto: TMaskEdit [13]
    Left = 105
    Top = 109
    Width = 65
    Height = 21
    TabOrder = 4
    OnExit = edtTipoDoctoExit
    OnKeyDown = edtTipoDoctoKeyDown
  end
  object edtCPFCNPJ: TMaskEdit [14]
    Left = 105
    Top = 158
    Width = 142
    Height = 21
    EditMask = '##############;1; '
    MaxLength = 14
    TabOrder = 6
    Text = '              '
    OnExit = edtCPFCNPJExit
  end
  object edtDataInicial: TMaskEdit [15]
    Left = 105
    Top = 182
    Width = 62
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object edtDataFinal: TMaskEdit [16]
    Left = 209
    Top = 182
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
  end
  object DBEdit1: TDBEdit [17]
    Tag = 1
    Left = 173
    Top = 37
    Width = 549
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 11
  end
  object DBEdit2: TDBEdit [18]
    Tag = 1
    Left = 173
    Top = 61
    Width = 549
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 12
  end
  object DBEdit3: TDBEdit [19]
    Tag = 1
    Left = 173
    Top = 85
    Width = 549
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 13
  end
  object DBEdit4: TDBEdit [20]
    Tag = 1
    Left = 173
    Top = 109
    Width = 549
    Height = 21
    DataField = 'cNmTipoDoctoFiscal'
    DataSource = dsTipoDocto
    TabOrder = 14
  end
  object rdgOperacao: TRadioGroup [21]
    Left = 104
    Top = 214
    Width = 233
    Height = 33
    Caption = ' Opera'#231#227'o '
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'Entrada'
      'Sa'#237'da')
    TabOrder = 9
  end
  object rdgSituacao: TRadioGroup [22]
    Left = 344
    Top = 214
    Width = 233
    Height = 33
    Caption = ' Situa'#231#227'o '
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'Ativo'
      'Cancelado')
    TabOrder = 10
  end
  object edtTipoPedido: TEdit [23]
    Left = 105
    Top = 133
    Width = 65
    Height = 21
    TabOrder = 5
    OnExit = edtTipoPedidoExit
    OnKeyDown = edtTipoPedidoKeyDown
  end
  object DBEdit5: TDBEdit [24]
    Tag = 1
    Left = 173
    Top = 133
    Width = 550
    Height = 21
    DataField = 'cNmTipoPedido'
    DataSource = dsTipoPedido
    TabOrder = 15
  end
  inherited ImageList1: TImageList
    Left = 264
    Top = 256
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '              ,cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa =:nPK')
    Left = 104
    Top = 256
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '             ,cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja =:nPK')
    Left = 136
    Top = 256
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryTipoDocto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoDoctoFiscal'
      '              ,cNmTipoDoctoFiscal'
      'FROM TipoDoctoFiscal'
      'WHERE nCdTipoDoctoFiscal =:nPK')
    Left = 200
    Top = 256
    object qryTipoDoctonCdTipoDoctoFiscal: TIntegerField
      FieldName = 'nCdTipoDoctoFiscal'
    end
    object qryTipoDoctocNmTipoDoctoFiscal: TStringField
      FieldName = 'cNmTipoDoctoFiscal'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '              ,cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro =:nPK')
    Left = 168
    Top = 256
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 104
    Top = 288
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 136
    Top = 288
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 168
    Top = 288
  end
  object dsTipoDocto: TDataSource
    DataSet = qryTipoDocto
    Left = 200
    Top = 288
  end
  object ACBrValidador1: TACBrValidador
    IgnorarChar = './-'
    OnMsgErro = ACBrValidador1MsgErro
    Left = 296
    Top = 256
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTipoPedido'
      '      ,cNmTipoPedido'
      '  FROM TipoPedido'
      ' WHERE nCdTipoPedido = :nPK')
    Left = 232
    Top = 256
    object qryTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object dsTipoPedido: TDataSource
    DataSet = qryTipoPedido
    Left = 232
    Top = 288
  end
end
