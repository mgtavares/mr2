unit rPosTituloAberto_Cliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosTituloAberto_cliente = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText9: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    QRDBText11: TQRDBText;
    usp_RelatorionCdTitulo: TIntegerField;
    usp_RelatorionCdEspTit: TIntegerField;
    usp_RelatoriocNmEspTit: TStringField;
    usp_RelatorionCdSP: TIntegerField;
    usp_RelatoriocNrTit: TStringField;
    usp_RelatorionCdTerceiro: TIntegerField;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatorionCdMoeda: TIntegerField;
    usp_RelatoriocSigla: TStringField;
    usp_RelatoriocSenso: TStringField;
    usp_RelatoriodDtVenc: TDateTimeField;
    usp_RelatorionValTit: TFloatField;
    usp_RelatorionSaldoTit: TFloatField;
    usp_RelatoriodDtCancel: TDateTimeField;
    usp_RelatorionValJuro: TFloatField;
    QRDBText12: TQRDBText;
    usp_RelatorioiDias: TIntegerField;
    QRDBText10: TQRDBText;
    QRDBText13: TQRDBText;
    usp_RelatorionCdBancoPortador: TIntegerField;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    usp_RelatorionValVencer: TFloatField;
    usp_RelatorionValVencido: TFloatField;
    QRDBText4: TQRDBText;
    QRExpr7: TQRExpr;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    usp_RelatoriocFlgPrev: TIntegerField;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    usp_RelatoriodDtEmissao: TDateTimeField;
    QRDBText1: TQRDBText;
    QRLabel22: TQRLabel;
    QRImage1: TQRImage;
    QRLabel14: TQRLabel;
    QRLabel42: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape3: TQRShape;
    QRShape1: TQRShape;
    usp_RelatorioiParcela: TIntegerField;
    QRLabel36: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText16: TQRDBText;
    QRExpr8: TQRExpr;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    QRShape4: TQRShape;
    QRDBText17: TQRDBText;
    usp_RelatorionCdLojaTit: TIntegerField;
    QRDBText18: TQRDBText;
    QRLabel9: TQRLabel;
    QRDBText19: TQRDBText;
    usp_RelatorionCdEmpresa: TIntegerField;
    QRLabel10: TQRLabel;
    usp_RelatoriocNrNf: TStringField;
    QRLabel12: TQRLabel;
    QRDBText20: TQRDBText;
    QRLabel15: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape2: TQRShape;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosTituloAberto_cliente: TrptPosTituloAberto_cliente;

implementation

{$R *.dfm}

end.
