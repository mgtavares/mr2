unit fGrupoUsuario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, Mask, DBCtrls, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, cxControls, cxNavigator,
  cxDBNavigator, cxPC, ImgList, DBGridEhGrouping, cxLookAndFeelPainters,
  cxButtons, cxContainer, cxTreeView, Menus, ToolCtrlsEh;

type
  TfrmGrupoUsuario_Cad = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoUsuario: TIntegerField;
    qryMastercNmGrupoUsuario: TStringField;
    qryMasternCdStatus: TIntegerField;
    qryMastercNmStatus: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    qryUsuario: TADOQuery;
    qryUsuarioGrupoUsuario: TADOQuery;
    qryUsuarioGrupoUsuarionCdGrupoUsuario: TIntegerField;
    qryUsuarioGrupoUsuarionCdUsuario: TIntegerField;
    qryUsuarioGrupoUsuariodDataAcesso: TDateTimeField;
    qryUsuarioGrupoUsuarionCdUsuarioAcesso: TIntegerField;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    qryUsuariocNmLogin: TStringField;
    qryUsuarioGrupoUsuariocNmUsuario: TStringField;
    qryUsuarioGrupoUsuariocNmLogin: TStringField;
    dsUsuarioGrupoUsuario: TDataSource;
    qryAplicacaoGrupoUsuario: TADOQuery;
    dsAplicacaoGrupoUsuario: TDataSource;
    qryAplicacaoGrupoUsuarionCdGrupoUsuario: TIntegerField;
    qryAplicacaoGrupoUsuarionCdAplicacao: TIntegerField;
    qryAplicacaoGrupoUsuariodDataAcesso: TDateTimeField;
    qryAplicacaoGrupoUsuarionCdUsuarioAcesso: TIntegerField;
    qryAplicacao: TADOQuery;
    qryAplicacaonCdAplicacao: TIntegerField;
    qryAplicacaocNmAplicacao: TStringField;
    qryAplicacaonCdStatus: TIntegerField;
    qryAplicacaonCdModulo: TIntegerField;
    qryAplicacaocNmObjeto: TStringField;
    qryAplicacaoGrupoUsuariocNmAplicacao: TStringField;
    qryParticulAplicacao: TADOQuery;
    qryParticulAplicacaocSiglaParticul: TStringField;
    qryParticulAplicacaonCdAplicacao: TIntegerField;
    qryParticulAplicacaocNmParticul: TStringField;
    qryParticulAplicacaoGrupoUsuario: TADOQuery;
    qryParticulAplicacaoGrupoUsuarionCdGrupoUsuario: TIntegerField;
    qryParticulAplicacaoGrupoUsuariocSiglaParticul: TStringField;
    qryParticulAplicacaoGrupoUsuariocFlgPermissao: TIntegerField;
    qryParticulAplicacaoGrupoUsuariocNmParticul: TStringField;
    dsParticulAplicacaoGrupoUsuario: TDataSource;
    qryAplicacaoGrupoUsuariocFlgAlterar: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    qryUsuarioGrupoUsuarionCdUsuarioGrupoUsuario: TIntegerField;
    qryAplicacaoGrupoUsuarionCdAplicacaoGrupoUsuario: TIntegerField;
    btMenuAninhado: TcxButton;
    procedure qryUsuarioGrupoUsuarioBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure qryAplicacaoGrupoUsuarioBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryParticulAplicacaoGrupoUsuarioBeforePost(
      DataSet: TDataSet);
    procedure qryAplicacaoGrupoUsuarioAfterPost(DataSet: TDataSet);
    procedure qryAplicacaoGrupoUsuarioBeforeDelete(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btMenuAninhadoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoUsuario_Cad: TfrmGrupoUsuario_Cad;

implementation

uses fMenu, fLookup_Padrao, fGrupoUsuario_Permissoes;


{$R *.dfm}

procedure TfrmGrupoUsuario_Cad.qryUsuarioGrupoUsuarioBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  inherited;

  if (qryUsuarioGrupoUsuario.State = dsInsert) then
      qryUsuarioGrupoUsuarionCdUsuarioGrupoUsuario.Value := frmMenu.fnProximoCodigo('USUARIOGRUPOUSUARIO') ;

  qryUsuarioGrupoUsuarionCdGrupoUsuario.Value  := qryMasternCdGrupoUsuario.Value;
  qryUsuarioGrupoUsuariodDataAcesso.Value      := Now() ;
  qryUsuarioGrupoUsuarionCdUsuarioAcesso.Value := frmMenu.nCdUsuarioLogado ;
end;

procedure TfrmGrupoUsuario_Cad.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioGrupoUsuario.Close ;
  qryAplicacaoGrupoUsuario.Close ;
  qryParticulAplicacaoGrupoUsuario.Close ;

end;

procedure TfrmGrupoUsuario_Cad.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioGrupoUsuario.Close ;
  qryUsuarioGrupoUsuario.Parameters.ParamByName('nCdGrupoUsuario').Value := qryMasternCdGrupoUsuario.Value ;
  qryUsuarioGrupoUsuario.Open ;

  qryAplicacaoGrupoUsuario.Close ;
  qryAplicacaoGrupoUsuario.Parameters.ParamByName('nCdGrupoUsuario').Value := qryMasternCdGrupoUsuario.Value ;
  qryAplicacaoGrupoUsuario.Open ;

end;

procedure TfrmGrupoUsuario_Cad.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmGrupoUsuario_Cad.qryAplicacaoGrupoUsuarioBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  inherited;

  if (qryAplicacaoGrupoUsuario.State = dsInsert) then
      qryAplicacaoGrupoUsuarionCdAplicacaoGrupoUsuario.Value := frmMenu.fnProximoCodigo('APLICACAOGRUPOUSUARIO') ;

  qryAplicacaoGrupoUsuarionCdGrupoUsuario.Value  := qryMasternCdGrupoUsuario.Value;
  qryAplicacaoGrupoUsuariodDataAcesso.Value      := Now() ;
  qryAplicacaoGrupoUsuarionCdUsuarioAcesso.Value := frmMenu.nCdUsuarioLogado ;

end;

procedure TfrmGrupoUsuario_Cad.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var nPK: Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioGrupoUsuario.State = dsBrowse) then
             qryUsuarioGrupoUsuario.Edit ;

        if (qryUsuarioGrupoUsuario.State = dsInsert) or (qryUsuarioGrupoUsuario.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            if (nPK > 0) then
                qryUsuarioGrupoUsuarionCdUsuario.Value := nPK ;

        end ;


    end ;

  end ;

end;

procedure TfrmGrupoUsuario_Cad.DBGridEh2KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var nPK: Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryAplicacaoGrupoUsuario.State = dsBrowse) then
             qryAplicacaoGrupoUsuario.Edit ;

        if (qryAplicacaoGrupoUsuario.State = dsInsert) or (qryAplicacaoGrupoUsuario.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(1);

            if (nPK > 0) then
                qryAplicacaoGrupoUsuarionCdAplicacao.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmGrupoUsuario_Cad.qryParticulAplicacaoGrupoUsuarioBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  inherited;

end;

procedure TfrmGrupoUsuario_Cad.qryAplicacaoGrupoUsuarioAfterPost(
  DataSet: TDataSet);
var
    qryAux : TADOQuery ;
    cQueryAntiga : TStrings ;
begin
  inherited;

end;

procedure TfrmGrupoUsuario_Cad.qryAplicacaoGrupoUsuarioBeforeDelete(
  DataSet: TDataSet);
var
    qryAux : TADOQuery ;
begin
  inherited;

end;

procedure TfrmGrupoUsuario_Cad.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmGrupoUsuario_Cad.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOUSUARIO' ;
  nCdTabelaSistema  := 4 ;
  nCdConsultaPadrao := 6 ;

end;

procedure TfrmGrupoUsuario_Cad.btMenuAninhadoClick(Sender: TObject);
var
  objForm : TfrmGrupoUsuario_Permissoes;
begin
  inherited;

  if not(qryMaster.Active) then
      exit;

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  objForm := TfrmGrupoUsuario_Permissoes.Create(nil);

  objForm.nCdGrupoUsuario := qryMasternCdGrupoUsuario.Value;

  showForm(objForm,True);

  qryAplicacaoGrupoUsuario.Close;
  qryAplicacaoGrupoUsuario.Open;

end;

initialization
    RegisterClass(tFrmGrupoUsuario_Cad) ;

end.

