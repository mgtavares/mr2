inherited frmTipoAlerta: TfrmTipoAlerta
  Left = 274
  Top = 99
  Height = 474
  Caption = 'Tipo de Alertas'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 411
  end
  object Label1: TLabel [1]
    Left = 54
    Top = 35
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 10
    Top = 59
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o Alerta'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 60
    Top = 83
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit3
  end
  inherited ToolBar2: TToolBar
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited ToolButton7: TToolButton
      Visible = False
    end
    inherited ToolButton9: TToolButton
      Visible = False
    end
    inherited ToolButton6: TToolButton
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [5]
    Left = 96
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdTipoAlerta'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Tag = 1
    Left = 96
    Top = 56
    Width = 489
    Height = 19
    DataField = 'cNmTipoAlerta'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [7]
    Left = 96
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 3
  end
  object DBEdit4: TDBEdit [8]
    Tag = 1
    Left = 164
    Top = 80
    Width = 421
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBGridEh1: TDBGridEh [9]
    Left = 8
    Top = 120
    Width = 577
    Height = 297
    DataGrouping.GroupLevels = <>
    DataSource = dsUsuarioTipoAlerta
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    OnEnter = DBGridEh1Enter
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTipoAlerta'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdUsuario'
        Footers = <>
        Title.Caption = 'Usu'#225'rios que Recebem o Alerta|C'#243'd.'
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
        ReadOnly = True
        Width = 470
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoAlerta'
      'WHERE nCdTipoAlerta = :nPK')
    Left = 256
    Top = 304
    object qryMasternCdTipoAlerta: TAutoIncField
      FieldName = 'nCdTipoAlerta'
      ReadOnly = True
    end
    object qryMastercNmTipoAlerta: TStringField
      FieldName = 'cNmTipoAlerta'
      Size = 30
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMastercNmStatus: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmStatus'
      LookupDataSet = qryStat
      LookupKeyFields = 'nCdStatus'
      LookupResultField = 'cNmStatus'
      KeyFields = 'nCdStatus'
      Size = 50
      Lookup = True
    end
  end
  inherited dsMaster: TDataSource
    Left = 256
    Top = 336
  end
  inherited qryID: TADOQuery
    Left = 320
    Top = 336
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 352
    Top = 304
  end
  inherited qryStat: TADOQuery
    Left = 384
    Top = 304
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 352
    Top = 336
  end
  inherited ImageList1: TImageList
    Left = 384
    Top = 336
  end
  object qryUsuarioTipoAlerta: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuarioTipoAlertaBeforePost
    OnCalcFields = qryUsuarioTipoAlertaCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM UsuarioTipoAlerta'
      'WHERE nCdTipoAlerta = :nPK')
    Left = 288
    Top = 304
    object qryUsuarioTipoAlertanCdUsuario: TIntegerField
      DisplayLabel = 'Usu'#225'rios que Recebem o Alerta|C'#243'd'
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioTipoAlertanCdTipoAlerta: TIntegerField
      FieldName = 'nCdTipoAlerta'
    end
    object qryUsuarioTipoAlertacNmUsuario: TStringField
      DisplayLabel = 'Usu'#225'rios que Recebem o Alerta|Nome Usu'#225'rio'
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 50
      Calculated = True
    end
  end
  object dsUsuarioTipoAlerta: TDataSource
    DataSet = qryUsuarioTipoAlerta
    Left = 288
    Top = 336
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 320
    Top = 304
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
end
