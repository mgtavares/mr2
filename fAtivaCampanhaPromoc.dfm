inherited frmAtivaCampanhaPromoc: TfrmAtivaCampanhaPromoc
  Caption = 'Ativa'#231#227'o Campanha Promocional'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    ButtonWidth = 93
    inherited ToolButton1: TToolButton
      Caption = 'Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 93
    end
    inherited ToolButton2: TToolButton
      Left = 101
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    DataGrouping.GroupLevels = <>
    DataSource = dsMaster
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    PopupMenu = PopupMenu1
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdCampanhaPromoc'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cChaveCampanha'
        Footers = <>
        Width = 184
      end
      item
        EditButtons = <>
        FieldName = 'cNmCampanhaPromoc'
        Footers = <>
        Width = 377
      end
      item
        EditButtons = <>
        FieldName = 'dDtValidadeIni'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtValidadeFim'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoTabPreco'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryMaster: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCampanhaPromoc'
      '      ,cChaveCampanha'
      '      ,cNmCampanhaPromoc'
      '      ,dDtValidadeIni'
      '      ,dDtValidadeFim'
      '      ,cNmTipoTabPreco'
      '  FROM CampanhaPromoc'
      
        '       LEFT JOIN TipoTabPreco ON TipoTabPreco.nCdTipoTabPreco = ' +
        'CampanhaPromoc.nCdTipoTabPreco'
      ' WHERE cFlgAtivada = 0'
      '   AND nCdEmpresa  = :nPK'
      '   AND cFlgSuspensa = 0'
      ' ORDER BY 1')
    Left = 328
    Top = 144
    object qryMasternCdCampanhaPromoc: TIntegerField
      DisplayLabel = 'Campanhas Pendentes de Ativa'#231#227'o|C'#243'd.'
      FieldName = 'nCdCampanhaPromoc'
    end
    object qryMastercChaveCampanha: TStringField
      DisplayLabel = 'Campanhas Pendentes de Ativa'#231#227'o|Chave'
      FieldName = 'cChaveCampanha'
      Size = 15
    end
    object qryMastercNmCampanhaPromoc: TStringField
      DisplayLabel = 'Campanhas Pendentes de Ativa'#231#227'o|Descri'#231#227'o Campanha'
      FieldName = 'cNmCampanhaPromoc'
      Size = 50
    end
    object qryMasterdDtValidadeIni: TDateTimeField
      DisplayLabel = 'Campanhas Pendentes de Ativa'#231#227'o|V'#225'lida de'
      FieldName = 'dDtValidadeIni'
    end
    object qryMasterdDtValidadeFim: TDateTimeField
      DisplayLabel = 'Campanhas Pendentes de Ativa'#231#227'o|V'#225'lida at'#233
      FieldName = 'dDtValidadeFim'
    end
    object qryMastercNmTipoTabPreco: TStringField
      DisplayLabel = 'Campanhas Pendentes de Ativa'#231#227'o|Tipo Tabela Pre'#231'o'
      FieldName = 'cNmTipoTabPreco'
      Size = 50
    end
  end
  object dsMaster: TDataSource
    DataSet = qryMaster
    Left = 408
    Top = 144
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 200
    Top = 168
  end
  object PopupMenu1: TPopupMenu
    Left = 352
    Top = 224
    object AtivarCampanha1: TMenuItem
      Caption = 'Ativar Campanha'
      OnClick = AtivarCampanha1Click
    end
  end
end
