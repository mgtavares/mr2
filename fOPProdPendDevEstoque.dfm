inherited frmOPProdPendDevEstoque: TfrmOPProdPendDevEstoque
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'Produto Pendente Devolu'#231#227'o Estoque'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1152
    Height = 785
  end
  inherited ToolBar1: TToolBar
    Width = 1152
    ButtonWidth = 97
    inherited ToolButton1: TToolButton
      Caption = '&Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 97
    end
    inherited ToolButton2: TToolButton
      Left = 105
    end
  end
  object cxGrid5: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1152
    Height = 785
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    LookAndFeel.NativeStyle = True
    object cxGridDBTableView5: TcxGridDBTableView
      DataController.DataSource = dsProdutoPendente
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTotal'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellMultiSelect = True
      Styles.Content = frmMenu.FonteSomenteLeitura
      object cxGridDBTableView5nCdOrdemProducao: TcxGridDBColumn
        Caption = 'C'#243'd. OP'
        DataBinding.FieldName = 'nCdOrdemProducao'
        Width = 57
      end
      object cxGridDBTableView5cNumeroOP: TcxGridDBColumn
        Caption = 'N'#250'mero OP'
        DataBinding.FieldName = 'cNumeroOP'
        Visible = False
        GroupIndex = 0
        Width = 89
      end
      object cxGridDBTableView5cNmTipoOP: TcxGridDBColumn
        Caption = 'Tipo OP'
        DataBinding.FieldName = 'cNmTipoOP'
        Width = 90
      end
      object cxGridDBTableView5dDtRealConclusao: TcxGridDBColumn
        Caption = 'Dt. Real Conclus'#227'o'
        DataBinding.FieldName = 'dDtRealConclusao'
        Width = 106
      end
      object cxGridDBTableView5nCdProduto: TcxGridDBColumn
        Caption = 'C'#243'd. Prod.'
        DataBinding.FieldName = 'nCdProduto'
      end
      object cxGridDBTableView5cNmProduto: TcxGridDBColumn
        Caption = 'Descri'#231#227'o Produto'
        DataBinding.FieldName = 'cNmProduto'
        Width = 233
      end
      object cxGridDBTableView5cUnidadeMedida: TcxGridDBColumn
        Caption = 'U.M'
        DataBinding.FieldName = 'cUnidadeMedida'
        Width = 46
      end
      object cxGridDBTableView5nQtdePlanejada: TcxGridDBColumn
        Caption = 'Qtde. Planej.'
        DataBinding.FieldName = 'nQtdePlanejada'
        Width = 99
      end
      object cxGridDBTableView5nQtdeAtendida: TcxGridDBColumn
        Caption = 'Qtde. Atendida'
        DataBinding.FieldName = 'nQtdeAtendida'
      end
      object cxGridDBTableView5nQtdeConsumida: TcxGridDBColumn
        Caption = 'Qtde Consumida'
        DataBinding.FieldName = 'nQtdeConsumida'
      end
      object cxGridDBTableView5nSaldoDevolver: TcxGridDBColumn
        Caption = 'Saldo Devolv.'
        DataBinding.FieldName = 'nSaldoDevolver'
        Width = 104
      end
      object cxGridDBTableView5nCdLocalEstoque: TcxGridDBColumn
        DataBinding.FieldName = 'nCdLocalEstoque'
        Visible = False
      end
      object cxGridDBTableView5cNmLocalEstoque: TcxGridDBColumn
        Caption = 'Estoque Sa'#237'da'
        DataBinding.FieldName = 'cNmLocalEstoque'
        Width = 227
      end
      object cxGridDBTableView5nCdPedido: TcxGridDBColumn
        Caption = 'Pedido Venda'
        DataBinding.FieldName = 'nCdPedido'
        Width = 100
      end
      object cxGridDBTableView5cNmTerceiro: TcxGridDBColumn
        Caption = 'Terceiro Entrega'
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 207
      end
    end
    object cxGridLevel5: TcxGridLevel
      GridView = cxGridDBTableView5
    end
  end
  inherited ImageList1: TImageList
    Left = 368
    Top = 144
  end
  object qryProdutoPendente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT OP.nCdOrdemProducao'
      '      ,OP.cNumeroOP'
      '      ,TipoOP.cNmTipoOP'
      '      ,OP.nCdPedido'
      '      ,Terceiro.cNmTerceiro '
      '      ,OP.dDtRealConclusao'
      '      ,Produto.nCdProduto'
      '      ,Produto.cNmProduto'
      '      ,Produto.cUnidadeMedida'
      '      ,PEOP.nQtdePlanejada'
      '      ,PEOP.nQtdeAtendida'
      '      ,PEOP.nQtdeConsumida'
      
        '      ,(PEOP.nQtdeAtendida - PEOP.nQtdeConsumida - PEOP.nQtdeDev' +
        'olvida) as nSaldoDevolver'
      '      ,LocalEstoque.nCdLocalEstoque'
      
        '      ,dbo.fn_ZeroEsquerda(LocalEstoque.nCdLocalEstoque,5) + '#39' -' +
        ' '#39' + cNmLocalEstoque as cNmLocalEstoque'
      '  FROM ProdutoEtapaOrdemProducao PEOP'
      
        '       INNER JOIN EtapaOrdemProducao EOP ON EOP.nCdEtapaOrdemPro' +
        'ducao    = PEOP.nCdEtapaOrdemProducao'
      
        '       LEFT  JOIN LocalEstoque           ON LocalEstoque.nCdLoca' +
        'lEstoque = PEOP.nCdLocalEstoque'
      
        '       INNER JOIN OrdemProducao OP       ON OP.nCdOrdemProducao ' +
        '         = EOP.nCdOrdemProducao'
      
        '       INNER JOIN TipoOP                 ON TipoOP.nCdTipoOP    ' +
        '         = OP.nCdTipoOP'
      
        '       INNER JOIN Produto                ON Produto.nCdProduto  ' +
        '         = PEOP.nCdProduto'
      
        '       LEFT  JOIN Pedido                 ON Pedido.nCdPedido    ' +
        '         = OP.nCdPedido'
      
        '       LEFT  JOIN Terceiro               ON Terceiro.nCdTerceiro' +
        '         = Pedido.nCdTerceiro'
      
        ' WHERE (PEOP.nQtdeConsumida+PEOP.nQtdeDevolvida) < PEOP.nQtdeAte' +
        'ndida '
      '   AND PEOP.nQtdeConsumida     > 0'
      '   AND PEOP.nQtdeAtendida      > 0'
      '   AND OP.nCdTabTipoStatusOP   = 5'
      '   AND OP.nCdEmpresa           = :nPK'
      ' ORDER BY dDtRealConclusao')
    Left = 168
    Top = 168
    object qryProdutoPendentenCdOrdemProducao: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object qryProdutoPendentecNumeroOP: TStringField
      FieldName = 'cNumeroOP'
    end
    object qryProdutoPendentenCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryProdutoPendentecNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryProdutoPendentedDtRealConclusao: TDateTimeField
      FieldName = 'dDtRealConclusao'
    end
    object qryProdutoPendentenCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutoPendentecNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutoPendentecUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryProdutoPendentenQtdePlanejada: TBCDField
      FieldName = 'nQtdePlanejada'
      Precision = 14
      Size = 6
    end
    object qryProdutoPendentenQtdeAtendida: TBCDField
      FieldName = 'nQtdeAtendida'
      Precision = 14
      Size = 6
    end
    object qryProdutoPendentenQtdeConsumida: TBCDField
      FieldName = 'nQtdeConsumida'
      Precision = 14
      Size = 6
    end
    object qryProdutoPendentenSaldoDevolver: TBCDField
      FieldName = 'nSaldoDevolver'
      ReadOnly = True
      Precision = 16
      Size = 6
    end
    object qryProdutoPendentenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryProdutoPendentecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      ReadOnly = True
      Size = 68
    end
    object qryProdutoPendentecNmTipoOP: TStringField
      FieldName = 'cNmTipoOP'
      Size = 50
    end
  end
  object dsProdutoPendente: TDataSource
    DataSet = qryProdutoPendente
    Left = 208
    Top = 168
  end
end
