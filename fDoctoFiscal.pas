unit fDoctoFiscal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxStyles,
  cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit,
  cxDBData, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGridLevel, cxClasses, cxControls, cxGridCustomView, cxGrid,
  cxLookAndFeelPainters, cxButtons, cxPC, DBGridEhGrouping, ToolCtrlsEh,
  Menus, ACBrNFe, ACBrNFeDANFEClass;

type
  TfrmDoctoFiscal = class(TfrmCadastro_Padrao)
    qryMasternCdDoctoFiscal: TAutoIncField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdSerieFiscal: TIntegerField;
    qryMastercFlgEntSai: TStringField;
    qryMastercModelo: TStringField;
    qryMastercCFOP: TStringField;
    qryMastercTextoCFOP: TStringField;
    qryMasteriNrDocto: TIntegerField;
    qryMasternCdTipoDoctoFiscal: TIntegerField;
    qryMasterdDtEmissao: TDateTimeField;
    qryMasterdDtSaida: TDateTimeField;
    qryMasterdDtImpressao: TDateTimeField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNmTerceiro: TStringField;
    qryMastercCNPJCPF: TStringField;
    qryMastercIE: TStringField;
    qryMastercTelefone: TStringField;
    qryMasternCdEndereco: TIntegerField;
    qryMastercEndereco: TStringField;
    qryMasteriNumero: TIntegerField;
    qryMastercBairro: TStringField;
    qryMastercCidade: TStringField;
    qryMastercCep: TStringField;
    qryMastercUF: TStringField;
    qryMasternValTotal: TBCDField;
    qryMasternValProduto: TBCDField;
    qryMasternValBaseICMS: TBCDField;
    qryMasternValICMS: TBCDField;
    qryMasternValBaseICMSSub: TBCDField;
    qryMasternValICMSSub: TBCDField;
    qryMasternValIsenta: TBCDField;
    qryMasternValOutras: TBCDField;
    qryMasternValDespesa: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMasterdDtCad: TDateTimeField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMastercFlgFaturado: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMastercNmTransp: TStringField;
    qryMastercCNPJTransp: TStringField;
    qryMastercIETransp: TStringField;
    qryMastercEnderecoTransp: TStringField;
    qryMastercBairroTransp: TStringField;
    qryMastercCidadeTransp: TStringField;
    qryMastercUFTransp: TStringField;
    qryMasteriQtdeVolume: TIntegerField;
    qryMasternPesoBruto: TBCDField;
    qryMasternPesoLiq: TBCDField;
    qryMastercPlaca: TStringField;
    qryMastercUFPlaca: TStringField;
    qryMasternValIPI: TBCDField;
    qryMasternValFatura: TBCDField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternCdUsuarioImpr: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternCdUsuarioCancel: TIntegerField;
    qryItemDoctoFiscal: TADOQuery;
    dsItem: TDataSource;
    ToolButton3: TToolButton;
    qryMasternCdTipoPedido: TIntegerField;
    qryMastercFlgIntegrado: TIntegerField;
    qryMasternValCredAdiant: TBCDField;
    qryMasternValIcentivoFiscal: TBCDField;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label22: TLabel;
    DBEdit1: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit29: TDBEdit;
    qryMastercNrProtocoloNFe: TStringField;
    qryMastercNrReciboNFe: TStringField;
    qryEnvioNFeEmail: TADOQuery;
    qryEnvioNFeEmailcFlgEnviaNFeEmail: TIntegerField;
    qryEnvioNFeEmailcEmailNFe: TStringField;
    qryEnvioNFeEmailcEmailCopiaNFe: TStringField;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    qryMastercChaveNFe: TStringField;
    qryMastercNrProtocoloCancNFe: TStringField;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label23: TLabel;
    DBEdit30: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    qryMastercNrProtocoloDPEC: TStringField;
    qryMasternCdTabTipoEmissaoDoctoFiscal: TIntegerField;
    qryMastercSerie: TStringField;
    qryItemDoctoFiscalnCdItemDoctoFiscal: TIntegerField;
    qryItemDoctoFiscalnCdDoctoFiscal: TIntegerField;
    qryItemDoctoFiscalnCdItemPedido: TIntegerField;
    qryItemDoctoFiscalnCdProduto: TIntegerField;
    qryItemDoctoFiscalcNmItem: TStringField;
    qryItemDoctoFiscalcUnidadeMedida: TStringField;
    qryItemDoctoFiscalcCFOP: TStringField;
    qryItemDoctoFiscalnQtde: TFloatField;
    qryItemDoctoFiscalnValUnitario: TFloatField;
    qryItemDoctoFiscalnValTotal: TFloatField;
    qryItemDoctoFiscalnAliqICMS: TFloatField;
    qryItemDoctoFiscalnValICMS: TFloatField;
    qryItemDoctoFiscalnAliqIPI: TFloatField;
    qryItemDoctoFiscalnValDesconto: TFloatField;
    qryItemDoctoFiscalnValAcrescimo: TFloatField;
    qryItemDoctoFiscalnValIPI: TFloatField;
    qryItemDoctoFiscalnValIcentivoFiscal: TFloatField;
    qryItemDoctoFiscalnValBaseICMS: TFloatField;
    qryItemDoctoFiscalnValBaseIPI: TFloatField;
    qryItemDoctoFiscalcCdST: TStringField;
    qryItemDoctoFiscalcCdSTIPI: TStringField;
    qryItemDoctoFiscalnValBaseICMSSub: TFloatField;
    qryItemDoctoFiscalnValICMSSub: TFloatField;
    qryItemDoctoFiscalnValDespesa: TFloatField;
    qryItemDoctoFiscalnValFrete: TFloatField;
    qryItemDoctoFiscalcCSTPIS: TStringField;
    qryItemDoctoFiscalnValBasePIS: TFloatField;
    qryItemDoctoFiscalnAliqPIS: TFloatField;
    qryItemDoctoFiscalnValPIS: TFloatField;
    qryItemDoctoFiscalcCSTCOFINS: TStringField;
    qryItemDoctoFiscalnValBaseCOFINS: TFloatField;
    qryItemDoctoFiscalnAliqCOFINS: TFloatField;
    qryItemDoctoFiscalnValCOFINS: TFloatField;
    qryItemDoctoFiscalnValSeguro: TFloatField;
    qryItemDoctoFiscalcNCMSH: TStringField;
    qryItemDoctoFiscalnCdPedido: TIntegerField;
    DBGridEh1: TDBGridEh;
    qryMasternValBaseIPI: TBCDField;
    qryMasternValPIS: TBCDField;
    qryMasternValCOFINS: TBCDField;
    Label13: TLabel;
    DBEdit20: TDBEdit;
    Label20: TLabel;
    DBEdit27: TDBEdit;
    Label21: TLabel;
    DBEdit28: TDBEdit;
    Label24: TLabel;
    DBEdit31: TDBEdit;
    Label25: TLabel;
    DBEdit32: TDBEdit;
    Label26: TLabel;
    DBEdit33: TDBEdit;
    qryMasternValSeguro: TBCDField;
    Label27: TLabel;
    DBEdit34: TDBEdit;
    qryMastercXMLNFe: TMemoField;
    qryItemDoctoFiscalnCdGrupoImposto: TIntegerField;
    qryItemDoctoFiscalnCdNaturezaOperacao: TIntegerField;
    qryItemDoctoFiscalnValUnitarioPed: TFloatField;
    qryItemDoctoFiscalnValCredAdiant: TFloatField;
    qryItemDoctoFiscalnCdServidorOrigem: TIntegerField;
    qryItemDoctoFiscaldDtReplicacao: TDateTimeField;
    qryItemDoctoFiscalcDescricaoAdicional: TStringField;
    qryItemDoctoFiscalnPercIVA: TFloatField;
    qryItemDoctoFiscalnPercBCICMS: TFloatField;
    qryItemDoctoFiscalnPercBCIVA: TFloatField;
    qryItemDoctoFiscalnPercBCICMSST: TFloatField;
    qryItemDoctoFiscalnValIsenta: TFloatField;
    qryItemDoctoFiscalnValOutras: TFloatField;
    qryItemDoctoFiscalnCdTipoICMSECF: TIntegerField;
    qryItemDoctoFiscalnCdTabTipoOrigemMercadoria: TIntegerField;
    qryItemDoctoFiscalcEnqLegalIPI: TStringField;
    qryItemDoctoFiscalcEXTIPI: TStringField;
    qryItemDoctoFiscalnPercCargaMediaST: TFloatField;
    qryItemDoctoFiscalnAliqICMSInternaDestino: TFloatField;
    qryItemDoctoFiscalnAliqICMSInternaOrigem: TFloatField;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    qryRepresentante: TADOQuery;
    qryRepresentantecNmTerceiro: TStringField;
    dsRepresentante: TDataSource;
    qryCartaCorrecaoNFe: TADOQuery;
    qryCartaCorrecaoNFenCdCartaCorrecaoNFe: TIntegerField;
    PopupMenu1: TPopupMenu;
    GerarXML1: TMenuItem;
    ToolButton10: TToolButton;
    qryUsuario: TADOQuery;
    dsUsuario: TDataSource;
    qryUsuariocNmUsuario: TStringField;
    Label29: TLabel;
    GerarPDF1: TMenuItem;
    DBEdit35: TDBEdit;
    N1: TMenuItem;
    qryMastercJustCancelamento: TStringField;
    qryMastercFlgCancForaPrazo: TIntegerField;
    Label28: TLabel;
    DBEdit36: TDBEdit;
    checkCancForaPrazo: TDBCheckBox;
    Label30: TLabel;
    qryMasteriStatusRetorno: TIntegerField;
    qryMastercNmStatusRetorno: TStringField;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    Label31: TLabel;
    qryItemDoctoFiscalnValImpostoAproxFed: TFloatField;
    qryItemDoctoFiscalnValImpostoAproxEst: TFloatField;
    qryItemDoctoFiscalnValImpostoAproxMun: TFloatField;
    qryItemDoctoFiscalnAliqFederal: TFloatField;
    qryItemDoctoFiscalnAliqEstadual: TFloatField;
    qryItemDoctoFiscalnAliqMunicipal: TFloatField;
    qryMastercFlgAmbienteHom: TIntegerField;
    qryMastercArquivoXML: TStringField;
    qryDoctoFiscalAux: TADOQuery;
    qryDoctoFiscalAuxnCdDoctoFiscal: TIntegerField;
    qryDoctoFiscalAuxnCdEmpresa: TIntegerField;
    qryDoctoFiscalAuxcChaveNFe: TStringField;
    qryDoctoFiscalAuxnCdStatus: TIntegerField;
    qryDoctoFiscalAuxcCaminhoXML: TStringField;
    qryDoctoFiscalAuxcCaminhoXMLCanc: TStringField;
    qryDoctoFiscalAuxcArquivoXML: TStringField;
    qryDoctoFiscalAuxcArquivoXMLCanc: TStringField;
    qryDoctoFiscalAuxcFlgAmbienteHom: TIntegerField;
    qryDoctoFiscalAuxcXMLNFe: TMemoField;
    btEnviaDocMonitorWeb: TMenuItem;
    qryEnviaPedidoWebMonitor: TADOQuery;
    qryMastercFlgCFe: TIntegerField;
    qryTipoDoctoFiscal: TADOQuery;
    dsTipoDoctoFiscal: TDataSource;
    Label32: TLabel;
    DBEdit39: TDBEdit;
    qryTipoDoctoFiscalcNmTipoDoctoFiscal: TStringField;
    DBEdit40: TDBEdit;
    cxButton6: TcxButton;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure GeraNFeClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton5Click(Sender: TObject);
    procedure GerarXML1Click(Sender: TObject);
    procedure qryMasterAfterOpen(DataSet: TDataSet);
    procedure GerarPDF1Click(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure prGerarXMLNFe(nCdDoctoFiscal : Integer; bGeraArqAtual : Boolean = True);
    procedure btEnviaDocMonitorWebClick(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDoctoFiscal: TfrmDoctoFiscal;

implementation

uses fImpDFPadrao, rRomaneio, fMenu, fEmiteNFe2,rRomaneio_Retrato,
     fCartaCorrecaoNFe_HistoricoCCe,fFuncoesSAT,fModImpETP;

{$R *.dfm}

procedure TfrmDoctoFiscal.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'DOCTOFISCAL' ;
  nCdTabelaSistema  := 42 ;
  nCdConsultaPadrao := 97 ;
end;

procedure TfrmDoctoFiscal.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryItemDoctoFiscal, qryMasternCdDoctoFiscal.asString) ;
  PosicionaQuery(qryTipoDoctoFiscal,qryMasternCdTipoDoctoFiscal.AsString);
end;

procedure TfrmDoctoFiscal.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryItemDoctoFiscal.Close ;
  qryTipoDoctoFiscal.Close ;
end;

procedure TfrmDoctoFiscal.cxButton3Click(Sender: TObject);
var
  objEmiteNfe : TfrmEmiteNfe2;
begin

  if not qryMaster.Active then
      exit ;

  qryEnvioNFeEmail.Close;
  qryEnvioNFeEmail.Parameters.ParamByName('nPK').Value := qryMasternCdTerceiro.AsString;
  qryEnvioNFeEmail.Open;

  if(qryEnvioNFeEmail.Eof) then
      exit;

  if (qryEnvioNFeEmailcFlgEnviaNFeEmail.Value <> 1) then
  begin
      MensagemAlerta('Terceiro n�o configurado para envio de NFe por email.');
      Exit;
  end;

  if (qryMasternCdStatus.Value <> 1) then
  begin
      case MessageDlg('Documento fiscal cancelado, deseja enviar o protocolo de cancelamento por e-mail ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit;
      end;
  end
  else
  begin
      case MessageDlg('Desejar enviar a NF-e por e-mail ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;
  end;

  objEmiteNfe := TfrmEmiteNfe2.Create(nil);

  try
      try
          objEmiteNfe.preparaNFe();

          case (qryMasternCdStatus.Value) of
              1 : objEmiteNfe.enviaEmailNFe(qryMasternCdDoctoFiscal.Value, qryEnvioNFeEmailcEmailNFe.Value, qryEnvioNFeEmailcEmailCopiaNFe.Value);
              2 : objEmiteNfe.enviaEmailCancNFe(qryMasternCdDoctoFiscal.Value, qryEnvioNFeEmailcEmailNFe.Value, qryEnvioNFeEmailcEmailCopiaNFe.Value);
          end;
      except
          MensagemErro('N�o foi poss�vel enviar a NF-e por e-mail, verifique as configura��es de email e/ou sua conex�o.');
          Abort;
      end;
  finally
      FreeAndNil(objEmiteNfe);
  end;

  if not(Trim(qryEnvioNFeEmailcEmailCopiaNFe.Value) = '') then
      ShowMessage('NF-e enviada por e-mail com sucesso'+#13#13+'Email de destino: '+qryEnvioNFeEmailcEmailNFe.Value + ' e ' + qryEnvioNFeEmailcEmailCopiaNFe.Value)
  else
      ShowMessage('NF-e enviada por e-mail com sucesso'+#13#13+'Email de destino: '+qryEnvioNFeEmailcEmailNFe.Value) ;
end;

procedure TfrmDoctoFiscal.GeraNFeClick(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) then
      frmEmiteNfe2.gerarNFe(qryMasternCdDoctoFiscal.Value);
end;

procedure TfrmDoctoFiscal.cxButton1Click(Sender: TObject);
var
  objEmiteNFe :  TfrmEmiteNfe2;
  objEmiteNF  :  TfrmImpDFPadrao;
  objSAT       : TfrmFuncoesSAT;
begin

  if not qryMaster.Active then
      exit ;

  if qryMasternCdStatus.Value <> 1 then
  begin
      MensagemAlerta('Documento fiscal cancelado.') ;
      exit ;
  end ;

  if    (qryMasteriNrDocto.Value = 0)
     or (    (qryMastercXMLNFe.Value   <> '')
         and (qryMastercChaveNFe.Value  = '')) then
  begin
      MensagemAlerta('O documento fiscal ainda n�o foi impresso pela primeira vez. Reimpress�o cancelada.') ;
      exit ;
  end ;

  objEmiteNFe :=  TfrmEmiteNfe2.Create(nil);
  objEmiteNF  :=  TfrmImpDFPadrao.Create(nil);
  objSAT      := TfrmFuncoesSAT.Create(Nil);

  Try
      Try

          if (qryMastercFlgCFe.Value = 0) then
          begin
              {-- nota fiscal eletronica --}
              if (qryMastercXMLNFe.Value <> '') then
              begin
                  if (qryMaster.Active) then
                      objEmiteNfe.ReimprimirDANFe(qryMasternCdDoctoFiscal.Value);
              end
              else {-- nota fiscal comum --}
              begin
                  objEmiteNf.nCdDoctoFiscal := qryMasternCdDoctoFiscal.Value ;
                  objEmiteNf.ImprimirDocumento;
              end ;

          end
          else
              objSAT.prImpExtratoVenda(qryMasternCdDoctoFiscal.Value);
      except
          MensagemErro('Erro ao imprimir o documento fiscal.');
          raise;
      end;
  Finally
      FreeAndNil(objEmiteNfe);
      FreeAndNil(objEmiteNf);
      FreeAndNil(objSAT);
  end;

end;

procedure TfrmDoctoFiscal.cxButton2Click(Sender: TObject);
var
  objRel   : TrptRomaneio ;
  objRel_R : TrptRomaneio_Retrato ;
begin

  if not qryMaster.Active then
      exit ;

  if qryMasternCdStatus.Value <> 1 then
  begin
      MensagemAlerta('Documento fiscal cancelado.') ;
      exit ;
  end ;

  if (qryMasteriNrDocto.Value = 0) then
  begin
      MensagemAlerta('O documento fiscal ainda n�o foi impresso pela primeira vez.') ;
      exit ;
  end ;

  PosicionaQuery(qryRepresentante, qryItemDoctoFiscalnCdPedido.AsString);

  if (frmMenu.LeParametro('TPROMANEIONF') = 'P') then
  begin

      objRel := TrptRomaneio.Create(nil);
      Try
          Try
              PosicionaQuery(objRel.qryDoctoFiscal, qryMasternCdDoctoFiscal.AsString) ;
              objRel.lblEmpresa.Caption       := frmMenu.cNmEmpresaAtiva ;
              objRel.lblRepresentante.Caption := qryRepresentantecNmTerceiro.Value;
              objRel.QuickRep1.PreviewModal;
          except
              MensagemErro('Erro na cria��o do relat�rio');
              raise;
          end;
      Finally
          FreeAndNil(objRel);
      end;
  end else
  begin
      objRel_R := TrptRomaneio_Retrato.Create(nil);
      Try
          Try
              PosicionaQuery(objRel_R.qryDoctoFiscal, qryMasternCdDoctoFiscal.AsString) ;
              objRel_R.lblEmpresa.Caption       := frmMenu.cNmEmpresaAtiva ;
              objRel_R.lblRepresentante.Caption := qryRepresentantecNmTerceiro.Value;
              objRel_R.QuickRep1.PreviewModal;
          except
              MensagemErro('Erro na cria��o do relat�rio');
              raise;
          end;
      Finally
          FreeAndNil(objRel_R);
      end;
  end;

end;

procedure TfrmDoctoFiscal.FormShow(Sender: TObject);
begin
  inherited;
  DBGridEh1.Columns[DBGridEh1.FieldColumns['nQtde'].Index].DisplayFormat        := ',0' ;
  DBGridEh1.Columns[DBGridEh1.FieldColumns['nValUnitario'].Index].DisplayFormat := '#,####0.0000' ;

end;

procedure TfrmDoctoFiscal.cxButton4Click(Sender: TObject);
var
  objForm : TfrmCartaCorrecaoNFe_HistoricoCCe;
begin
  inherited;

  if (not qryMaster.Active) then
      Exit;

  try
      objForm := TfrmCartaCorrecaoNFe_HistoricoCCe.Create(nil);

      PosicionaQuery(objForm.qryCartaCorrecaoNFe, qryMasternCdDoctoFiscal.AsString);

      showForm(objForm, True);
  except
      MensagemErro('Erro no processamento.');
      Raise;
      Abort;
  end;
end;

procedure TfrmDoctoFiscal.cxButton5Click(Sender: TObject);
var
  objCCe : TfrmEmiteNFe2;
begin
  //inherited;

  if (not qryMaster.Active) then
      Exit;

  if (qryMasternCdStatus.Value <> 1) then
  begin
      MensagemAlerta('Documento fiscal cancelado.');
      Exit;
  end;

  { -- consulta �ltima CC-e gerada -- }
  qryCartaCorrecaoNFe.Close;
  qryCartaCorrecaoNFe.Parameters.ParamByName('nCdDoctoFiscal').Value := qryMasternCdDoctoFiscal.Value;
  qryCartaCorrecaoNFe.Open;

  if (qryCartaCorrecaoNFe.IsEmpty) then
  begin
      MensagemAlerta('Documento fiscal n�o possui carta de corre��o.');
      Abort;
  end;

  try
      try
          objCCe := TfrmEmiteNFe2.Create(nil);

          objCCe.imprimirCCe(qryCartaCorrecaoNFenCdCartaCorrecaoNFe.Value);
      except
          MensagemErro('Erro ao imprimir o documento fiscal.');
          Raise;
      end;
  finally
      FreeAndNil(objCCe);
  end;
end;

procedure TfrmDoctoFiscal.GerarXML1Click(Sender: TObject);
begin
  if (qryMaster.Eof) then
      Exit;

  prGerarXMLNFe(qryMasternCdDoctoFiscal.Value);
end;

procedure TfrmDoctoFiscal.prGerarXMLNFe(nCdDoctoFiscal : Integer; bGeraArqAtual : Boolean = True);
var
  cPathNFe : String;
begin
  inherited;

  PosicionaQuery(qryDoctoFiscalAux, IntToStr(nCdDoctoFiscal));

  if (qryDoctoFiscalAux.IsEmpty) then
      Exit;

  { -- verfica se documento fiscal possui chave v�lida -- }
  if (qryDoctoFiscalAuxcChaveNFe.Value = '') then
  begin
      MensagemAlerta('Documento fiscal deve possuir chave v�lida para gera��o do arquivo XML.');
      Exit;
  end;

  if (qryDoctoFiscalAuxnCdStatus.Value <> 1) then
  begin
      MensagemAlerta('Documento fiscal cancelado.');
      Exit;
  end;

  cPathNFe := frmMenu.cPathSistema + qryDoctoFiscalAuxcCaminhoXML.Value;

  if (FileExists(cPathNFe + qryDoctoFiscalAuxcArquivoXML.Value)) then
  begin
      ShowMessage('Arquivo XML j� existe. ' + #13 + 'Dir.: ' + cPathNFe);
      Exit;
  end;

  if (bGeraArqAtual) then
  begin
      case MessageDlg('Confirma gera��o do XML ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo    : Abort;
          mrCancel: Abort;
      end;
  end;

  try
      { -- gera arquivo xml -- }
      qryDoctoFiscalAuxcXMLNFe.SaveToFile(cPathNFe + qryDoctoFiscalAuxcArquivoXML.Value);

      { -- atualiza novo caminho -- }
      {qryDoctoFiscalAux.Edit;
      qryDoctoFiscalAuxcCaminhoXML.Value := cPathNFe;
      qryDoctoFiscalAux.Post;}
  except
      MensagemErro('Erro no processamento.');
      Raise;
  end;

  if (bGeraArqAtual) then
      ShowMessage('Arquivo XML gerado com sucesso.');
end;

procedure TfrmDoctoFiscal.qryMasterAfterOpen(DataSet: TDataSet);
begin
  inherited;

  { -- inclus�o do campo nome usu�rio impress�o (Rog�rio) -- }
  qryUsuario.Close;
  qryUsuario.Parameters.ParamByName('nPK').Value := qryMasternCdUsuarioImpr.Value;
  qryUsuario.Open;
end;

procedure TfrmDoctoFiscal.GerarPDF1Click(Sender: TObject);
var
 objForm  : TfrmEmiteNFe2;
 cPathPDF : String;
begin
  inherited;

  if (qryMaster.Eof) then
      Exit;

  PosicionaQuery(qryDoctoFiscalAux, qryMasternCdDoctoFiscal.AsString);

  if (qryDoctoFiscalAux.IsEmpty) then
      Exit;

  { -- verfica se documento fiscal possui chave v�lida -- }
  if (qryDoctoFiscalAuxcChaveNFe.Value = '') then
  begin
      MensagemAlerta('Documento fiscal deve possuir chave v�lida para gera��o do arquivo XML.');
      Exit;
  end;

  if (qryDoctoFiscalAuxnCdStatus.Value <> 1) then
  begin
      MensagemAlerta('Documento fiscal cancelado.');
      Exit;
  end;

  { -- verfica se documento fiscal possui chave v�lida -- }
  if (DBEdit6.Text = '') then
  begin
      MensagemAlerta('Documento fiscal deve possuir chave v�lida para gera��o do arquivo PDF.');
      Exit;
  end;

  if qryMasternCdStatus.Value <> 1 then
  begin
      MensagemAlerta('Documento fiscal cancelado.');
      Exit;
  end;

  cPathPDF := frmMenu.cPathSistema + StringReplace(qryDoctoFiscalAuxcCaminhoXML.Value, 'NFeAtivo\', '',[rfIgnoreCase]);

  { -- verifica exist�ncia do arquivo xml para gera��o do pdf -- }
  if (not FileExists(cPathPDF + 'NFeAtivo\' + qryDoctoFiscalAuxcChaveNFe.Value + '-nfe.xml')) then
  begin
      MensagemAlerta('Arquivo XML n�o encontrado.');
      Exit;
  end;

  { -- verifica exist�ncia do pdf na pasta (PDF) -- }
  if FileExists(cPathPDF + 'PDF\' + qryDoctoFiscalAuxcChaveNFe.Value + '.pdf') then
  begin
      ShowMessage('Arquivo PDF j� existe. ' +  #13 + 'Dir.: ' + cPathPDF + 'PDF\');
      Exit;
  end;

  case MessageDlg('Confirma gera��o do PDF ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo    : Abort;
      mrCancel: Abort;
  end;

  try
      try
          { -- prepara ambiente -- }
          objForm := TfrmEmiteNFe2.Create(nil);
          objForm.preparaNFe;

          { -- converte arquivo xml para pdf --}
          objForm.ACBrNFe1.NotasFiscais.LoadFromFile(cPathPDF + 'NFeAtivo\' + qryDoctoFiscalAuxcChaveNFe.Value + '-nfe.xml');
          objForm.ACBrNFeDANFeRL1.PathPDF := cPathPDF + 'PDF\';
          objForm.ACBrNFe1.NotasFiscais.ImprimirPDF;

          ShowMessage('Arquivo PDF gerado com sucesso.' +  #13 + 'Dir.: ' + cPathPDF + 'PDF\');
      except
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      FreeAndNil(objForm);
  end;
end;

procedure TfrmDoctoFiscal.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryUsuario.Close;
end;

procedure TfrmDoctoFiscal.btEnviaDocMonitorWebClick(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (frmMenu.LeParametro('REPLWEBATIVA') <> 'S') then
  begin
      MensagemErro('Replica��o web n�o esta ativa.');
      Exit;
  end;

  if (qryMastercChaveNFe.Value = '') then
  begin
      MensagemAlerta('Imprima o documento fiscal para utilizar esta fun��o.');
      Exit;
  end;

  try
      qryEnviaPedidoWebMonitor.Close;
      qryEnviaPedidoWebMonitor.Parameters.ParamByName('nCdDoctoFiscal').Value := qryMasternCdDoctoFiscal.Value;
      qryEnviaPedidoWebMonitor.ExecSQL;

      ShowMessage('Documento enviado para o monitor de transmiss�o web.');
  except on E : Exception do
      MensagemErro(E.Message);
  end;
end;

procedure TfrmDoctoFiscal.cxButton6Click(Sender: TObject);
var
    objForm : TfrmModImpETP;
    arrPed  : array of integer;
begin
    inherited;

    if (not qryMaster.Active) then
        Exit;

    try
        objForm := TfrmModImpETP.Create(nil);

        SetLength(arrPed,1);

        arrPed[0] := qryMasternCdDoctoFiscal.Value;

        objForm.emitirEtiquetas(arrPed,2);
    finally
        FreeAndNil(objForm);
    end;
end;

initialization
    RegisterClass(TfrmDoctoFiscal) ;

end.
