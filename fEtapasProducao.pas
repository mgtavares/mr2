unit fEtapasProducao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, DBCtrls, ER2Lookup, StdCtrls, Mask;

type
  TfrmEtapasProducao = class(TfrmCadastro_Padrao)
    qryMasternCdEtapaProducao: TIntegerField;
    qryMastercNmEtapaProducao: TStringField;
    qryMasternCdGrupoCalculoTempo: TIntegerField;
    qryMastercFlgServicoExterno: TIntegerField;
    qryMastercFlgGeraPedidoComercial: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    edtGrupoCalcTempo: TER2LookupDBEdit;
    qryGrupoCalcTempo: TADOQuery;
    DBLookupComboBox1: TDBLookupComboBox;
    DBLookupComboBox2: TDBLookupComboBox;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    qryGrupoCalcTemponCdGrupoCalculoTempo: TIntegerField;
    qryGrupoCalcTempocNmGrupoCalculoTempo: TStringField;
    DBEdit3: TDBEdit;
    dsGrupoCalcTempo: TDataSource;
    dsServicoExterno: TDataSource;
    dsGeraPedido: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforeClose(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEtapasProducao: TfrmEtapasProducao;

implementation

{$R *.dfm}

procedure TfrmEtapasProducao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'ETAPAPRODUCAO' ;
  nCdTabelaSistema  := 315 ;
  nCdConsultaPadrao := 741 ;
  bCodigoAutomatico := false ;
end;

procedure TfrmEtapasProducao.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

procedure TfrmEtapasProducao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryGrupoCalcTempo.Close;
  PosicionaQuery(qryGrupoCalcTempo,qryMasternCdGrupoCalculoTempo.AsString);
  
end;

procedure TfrmEtapasProducao.qryMasterBeforeClose(DataSet: TDataSet);
begin
  inherited;

  qryGrupoCalcTempo.Close;
end;

procedure TfrmEtapasProducao.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasternCdEtapaProducao.Value = 0) then
  begin
      MensagemAlerta('Informe o C�digo da Etapa de Produ��o');
      DBEdit1.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe o Descri��o da Etapa de Produ��o');
      DBEdit2.SetFocus;
      Abort;
  end;

  if (qryMasternCdGrupoCalculoTempo.Value = 0) then
  begin
      MensagemAlerta('Informe o Grupo de Calculo de Tempo');
      edtGrupoCalcTempo.SetFocus;
      Abort;
  end;

  inherited;

end;

initialization
    RegisterClass(TfrmEtapasProducao);
end.
