inherited frmMotivoCancelaSaldoPedido: TfrmMotivoCancelaSaldoPedido
  Left = 206
  Top = 202
  Width = 686
  Height = 123
  Caption = 'Motivo Cancelamento Saldo Pedido'
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 670
    Height = 58
  end
  object Label1: TLabel [1]
    Left = 8
    Top = 40
    Width = 168
    Height = 13
    Caption = 'Motivo do Cancelamento do Pedido'
  end
  inherited ToolBar1: TToolBar
    Width = 670
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object comboMotivos: TDBLookupComboBox [3]
    Left = 8
    Top = 56
    Width = 649
    Height = 21
    KeyField = 'nCdMotivoCancSaldoPed'
    ListField = 'cNmMotivoCancSaldoPed'
    ListSource = dsMotivo
    TabOrder = 1
  end
  inherited ImageList1: TImageList
    Left = 264
    Top = 32
  end
  object qryMotivo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdPedido int'
      ''
      'Set @nCdPedido = :nCdPedido'
      ''
      'SELECT *'
      '  FROM MotivoCancSaldoPed'
      ' WHERE nCdTabTipoPedido = (SELECT TipoPedido.nCdTabTipoPedido'
      '                             FROM Pedido'
      
        '                                  INNER JOIN TipoPedido ON TipoP' +
        'edido.nCdTipoPedido = Pedido.nCdTipoPedido'
      '                            WHERE Pedido.nCdPedido = @nCdPedido)'
      ' ORDER BY cNmMotivoCancSaldoPed                            ')
    Left = 304
    Top = 40
    object qryMotivonCdMotivoCancSaldoPed: TIntegerField
      FieldName = 'nCdMotivoCancSaldoPed'
    end
    object qryMotivocNmMotivoCancSaldoPed: TStringField
      FieldName = 'cNmMotivoCancSaldoPed'
      Size = 50
    end
    object qryMotivonCdTabTipoPedido: TIntegerField
      FieldName = 'nCdTabTipoPedido'
    end
  end
  object dsMotivo: TDataSource
    DataSet = qryMotivo
    Left = 336
    Top = 40
  end
  object SP_CANCELA_PEDIDO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CANCELA_PEDIDO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdMotivoCancSaldoPed'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 416
    Top = 32
  end
  object SP_CANCELA_ITEM_PEDIDO_MOTIVO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CANCELA_ITEM_PEDIDO_MOTIVO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdItemPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdMotivoCancSaldoPed'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end>
    Left = 472
    Top = 32
  end
end
