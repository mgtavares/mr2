inherited frmConciliaCartao: TfrmConciliaCartao
  Left = 72
  Top = 68
  Width = 1253
  Height = 656
  Caption = 'Concilia'#231#227'o Extrato Cart'#227'o'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1245
    Height = 600
  end
  inherited ToolBar1: TToolBar
    Width = 1245
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1245
    Height = 600
    ActivePage = cxTabSheet1
    Align = alClient
    Focusable = False
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 600
    ClientRectRight = 1245
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'Pr'#233'-Concilia'#231#227'o'
      ImageIndex = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1245
        Height = 97
        Align = alTop
        Caption = ' Filtro '
        TabOrder = 0
        object Label5: TLabel
          Tag = 1
          Left = 73
          Top = 24
          Width = 20
          Height = 13
          Alignment = taRightJustify
          Caption = 'Loja'
        end
        object Label3: TLabel
          Tag = 1
          Left = 496
          Top = 23
          Width = 109
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo de Vencimento'
        end
        object Label1: TLabel
          Tag = 1
          Left = 9
          Top = 48
          Width = 84
          Height = 13
          Alignment = taRightJustify
          Caption = 'Grupo Operadora'
        end
        object Label2: TLabel
          Tag = 1
          Left = 41
          Top = 72
          Width = 52
          Height = 13
          Alignment = taRightJustify
          Caption = 'Operadora'
        end
        object Label4: TLabel
          Tag = 1
          Left = 501
          Top = 47
          Width = 104
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo de Transa'#231#227'o'
        end
        object Label6: TLabel
          Tag = 1
          Left = 708
          Top = 71
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = 'Parcela'
        end
        object Label7: TLabel
          Tag = 1
          Left = 529
          Top = 72
          Width = 76
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'mero do Lote'
        end
        object Label8: TLabel
          Tag = 1
          Left = 775
          Top = 68
          Width = 56
          Height = 13
          Alignment = taRightJustify
          Caption = 'N'#250'm. Docto'
        end
        object edtLoja: TMaskEdit
          Left = 96
          Top = 16
          Width = 60
          Height = 21
          EditMask = '######;1; '
          MaxLength = 6
          TabOrder = 0
          Text = '      '
          OnExit = edtLojaExit
          OnKeyDown = edtLojaKeyDown
        end
        object edtDtVencIni: TMaskEdit
          Left = 608
          Top = 15
          Width = 81
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 3
          Text = '  /  /    '
        end
        object edtGrupoOperadora: TMaskEdit
          Left = 96
          Top = 40
          Width = 60
          Height = 21
          EditMask = '######;1; '
          MaxLength = 6
          TabOrder = 1
          Text = '      '
          OnExit = edtGrupoOperadoraExit
          OnKeyDown = edtGrupoOperadoraKeyDown
        end
        object edtOperadora: TMaskEdit
          Left = 96
          Top = 64
          Width = 60
          Height = 21
          EditMask = '######;1; '
          MaxLength = 6
          TabOrder = 2
          Text = '      '
          OnExit = edtOperadoraExit
          OnKeyDown = edtOperadoraKeyDown
        end
        object edtDtVencFim: TMaskEdit
          Left = 696
          Top = 15
          Width = 77
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 4
          Text = '  /  /    '
        end
        object edtDtTransacaoFim: TMaskEdit
          Left = 696
          Top = 39
          Width = 77
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 6
          Text = '  /  /    '
        end
        object edtDtTransacaoIni: TMaskEdit
          Left = 608
          Top = 39
          Width = 81
          Height = 21
          EditMask = '!99/99/9999;1;_'
          MaxLength = 10
          TabOrder = 5
          Text = '  /  /    '
        end
        object edtNrParcela: TMaskEdit
          Left = 750
          Top = 63
          Width = 23
          Height = 21
          EditMask = '##;1; '
          MaxLength = 2
          TabOrder = 8
          Text = '  '
        end
        object edtNrLote: TMaskEdit
          Left = 608
          Top = 64
          Width = 81
          Height = 21
          MaxLength = 15
          TabOrder = 7
        end
        object DBEdit1: TDBEdit
          Tag = 1
          Left = 159
          Top = 16
          Width = 321
          Height = 21
          DataField = 'cNmLoja'
          DataSource = dsLoja
          TabOrder = 11
        end
        object DBEdit2: TDBEdit
          Tag = 1
          Left = 159
          Top = 40
          Width = 321
          Height = 21
          DataField = 'cNmGrupoOperadoraCartao'
          DataSource = dsGrupoOperadora
          TabOrder = 12
        end
        object DBEdit3: TDBEdit
          Tag = 1
          Left = 159
          Top = 64
          Width = 321
          Height = 21
          DataField = 'cNmOperadoraCartao'
          DataSource = dsOperadora
          TabOrder = 13
        end
        object cxButton1: TcxButton
          Left = 920
          Top = 58
          Width = 121
          Height = 25
          Caption = 'Exibir &Transa'#231#245'es'
          TabOrder = 10
          OnClick = cxButton1Click
          Glyph.Data = {
            36030000424D360300000000000036000000280000000F000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
            393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
            00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
            F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
            83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
            68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
            FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
            9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
            8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
            D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
            B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
            5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
            A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
            9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
            488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
            FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
            FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
            FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
          LookAndFeel.NativeStyle = True
        end
        object edtNrDocto: TMaskEdit
          Left = 834
          Top = 60
          Width = 80
          Height = 21
          EditMask = '#########;1; '
          MaxLength = 9
          TabOrder = 9
          Text = '         '
        end
        object rgOrigemTransacao: TRadioGroup
          Left = 834
          Top = 10
          Width = 207
          Height = 41
          Caption = ' Origem da Transa'#231#227'o '
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Interna'
            'Web')
          TabOrder = 14
        end
        object rgConciliacaoAut: TRadioGroup
          Left = 1050
          Top = 10
          Width = 167
          Height = 72
          Caption = ' Concilia'#231#227'o Autom'#225'tica '
          TabOrder = 15
        end
        object btGerarArquivo: TcxButton
          Left = 1063
          Top = 31
          Width = 140
          Height = 39
          Caption = 'Gerar Arquivo'
          TabOrder = 16
          OnClick = btGerarArquivoClick
          Glyph.Data = {
            EE030000424DEE03000000000000360000002800000012000000110000000100
            180000000000B803000000000000000000000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFF000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00000080808000
            0000000000000000808080000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF808080000000DEBED4FFFFFFFFFFFF
            000000808080FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            000000000000000000000000DEBED4DEBED4000000DEBED4FFFFFF0000000000
            00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFF0000009999999999999999
            99000000FFFFFF000000000000000000FFFFFF000000FFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF0000FFFFFF000000FFFFFFE6E6E6000000000000FFFFFFDE
            BED4000000DEBED4DEBED4000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            0000FFFFFF000000FFFFFFE6E6E6E6E6E6808080000000FFFFFFFFFFFFDEBED4
            0000008080804E7EA6FFFFFF4E7EA6FFFFFFFFFFFFFFFFFF0000FFFFFF000000
            FFFFFFE6E6E6E6E6E60000008080800000000000000000008080804E7EA60000
            000000000000004E7EA6FFFFFFFFFFFF0000FFFFFF000000FFFFFF008000E6E6
            E6E6E6E6E6E6E60000009999990000004E7EA60000004E7EA64E7EA64E7EA600
            00004E7EA6FFFFFF0000FFFFFF000000FFFFFF008000008000E6E6E6E6E6E6E6
            E6E6999999000000FFFFFF0000004E7EA60000004E7EA6000000FFFFFFFFFFFF
            0000FFFFFF000000FFFFFF008000008000008000E6E6E6E6E6E6999999000000
            4E7EA60000004E7EA64E7EA64E7EA60000004E7EA6FFFFFF0000FFFFFF000000
            FFFFFF008000008000E6E6E6E6E6E6E6E6E6999999000000FFFFFF4E7EA60000
            000000000000004E7EA6FFFFFFFFFFFF0000FFFFFF000000FFFFFF008000E6E6
            E6E6E6E6E6E6E6E6E6E6999999000000FFFFFFFFFFFF4E7EA6FFFFFF4E7EA6FF
            FFFFFFFFFFFFFFFF0000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            0000FFFFFFFFFFFF000000000000000000000000000000000000000000FFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000}
          LookAndFeel.NativeStyle = True
        end
      end
      object StaticText1: TStaticText
        Left = 0
        Top = 97
        Width = 1245
        Height = 17
        Align = alTop
        Alignment = taCenter
        BorderStyle = sbsSingle
        Caption = 
          'Parcelas Pendentes                                              ' +
          '                                                                ' +
          '                                                                ' +
          '                                  Parcelas Selecionadas'
        TabOrder = 1
      end
      object cxGrid1: TcxGrid
        Left = 640
        Top = 114
        Width = 625
        Height = 407
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnDblClick = cxGrid1DBTableView1DblClick
          DataController.DataSource = dsTransacaoSelecionada
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'nSaldoTit'
              Column = cxGrid1DBTableView1nSaldoTit
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'nValTit'
              Column = cxGrid1DBTableView1nValTit
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTit'
              Column = cxGrid1DBTableView1nValTit
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nSaldoTit'
              Column = cxGrid1DBTableView1nSaldoTit
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GridLines = glVertical
          OptionsView.GroupFooters = gfAlwaysVisible
          object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTitulo'
            Visible = False
          end
          object cxGrid1DBTableView1nCdTransacaoCartao: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTransacaoCartao'
            Visible = False
          end
          object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
            Caption = 'Dt. Vencimento'
            DataBinding.FieldName = 'dDtVenc'
            Width = 90
          end
          object cxGrid1DBTableView1dDtTransacao: TcxGridDBColumn
            Caption = 'Dt. Transa'#231#227'o'
            DataBinding.FieldName = 'dDtTransacao'
            Width = 100
          end
          object cxGrid1DBTableView1DBcNmOperadoraCartao: TcxGridDBColumn
            Caption = 'Op. Cart'#227'o'
            DataBinding.FieldName = 'cNmOperadoraCartao'
            Width = 100
          end
          object cxGrid1DBTableView1DBiNrDocto: TcxGridDBColumn
            Caption = 'Docto.'
            DataBinding.FieldName = 'iNrDocto'
            Width = 65
          end
          object cxGrid1DBTableView1DBiNrAutorizacao: TcxGridDBColumn
            Caption = 'Aut.'
            DataBinding.FieldName = 'iNrAutorizacao'
            Width = 65
          end
          object cxGrid1DBTableView1cNrLote: TcxGridDBColumn
            Caption = 'Lote'
            DataBinding.FieldName = 'cNrLote'
            Width = 58
          end
          object cxGrid1DBTableView1cParcela: TcxGridDBColumn
            Caption = 'Parc.'
            DataBinding.FieldName = 'cParcela'
            Width = 60
          end
          object cxGrid1DBTableView1nCdOperadora: TcxGridDBColumn
            DataBinding.FieldName = 'nCdOperadora'
            Visible = False
          end
          object cxGrid1DBTableView1cNmOperadora: TcxGridDBColumn
            Caption = 'Operadora'
            DataBinding.FieldName = 'cNmOperadora'
            Visible = False
            GroupIndex = 0
          end
          object cxGrid1DBTableView1nCdGrupoOperadora: TcxGridDBColumn
            DataBinding.FieldName = 'nCdGrupoOperadora'
            Visible = False
          end
          object cxGrid1DBTableView1cNmGrupoOperadora: TcxGridDBColumn
            DataBinding.FieldName = 'cNmGrupoOperadora'
            Visible = False
          end
          object cxGrid1DBTableView1nValTit: TcxGridDBColumn
            Caption = 'Valor'
            DataBinding.FieldName = 'nValTit'
            Width = 66
          end
          object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
            Caption = 'L'#237'quido'
            DataBinding.FieldName = 'nSaldoTit'
            Width = 66
          end
          object cxGrid1DBTableView1dDtVencOriginal: TcxGridDBColumn
            DataBinding.FieldName = 'dDtVencOriginal'
            Visible = False
          end
          object cxGrid1DBTableView1nSaldoTitOriginal: TcxGridDBColumn
            DataBinding.FieldName = 'nSaldoTitOriginal'
            Visible = False
          end
          object cxGrid1DBTableView1cStatus: TcxGridDBColumn
            DataBinding.FieldName = 'cStatus'
            Visible = False
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
      object cxGrid2: TcxGrid
        Left = 0
        Top = 114
        Width = 625
        Height = 407
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        object cxGridDBTableView1: TcxGridDBTableView
          PopupMenu = PopupMenu1
          OnDblClick = cxGridDBTableView1DblClick
          OnDragOver = cxGridDBTableView1DragOver
          DataController.DataSource = dsTransacaoPendente
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'nValTit'
              Column = cxGridDBTableView1nValTit
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'nSaldoTit'
              Column = cxGridDBTableView1nSaldoTit
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTit'
              Column = cxGridDBTableView1nValTit
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nSaldoTit'
              Column = cxGridDBTableView1nSaldoTit
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnHidingOnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GridLines = glVertical
          OptionsView.GroupFooters = gfAlwaysVisible
          object cxGridDBTableView1nCdTitulo: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTitulo'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBTableView1nCdTransacaoCartao: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTransacaoCartao'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBTableView1dDtVenc: TcxGridDBColumn
            Caption = 'Dt. Vencimento'
            DataBinding.FieldName = 'dDtVenc'
            Width = 90
          end
          object cxGridDBTableView1dDtTransacao: TcxGridDBColumn
            Caption = 'Dt. Transa'#231#227'o'
            DataBinding.FieldName = 'dDtTransacao'
            Options.Editing = False
            Width = 100
          end
          object cxGridDBTableView1DBcNmOperadoraCartao: TcxGridDBColumn
            Caption = 'Op.Cart'#227'o'
            DataBinding.FieldName = 'cNmOperadoraCartao'
            Width = 100
          end
          object cxGridDBTableView1DBiNrDocto: TcxGridDBColumn
            Caption = 'Docto.'
            DataBinding.FieldName = 'iNrDocto'
            Width = 65
          end
          object cxGridDBTableView1DBiNrAutorizacao: TcxGridDBColumn
            Caption = 'Aut.'
            DataBinding.FieldName = 'iNrAutorizacao'
            Width = 65
          end
          object cxGridDBTableView1cNrLote: TcxGridDBColumn
            Caption = 'Lote'
            DataBinding.FieldName = 'cNrLote'
            Options.Editing = False
            Width = 58
          end
          object cxGridDBTableView1cParcela: TcxGridDBColumn
            Caption = 'Parc.'
            DataBinding.FieldName = 'cParcela'
            Options.Editing = False
            Width = 60
          end
          object cxGridDBTableView1nCdOperadora: TcxGridDBColumn
            DataBinding.FieldName = 'nCdOperadora'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBTableView1cNmOperadora: TcxGridDBColumn
            Caption = 'Operadora'
            DataBinding.FieldName = 'cNmOperadora'
            Visible = False
            GroupIndex = 0
            Options.Editing = False
          end
          object cxGridDBTableView1nCdGrupoOperadora: TcxGridDBColumn
            DataBinding.FieldName = 'nCdGrupoOperadora'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBTableView1cNmGrupoOperadora: TcxGridDBColumn
            DataBinding.FieldName = 'cNmGrupoOperadora'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBTableView1nValTit: TcxGridDBColumn
            Caption = 'Valor'
            DataBinding.FieldName = 'nValTit'
            Options.Editing = False
            Width = 66
          end
          object cxGridDBTableView1nSaldoTit: TcxGridDBColumn
            Caption = 'L'#237'quido'
            DataBinding.FieldName = 'nSaldoTit'
            Width = 66
          end
          object cxGridDBTableView1dDtVencOriginal: TcxGridDBColumn
            DataBinding.FieldName = 'dDtVencOriginal'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBTableView1nSaldoTitOriginal: TcxGridDBColumn
            DataBinding.FieldName = 'nSaldoTitOriginal'
            Visible = False
            Options.Editing = False
          end
          object cxGridDBTableView1cStatus: TcxGridDBColumn
            DataBinding.FieldName = 'cStatus'
            Visible = False
            Options.Editing = False
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
      object cxButton2: TcxButton
        Left = 640
        Top = 528
        Width = 130
        Height = 33
        Caption = 'Pr'#233' Conciliar'
        TabOrder = 4
        OnClick = cxButton2Click
        Glyph.Data = {
          66060000424D6606000000000000360000002800000017000000160000000100
          1800000000003006000000000000000000000000000000000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F1F1F1
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F1
          F1F1F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F090490A90490A90490A90490A9049
          0A90490A90490AF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F098551ADAA525
          CC8D21CC8D21CC8D21DAA525944D0EF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F09D5920DBA82FCE912ACE912ACE912ADBA82F975212F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0A05E26DCAB39D09533D09533D09533DCAB399B5618F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0A6642CDEAE43D2993CD2993CD299
          3CDEAE43A05C1DF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0A96A33FAE062
          FAE062FAE062FAE062FAE062A46223F2F2F2F0F0F0F1F1F1F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0A36021AA672AAA672AAA672AAA672AAA672AAA672AF0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0001FFFF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0001FFF001FFF001FFFF0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFF001FFF001FFF001FFF001FFFF0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFFF0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFF
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFFF0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0001FFFF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0001FFF
          F0F0F0F0F0F0001FFFF0F0F0001FFFF0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0000000F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F000
          0000F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0
          F0F0F0F0F0F0F0000000}
        LookAndFeel.NativeStyle = True
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Concilia'#231#227'o'
      ImageIndex = 1
      object cxGrid3: TcxGrid
        Left = 0
        Top = 48
        Width = 1245
        Height = 529
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = False
        object cxGrid3DBTableView1: TcxGridDBTableView
          DataController.DataSource = dsTransacaoConciliada
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'nValTit'
              Column = cxGrid3DBTableView1nValTit
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              Position = spFooter
              FieldName = 'nSaldoTit'
              Column = cxGrid3DBTableView1nSaldoTit
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTit'
              Column = cxGrid3DBTableView1nValTit
            end
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nSaldoTit'
              Column = cxGrid3DBTableView1nSaldoTit
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsCustomize.ColumnMoving = False
          OptionsCustomize.ColumnSorting = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GridLines = glVertical
          OptionsView.GroupFooters = gfAlwaysVisible
          Styles.Footer = frmMenu.Footer
          object cxGrid3DBTableView1nCdTitulo: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTitulo'
            Visible = False
          end
          object cxGrid3DBTableView1nCdTransacaoCartao: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTransacaoCartao'
            Visible = False
          end
          object cxGrid3DBTableView1dDtVenc: TcxGridDBColumn
            Caption = 'Dt. Vencimento'
            DataBinding.FieldName = 'dDtVenc'
            Visible = False
            GroupIndex = 1
          end
          object cxGrid3DBTableView1dDtTransacao: TcxGridDBColumn
            Caption = 'Dt. Transa'#231#227'o'
            DataBinding.FieldName = 'dDtTransacao'
          end
          object cxGrid3DBTableView1cNrLote: TcxGridDBColumn
            Caption = 'Lote'
            DataBinding.FieldName = 'cNrLote'
          end
          object cxGrid3DBTableView1cParcela: TcxGridDBColumn
            Caption = 'Parcela'
            DataBinding.FieldName = 'cParcela'
          end
          object cxGrid3DBTableView1nCdOperadora: TcxGridDBColumn
            DataBinding.FieldName = 'nCdOperadora'
            Visible = False
          end
          object cxGrid3DBTableView1cNmOperadora: TcxGridDBColumn
            Caption = 'Operadora'
            DataBinding.FieldName = 'cNmOperadora'
          end
          object cxGrid3DBTableView1nCdGrupoOperadora: TcxGridDBColumn
            DataBinding.FieldName = 'nCdGrupoOperadora'
            Visible = False
          end
          object cxGrid3DBTableView1cNmGrupoOperadora: TcxGridDBColumn
            Caption = 'Grupo de Operadora'
            DataBinding.FieldName = 'cNmGrupoOperadora'
            Visible = False
            GroupIndex = 0
          end
          object cxGrid3DBTableView1nValTit: TcxGridDBColumn
            Caption = 'Valor Bruto'
            DataBinding.FieldName = 'nValTit'
            Width = 122
          end
          object cxGrid3DBTableView1nSaldoTit: TcxGridDBColumn
            Caption = 'Valor L'#237'quido'
            DataBinding.FieldName = 'nSaldoTit'
            Width = 128
          end
          object cxGrid3DBTableView1dDtVencOriginal: TcxGridDBColumn
            DataBinding.FieldName = 'dDtVencOriginal'
            Visible = False
          end
          object cxGrid3DBTableView1nSaldoTitOriginal: TcxGridDBColumn
            DataBinding.FieldName = 'nSaldoTitOriginal'
            Visible = False
          end
          object cxGrid3DBTableView1cStatus: TcxGridDBColumn
            DataBinding.FieldName = 'cStatus'
            Visible = False
          end
        end
        object cxGrid3Level1: TcxGridLevel
          GridView = cxGrid3DBTableView1
        end
      end
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1245
        Height = 48
        Align = alTop
        TabOrder = 1
        object cxButton3: TcxButton
          Left = 4
          Top = 10
          Width = 130
          Height = 33
          Caption = '&Atualizar'
          TabOrder = 0
          OnClick = cxButton3Click
          Glyph.Data = {
            72020000424D720200000000000036000000280000000E0000000D0000000100
            1800000000003C020000000000000000000000000000000000008C9C9C000000
            0000000000000000000000000000000000000000000000000000000000000000
            008C9C9C00000000000000840000840000840000840000840000840000840000
            8400008400008400008400008400000000000000009C9CFF0000FF0000FF0000
            FFEFFFFF0000840000840000840000840000FF0000FF00008400000000000000
            009C9CFF0000FF0000FFEFFFFFEFFFFFEFFFFFEFFFFF00008400008400008400
            008400008400000000000000009C9CFF0000FFEFFFFFEFFFFFEFFFFFEFFFFFEF
            FFFFEFFFFFEFFFFF0000840000FF00008400000000000000009C9CFF0000FF00
            00FF0000FF0000FFEFFFFFEFFFFF0000840000840000FF0000FF000084000000
            00000000009C9CFF0000FF0000FF0000FF0000FF0000FFEFFFFFEFFFFF000084
            0000840000FF00008400000000000000009C9CFF0000FF0000FFEFFFFFEFFFFF
            EFFFFFEFFFFFEFFFFFEFFFFF0000FF0000FF00008400000000000000009C9CFF
            0000FF0000FF0000FFEFFFFFEFFFFF0000840000840000FF0000FF0000FF0000
            8400000000000000009C9CFF0000FF0000FF0000FF0000FFEFFFFFEFFFFF0000
            840000840000FF0000FF00008400000000000000009C9CFF0000FF0000FF0000
            FF0000FF0000FFEFFFFFEFFFFF0000FF0000FF0000FF00008400000000000000
            009C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C9CFF9C
            9CFF9C9CFF00000000008C9C9C00000000000000000000000000000000000000
            00000000000000000000000000000000008C9C9C0000}
          LookAndFeel.NativeStyle = True
        end
        object cxButton5: TcxButton
          Left = 137
          Top = 10
          Width = 130
          Height = 33
          Caption = '&Limpar Lista'
          TabOrder = 1
          OnClick = cxButton5Click
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C30E0000C30E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2E5F78090DD2B41BB1B
            30B1182AB0253DB4758DD7DDE3F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFB5BEEB2C3CC2192AC24B51E56A6EEC6A6EEC474EE01426B31C32AFA7B7
            E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8BEEE2936C3323BDC848CF4555FEE31
            39E73033E4545CEC8083F32933D0142AA9A7B7E4FFFFFFFFFFFFFFFFFFE5E8F9
            3B47CC3741DF8495F42B35EA2832E82630E72630E3252DE3282DE38086F32B36
            CF1C32B0DEE4F4FFFFFFFFFFFF959BE8242DD58693F62D3AEF2A3CEC2630E726
            30E72630E72630E7252FE32A30E38187F41428B7748CD7FFFFFFFFFFFF494ED8
            5966EC556DF73349F22630E72630E72630E72630E72630E72630E72731E7525B
            EF4B52DE253AB7FFFFFFFFFFFF3B3ED58493F7465FF83D5AF6FDFDFDFDFDFDFD
            FDFDFDFDFDFDFDFDFDFDFD2630E7323CE8747DF0172DAFFFFFFFFFFFFF3E44D7
            99A7F95670FA465DEFFFFFFFFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD2630E7343F
            EC747EF01A2CB1FFFFFFFFFFFF5356DF8A99F47E93FA4D66EF2B35EA2B35EA2B
            35EA2B35EA2B35EA2B35EA394DF15969F34E56E72D41BEFFFFFFFFFFFFA2A2EF
            545BE8C3CCFC657EFA5570F74B64EC425CF6425CF6425CF6425CF6394FF28999
            F81B27C78393DDFFFFFFFFFFFFE9E9FB4F4EDF878DF2CCD5FD607AFA566DEF51
            69F05671F8556EF94C66F88FA3F83944DF2F3DC3E3E5F7FFFFFFFFFFFFFFFFFF
            C6C6F64645DD9296F1D1D7FD98A9FA6D87FA657EFA7D90FBA9BBFC4652E72B38
            C4B5BEECFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6C6F64F4FDE6970EBAFB5F8C2
            CBF9B7C0F992A0F33C46DE3A44CFBABFEEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFE9E9FBA3A3EF5456DE3F41D93D41D74A4FD9999FE9E5E7F9FFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
        object cxButton6: TcxButton
          Left = 270
          Top = 10
          Width = 130
          Height = 33
          Caption = 'Efetivar &Concilia'#231#227'o'
          TabOrder = 2
          OnClick = cxButton6Click
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7FAF7F9FBF9FF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFF7FAF837833D347D3AF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8FBF8408E4754A35C4F9F5733
            7D39F8FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            F8FBF8499A515BAC6477CA8274C87E51A059347E3AF8FBF9FFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFF8FCF951A65A63B56D7ECE897BCC8776CA8176
            C98152A25A357F3BF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9FCFA59B063
            6BBD7684D2907AC98560B26A63B46D78C98378CB8253A35C36803CF9FBF9FFFF
            FFFFFFFFFFFFFFFFFFFFD3ECD66CBD7679C98680CE8D53A75CB2D6B59CC9A05C
            AD677CCC8679CB8554A45D37813DF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD9EFDC
            6CBD756DC079B5DBB9FFFFFFFFFFFF98C79D5EAE687DCD897CCD8756A55F3882
            3EF9FBF9FFFFFFFFFFFFFFFFFFFFFFFFD5EDD8BEE2C3FFFFFFFFFFFFFFFFFFFF
            FFFF99C89D5FAF697FCE8A7ECE8957A66039833FF9FBF9FFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF99C89E60B06A81CF8D7FCF
            8B58A761398540F9FBF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFF99C99E62B26C82D18F7AC88557A6609FC4A2FFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9F63B3
            6D5FAF69A5CBA9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFF9ACA9FA5CEA9FFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
            FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
          LookAndFeel.NativeStyle = True
        end
        object btLerArquivo: TcxButton
          Left = 403
          Top = 10
          Width = 130
          Height = 33
          Caption = '&Ler Arquivo Retorno'
          TabOrder = 3
          Visible = False
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            18000000000000030000C40E0000C40E00000000000000000000FF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            B3B3B3B3B3B3B3B3B3FF00FF2B2B2B2424241E1E1E1717171212120C0C0CFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFB3B3B3E4E4E4B3B3B3FF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            B4B4B4E5E5E5B3B3B3FF00FF3D3D3D3636362F2F2F2828282121211B1B1B1414
            140F0F0F989898FF00FFFF00FFFF00FFB6B6B6E6E6E6B3B3B3FF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            B8B8B8E8E8E8B5B5B5FF00FF4F4F4F4848484141413A3A3A3232322B2B2BFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFBABABAEBEBEBB7B7B7FF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            BBBBBBEDEDEDB9B9B9FF00FF6060605959595353534C4C4C4545453D3D3D3636
            362F2F2FA5A5A5FF00FFFF00FFFF00FFBCBCBCEFEFEFBBBBBBFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            BCBCBCF0F0F0BBBBBBFF00FF6D6D6D6868686363635C5C5C5656564F4F4FFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFBCBCBCF0F0F0BCBCBCFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            BCBCBCBCBCBCBCBCBCFF00FF7777777474747070706B6B6B6565656060605959
            59535353B4B4B4FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
          LookAndFeel.NativeStyle = True
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 392
    Top = 336
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdLoja    = Loja.nCdLoja'
      '                 AND UL.nCdUsuario = :nCdUsuario)')
    Left = 192
    Top = 365
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 192
    Top = 400
  end
  object qryGrupoOperadora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoOperadoraCartao, cNmGrupoOperadoraCartao'
      'FROM GrupoOperadoraCartao'
      'WHERE nCdGrupoOperadoraCartao = :nPK')
    Left = 224
    Top = 368
    object qryGrupoOperadoranCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryGrupoOperadoracNmGrupoOperadoraCartao: TStringField
      FieldName = 'cNmGrupoOperadoraCartao'
      Size = 50
    end
  end
  object dsGrupoOperadora: TDataSource
    DataSet = qryGrupoOperadora
    Left = 224
    Top = 400
  end
  object qryOperadora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdOperadoraCartao, cNmOperadoraCartao'
      'FROM OperadoraCartao'
      'WHERE nCdOperadoraCartao = :nPK')
    Left = 256
    Top = 368
    object qryOperadoranCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryOperadoracNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
  end
  object dsOperadora: TDataSource
    DataSet = qryOperadora
    Left = 256
    Top = 400
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempTransacoes'#39') IS NULL) '#13#10'BEGIN'#13#10#13#10#9'CR' +
      'EATE TABLE #TempTransacoes (nCdTitulo          int           pri' +
      'mary key'#13#10#9#9#9#9#9#9#9#9#9#9'  ,nCdTransacaoCartao int'#13#10#9#9#9#9#9#9#9#9#9#9'  ,dDtV' +
      'enc            datetime'#13#10#9#9#9#9#9#9#9#9#9#9'  ,dDtTransacao       datetim' +
      'e '#13#10#9#9#9#9#9#9#9#9#9#9'  ,cNrLote            char(15)'#13#10#9#9#9#9#9#9#9#9#9#9'  ,cParc' +
      'ela           varchar(20)'#13#10#9#9#9#9#9#9#9#9#9#9'  ,nCdOperadora       int'#13#10 +
      #9#9#9#9#9#9#9#9#9#9'  ,cNmOperadora       varchar(50)'#13#10#9#9#9#9#9#9#9#9#9#9'  ,nCdGru' +
      'poOperadora  int'#13#10#9#9#9#9#9#9#9#9#9#9'  ,cNmGrupoOperadora  varchar(50)'#13#10#9 +
      #9#9#9#9#9#9'              ,cNmOperadoraCartao varchar(50)'#13#10#9#9#9#9#9#9#9'    ' +
      '          ,iNrDocto           int'#13#10#9#9#9#9#9#9#9'              ,iNrAuto' +
      'rizacao     int'#13#10#9#9#9#9#9#9#9#9#9#9'  ,nValTit            decimal(12,2) d' +
      'efault 0 not null'#13#10#9#9#9#9#9#9#9#9#9#9'  ,nSaldoTit          decimal(12,2)' +
      ' default 0 not null'#13#10#9#9#9#9#9#9#9#9#9#9'  ,dDtVencOriginal    datetime'#13#10#9 +
      #9#9#9#9#9#9#9#9#9'  ,nSaldoTitOriginal  decimal(12,2) default 0 not null'#13 +
      #10#9#9#9#9#9#9#9#9#9#9'  ,cStatus            char(3), iParcela int)'#13#10#13#10'END'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 356
    Top = 333
  end
  object qryTransacaoPendente: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    BeforePost = qryTransacaoPendenteBeforePost
    AfterPost = qryTransacaoPendenteAfterPost
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM #TempTransacoes'
      'WHERE cStatus = '#39'PEN'#39)
    Left = 292
    Top = 373
    object qryTransacaoPendentenCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTransacaoPendentenCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTransacaoPendentedDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTransacaoPendentedDtTransacao: TDateTimeField
      FieldName = 'dDtTransacao'
    end
    object qryTransacaoPendentecNrLote: TStringField
      FieldName = 'cNrLote'
      FixedChar = True
      Size = 15
    end
    object qryTransacaoPendentecParcela: TStringField
      FieldName = 'cParcela'
    end
    object qryTransacaoPendentenCdOperadora: TIntegerField
      FieldName = 'nCdOperadora'
    end
    object qryTransacaoPendentecNmOperadora: TStringField
      FieldName = 'cNmOperadora'
      Size = 50
    end
    object qryTransacaoPendentenCdGrupoOperadora: TIntegerField
      FieldName = 'nCdGrupoOperadora'
    end
    object qryTransacaoPendentecNmGrupoOperadora: TStringField
      FieldName = 'cNmGrupoOperadora'
      Size = 50
    end
    object qryTransacaoPendentenValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTransacaoPendentenSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTransacaoPendentedDtVencOriginal: TDateTimeField
      FieldName = 'dDtVencOriginal'
    end
    object qryTransacaoPendentenSaldoTitOriginal: TBCDField
      FieldName = 'nSaldoTitOriginal'
      Precision = 12
      Size = 2
    end
    object qryTransacaoPendentecStatus: TStringField
      FieldName = 'cStatus'
      FixedChar = True
      Size = 3
    end
    object qryTransacaoPendenteiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTransacaoPendentecNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryTransacaoPendenteiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryTransacaoPendenteiNrAutorizacao: TIntegerField
      FieldName = 'iNrAutorizacao'
    end
  end
  object qryTransacaoSelecionada: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM #TempTransacoes'
      'WHERE cStatus = '#39'PRE'#39)
    Left = 324
    Top = 373
    object qryTransacaoSelecionadanCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTransacaoSelecionadanCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTransacaoSelecionadadDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTransacaoSelecionadadDtTransacao: TDateTimeField
      FieldName = 'dDtTransacao'
    end
    object qryTransacaoSelecionadacNrLote: TStringField
      FieldName = 'cNrLote'
      FixedChar = True
      Size = 15
    end
    object qryTransacaoSelecionadacParcela: TStringField
      FieldName = 'cParcela'
    end
    object qryTransacaoSelecionadanCdOperadora: TIntegerField
      FieldName = 'nCdOperadora'
    end
    object qryTransacaoSelecionadacNmOperadora: TStringField
      FieldName = 'cNmOperadora'
      Size = 50
    end
    object qryTransacaoSelecionadanCdGrupoOperadora: TIntegerField
      FieldName = 'nCdGrupoOperadora'
    end
    object qryTransacaoSelecionadacNmGrupoOperadora: TStringField
      FieldName = 'cNmGrupoOperadora'
      Size = 50
    end
    object qryTransacaoSelecionadanValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTransacaoSelecionadanSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTransacaoSelecionadadDtVencOriginal: TDateTimeField
      FieldName = 'dDtVencOriginal'
    end
    object qryTransacaoSelecionadanSaldoTitOriginal: TBCDField
      FieldName = 'nSaldoTitOriginal'
      Precision = 12
      Size = 2
    end
    object qryTransacaoSelecionadacStatus: TStringField
      FieldName = 'cStatus'
      FixedChar = True
      Size = 3
    end
    object qryTransacaoSelecionadaiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTransacaoSelecionadacNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryTransacaoSelecionadaiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryTransacaoSelecionadaiNrAutorizacao: TIntegerField
      FieldName = 'iNrAutorizacao'
    end
  end
  object dsTransacaoPendente: TDataSource
    DataSet = qryTransacaoPendente
    Left = 292
    Top = 405
  end
  object dsTransacaoSelecionada: TDataSource
    DataSet = qryTransacaoSelecionada
    Left = 324
    Top = 405
  end
  object qryTransacoes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdLoja'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdGrupoOperadoraCartao'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdOperadoraCartao'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtVencIni'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtVencFim'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtTransacaoIni'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtTransacaoFim'
        Size = -1
        Value = Null
      end
      item
        Name = 'cNrLotePOS'
        Size = -1
        Value = Null
      end
      item
        Name = 'iParcela'
        Size = -1
        Value = Null
      end
      item
        Name = 'iNrDocto'
        Size = -1
        Value = Null
      end
      item
        Name = 'cFlgOrigemTransacao'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      'SET NOCOUNT ON'
      ''
      'DECLARE @nCdEmpresa              int'
      '       ,@nCdLoja                 int'
      '       ,@nCdGrupoOperadoraCartao int'
      '       ,@nCdOperadoraCartao      int'
      '       ,@dDtVencIni              varchar(10)'
      '       ,@dDtVencFim              varchar(10)'
      '       ,@dDtTransacaoIni         varchar(10)'
      '       ,@dDtTransacaoFim         varchar(10)'
      '       ,@cNrLotePOS              char(15)'
      '       ,@iParcela                int'
      '       ,@iNrDocto                int'
      ''
      'Set @nCdEmpresa              = :nCdEmpresa'
      'Set @nCdLoja                 = :nCdLoja   '
      'Set @nCdGrupoOperadoraCartao = :nCdGrupoOperadoraCartao'
      'Set @nCdOperadoraCartao      = :nCdOperadoraCartao'
      'Set @dDtVencIni              = :dDtVencIni'
      'Set @dDtVencFim              = :dDtVencFim'
      'Set @dDtTransacaoIni         = :dDtTransacaoIni'
      'Set @dDtTransacaoFim         = :dDtTransacaoFim'
      'Set @cNrLotePOS              = :cNrLotePOS'
      'Set @iParcela                = :iParcela'
      'Set @iNrDocto                = :iNrDocto'
      ''
      'DELETE'
      '  FROM #TempTransacoes'
      ' WHERE cStatus = '#39'PEN'#39
      ''
      'INSERT INTO #TempTransacoes (nCdTitulo'
      #9#9#9#9#9#9#9',nCdTransacaoCartao'
      #9#9#9#9#9#9#9',dDtVenc'
      #9#9#9#9#9#9#9',dDtTransacao'
      #9#9#9#9#9#9#9',cNrLote'
      #9#9#9#9#9#9#9',cParcela'
      #9#9#9#9#9#9#9',nCdOperadora'
      #9#9#9#9#9#9#9',cNmOperadora'
      #9#9#9#9#9#9#9',nCdGrupoOperadora'
      #9#9#9#9#9#9#9',cNmGrupoOperadora'
      #9#9#9#9#9#9#9',cNmOperadoraCartao'
      #9#9#9#9#9#9#9',iNrDocto'
      #9#9#9#9#9#9#9',iNrAutorizacao'
      #9#9#9#9#9#9#9',nValTit'
      #9#9#9#9#9#9#9',nSaldoTit'
      #9#9#9#9#9#9#9',dDtVencOriginal'
      #9#9#9#9#9#9#9',nSaldoTitOriginal'
      #9#9#9#9#9#9#9',cStatus'
      '                            ,iParcela)'
      '                      SELECT Titulo.nCdTitulo'
      '                            ,Titulo.nCdTransacaoCartao'
      '                            ,Titulo.dDtVenc'
      '                            ,Titulo.dDtEmissao'
      '                            ,TransacaoCartao.cNrLotePOS'
      
        '                            ,CASE WHEN TransacaoCartao.iParcelas' +
        ' = 1 THEN '#39'A VISTA'#39
      
        '                                  ELSE Convert(VARCHAR(3),Titulo' +
        '.iParcela) + '#39' de '#39' + Convert(VARCHAR(3),TransacaoCartao.iParcel' +
        'as)'
      '                             END'
      '                            ,Titulo.nCdOperadoraCartao'
      '                            ,OperadoraCartao.cNmOperadoraCartao'
      '                            ,Titulo.nCdGrupoOperadoraCartao'
      
        '                            ,GrupoOperadoraCartao.cNmGrupoOperad' +
        'oraCartao'
      '                            ,OperadoraCartao.cNmOperadoraCartao'
      '                            ,TransacaoCartao.iNrDocto'
      '                            ,TransacaoCartao.iNrAutorizacao'
      
        '                            ,Titulo.nSaldoTit + Titulo.nValTaxaO' +
        'peradora'
      '                            ,Titulo.nSaldoTit'
      '                            ,Titulo.dDtVenc'
      '                            ,Titulo.nSaldoTit'
      '                            ,'#39'PEN'#39
      '                            ,Titulo.iParcela'
      '                        FROM Titulo'
      
        '                             INNER JOIN TransacaoCartao       ON' +
        ' TransacaoCartao.nCdTransacaoCartao           = Titulo.nCdTransa' +
        'caoCartao'
      
        '                             INNER JOIN OperadoraCartao       ON' +
        ' OperadoraCartao.nCdOperadoraCartao           = Titulo.nCdOperad' +
        'oraCartao'
      
        '                             INNER JOIN GrupoOperadoraCartao  ON' +
        ' GrupoOperadoraCartao.nCdGrupoOperadoraCartao = Titulo.nCdGrupoO' +
        'peradoraCartao'
      
        '                       WHERE Titulo.cFlgCartaoConciliado       =' +
        ' 0'
      
        '                         AND TransacaoCartao.cFlgPOSEncerrado  =' +
        ' 1'
      
        '                         AND TransacaoCartao.cFlgGeradoWeb     =' +
        ' :cFlgOrigemTransacao'
      
        '                         AND Titulo.dDtCancel                  I' +
        'S NULL'
      
        '                         AND Titulo.nCdEmpresa                 =' +
        ' @nCdEmpresa'
      
        '                         AND Titulo.nCdLojaTit                 =' +
        ' @nCdLoja'
      
        '                         AND ((Titulo.nCdGrupoOperadoraCartao  =' +
        ' @nCdGrupoOperadoraCartao) OR (@nCdGrupoOperadoraCartao = 0))'
      
        '                         AND ((Titulo.nCdOperadoraCartao       =' +
        ' @nCdOperadoraCartao)      OR (@nCdOperadoraCartao      = 0))'
      
        '                         AND ((Titulo.dDtVenc                 >=' +
        ' Convert(DATETIME,@dDtVencIni,103))          OR (@dDtVencIni    ' +
        '  = '#39'01/01/1900'#39'))'
      
        '                         AND ((Titulo.dDtVenc                  <' +
        ' (Convert(DATETIME,@dDtVencFim,103)+1))      OR (@dDtVencFim    ' +
        '  = '#39'01/01/1900'#39'))'
      
        '                         AND ((Titulo.dDtEmissao              >=' +
        ' Convert(DATETIME,@dDtTransacaoIni,103))     OR (@dDtTransacaoIn' +
        'i = '#39'01/01/1900'#39'))'
      
        '                         AND ((Titulo.dDtEmissao               <' +
        ' (Convert(DATETIME,@dDtTransacaoFim,103)+1)) OR (@dDtTransacaoFi' +
        'm = '#39'01/01/1900'#39'))'
      
        '                         AND ((TransacaoCartao.cNrLotePOS      =' +
        ' @cNrLotePOS)                                OR (RTRIM(@cNrLoteP' +
        'OS) = '#39#39'))'
      
        '                         AND ((Titulo.iParcela                 =' +
        ' @iParcela)                                  OR (@iParcela = 0))'
      
        '                         AND ((TransacaoCartao.iNrDocto        =' +
        ' @iNrDocto)                                  OR (@iNrDocto = 0))'
      '                         AND NOT EXISTS(SELECT 1'
      '                                          FROM #TempTransacoes'
      
        '                                         WHERE #TempTransacoes.n' +
        'CdTitulo = Titulo.nCdTitulo)')
    Left = 420
    Top = 405
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 420
    Top = 373
  end
  object qryTransacaoConciliada: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM #TempTransacoes'
      ' WHERE cStatus = '#39'CON'#39)
    Left = 356
    Top = 373
    object qryTransacaoConciliadanCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTransacaoConciliadanCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTransacaoConciliadadDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTransacaoConciliadadDtTransacao: TDateTimeField
      FieldName = 'dDtTransacao'
    end
    object qryTransacaoConciliadacNrLote: TStringField
      FieldName = 'cNrLote'
      FixedChar = True
      Size = 15
    end
    object qryTransacaoConciliadacParcela: TStringField
      FieldName = 'cParcela'
    end
    object qryTransacaoConciliadanCdOperadora: TIntegerField
      FieldName = 'nCdOperadora'
    end
    object qryTransacaoConciliadacNmOperadora: TStringField
      FieldName = 'cNmOperadora'
      Size = 50
    end
    object qryTransacaoConciliadanCdGrupoOperadora: TIntegerField
      FieldName = 'nCdGrupoOperadora'
    end
    object qryTransacaoConciliadacNmGrupoOperadora: TStringField
      FieldName = 'cNmGrupoOperadora'
      Size = 50
    end
    object qryTransacaoConciliadanValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTransacaoConciliadanSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTransacaoConciliadadDtVencOriginal: TDateTimeField
      FieldName = 'dDtVencOriginal'
    end
    object qryTransacaoConciliadanSaldoTitOriginal: TBCDField
      FieldName = 'nSaldoTitOriginal'
      Precision = 12
      Size = 2
    end
    object qryTransacaoConciliadacStatus: TStringField
      FieldName = 'cStatus'
      FixedChar = True
      Size = 3
    end
    object qryTransacaoConciliadaiParcela: TIntegerField
      FieldName = 'iParcela'
    end
  end
  object dsTransacaoConciliada: TDataSource
    DataSet = qryTransacaoConciliada
    Left = 356
    Top = 405
  end
  object SP_CONCILIA_CARTAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CONCILIA_CARTAO'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 324
    Top = 333
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 356
    Top = 301
  end
  object PopupMenu1: TPopupMenu
    Left = 388
    Top = 301
    object AlterarDadosdoPagamento1: TMenuItem
      Caption = 'Alterar Dados do Pagamento'
      OnClick = AlterarDadosdoPagamento1Click
    end
  end
  object qryTransacaoCartao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM TransacaoCartao'
      'WHERE nCdTransacaoCartao = :nPK')
    Left = 388
    Top = 405
    object qryTransacaoCartaonCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTransacaoCartaodDtTransacao: TDateTimeField
      FieldName = 'dDtTransacao'
    end
    object qryTransacaoCartaodDtCredito: TDateTimeField
      FieldName = 'dDtCredito'
    end
    object qryTransacaoCartaoiNrCartao: TLargeintField
      FieldName = 'iNrCartao'
    end
    object qryTransacaoCartaoiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryTransacaoCartaoiNrAutorizacao: TIntegerField
      FieldName = 'iNrAutorizacao'
    end
    object qryTransacaoCartaonCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryTransacaoCartaonCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryTransacaoCartaonValTransacao: TBCDField
      FieldName = 'nValTransacao'
      Precision = 12
      Size = 2
    end
    object qryTransacaoCartaonCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryTransacaoCartaocFlgConferencia: TIntegerField
      FieldName = 'cFlgConferencia'
    end
    object qryTransacaoCartaonCdUsuarioConf: TIntegerField
      FieldName = 'nCdUsuarioConf'
    end
    object qryTransacaoCartaodDtConferencia: TDateTimeField
      FieldName = 'dDtConferencia'
    end
    object qryTransacaoCartaonCdStatusDocto: TIntegerField
      FieldName = 'nCdStatusDocto'
    end
    object qryTransacaoCartaoiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryTransacaoCartaocNrLotePOS: TStringField
      FieldName = 'cNrLotePOS'
      FixedChar = True
      Size = 15
    end
    object qryTransacaoCartaonCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryTransacaoCartaocFlgPOSEncerrado: TIntegerField
      FieldName = 'cFlgPOSEncerrado'
    end
    object qryTransacaoCartaonCdLojaCartao: TIntegerField
      FieldName = 'nCdLojaCartao'
    end
    object qryTransacaoCartaonValTaxaOperadora: TBCDField
      FieldName = 'nValTaxaOperadora'
      Precision = 12
      Size = 2
    end
    object qryTransacaoCartaonCdCondPagtoCartao: TIntegerField
      FieldName = 'nCdCondPagtoCartao'
    end
    object qryTransacaoCartaonCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryTransacaoCartaodDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryTransacaoCartaocIDExterno: TStringField
      FieldName = 'cIDExterno'
      Size = 50
    end
    object qryTransacaoCartaocNrDoctoNSU: TSmallintField
      FieldName = 'cNrDoctoNSU'
    end
    object qryTransacaoCartaocNrAutorizacao: TStringField
      FieldName = 'cNrAutorizacao'
    end
  end
  object qryCondPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nPercAcrescimo'
      ',nCdTabTipoFormaPagto'
      'FROM CondPagto'
      
        'LEFT JOIN FormaPagto ON FormaPagto.nCdFormaPagto = CondPagto.nCd' +
        'FormaPagto'
      'WHERE nCdCondPagto = :nPK')
    Left = 288
    Top = 304
    object qryCondPagtonPercAcrescimo: TBCDField
      FieldName = 'nPercAcrescimo'
      Precision = 12
      Size = 2
    end
    object qryCondPagtonCdTabTipoFormaPagto: TIntegerField
      FieldName = 'nCdTabTipoFormaPagto'
    end
  end
  object qryCartaoSituacaoAtual: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmOperadoraCartao'
      '      ,cNmCondPagto'
      '      ,cNmFormaPagto'
      '  FROM TransacaoCartao'
      
        '       LEFT JOIN OperadoraCartao ON OperadoraCartao.nCdOperadora' +
        'Cartao = TransacaoCartao.nCdOperadoraCartao'
      
        '       LEFT JOIN CondPagto       ON CondPagto.nCdCondPagto      ' +
        '       = TransacaoCartao.nCdCondPagtoCartao'
      
        '       LEFT JOIN FormaPagto      ON FormaPagto.nCdFormaPagto    ' +
        '       = CondPagto.nCdFormaPagto'
      ' WHERE nCdTransacaoCartao = :nPK')
    Left = 288
    Top = 336
    object qryCartaoSituacaoAtualcNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryCartaoSituacaoAtualcNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryCartaoSituacaoAtualcNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
  end
  object SP_ALTERA_CONDICAO_TRANSACAO_CARTAO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_ALTERA_CONDICAO_TRANSACAO_CARTAO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdTransacaoCartao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdCondPagto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdOperadoraCartao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNrDocto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@iNrAutorizacao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@cNrDoctoNSU'
        Attributes = [paNullable]
        DataType = ftString
        Size = 50
        Value = Null
      end
      item
        Name = '@cNrAutorizacao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 12
        Value = Null
      end>
    Left = 320
    Top = 304
  end
  object qryDadosTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTitulo, nCdLojaTit'
      '  FROM Titulo'
      ' WHERE nCdTitulo = :nPK')
    Left = 388
    Top = 373
    object qryDadosTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryDadosTitulonCdLojaTit: TIntegerField
      FieldName = 'nCdLojaTit'
    end
  end
  object qryArquivo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT '#39'A2'#39'                                         as cColuna'
      '      ,cNmGrupoOperadoraCartao'
      '      ,cNmLoja'
      '      ,Temp.cNmOperadoraCartao'
      '      ,CASE WHEN cFlgTipoOperacao = '#39'C'#39
      '            THEN '#39'CREDITO'#39
      '            ELSE '#39'DEBITO'#39
      '       END cTipoOperacao'
      '      ,iParcelas'
      '      ,Temp.iNrDocto'
      '      ,Temp.iNrAutorizacao'
      
        '      ,REPLACE(nValTransacao,'#39'.'#39','#39','#39')               as nValTrans' +
        'acao'
      '      ,cFlgTipoOperacao'
      
        '      ,ISNULL(cNmTerceiro,'#39'N'#195'O IDENTIFICADO'#39')       as cNmTercei' +
        'ro'
      
        '      ,CONVERT(varchar(10), Temp.dDtTransacao, 103) as cDtTransa' +
        'cao'
      '      ,CASE WHEN T.cFlgGeradoFinanceiro = 1'
      '            THEN '#39'FINANCEIRO'#39
      '            WHEN T.cFlgGeradoWeb        = 1'
      '            THEN '#39'E-COMMERCE'#39
      '            ELSE '#39'VENDA'#39
      '       END cOrigem'
      
        '      ,CAST(Temp.nCdTitulo as varchar)               as cReferen' +
        'cia1'
      '  FROM #TempTransacoes Temp'
      
        '       INNER JOIN TransacaoCartao      T   ON T.nCdTransacaoCart' +
        'ao        = Temp.nCdTransacaoCartao'
      
        '       INNER JOIN Loja                     ON Loja.nCdLoja      ' +
        '          = T.nCdLojaCartao'
      
        '       INNER JOIN GrupoOperadoraCartao GOC ON GOC.nCdGrupoOperad' +
        'oraCartao = T.nCdGrupoOperadoraCartao'
      
        '       INNER JOIN OperadoraCartao      OC  ON OC.nCdOperadoraCar' +
        'tao       = T.nCdOperadoraCartao'
      
        '       LEFT  JOIN Terceiro                 ON Terceiro.nCdTercei' +
        'ro        = T.nCdTerceiro'
      ' WHERE cStatus = '#39'PRE'#39
      ' ORDER BY T.dDtTransacao')
    Left = 452
    Top = 376
    object qryArquivocColuna: TStringField
      FieldName = 'cColuna'
      ReadOnly = True
      Size = 2
    end
    object qryArquivocNmGrupoOperadoraCartao: TStringField
      FieldName = 'cNmGrupoOperadoraCartao'
      Size = 50
    end
    object qryArquivocNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryArquivocNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryArquivocTipoOperacao: TStringField
      FieldName = 'cTipoOperacao'
      ReadOnly = True
      Size = 7
    end
    object qryArquivoiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryArquivoiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryArquivoiNrAutorizacao: TIntegerField
      FieldName = 'iNrAutorizacao'
    end
    object qryArquivonValTransacao: TStringField
      FieldName = 'nValTransacao'
      ReadOnly = True
      EditMask = '#,##0.00 '
      Size = 8000
    end
    object qryArquivocFlgTipoOperacao: TStringField
      FieldName = 'cFlgTipoOperacao'
      FixedChar = True
      Size = 1
    end
    object qryArquivocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      ReadOnly = True
      Size = 100
    end
    object qryArquivocDtTransacao: TStringField
      FieldName = 'cDtTransacao'
      ReadOnly = True
      Size = 10
    end
    object qryArquivocOrigem: TStringField
      FieldName = 'cOrigem'
      ReadOnly = True
      Size = 10
    end
    object qryArquivocReferencia1: TStringField
      FieldName = 'cReferencia1'
      ReadOnly = True
      Size = 10
    end
  end
end
