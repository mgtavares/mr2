inherited frmServidorReplicacao_FilaTransmissao_Novo: TfrmServidorReplicacao_FilaTransmissao_Novo
  Left = 0
  Top = 0
  Width = 1152
  Height = 738
  Caption = 'Fila Transmiss'#227'o'
  OldCreateOrder = True
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1144
    Height = 678
  end
  inherited ToolBar1: TToolBar
    Width = 1144
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1144
    Height = 678
    ActivePage = cxTabSheet4
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 674
    ClientRectLeft = 4
    ClientRectRight = 1140
    ClientRectTop = 24
    object cxTabSheet4: TcxTabSheet
      Caption = 'Registros Pendentes - Por Servidor Destino'
      ImageIndex = 3
      object cxGrid3: TcxGrid
        Left = 0
        Top = 0
        Width = 1136
        Height = 650
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView3: TcxGridDBTableView
          PopupMenu = PopupMenu4
          DataController.DataSource = dsPendentesServidor
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#'
              Kind = skCount
            end
            item
              Format = ',0'
              Kind = skSum
              Column = cxGridDBTableView3iQtdePendente
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView3nCdServidor: TcxGridDBColumn
            Caption = 'C'#243'd. Servidor'
            DataBinding.FieldName = 'nCdServidorDestino'
            Width = 93
          end
          object cxGridDBTableView3cNmServidor: TcxGridDBColumn
            Caption = 'Servidor Destino'
            DataBinding.FieldName = 'cNmServidor'
            Width = 171
          end
          object cxGridDBTableView3iQtdePendente: TcxGridDBColumn
            Caption = 'Registros Pendentes - Retorno'
            DataBinding.FieldName = 'iQtdePendenteRetorno'
            Width = 206
          end
          object cxGridDBTableView3DBColumn1: TcxGridDBColumn
            Caption = 'Registros Pendentes - Gera'#231#227'o'
            DataBinding.FieldName = 'iQtdePendentePacote'
            Width = 202
          end
        end
        object cxGridLevel3: TcxGridLevel
          GridView = cxGridDBTableView3
        end
      end
    end
    object cxTabSheet5: TcxTabSheet
      Caption = 'Registros Pendentes - Por Tabela'
      ImageIndex = 4
      object cxGrid4: TcxGrid
        Left = 0
        Top = 0
        Width = 1136
        Height = 650
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView4: TcxGridDBTableView
          PopupMenu = PopupMenu5
          DataController.DataSource = dsPendentesTabela
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#'
              Kind = skCount
            end
            item
              Format = ',0'
              Kind = skSum
              Column = cxGridDBTableView4iQtdePendente
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView4nCdTabelaSistema: TcxGridDBColumn
            Caption = 'C'#243'd.'
            DataBinding.FieldName = 'nCdTabelaSistema'
          end
          object cxGridDBTableView4cTabela: TcxGridDBColumn
            Caption = 'Tabela'
            DataBinding.FieldName = 'cTabela'
            Width = 202
          end
          object cxGridDBTableView4iQtdePendente: TcxGridDBColumn
            Caption = 'Registros Pendentes'
            DataBinding.FieldName = 'iQtdePendente'
            Width = 141
          end
          object cxGridDBTableView4cNmTabela: TcxGridDBColumn
            Caption = 'Descri'#231#227'o Tabela'
            DataBinding.FieldName = 'cNmTabela'
            Width = 538
          end
        end
        object cxGridLevel4: TcxGridLevel
          GridView = cxGridDBTableView4
        end
      end
    end
    object cxTabSheet1: TcxTabSheet
      Caption = 'Registros Pendentes - Por Registro'
      ImageIndex = 0
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 1136
        Height = 650
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView1: TcxGridDBTableView
          PopupMenu = PopupMenu1
          DataController.DataSource = dsTempRegistro
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#'
              Kind = skCount
              Column = cxGridDBTableView1nCdTempRegistro
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView1nCdTempRegistro: TcxGridDBColumn
            Caption = 'ID'
            DataBinding.FieldName = 'nCdTempRegistro'
          end
          object cxGridDBTableView1dDtInclusao: TcxGridDBColumn
            Caption = 'Dt. Inclus'#227'o'
            DataBinding.FieldName = 'dDtInclusao'
            Width = 177
          end
          object cxGridDBTableView1cTabela: TcxGridDBColumn
            Caption = 'Tabela'
            DataBinding.FieldName = 'cTabela'
            Width = 218
          end
          object cxGridDBTableView1iIDRegistro: TcxGridDBColumn
            Caption = 'C'#243'digo Registro'
            DataBinding.FieldName = 'iIDRegistro'
            Width = 122
          end
          object cxGridDBTableView1cFlgAcao: TcxGridDBColumn
            Caption = 'A'#231#227'o'
            DataBinding.FieldName = 'cFlgAcao'
            Width = 135
          end
          object cxGridDBTableView1cNmServidor: TcxGridDBColumn
            Caption = 'Servidor Destino'
            DataBinding.FieldName = 'cNmServidor'
            Width = 270
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
    object cxTabSheet3: TcxTabSheet
      Caption = 'Registros Descartados'
      ImageIndex = 2
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1136
        Height = 650
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        PopupMenu = PopupMenu3
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView2: TcxGridDBTableView
          PopupMenu = PopupMenu3
          DataController.DataSource = dsDescartados
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              FieldName = 'nValTotal'
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#,##0.00'
              Kind = skSum
            end
            item
              Format = '#'
              Kind = skCount
            end
            item
              Format = '#'
              Kind = skCount
              Column = cxGridDBTableView2nCdTempRegistro
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnGrouping = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsSelection.CellMultiSelect = True
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          Styles.Content = frmMenu.FonteSomenteLeitura
          object cxGridDBTableView2nCdTempRegistro: TcxGridDBColumn
            Caption = 'ID'
            DataBinding.FieldName = 'nCdTempRegistro'
          end
          object cxGridDBTableView2cTabela: TcxGridDBColumn
            Caption = 'Tabela'
            DataBinding.FieldName = 'cTabela'
            Width = 235
          end
          object cxGridDBTableView2iIDRegistro: TcxGridDBColumn
            Caption = 'C'#243'digo Registro'
            DataBinding.FieldName = 'iIDRegistro'
          end
          object cxGridDBTableView2cFlgAcao: TcxGridDBColumn
            Caption = 'A'#231#227'o'
            DataBinding.FieldName = 'cFlgAcao'
          end
          object cxGridDBTableView2cNmServidor: TcxGridDBColumn
            Caption = 'Servidor Destino'
            DataBinding.FieldName = 'cNmServidor'
            Width = 178
          end
          object cxGridDBTableView2dDtUltTentativa: TcxGridDBColumn
            Caption = #218'lt. Tentativa'
            DataBinding.FieldName = 'dDtUltTentativa'
          end
          object cxGridDBTableView2iQtdeTentativas: TcxGridDBColumn
            Caption = 'Tentativas'
            DataBinding.FieldName = 'iQtdeTentativas'
          end
          object cxGridDBTableView2cMsgErro: TcxGridDBColumn
            Caption = 'Mensagem Erro'
            DataBinding.FieldName = 'cMsgErro'
            Width = 242
          end
          object cxGridDBTableView2dDtInclusao: TcxGridDBColumn
            Caption = 'Dt. Inclus'#227'o'
            DataBinding.FieldName = 'dDtInclusao'
          end
        end
        object cxGridLevel2: TcxGridLevel
          GridView = cxGridDBTableView2
        end
      end
    end
  end
  object qryTempRegistro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT nCdTempRegistro'
      '      ,cTabela'
      '      ,iIDRegistro'
      '      ,CASE WHEN cFlgAcao = '#39'I'#39' THEN '#39'Inclus'#227'o'#39
      '            ELSE '#39'Exclus'#227'o'#39
      '       END cFlgAcao'
      '      ,Servidor.cNmServidor'
      ',TempRegistro.dDtInclusao'
      '  FROM TempRegistro'
      
        '       INNER JOIN TabelaSistema ON TabelaSistema.nCdTabelaSistem' +
        'a = TempRegistro.nCdTabelaSistema'
      
        '       INNER JOIN Servidor      ON Servidor.nCdServidor         ' +
        '  = TempRegistro.nCdServidorDestino'
      ' WHERE NOT EXISTS(SELECT TOP 1 1'
      '                    FROM ItemPacoteReplicacao IPR'
      
        '                   WHERE IPR.nCdTempRegistro = TempRegistro.nCdT' +
        'empRegistro'
      '                     AND (    (IPR.cFlgIntegrado = 1)'
      '                           OR (IPR.cFlgErro      = 1)))'
      ' ORDER BY dDtInclusao'
      '')
    Left = 192
    Top = 264
    object qryTempRegistronCdTempRegistro: TAutoIncField
      DisplayLabel = 'Registros Pendentes de Transmiss'#227'o|C'#243'd'
      FieldName = 'nCdTempRegistro'
      ReadOnly = True
    end
    object qryTempRegistrocTabela: TStringField
      DisplayLabel = 'Registros Pendentes de Transmiss'#227'o|Tabela'
      FieldName = 'cTabela'
      Size = 50
    end
    object qryTempRegistroiIDRegistro: TIntegerField
      DisplayLabel = 'Registros Pendentes de Transmiss'#227'o|ID Registro'
      FieldName = 'iIDRegistro'
    end
    object qryTempRegistrocFlgAcao: TStringField
      DisplayLabel = 'Registros Pendentes de Transmiss'#227'o|A'#231#227'o'
      FieldName = 'cFlgAcao'
      ReadOnly = True
      Size = 8
    end
    object qryTempRegistrocNmServidor: TStringField
      DisplayLabel = 'Registros Pendentes de Transmiss'#227'o|Servidor Destino'
      FieldName = 'cNmServidor'
      Size = 50
    end
    object qryTempRegistrodDtInclusao: TDateTimeField
      DisplayLabel = 'Registros Pendentes de Transmiss'#227'o|Dt. Inclus'#227'o'
      FieldName = 'dDtInclusao'
    end
  end
  object dsTempRegistro: TDataSource
    DataSet = qryTempRegistro
    Left = 232
    Top = 264
  end
  object PopupMenu1: TPopupMenu
    Left = 176
    Top = 136
    object Atualizar1: TMenuItem
      Caption = 'Atualizar'
      OnClick = Atualizar1Click
    end
  end
  object PopupMenu3: TPopupMenu
    Left = 216
    Top = 136
    object MenuItem2: TMenuItem
      Caption = 'Atualizar'
      OnClick = MenuItem2Click
    end
    object MenuItem3: TMenuItem
      Caption = 'Exibir Erro Completo'
      OnClick = MenuItem3Click
    end
    object Retransmitir1: TMenuItem
      Caption = 'Retransmitir'
      OnClick = Retransmitir1Click
    end
  end
  object qryDescartados: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT TempRegistro.nCdTempRegistro'
      '      ,cTabela'
      '      ,TempRegistro.iIDRegistro'
      '      ,CASE WHEN TempRegistro.cFlgAcao = '#39'I'#39' THEN '#39'Inclus'#227'o'#39
      '            ELSE '#39'Exclus'#227'o'#39
      '       END cFlgAcao'
      '      ,Servidor.cNmServidor'
      '      ,IPR.*'
      '  FROM TempRegistro'
      
        '       INNER JOIN TabelaSistema ON TabelaSistema.nCdTabelaSistem' +
        'a = TempRegistro.nCdTabelaSistema'
      
        '       INNER JOIN Servidor      ON Servidor.nCdServidor         ' +
        '  = TempRegistro.nCdServidorDestino'
      
        '       INNER JOIN ItemPacoteReplicacao IPR ON IPR.nCdTempRegistr' +
        'o = TempRegistro.nCdTempRegistro'
      
        ' WHERE TempRegistro.dDtInclusao   >= (dbo.fn_OnlyDate(GetDate())' +
        ' - 15)'
      '   AND EXISTS(SELECT TOP 1 1'
      '                FROM ItemPacoteReplicacao IPR'
      
        '               WHERE IPR.nCdTempRegistro = TempRegistro.nCdTempR' +
        'egistro'
      '                 AND IPR.cFlgIntegrado = 0'
      '                 AND IPR.cFlgErro      = 1)'
      '                       '
      ' ORDER BY TempRegistro.dDtInclusao DESC')
    Left = 368
    Top = 296
    object qryDescartadosnCdTempRegistro: TIntegerField
      FieldName = 'nCdTempRegistro'
    end
    object qryDescartadoscTabela: TStringField
      FieldName = 'cTabela'
      Size = 50
    end
    object qryDescartadosiIDRegistro: TIntegerField
      FieldName = 'iIDRegistro'
    end
    object qryDescartadoscFlgAcao: TStringField
      FieldName = 'cFlgAcao'
      ReadOnly = True
      Size = 8
    end
    object qryDescartadoscNmServidor: TStringField
      FieldName = 'cNmServidor'
      Size = 50
    end
    object qryDescartadosnCdItemPacoteReplicacao: TAutoIncField
      FieldName = 'nCdItemPacoteReplicacao'
      ReadOnly = True
    end
    object qryDescartadosnCdPacoteReplicacao: TIntegerField
      FieldName = 'nCdPacoteReplicacao'
    end
    object qryDescartadosnCdTempRegistro_1: TIntegerField
      FieldName = 'nCdTempRegistro_1'
    end
    object qryDescartadosdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryDescartadosdDtUltTentativa: TDateTimeField
      FieldName = 'dDtUltTentativa'
    end
    object qryDescartadosiQtdeTentativas: TIntegerField
      FieldName = 'iQtdeTentativas'
    end
    object qryDescartadoscFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryDescartadoscFlgErro: TIntegerField
      FieldName = 'cFlgErro'
    end
    object qryDescartadoscFlgBroken: TIntegerField
      FieldName = 'cFlgBroken'
    end
    object qryDescartadoscFlgObsoleto: TIntegerField
      FieldName = 'cFlgObsoleto'
    end
    object qryDescartadoscFlgNaoEncontrado: TIntegerField
      FieldName = 'cFlgNaoEncontrado'
    end
    object qryDescartadoscMsgErro: TMemoField
      FieldName = 'cMsgErro'
      BlobType = ftMemo
    end
  end
  object dsDescartados: TDataSource
    DataSet = qryDescartados
    Left = 408
    Top = 296
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 364
    Top = 221
  end
  object qryPendentesServidor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT *'
      '  FROM (SELECT TempRegistro.nCdServidorDestino'
      '              ,Servidor.cNmServidor'
      '              ,(SELECT COUNT(1)'
      '                  FROM TempRegistro TR'
      
        '                 WHERE TR.nCdServidorDestino = TempRegistro.nCdS' +
        'ervidorDestino'
      '                   AND NOT EXISTS (SELECT TOP 1 1'
      
        '                                     FROM ItemPacoteReplicacao I' +
        'PR'
      
        '                                    WHERE IPR.nCdTempRegistro = ' +
        'TR.nCdTempRegistro'
      
        '                                      AND (    (IPR.cFlgIntegrad' +
        'o = 1)'
      
        '                                            OR (IPR.cFlgErro    ' +
        '  = 1)))'
      '                   AND EXISTS(SELECT TOP 1 1'
      '                                FROM PacoteTempRegistro PTR'
      
        '                               WHERE PTR.nCdTempRegistro = TR.nC' +
        'dTempRegistro)) iQtdePendenteRetorno'
      '              ,COUNT(1) - (SELECT COUNT(1)'
      '                             FROM TempRegistro TR'
      
        '                            WHERE TR.nCdServidorDestino = TempRe' +
        'gistro.nCdServidorDestino'
      '                              AND EXISTS(SELECT TOP 1 1'
      
        '                                           FROM PacoteTempRegist' +
        'ro PTR'
      
        '                                          WHERE PTR.nCdTempRegis' +
        'tro = TR.nCdTempRegistro)) iQtdePendentePacote'
      '         FROM TempRegistro'
      
        '              INNER JOIN Servidor ON Servidor.nCdServidor = Temp' +
        'Registro.nCdServidorDestino'
      '        GROUP BY TempRegistro.nCdServidorDestino'
      '                ,cNmServidor) RegistroServidor'
      '    WHERE iQtdePendentePacote > 0'
      '       OR iQtdePendenteRetorno > 0'
      ' ORDER BY 1')
    Left = 192
    Top = 312
    object qryPendentesServidornCdServidorDestino: TIntegerField
      FieldName = 'nCdServidorDestino'
    end
    object qryPendentesServidorcNmServidor: TStringField
      FieldName = 'cNmServidor'
      Size = 50
    end
    object qryPendentesServidoriQtdePendenteRetorno: TIntegerField
      FieldName = 'iQtdePendenteRetorno'
      ReadOnly = True
    end
    object qryPendentesServidoriQtdePendentePacote: TIntegerField
      FieldName = 'iQtdePendentePacote'
      ReadOnly = True
    end
  end
  object dsPendentesServidor: TDataSource
    DataSet = qryPendentesServidor
    Left = 232
    Top = 312
  end
  object PopupMenu4: TPopupMenu
    Left = 136
    Top = 136
    object MenuItem4: TMenuItem
      Caption = 'Atualizar'
      OnClick = MenuItem4Click
    end
  end
  object qryPendentesTabela: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT TabelaSistema.nCdTabelaSistema'
      '      ,TabelaSistema.cTabela'
      '      ,TabelaSistema.cNmTabela'
      '      ,Count(1) as iQtdePendente'
      '  FROM TempRegistro'
      
        '       INNER JOIN TabelaSistema ON TabelaSistema.nCdTabelaSistem' +
        'a = TempRegistro.nCdTabelaSistema'
      ' WHERE NOT EXISTS(SELECT TOP 1 1'
      '                    FROM ItemPacoteReplicacao IPR'
      
        '                   WHERE IPR.nCdTempRegistro = TempRegistro.nCdT' +
        'empRegistro'
      '                     AND (    (IPR.cFlgIntegrado = 1)'
      '                           OR (IPR.cFlgErro      = 1)))'
      ' GROUP BY TabelaSistema.nCdTabelaSistema'
      '         ,TabelaSistema.cTabela'
      '         ,TabelaSistema.cNmTabela'
      ' ORDER BY 4 DESC'
      '')
    Left = 192
    Top = 360
    object qryPendentesTabelanCdTabelaSistema: TIntegerField
      FieldName = 'nCdTabelaSistema'
    end
    object qryPendentesTabelacTabela: TStringField
      FieldName = 'cTabela'
      Size = 50
    end
    object qryPendentesTabelacNmTabela: TStringField
      FieldName = 'cNmTabela'
      Size = 50
    end
    object qryPendentesTabelaiQtdePendente: TIntegerField
      FieldName = 'iQtdePendente'
      ReadOnly = True
      DisplayFormat = ',0'
    end
  end
  object dsPendentesTabela: TDataSource
    DataSet = qryPendentesTabela
    Left = 232
    Top = 360
  end
  object PopupMenu5: TPopupMenu
    Left = 96
    Top = 136
    object MenuItem5: TMenuItem
      Caption = 'Atualizar'
      OnClick = MenuItem5Click
    end
  end
end
