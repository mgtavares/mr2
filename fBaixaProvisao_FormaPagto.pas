unit fBaixaProvisao_FormaPagto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmBaixaProvisao_FormaPagto = class(TfrmProcesso_Padrao)
    qryFormaPagto: TADOQuery;
    qryFormaPagtocNmFormaPagto: TStringField;
    qryFormaPagtonValTotal: TBCDField;
    DBGridEh1: TDBGridEh;
    dsFormaPagto: TDataSource;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBaixaProvisao_FormaPagto: TfrmBaixaProvisao_FormaPagto;

implementation

uses fBaixaProvisao_Provisoes, fMenu;

{$R *.dfm}

procedure TfrmBaixaProvisao_FormaPagto.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmBaixaProvisao_Provisoes ;
begin
  inherited;

  if not qryFormaPagto.Eof and (qryFormaPagtocNmFormaPagto.Value <> '') then
  begin

      objForm := TfrmBaixaProvisao_Provisoes.Create(nil) ;

      objForm.qryProvisoes.Close ;
      objForm.qryProvisoes.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva;
      objForm.qryProvisoes.Parameters.ParamByName('dDtPagto').Value         := qryFormaPagto.Parameters.ParamByName('dDtPagto').Value ;
      objForm.qryProvisoes.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
      objForm.qryProvisoes.Parameters.ParamByName('nCdContaBancaria').Value := qryFormaPagto.Parameters.ParamByName('nCdContaBancaria').Value ;
      objForm.qryProvisoes.Parameters.ParamByName('nCdFormapagto').Value    := qryFormaPagtonCdFormaPagto.Value ;
      objForm.qryProvisoes.Open ;

      showForm( objForm , TRUE ) ;
      
      qryFormaPagto.Close ;
      qryFormaPagto.Open ;

      if (qryFormaPagto.eof) then
          close ;
          
  end ;

end;

end.
