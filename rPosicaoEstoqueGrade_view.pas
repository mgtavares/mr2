unit rPosicaoEstoqueGrade_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosicaoEstoqueGrade_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRLabel4: TQRLabel;
    QRShape2: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel11: TQRLabel;
    SPREL_POSICAOESTOQUE_GRADE: TADOStoredProc;
    QRDBText9: TQRDBText;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel8: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    SummaryBand1: TQRBand;
    QRShape6: TQRShape;
    QRExpr3: TQRExpr;
    QRLabel20: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText3: TQRDBText;
    QRBand2: TQRBand;
    QRLabel9: TQRLabel;
    QRExpr2: TQRExpr;
    QRDBText25: TQRDBText;
    QRLabel33: TQRLabel;
    QRDBText26: TQRDBText;
    QRLabel34: TQRLabel;
    QRDBText27: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText28: TQRDBText;
    QRLabel37: TQRLabel;
    QRDBText20: TQRDBText;
    QRLabel28: TQRLabel;
    QRDBText22: TQRDBText;
    QRLabel30: TQRLabel;
    QRDBText8: TQRDBText;
    QRLabel26: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel22: TQRLabel;
    QRDBText21: TQRDBText;
    QRLabel29: TQRLabel;
    QRDBText24: TQRDBText;
    QRLabel32: TQRLabel;
    QRDBText23: TQRDBText;
    QRLabel31: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText11: TQRDBText;
    QRBand4: TQRBand;
    QRLabel10: TQRLabel;
    QRExpr1: TQRExpr;
    QRDBText12: TQRDBText;
    QRBand6: TQRBand;
    QRLabel12: TQRLabel;
    QRExpr4: TQRExpr;
    QRDBText13: TQRDBText;
    QRBand7: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr5: TQRExpr;
    QRDBText29: TQRDBText;
    QRGroup2: TQRGroup;
    QRGroup3: TQRGroup;
    QRGroup1: TQRGroup;
    QRGroup5: TQRGroup;
    QRGroup6: TQRGroup;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRLabel21: TQRLabel;
    QRLabel27: TQRLabel;
    SPREL_POSICAOESTOQUE_GRADEnCdTempResultado: TAutoIncField;
    SPREL_POSICAOESTOQUE_GRADEcNmProduto: TStringField;
    SPREL_POSICAOESTOQUE_GRADEnCdProduto: TIntegerField;
    SPREL_POSICAOESTOQUE_GRADEcReferencia: TStringField;
    SPREL_POSICAOESTOQUE_GRADEcNmMarca: TStringField;
    SPREL_POSICAOESTOQUE_GRADEnQtdeEstoque: TBCDField;
    SPREL_POSICAOESTOQUE_GRADEnValCusto: TBCDField;
    SPREL_POSICAOESTOQUE_GRADEnValVenda: TBCDField;
    SPREL_POSICAOESTOQUE_GRADEcNmDepartamento: TStringField;
    SPREL_POSICAOESTOQUE_GRADEcNmCategoria: TStringField;
    SPREL_POSICAOESTOQUE_GRADEcNmSubCategoria: TStringField;
    SPREL_POSICAOESTOQUE_GRADEcNmSegmento: TStringField;
    SPREL_POSICAOESTOQUE_GRADEnCdGrade: TIntegerField;
    SPREL_POSICAOESTOQUE_GRADEcTituloGrade: TStringField;
    SPREL_POSICAOESTOQUE_GRADEnCdCampanhaPromoc: TIntegerField;
    SPREL_POSICAOESTOQUE_GRADEnCdLocalEstoque: TIntegerField;
    SPREL_POSICAOESTOQUE_GRADEnQtdeEstoqueLocalEstoque: TBCDField;
    SPREL_POSICAOESTOQUE_GRADEcQtdeGrade: TStringField;
    SPREL_POSICAOESTOQUE_GRADEnCdLoja: TStringField;
    SPREL_POSICAOESTOQUE_GRADEcNmLocalEstoque: TStringField;
    SPREL_POSICAOESTOQUE_GRADEdDtUltReceb: TDateTimeField;
    SPREL_POSICAOESTOQUE_GRADEdDtUltVenda: TDateTimeField;
    SPREL_POSICAOESTOQUE_GRADEiDiasEstoque: TIntegerField;
    SPREL_POSICAOESTOQUE_GRADEnValTotalCusto: TBCDField;
    SPREL_POSICAOESTOQUE_GRADEnValTotalVenda: TBCDField;
    QRLabel38: TQRLabel;
    QRExpr6: TQRExpr;
    QRLabel39: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoEstoqueGrade_view: TrptPosicaoEstoqueGrade_view;

implementation

{$R *.dfm}

end.
