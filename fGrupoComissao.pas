unit fGrupoComissao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  Menus, DBGridEhGrouping;

type
  TfrmGrupoComissao = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoComissao: TIntegerField;
    qryMastercNmGrupoComissao: TStringField;
    qryMasteriDiaPagamento: TIntegerField;
    qryMasternCdCategFinanc: TIntegerField;
    qryMasternCdCC: TIntegerField;
    qryMasternCdUnidadeNegocio: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    qryRegraGrupoComissao: TADOQuery;
    qryRegraGrupoComissaonCdRegraGrupoComissao: TAutoIncField;
    qryRegraGrupoComissaonCdGrupoComissao: TIntegerField;
    qryRegraGrupoComissaonCdGrupoProduto: TIntegerField;
    qryRegraGrupoComissaonValVendaIni: TBCDField;
    qryRegraGrupoComissaonValVendaFim: TBCDField;
    qryRegraGrupoComissaonPercComissao: TBCDField;
    qryRegraGrupoComissaonPercBaseCalc: TBCDField;
    qryRegraGrupoComissaocFlgAtivo: TIntegerField;
    dsRegraGrupoComissao: TDataSource;
    qryRegraGrupoComissaocNmGrupoProduto: TStringField;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    qryRegraGrupoComissaonCdTerceiro: TIntegerField;
    qryRegraGrupoComissaocNmTerceiro: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryCategFinanc: TADOQuery;
    qryCategFinanccNmCategFinanc: TStringField;
    DBEdit7: TDBEdit;
    DataSource1: TDataSource;
    qryCC: TADOQuery;
    qryCCcNmCC: TStringField;
    DBEdit8: TDBEdit;
    DataSource2: TDataSource;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    DBEdit9: TDBEdit;
    DataSource3: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure qryRegraGrupoComissaoBeforePost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryRegraGrupoComissaoCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoComissao: TfrmGrupoComissao;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmGrupoComissao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOCOMISSAO' ;
  nCdTabelaSistema  := 71  ;
  nCdConsultaPadrao := 159 ;
  bLimpaAposSalvar  := False ;

end;

procedure TfrmGrupoComissao.qryRegraGrupoComissaoBeforePost(
  DataSet: TDataSet);
begin

  if (qryRegraGrupoComissaonValVendaIni.Value > qryRegraGrupoComissaonValVendaFim.Value) then
  begin
      MensagemAlerta('Intervalo de valor de faturamento inv�lido.') ;
      abort ;
  end ;

  if (qryRegraGrupoComissaonCdTerceiro.Value > 0) and (qryRegraGrupoComissaonCdGrupoProduto.Value = 0) then
  begin
      MensagemAlerta('Para criar uma regra utilizando terceiros � necess�rio selecionar um grupo de produtos.') ;
      abort ;
  end ;

  if (qryRegraGrupoComissaonPercBaseCalc.Value <= 0) then
  begin
      MensagemAlerta('Percentual de base de c�lculo inv�lido.') ;
      abort ;
  end ;

  if (qryRegraGrupoComissaonPercComissao.Value < 0) then
  begin
      MensagemAlerta('Percentual de comiss�o inv�lido.') ;
      abort ;
  end ;

  qryRegraGrupoComissaonCdGrupoComissao.Value := qryMasternCdGrupoComissao.Value ;

  if (qryRegraGrupoComissao.State = dsInsert) then
      qryRegraGrupoComissaocFlgAtivo.Value := 1 ;
      
  inherited;

end;

procedure TfrmGrupoComissao.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin


        if (qryRegraGrupoComissao.State = dsBrowse) then
             qryRegraGrupoComissao.Edit ;

        if (qryRegraGrupoComissao.State = dsInsert) or (qryRegraGrupoComissao.State = dsEdit) then
        begin

            if (DbGridEh1.Col = 3) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(69);

                If (nPK > 0) then
                    qryRegraGrupoComissaonCdGrupoProduto.Value := nPK ;

            end ;

            if (DbGridEh1.Col = 5) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(200);

                If (nPK > 0) then
                    qryRegraGrupoComissaonCdTerceiro.Value := nPK ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmGrupoComissao.qryRegraGrupoComissaoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryRegraGrupoComissao.Active) then
  begin

      qryGrupoProduto.Close ;
      PosicionaQuery(qryGrupoProduto, qryRegraGrupoComissaonCdGrupoProduto.AsString) ;

      if not qryGrupoProduto.eof then
          qryRegraGrupoComissaocNmGrupoProduto.Value := qryGrupoProdutocNmGrupoProduto.Value ;

      qryTerceiro.Close ;
      PosicionaQuery(qryTerceiro, qryRegraGrupoComissaonCdTerceiro.AsString) ;

      if not qryTerceiro.eof then
          qryRegraGrupoComissaocNmTerceiro.Value := qryTerceirocNmTerceiro.Value ;

  end ;
  
end;

procedure TfrmGrupoComissao.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryRegraGrupoComissao, qryMasternCdGrupoComissao.asString) ;

end;

procedure TfrmGrupoComissao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryCategFinanc,qryMasternCdCategFinanc.AsString);
  PosicionaQuery(qryUnidadeNegocio,qryMasternCdUnidadeNegocio.AsString);
  PosicionaQuery(qryCC,qryMasternCdCC.AsString);

  PosicionaQuery(qryRegraGrupoComissao, qryMasternCdGrupoComissao.asString) ;

end;

procedure TfrmGrupoComissao.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryCategFinanc.Close;
  qryUnidadeNegocio.Close;
  qryCC.Close;

  qryRegraGrupoComissao.Close ;
  
end;

procedure TfrmGrupoComissao.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus ;
  
end;

procedure TfrmGrupoComissao.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DBedit2.Text) = '') then
  begin
      MensagemAlerta('Informe o nome do grupo.') ;
      DBEdit2.SetFocus ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmGrupoComissao.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  if qryMaster.Active and (qryMasternCdGrupoComissao.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmGrupoComissao.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryCategFinanc.Close;
  PosicionaQuery(qryCategFinanc, DBedit4.Text) ;

end;

procedure TfrmGrupoComissao.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  qryCC.Close;
  PosicionaQuery(qryCC, DBedit5.Text) ;

end;

procedure TfrmGrupoComissao.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  qryUnidadeNegocio.Close;
  PosicionaQuery(qryUnidadeNegocio, DBedit6.Text) ;

end;

procedure TfrmGrupoComissao.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(109);

            If (nPK > 0) then
                qryMasternCdCategFinanc.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmGrupoComissao.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(28,'nCdStatus = 1 AND cFlgLanc = 1');

            If (nPK > 0) then
                qryMasternCdCC.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmGrupoComissao.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(110);

            If (nPK > 0) then
                qryMasternCdUnidadeNegocio.Value := nPK ;

        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmGrupoComissao) ;
    
end.
