unit fMRP_FormulaProd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, GridsEh, DBGridEh, DB, ADODB, ImgList,
  ComCtrls, ToolWin, ExtCtrls, Menus, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmMRP_FormulaProd = class(TfrmProcesso_Padrao)
    qryFormula: TADOQuery;
    qryFormulanCdProduto: TIntegerField;
    qryFormulacNmProduto: TStringField;
    qryFormulanQtde: TBCDField;
    qryFormulacFornecHom: TStringField;
    DataSource1: TDataSource;
    PopupMenu1: TPopupMenu;
    FornecedoresHomologados1: TMenuItem;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1nQtde: TcxGridDBColumn;
    cxGrid1DBTableView1cFornecHom: TcxGridDBColumn;
    procedure FornecedoresHomologados1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMRP_FormulaProd: TfrmMRP_FormulaProd;

implementation

uses fMRP_FornecHomol;

{$R *.dfm}

procedure TfrmMRP_FormulaProd.FornecedoresHomologados1Click(
  Sender: TObject);
begin
  inherited;

  PosicionaQuery(frmMRP_FornecHomol.qryFornecedores, qryFormulanCdProduto.AsString) ;
  frmMRP_FornecHomol.ShowModal ;

end;

end.
