unit fLocalizacaoProduto_Vinculo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping;

type
  TfrmLocalizacaoProduto_Vinculo = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryPosicaoEstoque: TADOQuery;
    qryPosicaoEstoquenCdPosicaoEstoque: TAutoIncField;
    qryPosicaoEstoquenCdLocalEstoque: TIntegerField;
    qryPosicaoEstoquenCdProduto: TIntegerField;
    qryPosicaoEstoquecLocalizacao: TStringField;
    qryPosicaoEstoqueiQuadra: TIntegerField;
    qryPosicaoEstoqueiRua: TIntegerField;
    qryPosicaoEstoqueiPrateleira: TIntegerField;
    qryPosicaoEstoqueiBox: TIntegerField;
    dsPosicaoEstoque: TDataSource;
    qryPosicaoEstoquecNmLocalEstoque: TStringField;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    procedure qryPosicaoEstoqueBeforePost(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryPosicaoEstoqueCalcFields(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdProduto : integer ;
  end;

var
  frmLocalizacaoProduto_Vinculo: TfrmLocalizacaoProduto_Vinculo;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmLocalizacaoProduto_Vinculo.qryPosicaoEstoqueBeforePost(
  DataSet: TDataSet);
begin

  If (qryPosicaoEstoquecNmLocalEstoque.Value = '') then
  begin
      MensagemAlerta('Informe o local de estoque.') ;
      abort ;
  end ;

  inherited;

  qryPosicaoEstoquenCdProduto.Value := nCdProduto ;

  if (qryPosicaoEstoquenCdPosicaoEstoque.Value = 0) then
      qryPosicaoEstoquenCdPosicaoEstoque.Value := frmMenu.fnProximoCodigo('POSICAOESTOQUE') ;

  if (qryPosicaoEstoqueiQuadra.Value > 0) then
      qryPosicaoEstoquecLocalizacao.Value := 'Q' + qryPosicaoEstoqueiQuadra.AsString ;

  if (qryPosicaoEstoqueiRua.Value > 0) then
      qryPosicaoEstoquecLocalizacao.Value := trim(qryPosicaoEstoquecLocalizacao.Value) + 'R' + qryPosicaoEstoqueiRua.AsString ;

  if (qryPosicaoEstoqueiPrateleira.Value > 0) then
      qryPosicaoEstoquecLocalizacao.Value := trim(qryPosicaoEstoquecLocalizacao.Value) + 'P' + qryPosicaoEstoqueiPrateleira.AsString ;

  if (qryPosicaoEstoqueiBox.Value > 0) then
      qryPosicaoEstoquecLocalizacao.Value := trim(qryPosicaoEstoquecLocalizacao.Value) + 'B' + qryPosicaoEstoqueiBox.AsString ;

end;

procedure TfrmLocalizacaoProduto_Vinculo.FormShow(Sender: TObject);
begin
  inherited;

  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva ;
  qryPosicaoEstoque.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;

  PosicionaQuery(qryPosicaoEstoque, IntToStr(nCdProduto)) ;

end;

procedure TfrmLocalizacaoProduto_Vinculo.qryPosicaoEstoqueCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryPosicaoEstoquecNmLocalEstoque.Value = '') then
  begin
      qryLocalEstoque.Close ;
      Posicionaquery(qryLocalEstoque, qryPosicaoEstoquenCdLocalEstoque.asString) ;

      if not qryLocalEstoque.eof then
          qryPosicaoEstoquecNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;

  end ;

end;

procedure TfrmLocalizacaoProduto_Vinculo.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryPosicaoEstoque.State = dsBrowse) then
             qryPosicaoEstoque.Edit ;
        

        if (qryPosicaoEstoque.State = dsInsert) or (qryPosicaoEstoque.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva));

            If (nPK > 0) then
            begin
                qryPosicaoEstoquenCdLocalEstoque.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmLocalizacaoProduto_Vinculo.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
{  inherited;}

end;

end.
