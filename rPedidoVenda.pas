unit rPedidoVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, QuickRpt, QRCtrls, jpeg;

type
  TrptPedidoVenda = class(TForm)
    QuickRep1: TQuickRep;
    qryPedido: TADOQuery;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidodDtPrevEntIni: TDateTimeField;
    qryPedidodDtPrevEntFim: TDateTimeField;
    qryPedidonCdEmpresa: TIntegerField;
    qryPedidocNmEmpresa: TStringField;
    qryPedidonCdLoja: TIntegerField;
    qryPedidocNmLoja: TStringField;
    qryPedidonCdTipoPedido: TIntegerField;
    qryPedidocNmTipoPedido: TStringField;
    qryPedidonCdTerceiro: TIntegerField;
    qryPedidocNrPedTerceiro: TStringField;
    qryPedidonCdCondPagto: TIntegerField;
    qryPedidocNmCondPagto: TStringField;
    qryPedidonCdTabStatusPed: TIntegerField;
    qryPedidocNmTabStatusPed: TStringField;
    qryPedidonPercDesconto: TBCDField;
    qryPedidonPercAcrescimo: TBCDField;
    qryPedidonValPedido: TBCDField;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText2: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel9: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel11: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel13: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel14: TQRLabel;
    QRDBText14: TQRDBText;
    QRLabel15: TQRLabel;
    QRLabel7: TQRLabel;
    QRBand2: TQRBand;
    qryItemEstoque_Grade: TADOQuery;
    qryItemEstoque_GradenCdProduto: TIntegerField;
    qryItemEstoque_GradecNmItem: TStringField;
    qryItemEstoque_GradenQtdePed: TBCDField;
    qryItemEstoque_GradenValCustoUnit: TBCDField;
    qryItemEstoque_GradenValTotalItem: TBCDField;
    qryItemEstoque_GradenCdGrade: TIntegerField;
    qryItemEstoque_GradecTituloGrade: TStringField;
    qryItemEstoque_GradecQtdeGrade: TStringField;
    QRSubDetail1: TQRSubDetail;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRGroup2: TQRGroup;
    qryItemAD: TADOQuery;
    qryItemADcCdProduto: TStringField;
    qryItemADcNmItem: TStringField;
    qryItemADcSiglaUnidadeMedida: TStringField;
    qryItemADnQtdePed: TBCDField;
    qryItemADnValCustoUnit: TBCDField;
    qryItemADnValTotalItem: TBCDField;
    QRSubDetail2: TQRSubDetail;
    QRDBText16: TQRDBText;
    QRGroup3: TQRGroup;
    qryItemADnCdPedido: TIntegerField;
    QRShape3: TQRShape;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    qryPedidocNmContato: TStringField;
    QRLabel27: TQRLabel;
    QRDBText21: TQRDBText;
    QRSubDetail3: TQRSubDetail;
    QRGroup4: TQRGroup;
    qryItemFormula: TADOQuery;
    qryItemFormulanCdProduto: TIntegerField;
    qryItemFormulacNmItem: TStringField;
    qryItemFormulanQtdePed: TBCDField;
    qryItemFormulanValCustoUnit: TBCDField;
    qryItemFormulanValTotalItem: TBCDField;
    qryItemFormulacComposicao: TStringField;
    QRShape4: TQRShape;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel35: TQRLabel;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRLabel18: TQRLabel;
    QRLabel17: TQRLabel;
    qryPedidocNmTerceiroRepres: TStringField;
    QRLabel34: TQRLabel;
    QRDBText33: TQRDBText;
    QRShape1: TQRShape;
    qryPedidonValDesconto: TBCDField;
    qryPedidonValAcrescimo: TBCDField;
    qryPedidonValProdutos: TBCDField;
    QRLabel36: TQRLabel;
    QRDBText34: TQRDBText;
    QRLabel37: TQRLabel;
    qryItemFormulanValUnitario: TBCDField;
    qryItemFormulanValDesconto: TBCDField;
    qryItemFormulanValAcrescimo: TBCDField;
    QRLabel38: TQRLabel;
    QRDBText35: TQRDBText;
    QRDBText36: TQRDBText;
    QRLabel39: TQRLabel;
    qryItemEstoque_GradenValUnitario: TBCDField;
    qryItemEstoque_GradenValDesconto: TBCDField;
    qryItemEstoque_GradenValAcrescimo: TBCDField;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRDBText37: TQRDBText;
    QRDBText38: TQRDBText;
    QRLabel45: TQRLabel;
    QRDBText41: TQRDBText;
    QRDBText42: TQRDBText;
    QRLabel46: TQRLabel;
    QRDBText43: TQRDBText;
    QRLabel47: TQRLabel;
    QRDBText44: TQRDBText;
    QRDBText45: TQRDBText;
    QRLabel48: TQRLabel;
    QRDBText46: TQRDBText;
    QRDBText47: TQRDBText;
    QRLabel49: TQRLabel;
    QRDBText48: TQRDBText;
    QRLabel50: TQRLabel;
    QRDBText49: TQRDBText;
    QRLabel51: TQRLabel;
    QRDBText50: TQRDBText;
    QRLabel52: TQRLabel;
    QRDBText51: TQRDBText;
    QRLabel53: TQRLabel;
    QRDBText52: TQRDBText;
    QRLabel54: TQRLabel;
    QRLabel55: TQRLabel;
    QRLabel56: TQRLabel;
    QRDBText53: TQRDBText;
    QRLabel57: TQRLabel;
    QRDBText54: TQRDBText;
    QRLabel58: TQRLabel;
    QRDBText55: TQRDBText;
    QRLabel59: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel42: TQRLabel;
    QRDBText39: TQRDBText;
    qryPedidocAnotacao: TMemoField;
    qryPedidonValDevoluc: TBCDField;
    qryPedidocEnderecoEntrega: TStringField;
    QRDBText56: TQRDBText;
    qryPedidocNmTerceiroFantasia: TStringField;
    QRShape5: TQRShape;
    QRLabel16: TQRLabel;
    QRLabel44: TQRLabel;
    QRShape6: TQRShape;
    SummaryBand1: TQRBand;
    QRShape7: TQRShape;
    QRLabel22: TQRLabel;
    QRShape8: TQRShape;
    QRDBText15: TQRDBText;
    QRShape2: TQRShape;
    QRLabel43: TQRLabel;
    qryPedidonValFrete: TBCDField;
    QRDBText40: TQRDBText;
    QRLabel60: TQRLabel;
    procedure QRGroup2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPedidoVenda: TrptPedidoVenda;

implementation

{$R *.dfm}

procedure TrptPedidoVenda.QRGroup2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

{  QRLabel22.Caption   := 'Grade' ;
  QRSubDetail1.Height := 32 ;

  if (qryItemEstoque_GradecTituloGrade.Value = '') then
  begin
      QRLabel22.Caption := 'Item Estoque' ;
  end ;
 }

  QRSubDetail1.Height := 11 ;

end;

end.
