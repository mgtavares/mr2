object frmQueryExecute: TfrmQueryExecute
  Left = 0
  Top = 0
  Width = 1152
  Height = 770
  Caption = 'Executar SQL'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1136
    Height = 49
    Align = alTop
    BevelInner = bvSpace
    BevelOuter = bvLowered
    TabOrder = 0
    object cxButton1: TcxButton
      Left = 8
      Top = 8
      Width = 89
      Height = 33
      Caption = 'Executar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
      OnClick = cxButton1Click
      Glyph.Data = {
        26010000424D26010000000000003600000028000000070000000A0000000100
        180000000000F000000000000000000000000000000000000000FFFFFF2E373A
        354143FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF363F42929B9E333C3FFFFF
        FFFFFFFFFFFFFF000000FFFFFF2E373AC0C9CC9DA6A9374345FFFFFFFFFFFF00
        0000FFFFFF434A4DC8D1D4CAD3D68D9699353E41FFFFFF000000FFFFFF343B3E
        BCC5C8DEE7EAD7E0E3929B9E3D494B000000FFFFFF363D40CBD4D7D7E0E3D5DE
        E1F6FFFF303C3E000000FFFFFF3F4649B9C2C5D0D9DCF6FFFF343D40FFFFFF00
        0000FFFFFF384144CBD4D7F5FEFF354143FFFFFFFFFFFF000000FFFFFF353E41
        F2FBFE374043FFFFFFE0ECEEFFFFFF000000FFFFFF3B4447323E40FFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
    object cxButton2: TcxButton
      Left = 104
      Top = 8
      Width = 137
      Height = 33
      Caption = 'Exportar Resultado'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = cxButton2Click
      Glyph.Data = {
        26010000424D26010000000000003600000028000000070000000A0000000100
        180000000000F000000000000000000000000000000000000000FFFFFF2E373A
        354143FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF363F42929B9E333C3FFFFF
        FFFFFFFFFFFFFF000000FFFFFF2E373AC0C9CC9DA6A9374345FFFFFFFFFFFF00
        0000FFFFFF434A4DC8D1D4CAD3D68D9699353E41FFFFFF000000FFFFFF343B3E
        BCC5C8DEE7EAD7E0E3929B9E3D494B000000FFFFFF363D40CBD4D7D7E0E3D5DE
        E1F6FFFF303C3E000000FFFFFF3F4649B9C2C5D0D9DCF6FFFF343D40FFFFFF00
        0000FFFFFF384144CBD4D7F5FEFF354143FFFFFFFFFFFF000000FFFFFF353E41
        F2FBFE374043FFFFFFE0ECEEFFFFFF000000FFFFFF3B4447323E40FFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
    object cxButton3: TcxButton
      Left = 248
      Top = 8
      Width = 81
      Height = 33
      Caption = 'Fechar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
      OnClick = cxButton3Click
      Glyph.Data = {
        26010000424D26010000000000003600000028000000070000000A0000000100
        180000000000F000000000000000000000000000000000000000FFFFFF2E373A
        354143FFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF363F42929B9E333C3FFFFF
        FFFFFFFFFFFFFF000000FFFFFF2E373AC0C9CC9DA6A9374345FFFFFFFFFFFF00
        0000FFFFFF434A4DC8D1D4CAD3D68D9699353E41FFFFFF000000FFFFFF343B3E
        BCC5C8DEE7EAD7E0E3929B9E3D494B000000FFFFFF363D40CBD4D7D7E0E3D5DE
        E1F6FFFF303C3E000000FFFFFF3F4649B9C2C5D0D9DCF6FFFF343D40FFFFFF00
        0000FFFFFF384144CBD4D7F5FEFF354143FFFFFFFFFFFF000000FFFFFF353E41
        F2FBFE374043FFFFFFE0ECEEFFFFFF000000FFFFFF3B4447323E40FFFFFFFFFF
        FFFFFFFFFFFFFF000000}
      LookAndFeel.NativeStyle = True
    end
  end
  object Edit1: TRichEdit
    Left = 0
    Top = 49
    Width = 1136
    Height = 144
    Align = alTop
    TabOrder = 1
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 193
    Width = 1136
    Height = 541
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object cxTV: TcxGridDBTableView
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
    end
    object cxGrid1Level1: TcxGridLevel
    end
  end
  object PrintDBGridEh1: TPrintDBGridEh
    Options = []
    PageFooter.Font.Charset = DEFAULT_CHARSET
    PageFooter.Font.Color = clWindowText
    PageFooter.Font.Height = -11
    PageFooter.Font.Name = 'MS Sans Serif'
    PageFooter.Font.Style = []
    PageHeader.Font.Charset = DEFAULT_CHARSET
    PageHeader.Font.Color = clWindowText
    PageHeader.Font.Height = -11
    PageHeader.Font.Name = 'MS Sans Serif'
    PageHeader.Font.Style = []
    Units = MM
    Left = 256
    Top = 288
  end
  object qryResultado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 376
    Top = 288
  end
  object dsResultado: TDataSource
    DataSet = qryResultado
    Left = 416
    Top = 288
  end
  object cxStyleRepository1: TcxStyleRepository
    Left = 464
    Top = 376
    object cxStyle1: TcxStyle
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 384
    Top = 344
  end
end
