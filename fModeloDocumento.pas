unit fModeloDocumento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons,
  cxPC, cxControls, OleCtrls, SHDocVw, Activex, MsHtml;

type
  TfrmModeloDocumento = class(TfrmCadastro_Padrao)
    qryTabTipoMod: TADOQuery;
    qryTabTipoModnCdTabTipoModeloDoc: TIntegerField;
    qryTabTipoModcNmTabTipoModeloDoc: TStringField;
    qryMasternCdModeloDocumento: TIntegerField;
    qryMastercNmModeloDocumento: TStringField;
    qryMastercArquivoModeloDoc: TStringField;
    qryMastercDescricaoModeloDoc: TMemoField;
    qryMasternCdStatus: TIntegerField;
    qryMasternCdTabTipoModeloDoc: TIntegerField;
    dsTabTipoModeloDoc: TDataSource;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    dsStat: TDataSource;
    qryMastercNmStatus: TStringField;
    qryMastercNmTabTipoModeloDoc: TStringField;
    cxPageControl1: TcxPageControl;
    tabDescricao: TcxTabSheet;
    Panel2: TPanel;
    btEditorHTML: TcxButton;
    WebBrowser1: TWebBrowser;
    tabMemo: TcxTabSheet;
    DBMemo1: TDBMemo;
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    cxTreeView: TTreeView;
    qryTerceiroOrigem: TADOQuery;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    qryEmpresa: TADOQuery;
    qryEmpresacNmEmpresa: TStringField;
    qryEmpresacSigla: TStringField;
    qryEmpresaimgLogoEmpresaGrande: TBlobField;
    qryEnderecoOrigem: TADOQuery;
    qryTerceiroOrigemnCdTerceiroOrigem: TIntegerField;
    qryTerceiroOrigemcNmTerceiroOrigem: TStringField;
    qryTerceiroOrigemcEmailOrigem: TStringField;
    qryTerceiroOrigemcEmailNFeOrigem: TStringField;
    qryTerceiroDestino: TADOQuery;
    qryTerceiroDestinonCdTerceiroDestino: TIntegerField;
    qryTerceiroDestinocNmTerceiroDestino: TStringField;
    qryTerceiroDestinocEmailDestino: TStringField;
    qryTerceiroDestinocEmailNFeDestino: TStringField;
    qryEnderecoOrigemcEnderecoOrigem: TStringField;
    qryEnderecoDestino: TADOQuery;
    qryEnderecoDestinocEnderecoDestino: TStringField;
    qryDoctoFiscal: TADOQuery;
    qryDoctoFiscalcChaveNFe: TStringField;
    qryDoctoFiscaliNrDocto: TIntegerField;
    qryDoctoFiscaldDtEmissao: TDateTimeField;
    qryDoctoFiscalnValTotal: TBCDField;
    qryDoctoFiscalcNmCondPagto: TStringField;
    qryDoctoFiscalcNmFormaPagto: TStringField;
    qryDoctoFiscalcNmStatus: TStringField;
    qryTitulo: TADOQuery;
    qryTitulonCdTitulo: TIntegerField;
    qryTitulocNrTit: TStringField;
    qryTitulonValTit: TBCDField;
    qryTitulonValDesconto: TBCDField;
    qryTitulonValJuro: TBCDField;
    qryTitulonValAbatimento: TBCDField;
    qryTitulonSaldoTit: TBCDField;
    qryTitulodDtVenc: TDateTimeField;
    qryTemp: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btEditorHTMLClick(Sender: TObject);
    procedure CarregarHTML(cCodigoHTML : String);
    procedure btIncluirClick(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure cxTreeViewDblClick(Sender: TObject);
    function  fnSubistituiVariaveis(codMod , nCdTerceiro: Integer; arr : Array of String) : String;
    function  fnTrataTipoModDoc(cVar : String; iTipoModDoc : Integer) : String;
    function  fnStringReplace(cVar, cConteudo, cValor : String) : String;
    function  fnRetornaRodape(nTipoModDoc : Integer) : String;
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    codModelo : String;

  public
    { Public declarations }
  end;

var
  frmModeloDocumento: TfrmModeloDocumento;

implementation

{$R *.dfm}

uses
  fMenu, fLookup_Padrao, fEditorHTML;

function TfrmModeloDocumento.fnRetornaRodape(nTipoModDoc : Integer) : String;
var
  cTexto : String;
begin
    {-- Caso o Documento seja HTML --}
    if (nTipoModDoc = 2) then
    begin
        cTexto := '<P></P><HR>'
                + '<TABLE BORDER=0 align=center>'
                + '<TR>'
                + '<TD>'
                + '<IMG src=./er2soft.jpg>'
                + '</TD>'
                + '<TD><P><FONT FACE=VERDANA SIZE=1>'
                + '<P align=left>Desenvolvido por ER2Soft Inform�tica - Solu��es Inteligentes para o seu neg�cio<BR>'
                + 'http://www.er2soft.com.br - (11) 2378-3200</P>'
                + '</FONT></P></TD>'
                + '</TR>'
                + '</TABLE>';
    end
    {-- Caso o Documento seja Texto --}
    else if (nTipoModDoc = 1) then
    begin
        cTexto := #13 + 'Desenvolvido por ER2Soft Inform�tica - Solu��es Inteligentes para o seu neg�cio.' + #13
                + 'http://www.er2soft.com.br' + #13
                + '(11) 2378-3200';
    end;
    // Retorna o texto do rodap�
    Result := cTexto;
end;
function TfrmModeloDocumento.fnStringReplace(cVar, cConteudo, cValor : String) : String;
begin

    if POS( fnTrataTipoModDoc(cVar, qryMasternCdTabTipoModeloDoc.Value), cConteudo) > 0 then
        cConteudo := StringReplace(cConteudo, fnTrataTipoModDoc(cVar, qryMasternCdTabTipoModeloDoc.Value), cValor, [rfReplaceAll, rfIgnoreCase]);

    Result := cConteudo;

end;

function TfrmModeloDocumento.fnTrataTipoModDoc(cVar : String; iTipoModDoc : Integer) : String;
begin
  {-- Verifica o tipo de documento: 1 = txt ; 2 = html --}
  if (iTipoModDoc = 1) then
      Result := '&' + cVar + '&'
  else if (iTipoModDoc = 2) then
      Result := '&amp;' + cVar + '&amp;';
end;
function TfrmModeloDocumento.fnSubistituiVariaveis(codMod,nCdTerceiro : Integer; arr : Array of String) : String;
var
  I            : Integer;
  count        : Integer;
  Split        : TStringList;
  cConteudo    : String;
  cPathArquivo : String;
  cValAux      : String;
  cTituloText  : String;
begin
    {-- Metodo para subistituir as variaveis do Modelo --}

    cPathArquivo := ExtractFilePath(Application.ExeName) + 'Arquivos\ModeloDocumento\' + IntToStr(codMod);

    Split := TStringList.Create;
    Split.Delimiter := ',';

    {-- Carrega o conteudo para subistui��o --}
    qryMaster.Close;
    qryMaster.Parameters.ParamByName('nPK').Value := codMod;
    qryMaster.Open;
    cConteudo := qryMastercDescricaoModeloDoc.Value;

    for I := 0 to Length(arr) - 1 do
    begin
        {-- Distrincha o Array para encontrar o valor separado por virgula --}
        Split.DelimitedText := arr[I];

        {-- Empresa --}
        if (Split[0] = 'Empresa') then
        begin
            qryEmpresa.Close;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := Split[1];
            qryEmpresa.Open;

            cConteudo := fnStringReplace('descricaoEmp', cConteudo, qryEmpresacNmEmpresa.Value);
            cConteudo := fnStringReplace('sigla', cConteudo, qryEmpresacSigla.Value);
        end;

        {-- Loja --}
        if (Split[0] = 'Loja') then
        begin
            qryLoja.Close;
            qryLoja.Parameters.ParamByName('nCdLoja').Value := Split[1];
            qryLoja.Open;
            cConteudo := fnStringReplace('descricaoLoja', cConteudo, qryLojacNmLoja.Value);
        end;

        {-- Terceiro Origem --}
        if (Split[0] = 'TerceiroOrigem') then
        begin
            qryTerceiroOrigem.Close;
            qryTerceiroOrigem.Parameters.ParamByName('nCdTerceiro').Value := Split[1];
            qryTerceiroOrigem.Open;

            {-- Endere�o --}
            qryEnderecoOrigem.Close;
            qryEnderecoOrigem.Parameters.ParamByName('nCdTerceiro').Value := Split[1];
            qryEnderecoOrigem.Open;

            cConteudo := fnStringReplace('nomeOrigem',      cConteudo, qryTerceiroOrigemcNmTerceiroOrigem.Value);
            cConteudo := fnStringReplace('emailOrigem',     cConteudo, qryTerceiroOrigemcEmailOrigem.Value);
            cConteudo := fnStringReplace('emailNFe',        cConteudo, qryTerceiroOrigemcEmailNFeOrigem.Value);
            cConteudo := fnStringReplace('enderecoOrigem',  cConteudo, qryEnderecoOrigemcEnderecoOrigem.Value);

        end;
        {-- Terceiro Destino --}
        if (Split[0] = 'TerceiroDestino') then
        begin
            qryTerceiroDestino.Close;
            qryTerceiroDestino.Parameters.ParamByName('nCdTerceiro').Value := Split[1];
            qryTerceiroDestino.Open;

            {-- Endere�o --}
            qryEnderecoDestino.Close;
            qryEnderecoDestino.Parameters.ParamByName('nCdTerceiro').Value := Split[1];
            qryEnderecoDestino.Open;

            cConteudo := fnStringReplace('nomeDestino',     cConteudo, qryTerceiroDestinocNmTerceiroDestino.Value);
            cConteudo := fnStringReplace('emailDestino',    cConteudo, qryTerceiroDestinocEmailDestino.Value);
            cConteudo := fnStringReplace('emailNFe',        cConteudo, qryTerceiroDestinocEmailNFeDestino.Value);
            cConteudo := fnStringReplace('enderecoDestino', cConteudo, qryEnderecoDestinocEnderecoDestino.Value);
        end;

        {-- Docto Fiscal --}
        if (Split[0] = 'DoctoFiscal') then
        begin
            qryDoctoFiscal.Close;
            qryDoctoFiscal.Parameters.ParamByName('nCdDoctoFiscal').Value := Split[1];
            qryDoctoFiscal.Open;

            cConteudo := fnStringReplace('chaveNFE',    cConteudo, qryDoctoFiscalcChaveNFe.Value);
            cConteudo := fnStringReplace('numDocto',    cConteudo, IntToStr(qryDoctoFiscaliNrDocto.Value) );
            cConteudo := fnStringReplace('dataEmissao', cConteudo, DateTimeToStr(qryDoctoFiscaldDtEmissao.Value) );
            cConteudo := fnStringReplace('valor',       cConteudo, CurrToStr(qryDoctoFiscalnValTotal.Value) );
            cConteudo := fnStringReplace('formaPagto',  cConteudo, qryDoctoFiscalcNmFormaPagto.Value);
            cConteudo := fnStringReplace('condPagto',   cConteudo, qryDoctoFiscalcNmCondPagto.Value);
            cConteudo := fnStringReplace('status',      cConteudo, qryDoctoFiscalcNmStatus.Value);
        end;

        {-- T�tulo --}
        if (Split[0] = 'Titulo') then
        begin
            qryTitulo.close;
            qryTitulo.Parameters.ParamByName('nPK').Value := nCdTerceiro;
            qryTitulo.Open;

            {-- Se tiver Loop para o t�tulo --}
            if ( pos('tituloText', cConteudo) > 0 ) then
            begin
                
                cTituloText := '<p style=' + #39 + 'padding: 5px; border: 1px solid #ebebeb' + #39 + '> &amp;nrTitulo&amp; - R$ &amp;valTitulo&amp; � vencer em &amp;dtVenc&amp;' + #13 + '&amp;tituloText&amp;</p>';
                {-- repete at� inserir todos os t�tulos encontrados --}
                repeat
                    cConteudo := fnStringReplace('tituloText',    cConteudo, cTituloText);
                    cConteudo := fnStringReplace('codTitulo',     cConteudo, qryTitulonCdTitulo.AsString);
                    cConteudo := fnStringReplace('nrTitulo',      cConteudo, qryTitulocNrTit.Value);
                    cConteudo := fnStringReplace('valTitulo',     cConteudo, CurrToStr(qryTitulonValTit.Value) );
                    cConteudo := fnStringReplace('valDesc',       cConteudo, CurrToStr(qryTitulonValDesconto.Value) );
                    cConteudo := fnStringReplace('valJuro',       cConteudo, CurrToStr(qryTitulonValTit.Value) );
                    cConteudo := fnStringReplace('valAbat',       cConteudo, CurrToStr(qryTitulonValAbatimento.Value) );
                    cConteudo := fnStringReplace('saldoTit',      cConteudo, CurrToStr(qryTitulonSaldoTit.Value) );
                    cConteudo := fnStringReplace('dtVenc',        cConteudo, DateTimeToStr(qryTitulodDtVenc.Value));
                    
                    qryTitulo.Next;

                until (qryTitulo.Eof);

                cConteudo := fnStringReplace('tituloText', cConteudo, ''); // limpa tituloText do documento

            end;
        end;

    end; // fim do for

    {-- troca o caminho dos anexos --}
    cConteudo := StringReplace(cConteudo, cPathArquivo, './', [rfReplaceAll, rfIgnoreCase]);
    Result    := cConteudo + fnRetornaRodape(qryMasternCdTabTipoModeloDoc.Value);
end;

procedure TfrmModeloDocumento.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster           := 'MODELODOCUMENTO';
  nCdTabelaSistema          := 543;
  nCdConsultaPadrao         := 784;
  bLimpaAposSalvar          := False;
  tabMemo.Enabled           := False;
  tabDescricao.Enabled      := False;
  cxPageControl1.ActivePage := tabMemo;
end;

procedure TfrmModeloDocumento.CarregarHTML(cCodigoHTML : String);
var
   sl            : TStringList;
   ms            : TMemoryStream;
   cHTMLOriginal : String;
begin
  sl := TStringList.Create;
  ms := TMemoryStream.Create;

  cHTMLOriginal := cCodigoHTML;
  WebBrowser1.Navigate('about:blank');
  while (WebBrowser1.ReadyState < READYSTATE_INTERACTIVE) do
      Application.ProcessMessages;

  if Assigned(WebBrowser1.Document) then
  begin
      try
          try
              sl.Text := cCodigoHTML;
              sl.SaveToStream(ms);
              ms.Seek(0,0);
             (WebBrowser1.Document as IPersistStreamInit).Load(TStreamAdapter.Create(ms)) ;
          finally
              ms.Free;
          end;
      finally
          sl.Free;
      end;
  end;
end;

procedure TfrmModeloDocumento.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;
  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(785);

            If (nPK > 0) then
            begin
                qryMasternCdTabTipoModeloDoc.Value := nPK;
            end ;

        end ;

    end ;

  end ;
  
end;

procedure TfrmModeloDocumento.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);

            If (nPK > 0) then
            begin
                qryMasternCdStatus.Value := nPK;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmModeloDocumento.btEditorHTMLClick(Sender: TObject);
var
  objEditorHtml : TfrmEditorHTML;
begin
  inherited;
  if (trim(DBEdit2.Text) = '1') then
      ShowMessage('Editor de Textos')
  else if(trim(DBEdit2.Text) = '2') then
  begin
      {-- Habilita o Editor HTML --}
      objEditorHtml := TfrmEditorHTML.Create(Nil);
      {-- Passa HTML Salvo no banco para o editor --}
      objEditorHtml.CarregarHTML(qryMastercDescricaoModeloDoc.Value);
      ShowForm(objEditorHtml, false);
      qryMastercDescricaoModeloDoc.Value := objEditorHtml.GeraHTML(objEditorHtml.webCodigoHTML);
      CarregarHTML(qryMastercDescricaoModeloDoc.Value);
  end
  else
  begin
      MensagemAlerta('Selecione um tipo de documento para editar.');
      DBEdit2.SetFocus;
      Abort;
  end;
end;

procedure TfrmModeloDocumento.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit3.SetFocus;
end;

procedure TfrmModeloDocumento.btSalvarClick(Sender: TObject);
var
  i             : Integer;
  HTMLDocumento : IHTMLDocument2;
  cPathArquivo  : String;
  cFileName     : String;
  cCaminho      : String;
  HTMLBody      : TStringList;
  HTMLDOC       : IHTMLDocument3;
begin
  inherited;

  cPathArquivo := 'Arquivos\ModeloDocumento\' + qryMasternCdModeloDocumento.AsString + '\';
  {-- Se o diretorio n�o existe, cria Ele com o nome do C�digo do Modelo--}
  if not DirectoryExists(frmMenu.cPathSistema + cPathArquivo) then
      CreateDir(frmMenu.cPathSistema + cPathArquivo);

  {-- Caso n�o exista um Tipo --}
  if (trim(DBEdit5.Text) = '') then
  begin
      MensagemAlerta('Informe o c�digo do tipo de documento.');
      DBEdit2.SetFocus;
      Abort;
  end;

  {-- Salva em .TXT --}
  if (DBEdit2.Text = '1') then
  begin
      DBMemo1.Lines.SaveToFile(frmMenu.cPathSistema + cPathArquivo + 'ModeloDoc.txt');
      qryMaster.Edit;
      qryMastercArquivoModeloDoc.Value := frmMenu.cPathSistema + cPathArquivo + 'ModeloDoc.txt';
      qryMaster.Post;
  end;

  {-- Salva em .HTML --}
  HTMLDocumento := WebBrowser1.Document as IHTMLDocument2;
  if (DBEdit2.Text = '2') then
  begin
      {-- Salva as imagens caso tenha --}
      if(HTMLDocumento.images.length > 0 ) then
      begin
          {-- Varre o Webbrowser e pega as imagens --}
          for i := 0 to HTMLDocumento.images.length - 1 do
          begin
              cFileName := (HTMLDocumento.images.item(i, 0) as IHTMLImgElement).nameProp; // nome do arquivo
              cCaminho  := (HTMLDocumento.images.item(i, 0) as IHTMLImgElement).alt; // Caminho do arquivo

              {-- Transfere a imagem para a pasta com o nome do arquivo salvo --}
              CopyFile(PChar(cCaminho), PChar(frmMenu.cPathSistema + cPathArquivo + cFileName), true);
              {-- Tranfere logo da ER2Soft para Pasta do Documento --}
              CopyFile(PChar(frmMenu.cPathSistema + 'Arquivos\ModeloDocumento\Imagens\er2soft.jpg'), PChar(frmMenu.cPathSistema + cPathArquivo + 'er2soft.jpg'), true);

              {-- Altera o valor de Src e Alt local --}
              (HTMLDocumento.images.item(i, 0) as IHTMLImgElement).src := frmMenu.cPathSistema + cPathArquivo + cFileName; // Coloca apenas o nome do arquivo
              (HTMLDocumento.images.item(i, 0) as IHTMLImgElement).alt := frmMenu.cPathSistema + cPathArquivo + cFileName; // Coloca o novo caminho
          end;
      end;

      {-- Salva Arquivo --}
      HTMLDOC := WebBrowser1.Document as IHTMLDocument3;
      try
          HTMLBody      := TStringList.Create;
          HTMLBody.Text := '<HTML>' + HTMLDOC.documentElement.innerHTML + '</HTML>';
          HTMLBody.SaveToFile(frmMenu.cPathSistema + cPathArquivo + 'ModeloDoc.html');
      finally

          {-- Abre o Arquivo Salvo para previsualiza��o --}
          WebBrowser1.Navigate(frmMenu.cPathSistema + cPathArquivo + 'ModeloDoc.html');

          {-- Troca o caminho das imagens, e insere o caminho do arquivo no Banco de Dados --}
          qryMaster.Edit;
          qryMastercDescricaoModeloDoc.Value := HTMLBody.Text;
          qryMastercArquivoModeloDoc.Value   := cPathArquivo + 'ModeloDoc.html';
          qryMaster.Post;

          HTMLBody.Free;
      end;
  end;
 end;
procedure TfrmModeloDocumento.DBEdit2Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  {-- Verifica se o do C�d. do Tipo de Documento � v�lido --}
  if (trim(DBEdit5.Text) = '') then
  begin
      MensagemAlerta('Informe o c�digo do tipo de documento.');
      DBEdit2.SetFocus;
      Abort;
  end;

  case (qryMasternCdTabTipoModeloDoc.Value) of
      1 : begin
          tabMemo.Enabled           := true;
          tabDescricao.Enabled      := false;
          cxPageControl1.ActivePage := tabMemo;
      end;
      2 : begin
          CarregarHTML(qryMastercDescricaoModeloDoc.Value);
          tabMemo.Enabled           := false;
          tabDescricao.Enabled      := true;
          cxPageControl1.ActivePage := tabDescricao;
      end;
  end;
end;

procedure TfrmModeloDocumento.btCancelarClick(Sender: TObject);
begin
  inherited;
  WebBrowser1.Navigate('about:blank');
  DBMemo1.Clear;
end;

procedure TfrmModeloDocumento.cxTreeViewDblClick(Sender: TObject);
var
  cGrupos : TStringList;
  I       : Integer;
begin
  inherited;
  {-- cria lista de grupos --}
  cGrupos := TStringList.Create;
  cGrupos.add('DoctoFiscal');
  cGrupos.add('Empresa');
  cGrupos.add('Loja');
  cGrupos.add('Terceiro');

  {-- Verifica se o usuario n�o clicou em cima dos grupos --}
  if (not cGrupos.Find(cxTreeView.Selected.Text, I)) then
  begin
      {-- Caso tenha algum texto selecionado, subistitui pela variavel --}
      if(DBMemo1.SelText <> '') then
      begin
          DBMemo1.SelText := cxTreeView.Selected.Text;
          DBMemo1.SetFocus;
          abort;
      end;
      {-- Add ao memo --}
      DBMemo1.Lines.Add(cxTreeView.Selected.Text);
  end;
end;

procedure TfrmModeloDocumento.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  case (qryMasternCdTabTipoModeloDoc.Value) of
      1 : begin
          tabMemo.Enabled           := true;
          tabDescricao.Enabled      := false;
          cxPageControl1.ActivePage := tabMemo;
      end;
      2 : begin
          CarregarHTML(qryMastercDescricaoModeloDoc.Value);
          tabMemo.Enabled           := false;
          tabDescricao.Enabled      := true;
          cxPageControl1.ActivePage := tabDescricao;
      end;
  end;
end;

procedure TfrmModeloDocumento.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(DBEdit3.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o do modelo de documento.');
      DBEdit3.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit5.Text) = '') then
  begin
      MensagemAlerta('Informe o c�digo do tipo de documento.');
      DBEdit2.SetFocus;
      Abort;
  end;

  if (Trim(DBEdit6.Text) = '') then
  begin
      MensagemAlerta('Informe o status do modelo de documento.');
      DBEdit4.SetFocus;
      Abort;
  end;

  inherited;
end;

initialization RegisterClass(TfrmModeloDocumento);

end.
