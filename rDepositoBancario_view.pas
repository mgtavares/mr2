unit rDepositoBancario_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptDepositoBancario_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    qryDepositoBancario: TADOQuery;
    cBancoCheque: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    nValCheque: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    qryDepositoBancarionCdDepositoBancario: TStringField;
    qryDepositoBancarionCdEmpresa: TIntegerField;
    qryDepositoBancarionCdLoja: TIntegerField;
    qryDepositoBancariocNmLoja: TStringField;
    qryDepositoBancariodDtMov: TDateTimeField;
    qryDepositoBancariodDtDeposito: TDateTimeField;
    qryDepositoBancariodDtProcesso: TDateTimeField;
    qryDepositoBancarionCdContaBancaria: TIntegerField;
    qryDepositoBancarionCdUsuario: TIntegerField;
    qryDepositoBancarionValDinheiro: TBCDField;
    qryDepositoBancarionValChequeNumerario: TBCDField;
    qryDepositoBancarionValChequePreDatado: TBCDField;
    qryDepositoBancarionValTotalDeposito: TBCDField;
    qryDepositoBancariocOBS: TMemoField;
    qryDepositoBancarioiQtdeCheques: TIntegerField;
    qryDepositoBancariocNmStatus: TStringField;
    qryDepositoBancarionCdBanco: TIntegerField;
    qryDepositoBancariocNmTitular: TStringField;
    qryDepositoBancariocNmConta: TStringField;
    qryDepositoBancariocNmBanco: TStringField;
    qryDepositoBancariocBancoCheque: TStringField;
    qryDepositoBancariocAgenciaCheque: TStringField;
    qryDepositoBancarionCdContaCheque: TStringField;
    qryDepositoBancarioiNrCheque: TStringField;
    qryDepositoBancariodDtDeposito_1: TDateTimeField;
    qryDepositoBancariocTipoCheque: TStringField;
    qryDepositoBancariocCNPJCPF: TStringField;
    qryDepositoBancariodDtCancel: TDateTimeField;
    QRBand6: TQRBand;
    QRLabel6: TQRLabel;
    QRDBText11: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRDBText23: TQRDBText;
    QRLabel13: TQRLabel;
    QRLabel25: TQRLabel;
    QRDBText24: TQRDBText;
    QRLabel26: TQRLabel;
    QRDBText25: TQRDBText;
    QRLabel27: TQRLabel;
    QRDBText26: TQRDBText;
    QRLabel28: TQRLabel;
    QRDBText30: TQRDBText;
    qryDepositoBancarioiTipo: TIntegerField;
    QRGroup2: TQRGroup;
    QRLabel54: TQRLabel;
    lbBanco: TQRLabel;
    lbAgencia: TQRLabel;
    lbConta: TQRLabel;
    lbNrCheque: TQRLabel;
    lblCancelado: TQRLabel;
    lbCPFCNPJ: TQRLabel;
    lbDtDeposito: TQRLabel;
    QRShape12: TQRShape;
    qryDepositoBancarionCdContaBancariaDep: TStringField;
    QRBand5: TQRBand;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    QRBand4: TQRBand;
    nCdContaBancariaDep: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    qryDepositoBancarionValDeposito: TBCDField;
    lbTipo: TQRLabel;
    QRBand2: TQRBand;
    QRLabel5: TQRLabel;
    QRLabel12: TQRLabel;
    QRExpr1: TQRExpr;
    QRLabel14: TQRLabel;
    qryDepositoBancariocIdentificacaoDepositoBanco: TStringField;
    QRLabel16: TQRLabel;
    QRDBText1: TQRDBText;
    procedure QRLabel54Print(sender: TObject; var Value: String);
    procedure QRGroup2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure lbBancoPrint(sender: TObject; var Value: String);
    procedure lbAgenciaPrint(sender: TObject; var Value: String);
    procedure lbContaPrint(sender: TObject; var Value: String);
    procedure lbNrChequePrint(sender: TObject; var Value: String);
    procedure lbCPFCNPJPrint(sender: TObject; var Value: String);
    procedure lbDtDepositoPrint(sender: TObject; var Value: String);
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QRBand2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptDepositoBancario_view: TrptDepositoBancario_view;

implementation

{$R *.dfm}

procedure TrptDepositoBancario_view.QRLabel54Print(sender: TObject;
  var Value: String);
begin
  {if (qryDepositoBancarioiTipo.Value = 1) then
      Value := '*--- RELA��O DE SANGRIAS NESTE DEP�SITO ---*'
  else
      Value := '*--- RELA��O DE CHEQUES NESTE DEP�SITO ---*'; }

end;

procedure TrptDepositoBancario_view.QRGroup2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
{  if (qryDepositoBancarioiTipo.Value = 1) then
      QRLabel2.Caption := 'Conta Banc�ria';  }
end;

procedure TrptDepositoBancario_view.lbBancoPrint(sender: TObject;
  var Value: String);
begin
 { if (qryDepositoBancarioiTipo.Value = 1) then
      Value := 'Conta Banc�ria'
  else
      Value := 'Banco';}
end;

procedure TrptDepositoBancario_view.lbAgenciaPrint(sender: TObject;
  var Value: String);
begin
  {if (qryDepositoBancarioiTipo.Value = 1) then
      Value := ''
  else
      Value := 'Ag�ncia'; }
end;

procedure TrptDepositoBancario_view.lbContaPrint(sender: TObject;
  var Value: String);
begin
  {if (qryDepositoBancarioiTipo.Value = 1) then
      Value := ''
  else
      Value := 'Conta';}
end;

procedure TrptDepositoBancario_view.lbNrChequePrint(sender: TObject;
  var Value: String);
begin
  {if (qryDepositoBancarioiTipo.Value = 1) then
      Value := ''
  else
      Value := 'Nr. Cheque'; }
end;

procedure TrptDepositoBancario_view.lbCPFCNPJPrint(sender: TObject;
  var Value: String);
begin
 { if (qryDepositoBancarioiTipo.Value = 1) then
      Value := ''
  else
      Value := 'CPF/CNPJ';    }
end;

procedure TrptDepositoBancario_view.lbDtDepositoPrint(sender: TObject;
  var Value: String);
begin
 { if (qryDepositoBancarioiTipo.Value = 1) then
      Value := ''
  else
      Value := 'Data de Dep�sito'; }
end;

procedure TrptDepositoBancario_view.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  {if (qryDepositoBancarioiTipo.Value = 1) then
  begin
      nCdContaBancariaDep.Visible := True;

      cBancoCheque.Visible        := False;
      nValorDinheiro.Mask         := '#,##0.00';
      nValCheque.Mask             := '';
  end
  else
  begin
      nCdContaBancariaDep.Visible := False;
      cBancoCheque.Visible        := True;
      nValorDinheiro.Mask         := '';
      nValCheque.Mask             := '#,##0.00';
  end;}
end;

procedure TrptDepositoBancario_view.QRBand2BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  {if (qryDepositoBancarioiTipo.Value = 1) then
      PrintBand := False; }
end;

procedure TrptDepositoBancario_view.FormActivate(Sender: TObject);
begin
  {nCdContaBancariaDep.Left := 8;
  nCdContaBancariaDep.Top  := 0;
  nValorDinheiro.Left      := 440;
  nValorDinheiro.Top       := 0; }
end;

end.
