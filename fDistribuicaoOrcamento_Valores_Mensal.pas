unit fDistribuicaoOrcamento_Valores_Mensal;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  StdCtrls, Mask, DBCtrls, ADODB, cxPC, cxControls, DBGridEhGrouping,
  GridsEh, DBGridEh;

type
  TfrmDistribuicaoOrcamento_Valores_Mensal = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    qryItemOrcamentoTemp: TADOQuery;
    qryItemOrcamentoTempnCdItemOrcamento: TIntegerField;
    qryItemOrcamentoTempnCdPlanoConta: TIntegerField;
    qryItemOrcamentoTempcNmMetodoDistribOrcamento: TStringField;
    qryItemOrcamentoTempnValorOrcamentoConta: TBCDField;
    qryItemOrcamentoTempcNmPlanoConta: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxPageControl2: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    qryItemOrcamento: TADOQuery;
    qryItemOrcamentonCdItemOrcamento: TIntegerField;
    qryItemOrcamentonCdOrcamento: TIntegerField;
    qryItemOrcamentonCdPlanoConta: TIntegerField;
    qryItemOrcamentonCdPlanoContaBase: TIntegerField;
    qryItemOrcamentonPercentBase: TBCDField;
    qryItemOrcamentonCdMetodoDistribOrcamento: TIntegerField;
    qryItemOrcamentonValorOrcamentoConta: TBCDField;
    qryItemOrcamentocOBS: TStringField;
    qryItemOrcamentocOBSGeral: TMemoField;
    qryItemOrcamentocFlgGerarRateio: TIntegerField;
    DBMemo1: TDBMemo;
    DataSource2: TDataSource;
    DBGridEh1: TDBGridEh;
    qryItemOrcamentoMes: TADOQuery;
    qryItemOrcamentoMesnCdItemOrcamentoMes: TIntegerField;
    qryItemOrcamentoMesnCdItemOrcamento: TIntegerField;
    qryItemOrcamentoMesnCdTabTipoMes: TIntegerField;
    qryItemOrcamentoMesnValOrcamento: TBCDField;
    qryItemOrcamentoMesnPercDistrib: TBCDField;
    qryItemOrcamentoMescOBS: TStringField;
    dsItemOrcamentoMes: TDataSource;
    qryItemOrcamentoMescNmTabTipoMes: TStringField;
    qryTabTipoMes: TADOQuery;
    qryTabTipoMescNmTabTipoMes: TStringField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    procedure exibeDistribuicao (nCdItemOrcamento : integer);
    procedure FormShow(Sender: TObject);
    procedure qryItemOrcamentoMesCalcFields(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDistribuicaoOrcamento_Valores_Mensal: TfrmDistribuicaoOrcamento_Valores_Mensal;

implementation

{$R *.dfm}

{ TfrmDistribuicaoOrcamento_Valores_Mensal }

procedure TfrmDistribuicaoOrcamento_Valores_Mensal.exibeDistribuicao(
  nCdItemOrcamento: integer);
begin

    qryItemOrcamentoTemp.Close;
    PosicionaQuery( qryItemOrcamentoTemp , intToStr( nCdItemOrcamento ) ) ;

    qryItemOrcamento.Close;
    PosicionaQuery( qryItemOrcamento , intToStr( nCdItemOrcamento ) ) ;

    qryItemOrcamentoMes.Close;
    PosicionaQuery( qryItemOrcamentoMes , intToStr( nCdItemOrcamento ) ) ;

    Self.ShowModal ;
    
end;

procedure TfrmDistribuicaoOrcamento_Valores_Mensal.FormShow(
  Sender: TObject);
begin
  inherited;

  DBGridEh1.Col := 5 ;
  DBGridEh1.SetFocus ;

end;

procedure TfrmDistribuicaoOrcamento_Valores_Mensal.qryItemOrcamentoMesCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  if (qryItemOrcamentoMescNmTabTipoMes.Value = '') then
  begin
      qryTabTipoMes.Close;
      PosicionaQuery(qryTabTipoMes , qryItemOrcamentoMesnCdTabTipoMes.asString) ;

      if not qryTabTipoMes.eof then
          qryItemOrcamentoMescNmTabTipoMes.Value := qryTabTipoMescNmTabTipoMes.Value ;
          
  end ;

end;

procedure TfrmDistribuicaoOrcamento_Valores_Mensal.FormKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmDistribuicaoOrcamento_Valores_Mensal.FormKeyUp(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmDistribuicaoOrcamento_Valores_Mensal.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (DBGridEh1.SumList.SumCollection.Items[2].SumValue <> qryItemOrcamentoTempnValorOrcamentoConta.Value) then
  begin

      MensagemAlerta('Total Distribuido n�o confere com o or�amento total da conta.') ;
      abort ;

  end ;

  qryItemOrcamentoMes.First ;

  while not qryItemOrcamentoMes.eof do
  begin

      if (qryItemOrcamentoMesnValOrcamento.Value <= 0) then
      begin
          MensagemAlerta('O Or�amento mensal deve ser maior que zero.') ;
          abort ;
      end ;

      qryItemOrcamentoMes.Edit;
      qryItemOrcamentoMesnPercDistrib.Value := (qryItemOrcamentoMesnValOrcamento.Value / qryItemOrcamentoTempnValorOrcamentoConta.Value) * 100;
      qryItemOrcamentoMes.Next ;
  end ;

  qryItemOrcamentoMes.First ;

  {-- envia as altera��es para o banco de dados, a query qryItemOrcamentoMes trabalha com Batch no LockType --}
  qryItemOrcamentoMes.UpdateBatch();

  if (qryItemOrcamento.State = dsEdit) then
      qryItemOrcamento.Post ;
  
  Close;

end;

procedure TfrmDistribuicaoOrcamento_Valores_Mensal.ToolButton5Click(
  Sender: TObject);
begin
  inherited;

  if (DBGridEh1.SumList.SumCollection.Items[2].SumValue <> qryItemOrcamentoTempnValorOrcamentoConta.Value) then
  begin

      qryItemOrcamentoMes.Last;
      qryItemOrcamentoMes.Edit;
      qryItemOrcamentoMesnValOrcamento.Value := (qryItemOrcamentoMesnValOrcamento.Value + (qryItemOrcamentoTempnValorOrcamentoConta.Value - DBGridEh1.SumList.SumCollection.Items[2].SumValue)) ;
      qryItemOrcamentoMes.Post;

      qryItemOrcamentoMes.First ;

  end ;

end;

end.
