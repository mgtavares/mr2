inherited frmHistoricoVersoes: TfrmHistoricoVersoes
  Left = 315
  Top = 129
  Width = 852
  Height = 515
  Caption = 'Hist'#243'rico de Vers'#245'es'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 844
    Height = 459
  end
  inherited ToolBar1: TToolBar
    Width = 844
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
    object ToolButton4: TToolButton
      Left = 166
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 1
      Style = tbsSeparator
    end
    object cmbHistoricoVersoes: TComboBox
      Left = 174
      Top = 0
      Width = 241
      Height = 22
      Style = csDropDownList
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ItemHeight = 14
      ParentFont = False
      TabOrder = 0
      OnChange = cmbHistoricoVersoesChange
    end
  end
  object WebBrowser1: TWebBrowser [2]
    Left = 0
    Top = 29
    Width = 844
    Height = 459
    Align = alClient
    TabOrder = 1
    ControlData = {
      4C0000003B570000702F00000000000000000000000000000000000000000000
      000000004C000000000000000000000001000000E0D057007335CF11AE690800
      2B2E126208000000000000004C0000000114020000000000C000000000000046
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000000100000000000000000000000000000000000000}
  end
  inherited ImageList1: TImageList
    Left = 688
    Top = 88
  end
end
