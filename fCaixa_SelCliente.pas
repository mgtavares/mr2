unit fCaixa_SelCliente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxLookAndFeelPainters, ADODB,
  StdCtrls, cxButtons, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxControls, cxGridCustomView, cxGrid, jpeg,
  ExtCtrls, cxCheckBox, cxImage, cxImageComboBox, ImgList;

type
  TfrmCaixa_SelCliente = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    edtNmCliente: TEdit;
    edtCPF: TEdit;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    btSeleciona: TcxButton;
    btProxima: TcxButton;
    btFechar: TcxButton;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocRG: TStringField;
    dsTerceiro: TDataSource;
    edtRG: TEdit;
    Label3: TLabel;
    cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cCNPJCPF: TcxGridDBColumn;
    cxGrid1DBTableView1cRG: TcxGridDBColumn;
    Image1: TImage;
    btClienteSimples: TcxButton;
    qryTerceironCdStatus: TIntegerField;
    cxGrid1DBTableView1nCdStatus: TcxGridDBColumn;
    ImageList1: TImageList;
    procedure ConsultaCliente;
    function SelecionaCliente:integer;
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure btFecharClick(Sender: TObject);
    procedure btProximaClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSelecionaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtNmClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtRGKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCPFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btClienteSimplesClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_SelCliente: TfrmCaixa_SelCliente;

implementation

uses fPdv, fClienteVarejoPessoaFisica_Simples, fMenu;

{$R *.dfm}

procedure TfrmCaixa_SelCliente.ConsultaCliente;
begin

    if (trim(edtNmCliente.text) <> '') and (length(trim(edtNmCliente.Text)) <= 3) then
    begin
        frmPDV.MensagemAlerta('Informe no m�nimo 4 letras do nome do cliente.') ;
        edtNmCliente.SetFocus;
        exit ;
    end ;

    if (trim(edtCPF.Text) = '') and (trim(edtRG.Text) = '') and (trim(edtNmCliente.Text) = '') then
    begin
        frmPDV.MensagemAlerta('Nenhum crit�rio para busca preenchido. Favor preencher um dos filtros.') ;
        edtCPF.Setfocus ;
        exit ;
    end ;

    qryTerceiro.Close ;
    qryTerceiro.Parameters.ParamByName('cNmTerceiro').Value         := edtNmCliente.Text ;
    qryTerceiro.Parameters.ParamByName('cCPF').Value                := edtCPF.Text ;
    qryTerceiro.Parameters.ParamByName('cRG').Value                 := edtRG.Text ;
    qryTerceiro.Parameters.ParamByName('cNmTerceiroAnterior').Value := '' ;
    qryTerceiro.Open ;

    if not qryTerceiro.Eof then
        cxGrid1.SetFocus
    else
    begin
        frmPDV.MensagemAlerta('Nenhum cliente encontrado para o crit�rio selecionado.') ;
        edtCPF.SetFocus;
        exit ;
    end ;

    if (qryTerceiro.RecordCount >= 20) then
        btProxima.Enabled := True ;

end;

function TfrmCaixa_SelCliente.SelecionaCliente: integer;
begin

    qryTerceiro.Close ;
    edtCPF.Text       := '' ;
    edtRG.Text        := '' ;
    edtNmCliente.Text := '' ;

    Self.ShowModal;

    if not qryTerceiro.eof then
        Result := qryTerceironCdTerceiro.Value
    else Result := 0 ;

end;

procedure TfrmCaixa_SelCliente.btFecharClick(Sender: TObject);
begin
    qryTerceiro.Close ;
    Close;
end;

procedure TfrmCaixa_SelCliente.btProximaClick(Sender: TObject);
begin

  if (edtCPF.text = '') and (edtRG.text = '') and (edtNmCliente.text = '') then
  begin
      frmPDV.MensagemAlerta('Nenhum filtro informado.');
      edtCPF.SetFocus;
      exit ;
  end ;

  if not (btProxima.Enabled) then
      exit ;

  qryTerceiro.last ;

  qryTerceiro.Parameters.ParamByName('cNmTerceiro').Value         := edtNmCliente.Text ;
  qryTerceiro.Parameters.ParamByName('cCPF').Value                := edtCPF.Text ;
  qryTerceiro.Parameters.ParamByName('cRG').Value                 := edtRG.Text ;
  qryTerceiro.Parameters.ParamByName('cNmTerceiroAnterior').Value := qryTerceirocNmTerceiro.Value ;

  btProxima.Enabled := false ;

  qryTerceiro.Close ;
  qryTerceiro.Open ;

  if not qryTerceiro.eof then
      btProxima.Enabled := True ;

  if (qryTerceiro.RecordCount < 20) then
      btProxima.Enabled := false ;

  cxGrid1.SetFocus ;

end;

procedure TfrmCaixa_SelCliente.cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  case key of

      vk_return : btSeleciona.Click;
      vk_ESCAPE : Close() ;
      vk_F8     : btProxima.Click;
      VK_F9     : btClienteSimples.Click;
      VK_F12    : btFechar.Click;

  end

end;

procedure TfrmCaixa_SelCliente.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  if (not qryTerceiro.Eof) then
  begin
      if (qryTerceironCdStatus.Value = 2) then
      begin
          frmMenu.MensagemAlerta('Cliente inativo n�o pode ser utilizado, efetue sua ativa��o para que seja liberado o uso do seu cadastro.');
          Exit;
      end;

      Close;
  end;
end;

procedure TfrmCaixa_SelCliente.btSelecionaClick(Sender: TObject);
begin
    if (not qryTerceiro.Eof) then
    begin
        if (qryTerceironCdStatus.Value = 2) then
        begin
            frmMenu.MensagemAlerta('Cliente inativo n�o pode ser utilizado, efetue sua ativa��o para que seja liberado o uso do seu cadastro.');
            Exit;
        end;

        Close;
    end;
end;

procedure TfrmCaixa_SelCliente.FormShow(Sender: TObject);
begin

    btProxima.Enabled := False ;

    edtCPF.Text       := '' ;
    edtRG.Text        := '' ;
    edtNmCliente.Text := '' ;

    edtCPF.SetFocus;

end;

procedure TfrmCaixa_SelCliente.edtNmClienteKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  case Key of
    VK_RETURN : ConsultaCliente ;
    VK_F9     : btClienteSimples.Click;
    VK_F12    : btFechar.Click ;
    vk_ESCAPE : Close() ;
  end ;

end;

procedure TfrmCaixa_SelCliente.edtRGKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin

  case Key of
    VK_RETURN : edtNmCliente.SetFocus ;
    VK_F9     : btClienteSimples.Click;
    VK_F12    : btFechar.Click ;
    vk_ESCAPE : Close() ;
  end ;

end;

procedure TfrmCaixa_SelCliente.edtCPFKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  case Key of
    VK_RETURN : edtRG.SetFocus ;
    VK_F9     : btClienteSimples.Click;
    VK_F12    : btFechar.Click ;
    vk_ESCAPE : Close() ;
  end ;

end;

procedure TfrmCaixa_SelCliente.btClienteSimplesClick(Sender: TObject);
var
  objClienteSimples : TfrmClienteVarejoPessoaFisica_Simples;
begin
  { -- valida permiss�o de usu�rio -- }
  frmMenu.qryUsuarioAplicacao.Close;
  frmMenu.qryUsuarioAplicacao.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  frmMenu.qryUsuarioAplicacao.Parameters.ParamByName('cNmObjeto').Value  := 'FRMCLIENTEVAREJOPESSOAFISICA_SIMPLES';
  frmMenu.qryUsuarioAplicacao.Open;

  if (frmMenu.qryUsuarioAplicacao.IsEmpty) then
  begin
      frmMenu.MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Abort;
  end;

  try
      objClienteSimples := TfrmClienteVarejoPessoaFisica_Simples.Create(nil);
      objClienteSimples.ShowModal;
  finally
      FreeAndNil(objClienteSimples);
  end;
end;

end.
