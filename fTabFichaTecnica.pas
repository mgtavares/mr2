unit fTabFichaTecnica;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, ER2Lookup;

type
  TfrmTabFichaTecnica = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    Label2: TLabel;
    qryMasternCdTabFichaTecnica: TIntegerField;
    qryMastercNmTabFichaTecnica: TStringField;
    qryMasternCdIdExternoWeb: TIntegerField;
    qryMastercTipoDado: TStringField;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTabFichaTecnica: TfrmTabFichaTecnica;

implementation

{$R *.dfm}
procedure TfrmTabFichaTecnica.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TABFICHATECNICA' ;
  nCdTabelaSistema  := 517 ;
  nCdConsultaPadrao := 768 ;

end;

procedure TfrmTabFichaTecnica.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

initialization
    RegisterClass(TfrmTabFichaTecnica);
end.
