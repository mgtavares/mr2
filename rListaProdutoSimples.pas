unit rListaProdutoSimples;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, StdCtrls, Mask, ImgList, ComCtrls, ToolWin,
  ExtCtrls, DB, DBCtrls, ADODB;

type
  TrptListaProdutoSimples = class(TfrmRelatorio_Padrao)
    Label1: TLabel;
    MaskEdit1: TMaskEdit;
    Label3: TLabel;
    MaskEdit6: TMaskEdit;
    Label6: TLabel;
    MaskEdit7: TMaskEdit;
    qryMarca: TADOQuery;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    qryGrupoProdutocFlgExpPDV: TIntegerField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    RadioGroup1: TRadioGroup;
    rgTipoProd: TRadioGroup;
    MaskEdit2: TMaskEdit;
    Label2: TLabel;
    qryMarcacNmMarca: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    rgProdWeb: TRadioGroup;
    rgModeloImp: TRadioGroup;
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit2Exit(Sender: TObject);
    procedure MaskEdit1Exit(Sender: TObject);
    procedure MaskEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure rgProdWebClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptListaProdutoSimples: TrptListaProdutoSimples;

implementation

uses rListaProdutoSimples_view, fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TrptListaProdutoSimples.ToolButton1Click(Sender: TObject);
var
    cFiltro : String ;
    objRel  : TrptListaProdutoSimples_view;
begin
  inherited;



  objRel := TrptListaProdutoSimples_view.Create(nil);

  try
      try
          objRel.qryProduto.Close ;
          objRel.qryProduto.Parameters.ParamByName('nCdGrupoProduto').Value   := frmMenu.ConvInteiro(MaskEdit1.Text);
          objRel.qryProduto.Parameters.ParamByName('nCdStatus').Value         := RadioGroup1.ItemIndex + 1;
          objRel.qryProduto.Parameters.ParamByName('nCdTabTipoProduto').Value := rgTipoProd.ItemIndex;
          objRel.qryProduto.Parameters.ParamByName('cFlgProdWeb').Value       := rgProdWeb.ItemIndex;
          objRel.qryProduto.Parameters.ParamByName('cDataInicial').Value      := frmMenu.ConvData(MaskEdit6.Text);
          objRel.qryProduto.Parameters.ParamByName('cDataFinal').Value        := frmMenu.ConvData(MaskEdit7.Text);
          objRel.qryProduto.Parameters.ParamByName('nCdMarca').Value          := frmMenu.ConvInteiro(MaskEdit2.Text);
          objRel.qryProduto.Open ;

          cFiltro := '' ;

          If (DbEdit1.Text <> '') then
              cFiltro := cFiltro + 'Grupo de Produtos: ' + Trim(MaskEdit1.Text) + '-' + Trim(DBEdit1.Text) ;

          If (DbEdit2.Text <> '') then
              cFiltro := cFiltro + '/ Marca: ' + MaskEdit2.Text + '-' + DBEdit2.Text ;

          If (Trim(MaskEdit6.Text) <> '/  /') or (Trim(MaskEdit7.Text) <> '/  /') then
              cFiltro := cFiltro + '/ Per�odo de Cadastro: ' + MaskEdit6.Text + ' a ' + MaskEdit7.Text ;

          cFiltro := cFiltro + '/ Status: ' ;

          if (RadioGroup1.ItemIndex = 0) then
          begin
              cFiltro := cFiltro + 'ATIVO' ;
          end ;

          if (RadioGroup1.ItemIndex = 1) then
          begin
              cFiltro := cFiltro + 'INATIVO' ;
          end ;

          if (RadioGroup1.ItemIndex = 2) then
          begin
              cFiltro := cFiltro + 'TODOS' ;
          end ;

          cFiltro := cFiltro + '/ Produto Web: ' ;

          if (rgProdWeb.ItemIndex = 0) then
          begin
              cFiltro := cFiltro + 'N�O' ;
          end ;

          if (rgProdWeb.ItemIndex = 1) then
          begin
              cFiltro := cFiltro + 'SIM' ;
          end ;
          {-- exibe relat�rio --}
          case (rgModeloImp.ItemIndex) of
              0 : begin
                      objRel.lblFiltro1.Caption := cFiltro;
                      objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
                      objRel.QuickRep1.PreviewModal;
                  end;
              1 : begin
                      frmMenu.ExportExcelDinamic('Listagem de Produtos - Resumida',cFiltro,'',objRel.qryProduto);
                  end;
          end;


      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel);
  end;
end;

procedure TrptListaProdutoSimples.MaskEdit2Exit(Sender: TObject);
begin
  inherited;

  qryMarca.Close ;
  PosicionaQuery(qryMarca, MaskEdit2.Text) ;
  
end;

procedure TrptListaProdutoSimples.MaskEdit1Exit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close ;
  PosicionaQuery(qryGrupoProduto, MaskEdit1.Text) ;
  
end;

procedure TrptListaProdutoSimples.MaskEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin
            MaskEdit1.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoProduto, MaskEdit1.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptListaProdutoSimples.MaskEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            MaskEdit2.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, MaskEdit2.Text) ;
        end ;

    end ;

  end ;

end;

procedure TrptListaProdutoSimples.rgProdWebClick(Sender: TObject);
begin
  inherited;

  rgTipoProd.Enabled := (rgProdWeb.ItemIndex = 0);
end;

initialization
    RegisterClass(TrptListaProdutoSimples) ;
    
end.
