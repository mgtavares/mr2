inherited frmPedidoVendaSimples_SelecaoAgrupada: TfrmPedidoVendaSimples_SelecaoAgrupada
  Top = 0
  Width = 1152
  Height = 784
  VertScrollBar.Position = 85
  Caption = 'Sele'#231#227'o Agrupada'
  OldCreateOrder = True
  Position = poDesktopCenter
  WindowState = wsMaximized
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Left = 361
    Top = -56
    Width = 791
    Height = 785
  end
  inherited ToolBar1: TToolBar
    Top = -85
    Width = 1152
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = -56
    Width = 361
    Height = 785
    Align = alLeft
    TabOrder = 1
    object GroupBox2: TGroupBox
      Left = 2
      Top = 15
      Width = 357
      Height = 162
      Align = alTop
      Caption = 'Marca'
      TabOrder = 0
      object cxGrid1: TcxGrid
        Left = 2
        Top = 15
        Width = 353
        Height = 145
        Align = alClient
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnKeyDown = cxGrid1DBTableView1KeyDown
          DataController.DataSource = dsMarca
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          OnCellClick = cxGrid1DBTableView1CellClick
          OptionsBehavior.IncSearch = True
          OptionsBehavior.IncSearchItem = cxGrid1DBTableView1cNmMarca
          OptionsCustomize.ColumnGrouping = False
          OptionsCustomize.ColumnMoving = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1nCdMarca: TcxGridDBColumn
            DataBinding.FieldName = 'nCdMarca'
            Visible = False
          end
          object cxGrid1DBTableView1cNmMarca: TcxGridDBColumn
            Caption = 'Marca'
            DataBinding.FieldName = 'cNmMarca'
            Width = 322
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object GroupBox3: TGroupBox
      Left = 2
      Top = 177
      Width = 357
      Height = 606
      Align = alClient
      Caption = 'Produto Estruturado'
      TabOrder = 1
      object cxGrid2: TcxGrid
        Left = 2
        Top = 15
        Width = 353
        Height = 589
        Align = alClient
        TabOrder = 0
        object cxGridDBTableView1: TcxGridDBTableView
          OnDblClick = cxGridDBTableView1DblClick
          OnKeyDown = cxGridDBTableView1KeyDown
          DataController.DataSource = dsProduto
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          OptionsBehavior.IncSearch = True
          OptionsBehavior.IncSearchItem = cxGridDBTableView1cNmProduto
          OptionsCustomize.ColumnGrouping = False
          OptionsCustomize.ColumnMoving = False
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          object cxGridDBTableView1nCdProduto: TcxGridDBColumn
            DataBinding.FieldName = 'nCdProduto'
            Visible = False
          end
          object cxGridDBTableView1cNmProduto: TcxGridDBColumn
            Caption = 'Produto'
            DataBinding.FieldName = 'cNmProduto'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  object GroupBox4: TGroupBox [3]
    Left = 361
    Top = -56
    Width = 791
    Height = 785
    Caption = 'Produto Final'
    TabOrder = 2
    object GroupBox5: TGroupBox
      Left = 2
      Top = 15
      Width = 787
      Height = 505
      Align = alClient
      Caption = 'Refer'#234'ncias do Produto'
      TabOrder = 0
      object DBGridEh3: TDBGridEh
        Left = 2
        Top = 15
        Width = 783
        Height = 488
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsTemp
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh3KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProduto'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 48
          end
          item
            EditButtons = <>
            FieldName = 'cReferencia'
            Footers = <>
            ReadOnly = True
          end
          item
            EditButtons = <>
            FieldName = 'cNmProduto'
            Footers = <>
            ReadOnly = True
            Width = 190
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePed'
            Footers = <>
            Width = 52
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeEstoque'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeEmpenhado'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nQtdeDisponivel'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nValUnitario'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 60
          end
          item
            EditButtons = <>
            FieldName = 'nValDescontoUnit'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            ReadOnly = True
            Width = 60
          end
          item
            EditButtons = <>
            FieldName = 'nValTotalItem'
            Footers = <>
            ReadOnly = True
            Width = 75
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object GroupBox6: TGroupBox
      Left = 2
      Top = 520
      Width = 787
      Height = 263
      Align = alBottom
      Caption = 'Resumo dos Itens'
      TabOrder = 1
      object DBGridEh1: TDBGridEh
        Left = 2
        Top = 15
        Width = 783
        Height = 246
        Align = alClient
        AllowedOperations = [alopUpdateEh]
        DataGrouping.GroupLevels = <>
        DataSource = dsResumo
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        FooterRowCount = 1
        IndicatorOptions = [gioShowRowIndicatorEh]
        SumList.Active = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdProdutoPai'
            Footer.Font.Charset = DEFAULT_CHARSET
            Footer.Font.Color = clWhite
            Footer.Font.Height = -11
            Footer.Font.Name = 'Tahoma'
            Footer.Font.Style = []
            Footers = <>
            Width = 47
          end
          item
            EditButtons = <>
            FieldName = 'cNmProduto'
            Footers = <>
            Width = 391
          end
          item
            EditButtons = <>
            FieldName = 'nQtdePedTotal'
            Footers = <>
            Width = 150
          end
          item
            EditButtons = <>
            FieldName = 'nValorTotal'
            Footers = <>
            Width = 150
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    AfterScroll = qryMarcaAfterScroll
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdMarca'
      '      ,cNmMarca'
      '  FROM Marca'
      ' WHERE nCdStatus = 1'
      ' ORDER BY cNmMarca')
    Left = 409
    Top = 181
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 449
    Top = 181
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTipoPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdMarca'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTipoPedido int'
      '       ,@nCdMarca      int'
      ''
      'Set @nCdTipoPedido = :nCdTipoPedido'
      'Set @nCdMarca      = :nCdMarca'
      ''
      'SELECT nCdProduto'
      '      ,cNmProduto'
      '  FROM Produto'
      ' WHERE nCdMarca      = @nCdMarca'
      '   AND nCdStatus     = 1'
      '   AND cFlgProdVenda = 1'
      '   AND nCdProdutoPai IS NULL'
      '   AND EXISTS(SELECT 1'
      '                FROM GrupoProdutoTipoPedido GPTP'
      
        '               WHERE GPTP.nCdGrupoProduto = Produto.nCdGrupo_Pro' +
        'duto'
      '                 AND GPTP.nCdTipoPedido   = @nCdTipoPedido)'
      ' ORDER BY 2')
    Left = 417
    Top = 237
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object dsProduto: TDataSource
    DataSet = qryProduto
    Left = 449
    Top = 237
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF OBJECT_ID('#39'tempdb..#TempItemPedidoAgrupado'#39') IS NULL'
      'BEGIN'
      ''
      #9'CREATE TABLE #TempItemPedidoAgrupado (nCdProdutoPai    int'
      '                                       ,nCdProduto       int'
      #9#9#9#9#9#9#9#9#9#9'                   ,cReferencia      varchar(20)'
      #9#9#9#9#9#9#9#9#9#9'                   ,cNmProduto       varchar(150)'
      
        #9#9#9#9#9#9#9#9#9#9'                   ,nQtdePed         decimal(12,2) def' +
        'ault 0 not null'
      
        #9#9#9#9#9#9#9#9#9#9'                   ,nQtdeEstoque     decimal(12,2) def' +
        'ault 0 not null'
      
        #9#9#9#9#9#9#9#9#9#9'                   ,nQtdeEmpenhado   decimal(12,2) def' +
        'ault 0 not null'
      
        #9#9#9#9#9#9#9#9#9#9'                   ,nQtdeDisponivel  decimal(12,2) def' +
        'ault 0 not null'
      
        '                                       ,nValUnitario     decimal' +
        '(12,2) default 0 not null'
      
        '                                       ,nValDescontoUnit decimal' +
        '(12,2) default 0 not null'
      
        '                                       ,nValTotalItem    decimal' +
        '(12,2) default 0 not null)'
      ''
      'END'
      ''
      'TRUNCATE TABLE #TempItemPedidoAgrupado')
    Left = 409
    Top = 285
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdProdutoPai'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdPedido'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'DECLARE @nCdProdutoPai  int'
      '       ,@nCdPedido      int'
      '       ,@nValor         decimal(12,2)'
      '       ,@nValDesconto   decimal(12,2)'
      '       ,@nValAcrescimo  decimal(12,2)'
      '       ,@nCdProduto     int'
      ''
      'Set @nCdProdutoPai = :nCdProdutoPai'
      'Set @nCdPedido     = :nCdPedido'
      ''
      'IF OBJECT_ID('#39'tempdb..#TempItemPedidoAgrupado'#39') IS NULL'
      'BEGIN'
      ''
      #9'CREATE TABLE #TempItemPedidoAgrupado (nCdProdutoPai    int'
      #9#9#9#9#9#9#9#9#9#9' ,nCdProduto       int'
      #9#9#9#9#9#9#9#9#9#9' ,cReferencia      varchar(20)'
      #9#9#9#9#9#9#9#9#9#9' ,cNmProduto       varchar(150)'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdePed         decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdeEstoque     decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdeEmpenhado   decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdeDisponivel  decimal(12,2) default 0 not null'
      
        '                                         ,nValUnitario     decim' +
        'al(12,2) default 0 not null'
      
        '                                         ,nValDescontoUnit decim' +
        'al(12,2) default 0 not null'
      
        '                                         ,nValTotalItem    decim' +
        'al(12,2) default 0 not null)'
      ''
      'END'
      ''
      'INSERT INTO #TempItemPedidoAgrupado (nCdProdutoPai'
      #9#9#9#9#9#9#9#9#9'                  ,nCdProduto'
      #9#9#9#9#9#9#9#9#9'                  ,cReferencia'
      '                                    ,cNmProduto)'
      
        '                              SELECT CASE WHEN nCdProdutoPai IS ' +
        'NULL THEN nCdProduto'
      '                                          ELSE nCdProdutoPai'
      '                                     END'
      #9#9#9#9#9#9#9#9#9'                  ,nCdProduto'
      #9#9#9#9#9#9#9#9#9'                  ,cReferencia'
      #9#9#9#9#9#9#9#9#9'                  ,cNmProduto'
      '                                FROM Produto'
      
        '                               WHERE nCdProdutoPai = @nCdProduto' +
        'Pai'
      '                                 AND nCdStatus     = 1'
      '                                 AND NOT EXISTS(SELECT 1'
      
        '                                                  FROM #TempItem' +
        'PedidoAgrupado Temp'
      
        '                                                 WHERE Temp.nCdP' +
        'rodutoPai = @nCdProdutoPai'
      
        '                                                   AND Temp.nCdP' +
        'roduto    = Produto.nCdProduto)'
      ''
      'INSERT INTO #TempItemPedidoAgrupado (nCdProdutoPai'
      #9#9#9#9#9#9#9#9#9'                  ,nCdProduto'
      #9#9#9#9#9#9#9#9#9'                  ,cReferencia'
      '                                    ,cNmProduto)'
      
        '                              SELECT CASE WHEN nCdProdutoPai IS ' +
        'NULL THEN nCdProduto'
      '                                          ELSE nCdProdutoPai'
      '                                     END'
      #9#9#9#9#9#9#9#9#9'                  ,nCdProduto'
      #9#9#9#9#9#9#9#9#9'                  ,cReferencia'
      #9#9#9#9#9#9#9#9#9'                  ,cNmProduto'
      '                                FROM Produto'
      '                               WHERE nCdProduto = @nCdProdutoPai'
      '                                 AND nCdStatus  = 1'
      '                                 AND NOT EXISTS(SELECT 1'
      
        '                                                  FROM Produto F' +
        'ilho'
      
        '                                                 WHERE Filho.nCd' +
        'ProdutoPai = Produto.nCdProduto)'
      '                                 AND NOT EXISTS(SELECT 1'
      
        '                                                  FROM #TempItem' +
        'PedidoAgrupado Temp'
      
        '                                                 WHERE Temp.nCdP' +
        'rodutoPai = @nCdProdutoPai'
      
        '                                                   AND Temp.nCdP' +
        'roduto    = Produto.nCdProduto)'
      ''
      '/* apura a posi'#231#227'o de estoques */'
      'UPDATE #TempItemPedidoAgrupado'
      '   Set nQtdeEstoque   = IsNull((SELECT Sum(nQtdeDisponivel)'
      '                                  FROM PosicaoEstoque'
      
        '                                 WHERE PosicaoEstoque.nCdProduto' +
        '   = #TempItemPedidoAgrupado.nCdProduto),0)'
      
        '      ,nQtdeEmpenhado = dbo.fn_CalculaSaldoEmpenhoProd(#TempItem' +
        'PedidoAgrupado.nCdProduto,0,0)/*IsNull((SELECT Sum(nQtdeEstoqueE' +
        'mpenhado)'
      '                                  FROM EstoqueEmpenhado'
      
        '                                 WHERE EstoqueEmpenhado.nCdProdu' +
        'to = #TempItemPedidoAgrupado.nCdProduto),0)*/'
      ' WHERE nCdProdutoPai  = @nCdProdutoPai'
      ''
      ''
      'UPDATE #TempItemPedidoAgrupado'
      '   Set nQtdeDisponivel = nQtdeEstoque - nQtdeEmpenhado'
      ' WHERE nCdProdutoPai   = @nCdProdutoPai'
      ''
      '/* apura o pre'#231'o de venda */'
      'DECLARE curItens CURSOR'
      '    FOR SELECT nCdProduto'
      '          FROM #TempItemPedidoAgrupado'
      '         WHERE nCdProdutoPai = @nCdProdutoPai'
      ''
      'OPEN curItens'
      ''
      'FETCH NEXT'
      ' FROM curItens'
      ' INTO @nCdProduto'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      '    EXEC SP_SUGERE_PRECO_TABELA  @nCdPedido'
      '                                ,@nCdProduto'
      '                                ,0'
      '                                ,@nValor         OUTPUT'
      '                                ,@nValDesconto   OUTPUT'
      '                                ,@nValAcrescimo  OUTPUT'
      ''
      '    IF (@nValor > 0)'
      '    BEGIN'
      ''
      '        UPDATE #TempItemPedidoAgrupado'
      '           Set nValUnitario     = @nValor'
      '              ,nValDescontoUnit = @nValDesconto'
      '         WHERE nCdProduto       = @nCdProduto'
      '           AND nCdProdutoPai    = @nCdProdutoPai'
      ''
      '    END'
      #9'FETCH NEXT'
      #9' FROM curItens'
      #9' INTO @nCdProduto'
      ''
      'END'
      ''
      'CLOSE curItens'
      'DEALLOCATE curItens')
    Left = 409
    Top = 341
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryTempBeforePost
    AfterPost = qryTempAfterPost
    Parameters = <
      item
        Name = 'nCdProdutoPai'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'IF OBJECT_ID('#39'tempdb..#TempItemPedidoAgrupado'#39') IS NULL'
      'BEGIN'
      ''
      #9'CREATE TABLE #TempItemPedidoAgrupado (nCdProdutoPai    int'
      #9#9#9#9#9#9#9#9#9#9' ,nCdProduto       int'
      #9#9#9#9#9#9#9#9#9#9' ,cReferencia      varchar(20)'
      #9#9#9#9#9#9#9#9#9#9' ,cNmProduto       varchar(150)'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdePed         decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdeEstoque     decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdeEmpenhado   decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdeDisponivel  decimal(12,2) default 0 not null'
      
        '                                         ,nValUnitario     decim' +
        'al(12,2) default 0 not null'
      
        '                                         ,nValDescontoUnit decim' +
        'al(12,2) default 0 not null'
      
        '                                         ,nValTotalItem    decim' +
        'al(12,2) default 0 not null)'
      ''
      'END'
      ''
      'SELECT * '
      '  FROM #TempItemPedidoAgrupado'
      ' WHERE nCdProdutoPai = :nCdProdutoPai'
      ' ORDER BY cReferencia, cNmProduto')
    Left = 401
    Top = 389
    object qryTempnCdProdutoPai: TIntegerField
      FieldName = 'nCdProdutoPai'
    end
    object qryTempnCdProduto: TIntegerField
      DisplayLabel = 'Produto|C'#243'd.'
      FieldName = 'nCdProduto'
    end
    object qryTempcReferencia: TStringField
      DisplayLabel = 'Produto|Refer'#234'ncia'
      FieldName = 'cReferencia'
    end
    object qryTempcNmProduto: TStringField
      DisplayLabel = 'Produto|Descri'#231#227'o'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryTempnQtdePed: TBCDField
      DisplayLabel = 'Produto|Qtd.'
      FieldName = 'nQtdePed'
      DisplayFormat = ',0'
      Precision = 12
      Size = 2
    end
    object qryTempnQtdeEstoque: TBCDField
      DisplayLabel = 'Posi'#231#227'o Estoque|F'#237'sico'
      FieldName = 'nQtdeEstoque'
      DisplayFormat = ',0'
      Precision = 12
      Size = 2
    end
    object qryTempnQtdeEmpenhado: TBCDField
      DisplayLabel = 'Posi'#231#227'o Estoque|Empenho'
      FieldName = 'nQtdeEmpenhado'
      DisplayFormat = ',0'
      Precision = 12
      Size = 2
    end
    object qryTempnQtdeDisponivel: TBCDField
      DisplayLabel = 'Posi'#231#227'o Estoque|Dispon'#237'vel'
      FieldName = 'nQtdeDisponivel'
      DisplayFormat = ',0'
      Precision = 12
      Size = 2
    end
    object qryTempnValUnitario: TBCDField
      DisplayLabel = 'Valores|Unit'#225'rio'
      FieldName = 'nValUnitario'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempnValDescontoUnit: TBCDField
      DisplayLabel = 'Valores|Desc. Unit.'
      FieldName = 'nValDescontoUnit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempnValTotalItem: TBCDField
      DisplayLabel = 'Valores|Total Item'
      FieldName = 'nValTotalItem'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsTemp: TDataSource
    DataSet = qryTemp
    Left = 441
    Top = 389
  end
  object qryResumo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'IF OBJECT_ID('#39'tempdb..#TempItemPedidoAgrupado'#39') IS NULL'
      'BEGIN'
      ''
      #9'CREATE TABLE #TempItemPedidoAgrupado (nCdProdutoPai    int'
      #9#9#9#9#9#9#9#9#9#9' ,nCdProduto       int'
      #9#9#9#9#9#9#9#9#9#9' ,cReferencia      varchar(20)'
      #9#9#9#9#9#9#9#9#9#9' ,cNmProduto       varchar(150)'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdePed         decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdeEstoque     decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdeEmpenhado   decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdeDisponivel  decimal(12,2) default 0 not null'
      
        '                                         ,nValUnitario     decim' +
        'al(12,2) default 0 not null'
      
        '                                         ,nValDescontoUnit decim' +
        'al(12,2) default 0 not null'
      
        '                                         ,nValTotalItem    decim' +
        'al(12,2) default 0 not null)'
      ''
      'END'
      ''
      'SELECT Temp.nCdProdutoPai'
      '      ,Produto.cNmProduto'
      '      ,Sum(Temp.nQtdePed)      as nQtdePedTotal'
      '      ,Sum(Temp.nValTotalItem) as nValorTotal'
      '  FROM #TempItemPedidoAgrupado Temp'
      
        '       INNER JOIN Produto ON Produto.nCdProduto = Temp.nCdProdut' +
        'oPai'
      ' GROUP BY Temp.nCdProdutoPai'
      '         ,Produto.cNmProduto'
      'HAVING Sum(Temp.nValTotalItem) > 0'
      ' ORDER BY 2')
    Left = 531
    Top = 420
    object qryResumonCdProdutoPai: TIntegerField
      DisplayLabel = 'C'#243'd.'
      FieldName = 'nCdProdutoPai'
    end
    object qryResumocNmProduto: TStringField
      DisplayLabel = 'Descri'#231#227'o Produto'
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryResumonQtdePedTotal: TBCDField
      DisplayLabel = 'Qt. Total'
      FieldName = 'nQtdePedTotal'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryResumonValorTotal: TBCDField
      DisplayLabel = 'Valor Total'
      FieldName = 'nValorTotal'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
  end
  object dsResumo: TDataSource
    DataSet = qryResumo
    Left = 561
    Top = 421
  end
  object qryTempAnalitica: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'IF OBJECT_ID('#39'tempdb..#TempItemPedidoAgrupado'#39') IS NULL'
      'BEGIN'
      ''
      #9'CREATE TABLE #TempItemPedidoAgrupado (nCdProdutoPai    int'
      #9#9#9#9#9#9#9#9#9#9' ,nCdProduto       int'
      #9#9#9#9#9#9#9#9#9#9' ,cReferencia      varchar(20)'
      #9#9#9#9#9#9#9#9#9#9' ,cNmProduto       varchar(150)'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdePed         decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdeEstoque     decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdeEmpenhado   decimal(12,2) default 0 not null'
      #9#9#9#9#9#9#9#9#9#9' ,nQtdeDisponivel  decimal(12,2) default 0 not null'
      
        '                                         ,nValUnitario     decim' +
        'al(12,2) default 0 not null'
      
        '                                         ,nValDescontoUnit decim' +
        'al(12,2) default 0 not null'
      
        '                                         ,nValTotalItem    decim' +
        'al(12,2) default 0 not null)'
      ''
      'END'
      ''
      'SELECT Temp.nCdProdutoPai'
      '      ,Temp.nCdProduto'
      '      ,Filho.cNmProduto'
      '      ,Temp.nQtdePed'
      '      ,Temp.nValUnitario'
      '      ,Temp.nValDescontoUnit'
      '      ,Temp.nValTotalItem'
      '  FROM #TempItemPedidoAgrupado Temp'
      
        '       INNER JOIN Produto       ON Produto.nCdProduto = Temp.nCd' +
        'ProdutoPai'
      
        '       INNER JOIN Produto Filho ON Filho.nCdProduto   = Temp.nCd' +
        'Produto'
      ' WHERE Temp.nQtdePed > 0'
      ' ORDER BY 2')
    Left = 443
    Top = 460
    object qryTempAnaliticanCdProdutoPai: TIntegerField
      FieldName = 'nCdProdutoPai'
    end
    object qryTempAnaliticacNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryTempAnaliticanQtdePed: TBCDField
      FieldName = 'nQtdePed'
      Precision = 12
      Size = 2
    end
    object qryTempAnaliticanValUnitario: TBCDField
      FieldName = 'nValUnitario'
      Precision = 12
      Size = 2
    end
    object qryTempAnaliticanValDescontoUnit: TBCDField
      FieldName = 'nValDescontoUnit'
      Precision = 12
      Size = 2
    end
    object qryTempAnaliticanValTotalItem: TBCDField
      FieldName = 'nValTotalItem'
      Precision = 12
      Size = 2
    end
    object qryTempAnaliticanCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
  end
  object qryPopulaTempInicial: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdPedido'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdPedido int'
      ''
      'Set @nCdPedido = :nCdPedido'
      ''
      'IF OBJECT_ID('#39'tempdb..#TempItemPedidoAgrupado'#39') IS NULL'
      'BEGIN'
      ''
      #9'CREATE TABLE #TempItemPedidoAgrupado (nCdProdutoPai    int'
      '                                       ,nCdProduto       int'
      #9#9#9#9#9#9#9#9#9#9'                   ,cReferencia      varchar(20)'
      #9#9#9#9#9#9#9#9#9#9'                   ,cNmProduto       varchar(150)'
      
        #9#9#9#9#9#9#9#9#9#9'                   ,nQtdePed         decimal(12,2) def' +
        'ault 0 not null'
      
        #9#9#9#9#9#9#9#9#9#9'                   ,nQtdeEstoque     decimal(12,2) def' +
        'ault 0 not null'
      
        #9#9#9#9#9#9#9#9#9#9'                   ,nQtdeEmpenhado   decimal(12,2) def' +
        'ault 0 not null'
      
        #9#9#9#9#9#9#9#9#9#9'                   ,nQtdeDisponivel  decimal(12,2) def' +
        'ault 0 not null'
      
        '                                       ,nValUnitario     decimal' +
        '(12,2) default 0 not null'
      
        '                                       ,nValDescontoUnit decimal' +
        '(12,2) default 0 not null'
      
        '                                       ,nValTotalItem    decimal' +
        '(12,2) default 0 not null)'
      ''
      'END'
      ''
      'INSERT INTO #TempItemPedidoAgrupado (nCdProdutoPai'
      '                                    ,nCdProduto'
      '                                    ,cReferencia'
      '                                    ,cNmProduto'
      '                                    ,nQtdePed'
      '                                    ,nValUnitario'
      '                                    ,nValDescontoUnit'
      '                                    ,nValTotalItem)'
      
        '                              SELECT CASE WHEN Produto.nCdProdut' +
        'oPai IS NULL THEN Produto.nCdProduto'
      
        '                                          ELSE Produto.nCdProdut' +
        'oPai'
      '                                     END'
      '                                    ,Produto.nCdProduto'
      '                                    ,Produto.cReferencia'
      '                                    ,Produto.cNmProduto'
      '                                    ,ItemPedido.nQtdePed'
      '                                    ,ItemPedido.nValUnitario'
      '                                    ,ItemPedido.nValDesconto'
      '                                    ,ItemPedido.nValTotalItem'
      '                                FROM ItemPedido'
      
        '                                     INNER JOIN Produto ON Produ' +
        'to.nCdProduto = ItemPedido.nCdProduto'
      
        '                               WHERE ItemPedido.nCdPedido      =' +
        ' @nCdPedido'
      
        '                                 AND ItemPedido.nCdTipoItemPed =' +
        ' 2')
    Left = 449
    Top = 285
  end
end
