unit rDoctoFiscalEmitido_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, QRPrev, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptDoctoFiscalEmitido_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    SummaryBand1: TQRBand;
    qryResultado: TADOQuery;
    DetailBand1: TQRBand;
    QRDBText4: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    cmdPreparaTemp: TADOCommand;
    SPREL_DOCTO_FISCAL_EMITIDO: TADOStoredProc;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel19: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText20: TQRDBText;
    QRBand2: TQRBand;
    QRExpr10: TQRExpr;
    QRExpr11: TQRExpr;
    QRExpr12: TQRExpr;
    QRExpr14: TQRExpr;
    QRExpr15: TQRExpr;
    QRExpr16: TQRExpr;
    QRExpr17: TQRExpr;
    QRExpr18: TQRExpr;
    QRShape9: TQRShape;
    QRExpr20: TQRExpr;
    QRExpr21: TQRExpr;
    QRExpr22: TQRExpr;
    QRLabel43: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel49: TQRLabel;
    QRLabel50: TQRLabel;
    QRLabel51: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel7: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRExpr9: TQRExpr;
    QRExpr13: TQRExpr;
    QRExpr19: TQRExpr;
    QRDBText9: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    qryResultadonCdDoctoFiscal: TIntegerField;
    qryResultadocEmpresa: TStringField;
    qryResultadocLoja: TStringField;
    qryResultadocNrDoctoSerie: TStringField;
    qryResultadodDtEmissao: TDateTimeField;
    qryResultadocNmTipoDoctoFiscal: TStringField;
    qryResultadocTipoOper: TStringField;
    qryResultadocCFOP: TStringField;
    qryResultadonCdTerceiro: TIntegerField;
    qryResultadocNmTerceiro: TStringField;
    qryResultadocCNPJCPF: TStringField;
    qryResultadocUF: TStringField;
    qryResultadonValProduto: TBCDField;
    qryResultadonValBaseICMS: TBCDField;
    qryResultadonValICMS: TBCDField;
    qryResultadonValBaseICMSSub: TBCDField;
    qryResultadonValICMSSub: TBCDField;
    qryResultadonValIPI: TBCDField;
    qryResultadonValIsenta: TBCDField;
    qryResultadonValOutras: TBCDField;
    qryResultadonValDespesa: TBCDField;
    qryResultadonValFrete: TBCDField;
    qryResultadonValTotal: TBCDField;
    qryResultadocSituacao: TStringField;
    QRLabel8: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    qryResultadonValDevoluc: TBCDField;
    qryResultadonValValeMP: TBCDField;
    QRLabel10: TQRLabel;
    QRDBText8: TQRDBText;
    QRShape2: TQRShape;
    QRExpr23: TQRExpr;
    QRExpr24: TQRExpr;
    QRExpr25: TQRExpr;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptDoctoFiscalEmitido_view: TrptDoctoFiscalEmitido_view;

implementation

{$R *.dfm}

uses
  fMenu;

end.
