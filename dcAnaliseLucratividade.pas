unit dcAnaliseLucratividade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, Mask, StdCtrls, DBCtrls, ImgList,
  ComCtrls, ToolWin, ExtCtrls;

type
  TdcmAnaliseLucratividade = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit6: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit4: TMaskEdit;
    MaskEdit5: TMaskEdit;
    DBEdit4: TDBEdit;
    MaskEdit7: TMaskEdit;
    DBEdit5: TDBEdit;
    MaskEdit8: TMaskEdit;
    DBEdit6: TDBEdit;
    MaskEdit9: TMaskEdit;
    DBEdit7: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    dsEmpresa: TDataSource;
    dsTerceiro: TDataSource;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    dsGrupoEconomico: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TAutoIncField;
    qryClasseProdutocNmClasseProduto: TStringField;
    dsDepartamento: TDataSource;
    dsMarca: TDataSource;
    dsLinha: TDataSource;
    dsClasseProduto: TDataSource;
    qryRamoAtividade: TADOQuery;
    qryRamoAtividadenCdRamoAtividade: TIntegerField;
    qryRamoAtividadecNmRamoAtividade: TStringField;
    MaskEdit10: TMaskEdit;
    Label10: TLabel;
    DBEdit8: TDBEdit;
    dsRamoAtividade: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure MaskEdit9Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit10Exit(Sender: TObject);
    procedure MaskEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmAnaliseLucratividade: TdcmAnaliseLucratividade;

implementation

uses fMenu, fLookup_Padrao, dcVendaProdutoCliente_view,
  dcAnaliseLucratividade_view;

{$R *.dfm}

procedure TdcmAnaliseLucratividade.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  MaskEdit3.SetFocus ;

end;

procedure TdcmAnaliseLucratividade.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TdcmAnaliseLucratividade.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TdcmAnaliseLucratividade.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TdcmAnaliseLucratividade.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TdcmAnaliseLucratividade.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, IntToStr(nPK));
        end ;

    end ;

  end ;


end;

procedure TdcmAnaliseLucratividade.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryGrupoEconomico.Close ;
  
  PosicionaQuery(qryGrupoEconomico, MaskEdit4.Text) ;

end;

procedure TdcmAnaliseLucratividade.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryDepartamento.Close ;

  If (Trim(MaskEdit5.Text) <> '') then
  begin

    PosicionaQuery(qryDepartamento, Maskedit5.Text) ;

  end ;


end;

procedure TdcmAnaliseLucratividade.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryMarca.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin

    PosicionaQuery(qryMarca, Maskedit7.Text) ;

  end ;

end;

procedure TdcmAnaliseLucratividade.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(45);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmAnaliseLucratividade.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmAnaliseLucratividade.MaskEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLinha, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmAnaliseLucratividade.MaskEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
        begin
            Maskedit9.Text := IntToStr(nPK) ;
            PosicionaQuery(qryClasseProduto, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmAnaliseLucratividade.MaskEdit8Exit(Sender: TObject);
begin
  inherited;
  qryLinha.Close ;

  If (Trim(MaskEdit8.Text) <> '') then
  begin

    PosicionaQuery(qryLinha, Maskedit8.Text) ;

  end ;

end;

procedure TdcmAnaliseLucratividade.MaskEdit9Exit(Sender: TObject);
begin
  inherited;
  qryClasseProduto.Close ;

  If (Trim(MaskEdit9.Text) <> '') then
  begin

    PosicionaQuery(qryClasseProduto, Maskedit9.Text) ;

  end ;

end;

procedure TdcmAnaliseLucratividade.ToolButton1Click(Sender: TObject);
var
   objRel : TdcmAnaliseLucratividade_view;
begin
  inherited;

  objRel := TdcmAnaliseLucratividade_view.Create(nil);

  if (trim(MaskEdit1.Text) = '/  /') or (Trim(MaskEdit2.Text) = '/  /') then
  begin
      MaskEdit2.Text := DateToStr(Now()) ;
      MaskEdit1.Text := DateToStr(Now()-180) ;
  end ;

  Try
      Try
          objRel.ADODataSet1.Close ;
          objRel.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
          objRel.ADODataSet1.Parameters.ParamByName('nCdGrupoEconomico').Value := frmMenu.ConvInteiro(MaskEdit4.Text) ;
          objRel.ADODataSet1.Parameters.ParamByName('nCdTerceiro').Value       := frmMenu.ConvInteiro(MaskEdit6.Text) ;
          objRel.ADODataSet1.Parameters.ParamByName('nCdDepartamento').Value   := frmMenu.ConvInteiro(MaskEdit5.Text) ;
          objRel.ADODataSet1.Parameters.ParamByName('nCdMarca').Value          := frmMenu.ConvInteiro(MaskEdit7.Text) ;
          objRel.ADODataSet1.Parameters.ParamByName('nCdLinha').Value          := frmMenu.ConvInteiro(MaskEdit8.Text) ;
          objRel.ADODataSet1.Parameters.ParamByName('nCdClasseProduto').Value  := frmMenu.ConvInteiro(MaskEdit9.Text) ;
          objRel.ADODataSet1.Parameters.ParamByName('dDtInicial').Value        := frmMenu.ConvData(MaskEdit1.Text) ;
          objRel.ADODataSet1.Parameters.ParamByName('dDtFinal').Value          := frmMenu.ConvData(MaskEdit2.Text) ;
          objRel.ADODataSet1.Parameters.ParamByName('nCdRamoAtividade').Value  := frmMenu.ConvInteiro(MaskEdit10.Text) ;
          objRel.ADODataSet1.Open ;

          if (objRel.ADODataSet1.eof) then
          begin
             ShowMessage('Nenhuma informa��o encontrada.') ;
             exit ;
          end ;


          frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

          if objRel.PivotCube1.Active then
          begin
              objRel.PivotCube1.Active := False;
          end;

          objRel.PivotCube1.ExtendedMode := True;
          objRel.PVMeasureToolBar1.HideButtons := False;
          objRel.PivotCube1.Active := True;

          objRel.PivotMap1.Measures[4].ColumnPercent := True;
          objRel.PivotMap1.Measures[4].Value := False;

          objRel.PivotMap1.Measures[5].Rank  := True ;
          objRel.PivotMap1.Measures[5].Value := False;

          objRel.PivotMap1.SortColumn(0,5,False) ;

          objRel.PivotMap1.Title := 'ER2Soft - An�lise de Vendas por Clientes' ;

          objRel.PivotGrid1.RefreshData;

          frmMenu.StatusBar1.Panels[6].Text := '' ;

          objRel.ShowModal ;
      except
          MensagemErro('Erro ao gerar Data Analys');
          raise;
      end;
  Finally
      FreeAndNil(objRel);
  end;    
end;

procedure TdcmAnaliseLucratividade.MaskEdit10Exit(Sender: TObject);
begin
  inherited;

  qryRamoAtividade.Close ;
  PosicionaQuery(qryRamoAtividade, Maskedit10.Text) ;
  
end;

procedure TdcmAnaliseLucratividade.MaskEdit10KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(158);

        If (nPK > 0) then
        begin
            Maskedit10.Text := IntToStr(nPK) ;
            PosicionaQuery(qryRamoAtividade, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TdcmAnaliseLucratividade) ;

end.

