unit fCobradora;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask;

type
  TfrmCobradora = class(TfrmCadastro_Padrao)
    qryMasternCdCobradora: TIntegerField;
    qryMastercNmCobradora: TStringField;
    qryMastercFlgAtivo: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    qryMastercNmFuncaoValEncargoSQL: TStringField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBCheckBox2: TDBCheckBox;
    qryMastercFlgPermiteLiqLoja: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCobradora: TfrmCobradora;

implementation

{$R *.dfm}

procedure TfrmCobradora.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'COBRADORA' ;
  nCdTabelaSistema  := 91 ;
  nCdConsultaPadrao := 199 ;
end;

procedure TfrmCobradora.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMastercFlgAtivo.Value := 1 ;
  qryMastercNmFuncaoValEncargoSQL.Value := '[DBO].[FN_CALCRETORNOCOBRADORA01]' ;
  DBEdit2.SetFocus ;
  
end;

procedure TfrmCobradora.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBEdit2.Text = '') then
  begin
      MensagemErro('Informe o nome da cobradora.') ;
      DBEdit2.SetFocus;
      abort ;
  end ;

  inherited;

end;

initialization
    RegisterClass(TfrmCobradora) ;

end.
