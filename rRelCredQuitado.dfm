inherited rptRelCredQuitado: TrptRelCredQuitado
  Left = 243
  Top = 268
  Width = 925
  Height = 246
  Caption = 'Relat'#243'rio de Credi'#225'rios Quitados'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 909
    Height = 184
  end
  object Label5: TLabel [1]
    Left = 81
    Top = 48
    Width = 20
    Height = 13
    Caption = 'Loja'
    FocusControl = DBEdit1
  end
  object lblDtQuitacao: TLabel [2]
    Left = 10
    Top = 96
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data da Liquida'#231#227'o'
  end
  object lblAte: TLabel [3]
    Left = 187
    Top = 96
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label3: TLabel [4]
    Left = 30
    Top = 72
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data da Venda'
  end
  object Label6: TLabel [5]
    Left = 187
    Top = 72
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  inherited ToolBar1: TToolBar
    Width = 909
    TabOrder = 6
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object ER2LookupMaskEdit1: TER2LookupMaskEdit [7]
    Left = 103
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    CodigoLookup = 59
    QueryLookup = qryTipoLoja
  end
  object DBEdit1: TDBEdit [8]
    Tag = 1
    Left = 171
    Top = 40
    Width = 446
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsTipoLoja
    TabOrder = 5
  end
  object MaskEdit1: TMaskEdit [9]
    Left = 103
    Top = 88
    Width = 63
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [10]
    Left = 207
    Top = 88
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object MaskEdit6: TMaskEdit [11]
    Left = 103
    Top = 64
    Width = 63
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
  end
  object MaskEdit7: TMaskEdit [12]
    Left = 207
    Top = 64
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object RadioGroup1: TRadioGroup [13]
    Left = 104
    Top = 119
    Width = 185
    Height = 65
    Caption = ' Modelo '
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Planilha')
    TabOrder = 7
  end
  inherited ImageList1: TImageList
    Left = 312
    Top = 144
  end
  object qryTipoLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '              ,nCdLoja'
      '    FROM Loja'
      '   WHERE nCdLoja = :nPK')
    Left = 344
    Top = 144
    object qryTipoLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryTipoLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
  end
  object dsTipoLoja: TDataSource
    DataSet = qryTipoLoja
    Left = 376
    Top = 144
  end
  object ER2Excel: TER2Excel
    Titulo = 'Rel. Credi'#225'rios Quitados'
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 408
    Top = 144
  end
end
