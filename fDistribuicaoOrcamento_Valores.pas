unit fDistribuicaoOrcamento_Valores;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, DBCtrls, StdCtrls, Mask, cxLookAndFeelPainters, cxButtons,
  ER2Lookup;

type
  TfrmDistribuicaoOrcamento_Valores = class(TfrmProcesso_Padrao)
    qryItemOrcamento: TADOQuery;
    qryItemOrcamentonCdItemOrcamento: TIntegerField;
    qryItemOrcamentonCdOrcamento: TIntegerField;
    qryItemOrcamentonCdPlanoConta: TIntegerField;
    qryItemOrcamentonCdPlanoContaBase: TIntegerField;
    qryItemOrcamentonPercentBase: TBCDField;
    qryItemOrcamentonCdMetodoDistribOrcamento: TIntegerField;
    qryItemOrcamentonValorOrcamentoConta: TBCDField;
    qryItemOrcamentocOBS: TStringField;
    qryItemOrcamentocOBSGeral: TMemoField;
    qryPlanoConta: TADOQuery;
    qryPlanoContacNmPlanoConta: TStringField;
    qryPlanoContaBase: TADOQuery;
    qryPlanoContaBasecNmPlanoConta: TStringField;
    qryTipoMetodoDistrib: TADOQuery;
    qryTipoMetodoDistribnCdMetodoDistribOrcamento: TIntegerField;
    qryTipoMetodoDistribcNmMetodoDistribOrcamento: TStringField;
    qryTipoMetodoDistribcFlgLinear: TIntegerField;
    qryTipoMetodoDistribcFlgPadrao: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    comboMetodoDistrib: TDBLookupComboBox;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DataSource2: TDataSource;
    DBEdit7: TDBEdit;
    DataSource3: TDataSource;
    dsTipoMetodoDistrib: TDataSource;
    cxButton1: TcxButton;
    DBEdit2: TER2LookupDBEdit;
    qryConfereContaBase: TADOQuery;
    qryConfereContaBasenCdItemOrcamento: TIntegerField;
    qryConfereContaBasenCdPlanoContaBase: TIntegerField;
    qryConfereContaBasenValorOrcamentoConta: TBCDField;
    qryPlanoContaBasenCdPlanoConta: TIntegerField;
    procedure exibeValores ( nCdItemOrcamento : integer ) ;
    procedure FormShow(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure qryItemOrcamentoBeforePost(DataSet: TDataSet);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDistribuicaoOrcamento_Valores: TfrmDistribuicaoOrcamento_Valores;

implementation

uses fMenu, fDistribuicaoOrcamento_Valores_Mensal;

{$R *.dfm}

{ TfrmDistribuicaoOrcamento_Valores }

procedure TfrmDistribuicaoOrcamento_Valores.exibeValores(
  nCdItemOrcamento: integer);
begin

    qryTipoMetodoDistrib.Close;
    qryTipoMetodoDistrib.Open;

    qryItemOrcamento.Close;
    PosicionaQuery( qryItemOrcamento , intToStr( nCdItemOrcamento ) ) ;

    qryPlanoConta.Close;
    qryPlanoContaBase.Close;

    PosicionaQuery( qryPlanoConta , qryItemOrcamentonCdPlanoConta.asString ) ;
    PosicionaQuery( qryPlanoContaBase , qryItemOrcamentonCdPlanoContaBase.asString ) ;

    if (qryItemOrcamentonCdPlanoContaBase.Value > 0) then
    begin
        qryConfereContaBase.Close;
        qryConfereContaBase.Parameters.ParamByName('nCdOrcamento').Value  := qryItemOrcamentonCdOrcamento.Value ;
        qryConfereContaBase.Parameters.ParamByName('nCdPlanoConta').Value := qryItemOrcamentonCdPlanoContaBase.Value ;
        qryConfereContaBase.Open;
    end ;

    qryItemOrcamento.Edit ;
    
    Self.ShowModal ;

end;

procedure TfrmDistribuicaoOrcamento_Valores.FormShow(Sender: TObject);
begin
  inherited;

  DBEdit4.Setfocus;

end;

procedure TfrmDistribuicaoOrcamento_Valores.DBEdit2Exit(Sender: TObject);
begin
  inherited;

  qryPlanoContaBase.Close;
  PosicionaQuery( qryPlanoContaBase, DBEdit2.Text) ;

  qryConfereContaBase.Close;

  if (DBEdit7.Text <> '') then
  begin
      qryConfereContaBase.Parameters.ParamByName('nCdOrcamento').Value  := qryItemOrcamentonCdOrcamento.Value ;
      qryConfereContaBase.Parameters.ParamByName('nCdPlanoConta').Value := qryItemOrcamentonCdPlanoContaBase.Value ;
      qryConfereContaBase.Open;

      if (qryConfereContaBase.eof) then
      begin
          MensagemAlerta('A conta base informada n�o participa deste or�amento.') ;
          DBEdit2.SetFocus;
          abort ;
      end ;

      {if (qryConfereContaBasenCdPlanoContaBase.Value > 0) then
      begin
          MensagemAlerta('A conta base informada j� � derivada de uma outra conta base, n�o sendo permitido mais um n�vel de baseamento.') ;
          DBEdit2.SetFocus;
          abort ;
      end ;}

  end ;
  
end;

procedure TfrmDistribuicaoOrcamento_Valores.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  if (qryItemOrcamentonPercentBase.Value <> 0) and (DBEdit7.Text = '') then
  begin
      MensagemAlerta('Para definir um percentual base � necess�rio informar primeiro uma conta base.') ;
      qryItemOrcamentonPercentBase.Value := 0 ;
      DBEdit2.SetFocus;
      abort ;
  end ;

  if (qryItemOrcamentonPercentBase.Value <> 0) and (not qryConfereContaBase.eof) then
      qryItemOrcamentonValorOrcamentoConta.Value := frmMenu.TBRound((qryConfereContaBasenValorOrcamentoConta.Value * (qryItemOrcamentonPercentBase.Value / 100)),2) ;

end;

procedure TfrmDistribuicaoOrcamento_Valores.qryItemOrcamentoBeforePost(
  DataSet: TDataSet);
begin

  if (DBEdit7.Text <> '') and (qryPlanoContaBasenCdPlanoConta.Value = qryItemOrcamentonCdPlanoConta.Value) then
  begin
      MensagemAlerta('A conta base n�o pode ser a pr�pria conta.') ;
      DBEdit2.SetFocus;
      abort ; 
  end ;

  if (qryItemOrcamentonPercentBase.Value < 0) or ((qryItemOrcamentonPercentBase.Value = 0) and (DBEdit7.Text <> '')) then
  begin
      MensagemAlerta('Percentual de base deve ser maior que zero.') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (qryItemOrcamentonValorOrcamentoConta.Value < 0) then
  begin
      MensagemAlerta('O Valor do or�amento n�o pode ser menor que zero.') ;
      DBEdit4.setFocus;
      abort ;
  end ;

  inherited;


end;

procedure TfrmDistribuicaoOrcamento_Valores.cxButton1Click(
  Sender: TObject);
var
  objForm : TfrmDistribuicaoOrcamento_Valores_Mensal ;
begin
  inherited;

  qryItemOrcamento.Post ;

  objForm := TfrmDistribuicaoOrcamento_Valores_Mensal.Create( Self ) ;
  objForm.exibeDistribuicao( qryItemOrcamentonCdItemOrcamento.Value );

  freeAndNil( objForm ) ;
  
  Close ;
  
end;

end.
