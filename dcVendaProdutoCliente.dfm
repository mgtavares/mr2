inherited dcmVendaProdutoCliente: TdcmVendaProdutoCliente
  Left = 0
  Top = 0
  Width = 1152
  Height = 770
  Caption = 'Data Analysis - Venda Cliente x Produto'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1136
    Height = 705
  end
  object Label1: TLabel [1]
    Left = 67
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label5: TLabel [2]
    Left = 75
    Top = 96
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
  end
  object Label3: TLabel [3]
    Left = 39
    Top = 360
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Venda'
  end
  object Label6: TLabel [4]
    Left = 190
    Top = 360
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label2: TLabel [5]
    Left = 26
    Top = 72
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Economico'
  end
  object Label4: TLabel [6]
    Left = 39
    Top = 264
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
  end
  object Label7: TLabel [7]
    Left = 79
    Top = 288
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  object Label8: TLabel [8]
    Left = 83
    Top = 312
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'Linha'
  end
  object Label9: TLabel [9]
    Left = 77
    Top = 336
    Width = 31
    Height = 13
    Alignment = taRightJustify
    Caption = 'Classe'
  end
  object Label10: TLabel [10]
    Left = 18
    Top = 120
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ramo de Atividade'
  end
  object Label11: TLabel [11]
    Left = 36
    Top = 144
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Representante'
  end
  object Label51: TLabel [12]
    Left = 37
    Top = 166
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = #193'rea de Venda'
    FocusControl = DBEdit63
  end
  object Label52: TLabel [13]
    Left = 26
    Top = 190
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Divis'#227'o de Venda'
    FocusControl = DBEdit64
  end
  object Label53: TLabel [14]
    Left = 8
    Top = 214
    Width = 100
    Height = 13
    Alignment = taRightJustify
    Caption = 'Canal de Distribui'#231#227'o'
    FocusControl = DBEdit65
  end
  object Label12: TLabel [15]
    Left = 37
    Top = 238
    Width = 71
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Terceiro'
    FocusControl = DBEdit65
  end
  inherited ToolBar1: TToolBar
    Width = 1136
    TabOrder = 15
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [17]
    Tag = 1
    Left = 184
    Top = 40
    Width = 60
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 16
  end
  object DBEdit3: TDBEdit [18]
    Tag = 1
    Left = 248
    Top = 40
    Width = 590
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 17
  end
  object DBEdit9: TDBEdit [19]
    Tag = 1
    Left = 184
    Top = 88
    Width = 120
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = DataSource4
    TabOrder = 19
  end
  object DBEdit10: TDBEdit [20]
    Tag = 1
    Left = 308
    Top = 88
    Width = 530
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource4
    TabOrder = 20
  end
  object MaskEdit1: TMaskEdit [21]
    Left = 112
    Top = 352
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 13
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [22]
    Left = 210
    Top = 352
    Width = 73
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 14
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [23]
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object MaskEdit6: TMaskEdit [24]
    Left = 112
    Top = 88
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object DBEdit1: TDBEdit [25]
    Tag = 1
    Left = 184
    Top = 64
    Width = 654
    Height = 21
    DataField = 'cNmGrupoEconomico'
    DataSource = dsGrupoEconomico
    TabOrder = 21
  end
  object MaskEdit4: TMaskEdit [26]
    Left = 112
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object MaskEdit5: TMaskEdit [27]
    Left = 112
    Top = 256
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 9
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object DBEdit4: TDBEdit [28]
    Tag = 1
    Left = 184
    Top = 256
    Width = 654
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = dsDepartamento
    TabOrder = 18
  end
  object MaskEdit7: TMaskEdit [29]
    Left = 112
    Top = 280
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 10
    Text = '      '
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object DBEdit5: TDBEdit [30]
    Tag = 1
    Left = 184
    Top = 280
    Width = 654
    Height = 21
    DataField = 'cNmMarca'
    DataSource = dsMarca
    TabOrder = 22
  end
  object MaskEdit8: TMaskEdit [31]
    Left = 112
    Top = 304
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 11
    Text = '      '
    OnExit = MaskEdit8Exit
    OnKeyDown = MaskEdit8KeyDown
  end
  object DBEdit6: TDBEdit [32]
    Tag = 1
    Left = 184
    Top = 304
    Width = 654
    Height = 21
    DataField = 'cNmLinha'
    DataSource = dsLinha
    TabOrder = 23
  end
  object MaskEdit9: TMaskEdit [33]
    Left = 112
    Top = 328
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 12
    Text = '      '
    OnExit = MaskEdit9Exit
    OnKeyDown = MaskEdit9KeyDown
  end
  object DBEdit7: TDBEdit [34]
    Tag = 1
    Left = 184
    Top = 328
    Width = 654
    Height = 21
    DataField = 'cNmClasseProduto'
    DataSource = dsClasseProduto
    TabOrder = 24
  end
  object MaskEdit10: TMaskEdit [35]
    Left = 112
    Top = 112
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = MaskEdit10Exit
    OnKeyDown = MaskEdit10KeyDown
  end
  object DBEdit8: TDBEdit [36]
    Tag = 1
    Left = 184
    Top = 112
    Width = 654
    Height = 21
    DataField = 'cNmRamoAtividade'
    DataSource = DataSource6
    TabOrder = 25
  end
  object MaskEdit11: TMaskEdit [37]
    Left = 112
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = MaskEdit11Exit
    OnKeyDown = MaskEdit11KeyDown
  end
  object DBEdit11: TDBEdit [38]
    Tag = 1
    Left = 184
    Top = 136
    Width = 654
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource7
    TabOrder = 26
  end
  object DBEdit63: TDBEdit [39]
    Tag = 1
    Left = 184
    Top = 160
    Width = 654
    Height = 21
    DataField = 'cNmAreaVenda'
    DataSource = dsAreaVenda
    TabOrder = 27
  end
  object DBEdit64: TDBEdit [40]
    Tag = 1
    Left = 184
    Top = 184
    Width = 654
    Height = 21
    DataField = 'cNmDivisaoVenda'
    DataSource = dsDivisaoVenda
    TabOrder = 28
  end
  object DBEdit65: TDBEdit [41]
    Tag = 1
    Left = 184
    Top = 208
    Width = 654
    Height = 21
    DataField = 'cNmCanalDistribuicao'
    DataSource = dsCanalDistribuicao
    TabOrder = 29
  end
  object edtAreaVenda: TER2LookupMaskEdit [42]
    Left = 112
    Top = 160
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 5
    Text = '         '
    CodigoLookup = 753
    WhereAdicional.Strings = (
      'nCdStatus = 1')
    QueryLookup = qryAreaVenda
  end
  object edtDivisaoVenda: TER2LookupMaskEdit [43]
    Left = 112
    Top = 184
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 6
    Text = '         '
    OnBeforeLookup = edtDivisaoVendaBeforeLookup
    OnBeforePosicionaQry = edtDivisaoVendaBeforePosicionaQry
    CodigoLookup = 754
    WhereAdicional.Strings = (
      'nCdStatus = 1')
    QueryLookup = qryDivisaoVenda
  end
  object edtCanalDistrib: TER2LookupMaskEdit [44]
    Left = 112
    Top = 208
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 7
    Text = '         '
    CodigoLookup = 755
    WhereAdicional.Strings = (
      'nCdStatus = 1')
    QueryLookup = qryCanalDistribuicao
  end
  object edtGrupoTerceiro: TER2LookupMaskEdit [45]
    Left = 112
    Top = 232
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 8
    Text = '         '
    CodigoLookup = 228
    QueryLookup = qryGrupoTerceiro
  end
  object DBEdit12: TDBEdit [46]
    Tag = 1
    Left = 184
    Top = 232
    Width = 654
    Height = 21
    DataField = 'cNmGrupoTerceiro'
    DataSource = DataSource8
    TabOrder = 30
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 488
    Top = 112
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nCdTerceiro')
    Left = 616
    Top = 104
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 624
    Top = 376
  end
  object DataSource2: TDataSource
    Left = 632
    Top = 384
  end
  object DataSource3: TDataSource
    Left = 640
    Top = 392
  end
  object DataSource4: TDataSource
    DataSet = qryTerceiro
    Left = 648
    Top = 400
  end
  object DataSource5: TDataSource
    Left = 656
    Top = 408
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoEconomico'
      'WHERE nCdGrupoEconomico = :nPK')
    Left = 424
    Top = 360
    object qryGrupoEconomiconCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object dsGrupoEconomico: TDataSource
    DataSet = qryGrupoEconomico
    Left = 480
    Top = 352
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento, cNmDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 784
    Top = 328
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarca, cNmMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 824
    Top = 328
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'npk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha'
      'WHERE nCdLinha = :npk')
    Left = 864
    Top = 328
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object qryClasseProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM ClasseProduto'
      'WHERE nCdClasseProduto = :nPK')
    Left = 904
    Top = 320
    object qryClasseProdutonCdClasseProduto: TAutoIncField
      FieldName = 'nCdClasseProduto'
      ReadOnly = True
    end
    object qryClasseProdutocNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
  end
  object dsDepartamento: TDataSource
    DataSet = qryDepartamento
    Left = 784
    Top = 360
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 824
    Top = 368
  end
  object dsLinha: TDataSource
    DataSet = qryLinha
    Left = 864
    Top = 368
  end
  object dsClasseProduto: TDataSource
    DataSet = qryClasseProduto
    Left = 912
    Top = 368
  end
  object qryRamoAtividade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM RamoAtividade'
      'WHERE nCdRamoAtividade = :nPK')
    Left = 496
    Top = 432
    object qryRamoAtividadenCdRamoAtividade: TIntegerField
      FieldName = 'nCdRamoAtividade'
    end
    object qryRamoAtividadecNmRamoAtividade: TStringField
      FieldName = 'cNmRamoAtividade'
      Size = 50
    end
  end
  object DataSource6: TDataSource
    DataSet = qryRamoAtividade
    Left = 632
    Top = 448
  end
  object qryTerceiroRepres: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM TerceiroTipoTerceiro TTT'
      '               WHERE TTT.nCdTerceiro     = Terceiro.nCdTerceiro'
      '                 AND TTT.nCdTipoTerceiro = 4)')
    Left = 448
    Top = 472
    object qryTerceiroRepresnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceiroReprescNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource7: TDataSource
    DataSet = qryTerceiroRepres
    Left = 640
    Top = 456
  end
  object qryAreaVenda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdAreaVenda'
      '      ,cNmAreaVenda'
      '  FROM AreaVenda'
      ' WHERE nCdAreaVenda = :nPK'
      '   AND nCdStatus    = 1 ')
    Left = 528
    Top = 600
    object qryAreaVendanCdAreaVenda: TIntegerField
      FieldName = 'nCdAreaVenda'
    end
    object qryAreaVendacNmAreaVenda: TStringField
      FieldName = 'cNmAreaVenda'
      Size = 50
    end
  end
  object qryDivisaoVenda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdAreaVenda'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDivisaoVenda'
      '      ,cNmDivisaoVenda'
      '  FROM DivisaoVenda'
      ' WHERE nCdDivisaoVenda = :nPK'
      '   AND nCdAreaVenda    = :nCdAreaVenda'
      '   AND nCdStatus       = 1 ')
    Left = 568
    Top = 600
    object qryDivisaoVendanCdDivisaoVenda: TIntegerField
      FieldName = 'nCdDivisaoVenda'
    end
    object qryDivisaoVendacNmDivisaoVenda: TStringField
      FieldName = 'cNmDivisaoVenda'
      Size = 50
    end
  end
  object qryCanalDistribuicao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCanalDistribuicao'
      '      ,cNmCanalDistribuicao'
      '  FROM CanalDistribuicao'
      ' WHERE nCdCanalDistribuicao = :nPK'
      '   AND nCdStatus            = 1')
    Left = 608
    Top = 600
    object qryCanalDistribuicaonCdCanalDistribuicao: TIntegerField
      FieldName = 'nCdCanalDistribuicao'
    end
    object qryCanalDistribuicaocNmCanalDistribuicao: TStringField
      FieldName = 'cNmCanalDistribuicao'
      Size = 50
    end
  end
  object dsCanalDistribuicao: TDataSource
    DataSet = qryCanalDistribuicao
    Left = 624
    Top = 632
  end
  object dsDivisaoVenda: TDataSource
    DataSet = qryDivisaoVenda
    Left = 584
    Top = 632
  end
  object dsAreaVenda: TDataSource
    DataSet = qryAreaVenda
    Left = 544
    Top = 632
  end
  object qryGrupoTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoTerceiro'
      'WHERE nCdGrupoTerceiro = :nPK')
    Left = 352
    Top = 464
    object qryGrupoTerceironCdGrupoTerceiro: TIntegerField
      FieldName = 'nCdGrupoTerceiro'
    end
    object qryGrupoTerceirocNmGrupoTerceiro: TStringField
      FieldName = 'cNmGrupoTerceiro'
      Size = 50
    end
  end
  object DataSource8: TDataSource
    DataSet = qryGrupoTerceiro
    Left = 560
    Top = 368
  end
end
