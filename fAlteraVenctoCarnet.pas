unit fAlteraVenctoCarnet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, GridsEh, DBGridEh, cxPC, cxControls,
  StdCtrls, Mask, DBCtrls, ImgList, ADODB, ComCtrls, ToolWin, ExtCtrls;

type
  TfrmAlteraVenctoCarnet = class(TfrmCadastro_Padrao)
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryTitulo: TADOQuery;
    qryTitulonCdTitulo: TIntegerField;
    qryTituloiParcela: TIntegerField;
    qryTitulodDtVenc: TDateTimeField;
    qryTitulodDtVencOriginal: TDateTimeField;
    qryTitulonValTit: TBCDField;
    qryTitulonValJuro: TBCDField;
    qryTitulonSaldoTit: TBCDField;
    dsTitulo: TDataSource;
    qryMasternCdCrediario: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNmEmpresa: TStringField;
    qryMasternCdLoja: TIntegerField;
    qryMastercNmLoja: TStringField;
    qryMasterdDtVenda: TDateTimeField;
    qryMasterdDtUltVencto: TDateTimeField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNmTerceiro: TStringField;
    qryMasternValCrediario: TBCDField;
    qryMasternSaldo: TBCDField;
    qryMasterdDtLiq: TDateTimeField;
    qryMasternCdCondPagto: TIntegerField;
    qryMastercNmCondPagto: TStringField;
    qryTitulodDtLiq: TDateTimeField;
    qryAux: TADOQuery;
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure qryTituloBeforePost(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure qryTituloBeforeEdit(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAlteraVenctoCarnet: TfrmAlteraVenctoCarnet;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAlteraVenctoCarnet.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryTitulo.Close ;
  PosicionaQuery(qryTitulo, qryMasternCdCrediario.AsString) ;

  if (qryTitulo.eof) then
  begin
      MensagemAlerta('Nenhuma presta��o em aberto para este carnet.') ;
  end ;

  DBGridEh1.ReadOnly := False ;

  if (qryMasterdDtLiq.AsString <> '') then
      DBGridEh1.ReadOnly := True ;

end;

procedure TfrmAlteraVenctoCarnet.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  
end;

procedure TfrmAlteraVenctoCarnet.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (qryTitulodDtLiq.AsString <> '') and (DataCol <= 3) then
  begin

    DBGridEh1.Canvas.Brush.Color := clRed ; //$0080FFFF ;
    DBGridEh1.Canvas.Font.Color  := clWhite ;
    DBGridEh1.Canvas.FillRect(Rect);
    DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);

  end ;

end;

procedure TfrmAlteraVenctoCarnet.qryTituloBeforePost(DataSet: TDataSet);
begin

  if (qryTitulodDtLiq.AsString <> '') then
  begin
      MensagemAlerta('N�o � permitido alterar parcela paga.') ;
      abort ;
  end ;

  if (qryTitulodDtVencOriginal.AsString <> '') then
  begin
      if (qryTitulodDtVenc.Value < qryTitulodDtVencOriginal.Value) then
      begin
          MensagemAlerta('A nova data de vencimento n�o pode ser menor que a data de vencimento original.') ;
          abort ;
      end ;
  end ;

  if (qryTitulodDtVenc.Value < qryMasterdDtVenda.Value) then
  begin
      MensagemAlerta('A nova data de vencimento n�o pode ser menor que a data de compra.') ;
      abort ;
  end ;

  if (qryTitulodDtVencOriginal.AsString = '') then
      qryTitulodDtVencOriginal.Value := StrToDate(qryTitulodDtVenc.OldValue) ;

  inherited;

end;

procedure TfrmAlteraVenctoCarnet.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CREDIARIO' ;
  nCdTabelaSistema  := 66 ;
  nCdConsultaPadrao := 170 ;

end;

procedure TfrmAlteraVenctoCarnet.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;

  qryTitulo.Close ;
  
end;

procedure TfrmAlteraVenctoCarnet.btSalvarClick(Sender: TObject);
begin
  //inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMasterdDtLiq.AsString <> '') then
  begin
      MensagemAlerta('Este carnet est� quitado e n�o pode ser alterado.') ;
      abort ;
  end ;

  if (qryTitulo.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhuma parcela em aberto para grava��o.') ;
      abort ;
  end ;

  case MessageDlg('Confirma a grava��o das parcelas ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  if (qryTitulo.State <> dsBrowse) then
      qryTitulo.Post ;

  try
      qryTitulo.UpdateBatch() ;
      frmMenu.LogAuditoria(66, 4, qryMaster.FieldList[0].Value,'Altera��o Vencimento Presta��o') ;

      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE Crediario Set dDtUltVencto = (SELECT MAX(dDtVenc) FROM Titulo WHERE Titulo.nCdCrediario = Crediario.nCdCrediario) WHERE Crediario.nCdCrediario = ' + qryMasternCdCrediario.AsString) ;
      qryAux.ExecSQL ; 

  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  qryMaster.Close ;
  qryTitulo.Close ;


end;

procedure TfrmAlteraVenctoCarnet.qryTituloBeforeEdit(DataSet: TDataSet);
begin
  inherited;

  if (qryTitulodDtLiq.AsString <> '') then
  begin
      qryTitulo.Cancel;
      exit ;
  end ;
      
end;

initialization
    RegisterClass(TfrmAlteraVenctoCarnet) ;

end.
