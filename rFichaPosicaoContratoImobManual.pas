unit rFichaPosicaoContratoImobManual;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptFichaPosicaoContratoImobManual = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText3: TQRDBText;
    QRBand5: TQRBand;
    QRDBText8: TQRDBText;
    QRDBText10: TQRDBText;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    qryContrato: TADOQuery;
    qryContratonCdContratoEmpImobiliario: TIntegerField;
    qryContratocNumContrato: TStringField;
    qryContratodDtContrato: TDateTimeField;
    qryContratodDtVenc: TDateTimeField;
    qryContratonValTit: TBCDField;
    qryContratonValJuro: TBCDField;
    qryContratonValDesconto: TBCDField;
    qryContratonValAbatimento: TBCDField;
    qryContratonValLiq: TBCDField;
    qryContratodDtLiq: TDateTimeField;
    qryContratonSaldoTit: TBCDField;
    qryContratocNmTerceiro: TStringField;
    qryContratocCNPJCPF: TStringField;
    qryContratocRG: TStringField;
    qryContratocTelefone1: TStringField;
    qryContratocTelefoneMovel: TStringField;
    qryContratocFax: TStringField;
    qryContratocEmail: TStringField;
    qryContratocNmcjg: TStringField;
    qryContratonValContrato: TBCDField;
    qryContratocNmEmpImobiliario: TStringField;
    qryContratocNmBlocoEmpImobiliario: TStringField;
    qryContratocNrUnidade: TStringField;
    QRDBText1: TQRDBText;
    QRLabel14: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText15: TQRDBText;
    QRLabel18: TQRLabel;
    QRLabel20: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabel21: TQRLabel;
    QRDBText17: TQRDBText;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRDBText18: TQRDBText;
    qryContratocTelefoneEmpTrab: TStringField;
    QRLabel4: TQRLabel;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText21: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    qryContratoiParcela: TStringField;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRDBText22: TQRDBText;
    QRLabel27: TQRLabel;
    QRDBText2: TQRDBText;
    qryContratonCdTitulo: TIntegerField;
    QRLabel13: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRDBText4: TQRDBText;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFichaPosicaoContratoImobManual: TrptFichaPosicaoContratoImobManual;

implementation

{$R *.dfm}

end.
