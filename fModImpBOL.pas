unit fModImpBOL;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, GridsEh, DBGridEh, StdCtrls, DBCtrls, Mask;

type
  TfrmModImpBOL = class(TfrmCadastro_Padrao)
    qryMasternCdModImpBOL: TIntegerField;
    qryMastercNmModImpBOL: TStringField;
    qryMasternCdBanco: TIntegerField;
    qryMastercQuery: TMemoField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBMemo1: TDBMemo;
    qryCampoC: TADOQuery;
    dsCampoC: TDataSource;
    qryCampoCnCdCampoModImpBOL: TAutoIncField;
    qryCampoCnCdModImpBOL: TIntegerField;
    qryCampoCiSequencia: TIntegerField;
    qryCampoCcNmCampo: TStringField;
    qryCampoCcDescricao: TStringField;
    qryCampoCiLinha: TIntegerField;
    qryCampoCiColuna: TIntegerField;
    qryCampoCcTipoDado: TStringField;
    qryCampoCcTamanhoFonte: TStringField;
    qryCampoCcEstiloFonte: TStringField;
    qryCampoCcLPP: TStringField;
    DBGridEh1: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure qryCampoCBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmModImpBOL: TfrmModImpBOL;

implementation

{$R *.dfm}

procedure TfrmModImpBOL.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'MODIMPBOL' ;
  nCdTabelaSistema  := 50 ;
  nCdConsultaPadrao := 104 ;
end;

procedure TfrmModImpBOL.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryCampoC.Close ;
  PosicionaQuery(qryCampoC, qryMasternCdModImpBOL.asString) ;

end;

procedure TfrmModImpBOL.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmModImpBOL.qryCampoCBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryCampoCnCdModImpBOL.Value := qryMasternCdModImpBol.Value ;

end;

initialization
    RegisterClass(TfrmModImpBOL) ;

end.
