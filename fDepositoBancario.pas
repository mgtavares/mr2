unit fDepositoBancario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, cxPC, cxControls, GridsEh, DBGridEh,
  cxLookAndFeelPainters, cxButtons, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmDepositoBancario = class(TfrmCadastro_Padrao)
    qryMasternCdDepositoBancario: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasterdDtMov: TDateTimeField;
    qryMasterdDtDeposito: TDateTimeField;
    qryMasterdDtProcesso: TDateTimeField;
    qryMasternCdContaBancaria: TIntegerField;
    qryMasternCdUsuario: TIntegerField;
    qryMasternValDinheiro: TBCDField;
    qryMasternValChequeNumerario: TBCDField;
    qryMasternValChequePreDatado: TBCDField;
    qryMasternValTotalDeposito: TBCDField;
    qryMastercOBS: TMemoField;
    qryMasteriQtdeCheques: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBMemo1: TDBMemo;
    Label14: TLabel;
    DBEdit13: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit14: TDBEdit;
    DataSource1: TDataSource;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit15: TDBEdit;
    DataSource2: TDataSource;
    qryContaBancariaCred: TADOQuery;
    qryContaBancariaCrednCdContaBancaria: TIntegerField;
    qryContaBancariaCredcNmConta: TStringField;
    DBEdit16: TDBEdit;
    DataSource3: TDataSource;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DBEdit17: TDBEdit;
    DataSource4: TDataSource;
    cxPageControl1: TcxPageControl;
    TabDinheiro: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxTabSheet3: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryDinheiro: TADOQuery;
    dsDinheiro: TDataSource;
    qryDinheironCdItemDepositoBancario: TAutoIncField;
    qryDinheironCdDepositoBancario: TIntegerField;
    qryDinheiroiTipo: TIntegerField;
    qryDinheironCdContaDebito: TIntegerField;
    qryDinheiroiQtdeCheques: TIntegerField;
    qryDinheironValor: TBCDField;
    qryDinheirocNmContaDebito: TStringField;
    qryChequeNumerario: TADOQuery;
    dsChequeNumerario: TDataSource;
    DBGridEh2: TDBGridEh;
    DBGridEh3: TDBGridEh;
    qryChequePreDatado: TADOQuery;
    dsChequePre: TDataSource;
    qryContaBancariaDeb: TADOQuery;
    qryContaBancariaDebnCdContaBancaria: TIntegerField;
    qryContaBancariaDebnCdConta: TStringField;
    qryChequeNumerarionCdItemDepositoBancario: TAutoIncField;
    qryChequeNumerarionCdDepositoBancario: TIntegerField;
    qryChequeNumerarioiTipo: TIntegerField;
    qryChequeNumerarionCdContaDebito: TIntegerField;
    qryChequeNumerarioiQtdeCheques: TIntegerField;
    qryChequeNumerarionValor: TBCDField;
    qryChequePreDatadonCdItemDepositoBancario: TAutoIncField;
    qryChequePreDatadonCdDepositoBancario: TIntegerField;
    qryChequePreDatadoiTipo: TIntegerField;
    qryChequePreDatadonCdContaDebito: TIntegerField;
    qryChequePreDatadoiQtdeCheques: TIntegerField;
    qryChequePreDatadonValor: TBCDField;
    qryChequeNumerariocNmContaDebito: TStringField;
    qryChequePreDatadocNmContaDebito: TStringField;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    qryExcluirCheques: TADOQuery;
    qryTotalControle: TADOQuery;
    qryTotalControlenTotalControle: TBCDField;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    SP_PROCESSA_DEPOSITO_BANCARIO: TADOStoredProc;
    qryConsultaSaldoConta: TADOQuery;
    qryConsultaSaldoContanSaldoConta: TBCDField;
    qryConsultaFeriado: TADOQuery;
    qryConsultaFeriadoCOLUMN1: TIntegerField;
    ToolButton12: TToolButton;
    qryMasterdDtCancel: TDateTimeField;
    qryMasternCdUsuarioCancel: TIntegerField;
    DBEdit18: TDBEdit;
    Label15: TLabel;
    DBEdit19: TDBEdit;
    Label16: TLabel;
    qryUsuarioCancel: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    DBEdit20: TDBEdit;
    DataSource5: TDataSource;
    qryDinheirocIdentificacaoDepositoBanco: TStringField;
    qryChequeNumerariocIdentificacaoDepositoBanco: TStringField;
    qryChequePreDatadocIdentificacaoDepositoBanco: TStringField;
    ToolButton13: TToolButton;
    procedure FormCreate(Sender: TObject);
    procedure qryDinheiroCalcFields(DataSet: TDataSet);
    procedure qryChequeNumerarioCalcFields(DataSet: TDataSet);
    procedure qryChequePreDatadoCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit7Exit(Sender: TObject);
    procedure DBEdit5Exit(Sender: TObject);
    procedure qryDinheiroBeforePost(DataSet: TDataSet);
    procedure qryChequeNumerarioBeforePost(DataSet: TDataSet);
    procedure qryChequePreDatadoBeforePost(DataSet: TDataSet);
    procedure qryDinheiroAfterPost(DataSet: TDataSet);
    procedure qryChequeNumerarioAfterPost(DataSet: TDataSet);
    procedure qryChequePreDatadoAfterPost(DataSet: TDataSet);
    procedure qryDinheiroBeforeDelete(DataSet: TDataSet);
    procedure qryDinheiroAfterDelete(DataSet: TDataSet);
    procedure qryChequeNumerarioBeforeDelete(DataSet: TDataSet);
    procedure qryChequeNumerarioAfterDelete(DataSet: TDataSet);
    procedure qryChequePreDatadoAfterDelete(DataSet: TDataSet);
    procedure qryChequePreDatadoBeforeDelete(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure DBGridEh2DblClick(Sender: TObject);
    procedure DBGridEh3DblClick(Sender: TObject);
    procedure ToolButton12Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDepositoBancario: TfrmDepositoBancario;

implementation

uses fMenu, fLookup_Padrao, fDepositoBancario_SelecaoCheque,
  fDepositoBancario_ConsultaCheques, rDepositoBancario_view, DateUtils;

{$R *.dfm}

procedure TfrmDepositoBancario.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'DEPOSITOBANCARIO' ;
  nCdTabelaSistema  := 72 ;
  nCdConsultaPadrao := 166 ;
  bLimpaAposSalvar  := False ;

end;

procedure TfrmDepositoBancario.qryDinheiroCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryDinheirocNmContaDebito.Value = '') and (qryDinheironCdContaDebito.Value > 0) then
  begin
      qryContaBancariaDeb.Close ;
      qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
      qryContaBancariaDeb.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva ;
      PosicionaQuery(qryContaBancariaDeb, qryDInheironCdContaDebito.AsString) ;

      if not qryContaBancariaDeb.Eof then
      begin
          qryDinheirocNmContaDebito.Value := qryContaBancariaDebnCdConta.Value ;
      end ;

  end ;

end;

procedure TfrmDepositoBancario.qryChequeNumerarioCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryChequeNumerariocNmContaDebito.Value = '') and (qryChequeNumerarionCdContaDebito.Value > 0) then
  begin
      qryContaBancariaDeb.Close ;
      qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
      qryContaBancariaDeb.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva ;
      PosicionaQuery(qryContaBancariaDeb, qryChequeNumerarionCdContaDebito.AsString) ;

      if not qryContaBancariaDeb.Eof then
      begin
          qryChequeNumerariocNmContaDebito.Value := qryContaBancariaDebnCdConta.Value ;
      end ;

  end ;

end;

procedure TfrmDepositoBancario.qryChequePreDatadoCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryChequePreDatadocNmContaDebito.Value = '') and (qryChequePreDatadonCdContaDebito.Value > 0) then
  begin
      qryContaBancariaDeb.Close ;
      qryContaBancariaDeb.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
      qryContaBancariaDeb.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva ;
      PosicionaQuery(qryContaBancariaDeb, qryChequePreDatadonCdContaDebito.AsString) ;

      if not qryContaBancariaDeb.Eof then
      begin
          qryChequePreDatadocNmContaDebito.Value := qryContaBancariaDebnCdConta.Value ;
      end ;

  end ;

end;

procedure TfrmDepositoBancario.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryEmpresa        , qryMasternCdEmpresa.asString) ;
  PosicionaQuery(qryLoja           , qryMasternCdLoja.asString) ;
  PosicionaQuery(qryUsuario        , qryMasternCdUsuario.AsString) ;
  PosicionaQuery(qryUsuarioCancel  , qryMasternCdUsuarioCancel.AsString);

  qryContaBancariaCred.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryContaBancariaCred.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value ;
  PosicionaQuery(qryContaBancariaCred, qryMasternCdContaBancaria.AsString) ;

  PosicionaQuery(qryDinheiro       , qryMasternCdDepositoBancario.AsString) ;
  PosicionaQuery(qryChequeNumerario, qryMasternCdDepositoBancario.AsString) ;
  PosicionaQuery(qryChequePreDatado, qryMasternCdDepositoBancario.AsString) ;

  ToolButton10.Enabled := True ;
  cxButton1.Enabled    := True ;
  cxButton2.Enabled    := True ;
  cxButton3.Enabled    := True ;
  cxButton4.Enabled    := True ;
  DBGridEh1.ReadOnly   := False ;
  btSalvar.Visible     := True  ;
  btExcluir.Visible    := True  ;

  if (qryMasterdDtProcesso.AsString <> '') then
  begin
      ToolButton10.Enabled := False ;
      cxButton1.Enabled    := False ;
      cxButton2.Enabled    := False ;
      cxButton3.Enabled    := False ;
      cxButton4.Enabled    := False ;
      DBGridEh1.ReadOnly   := True  ;
      btSalvar.Visible     := False ;
      ToolButton2.Visible  := False ;
      btExcluir.Visible    := False ;
      ToolButton6.Visible  := False ;
  end ;

end;

procedure TfrmDepositoBancario.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryLoja.Close ;
  qryUsuario.Close ;

  qryContaBancariaCred.Close ;
  qryDinheiro.Close ;
  qryChequeNumerario.Close ;
  qryChequePreDatado.Close ;

end;

procedure TfrmDepositoBancario.btIncluirClick(Sender: TObject);
begin
  inherited;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
  qryMasternCdUsuario.Value := frmMenu.nCdUsuarioLogado ;
  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;
  PosicionaQuery(qryUsuario, qryMasternCdUsuario.AsString) ;

  if (DBEdit3.Enabled) then
  begin
      qryMasternCdLoja.Value := frmMenu.nCdLojaAtiva;
      PosicionaQuery(qryLoja, qryMasternCdLoja.AsString) ;
      
      DBEdit3.SetFocus ;
  end
  else DBedit7.SetFocus;

  btSalvar.Visible  := True ;
  btExcluir.Visible := True ;

end;

procedure TfrmDepositoBancario.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      DBEdit3.Enabled := False ;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  cxPageControl1.ActivePageIndex := 0 ;
      
end;

procedure TfrmDepositoBancario.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBEdit3.Enabled) and (DBEdit15.Text = '') then
  begin
      MensagemAlerta('Informe a loja.') ;
      DBEdit3.SetFocus ;
      abort ;
  end ;

  if (DBEdit16.Text = '') then
  begin
      MensagemAlerta('Informe a conta banc�ria de cr�dito.') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (qryMasterdDtDeposito.AsString = '') then
  begin
      MensagemAlerta('Informe a data de dep�sito.') ;
      DBEdit5.SetFocus;
      abort ;
  end ;

  if (frmMenu.LeParametro('DIASTOLDTDEPOS') <> '') then
  begin
  if (qryMasterdDtDeposito.Value - Today > StrToInt(frmMenu.LeParametro('DIASTOLDTDEPOS'))) then
      begin
          MensagemAlerta('A data de dep�sito informado est� fora da toler�ncia m�xima permitida.' + #13#13 + 'Toler�ncia ' + DateToStr(Today + StrToInt(frmMenu.LeParametro('DIASTOLDTDEPOS'))));
          DBEdit5.SetFocus;
          abort ;
      end ;
  end;

  if (frmMenu.LeParametro('DIASTOLDTDEANT') <> '') then
  begin
  if (Today - qryMasterdDtDeposito.Value > StrToInt(frmMenu.LeParametro('DIASTOLDTDEANT'))) then
      begin
          MensagemAlerta('A data de dep�sito informado est� fora da toler�ncia m�nima permitida.' + #13#13 + 'Toler�ncia ' + DateToStr(Today - StrToInt(frmMenu.LeParametro('DIASTOLDTDEANT'))));
          DBEdit5.SetFocus;
          abort ;
      end ;
  end;

  if (StrToDateTime(frmMenu.LeParametro('DTCONTAB')) >= qryMasterdDtDeposito.Value) then
  begin
      MensagemAlerta('A data do dep�sito n�o pode ser inferior a data da contabiliza��o financeira.') ;
      Abort ;
  end ;

  inherited;

  qryMasterdDtMov.Value := Now() ;

end;

procedure TfrmDepositoBancario.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  if not qryDinheiro.Active then
  begin
      PosicionaQuery(qryDinheiro       , qryMasternCdDepositoBancario.AsString) ;
      PosicionaQuery(qryChequeNumerario, qryMasternCdDepositoBancario.AsString) ;
      PosicionaQuery(qryChequePreDatado, qryMasternCdDepositoBancario.AsString) ;
  end ;

end;

procedure TfrmDepositoBancario.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, DBEdit3.Text) ;
  
end;

procedure TfrmDepositoBancario.DBEdit7Exit(Sender: TObject);
begin
  inherited;

  if (DBEdit15.Text = '') and (DBEdit3.Enabled) then
  begin
      MensagemAlerta('Informe a Loja.') ;
      DbEdit3.SetFocus ;
      exit ;
  end ;

  qryContaBancariaCred.Close ;
  qryContaBancariaCred.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryContaBancariaCred.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value ;
  PosicionaQuery(qryContaBancariaCred, DBedit7.Text) ;


end;

procedure TfrmDepositoBancario.DBEdit5Exit(Sender: TObject);
begin
  inherited;
  if (qryMaster.State = dsInsert) then
  begin
      if (trim(DBEdit5.Text) <> '/  /') then
      begin
          qryConsultaFeriado.Close ;
          qryConsultaFeriado.Parameters.ParamByName('cDtFeriado').Value := DBEdit5.Text ;
          qryConsultaFeriado.Open ;

          if not qryConsultaFeriado.eof then
              MensagemAlerta('Esta data � um feriado banc�rio.') ;

          qryConsultaFeriado.Close ;
      end ;
      
      qryMaster.Post ;
  end ;

  if (cxPageControl1.ActivePageIndex = 0) then
      DBGridEh1.SetFocus;

end;

procedure TfrmDepositoBancario.qryDinheiroBeforePost(DataSet: TDataSet);
begin

  if (qryDinheirocNmContaDebito.Value = '') then
  begin
      MensagemAlerta('Informe a conta de D�bito.') ;
      abort ;
  end ;

  if (qryDinheironValor.Value <= 0) then
  begin
      MensagemAlerta('Informe o montante em dinheiro.') ;
      abort ;
  end ;

  qryConsultaSaldoConta.Close ;
  PosicionaQuery(qryConsultaSaldoConta, qryDinheironCdContaDebito.AsString) ;

  if (not qryConsultaSaldoConta.Eof) then
  begin
      if (qryDinheironValor.Value > qryConsultaSaldoContanSaldoCOnta.Value) then
      begin
          qryConsultaSaldoConta.Close ;
          MensagemAlerta('Valor Informado para dep�sito maior que o saldo dispon�vel na conta.') ;
          abort ;
      end ;
  end ;

  qryConsultaSaldoConta.Close ;

  inherited;

  qryDinheironCdDepositoBancario.Value := qryMasternCdDepositoBancario.Value ;
  qryDinheiroiTipo.Value               := 1 ;

  if (qryDinheiro.State = dsEdit) then
  begin
      qryMasternValDinheiro.Value      := qryMasternValDinheiro.Value      - StrToFloat(qryDinheironValor.OldValue) ;
      qryMasternValTotalDeposito.Value := qryMasternValTotalDeposito.Value - StrToFloat(qryDinheironValor.OldValue) ;
  end ;

  if (qryDinheiro.State = dsInsert) then
      qryDinheironCdItemDepositoBancario.Value := frmMenu.fnProximoCodigo('ITEMDEPOSITOBANCARIO') ;

end;

procedure TfrmDepositoBancario.qryChequeNumerarioBeforePost(
  DataSet: TDataSet);
begin
  inherited;
  if (qryChequeNumerariocNmContaDebito.Value = '') then
  begin
      MensagemAlerta('Informe a conta de D�bito.') ;
      abort ;
  end ;

  qryChequeNumerarionCdDepositoBancario.Value := qryMasternCdDepositoBancario.Value ;
  qryChequeNumerarioiTipo.Value               := 2 ;

  if (qryChequeNumerario.State = dsEdit) then
  begin
      qryMasternValChequeNumerario.Value := qryMasternValChequeNumerario.Value - StrToFloat(qryChequeNumerarionValor.OldValue) ;
      qryMasternValTotalDeposito.Value   := qryMasternValTotalDeposito.Value   - StrToFloat(qryChequeNumerarionValor.OldValue) ;
  end ;

end;

procedure TfrmDepositoBancario.qryChequePreDatadoBeforePost(
  DataSet: TDataSet);
begin

  if (qryChequePreDatadocNmContaDebito.Value = '') then
  begin
      MensagemAlerta('Informe a conta de D�bito.') ;
      abort ;
  end ;

  inherited;
  qryChequePreDatadonCdDepositoBancario.Value := qryMasternCdDepositoBancario.Value ;
  qryChequePreDatadoiTipo.Value               := 3 ;

  if (qryChequePreDatado.State = dsEdit) then
  begin
      qryMasternValChequePreDatado.Value := qryMasternValChequePreDatado.Value - StrToFloat(qryChequePreDatadonValor.OldValue) ;
      qryMasternValTotalDeposito.Value   := qryMasternValTotalDeposito.Value   - StrToFloat(qryChequePreDatadonValor.OldValue) ;
  end ;

end;

procedure TfrmDepositoBancario.qryDinheiroAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryMasternValDinheiro.Value          := qryMasternValDinheiro.Value      + qryDinheironValor.Value ;
  qryMasternValTotalDeposito.Value     := qryMasternValTotalDeposito.Value + qryDinheironValor.Value ;

  qryMaster.Post ;

end;

procedure TfrmDepositoBancario.qryChequeNumerarioAfterPost(
  DataSet: TDataSet);
begin
  inherited;
  qryMasternValChequeNumerario.Value := qryMasternValChequeNumerario.Value + qryChequeNumerarionValor.Value ;
  qryMasternValTotalDeposito.Value   := qryMasternValTotalDeposito.Value + qryChequeNumerarionValor.Value ;

  qryMaster.Post ;
end;

procedure TfrmDepositoBancario.qryChequePreDatadoAfterPost(
  DataSet: TDataSet);
begin
  inherited;

  qryMasternValChequePreDatado.Value   := qryMasternValChequePreDatado.Value + qryChequePreDatadonValor.Value ;
  qryMasternValTotalDeposito.Value     := qryMasternValTotalDeposito.Value   + qryChequePreDatadonValor.Value ;
  qryMaster.Post ;

end;

procedure TfrmDepositoBancario.qryDinheiroBeforeDelete(DataSet: TDataSet);
begin
  inherited;
  qryMasternValDinheiro.Value          := qryMasternValDinheiro.Value      - qryDinheironValor.Value ;
  qryMasternValTotalDeposito.Value     := qryMasternValTotalDeposito.Value - qryDinheironValor.Value ;

end;

procedure TfrmDepositoBancario.qryDinheiroAfterDelete(DataSet: TDataSet);
begin
  inherited;

  qryMaster.Post ;
  
end;

procedure TfrmDepositoBancario.qryChequeNumerarioBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;
  qryMasternValChequeNumerario.Value := qryMasternValChequeNumerario.Value - qryChequeNumerarionValor.Value ;
  qryMasternValTotalDeposito.Value   := qryMasternValTotalDeposito.Value - qryChequeNumerarionValor.Value ;

end;

procedure TfrmDepositoBancario.qryChequeNumerarioAfterDelete(
  DataSet: TDataSet);
begin
  inherited;

  qryMaster.Post ;
  
end;

procedure TfrmDepositoBancario.qryChequePreDatadoAfterDelete(
  DataSet: TDataSet);
begin
  inherited;

  qryMaster.Post ;
end;

procedure TfrmDepositoBancario.qryChequePreDatadoBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  qryMasternValChequePreDatado.Value   := qryMasternValChequePreDatado.Value - qryChequePreDatadonValor.Value ;
  qryMasternValTotalDeposito.Value     := qryMasternValTotalDeposito.Value   - qryChequePreDatadonValor.Value ;

end;

procedure TfrmDepositoBancario.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDepositoBancario.DBEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (frmMenu.LeParametro('VAREJO') = 'S') then
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = @@Empresa AND ((@@Loja = 0) OR (' + qryLojanCdLoja.AsString +' = ContaBancaria.nCdLoja)) AND cFlgDeposito = 1');
            end
            else
            begin
                nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = @@Empresa AND cFlgDeposito = 1');
            end ;

            If (nPK > 0) then
            begin
                qryMasternCdContaBancaria.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDepositoBancario.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryDinheiro.State = dsBrowse) then
            qryDinheiro.Edit ;

        if (qryDinheiro.State = dsInsert) or (qryDinheiro.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(37,'ContaBancaria.nCdEmpresa = @@Empresa AND ((@@Loja = 0) OR (@@Loja = ContaBancaria.nCdLoja)) AND ((cFlgCaixa = 1 OR cFlgCofre = 1))');

            If (nPK > 0) then
            begin
                qryDinheironCdContaDebito.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDepositoBancario.cxButton1Click(Sender: TObject);
var
  objForm : TfrmDepositoBancario_SelecaoCheque ;
begin

  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum dep�sito banc�rio ativo.') ;
      exit ;
  end ;

  if (qryMasterdDtProcesso.AsString <> '') then
  begin
      MensagemAlerta('Dep�sito j� processado. Imposs�vel adicionar cheques.') ;
      exit ;
  end ;

  objForm := TfrmDepositoBancario_SelecaoCheque.Create(Self) ;

  objForm.nCdEmpresa          := qryMasternCdEmpresa.Value ;
  objForm.nCdLoja             := frmMenu.nCdLojaAtiva ;
  objForm.nCdContaBancariaDep := qryMasternCdContaBancaria.Value ;
  objForm.nCdDepositoBancario := qryMasternCdDepositoBancario.Value ;

  objForm.ChequeNumerario(qryMasterdDtDeposito.AsString);

  freeAndNil( objForm ) ;

  PosicionaQuery(qryMaster, qryMasternCdDepositoBancario.AsString) ;

  DBGridEh2.SetFocus ;

end;

procedure TfrmDepositoBancario.cxButton4Click(Sender: TObject);
var
  objForm : TfrmDepositoBancario_SelecaoCheque ;
begin

  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum dep�sito banc�rio ativo.') ;
      exit ;
  end ;

  if (qryMasterdDtProcesso.AsString <> '') then
  begin
      MensagemAlerta('Dep�sito j� processado. Imposs�vel adicionar cheques.') ;
      exit ;
  end ;

  objForm := TfrmDepositoBancario_SelecaoCheque.Create(Self) ;

  objForm.nCdEmpresa          := qryMasternCdEmpresa.Value ;
  objForm.nCdLoja             := frmMenu.nCdLojaAtiva;
  objForm.nCdContaBancariaDep := qryMasternCdContaBancaria.Value ;
  objForm.nCdDepositoBancario := qryMasternCdDepositoBancario.Value ;

  objForm.ChequePreDatado(DateToStr(Now()+1));

  freeAndNil( objForm ) ;

  PosicionaQuery(qryMaster, qryMasternCdDepositoBancario.AsString) ;

  DBGridEh3.SetFocus ;  

end;

procedure TfrmDepositoBancario.cxButton2Click(Sender: TObject);
begin

  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum dep�sito banc�rio ativo.') ;
      exit ;
  end ;

  if (qryMasterdDtProcesso.AsString <> '') then
  begin
      MensagemAlerta('Dep�sito j� processado. Imposs�vel adicionar cheques.') ;
      exit ;
  end ;

  if (qryChequeNumerario.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum lote de cheque selecionado.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a exclus�o ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  qryExcluirCheques.Close ;
  qryExcluirCheques.Parameters.ParamByName('iTipo').Value                   := 2;
  qryExcluirCheques.Parameters.ParamByName('nCdDepositoBancario').Value     := qryMasternCdDepositoBancario.Value ;
  qryExcluirCheques.Parameters.ParamByName('nCdItemDepositoBancario').Value := qryChequeNumerarionCdItemDepositoBancario.Value ;

  frmMenu.Connection.BeginTrans ;

  try
      qryExcluirCheques.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Cheque(s) excluso(s) com sucesso.') ;

  PosicionaQuery(qryMaster, qryMasternCdDepositoBancario.AsString) ;

end;

procedure TfrmDepositoBancario.cxButton3Click(Sender: TObject);
begin
  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum dep�sito banc�rio ativo.') ;
      exit ;
  end ;

  if (qryMasterdDtProcesso.AsString <> '') then
  begin
      MensagemAlerta('Dep�sito j� processado. Imposs�vel adicionar cheques.') ;
      exit ;
  end ;

  if (qryChequePreDatado.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum lote de cheque selecionado.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a exclus�o ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  qryExcluirCheques.Close ;
  qryExcluirCheques.Parameters.ParamByName('iTipo').Value                   := 3;
  qryExcluirCheques.Parameters.ParamByName('nCdDepositoBancario').Value     := qryMasternCdDepositoBancario.Value ;
  qryExcluirCheques.Parameters.ParamByName('nCdItemDepositoBancario').Value := qryChequePreDatadonCdItemDepositoBancario.Value ;

  frmMenu.Connection.BeginTrans ;

  try
      qryExcluirCheques.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Cheque(s) excluso(s) com sucesso.') ;

  PosicionaQuery(qryMaster, qryMasternCdDepositoBancario.AsString) ;

end;

procedure TfrmDepositoBancario.ToolButton10Click(Sender: TObject);
var
    cTotalControle : string ;
    nTotalControle : double ;
begin
  inherited;

  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum dep�sito banc�rio ativo.') ;
      exit ;
  end ;

  if (qryMasterdDtProcesso.AsString <> '') then
  begin
      MensagemAlerta('Dep�sito j� processado.') ;
      exit ;
  end ;

  if (qryMasternValTotalDeposito.Value <= 0) then
  begin
      MensagemAlerta('Dep�sito sem valor n�o pode ser processado.') ;
      exit ;
  end ;

  if (frmMenu.nCdUsuarioLogado <> qryMasternCdUsuario.Value) then
  begin
      MensagemAlerta('Somente o usu�rio que cadastrou o dep�sito pode processar.') ;
      exit ;
  end ;

  ShowMessage('Insira o total de controle.' +#13#13+ 'O Total de controle � formado pela soma do valor em dinheiro + a soma do valor dos cheques + a soma da numera��o dos cheques do dep�sito') ;

  InputQuery('ER2Soft','Total de Controle',cTotalControle) ;

  if (trim(cTotalControle) = '') then
  begin
      cTotalControle := '0' ;
  end ;

  cTotalControle := StringReplace(cTotalControle,'.',',',[rfReplaceAll, rfIgnoreCase]) ;

  try
      nTotalControle := StrToFloat(cTotalControle) ;
  except
      MensagemAlerta('Valor inv�lido. N�o informe o ponto do milhar, somente o ponto do centavo.' +#13#13+ 'Correto: 1000,00   Errado: 1.000,00') ;
      exit ;
  end ;

  qryTotalControle.Close ;
  qryTotalControle.Parameters.ParamByName('nCdDepositoBancario').Value := qryMasternCdDepositoBancario.Value ;
  qryTotalControle.Open ;

  if not qryTotalControle.Eof then
  begin

      if (ABS(nTotalControle - qryTotalControlenTotalControle.Value) > 0.01) then
      begin
          qryTotalControle.Close ;
          MensagemAlerta('Total de controle n�o confere.') ;
          exit ;
      end ;

  end ;

  qryTotalControle.Close ;

  case MessageDlg('Confirma o processamento do dep�sito ? ' +#13#13+ 'Ap�s este passo o dep�sito n�o poder� sofrer altera��es.',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;
  
  frmMenu.Connection.BeginTrans;

  try
      SP_PROCESSA_DEPOSITO_BANCARIO.Close ;
      SP_PROCESSA_DEPOSITO_BANCARIO.Parameters.ParamByName('@nCdDepositoBancario').Value := qryMasternCdDepositoBancario.Value ;
      SP_PROCESSA_DEPOSITO_BANCARIO.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Dep�sito Processado com Sucesso.') ;

  case MessageDlg('Deseja imprimir o comprovante ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes:ToolButton12.Click ;
  end ;

  btCancelar.Click;

end;

procedure TfrmDepositoBancario.DBGridEh2DblClick(Sender: TObject);
var
  objForm : TfrmDepositoBancario_ConsultaCheques ;
begin
  inherited;

  if (not qryChequeNumerario.eof) then
  begin

      objForm := TfrmDepositoBancario_ConsultaCheques.Create(Self) ;

      PosicionaQuery(objForm.qryCheques, qryChequeNumerarionCdItemDepositoBancario.AsString) ;
      showForm( objForm , TRUE ) ;

  end ;

end;

procedure TfrmDepositoBancario.DBGridEh3DblClick(Sender: TObject);
var
  objForm : TfrmDepositoBancario_ConsultaCheques ;
begin
  inherited;

  if (not qryChequePreDatado.eof) then
  begin

      objForm := TfrmDepositoBancario_ConsultaCheques.Create(Self) ;

      PosicionaQuery(objForm.qryCheques, qryChequePreDatadonCdItemDepositoBancario.AsString) ;
      showForm( objForm , TRUE ) ;

  end ;

end;

procedure TfrmDepositoBancario.ToolButton12Click(Sender: TObject);
var
  objRel : TrptDepositoBancario_view;
begin
  inherited;

  if (not qryMaster.Active) then
  begin
      MensagemAlerta('Nenhum dep�sito banc�rio ativo.') ;
      exit ;
  end ;

  objRel := TrptDepositoBancario_view.Create(Self);

  try
      try
          PosicionaQuery(objRel.qryDepositoBancario, qryMasternCdDepositoBancario.AsString);

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

          if(objRel.qryDepositoBancariodDtCancel.AsString <> '') then
              objRel.lblCancelado.Caption := '*** CANCELADO EM ' + objRel.qryDepositoBancariodDtCancel.AsString + ' ***'
          else
              objRel.lblCancelado.Caption := '';

          {--visualiza o relat�rio--}
          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          Raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

initialization
    RegisterClass(TfrmDepositoBancario) ;
    
end.
