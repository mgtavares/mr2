unit rImpSP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QuickRpt, QRCtrls, DB, ADODB, ExtCtrls, jpeg;

type
  TrptImpSP = class(TForm)
    QuickRep1: TQuickRep;
    qryImpSP: TADOQuery;
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRBand2: TQRBand;
    QRGroup1: TQRGroup;
    qryImpSPnCdSP: TIntegerField;
    qryImpSPdDtRef: TDateTimeField;
    qryImpSPnValPagto: TBCDField;
    qryImpSPnCdCategFinanc: TIntegerField;
    qryImpSPnCdCC: TIntegerField;
    qryImpSPnCdUnidadeNegocio: TIntegerField;
    DataSource1: TDataSource;
    QRDBText1: TQRDBText;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRBand3: TQRBand;
    qryImpSPnCdTerceiro: TIntegerField;
    qryImpSPcCNPJCPF: TStringField;
    qryImpSPcNmTerceiro: TStringField;
    qryImpSPcNrTit: TStringField;
    qryImpSPcSerieTit: TStringField;
    qryImpSPdDtReceb: TDateTimeField;
    qryImpSPdDtEmissao: TDateTimeField;
    qryImpSPnValSP: TBCDField;
    qryImpSPdDtCad: TDateTimeField;
    qryImpSPdDtLiber: TDateTimeField;
    qryImpSPnCdTipoSP: TIntegerField;
    qryImpSPcNmTipoSP: TStringField;
    qryImpSPcNmCategFinanc: TStringField;
    qryImpSPcNmCC: TStringField;
    qryImpSPcNmUnidadeNegocio: TStringField;
    QRLabel4: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel5: TQRLabel;
    lblEmpresa: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    a: TQRBand;
    QRLabel12: TQRLabel;
    QRDBText8: TQRDBText;
    QRLabel13: TQRLabel;
    QRDBText9: TQRDBText;
    QRLabel14: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel15: TQRLabel;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText17: TQRDBText;
    QRShape2: TQRShape;
    qryPrazo: TADOQuery;
    qryPrazonCdPrazoSP: TAutoIncField;
    qryPrazonCdSP: TIntegerField;
    qryPrazodDtVenc: TDateTimeField;
    qryPrazonValPagto: TBCDField;
    QRSubDetail1: TQRSubDetail;
    QRSubDetail2: TQRSubDetail;
    QRDBText6: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText18: TQRDBText;
    GroupHeaderBand1: TQRBand;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRDBText19: TQRDBText;
    QRLabel23: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel24: TQRLabel;
    QRShape1: TQRShape;
    QRShape4: TQRShape;
    qryImpSPcNmLoja: TStringField;
    qryImpSPcNmUsuario: TStringField;
    QRDBText20: TQRDBText;
    QRShape6: TQRShape;
    QRLabel16: TQRLabel;
    QRShape7: TQRShape;
    QRLabel17: TQRLabel;
    QRDBText14: TQRDBText;
    QRLabel18: TQRLabel;
    qryImpSPcOBSSP: TMemoField;
    QRDBText15: TQRDBText;
    QRLabel22: TQRLabel;
    qryImpSPcNmFormaPagto: TStringField;
    QRLabel25: TQRLabel;
    QRDBText16: TQRDBText;
    qryImpSPnCdLoja: TStringField;
    qryImpSPcOBS: TStringField;
    QRDBText21: TQRDBText;
    QRLabel26: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel2: TQRLabel;
    qryPrazocNrTit: TStringField;
    qryPrazocCodBarra: TStringField;
    QRLabel27: TQRLabel;
    QRDBText22: TQRDBText;
    QRLabel28: TQRLabel;
    QRDBText23: TQRDBText;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptImpSP: TrptImpSP;

implementation

uses
  fMenu;

{$R *.dfm}

end.
