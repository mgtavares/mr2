unit fBaixaProvisao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  cxLookAndFeelPainters, cxButtons, DB, ADODB, GridsEh, DBGridEh,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmBaixaProvisao = class(TfrmProcesso_Padrao)
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdBanco: TIntegerField;
    qryContaBancariacNmBanco: TStringField;
    qryContaBancariacAgencia: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    qryContaBancariacNmTitular: TStringField;
    qryContaBancarianValTotal: TBCDField;
    dsContaBancaria: TDataSource;
    DBGridEh1: TDBGridEh;
    qryContaBancarianCdContaBancaria: TIntegerField;
    GroupBox1: TGroupBox;
    cxButton1: TcxButton;
    edtDtPagto: TDateTimePicker;
    Label1: TLabel;
    procedure cxButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBaixaProvisao: TfrmBaixaProvisao;

implementation

uses fMenu, fBaixaProvisao_FormaPagto;

{$R *.dfm}

procedure TfrmBaixaProvisao.cxButton1Click(Sender: TObject);
begin
  inherited;

  qryContaBancaria.Close ;
  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryContaBancaria.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryContaBancaria.Parameters.ParamByName('dDtPagto').Value   := DateToStr(edtDtPagto.Date) ;
  qryContaBancaria.Open ;

  if (qryContaBancaria.eof) then
  begin
      MensagemAlerta('Nenhuma provis�o para esta data de pagamento.') ;
  end ;

end;

procedure TfrmBaixaProvisao.FormShow(Sender: TObject);
begin
  inherited;
  edtDtPagto.DateTime := Now() ;

end;

procedure TfrmBaixaProvisao.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmBaixaProvisao_FormaPagto;
begin
  inherited;

  if not qryContaBancaria.eof and (qryContaBancarianCdContaBancaria.Value > 0) then
  begin

      objForm := TfrmBaixaProvisao_FormaPagto.Create(nil) ;

      objForm.qryFormaPagto.Close ;
      objForm.qryFormaPagto.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva;
      objForm.qryFormaPagto.Parameters.ParamByName('dDtPagto').Value         := DateToStr(edtDtPagto.Date);
      objForm.qryFormaPagto.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
      objForm.qryFormaPagto.Parameters.ParamByName('nCdContaBancaria').Value := qryContaBancarianCdContaBancaria.Value ;
      objForm.qryFormaPagto.Open ;

      showForm( objForm , TRUE ) ;

      qryContaBancaria.Close ;
      qryContaBancaria.Open ;

  end ;

end;

initialization
    RegisterClass(TfrmBaixaProvisao) ;
    
end.
