inherited frmEncerrarMovtoPOS: TfrmEncerrarMovtoPOS
  Left = 151
  Top = 14
  Width = 1177
  Height = 699
  Caption = 'Encerrar Movimento POS'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 410
    Width = 1161
    Height = 251
  end
  inherited ToolBar1: TToolBar
    Width = 1161
    ButtonWidth = 84
    inherited ToolButton1: TToolButton
      Caption = '&Gerar Lote'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 84
    end
    inherited ToolButton2: TToolButton
      Left = 92
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1161
    Height = 76
    Align = alTop
    Caption = ' Filtro '
    TabOrder = 1
    object Label5: TLabel
      Tag = 1
      Left = 11
      Top = 24
      Width = 99
      Height = 13
      Alignment = taRightJustify
      Caption = 'Grupo de Operadora'
    end
    object Label3: TLabel
      Tag = 1
      Left = 32
      Top = 49
      Width = 78
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Movimento'
    end
    object MaskEdit6: TMaskEdit
      Left = 120
      Top = 16
      Width = 61
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnChange = MaskEdit6Change
      OnExit = MaskEdit6Exit
      OnKeyDown = MaskEdit6KeyDown
    end
    object MaskEdit1: TMaskEdit
      Left = 120
      Top = 41
      Width = 81
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 1
      Text = '  /  /    '
      OnChange = MaskEdit1Change
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 184
      Top = 16
      Width = 401
      Height = 21
      DataField = 'cNmGrupoOperadoraCartao'
      DataSource = dsGrupoOperadora
      TabOrder = 3
    end
    object cxButton1: TcxButton
      Left = 202
      Top = 40
      Width = 145
      Height = 25
      Caption = 'Exibir Comprovantes'
      TabOrder = 2
      OnClick = cxButton1Click
      Glyph.Data = {
        36030000424D360300000000000036000000280000000F000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFF3E3934
        393430332F2B2C2925272421201D1BE7E7E73331300B0A090707060404030000
        00000000FFFFFF000000FFFFFF46413B857A70C3B8AE7C72687F756B36322DF2
        F2F14C4A4795897DBAAEA27C72687F756B010101FFFFFF000000FFFFFF4D4741
        83786FCCC3BA786F657B716734302DFEFEFE2C2A2795897DC2B8AD786F657C72
        68060505FFFFFF000000FFFFFF554E4883786FCCC3BA79706671685F585550FF
        FFFF494645857A70C2B8AD786F657B71670D0C0BFFFFFF000000FFFFFF817B76
        9F9286CCC3BAC0B4AAA6988B807D79FFFFFF74726F908479C2B8ADC0B4AAA89B
        8E494747FFFFFF000000FCFCFC605952423D3858514A3D3833332F2B393734D3
        D3D35F5E5C1A18162522201917150F0E0D121212FDFDFD000000FDFDFD9D9185
        B1A3967F756B7C7268776D646C635B2E2A26564F4880766C7C7268776D647067
        5E010101FAFAFA000000FEFDFDB8ACA1BAAEA282776D82776DAA917BBAA794B8
        A690B097819F8D7D836D5B71635795897D232322FCFCFC000000FDFCFCDDDAD7
        9B8E829D9185867B71564F48504A4480766C6E665D826C58A6917D948474564F
        488B8A8AFEFEFE000000FFFFFFFFFFFF746B62A4978A95897D9F92863E3934FF
        FFFF4C46407E746A857A703E393485817EF5F5F5FDFDFD000000FFFFFFFFFFFF
        FFFFFFFFFFFF9B9187C3B8AE655D55FFFFFF7C7268A89B8EA69B90FFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFA79C91BCB0A49D9185FF
        FFFFAEA0939D91857B756EFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000}
      LookAndFeel.Kind = lfUltraFlat
      LookAndFeel.NativeStyle = True
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 122
    Width = 1161
    Height = 271
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      DataController.DataSource = dsTransacaoPendente
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skCount
          FieldName = 'iNrDocto'
          Column = cxGrid1DBTableView1iNrDocto
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValTransacao
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValLiquido
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGrid1DBTableView1nValTaxaOperadora
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1nCdTransacaoCartao: TcxGridDBColumn
        DataBinding.FieldName = 'nCdTransacaoCartao'
        Visible = False
      end
      object cxGrid1DBTableView1dDtTransacao: TcxGridDBColumn
        Caption = 'Dt. Transa'#231#227'o'
        DataBinding.FieldName = 'dDtTransacao'
        Width = 120
      end
      object cxGrid1DBTableView1iNrCartao: TcxGridDBColumn
        Caption = 'N'#250'm. Cart'#227'o'
        DataBinding.FieldName = 'iNrCartao'
        Visible = False
        Width = 100
      end
      object cxGrid1DBTableView1iNrDocto: TcxGridDBColumn
        Caption = 'N'#250'm. Docto'
        DataBinding.FieldName = 'iNrDocto'
        Width = 96
      end
      object cxGrid1DBTableView1nCdOperadoraCartao: TcxGridDBColumn
        DataBinding.FieldName = 'nCdOperadoraCartao'
        Visible = False
      end
      object cxGrid1DBTableView1cNmOperadoraCartao: TcxGridDBColumn
        Caption = 'Operadora'
        DataBinding.FieldName = 'cNmOperadoraCartao'
        Width = 200
      end
      object cxGrid1DBTableView1iParcelas: TcxGridDBColumn
        Caption = 'Parcelas'
        DataBinding.FieldName = 'iParcelas'
        Width = 67
      end
      object cxGrid1DBTableView1nValTransacao: TcxGridDBColumn
        Caption = 'Valor Bruto'
        DataBinding.FieldName = 'nValTransacao'
        Width = 95
      end
      object cxGrid1DBTableView1nValLiquido: TcxGridDBColumn
        Caption = 'Valor L'#237'quido'
        DataBinding.FieldName = 'nValLiquido'
        Width = 97
      end
      object cxGrid1DBTableView1nValTaxaOperadora: TcxGridDBColumn
        Caption = 'Valor Desconto'
        DataBinding.FieldName = 'nValTaxaOperadora'
        Width = 111
      end
      object cxGrid1DBTableView1nCdStatusDocto: TcxGridDBColumn
        DataBinding.FieldName = 'nCdStatusDocto'
        Visible = False
      end
      object cxGrid1DBTableView1cNmStatusDocto: TcxGridDBColumn
        Caption = 'Situa'#231#227'o do Comprovante'
        DataBinding.FieldName = 'cNmStatusDocto'
        Width = 276
      end
      object cxGrid1DBTableView1cFlgSelecionado: TcxGridDBColumn
        DataBinding.FieldName = 'cFlgSelecionado'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxGrid2: TcxGrid [4]
    Left = 0
    Top = 410
    Width = 1161
    Height = 251
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object cxGridDBTableView1: TcxGridDBTableView
      OnDblClick = cxGridDBTableView1DblClick
      DataController.DataSource = dsTransacaoSelecionada
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skCount
          FieldName = 'iNrDocto'
          Column = cxGridDBTableView1iNrDocto
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValTransacao
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValLiquido
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValTaxaOperadora
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.ColumnAutoWidth = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object cxGridDBTableView1nCdTransacaoCartao: TcxGridDBColumn
        DataBinding.FieldName = 'nCdTransacaoCartao'
        Visible = False
      end
      object cxGridDBTableView1dDtTransacao: TcxGridDBColumn
        Caption = 'Dt. Transa'#231#227'o'
        DataBinding.FieldName = 'dDtTransacao'
        Width = 120
      end
      object cxGridDBTableView1iNrCartao: TcxGridDBColumn
        Caption = 'N'#250'm. Cart'#227'o'
        DataBinding.FieldName = 'iNrCartao'
        Visible = False
        Width = 100
      end
      object cxGridDBTableView1iNrDocto: TcxGridDBColumn
        Caption = 'N'#250'm. Docto'
        DataBinding.FieldName = 'iNrDocto'
        Width = 94
      end
      object cxGridDBTableView1nCdOperadoraCartao: TcxGridDBColumn
        DataBinding.FieldName = 'nCdOperadoraCartao'
        Visible = False
      end
      object cxGridDBTableView1cNmOperadoraCartao: TcxGridDBColumn
        Caption = 'Operadora'
        DataBinding.FieldName = 'cNmOperadoraCartao'
        Width = 200
      end
      object cxGridDBTableView1iParcelas: TcxGridDBColumn
        Caption = 'Parcelas'
        DataBinding.FieldName = 'iParcelas'
        Width = 67
      end
      object cxGridDBTableView1nValTransacao: TcxGridDBColumn
        Caption = 'Valor Bruto'
        DataBinding.FieldName = 'nValTransacao'
        Width = 94
      end
      object cxGridDBTableView1nValLiquido: TcxGridDBColumn
        Caption = 'Valor L'#237'quido'
        DataBinding.FieldName = 'nValLiquido'
        Width = 98
      end
      object cxGridDBTableView1nValTaxaOperadora: TcxGridDBColumn
        Caption = 'Valor Desconto'
        DataBinding.FieldName = 'nValTaxaOperadora'
        Width = 109
      end
      object cxGridDBTableView1nCdStatusDocto: TcxGridDBColumn
        DataBinding.FieldName = 'nCdStatusDocto'
        Visible = False
      end
      object cxGridDBTableView1cNmStatusDocto: TcxGridDBColumn
        Caption = 'Situa'#231#227'o do Comprovante'
        DataBinding.FieldName = 'cNmStatusDocto'
        Width = 279
      end
      object cxGridDBTableView1cFlgSelecionado: TcxGridDBColumn
        DataBinding.FieldName = 'cFlgSelecionado'
        Visible = False
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object StaticText1: TStaticText [5]
    Left = 0
    Top = 105
    Width = 1161
    Height = 17
    Align = alTop
    Alignment = taCenter
    BorderStyle = sbsSingle
    Caption = 'Comprovantes Aguardando Confirma'#231#227'o'
    TabOrder = 4
  end
  object StaticText2: TStaticText [6]
    Left = 0
    Top = 393
    Width = 1161
    Height = 17
    Align = alTop
    Alignment = taCenter
    BorderStyle = sbsSingle
    Caption = 'Comprovantes Selecionados'
    TabOrder = 5
  end
  inherited ImageList1: TImageList
    Left = 512
    Top = 240
  end
  object qryGrupoOperadora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoOperadoraCartao'
      '      ,cNmGrupoOperadoraCartao'
      '      ,cFlgDataCredFechPOS'
      '      ,cFlgTotalLiqFechPOS'
      '  FROM GrupoOperadoraCartao'
      'WHERE nCdGrupoOperadoraCartao = :nPK')
    Left = 384
    Top = 208
    object qryGrupoOperadoranCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryGrupoOperadoracNmGrupoOperadoraCartao: TStringField
      FieldName = 'cNmGrupoOperadoraCartao'
      Size = 50
    end
    object qryGrupoOperadoracFlgDataCredFechPOS: TIntegerField
      FieldName = 'cFlgDataCredFechPOS'
    end
    object qryGrupoOperadoracFlgTotalLiqFechPOS: TIntegerField
      FieldName = 'cFlgTotalLiqFechPOS'
    end
  end
  object dsGrupoOperadora: TDataSource
    DataSet = qryGrupoOperadora
    Left = 384
    Top = 240
  end
  object qryTransacaoCartao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'dDtTransacao'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdGrupoOperadoraCartao'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @dDtTransacao varchar(10)'
      ''
      'Set @dDtTransacao = :dDtTransacao'
      ''
      'INSERT  INTO #TempTransacaoCartao'
      'SELECT TransacaoCartao.nCdTransacaoCartao'
      '      ,TransacaoCartao.dDtTransacao'
      '      ,TransacaoCartao.iNrCartao'
      '      ,TransacaoCartao.iNrDocto'
      '      ,TransacaoCartao.nCdOperadoraCartao'
      '      ,OperadoraCartao.cNmOperadoraCartao'
      '      ,TransacaoCartao.iParcelas'
      '      ,nValTransacao'
      '      ,(nValTransacao - nValTaxaOperadora) nValLiquido'
      '      ,nValTaxaOperadora'
      '      ,StatusDocto.nCdStatusDocto'
      '      ,StatusDocto.cNmStatusDocto'
      '      ,0'
      '  FROM TransacaoCartao'
      
        '       INNER JOIN OperadoraCartao ON OperadoraCartao.nCdOperador' +
        'aCartao = TransacaoCartao.nCdOperadoraCartao'
      
        '       INNER JOIN StatusDocto     ON StatusDocto.nCdStatusDocto ' +
        '        = TransacaoCartao.nCdStatusDocto'
      ' WHERE TransacaoCartao.nCdLojaCartao           = :nCdLoja'
      
        '   AND TransacaoCartao.nCdGrupoOperadoraCartao = :nCdGrupoOperad' +
        'oraCartao'
      '   AND TransacaoCartao.cFlgPOSEncerrado        = 0'
      
        '   AND TransacaoCartao.dDtTransacao            < Convert(DATETIM' +
        'E,@dDtTransacao,103)+1'
      
        '      --AND TransacaoCartao.dDtTransacao           >= Convert(DA' +
        'TETIME,@dDtTransacao,103)'
      ' ORDER BY 1'
      '')
    Left = 480
    Top = 208
    object qryTransacaoCartaonCdTransacaoCartao: TAutoIncField
      FieldName = 'nCdTransacaoCartao'
      ReadOnly = True
    end
    object qryTransacaoCartaodDtTransacao: TDateTimeField
      FieldName = 'dDtTransacao'
    end
    object qryTransacaoCartaoiNrCartao: TLargeintField
      FieldName = 'iNrCartao'
    end
    object qryTransacaoCartaoiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryTransacaoCartaonCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryTransacaoCartaocNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryTransacaoCartaoiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryTransacaoCartaonValTransacao: TBCDField
      FieldName = 'nValTransacao'
      Precision = 12
      Size = 2
    end
    object qryTransacaoCartaonValLiquido: TBCDField
      FieldName = 'nValLiquido'
      ReadOnly = True
      Precision = 13
      Size = 2
    end
    object qryTransacaoCartaonValTaxaOperadora: TBCDField
      FieldName = 'nValTaxaOperadora'
      Precision = 12
      Size = 2
    end
    object qryTransacaoCartaonCdStatusDocto: TIntegerField
      FieldName = 'nCdStatusDocto'
    end
    object qryTransacaoCartaocNmStatusDocto: TStringField
      FieldName = 'cNmStatusDocto'
      Size = 50
    end
  end
  object cmdPreparaTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#TempTransacaoCartao'#39') IS NULL)'#13#10'BEGIN'#13#10#13 +
      #10'SELECT TOP 0 TransacaoCartao.nCdTransacaoCartao'#13#10'      ,Transac' +
      'aoCartao.dDtTransacao'#13#10'      ,TransacaoCartao.iNrCartao'#13#10'      ,' +
      'TransacaoCartao.iNrDocto'#13#10'      ,TransacaoCartao.nCdOperadoraCar' +
      'tao'#13#10'      ,OperadoraCartao.cNmOperadoraCartao'#13#10'      ,Transacao' +
      'Cartao.iParcelas'#13#10'      ,nValTransacao'#13#10'      ,(nValTransacao - ' +
      'nValTaxaOperadora) nValLiquido'#13#10'      ,nValTaxaOperadora'#13#10'      ' +
      ',StatusDocto.nCdStatusDocto'#13#10'      ,StatusDocto.cNmStatusDocto'#13#10 +
      ',0 cFlgSelecionado'#13#10'  INTO #TempTransacaoCartao'#13#10'  FROM Transaca' +
      'oCartao'#13#10'       INNER JOIN OperadoraCartao ON OperadoraCartao.nC' +
      'dOperadoraCartao = TransacaoCartao.nCdOperadoraCartao'#13#10'       IN' +
      'NER JOIN StatusDocto     ON StatusDocto.nCdStatusDocto         =' +
      ' TransacaoCartao.nCdStatusDocto'#13#10#13#10'END'#13#10#13#10'TRUNCATE TABLE #TempTr' +
      'ansacaoCartao'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 512
    Top = 208
  end
  object qryTransacaoPendente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempTransacaoCartao'#39') IS NULL)'
      'BEGIN'
      ''
      'SELECT TOP 0 TransacaoCartao.nCdTransacaoCartao'
      '      ,TransacaoCartao.dDtTransacao'
      '      ,TransacaoCartao.iNrCartao'
      '      ,TransacaoCartao.iNrDocto'
      '      ,TransacaoCartao.nCdOperadoraCartao'
      '      ,OperadoraCartao.cNmOperadoraCartao'
      '      ,TransacaoCartao.iParcelas'
      '      ,nValTransacao'
      '      ,(nValTransacao - nValTaxaOperadora) nValLiquido'
      '      ,nValTaxaOperadora'
      '      ,StatusDocto.nCdStatusDocto'
      '      ,StatusDocto.cNmStatusDocto'
      '      ,0 cFlgSelecionado'
      '  INTO #TempTransacaoCartao'
      '  FROM TransacaoCartao'
      
        '       INNER JOIN OperadoraCartao ON OperadoraCartao.nCdOperador' +
        'aCartao = TransacaoCartao.nCdOperadoraCartao'
      
        '       INNER JOIN StatusDocto     ON StatusDocto.nCdStatusDocto ' +
        '        = TransacaoCartao.nCdStatusDocto'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #TempTransacaoCartao'
      ' WHERE cFlgSelecionado = 0'
      ' ORDER BY dDtTransacao'
      '         ,iNrDocto')
    Left = 416
    Top = 208
    object qryTransacaoPendentenCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTransacaoPendentedDtTransacao: TDateTimeField
      FieldName = 'dDtTransacao'
    end
    object qryTransacaoPendenteiNrCartao: TLargeintField
      FieldName = 'iNrCartao'
    end
    object qryTransacaoPendenteiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryTransacaoPendentenCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryTransacaoPendentecNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryTransacaoPendenteiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryTransacaoPendentenValTransacao: TBCDField
      FieldName = 'nValTransacao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTransacaoPendentenValLiquido: TBCDField
      FieldName = 'nValLiquido'
      DisplayFormat = '#,##0.00'
      Precision = 13
      Size = 2
    end
    object qryTransacaoPendentenValTaxaOperadora: TBCDField
      FieldName = 'nValTaxaOperadora'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTransacaoPendentenCdStatusDocto: TIntegerField
      FieldName = 'nCdStatusDocto'
    end
    object qryTransacaoPendentecNmStatusDocto: TStringField
      FieldName = 'cNmStatusDocto'
      Size = 50
    end
    object qryTransacaoPendentecFlgSelecionado: TIntegerField
      FieldName = 'cFlgSelecionado'
    end
  end
  object qryTransacaoSelecionada: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempTransacaoCartao'#39') IS NULL)'
      'BEGIN'
      ''
      'SELECT TOP 0 TransacaoCartao.nCdTransacaoCartao'
      '      ,TransacaoCartao.dDtTransacao'
      '      ,TransacaoCartao.iNrCartao'
      '      ,TransacaoCartao.iNrDocto'
      '      ,TransacaoCartao.nCdOperadoraCartao'
      '      ,OperadoraCartao.cNmOperadoraCartao'
      '      ,TransacaoCartao.iParcelas'
      '      ,nValTransacao'
      '      ,(nValTransacao - nValTaxaOperadora) nValLiquido'
      '      ,nValTaxaOperadora'
      '      ,StatusDocto.nCdStatusDocto'
      '      ,StatusDocto.cNmStatusDocto'
      '      ,0 cFlgSelecionado'
      '  INTO #TempTransacaoCartao'
      '  FROM TransacaoCartao'
      
        '       INNER JOIN OperadoraCartao ON OperadoraCartao.nCdOperador' +
        'aCartao = TransacaoCartao.nCdOperadoraCartao'
      
        '       INNER JOIN StatusDocto     ON StatusDocto.nCdStatusDocto ' +
        '        = TransacaoCartao.nCdStatusDocto'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #TempTransacaoCartao'
      ' WHERE cFlgSelecionado = 1'
      ' ORDER BY dDtTransacao'
      '         ,iNrDocto')
    Left = 448
    Top = 208
    object qryTransacaoSelecionadanCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTransacaoSelecionadadDtTransacao: TDateTimeField
      FieldName = 'dDtTransacao'
    end
    object qryTransacaoSelecionadaiNrCartao: TLargeintField
      FieldName = 'iNrCartao'
    end
    object qryTransacaoSelecionadaiNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryTransacaoSelecionadanCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryTransacaoSelecionadacNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryTransacaoSelecionadaiParcelas: TIntegerField
      FieldName = 'iParcelas'
    end
    object qryTransacaoSelecionadanValTransacao: TBCDField
      FieldName = 'nValTransacao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTransacaoSelecionadanValLiquido: TBCDField
      FieldName = 'nValLiquido'
      DisplayFormat = '#,##0.00'
      Precision = 13
      Size = 2
    end
    object qryTransacaoSelecionadanValTaxaOperadora: TBCDField
      FieldName = 'nValTaxaOperadora'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTransacaoSelecionadanCdStatusDocto: TIntegerField
      FieldName = 'nCdStatusDocto'
    end
    object qryTransacaoSelecionadacNmStatusDocto: TStringField
      FieldName = 'cNmStatusDocto'
      Size = 50
    end
    object qryTransacaoSelecionadacFlgSelecionado: TIntegerField
      FieldName = 'cFlgSelecionado'
    end
  end
  object dsTransacaoPendente: TDataSource
    DataSet = qryTransacaoPendente
    Left = 416
    Top = 240
  end
  object dsTransacaoSelecionada: TDataSource
    DataSet = qryTransacaoSelecionada
    Left = 448
    Top = 240
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 480
    Top = 240
  end
end
