inherited frmConsultaVendaProduto: TfrmConsultaVendaProduto
  Left = 88
  Top = 78
  Width = 1133
  Height = 608
  Caption = 'Consulta de Venda por Produto'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 193
    Width = 1117
    Height = 379
  end
  inherited ToolBar1: TToolBar
    Width = 1117
    ButtonWidth = 73
    inherited ToolButton1: TToolButton
      Caption = 'Consultar'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 73
    end
    inherited ToolButton2: TToolButton
      Left = 81
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1117
    Height = 164
    Align = alTop
    Caption = 'Filtro'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 53
      Top = 24
      Width = 38
      Height = 13
      Caption = 'Produto'
    end
    object Label2: TLabel
      Tag = 1
      Left = 71
      Top = 48
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
    end
    object Label3: TLabel
      Tag = 1
      Left = 31
      Top = 120
      Width = 60
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Pedido'
    end
    object Label4: TLabel
      Tag = 1
      Left = 215
      Top = 120
      Width = 117
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Nota/Cupom Fiscal'
    end
    object Label6: TLabel
      Tag = 1
      Left = 596
      Top = 120
      Width = 16
      Height = 13
      Alignment = taRightJustify
      Caption = 'at'#233
    end
    object Label5: TLabel
      Tag = 1
      Left = 420
      Top = 120
      Width = 84
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo de Venda'
    end
    object Label7: TLabel
      Tag = 1
      Left = 58
      Top = 72
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cliente'
    end
    object Label8: TLabel
      Tag = 1
      Left = 7
      Top = 96
      Width = 84
      Height = 13
      Caption = 'CPF/CNPJ Cliente'
    end
    object Label9: TLabel
      Tag = 1
      Left = 30
      Top = 144
      Width = 61
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'm. Carnet'
    end
    object edtEANProduto: TEdit
      Left = 96
      Top = 16
      Width = 97
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 20
      TabOrder = 0
      OnExit = edtEANProdutoExit
      OnKeyDown = edtEANProdutoKeyDown
    end
    object edtLoja: TMaskEdit
      Left = 96
      Top = 40
      Width = 65
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 2
      Text = '      '
      OnExit = edtLojaExit
      OnKeyDown = edtLojaKeyDown
    end
    object edtNumPedido: TMaskEdit
      Left = 96
      Top = 112
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 7
      Text = '         '
    end
    object edtNumCupom: TMaskEdit
      Left = 338
      Top = 112
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 8
      Text = '         '
    end
    object edtDtFinal: TMaskEdit
      Left = 616
      Top = 112
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 10
      Text = '  /  /    '
    end
    object edtDtInicial: TMaskEdit
      Left = 512
      Top = 112
      Width = 73
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 9
      Text = '  /  /    '
    end
    object edtCliente: TMaskEdit
      Left = 96
      Top = 64
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 4
      Text = '         '
      OnExit = edtClienteExit
      OnKeyDown = edtClienteKeyDown
    end
    object edtCPF: TEdit
      Left = 96
      Top = 88
      Width = 97
      Height = 21
      CharCase = ecUpperCase
      MaxLength = 20
      TabOrder = 6
    end
    object edtNumCarnet: TMaskEdit
      Left = 96
      Top = 136
      Width = 95
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 11
      Text = '         '
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 200
      Top = 16
      Width = 490
      Height = 21
      DataField = 'cNmProduto'
      DataSource = DataSource1
      TabOrder = 1
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 200
      Top = 64
      Width = 489
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = DataSource2
      TabOrder = 5
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 200
      Top = 40
      Width = 489
      Height = 21
      DataField = 'cNmLoja'
      DataSource = DataSource3
      TabOrder = 3
    end
  end
  object cxGrid1: TcxGrid [3]
    Left = 0
    Top = 193
    Width = 1117
    Height = 379
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    LookAndFeel.NativeStyle = True
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      DataController.DataSource = dsResultado
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTotal'
          Column = cxGrid1DBTableView1nValTotal
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnGrouping = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1nCdPedido: TcxGridDBColumn
        Caption = 'Pedido'
        DataBinding.FieldName = 'nCdPedido'
        Width = 63
      end
      object cxGrid1DBTableView1dDtPedido: TcxGridDBColumn
        Caption = 'Dt. Pedido'
        DataBinding.FieldName = 'dDtPedido'
        Width = 135
      end
      object cxGrid1DBTableView1nCdUsuario: TcxGridDBColumn
        Caption = 'C'#243'd. Vendedor'
        DataBinding.FieldName = 'nCdUsuario'
        Width = 97
      end
      object cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn
        Caption = 'Vendedor'
        DataBinding.FieldName = 'cNmUsuario'
        Width = 124
      end
      object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Cliente'
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 115
      end
      object cxGrid1DBTableView1cNmLoja: TcxGridDBColumn
        Caption = 'Loja'
        DataBinding.FieldName = 'cNmLoja'
        Width = 115
      end
      object cxGrid1DBTableView1cNmTipoPedido: TcxGridDBColumn
        Caption = 'Tipo de Pedido'
        DataBinding.FieldName = 'cNmTipoPedido'
        Width = 51
      end
      object cxGrid1DBTableView1cNmProduto: TcxGridDBColumn
        Caption = 'Produto'
        DataBinding.FieldName = 'cNmProduto'
        Width = 203
      end
      object cxGrid1DBTableView1cReferencia: TcxGridDBColumn
        Caption = 'Refer'#234'ncia'
        DataBinding.FieldName = 'cReferencia'
      end
      object cxGrid1DBTableView1nQtdeExpRec: TcxGridDBColumn
        Caption = 'Qt.'
        DataBinding.FieldName = 'nQtdeExpRec'
        Width = 55
      end
      object cxGrid1DBTableView1nValUnitario: TcxGridDBColumn
        Caption = 'Val. Unit'#225'rio'
        DataBinding.FieldName = 'nValUnitario'
        Width = 108
      end
      object cxGrid1DBTableView1nValDesconto: TcxGridDBColumn
        Caption = 'Val. Desconto'
        DataBinding.FieldName = 'nValDesconto'
        Width = 105
      end
      object cxGrid1DBTableView1nValAcrescimo: TcxGridDBColumn
        Caption = 'Val. Acr'#233'scimo'
        DataBinding.FieldName = 'nValAcrescimo'
        Width = 109
      end
      object cxGrid1DBTableView1nValTotal: TcxGridDBColumn
        Caption = 'Valor Total'
        DataBinding.FieldName = 'nValTotal'
        Width = 102
      end
      object cxGrid1DBTableView1nCdLanctoFin: TcxGridDBColumn
        DataBinding.FieldName = 'nCdLanctoFin'
        Visible = False
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C6948C00C6948C00C694
      8C00C6948C00C6948C00C6948C00000000000000000000000000C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00C6948C0000000000000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C00000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C006363630000000000C6948C00C6948C0000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C0063636300000000000000000000000000000000000000000000000000EFFF
      FF00EFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C0000000000EFFF
      FF00EFFFFF000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C00C6948C00C6948C00C6948C00C6948C00C6948C00000000000000
      0000000000000000000000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF0000008400000000000000000000000000EFFFFF00C6948C00C694
      8C00C6948C000000000000000000000000000000000000000000C6948C00C694
      8C00C6948C006363630000000000C6948C000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C00636363000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000EFFFFF00C694
      8C006363630000000000C6948C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF81C00000FE00FFFF01800000
      FE00C00300000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010181000000018001818100000003800181810000
      0077C00381810000007FFFFF8383000000000000000000000000000000000000
      000000000000}
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @cCodigo    varchar(20)'
      '       ,@nCdProduto int'
      ''
      'Set @cCodigo = :nPK'
      ''
      'BEGIN TRY'
      '    SELECT TOP 1 @nCdProduto = nCdProduto'
      '      FROM Produto'
      '     WHERE nCdProduto  = @cCodigo'
      '        OR cEANFornec  = @cCodigo'
      '        OR cEAN = @cCodigo'
      'END TRY'
      'BEGIN CATCH'
      '    SELECT TOP 1 @nCdProduto = nCdProduto'
      '      FROM Produto'
      '     WHERE cEANFornec  = @cCodigo'
      '        OR cEAN = @cCodigo'
      'END CATCH'
      ''
      'SELECT nCdProduto, cNmProduto'
      '  FROM Produto'
      ' WHERE nCdProduto = @nCdProduto'
      '')
    Left = 304
    Top = 85
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object DataSource1: TDataSource
    DataSet = qryProduto
    Left = 384
    Top = 240
  end
  object qryCliente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro, cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 456
    Top = 77
    object qryClientenCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryClientecNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryCliente
    Left = 392
    Top = 248
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 344
    Top = 117
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qryLoja
    Left = 400
    Top = 256
  end
  object SP_CONSULTA_VENDA_PRODUTO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CONSULTA_VENDA_PRODUTO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@nCdCliente'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@cCNPJCPF'
        Attributes = [paNullable]
        DataType = ftString
        Size = 14
        Value = Null
      end
      item
        Name = '@nCdPedido'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@iNrDoctoFiscal'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@dDtInicial'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@dDtFinal'
        Attributes = [paNullable]
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = '@nCdCrediario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end
      item
        Name = '@iNrCartao'
        Attributes = [paNullable]
        DataType = ftString
        Size = 16
        Value = Null
      end>
    Left = 272
    Top = 256
    object SP_CONSULTA_VENDA_PRODUTOnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object SP_CONSULTA_VENDA_PRODUTOdDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object SP_CONSULTA_VENDA_PRODUTOcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object SP_CONSULTA_VENDA_PRODUTOcNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object SP_CONSULTA_VENDA_PRODUTOcNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object SP_CONSULTA_VENDA_PRODUTOnQtdeExpRec: TBCDField
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object SP_CONSULTA_VENDA_PRODUTOnValUnitario: TBCDField
      FieldName = 'nValUnitario'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object SP_CONSULTA_VENDA_PRODUTOnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object SP_CONSULTA_VENDA_PRODUTOnValAcrescimo: TBCDField
      FieldName = 'nValAcrescimo'
      DisplayFormat = '#,##0.00'
      Precision = 12
    end
    object SP_CONSULTA_VENDA_PRODUTOnValTotal: TBCDField
      FieldName = 'nValTotal'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 27
      Size = 8
    end
    object SP_CONSULTA_VENDA_PRODUTOcNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object SP_CONSULTA_VENDA_PRODUTOcReferencia: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object SP_CONSULTA_VENDA_PRODUTOnCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object SP_CONSULTA_VENDA_PRODUTOnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object SP_CONSULTA_VENDA_PRODUTOcNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsResultado: TDataSource
    DataSet = SP_CONSULTA_VENDA_PRODUTO
    Left = 312
    Top = 256
  end
end
