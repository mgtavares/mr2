unit rParcelaCrediarioAberta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls;

type
  TrptParcelaCrediarioAberta = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    dsTerceiro: TDataSource;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit6: TMaskEdit;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    MaskEdit4: TMaskEdit;
    DBEdit4: TDBEdit;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    Image2: TImage;
    dsLoja: TDataSource;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    MaskEdit3: TMaskEdit;
    MaskEdit8: TMaskEdit;
    Label4: TLabel;
    MaskEdit9: TMaskEdit;
    Label5: TLabel;
    RadioGroup3: TRadioGroup;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocRG: TStringField;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptParcelaCrediarioAberta: TrptParcelaCrediarioAberta;

implementation

uses fMenu, rImpSP, fLookup_Padrao,rParcelaCrediarioAberta_view;

{$R *.dfm}

procedure TrptParcelaCrediarioAberta.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      MaskEdit4.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLoja, MaskEdit4.text) ;
  end
  else
  begin
      MasKEdit4.ReadOnly := True ;
      MasKEdit4.Color    := $00E9E4E4 ;
  end ;

end;


procedure TrptParcelaCrediarioAberta.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TrptParcelaCrediarioAberta.ToolButton1Click(Sender: TObject);
var
  objRel : TrptParcelaCrediarioAberta_view ;
begin

  objRel := TrptParcelaCrediarioAberta_view.Create(Self) ;

  objRel.usp_Relatorio.Close ;

  {--posiciona qryEmpresa para verificar se a empresa digitada � valida
   --e tem acesso permitido para o usu�rio logado--}

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Open;

  if qryEmpresa.Eof then
      MaskEdit3.Text := '';

  {--posiciona qryLoja para verificar se a loja digitada � valida
   --e tem acesso permitido para o usu�rio logado(quando em Modo Varejo)--}

  qryLoja.Close ;

  if (Trim(MaskEdit4.Text) <> '') then
  begin

      qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryLoja.Parameters.ParamByName('nPK').Value := MaskEdit4.Text ;
      qryLoja.Open ;

      if qryLoja.Eof then
          MaskEdit4.Text := '';

  end;

  {--aqui passa os valores para os parametros de entrada da procedure--}

  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value      := frmMenu.ConvInteiro(MaskEdit3.Text);
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdLojaEmissora').Value := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdUsuario').Value      := frmMenu.nCdUsuarioLogado ;

  if (Trim(MaskEdit6.Text) <> '') then
      objRel.usp_Relatorio.Parameters.ParamByName('@nCdCliente').Value  := frmMenu.ConvInteiro(MaskEdit6.Text)
  else objRel.usp_Relatorio.Parameters.ParamByName('@nCdCliente').Value := 0;

  if (RadioGroup1.ItemIndex = 0) then
  begin

      MaskEdit9.Text := '';
      MaskEdit8.Text := '';

      objRel.usp_Relatorio.Parameters.ParamByName('@cFlgSomnteAtraso').Value := '1'

  end
  else objRel.usp_Relatorio.Parameters.ParamByName('@cFlgSomnteAtraso').Value := '0' ;

  {-- verifica a data e preenche(quando vazio) para listar tudo --}

  objRel.usp_Relatorio.Parameters.ParamByName('@dDtEmissaoInicial').Value := frmMenu.ConvData(MaskEdit1.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtEmissaoFinal').Value   := frmMenu.ConvData(MaskEdit2.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtVenctoInicial').Value  := frmMenu.ConvData(MaskEdit9.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtVenctoFinal').Value    := frmMenu.ConvData(MaskEdit8.Text) ;

  {-- faz verifica��o das flags --}

  if (RadioGroup2.ItemIndex = 0) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cFlgCalculaJuros').Value := 1
  else objRel.usp_Relatorio.Parameters.ParamByName('@cFlgCalculaJuros').Value := 0 ;

  if (RadioGroup3.ItemIndex = 0) then
      objRel.usp_Relatorio.Parameters.ParamByName('@cFlgSomnteReneg').Value := 1
  else objRel.usp_Relatorio.Parameters.ParamByName('@cFlgSomnteReneg').Value := 0 ;

  {-- abre a procedure para exibi��o dos dados no relat�rio --}

  objRel.usp_Relatorio.Open ;

  {-- aqui preenche os filtros do relat�rio--}

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  objRel.lblFiltro1.Caption := '';

  if (Trim(DBedit3.Text) <> '') then
      objRel.lblFiltro1.Caption := 'Empresa: ' + Trim(MaskEdit3.Text) + '-' + DbEdit3.Text ;

  if (Trim(MaskEdit4.Text) <> '')then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption +  ' / Loja : ' + trim(MaskEdit4.Text) + '-' + DbEdit4.Text ;

  if (Trim(DBedit10.Text) <> '') then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Cliente: ' + trim(MaskEdit6.Text) + '-' + DbEdit10.Text ;

  if (((Trim(MaskEdit1.Text)) <> '/  /') or ((Trim(MaskEdit2.Text)) <> '/  /')) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo Emiss�o: ' + trim(MaskEdit1.Text) + '-' + trim(MaskEdit2.Text) ;

  if (((Trim(MaskEdit9.Text)) <> '/  /') or ((Trim(MaskEdit8.Text)) <> '/  /')) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Per�odo Vencimento: ' + trim(MaskEdit9.Text) + '-' + trim(MaskEdit8.Text) ;

  if (RadioGroup1.ItemIndex = 0) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Somente parcelas em atraso: Sim'
  else objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Somente parcelas em atraso: N�o' ;

  if (RadioGroup2.ItemIndex = 0) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Calcular juros: Sim'
  else objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Calcular juros: N�o' ;

  if (RadioGroup3.ItemIndex = 0) then
      objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Somente parcelas em Reneg.: Sim'
  else objRel.lblFiltro1.Caption := objRel.lblFiltro1.Caption + ' / Somente parcelas em Reneg.: N�o' ;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;


procedure TrptParcelaCrediarioAberta.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(180);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptParcelaCrediarioAberta.MaskEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptParcelaCrediarioAberta.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, MaskEdit4.Text) ;

end;

procedure TrptParcelaCrediarioAberta.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
  var
   nPk : integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptParcelaCrediarioAberta.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

initialization
     RegisterClass(TrptParcelaCrediarioAberta) ;

end.
