unit fContaCaixaCofre;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  DBGridEhGrouping, ToolCtrlsEh, ER2Lookup;

type
  TfrmContaCaixaCofre = class(TfrmCadastro_Padrao)
    qryMasternCdContaBancaria: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdBanco: TIntegerField;
    qryMastercAgencia: TIntegerField;
    qryMasternCdConta: TStringField;
    qryMasternCdCC: TIntegerField;
    qryMastercFlgFluxo: TIntegerField;
    qryMasteriUltimoCheque: TIntegerField;
    qryMasteriUltimoBordero: TIntegerField;
    qryMasternValLimiteCredito: TBCDField;
    qryMastercFlgDeposito: TIntegerField;
    qryMastercFlgEmiteCheque: TIntegerField;
    qryMastercFlgCaixa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdUsuarioOperador: TIntegerField;
    qryMasternSaldoConta: TBCDField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    DBEdit6: TDBEdit;
    dsLoja: TDataSource;
    DBEdit7: TDBEdit;
    dsUsuario: TDataSource;
    qryMastercFlgCofre: TIntegerField;
    qryMasterdDtUltConciliacao: TDateTimeField;
    qryMastercFlgEmiteBoleto: TIntegerField;
    qryMastercNmTitular: TStringField;
    qryMastercFlgProvTit: TIntegerField;
    qryUsuariosContaCofre: TADOQuery;
    dsUsuariosContaCofre: TDataSource;
    qryUsuariosContaCofrenCdUsuarioContaBancaria: TAutoIncField;
    qryUsuariosContaCofrenCdContaBancaria: TIntegerField;
    qryUsuariosContaCofrenCdUsuario: TIntegerField;
    qryUsuariosContaCofrenCdServidorOrigem: TIntegerField;
    qryUsuariosContaCofredDtReplicacao: TDateTimeField;
    cxPageControl1: TcxPageControl;
    TabUsuarios: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarios: TADOQuery;
    qryUsuariosnCdUsuario: TIntegerField;
    qryUsuarioscNmUsuario: TStringField;
    qryUsuariosContaCofrecNmUsuario: TStringField;
    Label8: TLabel;
    ER2LkpStatus: TER2LookupDBEdit;
    DBEdit8: TDBEdit;
    qryStatus: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    qryMasternCdStatus: TIntegerField;
    dsStatus: TDataSource;
    qryMasternSaldoInicial: TBCDField;
    qryMasterdDtSaldoInicial: TDateTimeField;
    qryMastercFlgReapresentaCheque: TIntegerField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMasteriUltimoBoleto: TIntegerField;
    qryMastercPrimeiraInstrucaoBoleto: TStringField;
    qryMastercSegundaInstrucaoBoleto: TStringField;
    qryMastercCedenteAgencia: TStringField;
    qryMastercCedenteAgenciaDigito: TStringField;
    qryMastercCedenteConta: TStringField;
    qryMastercCedenteContaDigito: TStringField;
    qryMastercCedenteCarteira: TStringField;
    qryMastercCedenteCNPJCPF: TStringField;
    qryMastercCedenteNome: TStringField;
    qryMastercCedenteCodCedente: TStringField;
    qryMasternCdTabTipoModeloImpBoleto: TIntegerField;
    qryMasternCdTabTipoRespEmissaoBoleto: TIntegerField;
    qryMasternCdTabTipoLayOutRemessa: TIntegerField;
    qryMasteriSeqNossoNumero: TIntegerField;
    qryMasteriSeqArquivoRemessa: TIntegerField;
    qryMasteriDiasProtesto: TIntegerField;
    qryMastercFlgAceite: TIntegerField;
    qryMastercDiretorioArquivoRemessa: TStringField;
    qryMastercDiretorioArquivoLicenca: TStringField;
    qryMasteriPrioridade: TIntegerField;
    qryMastercFlgBloqDinheiroRemessa: TIntegerField;
    gbOpcao: TGroupBox;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox4: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryUsuariosContaCofreBeforePost(DataSet: TDataSet);
    procedure DBCheckBox1Click(Sender: TObject);
    procedure qryUsuariosContaCofreCalcFields(DataSet: TDataSet);
    procedure DBCheckBox1Exit(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmContaCaixaCofre: TfrmContaCaixaCofre;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmContaCaixaCofre.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'CONTABANCARIA' ;
  nCdTabelaSistema  := 22 ;
  nCdConsultaPadrao := 131 ;

end;

procedure TfrmContaCaixaCofre.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, DbEdit3.Text) ;

end;

procedure TfrmContaCaixaCofre.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryUsuario.Close ;
  PosicionaQuery(qryUsuario, DBEdit4.Text) ;

end;

procedure TfrmContaCaixaCofre.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value   := frmMenu.nCdEmpresaAtiva ;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      DBEDit3.Enabled := False ;

  cxPageControl1.Visible := False;

end;

procedure TfrmContaCaixaCofre.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryStatus,IntToStr(qryMasternCdStatus.Value));

  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;

  qryUsuario.Close ;
  qryLoja.Close ;

  PosicionaQuery(qryUsuario, qryMasternCdUsuarioOperador.AsString) ;
  PosicionaQuery(qryLoja, qryMasternCdLoja.asString) ;
  PosicionaQuery(qryUsuariosContaCofre, qryMasternCdContaBancaria.asString) ;

  if qryMastercFlgCofre.Value = 0 then cxPageControl1.Visible := False;
  if qryMastercFlgCofre.Value = 1 then cxPageControl1.Visible := True;

end;

procedure TfrmContaCaixaCofre.btCancelarClick(Sender: TObject);
begin
  inherited;

  qryUsuario.Close ;
  qryLoja.Close ;

end;

procedure TfrmContaCaixaCofre.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmContaCaixaCofre.qryMasterBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (DbEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o da conta.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if ((DbEdit3.Text = '') or (Dbedit6.Text = '')) and (DBEdit3.Enabled) then
  begin
      MensagemAlerta('Seleciona a loja vinculada.') ;
      DbEdit3.SetFocus;
      abort ;
  end ;

  qryMastercFlgCaixa.Value  := 1 ;
  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmContaCaixaCofre.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(86,'Loja.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva));

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmContaCaixaCofre.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                 qryMasternCdUsuarioOperador.Value  := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmContaCaixaCofre.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuariosContaCofre.State = dsBrowse) then
             qryUsuariosContaCofre.Edit ;

        if (qryUsuariosContaCofre.State = dsInsert) or (qryUsuariosContaCofre.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuariosContaCofrenCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmContaCaixaCofre.qryUsuariosContaCofreBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryMastercFlgCofre.Value := 1;
  qryMaster.Post ;

  qryUsuariosContaCofrenCdContaBancaria.Value := qryMaster.FieldList[0].Value ;
  inherited;

  if (qryUsuariosContaCofre.State = dsInsert) then
      qryUsuariosContaCofrenCdUsuarioContaBancaria.Value := frmMenu.fnProximoCodigo('USUARIOCONTABANCARIA') ;

end;

procedure TfrmContaCaixaCofre.DBCheckBox1Click(Sender: TObject);
begin
  inherited;
  if DBCheckBox1.Checked then cxPageControl1.Visible := True
  else cxPageControl1.Visible := False;
end;

procedure TfrmContaCaixaCofre.qryUsuariosContaCofreCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  If (qryUsuariosContaCofrenCdUsuario.Value > 0) then
  begin
      PosicionaQuery(qryUsuarios, qryUsuariosContaCofrenCdUsuario.asString) ;

      if not qryUsuarios.eof then
          qryUsuariosContaCofrecNmUsuario.Value := qryUsuarioscNmUsuario.Value ;
  end ;

end;

procedure TfrmContaCaixaCofre.DBCheckBox1Exit(Sender: TObject);
begin
  inherited;
  if DBCheckBox1.Checked then
  begin
     cxPageControl1.Visible := True;
  end;
end;

procedure TfrmContaCaixaCofre.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  if qryMastercFlgCofre.Value = 0 then
  begin
     PosicionaQuery(qryUsuariosContaCofre, qryMasternCdContaBancaria.asString) ;
     while not qryUsuariosContaCofre.eof do
     begin
        qryUsuariosContaCofre.Delete;
     end;
     qryUsuariosContaCofre.Next;
  end;
end;

procedure TfrmContaCaixaCofre.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryStatus.Close;
end;

initialization
    RegisterClass(TfrmContaCaixaCofre) ;

end.
