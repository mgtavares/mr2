inherited frmGrupoEconomico: TfrmGrupoEconomico
  Caption = 'Grupo Econ'#244'mico'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 24
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 29
    Top = 62
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo'
    FocusControl = DBEdit2
  end
  object DBEdit1: TDBEdit [4]
    Tag = 1
    Left = 64
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdGrupoEconomico'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [5]
    Left = 64
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmGrupoEconomico'
    DataSource = dsMaster
    TabOrder = 2
  end
  inherited qryMaster: TADOQuery
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GRUPOECONOMICO'
      'WHERE nCdGrupoEconomico = :nPK')
    object qryMasternCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryMastercNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
end
