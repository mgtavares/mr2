unit rBlocoUnidades;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBCtrls, StdCtrls, Mask, DB, ADODB;

type
  TrptBlocoUnidades = class(TfrmRelatorio_Padrao)
    Label1: TLabel;
    edtCdEmpresa: TMaskEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    edtCdBloco: TMaskEdit;
    DBEdit7: TDBEdit;
    DBEdit6: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryBlocoTorre: TADOQuery;
    qryBlocoTorreEmpreendimentosCd: TAutoIncField;
    qryBlocoTorreEmpreendimentosEmpreendimento: TStringField;
    qryBlocoTorreEmpreendimentosBlocoTorre: TStringField;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    qryBlocoTorreiQtdeUnidade: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure edtCdEmpresaExit(Sender: TObject);
    procedure edtCdBlocoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCdBlocoExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtCdEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptBlocoUnidades: TrptBlocoUnidades;

implementation

uses fMenu, fLookup_Padrao, rBlocoUnidadesView;

{$R *.dfm}

procedure TrptBlocoUnidades.FormShow(Sender: TObject);
begin
  inherited;

  edtCdEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

end;

procedure TrptBlocoUnidades.edtCdEmpresaExit(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;

  if (Trim(edtCdEmpresa.Text) <> '') then
  begin
      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro(edtCdEmpresa.Text) ;
      qryEmpresa.Open ;
  end ;

end;

procedure TrptBlocoUnidades.edtCdBlocoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBEdit3.Text = '') then
        begin
            MensagemAlerta('Selecione uma empresa.') ;
            edtCdEmpresa.SetFocus;
            abort ;
        end ;

        nPK := frmLookup_Padrao.ExecutaConsulta2(187,'nCdEmpresa = ' + edtCdEmpresa.Text);

        If (nPK > 0) then
        begin
            edtCdBloco.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptBlocoUnidades.edtCdBlocoExit(Sender: TObject);
begin
  inherited;
  qryBlocoTorre.Close ;
  qryBlocoTorre.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro(edtCdEmpresa.Text) ;

  PosicionaQuery( qryBlocoTorre ,edtCdBloco.Text) ;

end;

procedure TrptBlocoUnidades.ToolButton1Click(Sender: TObject);
var
  objRel : TrptBlocoUnidadesView ;
begin
  inherited;

  if (DBEdit3.Text = '') then
  begin
      MensagemAlerta('Selecione a empresa.') ;
      edtCdEmpresa.Setfocus;
      abort ;
  end ;

  if (DBEdit6.Text = '') then
  begin
      MensagemAlerta('Selecione o bloco/torre.') ;
      edtCdBloco.Setfocus;
      abort ;
  end ;

  objRel := TrptBlocoUnidadesView.Create( Self ) ;
  
  objRel.lblEmpresa.Caption := DBEdit3.Text;
  objRel.lblFiltro1.Caption := 'Bloco/Torre: ' + DBEdit6.Text + ' - ' + DBEdit7.Text;

  objRel.uspRelatorio.Close;
  objRel.uspRelatorio.Parameters.ParamByName('@nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
  objRel.uspRelatorio.Parameters.ParamByName('@nCdBloco').Value    := frmMenu.ConvInteiro(edtCdBloco.Text) ;
  objRel.uspRelatorio.Parameters.ParamByName('@iQtdeUnid').Value   := qryBlocoTorreiQtdeUnidade.Value ;
  objRel.uspRelatorio.Open;

  try

      try

         {--visualiza o relat�rio--}
          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TrptBlocoUnidades.edtCdEmpresaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            edtCdEmpresa.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

initialization
     RegisterClass(TrptBlocoUnidades) ;
end.
