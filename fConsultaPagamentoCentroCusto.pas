unit fConsultaPagamentoCentroCusto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, cxPC, StdCtrls, DBCtrls, Mask, ADODB;

type
  TfrmConsultaPagamentoCentroCusto = class(TfrmProcesso_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    dsEmpresa: TDataSource;
    qryCentroCustoNivel3: TADOQuery;
    qryCentroCustoNivel3nCdCC: TIntegerField;
    qryCentroCustoNivel3cCdCC: TStringField;
    qryCentroCustoNivel3cNmCC: TStringField;
    dsCentroCustoNivel3: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    dsTerceiro: TDataSource;
    qryCentroCustoNivel1: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    dsCentroCustoNivel1: TDataSource;
    qryCentroCustoNivel2: TADOQuery;
    IntegerField2: TIntegerField;
    StringField3: TStringField;
    StringField4: TStringField;
    dsCentroCustoNivel2: TDataSource;
    qryUnidadeNegocio: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    dsUnidadeNegocio: TDataSource;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    MaskEdit4: TMaskEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    MaskEdit8: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit5: TMaskEdit;
    DBEdit11: TDBEdit;
    MaskEdit7: TMaskEdit;
    DBEdit4: TDBEdit;
    DBEdit12: TDBEdit;
    MaskEdit3: TMaskEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    MaskEdit6: TMaskEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    Label4: TLabel;
    Label9: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit6KeyPress(Sender: TObject; var Key: Char);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaPagamentoCentroCusto: TfrmConsultaPagamentoCentroCusto;

implementation

uses fLookup_Padrao, fMenu,fConsultaPagamentoCentroCustoSint;

{$R *.dfm}
procedure TfrmConsultaPagamentoCentroCusto.MaskEdit4KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
  var
   nPk : integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit4.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaPagamentoCentroCusto.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit4.Text) <> '') then
  begin
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit4.Text ;
    qryEmpresa.Open ;
  end ;
  if qryEmpresa.Eof then MaskEdit4.Text := '';

end;

procedure TfrmConsultaPagamentoCentroCusto.MaskEdit8KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(59);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaPagamentoCentroCusto.MaskEdit8Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  PosicionaQuery(qryLoja, MaskEdit8.Text) ;
  if qryLoja.Eof then MaskEdit8.Text := '';
end;

procedure TfrmConsultaPagamentoCentroCusto.MaskEdit5KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(26);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryCentroCustoNivel1, MaskEdit5.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaPagamentoCentroCusto.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryCentroCustoNivel1.Close;
  PosicionaQuery(qryCentroCustoNivel1, MaskEdit5.Text) ;
  if qryCentroCustoNivel1.Eof then MaskEdit5.Clear;

end;

procedure TfrmConsultaPagamentoCentroCusto.MaskEdit7KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
  begin
  inherited;
  case key of
    vk_F4 : begin
        if (frmMenu.ConvInteiro(MaskEdit5.Text)) > 0 then
        begin
           nPK := frmLookup_Padrao.ExecutaConsulta2(27,'nCdCC1 = ' + MaskEdit5.Text);

           If (nPK > 0) then
           begin
               Maskedit7.Text := IntToStr(nPK) ;
               PosicionaQuery(qryCentroCustoNivel2, MaskEdit7.Text) ;
           end ;
        end;
    end ;
  end ;

end;

procedure TfrmConsultaPagamentoCentroCusto.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryCentroCustoNivel2.Close;
  qryCentroCustoNivel2.Parameters.ParamByName('nCdCC1').Value :=  frmMenu.ConvInteiro(MaskEdit5.Text) ;
  PosicionaQuery(qryCentroCustoNivel2, MaskEdit7.Text) ;
  if qryCentroCustoNivel2.Eof then MaskEdit7.Clear;
end;

procedure TfrmConsultaPagamentoCentroCusto.MaskEdit3KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin
        if (frmMenu.ConvInteiro(MaskEdit7.Text) > 0) then
        begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(29, ' nCdCC2 = ' + MaskEdit7.Text);

            If (nPK > 0) then
            begin
               Maskedit3.Text := IntToStr(nPK) ;
               PosicionaQuery(qryCentroCustoNivel3, MaskEdit3.Text) ;
            end ;
        end;
    end ;

  end ;


end;

procedure TfrmConsultaPagamentoCentroCusto.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryCentroCustoNivel3.Close ;
  qryCentroCustoNivel3.Parameters.ParamByName('nCdCC1').Value :=  frmMenu.ConvInteiro(MaskEdit5.Text) ;
  qryCentroCustoNivel3.Parameters.ParamByName('nCdCC2').Value :=  frmMenu.ConvInteiro(MaskEdit7.Text) ;
  PosicionaQuery(qryCentroCustoNivel3, MaskEdit3.Text) ;
  if qryCentroCustoNivel3.Eof then MaskEdit3.Clear;
end;

procedure TfrmConsultaPagamentoCentroCusto.MaskEdit6KeyPress(
  Sender: TObject; var Key: Char);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TfrmConsultaPagamentoCentroCusto.MaskEdit6KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;
   case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, MaskEdit6.Text) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaPagamentoCentroCusto.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, MaskEdit6.Text) ;
end;

procedure TfrmConsultaPagamentoCentroCusto.ToolButton1Click(
  Sender: TObject);
var
  objForm : TfrmConsultaPagamentoCentroCustoSint ;
begin
  inherited;

  objForm := TfrmConsultaPagamentoCentroCustoSint.Create(nil) ;

  qryCentroCustoNivel2.Close;
  qryCentroCustoNivel2.Parameters.ParamByName('nCdCC1').Value :=  frmMenu.ConvInteiro(MaskEdit5.Text) ;

  PosicionaQuery(qryCentroCustoNivel2, MaskEdit7.Text) ;

  if qryCentroCustoNivel2.Eof then
      MaskEdit7.Clear;

  qryCentroCustoNivel3.Close ;
  qryCentroCustoNivel3.Parameters.ParamByName('nCdCC1').Value :=  frmMenu.ConvInteiro(MaskEdit5.Text) ;
  qryCentroCustoNivel3.Parameters.ParamByName('nCdCC2').Value :=  frmMenu.ConvInteiro(MaskEdit7.Text) ;

  PosicionaQuery(qryCentroCustoNivel3, MaskEdit3.Text) ;

  if qryCentroCustoNivel3.Eof then
      MaskEdit3.Clear;

  objForm.uspConsulta.Close ;
  objForm.uspConsulta.Parameters.ParamByName('@nCdEmpresa').Value                  :=  frmMenu.ConvInteiro(MaskEdit4.Text);
  objForm.uspConsulta.Parameters.ParamByName('@nCdCentroCustoNivel1').Value        :=  frmMenu.ConvInteiro(MaskEdit5.Text);
  objForm.uspConsulta.Parameters.ParamByName('@nCdCentroCustoNivel2').Value        :=  frmMenu.ConvInteiro(MaskEdit7.Text);
  objForm.uspConsulta.Parameters.ParamByName('@nCdCentroCustoNivel3').Value        :=  frmMenu.ConvInteiro(MaskEdit3.Text);
  objForm.uspConsulta.Parameters.ParamByName('@nCdTerceiro').Value                 :=  frmMenu.ConvInteiro(MaskEdit6.Text);
  objForm.uspConsulta.Parameters.ParamByName('@dDtInicial').Value                  :=  frmMenu.ConvData(MaskEdit1.Text);
  objForm.uspConsulta.Parameters.ParamByName('@dDtFinal').Value                    :=  frmMenu.ConvData(MaskEdit2.Text);
  objForm.uspConsulta.Parameters.ParamByName('@nCdUsuario').Value                  :=  frmMenu.nCdUsuarioLogado;


  if (DBEdit1.Text = '') then MaskEdit8.Text :='';

  if (frmMenu.LeParametro('VAREJO') = 'N') then
     objForm.uspConsulta.Parameters.ParamByName('@nCdLoja').Value       := 0
  else objForm.uspConsulta.Parameters.ParamByName('@nCdLoja').Value     := frmMenu.ConvInteiro(MaskEdit8.Text) ;

  objForm.uspConsulta.Parameters.ParamByName('@nCdEmiteCCNivel2').Value :=  1;
  objForm.uspConsulta.Parameters.ParamByName('@nCdEmiteCCNivel3').Value :=  1;
  objForm.uspConsulta.Parameters.ParamByName('@nCdEmiteCCZerado').Value :=  1;

  objForm.uspConsulta.Open ;

  showForm( objForm , TRUE ) ;

end;

procedure TfrmConsultaPagamentoCentroCusto.FormShow(Sender: TObject);
var
iAux : Integer ;
begin
  inherited;
  iAux := frmMenu.UsuarioEmpresaAutorizada;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit4.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  MaskEdit4.SetFocus ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.nCdLojaAtiva > 0) then
  begin
      MaskEdit8.Text := IntToStr(frmMenu.nCdLojaAtiva) ;
      PosicionaQuery(qryLoja, MaskEdit8.text) ;
  end
  else
  begin
      MasKEdit8.ReadOnly := True ;
      MasKEdit8.Color    := $00E9E4E4 ;
  end ;
end;

initialization
    RegisterClass(TfrmConsultaPagamentoCentroCusto) ;

end.
