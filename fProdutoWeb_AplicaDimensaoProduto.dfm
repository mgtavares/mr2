inherited frmProdutoWeb_AplicaDimensaoProduto: TfrmProdutoWeb_AplicaDimensaoProduto
  Left = 553
  Top = 224
  VertScrollBar.Range = 0
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Aplica Dimens'#227'o Produto Web'
  ClientHeight = 191
  ClientWidth = 283
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 283
    Height = 162
  end
  object Label1: TLabel [1]
    Left = 23
    Top = 56
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Comprimento (m)'
  end
  object Label2: TLabel [2]
    Left = 57
    Top = 88
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Altura (m)'
  end
  object Label3: TLabel [3]
    Left = 49
    Top = 120
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Largura (m)'
  end
  object Label4: TLabel [4]
    Left = 22
    Top = 152
    Width = 83
    Height = 13
    Alignment = taRightJustify
    Caption = 'Peso L'#237'quido (KG)'
  end
  inherited ToolBar1: TToolBar
    Width = 283
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object dxCurComprimento: TdxCurrencyEdit [6]
    Left = 110
    Top = 48
    Width = 147
    TabOrder = 1
    DisplayFormat = '##,##0.00;-0.00'
    StoredValues = 0
  end
  object dxCurAltura: TdxCurrencyEdit [7]
    Left = 110
    Top = 80
    Width = 147
    TabOrder = 2
    DisplayFormat = '##,##0.00;-0.00'
    StoredValues = 0
  end
  object dxCurLargura: TdxCurrencyEdit [8]
    Left = 110
    Top = 112
    Width = 147
    TabOrder = 3
    DisplayFormat = '##,##0.00;-0.00'
    StoredValues = 0
  end
  object dxCurPesoLiquido: TdxCurrencyEdit [9]
    Left = 110
    Top = 144
    Width = 147
    TabOrder = 4
    DisplayFormat = '##,##0.00;-0.00'
    StoredValues = 0
  end
  inherited ImageList1: TImageList
    Left = 8
    Top = 72
  end
  object qryAplicaDimensaoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nDimensao'
        Attributes = [paSigned]
        DataType = ftBCD
        NumericScale = 4
        Precision = 12
        Size = 19
        Value = Null
      end
      item
        Name = 'nAltura'
        Attributes = [paSigned]
        DataType = ftBCD
        NumericScale = 4
        Precision = 12
        Size = 19
        Value = Null
      end
      item
        Name = 'nLargura'
        Attributes = [paSigned]
        DataType = ftBCD
        NumericScale = 4
        Precision = 12
        Size = 19
        Value = Null
      end
      item
        Name = 'nPesoLiquido'
        Attributes = [paSigned]
        DataType = ftBCD
        NumericScale = 4
        Precision = 12
        Size = 19
        Value = Null
      end
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'UPDATE Produto'
      '   SET nDimensao     = :nDimensao'
      '      ,nAltura       = :nAltura'
      '      ,nLargura      = :nLargura'
      '      ,nPesoLiquido  = :nPesoLiquido'
      ' WHERE nCdProdutoPai = :nPK')
    Left = 8
    Top = 104
  end
end
