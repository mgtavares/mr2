unit rEvolucaoVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, DBCtrls, QRCtrls, ComObj, ExcelXP;

type
  TrptEvolucaoVenda = class(TfrmRelatorio_Padrao)
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    Label1: TLabel;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    Label5: TLabel;
    DataSource4: TDataSource;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DataSource5: TDataSource;
    MaskEdit3: TMaskEdit;
    MaskEdit6: TMaskEdit;
    DBEdit1: TDBEdit;
    MaskEdit4: TMaskEdit;
    Label2: TLabel;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    dsGrupoEconomico: TDataSource;
    MaskEdit5: TMaskEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    MaskEdit7: TMaskEdit;
    Label7: TLabel;
    DBEdit5: TDBEdit;
    MaskEdit8: TMaskEdit;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    MaskEdit9: TMaskEdit;
    Label9: TLabel;
    DBEdit7: TDBEdit;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TAutoIncField;
    qryClasseProdutocNmClasseProduto: TStringField;
    dsDepartamento: TDataSource;
    dsMarca: TDataSource;
    dsLinha: TDataSource;
    dsClasseProduto: TDataSource;
    MaskEdit10: TMaskEdit;
    Label10: TLabel;
    RadioGroup3: TRadioGroup;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure MaskEdit9Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptEvolucaoVenda: TrptEvolucaoVenda;

implementation

uses fMenu, fLookup_Padrao, rVendaPeriodoProduto_view, rEvolucaoVenda_view;

{$R *.dfm}

procedure TrptEvolucaoVenda.FormShow(Sender: TObject);
var
    iAux : Integer ;
begin
  inherited;

  iAux := frmMenu.UsuarioEmpresaAutorizada;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;

  MaskEdit3.SetFocus ;

end;


procedure TrptEvolucaoVenda.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TrptEvolucaoVenda.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TrptEvolucaoVenda.ToolButton1Click(Sender: TObject);
var
  objRel : TrptEvolucaoVenda_view;
var
    dAux : TDateTime ;
    dia,Mes,Ano  : Word;
    i, nLabel    : integer;
var
  linha, coluna,LCID, iColuna : integer;
  planilha      : variant;
  valorcampo    : string;
const
  arrCells      : array[0..5] of String = ('L','J','H','F','D','B');
begin

  if (Length(Trim(MaskEdit10.Text)) <> 7) then
  begin
      ShowMessage('Compet�ncia Inv�lida.') ;
      exit ;
  end ;

  try
      dAux := StrToDateTime('01/' + MaskEdit10.Text) ;
  except
      ShowMessage('Compet�ncia Inv�lida.') ;
      exit ;
  end ;

  objRel := TrptEvolucaoVenda_view.Create(nil);
  Try
      Try
          objRel.usp_Relatorio.Close ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdGrupoEconomico').Value := frmMenu.ConvInteiro(MaskEdit4.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdTerceiro').Value       := frmMenu.ConvInteiro(MaskEdit6.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdDepartamento').Value   := frmMenu.ConvInteiro(MaskEdit5.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdMarca').Value          := frmMenu.ConvInteiro(MaskEdit7.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdLinha').Value          := frmMenu.ConvInteiro(MaskEdit8.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@nCdClasse').Value         := frmMenu.ConvInteiro(MaskEdit9.Text) ;
          objRel.usp_Relatorio.Parameters.ParamByName('@cMesAno').Value           := MaskEdit10.Text;
          objRel.usp_Relatorio.Open  ;

          if (RadioGroup3.ItemIndex = 0) then
          begin
              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
              objRel.lblFiltro1.Caption := 'Empresa : '        + Trim(MaskEdit3.Text) + ' ' + DbEdit3.Text ;
              objRel.lblFiltro2.Caption := 'Terceiro: '        + String(MaskEdit6.Text) + ' ' + DbEdit10.Text ;
              objRel.lblFiltro3.Caption := 'Grupo Economico: ' + String(MaskEdit4.Text) + ' ' + DbEdit1.Text ;
              objRel.lblFiltro4.Caption := 'Mes/Ano Base   : ' + MaskEdit10.Text ;

              DecodeDate(StrToDate('01/'+MaskEdit10.Text),Ano,mes,dia);

              nLabel := 4;
              for i := 0 to 5 do
              begin
                  if (mes = 0) then
                  begin
                      mes := 12;
                      ano := ano - 1;
                  end;

                  (objRel.FindComponent('QRLabel' + IntToStr(nLabel)) as TQRLabel).Caption := copy(FormatDateTime('mmmm',StrToDate(IntToStr(dia) + '/' + IntToStr(mes) + '/' + IntToStr(ano))),1,3) + '/' + IntToStr(ano);
                  nLabel := nLabel + 1;
                  mes := mes - 1;
              end;

              objRel.QuickRep1.PreviewModal;
          end
          else
          begin
              try
                  planilha := CreateoleObject('Excel.Application');
              except
                  MensagemErro('Ocorreu um erro no processamento da planilha, talvez o aplicativo de planilhas n�o esteja instalado corretamente.') ;
                  exit ;
              end ;

              frmMenu.StatusBar1.Panels.Items[6].Text := 'Gerando Planilha...';

              LCID := GetUserDefaultLCID;
              planilha.DisplayAlerts  := False;
              planilha.WorkBooks.add(1);
              planilha.ScreenUpdating := true;

              planilha.Caption := 'Evolu��o de Vendas - Semestral';

              icoluna := 1;

              for linha := 0 to objRel.usp_Relatorio.RecordCount - 1 do
              begin

                  for coluna := 1 to objRel.usp_Relatorio.FieldCount do
                  begin
                      valorcampo := objRel.usp_Relatorio.Fields[coluna - 1].AsString;
                      
                      if (objRel.usp_Relatorio.Fields[coluna - 1].DataType = ftBCD) then
                      begin
                          planilha.Cells[Linha+2,iColuna].NumberFormat        := '#.##0,00';
                          planilha.Cells[Linha+2,iColuna].HorizontalAlignment := xlRight ;
                          planilha.cells[linha + 2,iColuna] := StrToFloat(valorCampo);
                      end
                      else
                      begin
                          planilha.cells[linha + 2,iColuna] := valorCampo;
                      end;

                      iColuna := iColuna + 1;
                  end;

                  objRel.usp_Relatorio.Next;
                  icoluna := 1;
              end;

              DecodeDate(StrToDate('01/'+MaskEdit10.Text),Ano,mes,dia);

              for i := 0 to 5 do
              begin
                  if (mes = 0) then
                  begin
                      mes := 12;
                      ano := ano - 1;
                  end;

                  linha := objRel.usp_Relatorio.RecordCount + 2;

                  planilha.Cells[1,12 - (i * 2)] := copy(FormatDateTime('mmmm',StrToDate(IntToStr(dia) + '/' + IntToStr(mes) + '/' + IntToStr(ano))),1,3) + '/' + IntToStr(ano);
                  planilha.Cells[1,11 - (i * 2)] := '% Varia��o';
                  planilha.Cells[1,12 - (i * 2)].font.bold := true;
                  planilha.Cells[1,11 - (i * 2)].font.bold := true;

                  valorCampo := '=SUM(' + arrCells[i] + '2:' + arrCells[i] + intToStr(linha-1) + ')' ;
                  
                  planilha.Cells[linha ,12 - (i * 2)].formula := valorCampo;

                  mes := mes - 1;
              end;

              planilha.Cells[1,1]              := 'Terceiro';
              planilha.Cells[1,13]             := 'Total Geral';
              planilha.Cells[linha,13].formula := '=SUM(M2:M' + intToStr(linha-1) + ')';
              planilha.Cells[1,1].font.bold    := true;
              planilha.Cells[1,13].font.bold   := true;

              planilha.Range['A1','M1'].Borders.Item[$00000009].Weight := $FFFFEFD6;
              planilha.Range['B' + intToStr(linha),'M' + intToStr(linha)].Borders.Item[$00000008].Weight := $00000002;
              planilha.Range['A1',planilha.Cells[planilha.Rows.Count,planilha.Columns.Count]].Interior.Color := clWhite;

              planilha.columns.Autofit;
              planilha.Visible := True;

          end

      except
          MensagemErro('Erro na cria��o do relat�rio');
          planilha := '';
          raise;
      end;
  Finally;
      FreeAndNil(objRel);
      planilha := '';
      frmMenu.StatusBar1.Panels.Items[6].Text := '';
  end;

end;


procedure TrptEvolucaoVenda.MaskEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;

procedure TrptEvolucaoVenda.MaskEdit6KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptEvolucaoVenda.MaskEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(94);

        If (nPK > 0) then
        begin
            Maskedit4.Text := IntToStr(nPK) ;
            PosicionaQuery(qryGrupoEconomico, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptEvolucaoVenda.MaskEdit4Exit(Sender: TObject);
begin
  inherited;

  qryGrupoEconomico.Close ;
  
  PosicionaQuery(qryGrupoEconomico, MaskEdit4.Text) ;
end;

procedure TrptEvolucaoVenda.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryDepartamento.Close ;

  If (Trim(MaskEdit5.Text) <> '') then
  begin

    PosicionaQuery(qryDepartamento, Maskedit5.Text) ;

  end ;

end;

procedure TrptEvolucaoVenda.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryMarca.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin

    PosicionaQuery(qryMarca, Maskedit7.Text) ;

  end ;

end;

procedure TrptEvolucaoVenda.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(45);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptEvolucaoVenda.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptEvolucaoVenda.MaskEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLinha, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TrptEvolucaoVenda.MaskEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
        begin
            Maskedit9.Text := IntToStr(nPK) ;
            PosicionaQuery(qryClasseProduto, IntToStr(nPK));
        end ;

    end ;

  end ;

end;


procedure TrptEvolucaoVenda.MaskEdit8Exit(Sender: TObject);
begin
  inherited;
  qryLinha.Close ;

  If (Trim(MaskEdit8.Text) <> '') then
  begin

    PosicionaQuery(qryLinha, Maskedit8.Text) ;

  end ;

end;

procedure TrptEvolucaoVenda.MaskEdit9Exit(Sender: TObject);
begin
  inherited;
  qryClasseProduto.Close ;

  If (Trim(MaskEdit9.Text) <> '') then
  begin

    PosicionaQuery(qryClasseProduto, Maskedit9.Text) ;

  end ;

end;

initialization
     RegisterClass(TrptEvolucaoVenda) ;

end.
