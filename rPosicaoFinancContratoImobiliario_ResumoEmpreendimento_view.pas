unit rPosicaoFinancContratoImobiliario_ResumoEmpreendimento_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosicaoFinancContratoImobiliario_ResumoEmpreendimento_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText7: TQRDBText;
    QRBand4: TQRBand;
    QRBand5: TQRBand;
    lblFiltro: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    qryTempResumoEmpreendimento: TADOQuery;
    QRDBText4: TQRDBText;
    QRDBText12: TQRDBText;
    QRShape6: TQRShape;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRLabel14: TQRLabel;
    qryTempResumoEmpreendimentocNmEmpreendimentoBloco: TStringField;
    qryTempResumoEmpreendimentoiQtdeParcVencida: TIntegerField;
    qryTempResumoEmpreendimentonValVencido: TBCDField;
    qryTempResumoEmpreendimentonValTotalVencido: TBCDField;
    qryTempResumoEmpreendimentoiQtdeParcVencer: TIntegerField;
    qryTempResumoEmpreendimentonValTotalVencer: TBCDField;
    qryTempResumoEmpreendimentoiQtdeParcTotal: TIntegerField;
    qryTempResumoEmpreendimentonValTotal: TBCDField;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel16: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoFinancContratoImobiliario_ResumoEmpreendimento_view: TrptPosicaoFinancContratoImobiliario_ResumoEmpreendimento_view;

implementation

{$R *.dfm}

end.
