unit rPedidoCom_Simples;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, QuickRpt, QRCtrls, jpeg;

type
  TrptPedidoCom_Simples = class(TForm)
    QuickRep1: TQuickRep;
    qryPedido: TADOQuery;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidodDtPrevEntIni: TDateTimeField;
    qryPedidodDtPrevEntFim: TDateTimeField;
    qryPedidonCdEmpresa: TIntegerField;
    qryPedidocNmEmpresa: TStringField;
    qryPedidonCdLoja: TIntegerField;
    qryPedidocNmLoja: TStringField;
    qryPedidonCdTipoPedido: TIntegerField;
    qryPedidocNmTipoPedido: TStringField;
    qryPedidonCdTerceiro: TIntegerField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidocNrPedTerceiro: TStringField;
    qryPedidonCdCondPagto: TIntegerField;
    qryPedidocNmCondPagto: TStringField;
    qryPedidonCdTabStatusPed: TIntegerField;
    qryPedidocNmTabStatusPed: TStringField;
    qryPedidonPercDesconto: TBCDField;
    qryPedidonPercAcrescimo: TBCDField;
    qryPedidonValPedido: TBCDField;
    QRBand1: TQRBand;
    lblTitulo: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand2: TQRBand;
    qryItemEstoque_Grade: TADOQuery;
    qryItemEstoque_GradenCdProduto: TIntegerField;
    qryItemEstoque_GradecNmItem: TStringField;
    qryItemEstoque_GradenQtdePed: TBCDField;
    qryItemEstoque_GradenValTotalItem: TBCDField;
    qryItemEstoque_GradenCdGrade: TIntegerField;
    qryItemEstoque_GradecTituloGrade: TStringField;
    qryItemEstoque_GradecQtdeGrade: TStringField;
    QRSubDetail1: TQRSubDetail;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRGroup2: TQRGroup;
    QRDBText15: TQRDBText;
    QRLabel22: TQRLabel;
    qryItemAD: TADOQuery;
    qryItemADcCdProduto: TStringField;
    qryItemADcNmItem: TStringField;
    qryItemADcSiglaUnidadeMedida: TStringField;
    qryItemADnQtdePed: TBCDField;
    qryItemADnValCustoUnit: TBCDField;
    qryItemADnValTotalItem: TBCDField;
    QRSubDetail2: TQRSubDetail;
    QRDBText16: TQRDBText;
    QRGroup3: TQRGroup;
    qryItemADnCdPedido: TIntegerField;
    QRLabel26: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    qryPedidocNmContato: TStringField;
    QRSubDetail3: TQRSubDetail;
    QRGroup4: TQRGroup;
    qryItemFormula: TADOQuery;
    qryItemFormulanCdProduto: TIntegerField;
    qryItemFormulacNmItem: TStringField;
    qryItemFormulanQtdePed: TBCDField;
    qryItemFormulanValCustoUnit: TBCDField;
    qryItemFormulanValTotalItem: TBCDField;
    qryItemFormulacComposicao: TStringField;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel35: TQRLabel;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    qryPedidocNmTerceiroRepres: TStringField;
    qryPedidonValImposto: TBCDField;
    qryPedidonValProdutos: TBCDField;
    qryPedidonValFrete: TBCDField;
    SummaryBand1: TQRBand;
    QRDBText11: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText34: TQRDBText;
    QRLabel37: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRLabel15: TQRLabel;
    qryPedidonValOutros: TBCDField;
    QRShape5: TQRShape;
    qryItemEstoque_GradedDtEntregaIni: TDateTimeField;
    qryItemEstoque_GradedDtEntregaFim: TDateTimeField;
    qryItemEstoque_GradenQtdePrev: TBCDField;
    qryItemEstoque_GradenPercIPI: TBCDField;
    qryItemEstoque_GradenPercICMSSub: TBCDField;
    QRLabel39: TQRLabel;
    qryPedidocNmUsuarioComprador: TStringField;
    qryItemEstoque_GradecSiglaUnidadeMedida: TStringField;
    QRDBText41: TQRDBText;
    QRDBText42: TQRDBText;
    QRDBText43: TQRDBText;
    QRDBText37: TQRDBText;
    qryPedidonQtdeItens: TBCDField;
    QRLabel44: TQRLabel;
    QRDBText36: TQRDBText;
    qryPedidocEnderecoLocalEntrega: TStringField;
    QRLabel47: TQRLabel;
    QRDBText39: TQRDBText;
    qryItemEstoque_GradenPercDescontoItem: TBCDField;
    QRDBText44: TQRDBText;
    qryItemEstoque_GradenValCustoUnit: TFloatField;
    qryItemEstoque_GradenValUnitario: TFloatField;
    qryItemEstoque_GradenValDesconto: TFloatField;
    qryItemEstoque_GradenValAcrescimo: TFloatField;
    qryItemEstoque_GradenValIPIUnitario: TFloatField;
    qryPedidocOBS: TMemoField;
    DataSource1: TDataSource;
    QRDBText1: TQRDBText;
    QRLabel4: TQRLabel;
    qryPedidocRevisaoPedido: TStringField;
    qryPedidocCodNBR: TStringField;
    qryPedidocCriterioAceite: TMemoField;
    QRDBText35: TQRDBText;
    QRLabel50: TQRLabel;
    qryPedidocObjetivoPedido: TStringField;
    QRDBText46: TQRDBText;
    QRDBText47: TQRDBText;
    QRDBText48: TQRDBText;
    QRBand3: TQRBand;
    QRDBText49: TQRDBText;
    qryItemADnPercIPI: TBCDField;
    qryItemADnPercICMSSub: TBCDField;
    qryItemADnValUnitario: TBCDField;
    qryItemADnValIPI: TBCDField;
    QRDBText50: TQRDBText;
    QRDBText38: TQRDBText;
    QRLabel25: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRLabel1: TQRLabel;
    QRDBText2: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel9: TQRLabel;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel11: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel27: TQRLabel;
    QRDBText21: TQRDBText;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel34: TQRLabel;
    QRDBText33: TQRDBText;
    QRLabel13: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel43: TQRLabel;
    QRDBText40: TQRDBText;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel48: TQRLabel;
    QRLabel38: TQRLabel;
    QRDBText45: TQRDBText;
    QRLabel2: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel54: TQRLabel;
    QRShape1: TQRShape;
    QRLabel16: TQRLabel;
    QRLabel49: TQRLabel;
    procedure QRGroup2BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure QRSubDetail1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPedidoCom_Simples: TrptPedidoCom_Simples;

implementation

uses fMenu;

{$R *.dfm}

procedure TrptPedidoCom_Simples.QRGroup2BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin

  QRLabel22.Caption   := 'Grade' ;
  QRSubDetail1.Height := 32 ;

  if (qryItemEstoque_GradecTituloGrade.Value = '') then
  begin
      QRLabel22.Caption := 'Item Estoque' ;
      QRSubDetail1.Height := 16 ;
  end ;


end;

procedure TrptPedidoCom_Simples.QuickRep1BeforePrint(
  Sender: TCustomQuickRep; var PrintReport: Boolean);
begin

  QRDBText42.Mask := frmMenu.cMascaraCompras;
  QRDBText43.Mask := frmMenu.cMascaraCompras;
  QRDBText37.Mask := frmMenu.cMascaraCompras;
  QRDBText31.Mask := frmMenu.cMascaraCompras;

  QRDBText50.Mask := frmMenu.cMascaraCompras;
  QRDBText38.Mask := frmMenu.cMascaraCompras;
  QRDBText18.Mask := frmMenu.cMascaraCompras;

  if (frmMenu.LeParametro('VAREJO') <> 'S') then
  begin

      QRLabel1.Enabled  := False ;
      QRDBText2.Enabled := False ;

  end ;

end;

procedure TrptPedidoCom_Simples.QRSubDetail1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
    QRLabel53.Enabled := ((qryItemEstoque_GradedDtEntregaIni.asString <> '') or (qryItemEstoque_GradedDtEntregaFim.asString <> '')) ; 

end;

end.
