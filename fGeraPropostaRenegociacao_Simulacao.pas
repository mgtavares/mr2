unit fGeraPropostaRenegociacao_Simulacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, StdCtrls, cxButtons, DB, ADODB, DBCtrls, Mask,fCobranca_VisaoContato_Contato;

type
  TfrmGeraPropostaRenegociacao_Simulacao = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    btGerarProposta: TcxButton;
    GroupBox4: TGroupBox;
    dsProposta: TADODataSet;
    dsPropostanCdPropostaReneg: TIntegerField;
    dsPropostaiQtdeParc: TIntegerField;
    dsPropostaiQtdeParcMax: TIntegerField;
    dsPropostaiDiasAtrasoMax: TIntegerField;
    dsPropostanValOriginal: TFloatField;
    dsPropostanValDescOriginalMax: TFloatField;
    dsPropostanValDescOriginal: TFloatField;
    dsPropostanValJuros: TFloatField;
    dsPropostanValDescJurosMax: TFloatField;
    dsPropostanValDescJuros: TFloatField;
    dsPropostanValRenegociado: TFloatField;
    dsPropostanValEntradaMin: TFloatField;
    dsPropostanValEntrada: TFloatField;
    dsPropostanValParcela: TFloatField;
    dsPropostanValParcelaMin: TFloatField;
    dsPropostadDtValProposta: TDateField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    dsPropostacObs: TMemoField;
    z: TDBMemo;
    qryTabRenegociacao: TADOQuery;
    qryTabRenegociacaonCdTabRenegociacao: TAutoIncField;
    qryTabRenegociacaonCdEmpresa: TIntegerField;
    qryTabRenegociacaoiDiasAtraso: TIntegerField;
    qryTabRenegociacaonPercDescPrincVista: TBCDField;
    qryTabRenegociacaoiParcLimDescPrinc: TIntegerField;
    qryTabRenegociacaonPercDescPrincParc: TBCDField;
    qryTabRenegociacaonPercDescPrincMaiorParc: TBCDField;
    qryTabRenegociacaonPercDescJurosVista: TBCDField;
    qryTabRenegociacaoiParcLimDescJuros: TIntegerField;
    qryTabRenegociacaonPercDescJurosParc: TBCDField;
    qryTabRenegociacaonPercDescJurosMaiorParc: TBCDField;
    qryTabRenegociacaoiQtdeMaxParc: TIntegerField;
    qryTabRenegociacaonPercMinEntrada: TBCDField;
    qryTabRenegociacaonPercEntradaSug: TBCDField;
    qryTabRenegociacaonValParcelaMin: TBCDField;
    dsPropostanPercDescOriginalMax: TFloatField;
    dsPropostanPercDescOriginal: TFloatField;
    dsPropostanPercDescJurosMax: TFloatField;
    dsPropostanPercDescJuros: TFloatField;
    Label16: TLabel;
    DBEdit16: TDBEdit;
    Label17: TLabel;
    DBEdit17: TDBEdit;
    Label18: TLabel;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    Label19: TLabel;
    qryPropostaReneg: TADOQuery;
    dsPropostanCdTerceiro: TIntegerField;
    dsPropostanCdItemListaCobranca: TIntegerField;
    qryAux: TADOQuery;
    qryPropostaRenegnCdPropostaReneg: TAutoIncField;
    qryPropostaRenegnCdEmpresa: TIntegerField;
    qryPropostaRenegnCdLoja: TIntegerField;
    qryPropostaRenegnCdTerceiro: TIntegerField;
    qryPropostaRenegdDtProposta: TDateTimeField;
    qryPropostaRenegnCdUsuarioGeracao: TIntegerField;
    qryPropostaRenegnCdUsuarioAprov: TIntegerField;
    qryPropostaRenegdDtAprov: TDateTimeField;
    qryPropostaRenegdDtValidade: TDateTimeField;
    qryPropostaRenegnCdTabStatusPropostaReneg: TIntegerField;
    qryPropostaRenegnCdItemListaCobranca: TIntegerField;
    qryPropostaRenegnCdLojaEfetiv: TIntegerField;
    qryPropostaRenegnCdUsuarioEfetiv: TIntegerField;
    qryPropostaRenegdDtEfetiv: TDateTimeField;
    qryPropostaRenegnCdUsuarioCancel: TIntegerField;
    qryPropostaRenegdDtCancel: TDateTimeField;
    qryPropostaRenegiDiasAtraso: TIntegerField;
    qryPropostaRenegiParcelas: TIntegerField;
    qryPropostaRenegnValOriginal: TBCDField;
    qryPropostaRenegnValJuros: TBCDField;
    qryPropostaRenegnValDescMaxOriginal: TBCDField;
    qryPropostaRenegnValDescOriginal: TBCDField;
    qryPropostaRenegnValDescMaxJuros: TBCDField;
    qryPropostaRenegnValDescJuros: TBCDField;
    qryPropostaRenegnSaldoNegociado: TBCDField;
    qryPropostaRenegnValEntrada: TBCDField;
    qryPropostaRenegnValEntradaMin: TBCDField;
    qryPropostaRenegnValParcela: TBCDField;
    qryPropostaRenegnValParcelaMin: TBCDField;
    qryPropostaRenegcOBS: TMemoField;
    qryTituloPropostaReneg: TADOQuery;
    qryTituloPropostaRenegnCdTituloPropostaReneg: TIntegerField;
    qryTituloPropostaRenegnCdPropostaReneg: TIntegerField;
    qryTituloPropostaRenegnCdTitulo: TIntegerField;
    qryTituloPropostaRenegnSaldoTit: TBCDField;
    qryTitulos: TADOQuery;
    qryTitulosnCdTitulo: TIntegerField;
    qryTitulosnSaldoTit: TBCDField;
    qryPropostaRenegcFlgRenegMultipla: TIntegerField;
    procedure NovaProposta (iDiasAtraso: integer; nValOriginal,nValJuros: double; nCdItemListaCorbanca,nCdTerceiro:integer);
    procedure FormShow(Sender: TObject);
    procedure btSimularClick(Sender: TObject);
    procedure atualizaSaldoNegociar();
    procedure atualizaTabelaRenegociacao(iParcelas, iDiasAtraso:integer);
    procedure DBEdit1Exit(Sender: TObject);
    procedure atualizaValorParcela();
    procedure atualizaValores();
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit9Exit(Sender: TObject);
    procedure DBEdit12Change(Sender: TObject);
    procedure atualizaValorEntrada(iParcelas:integer);
    procedure DBEdit10Change(Sender: TObject);
    procedure DBEdit17Exit(Sender: TObject);
    procedure DBEdit19Exit(Sender: TObject);
    procedure btGerarPropostaClick(Sender: TObject);
    procedure RegistraContato(cFlgForcaContato: boolean; cTextoSugerido:string);
  private
    { Private declarations }
    objfrmCobranca_VisaoContato_Contato : TfrmCobranca_VisaoContato_Contato;
  public
    { Public declarations }
    cFlgRenegMultipla : Integer;
  end;

var
  frmGeraPropostaRenegociacao_Simulacao: TfrmGeraPropostaRenegociacao_Simulacao;
  bPermExcederDesconto, bExigirAutorizacao : string ;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

{ TfrmGeraPropostaRenegociacao_Simulacao }

procedure TfrmGeraPropostaRenegociacao_Simulacao.NovaProposta(
  iDiasAtraso: integer; nValOriginal,nValJuros:double; nCdItemListaCorbanca, nCdTerceiro:integer);
begin

    if not dsProposta.Active then
    begin
        dsProposta.CreateDataSet;
        dsProposta.Active := True;
    end ;

    dsProposta.Insert;

    dsPropostaiDiasAtrasoMax.Value       := iDiasAtraso ;
    dsPropostaiQtdeParc.Value            := 0 ;
    dsPropostanValOriginal.Value         := nValOriginal ;
    dsPropostanValDescOriginalMax.Value  := 0 ;
    dsPropostanValDescOriginal.Value     := 0 ;
    dsPropostanValJuros.Value            := nValJuros ;
    dsPropostanValDescJurosMax.Value     := 0 ;
    dsPropostanValDescJuros.Value        := 0 ;
    dsPropostanValRenegociado.Value      := (nValOriginal + nValJuros) ;
    dsPropostanValEntradaMin.Value       := 0 ;
    dsPropostanValEntrada.Value          := (nValOriginal + nValJuros) ;
    dsPropostanValParcela.Value          := 0 ;
    dsPropostanValParcelaMin.Value       := 0 ;
    dsPropostadDtValProposta.Value       := Date+StrToInt(frmMenu.LeParametro('DIASVALPROPREN'));
    dsPropostanPercDescOriginalMax.Value := 0 ;
    dsPropostanPercDescOriginal.Value    := 0 ;
    dsPropostanPercDescJurosMax.Value    := 0 ;
    dsPropostanPercDescJuros.Value       := 0 ;
    dsPropostanCdItemListaCobranca.Value := nCdItemListaCorbanca ;
    dsPropostanCdTerceiro.Value          := nCdTerceiro ;

    // quando abrir a tela, j� abre com a simula��o a vista
    atualizaTabelaRenegociacao(0, dsPropostaiDiasAtrasoMax.Value) ;


    Self.ShowModal;

end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.FormShow(Sender: TObject);
begin
  inherited;

  bPermExcederDesconto := frmMenu.LeParametro('PERMEXDLIMDREN') ;
  bExigirAutorizacao   := frmMenu.LeParametro('EXIAUTORTODREN') ;


  DBEdit1.SetFocus;

end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.btSimularClick(
  Sender: TObject);
begin
  inherited;

  DBEdit12.SetFocus;
  
end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.atualizaSaldoNegociar;
begin

    dsPropostanValRenegociado.Value := (dsPropostanValOriginal.Value + dsPropostanValJuros.Value) ;
    dsPropostanValRenegociado.Value := dsPropostanValRenegociado.Value - dsPropostanValDescOriginal.Value - dsPropostanValDescJuros.Value ;

end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.atualizaTabelaRenegociacao(
  iParcelas, iDiasAtraso: integer);
begin

    dsPropostaiQtdeParc.Value := iParcelas;
    
    qryTabRenegociacao.Close;
    qryTabRenegociacao.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
    qryTabRenegociacao.Parameters.ParamByName('iDiasAtraso').Value := iDiasAtraso ;
    qryTabRenegociacao.Open;

    if (qryTabRenegociacao.eof) then
    begin
        MensagemAlerta('Tabela de renegocia��o n�o encontrada para ' + intTostr(iDiasAtraso) + ' dias de atraso.') ;
        PostMessage(Self.Handle, WM_CLOSE, 0, 0);
        abort ;
    end ;

    dsPropostaiQtdeParcMax.Value         := qryTabRenegociacaoiQtdeMaxParc.Value ;
    dsPropostanValParcelaMin.Value       := qryTabRenegociacaonValParcelaMin.Value ;

    dsPropostanPercDescOriginalMax.Value := 0 ;
    dsPropostanPercDescOriginal.Value    := 0 ;
    dsPropostanPercDescJurosMax.Value    := 0 ;
    dsPropostanPercDescJuros.Value       := 0 ;
    dsPropostanValDescOriginal.Value     := 0 ;
    dsPropostanValDescJuros.Value        := 0 ;

    if (iParcelas > qryTabRenegociacaoiQtdeMaxParc.Value) then
    begin
        MensagemAlerta('Quantidade de parcelas maior que o limite dispon�vel.') ;
        atualizatabelarenegociacao(0, iDiasAtraso) ; // volta para a condi��o a vista ;
        DBEdit1.SetFocus;
        abort ;
    end ;

    {-- calcula o desconto no principal --}


    {-- se for a vista --}
    if (iParcelas = 0) then
    begin
        if (qryTabRenegociacaonPercDescPrincVista.Value > 0) then
        begin
            dsPropostanValDescOriginalMax.Value  := frmMenu.TBRound((dsPropostanValOriginal.Value * (qryTabRenegociacaonPercDescPrincVista.Value/100)),2);
            dsPropostanPercDescOriginalMax.Value := qryTabRenegociacaonPercDescPrincVista.Value ;
        end 
        else
        begin
            dsPropostanValDescOriginalMax.Value  := 0 ;
            dsPropostanPercDescOriginalMax.Value := 0 ;
        end ;
    end ;

    {-- se for parcelado --}
    if (iParcelas > 0) then
    begin
        {-- se a parcela selecionada estiver na primeira faixa de descontos --}
        if (iParcelas <= qryTabRenegociacaoiParcLimDescPrinc.Value) then
            if (qryTabRenegociacaonPercDescPrincParc.Value > 0) then
            begin
                dsPropostanValDescOriginalMax.Value  := frmMenu.TBRound((dsPropostanValOriginal.Value * (qryTabRenegociacaonPercDescPrincParc.Value/100)),2);
                dsPropostanPercDescOriginalMax.Value := qryTabRenegociacaonPercDescPrincParc.Value ;
            end
            else
            begin
                dsPropostanValDescOriginalMax.Value  := 0 ;
                dsPropostanPercDescOriginalMax.Value := 0 ;
            end ;

        {-- se a parcela selecionada for maior que a primeira faixa de descontos --}
        if (iParcelas > qryTabRenegociacaoiParcLimDescPrinc.Value) then
            if (qryTabRenegociacaonPercDescPrincMaiorParc.Value > 0) then
            begin
                dsPropostanValDescOriginalMax.Value  := frmMenu.TBRound((dsPropostanValOriginal.Value * (qryTabRenegociacaonPercDescPrincMaiorParc.Value/100)),2);
                dsPropostanPercDescOriginalMax.Value := qryTabRenegociacaonPercDescPrincMaiorParc.Value ;
            end
            else
            begin
                dsPropostanValDescOriginalMax.Value  := 0 ;
                dsPropostanPercDescOriginalMax.Value := 0 ;
            end ;

    end ;


    {-- calcula o desconto no juros --}

    {-- se for a vista --}
    if (iParcelas = 0) then
    begin
        if (qryTabRenegociacaonPercDescJurosVista.Value > 0) then
        begin
            dsPropostanValDescJurosMax.Value  := frmMenu.TBRound((dsPropostanValJuros.Value * (qryTabRenegociacaonPercDescJurosVista.Value/100)),2);
            dsPropostanPercDescJurosMax.Value := qryTabRenegociacaonPercDescJurosVista.Value ;
        end
        else
        begin
            dsPropostanValDescJurosMax.Value  := 0 ;
            dsPropostanPercDescJurosMax.Value := 0 ;
        end ;

    end ;

    {-- se for parcelado --}
    if (iParcelas > 0) then
    begin
        {-- se a parcela selecionada estiver na primeira faixa de descontos --}
        if (iParcelas <= qryTabRenegociacaoiParcLimDescJuros.Value) then
            if (qryTabRenegociacaonPercDescJurosParc.Value > 0) then
            begin
                dsPropostanValDescJurosMax.Value  := frmMenu.TBRound((dsPropostanValJuros.Value * (qryTabRenegociacaonPercDescJurosParc.Value/100)),2);
                dsPropostanPercDescJurosMax.Value := qryTabRenegociacaonPercDescJurosParc.Value ;
            end
            else
            begin
                dsPropostanValDescJurosMax.Value  := 0 ;
                dsPropostanPercDescJurosMax.Value := 0 ;
            end ;

        {-- se a parcela selecionada for maior que a primeira faixa de descontos --}
        if (iParcelas > qryTabRenegociacaoiParcLimDescJuros.Value) then
            if (qryTabRenegociacaonPercDescJurosMaiorParc.Value > 0) then
            begin
                dsPropostanValDescJurosMax.Value  := frmMenu.TBRound((dsPropostanValJuros.Value * (qryTabRenegociacaonPercDescJurosMaiorParc.Value/100)),2);
                dsPropostanPercDescJurosMax.Value := qryTabRenegociacaonPercDescJurosMaiorParc.Value;
            end
            else
            begin
                dsPropostanValDescJurosMax.Value  := 0 ;
                dsPropostanPercDescJurosMax.Value := 0 ;
            end ;

    end ;

    {-- calcula a entrada m�nima --}
    atualizaValorEntrada(iParcelas);
    atualizaValorParcela();

end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.DBEdit1Exit(
  Sender: TObject);
begin
  inherited;

  atualizaTabelaRenegociacao(dsPropostaiQtdeParc.Value, dsPropostaiDiasAtrasoMax.Value) ;

  atualizaValores();
  
  DBEdit12.ReadOnly := (dsPropostaiQtdeParc.Value = 0) ;

  if (DBEdit12.ReadOnly) then
      DBEdit12.Color := $00E9E4E4
  else DBEdit12.Color := clWhite ;

end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.atualizaValorParcela;
begin

  if (dsPropostaiQtdeParc.Value > 0) then
      dsPropostanValParcela.Value := (dsPropostanValRenegociado.Value - dsPropostanValEntrada.Value)/dsPropostaiQtdeParc.Value
  else dsPropostanValParcela.Value := 0 ;

end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.atualizaValores;
begin
    atualizaSaldoNegociar();
    atualizaValorParcela() ;
end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.DBEdit6Exit(
  Sender: TObject);
begin
  inherited;

  if (dsPropostanValDescOriginal.Value > dsPropostanValOriginal.Value) then
  begin
      MensagemErro('Valor do desconto n�o pode ser maior que o valor original.') ;
      dsPropostanValDescOriginal.Value  := 0 ;
      dsPropostanPercDescOriginal.Value := 0 ;
      abort ;
  end ;

  if (dsPropostanValDescOriginal.Value > 0) then
      dsPropostanPercDescOriginal.Value := ((dsPropostanValDescOriginal.Value/dsPropostanValOriginal.Value)*100)
  else dsPropostanPercDescOriginal.Value := 0 ;

  atualizaValores();
end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.DBEdit9Exit(
  Sender: TObject);
begin
  inherited;

  if (dsPropostanValDescJuros.Value > dsPropostanValJuros.Value) then
  begin
      MensagemErro('Valor do desconto n�o pode ser maior que o valor do juros.') ;
      dsPropostanValDescJuros.Value  := 0 ;
      dsPropostanPercDescJuros.Value := 0 ;
      abort ;
  end ;

  if (dsPropostanValDescJuros.Value > 0) then
      dsPropostanPercDescJuros.Value := ((dsPropostanValDescJuros.Value/dsPropostanValJuros.Value)*100)
  else dsPropostanPercDescJuros.Value := 0 ;

  atualizaValores();

end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.DBEdit12Change(
  Sender: TObject);
begin
  inherited;

  if (dsPropostanValEntrada.Value > dsPropostanValRenegociado.Value) then
  begin

      MensagemAlerta('O valor da entrada n�o pode ser maior que o saldo a renegociar.') ;
      dsPropostanValEntrada.Value := dsPropostanValRenegociado.Value;

  end ;

  atualizaValorParcela() ;

end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.atualizaValorEntrada(
  iParcelas: integer);
begin

    if (iParcelas = 0) then
    begin
        dsPropostanValEntrada.Value    := dsPropostanValRenegociado.Value ;
        dsPropostanValEntradaMin.Value := dsPropostanValRenegociado.Value ;
    end
    else
    begin
        dsPropostanValEntradaMin.Value := frmMenu.TBRound(dsPropostanValRenegociado.Value * (qryTabRenegociacaonPercMinEntrada.Value/100),2);
        dsPropostanValEntrada.Value    := frmMenu.TBRound(dsPropostanValRenegociado.Value * (qryTabRenegociacaonPercEntradaSug.Value/100),2);

        dsPropostanValEntradaMin.Value := dsPropostanValEntradaMin.Value - Frac(dsPropostanValEntradaMin.Value) ;
        dsPropostanValEntrada.Value    := dsPropostanValEntrada.Value - Frac(dsPropostanValEntrada.Value) ;
    end ;

end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.DBEdit10Change(
  Sender: TObject);
begin
  inherited;

  atualizaValorEntrada(dsPropostaiQtdeParc.Value) ;

end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.DBEdit17Exit(
  Sender: TObject);
begin
  inherited;

  if (dsPropostanPercDescOriginal.Value > 100) then
      dsPropostanPercDescOriginal.Value := 100 ;

  if (dsPropostanPercDescOriginal.Value > 0) then
      dsPropostanValDescOriginal.Value := frmMenu.TBRound((dsPropostanValOriginal.Value * (dsPropostanPercDescOriginal.Value/100)),2)
  else dsPropostanValDescOriginal.Value := 0 ;

  atualizaValores();
  
end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.DBEdit19Exit(
  Sender: TObject);
begin
  inherited;
  if (dsPropostanPercDescJuros.Value > 100) then
      dsPropostanPercDescJuros.Value := 100 ;

  if (dsPropostanPercDescJuros.Value > 0) then
      dsPropostanValDescJuros.Value := frmMenu.TBRound((dsPropostanValJuros.Value * (dsPropostanPercDescJuros.Value/100)),2)
  else dsPropostanValDescJuros.Value := 0 ;

  atualizaValores();

end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.btGerarPropostaClick(
  Sender: TObject);
var
    nCdListaCobranca : integer ;
    cTextoSugerido   : string ;
    cFlgDivergencia  : string ;
begin
  inherited;

  cFlgDivergencia := 'N' ;

  if (bPermExcederDesconto = 'N') then
  begin

      if (dsPropostanValDescOriginal.Value > dsPropostanValDescOriginalMax.Value) then
      begin
          cFlgDivergencia := 'S' ;
          MensagemErro('Desconto do valor original maior que o valor m�ximo permitido.');
          DBEdit6.SetFocus;
          abort ;
      end ;

      if (dsPropostanValDescJuros.Value > dsPropostanValDescJurosMax.Value) then
      begin
          cFlgDivergencia := 'S' ;
          MensagemErro('Desconto do valor dos juros maior que o valor m�ximo permitido.');
          DBEdit9.SetFocus;
          abort ;
      end ;

      if (dsPropostaiQtdeParc.Value > 0) and (dsPropostanValParcela.Value < dsPropostanValParcelaMin.Value) then
      begin
          cFlgDivergencia := 'S' ;
          MensagemErro('Valor da parcela menor que o valor m�nimo.');
          DBEdit12.SetFocus;
          abort ;
      end ;
  end ;

  if (bPermExcederDesconto = 'S') then
  begin

      if (dsPropostanValDescOriginal.Value > dsPropostanValDescOriginalMax.Value) then
      begin
          cFlgDivergencia := 'S' ;
          if (MessageDlg('Desconto do valor original maior que o valor m�ximo permitido.' +#13#13 + 'Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          begin
              DBEdit6.SetFocus;
              abort ;
          end ;
      end ;

      if (dsPropostanValDescJuros.Value > dsPropostanValDescJurosMax.Value) then
      begin
          cFlgDivergencia := 'S' ;
          if (MessageDlg('Desconto do valor dos juros maior que o valor m�ximo permitido.' +#13#13 + 'Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          begin
              DBEdit9.SetFocus;
              abort ;
          end ;
      end ;

      if (dsPropostaiQtdeParc.Value > 0) and (dsPropostanValParcela.Value < dsPropostanValParcelaMin.Value) then
      begin
          cFlgDivergencia := 'S' ;
          if (MessageDlg('Valor da parcela menor que o valor m�nimo.' +#13#13 + 'Deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          begin
              DBEdit12.SetFocus;
              abort ;
          end ;
      end ;

  end ;

  if (MessageDlg('Confirma a gera��o desta proposta ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  qryPropostaReneg.Close;
  qryPropostaReneg.Open;

  qryPropostaReneg.Insert;
  qryPropostaRenegnCdPropostaReneg.Value          := frmMenu.fnProximoCodigo('PROPOSTARENEG') ;
  qryPropostaRenegnCdEmpresa.Value                := frmMenu.nCdEmpresaAtiva;

  if (frmMenu.nCdLojaAtiva > 0) then
      qryPropostaRenegnCdLoja.Value               := frmMenu.nCdLojaAtiva
  else qryPropostaRenegnCdLoja.AsString           := '' ;

  qryPropostaRenegnCdTerceiro.Value               := dsPropostanCdTerceiro.Value;
  qryPropostaRenegdDtProposta.Value               := Now() ;
  qryPropostaRenegnCdUsuarioGeracao.Value         := frmMenu.nCdUsuarioLogado;
  qryPropostaRenegdDtValidade.Value               := dsPropostadDtValProposta.Value;
  qryPropostaRenegcFlgRenegMultipla.Value         := cFlgRenegMultipla;
  qryPropostaRenegnCdTabStatusPropostaReneg.Value := 1; // Gerada

  {-- se n�o exige autoriza��o em todas renegocia��es e n�o tem diverg�ncias na renegocia��o, autoriza. --}
  {-- se a renegocia��o tiver divergencia, o status fica como gerado e aguarda aprova��o --}
  if (bExigirAutorizacao = 'N') and (cFlgDivergencia = 'N') then
  begin
      qryPropostaRenegnCdUsuarioAprov.Value           := qryPropostaRenegnCdUsuarioGeracao.Value ;
      qryPropostaRenegdDtAprov.Value                  := qryPropostaRenegdDtProposta.Value ;
      qryPropostaRenegnCdTabStatusPropostaReneg.Value := 2 ; //Aprovada
  end ;

  if (dsPropostanCdItemListaCobranca.Value > 0) then
      qryPropostaRenegnCdItemListaCobranca.Value      := dsPropostanCdItemListaCobranca.Value
  else qryPropostaRenegnCdItemListaCobranca.asString  := '' ;

  qryPropostaRenegiDiasAtraso.Value               := dsPropostaiDiasAtrasoMax.Value;
  qryPropostaRenegiParcelas.Value                 := dsPropostaiQtdeParc.Value;
  qryPropostaRenegnValOriginal.Value              := dsPropostanValOriginal.Value;
  qryPropostaRenegnValJuros.Value                 := dsPropostanValJuros.Value;
  qryPropostaRenegnValDescMaxOriginal.Value       := dsPropostanValDescOriginalMax.Value;
  qryPropostaRenegnValDescOriginal.Value          := dsPropostanValDescOriginal.Value;
  qryPropostaRenegnValDescMaxJuros.Value          := dsPropostanValDescJurosMax.Value;
  qryPropostaRenegnValDescJuros.Value             := dsPropostanValDescJuros.Value;
  qryPropostaRenegnSaldoNegociado.Value           := dsPropostanValRenegociado.Value;
  qryPropostaRenegnValEntrada.Value               := dsPropostanValEntrada.Value;
  qryPropostaRenegnValEntradaMin.Value            := dsPropostanValEntradaMin.Value;
  qryPropostaRenegnValParcela.Value               := dsPropostanValParcela.Value;
  qryPropostaRenegnValParcelaMin.Value            := dsPropostanValParcelaMin.Value;
  qryPropostaRenegcOBS.Value                      := dsPropostacOBS.Value;


  cTextoSugerido := 'RENEGOCIA��O REALIZADA POR ' + frmMenu.cNmUsuarioLogado + ' em ' + DateTimeToStr(Now()) + '.' +#13#13 ;

  if (frmMenu.nCdLojaAtiva > 0) then
      cTextoSugerido := cTextoSugerido + 'Loja Cobradora : ' + frmMenu.ZeroEsquerda(intTostr(frmMenu.nCdLojaAtiva),3) + #13#13;

  cTextoSugerido := cTextoSugerido + 'Valor L�quido  : ' + Format('%m',[qryPropostaRenegnSaldoNegociado.Value]) + #13#13 ;
  cTextoSugerido := cTextoSugerido + 'Parcelas       : ' + frmMenu.ZeroEsquerda(qryPropostaRenegiParcelas.AsString,3) +#13#13 ;
  cTextoSugerido := cTextoSugerido + 'Valor Entrada  : ' + Format('%m',[qryPropostaRenegnValEntrada.Value]) + #13#13 ;
  cTextoSugerido := cTextoSugerido + 'Valor Parcelas : ' + Format('%m',[qryPropostaRenegnValParcela.Value]) + #13#13 ;
  cTextoSugerido := cTextoSugerido + 'Valida at�     : ' + qryPropostaRenegdDtValidade.asString;

  {-- se a renegocia��o veio da lista de cobran�a, ajusta os calculos da lista --}
  if (dsPropostanCdItemListaCobranca.Value > 0) then
  begin

      {-- abre a tela para registro do contato --}
      registraContato(true,cTextoSugerido) ;

      {-- se o contato foi registrado, continua aqui... --}
      if (objfrmCobranca_VisaoContato_Contato.cGravacaoEfetuada) then
      begin
          frmMenu.Connection.BeginTrans;

          try

              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT nCdListaCobranca FROM ItemListaCobranca WHERE nCdItemListaCobranca = ' + dsPropostanCdItemListaCobranca.asString) ;
              qryAux.Open ;

              if not qryAux.Eof then
              begin

                  {-- obtem o c�digo da lista de cobran�a --}
                  nCdListaCobranca := qryAux.FieldList[0].Value ;

                  qryAux.Close;
                  qryAux.SQL.Clear;

                  {-- Atualiza a quantidade de contatos da lista --}
                  qryAux.SQL.Add('UPDATE ListaCobranca SET iQtdeClienteContato = iQtdeClienteContato + 1, iQtdeAcordo = iQtdeAcordo + 1 WHERE nCdListaCobranca = ' + intToStr(nCdListaCobranca)) ;
                  qryAux.ExecSQL;

                  {-- Caso a lista n�o tenha mais clientes para cobrar, encerra a lista --}
                  qryAux.SQL.Clear ;
                  qryAux.SQL.Add('UPDATE ListaCobranca SET dDtEncerramento = GetDate() WHERE (iQtdeClienteTotal - iQtdeClienteContato - iQtdeSemContato) <= 0 AND nCdListaCobranca = ' + intToStr(nCdListaCobranca)) ;
                  qryAux.ExecSQL;

                  {-- Encerra o contato da lista --}
                  qryAux.SQL.Clear ;
                  qryAux.SQL.Add('UPDATE ItemListaCobranca SET dDtEncerramento = GetDate() WHERE nCdItemListaCobranca = ' + intToStr(dsPropostanCdItemListaCobranca.Value)) ;
                  qryAux.ExecSQL;

              end ;

              // efetiva a grava��o da renegocia��o
              qryPropostaReneg.Post ;

              // vincula os t�tulos a negocia��o
              qryTituloPropostaReneg.Close;
              qryTituloPropostaReneg.Open;

              qryTitulos.Close;
              qryTitulos.Open;

              qryTitulos.First;

              while not qryTitulos.Eof do
              begin
                  qryTituloPropostaReneg.Insert;
                  qryTituloPropostaRenegnCdPropostaReneg.Value       := qryPropostaRenegnCdPropostaReneg.Value;
                  qryTituloPropostaRenegnCdTituloPropostaReneg.Value := frmMenu.fnProximoCodigo('TITULOPROPOSTARENEG') ;
                  qryTituloPropostaRenegnCdTitulo.Value              := qryTitulosnCdTitulo.Value ;
                  qryTituloPropostaRenegnSaldoTit.Value              := qryTitulosnSaldoTit.Value;
                  qryTituloPropostaReneg.Post;

                  qryTitulos.Next;
              end ;

              qryTitulos.Close;

          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          frmMenu.Connection.CommitTrans;

      end
      else
      begin
          MensagemErro('O Registro do contato n�o foi gravado. A renegocia��o n�o foi conclu�da!') ;
          abort ;
      end ;

  end
  else
  begin

      {-- quando n�o veio da lista de cobran�a, grava aqui a gera��o da proposta --}
      try

          // efetiva a grava��o da renegocia��o
          qryPropostaReneg.Post ;

          // vincula os t�tulos a negocia��o
          qryTituloPropostaReneg.Close;
          qryTituloPropostaReneg.Open;

          qryTitulos.Close;
          qryTitulos.Open;

          qryTitulos.First;

          while not qryTitulos.Eof do
          begin
              qryTituloPropostaReneg.Insert;
              qryTituloPropostaRenegnCdTituloPropostaReneg.Value := frmMenu.fnProximoCodigo('TITULOPROPOSTARENEG') ;
              qryTituloPropostaRenegnCdPropostaReneg.Value       := qryPropostaRenegnCdPropostaReneg.Value;
              qryTituloPropostaRenegnCdTitulo.Value              := qryTitulosnCdTitulo.Value ;
              qryTituloPropostaRenegnSaldoTit.Value              := qryTitulosnSaldoTit.Value;
              qryTituloPropostaReneg.Post;

              qryTitulos.Next;
          end ;

          qryTitulos.Close;

      except
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

  end ;

  ShowMessage('Renegocia��o conclu�da com sucesso!') ;

  if (cFlgDivergencia = 'S') then
      MensagemAlerta('Esta proposta cont�m diverg�ncias e dever� ser liberada.') ;

  Close ;

  FreeAndNil(objfrmCobranca_VisaoContato_Contato);
end;

procedure TfrmGeraPropostaRenegociacao_Simulacao.RegistraContato(
  cFlgForcaContato: boolean; cTextoSugerido:string);
begin

    objfrmCobranca_VisaoContato_Contato := TfrmCobranca_VisaoContato_Contato.Create(nil);

    objfrmCobranca_VisaoContato_Contato.cTextoSugerido       := cTextoSugerido ;
    objfrmCobranca_VisaoContato_Contato.cFlgForcaContato     := cFlgForcaContato;
    objfrmCobranca_VisaoContato_Contato.nCdItemListaCobranca := dsPropostanCdItemListaCobranca.Value ;

    PosicionaQuery(objfrmCobranca_VisaoContato_Contato.qryContatoCobranca,'0');
    objfrmCobranca_VisaoContato_Contato.NovoContato(dsPropostanCdTerceiro.Value);

end;



end.
