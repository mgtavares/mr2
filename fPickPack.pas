unit fPickPack;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  DB, ADODB;

type
  TfrmPickPack = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    RadioGroup1: TRadioGroup;
    qryPedidoAguardandoPickingResumido: TADOQuery;
    qryPedidoAguardandoPickingDetalhado: TADOQuery;
    qryPedidoAguardandoPickingDetalhadonCdPedido: TIntegerField;
    qryPedidoAguardandoPickingDetalhadodDtPedido: TDateTimeField;
    qryPedidoAguardandoPickingDetalhadodDtAutor: TDateTimeField;
    qryPedidoAguardandoPickingDetalhadodDtPrevEntIni: TDateTimeField;
    qryPedidoAguardandoPickingDetalhadodDtPrevEntFim: TDateTimeField;
    qryPedidoAguardandoPickingDetalhadoiDiasEntrega: TIntegerField;
    qryPedidoAguardandoPickingDetalhadocNmTipoPedido: TStringField;
    qryPedidoAguardandoPickingDetalhadocNmTerceiroEntrega: TStringField;
    qryPedidoAguardandoPickingDetalhadocNmTerceiroPagador: TStringField;
    qryPedidoAguardandoPickingDetalhadonCdProduto: TIntegerField;
    qryPedidoAguardandoPickingDetalhadocNmItem: TStringField;
    qryPedidoAguardandoPickingDetalhadocDescricaoTecnica: TMemoField;
    qryPedidoAguardandoPickingDetalhadonSaldoQtde: TBCDField;
    qryPedidoAguardandoPickingDetalhadonValSaldoItem: TBCDField;
    qryPedidoAguardandoPickingDetalhadocNmTabStatusPed: TStringField;
    qryPedidoAguardandoPickingResumidonCdPedido: TIntegerField;
    qryPedidoAguardandoPickingResumidodDtPedido: TDateTimeField;
    qryPedidoAguardandoPickingResumidodDtAutor: TDateTimeField;
    qryPedidoAguardandoPickingResumidodDtPrevEntIni: TDateTimeField;
    qryPedidoAguardandoPickingResumidodDtPrevEntFim: TDateTimeField;
    qryPedidoAguardandoPickingResumidoiDiasEntrega: TIntegerField;
    qryPedidoAguardandoPickingResumidocNmTipoPedido: TStringField;
    qryPedidoAguardandoPickingResumidocNmTerceiroEntrega: TStringField;
    qryPedidoAguardandoPickingResumidocNmTerceiroPagador: TStringField;
    qryPedidoAguardandoPickingResumidonValSaldoItem: TBCDField;
    qryPedidoAguardandoPickingResumidocNmTabStatusPed: TStringField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPickPack: TfrmPickPack;

implementation

{$R *.dfm}

end.
