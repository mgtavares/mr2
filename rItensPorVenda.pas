unit rItensPorVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, ADODB,
  DB, Mask, StdCtrls, DBCtrls, ER2Lookup, ER2Excel;

type
  TrptItensPorVenda = class(TfrmRelatorio_Padrao)
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit1: TDBEdit;
    dsLoja: TDataSource;
    qryVendedor: TADOQuery;
    DBEdit2: TDBEdit;
    dsVendedor: TDataSource;
    Label5: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    MaskEdit1: TMaskEdit;
    Label6: TLabel;
    MaskEdit2: TMaskEdit;
    qryVendedornCdUsuario: TIntegerField;
    qryVendedorcNmUsuario: TStringField;
    er2LkpLoja: TER2LookupMaskEdit;
    er2LkpVendedor: TER2LookupMaskEdit;
    RadioGroup1: TRadioGroup;
    rgModeloImp: TRadioGroup;
    ER2Excel: TER2Excel;
    procedure ToolButton1Click(Sender: TObject);
    procedure qryLojaBeforeOpen(DataSet: TDataSet);
    procedure qryVendedorBeforeOpen(DataSet: TDataSet);
    procedure er2LkpVendedorBeforeLookup(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptItensPorVenda: TrptItensPorVenda;
  cFiltro         : string;

implementation
uses fMenu, rItensPorVenda_view, fLookup_Padrao;
{$R *.dfm}

procedure TrptItensPorVenda.ToolButton1Click(Sender: TObject);
var
  objRel : TrptItensPorVenda_view;
  iLinha : integer;
begin
  inherited;

  if (MaskEdit1.Text = '  /  /    ') then
      MaskEdit1.Text := DateToStr(Now())
  else
  begin
      if (frmMenu.ConvData(MaskEdit1.Text) = frmMenu.ConvData('01/01/1900')) then
      begin
          MaskEdit1.Clear;
          MaskEdit1.SetFocus;
          Abort;
      end;
  end;

  if (MaskEdit2.Text = '  /  /    ') then
      MaskEdit2.Text := DateToStr(Now())
  else
  begin
      if (frmMenu.ConvData(MaskEdit2.Text) = frmMenu.ConvData('01/01/1900')) then
      begin
          MaskEdit2.Clear;
          MaskEdit2.SetFocus;
          Abort;
      end;
  end;

  if (frmMenu.ConvData(MaskEdit1.Text) > frmMenu.ConvData(MaskEdit2.Text)) then
  begin
      MensagemAlerta('Data inicial maior do que a data final.');
      MaskEdit1.SetFocus;
      Abort;
  end;

  objRel := TrptItensPorVenda_view.Create(nil);

  try
      try
          objRel.SPREL_ITENS_POR_VENDA.Close;
          objRel.SPREL_ITENS_POR_VENDA.Parameters.ParamByName('@nCdLoja').Value          := frmMenu.ConvInteiro(er2LkpLoja.Text);
          objRel.SPREL_ITENS_POR_VENDA.Parameters.ParamByName('@nCdTerceiroColab').Value := frmMenu.ConvInteiro(er2LkpVendedor.Text);
          objRel.SPREL_ITENS_POR_VENDA.Parameters.ParamByName('@dDtInicial').Value       := frmMenu.ConvData(MaskEdit1.Text);
          objRel.SPREL_ITENS_POR_VENDA.Parameters.ParamByName('@dDtFinal').Value         := frmMenu.ConvData(MaskEdit2.Text);
          objRel.SPREL_ITENS_POR_VENDA.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
          objRel.SPREL_ITENS_POR_VENDA.Parameters.ParamByName('@cFlgAtivo').Value        := RadioGroup1.ItemIndex;
          objRel.cmdPreparaTemp.Execute;
          objRel.SPREL_ITENS_POR_VENDA.ExecProc;

          objRel.qryResultado.Close;
          objRel.qryResultado.Open;

          {--envia filtros utilizados--}
          cFiltro := '';

          if (DBEdit1.Text <> '') then
            cFiltro := cFiltro + 'Loja: ' + DBEdit1.Text + '/ ';

          if (DBEdit2.Text <> '') then
            cFiltro := cFiltro + 'Vendedor: ' + DBEdit2.Text + '/ ';

          cFiltro := cFiltro  + 'Per�odo: ' + MaskEdit1.Text + ' � ' + MaskEdit2.Text;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
          objRel.lblFiltro1.Caption := cFiltro;

          {--visualiza o relat�rio--}
          case (rgModeloImp.ItemIndex) of
          0 : objRel.QuickRep1.PreviewModal;
          1 : begin

                  frmMenu.mensagemUsuario('Exportando Planilha...');

                  ER2Excel.Celula['A1'].Mesclar('S1');
                  ER2Excel.Celula['A2'].Mesclar('S2');
                  ER2Excel.Celula['A3'].Mesclar('S3');
                  ER2Excel.Celula['A4'].Mesclar('S4');
                  ER2Excel.Celula['G5'].Mesclar('J5');
                  ER2Excel.Celula['K5'].Mesclar('P5');
                  ER2Excel.Celula['Q5'].Mesclar('S5');
                  ER2Excel.Celula['B6'].Mesclar('F6');

                  ER2Excel.Celula['A1'].Background := RGB(221, 221, 221);
                  ER2Excel.Celula['A2'].Background := RGB(221, 221, 221);
                  ER2Excel.Celula['A3'].Background := RGB(221, 221, 221);
                  ER2Excel.Celula['A4'].Background := RGB(221, 221, 221);


                  { -- inseri informa��es do cabe�alho -- }
                  ER2Excel.Celula['A1'].Text := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio.';
                  ER2Excel.Celula['A2'].Text := 'Rel. Itens por Venda - Par';
                  ER2Excel.Celula['A3'].Text := 'Filtros: ' + cFiltro;
                  ER2Excel.Celula['G5'].Text := '* ------------ TOTAL GERAL DE VENDAS ------------*';
                  ER2Excel.Celula['K5'].Text := '* --------------------------------------- TOTAL DE VENDAS ---------------------------------------*';
                  ER2Excel.Celula['Q5'].Text := '* ------------- TOTAL DE TROCAS ------------- *';
                  ER2Excel.Celula['A6'].Text := 'C�digo';
                  ER2Excel.Celula['B6'].Text := 'Vendedor';
                  ER2Excel.Celula['G6'].Text := 'Total Geral';
                  ER2Excel.Celula['H6'].Text := '%Part';
                  ER2Excel.Celula['I6'].Text := 'Qt.';
                  ER2Excel.Celula['J6'].Text := 'Itens';
                  ER2Excel.Celula['K6'].Text := 'Total Venda';
                  ER2Excel.Celula['L6'].Text := 'Qt. Vd';
                  ER2Excel.Celula['M6'].Text := 'Itens';
                  ER2Excel.Celula['N6'].Text := 'Vd M�dia';
                  ER2Excel.Celula['O6'].Text := 'Itens Vd';
                  ER2Excel.Celula['P6'].Text := 'VM Item';
                  ER2Excel.Celula['Q6'].Text := 'Tot Troca';
                  ER2Excel.Celula['R6'].Text := 'Qt. Tr';
                  ER2Excel.Celula['S6'].Text := 'Itens';

                  iLinha := 7;

                  objRel.qryResultado.First;

                  while (not objRel.qryResultado.Eof) do
                  begin
                      ER2Excel.Celula['A' + IntToStr(iLinha)].Text := objRel.qryResultadonCdVendedor.AsString;
                      ER2Excel.Celula['B' + IntToStr(iLinha)].Text := objRel.qryResultadocNmVendedor.AsString; //'Vendedor';
                      ER2Excel.Celula['G' + IntToStr(iLinha)].Text := objRel.qryResultadonValPedidosGeral.AsString; //'Total Geral';
                      ER2Excel.Celula['H' + IntToStr(iLinha)].Text := objRel.qryResultadonPercentParticip.AsString; //'%Part';
                      ER2Excel.Celula['I' + IntToStr(iLinha)].Text := objRel.qryResultadoiTotalPedidosGeral.AsString; //'Qt.';
                      ER2Excel.Celula['J' + IntToStr(iLinha)].Text := objRel.qryResultadoiQtdItensGeral.AsString; //'Itens';
                      ER2Excel.Celula['K' + IntToStr(iLinha)].Text := objRel.qryResultadonValPedidosVenda.AsString; //'Total Venda';
                      ER2Excel.Celula['L' + IntToStr(iLinha)].Text := objRel.qryResultadoiQtdVenda.AsString; //'Qt. Vd';
                      ER2Excel.Celula['M' + IntToStr(iLinha)].Text := objRel.qryResultadoiQtdItensVenda.AsString; //'Itens';
                      ER2Excel.Celula['N' + IntToStr(iLinha)].Text := objRel.qryResultadonValMedio.AsString; //'Vd M�dia';
                      ER2Excel.Celula['O' + IntToStr(iLinha)].Text := objRel.qryResultadonItemPorVenda.AsString; //'Itens Vd';
                      ER2Excel.Celula['P' + IntToStr(iLinha)].Text := objRel.qryResultadonValItemMedio.AsString; //'VM Item';
                      ER2Excel.Celula['Q' + IntToStr(iLinha)].Text := objRel.qryResultadonValPedidosTroca.AsString; //'Tot Troca';
                      ER2Excel.Celula['R' + IntToStr(iLinha)].Text := objRel.qryResultadoiTotalTrocas.AsString; //'Qt. Tr';
                      ER2Excel.Celula['S' + IntToStr(iLinha)].Text := objRel.qryResultadoiQtdItensTrocas.AsString; //'Itens';
                      
                      objRel.qryResultado.Next;

                      Inc(iLinha);

                  end;

                  { -- exporta planilha e limpa result do componente -- }
                  ER2Excel.ExportXLS;
                  ER2Excel.CleanupInstance;

                  frmMenu.mensagemUsuario('');

              end;
          end;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end ;

  finally
      FreeAndNil(objRel) ;
  end;
end;

procedure TrptItensPorVenda.qryLojaBeforeOpen(DataSet: TDataSet);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

procedure TrptItensPorVenda.qryVendedorBeforeOpen(DataSet: TDataSet);
begin
  inherited;

  if (Trim(DBEdit1.Text) = '') then
  begin
      MensagemAlerta('Para selecionar um vendedor, selecione primeiro uma loja.');
      er2LkpVendedor.Clear;
      er2LkpLoja.SetFocus;
      Abort;
  end;

  qryVendedor.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value;

  er2LkpVendedor.WhereAdicional.Text := 'EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdUsuario = Usuario.nCdUsuario AND UL.nCdLoja = ' + Trim(er2LkpLoja.Text) + ')' + ' ORDER BY Usuario.cNmUsuario';
end;

procedure TrptItensPorVenda.er2LkpVendedorBeforeLookup(Sender: TObject);
begin
  inherited;
  qryVendedorBeforeOpen(qryVendedor);
end;

initialization
    RegisterClass(TrptItensPorVenda) ;

end.
