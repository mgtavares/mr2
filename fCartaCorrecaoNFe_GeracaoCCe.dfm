inherited frmCartaCorrecaoNFe_GeracaoCCe: TfrmCartaCorrecaoNFe_GeracaoCCe
  Left = 275
  Top = 60
  Width = 787
  Height = 624
  BorderIcons = []
  Caption = 'Gera'#231#227'o CC-e'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 771
    Height = 557
  end
  inherited ToolBar1: TToolBar
    Width = 771
    TabOrder = 1
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object GroupBox2: TGroupBox [2]
    Left = 8
    Top = 37
    Width = 753
    Height = 164
    Align = alCustom
    Caption = ' Dados do Documento Fiscal '
    Color = 13086366
    ParentColor = False
    TabOrder = 0
    object Label1: TLabel
      Tag = 1
      Left = 26
      Top = 38
      Width = 58
      Height = 13
      Alignment = taRightJustify
      Caption = 'C'#243'digo Doc.'
    end
    object Label2: TLabel
      Tag = 1
      Left = 175
      Top = 38
      Width = 37
      Height = 13
      Alignment = taRightJustify
      Caption = 'N'#250'mero'
    end
    object Label3: TLabel
      Tag = 1
      Left = 327
      Top = 38
      Width = 24
      Height = 13
      Alignment = taRightJustify
      Caption = 'S'#233'rie'
    end
    object Label4: TLabel
      Tag = 1
      Left = 425
      Top = 38
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Emiss'#227'o'
    end
    object Label5: TLabel
      Tag = 1
      Left = 31
      Top = 90
      Width = 53
      Height = 13
      Alignment = taRightJustify
      Caption = 'Chave NFe'
    end
    object Label6: TLabel
      Tag = 1
      Left = 16
      Top = 64
      Width = 68
      Height = 13
      Alignment = taRightJustify
      Caption = 'Entrada/Sa'#237'da'
    end
    object Label7: TLabel
      Tag = 1
      Left = 576
      Top = 38
      Width = 76
      Height = 13
      Alignment = taRightJustify
      Caption = 'Data Impress'#227'o'
    end
    object Label8: TLabel
      Tag = 1
      Left = 161
      Top = 64
      Width = 51
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Total'
    end
    object Label9: TLabel
      Tag = 1
      Left = 45
      Top = 116
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro'
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 88
      Top = 30
      Width = 65
      Height = 21
      DataField = 'nCdDoctoFiscal'
      DataSource = dsDoctoFiscal
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 216
      Top = 30
      Width = 81
      Height = 21
      DataField = 'iNrDocto'
      DataSource = dsDoctoFiscal
      TabOrder = 1
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 355
      Top = 30
      Width = 40
      Height = 21
      DataField = 'cSerie'
      DataSource = dsDoctoFiscal
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 466
      Top = 30
      Width = 80
      Height = 21
      DataField = 'dDtEmissao'
      DataSource = dsDoctoFiscal
      TabOrder = 3
    end
    object DBEdit8: TDBEdit
      Tag = 1
      Left = 88
      Top = 82
      Width = 649
      Height = 21
      DataField = 'cChaveNFe'
      DataSource = dsDoctoFiscal
      TabOrder = 7
    end
    object DBEdit6: TDBEdit
      Tag = 1
      Left = 88
      Top = 56
      Width = 65
      Height = 21
      DataField = 'cFlgEntSai'
      DataSource = dsDoctoFiscal
      TabOrder = 5
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 656
      Top = 30
      Width = 80
      Height = 21
      DataField = 'dDtImpressao'
      DataSource = dsDoctoFiscal
      TabOrder = 4
    end
    object DBEdit7: TDBEdit
      Tag = 1
      Left = 216
      Top = 56
      Width = 81
      Height = 21
      DataField = 'nValTotal'
      DataSource = dsDoctoFiscal
      TabOrder = 6
    end
    object DBEdit9: TDBEdit
      Tag = 1
      Left = 88
      Top = 109
      Width = 65
      Height = 21
      DataField = 'nCdTerceiro'
      DataSource = dsDoctoFiscal
      TabOrder = 8
    end
    object DBEdit10: TDBEdit
      Tag = 1
      Left = 154
      Top = 109
      Width = 583
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = dsDoctoFiscal
      TabOrder = 9
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 8
    Top = 208
    Width = 753
    Height = 129
    Caption = ' Condi'#231#245'es de uso da Carta de Corre'#231#227'o '
    Color = 13086366
    ParentColor = False
    TabOrder = 2
    object Memo1: TMemo
      Left = 8
      Top = 24
      Width = 737
      Height = 95
      TabStop = False
      BorderStyle = bsNone
      Color = 13086366
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Lines.Strings = (
        
          'A Carta de Corre'#231#227'o '#233' disciplinada pelo '#167' 1'#186'-A do art. 7'#186' do Con' +
          'v'#234'nio S/N, de 15 de dezembro de 1970 e pode ser utilizada para r' +
          'egulariza'#231#227'o de erro '
        
          'ocorrido na emiss'#227'o de documento fiscal, desde que o erro n'#227'o es' +
          'teja relacionado com: '
        ''
        
          'I - as vari'#225'veis que determinam o valor do imposto tais como: ba' +
          'se de c'#225'lculo, al'#237'quota, diferen'#231'a de pre'#231'o, quantidade, valor d' +
          'a opera'#231#227'o ou da '
        'presta'#231#227'o; '
        
          'II - a corre'#231#227'o de dados cadastrais que implique mudan'#231'a do reme' +
          'tente ou do destinat'#225'rio; '
        'III - a data de emiss'#227'o ou de sa'#237'da.')
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
    end
  end
  object GroupBox3: TGroupBox [4]
    Left = 8
    Top = 344
    Width = 753
    Height = 233
    Caption = ' Dados CC-e '
    Color = 13086366
    ParentColor = False
    TabOrder = 3
    object Label11: TLabel
      Tag = 1
      Left = 8
      Top = 28
      Width = 142
      Height = 13
      Alignment = taRightJustify
      Caption = 'Sequ'#234'ncia do Evento (1 a 20)'
    end
    object Label12: TLabel
      Tag = 1
      Left = 233
      Top = 28
      Width = 21
      Height = 13
      Alignment = taRightJustify
      Caption = 'Lote'
      Visible = False
    end
    object Label13: TLabel
      Tag = 1
      Left = 9
      Top = 212
      Width = 101
      Height = 13
      Alignment = taRightJustify
      Caption = 'm'#237'nimo 15 caracteres'
    end
    object Label10: TLabel
      Tag = 1
      Left = 11
      Top = 58
      Width = 79
      Height = 13
      Caption = 'Motivo Corre'#231#227'o'
    end
    object mskSequencia: TMaskEdit
      Left = 158
      Top = 20
      Width = 57
      Height = 21
      TabStop = False
      EditMask = '##;1; '
      MaxLength = 2
      TabOrder = 2
      Text = '1 '
    end
    object mskLote: TMaskEdit
      Left = 262
      Top = 20
      Width = 61
      Height = 21
      EditMask = '##;1; '
      MaxLength = 2
      TabOrder = 1
      Text = '1 '
      Visible = False
    end
    object xCorrecao: TMemo
      Left = 8
      Top = 72
      Width = 737
      Height = 137
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      MaxLength = 1000
      ParentFont = False
      TabOrder = 0
    end
  end
  inherited ImageList1: TImageList
    Left = 344
    Top = 432
  end
  object qryDoctoFiscal: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDoctoFiscal'
      '      ,iNrDocto'
      '      ,cSerie'
      '      ,dDtEmissao'
      '      ,cChaveNFe'
      '      ,cFlgEntSai'
      '      ,dDtImpressao'
      '      ,nValTotal'
      '      ,DoctoFiscal.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,cCNPJCPFEmitente'
      '  FROM DoctoFiscal'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = DoctoFiscal' +
        '.nCdTerceiro'
      ' WHERE nCdDoctoFiscal = :nPK')
    Left = 344
    Top = 397
    object qryDoctoFiscalnCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryDoctoFiscaliNrDocto: TIntegerField
      FieldName = 'iNrDocto'
    end
    object qryDoctoFiscalcSerie: TStringField
      FieldName = 'cSerie'
      FixedChar = True
      Size = 3
    end
    object qryDoctoFiscaldDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryDoctoFiscalcChaveNFe: TStringField
      FieldName = 'cChaveNFe'
      Size = 100
    end
    object qryDoctoFiscalcFlgEntSai: TStringField
      Alignment = taCenter
      FieldName = 'cFlgEntSai'
      FixedChar = True
      Size = 1
    end
    object qryDoctoFiscaldDtImpressao: TDateTimeField
      FieldName = 'dDtImpressao'
    end
    object qryDoctoFiscalnValTotal: TBCDField
      FieldName = 'nValTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDoctoFiscalnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryDoctoFiscalcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryDoctoFiscalcCNPJCPFEmitente: TStringField
      FieldName = 'cCNPJCPFEmitente'
      Size = 14
    end
  end
  object dsDoctoFiscal: TDataSource
    DataSet = qryDoctoFiscal
    Left = 312
    Top = 432
  end
  object qryCartaCorrecaoNFe: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT * '
      '  FROM CartaCorrecaoNFe')
    Left = 312
    Top = 400
    object qryCartaCorrecaoNFenCdCartaCorrecaoNFe: TIntegerField
      FieldName = 'nCdCartaCorrecaoNFe'
    end
    object qryCartaCorrecaoNFenCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryCartaCorrecaoNFedDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryCartaCorrecaoNFecCorrecao: TStringField
      FieldName = 'cCorrecao'
      Size = 1000
    end
    object qryCartaCorrecaoNFeiSeq: TIntegerField
      FieldName = 'iSeq'
    end
    object qryCartaCorrecaoNFeiLote: TIntegerField
      FieldName = 'iLote'
    end
    object qryCartaCorrecaoNFecCaminhoXML: TStringField
      FieldName = 'cCaminhoXML'
      Size = 200
    end
    object qryCartaCorrecaoNFecArquivoXML: TStringField
      FieldName = 'cArquivoXML'
      Size = 200
    end
    object qryCartaCorrecaoNFecChaveNFe: TStringField
      FieldName = 'cChaveNFe'
      Size = 200
    end
    object qryCartaCorrecaoNFenCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryCartaCorrecaoNFedDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryCartaCorrecaoNFecNrProtocoloCCe: TStringField
      FieldName = 'cNrProtocoloCCe'
      Size = 100
    end
    object qryCartaCorrecaoNFecXMLCCe: TMemoField
      FieldName = 'cXMLCCe'
      BlobType = ftMemo
    end
  end
  object qryVerificaSeqCCe: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT (MAX(iSeq) + 1) as iSeq'
      '  FROM CartaCorrecaoNFe'
      ' WHERE nCdDoctoFiscal = :nPK')
    Left = 280
    Top = 400
    object qryVerificaSeqCCeiSeq: TIntegerField
      FieldName = 'iSeq'
      ReadOnly = True
    end
  end
end
