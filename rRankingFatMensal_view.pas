unit rRankingFatMensal_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptRankingFatMensal_view = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRDBText13: TQRDBText;
    QRCSVFilter1: TQRCSVFilter;
    QRTextFilter1: TQRTextFilter;
    QRCompositeReport1: TQRCompositeReport;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel18: TQRLabel;
    QRDBText1: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel7: TQRLabel;
    QRDBText9: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText10: TQRDBText;
    QRLabel9: TQRLabel;
    QRDBText12: TQRDBText;
    SummaryBand1: TQRBand;
    QRExpr1: TQRExpr;
    QRShape2: TQRShape;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRExpr8: TQRExpr;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    QRExpr11: TQRExpr;
    QRExpr12: TQRExpr;
    QRExpr13: TQRExpr;
    QRExpr14: TQRExpr;
    usp_RelatorioiPosicao: TIntegerField;
    usp_RelatorionCdTerceiro: TIntegerField;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatorionValDia1: TFloatField;
    usp_RelatorionValDia2: TFloatField;
    usp_RelatorionValDia3: TFloatField;
    usp_RelatorionValDia4: TFloatField;
    usp_RelatorionValDia5: TFloatField;
    usp_RelatorionValDia6: TFloatField;
    usp_RelatorionValDia7: TFloatField;
    usp_RelatorionValDia8: TFloatField;
    usp_RelatorionValDia9: TFloatField;
    usp_RelatorionValDia10: TFloatField;
    usp_RelatorionValDia11: TFloatField;
    usp_RelatorionValDia12: TFloatField;
    usp_RelatorionValDia13: TFloatField;
    usp_RelatorionValDia14: TFloatField;
    usp_RelatorionValDia15: TFloatField;
    usp_RelatorionValDia16: TFloatField;
    usp_RelatorionValDia17: TFloatField;
    usp_RelatorionValDia18: TFloatField;
    usp_RelatorionValDia19: TFloatField;
    usp_RelatorionValDia20: TFloatField;
    usp_RelatorionValDia21: TFloatField;
    usp_RelatorionValDia22: TFloatField;
    usp_RelatorionValDia23: TFloatField;
    usp_RelatorionValDia24: TFloatField;
    usp_RelatorionValDia25: TFloatField;
    usp_RelatorionValDia26: TFloatField;
    usp_RelatorionValDia27: TFloatField;
    usp_RelatorionValDia28: TFloatField;
    usp_RelatorionValDia29: TFloatField;
    usp_RelatorionValDia30: TFloatField;
    usp_RelatorionValDia31: TFloatField;
    usp_RelatorionValTotal: TFloatField;
    usp_RelatorionVariacaoTotal: TFloatField;
    usp_RelatorionMediaMensal: TFloatField;
    usp_RelatorionPercPart: TFloatField;
    usp_RelatorionValAcumulado: TFloatField;
    usp_RelatorionPerc2: TFloatField;
    usp_RelatoriocCurva: TStringField;
    QRDBText14: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRDBText33: TQRDBText;
    QRDBText34: TQRDBText;
    QRDBText35: TQRDBText;
    QRDBText36: TQRDBText;
    QRDBText37: TQRDBText;
    QRDBText38: TQRDBText;
    QRDBText39: TQRDBText;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRLabel44: TQRLabel;
    QRLabel45: TQRLabel;
    QRLabel46: TQRLabel;
    QRLabel47: TQRLabel;
    QRExpr15: TQRExpr;
    QRExpr16: TQRExpr;
    QRExpr17: TQRExpr;
    QRExpr18: TQRExpr;
    QRImage1: TQRImage;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRShape5: TQRShape;
    QRShape6: TQRShape;
    QRShape3: TQRShape;
    QRShape1: TQRShape;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRankingFatMensal_view: TrptRankingFatMensal_view;

implementation

{$R *.dfm}

end.
