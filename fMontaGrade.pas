unit fMontaGrade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Grids, GridsEh, DBGridEh, DB, DBClient, ADODB,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmMontaGrade = class(TForm)
    DataSource1: TDataSource;
    DBGridEh1: TDBGridEh;
    cdsQuantidade: TClientDataSet;
    qryGrade: TADOQuery;
    qryGradenCdItemGrade: TAutoIncField;
    qryGradenCdGrade: TIntegerField;
    qryGradeiTamanho: TIntegerField;
    qryGradecNmTamanho: TStringField;
    cdsCodigo: TClientDataSet;
    DataSource2: TDataSource;
    DBGridEh2: TDBGridEh;
    qryCodigoProduto: TADOQuery;
    qryCodigoProdutonCdProduto: TIntegerField;
    qryCodigoProdutoiTamanho: TIntegerField;
    qryValorAnterior: TADOQuery;
    procedure PreparaDataSet(nCdProduto: Integer) ;
    procedure Renderiza() ;
    procedure cdsQuantidadeAfterPost(DataSet: TDataSet);
    procedure cdsQuantidadeAfterCancel(DataSet: TDataSet);
    procedure LiberaGrade();
  private
    { Private declarations }
  public
    { Public declarations }
    nQtdeTotal : integer ;
    bMontada   : boolean ;
  end;

var
  frmMontaGrade: TfrmMontaGrade;

implementation

uses fMenu;

{$R *.dfm}


procedure TfrmMontaGrade.Renderiza() ;
var
  i     : integer ;
begin

    DBGridEh1.AutoFitColWidths := True ;

    if (cdsQuantidade.RecordCount = 0) then
    begin
        cdsQuantidade.Insert ;
    end 
    else
    begin
        cdsQuantidade.First ;
        cdsQuantidade.Edit ;
    end ;

    Self.ShowModal ;

    nQtdeTotal := 0 ;

    cdsQuantidade.First;

    for i := 0 to cdsQuantidade.FieldList.Count-1 do
    begin

        if (cdsQuantidade.FieldList[i].AsString = '') then
        begin
            cdsQuantidade.Edit ;
            cdsQuantidade.FieldList[i].Value := '0';
            cdsQuantidade.Post ;
        end ;

        nQtdeTotal := nQtdeTotal + StrToInt(cdsQuantidade.FieldList[i].Value) ;
    end ;

end ;

procedure TfrmMontaGrade.LiberaGrade();
begin
    if (cdsQuantidade.Active) and (cdsQuantidade.RecordCount > 0) then
        cdsQuantidade.Delete ;

    if (cdsCodigo.Active) and (cdsCodigo.RecordCount > 0) then
        cdsCodigo.Delete ;

    cdsQuantidade.Close;
    cdsCodigo.Close ;

    bMontada := False ;
end ;

procedure TfrmMontaGrade.PreparaDataSet(nCdProduto: Integer) ;
var
    i : integer ;
begin

    frmMenu.mensagemUsuario('Executando qry Grade');

    qryGrade.Close ;
    qryGrade.Parameters.ParamByName('nCdProduto').Value := nCdProduto ;
    qryGrade.Open ;

    qryGrade.First ;

    cdsQuantidade.Close;

    frmMenu.mensagemUsuario('Looping cdsQuantidade');

    with cdsQuantidade.FieldDefs do
    begin
        Clear;
        while not qryGrade.Eof do
        begin
            Add(qryGradeiTamanho.AsString,ftInteger,0,False);
            qryGrade.Next ;
        end ;
    end;

    cdsQuantidade.CreateDataSet;

    qryGrade.First ;

    // altera o display label das colunas do data set da quantidade
    while not qryGrade.Eof do
    begin
        cdsQuantidade.FieldList[qryGrade.RecNo-1].DisplayLabel := qryGradecNmTamanho.Value ;
        qryGrade.Next ;
    end ;


    // monta o dataset dos c�digos
    qryGrade.First ;

    cdsCodigo.Close;

    with cdsCodigo.FieldDefs do
    begin
        Clear;
        while not qryGrade.Eof do
        begin
            Add(qryGradeiTamanho.AsString,ftInteger,0,False);
            qryGrade.Next ;
        end ;
    end;

    cdsCodigo.CreateDataSet;

    frmMenu.mensagemUsuario('atualiza o c�digo dos produtos');

    // atualiza o c�digo dos produtos da grade no dataset de codigos
    qryCodigoProduto.Close ;
    qryCodigoProduto.Parameters.ParamByName('nCdProduto').Value := nCdProduto ;
    qryCodigoProduto.Open ;

    if (qryCodigoProduto.eof) then
    begin
        ShowMessage('ERRO DE GRADE: Sequ�ncia de c�digo n�o encontrada ou produtos gerados divergente da sequ�ncia da grade.') ;
        abort ;
    end ;

    qryCodigoProduto.First ;

    cdsCodigo.Insert ;

    while not qryCodigoProduto.eof do
    begin
        try
            cdsCodigo.FieldList.FieldByName(qryCodigoProdutoiTamanho.AsString).Value := qryCodigoProdutonCdProduto.Value ;
        except
            ShowMessage('N�o foi poss�vel montar a grade.  ID: ' + intToStr(qryCodigoProduto.RecNo -1) + ' Tamanho: ' + qryCodigoProdutoiTamanho.AsString) ;
            raise ;
        end ;
        
        qryCodigoProduto.Next ;
    end ;

    cdsCodigo.Post ;

    //atribui os valores anteriores quando o registro for alterado
    qryValorAnterior.Close ;
    if (qryValorAnterior.SQL.Text <> '') then
    begin

        qryValorAnterior.Open  ;

        qryValorAnterior.First ;

        // verifica todos os valores da grade que vierem anteriores
        while not qryValorAnterior.eof do
        begin

            cdsCodigo.First ;

            for i := 0 to (cdsCodigo.Fields.Count-1) do
            begin

                //se o valor da coluna do dataset cdsCodigo for igual ao codigo do produto
                //da query qryValoranterior, grava a quantidade no dataset cdsQuantidade
                if (cdsCodigo.FieldList[i].asString = qryValorAnterior.FieldList[0].asString) then
                begin

                    cdsQuantidade.First ;
                    cdsQuantidade.Edit ;
                    cdsQuantidade.FieldList[i].Value := qryValorAnterior.FieldList[1].AsString ;
                    cdsQuantidade.Post ;
                    
                end ;

            end ;

            qryValorAnterior.Next ;

        end ;

    end ;

    cdsQuantidade.First;

    for i := 0 to cdsQuantidade.FieldList.Count-1 do
    begin

        if (cdsQuantidade.FieldList[i].AsString = '') then
        begin
            cdsQuantidade.Edit ;
            cdsQuantidade.FieldList[i].Value := '0';
            cdsQuantidade.Post ;
        end ;

        nQtdeTotal := nQtdeTotal + StrToInt(cdsQuantidade.FieldList[i].Value) ;
    end ;
    

    bMontada := True ;

end;

procedure TfrmMontaGrade.cdsQuantidadeAfterPost(DataSet: TDataSet);
begin
    Close ;
end;

procedure TfrmMontaGrade.cdsQuantidadeAfterCancel(DataSet: TDataSet);
begin
    Close ;
end;



end.
