inherited frmNaturezaOperacao: TfrmNaturezaOperacao
  Left = 261
  Top = 206
  Width = 892
  Height = 337
  Caption = 'Natureza Opera'#231#227'o'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 876
    Height = 274
  end
  object Label1: TLabel [1]
    Left = 91
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 80
    Top = 62
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 60
    Top = 86
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'CFOP Interno'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 59
    Top = 110
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'CFOP Externo'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 54
    Top = 134
    Width = 75
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Opera'#231#227'o'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 56
    Top = 158
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'Mensagem NF'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 13
    Top = 182
    Width = 116
    Height = 13
    Alignment = taRightJustify
    Caption = 'V'#237'nculo Nat. Opera'#231#227'o'
    FocusControl = DBEdit6
  end
  inherited ToolBar2: TToolBar
    Width = 876
    TabOrder = 13
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [9]
    Tag = 1
    Left = 136
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdNaturezaOperacao'
    DataSource = dsMaster
    TabOrder = 0
  end
  object DBEdit2: TDBEdit [10]
    Left = 136
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmNaturezaOperacao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit3: TDBEdit [11]
    Left = 136
    Top = 80
    Width = 52
    Height = 19
    DataField = 'cCFOPInterno'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit4: TDBEdit [12]
    Left = 136
    Top = 104
    Width = 52
    Height = 19
    DataField = 'cCFOPExterno'
    DataSource = dsMaster
    TabOrder = 3
  end
  object rgOperacao: TDBRadioGroup [13]
    Left = 195
    Top = 83
    Width = 145
    Height = 38
    Caption = ' Opera'#231#227'o '
    Columns = 2
    DataField = 'cFlgEntSai'
    DataSource = dsMaster
    Items.Strings = (
      'Entrada'
      'Sa'#237'da')
    TabOrder = 4
    Values.Strings = (
      'E'
      'S')
    OnChange = rgOperacaoChange
  end
  object DBCheckBox1: TDBCheckBox [14]
    Left = 616
    Top = 131
    Width = 185
    Height = 17
    Caption = 'Opera'#231#227'o envolvendo terceiros'
    DataField = 'cFlgOpTerceiro'
    DataSource = dsMaster
    TabOrder = 7
    ValueChecked = '1'
    ValueUnchecked = '0'
    OnClick = DBCheckBox1Click
  end
  object DBEdit5: TDBEdit [15]
    Left = 136
    Top = 128
    Width = 65
    Height = 19
    DataField = 'nCdTipoOpTerceiro'
    DataSource = dsMaster
    TabOrder = 5
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [16]
    Left = 136
    Top = 152
    Width = 650
    Height = 19
    DataField = 'cMsgNF'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit7: TDBEdit [17]
    Tag = 1
    Left = 204
    Top = 128
    Width = 405
    Height = 19
    DataField = 'cNmTipoOpTerceiro'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit9: TDBEdit [18]
    Tag = 1
    Left = 204
    Top = 176
    Width = 488
    Height = 19
    DataField = 'cNmNaturezaOperacao'
    DataSource = dsVinculoNatOperacao
    TabOrder = 10
  end
  object DBEdit10: TDBEdit [19]
    Tag = 1
    Left = 695
    Top = 176
    Width = 91
    Height = 19
    DataField = 'cTipoNatOperacao'
    DataSource = dsVinculoNatOperacao
    TabOrder = 11
  end
  object er2LkpNaturezaOperacaoEntSai: TER2LookupDBEdit [20]
    Left = 136
    Top = 176
    Width = 65
    Height = 19
    DataField = 'nCdNaturezaOperacaoEntSai'
    DataSource = dsMaster
    TabOrder = 9
    OnExit = er2LkpNaturezaOperacaoEntSaiExit
    CodigoLookup = 74
    WhereAdicional.Strings = (
      'cFlgEntSai <>')
    QueryLookup = qryVinculoNatOperacao
  end
  object rgOpcao: TGroupBox [21]
    Left = 136
    Top = 200
    Width = 650
    Height = 57
    Caption = ' Op'#231#245'es '
    TabOrder = 12
    object DBCheckBox2: TDBCheckBox
      Left = 16
      Top = 24
      Width = 97
      Height = 17
      Caption = 'Calcular ICMS'
      DataField = 'cFlgCalcICMS'
      DataSource = dsMaster
      TabOrder = 0
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox3: TDBCheckBox
      Left = 120
      Top = 24
      Width = 81
      Height = 17
      Caption = 'Calcular IPI'
      DataField = 'cFlgCalcIPI'
      DataSource = dsMaster
      TabOrder = 1
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM NaturezaOperacao'
      ' WHERE nCdNaturezaOperacao = :nPK')
    Left = 632
    Top = 224
    object qryMasternCdNaturezaOperacao: TIntegerField
      FieldName = 'nCdNaturezaOperacao'
    end
    object qryMastercNmNaturezaOperacao: TStringField
      FieldName = 'cNmNaturezaOperacao'
      Size = 50
    end
    object qryMastercCFOPInterno: TStringField
      FieldName = 'cCFOPInterno'
      FixedChar = True
      Size = 4
    end
    object qryMastercCFOPExterno: TStringField
      FieldName = 'cCFOPExterno'
      FixedChar = True
      Size = 4
    end
    object qryMastercFlgEntSai: TStringField
      FieldName = 'cFlgEntSai'
      FixedChar = True
      Size = 1
    end
    object qryMastercFlgOpTerceiro: TIntegerField
      FieldName = 'cFlgOpTerceiro'
    end
    object qryMasternCdTipoOpTerceiro: TIntegerField
      FieldName = 'nCdTipoOpTerceiro'
    end
    object qryMastercFlgCalcICMS: TIntegerField
      FieldName = 'cFlgCalcICMS'
    end
    object qryMastercFlgCalcIPI: TIntegerField
      FieldName = 'cFlgCalcIPI'
    end
    object qryMastercMsgNF: TStringField
      FieldName = 'cMsgNF'
      Size = 150
    end
    object qryMastercNmTipoOpTerceiro: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmTipoOpTerceiro'
      LookupDataSet = qryTipoOpTerceiro
      LookupKeyFields = 'nCdTipoOpTerceiro'
      LookupResultField = 'cNmTipoOpTerceiro'
      KeyFields = 'nCdTipoOpTerceiro'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMasternCdNaturezaOperacaoEntSai: TIntegerField
      FieldName = 'nCdNaturezaOperacaoEntSai'
    end
  end
  inherited dsMaster: TDataSource
    Left = 632
    Top = 256
  end
  inherited qryID: TADOQuery
    Left = 696
    Top = 224
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 728
    Top = 224
  end
  inherited qryStat: TADOQuery
    Left = 696
    Top = 256
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 728
    Top = 256
  end
  inherited ImageList1: TImageList
    Left = 760
    Top = 224
  end
  object qryTipoOpTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM TipoOpTerceiro')
    Left = 760
    Top = 256
    object qryTipoOpTerceironCdTipoOpTerceiro: TIntegerField
      FieldName = 'nCdTipoOpTerceiro'
    end
    object qryTipoOpTerceirocNmTipoOpTerceiro: TStringField
      FieldName = 'cNmTipoOpTerceiro'
      Size = 50
    end
  end
  object qryVinculoNatOperacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cFlgEntSai'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdNaturezaOperacao'
      '      ,cNmNaturezaOperacao'
      '      ,cFlgEntSai'
      '      ,CASE WHEN (cFlgEntSai = '#39'E'#39')'
      '            THEN '#39'ENTRADA'#39
      '            ELSE '#39'SA'#205'DA'#39
      '       END cTipoNatOperacao'
      '  FROM NaturezaOperacao'
      ' WHERE nCdNaturezaOperacao  = :nPK'
      '   AND cFlgEntSai          <> :cFlgEntSai')
    Left = 664
    Top = 224
    object qryVinculoNatOperacaonCdNaturezaOperacao: TIntegerField
      FieldName = 'nCdNaturezaOperacao'
    end
    object qryVinculoNatOperacaocNmNaturezaOperacao: TStringField
      FieldName = 'cNmNaturezaOperacao'
      Size = 50
    end
    object qryVinculoNatOperacaocTipoNatOperacao: TStringField
      FieldName = 'cTipoNatOperacao'
      ReadOnly = True
      Size = 7
    end
    object qryVinculoNatOperacaocFlgEntSai: TStringField
      FieldName = 'cFlgEntSai'
      FixedChar = True
      Size = 1
    end
  end
  object dsVinculoNatOperacao: TDataSource
    DataSet = qryVinculoNatOperacao
    Left = 664
    Top = 256
  end
end
