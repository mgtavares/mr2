unit rRomaneio_Retrato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, QuickRpt, QRCtrls, jpeg;

type
  TrptRomaneio_Retrato = class(TForm)
    QuickRep1: TQuickRep;
    qryDoctoFiscal: TADOQuery;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand2: TQRBand;
    QRBand3: TQRBand;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText27: TQRDBText;
    QRShape3: TQRShape;
    QRShape4: TQRShape;
    QRDBText3: TQRDBText;
    QRDBText7: TQRDBText;
    QRLabel15: TQRLabel;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText8: TQRDBText;
    QRLabel18: TQRLabel;
    QRDBText10: TQRDBText;
    QRImage2: TQRImage;
    QRLabel32: TQRLabel;
    QRLabel35: TQRLabel;
    QRShape7: TQRShape;
    QRShape8: TQRShape;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText9: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel14: TQRLabel;
    QRShape5: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel11: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel7: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    qryDoctoFiscalcSerie: TStringField;
    qryDoctoFiscalcTextoCFOP: TStringField;
    qryDoctoFiscaliNrDocto: TIntegerField;
    qryDoctoFiscaldDtEmissao: TDateTimeField;
    qryDoctoFiscalcNmTerceiro: TStringField;
    qryDoctoFiscalcEndereco: TStringField;
    qryDoctoFiscalcBairro: TStringField;
    qryDoctoFiscalnCdPedido: TStringField;
    qryDoctoFiscaldDtPedido: TDateTimeField;
    qryDoctoFiscalnCdItemPedido: TStringField;
    qryDoctoFiscalnCdProduto: TIntegerField;
    qryDoctoFiscalcNmItem: TStringField;
    qryDoctoFiscalnQtde: TBCDField;
    qryDoctoFiscalcCFOP: TStringField;
    qryDoctoFiscalnCdTipoItemPed: TIntegerField;
    qryDoctoFiscalcComposicao: TStringField;
    qryDoctoFiscalnValUnitario: TBCDField;
    qryDoctoFiscalnValUnitarioPed: TBCDField;
    qryDoctoFiscalnValDesconto: TBCDField;
    qryDoctoFiscalnValTotal: TBCDField;
    QRDBText11: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText12: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText13: TQRDBText;
    QRLabel13: TQRLabel;
    qryDoctoFiscalcUnidadeMedida: TStringField;
    QRDBText14: TQRDBText;
    qryDoctoFiscalnValunitarioTotal: TBCDField;
    qryDoctoFiscalnValDescontoTotal: TBCDField;
    qryDoctoFiscalnValTotalDocto: TBCDField;
    qryDoctoFiscalnValProduto: TBCDField;
    qryDoctoFiscalnValIPI: TBCDField;
    qryDoctoFiscalnValICMSSub: TBCDField;
    QRDBText15: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabel20: TQRLabel;
    QRDBText17: TQRDBText;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel23: TQRLabel;
    QRDBText20: TQRDBText;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRDBText21: TQRDBText;
    qryDoctoFiscalnValFatura: TBCDField;
    qryDoctoFiscalnCdCondPagto: TIntegerField;
    qryDoctoFiscalcNmCondPagto: TStringField;
    QRLabel26: TQRLabel;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRLabel27: TQRLabel;
    lblRepresentante: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRomaneio_Retrato: TrptRomaneio_Retrato;

implementation

{$R *.dfm}

end.
