unit rPickList_Retrato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, QuickRpt, QRCtrls, jpeg;

type
  TrptPickList_Retrato = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRGroup1: TQRGroup;
    QRDBText5: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel7: TQRLabel;
    QRBand2: TQRBand;
    QRBand3: TQRBand;
    QRDBText29: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText27: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel11: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel13: TQRLabel;
    QRShape4: TQRShape;
    QRTotalPagina: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel10: TQRLabel;
    QRDBText3: TQRDBText;
    QRGroup2: TQRGroup;
    QRDBText11: TQRDBText;
    QRLabel15: TQRLabel;
    QRShape1: TQRShape;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRBand4: TQRBand;
    QRLabel5: TQRLabel;
    QRExpr1: TQRExpr;
    QRShape2: TQRShape;
    SPREL_PICK_LIST: TADOStoredProc;
    SPREL_PICK_LISTnCdPedido: TIntegerField;
    SPREL_PICK_LISTnCdPickList: TIntegerField;
    SPREL_PICK_LISTdDtPedido: TDateTimeField;
    SPREL_PICK_LISTnCdTerceiro: TIntegerField;
    SPREL_PICK_LISTcNmTerceiro: TStringField;
    SPREL_PICK_LISTcEnderecoEntrega: TStringField;
    SPREL_PICK_LISTdDtPrevEntIni: TDateTimeField;
    SPREL_PICK_LISTdDtPrevEntFim: TDateTimeField;
    SPREL_PICK_LISTnCdItemPedido: TIntegerField;
    SPREL_PICK_LISTcOBS: TStringField;
    SPREL_PICK_LISTnCdProduto: TIntegerField;
    SPREL_PICK_LISTcNmProduto: TStringField;
    SPREL_PICK_LISTnQtdePick: TBCDField;
    SPREL_PICK_LISTnCdTipoItemPed: TIntegerField;
    SPREL_PICK_LISTnCdAlaProducao: TIntegerField;
    SPREL_PICK_LISTcNmAlaProducao: TStringField;
    SPREL_PICK_LISTcComposicao: TStringField;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText4: TQRDBText;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPickList_Retrato: TrptPickList_Retrato;

implementation

{$R *.dfm}

end.
