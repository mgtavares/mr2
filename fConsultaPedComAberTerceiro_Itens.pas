unit fConsultaPedComAberTerceiro_Itens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, GridsEh, DBGridEh, ADODB, ImgList,
  ComCtrls, ToolWin, ExtCtrls, cxStyles, cxCustomData, cxGraphics,
  cxFilter, cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmConsultaPedComAberTerceiro_Itens = class(TfrmProcesso_Padrao)
    qryItens: TADOQuery;
    qryItensnCdItemPedido: TAutoIncField;
    qryItenscNmTipoItemPed: TStringField;
    qryItensnCdProduto: TIntegerField;
    qryItenscNmItem: TStringField;
    qryItensnQtde: TBCDField;
    qryItensdDtEntregaIni: TDateTimeField;
    qryItensdDtEntregaFim: TDateTimeField;
    dsItens: TDataSource;
    qryItenscSiglaUnidadeMedida: TStringField;
    cxGrid4: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    cxGridDBTableView3nCdItemPedido: TcxGridDBColumn;
    cxGridDBTableView3cNmTipoItemPed: TcxGridDBColumn;
    cxGridDBTableView3nCdProduto: TcxGridDBColumn;
    cxGridDBTableView3cNmItem: TcxGridDBColumn;
    cxGridDBTableView3nQtde: TcxGridDBColumn;
    cxGridDBTableView3dDtEntregaIni: TcxGridDBColumn;
    cxGridDBTableView3dDtEntregaFim: TcxGridDBColumn;
    cxGridDBTableView3cSiglaUnidadeMedida: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaPedComAberTerceiro_Itens: TfrmConsultaPedComAberTerceiro_Itens;

implementation

{$R *.dfm}

end.
