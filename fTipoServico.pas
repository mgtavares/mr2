unit fTipoServico;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, DBGridEhGrouping, GridsEh, DBGridEh,
  cxPC, cxControls;

type
  TfrmTipoServico = class(TfrmCadastro_Padrao)
    qryMasternCdTipoServico: TIntegerField;
    qryMastercNmTipoServico: TStringField;
    qryMastercDescricaoDetalhada: TStringField;
    qryMastercFlgServicoTomado: TIntegerField;
    qryMastercFlgServicoPrestado: TIntegerField;
    qryMastercFlgRetencaoAtiva: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    qryTipoServicoTipoImposto: TADOQuery;
    qryTipoServicoMunicipio: TADOQuery;
    qryTipoServicoTipoImpostonCdTipoServicoTipoImposto: TIntegerField;
    qryTipoServicoTipoImpostonCdTabTipoPessoa: TIntegerField;
    qryTipoServicoTipoImpostonCdTipoImposto: TIntegerField;
    qryTipoServicoTipoImpostonPercBaseCalc: TBCDField;
    qryTipoServicoTipoImpostonPercAliq: TBCDField;
    qryTipoServicoTipoImpostocFlgProtegerBase: TIntegerField;
    qryTipoServicoTipoImpostocCdCodRecolhimento: TStringField;
    dsTipoServicoTipoImposto: TDataSource;
    qryTipoImposto: TADOQuery;
    qryTipoPessoa: TADOQuery;
    qryTipoPessoanCdTabTipoPessoa: TIntegerField;
    qryTipoPessoacNmTabTipoPessoa: TStringField;
    qryTipoImpostonCdTipoImposto: TIntegerField;
    qryTipoImpostocNmTipoImposto: TStringField;
    qryTipoServicoTipoImpostocNmTipoImposto: TStringField;
    qryTipoServicoTipoImpostocNmTipoPessoa: TStringField;
    DBGridEh2: TDBGridEh;
    qryTipoServicoTipoImpostonCdTipoServico: TIntegerField;
    dsAbatimento: TDataSource;
    DBGridEh1: TDBGridEh;
    qryTipoServicoMunicipionCdTipoServicoMunicipio: TIntegerField;
    qryTipoServicoMunicipionCdTipoServico: TIntegerField;
    qryTipoServicoMunicipionCdMunicipio: TIntegerField;
    qryTipoServicoMunicipiocCdServMunicipal: TStringField;
    qryMunicipio: TADOQuery;
    qryMunicipionCdMunicipio: TIntegerField;
    qryMunicipionCdEstado: TIntegerField;
    qryMunicipiocNmMunicipio: TStringField;
    qryMunicipionCdMunicipioIBGE: TIntegerField;
    dsTipoServicoTipoMunicipio: TDataSource;
    qryTipoServicoMunicipiocNmMunicipio: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryTipoServicoTipoImpostoBeforePost(DataSet: TDataSet);
    procedure DBGridEh2ColExit(Sender: TObject);
    procedure qryTipoServicoTipoImpostoCalcFields(DataSet: TDataSet);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTipoServicoMunicipioBeforePost(DataSet: TDataSet);
    procedure qryTipoServicoMunicipioCalcFields(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh2Enter(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoServico: TfrmTipoServico;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmTipoServico.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOSERVICO' ;
  nCdTabelaSistema  := 506 ;
  nCdConsultaPadrao := 1013 ;
  bLimpaAposSalvar  := False ;

end;

procedure TfrmTipoServico.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmTipoServico.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  
end;

procedure TfrmTipoServico.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryTipoServicoTipoImposto.Close;
  qryTipoServicoMunicipio.Close;

  PosicionaQuery(qryTipoServicoTipoImposto,qryMasternCdTipoServico.AsString);
  PosicionaQuery(qryTipoServicoMunicipio,qryMasternCdTipoServico.AsString);

end;

procedure TfrmTipoServico.qryTipoServicoTipoImpostoBeforePost(
  DataSet: TDataSet);
begin

  if (qryTipoServicoTipoImpostonCdTipoImposto.Value = 1) then
  begin
      qryTipoServicoTipoImpostonPercBaseCalc.Value := 0;
      qryTipoServicoTipoImpostonPercAliq.Value     := 0;
  end;

  qryTipoServicoTipoImpostonCdTipoServico.Value := qryMasternCdTipoServico.Value;

  if (qryTipoServicoTipoImposto.State = dsInsert) then
      qryTipoServicoTipoImpostonCdTipoServicoTipoImposto.Value := frmMenu.fnProximoCodigo('TIPOSERVICOTIPOIMPOSTO') ;

  inherited;

end;

procedure TfrmTipoServico.DBGridEh2ColExit(Sender: TObject);
begin
  inherited;
  if (DBGridEh2.Col = 3) then
      PosicionaQuery(qryTipoImposto,qryTipoServicoTipoImpostonCdTipoImposto.AsString);


  if (DBGridEh2.Col = 1) then
      PosicionaQuery(qryTipoPessoa,qryTipoServicoTipoImpostonCdTabTipoPessoa.AsString);
end;

procedure TfrmTipoServico.qryTipoServicoTipoImpostoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryTipoPessoa.Close;
  qryTipoImposto.Close;

  PosicionaQuery(qryTipoPessoa,qryTipoServicoTipoImpostonCdTabTipoPessoa.AsString);
  PosicionaQuery(qryTipoImposto,qryTipoServicoTipoImpostonCdTipoImposto.AsString);

  if not qryTipoPessoa.eof then
      qryTipoServicoTipoImpostocNmTipoPessoa.Value := qryTipoPessoacNmTabTipoPessoa.Value;

  if not qryTipoImposto.eof then
      qryTipoServicoTipoImpostocNmTipoImposto.Value := qryTipoImpostocNmTipoImposto.Value;

end;

procedure TfrmTipoServico.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh2.Col = 1) then
        begin

            if (qryTipoServicoTipoImposto.State = dsBrowse) then
                qryTipoServicoTipoImposto.Edit ;

            if (qryTipoServicoTipoImposto.State = dsInsert) or (qryTipoServicoTipoImposto.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(1019);

                If (nPK > 0) then
                begin
                    qryTipoServicoTipoImpostonCdTabTipoPessoa.Value := nPK ;
                end ;

            end ;
        end;

        if (DBGridEh2.Col = 3) then
        begin

            if (qryTipoServicoTipoImposto.State = dsBrowse) then
                qryTipoServicoTipoImposto.Edit ;

            if (qryTipoServicoTipoImposto.State = dsInsert) or (qryTipoServicoTipoImposto.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(1012);

                If (nPK > 0) then
                begin
                    qryTipoServicoTipoImpostonCdTipoImposto.Value := nPK ;
                end ;

            end ;
        end;

    end ;

  end ;

end;

procedure TfrmTipoServico.qryTipoServicoMunicipioBeforePost(
  DataSet: TDataSet);
begin

  if (Trim(qryTipoServicoMunicipiocCdServMunicipal.Value) = '') then
  begin
      MensagemAlerta('Informe o Codigo servi�o Municipal.') ;
      abort ;
  end;

  inherited;

  qryTipoServicoMunicipionCdTipoServico.Value := qryMasternCdTipoServico.Value;

  if (qryTipoServicoMunicipio.State = dsInsert) then
      qryTipoServicoMunicipionCdTipoServicoMunicipio.Value := frmMenu.fnProximoCodigo('TIPOSERVICOMUNICIPIO') ;
  
end;

procedure TfrmTipoServico.qryTipoServicoMunicipioCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryMunicipio.Close;
  PosicionaQuery(qryMunicipio,qryTipoServicoMunicipionCdMunicipio.AsString);

  if (qryTipoServicoMunicipionCdMunicipio.Value > 0) then
      qryTipoServicoMunicipiocNmMunicipio.Value := qryMunicipiocNmMunicipio.Value;

end;

procedure TfrmTipoServico.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
  var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (DBGridEh1.Col = 1) then
        begin

            if (qryTipoServicoMunicipio.State = dsBrowse) then
                qryTipoServicoMunicipio.Edit ;

            if (qryTipoServicoMunicipio.State = dsInsert) or (qryTipoServicoMunicipio.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta(205);

                If (nPK > 0) then
                begin
                    qryTipoServicoMunicipionCdMunicipio.Value := nPK ;
                end ;

            end ;

        end;

    end ;

  end ;

end;

procedure TfrmTipoServico.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryTipoServicoTipoImposto.Close;
  qryTipoServicoMunicipio.Close;

end;

procedure TfrmTipoServico.DBGridEh2Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State in [dsInsert,dsEdit]) then
      qryMaster.Post ;

end;

procedure TfrmTipoServico.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  if (qryMaster.State in [dsInsert,dsEdit]) then
      qryMaster.Post ;

end;

procedure TfrmTipoServico.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryTipoServicoTipoImposto.Close;
  qryTipoServicoMunicipio.Close;

  PosicionaQuery(qryTipoServicoTipoImposto,qryMasternCdTipoServico.AsString);
  PosicionaQuery(qryTipoServicoMunicipio,qryMasternCdTipoServico.AsString);
  
end;

initialization
    RegisterClass(tFrmTipoServico) ;

end.
