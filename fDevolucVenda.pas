unit fDevolucVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, cxContainer, cxEdit, cxTextEdit, Menus,
  DBGridEhGrouping, ToolCtrlsEh, ACBrNFe, fMontaGrade;

type
  TfrmDevolucVenda = class(TfrmCadastro_Padrao)
    qryMasternCdPedido: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    qryMasternCdTabTipoPedido: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdTerceiroColab: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternCdTabStatusPed: TIntegerField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternPercDesconto: TBCDField;
    qryMasternPercAcrescimo: TBCDField;
    qryMasternValProdutos: TBCDField;
    qryMasternValServicos: TBCDField;
    qryMasternValImposto: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMasternValAcrescimo: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMasternValOutros: TBCDField;
    qryMasternValPedido: TBCDField;
    qryMastercNrPedTerceiro: TStringField;
    qryMastercOBS: TMemoField;
    qryMasterdDtAutor: TDateTimeField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMasterdDtRejeicao: TDateTimeField;
    qryMasternCdUsuarioRejeicao: TIntegerField;
    qryMastercMotivoRejeicao: TMemoField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTipoPedidonCdTabTipoPedido: TIntegerField;
    qryTipoPedidocFlgCalcImposto: TIntegerField;
    qryTipoPedidocFlgComporRanking: TIntegerField;
    qryTipoPedidocFlgVenda: TIntegerField;
    qryTipoPedidocGerarFinanc: TIntegerField;
    qryTipoPedidocExigeAutor: TIntegerField;
    qryTipoPedidonCdEspTitPrev: TIntegerField;
    qryTipoPedidonCdEspTit: TIntegerField;
    qryTipoPedidonCdCategFinanc: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryEstoque: TADOQuery;
    qryEstoquenCdEstoque: TAutoIncField;
    qryEstoquenCdEmpresa: TIntegerField;
    qryEstoquenCdLoja: TIntegerField;
    qryEstoquenCdTipoEstoque: TIntegerField;
    qryEstoquecNmEstoque: TStringField;
    qryEstoquenCdStatus: TIntegerField;
    qryStatusPed: TADOQuery;
    qryStatusPednCdTabStatusPed: TIntegerField;
    qryStatusPedcNmTabStatusPed: TStringField;
    qryMastercNmContato: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit12: TDBEdit;
    dsEmpresa: TDataSource;
    dsLoja: TDataSource;
    DBEdit14: TDBEdit;
    dsTipoPedido: TDataSource;
    DBEdit15: TDBEdit;
    dsTerceiro: TDataSource;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    dsStatusPed: TDataSource;
    qryMasternCdEstoqueMov: TIntegerField;
    DataSource6: TDataSource;
    dsEstoque: TDataSource;
    DataSource8: TDataSource;
    DataSource9: TDataSource;
    DataSource10: TDataSource;
    cxPageControl1: TcxPageControl;
    TabItemEstoque: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioTipoPedido: TADOQuery;
    qryUsuarioTipoPedidonCdTipoPedido: TIntegerField;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdLoja: TIntegerField;
    qryTerceironCdTerceiroPagador: TIntegerField;
    Label26: TLabel;
    DBEdit37: TDBEdit;
    DataSource11: TDataSource;
    qryItemEstoque: TADOQuery;
    qryItemEstoquenCdProduto: TIntegerField;
    qryItemEstoquecNmItem: TStringField;
    qryItemEstoquenQtdePed: TBCDField;
    qryItemEstoquenQtdeExpRec: TBCDField;
    qryItemEstoquenQtdeCanc: TBCDField;
    qryItemEstoquenValUnitario: TBCDField;
    qryItemEstoquenValDesconto: TBCDField;
    qryItemEstoquenValSugVenda: TBCDField;
    qryItemEstoquenValTotalItem: TBCDField;
    qryItemEstoquenPercIPI: TBCDField;
    qryItemEstoquenValIPI: TBCDField;
    dsItemEstoque: TDataSource;
    qryItemEstoquenCdPedido: TIntegerField;
    qryItemEstoquenCdTipoItemPed: TIntegerField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryItemEstoquecCalc: TStringField;
    qryTerceironCdTerceiroTransp: TIntegerField;
    qryTerceironCdIncoterms: TIntegerField;
    qryTerceironCdCondPagto: TIntegerField;
    qryTerceironPercDesconto: TBCDField;
    qryTerceironPercAcrescimo: TBCDField;
    DBGridEh2: TDBGridEh;
    usp_Grade: TADOStoredProc;
    dsGrade: TDataSource;
    qryTemp: TADOQuery;
    usp_Gera_SubItem: TADOStoredProc;
    qryItemEstoquenCdItemPedido: TAutoIncField;
    cmdExcluiSubItem: TADOCommand;
    qryAux: TADOQuery;
    qryItemEstoquenValAcrescimo: TBCDField;
    qryItemEstoquenValCustoUnit: TBCDField;
    qryPrazoPedido: TADOQuery;
    dsPrazoPedido: TDataSource;
    qryPrazoPedidonCdPrazoPedido: TAutoIncField;
    qryPrazoPedidonCdPedido: TIntegerField;
    qryPrazoPedidodVencto: TDateTimeField;
    qryPrazoPedidoiDias: TIntegerField;
    qryPrazoPedidonValPagto: TBCDField;
    usp_Sugere_Parcela: TADOStoredProc;
    ToolButton3: TToolButton;
    btFinalizar: TToolButton;
    qryProdutonCdGrade: TIntegerField;
    dsItemAD: TDataSource;
    qryTipoPedidocFlgAtuPreco: TIntegerField;
    qryTipoPedidocFlgItemEstoque: TIntegerField;
    qryTipoPedidocFlgItemAD: TIntegerField;
    qryTipoPedidonCdTipoPedido_1: TIntegerField;
    qryTipoPedidonCdUsuario: TIntegerField;
    usp_Finaliza: TADOStoredProc;
    btImprimir: TToolButton;
    qryDadoAutorz: TADOQuery;
    qryDadoAutorzcDadoAutoriz: TStringField;
    DBEdit34: TDBEdit;
    dsDadoAutorz: TDataSource;
    qryMastercFlgCritico: TIntegerField;
    qryMasternSaldoFat: TBCDField;
    DataSource13: TDataSource;
    dsItemFormula: TDataSource;
    usp_gera_item_embalagem: TADOStoredProc;
    qryTipoPedidocFlgExibeAcomp: TIntegerField;
    qryTipoPedidocFlgCompra: TIntegerField;
    qryTipoPedidocFlgItemFormula: TIntegerField;
    qryMastercOBSFinanc: TMemoField;
    cxTabSheet2: TcxTabSheet;
    Image4: TImage;
    btCancDevolucao: TToolButton;
    usp_valida_credito: TADOStoredProc;
    sp_tab_preco: TADOStoredProc;
    qryMastercMsgNF1: TStringField;
    qryMastercMsgNF2: TStringField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit13: TDBEdit;
    qryMasternPercDescontoVencto: TBCDField;
    qryMasternPercAcrescimoVendor: TBCDField;
    qryMasternCdMotBloqPed: TIntegerField;
    qryMasternCdTerceiroRepres: TIntegerField;
    qryMastercAtuCredito: TIntegerField;
    qryMastercCreditoLibMan: TIntegerField;
    qryMasterdDtIniProducao: TDateTimeField;
    DataSource14: TDataSource;
    cxTabSheet3: TcxTabSheet;
    qryMastercAnotacao: TMemoField;
    DBMemo5: TDBMemo;
    qryMastercFlgOrcamento: TIntegerField;
    SP_EMPENHA_ESTOQUE_PEDIDO: TADOStoredProc;
    qryMasternValAdiantamento: TBCDField;
    qryMastercFlgAdiantConfirm: TIntegerField;
    qryMasternCdUsuarioConfirmAdiant: TIntegerField;
    PopupMenu1: TPopupMenu;
    ExibirAtendimentosdoItem1: TMenuItem;
    cxButton10: TcxButton;
    qryDoctoFiscal: TADOQuery;
    qryDoctoFiscalnCdDoctoFiscal: TIntegerField;
    sp_imprime_docto: TADOStoredProc;
    cxButton11: TcxButton;
    SP_MOVIMENTA_ESTOQUE_ITEM_PEDIDO: TADOStoredProc;
    qryMasternValDevoluc: TBCDField;
    qryMasternValValePres: TBCDField;
    qryMasternValValeMerc: TBCDField;
    qryMasterdDtContab: TDateTimeField;
    qryMasternCdLanctoFin: TIntegerField;
    qryMasternCdMapaCompra: TIntegerField;
    qryMastercFlgIntegrado: TIntegerField;
    qryMastercOrcamentoFin: TIntegerField;
    qryMasternCdPedidoOrcamento: TIntegerField;
    qryMasternValAdiantUsado: TBCDField;
    qryMastercFlgDevPag: TIntegerField;
    qryMastercFlgMovEstoque: TIntegerField;
    qryItemEstoquenQtdeLibFat: TBCDField;
    qryItemEstoquenPercDescontoItem: TBCDField;
    qryItemEstoquecDescricaoTecnica: TMemoField;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    qryInseriItemGrade: TADOQuery;
    panelItem: TPanel;
    btItemGrade: TcxButton;
    Label9: TLabel;
    DBEdit8: TDBEdit;
    Label10: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label11: TLabel;
    qrySubItemEstoque: TADOQuery;
    qrySubItemEstoquenCdProduto: TIntegerField;
    qrySubItemEstoquecNmItem: TStringField;
    qrySubItemEstoquenQtdePed: TBCDField;
    qrySubItemEstoquenQtdeExpRec: TBCDField;
    qrySubItemEstoquenQtdeCanc: TBCDField;
    qrySubItemEstoquenValUnitario: TBCDField;
    qrySubItemEstoquenValDesconto: TBCDField;
    qrySubItemEstoquenValAcrescimo: TBCDField;
    qrySubItemEstoquenValSugVenda: TBCDField;
    qrySubItemEstoquenValTotalItem: TBCDField;
    qrySubItemEstoquenPercIPI: TBCDField;
    qrySubItemEstoquenValIPI: TBCDField;
    qrySubItemEstoquenValCustoUnit: TBCDField;
    qrySubItemEstoquenCdPedido: TIntegerField;
    qrySubItemEstoquenCdTipoItemPed: TIntegerField;
    qrySubItemEstoquenCdItemPedido: TIntegerField;
    qrySubItemEstoquenCdItemPedidoPai: TIntegerField;
    qrySubItemEstoquenQtdeLibFat: TBCDField;
    qrySubItemEstoquenPercDescontoItem: TBCDField;
    qrySubItemEstoquecDescricaoTecnica: TMemoField;
    dsSubItemEstoque: TDataSource;
    qryMastercFlgPedidoWeb: TIntegerField;
    CheckcFlgDocEntregue: TDBCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTerceiroAfterScroll(DataSet: TDataSet);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit25KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit24KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit38KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryItemEstoqueBeforePost(DataSet: TDataSet);
    procedure qryItemEstoqueCalcFields(DataSet: TDataSet);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit19KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueBeforeDelete(DataSet: TDataSet);
    procedure cxButton10Click(Sender: TObject);
    procedure qryItemEstoquenValUnitarioValidate(Sender: TField);
    procedure qryItemEstoquenValAcrescimoValidate(Sender: TField);
    procedure qryItemEstoqueAfterDelete(DataSet: TDataSet);
    procedure qryPrazoPedidoBeforePost(DataSet: TDataSet);
    procedure btSugParcelaClick(Sender: TObject);
    procedure btFinalizarClick(Sender: TObject);
    procedure qryTempAfterCancel(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh4Enter(Sender: TObject);
    procedure btImprimirClick(Sender: TObject);
    procedure GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
    procedure btSalvarClick(Sender: TObject);
    procedure DBEdit41KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btCancDevolucaoClick(Sender: TObject);
    procedure DBGridEh1ColEnter(Sender: TObject);
    procedure ExibirAtendimentosdoItem1Click(Sender: TObject);
    procedure cxButton11Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btItemGradeClick(Sender: TObject);
    procedure DBEdit11Exit(Sender: TObject);
    procedure DBEdit11KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmDevolucVenda : TfrmDevolucVenda;
  objGrade        : TfrmMontaGrade;

implementation

uses Math,fMenu, fLookup_Padrao, fEmbFormula, fPrecoEspPedCom, rPedidoVenda,
  fItemPedidoAtendido, fItemFaturar, fImpDFPadrao;

{$R *.dfm}
  
procedure TfrmDevolucVenda.GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
var
  X:integer;
  dr,dg,db:Extended;
  C1,C2:TColor;
  r1,r2,g1,g2,b1,b2:Byte;
  R,G,B:Byte;
  cnt:integer;
begin
  C1 := FromColor;
  R1 := GetRValue(C1) ;
  G1 := GetGValue(C1) ;
  B1 := GetBValue(C1) ;

  C2 := ToColor;
  R2 := GetRValue(C2) ;
  G2 := GetGValue(C2) ;
  B2 := GetBValue(C2) ;

  dr := (R2-R1) / Rect.Right-Rect.Left;
  dg := (G2-G1) / Rect.Right-Rect.Left;
  db := (B2-B1) / Rect.Right-Rect.Left;

  cnt := 0;
  for X := Rect.Left to Rect.Right-1 do
  begin
    R := R1+Ceil(dr*cnt) ;
    G := G1+Ceil(dg*cnt) ;
    B := B1+Ceil(db*cnt) ;

    Canvas.Pen.Color := RGB(R,G,B) ;
    Canvas.MoveTo(X,Rect.Top) ;
    Canvas.LineTo(X,Rect.Bottom) ;
    inc(cnt) ;
  end;
end;

procedure TfrmDevolucVenda.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PEDIDO' ;
  nCdTabelaSistema  := 30 ;
  nCdConsultaPadrao := 143;
  bLimpaAposSalvar  := False ;

  DBGridEh2.Top  := 408;
  DBGridEh2.Left := 80 ;

end;

procedure TfrmDevolucVenda.btIncluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  if not qryEmpresa.Active then
  begin
      qryEmpresa.Close ;
      qryEmpresa.Parameters.ParamByName('nPK').Value := frmMenu.nCdEmpresaAtiva ;
      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryEmpresa.Open ;

      if not qryEmpresa.eof then
      begin
          qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
      end ;
  end ;

  qryUsuarioTipoPedido.Close ;
  qryUsuarioTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryUsuarioTipoPedido.Open ;

  if (qryUsuarioTipoPedido.eof) then
  begin
      MensagemAlerta('Nenhum tipo de devolu��o vinculado para este usu�rio.') ;
      btCancelar.Click;
      abort ;
  end ;

  if (qryUsuarioTipoPedido.RecordCount = 1) then
  begin
      PosicionaQuery(qryTipoPedido,qryUsuarioTipoPedidonCdTipoPedido.AsString) ;
      qryMasternCdTipoPedido.Value := qryUsuarioTipoPedidonCdTipoPedido.Value ;
      DBEdit4.OnExit(nil) ;
  end ;

  qryUsuarioTipoPedido.Close ;

  if (qryMasternCdTipoPedido.Value = 0) then
      if (not DBEdit11.ReadOnly) then
          DBEdit11.SetFocus
      else
          DBEdit4.SetFocus;
  //else
  //    DBEdit5.SetFocus;

  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryEstoque.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value ;

  DBGridEh1.ReadOnly       := False ;
  btSalvar.Enabled         := True ;
  btFinalizar.Enabled      := True ;
  qryMasterdDtPedido.Value := StrToDate(DateToStr(Now())) ;

end;

procedure TfrmDevolucVenda.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTipoPedido.Close ;

end;

procedure TfrmDevolucVenda.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(142);

            If (nPK > 0) then
            begin
                qryMasternCdTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (Trim(DbEdit4.Text) = '') then
            begin
                MensagemAlerta('Informe o Tipo de Pedido.') ;
                DBEdit4.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(17,'EXISTS(SELECT 1 FROM TerceiroTipoTerceiro TTT  INNER JOIN TipoPedidoTipoTerceiro TTTP ON TTTP.nCdTipoTerceiro = TTT.nCdTipoTerceiro AND TTTp.nCdTipoPedido = ' + DbEdit4.Text + ' WHERE TTT.nCdTerceiro = vTerceiros.nCdTerceiro)');

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.DBEdit20KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.qryTerceiroAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

end;

procedure TfrmDevolucVenda.DBEdit23KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.DBEdit25KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.DBEdit24KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(61);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  if not qryTipoPedido.Active then
  begin
      MensagemAlerta('Informe o Tipo do Pedido.') ;
      DbEdit4.SetFocus ;
      exit ;
  end ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, dbEdit5.Text) ;

  if not qryTerceiro.active then
      exit ;

  if (qryMaster.State = dsInsert) then
  begin
  end ;

end;

procedure TfrmDevolucVenda.DBEdit38KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(61);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.DBEdit4Exit(Sender: TObject);
begin
  inherited;

  qryTipoPedido.Close;
  PosicionaQuery(qryTipoPedido, DBEdit4.Text) ;

  TabItemEstoque.Enabled := True ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmDevolucVenda.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryEmpresa, DBEdit2.Text) ;
  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;

end;

procedure TfrmDevolucVenda.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
      MensagemErro('Este pedido n�o pertence a esta empresa.') ;
      btCancelar.Click;
      exit ;
  end ;

  if (qryMastercFlgOrcamento.Value <> 0) then
  begin
      MensagemAlerta('Este pedido � um or�amento e n�o pode ser visualizado nesta tela.') ;
      btCancelar.Click;
      exit ;
  end ;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryLoja.Parameters.ParamByName('nPK').Value        := qryMasternCdLoja.Value;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryLoja.Open;
  
  PosicionaQuery(qryTipoPedido, qryMasternCdTipoPedido.asString) ;

  if (qryTipoPedido.eof) then
  begin
      MensagemErro('Voc� n�o tem autoriza��o para visualizar este tipo de pedido.') ;
      btCancelar.Click;
      exit;
  end ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryDadoAutorz,qryMasternCdPedido.asString) ;

  DBGridEh1.ReadOnly   := False ;

  btSalvar.Enabled     := True ;
  btFinalizar.Enabled  := True ;

  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      DBGridEh1.ReadOnly   := True ;
      //DBGridEh3.ReadOnly   := True ;
      btSalvar.Enabled     := False ;
      btFinalizar.Enabled  := False ;
  end ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmDevolucVenda.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTipoPedido.Close ;
  qryLoja.Close ;
  qryEstoque.Close ;
  qryTerceiro.Close ;
  qryItemEstoque.Close ;
  qryStatusPed.Close ;
  qryPrazoPedido.Close ;
  qryDadoAutorz.Close ;

end;

procedure TfrmDevolucVenda.qryItemEstoqueBeforePost(DataSet: TDataSet);
begin
  if ((qryItemEstoquenPercDescontoItem.Value < 0) or (qryItemEstoquenPercDescontoItem.Value > 100)) then
  begin
      MensagemAlerta('Percentual de Abatimento inv�lido.');
      DBGridEh1.SelectedField := qryItemEstoquenPercDescontoItem;
      DBGridEh1.SetFocus;
      abort ;
  end;

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      MensagemAlerta('Informe o produto.');
      DBGridEh1.SelectedField := qryItemEstoquenCdProduto;
      DBGridEh1.SetFocus;
      Abort;
  end ;

  if (qryItemEstoquenQtdePed.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade do produto.');
      DBGridEh1.SelectedField := qryItemEstoquenQtdePed;
      DBGridEh1.SetFocus;
      Abort;
  end;

  if (qryItemEstoquenValUnitario.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor unit�rio do produto.');
      DBGridEh1.SelectedField := qryItemEstoquenValUnitario;
      DBGridEh1.SetFocus;
      Abort;
  end;

  if (qryItemEstoque.State = dsInsert) then
  begin
      qryItemEstoquenCdPedido.Value      := qryMasternCdPedido.Value ;

      PosicionaQuery(qryProduto, qryItemEstoquencdProduto.AsString) ;

      if not qryProduto.eof and (qryProdutonCdGrade.Value = 0) then
          qryItemEstoquenCdTipoItemPed.Value := 2  //produto ERP
      else
          qryItemEstoquenCdTipoItemPed.Value := 1; //produto grade

      qryItemEstoquecNmItem.Value     := Uppercase(qryItemEstoquecNmItem.Value) ;
      qryItemEstoquenQtdeLibFat.Value := qryItemEstoquenQtdePed.asInteger ;

  end ;

  {if (frmMenu.LeParametroEmpresa('PRECOMANUAL') = 'N') then
  begin

      qryItemEstoquenValUnitario.Value  := qryItemEstoquenValSugVenda.Value ;
      qryItemEstoquenValTotalItem.Value := (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value) ;

  end ;}

  inherited;

  qryItemEstoquenValDesconto.Value  := frmMenu.TBRound((qryItemEstoquenValUnitario.Value * (((qryItemEstoquenPercDescontoItem.Value)/100))),2) ;
  qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) ;
  qryItemEstoquenValTotalItem.Value := (qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value) ; //+ 0.005  ;

  if (qryItemEstoque.State = dsEdit) then
  begin
      qryMasternValDesconto.Value := qryMasternValDesconto.Value - (VarAsType(qryItemEstoquenQtdePed.OldValue, varDouble) * VarAsType(qryItemEstoquenValDesconto.OldValue, varDouble));
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - (VarAsType(qryItemEstoquenQtdePed.OldValue, varDouble) * VarAsType(qryItemEstoquenValUnitario.OldValue, varDouble));

      if (qryMasternValDesconto.Value   < 0) then qryMasternValDesconto.Value  := 0;
      if (qryMasternValProdutos.Value   < 0) then qryMasternValProdutos.Value  := 0;

  end ;

  if (qryItemEstoque.State = dsInsert) then
  begin
      qryItemEstoquenCdItemPedido.Value := frmMenu.fnProximoCodigo('ITEMPEDIDO');
  end ;

  qryItemEstoquecDescricaoTecnica.Value := Uppercase( qryItemEstoquecDescricaoTecnica.Value ) ;
end;

procedure TfrmDevolucVenda.qryItemEstoqueCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      qryProduto.Close;
      qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
      PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

      if not qryProduto.eof then
          qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;
  end ;

end;

procedure TfrmDevolucVenda.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  DBGridEh1.Columns[6].ReadOnly   := True ;

  if (DbGridEh1.Col = 2) then
      DBGridEh1.Col := 3 ;

  If (DBGridEh1.Col = 1) then
  begin

      DBGridEh1.Columns[2].ReadOnly   := True ;

      if (qryItemEstoque.State <> dsBrowse) then
      begin

        {qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('SELECT 1 FROM ItemPedido WHERE nCdPedido = ' + qryMasternCdPedido.asString + ' AND nCdProduto = ' + qryItemEstoquenCdProduto.asString) ;
        qryAux.Open ;

        if not qryAux.Eof then
        begin
            qryAux.Close ;
            ShowMessage('Item j� digitado neste pedido.') ;
            abort ;
        end ;

        qryAux.Close ;}

        qryProduto.Close;
        qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

        if (Trim(qryItemEstoquecNmItem.Value) = '') then
        begin
            MensagemAlerta('Informe o produto.');
            DBGridEh1.SelectedField := qryItemEstoquenCdProduto;
            DBGridEh1.SetFocus;
            Abort;
        end;

        PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

        { -- produto grade -- }
        if not qryProduto.eof and (qryProdutonCdGrade.Value > 0) then
        begin
            qryItemEstoquenCdProduto.Value := qryProdutonCdProduto.Value;
            qryItemEstoquecNmItem.Value    := qryProdutocNmProduto.Value ;

            { -- removido utiliza��o do bloco abaixo devido nova forma de inclus�o dos sub itens atrav�s da fMontaGrade -- }
            {DBGridEh2.AutoFitColWidths := True ;

            usp_Grade.Close ;
            usp_Grade.Parameters.ParamByName('@nCdPedido').Value  := qryMasternCdPedido.Value ;
            usp_Grade.Parameters.ParamByName('@nCdProduto').Value := qryItemEstoquenCdProduto.Value ;
            usp_Grade.ExecProc ;

            qryTemp.Close ;
            qryTemp.SQL.Clear ;
            qryTemp.SQL.Add(usp_Grade.Parameters.ParamByName('@cSQLRetorno').Value);
            qryTemp.Open ;

            qryTemp.Edit ;

            DBGridEh2.Columns.Items[0].Visible := False ;

            DBGridEh2.Visible := True ;

            DBGridEh2.SetFocus ;}

            objGrade.qryValorAnterior.SQL.Text := '' ;

            frmMenu.mensagemUsuario('Montando Grade...');

            if (qryItemEstoquenCdItemPedido.Value > 0) then
                objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtdePed FROM ItemPedido WITH(INDEX=IN01_ItemPedido) WHERE nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString + ' AND nCdTipoItemPed = 4 ORDER BY ncdProduto') ;

            objGrade.PreparaDataSet(qryItemEstoquenCdProduto.Value);

            { -- se pedido j� foi finalizado, monta grade apenas para exibi��o -- }
            if (qryMasternCdTabStatusPed.Value > 1) then
                objGrade.DBGridEh1.ReadOnly := True ;

            frmMenu.mensagemUsuario('Renderizando...');

            objGrade.Renderiza;
            objGrade.DBGridEh1.ReadOnly := False ;

            { -- se pedido esta como digitado, atualiza saldo do item -- }
            if (qryMasternCdTabStatusPed.Value = 1) then
            begin
                frmMenu.mensagemUsuario('Atualizando Item...');

                qryItemEstoque.Edit ;
                qryItemEstoquenQtdePed.Value := objGrade.nQtdeTotal ;

                DBGridEh1.Col := DBGridEh1.FieldColumns['nValUnitario'].Index;
                DBGridEh1.SetFocus;
            end ;

            frmMenu.mensagemUsuario('');

            DBGridEh1.Col := 3 ;
        end ;

        { -- produto ERP -- }
        if not qryProduto.eof and (qryProdutonCdGrade.Value = 0) then
        begin

            DBGridEh1.Columns[2].ReadOnly   := False ;
            DBGridEh1.Col := 2 ;

        end ;

      end ;

  end ;

  if ((DBGridEh1.Col = 4) and (qryItemEstoque.State <> dsBrowse)) then
  begin
      qryItemEstoquenValDesconto.Value  := frmMenu.TBRound((qryItemEstoquenValUnitario.Value * (((qryItemEstoquenPercDescontoItem.Value)/100))),2) ;
  end ;

end;

procedure TfrmDevolucVenda.FormShow(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
  begin
      desativaDBEdit(DBEdit11);
  end;

  { -- cria objetos globais -- }
  objGrade := TfrmMontaGrade.Create(Self);
end;

procedure TfrmDevolucVenda.DBEdit19KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  if not qryItemEstoque.Active then
      PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;

end;

procedure TfrmDevolucVenda.qryItemEstoqueAfterPost(DataSet: TDataSet);
var
  i : Integer;
begin
  inherited;

  { -- removido utiliza��o da procedure devido nova forma de inclus�o dos sub itens -- }
  {usp_Gera_SubItem.Close ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdPedido').Value     := qryMasternCdPedido.Value ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
  usp_Gera_SubItem.ExecProc ;}

  { -- se pedido utiliza produto grade, gera os sub itens -- }
  if (objGrade.bMontada) then
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('DELETE FROM ItemPedido WHERE nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString) ;
      qryAux.ExecSQL;

      objGrade.cdsQuantidade.First;
      objGrade.cdsCodigo.First ;

      for i := 0 to objGrade.cdsQuantidade.FieldList.Count-1 do
      begin
          if (StrToInt(objGrade.cdsQuantidade.Fields[i].Value) > 0) then
          begin
              qryInseriItemGrade.Close ;
              qryInseriItemGrade.Parameters.ParamByName('nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
              qryInseriItemGrade.Parameters.ParamByName('nCdProduto').Value    := StrToInt(objGrade.cdsCodigo.Fields[i].Value) ;
              qryInseriItemGrade.Parameters.ParamByName('iQtde').Value         := StrToInt(objGrade.cdsQuantidade.Fields[i].Value) ;
              qryInseriItemGrade.ExecSQL;
          end ;
      end ;

      objGrade.LiberaGrade;

  end ;

  qryMasternValDesconto.Value := qryMasternValDesconto.Value + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDesconto.Value);
  qryMasternValProdutos.Value := qryMasternValProdutos.Value + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value);

  qryMaster.Post ;
end;

procedure TfrmDevolucVenda.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabTipoPedido.Value := qryTipoPedidonCdTabTipoPedido.Value ;
      qryMasternCdTabStatusPed.Value  := 1 ;
  end ;

  if (qryMasternCdEmpresa.Value = 0) then
  begin
      MensagemAlerta('Informe a empresa.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if ((qryMasternCdLoja.Value = 0) and (not DBEdit11.ReadOnly)) then
  begin
      MensagemAlerta('Informe a loja.') ;
      DbEdit11.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTipoPedido.Value = 0) or (DbEdit14.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de devolu��o.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTerceiro.Value = 0) or (DbEdit15.Text = '') then
  begin
      MensagemAlerta('Informe o Terceiro.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  inherited;

  if (qryMaster.State = dsInsert) then
      qryMasternCdTabStatusPed.Value := 1;

  qryMasternValPedido.Value := qryMasternValOutros.Value + qryMasternValAcrescimo.Value - qryMasternValDesconto.Value + qryMasternValImposto.Value + qryMasternValServicos.Value + qryMasternValProdutos.Value;
end;

procedure TfrmDevolucVenda.qryTempAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmDevolucVenda.qryItemEstoqueBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemEstoquenCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  qryMasternValDesconto.Value := qryMasternValDesconto.Value - (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDesconto.Value) ;
  qryMasternValProdutos.Value := qryMasternValProdutos.Value - (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value) ;

end;

procedure TfrmDevolucVenda.cxButton10Click(Sender: TObject);
var
    nCdSerieFiscal : integer ;
begin
  if not qryMaster.active then
  begin
      ShowMessage('Nenhuma devolu��o ativa.') ;
      exit ;
  end ;

  qryDoctoFiscal.Close ;
  PosicionaQuery(qryDoctoFiscal, qryMasternCdPedido.asString) ;

  if not qryDoctoFiscal.Eof then
  begin
      MensagemAlerta('J� foi emitida uma nota fiscal para esta devolu��o.') ;
      exit ;
  end ;

  if (qryMastercFlgMovEstoque.Value = 1) then
  begin
      MensagemAlerta('O cr�dito de estoque j� foi processado.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a emiss�o da Nota Fiscal de entrada ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  nCdSerieFiscal := frmLookup_Padrao.ExecutaConsulta(78);

  if (nCdSerieFiscal <= 0) or (nCdSerieFiscal > 1000) then
  begin
      MensagemAlerta('Nenhuma s�rie fiscal escolhida.') ;
      exit ;
  end ;

  ShowMessage('Prepare a impressora e clique em OK') ;

  frmItemFaturar.nCdPedido  := qryMasternCdPedido.Value ;
  frmItemFaturar.dDtFaturar := Now() ;
  PosicionaQuery(frmItemFaturar.qryItemPedido,qryMasternCdPedido.AsString) ;
  frmItemFaturar.ToolButton1.Enabled := True ;

  try
      frmItemFaturar.ToolButton1.Click ;
  except
      raise ;
  end ;

  qryDoctoFiscal.Close ;
  PosicionaQuery(qryDoctoFiscal, qryMasternCdPedido.asString) ;

  frmMenu.Connection.BeginTrans;

  try
      sp_imprime_docto.close ;
      sp_imprime_docto.Parameters.ParamByName('@nCdDoctoFiscal').Value := qryDoctoFiscalnCdDoctoFiscal.Value ;
      sp_imprime_docto.Parameters.ParamByName('@nCdSerieFiscal').Value := nCdSerieFiscal;
      sp_imprime_docto.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
      sp_imprime_docto.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  frmImpDFPadrao.nCdDoctoFiscal := qryDoctoFiscalnCdDoctoFiscal.Value ;
  frmImpDFPadrao.ImprimirDocumento;


end;

procedure TfrmDevolucVenda.qryItemEstoquenValUnitarioValidate(
  Sender: TField);
begin
  inherited;

  if (qryMasternPercDesconto.Value > 0) then
      qryItemEstoquenValDesconto.Value := qryItemEstoquenValUnitario.Value * (qryMasternPercDesconto.AsFloat  / 100) ;

  if (qryMasternPercAcrescimo.Value > 0) then
      qryItemEstoquenValAcrescimo.Value := qryItemEstoquenValUnitario.Value * (qryMasternPercAcrescimo.AsFloat / 100) ;

end;

procedure TfrmDevolucVenda.qryItemEstoquenValAcrescimoValidate(
  Sender: TField);
begin
  inherited;
  qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) ;
  qryItemEstoquenValCustoUnit.Value := qryItemEstoquenValCustoUnit.Value ;

  qryItemEstoquenValTotalItem.Value := qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value ;

end;

procedure TfrmDevolucVenda.qryItemEstoqueAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmDevolucVenda.qryPrazoPedidoBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryPrazoPedidonCdPedido.Value := qryMasternCdPedido.Value ;

  if (qryPrazoPedidoiDias.Value > 0) then
      qryPrazoPedidodVencto.Value := qryMasterdDtPrevEntIni.Value + qryPrazoPedidoiDias.Value ;

  if (qryPrazoPedidodVencto.asString = '') then
  begin
      MensagemAlerta('Informe a data do vencimento.') ;
      abort ;
  end ;

  if (qryPrazoPedidonValPagto.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

end;

procedure TfrmDevolucVenda.btSugParcelaClick(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternValPedido.Value = 0) then
  begin
      ShowMessage('Pedido sem valor.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_Sugere_Parcela.Close ;
      usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Sugere_Parcela.ExecProc ;
  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

end;

procedure TfrmDevolucVenda.btFinalizarClick(Sender: TObject);
var
  nValProdutos : Double;
  nValDesconto : Double;
begin
  nValProdutos := 0;
  nValDesconto := 0;

  if (not qryMaster.Active) then
  begin
      ShowMessage('Nenhuma devolu��o ativa.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  { -- recalcula o total do pedido -- }
  if (qryItemEstoque.Active) and (not qryItemEstoque.eof) then
  begin
      qryItemEstoque.First;

      while (not qryItemEstoque.Eof) do
      begin
          nValProdutos  := nValProdutos  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValUnitario.Value);
          nValDesconto  := nValDesconto  + (qryItemEstoquenQtdePed.Value * qryItemEstoquenValDesconto.Value);

          qryItemEstoque.Next;
      end;
  end;

  qryMaster.Edit;
  qryMasternValProdutos.Value  := nValProdutos;
  qryMasternValDesconto.Value  := frmMenu.ArredondaDecimal(nValDesconto,2);
  qryMaster.Post;

  case MessageDlg('O devolu��o ser� finalizada e n�o poder� sofrer altera��es. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Abort;
  end;

  frmMenu.Connection.BeginTrans ;

  try
      qryMaster.Edit ;
      qryMasternCdTabStatusPed.Value := 2 ; // Aguardando aprova��o
      qryMasternSaldoFat.Value := qryMasternValPedido.Value ;
      qryMaster.Post ;

      frmMenu.SP_GERA_ALERTA.Close;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@nCdTipoAlerta').Value := 12 ;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cAssunto').Value      := 'APROVAR DEVOLU��O VENDA' ;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cMensagem').Value     := 'No Devolu��o: ' + qryMasternCdPedido.AsString + '  Terceiro: ' + qryTerceirocNmTerceiro.Value + '   Valor : ' + Format('%m',[qryMasternValPedido.Value]) ;
      frmMenu.SP_GERA_ALERTA.ExecProc;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  PosicionaQuery(qryMaster, qryMasternCdPedido.AsString) ;

  ShowMessage('Devolu��o finalizada com sucesso.'+#13#13+'Comunique o departamento financeiro para autorizar o pagamento.') ;

end;

procedure TfrmDevolucVenda.qryTempAfterCancel(DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmDevolucVenda.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemEstoque.State = dsBrowse) then
             qryItemEstoque.Edit ;

        if (qryItemEstoque.State = dsInsert) or (qryItemEstoque.State = dsEdit) then
        begin
            If (qryMasternCdTipoPedido.asString = '') then
            begin
                ShowMessage('Selecione um tipo de pedido.') ;
                abort ;
            end ;

            if (frmMenu.LeParametro('VAREJO') = 'S') then
                nPK := frmLookup_Padrao.ExecutaConsulta2(76,'nCdTabTipoProduto = 2 AND EXISTS(SELECT 1 FROM GrupoProdutoTipoPedido GPTP WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto AND GPTP.nCdTipoPedido = ' + qryMasternCdTipoPedido.AsString + ')')
            else
                nPK := frmLookup_Padrao.ExecutaConsulta2(76,'NOT EXISTS(SELECT 1 FROM Produto Filho WHERE Filho.nCdProdutoPai = Produto.nCdProduto) AND EXISTS(SELECT 1 FROM GrupoProdutoTipoPedido GPTP WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto AND GPTP.nCdTipoPedido = ' + qryMasternCdTipoPedido.AsString + ')');

            If (nPK > 0) then
            begin
                qryItemEstoquenCdProduto.Value := nPK ;

                qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
                PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

                if not qryProduto.eof then
                    qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

  DBGridEh1.Columns[8].ReadOnly := True ;

  if (qryTipoPedidocFlgAtuPreco.Value = 1) then
      DBGridEh1.Columns[8].ReadOnly := false ;
end;

procedure TfrmDevolucVenda.DBGridEh4Enter(Sender: TObject);
begin
  inherited;
  if (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmDevolucVenda.btImprimirClick(Sender: TObject);
var
  objRel : TrptPedidoVenda;
begin

  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      ShowMessage('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  objRel := TrptPedidoVenda.Create(nil) ;
  try
      try

          PosicionaQuery(objRel.qryPedido,qryMasternCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemEstoque_Grade,qryMasternCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemAD,qryMasternCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemFormula,qryMasternCdPedido.asString) ;

          objRel.QRSubDetail1.Enabled := True ;
          objRel.QRSubDetail2.Enabled := True ;
          objRel.QRSubDetail3.Enabled := True ;

          if (objRel.qryItemEstoque_Grade.eof) then
              objRel.QRSubDetail1.Enabled := False ;

          if (objRel.qryItemAD.eof) then
              objRel.QRSubDetail2.Enabled := False ;

          if (objRel.qryItemFormula.eof) then
              objRel.QRSubDetail3.Enabled := False ;

          {--envia filtros utilizados--}
          
          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TfrmDevolucVenda.btSalvarClick(Sender: TObject);
begin

  if (qryMaster.State <> dsInsert) then
  begin
      if (qryMasternCdTabStatusPed.Value > 1) then
      begin
          MensagemAlerta('Pedido n�o pode ser alterado.') ;
          abort ;
      end ;
  end ;

  inherited;

end;

procedure TfrmDevolucVenda.DBEdit41KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.DBEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmDevolucVenda.btCancDevolucaoClick(Sender: TObject);
begin
  inherited;
  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMastercFlgMovEstoque.Value = 1) then
  begin
      MensagemAlerta('O cr�dito de estoque j� foi processado, imposs�vel cancelar esta devolu��o.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 2) then
  begin
      MensagemAlerta('O status do pedido n�o permite mais cancelamento.') ;
      exit ;
  end ;

  case MessageDlg('O pedido ser� cancelado. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Exit;
  end;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Edit ;
      qryMasternCdTabStatusPed.Value := 10 ;
      qryMaster.Post ;

      frmMenu.LogAuditoria(30,3,qryMasternCdPedido.Value,'Pedido Cancelado') ;

  except
      frmMenu.Connection.RollbackTrans;
      qryMaster.Cancel;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Pedido cancelado com sucesso.') ;
  btCancelar.Click;

end;

procedure TfrmDevolucVenda.DBGridEh1ColEnter(Sender: TObject);
begin
  inherited;
  if (dbgridEh1.Col = 4) and (qryItemEstoque.State = dsInsert) then
  begin

      sp_tab_preco.Close;
      sp_tab_preco.Parameters.ParamByName('@nCdPedido').Value  := qryMasternCdPedido.Value ;
      sp_tab_preco.Parameters.ParamByName('@nCdProduto').Value := qryItemEstoquenCdProduto.Value ;
      sp_tab_preco.ExecProc;

      if (sp_tab_preco.Parameters.ParamByName('@nValor').Value = -1.00 ) then
      begin

          if (frmMenu.LeParametroEmpresa('PRECOMANUAL') = 'N') then
          begin
              MensagemAlerta('Tabela de pre�o n�o encontrada. Verifique.') ;
              abort ;
          end ;

      end ;

      if (sp_tab_preco.Parameters.ParamByName('@nValor').Value > 0) then
      begin

          qryItemEstoquenValUnitario.Value  := sp_tab_preco.Parameters.ParamByName('@nValor').Value ;
          qryItemEstoquenValDesconto.Value  := sp_tab_preco.Parameters.ParamByName('@nValDesconto').Value ;
          qryItemEstoquenValSugVenda.Value  := sp_tab_preco.Parameters.ParamByName('@nValor').Value ;
          qryItemEstoquenValTotalItem.Value := (qryItemEstoquenQtdePed.Value * (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value)) ;

      end ;

  end ;


end;

procedure TfrmDevolucVenda.ExibirAtendimentosdoItem1Click(
  Sender: TObject);
begin
  inherited;

  if (cxPageControl1.ActivePage = TabItemEstoque) then
  begin
      if (qryItemEstoque.Active) and (qryItemEstoquenCdItemPedido.Value > 0) then
      begin
          PosicionaQuery(frmItemPedidoAtendido.qryItem, qryItemEstoquenCdItemPedido.AsString) ;
          frmItemPedidoAtendido.ShowModal ;
      end ;
  end ;

end;

procedure TfrmDevolucVenda.cxButton11Click(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      ShowMessage('Nenhuma devolu��o ativa.') ;
      exit ;
  end ;

  qryDoctoFiscal.Close ;
  PosicionaQuery(qryDoctoFiscal, qryMasternCdPedido.asString) ;

  if not qryDoctoFiscal.Eof then
  begin
      MensagemAlerta('J� foi emitida uma nota fiscal para esta devolu��o.') ;
      exit ;
  end ;

  if (qryMastercFlgMovEstoque.Value = 1) then
  begin
      MensagemAlerta('O cr�dito de estoque j� foi processado.') ;
      exit ;
  end ;

  case MessageDlg('Este processo ir� gerar as entradas no estoque e n�o ser� poss�vel emitir uma nota de entrada.' +#13#13 + 'Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryItemEstoque.First;

      while (not qryItemEstoque.Eof) do
      begin
          { -- produto grade -- }
          if (qryItemEstoquenCdTipoItemPed.Value = 1) then
          begin
              qrySubItemEstoque.Close;
              qrySubItemEstoque.Parameters.ParamByName('nPK').Value              := qryMasternCdPedido.Value;
              qrySubItemEstoque.Parameters.ParamByName('nCdItemPedidoPai').Value := qryItemEstoquenCdItemPedido.Value;
              qrySubItemEstoque.Open;

              qrySubItemEstoque.First;

              while (not qrySubItemEstoque.Eof) do
              begin
                  SP_MOVIMENTA_ESTOQUE_ITEM_PEDIDO.Close;
                  SP_MOVIMENTA_ESTOQUE_ITEM_PEDIDO.Parameters.ParamByName('@nCdItemPedido').Value      := qrySubItemEstoquenCdItemPedido.Value;
                  SP_MOVIMENTA_ESTOQUE_ITEM_PEDIDO.Parameters.ParamByName('@nQtde').Value              := qrySubItemEstoquenQtdePed.Value;
                  SP_MOVIMENTA_ESTOQUE_ITEM_PEDIDO.Parameters.ParamByName('@nCdOperacaoEstoque').Value := 0;
                  SP_MOVIMENTA_ESTOQUE_ITEM_PEDIDO.ExecProc;

                  qrySubItemEstoque.Next;
              end;
          end
          { -- produto ERP -- }
          else
          begin
              SP_MOVIMENTA_ESTOQUE_ITEM_PEDIDO.Close;
              SP_MOVIMENTA_ESTOQUE_ITEM_PEDIDO.Parameters.ParamByName('@nCdItemPedido').Value      := qryItemEstoquenCdItemPedido.Value;
              SP_MOVIMENTA_ESTOQUE_ITEM_PEDIDO.Parameters.ParamByName('@nQtde').Value              := qryItemEstoquenQtdePed.Value;
              SP_MOVIMENTA_ESTOQUE_ITEM_PEDIDO.Parameters.ParamByName('@nCdOperacaoEstoque').Value := 0;
              SP_MOVIMENTA_ESTOQUE_ITEM_PEDIDO.ExecProc;
          end;

          qryItemEstoque.Next;
      end;

      qryMaster.Edit;
      qryMastercFlgMovEstoque.Value := 1;
      qryMasternSaldoFat.Value      := 0;
      qryMaster.Post;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      Raise;
      Abort;
  end;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Processamento realizado com sucesso.') ;
end;

procedure TfrmDevolucVenda.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;

  { -- destr�i objetos globais da mem�ria -- }
  FreeAndNil(objGrade);
end;

procedure TfrmDevolucVenda.btItemGradeClick(Sender: TObject);
begin
  if not qryItemEstoque.Active then
  begin
      ShowMessage('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  qryProduto.Close;
  qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
  PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

  if qryProduto.eof then
  begin
      ShowMessage('Selecione um item.') ;
      exit ;
  end ;

  if (qryProdutonCdGrade.Value = 0) then
  begin
      MensagemAlerta('Este item n�o est� configurado para trabalhar em grade.') ;
      exit ;
  end ;

  objGrade.qryValorAnterior.SQL.Text := '' ;

  frmMenu.mensagemUsuario('Montando Grade...');

  if (qryItemEstoquenCdItemPedido.Value > 0) then
      objGrade.qryValorAnterior.SQL.Text := ('SELECT nCdProduto, nQtdePed FROM ItemPedido WITH(INDEX=IN01_ItemPedido) WHERE nCdItemPedidoPai = ' + qryItemEstoquenCdItemPedido.AsString + ' AND nCdTipoItemPed = 4 ORDER BY ncdProduto') ;

  objGrade.PreparaDataSet(qryItemEstoquenCdProduto.Value);

  { -- se pedido j� foi finalizado, monta grade apenas para exibi��o -- }
  if (qryMasternCdTabStatusPed.Value > 1) then
      objGrade.DBGridEh1.ReadOnly := True ;

  frmMenu.mensagemUsuario('Renderizando...');

  objGrade.Renderiza;
  objGrade.DBGridEh1.ReadOnly := False ;

  { -- se pedido esta como digitado, atualiza saldo do item -- }
  if (qryMasternCdTabStatusPed.Value = 1) then
  begin
      frmMenu.mensagemUsuario('Atualizando Item...');

      qryItemEstoque.Edit ;
      qryItemEstoquenQtdePed.Value      := objGrade.nQtdeTotal;
      
      DBGridEh1.Col := DBGridEh1.FieldColumns['nValUnitario'].Index;
      DBGridEh1.SetFocus;
  end ;

  frmMenu.mensagemUsuario('');
end;

procedure TfrmDevolucVenda.DBEdit11Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  qryLoja.Parameters.ParamByName('nPK').Value        := qryMasternCdLoja.Value;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryLoja.Open;
end;

procedure TfrmDevolucVenda.DBEdit11KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  if (DBEdit11.ReadOnly) then
      Exit;

  case key of
      vk_F4 : begin
                  if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
                  begin
                      nPK := frmLookup_Padrao.ExecutaConsulta(59);

                      if (nPK > 0) then
                          qryMasternCdLoja.Value := nPK;
                  end;
              end;
  end;
end;

initialization
  RegisterClass(TfrmDevolucVenda);

end.
