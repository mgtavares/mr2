unit fBaixaColetiva_ConsTitulo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, StdCtrls, ADODB;

type
  TfrmBaixaColetiva_ConsTitulo = class(TfrmProcesso_Padrao)
    GroupBox2: TGroupBox;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    GroupBox1: TGroupBox;
    qryTituloPend: TADOQuery;
    dsTituloPend: TDataSource;
    qryPreparaTemp: TADOQuery;
    qryTituloPendnCdTitulo: TIntegerField;
    qryTituloPendcNrTit: TStringField;
    qryTituloPenddDtVenc: TDateTimeField;
    qryTituloPendcNmEspTit: TStringField;
    qryTituloPendnSaldoAnterior: TBCDField;
    qryTituloPendnValJuro: TBCDField;
    qryTituloPendnValDesconto: TBCDField;
    qryTituloPendnSaldoTit: TBCDField;
    qryTituloPendcFlgAux: TIntegerField;
    dsTituloSel: TDataSource;
    qryTituloSel: TADOQuery;
    qryTituloSelnCdTitulo: TIntegerField;
    qryTituloSelcNrTit: TStringField;
    qryTituloSeldDtVenc: TDateTimeField;
    qryTituloSelcNmEspTit: TStringField;
    qryTituloSelnSaldoAnterior: TBCDField;
    qryTituloSelnValJuro: TBCDField;
    qryTituloSelnValDesconto: TBCDField;
    qryTituloSelnSaldoTit: TBCDField;
    qryTituloSelcFlgAux: TIntegerField;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoAnterior: TcxGridDBColumn;
    cxGrid1DBTableView1nValJuro: TcxGridDBColumn;
    cxGrid1DBTableView1nValDesconto: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1cFlgAux: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdTitulo: TcxGridDBColumn;
    cxGridDBTableView1cNrTit: TcxGridDBColumn;
    cxGridDBTableView1dDtVenc: TcxGridDBColumn;
    cxGridDBTableView1cNmEspTit: TcxGridDBColumn;
    cxGridDBTableView1nSaldoAnterior: TcxGridDBColumn;
    cxGridDBTableView1nValJuro: TcxGridDBColumn;
    cxGridDBTableView1nValDesconto: TcxGridDBColumn;
    cxGridDBTableView1nSaldoTit: TcxGridDBColumn;
    cxGridDBTableView1cFlgAux: TcxGridDBColumn;
    qryAux: TADOQuery;
    qryPopulaTemp: TADOQuery;
    qryGravaTituloSelecionado: TADOQuery;
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdLoteLiq : integer ;
  end;

var
  frmBaixaColetiva_ConsTitulo: TfrmBaixaColetiva_ConsTitulo;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmBaixaColetiva_ConsTitulo.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  if not qryTituloPend.Active or (qryTituloPendncdTitulo.Value = 0) then
      exit ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE #TempTitulo Set cFlgAux = 1 WHERE nCdTitulo = ' + qryTituloPendncdTitulo.AsString) ;
  qryAux.ExecSQL ;

  qryTituloPend.Requery();

  qryTituloSel.Close ;
  qryTituloSel.Open ;

end;

procedure TfrmBaixaColetiva_ConsTitulo.cxGridDBTableView1DblClick(
  Sender: TObject);
begin
  inherited;

  if not qryTituloSel.Active or (qryTituloSelnCdTitulo.Value = 0) then
      exit ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE #TempTitulo Set cFlgAux = 0 WHERE nCdTitulo = ' + qryTituloSelnCdTitulo.AsString) ;
  qryAux.ExecSQL ;

  qryTituloSel.Requery();

  qryTituloPend.Close ;
  qryTituloPend.Open ;

end;

procedure TfrmBaixaColetiva_ConsTitulo.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if not qryTituloSel.Active or (qryTituloSel.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum t�tulo selecionado.') ;
      exit ;
  end ;

  if (MessageDlg('Confirma os t�tulos selecionados ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans ;

  try
      qryGravaTituloSelecionado.Close;
      qryGravaTituloSelecionado.Parameters.ParamByName('nCdLoteLiq').Value := nCdLoteLiq ;
      qryGravaTituloSelecionado.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  Close ;
  
end;

end.
