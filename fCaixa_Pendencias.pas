unit fCaixa_Pendencias;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, StdCtrls, cxPC, ADODB;

type
  TfrmCaixa_Pendencias = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryCrediario: TADOQuery;
    dsCrediario: TDataSource;
    qryCheque: TADOQuery;
    dsCheque: TDataSource;
    qryCrediarionCdCrediario: TIntegerField;
    qryCrediarioiParcela: TIntegerField;
    qryCrediariodDtVenda: TDateTimeField;
    qryCrediariodDtVenc: TDateTimeField;
    qryCrediarionValTit: TBCDField;
    qryCrediarionValJuro: TBCDField;
    qryCrediarionValDesconto: TBCDField;
    qryCrediarionSaldoTit: TBCDField;
    qryCrediarioiDiasAtraso: TIntegerField;
    cxGrid1DBTableView1nCdCrediario: TcxGridDBColumn;
    cxGrid1DBTableView1iParcela: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenda: TcxGridDBColumn;
    cxGrid1DBTableView1dDtVenc: TcxGridDBColumn;
    cxGrid1DBTableView1nValTit: TcxGridDBColumn;
    cxGrid1DBTableView1nValJuro: TcxGridDBColumn;
    cxGrid1DBTableView1nValDesconto: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn;
    cxGrid1DBTableView1iDiasAtraso: TcxGridDBColumn;
    qryChequenCdCheque: TAutoIncField;
    qryChequenCdBanco: TIntegerField;
    qryChequecAgencia: TStringField;
    qryChequecConta: TStringField;
    qryChequecCNPJCPF: TStringField;
    qryChequeiNrCheque: TIntegerField;
    qryChequenValCheque: TBCDField;
    qryChequedDtDeposito: TDateTimeField;
    qryChequecNmTabStatusCheque: TStringField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdCheque: TcxGridDBColumn;
    cxGridDBTableView1nCdBanco: TcxGridDBColumn;
    cxGridDBTableView1cAgencia: TcxGridDBColumn;
    cxGridDBTableView1cConta: TcxGridDBColumn;
    cxGridDBTableView1cCNPJCPF: TcxGridDBColumn;
    cxGridDBTableView1iNrCheque: TcxGridDBColumn;
    cxGridDBTableView1nValCheque: TcxGridDBColumn;
    cxGridDBTableView1dDtDeposito: TcxGridDBColumn;
    cxGridDBTableView1cNmTabStatusCheque: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCaixa_Pendencias: TfrmCaixa_Pendencias;

implementation

{$R *.dfm}

end.
