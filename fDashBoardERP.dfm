inherited frmDashBoardERP: TfrmDashBoardERP
  Left = -8
  Top = -8
  Width = 1164
  Height = 850
  Caption = 'DashBoard ERP'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1148
    Height = 785
  end
  inherited ToolBar1: TToolBar
    Width = 1148
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1148
    Height = 785
    ActivePage = cxTabFinanceiro
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 781
    ClientRectLeft = 4
    ClientRectRight = 1144
    ClientRectTop = 24
    object cxTabFinanceiro: TcxTabSheet
      Caption = 'Financeiro'
      ImageIndex = 1
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 1140
        Height = 49
        Align = alTop
        TabOrder = 0
        object Label3: TLabel
          Tag = 1
          Left = 8
          Top = 24
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo'
        end
        object Label4: TLabel
          Tag = 1
          Left = 152
          Top = 24
          Width = 16
          Height = 13
          Alignment = taRightJustify
          Caption = 'at'#233
        end
        object FinanceirodDtInicial: TDateTimePicker
          Left = 56
          Top = 16
          Width = 89
          Height = 21
          Date = 40569.501631539350000000
          Time = 40569.501631539350000000
          TabOrder = 0
        end
        object FinanceirodDtFinal: TDateTimePicker
          Left = 176
          Top = 16
          Width = 89
          Height = 21
          Date = 40569.501653148150000000
          Time = 40569.501653148150000000
          TabOrder = 1
        end
        object btAtualizarFinanceiro: TcxButton
          Left = 272
          Top = 12
          Width = 137
          Height = 30
          Caption = 'Atualizar'
          TabOrder = 2
          OnClick = btAtualizarFinanceiroClick
          Glyph.Data = {
            6E040000424D6E04000000000000360000002800000013000000120000000100
            1800000000003804000000000000000000000000000000000000C6D6DEC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
            DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6
            D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE0000000000000000008C9C9CC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DEC6DEC6C6D6DEC6D6
            DEC6D6DEC6D6DEC6D6DE00000018A54A18A54A18A54A0000008C9C9CC6D6DEC6
            D6DEC6D6DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
            00000000000018A54A18A54A18A54A18A54A18A54A0000008C9C9CC6D6DEC6D6
            DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6D6DE0000000000000000006B
            D6BD18A54A6BD6BD0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE
            C6D6DE000000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFF0000006BD6
            BD000000EFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE00
            0000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFFEFFFFF000000EFFFFF
            EFFFFFEFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DE000000C6DE
            C6C6D6DE000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFBDBDBDEFFFFFEF
            FFFFEFFFFF0000006BD6BD18A54A18A54A000000C6D6DE000000C6D6DEC6D6DE
            000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDBDBDBDBDBDBDBDBD
            BDEFFFFF0000006BD6BD18A54A000000C6D6DE000000C6D6DEC6D6DE000000BD
            BDBDEFFFFFEFFFFFEFFFFF000000000000000000000000000000EFFFFFEFFFFF
            000000000000000000C6D6DEC6D6DE000000C6D6DEC6D6DE000000BDBDBDEFFF
            FFEFFFFFEFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6
            D6DEC6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DE000000BDBDBDEFFFFFEFFFFF
            EFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6
            DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DE000000BDBDBDEFFFFFEFFFFFEF
            FFFF000000BDBDBDEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6DEC6D6DEC6D6DE
            C6D6DE000000C6D6DEC6DEC6C6D6DE000000BDBDBDBDBDBDEFFFFFEFFFFF0000
            00EFFFFFEFFFFFDEBDD6DEBDD6000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE00
            0000C6D6DEC6D6DEC6D6DEC6D6DE000000000000BDBDBDBDBDBDBDBDBDBDBDBD
            BDBDBD000000000000C6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DE000000C6D6
            DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000000000000000000000C6
            D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DE
            C6DEC6C6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
            DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6DEC6000000C6DEC6C6D6DEC6D6DEC6
            D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000}
          LookAndFeel.NativeStyle = True
        end
        object btFluxoCaixa: TcxButton
          Left = 704
          Top = 12
          Width = 137
          Height = 30
          Caption = 'Fluxo de Caixa'
          TabOrder = 3
          OnClick = btFluxoCaixaClick
          Glyph.Data = {
            0A020000424D0A0200000000000036000000280000000C0000000D0000000100
            180000000000D401000000000000000000000000000000000000C2C2C29E9E9E
            656464656464656464656464656464656464656464656464898A89B9BABAD5D5
            D5515757313332484E4E3133323D42423D42423D42423D4242191D23515757C6
            C6C68C908F191D23313332313332313332313332313332313332313332191D23
            040506898A89656464898A89DADDD7B9BABABEBEBEBEBEBEBEBEBEBEBEBEBEBA
            BCCCCCCC5157576564647172723D4242898A89777B7B7B85857B85857B85857B
            85857B85859392923D4242777B7BB6B6B63133327B8585AEAEAEA4A8A8A4A8A8
            A4A8A8A4A8A8A4A8A8484E4E313332B9BABAD5D5D5484E4EC6C6C6FCFEFEFCFE
            FEFCFEFEFCFEFEFCFEFEFCFEFE898A89484E4EC6C6C6CCCCCC484E4EC6C6C6FC
            FEFEFCFEFEFCFEFEFCFEFEFCFEFEFCFEFE7B85853D4242C2C2C2CCCCCC484E4E
            C6C6C6FCFEFEFCFEFEFCFEFEFCFEFEFCFEFED5D5D57172723D4242C2C2C2CCCC
            CC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE7B8585191D23191D23515757C6
            C6C6CCCCCC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE717272484E4E3D4242
            939292C6C6C6CCCCCC484E4E313332575D5E515757515757575D5E191D23191D
            23939292CCCCCCBEBEBEC2C2C2B6B6B69E9E9E9E9E9E9E9E9E9E9E9E9E9E9EA4
            A8A8AEAEAEC6C6C6BEBEBEBEBEBE}
          LookAndFeel.NativeStyle = True
        end
        object btOrcamento: TcxButton
          Left = 848
          Top = 12
          Width = 137
          Height = 30
          Caption = 'Or'#231'amento Realizado'
          TabOrder = 4
          OnClick = btOrcamentoClick
          Glyph.Data = {
            0A020000424D0A0200000000000036000000280000000C0000000D0000000100
            180000000000D401000000000000000000000000000000000000C2C2C29E9E9E
            656464656464656464656464656464656464656464656464898A89B9BABAD5D5
            D5515757313332484E4E3133323D42423D42423D42423D4242191D23515757C6
            C6C68C908F191D23313332313332313332313332313332313332313332191D23
            040506898A89656464898A89DADDD7B9BABABEBEBEBEBEBEBEBEBEBEBEBEBEBA
            BCCCCCCC5157576564647172723D4242898A89777B7B7B85857B85857B85857B
            85857B85859392923D4242777B7BB6B6B63133327B8585AEAEAEA4A8A8A4A8A8
            A4A8A8A4A8A8A4A8A8484E4E313332B9BABAD5D5D5484E4EC6C6C6FCFEFEFCFE
            FEFCFEFEFCFEFEFCFEFEFCFEFE898A89484E4EC6C6C6CCCCCC484E4EC6C6C6FC
            FEFEFCFEFEFCFEFEFCFEFEFCFEFEFCFEFE7B85853D4242C2C2C2CCCCCC484E4E
            C6C6C6FCFEFEFCFEFEFCFEFEFCFEFEFCFEFED5D5D57172723D4242C2C2C2CCCC
            CC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE7B8585191D23191D23515757C6
            C6C6CCCCCC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE717272484E4E3D4242
            939292C6C6C6CCCCCC484E4E313332575D5E515757515757575D5E191D23191D
            23939292CCCCCCBEBEBEC2C2C2B6B6B69E9E9E9E9E9E9E9E9E9E9E9E9E9E9EA4
            A8A8AEAEAEC6C6C6BEBEBEBEBEBE}
          LookAndFeel.NativeStyle = True
        end
        object btDRE: TcxButton
          Left = 992
          Top = 12
          Width = 137
          Height = 30
          Caption = 'DRE'
          TabOrder = 5
          OnClick = btDREClick
          Glyph.Data = {
            0A020000424D0A0200000000000036000000280000000C0000000D0000000100
            180000000000D401000000000000000000000000000000000000C2C2C29E9E9E
            656464656464656464656464656464656464656464656464898A89B9BABAD5D5
            D5515757313332484E4E3133323D42423D42423D42423D4242191D23515757C6
            C6C68C908F191D23313332313332313332313332313332313332313332191D23
            040506898A89656464898A89DADDD7B9BABABEBEBEBEBEBEBEBEBEBEBEBEBEBA
            BCCCCCCC5157576564647172723D4242898A89777B7B7B85857B85857B85857B
            85857B85859392923D4242777B7BB6B6B63133327B8585AEAEAEA4A8A8A4A8A8
            A4A8A8A4A8A8A4A8A8484E4E313332B9BABAD5D5D5484E4EC6C6C6FCFEFEFCFE
            FEFCFEFEFCFEFEFCFEFEFCFEFE898A89484E4EC6C6C6CCCCCC484E4EC6C6C6FC
            FEFEFCFEFEFCFEFEFCFEFEFCFEFEFCFEFE7B85853D4242C2C2C2CCCCCC484E4E
            C6C6C6FCFEFEFCFEFEFCFEFEFCFEFEFCFEFED5D5D57172723D4242C2C2C2CCCC
            CC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE7B8585191D23191D23515757C6
            C6C6CCCCCC484E4EC6C6C6FCFEFEFCFEFEFCFEFEFCFEFE717272484E4E3D4242
            939292C6C6C6CCCCCC484E4E313332575D5E515757515757575D5E191D23191D
            23939292CCCCCCBEBEBEC2C2C2B6B6B69E9E9E9E9E9E9E9E9E9E9E9E9E9E9EA4
            A8A8AEAEAEC6C6C6BEBEBEBEBEBE}
          LookAndFeel.NativeStyle = True
        end
      end
      object cxPageControl10: TcxPageControl
        Left = 0
        Top = 242
        Width = 1140
        Height = 515
        ActivePage = cxTabSheet10
        Align = alClient
        LookAndFeel.NativeStyle = True
        TabOrder = 1
        ClientRectBottom = 511
        ClientRectLeft = 4
        ClientRectRight = 1136
        ClientRectTop = 24
        object cxTabSheet10: TcxTabSheet
          Caption = 'T'#237'tulos a Pagar'
          ImageIndex = 0
          object cxPageControl12: TcxPageControl
            Left = 0
            Top = 0
            Width = 1132
            Height = 487
            ActivePage = cxPagarHoje
            Align = alClient
            LookAndFeel.NativeStyle = True
            TabOrder = 0
            ClientRectBottom = 483
            ClientRectLeft = 4
            ClientRectRight = 1128
            ClientRectTop = 24
            object cxPagarHoje: TcxTabSheet
              Caption = 'Vencendo Hoje'
              ImageIndex = 0
              object cxGrid5: TcxGrid
                Left = 0
                Top = 0
                Width = 1124
                Height = 459
                Align = alClient
                TabOrder = 0
                LookAndFeel.NativeStyle = True
                object cxGridDBTableView4: TcxGridDBTableView
                  DataController.DataSource = dsPagarHoje
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                      Column = cxGridDBTableView4nSaldoTit
                    end>
                  DataController.Summary.SummaryGroups = <>
                  NavigatorButtons.ConfirmDelete = False
                  OptionsCustomize.ColumnFiltering = False
                  OptionsCustomize.ColumnGrouping = False
                  OptionsCustomize.ColumnHidingOnGrouping = False
                  OptionsCustomize.ColumnMoving = False
                  OptionsCustomize.ColumnSorting = False
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsView.Footer = True
                  OptionsView.GroupByBox = False
                  object cxGridDBTableView4dDtVenc: TcxGridDBColumn
                    Caption = 'Dt. Vencto'
                    DataBinding.FieldName = 'dDtVenc'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 85
                  end
                  object cxGridDBTableView4nCdTitulo: TcxGridDBColumn
                    Caption = 'ID T'#237'tulo'
                    DataBinding.FieldName = 'nCdTitulo'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBTableView4cNrTit: TcxGridDBColumn
                    Caption = 'N'#250'm. T'#237'tulo'
                    DataBinding.FieldName = 'cNrTit'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBTableView4cNrNF: TcxGridDBColumn
                    Caption = 'N'#250'm. NF'
                    DataBinding.FieldName = 'cNrNF'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 57
                  end
                  object cxGridDBTableView4iParcela: TcxGridDBColumn
                    Caption = 'Parcela'
                    DataBinding.FieldName = 'iParcela'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 45
                  end
                  object cxGridDBTableView4cNmEspTit: TcxGridDBColumn
                    Caption = 'Esp'#233'cie T'#237'tulo'
                    DataBinding.FieldName = 'cNmEspTit'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 150
                  end
                  object cxGridDBTableView4cNmTerceiro: TcxGridDBColumn
                    Caption = 'Terceiro Credor'
                    DataBinding.FieldName = 'cNmTerceiro'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 229
                  end
                  object cxGridDBTableView4cNmCategFinanc: TcxGridDBColumn
                    Caption = 'Categoria Financeira'
                    DataBinding.FieldName = 'cNmCategFinanc'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 150
                  end
                  object cxGridDBTableView4nSaldoTit: TcxGridDBColumn
                    Caption = 'Saldo Pagar'
                    DataBinding.FieldName = 'nSaldoTit'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 103
                  end
                  object cxGridDBTableView4cSenso: TcxGridDBColumn
                    Caption = 'Senso'
                    DataBinding.FieldName = 'cSenso'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 43
                  end
                end
                object cxGridLevel4: TcxGridLevel
                  GridView = cxGridDBTableView4
                end
              end
            end
            object cxPagarAtraso: TcxTabSheet
              Caption = 'Atrasados'
              ImageIndex = 1
              object cxGrid7: TcxGrid
                Left = 0
                Top = 0
                Width = 1124
                Height = 459
                Align = alClient
                TabOrder = 0
                LookAndFeel.NativeStyle = True
                object cxGridDBTableView6: TcxGridDBTableView
                  DataController.DataSource = dsPagarAtraso
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                      Column = cxGridDBColumn16
                    end>
                  DataController.Summary.SummaryGroups = <>
                  NavigatorButtons.ConfirmDelete = False
                  OptionsCustomize.ColumnFiltering = False
                  OptionsCustomize.ColumnGrouping = False
                  OptionsCustomize.ColumnHidingOnGrouping = False
                  OptionsCustomize.ColumnMoving = False
                  OptionsCustomize.ColumnSorting = False
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsView.Footer = True
                  OptionsView.GroupByBox = False
                  object cxGridDBColumn8: TcxGridDBColumn
                    Caption = 'Dt. Vencto'
                    DataBinding.FieldName = 'dDtVenc'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 85
                  end
                  object cxGridDBColumn9: TcxGridDBColumn
                    Caption = 'ID T'#237'tulo'
                    DataBinding.FieldName = 'nCdTitulo'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBColumn10: TcxGridDBColumn
                    Caption = 'N'#250'm. T'#237'tulo'
                    DataBinding.FieldName = 'cNrTit'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBColumn11: TcxGridDBColumn
                    Caption = 'N'#250'm. NF'
                    DataBinding.FieldName = 'cNrNF'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 57
                  end
                  object cxGridDBColumn12: TcxGridDBColumn
                    Caption = 'Parcela'
                    DataBinding.FieldName = 'iParcela'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 45
                  end
                  object cxGridDBColumn13: TcxGridDBColumn
                    Caption = 'Esp'#233'cie T'#237'tulo'
                    DataBinding.FieldName = 'cNmEspTit'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 150
                  end
                  object cxGridDBColumn14: TcxGridDBColumn
                    Caption = 'Terceiro Credor'
                    DataBinding.FieldName = 'cNmTerceiro'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 229
                  end
                  object cxGridDBColumn15: TcxGridDBColumn
                    Caption = 'Categoria Financeira'
                    DataBinding.FieldName = 'cNmCategFinanc'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 150
                  end
                  object cxGridDBColumn16: TcxGridDBColumn
                    Caption = 'Saldo Pagar'
                    DataBinding.FieldName = 'nSaldoTit'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 103
                  end
                  object cxGridDBColumn17: TcxGridDBColumn
                    Caption = 'Senso'
                    DataBinding.FieldName = 'cSenso'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 43
                  end
                end
                object cxGridLevel6: TcxGridLevel
                  GridView = cxGridDBTableView6
                end
              end
            end
            object cxPagarTotal: TcxTabSheet
              Caption = 'Total do Per'#237'odo'
              ImageIndex = 2
              object cxGrid8: TcxGrid
                Left = 0
                Top = 0
                Width = 1124
                Height = 459
                Align = alClient
                TabOrder = 0
                LookAndFeel.NativeStyle = True
                object cxGridDBTableView7: TcxGridDBTableView
                  DataController.DataSource = dsPagarPeriodo
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                      Column = cxGridDBColumn26
                    end>
                  DataController.Summary.SummaryGroups = <>
                  NavigatorButtons.ConfirmDelete = False
                  OptionsCustomize.ColumnFiltering = False
                  OptionsCustomize.ColumnGrouping = False
                  OptionsCustomize.ColumnHidingOnGrouping = False
                  OptionsCustomize.ColumnMoving = False
                  OptionsCustomize.ColumnSorting = False
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsView.Footer = True
                  OptionsView.GroupByBox = False
                  object cxGridDBColumn18: TcxGridDBColumn
                    Caption = 'Dt. Vencto'
                    DataBinding.FieldName = 'dDtVenc'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 85
                  end
                  object cxGridDBColumn19: TcxGridDBColumn
                    Caption = 'ID T'#237'tulo'
                    DataBinding.FieldName = 'nCdTitulo'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBColumn20: TcxGridDBColumn
                    Caption = 'N'#250'm. T'#237'tulo'
                    DataBinding.FieldName = 'cNrTit'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBColumn21: TcxGridDBColumn
                    Caption = 'N'#250'm. NF'
                    DataBinding.FieldName = 'cNrNF'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 57
                  end
                  object cxGridDBColumn22: TcxGridDBColumn
                    Caption = 'Parcela'
                    DataBinding.FieldName = 'iParcela'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 45
                  end
                  object cxGridDBColumn23: TcxGridDBColumn
                    Caption = 'Esp'#233'cie T'#237'tulo'
                    DataBinding.FieldName = 'cNmEspTit'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 150
                  end
                  object cxGridDBColumn24: TcxGridDBColumn
                    Caption = 'Terceiro Credor'
                    DataBinding.FieldName = 'cNmTerceiro'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 229
                  end
                  object cxGridDBColumn25: TcxGridDBColumn
                    Caption = 'Categoria Financeira'
                    DataBinding.FieldName = 'cNmCategFinanc'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 150
                  end
                  object cxGridDBColumn26: TcxGridDBColumn
                    Caption = 'Saldo Pagar'
                    DataBinding.FieldName = 'nSaldoTit'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 103
                  end
                  object cxGridDBColumn27: TcxGridDBColumn
                    Caption = 'Senso'
                    DataBinding.FieldName = 'cSenso'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 43
                  end
                end
                object cxGridLevel7: TcxGridLevel
                  GridView = cxGridDBTableView7
                end
              end
            end
          end
        end
        object cxTabSheet12: TcxTabSheet
          Caption = 'T'#237'tulos a Receber'
          ImageIndex = 1
          object cxPageControl13: TcxPageControl
            Left = 0
            Top = 0
            Width = 1132
            Height = 487
            ActivePage = cxTabSheet13
            Align = alClient
            LookAndFeel.NativeStyle = True
            TabOrder = 0
            ClientRectBottom = 483
            ClientRectLeft = 4
            ClientRectRight = 1128
            ClientRectTop = 24
            object cxTabSheet13: TcxTabSheet
              Caption = 'Vencendo Hoje'
              ImageIndex = 0
              object cxGrid9: TcxGrid
                Left = 0
                Top = 0
                Width = 1124
                Height = 459
                Align = alClient
                TabOrder = 0
                LookAndFeel.NativeStyle = True
                object cxGridDBTableView8: TcxGridDBTableView
                  DataController.DataSource = dsReceberHoje
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                      Column = cxGridDBColumn36
                    end>
                  DataController.Summary.SummaryGroups = <>
                  NavigatorButtons.ConfirmDelete = False
                  OptionsCustomize.ColumnFiltering = False
                  OptionsCustomize.ColumnGrouping = False
                  OptionsCustomize.ColumnHidingOnGrouping = False
                  OptionsCustomize.ColumnMoving = False
                  OptionsCustomize.ColumnSorting = False
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsView.Footer = True
                  OptionsView.GroupByBox = False
                  object cxGridDBColumn28: TcxGridDBColumn
                    Caption = 'Dt. Vencto'
                    DataBinding.FieldName = 'dDtVenc'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 85
                  end
                  object cxGridDBColumn29: TcxGridDBColumn
                    Caption = 'ID T'#237'tulo'
                    DataBinding.FieldName = 'nCdTitulo'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBColumn30: TcxGridDBColumn
                    Caption = 'N'#250'm. T'#237'tulo'
                    DataBinding.FieldName = 'cNrTit'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBColumn31: TcxGridDBColumn
                    Caption = 'N'#250'm. NF'
                    DataBinding.FieldName = 'cNrNF'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 57
                  end
                  object cxGridDBColumn32: TcxGridDBColumn
                    Caption = 'Parcela'
                    DataBinding.FieldName = 'iParcela'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 45
                  end
                  object cxGridDBColumn33: TcxGridDBColumn
                    Caption = 'Esp'#233'cie T'#237'tulo'
                    DataBinding.FieldName = 'cNmEspTit'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 150
                  end
                  object cxGridDBColumn34: TcxGridDBColumn
                    Caption = 'Terceiro Credor'
                    DataBinding.FieldName = 'cNmTerceiro'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 229
                  end
                  object cxGridDBColumn35: TcxGridDBColumn
                    Caption = 'Categoria Financeira'
                    DataBinding.FieldName = 'cNmCategFinanc'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 150
                  end
                  object cxGridDBColumn36: TcxGridDBColumn
                    Caption = 'Saldo Pagar'
                    DataBinding.FieldName = 'nSaldoTit'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 103
                  end
                  object cxGridDBColumn37: TcxGridDBColumn
                    Caption = 'Senso'
                    DataBinding.FieldName = 'cSenso'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 43
                  end
                end
                object cxGridLevel8: TcxGridLevel
                  GridView = cxGridDBTableView8
                end
              end
            end
            object cxTabSheet14: TcxTabSheet
              Caption = 'Atrasados'
              ImageIndex = 1
              object cxGrid10: TcxGrid
                Left = 0
                Top = 0
                Width = 1124
                Height = 459
                Align = alClient
                TabOrder = 0
                LookAndFeel.NativeStyle = True
                object cxGridDBTableView9: TcxGridDBTableView
                  DataController.DataSource = dsReceberAtraso
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                      Column = cxGridDBColumn46
                    end>
                  DataController.Summary.SummaryGroups = <>
                  NavigatorButtons.ConfirmDelete = False
                  OptionsCustomize.ColumnFiltering = False
                  OptionsCustomize.ColumnGrouping = False
                  OptionsCustomize.ColumnHidingOnGrouping = False
                  OptionsCustomize.ColumnMoving = False
                  OptionsCustomize.ColumnSorting = False
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsView.Footer = True
                  OptionsView.GroupByBox = False
                  object cxGridDBColumn38: TcxGridDBColumn
                    Caption = 'Dt. Vencto'
                    DataBinding.FieldName = 'dDtVenc'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 85
                  end
                  object cxGridDBColumn39: TcxGridDBColumn
                    Caption = 'ID T'#237'tulo'
                    DataBinding.FieldName = 'nCdTitulo'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBColumn40: TcxGridDBColumn
                    Caption = 'N'#250'm. T'#237'tulo'
                    DataBinding.FieldName = 'cNrTit'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBColumn41: TcxGridDBColumn
                    Caption = 'N'#250'm. NF'
                    DataBinding.FieldName = 'cNrNF'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 57
                  end
                  object cxGridDBColumn42: TcxGridDBColumn
                    Caption = 'Parcela'
                    DataBinding.FieldName = 'iParcela'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 45
                  end
                  object cxGridDBColumn43: TcxGridDBColumn
                    Caption = 'Esp'#233'cie T'#237'tulo'
                    DataBinding.FieldName = 'cNmEspTit'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 150
                  end
                  object cxGridDBColumn44: TcxGridDBColumn
                    Caption = 'Terceiro Credor'
                    DataBinding.FieldName = 'cNmTerceiro'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 229
                  end
                  object cxGridDBColumn45: TcxGridDBColumn
                    Caption = 'Categoria Financeira'
                    DataBinding.FieldName = 'cNmCategFinanc'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 150
                  end
                  object cxGridDBColumn46: TcxGridDBColumn
                    Caption = 'Saldo Pagar'
                    DataBinding.FieldName = 'nSaldoTit'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 103
                  end
                  object cxGridDBColumn47: TcxGridDBColumn
                    Caption = 'Senso'
                    DataBinding.FieldName = 'cSenso'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 43
                  end
                end
                object cxGridLevel9: TcxGridLevel
                  GridView = cxGridDBTableView9
                end
              end
            end
            object cxTabSheet15: TcxTabSheet
              Caption = 'Total do Per'#237'odo'
              ImageIndex = 2
              object cxGrid11: TcxGrid
                Left = 0
                Top = 0
                Width = 1124
                Height = 459
                Align = alClient
                TabOrder = 0
                LookAndFeel.NativeStyle = True
                object cxGridDBTableView10: TcxGridDBTableView
                  DataController.DataSource = dsReceberPeriodo
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                    end
                    item
                      Format = '#,##0.00'
                      Kind = skSum
                      Column = cxGridDBColumn56
                    end>
                  DataController.Summary.SummaryGroups = <>
                  NavigatorButtons.ConfirmDelete = False
                  OptionsCustomize.ColumnFiltering = False
                  OptionsCustomize.ColumnGrouping = False
                  OptionsCustomize.ColumnHidingOnGrouping = False
                  OptionsCustomize.ColumnMoving = False
                  OptionsCustomize.ColumnSorting = False
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsView.Footer = True
                  OptionsView.GroupByBox = False
                  object cxGridDBColumn48: TcxGridDBColumn
                    Caption = 'Dt. Vencto'
                    DataBinding.FieldName = 'dDtVenc'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 85
                  end
                  object cxGridDBColumn49: TcxGridDBColumn
                    Caption = 'ID T'#237'tulo'
                    DataBinding.FieldName = 'nCdTitulo'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBColumn50: TcxGridDBColumn
                    Caption = 'N'#250'm. T'#237'tulo'
                    DataBinding.FieldName = 'cNrTit'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBColumn51: TcxGridDBColumn
                    Caption = 'N'#250'm. NF'
                    DataBinding.FieldName = 'cNrNF'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 57
                  end
                  object cxGridDBColumn52: TcxGridDBColumn
                    Caption = 'Parcela'
                    DataBinding.FieldName = 'iParcela'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 45
                  end
                  object cxGridDBColumn53: TcxGridDBColumn
                    Caption = 'Esp'#233'cie T'#237'tulo'
                    DataBinding.FieldName = 'cNmEspTit'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 150
                  end
                  object cxGridDBColumn54: TcxGridDBColumn
                    Caption = 'Terceiro Credor'
                    DataBinding.FieldName = 'cNmTerceiro'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 229
                  end
                  object cxGridDBColumn55: TcxGridDBColumn
                    Caption = 'Categoria Financeira'
                    DataBinding.FieldName = 'cNmCategFinanc'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 150
                  end
                  object cxGridDBColumn56: TcxGridDBColumn
                    Caption = 'Saldo Pagar'
                    DataBinding.FieldName = 'nSaldoTit'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 103
                  end
                  object cxGridDBColumn57: TcxGridDBColumn
                    Caption = 'Senso'
                    DataBinding.FieldName = 'cSenso'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 43
                  end
                end
                object cxGridLevel10: TcxGridLevel
                  GridView = cxGridDBTableView10
                end
              end
            end
          end
        end
      end
      object cxPageControl11: TcxPageControl
        Left = 0
        Top = 49
        Width = 1140
        Height = 193
        ActivePage = cxTabSheet11
        Align = alTop
        LookAndFeel.NativeStyle = True
        TabOrder = 2
        ClientRectBottom = 189
        ClientRectLeft = 4
        ClientRectRight = 1136
        ClientRectTop = 24
        object cxTabSheet11: TcxTabSheet
          Caption = 'Disponibilidade Imediata'
          ImageIndex = 0
          object cxGrid6: TcxGrid
            Left = 0
            Top = 0
            Width = 1132
            Height = 165
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView5: TcxGridDBTableView
              DataController.DataSource = dsDispCaixa
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  Column = cxGridDBColumn4
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  Column = cxGridDBColumn6
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  Column = cxGridDBColumn7
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              OptionsCustomize.ColumnFiltering = False
              OptionsCustomize.ColumnGrouping = False
              OptionsCustomize.ColumnHidingOnGrouping = False
              OptionsCustomize.ColumnMoving = False
              OptionsCustomize.ColumnSorting = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              object cxGridDBColumn1: TcxGridDBColumn
                Caption = 'Banco'
                DataBinding.FieldName = 'cBanco'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 202
              end
              object cxGridDBColumn2: TcxGridDBColumn
                Caption = 'Ag'#234'ncia'
                DataBinding.FieldName = 'cAgencia'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn3: TcxGridDBColumn
                Caption = 'N'#250'mero Conta'
                DataBinding.FieldName = 'nCdConta'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 103
              end
              object cxGridDBColumn4: TcxGridDBColumn
                Caption = 'Saldo Dispon'#237'vel'
                DataBinding.FieldName = 'nSaldoConta'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 137
              end
              object cxGridDBColumn5: TcxGridDBColumn
                Caption = 'Data Saldo'
                DataBinding.FieldName = 'dDtUltConciliacao'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn6: TcxGridDBColumn
                Caption = 'Limite Cr'#233'dito'
                DataBinding.FieldName = 'nValLimiteCredito'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 137
              end
              object cxGridDBColumn7: TcxGridDBColumn
                Caption = 'Saldo Total'
                DataBinding.FieldName = 'nSaldoTotal'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 137
              end
            end
            object cxGridLevel5: TcxGridLevel
              GridView = cxGridDBTableView5
            end
          end
        end
      end
    end
    object cxTop10: TcxTabSheet
      Caption = 'Top 10'
      ImageIndex = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1140
        Height = 49
        Align = alTop
        TabOrder = 0
        object Label1: TLabel
          Tag = 1
          Left = 8
          Top = 24
          Width = 36
          Height = 13
          Alignment = taRightJustify
          Caption = 'Per'#237'odo'
        end
        object Label2: TLabel
          Tag = 1
          Left = 152
          Top = 24
          Width = 16
          Height = 13
          Alignment = taRightJustify
          Caption = 'at'#233
        end
        object Top10dDtInicial: TDateTimePicker
          Left = 56
          Top = 16
          Width = 89
          Height = 21
          Date = 40569.501631539350000000
          Time = 40569.501631539350000000
          TabOrder = 0
        end
        object Top10dDtFinal: TDateTimePicker
          Left = 176
          Top = 16
          Width = 89
          Height = 21
          Date = 40569.501653148150000000
          Time = 40569.501653148150000000
          TabOrder = 1
        end
        object btAtualizarTop10: TcxButton
          Left = 272
          Top = 12
          Width = 86
          Height = 30
          Caption = 'Atualizar'
          TabOrder = 2
          OnClick = btAtualizarTop10Click
          Glyph.Data = {
            6E040000424D6E04000000000000360000002800000013000000120000000100
            1800000000003804000000000000000000000000000000000000C6D6DEC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
            DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6
            D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE0000000000000000008C9C9CC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DEC6DEC6C6D6DEC6D6
            DEC6D6DEC6D6DEC6D6DE00000018A54A18A54A18A54A0000008C9C9CC6D6DEC6
            D6DEC6D6DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
            00000000000018A54A18A54A18A54A18A54A18A54A0000008C9C9CC6D6DEC6D6
            DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6D6DE0000000000000000006B
            D6BD18A54A6BD6BD0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE
            C6D6DE000000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFF0000006BD6
            BD000000EFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE00
            0000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFFEFFFFF000000EFFFFF
            EFFFFFEFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DE000000C6DE
            C6C6D6DE000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFBDBDBDEFFFFFEF
            FFFFEFFFFF0000006BD6BD18A54A18A54A000000C6D6DE000000C6D6DEC6D6DE
            000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDBDBDBDBDBDBDBDBD
            BDEFFFFF0000006BD6BD18A54A000000C6D6DE000000C6D6DEC6D6DE000000BD
            BDBDEFFFFFEFFFFFEFFFFF000000000000000000000000000000EFFFFFEFFFFF
            000000000000000000C6D6DEC6D6DE000000C6D6DEC6D6DE000000BDBDBDEFFF
            FFEFFFFFEFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6
            D6DEC6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DE000000BDBDBDEFFFFFEFFFFF
            EFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6
            DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DE000000BDBDBDEFFFFFEFFFFFEF
            FFFF000000BDBDBDEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6DEC6D6DEC6D6DE
            C6D6DE000000C6D6DEC6DEC6C6D6DE000000BDBDBDBDBDBDEFFFFFEFFFFF0000
            00EFFFFFEFFFFFDEBDD6DEBDD6000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE00
            0000C6D6DEC6D6DEC6D6DEC6D6DE000000000000BDBDBDBDBDBDBDBDBDBDBDBD
            BDBDBD000000000000C6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DE000000C6D6
            DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000000000000000000000C6
            D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DE
            C6DEC6C6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
            DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6DEC6000000C6DEC6C6D6DEC6D6DEC6
            D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000}
          LookAndFeel.NativeStyle = True
        end
      end
      object cxPageControl2: TcxPageControl
        Left = 0
        Top = 57
        Width = 553
        Height = 288
        ActivePage = cxTop10Clientes
        LookAndFeel.NativeStyle = True
        TabOrder = 1
        ClientRectBottom = 284
        ClientRectLeft = 4
        ClientRectRight = 549
        ClientRectTop = 24
        object cxTop10Clientes: TcxTabSheet
          Caption = 'Clientes'
          ImageIndex = 0
          object cxPageControl6: TcxPageControl
            Left = 0
            Top = 0
            Width = 545
            Height = 260
            ActivePage = cxTop10ClienteDados
            Align = alClient
            LookAndFeel.NativeStyle = True
            TabOrder = 0
            ClientRectBottom = 256
            ClientRectLeft = 4
            ClientRectRight = 541
            ClientRectTop = 24
            object cxTop10ClienteDados: TcxTabSheet
              Caption = 'Dados'
              ImageIndex = 1
              object cxGrid1: TcxGrid
                Left = 0
                Top = 0
                Width = 537
                Height = 232
                Align = alClient
                TabOrder = 0
                LookAndFeel.NativeStyle = True
                object cxGrid1DBTableView1: TcxGridDBTableView
                  DataController.DataSource = dsTop10Clientes
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  NavigatorButtons.ConfirmDelete = False
                  OptionsCustomize.ColumnFiltering = False
                  OptionsCustomize.ColumnGrouping = False
                  OptionsCustomize.ColumnHidingOnGrouping = False
                  OptionsCustomize.ColumnMoving = False
                  OptionsCustomize.ColumnSorting = False
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsView.GroupByBox = False
                  object cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn
                    Caption = 'C'#243'digo'
                    DataBinding.FieldName = 'nCdTerceiro'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn
                    Caption = 'Cliente'
                    DataBinding.FieldName = 'cNmTerceiro'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 261
                  end
                  object cxGrid1DBTableView1nValTotalPedido: TcxGridDBColumn
                    Caption = 'Total Pedidos'
                    DataBinding.FieldName = 'nValTotalPedido'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 136
                  end
                end
                object cxGrid1Level1: TcxGridLevel
                  GridView = cxGrid1DBTableView1
                end
              end
            end
          end
        end
      end
      object cxPageControl3: TcxPageControl
        Left = 568
        Top = 57
        Width = 560
        Height = 288
        ActivePage = cxTabSheet1
        LookAndFeel.NativeStyle = True
        TabOrder = 2
        ClientRectBottom = 284
        ClientRectLeft = 4
        ClientRectRight = 556
        ClientRectTop = 24
        object cxTabSheet1: TcxTabSheet
          Caption = 'Fornecedores'
          ImageIndex = 0
          object cxPageControl7: TcxPageControl
            Left = 0
            Top = 0
            Width = 552
            Height = 260
            ActivePage = cxTabSheet5
            Align = alClient
            LookAndFeel.NativeStyle = True
            TabOrder = 0
            ClientRectBottom = 256
            ClientRectLeft = 4
            ClientRectRight = 548
            ClientRectTop = 24
            object cxTabSheet5: TcxTabSheet
              Caption = 'Dados'
              ImageIndex = 1
              object cxGrid2: TcxGrid
                Left = 0
                Top = 0
                Width = 544
                Height = 232
                Align = alClient
                TabOrder = 0
                LookAndFeel.NativeStyle = True
                object cxGridDBTableView1: TcxGridDBTableView
                  DataController.DataSource = dsTop10Fornecedores
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  NavigatorButtons.ConfirmDelete = False
                  OptionsCustomize.ColumnFiltering = False
                  OptionsCustomize.ColumnGrouping = False
                  OptionsCustomize.ColumnHidingOnGrouping = False
                  OptionsCustomize.ColumnMoving = False
                  OptionsCustomize.ColumnSorting = False
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsView.GroupByBox = False
                  object cxGridDBTableView1nCdTerceiro: TcxGridDBColumn
                    Caption = 'C'#243'digo'
                    DataBinding.FieldName = 'nCdTerceiro'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBTableView1cNmTerceiro: TcxGridDBColumn
                    Caption = 'Fornecedor'
                    DataBinding.FieldName = 'cNmTerceiro'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 261
                  end
                  object cxGridDBTableView1nValTotalReceb: TcxGridDBColumn
                    Caption = 'Total Recebido'
                    DataBinding.FieldName = 'nValTotalReceb'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 136
                  end
                end
                object cxGridLevel1: TcxGridLevel
                  GridView = cxGridDBTableView1
                end
              end
            end
          end
        end
      end
      object cxPageControl4: TcxPageControl
        Left = 0
        Top = 393
        Width = 553
        Height = 288
        ActivePage = cxTabSheet2
        LookAndFeel.NativeStyle = True
        TabOrder = 3
        ClientRectBottom = 284
        ClientRectLeft = 4
        ClientRectRight = 549
        ClientRectTop = 24
        object cxTabSheet2: TcxTabSheet
          Caption = 'Representantes'
          ImageIndex = 0
          object cxPageControl9: TcxPageControl
            Left = 0
            Top = 0
            Width = 545
            Height = 260
            ActivePage = cxTabSheet9
            Align = alClient
            LookAndFeel.NativeStyle = True
            TabOrder = 0
            ClientRectBottom = 256
            ClientRectLeft = 4
            ClientRectRight = 541
            ClientRectTop = 24
            object cxTabSheet9: TcxTabSheet
              Caption = 'Dados'
              ImageIndex = 1
              object cxGrid4: TcxGrid
                Left = 0
                Top = 0
                Width = 537
                Height = 232
                Align = alClient
                TabOrder = 0
                LookAndFeel.NativeStyle = True
                object cxGridDBTableView3: TcxGridDBTableView
                  DataController.DataSource = dsTop10Representantes
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  NavigatorButtons.ConfirmDelete = False
                  OptionsCustomize.ColumnFiltering = False
                  OptionsCustomize.ColumnGrouping = False
                  OptionsCustomize.ColumnHidingOnGrouping = False
                  OptionsCustomize.ColumnMoving = False
                  OptionsCustomize.ColumnSorting = False
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsView.GroupByBox = False
                  object cxGridDBTableView3nCdTerceiro: TcxGridDBColumn
                    Caption = 'C'#243'digo'
                    DataBinding.FieldName = 'nCdTerceiroRepres'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBTableView3cNmTerceiro: TcxGridDBColumn
                    Caption = 'Representante'
                    DataBinding.FieldName = 'cNmTerceiro'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 261
                  end
                  object cxGridDBTableView3nValTotalPedido: TcxGridDBColumn
                    Caption = 'Valor Venda'
                    DataBinding.FieldName = 'nValTotalPedido'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 136
                  end
                end
                object cxGridLevel3: TcxGridLevel
                  GridView = cxGridDBTableView3
                end
              end
            end
          end
        end
      end
      object cxPageControl5: TcxPageControl
        Left = 568
        Top = 393
        Width = 553
        Height = 288
        ActivePage = cxTabSheet3
        LookAndFeel.NativeStyle = True
        TabOrder = 4
        ClientRectBottom = 284
        ClientRectLeft = 4
        ClientRectRight = 549
        ClientRectTop = 24
        object cxTabSheet3: TcxTabSheet
          Caption = 'Produtos'
          ImageIndex = 0
          object cxPageControl8: TcxPageControl
            Left = 0
            Top = 0
            Width = 545
            Height = 260
            ActivePage = cxTabSheet7
            Align = alClient
            LookAndFeel.NativeStyle = True
            TabOrder = 0
            ClientRectBottom = 256
            ClientRectLeft = 4
            ClientRectRight = 541
            ClientRectTop = 24
            object cxTabSheet7: TcxTabSheet
              Caption = 'Dados'
              ImageIndex = 1
              object cxGrid3: TcxGrid
                Left = 0
                Top = 0
                Width = 537
                Height = 232
                Align = alClient
                TabOrder = 0
                LookAndFeel.NativeStyle = True
                object cxGridDBTableView2: TcxGridDBTableView
                  DataController.DataSource = dsTop10Produtos
                  DataController.Summary.DefaultGroupSummaryItems = <>
                  DataController.Summary.FooterSummaryItems = <>
                  DataController.Summary.SummaryGroups = <>
                  NavigatorButtons.ConfirmDelete = False
                  OptionsCustomize.ColumnFiltering = False
                  OptionsCustomize.ColumnGrouping = False
                  OptionsCustomize.ColumnHidingOnGrouping = False
                  OptionsCustomize.ColumnMoving = False
                  OptionsCustomize.ColumnSorting = False
                  OptionsData.CancelOnExit = False
                  OptionsData.Deleting = False
                  OptionsData.DeletingConfirmation = False
                  OptionsData.Editing = False
                  OptionsData.Inserting = False
                  OptionsView.GroupByBox = False
                  object cxGridDBTableView2nCdProduto: TcxGridDBColumn
                    Caption = 'C'#243'digo'
                    DataBinding.FieldName = 'nCdProduto'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                  end
                  object cxGridDBTableView2cNmProduto: TcxGridDBColumn
                    Caption = 'Produto'
                    DataBinding.FieldName = 'cNmProduto'
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 261
                  end
                  object cxGridDBTableView2nValTotalPedido: TcxGridDBColumn
                    Caption = 'Total Vendido'
                    DataBinding.FieldName = 'nValTotalPedido'
                    HeaderAlignmentHorz = taRightJustify
                    Styles.Content = frmMenu.FonteSomenteLeitura
                    Width = 136
                  end
                end
                object cxGridLevel2: TcxGridLevel
                  GridView = cxGridDBTableView2
                end
              end
            end
          end
        end
      end
    end
    object cxTabVendas: TcxTabSheet
      Caption = 'Meta de Venda'
      ImageIndex = 2
      object GroupBox3: TGroupBox
        Left = 0
        Top = 0
        Width = 1140
        Height = 49
        Align = alTop
        TabOrder = 0
        object Label5: TLabel
          Tag = 1
          Left = 10
          Top = 24
          Width = 42
          Height = 13
          Alignment = taRightJustify
          Caption = 'M'#234's/Ano'
        end
        object btAtualizarMetaVenda: TcxButton
          Left = 128
          Top = 12
          Width = 86
          Height = 30
          Caption = 'Atualizar'
          TabOrder = 0
          OnClick = btAtualizarMetaVendaClick
          Glyph.Data = {
            6E040000424D6E04000000000000360000002800000013000000120000000100
            1800000000003804000000000000000000000000000000000000C6D6DEC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
            DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6
            D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE0000000000000000008C9C9CC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DEC6DEC6C6D6DEC6D6
            DEC6D6DEC6D6DEC6D6DE00000018A54A18A54A18A54A0000008C9C9CC6D6DEC6
            D6DEC6D6DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
            00000000000018A54A18A54A18A54A18A54A18A54A0000008C9C9CC6D6DEC6D6
            DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6D6DE0000000000000000006B
            D6BD18A54A6BD6BD0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE
            C6D6DE000000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFF0000006BD6
            BD000000EFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE00
            0000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFFEFFFFF000000EFFFFF
            EFFFFFEFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DE000000C6DE
            C6C6D6DE000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFBDBDBDEFFFFFEF
            FFFFEFFFFF0000006BD6BD18A54A18A54A000000C6D6DE000000C6D6DEC6D6DE
            000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDBDBDBDBDBDBDBDBD
            BDEFFFFF0000006BD6BD18A54A000000C6D6DE000000C6D6DEC6D6DE000000BD
            BDBDEFFFFFEFFFFFEFFFFF000000000000000000000000000000EFFFFFEFFFFF
            000000000000000000C6D6DEC6D6DE000000C6D6DEC6D6DE000000BDBDBDEFFF
            FFEFFFFFEFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6
            D6DEC6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DE000000BDBDBDEFFFFFEFFFFF
            EFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6
            DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DE000000BDBDBDEFFFFFEFFFFFEF
            FFFF000000BDBDBDEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6DEC6D6DEC6D6DE
            C6D6DE000000C6D6DEC6DEC6C6D6DE000000BDBDBDBDBDBDEFFFFFEFFFFF0000
            00EFFFFFEFFFFFDEBDD6DEBDD6000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE00
            0000C6D6DEC6D6DEC6D6DEC6D6DE000000000000BDBDBDBDBDBDBDBDBDBDBDBD
            BDBDBD000000000000C6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DE000000C6D6
            DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000000000000000000000C6
            D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DE
            C6DEC6C6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
            DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6DEC6000000C6DEC6C6D6DEC6D6DEC6
            D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000}
          LookAndFeel.NativeStyle = True
        end
        object edtMesAnoMeta: TMaskEdit
          Left = 56
          Top = 16
          Width = 63
          Height = 21
          EditMask = '99/9999'
          MaxLength = 7
          TabOrder = 1
          Text = '  /    '
        end
        object cxButton3: TcxButton
          Left = 216
          Top = 12
          Width = 161
          Height = 30
          Caption = 'Separa'#231#227'o/Embalagem'
          TabOrder = 2
          OnClick = cxButton3Click
          Glyph.Data = {
            BA030000424DBA030000000000003600000028000000130000000F0000000100
            1800000000008403000000000000000000000000000000000000DDDDDDDDDDDD
            9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C6F9C8C
            6F9C8C6F9C8C6F9C8C6F9C8C6FDDDDDDDDDDDD000000DDDDDD00000000000000
            0000000000000000000000000000000000000000000000000000000000000000
            000000000000000000DDDDDDDDDDDD000000DDDDDD0000004E7EA64E7EA64E7E
            A64E7EA64E7EA64E7EA60000000000004E7EA64E7EA64E7EA64E7EA64E7EA64E
            7EA6000000DDDDDDDDDDDD000000DDDDDD000000D6FFFF9EF0FF9EF0FF9EF0FF
            9EF0FF4E7EA6000000000000D6FFFF9EF0FF9EF0FF9EF0FF9EF0FF4E7EA60000
            00DDDDDDDDDDDD000000DDDDDD000000D6FFFF9EF0FF9EF0FF9EF0FF9EF0FF4E
            7EA6000000000000D6FFFF9EF0FF9EF0FF9EF0FF9EF0FF4E7EA6000000DDDDDD
            DDDDDD000000DDDDDD000000D6FFFF9EF0FF9EF0FF9EF0FF9EF0FF4E7EA60000
            00000000D6FFFF9EF0FF9EF0FF9EF0FF9EF0FF4E7EA6000000DDDDDDDDDDDD00
            0000DDDDDD000000D6FFFFD6FFFFD6FFFFD6FFFFD6FFFFD6FFFF000000000000
            D6FFFFD6FFFFD6FFFFD6FFFFD6FFFFD6FFFF000000DDDDDDDDDDDD000000DDDD
            DD00000000000000000000000000000000000000000000000000000000000000
            0000000000000000000000000000000000DDDDDDDDDDDD000000DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDD0000004E7EA64E7EA64E7EA64E7EA64E7EA64E7EA60000
            009C8C6FDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD000000DDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDD000000D6FFFF9EF0FF9EF0FF9EF0FF9EF0FF4E7EA60000009C8C6F
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDD000000DDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DD000000D6FFFF9EF0FF9EF0FF9EF0FF9EF0FF4E7EA60000009C8C6FDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDD000000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDD000000
            D6FFFF9EF0FF9EF0FF9EF0FF9EF0FF4E7EA60000009C8C6FDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDD000000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDD000000D6FFFFD6
            FFFFD6FFFFD6FFFFD6FFFFD6FFFF0000009C8C6FDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDD000000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDD0000000000000000000000
            00000000000000000000000000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD00
            0000DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD000000}
          LookAndFeel.NativeStyle = True
        end
        object cxButton4: TcxButton
          Left = 380
          Top = 12
          Width = 161
          Height = 30
          Caption = 'Cubo de Vendas'
          TabOrder = 3
          OnClick = cxButton4Click
          Glyph.Data = {
            06030000424D06030000000000003600000028000000100000000F0000000100
            180000000000D0020000C40E0000C40E0000000000000000000000000C060A15
            0002030C0B070701000C00002617150401000006000010020B0300040400000B
            000013060001040000070C0D00B5AD9C81685E8869609E8178190100FFFBEA94
            95817795827792890F000AF1FDFF0C717307807C4183820004060C0F00F1E2BB
            DAAD98D69E9395675C220000FFFEE9EBEED5DBFBE878928C0B000DE4FAFF65F2
            FB4BF3F41C8380000906192100D6C78FE0A486F5AC9E9056503B130EFFFFF4EA
            EBDBCCE9DA667D791D0A1DDDFCFF44EEF22EF7F3007F7D000E0E0B1700DBD094
            E6A687EA9C8F9E6564220000FFF4F7FBFBFBE9FFFD95A7A80B0007DAFFFF3AED
            E42BFBEF007D7C00080E021400D6D1A0D4A082EBA99E9D6D7315000811021E00
            0012000315000006070000DCFFFB56FEE148FFEA268A8E000212000E00DDD7B2
            CCA088D09C90926F730B000BF2EBFFF1F3FF00001CAFB4C9090B0CDBFFF874FA
            DC6CF9E4266C73000317131100D1C5ADBC9A83CDA795816E67040006F3F6FFED
            F3FF000023AFAED8000012DDFFFFC1FFFFB0F8F263828B00000D170200D7BFAD
            D5B59ED2B79D7F715E020300E5F0FEE8F2FF0000279A9AD600002B0000200000
            1600000F00000901050A2D0200E8BEABC9A68CB9A07E817551151700F6FFF7EF
            FDFF000025A8A8F42C3F9A29348E26135C0E0024FDFCFFF2FDF3350000F9CAB5
            CEAC8FBFA98585784C0E0C00FAFFE8F2FCFC0000219395E31B2C932124902B07
            61100023FBF9FFF6FFF4330000F8CCB5BB9D80C0AC898E7D56150A00FFFFEBFC
            FDFB000018A2A8E998A3FD9190F03A1D6D0D0022F8FBFFF0FFF12B0200E1BFA8
            B9A489C6B699887357190500FFFFF4FDF2F500001500002100002B0000350000
            220A001DFAFEFFEEFDF5210D00BFB19BC9C3ACCEC5B17E6A5F1D0000FFF7FFFF
            F5FFF8F9FFDAE4F6FBFBFFF6F5FEF8FFFFF1FAFDE7EAF2FAFDFF0E0400241E0B
            0706000904001300001E0007FFECFFFFF3FFF3F4FFF4FFFFFAF8EEFFFFF1F3FF
            EFEFFFF6FAFDFFF5F2FF}
          LookAndFeel.NativeStyle = True
        end
      end
      object GroupBox4: TGroupBox
        Left = 0
        Top = 352
        Width = 1140
        Height = 64
        Align = alTop
        Caption = 'Resumo'
        TabOrder = 1
        object Label6: TLabel
          Tag = 1
          Left = 90
          Top = 16
          Width = 46
          Height = 13
          Caption = 'Meta M'#234's'
          FocusControl = DBEdit1
        end
        object Label7: TLabel
          Tag = 1
          Left = 235
          Top = 16
          Width = 42
          Height = 13
          Caption = 'Meta Dia'
          FocusControl = DBEdit2
        end
        object Label8: TLabel
          Tag = 1
          Left = 322
          Top = 16
          Width = 97
          Height = 13
          Caption = '% Meta At. (Venda)'
          FocusControl = DBEdit3
        end
        object Label9: TLabel
          Tag = 1
          Left = 450
          Top = 16
          Width = 111
          Height = 13
          Caption = '% Meta At. (Faturam.)'
          FocusControl = DBEdit4
        end
        object Label10: TLabel
          Tag = 1
          Left = 612
          Top = 16
          Width = 90
          Height = 13
          Caption = 'Proje'#231#227'o de Venda'
          FocusControl = DBEdit5
        end
        object Label11: TLabel
          Tag = 1
          Left = 737
          Top = 16
          Width = 107
          Height = 13
          Caption = 'Proje'#231#227'o Faturamento'
          FocusControl = DBEdit6
        end
        object Label12: TLabel
          Tag = 1
          Left = 880
          Top = 16
          Width = 106
          Height = 13
          Caption = 'Venda Dia p/ At. Meta'
          FocusControl = DBEdit7
        end
        object Label13: TLabel
          Tag = 1
          Left = 1008
          Top = 16
          Width = 120
          Height = 13
          Caption = 'Faturam. Dia p/ At. Meta'
          FocusControl = DBEdit8
        end
        object DBEdit1: TDBEdit
          Tag = 1
          Left = 16
          Top = 32
          Width = 120
          Height = 21
          DataField = 'nValMeta'
          DataSource = DataSource1
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Tag = 1
          Left = 157
          Top = 32
          Width = 120
          Height = 21
          DataField = 'nValMetaDia'
          DataSource = DataSource1
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Tag = 1
          Left = 299
          Top = 32
          Width = 120
          Height = 21
          DataField = 'nPercMetaAting_Venda'
          DataSource = DataSource1
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Tag = 1
          Left = 441
          Top = 32
          Width = 120
          Height = 21
          DataField = 'nPercMetaAting_Fat'
          DataSource = DataSource1
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Tag = 1
          Left = 582
          Top = 32
          Width = 120
          Height = 21
          DataField = 'nProjecao_Venda'
          DataSource = DataSource1
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Tag = 1
          Left = 724
          Top = 32
          Width = 120
          Height = 21
          DataField = 'nProjecao_Fat'
          DataSource = DataSource1
          TabOrder = 5
        end
        object DBEdit7: TDBEdit
          Tag = 1
          Left = 866
          Top = 32
          Width = 120
          Height = 21
          DataField = 'nValVendaDia_AtMeta'
          DataSource = DataSource1
          TabOrder = 6
        end
        object DBEdit8: TDBEdit
          Tag = 1
          Left = 1008
          Top = 32
          Width = 120
          Height = 21
          DataField = 'nValFatDia_AtMeta'
          DataSource = DataSource1
          TabOrder = 7
        end
      end
      object GroupBox5: TGroupBox
        Left = 0
        Top = 49
        Width = 1140
        Height = 303
        Align = alTop
        TabOrder = 2
        object DBChart1: TDBChart
          Left = 2
          Top = 15
          Width = 1136
          Height = 286
          BackWall.Brush.Color = clWhite
          BackWall.Brush.Style = bsClear
          Gradient.Direction = gdLeftRight
          Gradient.EndColor = 12320767
          Gradient.Visible = True
          Title.Font.Charset = ANSI_CHARSET
          Title.Font.Color = clBlue
          Title.Font.Height = -16
          Title.Font.Name = 'Calibri'
          Title.Font.Style = [fsBold]
          Title.Text.Strings = (
            'Acompanhamento de Meta de Venda Mensal')
          LeftAxis.Visible = False
          Legend.Font.Charset = ANSI_CHARSET
          Legend.Font.Color = clBlack
          Legend.Font.Height = -13
          Legend.Font.Name = 'Calibri'
          Legend.Font.Style = []
          Align = alClient
          TabOrder = 0
          object Series1: TBarSeries
            Marks.ArrowLength = 20
            Marks.Style = smsValue
            Marks.Visible = True
            DataSource = qryTempVenda
            SeriesColor = clRed
            Title = 'Meta M'#234's'
            XLabelsSource = 'nValMeta'
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Bar'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
            YValues.ValueSource = 'nValMeta'
          end
          object Series2: TBarSeries
            Marks.ArrowLength = 20
            Marks.Visible = True
            DataSource = qrySomaVendaDia
            SeriesColor = clBlue
            Title = 'Venda M'#234's'
            XLabelsSource = 'nValTotalVenda'
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Bar'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
            YValues.ValueSource = 'nValTotalVenda'
          end
          object Series3: TBarSeries
            Marks.ArrowLength = 20
            Marks.Visible = True
            DataSource = qrySomaVendaDia
            SeriesColor = clYellow
            Title = 'Faturamento M'#234's'
            XLabelsSource = 'nValFaturamento'
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1.000000000000000000
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Bar'
            YValues.Multiplier = 1.000000000000000000
            YValues.Order = loNone
            YValues.ValueSource = 'nValFaturamento'
          end
        end
      end
      object cxPageControl14: TcxPageControl
        Left = 0
        Top = 416
        Width = 1140
        Height = 341
        ActivePage = cxTabSheet16
        Align = alClient
        LookAndFeel.NativeStyle = True
        TabOrder = 3
        ClientRectBottom = 337
        ClientRectLeft = 4
        ClientRectRight = 1136
        ClientRectTop = 24
        object cxTabSheet16: TcxTabSheet
          Caption = 'Venda x Faturamento - Di'#225'rio'
          ImageIndex = 0
          object cxGrid12: TcxGrid
            Left = 0
            Top = 0
            Width = 1132
            Height = 313
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView11: TcxGridDBTableView
              DataController.DataSource = dsTempVendaDia
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  Column = cxGridDBTableView11nValVenda
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  Column = cxGridDBTableView11nValFaturamento
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              OptionsCustomize.ColumnFiltering = False
              OptionsCustomize.ColumnGrouping = False
              OptionsCustomize.ColumnHidingOnGrouping = False
              OptionsCustomize.ColumnMoving = False
              OptionsCustomize.ColumnSorting = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              object cxGridDBTableView11dData: TcxGridDBColumn
                Caption = 'Data Venda'
                DataBinding.FieldName = 'dData'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 97
              end
              object cxGridDBTableView11nQtdePedido: TcxGridDBColumn
                Caption = 'Qt. Pedidos'
                DataBinding.FieldName = 'nQtdePedido'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 85
              end
              object cxGridDBTableView11nValVenda: TcxGridDBColumn
                Caption = 'Valor Venda'
                DataBinding.FieldName = 'nValVenda'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 109
              end
              object cxGridDBTableView11nValFaturamento: TcxGridDBColumn
                Caption = 'Valor Faturado'
                DataBinding.FieldName = 'nValFaturamento'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 115
              end
              object cxGridDBTableView11nValMeta: TcxGridDBColumn
                Caption = 'Meta Di'#225'ria'
                DataBinding.FieldName = 'nValMeta'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 115
              end
              object cxGridDBTableView11nValDiferenca: TcxGridDBColumn
                Caption = 'Diferen'#231'a'
                DataBinding.FieldName = 'nValDiferenca'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 115
              end
              object cxGridDBTableView11nValVendaAcum: TcxGridDBColumn
                Caption = 'Venda Acumulada'
                DataBinding.FieldName = 'nValVendaAcum'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 122
              end
              object cxGridDBTableView11nValFaturamentoAcum: TcxGridDBColumn
                Caption = 'Faturamento Acumulado'
                DataBinding.FieldName = 'nValFaturamentoAcum'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 125
              end
              object cxGridDBTableView11nValMetaAcum: TcxGridDBColumn
                Caption = 'Meta Acumulada'
                DataBinding.FieldName = 'nValMetaAcum'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 115
              end
              object cxGridDBTableView11nValDiferencaAcum: TcxGridDBColumn
                Caption = 'Diferen'#231'a Acumulada'
                DataBinding.FieldName = 'nValDiferencaAcum'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 115
              end
            end
            object cxGridLevel11: TcxGridLevel
              GridView = cxGridDBTableView11
            end
          end
        end
      end
    end
    object cxTabEstoque: TcxTabSheet
      Caption = 'Estoque'
      ImageIndex = 3
      object cxPageControl15: TcxPageControl
        Left = 0
        Top = 49
        Width = 1140
        Height = 369
        ActivePage = cxTabSheet17
        Align = alTop
        LookAndFeel.NativeStyle = True
        TabOrder = 0
        ClientRectBottom = 365
        ClientRectLeft = 4
        ClientRectRight = 1136
        ClientRectTop = 24
        object cxTabSheet17: TcxTabSheet
          Caption = 'Produtos Abaixo Estoque M'#237'nimo'
          ImageIndex = 0
          object cxGrid13: TcxGrid
            Left = 0
            Top = 0
            Width = 1132
            Height = 341
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView12: TcxGridDBTableView
              OnDblClick = cxGridDBTableView12DblClick
              DataController.DataSource = dsProdutoAbaixoMinimo
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              OptionsCustomize.ColumnFiltering = False
              OptionsCustomize.ColumnHiding = True
              OptionsCustomize.ColumnSorting = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              object cxGridDBTableView12nCdProduto: TcxGridDBColumn
                Caption = 'C'#243'digo'
                DataBinding.FieldName = 'nCdProduto'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView12cNmProduto: TcxGridDBColumn
                Caption = 'Descri'#231#227'o Produto'
                DataBinding.FieldName = 'cNmProduto'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 453
              end
              object cxGridDBTableView12cReferencia: TcxGridDBColumn
                Caption = 'Refer'#234'ncia'
                DataBinding.FieldName = 'cReferencia'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView12cUnidadeMedida: TcxGridDBColumn
                Caption = 'U.M'
                DataBinding.FieldName = 'cUnidadeMedida'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 32
              end
              object cxGridDBTableView12cNmGrupoProduto: TcxGridDBColumn
                Caption = 'Grupo Produto:'
                DataBinding.FieldName = 'cNmGrupoProduto'
                Visible = False
                GroupIndex = 0
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView12nQtdeEstoque: TcxGridDBColumn
                Caption = 'Estoque Atual'
                DataBinding.FieldName = 'nQtdeEstoque'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 90
              end
              object cxGridDBTableView12nEstoqueMinimo: TcxGridDBColumn
                Caption = 'Estoque M'#237'nimo'
                DataBinding.FieldName = 'nEstoqueMinimo'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 90
              end
              object cxGridDBTableView12nQtdeEmPedido: TcxGridDBColumn
                Caption = 'Qtde Em Pedido'
                DataBinding.FieldName = 'nQtdeEmPedido'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 90
              end
              object cxGridDBTableView12iLeadTime: TcxGridDBColumn
                Caption = 'Lead Time'
                DataBinding.FieldName = 'iLeadTime'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 90
              end
            end
            object cxGridLevel12: TcxGridLevel
              GridView = cxGridDBTableView12
            end
          end
        end
        object cxTabSheet18: TcxTabSheet
          Caption = 'Produtos Abaixo Ponto Ressuprimento'
          ImageIndex = 1
          object cxGrid14: TcxGrid
            Left = 0
            Top = 0
            Width = 1132
            Height = 341
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView13: TcxGridDBTableView
              OnDblClick = cxGridDBTableView13DblClick
              DataController.DataSource = dsProdutoAbaixoRessuprimento
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              OptionsCustomize.ColumnFiltering = False
              OptionsCustomize.ColumnHiding = True
              OptionsCustomize.ColumnSorting = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              object cxGridDBColumn58: TcxGridDBColumn
                Caption = 'C'#243'digo'
                DataBinding.FieldName = 'nCdProduto'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn59: TcxGridDBColumn
                Caption = 'Descri'#231#227'o Produto'
                DataBinding.FieldName = 'cNmProduto'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 453
              end
              object cxGridDBColumn60: TcxGridDBColumn
                Caption = 'Refer'#234'ncia'
                DataBinding.FieldName = 'cReferencia'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn61: TcxGridDBColumn
                Caption = 'U.M'
                DataBinding.FieldName = 'cUnidadeMedida'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 32
              end
              object cxGridDBColumn62: TcxGridDBColumn
                Caption = 'Grupo Produto:'
                DataBinding.FieldName = 'cNmGrupoProduto'
                Visible = False
                GroupIndex = 0
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn63: TcxGridDBColumn
                Caption = 'Estoque Atual'
                DataBinding.FieldName = 'nQtdeEstoque'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 90
              end
              object cxGridDBColumn64: TcxGridDBColumn
                Caption = 'Ponto Ressuprimento'
                DataBinding.FieldName = 'nEstoqueMinimo'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 117
              end
              object cxGridDBColumn65: TcxGridDBColumn
                Caption = 'Qtde Em Pedido'
                DataBinding.FieldName = 'nQtdeEmPedido'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 90
              end
              object cxGridDBColumn66: TcxGridDBColumn
                Caption = 'Lead Time'
                DataBinding.FieldName = 'iLeadTime'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 90
              end
            end
            object cxGridLevel13: TcxGridLevel
              GridView = cxGridDBTableView13
            end
          end
        end
      end
      object DBChart2: TDBChart
        Left = 0
        Top = 418
        Width = 1140
        Height = 339
        AllowPanning = pmNone
        AllowZoom = False
        BackWall.Brush.Color = clWhite
        BackWall.Brush.Style = bsClear
        BackWall.Pen.Visible = False
        Gradient.Direction = gdLeftRight
        Gradient.EndColor = 12320767
        Gradient.Visible = True
        Title.Font.Charset = ANSI_CHARSET
        Title.Font.Color = clBlue
        Title.Font.Height = -16
        Title.Font.Name = 'Calibri'
        Title.Font.Style = [fsBold]
        Title.Text.Strings = (
          'Total em Estoque por Grupo de Produto')
        AxisVisible = False
        Chart3DPercent = 40
        ClipPoints = False
        Frame.Visible = False
        LeftAxis.Visible = False
        Legend.Font.Charset = ANSI_CHARSET
        Legend.Font.Color = clBlack
        Legend.Font.Height = -13
        Legend.Font.Name = 'Calibri'
        Legend.Font.Style = []
        View3DOptions.Elevation = 315
        View3DOptions.Orthogonal = False
        View3DOptions.Perspective = 0
        View3DOptions.Rotation = 360
        View3DWalls = False
        Align = alClient
        TabOrder = 1
        object BarSeries3: TPieSeries
          Marks.ArrowLength = 20
          Marks.Visible = True
          DataSource = qryTotalEstoqueGrupo
          SeriesColor = clYellow
          Title = 'Grupo de Produto'
          XLabelsSource = 'cNmGrupoProduto'
          OtherSlice.Text = 'Other'
          PieValues.DateTime = False
          PieValues.Name = 'Pie'
          PieValues.Multiplier = 1.000000000000000000
          PieValues.Order = loNone
          PieValues.ValueSource = 'nValTotalCusto'
        end
      end
      object GroupBox6: TGroupBox
        Left = 0
        Top = 0
        Width = 1140
        Height = 49
        Align = alTop
        TabOrder = 2
        object btAtualizaEstoque: TcxButton
          Left = 8
          Top = 12
          Width = 86
          Height = 30
          Caption = 'Atualizar'
          TabOrder = 0
          OnClick = btAtualizaEstoqueClick
          Glyph.Data = {
            6E040000424D6E04000000000000360000002800000013000000120000000100
            1800000000003804000000000000000000000000000000000000C6D6DEC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
            DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6
            D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE0000000000000000008C9C9CC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DEC6DEC6C6D6DEC6D6
            DEC6D6DEC6D6DEC6D6DE00000018A54A18A54A18A54A0000008C9C9CC6D6DEC6
            D6DEC6D6DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
            00000000000018A54A18A54A18A54A18A54A18A54A0000008C9C9CC6D6DEC6D6
            DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6D6DE0000000000000000006B
            D6BD18A54A6BD6BD0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE
            C6D6DE000000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFF0000006BD6
            BD000000EFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE00
            0000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFFEFFFFF000000EFFFFF
            EFFFFFEFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DE000000C6DE
            C6C6D6DE000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFBDBDBDEFFFFFEF
            FFFFEFFFFF0000006BD6BD18A54A18A54A000000C6D6DE000000C6D6DEC6D6DE
            000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDBDBDBDBDBDBDBDBD
            BDEFFFFF0000006BD6BD18A54A000000C6D6DE000000C6D6DEC6D6DE000000BD
            BDBDEFFFFFEFFFFFEFFFFF000000000000000000000000000000EFFFFFEFFFFF
            000000000000000000C6D6DEC6D6DE000000C6D6DEC6D6DE000000BDBDBDEFFF
            FFEFFFFFEFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6
            D6DEC6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DE000000BDBDBDEFFFFFEFFFFF
            EFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6
            DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DE000000BDBDBDEFFFFFEFFFFFEF
            FFFF000000BDBDBDEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6DEC6D6DEC6D6DE
            C6D6DE000000C6D6DEC6DEC6C6D6DE000000BDBDBDBDBDBDEFFFFFEFFFFF0000
            00EFFFFFEFFFFFDEBDD6DEBDD6000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE00
            0000C6D6DEC6D6DEC6D6DEC6D6DE000000000000BDBDBDBDBDBDBDBDBDBDBDBD
            BDBDBD000000000000C6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DE000000C6D6
            DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000000000000000000000C6
            D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DE
            C6DEC6C6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
            DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6DEC6000000C6DEC6C6D6DEC6D6DEC6
            D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000}
          LookAndFeel.NativeStyle = True
        end
      end
    end
    object cxTabProducao: TcxTabSheet
      Caption = 'Produ'#231#227'o'
      ImageIndex = 4
      object GroupBox7: TGroupBox
        Left = 0
        Top = 0
        Width = 1140
        Height = 49
        Align = alTop
        TabOrder = 0
        object cxButton1: TcxButton
          Left = 8
          Top = 12
          Width = 86
          Height = 30
          Caption = 'Atualizar'
          TabOrder = 0
          OnClick = cxButton1Click
          Glyph.Data = {
            6E040000424D6E04000000000000360000002800000013000000120000000100
            1800000000003804000000000000000000000000000000000000C6D6DEC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
            DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6
            D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE0000000000000000008C9C9CC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DEC6DEC6C6D6DEC6D6
            DEC6D6DEC6D6DEC6D6DE00000018A54A18A54A18A54A0000008C9C9CC6D6DEC6
            D6DEC6D6DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
            00000000000018A54A18A54A18A54A18A54A18A54A0000008C9C9CC6D6DEC6D6
            DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6D6DE0000000000000000006B
            D6BD18A54A6BD6BD0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE
            C6D6DE000000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFF0000006BD6
            BD000000EFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE00
            0000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFFEFFFFF000000EFFFFF
            EFFFFFEFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DE000000C6DE
            C6C6D6DE000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFBDBDBDEFFFFFEF
            FFFFEFFFFF0000006BD6BD18A54A18A54A000000C6D6DE000000C6D6DEC6D6DE
            000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDBDBDBDBDBDBDBDBD
            BDEFFFFF0000006BD6BD18A54A000000C6D6DE000000C6D6DEC6D6DE000000BD
            BDBDEFFFFFEFFFFFEFFFFF000000000000000000000000000000EFFFFFEFFFFF
            000000000000000000C6D6DEC6D6DE000000C6D6DEC6D6DE000000BDBDBDEFFF
            FFEFFFFFEFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6
            D6DEC6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DE000000BDBDBDEFFFFFEFFFFF
            EFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6
            DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DE000000BDBDBDEFFFFFEFFFFFEF
            FFFF000000BDBDBDEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6DEC6D6DEC6D6DE
            C6D6DE000000C6D6DEC6DEC6C6D6DE000000BDBDBDBDBDBDEFFFFFEFFFFF0000
            00EFFFFFEFFFFFDEBDD6DEBDD6000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE00
            0000C6D6DEC6D6DEC6D6DEC6D6DE000000000000BDBDBDBDBDBDBDBDBDBDBDBD
            BDBDBD000000000000C6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DE000000C6D6
            DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000000000000000000000C6
            D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DE
            C6DEC6C6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
            DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6DEC6000000C6DEC6C6D6DEC6D6DEC6
            D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000}
          LookAndFeel.NativeStyle = True
        end
      end
      object cxPageControl16: TcxPageControl
        Left = 0
        Top = 49
        Width = 1140
        Height = 708
        ActivePage = cxTabSheet19
        Align = alClient
        LookAndFeel.NativeStyle = True
        TabOrder = 1
        ClientRectBottom = 704
        ClientRectLeft = 4
        ClientRectRight = 1136
        ClientRectTop = 24
        object cxTabSheet19: TcxTabSheet
          Caption = 'OP em Aberto'
          ImageIndex = 0
          object cxGrid15: TcxGrid
            Left = 0
            Top = 0
            Width = 1132
            Height = 680
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView14: TcxGridDBTableView
              OnDblClick = cxGridDBTableView14DblClick
              DataController.DataSource = dsOPAberta
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              OptionsCustomize.ColumnFiltering = False
              OptionsCustomize.ColumnGrouping = False
              OptionsCustomize.ColumnHidingOnGrouping = False
              OptionsCustomize.ColumnMoving = False
              OptionsCustomize.ColumnSorting = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              object cxGridDBTableView14nCdOrdemProducao: TcxGridDBColumn
                Caption = 'C'#243'digo'
                DataBinding.FieldName = 'nCdOrdemProducao'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 48
              end
              object cxGridDBTableView14cFlgAtraso: TcxGridDBColumn
                Caption = 'Em Atraso?'
                DataBinding.FieldName = 'cFlgAtraso'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 75
              end
              object cxGridDBTableView14cNumeroOP: TcxGridDBColumn
                Caption = 'N'#250'm. OP'
                DataBinding.FieldName = 'cNumeroOP'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 58
              end
              object cxGridDBTableView14cNmTabTipoOrigemOP: TcxGridDBColumn
                Caption = 'Origem'
                DataBinding.FieldName = 'cNmTabTipoOrigemOP'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 73
              end
              object cxGridDBTableView14dDtAbertura: TcxGridDBColumn
                Caption = 'Dt. Abertura'
                DataBinding.FieldName = 'dDtAbertura'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 83
              end
              object cxGridDBTableView14dDtInicioPrevisto: TcxGridDBColumn
                Caption = 'Previs'#227'o In'#237'cio'
                DataBinding.FieldName = 'dDtInicioPrevisto'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView14dDtPrevConclusao: TcxGridDBColumn
                Caption = 'Conclus'#227'o Prevista'
                DataBinding.FieldName = 'dDtPrevConclusao'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView14cNmTipoOP: TcxGridDBColumn
                Caption = 'Tipo OP'
                DataBinding.FieldName = 'cNmTipoOP'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 84
              end
              object cxGridDBTableView14nCdProduto: TcxGridDBColumn
                Caption = 'C'#243'd. Prod.'
                DataBinding.FieldName = 'nCdProduto'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView14cReferencia: TcxGridDBColumn
                Caption = 'Refer'#234'ncia'
                DataBinding.FieldName = 'cReferencia'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView14cNmProduto: TcxGridDBColumn
                Caption = 'Descri'#231#227'o Produto'
                DataBinding.FieldName = 'cNmProduto'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 179
              end
              object cxGridDBTableView14nQtdePlanejada: TcxGridDBColumn
                Caption = 'Qt. Planejada'
                DataBinding.FieldName = 'nQtdePlanejada'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView14nQtdeConcluida: TcxGridDBColumn
                Caption = 'Qt. Conclu'#237'da'
                DataBinding.FieldName = 'nQtdeConcluida'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView14nQtdeRetrabalho: TcxGridDBColumn
                Caption = 'Qt. Retrab.'
                DataBinding.FieldName = 'nQtdeRetrabalho'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView14nQtdeRefugo: TcxGridDBColumn
                Caption = 'Qt. Refugo'
                DataBinding.FieldName = 'nQtdeRefugo'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView14nSaldoProduzir: TcxGridDBColumn
                Caption = 'Saldo OP'
                DataBinding.FieldName = 'nSaldoProduzir'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 68
              end
              object cxGridDBTableView14nQtdeCancelada: TcxGridDBColumn
                Caption = 'Qt. Canc.'
                DataBinding.FieldName = 'nQtdeCancelada'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView14nCdPedido: TcxGridDBColumn
                Caption = 'N'#250'm. Pedido'
                DataBinding.FieldName = 'nCdPedido'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView14cNmTerceiro: TcxGridDBColumn
                Caption = 'Cliente'
                DataBinding.FieldName = 'cNmTerceiro'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView14cNmTabTipoStatusOP: TcxGridDBColumn
                Caption = 'Status OP'
                DataBinding.FieldName = 'cNmTabTipoStatusOP'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 100
              end
            end
            object cxGridLevel14: TcxGridLevel
              GridView = cxGridDBTableView14
            end
          end
        end
        object cxTabSheet20: TcxTabSheet
          Caption = 'OP Aguardando Produto'
          ImageIndex = 1
          object cxGrid16: TcxGrid
            Left = 0
            Top = 0
            Width = 1132
            Height = 680
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView15: TcxGridDBTableView
              OnDblClick = cxGridDBTableView15DblClick
              DataController.DataSource = dsOPAguardaProduto
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              OptionsCustomize.ColumnFiltering = False
              OptionsCustomize.ColumnGrouping = False
              OptionsCustomize.ColumnHidingOnGrouping = False
              OptionsCustomize.ColumnMoving = False
              OptionsCustomize.ColumnSorting = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              object cxGridDBColumn67: TcxGridDBColumn
                Caption = 'C'#243'digo'
                DataBinding.FieldName = 'nCdOrdemProducao'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 48
              end
              object cxGridDBColumn68: TcxGridDBColumn
                Caption = 'Em Atraso?'
                DataBinding.FieldName = 'cFlgAtraso'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 75
              end
              object cxGridDBColumn69: TcxGridDBColumn
                Caption = 'N'#250'm. OP'
                DataBinding.FieldName = 'cNumeroOP'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 58
              end
              object cxGridDBColumn70: TcxGridDBColumn
                Caption = 'Origem'
                DataBinding.FieldName = 'cNmTabTipoOrigemOP'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 73
              end
              object cxGridDBColumn71: TcxGridDBColumn
                Caption = 'Dt. Abertura'
                DataBinding.FieldName = 'dDtAbertura'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 83
              end
              object cxGridDBColumn72: TcxGridDBColumn
                Caption = 'Previs'#227'o In'#237'cio'
                DataBinding.FieldName = 'dDtInicioPrevisto'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn73: TcxGridDBColumn
                Caption = 'Conclus'#227'o Prevista'
                DataBinding.FieldName = 'dDtPrevConclusao'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn74: TcxGridDBColumn
                Caption = 'Tipo OP'
                DataBinding.FieldName = 'cNmTipoOP'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 84
              end
              object cxGridDBColumn75: TcxGridDBColumn
                Caption = 'C'#243'd. Prod.'
                DataBinding.FieldName = 'nCdProduto'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn76: TcxGridDBColumn
                Caption = 'Refer'#234'ncia'
                DataBinding.FieldName = 'cReferencia'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn77: TcxGridDBColumn
                Caption = 'Descri'#231#227'o Produto'
                DataBinding.FieldName = 'cNmProduto'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 179
              end
              object cxGridDBColumn78: TcxGridDBColumn
                Caption = 'Qt. Planejada'
                DataBinding.FieldName = 'nQtdePlanejada'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn79: TcxGridDBColumn
                Caption = 'Qt. Conclu'#237'da'
                DataBinding.FieldName = 'nQtdeConcluida'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn80: TcxGridDBColumn
                Caption = 'Qt. Retrab.'
                DataBinding.FieldName = 'nQtdeRetrabalho'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn81: TcxGridDBColumn
                Caption = 'Qt. Refugo'
                DataBinding.FieldName = 'nQtdeRefugo'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn82: TcxGridDBColumn
                Caption = 'Saldo OP'
                DataBinding.FieldName = 'nSaldoProduzir'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 68
              end
              object cxGridDBColumn83: TcxGridDBColumn
                Caption = 'Qt. Canc.'
                DataBinding.FieldName = 'nQtdeCancelada'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn84: TcxGridDBColumn
                Caption = 'N'#250'm. Pedido'
                DataBinding.FieldName = 'nCdPedido'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn85: TcxGridDBColumn
                Caption = 'Cliente'
                DataBinding.FieldName = 'cNmTerceiro'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn86: TcxGridDBColumn
                Caption = 'Status OP'
                DataBinding.FieldName = 'cNmTabTipoStatusOP'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 100
              end
            end
            object cxGridLevel15: TcxGridLevel
              GridView = cxGridDBTableView15
            end
          end
        end
        object cxTabSheet21: TcxTabSheet
          Caption = 'OP Aguardando Servi'#231'o Externo'
          ImageIndex = 2
          object cxGrid17: TcxGrid
            Left = 0
            Top = 0
            Width = 1132
            Height = 680
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView16: TcxGridDBTableView
              OnDblClick = cxGridDBTableView16DblClick
              DataController.DataSource = dsOPAguardaServico
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              OptionsCustomize.ColumnFiltering = False
              OptionsCustomize.ColumnGrouping = False
              OptionsCustomize.ColumnHidingOnGrouping = False
              OptionsCustomize.ColumnMoving = False
              OptionsCustomize.ColumnSorting = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              object cxGridDBColumn87: TcxGridDBColumn
                Caption = 'C'#243'digo'
                DataBinding.FieldName = 'nCdOrdemProducao'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 48
              end
              object cxGridDBColumn88: TcxGridDBColumn
                Caption = 'Em Atraso?'
                DataBinding.FieldName = 'cFlgAtraso'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 75
              end
              object cxGridDBColumn89: TcxGridDBColumn
                Caption = 'N'#250'm. OP'
                DataBinding.FieldName = 'cNumeroOP'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 58
              end
              object cxGridDBColumn90: TcxGridDBColumn
                Caption = 'Origem'
                DataBinding.FieldName = 'cNmTabTipoOrigemOP'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 73
              end
              object cxGridDBColumn91: TcxGridDBColumn
                Caption = 'Dt. Abertura'
                DataBinding.FieldName = 'dDtAbertura'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 83
              end
              object cxGridDBColumn92: TcxGridDBColumn
                Caption = 'Previs'#227'o In'#237'cio'
                DataBinding.FieldName = 'dDtInicioPrevisto'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn93: TcxGridDBColumn
                Caption = 'Conclus'#227'o Prevista'
                DataBinding.FieldName = 'dDtPrevConclusao'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn94: TcxGridDBColumn
                Caption = 'Tipo OP'
                DataBinding.FieldName = 'cNmTipoOP'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 84
              end
              object cxGridDBColumn95: TcxGridDBColumn
                Caption = 'C'#243'd. Prod.'
                DataBinding.FieldName = 'nCdProduto'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn96: TcxGridDBColumn
                Caption = 'Refer'#234'ncia'
                DataBinding.FieldName = 'cReferencia'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn97: TcxGridDBColumn
                Caption = 'Descri'#231#227'o Produto'
                DataBinding.FieldName = 'cNmProduto'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 179
              end
              object cxGridDBColumn98: TcxGridDBColumn
                Caption = 'Qt. Planejada'
                DataBinding.FieldName = 'nQtdePlanejada'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn99: TcxGridDBColumn
                Caption = 'Qt. Conclu'#237'da'
                DataBinding.FieldName = 'nQtdeConcluida'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn100: TcxGridDBColumn
                Caption = 'Qt. Retrab.'
                DataBinding.FieldName = 'nQtdeRetrabalho'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn101: TcxGridDBColumn
                Caption = 'Qt. Refugo'
                DataBinding.FieldName = 'nQtdeRefugo'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn102: TcxGridDBColumn
                Caption = 'Saldo OP'
                DataBinding.FieldName = 'nSaldoProduzir'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 68
              end
              object cxGridDBColumn103: TcxGridDBColumn
                Caption = 'Qt. Canc.'
                DataBinding.FieldName = 'nQtdeCancelada'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn104: TcxGridDBColumn
                Caption = 'N'#250'm. Pedido'
                DataBinding.FieldName = 'nCdPedido'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn105: TcxGridDBColumn
                Caption = 'Cliente'
                DataBinding.FieldName = 'cNmTerceiro'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn106: TcxGridDBColumn
                Caption = 'Status OP'
                DataBinding.FieldName = 'cNmTabTipoStatusOP'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 100
              end
            end
            object cxGridLevel16: TcxGridLevel
              GridView = cxGridDBTableView16
            end
          end
        end
      end
    end
    object cxTabCompras: TcxTabSheet
      Caption = 'Compra'
      ImageIndex = 5
      object GroupBox8: TGroupBox
        Left = 0
        Top = 0
        Width = 1140
        Height = 49
        Align = alTop
        TabOrder = 0
        object cxButton2: TcxButton
          Left = 8
          Top = 12
          Width = 86
          Height = 30
          Caption = 'Atualizar'
          TabOrder = 0
          OnClick = cxButton2Click
          Glyph.Data = {
            6E040000424D6E04000000000000360000002800000013000000120000000100
            1800000000003804000000000000000000000000000000000000C6D6DEC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
            DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6
            D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE0000000000000000008C9C9CC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DEC6DEC6C6D6DEC6D6
            DEC6D6DEC6D6DEC6D6DE00000018A54A18A54A18A54A0000008C9C9CC6D6DEC6
            D6DEC6D6DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE
            00000000000018A54A18A54A18A54A18A54A18A54A0000008C9C9CC6D6DEC6D6
            DEC6D6DEC6D6DE000000C6DEC6C6D6DEC6D6DEC6D6DE0000000000000000006B
            D6BD18A54A6BD6BD0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE
            C6D6DE000000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFF0000006BD6
            BD000000EFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DEC6D6DE00
            0000C6D6DEC6D6DEC6D6DE000000DEBDD6EFFFFFEFFFFFEFFFFF000000EFFFFF
            EFFFFFEFFFFF0000006BD6BD18A54A18A54A0000008C9C9CC6D6DE000000C6DE
            C6C6D6DE000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFFEFFFFFBDBDBDEFFFFFEF
            FFFFEFFFFF0000006BD6BD18A54A18A54A000000C6D6DE000000C6D6DEC6D6DE
            000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000BDBDBDBDBDBDBDBDBDBDBD
            BDEFFFFF0000006BD6BD18A54A000000C6D6DE000000C6D6DEC6D6DE000000BD
            BDBDEFFFFFEFFFFFEFFFFF000000000000000000000000000000EFFFFFEFFFFF
            000000000000000000C6D6DEC6D6DE000000C6D6DEC6D6DE000000BDBDBDEFFF
            FFEFFFFFEFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6
            D6DEC6D6DEC6D6DEC6D6DE000000C6DEC6C6D6DE000000BDBDBDEFFFFFEFFFFF
            EFFFFFEFFFFF000000BDBDBDEFFFFFEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6
            DEC6DEC6C6D6DE000000C6D6DEC6D6DEC6D6DE000000BDBDBDEFFFFFEFFFFFEF
            FFFF000000BDBDBDEFFFFFEFFFFFEFFFFF000000C6D6DEC6D6DEC6D6DEC6D6DE
            C6D6DE000000C6D6DEC6DEC6C6D6DE000000BDBDBDBDBDBDEFFFFFEFFFFF0000
            00EFFFFFEFFFFFDEBDD6DEBDD6000000C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE00
            0000C6D6DEC6D6DEC6D6DEC6D6DE000000000000BDBDBDBDBDBDBDBDBDBDBDBD
            BDBDBD000000000000C6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DE000000C6D6
            DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000000000000000000000000000C6
            D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000C6D6DEC6D6DE
            C6DEC6C6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6D6
            DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6DEC6000000C6DEC6C6D6DEC6D6DEC6
            D6DEC6DEC6C6D6DEC6D6DEC6D6DEC6D6DEC6D6DEC6DEC6C6D6DEC6D6DEC6D6DE
            C6D6DEC6D6DEC6D6DEC6D6DEC6D6DE000000}
          LookAndFeel.NativeStyle = True
        end
      end
      object cxPageControl17: TcxPageControl
        Left = 0
        Top = 49
        Width = 1140
        Height = 708
        ActivePage = cxTabSheet22
        Align = alClient
        LookAndFeel.NativeStyle = True
        TabOrder = 1
        ClientRectBottom = 704
        ClientRectLeft = 4
        ClientRectRight = 1136
        ClientRectTop = 24
        object cxTabSheet22: TcxTabSheet
          Caption = 'Pedido Aguardando Aprova'#231#227'o'
          ImageIndex = 0
          object cxGrid18: TcxGrid
            Left = 0
            Top = 0
            Width = 1132
            Height = 680
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView17: TcxGridDBTableView
              OnDblClick = cxGridDBTableView17DblClick
              DataController.DataSource = dsPDCAguardAutoriz
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  Column = cxGridDBTableView17nValPedido
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              OptionsCustomize.ColumnFiltering = False
              OptionsCustomize.ColumnGrouping = False
              OptionsCustomize.ColumnHidingOnGrouping = False
              OptionsCustomize.ColumnMoving = False
              OptionsCustomize.ColumnSorting = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              object cxGridDBTableView17nCdPedido: TcxGridDBColumn
                Caption = 'Pedido'
                DataBinding.FieldName = 'nCdPedido'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView17dDtPedido: TcxGridDBColumn
                Caption = 'Dt. Pedido'
                DataBinding.FieldName = 'dDtPedido'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView17cNmTipoPedido: TcxGridDBColumn
                Caption = 'Tipo de Pedido'
                DataBinding.FieldName = 'cNmTipoPedido'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView17cNmTerceiro: TcxGridDBColumn
                Caption = 'Fornecedor'
                DataBinding.FieldName = 'cNmTerceiro'
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBTableView17nValPedido: TcxGridDBColumn
                Caption = 'Valor Pedido'
                DataBinding.FieldName = 'nValPedido'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 110
              end
            end
            object cxGridLevel17: TcxGridLevel
              GridView = cxGridDBTableView17
            end
          end
        end
        object cxTabSheet23: TcxTabSheet
          Caption = 'Pedido em Atraso'
          ImageIndex = 1
          object cxGrid19: TcxGrid
            Left = 0
            Top = 0
            Width = 1132
            Height = 680
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView18: TcxGridDBTableView
              OnDblClick = cxGridDBTableView18DblClick
              DataController.DataSource = dsPDCAtraso
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  Column = cxGridDBColumn111
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              OptionsCustomize.ColumnFiltering = False
              OptionsCustomize.ColumnGrouping = False
              OptionsCustomize.ColumnHidingOnGrouping = False
              OptionsCustomize.ColumnMoving = False
              OptionsCustomize.ColumnSorting = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              object cxGridDBTableView18dDtEntregaFim: TcxGridDBColumn
                Caption = 'Prev. Entrega'
                DataBinding.FieldName = 'dDtEntregaFim'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 99
              end
              object cxGridDBColumn107: TcxGridDBColumn
                Caption = 'Pedido'
                DataBinding.FieldName = 'nCdPedido'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn108: TcxGridDBColumn
                Caption = 'Dt. Pedido'
                DataBinding.FieldName = 'dDtPedido'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 90
              end
              object cxGridDBColumn109: TcxGridDBColumn
                Caption = 'Tipo de Pedido'
                DataBinding.FieldName = 'cNmTipoPedido'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 116
              end
              object cxGridDBColumn110: TcxGridDBColumn
                Caption = 'Fornecedor'
                DataBinding.FieldName = 'cNmTerceiro'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 189
              end
              object cxGridDBTableView18cCdProduto: TcxGridDBColumn
                Caption = 'C'#243'd. Prod.'
                DataBinding.FieldName = 'cCdProduto'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 76
              end
              object cxGridDBTableView18cNmItem: TcxGridDBColumn
                Caption = 'Descri'#231#227'o Item'
                DataBinding.FieldName = 'cNmItem'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 184
              end
              object cxGridDBTableView18nSaldoReceber: TcxGridDBColumn
                Caption = 'Qt. a Receber'
                DataBinding.FieldName = 'nSaldoReceber'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 90
              end
              object cxGridDBColumn111: TcxGridDBColumn
                Caption = 'Total Item'
                DataBinding.FieldName = 'nValTotalItem'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 94
              end
            end
            object cxGridLevel18: TcxGridLevel
              GridView = cxGridDBTableView18
            end
          end
        end
        object cxTabSheet24: TcxTabSheet
          Caption = 'Previs'#227'o Entrega (Pr'#243'ximos 15 dias)'
          ImageIndex = 2
          object cxGrid20: TcxGrid
            Left = 0
            Top = 0
            Width = 1132
            Height = 680
            Align = alClient
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView19: TcxGridDBTableView
              OnDblClick = cxGridDBTableView19DblClick
              DataController.DataSource = dsPDCProx15D
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                end
                item
                  Format = '#,##0.00'
                  Kind = skSum
                  Column = cxGridDBColumn120
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              OptionsCustomize.ColumnFiltering = False
              OptionsCustomize.ColumnGrouping = False
              OptionsCustomize.ColumnHidingOnGrouping = False
              OptionsCustomize.ColumnMoving = False
              OptionsCustomize.ColumnSorting = False
              OptionsData.CancelOnExit = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              object cxGridDBColumn112: TcxGridDBColumn
                Caption = 'Prev. Entrega'
                DataBinding.FieldName = 'dDtEntregaFim'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 99
              end
              object cxGridDBColumn113: TcxGridDBColumn
                Caption = 'Pedido'
                DataBinding.FieldName = 'nCdPedido'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
              end
              object cxGridDBColumn114: TcxGridDBColumn
                Caption = 'Dt. Pedido'
                DataBinding.FieldName = 'dDtPedido'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 90
              end
              object cxGridDBColumn115: TcxGridDBColumn
                Caption = 'Tipo de Pedido'
                DataBinding.FieldName = 'cNmTipoPedido'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 116
              end
              object cxGridDBColumn116: TcxGridDBColumn
                Caption = 'Fornecedor'
                DataBinding.FieldName = 'cNmTerceiro'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 189
              end
              object cxGridDBColumn117: TcxGridDBColumn
                Caption = 'C'#243'd. Prod.'
                DataBinding.FieldName = 'nCdPedido'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 76
              end
              object cxGridDBColumn118: TcxGridDBColumn
                Caption = 'Descri'#231#227'o Item'
                DataBinding.FieldName = 'cNmItem'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 184
              end
              object cxGridDBColumn119: TcxGridDBColumn
                Caption = 'Qt. a Receber'
                DataBinding.FieldName = 'nSaldoReceber'
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 90
              end
              object cxGridDBColumn120: TcxGridDBColumn
                Caption = 'Total Item'
                DataBinding.FieldName = 'nValTotalItem'
                HeaderAlignmentHorz = taRightJustify
                Styles.Content = frmMenu.FonteSomenteLeitura
                Width = 94
              end
            end
            object cxGridLevel19: TcxGridLevel
              GridView = cxGridDBTableView19
            end
          end
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 424
    Top = 168
  end
  object qryTop10Clientes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    SQL.Strings = (
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'DECLARE @nCdEmpresa int'
      '       ,@dDtInicial datetime'
      '       ,@dDtFinal   datetime'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      'Set @dDtInicial = Convert(DATETIME,:dDtInicial,103)'
      'Set @dDtFinal   = Convert(DATETIME,:dDtFinal,103)'
      '      '
      'SELECT TOP 10 Pedido.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Sum(nValPedido) as nValTotalPedido'
      '  FROM Pedido'
      
        '       INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Pedid' +
        'o.nCdTerceiro'
      
        '       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedid' +
        'o.nCdTipoPedido'
      ' WHERE Pedido.nCdTabStatusPed  IN (3,4,5)'
      '   AND TipoPedido.cFlgVenda     = 1'
      '   AND TipoPedido.cGerarFinanc  = 1'
      '   AND Pedido.nCdEmpresa        = @nCdEmpresa'
      '   AND Pedido.dDtPedido        >= @dDtInicial'
      '   AND Pedido.dDtPedido         < (@dDtFinal+1)'
      ' GROUP BY Pedido.nCdTerceiro'
      '         ,Terceiro.cNmTerceiro'
      ' ORDER BY 3 DESC')
    Left = 384
    Top = 582
    object qryTop10ClientesnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTop10ClientescNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTop10ClientesnValTotalPedido: TBCDField
      FieldName = 'nValTotalPedido'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
  end
  object dsTop10Clientes: TDataSource
    DataSet = qryTop10Clientes
    Left = 412
    Top = 581
  end
  object qryTop10Fornecedores: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa int'
      '       ,@dDtInicial datetime'
      '       ,@dDtFinal   datetime'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      'Set @dDtInicial = Convert(DATETIME,:dDtInicial,103)'
      'Set @dDtFinal   = Convert(DATETIME,:dDtFinal,103)'
      '      '
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      '      '
      'SELECT TOP 10 Recebimento.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,Sum(ItemRecebimento.nValTotal) as nValTotalReceb'
      '  FROM ItemRecebimento'
      
        '       INNER JOIN Recebimento ON Recebimento.nCdRecebimento = It' +
        'emRecebimento.nCdRecebimento'
      
        '       LEFT  JOIN ItemPedido  ON ItemPedido.nCdItemPedido   = It' +
        'emRecebimento.nCdItemPedido'
      
        '       LEFT  JOIN Pedido      ON Pedido.nCdPedido           = It' +
        'emPedido.nCdPedido'
      
        '       LEFT  JOIN Terceiro    ON Terceiro.nCdTerceiro       = Re' +
        'cebimento.nCdTerceiro'
      
        '       LEFT  JOIN TipoPedido  ON TipoPedido.nCdTipoPedido   = Pe' +
        'dido.nCdTipoPedido'
      ' WHERE Recebimento.nCdTabStatusReceb  IN (4,5)'
      '   AND TipoPedido.cFlgCompra           = 1'
      '   AND TipoPedido.cGerarFinanc         = 1'
      '   AND ItemRecebimento.nCdTipoItemPed IN (1,2,3,5)'
      '   AND Recebimento.nCdEmpresa          = @nCdEmpresa'
      '   AND Recebimento.dDtReceb           >= @dDtInicial'
      '   AND Recebimento.dDtReceb            <(@dDtFinal+1)'
      ' GROUP BY Recebimento.nCdTerceiro'
      '         ,Terceiro.cNmTerceiro'
      ' ORDER BY 3 DESC')
    Left = 384
    Top = 614
    object qryTop10FornecedoresnCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTop10FornecedorescNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTop10FornecedoresnValTotalReceb: TBCDField
      FieldName = 'nValTotalReceb'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 38
      Size = 2
    end
  end
  object dsTop10Fornecedores: TDataSource
    DataSet = qryTop10Fornecedores
    Left = 412
    Top = 613
  end
  object qryTop10Produtos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa int'
      '       ,@dDtInicial datetime'
      '       ,@dDtFinal   datetime'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      'Set @dDtInicial = Convert(DATETIME,:dDtInicial,103)'
      'Set @dDtFinal   = Convert(DATETIME,:dDtFinal,103)'
      '      '
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      '      '
      'SELECT TOP 10 Produto.nCdProduto'
      '      ,Produto.cNmProduto'
      '      ,Sum(ItemPedido.nValTotalItem) as nValTotalPedido'
      '  FROM Pedido'
      
        '       INNER JOIN ItemPedido ON ItemPedido.nCdPedido     = Pedid' +
        'o.nCdPedido'
      
        '       INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Pedid' +
        'o.nCdTerceiro'
      
        '       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedid' +
        'o.nCdTipoPedido'
      
        '       LEFT  JOIN Produto    ON Produto.nCdProduto       = ItemP' +
        'edido.nCdProduto'
      ' WHERE Pedido.nCdTabStatusPed  IN (3,4,5)'
      '   AND ItemPedido.nCdTipoItemPed IN (1,2,3,5)'
      '   AND TipoPedido.cFlgVenda     = 1'
      '   AND TipoPedido.cGerarFinanc  = 1'
      '   AND Pedido.nCdEmpresa        = @nCdEmpresa'
      '   AND Pedido.dDtPedido        >= @dDtInicial'
      '   AND Pedido.dDtPedido         < (@dDtFinal+1)'
      ' GROUP BY Produto.nCdProduto'
      '         ,Produto.cNmProduto'
      ' ORDER BY 3 DESC')
    Left = 584
    Top = 582
    object qryTop10ProdutosnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryTop10ProdutoscNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryTop10ProdutosnValTotalPedido: TBCDField
      FieldName = 'nValTotalPedido'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 38
      Size = 2
    end
  end
  object dsTop10Produtos: TDataSource
    DataSet = qryTop10Produtos
    Left = 620
    Top = 581
  end
  object qryTop10Representantes: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end>
    SQL.Strings = (
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'DECLARE @nCdEmpresa int'
      '       ,@dDtInicial datetime'
      '       ,@dDtFinal   datetime'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      'Set @dDtInicial = Convert(DATETIME,:dDtInicial,103)'
      'Set @dDtFinal   = Convert(DATETIME,:dDtFinal,103)'
      '      '
      'SELECT TOP 10 Pedido.nCdTerceiroRepres'
      '      ,Terceiro.cNmTerceiro'
      '      ,Sum(nValPedido) as nValTotalPedido'
      '  FROM Pedido'
      
        '       INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Pedid' +
        'o.nCdTerceiroRepres'
      
        '       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedid' +
        'o.nCdTipoPedido'
      ' WHERE Pedido.nCdTabStatusPed  IN (3,4,5)'
      '   AND TipoPedido.cFlgVenda     = 1'
      '   AND TipoPedido.cGerarFinanc  = 1'
      '   AND Pedido.nCdEmpresa        = @nCdEmpresa'
      '   AND Pedido.dDtPedido        >= @dDtInicial'
      '   AND Pedido.dDtPedido         < (@dDtFinal+1)'
      ' GROUP BY Pedido.nCdTerceiroRepres'
      '         ,Terceiro.cNmTerceiro'
      ' ORDER BY 3 DESC')
    Left = 392
    Top = 534
    object qryTop10RepresentantesnCdTerceiroRepres: TIntegerField
      FieldName = 'nCdTerceiroRepres'
    end
    object StringField1: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object BCDField1: TBCDField
      FieldName = 'nValTotalPedido'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
  end
  object dsTop10Representantes: TDataSource
    DataSet = qryTop10Representantes
    Left = 436
    Top = 533
  end
  object qryDispCaixa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      
        'SELECT dbo.fn_ZeroEsquerda(ContaBancaria.nCdBanco,3) + '#39'-'#39' + Ban' +
        'co.cNmBanco cBanco'
      '      ,ContaBancaria.cAgencia'
      '      ,ContaBancaria.nCdConta'
      '      ,ContaBancaria.nSaldoConta'
      '      ,ContaBancaria.dDtUltConciliacao'
      '      ,ContaBancaria.nValLimiteCredito'
      
        '      ,(ContaBancaria.nValLimiteCredito + ContaBancaria.nSaldoCo' +
        'nta) as nSaldoTotal'
      '  FROM ContaBancaria '
      
        '       LEFT JOIN Banco ON Banco.nCdBanco = ContaBancaria.nCdBanc' +
        'o'
      ' WHERE nCdEmpresa = :nCdEmpresa')
    Left = 528
    Top = 206
    object qryDispCaixacBanco: TStringField
      FieldName = 'cBanco'
      ReadOnly = True
      Size = 66
    end
    object qryDispCaixacAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryDispCaixanCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryDispCaixanSaldoConta: TBCDField
      FieldName = 'nSaldoConta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDispCaixadDtUltConciliacao: TDateTimeField
      FieldName = 'dDtUltConciliacao'
    end
    object qryDispCaixanValLimiteCredito: TBCDField
      FieldName = 'nValLimiteCredito'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryDispCaixanSaldoTotal: TBCDField
      FieldName = 'nSaldoTotal'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 13
      Size = 2
    end
  end
  object dsDispCaixa: TDataSource
    DataSet = qryDispCaixa
    Left = 568
    Top = 206
  end
  object qryPagarHoje: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Titulo.dDtVenc'
      '      ,Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.cNrNF'
      '      ,Titulo.iParcela'
      '      ,EspTit.cNmEspTit'
      '      ,Terceiro.cNmTerceiro'
      '      ,CategFinanc.cNmCategFinanc'
      '      ,Titulo.nSaldoTit'
      '      ,Titulo.cSenso'
      '  FROM Titulo'
      
        '       LEFT JOIN EspTit      ON EspTit.nCdEspTit           = Tit' +
        'ulo.nCdEspTit'
      
        '       LEFT JOIN Terceiro    ON Terceiro.nCdTerceiro       = Tit' +
        'ulo.nCdTerceiro'
      
        '       LEFT JOIN CategFinanc ON CategFinanc.nCdCategFinanc = Tit' +
        'ulo.nCdCategFinanc'
      ' WHERE Titulo.dDtCancel      IS NULL'
      '   AND Titulo.nSaldoTit       > 0'
      '   AND Titulo.cSenso          = '#39'D'#39
      '   AND Titulo.dDtVenc        >= dbo.fn_OnlyDate(GetDate())'
      '   AND Titulo.dDtVenc         < (dbo.fn_OnlyDate(GetDate())+1)'
      '   AND Titulo.nCdEmpresa      = :nCdEmpresa')
    Left = 652
    Top = 295
    object qryPagarHojedDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryPagarHojenCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryPagarHojecNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryPagarHojecNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryPagarHojeiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryPagarHojecNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryPagarHojecNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryPagarHojecNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object qryPagarHojenSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryPagarHojecSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
  end
  object dsPagarHoje: TDataSource
    DataSet = qryPagarHoje
    Left = 684
    Top = 293
  end
  object qryPagarAtraso: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Titulo.dDtVenc'
      '      ,Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.cNrNF'
      '      ,Titulo.iParcela'
      '      ,EspTit.cNmEspTit'
      '      ,Terceiro.cNmTerceiro'
      '      ,CategFinanc.cNmCategFinanc'
      '      ,Titulo.nSaldoTit'
      '      ,Titulo.cSenso'
      '  FROM Titulo'
      
        '       LEFT JOIN EspTit      ON EspTit.nCdEspTit           = Tit' +
        'ulo.nCdEspTit'
      
        '       LEFT JOIN Terceiro    ON Terceiro.nCdTerceiro       = Tit' +
        'ulo.nCdTerceiro'
      
        '       LEFT JOIN CategFinanc ON CategFinanc.nCdCategFinanc = Tit' +
        'ulo.nCdCategFinanc'
      ' WHERE Titulo.dDtCancel      IS NULL'
      '   AND Titulo.nSaldoTit       > 0'
      '   AND Titulo.cSenso          = '#39'D'#39
      '   AND Titulo.dDtVenc         < dbo.fn_OnlyDate(GetDate())'
      '   AND Titulo.nCdEmpresa      = :nCdEmpresa'
      'ORDER BY dDtVenc   ')
    Left = 660
    Top = 327
    object DateTimeField1: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object IntegerField1: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object StringField2: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object StringField3: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object IntegerField2: TIntegerField
      FieldName = 'iParcela'
    end
    object StringField4: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object StringField5: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object StringField6: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object BCDField2: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object StringField7: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
  end
  object dsPagarAtraso: TDataSource
    DataSet = qryPagarAtraso
    Left = 692
    Top = 325
  end
  object qryPagarPeriodo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'dDtInicial'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtFinal'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Titulo.dDtVenc'
      '      ,Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.cNrNF'
      '      ,Titulo.iParcela'
      '      ,EspTit.cNmEspTit'
      '      ,Terceiro.cNmTerceiro'
      '      ,CategFinanc.cNmCategFinanc'
      '      ,Titulo.nSaldoTit'
      '      ,Titulo.cSenso'
      '  FROM Titulo'
      
        '       LEFT JOIN EspTit      ON EspTit.nCdEspTit           = Tit' +
        'ulo.nCdEspTit'
      
        '       LEFT JOIN Terceiro    ON Terceiro.nCdTerceiro       = Tit' +
        'ulo.nCdTerceiro'
      
        '       LEFT JOIN CategFinanc ON CategFinanc.nCdCategFinanc = Tit' +
        'ulo.nCdCategFinanc'
      ' WHERE Titulo.dDtCancel      IS NULL'
      '   AND Titulo.nSaldoTit       > 0'
      '   AND Titulo.cSenso          = '#39'D'#39
      
        '   AND Titulo.dDtVenc        >= Convert(DATETIME,:dDtInicial,103' +
        ')'
      
        '   AND Titulo.dDtVenc         < (Convert(DATETIME,:dDtFinal,103)' +
        '+1)'
      '   AND Titulo.nCdEmpresa      = :nCdEmpresa'
      'ORDER BY dDtVenc   ')
    Left = 668
    Top = 359
    object DateTimeField2: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object IntegerField3: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object StringField8: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object StringField9: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object IntegerField4: TIntegerField
      FieldName = 'iParcela'
    end
    object StringField10: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object StringField11: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object StringField12: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object BCDField3: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object StringField13: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
  end
  object dsPagarPeriodo: TDataSource
    DataSet = qryPagarPeriodo
    Left = 700
    Top = 357
  end
  object qryReceberHoje: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Titulo.dDtVenc'
      '      ,Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.cNrNF'
      '      ,Titulo.iParcela'
      '      ,EspTit.cNmEspTit'
      '      ,Terceiro.cNmTerceiro'
      '      ,CategFinanc.cNmCategFinanc'
      '      ,Titulo.nSaldoTit'
      '      ,Titulo.cSenso'
      '  FROM Titulo'
      
        '       LEFT JOIN EspTit      ON EspTit.nCdEspTit           = Tit' +
        'ulo.nCdEspTit'
      
        '       LEFT JOIN Terceiro    ON Terceiro.nCdTerceiro       = Tit' +
        'ulo.nCdTerceiro'
      
        '       LEFT JOIN CategFinanc ON CategFinanc.nCdCategFinanc = Tit' +
        'ulo.nCdCategFinanc'
      ' WHERE Titulo.dDtCancel      IS NULL'
      '   AND Titulo.nSaldoTit       > 0'
      '   AND Titulo.cSenso          = '#39'C'#39
      '   AND Titulo.dDtVenc        >= dbo.fn_OnlyDate(GetDate())'
      '   AND Titulo.dDtVenc         < (dbo.fn_OnlyDate(GetDate())+1)'
      '   AND Titulo.nCdEmpresa      = :nCdEmpresa')
    Left = 660
    Top = 263
    object DateTimeField3: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object IntegerField5: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object StringField14: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object StringField15: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object IntegerField6: TIntegerField
      FieldName = 'iParcela'
    end
    object StringField16: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object StringField17: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object StringField18: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object BCDField4: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object StringField19: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
  end
  object dsReceberHoje: TDataSource
    DataSet = qryReceberHoje
    Left = 692
    Top = 261
  end
  object qryReceberAtraso: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Titulo.dDtVenc'
      '      ,Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.cNrNF'
      '      ,Titulo.iParcela'
      '      ,EspTit.cNmEspTit'
      '      ,Terceiro.cNmTerceiro'
      '      ,CategFinanc.cNmCategFinanc'
      '      ,Titulo.nSaldoTit'
      '      ,Titulo.cSenso'
      '  FROM Titulo'
      
        '       LEFT JOIN EspTit      ON EspTit.nCdEspTit           = Tit' +
        'ulo.nCdEspTit'
      
        '       LEFT JOIN Terceiro    ON Terceiro.nCdTerceiro       = Tit' +
        'ulo.nCdTerceiro'
      
        '       LEFT JOIN CategFinanc ON CategFinanc.nCdCategFinanc = Tit' +
        'ulo.nCdCategFinanc'
      ' WHERE Titulo.dDtCancel      IS NULL'
      '   AND Titulo.nSaldoTit       > 0'
      '   AND Titulo.cSenso          = '#39'C'#39
      '   AND Titulo.dDtVenc         < dbo.fn_OnlyDate(GetDate())'
      '   AND Titulo.nCdEmpresa      = :nCdEmpresa'
      'ORDER BY dDtVenc   ')
    Left = 652
    Top = 231
    object DateTimeField4: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object IntegerField7: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object StringField20: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object StringField21: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object IntegerField8: TIntegerField
      FieldName = 'iParcela'
    end
    object StringField22: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object StringField23: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object StringField24: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object BCDField5: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object StringField25: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
  end
  object dsReceberAtraso: TDataSource
    DataSet = qryReceberAtraso
    Left = 684
    Top = 229
  end
  object qryReceberPeriodo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'dDtInicial'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtFinal'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Titulo.dDtVenc'
      '      ,Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.cNrNF'
      '      ,Titulo.iParcela'
      '      ,EspTit.cNmEspTit'
      '      ,Terceiro.cNmTerceiro'
      '      ,CategFinanc.cNmCategFinanc'
      '      ,Titulo.nSaldoTit'
      '      ,Titulo.cSenso'
      '  FROM Titulo'
      
        '       LEFT JOIN EspTit      ON EspTit.nCdEspTit           = Tit' +
        'ulo.nCdEspTit'
      
        '       LEFT JOIN Terceiro    ON Terceiro.nCdTerceiro       = Tit' +
        'ulo.nCdTerceiro'
      
        '       LEFT JOIN CategFinanc ON CategFinanc.nCdCategFinanc = Tit' +
        'ulo.nCdCategFinanc'
      ' WHERE Titulo.dDtCancel      IS NULL'
      '   AND Titulo.nSaldoTit       > 0'
      '   AND Titulo.cSenso          = '#39'C'#39
      
        '   AND Titulo.dDtVenc        >= Convert(DATETIME,:dDtInicial,103' +
        ')'
      
        '   AND Titulo.dDtVenc         < (Convert(DATETIME,:dDtFinal,103)' +
        '+1)'
      '   AND Titulo.nCdEmpresa      = :nCdEmpresa'
      'ORDER BY dDtVenc')
    Left = 652
    Top = 199
    object DateTimeField5: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object IntegerField9: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object StringField26: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object StringField27: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object IntegerField10: TIntegerField
      FieldName = 'iParcela'
    end
    object StringField28: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object StringField29: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object StringField30: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object BCDField6: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object StringField31: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
  end
  object dsReceberPeriodo: TDataSource
    DataSet = qryReceberPeriodo
    Left = 684
    Top = 197
  end
  object qryPreparaTempMeta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Venda'#39') IS NULL)'
      'BEGIN'
      ''
      #9#9'CREATE TABLE #Temp_Venda (iDiasUteis           int'
      #9#9#9#9#9#9#9#9'  ,iDiasDecorridos      int'
      #9#9#9#9#9#9#9#9'  ,nPercDecorridos      decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nValMeta             decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nValMetaDia          decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nPercMetaAting_Venda decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nPercMetaAting_Fat   decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nProjecao_Venda      decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nProjecao_Fat        decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nValVendaDia_AtMeta  decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nValFatDia_AtMeta    decimal(12,2))'
      'END'
      ''
      'IF (OBJECT_ID('#39'tempdb..#Temp_Venda_Dia'#39') IS NULL)'
      'BEGIN'
      ''
      #9#9'CREATE TABLE #Temp_Venda_Dia (dData                datetime'
      
        #9#9#9#9#9#9#9#9#9'  ,nQtdePedido          int           default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValVenda            decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValFaturamento      decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValMeta             decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValDiferenca        decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValVendaAcum        decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValFaturamentoAcum  decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValMetaAcum         decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValDiferencaAcum    decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,cFlgDiaUtil          int           default 0 not nul' +
        'l)'
      ''
      'END'
      ''
      'TRUNCATE TABLE #Temp_Venda'
      'TRUNCATE TABLE #Temp_Venda_Dia')
    Left = 340
    Top = 277
  end
  object qryTempVenda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Venda'#39') IS NULL)'
      'BEGIN'
      ''
      #9#9'CREATE TABLE #Temp_Venda (iDiasUteis           int'
      #9#9#9#9#9#9#9#9'  ,iDiasDecorridos      int'
      #9#9#9#9#9#9#9#9'  ,nPercDecorridos      decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nValMeta             decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nValMetaDia          decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nPercMetaAting_Venda decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nPercMetaAting_Fat   decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nProjecao_Venda      decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nProjecao_Fat        decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nValVendaDia_AtMeta  decimal(12,2)'
      #9#9#9#9#9#9#9#9'  ,nValFatDia_AtMeta    decimal(12,2))'
      'END'
      ''
      'SELECT *'
      '  FROM #Temp_Venda')
    Left = 380
    Top = 277
    object qryTempVendaiDiasUteis: TIntegerField
      FieldName = 'iDiasUteis'
    end
    object qryTempVendaiDiasDecorridos: TIntegerField
      FieldName = 'iDiasDecorridos'
    end
    object qryTempVendanPercDecorridos: TBCDField
      FieldName = 'nPercDecorridos'
      Precision = 12
      Size = 2
    end
    object qryTempVendanValMeta: TBCDField
      FieldName = 'nValMeta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendanValMetaDia: TBCDField
      FieldName = 'nValMetaDia'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendanPercMetaAting_Venda: TBCDField
      FieldName = 'nPercMetaAting_Venda'
      DisplayFormat = '#,##0.00 %'
      Precision = 12
      Size = 2
    end
    object qryTempVendanPercMetaAting_Fat: TBCDField
      FieldName = 'nPercMetaAting_Fat'
      DisplayFormat = '#,##0.00 %'
      Precision = 12
      Size = 2
    end
    object qryTempVendanProjecao_Venda: TBCDField
      FieldName = 'nProjecao_Venda'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendanProjecao_Fat: TBCDField
      FieldName = 'nProjecao_Fat'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendanValVendaDia_AtMeta: TBCDField
      FieldName = 'nValVendaDia_AtMeta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendanValFatDia_AtMeta: TBCDField
      FieldName = 'nValFatDia_AtMeta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object qryTempVendaDia: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Venda_Dia'#39') IS NULL)'
      'BEGIN'
      ''
      #9#9'CREATE TABLE #Temp_Venda_Dia (dData                datetime'
      
        #9#9#9#9#9#9#9#9#9'  ,nQtdePedido          int           default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValVenda            decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValFaturamento      decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValMeta             decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValDiferenca        decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValVendaAcum        decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValFaturamentoAcum  decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValMetaAcum         decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValDiferencaAcum    decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,cFlgDiaUtil          int           default 0 not nul' +
        'l)'
      ''
      'END'
      ''
      'SELECT *'
      '  FROM #Temp_Venda_Dia'
      ' ORDER BY dData')
    Left = 420
    Top = 277
    object qryTempVendaDiadData: TDateTimeField
      FieldName = 'dData'
    end
    object qryTempVendaDianQtdePedido: TIntegerField
      FieldName = 'nQtdePedido'
      DisplayFormat = ',0'
    end
    object qryTempVendaDianValVenda: TBCDField
      FieldName = 'nValVenda'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendaDianValFaturamento: TBCDField
      FieldName = 'nValFaturamento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendaDianValMeta: TBCDField
      FieldName = 'nValMeta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendaDianValDiferenca: TBCDField
      FieldName = 'nValDiferenca'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendaDianValVendaAcum: TBCDField
      FieldName = 'nValVendaAcum'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendaDianValFaturamentoAcum: TBCDField
      FieldName = 'nValFaturamentoAcum'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendaDianValMetaAcum: TBCDField
      FieldName = 'nValMetaAcum'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendaDianValDiferencaAcum: TBCDField
      FieldName = 'nValDiferencaAcum'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTempVendaDiacFlgDiaUtil: TIntegerField
      FieldName = 'cFlgDiaUtil'
    end
  end
  object SPREL_VENDA_DIARIA: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SPREL_VENDA_DIARIA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
      end
      item
        Name = '@cCompetencia'
        Attributes = [paNullable]
        DataType = ftString
        Size = 7
      end>
    Left = 460
    Top = 277
  end
  object dsTempVendaDia: TDataSource
    DataSet = qryTempVendaDia
    Left = 428
    Top = 318
  end
  object DataSource1: TDataSource
    DataSet = qryTempVenda
    Left = 560
    Top = 264
  end
  object dsTempVenda: TDataSource
    DataSet = qryTempVenda
    Left = 382
    Top = 317
  end
  object qrySomaVendaDia: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Venda_Dia'#39') IS NULL)'
      'BEGIN'
      ''
      #9#9'CREATE TABLE #Temp_Venda_Dia (dData                datetime'
      
        #9#9#9#9#9#9#9#9#9'  ,nQtdePedido          int           default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValVenda            decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValFaturamento      decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValMeta             decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValDiferenca        decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValVendaAcum        decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValFaturamentoAcum  decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValMetaAcum         decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,nValDiferencaAcum    decimal(12,2) default 0 not nul' +
        'l'
      
        #9#9#9#9#9#9#9#9#9'  ,cFlgDiaUtil          int           default 0 not nul' +
        'l)'
      ''
      'END'
      ''
      'SELECT Sum(nValVenda)       as nValTotalVenda'
      '      ,Sum(nValFaturamento) as nValFaturamento'
      '  FROM #Temp_Venda_Dia'
      '')
    Left = 486
    Top = 309
    object qrySomaVendaDianValTotalVenda: TBCDField
      DisplayLabel = 'Venda M'#234's'
      FieldName = 'nValTotalVenda'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 38
      Size = 2
    end
    object qrySomaVendaDianValFaturamento: TBCDField
      DisplayLabel = 'Venda M'#234's'
      FieldName = 'nValFaturamento'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 38
      Size = 2
    end
  end
  object dsSomaVendaDia: TDataSource
    DataSet = qrySomaVendaDia
    Left = 524
    Top = 318
  end
  object qryProdutoAbaixoMinimo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa int'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      ''
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT Produto.nCdProduto'
      '      ,Produto.cNmProduto'
      '      ,Produto.cReferencia'
      '      ,Produto.cUnidadeMedida'
      '      ,cNmGrupoProduto'
      '      ,nQtdeEstoque'
      
        '      ,CASE WHEN cFlgTipoPonto = '#39'D'#39' THEN (nEstoqueSeg * Produto' +
        'Empresa.nConsumoMedDia)'
      '            ELSE nEstoqueSeg'
      '       END as nEstoqueMinimo'
      '      ,Produto.iDiaEntrega as iLeadTime'
      '      ,IsNull((SELECT Sum(nQtdePed-nQtdeExpRec-nQtdeCanc)'
      '                 FROM ItemPedido'
      
        '                      INNER JOIN Pedido     ON Pedido.nCdPedido ' +
        '        = ItemPedido.nCdPedido'
      
        '                      INNER JOIN TipoPedido ON TipoPedido.nCdTip' +
        'oPedido = Pedido.nCdTipoPedido'
      '                WHERE Pedido.nCdEmpresa       = @nCdEmpresa'
      '                  AND TipoPedido.cFlgCompra   = 1'
      '                  AND Pedido.nCdTabStatusPed IN (3,4)'
      
        '                  AND ItemPedido.nCdProduto   = Produto.nCdProdu' +
        'to),0) as nQtdeEmPedido'
      '  FROM Produto'
      
        '       LEFT  JOIN GrupoProduto   ON GrupoProduto.nCdGrupoProduto' +
        ' = Produto.nCdGrupo_Produto'
      
        '       INNER JOIN ProdutoEmpresa ON ProdutoEmpresa.nCdProduto   ' +
        ' = Produto.nCdProduto'
      ' WHERE ProdutoEmpresa.nCdEmpresa = @nCdEmpresa'
      '   AND Produto.nCdStatus         = 1'
      
        '   AND Produto.nQtdeEstoque < CASE WHEN cFlgTipoPonto = '#39'D'#39' THEN' +
        ' (nEstoqueSeg * ProdutoEmpresa.nConsumoMedDia)'
      '                                   ELSE nEstoqueSeg'
      '                              END'
      '   AND cFlgProdEstoque = 1'
      ' ORDER BY cNmGrupoProduto'
      '         ,cNmProduto')
    Left = 104
    Top = 157
    object qryProdutoAbaixoMinimonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutoAbaixoMinimocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object qryProdutoAbaixoMinimocReferencia: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object qryProdutoAbaixoMinimocUnidadeMedida: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object qryProdutoAbaixoMinimocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
    object qryProdutoAbaixoMinimonQtdeEstoque: TBCDField
      FieldName = 'nQtdeEstoque'
      DisplayFormat = ',0'
      Precision = 12
    end
    object qryProdutoAbaixoMinimonEstoqueMinimo: TBCDField
      FieldName = 'nEstoqueMinimo'
      ReadOnly = True
      DisplayFormat = ',0'
      Precision = 25
    end
    object qryProdutoAbaixoMinimoiLeadTime: TIntegerField
      FieldName = 'iLeadTime'
      DisplayFormat = ',0'
    end
    object qryProdutoAbaixoMinimonQtdeEmPedido: TBCDField
      FieldName = 'nQtdeEmPedido'
      ReadOnly = True
      DisplayFormat = ',0'
      Precision = 38
    end
  end
  object dsProdutoAbaixoMinimo: TDataSource
    DataSet = qryProdutoAbaixoMinimo
    Left = 134
    Top = 157
  end
  object qryProdutoAbaixoRessuprimento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa int'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      ''
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'SELECT Produto.nCdProduto'
      '      ,Produto.cNmProduto'
      '      ,Produto.cReferencia'
      '      ,Produto.cUnidadeMedida'
      '      ,cNmGrupoProduto'
      '      ,nQtdeEstoque'
      
        '      ,CASE WHEN cFlgTipoPonto = '#39'D'#39' THEN (nEstoqueRessup * Prod' +
        'utoEmpresa.nConsumoMedDia)'
      '            ELSE nEstoqueRessup'
      '       END as nEstoqueMinimo'
      '      ,Produto.iDiaEntrega as iLeadTime'
      '      ,IsNull((SELECT Sum(nQtdePed-nQtdeExpRec-nQtdeCanc)'
      '                 FROM ItemPedido'
      
        '                      INNER JOIN Pedido     ON Pedido.nCdPedido ' +
        '        = ItemPedido.nCdPedido'
      
        '                      INNER JOIN TipoPedido ON TipoPedido.nCdTip' +
        'oPedido = Pedido.nCdTipoPedido'
      '                WHERE Pedido.nCdEmpresa       = @nCdEmpresa'
      '                  AND TipoPedido.cFlgCompra   = 1'
      '                  AND Pedido.nCdTabStatusPed IN (3,4)'
      
        '                  AND ItemPedido.nCdProduto   = Produto.nCdProdu' +
        'to),0) as nQtdeEmPedido'
      '  FROM Produto'
      
        '       LEFT  JOIN GrupoProduto   ON GrupoProduto.nCdGrupoProduto' +
        ' = Produto.nCdGrupo_Produto'
      
        '       INNER JOIN ProdutoEmpresa ON ProdutoEmpresa.nCdProduto   ' +
        ' = Produto.nCdProduto'
      ' WHERE ProdutoEmpresa.nCdEmpresa = @nCdEmpresa'
      '   AND Produto.nCdStatus         = 1'
      
        '   AND Produto.nQtdeEstoque < CASE WHEN cFlgTipoPonto = '#39'D'#39' THEN' +
        ' (nEstoqueRessup * ProdutoEmpresa.nConsumoMedDia)'
      '                                   ELSE nEstoqueRessup'
      '                              END'
      
        '   AND Produto.nQtdeEstoque >= CASE WHEN cFlgTipoPonto = '#39'D'#39' THE' +
        'N (nEstoqueSeg * ProdutoEmpresa.nConsumoMedDia)'
      '                                    ELSE nEstoqueSeg'
      '                               END'
      '   AND cFlgProdEstoque = 1'
      ' ORDER BY cNmGrupoProduto'
      '         ,cNmProduto')
    Left = 104
    Top = 189
    object IntegerField11: TIntegerField
      FieldName = 'nCdProduto'
    end
    object StringField32: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
    object StringField33: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object StringField34: TStringField
      FieldName = 'cUnidadeMedida'
      FixedChar = True
      Size = 3
    end
    object StringField35: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
    object BCDField7: TBCDField
      FieldName = 'nQtdeEstoque'
      DisplayFormat = ',0'
      Precision = 12
    end
    object BCDField8: TBCDField
      FieldName = 'nEstoqueMinimo'
      ReadOnly = True
      DisplayFormat = ',0'
      Precision = 25
    end
    object IntegerField12: TIntegerField
      FieldName = 'iLeadTime'
      DisplayFormat = ',0'
    end
    object BCDField9: TBCDField
      FieldName = 'nQtdeEmPedido'
      ReadOnly = True
      DisplayFormat = ',0'
      Precision = 32
    end
  end
  object dsProdutoAbaixoRessuprimento: TDataSource
    DataSet = qryProdutoAbaixoRessuprimento
    Left = 134
    Top = 189
  end
  object qryTotalEstoqueGrupo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT cNmGrupoProduto'
      '      ,Sum(nQtdeEstoque * nValCusto) as nValTotalCusto'
      '  FROM Produto'
      
        '       INNER JOIN GrupoProduto ON GrupoProduto.nCdGrupoProduto =' +
        ' Produto.nCdGrupo_Produto'
      ' WHERE Produto.nCdStatus = 1'
      '   AND cFlgProdEstoque   = 1'
      ' GROUP BY cNmGrupoProduto'
      'HAVING Sum(nQtdeEstoque * nValCusto) > 0'
      ' ORDER BY 1')
    Left = 104
    Top = 230
    object qryTotalEstoqueGrupocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
    object qryTotalEstoqueGruponValTotalCusto: TBCDField
      FieldName = 'nValTotalCusto'
      ReadOnly = True
      Precision = 38
      Size = 10
    end
  end
  object dsTotalEstoqueGrupo: TDataSource
    DataSet = qryTotalEstoqueGrupo
    Left = 134
    Top = 229
  end
  object qryOPAberta: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdOrdemProducao'
      '      ,cNumeroOP                            '
      
        '      ,SUBSTRING(cNmTabTipoOrigemOP,1,20)   as cNmTabTipoOrigemO' +
        'P'
      '      ,dDtAbertura'
      '      ,dDtInicioPrevisto'
      '      ,dDtPrevConclusao'
      '      ,SUBSTRING(TipoOP.cNmTipoOP,1,20)     as cNmTipoOP'
      '      ,Produto.nCdProduto                   '
      '      ,Produto.cReferencia                  '
      '      ,SUBSTRING(Produto.cNmProduto,1,50)   as cNmProduto'
      '      ,nQtdePlanejada                       '
      '      ,OP.nCdPedido                         '
      '      ,SUBSTRING(Terceiro.cNmTerceiro,1,30) as cNmTerceiro'
      
        '      ,CASE WHEN dDtPrevConclusao < dbo.fn_OnlyDate(GetDate()) T' +
        'HEN '#39'Sim'#39
      '            ELSE NULL'
      '       END cFlgAtraso'
      '      ,nQtdeConcluida'
      '      ,nQtdeCancelada'
      '      ,nQtdeRetrabalho'
      '      ,nQtdeRefugo'
      
        '      ,(nQtdePlanejada - nQtdeConcluida - nQtdeCancelada - nQtde' +
        'Retrabalho - nQtdeRefugo) as nSaldoProduzir'
      '      ,cNmTabTipoStatusOP'
      '  FROM OrdemProducao OP'
      
        '       INNER JOIN TipoOP          ON TipoOP.nCdTipoOP           ' +
        '        = OP.nCdTipoOP'
      
        '       INNER JOIN Produto         ON Produto.nCdProduto         ' +
        '        = OP.nCdProduto'
      
        '       INNER JOIN TabTipoOrigemOP ON TabTipoOrigemOP.nCdTabTipoO' +
        'rigemOP = OP.nCdTabTipoOrigemOP'
      
        '       LEFT  JOIN Pedido          ON Pedido.nCdPedido           ' +
        '        = OP.nCdPedido'
      
        '       LEFT  JOIN Terceiro        ON Terceiro.nCdTerceiro       ' +
        '        = Pedido.nCdTerceiro'
      
        '       LEFT  JOIN TabTipoStatusOP ON TabTipoStatusOP.nCdTabTipoS' +
        'tatusOP = OP.nCdTabTipoStatusOP'
      ' WHERE OP.nCdEmpresa          = :nPK'
      '   AND OP.nCdTabTipoStatusOP <= 4')
    Left = 220
    Top = 157
    object IntegerField13: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object StringField36: TStringField
      FieldName = 'cNumeroOP'
    end
    object StringField37: TStringField
      FieldName = 'cNmTabTipoOrigemOP'
      ReadOnly = True
    end
    object DateTimeField6: TDateTimeField
      FieldName = 'dDtAbertura'
    end
    object DateTimeField7: TDateTimeField
      FieldName = 'dDtInicioPrevisto'
    end
    object DateTimeField8: TDateTimeField
      FieldName = 'dDtPrevConclusao'
    end
    object StringField38: TStringField
      FieldName = 'cNmTipoOP'
      ReadOnly = True
    end
    object IntegerField14: TIntegerField
      FieldName = 'nCdProduto'
    end
    object StringField39: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object StringField40: TStringField
      FieldName = 'cNmProduto'
      ReadOnly = True
      Size = 50
    end
    object BCDField10: TBCDField
      FieldName = 'nQtdePlanejada'
      Precision = 14
      Size = 6
    end
    object IntegerField15: TIntegerField
      FieldName = 'nCdPedido'
    end
    object StringField41: TStringField
      FieldName = 'cNmTerceiro'
      ReadOnly = True
      Size = 30
    end
    object IntegerField16: TIntegerField
      FieldName = 'nQtdeCancelada'
    end
    object IntegerField17: TIntegerField
      FieldName = 'nQtdeRetrabalho'
    end
    object IntegerField18: TIntegerField
      FieldName = 'nQtdeRefugo'
    end
    object BCDField11: TBCDField
      FieldName = 'nSaldoProduzir'
      ReadOnly = True
      Precision = 20
      Size = 6
    end
    object StringField42: TStringField
      FieldName = 'cFlgAtraso'
      ReadOnly = True
      Size = 3
    end
    object IntegerField19: TIntegerField
      FieldName = 'nQtdeConcluida'
    end
    object qryOPAbertacNmTabTipoStatusOP: TStringField
      FieldName = 'cNmTabTipoStatusOP'
      Size = 50
    end
  end
  object dsOPAberta: TDataSource
    DataSet = qryOPAberta
    Left = 260
    Top = 157
  end
  object qryOPAguardaProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdOrdemProducao'
      '      ,cNumeroOP                            '
      
        '      ,SUBSTRING(cNmTabTipoOrigemOP,1,20)   as cNmTabTipoOrigemO' +
        'P'
      '      ,dDtAbertura'
      '      ,dDtInicioPrevisto'
      '      ,dDtPrevConclusao'
      '      ,SUBSTRING(TipoOP.cNmTipoOP,1,20)     as cNmTipoOP'
      '      ,Produto.nCdProduto                   '
      '      ,Produto.cReferencia                  '
      '      ,SUBSTRING(Produto.cNmProduto,1,50)   as cNmProduto'
      '      ,nQtdePlanejada                       '
      '      ,OP.nCdPedido                         '
      '      ,SUBSTRING(Terceiro.cNmTerceiro,1,30) as cNmTerceiro'
      
        '      ,CASE WHEN dDtPrevConclusao < dbo.fn_OnlyDate(GetDate()) T' +
        'HEN '#39'Sim'#39
      '            ELSE NULL'
      '       END cFlgAtraso'
      '      ,nQtdeConcluida'
      '      ,nQtdeCancelada'
      '      ,nQtdeRetrabalho'
      '      ,nQtdeRefugo'
      
        '      ,(nQtdePlanejada - nQtdeConcluida - nQtdeCancelada - nQtde' +
        'Retrabalho - nQtdeRefugo) as nSaldoProduzir'
      '      ,cNmTabTipoStatusOP'
      '  FROM OrdemProducao OP'
      
        '       INNER JOIN TipoOP          ON TipoOP.nCdTipoOP           ' +
        '        = OP.nCdTipoOP'
      
        '       INNER JOIN Produto         ON Produto.nCdProduto         ' +
        '        = OP.nCdProduto'
      
        '       INNER JOIN TabTipoOrigemOP ON TabTipoOrigemOP.nCdTabTipoO' +
        'rigemOP = OP.nCdTabTipoOrigemOP'
      
        '       LEFT  JOIN Pedido          ON Pedido.nCdPedido           ' +
        '        = OP.nCdPedido'
      
        '       LEFT  JOIN Terceiro        ON Terceiro.nCdTerceiro       ' +
        '        = Pedido.nCdTerceiro'
      
        '       LEFT  JOIN TabTipoStatusOP ON TabTipoStatusOP.nCdTabTipoS' +
        'tatusOP = OP.nCdTabTipoStatusOP'
      ' WHERE OP.nCdEmpresa             = :nPK'
      '   AND OP.nCdTabTipoStatusOP    <= 4'
      '   AND OP.cFlgAguardandoProduto  = 1')
    Left = 220
    Top = 189
    object IntegerField20: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object StringField43: TStringField
      FieldName = 'cNumeroOP'
    end
    object StringField44: TStringField
      FieldName = 'cNmTabTipoOrigemOP'
      ReadOnly = True
    end
    object DateTimeField9: TDateTimeField
      FieldName = 'dDtAbertura'
    end
    object DateTimeField10: TDateTimeField
      FieldName = 'dDtInicioPrevisto'
    end
    object DateTimeField11: TDateTimeField
      FieldName = 'dDtPrevConclusao'
    end
    object StringField45: TStringField
      FieldName = 'cNmTipoOP'
      ReadOnly = True
    end
    object IntegerField21: TIntegerField
      FieldName = 'nCdProduto'
    end
    object StringField46: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object StringField47: TStringField
      FieldName = 'cNmProduto'
      ReadOnly = True
      Size = 50
    end
    object BCDField12: TBCDField
      FieldName = 'nQtdePlanejada'
      Precision = 14
      Size = 6
    end
    object IntegerField22: TIntegerField
      FieldName = 'nCdPedido'
    end
    object StringField48: TStringField
      FieldName = 'cNmTerceiro'
      ReadOnly = True
      Size = 30
    end
    object IntegerField23: TIntegerField
      FieldName = 'nQtdeCancelada'
    end
    object IntegerField24: TIntegerField
      FieldName = 'nQtdeRetrabalho'
    end
    object IntegerField25: TIntegerField
      FieldName = 'nQtdeRefugo'
    end
    object BCDField13: TBCDField
      FieldName = 'nSaldoProduzir'
      ReadOnly = True
      Precision = 20
      Size = 6
    end
    object StringField49: TStringField
      FieldName = 'cFlgAtraso'
      ReadOnly = True
      Size = 3
    end
    object IntegerField26: TIntegerField
      FieldName = 'nQtdeConcluida'
    end
    object StringField50: TStringField
      FieldName = 'cNmTabTipoStatusOP'
      Size = 50
    end
  end
  object dsOPAguardaProduto: TDataSource
    DataSet = qryOPAguardaProduto
    Left = 260
    Top = 189
  end
  object qryOPAguardaServico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdOrdemProducao'
      '      ,cNumeroOP                            '
      
        '      ,SUBSTRING(cNmTabTipoOrigemOP,1,20)   as cNmTabTipoOrigemO' +
        'P'
      '      ,dDtAbertura'
      '      ,dDtInicioPrevisto'
      '      ,dDtPrevConclusao'
      '      ,SUBSTRING(TipoOP.cNmTipoOP,1,20)     as cNmTipoOP'
      '      ,Produto.nCdProduto                   '
      '      ,Produto.cReferencia                  '
      '      ,SUBSTRING(Produto.cNmProduto,1,50)   as cNmProduto'
      '      ,nQtdePlanejada                       '
      '      ,OP.nCdPedido                         '
      '      ,SUBSTRING(Terceiro.cNmTerceiro,1,30) as cNmTerceiro'
      
        '      ,CASE WHEN dDtPrevConclusao < dbo.fn_OnlyDate(GetDate()) T' +
        'HEN '#39'Sim'#39
      '            ELSE NULL'
      '       END cFlgAtraso'
      '      ,nQtdeConcluida'
      '      ,nQtdeCancelada'
      '      ,nQtdeRetrabalho'
      '      ,nQtdeRefugo'
      
        '      ,(nQtdePlanejada - nQtdeConcluida - nQtdeCancelada - nQtde' +
        'Retrabalho - nQtdeRefugo) as nSaldoProduzir'
      '      ,cNmTabTipoStatusOP'
      '  FROM OrdemProducao OP'
      
        '       INNER JOIN TipoOP          ON TipoOP.nCdTipoOP           ' +
        '        = OP.nCdTipoOP'
      
        '       INNER JOIN Produto         ON Produto.nCdProduto         ' +
        '        = OP.nCdProduto'
      
        '       INNER JOIN TabTipoOrigemOP ON TabTipoOrigemOP.nCdTabTipoO' +
        'rigemOP = OP.nCdTabTipoOrigemOP'
      
        '       LEFT  JOIN Pedido          ON Pedido.nCdPedido           ' +
        '        = OP.nCdPedido'
      
        '       LEFT  JOIN Terceiro        ON Terceiro.nCdTerceiro       ' +
        '        = Pedido.nCdTerceiro'
      
        '       LEFT  JOIN TabTipoStatusOP ON TabTipoStatusOP.nCdTabTipoS' +
        'tatusOP = OP.nCdTabTipoStatusOP'
      ' WHERE OP.nCdEmpresa                 = :nPK'
      '   AND OP.nCdTabTipoStatusOP        <= 4'
      '   AND OP.cFlgAguardandoServExterno  = 1')
    Left = 220
    Top = 221
    object IntegerField27: TIntegerField
      FieldName = 'nCdOrdemProducao'
    end
    object StringField51: TStringField
      FieldName = 'cNumeroOP'
    end
    object StringField52: TStringField
      FieldName = 'cNmTabTipoOrigemOP'
      ReadOnly = True
    end
    object DateTimeField12: TDateTimeField
      FieldName = 'dDtAbertura'
    end
    object DateTimeField13: TDateTimeField
      FieldName = 'dDtInicioPrevisto'
    end
    object DateTimeField14: TDateTimeField
      FieldName = 'dDtPrevConclusao'
    end
    object StringField53: TStringField
      FieldName = 'cNmTipoOP'
      ReadOnly = True
    end
    object IntegerField28: TIntegerField
      FieldName = 'nCdProduto'
    end
    object StringField54: TStringField
      FieldName = 'cReferencia'
      FixedChar = True
      Size = 15
    end
    object StringField55: TStringField
      FieldName = 'cNmProduto'
      ReadOnly = True
      Size = 50
    end
    object BCDField14: TBCDField
      FieldName = 'nQtdePlanejada'
      Precision = 14
      Size = 6
    end
    object IntegerField29: TIntegerField
      FieldName = 'nCdPedido'
    end
    object StringField56: TStringField
      FieldName = 'cNmTerceiro'
      ReadOnly = True
      Size = 30
    end
    object IntegerField30: TIntegerField
      FieldName = 'nQtdeCancelada'
    end
    object IntegerField31: TIntegerField
      FieldName = 'nQtdeRetrabalho'
    end
    object IntegerField32: TIntegerField
      FieldName = 'nQtdeRefugo'
    end
    object BCDField15: TBCDField
      FieldName = 'nSaldoProduzir'
      ReadOnly = True
      Precision = 20
      Size = 6
    end
    object StringField57: TStringField
      FieldName = 'cFlgAtraso'
      ReadOnly = True
      Size = 3
    end
    object IntegerField33: TIntegerField
      FieldName = 'nQtdeConcluida'
    end
    object StringField58: TStringField
      FieldName = 'cNmTabTipoStatusOP'
      Size = 50
    end
  end
  object dsOPAguardaServico: TDataSource
    DataSet = qryOPAguardaServico
    Left = 260
    Top = 221
  end
  object qryPDCAguardAutoriz: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'DECLARE @nCdEmpresa int'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      ''
      'SELECT Pedido.nCdPedido'
      '      ,Pedido.dDtPedido'
      '      ,TipoPedido.cNmTipoPedido'
      '      ,Terceiro.cNmTerceiro'
      '      ,Pedido.nValPedido'
      '  FROM Pedido'
      
        '       INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Pedid' +
        'o.nCdTerceiro'
      
        '       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedid' +
        'o.nCdTipoPedido'
      ' WHERE Pedido.nCdTabStatusPed   = 2'
      '   AND TipoPedido.cFlgCompra    = 1'
      '   AND Pedido.nCdEmpresa        = @nCdEmpresa')
    Left = 112
    Top = 294
    object qryPDCAguardAutoriznCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryPDCAguardAutorizdDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object qryPDCAguardAutorizcNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryPDCAguardAutorizcNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryPDCAguardAutoriznValPedido: TBCDField
      FieldName = 'nValPedido'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsPDCAguardAutoriz: TDataSource
    DataSet = qryPDCAguardAutoriz
    Left = 142
    Top = 293
  end
  object qryPDCAtraso: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'DECLARE @nCdEmpresa int'
      '       ,@dDtInicial datetime'
      '       ,@dDtFinal   datetime'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      ''
      'SELECT ItemPedido.dDtEntregaFim'
      '      ,Pedido.nCdPedido'
      '      ,Pedido.dDtPedido'
      '      ,TipoPedido.cNmTipoPedido'
      '      ,Terceiro.cNmTerceiro'
      '      ,ItemPedido.cCdProduto'
      '      ,ItemPedido.cNmItem'
      
        '      ,(ItemPedido.nQtdePed-ItemPedido.nQtdeExprec-ItemPedido.nQ' +
        'tdeCanc) as nSaldoReceber'
      
        '      ,(ItemPedido.nValCustoUnit * (ItemPedido.nQtdePed-ItemPedi' +
        'do.nQtdeExprec-ItemPedido.nQtdeCanc)) as nValTotalItem'
      '  FROM Pedido'
      
        '       INNER JOIN ItemPedido ON ItemPedido.nCdPedido     = Pedid' +
        'o.nCdPedido'
      
        '       INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Pedid' +
        'o.nCdTerceiro'
      
        '       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedid' +
        'o.nCdTipoPedido'
      ' WHERE Pedido.nCdTabStatusPed   IN (3,4,5)'
      '   AND TipoPedido.cFlgCompra    = 1'
      '   AND Pedido.nCdEmpresa        = @nCdEmpresa'
      '   AND ItemPedido.nCdTipoItemped IN (1,2,5)'
      '   AND ItemPedido.dDtEntregaFim < dbo.fn_OnlyDate(GetDate())'
      
        '   AND (ItemPedido.nQtdePed-ItemPedido.nQtdeExprec-ItemPedido.nQ' +
        'tdeCanc) > 0'
      ' ORDER BY dDtEntregaFim'
      '')
    Left = 112
    Top = 334
    object qryPDCAtrasodDtEntregaFim: TDateTimeField
      FieldName = 'dDtEntregaFim'
    end
    object qryPDCAtrasonCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryPDCAtrasodDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object qryPDCAtrasocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object qryPDCAtrasocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryPDCAtrasocCdProduto: TStringField
      FieldName = 'cCdProduto'
      Size = 15
    end
    object qryPDCAtrasocNmItem: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryPDCAtrasonSaldoReceber: TBCDField
      FieldName = 'nSaldoReceber'
      ReadOnly = True
      Precision = 14
    end
    object qryPDCAtrasonValTotalItem: TBCDField
      FieldName = 'nValTotalItem'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 29
      Size = 10
    end
  end
  object dsPDCAtraso: TDataSource
    DataSet = qryPDCAtraso
    Left = 142
    Top = 333
  end
  object qryPDCProx15D: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SET NOCOUNT ON'
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      ''
      'DECLARE @nCdEmpresa int'
      '       ,@dDtInicial datetime'
      '       ,@dDtFinal   datetime'
      ''
      'Set @nCdEmpresa = :nCdEmpresa'
      '      '
      'SELECT ItemPedido.dDtEntregaFim'
      '      ,Pedido.nCdPedido'
      '      ,Pedido.dDtPedido'
      '      ,TipoPedido.cNmTipoPedido'
      '      ,Terceiro.cNmTerceiro'
      '      ,ItemPedido.nCdProduto'
      '      ,ItemPedido.cNmItem'
      
        '      ,(ItemPedido.nQtdePed-ItemPedido.nQtdeExprec-ItemPedido.nQ' +
        'tdeCanc) as nSaldoReceber'
      
        '      ,(ItemPedido.nValCustoUnit * (ItemPedido.nQtdePed-ItemPedi' +
        'do.nQtdeExprec-ItemPedido.nQtdeCanc)) as nValTotalItem'
      '  FROM Pedido'
      
        '       INNER JOIN ItemPedido ON ItemPedido.nCdPedido     = Pedid' +
        'o.nCdPedido'
      
        '       INNER JOIN Terceiro   ON Terceiro.nCdTerceiro     = Pedid' +
        'o.nCdTerceiro'
      
        '       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedid' +
        'o.nCdTipoPedido'
      ' WHERE Pedido.nCdTabStatusPed   IN (3,4,5)'
      '   AND TipoPedido.cFlgCompra    = 1'
      '   AND Pedido.nCdEmpresa        = @nCdEmpresa'
      '   AND ItemPedido.nCdTipoItemped IN (1,2,5)'
      '   AND ItemPedido.dDtEntregaFim >= dbo.fn_OnlyDate(GetDate())'
      
        '   AND ItemPedido.dDtEntregaFim  < dbo.fn_OnlyDate(GetDate()) + ' +
        '16'
      
        '   AND (ItemPedido.nQtdePed-ItemPedido.nQtdeExprec-ItemPedido.nQ' +
        'tdeCanc) > 0'
      ' ORDER BY dDtEntregaFim'
      '')
    Left = 112
    Top = 374
    object DateTimeField15: TDateTimeField
      FieldName = 'dDtEntregaFim'
    end
    object IntegerField34: TIntegerField
      FieldName = 'nCdPedido'
    end
    object DateTimeField16: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object StringField59: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
    object StringField60: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object StringField62: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object BCDField16: TBCDField
      FieldName = 'nSaldoReceber'
      ReadOnly = True
      Precision = 14
    end
    object BCDField17: TBCDField
      FieldName = 'nValTotalItem'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 29
      Size = 10
    end
    object qryPDCProx15DnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
  end
  object dsPDCProx15D: TDataSource
    DataSet = qryPDCProx15D
    Left = 142
    Top = 373
  end
end
