unit fPedidoDevCompra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StrUtils, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, cxContainer, cxEdit, cxTextEdit,
  DBCtrlsEh, DBLookupEh, Menus, DBGridEhGrouping, fTransfEst_Itens,
  ToolCtrlsEh;

type
  TfrmPedidoDevCompra = class(TfrmCadastro_Padrao)
    qryMasternCdPedido: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    qryMasternCdTabTipoPedido: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdTerceiroColab: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternCdTabStatusPed: TIntegerField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternPercDesconto: TBCDField;
    qryMasternPercAcrescimo: TBCDField;
    qryMasternValProdutos: TBCDField;
    qryMasternValServicos: TBCDField;
    qryMasternValImposto: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMasternValAcrescimo: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMasternValOutros: TBCDField;
    qryMasternValPedido: TBCDField;
    qryMastercNrPedTerceiro: TStringField;
    qryMastercOBS: TMemoField;
    qryMasterdDtAutor: TDateTimeField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMasterdDtRejeicao: TDateTimeField;
    qryMasternCdUsuarioRejeicao: TIntegerField;
    qryMastercMotivoRejeicao: TMemoField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryTipoPedido: TADOQuery;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceiroTransp: TADOQuery;
    qryTerceiroTranspnCdTerceiro: TIntegerField;
    qryTerceiroTranspcCNPJCPF: TStringField;
    qryTerceiroTranspcNmTerceiro: TStringField;
    qryIncoterms: TADOQuery;
    qryIncotermsnCdIncoterms: TIntegerField;
    qryIncotermscSigla: TStringField;
    qryIncotermscNmIncoterms: TStringField;
    qryIncotermscFlgPagador: TStringField;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    qryEstoque: TADOQuery;
    qryEstoquenCdEstoque: TAutoIncField;
    qryEstoquenCdEmpresa: TIntegerField;
    qryEstoquenCdLoja: TIntegerField;
    qryEstoquenCdTipoEstoque: TIntegerField;
    qryEstoquecNmEstoque: TStringField;
    qryEstoquenCdStatus: TIntegerField;
    qryStatusPed: TADOQuery;
    qryStatusPednCdTabStatusPed: TIntegerField;
    qryStatusPedcNmTabStatusPed: TStringField;
    qryMastercNmContato: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    dsEmpresa: TDataSource;
    DBEdit13: TDBEdit;
    dsLoja: TDataSource;
    DBEdit14: TDBEdit;
    dsTipoPedido: TDataSource;
    DBEdit15: TDBEdit;
    dsTerceiro: TDataSource;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    dsStatusPed: TDataSource;
    qryMasternCdEstoqueMov: TIntegerField;
    dsTerceiroTransp: TDataSource;
    dsEstoque: TDataSource;
    Label17: TLabel;
    DBEdit24: TDBEdit;
    DBEdit27: TDBEdit;
    dsCondPagto: TDataSource;
    dsIcoterms: TDataSource;
    cxPageControl1: TcxPageControl;
    TabItemEstoque: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioTipoPedido: TADOQuery;
    qryUsuarioTipoPedidonCdTipoPedido: TIntegerField;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdLoja: TIntegerField;
    qryTerceironCdTerceiroPagador: TIntegerField;
    Label19: TLabel;
    DBEdit30: TDBEdit;
    Label20: TLabel;
    DBEdit31: TDBEdit;
    Label21: TLabel;
    DBEdit32: TDBEdit;
    Label22: TLabel;
    DBEdit33: TDBEdit;
    Label25: TLabel;
    DBEdit36: TDBEdit;
    Label26: TLabel;
    DBEdit37: TDBEdit;
    TabParcela: TcxTabSheet;
    qryItemEstoque: TADOQuery;
    dsItemEstoque: TDataSource;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryTerceironCdTerceiroTransp: TIntegerField;
    qryTerceironCdIncoterms: TIntegerField;
    qryTerceironCdCondPagto: TIntegerField;
    qryTerceironPercDesconto: TBCDField;
    qryTerceironPercAcrescimo: TBCDField;
    usp_Grade: TADOStoredProc;
    dsGrade: TDataSource;
    qryTemp: TADOQuery;
    usp_Gera_SubItem: TADOStoredProc;
    cmdExcluiSubItem: TADOCommand;
    qryAux: TADOQuery;
    DBGridEh3: TDBGridEh;
    qryPrazoPedido: TADOQuery;
    dsPrazoPedido: TDataSource;
    qryPrazoPedidonCdPrazoPedido: TAutoIncField;
    qryPrazoPedidonCdPedido: TIntegerField;
    qryPrazoPedidodVencto: TDateTimeField;
    qryPrazoPedidoiDias: TIntegerField;
    qryPrazoPedidonValPagto: TBCDField;
    usp_Sugere_Parcela: TADOStoredProc;
    Image3: TImage;
    ToolButton3: TToolButton;
    btFinalizarPed: TToolButton;
    qryProdutonCdGrade: TIntegerField;
    usp_Finaliza: TADOStoredProc;
    btImprimirPed: TToolButton;
    qryDadoAutorz: TADOQuery;
    qryDadoAutorzcDadoAutoriz: TStringField;
    DBEdit34: TDBEdit;
    dsDadoAutorz: TDataSource;
    qryMastercFlgCritico: TIntegerField;
    qryMasternSaldoFat: TBCDField;
    usp_gera_item_embalagem: TADOStoredProc;
    qryMastercOBSFinanc: TMemoField;
    qryMasternPercDescontoVencto: TBCDField;
    qryMasternPercAcrescimoVendor: TBCDField;
    usp_copia_cc: TADOStoredProc;
    qryMasternCdMotBloqPed: TIntegerField;
    qryMasternCdTerceiroRepres: TIntegerField;
    btCancelarPed: TToolButton;
    PopupMenu1: TPopupMenu;
    ExibirRecebimentosdoItem1: TMenuItem;
    qryMasternCdUsuarioComprador: TIntegerField;
    qryProdutocUnidadeMedida: TStringField;
    qryProdutonQtdeMinimaCompra: TBCDField;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    DBEdit23: TDBEdit;
    qryLojanCdLocalEstoquePadrao: TIntegerField;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTipoPedidocGerarFinanc: TIntegerField;
    qryTipoPedidocFlgItemEstoque: TIntegerField;
    qryTipoPedidocFlgItemAD: TIntegerField;
    qryLocalEstoqueSaida: TADOQuery;
    qryLocalEstoqueSaidanCdLocalEstoque: TIntegerField;
    qryLocalEstoqueSaidacNmLocalEstoque: TStringField;
    dsLocalEstoqueSaida: TDataSource;
    qryTipoPedidonCdTabTipoPedido: TIntegerField;
    qryTipoPedidocExigeAutor: TIntegerField;
    qryUltRecebItem: TADOQuery;
    qryUltRecebItemnCdItemRecebimento: TAutoIncField;
    qryUltRecebItemnPercIPI: TBCDField;
    qryTrataProduto: TADOQuery;
    tabFaturamento: TcxTabSheet;
    Image2: TImage;
    qryMastercMsgNF1: TStringField;
    qryMastercMsgNF2: TStringField;
    DBEdit2: TDBEdit;
    DBEdit7: TDBEdit;
    Label10: TLabel;
    qryDadosReceb: TADOQuery;
    qryDadosRecebcNrDocto: TStringField;
    cxButton4: TcxButton;
    qryUltRecebItemnValUnitario: TFloatField;
    Label8: TLabel;
    Label2: TLabel;
    Label13: TLabel;
    DBEdit10: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit41: TDBEdit;
    qryTerceiroPagador: TADOQuery;
    qryTerceiroPagadornCdTerceiro: TIntegerField;
    qryTerceiroPagadorcCNPJCPF: TStringField;
    qryTerceiroPagadorcNmTerceiro: TStringField;
    DBEdit8: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit18: TDBEdit;
    dsTerceiroPagador: TDataSource;
    qryLocalEstoqueSaidanCdLoja: TIntegerField;
    qryProdutonValCusto: TBCDField;
    Label11: TLabel;
    DBEdit20: TDBEdit;
    qryGrupoImposto: TADOQuery;
    qryCFOP: TADOQuery;
    qryCFOPnCdCFOP: TIntegerField;
    qryCFOPcCFOP: TStringField;
    qryCFOPcNmCFOP: TStringField;
    qryCFOPnCdTipoICMS: TIntegerField;
    qryCFOPnCdTipoIPI: TIntegerField;
    qryCST_ICMS: TADOQuery;
    qryTipoPedidonCdNaturezaOperacao: TIntegerField;
    qryNaturezaOperacao: TADOQuery;
    qryNaturezaOperacaocCFOPInterno: TStringField;
    qryNaturezaOperacaocCFOPExterno: TStringField;
    qryEndereco: TADOQuery;
    qryRegraICMS: TADOQuery;
    qryUFEmissao: TADOQuery;
    qryEmpresanCdTerceiroEmp: TIntegerField;
    qryUfDestino: TADOQuery;
    qryUFEmissaonCdEndereco: TIntegerField;
    qryUFEmissaonCdTerceiro: TIntegerField;
    qryUFEmissaocUF: TStringField;
    qryUfDestinonCdEndereco: TIntegerField;
    qryUfDestinonCdTerceiro: TIntegerField;
    qryUfDestinocUF: TStringField;
    qryRegraICMSnPercIva: TBCDField;
    qryRegraICMSnPercBCIva: TBCDField;
    qryRegraICMSnAliqICMS: TBCDField;
    qryRegraICMSnAliqICMSInterna: TBCDField;
    qryRegraICMSnCdTipoTributacaoICMS: TStringField;
    qryRegraICMSnPercBC: TBCDField;
    qryItemEstoquenCdItemPedido: TIntegerField;
    qryItemEstoquenCdPedido: TIntegerField;
    qryItemEstoquenCdItemPedidoPai: TIntegerField;
    qryItemEstoquenCdProduto: TIntegerField;
    qryItemEstoquecCdProduto: TStringField;
    qryItemEstoquenCdTipoItemPed: TIntegerField;
    qryItemEstoquenCdEstoqueMov: TIntegerField;
    qryItemEstoquecNmItem: TStringField;
    qryItemEstoquenQtdePed: TFloatField;
    qryItemEstoquenQtdeExpRec: TFloatField;
    qryItemEstoquenQtdeCanc: TFloatField;
    qryItemEstoquenValUnitario: TFloatField;
    qryItemEstoquenPercIPI: TFloatField;
    qryItemEstoquenValIPI: TFloatField;
    qryItemEstoquenValDesconto: TFloatField;
    qryItemEstoquenValCustoUnit: TFloatField;
    qryItemEstoquenValSugVenda: TFloatField;
    qryItemEstoquenValTotalItem: TFloatField;
    qryItemEstoquenValAcrescimo: TFloatField;
    qryItemEstoquecSiglaUnidadeMedida: TStringField;
    qryItemEstoqueiColuna: TIntegerField;
    qryItemEstoquenCdTabStatusItemPed: TIntegerField;
    qryItemEstoquenQtdeLibFat: TFloatField;
    qryItemEstoquenValUnitarioEsp: TFloatField;
    qryItemEstoquenCdGrupoImposto: TIntegerField;
    qryItemEstoquecFlgPromocional: TIntegerField;
    qryItemEstoquenCdTabPreco: TIntegerField;
    qryItemEstoquenCdLoja: TIntegerField;
    qryItemEstoquecFlgTrocado: TIntegerField;
    qryItemEstoquenCdItemPedidoTroca: TIntegerField;
    qryItemEstoquenCdTerceiroColab: TIntegerField;
    qryItemEstoquedDtPedidoItem: TDateTimeField;
    qryItemEstoquenCdVale: TIntegerField;
    qryItemEstoquecDescricaoTecnica: TMemoField;
    qryItemEstoquenDimensao: TFloatField;
    qryItemEstoquenAltura: TFloatField;
    qryItemEstoquenLargura: TFloatField;
    qryItemEstoquedDtEntregaIni: TDateTimeField;
    qryItemEstoquedDtEntregaFim: TDateTimeField;
    qryItemEstoquenQtdePrev: TFloatField;
    qryItemEstoquenPercentCompra: TFloatField;
    qryItemEstoquenPercICMSSub: TFloatField;
    qryItemEstoquenValICMSSub: TFloatField;
    qryItemEstoquecFlgComplemento: TIntegerField;
    qryItemEstoquenPercDescontoItem: TFloatField;
    qryItemEstoquenValDescAbatUnit: TFloatField;
    qryItemEstoquecFlgDefeito: TIntegerField;
    qryItemEstoquenCdItemRecebimentoDev: TIntegerField;
    qryItemEstoquenCdCampanhaPromoc: TIntegerField;
    qryItemEstoquenValCMV: TFloatField;
    qryItemEstoquenCdServidorOrigem: TIntegerField;
    qryItemEstoquedDtReplicacao: TDateTimeField;
    qryItemEstoquenFatorConvUnidadeEstoque: TFloatField;
    qryItemEstoquenCdMotivoCancSaldoPed: TIntegerField;
    qryItemEstoquedDtCancelSaldoItemPed: TDateTimeField;
    qryItemEstoquenCdUsuarioCancelSaldoItemPed: TIntegerField;
    qryItemEstoquenValFrete: TFloatField;
    qryItemEstoquenValSeguro: TFloatField;
    qryItemEstoquenValAcessorias: TFloatField;
    qryItemEstoquenPercRedBaseICMS: TFloatField;
    qryItemEstoquenValBaseICMS: TFloatField;
    qryItemEstoquenPercAliqICMS: TFloatField;
    qryItemEstoquenPercIVA: TFloatField;
    qryItemEstoquenPercAliqICMSInt: TFloatField;
    qryItemEstoquenValBaseICMSSub: TFloatField;
    qryItemEstoquenPercIncFiscal: TFloatField;
    qryItemEstoquecNCM: TStringField;
    qryItemEstoquecCdSTIPI: TStringField;
    qryItemEstoquecCFOPItemPedido: TStringField;
    GroupBox1: TGroupBox;
    cxTextEdit1: TcxTextEdit;
    Label23: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label24: TLabel;
    cxTextEdit3: TcxTextEdit;
    Label29: TLabel;
    btConsultarDocEntrada: TcxButton;
    btLeitura: TcxButton;
    btPrecoEsperado: TcxButton;
    qryItemEstoquenPercBaseCalcIPI: TFloatField;
    qryItemEstoquenValBaseIPI: TFloatField;
    qryUnidadeMedida: TADOQuery;
    qryUnidadeMedidanCdUnidadeMedida: TAutoIncField;
    qryGrupoImpostocNCM: TStringField;
    qryItemEstoquecCSTPIS: TStringField;
    qryItemEstoquenValBasePIS: TFloatField;
    qryItemEstoquenAliqPIS: TFloatField;
    qryItemEstoquenValPIS: TFloatField;
    qryItemEstoquecCSTCOFINS: TStringField;
    qryItemEstoquenValBaseCOFINS: TFloatField;
    qryItemEstoquenAliqCOFINS: TFloatField;
    qryItemEstoquenValCOFINS: TFloatField;
    qryCST_IPI: TADOQuery;
    qryCST_IPInCdTipoTributacaoIPI: TIntegerField;
    qryCST_IPIcCdStIPI: TStringField;
    qryCST_IPIcNmTipoTributacaoIPI: TStringField;
    qryCST_PISCOFINS: TADOQuery;
    qryCST_PISCOFINSnCdTipoTributacaoPISCOFINS: TIntegerField;
    qryCST_PISCOFINScCdStPISCOFINS: TStringField;
    qryCST_PISCOFINScNmTipoTributacaoPISCOFINS: TStringField;
    DBEdit21: TDBEdit;
    Label27: TLabel;
    qryEnderecoEntrega: TADOQuery;
    qryEnderecoEntreganCdEndereco: TAutoIncField;
    qryEnderecoEntregacNmstatus: TStringField;
    qryEnderecoEntregacNmTipoEnd: TStringField;
    qryEnderecoEntregacEnderecoCompleto: TStringField;
    DBEdit22: TDBEdit;
    qryMastercAtuCredito: TIntegerField;
    qryMastercCreditoLibMan: TIntegerField;
    qryMasterdDtIniProducao: TDateTimeField;
    qryMasternValDevoluc: TBCDField;
    qryMasternValValePres: TBCDField;
    qryMasternValValeMerc: TBCDField;
    qryMasterdDtContab: TDateTimeField;
    qryMasternCdLanctoFin: TIntegerField;
    qryMasternCdMapaCompra: TIntegerField;
    qryMastercFlgIntegrado: TIntegerField;
    qryMastercAnotacao: TMemoField;
    qryMastercFlgOrcamento: TIntegerField;
    qryMastercOrcamentoFin: TIntegerField;
    qryMasternCdPedidoOrcamento: TIntegerField;
    qryMasternValAdiantamento: TBCDField;
    qryMastercFlgAdiantConfirm: TIntegerField;
    qryMasternCdUsuarioConfirmAdiant: TIntegerField;
    qryMasternValAdiantUsado: TBCDField;
    qryMastercFlgDevPag: TIntegerField;
    qryMastercFlgMovEstoque: TIntegerField;
    qryMasternCdGrupoEconomico: TIntegerField;
    qryMasterdDtPrevEntIniOriginal: TDateTimeField;
    qryMasterdDtPrevEntFimOriginal: TDateTimeField;
    qryMastercFlgLibParcial: TIntegerField;
    qryMasterdDtIntegracao: TDateTimeField;
    qryMasternValDescontoCondPagto: TBCDField;
    qryMasternValJurosCondPagto: TBCDField;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMastercFlgReplicado: TIntegerField;
    qryMasternCdEnderecoEntrega: TIntegerField;
    qryMasternCdAreaVenda: TIntegerField;
    qryMasternCdDivisaoVenda: TIntegerField;
    qryMasternCdCanalDistribuicao: TIntegerField;
    qrySugereEnderecoEntrega: TADOQuery;
    qrySugereEnderecoEntreganCdEndereco: TIntegerField;
    qryItemEstoquecCdSTICMS: TStringField;
    qryTerceirocFlgOptSimples: TIntegerField;
    dsEnderecoEntrega: TDataSource;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    btSugParcela: TcxButton;
    qryGrupoPedido: TADOQuery;
    qryGrupoPedidocFlgFaturar: TIntegerField;
    qryTipoPedidonCdGrupoPedido: TIntegerField;
    Label12: TLabel;
    qryItemEstoquecDoctoCompra: TStringField;
    DBEdit25: TDBEdit;
    Label14: TLabel;
    qryItemEstoquenValBaseICMSSubRet: TFloatField;
    qryItemEstoquenValICMSSubRet: TFloatField;
    qryItemEstoquecInfoAdicionalProd: TStringField;
    qryCST_ICMSnCdTipoTributacaoICMS: TAutoIncField;
    qryCST_ICMScCdST: TStringField;
    qryCST_ICMScNmTipoTributacaoICMS: TStringField;
    qrySaldoItemDevoluc: TADOQuery;
    qrySaldoItemDevolucnQtdePed: TBCDField;
    qryPrazoPedidonValDif: TBCDField;
    qryPrazoPedidoDif: TADOQuery;
    qryItemEstoquenValICMS: TFloatField;
    qryTabTipoModFrete: TADOQuery;
    qryTabTipoModFretenCdTabTipoModFrete: TIntegerField;
    qryTabTipoModFretecNmTabTipoModFrete: TStringField;
    dsTabTipoModFrete: TDataSource;
    qryMasternCdTabTipoModFrete: TIntegerField;
    Label15: TLabel;
    DBEdit29: TDBEdit;
    DBEdit28: TDBEdit;
    qryLojanCdTerceiro: TIntegerField;
    qryTerceiroEmit: TADOQuery;
    qryTerceiroEmitnCdTerceiro: TIntegerField;
    qryTerceiroEmitcNmTerceiro: TStringField;
    qryTerceiroEmitnCdTabTipoEnquadTributario: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit25KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit24KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit38KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit24Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryItemEstoqueBeforePost(DataSet: TDataSet);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit19KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueBeforeDelete(DataSet: TDataSet);
    procedure qryItemEstoqueAfterDelete(DataSet: TDataSet);
    procedure qryPrazoPedidoBeforePost(DataSet: TDataSet);
    procedure btSugParcelaClick(Sender: TObject);
    procedure btFinalizarPedClick(Sender: TObject);
    procedure qryTempAfterCancel(DataSet: TDataSet);
    procedure qryItemADAfterDelete(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh4Enter(Sender: TObject);
    procedure btImprimirPedClick(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
    procedure qryItemFormulaAfterDelete(DataSet: TDataSet);
    procedure btPrecoEsperadoClick(Sender: TObject);
    procedure btCancelarPedClick(Sender: TObject);
    procedure ExibirRecebimentosdoItem1Click(Sender: TObject);
    procedure cxButton6Click(Sender: TObject);
    procedure DBEdit9Enter(Sender: TObject);
    procedure DBEdit9Exit(Sender: TObject);
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btLeituraClick(Sender: TObject);
    procedure TrataProdutos();
    procedure btConsultarDocEntradaClick(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure DBEdit41KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit41Exit(Sender: TObject);
    procedure DBEdit19Exit(Sender: TObject);
    procedure DBEdit10KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit10Exit(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure CompletaDadosFaturamento();
    function RightStr(Const Str: String; Size: Word): String;
    procedure qryItemEstoquecCdProdutoChange(Sender: TField);
    procedure verificaControlesICMS(cCST: string);
    procedure qryItemEstoqueAfterScroll(DataSet: TDataSet);
    procedure qryItemEstoquecCdSTICMSChange(Sender: TField);
    procedure calculaImpostosItem( bGravar : boolean ) ;
    procedure ativaEdicaoProduto( bPermitirEditar : boolean ) ;
    procedure DBEdit21Enter(Sender: TObject);
    procedure DBEdit21KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit21Exit(Sender: TObject);
    procedure qryTipoPedidoAfterOpen(DataSet: TDataSet);
    procedure DBGridEh1ColEnter(Sender: TObject);
    procedure DBEdit29Exit(Sender: TObject);
    procedure DBEdit29KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    objForm : TfrmTransfEst_Itens;
    { Private declarations }

  public
    { Public declarations }
    bFaturaDev : Boolean;
  end;

var
  frmPedidoDevCompra: TfrmPedidoDevCompra;

implementation

uses Math,fMenu, fLookup_Padrao, rPedidoCom_Simples, fEmbFormula,
  fPrecoEspPedCom, fItemPedidoCompraAtendido, fPedidoComercial_ProgEntrega,
  rPedCom_Contrato, fItemPedidoAtendido,
  rPedidoDevCompra, fPedidoDevCompra_ConsultaItens;

{$R *.dfm}


procedure TfrmPedidoDevCompra.GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
var
  X:integer;
  dr,dg,db:Extended;
  C1,C2:TColor;
  r1,r2,g1,g2,b1,b2:Byte;
  R,G,B:Byte;
  cnt:integer;
begin
  C1 := FromColor;
  R1 := GetRValue(C1) ;
  G1 := GetGValue(C1) ;
  B1 := GetBValue(C1) ;

  C2 := ToColor;
  R2 := GetRValue(C2) ;
  G2 := GetGValue(C2) ;
  B2 := GetBValue(C2) ;

  dr := (R2-R1) / Rect.Right-Rect.Left;
  dg := (G2-G1) / Rect.Right-Rect.Left;
  db := (B2-B1) / Rect.Right-Rect.Left;

  cnt := 0;
  for X := Rect.Left to Rect.Right-1 do
  begin
    R := R1+Ceil(dr*cnt) ;
    G := G1+Ceil(dg*cnt) ;
    B := B1+Ceil(db*cnt) ;

    Canvas.Pen.Color := RGB(R,G,B) ;
    Canvas.MoveTo(X,Rect.Top) ;
    Canvas.LineTo(X,Rect.Bottom) ;
    inc(cnt) ;
  end;
end;

procedure TfrmPedidoDevCompra.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PEDIDO' ;
  nCdTabelaSistema  := 30 ;
  nCdConsultaPadrao := 164 ;
  bLimpaAposSalvar  := False ;

  if frmMenu.LeParametro('USAPRECOESP') = 'N' then
  begin
      btPrecoEsperado.Visible      := true;
      btLeitura.Left               := 3;
      btConsultarDocEntrada.Left   := 163;
      DBGridEh3.Columns[5].Visible := false;
      DBGridEh3.Width              := 300;
  end;

end;

procedure TfrmPedidoDevCompra.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit1.SetFocus;
  cxPageControl1.ActivePageIndex := 0;

  if not qryEmpresa.Active then
  begin
      qryEmpresa.Close ;
      qryEmpresa.Parameters.ParamByName('nPK').Value := frmMenu.nCdEmpresaAtiva ;
      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryEmpresa.Open ;

      if not qryEmpresa.eof then
      begin
          qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;

          PosicionaQuery(qryTerceiroEmit, qryEmpresanCdTerceiroEmp.AsString);
      end ;
  end ;

  qryMasterdDtPedido.Value := Date;

  qryUsuarioTipoPedido.Close ;
  qryUsuarioTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryUsuarioTipoPedido.Open ;

  if (qryUsuarioTipoPedido.eof) then
  begin
      ShowMessage('Nenhum tipo de pedido comercial vinculado para este usu�rio.') ;
      btCancelar.Click;
      abort ;
  end ;

  if (qryUsuarioTipoPedido.RecordCount = 1) then
  begin
      PosicionaQuery(qryTipoPedido,qryUsuarioTipoPedidonCdTipoPedido.AsString) ;
      qryMasternCdTipoPedido.Value := qryUsuarioTipoPedidonCdTipoPedido.Value ;
  end ;

  qryUsuarioTipoPedido.Close ;


  If (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      qryUsuarioLoja.Close ;
      qryUsuarioLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryUsuarioLoja.Open ;

      if (qryUsuarioLoja.eof) then
      begin
          ShowMessage('Nenhuma loja vinculada para este usu�rio.') ;
          btCancelar.Click;
          abort ;
      end ;

      if (qryUsuarioLoja.RecordCount = 1) then
      begin
          PosicionaQuery(qryLoja,qryUsuarioLojanCdLoja.AsString) ;
          qryMasternCdLoja.Value := qryUsuarioLojanCdLoja.Value ;
      end ;

      qryUsuarioLoja.Close ;
      DBEdit3.Enabled := True ;

  end
  else begin
      DBEdit3.Enabled := False ;
  end ;

  if (qryMasternCdTipoPedido.Value = 0) then
      DbEdit4.SetFocus
  Else begin
      DbEdit4.OnExit(nil) ;
      DBEdit5.SetFocus ;
  end ;

  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryEstoque.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value ;

  DBGridEh1.ReadOnly   := False ;
  DBGridEh3.ReadOnly   := False ;
  btSugParcela.Enabled := True ;

  if (DbEdit3.Text = '') and DbEdit3.Enabled then
      DBEdit3.SetFocus ;

end;

procedure TfrmPedidoDevCompra.btCancelarClick(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTipoPedido.Close ;
  qryTabTipoModFrete.Close;
end;

procedure TfrmPedidoDevCompra.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(163);

            If (nPK > 0) then
            begin
                qryMasternCdTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (Trim(DbEdit4.Text) = '') then
            begin
                ShowMessage('Informe o Tipo de Pedido.') ;
                DBEdit4.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(17,'EXISTS(SELECT 1 FROM TerceiroTipoTerceiro TTT  INNER JOIN TipoPedidoTipoTerceiro TTTP ON TTTP.nCdTipoTerceiro = TTT.nCdTipoTerceiro AND TTTp.nCdTipoPedido = ' + DbEdit4.Text + ' WHERE TTT.nCdTerceiro = vTerceiros.nCdTerceiro)');

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit20KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit23KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit25KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit24KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(61,'cFlgPdv = 0');

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if not qryTipoPedido.Active then
  begin
      ShowMessage('Informe o Tipo do Pedido.') ;
      DbEdit4.SetFocus ;
      exit ;
  end ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, dbEdit5.Text) ;

  if not qryTerceiro.active then
      exit ;

  if (qryMaster.State = dsInsert) then
  begin
      if (qryTerceironCdTerceiroTransp.Value > 0) then
      begin
          qryMasternCdTerceiroTransp.Value := qryTerceironCdTerceiroTransp.Value ;
          PosicionaQuery(qryTerceiroTransp,qryTerceironCdTerceiroTransp.asString) ;
      end ;

      if (qryTerceironCdIncoterms.Value > 0) then
      begin
          qryMasternCdIncoterms.Value := qryTerceironCdIncoterms.Value ;
          PosicionaQuery(qryIncoterms,qryTerceironCdIncoterms.asString) ;
      end ;

      if (qryTerceironCdCondPagto.Value > 0) then
      begin
          qryMasternCdCondPagto.Value := qryTerceironCdCondPagto.Value ;
          PosicionaQuery(qryCondPagto, qryTerceironCdCondPagto.asString) ;
      end ;

      if (qryTerceironCdTerceiroPagador.Value > 0) then
      begin
          qryMasternCdTerceiroPagador.Value := qryTerceironCdTerceiroPagador.Value ;
          PosicionaQuery(qryTerceiroPagador, qryTerceironCdTerceiroPagador.asString) ;
      end
      else
      begin
          qryMasternCdTerceiroPagador.Value := qryTerceironCdTerceiro.Value ;
          PosicionaQuery(qryTerceiroPagador, qryTerceironCdTerceiro.asString) ;
      end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit38KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(62);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroColab.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmPedidoDevCompra.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTipoPedido, DBEdit4.Text) ;

  //TabItemEstoque.TabVisible := True ;

  if not qryTipoPedido.eof then
  begin
    {if (qryTipoPedidocFlgItemEstoque.Value = 1) then
    begin
      TabItemEstoque.TabVisible := True ;
      TabItemEstoque.Enabled    := True ;
    end ;}

    if (qryTipoPedidocGerarFinanc.Value = 1) then
    begin
      TabParcela.Enabled     := True ;
    end ;

  end ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmPedidoDevCompra.DBEdit3Exit(Sender: TObject);
begin
  inherited;
  qryLoja.Close ;
  PosicionaQuery(qryLoja, DBEdit3.Text) ;

  if not qryLoja.eof then
  begin
      qryEstoque.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value ;
      PosicionaQuery(qryTerceiroEmit, qryLojanCdTerceiro.AsString);
  end;

end;

procedure TfrmPedidoDevCompra.DBEdit24Exit(Sender: TObject);
begin
  inherited;
  qryCondPagto.Close ;
  PosicionaQuery(qryCondPagto, DBEdit24.Text) ;
end;

procedure TfrmPedidoDevCompra.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  DBGridEh1.ReadOnly   := False ;
  DBGridEh3.ReadOnly   := False ;

  btSugParcela.Enabled := True ;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
      ShowMessage('Este pedido n�o pertence a esta empresa.') ;
      btCancelar.Click;
      exit ;
  end ;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;
  PosicionaQuery(qryTipoPedido, qryMasternCdTipoPedido.asString) ;
  PosicionaQuery(qryTerceiroEmit, qryEmpresanCdTerceiroEmp.AsString);

  if (qryTipoPedido.eof) then
  begin
      ShowMessage('Voc� n�o tem autoriza��o para visualizar este tipo de pedido.') ;
      btCancelar.Click;
      exit;
  end ;

  PosicionaQuery(qryLoja, qryMasternCdLoja.asString) ;

  if not qryLoja.Eof then
  begin
      qryEstoque.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value ;
      PosicionaQuery(qryTerceiroEmit, qryLojanCdTerceiro.AsString);
  end;

  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;
  PosicionaQuery(qryEstoque, qryMasternCdEstoqueMov.asString) ;
  PosicionaQuery(qryCondPagto, qryMasternCdCondPagto.asString) ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;
  PosicionaQuery(qryTerceiroTransp,qryMasternCdTerceiroTransp.asString) ;
  PosicionaQuery(qryIncoterms,qryMasternCdIncoterms.asString) ;
  PosicionaQuery(qryTerceiroPagador, qryMasternCdTerceiroPagador.asString) ;

  PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;
  {PosicionaQuery(qryItemAD,qryMasternCdPedido.asString) ;}
  PosicionaQuery(qryDadoAutorz,qryMasternCdPedido.asString) ;

  qryLocalEstoqueSaida.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;

  PosicionaQuery(qryLocalEstoqueSaida, qryMasternCdEstoqueMov.AsString) ;

  PosicionaQuery(qryTabTipoModFrete, qryMasternCdTabTipoModFrete.AsString) ;

  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      DBGridEh1.ReadOnly   := True ;
      DBGridEh3.ReadOnly   := True ;
      btSugParcela.Enabled := False ;
  end ;

  //TabItemEstoque.Enabled := False ;
  //TabItemEstoque.TabStop  := False ;
  TabParcela.TabStop      := False ;

  {if (qryTipoPedidocFlgItemEstoque.Value = 1) then
  begin
      TabItemEstoque.TabVisible := True ;
      TabItemEstoque.Enabled    := True ;
  end ;}

  if (qryTipoPedidocGerarFinanc.Value = 1) then
  begin
      TabParcela.Enabled     := True ;
  end ;

  qryEnderecoEntrega.Close;
  qryEnderecoEntrega.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  PosicionaQuery(qryEnderecoEntrega, qryMasternCdEnderecoEntrega.AsString) ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmPedidoDevCompra.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTipoPedido.Close ;
  qryLoja.Close ;
  qryEstoque.Close ;
  qryCondPagto.Close ;
  qryTerceiro.Close ;
  qryTerceiroEmit.Close;
  qryItemEstoque.Close ;
  qryStatusPed.Close ;
  qryPrazoPedido.Close ;
  qryDadoAutorz.Close ;

  qryLocalEstoqueSaida.Close ;
  qryEnderecoEntrega.Close;

  TabParcela.Enabled     := False ;
  //TabItemEstoque.Enabled := False ;

  cxPageControl1.ActivePageIndex := 0 ;

  //TabItemEstoque.TabStop  := False ;
  TabParcela.TabStop      := False ;

end;

procedure TfrmPedidoDevCompra.qryItemEstoqueBeforePost(DataSet: TDataSet);
var
  iOrigem : Integer;
begin
  if (qryTipoPedidocFlgItemEstoque.Value <> 1) then
  begin
      MensagemAlerta('Tipo de pedido n�o permite itens em estoque.');
      Abort;
  end;

  qryItemEstoquecCdProduto.Value          := Uppercase( trim( qryItemEstoquecCdProduto.Value ) ) ;
  qryItemEstoquecSiglaUnidadeMedida.Value := Uppercase( trim( qryItemEstoquecSiglaUnidadeMedida.Value ) ) ;

  if ( trim( qryItemEstoquecCdProduto.Value ) = '' ) then
  begin
      MensagemAlerta('Informe o c�digo do produto. Informe "AD" para itens n�o cadastrados.') ;
      abort;
  end ;

  if (qryItemEstoquecCdProduto.Value <> 'AD') then
  begin

      try
          strToInt( qryItemEstoquecCdProduto.Value ) ;
      except
          MensagemAlerta('C�digo de produto inv�lido. Informe "AD" para itens n�o cadastrados.') ;
          abort ;
      end ;

      qryProduto.Close;
      qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryMasternCdTipoPedido.Value;
      PosicionaQuery( qryProduto , qryItemEstoquecCdProduto.Value ) ;

      if ( qryProduto.eof ) then
      begin
          MensagemAlerta('C�digo de produto n�o cadastrado ou sem permiss�o de uso neste tipo de pedido.') ;
          abort ;
      end ;

      if (qryItemEstoque.State = dsInsert) then
      begin
          // Valida a duplicidade de itens no pedido
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Append('SELECT 1');
          qryAux.SQL.Append('  FROM ItemPedido');
          qryAux.SQL.Append(' WHERE nCdPedido  = ' + qryMasternCdPedido.AsString);
          qryAux.SQL.Append('   AND nCdProduto = ' + qryProdutonCdProduto.AsString);
          qryAux.Open;

          if not (qryAux.IsEmpty) then
          begin
              MensagemAlerta('O Produto ' + qryProdutocNmProduto.Value + ' j� consta como item deste pedido.');
              Abort;
          end;
      end;

  end ;

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o do produto.') ;
      abort ;
  end ;

  if (qryItemEstoquecSiglaUnidadeMedida.Value = '') then
  begin
      MensagemAlerta('Informe a unidade de medida.') ;
      abort ;
  end ;

  qryUnidadeMedida.Close;
  PosicionaQuery( qryUnidadeMedida , qryItemEstoquecSiglaUnidadeMedida.Value ) ;

  if (qryUnidadeMedida.eof) then
  begin
      MensagemAlerta('Unidade de Medida n�o cadastrada.') ;
      abort ;
  end ;

  if (qryItemEstoquenQtdePed.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade devolvida.') ;
      abort ;
  end ;

  iOrigem := StrToIntDef(LeftStr(qryItemEstoquecCdSTICMS.Value, 1),0);

  if ((iOrigem < 0) or (iOrigem > 8)) then
  begin
      MensagemAlerta('Origem da mercadoria inv�lida.' + #13 + 'Informe o c�digo da origem entre 0 e 8.');
      Abort;
  end;

  { -- se terceiro � optante do simples nacional valida CSOSN de 4 d�gitos -- }
  if (qryTerceiroEmitnCdTabTipoEnquadTributario.Value = 1) then
  begin
      if (Length(Trim(qryItemEstoquecCdSTICMS.Value)) < 4) then
      begin
          MensagemAlerta('O Terceiro ' + qryTerceiroEmitnCdTerceiro.AsString + ' - ' + qryTerceiroEmitcNmTerceiro.Value + ' � optante do Simples Nacional. Informe o CSOSN de 4 d�gitos v�lido.');
          Abort;
      end;

      qryCST_ICMS.Close;
      PosicionaQuery(qryCST_ICMS, RightStr(qryItemEstoquecCdStICMS.Value,3));

      if (qryCST_ICMS.Eof) then
      begin
          MensagemAlerta('CSOSN do ICMS inv�lido.');
          Abort;
      end;
  end
  { -- se terceiro n�o � optante do simples nacional valida CST de 3 d�gitos -- }
  else
  begin
      if (Length(Trim(qryItemEstoquecCdSTICMS.Value)) > 3) then
      begin
          MensagemAlerta('O Terceiro ' + qryTerceiroEmitnCdTerceiro.AsString + ' - ' + qryTerceiroEmitcNmTerceiro.Value + ' n�o � optante do Simples Nacional. Informe o CST de 3 d�gitos v�lido.');
          Abort;
      end;

      qryCST_ICMS.Close;
      PosicionaQuery(qryCST_ICMS, RightStr(qryItemEstoquecCdStICMS.Value,2));

      if (qryCST_ICMS.Eof) then
      begin
          MensagemAlerta('CST do ICMS inv�lido.');
          Abort;
      end;
  end;

  if (qryItemEstoquenValUnitario.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor unit�rio do produto.') ;
      abort ;
  end ;

  if (qryItemEstoquenPercIPI.Value < 0) then
  begin
      MensagemAlerta('Al�quota de IPI inv�lida.') ;
      abort ;
  end ;

  if (qryItemEstoquecDoctoCompra.IsNull <> True) and (qryItemEstoquecDoctoCompra.Value <> '') then
  begin
      qrySaldoItemDevoluc.Close;
      qrySaldoItemDevoluc.Parameters.ParamByName('nCdTipoPedido').Value := qryMasternCdTipoPedido.Value;
      qrySaldoItemDevoluc.Parameters.ParamByName('cDoctoCompra').Value  := Trim(qryItemEstoquecDoctoCompra.Value);
      qrySaldoItemDevoluc.Parameters.ParamByName('nCdPedido').Value     := qryMasternCdPedido.Value;
      qrySaldoItemDevoluc.Parameters.ParamByName('nCdProduto').Value    := qryItemEstoquenCdProduto.Value;
      qrySaldoItemDevoluc.Open;

      if qrySaldoItemDevolucnQtdePed.Value > 0 then
      begin
          case MessageDlg('Item selecionado j� foi devolvido anteriormente, deseja continuar ?' + #13#13 + 'Total Dev.: ' + qrySaldoItemDevolucnQtdePed.AsString, mtConfirmation,[mbYes,mbNo],0) of
              mrNo:Abort ;
          end ;
      end;
  end;

  if ( bFaturaDev ) then
  begin

      if (length(Trim(qryItemEstoquecCdSTICMS.Value)) < 3) then
      begin
          MensagemAlerta('Informe o C�digo CST/CSOSN do item. (Origem da Mercadoria e C�digo Cst) Ex. 000.') ;
          abort ;
      end;

      if (Trim(qryItemEstoquecCFOPItemPedido.Value) = '') then
      begin
          MensagemAlerta('Informe o C�digo CFOP de devolu��o.') ;
          abort ;
      end;

      if     (length(Trim(qryItemEstoquecNCM.Value)) <> 2)
         and (length(Trim(qryItemEstoquecNCM.Value)) <> 8) then
      begin
          MensagemAlerta('NCM n�o informado ou inv�lido.') ;
          abort ;
      end;

      { -- valida NCM -- }
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM TabIBPT WHERE cNCM = ' + #39 + qryItemEstoquecNCM.Value + #39);
      qryAux.Open;

      if (qryAux.IsEmpty) then
      begin
          MensagemAlerta('NCM ' + qryItemEstoquecNCM.Value + ' n�o cadastrado.');
          DBGridEh1.SelectedField := qryItemEstoquecNCM;
          DBGridEh1.SetFocus;
          Abort;
      end;

      PosicionaQuery(qryCFOP,qryItemEstoquecCFOPItemPedido.AsString);

      if qryCFOP.Eof then
      begin
          MensagemAlerta('CFOP n�o cadastrado.');
          abort;
      end;

      // Verifica tipo de cfop
      if (strToInt(Copy(qryItemEstoquecCFOPItemPedido.Value,1,1)) < 5) then
      begin
          MensagemAlerta('O CFOP informado � um CFOP de entrada. Informe um CFOP de saida.');
          abort;
      end;

  end ;

  qryItemEstoquenCdPedido.Value      := qryMasternCdPedido.Value ;
  qryItemEstoquenCdTipoItemPed.Value := 2 ;
  qryItemEstoquecCdProduto.Value     := Uppercase( trim( qryItemEstoquecCdProduto.Value ) ) ;

  if (qryItemEstoquecCdProduto.Value = 'AD') then
      qryItemEstoquenCdTipoItemPed.Value := 5 ;

  inherited;

  {-- chama a rotina que calcula os impostos do item --}
  if ( bFaturaDev ) then
      calculaImpostosItem( TRUE );

  if (qryItemEstoquenValFrete.IsNull) then
      qryItemEstoquenValFrete.Value := 0;

  { -- custo do item n�o contabiliza IPI e ST pois os mesmos s�o contabilizados no total do pedido/nota fiscal -- }
  { -- impostos como ICMS, PIS e COFINS j� est�o embutidos no valor unit�rio do item                           -- }
  qryItemEstoquecNmItem.Value       := Uppercase(qryItemEstoquecNmItem.Value);
  qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - (qryItemEstoquenValUnitario.Value * (qryItemEstoquenPercDescontoItem.Value / 100))); //+ qryItemEstoquenValIPI.Value + qryItemEstoquenValICMSSub.Value;
  qryItemEstoquenValCustoUnit.Value := frmMenu.TBRound(qryItemEstoquenValCustoUnit.Value,4);
  qryItemEstoquenValTotalItem.Value := qryItemEstoquenValCustoUnit.Value * qryItemEstoquenQtdePed.Value;

  if (qryItemEstoque.State = dsInsert) then
      qryItemEstoquenCdItemPedido.Value := frmMenu.fnProximoCodigo('ITEMPEDIDO');

  if (qryItemEstoque.State = dsEdit) then
  begin
      //qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloat(qryItemEstoquenValTotalItem.OldValue) ;
      //qryMasternValImposto.Value  := qryMasternValImposto.Value  - StrToFloat(qryItemEstoquenValIPI.OldValue) - StrToFloat(qryItemEstoquenValICMSSub.OldValue);

      qryMasternValProdutos.Value := qryMasternValProdutos.Value - (StrToFloatDef(qryItemEstoquenValUnitario.OldValue, 0) * StrToFloatDef(qryItemEstoquenQtdePed.OldValue, 0));
      qryMasternValFrete.Value    := qryMasternValFrete.Value    - (StrToFloatDef(qryItemEstoquenValFrete.OldValue, 0)    * StrToFloatDef(qryItemEstoquenQtdePed.OldValue, 0));
      qryMasternValDesconto.Value := qryMasternValDesconto.Value - (StrToFloatDef(qryItemEstoquenValDesconto.OldValue, 0) * StrToFloatDef(qryItemEstoquenQtdePed.OldValue, 0));
      qryMasternValImposto.Value  := qryMasternValImposto.Value  - (StrToFloatDef(qryItemEstoquenValIPI.OldValue, 0)      + StrToFloatDef(qryItemEstoquenValICMSSub.OldValue, 0));
      qryMasternValOutros.Value   := qryMasternValOutros.Value   - (StrToFloatDef(qryItemEstoquenValSeguro.OldValue, 0)   + StrToFloatDef(qryItemEstoquenValAcessorias.OldValue, 0));
  end;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value + (qryItemEstoquenValUnitario.Value * qryItemEstoquenQtdePed.Value);
  qryMasternValFrete.Value    := qryMasternValFrete.Value    + (qryItemEstoquenValFrete.Value    * qryItemEstoquenQtdePed.Value);
  qryMasternValDesconto.Value := qryMasternValDesconto.Value + (qryItemEstoquenValDesconto.Value * qryItemEstoquenQtdePed.Value);
  qryMasternValImposto.Value  := qryMasternValImposto.Value  + (qryItemEstoquenValIPI.Value      + qryItemEstoquenValICMSSub.Value);
  qryMasternValOutros.Value   := qryMasternValOutros.Value   + (qryItemEstoquenValSeguro.Value   + qryItemEstoquenValAcessorias.Value);
end;

procedure TfrmPedidoDevCompra.DBGridEh1ColExit(Sender: TObject);
var
  i : integer;
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMasternCdTabStatusPed.Value <> 1) then
      Exit;

  if (DBGridEh1.Col = 3) then
  begin
     if (qryItemEstoque.State = dsBrowse) then
         qryItemEstoque.Edit;

     completaDadosFaturamento();

  end;

  if (qryItemEstoque.State in ([dsInsert,dsEdit])) then
  begin

      if (DBGridEh1.Col = 1) then
      begin
          ativaEdicaoProduto( (Uppercase( qryItemEstoquecCdProduto.Value ) = 'AD') ) ;

          if ( Uppercase( qryItemEstoquecCdProduto.Value ) = 'AD' ) then
              DBGridEh1.Col := 2 ;

      end ;

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cDoctoCompra') then
      begin
          qryItemEstoquecDoctoCompra.Value := StringReplace(qryItemEstoquecDoctoCompra.Value,' ','',[rfReplaceAll]);
      end;

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nPercDescontoItem') then
          if (qryItemEstoquenPercDescontoItem.Value > 0) then
              qryItemEstoquenValDesconto.Value := frmMenu.TBRound(((qryItemEstoquenValUnitario.Value * (qryItemEstoquenPercDescontoItem.Value/100)){* qryItemEstoquenQtdePed.Value}),2) ;

      if (dbGridEh1.Col = 3) and (qryItemEstoque.State = dsInsert) and (qryItemEstoquenValUnitario.Value = 0) then
      begin

          qryUltRecebItem.Close ;
          qryUltRecebItem.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
          qryUltRecebItem.Parameters.ParamByName('nCdProduto').Value  := qryItemEstoquenCdProduto.Value ;
          qryUltRecebItem.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
          qryUltRecebItem.Open ;

          if not qryUltRecebItem.Eof then
              qryItemEstoquenValUnitario.Value := qryultRecebItemnValUnitario.Value ;

          qryUltRecebItem.Close;

      end ;

      if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cCFOPItemPedido') then
      begin

          if (qryItemEstoquecCFOPItemPedido.Value <> '') then
          begin

              PosicionaQuery( qryCFOP , qryItemEstoquecCFOPItemPedido.Value );

              if (qryCFOP.eof) then
              begin

                  MensagemAlerta('CFOP inv�lido ou n�o cadastrado.') ;
                  abort ;

              end ;

              if     ( Copy( qryItemEstoquecCFOPItemPedido.Value , 1 , 1 ) <> '5' )
                 and ( Copy( qryItemEstoquecCFOPItemPedido.Value , 1 , 1 ) <> '6' ) then
              begin
                  MensagemAlerta('Informe um CFOP de sa�da.') ;
                  abort;
              end ;

          end ;

      end;

      {-- chama a rotina que calcula os impostos do item --}
      calculaImpostosItem( FALSE );

  end;

end;

procedure TfrmPedidoDevCompra.FormShow(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado ;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva ;
  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  bFaturaDev := False;

  If (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      qryUsuarioLoja.Close ;
      qryUsuarioLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
      qryUsuarioLoja.Open ;

      if (qryUsuarioLoja.eof) then
      begin
          ShowMessage('Nenhuma loja vinculada para este usu�rio.') ;
          btCancelar.Click;
          abort ;
      end ;

      qryUsuarioLoja.Close ;
      DBEdit3.Enabled := True ;

  end
  else begin
      desativaDBEdit(DBEdit3);
      DBEdit3.Enabled := False;
  end ;

  //TabItemEstoque.TabStop  := False ;
  TabParcela.TabStop      := False ;

  {-- formata a casa da quantidade sem decimal --}
  DBGridEh1.Columns[DBGridEh1.FieldColumns['nQtdePed'].Index].DisplayFormat := ',0' ;
  
  qryItemEstoquenValUnitario.DisplayFormat  := frmMenu.cMascaraCompras;
  qryItemEstoquenValCustoUnit.DisplayFormat := frmMenu.cMascaraCompras;

  DBGridEh1.Columns[DBGridEh1.FieldColumns['nValUnitario'].Index].DisplayFormat := frmMenu.cMascaraCompras;

  

end;

procedure TfrmPedidoDevCompra.DBEdit19KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;


end;

procedure TfrmPedidoDevCompra.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  PosicionaQuery(qryTabTipoModFrete, qryMasternCdTabTipoModFrete.asString) ; // modo de frete

  if not qryItemEstoque.Active then
      PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;

end;

procedure TfrmPedidoDevCompra.qryItemEstoqueAfterPost(DataSet: TDataSet);
begin
  usp_Gera_SubItem.Close ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdPedido').Value     := qryMasternCdPedido.Value ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
  usp_Gera_SubItem.ExecProc ;

  inherited;

  {qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemEstoquenValTotalItem.Value ;
  qryMasternValImposto.Value  := qryMasternValImposto.Value  + qryItemEstoquenValIPI.Value    + qryItemEstoquenValICMSSub.Value;
  qryMasternValFrete.Value    := qryMasternValFrete.Value    + qryItemEstoquenValFrete.Value ;
  qryMasternValOutros.Value   := qryMasternValOutros.Value   + qryItemEstoquenValSeguro.Value + qryItemEstoquenValAcessorias.Value ;}

  qryMaster.Post ;
end;

procedure TfrmPedidoDevCompra.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabTipoPedido.Value := qryTipoPedidonCdTabTipoPedido.Value ;
      qryMasternCdTabStatusPed.Value  := 1 ;
  end ;

  if ((qryMasternCdLoja.Value = 0) or (DbEdit13.Text = '')) and (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      MensagemAlerta('Informe a Loja.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTipoPedido.Value = 0) or (DbEdit14.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de pedido.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdEstoqueMov.Value = 0) or (DBEdit23.Text = '') then
  begin
      MensagemAlerta('Informe o Estoque de Sa�da dos produtos.') ;
      DbEdit9.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTerceiro.Value = 0) or (DbEdit15.Text = '') then
  begin
      MensagemAlerta('Informe o Terceiro Fornecedor.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  if (DBEdit22.Text = '') then
  begin
      MensagemAlerta('Informe o endere�o de entrega da devolu��o.') ;
      DBEdit21.SetFocus;
      abort ;
  end ;

  if (DBEdit28.Text = '') then
  begin
      MensagemAlerta('Informe o modo do Frete.');
      cxPageControl1.ActivePage := tabFaturamento;
      DBEdit29.SetFocus;
      Abort;
  end;

  if (qryMasterdDtPedido.asString = '') then
  begin
      MensagemAlerta('Informe a data do pedido.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;

  if ((bFaturaDev) and (qryCondPagto.IsEmpty)) then
  begin
      MensagemAlerta('Informe a condi��o de pagamento.');
      DBEdit24.SetFocus;
      Abort;
  end;  

  if (qryTipoPedidocGerarFinanc.Value = 1) and (DBEDit27.Text = '') then
  begin
      MensagemAlerta('Informe a condi��o de pagamento.') ;
      DBEdit24.SetFocus;
      abort ;
  end ;

  inherited;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabStatusPed.Value := 1;
      qryMasternCdEmpresa.Value      := frmMenu.nCdEmpresaAtiva;
  end ;

  qryMasternValPedido.Value := qryMasternValOutros.Value
                             + qryMasternValAcrescimo.Value
                             - qryMasternValDesconto.Value
                             + qryMasternValImposto.Value
                             + qryMasternValServicos.Value
                             + qryMasternValFrete.Value
                             + qryMasternValProdutos.Value;
end;

procedure TfrmPedidoDevCompra.qryTempAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmPedidoDevCompra.qryItemEstoqueBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemEstoquenCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  //qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemEstoquenValTotalItem.Value ;
  //qryMasternValImposto.Value  := qryMasternValImposto.Value  - qryItemEstoquenValIPI.Value - qryItemEstoquenValICMSSub.Value ;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value - (qryItemEstoquenValUnitario.Value   * qryItemEstoquenQtdePed.Value);
  qryMasternValFrete.Value    := qryMasternValFrete.Value    - (qryItemEstoquenValFrete.Value      * qryItemEstoquenQtdePed.Value);
  qryMasternValDesconto.Value := qryMasternValDesconto.Value - (qryItemEstoquenValDesconto.Value   * qryItemEstoquenQtdePed.Value);
  qryMasternValImposto.Value  := qryMasternValImposto.Value  - (qryItemEstoquenValIPI.Value        + qryItemEstoquenValICMSSub.Value);
  qryMasternValOutros.Value   := qryMasternValOutros.Value   - (qryItemEstoquenValAcessorias.Value + qryItemEstoquenValSeguro.Value);
end;

procedure TfrmPedidoDevCompra.qryItemEstoqueAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmPedidoDevCompra.qryPrazoPedidoBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryPrazoPedidonCdPedido.Value := qryMasternCdPedido.Value ;

  if (qryPrazoPedidoiDias.Value > 0) then
      qryPrazoPedidodVencto.Value := qryMasterdDtPrevEntIni.Value + qryPrazoPedidoiDias.Value ;

  if (qryPrazoPedidodVencto.asString = '') then
  begin
      MensagemAlerta('Informe a data do vencimento.') ;
      abort ;
  end ;

  if (qryPrazoPedidonValPagto.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

end;

procedure TfrmPedidoDevCompra.btSugParcelaClick(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternValPedido.Value = 0) then
  begin
      MensagemAlerta('Pedido sem valor.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_Sugere_Parcela.Close ;
      usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Sugere_Parcela.ExecProc ;

      qryPrazoPedidoDif.Close;
      qryPrazoPedidoDif.Parameters.ParamByName('nPK').Value := qryMasternCdPedido.Value;
      qryPrazoPedidoDif.ExecSQL;

  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

end;

procedure TfrmPedidoDevCompra.btFinalizarPedClick(Sender: TObject);
var
  nValProdutos  : Double ;
  nValDescontos : Double ;
  nValImpostos  : Double ;
begin

  nValProdutos  := 0 ;
  nValDescontos := 0 ;
  nValImpostos  := 0 ;

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value <> 1) then
  begin
      MensagemAlerta('Pedido j� finalizado.') ;
      exit ;
  end ;

  if qryItemEstoque.Active then
      qryItemEstoque.First ;

  // calcula o total do pedido
  if qryItemEstoque.Active and (qryItemEstoque.RecordCount > 0) then
  begin

      qryItemEstoque.First ;

      while not qryItemEstoque.Eof do
      begin
          nValProdutos  := nValProdutos  + (qryItemEstoquenValUnitario.Value * qryItemEstoquenQtdePed.Value);
          nValDescontos := nValDescontos + (qryItemEstoquenValDesconto.Value * qryItemEstoquenQtdePed.Value);
          nValImpostos  := nValImpostos  + qryItemEstoquenValIPI.Value       + qryItemEstoquenValICMSSub.Value;
          qryItemEstoque.Next ;
      end ;

      qryItemEstoque.First ;

  end ;

  qryMaster.Edit ;
  qryMasternValProdutos.Value := nValProdutos ;
  qryMasternValDesconto.Value := nValDescontos ;
  qryMasternValImposto.Value  := nValImpostos ;
  qryMaster.Post ;

  if (qryMasternValPedido.Value = 0) then
  begin

      case MessageDlg('Pedido sem valor n�o pode ser finalizado.', mtConfirmation,[mbYes,mbNo],0) of
        mrNo:Abort;
      end;

  end ;

  if (qryTipoPedidocGerarFinanc.Value = 1) then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT Sum(nValPagto) FROM PrazoPedido WHERE nCdPedido = ' + qryMasternCdPedido.asString) ;
      qryAux.Open ;

      If (qryAux.Eof and (qryMasternValPedido.Value > 0)) or (qryAux.FieldList[0].Value <> qryMasternValPedido.Value) then
      begin

          frmMenu.Connection.BeginTrans;

          try
              usp_Sugere_Parcela.Close ;
              usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
              usp_Sugere_Parcela.ExecProc ;
          except
              frmMenu.Connection.RollbackTrans ;
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          frmMenu.Connection.CommitTrans ;

          PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

      end ;

      qryAux.Close ;

  end ;

  {-- verifica se todos os impostos foram digitados corretamente --}
  if ( bFaturaDev ) then
  begin

      qryItemEstoque.First ;

      while not qryItemEstoque.Eof do
      begin

          if (length(Trim(qryItemEstoquecCdSTICMS.Value)) < 3) then
          begin
              MensagemAlerta('Informe o C�digo CST do item. (Origem da Mercadoria e C�digo Cst) Ex. 000.') ;
              abort ;
          end;

          if (Trim(qryItemEstoquecCFOPItemPedido.Value) = '') then
          begin
              MensagemAlerta('Informe o C�digo CFOP de devolu��o.') ;
              abort ;
          end;

          if     (length(Trim(qryItemEstoquecNCM.Value)) <> 2)
             and (length(Trim(qryItemEstoquecNCM.Value)) <> 8) then
          begin
              MensagemAlerta('NCM n�o informado ou inv�lido.') ;
              abort ;
          end;

          PosicionaQuery(qryCFOP,qryItemEstoquecCFOPItemPedido.AsString);

          if qryCFOP.Eof then
          begin
              MensagemAlerta('CFOP n�o cadastrado.');
              abort;
          end;

          // Verifica tipo de cfop
          if ( strToInt( Copy(qryItemEstoquecCFOPItemPedido.Value,1,1) ) < 5 ) then
          begin
              MensagemAlerta('O CFOP informado � um CFOP de entrada. Informe um CFOP de saida.');
              abort;
          end;

          qryItemEstoque.Next ;
      end ;

      qryItemEstoque.First ;

  end ;

  case MessageDlg('O pedido ser� finalizado e n�o poder� sofrer altera��es. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Abort;
  end;

  frmMenu.Connection.BeginTrans ;

  try
      qryMaster.Edit ;
      // verifica se o tipo do pedido exige autoriza��o
      if (qryTipoPedidocExigeAutor.Value = 1) then
          qryMasternCdTabStatusPed.Value := 2  // Aguardando aprova��o
      else begin
          qryMasternCdTabStatusPed.Value := 3 ; // Aprovado
          qryMasterdDtAutor.Value        := Now() ;
          qryMasternCdUsuarioAutor.Value := frmMenu.nCdUsuarioLogado;

      end ;

      qryMasternSaldoFat.Value := qryMasternValPedido.Value ;
      qryMaster.Post ;

      usp_Finaliza.Close ;
      usp_Finaliza.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Finaliza.ExecProc;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Pedido finalizado com sucesso.') ;
  btCancelar.Click;

end;

procedure TfrmPedidoDevCompra.qryTempAfterCancel(DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;

  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;

end;

procedure TfrmPedidoDevCompra.qryItemADAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;
end;

procedure TfrmPedidoDevCompra.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemEstoque.State = dsBrowse) then
             qryItemEstoque.Edit ;

        if (qryItemEstoque.State = dsInsert) or (qryItemEstoque.State = dsEdit) then
        begin

            if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cCdProduto') then
            begin

                If (qryMasternCdTipoPedido.asString = '') then
                begin
                    ShowMessage('Selecione um tipo de pedido.') ;
                    abort ;
                end ;

                nPK := frmLookup_Padrao.ExecutaConsulta2(76,'NOT EXISTS(SELECT 1 FROM Produto Filho WHERE Filho.nCdProdutoPai = Produto.nCdProduto) AND EXISTS(SELECT 1 FROM GrupoProdutoTipoPedido GPTP WHERE GPTP.nCdGrupoProduto = nCdGrupo_Produto AND GPTP.nCdTipoPedido = ' + qryMasternCdTipoPedido.asString + ')');

                If (nPK > 0) then
                begin
                    qryItemEstoquenCdProduto.Value := nPK ;
                    qryItemEstoquecCdProduto.Value := qryItemEstoquenCdProduto.asString ;

                    qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
                    PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

                    if not qryProduto.eof then
                    begin
                        qryItemEstoquecNmItem.Value             := qryProdutocNmProduto.Value ;
                        qryItemEstoquecSiglaUnidadeMedida.Value := qryProdutocUnidadeMedida.Value;
                    end;

                end ;

            end;

            if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'cCFOP') then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(747,'WHERE Convert(INTEGER,cCFOP) > 4000');

                If (nPK > 0) then
                    qryItemEstoquecCFOPItemPedido.Value := intToStr(nPk);
            end;

        end;


    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State = dsInsert) then
  begin
      btSalvar.Click ;
  end ;

  DBGridEh1.Col := 1 ;

end;

procedure TfrmPedidoDevCompra.DBGridEh4Enter(Sender: TObject);
begin
  inherited;
  if (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmPedidoDevCompra.btImprimirPedClick(Sender: TObject);
var
   objRel : TrptPedidoDevCompra;

begin
  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      ShowMessage('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) and (qryMasternCdTabStatusPed.Value = 1) then
  begin
      qryMaster.Post ;
  end ;

  objRel := TrptPedidoDevCompra.Create(nil);

  Try
      Try
          PosicionaQuery(objRel.qryPedido,qryMasternCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemEstoque_Grade,qryMasternCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemAD,qryMasternCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemFormula,qryMasternCdPedido.asString) ;

          objRel.QRSubDetail1.Enabled := True ;
          objRel.QRSubDetail2.Enabled := True ;
          objRel.QRSubDetail3.Enabled := True ;

          if (objRel.qryItemEstoque_Grade.eof) then
              objRel.QRSubDetail1.Enabled := False ;

          if (objRel.qryItemAD.eof) then
              objRel.QRSubDetail2.Enabled := False ;

          if (objRel.qryItemFormula.eof) then
              objRel.QRSubDetail3.Enabled := False ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  Finally;
      FreeAndNil(objRel);
  end;

end;

procedure TfrmPedidoDevCompra.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  If (dataCol = 4) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) < qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clYellow;
        DBGridEh1.Canvas.Font.Color  := clBlue ;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) >= qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clBlue;
        DBGridEh1.Canvas.Font.Color  := clWhite ;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  If (dataCol = 5) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemEstoquenQtdeCanc.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clRed;
        DBGridEh1.Canvas.Font.Color  := clWhite ;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;
end;

procedure TfrmPedidoDevCompra.qryItemFormulaAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmPedidoDevCompra.btPrecoEsperadoClick(Sender: TObject);
var
   objForm : TfrmPrecoEspPedCom;
begin

    if not qryMaster.active then
        exit ;

    if (qryMasternCdPedido.Value = 0) then
    begin
        ShowMessage('Nenhum pedido ativo.') ;
        exit ;
    end ;

    objForm := TfrmPrecoEspPedCom.Create(nil);
    objForm.nCdPedido := qryMasternCdPedido.Value ;
    ShowForm(objform,True);

end;

procedure TfrmPedidoDevCompra.btCancelarPedClick(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 2) then
  begin
      MensagemAlerta('O status do pedido n�o permite mais cancelamento.') ;
      exit ;
  end ;

  case MessageDlg('O pedido ser� cancelado. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Exit;
  end;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Edit ;
      qryMasternCdTabStatusPed.Value := 10 ;
      qryMaster.Post ;

      frmMenu.LogAuditoria(30,3,qryMasternCdPedido.Value,'Pedido Cancelado') ;

  except
      frmMenu.Connection.RollbackTrans;
      qryMaster.Cancel;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;
  
  ShowMessage('Pedido cancelado com sucesso.') ;
  btCancelar.Click;

end;

procedure TfrmPedidoDevCompra.ExibirRecebimentosdoItem1Click(
  Sender: TObject);
  var
    objCons : TfrmItemPedidoAtendido;
begin
  inherited;

  objCons :=  TfrmItemPedidoAtendido.Create(nil);

  if (cxPageControl1.ActivePage = TabItemEstoque) then
  begin
      if (qryItemEstoque.Active) and (qryItemEstoquenCdItemPedido.Value > 0) then
      begin
          PosicionaQuery(objCons.qryItem, qryItemEstoquenCdItemPedido.AsString) ;
          showForm(objCons,True) ;
      end ;
  end ;

end;

procedure TfrmPedidoDevCompra.cxButton6Click(Sender: TObject);
begin
  inherited;
  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  PosicionaQuery(rptPedCom_Contrato.qryPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(rptPedCom_Contrato.qryItemEstoque_Grade,qryMasternCdPedido.asString) ;
  PosicionaQuery(rptPedCom_Contrato.qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(rptPedCom_Contrato.qryItemFormula,qryMasternCdPedido.asString) ;

  rptPedCom_Contrato.QRSubDetail1.Enabled := True ;
  rptPedCom_Contrato.QRSubDetail2.Enabled := True ;
  rptPedCom_Contrato.QRSubDetail3.Enabled := True ;

  if (rptPedCom_Contrato.qryItemEstoque_Grade.eof) then
      rptPedCom_Contrato.QRSubDetail1.Enabled := False ;

  if (rptPedCom_Contrato.qryItemAD.eof) then
      rptPedCom_Contrato.QRSubDetail2.Enabled := False ;

  if (rptPedCom_Contrato.qryItemFormula.eof) then
      rptPedCom_Contrato.QRSubDetail3.Enabled := False ;

  rptPedCom_Contrato.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

  rptPedCom_Contrato.QuickRep1.PreviewModal;

end;

procedure TfrmPedidoDevCompra.DBEdit9Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  IF (DBEdit3.Enabled) and (DbEdit3.Text = '') then
  begin
      MensagemAlerta('Informe a Loja.') ;
      DBEdit3.SetFocus ;
      abort ;
  end ;

  qryLocalEstoqueSaida.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmPedidoDevCompra.DBEdit9Exit(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  qryLocalEstoqueSaida.Close ;
  PosicionaQuery(qryLocalEstoqueSaida, DBEdit9.Text) ;

  if (DBEdit3.Enabled) then
  begin
      if (DBEdit13.Text = '') then
      begin
          MensagemAlerta('Selecione a loja.') ;
          DBEdit3.SetFocus ;
          abort ;
      end ;

      if (qryMaster.State = dsInsert) then
      begin
          if (qryLocalEstoqueSaida.IsEmpty) then
              Exit;

          if (qryLocalEstoqueSaidanCdLoja.Value <> qryLojanCdLoja.Value) then
          begin

              if (MessageDlg('O local de estoque selecionado n�o pertence a loja informada. Tem certeza que deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MRNo) then
              begin
                  DBEdit3.SetFocus;
                  abort ;
              end ;

          end ;

      end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsBrowse) then
             qryMaster.Edit ;

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva));

            If (nPK > 0) then
            begin
                qryMasternCdEstoqueMov.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.btLeituraClick(Sender: TObject);
begin

    if not (qryMaster.Active) then
        exit ;

    if (qryMasternCdPedido.Value = 0) then
    begin
        MensagemAlerta('Salve o pedido antes de iniciar a leitura.') ;
        exit ;
    end ;

    if (qryMasternCdTabStatusPed.Value > 1) then
    begin
        MensagemAlerta('O status do pedido n�o permite mais leituras.') ;
        exit ;
    end ;

    objForm := TfrmTransfEst_Itens.Create(nil);

    objForm.RadioGroup1.Visible   := False ;
    objForm.RadioGroup1.ItemIndex := 0 ;
    objForm.Caption := 'Itens da Devolu��o' ;

    showForm(objForm,False); //frmTransfEst_Itens.ShowModal ;

    case MessageDlg('Confirma a grava��o dos itens ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo:exit ;
    end ;

    TrataProdutos;

    FreeAndNil(objForm);

end;

procedure TfrmPedidoDevCompra.TrataProdutos();
begin

    objForm.cdsProduto.First ;

    frmMenu.Connection.BeginTrans;

    while not objForm.cdsProduto.Eof do
    begin

      qryTrataProduto.Close ;
      qryTrataProduto.Parameters.ParamByName('nCdPedido').Value   := qryMasternCdPedido.Value ;
      qryTrataProduto.Parameters.ParamByName('nQtde').Value       := objForm.cdsProdutonQtde.Value ;
      qryTrataProduto.Parameters.ParamByName('cCodigo').Value     := objForm.cdsProdutocCodigo.Value ;
      qryTrataProduto.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
      qryTrataProduto.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;

      try
          qryTrataProduto.ExecSQL;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      objForm.cdsProduto.Delete;

      objForm.cdsProduto.First;

    end ;

    frmMenu.Connection.CommitTrans;

    objForm.Close ;

    PosicionaQuery(qryMaster, qryMasternCdPedido.AsString) ;

end ;


procedure TfrmPedidoDevCompra.btConsultarDocEntradaClick(Sender: TObject);
var
objCons : TfrmPedidoDevCompra_ConsultaItens;
begin
  inherited;

  if not (qryMaster.Active) then
      exit ;

  if (qryMasternCdPedido.Value = 0) then
  begin
      MensagemAlerta('Salve o pedido antes de iniciar a leitura.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      MensagemAlerta('O status do pedido n�o permite altera��es.') ;
      exit ;
  end ;

  objCons := TfrmPedidoDevCompra_ConsultaItens.Create(nil);

  objCons.nCdPedido     := qryMasternCdPedido.Value ;
  objCons.nCdTipoPedido := qryMasternCdTipoPedido.Value ;
  objCons.nCdTerceiroAux:= qryMasternCdterceiro.Value ;
  objCons.nCdLoja       := qryMasternCdLoja.Value; 
  
  showForm(objCons,True);

  PosicionaQuery(qryMaster, qryMasternCdPedido.AsString) ;

  DBGridEh1.SetFocus;
  DBGridEh1.Col := 3;
  
  if (qryItemEstoque.Active) then
      qryItemEstoque.Last;

  completaDadosFaturamento();
      
end;

procedure TfrmPedidoDevCompra.cxButton4Click(Sender: TObject);
var
  cOBS    : string ;
  iQtdeNF : integer ;
begin

  if not (qryMaster.Active) then
      exit ;

  if (qryMasternCdPedido.Value = 0) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      MensagemAlerta('O status do pedido n�o permite altera��es.') ;
      exit ;
  end ;

  if (not qryItemEstoque.Active) or (qryItemEstoque.RecordCount = 0) then
  begin
      MensagemAlerta('Nenhum item de estoque selecionado para devolu��o.') ;
      exit ;
  end ;

  qryItemEstoque.DisableControls;
  qryItemEstoque.First ;

  cOBS    := 'DEV CF NF ' ;
  iQtdeNF := 0 ;

  while not qryItemEstoque.eof do
  begin

      if (Pos(cOBS,Trim(qryItemEstoquecDoctoCompra.asString)) = 0) and (trim(qryItemEstoquecDoctoCompra.AsString) <> '') then
      begin
          if (iQtdeNF = 0) then
              cOBS := cOBS + Trim( qryItemEstoquecDoctoCompra.AsString )
          else cOBS := cOBS + ' / ' + Trim( qryItemEstoquecDoctoCompra.AsString ) ;

          iQtdeNF := iQtdeNF + 1 ;
      end ;

      qryItemEstoque.Next ;
  end ;

  qryItemEstoque.First ;
  qryItemEstoque.EnableControls;

  qryMaster.Edit ;
  qryMastercMsgNF1.Value := Copy(cOBS,1,150) ;
  qryMastercMsgNF2.Value := Copy(cOBS,151,150) ;

end;

procedure TfrmPedidoDevCompra.DBEdit41KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit41Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTerceiroPagador,DBEdit41.Text) ;

end;

procedure TfrmPedidoDevCompra.DBEdit19Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryIncoterms,DbEdit19.Text) ;

end;

procedure TfrmPedidoDevCompra.DBEdit10KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit10Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTerceiroTransp, DbEdit10.Text) ;

end;

procedure TfrmPedidoDevCompra.btSalvarClick(Sender: TObject);
begin
  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      MensagemAlerta('O status do pedido n�o permite altera��es.') ;
      exit ;
  end ;

  inherited;

end;

procedure TfrmPedidoDevCompra.ToolButton5Click(Sender: TObject);
begin
  cxPageControl1.ActivePageIndex := 0 ;
  inherited;

end;

procedure TfrmPedidoDevCompra.CompletaDadosFaturamento;
begin
    {if (qryItemEstoquenCdProduto.Value > 0) then
    begin
        qryGrupoImposto.Close;
        qryGrupoImposto.Parameters.ParamByName('nPk').Value := qryItemEstoquenCdProduto.Value;
        qryGrupoImposto.Open;

        if (qryItemEstoquecNCM.Value = '') then
            qryItemEstoquecNCM.Value := qryGrupoImpostocNCM.Value;

        qryUFEmissao.Close;
        qryUFEmissao.Parameters.ParamByName('nCdTerceiroEmp').Value  := qryEmpresanCdTerceiroEmp.Value;
        qryUFEmissao.Open;

        qryUFDestino.Close;
        qryUFDestino.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value;
        qryUFDestino.Open;

        qryRegraICMS.Close;
        qryRegraICMS.Parameters.ParamByName('nCdEmpresa').Value      := frmMenu.nCdEmpresaAtiva;
        qryRegraICMS.Parameters.ParamByName('cUFOrigem').Value       := qryUFEmissaocUF.Value;
        qryRegraICMS.Parameters.ParamByName('cUFDestino').Value      := qryUFDestinocUF.Value;
        qryRegraICMS.Parameters.ParamByName('nCdGrupoImposto').Value := qryGrupoImpostonCdGrupoImposto.Value;
        qryRegraICMS.Parameters.ParamByName('nCdTipoPedido').Value   := qryMasternCdTipoPedido.Value;
        qryRegraICMs.Open;

        if (qryItemEstoque.State = dsBrowse) then
            qryItemEstoque.Edit;

        qryItemEstoquenPercIva.Value         := qryRegraICMSnPercIva.Value;
        qryItemEstoquenPercAliqICMS.Value    := qryRegraICMSnAliqICMS. Value;
        qryItemEstoquenPercAliqICMSInt.Value := qryRegraICMSnAliqICMSInterna.Value;
        qryItemEstoquenPercRedBaseIcms.Value := (100 - qryRegraICMSnPercBC.Value);
        qryItemEstoquecCdSTICMS.Value        := qryGrupoImpostonCdTabTipoOrigemMercadoria.AsString + qryRegraICMSnCdTipoTributacaoICMS.Value;

    end;}

end;

function TfrmPedidoDevCompra.RightStr(const Str: String;
  Size: Word): String;
begin
  if Size > Length(Str) then Size := Length(Str) ;
  RightStr := Copy(Str, Length(Str)-Size+1, Size)
end;

procedure TfrmPedidoDevCompra.qryItemEstoquecCdProdutoChange(
  Sender: TField);
begin
  inherited;

  if ( trim( qryItemEstoquecCdProduto.Value ) <> '' ) and ( trim( Uppercase( qryItemEstoquecCdProduto.Value ) ) <> 'AD' )  then
  begin

      try
          strToInt( qryItemEstoquecCdProduto.Value ) ;
      except
          MensagemAlerta('C�digo de produto inv�lido. Informe "AD" para itens n�o cadastrados.') ;
          abort ;
      end ;

      qryItemEstoquenCdProduto.Value := strToInt( qryItemEstoquecCdProduto.Value ) ;

      qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
      PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

      if not qryProduto.eof and (Trim( qryItemEstoquecNmItem.Value ) <> Trim( qryProdutocNmProduto.Value ) ) then
      begin
          qryItemEstoquecNmItem.Value             := qryProdutocNmProduto.Value ;
          qryItemEstoquecSiglaUnidadeMedida.Value := qryProdutocUnidadeMedida.Value;
      end;

  end ;

end;

procedure TfrmPedidoDevCompra.verificaControlesICMS(cCST: string);
var
    bICMS   , bICMSST    : boolean ;
    bICMSRed, bICMSSTRed : boolean ;
    i                    : integer ;

begin

    if (not qryMaster.Active) then
        exit ;

    bICMS   := true ;
    bICMSST := false ;

    if (trim(RightStr(cCST,2)) = '30') or (trim(RightStr(cCST,2)) = '40') or (trim(RightStr(cCST,2)) = '41') or (trim(RightStr(cCST,2)) = '50') then
        bICMS := false ;

    if (trim(RightStr(cCST,2)) = '10') or (trim(RightStr(cCST,2)) = '20') or (trim(RightStr(cCST,2)) = '30') or (trim(RightStr(cCST,2)) = '70') then
    begin
        bICMSST := true ;
    end ;

    bICMSRed   := (trim(RightStr(cCST,2)) = '20') or (trim(RightStr(cCST,2)) = '70') ;
    bICMSSTRed := (trim(RightStr(cCST,2)) = '70') ;

    // formata as colunas do grid
    with DBGridEh1 do
    begin

        {-- ICMS NORMAL --}
        // % reducao base calc icms
        Columns[FieldColumns['nPercRedBaseICMS'].Index].ReadOnly := (not bICMSRed) ;

        if (bICMSRed)  then
            Columns[FieldColumns['nPercRedBaseICMS'].Index].Title.Font.Color := clBlack
        else
        begin
            Columns[FieldColumns['nPercRedBaseICMS'].Index].Title.Font.Color := clRed ;
        end ;


        {-- as colunas de base de c�lculo do ICMS de Valor do ICMS SEMPRE ficam somente leitura --}
        Columns[FieldColumns['nValBaseICMS'].Index].ReadOnly  := TRUE ;
        Columns[FieldColumns['nPercAliqICMS'].Index].ReadOnly := TRUE ;
        Columns[FieldColumns['nValICMS'].Index].ReadOnly      := TRUE ;

        Columns[FieldColumns['nValBaseICMS'].Index].Title.Font.Color  := clRed ;
        Columns[FieldColumns['nPercAliqICMS'].Index].Title.Font.Color := clRed ;
        Columns[FieldColumns['nValICMS'].Index].Title.Font.Color      := clRed ;

        {-- *** Substitui��o Tributaria *** --}
        {-- se bICMSST for TRUE, indica que o CST do item tem substitui��o tribut�ria --}
        {-- quando bICMSST for TRUE, ele precisa deixar o ReadOnly como false, por isso � atribuido o valor de (not bICMSST) --}

        Columns[FieldColumns['nPercIVA'].Index].ReadOnly         := (not bICMSST) ;
        Columns[FieldColumns['nPercAliqICMSInt'].Index].ReadOnly := (not bICMSST) ;
        Columns[FieldColumns['nValBaseICMSSub'].Index].ReadOnly  := (not bICMSST) ;
        Columns[FieldColumns['nValICMSSub'].Index].ReadOnly      := (not bICMSST) ;

        if (bICMS) then
        begin
            Columns[FieldColumns['nPercAliqICMS'].Index].ReadOnly         := FALSE ;
            Columns[FieldColumns['nPercAliqICMS'].Index].Title.Font.Color := clBlack ;
        end;

        if (bICMSST)  then
        begin

            Columns[FieldColumns['nPercIVA'].Index].Title.Font.Color         := clBlack ;
            Columns[FieldColumns['nPercAliqICMSInt'].Index].Title.Font.Color := clBlack ;
            Columns[FieldColumns['nValBaseICMSSub'].Index].Title.Font.Color  := clBlack ;
            Columns[FieldColumns['nValICMSSub'].Index].Title.Font.Color      := clBlack ;

        end
        else
        begin

            Columns[FieldColumns['nPercIVA'].Index].Title.Font.Color         := clRed ;
            Columns[FieldColumns['nPercAliqICMSInt'].Index].Title.Font.Color := clRed ;
            Columns[FieldColumns['nValBaseICMSSub'].Index].Title.Font.Color  := clRed ;
            Columns[FieldColumns['nValICMSSub'].Index].Title.Font.Color      := clRed ;

        end ;

    end ;

    if (qryItemEstoque.State in [dsInsert,dsEdit]) then
    begin
         if not (bICMS) Then
         Begin
             qryItemEstoquenPercAliqICMS.Value    := 0;
             qryItemEstoquenValBaseICMS.Value     := 0;
             qryItemEstoquenValICMS.Value         := 0;
             qryItemEstoquenPercRedBaseICMS.Value := 0;
         end;

         if not (bICMSRed) then
             qryItemEstoquenPercRedBaseICMS.Value := 0;

         if not (bICMSST) then
         begin
             qryItemEstoquenPercIVA.Value                := 0;
             qryItemEstoquenPercAliqICMSInt.Value        := 0;
             qryItemEstoquenValBaseICMSSub.Value         := 0;
             qryItemEstoquenPercICMSSub.Value            := 0;
             qryItemEstoquenValICMSSub.Value             := 0;
         end ;

    end;

    calculaImpostosItem( FALSE );

end;

procedure TfrmPedidoDevCompra.qryItemEstoqueAfterScroll(DataSet: TDataSet);
begin
  inherited;

  {if (qryItemEstoque.State = dsBrowse) then
      verificaControlesICMS( qryItemEstoquecCdSTICMS.Value ) ;}

  ativaEdicaoProduto( (qryItemEstoquecCdProduto.Value = 'AD') ) ;
end;

procedure TfrmPedidoDevCompra.qryItemEstoquecCdSTICMSChange(
  Sender: TField);
begin
  inherited;

  {verificaControlesICMS( qryItemEstoquecCdSTICMS.Value ) ;}

end;

procedure TfrmPedidoDevCompra.calculaImpostosItem( bGravar : boolean ) ;
begin
    { -- se grupo de pedido n�o permite faturamento, n�o valida os impostos porque n�o ser� emitida a nota de devolu��o -- }
    if (not bFaturaDev) then
        exit ;

    if (qryItemEstoque.State in ([dsInsert,dsEdit])) then
    begin
    
        qryItemEstoquenValTotalItem.Value := (qryItemEstoquenValCustoUnit.Value * qryItemEstoquenQtdePed.Value) ;

        {-- calcula o IPI --}
        if (qryItemEstoquenPercIPI.Value > 0) then
        begin

            qryItemEstoquenValBaseIPI.Value := qryItemEstoquenValTotalItem.Value;

            if (qryItemEstoquenPercBaseCalcIPI.Value > 0) then
                qryItemEstoquenValBaseIPI.Value := (qryItemEstoquenValTotalItem.Value * ( qryItemEstoquenPercBaseCalcIPI.Value / 100 ) );

            qryItemEstoquenValIPI.Value := (qryItemEstoquenValBaseIPI.Value * (qryItemEstoquenPercIPI.Value/100));

            if (bGravar) then
            begin

                if ( trim( qryItemEstoquecCdSTIPI.Value ) = '' ) then
                begin
                    MensagemAlerta('Informe o CST do IPI.') ;
                    abort ;
                end ;

                qryCST_IPI.Close;
                PosicionaQuery( qryCST_IPI , qryItemEstoquecCdSTIPI.Value ) ;

                if ( qryCST_IPI.eof ) then
                begin
                    MensagemAlerta('CST do IPI inv�lido.') ;
                    abort ;
                end
                else
                begin

                    {-- aqui converte um campo VARCHAR para inteiro sem testar pois essa tabela                --}
                    {-- � uma tabela dom�nio sem acesso pelo usu�rio via interface, ent�o os c�digo s�o sempre --}
                    {-- num�ricos e mantidos pela ER2.                                                         --}
                    if ( strToint( qryCST_IPIcCdStIPI.Value ) < 50 ) then
                    begin
                        MensagemAlerta('Informe um CST de Sa�da para o IPI.') ;
                        abort ;
                    end ;

                end ;

            end ;

        end
        else
        begin

            if (bGravar) then
            begin

                qryItemEstoquenValBaseIPI.Value      := 0 ;
                qryItemEstoquenPercBaseCalcIPI.Value := 0 ;
                qryItemEstoquenValIPI.Value          := 0 ;

            end ;

        end ;

        { -- ICMS -- }
        {-- se a aliquota est� somente leitura � porque o CST do ICMS diz que n�o tem ICMS --}
        if DBGridEh1.Columns[DBGridEh1.FieldColumns['nPercAliqICMS'].Index].ReadOnly
           or (qryItemEstoquenPercAliqICMS.Value = 0) then
        begin

            if (bGravar) then
            begin

                qryItemEstoquenValBaseICMS.Value     := 0 ;
                qryItemEstoquenPercRedBaseICMS.Value := 0 ;
                qryItemEstoquenPercAliqICMS.Value    := 0 ;
                qryItemEstoquenValICMS.Value         := 0 ;

            end ;

        end
        else
        begin

            qryItemEstoquenValBaseICMS.Value := qryItemEstoquenValTotalItem.Value;

            if (qryItemEstoquenPercRedBaseICMS.Value > 0) then
                qryItemEstoquenValBaseICMS.Value := (qryItemEstoquenValTotalItem.Value * (qryItemEstoquenPercRedBaseICMS.Value/100) );

            qryItemEstoquenValICMS.Value := (qryItemEstoquenValBaseICMS.Value * (qryItemEstoquenPercAliqICMS.Value/100));

            {
            qryCST_ICMS.Close;
            PosicionaQuery( qryCST_ICMS , RightStr(qryItemEstoquecCdStICMS.Value,2) );

            if qryCST_ICMS.Eof then
            begin
                MensagemAlerta('CST/CSOSN do ICMS inv�lido.');
                exit ;
            end;
            }

        end ;

        { -- ICMS Sub -- }
        if (qryItemEstoquenPercAliqICMSInt.Value = 0) then
        begin
            if (bGravar) then
            begin
                qryItemEstoquenPercIVA.Value           := 0;
                qryItemEstoquenPercAliqICMSInt.Value   := 0;
                qryItemEstoquenValBaseICMSSub.Value    := 0;
                qryItemEstoquenValICMSSub.Value        := 0;
                qryItemEstoquenValBaseICMSSubRet.Value := 0;
                qryItemEstoquenValICMSSubRet.Value     := 0;
            end;
        end
        else
        begin

            { -- mem�ria de c�lculo ICMS Sub. -- }
            { -- (total liq * %IPI) + total liq + (((total liq * %IPI) + total liq) * %IVA) -- }
            qryItemEstoquenValBaseICMSSub.Value := (qryItemEstoquenValTotalItem.Value * (qryItemEstoquenPercIPI.Value / 100))
                                                 + qryItemEstoquenValTotalItem.Value
                                                 + (((qryItemEstoquenValTotalItem.Value * (qryItemEstoquenPercIPI.Value / 100))
                                                 + qryItemEstoquenValTotalItem.Value) * (qryItemEstoquenPercIVA.Value / 100));

            qryItemEstoquenValICMSSub.Value        := ((qryItemEstoquenValBaseICMSSub.Value * (qryItemEstoquenPercAliqICMSInt.Value / 100))) - qryItemEstoquenValICMS.Value;
            qryItemEstoquenValBaseICMSSubRet.Value := 0;
            qryItemEstoquenValICMSSubRet.Value     := 0;

            { -- verifica se utiliza st normal ou st retido -- }
            if (((qryTerceirocFlgOptSimples.Value = 1) and (RightStr(qryItemEstoquecCdSTICMS.Value,3) = '500'))
               or((qryTerceirocFlgOptSimples.Value = 0) and (RightStr(qryItemEstoquecCdSTICMS.Value,2) = '60'))) then
            begin
                qryItemEstoquenValBaseICMSSubRet.Value := qryItemEstoquenValBaseICMSSub.Value;
                qryItemEstoquenValICMSSubRet.Value     := qryItemEstoquenValICMSSub.Value;
                qryItemEstoquenValBaseICMSSub.Value    := 0;
                qryItemEstoquenValICMSSub.Value        := 0;
            end;
        end;

        {-- PIS --}
        if (qryItemEstoquenAliqPIS.Value = 0) then
        begin

            if (bGravar) then
            begin
                if (qryItemEstoquenValBasePIS.Value > 0) then
                begin
                    MensagemAlerta('Informe a al�quota do PIS ou zere a base de c�lculo.') ;
                    abort ;
                end ;

                qryItemEstoquenValPIS.Value := 0 ;
                qryItemEstoquecCSTPIS.Value := '' ;

            end ;

        end
        else
        begin

            qryItemEstoquenValBasePIS.Value := qryItemEstoquenValTotalItem.Value + qryItemEstoquenValIPI.Value ;
            qryItemEstoquenValPIS.Value     := (qryItemEstoquenValBasePIS.Value * (qryItemEstoquenAliqPIS.Value/100));

            if (bGravar) then
            begin

                if ( trim( qryItemEstoquecCSTPIS.Value ) = '' ) then
                begin
                    MensagemAlerta('Informe o CST do PIS.') ;
                    abort ;
                end ;

                qryCST_PISCOFINS.Close;
                PosicionaQuery( qryCST_PISCOFINS , qryItemEstoquecCSTPIS.Value ) ;

                if ( qryCST_PISCOFINS.eof ) then
                begin
                    MensagemAlerta('CST do PIS inv�lido.') ;
                    abort ;
                end ;

            end ;

        end ;

        {-- COFINS --}
        if (qryItemEstoquenAliqCOFINS.Value = 0) then
        begin

            if (bGravar) then
            begin
                if (qryItemEstoquenValBaseCOFINS.Value > 0) then
                begin
                    MensagemAlerta('Informe a al�quota do COFINS ou zere a base de c�lculo.') ;
                    abort ;
                end ;

                qryItemEstoquenValCOFINS.Value := 0 ;
                qryItemEstoquecCSTCOFINS.Value := '' ;

            end ;

        end
        else
        begin
        
            qryItemEstoquenValBaseCOFINS.Value := qryItemEstoquenValTotalItem.Value + qryItemEstoquenValIPI.Value ;
            qryItemEstoquenValCOFINS.Value     := (qryItemEstoquenValBaseCOFINS.Value * (qryItemEstoquenAliqCOFINS.Value/100));

            if (bGravar) then
            begin

                if ( trim( qryItemEstoquecCSTCOFINS.Value ) = '' ) then
                begin
                    MensagemAlerta('Informe o CST do COFINS.') ;
                    abort ;
                end ;

                qryCST_PISCOFINS.Close;
                PosicionaQuery( qryCST_PISCOFINS , qryItemEstoquecCSTCOFINS.Value ) ;

                if ( qryCST_PISCOFINS.eof ) then
                begin
                    MensagemAlerta('CST do COFINS inv�lido.') ;
                    abort ;
                end ;

            end ;

        end ;

    end ;

end;

procedure TfrmPedidoDevCompra.ativaEdicaoProduto(bPermitirEditar: boolean);
begin

    if (bPermitirEditar) then
    begin
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Title.Font.Color := clBlack ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].ReadOnly         := False ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Font.Color       := clBlack ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Color            := clWhite ;
    end
    else
    begin
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Title.Font.Color := clRed ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].ReadOnly         := True ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Font.Color       := clBlack ;
        DBGridEh1.Columns[DBGridEh1.FieldColumns['cNmItem'].Index].Color            := clSilver ;
    end ;

end;

procedure TfrmPedidoDevCompra.DBEdit21Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.state = dsInsert) and (DBEdit22.Text = '') then
  begin

      qrySugereEnderecoEntrega.Close;
      qrySugereEnderecoEntrega.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
      qrySugereEnderecoEntrega.Open ;

      if not qrySugereEnderecoEntrega.Eof then
      begin
          qryMasternCdEnderecoEntrega.Value := qrySugereEnderecoEntreganCdEndereco.Value ;
          qryEnderecoEntrega.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
          PosicionaQuery(qryEnderecoEntrega, qryMasternCdEnderecoEntrega.AsString) ;
      end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit21KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (Trim(DbEdit15.Text) = '') then
            begin
                MensagemAlerta('Informe o Terceiro.') ;
                DBEdit5.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(206,'Endereco.nCdTerceiro = ' + qryMasternCdTerceiro.AsString);

            If (nPK > 0) then
            begin
                qryMasternCdEnderecoEntrega.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedidoDevCompra.DBEdit21Exit(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  qryEnderecoEntrega.Close;
  qryEnderecoEntrega.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
  PosicionaQuery(qryEnderecoEntrega, DBEdit21.Text) ;

  if (qryEnderecoEntregacNmStatus.Value = 'Inativo') then
      MensagemAlerta('Este endere�o est� inativo. Verifique.') ;

end;


procedure TfrmPedidoDevCompra.qryTipoPedidoAfterOpen(DataSet: TDataSet);
begin
  inherited;

  if not (qryTipoPedido.IsEmpty) then
  begin
      qryGrupoPedido.Close;
      PosicionaQuery(qryGrupoPedido, qryTipoPedidonCdGrupoPedido.AsString);

      bFaturaDev := (qryGrupoPedidocFlgFaturar.Value = 1);

      btPrecoEsperado.Enabled       := (qryTipoPedidocFlgItemEstoque.Value = 1);
      btLeitura.Enabled             := (qryTipoPedidocFlgItemEstoque.Value = 1);
      btConsultarDocEntrada.Enabled := (qryTipoPedidocFlgItemEstoque.Value = 1);
  end;
end;

procedure TfrmPedidoDevCompra.DBGridEh1ColEnter(Sender: TObject);
begin
  inherited;

  if (DBGridEh1.Columns[DBGridEh1.Col-1].FieldName = 'nValICMS') then
      DBGridEh1.Columns[DBGridEh1.FieldColumns['nValICMS'].Index].DisplayFormat := '#,##0.0000'
  else
      DBGridEh1.Columns[DBGridEh1.FieldColumns['nValICMS'].Index].DisplayFormat := '#,##0.00';
end;

procedure TfrmPedidoDevCompra.DBEdit29Exit(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  qryTabTipoModFrete.Close;
  PosicionaQuery(qryTabTipoModFrete, qryMasternCdTabTipoModFrete.AsString);
end;

procedure TfrmPedidoDevCompra.DBEdit29KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

  case (Key) of
      VK_F4 : begin
          if ((qryMaster.State = dsInsert) or (qryMaster.State = dsEdit)) then
          begin
              nPK := -1;
              nPK := frmLookup_Padrao.ExecutaConsulta(776);

              if (nPK > -1) then
                  qryMasternCdTabTipoModFrete.Value := nPK;

              {if ((qryMasternValFrete.Value = 0) and (nPK <> 9)) then
              begin
                  MensagemAlerta('Modo de Frete inv�lido devido pedido n�o possuir valor de frete informado.');
                  Abort;
              end;

              if ((qryMasternValFrete.Value > 0) and (nPK = 9)) then
              begin
                  MensagemAlerta('Modo de Frete inv�lido devido pedido possuir valor de frete informado.');
                  Abort;
              end;}
          end;
      end;
  end;
end;

initialization
    RegisterClass(TfrmPedidoDevCompra) ;

end.
