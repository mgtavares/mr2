unit rPosicaoEstoqueGeral_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosicaoEstoqueGeral_view = class(TForm)
    SPREL_POSICAO_ESTOQUE_GERAL: TADOStoredProc;
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRBand5: TQRBand;
    SummaryBand1: TQRBand;
    QRShape6: TQRShape;
    QRLabel38: TQRLabel;
    QRExpr6: TQRExpr;
    DetailBand1: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText30: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText8: TQRDBText;
    QRGroup1: TQRGroup;
    QRDBText2: TQRDBText;
    QRLabel5: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel6: TQRLabel;
    QRBand2: TQRBand;
    QRShape7: TQRShape;
    QRLabel4: TQRLabel;
    QRExpr1: TQRExpr;
    QRShape8: TQRShape;
    QRLabel7: TQRLabel;
    SPREL_POSICAO_ESTOQUE_GERALnCdProduto: TIntegerField;
    SPREL_POSICAO_ESTOQUE_GERALcNmProduto: TStringField;
    SPREL_POSICAO_ESTOQUE_GERALcNmGrupoProduto: TStringField;
    SPREL_POSICAO_ESTOQUE_GERALcUnidadeMedida: TStringField;
    SPREL_POSICAO_ESTOQUE_GERALdDtUltReceb: TDateTimeField;
    SPREL_POSICAO_ESTOQUE_GERALnQtdeDisponivel: TBCDField;
    SPREL_POSICAO_ESTOQUE_GERALdDtUltInventario: TDateTimeField;
    SPREL_POSICAO_ESTOQUE_GERALnValCustoUnitario: TBCDField;
    SPREL_POSICAO_ESTOQUE_GERALnValCustoTotal: TBCDField;
    QRShape5: TQRShape;
    QRLabel26: TQRLabel;
    QRLabel42: TQRLabel;
    QRLabel8: TQRLabel;
    QRExpr2: TQRExpr;
    SPREL_POSICAO_ESTOQUE_GERALnQtdeTransito: TBCDField;
    QRLabel13: TQRLabel;
    QRDBText9: TQRDBText;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoEstoqueGeral_view: TrptPosicaoEstoqueGeral_view;

implementation

{$R *.dfm}

end.
