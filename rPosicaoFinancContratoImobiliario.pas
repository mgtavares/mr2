unit rPosicaoFinancContratoImobiliario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, ER2Lookup, DB, DBCtrls, ADODB;

type
  TrptPosicaoFinancContratoImobiliario = class(TfrmRelatorio_Padrao)
    edtCdEmpresa: TER2LookupMaskEdit;
    edtCdEmpreendimento: TER2LookupMaskEdit;
    edtCdTerceiro: TER2LookupMaskEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryEmpImobiliario: TADOQuery;
    qryEmpImobiliarionCdEmpImobiliario: TIntegerField;
    qryEmpImobiliarionCdEmpresa: TIntegerField;
    qryEmpImobiliarionCdTipoPedido: TIntegerField;
    qryEmpImobiliariocNmEmpImobiliario: TStringField;
    qryEmpImobiliariodDtLancamento: TDateTimeField;
    qryEmpImobiliariocEndereco: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceirocNmTerceiro: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    edtBlocoTorre: TER2LookupMaskEdit;
    Label4: TLabel;
    qryBloco: TADOQuery;
    qryBloconCdBlocoEmpImobiliario: TAutoIncField;
    qryBloconCdEmpImobiliario: TIntegerField;
    qryBlococNmBlocoEmpImobiliario: TStringField;
    qryBloconCdCC: TIntegerField;
    qryBlocodDtHabitese: TDateTimeField;
    qryBlocodDtChaves: TDateTimeField;
    qryBlocoiQtdeUnidade: TIntegerField;
    DBEdit4: TDBEdit;
    DataSource4: TDataSource;
    Label5: TLabel;
    edtDtInicial: TMaskEdit;
    Label6: TLabel;
    edtDtFinal: TMaskEdit;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    SPREL_POSICAO_CARTEIRAFINANC_IMOB_ANALITICA: TADOStoredProc;
    cmdPreparaTemp: TADOCommand;
    qryTempParcelas: TADOQuery;
    qryTempParcelasnCdTitulo: TIntegerField;
    qryTempParcelascNmTerceiro: TStringField;
    qryTempParcelascNmEmpreendimentoBloco: TStringField;
    qryTempParcelasdDtVenc: TDateTimeField;
    qryTempParcelasiParcela: TIntegerField;
    qryTempParcelascNmTipoParcela: TStringField;
    qryTempParcelasnValTit: TBCDField;
    qryTempParcelasnValCorrecao: TBCDField;
    qryTempParcelasnValJuroMulta: TBCDField;
    qryTempParcelasnSaldoFinal: TBCDField;
    qryTempParcelascNumContrato: TStringField;
    qryTempParcelascNrUnidade: TStringField;
    qryPopulaTempResumoEmpreendimento: TADOQuery;
    qryPopulaTempResumoContrato: TADOQuery;
    procedure ToolButton1Click(Sender: TObject);
    procedure edtCdEmpreendimentoExit(Sender: TObject);
    procedure edtBlocoTorreBeforeLookup(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosicaoFinancContratoImobiliario: TrptPosicaoFinancContratoImobiliario;

implementation

uses fMenu, fAtualizacaoIndice, rPosicaoFinancContratoImobiliario_Diario_view, rPosicaoFinancContratoImobiliario_ResumoEmpreendimento_view,
     rPosicaoFinancContratoImobiliario_ResumoContrato_view ;

{$R *.dfm}

procedure TrptPosicaoFinancContratoImobiliario.ToolButton1Click(
  Sender: TObject);
var
    objCorrecao     : TfrmAtualizacaoMonetaria ;
    objRelDiario    : TrptPosicaoFinancContratoImobiliario_Diario_view;
    objRelResumoEmp : TrptPosicaoFinancContratoImobiliario_ResumoEmpreendimento_view ;
    objRelResumoContrato : TrptPosicaoFinancContratoImobiliario_ResumoContrato_view ;
    nValJuros, nValMulta : Double;
    cFiltro     : string ;
begin
  inherited;

  {-- prepara a tabela tempor�ria --}
  cmdPreparaTemp.Execute;

  {-- popula a tabela tempor�ria com as parcelas --}
  try
      SPREL_POSICAO_CARTEIRAFINANC_IMOB_ANALITICA.Close;
      SPREL_POSICAO_CARTEIRAFINANC_IMOB_ANALITICA.Parameters.ParamByName('@nCdEmpresa').Value             := frmMenu.ConvInteiro(edtCdEmpresa.Text);
      SPREL_POSICAO_CARTEIRAFINANC_IMOB_ANALITICA.Parameters.ParamByName('@nCdEmpImobiliario').Value      := frmMenu.ConvInteiro(edtCdEmpreendimento.Text);
      SPREL_POSICAO_CARTEIRAFINANC_IMOB_ANALITICA.Parameters.ParamByName('@nCdBlocoEmpImobiliario').Value := frmMenu.ConvInteiro(edtBlocoTorre.Text);
      SPREL_POSICAO_CARTEIRAFINANC_IMOB_ANALITICA.Parameters.ParamByName('@nCdTerceiro').Value            := frmMenu.ConvInteiro(edtCdTerceiro.Text);
      SPREL_POSICAO_CARTEIRAFINANC_IMOB_ANALITICA.Parameters.ParamByName('@cDtVencInicial').Value         := frmMenu.ConvData(edtDtInicial.Text);
      SPREL_POSICAO_CARTEIRAFINANC_IMOB_ANALITICA.Parameters.ParamByName('@cDtVencFinal').Value           := frmMenu.ConvData(edtDtFinal.Text);
      SPREL_POSICAO_CARTEIRAFINANC_IMOB_ANALITICA.ExecProc;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  {-- verifica parcela a parcela para aplicar a corre��o monet�ria e os juros --}
  qryTempParcelas.Close;
  qryTempParcelas.Open;

  qryTempParcelas.First;

  objCorrecao := TfrmAtualizacaoMonetaria.Create( Self ) ;

  while not qryTempParcelas.Eof do
  begin

      try
      
          qryTempParcelas.Edit ;

          {-- corrigi o valor base da parcela --}

          {
          qryTempParcelasnValTit.Value := objCorrecao.atualizaIndiceParcela(Date()
                                                                           ,qryTempParcelasnCdTitulo.Value
                                                                           ,0) ;
          }

          {-- se estiver vencido aplica juros depois multa --}
          if (qryTempParcelasdDtVenc.Value < Date) then
          begin

              nValJuros := 0;
              nValMulta := 0;

              nValJuros := objCorrecao.atualizaJurosParcelaAtraso(Date()
                                                                 ,qryTempParcelasnCdTitulo.Value
                                                                 ,qryTempParcelasnValTit.Value); 

              {-- grava no campo nValJuroMulta o Valor SOMENTE DOS JUROS --}
              qryTempParcelasnValJuroMulta.Value := (nValJuros - qryTempParcelasnValTit.Value);

              {-- aqui a variavel nValJuros cont�m o Valor da Parcela + Corre��o Monet�ria + JUROS, e NAO SOMENTE o valor dos JUROS --}
              nValMulta := objCorrecao.calculaMulta(nValJuros);
              nValMulta := (nValMulta - nValJuros);

              {-- soma no campo nValJuroMulta SOMENTE O VALOR DA MULTA --}
              qryTempParcelasnValJuroMulta.Value := qryTempParcelasnValJuroMulta.Value + nValMulta ;

              qryTempParcelasnSaldoFinal.Value := qryTempParcelasnValTit.Value + qryTempParcelasnValJuroMulta.Value ;


          end ;

          qryTempParcelas.Post;

      except
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      qryTempParcelas.Next;
      
  end ;

  qryTempParcelas.First;

  freeAndNil( objCorrecao ) ;

  cFiltro := 'Filtros: ' ;

  if (DBEdit1.Text <> '') then
      cFiltro := cFiltro + ' / Empresa: ' + Trim( edtCdEmpresa.Text ) + '-' + Trim( DBEdit1.Text ) ;
       
  if (DBEdit2.Text <> '') then
      cFiltro := cFiltro + ' / Empreendimento: ' + Trim( edtCdEmpreendimento.Text ) + '-' + Trim( DBEdit2.Text ) ;

  if (DBEdit4.Text <> '') then
      cFiltro := cFiltro + ' / Bloco/Torre: ' + Trim( edtBlocoTorre.Text ) + '-' + Trim( DBEdit4.Text ) ;

  if (DBEdit3.Text <> '') then
      cFiltro := cFiltro + ' / Cliente: ' + Trim( edtCdTerceiro.Text ) + '-' + Trim( DBEdit3.Text ) ;

  if (Trim(edtDtInicial.Text) = '/  /') and (Trim(edtDtFinal.Text) = '/  /') then
      cFiltro := cFiltro + ' / Per�odo: TODAS PARCELAS' 
  else
  begin
      cFiltro := cFiltro + ' / Per�odo de ' + edtDtInicial.Text + ' at� ' + edtDtFinal.Text ;
  end ;

  {-- Resumido por Empreendimento --}
  if ( RadioGroup1.ItemIndex = 0) then
  begin

      qryPopulaTempResumoEmpreendimento.ExecSQL;

      objRelResumoEmp := TrptPosicaoFinancContratoImobiliario_ResumoEmpreendimento_view.Create( Self ) ;
      objRelResumoEmp.qryTempResumoEmpreendimento.Close;
      objRelResumoEmp.qryTempResumoEmpreendimento.Open;

      objRelResumoEmp.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
      objRelResumoEmp.lblFiltro.Caption  := cFiltro;

      objRelResumoEmp.QuickRep1.PreviewModal;

  end ;

  {-- Resumido por Contrato --}
  if ( RadioGroup1.ItemIndex = 1) then
  begin

      qryPopulaTempResumoContrato.ExecSQL;

      objRelResumoContrato := TrptPosicaoFinancContratoImobiliario_ResumoContrato_view.Create( Self ) ;
      objRelResumoContrato.qryTempResumoContrato.Close;
      objRelResumoContrato.qryTempResumoContrato.Open;

      objRelResumoContrato.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
      objRelResumoContrato.lblFiltro.Caption  := cFiltro;

      objRelResumoContrato.QuickRep1.PreviewModal;

  end ;

  {-- Di�rio --}
  if ( RadioGroup1.ItemIndex = 2) then
  begin

      objRelDiario := TrptPosicaoFinancContratoImobiliario_Diario_view.Create( Self );
      objRelDiario.qryTempParcelas.Close;
      objRelDiario.qryTempParcelas.Open;

      objRelDiario.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;
      objRelDiario.lblFiltro.Caption  := cFiltro;

      objRelDiario.QuickRep1.PreviewModal;

  end ;

end;

procedure TrptPosicaoFinancContratoImobiliario.edtCdEmpreendimentoExit(
  Sender: TObject);
begin
  inherited;

  qryBloco.Parameters.ParamByName('nCdEmpImobiliario').Value := frmMenu.ConvInteiro(edtCdEmpreendimento.Text) ;
  
end;

procedure TrptPosicaoFinancContratoImobiliario.edtBlocoTorreBeforeLookup(
  Sender: TObject);
begin
  inherited;

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Selecione um empreendimento imobili�rio.') ;
      edtCdEmpreendimento.SetFocus;
      abort;
  end ;

  edtBlocoTorre.WhereAdicional.Add('BlocoEmpImobiliario.nCdEmpImobiliario = ' + edtCdEmpreendimento.Text) ;

end;

initialization
    RegisterClass( TrptPosicaoFinancContratoImobiliario ) ;

end.
