inherited frmCargaInicial_ChequeTerceiro: TfrmCargaInicial_ChequeTerceiro
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'Cheque Terceiro - Carga Inicial'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1152
    Height = 785
  end
  inherited ToolBar1: TToolBar
    Width = 1152
    ButtonWidth = 107
    inherited ToolButton1: TToolButton
      Caption = 'Gravar Cheques'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 107
    end
    inherited ToolButton2: TToolButton
      Left = 115
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1152
    Height = 785
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsCheque
    Flat = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    ParentFont = False
    RowDetailPanel.Color = clBtnFace
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    OnColExit = DBGridEh1ColExit
    OnKeyUp = DBGridEh1KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdCheque'
        Footers = <>
        Width = 61
      end
      item
        EditButtons = <>
        FieldName = 'cLinha'
        Footers = <>
        Width = 179
      end
      item
        EditButtons = <>
        FieldName = 'nCdBanco'
        Footers = <>
        Width = 42
      end
      item
        EditButtons = <>
        FieldName = 'cAgencia'
        Footers = <>
        Width = 53
      end
      item
        EditButtons = <>
        FieldName = 'cConta'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cDigito'
        Footers = <>
        Width = 28
      end
      item
        EditButtons = <>
        FieldName = 'cCNPJCPF'
        Footers = <>
        Width = 72
      end
      item
        EditButtons = <>
        FieldName = 'iNrCheque'
        Footers = <>
        Width = 78
      end
      item
        EditButtons = <>
        FieldName = 'nValCheque'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'dDtDeposito'
        Footers = <>
        Width = 82
      end
      item
        EditButtons = <>
        FieldName = 'nCdTerceiroResp'
        Footers = <>
        Width = 32
      end
      item
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        ReadOnly = True
        Width = 190
      end
      item
        EditButtons = <>
        FieldName = 'nCdContaBancariaDep'
        Footers = <>
        Width = 33
      end
      item
        EditButtons = <>
        FieldName = 'cNmContaBancaria'
        Footers = <>
        Width = 190
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 352
    Top = 160
  end
  object cmdTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#Temp_Cheque'#39') IS NULL) '#13#10#9'CREATE TABLE #' +
      'Temp_Cheque (nCdCheque       int identity(1,1)'#13#10#9#9#9#9#9#9#9'  ,cLinha' +
      '          varchar(50)'#13#10#9#9#9#9#9#9#9'  ,nCdBanco        int'#13#10#9#9#9#9#9#9#9'  ,' +
      'cAgencia        char(4)'#13#10#9#9#9#9#9#9#9'  ,cConta          char(15)'#13#10#9#9#9 +
      #9#9#9#9'  ,cDigito         char(1)'#13#10#9#9#9#9#9#9#9'  ,cCNPJCPF        varcha' +
      'r(14)'#13#10#9#9#9#9#9#9#9'  ,iNrCheque       int'#13#10#9#9#9#9#9#9#9'  ,nValCheque      ' +
      'decimal(12,2) default 0 not null'#13#10#9#9#9#9#9#9#9'  ,dDtDeposito     date' +
      'time'#13#10#9#9#9#9#9#9#9'  ,nCdTerceiroResp int'#13#10',nCdContaBancariaDep int)'#13#10 +
      #13#10#13#10'TRUNCATE TABLE #Temp_Cheque'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 248
    Top = 120
  end
  object qryCheque: TADOQuery
    Connection = frmMenu.Connection
    OnCalcFields = qryChequeCalcFields
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM #Temp_Cheque')
    Left = 144
    Top = 192
    object qryChequenCdCheque: TAutoIncField
      DisplayLabel = 'ID'
      FieldName = 'nCdCheque'
      ReadOnly = True
    end
    object qryChequecLinha: TStringField
      DisplayLabel = 'Leitura M'#225'quina Cheque'
      FieldName = 'cLinha'
      Size = 50
    end
    object qryChequenCdBanco: TIntegerField
      DisplayLabel = 'Dados Emissor|Banco'
      FieldName = 'nCdBanco'
    end
    object qryChequecAgencia: TStringField
      DisplayLabel = 'Dados Emissor|Ag'#234'ncia'
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryChequecConta: TStringField
      DisplayLabel = 'Dados Emissor|N'#250'm. Conta'
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryChequecDigito: TStringField
      DisplayLabel = 'Dados Emissor|DV'
      FieldName = 'cDigito'
      FixedChar = True
      Size = 1
    end
    object qryChequecCNPJCPF: TStringField
      DisplayLabel = 'Dados Emissor|CNPJ/CPF'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequeiNrCheque: TIntegerField
      DisplayLabel = 'Dados do Cheque|N'#250'm. Cheque'
      FieldName = 'iNrCheque'
    end
    object qryChequenValCheque: TBCDField
      DisplayLabel = 'Dados do Cheque|Valor'
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryChequedDtDeposito: TDateTimeField
      DisplayLabel = 'Dados do Cheque|Dt. Dep'#243'sito'
      FieldName = 'dDtDeposito'
    end
    object qryChequenCdTerceiroResp: TIntegerField
      DisplayLabel = 'Cliente Respons'#225'vel|C'#243'd.'
      FieldName = 'nCdTerceiroResp'
    end
    object qryChequecNmTerceiro: TStringField
      DisplayLabel = 'Cliente Respons'#225'vel|Nome'
      FieldKind = fkCalculated
      FieldName = 'cNmTerceiro'
      Size = 50
      Calculated = True
    end
    object qryChequenCdContaBancariaDep: TIntegerField
      DisplayLabel = 'Conta Portadora|C'#243'd'
      FieldName = 'nCdContaBancariaDep'
    end
    object qryChequecNmContaBancaria: TStringField
      DisplayLabel = 'Conta Portadora|Nome Conta'
      FieldKind = fkCalculated
      FieldName = 'cNmContaBancaria'
      Size = 50
      Calculated = True
    end
  end
  object dsCheque: TDataSource
    DataSet = qryCheque
    Left = 176
    Top = 192
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmTerceiro'
      'FROM Terceiro'
      'WHERE nCdTerceiro = :nPK')
    Left = 496
    Top = 256
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object SP_CARGA_INICIAL_CHEQUE_TERCEIRO: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_CARGA_INICIAL_CHEQUE_TERCEIRO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 368
    Top = 200
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,CASE WHEN (cFlgCaixa = 1 OR cFlgCofre = 1) THEN nCdConta'
      
        '            ELSE (Convert(VARCHAR(5),nCdBanco) + '#39' - '#39' + Convert' +
        '(VARCHAR(5),cAgencia) + '#39' - '#39' + nCdConta + '#39' - '#39' + IsNull(cNmTit' +
        'ular,'#39' '#39'))'
      '       END nCdConta'
      '  FROM ContaBancaria'
      ' WHERE nCdContaBancaria = :nPK'
      '   AND ((cFlgCaixa = 1 OR cFlgCofre = 1))'
      '   AND nCdEmpresa       = :nCdEmpresa')
    Left = 520
    Top = 344
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdConta: TStringField
      FieldName = 'nCdConta'
      ReadOnly = True
      Size = 84
    end
  end
end
