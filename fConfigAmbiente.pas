unit fConfigAmbiente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, DB, StdCtrls, Mask, DBCtrls, ADODB,
  cxLookAndFeelPainters, cxButtons, IdMessage, IdBaseComponent,
  IdComponent, IdTCPConnection, IdTCPClient, IdMessageClient, IdSMTP,
  IdServerIOHandler, IdSSLOpenSSL, IdIOHandler, IdIOHandlerSocket, cxPC,
  cxControls, ExtDlgs, IdHashMessageDigest, Spin;

type
  TfrmConfigAmbiente = class(TForm)
    qryAmbiente: TADOQuery;
    dsAmbiente: TDataSource;
    qryTipoImpressoraPDV: TADOQuery;
    qryTipoImpressoraPDVnCdTipoImpressoraPDV: TIntegerField;
    qryTipoImpressoraPDVcNmTipoImpressoraPDV: TStringField;
    qryTipoImpressoraPDVcNomeDLL: TStringField;
    dsImpressoraPDV: TDataSource;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    qryAmbientecNmComputador: TStringField;
    qryAmbientecPortaMatricial: TStringField;
    qryAmbientecFlgUsaECF: TIntegerField;
    qryAmbientecModeloECF: TStringField;
    qryAmbientecPortaECF: TStringField;
    qryAmbientenCdTipoImpressoraPDV: TIntegerField;
    qryAmbienteiViasECF: TIntegerField;
    qryAmbientecPortaEtiqueta: TStringField;
    qryAmbientecFlgEmiteNFe: TIntegerField;
    qryAmbientecNrSerieCertificadoNFe: TStringField;
    qryAmbientedDtUltAcesso: TDateTimeField;
    qryAmbientecNmUsuarioUltAcesso: TStringField;
    qryAmbientecUFEmissaoNfe: TStringField;
    qryAmbientecPathLogoNFe: TStringField;
    qryAmbientenCdTipoAmbienteNFe: TIntegerField;
    qryAmbientenCdUFEmissaoNFe: TIntegerField;
    qryAmbientenCdMunicipioEmissaoNFe: TIntegerField;
    qryAmbientecPathArqNFeProd: TStringField;
    qryAmbientecPathArqNFeHom: TStringField;
    qryAmbientecServidorSMTP: TStringField;
    qryAmbientecEmail: TStringField;
    qryAmbientecSenhaEmail: TStringField;
    qryAmbientenPortaSMTP: TIntegerField;
    qryAmbientecFlgUsaSSL: TIntegerField;
    qryAmbientecTefDialArqLog: TStringField;
    qryAmbientecTefDialArqReq: TStringField;
    qryAmbientecTefDialArqResp: TStringField;
    qryAmbientecTefDialArqSTS: TStringField;
    qryAmbientecTefDialArqTemp: TStringField;
    qryAmbientecTefDialGPExeName: TStringField;
    qryAmbientecTefDialPatchBackup: TStringField;
    qryAmbientecTefDialAutoAtivarGP: TIntegerField;
    qryAmbienteiTefDialEsperaSTS: TIntegerField;
    qryAmbienteiTefDialNumVias: TIntegerField;
    qryAmbientecFlgTEFAtivo: TIntegerField;
    qryTipoImpressoraCaixa: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    StringField2: TStringField;
    dsTipoImpressoraCaixa: TDataSource;
    qryAmbientenCdTipoImpressoraPDVCaixa: TIntegerField;
    qryAmbientecNmImpCompartilhadaPDV: TStringField;
    qryAmbientecNmImpCompartilhadaEtiqueta: TStringField;
    qryAmbientecFlgUsaMapeamentoAutPDV: TIntegerField;
    qryAmbientecFlgUsaMapeamentoAutEtiqueta: TIntegerField;
    qryAmbientecFlgUsaGaveta: TIntegerField;
    qryTipoGavetaPDV: TADOQuery;
    dsTipoGavetaPDV: TDataSource;
    qryTipoGavetaPDVnCdTipoGavetaPDV: TIntegerField;
    qryTipoGavetaPDVcNmTipoGavetaPDV: TStringField;
    qryAmbientenCdTipoGavetaPDV: TIntegerField;
    qryAmbientecNmPortaGaveta: TStringField;
    cxPageControl1: TcxPageControl;
    tabPDV: TcxTabSheet;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    Label3: TLabel;
    Label29: TLabel;
    Label6: TLabel;
    Label26: TLabel;
    Label28: TLabel;
    DBEdit2: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    DBEdit25: TDBEdit;
    DBEdit5: TDBEdit;
    DBLookupComboBox2: TDBLookupComboBox;
    DBEdit26: TDBEdit;
    DBCheckBox6: TDBCheckBox;
    DBCheckBox7: TDBCheckBox;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    DBCheckBox1: TDBCheckBox;
    DBEdit3: TDBEdit;
    GroupBox4: TGroupBox;
    Label27: TLabel;
    Label30: TLabel;
    DBLookupComboBox3: TDBLookupComboBox;
    DBComboBox2: TDBComboBox;
    DBCheckBox8: TDBCheckBox;
    tabTEF: TcxTabSheet;
    Label16: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBCheckBox3: TDBCheckBox;
    DBCheckBox5: TDBCheckBox;
    tabNFeNFCe: TcxTabSheet;
    Label10: TLabel;
    DBCheckBox2: TDBCheckBox;
    DBEdit9: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    tabEmail: TcxTabSheet;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBCheckBox4: TDBCheckBox;
    GroupBox5: TGroupBox;
    btImgPDV: TcxButton;
    btImgCaixa: TcxButton;
    qryAmbientecPathLogoPDV: TStringField;
    qryAmbientecPathLogoCaixa: TStringField;
    Label31: TLabel;
    DBEdit27: TDBEdit;
    Panel1: TPanel;
    btGravar: TcxButton;
    btFechar: TcxButton;
    Label32: TLabel;
    DBEdit28: TDBEdit;
    OpenDialog: TOpenDialog;
    Label33: TLabel;
    Label34: TLabel;
    btResetaConfigECF: TcxButton;
    qryAux: TADOQuery;
    qryAmbienteiBaudECF: TIntegerField;
    Label35: TLabel;
    dbCmbVeloECF: TDBComboBox;
    btnVerificaConexao: TcxButton;
    rgTipoConta: TRadioGroup;
    tabCFe: TcxTabSheet;
    qryAmbientecFlgCFeAtivo: TIntegerField;
    qryAmbientecModoEnvioCFe: TStringField;
    qryAmbientecCdAtivacaoSAT: TStringField;
    qryAmbientecAssinaturaACSAT: TStringField;
    rgModoEnvioCFe: TDBRadioGroup;
    DBCheckBox9: TDBCheckBox;
    qryAmbientenCdUFEmissaoCFe: TIntegerField;
    qryAmbientecCNPJACSAT: TStringField;
    qryAmbientecFlgTipoAmbienteCFe: TIntegerField;
    qryAmbientecModeloSAT: TStringField;
    Label42: TLabel;
    qryAmbientenCdCodPDVSAT: TIntegerField;
    qryAmbientecFlgCodSeguranca: TIntegerField;
    qryVerificaNumCaixa: TADOQuery;
    qryVerificaNumCaixacNmComputador: TStringField;
    qryAmbientecFlgSATBloqueado: TIntegerField;
    qryAmbientecFlgTipoImpSAT: TIntegerField;
    qryAmbienteiLarguraImpSAT: TIntegerField;
    qryAmbienteiTopoImpSAT: TIntegerField;
    qryAmbienteiFundoImpSAT: TIntegerField;
    qryAmbienteiEsquerdoImpSAT: TIntegerField;
    qryAmbienteiDireitoImpSAT: TIntegerField;
    qryAmbientecFlgPreviewImpSAT: TIntegerField;
    qryAmbientecPortaImpSAT: TStringField;
    PrintDialog1: TPrintDialog;
    qryAmbientecNmImpSATDefault: TStringField;
    cxPageControl2: TcxPageControl;
    tabSATConfig: TcxTabSheet;
    Label44: TLabel;
    Label36: TLabel;
    Label38: TLabel;
    lblCNPJAC: TLabel;
    Label39: TLabel;
    Label37: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    DBEdit32: TDBEdit;
    DBEdit29: TDBEdit;
    dbeAssAC: TDBEdit;
    dbeCNPJAC: TDBEdit;
    GroupBox6: TGroupBox;
    Label45: TLabel;
    DBComboBox3: TDBComboBox;
    GroupBox7: TGroupBox;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    dbCheckPreviewImpSAT: TDBCheckBox;
    btnImpSATDefault: TcxButton;
    DBEdit33: TDBEdit;
    checkEscPOS: TCheckBox;
    edtConfCodigoAtivacaoSAT: TEdit;
    DBEdit31: TDBEdit;
    DBComboBox1: TDBComboBox;
    checkFortes: TCheckBox;
    DBRadioGroup2: TDBRadioGroup;
    tabSATRede: TcxTabSheet;
    rgRedeTipoInter: TRadioGroup;
    rgRedeTipoLan: TRadioGroup;
    gbWiFi: TGroupBox;
    Label53: TLabel;
    Label54: TLabel;
    Label52: TLabel;
    cmbWifiSeg: TComboBox;
    edtWifiSenha: TEdit;
    edtWifiSSID: TEdit;
    gbIPFix: TGroupBox;
    Label57: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    Label61: TLabel;
    edtIPFixIP: TEdit;
    edtIPFixMasc: TEdit;
    edtIPFixGateway: TEdit;
    edtIPFixDNS2: TEdit;
    edtIPFixDNS1: TEdit;
    gbProxy: TGroupBox;
    Label62: TLabel;
    Label63: TLabel;
    Label64: TLabel;
    Label65: TLabel;
    Label66: TLabel;
    cmbProxy: TComboBox;
    edtProxyIP: TEdit;
    edtProxyUsuario: TEdit;
    edtProxySenha: TEdit;
    mskProxyPorta: TMaskEdit;
    gbPPPoE: TGroupBox;
    Label55: TLabel;
    Label56: TLabel;
    edtPPPoEUsuario: TEdit;
    edtPPPoESenha: TEdit;
    btGerarXMLRede: TcxButton;
    btLerXMLRede: TcxButton;
    qryAmbientecFlgSATCompartilhado: TIntegerField;
    qryAmbientecArquivoXMLRede: TMemoField;
    SaveDialog1: TSaveDialog;
    tabSATCompartilhado: TcxTabSheet;
    gbDadosSAT: TGroupBox;
    rgServidorSat: TDBRadioGroup;
    qryServidorSAT: TADOQuery;
    dsServidorSAT: TDataSource;
    qryServidorSATnCdServidorSAT: TIntegerField;
    qryServidorSATcNmServidorSAT: TStringField;
    qryServidorSATnCdStatus: TIntegerField;
    qryServidorSATdDtCadastro: TDateTimeField;
    qryAmbientenCdServidorSAT: TIntegerField;
    Label67: TLabel;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    qryServidorSATcNmStatus: TStringField;
    DBEdit42: TDBEdit;
    btResetarConfigRedeSAT: TcxButton;
    Label43: TLabel;
    qryAmbientecPathLogoCFe: TStringField;
    qryAmbientecModeloImpSAT: TStringField;
    gbCertificadoDigital: TGroupBox;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    btSelCertificadoDigPadrao: TcxButton;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    Label9: TLabel;
    DBEdit8: TDBEdit;
    GroupBox8: TGroupBox;
    DBCheckBox10: TDBCheckBox;
    qryAmbientecFlgRemoto: TIntegerField;
    qryAmbientecIPServidor: TStringField;
    qryAmbienteiPortaServidor: TIntegerField;
    qryAmbientecPortaPinPad: TStringField;
    qryAmbientecIPSiTef: TStringField;
    qryAmbientecIDLojaTef: TStringField;
    qryAmbientecIDTerminalTef: TStringField;
    qryAmbientecMsgPinPad: TStringField;
    qryAmbientecNumSerieSAT: TStringField;
    qryAmbientecFlgWidePdvCaixa: TIntegerField;
    DBEdit34: TDBEdit;
    Label68: TLabel;
    qryAmbientecArquivoDllSAT: TStringField;
    Label71: TLabel;
    DBEdit43: TDBEdit;
    IdSSLIOHandlerSocket1: TIdSSLIOHandlerSocket;
    IdSMTP: TIdSMTP;
    DBCheckBox11: TDBCheckBox;
    DBEdit44: TDBEdit;
    Label72: TLabel;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    Label74: TLabel;
    Label75: TLabel;
    DBComboBox5: TDBComboBox;
    Label73: TLabel;
    qryAmbientecFlgUpdateER2: TIntegerField;
    qryAmbientecPageCodImpSAT: TStringField;
    qryAmbienteiLinhaImpSAT: TIntegerField;
    qryAmbienteiColunaImpSAT: TIntegerField;
    qryAmbienteiEspacoImpSAT: TIntegerField;
    qryAmbientecFlgImpUmaLinha: TIntegerField;
    qryAmbientecFlgServidorSAT: TIntegerField;
    rgSatCompartilhado: TDBRadioGroup;
    Image1: TImage;
    Label11: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit47: TDBEdit;
    Label12: TLabel;
    Label69: TLabel;
    Label70: TLabel;
    qryAmbientecCNPJTEF: TStringField;
    qryAmbientenPDVTEF: TIntegerField;
    qryAmbientecChaveAutTEF: TStringField;
    procedure FormShow(Sender: TObject);
    procedure btGravarClick(Sender: TObject);
    procedure qryAmbienteBeforePost(DataSet: TDataSet);
    procedure btFecharClick(Sender: TObject);
    procedure DBEdit4Exit(Sender: TObject);
    procedure btImgCaixaClick(Sender: TObject);
    procedure btImgPDVClick(Sender: TObject);
    procedure btResetaConfigECFClick(Sender: TObject);
    procedure rgTipoContaClick(Sender: TObject);
    procedure btnVerificaConexaoClick(Sender: TObject);
    procedure qryAmbienteAfterScroll(DataSet: TDataSet);
    procedure checkEscPOSClick(Sender: TObject);
    procedure checkFortesClick(Sender: TObject);
    procedure btnImpSATDefaultClick(Sender: TObject);
    function fnGeraXMLRedeSAT : WideString;
    procedure prLeituraXMLRedeSAT(bXMLExterno : Boolean);
    procedure btGerarXMLRedeClick(Sender: TObject);
    procedure btLerXMLRedeClick(Sender: TObject);
    procedure prAtivaCampo(objComponente : TComponent);
    procedure prDesativaCampo(objComponente : TComponent);
    procedure prConfigCamposRedeSAT();
    procedure rgRedeTipoInterClick(Sender: TObject);
    procedure rgRedeTipoLanClick(Sender: TObject);
    procedure cmbProxyChange(Sender: TObject);
    procedure DBEdit40Exit(Sender: TObject);
    procedure btSelCertificadoDigPadraoClick(Sender: TObject);
    procedure btResetarConfigRedeSATClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfigAmbiente: TfrmConfigAmbiente;
  cFlgTesteConexao : boolean;

implementation

uses
  fMenu, RLPrinters, Printers, pcnConversao, pcnRede, ACBrSAT, fEmiteNFE2,
  ACBrNFeConfiguracoes, fValidaCNPJ;

{$R *.dfm}

procedure TfrmConfigAmbiente.FormShow(Sender: TObject);
begin

    qryTipoImpressoraPDV.Open ;
    qryTipoImpressoraCaixa.Open;

    qryAmbiente.Close ;
    qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
    qryAmbiente.Open ;

    qryTipoGavetaPDV.Open;

    if (qryAmbiente.Eof) then
    begin
        qryAmbiente.Insert ;
        qryAmbientecNmComputador.Value := frmMenu.cNomeComputador;
        qryAmbiente.Post ;

        qryAmbiente.Close ;
        qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
        qryAmbiente.Open ;
    end ;

    cxPageControl1.ActivePage := tabPDV;

    case (qryAmbientecFlgSATCompartilhado.Value) of
        0 : cxPageControl2.ActivePage := tabSATConfig;
        1 : cxPageControl2.ActivePage := tabSATCompartilhado;
    end;

    prConfigCamposRedeSAT;

    prDesativaCampo(DBEdit33);
    prDesativaCampo(DBEdit41);
    prDesativaCampo(DBEdit42);
    prDesativaCampo(dbeAssAC);
    prDesativaCampo(dbeCNPJAC);

    checkFortes.Checked := False;
    checkEscPOS.Checked := False;

    case (qryAmbientecFlgTipoImpSAT.Value) of
        0 : checkFortes.Checked := True;
        1 : checkEscPOS.Checked := True;
    end;

    { -- se usu�rio master -- }
    if (frmMenu.cFlgUsuarioMaster = '1') then
    begin
        //lblCNPJAC.Visible := True;
        //dbeCNPJAC.Visible := True;
        prAtivaCampo(dbeAssAC);
    end;
end;

procedure TfrmConfigAmbiente.btGravarClick(Sender: TObject);
begin
    qryAmbiente.Edit;

    if (qryAmbiente.State in [dsEdit,dsInsert]) then
    begin
        qryAmbiente.Post ;
    end;

    qryAmbiente.Close ;
    qryAmbiente.Parameters.ParamByName('nPK').Value := frmMenu.cNomeComputador;
    qryAmbiente.Open ;

    if (qryAmbientecFlgUsaMapeamentoAutPDV.Value = 1) then
        frmMenu.fnMapeamentoImpressora(DBEdit2.Text,DBEdit1.Text,DBEdit25.Text);

    if (qryAmbientecFlgUsaMapeamentoAutEtiqueta .Value = 1) then
        frmMenu.fnMapeamentoImpressora(DBEdit5.Text,DBEdit1.Text,DBEdit26.Text);

end;

procedure TfrmConfigAmbiente.qryAmbienteBeforePost(DataSet: TDataSet);
var
  strExtensao : TStringList;
  ValidaCNPJ : TfrmValidaCNPJ;
label
  pulaValidacaoSAT;
begin
  strExtensao := TStringList.Create;

  strExtensao.Add('.JPG');
  strExtensao.Add('.JPEG');
  strExtensao.Add('.GIF');
  strExtensao.Add('.BMP');

  { -- elimina espa�amento autom�tico -- }
  qryAmbientecPathLogoPDV.Value   := StringReplace(qryAmbientecPathLogoPDV.Value,' ','',[rfReplaceAll]);
  qryAmbientecPathLogoCaixa.Value := StringReplace(qryAmbientecPathLogoCaixa.Value,' ','',[rfReplaceAll]);

  { -- valida extens�o da imagem -- }
  if ((Trim(qryAmbientecPathLogoPDV.Value) <> '') and (strExtensao.IndexOf(ExtractFileExt(qryAmbientecPathLogoPDV.Value)) = -1)) then
  begin
      frmMenu.MensagemErro('Extens�o do arquivo inv�lida.' + #13#13 + 'Extens�es Permitidas: *.JPG *.JPEG *.GIF *.BMP');
      DBEdit27.SetFocus;
      Abort;
  end;

  if ((Trim(qryAmbientecPathLogoCaixa.Value) <> '') and (strExtensao.IndexOf(ExtractFileExt(qryAmbientecPathLogoCaixa.Value)) = -1)) then
  begin
      frmMenu.MensagemErro('Extens�o do arquivo inv�lida.' + #13#13 + 'Extens�es Permitidas: *.JPG *.JPEG *.GIF *.BMP');
      DBEdit28.SetFocus;
      Abort;
  end;

  {if     (qryAmbientecFlgEmiteNFe.Value = 1)
     and (Trim(qryAmbientecNrSerieCertificadoNFe.Value) = '') then
  begin
      ShowMessage('Informe o n�mero de s�rie do certificado digital.') ;
      DBEdit4.SetFocus;
  end ;}

  { -- cupom fiscal eletr�nico ativo -- }
  if (qryAmbientecFlgCFeAtivo.Value = 1) then
  begin
      { -- padr�o SAT devido NFC-e estar em projeto piloto, liberar uso ap�s implementa��o da funcionalidade -- }
      qryAmbientecModoEnvioCFe.Value := 'S';

      { -- se sat esta em produ��o, for�a cnpj da er2 -- }
      if (qryAmbientecFlgTipoAmbienteCFe.Value = 0) then
          qryAmbientecCNPJACSAT.Value := '11864296000105';

      { -- se terminal esta config. como cliente, for�a uso do sat compartilhado -- }
      if (qryAmbientecFlgServidorSAT.Value = 0) then
          qryAmbientecFlgSATCompartilhado.Value := 1;

      if (rgModoEnvioCFe.ItemIndex = -1) then
      begin
          frmMenu.MensagemAlerta('Informe o modo de transmiss�o do cupom fiscal eletr�nico.');
          cxPageControl1.ActivePage := tabCFe;
          rgModoEnvioCFe.SetFocus;
          Abort;
      end;

      if ((qryAmbientenCdCodPDVSAT.Value < 1) or (qryAmbientenCdCodPDVSAT.Value > 999)) then
      begin
          frmMenu.MensagemAlerta('N�mero do caixa deve estar entre 1 a 999.');
          
          if (qryAmbientecFlgSATCompartilhado.Value = 0) then
          begin
              cxPageControl1.ActivePage := tabCFe;
              cxPageControl2.ActivePage := tabSATConfig;
              DBEdit32.SetFocus;
          end
          else
          begin
              cxPageControl1.ActivePage := tabCFe;
              cxPageControl2.ActivePage := tabSATCompartilhado;
              DBEdit43.SetFocus;
          end;

          Abort;
      end;

      { -- se utiliza sat compartilhado, pula valida��es pois a configura��o do equipamento -- }
      { -- � feita diretamente no terminal (servidor) que possui o sat instalado            -- }
      if (qryAmbientecFlgSATCompartilhado.Value = 1) then
          goto pulaValidacaoSAT;

      if (qryAmbientenCdUFEmissaoCFe.Value = 0) then
      begin
          frmMenu.MensagemAlerta('Informe o c�digo do estado IBGE.');
          cxPageControl1.ActivePage := tabCFe;
          cxPageControl2.ActivePage := tabSATConfig;
          DBEdit31.SetFocus;
          Abort;
      end;

      if (Trim(qryAmbientecArquivoDllSAT.Value) = '') then
      begin
          frmMenu.MensagemAlerta('Informe o nome do arquivo dll do SAT.');
          cxPageControl1.ActivePage := tabCFe;
          cxPageControl2.ActivePage := tabSATConfig;
          DBEdit34.SetFocus;
          Abort;
      end;

      if (Trim(qryAmbientecCdAtivacaoSAT.Value) = '') then
      begin
          frmMenu.MensagemAlerta('Informe o c�digo de ativa��o do SAT.');
          cxPageControl1.ActivePage := tabCFe;
          cxPageControl2.ActivePage := tabSATConfig;
          DBEdit29.SetFocus;
          Abort;
      end;

      if (Length(qryAmbientecCdAtivacaoSAT.Value) < 8) then
      begin
          frmMenu.MensagemAlerta('C�digo de ativa��o do SAT deve conter no m�nimo 8 caracteres.');
          cxPageControl1.ActivePage := tabCFe;
          cxPageControl2.ActivePage := tabSATConfig;
          DBEdit29.SetFocus;
          Abort;
      end;
      
      if (Trim(edtConfCodigoAtivacaoSAT.Text) = '') then
      begin
          frmMenu.MensagemAlerta('Informe a confirma��o do c�digo de ativa��o do SAT.');
          cxPageControl1.ActivePage := tabCFe;
          cxPageControl2.ActivePage := tabSATConfig;
          edtConfCodigoAtivacaoSAT.SetFocus;
          Abort;
      end;

      if (qryAmbientecCdAtivacaoSAT.Value <> edtConfCodigoAtivacaoSAT.Text) then
      begin
          frmMenu.MensagemAlerta('Confirma��o do c�digo de ativa��o do SAT n�o confere com o c�digo informado.');
          cxPageControl1.ActivePage := tabCFe;
          cxPageControl2.ActivePage := tabSATConfig;
          edtConfCodigoAtivacaoSAT.SetFocus;
          Abort;
      end;

      if (Trim(qryAmbientecAssinaturaACSAT.Value) <> '') and (Length(qryAmbientecAssinaturaACSAT.Value) < 344) then
      begin
          frmMenu.MensagemAlerta('C�digo de assinatura do SAT deve conter 344 caracteres.');
          cxPageControl1.ActivePage := tabCFe;
          cxPageControl2.ActivePage := tabSATConfig;
          dbeAssAC.SetFocus;
          //Abort;
      end;

      pulaValidacaoSAT:

      if (rgModoEnvioCFe.ItemIndex = -1) then
      begin
          frmMenu.MensagemAlerta('Informe o tipo de ambiente de emiss�o do cupom fiscal eletr�nico.');
          cxPageControl1.ActivePage := tabCFe;
          cxPageControl2.ActivePage := tabSATConfig;
          rgModoEnvioCFe.SetFocus;
          Abort;
      end;

      if (checkEscPOS.Checked) then
          qryAmbientecFlgTipoImpSAT.Value := 1  //EscPOS
      else
          qryAmbientecFlgTipoImpSAT.Value := 0; //Fortes Report

      if ((rgModoEnvioCFe.ItemIndex in [0,2]) and (qryAmbientecFlgEmiteNFe.Value = 0)) then
      begin
          frmMenu.MensagemAlerta('Para envio da NFC-e � necess�rio configurar o ambiente.');
          cxPageControl1.ActivePage := tabNFeNFCe;
          Abort;
      end;

      { -- sat compartilhado -- }
      if (qryAmbientecFlgSATCompartilhado.Value = 1) then
      begin
          { -- n�mero pdv deve ser �nico por sat -- }
          qryVerificaNumCaixa.Close;
          qryVerificaNumCaixa.Parameters.ParamByName('cNmComputador').Value  := qryAmbientecNmComputador.Value;
          qryVerificaNumCaixa.Parameters.ParamByName('nCdCodPDVSAT').Value   := qryAmbientenCdCodPDVSAT.Value;
          qryVerificaNumCaixa.Parameters.ParamByName('nCdServidorSAT').Value := qryAmbientenCdServidorSAT.Value;
          qryVerificaNumCaixa.Open;

          if (not qryVerificaNumCaixa.IsEmpty) then
          begin
              frmMenu.MensagemAlerta('N�mero do Caixa ' + qryAmbientenCdCodPDVSAT.AsString + ' j� esta em uso pelo workstation ' + qryVerificaNumCaixacNmComputador.Value + '.' + #13 + 'Para uso do SAT compartilhado, n�mero do caixa deve ser �nico por equipamento.');
              cxPageControl1.ActivePage := tabCFe;
              cxPageControl2.ActivePage := tabSATCompartilhado;
              DBEdit43.SetFocus;
              Abort;
          end;

          if (Trim(DBEdit41.Text) = '') then
          begin
              frmMenu.MensagemAlerta('Para uso do SAT compartilhado � necess�rio vincular o terminal ao servidor SAT.');
              cxPageControl1.ActivePage := tabCFe;
              cxPageControl2.ActivePage := tabSATCompartilhado;
              DBEdit40.SetFocus;
              Abort;
          end;
      end;

      qryAmbientecArquivoXMLRede.Value := fnGeraXMLRedeSAT;
  end;

  { -- Valida��o TEF Cappta --}
  if (qryAmbientecFlgTEFAtivo.Value = 1) then
  begin
    ValidaCNPJ := TfrmValidaCNPJ.Create(Self);
    if (qryAmbientenPDVTEF.Value <= 0) then
    begin
      frmMenu.MensagemAlerta('O N�mero do PDV configurado para TEF � inv�lido.');
      Abort;
    end;
    if not(ValidaCNPJ.isCNPJ(qryAmbientecCNPJTEF.Value)) then
    begin
      frmMenu.MensagemAlerta('O CNPJ configurado para TEF � inv�lido.');
      Abort;
    end;
   ValidaCNPJ.Free;
  end;

end;

procedure TfrmConfigAmbiente.btFecharClick(Sender: TObject);
begin
    qryAmbiente.Close;
    Close;
end;

procedure TfrmConfigAmbiente.DBEdit4Exit(Sender: TObject);
begin
  { -- elimina espa�amento autom�tico e caracter especial -- }
  qryAmbiente.Edit;
  qryAmbientecNrSerieCertificadoNFe.Value := StringReplace(qryAmbientecNrSerieCertificadoNFe.Value,' ','',[rfReplaceAll]);
  qryAmbientecNrSerieCertificadoNFe.Value := StringReplace(qryAmbientecNrSerieCertificadoNFe.Value,'?','',[rfReplaceAll]);
end;

procedure TfrmConfigAmbiente.btImgCaixaClick(Sender: TObject);
begin
  OpenDialog.FileName   := '';
  OpenDialog.Title      := 'Selecione o arquivo';
  OpenDialog.DefaultExt := '.JPG';
  OpenDialog.Filter     := 'Imagens (*.JPG)|*.JPG|Imagens (*.JPEG)|*.JPEG|Imagens (*.GIF)|*.GIF|Imagens (*.BMP)|*.BMP|Todos os Arquivos (*.*)|*.*';

  if (OpenDialog.Execute) then
  begin
      if (OpenDialog.FileName <> '') then
      begin
          qryAmbiente.Edit;
          qryAmbientecPathLogoCaixa.Value := OpenDialog.FileName;
      end;
  end;
end;

procedure TfrmConfigAmbiente.btImgPDVClick(Sender: TObject);
begin
  OpenDialog.FileName   := '';
  OpenDialog.Title      := 'Selecione o arquivo';
  OpenDialog.DefaultExt := '.JPG';
  OpenDialog.Filter     := 'Imagens (*.JPG)|*.JPG|Imagens (*.JPEG)|*.JPEG|Imagens (*.GIF)|*.GIF|Imagens (*.BMP)|*.BMP|Todos os Arquivos (*.*)|*.*';

  if (OpenDialog.Execute) then
  begin
      if (OpenDialog.FileName <> '') then
      begin
          qryAmbiente.Edit;
          qryAmbientecPathLogoPDV.Value := OpenDialog.FileName;
      end;
  end;
end;

procedure TfrmConfigAmbiente.btResetaConfigECFClick(Sender: TObject);
begin
    try
        qryAux.Close;
        qryAux.SQL.Clear;
        qryAux.SQL.Add('UPDATE ConfigAmbiente');
        qryAux.SQL.Add('   SET cModeloECF    = NULL');
        qryAux.SQL.Add('      ,cPortaECF     = NULL');
        qryAux.SQL.Add(' WHERE cNmComputador = ' + #39 + qryAmbientecNmComputador.Value + #39);
        qryAux.ExecSQL;

        frmMenu.ShowMessage('Modelo e Porta da ECF resetados com sucesso.');
    except
        frmMenu.MensagemAlerta('Erro no processamento.');

        Raise;
    end;
end;

procedure TfrmConfigAmbiente.rgTipoContaClick(Sender: TObject);
begin
  qryAmbiente.Edit;

  case (rgTipoConta.ItemIndex) of
      { -- pessoal -- }
      0 : begin
          qryAmbientecServidorSMTP.Value := '';
          qryAmbientenPortaSMTP.Value    := 587;
          qryAmbientecFlgUsaSSL.Value    := 0;
      end;
      { -- gmail -- }
      1 : begin
          qryAmbientecServidorSMTP.Value := 'smtp.gmail.com';
          qryAmbientenPortaSMTP.Value    := 587;
          qryAmbientecFlgUsaSSL.Value    := 01
      end;
      { -- hotmail -- }
      2 : begin
          qryAmbientecServidorSMTP.Value := 'smtp.live.com';
          qryAmbientenPortaSMTP.Value    := 587;
          qryAmbientecFlgUsaSSL.Value    := 1;
      end;
      { -- yahoo -- }
      3 : begin
          qryAmbientecServidorSMTP.Value := 'smtp.mail.yahoo.com.br';
          qryAmbientenPortaSMTP.Value    := 587;
          qryAmbientecFlgUsaSSL.Value    := 1;
      end;
  end;
end;

procedure TfrmConfigAmbiente.btnVerificaConexaoClick(Sender: TObject);
begin
  try
      { -- testa configura��o de e-mail -- }
      frmMenu.mensagemUsuario('Testando e-mail...');

      idSMTP.Host               := qryAmbientecServidorSMTP.Value;
      idSMTP.Port               := qryAmbientenPortaSMTP.Value;
      idSMTP.Username           := qryAmbientecEmail.Value;
      idSMTP.Password           := qryAmbientecSenhaEmail.Value;
      idSMTP.AuthenticationType := atLogin;

      if (DBCheckBox4.Checked) then
      begin
          idSMTP.IOHandler          := IdSSLIOHandlerSocket1;
          IdSSLIOHandlerSocket1.SSLOptions.Method := sslvTLSv1;
          IdSSLIOHandlerSocket1.PassThrough       := true;
          IdSSLIOHandlerSocket1.SSLOptions.Mode   := sslmClient;
      end;
      
      { -- envia o email -- }
      try
          idSMTP.Connect(0);
          if (DBCheckBox4.Checked) then
          begin
              idSMTP.SendCmd('STARTTLS', 220);
              IdSSLIOHandlerSocket1.PassThrough := false;
              idSMTP.Authenticate;
          end;
          frmMenu.ShowMessage('Teste de conex�o efetuado com sucesso.');
      except on E:Exception do
          frmMenu.MensagemErro('Erro ao efetuar teste de conex�o.' + #13#13 + 'Erro: ' + E.Message);
      end;
  finally
      frmMenu.mensagemUsuario('');
      if (idSMTP.Connected) then
          idSMTP.Disconnect;
  end;
end;

procedure TfrmConfigAmbiente.qryAmbienteAfterScroll(DataSet: TDataSet);
begin
  if (qryAmbiente.IsEmpty) then
      Exit;

  if (Trim(qryAmbientecCdAtivacaoSAT.Value) <> '') then
  begin
      edtConfCodigoAtivacaoSAT.Text := qryAmbientecCdAtivacaoSAT.Value;
  end;

  qryServidorSAT.Close;
  qryServidorSAT.Parameters.ParamByName('nPK').Value := qryAmbientenCdServidorSAT.Value;
  qryServidorSAT.Open;

  prLeituraXMLRedeSAT(False);
end;

procedure TfrmConfigAmbiente.checkEscPOSClick(Sender: TObject);
begin
  if (checkEscPOS.Checked) then
      checkFortes.Checked := False;
end;

procedure TfrmConfigAmbiente.checkFortesClick(Sender: TObject);
begin
  if (checkFortes.Checked) then
      checkEscPOS.Checked := False;
end;

procedure TfrmConfigAmbiente.btnImpSATDefaultClick(Sender: TObject);
begin
  if (PrintDialog1.Execute) then
  begin
      qryAmbiente.Edit;
      qryAmbientecNmImpSATDefault.Value := Printer.Printers[Printer.PrinterIndex];
  end;
end;

function TfrmConfigAmbiente.fnGeraXMLRedeSAT : WideString;
begin
  with (frmMenu.ACBrSAT1.Rede) do
  begin
      tipoInter   := TTipoInterface(rgRedeTipoInter.ItemIndex);
      SSID        := edtWifiSSID.Text;
      seg         := TSegSemFio(cmbWifiSeg.ItemIndex);
      codigo      := edtWifiSenha.Text;
      tipoLan     := TTipoLan(rgRedeTipoLan.ItemIndex);
      lanIP       := edtIPFixIP.Text;
      lanMask     := edtIPFixMasc.Text;
      lanGW       := edtIPFixGateway.Text;
      lanDNS1     := edtIPFixDNS1.Text;
      lanDNS2     := edtIPFixDNS2.Text;
      usuario     := edtPPPoEUsuario.Text;
      senha       := edtPPPoESenha.Text;
      proxy       := cmbProxy.ItemIndex;
      proxy_ip    := edtProxyIP.Text;
      proxy_porta := StrToIntDef(mskProxyPorta.Text,0);
      proxy_user  := edtProxyUsuario.Text;
      proxy_senha := edtProxySenha.Text;
  end;

  Result := frmMenu.ACBrSAT1.Rede.AsXMLString;
end;

procedure TfrmConfigAmbiente.prLeituraXMLRedeSAT(bXMLExterno : Boolean);
begin
  with (frmMenu.ACBrSAT1.Rede) do
  begin
      if (bXMLExterno) then
          LoadFromFile(OpenDialog.FileName)
      else
          SetXMLString(qryAmbientecArquivoXMLRede.Value);

      if (AsXMLString = '') then
          Exit;

      rgRedeTipoInter.ItemIndex := Integer(tipoInter);
      edtWifiSSID.Text          := SSID;
      cmbWifiSeg.ItemIndex      := Integer(seg);
      edtWifiSenha.Text         := codigo;
      rgRedeTipoLan.ItemIndex   := Integer(tipoLan);
      edtIPFixIP.Text           := lanIP;
      edtIPFixMasc.Text         := lanMask;
      edtIPFixGateway.Text      := lanGW;
      edtIPFixDNS1.Text         := lanDNS1;
      edtIPFixDNS2.Text         := lanDNS2;
      edtPPPoEUsuario.Text      := usuario;
      edtPPPoESenha.Text        := senha;
      cmbProxy.ItemIndex        := proxy;
      edtProxyIP.Text           := proxy_ip;
      mskProxyPorta.Text        := IntToStr(proxy_porta);
      edtProxyUsuario.Text      := proxy_user;
      edtProxySenha.Text        := proxy_senha;
  end;
end;

procedure TfrmConfigAmbiente.btGerarXMLRedeClick(Sender: TObject);
begin
  try
      fnGeraXMLRedeSAT;
      
      SaveDialog1.FileName := 'ER2ConfigRedeSAT.xml';
      SaveDialog1.Title    := 'Salvar arquivo xml de rede';
      SaveDialog1.Filter   := 'Arquivo XML (*.xml)|*.xml';

      if (SaveDialog1.Execute) then
      begin
         frmMenu.ACBrSAT1.Rede.SaveToFile(SaveDialog1.FileName);

         frmMenu.ShowMessage('Arquivo configura��o de rede gerado com sucesso.');
      end;
  except
      frmMenu.MensagemErro('Erro ao efetuar a gera��o do arquivo xml.');
      Raise;
  end;
end;

procedure TfrmConfigAmbiente.btLerXMLRedeClick(Sender: TObject);
begin
  try
      OpenDialog.FileName   := '';
      OpenDialog.Title      := 'Selecione o arquivo arquivo xml de rede';
      OpenDialog.DefaultExt := '.xml';
      OpenDialog.Filter     := 'Arquivo XML (*.xml)|*.xml';

      if (OpenDialog.Execute) then
      begin
          if (OpenDialog.FileName = '') then
              Exit;

          prLeituraXMLRedeSAT(True);

          frmMenu.ShowMessage('Arquivo de configura��o de rede importado com sucesso.');
      end;
  except
      prLeituraXMLRedeSAT(False);
      frmMenu.MensagemErro('Erro ao efetuar a leitura do arquivo xml.');
      Raise;
  end;
end;

procedure TfrmConfigAmbiente.prAtivaCampo(objComponente : TComponent);
begin
  if (objComponente is TDBEdit) then
  begin
      (objComponente as TDBEdit).ReadOnly   := False;
      (objComponente as TDBEdit).Color      := clWhite;
      (objComponente as TDBEdit).Font.Color := clBlack;
      (objComponente as TDBEdit).TabStop    := True;
      Exit;
  end;

  if (objComponente is TEdit) then
  begin
      (objComponente as TEdit).ReadOnly   := False;
      (objComponente as TEdit).Color      := clWhite;
      (objComponente as TEdit).Font.Color := clBlack;
      (objComponente as TEdit).TabStop    := True;
      Exit;
  end;

  if (objComponente is TMaskEdit) then
  begin
      (objComponente as TMaskEdit).ReadOnly   := False;
      (objComponente as TMaskEdit).Color      := clWhite;
      (objComponente as TMaskEdit).Font.Color := clBlack;
      (objComponente as TMaskEdit).TabStop    := True;
      Exit;
  end;
end;

procedure TfrmConfigAmbiente.prDesativaCampo(objComponente : TComponent);
begin
  if (objComponente is TDBEdit) then
  begin
      (objComponente as TDBEdit).ReadOnly   := True;
      (objComponente as TDBEdit).Color      := $00E9E4E4;
      (objComponente as TDBEdit).Font.Color := clBlack;
      (objComponente as TDBEdit).TabStop    := False;
      Exit;
  end;

  if (objComponente is TEdit) then
  begin
      (objComponente as TEdit).ReadOnly   := True;
      (objComponente as TEdit).Color      := $00E9E4E4;
      (objComponente as TEdit).Font.Color := clBlack;
      (objComponente as TEdit).TabStop    := False;
      Exit;
  end;

  if (objComponente is TMaskEdit) then
  begin
      (objComponente as TMaskEdit).ReadOnly   := True;
      (objComponente as TMaskEdit).Color      := $00E9E4E4;
      (objComponente as TMaskEdit).Font.Color := clBlack;
      (objComponente as TMaskEdit).TabStop    := False;
      Exit;
  end;
end;

procedure TfrmConfigAmbiente.prConfigCamposRedeSAT();
begin
  prDesativaCampo(rgRedeTipoInter);
  prDesativaCampo(edtWifiSSID);
  prDesativaCampo(cmbWifiSeg);
  prDesativaCampo(edtWifiSenha);
  prDesativaCampo(rgRedeTipoLan);
  prDesativaCampo(edtIPFixIP);
  prDesativaCampo(edtIPFixMasc);
  prDesativaCampo(edtIPFixGateway);
  prDesativaCampo(edtIPFixDNS1);
  prDesativaCampo(edtIPFixDNS2);
  prDesativaCampo(edtPPPoEUsuario);
  prDesativaCampo(edtPPPoESenha);
  prDesativaCampo(cmbProxy);
  prDesativaCampo(edtProxyIP);
  prDesativaCampo(mskProxyPorta);
  prDesativaCampo(edtProxyUsuario);
  prDesativaCampo(edtProxySenha);
  cmbWifiSeg.Enabled := False;

  case (rgRedeTipoInter.ItemIndex) of
      { -- Wifi -- }
      1 : begin
          prAtivaCampo(edtWifiSSID);
          prAtivaCampo(cmbWifiSeg);
          prAtivaCampo(edtWifiSenha);
          cmbWifiSeg.Enabled := True;
      end;
  end;

  case (rgRedeTipoLan.ItemIndex) of
      { -- PPPoE -- }
      1 : begin
          prAtivaCampo(edtPPPoEUsuario);
          prAtivaCampo(edtPPPoESenha);
      end;
      { -- IPFIX -- }
      2 : begin
          prAtivaCampo(edtIPFixIP);
          prAtivaCampo(edtIPFixMasc);
          prAtivaCampo(edtIPFixGateway);
          prAtivaCampo(edtIPFixDNS1);
          prAtivaCampo(edtIPFixDNS2);
      end;
  end;

  { -- Proxy-- }
  if (cmbProxy.ItemIndex > 0) then
  begin
      prAtivaCampo(edtProxyIP);
      prAtivaCampo(mskProxyPorta);
      prAtivaCampo(edtProxyUsuario);
      prAtivaCampo(edtProxySenha);
  end;
end;

procedure TfrmConfigAmbiente.rgRedeTipoInterClick(Sender: TObject);
begin
  case (rgRedeTipoInter.ItemIndex) of
      { -- Ethernet -- }
      0 : begin
          prDesativaCampo(edtWifiSSID);
          prDesativaCampo(cmbWifiSeg);
          prDesativaCampo(edtWifiSenha);
          cmbWifiSeg.Enabled := False;
      end;
      { -- Wifi -- }
      1 : begin
          prAtivaCampo(edtWifiSSID);
          prAtivaCampo(cmbWifiSeg);
          prAtivaCampo(edtWifiSenha);
          cmbWifiSeg.Enabled := True;
      end;
  end;
end;

procedure TfrmConfigAmbiente.rgRedeTipoLanClick(Sender: TObject);
begin
  case (rgRedeTipoLan.ItemIndex) of
      0 : begin
          prDesativaCampo(edtPPPoEUsuario);
          prDesativaCampo(edtPPPoESenha);
          prDesativaCampo(edtIPFixIP);
          prDesativaCampo(edtIPFixMasc);
          prDesativaCampo(edtIPFixGateway);
          prDesativaCampo(edtIPFixDNS1);
          prDesativaCampo(edtIPFixDNS2);
      end;
      { -- PPPoE -- }
      1 : begin
          prAtivaCampo(edtPPPoEUsuario);
          prAtivaCampo(edtPPPoESenha);
          prDesativaCampo(edtIPFixIP);
          prDesativaCampo(edtIPFixMasc);
          prDesativaCampo(edtIPFixGateway);
          prDesativaCampo(edtIPFixDNS1);
          prDesativaCampo(edtIPFixDNS2);
      end;
      { -- IPFIX -- }
      2 : begin
          prDesativaCampo(edtPPPoEUsuario);
          prDesativaCampo(edtPPPoESenha);
          prAtivaCampo(edtIPFixIP);
          prAtivaCampo(edtIPFixMasc);
          prAtivaCampo(edtIPFixGateway);
          prAtivaCampo(edtIPFixDNS1);
          prAtivaCampo(edtIPFixDNS2);
      end;
  end;
end;

procedure TfrmConfigAmbiente.cmbProxyChange(Sender: TObject);
begin
  { -- Proxy-- }
  if (cmbProxy.ItemIndex > 0) then
  begin
      prAtivaCampo(edtProxyIP);
      prAtivaCampo(mskProxyPorta);
      prAtivaCampo(edtProxyUsuario);
      prAtivaCampo(edtProxySenha);
  end
  else
  begin
      prDesativaCampo(edtProxyIP);
      prDesativaCampo(mskProxyPorta);
      prDesativaCampo(edtProxyUsuario);
      prDesativaCampo(edtProxySenha);
  end;
end;

procedure TfrmConfigAmbiente.DBEdit40Exit(Sender: TObject);
begin
  qryServidorSAT.Close;
  qryServidorSAT.Parameters.ParamByName('nPK').Value := qryAmbientenCdServidorSAT.Value;
  qryServidorSAT.Open;

  if (qryServidorSAT.IsEmpty) then
      qryAmbientenCdServidorSAT.Clear;
end;

procedure TfrmConfigAmbiente.btSelCertificadoDigPadraoClick(
  Sender: TObject);
var
  objNFe2 : TfrmEmiteNFE2;
begin
  try
      objNFe2 := TfrmEmiteNFe2.Create(Nil);

      objNFe2.prSelCertificadoDigital;

      if (objNFe2.cCertNumSerie = '') then
          Exit;
          
      qryAmbiente.Edit;
      qryAmbientecNrSerieCertificadoNFe.Value := objNFe2.cCertNumSerie;
      qryAmbientenCdMunicipioEmissaoNFe.Value := objNFe2.qryConsultaDadosCertnCdMunicipio.Value;
      qryAmbientenCdUFEmissaoNFe.Value        := objNFe2.qryConsultaDadosCertnCdEstado.Value;
      qryAmbientecUFEmissaoNfe.Value          := objNFe2.qryConsultaDadosCertcUF.Value;
  finally
      FreeAndNil(objNFe2);
  end;
end;

procedure TfrmConfigAmbiente.btResetarConfigRedeSATClick(Sender: TObject);
begin
  try
      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE ConfigAmbiente        ');
      qryAux.SQL.Add('   SET cArquivoXMLRede = NULL');
      qryAux.SQL.Add(' WHERE cNmComputador   = ' + #39 + qryAmbientecNmComputador.Value + #39);
      qryAux.ExecSQL;

      frmMenu.ShowMessage('Configura��es de rede SAT resetados com sucesso.');
  except
      frmMenu.MensagemErro('Erro no processamento.');
      Raise;
  end;
end;

end.
