inherited frmConsultaVendaDesconto: TfrmConsultaVendaDesconto
  Left = 210
  Top = 58
  Width = 1155
  Height = 660
  Caption = 'Consulta de Vendas com Desconto'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 161
    Width = 1139
    Height = 461
  end
  inherited ToolBar1: TToolBar
    Width = 1139
    ButtonWidth = 117
    inherited ToolButton1: TToolButton
      Caption = '&Executar Consulta'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 117
    end
    inherited ToolButton2: TToolButton
      Left = 125
    end
  end
  object GroupBox1: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1139
    Height = 132
    Align = alTop
    Caption = 'Filtros'
    TabOrder = 1
    object Label1: TLabel
      Tag = 1
      Left = 105
      Top = 24
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Tag = 1
      Left = 9
      Top = 49
      Width = 116
      Height = 13
      Alignment = taRightJustify
      Caption = 'Condi'#231#227'o de Pagamento'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Tag = 1
      Left = 36
      Top = 99
      Width = 89
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo de Vendas'
    end
    object Label4: TLabel
      Tag = 1
      Left = 208
      Top = 99
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label5: TLabel
      Tag = 1
      Left = 51
      Top = 70
      Width = 74
      Height = 13
      Alignment = taRightJustify
      Caption = '% de Desconto'
      FocusControl = DBEdit1
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 196
      Top = 16
      Width = 377
      Height = 21
      DataField = 'cNmLoja'
      DataSource = DataSource1
      TabOrder = 5
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 196
      Top = 41
      Width = 377
      Height = 21
      DataField = 'cNmCondPagto'
      DataSource = DataSource2
      TabOrder = 6
    end
    object edtLoja: TMaskEdit
      Left = 128
      Top = 16
      Width = 64
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 0
      Text = '      '
      OnExit = edtLojaExit
      OnKeyDown = edtLojaKeyDown
    end
    object edtCondPagto: TMaskEdit
      Left = 128
      Top = 41
      Width = 64
      Height = 21
      EditMask = '######;1; '
      MaxLength = 6
      TabOrder = 1
      Text = '      '
      OnExit = edtCondPagtoExit
      OnKeyDown = edtCondPagtoKeyDown
    end
    object MaskEdit2: TMaskEdit
      Left = 128
      Top = 91
      Width = 70
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
    object MaskEdit3: TMaskEdit
      Left = 232
      Top = 91
      Width = 69
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 4
      Text = '  /  /    '
    end
    object edtPercDesconto: TEdit
      Left = 128
      Top = 66
      Width = 65
      Height = 21
      TabOrder = 2
    end
  end
  object cxGrid2: TcxGrid [3]
    Left = 0
    Top = 161
    Width = 1139
    Height = 461
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    LookAndFeel.NativeStyle = True
    object cxGridDBTableView1: TcxGridDBTableView
      OnDblClick = cxGridDBTableView1DblClick
      DataController.DataSource = dsResultado
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTotal'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValDesconto
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValDevolucao
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValPedido
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValProdutos
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValPercDesconto
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsCustomize.ColumnGrouping = False
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.CellMultiSelect = True
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.FonteSomenteLeitura
      object cxGridDBTableView1nCdPedido: TcxGridDBColumn
        DataBinding.FieldName = 'nCdPedido'
        Width = 102
      end
      object cxGridDBTableView1cNmLoja: TcxGridDBColumn
        DataBinding.FieldName = 'cNmLoja'
        Width = 137
      end
      object cxGridDBTableView1dDtVenda: TcxGridDBColumn
        DataBinding.FieldName = 'dDtVenda'
        Width = 130
      end
      object cxGridDBTableView1cNmCondPagto: TcxGridDBColumn
        DataBinding.FieldName = 'cNmCondPagto'
        Width = 178
      end
      object cxGridDBTableView1cNmVendedor: TcxGridDBColumn
        DataBinding.FieldName = 'cNmVendedor'
        Width = 176
      end
      object cxGridDBTableView1nCdLanctoFin: TcxGridDBColumn
        DataBinding.FieldName = 'nCdLanctoFin'
        Visible = False
        Width = 74
      end
      object cxGridDBTableView1nValProdutos: TcxGridDBColumn
        DataBinding.FieldName = 'nValProdutos'
        Visible = False
        Options.Moving = False
        Width = 95
      end
      object cxGridDBTableView1nValPedido: TcxGridDBColumn
        DataBinding.FieldName = 'nValPedido'
        Options.Moving = False
        Width = 131
      end
      object cxGridDBTableView1nValDevolucao: TcxGridDBColumn
        DataBinding.FieldName = 'nValDevolucao'
        Options.Moving = False
        Width = 120
      end
      object cxGridDBTableView1nValDesconto: TcxGridDBColumn
        DataBinding.FieldName = 'nValDesconto'
        Options.Moving = False
        Width = 121
      end
      object cxGridDBTableView1nValPercDesconto: TcxGridDBColumn
        DataBinding.FieldName = 'nValPercDesconto'
        Options.Moving = False
        Width = 98
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  inherited ImageList1: TImageList
    Left = 464
    Top = 288
  end
  object qryResultado: TADOQuery
    Connection = frmMenu.Connection
    CommandTimeout = 55
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdCondPagto'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nValPercDesconto'
        DataType = ftFloat
        Size = 1
        Value = 0.000000000000000000
      end>
    SQL.Strings = (
      'DECLARE @nCdLoja          int'
      '       ,@nCdCondPagto     int'
      '       ,@dDtInicial       varchar(10)'
      '       ,@dDtFinal         varchar(10)'
      '       ,@nValPercDesconto decimal(12,2)'
      ''
      'SET @nCdLoja          = :nCdLoja'
      'SET @nCdCondPagto     = :nCdCondPagto'
      'SET @dDtInicial       = :dDtInicial'
      'SET @dDtFinal         = :dDtFinal'
      'SET @nValPercDesconto = :nValPercDesconto'
      ''
      'IF (OBJECT_ID('#39'tempdb..#Temp_VendasDesconto'#39') IS NULL)'
      'BEGIN'
      
        '    CREATE TABLE #Temp_VendasDesconto (nCdPedido        int     ' +
        '      default 0 not null'
      
        '                                      ,cNmLoja          varchar(' +
        '50)'
      '                                      ,dDtVenda         datetime'
      
        '                                      ,cNmCondPagto     varchar(' +
        '50)   '
      
        '                                      ,cNmVendedor      varchar(' +
        '50)  '
      
        '                                      ,nCdLanctoFin     int     ' +
        '      default 0 not null'
      
        '                                      ,nValProdutos     decimal(' +
        '12,2) default 0 not null'
      
        '                                      ,nValDesconto     decimal(' +
        '12,2) default 0 not null'
      
        '                                      ,nValPedido       decimal(' +
        '12,2) default 0 not null'
      
        '                                      ,nValDevolucao    decimal(' +
        '12,2) default 0 not null'
      
        '                                      ,nValPercDesconto decimal(' +
        '12,2) default 0 not null)'
      'END'
      ''
      'TRUNCATE TABLE #Temp_VendasDesconto'
      ''
      'INSERT INTO #Temp_VendasDesconto (nCdPedido'
      '                                 ,cNmLoja'
      '                                 ,dDtVenda'
      '                                 ,cNmCondPagto'
      '                                 ,cNmVendedor'
      '                                 ,nCdLanctoFin'
      '                                 ,nValProdutos'
      '                                 ,nValDesconto'
      '                                 ,nValPedido'
      '                                 ,nValDevolucao)'
      '    SELECT Pedido.nCdPedido'
      '          ,Loja.cNmLoja'
      '          ,dbo.fn_OnlyDate(LF.dDtLancto)'
      '          ,CP.cNmCondPagto'
      '          ,Usuario.cNmUsuario'
      '          ,Pedido.nCdLanctoFin'
      '          ,Pedido.nValProdutos'
      '          ,(SELECT Sum(nValDesconto)'
      '              FROM ItemPedido'
      '             WHERE nCdPedido = Pedido.nCdPedido) as nValDesconto'
      '          ,(SELECT Sum(nValTotalItem)'
      '              FROM ItemPedido'
      '             WHERE nCdPedido = Pedido.nCdPedido) as nValPedido'
      '          ,Pedido.nValDevoluc'
      '      FROM Pedido'
      
        '           INNER JOIN LanctoFin LF ON LF.nCdLanctoFin           ' +
        '     = Pedido.nCdLanctoFin'
      
        '           INNER JOIN CondPagto CP ON CP.nCdCondPagto           ' +
        '     = Pedido.nCdCondPagto'
      
        '           INNER JOIN Usuario      ON Usuario.nCdTerceiroRespons' +
        'avel = Pedido.nCdTerceiroColab'
      
        '           INNER JOIN Loja         ON Loja.nCdLoja              ' +
        '     = Pedido.nCdLoja'
      
        '     WHERE ((@nCdLoja          = 0)                             ' +
        '      OR (Pedido.nCdLoja      = @nCdLoja))'
      
        '       AND ((@nCdCondPagto     = 0)                             ' +
        '      OR (Pedido.nCdCondPagto = @nCdCondPagto))'
      
        '       AND ((Pedido.dDtPedido >=  Convert(DATETIME,@dDtInicial,1' +
        '03))  OR (@dDtInicial         = '#39'01/01/1900'#39'))'
      
        '       AND ((Pedido.dDtPedido  < (Convert(DATETIME,@dDtFinal,103' +
        ')+1)) OR (@dDtInicial         = '#39'01/01/1900'#39'))'
      ''
      ''
      '--'
      '-- Calcula a Porcentagem de desconto'
      '--'
      'UPDATE #Temp_VendasDesconto'
      '   SET nValPercDesconto = CASE WHEN nValDesconto > 0'
      '                                AND nValProdutos > 0 '
      
        '                               THEN Convert(DECIMAL(12,2),ROUND(' +
        '(nValDesconto/nValProdutos)*100,2))'
      '                               ELSE 0 '
      '                           END '
      ''
      'SELECT * '
      '  FROM #Temp_VendasDesconto'
      ' WHERE nValPercDesconto >= @nValPercDesconto'
      '   AND nValPercDesconto > 0')
    Left = 504
    Top = 288
    object qryResultadonCdPedido: TIntegerField
      DisplayLabel = 'N'#250'm. Pedido'
      FieldName = 'nCdPedido'
    end
    object qryResultadocNmLoja: TStringField
      DisplayLabel = 'Loja'
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryResultadodDtVenda: TDateTimeField
      DisplayLabel = 'Data da Venda'
      FieldName = 'dDtVenda'
    end
    object qryResultadocNmCondPagto: TStringField
      DisplayLabel = 'Condi'#231#227'o de Pagamento'
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryResultadocNmVendedor: TStringField
      DisplayLabel = 'Vendedor'
      FieldName = 'cNmVendedor'
      Size = 50
    end
    object qryResultadonCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryResultadonValProdutos: TBCDField
      FieldName = 'nValProdutos'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResultadonValDesconto: TBCDField
      DisplayLabel = 'Valor Desconto'
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResultadonValPedido: TBCDField
      DisplayLabel = 'Valor do Pedido'
      FieldName = 'nValPedido'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResultadonValPercDesconto: TBCDField
      DisplayLabel = '% Desconto'
      FieldName = 'nValPercDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryResultadonValDevolucao: TBCDField
      DisplayLabel = 'Valor Devolu'#231#227'o'
      FieldName = 'nValDevolucao'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsResultado: TDataSource
    DataSet = qryResultado
    Left = 504
    Top = 320
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK'
      '   AND EXISTS(SELECT 1'
      '                FROM UsuarioLoja UL'
      '               WHERE UL.nCdLoja = Loja.nCdLoja'
      '                 AND UL.nCdUsuario = :nCdUsuario)')
    Left = 544
    Top = 288
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryCondPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdLoja'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCondPagto'
      '      ,cNmCondPagto'
      '  FROM CondPagto'
      ' WHERE nCdCondPagto =:nPK'
      '   AND EXISTS (SELECT 1'
      '                 FROM LojaCondPagto'
      '                WHERE nCdLoja =:nCdLoja'
      
        '                  AND LojaCondPagto.nCdCondPagto = CondPagto.nCd' +
        'CondPagto)')
    Left = 592
    Top = 288
    object qryCondPagtonCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryCondPagtocNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLoja
    Left = 544
    Top = 320
  end
  object DataSource2: TDataSource
    DataSet = qryCondPagto
    Left = 592
    Top = 320
  end
end
