unit fImpCarnet;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, fMenu ;

type
  TfrmImpCarnet = class(TForm)
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function fImprimirCarnet (nCdCrediario : integer) : boolean ;
  end;

var
  frmImpCarnet: TfrmImpCarnet;


implementation

uses fCarnet_Rensz;

{$R *.dfm}


procedure TfrmImpCarnet.Button1Click(Sender: TObject);
var
    objCarnet : ICarnet ;
begin

    objCarnet := TCarnetRensz.Create;
    objCarnet.ImprimirCarnet(16713144) ;

end;

function TfrmImpCarnet.fImprimirCarnet (nCdCrediario : integer) : boolean ;
var
    objCarnet : ICarnet ;
begin

    objCarnet := TCarnetRensz.Create;
    Result := objCarnet.ImprimirCarnet(nCdCrediario) ;

end ;


end.
