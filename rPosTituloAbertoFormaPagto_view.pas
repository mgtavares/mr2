unit rPosTituloAbertoFormaPagto_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptPosTituloAbertoFormaPagto = class(TForm)
    QuickRep1: TQuickRep;
    usp_Relatorio: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape1: TQRShape;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRExpr3: TQRExpr;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    QRExpr6: TQRExpr;
    QRExpr7: TQRExpr;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRShape4: TQRShape;
    usp_RelatorionCdTitulo: TIntegerField;
    usp_RelatorionCdEspTit: TIntegerField;
    usp_RelatoriocNmEspTit: TStringField;
    usp_RelatorionCdSP: TIntegerField;
    usp_RelatoriocNrTit: TStringField;
    usp_RelatorionCdTerceiro: TIntegerField;
    usp_RelatoriocNmTerceiro: TStringField;
    usp_RelatorionCdMoeda: TIntegerField;
    usp_RelatoriocSigla: TStringField;
    usp_RelatoriocSenso: TStringField;
    usp_RelatoriodDtVenc: TDateTimeField;
    usp_RelatorionValTit: TFloatField;
    usp_RelatorionSaldoTit: TFloatField;
    usp_RelatoriodDtCancel: TDateTimeField;
    usp_RelatorionValJuro: TFloatField;
    usp_RelatorioiDias: TIntegerField;
    usp_RelatorionCdBancoPortador: TIntegerField;
    usp_RelatorionValVencido: TFloatField;
    usp_RelatorionValVencer: TFloatField;
    usp_RelatoriocFlgPrev: TIntegerField;
    usp_RelatorionCdFormaPagtoTit: TIntegerField;
    usp_RelatorionCdFormaPagto: TIntegerField;
    usp_RelatoriocNmFormaPagto: TStringField;
    QRLabel22: TQRLabel;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    usp_RelatoriodDtEmissao: TDateTimeField;
    QRLabel27: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel35: TQRLabel;
    QRLabel37: TQRLabel;
    QRLabel38: TQRLabel;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRLabel41: TQRLabel;
    QRLabel43: TQRLabel;
    usp_RelatorioiParcela: TIntegerField;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRExpr8: TQRExpr;
    QRDBText29: TQRDBText;
    QRExpr9: TQRExpr;
    QRExpr10: TQRExpr;
    QRDBText1: TQRDBText;
    QRShape7: TQRShape;
    QRLabel4: TQRLabel;
    QRDBText2: TQRDBText;
    usp_RelatorionCdLojaTit: TIntegerField;
    QRDBText3: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText16: TQRDBText;
    QRLabel1: TQRLabel;
    usp_RelatorionCdEmpresa: TIntegerField;
    usp_RelatoriocNrNf: TStringField;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRImage2: TQRImage;
    QRShape6: TQRShape;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPosTituloAbertoFormaPagto: TrptPosTituloAbertoFormaPagto;

implementation

{$R *.dfm}

end.
