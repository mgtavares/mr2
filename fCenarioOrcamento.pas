unit fCenarioOrcamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, ER2Lookup, cxLookAndFeelPainters,
  cxButtons, DBGridEhGrouping, GridsEh, DBGridEh, cxPC, cxControls;

type
  TfrmCenarioOrcamento = class(TfrmCadastro_Padrao)
    qryMasternCdOrcamento: TIntegerField;
    qryMastercNmOrcamento: TStringField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdOrcamentoPai: TIntegerField;
    qryMasternPercentInicio: TBCDField;
    qryMasternValOrcamento: TBCDField;
    qryMasteriAnoBase: TIntegerField;
    qryMasternCdCC: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit12: TDBEdit;
    DataSource1: TDataSource;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    DBEdit13: TDBEdit;
    DataSource2: TDataSource;
    qryOrcamentoBase: TADOQuery;
    qryOrcamentoBasenCdOrcamento: TIntegerField;
    qryOrcamentoBasecNmOrcamento: TStringField;
    DBEdit14: TDBEdit;
    DataSource3: TDataSource;
    qryMesInicial: TADOQuery;
    qryMesInicialnCdTabTipoMes: TIntegerField;
    qryMesInicialcNmTabTipoMes: TStringField;
    DBEdit15: TDBEdit;
    DataSource4: TDataSource;
    qryMesFinal: TADOQuery;
    qryMesFinalnCdTabTipoMes: TIntegerField;
    qryMesFinalcNmTabTipoMes: TStringField;
    DBEdit16: TDBEdit;
    DataSource5: TDataSource;
    qryCentroCusto: TADOQuery;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocCdCC: TStringField;
    qryCentroCustocNmCC: TStringField;
    DBEdit17: TDBEdit;
    DataSource6: TDataSource;
    DBEdit18: TDBEdit;
    edtCdEmpresa: TER2LookupDBEdit;
    edtCdLoja: TER2LookupDBEdit;
    edtCdOrcamentoPai: TER2LookupDBEdit;
    edtCdMesInicial: TER2LookupDBEdit;
    edtCdMesFinal: TER2LookupDBEdit;
    edtCdCC: TER2LookupDBEdit;
    qryMasternCdTabTipoMesIni: TIntegerField;
    qryMasternCdTabTipoMesFim: TIntegerField;
    qryOrcamentoBaseiAnoBase: TIntegerField;
    qryOrcamentoBasenCdTabTipoMesIni: TIntegerField;
    qryOrcamentoBasenCdTabTipoMesFim: TIntegerField;
    qryOrcamentoBasenCdEmpresa: TIntegerField;
    qryMastercFlgDistribIniciada: TIntegerField;
    cxButton1: TcxButton;
    qryPreparaTemp: TADOQuery;
    SP_EXCLUI_CONTA_SEM_ORCAMENTO: TADOStoredProc;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioOrcamento: TADOQuery;
    dsUsuarioOrcamento: TDataSource;
    qryUsuarioOrcamentonCdUsuarioOrcamento: TIntegerField;
    qryUsuarioOrcamentonCdOrcamento: TIntegerField;
    qryUsuarioOrcamentonCdUsuario: TIntegerField;
    qryUsuarioOrcamentocNmUsuario: TStringField;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    procedure btIncluirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryOrcamentoBaseAfterClose(DataSet: TDataSet);
    procedure qryOrcamentoBaseAfterScroll(DataSet: TDataSet);
    procedure edtCdOrcamentoPaiExit(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure distribuirOrcamento( nCdOrcamento : integer);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryUsuarioOrcamentoCalcFields(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryUsuarioOrcamentoBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
    bInsert : boolean ;
  public
    { Public declarations }
  end;

var
  frmCenarioOrcamento: TfrmCenarioOrcamento;

implementation

uses fMenu, fDistribuicaoOrcamento, fLookup_Padrao;

{$R *.dfm}

procedure TfrmCenarioOrcamento.btIncluirClick(Sender: TObject);
begin
  inherited;

  ativaDBEDit( edtCdOrcamentoPai ) ;
  ativaDBEDit( DBEdit8 ) ;
  ativaDBEDit( edtCdMesInicial ) ;
  ativaDBEDit( edtCdMesFinal ) ;
  
  DBEdit2.SetFocus;
  
end;

procedure TfrmCenarioOrcamento.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'ORCAMENTO' ;
  nCdTabelaSistema  := 428 ;
  nCdConsultaPadrao := 210 ;
  bLimpaAposSalvar  := False ;

end;

procedure TfrmCenarioOrcamento.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  qryLoja.Close;
  qryOrcamentoBase.Close;
  qryMesInicial.Close;
  qryMesFinal.Close;
  qryCentroCusto.Close;
  qryUsuarioOrcamento.Close;

  ativaDBEDit( edtCdOrcamentoPai ) ;
  ativaDBEDit( DBEdit8 ) ;
  ativaDBEDit( edtCdMesInicial ) ;
  ativaDBEDit( edtCdMesFinal ) ;
  
end;

procedure TfrmCenarioOrcamento.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  qryLoja.Close;
  qryOrcamentoBase.Close;
  qryMesInicial.Close;
  qryMesFinal.Close;
  qryCentroCusto.Close;
  qryUsuarioOrcamento.Close;

  PosicionaQuery( qryEmpresa         , qryMasternCdEmpresa.asString) ;
  PosicionaQuery( qryLoja            , qryMasternCdLoja.asString) ;
  PosicionaQuery( qryOrcamentoBase   , qryMasternCdOrcamentoPai.asString) ;
  PosicionaQuery( qryMesInicial      , qryMasternCdTabTipoMesIni.asString) ;
  PosicionaQuery( qryMesFinal        , qryMasternCdTabTipoMesFim.asString) ;
  PosicionaQuery( qryCentroCusto     , qryMasternCdCC.asString) ;
  PosicionaQuery( qryUsuarioOrcamento, qryMasternCdOrcamento.asString);

  desativaDBEDit( DBEdit6 ) ;
  desativaDBEDit( edtCdOrcamentoPai ) ;
  desativaDBEDit( DBEdit8 ) ;
  desativaDBEDit( edtCdMesInicial ) ;
  desativaDBEDit( edtCdMesFinal ) ;

end;

procedure TfrmCenarioOrcamento.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      desativaDBEdit( edtCdLoja ) ;

end;

procedure TfrmCenarioOrcamento.qryMasterBeforePost(DataSet: TDataSet);
begin

  bInsert := (qryMaster.State = dsInsert) ;
  
  if (Trim(qryMastercNmOrcamento.Value) = '') then
  begin
      MensagemAlerta('Informe a descri��o do or�amento.') ;
      DBEdit2.SetFocus;
      abort ;
  end ;

  if (DBEdit12.Text = '') then
  begin
      MensagemAlerta('Informe a empresa.') ;
      edtCdEmpresa.SetFocus;
      abort ;
  end ;

  if (DBEdit8.ReadOnly) and (DBEdit14.Text <> '') then
  begin

      if (qryMasternCdEmpresa.Value <> qryOrcamentoBasenCdEmpresa.Value) then
      begin
          MensagemAlerta('O or�amento base selecionado n�o pertence a empresa informada neste or�amento.') ;
          edtCdEmpresa.SetFocus;
          abort ;
      end ;

      qryMasteriAnoBase.Value         := qryOrcamentoBaseiAnoBase.Value ;
      qryMasternCdTabTipoMesIni.Value := qryOrcamentoBasenCdTabTipoMesIni.Value ;
      qryMasternCdTabTipoMesFim.Value := qryOrcamentoBasenCdTabTipoMesFim.Value ;

  end ;

  if (qryMasteriAnoBase.Value > 0) and (not DBEdit8.ReadOnly) then
  begin

      if (DBEdit15.Text = '') then
      begin
          MensagemAlerta('Informe o m�s inicial.') ;
          edtCdMesInicial.SetFocus;
          abort ;
      end ;

      if (DBEdit16.Text = '') then
      begin
          MensagemAlerta('Informe o m�s final.') ;
          edtCdMesFinal.SetFocus;
          abort ;
      end ;

      if (qryMasternCdTabTipoMesIni.Value > qryMasternCdTabTipoMesFim.Value) then
      begin
          MensagemAlerta('O M�s final n�o pode ser menor que o m�s inicial.') ;
          edtCdMesInicial.SetFocus;
          abort ;
      end ;

  end ;

  inherited;


end;

procedure TfrmCenarioOrcamento.qryOrcamentoBaseAfterClose(
  DataSet: TDataSet);
begin
  inherited;

  desativaDBEdit ( DBEdit6 ) ;

  ativaDBEdit ( DBEdit8 ) ;
  ativaDBEdit ( edtCdMesInicial ) ;
  ativaDBEdit ( edtCdMesFinal ) ;

end;

procedure TfrmCenarioOrcamento.qryOrcamentoBaseAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  ativaDBEdit ( DBEdit6 ) ;

  desativaDBEdit ( DBEdit8 ) ;
  desativaDBEdit ( edtCdMesInicial ) ;
  desativaDBEdit ( edtCdMesFinal ) ;

end;

procedure TfrmCenarioOrcamento.edtCdOrcamentoPaiExit(Sender: TObject);
begin
  inherited;

  if (DBEdit14.Text <> '') then
      DBEdit6.SetFocus
  else DBEdit8.SetFocus;
  
end;

procedure TfrmCenarioOrcamento.cxButton1Click(Sender: TObject);
begin

    if (not qryMaster.Active) then
    begin
        MensagemAlerta('Nenhum or�amento selecionado.') ;
        abort ;
    end ;

    if (qryMaster.State in [dsEdit,dsInsert]) then
        qryMaster.Post ;

    distribuirOrcamento( qryMasternCdOrcamento.Value ) ;

end;

procedure TfrmCenarioOrcamento.distribuirOrcamento(nCdOrcamento: integer);
var
    objForm : TfrmDistribuicaoOrcamento ;
begin

    objForm := TfrmDistribuicaoOrcamento.Create( Self ) ;

    qryPreparaTemp.ExecSQL;

    frmMenu.Connection.BeginTrans;

    try
        objForm.SP_CARGA_INICIAL_DISTRIB_ORCAMENTARIA.Close;
        objForm.SP_CARGA_INICIAL_DISTRIB_ORCAMENTARIA.Parameters.ParamByName('@nCdOrcamento').Value     := qryMasternCdOrcamento.Value ;
        objForm.SP_CARGA_INICIAL_DISTRIB_ORCAMENTARIA.Parameters.ParamByName('@cFlgComplementar').Value := 'N' ;
        objForm.SP_CARGA_INICIAL_DISTRIB_ORCAMENTARIA.ExecProc;
    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento.') ;
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

    objForm.qryPopulaTemp.Parameters.ParamByName('nPK').Value := qryMasternCdOrcamento.Value ;
    objForm.qryPopulaTemp.ExecSQL;

    objForm.qryMetodoDistribOrcamento.Open;

    objForm.qryTemp.Close;
    objForm.qryTemp.Open;

    objForm.nCdOrcamento := qryMasternCdOrcamento.Value ;
    
    showForm( objForm , TRUE ) ;

    frmMenu.Connection.BeginTrans;

    try
        SP_EXCLUI_CONTA_SEM_ORCAMENTO.Close;
        SP_EXCLUI_CONTA_SEM_ORCAMENTO.Parameters.ParamByName('@nCdOrcamento').Value := qryMasternCdOrcamento.Value ;
        SP_EXCLUI_CONTA_SEM_ORCAMENTO.ExecProc;
    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento.') ;
        abort ;
    end ;

    frmMenu.Connection.CommitTrans;



end;

procedure TfrmCenarioOrcamento.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  if (bInsert) then
  begin
      distribuirOrcamento( qryMasternCdOrcamento.Value ) ;

      qryUsuarioOrcamento.Close;
      PosicionaQuery( qryUsuarioOrcamento, qryMasternCdOrcamento.asString);
      
      abort ;
  end ;

end;

procedure TfrmCenarioOrcamento.qryUsuarioOrcamentoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryUsuario.Close;
  PosicionaQuery(qryUsuario, qryUsuarioOrcamentonCdUsuario.AsString);

  if not(qryUsuario.Eof) then
      qryUsuarioOrcamentocNmUsuario.Value := qryUsuariocNmUsuario.Value;
      
end;

procedure TfrmCenarioOrcamento.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;
end;

procedure TfrmCenarioOrcamento.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioOrcamento.State = dsBrowse) then
             qryUsuarioOrcamento.Edit ;

        if (qryUsuarioOrcamento.State = dsInsert) or (qryUsuarioOrcamento.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioOrcamentonCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
  
end;

procedure TfrmCenarioOrcamento.qryUsuarioOrcamentoBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  qryUsuarioOrcamentonCdUsuarioOrcamento.Value := frmMenu.fnProximoCodigo('USUARIOORCAMENTO');
  qryUsuarioOrcamentonCdOrcamento.Value        := qryMasternCdOrcamento.Value;

end;

initialization
    RegisterClass( TfrmCenarioOrcamento ) ;
    
end.
