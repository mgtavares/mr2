unit rConsultaSitCadastralCliente_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, QRCtrls, jpeg, DB, ADODB;

type
  TrptConsultaSitCadastralCliente_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel5: TQRLabel;
    QRBand2: TQRBand;
    QRShape2: TQRShape;
    qryGeraRelatorio: TADOQuery;
    QRLabel4: TQRLabel;
    QRBand5: TQRBand;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRDBText2: TQRDBText;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText7: TQRDBText;
    qryGeraRelatorionCdLoja: TStringField;
    qryGeraRelatoriocNmLoja: TStringField;
    qryGeraRelatorionCdTerceiro: TIntegerField;
    qryGeraRelatoriocNmTerceiro: TStringField;
    qryGeraRelatoriocCNPJCPF: TStringField;
    qryGeraRelatoriodDtCadastro: TStringField;
    qryGeraRelatoriocNmTabTipoSituacao: TStringField;
    QRDBText6: TQRDBText;
    QRLabel6: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptConsultaSitCadastralCliente_View: TrptConsultaSitCadastralCliente_View;

implementation

uses
 fMenu;

{$R *.dfm}

end.
