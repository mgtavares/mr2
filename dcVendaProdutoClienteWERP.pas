unit dcVendaProdutoClienteWERP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, Mask, StdCtrls, DBCtrls, ImgList,
  ComCtrls, ToolWin, ExtCtrls;

type
  TdcmVendaProdutoClienteWERP = class(TfrmProcesso_Padrao)
    Label1: TLabel;
    Label5: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    MaskEdit6: TMaskEdit;
    MaskEdit5: TMaskEdit;
    DBEdit4: TDBEdit;
    MaskEdit7: TMaskEdit;
    DBEdit5: TDBEdit;
    MaskEdit8: TMaskEdit;
    DBEdit6: TDBEdit;
    MaskEdit9: TMaskEdit;
    DBEdit7: TDBEdit;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    qryGrupoEconomico: TADOQuery;
    qryGrupoEconomiconCdGrupoEconomico: TIntegerField;
    qryGrupoEconomicocNmGrupoEconomico: TStringField;
    dsGrupoEconomico: TDataSource;
    qryDepartamento: TADOQuery;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryMarca: TADOQuery;
    qryMarcanCdMarca: TIntegerField;
    qryMarcacNmMarca: TStringField;
    qryLinha: TADOQuery;
    qryLinhanCdLinha: TAutoIncField;
    qryLinhacNmLinha: TStringField;
    qryClasseProduto: TADOQuery;
    qryClasseProdutonCdClasseProduto: TAutoIncField;
    qryClasseProdutocNmClasseProduto: TStringField;
    dsDepartamento: TDataSource;
    dsMarca: TDataSource;
    dsLinha: TDataSource;
    dsClasseProduto: TDataSource;
    qryRamoAtividade: TADOQuery;
    qryRamoAtividadenCdRamoAtividade: TIntegerField;
    qryRamoAtividadecNmRamoAtividade: TStringField;
    DataSource6: TDataSource;
    qryTerceiroRepres: TADOQuery;
    qryTerceiroRepresnCdTerceiro: TIntegerField;
    qryTerceiroReprescNmTerceiro: TStringField;
    DataSource7: TDataSource;
    qryEmpresa: TADOQuery;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    procedure FormShow(Sender: TObject);
    procedure MaskEdit5Exit(Sender: TObject);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure MaskEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit8Exit(Sender: TObject);
    procedure MaskEdit9Exit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmVendaProdutoClienteWERP: TdcmVendaProdutoClienteWERP;

implementation

uses fMenu, fLookup_Padrao, dcVendaProdutoCliente_view;

{$R *.dfm}

procedure TdcmVendaProdutoClienteWERP.FormShow(Sender: TObject);
begin
  inherited;

  qryEmpresa.Close ;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryEmpresa.Open ;

  MaskEdit3.Text := IntToStr(frmMenu.nCdEmpresaAtiva) ;
  Maskedit6.Text := intToStr(frmMenu.nCdTerceiroUsuario) ;

  qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit6.Text ;
  qryTerceiro.Open ;

  MaskEdit5.SetFocus ;

end;

procedure TdcmVendaProdutoClienteWERP.MaskEdit5Exit(Sender: TObject);
begin
  inherited;
  qryDepartamento.Close ;

  If (Trim(MaskEdit5.Text) <> '') then
  begin

    PosicionaQuery(qryDepartamento, Maskedit5.Text) ;

  end ;


end;

procedure TdcmVendaProdutoClienteWERP.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryMarca.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin

    PosicionaQuery(qryMarca, Maskedit7.Text) ;

  end ;

end;

procedure TdcmVendaProdutoClienteWERP.MaskEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(45);

        If (nPK > 0) then
        begin
            Maskedit5.Text := IntToStr(nPK) ;
            PosicionaQuery(qryDepartamento, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmVendaProdutoClienteWERP.MaskEdit7KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(49);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
            PosicionaQuery(qryMarca, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmVendaProdutoClienteWERP.MaskEdit8KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(50);

        If (nPK > 0) then
        begin
            Maskedit8.Text := IntToStr(nPK) ;
            PosicionaQuery(qryLinha, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmVendaProdutoClienteWERP.MaskEdit9KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(53);

        If (nPK > 0) then
        begin
            Maskedit9.Text := IntToStr(nPK) ;
            PosicionaQuery(qryClasseProduto, IntToStr(nPK));
        end ;

    end ;

  end ;

end;

procedure TdcmVendaProdutoClienteWERP.MaskEdit8Exit(Sender: TObject);
begin
  inherited;
  qryLinha.Close ;

  If (Trim(MaskEdit8.Text) <> '') then
  begin

    PosicionaQuery(qryLinha, Maskedit8.Text) ;

  end ;

end;

procedure TdcmVendaProdutoClienteWERP.MaskEdit9Exit(Sender: TObject);
begin
  inherited;
  qryClasseProduto.Close ;

  If (Trim(MaskEdit9.Text) <> '') then
  begin

    PosicionaQuery(qryClasseProduto, Maskedit9.Text) ;

  end ;

end;

procedure TdcmVendaProdutoClienteWERP.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (trim(MaskEdit1.Text) = '/  /') or (Trim(MaskEdit2.Text) = '/  /') then
  begin
      MaskEdit2.Text := DateToStr(Now()) ;
      MaskEdit1.Text := DateToStr(Now()-180) ;
  end ;

  dcmVendaProdutoCliente_view.ADODataSet1.Close ;
  dcmVendaProdutoCliente_view.ADODataSet1.Parameters.ParamByName('nCdEmpresa').Value        := frmMenu.ConvInteiro(MaskEdit3.Text) ;
  dcmVendaProdutoCliente_view.ADODataSet1.Parameters.ParamByName('nCdGrupoEconomico').Value := 0 ;
  dcmVendaProdutoCliente_view.ADODataSet1.Parameters.ParamByName('nCdTerceiro').Value       := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  dcmVendaProdutoCliente_view.ADODataSet1.Parameters.ParamByName('nCdDepartamento').Value   := frmMenu.ConvInteiro(MaskEdit5.Text) ;
  dcmVendaProdutoCliente_view.ADODataSet1.Parameters.ParamByName('nCdMarca').Value          := frmMenu.ConvInteiro(MaskEdit7.Text) ;
  dcmVendaProdutoCliente_view.ADODataSet1.Parameters.ParamByName('nCdLinha').Value          := frmMenu.ConvInteiro(MaskEdit8.Text) ;
  dcmVendaProdutoCliente_view.ADODataSet1.Parameters.ParamByName('nCdClasseProduto').Value  := frmMenu.ConvInteiro(MaskEdit9.Text) ;
  dcmVendaProdutoCliente_view.ADODataSet1.Parameters.ParamByName('dDtInicial').Value        := frmMenu.ConvData(MaskEdit1.Text) ;
  dcmVendaProdutoCliente_view.ADODataSet1.Parameters.ParamByName('dDtFinal').Value          := frmMenu.ConvData(MaskEdit2.Text) ;
  dcmVendaProdutoCliente_view.ADODataSet1.Parameters.ParamByName('nCdRamoAtividade').Value  := 0 ;
  dcmVendaProdutoCliente_view.ADODataSet1.Parameters.ParamByName('nCdTerceiroRepres').Value := 0 ;
  dcmVendaProdutoCliente_view.ADODataSet1.Open ;

  if (dcmVendaProdutoCliente_view.ADODataSet1.eof) then
  begin
      ShowMessage('Nenhuma informa��o encontrada.') ;
      exit ;
  end ;

  frmMenu.StatusBar1.Panels[6].Text := 'Preparando cubo...' ;

  if dcmVendaProdutoCliente_view.PivotCube1.Active then
  begin
      dcmVendaProdutoCliente_view.PivotCube1.Active := False;
  end;

  dcmVendaProdutoCliente_view.PivotCube1.ExtendedMode       := True;
  dcmVendaProdutoCliente_view.PVMeasureToolBar1.HideButtons := False;
  dcmVendaProdutoCliente_view.PivotCube1.Active             := True;

  dcmVendaProdutoCliente_view.PivotMap1.Measures[1].DisplayName := 'Quantidade' ;

  dcmVendaProdutoCliente_view.PivotMap1.Measures[2].ColumnPercent := True;
  dcmVendaProdutoCliente_view.PivotMap1.Measures[2].Value := False;

  dcmVendaProdutoCliente_view.PivotMap1.Measures[3].FieldName := 'nQtde' ;
  dcmVendaProdutoCliente_view.PivotMap1.Measures[3].Rank      := True ;
  dcmVendaProdutoCliente_view.PivotMap1.Measures[3].Value     := False;

  dcmVendaProdutoCliente_view.PivotMap1.SortColumn(0,3,False) ;

  dcmVendaProdutoCliente_view.PivotMap1.Title := 'ER2Soft - An�lise de Vendas por Clientes' ;
  dcmVendaProdutoCliente_view.PivotGrid1.ColWidths[0] := 200 ;

  dcmVendaProdutoCliente_view.PivotGrid1.RefreshData;

  frmMenu.StatusBar1.Panels[6].Text := '' ;

  dcmVendaProdutoCliente_view.ShowModal ;
  dcmVendaProdutoCliente_view.PivotMap1.Measures[3].FieldName := 'nValor' ;
  Self.Activate ;

end;

initialization
    RegisterClass(TdcmVendaProdutoClienteWERP) ;

end.

