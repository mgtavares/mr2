unit rRelCredQuitado_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, QRCtrls, jpeg, QuickRpt, ExtCtrls;

type
  TrptRelCredQuitado_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand2: TQRBand;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText6: TQRDBText;
    QRBand3: TQRBand;
    QRShape6: TQRShape;
    QRBand5: TQRBand;
    QRImage1: TQRImage;
    QRLabel15: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape3: TQRShape;
    QRShape5: TQRShape;
    QRBand1: TQRBand;
    lblTipoRelatorio: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    lblCodCred: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRLabel2: TQRLabel;
    QRlblCred: TQRLabel;
    qryRelCredQuitado: TADOQuery;
    qryRelCredQuitadonCdCrediario: TIntegerField;
    qryRelCredQuitadoDataVenda: TStringField;
    qryRelCredQuitadonCdTerceiro: TIntegerField;
    qryRelCredQuitadocNmTerceiro: TStringField;
    qryRelCredQuitadoiParcelas: TIntegerField;
    qryRelCredQuitadonValCrediario: TBCDField;
    qryRelCredQuitadonSaldo: TBCDField;
    qryRelCredQuitadoDataLiq: TStringField;
    dsRelCredQuitado: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRelCredQuitado_view: TrptRelCredQuitado_view;

implementation

{$R *.dfm}
uses
  fMenu;

end.
