unit fCaixa_Reimpressao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, cxPC, cxControls, DB, ADODB, GridsEh, DBGridEh,
  cxContainer, cxEdit, cxTextEdit, StdCtrls, cxLookAndFeelPainters,
  cxButtons, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxLabel, cxCheckBox, Cappta_Gp_Api_Com_TLB, ACBrBase, ACBrPosPrinter,StrUtils,Typinfo;

type
  TfrmCaixa_Reimpressao = class(TForm)
    Image1: TImage;
    cxPageControl1: TcxPageControl;
    tabCarnet: TcxTabSheet;
    tabVale: TcxTabSheet;
    tabComprovanteCaixa: TcxTabSheet;
    qryCrediario: TADOQuery;
    qryCrediarionCdCrediario: TIntegerField;
    qryCrediarionCdLoja: TIntegerField;
    qryCrediariodDtVenda: TDateTimeField;
    qryCrediariodDtUltVencto: TDateTimeField;
    qryCrediarionValCrediario: TBCDField;
    qryCrediarionSaldo: TBCDField;
    qryCrediariocNmCondPagto: TStringField;
    dsCrediario: TDataSource;
    Label2: TLabel;
    edtnCdTerceiro: TcxTextEdit;
    edtcNmTerceiro: TcxTextEdit;
    btConsultarCarnet: TcxButton;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    btSair: TcxButton;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdCrediario: TcxGridDBColumn;
    cxGridDBTableView1nCdLoja: TcxGridDBColumn;
    cxGridDBTableView1dDtVenda: TcxGridDBColumn;
    cxGridDBTableView1dDtUltVencto: TcxGridDBColumn;
    cxGridDBTableView1nValCrediario: TcxGridDBColumn;
    cxGridDBTableView1nSaldo: TcxGridDBColumn;
    cxGridDBTableView1cNmCondPagto: TcxGridDBColumn;
    cxGrid1: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    dsLanctoMan: TDataSource;
    qryLanctoMan: TADOQuery;
    qryLanctoMannCdLanctoFin: TAutoIncField;
    qryLanctoMandDtLancto: TDateTimeField;
    qryLanctoMannCdTipoLancto: TIntegerField;
    qryLanctoMancNmTipoLancto: TStringField;
    qryLanctoMannValLancto: TBCDField;
    cxGridDBTableView2nCdLanctoFin: TcxGridDBColumn;
    cxGridDBTableView2dDtLancto: TcxGridDBColumn;
    cxGridDBTableView2cNmTipoLancto: TcxGridDBColumn;
    cxGridDBTableView2nValLancto: TcxGridDBColumn;
    cxGrid3: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    qryVale: TADOQuery;
    qryValenCdVale: TAutoIncField;
    qryValedDtVale: TDateTimeField;
    qryValecTipoVale: TStringField;
    qryValecNmTerceiro: TStringField;
    qryValecRG: TStringField;
    qryValenValVale: TBCDField;
    dsVale: TDataSource;
    cxGridDBTableView3nCdVale: TcxGridDBColumn;
    cxGridDBTableView3dDtVale: TcxGridDBColumn;
    cxGridDBTableView3cTipoVale: TcxGridDBColumn;
    cxGridDBTableView3cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView3cRG: TcxGridDBColumn;
    cxGridDBTableView3nValVale: TcxGridDBColumn;
    tabReciboParcela: TcxTabSheet;
    cxGrid4: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridLevel4: TcxGridLevel;
    qryRecibo: TADOQuery;
    dsRecibo: TDataSource;
    qryRecibonCdLanctoFin: TAutoIncField;
    qryRecibodDtLancto: TDateTimeField;
    qryRecibonCdTipoLancto: TIntegerField;
    qryRecibocNmTipoLancto: TStringField;
    qryRecibonValLancto: TBCDField;
    cxGridDBTableView4nCdLanctoFin: TcxGridDBColumn;
    cxGridDBTableView4dDtLancto: TcxGridDBColumn;
    cxGridDBTableView4nCdTipoLancto: TcxGridDBColumn;
    cxGridDBTableView4cNmTipoLancto: TcxGridDBColumn;
    cxGridDBTableView4nValLancto: TcxGridDBColumn;
    qryRenegociacao: TADOQuery;
    qryRenegociacaonCdPropostaReneg: TIntegerField;
    qryCFe: TADOQuery;
    cxTabSheet5: TcxTabSheet;
    tabExtratoCFe: TcxGrid;
    cxGridDBTableView5: TcxGridDBTableView;
    cxGridLevel5: TcxGridLevel;
    dsCFe: TDataSource;
    cxGridDBTableView5DBColumn1: TcxGridDBColumn;
    cxGridDBTableView5DBColumn2: TcxGridDBColumn;
    cxGridDBTableView5DBColumn3: TcxGridDBColumn;
    cxGridDBTableView5DBColumn4: TcxGridDBColumn;
    cxGridDBTableView5DBColumn5: TcxGridDBColumn;
    cxGridDBTableView5DBColumn6: TcxGridDBColumn;
    qryCFenCdDoctoFiscal: TIntegerField;
    qryCFenCdTerceiro: TIntegerField;
    qryCFecNmTerceiro: TStringField;
    qryCFecCNPJCPF: TStringField;
    qryCFenValTotal: TBCDField;
    qryCFecChave: TStringField;
    qryCFenCdStatus: TIntegerField;
    qryCFecNmStatus: TStringField;
    qryCFedDtEmissao: TDateTimeField;
    cxGridDBTableView5DBColumn7: TcxGridDBColumn;
    cxGridDBTableView5DBColumn8: TcxGridDBColumn;
    qryCFeiNrDocto: TIntegerField;
    cxTabSheet1: TcxTabSheet;
    tabCupomTEF: TcxGrid;
    cxGridDBTableView6: TcxGridDBTableView;
    cxGridLevel6: TcxGridLevel;
    qryTEF: TADOQuery;
    dsTEF: TDataSource;
    qryTEFcNumeroControle: TStringField;
    qryTEFcNmTerceiro: TStringField;
    qryTEFiNrDocto: TIntegerField;
    qryTEFiNrAutorizacao: TIntegerField;
    qryTEFnValTransacao: TFloatField;
    cxGridDBTableView6cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView6nValTransacao: TcxGridDBColumn;
    cxGridDBTableView6iNrDocto: TcxGridDBColumn;
    cxGridDBTableView6iNrAutorizacao: TcxGridDBColumn;
    cxGridDBTableView6cNumeroControle: TcxGridDBColumn;
    qryImpressora: TADOQuery;
    GroupBox1: TGroupBox;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    RadioButton3: TRadioButton;
    qryImpressoracModeloImpSAT: TStringField;
    qryImpressoracPortaImpSAT: TStringField;
    qryImpressoracPageCodImpSAT: TStringField;
    qryImpressoracFlgTipoImpSAT: TIntegerField;
    procedure edtnCdTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btConsultarCarnetClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure edtnCdTerceiroKeyPress(Sender: TObject; var Key: Char);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure cxGridDBTableView2DblClick(Sender: TObject);
    procedure tabComprovanteCaixaEnter(Sender: TObject);
    procedure cxGridDBTableView2KeyPress(Sender: TObject; var Key: Char);
    procedure cxGridDBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure cxGridDBTableView3KeyPress(Sender: TObject; var Key: Char);
    procedure cxGridDBTableView3DblClick(Sender: TObject);
    procedure tabValeEnter(Sender: TObject);
    procedure tabReciboParcelaEnter(Sender: TObject);
    procedure cxGridDBTableView4DblClick(Sender: TObject);
    procedure cxGridDBTableView4KeyPress(Sender: TObject; var Key: Char);
    procedure tabExtratoCFeEnter(Sender: TObject);
    procedure cxGridDBTableView5DblClick(Sender: TObject);
    procedure tabCupomTEFEnter(Sender: TObject);
    procedure cxGridDBTableView6DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdResumoCaixa : integer ;
  end;

var
  frmCaixa_Reimpressao: TfrmCaixa_Reimpressao;

implementation

uses
  fCaixa_SelCliente, fMenu,fCaixa_LanctoMan, fCaixa_Recebimento, rCaixa_Recebimento_Comprovante,
  fImpDFPadrao, fFuncoesSAT, fCapptAPI, uPDV_Bematech_Termica;

{$R *.dfm}

procedure TfrmCaixa_Reimpressao.edtnCdTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nCdTerceiro : integer ;
    objForm : TfrmCaixa_SelCliente;
begin


  if (Key = VK_F4) then
  begin
      qryCrediario.Close ;
      edtcNmTerceiro.Text := '' ;

      objForm := TfrmCaixa_SelCliente.Create(nil);

      nCdTerceiro := objForm.SelecionaCliente ;

      edtnCdTerceiro.Text := intToStr(nCdTerceiro) ;

  end ;

end;

procedure TfrmCaixa_Reimpressao.btConsultarCarnetClick(Sender: TObject);
begin

  qryCrediario.Close ;
  qryCrediario.Parameters.ParamByName('nPK').Value := strToint(edtnCdTerceiro.Text) ;
  qryCrediario.open ;

  if (qryCrediario.eof) then
  begin
      frmMenu.MensagemAlerta('Nenhum Carnet em aberto para este cliente.') ;
      edtnCdTerceiro.SetFocus;
  end ;

end;

procedure TfrmCaixa_Reimpressao.FormShow(Sender: TObject);
begin

    cxPageControl1.ActivePage := tabCarnet;

    if (nCdResumoCaixa <= 0) then
    begin
        tabVale.Visible             := False;
        tabComprovanteCaixa.Visible := False;
        tabExtratoCFe.Visible       := False;
    end ;

    qryTerceiro.Close ;
    edtnCdTerceiro.Text := '' ;
    edtcNmTerceiro.Text := '' ;

    edtnCdTerceiro.Height := 33 ;
    edtcNmTerceiro.Height := 33 ;

    edtnCdTerceiro.SetFocus ;

end;

procedure TfrmCaixa_Reimpressao.btSairClick(Sender: TObject);
begin
    qryTerceiro.Close ;
    qryCrediario.Close;
    qryLanctoMan.Close;
    qryVale.Close;
    qryCFe.Close;
    Close ;
end;

procedure TfrmCaixa_Reimpressao.edtnCdTerceiroKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
    begin
      if (edtnCdTerceiro.Text <> '') then
      begin
          qryTerceiro.Close ;
          qryTerceiro.Parameters.ParamByName('nPK').Value := strToint(edtnCdTerceiro.Text) ;
          qryTerceiro.Open;

          if not qryTerceiro.eof then
              edtcNmTerceiro.Text := qryTerceirocNmTerceiro.Value ;
      end ;

      btConsultarCarnet.SetFocus ;
    end ;

end;

procedure TfrmCaixa_Reimpressao.cxGridDBTableView1DblClick(
  Sender: TObject);
begin

  if (qryCrediario.eof) then
      exit ;

  case frmMenu.MessageDlg('Confirma a reimpress�o deste carnet ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.ShowMessage('Prepare a impressora e clique em OK para iniciar a impress�o.') ;

  frmImpDFPadrao.ImprimirCarnet(qryCrediarionCdCrediario.Value, 1) ;

end;

procedure TfrmCaixa_Reimpressao.cxGridDBTableView2DblClick(
  Sender: TObject);
var
  objForm : TfrmCaixa_LanctoMan;
begin

  if not qryLanctoMan.eof then
  begin

      case frmMenu.MessageDlg('Confirma a impress�o do comprovante ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo:exit ;
      end ;

      objForm := TfrmCaixa_LanctoMan.Create(nil);

      objForm.ImprimirComprovante(qryLanctoMannCdLanctoFin.Value);

  end ;

end;

procedure TfrmCaixa_Reimpressao.tabComprovanteCaixaEnter(Sender: TObject);
begin

    qryLanctoMan.Close ;
    qryLanctoMan.Parameters.ParamByName('nPK').Value := nCdResumoCaixa ;
    qryLanctoMan.Open;

end;

procedure TfrmCaixa_Reimpressao.cxGridDBTableView2KeyPress(Sender: TObject;
  var Key: Char);
var
  objForm : TfrmCaixa_LanctoMan;
begin

  if (Key = #13) then
      if not qryLanctoMan.eof then
      begin

          case frmMenu.MessageDlg('Confirma a impress�o do comprovante ?',mtConfirmation,[mbYes,mbNo],0) of
            mrNo:exit ;
          end ;

          objForm := TfrmCaixa_LanctoMan.Create(nil);

          objForm.ImprimirComprovante(qryLanctoMannCdLanctoFin.Value);

      end ;

end;

procedure TfrmCaixa_Reimpressao.cxGridDBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin

  if (Key = #13) then
  begin
      if (qryCrediario.eof) then
          exit ;

      case frmMenu.MessageDlg('Confirma a reimpress�o deste carnet ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

      frmMenu.ShowMessage('Prepare a impressora e clique em OK para iniciar a impress�o.') ;

      frmImpDFPadrao.ImprimirCarnet(qryCrediarionCdCrediario.Value, 1) ;

  end ;

end;

procedure TfrmCaixa_Reimpressao.cxGridDBTableView3KeyPress(Sender: TObject;
  var Key: Char);
var
  objForm : TfrmCaixa_Recebimento;
begin

    if (Key = #13) then
    
        if not qryVale.eof then
        begin

            case frmMenu.MessageDlg('Confirma a impress�o deste vale ?',mtConfirmation,[mbYes,mbNo],0) of
                mrNo:exit ;
            end ;

            objForm := TfrmCaixa_Recebimento.Create(nil);
            objForm.ImprimirVale(qryValenCdVale.Value);

        end ;


end;

procedure TfrmCaixa_Reimpressao.cxGridDBTableView3DblClick(
  Sender: TObject);
var
  objForm : TfrmCaixa_Recebimento;
begin


  if not qryVale.eof then
  begin

      case frmMenu.MessageDlg('Confirma a impress�o deste vale ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo:exit ;
      end ;

      objForm := TfrmCaixa_Recebimento.Create(nil);

      objForm.ImprimirVale(qryValenCdVale.Value);

  end ;

end;

procedure TfrmCaixa_Reimpressao.tabValeEnter(Sender: TObject);
begin

    qryVale.Close ;
    qryVale.Parameters.ParamByName('nPK').Value := nCdResumoCaixa;
    qryVale.Open;

end;

procedure TfrmCaixa_Reimpressao.tabReciboParcelaEnter(Sender: TObject);
begin

    qryRecibo.Close ;
    qryRecibo.Parameters.ParamByName('nPK').Value := nCdResumoCaixa ;
    qryRecibo.Open;

end;

procedure TfrmCaixa_Reimpressao.cxGridDBTableView4DblClick(
  Sender: TObject);
var
  objForm : TrptCaixa_Recebimento_Comprovante;
begin
  if (not qryRecibo.eof) then
  begin
      { -- verifica se o lan�amento corresponde a uma renegocia��o a vista -- }
      qryRenegociacao.Close;
      qryRenegociacao.Parameters.ParamByName('nCdLanctoFin').Value := qryRecibonCdLanctoFin.Value;
      qryRenegociacao.Open;

      if (qryRenegociacao.IsEmpty) then
      begin
          objForm := TrptCaixa_Recebimento_Comprovante.Create(nil);
          objForm.imprimeRecibo(qryRecibonCdLanctoFin.Value);
      end
      else
          frmImpDFPadrao.ImprimirReneg(qryRenegociacaonCdPropostaReneg.Value);

      FreeAndNil(objForm);
  end;
end;

procedure TfrmCaixa_Reimpressao.cxGridDBTableView4KeyPress(Sender: TObject;
  var Key: Char);
var
  objForm : TrptCaixa_Recebimento_Comprovante;
begin

    if (Key = #13) then
      if not qryRecibo.eof then
      begin
          objForm := TrptCaixa_Recebimento_Comprovante.Create(nil);
          objForm.imprimeRecibo(qryRecibonCdLanctoFin.Value);
      end;
end;

procedure TfrmCaixa_Reimpressao.tabExtratoCFeEnter(Sender: TObject);
begin
  qryCFe.Close ;
  qryCFe.Parameters.ParamByName('nPK').Value := nCdResumoCaixa;
  qryCFe.Open;
end;

procedure TfrmCaixa_Reimpressao.cxGridDBTableView5DblClick(
  Sender: TObject);
var
  objSAT : TfrmFuncoesSAT;
begin
  if (Trim(qryCFecChave.Value) = '') then
  begin
      frmMenu.MensagemAlerta('Chave do documento n�o informado.');
      Abort;
  end;

  try
      objSAT := TfrmFuncoesSAT.Create(Nil);

      case (qryCFenCdStatus.Value) of
          1 : objSAT.prImpExtratoVenda(qryCFenCdDoctoFiscal.Value);
          2 : objSAT.prImpExtratoCancelamento(qryCFenCdDoctoFiscal.Value);
      end;
  finally
      FreeAndNil(objSAT);
  end;
end;

procedure TfrmCaixa_Reimpressao.tabCupomTEFEnter(Sender: TObject);
begin
  qryTEF.Close ;
  qryTEF.Parameters.ParamByName('nPK').Value := nCdResumoCaixa;
  qryTEF.Open;
end;

procedure TfrmCaixa_Reimpressao.cxGridDBTableView6DblClick(Sender: TObject);
var
  objTEF                  : TfrmCapptAPI;
  opAprovada              : IRespostaOperacaoAprovada;
  opNegada                : IRespostaOperacaoRecusada;
  iteracaoTef             : IIteracaoTef;
  tipoIteracao            : Integer;
  tipoVia                 : Integer;
  cupomCliente, cupomLoja : TStringList;
  I : Integer;
  ppMarcaImpSAT : TACBrPosPrinterModelo;
  pcPagCodigo   : TACBrPosPaginaCodigo;

begin
try
  objTEF := TfrmCapptAPI.Create(Self);
  cupomCliente  := TStringList.Create;
  cupomLoja := TStringList.Create;

  if (RadioButton1.Checked) then
    tipoVia := 1;

  if (RadioButton2.Checked) then
    tipoVia := 2;

  if (RadioButton3.Checked) then
    tipoVia := 3;

  objTEF.reimprimeCupom(qryTEFcNumeroControle.AsString,tipoVia);

  { ---- In�cio da Itera��o TEF ---- }

  Repeat

    iteracaoTef := ObjTEF.cappta.IterarOperacaoTef();
    tipoIteracao := iteracaoTef.Get_TipoIteracao;

  Until (iteracaoTef.Get_TipoIteracao = 1) or (iteracaoTef.Get_TipoIteracao = 2);

  { ---- Fim da Itera��o TEF ---- }

  if (tipoIteracao <> 1) then
  begin
    opNegada := (iteracaoTef as IRespostaOperacaoRecusada);
    frmMenu.ShowMessage('Opera��o n�o processada. ' + opNegada.Get_Motivo);
    Exit;
  end;

  opAprovada := (iteracaoTef as IRespostaOperacaoAprovada);

  cupomCliente.Add(opAprovada.Get_CupomCliente);

  cupomLoja.Add(opAprovada.Get_CupomLojista);

  //Imprime cupons TEF

        qryImpressora.Close;
        qryImpressora.Parameters.ParamByName('cNmComputador').Value := frmMenu.qryConfigAmbientecNmComputador.Value;
        qryImpressora.Open;

       if qryImpressora.IsEmpty then
          frmMenu.MensagemErro('N�o Encontrada configura��o impressora para '+frmMenu.qryConfigAmbientecNmComputador.Value)
       else begin
//        if qryImpressoracFlgTipoImpSAT.Value = 1 then  // Tipo ESC POS
//        begin

           case AnsiIndexStr(qryImpressoracModeloImpSAT.Value,['Epson','Bematech','Daruma','Elgin','Diebold']) of
           0 : ppMarcaImpSAT := ppEscPosEpson;
           1 : ppMarcaImpSAT := ppEscBematech;
           2 : ppMarcaImpSAT := ppEscDaruma;
           3 : ppMarcaImpSAT := ppEscVox;    //ppEscElgin;   Alterado 02/08/18 nova versoa ACBR por Marcelo
           4 : ppMarcaImpSAT := ppEscDiebold;
           end;

           case AnsiIndexStr(qryImpressoracPageCodImpSat.Value,['pcNone','pc437','pc850','pc852','pc860','pcUTF8','pc1252']) of
           0 : pcPagCodigo := pcNone;
           1 : pcPagCodigo := pc437;
           2 : pcPagCodigo := pc850;
           3 : pcPagCodigo := pc852;
           4 : pcPagCodigo := pc860;
           5 : pcPagCodigo := pcUTF8;
           6 : pcPagCodigo := pc1252;
           end;

            frmMenu.ACBrPosPrinter1.Desativar;

            frmMenu.ACBrPosPrinter1.Modelo             := ppMarcaImpSAT;
            frmMenu.ACBrPosPrinter1.Porta              := qryImpressoracPortaImpSAT.Value;
            frmMenu.ACBrPosPrinter1.CortaPapel         := True;
            frmMenu.ACBrPosPrinter1.PaginaDeCodigo     := pcPagCodigo;


            frmMenu.ACBRPosPrinter1AguardarPronta( True );


            try

             frmMenu.ACBrPosPrinter1.Ativar;
             frmMenu.ACBrPosPrinter1.Imprimir(cupomCliente.Text);
             frmMenu.ACBrPosPrinter1.CortarPapel( True );
             frmMenu.ACBrPosPrinter1.Imprimir(cupomLoja.Text);
             frmMenu.ACBrPosPrinter1.CortarPapel( False );  // Corte Total
             frmMenu.ACBrPosPrinter1.Desativar;

             frmMenu.ACBRPosPrinter1FechaImpTermica;
            except
             frmMenu.MensagemErro('Erro ao efetuar a impress�o do cupom TEF, entre em "Reimpress�o" e reimprima o comprovante.');
            end;
       end;
finally

  cupomCliente.Free;

  cupomLoja.Free;

  objTEF.Free;

end;

end;

end.
