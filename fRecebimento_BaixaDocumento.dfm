inherited frmRecebimento_BaixaDocumento: TfrmRecebimento_BaixaDocumento
  Left = 302
  Top = 204
  Width = 978
  Caption = 'Baixa de Documento Pendente no Recebimento'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 145
    Width = 962
    Height = 317
  end
  inherited ToolBar1: TToolBar
    Width = 962
    ButtonWidth = 117
    inherited ToolButton1: TToolButton
      Caption = 'Executar &Consulta'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 117
    end
    inherited ToolButton2: TToolButton
      Left = 125
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 145
    Width = 962
    Height = 317
    Align = alClient
    AllowedOperations = []
    DataGrouping.GroupLevels = <>
    DataSource = dsRecebimento
    FixedColor = clMoneyGreen
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdRecebimento'
        Footers = <>
        Title.Caption = 'Recebimentos com Documento Pendente|C'#243'd.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clRed
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 65
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'nCdLoja'
        Footers = <>
        Title.Caption = 'Recebimentos com Documento Pendente|Loja'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clRed
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 65
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'nCdTerceiro'
        Footers = <>
        Title.Caption = 'Recebimentos com Documento Pendente|Terceiro|C'#243'd.'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clRed
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 65
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'cNmTerceiro'
        Footers = <>
        Title.Caption = 'Recebimentos com Documento Pendente|Terceiro|Descri'#231#227'o'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clRed
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 413
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'cNrDocto'
        Footers = <>
        Title.Caption = 'Recebimentos com Documento Pendente|Nr. Docto'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clRed
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 100
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'dDtReceb'
        Footers = <>
        Title.Caption = 'Recebimentos com Documento Pendente|Data Recebimento'
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clRed
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 101
      end
      item
        Color = clSilver
        EditButtons = <>
        FieldName = 'nValTotalNF'
        Footers = <>
        Title.Caption = 'Recebimentos com Documento Pendente|Val. Total '
        Title.Font.Charset = DEFAULT_CHARSET
        Title.Font.Color = clRed
        Title.Font.Height = -11
        Title.Font.Name = 'Tahoma'
        Title.Font.Style = []
        Width = 100
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object GroupBox2: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 962
    Height = 116
    Align = alTop
    Caption = ' Filtros de Consulta '
    Color = clWhite
    ParentColor = False
    TabOrder = 2
    object Label6: TLabel
      Tag = 1
      Left = 93
      Top = 32
      Width = 39
      Height = 13
      Alignment = taRightJustify
      Caption = 'Terceiro'
    end
    object Label10: TLabel
      Tag = 1
      Left = 16
      Top = 78
      Width = 116
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo de Recebimento'
    end
    object Label7: TLabel
      Tag = 1
      Left = 216
      Top = 78
      Width = 16
      Height = 13
      Alignment = taCenter
      Caption = 'at'#233
    end
    object Label12: TLabel
      Tag = 1
      Left = 47
      Top = 56
      Width = 85
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja Recebimento'
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 202
      Top = 24
      Width = 463
      Height = 21
      TabStop = False
      DataField = 'cNmTerceiro'
      DataSource = dsTerceiro
      TabOrder = 4
    end
    object er2LkpTerceiro: TER2LookupMaskEdit
      Left = 136
      Top = 24
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      CodigoLookup = 17
      QueryLookup = qryTerceiro
    end
    object mskdDtInicial: TMaskEdit
      Left = 136
      Top = 72
      Width = 65
      Height = 21
      EditMask = '##/##/####;1;_'
      MaxLength = 10
      TabOrder = 2
      Text = '  /  /    '
    end
    object mskdDtFinal: TMaskEdit
      Left = 248
      Top = 72
      Width = 65
      Height = 21
      EditMask = '##/##/####;1;_'
      MaxLength = 10
      TabOrder = 3
      Text = '  /  /    '
    end
    object er2LkpLoja: TER2LookupMaskEdit
      Left = 136
      Top = 48
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 1
      Text = '         '
      CodigoLookup = 147
      QueryLookup = qryLoja
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 202
      Top = 48
      Width = 463
      Height = 21
      TabStop = False
      DataField = 'cNmLoja'
      DataSource = dsLoja
      TabOrder = 5
    end
  end
  inherited ImageList1: TImageList
    Left = 392
    Top = 224
  end
  object qryRecebimento: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'dDtFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro     int'
      '       ,@nCdLoja         int'
      '       ,@dDtInicial      varchar(10)'
      '       ,@dDtFinal        varchar(10)'
      ''
      'SET @nCdTerceiro     = :nCdTerceiro'
      'SET @nCdLoja         = :nCdLoja'
      'SET @dDtInicial      = :dDtInicial'
      'SET @dDtFinal        = :dDtFinal'
      ''
      'SELECT nCdRecebimento'
      '      ,nCdLoja'
      '      ,Terceiro.nCdTerceiro'
      '      ,Terceiro.cNmTerceiro'
      '      ,cNrDocto'
      '      ,dDtReceb'
      '      ,nValTotalNF'
      '      ,cFlgDocEntregue'
      '  FROM Recebimento'
      
        '       INNER JOIN Terceiro ON Terceiro.nCdTerceiro = Recebimento' +
        '.nCdTerceiro'
      ' WHERE nCdEmpresa                = :nCdEmpresa'
      
        '   AND ((Recebimento.nCdTerceiro = @nCdTerceiro)                ' +
        '      OR (@nCdTerceiro     = 0))'
      
        '   AND ((nCdLoja                 = @nCdLoja)                    ' +
        '      OR (@nCdLoja         = 0))'
      
        '   AND ((dDtReceb               >= CONVERT(datetime,@dDtInicial,' +
        '103)) OR (@dDtInicial      = '#39'01/01/1900'#39'))'
      
        '   AND ((dDtReceb               <  CONVERT(datetime,@dDtFinal,10' +
        '3)+1) OR (@dDtFinal        = '#39'01/01/1900'#39'))'
      '   AND cFlgDocEntregue           = 0'
      '   AND nCdTabStatusReceb        >  2'
      ' ORDER BY dDtReceb')
    Left = 424
    Top = 224
    object qryRecebimentonCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryRecebimentonCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryRecebimentonCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryRecebimentocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryRecebimentocNrDocto: TStringField
      FieldName = 'cNrDocto'
      FixedChar = True
      Size = 17
    end
    object qryRecebimentonValTotalNF: TBCDField
      FieldName = 'nValTotalNF'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryRecebimentocFlgDocEntregue: TIntegerField
      FieldName = 'cFlgDocEntregue'
    end
    object qryRecebimentodDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
    end
  end
  object dsRecebimento: TDataSource
    DataSet = qryRecebimento
    Left = 424
    Top = 256
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '       ,cNmLoja'
      '   FROM Loja'
      '  WHERE nCdLoja = :nPK')
    Left = 488
    Top = 224
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 488
    Top = 256
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 456
    Top = 256
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      ' WHERE nCdTerceiro = :nPK'
      '   AND nCdStatus   = 1')
    Left = 456
    Top = 221
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
  end
end
