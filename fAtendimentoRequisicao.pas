unit fAtendimentoRequisicao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, PrnDbgeh, GridsEh, DBGridEh, cxPC,
  cxControls, cxLookAndFeelPainters, cxButtons, DBGridEhGrouping,
  cxContainer, cxEdit, cxTextEdit, ToolCtrlsEh;

type
  TfrmAtendimentoRequisicao = class(TfrmCadastro_Padrao)
    cxPageControl1: TcxPageControl;
    tabItemRequisicao: TcxTabSheet;
    qryItens: TADOQuery;
    dsItens: TDataSource;
    DBGridEh1: TDBGridEh;
    tabItemAtendido: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryAtendRequisicao: TADOQuery;
    dsAtendRequisicao: TDataSource;
    qryItensnCdProduto: TIntegerField;
    qryItenscNmItem: TStringField;
    qryItensnQtdeReq: TBCDField;
    qryItensnQtdeCanc: TBCDField;
    qryItensnQtdeAtend: TBCDField;
    SP_MOVIMENTA_ESTOQUE: TADOStoredProc;
    GroupBox1: TGroupBox;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    qryItensnCdTipoItemRequisicao: TIntegerField;
    qryMastercNmEmpresa: TStringField;
    qryMastercNmLoja: TStringField;
    qryMasternCdRequisicao: TIntegerField;
    qryMastercNmTipoRequisicao: TStringField;
    qryMasterdDtRequisicao: TDateTimeField;
    qryMasterdDtAutor: TDateTimeField;
    qryMastercNmSetor: TStringField;
    qryMastercNmSolicitante: TStringField;
    qryMastercNumeroOP: TStringField;
    qryMastercNmCentroProdutivo: TStringField;
    qryMastercNmEtapaProducao: TStringField;
    qryMasternCdPedido: TIntegerField;
    qryMastercNmTabStatusRequis: TStringField;
    qryMastercOBS: TMemoField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    Label16: TLabel;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    Label17: TLabel;
    DBEdit16: TDBEdit;
    Label18: TLabel;
    DBEdit17: TDBEdit;
    DBMemo1: TDBMemo;
    qryItenscUnidadeMedida: TStringField;
    qryItensnQtdeSaldo: TBCDField;
    qryMasternCdTabStatusRequis: TIntegerField;
    qryMasternCdTabTipoRequis: TIntegerField;
    SP_CANCELA_SALDO_ITEMREQUISICAO: TADOStoredProc;
    qryMastercNmTerceiro: TStringField;
    qryMasternCdTerceiro: TIntegerField;
    DBEdit18: TDBEdit;
    panel: TPanel;
    btAtender: TcxButton;
    btZerarSaldo: TcxButton;
    cxTextEdit1: TcxTextEdit;
    Label7: TLabel;
    qryItensnCdItemRequisicao: TIntegerField;
    qryAtendRequisicaodDtAtendimento: TDateTimeField;
    qryAtendRequisicaocNmUsuario: TStringField;
    qryAtendRequisicaonQtdeAtend: TIntegerField;
    qryAtendRequisicaocNmLocalEstoque: TStringField;
    qryAtendRequisicaonCdLocalEstoque: TIntegerField;
    qryAtendRequisicaonCdUsuarioAtend: TIntegerField;
    qryAtendRequisicaonCdItemRequisicao: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure cxPageControl1Change(Sender: TObject);
    procedure btAtenderClick(Sender: TObject);
    procedure btZerarSaldoClick(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAtendimentoRequisicao: TfrmAtendimentoRequisicao;

implementation

uses fMenu, fAtendRequisicao_Local, fAtendimentoRequisicao_Atende;

{$R *.dfm}

procedure TfrmAtendimentoRequisicao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'REQUISICAO' ;
  nCdTabelaSistema  := 54 ;
  nCdConsultaPadrao := 120 ;

end;

procedure TfrmAtendimentoRequisicao.qryMasterAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.Active) and (qryMasterdDtAutor.AsString = '') then
  begin
      MensagemAlerta('Requisi��o n�o autorizada, imposs�vel atender.') ;
  end ;

  if (qryMasternCdTabTipoRequis.Value = 2) then
  begin
      MensagemAlerta('Requisi��o de compra n�o pode ser atendida nesta tela.') ;
  end;

  if (qryMasternCdTabTipoRequis.Value = 3) then
  begin
      MensagemAlerta('Requisi��o de empenho n�o pode ser atendida nesta tela.');
      qryMaster.Close;
      Exit;
  end;

  PosicionaQuery(qryItens, qryMasternCdRequisicao.AsString) ;

  cxPageControl1.ActivePage := tabItemRequisicao;
end;

procedure TfrmAtendimentoRequisicao.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePage := tabItemRequisicao;
end;

procedure TfrmAtendimentoRequisicao.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryItens.Close;
  qryAtendRequisicao.Close;
end;

procedure TfrmAtendimentoRequisicao.cxPageControl1Change(Sender: TObject);
begin
  inherited;

  if (cxPageControl1.ActivePage = tabItemAtendido) then
  begin
      if not qryItens.eof then
      begin
          qryAtendRequisicao.Close ;
          PosicionaQuery(qryAtendRequisicao, qryItensnCdItemRequisicao.asString) ;
      end ;
      
  end ;

end;

procedure TfrmAtendimentoRequisicao.btAtenderClick(Sender: TObject);
var
  objForm : TfrmAtendimentoRequisicao_Atende;
begin
  if not qryItens.Active then
      exit ;

  if ( qryMasternCdTabStatusRequis.Value = 1 ) then
  begin
      MensagemAlerta('A requisi��o n�o est� autorizada.') ;
      abort ;
  end ;

  if ( not qryMasternCdTabStatusRequis.Value in [2,3] ) then
  begin
      MensagemAlerta('O status da requisi��o n�o permite atendimentos.') ;
      abort ;
  end ;

  if (qryMasternCdTabTipoRequis.Value = 2) then
  begin
      MensagemAlerta('Requisi��o de compra n�o pode ser atendida nesta tela.') ;
      abort ;
  end ;

  objForm := TfrmAtendimentoRequisicao_Atende.Create( Self );

  objForm.cNmCentroProdutivo := qryMastercNmCentroProdutivo.Value ;
  objForm.nCdRequisicao      := qryMasternCdRequisicao.Value ;
  objForm.cOBSMovLote        := 'REQUIS.: ' + Trim( qryMasternCdRequisicao.AsString ) ;

  if (qryMastercNumeroOP.Value <> '') then
      objForm.cOBSMovLote := objForm.cOBSMovLote + '  OP: ' + Trim( qryMastercNumeroOP.Value ) ;

  if (qryMastercNmTerceiro.Value <> '') then
      objForm.cOBSMovLote := objForm.cOBSMovLote + ' - ' + Trim( qryMastercNmTerceiro.Value ) ;

  objForm.qryPreparaTemp.ExecSQL ;

  PosicionaQuery( objForm.qryPopulaTemp , qryMasternCdRequisicao.AsString ) ;

  try

      if (objForm.qryPopulaTemp.Eof) then
          MensagemAlerta('Nenhum item pendente de atendimento.')
      else showForm( objForm , FALSE ) ;

  finally

      freeAndNil( objForm ) ;

  end ;

  qryMaster.Requery();
  
end;

procedure TfrmAtendimentoRequisicao.btZerarSaldoClick(Sender: TObject);
begin
  inherited;

  if not qryItens.Active then
      exit ;

  if ( qryMasternCdTabStatusRequis.Value = 1 ) then
  begin
      MensagemAlerta('A requisi��o n�o est� autorizada.') ;
      abort ;
  end ;

  if ( not qryMasternCdTabStatusRequis.Value in [2,3] ) then
  begin
      MensagemAlerta('O status da requisi��o n�o permite cancelamento de saldo.') ;
      abort ;
  end ;

  if (qryMasternCdTabTipoRequis.Value = 2) then
  begin
      MensagemAlerta('Requisi��o de compra n�o pode ser movimentada nesta tela.') ;
      abort ;
  end ;

  if ((qryItensnQtdeReq.Value - qryItensnQtdeAtend.Value - qryItensnQtdeCanc.Value) <= 0) then
  begin
      MensagemAlerta('O item n�o tem saldo pendente.') ;
      abort ;
  end ;

  case MessageDlg('Confirma o cancelamento do saldo deste item?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try

      SP_CANCELA_SALDO_ITEMREQUISICAO.Close;
      SP_CANCELA_SALDO_ITEMREQUISICAO.Parameters.ParamByName('@nCdItemRequisicao').Value := qryItensnCdItemRequisicao.Value;
      SP_CANCELA_SALDO_ITEMREQUISICAO.Parameters.ParamByName('@nCdUsuario').Value        := frmMenu.nCdUsuarioLogado;
      SP_CANCELA_SALDO_ITEMREQUISICAO.ExecProc;

  except

      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;

  end ;

  frmMenu.Connection.CommitTrans;

  qryMaster.Requery();
  
end;

procedure TfrmAtendimentoRequisicao.DBGridEh1DblClick(Sender: TObject);
begin
  inherited;

  btAtender.Click ;

end;

procedure TfrmAtendimentoRequisicao.DBGridEh1DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  // item AD pinta de vermelho para destacar
  if (qryItensnCdTipoItemRequisicao.Value = 2) then
  begin

     DBGridEh1.Canvas.Brush.Color := clRed;
     DBGridEh1.Canvas.Font.Color  := clWhite ;

     DBGridEh1.Canvas.FillRect(Rect);
     DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);

  end ;

end;

initialization
    RegisterClass(TfrmAtendimentoRequisicao) ;

end.
