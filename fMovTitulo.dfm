inherited frmMovTitulo: TfrmMovTitulo
  Left = 25
  Top = 35
  Width = 1332
  Height = 669
  Caption = 'Movimenta'#231#227'o Individual T'#237'tulo Financeiro'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1316
    Height = 606
  end
  object Label1: TLabel [1]
    Left = 54
    Top = 46
    Width = 70
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo T'#237'tulo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 81
    Top = 70
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 38
    Top = 142
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Esp'#233'cie de T'#237'tulo'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 51
    Top = 190
    Width = 73
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero T'#237'tulo'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 256
    Top = 190
    Width = 36
    Height = 13
    Caption = 'Parcela'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 84
    Top = 166
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit6
  end
  object Label9: TLabel [7]
    Left = 591
    Top = 166
    Width = 117
    Height = 13
    Alignment = taRightJustify
    Caption = 'Senso (Debito/Cr'#233'dito)'
    FocusControl = DBEdit9
  end
  object Label12: TLabel [8]
    Left = 36
    Top = 214
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Vencimento'
    FocusControl = DBEdit12
  end
  object Label16: TLabel [9]
    Left = 233
    Top = 214
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor T'#237'tulo'
    FocusControl = DBEdit16
  end
  object Label17: TLabel [10]
    Left = 393
    Top = 214
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Liquidado'
    FocusControl = DBEdit17
  end
  object Label18: TLabel [11]
    Left = 567
    Top = 238
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo em Aberto'
    FocusControl = DBEdit18
  end
  object Label19: TLabel [12]
    Left = 72
    Top = 238
    Width = 52
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Juro'
    FocusControl = DBEdit19
  end
  object Label20: TLabel [13]
    Left = 213
    Top = 238
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Desconto'
    FocusControl = DBEdit20
  end
  object Label21: TLabel [14]
    Left = 204
    Top = 46
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero SP'
    FocusControl = DBEdit21
  end
  object Label24: TLabel [15]
    Left = 743
    Top = 48
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observa'#231#227'o T'#237'tulo'
    FocusControl = DBMemo1
  end
  object Label27: TLabel [16]
    Left = 18
    Top = 94
    Width = 106
    Height = 13
    Alignment = taRightJustify
    Caption = 'Unidade de Neg'#243'cio'
    FocusControl = DBEdit34
  end
  object Label7: TLabel [17]
    Left = 418
    Top = 190
    Width = 58
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero NF'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [18]
    Left = 342
    Top = 46
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'mero Provis'#227'o'
    FocusControl = DBEdit8
  end
  object Label11: TLabel [19]
    Left = 8
    Top = 262
    Width = 116
    Height = 13
    Alignment = taRightJustify
    Caption = 'Conta Banc'#225'ria D'#233'bito'
    FocusControl = DBEdit19
  end
  object Label13: TLabel [20]
    Left = 103
    Top = 118
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit22
  end
  object Label10: TLabel [21]
    Left = 385
    Top = 238
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Abatimento'
    FocusControl = DBEdit28
  end
  inherited ToolBar2: TToolBar
    Width = 1316
    inherited btIncluir: TToolButton
      Visible = False
    end
    inherited ToolButton7: TToolButton
      Visible = False
    end
    inherited ToolButton9: TToolButton
      Visible = False
    end
    inherited ToolButton2: TToolButton
      Visible = False
    end
    inherited btSalvar: TToolButton
      Visible = False
    end
    inherited ToolButton6: TToolButton
      Visible = False
    end
    inherited btExcluir: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [23]
    Tag = 1
    Left = 128
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdTitulo'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [24]
    Tag = 1
    Left = 128
    Top = 64
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [25]
    Tag = 1
    Left = 128
    Top = 136
    Width = 65
    Height = 19
    DataField = 'nCdEspTit'
    DataSource = dsMaster
    TabOrder = 4
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [26]
    Tag = 1
    Left = 128
    Top = 184
    Width = 121
    Height = 19
    DataField = 'cNrTit'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit5: TDBEdit [27]
    Tag = 1
    Left = 296
    Top = 184
    Width = 81
    Height = 19
    DataField = 'iParcela'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit6: TDBEdit [28]
    Tag = 1
    Left = 128
    Top = 160
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 7
    OnKeyDown = DBEdit6KeyDown
  end
  object DBEdit9: TDBEdit [29]
    Tag = 1
    Left = 712
    Top = 160
    Width = 25
    Height = 19
    DataField = 'cSenso'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit12: TDBEdit [30]
    Tag = 1
    Left = 128
    Top = 208
    Width = 81
    Height = 19
    DataField = 'dDtVenc'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit16: TDBEdit [31]
    Tag = 1
    Left = 296
    Top = 208
    Width = 81
    Height = 19
    DataField = 'nValTit'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit17: TDBEdit [32]
    Tag = 1
    Left = 480
    Top = 208
    Width = 81
    Height = 19
    DataField = 'nValLiq'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit18: TDBEdit [33]
    Tag = 1
    Left = 656
    Top = 232
    Width = 81
    Height = 19
    DataField = 'nSaldoTit'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit19: TDBEdit [34]
    Tag = 1
    Left = 128
    Top = 232
    Width = 81
    Height = 19
    DataField = 'nValJuro'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit20: TDBEdit [35]
    Tag = 1
    Left = 296
    Top = 232
    Width = 81
    Height = 19
    DataField = 'nValDesconto'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBEdit21: TDBEdit [36]
    Tag = 1
    Left = 264
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdSP'
    DataSource = dsMaster
    TabOrder = 15
  end
  object DBMemo1: TDBMemo [37]
    Tag = 1
    Left = 744
    Top = 64
    Width = 361
    Height = 211
    DataField = 'cObsTit'
    DataSource = dsMaster
    TabOrder = 16
  end
  object DBEdit34: TDBEdit [38]
    Tag = 1
    Left = 128
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdUnidadeNegocio'
    DataSource = dsMaster
    TabOrder = 3
    OnKeyDown = DBEdit34KeyDown
  end
  object cxButton1: TcxButton [39]
    Left = 8
    Top = 584
    Width = 145
    Height = 33
    Caption = 'Incluir Movimento'
    TabOrder = 17
    TabStop = False
    OnClick = cxButton1Click
    Glyph.Data = {
      F6020000424DF60200000000000036000000280000000E000000100000000100
      180000000000C0020000C40E0000C40E00000000000000000000E9E9E9AAAAAA
      838383838383838383838383838383838383838383838383838383838383AAAA
      AAE9E9E90000AAAAAAF9F9F9FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
      FCFCFCFCFCFCFCFCFCFCF9F9F9AAAAAA0000838383FCFCFCFCFCFCFCFCFCFCFC
      FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFC83838300008383
      83FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFBFBFBFBFBFBFBFBFBFBFBFBFBFBFBFB
      FBFBFCFCFC8383830000838383FCFCFCFCFCFCFCFCFCFCFCFCFBFBFBFBFBFBFA
      FAFAFAFAFAFAFAFAFAFAFAFAFAFAFCFCFC8383830000838383FCFCFCFCFCFCFC
      FCFCFCFCFCFBFBFBFBFBFBFBFBFBFAFAFAFAFAFAF8F8F8F8F8F8FCFCFC838383
      0000838383FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFBFBFBF9F9F9
      F9F9F9F8F8F8FCFCFC8383830000838383FCFCFCFCFCFCFCFCFCFCFCFCFCFCFC
      FCFCFCFCFCFCFAFAFAF9F9F9F6F6F6F6F6F6FCFCFC8383830000838383FCFCFC
      FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFBFBFBF8F8F8F6F6F6F3F3F3F2F2F2FCFC
      FC8383830000838383FCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFBFBFBF8F8F8F5F5
      F5F2F2F2EFEFEFEDEDEDFCFCFC8383830000838383FCFCFCFBFBFBFCFCFCFCFC
      FCFBFBFBF8F8F8F5F5F5F1F1F1ECECECEAEAEAE6E6E6FCFCFC83838300008383
      83FCFCFCF9F9F9F9F9F9F9F9F9F7F7F7F6F6F6F2F2F2EBEBEBFCFCFCFCFCFCFC
      FCFCFCFCFC8383830000838383FCFCFCF7F7F7F9F9F9F7F7F7F7F7F7F3F3F3F0
      F0F0EAEAEAFCFCFCF6F6F6F4F4F4838383E9E9E90000838383FBFBFBF4F4F4F5
      F5F5F5F5F5F5F5F5F1F1F1EFEFEFE9E9E9FCFCFCE7E7E7838383E9E9E9E9E9E9
      0000AAAAAAF8F8F8FBFBFBFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCFCF8F8F8
      838383E9E9E9E9E9E9E9E9E90000E9E9E9AAAAAA838383838383838383838383
      838383838383838383838383E9E9E9E9E9E9E9E9E9E9E9E90000}
    LookAndFeel.Kind = lfOffice11
  end
  object cxButton2: TcxButton [40]
    Left = 160
    Top = 584
    Width = 145
    Height = 33
    Caption = 'Cancelar Movimento'
    TabOrder = 18
    TabStop = False
    OnClick = cxButton2Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1313F20000F10000F100
      00F10000EF0000EF0000ED1212EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFF1313F61A20F53C4CF93A49F83847F83545F83443F73242F7141BF11717
      EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1313F81D23F94453FA2429F91212F70F
      0FF60C0CF50909F5161BF53343F7141BF11717EFFFFFFFFFFFFFFFFFFF1313F9
      1F25FA4A58FB4247FBC9C9FD3B3BF91313F71010F63333F7C5C5FD3035F73444
      F7141BF21717EFFFFFFFFFFFFF0000FB4F5DFD3237FBCBCBFEF2F2FFEBEBFE3B
      3BF93939F8EAEAFEF1F1FEC5C5FD181DF63343F70000EFFFFFFFFFFFFF0000FD
      525FFD2828FC4747FCECECFFF2F2FFECECFFECECFEF1F1FFEAEAFE3434F70B0B
      F53545F80000EFFFFFFFFFFFFF0000FD5562FE2C2CFD2929FC4848FCEDEDFFF2
      F2FFF2F2FFECECFE3A3AF91212F70F0FF63848F80000F1FFFFFFFFFFFF0000FD
      5764FE3030FD2D2DFD4B4BFCEDEDFFF2F2FFF2F2FFECECFF3D3DF91616F81313
      F73C4BF80000F1FFFFFFFFFFFF0000FF5A67FE3333FE5050FDEDEDFFF3F3FFED
      EDFFEDEDFFF2F2FFECECFE3E3EFA1717F83F4EF90000F1FFFFFFFFFFFF0000FF
      5B68FF4347FECFCFFFF3F3FFEDEDFF4C4CFC4A4AFCECECFFF2F2FFCACAFE2A2F
      FA4251FA0000F3FFFFFFFFFFFF1414FF262BFF5D6AFF585BFFCFCFFF5252FE2F
      2FFD2C2CFD4B4BFCCCCCFE484CFB4957FB1D23F91414F6FFFFFFFFFFFFFFFFFF
      1414FF262BFF5D6AFF4347FF3434FE3232FE3030FD2D2DFD383CFC4F5DFC1F25
      FA1414F8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF1414FF262BFF5C69FF5B68FF5A
      67FE5865FE5663FE5461FE2227FC0D0DFBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFF1313FF0000FF0000FF0000FF0000FD0000FD0000FD1313FDFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    LookAndFeel.Kind = lfOffice11
  end
  object DBEdit7: TDBEdit [41]
    Tag = 1
    Left = 480
    Top = 184
    Width = 81
    Height = 19
    DataField = 'cNrNF'
    DataSource = dsMaster
    TabOrder = 19
  end
  object DBEdit8: TDBEdit [42]
    Tag = 1
    Left = 432
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdProvisaoTit'
    DataSource = dsMaster
    TabOrder = 20
  end
  object btAbatimento: TcxButton [43]
    Left = 312
    Top = 584
    Width = 145
    Height = 33
    Caption = 'Abatimento do Saldo'
    Enabled = False
    TabOrder = 21
    OnClick = btAbatimentoClick
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000E0F4FB82CFEC
      41B4E117A0D841AEDD80C8E8E0F1F9EFEFEFEFEFEFE0F2FB5293D90A51BE0442
      BC0A4FBD4486D0E0EFF898DBF25FCBED8AE0F688E3F96FDAF447C1E64FB4E040
      ACDD1596D4227ED02665CA2177E60579EA0164DD054EBD4F8BD365CCEE8EDDF4
      9EE6FC85DEFA7BDDFA6CD3F224A9DF61D1F172DEF90851BF639DF4187FFF0076
      F80076EE0368E10D51BD16B7E9BFF1FD74D9F75BD1F644CBF58EE6FC12A2DC47
      C7F42EB6F10442BCAECDFEFFFFFFFFFFFFFFFFFF187FEF0442BC18BBECBDEFFC
      99E3FB89DFFA74D9F963DAF813A8E05BD2F944C9F70653C08DB5F64D92FF1177
      FF2186FF408AEB0951BF19C0EFAAEAFB78DAF861D4F644CDF566DBF814ADE436
      C1F22BB7F11C87DD3775D28DB5F7B8D6FE72A8F52E6CCB6A9FDE1BC4F1CFF5FE
      C3F0FEBDEFFEA2E7FC99E8FC15B3E75ED3F948CCF836C2F54199E00E57C30543
      BC205AC188A7DEEFEFEF1BC8F4DAF7FED1F4FFC3F1FFB7EEFFBFF2FE17B7EB3A
      C4F32DBBF220B0EF51C7F412A5DFEFEFEFEFEFEFEFEFEFEFEFEF86E3FA91E1FA
      DFF8FFD7F7FFCCF4FFA9E9FA2AC2F064D6F94CCFF83BC6F668D6F914AAE2EFEF
      EFEFEFEFEFEFEFEFEFEFF3FCFF35D1F81ECBF61BC8F520C7F41AC2F279DCF83C
      C7F430BFF323B5F06CD7F914AFE5EFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEF
      EFEFEFEFEFEFEFEFEF19C3F1A2E9FC69D9FA51D2F93EC9F75ACCF515B3E8EFEF
      EFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEF1BC6F387E2FA40
      CBF534C4F325BAF15CCDF617B7EBEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEF
      EFEFEFEFEFEFEFEFEF1CC9F4BAF0FDAAEAFEA2E9FE79DDFB7BDFFB17BBEDEFEF
      EFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEF1CCCF6D6F7FFBC
      EFFFABEBFF9AE6FFA7EDFD18BFF0EFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEF
      EFEFEFEFEFEFEFEFEF29D1F7A2E7FBD3F6FFC7F4FFBBF1FFA2E9FB39CBF4EFEF
      EFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFEFDFF9FE36D4F921
      CFF71CCCF622CBF547D3F6B8EDFCEFEFEFEFEFEFEFEFEFEFEFEF}
    LookAndFeel.NativeStyle = True
  end
  object DBEdit10: TDBEdit [44]
    Tag = 1
    Left = 128
    Top = 256
    Width = 65
    Height = 19
    DataField = 'nCdContaBancaria'
    DataSource = dsContaBancaria
    TabOrder = 22
  end
  object DBEdit11: TDBEdit [45]
    Tag = 1
    Left = 196
    Top = 256
    Width = 45
    Height = 19
    DataField = 'nCdBanco'
    DataSource = dsContaBancaria
    TabOrder = 23
  end
  object DBEdit13: TDBEdit [46]
    Tag = 1
    Left = 244
    Top = 256
    Width = 49
    Height = 19
    DataField = 'cAgencia'
    DataSource = dsContaBancaria
    TabOrder = 24
  end
  object DBEdit14: TDBEdit [47]
    Tag = 1
    Left = 296
    Top = 256
    Width = 81
    Height = 19
    DataField = 'nCdCOnta'
    DataSource = dsContaBancaria
    TabOrder = 25
  end
  object DBEdit15: TDBEdit [48]
    Tag = 1
    Left = 380
    Top = 256
    Width = 357
    Height = 19
    DataField = 'cNmTitular'
    DataSource = dsContaBancaria
    TabOrder = 26
  end
  object DBEdit22: TDBEdit [49]
    Tag = 1
    Left = 128
    Top = 112
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsLoja
    TabOrder = 27
  end
  object DBEdit23: TDBEdit [50]
    Tag = 1
    Left = 196
    Top = 112
    Width = 541
    Height = 19
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 28
  end
  object DBEdit24: TDBEdit [51]
    Tag = 1
    Left = 196
    Top = 64
    Width = 541
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 29
  end
  object DBEdit25: TDBEdit [52]
    Tag = 1
    Left = 196
    Top = 88
    Width = 541
    Height = 19
    DataField = 'cNmUnidadeNegocio'
    DataSource = dsUnidadeNegocio
    TabOrder = 30
  end
  object DBEdit26: TDBEdit [53]
    Tag = 1
    Left = 196
    Top = 136
    Width = 541
    Height = 19
    DataField = 'cNmEspTit'
    DataSource = dsEspTit
    TabOrder = 31
  end
  object DBEdit27: TDBEdit [54]
    Tag = 1
    Left = 196
    Top = 160
    Width = 365
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 32
  end
  object cxPageControl1: TcxPageControl [55]
    Left = 3
    Top = 288
    Width = 1302
    Height = 281
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 33
    ClientRectBottom = 277
    ClientRectLeft = 4
    ClientRectRight = 1298
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Movimentos do T'#237'tulo'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1294
        Height = 253
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsMovimento
        DrawMemoText = True
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            EditButtons = <>
            FieldName = 'Movimento|ID'
            Footers = <>
            Width = 42
          end
          item
            EditButtons = <>
            FieldName = 'Opera'#231#227'o | Conta'
            Footers = <>
            Width = 108
          end
          item
            EditButtons = <>
            FieldName = 'Opera'#231#227'o | C'#243'digo'
            Footers = <>
            Width = 30
          end
          item
            EditButtons = <>
            FieldName = 'Opera'#231#227'o | Descri'#231#227'o'
            Footers = <>
            Width = 105
          end
          item
            EditButtons = <>
            FieldName = 'Forma Movimento | C'#243'digo'
            Footers = <>
            Width = 28
          end
          item
            EditButtons = <>
            FieldName = 'Forma Movimento | Descri'#231#227'o'
            Footers = <>
            Width = 102
          end
          item
            EditButtons = <>
            FieldName = 'Movimento|Nr.Cheque'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'Movimento|Valor'
            Footers = <>
            Width = 61
          end
          item
            EditButtons = <>
            FieldName = 'Movimento|Data'
            Footers = <>
            Width = 73
          end
          item
            EditButtons = <>
            FieldName = 'Movimento|Cadastro'
            Footers = <>
            Width = 78
          end
          item
            EditButtons = <>
            FieldName = 'Movimento|Cancel.'
            Footers = <>
            Width = 76
          end
          item
            EditButtons = <>
            FieldName = 'Movimento|Nr. Doc'
            Footers = <>
            Width = 51
          end
          item
            EditButtons = <>
            FieldName = 'Movimento|Contab.'
            Footers = <>
            Width = 68
          end
          item
            EditButtons = <>
            FieldName = 'cObsMov'
            Footers = <>
            Title.Alignment = taLeftJustify
            Width = 359
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit28: TDBEdit [56]
    Tag = 1
    Left = 480
    Top = 232
    Width = 81
    Height = 19
    DataField = 'nValAbatimento'
    DataSource = dsMaster
    TabOrder = 34
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    CommandTimeout = 50
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdUsuario'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM TITULO'
      
        '       INNER JOIN UsuarioEspTit ON UsuarioEspTit.nCdEspTit = Tit' +
        'ulo.nCdEspTit'
      ' WHERE nCdTitulo = :nPK'
      '   AND UsuarioEspTit.nCdUsuario =:nCdUsuario'
      '   AND UsuarioEspTit.cFlgTipoAcesso = '#39'M'#39)
    Left = 264
    Top = 416
    object qryMasternCdTitulo: TIntegerField
      Tag = 1
      FieldName = 'nCdTitulo'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryMastercNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryMasteriParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasternCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryMasternCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMastercSenso: TStringField
      Tag = 1
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryMasterdDtEmissao: TDateTimeField
      Tag = 1
      FieldName = 'dDtEmissao'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtReceb: TDateTimeField
      Tag = 1
      FieldName = 'dDtReceb'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtVenc: TDateTimeField
      Tag = 1
      FieldName = 'dDtVenc'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtLiq: TDateTimeField
      Tag = 1
      FieldName = 'dDtLiq'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtCad: TDateTimeField
      Tag = 1
      FieldName = 'dDtCad'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtCancel: TDateTimeField
      Tag = 1
      FieldName = 'dDtCancel'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasternValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValLiq: TBCDField
      Tag = 1
      FieldName = 'nValLiq'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternSaldoTit: TBCDField
      Tag = 1
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValJuro: TBCDField
      Tag = 1
      FieldName = 'nValJuro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternValDesconto: TBCDField
      Tag = 1
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercObsTit: TMemoField
      FieldName = 'cObsTit'
      BlobType = ftMemo
    end
    object qryMasternCdSP: TIntegerField
      Tag = 1
      FieldName = 'nCdSP'
    end
    object qryMasternCdBancoPortador: TIntegerField
      FieldName = 'nCdBancoPortador'
    end
    object qryMasterdDtRemPortador: TDateTimeField
      FieldName = 'dDtRemPortador'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtBloqTit: TDateTimeField
      FieldName = 'dDtBloqTit'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMastercObsBloqTit: TStringField
      FieldName = 'cObsBloqTit'
      Size = 50
    end
    object qryMasternCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryMasternCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMastercNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryMasternCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
    object qryMasternCdContaBancariaDA: TIntegerField
      FieldName = 'nCdContaBancariaDA'
    end
    object qryMasternCdTipoLanctoDA: TIntegerField
      FieldName = 'nCdTipoLanctoDA'
    end
    object qryMasternCdLojaTit: TIntegerField
      FieldName = 'nCdLojaTit'
    end
    object qryMasternValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  inherited dsMaster: TDataSource
    Left = 264
    Top = 448
  end
  inherited qryID: TADOQuery
    Left = 616
    Top = 416
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 720
    Top = 448
  end
  inherited qryStat: TADOQuery
    Left = 648
    Top = 416
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 712
    Top = 416
  end
  inherited ImageList1: TImageList
    Left = 744
    Top = 416
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      ',cSigla'
      ',cNmEmpresa'
      'FROM EMPRESA'
      'WHERE nCdEmpresa = :nPK'
      'AND nCdStatus = 1')
    Left = 296
    Top = 416
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryEspTit: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM EspTit'
      'WHERE nCdEspTit = :nPK')
    Left = 392
    Top = 416
    object qryEspTitnCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryEspTitnCdGrupoEspTit: TIntegerField
      FieldName = 'nCdGrupoEspTit'
    end
    object qryEspTitcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryEspTitcTipoMov: TStringField
      FieldName = 'cTipoMov'
      FixedChar = True
      Size = 1
    end
    object qryEspTitcFlgPrev: TIntegerField
      FieldName = 'cFlgPrev'
    end
  end
  object qryCategFinanc: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM CategFinanc')
    Left = 648
    Top = 448
    object qryCategFinancnCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryCategFinanccNmCategFinanc: TStringField
      FieldName = 'cNmCategFinanc'
      Size = 50
    end
    object qryCategFinancnCdGrupoCategFinanc: TIntegerField
      FieldName = 'nCdGrupoCategFinanc'
    end
    object qryCategFinanccFlgRecDes: TStringField
      FieldName = 'cFlgRecDes'
      FixedChar = True
      Size = 1
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      ',nCdStatus'
      ',nCdMoeda'
      ',nCdGrupoEconomico'
      'FROM TERCEIRO'
      'WHERE nCdTerceiro = :nPK')
    Left = 424
    Top = 416
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryTerceironCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryTerceironCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryTerceironCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
  end
  object qryMoeda: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM MOEDA')
    Left = 680
    Top = 416
    object qryMoedanCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryMoedacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 10
    end
    object qryMoedacNmMoeda: TStringField
      FieldName = 'cNmMoeda'
      Size = 50
    end
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM UnidadeNegocio'
      'WHERE nCdUnidadeNegocio = :nPK')
    Left = 328
    Top = 416
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM CentroCusto'
      'WHERE nCdStatus = 1'
      'AND cFlgLanc = 1'
      'AND iNivel = 3')
    Left = 584
    Top = 448
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
    object qryCCnCdCC1: TIntegerField
      FieldName = 'nCdCC1'
    end
    object qryCCnCdCC2: TIntegerField
      FieldName = 'nCdCC2'
    end
    object qryCCcCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object qryCCcCdHie: TStringField
      FieldName = 'cCdHie'
      FixedChar = True
      Size = 4
    end
    object qryCCiNivel: TSmallintField
      FieldName = 'iNivel'
    end
    object qryCCcFlgLanc: TIntegerField
      FieldName = 'cFlgLanc'
    end
    object qryCCnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object qryMTitulo: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTitulo'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT MT.nCdMTitulo "Movimento|ID"'
      
        '      ,CASE WHEN nCdBanco IS NOT NULL THEN (Convert(VARCHAR,nCdB' +
        'anco) + '#39' - '#39' + Convert(VARCHAR,cAgencia) + '#39' - '#39' + nCdConta)'
      '            ELSE nCdConta'
      '       END as "Opera'#231#227'o | Conta"'
      '      ,MT.nCdOperacao as "Opera'#231#227'o | C'#243'digo"'
      '      ,OP.cNmOperacao as "Opera'#231#227'o | Descri'#231#227'o"'
      '      ,MT.nCdFormaPagto as "Forma Movimento | C'#243'digo"'
      '      ,FP.cNmFormaPagto as "Forma Movimento | Descri'#231#227'o"'
      '      ,iNrCheque as  "Movimento|Nr.Cheque"'
      
        '      ,nValMov   as "Movimento|Valor"                           ' +
        '   '
      '      ,dDtMov    as "Movimento|Data"             '
      '      ,dDtCad    as "Movimento|Cadastro"              '
      '      ,dDtCancel as "Movimento|Cancel."              '
      '      ,cNrDoc    as "Movimento|Nr. Doc"      '
      '      ,dDtContab as "Movimento|Contab."         '
      '     ,nCdUsuarioCancel'
      '     ,cFlgMovAutomatico'
      '     ,MT.nCdContaBancaria'
      '     ,nCdLanctoFin'
      '     ,cObsMov'
      '     ,nCdMTitulo'
      '    ,nCdTitulo'
      '  FROM MTitulo MT'
      
        '       LEFT  JOIN ContaBancaria CB ON CB.nCdContaBancaria = MT.n' +
        'CdContaBancaria'
      
        '       INNER JOIN Operacao      OP ON OP.nCdOperacao      = MT.n' +
        'CdOperacao'
      
        '       LEFT  JOIN FormaPagto    FP ON FP.nCdFormaPagto    = MT.n' +
        'CdFormaPagto'
      ' WHERE MT.nCdTitulo = :nCdTitulo'
      ' ORDER BY 1')
    Left = 488
    Top = 416
    object qryMTituloMovimentoID: TAutoIncField
      DisplayLabel = 'ID'
      FieldName = 'Movimento|ID'
      ReadOnly = True
    end
    object qryMTituloOperaoConta: TStringField
      FieldName = 'Opera'#231#227'o | Conta'
      ReadOnly = True
      Size = 55
    end
    object qryMTituloOperaoCdigo: TIntegerField
      DisplayLabel = 'Opera'#231#227'o | C'#243'd'
      FieldName = 'Opera'#231#227'o | C'#243'digo'
    end
    object qryMTituloOperaoDescrio: TStringField
      FieldName = 'Opera'#231#227'o | Descri'#231#227'o'
      Size = 50
    end
    object qryMTituloFormaMovimentoCdigo: TIntegerField
      DisplayLabel = 'Forma Movimento | C'#243'd'
      FieldName = 'Forma Movimento | C'#243'digo'
    end
    object qryMTituloFormaMovimentoDescrio: TStringField
      FieldName = 'Forma Movimento | Descri'#231#227'o'
      Size = 50
    end
    object qryMTituloMovimentoNrCheque: TIntegerField
      FieldName = 'Movimento|Nr.Cheque'
    end
    object qryMTituloMovimentoValor: TBCDField
      FieldName = 'Movimento|Valor'
      Precision = 12
      Size = 2
    end
    object qryMTituloMovimentoData: TDateTimeField
      FieldName = 'Movimento|Data'
    end
    object qryMTituloMovimentoCadastro: TDateTimeField
      FieldName = 'Movimento|Cadastro'
    end
    object qryMTituloMovimentoCancel: TDateTimeField
      FieldName = 'Movimento|Cancel.'
    end
    object qryMTituloMovimentoNrDoc: TStringField
      FieldName = 'Movimento|Nr. Doc'
      FixedChar = True
      Size = 15
    end
    object qryMTituloMovimentoContab: TDateTimeField
      FieldName = 'Movimento|Contab.'
    end
    object qryMTitulonCdUsuarioCancel: TIntegerField
      FieldName = 'nCdUsuarioCancel'
    end
    object qryMTitulocFlgMovAutomatico: TIntegerField
      FieldName = 'cFlgMovAutomatico'
    end
    object qryMTitulonCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryMTitulonCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMTitulocObsMov: TMemoField
      DisplayLabel = 'Observa'#231#227'o Movimento'
      FieldName = 'cObsMov'
      BlobType = ftMemo
    end
    object qryMTitulonCdMTitulo: TIntegerField
      FieldName = 'nCdMTitulo'
    end
    object qryMTitulonCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
  end
  object dsMovimento: TDataSource
    DataSet = qryMTitulo
    Left = 488
    Top = 448
  end
  object qryOperacao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdOperacao'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM OPERACAO'
      'WHERE nCdOperacao = :nCdOperacao')
    Left = 584
    Top = 416
    object qryOperacaonCdOperacao: TIntegerField
      FieldName = 'nCdOperacao'
    end
    object qryOperacaocNmOperacao: TStringField
      FieldName = 'cNmOperacao'
      Size = 50
    end
    object qryOperacaocTipoOper: TStringField
      FieldName = 'cTipoOper'
      FixedChar = True
      Size = 1
    end
    object qryOperacaocSinalOper: TStringField
      FieldName = 'cSinalOper'
      FixedChar = True
      Size = 1
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 616
    Top = 448
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      '      ,nCdBanco'
      '      ,cAgencia'
      '      ,nCdCOnta'
      '      ,cNmTitular'
      '  FROM ContaBancaria'
      ' WHERE nCdContaBancaria = :nPK'
      '   AND cFlgCaixa     = 0'
      '   AND cFlgCofre     = 0')
    Left = 456
    Top = 416
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancarianCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryContaBancariacAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryContaBancarianCdCOnta: TStringField
      FieldName = 'nCdCOnta'
      FixedChar = True
      Size = 15
    end
    object qryContaBancariacNmTitular: TStringField
      FieldName = 'cNmTitular'
      Size = 50
    end
  end
  object dsContaBancaria: TDataSource
    DataSet = qryContaBancaria
    Left = 456
    Top = 448
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 360
    Top = 448
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 296
    Top = 448
  end
  object dsUnidadeNegocio: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 328
    Top = 448
  end
  object dsEspTit: TDataSource
    DataSet = qryEspTit
    Left = 392
    Top = 448
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 424
    Top = 448
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 360
    Top = 416
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryContaBancariaMovto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM ContaBancaria '
      'WHERE nCdContaBancaria = :nPk')
    Left = 520
    Top = 415
    object qryContaBancariaMovtonCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariaMovtonCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryContaBancariaMovtonCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryContaBancariaMovtocAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryContaBancariaMovtonCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryContaBancariaMovtonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryContaBancariaMovtocFlgFluxo: TIntegerField
      FieldName = 'cFlgFluxo'
    end
    object qryContaBancariaMovtoiUltimoCheque: TIntegerField
      FieldName = 'iUltimoCheque'
    end
    object qryContaBancariaMovtoiUltimoBordero: TIntegerField
      FieldName = 'iUltimoBordero'
    end
    object qryContaBancariaMovtonValLimiteCredito: TBCDField
      FieldName = 'nValLimiteCredito'
      Precision = 12
      Size = 2
    end
    object qryContaBancariaMovtocFlgDeposito: TIntegerField
      FieldName = 'cFlgDeposito'
    end
    object qryContaBancariaMovtocFlgEmiteCheque: TIntegerField
      FieldName = 'cFlgEmiteCheque'
    end
    object qryContaBancariaMovtocFlgCaixa: TIntegerField
      FieldName = 'cFlgCaixa'
    end
    object qryContaBancariaMovtonCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryContaBancariaMovtonCdUsuarioOperador: TIntegerField
      FieldName = 'nCdUsuarioOperador'
    end
    object qryContaBancariaMovtonSaldoConta: TBCDField
      FieldName = 'nSaldoConta'
      Precision = 12
      Size = 2
    end
    object qryContaBancariaMovtocFlgCofre: TIntegerField
      FieldName = 'cFlgCofre'
    end
    object qryContaBancariaMovtodDtUltConciliacao: TDateTimeField
      FieldName = 'dDtUltConciliacao'
    end
    object qryContaBancariaMovtocFlgEmiteBoleto: TIntegerField
      FieldName = 'cFlgEmiteBoleto'
    end
    object qryContaBancariaMovtocNmTitular: TStringField
      FieldName = 'cNmTitular'
      Size = 50
    end
    object qryContaBancariaMovtocFlgProvTit: TIntegerField
      FieldName = 'cFlgProvTit'
    end
    object qryContaBancariaMovtonSaldoInicial: TBCDField
      FieldName = 'nSaldoInicial'
      Precision = 12
      Size = 2
    end
    object qryContaBancariaMovtodDtSaldoInicial: TDateTimeField
      FieldName = 'dDtSaldoInicial'
    end
    object qryContaBancariaMovtocFlgReapresentaCheque: TIntegerField
      FieldName = 'cFlgReapresentaCheque'
    end
    object qryContaBancariaMovtonCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryContaBancariaMovtodDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryContaBancariaMovtoiUltimoBoleto: TIntegerField
      FieldName = 'iUltimoBoleto'
    end
  end
  object qryTituloAdiantReceber: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM Titulo'
      'WHERE nCdMTituloGerador = :nPk')
    Left = 520
    Top = 447
    object qryTituloAdiantRecebernCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTituloAdiantRecebernCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryTituloAdiantRecebernCdEspTit: TIntegerField
      FieldName = 'nCdEspTit'
    end
    object qryTituloAdiantRecebercNrTit: TStringField
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTituloAdiantReceberiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryTituloAdiantRecebernCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTituloAdiantRecebernCdCategFinanc: TIntegerField
      FieldName = 'nCdCategFinanc'
    end
    object qryTituloAdiantRecebernCdMoeda: TIntegerField
      FieldName = 'nCdMoeda'
    end
    object qryTituloAdiantRecebercSenso: TStringField
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryTituloAdiantReceberdDtEmissao: TDateTimeField
      FieldName = 'dDtEmissao'
    end
    object qryTituloAdiantReceberdDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
    end
    object qryTituloAdiantReceberdDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTituloAdiantReceberdDtLiq: TDateTimeField
      FieldName = 'dDtLiq'
    end
    object qryTituloAdiantReceberdDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryTituloAdiantReceberdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryTituloAdiantRecebernValTit: TBCDField
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTituloAdiantRecebernValLiq: TBCDField
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryTituloAdiantRecebernSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTituloAdiantRecebernValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTituloAdiantRecebernValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryTituloAdiantRecebercObsTit: TMemoField
      FieldName = 'cObsTit'
      BlobType = ftMemo
    end
    object qryTituloAdiantRecebernCdSP: TIntegerField
      FieldName = 'nCdSP'
    end
    object qryTituloAdiantRecebernCdBancoPortador: TIntegerField
      FieldName = 'nCdBancoPortador'
    end
    object qryTituloAdiantReceberdDtRemPortador: TDateTimeField
      FieldName = 'dDtRemPortador'
    end
    object qryTituloAdiantReceberdDtBloqTit: TDateTimeField
      FieldName = 'dDtBloqTit'
    end
    object qryTituloAdiantRecebercObsBloqTit: TStringField
      FieldName = 'cObsBloqTit'
      Size = 50
    end
    object qryTituloAdiantRecebernCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryTituloAdiantRecebernCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryTituloAdiantReceberdDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryTituloAdiantRecebernCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryTituloAdiantReceberdDtCalcJuro: TDateTimeField
      FieldName = 'dDtCalcJuro'
    end
    object qryTituloAdiantRecebernCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryTituloAdiantRecebernCdCrediario: TIntegerField
      FieldName = 'nCdCrediario'
    end
    object qryTituloAdiantRecebernCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTituloAdiantRecebernCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryTituloAdiantRecebercFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryTituloAdiantRecebercFlgDocCobranca: TIntegerField
      FieldName = 'cFlgDocCobranca'
    end
    object qryTituloAdiantRecebercNrNF: TStringField
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTituloAdiantRecebernCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
    object qryTituloAdiantRecebercFlgDA: TIntegerField
      FieldName = 'cFlgDA'
    end
    object qryTituloAdiantRecebernCdContaBancariaDA: TIntegerField
      FieldName = 'nCdContaBancariaDA'
    end
    object qryTituloAdiantRecebernCdTipoLanctoDA: TIntegerField
      FieldName = 'nCdTipoLanctoDA'
    end
    object qryTituloAdiantRecebernCdLojaTit: TIntegerField
      FieldName = 'nCdLojaTit'
    end
    object qryTituloAdiantRecebercFlgInclusaoManual: TIntegerField
      FieldName = 'cFlgInclusaoManual'
    end
    object qryTituloAdiantReceberdDtVencOriginal: TDateTimeField
      FieldName = 'dDtVencOriginal'
    end
    object qryTituloAdiantRecebercFlgCartaoConciliado: TIntegerField
      FieldName = 'cFlgCartaoConciliado'
    end
    object qryTituloAdiantRecebernCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryTituloAdiantRecebernCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryTituloAdiantRecebernValTaxaOperadora: TBCDField
      FieldName = 'nValTaxaOperadora'
      Precision = 12
      Size = 2
    end
    object qryTituloAdiantRecebernCdLoteConciliacaoCartao: TIntegerField
      FieldName = 'nCdLoteConciliacaoCartao'
    end
    object qryTituloAdiantReceberdDtIntegracao: TDateTimeField
      FieldName = 'dDtIntegracao'
    end
    object qryTituloAdiantRecebernCdParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdParcContratoEmpImobiliario'
    end
    object qryTituloAdiantRecebernValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object qryTituloAdiantRecebernValMulta: TBCDField
      FieldName = 'nValMulta'
      Precision = 12
      Size = 2
    end
    object qryTituloAdiantReceberdDtNegativacao: TDateTimeField
      FieldName = 'dDtNegativacao'
    end
    object qryTituloAdiantRecebernCdUsuarioNeg: TIntegerField
      FieldName = 'nCdUsuarioNeg'
    end
    object qryTituloAdiantRecebernCdItemListaCobrancaNeg: TIntegerField
      FieldName = 'nCdItemListaCobrancaNeg'
    end
    object qryTituloAdiantRecebernCdLojaNeg: TIntegerField
      FieldName = 'nCdLojaNeg'
    end
    object qryTituloAdiantReceberdDtReabNeg: TDateTimeField
      FieldName = 'dDtReabNeg'
    end
    object qryTituloAdiantRecebercFlgCobradora: TIntegerField
      FieldName = 'cFlgCobradora'
    end
    object qryTituloAdiantRecebernCdCobradora: TIntegerField
      FieldName = 'nCdCobradora'
    end
    object qryTituloAdiantReceberdDtRemCobradora: TDateTimeField
      FieldName = 'dDtRemCobradora'
    end
    object qryTituloAdiantRecebernCdUsuarioRemCobradora: TIntegerField
      FieldName = 'nCdUsuarioRemCobradora'
    end
    object qryTituloAdiantRecebernCdItemListaCobRemCobradora: TIntegerField
      FieldName = 'nCdItemListaCobRemCobradora'
    end
    object qryTituloAdiantRecebercFlgReabManual: TIntegerField
      FieldName = 'cFlgReabManual'
    end
    object qryTituloAdiantRecebernCdUsuarioReabManual: TIntegerField
      FieldName = 'nCdUsuarioReabManual'
    end
    object qryTituloAdiantRecebernCdFormaPagtoTit: TIntegerField
      FieldName = 'nCdFormaPagtoTit'
    end
    object qryTituloAdiantRecebernPercTaxaJurosDia: TBCDField
      FieldName = 'nPercTaxaJurosDia'
      Precision = 12
    end
    object qryTituloAdiantRecebernPercTaxaMulta: TBCDField
      FieldName = 'nPercTaxaMulta'
      Precision = 12
    end
    object qryTituloAdiantReceberiDiaCarenciaJuros: TIntegerField
      FieldName = 'iDiaCarenciaJuros'
    end
    object qryTituloAdiantRecebercFlgRenegociado: TIntegerField
      FieldName = 'cFlgRenegociado'
    end
    object qryTituloAdiantRecebercCodBarra: TStringField
      FieldName = 'cCodBarra'
      Size = 100
    end
    object qryTituloAdiantRecebernValEncargoCobradora: TBCDField
      FieldName = 'nValEncargoCobradora'
      Precision = 12
      Size = 2
    end
    object qryTituloAdiantRecebercFlgRetCobradoraManual: TIntegerField
      FieldName = 'cFlgRetCobradoraManual'
    end
    object qryTituloAdiantRecebernCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryTituloAdiantReceberdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryTituloAdiantRecebernValIndiceCorrecao: TBCDField
      FieldName = 'nValIndiceCorrecao'
      Precision = 12
      Size = 8
    end
    object qryTituloAdiantRecebernValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      Precision = 12
      Size = 2
    end
    object qryTituloAdiantRecebercFlgNegativadoManual: TIntegerField
      FieldName = 'cFlgNegativadoManual'
    end
    object qryTituloAdiantRecebercIDExternoTit: TStringField
      FieldName = 'cIDExternoTit'
    end
    object qryTituloAdiantRecebernCdTipoAdiantamento: TIntegerField
      FieldName = 'nCdTipoAdiantamento'
    end
    object qryTituloAdiantRecebercFlgAdiantamento: TIntegerField
      FieldName = 'cFlgAdiantamento'
    end
    object qryTituloAdiantRecebernCdMTituloGerador: TIntegerField
      FieldName = 'nCdMTituloGerador'
    end
  end
  object qryMTituloAdiantReceber: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM MTitulo'
      'WHERE nCdTitulo = :nPk')
    Left = 552
    Top = 415
    object qryMTituloAdiantRecebernCdMTitulo: TIntegerField
      FieldName = 'nCdMTitulo'
    end
    object qryMTituloAdiantRecebernCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryMTituloAdiantRecebernCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryMTituloAdiantRecebernCdOperacao: TIntegerField
      FieldName = 'nCdOperacao'
    end
    object qryMTituloAdiantRecebernCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryMTituloAdiantReceberiNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryMTituloAdiantRecebernValMov: TBCDField
      FieldName = 'nValMov'
      Precision = 12
      Size = 2
    end
    object qryMTituloAdiantReceberdDtMov: TDateTimeField
      FieldName = 'dDtMov'
    end
    object qryMTituloAdiantReceberdDtCad: TDateTimeField
      FieldName = 'dDtCad'
    end
    object qryMTituloAdiantReceberdDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryMTituloAdiantRecebercNrDoc: TStringField
      FieldName = 'cNrDoc'
      FixedChar = True
      Size = 15
    end
    object qryMTituloAdiantReceberdDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryMTituloAdiantRecebernCdUsuarioCad: TIntegerField
      FieldName = 'nCdUsuarioCad'
    end
    object qryMTituloAdiantRecebernCdUsuarioCancel: TIntegerField
      FieldName = 'nCdUsuarioCancel'
    end
    object qryMTituloAdiantRecebercObsMov: TMemoField
      FieldName = 'cObsMov'
      BlobType = ftMemo
    end
    object qryMTituloAdiantRecebernCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMTituloAdiantRecebercFlgMovAutomatico: TIntegerField
      FieldName = 'cFlgMovAutomatico'
    end
    object qryMTituloAdiantReceberdDtBomPara: TDateTimeField
      FieldName = 'dDtBomPara'
    end
    object qryMTituloAdiantRecebernCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryMTituloAdiantRecebercFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryMTituloAdiantRecebernCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
    object qryMTituloAdiantRecebercFlgMovDA: TIntegerField
      FieldName = 'cFlgMovDA'
    end
    object qryMTituloAdiantReceberdDtIntegracao: TDateTimeField
      FieldName = 'dDtIntegracao'
    end
    object qryMTituloAdiantRecebernCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMTituloAdiantReceberdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
  end
  object qryMTitulo1: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTitulo'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMTitulo '
      '  FROM MTitulo '
      ' WHERE nCdTitulo = :nCdTitulo'
      ' ORDER BY 1'
      '')
    Left = 552
    Top = 448
    object qryMTitulo1nCdMTitulo: TIntegerField
      FieldName = 'nCdMTitulo'
    end
  end
  object qryAjustaSaldoCaixa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdContaBancaria'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtLancto'
        Size = -1
        Value = Null
      end
      item
        Name = 'nValLancto'
        Size = -1
        Value = Null
      end
      item
        Name = 'cSenso'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdContaBancaria int'
      '       ,@dDtLancto        varchar(10)'
      '       ,@nValLancto       decimal(12,2)'
      '       ,@cSenso           char(1)'
      ''
      'Set @nCdContaBancaria = :nCdContaBancaria'
      'Set @dDtLancto        = :dDtLancto'
      'Set @nValLancto       = :nValLancto'
      'Set @cSenso           = :cSenso'
      ''
      
        '/* Se for um movimento em um t'#237'tulo a receber, inverte os valore' +
        's para debitar do saldo da conta */'
      'IF (@cSenso = '#39'C'#39')'
      'BEGIN'
      ''
      '    Set @nValLancto = @nValLancto * -1'
      ''
      'END'
      ''
      'UPDATE SaldoContaBancaria'
      '   Set nSaldo            = nSaldo         + @nValLancto'
      '      ,nSaldoPrevisto    = nSaldoPrevisto + @nValLancto'
      ' WHERE nCdContaBancaria  = @nCdContaBancaria'
      '   AND dDtSaldo         >= Convert(DATETIME,@dDtLancto,103)'
      ''
      'UPDATE ContaBancaria'
      '   Set nSaldoConta       = nSaldoConta + @nValLancto'
      ' WHERE nCdContaBancaria  = @nCdContaBancaria'
      '')
    Left = 684
    Top = 448
  end
end
