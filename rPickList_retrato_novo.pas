unit rPickList_retrato_novo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, ExtCtrls, QuickRpt, QRCtrls, jpeg;

type
  TrptPickList_retrato_novo = class(TForm)
    QuickRep1: TQuickRep;
    qryPedido: TADOQuery;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    QRGroup1: TQRGroup;
    QRDBText4: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText5: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel7: TQRLabel;
    QRBand2: TQRBand;
    QRBand3: TQRBand;
    QRDBText30: TQRDBText;
    QRDBText27: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel11: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRLabel13: TQRLabel;
    QRShape4: TQRShape;
    QRTotalPagina: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel1: TQRLabel;
    QRLabel10: TQRLabel;
    QRDBText3: TQRDBText;
    QRGroup2: TQRGroup;
    QRDBText11: TQRDBText;
    QRLabel15: TQRLabel;
    QRShape1: TQRShape;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRBand4: TQRBand;
    QRLabel5: TQRLabel;
    QRExpr1: TQRExpr;
    QRShape2: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel8: TQRLabel;
    QRDBText6: TQRDBText;
    QRDBText8: TQRDBText;
    QRLabel9: TQRLabel;
    qryPickList: TADOQuery;
    IntegerField1: TIntegerField;
    DateTimeField1: TDateTimeField;
    IntegerField2: TIntegerField;
    StringField1: TStringField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    StringField2: TStringField;
    BCDField1: TBCDField;
    IntegerField5: TIntegerField;
    IntegerField6: TIntegerField;
    StringField3: TStringField;
    StringField4: TStringField;
    DateTimeField2: TDateTimeField;
    DateTimeField3: TDateTimeField;
    StringField5: TStringField;
    MemoField1: TMemoField;
    StringField6: TStringField;
    StringField7: TStringField;
    qryPedidonCdPedido: TIntegerField;
    qryPedidonCdPickList: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidonCdTerceiro: TIntegerField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidocEnderecoEntrega: TStringField;
    qryPedidodDtPrevEntIni: TDateTimeField;
    qryPedidodDtPrevEntFim: TDateTimeField;
    qryPedidonCdItemPedido: TIntegerField;
    qryPedidocOBS: TStringField;
    qryPedidonCdProduto: TIntegerField;
    qryPedidocNmProduto: TStringField;
    qryPedidonQtdePick: TBCDField;
    qryPedidonCdTipoItemPed: TIntegerField;
    qryPedidonCdAlaProducao: TIntegerField;
    qryPedidocNmAlaProducao: TStringField;
    qryPedidocComposicao: TStringField;
    qryPedidocNmFantasia: TStringField;
    qryPedidocNmRepresentante: TStringField;
    QRDBText10: TQRDBText;
    QRDBText12: TQRDBText;
    QRLabel12: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptPickList_retrato_novo: TrptPickList_retrato_novo;

implementation

{$R *.dfm}

end.
