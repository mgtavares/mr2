unit rAcompanhaGiroProduto_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptAcompanhaGiroProduto_view = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_ACOMPANHA_GIRO_PRODUTO: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    SPREL_ACOMPANHA_GIRO_PRODUTOnCdProduto: TIntegerField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnCdTabTipoProduto: TIntegerField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmProduto: TStringField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmMarca: TStringField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnQtdeEstoqueAtual: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOiDiasEstoqueMedio: TIntegerField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnQtdeVendaPeriodo: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnGiroPeriodo: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnQtdeVendaTotal: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnGiroTotal: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnPercVenda: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOnPercVendaIdeal: TFloatField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmDepartamento: TStringField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmCategoria: TStringField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmSubCategoria: TStringField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmSegmento: TStringField;
    SPREL_ACOMPANHA_GIRO_PRODUTOcNmMarca_1: TStringField;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRShape2: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRLabel11: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    procedure QRBand3BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
    bZebrado : boolean ;
  end;

var
  rptAcompanhaGiroProduto_view: TrptAcompanhaGiroProduto_view;

implementation

{$R *.dfm}

procedure TrptAcompanhaGiroProduto_view.QRBand3BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin

    if (bZebrado) then
        if (QRBand3.Color = clSilver) then QRBand3.Color := clWhite
        else QRBand3.Color := clSilver ;

end;

end.
