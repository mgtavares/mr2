unit fConfirmaReceb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, DB, ADODB, DBGridEhGrouping, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGrid, ToolCtrlsEh;

type
  TfrmConfirmaReceb = class(TfrmProcesso_Padrao)
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryRecebimento: TADOQuery;
    DataSource1: TDataSource;
    qryRecebimentocNmEmpresa: TStringField;
    qryRecebimentocNmLoja: TStringField;
    qryRecebimentonCdRecebimento: TIntegerField;
    qryRecebimentodDtReceb: TDateTimeField;
    qryRecebimentocNmTerceiro: TStringField;
    qryRecebimentocNrDocto: TStringField;
    DBGridEh2: TDBGridEh;
    qryItem: TADOQuery;
    DataSource2: TDataSource;
    qryItemnCdProduto: TIntegerField;
    qryItemcNmItem: TStringField;
    qryItemnQtde: TBCDField;
    qryAux: TADOQuery;
    usp_processo: TADOStoredProc;
    SP_AUTORIZA_PAGAMENTO_RECEBIMENTO: TADOStoredProc;
    qryItemcSiglaUnidadeMedida: TStringField;
    qryRecebimentonValTotalNF: TBCDField;
    qryRecebimentonValDocto: TBCDField;
    qryRecebimentonCdEmpresa: TStringField;
    qryRecebimentonCdLoja: TStringField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdRecebimento: TcxGridDBColumn;
    cxGridDBTableView1dDtReceb: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView1cNrDocto: TcxGridDBColumn;
    cxGridDBTableView1nValTotalNF: TcxGridDBColumn;
    cxGridDBTableView1nCdEmpresa: TcxGridDBColumn;
    cxGridDBTableView1nCdLoja: TcxGridDBColumn;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    qryRecebimentocFlgRecebTransfEst: TIntegerField;
    qryAtualizaTransfEst: TADOQuery;
    SP_GERA_ORDEM_SEPARACAO_DISTRIBUICAO: TADOStoredProc;
    procedure FormShow(Sender: TObject);
    procedure DBGridEh2Exit(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfirmaReceb: TfrmConfirmaReceb;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmConfirmaReceb.FormShow(Sender: TObject);
begin
  inherited;

  qryRecebimento.Close ;
  qryRecebimento.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryRecebimento.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryRecebimento.Open ;

  if (qryRecebimento.eof) then
  begin
      ShowMessage('Nenhum recebimento pendente de autoriza��o ou nenhum que voc� possa confirmar.') ;
  end ;

end;

procedure TfrmConfirmaReceb.DBGridEh2Exit(Sender: TObject);
begin
  inherited;
  DbGridEh2.Visible := False ;
  qryItem.Close ;
end;

procedure TfrmConfirmaReceb.ToolButton5Click(Sender: TObject);
begin

  DbGridEh2.Visible := False ;
  if not qryRecebimento.Active or (qryRecebimentonCdRecebimento.Value = 0) then
  begin
      MensagemAlerta('Nenhum Recebimento Selecionado.') ;
      exit ;
  end ;

  case MessageDlg('O recebimento voltar� para a digita��o e ser� liberado para altera��es. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('UPDATE Recebimento Set nCdTabStatusReceb = 1 WHERE nCdRecebimento = ' + qryRecebimentonCdRecebimento.AsString) ;
      qryAux.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Processamento OK.') ;

  qryRecebimento.Close ;
  qryRecebimento.Open ;

end;

procedure TfrmConfirmaReceb.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryRecebimento.Requery();
  

end;

procedure TfrmConfirmaReceb.cxGridDBTableView1DblClick(Sender: TObject);
begin
  inherited;

  if not qryRecebimento.IsEmpty then
  begin
      PosicionaQuery(qryItem, qryRecebimentonCdRecebimento.AsString) ;
      DBGridEh2.Visible := True ;
      DBGridEh2.SetFocus ;
  end ;

end;

procedure TfrmConfirmaReceb.ToolButton6Click(Sender: TObject);
begin
  inherited;

  DbGridEh2.Visible := False ;
  if not qryRecebimento.Active or (qryRecebimentonCdRecebimento.Value = 0) then
  begin
      MensagemAlerta('Nenhum Recebimento Selecionado.') ;
      exit ;
  end ;

  case MessageDlg('Confirma o recebimento deste pedido?                 ' + #13#13 +'Ap�s este passo n�o ser� mais poss�vel estornar o recebimento.',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_processo.Close;
      usp_processo.Parameters.ParamByName('@nCdRecebimento').Value := qryRecebimentonCdRecebimento.Value ;
      usp_processo.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
      usp_processo.ExecProc;

      // Se o parametro estiver ativado, gera o pagamento do recebimento aqui.
      If (frmMenu.LeParametro('GERAFINANCCR') = 'S') then
      begin

          SP_AUTORIZA_PAGAMENTO_RECEBIMENTO.Close ;
          SP_AUTORIZA_PAGAMENTO_RECEBIMENTO.Parameters.ParamByName('@nCdRecebimento').Value := qryRecebimentonCdRecebimento.Value ;
          SP_AUTORIZA_PAGAMENTO_RECEBIMENTO.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
          SP_AUTORIZA_PAGAMENTO_RECEBIMENTO.Parameters.ParamByName('@cOBSTitulo').Value     := '' ;
          SP_AUTORIZA_PAGAMENTO_RECEBIMENTO.ExecProc;

      end ;

     {--  Gera ordem de distribui��o para os itens pr� distribuidos --}
     SP_GERA_ORDEM_SEPARACAO_DISTRIBUICAO.Close;
     SP_GERA_ORDEM_SEPARACAO_DISTRIBUICAO.Parameters.ParamByName('@nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva;
     SP_GERA_ORDEM_SEPARACAO_DISTRIBUICAO.Parameters.ParamByName('@nCdUsuario').Value     := frmMenu.nCdUsuarioLogado;
     SP_GERA_ORDEM_SEPARACAO_DISTRIBUICAO.Parameters.ParamByName('@nCdRecebimento').Value := qryRecebimentonCdRecebimento.Value;
     SP_GERA_ORDEM_SEPARACAO_DISTRIBUICAO.ExecProc;

      if qryRecebimentocFlgRecebTransfEst.Value = 1 then
      begin
         {-- Atualiza transferencias Faturadas --}
         qryAtualizaTransfEst.Close;
         qryAtualizaTransfEst.Parameters.ParamByName('nCdReceb').Value   := qryRecebimentonCdRecebimento.Value;
         qryAtualizaTransfEst.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
         qryAtualizaTransfEst.ExecSQL;

      end

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  ShowMessage('Recebimento confirmado com sucesso!') ;

  qryRecebimento.Close ;
  qryRecebimento.Open  ;
  
end;

initialization
    RegisterClass(TfrmConfirmaReceb) ;

end.
