unit rProtocoloConsignacao_view;

interface

uses Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, Forms, QuickRpt, QRCtrls, DB, ADODB, jpeg;

type
  TrptProtocoloConsignacao_view = class(TQuickRep)
    QRBand1: TQRBand;
    QRLabel1: TQRLabel;
    QRSysData1: TQRSysData;
    QRLabel3: TQRLabel;
    QRSysData2: TQRSysData;
    QRLabel2: TQRLabel;
    QRBand2: TQRBand;
    DetailBand1: TQRBand;
    QRLabel13: TQRLabel;
    QRShape6: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    qryConsignacao: TADOQuery;
    qryItemConsignacao: TADOQuery;
    QRLabel16: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRLabel20: TQRLabel;
    QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRLabel5: TQRLabel;
    cmdPreparaTemp: TADOCommand;
    SPREL_CONSIGNACAO: TADOStoredProc;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRDBText18: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    lblEmpresa: TQRLabel;
    ColumnHeaderBand1: TQRBand;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel30: TQRLabel;
    QRLabel31: TQRLabel;
    QRLabel32: TQRLabel;
    QRLabel33: TQRLabel;
    QRBand3: TQRBand;
    qryTotal: TADOQuery;
    QRDBText24: TQRDBText;
    QRLabel7: TQRLabel;
    QRShape2: TQRShape;
    QRShape3: TQRShape;
    QRLabel4: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText25: TQRDBText;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel25: TQRLabel;
    QRLabel28: TQRLabel;
    QRLabel29: TQRLabel;
    QRLabel34: TQRLabel;
    QRLabel6: TQRLabel;
    qryTotalPedido: TADOQuery;
    QRLabel24: TQRLabel;
    QRLabel26: TQRLabel;
    QRLabel27: TQRLabel;
    qryTotalDevolvida: TADOQuery;
    qryTotalRetirada: TADOQuery;
    QRShape4: TQRShape;
    QRChildBand1: TQRChildBand;
    QRExpr2: TQRExpr;
    QRExpr1: TQRExpr;
    QRExpr3: TQRExpr;
    qryConsignacaonCdConsignacao: TIntegerField;
    qryConsignacaocNmEmpresa: TStringField;
    qryConsignacaocNmLoja: TStringField;
    qryConsignacaocNmVendedor: TStringField;
    qryConsignacaonCdPedido: TIntegerField;
    qryConsignacaocNmTerceiro: TStringField;
    qryConsignacaocRG: TStringField;
    qryConsignacaocCNPJCPF: TStringField;
    qryConsignacaocEnderecoNumComp: TStringField;
    qryConsignacaocCepBairroCidade: TStringField;
    qryConsignacaocTelefones: TStringField;
    qryConsignacaodDtPedido: TStringField;
    qryConsignacaodDtDevolucao: TStringField;
    qryConsignacaodDtCancelamento: TStringField;
    qryConsignacaodDtFinalizacao: TStringField;
    qryConsignacaocTipoStatusConsig: TStringField;
    qryItemConsignacaonCdItemConsig: TIntegerField;
    qryItemConsignacaonCdConsignacao: TIntegerField;
    qryItemConsignacaonCdProduto: TIntegerField;
    qryItemConsignacaocNmProduto: TStringField;
    qryItemConsignacaocReferencia: TStringField;
    qryItemConsignacaonQtdePedido: TIntegerField;
    qryItemConsignacaonQtdeDevolvida: TIntegerField;
    qryItemConsignacaonQtdeRetirada: TIntegerField;
    qryItemConsignacaonValUnitario: TBCDField;
    qryItemConsignacaonValTotal: TBCDField;
    procedure qryConsignacaoAfterOpen(DataSet: TDataSet);
  private

  public

  end;

var
  rptProtocoloConsignacao_view: TrptProtocoloConsignacao_view;

implementation

uses fMenu;

{$R *.DFM}

procedure TrptProtocoloConsignacao_view.qryConsignacaoAfterOpen(
  DataSet: TDataSet);
begin
    {Oculta ou ajusta os campos que ser�o exibidos no relat�rio}

    if (qryConsignacaodDtDevolucao.IsNull) then
    begin
        QRLabel5.Caption := '';

        if not (qryConsignacaodDtCancelamento.IsNull) then
        begin
            QRLabel20.Top  := 80;
            QRDBText18.Top := 80;
        end;
    end;

    if (qryConsignacaodDtFinalizacao.IsNull) then
    begin
        QRLabel21.Caption := '';

        if not (qryConsignacaodDtCancelamento.IsNull) then
        begin
            QRLabel20.Top  := 96;
            QRDBText18.Top := 96;
        end;
    end;

    if (qryConsignacaodDtCancelamento.IsNull) then
        QRLabel20.Caption := '';

    if (qryConsignacaonCdPedido.IsNull) then
        QRLabel23.Caption := '';

    if (Trim(qryConsignacaocTelefones.Value) = '') then
        QRLabel6.Caption := '';
end;

end.
