unit fMonitorProducao_Exibe_ItensPedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DBGridEhGrouping, ToolCtrlsEh, DB, ADODB,
  GridsEh, DBGridEh, cxPC, cxControls, ImgList, ComCtrls, ToolWin, ExtCtrls;

type
  TfrmMonitorProducao_Exibe_ItensPedido = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryItemPedidoAberto: TADOQuery;
    qryItemPedidoAbertonCdProduto: TIntegerField;
    qryItemPedidoAbertocNmItem: TStringField;
    qryItemPedidoAbertonQtdePed: TBCDField;
    qryItemPedidoAbertonQtdeExpRec: TBCDField;
    qryItemPedidoAbertonQtdeSaldo: TBCDField;
    DataSource1: TDataSource;
    DBGridEh2: TDBGridEh;
    qryItemPedidoTotal: TADOQuery;
    IntegerField1: TIntegerField;
    StringField1: TStringField;
    BCDField1: TBCDField;
    BCDField2: TBCDField;
    BCDField3: TBCDField;
    DataSource2: TDataSource;
    procedure exibeItensPedido( nCdPedido : integer );
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMonitorProducao_Exibe_ItensPedido: TfrmMonitorProducao_Exibe_ItensPedido;

implementation

{$R *.dfm}

{ TfrmMonitorProducao_Exibe_ItensPedido }

procedure TfrmMonitorProducao_Exibe_ItensPedido.exibeItensPedido(
  nCdPedido: integer);
begin

    qryItemPedidoAberto.Close;
    qryItemPedidoAberto.Parameters.ParamByName('nCdPedido').Value := nCdPedido;
    qryItemPedidoAberto.Open;

    qryItemPedidoTotal.Close;
    qryItemPedidoTotal.Parameters.ParamByName('nCdPedido').Value := nCdPedido;
    qryItemPedidoTotal.Open;

    Self.ShowModal;

end;

procedure TfrmMonitorProducao_Exibe_ItensPedido.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  
  DBGridEh1.SetFocus;


end;

end.
