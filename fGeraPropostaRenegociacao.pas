unit fGeraPropostaRenegociacao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  DB, DBCtrls, ADODB, Mask, cxLookAndFeelPainters, GridsEh, DBGridEh,
  cxButtons, cxControls, cxContainer, cxEdit, cxTextEdit, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmGeraPropostaRenegociacao = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    qryLojaCobradora: TADOQuery;
    Label2: TLabel;
    qryLojaCobradoranCdLoja: TIntegerField;
    qryLojaCobradoracNmLoja: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    edtCliente: TMaskEdit;
    Label5: TLabel;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    DBEdit4: TDBEdit;
    GroupBox2: TGroupBox;
    btExibeTitulo: TcxButton;
    qryPopulaTemp: TADOQuery;
    qryPreparaTemp: TADOQuery;
    qryTitulos: TADOQuery;
    qryTitulosnCdTitulo: TIntegerField;
    qryTituloscNrTit: TStringField;
    qryTitulosiParcela: TIntegerField;
    qryTituloscNmEspTit: TStringField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTitulosiDiasAtraso: TIntegerField;
    qryTitulosnValTit: TBCDField;
    qryTitulosnValLiq: TBCDField;
    qryTitulosnValJuro: TBCDField;
    qryTitulosnValMulta: TBCDField;
    qryTitulosnValDesconto: TBCDField;
    qryTitulosnSaldoTit: TBCDField;
    qryTituloscFlgAux: TIntegerField;
    dsTitulos: TDataSource;
    qrySomaTitulos: TADOQuery;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryTituloscFlgRenegociado: TIntegerField;
    GroupBox3: TGroupBox;
    Label1: TLabel;
    cxTextEdit1: TcxTextEdit;
    qryTitulosnValAbatimento: TBCDField;
    qrySomaTitulosiQtdeTitulos: TIntegerField;
    qrySomaTitulosiMaxDiaAtraso: TIntegerField;
    qrySomaTitulosnValTotalTit: TBCDField;
    qrySomaTitulosnValTotalJuro: TBCDField;
    qrySomaTitulosnTotalSaldoTit: TBCDField;
    DBGridEh1: TDBGridEh;
    rgFormaReneg: TRadioGroup;
    qryTitulosnCdCobradora: TIntegerField;
    QryTestaCobradora: TADOQuery;
    QryTestaCobradoraiQuantCobradoras: TIntegerField;
    procedure btExibeTituloClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtClienteExit(Sender: TObject);
    procedure edtClienteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton5Click(Sender: TObject);
    procedure ChamadaListaCobranca(nCdTerceiro,__nCdItemListaCobranca:integer) ;
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure qryTitulosAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdItemListaCobranca : integer;
  end;

var
  frmGeraPropostaRenegociacao: TfrmGeraPropostaRenegociacao;

implementation

uses fMenu, fLookup_Padrao, fGeraPropostaRenegociacao_Simulacao;

{$R *.dfm}

procedure TfrmGeraPropostaRenegociacao.btExibeTituloClick(Sender: TObject);
begin
  inherited;

  qryPreparaTemp.ExecSQL;

  qryPopulaTemp.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryPopulaTemp.Parameters.ParamByName('nCdLoja').Value     := frmMenu.nCdLojaAtiva;
  qryPopulaTemp.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(edtCliente.Text);
  qryPopulaTemp.ExecSQL;

  qryTitulos.Close;
  qryTitulos.Open;

  if qryTitulos.Eof then
  begin
      Showmessage('Nenhum t�tulo pendente para este cliente.') ;
      abort ;
  end ;

end;

procedure TfrmGeraPropostaRenegociacao.FormShow(Sender: TObject);
begin
  inherited;

  qryLojaCobradora.Close;
  PosicionaQuery(qryLojaCobradora, intToStr(frmMenu.nCdLojaAtiva)) ;
  
end;

procedure TfrmGeraPropostaRenegociacao.edtClienteExit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, edtCliente.Text) ;
  
end;

procedure TfrmGeraPropostaRenegociacao.edtClienteKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(200);

        If (nPK > 0) then
            edtCliente.Text := intToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmGeraPropostaRenegociacao.ToolButton5Click(Sender: TObject);
var
  objForm : TfrmGeraPropostaRenegociacao_Simulacao;
begin
  if (not qryTitulos.Active) then
      exit ;

  if (qryTitulos.State <> dsBrowse) then
      qryTitulos.Post ;

  qrySomaTitulos.Close;
  qrySomaTitulos.Open;

  if (qrySomaTitulosiQtdeTitulos.Value <= 0) then
  begin
      ShowMessage('Nenhum t�tulo selecionado para renegocia��o.') ;
      exit ;
  end ;

  qryTestaCobradora.Close;
  qryTestaCobradora.Open;

  if (qryTestaCobradoraiQuantCobradoras.Value >1 ) then
  begin
      ShowMessage('Somente T�tulos de mesma cobradora podem ser selecionados. ' + qryTestaCobradoraiQuantCobradoras.AsString) ;
      exit ;
  end ;

  objForm := TfrmGeraPropostaRenegociacao_Simulacao.Create(Self);

  case (rgFormaReneg.ItemIndex) of
      0 : begin
              objForm.Caption           := 'Simula��o Renegocia��o �nica';
              objForm.cFlgRenegMultipla := 0;
              objForm.NovaProposta(qrySomaTitulosiMaxDiaAtraso.Value,qrySomaTitulosnValTotalTit.Value,qrySomaTitulosnValTotalJuro.Value,nCdItemListaCobranca,qryTerceironCdTerceiro.Value ) ;
          end;
      1 : begin
              objForm.Caption           := 'Simula��o Renegocia��o M�ltipla';
              objForm.cFlgRenegMultipla := 1;
              objForm.NovaProposta(qrySomaTitulosiMaxDiaAtraso.Value,qrySomaTitulosnValTotalTit.Value,qrySomaTitulosnValTotalJuro.Value,nCdItemListaCobranca,qryTerceironCdTerceiro.Value ) ;

              while (MessageDLG('Deseja gerar nova proposta de renegocia��o ?', mtConfirmation, mbOKCancel, 0) = mrYes) do
                  objForm.NovaProposta(qrySomaTitulosiMaxDiaAtraso.Value,qrySomaTitulosnValTotalTit.Value,qrySomaTitulosnValTotalJuro.Value,nCdItemListaCobranca,qryTerceironCdTerceiro.Value ) ;
          end;
  end;

  btExibeTitulo.Click;
end;

procedure TfrmGeraPropostaRenegociacao.ChamadaListaCobranca(
  nCdTerceiro,__nCdItemListaCobranca: integer);
begin
  edtCliente.Text := intToStr(nCdTerceiro) ;
  PosicionaQuery(qryTerceiro, edtCliente.Text) ;

  qryPreparaTemp.ExecSQL;

  qryPopulaTemp.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryPopulaTemp.Parameters.ParamByName('nCdLoja').Value     := frmMenu.nCdLojaAtiva;
  qryPopulaTemp.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(edtCliente.Text);
  qryPopulaTemp.ExecSQL;

  qryTitulos.Close;
  qryTitulos.Open;

  {--se o cliente n�o tem t�tulo aberto, avisa e fecha a janela --}
  if qryTitulos.Eof then
  begin
      Showmessage('Nenhum t�tulo pendente para este cliente.') ;
      PostMessage(Self.Handle, WM_CLOSE, 0, 0);
      close ;
  end ;

  GroupBox1.Enabled := False ;

  nCdItemListaCobranca := __nCdItemListaCobranca ;

  Self.ShowModal;

  GroupBox1.Enabled := True ;


end;

procedure TfrmGeraPropostaRenegociacao.DBGridEh1DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;
  // em atraso
  if (qryTituloscFlgRenegociado.Value = 1) and (DataCol IN [1,2,3,4,5,6,7]) then
  begin

     DBGridEh1.Canvas.Brush.Color := clRed;
     DBGridEh1.Canvas.Font.Color  := clWhite ;

     DBGridEh1.Canvas.FillRect(Rect);
     DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
  end;
end;

procedure TfrmGeraPropostaRenegociacao.qryTitulosAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  DBGridEh1.ReadOnly := (qryTituloscFlgRenegociado.Value = 1) ;
end;

initialization
    RegisterClass(TfrmGeraPropostaRenegociacao) ;

end.
