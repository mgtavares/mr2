unit fRequisicao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, DBCtrls, ExtCtrls, Mask, DB,
  ImgList, ADODB, ComCtrls, ToolWin, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, DBGridEhGrouping, ToolCtrlsEh, Buttons,
  Menus, ER2Lookup;

type
  TfrmRequisicao = class(TfrmCadastro_Padrao)
    qryMasternCdRequisicao: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTipoRequisicao: TIntegerField;
    qryMasterdDtRequisicao: TDateTimeField;
    qryMasterdDtAutor: TDateTimeField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMastercNmSolicitante: TStringField;
    qryMasternCdTabTipoRequis: TIntegerField;
    qryMastercNrFormulario: TStringField;
    qryMasternCdTabStatusRequis: TIntegerField;
    qryMastercOBS: TMemoField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    DBEdit8: TDBEdit;
    Label10: TLabel;
    DBEdit9: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit10: TDBEdit;
    dsEmpresa: TDataSource;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    DBEdit11: TDBEdit;
    dsLoja: TDataSource;
    qryTipoRequisicao: TADOQuery;
    qryTipoRequisicaonCdTipoRequisicao: TIntegerField;
    qryTipoRequisicaocNmTipoRequisicao: TStringField;
    DBEdit12: TDBEdit;
    dsTipoRequisicao: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryItens: TADOQuery;
    qryItensnCdRequisicao: TIntegerField;
    qryItensnCdProduto: TIntegerField;
    qryItensnQtdeReq: TBCDField;
    qryItensnQtdeAtend: TBCDField;
    qryItensnQtdeCanc: TBCDField;
    dsItens: TDataSource;
    qryTipoRequisicaonCdTabTipoRequis: TIntegerField;
    qryItenscNmItem: TStringField;
    qryTabStatusRequis: TADOQuery;
    qryTabStatusRequisnCdTabStatusRequis: TIntegerField;
    qryTabStatusRequiscNmTabStatusRequis: TStringField;
    Label11: TLabel;
    DBEdit13: TDBEdit;
    dsTabStatusRequis: TDataSource;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    DBEdit14: TDBEdit;
    dsUsuario: TDataSource;
    qryMasternCdSetor: TIntegerField;
    qrySetor: TADOQuery;
    qrySetorcNmSetor: TStringField;
    DBEdit6: TDBEdit;
    DBEdit15: TDBEdit;
    dsSetor: TDataSource;
    ToolButton10: TToolButton;
    qryItensnCdMapaCompra: TIntegerField;
    qryItensnCdPedido: TIntegerField;
    qryItenscReferencia: TStringField;
    qryItenscNotaComprador: TStringField;
    qryItenscFlgUrgente: TIntegerField;
    qryItenscJustificativa: TStringField;
    qryItenscDescricaoTecnica: TMemoField;
    qryItenscUnidadeMedida: TStringField;
    cxTabSheet2: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryProdutocReferencia: TStringField;
    qryTipoRequisicaocFlgExigeCC: TIntegerField;
    qryTipoRequisicaocExigeAutor: TIntegerField;
    qryCC: TADOQuery;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    DBEdit16: TDBEdit;
    dsCC: TDataSource;
    qryMasternCdCC: TIntegerField;
    Label12: TLabel;
    DBEdit17: TDBEdit;
    cxTabSheet3: TcxTabSheet;
    DBGridEh3: TDBGridEh;
    DBMemo2: TDBMemo;
    qryMastercFlgAutomatica: TIntegerField;
    qryMasternValRequisicao: TBCDField;
    qryItensnValTotal: TBCDField;
    qryProdutonValCusto: TBCDField;
    qryItensnValUnitario: TBCDField;
    qryAux: TADOQuery;
    qryProdutocUnidadeMedida: TStringField;
    qryItensnCdTipoItemRequisicao: TIntegerField;
    cxTabSheet4: TcxTabSheet;
    DBGridEh4: TDBGridEh;
    qryItensAD: TADOQuery;
    qryItensADnCdItemRequisicao: TAutoIncField;
    qryItensADnCdRequisicao: TIntegerField;
    qryItensADnCdProduto: TIntegerField;
    qryItensADcNmItem: TStringField;
    qryItensADnQtdeReq: TBCDField;
    qryItensADnQtdeAtend: TBCDField;
    qryItensADnQtdeCanc: TBCDField;
    qryItensADnCdMapaCompra: TIntegerField;
    qryItensADnCdPedido: TIntegerField;
    qryItensADcReferencia: TStringField;
    qryItensADcNotaComprador: TStringField;
    qryItensADcFlgUrgente: TIntegerField;
    qryItensADcJustificativa: TStringField;
    qryItensADcDescricaoTecnica: TMemoField;
    qryItensADcUnidadeMedida: TStringField;
    qryItensADnValUnitario: TBCDField;
    qryItensADnValTotal: TBCDField;
    qryItensADnCdTipoItemRequisicao: TIntegerField;
    dsItemAD: TDataSource;
    qryMasterdDtFinalizacao: TDateTimeField;
    Label14: TLabel;
    DBEdit19: TDBEdit;
    btnFinalizar: TToolButton;
    ToolButton13: TToolButton;
    tabObservacao: TcxTabSheet;
    DBMemo1: TDBMemo;
    Image2: TImage;
    qryMasterdDtPrevAtendIni: TDateTimeField;
    qryMasterdDtPrevAtendFim: TDateTimeField;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    Label9: TLabel;
    Label15: TLabel;
    qryFollowUp: TADOQuery;
    qryFollowUpdDtFollowUp: TDateTimeField;
    qryFollowUpcNmUsuario: TStringField;
    qryFollowUpcOcorrenciaResum: TStringField;
    qryFollowUpdDtProxAcao: TDateTimeField;
    qryFollowUpcOcorrencia: TMemoField;
    Image3: TImage;
    DBMemo3: TDBMemo;
    cxTabsheet5: TcxTabSheet;
    cxTabSheet6: TcxTabSheet;
    DBGridEh5: TDBGridEh;
    DBMemo4: TDBMemo;
    dsFollowUp: TDataSource;
    btOpcao: TToolButton;
    menuOpcao: TPopupMenu;
    btImprimirRequis: TMenuItem;
    N1: TMenuItem;
    btCancelarRequis: TMenuItem;
    qryItensnCdItemRequisicao: TIntegerField;
    qryCentroDistribuicao: TADOQuery;
    qryMasternCdServidorOrigem: TIntegerField;
    qryMasterdDtReplicacao: TDateTimeField;
    qryMasternCdOrdemProducao: TIntegerField;
    qryMasternCdEtapaOrdemProducao: TIntegerField;
    qryMastercFlgCritico: TIntegerField;
    qryMasterdDtPrevAtendIniOriginal: TDateTimeField;
    qryMasterdDtPrevAtendFimOriginal: TDateTimeField;
    qryMasternCdCentroDistribuicao: TIntegerField;
    dsCentroDistribuicao: TDataSource;
    qryCentroDistribuicaonCdCentroDistribuicao: TIntegerField;
    qryCentroDistribuicaocNmCentroDistribuicao: TStringField;
    Label16: TLabel;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    Panel2: TPanel;
    btExcluirItens: TcxButton;
    btLeitura: TcxButton;
    qryGravaConfPadrao: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit9Exit(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryItensBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBEdit9KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6Exit(Sender: TObject);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit17Exit(Sender: TObject);
    procedure DBEdit17KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTipoRequisicaoAfterScroll(DataSet: TDataSet);
    procedure btCancelarClick(Sender: TObject);
    procedure qryTipoRequisicaoAfterCancel(DataSet: TDataSet);
    procedure DBGridEh1ColEnter(Sender: TObject);
    procedure qryItensADBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure btImprimirRequisClick(Sender: TObject);
    procedure btCancelarRequisClick(Sender: TObject);
    procedure qryTipoRequisicaoAfterOpen(DataSet: TDataSet);
    procedure qryCCBeforeOpen(DataSet: TDataSet);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit23Exit(Sender: TObject);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btLeituraClick(Sender: TObject);
    procedure btExcluirItensClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SalvarProc : boolean;
    nCdLojaOld : integer;
  end;

var
  frmRequisicao: TfrmRequisicao;

implementation

uses fMenu, fLookup_Padrao, rRequisicao_view, fConferenciaProdutoPadrao;

{$R *.dfm}

procedure TfrmRequisicao.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'REQUISICAO' ;
  nCdTabelaSistema  := 54 ;
  nCdConsultaPadrao := 120 ;
end;

procedure TfrmRequisicao.btIncluirClick(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

  cxTabSheet1.Enabled := True ;
  cxTabSheet2.Enabled := False ;
  cxTabSheet3.Enabled := False ;
  cxTabSheet4.Enabled := False ;

  PosicionaQuery(qryEmpresa, IntToStr(frmMenu.nCdEmpresaAtiva)) ;
  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;

  if (frmMenu.LeParametro('VAREJO') = 'S') then
  begin
      //qryMasternCdLoja.Value := frmMenu.nCdLojaAtiva;
      //PosicionaQuery(qryLoja, IntToStr(frmMenu.nCdLojaAtiva)) ;
      ativaDBEdit(DbEdit3);
      DbEdit3.SetFocus ;
  end
  else
  begin
      DbEdit9.SetFocus ;
  end;

  qryMasterdDtRequisicao.Value    := Now() ;
  qryMasternCdTabTipoRequis.Value := 1 ;
end;

procedure TfrmRequisicao.DBEdit9Exit(Sender: TObject);
var
    nPK : integer ;
begin
  inherited;

  qryTipoRequisicao.Close ;
  PosicionaQuery(qryTipoRequisicao, DBEdit9.Text) ;

  if DBEdit23.Text <> '' then
      Abort;

  if not (qryTipoRequisicao.eof) then
  begin
      if (not DBEdit23.ReadOnly) then
      begin

          if DBEdit3.Text = '' then
          begin
              MensagemAlerta('Informe a loja para selecionar o centro de distribui��o.');
              DBEdit3.SetFocus;
              Abort;
          end;

          nPK := frmLookup_Padrao.ExecutaConsulta2(782,' EXISTS (SELECT 1 FROM LojaCentroDistribuicao WHERE nCdLoja = ' + qryMasternCdLoja.AsString + '  AND LojaCentroDistribuicao.nCdCentroDistribuicao = CentroDistribuicao.nCdCentroDistribuicao)');

          if (nPK > 0) then
          begin
              qryMasternCdCentroDistribuicao.Value := nPK;

              qryCentroDistribuicao.Close ;
              qryCentroDistribuicao.Parameters.ParamByName('nPK').Value     := StrToInt(DBEdit23.Text)   ;
              qryCentroDistribuicao.Parameters.ParamByName('nCdLoja').Value := StrToInt(DBEdit3.Text) ;
              qryCentroDistribuicao.Open;
              
          end;

          DBEdit17.SetFocus;
      end
      else
          qryCentroDistribuicao.Close;

      nPK := frmLookup_Padrao.ExecutaConsulta2(157,'EXISTS(SELECT 1 FROM CentroCustoTipoRequisicao CCTR WHERE CCTR.nCdCC = CentroCusto.nCdCC AND CCTR.nCdTipoRequisicao = '+ qryTipoRequisicaonCdTipoRequisicao.asString +')');

      if (nPK > 0) then
      begin
          qryMasternCdCC.Value := nPK ;
          PosicionaQuery(qryCC, IntToStr(nPK)) ;
      end
      else
          qryMasternCdCC.Clear;

  end ;

end;

procedure TfrmRequisicao.qryMasterBeforePost(DataSet: TDataSet);
var
  nValTotal : Double;
begin

  if qryMaster.IsEmpty then
      Abort;

  if ((not DBEdit3.ReadOnly) and (DBEdit11.Text = '')) then
  begin
      MensagemAlerta('Informe a loja requisitante.');
      DBEdit3.SetFocus;
      Abort;
  end;

  if (qryTipoRequisicao.IsEmpty) then
  begin
      MensagemAlerta('Informe o tipo de requisi��o.') ;
      DbEdit9.SetFocus;
      abort ;
  end ;

  if (qrySetor.IsEmpty) then
  begin
      MensagemAlerta('Informe o setor requisitante.') ;
      DbEdit6.SetFocus;
      abort ;
  end ;

  if (Trim(qryMastercNmSolicitante.Value) = '') then
  begin
      MensagemAlerta('Informe o solicitante.') ;
      DbEdit7.SetFocus;
      abort ;
  end ;

  if ((not DBEdit23.ReadOnly) and (qryCentroDistribuicao.IsEmpty)) then
  begin
      MensagemAlerta('Informe um centro de distribui��o.');
      DBEdit23.SetFocus;
      Abort;
  end;

  if (qryTipoRequisicaocFlgExigeCC.Value = 1) AND (qryCC.IsEmpty) then
  begin
      MensagemAlerta('Selecione um centro de custo.') ;
      DBEdit17.SetFocus;
      Abort ;
  end ;

  if (qryMasterdDtPrevAtendIni.IsNull) then
  begin
      MensagemAlerta('Informe a data inicial da previs�o de atendimento.');
      DBEdit20.SetFocus;
      Abort;
  end;

  if (qryMasterdDtPrevAtendFim.IsNull) then
  begin
      MensagemAlerta('Informe a data final da previs�o de atendimento.');
      DBEdit21.SetFocus;
      Abort;
  end;

  if DateToStr(qryMasterdDtPrevAtendIni.Value) < DateToStr(qryMasterdDtRequisicao.Value) then
  begin
      MensagemAlerta('A data da previs�o de atendimento inicial � menor do que a data da requisi��o.');
      DBEdit20.SetFocus;
      Abort;
  end;

  if (not frmMenu.fnValidaPeriodo(qryMasterdDtPrevAtendIni.Value,qryMasterdDtPrevAtendFim.Value)) then
  begin
      DBEdit20.SetFocus;
      Abort;
  end;

  inherited;

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabTipoRequis.Value   := qryTipoRequisicaonCdTabTipoRequis.Value ;
      qryMasternCdTabStatusRequis.Value := 1 ;
  end ;

  nValTotal := 0 ;

  if (qryItens.Active) and (qryItens.RecordCount > 0) then
  begin
      qryItens.First ;
      while not qryItens.Eof do
      begin
          nValTotal := nValTotal + qryItensnValTotal.Value ;
          qryItens.Next ;
      end ;
      qryItens.First ;
  end ;

  if (qryItensAD.Active) and (qryItensAD.RecordCount > 0) then
  begin
      qryItensAD.First ;
      while not qryItensAD.Eof do
      begin
          nValTotal := nValTotal + qryItensADnValTotal.Value ;
          qryItensAD.Next ;
      end ;
      qryItensAD.First ;
  end ;

  qryMasternValRequisicao.Value := nValTotal ;
end;

procedure TfrmRequisicao.qryItensBeforePost(DataSet: TDataSet);
begin

  if (qryItenscNmItem.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o do item.') ;
      abort ;
  end ;

  if (qryItenscUnidadeMedida.Value = '') then
  begin
      MensagemAlerta('Informe a unidade de medida do item.') ;
      abort ;
  end ;

  if (qryItensnQtdeReq.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade requisitada.') ;
      abort ;
  end ;

  inherited;

  qryItensnValTotal.Value    := qryItensnValUnitario.Value * qryItensnQtdeReq.Value ;

  if (qryItensnValTotal.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor unit�rio do item.') ;
      abort ;
  end ;

  qryItensnCdRequisicao.Value  := qryMasternCdRequisicao.Value            ;
  qryItenscNmItem.Value        := Uppercase(qryItenscNmItem.Value)        ;
  qryItenscUnidadeMedida.Value := Uppercase(qryItenscUnidadeMedida.Value) ;
  qryitenscJustificativa.Value := Uppercase(qryItenscJustificativa.Value) ;
  qryItenscNotaComprador.Value := Uppercase(qryItenscNotaComprador.Value) ;

  if (qryItens.State = dsInsert) then
  begin
      qryItensnCdItemRequisicao.Value     := frmMenu.fnProximoCodigo('ITEMREQUISICAO');
      qryItensnQtdeAtend.Value            := 0 ;
      qryItensnQtdeCanc.Value             := 0 ;
      qryItensnCdTipoItemRequisicao.Value := 1 ;
  end ;

end;

procedure TfrmRequisicao.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  btSalvar.Enabled              := True ;
  DBGridEh1.AllowedOperations   := [alopInsertEh,alopUpdateEh,alopDeleteEh,alopAppendEh] ;

  if (qryMaster.State = dsInsert) then
      exit ;

  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTipoRequisicao.Close ;
  qryItens.Close ;
  qrySetor.Close ;
  qryCC.Close ;
  qryItensAD.Close;
  qryFollowUp.Close;
  qryCentroDistribuicao.Close;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString);

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuarioAtivo').Value := frmMenu.nCdUsuarioLogado;
  qryLoja.Parameters.ParamByName('nPK').Value             := qryMasternCdLoja.Value;
  qryLoja.Open;

  nCdLojaOld := qryMasternCdLoja.Value;

  PosicionaQuery(qryTipoRequisicao, qryMasternCdTipoRequisicao.AsString);
  PosicionaQuery(qryItens, qryMasternCdRequisicao.AsString);
  PosicionaQuery(qryItensAD, qryMasternCdRequisicao.AsString);
  PosicionaQuery(qryTabStatusRequis, qryMasternCdTabStatusRequis.AsString);
  PosicionaQuery(qryUsuario, qryMasternCdUsuarioAutor.AsString);
  PosicionaQuery(qryFollowUp, qryMasternCdRequisicao.AsString);

  qrySetor.Close;
  qrySetor.Parameters.ParamByName('nPK').Value := qryMasternCdSetor.Value ;
  qrySetor.Parameters.ParamByName('nCdUsuarioLogado').Value := frmMenu.nCdUsuarioLogado ;
  qrySetor.Open;

  qryCentroDistribuicao.Close ;
  qryCentroDistribuicao.Parameters.ParamByName('nPK').Value     := qryMasternCdCentroDistribuicao.Value ;
  qryCentroDistribuicao.Parameters.ParamByName('nCdLoja').Value := qryMasternCdLoja.Value ;
  qryCentroDistribuicao.Open;

  PosicionaQuery(qryCC, qryMasternCdCC.asString) ;

  if (qryMasternCdTabStatusRequis.Value > 1) then
  begin
      DBGridEh1.AllowedOperations := [alopUpdateEh,alopDeleteEh] ;
      DBGridEh1.ReadOnly          := True;
  end ;

  if (qryMasterdDtFinalizacao.AsString <> '') then
  begin
      btSalvar.Enabled := false ;
      DBGridEh1.ReadOnly := True ;
      DBGridEh2.ReadOnly := True ;
      DBGridEh3.ReadOnly := True ;
      DBGridEh4.ReadOnly := True ;
  end ;

  cxPageControl1.ActivePageIndex := 0;
end;

procedure TfrmRequisicao.FormShow(Sender: TObject);
begin
  inherited;
  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  cxPageControl1.ActivePageIndex := 0 ;

  qryItensnValUnitario.DisplayFormat  := frmMenu.cMascaraCompras;
  qryItensADnValUnitario.DisplayFormat  := frmMenu.cMascaraCompras;

end;

procedure TfrmRequisicao.btSalvarClick(Sender: TObject);
var
  objRel : TrptRequisicao_view;
begin

  if (qryMaster.Active) and (qryMasternCdTabStatusRequis.Value > 1) then
  begin
      MensagemAlerta('Esta requisi��o j� foi aprovada e n�o pode ser alterada.') ;
      abort ;
  end ;

  if ((not SalvarProc) and (qryMasterdDtFinalizacao.AsString <> '')) then
  begin
      MensagemAlerta('Esta requisi��o j� foi processada e n�o pode ser alterada.') ;
      abort ;
  end ;

  inherited;

  {if (qryItens.IsEmpty) and (qryItensAD.IsEmpty) then
  begin
      qryMaster.Close;
      Abort;
  end;}

  {case MessageDlg('Deseja imprimir a requisi��o ?',mtConfirmation,[mbYes,mbNo],0) of
      mrYes: begin

          objRel := TrptRequisicao_view.Create(nil);

          try
              try
                  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

                  PosicionaQuery(objRel.qryRequisicao, qryMasternCdRequisicao.AsString);

                  objRel.QuickRep1.PreviewModal;
              except
                  MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
                  raise ;
              end ;
          finally
              FreeAndNil(objRel);
          end;
      end ;
  end ;

  SalvarProc := false;}

  btCancelar.Click;
end;

procedure TfrmRequisicao.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTipoRequisicao.Close ;
  qryTabstatusRequis.Close ;
  qryItens.Close ;
  qryItensAD.Close ;
  qrySetor.Close ;
  qryCC.Close ;
  qryUsuario.Close ;
  qryFollowUp.Close;
  qryCentroDistribuicao.Close;

  desativaDBEdit(DBEdit23);
  nCdLojaOld := 0;
end;

procedure TfrmRequisicao.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  if (DbGridEh1.Col = 3) and (qryItens.State = dsInsert) then
  begin
      if (qryItensnCdProduto.Value > 0) then
      begin

          qryProduto.Close ;
          PosicionaQuery(qryProduto, qryItensnCdProduto.asString) ;

          if not qryProduto.eof then
          begin
              qryItenscReferencia.Value    := qryProdutocReferencia.Value ;
              qryItenscNmItem.Value        := qryProdutocNmProduto.Value ;
              qryItenscUnidadeMedida.Value := qryProdutocUnidadeMedida.Value ;

              PosicionaQuery(qryTipoRequisicao,qryMasternCdTipoRequisicao.AsString);

              {-- Quando o tipo de requisi��o for CD atribui o valor do custo do produto para o item da requisi��o --}
              if qryTipoRequisicaonCdTabTipoRequis.Value = 3 then
              begin
                  qryItensnValUnitario.Value := qryProdutonValCusto.Value;
              end;
          end ;


      end ;

  end ;
end;

procedure TfrmRequisicao.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  if (qryMaster.State = dsInactive) then
      Abort;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

  //if not qryItens.Active then
  //begin
      PosicionaQuery(qryItens, qryMasternCdRequisicao.AsString) ;
      PosicionaQuery(qryItensAD, qryMasternCdRequisicao.AsString) ;
  //end ;

end;

procedure TfrmRequisicao.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryTabStatusRequis, qryMasternCdTabStatusRequis.AsString) ;

end;

procedure TfrmRequisicao.DBEdit9KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(121);

            If (nPK > 0) then
            begin
                qryMasternCdTipoRequisicao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRequisicao.DBEdit6Exit(Sender: TObject);
begin
  inherited;

  qrySetor.Close;
  qrySetor.Parameters.ParamByName('nPK').Value :=   frmMenu.ConvInteiro(DbEdit6.Text) ;
  qrySetor.Parameters.ParamByName('nCdUsuarioLogado').Value := frmMenu.nCdUsuarioLogado ;
  qrySetor.Open;


end;

procedure TfrmRequisicao.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (frmMenu.LeParametro('BLOQSETORREQUIS') = 'N' ) then
                nPK := frmLookup_Padrao.ExecutaConsulta2(122,'EXISTS (SELECT 1 FROM UsuarioSetor US WHERE nCdUsuario = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ' AND US.nCdSetor = Setor.nCdSetor)')
            else if (Trim(DBEdit17.Text) <> '') then
                nPK := frmLookup_Padrao.ExecutaConsulta2(122,'nCdCC = ' + Trim(DBEdit17.Text))
            else
            begin
                MensagemAlerta('Selecione um centro de custo.');
                DBEdit17.SetFocus;
                abort;
            end;

            If (nPK > 0) then
            begin
                qryMasternCdSetor.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRequisicao.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  if (DbGridEh1.Col = 3) then
  begin

      case key of
        vk_F4 : begin

            if (qryItens.State = dsBrowse) then
                 qryItens.Edit ;

            if (qryItens.State = dsInsert) or (qryItens.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(123,'EXISTS(SELECT 1 FROM GrupoProdutoTipoRequisicao GPTR WHERE GPTR.nCdGrupoProduto = nCdGrupo_Produto AND nCdTipoRequisicao = ' + qryMasternCdTipoRequisicao.AsString + ')');

                If (nPK > 0) then
                begin
                    qryItensnCdProduto.Value := nPK ;
                end ;

            end ;

        end ;

      end ;

  end ;

end;

procedure TfrmRequisicao.DBEdit17Exit(Sender: TObject);
begin
  inherited;

  qryCC.Close ;
  PosicionaQuery(qryCC, DBEdit17.Text) ;

end;

procedure TfrmRequisicao.DBEdit17KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (DbEdit12.Text = '') then
            begin
                MensagemAlerta('Selecione o tipo de requisi��o.') ;
                DbEdit9.Text ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(157,'EXISTS(SELECT 1 FROM CentroCustoTipoRequisicao CCTR WHERE CCTR.nCdCC = CentroCusto.nCdCC AND CCTR.nCdTipoRequisicao = '+ qryTipoRequisicaonCdTipoRequisicao.asString +')');

            If (nPK > 0) then
            begin
                qryMasternCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRequisicao.qryTipoRequisicaoAfterScroll(DataSet: TDataSet);
begin
  inherited;

  cxTabSheet1.Enabled := (qryTipoRequisicao.Active) and (qryTipoRequisicaonCdTabTipoRequis.Value in [1,3]) ;
  cxTabSheet2.Enabled := (qryTipoRequisicao.Active) and (qryTipoRequisicaonCdTabTipoRequis.Value = 2) ;
  cxTabSheet3.Enabled := (qryTipoRequisicao.Active) and (qryTipoRequisicaonCdTabTipoRequis.Value = 2) ;
  cxTabSheet4.Enabled := (qryTipoRequisicao.Active) and (qryTipoRequisicaonCdTabTipoRequis.Value = 2) ;

  if (not cxTabSheet1.Enabled) then
      cxPageControl1.ActivePage := cxTabSheet4 ;

end;

procedure TfrmRequisicao.btCancelarClick(Sender: TObject);
begin
  cxPageControl1.ActivePageIndex := 0 ;

  cxTabSheet1.Enabled := True ;
  cxTabSheet2.Enabled := False ;
  cxTabSheet3.Enabled := False ;
  cxTabSheet4.Enabled := False ;

  inherited;

end;

procedure TfrmRequisicao.qryTipoRequisicaoAfterCancel(DataSet: TDataSet);
begin
  inherited;

  cxTabSheet1.Enabled := false ;
  cxTabSheet2.Enabled := false ;
  cxTabSheet3.Enabled := false ;
  cxTabSheet4.Enabled := false ;

end;

procedure TfrmRequisicao.DBGridEh1ColEnter(Sender: TObject);
begin
  inherited;

  if (DBGridEh1.Col = 8) then
  begin
      if (qryItens.State = dsInsert) and (qryItensnCdProduto.Value > 0) and (qryItensnValUnitario.Value = 0) then
      begin
          if (qryProdutonValCusto.Value > 0) and (qryItensnValUnitario.Value = 0) then
              qryItensnValUnitario.Value := qryProdutonValCusto.Value ;

          if (qryItensnValUnitario.Value = 0) then
          begin
              {-- Localiza o pre�o do �ltimo recebimento --}
              qryAux.Close ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT TOP 1 nValCustoFinal');
              qryAux.SQL.Add('  FROM ItemRecebimento');
              qryAux.SQL.Add('       INNER JOIN Recebimento ON Recebimento.nCdRecebimento = ItemRecebimento.nCdRecebimento');
              qryAux.SQL.Add(' WHERE Recebimento.nCdTabStatusReceb >= 4') ;
              qryAux.SQL.Add('   AND Recebimento.nCdEmpresa         = ' + intToStr(frmMenu.nCdEmpresaAtiva)) ;
              qryAux.SQL.Add('   AND nCdProduto                     = ' + qryItensnCdProduto.AsString) ;
              qryAux.SQL.Add(' ORDER BY dDtReceb DESC ');
              qryAux.Open;

              if not qryAux.Eof then
                  qryItensnValUnitario.Value := qryAux.FieldList[0].Value
              else
              begin

                  {-- Localiza o pre�o do pedido recebido --}
                  qryAux.Close ;
                  qryAux.SQL.Clear ;
                  qryAux.SQL.Add('SELECT TOP 1 nValCustoUnit');
                  qryAux.SQL.Add('  FROM ItemPedido');
                  qryAux.SQL.Add('       INNER JOIN Pedido     ON Pedido.nCdPedido = ItemPedido.nCdPedido');
                  qryAux.SQL.Add('       INNER JOIN TipoPedido ON TipoPedido.nCdTipoPedido = Pedido.nCdTipoPedido');
                  qryAux.SQL.Add(' WHERE Pedido.nCdTabStatusPed IN (4,5)');
                  qryAux.SQL.Add('   AND Pedido.nCdEmpresa       = ' + intToStr(frmMenu.nCdEmpresaAtiva)) ;
                  qryAux.SQL.Add('   AND ItemPedido.nCdProduto   = ' + qryItensnCdProduto.AsString) ;
                  qryAux.SQL.Add('   AND ItemPedido.nQtdeExpRec  > 0');
                  qryAux.SQL.Add('   AND TipoPedido.cFlgCompra   = 1');
                  qryAux.SQL.Add(' ORDER BY dDtPedido DESC');
                  qryAux.Open;

                  if not qryAux.Eof then
                      qryItensnValUnitario.Value := qryAux.FieldList[0].Value ;
              end ;

          end ;

      end ;

  end ;

end;

procedure TfrmRequisicao.qryItensADBeforePost(DataSet: TDataSet);
begin

  if (qryItensADcNmItem.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o do item.') ;
      abort ;
  end ;

  if (qryItensADcUnidadeMedida.Value = '') then
  begin
      MensagemAlerta('Informe a unidade de medida do item.') ;
      abort ;
  end ;

  if (qryItensADnQtdeReq.Value <= 0) then
  begin
      MensagemAlerta('Informe a quantidade requisitada.') ;
      abort ;
  end ;

  inherited;


  if (qryItensADnValUnitario.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor unit�rio do item.') ;
      abort ;
  end ;

  qryItensADnCdRequisicao.Value  := qryMasternCdRequisicao.Value            ;
  qryItensADcNmItem.Value        := Uppercase(qryItensADcNmItem.Value)        ;
  qryItensADcUnidadeMedida.Value := Uppercase(qryItensADcUnidadeMedida.Value) ;
  qryitensADcJustificativa.Value := Uppercase(qryItensADcJustificativa.Value) ;
  qryItensADcNotaComprador.Value := Uppercase(qryItensADcNotaComprador.Value) ;

  if (qryItensAD.State = dsInsert) then
  begin
      qryItensADnQtdeAtend.Value            := 0 ;
      qryItensADnQtdeCanc.Value             := 0 ;
      qryItensADnCdTipoItemRequisicao.Value := 2 ;
  end ;

  qryItensADnValTotal.Value     := qryItensADnValUnitario.Value * qryItensADnQtdeReq.Value ;

end;

procedure TfrmRequisicao.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  cxTabSheet1.Enabled := True ;
  cxTabSheet2.Enabled := False ;
  cxTabSheet3.Enabled := False ;
  cxTabSheet4.Enabled := False ;

end;

procedure TfrmRequisicao.ToolButton1Click(Sender: TObject);
begin
  inherited;
  SalvarProc := false;
end;

procedure TfrmRequisicao.btConsultarClick(Sender: TObject);
begin
  inherited;
  SalvarProc := false;
end;

procedure TfrmRequisicao.cxButton1Click(Sender: TObject);
begin
  if not qryMaster.Active then
  begin
      MensagemAlerta('Nenhuma requisi��o ativa.') ;
      exit ;
  end ;

  if (qryMasterdDtFinalizacao.AsString <> '') then
  begin
      MensagemAlerta('A requisi��o j� foi processada.') ;
      exit ;
  end ;

  if ((qryItens.RecordCount = 0) and (qryItensAD.RecordCount = 0)) then
  begin
      MensagemAlerta('Requisi��o deve conter no m�nimo um item informado.');
      Exit;
  end;

  if (MessageDlg('Confirma o processamento desta requisi��o ?',mtConfirmation,[mbYes,mbNo],0) = MRNO) then
      exit ;

  try
      qryMaster.Edit;
      qryMasterdDtFinalizacao.Value := Now();
      SalvarProc                    := true;

      if (qryTipoRequisicaocExigeAutor.Value = 0) then
          qryMasterdDtAutor.Value := Now();

      qryMaster.Post;

      frmMenu.LogAuditoria(54,4,qryMasternCdRequisicao.Value,'Requisi��o Processada.');

      ShowMessage('Requisi��o finalizada com sucesso.');
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  btCancelar.Click;

end;

procedure TfrmRequisicao.cxButton2Click(Sender: TObject);
begin
  if not qryMaster.Active then
  begin
      MensagemAlerta('Nenhuma requisi��o ativa.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusRequis.Value > 1) then
  begin
      MensagemAlerta('O status desta requisi��o n�o permite cancelamento.') ;
      exit ;
  end ;

  case MessageDlg('Confirma o cancelamento desta requisi��o ?',mtConfirmation,[mbYes,mbNo],0) of
    mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Edit ;
      qryMasternCdTabStatusRequis.Value := 5 ;
      qryMaster.Post ;

      frmMenu.LogAuditoria(54,3,qryMasternCdRequisicao.Value,'Cancelada');

      { -- gera alerta de requisi��o -- }
      frmMenu.SP_GERA_ALERTA.Close;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@nCdTipoAlerta').Value := 5; //Cancelamento de Req. de Compra
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cAssunto').Value      := 'CANC. REQUISI��O No ' + qryMasternCdRequisicao.AsString;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cMensagem').Value     := 'Cancelamento de Requisi��o No ' + qryMasternCdRequisicao.AsString + ' R$ ' + FloatToStrF(qryMasternValRequisicao.Value,ffNumber,12,2) + ' Cancelado por: ' + qryMastercNmSolicitante.Value + ' - Tipo Req.: ' + qryTipoRequisicaocNmTipoRequisicao.Value + ' / Setor: ' + qrySetorcNmSetor.Value + ' / Cancelado por ' + frmMenu.cNmUsuarioLogado + ' em ' + DateTimeToStr(Now()) + '.';
      frmMenu.SP_GERA_ALERTA.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Requisi��o cancelada com sucesso.') ;
  btCancelar.Click;

end;

procedure TfrmRequisicao.btCancelarRequisClick(Sender: TObject);
begin
  if not qryMaster.Active then
  begin
      MensagemAlerta('Nenhuma requisi��o ativa.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusRequis.Value > 1) then
  begin
      MensagemAlerta('O status desta requisi��o n�o permite cancelamento.') ;
      exit ;
  end ;

  case MessageDlg('Confirma o cancelamento desta requisi��o ?',mtConfirmation,[mbYes,mbNo],0) of
    mrNo:exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Edit ;
      qryMasternCdTabStatusRequis.Value := 5 ;
      qryMaster.Post ;

      frmMenu.LogAuditoria(54,3,qryMasternCdRequisicao.Value,'Cancelada');

      { -- gera alerta de requisi��o -- }
      frmMenu.SP_GERA_ALERTA.Close;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@nCdTipoAlerta').Value := 5; //Cancelamento de Req. de Compra
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cAssunto').Value      := 'CANC. REQUISI��O No ' + qryMasternCdRequisicao.AsString;
      frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cMensagem').Value     := 'Cancelamento de Requisi��o No ' + qryMasternCdRequisicao.AsString + ' R$ ' + FloatToStrF(qryMasternValRequisicao.Value,ffNumber,12,2) + ' Cancelado por: ' + qryMastercNmSolicitante.Value + ' - Tipo Req.: ' + qryTipoRequisicaocNmTipoRequisicao.Value + ' / Setor: ' + qrySetorcNmSetor.Value + ' / Cancelado por ' + frmMenu.cNmUsuarioLogado + ' em ' + DateTimeToStr(Now()) + '.';
      frmMenu.SP_GERA_ALERTA.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Requisi��o cancelada com sucesso.') ;
  btCancelar.Click;

end;

procedure TfrmRequisicao.btImprimirRequisClick(Sender: TObject);
var
  objRel : TrptRequisicao_view;
begin

  if not qryMaster.Active then
  begin
      MensagemAlerta('Nenhuma requisi��o ativa.') ;
      exit ;
  end ;

  if qryMasternCdTabStatusRequis.Value = 5 then
  begin
      MensagemAlerta('Requisi��o cancelada n�o pode ser impressa.') ;
      exit ;
  end ;

  objRel := TrptRequisicao_view.Create(nil);
  try
      try
          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;
          objRel.QRDBText21.Mask    := frmMenu.cMascaraCompras;

          PosicionaQuery(objRel.qryRequisicao, qryMasternCdRequisicao.AsString) ;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o da tela. Classe:  ' + objRel.ClassName) ;
          raise ;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TfrmRequisicao.qryTipoRequisicaoAfterOpen(DataSet: TDataSet);
begin
  inherited;

  if (qryTipoRequisicaonCdTabTipoRequis.Value = 3) then
      ativaDBEdit(DBEdit23)

  else
  begin
      if (qryMaster.State = dsEdit) or (qryMaster.State = dsInsert) then
          qryMasternCdCentroDistribuicao.Clear;

      desativaDBEdit(DBEdit23);
  end;
end;

procedure TfrmRequisicao.qryCCBeforeOpen(DataSet: TDataSet);
begin
  inherited;

  qryCC.Parameters.ParamByName('nCdTipoRequisicao').Value := qryMasternCdTipoRequisicao.Value; //frmMenu.ConvInteiro(DBEdit9.Text);
end;

procedure TfrmRequisicao.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  if ((DBEdit3.ReadOnly) or (qryMaster.IsEmpty)) then
      Exit;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;


end;

procedure TfrmRequisicao.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  if ((DBEdit3.ReadOnly) or (qryMaster.IsEmpty)) then
      Exit;

  if (nCdLojaOld = 0) then
     nCdLojaOld := qryMasternCdLoja.Value;

  if (not DBEdit23.ReadOnly) and (qryMasternCdLoja.Value <> nCdLojaOld) and (DBEdit3.Text <> '') then
  begin
      DBEdit23.Clear;
      DBEdit22.Clear;
      nCdLojaOld := qryMasternCdLoja.Value;
  end;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nPK').Value := qryMasternCdLoja.Value;
  qryLoja.Parameters.ParamByName('nCdUsuarioAtivo').Value := frmMenu.nCdUsuarioLogado;
  qryLoja.Open;
  
end;

procedure TfrmRequisicao.DBEdit23Exit(Sender: TObject);
begin
  inherited;

  if DBEdit23.Text = '' then
      Abort;

  if DBEdit3.Text = '' then
  begin
      MensagemAlerta('Informe primeiro a loja para consultar o centro de distribui��o.');
      DBEdit3.SetFocus;
      Abort;
  end;

  qryCentroDistribuicao.Close ;
  qryCentroDistribuicao.Parameters.ParamByName('nPK').Value     := StrToInt(DBEdit23.Text)   ;
  qryCentroDistribuicao.Parameters.ParamByName('nCdLoja').Value := StrToInt(DBEdit3.Text) ;
  qryCentroDistribuicao.Open;

end;

procedure TfrmRequisicao.DBEdit23KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(782,' EXISTS (SELECT 1 FROM LojaCentroDistribuicao WHERE nCdLoja = ' + qryMasternCdLoja.AsString + '  AND LojaCentroDistribuicao.nCdCentroDistribuicao = CentroDistribuicao.nCdCentroDistribuicao)');

            If (nPK > 0) then
            begin
                qryMasternCdCentroDistribuicao.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmRequisicao.btLeituraClick(Sender: TObject);
var
  objConfProdPadrao : TfrmConferenciaProdutoPadrao;
begin
  inherited;

  if not (qryMaster.Active) then
      Exit;

  if (qryMasternCdRequisicao.Value = 0) then
  begin
      MensagemAlerta('Salve a requisi��o antes de iniciar o processo de digita��o/leitura.');
      Exit;
  end;

  if (qryMasternCdTabStatusRequis.Value > 1) then
  begin
      MensagemAlerta('Status da requisi��o n�o permite altera��es.');
      PosicionaPK(qryMasternCdRequisicao.Value);
      Exit;
  end;

  try
      try
          { -- configura tela de digit���o padr�o -- }
          objConfProdPadrao := TfrmConferenciaProdutoPadrao.Create(nil);
          objConfProdPadrao.Caption                    := 'Digita��o/Leitura de Produtos';
          objConfProdPadrao.rbDigitacaoLeitura.Checked := True;

          showForm(objConfProdPadrao,False);

          if (objConfProdPadrao.bProcessa) then
              case MessageDlg('Confirma a grava��o dos itens ?',mtConfirmation,[mbYes,mbNo],0) of
                  mrNo:Exit;
              end
          else
              Exit;

          frmMenu.Connection.BeginTrans;

          { -- processa inclus�o dos itens -- }
          qryGravaConfPadrao.Close;
          qryGravaConfPadrao.Parameters.ParamByName('nCdRequisicao').Value := qryMasternCdRequisicao.Value;
          qryGravaConfPadrao.Parameters.ParamByName('nCdEmpresa').Value    := qryMasternCdEmpresa.Value;
          qryGravaConfPadrao.ExecSQL;

          frmMenu.Connection.CommitTrans;

          qryItens.Close;
          PosicionaQuery(qryItens, qryMasternCdRequisicao.AsString);
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  finally
      FreeAndNil(objConfProdPadrao);
      frmMenu.mensagemUsuario('');
  end;
end;

procedure TfrmRequisicao.btExcluirItensClick(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      Exit;

  if (qryMasternCdRequisicao.Value = 0) then
  begin
      MensagemAlerta('Salve a requisi��o antes de iniciar o processo de digita��o/leitura.');
      Exit;
  end;

  if (qryMasternCdTabStatusRequis.Value > 1) then
  begin
      MensagemAlerta('Status da requisi��o n�o permite altera��es.');
      PosicionaPK(qryMasternCdRequisicao.Value);
      Exit;
  end;

  if ((qryItens.Active) and (qryItens.RecordCount > 0)) then
  begin
      case MessageDlg('Confirma a exclus�o dos itens desta requisi��o ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:Exit;
      end;

      try
          frmMenu.Connection.BeginTrans;
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('DELETE FROM ItemRequisicao WHERE nCdRequisicao = ' + qryMasternCdRequisicao.AsString);
          qryAux.ExecSQL;
          frmMenu.Connection.CommitTrans;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
      end;

      PosicionaQuery(qryItens, qryMasternCdRequisicao.AsString);
  end;
end;

initialization
    RegisterClass(TfrmRequisicao) ;

end.
