inherited rptBlocoUnidades: TrptBlocoUnidades
  Left = 59
  Top = 175
  Caption = 'rptBlocoUnidades'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 26
    Top = 48
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label4: TLabel [2]
    Left = 6
    Top = 71
    Width = 61
    Height = 13
    Alignment = taRightJustify
    Caption = 'Bloco / Torre'
  end
  inherited ToolBar1: TToolBar
    TabOrder = 1
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtCdEmpresa: TMaskEdit [4]
    Left = 72
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = edtCdEmpresaExit
    OnKeyDown = edtCdEmpresaKeyDown
  end
  object DBEdit2: TDBEdit [5]
    Tag = 1
    Left = 141
    Top = 40
    Width = 66
    Height = 21
    DataField = 'cSigla'
    DataSource = DataSource1
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [6]
    Tag = 1
    Left = 210
    Top = 40
    Width = 651
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 3
  end
  object edtCdBloco: TMaskEdit [7]
    Left = 72
    Top = 64
    Width = 65
    Height = 21
    TabOrder = 4
    OnExit = edtCdBlocoExit
    OnKeyDown = edtCdBlocoKeyDown
  end
  object DBEdit7: TDBEdit [8]
    Tag = 1
    Left = 499
    Top = 64
    Width = 249
    Height = 21
    DataField = 'Empreendimentos|Bloco/Torre'
    DataSource = DataSource2
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [9]
    Tag = 1
    Left = 141
    Top = 64
    Width = 353
    Height = 21
    DataField = 'Empreendimentos|Empreendimento'
    DataSource = DataSource2
    TabOrder = 6
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 536
    Top = 128
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nCdTerceiro')
    Left = 696
    Top = 128
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryBlocoTorre: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT BlocoEmpImobiliario.nCdBlocoEmpImobiliario "Empreendiment' +
        'os|C'#243'd"        '
      
        '      ,EmpImobiliario.cNmEmpImobiliario "Empreendimentos|Empreen' +
        'dimento"   '
      
        '      ,BlocoEmpImobiliario.cNmBlocoEmpImobiliario "Empreendiment' +
        'os|Bloco/Torre"   '
      '      ,BlocoEmpImobiliario.iQtdeUnidade'
      '  FROM BlocoEmpImobiliario '
      
        '       INNER JOIN EmpImobiliario ON EmpImobiliario.nCdEmpImobili' +
        'ario = BlocoEmpImobiliario.nCdEmpImobiliario'
      ' WHERE EmpImobiliario.nCdEmpresa =:nCdEmpresa'
      '       AND BlocoEmpImobiliario.nCdBlocoEmpImobiliario =:nPK ')
    Left = 624
    Top = 128
    object qryBlocoTorreEmpreendimentosCd: TAutoIncField
      FieldName = 'Empreendimentos|C'#243'd'
      ReadOnly = True
    end
    object qryBlocoTorreEmpreendimentosEmpreendimento: TStringField
      FieldName = 'Empreendimentos|Empreendimento'
      Size = 100
    end
    object qryBlocoTorreEmpreendimentosBlocoTorre: TStringField
      FieldName = 'Empreendimentos|Bloco/Torre'
      Size = 50
    end
    object qryBlocoTorreiQtdeUnidade: TIntegerField
      FieldName = 'iQtdeUnidade'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 552
    Top = 184
  end
  object DataSource2: TDataSource
    DataSet = qryBlocoTorre
    Left = 640
    Top = 176
  end
  object DataSource3: TDataSource
    DataSet = qryTerceiro
    Left = 720
    Top = 176
  end
end
