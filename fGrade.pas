unit fGrade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, GridsEh, DBGridEh, DB, StdCtrls, Mask,
  DBCtrls, ImgList, ADODB, ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmGrade = class(TfrmCadastro_Padrao)
    qryMasternCdGrade: TIntegerField;
    qryMastercNmGrade: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryItemGrade: TADOQuery;
    qryItemGradenCdItemGrade: TAutoIncField;
    qryItemGradenCdGrade: TIntegerField;
    qryItemGradeiTamanho: TIntegerField;
    qryItemGradecNmTamanho: TStringField;
    dsItemGrade: TDataSource;
    DBGridEh1: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryItemGradeBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrade: TfrmGrade;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmGrade.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRADE' ;
  nCdTabelaSistema  := 31 ;
  nCdConsultaPadrao := 51 ;

end;

procedure TfrmGrade.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmGrade.qryItemGradeBeforePost(DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post;

  if (Trim(qryItemGradecNmTamanho.Value) = '') then
  begin
      MensagemAlerta('Informe a descri��o do item da grade.');
      DBGridEh1.SelectedField := qryItemGradecNmTamanho;
      DBGridEh1.SetFocus;
      Abort;
  end;

  inherited;

  qryItemGradenCdGrade.Value := qryMasternCdGrade.Value ;

  if (qryItemGrade.State = dsInsert) then
      qryItemGradenCdItemGrade.Value := frmMenu.fnProximoCodigo('ITEMGRADE');
end;

procedure TfrmGrade.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryItemGrade.Close ;
  qryItemGrade.Parameters.ParamByName('nPK').Value := qryMasternCdGrade.Value ;
  qryItemGrade.Open ;
  
end;

procedure TfrmGrade.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryItemGrade.Close ;

end;

procedure TfrmGrade.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (Trim(qryMastercNmGrade.Value) = '') then
  begin
      MensagemAlerta('Informe a descri��o da grade.');
      Abort
  end;

  if (qryItemGrade.IsEmpty) then
  begin
      MensagemAlerta('Necess�rio informar ao menos um tamanho da grade.');
      DBGridEh1.SelectedField := qryItemGradeiTamanho;
      DBGridEh1.SetFocus;
      Abort;
  end;

  inherited;
end;

initialization
    RegisterClass(TfrmGrade) ;

end.
