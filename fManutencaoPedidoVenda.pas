unit fManutencaoPedidoVenda;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, cxLookAndFeelPainters, StdCtrls, cxButtons, GridsEh, DBGridEh,
  Mask, DBCtrls, cxPC, cxControls, DBGridEhGrouping, ToolCtrlsEh, ER2Lookup;

type
  TfrmManutencaoPedidoVenda = class(TfrmCadastro_Padrao)
    qryMasternCdPedido: TIntegerField;
    qryMastercNmEmpresa: TStringField;
    qryMastercNmLoja: TStringField;
    qryMastercNmTerceiro: TStringField;
    qryMastercNmVendedor: TStringField;
    qryMastercNmTerceiroPagador: TStringField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtLancto: TDateTimeField;
    qryMasternCdConta: TStringField;
    qryMastercNmCondPagto: TStringField;
    qryMastercNmTabStatusPed: TStringField;
    qryMasternValProdutos: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMasternValPedido: TBCDField;
    qryMasternCdLanctoFin: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label9: TLabel;
    DBEdit9: TDBEdit;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    Label11: TLabel;
    DBEdit11: TDBEdit;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label13: TLabel;
    DBEdit13: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    Label15: TLabel;
    DBEdit15: TDBEdit;
    qryItemPedidos: TADOQuery;
    qryItemPedidosnCdPedido: TIntegerField;
    qryItemPedidosnCdProduto: TIntegerField;
    qryItemPedidoscNmItem: TStringField;
    qryItemPedidosnQtdePed: TBCDField;
    qryItemPedidosnValUnitario: TBCDField;
    qryItemPedidosnValTotalItem: TBCDField;
    dsItemPedidos: TDataSource;
    qryVendedoresRede: TADOQuery;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    DBEdit6: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit16: TDBEdit;
    qryMasternCdUsuario: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryVendedoresRedenCdUsuario: TIntegerField;
    qryVendedoresRedecNmUsuario: TStringField;
    DBEdit18: TDBEdit;
    dsVendedoresRede: TDataSource;
    qryMasternCdTerceiroColab: TIntegerField;
    qryVendedoresRedenCdTerceiroResponsavel: TIntegerField;
    qryMasternCdDoctoFiscal: TIntegerField;
    Label6: TLabel;
    DBEdit17: TDBEdit;
    edtVendedor: TER2LookupMaskEdit;
    SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO: TADOStoredProc;
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterOpen(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure edtVendedorExit(Sender: TObject);
    procedure edtVendedorBeforeLookup(Sender: TObject);
    procedure edtVendedorKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmManutencaoPedidoVenda: TfrmManutencaoPedidoVenda;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmManutencaoPedidoVenda.qryMasterAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryItemPedidos.Close;
  PosicionaQuery(qryItemPedidos,qryMasternCdPedido.AsString);

  qryVendedoresRede.Close;
  PosicionaQuery(qryVendedoresRede,qryMasternCdUsuario.AsString);

end;

procedure TfrmManutencaoPedidoVenda.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PEDIDO' ;
  nCdTabelaSistema  := 30 ;
  nCdConsultaPadrao := 706 ;
end;

procedure TfrmManutencaoPedidoVenda.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryVendedoresRede.Close;
  qryItemPedidos.Close;
  edtVendedor.Clear;

end;

procedure TfrmManutencaoPedidoVenda.qryMasterAfterOpen(DataSet: TDataSet);
begin
  inherited;

  if not (qryMaster.IsEmpty) then
  begin
      PosicionaQuery(qryVendedoresRede, qryMasternCdUsuario.AsString);

      edtVendedor.Text := qryVendedoresRedenCdUsuario.AsString;
  end;
end;

procedure TfrmManutencaoPedidoVenda.qryMasterBeforePost(DataSet: TDataSet);
begin

  PosicionaQuery(qryVendedoresRede, edtVendedor.Text);

  if (qryVendedoresRede.IsEmpty) then
  begin
      MensagemAlerta('Informe o vendedor.');

      edtVendedor.SetFocus;

      Abort;
  end;

  if (qryVendedoresRedenCdTerceiroResponsavel.Value <> qryMasternCdTerceiroColab.Value) then
      if ((MessageDlg('Confirma o vendedor selecionado?', mtConfirmation, [mbYes,mbNo],0)) = mrYes) then
      begin
          qryMasternCdTerceiroColab.Value := qryVendedoresRedenCdTerceiroResponsavel.Value;

          SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO.Close;
          SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value;
          SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO.ExecProc;

          ShowMessage('Vendedor alterado com sucesso.');
      end
      else
      begin
          PosicionaQuery(qryVendedoresRede, qryMasternCdUsuario.AsString);

          edtVendedor.Text := qryVendedoresRedenCdUsuario.AsString;
      end;

  inherited;
end;

procedure TfrmManutencaoPedidoVenda.edtVendedorExit(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsEdit) then
      qryMaster.Post;
end;

procedure TfrmManutencaoPedidoVenda.edtVendedorBeforeLookup(
  Sender: TObject);
begin
  if (qryMaster.State <> dsEdit) then
      Abort;

  inherited;
end;

procedure TfrmManutencaoPedidoVenda.edtVendedorKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  if (Key = VK_RETURN) then
      if (qryMaster.State = dsEdit) then
          qryMaster.Post;
end;

initialization
    RegisterClass (TfrmManutencaoPedidoVenda);

end.
