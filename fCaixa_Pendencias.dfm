inherited frmCaixa_Pendencias: TfrmCaixa_Pendencias
  Left = 164
  Top = 170
  Width = 906
  Height = 483
  BorderIcons = [biSystemMenu]
  Caption = 'Pend'#234'ncias'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 890
    Height = 418
  end
  inherited ToolBar1: TToolBar
    Width = 890
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 890
    Height = 418
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 414
    ClientRectLeft = 4
    ClientRectRight = 886
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Credi'#225'rio em Aberto'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 882
        Height = 390
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGrid1DBTableView1: TcxGridDBTableView
          DataController.DataSource = dsCrediario
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              Column = cxGrid1DBTableView1nSaldoTit
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GridLines = glVertical
          OptionsView.GroupByBox = False
          Styles.Header = frmMenu.Header
          object cxGrid1DBTableView1nCdCrediario: TcxGridDBColumn
            Caption = 'Carnet'
            DataBinding.FieldName = 'nCdCrediario'
            Width = 72
          end
          object cxGrid1DBTableView1iParcela: TcxGridDBColumn
            Caption = 'Parcela'
            DataBinding.FieldName = 'iParcela'
            Width = 69
          end
          object cxGrid1DBTableView1dDtVenda: TcxGridDBColumn
            Caption = 'Dt. Venda'
            DataBinding.FieldName = 'dDtVenda'
          end
          object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
            Caption = 'Dt. Vencto'
            DataBinding.FieldName = 'dDtVenc'
          end
          object cxGrid1DBTableView1nValTit: TcxGridDBColumn
            Caption = 'Valor Parcela'
            DataBinding.FieldName = 'nValTit'
            Width = 111
          end
          object cxGrid1DBTableView1nValJuro: TcxGridDBColumn
            Caption = 'Juros'
            DataBinding.FieldName = 'nValJuro'
          end
          object cxGrid1DBTableView1nValDesconto: TcxGridDBColumn
            Caption = 'Desconto'
            DataBinding.FieldName = 'nValDesconto'
          end
          object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
            Caption = 'Valor a Pagar'
            DataBinding.FieldName = 'nSaldoTit'
            Width = 107
          end
          object cxGrid1DBTableView1iDiasAtraso: TcxGridDBColumn
            Caption = 'Dias Atraso'
            DataBinding.FieldName = 'iDiasAtraso'
            Width = 101
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Cheques em Aberto'
      ImageIndex = 1
      object cxGrid2: TcxGrid
        Left = 0
        Top = 0
        Width = 882
        Height = 390
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        object cxGridDBTableView1: TcxGridDBTableView
          DataController.DataSource = dsCheque
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '#,##0.00'
              Kind = skSum
              Column = cxGridDBTableView1nValCheque
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsData.CancelOnExit = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GridLines = glVertical
          OptionsView.GroupByBox = False
          Styles.Header = frmMenu.Header
          object cxGridDBTableView1nCdCheque: TcxGridDBColumn
            Caption = 'ID'
            DataBinding.FieldName = 'nCdCheque'
          end
          object cxGridDBTableView1nCdBanco: TcxGridDBColumn
            Caption = 'Banco'
            DataBinding.FieldName = 'nCdBanco'
            Width = 57
          end
          object cxGridDBTableView1cAgencia: TcxGridDBColumn
            Caption = 'Ag'#234'ncia'
            DataBinding.FieldName = 'cAgencia'
            Width = 65
          end
          object cxGridDBTableView1cConta: TcxGridDBColumn
            Caption = 'Conta'
            DataBinding.FieldName = 'cConta'
          end
          object cxGridDBTableView1cCNPJCPF: TcxGridDBColumn
            Caption = 'CNPJ/CPF Emissor'
            DataBinding.FieldName = 'cCNPJCPF'
            Width = 126
          end
          object cxGridDBTableView1iNrCheque: TcxGridDBColumn
            Caption = 'N'#250'm. Cheque'
            DataBinding.FieldName = 'iNrCheque'
            Width = 95
          end
          object cxGridDBTableView1nValCheque: TcxGridDBColumn
            Caption = 'Valor'
            DataBinding.FieldName = 'nValCheque'
          end
          object cxGridDBTableView1dDtDeposito: TcxGridDBColumn
            Caption = 'Data Dep'#243'sito'
            DataBinding.FieldName = 'dDtDeposito'
            Width = 104
          end
          object cxGridDBTableView1cNmTabStatusCheque: TcxGridDBColumn
            Caption = 'Status Atual'
            DataBinding.FieldName = 'cNmTabStatusCheque'
            Width = 172
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
    end
  end
  object qryCrediario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLanctoFin'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdLanctoFin int'
      ''
      'Set @nCdLanctoFin = :nCdLanctoFin'
      ''
      'SELECT Titulo.nCdCrediario'
      '      ,Titulo.iParcela'
      '      ,Crediario.dDtVenda'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValTit'
      '      ,Titulo.nValJuro'
      '      ,Titulo.nValDesconto'
      '      ,Titulo.nSaldoTit'
      
        '      ,CASE WHEN Titulo.dDtVenc < dbo.fn_OnlyDate(GetDate()) THE' +
        'N DateDiff(day,Titulo.dDtVenc, dbo.fn_OnlyDate(GetDate()))'
      '            ELSE 0'
      '       END iDiasAtraso'
      '  FROM Titulo'
      
        '       INNER JOIN Crediario ON Titulo.nCdCrediario = Crediario.n' +
        'CdCrediario'
      
        ' WHERE ((Crediario.nCdLanctoFin = @nCdLanctoFin) OR (@nCdLanctoF' +
        'in = 0))'
      '   AND Crediario.nCdTerceiro  = :nCdTerceiro'
      
        '   AND ((@nCdLanctoFin > 0) OR (@nCdLanctoFin = 0 AND (Titulo.dD' +
        'tVenc < dbo.fn_OnlyDate(GetDate()))))'
      ' ORDER BY Titulo.dDtVenc')
    Left = 132
    Top = 222
    object qryCrediarionCdCrediario: TIntegerField
      FieldName = 'nCdCrediario'
    end
    object qryCrediarioiParcela: TIntegerField
      FieldName = 'iParcela'
    end
    object qryCrediariodDtVenda: TDateTimeField
      FieldName = 'dDtVenda'
    end
    object qryCrediariodDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryCrediarionValTit: TBCDField
      FieldName = 'nValTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCrediarionValJuro: TBCDField
      FieldName = 'nValJuro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCrediarionValDesconto: TBCDField
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCrediarionSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryCrediarioiDiasAtraso: TIntegerField
      FieldName = 'iDiasAtraso'
      ReadOnly = True
    end
  end
  object dsCrediario: TDataSource
    DataSet = qryCrediario
    Left = 172
    Top = 222
  end
  object qryCheque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'DECLARE @nCdLanctoFin int'
      ''
      'Set @nCdLanctoFin = 0'
      ''
      'SELECT Cheque.nCdCheque'
      '      ,nCdBanco'
      '      ,cAgencia'
      '      ,cConta'
      '      ,cCNPJCPF'
      '      ,iNrCheque'
      '      ,nValCheque'
      '      ,dDtDeposito'
      '      ,TabStatusCheque.cNmTabStatusCheque'
      '  FROM Cheque'
      
        '       INNER JOIN TabStatusCheque ON TabStatusCheque.nCdTabStatu' +
        'sCheque = Cheque.nCdTabStatusCheque'
      ' WHERE Cheque.nCdTerceiroResp       = :nCdTerceiro'
      '   AND Cheque.nCdTabTipoCheque      = 2'
      
        '   AND ((Cheque.nCdTabStatusCheque IN (1,5,6,9,10))  AND (@nCdLa' +
        'nctoFin > 0))'
      
        '   AND ((Cheque.nCdTabStatusCheque IN (5,6,8,10))    AND (@nCdLa' +
        'nctoFin = 0))'
      
        '   AND ((Cheque.nCdLanctoFin        = @nCdLanctoFin)  OR (@nCdLa' +
        'nctoFin = 0))'
      ' ORDER BY iNrCheque')
    Left = 420
    Top = 222
    object qryChequenCdCheque: TAutoIncField
      FieldName = 'nCdCheque'
      ReadOnly = True
    end
    object qryChequenCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryChequecAgencia: TStringField
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryChequecConta: TStringField
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryChequecCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequeiNrCheque: TIntegerField
      FieldName = 'iNrCheque'
    end
    object qryChequenValCheque: TBCDField
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryChequedDtDeposito: TDateTimeField
      FieldName = 'dDtDeposito'
    end
    object qryChequecNmTabStatusCheque: TStringField
      FieldName = 'cNmTabStatusCheque'
      Size = 50
    end
  end
  object dsCheque: TDataSource
    DataSet = qryCheque
    Left = 460
    Top = 222
  end
end
