unit fRegAtendOcorrencia_Atendimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, StdCtrls, DBCtrls, Mask, DB, ADODB, ImgList,
  ComCtrls, ToolWin, ExtCtrls, cxPC, cxControls, cxLookAndFeelPainters,
  cxButtons;

type
  TfrmRegAtendOcorrencia_Atendimento = class(TfrmProcesso_Padrao)
    qryContatoOcorrencia: TADOQuery;
    dsContatoOcorrencia: TDataSource;
    qryContatoOcorrencianCdContatoOcorrencia: TIntegerField;
    qryContatoOcorrencianCdUsuarioContato: TIntegerField;
    qryContatoOcorrencianCdOcorrencia: TIntegerField;
    qryContatoOcorrenciacDescAtendimento: TMemoField;
    qryContatoOcorrenciadDtAtendimento: TDateTimeField;
    qryContatoOcorrenciacNmContatoOcorrencia: TStringField;
    qryContatoOcorrenciacTelefoneContato: TStringField;
    qryAux: TADOQuery;
    qryTabStatusOcorrencia: TADOQuery;
    dsTabStatusOcorrencia: TDataSource;
    qryTabStatusOcorrencianCdTabStatusOcorrencia: TIntegerField;
    qryTabStatusOcorrenciacNmTabStatusOcorrencia: TStringField;
    qryContatoOcorrencianCdTabStatusOcorrencia: TIntegerField;
    qryContatoTerceiro: TADOQuery;
    qryContatoTerceironCdTerceiro: TIntegerField;
    qryContatoTerceirocNmTipoTelefone: TStringField;
    qryContatoTerceirocTelefone: TStringField;
    qryContatoTerceirocRamal: TStringField;
    qryContatoTerceirocContato: TStringField;
    qryContatoTerceirocDepartamento: TStringField;
    dsContatoTerceiro: TDataSource;
    GroupBox1: TGroupBox;
    dbLkStatusOcorrencia: TDBLookupComboBox;
    Label2: TLabel;
    GroupBox2: TGroupBox;
    DBMemo1: TDBMemo;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    btExibeCliente: TcxButton;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btExibeClienteClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdOcorrencia          : integer;
    nCdTabStatusOcorrencia : integer;
    nCdTerceiro            : integer;
    cNmTabSatusAtual       : String;
  end;

var
  frmRegAtendOcorrencia_Atendimento: TfrmRegAtendOcorrencia_Atendimento;

implementation

uses
  fMenu, fRegAtendOcorrencia_Atendimento_SelContato;

{$R *.dfm}

procedure TfrmRegAtendOcorrencia_Atendimento.FormCreate(Sender: TObject);
begin
  inherited;

  qryContatoOcorrencia.Close;
  qryContatoOcorrencia.Open;
  qryContatoOcorrencia.Insert;

  qryContatoOcorrenciadDtAtendimento.Value := Now;

end;

procedure TfrmRegAtendOcorrencia_Atendimento.FormShow(Sender: TObject);
begin
  inherited;

  qryTabStatusOcorrencia.Close;
  qryTabStatusOcorrencia.Parameters.ParamByName('nPK').Value := nCdTabStatusOcorrencia;
  qryTabStatusOcorrencia.Open;

end;


procedure TfrmRegAtendOcorrencia_Atendimento.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (qryContatoOcorrencianCdTabStatusOcorrencia.Value = 0) then
  begin
      MensagemAlerta('Informe o status do atendimento.');
      dbLkStatusOcorrencia.SetFocus;
      Abort;
  end;

  if (Trim(qryContatoOcorrenciacNmContatoOcorrencia.Value) = '') then
  begin
      MensagemAlerta('Informe o nome do contato.');
      DBEdit3.SetFocus;
      Abort;
  end;

  if (Trim(qryContatoOcorrenciacDescAtendimento.Value) = '') then
  begin
      MensagemAlerta('Informe a descri��o do atendimento.');
      DBMemo1.SetFocus;
      Abort;
  end;

  case MessageDlg('Confirma a inclus�o do atendimento desta ocorr�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo : Exit;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryContatoOcorrencianCdContatoOcorrencia.Value := frmMenu.fnProximoCodigo('CONTATOOCORRENCIA');
      qryContatoOcorrencianCdUsuarioContato.Value    := frmMenu.nCdUsuarioLogado;
      qryContatoOcorrencianCdOcorrencia.Value        := nCdOcorrencia;
      qryContatoOcorrenciacDescAtendimento.Value     := UpperCase(qryContatoOcorrenciacDescAtendimento.Value);
      qryContatoOcorrencia.Post;

      { -- 3 : an�lise            -- }
      { -- 4 : retorno an�lise    -- }
      { -- 5 : aguardando contato -- }
      { -- 6 : aguardando retorno -- }
      if (StrToInt(dbLkStatusOcorrencia.KeyValue) in [3,4,5,6]) then
      begin
          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('UPDATE Ocorrencia SET nCdTabStatusOcorrencia = ' + VarToStr(dbLkStatusOcorrencia.KeyValue) + ' WHERE nCdOcorrencia = ' + IntToStr(nCdOcorrencia));
          qryAux.ExecSQL;
      end
      else
      begin
          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('UPDATE Ocorrencia SET nCdTabStatusOcorrencia = ' + VarToStr(dblkStatusOcorrencia.KeyValue) + ',nCdUsuarioAtend = ' + IntToStr(frmMenu.nCdUsuarioLogado) + ',dDtUsuarioAtend = GETDATE() WHERE nCdOcorrencia = ' + IntToStr(nCdOcorrencia)) ;
          qryAux.ExecSQL;
      end;

      frmMenu.LogAuditoria(529,2,nCdOcorrencia,'Atualiza��o de Status de ' + cNmTabSatusAtual + ' para ' + UpperCase(dbLkStatusOcorrencia.Text));
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      Raise;
      Exit;
  end;

  frmMenu.Connection.CommitTrans;

  Close;
end;

procedure TfrmRegAtendOcorrencia_Atendimento.btExibeClienteClick(
  Sender: TObject);
var
  objContato : TfrmRegAtendOcorrencia_Atendimento_SelContato;
begin
  inherited;

  try
      objContato := TfrmRegAtendOcorrencia_Atendimento_SelContato.Create(nil);

      PosicionaQuery(objContato.qryListaContato, IntToStr(nCdTerceiro));

      showForm(objContato,False);

      if (not objContato.qryListaContato.Eof) then
      begin
          qryContatoOcorrenciacTelefoneContato.Value := objContato.qryListaContatocTelefone.Value;

          if (Trim(qryContatoOcorrenciacNmContatoOcorrencia.Value) = '') then
              qryContatoOcorrenciacNmContatoOcorrencia.Value := objContato.qryListaContatocContato.Value;
      end;
  finally
      FreeAndNil(objContato);
  end;
end;

end.
