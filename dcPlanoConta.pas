unit dcPlanoConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  TeeProcs, TeEngine, Chart, MXGRAPH, DB, MXPIVSRC, Grids, MXGRID, ADODB,
  Mxstore, MXDB, DBTables, MXTABLES, PivotMap_SRC, PivotToolBar_SRC,
  PivotGrid_SRC, PivotCube_SRC, StdCtrls, Mask, Series;

type
  TdcmPlanoConta = class(TfrmProcesso_Padrao)
    qryCube: TADOQuery;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresanCdTerceiroEmp: TIntegerField;
    qryEmpresanCdTerceiroMatriz: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresanCdStatus: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryUnidadeNegocio: TADOQuery;
    qryPlanoConta: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryPlanoContanCdPlanoConta: TAutoIncField;
    qryPlanoContacNmPlanoConta: TStringField;
    qryPlanoContanCdGrupoPlanoConta: TIntegerField;
    qryPlanoContacFlgRecDes: TStringField;
    DecisionSource1: TDecisionSource;
    DecisionCube1: TDecisionCube;
    DecisionGrid1: TDecisionGrid;
    ER2Soft: TDecisionPivot;
    qryCubecNmUnidadeNegocio: TStringField;
    qryCubecNmPlanoConta: TStringField;
    qryCubeMes: TStringField;
    qryCubenValLancto: TBCDField;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    qryCubecSigla: TStringField;
    qryCubeTipo: TStringField;
    Label1: TLabel;
    Label2: TLabel;
    qryParametro: TADOQuery;
    qryParametrocValor: TStringField;
    qryCubecNmGrupoPlanoConta: TStringField;
    DecisionGraph1: TDecisionGraph;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmPlanoConta: TdcmPlanoConta;

implementation

uses fMenu;

{$R *.dfm}

procedure TdcmPlanoConta.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (trim(MaskEdit1.Text) = '/  /') or (Trim(MaskEdit2.Text) = '/  /') then
  begin
      qryParametro.Close ;
      qryParametro.Parameters.ParamByName('cParametro').Value := 'DTCONTAB' ;
      qryParametro.Open ;

      if not qryParametro.eof then
      begin
          if (qryParametrocValor.Value <> '') then
          begin
              MaskEdit2.Text := qryParametrocValor.Value ;
              MaskEdit1.Text := DateTimeToStr(StrToDateTime(qryParametrocValor.Value) - 60) ;
          end ;
      end ;

  end ;

  if (trim(MaskEdit1.Text) = '/  /') or (Trim(MaskEdit2.Text) = '/  /') then
  begin
      ShowMessage('Informe o per�odo de an�lise.') ;
      exit ;
  end ;

  qryCube.Close ;
  qryCube.Parameters.ParamByName('dDtInicial').Value := Trim(MaskEdit1.Text) ;
  qryCube.Parameters.ParamByName('dDtFinal').Value   := Trim(MaskEdit2.Text) ;
  qryCube.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryCube.Open ;

end;

procedure TdcmPlanoConta.FormShow(Sender: TObject);
begin
  inherited;
  MaskEdit1.SetFocus;
end;

initialization
    RegisterClass(tdcmPlanoConta) ;

end.
