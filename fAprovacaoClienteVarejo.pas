unit fAprovacaoClienteVarejo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, cxPC,
  cxControls, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxEdit, DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, ADODB, Menus, cxLookAndFeelPainters, StdCtrls,
  cxButtons;

type
  TfrmAprovacaoClienteVarejo = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    Tab1: TcxTabSheet;
    Tab2: TcxTabSheet;
    Tab3: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    qryCadastrados: TADOQuery;
    qryCadastradosnCdTerceiro: TIntegerField;
    qryCadastradoscCNPJCPF: TStringField;
    qryCadastradoscRG: TStringField;
    qryCadastradoscNmTerceiro: TStringField;
    qryCadastradoscNmUsuario: TStringField;
    dsCadastrados: TDataSource;
    cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cCNPJCPF: TcxGridDBColumn;
    cxGrid1DBTableView1cRG: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    qryPendentes: TADOQuery;
    dsPendentes: TDataSource;
    qryPendentesnCdTerceiro: TIntegerField;
    qryPendentescCNPJCPF: TStringField;
    qryPendentescRG: TStringField;
    qryPendentescNmTerceiro: TStringField;
    qryPendentescNmUsuario: TStringField;
    PopupMenu1: TPopupMenu;
    ExibirCadastro1: TMenuItem;
    AprovarCadastro1: TMenuItem;
    MarcarcomoPendente1: TMenuItem;
    BloquearCadastro1: TMenuItem;
    AtualizarLista1: TMenuItem;
    N1: TMenuItem;
    qryAux: TADOQuery;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem6: TMenuItem;
    qryPendentescOBSSituacaoCadastro: TStringField;
    cxGridDBTableView1DBColumn1: TcxGridDBColumn;
    GroupBox1: TGroupBox;
    edtNmCliente: TEdit;
    Label1: TLabel;
    edtRG: TEdit;
    Label2: TLabel;
    btConsultar: TcxButton;
    qryBloqueados: TADOQuery;
    dsBloqueados: TDataSource;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    PopupMenu3: TPopupMenu;
    MenuItem5: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    qryBloqueadosnCdTerceiro: TIntegerField;
    qryBloqueadoscCNPJCPF: TStringField;
    qryBloqueadoscRG: TStringField;
    qryBloqueadoscNmTerceiro: TStringField;
    qryBloqueadoscNmUsuario: TStringField;
    qryBloqueadoscOBSSituacaoCadastro: TStringField;
    qryCadastradoscFlgCadSimples: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure Tab1Show(Sender: TObject);
    procedure Tab2Show(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ExibirCadastro1Click(Sender: TObject);
    procedure AtualizarLista1Click(Sender: TObject);
    procedure MarcarcomoPendente1Click(Sender: TObject);
    procedure BloquearCadastro1Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure AprovarCadastro1Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure btConsultarClick(Sender: TObject);
    procedure MenuItem8Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAprovacaoClienteVarejo: TfrmAprovacaoClienteVarejo;
  cObs : string ;

implementation

uses
  fMenu, fClienteVarejoPessoaFisica, fAprovacaoClienteVarejo_LimiteInicial,
  fClienteVarejoPessoaFisica_Simples;

{$R *.dfm}

procedure TfrmAprovacaoClienteVarejo.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmAprovacaoClienteVarejo.Tab1Show(Sender: TObject);
begin
  inherited;
  if not (qryCadastrados.Active) then
      PosicionaQuery(qryCadastrados, intToStr(frmMenu.nCdLojaAtiva)) ;

end;

procedure TfrmAprovacaoClienteVarejo.Tab2Show(Sender: TObject);
begin
  inherited;
  if not (qryPendentes.Active) then
      PosicionaQuery(qryPendentes, intToStr(frmMenu.nCdLojaAtiva)) ;

end;

procedure TfrmAprovacaoClienteVarejo.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  ExibirCadastro1.Enabled     := True ;
  AprovarCadastro1.Enabled    := True ;
  MarcarcomoPendente1.Enabled := True ;
  BloquearCadastro1.Enabled   := True ;

  if not (qryCadastrados.Active) or (qryCadastrados.RecordCount = 0) then
  begin
      ExibirCadastro1.Enabled     := False ;
      AprovarCadastro1.Enabled    := False ;
      MarcarcomoPendente1.Enabled := False ;
      BloquearCadastro1.Enabled   := False ;
  end ;

end;

procedure TfrmAprovacaoClienteVarejo.ExibirCadastro1Click(Sender: TObject);
var
  objForm        : TfrmClienteVarejoPessoaFisica;
  objFormSimples : TfrmClienteVarejoPessoaFisica_Simples;
begin
  inherited;

  case (qryCadastradoscFlgCadSimples.Value) of
      0 : begin
          objForm := TfrmClienteVarejoPessoaFisica.Create(nil);

          objForm.PosicionaQuery(objForm.qryMaster, qryCadastradosnCdTerceiro.asString);
          objForm.bChamadaExterna := True;
          showForm(objForm,True);
      end;
      1 : begin
          objFormSimples := TfrmClienteVarejoPessoaFisica_Simples.Create(Nil);

          objFormSimples.PosicionaQuery(objFormSimples.qryMaster, qryCadastradosnCdTerceiro.AsString);
          showForm(objFormSimples,True);
      end;
  end;
end;

procedure TfrmAprovacaoClienteVarejo.AtualizarLista1Click(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryCadastrados, intToStr(frmMenu.nCdLojaAtiva)) ;

end;

procedure TfrmAprovacaoClienteVarejo.MarcarcomoPendente1Click(
  Sender: TObject);
begin
  inherited;

  InputQuery('Observa��o (m�x. 50 caracteres)','Observa��o da Pend�ncia',cObs) ;

  if (MessageDlg('Confirma o registro de pend�ncia para este cadastro ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  cObs := Copy(cObs, 1, 50);

  qryAux.Close ;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE PessoaFisica SET nCdUsuarioAprova = ' + intToStr(frmMenu.nCdUsuarioLogado)+ ', dDtusuarioAprova = GetDate() ,nCdTabTipoSituacao = 3 , cOBSSituacaoCadastro = UPPER(' + Chr(39) + cObs + Chr(39) + ') WHERE nCdTerceiro = ' + qryCadastradosnCdTerceiro.AsString) ;

  try
      qryAux.ExecSQL;
  except
      MensagemErro('Erro no processamento.') ;
      raise;
  end ;

  ShowMessage('Cadastro alterado com sucesso!') ;

  PosicionaQuery(qryCadastrados, intToStr(frmMenu.nCdLojaAtiva)) ;

end;

procedure TfrmAprovacaoClienteVarejo.BloquearCadastro1Click(
  Sender: TObject);
begin
  inherited;

  InputQuery('Observa��o (m�x. 50 caracteres)','Motivo do Bloqueio',cObs) ;

  if (MessageDlg('Confirma o registro do bloqueio deste cadastro ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  cObs := Copy(cObs, 1, 50);

  qryAux.Close ;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE PessoaFisica SET nCdUsuarioAprova = ' + intToStr(frmMenu.nCdUsuarioLogado)+ ', dDtusuarioAprova = GetDate() ,nCdTabTipoSituacao = 4 , cOBSSituacaoCadastro = UPPER(' + Chr(39) + cObs + Chr(39) + ') WHERE nCdTerceiro = ' + qryCadastradosnCdTerceiro.AsString) ;

  try
      qryAux.ExecSQL;
  except
      MensagemErro('Erro no processamento.') ;
      raise;
  end ;

  ShowMessage('Cadastro bloqueado com sucesso!') ;

  PosicionaQuery(qryCadastrados, intToStr(frmMenu.nCdLojaAtiva)) ;

end;

procedure TfrmAprovacaoClienteVarejo.MenuItem1Click(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryPendentes, intToStr(frmMenu.nCdLojaAtiva)) ;

end;

procedure TfrmAprovacaoClienteVarejo.MenuItem3Click(Sender: TObject);
var
  objForm : TfrmClienteVarejoPessoaFisica;
begin
  inherited;

  objForm := TfrmClienteVarejoPessoaFisica.Create(nil);

  objForm.PosicionaQuery(objForm.qryMaster, qryPendentesnCdTerceiro.asString);
  objForm.bChamadaExterna     := True ;
  showForm(objForm,true) ;

end;

procedure TfrmAprovacaoClienteVarejo.MenuItem6Click(Sender: TObject);
begin
  inherited;
  InputQuery('Observa��o (m�x. 50 caracteres)','Motivo do Bloqueio',cObs) ;

  if (MessageDlg('Confirma o registro do bloqueio deste cadastro ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  cObs := Copy(cObs, 1, 50);

  qryAux.Close ;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('UPDATE PessoaFisica SET nCdUsuarioAprova = ' + intToStr(frmMenu.nCdUsuarioLogado)+ ', dDtusuarioAprova = GetDate() ,nCdTabTipoSituacao = 4 , cOBSSituacaoCadastro = UPPER(' + Chr(39) + cObs + Chr(39) + ') WHERE nCdTerceiro = ' + qryPendentesnCdTerceiro.AsString) ;

  try
      qryAux.ExecSQL;
  except
      MensagemErro('Erro no processamento.') ;
      raise;
  end ;

  ShowMessage('Cadastro bloqueado com sucesso!') ;

  PosicionaQuery(qryPendentes, intToStr(frmMenu.nCdLojaAtiva)) ;

end;

procedure TfrmAprovacaoClienteVarejo.AprovarCadastro1Click(
  Sender: TObject);
var
  objForm : TfrmAprovacaoClienteVarejo_LimiteInicial;
begin
  inherited;

  objForm := TfrmAprovacaoClienteVarejo_LimiteInicial.Create(nil);

  objForm.nCdTerceiro            := qryCadastradosnCdTerceiro.Value ;
  objForm.cOBS                   := 'LIMITE INICIAL' ;
  objForm.edtLimiteCredito.Value := 0 ;
  objForm.edtPercEntrada.Value   := 0 ;
  objForm.bAprovacao             := true ;
  objForm.bLimiteManual          := false ;
  showForm(objForm,true) ;

  PosicionaQuery(qryCadastrados, intToStr(frmMenu.nCdLojaAtiva)) ;

end;

procedure TfrmAprovacaoClienteVarejo.MenuItem4Click(Sender: TObject);
var
  objForm : TfrmAprovacaoClienteVarejo_LimiteInicial;
begin
  inherited;

  objForm := TfrmAprovacaoClienteVarejo_LimiteInicial.Create(nil);

  objForm.nCdTerceiro            := qryPendentesnCdTerceiro.Value ;
  objForm.cOBS                   := 'LIMITE INICIAL' ;
  objForm.edtLimiteCredito.Value := 0 ;
  objForm.edtPercEntrada.Value   := 0 ;
  objForm.bAprovacao             := true ;
  objForm.bLimiteManual          := false ;
  showForm(objForm,true);

  qryPendentes.Close ;
  qryPendentes.Open ;

end;

procedure TfrmAprovacaoClienteVarejo.btConsultarClick(Sender: TObject);
begin
  inherited;

  if (edtNmCliente.Text = '') and (edtRG.Text = '') then
  begin
      ShowMessage('Informe o nome do cliente ou o n�mero do R.G.') ;
      edtNmCliente.SetFocus;
      exit ;
  end ;

  qryBloqueados.Close ;
  qryBloqueados.Parameters.ParamByName('cNmTerceiro').Value := edtNmCliente.Text ;
  qryBloqueados.Parameters.ParamByName('cRG').Value         := edtRG.Text ;
  qryBloqueados.Open ;

  if (qryBloqueados.Eof) then
  begin
      MensagemAlerta('Nenhum cliente bloqueado encontrado com os crit�rios informados.') ;
      edtNmCliente.Text := '' ;
      edtRG.Text        := '' ;

      edtNmCliente.SetFocus ;
      exit ;
  end ;

  
end;

procedure TfrmAprovacaoClienteVarejo.MenuItem8Click(Sender: TObject);
var
  objForm : TfrmClienteVarejoPessoaFisica;
begin
  inherited;

  objForm := TfrmClienteVarejoPessoaFisica.Create(nil);

  objForm.PosicionaQuery(objForm.qryMaster, qryBloqueadosnCdTerceiro.asString);
  objForm.bChamadaExterna     := True ;
  showForm(objForm,true) ;

end;

procedure TfrmAprovacaoClienteVarejo.MenuItem5Click(Sender: TObject);
begin
  inherited;
  qryBloqueados.Close ;
  qryBloqueados.Open ;

end;

procedure TfrmAprovacaoClienteVarejo.MenuItem9Click(Sender: TObject);
var
  objForm : TfrmAprovacaoClienteVarejo_LimiteInicial;
begin
  inherited;

  objForm := TfrmAprovacaoClienteVarejo_LimiteInicial.Create(nil);

  objForm.nCdTerceiro            := qryBloqueadosnCdTerceiro.Value ;
  objForm.cOBS                   := 'LIMITE INICIAL' ;
  objForm.edtLimiteCredito.Value := 0 ;
  objForm.edtPercEntrada.Value   := 0 ;
  objForm.bAprovacao             := true ;
  objForm.bLimiteManual          := false ;
  showForm(objForm,true) ;

  qryBloqueados.Close ;
  qryBloqueados.Open ;

end;

initialization
    RegisterClass(TfrmAprovacaoClienteVarejo);
end.
