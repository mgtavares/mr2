unit rAlteracaoPrecoProduto_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptAlteracaoPrecoProduto_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText3: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText6: TQRDBText;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRDBText9: TQRDBText;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRLabel4: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRShape2: TQRShape;
    qryHistAltPreco: TADOQuery;
    qryHistAltPrecodDtAltPreco: TDateTimeField;
    qryHistAltPreconCdProduto: TIntegerField;
    qryHistAltPrecocNmProduto: TStringField;
    qryHistAltPreconCdTabTipoProduto: TIntegerField;
    qryHistAltPreconValPrecoAtual: TBCDField;
    qryHistAltPreconValPrecoAnterior: TBCDField;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    QRLabel1: TQRLabel;
    qryHistAltPreconQtdeEstoque: TBCDField;
    QRLabel5: TQRLabel;
    QRDBText2: TQRDBText;
    qryHistAltPrecocOBS: TStringField;
    QRDBText5: TQRDBText;
    QRLabel10: TQRLabel;
    qryHistAltPrecocNmProdutoPai: TStringField;
    QRGroup1: TQRGroup;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptAlteracaoPrecoProduto_view: TrptAlteracaoPrecoProduto_view;

implementation

{$R *.dfm}

end.
