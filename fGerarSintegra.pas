unit fGerarSintegra;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ACBrSintegra, ImgList, ComCtrls, ToolWin,
  ExtCtrls, DB, ADODB, StdCtrls, Mask, ER2Lookup, DBCtrls;

type
  TfrmGerarSintegra = class(TfrmProcesso_Padrao)
    ACBrSintegra1: TACBrSintegra;
    qryEmpresa: TADOQuery;
    qryLoja: TADOQuery;
    SaveDialog1: TSaveDialog;
    edtEmpresa: TER2LookupMaskEdit;
    edtLoja: TER2LookupMaskEdit;
    edtDtInicial: TMaskEdit;
    edtDtFinal: TMaskEdit;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    cbFinalidade: TComboBox;
    qryDadosInformante: TADOQuery;
    qryNotasFiscais: TADOQuery;
    qryItensDoctoFiscal: TADOQuery;
    SP_GERA_DADOS_SINTEGRA: TADOStoredProc;
    cmdPreparaTemp: TADOCommand;
    qryEmpresanCdTerceiroEmp: TIntegerField;
    qryLojanCdTerceiro: TIntegerField;
    qryDadosInformantecCNPJCPF: TStringField;
    qryDadosInformantecIE: TStringField;
    qryDadosInformantecNmTerceiro: TStringField;
    qryDadosInformantecCidade: TStringField;
    qryDadosInformantecUF: TStringField;
    qryDadosInformantecFax: TStringField;
    qryDadosInformantecEndereco: TStringField;
    qryDadosInformanteiNumero: TIntegerField;
    qryDadosInformantecComplemento: TStringField;
    qryDadosInformantecBairro: TStringField;
    qryDadosInformantecCep: TStringField;
    qryDadosInformantecNmContato: TStringField;
    qryNotasFiscaisnCdDoctoFiscal: TIntegerField;
    qryNotasFiscaiscCNPJCPF: TStringField;
    qryNotasFiscaiscIE: TStringField;
    qryNotasFiscaisdDtEmissao: TDateTimeField;
    qryNotasFiscaiscUF: TStringField;
    qryNotasFiscaiscModelo: TStringField;
    qryNotasFiscaiscSerie: TStringField;
    qryNotasFiscaisiNrDocto: TIntegerField;
    qryNotasFiscaiscCFOP: TStringField;
    qryNotasFiscaiscEmitente: TStringField;
    qryNotasFiscaisnValTotal: TBCDField;
    qryNotasFiscaisnValBaseICMS: TBCDField;
    qryNotasFiscaisnValICMS: TBCDField;
    qryNotasFiscaisnValIsenta: TBCDField;
    qryNotasFiscaisnValOutras: TBCDField;
    qryNotasFiscaisnAliqICMS: TBCDField;
    qryNotasFiscaiscSituacaoCancel: TStringField;
    qryItensDoctoFiscalnCdItemDoctoFiscal: TIntegerField;
    qryItensDoctoFiscalcCNPJCPF: TStringField;
    qryItensDoctoFiscalcModelo: TStringField;
    qryItensDoctoFiscalcSerie: TStringField;
    qryItensDoctoFiscaliNrDocto: TIntegerField;
    qryItensDoctoFiscalcCFOP: TStringField;
    qryItensDoctoFiscaliOrdemItem: TIntegerField;
    qryItensDoctoFiscalnCdProduto: TIntegerField;
    qryItensDoctoFiscalnQtde: TBCDField;
    qryItensDoctoFiscalnValUnitario: TBCDField;
    qryItensDoctoFiscalnValDesconto: TBCDField;
    qryItensDoctoFiscalnValDespesa: TBCDField;
    qryItensDoctoFiscalnValBaseICMS: TBCDField;
    qryItensDoctoFiscalnValBaseICMSSub: TBCDField;
    qryItensDoctoFiscalnValIPI: TBCDField;
    qryItensDoctoFiscaldDtInicial: TDateTimeField;
    qryItensDoctoFiscaldDtFinal: TDateTimeField;
    qryItensDoctoFiscalcNCM: TStringField;
    qryItensDoctoFiscalcNmItem: TStringField;
    qryItensDoctoFiscalcUnidadeMedida: TStringField;
    qryItensDoctoFiscalnAliqIPI: TBCDField;
    qryItensDoctoFiscalnAliqICMS: TBCDField;
    qryItensDoctoFiscalnPercRedBCICMS: TBCDField;
    qryDadosInformantecTelefone: TStringField;
    qryItensDoctoFiscalcCdST: TStringField;
    qrySubTributaria: TADOQuery;
    qrySubTributarianCdDoctoFiscal: TIntegerField;
    qrySubTributariacCNPJCPF: TStringField;
    qrySubTributariacIE: TStringField;
    qrySubTributariadDtEmissao: TDateTimeField;
    qrySubTributariacUF: TStringField;
    qrySubTributariacModelo: TStringField;
    qrySubTributariacSerie: TStringField;
    qrySubTributariaiNrDocto: TIntegerField;
    qrySubTributariacCFOP: TStringField;
    qrySubTributariacEmitente: TStringField;
    qrySubTributarianValBaseICMSST: TBCDField;
    qrySubTributarianValICMSRetido: TBCDField;
    qrySubTributarianValDespesas: TBCDField;
    qrySubTributariacSituacaoCancel: TStringField;
    qrySubTributariacCdAnteceipacao: TStringField;
    qryDoctoIPI: TADOQuery;
    qryDoctoIPInCdDoctoFiscal: TIntegerField;
    qryDoctoIPIcCNPJCPF: TStringField;
    qryDoctoIPIcIE: TStringField;
    qryDoctoIPIdDtEmissao: TDateTimeField;
    qryDoctoIPIcUF: TStringField;
    qryDoctoIPIcSerie: TStringField;
    qryDoctoIPIiNrDocto: TIntegerField;
    qryDoctoIPIcCFOP: TStringField;
    qryDoctoIPInValTotal: TBCDField;
    qryDoctoIPInValIPI: TBCDField;
    qryDoctoIPInValIsentaIPI: TBCDField;
    qryDoctoIPInValOutrasIPI: TBCDField;
    qryDoctoIPIcSituacaoCancel: TStringField;
    qryTempMovtoECFItem: TADOQuery;
    qryTempMovtoECFItemnCdMovtoECFItem: TAutoIncField;
    qryTempMovtoECFItemnCdDoctoFiscal: TIntegerField;
    qryTempMovtoECFItemdDtEmissao: TDateTimeField;
    qryTempMovtoECFItemcCdNumSerieECF: TStringField;
    qryTempMovtoECFItemcModelo: TStringField;
    qryTempMovtoECFItemiNrDocto: TIntegerField;
    qryTempMovtoECFItemiOrdemItem: TIntegerField;
    qryTempMovtoECFItemnCdProduto: TIntegerField;
    qryTempMovtoECFItemnQtde: TBCDField;
    qryTempMovtoECFItemnValTotal: TBCDField;
    qryTempMovtoECFItemnValBaseICMS: TBCDField;
    qryTempMovtoECFItemcCdSTAliq: TStringField;
    qryTempMovtoECFItemnValICMS: TBCDField;
    qryMovtoDiaECF: TADOQuery;
    qryMovtoDiaECFAnalitico: TADOQuery;
    qryMovtoDiaECFnCdMovtoDiaECF: TIntegerField;
    qryMovtoDiaECFnCdEmpresa: TIntegerField;
    qryMovtoDiaECFnCdLoja: TIntegerField;
    qryMovtoDiaECFdDtMovto: TDateTimeField;
    qryMovtoDiaECFcCdNumSerieECF: TStringField;
    qryMovtoDiaECFiNrECF: TIntegerField;
    qryMovtoDiaECFcModelo: TStringField;
    qryMovtoDiaECFiNrOrdemOperInicial: TIntegerField;
    qryMovtoDiaECFiNrOrdemOperFinal: TIntegerField;
    qryMovtoDiaECFiNrReducaoZ: TIntegerField;
    qryMovtoDiaECFiNrReinicioOper: TIntegerField;
    qryMovtoDiaECFnValVendaBruta: TBCDField;
    qryMovtoDiaECFnValTotalGeral: TBCDField;
    qryMovtoDiaECFAnaliticonCdMovtoDiaECFAnalitico: TIntegerField;
    qryMovtoDiaECFAnaliticonCdMovtoDiaECF: TIntegerField;
    qryMovtoDiaECFAnaliticocCdSTAliq: TStringField;
    qryMovtoDiaECFAnaliticonValAcumTotalParcial: TBCDField;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtEmpresaBeforePosicionaQry(Sender: TObject);
    procedure edtLojaBeforePosicionaQry(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGerarSintegra: TfrmGerarSintegra;

implementation

uses fMenu, ConvUtils;

{$R *.dfm}

procedure TfrmGerarSintegra.ToolButton1Click(Sender: TObject);
var
  mRegistro50  : TRegistro50;
  wRegistro51  : TRegistro51;
  wregistro54  : TRegistro54;
  wregistro75  : TRegistro75;
  wRegistro53  : TRegistro53;
  wRegistro60M : TRegistro60M;
  wRegistro60A : TRegistro60A;
  wRegistro60I : TRegistro60I;
begin
  inherited;

  edtEmpresa.PosicionaQuery;
  edtLoja.PosicionaQuery;

  if (Trim(DBEdit1.Text) = '') then
  begin
      MensagemAlerta('Selecione uma empresa.');
      edtEmpresa.SetFocus;
      abort;
  end;

  if ((Trim(DBEdit2.Text) = '') and (frmMenu.LeParametro('VAREJO') = 'S')) then
  begin
      MensagemAlerta('Selecione uma loja.');
      edtLoja.SetFocus;
      abort;
  end;

  if (Trim(edtDtInicial.Text) = '/  /') then
  begin
      MensagemAlerta('Digite a data inicial.');
      edtDtInicial.SetFocus;
      abort;
  end;

  if (Trim(edtDtFinal.Text) = '/  /') then
  begin
      MensagemAlerta('Digite a data final.');
      edtDtFinal.SetFocus;
      abort;
  end;

  if (frmMenu.ConvData(edtDtInicial.Text) > frmMenu.ConvData(edtDtFinal.Text)) then
  begin
      MensagemAlerta('Data inicial n�o pode ser maior que a data final.');
      edtDtInicial.SetFocus;
      abort;
  end;

  if (not DirectoryExists(ExtractFilePath(Application.ExeName) + '/SINTEGRA/')) then
      CreateDir(ExtractFilePath(Application.ExeName) + '/SINTEGRA/');

  SaveDialog1.InitialDir := ExtractFilePath(Application.ExeName) + '/SINTEGRA';
  SaveDialog1.Filter     := 'Arquivo Texto (.txt)|*.txt';
  SaveDialog1.DefaultExt := '*.txt';
  SaveDialog1.FileName   := 'SINTEGRA' + frmMenu.ZeroEsquerda(Trim(edtEmpresa.Text),3)
                                       + StringReplace(edtDtInicial.Text,'/','',[rfReplaceAll])
                                       + '-' + StringReplace(edtDtFinal.Text,'/','',[rfReplaceAll]);

  if (not SaveDialog1.Execute) then
      exit;

  if (MessageDLG('Deseja realmente gerar o arquivo do SINTEGRA ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      exit;

  ACBrSintegra1.FileName := SaveDialog1.FileName;

  cmdPreparaTemp.Execute;

  try

      SP_GERA_DADOS_SINTEGRA.Close;

      if (frmMenu.LeParametro('VAREJO') = 'N') then
          SP_GERA_DADOS_SINTEGRA.Parameters.ParamByName('@nCdLoja').Value := 0
      else SP_GERA_DADOS_SINTEGRA.Parameters.ParamByName('@nCdLoja').Value := qryLojanCdLoja.Value;

      SP_GERA_DADOS_SINTEGRA.Parameters.ParamByName('@nCdEmpresa').Value  := qryEmpresanCdEmpresa.Value;
      SP_GERA_DADOS_SINTEGRA.Parameters.ParamByName('@dDtInicial').Value  := edtDtInicial.Text;
      SP_GERA_DADOS_SINTEGRA.Parameters.ParamByName('@dDtFinal').Value    := edtDtFinal.Text;

      SP_GERA_DADOS_SINTEGRA.ExecProc;

      qryDadosInformante.Close;
      qryDadosInformante.Open;

      qryNotasFiscais.Close;
      qryNotasFiscais.Open;

      qryItensDoctoFiscal.Close;
      qryItensDoctoFiscal.Open;

      qrySubTributaria.Close;
      qrySubTributaria.Open;

      qryDoctoIPI.Close;
      qryDoctoIPI.Open;

      //Registro 10
      ACBrSintegra1.Registro10.CNPJ                := qryDadosInformantecCNPJCPF.Value;
      ACBrSintegra1.Registro10.Inscricao           := qryDadosInformantecIE.Value;
      ACBrSintegra1.Registro10.RazaoSocial         := qryDadosInformantecNmTerceiro.Value;
      ACBrSintegra1.Registro10.Cidade              := qryDadosInformantecCidade.Value;
      ACBrSintegra1.Registro10.Estado              := qryDadosInformantecUF.Value;
      ACBrSintegra1.Registro10.Telefone            := qryDadosInformantecFax.Value;
      ACBrSintegra1.Registro10.DataInicial         := frmMenu.ConvData(edtDtInicial.Text);
      ACBrSintegra1.Registro10.DataFinal           := frmMenu.ConvData(edtDtFinal.Text);
      ACBrSintegra1.Registro10.CodigoConvenio      := '3';
      ACBrSintegra1.Registro10.NaturezaInformacoes := '3';
      ACBrSintegra1.Registro10.FinalidadeArquivo   := Copy(cbFinalidade.Items[cbFinalidade.ItemIndex], 1, 1) ;

      //Registro 11
      ACBrSintegra1.Registro11.Endereco    := qryDadosInformantecEndereco.Value;
      ACBrSintegra1.Registro11.Numero      := qryDadosInformanteiNumero.AsString;
      ACBrSintegra1.Registro11.Complemento := qryDadosInformantecComplemento.Value;
      ACBrSintegra1.Registro11.Bairro      := qryDadosInformantecBairro.Value;
      ACBrSintegra1.Registro11.Cep         := qryDadosInformantecCep.Value;
      ACBrSintegra1.Registro11.Responsavel := qryDadosInformantecNmContato.Value;
      ACBrSintegra1.Registro11.Telefone    := qryDadosInformantecTelefone.Value;

      //Registro 50

      qryNotasFiscais.First;

      while not qryNotasFiscais.Eof do
      begin
          mRegistro50 := TRegistro50.Create;

          mRegistro50.CPFCNPJ          := qryNotasFiscaiscCNPJCPF.Value;
          mRegistro50.Inscricao        := qryNotasFiscaiscIE.Value;
          mRegistro50.DataDocumento    := qryNotasFiscaisdDtEmissao.Value;
          mRegistro50.UF               := qryNotasFiscaiscUF.Value;
          mRegistro50.Modelo           := qryNotasFiscaiscModelo.Value;
          mRegistro50.Serie            := qryNotasFiscaiscSerie.Value;
          mRegistro50.Numero           := qryNotasFiscaisiNrDocto.AsString;
          mRegistro50.Cfop             := qryNotasFiscaiscCFOP.Value;
          mRegistro50.EmissorDocumento := qryNotasFiscaiscEmitente.Value;
          mRegistro50.ValorContabil    := qryNotasFiscaisnValTotal.Value;
          mRegistro50.BasedeCalculo    := qryNotasFiscaisnValBaseICMS.Value;
          mRegistro50.Icms             := qryNotasFiscaisnValICMS.Value;
          mRegistro50.Isentas          := qryNotasFiscaisnValIsenta.Value;
          mRegistro50.Outras           := qryNotasFiscaisnValOutras.Value;
          mRegistro50.Aliquota         := qryNotasFiscaisnAliqICMS.Value;
          mRegistro50.Situacao         := qryNotasFiscaiscSituacaoCancel.Value;

          ACBrSintegra1.Registros50.Add(mRegistro50);

          qryNotasFiscais.Next;
      end;

      //Registro 51

      qryDoctoIPI.First;

      while not qryDoctoIPI.Eof do
      begin
          wRegistro51 := TRegistro51.Create;

          wRegistro51.CPFCNPJ       := qryDoctoIPIcCNPJCPF.Value;
          wRegistro51.Inscricao     := qryDoctoIPIcIE.Value;
          wRegistro51.DataDocumento := qryDoctoIPIdDtEmissao.Value;
          wRegistro51.Estado        := qryDoctoIPIcUF.Value;
          wRegistro51.Serie         := qryDoctoIPIcSerie.Value;
          wRegistro51.Numero        := qryDoctoIPIiNrDocto.AsString;
          wRegistro51.CFOP          := qryDoctoIPIcCFOP.Value;
          wRegistro51.ValorContabil := qryDoctoIPInValTotal.Value;
          wRegistro51.ValorIpi      := qryDoctoIPInValIPI.Value;
          wRegistro51.ValorOutras   := qryDoctoIPInValOutrasIPI.Value;
          wRegistro51.ValorIsentas  := qryDoctoIPInValIsentaIPI.Value;
          wRegistro51.Situacao      := qryDoctoIPIcSituacaoCancel.Value;

          ACBrSintegra1.Registros51.Add(wRegistro51);

          qryDoctoIPI.Next;
      end;

      //Registro 53

      qrySubTributaria.First;

      while not qrySubTributaria.Eof do
      begin
          wRegistro53 := TRegistro53.Create;

          wRegistro53.CPFCNPJ           := qrySubTributariacCNPJCPF.Value;
          wRegistro53.Inscricao         := qrySubTributariacIE.Value;
          wRegistro53.DataDocumento     := qrySubTributariadDtEmissao.Value;
          wRegistro53.Estado            := qrySubTributariacUF.Value;
          wRegistro53.Modelo            := qrySubTributariacModelo.Value;
          wRegistro53.Serie             := qrySubTributariacSerie.Value;
          wRegistro53.Numero            := qrySubTributariaiNrDocto.AsString;
          wRegistro53.CFOP              := qrySubTributariacCFOP.Value;
          wRegistro53.Emitente          := qrySubTributariacEmitente.Value;
          wRegistro53.BaseST            := qrySubTributarianValBaseICMSST.Value;
          wRegistro53.IcmsRetido        := qrySubTributarianValICMSRetido.Value;
          wRegistro53.Despesas          := qrySubTributarianValDespesas.Value;
          wRegistro53.Situacao          := qrySubTributariacSituacaoCancel.Value;
          wRegistro53.CodigoAntecipacao := qrySubTributariacCdAnteceipacao.Value;

          ACBrSintegra1.Registros53.Add(wRegistro53);

          qrySubTributaria.Next;
      end;

      //Registro 54

      qryItensDoctoFiscal.First;

      while not qryItensDoctoFiscal.Eof do
      begin

          wregistro54 := TRegistro54.Create;
          wregistro75 := TRegistro75.Create;

          wregistro54.CPFCNPJ              := qryItensDoctoFiscalcCNPJCPF.Value;
          wregistro54.Modelo               := qryItensDoctoFiscalcModelo.Value;
          wregistro54.Serie                := qryItensDoctoFiscalcSerie.Value;
          wregistro54.Numero               := qryItensDoctoFiscaliNrDocto.AsString;
          wregistro54.CFOP                 := qryItensDoctoFiscalcCFOP.Value;
          wregistro54.CST                  := qryItensDoctoFiscalcCdST.Value;
          wregistro54.NumeroItem           := qryItensDoctoFiscaliOrdemItem.Value;
          wregistro54.Codigo               := qryItensDoctoFiscalnCdProduto.AsString;
          wregistro54.Descricao            := qryItensDoctoFiscalcNmItem.Value;
          wregistro54.Quantidade           := qryItensDoctoFiscalnQtde.Value;
          wregistro54.Valor                := qryItensDoctoFiscalnValUnitario.Value;
          wregistro54.ValorDescontoDespesa := qryItensDoctoFiscalnValDesconto.Value;
          wregistro54.BasedeCalculo        := qryItensDoctoFiscalnValBaseICMS.Value;
          wregistro54.BaseST               := qryItensDoctoFiscalnValBaseICMSSub.Value;
          wregistro54.ValorIpi             := qryItensDoctoFiscalnValIPI.Value;
          wregistro54.Aliquota             := qryItensDoctoFiscalnAliqICMS.Value;

          //Registro 75
          wregistro75.DataInicial  := qryItensDoctoFiscaldDtInicial.Value;
          wregistro75.DataFinal    := qryItensDoctoFiscaldDtFinal.Value;
          wregistro75.Codigo       := qryItensDoctoFiscalnCdProduto.AsString;
          wregistro75.NCM          := qryItensDoctoFiscalcNCM.Value;
          wregistro75.Descricao    := qryItensDoctoFiscalcNmItem.Value;
          wregistro75.Unidade      := qryItensDoctoFiscalcUnidadeMedida.Value;
          wregistro75.AliquotaIpi  := qryItensDoctoFiscalnAliqIPI.Value;
          wregistro75.AliquotaICMS := qryItensDoctoFiscalnAliqICMS.Value;
          wregistro75.Reducao      := qryItensDoctoFiscalnPercRedBCICMS.Value;
          wregistro75.BaseST       := qryItensDoctoFiscalnValBaseICMSSub.Value;

          ACBrSintegra1.Registros54.Add(wregistro54);
          ACBrSintegra1.Registros75.Add(wregistro75);

          qryItensDoctoFiscal.Next;

      end;

      {-- registros referentes ao cupom fiscal --}

      qryMovtoDiaECF.Close;
      qryMovtoDiaECF.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value;
      qryMovtoDiaECF.Parameters.ParamByName('nCdLoja').Value    := qryLojanCdLoja.Value;
      qryMovtoDiaECF.Parameters.ParamByName('dDtInicial').Value := edtDtInicial.Text;
      qryMovtoDiaECF.Parameters.ParamByName('dDtFinal').Value   := edtDtFinal.Text;
      qryMovtoDiaECF.Open;

      if (not qryMovtoDiaECF.IsEmpty) then
      begin

          qryMovtoDiaECF.First;

          {-- registro 60M --}
          while not qryMovtoDiaECF.Eof do
          begin

              wRegistro60M := TRegistro60M.Create;

              wRegistro60M.Emissao    := qryMovtoDiaECFdDtMovto.Value;
              wRegistro60M.NumSerie   := qryMovtoDiaECFcCdNumSerieECF.Value;
              wRegistro60M.NumOrdem   := qryMovtoDiaECFiNrECF.Value;
              wRegistro60M.ModeloDoc  := qryMovtoDiaECFcModelo.Value;
              wRegistro60M.CooInicial := qryMovtoDiaECFiNrOrdemOperInicial.Value;
              wRegistro60M.CooFinal   := qryMovtoDiaECFiNrOrdemOperFinal.Value;
              wRegistro60M.CRZ        := qryMovtoDiaECFiNrReducaoZ.Value;
              wRegistro60M.CRO        := qryMovtoDiaECFiNrReinicioOper.Value;
              wRegistro60M.VendaBruta := qryMovtoDiaECFnValVendaBruta.Value;
              wRegistro60M.ValorGT    := qryMovtoDiaECFnValTotalGeral.Value;

              ACBrSintegra1.Registros60M.Add(wRegistro60M);

              qryMovtoDiaECFAnalitico.Close;
              PosicionaQuery(qryMovtoDiaECFAnalitico,qryMovtoDiaECFnCdMovtoDiaECF.AsString);

              {-- registro 60A --}
              while not qryMovtoDiaECFAnalitico.Eof do
              begin

                  wRegistro60A := TRegistro60A.Create;

                  wRegistro60A.Emissao    := qryMovtoDiaECFdDtMovto.Value;
                  wRegistro60A.NumSerie   := qryMovtoDiaECFcCdNumSerieECF.Value;
                  wRegistro60A.StAliquota := qryMovtoDiaECFAnaliticocCdSTAliq.Value;
                  wRegistro60A.Valor      := qryMovtoDiaECFAnaliticonValAcumTotalParcial.Value;

                  ACBrSintegra1.Registros60A.Add(wRegistro60A);

                  qryMovtoDiaECFAnalitico.Next;

              end;

              qryTempMovtoECFItem.Close;
              PosicionaQuery(qryTempMovtoECFItem, qryMovtoDiaECFnCdMovtoDiaECF.AsString);

              {-- registro 60I --}
              while not qryTempMovtoECFItem.Eof do
              begin

                  wRegistro60I := TRegistro60I.Create;

                  wRegistro60I.Emissao       := qryTempMovtoECFItemdDtEmissao.Value;
                  wRegistro60I.NumSerie      := qryTempMovtoECFItemcCdNumSerieECF.Value;
                  wRegistro60I.ModeloDoc     := qryTempMovtoECFItemcModelo.Value;
                  wRegistro60I.Cupom         := qryTempMovtoECFItemiNrDocto.AsString;
                  wRegistro60I.Item          := qryTempMovtoECFItemiOrdemItem.Value;
                  wRegistro60I.Codigo        := qryTempMovtoECFItemnCdProduto.AsString;
                  wRegistro60I.Quantidade    := qryTempMovtoECFItemnQtde.Value;
                  wRegistro60I.Valor         := qryTempMovtoECFItemnValTotal.Value;
                  wRegistro60I.BaseDeCalculo := qryTempMovtoECFItemnValBaseICMS.Value;
                  wRegistro60I.StAliquota    := qryTempMovtoECFItemcCdSTAliq.Value;
                  wRegistro60I.ValorIcms     := qryTempMovtoECFItemnValICMS.Value;

                  ACBrSintegra1.Registros60I.Add(wRegistro60I);

                  qryTempMovtoECFItem.Next;
              end;

              qryMovtoDiaECF.Next;
          end;

      end;

      ACBrSintegra1.GeraArquivo;
      
  except
      MensagemErro('Erro ao gerar o arquivo Sintegra.');
      raise;
  end;

  ShowMessage('Arquivo Criado com Sucesso.');
  
end;

procedure TfrmGerarSintegra.FormShow(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      desativaMaskEdit(edtLoja);

  edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);
  edtEmpresa.PosicionaQuery;
      
end;

procedure TfrmGerarSintegra.edtEmpresaBeforePosicionaQry(Sender: TObject);
begin
  inherited;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

procedure TfrmGerarSintegra.edtLojaBeforePosicionaQry(Sender: TObject);
begin
  inherited;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

initialization
    RegisterClass(TfrmGerarSintegra);

end.
