inherited frmClienteVarejoPessoaFisica_TitulosNeg: TfrmClienteVarejoPessoaFisica_TitulosNeg
  Left = 66
  Top = 125
  Width = 908
  BorderIcons = [biSystemMenu]
  Caption = 'T'#237'tulos Negativados do Cliente'
  OldCreateOrder = True
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 892
  end
  inherited ToolBar1: TToolBar
    Width = 892
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 892
    Height = 433
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsTitulos
    Flat = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Consolas'
    Font.Style = []
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = []
    Options = [dgTitles, dgColumnResize, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -13
    TitleFont.Name = 'Consolas'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdTitulo'
        Footers = <>
        Width = 55
      end
      item
        EditButtons = <>
        FieldName = 'cNmEspTit'
        Footers = <>
        Width = 110
      end
      item
        EditButtons = <>
        FieldName = 'cNrTit'
        Footers = <>
        Width = 92
      end
      item
        EditButtons = <>
        FieldName = 'iParcela'
        Footers = <>
        Width = 43
      end
      item
        EditButtons = <>
        FieldName = 'dDtEmissao'
        Footers = <>
        Width = 101
      end
      item
        EditButtons = <>
        FieldName = 'dDtVenc'
        Footers = <>
        Width = 95
      end
      item
        EditButtons = <>
        FieldName = 'nValTit'
        Footers = <>
        Width = 104
      end
      item
        EditButtons = <>
        FieldName = 'nSaldoTit'
        Footers = <>
        Width = 108
      end
      item
        EditButtons = <>
        FieldName = 'dDtLiq'
        Footers = <>
        Width = 107
      end
      item
        EditButtons = <>
        FieldName = 'nCdLojaTit'
        Footers = <>
        Width = 51
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryTitulos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Titulo.nCdTitulo'
      '      ,Titulo.cNrTit'
      '      ,Titulo.iParcela'
      '      ,Titulo.dDtEmissao'
      '      ,Titulo.dDtVenc'
      '      ,Titulo.nValTit'
      '      ,Titulo.nSaldoTit'
      '      ,Titulo.dDtLiq'
      '      ,dbo.fn_ZeroEsquerda(Titulo.nCdLojaTit,3) nCdLojaTit'
      '      ,cNmEspTit'
      '  FROM Titulo'
      '       INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEspTit'
      
        '       LEFT  JOIN TituloNegativado ON Titulo.nCdTitulo = TituloN' +
        'egativado.nCdTitulo'
      ' WHERE Titulo.nCdTerceiro = :nPK'
      
        '   AND ISNULL(TituloNegativado.dDtNegativacao, Titulo.dDtNegativ' +
        'acao) IS NOT NULL'
      
        '   AND ISNULL(TituloNegativado.dDtReabNeg, Titulo.dDtReabNeg)   ' +
        '  IS NULL')
    Left = 296
    Top = 64
    object qryTitulosnCdTitulo: TIntegerField
      DisplayLabel = 'C'#243'd.'
      FieldName = 'nCdTitulo'
    end
    object qryTituloscNrTit: TStringField
      DisplayLabel = 'N'#250'm. T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosiParcela: TIntegerField
      DisplayLabel = 'Parc.'
      FieldName = 'iParcela'
    end
    object qryTitulosdDtEmissao: TDateTimeField
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryTitulosdDtVenc: TDateTimeField
      DisplayLabel = 'Dt. Vencto.'
      FieldName = 'dDtVenc'
    end
    object qryTitulosnValTit: TBCDField
      DisplayLabel = 'Valor Titulo'
      FieldName = 'nValTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosnSaldoTit: TBCDField
      DisplayLabel = 'Saldo T'#237'tulo'
      FieldName = 'nSaldoTit'
      Precision = 12
      Size = 2
    end
    object qryTitulosdDtLiq: TDateTimeField
      DisplayLabel = 'Dt. Pagamento'
      FieldName = 'dDtLiq'
    end
    object qryTituloscNmEspTit: TStringField
      DisplayLabel = 'Esp'#233'cie'
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTitulosnCdLojaTit: TStringField
      DisplayLabel = 'Loja'
      FieldName = 'nCdLojaTit'
      ReadOnly = True
      Size = 15
    end
  end
  object dsTitulos: TDataSource
    DataSet = qryTitulos
    Left = 328
    Top = 64
  end
end
