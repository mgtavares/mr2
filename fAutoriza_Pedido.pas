unit fAutoriza_Pedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, GridsEh, DBGridEh, StdCtrls, cxControls, cxContainer, cxEdit,
  cxTextEdit, DBGridEhGrouping, MemTableDataEh, MemTableEh, DataDriverEh,
  Menus, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData,
  cxDataStorage, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView, cxGrid,
  cxImage, cxImageComboBox;

type
  TfrmAutoriza_Pedido = class(TfrmProcesso_Padrao)
    qryPedido: TADOQuery;
    ToolButton4: TToolButton;
    dsPedido: TDataSource;
    qryPedidonCdPedido: TIntegerField;
    qryPedidodDtPedido: TDateTimeField;
    qryPedidocNmTipoPedido: TStringField;
    qryPedidocNmTerceiro: TStringField;
    qryPedidocNmCondPagto: TStringField;
    qryPedidonPercDesconto: TBCDField;
    qryPedidonPercAcrescimo: TBCDField;
    qryPedidonValPedido: TBCDField;
    qryPedidocNmTerceiro_1: TStringField;
    qryPedidonCdTabStatusPed: TIntegerField;
    qryPedidodDtAutor: TDateTimeField;
    qryPedidonCdUsuarioAutor: TIntegerField;
    ToolButton5: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    qryPedidocFlgVenda: TIntegerField;
    qryPedidocGerarFinanc: TIntegerField;
    qryAux: TADOQuery;
    qryPedidonCdTerceiroPagador: TIntegerField;
    qryPedidocAtuCredito: TIntegerField;
    qryPedidocCreditoLibMan: TIntegerField;
    qryPedidonCdTipoPedido: TIntegerField;
    SP_EMPENHA_ESTOQUE_PEDIDO: TADOStoredProc;
    ToolButton12: TToolButton;
    qryPedidocFlgLibParcial: TIntegerField;
    qryPedidocNmTerceiroEntrega: TStringField;
    qryPedidocFlgProdutoLiberado: TIntegerField;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label1: TLabel;
    qryPedidonCdTabTipoPedido: TIntegerField;
    SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO: TADOStoredProc;
    ToolButton14: TToolButton;
    qryPedidonCdLoja: TStringField;
    PopupMenu1: TPopupMenu;
    ExibirItensdoPedido1: TMenuItem;
    ImageList2: TImageList;
    qryPedidocFlgLiberadoParcial: TIntegerField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxIndicador: TcxGridDBColumn;
    cxGridDBTableView1nCdPedido: TcxGridDBColumn;
    cxGridDBTableView1nCdLoja: TcxGridDBColumn;
    cxGridDBTableView1dDtPedido: TcxGridDBColumn;
    cxGridDBTableView1cNmTipoPedido: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView1cNmCondPagto: TcxGridDBColumn;
    cxGridDBTableView1nPercDesconto: TcxGridDBColumn;
    cxGridDBTableView1nPercAcrescimo: TcxGridDBColumn;
    cxGridDBTableView1nValPedido: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    Image2: TImage;
    Image3: TImage;
    PopupMenu2: TPopupMenu;
    FollowUp1: TMenuItem;
    Restries1: TMenuItem;
    CarteiraFinanceira1: TMenuItem;
    cxGridDBTableView1cNmTerceiroEntrega: TcxGridDBColumn;
    qryPedidonCdEmpresa: TStringField;
    cxGridDBTableView1nCdEmpresa: TcxGridDBColumn;
    qryPedidocNmTabTipoPedido: TStringField;
    cxGridDBTableView1cNmTabTipoPedido: TcxGridDBColumn;
    qryPedidocCidadeEntrega: TStringField;
    qryPedidocNmGrupoTerceiro: TStringField;
    cxGridDBTableView1cCidadeEntrega: TcxGridDBColumn;
    cxGridDBTableView1cNmGrupoTerceiro: TcxGridDBColumn;
    ToolButton6: TToolButton;
    procedure FormShow(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure AutorizarPedido(nCdPedido:Integer);
    procedure ToolButton1Click(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure ToolButton9Click(Sender: TObject);
    procedure qryPedidoAfterScroll(DataSet: TDataSet);
    procedure btCarteiraClick(Sender: TObject);
    procedure ToolButton12Click(Sender: TObject);
    procedure ExibirItensdoPedido1Click(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
    procedure qryPedidoCalcFields(DataSet: TDataSet);
    procedure ToolButton16Click(Sender: TObject);
    procedure Restries1Click(Sender: TObject);
    procedure FollowUp1Click(Sender: TObject);
    procedure CarteiraFinanceira1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAutoriza_Pedido: TfrmAutoriza_Pedido;

implementation

uses fMenu, rPedidoCom_Simples, fSelecMotBloqPed, fCarteiraFinancAutorPed,
  fNovo_FollowUp, fItensPedido,fRestricoesPedido;

{$R *.dfm}

procedure TfrmAutoriza_Pedido.FormShow(Sender: TObject);
begin
  inherited;
  qryPedido.Close ;
  qryPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  {qryPedido.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;}
  qryPedido.Open  ;

  if(frmMenu.LeParametro('VAREJO') = 'N') then
      cxGridDBTableView1nCdLoja.Visible := False;

end;

procedure TfrmAutoriza_Pedido.ToolButton4Click(Sender: TObject);
begin

  if not qryPedido.active or (qryPedidonCdPedido.Value = 0) then
  begin
      MensagemAlerta('Nenhum pedido selecionado.') ;
      exit ;
  end ;

  case MessageDlg('Somente a montagem do pedido ser� autorizada. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo: exit ;
  end ;

  frmMenu.Connection.BeginTrans ;

  try
      qryPedido.Edit ;
      qryPedidocFlgLibParcial.Value  := 1 ;
      qryPedidodDtAutor.Value        := Now() ;
      qryPedidonCdUsuarioAutor.Value := frmMenu.nCdUsuarioLogado;
      qryPedido.Post ;
  except
      frmmenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      Raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  ShowMessage('Pedido liberado parcialmente.') ;
  
end;

procedure TfrmAutoriza_Pedido.AutorizarPedido(nCdPedido:Integer);
begin

    case MessageDlg('Confirma a autoriza��o deste pedido ?',mtConfirmation,[mbYes,mbNo],0) of
        mrNo: exit ;
    end ;

    frmMenu.Connection.BeginTrans ;

    try

        if (qryPedidocFlgLibParcial.Value = 1) and (qryPedidocFlgProdutoLiberado.Value = 1) then
        begin
            frmMenu.SP_GERA_ALERTA.Close;
            frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@nCdTipoAlerta').Value := 13 ;
            frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cAssunto').Value      := 'FATURAR PEDIDO' ;
            frmMenu.SP_GERA_ALERTA.Parameters.ParamByName('@cMensagem').Value     := 'O Pedido ' + qryPedidonCdPedido.AsString + ',  Terceiro: ' + qryPedidocNmTerceiroEntrega.Value + ' j� est� liberado para faturamento.' ;
            frmMenu.SP_GERA_ALERTA.ExecProc;
        end ;

        qryPedido.Edit ;
        qryPedidonCdTabStatusPed.Value := 3 ;
        qryPedidodDtAutor.Value        := Now() ;
        qryPedidonCdUsuarioAutor.Value := frmMenu.nCdUsuarioLogado;

        qryAux.Close ;
        qryAux.SQL.Add('SELECT cLimiteCredito FROM TipoPedido WHERE nCdTipoPedido = ' + qryPedidonCdTipoPedido.asString) ;
        qryAux.Open ;

        if not qryAux.eof and (qryAux.FieldList[0].Value = 1) then
        begin
            qryPedidocAtuCredito.Value    := 1 ;
            qryPedidocCreditoLibMan.Value := 1 ;
        end ;

        qryAux.Close ;

        qryPedido.Post ;

        if (qryPedidocGerarFinanc.Value = 1) and (qryPedidocFlgVenda.Value = 1) then
        begin

            qryAux.Close ;
            qryAux.SQL.Clear ;
            qryAux.SQL.Add('UPDATE Terceiro SET nValPedidoAberto = nValPedidoAberto + IsNull((SELECT nValPedido FROM Pedido WHERE nCdPedido = ' + qryPedidonCdPedido.AsString + '),0) WHERE nCdTerceiro = ' + qryPedidonCdTerceiroPagador.AsString) ;
            qryAux.ExecSQL;
            qryAux.Close;

        end ;

        // se o pedido � de saida e j� est� autorizado, empenha estoque.
        if (qryPedidonCdTabTipoPedido.Value = 1) then
        begin

            SP_EMPENHA_ESTOQUE_PEDIDO.Close ;
            SP_EMPENHA_ESTOQUE_PEDIDO.Parameters.ParamByName('@nCdPedido').Value := qryPedidonCdPedido.Value ;
            SP_EMPENHA_ESTOQUE_PEDIDO.ExecProc;

        end;

        // envia o pedido para as lojas
        SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO.Close;
        SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO.Parameters.ParamByName('@nCdPedido').Value := qryPedidonCdPedido.Value ;
        SP_ENVIA_SOMENTE_PEDIDO_TEMPREGISTRO.ExecProc;

    except
        frmmenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento.') ;
        raise ;
    end ;

    frmMenu.Connection.CommitTrans ;

    ShowMessage('Pedido Autorizado com Sucesso.') ;
    ToolButton1.Click;

end ;

procedure TfrmAutoriza_Pedido.ToolButton1Click(Sender: TObject);
begin
  inherited;
  qryPedido.Close ;
  qryPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  {qryPedido.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;}
  qryPedido.Open  ;
end;

procedure TfrmAutoriza_Pedido.ToolButton5Click(Sender: TObject);
var
  objRel  : TrptPedidoCom_Simples ;
begin

  if not qryPedido.active or (qryPedidonCdPedido.Value = 0) then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  objRel := TrptPedidoCom_Simples.Create(nil) ;

  try
      try
          PosicionaQuery(objRel.qryPedido           , qryPedidonCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemEstoque_Grade, qryPedidonCdPedido.asString) ;
          PosicionaQuery(objRel.qryItemAD           , qryPedidonCdPedido.asString) ;

          objRel.QRSubDetail1.Enabled := True ;
          objRel.QRSubDetail2.Enabled := True ;

          if (objRel.qryItemEstoque_Grade.eof) then
              objRel.QRSubDetail1.Enabled := False ;

          if (objRel.qryItemAD.eof) then
              objRel.QRSubDetail2.Enabled := False ;

          objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

          objRel.QuickRep1.PreviewModal;
      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end ;

end;

procedure TfrmAutoriza_Pedido.ToolButton9Click(Sender: TObject);
var
    objForm : TfrmSelecMotBloqPed ;
begin
  inherited;
  if not qryPedido.active or (qryPedidonCdPedido.Value = 0) then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  case MessageDlg('Confirma o bloqueio deste pedido ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo: exit ;
  end ;

  objForm := TfrmSelecMotBloqPed.Create(nil) ;
  
  objForm.nCdPedido := qryPedidonCdPedido.Value ;
  showForm(objForm , TRUE) ;

  qryPedido.Close ;
  qryPedido.Open ;

end;

procedure TfrmAutoriza_Pedido.qryPedidoAfterScroll(DataSet: TDataSet);
begin
  inherited;

  CarteiraFinanceira1.Enabled := False;

  if (qryPedidocFlgVenda.Value = 1) then
      CarteiraFinanceira1.Enabled := True ;

end;

procedure TfrmAutoriza_Pedido.btCarteiraClick(Sender: TObject);
var
  objForm : TfrmCarteiraFinancAutorPed ;
begin
  inherited;

  objForm := TfrmCarteiraFinancAutorPed.Create(nil) ;

  objForm.nCdTerceiro := qryPedidonCdTerceiroPagador.Value ;

  showForm(objForm , TRUE) ;
end;

procedure TfrmAutoriza_Pedido.ToolButton12Click(Sender: TObject);
begin
  inherited;

  if not qryPedido.active or (qryPedidonCdPedido.Value = 0) then
  begin
      MensagemAlerta('Nenhum pedido selecionado.') ;
      exit ;
  end ;

  if (qryPedidonCdPedido.Value > 0) then
  begin
      AutorizarPedido(qryPedidonCdPedido.Value) ;
  end ;

end;

procedure TfrmAutoriza_Pedido.ExibirItensdoPedido1Click(Sender: TObject);
var
  objForm : TfrmItensPedido;
begin
  inherited;

  if (qryPedido.RecordCount > 0) then
  begin
      objForm := TfrmItensPedido.Create(nil);

      objForm.qryItens.Close;
      objForm.qryItens.Parameters.ParamByName('nCdPedido').Value := qryPedidonCdPedido.Value;
      objForm.qryItens.Open;

      if (frmMenu.LeParametro('VAREJO') = 'N') then
          objForm.DBGridEh1.Columns.Items[8].Visible := False;

      showForm(objForm,true);
  end;

end;

procedure TfrmAutoriza_Pedido.cxGridDBTableView1DblClick(Sender: TObject);
begin
  inherited;
  if (qryPedidonCdPedido.Value > 0) then
  begin
      AutorizarPedido(qryPedidonCdPedido.Value) ;
  end ;
end;

procedure TfrmAutoriza_Pedido.qryPedidoCalcFields(DataSet: TDataSet);
begin
  inherited;

  {-- somente pedido de sa�das --}
  
  if (qryPedidocFlgLibParcial.Value = 1) and (qryPedidonCdTabTipoPedido.Value = 1) then
  begin

      {-- � um campo calculado que determina qual imagem ir� aparecer ao lado esquerdo do registro do pedido --}
      qryPedidocFlgLiberadoParcial.Value := qryPedidocFlgProdutoLiberado.Value ;

  end ;

end;

procedure TfrmAutoriza_Pedido.ToolButton16Click(Sender: TObject);
var
  objForm : TfrmRestricoesPedido ;
begin
  inherited;

  objForm := TfrmRestricoesPedido.Create(nil) ;

  objForm.qryRestricao.Close;
  objForm.PosicionaQuery(objForm.qryRestricao,qryPedidonCdPedido.AsString);

  showForm(objForm , TRUE) ;

end;

procedure TfrmAutoriza_Pedido.Restries1Click(Sender: TObject);
var
  objForm : TfrmRestricoesPedido ;
begin
  inherited;

  objForm := TfrmRestricoesPedido.Create(nil) ;

  objForm.qryRestricao.Close;
  objForm.PosicionaQuery(objForm.qryRestricao,qryPedidonCdPedido.AsString);

  showForm(objForm , TRUE) ;
end;

procedure TfrmAutoriza_Pedido.FollowUp1Click(Sender: TObject);
var
    objForm : TfrmNovo_FollowUp ;
begin
  inherited;
  if not qryPedido.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  objForm := TfrmNovo_FollowUp.Create(nil) ;

  objForm.qryFollow.Close ;
  objForm.qryFollow.Open ;
  objForm.qryFollow.Insert ;
  objForm.qryFollownCdPedido.Value   := qryPedidonCdPedido.Value ;
  objForm.qryFollownCdUsuario.Value  := frmMenu.nCdUsuarioLogado ;
  objForm.qryFollowdDtFollowUp.Value := Now() ;

  showForm( objForm , TRUE ) ;
end;

procedure TfrmAutoriza_Pedido.CarteiraFinanceira1Click(Sender: TObject);
var
  objForm : TfrmCarteiraFinancAutorPed ;
begin
  inherited;

  objForm := TfrmCarteiraFinancAutorPed.Create(nil) ;

  objForm.nCdTerceiro := qryPedidonCdTerceiroPagador.Value ;

  showForm(objForm , TRUE) ;
end;

initialization
    RegisterClass(tFrmAutoriza_Pedido) ;

end.
