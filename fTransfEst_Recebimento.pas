unit fTransfEst_Recebimento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  GridsEh, DBGridEh, ADODB, Menus, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmTransfEst_Recebimento = class(TfrmProcesso_Padrao)
    qryMaster: TADOQuery;
    DBGridEh1: TDBGridEh;
    dsMaster: TDataSource;
    qryMasternCdTransfEst: TIntegerField;
    qryMastercNmLojaOrigem: TStringField;
    qryMastercNmLojaDestino: TStringField;
    qryMastercNmTabTipoTransf: TStringField;
    qryMastercNmLocalOrigem: TStringField;
    qryMastercNmLocalDestino: TStringField;
    qryMasterdDtFinalizacao: TDateTimeField;
    PopupMenu1: TPopupMenu;
    ExibirItens1: TMenuItem;
    ConfirmarRecebimento1: TMenuItem;
    qryMasternCdLojaOrigem: TIntegerField;
    qryMasternCdLojaDestino: TIntegerField;
    qryMasternCdTabTipoTransf: TIntegerField;
    qryMasternCdLocalEstoqueOrigem: TIntegerField;
    qryMasternCdLocalEstoqueDestino: TIntegerField;
    qryMastercFlgModoManual: TIntegerField;
    SP_PROCESSA_RECEBIMENTO_TRANSFERENCIA: TADOStoredProc;
    qryMastercModo: TStringField;
    qryItem: TADOQuery;
    qryItemnCdProduto: TIntegerField;
    qryItemcNmItem: TStringField;
    qryItemnQtde: TBCDField;
    qryItemnValUnitario: TBCDField;
    qryItemnValTotal: TBCDField;
    qryItemnCdTransfEst: TIntegerField;
    qryItemnCdItemTransfEst: TAutoIncField;
    qryItemnQtdeConf: TBCDField;
    qryItemnQtdeDiv: TFloatField;
    dsItem: TDataSource;
    procedure ToolButton1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ExibirItens1Click(Sender: TObject);
    procedure ConfirmarRecebimento1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTransfEst_Recebimento: TfrmTransfEst_Recebimento;

implementation

uses fMenu, fTransfEst_Recebimento_Itens, fTransfEst_Recebimento_Processo
    ,fTransfEst_Recebimento_LoteSerial;

{$R *.dfm}

procedure TfrmTransfEst_Recebimento.ToolButton1Click(Sender: TObject);
begin
  inherited;

  PosicionaQuery(qryMaster, IntToStr(frmMenu.nCdUsuarioLogado)) ;

  if (qryMaster.eof) then
  begin
      MensagemAlerta('Nenhuma transfer�ncia pendente ou nenhum estoque que voc� possa movimentar.') ;
  end ;
  
end;

procedure TfrmTransfEst_Recebimento.FormActivate(Sender: TObject);
begin
  inherited;
  ToolButton1.Click;
end;

procedure TfrmTransfEst_Recebimento.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active or qryMaster.eof then
  Begin
      MensagemAlerta('Nenhuma transfer�ncia selecionada.') ;
      abort ;
  End ;

end;

procedure TfrmTransfEst_Recebimento.ExibirItens1Click(Sender: TObject);
var
  objForm : TfrmTransfEst_Recebimento_Itens;
begin
  inherited;

  objForm := TfrmTransfEst_Recebimento_Itens.Create(nil);

  PosicionaQuery(objForm.qryItens, qryMasternCdTransfEst.AsString) ;
  showForm(objForm,true);

end;

procedure TfrmTransfEst_Recebimento.ConfirmarRecebimento1Click(
  Sender: TObject);
var
  objForm        : TfrmTransfEst_Recebimento_Processo;
  objFormGerLote : TfrmTransfEst_Recebimento_LoteSerial;

begin
  inherited;

  if (qryMastercFlgModoManual.Value = 0) then {-- via leitor de c�digo de barras --}
  begin
      objForm := TfrmTransfEst_Recebimento_Processo.Create(nil);

      objForm.nCdTransfEst := qryMasternCdTransfEst.Value ;

      objForm.qryPrepara_Temp.Close ;
      objForm.qryPrepara_Temp.ExecSQL;

      PosicionaQuery(objForm.qryItens, qryMasternCdTransfEst.asString) ;
      PosicionaQuery(objForm.qryItensEAN, qryMasternCdTransfEst.asString) ;

      objForm.qryResumo.Close ;
      objForm.qryResumo.Open ;

      showForm(objForm,true) ;
  end
  else
  begin {-- manual --}

      objFormGerLote := TfrmTransfEst_Recebimento_LoteSerial.Create(nil);

      objFormGerLote.ItemLoteSerial(qryMasternCdTransfEst.Value
                                   ,5
                                   ,qryMasternCdLocalEstoqueDestino.Value
                                   ,True
                                   ,False
                                   ,True );

      if (not objFormGerLote.bLoteProcessado) then
          abort;

      if (MessageDlg('Confirma o recebimento dos itens desta transfer�ncia ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      frmMenu.Connection.BeginTrans;

      try
          SP_PROCESSA_RECEBIMENTO_TRANSFERENCIA.Parameters.ParamByName('@nCdTransfEst').Value := qryMasternCdTransfEst.Value ;
          SP_PROCESSA_RECEBIMENTO_TRANSFERENCIA.Parameters.ParamByName('@nCdUsuario').Value   := frmMenu.nCdUsuarioLogado;
          SP_PROCESSA_RECEBIMENTO_TRANSFERENCIA.ExecProc;
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Os itens foram creditados no estoque.') ;

  end ;

  qryMaster.Close ;
  qryMaster.Open ;

  if (qryMaster.eof) then
  begin
      MensagemAlerta('Nenhuma transfer�ncia pendente ou nenhum estoque que voc� possa movimentar.') ;
  end ;

end;

initialization
    RegisterClass(TfrmTransfEst_Recebimento) ;

end.
