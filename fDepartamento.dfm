inherited frmDepartamento: TfrmDepartamento
  Left = 147
  Top = 18
  Width = 1168
  Height = 682
  Caption = 'Departamento'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1152
    Height = 619
  end
  object Label1: TLabel [1]
    Left = 48
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 12
    Top = 70
    Width = 74
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 54
    Top = 118
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 8
    Top = 96
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Produto'
    FocusControl = DBEdit5
  end
  object Label7: TLabel [5]
    Left = 168
    Top = 45
    Width = 107
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo Externo Web'
  end
  inherited ToolBar2: TToolBar
    Width = 1152
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 91
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdDepartamento'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [8]
    Left = 91
    Top = 64
    Width = 645
    Height = 19
    DataField = 'cNmDepartamento'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [9]
    Left = 91
    Top = 112
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 5
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [10]
    Tag = 1
    Left = 159
    Top = 112
    Width = 169
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBGridEh1: TDBGridEh [11]
    Left = 8
    Top = 160
    Width = 545
    Height = 353
    DataGrouping.GroupLevels = <>
    DataSource = dsCategoria
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 7
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    OnEnter = DBGridEh1Enter
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdCategoria'
        Footers = <>
        ReadOnly = True
      end
      item
        EditButtons = <>
        FieldName = 'cNmCategoria'
        Footers = <>
        Width = 302
      end
      item
        EditButtons = <>
        FieldName = 'nCdStatus'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdIdExternoWeb'
        Footers = <>
        Title.Caption = 'C'#243'd. Externo Web'
        Width = 65
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StaticText2: TStaticText [12]
    Left = 8
    Top = 144
    Width = 545
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Categorias'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 8
  end
  object DBGridEh2: TDBGridEh [13]
    Left = 632
    Top = 160
    Width = 505
    Height = 193
    DataGrouping.GroupLevels = <>
    DataSource = dsSubCategoria
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 9
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    Visible = False
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdSubCategoria'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdCategoria'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cNmSubCategoria'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdStatus'
        Footers = <>
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StaticText1: TStaticText [14]
    Left = 632
    Top = 144
    Width = 505
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Categoria x Sub Categoria'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Visible = False
  end
  object DBGridEh3: TDBGridEh [15]
    Left = 632
    Top = 296
    Width = 505
    Height = 169
    DataGrouping.GroupLevels = <>
    DataSource = dsSegmento
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 11
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    Visible = False
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdSegmento'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdSubCategoria'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cNmSegmento'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdStatus'
        Footers = <>
        Width = 88
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StaticText3: TStaticText [16]
    Left = 632
    Top = 280
    Width = 505
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Sub Categoria x Segmento'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 12
    Visible = False
  end
  object DBGridEh4: TDBGridEh [17]
    Left = 632
    Top = 464
    Width = 505
    Height = 169
    DataGrouping.GroupLevels = <>
    DataSource = dsMarcaSub
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 13
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    Visible = False
    OnKeyUp = DBGridEh4KeyUp
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdMarcaSubCategoria'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'nCdMarca'
        Footers = <>
        Width = 62
      end
      item
        EditButtons = <>
        FieldName = 'cNmMarca'
        Footers = <>
        Width = 374
      end
      item
        EditButtons = <>
        FieldName = 'nCdSubCategoria'
        Footers = <>
        Visible = False
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object StaticText4: TStaticText [18]
    Left = 632
    Top = 448
    Width = 505
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSingle
    Caption = 'Sub Categoria x Marca'
    Color = clMoneyGreen
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 14
    Visible = False
  end
  object DBEdit5: TDBEdit [19]
    Left = 91
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdGrupoProduto'
    DataSource = dsMaster
    TabOrder = 3
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [20]
    Tag = 1
    Left = 159
    Top = 88
    Width = 577
    Height = 19
    DataField = 'cNmGrupoProduto'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit7: TDBEdit [21]
    Left = 283
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdIdExternoWeb'
    DataSource = dsMaster
    TabOrder = 15
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM DEPARTAMENTO'
      'WHERE nCdDepartamento = :nPK')
    Left = 376
    Top = 376
    object qryMasternCdDepartamento: TAutoIncField
      FieldName = 'nCdDepartamento'
    end
    object qryMastercNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMastercNmStatus: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmStatus'
      LookupDataSet = qryStat
      LookupKeyFields = 'nCdStatus'
      LookupResultField = 'cNmStatus'
      KeyFields = 'nCdStatus'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMasternCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryMastercNmGrupoProduto: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmGrupoProduto'
      LookupDataSet = qryGrupoProduto
      LookupKeyFields = 'nCdGrupoProduto'
      LookupResultField = 'cNmGrupoProduto'
      KeyFields = 'nCdGrupoProduto'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMasternCdIdExternoWeb: TIntegerField
      FieldName = 'nCdIdExternoWeb'
    end
  end
  inherited dsMaster: TDataSource
    Left = 376
    Top = 408
  end
  inherited qryID: TADOQuery
    Left = 568
    Top = 376
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 600
    Top = 408
  end
  inherited qryStat: TADOQuery
    Left = 536
    Top = 408
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 600
    Top = 376
  end
  inherited ImageList1: TImageList
    Left = 632
    Top = 376
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    AfterClose = qryCategoriaAfterClose
    BeforePost = qryCategoriaBeforePost
    AfterScroll = qryCategoriaAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '   FROM Categoria'
      '  WHERE nCdDepartamento = :nPK')
    Left = 408
    Top = 376
    object qryCategorianCdCategoria: TAutoIncField
      DisplayLabel = 'Categoria|C'#243'd'
      FieldName = 'nCdCategoria'
    end
    object qryCategorianCdDepartamento: TIntegerField
      DisplayLabel = 'Categoria|nCdDepartamento'
      FieldName = 'nCdDepartamento'
    end
    object qryCategoriacNmCategoria: TStringField
      DisplayLabel = 'Categoria|Descri'#231#227'o'
      FieldName = 'cNmCategoria'
      Size = 50
    end
    object qryCategorianCdStatus: TIntegerField
      DisplayLabel = 'Categoria|Status'
      FieldName = 'nCdStatus'
    end
    object qryCategorianCdIdExternoWeb: TIntegerField
      FieldName = 'nCdIdExternoWeb'
    end
  end
  object dsCategoria: TDataSource
    DataSet = qryCategoria
    Left = 408
    Top = 408
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    AfterClose = qrySubCategoriaAfterClose
    BeforePost = qrySubCategoriaBeforePost
    AfterScroll = qrySubCategoriaAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM SUBCATEGORIA'
      'WHERE nCdCategoria = :nPK')
    Left = 440
    Top = 376
    object qrySubCategorianCdSubCategoria: TAutoIncField
      DisplayLabel = 'Sub Categoria|C'#243'd'
      FieldName = 'nCdSubCategoria'
      ReadOnly = True
    end
    object qrySubCategorianCdCategoria: TIntegerField
      DisplayLabel = 'Sub Categoria|Categoria'
      FieldName = 'nCdCategoria'
    end
    object qrySubCategoriacNmSubCategoria: TStringField
      DisplayLabel = 'Sub Categoria|Descri'#231#227'o'
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object qrySubCategorianCdStatus: TIntegerField
      DisplayLabel = 'Sub Categoria|Status'
      FieldName = 'nCdStatus'
    end
  end
  object dsSubCategoria: TDataSource
    DataSet = qrySubCategoria
    Left = 440
    Top = 408
  end
  object qrySegmento: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qrySegmentoBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM SEGMENTO'
      'WHERE nCdSubCategoria = :nPK')
    Left = 472
    Top = 376
    object qrySegmentonCdSegmento: TAutoIncField
      DisplayLabel = 'Segmento|C'#243'd'
      FieldName = 'nCdSegmento'
      ReadOnly = True
    end
    object qrySegmentonCdSubCategoria: TIntegerField
      DisplayLabel = 'Segmento|SubCategoria'
      FieldName = 'nCdSubCategoria'
    end
    object qrySegmentocNmSegmento: TStringField
      DisplayLabel = 'Segmento|Descri'#231#227'o'
      FieldName = 'cNmSegmento'
      Size = 50
    end
    object qrySegmentonCdStatus: TIntegerField
      DisplayLabel = 'Segmento|Status'
      FieldName = 'nCdStatus'
    end
  end
  object dsSegmento: TDataSource
    DataSet = qrySegmento
    Left = 472
    Top = 408
  end
  object qryMarcaSub: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryMarcaSubBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarcaSubCategoria'
      '      ,MarcaSubCategoria.nCdMarca'
      '      ,MarcaSubCategoria.nCdSubCategoria'
      '  FROM MarcaSubCategoria'
      ' WHERE nCdSubCategoria = :nPK')
    Left = 504
    Top = 376
    object qryMarcaSubnCdMarcaSubCategoria: TAutoIncField
      DisplayLabel = 'Marca|C'#243'd'
      FieldName = 'nCdMarcaSubCategoria'
      ReadOnly = True
    end
    object qryMarcaSubnCdMarca: TIntegerField
      DisplayLabel = 'Marca|C'#243'd'
      FieldName = 'nCdMarca'
    end
    object qryMarcaSubnCdSubCategoria: TIntegerField
      DisplayLabel = 'Marca|C'#243'd'
      FieldName = 'nCdSubCategoria'
    end
    object qryMarcaSubcNmMarca: TStringField
      DisplayLabel = 'Marca|Descri'#231#227'o'
      FieldKind = fkLookup
      FieldName = 'cNmMarca'
      LookupDataSet = qryMarca
      LookupKeyFields = 'nCdMarca'
      LookupResultField = 'cNmMarca'
      KeyFields = 'nCdMarca'
      LookupCache = True
      ReadOnly = True
      Size = 50
      Lookup = True
    end
  end
  object dsMarcaSub: TDataSource
    DataSet = qryMarcaSub
    Left = 504
    Top = 408
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdMarca'
      ',cNmMarca'
      'FROM Marca')
    Left = 536
    Top = 376
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      'FROM GRUPOPRODUTO')
    Left = 568
    Top = 408
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
    object qryGrupoProdutocFlgExpPDV: TIntegerField
      FieldName = 'cFlgExpPDV'
    end
  end
end
