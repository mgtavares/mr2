inherited frmTabIBPT: TfrmTabIBPT
  Left = 230
  Top = 207
  Width = 893
  Height = 337
  Caption = 'Tabela IBPT'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 877
    Height = 274
  end
  object Label1: TLabel [1]
    Left = 85
    Top = 48
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
  end
  object Label2: TLabel [2]
    Left = 536
    Top = 71
    Width = 25
    Height = 13
    Alignment = taRightJustify
    Caption = 'NCM'
    FocusControl = DBEdit2
  end
  object Label4: TLabel [3]
    Left = 31
    Top = 120
    Width = 92
    Height = 13
    Alignment = taRightJustify
    Caption = 'Al'#237'quota Nacional'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [4]
    Left = 213
    Top = 119
    Width = 100
    Height = 13
    Alignment = taRightJustify
    Caption = 'Al'#237'quota Importada'
    FocusControl = DBEdit5
  end
  object Label3: TLabel [5]
    Left = 76
    Top = 192
    Width = 45
    Height = 13
    Alignment = taRightJustify
    Caption = 'Validade'
  end
  object Label6: TLabel [6]
    Left = 216
    Top = 191
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label7: TLabel [7]
    Left = 31
    Top = 142
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Al'#237'quota Estadual'
    FocusControl = DBEdit8
  end
  object Label8: TLabel [8]
    Left = 215
    Top = 143
    Width = 98
    Height = 13
    Alignment = taRightJustify
    Caption = 'Al'#237'quota Municipal'
    FocusControl = DBEdit9
  end
  object Label9: TLabel [9]
    Left = 87
    Top = 167
    Width = 35
    Height = 13
    Caption = 'Vers'#227'o'
    FocusControl = DBEdit10
  end
  object Label10: TLabel [10]
    Left = 281
    Top = 167
    Width = 31
    Height = 13
    Caption = 'Chave'
    FocusControl = DBEdit11
  end
  object Label11: TLabel [11]
    Left = 73
    Top = 71
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit4
  end
  object Label12: TLabel [12]
    Left = 87
    Top = 95
    Width = 35
    Height = 13
    Alignment = taRightJustify
    Caption = 'Estado'
  end
  inherited ToolBar2: TToolBar
    Width = 877
    TabOrder = 14
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit2: TDBEdit [14]
    Left = 566
    Top = 66
    Width = 65
    Height = 19
    DataField = 'cNCM'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [15]
    Left = 127
    Top = 66
    Width = 397
    Height = 19
    DataField = 'cDescricao'
    DataSource = dsMaster
    MaxLength = 100
    TabOrder = 1
  end
  object DBEdit4: TDBEdit [16]
    Left = 127
    Top = 114
    Width = 65
    Height = 19
    DataField = 'nAliqNacional'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit5: TDBEdit [17]
    Left = 317
    Top = 114
    Width = 65
    Height = 19
    DataField = 'nAliqImportado'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit1: TDBEdit [18]
    Tag = 1
    Left = 127
    Top = 42
    Width = 65
    Height = 19
    DataField = 'nCdTabIBPT'
    DataSource = dsMaster
    TabOrder = 0
  end
  object DBRadioGroup1: TDBRadioGroup [19]
    Left = 127
    Top = 214
    Width = 194
    Height = 44
    Hint = '0 - Mercadoria / 1 - Servi'#231'o'
    Caption = '  Classifica'#231#227'o '
    Columns = 2
    DataField = 'iTabela'
    DataSource = dsMaster
    Items.Strings = (
      'Mercadoria'
      'Servi'#231'o')
    ParentShowHint = False
    ShowHint = True
    TabOrder = 13
    Values.Strings = (
      '0'
      '1')
  end
  object DBEdit8: TDBEdit [20]
    Left = 127
    Top = 138
    Width = 65
    Height = 19
    DataField = 'nAliqEstadual'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit9: TDBEdit [21]
    Left = 317
    Top = 138
    Width = 65
    Height = 19
    DataField = 'nAliqMunicipal'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit10: TDBEdit [22]
    Tag = 1
    Left = 127
    Top = 162
    Width = 65
    Height = 19
    DataField = 'cVersao'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit11: TDBEdit [23]
    Tag = 1
    Left = 317
    Top = 162
    Width = 65
    Height = 19
    DataField = 'cChave'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit12: TDBEdit [24]
    Tag = 1
    Left = 195
    Top = 90
    Width = 436
    Height = 19
    DataField = 'cNmEstado'
    DataSource = dsEstado
    TabOrder = 4
  end
  object DBEdit6: TDBEdit [25]
    Tag = 1
    Left = 127
    Top = 186
    Width = 77
    Height = 19
    DataField = 'dDtValidInicial'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit7: TDBEdit [26]
    Tag = 1
    Left = 243
    Top = 186
    Width = 77
    Height = 19
    DataField = 'dDtValidFinal'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit13: TDBEdit [27]
    Left = 127
    Top = 90
    Width = 65
    Height = 19
    DataField = 'nCdEstado'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit13Exit
    OnKeyDown = DBEdit13KeyDown
  end
  object DBRadioGroup2: TDBRadioGroup [28]
    Left = 328
    Top = 214
    Width = 185
    Height = 44
    Caption = ' Modo de Inclus'#227'o '
    Columns = 2
    DataField = 'cFlgManual'
    DataSource = dsMaster
    Items.Strings = (
      'Importado'
      'Manual')
    ReadOnly = True
    TabOrder = 15
    Values.Strings = (
      '0'
      '1')
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT *'
      '   FROM TabIBPT'
      'WHERE nCdTabIBPT = :nPK')
    Left = 560
    Top = 120
    object qryMasternCdTabIBPT: TIntegerField
      FieldName = 'nCdTabIBPT'
    end
    object qryMastercNCM: TStringField
      FieldName = 'cNCM'
      Size = 8
    end
    object qryMasteriTabela: TIntegerField
      FieldName = 'iTabela'
    end
    object qryMastercDescricao: TStringField
      FieldName = 'cDescricao'
      Size = 1000
    end
    object qryMasternAliqNacional: TBCDField
      FieldName = 'nAliqNacional'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternAliqImportado: TBCDField
      FieldName = 'nAliqImportado'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgManual: TIntegerField
      FieldName = 'cFlgManual'
    end
    object qryMasternAliqEstadual: TBCDField
      FieldName = 'nAliqEstadual'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternAliqMunicipal: TBCDField
      FieldName = 'nAliqMunicipal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercVersao: TStringField
      FieldName = 'cVersao'
    end
    object qryMasterdDtValidInicial: TDateTimeField
      FieldName = 'dDtValidInicial'
    end
    object qryMasterdDtValidFinal: TDateTimeField
      FieldName = 'dDtValidFinal'
    end
    object qryMastercChave: TStringField
      FieldName = 'cChave'
    end
    object qryMasternCdEstado: TIntegerField
      FieldName = 'nCdEstado'
    end
  end
  inherited dsMaster: TDataSource
    Left = 560
    Top = 152
  end
  inherited qryID: TADOQuery
    Left = 592
    Top = 120
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 624
    Top = 120
  end
  inherited qryStat: TADOQuery
    Left = 592
    Top = 152
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 624
    Top = 152
  end
  inherited ImageList1: TImageList
    Left = 496
    Top = 152
  end
  object qryEstado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEstado ,cUF '
      '      ,cNmEstado '
      '  FROM Estado '
      ' WHERE nCdEstado = :nPK')
    Left = 528
    Top = 120
    object qryEstadocUF: TStringField
      FieldName = 'cUF'
      FixedChar = True
      Size = 2
    end
    object qryEstadocNmEstado: TStringField
      FieldName = 'cNmEstado'
      Size = 50
    end
    object qryEstadonCdEstado: TIntegerField
      FieldName = 'nCdEstado'
    end
  end
  object dsEstado: TDataSource
    DataSet = qryEstado
    Left = 528
    Top = 152
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @dDtInicial datetime'
      '       ,@dDtFinal   datetime'
      '       ,@cVersao    varchar(20)'
      '       ,@cChave     varchar(20)'
      '       '
      'SELECT TOP 1'
      '       @dDtInicial = dDtValidInicial'
      '      ,@dDtFinal   = dDtValidFinal'
      '      ,@cVersao    = cVersao'
      '      ,@cChave     = cChave   '
      '  FROM TabIBPT'
      ' WHERE dDtValidInicial IS NOT NULL'
      '   AND dDtValidFinal   IS NOT NULL'
      '   AND cVersao         IS NOT NULL'
      '   AND cChave          IS NOT NULL        '
      ''
      'UPDATE TabIBPT '
      '   SET dDtValidInicial = @dDtInicial'
      '      ,dDtValidFinal   = @dDtFinal  '
      '      ,cVersao         = @cVersao'
      '      ,cChave          = @cChave'
      ' WHERE nCdTabIBPT = :nPK'
      '')
    Left = 496
    Top = 120
  end
end
