inherited frmServidorSAT: TfrmServidorSAT
  Left = 213
  Top = 183
  Width = 1075
  Height = 369
  Caption = 'Servidores SAT'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1059
    Height = 302
  end
  inherited ToolBar1: TToolBar
    Width = 1059
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1059
    Height = 302
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsServidorSAT
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdServidorSAT'
        Footers = <>
        Title.Caption = 'Servidores SAT Compartilhado|ID'
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'nCdEmpresa'
        Footers = <>
        Title.Caption = 'Servidores SAT Compartilhado|Empresa|C'#243'd.'
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cNmEmpresa'
        Footers = <>
        ReadOnly = True
        Title.Caption = 'Servidores SAT Compartilhado|Empresa|Descri'#231#227'o'
        Width = 150
      end
      item
        EditButtons = <>
        FieldName = 'nCdLoja'
        Footers = <>
        Title.Caption = 'Servidores SAT Compartilhado|Loja|C'#243'd.'
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cNmLoja'
        Footers = <>
        ReadOnly = True
        Title.Caption = 'Servidores SAT Compartilhado|Loja|Descri'#231#227'o'
        Width = 150
      end
      item
        EditButtons = <>
        FieldName = 'cNmServidorSAT'
        Footers = <>
        Title.Caption = 'Servidores SAT Compartilhado|SAT|Descri'#231#227'o Servidor'
        Width = 150
      end
      item
        Checkboxes = True
        EditButtons = <>
        FieldName = 'nCdStatus'
        Footers = <>
        KeyList.Strings = (
          '1'
          '2')
        Title.Caption = 'Servidores SAT Compartilhado|SAT|Status'
        Width = 65
      end
      item
        EditButtons = <>
        FieldName = 'cIPPrimario'
        Footers = <>
        Title.Caption = 'Servidores SAT Compartilhado|Dados de Conex'#227'o|Endere'#231'o IP'
        Width = 150
      end
      item
        EditButtons = <>
        FieldName = 'cNmDatabase'
        Footers = <>
        Title.Caption = 'Servidores SAT Compartilhado|Dados de Conex'#227'o|Database'
        Width = 150
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  inherited ImageList1: TImageList
    Left = 360
    Top = 160
  end
  object qryServidorSAT: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryServidorSATBeforePost
    OnCalcFields = qryServidorSATCalcFields
    Parameters = <>
    SQL.Strings = (
      'SELECT * '
      '  FROM ServidorSAT'
      ' ORDER BY nCdEmpresa'
      '         ,nCdLoja'
      '         ,cNmServidorSAT')
    Left = 392
    Top = 160
    object qryServidorSATnCdServidorSAT: TIntegerField
      FieldName = 'nCdServidorSAT'
    end
    object qryServidorSATcNmServidorSAT: TStringField
      FieldName = 'cNmServidorSAT'
      Size = 100
    end
    object qryServidorSATnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryServidorSATcNmStatus: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmStatus'
      Size = 50
      Calculated = True
    end
    object qryServidorSATdDtCadastro: TDateTimeField
      FieldName = 'dDtCadastro'
    end
    object qryServidorSATnCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryServidorSATcNmEmpresa: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmEmpresa'
      Size = 50
      Calculated = True
    end
    object qryServidorSATnCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryServidorSATcNmLoja: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmLoja'
      Size = 50
      Calculated = True
    end
    object qryServidorSATcIPPrimario: TStringField
      FieldName = 'cIPPrimario'
      Size = 50
    end
    object qryServidorSATcNmDatabase: TStringField
      FieldName = 'cNmDatabase'
      Size = 50
    end
  end
  object dsServidorSAT: TDataSource
    DataSet = qryServidorSAT
    Left = 392
    Top = 192
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Empresa'
      ' WHERE nCdEmpresa = :nPK'
      '')
    Left = 424
    Top = 160
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM Loja'
      ' WHERE nCdLoja    = :nPK'
      '   AND nCdEmpresa = :nCdEmpresa'
      '')
    Left = 424
    Top = 192
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
end
