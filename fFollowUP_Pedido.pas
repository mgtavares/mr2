unit fFollowUP_Pedido;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ImgList, ComCtrls, ToolWin, ExtCtrls,
  GridsEh, DBGridEh, StdCtrls, DB, ADODB, cxControls, cxContainer, cxEdit,
  cxTextEdit, fProcesso_Padrao, DBCtrls, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSCore, dxPScxCommon, dxPScxGridLnk, Menus, ER2Excel,
  dxPSContainerLnk;

type
  TfrmFollowUP_Pedido = class(TfrmProcesso_Padrao)
    usp_Processo: TADOStoredProc;
    dsUsp_Processo: TDataSource;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    qryAux: TADOQuery;
    qryFollow: TADOQuery;
    dsFollow: TDataSource;
    qryFollowdDtFollowUp: TDateTimeField;
    qryFollowcNmUsuario: TStringField;
    qryFollowcOcorrenciaResum: TStringField;
    qryFollowdDtProxAcao: TDateTimeField;
    qryFollowcOcorrencia: TMemoField;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    GroupBox1: TGroupBox;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    GroupBox2: TGroupBox;
    cxGrid4: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    GroupBox3: TGroupBox;
    cxGrid1: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1dDtFollowUp: TcxGridDBColumn;
    cxGridDBTableView1cNmUsuario: TcxGridDBColumn;
    cxGridDBTableView1cOcorrenciaResum: TcxGridDBColumn;
    cxGridDBTableView1dDtProxAcao: TcxGridDBColumn;
    cxGridDBTableView1cOcorrencia: TcxGridDBColumn;
    DBMemo1: TDBMemo;
    usp_ProcessocMesAnoEntrega: TStringField;
    usp_ProcessonCdPedido: TIntegerField;
    usp_ProcessocNmTipoPedido: TStringField;
    usp_ProcessocNmSituacao: TStringField;
    usp_ProcessonCdLoja: TStringField;
    usp_ProcessonCdTerceiro: TIntegerField;
    usp_ProcessocNmTerceiro: TStringField;
    usp_ProcessocNrPedTerceiro: TStringField;
    usp_ProcessocNmContato: TStringField;
    usp_ProcessocUltimoAcomp: TStringField;
    usp_ProcessocNmMarca: TStringField;
    usp_ProcessonValTotalItem: TBCDField;
    usp_ProcessonSaldoFat: TBCDField;
    cxGridDBTableView3cMesAnoEntrega: TcxGridDBColumn;
    cxGridDBTableView3nCdPedido: TcxGridDBColumn;
    cxGridDBTableView3cNmTipoPedido: TcxGridDBColumn;
    cxGridDBTableView3cNmSituacao: TcxGridDBColumn;
    cxGridDBTableView3nCdLoja: TcxGridDBColumn;
    cxGridDBTableView3nCdTerceiro: TcxGridDBColumn;
    cxGridDBTableView3cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView3cNrPedTerceiro: TcxGridDBColumn;
    cxGridDBTableView3cNmContato: TcxGridDBColumn;
    cxGridDBTableView3cUltimoAcomp: TcxGridDBColumn;
    cxGridDBTableView3cNmMarca: TcxGridDBColumn;
    cxGridDBTableView3nValTotalItem: TcxGridDBColumn;
    cxGridDBTableView3nSaldoFat: TcxGridDBColumn;
    dxComponentPrinter1: TdxComponentPrinter;
    usp_ProcessocNmTerceiroRepres: TStringField;
    cxGridDBTableView3DBColumn1: TcxGridDBColumn;
    qryBuscaPedido: TADOQuery;
    RadioGroup3: TRadioGroup;
    usp_ProcessocNmDepartamento: TStringField;
    usp_ProcessocNmCategoria: TStringField;
    cxGridDBTableView3cNmDepartamento: TcxGridDBColumn;
    cxGridDBTableView3cNmCategoria: TcxGridDBColumn;
    ER2Excel1: TER2Excel;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    PopupMenu1: TPopupMenu;
    ManutenoPedido1: TMenuItem;
    dxComponentPrinter1Link1: TdxCustomContainerReportLink;
    GroupBox4: TGroupBox;
    cxTextEdit3: TcxTextEdit;
    Label29: TLabel;
    cxTextEdit2: TcxTextEdit;
    Label24: TLabel;
    cxGridDBTableView3nQtdePend: TcxGridDBColumn;
    usp_ProcessonQtdePend: TBCDField;
    procedure RadioGroup1Click(Sender: TObject);
    procedure ToolButton6Click(Sender: TObject);
    procedure ToolButton4Click(Sender: TObject);
    procedure usp_ProcessoAfterScroll(DataSet: TDataSet);
    procedure ToolButton8Click(Sender: TObject);
    procedure RadioGroup2Click(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure cxGridDBTableView3DblClick(Sender: TObject);
    procedure cxGridDBTableView3StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure cxGridDBTableView3CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure RadioGroup3Click(Sender: TObject);
    procedure ToolButton13Click(Sender: TObject);
    procedure ER2Excel1BeforeExport(Sender: TObject);
    procedure ER2Excel1AfterExport(Sender: TObject);
    procedure ManutenoPedido1Click(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFollowUP_Pedido: TfrmFollowUP_Pedido;

implementation

uses fMenu, rPedidoCom_Simples, fNovo_FollowUp, fFiltroFollowUp,
  fConsultaPedComAberTerceiro_Itens, fManutPedido;

{$R *.dfm}

procedure TfrmFollowUP_Pedido.RadioGroup1Click(Sender: TObject);
begin
  inherited;

  usp_Processo.Close ;
  usp_Processo.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (RadioGroup1.ItemIndex = 0) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'T' ;

  if (RadioGroup1.ItemIndex = 1) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'A' ;

  if (RadioGroup1.ItemIndex = 2) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'C' ;


  if (RadioGroup2.ItemIndex = 0) then
      usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'T' ;

  if (RadioGroup2.ItemIndex = 1) then
      usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'C' ;

  if (RadioGroup2.ItemIndex = 2) then
      usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'V' ;

  usp_Processo.Parameters.ParamByName('@cFlgModoVisao').Value := RadioGroup3.ItemIndex ;
  usp_Processo.Open ;

  //DBGridEh1.Columns[11].DisplayFormat := '#,##0.00' ;
  //DBGridEh1.Columns[10].DisplayFormat := '#,##0.00' ;

end;

procedure TfrmFollowUP_Pedido.ToolButton6Click(Sender: TObject);
begin
  inherited;

  dxComponentPrinter1.Preview(True,nil);

end;

procedure TfrmFollowUP_Pedido.ToolButton4Click(Sender: TObject);
begin

  if not usp_processo.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (usp_Processo.FieldList[3].Value = 'Cr�tico') then
  begin
      ShowMessage('Pedido j� est� em situa��o cr�tica.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a altera��o da situa��o deste pedido?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo: exit ;
  end;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('UPDATE Pedido SET cFlgCritico = 1 WHERE nCdPedido = ' + usp_Processo.FieldList[1].asString) ;

  frmMenu.Connection.BeginTrans;

  try
      qryAux.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans ;
      ShowMessage('Erro no processamento.') ;
      raise ;
  end ;

  qryAux.Close ;
  
  frmMenu.Connection.CommitTrans ;

  ShowMessage('Pedido marcado.') ;

end;

procedure TfrmFollowUP_Pedido.usp_ProcessoAfterScroll(DataSet: TDataSet);
begin
  inherited;
  {PosicionaQuery(qryFollow,usp_Processo.FieldList[1].asString) ;}
end;

procedure TfrmFollowUP_Pedido.ToolButton8Click(Sender: TObject);
var
  objForm : TfrmNovo_FollowUp ;
  i,nCdPedido : integer;
begin
  inherited;
  if not usp_processo.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  objForm := TfrmNovo_FollowUp.Create( Self ) ;

  objForm.qryPreparaTemp.Close;
  objForm.qryPreparaTemp.ExecSQL ;

  objForm.qryTempFollowUP.Close ;
  objForm.qryTempFollowUP.Open ;

  for i:= 0 to cxGridDBTableView3.Controller.SelectedRowCount -1 do
  begin

      nCdPedido := cxGridDBTableView3.Controller.SelectedRows[i].Values[0];

      qryBuscaPedido.Close;
      qryBuscaPedido.Parameters.ParamByName('nPK').Value := nCdPedido;
      qryBuscaPedido.Open;

      if (qryBuscaPedido.Eof) then
      begin
          objForm.qryTempFollowUP.Insert ;
          objForm.qryTempFollowUPnCdPedido.Value   := nCdPedido ;
          objForm.qryTempFollowUPnCdUsuario.Value  := frmMenu.nCdUsuarioLogado ;
          objForm.qryTempFollowUPdDtFollowUp.Value := Now() ;

          objForm.qryTempFollowUP.Post;
      end;
  end;
  
  showForm( objForm , TRUE ) ;

  usp_Processo.Close ;
  usp_Processo.Open ;

  PosicionaQuery(qryFollow,usp_Processo.FieldList[1].asString) ;

end;

procedure TfrmFollowUP_Pedido.RadioGroup2Click(Sender: TObject);
begin
  inherited;
  usp_Processo.Close ;
  usp_Processo.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (RadioGroup1.ItemIndex = 0) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'T' ;

  if (RadioGroup1.ItemIndex = 1) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'A' ;

  if (RadioGroup1.ItemIndex = 2) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'C' ;


  if (RadioGroup2.ItemIndex = 0) then
      usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'T' ;

  if (RadioGroup2.ItemIndex = 1) then
      usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'C' ;

  if (RadioGroup2.ItemIndex = 2) then
      usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'V' ;

  usp_Processo.Parameters.ParamByName('@cFlgModoVisao').Value := RadioGroup3.ItemIndex ;

  usp_Processo.Open ;

//  DBGridEh1.Columns[11].DisplayFormat := '#,##0.00' ;
//  DBGridEh1.Columns[10].DisplayFormat := '#,##0.00' ;

end;

procedure TfrmFollowUP_Pedido.ToolButton10Click(Sender: TObject);
var
  objFiltroFollwUp : TfrmFiltroFollowUp ;
begin
  inherited;

  objFiltroFollwUp := TfrmFiltroFollowUp.Create( Self ) ;

  showForm( objFiltroFollwUp , FALSE ) ;

  usp_Processo.Close ;

  if (RadioGroup1.ItemIndex = 0) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'T' ;

  if (RadioGroup1.ItemIndex = 1) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'A' ;

  if (RadioGroup1.ItemIndex = 2) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'C' ;


  if (RadioGroup2.ItemIndex = 0) then
      usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'T' ;

  if (RadioGroup2.ItemIndex = 1) then
      usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'C' ;

  if (RadioGroup2.ItemIndex = 2) then
      usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'V' ;

  usp_Processo.Parameters.ParamByName('@nCdUsuario').Value        := frmMenu.nCdUsuarioLogado ;
  usp_processo.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(objFiltroFollwUp.MaskEdit1.Text) ;
  usp_processo.Parameters.ParamByName('@nCdGrupoEconomico').Value := frmMenu.ConvInteiro(objFiltroFollwUp.MaskEdit2.Text) ;
  usp_processo.Parameters.ParamByName('@nCdTerceiro').Value       := frmMenu.ConvInteiro(objFiltroFollwUp.MaskEdit3.Text) ;
  usp_processo.Parameters.ParamByName('@nCdTipoPedido').Value     := frmMenu.ConvInteiro(objFiltroFollwUp.MaskEdit4.Text) ;
  usp_processo.Parameters.ParamByName('@nCdMarca').Value          := frmMenu.ConvInteiro(objFiltroFollwUp.MaskEdit5.Text) ;
  usp_processo.Parameters.ParamByName('@dDtPrevEntIni').Value     := frmMenu.ConvData(objFiltroFollwUp.MaskEdit6.Text)    ;
  usp_processo.Parameters.ParamByName('@dDtPrevEntFim').Value     := frmMenu.ConvData(objFiltroFollwUp.MaskEdit7.Text)    ;
  usp_processo.Parameters.ParamByName('@dDtPedidoIni').Value      := frmMenu.ConvData(objFiltroFollwUp.MaskEdit8.Text)    ;
  usp_processo.Parameters.ParamByName('@dDtPedidoFim').Value      := frmMenu.ConvData(objFiltroFollwUp.MaskEdit9.Text)    ;
  usp_processo.Parameters.ParamByName('@nCdCC').Value             := frmMenu.Convinteiro(objFiltroFollwUp.MaskEdit10.Text)   ;
  usp_processo.Parameters.ParamByName('@cFlgWERP').Value          := 0 ;
  usp_processo.Parameters.ParamByName('@nCdProduto').Value        := frmMenu.ConvInteiro(objfiltroFollwUp.edtCdProduto.Text) ;
  usp_Processo.Open ;

  freeAndNil( objFiltroFollwUp ) ;

end;

procedure TfrmFollowUP_Pedido.FormShow(Sender: TObject);
begin
  inherited;

  Application.ProcessMessages;

  {-- exibe as colunas de departamento e categoria se o modo de vis�o for marca/departamento/categoria --}
  cxGridDBTableView3cNmDepartamento.Visible := FALSE;
  cxGridDBTableView3cNmCategoria.Visible    := FALSE;

  ToolButton10Click(nil) ;

end;

procedure TfrmFollowUP_Pedido.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmConsultaPedComAberTerceiro_Itens ;
begin
  inherited;

  if not usp_Processo.Eof then
  begin

      objForm := TfrmConsultaPedComAberTerceiro_Itens.Create( Self ) ;

      PosicionaQuery(objForm.qryItens, usp_ProcessonCdPedido.AsString) ;

      showForm( objForm , TRUE ) ;

  end ;

end;

procedure TfrmFollowUP_Pedido.cxGridDBTableView3DblClick(Sender: TObject);
var
  objForm : TfrmConsultaPedComAberTerceiro_Itens ;
begin
  inherited;

  if not usp_Processo.Eof then
  begin

      objForm := TfrmConsultaPedComAberTerceiro_Itens.Create( Self ) ;

      PosicionaQuery(objForm.qryItens, usp_ProcessonCdPedido.AsString) ;

      showForm( objForm , TRUE ) ;

  end ;

end;

procedure TfrmFollowUP_Pedido.cxGridDBTableView3StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if (AItem = cxGridDBTableView3nCdPedido) then
  begin

      try
          if (ARecord.Values[3] <> 'Normal') then
              AStyle := frmMenu.LinhaAmarela ;

          if (ARecord.Values[3] = 'Cr�tico') then
              AStyle := frmMenu.LinhaVermelha ;

      except
      end ;

  end ;

end;

procedure TfrmFollowUP_Pedido.cxGridDBTableView3CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;

  qryFollow.Close;

  if (usp_ProcessonCdPedido.Value > 0) then
      PosicionaQuery(qryFollow,usp_ProcessonCdPedido.asString) ;

end;

procedure TfrmFollowUP_Pedido.RadioGroup3Click(Sender: TObject);
begin
  inherited;

  {-- exibe as colunas de departamento e categoria se o modo de vis�o for marca/departamento/categoria --}
  cxGridDBTableView3cNmDepartamento.Visible := (RadioGroup3.ItemIndex = 1);
  cxGridDBTableView3cNmCategoria.Visible    := (RadioGroup3.ItemIndex = 1);

  usp_Processo.Close ;
  usp_Processo.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  if (RadioGroup1.ItemIndex = 0) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'T' ;

  if (RadioGroup1.ItemIndex = 1) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'A' ;

  if (RadioGroup1.ItemIndex = 2) then
      usp_Processo.Parameters.ParamByName('@cTipoExibicao').Value := 'C' ;


  if (RadioGroup2.ItemIndex = 0) then
      usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'T' ;

  if (RadioGroup2.ItemIndex = 1) then
      usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'C' ;

  if (RadioGroup2.ItemIndex = 2) then
      usp_Processo.Parameters.ParamByName('@cTipoPedido').Value := 'V' ;

  usp_Processo.Parameters.ParamByName('@cFlgModoVisao').Value := RadioGroup3.ItemIndex ;
  usp_Processo.Open ;

end;

procedure TfrmFollowUP_Pedido.ToolButton13Click(Sender: TObject);
var
  iLinha    : integer ;
  nQtdePend : double;
  cQtdePend : String;
begin
  inherited;

  ER2Excel1.Celula['A1'].Mesclar('H1');
  ER2Excel1.Celula['A1'].HorizontalAlign := haLeft;
  ER2Excel1.Celula['A1'].Text            := 'ER2SOFT - Solu��es Inteligentes para o seu Neg�cio' ;

  ER2Excel1.Celula['A2'].Mesclar('D2');
  ER2Excel1.Celula['A2'].Text := 'Follow UP de Pedidos' ;

  iLinha := 4 ;

  ER2Excel1.Celula['A' + intToStr(iLinha)].Text := 'Pedido' ;
  ER2Excel1.Celula['B' + intToStr(iLinha)].Text := 'Loja' ;
  ER2Excel1.Celula['C' + intToStr(iLinha)].Text := 'Prev. Entrega' ;
  ER2Excel1.Celula['D' + intToStr(iLinha)].Text := 'Tipo de Pedido' ;
  ER2Excel1.Celula['E' + intToStr(iLinha)].Text := 'Situa��o' ;
  ER2Excel1.Celula['F' + intToStr(iLinha)].Text := 'Terceiro' ;
  ER2Excel1.Celula['G' + intToStr(iLinha)].Text := 'Representante' ;
  ER2Excel1.Celula['H' + intToStr(iLinha)].Text := 'Marca' ;
  ER2Excel1.Celula['I' + intToStr(iLinha)].Text := 'Departamento' ;
  ER2Excel1.Celula['J' + intToStr(iLinha)].Text := 'Categoria' ;
  ER2Excel1.Celula['K' + intToStr(iLinha)].Text := 'Qtde. Pendente' ;
  ER2Excel1.Celula['L' + intToStr(iLinha)].Text := 'Saldo' ;
  ER2Excel1.Celula['M' + intToStr(iLinha)].Text := 'Contato' ;
  ER2Excel1.Celula['N' + intToStr(iLinha)].Text := '�ltimo Follow UP' ;

  ER2Excel1.Celula['A' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['B' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['C' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['D' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['E' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['F' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['G' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['H' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['I' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['J' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['K' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['L' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['M' + intToStr(iLinha)].Negrito;
  ER2Excel1.Celula['N' + intToStr(iLinha)].Negrito;

  ER2Excel1.Celula['A' + intToStr(iLinha)].Width := 8 ;
  ER2Excel1.Celula['B' + intToStr(iLinha)].Width := 4 ;
  ER2Excel1.Celula['C' + intToStr(iLinha)].Width := 11 ;
  ER2Excel1.Celula['D' + intToStr(iLinha)].Width := 18 ;
  ER2Excel1.Celula['E' + intToStr(iLinha)].Width := 15 ;
  ER2Excel1.Celula['F' + intToStr(iLinha)].Width := 25 ;
  ER2Excel1.Celula['G' + intToStr(iLinha)].Width := 25 ;
  ER2Excel1.Celula['H' + intToStr(iLinha)].Width := 12 ;
  ER2Excel1.Celula['I' + intToStr(iLinha)].Width := 12 ;
  ER2Excel1.Celula['J' + intToStr(iLinha)].Width := 12 ;
  ER2Excel1.Celula['K' + intToStr(iLinha)].Width := 20 ;
  ER2Excel1.Celula['L' + intToStr(iLinha)].Width := 25 ;
  ER2Excel1.Celula['M' + intToStr(iLinha)].Width := 25 ;
  ER2Excel1.Celula['N' + intToStr(iLinha)].Width := 25 ;

  ER2Excel1.Celula['M' + intToStr(iLinha)].HorizontalAlign := haRight;

  frmMenu.mensagemUsuario('Aguarde...') ;
  
  usp_Processo.First ;

  nQtdePend := 0; // totalizador de qtde pendente
  while not usp_Processo.Eof do
  begin

      inc( iLinha ) ;
      
      ER2Excel1.Celula['A' + intToStr(iLinha)].Text := usp_ProcessonCdPedido.Value ;
      ER2Excel1.Celula['B' + intToStr(iLinha)].Text := usp_ProcessonCdLoja.asString ;
      ER2Excel1.Celula['C' + intToStr(iLinha)].Text := usp_ProcessocMesAnoEntrega.Value ;
      ER2Excel1.Celula['D' + intToStr(iLinha)].Text := usp_ProcessocNmTipoPedido.Value ;
      ER2Excel1.Celula['E' + intToStr(iLinha)].Text := usp_ProcessocNmSituacao.Value ;
      ER2Excel1.Celula['F' + intToStr(iLinha)].Text := usp_ProcessocNmTerceiro.Value ;
      ER2Excel1.Celula['G' + intToStr(iLinha)].Text := usp_ProcessocNmTerceiroRepres.Value ;
      ER2Excel1.Celula['H' + intToStr(iLinha)].Text := usp_ProcessocNmMarca.Value ;
      ER2Excel1.Celula['I' + intToStr(iLinha)].Text := usp_ProcessocNmDepartamento.Value ;
      ER2Excel1.Celula['J' + intToStr(iLinha)].Text := usp_ProcessocNmCategoria.Value ;
      ER2Excel1.Celula['K' + intToStr(iLinha)].Text := usp_ProcessonQtdePend.Value ;
      ER2Excel1.Celula['L' + intToStr(iLinha)].Text := usp_ProcessonSaldoFat.Value ;
      ER2Excel1.Celula['M' + intToStr(iLinha)].Text := usp_ProcessocNmContato.Value ;
      ER2Excel1.Celula['N' + intToStr(iLinha)].Text := usp_ProcessocUltimoAcomp.Value ;

      ER2Excel1.Celula['A' + intToStr(iLinha)].Width := 8 ;
      ER2Excel1.Celula['B' + intToStr(iLinha)].Width := 4 ;
      ER2Excel1.Celula['C' + intToStr(iLinha)].Width := 11 ;
      ER2Excel1.Celula['D' + intToStr(iLinha)].Width := 18 ;
      ER2Excel1.Celula['E' + intToStr(iLinha)].Width := 15 ;
      ER2Excel1.Celula['F' + intToStr(iLinha)].Width := 25 ;
      ER2Excel1.Celula['G' + intToStr(iLinha)].Width := 25 ;
      ER2Excel1.Celula['H' + intToStr(iLinha)].Width := 12 ;
      ER2Excel1.Celula['I' + intToStr(iLinha)].Width := 12 ;
      ER2Excel1.Celula['J' + intToStr(iLinha)].Width := 12 ;
      ER2Excel1.Celula['K' + intToStr(iLinha)].Width := 20 ;
      ER2Excel1.Celula['L' + intToStr(iLinha)].Width := 25 ;
      ER2Excel1.Celula['M' + intToStr(iLinha)].Width := 25 ;
      ER2Excel1.Celula['N' + intToStr(iLinha)].Width := 25 ;

      ER2Excel1.Celula['M' + intToStr(iLinha)].HorizontalAlign := haRight;


      nQtdePend := nQtdePend + usp_ProcessonQtdePend.Value; // totalizador de qtde pendente
      usp_Processo.Next ;
  end ;

  {-- Coloca qtde pendente totalizada no rodap� --}
  cQtdePend := FormatFloat('#,##0.00', nQtdePend);
  ER2Excel1.Celula['K' + intToStr(iLinha + 2)].Text := 'R$ ' + cQtdePend;

  usp_Processo.First ;

  { -- exporta planilha e limpa result do componente -- }
  ER2Excel1.ExportXLS;
  ER2Excel1.CleanupInstance;
end;

procedure TfrmFollowUP_Pedido.ER2Excel1BeforeExport(Sender: TObject);
begin
  inherited;
  frmMenu.mensagemUsuario('Exportando planilha...');
end;

procedure TfrmFollowUP_Pedido.ER2Excel1AfterExport(Sender: TObject);
begin
  inherited;
  frmMenu.mensagemUsuario('');

end;

procedure TfrmFollowUP_Pedido.ManutenoPedido1Click(Sender: TObject);
var
  objForm : TfrmManutPedido ;
begin
  inherited;

  objForm := TfrmManutPedido.Create( Self ) ;
  objForm.PosicionaPK( usp_ProcessonCdPedido.Value );

  showForm( objForm , TRUE ) ;

  usp_Processo.Close;
  usp_Processo.Open;

end;

procedure TfrmFollowUP_Pedido.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  ManutenoPedido1.Enabled := ( usp_Processo.RecordCount > 0) ;
end;

initialization
    RegisterClass(TfrmFollowUP_Pedido) ;

end.
