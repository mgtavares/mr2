unit fProdutoWeb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxPC, cxControls, ER2Lookup, dxExEdtr,
  dxCntner, dxTL, dxDBCtrl, dxDBGrid, DBGridEhGrouping, ToolCtrlsEh,
  GridsEh, DBGridEh, cxLookAndFeelPainters, cxButtons, dxEditor, dxEdLib,
  Buttons, Menus, OleCtrls, SHDocVw, Activex, MsHtml;

type
  TfrmProdutoWeb = class(TfrmCadastro_Padrao)
    cxPageControl1: TcxPageControl;
    tabSubItem: TcxTabSheet;
    qryMasternAltura: TBCDField;
    qryMasternEspessura: TBCDField;
    qryMasternPesoBruto: TBCDField;
    qryMasternVolume: TBCDField;
    qryMasternLargura: TBCDField;
    qryMasternFatorPeso: TBCDField;
    qryMasternPesoLiquido: TBCDField;
    qryMastercUnidadeMedida: TStringField;
    qryMasternDimensao: TBCDField;
    qryMasternCdIdExternoWeb: TIntegerField;
    DBEdit14: TDBEdit;
    Label1: TLabel;
    DBEdit15: TDBEdit;
    qryMasternCdCategoria: TIntegerField;
    qryMasternCdSubCategoria: TIntegerField;
    qryMasternCdMarca: TIntegerField;
    Label2: TLabel;
    DBEdit16: TDBEdit;
    Label3: TLabel;
    DBEdit17: TDBEdit;
    Label4: TLabel;
    DBEdit18: TDBEdit;
    qryMarca: TADOQuery;
    dsMarca: TDataSource;
    qryMarcacNmMarca: TStringField;
    qryMarcanCdIdExternoWeb: TIntegerField;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    qryCategoria: TADOQuery;
    dsCategoria: TDataSource;
    Image2: TImage;
    qryMasternValVendaWebDe: TBCDField;
    qryMasternValVendaWebPor: TBCDField;
    Label5: TLabel;
    qryCategoriacNmCategoria: TStringField;
    qryCategorianCdIdExternoWeb: TIntegerField;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    qrySubCategoria: TADOQuery;
    dsSubCategoria: TDataSource;
    qrySubCategoriacNmSubCategoria: TStringField;
    qrySubCategorianCdIdExternoWeb: TIntegerField;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    qryMasternCdFornecedorWeb: TIntegerField;
    er2LkpFornecedorWeb: TER2LookupDBEdit;
    qryFornecedor: TADOQuery;
    dsFornecedor: TDataSource;
    qryFornecedornCdTerceiro: TIntegerField;
    qryFornecedorcNmTerceiro: TStringField;
    qryFornecedornCdIdExternoWeb: TIntegerField;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    Label6: TLabel;
    DBGridEh1: TDBGridEh;
    qryProdutoFinal: TADOQuery;
    dsProdutoFinal: TDataSource;
    qryMasternCdProdutoPai: TIntegerField;
    qryTabFichaTecnicaProduto: TADOQuery;
    dsTabFichaTecnicaProduto: TDataSource;
    tabFichaTecnica: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryTabFichaTecnicaProdutonCdTabFichaTecnicaProduto: TIntegerField;
    qryTabFichaTecnicaProdutonCdProduto: TIntegerField;
    qryTabFichaTecnicaProdutonCdTabFichaTecnica: TIntegerField;
    qryTabFichaTecnicaProdutocDescricao: TStringField;
    qryTabFichaTecnica: TADOQuery;
    qryTabFichaTecnicacNmTabFichaTecnica: TStringField;
    dsTabFichaTecnica: TDataSource;
    qryTabFichaTecnicanCdTabFichaTecnica: TIntegerField;
    qryTabFichaTecnicaProdutocNmTabFichaTecnica: TStringField;
    qryMastercDescricaoWeb: TMemoField;
    qryProdutoFinalnCdProduto: TIntegerField;
    qryProdutoFinalcNmTamanho: TStringField;
    qryProdutoFinalcNmProduto: TStringField;
    qryProdutoFinalnValVendaWebDe: TBCDField;
    qryProdutoFinalnValVendaWebPor: TBCDField;
    Panel1: TPanel;
    btAplicaPreco: TcxButton;
    qryMastercNmProduto: TStringField;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    qryDepartamento: TADOQuery;
    dsDepartamento: TDataSource;
    qryMasternCdDepartamento: TIntegerField;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    Label7: TLabel;
    DBEdit9: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    qryMastercFlgProdWeb: TIntegerField;
    qryProdutoFinalnDimensao: TBCDField;
    qryProdutoFinalnAltura: TBCDField;
    qryProdutoFinalnLargura: TBCDField;
    qryProdutoFinalcFlgProdWeb: TIntegerField;
    qryProdutoFinalnPesoLiquido: TBCDField;
    menuOpcao: TPopupMenu;
    ToolButton3: TToolButton;
    btOpcao: TToolButton;
    Departamento1: TMenuItem;
    Marca1: TMenuItem;
    Fornecedor1: TMenuItem;
    N1: TMenuItem;
    GerarPosioEstoque1: TMenuItem;
    SP_ENVIA_POSICAO_ESTOQUE_TEMPREGISTROWEB: TADOStoredProc;
    qryProdutoFinalnQtdeDisponivel: TBCDField;
    qryValidaSubItem: TADOQuery;
    tabDescricao: TcxTabSheet;
    Panel2: TPanel;
    btEditorHTML: TcxButton;
    btAtivaProduto: TcxButton;
    btDesativaProduto: TcxButton;
    Label15: TLabel;
    edtStatusWeb: TEdit;
    btAplicaDimensao: TcxButton;
    qryDescricaoProdutoWeb: TADOQuery;
    qryDescricaoProdutoWebcNmProdutoWeb: TStringField;
    qryMastercNmProdutoWeb: TStringField;
    Label16: TLabel;
    DBEdit1: TDBEdit;
    qryMasternCdProduto: TIntegerField;
    qryProdutoFinalcNmProdutoWeb: TStringField;
    Label17: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label18: TLabel;
    DBEdit4: TDBEdit;
    dsSegmento: TDataSource;
    qrySegmento: TADOQuery;
    qrySegmentonCdSegmento: TAutoIncField;
    qrySegmentonCdSubCategoria: TIntegerField;
    qrySegmentocNmSegmento: TStringField;
    qrySegmentonCdStatus: TIntegerField;
    qryMasternCdSegmento: TIntegerField;
    qrySubCategorianCdSubCategoria: TIntegerField;
    qrySegmentonCdIdExternoWeb: TIntegerField;
    DBMemo1: TDBMemo;
    qryProdutoFinalnQtdeEmpenho: TBCDField;
    qryProdutoFinalnQtdeSaldoDisp: TBCDField;
    qryDepartamentonCdIdExternoWeb: TIntegerField;
    Label19: TLabel;
    DBEdit5: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryTabFichaTecnicaProdutoCalcFields(DataSet: TDataSet);
    procedure DBGridEh2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTabFichaTecnicaProdutoBeforePost(DataSet: TDataSet);
    procedure btEditorHTMLClick(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btAplicaPrecoClick(Sender: TObject);
    procedure qryProdutoFinalBeforePost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure Departamento1Click(Sender: TObject);
    procedure Marca1Click(Sender: TObject);
    procedure Fornecedor1Click(Sender: TObject);
    procedure GerarPosioEstoque1Click(Sender: TObject);
    procedure CarregarHTML(cCodigoHTML : String);
    procedure AlteraStatusWeb();
    procedure btAtivaProdutoClick(Sender: TObject);
    procedure btDesativaProdutoClick(Sender: TObject);
    procedure btAplicaDimensaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmProdutoWeb: TfrmProdutoWeb;
  HTMLDocumento : IHTMLDocument2;

implementation

uses fLookup_Padrao, fMenu, fEditorHTML, fProdutoWeb_AplicaPrecoProduto,
     fDepartamento, fMarca, fFornecedor, fProdutoWeb_AplicaDimensaoProduto;

{$R *.dfm}

procedure TfrmProdutoWeb.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster           := 'PRODUTO';
  nCdTabelaSistema          := 25;
  nCdConsultaPadrao         := 769;
  cxPageControl1.ActivePage := tabSubItem;
  bLimpaAposSalvar          := False;

  { -- define p�gina em branco -- }
  //webCodigoHTML.Navigate('about:blank');

  btAtivaProduto.Left    := 850;
  btDesativaProduto.Left := 850;
end;

procedure TfrmProdutoWeb.CarregarHTML(cCodigoHTML : String);
var
   sl: TStringList;
   ms: TMemoryStream;
begin
  {sl := TStringList.Create;
  ms := TMemoryStream.Create;

  while (webCodigoHTML.ReadyState < READYSTATE_INTERACTIVE) do
      Application.ProcessMessages;

  if Assigned(webCodigoHTML.Document) then
  begin
      try
          try
              sl.Text := cCodigoHTML;
              sl.SaveToStream(ms);
              ms.Seek(0,0);
             (webCodigoHTML.Document as IPersistStreamInit).Load(TStreamAdapter.Create(ms)) ;
          finally
              ms.Free;
          end;
      finally
          sl.Free;
      end;
  end;}
end;

procedure TfrmProdutoWeb.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if Trim(qryMastercNmProdutoWeb.Value) = '' then
  begin
      qryDescricaoProdutoWeb.Close;
      qryDescricaoProdutoWeb.Parameters.ParamByName('nPK').Value := qryMasternCdProduto.Value;
      qryDescricaoProdutoWeb.ExecSQL;

      PosicionaQuery(qryMaster,qryMasternCdProduto.AsString);
  end;

  PosicionaQuery(qryMarca,qryMasternCdMarca.AsString);
  PosicionaQuery(qryDepartamento,qryMasternCdDepartamento.AsString);
  PosicionaQuery(qryCategoria,qryMasternCdCategoria.AsString);
  PosicionaQuery(qrySubCategoria,qryMasternCdSubCategoria.AsString);

  qrySegmento.Close;
  qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qryMasternCdSubCategoria.Value;
  qrySegmento.Parameters.ParamByName('nPK').Value             := qryMasternCdSegmento.Value;
  qrySegmento.Open;

  PosicionaQuery(qryFornecedor,qryMasternCdFornecedorWeb.AsString);
  PosicionaQuery(qryProdutoFinal,qryMasternCdProduto.AsString);
  PosicionaQuery(qryTabFichaTecnicaProduto,qryMasternCdProduto.AsString);

  cxPageControl1.ActivePage := tabSubItem;
  CarregarHTML(qryMastercDescricaoWeb.Value);

  edtStatusWeb.Clear;
  btAtivaProduto.Visible    := False;
  btDesativaProduto.Visible := False;

  if (not qryMaster.Eof) then
  begin
      if (qryMastercFlgProdWeb.Value = 0) then
      begin
          edtStatusWeb .Text := 'PRODUTO INATIVO';

          btAtivaProduto.Visible    := False;
          btDesativaProduto.Visible := True;
      end
      else
      begin
          edtStatusWeb.Text := 'PRODUTO ATIVO';

          btAtivaProduto.Visible    := True;
          btDesativaProduto.Visible := False;
      end;
  end;
end;

procedure TfrmProdutoWeb.qryTabFichaTecnicaProdutoCalcFields(DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryTabFichaTecnica,qryTabFichaTecnicaProdutonCdTabFichaTecnica.AsString);
  qryTabFichaTecnicaProdutocNmTabFichaTecnica.Value := qryTabFichaTecnicacNmTabFichaTecnica.Value;
end;

procedure TfrmProdutoWeb.DBGridEh2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : Integer;
begin
  inherited;

    case (Key) of
        VK_F4 : begin
                    if (qryTabFichaTecnicaProduto.State = dsBrowse) then
                        qryTabFichaTecnicaProduto.Edit ;

                    if (qryTabFichaTecnicaProduto.State in [dsInsert,dsEdit]) then
                    begin
                        if (DBGridEh2.Columns[DBGridEh2.Col-1].FieldName = 'nCdTabFichaTecnica') then
                        begin
                            nPK := frmLookup_Padrao.ExecutaConsulta(768);

                            if (nPK > 0) then
                                qryTabFichaTecnicaProdutonCdTabFichaTecnica.Value := nPK ;
                        end;
                    end;
                end;
    end;
end;

procedure TfrmProdutoWeb.qryTabFichaTecnicaProdutoBeforePost(DataSet: TDataSet);
begin
  if (qryTabFichaTecnica.IsEmpty) then
  begin
      MensagemAlerta('C�digo da ficha t�cnica inv�lida.');
      Abort;
  end;

  if (Trim(qryTabFichaTecnicaProdutocDescricao.Value) = '') then
  begin
      MensagemAlerta('Descri��o da ficha t�cnica deve ser preenchido.');
      Abort;
  end;

  inherited;

  qryTabFichaTecnicaProdutocDescricao.Value                := UpperCase(qryTabFichaTecnicaProdutocDescricao.Value);
  qryTabFichaTecnicaProdutonCdTabFichaTecnicaProduto.Value := frmMenu.fnProximoCodigo('TABFICHATECNICAPRODUTO');
  qryTabFichaTecnicaProdutonCdProduto.Value                := qryMasternCdProduto.Value;
end;

procedure TfrmProdutoWeb.btEditorHTMLClick(Sender: TObject);
var
  objForm : TfrmEditorHTML;
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  objForm := TfrmEditorHTML.Create(Self);

  { -- carrega descri��o html -- }
  if (not qryMastercDescricaoWeb.IsNull) then
  begin
     objForm.CarregarHTML(qryMastercDescricaoWeb.Value);
  end;

  objForm.ShowModal;

  { -- gera descri��o html -- }
  qryMastercDescricaoWeb.Value := objForm.GeraHTML(objForm.webCodigoHTML);
  qryMaster.Post;

  CarregarHTML(qryMastercDescricaoWeb.Value);

  FreeAndNil(objForm);
end;

procedure TfrmProdutoWeb.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryMarca.Close;
  qryDepartamento.Close;
  qryCategoria.Close;
  qrySubCategoria.Close;
  qrySegmento.Close;
  qryFornecedor.Close;
  qryTabFichaTecnicaProduto.Close;
  qryTabFichaTecnica.Close;
  qryProdutoFinal.Close;

  { -- define p�gina em branco -- }
  //webCodigoHTML.Navigate('about:blank');

  edtStatusWeb.Clear;
  btAtivaProduto.Visible    := False;
  btDesativaProduto.Visible := False;

end;

procedure TfrmProdutoWeb.btAplicaPrecoClick(Sender: TObject);
var
  objForm : TfrmProdutoWeb_AplicaPrecoProduto;
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  objForm               := TfrmProdutoWeb_AplicaPrecoProduto.Create(nil);
  objForm.nCdProdutoPai := qryMasternCdProduto.Value;
  objForm.ShowModal;

  PosicionaQuery(qryProdutoFinal,qryMasternCdProduto.AsString);

  { -- se sub item possui valor, zera valor do produto -- }
  qryProdutoFinal.First;

  if (qryProdutoFinalnValVendaWebDe.Value <> 0) then
  begin
      qryMasternValVendaWebDe.Value  := 0;
      qryMasternValVendaWebPor.Value := 0;
  end;

  FreeAndNil(objForm);
end;

procedure TfrmProdutoWeb.qryProdutoFinalBeforePost(DataSet: TDataSet);
begin

  { -- n�o permite informar pre�os menor que 0 -- }
  if ((qryProdutoFinalnValVendaWebDe.Value < 0) or (qryProdutoFinalnValVendaWebPor.Value < 0)) then
  begin
      MensagemAlerta('Os pre�os de venda informados devem ser maior ou igual a zero.');
      Abort;
  end;

  { -- se o valor do sub item for igual a 0, obriga que o usu�rio informe os pre�os do produto n�vel 2. -- }
  if ( (qryMasternValVendaWebDe.Value       = 0)  and (qryMasternValVendaWebPor.Value       = 0)   and
      ((qryProdutoFinalnValVendaWebDe.Value = 0)  and (qryProdutoFinalnValVendaWebPor.Value = 0))) then
  begin
      MensagemAlerta('� necess�rio informar os pre�os de venda do produto ou informe os pre�os de venda de todos os sub itens.');
      Abort;
  end;

  { -- o pre�o de venda (de) n�o pode ser menor do que o pre�o de venda (por) -- }
  if (qryProdutoFinalnValVendaWebDe.Value < qryProdutoFinalnValVendaWebPor.Value) then
  begin
      MensagemAlerta('Pre�o de Venda (De) informado deve ser maior ou igual ao Pre�o de Venda (Por).');
      Abort;
  end;

  { -- valida as dimens�es do produto -- }
  if (qryProdutoFinalnDimensao.Value < 0) then
  begin
      MensagemAlerta('O comprimento do produto n�o pode ser menor que 0.');
      Abort;
  end;

  if (qryProdutoFinalnAltura.Value < 0) then
  begin
      MensagemAlerta('A altura do produto n�o pode ser menor que 0.');
      Abort;
  end;

  if (qryProdutoFinalnLargura.Value < 0) then
  begin
      MensagemAlerta('A largura do produto n�o pode ser menor que 0.');
      Abort;
  end;

  if (qryProdutoFinalnPesoLiquido.Value < 0) then
  begin
      MensagemAlerta('O peso l�quido do produto n�o pode ser menor que 0.');
      Abort;
  end;

  inherited;
end;

procedure TfrmProdutoWeb.qryMasterBeforePost(DataSet: TDataSet);
begin
  { -- se o produto web estiver ativo  -- }
  if (qryMastercFlgProdWeb.Value = 1) then
  begin
      { -- valida departamento web -- }
      if (qryDepartamentonCdIdExternoWeb.IsNull) then
      begin
          MensagemAlerta('O departamento do produto n�o possui um c�digo externo.');
          DBEdit5.SetFocus;
          Abort;
      end;

      { -- valida categoria/subcategoria/segmento web -- }
      if (qryCategorianCdIdExternoWeb.IsNull) and (qrySubCategorianCdIdExternoWeb.IsNull) and (qrySegmentonCdIdExternoWeb.IsNull) then
      begin
          MensagemAlerta('A categoria e(ou) a subcategoria e(ou) o segmento do produto n�o possui um c�digo externo.');
          DBEdit13.SetFocus;
          Abort;
      end;

      { -- valida marca web -- }
      if (qryMarcanCdIdExternoWeb.IsNull) then
      begin
          MensagemAlerta('A marca do produto n�o possui um c�digo externo.');
          DBEdit20.SetFocus;
          Abort;
      end;

      { -- verifica se produto possui no m�nimo um sub item ativo na web -- }
      qryValidaSubItem.Close;
      qryValidaSubItem.Parameters.ParamByName('nPK').Value := qryMasternCdProduto.Value;
      qryValidaSubItem.Open;

      if (qryValidaSubItem.IsEmpty) then
      begin
          MensagemAlerta('� necess�rio existir no m�nimo um sub item ativo.');
          Abort;
      end;
  end;

  { -- valida fornecedor web -- }
  if ((Trim(er2LkpFornecedorWeb.Text) <> '') and (qryFornecedor.Eof)) then
  begin
      MensagemAlerta('C�digo do fornecedor inv�lido.');
      er2LkpFornecedorWeb.SetFocus;
      Abort;
  end;

  { -- n�o permite informar pre�os menor que 0 -- }
  if ((qryMasternValVendaWebDe.Value < 0) or (qryMasternValVendaWebPor.Value < 0)) then
  begin
      MensagemAlerta('Os pre�os de venda informados devem ser maior ou igual a zero.');
      DBEdit26.SetFocus;
      Abort;
  end;

  { -- o pre�o de venda (de) n�o pode ser menor do que o pre�o de venda (por) -- }
  if (qryMasternValVendaWebDe.Value < qryMasternValVendaWebPor.Value) then
  begin
      MensagemAlerta('Pre�o de Venda (De) informado deve ser maior ou igual ao Pre�o de Venda (Por).');
      DBEdit26.SetFocus;
      Abort;
  end;

  { -- se os valores de venda forem igual a zero, obriga o usu�rio informar pre�o (De/Por) nos sub-itens (SKU's) -- }
  if ((qryMasternValVendaWebDe.Value = 0) and (qryMasternValVendaWebPor.Value = 0)) then
  begin
      qryProdutoFinal.First;

      while not (qryProdutoFinal.Eof) do
      begin
          if ((qryProdutoFinalnValVendaWebDe.Value = 0) and (qryProdutoFinalnValVendaWebPor.Value = 0)) then
          begin
              MensagemAlerta('� necess�rio informar os pre�os de venda do produto ou informe os pre�os de venda de todos os sub itens.');
              Abort;
          end;

          qryProdutoFinal.Next;
      end;
  end;

  qryProdutoFinal.First;

  while not (qryProdutoFinal.Eof) do
  begin
      if ((qryProdutoFinalnValVendaWebDe.Value <> 0) and (qryProdutoFinalnValVendaWebPor.Value <> 0)) then
      begin
          if ((qryMasternValVendaWebDe.Value <> 0) and (qryMasternValVendaWebPor.Value <> 0)) then
          begin
              MensagemAlerta('� necess�rio informar os pre�os de venda do produto ou informe os pre�os de venda de todos os sub itens.');
              Abort;
          end;
      end;

      qryProdutoFinal.Next;
  end;

  inherited;
end;

procedure TfrmProdutoWeb.Departamento1Click(Sender: TObject);
var
  objForm : TfrmDepartamento;
begin
  inherited;

  { -- valida permiss�o de usu�rio -- }
  frmMenu.qryUsuarioAplicacao.Close;
  frmMenu.qryUsuarioAplicacao.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  frmMenu.qryUsuarioAplicacao.Parameters.ParamByName('cNmObjeto').Value  := 'FRMDEPARTAMENTO';
  frmMenu.qryUsuarioAplicacao.Open;

  if (frmMenu.qryUsuarioAplicacao.IsEmpty) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Abort;
  end;

  { -- carrega aplica��o -- }
  objForm := TfrmDepartamento.Create(nil);

  if (not qryMaster.IsEmpty) then
      PosicionaQuery(objForm.qryMaster, qryMasternCdDepartamento.AsString);

  showForm(objForm,True);

  { -- da um refresh no cadastro  -- }
  if (not qryMaster.IsEmpty) then
  begin
      PosicionaQuery(qryDepartamento,qryMasternCdDepartamento.AsString);
      PosicionaQuery(qryCategoria,qryMasternCdCategoria.AsString);
      PosicionaQuery(qrySubCategoria,qryMasternCdSubCategoria.AsString);

      qrySegmento.Close;
      qrySegmento.Parameters.ParamByName('nCdSubCategoria').Value := qrySubCategorianCdSubCategoria.Value;
      qrySegmento.Parameters.ParamByName('nPK').Value             := qryMasternCdSegmento.Value;
      qrySegmento.Open;
  end;
end;

procedure TfrmProdutoWeb.Marca1Click(Sender: TObject);
var
  objForm : TfrmMarca;
begin
  inherited;

  { -- valida permiss�o de usu�rio -- }
  frmMenu.qryUsuarioAplicacao.Close;
  frmMenu.qryUsuarioAplicacao.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  frmMenu.qryUsuarioAplicacao.Parameters.ParamByName('cNmObjeto').Value  := 'FRMMARCA';
  frmMenu.qryUsuarioAplicacao.Open;

  if (frmMenu.qryUsuarioAplicacao.IsEmpty) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Abort;
  end;

  { -- carrega aplica��o -- }
  objForm := TfrmMarca.Create(nil);

  if (not qryMaster.IsEmpty) then
      PosicionaQuery(objForm.qryMaster, qryMasternCdMarca.AsString);
      
  showForm(objForm,True);

  { -- da um refresh no cadastro  -- }
  if (not qryMaster.IsEmpty) then
      PosicionaQuery(qryMarca, qryMasternCdMarca.AsString);
end;

procedure TfrmProdutoWeb.Fornecedor1Click(Sender: TObject);
var
  objForm : TfrmFornecedor;
begin
  inherited;

  { -- valida permiss�o de usu�rio -- }
  frmMenu.qryUsuarioAplicacao.Close;
  frmMenu.qryUsuarioAplicacao.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  frmMenu.qryUsuarioAplicacao.Parameters.ParamByName('cNmObjeto').Value  := 'FRMFORNECEDOR';
  frmMenu.qryUsuarioAplicacao.Open;

  if (frmMenu.qryUsuarioAplicacao.IsEmpty) then
  begin
      MensagemAlerta('Usu�rio n�o possui permiss�o para utilizar esta aplica��o.');
      Abort;
  end;

  { -- carrega aplica��o -- }
  objForm := TfrmFornecedor.Create(nil);

  if (not qryMaster.IsEmpty) then
      PosicionaQuery(objForm.qryMaster, qryMasternCdFornecedorWeb.AsString);
      
  showForm(objForm,True);

  { -- da um refresh no cadastro  -- }
  if (not qryMaster.IsEmpty) then
      PosicionaQuery(qryFornecedor, qryMasternCdFornecedorWeb.AsString);
end;

procedure TfrmProdutoWeb.GerarPosioEstoque1Click(Sender: TObject);
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  { -- verifica se produto j� foi transmitido para web -- }
  if (qryMasternCdIdExternoWeb.IsNull) then
  begin
      MensagemAlerta('Este produto n�o possui cadastro na web.');
      Abort;
  end;

  if (MessageDLG('Confirma gera��o da posi��o de estoque dos sub itens do produto ' + qryMastercNmProduto.Value + ' ?',mtConfirmation,[mbYes,mbNo],0) =  mrYes) then
  begin
      qryProdutoFinal.First;

      try
          frmMenu.Connection.BeginTrans;

          while not (qryProdutoFinal.Eof) do
          begin
              if (qryProdutoFinalcFlgProdWeb.Value = 1) then
              begin
                  { -- envia posi��o de estoque dos sub itens para a TempRegistroWeb -- }
                  SP_ENVIA_POSICAO_ESTOQUE_TEMPREGISTROWEB.Close;
                  SP_ENVIA_POSICAO_ESTOQUE_TEMPREGISTROWEB.Parameters.ParamByName('@nCdProdutoWeb').Value := qryProdutoFinalnCdProduto.Value;
                  SP_ENVIA_POSICAO_ESTOQUE_TEMPREGISTROWEB.ExecProc;
              end;

              qryProdutoFinal.Next;
          end;

          frmMenu.Connection.CommitTrans;

          ShowMessage('Posi��o de Estoque dos sub itens gerada com sucesso.');
      except
          MensagemErro('Erro no processamento.');
          frmMenu.Connection.RollbackTrans;

          Raise;
      end;
  end;
end;

procedure TfrmProdutoWeb.AlteraStatusWeb();
begin
  inherited;

  if (qryMaster.Active = False) then
      Exit;

  qryMaster.Edit;

  if (qryMastercFlgProdWeb.Value = 0) then
  begin
      if (MessageDLG('Deseja ativar o produto ' + #13 + qryMastercNmProduto.Value + ' na Web ?',mtConfirmation,[mbYes,mbNo],0) =  mrYes) then
      begin
          qryMastercFlgProdWeb.Value  := 1;
          edtStatusWeb.Text           := 'PRODUTO ATIVO';

          btAtivaProduto.Visible    := True;
          btDesativaProduto.Visible := False;

          ShowMessage('Produto ativado na Web.' + #13 + 'Clique em Salvar para efetivar a altera��o.');
          Exit;
      end;
  end
  else
  begin
      if (MessageDLG('Deseja desativar o produto ' + #13 + qryMastercNmProduto.Value + ' da Web ?',mtConfirmation,[mbYes,mbNo],0) =  mrYes) then
      begin
          qryMastercFlgProdWeb.Value  := 0;
          edtStatusWeb.Text           := 'PRODUTO INATIVO';

          btAtivaProduto.Visible    := False;
          btDesativaProduto.Visible := True;

          ShowMessage('Produto desativado da Web.' + #13 + 'Clique em Salvar para efetivar a altera��o.');
          Exit;
      end;
  end;
end;

procedure TfrmProdutoWeb.btAtivaProdutoClick(Sender: TObject);
begin
  inherited;

  AlteraStatusWeb();
end;

procedure TfrmProdutoWeb.btDesativaProdutoClick(Sender: TObject);
begin
  inherited;

  AlteraStatusWeb();
end;

procedure TfrmProdutoWeb.btAplicaDimensaoClick(Sender: TObject);
var
  objForm : TfrmProdutoWeb_AplicaDimensaoProduto;
begin
  inherited;

  if (qryMaster.IsEmpty) then
      Exit;

  objForm := TfrmProdutoWeb_AplicaDimensaoProduto.Create(nil);
  objForm.nCdProdutoPai := qryMasternCdProduto.Value;
  objForm.ShowModal;

  PosicionaQuery(qryProdutoFinal,qryMasternCdProduto.AsString);

  qryProdutoFinal.First;

  FreeAndNil(objForm);
end;

initialization
    RegisterClass(TfrmProdutoWeb);

end.

