unit fColunaGrupoTotalCC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, DBGridEhGrouping, GridsEh, DBGridEh, cxPC, cxControls, StdCtrls,
  Mask, DBCtrls;

type
  TfrmColunaGrupoTotalCC = class(TfrmProcesso_Padrao)
    qryColunaGrupoTotalCC: TADOQuery;
    qryCentroCusto: TADOQuery;
    qryCentroCustoColunaGrupoTotalCC: TADOQuery;
    qryCentroCustoColunaGrupoTotalCCnCdCentroCustoColunaGrupoTotalCC: TIntegerField;
    qryCentroCustoColunaGrupoTotalCCnCdColunaGrupoTotalCC: TIntegerField;
    qryCentroCustoColunaGrupoTotalCCnCdCC: TIntegerField;
    qryColunaGrupoTotalCCnCdColunaGrupoTotalCC: TIntegerField;
    qryColunaGrupoTotalCCnCdGrupoTotalCC: TIntegerField;
    qryColunaGrupoTotalCCcNmColunaGrupoTotalCC: TStringField;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocNmCC: TStringField;
    qryCentroCustoColunaGrupoTotalCCcNmCC: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    dsCentroCustoColunaGrupoTotalCC: TDataSource;
    qryVerificaCC: TADOQuery;
    qryExcluirDpCC: TADOQuery;
    qryVerificaCCcNmGrupoTotalCC: TStringField;
    qryVerificaCCcNmCC: TStringField;
    qryVerificaCCcNmColunaGrupoTotalCC: TStringField;
    procedure qryColunaGrupoTotalCCAfterScroll(DataSet: TDataSet);
    procedure qryCentroCustoColunaGrupoTotalCCCalcFields(
      DataSet: TDataSet);
    procedure ToolButton1Click(Sender: TObject);
    procedure qryColunaGrupoTotalCCBeforePost(DataSet: TDataSet);
    procedure qryCentroCustoColunaGrupoTotalCCBeforePost(
      DataSet: TDataSet);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    nCdColunaGrupoTotalCC : integer;
  public
    { Public declarations }
    nCdGrupoTotalCC : integer;
    procedure RegistroTotalizador (nCdColunaGrupoTotalCCAux, iComando : integer); {0-Consulta; 1-Insere; 2-Remove}
  end;

var
  frmColunaGrupoTotalCC: TfrmColunaGrupoTotalCC;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

{ TfrmColunaGrupoTotalCC }

procedure TfrmColunaGrupoTotalCC.RegistroTotalizador(nCdColunaGrupoTotalCCAux,
  iComando: integer);
begin

    nCdColunaGrupoTotalCC := nCdColunaGrupoTotalCCAux;

    {0-Consulta; 1-Insere; 2-Remove}
    if (iComando = 0) then
    begin
        qryColunaGrupoTotalCC.Close;
        PosicionaQuery(qryColunaGrupoTotalCC, IntToStr(nCdColunaGrupoTotalCC));

        Self.ShowModal;
    end else
    if (iComando = 1) then
    begin
        qryColunaGrupoTotalCC.Open;
        qryColunaGrupoTotalCC.Insert;

        Self.ShowModal;
    end else
    if (iComando = 2) then
    begin
        qryColunaGrupoTotalCC.Close;
        PosicionaQuery(qryColunaGrupoTotalCC, IntToStr(nCdColunaGrupoTotalCC));

        if (MessageDLG('Deseja realmente remover o Totalizador "' + qryColunaGrupoTotalCCcNmColunaGrupoTotalCC.Value + '", esta a��o n�o poder� ser desfeita. Confirma ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
            exit;

        qryCentroCustoColunaGrupoTotalCC.Close;
        PosicionaQuery(qryCentroCustoColunaGrupoTotalCC, qryColunaGrupoTotalCCnCdColunaGrupoTotalCC.AsString);

        if (qryCentroCustoColunaGrupoTotalCC.RecordCount > 0) then
        begin
            qryExcluirDpCC.Parameters.ParamByName('nCdColunaGrupoTotalCC').Value := nCdColunaGrupoTotalCCAux;
            qryExcluirDpCC.ExecSQL;
        end;

        qryColunaGrupoTotalCC.Delete;

        qryCentroCustoColunaGrupoTotalCC.UpdateBatch;
        qryColunaGrupoTotalCC.UpdateBatch;
    end;

end;

procedure TfrmColunaGrupoTotalCC.qryColunaGrupoTotalCCAfterScroll(
  DataSet: TDataSet);
begin
  inherited;

  qryCentroCustoColunaGrupoTotalCC.Close;
  PosicionaQuery(qryCentroCustoColunaGrupoTotalCC, IntToStr(nCdColunaGrupoTotalCC));
end;

procedure TfrmColunaGrupoTotalCC.qryCentroCustoColunaGrupoTotalCCCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryCentroCusto.Close;
  PosicionaQuery(qryCentroCusto,qryCentroCustoColunaGrupoTotalCCnCdCC.AsString);

  qryCentroCustoColunaGrupoTotalCCcNmCC.Value := qryCentroCustocNmCC.Value;
end;

procedure TfrmColunaGrupoTotalCC.ToolButton1Click(Sender: TObject);
begin
  inherited;

  if (qryColunaGrupoTotalCC.State <> dsBrowse) then
      qryColunaGrupoTotalCC.Post;

  if(Trim(qryColunaGrupoTotalCCcNmColunaGrupoTotalCC.Value) = '') then
  begin
      MensagemAlerta('Digite a Descri��o do Totalizador.');
      DBEdit1.SetFocus;
      abort;
  end;

  Close;
  
end;

procedure TfrmColunaGrupoTotalCC.qryColunaGrupoTotalCCBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if (qryColunaGrupoTotalCCnCdColunaGrupoTotalCC.Value = 0) then
  begin
      qryColunaGrupoTotalCCnCdColunaGrupoTotalCC.Value := frmMenu.fnProximoCodigo('COLUNAGRUPOTOTALCC');
      qryColunaGrupoTotalCCnCdGrupoTotalCC.Value       := nCdGrupoTotalCC;
  end;
end;

procedure TfrmColunaGrupoTotalCC.qryCentroCustoColunaGrupoTotalCCBeforePost(
  DataSet: TDataSet);
begin
  inherited;

  if((qryColunaGrupoTotalCC.State = dsInsert) or (qryColunaGrupoTotalCC.State = dsEdit)) then
      qryColunaGrupoTotalCC.Post;

  if (qryCentroCustoColunaGrupoTotalCCcNmCC.Value = '') then
  begin
      MensagemAlerta('Centro de Custo inv�lido.');
      abort;
  end;

  qryVerificaCC.Close;
  qryVerificaCC.Parameters.ParamByName('nCdColunaGrupoTotalCC').Value := nCdColunaGrupoTotalCC;
  qryVerificaCC.Parameters.ParamByName('nCdGrupoTotalCC').Value       := qryColunaGrupoTotalCCnCdGrupoTotalCC.Value;
  qryVerificaCC.Parameters.ParamByName('nCdCC').Value                 := qryCentroCustoColunaGrupoTotalCCnCdCC.Value;
  qryVerificaCC.Open;

  if (qryVerificaCC.RecordCount > 0) then
  begin
      MensagemAlerta('N�o � possivel inserir o Centro de Custo '
                   + qryVerificaCCcNmCC.Value
                   + ' pois ele j� pertence ao Totalizador '
                   + qryVerificaCCcNmColunaGrupoTotalCC.Value + '.');
      abort;
  end;

  if (qryCentroCustoColunaGrupoTotalCCnCdCentroCustoColunaGrupoTotalCC.Value = 0) then
  begin
      qryCentroCustoColunaGrupoTotalCCnCdCentroCustoColunaGrupoTotalCC.Value := frmMenu.fnProximoCodigo('CENTROCUSTOCOLUNAGRUPOTOTALCC');
      qryCentroCustoColunaGrupoTotalCCnCdColunaGrupoTotalCC.Value            := qryColunaGrupoTotalCCnCdColunaGrupoTotalCC.Value;
  end;

end;

procedure TfrmColunaGrupoTotalCC.FormKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmColunaGrupoTotalCC.DBGridEh1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  nPK : integer;
begin
  inherited;

  if not (qryCentroCustoColunaGrupoTotalCC.Active) then
      exit;

  if (Key = vk_F4) then
  begin
      if (qryCentroCustoColunaGrupoTotalCC.State <> dsEdit) then
          qryCentroCustoColunaGrupoTotalCC.Edit;

      if (DBGridEh1.SelectedField.FieldName = 'nCdCC') then
      begin
          nPK := frmLookup_Padrao.ExecutaConsulta2(28,'iNivel = 3');

          If (nPK > 0) then
          begin
              qryCentroCustoColunaGrupoTotalCCnCdCC.Value := nPK ;
          end ;
      end;
  end;
end;

procedure TfrmColunaGrupoTotalCC.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

  {inherited;}

end;

procedure TfrmColunaGrupoTotalCC.DBEdit1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;

  DBGridEh1.SetFocus;
end;

end.
