program ER2Soft;

{%File '..\..\Program Files\EXTRAFIL\DELPHI7\LIB\xtrafil.bpl'}
{%ToDo 'ER2Soft.todo'}

uses
  Forms,
  fCadastro_Template in 'fCadastro_Template.pas' {frmCadastro_Padrao},
  fMenu in 'fMenu.pas' {frmMenu},
  fConsulta_Template in 'fConsulta_Template.pas' {frmConsulta_Template},
  fAplicacao_CONS in 'fAplicacao_CONS.pas' {frmAplicacao_CONS},
  fAplicacao_CAD in 'fAplicacao_CAD.pas' {frmAplicacao_Cad},
  fLookup_Padrao in 'fLookup_Padrao.pas' {frmLookup_Padrao},
  fBuscaPK in 'fBuscaPK.pas' {frmBuscaPK},
  fAuditoria in 'fAuditoria.pas' {frmAuditoria},
  fMenu_Cad in 'fMenu_Cad.pas' {frmMenu_Cad},
  fLogin in 'fLogin.pas' {frmLogin},
  fUsuario in 'fUsuario.pas' {frmUsuario},
  fGrupoUsuario in 'fGrupoUsuario.pas' {frmGrupoUsuario_Cad},
  fTabelaConsulta in 'fTabelaConsulta.pas' {frmTabelaConsulta},
  fSelecLoja in 'fSelecLoja.pas' {frmSelecLoja},
  fGrupoEspTit in 'fGrupoEspTit.pas' {frmGrupoEspTit},
  fEspTit in 'fEspTit.pas' {frmEspTit},
  fTipoSP in 'fTipoSP.pas' {frmTipoSP},
  fGrupoCategFinanc in 'fGrupoCategFinanc.pas' {frmGrupoCategFinanc},
  fCategFinanc in 'fCategFinanc.pas' {frmCategFinanc},
  fUnidadeNegocio in 'fUnidadeNegocio.pas' {frmUnidadeNegocio},
  fFormaPagto in 'fFormaPagto.pas' {frmFormaPagto},
  fMoeda in 'fMoeda.pas' {frmMoeda},
  fBanco in 'fBanco.pas' {frmBanco},
  fPais in 'fPais.pas' {frmPais},
  fEmpresa in 'fEmpresa.pas' {frmEmpresa},
  fRegiao in 'fRegiao.pas' {frmRegiao},
  fCentroCusto in 'fCentroCusto.pas' {frmCentroCusto},
  fArvoreCC in 'fArvoreCC.pas' {frmArvoreCC},
  fOperacao in 'fOperacao.pas' {frmOperacao},
  fSP in 'fSP.pas' {frmSP},
  rImpSP in 'rImpSP.pas' {rptImpSP},
  fAltVencTit in 'fAltVencTit.pas' {frmAltVencTit},
  fSaldoInicialContaBancaria in 'fSaldoInicialContaBancaria.pas' {frmSaldoInicialContaBancaria},
  fAutorizaSP in 'fAutorizaSP.pas' {frmAutorizaSP},
  fTituloProv in 'fTituloProv.pas' {frmTituloProv},
  rPerformanceProdutoV2 in 'rPerformanceProdutoV2.pas' {rptPerformanceProdutoV2},
  rFluxoCaixa in 'rFluxoCaixa.pas' {rptFluxoCaixa},
  rFluxoCaixaRealizado_view in 'rFluxoCaixaRealizado_view.pas' {rptFluxoCaixaRealizado_view},
  fPlanoConta in 'fPlanoConta.pas' {frmPlanoConta},
  fProcesso_Padrao in 'fProcesso_Padrao.pas' {frmProcesso_Padrao},
  fEstornaContab in 'fEstornaContab.pas' {frmEstornaContab},
  DecisionCubeBugWorkaround in 'DecisionCubeBugWorkaround.pas',
  dcVendaProduto in 'dcVendaProduto.pas' {dcmVendaProduto},
  dcMaiorCredor in 'dcMaiorCredor.pas' {dcmMaiorCredor},
  fConsTituloAbertoWERP in 'fConsTituloAbertoWERP.pas' {frmConsTituloAbertoWERP},
  rPerfOrcamento_view in 'rPerfOrcamento_view.pas' {rptPerfOrcamento_view},
  fAltSenha in 'fAltSenha.pas' {frmAltSenha},
  rMovTit in 'rMovTit.pas' {rptMovTit},
  rTransferencia_view in 'rTransferencia_view.pas' {rptTransferencia_view},
  fContabiliza in 'fContabiliza.pas' {frmContabiliza},
  rVendaFatDia in 'rVendaFatDia.pas' {rptVendaFatDia},
  rRankingFatAnualProduto_view in 'rRankingFatAnualProduto_view.pas' {rptRankingFatAnualProduto_view},
  fBaixaColetiva in 'fBaixaColetiva.pas' {frmBaixaColetiva},
  fProdutoFormulado in 'fProdutoFormulado.pas' {frmProdutoFormulado},
  fGerarGrade in 'fGerarGrade.pas' {frmGerarGrade},
  fDepartamento in 'fDepartamento.pas' {frmDepartamento},
  fCorPredom in 'fCorPredom.pas' {frmCorPredom},
  fConsultaChequeResponsavel in 'fConsultaChequeResponsavel.pas' {frmConsultaChequeResponsavel},
  fSugerirCheques in 'fSugerirCheques.pas' {frmSugerirCheques},
  fSelecServer in 'fSelecServer.pas' {frmSelecServer},
  fAddServer in 'fAddServer.pas' {frmAddServer},
  fMarca in 'fMarca.pas' {frmMarca},
  fTipoPedido in 'fTipoPedido.pas' {frmTipoPedido},
  fOrcamentoEnergy in 'fOrcamentoEnergy.pas' {frmOrcamentoEnergy},
  fGrade in 'fGrade.pas' {frmGrade},
  rPerfOrcamento in 'rPerfOrcamento.pas' {rptPerfOrcamento},
  fIncLinha in 'fIncLinha.pas' {frmIncLinha},
  rRomaneio in 'rRomaneio.pas' {rptRomaneio},
  fCheque in 'fCheque.pas' {frmCheque},
  fChequeDevolvido in 'fChequeDevolvido.pas' {frmChequeDevolvido},
  fGrupoProduto in 'fGrupoProduto.pas' {frmGrupoProduto},
  fAutoriza_Pedido in 'fAutoriza_Pedido.pas' {frmAutoriza_Pedido},
  fPosTituloAberto_Cliente in 'fPosTituloAberto_Cliente.pas' {frmPosTituloAberto_Cliente},
  rPosTituloAberto_Cliente in 'rPosTituloAberto_Cliente.pas' {rptPosTituloAberto_cliente},
  fFollowUPWERP in 'fFollowUPWERP.pas' {frmFollowUPWERP},
  fNovo_FollowUp in 'fNovo_FollowUp.pas' {frmNovo_FollowUp},
  fManutPedido in 'fManutPedido.pas' {frmManutPedido},
  fProdutoERP in 'fProdutoERP.pas' {frmProdutoERP},
  fEmbFormula in 'fEmbFormula.pas' {frmEmbFormula},
  fParam_TipoPedido in 'fParam_TipoPedido.pas' {frmParam_TipoPedido},
  fGrupoImposto in 'fGrupoImposto.pas' {frmGrupoImposto},
  fNaturezaOperacao in 'fNaturezaOperacao.pas' {frmNaturezaOperacao},
  fSerieFiscal in 'fSerieFiscal.pas' {frmSerieFiscal},
  fRegraICMS in 'fRegraICMS.pas' {frmRegraICMS},
  fGeraFaturamento in 'fGeraFaturamento.pas' {frmGeraFaturamento},
  fItemFaturar in 'fItemFaturar.pas' {frmItemFaturar},
  fNC in 'fNC.pas' {frmNC},
  fLiberaFaturamento in 'fLiberaFaturamento.pas' {frmLiberaFaturamento},
  fImpDoctoFiscal in 'fImpDoctoFiscal.pas' {frmImpDoctoFiscal},
  fDadoComplDoctoFiscal in 'fDadoComplDoctoFiscal.pas' {frmDadoComplDoctoFiscal},
  fOperacaoEstoque in 'fOperacaoEstoque.pas' {frmOperacaoEstoque},
  fGrupoEstoque in 'fGrupoEstoque.pas' {frmGrupoEstoque},
  fLocalEstoque in 'fLocalEstoque.pas' {frmLocalEstoque},
  fOperacaoLocalEstoque in 'fOperacaoLocalEstoque.pas' {frmOperacaoLocalEstoque},
  fRecebimentoCompleto in 'fRecebimentoCompleto.pas' {frmRecebimentoCompleto},
  fConsultaItemPedidoAberto in 'fConsultaItemPedidoAberto.pas' {frmConsultaItemPedidoAberto},
  fTerceiro in 'fTerceiro.pas' {frmTerceiro},
  fCancDoctoFiscal in 'fCancDoctoFiscal.pas' {frmCancDoctoFiscal},
  fConfigAmbiente in 'fConfigAmbiente.pas' {frmConfigAmbiente},
  fModImpDF in 'fModImpDF.pas' {frmModImpDF},
  fImpBOLPadrao in 'fImpBOLPadrao.pas' {frmImpBOLPadrao},
  fPrecoEspPedCom in 'fPrecoEspPedCom.pas' {frmPrecoEspPedCom},
  rMovTit_view in 'rMovTit_view.pas' {rptMovTit_view},
  fLibDivRec in 'fLibDivRec.pas' {frmLibDivRec},
  fGrupoEconomico in 'fGrupoEconomico.pas' {frmGrupoEconomico},
  rChequeRecebido in 'rChequeRecebido.pas' {rptChequeRecebido},
  rRequisicao_view in 'rRequisicao_view.pas' {rptRequisicao_view},
  rRankingVendaCliente in 'rRankingVendaCliente.pas' {rptRankingVendaCliente},
  rRankingVendaCliente_view in 'rRankingVendaCliente_view.pas' {rptRankingVendaCliente_view},
  rEvolucaoVenda in 'rEvolucaoVenda.pas' {rptEvolucaoVenda},
  fNovaReferencia in 'fNovaReferencia.pas' {frmNovaReferencia},
  rVendaPeriodoProduto_view in 'rVendaPeriodoProduto_view.pas' {rptVendaPeriodoProduto_view},
  rFaturamentoPeriodo in 'rFaturamentoPeriodo.pas' {rptFaturamentoPeriodo},
  rFaturamentoPeriodo_view in 'rFaturamentoPeriodo_view.pas' {rptFaturamentoPeriodo_view},
  rFaturamentoPeriodoProduto in 'rFaturamentoPeriodoProduto.pas' {rptFaturamentoPeriodoProduto},
  rFaturamentoPeriodoProduto_view in 'rFaturamentoPeriodoProduto_view.pas' {rptFaturamentoPeriodoProduto_view},
  rVendaPeriodo in 'rVendaPeriodo.pas' {rptVendaPeriodo},
  rEvolucaoVenda_view in 'rEvolucaoVenda_view.pas' {rptEvolucaoVenda_view},
  fPedCompraContrato in 'fPedCompraContrato.pas' {frmPedCompraContrato},
  rPedCom_Contrato in 'rPedCom_Contrato.pas' {rptPedCom_Contrato},
  fDoctoFiscal in 'fDoctoFiscal.pas' {frmDoctoFiscal},
  fMensagemPadrao in 'fMensagemPadrao.pas',
  rVendaPeriodo_view in 'rVendaPeriodo_view.pas' {rptVendaPeriodo_view},
  fConfirmaReceb in 'fConfirmaReceb.pas' {frmConfirmaReceb},
  fClasseProduto in 'fClasseProduto.pas' {frmClasseProduto},
  fColecaoProduto in 'fColecaoProduto.pas' {frmColecaoProduto},
  fCondPagtoPDV in 'fCondPagtoPDV.pas' {frmCondPagtoPDV},
  fConsultaMovEstoque in 'fConsultaMovEstoque.pas' {frmConsultaMovEstoque},
  fTipoReceb in 'fTipoReceb.pas' {frmTipoReceb},
  fConsultaPosicaoEstoque in 'fConsultaPosicaoEstoque.pas' {frmConsultaPosicaoEstoque},
  fTipoDiverg in 'fTipoDiverg.pas' {frmTipoDiverg},
  fMotBloqPed in 'fMotBloqPed.pas' {frmMotBloqPed},
  fSelecMotBloqPed in 'fSelecMotBloqPed.pas' {frmSelecMotBloqPed},
  dcPlanoConta in 'dcPlanoConta.pas' {dcmPlanoConta},
  rVendaPeriodoProduto in 'rVendaPeriodoProduto.pas' {rptVendaPeriodoProduto},
  dcCompraCentroCusto in 'dcCompraCentroCusto.pas' {dcmCompraCentroCusto},
  dcCompraCentroCusto_view in 'dcCompraCentroCusto_view.pas' {dcmCompraCentroCusto_view},
  fLibFinRec in 'fLibFinRec.pas' {frmLibFinRec},
  fLibFinRec_Compl in 'fLibFinRec_Compl.pas' {frmLibFinRec_Compl},
  fGrupoPedido in 'fGrupoPedido.pas' {frmGrupoPedido},
  dcAnaliseLucratividade in 'dcAnaliseLucratividade.pas' {dcmAnaliseLucratividade},
  dcAnaliseLucratividade_view in 'dcAnaliseLucratividade_view.pas' {dcmAnaliseLucratividade_view},
  dcCompraRepresPeriodo in 'dcCompraRepresPeriodo.pas' {dcmCompraRepresPeriodo},
  dcCompraRepresPeriodo_view in 'dcCompraRepresPeriodo_view.pas' {dcmCompraRepresPeriodo_view},
  dcCompraFornecPeriodo in 'dcCompraFornecPeriodo.pas' {dcmCompraFornecPeriodo},
  dcCompraFornecPeriodo_view in 'dcCompraFornecPeriodo_view.pas' {dcmCompraFornecPeriodo_view},
  fCarteiraFinancAutorPed in 'fCarteiraFinancAutorPed.pas' {frmCarteiraFinancAutorPed},
  fModImpBOL in 'fModImpBOL.pas' {frmModImpBOL},
  fImpDFPadrao in 'fImpDFPadrao.pas' {frmImpDFPadrao},
  fTabPreco in 'fTabPreco.pas' {frmTabPreco},
  fTransfEstManual in 'fTransfEstManual.pas' {frmTransfEstManual},
  fConsultaChequeEmitido in 'fConsultaChequeEmitido.pas' {frmConsultaChequeEmitido},
  fBaixaChequePre in 'fBaixaChequePre.pas' {frmBaixaChequePre},
  fContratoFinanc in 'fContratoFinanc.pas' {frmContratoFinanc},
  fParametro in 'fParametro.pas' {frmParametro},
  fRastreaLoteProduto in 'fRastreaLoteProduto.pas' {frmRastreaLoteProduto},
  fMonitorProducao in 'fMonitorProducao.pas' {frmMonitorProducao},
  fLimiteCredCliente in 'fLimiteCredCliente.pas' {frmLimiteCredCliente},
  fSelecEmpresa in 'fSelecEmpresa.pas' {frmSelecEmpresa},
  fPdv in 'fPdv.pas' {frmPDV},
  fPdvFormaPagto in 'fPdvFormaPagto.pas' {frmPdvFormaPagto},
  fPdvCondPagto in 'fPdvCondPagto.pas' {frmPdvCondPagto},
  fTitulo in 'fTitulo.pas' {frmTitulo},
  fConvTitSP in 'fConvTitSP.pas' {frmConvTitSP},
  fDoctoFiscalManual in 'fDoctoFiscalManual.pas' {frmDoctoFiscalManual},
  fRequisicao in 'fRequisicao.pas' {frmRequisicao},
  rMapaCompra in 'rMapaCompra.pas' {rptMapaCompra},
  fAutoriza_Requisicao in 'fAutoriza_Requisicao.pas' {frmAutoriza_Requisicao},
  fAtendimentoRequisicao in 'fAtendimentoRequisicao.pas' {frmAtendimentoRequisicao},
  fAtendRequisicao_Local in 'fAtendRequisicao_Local.pas' {frmAtendRequisicao_Local},
  fTipoRequisicao in 'fTipoRequisicao.pas' {frmTipoRequisicao},
  fSetor in 'fSetor.pas' {frmSetor},
  fCaixa_Recebimento in 'fCaixa_Recebimento.pas' {frmCaixa_Recebimento},
  fCaixa_Cheques in 'fCaixa_Cheques.pas' {frmCaixa_Cheques},
  fCaixa_PagtoCheque in 'fCaixa_PagtoCheque.pas' {frmCaixa_PagtoCheque},
  fCaixa_ListaParcelas in 'fCaixa_ListaParcelas.pas' {frmCaixa_ListaParcelas},
  fCaixa_EntradaDin in 'fCaixa_EntradaDin.pas' {frmCaixa_EntradaDin},
  rBorderoCaixa in 'rBorderoCaixa.pas' {rptBorderoCaixa},
  rRemessaNum_view in 'rRemessaNum_view.pas' {rptRemessaNum_view: TQuickRep},
  fCaixa_Fechamento in 'fCaixa_Fechamento.pas' {frmCaixa_Fechamento},
  fRemessa_Numerario in 'fRemessa_Numerario.pas' {frmRemessa_Numerarios},
  rBorderoCaixa_view in 'rBorderoCaixa_view.pas' {rptBorderoCaixa_view: TQuickRep},
  fInventario_Abertura in 'fInventario_Abertura.pas' {frmInventario_Abertura},
  rExtratoContaBancaria_view in 'rExtratoContaBancaria_view.pas' {rptExtratoContaBancaria_view: TQuickRep},
  fInventario_Fechamento in 'fInventario_Fechamento.pas' {frmInventario_Fechamento},
  fInventario_Digitacao in 'fInventario_Digitacao.pas' {frmInventario_Digitacao},
  rInventario_Resumo in 'rInventario_Resumo.pas' {rptInventario_Resumo: TQuickRep},
  fConfirma_Numerarios in 'fConfirma_Numerarios.pas' {frmConfirma_Numerarios},
  fSaldoEstoqueInicial in 'fSaldoEstoqueInicial.pas' {frmSaldoEstoqueInicial},
  fCaixa_EstornoDia in 'fCaixa_EstornoDia.pas' {frmCaixa_EstornoDia},
  fTipoLancto in 'fTipoLancto.pas' {frmTipoLancto},
  fContaCaixaCofre in 'fContaCaixaCofre.pas' {frmContaCaixaCofre},
  fConsNecessidadeCompra in 'fConsNecessidadeCompra.pas' {frmConsNecessidadeCompra},
  fMapaConcorrencia_Fornecedor in 'fMapaConcorrencia_Fornecedor.pas' {frmMapaConcorrencia_Fornecedor},
  fAprovacaoMapaCompra in 'fAprovacaoMapaCompra.pas' {frmAprovacaoMapaCompra},
  fGeraPedidoCompraAutom in 'fGeraPedidoCompraAutom.pas' {frmGeraPedidoCompraAutom},
  rVendaFatDia_view in 'rVendaFatDia_view.pas' {rptVendaFatDia_view},
  fMapaCompra in 'fMapaCompra.pas' {frmMapaCompra},
  fPosTituloLiquidado in 'fPosTituloLiquidado.pas' {frmPosTituloLiquidado},
  rPosTituloLiquidado in 'rPosTituloLiquidado.pas' {rptPosTituloLiquidado},
  fGerarMapaConcorrencia in 'fGerarMapaConcorrencia.pas' {frmGerarMapaConcorrencia},
  fPedidoAbertoProduto in 'fPedidoAbertoProduto.pas' {frmPedidoAbertoProduto},
  fConsComprasProduto in 'fConsComprasProduto.pas' {frmConsComprasProduto},
  fSyncInfra in 'fSyncInfra.pas' {frmSyncInfra},
  fOperManEstoque in 'fOperManEstoque.pas' {frmOperManEstoque},
  fMonitorOrcamento in 'fMonitorOrcamento.pas' {frmMonitorOrcamento},
  fMonitorOrcamento_Itens in 'fMonitorOrcamento_Itens.pas' {frmMonitorOrcamento_Itens},
  fMonitorOrcamento_GerarPedido in 'fMonitorOrcamento_GerarPedido.pas' {frmMonitorOrcamento_GerarPedido},
  fConsultaCheque in 'fConsultaCheque.pas' {frmConsultaCheque},
  fCondPagto in 'fCondPagto.pas' {frmCondPagto},
  fCaixa_MotivoEstorno in 'fCaixa_MotivoEstorno.pas' {frmCaixa_MotivoEstorno},
  fCaixa_StatusDocto in 'fCaixa_StatusDocto.pas' {frmCaixa_StatusDocto},
  fMRP_FornecHomol in 'fMRP_FornecHomol.pas' {frmMRP_FornecHomol},
  fMRP_FormulaProd in 'fMRP_FormulaProd.pas' {frmMRP_FormulaProd},
  fMRP_GeraPedido in 'fMRP_GeraPedido.pas' {frmMRP_GeraPedido},
  fMRP_DetalheEstoqueAtual in 'fMRP_DetalheEstoqueAtual.pas' {frmMRP_DetalheEstoqueAtual},
  fMRP_BalancoFornecedores in 'fMRP_BalancoFornecedores.pas' {frmMRP_BalancoFornecedores},
  fOrdemCompraVarejo in 'fOrdemCompraVarejo.pas' {frmOrdemCompraVarejo},
  rPedidoDevCompra in 'rPedidoDevCompra.pas' {rptPedidoDevCompra},
  fMRP_UltimosPedidos in 'fMRP_UltimosPedidos.pas' {frmMRP_UltimosPedidos},
  fApropAdiantamento in 'fApropAdiantamento.pas' {frmApropAdiantamento},
  fApropAdiantamento_dados in 'fApropAdiantamento_dados.pas' {frmApropAdiantamento_dados},
  fExploraCaixaCofre in 'fExploraCaixaCofre.pas' {frmExploraCaixaCofre},
  fMovTitulo in 'fMovTitulo.pas' {frmMovTitulo},
  fFuncoesECF in 'fFuncoesECF.pas' {frmFuncoesECF},
  fAlaProducao in 'fAlaProducao.pas' {frmAlaProducao},
  fVinculoProdutoAla in 'fVinculoProdutoAla.pas' {frmVinculoProdutoAla},
  rOrcamentoEnergy in 'rOrcamentoEnergy.pas' {rptOrcamentoEnergy},
  fItemPedidoCompraAtendido in 'fItemPedidoCompraAtendido.pas' {frmItemPedidoCompraAtendido},
  fSplash in 'fSplash.pas' {SplashScreen},
  fItemPedidoAtendido in 'fItemPedidoAtendido.pas' {frmItemPedidoAtendido},
  fPedidoVendaWERP in 'fPedidoVendaWERP.pas' {frmPedidoVendaWERP},
  fAutoriza_devolucao in 'fAutoriza_devolucao.pas' {frmAutoriza_devolucao},
  rPedidoPendFat in 'rPedidoPendFat.pas' {rptPedidoPendFat},
  rPedidoPendFat_view in 'rPedidoPendFat_view.pas' {rptPedidoPendFat_view},
  fRecebimento_Divergencias in 'fRecebimento_Divergencias.pas' {frmRecebimento_Divergencias},
  rPedidoVenda in 'rPedidoVenda.pas' {rptPedidoVenda},
  fTitPagarAguardDoc in 'fTitPagarAguardDoc.pas' {frmTitPagarAguardDoc},
  fErroPadrao in 'fErroPadrao.pas' {frmErroPadrao},
  fPlanoContaNovo in 'fPlanoContaNovo.pas' {frmPlanoContaNovo},
  rRankingFatAnualProdutoWERP in 'rRankingFatAnualProdutoWERP.pas' {rptRankingFatAnualProdutoWERP},
  rPlanilhaCobranca_view in 'rPlanilhaCobranca_view.pas' {rptPlanilhaCobranca_view},
  rDRE in 'rDRE.pas' {rptDRE},
  rDRE_view in 'rDRE_view.pas' {rptDRE_View},
  rRankingFatMensal in 'rRankingFatMensal.pas' {rptRankingFatMensal},
  rRankingFatMensal_view in 'rRankingFatMensal_view.pas' {rptRankingFatMensal_view},
  rFaturamentoAnual in 'rFaturamentoAnual.pas' {rptFaturamentoAnual},
  rFaturamentoAnual_view in 'rFaturamentoAnual_view.pas' {rptFaturamentoAnual_view},
  rRankingFatAnual in 'rRankingFatAnual.pas' {rptRankingFatAnual},
  rRankingFatAnual_view in 'rRankingFatAnual_view.pas' {rptRankingFatAnual_view},
  fPlanilhaCobranca in 'fPlanilhaCobranca.pas' {frmPlanilhaCobranca},
  fPlanilhaCobranca_Ocorrencias in 'fPlanilhaCobranca_Ocorrencias.pas' {frmPlanilhaCobranca_Ocorrencias},
  rRelPedidoVenda_view in 'rRelPedidoVenda_view.pas' {rptRelPedidoVenda_view},
  fLibDivRec_PrecoVenda in 'fLibDivRec_PrecoVenda.pas' {frmLibDivRec_PrecoVenda},
  fDepartamento_Segmento in 'fDepartamento_Segmento.pas' {frmDepartamento_Segmento},
  fDepartamento_SubCategoria in 'fDepartamento_SubCategoria.pas' {frmDepartamento_SubCategoria},
  fDepartamento_Marca in 'fDepartamento_Marca.pas' {frmDepartamento_Marca},
  fPedidoDevCompra in 'fPedidoDevCompra.pas' {frmPedidoDevCompra},
  fMetaVenda in 'fMetaVenda.pas' {frmMetaVenda},
  fOrdemCompraVarejo_GradeAberta in 'fOrdemCompraVarejo_GradeAberta.pas' {frmOrdemCompraVarejo_GradeAberta},
  fOrcamento in 'fOrcamento.pas' {frmOrcamento},
  rOrcamento in 'rOrcamento.pas' {rptOrcamento},
  fConsultaRequisAberta in 'fConsultaRequisAberta.pas' {frmConsultaRequisAberta},
  fConsultaRequisAberta_Itens in 'fConsultaRequisAberta_Itens.pas' {frmConsultaRequisAberta_Itens},
  rRecebimento_view in 'rRecebimento_view.pas' {rptRecebimento_view},
  fTransfEst_Recebimento in 'fTransfEst_Recebimento.pas' {frmTransfEst_Recebimento},
  fTransfEst_Recebimento_Itens in 'fTransfEst_Recebimento_Itens.pas' {frmTransfEst_Recebimento_Itens},
  fTransfEst_Recebimento_Processo in 'fTransfEst_Recebimento_Processo.pas' {frmTransfEst_Recebimento_Processo},
  fAnaliseQualidade in 'fAnaliseQualidade.pas' {frmAnaliseQualidade},
  fAnaliseQualidade_Itens in 'fAnaliseQualidade_Itens.pas' {frmAnaliseQualidade_Itens: Unit1},
  fLoja in 'fLoja.pas' {frmLoja},
  fCaixa_Dinheiro in 'fCaixa_Dinheiro.pas' {frmCaixa_Dinheiro},
  fProdutoMRP in 'fProdutoMRP.pas' {frmProdutoMRP},
  fProdutoPosicaoEstoque in 'fProdutoPosicaoEstoque.pas' {frmProdutoPosicaoEstoque},
  fProdutoFornecedor in 'fProdutoFornecedor.pas' {frmProdutoFornecedor},
  fConciliaBancoManual in 'fConciliaBancoManual.pas' {frmConciliaBancoManual},
  fConciliaBancoManual_IncluiLancto in 'fConciliaBancoManual_IncluiLancto.pas' {frmConciliaBancoManual_IncluiLancto},
  rExtratoContaBancaria in 'rExtratoContaBancaria.pas' {rptExtratoContaBancaria},
  rInventario in 'rInventario.pas' {rptInventario_view: TQuickRep},
  fCargaInicial_ChequeTerceiro in 'fCargaInicial_ChequeTerceiro.pas' {frmCargaInicial_ChequeTerceiro},
  fInventarioLoja_Abertura in 'fInventarioLoja_Abertura.pas' {frmInventarioLoja_Abertura},
  fImpBoletoNF in 'fImpBoletoNF.pas' {frmImpBoletoNF},
  fGrupoInventario in 'fGrupoInventario.pas' {frmGrupoInventario},
  fLocalizacaoProduto in 'fLocalizacaoProduto.pas' {frmLocalizacaoProduto},
  fLocalizacaoProduto_Vinculo in 'fLocalizacaoProduto_Vinculo.pas' {frmLocalizacaoProduto_Vinculo},
  fReimpressaoCarnet in 'fReimpressaoCarnet.pas' {frmReimpressaoCarnet},
  fConsultaPedComAberTerceiro in 'fConsultaPedComAberTerceiro.pas' {frmConsultaPedComAberTerceiro},
  fConsultaPedComAberTerceiro_Itens in 'fConsultaPedComAberTerceiro_Itens.pas' {frmConsultaPedComAberTerceiro_Itens},
  fQueryExecute in 'fQueryExecute.pas' {frmQueryExecute},
  fTipoAlerta in 'fTipoAlerta.pas' {frmTipoAlerta},
  fAlertas in 'fAlertas.pas' {frmAlertas},
  frmSQLWait in 'frmSQLWait.pas' {frmWait},
  fPDVItemGrade in 'fPDVItemGrade.pas' {frmPDVItemGrade},
  fProduto_Duplicar in 'fProduto_Duplicar.pas' {frmProduto_Duplicar},
  fPedidoComercial_ProgEntrega in 'fPedidoComercial_ProgEntrega.pas' {frmPedidoComercial_ProgEntrega},
  rRelPedidoVenda in 'rRelPedidoVenda.pas' {rptRelPedidoVenda},
  rPreVendaPendente_view in 'rPreVendaPendente_view.pas' {rptPreVendaPendente_view},
  rPedidoCompra in 'rPedidoCompra.pas' {rptPedidoCompra},
  rPedidoCompra_view in 'rPedidoCompra_view.pas' {rptPedidoCompra_view},
  rListaProdutoSimples in 'rListaProdutoSimples.pas' {rptListaProdutoSimples},
  rFichaKardex_view in 'rFichaKardex_view.pas' {rptFichaKardex_view},
  fOperadoraCartao in 'fOperadoraCartao.pas' {frmOperadoraCartao},
  fProdutoGradeSimples in 'fProdutoGradeSimples.pas' {frmProdutoGradeSimples},
  fGerarGradeSimples in 'fGerarGradeSimples.pas' {frmGerarGradeSimples},
  fRecebimento_CadProduto in 'fRecebimento_CadProduto.pas' {frmRecebimento_CadProduto},
  rPosicaoVale in 'rPosicaoVale.pas' {rptPosicaoVale},
  rOperacaoCartao_view in 'rOperacaoCartao_view.pas' {rptOperacaoCartao_view},
  rOperacaoCartao in 'rOperacaoCartao.pas' {rptOperacaoCartao},
  rComissaoRepres_view in 'rComissaoRepres_view.pas' {rptComissaoRepres_view},
  fMonitorMapaCompra in 'fMonitorMapaCompra.pas' {frmMonitorMapaCompra},
  fFornecedor in 'fFornecedor.pas' {frmFornecedor},
  rListaProdutoSimples_view in 'rListaProdutoSimples_view.pas' {rptListaProdutoSimples_view},
  fRamoAtividade in 'fRamoAtividade.pas' {frmRamoAtividade},
  dcVendaProdutoClienteWERP in 'dcVendaProdutoClienteWERP.pas' {dcmVendaProdutoClienteWERP},
  dcAnaliseGiroProduto_view in 'dcAnaliseGiroProduto_view.pas' {dcmAnaliseGiroProduto_view},
  rComissaoRepres in 'rComissaoRepres.pas' {rptComissaoRepres},
  rDepositoBancario_view in 'rDepositoBancario_view.pas' {rptDepositoBancario_view},
  fGrupoComissao in 'fGrupoComissao.pas' {frmGrupoComissao},
  fProvisaoPagto in 'fProvisaoPagto.pas' {frmProvisaoPagto},
  fProvisaoPagto_Titulos in 'fProvisaoPagto_Titulos.pas' {frmProvisaoPagto_Titulos},
  fProvisaoPagto_ContaBancaria in 'fProvisaoPagto_ContaBancaria.pas' {frmProvisaoPagto_ContaBancaria},
  fCor in 'fCor.pas' {frmCor},
  fMontaGrade in 'fMontaGrade.pas' {frmMontaGrade},
  fLibDivRec_Parcelas in 'fLibDivRec_Parcelas.pas' {frmLibDivRec_Parcelas},
  fConsultaItemPedidoAberto_SomenteItens in 'fConsultaItemPedidoAberto_SomenteItens.pas' {frmConsultaItemPedidoAberto_SomenteItens},
  fRecebimento_Etiqueta in 'fRecebimento_Etiqueta.pas' {frmRecebimento_Etiqueta},
  rRecebimento_Etiqueta_view in 'rRecebimento_Etiqueta_view.pas' {rptRecebimento_Etiqueta_view},
  fProvisaoPagto_DetalheProvisao in 'fProvisaoPagto_DetalheProvisao.pas' {frmProvisaoPagto_DetalheProvisao},
  fBaixaProvisao in 'fBaixaProvisao.pas' {frmBaixaProvisao},
  fBaixaProvisao_FormaPagto in 'fBaixaProvisao_FormaPagto.pas' {frmBaixaProvisao_FormaPagto},
  fBaixaProvisao_Provisoes in 'fBaixaProvisao_Provisoes.pas' {frmBaixaProvisao_Provisoes},
  fBaixaProvisao_BaixaTitulos in 'fBaixaProvisao_BaixaTitulos.pas' {frmBaixaProvisao_BaixaTitulos},
  fEstornoBaixaProvisao in 'fEstornoBaixaProvisao.pas' {frmEstornoBaixaProvisao},
  fVerbaPublicidade in 'fVerbaPublicidade.pas' {frmVerbaPublicidade},
  fMovTitulo_Abatimento in 'fMovTitulo_Abatimento.pas' {frmMovTitulo_Abatimento},
  fTransfEst_Itens in 'fTransfEst_Itens.pas' {frmTransfEst_Itens},
  fConfigCustoCompra in 'fConfigCustoCompra.pas' {frmConfigCustoCompra},
  fPedidoComercial in 'fPedidoComercial.pas' {frmPedidoComercial},
  rPedidoCom_Simples in 'rPedidoCom_Simples.pas' {rptPedidoCom_Simples},
  fTituloProv_Duplicar in 'fTituloProv_Duplicar.pas' {frmTituloProv_Duplicar},
  fCheque_TrocarProtador in 'fCheque_TrocarProtador.pas' {frmCheque_TrocarProtador},
  fDepositoBancario in 'fDepositoBancario.pas' {frmDepositoBancario},
  fDepositoBancario_SelecaoCheque in 'fDepositoBancario_SelecaoCheque.pas' {frmDepositoBancario_SelecaoCheque},
  fConciliaBancoManual__ConsultaCheques in 'fConciliaBancoManual__ConsultaCheques.pas' {frmConciliaBancoManual__ConsultaCheques},
  fDepositoBancario_ConsultaCheques in 'fDepositoBancario_ConsultaCheques.pas' {frmDepositoBancario_ConsultaCheques},
  fTabelaJuros in 'fTabelaJuros.pas' {frmTabelaJuros},
  fPosicaoCarteiraCheque in 'fPosicaoCarteiraCheque.pas' {frmPosicaoCarteiraCheque},
  fPosicaoCarteiraCheque_Cheques in 'fPosicaoCarteiraCheque_Cheques.pas' {frmPosicaoCarteiraCheque_Cheques},
  fCadastroChequeTerceiro in 'fCadastroChequeTerceiro.pas' {frmCadastroChequeTerceiro},
  fConsultaLanctoCofre in 'fConsultaLanctoCofre.pas' {frmConsultaLanctoCofre},
  fTransfConta in 'fTransfConta.pas' {frmTransfConta},
  fConfereUsuario in 'fConfereUsuario.pas' {frmSenhaAcesso},
  fAlteraVenctoCarnet in 'fAlteraVenctoCarnet.pas' {frmAlteraVenctoCarnet},
  fArvorePlanoConta in 'fArvorePlanoConta.pas' {frmArvorePlanoConta},
  fNotaFiscalRecusada_Digitacao in 'fNotaFiscalRecusada_Digitacao.pas' {frmNotaFiscalRecusada_Digitacao},
  fContaBancaria in 'fContaBancaria.pas' {frmContaBancaria},
  rFluxoCaixaRealizado in 'rFluxoCaixaRealizado.pas' {rptFluxoCaixaRealizado},
  fFluxoCaixaRealizado_View in 'fFluxoCaixaRealizado_View.pas' {frmFluxoCaixaRealizado_View},
  fFluxoCaixaRealizado_Lanctos in 'fFluxoCaixaRealizado_Lanctos.pas' {frmFluxoCaixaRealizado_Lanctos},
  rFluxoCaixa_view in 'rFluxoCaixa_view.pas' {rptFluxoCaixa_View},
  fEncerrarMovtoPOS in 'fEncerrarMovtoPOS.pas' {frmEncerrarMovtoPOS},
  fEncerrarMovtoPOS_GeraLote in 'fEncerrarMovtoPOS_GeraLote.pas' {frmEncerrarMovtoPOS_GeraLote},
  fConciliaCartao in 'fConciliaCartao.pas' {frmConciliaCartao},
  fConciliacaoBancariaManual_Ultimas in 'fConciliacaoBancariaManual_Ultimas.pas' {frmConciliacaoBancariaManual_Ultimas},
  fConciliacaoBancariaManual in 'fConciliacaoBancariaManual.pas' {frmConciliacaoBancariaManual},
  fEstornoConciliacaoCartao in 'fEstornoConciliacaoCartao.pas' {frmEstornoConciliacaoCartao},
  fConsultaVendaProduto in 'fConsultaVendaProduto.pas' {frmConsultaVendaProduto},
  fConsultaVendaProduto_Dados in 'fConsultaVendaProduto_Dados.pas' {frmConsultaVendaProduto_Dados},
  fUtils in 'fUtils.pas',
  fProdutoERPInsumos in 'fProdutoERPInsumos.pas' {frmProdutoERPInsumos},
  fGerenciaPreco in 'fGerenciaPreco.pas' {frmGerenciaPreco},
  rAvaliacaoCQM_View in 'rAvaliacaoCQM_View.pas' {rptAvaliacaoCQM_View},
  fCargaInicial_TitulosPagar in 'fCargaInicial_TitulosPagar.pas' {frmCargaInicial_TitulosPagar},
  rLoteLiqCobradora_view in 'rLoteLiqCobradora_view.pas' {rptLoteLiqCobradora_view},
  fLancManConta in 'fLancManConta.pas' {frmLancManConta},
  fConsultaSaldoConta in 'fConsultaSaldoConta.pas' {frmConsultaSaldoConta},
  fCaixa_Pendencias in 'fCaixa_Pendencias.pas' {frmCaixa_Pendencias},
  rPickList_retrato_novo in 'rPickList_retrato_novo.pas' {rptPickList_retrato_novo},
  fMRP in 'fMRP.pas' {frmMRP},
  uPDV_Bematech_Termica in 'uPDV_Bematech_Termica.pas',
  fMRP_Sugestao in 'fMRP_Sugestao.pas' {frmMRP_Sugestao},
  fCaixa_ResgateCheque in 'fCaixa_ResgateCheque.pas' {frmCaixa_ResgateCheque},
  fPedidoDevCompra_ConsultaItens in 'fPedidoDevCompra_ConsultaItens.pas' {frmPedidoDevCompra_ConsultaItens},
  fTransfEst_ItensNaoLidos in 'fTransfEst_ItensNaoLidos.pas' {frmTransfEst_ItensNaoLidos},
  fReabrePedido in 'fReabrePedido.pas' {frmReabrePedido},
  fGrupoMRP in 'fGrupoMRP.pas' {frmGrupoMRP},
  fMRP_ListaPedidosDia in 'fMRP_ListaPedidosDia.pas' {frmMRP_ListaPedidoDia},
  fMapaCompra_Itens in 'fMapaCompra_Itens.pas' {frmMapaCompra_Itens},
  fClienteVarejoPessoaFisica in 'fClienteVarejoPessoaFisica.pas' {frmClienteVarejoPessoaFisica},
  fConsultaEndereco in 'fConsultaEndereco.pas' {frmConsultaEndereco},
  fClienteVarejo in 'fClienteVarejo.pas' {frmClienteVarejo},
  fIdentificaCredito in 'fIdentificaCredito.pas' {frmIdentificaCredito},
  fBaixaTituloDeposito in 'fBaixaTituloDeposito.pas' {frmBaixaTituloDeposito},
  fBaixaTituloDeposito_Movtos in 'fBaixaTituloDeposito_Movtos.pas' {frmBaixaTituloDeposito_Movtos},
  fTrilhaClienteVarejo in 'fTrilhaClienteVarejo.pas' {frmTrilhaClienteVarejo},
  fBaixaTituloDeposito_Titulos in 'fBaixaTituloDeposito_Titulos.pas' {frmBaixaTituloDeposito_Titulos},
  fClienteVarejoPessoaFisica_HistAltEnd in 'fClienteVarejoPessoaFisica_HistAltEnd.pas' {frmClienteVarejoPessoaFisica_HistAltEnd},
  fImagemDigital in 'fImagemDigital.pas' {frmImagemDigital},
  fImagemDigital_Visualizar in 'fImagemDigital_Visualizar.pas' {frmImagemDigital_Visualizar},
  fMensagem in 'fMensagem.pas' {frmMensagem},
  fTimedMessage in 'fTimedMessage.pas' {frmTimedMessage},
  fCaixa_Justificativa in 'fCaixa_Justificativa.pas' {frmCaixa_Justificativa},
  fCaixa_ParcelaAberto in 'fCaixa_ParcelaAberto.pas' {frmCaixa_ParcelaAberto},
  fCaixa_LiberaRestricao in 'fCaixa_LiberaRestricao.pas' {frmCaixa_LiberaRestricao},
  fPedidoVendaSimplesDesconto in 'fPedidoVendaSimplesDesconto.pas' {frmPedidoVendaSimplesDesconto},
  fPdvConsProduto in 'fPdvConsProduto.pas' {frmPdvConsProduto},
  fCentralArquivos in 'fCentralArquivos.pas' {frmCentralArquivos},
  fPdvReabrePedido in 'fPdvReabrePedido.pas' {frmPdvReabrePedido},
  fPdvSupervisor in 'fPdvSupervisor.pas' {frmPdvSupervisor},
  fPDVValePresente in 'fPDVValePresente.pas' {frmPdvValePresente},
  fPdvValeMerc in 'fPdvValeMerc.pas' {frmPDVValeMerc},
  fCaixa_SelCliente in 'fCaixa_SelCliente.pas' {frmCaixa_SelCliente},
  DBCtrls in 'DBCtrls.pas',
  fManutPedido_BaixaManual in 'fManutPedido_BaixaManual.pas' {frmManutPedido_BaixaManual},
  fQuestionarioCQM in 'fQuestionarioCQM.pas' {frmQuestionarioCQM},
  fLanctoPlanoConta in 'fLanctoPlanoConta.pas' {frmLanctoPlanoConta},
  fAnaliseLanctoCtbConta in 'fAnaliseLanctoCtbConta.pas' {frmAnaliseLanctoCtbConta},
  fAnaliseLanctoCtbConta_Detalhe in 'fAnaliseLanctoCtbConta_Detalhe.pas' {frmAnaliseLanctoCtbConta_Detalhe},
  fAvaliacaoCQM in 'fAvaliacaoCQM.pas' {frmAvaliacaoCQM},
  fCaixa_Sangria in 'fCaixa_Sangria.pas' {frmCaixa_Sangria},
  fCaixa_Suprimento in 'fCaixa_Suprimento.pas' {frmCaixa_Suprimento},
  rFichaPosicaoContratoImobManual in 'rFichaPosicaoContratoImobManual.pas' {rptFichaPosicaoContratoImobManual},
  fCaixa_ParcelaCred in 'fCaixa_ParcelaCred.pas' {frmCaixa_ParcelaCred},
  fCaixa_DadosCheque in 'fCaixa_DadosCheque.pas' {frmCaixa_DadosCheque},
  fEmpImobiliario in 'fEmpImobiliario.pas' {frmEmpImobiliario},
  fContratoImobiliario in 'fContratoImobiliario.pas' {frmContratoImobiliario},
  fCaixa_DadosCartao in 'fCaixa_DadosCartao.pas' {frmCaixa_DadosCartao},
  fTransfEst in 'fTransfEst.pas' {frmTransfEst},
  fCaixa_Funcoes in 'fCaixa_Funcoes.pas' {frmCaixa_Funcoes},
  fCaixa_ReembolsoVale in 'fCaixa_ReembolsoVale.pas' {frmCaixa_ReembolsoVale},
  fCaixa_LanctoMan in 'fCaixa_LanctoMan.pas' {frmCaixa_LanctoMan},
  rTituloReabilitado_view in 'rTituloReabilitado_view.pas' {rptTituloReabilitado_view},
  fCaixa_NumValePresente in 'fCaixa_NumValePresente.pas' {frmCaixa_NumValePresente},
  rFichaPosicaoContratoImob in 'rFichaPosicaoContratoImob.pas' {rptFichaPosicaoContratoImob},
  fCaixa_ConsOperadora in 'fCaixa_ConsOperadora.pas' {frmCaixa_ConsOperadora},
  fPDVConsVended in 'fPDVConsVended.pas' {frmPDVConsVended},
  fCaixa_ConfUsuarioCaixa in 'fCaixa_ConfUsuarioCaixa.pas' {frmCaixa_ConfUsuarioCaixa},
  fCaixa_AlteraCartao in 'fCaixa_AlteraCartao.pas' {frmCaixa_AlteraCartao},
  fCaixa_Reimpressao in 'fCaixa_Reimpressao.pas' {frmCaixa_Reimpressao},
  fAprovacaoClienteVarejo in 'fAprovacaoClienteVarejo.pas' {frmAprovacaoClienteVarejo},
  fAprovacaoClienteVarejo_LimiteInicial in 'fAprovacaoClienteVarejo_LimiteInicial.pas' {frmAprovacaoClienteVarejo_LimiteInicial},
  fManutLimiteClienteManual in 'fManutLimiteClienteManual.pas' {frmManutLimiteClienteManual},
  fHistoricoLimiteCredito in 'fHistoricoLimiteCredito.pas' {frmHistoricoLimiteCredito},
  fManutBloqueioCliente in 'fManutBloqueioCliente.pas' {frmManutBloqueioCliente},
  fContratoImobiliario_Parcelas in 'fContratoImobiliario_Parcelas.pas' {frmContratoImobiliario_Parcelas},
  fManutLimiteCliente in 'fManutLimiteCliente.pas' {frmManutLimiteCliente},
  fManutBloqueioCliente_Inserir in 'fManutBloqueioCliente_Inserir.pas' {frmManutBloqueioCliente_Inserir},
  fTipoBloqueioCliente in 'fTipoBloqueioCliente.pas' {frmTipoBloqueioCliente},
  rBorderoCaixaNovo in 'rBorderoCaixaNovo.pas' {rptBorderoCaixaNovo},
  fPedidoVendaSimples in 'fPedidoVendaSimples.pas' {frmPedidoVendaSimples},
  fFollowUP_Pedido in 'fFollowUP_Pedido.pas' {frmFollowUP_Pedido},
  fConsTituloAberto in 'fConsTituloAberto.pas' {frmConsTituloAberto},
  rRankingFatAnualProduto in 'rRankingFatAnualProduto.pas' {rptRankingFatAnualProduto},
  dcVendaProdutoCliente in 'dcVendaProdutoCliente.pas' {dcmVendaProdutoCliente},
  fConsultaPagamentoWERP in 'fConsultaPagamentoWERP.pas' {frmConsultaPagamentoWERP},
  fConsultaNotaFiscalWERP in 'fConsultaNotaFiscalWERP.pas' {frmConsultaNotaFiscalWERP},
  fTipoTabelaPreco in 'fTipoTabelaPreco.pas' {frmTipoTabelaPreco},
  fCampanhaPromoc in 'fCampanhaPromoc.pas' {frmCampanhaPromoc},
  fInventarioLoja_IncluiProduto in 'fInventarioLoja_IncluiProduto.pas' {frmInventarioLoja_IncluiProduto},
  fCampanhaPromoc_ExibeProduto in 'fCampanhaPromoc_ExibeProduto.pas' {frmCampanhaPromoc_ExibeProduto},
  fAtivaCampanhaPromoc in 'fAtivaCampanhaPromoc.pas' {frmAtivaCampanhaPromoc},
  fAcompanhaCampanhaPromoc in 'fAcompanhaCampanhaPromoc.pas' {frmAcompanhaCampanhaPromoc},
  fCampanhaPromoc_AnaliseVendasERP in 'fCampanhaPromoc_AnaliseVendasERP.pas' {frmCampanhaPromoc_AnaliseVendasERP},
  fCampanhaPromoc_AnaliseVendas in 'fCampanhaPromoc_AnaliseVendas.pas' {frmCampanhaPromoc_AnaliseVendas},
  fCaixa_ListaPedidos in 'fCaixa_ListaPedidos.pas' {frmCaixa_ListaPedidos},
  fEtapaCobranca in 'fEtapaCobranca.pas' {frmEtapaCobranca},
  fCobranca_VisaoContato in 'fCobranca_VisaoContato.pas' {frmCobranca_VisaoContato},
  fListaCobranca in 'fListaCobranca.pas' {frmListaCobranca},
  fCobranca_VisaoContato_MovTit in 'fCobranca_VisaoContato_MovTit.pas' {frmCobranca_VisaoContato_MovTit},
  fListaCobranca_Itens in 'fListaCobranca_Itens.pas' {frmListaCobranca_Itens},
  fTipoResultadoContato in 'fTipoResultadoContato.pas' {frmTipoResultadoContato},
  fCobranca_VisaoContato_Contato in 'fCobranca_VisaoContato_Contato.pas' {frmCobranca_VisaoContato_Contato},
  fRegAtendOcorrencia_Atendimento_SelContato in 'fRegAtendOcorrencia_Atendimento_SelContato.pas' {frmRegAtendOcorrencia_Atendimento_SelContato},
  fAdmListaCobranca in 'fAdmListaCobranca.pas' {frmAdmListaCobranca},
  fAdmListaCobranca_Listas in 'fAdmListaCobranca_Listas.pas' {frmAdmListaCobranca_Listas},
  fAdmListaCobranca_Listas_SelLista in 'fAdmListaCobranca_Listas_SelLista.pas' {frmAdmListaCobranca_Listas_SelLista},
  fListaCobranca_Itens_NovaLista in 'fListaCobranca_Itens_NovaLista.pas' {frmListaCobranca_Itens_NovaLista},
  fModeloCartaCobranca in 'fModeloCartaCobranca.pas' {frmModeloCartaCobranca},
  fListaCobranca_Itens_GerarCarta in 'fListaCobranca_Itens_GerarCarta.pas' {frmListaCobranca_Itens_GerarCarta},
  fCobranca_VisaoContato_Titulos in 'fCobranca_VisaoContato_Titulos.pas' {frmCobranca_VisaoContato_Titulos},
  fListaCobranca_Itens_SelCliCobradora in 'fListaCobranca_Itens_SelCliCobradora.pas' {frmListaCobranca_Itens_SelCliCobradora},
  rFichaCobradora_view in 'rFichaCobradora_view.pas' {rptFichaCobradora_view},
  fClienteVarejoPessoaFisica_TitulosNeg in 'fClienteVarejoPessoaFisica_TitulosNeg.pas' {frmClienteVarejoPessoaFisica_TitulosNeg},
  fReabilitacaoCliente in 'fReabilitacaoCliente.pas' {frmReabilitacaoCliente},
  rTituloNegativado_view in 'rTituloNegativado_view.pas' {rptTituloNegativado_view},
  fTitulo_DetalheJuros in 'fTitulo_DetalheJuros.pas' {frmTitulo_DetalheJuros},
  fCobradora in 'fCobradora.pas' {frmCobradora},
  fListaCobranca_Itens_EnvioCobradora in 'fListaCobranca_Itens_EnvioCobradora.pas' {frmListaCobranca_Itens_EnvioCobradora},
  rAlteracaoPrecoProduto_view in 'rAlteracaoPrecoProduto_view.pas' {rptAlteracaoPrecoProduto_view},
  fGeraListaCobranca in 'fGeraListaCobranca.pas' {frmGeraListaCobranca},
  fCalendarioCobrancaDiario in 'fCalendarioCobrancaDiario.pas' {frmCalendarioCobrancaDiario},
  fGeraPropostaRenegociacao in 'fGeraPropostaRenegociacao.pas' {frmGeraPropostaRenegociacao},
  fGeraPropostaRenegociacao_Simulacao in 'fGeraPropostaRenegociacao_Simulacao.pas' {frmGeraPropostaRenegociacao_Simulacao},
  rRecebimentoCompras in 'rRecebimentoCompras.pas' {rptRecebimentoCompras},
  UrelRecebimentoCompras_View in 'UrelRecebimentoCompras_View.pas' {rptRecebimentoCompras_View},
  fAutorizaPropostaReneg in 'fAutorizaPropostaReneg.pas' {frmAutorizaPropostaReneg},
  fAutorizaPropostaReneg_Titulos in 'fAutorizaPropostaReneg_Titulos.pas' {frmAutorizaPropostaReneg_Titulos},
  fCaixa_DescontoJuros in 'fCaixa_DescontoJuros.pas' {frmCaixa_DescontoJuros},
  fCaixa_ConsPropReneg in 'fCaixa_ConsPropReneg.pas' {frmCaixa_ConsPropReneg},
  fCaixa_EfetivaPropReneg in 'fCaixa_EfetivaPropReneg.pas' {frmCaixa_EfetivaPropReneg},
  rPosTituloAbertoFormaPagto_view in 'rPosTituloAbertoFormaPagto_view.pas' {rptPosTituloAbertoFormaPagto},
  fPosTituloAbertoFormaPagto in 'fPosTituloAbertoFormaPagto.pas' {frmPosTituloAbertoFormaPagto},
  fListaCobranca_Itens_Negativacao in 'fListaCobranca_Itens_Negativacao.pas' {frmListaCobranca_Itens_Negativacao},
  fMTitulo_Movimenta in 'fMTitulo_Movimenta.pas' {frmMTitulo_Movimenta},
  fBaixaColetiva_ConsTitulo in 'fBaixaColetiva_ConsTitulo.pas' {frmBaixaColetiva_ConsTitulo},
  fLoteLiqCobradora in 'fLoteLiqCobradora.pas' {frmLoteLiqCobradora},
  fLoteLiqCobradora_Cliente in 'fLoteLiqCobradora_Cliente.pas' {frmLoteLiqCobradora_Cliente},
  fLoteLiqCobradora_Titulos in 'fLoteLiqCobradora_Titulos.pas' {frmLoteLiqCobradora_Titulos},
  rBaixaColetiva_view in 'rBaixaColetiva_view.pas' {rptBaixaColetiva_view},
  fRetiraClienteCobradora in 'fRetiraClienteCobradora.pas' {frmRetiraClienteCobradora},
  fPosTituloAberto in 'fPosTituloAberto.pas',
  rPosTituloAberto in 'rPosTituloAberto.pas' {rptPosTituloAberto},
  rProdutoMenosVendido in 'rProdutoMenosVendido.pas' {rptProdutoMenosVendido},
  dcVendaProdutoCliente_view in 'dcVendaProdutoCliente_view.pas' {dcmVendaProdutoCliente_view},
  dcAnaliseGiroProduto in 'dcAnaliseGiroProduto.pas' {dcmAnaliseGiroProduto},
  rProdutoMenosVendido_view in 'rProdutoMenosVendido_view.pas' {rptProdutoMenosVendido_view},
  rPreVendaPendente in 'rPreVendaPendente.pas' {rptPreVendaPendente},
  rVendaEstruturaMarcaLoja_view in 'rVendaEstruturaMarcaLoja_view.pas' {rptVendaEstruturaMarcaLoja_view},
  rPerformanceProduto in 'rPerformanceProduto.pas' {rptPerformanceProduto},
  rPosicaoEstoqueSimples_view in 'rPosicaoEstoqueSimples_view.pas' {rptPosicaoEstoqueSimples_view},
  fMonitorTransferencia in 'fMonitorTransferencia.pas' {frmMonitorTransferencia},
  fProgAltPreco in 'fProgAltPreco.pas' {frmProgAltPreco},
  fProgaltPreco_Incluir in 'fProgaltPreco_Incluir.pas' {frmProgaltPreco_Incluir},
  rAlteracaoPrecoProduto in 'rAlteracaoPrecoProduto.pas' {rptAlteracaoPrecoProduto},
  rCampanhaPromoc_view in 'rCampanhaPromoc_view.pas' {rptCampanhaPromoc_view},
  rOrcamentoPrevReal_View_Semestral in 'rOrcamentoPrevReal_View_Semestral.pas' {rptOrcamentoPrevReal_View_Semestral},
  fProduto_DetalheGrade in 'fProduto_DetalheGrade.pas' {frmProduto_DetalheGrade},
  fProduto_HistPrecoVenda in 'fProduto_HistPrecoVenda.pas' {frmProduto_HistPrecoVenda},
  rAcompanhaGiroProduto in 'rAcompanhaGiroProduto.pas' {rptAcompanhaGiroProduto},
  rPosicaoEstoqueGrade_view in 'rPosicaoEstoqueGrade_view.pas' {rptPosicaoEstoqueGrade_view},
  rPosicaoEstoqueEstruturaMarca in 'rPosicaoEstoqueEstruturaMarca.pas' {rptPosicaoEstoqueEstruturaMarca},
  rPosicaoEstoqueEstruturaMarca_view in 'rPosicaoEstoqueEstruturaMarca_view.pas' {rptPosicaoEstoqueEstruturaMarca_view},
  fServidorReplicacao in 'fServidorReplicacao.pas' {frmServidorReplicacao},
  fServidorReplicacao_FilaTransmissao_Novo in 'fServidorReplicacao_FilaTransmissao_Novo.pas' {frmServidorReplicacao_FilaTransmissao_Novo},
  fRegistrosRecebidos in 'fRegistrosRecebidos.pas' {frmRegistrosRecebidos},
  dcVendaLojaWeb in 'dcVendaLojaWeb.pas' {dcmVendaLojaWeb},
  dcVendaLojaWeb_View in 'dcVendaLojaWeb_View.pas' {dcmVendaLojaWeb_View},
  rPosicaoEstoquePlanilha in 'rPosicaoEstoquePlanilha.pas' {rptPosicaoEstoquePlanilha},
  rComportamentoCrediario_view in 'rComportamentoCrediario_view.pas' {rptComportamentoCrediario_view},
  fAcompanhaGiroProduto_view in 'fAcompanhaGiroProduto_view.pas' {frmAcompanhaGiroProduto_view},
  rPosicaoEstoqueSimples in 'rPosicaoEstoqueSimples.pas' {rptPosicaoEstoqueSimples},
  rPerformanceProduto_view in 'rPerformanceProduto_view.pas' {rptPerformanceProduto_view},
  rVendaEstruturaMarcaLoja in 'rVendaEstruturaMarcaLoja.pas' {rptVendaEstruturaMarcaLoja},
  rComportamentoCrediario in 'rComportamentoCrediario.pas' {rptComportamentoCrediario},
  rVendaCrediario_view in 'rVendaCrediario_view.pas' {rptVendaCrediario_view},
  rVendaCrediario in 'rVendaCrediario.pas' {rptVendaCrediario},
  rPosicaoVale_view in 'rPosicaoVale_view.pas' {rptPosicaoVale_view},
  rPosicaoInadimplencia in 'rPosicaoInadimplencia.pas' {rptPosicaoInadimplencia},
  rPosicaoInadimplencia_view in 'rPosicaoInadimplencia_view.pas' {rptPosicaoInadimplencia_view},
  fModImpETP in 'fModImpETP.pas' {frmModImpETP},
  fSelPortaEtiqueta in 'fSelPortaEtiqueta.pas' {frmSelPortaEtiqueta},
  fAtualizacaoIndice in 'fAtualizacaoIndice.pas' {frmAtualizacaoMonetaria},
  fBaixaParcela in 'fBaixaParcela.pas' {frmBaixaParcela},
  fIndice in 'fIndice.pas' {FrmIndice},
  fLiquidaParcela in 'fLiquidaParcela.pas' {frmLiquidaParcela},
  rBlocoUnidades in 'rBlocoUnidades.pas' {rptBlocoUnidades},
  rBlocoUnidadesView in 'rBlocoUnidadesView.pas' {rptBlocoUnidadesView},
  rFichaContratoImobiliario in 'rFichaContratoImobiliario.pas' {rptFichaContratoImobiliario},
  rRecibo in 'rRecibo.pas' {rptRecibo},
  rReciboView in 'rReciboView.pas' {rptReciboView},
  Extens in 'Extens.pas',
  pasModuloTEF in 'pasModuloTEF.pas',
  pasEPL2 in 'pasEPL2.pas',
  fGeraSPComissaoRepres in 'fGeraSPComissaoRepres.pas' {frmGeraSPComissaoRepres},
  fGeraSPComissaoRepres_Resumo in 'fGeraSPComissaoRepres_Resumo.pas' {frmGeraSPComissaoRepres_Resumo},
  fImportaMunicipioIBGE in 'fImportaMunicipioIBGE.pas' {frmImportaMunicipioIBGE},
  rPicking in 'rPicking.pas' {rptPicking},
  fClienteVarejoPessoaFisica_HistMov in 'fClienteVarejoPessoaFisica_HistMov.pas' {frmClienteVarejoPessoaFisica_HistMov},
  fProduto in 'fProduto.pas' {frmProduto},
  fTabRenegociacao in 'fTabRenegociacao.pas' {frmTabRenegociacao},
  fEmiteEtiqProdAvulsa in 'fEmiteEtiqProdAvulsa.pas' {frmEmiteEtiqProdAvulsa},
  fGrupoOperadoraCartao in 'fGrupoOperadoraCartao.pas' {frmGrupoOperadoraCartao},
  fInventario_AberturaMod2 in 'fInventario_AberturaMod2.pas' {frmInventario_AberturaMod2},
  fCampanhaPromoc_IncluiProduto in 'fCampanhaPromoc_IncluiProduto.pas' {frmCampanhaPromoc_IncluiProduto},
  fGrupoPlanoConta in 'fGrupoPlanoConta.pas' {frmGrupoPlanoConta},
  fReabilitacaoClienteManual in 'fReabilitacaoClienteManual.pas' {frmReabilitacaoClienteManual},
  fInventarioLoja_Encerramento in 'fInventarioLoja_Encerramento.pas' {frmInventarioLoja_Encerramento},
  fInventarioLoja_Contagem_ExibeLotes in 'fInventarioLoja_Contagem_ExibeLotes.pas' {frmInventarioLoja_Contagem_ExibeLotes},
  fInventarioLoja_Contagem in 'fInventarioLoja_Contagem.pas' {frmInventarioLoja_Contagem},
  fInventarioLoja_Apuracao_ExibeDiverg in 'fInventarioLoja_Apuracao_ExibeDiverg.pas' {frmInventarioLoja_Apuracao_ExibeDiverg},
  fInventarioLoja_Apuracao in 'fInventarioLoja_Apuracao.pas' {frmInventarioLoja_Apuracao},
  fProvisaoPagto_SaldoProjetado in 'fProvisaoPagto_SaldoProjetado.pas' {frmProvisaoPagtoSaldoProjetado},
  rResumoMovimentoLoja in 'rResumoMovimentoLoja.pas' {rptResumoMovimentoLoja},
  rComissaoRepresSintetico_View in 'rComissaoRepresSintetico_View.pas' {rptComissaoRepresSintetico_View},
  rItensPorVenda_view in 'rItensPorVenda_view.pas' {rptItensPorVenda_view},
  rItensPorVenda in 'rItensPorVenda.pas' {rptItensPorVenda},
  fCancelaPropostaReneg in 'fCancelaPropostaReneg.pas' {frmCancelaPropostaReneg},
  rAniversariantesMes in 'rAniversariantesMes.pas' {rptAniversariantesMes},
  rClientesComprasPeriodo in 'rClientesComprasPeriodo.pas' {rptClientesComprasPeriodo},
  rClientesComprasPeriodo_view in 'rClientesComprasPeriodo_view.pas' {rptClientesComprasPeriodo_view},
  rAniversariantesMes_view in 'rAniversariantesMes_view.pas' {rptAniversariantesMes_view},
  rRelMovimentoCaixaAnalitico in 'rRelMovimentoCaixaAnalitico.pas' {rptRelMovimentoCaixaAnalitico},
  fLojaOperadoraCartaoTaxaJuros in 'fLojaOperadoraCartaoTaxaJuros.pas' {frmLojaOperadoraCartaoTaxaJuros},
  rPagamentoCentroCusto in 'rPagamentoCentroCusto.pas' {rptRelPagtoCentroCusto},
  rPagamentoCentroCusto_view in 'rPagamentoCentroCusto_view.pas' {rptPagamentoCentroCusto_View},
  fPDVConsProdutoDetalhe in 'fPDVConsProdutoDetalhe.pas' {frmPDVConsProdutoDetalhe},
  fClienteVarejoPessoaFisica_HistContato in 'fClienteVarejoPessoaFisica_HistContato.pas' {frmClienteVarejoPessoaFisica_HistContato},
  fClienteVarejoPessoaFisica_HistoricoNeg in 'fClienteVarejoPessoaFisica_HistoricoNeg.pas' {frmClienteVarejoPessoaFisica_HistoricoNeg},
  fEnvioClienteCobradora in 'fEnvioClienteCobradora.pas' {frmEnvioClienteCobradora},
  fEnvioClienteCobradora_Titulos in 'fEnvioClienteCobradora_Titulos.pas' {frmEnvioClienteCobradora_Titulos},
  fSitSincServidor in 'fSitSincServidor.pas' {frmSitSincServidor},
  rVendaDiariaXFormaPagto in 'rVendaDiariaXFormaPagto.pas' {rptVendaDiariaXFormaPagto},
  rVendaDiariaXFormaPagto_view in 'rVendaDiariaXFormaPagto_view.pas' {rptVendaDiariaXFormaPagto_view},
  rChequeDevol in 'rChequeDevol.pas' {rptChequeDevolv},
  rChequeRecebido_view in 'rChequeRecebido_view.pas' {rptChequeRecebido_view},
  rPosicaoFinancContratoImobiliario_ResumoContrato_view in 'rPosicaoFinancContratoImobiliario_ResumoContrato_view.pas' {rptPosicaoFinancContratoImobiliario_ResumoContrato_view},
  rChequeEmitido in 'rChequeEmitido.pas' {rptChequeEmitido},
  rFluxoCaixaRealizadoAnalitico_view in 'rFluxoCaixaRealizadoAnalitico_view.pas' {rptFluxoCaixaRealizadoAnalitico_view},
  fConsultaMovtoCaixa in 'fConsultaMovtoCaixa.pas' {frmConsultaMovtoCaixa},
  fConsultaMovtoCaixa_Parcelas in 'fConsultaMovtoCaixa_Parcelas.pas' {frmConsultaMovtoCaixa_Parcelas},
  rParcelaCrediarioAberta in 'rParcelaCrediarioAberta.pas' {rptParcelaCrediarioAberta},
  rParcelaCrediarioAberta_view in 'rParcelaCrediarioAberta_view.pas' {rptParcelaCrediarioAberta_view},
  fConciliaLoteLiqCobradora in 'fConciliaLoteLiqCobradora.pas' {frmConciliaLoteLiqCobradora},
  fConciliaLoteLiqCobradora_Efetiva in 'fConciliaLoteLiqCobradora_Efetiva.pas' {frmConciliaLoteLiqCobradora_Efetiva},
  rParcelaCrediarioRecebida in 'rParcelaCrediarioRecebida.pas' {rptParcelaCrediarioRecebida},
  rParcelaCrediarioRecebida_view in 'rParcelaCrediarioRecebida_view.pas' {rptParcelaCrediarioRecebida_view},
  rPagamentoCentroCustoSint_view in 'rPagamentoCentroCustoSint_view.pas' {rptPagamentoCentroCustoSint_View},
  fConsultaLanctoConta in 'fConsultaLanctoConta.pas' {frmConsultaLanctoConta},
  rLanctoCaixa in 'rLanctoCaixa.pas' {rptLanctoCaixa},
  rLanctoCaixa_view in 'rLanctoCaixa_view.pas' {rptLanctoCaixa_view},
  rClientesCadastroPeriodo in 'rClientesCadastroPeriodo.pas' {rptClientesCadastroPeriodo},
  rClientesCadastroPeriodo_View in 'rClientesCadastroPeriodo_View.pas' {rClientesCadastroPeriodoView},
  fConsultaPagamentoCentroCusto in 'fConsultaPagamentoCentroCusto.pas' {frmConsultaPagamentoCentroCusto},
  fConsultaPagamentoCentroCustoAnalit in 'fConsultaPagamentoCentroCustoAnalit.pas' {frmConsultaPagamentoCentroCustoAnalit},
  fConsultaPagamentoCentroCustoSint in 'fConsultaPagamentoCentroCustoSint.pas' {frmConsultaPagamentoCentroCustoSint},
  rTituloNegativado in 'rTituloNegativado.pas' {rptTituloNegativado},
  fManutencaoVales in 'fManutencaoVales.pas' {frmManutencaoVales},
  fManutencaoCrediario in 'fManutencaoCrediario.pas' {frmManutencaoCrediario},
  fAlterarContaBancariaDeposito in 'fAlterarContaBancariaDeposito.pas' {frmAlteraContaBancariaDeposito},
  fCancelaDepositoBancario in 'fCancelaDepositoBancario.pas' {frmCancelaDepositoBancario},
  fConsultaVendaDesconto in 'fConsultaVendaDesconto.pas' {frmConsultaVendaDesconto},
  rProvisaoPagamentoBaixada in 'rProvisaoPagamentoBaixada.pas' {rptProvisaoPagamentoBaixada},
  rProvisaoPagamentoBaixada_View in 'rProvisaoPagamentoBaixada_View.pas' {rptProvisaoPagamentoBaixada_View},
  fManutencaoPedidoVenda in 'fManutencaoPedidoVenda.pas' {frmManutencaoPedidoVenda},
  fLanctoCaixaEstornado in 'fLanctoCaixaEstornado.pas' {frmLanctoCaixaEstornado},
  fCadTransacaoCartao in 'fCadTransacaoCartao.pas' {frmCadTransacaoCartao},
  rPosicaoEstoqueGrade in 'rPosicaoEstoqueGrade.pas' {rptPosicaoEstoqueGrade},
  rVendaXVendedorAnalitico in 'rVendaXVendedorAnalitico.pas' {rptVendaXVendedorAnalitico},
  rVendaXVendedorAnalitico_view in 'rVendaXVendedorAnalitico_view.pas' {rptVendaXVendedorAnalitico_view},
  rDoctoFiscalWebEmitido in 'rDoctoFiscalWebEmitido.pas' {rptDoctoFiscalWebEmitido},
  rDoctoFiscalWebEmitido_View in 'rDoctoFiscalWebEmitido_View.pas' {rptDoctoFiscalWebEmitido_View},
  fSituacaoEstoqueProduto in 'fSituacaoEstoqueProduto.pas' {frmSituacaoEstoqueProduto},
  fCaixa_CPFNotaFiscal in 'fCaixa_CPFNotaFiscal.pas' {frmCaixa_CPFNotaFiscal},
  fTipoAdiantamento in 'fTipoAdiantamento.pas' {frmTipoAdiantamento},
  fConciliaBancoManual_TituloMovimentado in 'fConciliaBancoManual_TituloMovimentado.pas' {frmConciliaBancoManual_TituloMovimentado},
  fAliquotaPisCofins in 'fAliquotaPisCofins.pas' {frmAliquotaPisCofins},
  rEmitePisCofinsVendas in 'rEmitePisCofinsVendas.pas' {frmEmitePisCofinsVendas},
  rEmitePisCofinsVendas_View in 'rEmitePisCofinsVendas_View.pas' {rEmitePisCofins_View},
  rEmitePisCofinsVendasSint_View in 'rEmitePisCofinsVendasSint_View.pas' {rEmitePisCofinsSint_View},
  fPickPack in 'fPickPack.pas' {frmPickPack},
  fTerceiro_MediaVenda in 'fTerceiro_MediaVenda.pas' {frmTerceiro_MediaVenda},
  fProvisaoPagto_TitulosAdiantamento in 'fProvisaoPagto_TitulosAdiantamento.pas' {frmProvisaoPagtoTituloAdiantamento},
  fGeraDoctoFiscalCompl in 'fGeraDoctoFiscalCompl.pas' {frmGeraDoctoFiscalCompl},
  fGeracaoComplemento in 'fGeracaoComplemento.pas' {frmGeracaoComplemento},
  fTerceiro_ChequePendente in 'fTerceiro_ChequePendente.pas' {frmTerceiro_ChequePendente},
  fTerceiro_VendaAutorizada in 'fTerceiro_VendaAutorizada.pas' {frmTerceiro_VendaAutorizada},
  fTerceiro_PosicaoReceber in 'fTerceiro_PosicaoReceber.pas' {frmTerceiro_PosicaoReceber},
  fMetaMensal in 'fMetaMensal.pas' {frmMetaMensal},
  fMetaSemanal in 'fMetaSemanal.pas' {frmMetaSemanal},
  fAlcadasSP in 'fAlcadasSP.pas' {frmAlcadasSP},
  fMetaVendedor in 'fMetaVendedor.pas' {frmMetaVendedor},
  fMetaApontamentoHoraOportunidade in 'fMetaApontamentoHoraOportunidade.pas' {frmMetaApontamentoHoraOportunidade},
  fMetaVendedor_CotaVendedor in 'fMetaVendedor_CotaVendedor.pas' {frmMetaVendedor_CotaVendedor},
  fMetaApontamentoHora_Vendedor in 'fMetaApontamentoHora_Vendedor.pas' {frmMetaApontamentoHora_Vendedor},
  fEmiteNFE2 in 'fEmiteNFE2.pas' {frmEmiteNFe2},
  fAcompMetaMensalLoja in 'fAcompMetaMensalLoja.pas' {frmAcompMetaMensalLoja},
  rClientesSemCompraPeriodo in 'rClientesSemCompraPeriodo.pas' {rptClientesSemCompraPeriodo},
  rClientesSemCompraPeriodo_View in 'rClientesSemCompraPeriodo_View.pas' {rptClientesSemCompraPeriodo_View},
  fResumoDesempSemanalLoja in 'fResumoDesempSemanalLoja.pas' {frmResumoDesempSemanalLoja},
  fResumoDesempSemanalGeral in 'fResumoDesempSemanalGeral.pas' {frmResumoDesempSemanalGeral},
  fTipoDocumentoFiscal in 'fTipoDocumentoFiscal.pas' {frmTipoDocumentoFiscal},
  rPosicaoEstoqueGeralNovo in 'rPosicaoEstoqueGeralNovo.pas' {rptPosicaoEstoqueGeralNovo},
  rPosicaoEstoqueGeral_view in 'rPosicaoEstoqueGeral_view.pas' {rptPosicaoEstoqueGeral_view},
  rOscilacaoEstoque in 'rOscilacaoEstoque.pas' {rptOscilacaoEstoque},
  rOscilacaoEstoque_View in 'rOscilacaoEstoque_View.pas' {rptOscilacaoEstoque_View},
  fOscilacaoEstoque_View in 'fOscilacaoEstoque_View.pas' {frmOscilacaoEstoque_View},
  fItensPedido in 'fItensPedido.pas' {frmItensPedido},
  fRoteiroProducao_Etapas in 'fRoteiroProducao_Etapas.pas' {frmRoteiroProducao_Etapas},
  fMemoText in 'fMemoText.pas' {frmMemoText},
  fCadastroCentroProdutivo in 'fCadastroCentroProdutivo.pas' {frmCadastroCentroProdutivo},
  fEtapasProducao in 'fEtapasProducao.pas' {frmEtapasProducao},
  fTipoOrdemProducao in 'fTipoOrdemProducao.pas' {frmTipoOrdemProducao},
  fGrupoCalculoTempo in 'fGrupoCalculoTempo.pas' {frmGrupoCalculoTempo},
  fOP in 'fOP.pas' {frmOP},
  fOP_ProdutoEtapa in 'fOP_ProdutoEtapa.pas' {frmOP_ProdutoEtapa},
  rRequisicaoOP in 'rRequisicaoOP.pas' {rptRequisicaoOP},
  fEntradaProducao in 'fEntradaProducao.pas' {frmEntradaProducao},
  fMonitorOP in 'fMonitorOP.pas' {frmMonitorOP},
  fMonitorOP_DataInicio in 'fMonitorOP_DataInicio.pas' {frmMonitorOP_DataInicio},
  fAcompProducaoPedidoVenda in 'fAcompProducaoPedidoVenda.pas' {frmAcompProducaoPedidoVenda},
  fPMP in 'fPMP.pas',
  rDiarioAuxiliarClientes in 'rDiarioAuxiliarClientes.pas' {rptDiarioAuxiliarClientes},
  rDiarioAuxiliarClientes_View in 'rDiarioAuxiliarClientes_View.pas' {rptDiarioAuxiliarClientes_View},
  fGerarRequisicaoOP in 'fGerarRequisicaoOP.pas' {frmGerarRequisicaoOP},
  fGerarRequisicaoOP_ExibeRequisicoes in 'fGerarRequisicaoOP_ExibeRequisicoes.pas' {frmGerarRequisicaoOP_ExibeRequisicoes},
  rRazaoAuxiliarFornecedores in 'rRazaoAuxiliarFornecedores.pas' {rptRazaoAuxiliarFornecedores},
  rRazaoAuxiliarFornecedores_View in 'rRazaoAuxiliarFornecedores_View.pas' {rptRazaoAuxiliarFornecedores_View},
  rOP in 'rOP.pas' {rptOP},
  fForecast in 'fForecast.pas' {frmForecast},
  fConversaoUndMedida in 'fConversaoUndMedida.pas' {frmConversaoUndMedida},
  fBemPatrim in 'fBemPatrim.pas' {frmBemPatrim},
  fClasseBemPatrim in 'fClasseBemPatrim.pas' {frmClasseBemPatrim},
  fConfigSubGrupoBemPatrim in 'fConfigSubGrupoBemPatrim.pas' {frmConfigSubGrupoBemPatrim},
  fGrupoBemPatrim in 'fGrupoBemPatrim.pas' {frmGrupoBemPatrim},
  fMotBaixaBemPatrim in 'fMotBaixaBemPatrim.pas' {frmMotBaixaBemPatrim},
  fSetorBemPatrim in 'fSetorBemPatrim.pas' {frmSetorBemPatrim},
  fAtendimentoRequisicao_Atende in 'fAtendimentoRequisicao_Atende.pas' {frmAtendimentoRequisicao_Atende},
  fAjusteEmpenho in 'fAjusteEmpenho.pas' {frmAjusteEmpenho},
  fAjusteEmpenho_IncluirEditar in 'fAjusteEmpenho_IncluirEditar.pas' {frmAjusteEmpenho_IncluirEditar},
  fAjusteEmpenho_Produtos in 'fAjusteEmpenho_Produtos.pas' {frmAjusteEmpenho_Produtos},
  fProdutoERP_Identado in 'fProdutoERP_Identado.pas' {frmProdutoERP_Identado},
  fRegistroMovLote in 'fRegistroMovLote.pas' {frmRegistroMovLote},
  fTabCFOP in 'fTabCFOP.pas' {frmTabCFOP},
  fTipoServicosImposto in 'fTipoServicosImposto.pas' {frmTipoServicoImposto},
  fCadastroImposto in 'fCadastroImposto.pas' {frmCadastroImpostos},
  fOPProdPendDevEstoque in 'fOPProdPendDevEstoque.pas' {frmOPProdPendDevEstoque},
  fTipoPerdaProducao in 'fTipoPerdaProducao.pas' {frmTipoPerdaProducao},
  fMotivoPerdaProducao in 'fMotivoPerdaProducao.pas' {frmMotivoPerdaProducao},
  fAlteraChequeTerceiro in 'fAlteraChequeTerceiro.pas' {frmAlteraChequeTerceiro},
  fTipoImposto in 'fTipoImposto.pas' {frmTipoImposto},
  fTipoServico in 'fTipoServico.pas' {frmTipoServico},
  fTabelaProgressivaIRRF in 'fTabelaProgressivaIRRF.pas' {frmTabelaProgressivaIRRF},
  fSeparaEmbalaPedido in 'fSeparaEmbalaPedido.pas' {frmSeparaEmbalaPedido},
  fGerarPickingList in 'fGerarPickingList.pas' {frmGerarPickingList},
  fAutorizarFaturamento in 'fAutorizarFaturamento.pas' {frmAutorizarFaturamento},
  rPickList in 'rPickList.pas' {rptPickList},
  rPickList_Retrato in 'rPickList_Retrato.pas' {rptPickList_Retrato},
  fOperadorPCP in 'fOperadorPCP.pas' {frmOperadorPCP},
  fMotivoHoraImprodutiva in 'fMotivoHoraImprodutiva.pas' {frmMotivoHoraImprodutiva},
  fApontaHoraImprod in 'fApontaHoraImprod.pas' {frmApontaHoraImprod},
  fCenarioOrcamento in 'fCenarioOrcamento.pas' {frmCenarioOrcamento},
  fDistribuicaoOrcamento in 'fDistribuicaoOrcamento.pas' {frmDistribuicaoOrcamento},
  fDistribuicaoOrcamento_Valores in 'fDistribuicaoOrcamento_Valores.pas' {frmDistribuicaoOrcamento_Valores},
  fDistribuicaoOrcamento_Valores_Mensal in 'fDistribuicaoOrcamento_Valores_Mensal.pas' {frmDistribuicaoOrcamento_Valores_Mensal},
  rFluxoCaixaNovo in 'rFluxoCaixaNovo.pas' {rptFluxoCaixaNovo},
  rFluxoCaixaNovo_Tela in 'rFluxoCaixaNovo_Tela.pas' {rptFluxoCaixaNovo_Tela},
  rOrcamentoPrevReal in 'rOrcamentoPrevReal.pas' {rptOrcamentoPrevReal},
  rAcompanhaGiroProduto_view in 'rAcompanhaGiroProduto_view.pas' {rptAcompanhaGiroProduto_view},
  rOrcamentoPrevReal_View_Tela in 'rOrcamentoPrevReal_View_Tela.pas' {rptOrcamentoPrevReal_View_Tela},
  rOrcamentoPrevReal_View_Mensal in 'rOrcamentoPrevReal_View_Mensal.pas' {rptOrcamentoPrevReal_View_Mensal},
  rOrcamentoPrevReal_View_Trimestral in 'rOrcamentoPrevReal_View_Trimestral.pas' {rptOrcamentoPrevReal_View_Trimestral},
  fDashBoardERP in 'fDashBoardERP.pas' {frmDashBoardERP},
  fMetodoDistribOrcamento in 'fMetodoDistribOrcamento.pas' {frmMetodoDistribOrcamento},
  fPedidoDevVenda_LeituraXML in 'fPedidoDevVenda_LeituraXML.pas' {frmPedidoDevVenda_LeituraXML},
  dcVendaLojaResumido in 'dcVendaLojaResumido.pas' {dcmVendaLojaResumido},
  dcVendaLojaResumido_view in 'dcVendaLojaResumido_view.pas' {dcmVendaLojaResumido_view},
  fGrupoUsuario_Permissoes in 'fGrupoUsuario_Permissoes.pas' {frmGrupoUsuario_Permissoes},
  fAreaVendas in 'fAreaVendas.pas' {frmAreaVendas},
  fCanalDistribuicao in 'fCanalDistribuicao.pas' {frmCanalDistribuicao},
  fGrupoTotalCC in 'fGrupoTotalCC.pas' {frmGrupoTotalCC},
  fColunaGrupoTotalCC in 'fColunaGrupoTotalCC.pas' {frmColunaGrupoTotalCC},
  fGrupoCredito in 'fGrupoCredito.pas' {frmGrupoCredito},
  fConciliaBancoManual_TituloMovimentado_Cartao in 'fConciliaBancoManual_TituloMovimentado_Cartao.pas' {frmConciliaBancoManual_TituloMovimentado_Cartao},
  fDigitaSaldoDiarioContaBancaria in 'fDigitaSaldoDiarioContaBancaria.pas' {frmDigitaSaldoDiarioContaBancaria},
  fImpBoleto in 'fImpBoleto.pas' {frmImpBoleto},
  fimpBoletoAgrupamento in 'fimpBoletoAgrupamento.pas' {frmImpBoletoAgrupamento},
  fimpBoletoInstrucaoTitulo in 'fimpBoletoInstrucaoTitulo.pas' {frmImpBoletoInstrucaoTitulo},
  fMotivoCancelaSaldoPedido in 'fMotivoCancelaSaldoPedido.pas' {frmMotivoCancelaSaldoPedido},
  rFluxoCaixaRealizadoAnalitico in 'rFluxoCaixaRealizadoAnalitico.pas' {rptFluxoCaixaRealizadoAnalitico},
  rChequeEmitido_view in 'rChequeEmitido_view.pas' {rptChequeEmitido_view},
  rPedidoItemCancelado in 'rPedidoItemCancelado.pas' {rptPedidoItemCancelado},
  fPedidoItemCancelado_tela in 'fPedidoItemCancelado_tela.pas' {frmPedidoItemCancelado_tela},
  rPedidoItemCancelado_view in 'rPedidoItemCancelado_view.pas' {rptPedidoItemCancelado_view},
  fMotivoCancSaldoPed in 'fMotivoCancSaldoPed.pas' {frmMotivoCancSaldoPed},
  fContaBancaria_Conf_CNAB in 'fContaBancaria_Conf_CNAB.pas' {frmContaBancaria_Conf_CNAB},
  fGeracaoBorderoRecebimento in 'fGeracaoBorderoRecebimento.pas' {frmGeracaoBorderoRecebimento},
  fGeracaoBorderoRecebimento_titulos in 'fGeracaoBorderoRecebimento_titulos.pas' {frmGeracaoBorderoRecebimento_titulos},
  fGeraRemessaRecebimento in 'fGeraRemessaRecebimento.pas' {frmGeraRemessaRecebimento},
  fGeraRemessaRecebimento_titulo in 'fGeraRemessaRecebimento_titulo.pas' {frmGeraRemessaRecebimento_titulo},
  fCancelaBorderoRecebimento in 'fCancelaBorderoRecebimento.pas' {frmCancelaBorderoRecebimento},
  fCancelaBorderoRecebimento_titulo in 'fCancelaBorderoRecebimento_titulo.pas' {frmCancelaBorderoRecebimento_titulo},
  fDoctoFiscal_PendenteTransmissao in 'fDoctoFiscal_PendenteTransmissao.pas' {frmDoctoFiscal_PendenteTransmissao},
  fConsultaStatusServico in 'fConsultaStatusServico.pas' {frmConsultaStatusServico},
  fInutilizacaoNFe_SCAN in 'fInutilizacaoNFe_SCAN.pas' {frmInutilizacaoNFe_SCAN},
  fInutilizacaoNFe_SEFAZ in 'fInutilizacaoNFe_SEFAZ.pas' {frmInutilizacaoNFe_SEFAZ},
  fPedidoVendaSimples_SelecaoAgrupada in 'fPedidoVendaSimples_SelecaoAgrupada.pas' {frmPedidoVendaSimples_SelecaoAgrupada},
  fRecebimento in 'fRecebimento.pas' {frmRecebimento},
  fRecebimentoCompletoMultiplosPedidos in 'fRecebimentoCompletoMultiplosPedidos.pas' {frmRecebimentoCompletoMultiplosPedidos},
  fGerarSintegra in 'fGerarSintegra.pas',
  fContaContabil in 'fContaContabil.pas' {frmContaContabil},
  fAgrupamentoCusto in 'fAgrupamentoCusto.pas' {frmAgrupamentoCusto},
  fAgrupamentoCusto_Estrutura in 'fAgrupamentoCusto_Estrutura.pas' {frmAgrupamentoCusto_Estrutura},
  fAgrupamentoCusto_AddSubConta in 'fAgrupamentoCusto_AddSubConta.pas' {frmAgrupamentoCusto_AddSubConta},
  fAgrupamentoCusto_VinculaCC in 'fAgrupamentoCusto_VinculaCC.pas' {frmAgrupamentoCusto_VinculaCC},
  fContaContabil_Vinculacoes in 'fContaContabil_Vinculacoes.pas' {frmContaContabil_Vinculacoes},
  fVinculacaoAgrupamentoCusto in 'fVinculacaoAgrupamentoCusto.pas' {frmVinculacaoAgrupamentoCusto},
  fAgrupamentoContabil in 'fAgrupamentoContabil.pas' {frmAgrupamentoContabil},
  fAgrupamentoContabil_Estrutura in 'fAgrupamentoContabil_Estrutura.pas' {frmAgrupamentoContabil_Estrutura},
  fAgrupamentoContabil_AddSubConta in 'fAgrupamentoContabil_AddSubConta.pas' {frmAgrupamentoContabil_AddSubConta},
  fAgrupamentoContabil_VinculaConta in 'fAgrupamentoContabil_VinculaConta.pas' {frmAgrupamentoContabil_VinculaConta},
  fCC in 'fCC.pas' {frmCC},
  fVinculacaoAgrupamentoContabil in 'fVinculacaoAgrupamentoContabil.pas' {frmVinculacaoAgrupamentoContabil},
  fHistoricoPadrao in 'fHistoricoPadrao.pas' {frmHistoricoPadrao},
  rPosicaoFinancContratoImobiliario in 'rPosicaoFinancContratoImobiliario.pas' {rptPosicaoFinancContratoImobiliario},
  rPossibilidadeFaturamento_view in 'rPossibilidadeFaturamento_view.pas' {rptPossibilidadeFaturamento_view},
  rPosicaoFinancContratoImobiliario_Diario_view in 'rPosicaoFinancContratoImobiliario_Diario_view.pas' {rptPosicaoFinancContratoImobiliario_Diario_view},
  rPosicaoFinancContratoImobiliario_ResumoEmpreendimento_view in 'rPosicaoFinancContratoImobiliario_ResumoEmpreendimento_view.pas' {rptPosicaoFinancContratoImobiliario_ResumoEmpreendimento_view},
  fMRP_DetalhaEmpenho in 'fMRP_DetalhaEmpenho.pas' {frmMRP_DetalhaEmpenho},
  rChequeDevolv_view in 'rChequeDevolv_view.pas' {rptChequeDevolv_view},
  fPdvDesconto in 'fPdvDesconto.pas' {frmPdvDesconto},
  fAtribuiMarca in 'fAtribuiMarca.pas' {frmAtribuiMarca},
  fPosTituloAberto_Cliente_Simplificado in 'fPosTituloAberto_Cliente_Simplificado.pas' {frmPosTituloAberto_Cliente_Simplificado},
  rPosTituloAberto_Cliente_Simplificado in 'rPosTituloAberto_Cliente_Simplificado.pas' {rptPosTituloAberto_cliente_simplificado},
  fConsignacao in 'fConsignacao.pas' {frmConsignacao},
  fConsignacao_Leitor in 'fConsignacao_Leitor.pas' {frmConsignacao_Leitor},
  rProtocoloConsignacao_view in 'rProtocoloConsignacao_view.pas' {rptProtocoloConsignacao_view: TQuickRep},
  rRomaneio_Retrato in 'rRomaneio_Retrato.pas' {rptRomaneio_Retrato},
  rRegistroEntradaFuncionario in 'rRegistroEntradaFuncionario.pas' {rptRegistroEntradaFuncionario},
  rRegistroEntradaFuncionario_view in 'rRegistroEntradaFuncionario_view.pas' {rptRegistroEntradaFuncionario_view},
  rPicking_Retrato in 'rPicking_Retrato.pas' {rptPicking_Retrato},
  fOrdemCompraVarejo_Grade in 'fOrdemCompraVarejo_Grade.pas' {frmOrdemCompraVarejo_Grade},
  rPosicaoEstoqueGeral in 'rPosicaoEstoqueGeral.pas' {rptPosicaoEstoqueGeral},
  fPlantaMRP in 'fPlantaMRP.pas' {frmPlantaMRP},
  fInventarioLoja_Contagem_ExibeLotes_LocalLote in 'fInventarioLoja_Contagem_ExibeLotes_LocalLote.pas' {frmInventarioLoja_Contagem_ExibeLotes_LocalLote},
  fGrupoTerceiro in 'fGrupoTerceiro.pas' {frmGrupoTerceiro},
  fMonitorProducao_Exibe in 'fMonitorProducao_Exibe.pas' {frmMonitorProducao_Exibe},
  fMonitorProducao_Exibe_ItensPedido in 'fMonitorProducao_Exibe_ItensPedido.pas' {frmMonitorProducao_Exibe_ItensPedido},
  rConsultaSitCadastralCliente in 'rConsultaSitCadastralCliente.pas' {rptConsultaSitCadastralCliente},
  rConsultaSitCadastralCliente_View in 'rConsultaSitCadastralCliente_View.pas' {rptConsultaSitCadastralCliente_View},
  rRelCredAberto in 'rRelCredAberto.pas' {rptRelCredAberto},
  rRelCredAberto_view in 'rRelCredAberto_view.pas' {rptRelCredAberto_view},
  rRelCredQuitado in 'rRelCredQuitado.pas' {rptRelCredQuitado},
  rRelCredQuitado_view in 'rRelCredQuitado_view.pas' {rptRelCredQuitado_view},
  fConsultaRemessaNumerario in 'fConsultaRemessaNumerario.pas' {frmConsultaRemessaNumerario},
  fConsultaRemessaNumerario_Lancamentos in 'fConsultaRemessaNumerario_Lancamentos.pas' {frmConsultaRemessaNumerario_Lancamentos},
  fBloqueioClienteInadimplente in 'fBloqueioClienteInadimplente.pas' {frmBloqueioClienteInadimplente},
  fPerfilCobranca in 'fPerfilCobranca.pas' {frmPerfilCobranca},
  fSegmentoCobranca in 'fSegmentoCobranca.pas' {frmSegmentoCobranca},
  fServidorReplicacao_FilaTransmissao in 'fServidorReplicacao_FilaTransmissao.pas' {frmServidorReplicacao_FilaTransmissao},
  fCartaCorrecaoNFe_HistoricoCCe in 'fCartaCorrecaoNFe_HistoricoCCe.pas' {frmCartaCorrecaoNFe_HistoricoCCe},
  fTransfEst_Recebimento_LoteSerial in 'fTransfEst_Recebimento_LoteSerial.pas' {frmTransfEst_Recebimento_LoteSerial},
  fRecebimento_BaixaDocumento in 'fRecebimento_BaixaDocumento.pas' {frmRecebimento_BaixaDocumento},
  fCartaCorrecaoNFe in 'fCartaCorrecaoNFe.pas' {frmCartaCorrecaoNFe},
  fCartaCorrecaoNFe_GeracaoCCe in 'fCartaCorrecaoNFe_GeracaoCCe.pas' {frmCartaCorrecaoNFe_GeracaoCCe},
  rRelacaoTerceiroRepresentante in 'rRelacaoTerceiroRepresentante.pas' {rptRelacaoTerceiroRepresentante},
  rRelacaoTerceiroRepresentante_View in 'rRelacaoTerceiroRepresentante_View.pas' {rptRelacaoTerceiroRepresentante_View},
  rRelSobraCaixa in 'rRelSobraCaixa.pas' {rptRelSobraCaixa},
  rRelSobraCaixa_view in 'rRelSobraCaixa_view.pas' {rptRelSobraCaixa_view},
  rCartaCorrecaoNFe_View in 'rCartaCorrecaoNFe_View.pas' {rptCartaCorrecaoNFe_View},
  fImportaAliquotaIBPT in 'fImportaAliquotaIBPT.pas' {frmImportaAliquotaIBPT},
  rRankingCompraCliente in 'rRankingCompraCliente.pas' {rptRankingCompraCliente},
  rRankingCompraCliente_view in 'rRankingCompraCliente_view.pas' {rptRankingCompraCliente_view},
  rTituloReabilitado in 'rTituloReabilitado.pas' {rptTituloReabilitado},
  rClientesVarejoSemCompraPeriodo in 'rClientesVarejoSemCompraPeriodo.pas' {rptClientesVarejoSemCompraPeriodo},
  rClientesVarejoSemCompraPeriodo_View in 'rClientesVarejoSemCompraPeriodo_View.pas' {rptClientesVarejoSemCompraPeriodo_View},
  fTabIBPT in 'fTabIBPT.pas' {frmTabIBPT},
  fDescricaoProdWeb in 'fDescricaoProdWeb.pas' {frmDescricaoProdWeb},
  fProdutoWeb in 'fProdutoWeb.pas' {frmProdutoWeb},
  fTabFichaTecnica in 'fTabFichaTecnica.pas' {frmTabFichaTecnica},
  fGerenciaProdutoWeb_AplicaPrecoProduto in 'fGerenciaProdutoWeb_AplicaPrecoProduto.pas' {frmGerenciaProdutoWeb_AplicaPrecoProduto},
  fPedidoVendaWeb in 'fPedidoVendaWeb.pas' {frmPedidoVendaWeb},
  rPosicaoInadimplenciaAnal_view in 'rPosicaoInadimplenciaAnal_view.pas' {relPosicaoInadimplenciaAnal_view},
  rTabelaPreco in 'rTabelaPreco.pas' {relTabelaPreco},
  fMonitorIntegracaoWeb in 'fMonitorIntegracaoWeb.pas' {frmMonitorIntegracaoWeb},
  fMonitorTransmissaoWeb in 'fMonitorTransmissaoWeb.pas' {frmMonitorTransmissaoWeb},
  fClienteVarejoPessoaFisica_Simples in 'fClienteVarejoPessoaFisica_Simples.pas' {frmClienteVarejoPessoaFisica_Simples},
  fEditorHTML in 'fEditorHTML.pas' {frmEditorHTML},
  rPosicaoInadimplenciaAnalTerc_view in 'rPosicaoInadimplenciaAnalTerc_view.pas' {relPosicaoInadimplenciaAnalTerc_view},
  rLiberacaoDiverReceb in 'rLiberacaoDiverReceb.pas' {rptLiberacaoDiverReceb},
  rLiberacaoDiverReceb_view in 'rLiberacaoDiverReceb_view.pas' {rptLiberacaoDiverReceb_view},
  fCalendario in 'fCalendario.pas' {frmCalendario},
  fTransacaoTEF in 'fTransacaoTEF.pas' {frmTransacaoTEF},
  fInputTEF in 'fInputTEF.pas' {frmInputTEF},
  fCaixa_CredErro in 'fCaixa_CredErro.pas' {frmCaixa_CredErro},
  fGerenciaProdutoWeb_AplicaDimensaoProduto in 'fGerenciaProdutoWeb_AplicaDimensaoProduto.pas' {frmGerenciaProdutoWeb_AplicaDimensaoProduto},
  fMonitorPedidoWeb in 'fMonitorPedidoWeb.pas' {frmMonitorPedidoWeb},
  fPedidoDevVenda in 'fPedidoDevVenda.pas' {frmPedidoDevVenda},
  rItensPorVendaPar in 'rItensPorVendaPar.pas' {rptItensPorVendaPar},
  rItensPorVendaPar_view in 'rItensPorVendaPar_view.pas' {rptItensPorVendaPar_view},
  fEnviaLoteXMLNFe in 'fEnviaLoteXMLNFe.pas' {frmEnviaLoteXMLNFe},
  fFollowUP_Requisicao in 'fFollowUP_Requisicao.pas' {frmFollowUP_Requisicao},
  fFiltroFollowUp in 'fFiltroFollowUp.pas' {frmFiltroFollowUp},
  fFollowUP_Requisicao_Filtro in 'fFollowUP_Requisicao_Filtro.pas' {frmFollowUP_Requisicao_Filtro},
  rPosicaoPedidoVendaWeb in 'rPosicaoPedidoVendaWeb.pas' {rptPosicaoPedidoVendaWeb},
  rPosicaoPedidoVendaWeb_view in 'rPosicaoPedidoVendaWeb_view.pas' {rptPosicaoPedidoVendaWeb_view},
  fFollowUP_Requisicao_NovoFollowUp in 'fFollowUP_Requisicao_NovoFollowUp.pas' {frmFollowUP_Requisicao_NovoFollowUp},
  fPedidoDevVenda_ConsultaItens in 'fPedidoDevVenda_ConsultaItens.pas' {frmPedidoDevVenda_ConsultaItens},
  fRecebimentoLeituraXML in 'fRecebimentoLeituraXML.pas' {frmRecebimentoLeituraXML},
  fAnaliseContatoCobranca in 'fAnaliseContatoCobranca.pas' {frmAnaliseContatoCobranca},
  fSobre in 'fSobre.pas' {frmSobre},
  fMonitorOcorrencias in 'fMonitorOcorrencias.pas' {frmMonitorOcorrencias},
  fTipoOcorrencia in 'fTipoOcorrencia.pas' {frmTipoOcorrencia},
  fRegAtendOcorrencia in 'fRegAtendOcorrencia.pas' {frmRegAtendOcorrencia},
  fRegAtendOcorrencia_Atendimento in 'fRegAtendOcorrencia_Atendimento.pas' {frmRegAtendOcorrencia_Atendimento},
  fFollowUP_Ocorrencia in 'fFollowUP_Ocorrencia.pas' {frmFollowUP_Ocorrencia},
  fCobranca_VisaoContato_Contato_SelContato in 'fCobranca_VisaoContato_Contato_SelContato.pas' {frmCobranca_VisaoContato_Contato_SelContato},
  fFollowUP_Ocorrencia_Filtro in 'fFollowUP_Ocorrencia_Filtro.pas' {frmFollowUp_Ocorrencia_Filtro},
  fFollowUP_Ocorrencia_NovoFollowUP in 'fFollowUP_Ocorrencia_NovoFollowUP.pas' {frmFollowUP_Ocorrencia_NovoFollowUP},
  fCentroDistribuicao in 'fCentroDistribuicao.pas' {frmCentroDistribuicao},
  fCriterioSolicitacaoCD in 'fCriterioSolicitacaoCD.pas' {frmCriterioSolicitacaoCD},
  fMonitorEmpenho in 'fMonitorEmpenho.pas' {frmMonitorEmpenho},
  fMonitorEmpenho_Itens in 'fMonitorEmpenho_Itens.pas' {frmMonitorEmpenho_Itens},
  fPedidoComercial_PreDistribuicao in 'fPedidoComercial_PreDistribuicao.pas' {frmPedidoComercial_PreDistribuicao},
  fTipoFrequenciaDistribuicao in 'fTipoFrequenciaDistribuicao.pas' {frmTipoFrequenciaDistribuicao},
  rItensSeparacaoDistribuicao in 'rItensSeparacaoDistribuicao.pas' {relItensSeparacaoDistribuicao_view},
  rItensPreDistribuicao_view in 'rItensPreDistribuicao_view.pas' {relItensPreDistribuicao_view},
  fConferenciaProdutoPadrao in 'fConferenciaProdutoPadrao.pas' {frmConferenciaProdutoPadrao},
  fRecebimento_Coferencia in 'fRecebimento_Coferencia.pas' {frmRecebimento_Conferencia},
  fFuncoesSAT in 'fFuncoesSAT.pas' {frmFuncoesSAT},
  fGuiaOperacional in 'fGuiaOperacional.pas' {frmGuiaOperacional},
  fFeedback in 'fFeedback.pas' {frmFeedback},
  fHistoricoVersoes in 'fHistoricoVersoes.pas' {frmHistoricoVersoes},
  fEnviaEmailPadrao in 'fEnviaEmailPadrao.pas' {frmEnviaEmailPadrao},
  rFichaCobradora_Individual_view in 'rFichaCobradora_Individual_view.pas' {rptFichaCobradora_Individual_view},
  rFichaCobradora_Individual in 'rFichaCobradora_Individual.pas' {relFichaCobradora_Individual},
  fModeloDocumento in 'fModeloDocumento.pas' {frmModeloDocumento},
  rHistoricoEmpenho in 'rHistoricoEmpenho.pas' {rRelHistoricoEmpenho},
  rHistoricoEmpenho_view in 'rHistoricoEmpenho_view.pas' {rptHistoricoEmpenho_view},
  fEditorHTML_PropTabela in 'fEditorHTML_PropTabela.pas' {frmEditorHTML_PropTabela},
  fParam_TipoPedido_Ajuda in 'fParam_TipoPedido_Ajuda.pas' {frmParam_TipoPedido_Ajuda},
  fServidorSAT in 'fServidorSAT.pas' {frmServidorSAT},
  fActivityMonitorSQL in 'fActivityMonitorSQL.pas' {frmActivityMonitorSQL},
  fProcessaClassificCRM in 'fProcessaClassificCRM.pas' {frmProcessaClassificCRM},
  fClassifcCRM in 'fClassifcCRM.pas' {frmClassifcCRM},
  fManutTransacaoCartao in 'fManutTransacaoCartao.pas' {frmManutTransacaoCartao},
  fGrupoCliente in 'fGrupoCliente.pas' {frmGrupoCliente},
  fMetaDescontoMensal in 'fMetaDescontoMensal.pas' {frmMetaDescontoMensal},
  fMetaDescontoMensal_Vendedor in 'fMetaDescontoMensal_Vendedor.pas' {frmMetaDescontoMensal_vendedor},
  fAcompanhaMetaDescVendedor in 'fAcompanhaMetaDescVendedor.pas' {frmAcompanhaMetaDescVendedor},
  rPosicaoEstoqueLoja in 'rPosicaoEstoqueLoja.pas' {rptPosicaoEstoqueLoja},
  fConfigGrupoCliente in 'fConfigGrupoCliente.pas' {frmConfigGrupoCliente},
  fNotificaVencTitEmail in 'fNotificaVencTitEmail.pas' {frmNotificaVencTitEmail},
  fMonitorTransferencia_Item in 'fMonitorTransferencia_Item.pas' {frmMonitorTransferencia_Item},
  fInventario_Monitor in 'fInventario_Monitor.pas' {frmInventario_Monitor},
  rVendaxVenda in 'rVendaxVenda.pas' {rptVendaxVenda},
  rVendaxVenda_View in 'rVendaxVenda_View.pas' {rptVendaxVenda_View},
  fGerenciaProdutoWeb in 'fGerenciaProdutoWeb.pas' {frmGerenciaProdutoWeb},
  fProdutoWeb_AplicaDimensaoProduto in 'fProdutoWeb_AplicaDimensaoProduto.pas' {frmProdutoWeb_AplicaDimensaoProduto},
  fProdutoWeb_AplicaPrecoProduto in 'fProdutoWeb_AplicaPrecoProduto.pas' {frmProdutoWeb_AplicaPrecoProduto},
  rDoctoFiscalEmitido in 'rDoctoFiscalEmitido.pas' {rptDoctoFiscalEmitido},
  fFormaPagtoWeb in 'fFormaPagtoWeb.pas' {frmFormaPagtoWeb},
  rDoctoFiscalEmitido_view in 'rDoctoFiscalEmitido_view.pas' {rptDoctoFiscalEmitido_view},
  dcVendaLoja in 'dcVendaLoja.pas' {dcmVendaLoja},
  dcVendaLoja_view in 'dcVendaLoja_view.pas' {dcmVendaLoja_view},
  fInventarioLoja_Contagem_ContagemItemLote in 'fInventarioLoja_Contagem_ContagemItemLote.pas' {frmInventarioLoja_Contagem_ContagemItemLote},
  fRelatorio_Padrao in 'fRelatorio_Padrao.pas' {frmRelatorio_Padrao},
  fCaixa_AtualizaCliente in 'fCaixa_AtualizaCliente.pas' {frmCaixa_AtualizaCliente},
  rInadimplencia in 'rInadimplencia.pas' {rptInadimplencia},
  rConsultasSCPC in 'rConsultasSCPC.pas' {rptConsultasSCPC},
  Cappta_Gp_Api_Com_TLB in '..\..\..\..\..\Program Files (x86)\Borland\Delphi7\Imports\Cappta_Gp_Api_Com_TLB.pas',
  fCapptAPI in 'fCapptAPI.pas' {frmCapptAPI},
  fCadastroMetasDia in 'fCadastroMetasDia.pas' {frmCadastroMetasDia},
  fValidaCNPJ in 'fValidaCNPJ.pas' {frmValidaCNPJ},
  fOrdemPagto in 'fOrdemPagto.pas' {frmOrdemPagto};

{$R *.res}

begin

  SplashScreen := TSplashScreen.Create(Application) ;
  SplashScreen.Show;
  Application.Initialize; //this line exists!
  SplashScreen.Update;

  Application.Title := 'ER2Soft';
  Application.CreateForm(TfrmMenu, frmMenu);
  Application.CreateForm(TfrmCadastro_Padrao, frmCadastro_Padrao);
  Application.CreateForm(TfrmProcesso_Padrao, frmProcesso_Padrao);
  Application.CreateForm(TfrmLookup_Padrao, frmLookup_Padrao);
  Application.CreateForm(TfrmConsulta_Template, frmConsulta_Template);
  Application.CreateForm(TfrmBuscaPK, frmBuscaPK);
  Application.CreateForm(TfrmAuditoria, frmAuditoria);
  Application.CreateForm(TfrmSelecServer, frmSelecServer);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TfrmSelecLoja, frmSelecLoja);
  Application.CreateForm(TfrmSelecEmpresa, frmSelecEmpresa);
  Application.CreateForm(TfrmErroPadrao, frmErroPadrao);
  Application.CreateForm(TfrmMensagem, frmMensagem);
  Application.CreateForm(TfrmAltSenha, frmAltSenha);
  Application.CreateForm(TfrmAddServer, frmAddServer);
  Application.CreateForm(TfrmSelecEmpresa, frmSelecEmpresa);
  Application.CreateForm(TfrmAlertas, frmAlertas);
  Application.CreateForm(TfrmImpDFPadrao, frmImpDFPadrao);
  Application.CreateForm(TfrmPdvDesconto, frmPdvDesconto);
  Application.CreateForm(TrptConsultasSCPC, rptConsultasSCPC);
  {  Application.CreateForm(TfrmPDV, frmPDV);
  Application.CreateForm(TfrmRoteiroProducao_Etapas, frmRoteiroProducao_Etapas);
  Application.CreateForm(TfrmMemoText, frmMemoText);
  Application.CreateForm(TfrmOP, frmOP);
  Application.CreateForm(TfrmOP_ProdutoEtapa, frmOP_ProdutoEtapa);
  Application.CreateForm(TrptRequisicaoOP, rptRequisicaoOP);
  Application.CreateForm(TfrmEntradaProducao, frmEntradaProducao);
  Application.CreateForm(TfrmMonitorOP, frmMonitorOP);
  Application.CreateForm(TfrmMonitorOP_DataInicio, frmMonitorOP_DataInicio);
  Application.CreateForm(TfrmAcompProducaoPedidoVenda, frmAcompProducaoPedidoVenda);
  Application.CreateForm(TrptDiarioAuxiliarClientes, rptDiarioAuxiliarClientes);
  Application.CreateForm(TrptDiarioAuxiliarClientes_View, rptDiarioAuxiliarClientes_View);
  Application.CreateForm(TfrmGerarRequisicaoOP, frmGerarRequisicaoOP);
  Application.CreateForm(TfrmGerarRequisicaoOP_ExibeRequisicoes, frmGerarRequisicaoOP_ExibeRequisicoes);
  Application.CreateForm(TrptRazaoAuxiliarFornecedores, rptRazaoAuxiliarFornecedores);
  Application.CreateForm(TrptRazaoAuxiliarFornecedores_View, rptRazaoAuxiliarFornecedores_View);
  Application.CreateForm(TrptOP, rptOP);
  Application.CreateForm(TfrmForecast, frmForecast);
  Application.CreateForm(TfrmConversaoUndMedida, frmConversaoUndMedida);
  Application.CreateForm(TfrmBemPatrim, frmBemPatrim);
  Application.CreateForm(TfrmClasseBemPatrim, frmClasseBemPatrim);
  Application.CreateForm(TfrmConfigSubGrupoBemPatrim, frmConfigSubGrupoBemPatrim);
  Application.CreateForm(TfrmGrupoBemPatrim, frmGrupoBemPatrim);
  Application.CreateForm(TfrmMotBaixaBemPatrim, frmMotBaixaBemPatrim);
  Application.CreateForm(TfrmSetorBemPatrim, frmSetorBemPatrim);
  Application.CreateForm(TfrmCadastroCentroProdutivo, frmCadastroCentroProdutivo);
  Application.CreateForm(TfrmEtapasProducao, frmEtapasProducao);
  Application.CreateForm(TfrmTipoOrdemProducao, frmTipoOrdemProducao);
  Application.CreateForm(TfrmGrupoCalculoTempo, frmGrupoCalculoTempo);
  Application.CreateForm(TfrmItensPedido, frmItensPedido);
  Application.CreateForm(TfrmOscilacaoEstoque_View, frmOscilacaoEstoque_View);
  Application.CreateForm(TrptOscilacaoEstoque, rptOscilacaoEstoque);
  Application.CreateForm(TrptOscilacaoEstoque_View, rptOscilacaoEstoque_View);
  Application.CreateForm(TrptPosicaoEstoqueGeral, rptPosicaoEstoqueGeral);
  Application.CreateForm(TrptPosicaoEstoqueGeral_view, rptPosicaoEstoqueGeral_view);
  Application.CreateForm(TfrmTipoDocumentoFiscal, frmTipoDocumentoFiscal);
  Application.CreateForm(TfrmResumoDesempSemanalGeral, frmResumoDesempSemanalGeral);
  Application.CreateForm(TfrmPais, frmPais);
  Application.CreateForm(TfrmConsulta_Template, frmConsulta_Template);
  Application.CreateForm(TfrmAplicacao_CONS, frmAplicacao_CONS);
  Application.CreateForm(TfrmAplicacao_Cad, frmAplicacao_Cad);
  Application.CreateForm(TfrmMenu_Cad, frmMenu_Cad);
  Application.CreateForm(TfrmUsuario, frmUsuario);
  Application.CreateForm(TfrmGrupoUsuario_Cad, frmGrupoUsuario_Cad);
  Application.CreateForm(TfrmTabelaConsulta, frmTabelaConsulta);
  Application.CreateForm(TfrmGrupoEspTit, frmGrupoEspTit);
  Application.CreateForm(TfrmEspTit, frmEspTit);
  Application.CreateForm(TfrmTipoSP, frmTipoSP);
  Application.CreateForm(TfrmGrupoCategFinanc, frmGrupoCategFinanc);
  Application.CreateForm(TfrmCategFinanc, frmCategFinanc);
  Application.CreateForm(TfrmUnidadeNegocio, frmUnidadeNegocio);
  Application.CreateForm(TfrmFormaPagto, frmFormaPagto);
  Application.CreateForm(TfrmMoeda, frmMoeda);
  Application.CreateForm(TfrmBanco, frmBanco);
  Application.CreateForm(TfrmEmpresa, frmEmpresa);
  Application.CreateForm(TfrmRegiao, frmRegiao);
  Application.CreateForm(TfrmCentroCusto, frmCentroCusto);
  Application.CreateForm(TfrmArvoreCC, frmArvoreCC);
  Application.CreateForm(TfrmOperacao, frmOperacao);
  Application.CreateForm(TfrmSP, frmSP);
  Application.CreateForm(TrptImpSP, rptImpSP);
  Application.CreateForm(TfrmAltVencTit, frmAltVencTit);
  Application.CreateForm(TfrmSaldoInicialContaBancaria, frmSaldoInicialContaBancaria);
  Application.CreateForm(TfrmAutorizaSP, frmAutorizaSP);
  Application.CreateForm(TfrmTituloProv, frmTituloProv);
  Application.CreateForm(TrptFluxoCaixa, rptFluxoCaixa);
  Application.CreateForm(TrptFluxoCaixaRealizado_view, rptFluxoCaixaRealizado_view);
  Application.CreateForm(TfrmPlanoConta, frmPlanoConta);
  Application.CreateForm(TfrmEstornaContab, frmEstornaContab);
  Application.CreateForm(TdcmVendaProduto, dcmVendaProduto);
  Application.CreateForm(TdcmMaiorCredor, dcmMaiorCredor);
  Application.CreateForm(TfrmConsTituloAbertoWERP, frmConsTituloAbertoWERP);
  Application.CreateForm(TrptPerfOrcamento_view, rptPerfOrcamento_view);
  Application.CreateForm(TrptMovTit, rptMovTit);
  Application.CreateForm(TrptTransferencia_view, rptTransferencia_view);
  Application.CreateForm(TfrmContabiliza, frmContabiliza);
  Application.CreateForm(TrptVendaFatDia, rptVendaFatDia);
  Application.CreateForm(TrptRankingFatAnualProduto_view, rptRankingFatAnualProduto_view);
  Application.CreateForm(TfrmBaixaColetiva, frmBaixaColetiva);
  Application.CreateForm(TfrmProdutoFormulado, frmProdutoFormulado);
  Application.CreateForm(TfrmGerarGrade, frmGerarGrade);
  Application.CreateForm(TfrmDepartamento, frmDepartamento);
  Application.CreateForm(TfrmCorPredom, frmCorPredom);
  Application.CreateForm(TfrmConsultaChequeResponsavel, frmConsultaChequeResponsavel);
  Application.CreateForm(TfrmSugerirCheques, frmSugerirCheques);
  Application.CreateForm(TfrmMarca, frmMarca);
  Application.CreateForm(TfrmTipoPedido, frmTipoPedido);
  Application.CreateForm(TfrmOrcamentoEnergy, frmOrcamentoEnergy);
  Application.CreateForm(TfrmGrade, frmGrade);
  Application.CreateForm(TrptPerfOrcamento, rptPerfOrcamento);
  Application.CreateForm(TfrmIncLinha, frmIncLinha);
  Application.CreateForm(TrptRomaneio, rptRomaneio);
  Application.CreateForm(TfrmCheque, frmCheque);
  Application.CreateForm(TfrmChequeDevolvido, frmChequeDevolvido);
  Application.CreateForm(TfrmGrupoProduto, frmGrupoProduto);
  Application.CreateForm(TfrmAutoriza_Pedido, frmAutoriza_Pedido);
  Application.CreateForm(TfrmPosTituloAberto_Cliente, frmPosTituloAberto_Cliente);
  Application.CreateForm(TfrmPosTituloLiquidado, frmPosTituloLiquidado);
  Application.CreateForm(TrptPosTituloAberto_cliente, rptPosTituloAberto_cliente);
  Application.CreateForm(TfrmFollowUPWERP, frmFollowUPWERP);
  Application.CreateForm(TfrmNovo_FollowUp, frmNovo_FollowUp);
  Application.CreateForm(TfrmManutPedido, frmManutPedido);
  Application.CreateForm(TfrmProdutoERP, frmProdutoERP);
  Application.CreateForm(TfrmEmbFormula, frmEmbFormula);
  Application.CreateForm(TfrmParam_TipoPedido, frmParam_TipoPedido);
  Application.CreateForm(TfrmGrupoImposto, frmGrupoImposto);
  Application.CreateForm(TfrmNaturezaOperacao, frmNaturezaOperacao);
  Application.CreateForm(TfrmSerieFiscal, frmSerieFiscal);
  Application.CreateForm(TfrmRegraICMS, frmRegraICMS);
  Application.CreateForm(TfrmGeraFaturamento, frmGeraFaturamento);
  Application.CreateForm(TfrmItemFaturar, frmItemFaturar);
  Application.CreateForm(TfrmNC, frmNC);
  Application.CreateForm(TfrmLiberaFaturamento, frmLiberaFaturamento);
  Application.CreateForm(TfrmImpDoctoFiscal, frmImpDoctoFiscal);
  Application.CreateForm(TfrmDadoComplDoctoFiscal, frmDadoComplDoctoFiscal);
  Application.CreateForm(TfrmOperacaoEstoque, frmOperacaoEstoque);
  Application.CreateForm(TfrmGrupoEstoque, frmGrupoEstoque);
  Application.CreateForm(TfrmLocalEstoque, frmLocalEstoque);
  Application.CreateForm(TfrmOperacaoLocalEstoque, frmOperacaoLocalEstoque);
  Application.CreateForm(TfrmRecebimento, frmRecebimento);
  Application.CreateForm(TfrmConsultaItemPedidoAberto, frmConsultaItemPedidoAberto);
  Application.CreateForm(TfrmTerceiro, frmTerceiro);
  Application.CreateForm(TfrmCancDoctoFiscal, frmCancDoctoFiscal);
  Application.CreateForm(TfrmConfigAmbiente, frmConfigAmbiente);
  Application.CreateForm(TfrmModImpDF, frmModImpDF);
  Application.CreateForm(TfrmImpBOLPadrao, frmImpBOLPadrao);
  Application.CreateForm(TfrmPrecoEspPedCom, frmPrecoEspPedCom);
  Application.CreateForm(TrptMovTit_view, rptMovTit_view);
  Application.CreateForm(TfrmLibDivRec, frmLibDivRec);
  Application.CreateForm(TfrmGrupoEconomico, frmGrupoEconomico);
  Application.CreateForm(TrptChequeRecebido, rptChequeRecebido);
  Application.CreateForm(TrptRequisicao_view, rptRequisicao_view);
  Application.CreateForm(TrptRankingVendaCliente, rptRankingVendaCliente);
  Application.CreateForm(TrptRankingVendaCliente_view, rptRankingVendaCliente_view);
  Application.CreateForm(TrptEvolucaoVenda, rptEvolucaoVenda);
  Application.CreateForm(TfrmNovaReferencia, frmNovaReferencia);
  Application.CreateForm(TrptVendaPeriodoProduto_view, rptVendaPeriodoProduto_view);
  Application.CreateForm(TrptFaturamentoPeriodo, rptFaturamentoPeriodo);
  Application.CreateForm(TrptFaturamentoPeriodo_view, rptFaturamentoPeriodo_view);
  Application.CreateForm(TrptFaturamentoPeriodoProduto, rptFaturamentoPeriodoProduto);
  Application.CreateForm(TrptFaturamentoPeriodoProduto_view, rptFaturamentoPeriodoProduto_view);
  Application.CreateForm(TrptVendaPeriodo, rptVendaPeriodo);
  Application.CreateForm(TrptEvolucaoVenda_view, rptEvolucaoVenda_view);
  Application.CreateForm(TfrmPedCompraContrato, frmPedCompraContrato);
  Application.CreateForm(TrptPedCom_Contrato, rptPedCom_Contrato);
  Application.CreateForm(TfrmPedidoVenda, frmPedidoVenda);
  Application.CreateForm(TfrmDoctoFiscal, frmDoctoFiscal);
  Application.CreateForm(TrptVendaPeriodo_view, rptVendaPeriodo_view);
  Application.CreateForm(TfrmConfirmaReceb, frmConfirmaReceb);
  Application.CreateForm(TfrmClasseProduto, frmClasseProduto);
  Application.CreateForm(TfrmColecaoProduto, frmColecaoProduto);
  Application.CreateForm(TfrmCondPagtoPDV, frmCondPagtoPDV);
  Application.CreateForm(TfrmConsultaMovEstoque, frmConsultaMovEstoque);
  Application.CreateForm(TfrmTipoReceb, frmTipoReceb);
  Application.CreateForm(TfrmConsultaPosicaoEstoque, frmConsultaPosicaoEstoque);
  Application.CreateForm(TfrmTipoDiverg, frmTipoDiverg);
  Application.CreateForm(TfrmMotBloqPed, frmMotBloqPed);
  Application.CreateForm(TfrmSelecMotBloqPed, frmSelecMotBloqPed);
  Application.CreateForm(TdcmPlanoConta, dcmPlanoConta);
  Application.CreateForm(TrptVendaPeriodoProduto, rptVendaPeriodoProduto);
  Application.CreateForm(TdcmCompraCentroCusto, dcmCompraCentroCusto);
  Application.CreateForm(TdcmCompraCentroCusto_view, dcmCompraCentroCusto_view);
  Application.CreateForm(TfrmLibFinRec, frmLibFinRec);
  Application.CreateForm(TfrmLibFinRec_Compl, frmLibFinRec_Compl);
  Application.CreateForm(TfrmGrupoPedido, frmGrupoPedido);
  Application.CreateForm(TfrmFiltroFollowUp, frmFiltroFollowUp);
  Application.CreateForm(TdcmAnaliseLucratividade, dcmAnaliseLucratividade);
  Application.CreateForm(TdcmAnaliseLucratividade_view, dcmAnaliseLucratividade_view);
  Application.CreateForm(TdcmCompraRepresPeriodo, dcmCompraRepresPeriodo);
  Application.CreateForm(TdcmCompraRepresPeriodo_view, dcmCompraRepresPeriodo_view);
  Application.CreateForm(TdcmCompraFornecPeriodo, dcmCompraFornecPeriodo);
  Application.CreateForm(TdcmCompraFornecPeriodo_view, dcmCompraFornecPeriodo_view);
  Application.CreateForm(TfrmCarteiraFinancAutorPed, frmCarteiraFinancAutorPed);
  Application.CreateForm(TfrmModImpBOL, frmModImpBOL);
  Application.CreateForm(TfrmTabPreco, frmTabPreco);
  Application.CreateForm(TfrmTransfEstManual, frmTransfEstManual);
  Application.CreateForm(TfrmConsultaChequeEmitido, frmConsultaChequeEmitido);
  Application.CreateForm(TfrmBaixaChequePre, frmBaixaChequePre);
  Application.CreateForm(TfrmContratoFinanc, frmContratoFinanc);
  Application.CreateForm(TfrmParametro, frmParametro);
  Application.CreateForm(TfrmRastreaLoteProduto, frmRastreaLoteProduto);
  Application.CreateForm(TfrmMonitorProducao, frmMonitorProducao);
  Application.CreateForm(TfrmLimiteCredCliente, frmLimiteCredCliente);
  Application.CreateForm(TfrmPdvFormaPagto, frmPdvFormaPagto);
  Application.CreateForm(TfrmPdvCondPagto, frmPdvCondPagto);
  Application.CreateForm(TfrmTitulo, frmTitulo);
  Application.CreateForm(TfrmConvTitSP, frmConvTitSP);
  Application.CreateForm(TfrmDoctoFiscalManual, frmDoctoFiscalManual);
  Application.CreateForm(TfrmRequisicao, frmRequisicao);
  Application.CreateForm(TrptMapaCompra, rptMapaCompra);
  Application.CreateForm(TfrmAutoriza_Requisicao, frmAutoriza_Requisicao);
  Application.CreateForm(TfrmAtendimentoRequisicao, frmAtendimentoRequisicao);
  Application.CreateForm(TfrmAtendRequisicao_Local, frmAtendRequisicao_Local);
  Application.CreateForm(TfrmTipoRequisicao, frmTipoRequisicao);
  Application.CreateForm(TfrmSetor, frmSetor);
  Application.CreateForm(TfrmCaixa_Recebimento, frmCaixa_Recebimento);
  Application.CreateForm(TfrmCaixa_Cheques, frmCaixa_Cheques);
  Application.CreateForm(TfrmCaixa_PagtoCheque, frmCaixa_PagtoCheque);
  Application.CreateForm(TfrmCaixa_ListaParcelas, frmCaixa_ListaParcelas);
  Application.CreateForm(TfrmCaixa_EntradaDin, frmCaixa_EntradaDin);
  Application.CreateForm(TrptBorderoCaixa, rptBorderoCaixa);
  Application.CreateForm(TrptRemessaNum_view, rptRemessaNum_view);
  Application.CreateForm(TfrmCaixa_Fechamento, frmCaixa_Fechamento);
  Application.CreateForm(TfrmRemessa_Numerarios, frmRemessa_Numerarios);
  Application.CreateForm(TrptBorderoCaixa_view, rptBorderoCaixa_view);
  Application.CreateForm(TfrmInventario_Abertura, frmInventario_Abertura);
  Application.CreateForm(TrptExtratoContaBancaria_view, rptExtratoContaBancaria_view);
  Application.CreateForm(TfrmInventario_Fechamento, frmInventario_Fechamento);
  Application.CreateForm(TfrmInventario_Digitacao, frmInventario_Digitacao);
  Application.CreateForm(TrptInventario_Resumo, rptInventario_Resumo);
  Application.CreateForm(TfrmConfirma_Numerarios, frmConfirma_Numerarios);
  Application.CreateForm(TfrmSaldoEstoqueInicial, frmSaldoEstoqueInicial);
  Application.CreateForm(TfrmCaixa_EstornoDia, frmCaixa_EstornoDia);
  Application.CreateForm(TfrmTipoLancto, frmTipoLancto);
  Application.CreateForm(TfrmContaCaixaCofre, frmContaCaixaCofre);
  Application.CreateForm(TfrmConsNecessidadeCompra, frmConsNecessidadeCompra);
  Application.CreateForm(TfrmMapaConcorrencia_Fornecedor, frmMapaConcorrencia_Fornecedor);
  Application.CreateForm(TfrmAprovacaoMapaCompra, frmAprovacaoMapaCompra);
  Application.CreateForm(TfrmGeraPedidoCompraAutom, frmGeraPedidoCompraAutom);
  Application.CreateForm(TrptVendaFatDia_view, rptVendaFatDia_view);
  Application.CreateForm(TfrmMapaCompra, frmMapaCompra);
  Application.CreateForm(TfrmPosTituloAberto, frmPosTituloAberto);
  Application.CreateForm(TrptPosTituloLiquidado, rptPosTituloLiquidado);
  Application.CreateForm(TfrmGerarMapaConcorrencia, frmGerarMapaConcorrencia);
  Application.CreateForm(TfrmPedidoAbertoProduto, frmPedidoAbertoProduto);
  Application.CreateForm(TfrmConsComprasProduto, frmConsComprasProduto);
  Application.CreateForm(TfrmSyncInfra, frmSyncInfra);
  Application.CreateForm(TfrmOperManEstoque, frmOperManEstoque);
  Application.CreateForm(TfrmDevolucVenda, frmDevolucVenda);
  Application.CreateForm(TfrmMonitorOrcamento, frmMonitorOrcamento);
  Application.CreateForm(TfrmMonitorOrcamento_Itens, frmMonitorOrcamento_Itens);
  Application.CreateForm(TfrmMonitorOrcamento_GerarPedido, frmMonitorOrcamento_GerarPedido);
  Application.CreateForm(TfrmConsultaCheque, frmConsultaCheque);
  Application.CreateForm(TfrmCondPagto, frmCondPagto);
  Application.CreateForm(TfrmCaixa_MotivoEstorno, frmCaixa_MotivoEstorno);
  Application.CreateForm(TfrmCaixa_StatusDocto, frmCaixa_StatusDocto);
  Application.CreateForm(TfrmMRP_FornecHomol, frmMRP_FornecHomol);
  Application.CreateForm(TfrmMRP_FormulaProd, frmMRP_FormulaProd);
  Application.CreateForm(TfrmMRP_GeraPedido, frmMRP_GeraPedido);
  Application.CreateForm(TfrmMRP_DetalheEstoqueAtual, frmMRP_DetalheEstoqueAtual);
  Application.CreateForm(TfrmMRP_BalancoFornecedores, frmMRP_BalancoFornecedores);
  Application.CreateForm(TfrmOrdemCompraVarejo, frmOrdemCompraVarejo);
  Application.CreateForm(TrptPedidoDevCompra, rptPedidoDevCompra);
  Application.CreateForm(TfrmMRP_UltimosPedidos, frmMRP_UltimosPedidos);
  Application.CreateForm(TfrmApropAdiantamento, frmApropAdiantamento);
  Application.CreateForm(TfrmApropAdiantamento_dados, frmApropAdiantamento_dados);
  Application.CreateForm(TfrmExploraCaixaCofre, frmExploraCaixaCofre);
  Application.CreateForm(TfrmMovTitulo, frmMovTitulo);
  Application.CreateForm(TfrmFuncoesECF, frmFuncoesECF);
  Application.CreateForm(TfrmAlaProducao, frmAlaProducao);
  Application.CreateForm(TfrmVinculoProdutoAla, frmVinculoProdutoAla);
  Application.CreateForm(TrptOrcamentoEnergy, rptOrcamentoEnergy);
  Application.CreateForm(TfrmItemPedidoCompraAtendido, frmItemPedidoCompraAtendido);
  Application.CreateForm(TfrmItemPedidoAtendido, frmItemPedidoAtendido);
  Application.CreateForm(TfrmPedidoVendaWERP, frmPedidoVendaWERP);
  Application.CreateForm(TfrmAutoriza_devolucao, frmAutoriza_devolucao);
  Application.CreateForm(TrptPedidoPendFat, rptPedidoPendFat);
  Application.CreateForm(TrptPedidoPendFat_view, rptPedidoPendFat_view);
  Application.CreateForm(TfrmRecebimento_Divergencias, frmRecebimento_Divergencias);
  Application.CreateForm(TrptPedidoVenda, rptPedidoVenda);
  Application.CreateForm(TfrmTitPagarAguardDoc, frmTitPagarAguardDoc);
  Application.CreateForm(TfrmPlanoContaNovo, frmPlanoContaNovo);
  Application.CreateForm(TrptRankingFatAnualProdutoWERP, rptRankingFatAnualProdutoWERP);
  Application.CreateForm(TrptPlanilhaCobranca_view, rptPlanilhaCobranca_view);
  Application.CreateForm(TrptDRE, rptDRE);
  Application.CreateForm(TrptDRE_View, rptDRE_View);
  Application.CreateForm(TrptRankingFatMensal, rptRankingFatMensal);
  Application.CreateForm(TrptRankingFatMensal_view, rptRankingFatMensal_view);
  Application.CreateForm(TrptFaturamentoAnual, rptFaturamentoAnual);
  Application.CreateForm(TrptFaturamentoAnual_view, rptFaturamentoAnual_view);
  Application.CreateForm(TrptRankingFatAnual, rptRankingFatAnual);
  Application.CreateForm(TrptRankingFatAnual_view, rptRankingFatAnual_view);
  Application.CreateForm(TfrmPlanilhaCobranca, frmPlanilhaCobranca);
  Application.CreateForm(TfrmPlanilhaCobranca_Ocorrencias, frmPlanilhaCobranca_Ocorrencias);
  Application.CreateForm(TrptRelPedidoVenda_view, rptRelPedidoVenda_view);
  Application.CreateForm(TfrmLibDivRec_PrecoVenda, frmLibDivRec_PrecoVenda);
  Application.CreateForm(TfrmDepartamento_Segmento, frmDepartamento_Segmento);
  Application.CreateForm(TfrmDepartamento_SubCategoria, frmDepartamento_SubCategoria);
  Application.CreateForm(TfrmDepartamento_Marca, frmDepartamento_Marca);
  Application.CreateForm(TfrmPedidoDevCompra, frmPedidoDevCompra);
  Application.CreateForm(TfrmMetaVenda, frmMetaVenda);
  Application.CreateForm(TfrmOrdemCompraVarejo_Grade, frmOrdemCompraVarejo_Grade);
  Application.CreateForm(TfrmOrcamento, frmOrcamento);
  Application.CreateForm(TrptOrcamento, rptOrcamento);
  Application.CreateForm(TfrmConsultaRequisAberta, frmConsultaRequisAberta);
  Application.CreateForm(TfrmConsultaRequisAberta_Itens, frmConsultaRequisAberta_Itens);
  Application.CreateForm(TrptRecebimento_view, rptRecebimento_view);
  Application.CreateForm(TfrmTransfEst_Recebimento, frmTransfEst_Recebimento);
  Application.CreateForm(TfrmTransfEst_Recebimento_Itens, frmTransfEst_Recebimento_Itens);
  Application.CreateForm(TfrmTransfEst_Recebimento_Processo, frmTransfEst_Recebimento_Processo);
  Application.CreateForm(TfrmAnaliseQualidade, frmAnaliseQualidade);
  Application.CreateForm(TfrmAnaliseQualidade_Itens, frmAnaliseQualidade_Itens);
  Application.CreateForm(TfrmLoja, frmLoja);
  Application.CreateForm(TfrmCaixa_Dinheiro, frmCaixa_Dinheiro);
  Application.CreateForm(TfrmProdutoMRP, frmProdutoMRP);
  Application.CreateForm(TfrmProdutoPosicaoEstoque, frmProdutoPosicaoEstoque);
  Application.CreateForm(TfrmProdutoFornecedor, frmProdutoFornecedor);
  Application.CreateForm(TCarnet_Rensz_Form, Carnet_Rensz_Form);
  Application.CreateForm(TfrmConciliaBancoManual, frmConciliaBancoManual);
  Application.CreateForm(TfrmConciliaBancoManual_IncluiLancto, frmConciliaBancoManual_IncluiLancto);
  Application.CreateForm(TrptExtratoContaBancaria, rptExtratoContaBancaria);
  Application.CreateForm(TrptInventario_view, rptInventario_view);
  Application.CreateForm(TfrmCargaInicial_ChequeTerceiro, frmCargaInicial_ChequeTerceiro);
  Application.CreateForm(TfrmInventarioLoja_Abertura, frmInventarioLoja_Abertura);
  Application.CreateForm(TfrmImpBoletoNF, frmImpBoletoNF);
  Application.CreateForm(TfrmGrupoInventario, frmGrupoInventario);
  Application.CreateForm(TfrmLocalizacaoProduto, frmLocalizacaoProduto);
  Application.CreateForm(TfrmLocalizacaoProduto_Vinculo, frmLocalizacaoProduto_Vinculo);
  Application.CreateForm(TfrmReimpressaoCarnet, frmReimpressaoCarnet);
  Application.CreateForm(TfrmConsultaPedComAberTerceiro, frmConsultaPedComAberTerceiro);
  Application.CreateForm(TfrmConsultaPedComAberTerceiro_Itens, frmConsultaPedComAberTerceiro_Itens);
  Application.CreateForm(TfrmQueryExecute, frmQueryExecute);
  Application.CreateForm(TfrmTipoAlerta, frmTipoAlerta);
  Application.CreateForm(TfrmTipoAlerta, frmTipoAlerta);
  Application.CreateForm(TfrmAlertas, frmAlertas);
  Application.CreateForm(TfrmWait, frmWait);
  Application.CreateForm(TfrmPDVItemGrade, frmPDVItemGrade);
  Application.CreateForm(TfrmProduto_Duplicar, frmProduto_Duplicar);
  Application.CreateForm(TfrmPedidoComercial_ProgEntrega, frmPedidoComercial_ProgEntrega);
  Application.CreateForm(TrptRelPedidoVenda, rptRelPedidoVenda);
  Application.CreateForm(TrptPreVendaPendente_view, rptPreVendaPendente_view);
  Application.CreateForm(TrptPedidoCompra, rptPedidoCompra);
  Application.CreateForm(TrptPedidoCompra_view, rptPedidoCompra_view);
  Application.CreateForm(TrptListaProdutoSimples, rptListaProdutoSimples);
  Application.CreateForm(TrptFichaKardex_view, rptFichaKardex_view);
  Application.CreateForm(TfrmOperadoraCartao, frmOperadoraCartao);
  Application.CreateForm(TfrmProdutoGradeSimples, frmProdutoGradeSimples);
  Application.CreateForm(TfrmGerarGradeSimples, frmGerarGradeSimples);
  Application.CreateForm(TfrmRecebimento_CadProduto, frmRecebimento_CadProduto);
  Application.CreateForm(TrptPosicaoVale, rptPosicaoVale);
  Application.CreateForm(TrptOperacaoCartao_view, rptOperacaoCartao_view);
  Application.CreateForm(TrptOperacaoCartao, rptOperacaoCartao);
  Application.CreateForm(TrptComissaoRepres_view, rptComissaoRepres_view);
  Application.CreateForm(TfrmMonitorMapaCompra, frmMonitorMapaCompra);
  Application.CreateForm(TfrmFornecedor, frmFornecedor);
  Application.CreateForm(TrptListaProdutoSimples_view, rptListaProdutoSimples_view);
  Application.CreateForm(TfrmRamoAtividade, frmRamoAtividade);
  Application.CreateForm(TdcmVendaProdutoClienteWERP, dcmVendaProdutoClienteWERP);
  Application.CreateForm(TdcmAnaliseGiroProduto_view, dcmAnaliseGiroProduto_view);
  Application.CreateForm(TrptComissaoRepres, rptComissaoRepres);
  Application.CreateForm(TrptDepositoBancario_view, rptDepositoBancario_view);
  Application.CreateForm(TfrmGrupoComissao, frmGrupoComissao);
  Application.CreateForm(TfrmProvisaoPagto, frmProvisaoPagto);
  Application.CreateForm(TfrmProvisaoPagto_Titulos, frmProvisaoPagto_Titulos);
  Application.CreateForm(TfrmProvisaoPagto_ContaBancaria, frmProvisaoPagto_ContaBancaria);
  Application.CreateForm(TfrmCor, frmCor);
  Application.CreateForm(TfrmMontaGrade, frmMontaGrade);
  Application.CreateForm(TfrmLibDivRec_Parcelas, frmLibDivRec_Parcelas);
  Application.CreateForm(TfrmConsultaItemPedidoAberto_SomenteItens, frmConsultaItemPedidoAberto_SomenteItens);
  Application.CreateForm(TfrmRecebimento_Etiqueta, frmRecebimento_Etiqueta);
  Application.CreateForm(TrptRecebimento_Etiqueta_view, rptRecebimento_Etiqueta_view);
  Application.CreateForm(TfrmProvisaoPagto_DetalheProvisao, frmProvisaoPagto_DetalheProvisao);
  Application.CreateForm(TfrmBaixaProvisao, frmBaixaProvisao);
  Application.CreateForm(TfrmBaixaProvisao_FormaPagto, frmBaixaProvisao_FormaPagto);
  Application.CreateForm(TfrmBaixaProvisao_Provisoes, frmBaixaProvisao_Provisoes);
  Application.CreateForm(TfrmBaixaProvisao_BaixaTitulos, frmBaixaProvisao_BaixaTitulos);
  Application.CreateForm(TfrmEstornoBaixaProvisao, frmEstornoBaixaProvisao);
  Application.CreateForm(TfrmVerbaPublicidade, frmVerbaPublicidade);
  Application.CreateForm(TfrmMovTitulo_Abatimento, frmMovTitulo_Abatimento);
  Application.CreateForm(TfrmTransfEst_Itens, frmTransfEst_Itens);
  Application.CreateForm(TfrmConfigCustoCompra, frmConfigCustoCompra);
  Application.CreateForm(TfrmPedidoComercial, frmPedidoComercial);
  Application.CreateForm(TrptPedidoCom_Simples, rptPedidoCom_Simples);
  Application.CreateForm(TfrmTituloProv_Duplicar, frmTituloProv_Duplicar);
  Application.CreateForm(TfrmCheque_TrocarProtador, frmCheque_TrocarProtador);
  Application.CreateForm(TfrmDepositoBancario, frmDepositoBancario);
  Application.CreateForm(TfrmDepositoBancario_SelecaoCheque, frmDepositoBancario_SelecaoCheque);
  Application.CreateForm(TfrmConciliaBancoManual__ConsultaCheques, frmConciliaBancoManual__ConsultaCheques);
  Application.CreateForm(TfrmDepositoBancario_ConsultaCheques, frmDepositoBancario_ConsultaCheques);
  Application.CreateForm(TfrmTabelaJuros, frmTabelaJuros);
  Application.CreateForm(TfrmPosicaoCarteiraCheque, frmPosicaoCarteiraCheque);
  Application.CreateForm(TfrmPosicaoCarteiraCheque_Cheques, frmPosicaoCarteiraCheque_Cheques);
  Application.CreateForm(TfrmCadastroChequeTerceiro, frmCadastroChequeTerceiro);
  Application.CreateForm(TfrmConsultaLanctoCofre, frmConsultaLanctoCofre);
  Application.CreateForm(TfrmTransfConta, frmTransfConta);
  Application.CreateForm(TfrmSenhaAcesso, frmSenhaAcesso);
  Application.CreateForm(TfrmAlteraVenctoCarnet, frmAlteraVenctoCarnet);
  Application.CreateForm(TfrmArvorePlanoConta, frmArvorePlanoConta);
  Application.CreateForm(TfrmNotaFiscalRecusada_Digitacao, frmNotaFiscalRecusada_Digitacao);
  Application.CreateForm(TfrmContaBancaria, frmContaBancaria);
  Application.CreateForm(TrptFluxoCaixaRealizado, rptFluxoCaixaRealizado);
  Application.CreateForm(TfrmFluxoCaixaRealizado_View, frmFluxoCaixaRealizado_View);
  Application.CreateForm(TfrmFluxoCaixaRealizado_Lanctos, frmFluxoCaixaRealizado_Lanctos);
  Application.CreateForm(TrptFluxoCaixa_View, rptFluxoCaixa_View);
  Application.CreateForm(TfrmEncerrarMovtoPOS, frmEncerrarMovtoPOS);
  Application.CreateForm(TfrmEncerrarMovtoPOS_GeraLote, frmEncerrarMovtoPOS_GeraLote);
  Application.CreateForm(TfrmConciliaCartao, frmConciliaCartao);
  Application.CreateForm(TfrmConciliacaoBancariaManual_Ultimas, frmConciliacaoBancariaManual_Ultimas);
  Application.CreateForm(TfrmConciliacaoBancariaManual, frmConciliacaoBancariaManual);
  Application.CreateForm(TfrmEstornoConciliacaoCartao, frmEstornoConciliacaoCartao);
  Application.CreateForm(TfrmConsultaVendaProduto, frmConsultaVendaProduto);
  Application.CreateForm(TfrmConsultaVendaProduto_Dados, frmConsultaVendaProduto_Dados);
  Application.CreateForm(TfrmProdutoERPInsumos, frmProdutoERPInsumos);
  Application.CreateForm(TfrmGerenciaPreco, frmGerenciaPreco);
  Application.CreateForm(TrptAvaliacaoCQM_View, rptAvaliacaoCQM_View);
  Application.CreateForm(TfrmCargaInicial_TitulosPagar, frmCargaInicial_TitulosPagar);
  Application.CreateForm(TrptLoteLiqCobradora_view, rptLoteLiqCobradora_view);
  Application.CreateForm(TfrmLancManConta, frmLancManConta);
  Application.CreateForm(TfrmConsultaSaldoConta, frmConsultaSaldoConta);
  Application.CreateForm(TfrmCaixa_Pendencias, frmCaixa_Pendencias);
  Application.CreateForm(TrptPicking_Retrato, rptPicking_Retrato);
  Application.CreateForm(TfrmMRP, frmMRP);
  Application.CreateForm(TfrmMRP_Sugestao, frmMRP_Sugestao);
  Application.CreateForm(TfrmCaixa_ResgateCheque, frmCaixa_ResgateCheque);
  Application.CreateForm(TfrmPedidoDevCompra_ConsultaItens, frmPedidoDevCompra_ConsultaItens);
  Application.CreateForm(TfrmIdentificaCredito, frmIdentificaCredito);
  Application.CreateForm(TfrmBaixaTituloDeposito, frmBaixaTituloDeposito);
  Application.CreateForm(TfrmBaixaTituloDeposito_Movtos, frmBaixaTituloDeposito_Movtos);
  Application.CreateForm(TfrmTrilhaClienteVarejo, frmTrilhaClienteVarejo);
  Application.CreateForm(TfrmBaixaTituloDeposito_Titulos, frmBaixaTituloDeposito_Titulos);
  Application.CreateForm(TfrmClienteVarejoPessoaFisica_HistAltEnd, frmClienteVarejoPessoaFisica_HistAltEnd);
  Application.CreateForm(TfrmImagemDigital, frmImagemDigital);
  Application.CreateForm(TfrmImagemDigital_Visualizar, frmImagemDigital_Visualizar);
  Application.CreateForm(TfrmCaixa_Justificativa, frmCaixa_Justificativa);
  Application.CreateForm(TfrmCaixa_ParcelaAberto, frmCaixa_ParcelaAberto);
  Application.CreateForm(TfrmCaixa_LiberaRestricao, frmCaixa_LiberaRestricao);
  Application.CreateForm(TfrmPdvDesconto, frmPdvDesconto);
  Application.CreateForm(TfrmPdvConsProduto, frmPdvConsProduto);
  Application.CreateForm(TfrmCentralArquivos, frmCentralArquivos);
  Application.CreateForm(TfrmPdvReabrePedido, frmPdvReabrePedido);
  Application.CreateForm(TfrmPdvSupervisor, frmPdvSupervisor);
  Application.CreateForm(TfrmPdvValePresente, frmPdvValePresente);
  Application.CreateForm(TfrmPDVValeMerc, frmPDVValeMerc);
  Application.CreateForm(TfrmCaixa_SelCliente, frmCaixa_SelCliente);
  Application.CreateForm(TfrmManutPedido_BaixaManual, frmManutPedido_BaixaManual);
  Application.CreateForm(TfrmQuestionarioCQM, frmQuestionarioCQM);
  Application.CreateForm(TfrmLanctoPlanoConta, frmLanctoPlanoConta);
  Application.CreateForm(TfrmAnaliseLanctoCtbConta, frmAnaliseLanctoCtbConta);
  Application.CreateForm(TfrmAnaliseLanctoCtbConta_Detalhe, frmAnaliseLanctoCtbConta_Detalhe);
  Application.CreateForm(TfrmAvaliacaoCQM, frmAvaliacaoCQM);
  Application.CreateForm(TfrmCaixa_Sangria, frmCaixa_Sangria);
  Application.CreateForm(TfrmCaixa_Suprimento, frmCaixa_Suprimento);
  Application.CreateForm(TrptFichaPosicaoContratoImobManual, rptFichaPosicaoContratoImobManual);
  Application.CreateForm(TfrmCaixa_ParcelaCred, frmCaixa_ParcelaCred);
  Application.CreateForm(TfrmCaixa_DadosCheque, frmCaixa_DadosCheque);
  Application.CreateForm(TfrmEmpImobiliario, frmEmpImobiliario);
  Application.CreateForm(TfrmContratoImobiliario, frmContratoImobiliario);
  Application.CreateForm(TfrmCaixa_DadosCartao, frmCaixa_DadosCartao);
  Application.CreateForm(TfrmTransfEst, frmTransfEst);
  Application.CreateForm(TfrmCaixa_Funcoes, frmCaixa_Funcoes);
  Application.CreateForm(TfrmCaixa_ReembolsoVale, frmCaixa_ReembolsoVale);
  Application.CreateForm(TfrmCaixa_LanctoMan, frmCaixa_LanctoMan);
  Application.CreateForm(TrptTituloReabilitado_view, rptTituloReabilitado_view);
  Application.CreateForm(TfrmCaixa_NumValePresente, frmCaixa_NumValePresente);
  Application.CreateForm(TrptFichaPosicaoContratoImob, rptFichaPosicaoContratoImob);
  Application.CreateForm(TfrmCaixa_ConsOperadora, frmCaixa_ConsOperadora);
  Application.CreateForm(TfrmPDVConsVended, frmPDVConsVended);
  Application.CreateForm(TfrmCaixa_ConfUsuarioCaixa, frmCaixa_ConfUsuarioCaixa);
  Application.CreateForm(TfrmCaixa_AlteraCartao, frmCaixa_AlteraCartao);
  Application.CreateForm(TfrmCaixa_Reimpressao, frmCaixa_Reimpressao);
  Application.CreateForm(TfrmAprovacaoClienteVarejo, frmAprovacaoClienteVarejo);
  Application.CreateForm(TfrmAprovacaoClienteVarejo_LimiteInicial, frmAprovacaoClienteVarejo_LimiteInicial);
  Application.CreateForm(TfrmManutLimiteClienteManual, frmManutLimiteClienteManual);
  Application.CreateForm(TfrmHistoricoLimiteCredito, frmHistoricoLimiteCredito);
  Application.CreateForm(TfrmManutBloqueioCliente, frmManutBloqueioCliente);
  Application.CreateForm(TfrmContratoImobiliario_Parcelas, frmContratoImobiliario_Parcelas);
  Application.CreateForm(TfrmManutLimiteCliente, frmManutLimiteCliente);
  Application.CreateForm(TfrmManutBloqueioCliente_Inserir, frmManutBloqueioCliente_Inserir);
  Application.CreateForm(TfrmTipoBloqueioCliente, frmTipoBloqueioCliente);
  Application.CreateForm(TrptBorderoCaixaNovo, rptBorderoCaixaNovo);
  Application.CreateForm(TfrmPedidoVendaSimples, frmPedidoVendaSimples);
  Application.CreateForm(TfrmFollowUP_Pedido, frmFollowUP_Pedido);
  Application.CreateForm(TfrmConsTituloAberto, frmConsTituloAberto);
  Application.CreateForm(TrptRankingFatAnualProduto, rptRankingFatAnualProduto);
  Application.CreateForm(TdcmVendaProdutoCliente, dcmVendaProdutoCliente);
  Application.CreateForm(TfrmConsultaPagamentoWERP, frmConsultaPagamentoWERP);
  Application.CreateForm(TfrmConsultaNotaFiscalWERP, frmConsultaNotaFiscalWERP);
  Application.CreateForm(TfrmTipoTabelaPreco, frmTipoTabelaPreco);
  Application.CreateForm(TfrmCampanhaPromoc, frmCampanhaPromoc);
  Application.CreateForm(TfrmInventarioLoja_IncluiProduto, frmInventarioLoja_IncluiProduto);
  Application.CreateForm(TfrmCampanhaPromoc_ExibeProduto, frmCampanhaPromoc_ExibeProduto);
  Application.CreateForm(TfrmAtivaCampanhaPromoc, frmAtivaCampanhaPromoc);
  Application.CreateForm(TfrmAcompanhaCampanhaPromoc, frmAcompanhaCampanhaPromoc);
  Application.CreateForm(TfrmCampanhaPromoc_AnaliseVendasERP, frmCampanhaPromoc_AnaliseVendasERP);
  Application.CreateForm(TfrmCampanhaPromoc_AnaliseVendas, frmCampanhaPromoc_AnaliseVendas);
  Application.CreateForm(TfrmCaixa_ListaPedidos, frmCaixa_ListaPedidos);
  Application.CreateForm(TfrmEtapaCobranca, frmEtapaCobranca);
  Application.CreateForm(TfrmCobranca_VisaoContato, frmCobranca_VisaoContato);
  Application.CreateForm(TfrmListaCobranca, frmListaCobranca);
  Application.CreateForm(TfrmCobranca_VisaoContato_MovTit, frmCobranca_VisaoContato_MovTit);
  Application.CreateForm(TfrmListaCobranca_Itens, frmListaCobranca_Itens);
  Application.CreateForm(TfrmTipoResultadoContato, frmTipoResultadoContato);
  Application.CreateForm(TfrmCobranca_VisaoContato_Contato, frmCobranca_VisaoContato_Contato);
  Application.CreateForm(TfrmCobranca_VisaoContato_Contato_SelContato, frmCobranca_VisaoContato_Contato_SelContato);
  Application.CreateForm(TfrmAdmListaCobranca, frmAdmListaCobranca);
  Application.CreateForm(TfrmAdmListaCobranca_Listas, frmAdmListaCobranca_Listas);
  Application.CreateForm(TfrmAdmListaCobranca_Listas_SelLista, frmAdmListaCobranca_Listas_SelLista);
  Application.CreateForm(TfrmListaCobranca_Itens_NovaLista, frmListaCobranca_Itens_NovaLista);
  Application.CreateForm(TfrmModeloCartaCobranca, frmModeloCartaCobranca);
  Application.CreateForm(TfrmListaCobranca_Itens_GerarCarta, frmListaCobranca_Itens_GerarCarta);
  Application.CreateForm(TfrmCobranca_VisaoContato_Titulos, frmCobranca_VisaoContato_Titulos);
  Application.CreateForm(TfrmListaCobranca_Itens_SelCliCobradora, frmListaCobranca_Itens_SelCliCobradora);
  Application.CreateForm(TrptFichaCobradora_view, rptFichaCobradora_view);
  Application.CreateForm(TfrmClienteVarejoPessoaFisica_TitulosNeg, frmClienteVarejoPessoaFisica_TitulosNeg);
  Application.CreateForm(TfrmReabilitacaoCliente, frmReabilitacaoCliente);
  Application.CreateForm(TrptTituloNegativado_view, rptTituloNegativado_view);
  Application.CreateForm(TfrmTitulo_DetalheJuros, frmTitulo_DetalheJuros);
  Application.CreateForm(TfrmCobradora, frmCobradora);
  Application.CreateForm(TfrmListaCobranca_Itens_EnvioCobradora, frmListaCobranca_Itens_EnvioCobradora);
  Application.CreateForm(TrptAlteracaoPrecoProduto_view, rptAlteracaoPrecoProduto_view);
  Application.CreateForm(TfrmGeraListaCobranca, frmGeraListaCobranca);
  Application.CreateForm(TfrmCalendarioCobrancaDiario, frmCalendarioCobrancaDiario);
  Application.CreateForm(TfrmGeraPropostaRenegociacao, frmGeraPropostaRenegociacao);
  Application.CreateForm(TfrmGeraPropostaRenegociacao_Simulacao, frmGeraPropostaRenegociacao_Simulacao);
  Application.CreateForm(TrptRecebimentoCompras, rptRecebimentoCompras);
  Application.CreateForm(TrptRecebimentoCompras_View, rptRecebimentoCompras_View);
  Application.CreateForm(TfrmAutorizaPropostaReneg, frmAutorizaPropostaReneg);
  Application.CreateForm(TfrmAutorizaPropostaReneg_Titulos, frmAutorizaPropostaReneg_Titulos);
  Application.CreateForm(TfrmCaixa_DescontoJuros, frmCaixa_DescontoJuros);
  Application.CreateForm(TfrmCaixa_ConsPropReneg, frmCaixa_ConsPropReneg);
  Application.CreateForm(TfrmCaixa_EfetivaPropReneg, frmCaixa_EfetivaPropReneg);
  Application.CreateForm(TrptPosTituloAbertoFormaPagto, rptPosTituloAbertoFormaPagto);
  Application.CreateForm(TfrmPosTituloAbertoFormaPagto, frmPosTituloAbertoFormaPagto);
  Application.CreateForm(TfrmListaCobranca_Itens_Negativacao, frmListaCobranca_Itens_Negativacao);
  Application.CreateForm(TfrmMTitulo_Movimenta, frmMTitulo_Movimenta);
  Application.CreateForm(TfrmBaixaColetiva_ConsTitulo, frmBaixaColetiva_ConsTitulo);
  Application.CreateForm(TfrmLoteLiqCobradora, frmLoteLiqCobradora);
  Application.CreateForm(TfrmLoteLiqCobradora_Cliente, frmLoteLiqCobradora_Cliente);
  Application.CreateForm(TfrmLoteLiqCobradora_Titulos, frmLoteLiqCobradora_Titulos);
  Application.CreateForm(TrptBaixaColetiva_view, rptBaixaColetiva_view);
  Application.CreateForm(TfrmRetiraClienteCobradora, frmRetiraClienteCobradora);
  Application.CreateForm(TrptPosTituloAberto, rptPosTituloAberto);
  Application.CreateForm(TrptProdutoMenosVendido, rptProdutoMenosVendido);
  Application.CreateForm(TdcmVendaProdutoCliente_view, dcmVendaProdutoCliente_view);
  Application.CreateForm(TdcmAnaliseGiroProduto, dcmAnaliseGiroProduto);
  Application.CreateForm(TrptProdutoMenosVendido_view, rptProdutoMenosVendido_view);
  Application.CreateForm(TrptPreVendaPendente, rptPreVendaPendente);
  Application.CreateForm(TrptVendaEstruturaMarcaLoja_view, rptVendaEstruturaMarcaLoja_view);
  Application.CreateForm(TrptPerformanceProduto, rptPerformanceProduto);
  Application.CreateForm(TrptPosicaoEstoqueSimples_view, rptPosicaoEstoqueSimples_view);
  Application.CreateForm(TfrmMonitorTransferencia, frmMonitorTransferencia);
  Application.CreateForm(TfrmProgAltPreco, frmProgAltPreco);
  Application.CreateForm(TfrmProgaltPreco_Incluir, frmProgaltPreco_Incluir);
  Application.CreateForm(TrptAlteracaoPrecoProduto, rptAlteracaoPrecoProduto);
  Application.CreateForm(TrptCampanhaPromoc_view, rptCampanhaPromoc_view);
  Application.CreateForm(TrptAcompanhaGiroProduto_view, rptAcompanhaGiroProduto_view);
  Application.CreateForm(TfrmProduto_DetalheGrade, frmProduto_DetalheGrade);
  Application.CreateForm(TfrmProduto_HistPrecoVenda, frmProduto_HistPrecoVenda);
  Application.CreateForm(TrptAcompanhaGiroProduto, rptAcompanhaGiroProduto);
  Application.CreateForm(TrptPosicaoEstoqueGrade_view, rptPosicaoEstoqueGrade_view);
  Application.CreateForm(TrptPosicaoEstoqueEstruturaMarca, rptPosicaoEstoqueEstruturaMarca);
  Application.CreateForm(TrptPosicaoEstoqueEstruturaMarca_view, rptPosicaoEstoqueEstruturaMarca_view);
  Application.CreateForm(TfrmServidorReplicacao, frmServidorReplicacao);
  Application.CreateForm(TfrmServidorReplicacao_FilaTransmissao, frmServidorReplicacao_FilaTransmissao);
  Application.CreateForm(TfrmRegistrosRecebidos, frmRegistrosRecebidos);
  Application.CreateForm(TdcmVendaLoja, dcmVendaLoja);
  Application.CreateForm(TdcmVendaLoja_view, dcmVendaLoja_view);
  Application.CreateForm(TrptPosicaoEstoquePlanilha, rptPosicaoEstoquePlanilha);
  Application.CreateForm(TrptComportamentoCrediario_view, rptComportamentoCrediario_view);
  Application.CreateForm(TfrmAcompanhaGiroProduto_view, frmAcompanhaGiroProduto_view);
  Application.CreateForm(TrptPosicaoEstoqueSimples, rptPosicaoEstoqueSimples);
  Application.CreateForm(TrptPerformanceProduto_view, rptPerformanceProduto_view);
  Application.CreateForm(TrptVendaEstruturaMarcaLoja, rptVendaEstruturaMarcaLoja);
  Application.CreateForm(TrptComportamentoCrediario, rptComportamentoCrediario);
  Application.CreateForm(TrptVendaCrediario_view, rptVendaCrediario_view);
  Application.CreateForm(TrptVendaCrediario, rptVendaCrediario);
  Application.CreateForm(TrptPosicaoVale_view, rptPosicaoVale_view);
  Application.CreateForm(TrptPosicaoInadimplencia, rptPosicaoInadimplencia);
  Application.CreateForm(TrptPosicaoInadimplencia_view, rptPosicaoInadimplencia_view);
  Application.CreateForm(TfrmModImpETP, frmModImpETP);
  Application.CreateForm(TfrmSelPortaEtiqueta, frmSelPortaEtiqueta);
  Application.CreateForm(TFrmAtualizacaoMonetaria, FrmAtualizacaoMonetaria);
  Application.CreateForm(TfrmBaixaParcela, frmBaixaParcela);
  Application.CreateForm(TFrmIndice, FrmIndice);
  Application.CreateForm(TfrmLiquidaParcela, frmLiquidaParcela);
  Application.CreateForm(TrptBlocoUnidades, rptBlocoUnidades);
  Application.CreateForm(TrptBlocoUnidadesView, rptBlocoUnidadesView);
  Application.CreateForm(TrptFichaContratoImobiliario, rptFichaContratoImobiliario);
  Application.CreateForm(TrptRecibo, rptRecibo);
  Application.CreateForm(TrptReciboView, rptReciboView);
  Application.CreateForm(TfrmGeraSPComissaoRepres, frmGeraSPComissaoRepres);
  Application.CreateForm(TfrmGeraSPComissaoRepres_Resumo, frmGeraSPComissaoRepres_Resumo);
  Application.CreateForm(TfrmImportaMunicipioIBGE, frmImportaMunicipioIBGE);
  Application.CreateForm(TrptPicking, rptPicking);
  Application.CreateForm(TfrmClienteVarejoPessoaFisica_HistMov, frmClienteVarejoPessoaFisica_HistMov);
  Application.CreateForm(TfrmProduto, frmProduto);
  Application.CreateForm(TfrmTabRenegociacao, frmTabRenegociacao);
  Application.CreateForm(TfrmEmiteEtiqProdAvulsa, frmEmiteEtiqProdAvulsa);
  Application.CreateForm(TfrmGrupoOperadoraCartao, frmGrupoOperadoraCartao);
  Application.CreateForm(TfrmInventario_AberturaMod2, frmInventario_AberturaMod2);
  Application.CreateForm(TfrmCampanhaPromoc_IncluiProduto, frmCampanhaPromoc_IncluiProduto);
  Application.CreateForm(TfrmGrupoPlanoConta, frmGrupoPlanoConta);
  Application.CreateForm(TfrmReabilitacaoClienteManual, frmReabilitacaoClienteManual);
  Application.CreateForm(TfrmInventarioLoja_Encerramento, frmInventarioLoja_Encerramento);
  Application.CreateForm(TfrmInventarioLoja_Contagem_ExibeLotes, frmInventarioLoja_Contagem_ExibeLotes);
  Application.CreateForm(TfrmInventarioLoja_Contagem_ContagemItemLote, frmInventarioLoja_Contagem_ContagemItemLote);
  Application.CreateForm(TfrmInventarioLoja_Contagem, frmInventarioLoja_Contagem);
  Application.CreateForm(TfrmInventarioLoja_Apuracao_ExibeDiverg, frmInventarioLoja_Apuracao_ExibeDiverg);
  Application.CreateForm(TfrmInventarioLoja_Apuracao, frmInventarioLoja_Apuracao);
  Application.CreateForm(TfrmProvisaoPagtoSaldoProjetado, frmProvisaoPagtoSaldoProjetado);
  Application.CreateForm(TrptResumoMovimentoLoja, rptResumoMovimentoLoja);
  Application.CreateForm(TrptComissaoRepresSintetico_View, rptComissaoRepresSintetico_View);
  Application.CreateForm(TrptItensPorVenda_view, rptItensPorVenda_view);
  Application.CreateForm(TrptItensPorVenda, rptItensPorVenda);
  Application.CreateForm(TfrmCancelaPropostaReneg, frmCancelaPropostaReneg);
  Application.CreateForm(TrptAniversariantesMes, rptAniversariantesMes);
  Application.CreateForm(TrptClientesComprasPeriodo, rptClientesComprasPeriodo);
  Application.CreateForm(TrptClientesComprasPeriodo_view, rptClientesComprasPeriodo_view);
  Application.CreateForm(TrptAniversariantesMes_view, rptAniversariantesMes_view);
  Application.CreateForm(TrptRelMovimentoCaixaAnalitico, rptRelMovimentoCaixaAnalitico);
  Application.CreateForm(TfrmLojaOperadoraCartaoTaxaJuros, frmLojaOperadoraCartaoTaxaJuros);
  Application.CreateForm(TrptRelPagtoCentroCusto, rptRelPagtoCentroCusto);
  Application.CreateForm(TrptPagamentoCentroCusto_View, rptPagamentoCentroCusto_View);
  Application.CreateForm(TfrmPDVConsProdutoDetalhe, frmPDVConsProdutoDetalhe);
  Application.CreateForm(TfrmClienteVarejoPessoaFisica_HistContato, frmClienteVarejoPessoaFisica_HistContato);
  Application.CreateForm(TfrmClienteVarejoPessoaFisica_HistoricoNeg, frmClienteVarejoPessoaFisica_HistoricoNeg);
  Application.CreateForm(TfrmEnvioClienteCobradora, frmEnvioClienteCobradora);
  Application.CreateForm(TfrmEnvioClienteCobradora_Titulos, frmEnvioClienteCobradora_Titulos);
  Application.CreateForm(TfrmSitSincServidor, frmSitSincServidor);
  Application.CreateForm(TrptVendaDiariaXFormaPagto, rptVendaDiariaXFormaPagto);
  Application.CreateForm(TrptVendaDiariaXFormaPagto_view, rptVendaDiariaXFormaPagto_view);
  Application.CreateForm(TrptChequeDevolv, rptChequeDevolv);
  Application.CreateForm(TrptChequeRecebido_view, rptChequeRecebido_view);
  Application.CreateForm(TrptChequeDevolv_view, rptChequeDevolv_view);
  Application.CreateForm(TrptChequeEmitido, rptChequeEmitido);
  Application.CreateForm(TrptChequeEmitido_view, rptChequeEmitido_view);
  Application.CreateForm(TfrmConsultaMovtoCaixa, frmConsultaMovtoCaixa);
  Application.CreateForm(TfrmConsultaMovtoCaixa_Parcelas, frmConsultaMovtoCaixa_Parcelas);
  Application.CreateForm(TrptParcelaCrediarioAberta, rptParcelaCrediarioAberta);
  Application.CreateForm(TrptParcelaCrediarioAberta_view, rptParcelaCrediarioAberta_view);
  Application.CreateForm(TfrmConciliaLoteLiqCobradora, frmConciliaLoteLiqCobradora);
  Application.CreateForm(TfrmConciliaLoteLiqCobradora_Efetiva, frmConciliaLoteLiqCobradora_Efetiva);
  Application.CreateForm(TrptParcelaCrediarioRecebida, rptParcelaCrediarioRecebida);
  Application.CreateForm(TrptParcelaCrediarioRecebida_view, rptParcelaCrediarioRecebida_view);
  Application.CreateForm(TrptPagamentoCentroCustoSint_View, rptPagamentoCentroCustoSint_View);
  Application.CreateForm(TfrmReabrePedido, frmReabrePedido);
  Application.CreateForm(TfrmGrupoMRP, frmGrupoMRP);
  Application.CreateForm(TfrmPDVValePresente, frmPDVValePresente);
  Application.CreateForm(TfrmMRP_ListaPedidoDia, frmMRP_ListaPedidoDia);
  Application.CreateForm(TfrmMapaCompra_Itens, frmMapaCompra_Itens);
  Application.CreateForm(TfrmClienteVarejoPessoaFisica, frmClienteVarejoPessoaFisica);
  Application.CreateForm(TfrmConsultaEndereco, frmConsultaEndereco);
  Application.CreateForm(TfrmClienteVarejo, frmClienteVarejo);
  Application.CreateForm(TfrmConsultaLanctoConta, frmConsultaLanctoConta);
  Application.CreateForm(TrptLanctoCaixa, rptLanctoCaixa);
  Application.CreateForm(TrptLanctoCaixa_view, rptLanctoCaixa_view);
  Application.CreateForm(TrptClientesCadastroPeriodo, rptClientesCadastroPeriodo);
  Application.CreateForm(TrClientesCadastroPeriodoView, rClientesCadastroPeriodoView);
  Application.CreateForm(TfrmConsultaPagamentoCentroCusto, frmConsultaPagamentoCentroCusto);
  Application.CreateForm(TfrmConsultaPagamentoCentroCustoAnalit, frmConsultaPagamentoCentroCustoAnalit);
  Application.CreateForm(TfrmConsultaPagamentoCentroCustoSint, frmConsultaPagamentoCentroCustoSint);
  Application.CreateForm(TrptTituloNegativado, rptTituloNegativado);
  Application.CreateForm(TfrmManutencaoVales, frmManutencaoVales);
  Application.CreateForm(TfrmManutencaoCrediario, frmManutencaoCrediario);
  Application.CreateForm(TfrmAlteraContaBancariaDeposito, frmAlteraContaBancariaDeposito);
  Application.CreateForm(TfrmCancelaDepositoBancario, frmCancelaDepositoBancario);
  Application.CreateForm(TfrmConsultaVendaDesconto, frmConsultaVendaDesconto);
  Application.CreateForm(TrptProvisaoPagamentoBaixada, rptProvisaoPagamentoBaixada);
  Application.CreateForm(TrptProvisaoPagamentoBaixada_View, rptProvisaoPagamentoBaixada_View);
  Application.CreateForm(TfrmManutencaoPedidoVenda, frmManutencaoPedidoVenda);
  Application.CreateForm(TfrmLanctoCaixaEstornado, frmLanctoCaixaEstornado);
  Application.CreateForm(TfrmCadTransacaoCartao, frmCadTransacaoCartao);
  Application.CreateForm(TrptPosicaoEstoqueGrade, rptPosicaoEstoqueGrade);
  Application.CreateForm(TrptVendaXVendedorAnalitico, rptVendaXVendedorAnalitico);
  Application.CreateForm(TrptVendaXVendedorAnalitico_view, rptVendaXVendedorAnalitico_view);
  Application.CreateForm(TrptDoctoFiscalEmitido, rptDoctoFiscalEmitido);
  Application.CreateForm(TrptDoctoFiscalEmitido_view, rptDoctoFiscalEmitido_view);
  Application.CreateForm(TfrmSituacaoEstoqueProduto, frmSituacaoEstoqueProduto);
  Application.CreateForm(TfrmCaixa_CPFNotaFiscal, frmCaixa_CPFNotaFiscal);
  Application.CreateForm(TfrmTipoAdiantamento, frmTipoAdiantamento);
  Application.CreateForm(TfrmConciliaBancoManual_TituloMovimentado, frmConciliaBancoManual_TituloMovimentado);
  Application.CreateForm(TfrmAliquotaPisCofins, frmAliquotaPisCofins);
  Application.CreateForm(TfrmEmitePisCofinsVendas, frmEmitePisCofinsVendas);
  Application.CreateForm(TrEmitePisCofins_View, rEmitePisCofins_View);
  Application.CreateForm(TrEmitePisCofinsSint_View, rEmitePisCofinsSint_View);
  Application.CreateForm(TfrmPickPack, frmPickPack);
  Application.CreateForm(TfrmTerceiro_MediaVenda, frmTerceiro_MediaVenda);
  Application.CreateForm(TfrmProvisaoPagtoTituloAdiantamento, frmProvisaoPagtoTituloAdiantamento);
  Application.CreateForm(TfrmGeraDoctoFiscalCompl, frmGeraDoctoFiscalCompl);
  Application.CreateForm(TfrmGeracaoComplemento, frmGeracaoComplemento);
  Application.CreateForm(TfrmTerceiro_ChequePendente, frmTerceiro_ChequePendente);
  Application.CreateForm(TfrmTerceiro_VendaAutorizada, frmTerceiro_VendaAutorizada);
  Application.CreateForm(TfrmTerceiro_PosicaoReceber, frmTerceiro_PosicaoReceber);
  Application.CreateForm(TfrmMetaMensal, frmMetaMensal);
  Application.CreateForm(TfrmMetaSemanal, frmMetaSemanal);
  Application.CreateForm(TfrmAlcadasSP, frmAlcadasSP);
  Application.CreateForm(TfrmMetaVendedor, frmMetaVendedor);
  Application.CreateForm(TfrmMetaApontamentoHoraOportunidade, frmMetaApontamentoHoraOportunidade);
  Application.CreateForm(TfrmMetaVendedor_CotaVendedor, frmMetaVendedor_CotaVendedor);
  Application.CreateForm(TfrmMetaApontamentoHora_Vendedor, frmMetaApontamentoHora_Vendedor);
  Application.CreateForm(TfrmEmiteNFe2, frmEmiteNFe2);
  Application.CreateForm(TfrmAcompMetaMensalLoja, frmAcompMetaMensalLoja);
  Application.CreateForm(TrptClientesSemCompraPeriodo, rptClientesSemCompraPeriodo);
  Application.CreateForm(TrptClientesSemCompraPeriodo_View, rptClientesSemCompraPeriodo_View);
  Application.CreateForm(TfrmResumoDesempSemanalLoja, frmResumoDesempSemanalLoja);}

  {Application.CreateForm(TfrmTipoAlerta, frmTipoAlerta);}
  //Application.CreateForm(TSplashScreen, SplashScreen);

  SplashScreen.Hide;
  SplashScreen.Free;

  Application.Run;
end.
