inherited rptRankingCompraCliente: TrptRankingCompraCliente
  Left = 337
  Top = 248
  Width = 758
  Height = 320
  Caption = 'Rel. Ranking Compra Cliente'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 742
    Height = 253
  end
  object Label1: TLabel [1]
    Left = 13
    Top = 73
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo'
  end
  object Label2: TLabel [2]
    Left = 139
    Top = 74
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label3: TLabel [3]
    Left = 29
    Top = 48
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  inherited ToolBar1: TToolBar
    Width = 742
    TabOrder = 3
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit1: TMaskEdit [5]
    Left = 56
    Top = 64
    Width = 71
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 1
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [6]
    Left = 166
    Top = 65
    Width = 71
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 124
    Top = 40
    Width = 605
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsloja
    TabOrder = 4
  end
  object ER2lpkLoja: TER2LookupMaskEdit [8]
    Left = 56
    Top = 40
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  object rgModeloImp: TRadioGroup [9]
    Left = 56
    Top = 96
    Width = 185
    Height = 41
    Caption = ' Modelo '
    Color = 13086366
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Impress'#227'o'
      'Planilha')
    ParentColor = False
    TabOrder = 5
  end
  inherited ImageList1: TImageList
    Left = 328
    Top = 80
  end
  object ER2Excel: TER2Excel
    Titulo = 'Rel. Ranking Compra Cliente'
    AutoSizeCol = False
    Background = clWhite
    Transparent = False
    Left = 328
    Top = 112
  end
  object dsloja: TDataSource
    DataSet = qryLoja
    Left = 296
    Top = 112
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 296
    Top = 80
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
end
