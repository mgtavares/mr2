unit fAreaVendas;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, DBGridEhGrouping, GridsEh, DBGridEh, cxPC, cxControls,
  StdCtrls, Mask, DBCtrls;

type
  TfrmAreaVendas = class(TfrmCadastro_Padrao)
    qryMasternCdAreaVenda: TIntegerField;
    qryMastercNmAreaVenda: TStringField;
    qryMasternCdStatus: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryStatus: TADOQuery;
    qryDivisaoVenda: TADOQuery;
    qryStatusnCdStatus: TIntegerField;
    qryStatuscNmStatus: TStringField;
    DBEdit4: TDBEdit;
    dsStatus: TDataSource;
    qryDivisaoVendanCdDivisaoVenda: TIntegerField;
    qryDivisaoVendacNmDivisaoVenda: TStringField;
    qryDivisaoVendanCdAreaVenda: TIntegerField;
    qryDivisaoVendanCdStatus: TIntegerField;
    dsDivisaoVenda: TDataSource;
    procedure FormCreate(Sender: TObject);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBEdit3Enter(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryDivisaoVendaBeforePost(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBEdit3Exit(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAreaVendas: TfrmAreaVendas;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAreaVendas.FormCreate(Sender: TObject);
begin
  inherited;

  cNmTabelaMaster   := 'AREAVENDA';
  nCdTabelaSistema  := 430;
  nCdConsultaPadrao := 753;
  bLimpaAposSalvar  := False;
  
end;

procedure TfrmAreaVendas.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;
end;

procedure TfrmAreaVendas.DBEdit3Enter(Sender: TObject);
begin
  inherited;

  if not (qryMaster.Active) then
      exit;

  if (qryMaster.State <> dsInsert) then
      exit;

  qryMasternCdStatus.Value := 1;

  qryStatus.Close;
  PosicionaQuery(qryStatus,qryMasternCdStatus.AsString);
  
end;

procedure TfrmAreaVendas.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (Trim(qryMastercNmAreaVenda.Value) = '') then
  begin
      MensagemAlerta('Digite a Descri��o da �rea de Vendas.');
      DBEdit2.SetFocus;
      abort;
  end;

  inherited;
  
end;

procedure TfrmAreaVendas.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryDivisaoVenda.Close;
  PosicionaQuery(qryDivisaoVenda,qryMasternCdAreaVenda.AsString);

  qryStatus.Close;
  PosicionaQuery(qryStatus,qryMasternCdStatus.AsString);
  
end;

procedure TfrmAreaVendas.qryDivisaoVendaBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (Trim(qryDivisaoVendacNmDivisaoVenda.Value) = '') then
  begin
      MensagemAlerta('Digite a descri��o da Divis�o de Venda.');
      DBGridEh1.Col := 2;
      abort;
  end;

  qryDivisaoVendanCdDivisaoVenda.Value := frmMenu.fnProximoCodigo('DIVISAOVENDA');

  qryDivisaoVendanCdAreaVenda.Value := qryMasternCdAreaVenda.Value;

  if (qryDivisaoVendanCdStatus.AsString = '') then
      qryDivisaoVendanCdStatus.Value := 1;

      
end;

procedure TfrmAreaVendas.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
  
end;

procedure TfrmAreaVendas.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryDivisaoVenda.Close;
  qryStatus.Close;
  
end;

procedure TfrmAreaVendas.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryDivisaoVenda.Close;
  PosicionaQuery(qryDivisaoVenda,qryMasternCdAreaVenda.AsString);
  
end;

procedure TfrmAreaVendas.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryStatus.Close;
  PosicionaQuery(qryStatus,qryMasternCdStatus.AsString);
  
end;

procedure TfrmAreaVendas.btSalvarClick(Sender: TObject);
begin
  inherited;

  btCancelar.Click;
end;

initialization
    RegisterClass(TfrmAreaVendas);

end.
