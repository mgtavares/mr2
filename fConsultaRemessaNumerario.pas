unit fConsultaRemessaNumerario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh, cxPC, cxControls,
  StdCtrls, Mask, DBCtrls, ER2Lookup, Menus;

type
  TfrmConsultaRemessaNumerario = class(TfrmProcesso_Padrao)
    qryRemessaNum: TADOQuery;
    qryContaBancaria: TADOQuery;
    qryContaBancarianCdContaBancaria: TIntegerField;
    qryContaBancarianCdConta: TStringField;
    dsContaBancaria: TDataSource;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    DBGridEh4: TDBGridEh;
    GroupBox1: TGroupBox;
    er2LkpLoja: TER2LookupMaskEdit;
    DBEdit1: TDBEdit;
    er2LkpContaBancaria: TER2LookupMaskEdit;
    DBEdit2: TDBEdit;
    rdgSituacao: TRadioGroup;
    mskdDtInicial: TMaskEdit;
    Label5: TLabel;
    mskdDtFinal: TMaskEdit;
    Label4: TLabel;
    Label3: TLabel;
    Label1: TLabel;
    dsRemessaNum: TDataSource;
    qryRemessaNumnCdRemessaNum: TIntegerField;
    qryRemessaNumdDtRemessa: TDateTimeField;
    qryRemessaNumnCdConta: TStringField;
    qryRemessaNumnCdConta_1: TStringField;
    qryRemessaNumdDtConfirma: TDateTimeField;
    qryRemessaNumdDtCancel: TDateTimeField;
    qryRemessaNumcNmUsuario: TStringField;
    qryRemessaNumcNmUsuario_1: TStringField;
    qryRemessaNumnValDinheiro: TBCDField;
    qryRemessaNumnValCheque: TBCDField;
    qryRemessaNumnValTransacao: TBCDField;
    qryRemessaNumnValCrediario: TBCDField;
    qryRemessaNumcOBS: TStringField;
    procedure er2LkpContaBancariaBeforeLookup(Sender: TObject);
    procedure qryContaBancariaBeforeOpen(DataSet: TDataSet);
    procedure DBGridEh4DblClick(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaRemessaNumerario: TfrmConsultaRemessaNumerario;

implementation

uses
  fMenu, fConsultaRemessaNumerario_Lancamentos, DateUtils;

{$R *.dfm}

procedure TfrmConsultaRemessaNumerario.er2LkpContaBancariaBeforeLookup(
  Sender: TObject);
begin
  inherited;

  er2LkpContaBancaria.WhereAdicional.Text := 'ContaBancaria.nCdLoja = ' + qryLojanCdLoja.AsString + ' AND ContaBancaria.nCdEmpresa = ' + IntToStr(frmMenu.nCdEmpresaAtiva);
end;

procedure TfrmConsultaRemessaNumerario.qryContaBancariaBeforeOpen(
  DataSet: TDataSet);
begin
  inherited;

  qryContaBancaria.Parameters.ParamByName('nCdLoja').Value    := qryLojanCdLoja.Value;
  qryContaBancaria.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
end;

procedure TfrmConsultaRemessaNumerario.DBGridEh4DblClick(Sender: TObject);
var
  objForm : TfrmConsultaRemessaNumerario_Lancamentos;
begin
  inherited;

  if (not qryRemessaNum.IsEmpty) then
  begin
      objForm := TfrmConsultaRemessaNumerario_Lancamentos.Create(nil);

      with objForm do
      begin
          PosicionaQuery(qryChequeRemessaNum, qryRemessaNumnCdRemessaNum.AsString);
          PosicionaQuery(qryCartaoRemessaNum, qryRemessaNumnCdRemessaNum.AsString);
          PosicionaQuery(qryCrediarioRemessaNum, qryRemessaNumnCdRemessaNum.AsString);
      end;

      showForm(objForm, True);
  end;
end;

procedure TfrmConsultaRemessaNumerario.ToolButton1Click(Sender: TObject);
var
  cFlgEmAberto   : Integer;
  cFlgConfirmado : Integer;
  cFlgCancelado  : Integer;
begin
  //inherited;

  if (qryLoja.IsEmpty) then
  begin
      MensagemAlerta('Selecione a loja.');
      er2LkpLoja.SetFocus;
      Abort;
  end;

  cFlgEmAberto     := 0;
  cFlgConfirmado   := 0;
  cFlgCancelado    := 0;

  case (rdgSituacao.ItemIndex) of
      0 : cFlgEmAberto   := 1;
      1 : cFlgConfirmado := 1;
      2 : cFlgCancelado  := 1;
  end;

  if (mskdDtInicial.Text = '  /  /    ') then
      mskdDtInicial.Text := DateTimeToStr(Date());

  if (mskdDtFinal.Text = '  /  /    ') then
      mskdDtFinal.Text := DateTimeToStr(Date());

  qryRemessaNum.Close;
  qryRemessaNum.Parameters.ParamByName('cFlgEmAberto').Value     := cFlgEmAberto;
  qryRemessaNum.Parameters.ParamByName('cFlgConfirmado').Value   := cFlgConfirmado;
  qryRemessaNum.Parameters.ParamByName('cFlgCancelado').Value    := cFlgCancelado;
  qryRemessaNum.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva;
  qryRemessaNum.Parameters.ParamByName('nCdLoja').Value          := qryLojanCdLoja.Value;
  qryRemessaNum.Parameters.ParamByName('nCdContaBancaria').Value := frmMenu.ConvInteiro(er2LkpContaBancaria.Text);
  qryRemessaNum.Parameters.ParamByName('dDtInicial').Value       := frmMenu.ConvData(mskdDtInicial.Text);
  qryRemessaNum.Parameters.ParamByName('dDtFinal').Value         := frmMenu.ConvData(mskdDtFinal.Text);
  qryRemessaNum.Open;


end;

procedure TfrmConsultaRemessaNumerario.FormActivate(Sender: TObject);
begin
  inherited;

  er2LkpLoja.SetFocus;
end;

initialization
  RegisterClass(TfrmConsultaRemessaNumerario);

end.
