unit fPDVValePresente;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit, jpeg, ExtCtrls;

type
  TfrmPdvValePresente = class(TForm)
    Image1: TImage;
    edtValValePresente: TcxCurrencyEdit;
    Label1: TLabel;
    procedure edtValValePresenteKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPdvValePresente: TfrmPdvValePresente;

implementation

{$R *.dfm}

procedure TfrmPdvValePresente.edtValValePresenteKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        close;

end;

end.
