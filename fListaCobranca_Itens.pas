unit fListaCobranca_Itens;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DB, ADODB, GridsEh, DBGridEh, cxPC, cxControls, Menus,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmListaCobranca_Itens = class(TfrmProcesso_Padrao)
    qryItensLista: TADOQuery;
    dsItensLista: TDataSource;
    qryItensListanCdTerceiro: TIntegerField;
    qryItensListacNmTerceiro: TStringField;
    qryItensListanValTotalAberto: TBCDField;
    qryItensListanValVencido: TBCDField;
    qryItensListacNmTabTipoMotivoListaCobranca: TStringField;
    qryItensContatados: TADOQuery;
    dsItensContatados: TDataSource;
    qryItensContatadosnCdTerceiro: TIntegerField;
    qryItensContatadoscNmTerceiro: TStringField;
    qryItensContatadosnValTotalAberto: TBCDField;
    qryItensContatadosnValVencido: TBCDField;
    qryItensContatadoscNmTabTipoMotivoListaCobranca: TStringField;
    qryItensContatadosdDtEncerramento: TDateTimeField;
    ToolButton4: TToolButton;
    btVerificarLista: TToolButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    qryItensListanCdItemListaCobranca: TAutoIncField;
    qryItensContatadoscNmUltimoResultado: TStringField;
    lblAnalista: TLabel;
    PopupMenu1: TPopupMenu;
    itemTrocarListaCobranca: TMenuItem;
    qryAux: TADOQuery;
    GerarNovaLista1: TMenuItem;
    qryGerarNovaLista: TADOQuery;
    qryGerarNovaListanCdListaCobrancaNova: TIntegerField;
    ToolButton6: TToolButton;
    btGerarCarta: TToolButton;
    qryListaCobranca: TADOQuery;
    qryListaCobrancacFlgCarta: TIntegerField;
    ToolButton8: TToolButton;
    btEncerraLista: TToolButton;
    qryEncerraLista: TADOQuery;
    btNegativaLista: TToolButton;
    qryListaCobrancacFlgNegativar: TIntegerField;
    qryListaCobrancacFlgCobradora: TIntegerField;
    qryPreparaNegativacao: TADOQuery;
    ToolButton10: TToolButton;
    qryListaCobrancacFlgNegativado: TIntegerField;
    qryEstornaNegativacao: TADOQuery;
    btEnviarCobradora: TToolButton;
    qryListaCobrancacFlgEnviadoCobradora: TIntegerField;
    ToolButton12: TToolButton;
    qryPreparaCobradora: TADOQuery;
    qryGerarContatoAuto: TADOQuery;
    procedure FormShow(Sender: TObject);
    procedure ExibeItensLista(nCdListaCobranca:integer; bAdmin:boolean; bTelefone:boolean; bNegativar:boolean);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure DBGridEh2DblClick(Sender: TObject);
    procedure cxTabSheet2Show(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure itemTrocarListaCobrancaClick(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure GerarNovaLista1Click(Sender: TObject);
    procedure btGerarCartaClick(Sender: TObject);
    procedure btEncerraListaClick(Sender: TObject);
    procedure btNegativaListaClick(Sender: TObject);
    procedure btEstNegativClick(Sender: TObject);
    procedure btEnviarCobradoraClick(Sender: TObject);
    procedure btVerificarListaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    cFlgAdmin    : Boolean;
    nCdListaCobr : Integer;
  end;

var
  frmListaCobranca_Itens: TfrmListaCobranca_Itens;

implementation

uses fCobranca_VisaoContato, fMenu, fAdmListaCobranca_Listas_SelLista,
  fListaCobranca_Itens_NovaLista, fListaCobranca_Itens_GerarCarta,
  fListaCobranca_Itens_Negativacao, fListaCobranca_Itens_EnvioCobradora,
  rFichaCobradora_view, fListaCobranca_Itens_SelCliCobradora;

{$R *.dfm}

procedure TfrmListaCobranca_Itens.ExibeItensLista(
  nCdListaCobranca: integer; bAdmin:boolean; bTelefone:boolean; bNegativar:boolean);
begin
    nCdListaCobr := nCdListaCobranca;

    qryItensLista.Close ;
    qryItensContatados.Close;

    PosicionaQuery(qryItensLista, intToStr(ncdListaCobranca)) ;

    if (qryItensLista.eof) then
        ShowMessage('Nenhum cliente pendente de contato.') ;

    cFlgAdmin := bAdmin ;

    btEncerraLista.Enabled := True ;

    if bTelefone then
        btEncerraLista.Enabled := False ;

    btNegativaLista.Enabled := bNegativar ;
    Self.ShowModal;

    cFlgAdmin := false ;
end;

procedure TfrmListaCobranca_Itens.FormShow(Sender: TObject);
begin
  inherited;
  lblAnalista.Caption    := 'Analista de Cobran�a: ' + frmMenu.cNmCompletoUsuario;
  lblAnalista.Font.Size  := 12 ;
  lblAnalista.Font.Color := clBlue ;

  DBGridEh1.PopupMenu  := nil ;
  DBGridEh1.Options    := [dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect,dgConfirmDelete,dgCancelOnExit] ;

  {btEstNegativ.Enabled := cFlgAdmin ;}

  cxPageControl1.ActivePageIndex := 0 ;
  DBGridEh2.Columns[5].Width     := 154 ;

  qryListaCobranca.Close;
  PosicionaQuery(qryListaCobranca,qryItensLista.Parameters.ParamByName('nPK').Value) ;

  btEnviarCobradora.Enabled := (qryListaCobrancacFlgCobradora.Value = 1) ;

  if (cFlgAdmin) then
  begin
      DBGridEh1.PopupMenu := PopupMenu1 ;
      DBGridEh1.Options   := [dgTitles,dgIndicator,dgColumnResize,dgColLines,dgRowLines,dgTabs,dgRowSelect,dgConfirmDelete,dgCancelOnExit,dgMultiSelect] ;

      btEnviarCobradora.Enabled := True ;
      btNegativaLista.Enabled   := True ;
      btEncerraLista.Enabled    := True ;
      btGerarCarta.Enabled      := True ;

  end ;

end;

procedure TfrmListaCobranca_Itens.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmCobranca_VisaoContato;
begin
  inherited;

  if (qryItensLista.Active) then
  begin

      objForm := TfrmCobranca_VisaoContato.Create(nil);

      objForm.nCdListaCobranca     := qryItensLista.Parameters.ParamByName('nPK').Value ;
      objForm.nCdItemListaCobranca := qryItensListanCdItemListaCobranca.Value ;
      objForm.cContatoPendente     := True ;
      objForm.ExibeDados(qryItensListanCdTerceiro.Value);

      qryItensLista.Requery();

      FreeAndNil(objForm);

  end ;

end;

procedure TfrmListaCobranca_Itens.DBGridEh2DblClick(Sender: TObject);
var
  objForm : TfrmCobranca_VisaoContato;
begin
  inherited;
  
  if (qryItensContatados.Active) then
  begin
      objForm := TfrmCobranca_VisaoContato.Create(nil);

      objForm.nCdListaCobranca     := qryItensLista.Parameters.ParamByName('nPK').Value ;
      objForm.cContatoPendente     := False ;
      objForm.nCdItemListaCobranca := qryItensListanCdItemListaCobranca.Value ;
      objForm.ExibeDados(qryItensContatadosnCdTerceiro.Value);

      FreeAndNil(objForm);
  end ;

end;

procedure TfrmListaCobranca_Itens.cxTabSheet2Show(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryItensContatados, qryItensLista.Parameters.ParamByName('nPK').Value) ;

end;

procedure TfrmListaCobranca_Itens.PopupMenu1Popup(Sender: TObject);
begin
  inherited;

  itemTrocarListaCobranca.Enabled := (DBGridEh1.SelectedRows.Count > 0) ;

end;

procedure TfrmListaCobranca_Itens.itemTrocarListaCobrancaClick(
  Sender: TObject);
var
  i: Integer;
  objForm : TfrmAdmListaCobranca_Listas_SelLista;
begin
  inherited;

  objForm := TfrmAdmListaCobranca_Listas_SelLista.Create(nil);

  objForm.nCdListaCobrancaAtual := qryItensLista.Parameters.ParamByName('nPK').Value ;
  showForm(objForm,false);

  if (objForm.qryListas.Active) then
  begin

      if (objForm.qryListasnCdListaCobranca.Value <> qryItensLista.Parameters.ParamByName('nPK').Value) then
      begin
          if (MessageDlg('Confirma a transfer�ncia dos itens selecionados para a nova lista de cobran�a ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
              exit ;

          frmMenu.Connection.BeginTrans;

          try

              for i := 0 to DBGridEh1.SelectedRows.Count - 1 do
              begin
                qryItensLista.GotoBookmark(pointer(DBGridEh1.SelectedRows.Items[i]));

                {-- Troca o item de lista --}
                qryAux.Close;
                qryAux.SQL.Clear;
                qryAux.SQL.Add('UPDATE ItemListaCobranca SET nCdListaCobranca = ' + objForm.qryListasnCdListaCobranca.AsString + ' WHERE nCdItemListaCobranca = ' + qryItenslistanCdItemListaCobranca.AsString) ;
                qryAux.ExecSQL;

                {-- debita quantidade de cliente da lista atual --}
                qryAux.SQL.Clear;
                qryAux.SQL.Add('UPDATE ListaCobranca SET iQtdeClienteTotal = iQtdeClienteTotal - 1 WHERE nCdListaCobranca = ' + String(qryItensLista.Parameters.ParamByName('nPK').Value)) ;
                qryAux.ExecSQL;

                {-- caso a lista atual fique sem clientes, cancela a lista --}
                qryAux.SQL.Clear;
                qryAux.SQL.Add('UPDATE ListaCobranca SET dDtCancel = GetDate() WHERE iQtdeClienteTotal <= 0 AND nCdListaCobranca = ' + String(qryItensLista.Parameters.ParamByName('nPK').Value)) ;
                qryAux.ExecSQL;

                {-- caso a lista atual fique sem clientes, cancela a lista --}
                qryAux.SQL.Clear;
                qryAux.SQL.Add('UPDATE ListaCobranca SET dDtEncerramento = GetDate() WHERE (iQtdeClienteTotal > 0) AND (iQtdeClienteTotal - iQtdeClienteContato - iQtdeSemContato) <= 0 AND nCdListaCobranca = ' + String(qryItensLista.Parameters.ParamByName('nPK').Value)) ;
                qryAux.ExecSQL;

                {-- credita a quantidade de clientes da nova lista --}
                qryAux.SQL.Clear;
                qryAux.SQL.Add('UPDATE ListaCobranca SET iQtdeClienteTotal = iQtdeClienteTotal + 1, dDtEncerramento = NULL, dDtCancel = NULL WHERE nCdListaCobranca = ' + objForm.qryListasnCdListaCobranca.AsString) ;
                qryAux.ExecSQL;
              end;

          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          frmMenu.Connection.CommitTrans;

          qryItensLista.Requery();

      end ;

  end ;

end;

procedure TfrmListaCobranca_Itens.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (gdSelected in State) and (DbGridEh1.SelectedRows.Count > 1) then
  begin
      DBGridEh1.Canvas.Brush.Color := clYellow ;
      DBGridEh1.Canvas.Font.Style  := [fsBold] ;
  end ;

  DBGridEh1.Canvas.FillRect(Rect);
  DBGridEh1.DefaultDrawColumnCell(Rect, DataCol, Column, State);


end;

procedure TfrmListaCobranca_Itens.GerarNovaLista1Click(Sender: TObject);
var
  i:integer;
  objForm : TfrmListaCobranca_Itens_Novalista;
begin
  inherited;

  objForm := TfrmListaCobranca_Itens_Novalista.Create(nil);

  objForm.nCdListaCobrancaAtual := qryItensLista.Parameters.ParamByName('nPK').Value ;
  showForm(objForm,false);

  if (objForm.cGerarLista) then
  begin

      frmMenu.Connection.BeginTrans;

      try
          qryGerarNovalista.Close;
          qryGerarNovalista.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.ConvInteiro(objForm.edtUsuario.Text) ;
          qryGerarNovalista.Parameters.ParamByName('nCdListaCobranca').Value := qryItenslistanCdItemListaCobranca.Value ;
          qryGerarNovalista.Open;

          {-- Caso a nova lista tenha sido gerada, adiciona os itens --}
          if (not qryGerarNovaLista.Eof) and (qryGerarNovalistanCdListaCobrancaNova.Value > 0) then
          begin

              for i := 0 to DBGridEh1.SelectedRows.Count - 1 do
              begin
                qryItensLista.GotoBookmark(pointer(DBGridEh1.SelectedRows.Items[i]));

                {-- Troca o item de lista --}
                qryAux.Close;
                qryAux.SQL.Clear;
                qryAux.SQL.Add('UPDATE ItemListaCobranca SET nCdListaCobranca = ' + qryGerarNovalistanCdListaCobrancaNova.AsString + ' WHERE nCdItemListaCobranca = ' + qryItenslistanCdItemListaCobranca.AsString) ;
                qryAux.ExecSQL;

                {-- debita quantidade de cliente da lista atual --}
                qryAux.SQL.Clear;
                qryAux.SQL.Add('UPDATE ListaCobranca SET iQtdeClienteTotal = iQtdeClienteTotal - 1 WHERE nCdListaCobranca = ' + String(qryItensLista.Parameters.ParamByName('nPK').Value)) ;
                qryAux.ExecSQL;

                {-- caso a lista atual fique sem clientes, cancela a lista --}
                qryAux.SQL.Clear;
                qryAux.SQL.Add('UPDATE ListaCobranca SET dDtCancel = GetDate() WHERE iQtdeClienteTotal <= 0 AND nCdListaCobranca = ' + String(qryItensLista.Parameters.ParamByName('nPK').Value)) ;
                qryAux.ExecSQL;

                {-- caso a lista atual fique sem clientes, cancela a lista --}
                qryAux.SQL.Clear;
                qryAux.SQL.Add('UPDATE ListaCobranca SET dDtEncerramento = GetDate() WHERE (iQtdeClienteTotal > 0) AND (iQtdeClienteTotal - iQtdeClienteContato - iQtdeSemContato) <= 0 AND nCdListaCobranca = ' + String(qryItensLista.Parameters.ParamByName('nPK').Value)) ;
                qryAux.ExecSQL;

                {-- credita a quantidade de clientes da nova lista --}
                qryAux.SQL.Clear;
                qryAux.SQL.Add('UPDATE ListaCobranca SET iQtdeClienteTotal = iQtdeClienteTotal + 1, dDtEncerramento = NULL, dDtCancel = NULL WHERE nCdListaCobranca = ' + qryGerarNovalistanCdListaCobrancaNova.AsString) ;
                qryAux.ExecSQL;
              end;


          end ;

      except
          qryGerarNovalista.Close;
          qryAux.Close ;
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.') ;
          raise ;
      end ;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Lista gerada com Sucesso.' +#13#13+'C�digo nova lista: ' + frmMenu.ZeroEsquerda(qryGerarNovalistanCdListaCobrancaNova.asString,10));

      qryGerarNovalista.Close;
      qryAux.Close ;
      
      qryItensLista.Requery();

  end ;

end;

procedure TfrmListaCobranca_Itens.btGerarCartaClick(Sender: TObject);
var
  objForm : TfrmListaCobranca_Itens_GerarCarta;
begin
  inherited;

  if (MessageDlg('Confirma a gera��o das cartas de cobran�a ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  qryListaCobranca.Close;
  PosicionaQuery(qryListaCobranca,qryItensLista.Parameters.ParamByName('nPK').Value) ;

  if (qryListaCobrancacFlgCarta.Value = 1) then
      if (MessageDlg('Esta lista n�o foi criada para envio de cartas. Deseja realmente gerar cartas de cobran�a para esta lista ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

  objForm := TfrmListaCobranca_Itens_GerarCarta.Create(nil);

  objForm.nCdListaCobrancaAtual := qryItensLista.Parameters.ParamByName('nPK').Value;
  showForm(objForm,true);

end;

procedure TfrmListaCobranca_Itens.btEncerraListaClick(Sender: TObject);
begin
  inherited;

  if (MessageDlg('Confirma o encerramento desta lista ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryEncerraLista.Close;
      qryEncerraLista.Parameters.ParamByName('nCdListaCobranca').Value := qryItensLista.Parameters.ParamByName('nPK').Value ;
      qryEncerraLista.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Lista Encerrada com Sucesso.') ;

  Close;


end;

procedure TfrmListaCobranca_Itens.btNegativaListaClick(Sender: TObject);
var
  objForm : TfrmListaCobranca_Itens_Negativacao;
begin
  inherited;

  qryListaCobranca.Close;
  PosicionaQuery(qryListaCobranca,qryItensLista.Parameters.ParamByName('nPK').Value) ;

  if (qryListaCobrancacFlgNegativar.Value = 0) then
  begin
      MensagemErro('Esta lista n�o foi criada para negativa��o.') ;
  end ;

  if (qryListaCobrancacFlgNegativado.Value = 1) then
  begin
      MensagemAlerta('Os clientes dessa lista j� foram negativados.') ;
  end ;

  objForm := TfrmListaCobranca_Itens_Negativacao.Create(nil);

  objForm.qryPreparaTemp.ExecSQL;

  qryPreparaNegativacao.Close;
  qryPreparaNegativacao.Parameters.ParamByName('nCdListaCobranca').Value := qryItensLista.Parameters.ParamByName('nPK').Value ;
  qryPreparaNegativacao.ExecSQL;

  objForm.qryTempTerceiroNegativar.Close;
  objForm.qryTempTerceiroNegativar.Open;

  objForm.nCdListaCobranca := qryItensLista.Parameters.ParamByName('nPK').Value ;

  showForm(objForm,true);

end;

procedure TfrmListaCobranca_Itens.btEstNegativClick(Sender: TObject);
begin
  inherited;

  qryListaCobranca.Close;
  PosicionaQuery(qryListaCobranca,qryItensLista.Parameters.ParamByName('nPK').Value) ;

  if (qryListaCobrancacFlgNegativado.Value = 0) then
  begin
      MensagemErro('Os clientes dessa lista ainda n�o foram negativados.') ;
      abort ;
  end ;

  if (MessageDlg('Confirma o estorno da negativa��o dessa lista ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  frmMenu.Connection.BeginTrans;

  try
      qryEstornaNegativacao.Close;
      qryEstornaNegativacao.Parameters.ParamByName('nCdListaCobranca').Value := qryItensLista.Parameters.ParamByName('nPK').Value ;
      qryEstornaNegativacao.ExecSQL;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Negativa��o estornada com sucesso.') ;

end;

procedure TfrmListaCobranca_Itens.btEnviarCobradoraClick(Sender: TObject);
var
  objForm : TfrmListaCobranca_Itens_SelCliCobradora;
begin
  inherited;

  qryListaCobranca.Close;
  PosicionaQuery(qryListaCobranca,qryItensLista.Parameters.ParamByName('nPK').Value) ;

  if (qryListaCobrancacFlgCobradora.Value = 0) then
      MensagemErro('Esta lista n�o foi criada para enviar para cobradora.') ;

  if (qryListaCobrancacFlgEnviadoCobradora.Value = 1) then
      MensagemErro('Esta lista j� foi enviada para cobradora.') ;

  {--Prepara a temp--}
  objForm := TfrmListaCobranca_Itens_SelCliCobradora.Create(nil);

  objForm.qryPreparaTemp.ExecSQL;

  qryPreparaCobradora.Close;
  qryPreparaCobradora.Parameters.ParamByName('nCdListaCobranca').Value := qryItensLista.Parameters.ParamByName('nPK').Value ;
  qryPreparaCobradora.ExecSQL;

  {--Abre a tela para selecionar os clientes--}
  objForm.qryTempTerceiroNegativar.Close;
  objForm.qryTempTerceiroNegativar.Open;

  objForm.nCdListaCobranca := qryItensLista.Parameters.ParamByName('nPK').Value ;

  objForm.ToolButton1.Enabled := (qryListaCobrancacFlgEnviadoCobradora.Value = 0) ;
  showForm(objForm,true);

end;

procedure TfrmListaCobranca_Itens.btVerificarListaClick(Sender: TObject);
begin
  inherited;

  if (MessageDlg('Confirma verifica��o dos itens desta lista ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
      Exit;

  { -- verifica se os t�tulos presentes nesta lista foram liquidados ap�s sua gera��o -- }
  qryAux.Close;
  qryAux.SQL.Clear;
  qryAux.SQL.Add('SELECT TOP 1 1                                                                                                          ');
  qryAux.SQL.Add('  FROM ItemListaCobranca                                                                                                ');
  qryAux.SQL.Add('       INNER JOIN TituloListaCobranca TituloLC ON TituloLC.nCdItemListaCobranca = ItemListaCobranca.nCdItemListaCobranca');
  qryAux.SQL.Add('       INNER JOIN Titulo                       ON Titulo.nCdTitulo              = TituloLC.nCdTitulo                    ');
  qryAux.SQL.Add(' WHERE nCdListaCobranca                  = ' + IntToStr(nCdListaCobr)                                                    );
  qryAux.SQL.Add('   AND ItemListaCobranca.dDtEncerramento IS NULL                                                                        ');
  qryAux.SQL.Add('   AND Titulo.nSaldoTit                  = 0                                                                            ');
  qryAux.Open;

  if (not qryAux.IsEmpty) then
  begin
      if (MessageDlg('Existem t�tulos na lista de cobran�a selecionada que j� foram pagos anteriormente.' + #13#13 + 'Deseja registrar o contato autom�tico para estes clientes ?',mtConfirmation,[mbYes,mbNo],0) = mrNo) then
          Exit;

      try
          frmMenu.Connection.BeginTrans;

          qryGerarContatoAuto.Close;
          qryGerarContatoAuto.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
          qryGerarContatoAuto.Parameters.ParamByName('nCdLoja').Value          := frmMenu.nCdLojaAtiva;
          qryGerarContatoAuto.Parameters.ParamByName('nCdListaCobranca').Value := nCdListaCobr;
          qryGerarContatoAuto.ExecSQL;

          frmMenu.Connection.CommitTrans;

          qryItensLista.Requery();
          qryItensContatados.Requery();
      except
          frmMenu.Connection.RollbackTrans;
          MensagemErro('Erro no processamento.');
          Raise;
      end;
  end;
  
  ShowMessage('Verifica��o da lista de cobran�a finalizado com sucesso.');
end;

end.
