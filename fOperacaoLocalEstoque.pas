unit fOperacaoLocalEstoque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, GridsEh, DBGridEh, DB, ADODB, ImgList,
  ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmOperacaoLocalEstoque = class(TfrmProcesso_Padrao)
    qryOperacaoLocalEstoque: TADOQuery;
    qryOperacaoLocalEstoquenCdOperacaoLocalEstoque: TAutoIncField;
    qryOperacaoLocalEstoquenCdOperacaoEstoque: TIntegerField;
    qryOperacaoLocalEstoquenCdEmpresa: TIntegerField;
    qryOperacaoLocalEstoquenCdGrupoProduto: TIntegerField;
    qryOperacaoLocalEstoquenCdLocalEstoque: TIntegerField;
    qryOperacaoLocalEstoquenCdLoja: TIntegerField;
    DBGridEh1: TDBGridEh;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryOperacaoLocalEstoquecNmEmpresa: TStringField;
    qryOperacaoLocalEstoquecNmGrupoProduto: TStringField;
    qryOperacaoLocalEstoquecNmLocalEstoque: TStringField;
    qryOperacaoLocalEstoquecNmLoja: TStringField;
    dsOperacaoLocalEstoque: TDataSource;
    qryOperacaoEstoque: TADOQuery;
    qryOperacaoEstoquenCdOperacaoEstoque: TIntegerField;
    qryOperacaoEstoquecNmOperacaoEstoque: TStringField;
    qryOperacaoLocalEstoquecNmOperacaoEstoque: TStringField;
    qryLocalEstoquenCdLoja: TIntegerField;
    procedure FormShow(Sender: TObject);
    procedure qryOperacaoLocalEstoqueCalcFields(DataSet: TDataSet);
    procedure qryOperacaoEstoqueAfterScroll(DataSet: TDataSet);
    procedure qryEmpresaAfterScroll(DataSet: TDataSet);
    procedure qryLojaAfterScroll(DataSet: TDataSet);
    procedure qryLocalEstoqueAfterScroll(DataSet: TDataSet);
    procedure qryGrupoProdutoAfterScroll(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryOperacaoLocalEstoqueBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOperacaoLocalEstoque: TfrmOperacaoLocalEstoque;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmOperacaoLocalEstoque.FormShow(Sender: TObject);
begin
  inherited;
  qryOperacaoLocalEstoque.Close ;
  qryOperacaoLocalEstoque.Open ;

  DBGridEh1.Align := alClient ;

  if (frmMenu.LeParametro('VAREJO') <> 'S') then
  begin
      DBGridEh1.Columns[5].ReadOnly := True
  end ;

end;

procedure TfrmOperacaoLocalEstoque.qryOperacaoLocalEstoqueCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  PosicionaQuery(qryOperacaoEstoque,qryOperacaoLocalEstoquenCdOperacaoEstoque.AsString) ;
  PosicionaQuery(qryEmpresa, qryOperacaoLocalEstoquenCdEmpresa.AsString);

  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryOperacaoLocalEstoquenCdEmpresa.Value ;
  PosicionaQuery(qryLoja, qryOperacaoLocalEstoquenCdLoja.AsString);

  PosicionaQuery(qryGrupoProduto,  qryOperacaoLocalEstoquenCdGrupoProduto.AsString) ;

  //qryLocalEstoque.Parameters.ParamByName('nCdLoja').Value := qryOperacaoLocalEstoquenCdLoja.Value;
  //qryLocalEstoque.Parameters.ParamByName('nCdLoja2').Value := qryOperacaoLocalEstoquenCdLoja.Value;
  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryOperacaoLocalEstoquenCdEmpresa.Value;
  qryLocalEstoque.Parameters.ParamByName('nCdGrupoProduto').Value := qryOperacaoLocalEstoquenCdGrupoProduto.Value ;
  PosicionaQuery(qryLocalEstoque, qryOperacaoLocalEstoquenCdLocalEstoque.asString) ;
  
end;

procedure TfrmOperacaoLocalEstoque.qryOperacaoEstoqueAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  qryOperacaoLocalEstoquecNmOperacaoEstoque.Value := qryOperacaoEstoquecNmOperacaoEstoque.Value ;
end;

procedure TfrmOperacaoLocalEstoque.qryEmpresaAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  qryOperacaoLocalEstoquecNmEmpresa.value := qryEmpresacNmEmpresa.value ;
  
end;

procedure TfrmOperacaoLocalEstoque.qryLojaAfterScroll(DataSet: TDataSet);
begin
  inherited;
  qryOperacaoLocalEstoquecNmLoja.Value := qryLojacNmLoja.Value ;
end;

procedure TfrmOperacaoLocalEstoque.qryLocalEstoqueAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  qryOperacaoLocalEstoquecNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;
  
end;

procedure TfrmOperacaoLocalEstoque.qryGrupoProdutoAfterScroll(
  DataSet: TDataSet);
begin
  inherited;
  qryOperacaoLocalEstoquecNmGrupoProduto.Value := qryGrupoProdutocNmGrupoProduto.Value ;
  
end;

procedure TfrmOperacaoLocalEstoque.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryOperacaoLocalEstoque.State = dsBrowse) then
             qryOperacaoLocalEstoque.Edit ;

        if (qryOperacaoLocalEstoque.State = dsInsert) or (qryOperacaoLocalEstoque.State = dsEdit) then
        begin

            // operacao de estoque
            if (DBGridEh1.Col = 2) then
            begin
              nPK := frmLookup_Padrao.ExecutaConsulta(84);

              If (nPK > 0) then
              begin
                qryOperacaoLocalEstoquenCdOperacaoEstoque.Value := nPK ;
                PosicionaQuery(qryOperacaoEstoque, IntToStr(nPK)) ;
              end ;
            end ;

            // empresa
            if (DBGridEh1.Col = 4) then
            begin
              nPK := frmLookup_Padrao.ExecutaConsulta(8);

              If (nPK > 0) then
              begin
                qryOperacaoLocalEstoquenCdEmpresa.Value := nPK ;
                PosicionaQuery(qryEmpresa, IntToStr(nPK)) ;
              end ;
            end ;

            // loja
            if (DBGridEh1.Col = 6) and (not DbGridEh1.Columns[5].ReadOnly) then
            begin
              if (qryOperacaoLocalEstoquenCdEmpresa.Value = 0)then
              begin
                  ShowMessage('Informe a empresa.') ;
                  exit ;
              end ;

              nPK := frmLookup_Padrao.ExecutaConsulta2(59,'Empresa.nCdEmpresa = ' + qryOperacaoLocalEstoquenCdEmpresa.asString);

              If (nPK > 0) then
              begin
                qryOperacaoLocalEstoquenCdLoja.Value := nPK ;
                qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryOperacaoLocalEstoquenCdEmpresa.Value ;
                PosicionaQuery(qryLoja, IntToStr(nPK)) ;
              end ;
            end ;

            // grupo de produtos
            if (DBGridEh1.Col = 8) then
            begin

              nPK := frmLookup_Padrao.ExecutaConsulta(69);

              If (nPK > 0) then
              begin
                qryOperacaoLocalEstoquenCdGrupoProduto.Value := nPK ;
                PosicionaQuery(qryGrupoProduto, IntToStr(nPK)) ;
              end ;
            end ;

            // local de estoque
            if (DBGridEh1.Col = 10) then
            begin

              if (qryOperacaoLocalEstoquenCdEmpresa.Value = 0)then
              begin
                  ShowMessage('Informe a empresa.') ;
                  exit ;
              end ;


              if (frmMenu.LeParametro('VAREJO') = 'S') then
              begin
                  if (qryOperacaoLocalEstoquenCdLoja.Value = 0)then
                  begin
                      ShowMessage('Informe a Loja.') ;
                      exit ;
                  end ;

                  { -- removido filtro de loja, para permitir o v�nculo do mesmo local de estoque -- }
                  { -- para outras lojas que n�o estejam vinculadas ao cadastro do mesmo          -- }
                  //nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + qryOperacaoLocalEstoquenCdEmpresa.AsString + ' AND LocalEstoque.nCdLoja = ' + qryOperacaoLocalEstoquenCdLoja.AsString + ' AND EXISTS(SELECT 1 FROM GrupoProdutoLocalEstoque GPLE WHERE GPLE.nCdLocalEstoque = LocalEstoque.nCdLocalEstoque AND GPLE.nCdGrupoProduto = ' + qryOperacaoLocalEstoquenCdGrupoProduto.AsString + ')');
                  nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + qryOperacaoLocalEstoquenCdEmpresa.AsString + ' AND EXISTS(SELECT 1 FROM GrupoProdutoLocalEstoque GPLE WHERE GPLE.nCdLocalEstoque = LocalEstoque.nCdLocalEstoque AND GPLE.nCdGrupoProduto = ' + qryOperacaoLocalEstoquenCdGrupoProduto.AsString + ')');
              end
              else
              begin
                  nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' + qryOperacaoLocalEstoquenCdEmpresa.AsString + ' AND LocalEstoque.nCdLoja IS NULL AND EXISTS(SELECT 1 FROM GrupoProdutoLocalEstoque GPLE WHERE GPLE.nCdLocalEstoque = LocalEstoque.nCdLocalEstoque AND GPLE.nCdGrupoProduto = ' + qryOperacaoLocalEstoquenCdGrupoProduto.AsString + ')');
              end ;

              If (nPK > 0) then
              begin
                qryOperacaoLocalEstoquenCdLocalEstoque.Value := nPK ;
                qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value      := qryOperacaoLocalEstoquenCdEmpresa.Value ;
                //qryLocalEstoque.Parameters.ParamByName('nCdLoja').Value         := qryOperacaoLocalEstoquenCdLoja.Value ;
                //qryLocalEstoque.Parameters.ParamByName('nCdLoja2').Value         := qryOperacaoLocalEstoquenCdLoja.Value ;
                qryLocalEstoque.Parameters.ParamByName('nCdGrupoProduto').Value := qryOperacaoLocalEstoquenCdGrupoProduto.Value ;
                PosicionaQuery(qryLocalEstoque, IntToStr(nPK)) ;
              end ;

            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmOperacaoLocalEstoque.qryOperacaoLocalEstoqueBeforePost(
  DataSet: TDataSet);
begin

  if (qryOperacaoLocalEstoquenCdOperacaoEstoque.Value = 0) or (qryOperacaoLocalEstoquecNmOperacaoEstoque.Value = '') then
  begin
      ShowMessage('Informe a operac�o de estoque.') ;
      DBGridEh1.Col := 2 ;
      abort ;
  end ;

  if (qryOperacaoLocalEstoquenCdEmpresa.Value = 0) or (qryOperacaoLocalEstoquecNmEmpresa.Value = '') then
  begin
      ShowMessage('Informe a Empresa.') ;
      DBGridEh1.Col := 4 ;
      abort ;
  end ;

  if (not DBGridEh1.Columns[5].ReadOnly) then
  begin

    if (qryOperacaoLocalEstoquenCdLoja.Value = 0) or (qryOperacaoLocalEstoquecNmLoja.Value = '') then
    begin
      ShowMessage('Informe a Loja.') ;
      DBGridEh1.Col := 6 ;
      abort ;
    end ;

  end ;

  if (qryOperacaoLocalEstoquenCdGrupoProduto.Value = 0) or (qryOperacaoLocalEstoquecNmGrupoProduto.Value = '') then
  begin
      ShowMessage('Informe o Grupo de Produtos.');
      DBGridEh1.Col := 8 ;
      abort ;
  end ;

  if (qryOperacaoLocalEstoquenCdLocalEstoque.Value = 0) or (qryOperacaoLocalEstoquecNmLocalEstoque.Value = '') then
  begin
      ShowMessage('Informe o Local de Estoque.') ;
      DBGridEh1.Col := 10 ;
      abort ;
  end ;

  if ((not DBGridEh1.Columns[5].ReadOnly) and (qryLojanCdLoja.Value <> qryLocalEstoquenCdLoja.Value)) then
  begin
      if (MessageDlg('O local de estoque selecionado n�o pertence a loja informada. Tem certeza que deseja continuar ?',mtConfirmation,[mbYes,mbNo],0) = MRNo) then
      begin
          DBGridEh1.SelectedField := qryOperacaoLocalEstoquenCdLocalEstoque;
          DBGridEh1.SetFocus;
          abort ;
      end ;
  end;

  if (qryOperacaoLocalEstoque.State = dsInsert) then
      qryOperacaoLocalEstoquenCdOperacaoLocalEstoque.Value := frmMenu.fnProximoCodigo('OPERACAOLOCALESTOQUE') ;

  inherited;
end;

initialization
    RegisterClass(TfrmOperacaoLocalEstoque) ;
    
end.
