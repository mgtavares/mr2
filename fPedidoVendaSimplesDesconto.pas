unit fPedidoVendaSimplesDesconto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, cxControls, cxContainer, cxEdit, cxTextEdit,
  cxCurrencyEdit;

type
  TfrmPedidoVendaSimplesDesconto = class(TForm)
    edtValDesconto: TcxCurrencyEdit;
    Label3: TLabel;
    Label2: TLabel;
    edtPercent: TcxCurrencyEdit;
    edtValVenda: TcxCurrencyEdit;
    Label1: TLabel;
    edtValliquido: TcxCurrencyEdit;
    Label4: TLabel;
    edtValAcrescimo: TcxCurrencyEdit;
    Label5: TLabel;
    procedure edtPercentExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtPercentKeyPress(Sender: TObject; var Key: Char);
    procedure edtValDescontoKeyPress(Sender: TObject; var Key: Char);
    procedure edtValliquidoKeyPress(Sender: TObject; var Key: Char);
    procedure ValidaDesconto;
    procedure edtValAcrescimoKeyPress(Sender: TObject; var Key: Char);
    procedure edtValAcrescimoExit(Sender: TObject);
    function ArredondaMatematico(Value: extended; Decimals: integer): extended;
  private
    { Private declarations }
  public
    { Public declarations }
    nPercMaxDesc : double ;
  end;

var
  frmPedidoVendaSimplesDesconto: TfrmPedidoVendaSimplesDesconto;

implementation

uses fPdv, fMenu, Math;

{$R *.dfm}

procedure TfrmPedidoVendaSimplesDesconto.edtPercentExit(Sender: TObject);
begin

  edtValDesconto.Value := frmMenu.TBRound((edtvalvenda.value * (edtPercent.Value/100)),2) ;
  edtValLiquido.Value := edtValVenda.Value - edtValDesconto.Value + edtValAcrescimo.Value ;

end;

procedure TfrmPedidoVendaSimplesDesconto.FormShow(Sender: TObject);
begin

    edtPercent.Value       := 0 ;
    edtValDesconto.Value   := 0 ;
    edtValAcrescimo.Value  := 0 ;
    edtvalLiquido.Value    := edtValVenda.Value;

    edtPercent.SelectAll;
    edtPercent.SetFocus;

end;

procedure TfrmPedidoVendaSimplesDesconto.edtPercentKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        edtValDesconto.SetFocus ;

end;

procedure TfrmPedidoVendaSimplesDesconto.edtValDescontoKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
    begin
        if (edtValAcrescimo.Enabled = True) then edtValAcrescimo.SetFocus
        else edtValliquido.SetFocus;
    end;
end;

procedure TfrmPedidoVendaSimplesDesconto.edtValliquidoKeyPress(Sender: TObject;
  var Key: Char);
begin
    if (Key = #13) then
        ValidaDesconto;

end;

procedure TfrmPedidoVendaSimplesDesconto.ValidaDesconto;
begin
                   
  if (frmMenu.LeParametro('VAREJO') = 'S') then
      edtValLiquido.Value   := frmMenu.arredondaVarejo(edtValLiquido.Value)
  else edtValliquido.Value  := ArredondaMatematico(edtValliquido.Value,2);

  edtValDesconto.Value  := 0 ;
  edtValAcrescimo.Value := 0 ;

  if (edtValLiquido.Value < edtValVenda.Value) then
      edtValDesconto.Value := edtValVenda.Value - edtValLiquido.Value ;

  if (edtValLiquido.Value > edtValVenda.Value) then
      edtValAcrescimo.Value := edtValLiquido.Value - edtValVenda.Value ;

  if (frmPDV.MessageDlg('Confirma o valor do desconto/acrescimo ?',mtConfirmation,[mbYes,mbNo],0) = MrYes) then
  begin

      close ;

  end
  else
  begin
      if (edtPercent.Enabled) then
          edtPercent.SetFocus
      else edtValLiquido.SetFocus;

  end ;

end;

procedure TfrmPedidoVendaSimplesDesconto.edtValAcrescimoKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (Key = #13) then
        edtValLiquido.SetFocus;

end;

procedure TfrmPedidoVendaSimplesDesconto.edtValAcrescimoExit(Sender: TObject);
begin

  edtValLiquido.Value := edtValVenda.Value - edtValDesconto.Value + edtValAcrescimo.Value ;

end;

function TfrmPedidoVendaSimplesDesconto.ArredondaMatematico(Value: extended;
  Decimals: integer): extended;
var
    Factor   : extended;
    Fraction : extended;
    i        : integer;
begin
    Factor   := IntPower(10,Decimals);
    Value    := StrToFloat(FloatToStr(Value * Factor));
    Result   := Int(Value);
    Fraction := Frac(Value);

    for i := 3 downto 1 do
    Begin
        Fraction := Fraction * intPower(10,i);

        if (frac(Fraction) > 0.5) then
            Fraction :=Fraction + 1 - Frac(Fraction)
        else if (Frac(Fraction) < 0.5) then
            Fraction := Fraction - Frac(Fraction);

        Fraction := Fraction / intPower(10,i);
    end;

    if ((((Result/2) - Int(Result/2) > 0) OR ( Fraction > 0.5))) then
    BEGIN
        IF (Fraction >= 0.5) then
            Result := Result +1
        Else
            if (Fraction <= -0.5) then
                Result :=Result - 1;
    END;

    Result := Result/Factor;

end;

end.
