inherited frmNotaFiscalRecusada_Digitacao: TfrmNotaFiscalRecusada_Digitacao
  Left = 37
  Top = 132
  Caption = 'Nota Fiscal Recusada'
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel [1]
    Left = 64
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 81
    Top = 62
    Width = 21
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 62
    Top = 86
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 8
    Top = 110
    Width = 94
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Recebimento'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 58
    Top = 134
    Width = 44
    Height = 13
    Alignment = taRightJustify
    Caption = 'N'#250'm. NF'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 184
    Top = 134
    Width = 42
    Height = 13
    Alignment = taRightJustify
    Caption = 'S'#233'rie NF'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 30
    Top = 158
    Width = 72
    Height = 13
    Alignment = taRightJustify
    Caption = 'Valor Total NF'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 62
    Top = 182
    Width = 40
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio'
    FocusControl = DBEdit8
  end
  object Label9: TLabel [9]
    Left = 53
    Top = 206
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'T'#237'tulos: 1)'
    FocusControl = DBEdit9
  end
  object Label10: TLabel [10]
    Left = 176
    Top = 206
    Width = 9
    Height = 13
    Alignment = taRightJustify
    Caption = '2)'
    FocusControl = DBEdit10
  end
  object Label11: TLabel [11]
    Left = 272
    Top = 206
    Width = 9
    Height = 13
    Alignment = taRightJustify
    Caption = '3)'
    FocusControl = DBEdit11
  end
  object Label12: TLabel [12]
    Left = 93
    Top = 230
    Width = 9
    Height = 13
    Alignment = taRightJustify
    Caption = '4)'
    FocusControl = DBEdit12
  end
  object Label13: TLabel [13]
    Left = 176
    Top = 230
    Width = 9
    Height = 13
    Alignment = taRightJustify
    Caption = '5)'
    FocusControl = DBEdit13
  end
  object Label14: TLabel [14]
    Left = 272
    Top = 230
    Width = 9
    Height = 13
    Alignment = taRightJustify
    Caption = '6)'
    FocusControl = DBEdit14
  end
  object Label15: TLabel [15]
    Left = 42
    Top = 248
    Width = 60
    Height = 13
    Alignment = taRightJustify
    Caption = 'Observa'#231#227'o'
    FocusControl = DBMemo1
  end
  object DBEdit1: TDBEdit [17]
    Tag = 1
    Left = 104
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdNFRecusada'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [18]
    Tag = 1
    Left = 104
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [19]
    Left = 104
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdTerceiro'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [20]
    Left = 104
    Top = 104
    Width = 89
    Height = 19
    DataField = 'dDtReceb'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [21]
    Left = 104
    Top = 128
    Width = 65
    Height = 19
    DataField = 'cNrNF'
    DataSource = dsMaster
    TabOrder = 5
  end
  object DBEdit6: TDBEdit [22]
    Left = 232
    Top = 128
    Width = 26
    Height = 19
    DataField = 'cSerieNF'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit7: TDBEdit [23]
    Left = 104
    Top = 152
    Width = 89
    Height = 19
    DataField = 'nValTotal'
    DataSource = dsMaster
    TabOrder = 7
  end
  object DBEdit8: TDBEdit [24]
    Tag = 1
    Left = 104
    Top = 176
    Width = 65
    Height = 19
    DataField = 'nCdUsuario'
    DataSource = dsMaster
    TabOrder = 8
  end
  object DBEdit9: TDBEdit [25]
    Left = 104
    Top = 200
    Width = 65
    Height = 19
    DataField = 'cNrTit1'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit10: TDBEdit [26]
    Left = 192
    Top = 200
    Width = 65
    Height = 19
    DataField = 'cNrTit2'
    DataSource = dsMaster
    TabOrder = 10
  end
  object DBEdit11: TDBEdit [27]
    Left = 288
    Top = 200
    Width = 65
    Height = 19
    DataField = 'cNrTit3'
    DataSource = dsMaster
    TabOrder = 11
  end
  object DBEdit12: TDBEdit [28]
    Left = 104
    Top = 224
    Width = 65
    Height = 19
    DataField = 'cNrTit4'
    DataSource = dsMaster
    TabOrder = 12
  end
  object DBEdit13: TDBEdit [29]
    Left = 192
    Top = 224
    Width = 65
    Height = 19
    DataField = 'cNrTit5'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit14: TDBEdit [30]
    Left = 288
    Top = 224
    Width = 65
    Height = 19
    DataField = 'cNrTit6'
    DataSource = dsMaster
    TabOrder = 14
  end
  object DBMemo1: TDBMemo [31]
    Left = 104
    Top = 248
    Width = 529
    Height = 209
    DataField = 'cOBS'
    DataSource = dsMaster
    TabOrder = 15
  end
  object DBEdit15: TDBEdit [32]
    Tag = 1
    Left = 176
    Top = 56
    Width = 457
    Height = 19
    DataField = 'cNmLoja'
    DataSource = DataSource1
    TabOrder = 16
  end
  object DBEdit16: TDBEdit [33]
    Tag = 1
    Left = 176
    Top = 80
    Width = 457
    Height = 19
    DataField = 'cNmTerceiro'
    DataSource = DataSource2
    TabOrder = 17
  end
  object DBEdit17: TDBEdit [34]
    Tag = 1
    Left = 176
    Top = 176
    Width = 457
    Height = 19
    DataField = 'cNmUsuario'
    DataSource = DataSource3
    TabOrder = 18
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM NFRECUSADA'
      'WHERE nCdNFRecusada = :nPK'
      'AND nCdEmpresa = :nCdEmpresa')
    object qryMasternCdNFRecusada: TIntegerField
      FieldName = 'nCdNFRecusada'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasternCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryMasterdDtReceb: TDateTimeField
      FieldName = 'dDtReceb'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMastercNrNF: TStringField
      FieldName = 'cNrNF'
      Size = 15
    end
    object qryMastercSerieNF: TStringField
      FieldName = 'cSerieNF'
      FixedChar = True
      Size = 2
    end
    object qryMasternValTotal: TBCDField
      FieldName = 'nValTotal'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasternCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryMastercNrTit1: TStringField
      FieldName = 'cNrTit1'
      Size = 17
    end
    object qryMastercNrTit2: TStringField
      FieldName = 'cNrTit2'
      Size = 17
    end
    object qryMastercNrTit3: TStringField
      FieldName = 'cNrTit3'
      Size = 17
    end
    object qryMastercNrTit4: TStringField
      FieldName = 'cNrTit4'
      Size = 17
    end
    object qryMastercNrTit5: TStringField
      FieldName = 'cNrTit5'
      Size = 17
    end
    object qryMastercNrTit6: TStringField
      FieldName = 'cNrTit6'
      Size = 17
    end
    object qryMastercOBS: TMemoField
      FieldName = 'cOBS'
      BlobType = ftMemo
    end
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK')
    Left = 272
    Top = 240
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryLoja
    Left = 592
    Top = 272
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdTerceiro, cNmTerceiro FROM Terceiro WHERE nCdTerceiro ' +
        '= :nPK')
    Left = 336
    Top = 272
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryTerceiro
    Left = 600
    Top = 280
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      
        'SELECT nCdUsuario, cNmUsuario FROM Usuario WHERE nCdUsuario = :n' +
        'PK')
    Left = 416
    Top = 376
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qryUsuario
    Left = 608
    Top = 288
  end
end
