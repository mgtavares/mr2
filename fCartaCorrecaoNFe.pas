unit fCartaCorrecaoNFe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, StdCtrls, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Menus, ADODB, Mask, ER2Lookup, DBCtrls;

type
  TfrmCartaCorrecaoNFe = class(TfrmProcesso_Padrao)
    qryTerceiro: TADOQuery;
    Popup: TPopupMenu;
    ExibirDocumentoFiscal1: TMenuItem;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    dsTerceiro: TDataSource;
    GroupBox2: TGroupBox;
    DBEdit1: TDBEdit;
    er2LkpTerceiro: TER2LookupMaskEdit;
    dDtInicial: TMaskEdit;
    dDtFinal: TMaskEdit;
    Label9: TLabel;
    Label10: TLabel;
    Label3: TLabel;
    DoctoFiscalInicial: TMaskEdit;
    DoctoFiscalFinal: TMaskEdit;
    Label2: TLabel;
    Label4: TLabel;
    qryDoctoFiscal: TADOQuery;
    dsDoctoFiscal: TDataSource;
    qryDoctoFiscalnCdDoctoFiscal: TIntegerField;
    qryDoctoFiscalnCdSerieFiscal: TIntegerField;
    qryDoctoFiscalcCFOP: TStringField;
    qryDoctoFiscaliNrDocto: TIntegerField;
    qryDoctoFiscaldDtEmissao: TDateTimeField;
    qryDoctoFiscalnCdTerceiro: TIntegerField;
    qryDoctoFiscalcNmTerceiro: TStringField;
    qryDoctoFiscalnValTotal: TBCDField;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1nCdDoctoFiscal: TcxGridDBColumn;
    cxGridDBTableView1dDtEmissao: TcxGridDBColumn;
    cxGridDBTableView1iNrDocto: TcxGridDBColumn;
    cxGridDBTableView1nCdSerieFiscal: TcxGridDBColumn;
    cxGridDBTableView1cCFOP: TcxGridDBColumn;
    cxGridDBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGridDBTableView1nValTotal: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    qryDoctoFiscaldDtImpressao: TDateTimeField;
    procedure ExibirDocumentoFiscal1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCartaCorrecaoNFe: TfrmCartaCorrecaoNFe;

implementation

uses
  fMenu, fDoctoFiscal, fCartaCorrecaoNFe_GeracaoCCe;

{$R *.dfm}

procedure TfrmCartaCorrecaoNFe.ExibirDocumentoFiscal1Click(
  Sender: TObject);
var
  objForm : TfrmDoctoFiscal;
begin
  inherited;

  if (not qryDoctoFiscal.IsEmpty) then
  begin
      objForm := TfrmDoctoFiscal.Create(nil);

      PosicionaQuery(objForm.qryMaster, qryDoctoFiscalnCdDoctoFiscal.AsString);

      showForm(objForm,True);
  end;
end;

procedure TfrmCartaCorrecaoNFe.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryDoctoFiscal.Close;
  qryDoctoFiscal.Parameters.ParamByName('nCdTerceiro').Value := frmMenu.ConvInteiro(Trim(er2LkpTerceiro.Text));
  qryDoctoFiscal.Parameters.ParamByName('dDtInicial').Value  := frmMenu.ConvData(Trim(dDtInicial.Text));
  qryDoctoFiscal.Parameters.ParamByName('dDtFinal').Value    := frmMenu.ConvData(Trim(dDtFinal.Text));
  qryDoctoFiscal.Parameters.ParamByName('iNrInicial').Value  := frmMenu.ConvInteiro(Trim(DoctoFiscalInicial .Text));
  qryDoctoFiscal.Parameters.ParamByName('iNrFinal').Value    := frmMenu.ConvInteiro(Trim(DoctoFiscalFinal.Text));
  qryDoctoFiscal.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
  qryDoctoFiscal.Open;
end;

procedure TfrmCartaCorrecaoNFe.cxGridDBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmCartaCorrecaoNFe_GeracaoCCe;
begin
  inherited;

  if (not qryDoctoFiscal.IsEmpty) then
  begin
      { -- valida prazo de corre��o -- }
      { -- removido conforme nota t�cnica 2011.004 -- }
      {if ((qryDoctoFiscaldDtImpressao.Value + 30) < Now) then
      begin
          MensagemAlerta('Carta de Corre��o n�o autorizada devido estar fora do prazo de 30 dias.');
          Exit;
      end;}

      objForm := TfrmCartaCorrecaoNFe_GeracaoCCe.Create(nil);

      { -- verifica sequ�ncia CC-e e carrega documento fiscal -- }
      PosicionaQuery(objForm.qryVerificaSeqCCe, qryDoctoFiscalnCdDoctoFiscal.AsString);
      PosicionaQuery(objForm.qryDoctoFiscal, qryDoctoFiscalnCdDoctoFiscal.AsString);

      if (objForm.qryVerificaSeqCCeiSeq.Value > 0) then
          objForm.mskSequencia.Text := objForm.qryVerificaSeqCCeiSeq.AsString;

      showForm(objForm, True);
  end;
end;

initialization
  RegisterClass(TfrmCartaCorrecaoNFe);

end.
