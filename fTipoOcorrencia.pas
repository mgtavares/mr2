unit fTipoOcorrencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, Mask, ER2Lookup, cxPC, cxControls,
  DBGridEhGrouping, ToolCtrlsEh, GridsEh, DBGridEh;

type
  TfrmTipoOcorrencia = class(TfrmCadastro_Padrao)
    er2LkpStatus: TER2LookupDBEdit;
    qryMasternCdTipoOcorrencia: TIntegerField;
    qryMastercNmTipoOcorrencia: TStringField;
    qryMasternCdStatus: TIntegerField;
    qryMasteriDiasAtendimento: TIntegerField;
    qryMastercFlgContaDiasUteis: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    DBCheckBox1: TDBCheckBox;
    cxPageControl1: TcxPageControl;
    tabUsuarioRegistro: TcxTabSheet;
    tabUsuarioAtendto: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    DBGridEh2: TDBGridEh;
    qryUsuarioRegTipoOcorrencia: TADOQuery;
    dsUsuarioRegTipoOcorrencia: TDataSource;
    dsUsuarioAtendTipoOcorrencia: TDataSource;
    qryUsuarioAtendTipoOcorrencia: TADOQuery;
    qryUsuario: TADOQuery;
    qryUsuarionCdUsuario: TIntegerField;
    qryUsuariocNmUsuario: TStringField;
    qryUsuarioAtendTipoOcorrencianCdUsuarioAtendTipoOcorrencia: TIntegerField;
    qryUsuarioAtendTipoOcorrencianCdTipoOcorrencia: TIntegerField;
    qryUsuarioAtendTipoOcorrencianCdUsuario: TIntegerField;
    qryUsuarioAtendTipoOcorrenciacNmUsuario: TStringField;
    qryUsuarioRegTipoOcorrencianCdUsuarioRegTipoOcorrencia: TIntegerField;
    qryUsuarioRegTipoOcorrencianCdTipoOcorrencia: TIntegerField;
    qryUsuarioRegTipoOcorrencianCdUsuario: TIntegerField;
    qryUsuarioRegTipoOcorrenciacNmUsuario: TStringField;
    qryMastercNmStatus: TStringField;
    DBEdit3: TDBEdit;
    qryAux: TADOQuery;
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryUsuarioRegTipoOcorrenciaCalcFields(DataSet: TDataSet);
    procedure qryUsuarioAtendTipoOcorrenciaCalcFields(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBGridEh2ColEnter(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryUsuarioRegTipoOcorrenciaBeforePost(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh2Enter(Sender: TObject);
    procedure qryUsuarioAtendTipoOcorrenciaBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure btIncluirClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoOcorrencia: TfrmTipoOcorrencia;

implementation

uses fLookup_Padrao,fMenu;

{$R *.dfm}

procedure TfrmTipoOcorrencia.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPOOCORRENCIA' ;
  nCdTabelaSistema  := 525 ;
  nCdConsultaPadrao := 777 ;

  cxPageControl1.ActivePage := tabUsuarioRegistro;
end;

procedure TfrmTipoOcorrencia.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioRegTipoOcorrencia.State = dsBrowse) then
             qryUsuarioRegTipoOcorrencia.Edit ;

        if (qryUsuarioRegTipoOcorrencia.State = dsInsert) or (qryUsuarioRegTipoOcorrencia.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioRegTipoOcorrencianCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoOcorrencia.DBGridEh2KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  nPK : integer;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryUsuarioAtendTipoOcorrencia.State = dsBrowse) then
             qryUsuarioAtendTipoOcorrencia.Edit ;

        if (qryUsuarioAtendTipoOcorrencia.State = dsInsert) or (qryUsuarioAtendTipoOcorrencia.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(5);

            If (nPK > 0) then
            begin
                qryUsuarioAtendTipoOcorrencianCdUsuario.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoOcorrencia.qryUsuarioRegTipoOcorrenciaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryUsuario.Close;
  PosicionaQuery(qryUsuario, qryUsuarioRegTipoOcorrencianCdUsuario.AsString) ;

  if not qryUsuario.eof then
      qryUsuarioRegTipoOcorrenciacNmUsuario.Value := qryUsuariocNmUsuario.Value ;
end;

procedure TfrmTipoOcorrencia.qryUsuarioAtendTipoOcorrenciaCalcFields(
  DataSet: TDataSet);
begin
  inherited;
  qryUsuario.Close;
  PosicionaQuery(qryUsuario, qryUsuarioAtendTipoOcorrencianCdUsuario.AsString) ;

  if not qryUsuario.eof then
      qryUsuarioAtendTipoOcorrenciacNmUsuario.Value := qryUsuariocNmUsuario.Value ;
end;

procedure TfrmTipoOcorrencia.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryUsuarioRegTipoOcorrencia.Close;
  PosicionaQuery(qryUsuarioRegTipoOcorrencia,qryMasternCdTipoOcorrencia.AsString);

  qryUsuarioAtendTipoOcorrencia.Close;
  PosicionaQuery(qryUsuarioAtendTipoOcorrencia,qryMasternCdTipoOcorrencia.AsString);

end;

procedure TfrmTipoOcorrencia.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (Trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o do tipo de ocorr�ncia.');
      DBEdit2.SetFocus;
      Abort;
  end;

  if (DBEdit3.Text = '') then
  begin
      MensagemAlerta('Informe o status do tipo de ocorr�ncia.');
      er2LkpStatus.SetFocus;
      Abort;
  end;

  if (qryMasteriDiasAtendimento.Value <= 0) then
  begin
      MensagemAlerta('Quantidade de dias para atendimento deve ser maior que zero.');
      DBEdit4.SetFocus;
      Abort;
  end;

  inherited;
end;

procedure TfrmTipoOcorrencia.DBGridEh2ColEnter(Sender: TObject);
begin
  inherited;
  
  if (qryMaster.State = dsInsert) then
      qryMaster.Post;
end;



procedure TfrmTipoOcorrencia.qryUsuarioRegTipoOcorrenciaBeforePost(
  DataSet: TDataSet);
begin
  if (qryUsuarioRegTipoOcorrenciacNmUsuario.Value = '') then
  begin
      MensagemAlerta('Usu�rio Invalido. ');
      Abort;
  end;

  qryAux.Close;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM UsuarioRegTipoOcorrencia WHERE nCdUsuario = ' + qryUsuarioRegTipoOcorrencianCdUsuario.asString + ' AND nCdTipoOcorrencia = ' + qryMasternCdTipoOcorrencia.AsString) ;
  qryAux.Open ;

  if (not qryAux.IsEmpty) then
  begin
      MensagemAlerta('Usu�rio ' + qryUsuariocNmUsuario.Value + ' j� vinculado a este Tipo de Ocorr�ncia.');
      Abort;
  end;

  inherited;

  qryUsuarioRegTipoOcorrencianCdTipoOcorrencia.Value           := qryMasternCdTipoOcorrencia.Value ;
  qryUsuarioRegTipoOcorrencianCdUsuarioRegTipoOcorrencia.Value := frmMenu.fnProximoCodigo('USUARIOREGTIPOOCORRENCIA');

end;

procedure TfrmTipoOcorrencia.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  
  if not qryMaster.Active then
     exit ;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;

  PosicionaQuery(qryUsuarioRegTipoOcorrencia, qryMasternCdTipoOcorrencia.AsString);
end;

procedure TfrmTipoOcorrencia.DBGridEh2Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;

  PosicionaQuery(qryUsuarioAtendTipoOcorrencia, qryMasternCdTipoOcorrencia.AsString);
end;

procedure TfrmTipoOcorrencia.qryUsuarioAtendTipoOcorrenciaBeforePost(
  DataSet: TDataSet);
begin
  if (qryUsuarioAtendTipoOcorrenciacNmUsuario.Value = '') then
  begin
      MensagemAlerta('Usu�rio Invalido. ');
      Abort;
  end;

  qryAux.Close;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT 1 FROM UsuarioAtendTipoOcorrencia WHERE nCdUsuario = ' + qryUsuarioAtendTipoOcorrencianCdUsuario.asString + ' AND nCdTipoOcorrencia = ' + qryMasternCdTipoOcorrencia.AsString) ;
  qryAux.Open ;

  if (not qryAux.IsEmpty) then
  begin
      MensagemAlerta('Usu�rio ' + qryUsuariocNmUsuario.Value + ' j� vinculado a este Tipo de Ocorr�ncia.');
      Abort;
  end;

  inherited;

  qryUsuarioAtendTipoOcorrencianCdTipoOcorrencia.Value           := qryMasternCdTipoOcorrencia.Value ;
  qryUsuarioAtendTipoOcorrencianCdUsuarioAtendTipoOcorrencia.Value := frmMenu.fnProximoCodigo('USUARIOATENDTIPOOCORRENCIA');


end;

procedure TfrmTipoOcorrencia.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryUsuarioRegTipoOcorrencia.Close;
  qryUsuarioAtendTipoOcorrencia.Close;
end;

procedure TfrmTipoOcorrencia.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit2.SetFocus;
  qryMasteriDiasAtendimento.Value := 0;
end;

initialization
    RegisterClass(TfrmTipoOcorrencia);

end.
