inherited rptRelPedidoVenda: TrptRelPedidoVenda
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'Rel. Pedido de Venda'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1152
    Height = 785
  end
  object Label5: TLabel [1]
    Left = 51
    Top = 72
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
  end
  object Label3: TLabel [2]
    Left = 8
    Top = 216
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Previs'#227'o Entrega'
  end
  object Label6: TLabel [3]
    Left = 180
    Top = 216
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label2: TLabel [4]
    Left = 8
    Top = 48
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo Economico'
  end
  object Label7: TLabel [5]
    Left = 61
    Top = 120
    Width = 29
    Height = 13
    Alignment = taRightJustify
    Caption = 'Marca'
  end
  object Label4: TLabel [6]
    Left = 180
    Top = 240
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label8: TLabel [7]
    Left = 11
    Top = 240
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Intervalo Pedido'
  end
  object Label9: TLabel [8]
    Left = 35
    Top = 96
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Pedido'
  end
  object Label1: TLabel [9]
    Left = 5
    Top = 144
    Width = 85
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo de Produto'
  end
  object Label10: TLabel [10]
    Left = 70
    Top = 192
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label11: TLabel [11]
    Left = 52
    Top = 168
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
  end
  inherited ToolBar1: TToolBar
    Width = 1152
    TabOrder = 15
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit10: TDBEdit [13]
    Tag = 1
    Left = 168
    Top = 64
    Width = 425
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 16
  end
  object MaskEdit6: TMaskEdit [14]
    Left = 96
    Top = 208
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 7
    Text = '  /  /    '
  end
  object MaskEdit7: TMaskEdit [15]
    Left = 200
    Top = 208
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [16]
    Left = 96
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object DBEdit1: TDBEdit [17]
    Tag = 1
    Left = 168
    Top = 40
    Width = 425
    Height = 21
    DataField = 'cNmGrupoEconomico'
    DataSource = dsGrupoEconomico
    TabOrder = 17
  end
  object MaskEdit2: TMaskEdit [18]
    Left = 96
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = MaskEdit2Exit
    OnKeyDown = MaskEdit2KeyDown
  end
  object MaskEdit5: TMaskEdit [19]
    Left = 96
    Top = 112
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object DBEdit5: TDBEdit [20]
    Tag = 1
    Left = 168
    Top = 112
    Width = 425
    Height = 21
    DataField = 'cNmMarca'
    DataSource = dsMarca
    TabOrder = 18
  end
  object MaskEdit9: TMaskEdit [21]
    Left = 200
    Top = 232
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 10
    Text = '  /  /    '
  end
  object MaskEdit8: TMaskEdit [22]
    Left = 96
    Top = 232
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 9
    Text = '  /  /    '
  end
  object DBEdit4: TDBEdit [23]
    Tag = 1
    Left = 168
    Top = 88
    Width = 425
    Height = 21
    DataField = 'cNmTipoPedido'
    DataSource = dsTipoPedido
    TabOrder = 19
  end
  object MaskEdit4: TMaskEdit [24]
    Left = 96
    Top = 88
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object RadioGroup1: TRadioGroup [25]
    Left = 8
    Top = 264
    Width = 281
    Height = 49
    Caption = 'Exibir Itens do Pedido ?'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 11
  end
  object RadioGroup2: TRadioGroup [26]
    Left = 296
    Top = 264
    Width = 281
    Height = 49
    Caption = 'Situa'#231#227'o do Item'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'S'#243' em aberto'
      'S'#243' em atraso'
      'S'#243' Faturados'
      'S'#243' Previs'#245'es'
      'S'#243' Cancelados')
    TabOrder = 12
  end
  object RadioGroup3: TRadioGroup [27]
    Left = 8
    Top = 320
    Width = 281
    Height = 49
    Caption = 'Exibir Valores ?'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 13
  end
  object RadioGroup4: TRadioGroup [28]
    Left = 296
    Top = 320
    Width = 281
    Height = 49
    Caption = 'Somente Trocas ?'
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 14
  end
  object MaskEdit1: TMaskEdit [29]
    Left = 96
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = MaskEdit1Exit
    OnKeyDown = MaskEdit1KeyDown
  end
  object DBEdit2: TDBEdit [30]
    Tag = 1
    Left = 168
    Top = 136
    Width = 425
    Height = 21
    DataField = 'cNmGrupoProduto'
    DataSource = DataSource1
    TabOrder = 20
  end
  object MaskEdit10: TMaskEdit [31]
    Left = 96
    Top = 184
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = MaskEdit10Exit
    OnKeyDown = MaskEdit10KeyDown
  end
  object DBEdit3: TDBEdit [32]
    Tag = 1
    Left = 168
    Top = 184
    Width = 425
    Height = 21
    DataField = 'cNmLoja'
    DataSource = DataSource2
    TabOrder = 21
  end
  object RadioGroup5: TRadioGroup [33]
    Left = 8
    Top = 376
    Width = 281
    Height = 49
    Caption = 'Exibir Bonifica'#231#245'es ?'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 22
  end
  object RadioGroup6: TRadioGroup [34]
    Left = 296
    Top = 376
    Width = 281
    Height = 49
    Caption = 'Somente Bonifica'#231#245'es ?'
    Columns = 2
    ItemIndex = 1
    Items.Strings = (
      'Sim'
      'N'#227'o')
    TabOrder = 23
  end
  object edtCdProduto: TER2LookupMaskEdit [35]
    Left = 96
    Top = 160
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 5
    Text = '         '
    CodigoLookup = 76
    QueryLookup = qryProduto
  end
  object DBEdit6: TDBEdit [36]
    Tag = 1
    Left = 168
    Top = 160
    Width = 425
    Height = 21
    DataField = 'cNmProduto'
    DataSource = DataSource3
    TabOrder = 24
  end
  object RadioGroup7: TRadioGroup [37]
    Left = 8
    Top = 432
    Width = 569
    Height = 81
    Caption = 'Situa'#231#227'o do Pedido'
    Columns = 2
    ItemIndex = 0
    Items.Strings = (
      'Todos'
      'Digitado'
      'An'#225'lise Financeiro'
      'Autorizado'
      'An'#225'lise Financeiro + Autorizado'
      'Faturado Parcial'
      'Autorizado e Faturado Parcialmente'
      'Faturado Total')
    TabOrder = 25
  end
  inherited ImageList1: TImageList
    Left = 328
    Top = 504
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nPK')
    Left = 488
    Top = 224
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nPK')
    Left = 296
    Top = 224
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 520
    Top = 224
  end
  object dsTipoPedido: TDataSource
    DataSet = qryTipoPedido
    Left = 456
    Top = 224
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 328
    Top = 224
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoEconomico'
      'WHERE nCdGrupoEconomico = :nPK')
    Left = 360
    Top = 224
    object qryGrupoEconomiconCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object dsGrupoEconomico: TDataSource
    DataSet = qryGrupoEconomico
    Left = 392
    Top = 224
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarca, cNmMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 552
    Top = 224
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 584
    Top = 224
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdTipoPedido, cNmTipoPedido'
      '  FROM TipoPedido'
      ' WHERE nCdTipoPedido = :nPK'
      'AND cFlgCompra = 0'
      'AND cFlgDevolucVenda = 0 AND cFlgDevolucCompra = 0')
    Left = 424
    Top = 224
    object qryTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM GrupoProduto'
      ' WHERE nCdGrupoProduto = :nPK')
    Left = 624
    Top = 224
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
    object qryGrupoProdutocFlgExpPDV: TIntegerField
      FieldName = 'cFlgExpPDV'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryGrupoProduto
    Left = 664
    Top = 200
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK')
    Left = 720
    Top = 200
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryLoja
    Left = 624
    Top = 296
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdProduto, cNmProduto'
      'FROM Produto'
      'WHERE nCdProduto = :nPK')
    Left = 728
    Top = 264
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object DataSource3: TDataSource
    DataSet = qryProduto
    Left = 496
    Top = 272
  end
end
