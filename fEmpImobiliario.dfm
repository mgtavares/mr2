inherited frmEmpImobiliario: TfrmEmpImobiliario
  Left = 119
  Top = 133
  Height = 800
  Caption = 'Empreendimento Imobili'#225'rio'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Height = 739
  end
  object Label1: TLabel [1]
    Left = 90
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 85
    Top = 70
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 51
    Top = 94
    Width = 77
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Pedido'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 7
    Top = 118
    Width = 121
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome Empreendimento'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 39
    Top = 166
    Width = 89
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Lan'#231'amento'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 80
    Top = 144
    Width = 48
    Height = 13
    Alignment = taRightJustify
    Caption = 'Endere'#231'o'
    FocusControl = DBEdit8
  end
  object DBEdit1: TDBEdit [8]
    Tag = 1
    Left = 132
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdEmpImobiliario'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [9]
    Tag = 1
    Left = 132
    Top = 64
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [10]
    Left = 132
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdTipoPedido'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [11]
    Left = 132
    Top = 112
    Width = 718
    Height = 19
    DataField = 'cNmEmpImobiliario'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit5: TDBEdit [12]
    Left = 132
    Top = 160
    Width = 73
    Height = 19
    DataField = 'dDtLancamento'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit6: TDBEdit [13]
    Tag = 1
    Left = 200
    Top = 64
    Width = 650
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = DataSource1
    TabOrder = 7
  end
  object DBEdit7: TDBEdit [14]
    Tag = 1
    Left = 200
    Top = 88
    Width = 650
    Height = 19
    DataField = 'cNmTipoPedido'
    DataSource = DataSource2
    TabOrder = 8
  end
  object cxPageControl1: TcxPageControl [15]
    Left = 8
    Top = 200
    Width = 945
    Height = 289
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 9
    ClientRectBottom = 285
    ClientRectLeft = 4
    ClientRectRight = 941
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Blocos / Torres'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 937
        Height = 261
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsBlocos
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdBlocoEmpImobiliario'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmBlocoEmpImobiliario'
            Footers = <>
            Width = 233
          end
          item
            EditButtons = <>
            FieldName = 'nCdCC'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmCC'
            Footers = <>
            Width = 227
          end
          item
            EditButtons = <>
            FieldName = 'dDtHabitese'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'dDtChaves'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'iQtdeUnidade'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit8: TDBEdit [16]
    Left = 132
    Top = 136
    Width = 717
    Height = 19
    DataField = 'cEndereco'
    DataSource = dsMaster
    TabOrder = 5
  end
  inherited qryMaster: TADOQuery
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * FROM EmpImobiliario'
      'WHERE nCdEmpImobiliario = :nPK'
      'AND nCdEmpresa = :nCdEmpresa')
    object qryMasternCdEmpImobiliario: TIntegerField
      FieldName = 'nCdEmpImobiliario'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryMastercNmEmpImobiliario: TStringField
      FieldName = 'cNmEmpImobiliario'
      Size = 100
    end
    object qryMasterdDtLancamento: TDateTimeField
      FieldName = 'dDtLancamento'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMastercEndereco: TStringField
      FieldName = 'cEndereco'
      Size = 100
    end
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 384
    Top = 240
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryEmpresa
    Left = 592
    Top = 272
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoPedido, cNmTipoPedido'
      'FROM TipoPedido'
      'WHERE nCdTipoPedido = :nPK'
      'AND cFlgVenda = 1')
    Left = 472
    Top = 312
    object qryTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryTipoPedido
    Left = 600
    Top = 280
  end
  object qryBlocos: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryBlocosBeforePost
    OnCalcFields = qryBlocosCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM BlocoEmpImobiliario'
      'WHERE nCdEmpImobiliario = :nPK')
    Left = 872
    Top = 248
    object qryBlocosnCdBlocoEmpImobiliario: TAutoIncField
      DisplayLabel = 'C'#243'd'
      FieldName = 'nCdBlocoEmpImobiliario'
      ReadOnly = True
    end
    object qryBlocosnCdEmpImobiliario: TIntegerField
      FieldName = 'nCdEmpImobiliario'
    end
    object qryBlocoscNmBlocoEmpImobiliario: TStringField
      DisplayLabel = 'Nome Bloco/Torre'
      FieldName = 'cNmBlocoEmpImobiliario'
      Size = 50
    end
    object qryBlocosnCdCC: TIntegerField
      DisplayLabel = 'Centro de Custo|C'#243'd'
      FieldName = 'nCdCC'
    end
    object qryBlocoscNmCC: TStringField
      DisplayLabel = 'Centro de Custo|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmCC'
      Size = 50
      Calculated = True
    end
    object qryBlocosdDtHabitese: TDateTimeField
      DisplayLabel = 'Data Habite-se'
      FieldName = 'dDtHabitese'
      EditMask = '!99/99/9999;1;_'
    end
    object qryBlocosdDtChaves: TDateTimeField
      DisplayLabel = 'Data Chaves'
      FieldName = 'dDtChaves'
      EditMask = '!99/99/9999;1;_'
    end
    object qryBlocosiQtdeUnidade: TIntegerField
      DisplayLabel = 'Qtde de Unidades'
      FieldName = 'iQtdeUnidade'
    end
  end
  object dsBlocos: TDataSource
    DataSet = qryBlocos
    Left = 904
    Top = 248
  end
  object qryCC: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTipoPedido'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdCC, cNmCC'
      'FROM CentroCusto'
      'WHERE nCdCC = :nPK'
      'AND iNivel = 3'
      'AND cFlgLanc = 1'
      
        'AND EXISTS(SELECT 1 FROM CentroCustoTipoPedido CCTP WHERE CCTP.n' +
        'CdTipopedido = :nCdTipoPedido AND CCTP.nCdCC = CentroCusto.nCdCC' +
        ')')
    Left = 876
    Top = 368
    object qryCCnCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCCcNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
end
