inherited frmCondPagto: TfrmCondPagto
  Left = 212
  Top = 109
  Width = 898
  Height = 467
  Caption = 'Condi'#231#245'es de Pagamento'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 882
    Height = 404
  end
  object Label1: TLabel [1]
    Left = 63
    Top = 46
    Width = 38
    Height = 13
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 52
    Top = 70
    Width = 49
    Height = 13
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit2
  end
  object Label5: TLabel [3]
    Left = 8
    Top = 94
    Width = 93
    Height = 13
    Caption = 'Forma Pagamento'
    FocusControl = DBEdit6
  end
  inherited ToolBar2: TToolBar
    Width = 882
  end
  object DBEdit1: TDBEdit [5]
    Tag = 1
    Left = 104
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdCondPagto'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [6]
    Left = 104
    Top = 64
    Width = 649
    Height = 19
    DataField = 'cNmCondPagto'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBGridEh1: TDBGridEh [7]
    Left = 104
    Top = 128
    Width = 209
    Height = 281
    DataGrouping.GroupLevels = <>
    DataSource = DataSource1
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Segoe UI'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Segoe UI'
    TitleFont.Style = []
    UseMultiTitle = True
    OnEnter = DBGridEh1Enter
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdCondPagto'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'iDiasPrazo'
        Footers = <>
        Width = 67
      end
      item
        EditButtons = <>
        FieldName = 'nPercent'
        Footers = <>
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'dDtSugerida'
        Footers = <>
        Visible = False
        Width = 83
      end
      item
        EditButtons = <>
        FieldName = 'dDtMaxima'
        Footers = <>
        Visible = False
        Width = 77
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object DBEdit6: TDBEdit [8]
    Left = 104
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdFormaPagto'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit6Exit
    OnKeyDown = DBEdit6KeyDown
  end
  object DBEdit7: TDBEdit [9]
    Tag = 1
    Left = 172
    Top = 88
    Width = 581
    Height = 19
    DataField = 'cNmFormaPagto'
    DataSource = DataSource2
    TabOrder = 4
  end
  inherited qryMaster: TADOQuery
    LockType = ltBatchOptimistic
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM CondPagto'
      'WHERE nCdCondPagto = :nPK'
      'AND cFlgPdv = 0')
    Left = 520
    object qryMasternCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryMastercNmCondPagto: TStringField
      FieldName = 'cNmCondPagto'
      Size = 50
    end
    object qryMasternCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryMasterdDtValidadeIni: TDateTimeField
      FieldName = 'dDtValidadeIni'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMasterdDtValidadeFim: TDateTimeField
      FieldName = 'dDtValidadeFim'
      EditMask = '!99/99/9999;1;_'
    end
    object qryMastercFlgAtivo: TIntegerField
      FieldName = 'cFlgAtivo'
    end
    object qryMastercFlgPermDesconto: TIntegerField
      FieldName = 'cFlgPermDesconto'
    end
    object qryMasternFatorArred: TBCDField
      FieldName = 'nFatorArred'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMasteriParcArred: TIntegerField
      FieldName = 'iParcArred'
    end
    object qryMastercFlgPrimParcEnt: TIntegerField
      FieldName = 'cFlgPrimParcEnt'
    end
    object qryMastercFlgPDV: TIntegerField
      FieldName = 'cFlgPDV'
    end
    object qryMastercFlgLiqCrediario: TIntegerField
      FieldName = 'cFlgLiqCrediario'
    end
  end
  inherited dsMaster: TDataSource
    Left = 520
  end
  inherited qryID: TADOQuery
    Left = 648
    Top = 160
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 712
  end
  inherited qryStat: TADOQuery
    Left = 648
    Top = 128
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 712
  end
  inherited ImageList1: TImageList
    Left = 744
  end
  object qryParc: TADOQuery
    Connection = frmMenu.Connection
    LockType = ltBatchOptimistic
    BeforePost = qryParcBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ParcCondPagto'
      'WHERE nCdCondPagto = :nPK'
      'ORDER BY iDiasPrazo')
    Left = 584
    Top = 128
    object qryParcnCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryParciDiasPrazo: TIntegerField
      DisplayLabel = 'Informa'#231#245'es das Parcelas|Dias'
      FieldName = 'iDiasPrazo'
    end
    object qryParcnPercent: TBCDField
      DisplayLabel = 'Informa'#231#245'es das Parcelas|Percentual'
      FieldName = 'nPercent'
      Precision = 12
      Size = 2
    end
    object qryParcdDtSugerida: TDateTimeField
      DisplayLabel = 'Informa'#231#245'es das Parcelas|Data Sugerida'
      FieldName = 'dDtSugerida'
      EditMask = '!99/99/9999;1;_'
    end
    object qryParcdDtMaxima: TDateTimeField
      DisplayLabel = 'Informa'#231#245'es das Parcelas|Data M'#225'xima'
      FieldName = 'dDtMaxima'
      EditMask = '!99/99/9999;1;_'
    end
  end
  object DataSource1: TDataSource
    DataSet = qryParc
    Left = 584
    Top = 160
  end
  object qryFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdFormaPagto, cNmFormaPagto'
      'FROM FormaPagto'
      'WHERE nCdFormaPagto = :nPK')
    Left = 552
    Top = 128
    object qryFormaPagtonCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
    object qryFormaPagtocNmFormaPagto: TStringField
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryFormaPagto
    Left = 552
    Top = 160
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 680
    Top = 128
  end
  object qryLojaCondPagto: TADOQuery
    Connection = frmMenu.Connection
    LockType = ltBatchOptimistic
    BeforePost = qryLojaCondPagtoBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM LojaCondPagto'
      'WHERE nCdCondPagto = :nPK')
    Left = 616
    Top = 128
    object qryLojaCondPagtonCdCondPagto: TIntegerField
      FieldName = 'nCdCondPagto'
    end
    object qryLojaCondPagtonCdLoja: TIntegerField
      DisplayLabel = 'Lojas Autorizadas|C'#243'd'
      FieldName = 'nCdLoja'
    end
    object qryLojaCondPagtocNmLoja: TStringField
      DisplayLabel = 'Lojas Autorizadas|Nome'
      FieldKind = fkLookup
      FieldName = 'cNmLoja'
      LookupDataSet = qryLoja
      LookupKeyFields = 'nCdLoja'
      LookupResultField = 'cNmLoja'
      KeyFields = 'nCdLoja'
      LookupCache = True
      Size = 50
      Lookup = True
    end
  end
  object dsLojaCondPagto: TDataSource
    DataSet = qryLojaCondPagto
    Left = 616
    Top = 160
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja')
    Left = 680
    Top = 160
    object qryLojanCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
end
