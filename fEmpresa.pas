unit fEmpresa;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ADODB, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DBCtrls, ImgList, cxLookAndFeelPainters, cxButtons,
  ExtDlgs, cxImage;

type
  TfrmEmpresa = class(TfrmCadastro_Padrao)
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdTerceiroEmp: TIntegerField;
    qryMasternCdTerceiroMatriz: TIntegerField;
    qryMastercSigla: TStringField;
    qryMasternCdStatus: TIntegerField;
    qryMastercNmEmpresa: TStringField;
    qryTerceiro: TADOQuery;
    qryTerceiroMatriz: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmFantasia: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceiroMatriznCdTerceiro: TIntegerField;
    qryTerceiroMatrizcNmFantasia: TStringField;
    qryTerceiroMatrizcNmTerceiro: TStringField;
    qryMastercNmTerceiroEmp: TStringField;
    qryMastercNmTerceiroMatriz: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    qryMastercNmStatus: TStringField;
    qryMasterimgLogoEmpresaGrande: TBlobField;
    qryMasterimgLogoEmpresaRelat: TBlobField;
    Label7: TLabel;
    DBImage1: TDBImage;
    Label8: TLabel;
    DBImage2: TDBImage;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    OpenPictureDialog1: TOpenPictureDialog;
    procedure FormCreate(Sender: TObject);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btIncluirClick(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmEmpresa: TfrmEmpresa;

implementation

uses fLookup_Padrao;

{$R *.dfm}

procedure TfrmEmpresa.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'EMPRESA' ;
  nCdTabelaSistema  := 16 ;
  nCdConsultaPadrao := 25 ;

end;

procedure TfrmEmpresa.DBEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroEmp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmEmpresa.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroMatriz.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmEmpresa.DBEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(2);

            If (nPK > 0) then
            begin
                qryMasternCdStatus.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmEmpresa.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus;
end;

procedure TfrmEmpresa.cxButton1Click(Sender: TObject);
var
    figura  : TPicture;
    Stream2 : TMemoryStream;
begin

  OpenPictureDialog1.InitialDir :='';
  OpenPictureDialog1.Filename   :='';

  figura := TPicture.Create;
  if OpenPictureDialog1.execute then
  begin
      DBImage1.Picture.Graphic.LoadFromFile(OpenPictureDialog1.FileName);

  end ;

end;

procedure TfrmEmpresa.cxButton2Click(Sender: TObject);
begin
  inherited;

  OpenPictureDialog1.InitialDir :='';
  OpenPictureDialog1.Filename   :='';

  if OpenPictureDialog1.execute then
    DBImage2.Picture.LoadFromFile(OpenPictureDialog1.Filename);

end;

procedure TfrmEmpresa.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  DBImage1.LoadPicture;
  DBImage2.LoadPicture;
  
end;

initialization
    RegisterClass(tFrmEmpresa) ;

end.
