unit fClienteVarejoPessoaFisica_HistoricoNeg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmClienteVarejoPessoaFisica_HistoricoNeg = class(TfrmProcesso_Padrao)
    Image2: TImage;
    DBGridEh1: TDBGridEh;
    qryTitulos: TADOQuery;
    qryTitulosnCdTitulo: TIntegerField;
    qryTituloscNrTit: TStringField;
    qryTitulosiParcela: TIntegerField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTitulosnValTit: TBCDField;
    qryTitulosnSaldoTit: TBCDField;
    qryTitulosdDtLiq: TDateTimeField;
    qryTituloscNmEspTit: TStringField;
    qryTitulosnCdLojaTit: TStringField;
    dsTitulos: TDataSource;
    qryTitulosdDtNegativacao: TDateTimeField;
    qryTitulosdDtReabNeg: TDateTimeField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmClienteVarejoPessoaFisica_HistoricoNeg: TfrmClienteVarejoPessoaFisica_HistoricoNeg;
  
implementation


{$R *.dfm}

end.
