unit rFluxoCaixaRealizadoAnalitico_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptFluxoCaixaRealizadoAnalitico_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel9: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRExpr2: TQRExpr;
    QRBand5: TQRBand;
    lblFiltro: TQRLabel;
    QRShape2: TQRShape;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRLabel12: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRShape5: TQRShape;
    QRDBText1: TQRDBText;
    QRDBText3: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText6: TQRDBText;
    QRLabel8: TQRLabel;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
    qryResultado: TADOQuery;
    qryResultadonCdPlanoConta: TIntegerField;
    qryResultadocNmPlanoConta: TStringField;
    qryResultadodDtLancto: TDateTimeField;
    qryResultadonCdLanctoFin: TIntegerField;
    qryResultadocNrDocto: TStringField;
    qryResultadocNrNF: TStringField;
    qryResultadonCdSP: TIntegerField;
    qryResultadocNmTerceiro: TStringField;
    qryResultadonValLancto: TBCDField;
    qryResultadocFlgManual: TStringField;
    qryResultadocHistorico: TStringField;
    qryResultadocNmUsuarioLiquidacao: TStringField;
    qryResultadoiNrCheque: TIntegerField;
    qryResultadonCdContaLiquidacao: TStringField;
    QRLabel14: TQRLabel;
    QRLabel6: TQRLabel;
    QRDBText8: TQRDBText;
    qryResultadonCdLojaPG: TStringField;
    qryResultadonCdLojaEmi: TStringField;
    QRLabel10: TQRLabel;
    QRLabel16: TQRLabel;
    QRDBText11: TQRDBText;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFluxoCaixaRealizadoAnalitico_view: TrptFluxoCaixaRealizadoAnalitico_view;

implementation

{$R *.dfm}

end.
