unit fConsultaCheque;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, GridsEh, DBGridEh, StdCtrls, ExtCtrls,
  ImgList, ComCtrls, ToolWin, Mask, DB, DBCtrls, ADODB, DBGridEhGrouping,
  cxPC, cxControls;

type
  TfrmConsultaCheque = class(TfrmProcesso_Padrao)
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    DataSource1: TDataSource;
    dsResultado: TDataSource;
    usp_Resultado: TADOStoredProc;
    usp_ResultadocConta: TStringField;
    usp_ResultadoiNrCheque: TIntegerField;
    usp_ResultadocCNPJCPF: TStringField;
    usp_ResultadonValCheque: TBCDField;
    usp_ResultadodDtDeposito: TDateTimeField;
    usp_ResultadocNmTerceiro: TStringField;
    usp_ResultadocNmTerceiroPort: TStringField;
    usp_ResultadodDtRemessaPort: TDateTimeField;
    usp_ResultadodDtDevol: TDateTimeField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    RadioGroup1: TRadioGroup;
    edtTerceiro: TMaskEdit;
    edtCNPJEmissor: TMaskEdit;
    edtDtDeposito: TMaskEdit;
    edtTipoSimulacao: TMaskEdit;
    edtValorPagamento: TMaskEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    edtNumCheque: TMaskEdit;
    procedure edtValorPagamentoExit(Sender: TObject);
    procedure edtTerceiroExit(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtTerceiroKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaCheque: TfrmConsultaCheque;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmConsultaCheque.edtValorPagamentoExit(Sender: TObject);
begin
  inherited;
  If (trim(edtValorPagamento.Text) <> '') then
      try
          edtValorPagamento.text := formatcurr('0.00',StrToFloat(StringReplace(edtValorPagamento.text,'.',',',[rfReplaceAll,rfIgnoreCase]))) ;
      except
          raise ;
          exit ;
      end ;

end;

procedure TfrmConsultaCheque.edtTerceiroExit(Sender: TObject);
begin
  inherited;

  qryTerceiro.Close ;
  
  if (Trim(edtTerceiro.Text) <> '') then
  begin
      qryTerceiro.Close ;
      qryTerceiro.Parameters.ParamByName('nPK').Value := StrToInt(Trim(edtTerceiro.Text)) ;
      qryTerceiro.Open ;
  end ;

end;

procedure TfrmConsultaCheque.RadioGroup1Click(Sender: TObject);
begin
  inherited;
  usp_Resultado.Close ;

  edtTerceiro.Enabled       := False ;
  edtCNPJEmissor.Enabled    := False ;
  edtDtDeposito.Enabled     := False ;
  edtTipoSimulacao.Enabled  := False ;
  edtValorPagamento.Enabled := False ;
  edtTipoSimulacao.Text     := '' ;

  if (RadioGroup1.ItemIndex = 1) then
  begin
      edtTerceiro.Enabled := True ;
      edtTerceiro.SetFocus ;
  end ;

  if (RadioGroup1.ItemIndex = 2) then
  begin
      edtCNPJEmissor.Enabled := True ;
      edtCNPJEmissor.SetFocus ;
  end ;

  if (RadioGroup1.ItemIndex = 3) then
  begin
      edtDtDeposito.Enabled := True ;
      edtDtDeposito.SetFocus ;
  end ;

  if (RadioGroup1.ItemIndex = 4) then
  begin
      edtTipoSimulacao.Enabled  := True ;
      edtValorPagamento.Enabled := True ;
      edtTipoSimulacao.Text     := '2' ;
      edtTipoSimulacao.SetFocus ;
  end ;

end;

procedure TfrmConsultaCheque.ToolButton1Click(Sender: TObject);
begin
  inherited;

  usp_Resultado.Close ;

  // todos os disponiveis para deposito
  If (RadioGroup1.ItemIndex = 0) then
      usp_Resultado.Parameters.ParamByName('@cTipoConsulta').Value := '0' ;


  // todos os disponiveis para deposito
  If (RadioGroup1.ItemIndex = 1) then
  begin
      if not qryTerceiro.Active then
      begin
          ShowMessage('Selecione o terceiro.') ;
          exit ;
      end ;

      usp_Resultado.Parameters.ParamByName('@cTipoConsulta').Value := '1' ;
      usp_Resultado.Parameters.ParamByName('@nCdTerceiro').Value   := qryTerceironCdTerceiro.Value ;
  end ;

  // por cpf
  If (RadioGroup1.ItemIndex = 2) then
  begin
      if (frmMenu.TestaCpfCgc(Trim(edtCNPJEmissor.Text)) = '') then
      begin
          exit ;
      end ;

      usp_Resultado.Parameters.ParamByName('@cTipoConsulta').Value := '2' ;
      usp_Resultado.Parameters.ParamByName('@cCNPJCPF').Value      := Trim(edtCNPJEmissor.Text) ;
  end ;

  // por data deposito
  If (RadioGroup1.ItemIndex = 3) then
  begin
      usp_Resultado.Parameters.ParamByName('@cTipoConsulta').Value := '3' ;
      usp_Resultado.Parameters.ParamByName('@dDtDeposito').Value   := Trim(edtDtDeposito.Text) ;
  end ;

  // simulacao pagamento
  If (RadioGroup1.ItemIndex = 4) then
  begin
      usp_Resultado.Parameters.ParamByName('@cTipoConsulta').Value := '4' ;
      usp_Resultado.Parameters.ParamByName('@cTpSimulacao').Value  := Trim(edtTipoSimulacao.Text) ;
      usp_Resultado.Parameters.ParamByName('@nValPagamento').Value := StrToFloat(Trim(edtValorPagamento.Text)) ;
  end ;

  if (trim(edtNumCheque.Text) <> '') then
      usp_Resultado.Parameters.ParamByName('@iNrCheque').Value := StrToInt(Trim(edtNumCheque.Text)) 
  else
      usp_Resultado.Parameters.ParamByName('@iNrCheque').Value := 0 ;

  usp_Resultado.Parameters.ParamByName('@nCdLoteLiq').Value    := 0 ;

  usp_Resultado.Open ;

end;

procedure TfrmConsultaCheque.edtTerceiroKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(101);

        If (nPK > 0) then
        begin
            edtTerceiro.Text := IntToStr(nPK) ;
            PosicionaQuery(qryTerceiro, edtTerceiro.Text) ;
        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmConsultaCheque) ;
    
end.
