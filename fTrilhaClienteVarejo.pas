unit fTrilhaClienteVarejo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, Buttons,
  cxPC, cxControls, StdCtrls, DB, ADODB, cxNavigator, cxDBNavigator, Mask,
  DBCtrls, cxLabel, cxContainer, cxEdit, cxTextEdit;

type
  TfrmTrilhaClienteVarejo = class(TfrmProcesso_Padrao)
    cxPageControl1: TcxPageControl;
    TabPessoais: TcxTabSheet;
    TabComerciais: TcxTabSheet;
    TabConjuge: TcxTabSheet;
    TabReferencias: TcxTabSheet;
    GroupBox1: TGroupBox;
    qryLog: TADOQuery;
    qryLognCdTrilhaAuditoria: TAutoIncField;
    qryLognCdUsuario: TIntegerField;
    qryLogcNmUsuario: TStringField;
    qryLogdDtLog: TDateTimeField;
    qryLogcHost: TStringField;
    dsLog: TDataSource;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    cxDBNavigator1: TcxDBNavigator;
    qryDadosPessoais: TADOQuery;
    qryDadosPessoaiscNmTerceiro_Antes: TStringField;
    qryDadosPessoaiscNmTerceiro_Depois: TStringField;
    qryDadosPessoaiscCNPJCPF_Antes: TStringField;
    qryDadosPessoaiscCNPJCPF_Depois: TStringField;
    qryDadosPessoaisnValLimiteCred_Antes: TBCDField;
    qryDadosPessoaisnValLimiteCred_Depois: TBCDField;
    qryDadosPessoaiscRG_Antes: TStringField;
    qryDadosPessoaiscRG_Depois: TStringField;
    qryDadosPessoaiscTelefoneMovel_Antes: TStringField;
    qryDadosPessoaiscTelefoneMovel_Depois: TStringField;
    qryDadosPessoaiscEmail_Antes: TStringField;
    qryDadosPessoaiscEmail_Depois: TStringField;
    qryDadosPessoaisdDtNasc_Antes: TDateTimeField;
    qryDadosPessoaisdDtNasc_Depois: TDateTimeField;
    qryDadosPessoaisdDtEmissaoRG_Antes: TDateTimeField;
    qryDadosPessoaisdDtEmissaoRG_Depois: TDateTimeField;
    qryDadosPessoaiscNmTabTipoSexo_Antes: TStringField;
    qryDadosPessoaiscNmTabTipoSexo_Depois: TStringField;
    qryDadosPessoaiscNmTabTipoEstadoCivil_Antes: TStringField;
    qryDadosPessoaiscNmTabTipoEstadoCivil_Depois: TStringField;
    qryDadosPessoaisiNrDependentes_Antes: TIntegerField;
    qryDadosPessoaisiNrDependentes_Depois: TIntegerField;
    qryDadosPessoaiscNaturalidade_Antes: TStringField;
    qryDadosPessoaiscNaturalidade_Depois: TStringField;
    qryDadosPessoaiscNmGrauEscola_Antes: TStringField;
    qryDadosPessoaiscNmGrauEscola_Depois: TStringField;
    qryDadosPessoaisiDiaVenctoPref_Antes: TIntegerField;
    qryDadosPessoaisiDiaVenctoPref_Depois: TIntegerField;
    qryDadosPessoaiscNmMae_Antes: TStringField;
    qryDadosPessoaiscNmMae_Depois: TStringField;
    qryDadosPessoaiscNmPai_Antes: TStringField;
    qryDadosPessoaiscNmPai_Depois: TStringField;
    Image2: TImage;
    Image3: TImage;
    Image4: TImage;
    Image5: TImage;
    Label3: TLabel;
    DBEdit5: TDBEdit;
    DataSource1: TDataSource;
    DBEdit6: TDBEdit;
    Label6: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label8: TLabel;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    Label5: TLabel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    Label9: TLabel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    Label12: TLabel;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    Label7: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label11: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    Label14: TLabel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    Label16: TLabel;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    Label18: TLabel;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    Label10: TLabel;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    Label15: TLabel;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    Label19: TLabel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    Label21: TLabel;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    Label13: TLabel;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    Label17: TLabel;
    lblRegistro: TLabel;
    qryDadosComerciais: TADOQuery;
    qryDadosComerciaiscNmEmpTrab_Antes: TStringField;
    qryDadosComerciaiscNmEmpTrab_Depois: TStringField;
    qryDadosComerciaiscTelefoneEmpTrab_Antes: TStringField;
    qryDadosComerciaiscTelefoneEmpTrab_Depois: TStringField;
    qryDadosComerciaiscRamalEmpTrab_Antes: TStringField;
    qryDadosComerciaiscRamalEmpTrab_Depois: TStringField;
    qryDadosComerciaiscEnderecoEmpTrab_Antes: TStringField;
    qryDadosComerciaiscEnderecoEmpTrab_Depois: TStringField;
    qryDadosComerciaisiNrEnderecoEmpTrab_Antes: TIntegerField;
    qryDadosComerciaisiNrEnderecoEmpTrab_Depois: TIntegerField;
    qryDadosComerciaiscBairroEmpTrab_Antes: TStringField;
    qryDadosComerciaiscBairroEmpTrab_Depois: TStringField;
    qryDadosComerciaiscCidadeEmpTrab_Antes: TStringField;
    qryDadosComerciaiscCidadeEmpTrab_Depois: TStringField;
    qryDadosComerciaiscUFEmpTrab_Antes: TStringField;
    qryDadosComerciaiscUFEmpTrab_Depois: TStringField;
    qryDadosComerciaiscCEPEmpTrab_Antes: TStringField;
    qryDadosComerciaiscCEPEmpTrab_Depois: TStringField;
    qryDadosComerciaiscNmTabTipoClasseProf_Antes: TStringField;
    qryDadosComerciaiscNmTabTipoClasseProf_Depois: TStringField;
    qryDadosComerciaiscNmTabTipoProfissao_Antes: TStringField;
    qryDadosComerciaiscNmTabTipoProfissao_Depois: TStringField;
    qryDadosComerciaiscCargoTrab_Antes: TStringField;
    qryDadosComerciaiscCargoTrab_Depois: TStringField;
    qryDadosComerciaisdDtAdmissao_Antes: TStringField;
    qryDadosComerciaisdDtAdmissao_Depois: TStringField;
    qryDadosComerciaisnValRendaBruta_Antes: TStringField;
    qryDadosComerciaisnValRendaBruta_Depois: TStringField;
    Label20: TLabel;
    DBEdit37: TDBEdit;
    DataSource2: TDataSource;
    DBEdit38: TDBEdit;
    Label23: TLabel;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    Label25: TLabel;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    Label27: TLabel;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    Label22: TLabel;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    Label26: TLabel;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    Label29: TLabel;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    Label24: TLabel;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    Label30: TLabel;
    DBEdit53: TDBEdit;
    DBEdit54: TDBEdit;
    Label28: TLabel;
    DBEdit55: TDBEdit;
    DBEdit56: TDBEdit;
    Label32: TLabel;
    DBEdit57: TDBEdit;
    DBEdit58: TDBEdit;
    Label31: TLabel;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    Label33: TLabel;
    DBEdit61: TDBEdit;
    DBEdit62: TDBEdit;
    Label34: TLabel;
    DBEdit63: TDBEdit;
    DBEdit64: TDBEdit;
    qryDadosConjuge: TADOQuery;
    qryDadosConjugecNmCjg_Antes: TStringField;
    qryDadosConjugecNmCjg_Depois: TStringField;
    qryDadosConjugedDtNascCjg_Antes: TDateTimeField;
    qryDadosConjugedDtNascCjg_Depois: TDateTimeField;
    qryDadosConjugedDtEmissaoRGCjg_Antes: TDateTimeField;
    qryDadosConjugedDtEmissaoRGCjg_Depois: TDateTimeField;
    qryDadosConjugecRG_Depois: TStringField;
    qryDadosConjugecRG_Antes: TStringField;
    qryDadosConjugecCPFCjg_Antes_Antes: TStringField;
    qryDadosConjugecCPFCjg_Depois_Depois: TStringField;
    qryDadosConjugecTelefoneCelCjg_Antes: TStringField;
    qryDadosConjugecTelefoneCelCjg_Depois: TStringField;
    qryDadosConjugecNmEmpresaTrabCjg_Antes: TStringField;
    qryDadosConjugecNmEmpresaTrabCjg_Depois: TStringField;
    qryDadosConjugecTelefoneEmpTrabCjg_Antes: TStringField;
    qryDadosConjugecTelefoneEmpTrabCjg_Depois: TStringField;
    qryDadosConjugecRamalEmpTrabCjg_Antes: TStringField;
    qryDadosConjugecRamalEmpTrabCjg_Depois: TStringField;
    qryDadosConjugecNmClasseProf_Antes: TStringField;
    qryDadosConjugecNmClasseProf_Depois: TStringField;
    qryDadosConjugecNmTabTipoProfissao_Antes: TStringField;
    qryDadosConjugecNmTabTipoProfissao_Depois: TStringField;
    qryDadosConjugecCargoTrabCjg_Antes: TStringField;
    qryDadosConjugecCargoTrabCjg_Depois: TStringField;
    qryDadosConjugedDtAdmissao_Antes: TStringField;
    qryDadosConjugedDtAdmissao_Depois: TStringField;
    qryDadosConjugenValRendaBruta_Antes: TStringField;
    qryDadosConjugenValRendaBruta_Depois: TStringField;
    Label35: TLabel;
    DBEdit65: TDBEdit;
    DataSource3: TDataSource;
    DBEdit66: TDBEdit;
    Label37: TLabel;
    DBEdit67: TDBEdit;
    DBEdit68: TDBEdit;
    Label39: TLabel;
    DBEdit69: TDBEdit;
    DBEdit70: TDBEdit;
    Label41: TLabel;
    DBEdit71: TDBEdit;
    DBEdit72: TDBEdit;
    Label43: TLabel;
    DBEdit73: TDBEdit;
    DBEdit74: TDBEdit;
    Label36: TLabel;
    DBEdit75: TDBEdit;
    DBEdit76: TDBEdit;
    Label40: TLabel;
    DBEdit77: TDBEdit;
    DBEdit78: TDBEdit;
    Label44: TLabel;
    DBEdit79: TDBEdit;
    DBEdit80: TDBEdit;
    Label46: TLabel;
    DBEdit81: TDBEdit;
    DBEdit82: TDBEdit;
    Label38: TLabel;
    DBEdit83: TDBEdit;
    DBEdit84: TDBEdit;
    Label45: TLabel;
    DBEdit85: TDBEdit;
    DBEdit86: TDBEdit;
    Label42: TLabel;
    DBEdit87: TDBEdit;
    DBEdit88: TDBEdit;
    Label48: TLabel;
    DBEdit89: TDBEdit;
    DBEdit90: TDBEdit;
    Label50: TLabel;
    DBEdit91: TDBEdit;
    DBEdit92: TDBEdit;
    Label47: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    qryReferencias: TADOQuery;
    qryReferenciascNmRendaAdd1_Depois: TStringField;
    qryReferenciascNmRendaAdd1_Antes: TStringField;
    qryReferenciascNmRendaAdd2_Depois: TStringField;
    qryReferenciascNmRendaAdd2_Antes: TStringField;
    qryReferenciascNmRendaAdd3_Depois: TStringField;
    qryReferenciascNmRendaAdd3_Antes: TStringField;
    qryReferenciascNmRefPessoal1_Depois: TStringField;
    qryReferenciascNmRefPessoal1_Antes: TStringField;
    qryReferenciascNmRefPessoal2_Depois: TStringField;
    qryReferenciascNmRefPessoal2_Antes: TStringField;
    qryReferenciascNmRefPessoal3_Depois: TStringField;
    qryReferenciascNmRefPessoal3_Antes: TStringField;
    qryReferenciascNmRefComercial1_Depois: TStringField;
    qryReferenciascNmRefComercial1_Antes: TStringField;
    qryReferenciascNmRefComercial2_Depois: TStringField;
    qryReferenciascNmRefComercial2_Antes: TStringField;
    qryReferenciascNmRefComercial3_Depois: TStringField;
    qryReferenciascNmRefComercial3_Antes: TStringField;
    DataSource4: TDataSource;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    Label51: TLabel;
    DBEdit99: TDBEdit;
    DBEdit100: TDBEdit;
    Label55: TLabel;
    DBEdit101: TDBEdit;
    DBEdit102: TDBEdit;
    Label57: TLabel;
    DBEdit103: TDBEdit;
    DBEdit104: TDBEdit;
    Label53: TLabel;
    DBEdit105: TDBEdit;
    Label56: TLabel;
    DBEdit106: TDBEdit;
    Label58: TLabel;
    DBEdit107: TDBEdit;
    DBEdit108: TDBEdit;
    DBEdit109: TDBEdit;
    DBEdit110: TDBEdit;
    qryDadosPessoaiscCaixaPostal_Antes: TStringField;
    qryDadosPessoaiscCaixaPostal_Depois: TStringField;
    qryDadosPessoaiscTelefoneRec1_Antes: TStringField;
    qryDadosPessoaiscTelefoneRec1_Depois: TStringField;
    qryDadosPessoaiscTelefoneRec2_Antes: TStringField;
    qryDadosPessoaiscTelefoneRec2_Depois: TStringField;
    qryDadosPessoaiscNrDocOutro_Antes: TStringField;
    qryDadosPessoaiscNrDocOutro_Depois: TStringField;
    Label59: TLabel;
    DBEdit111: TDBEdit;
    DBEdit112: TDBEdit;
    DBEdit113: TDBEdit;
    Label62: TLabel;
    DBEdit114: TDBEdit;
    Label63: TLabel;
    DBEdit115: TDBEdit;
    DBEdit116: TDBEdit;
    Label65: TLabel;
    DBEdit117: TDBEdit;
    DBEdit118: TDBEdit;
    Label49: TLabel;
    Label52: TLabel;
    Label54: TLabel;
    DBEdit93: TDBEdit;
    DBEdit94: TDBEdit;
    DBEdit95: TDBEdit;
    DBEdit96: TDBEdit;
    DBEdit97: TDBEdit;
    DBEdit98: TDBEdit;
    Label60: TLabel;
    DBEdit119: TDBEdit;
    DBEdit120: TDBEdit;
    qryDadosComerciaiscCNPJEmpTrab_Antes: TStringField;
    qryDadosComerciaiscCNPJEmpTrab_Depois: TStringField;
    qryDadosComerciaiscComplementocEndEmpTrab_Antes: TStringField;
    qryDadosComerciaiscComplementocEndEmpTrab_Depois: TStringField;
    Label61: TLabel;
    DBEdit121: TDBEdit;
    DBEdit122: TDBEdit;
    qryDadosConjugecCNPJEmpTrabCjg_Antes: TStringField;
    qryDadosConjugecCNPJEmpTrabCjg_Depois: TStringField;
    qryDadosConjugecFlgRendaLimiteConjuge_Antes: TStringField;
    qryDadosConjugecFlgRendaLimiteConjuge_Depois: TStringField;
    DBEdit123: TDBEdit;
    DBEdit124: TDBEdit;
    Label64: TLabel;
    DBEdit125: TDBEdit;
    DBEdit126: TDBEdit;
    Label66: TLabel;
    qryReferenciascRefComercialGeral_Antes: TStringField;
    qryReferenciascRefComercialGeral_Depois: TStringField;
    qryReferenciasnCdBancoRef_Antes: TStringField;
    qryReferenciasnCdBancoRef_Depois: TStringField;
    qryReferenciascAgenciaRef_Antes: TStringField;
    qryReferenciascAgenciaRef_Depois: TStringField;
    qryReferenciascContaBancariaRef_Antes: TStringField;
    qryReferenciascContaBancariaRef_Depois: TStringField;
    qryReferenciascFlgCartaoCredito_Antes: TStringField;
    qryReferenciascFlgCartaoCredito_Depois: TStringField;
    qryReferenciascFlgTalaoCheque_Antes: TStringField;
    qryReferenciascFlgTalaoCheque_Depois: TStringField;
    qryReferenciascNmTipoContaBancaria_Antes: TStringField;
    qryReferenciascNmTipoContaBancaria_Depois: TStringField;
    Label67: TLabel;
    DBEdit127: TDBEdit;
    DBEdit128: TDBEdit;
    Label69: TLabel;
    DBEdit129: TDBEdit;
    DBEdit130: TDBEdit;
    Label71: TLabel;
    Label73: TLabel;
    Label75: TLabel;
    Label77: TLabel;
    Label79: TLabel;
    DBEdit141: TDBEdit;
    DBEdit142: TDBEdit;
    DBEdit143: TDBEdit;
    DBEdit144: TDBEdit;
    DBEdit131: TDBEdit;
    DBEdit132: TDBEdit;
    DBEdit133: TDBEdit;
    DBEdit134: TDBEdit;
    DBEdit135: TDBEdit;
    DBEdit136: TDBEdit;
    qryDadosPessoaiscFlgBloqAtuLimAutom_Antes: TStringField;
    qryDadosPessoaiscFlgBloqAtuLimAutom_Depois: TStringField;
    DBEdit137: TDBEdit;
    DBEdit138: TDBEdit;
    Label68: TLabel;
    procedure qryLogAfterScroll(DataSet: TDataSet);
    procedure qryDadosPessoaisAfterScroll(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure ColorirDiferenca(edtAntes: TDBEdit; edtDepois: TDBEDit) ;
    procedure qryDadosComerciaisAfterScroll(DataSet: TDataSet);
    procedure cxPageControl1Change(Sender: TObject);
    procedure qryDadosConjugeAfterScroll(DataSet: TDataSet);
    procedure qryReferenciasAfterScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    cCor : DWord  ;
  public
    { Public declarations }
  end;

var
  frmTrilhaClienteVarejo: TfrmTrilhaClienteVarejo;

implementation

{$R *.dfm}

procedure TfrmTrilhaClienteVarejo.qryLogAfterScroll(DataSet: TDataSet);
begin
  inherited;

  lblRegistro.Caption := IntToStr(qryLog.RecNo) + ' / ' + IntToStr(qryLog.RecordCount) ;

  qryDadosPessoais.Close ;
  qryDadosComerciais.Close ;
  qryDadosConjuge.Close ;
  qryReferencias.Close ;

  if (cxPageControl1.ActivePageIndex = 0) then
      PosicionaQuery(qryDadosPessoais, qryLognCdTrilhaAuditoria.AsString) ;

  if (cxPageControl1.ActivePageIndex = 1) then
      PosicionaQuery(qryDadosComerciais, qryLognCdTrilhaAuditoria.AsString) ;

  if (cxPageControl1.ActivePageIndex = 2) then
      PosicionaQuery(qryDadosConjuge, qryLognCdTrilhaAuditoria.AsString) ;

  if (cxPageControl1.ActivePageIndex = 3) then
      PosicionaQuery(qryReferencias, qryLognCdTrilhaAuditoria.AsString) ;


end;

procedure TfrmTrilhaClienteVarejo.qryDadosPessoaisAfterScroll(
  DataSet: TDataSet);
var
    i : Integer;
    i2 : integer ;
begin
  inherited;

  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TDBEdit) then
    begin
        if ((Components[I] as TDBEdit).Tag = 1) then
        begin

            (Components[I] as TDBEdit).Color      := cCor ;
            (Components[I] as TDBEdit).Font.Color := clBlack ;
            (Components[I] as TDBEdit).ReadOnly   := True ;

        end ;
    end ;

  end ;

  ColorirDiferenca(DBEdit5 , DBEdit6) ;
  ColorirDiferenca(DBEdit7 , DBEdit8) ;
  ColorirDiferenca(DBEdit9 , DBEdit10);
  ColorirDiferenca(DBEdit17, DBEdit18);
  ColorirDiferenca(DBEdit13, DBEdit14);
  ColorirDiferenca(DBEdit15, DBEdit16);
  ColorirDiferenca(DBEdit11, DBEdit12);
  ColorirDiferenca(DBEdit19, DBEdit20);
  ColorirDiferenca(DBEdit21, DBEdit22);
  ColorirDiferenca(DBEdit23, DBEdit24);
  ColorirDiferenca(DBEdit25, DBEdit26);
  ColorirDiferenca(DBEdit27, DBEdit28);
  ColorirDiferenca(DBEdit29, DBEdit30);
  ColorirDiferenca(DBEdit31, DBEdit32);
  ColorirDiferenca(DBEdit33, DBEdit34);
  ColorirDiferenca(DBEdit35, DBEdit36);

  ColorirDiferenca(DBEdit115, DBEdit116);
  ColorirDiferenca(DBEdit111, DBEdit112);
  ColorirDiferenca(DBEdit113, DBEdit114);
  ColorirDiferenca(DBEdit117, DBEdit118);
  ColorirDiferenca(DBEdit35, DBEdit36);
  ColorirDiferenca(DBEdit137, DBEdit138);


end;

procedure TfrmTrilhaClienteVarejo.FormCreate(Sender: TObject);
begin
  inherited;

    cCor := $00E9E4E4 ;
end;

procedure TfrmTrilhaClienteVarejo.ColorirDiferenca(edtAntes,
  edtDepois: TDBEDit);
begin

    if (edtAntes.Text <> edtDepois.Text) then
    begin

        edtAntes.Color       := clTeal ;
        edtAntes.Font.Color  := clBlack ;

        edtDepois.Color      := clTeal ;
        edtDepois.Font.Color := clBlack ;

    end ;

end;

procedure TfrmTrilhaClienteVarejo.qryDadosComerciaisAfterScroll(
  DataSet: TDataSet);
var
    i : Integer;
    i2 : integer ;
begin
  inherited;

  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TDBEdit) then
    begin
        if ((Components[I] as TDBEdit).Tag = 2) then
        begin

            (Components[I] as TDBEdit).Color      := cCor ;
            (Components[I] as TDBEdit).Font.Color := clBlack ;
            (Components[I] as TDBEdit).ReadOnly   := True ;

        end ;
    end ;

  end ;

  ColorirDiferenca(DBEdit37, DBEdit38);
  ColorirDiferenca(DBEdit119,DBEdit120);
  ColorirDiferenca(DBEdit39, DBEdit40);
  ColorirDiferenca(DBEdit41, DBEdit42);
  ColorirDiferenca(DBEdit43, DBEdit44);
  ColorirDiferenca(DBEdit45, DBEdit46);
  ColorirDiferenca(DBEdit121,DBEdit122);
  ColorirDiferenca(DBEdit47, DBEdit48);
  ColorirDiferenca(DBEdit49, DBEdit50);
  ColorirDiferenca(DBEdit51, DBEdit52);
  ColorirDiferenca(DBEdit53, DBEdit54);
  ColorirDiferenca(DBEdit55, DBEdit56);
  ColorirDiferenca(DBEdit57, DBEdit58);
  ColorirDiferenca(DBEdit59, DBEdit60);
  ColorirDiferenca(DBEdit61, DBEdit62);
  ColorirDiferenca(DBEdit63, DBEdit64);
  ColorirDiferenca(DBEdit93, DBEdit94);
  ColorirDiferenca(DBEdit95, DBEdit96);
  ColorirDiferenca(DBEdit97, DBEdit98);

end;

procedure TfrmTrilhaClienteVarejo.cxPageControl1Change(Sender: TObject);
begin
  inherited;

  if (cxPageControl1.ActivePageIndex = 0) and (qryDadosPessoais.Eof) then
      PosicionaQuery(qryDadosPessoais, qryLognCdTrilhaAuditoria.AsString) ;

  if (cxPageControl1.ActivePageIndex = 1) and (qryDadosComerciais.Eof) then
      PosicionaQuery(qryDadosComerciais, qryLognCdTrilhaAuditoria.AsString) ;

  if (cxPageControl1.ActivePageIndex = 2) and (qryDadosConjuge.Eof) then
      PosicionaQuery(qryDadosConjuge, qryLognCdTrilhaAuditoria.AsString) ;

  if (cxPageControl1.ActivePageIndex = 3) and (qryReferencias.Eof) then
      PosicionaQuery(qryReferencias, qryLognCdTrilhaAuditoria.AsString) ;

end;

procedure TfrmTrilhaClienteVarejo.qryDadosConjugeAfterScroll(
  DataSet: TDataSet);
var
    i : Integer;
    i2 : integer ;
begin
  inherited;

  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TDBEdit) then
    begin
        if ((Components[I] as TDBEdit).Tag = 3) then
        begin

            (Components[I] as TDBEdit).Color      := cCor ;
            (Components[I] as TDBEdit).Font.Color := clBlack ;
            (Components[I] as TDBEdit).ReadOnly   := True ;

        end ;
    end ;

  end ;

  ColorirDiferenca(DBEdit65, DBEdit66);
  ColorirDiferenca(DBEdit67, DBEdit68);
  ColorirDiferenca(DBEdit69, DBEdit70);
  ColorirDiferenca(DBEdit71, DBEdit72);
  ColorirDiferenca(DBEdit73, DBEdit74);
  ColorirDiferenca(DBEdit75, DBEdit76);
  ColorirDiferenca(DBEdit77, DBEdit78);
  ColorirDiferenca(DBEdit124, DBEdit123);
  ColorirDiferenca(DBEdit79, DBEdit80);
  ColorirDiferenca(DBEdit81, DBEdit82);
  ColorirDiferenca(DBEdit83, DBEdit84);
  ColorirDiferenca(DBEdit85, DBEdit86);
  ColorirDiferenca(DBEdit87, DBEdit88);
  ColorirDiferenca(DBEdit89, DBEdit90);
  ColorirDiferenca(DBEdit91, DBEdit92);
  ColorirDiferenca(DBEdit126, DBEdit125);

end;

procedure TfrmTrilhaClienteVarejo.qryReferenciasAfterScroll(
  DataSet: TDataSet);
var
    i : Integer;
    i2 : integer ;
begin
  inherited;

  for I := 0 to ComponentCount - 1 do
  begin

    if (Components [I] is TDBEdit) then
    begin
        if ((Components[I] as TDBEdit).Tag = 4) then
        begin

            (Components[I] as TDBEdit).Color      := cCor ;
            (Components[I] as TDBEdit).Font.Color := clBlack ;
            (Components[I] as TDBEdit).ReadOnly   := True ;

        end ;
    end ;

  end ;

  ColorirDiferenca(DBEdit99, DBEdit100);
  ColorirDiferenca(DBEdit101,DBEdit102);
  ColorirDiferenca(DBEdit103,DBEdit104);
  ColorirDiferenca(DBEdit105,DBEdit106);
  ColorirDiferenca(DBEdit107,DBEdit108);
  ColorirDiferenca(DBEdit109,DBEdit110);
  ColorirDiferenca(DBEdit127,DBEdit128);
  ColorirDiferenca(DBEdit109,DBEdit110);
  ColorirDiferenca(DBEdit129,DBEdit130);
  ColorirDiferenca(DBEdit109,DBEdit110);
  ColorirDiferenca(DBEdit141,DBEdit142);
  ColorirDiferenca(DBEdit143,DBEdit144);
  ColorirDiferenca(DBEdit131,DBEdit132);
  ColorirDiferenca(DBEdit133,DBEdit134);
  ColorirDiferenca(DBEdit135,DBEdit136);


end;

procedure TfrmTrilhaClienteVarejo.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  
end;

end.
