unit rPedidoItemCancelado;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, ER2Lookup, DB, ADODB, DBCtrls, ER2Excel;

type
  TrptPedidoItemCancelado = class(TfrmRelatorio_Padrao)
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    edtEmpresa: TER2LookupMaskEdit;
    edtTerceiro: TER2LookupMaskEdit;
    edtGrupoProduto: TER2LookupMaskEdit;
    edtProduto: TER2LookupMaskEdit;
    edtTipoPedido2: TER2LookupMaskEdit;
    edtMotivoCancelamento: TER2LookupMaskEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    qryEmpresa: TADOQuery;
    qryGrupoProduto: TADOQuery;
    qryMotivoCancelamento: TADOQuery;
    qryTerceiro: TADOQuery;
    qryProduto: TADOQuery;
    qryTipoPedido: TADOQuery;
    DataSource3: TDataSource;
    DataSource4: TDataSource;
    DataSource5: TDataSource;
    DataSource6: TDataSource;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource7: TDataSource;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DataSource8: TDataSource;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    qryTabTipoPedido: TADOQuery;
    DataSource1: TDataSource;
    qryTabTipoPedidonCdTabTipoPedido: TIntegerField;
    qryTabTipoPedidocNmTabTipoPedido: TStringField;
    edtTabTipoPedido: TER2LookupMaskEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    qryMotivoCancelamentonCdMotivoCancSaldoPed: TIntegerField;
    qryMotivoCancelamentocNmMotivoCancSaldoPed: TStringField;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    Label9: TLabel;
    ER2Excel1: TER2Excel;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    dsLoja: TDataSource;
    dsDepartamento: TDataSource;
    dsCategoria: TDataSource;
    dsSubCategoria: TDataSource;
    qryDepartamento: TADOQuery;
    qrySubCategoria: TADOQuery;
    qryCategoria: TADOQuery;
    edtLoja: TER2LookupMaskEdit;
    edtDepartamento: TER2LookupMaskEdit;
    edtCategoria: TER2LookupMaskEdit;
    edtSubCategoria: TER2LookupMaskEdit;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit8: TDBEdit;
    qryDepartamentonCdDepartamento: TIntegerField;
    qryDepartamentocNmDepartamento: TStringField;
    qryCategorianCdCategoria: TIntegerField;
    qryCategoriacNmCategoria: TStringField;
    qrySubCategorianCdSubCategoria: TIntegerField;
    qrySubCategoriacNmSubCategoria: TStringField;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    procedure edtEmpresaBeforePosicionaQry(Sender: TObject);
    procedure edtProdutoBeforePosicionaQry(Sender: TObject);
    procedure edtTabTipoPedidoAfterPosicionaQry(Sender: TObject);
    procedure edtMotivoCancelamentoBeforeLookup(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure ER2Excel1BeforeExport(Sender: TObject);
    procedure ER2Excel1AfterExport(Sender: TObject);
    procedure edtTipoPedido2BeforeLookup(Sender: TObject);
    procedure edtLojaBeforeLookup(Sender: TObject);
    procedure qryLojaBeforeOpen(DataSet: TDataSet);
    procedure qrySubCategoriaBeforeOpen(DataSet: TDataSet);
    procedure qryProdutoBeforeOpen(DataSet: TDataSet);
    procedure qryCategoriaBeforeOpen(DataSet: TDataSet);
    procedure edtCategoriaBeforeLookup(Sender: TObject);
    procedure edtSubCategoriaBeforeLookup(Sender: TObject);
    procedure edtProdutoBeforeLookup(Sender: TObject);
    procedure qryMotivoCancelamentoBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    iCursor : integer;
  public
    { Public declarations }
  end;

var
  rptPedidoItemCancelado: TrptPedidoItemCancelado;

implementation

uses fMenu, rPedidoItemCancelado_view, fPedidoItemCancelado_tela;

{$R *.dfm}

procedure TrptPedidoItemCancelado.edtEmpresaBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
end;

procedure TrptPedidoItemCancelado.edtProdutoBeforePosicionaQry(
  Sender: TObject);
begin
  inherited;

  qryProduto.Parameters.ParamByName('nCdGrupoProduto').Value := frmMenu.ConvInteiro(edtGrupoProduto.Text);
end;

procedure TrptPedidoItemCancelado.edtTabTipoPedidoAfterPosicionaQry(
  Sender: TObject);
begin
  inherited;

  if (not qryTabTipoPedido.Active) then
      exit;

  if (qryTabTipoPedidonCdTabTipoPedido.Value = 1) then
      RadioGroup1.ItemIndex := 2;

  if (qryTabTipoPedidonCdTabTipoPedido.Value = 2) then
      RadioGroup1.ItemIndex := 3;
end;

procedure TrptPedidoItemCancelado.edtMotivoCancelamentoBeforeLookup(
  Sender: TObject);
begin
  inherited;

  edtMotivoCancelamento.WhereAdicional.Text := 'nCdTabTipoPedido = ' + edtTabTipoPedido.Text;
end;

procedure TrptPedidoItemCancelado.ToolButton1Click(Sender: TObject);
var
  objRel    : TrptPedidoItemCancelado_view;
  objForm   : TfrmPedidoItemCancelado_tela;
  cFiltro   : String;
  iLinha    : integer;
  iLinhaAux : integer;
  dDtCancel : String;
  nValTotal : Double;
begin
  inherited;

  if (Trim(edtTabTipoPedido.Text) = '') then
  begin
      MensagemAlerta('Selecione o Tipo de Pedido.');
      edtTabTipoPedido.SetFocus;
      abort;
  end;

  qryTabTipoPedido.Close;
  PosicionaQuery(qryTabTipoPedido,edtTabTipoPedido.Text);

  if ((qryTabTipoPedidonCdTabTipoPedido.Value = 1) and ((RadioGroup1.ItemIndex <> 0) and (RadioGroup1.ItemIndex <> 2))) then
  begin
      MensagemAlerta('� imposs�vel exibir estes pedidos para o Tipo Pedido de Sa�da.');
      Abort;
  end;

  if ((qryTabTipoPedidonCdTabTipoPedido.Value = 2) and ((RadioGroup1.ItemIndex <> 1) and (RadioGroup1.ItemIndex <> 3))) then
  begin
      MensagemAlerta('� imposs�vel exibir estes pedidos para o Tipo Pedido de Entrada.');
      Abort;
  end;

  if (trim(MaskEdit1.Text) = '/  /') or (Trim(MaskEdit2.Text) = '/  /') then
  begin
      MaskEdit2.Text := DateToStr(Now()) ;
      MaskEdit1.Text := DateToStr(Now()-30) ;
  end ;


  {-- faz a chamada da procedure para exibir os dados --}
  objRel := TrptPedidoItemCancelado_view.Create(nil);

  try
      try

          if (RadioGroup2.ItemIndex = 0) or (RadioGroup2.ItemIndex = 2) then
          begin
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Close;
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdEmpresa').Value                   := frmMenu.ConvInteiro(edtEmpresa.Text);
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdLoja').Value                      := frmMenu.ConvInteiro(edtLoja.Text);
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdTerceiro').Value                  := frmMenu.ConvInteiro(edtTerceiro.Text);
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdGrupoProduto').Value              := frmMenu.ConvInteiro(edtGrupoProduto.Text);
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdProduto').Value                   := frmMenu.ConvInteiro(edtProduto.Text);
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdDepartamento').Value              := frmMenu.ConvInteiro(edtDepartamento.Text);
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdCategoria').Value                 := frmMenu.ConvInteiro(edtCategoria.Text);
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdSubCategoria').Value              := frmMenu.ConvInteiro(edtSubCategoria.Text);
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdTipoPedido').Value                := frmMenu.ConvInteiro(edtTipoPedido2.Text);
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdMotivoCancSaldoPed').Value        := frmMenu.ConvInteiro(edtMotivoCancelamento.Text);
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@dDtCancelSaldoItemPedInicial').Value := MaskEdit1.Text;
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@dDtCancelSaldoItemPedFinal').Value   := MaskEdit2.Text;
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@cFlgTipoPedido').Value               := RadioGroup1.ItemIndex;
              objRel.SPREL_PEDIDO_ITEM_CANCELADO.Open;

              {-- quando n�o tem nenhum resultado encerra o processo aqui --}
              if (objRel.SPREL_PEDIDO_ITEM_CANCELADO.RecordCount = 0) then
              begin
                  MensagemAlerta('Nenhum resultado encontrado para os crit�rios utilizados.');
                  exit;
              end;
          end;

          {-- quando o modo de visualiza��o � relat�rio monta o filtro e exibibe o relat�rio --}
          if (RadioGroup2.ItemIndex = 0) then
          begin
              objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

              cFiltro := '';

              cFiltro := cFiltro + RadioGroup1.Items.Strings[RadioGroup1.ItemIndex];

              if (DBEdit1.Text <> '') then
                  cFiltro := cFiltro + '/ ' + 'Empresa: ' + DBEdit1.Text;

              if (DBEdit8.Text <> '') then
                  cFiltro := cFiltro + '/ ' + 'Loja: ' + DBEdit8.Text;

              if (DBEdit2.Text <> '') then
                  cFiltro := cFiltro + '/ ' + 'Terceiro: ' + DBEdit2.Text;

              if (DBEdit3.Text <> '') then
                  cFiltro := cFiltro + '/ ' + 'Grupo de Produtos: ' + DBEdit3.Text;

              if (DBEdit9.Text <> '') then
                  cFiltro := cFiltro + '/ ' + 'Departamento: ' + DBEdit9.Text;

              if (DBEdit10.Text <> '') then
                  cFiltro := cFiltro + '/ ' + 'Categoria: ' + DBEdit10.Text;

              if (DBEdit11.Text <> '') then
                  cFiltro := cFiltro + '/ ' + 'Sub Categoria: ' + DBEdit11.Text;

              if (DBEdit4.Text <> '') then
              cFiltro := cFiltro + '/ ' + 'Produto: ' + DBEdit4.Text;

              if (DBEdit5.Text <> '') then
              cFiltro := cFiltro + '/ ' + 'Tipo de Pedido: ' + DBEdit5.Text;

              if (DBEdit7.Text <> '') then
              cFiltro := cFiltro + '/ ' + 'Motivo Cancelamento: ' + DBEdit7.Text;

              if ((Trim(MaskEdit1.Text) <> '/  /') and (Trim(MaskEdit2.Text) <> '/  /')) then
                  cFiltro := cFiltro + '/ ' + 'Per�odo: ' + MaskEdit1.Text + ' � ' + MaskEdit2.Text;

              objRel.lblFiltro1.Caption := cFiltro;

              objRel.QuickRep1.PreviewModal;
          end else

          {-- no modo de visualiza��o em tela apresenta o resultado da consulta em grid --}
          if (RadioGroup2.ItemIndex = 1) then
          begin

              objForm := TfrmPedidoItemCancelado_tela.Create(nil);

              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Close;
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdEmpresa').Value                   := frmMenu.ConvInteiro(edtEmpresa.Text);
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdLoja').Value                      := frmMenu.ConvInteiro(edtLoja.Text);
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdTerceiro').Value                  := frmMenu.ConvInteiro(edtTerceiro.Text);
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdGrupoProduto').Value              := frmMenu.ConvInteiro(edtGrupoProduto.Text);
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdProduto').Value                   := frmMenu.ConvInteiro(edtProduto.Text);
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdDepartamento').Value              := frmMenu.ConvInteiro(edtDepartamento.Text);
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdCategoria').Value                 := frmMenu.ConvInteiro(edtCategoria.Text);
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdSubCategoria').Value              := frmMenu.ConvInteiro(edtSubCategoria.Text);
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdTipoPedido').Value                := frmMenu.ConvInteiro(edtTipoPedido2.Text);
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@nCdMotivoCancSaldoPed').Value        := frmMenu.ConvInteiro(edtMotivoCancelamento.Text);
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@dDtCancelSaldoItemPedInicial').Value := MaskEdit1.Text;
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@dDtCancelSaldoItemPedFinal').Value   := MaskEdit2.Text;
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Parameters.ParamByName('@cFlgTipoPedido').Value               := RadioGroup1.ItemIndex;
              objForm.SPREL_PEDIDO_ITEM_CANCELADO.Open;

              {-- quando n�o tem nenhum resultado encerra o processo aqui --}
              if (objForm.SPREL_PEDIDO_ITEM_CANCELADO.RecordCount = 0) then
              begin
                  MensagemAlerta('Nenhum resultado encontrado para os crit�rios utilizados.');
                  exit;
              end;

              showForm(objForm, True);
          end else

          {-- gera a exporta��o do relat�rio para uma planilha do excel --}
          if (RadioGroup2.ItemIndex = 2) then
          begin
              iLinha    := 3;
              iLinhaAux := 0;
              nValTotal := 0;

              objRel.SPREL_PEDIDO_ITEM_CANCELADO.First;

              while not objRel.SPREL_PEDIDO_ITEM_CANCELADO.Eof do
              begin
                  {-- quando a data de cancelamento for diferente reimprime o cabe�alho,
                   -- pois se trata de um novo agrupamento --}
                  if (objRel.SPREL_PEDIDO_ITEM_CANCELADOdDtCancelSaldoItemPed.AsString <> dDtCancel) then
                  begin
                      ER2Excel1.Celula['A' + intToStr(iLinha)].Text            := 'Data de Cancelamento: ';
                      ER2Excel1.Celula['A' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['A' + intToStr(iLinha)].Mesclar('B' + intToStr(iLinha));
                      ER2Excel1.Celula['A' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['C' + intToStr(iLinha)].Text    := objRel.SPREL_PEDIDO_ITEM_CANCELADOdDtCancelSaldoItemPed.AsString;
                      ER2Excel1.Celula['C' + intToStr(iLinha)].Mascara := 'dd/mm/aaaa';

                      iLinha := iLinha + 1;

                      ER2Excel1.Celula['A' + intToStr(iLinha)].Text            := 'C�d. Pedido';
                      ER2Excel1.Celula['A' + intToStr(iLinha)].Border          := [EdgeBottom];
                      ER2Excel1.Celula['A' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['A' + intToStr(iLinha)].Range('I' + intToStr(iLinha));
                      ER2Excel1.Celula['A' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['B' + intToStr(iLinha)].Text   := 'Data Pedido';
                      ER2Excel1.Celula['B' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['C' + intToStr(iLinha)].Text   := 'Terceiro';
                      ER2Excel1.Celula['C' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['D' + intToStr(iLinha)].Text   := 'Tipo Pedido';
                      ER2Excel1.Celula['D' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['E' + intToStr(iLinha)].Text   := 'Produto';
                      ER2Excel1.Celula['E' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['F' + intToStr(iLinha)].Text            := 'Qtde Cancelada';
                      ER2Excel1.Celula['F' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['F' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['G' + intToStr(iLinha)].Text            := 'Valor Total Cancelado';
                      ER2Excel1.Celula['G' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['G' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['H' + intToStr(iLinha)].Text   := 'Usu�rio Cancelamento';
                      ER2Excel1.Celula['H' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['I' + intToStr(iLinha)].Text   := 'Motivo Cancelamento';
                      ER2Excel1.Celula['I' + intToStr(iLinha)].Negrito;

                      iLinha    := iLinha + 1;
                      iLinhaAux := iLinha;

                      dDtCancel := objRel.SPREL_PEDIDO_ITEM_CANCELADOdDtCancelSaldoItemPed.AsString;

                  end;

                  {-- imprime os dados dos itens cancelados --}

                  ER2Excel1.Celula['A' + intToStr(iLinha)].Text            := objRel.SPREL_PEDIDO_ITEM_CANCELADOnCdPedido.Value;
                  ER2Excel1.Celula['A' + intToStr(iLinha)].HorizontalAlign := haRight;

                  ER2Excel1.Celula['B' + intToStr(iLinha)].Text            := objRel.SPREL_PEDIDO_ITEM_CANCELADOdDtPedido.AsString;
                  ER2Excel1.Celula['B' + intToStr(iLinha)].Mascara         := 'dd/mm/aaaa';

                  ER2Excel1.Celula['C' + intToStr(iLinha)].Text            := objRel.SPREL_PEDIDO_ITEM_CANCELADOcNmTerceiro.Value;

                  ER2Excel1.Celula['D' + intToStr(iLinha)].Text            := objRel.SPREL_PEDIDO_ITEM_CANCELADOcNmTipoPedido.Value;

                  ER2Excel1.Celula['E' + intToStr(iLinha)].Text            := objRel.SPREL_PEDIDO_ITEM_CANCELADOcNmProduto.Value;

                  ER2Excel1.Celula['F' + intToStr(iLinha)].Text            := objRel.SPREL_PEDIDO_ITEM_CANCELADOnQtdeCanc.Value;
                  ER2Excel1.Celula['F' + intToStr(iLinha)].HorizontalAlign := haRight;
                  ER2Excel1.Celula['F' + intToStr(iLinha)].Mascara         := '0';

                  ER2Excel1.Celula['G' + intToStr(iLinha)].Text            := objRel.SPREL_PEDIDO_ITEM_CANCELADOnValTotalCanc.Value;
                  ER2Excel1.Celula['G' + intToStr(iLinha)].HorizontalAlign := haRight;
                  ER2Excel1.Celula['G' + intToStr(iLinha)].Mascara         := '0,00';

                  nValTotal := nValTotal + objRel.SPREL_PEDIDO_ITEM_CANCELADOnValTotalCanc.Value;

                  ER2Excel1.Celula['H' + intToStr(iLinha)].Text := objRel.SPREL_PEDIDO_ITEM_CANCELADOcNmUsuario.Value;

                  ER2Excel1.Celula['I' + intToStr(iLinha)].Text := objRel.SPREL_PEDIDO_ITEM_CANCELADOcNmMotivoCancSaldoPed.Value;

                  iLinha := iLinha + 1;

                  objRel.SPREL_PEDIDO_ITEM_CANCELADO.Next;

                  {-- quando a data do proximo item for diferente, ou seja o item pertence a um novo agrupamento,
                   -- subtotaliza o grupo --}
                  if (objRel.SPREL_PEDIDO_ITEM_CANCELADOdDtCancelSaldoItemPed.AsString <> dDtCancel) then
                  begin
                      ER2Excel1.Celula['F' + intToStr(iLinha)].Text            := 'SubTotal Cancelado: ';
                      ER2Excel1.Celula['F' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['F' + intToStr(iLinha)].Negrito;
                      ER2Excel1.Celula['F' + intToStr(iLinha)].Italico;

                      ER2Excel1.Celula['G' + intToStr(iLinha)].AutoSoma('G' + intToStr(iLinhaAux),'G' + intToStr(iLinha - 1));
                      ER2Excel1.Celula['G' + intToStr(iLinha)].Mascara         := '0,00';
                      ER2Excel1.Celula['G' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['G' + intToStr(iLinha)].Negrito;

                      iLinha := iLinha + 2;
                  end;

                  {-- quando chega ao ultimo registro da query (ultimo item do ultimo agrupamento),
                   -- subtotaliza o grupo e coloca a soma total de todos os grupos --}
                  if (objRel.SPREL_PEDIDO_ITEM_CANCELADO.Eof) then
                  begin
                      ER2Excel1.Celula['F' + intToStr(iLinha)].Text            := 'SubTotal Cancelado: ';
                      ER2Excel1.Celula['F' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['F' + intToStr(iLinha)].Negrito;
                      ER2Excel1.Celula['F' + intToStr(iLinha)].Italico;

                      ER2Excel1.Celula['G' + intToStr(iLinha)].AutoSoma('G' + intToStr(iLinhaAux),'G' + intToStr(iLinha - 1));
                      ER2Excel1.Celula['G' + intToStr(iLinha)].Mascara         := '0,00';
                      ER2Excel1.Celula['G' + intToStr(iLinha)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['G' + intToStr(iLinha)].Negrito;

                      ER2Excel1.Celula['F' + intToStr(iLinha + 1)].Text            := 'Total Cancelado: ';
                      ER2Excel1.Celula['F' + intToStr(iLinha + 1)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['F' + intToStr(iLinha + 1)].Negrito;
                      ER2Excel1.Celula['F' + intToStr(iLinha + 1)].Italico;

                      ER2Excel1.Celula['G' + intToStr(iLinha + 1)].Text            := nValTotal;
                      ER2Excel1.Celula['G' + intToStr(iLinha + 1)].HorizontalAlign := haRight;
                      ER2Excel1.Celula['G' + intToStr(iLinha + 1)].Mascara         := '0,00';
                      ER2Excel1.Celula['G' + intToStr(iLinha + 1)].Negrito;

                  end;
              end;

              ER2Excel1.ExportXLS;
          end;
      except
          MensagemErro('Erro ao criar a classe ' + objRel.ClassName);
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;
end;

procedure TrptPedidoItemCancelado.ER2Excel1BeforeExport(Sender: TObject);
begin
  inherited;

  iCursor       := Screen.Cursor;
  Screen.Cursor := crSQLWait;

  frmMenu.mensagemUsuario('Exportando planilha...');
end;

procedure TrptPedidoItemCancelado.ER2Excel1AfterExport(Sender: TObject);
begin
  inherited;

  Screen.Cursor := iCursor;

  frmMenu.mensagemUsuario('');
end;

procedure TrptPedidoItemCancelado.edtTipoPedido2BeforeLookup(
  Sender: TObject);
begin
    inherited;

    edtTipoPedido2.WhereAdicional.Text := 'nCdTabTipoPedido = ' + IntToStr(frmMenu.ConvInteiro(edtTabTipoPedido.Text));
end;

procedure TrptPedidoItemCancelado.edtLojaBeforeLookup(Sender: TObject);
begin
    inherited;

    edtLoja.WhereAdicional.Text := 'Empresa.nCdEmpresa = ' + IntToStr(frmMenu.ConvInteiro(edtEmpresa.Text));
end;

procedure TrptPedidoItemCancelado.edtCategoriaBeforeLookup(
  Sender: TObject);
begin
    inherited;

    edtCategoria.WhereAdicional.Text := 'nCdDepartamento = ' + IntToStr(frmMenu.ConvInteiro(edtDepartamento.Text));
end;

procedure TrptPedidoItemCancelado.edtSubCategoriaBeforeLookup(
  Sender: TObject);
begin
    inherited;

    edtSubCategoria.WhereAdicional.Text := 'nCdCategoria = ' + IntToStr(frmMenu.ConvInteiro(edtCategoria.Text));
end;

procedure TrptPedidoItemCancelado.qryLojaBeforeOpen(DataSet: TDataSet);
begin
    inherited;

    qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.ConvInteiro(edtEmpresa.Text);
end;

procedure TrptPedidoItemCancelado.qrySubCategoriaBeforeOpen(
  DataSet: TDataSet);
begin
    inherited;

    qrySubCategoria.Parameters.ParamByName('nCdCategoria').Value := frmMenu.ConvInteiro(edtCategoria.Text);
end;

procedure TrptPedidoItemCancelado.qryProdutoBeforeOpen(DataSet: TDataSet);
begin
    inherited;

    qryProduto.Parameters.ParamByName('nCdDepartamento').Value := frmMenu.ConvInteiro(edtDepartamento.Text);
    qryProduto.Parameters.ParamByName('nCdCategoria').Value    := frmMenu.ConvInteiro(edtCategoria.Text);
    qryProduto.Parameters.ParamByName('nCdSubCategoria').Value := frmMenu.ConvInteiro(edtSubCategoria.Text);
end;

procedure TrptPedidoItemCancelado.qryCategoriaBeforeOpen(
  DataSet: TDataSet);
begin
    inherited;

    qryCategoria.Parameters.ParamByName('nCdDepartamento').Value := frmMenu.ConvInteiro(edtDepartamento.Text);
end;

procedure TrptPedidoItemCancelado.edtProdutoBeforeLookup(Sender: TObject);
var
    nCdGrupo_Produto, nCdDepartamento, nCdCategoria, nCdSubCategoria : integer;
    cWhere : String;
begin
  inherited;

    cWhere := '';

    if not (qryGrupoProduto.IsEmpty) then
        cWhere := cWhere + 'nCdGrupo_Produto = ' + qryGrupoProdutonCdGrupoProduto.AsString + ' ';

    if not (qryDepartamento.IsEmpty) then
    begin
        if not (qryDepartamento.IsEmpty) then
            cWhere := cWhere + 'AND ';

        cWhere := cWhere + 'nCdDepartamento = ' + qryDepartamentonCdDepartamento.AsString + ' ';

        if not (qryCategoria.IsEmpty) then
        begin
            cWhere := cWhere + 'AND nCdCategoria = ' + qryCategorianCdCategoria.AsString + ' ';

            if not (qrySubCategoria.IsEmpty) then
                cWhere := cWhere + 'AND nCdSubCategoria = ' + qrySubCategorianCdSubCategoria.AsString;
        end;
    end;

    edtProduto.WhereAdicional.Text := cWhere;
end;

procedure TrptPedidoItemCancelado.qryMotivoCancelamentoBeforeOpen(
  DataSet: TDataSet);
begin
    inherited;

    qryMotivoCancelamento.Parameters.ParamByName('nCdTabTipoPedido').Value := frmMenu.ConvInteiro(edtTabTipoPedido.Text);
end;

procedure TrptPedidoItemCancelado.FormCreate(Sender: TObject);
begin
  inherited;

  { -- desabilita campo para empresas que n�o -- }
  if (frmMenu.LeParametro('VAREJO') <> 'S') then
      desativaMaskEdit(edtLoja);

end;

initialization
  RegisterClass(TrptPedidoItemCancelado);

end.
