unit fTipoReceb;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, DBCtrls, Mask, ImgList, DB, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh, DBGridEhGrouping, cxPC,
  cxControls, Grids, DBGrids, ToolCtrlsEh;

type
  TfrmTipoReceb = class(TfrmCadastro_Padrao)
    qryMasternCdTipoReceb: TIntegerField;
    qryMastercNmTipoReceb: TStringField;
    qryMastercFlgExigeNF: TIntegerField;
    qryMastercFlgPrecoMedio: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryTipoPedidoTipoReceb: TADOQuery;
    qryTipoPedidoTipoRecebnCdTipoReceb: TIntegerField;
    qryTipoPedidoTipoRecebnCdTipoPedido: TIntegerField;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTipoPedidoTipoRecebcNmTipoPedido: TStringField;
    dsTipoPedidoTipoReceb: TDataSource;
    qryMastercFlgRecebCego: TIntegerField;
    qryMastercFlgAguardConfirm: TIntegerField;
    qryMasternCdQuestionarioCQM: TIntegerField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    qryQuestionarioCQM: TADOQuery;
    qryQuestionarioCQMnCdQuestionarioCQM: TIntegerField;
    DBEdit4: TDBEdit;
    dsQuestionarioCQM: TDataSource;
    qryMastercFlgExigeCQM: TIntegerField;
    qryQuestionarioCQMcNmQuestionarioCQM: TStringField;
    qryMastercFlgDivValor: TIntegerField;
    qryMastercFlgDivIPI: TIntegerField;
    qryMastercFlgDivICMSSub: TIntegerField;
    qryMastercFlgDivPrazoEntrega: TIntegerField;
    qryMastercFlgDivPrazoPagto: TIntegerField;
    qryMastercFlgDivGrade: TIntegerField;
    qryMastercFlgAtuPrecoVenda: TIntegerField;
    qryTipoPedidoTipoRecebnCdTipoPedidoTipoReceb: TIntegerField;
    qryMastercFlgExigeNFe: TIntegerField;
    qryMastercFlgPermiteItemAD: TIntegerField;
    qryMastercFlgExigeInformacoesFiscais: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryMastercFlgEscriturarEntrada: TIntegerField;
    qryMastercFlgDivSemPedido: TIntegerField;
    cxTabSheet2: TcxTabSheet;
    Image2: TImage;
    chkExigeDadoNF: TDBCheckBox;
    chkRecebCego: TDBCheckBox;
    chkExigeDadosFiscais: TDBCheckBox;
    chkExigeNFe: TDBCheckBox;
    chkLivroEntrada: TDBCheckBox;
    chkAtualizaCustoEstoque: TDBCheckBox;
    chkAguardaEntradaEstoque: TDBCheckBox;
    chkPermiteAD: TDBCheckBox;
    chkAvaliaCQM: TDBCheckBox;
    chkAtualizaPrecoVenda: TDBCheckBox;
    chkDivSemPedido: TDBCheckBox;
    chkDivValor: TDBCheckBox;
    chkDivPrazoEntrega: TDBCheckBox;
    chkDivPrazoPagto: TDBCheckBox;
    chkDivIPI: TDBCheckBox;
    chkDivST: TDBCheckBox;
    chkDivGrade: TDBCheckBox;
    qryMastercFlgExigeImportacaoXMLNFe: TIntegerField;
    chkExigeImportacaoXML: TDBCheckBox;
    cxTabSheet3: TcxTabSheet;
    qryEmailTipoReceb: TADOQuery;
    qryEmailTipoRecebnCdEmailTipoReceb: TIntegerField;
    qryEmailTipoRecebnCdTipoReceb: TIntegerField;
    qryEmailTipoRecebcNmDestinatario: TStringField;
    qryEmailTipoRecebcEmail: TStringField;
    DBGridEh2: TDBGridEh;
    dsEmailTipoReceb: TDataSource;
    DBCheckBox1: TDBCheckBox;
    qryMastercFlgAtuPrecoWeb: TIntegerField;
    qryMastercFlgConfereItem: TIntegerField;
    DBCheckBox2: TDBCheckBox;
    DBCheckBox3: TDBCheckBox;
    qryMastercFlgAtivaSkuWeb: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTipoPedidoTipoRecebBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh2ColEnter(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure qryEmailTipoRecebBeforePost(DataSet: TDataSet);
    procedure DBGridEh2Enter(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTipoReceb: TfrmTipoReceb;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmTipoReceb.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'TIPORECEB' ;
  nCdTabelaSistema  := 48 ;
  nCdConsultaPadrao := 88 ;
end;

procedure TfrmTipoReceb.btIncluirClick(Sender: TObject);
begin
  inherited;
  qryMastercFlgExigeNF.Value := 1 ;
  DBEdit2.SetFocus ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmTipoReceb.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTipoPedidoTipoReceb.State = dsBrowse) then
             qryTipoPedidoTipoReceb.Edit ;

        if (qryTipoPedidoTipoReceb.State = dsInsert) or (qryTipoPedidoTipoReceb.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(98);

            If (nPK > 0) then
            begin
                qryTipoPedidoTipoRecebnCdTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoReceb.qryTipoPedidoTipoRecebBeforePost(
  DataSet: TDataSet);
begin

  if (qryTipoPedidoTipoRecebnCdTipoPedido.Value = 0) or (qryTipoPedidoTipoRecebcNmTipoPedido.Value = '') then
  begin
      MensagemAlerta('Selecione um tipo de pedido.') ;
      abort ;
  end ;

  qryTipoPedidoTipoRecebnCdTipoReceb.Value := qryMasternCdTipoReceb.Value ;

  if (qryTipoPedidoTipoReceb.State = dsInsert) then
      qryTipoPedidoTipoRecebnCdTipoPedidoTipoReceb.Value := frmMenu.fnProximoCodigo('TIPOPEDIDOTIPORECEB') ;

  inherited;


end;

procedure TfrmTipoReceb.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryQuestionarioCQM, qryMasternCdQuestionarioCQM.AsString) ;
  PosicionaQuery(qryTipoPedidoTipoReceb, qryMasternCdTipoReceb.AsString) ;
  PosicionaQuery(qryEmailTipoReceb, qryMasternCdTipoReceb.AsString) ;

end;

procedure TfrmTipoReceb.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryTipoPedidoTipoReceb.Close ;
  qryQuestionarioCQM.Close ;
  qryEmailTipoReceb.Close ;
end;

procedure TfrmTipoReceb.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) then
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;
          
end;

procedure TfrmTipoReceb.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o.') ;
      DBEdit2.SetFocus ;
      abort ;
  end ;

  if (DBEdit4.Text = '') and (qryMastercFlgExigeCQM.Value = 1) then
  begin
      MensagemAlerta('Selecione um question�rio para avalia��o do controle de qualidade.') ;
      DBedit3.SetFocus ;
      abort ;
  end ;

  if (qryMastercFlgExigeInformacoesFiscais.Value = 1) and (qryMastercFlgExigeNF.Value = 0) then
  begin
      MensagemAlerta('Para exigir os dados fiscais tamb�m � necess�rio exigir os dados da NF.') ;
      abort ;
  end;

  if (qryMastercFlgEscriturarEntrada.Value = 1) then
  begin

      if (qryMastercFlgExigeInformacoesFiscais.Value = 0) then
      begin

          MensagemAlerta('Para Registrar no Livro de Entrada � obrigat�rio selecionar a exig�ncia de informa��es fiscais.') ;
          abort ;

      end ;

  end ;

  inherited;

end;

procedure TfrmTipoReceb.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryTipoPedidoTipoReceb, qryMasternCdTipoReceb.AsString) ;

end;

procedure TfrmTipoReceb.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;
  qryQuestionarioCQM.Close ;
  qryTipoPedidoTipoReceb.Close ;
  qryEmailTipoReceb.Close ;
end;

procedure TfrmTipoReceb.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryQuestionarioCQM.Close ;
  PosicionaQuery(qryQuestionarioCQM, DBedit3.Text) ;

end;

procedure TfrmTipoReceb.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(181);

            If (nPK > 0) then
                qryMasternCdQuestionarioCQM.Value := nPK ;

        end ;

    end ;

  end ;

end;

procedure TfrmTipoReceb.DBGridEh2ColEnter(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) then
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;

end;

procedure TfrmTipoReceb.btCancelarClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmTipoReceb.qryEmailTipoRecebBeforePost(DataSet: TDataSet);
begin

  if ( trim( qryEmailTipoRecebcNmDestinatario.Value ) = '' ) then
  begin
      MensagemAlerta('Informe o nome do destinat�rio.') ;
      abort ;
  end ;

  if ( trim( qryEmailTipoRecebcEmail.Value ) = '' ) then
  begin
      MensagemAlerta('Informe o email do destinat�rio.') ;
      abort ;
  end ;

  inherited;

  qryEmailTipoRecebnCdTipoReceb.Value := qryMasternCdTipoReceb.Value ;

  if (qryEmailTipoReceb.State = dsInsert) then
      qryEmailTipoRecebnCdEmailTipoReceb.Value := frmMenu.fnProximoCodigo('EMAILTIPORECEB') ;

end;

procedure TfrmTipoReceb.DBGridEh2Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) then
      if (qryMaster.State <> dsBrowse) then
          qryMaster.Post ;

end;

initialization
    RegisterClass(TfrmTipoReceb) ;

end.
