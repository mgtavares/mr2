unit fOrdemCompraVarejo_Grade;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, ADODB, DB, GridsEh, DBGridEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmOrdemCompraVarejo_Grade = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    qryLoja: TADOQuery;
    qryLojanCdProduto: TIntegerField;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    qryLojanQtdeGrade: TIntegerField;
    qryLojanQtdeEstoque: TIntegerField;
    qryLojanVendaMarca: TBCDField;
    qryLojanVendaLinha: TBCDField;
    qryLojanVendaProduto: TBCDField;
    dsLoja: TDataSource;
    qryPreparaTemp: TADOQuery;
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nQtdeTotalPares : integer ;
    bGrade : boolean ;
    nCdItemPedido : integer ;
  end;

var
  frmOrdemCompraVarejo_Grade: TfrmOrdemCompraVarejo_Grade;

implementation

{$R *.dfm}

procedure TfrmOrdemCompraVarejo_Grade.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
//  inherited;

end;

procedure TfrmOrdemCompraVarejo_Grade.ToolButton1Click(Sender: TObject);
var
    iQtde, iQtdeLoja, i : integer ;
begin
  inherited;

  iQtdeLoja := 0 ;
  
  // conta a quantidade de grades nas lojas
  qryLoja.First;

  while not qryLoja.Eof do
  begin
      iQtdeLoja := iQtdeLoja + qryLojanQtdeGrade.Value ;

      qryLoja.Next ;
  end ;

  qryLoja.First ;

  if (iQtdeLoja = 0) then
  begin
      MensagemAlerta('Informe a quantidade de grades por loja.') ;
      abort ;
  end ;

  nQtdeTotalPares := iQtdeLoja ;

  close ;

end;

procedure TfrmOrdemCompraVarejo_Grade.FormShow(Sender: TObject);
begin
  inherited;

  qryPreparaTemp.Close ;
  qryPreparaTemp.ExecSQL;
  
  qryLoja.Close ;
  qryLoja.Parameters.ParamByName('nCdItemPedido').Value := nCdItemPedido ;
  qryLoja.Open ;

end;

end.
