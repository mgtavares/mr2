inherited rptPedidoItemCancelado: TrptPedidoItemCancelado
  Left = 358
  Top = 175
  Width = 761
  Height = 472
  Caption = 'Rel. Pedido/Item Cancelado'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 745
    Height = 405
  end
  object Label1: TLabel [1]
    Left = 66
    Top = 41
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 68
    Top = 89
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 17
    Top = 113
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo de Produtos'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 69
    Top = 209
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'Produto'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 43
    Top = 257
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Pedido 2'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [6]
    Left = 52
    Top = 233
    Width = 55
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo Pedido'
    FocusControl = DBEdit6
  end
  object Label7: TLabel [7]
    Left = 4
    Top = 281
    Width = 103
    Height = 13
    Alignment = taRightJustify
    Caption = 'Motivo Cancelamento'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [8]
    Left = 71
    Top = 305
    Width = 36
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo'
    FocusControl = DBEdit5
  end
  object Label9: TLabel [9]
    Left = 198
    Top = 305
    Width = 6
    Height = 13
    Caption = #224
    FocusControl = DBEdit5
  end
  object Label10: TLabel [10]
    Left = 87
    Top = 65
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
    FocusControl = DBEdit1
  end
  object Label11: TLabel [11]
    Left = 38
    Top = 137
    Width = 69
    Height = 13
    Alignment = taRightJustify
    Caption = 'Departamento'
    FocusControl = DBEdit1
  end
  object Label12: TLabel [12]
    Left = 60
    Top = 161
    Width = 47
    Height = 13
    Alignment = taRightJustify
    Caption = 'Categoria'
    FocusControl = DBEdit1
  end
  object Label13: TLabel [13]
    Left = 39
    Top = 185
    Width = 68
    Height = 13
    Alignment = taRightJustify
    Caption = 'Sub Categoria'
    FocusControl = DBEdit1
  end
  inherited ToolBar1: TToolBar
    Width = 745
    TabOrder = 15
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object RadioGroup1: TRadioGroup [15]
    Left = 110
    Top = 329
    Width = 379
    Height = 56
    Caption = 'Exibir'
    Columns = 2
    Items.Strings = (
      'Somente Pedido Venda'
      'Somente Pedido Compra'
      'Todos os Pedidos de Sa'#237'da'
      'Todos os Pedidos de Entrada')
    TabOrder = 13
  end
  object RadioGroup2: TRadioGroup [16]
    Left = 494
    Top = 329
    Width = 235
    Height = 56
    Caption = 'Op'#231#245'es de Visualiza'#231#227'o'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      'Relat'#243'rio'
      'Tela'
      'Planilha')
    TabOrder = 14
  end
  object edtEmpresa: TER2LookupMaskEdit [17]
    Left = 110
    Top = 33
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    OnBeforePosicionaQry = edtEmpresaBeforePosicionaQry
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  object edtTerceiro: TER2LookupMaskEdit [18]
    Left = 110
    Top = 81
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 2
    Text = '         '
    CodigoLookup = 17
    QueryLookup = qryTerceiro
  end
  object edtGrupoProduto: TER2LookupMaskEdit [19]
    Left = 110
    Top = 105
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 3
    Text = '         '
    CodigoLookup = 69
    QueryLookup = qryGrupoProduto
  end
  object edtProduto: TER2LookupMaskEdit [20]
    Left = 110
    Top = 201
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 7
    Text = '         '
    OnBeforeLookup = edtProdutoBeforeLookup
    CodigoLookup = 76
    QueryLookup = qryProduto
  end
  object edtTipoPedido2: TER2LookupMaskEdit [21]
    Left = 110
    Top = 249
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 9
    Text = '         '
    OnBeforeLookup = edtTipoPedido2BeforeLookup
    CodigoLookup = 54
    QueryLookup = qryTipoPedido
  end
  object edtMotivoCancelamento: TER2LookupMaskEdit [22]
    Left = 110
    Top = 273
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 10
    Text = '         '
    OnBeforeLookup = edtMotivoCancelamentoBeforeLookup
    CodigoLookup = 760
    QueryLookup = qryMotivoCancelamento
  end
  object MaskEdit1: TMaskEdit [23]
    Left = 110
    Top = 297
    Width = 81
    Height = 21
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 11
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [24]
    Left = 214
    Top = 297
    Width = 81
    Height = 21
    EditMask = '99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 12
    Text = '  /  /    '
  end
  object DBEdit1: TDBEdit [25]
    Tag = 1
    Left = 178
    Top = 33
    Width = 515
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = DataSource7
    TabOrder = 16
  end
  object DBEdit2: TDBEdit [26]
    Tag = 1
    Left = 178
    Top = 81
    Width = 515
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = DataSource8
    TabOrder = 17
  end
  object DBEdit3: TDBEdit [27]
    Tag = 1
    Left = 178
    Top = 105
    Width = 515
    Height = 21
    DataField = 'cNmGrupoProduto'
    DataSource = DataSource3
    TabOrder = 18
  end
  object DBEdit4: TDBEdit [28]
    Tag = 1
    Left = 178
    Top = 201
    Width = 515
    Height = 21
    DataField = 'cNmProduto'
    DataSource = DataSource4
    TabOrder = 19
  end
  object DBEdit5: TDBEdit [29]
    Tag = 1
    Left = 178
    Top = 249
    Width = 227
    Height = 21
    DataField = 'cNmTipoPedido'
    DataSource = DataSource5
    TabOrder = 20
  end
  object edtTabTipoPedido: TER2LookupMaskEdit [30]
    Left = 110
    Top = 225
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 8
    Text = '         '
    OnAfterPosicionaQry = edtTabTipoPedidoAfterPosicionaQry
    CodigoLookup = 55
    WhereAdicional.Strings = (
      'nCdTabTipoPedido IN (1,2)')
    QueryLookup = qryTabTipoPedido
  end
  object DBEdit6: TDBEdit [31]
    Tag = 1
    Left = 178
    Top = 225
    Width = 227
    Height = 21
    DataField = 'cNmTabTipoPedido'
    DataSource = DataSource1
    TabOrder = 21
  end
  object DBEdit7: TDBEdit [32]
    Tag = 1
    Left = 178
    Top = 273
    Width = 227
    Height = 21
    DataField = 'cNmMotivoCancSaldoPed'
    DataSource = DataSource6
    TabOrder = 22
  end
  object edtLoja: TER2LookupMaskEdit [33]
    Left = 110
    Top = 57
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 1
    Text = '         '
    OnBeforeLookup = edtLojaBeforeLookup
    CodigoLookup = 59
    QueryLookup = qryLoja
  end
  object edtDepartamento: TER2LookupMaskEdit [34]
    Left = 110
    Top = 129
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 4
    Text = '         '
    CodigoLookup = 129
    QueryLookup = qryDepartamento
  end
  object edtCategoria: TER2LookupMaskEdit [35]
    Left = 110
    Top = 153
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 5
    Text = '         '
    OnBeforeLookup = edtCategoriaBeforeLookup
    CodigoLookup = 46
    QueryLookup = qryCategoria
  end
  object edtSubCategoria: TER2LookupMaskEdit [36]
    Left = 110
    Top = 177
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 6
    Text = '         '
    OnBeforeLookup = edtSubCategoriaBeforeLookup
    CodigoLookup = 47
    QueryLookup = qrySubCategoria
  end
  object DBEdit8: TDBEdit [37]
    Tag = 1
    Left = 178
    Top = 57
    Width = 515
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 23
  end
  object DBEdit9: TDBEdit [38]
    Tag = 1
    Left = 178
    Top = 129
    Width = 515
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = dsDepartamento
    TabOrder = 24
  end
  object DBEdit10: TDBEdit [39]
    Tag = 1
    Left = 178
    Top = 153
    Width = 515
    Height = 21
    DataField = 'cNmCategoria'
    DataSource = dsCategoria
    TabOrder = 25
  end
  object DBEdit11: TDBEdit [40]
    Tag = 1
    Left = 178
    Top = 177
    Width = 515
    Height = 21
    DataField = 'cNmSubCategoria'
    DataSource = dsSubCategoria
    TabOrder = 26
  end
  inherited ImageList1: TImageList
    Left = 96
    Top = 360
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nPK'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nPK')
    Left = 174
    Top = 361
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryGrupoProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdGrupoProduto'
      '      ,cNmGrupoProduto'
      '  FROM GrupoProduto'
      ' WHERE nCdGrupoProduto = :nPK')
    Left = 270
    Top = 361
    object qryGrupoProdutonCdGrupoProduto: TIntegerField
      FieldName = 'nCdGrupoProduto'
    end
    object qryGrupoProdutocNmGrupoProduto: TStringField
      FieldName = 'cNmGrupoProduto'
      Size = 50
    end
  end
  object qryMotivoCancelamento: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryMotivoCancelamentoBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdTabTipoPedido'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMotivoCancSaldoPed'
      '      ,cNmMotivoCancSaldoPed '
      '  FROM MotivoCancSaldoPed'
      ' WHERE nCdMotivoCancSaldoPed = :nPK'
      '   AND nCdTabTipoPedido      = :nCdTabTipoPedido')
    Left = 494
    Top = 361
    object qryMotivoCancelamentonCdMotivoCancSaldoPed: TIntegerField
      FieldName = 'nCdMotivoCancSaldoPed'
    end
    object qryMotivoCancelamentocNmMotivoCancSaldoPed: TStringField
      FieldName = 'cNmMotivoCancSaldoPed'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '  FROM Terceiro'
      'WHERE nCdTerceiro = :nPK'
      '')
    Left = 238
    Top = 361
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object qryProduto: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryProdutoBeforeOpen
    Parameters = <
      item
        Name = 'nCdGrupoProduto'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdDepartamento'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdCategoria'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdSubCategoria'
        Size = -1
        Value = Null
      end
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdGrupoProduto int'
      '       ,@nCdDepartamento int'
      '       ,@nCdCategoria    int'
      '       ,@nCdSubCategoria int'
      ''
      'SET @nCdGrupoProduto = :nCdGrupoProduto'
      'SET @nCdDepartamento = :nCdDepartamento'
      'SET @nCdCategoria    = :nCdCategoria'
      'SET @nCdSubCategoria = :nCdSubCategoria'
      ''
      'SELECT nCdProduto'
      '      ,cNmProduto '
      '  FROM Produto'
      ' WHERE nCdProduto = :nPK'
      
        '   AND ((@nCdGrupoProduto = 0) OR (nCdGrupo_Produto = @nCdGrupoP' +
        'roduto))'
      
        '   AND ((@nCdDepartamento = 0) OR (nCdDepartamento  = @nCdDepart' +
        'amento))'
      
        '   AND ((@nCdCategoria    = 0) OR (nCdCategoria     = @nCdCatego' +
        'ria))'
      
        '   AND ((@nCdSubCategoria = 0) OR (nCdSubCategoria  = @nCdSubCat' +
        'egoria))')
    Left = 398
    Top = 361
    object qryProdutonCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryProdutocNmProduto: TStringField
      FieldName = 'cNmProduto'
      Size = 150
    end
  end
  object qryTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTipoPedido'
      '      ,cNmTipoPedido '
      '  FROM TipoPedido'
      ' WHERE nCdTipoPedido = :nPK')
    Left = 462
    Top = 361
    object qryTipoPedidonCdTipoPedido: TIntegerField
      FieldName = 'nCdTipoPedido'
    end
    object qryTipoPedidocNmTipoPedido: TStringField
      FieldName = 'cNmTipoPedido'
      Size = 50
    end
  end
  object DataSource3: TDataSource
    DataSet = qryGrupoProduto
    Left = 278
    Top = 393
  end
  object DataSource4: TDataSource
    DataSet = qryProduto
    Left = 406
    Top = 393
  end
  object DataSource5: TDataSource
    DataSet = qryTipoPedido
    Left = 470
    Top = 393
  end
  object DataSource6: TDataSource
    DataSet = qryMotivoCancelamento
    Left = 502
    Top = 393
  end
  object DataSource7: TDataSource
    DataSet = qryEmpresa
    Left = 182
    Top = 393
  end
  object DataSource8: TDataSource
    DataSet = qryTerceiro
    Left = 246
    Top = 393
  end
  object qryTabTipoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTabTipoPedido'
      '      ,cNmTabTipoPedido '
      '  FROM TabTipoPedido'
      ' WHERE nCdTabTipoPedido = :nPK')
    Left = 430
    Top = 361
    object qryTabTipoPedidonCdTabTipoPedido: TIntegerField
      FieldName = 'nCdTabTipoPedido'
    end
    object qryTabTipoPedidocNmTabTipoPedido: TStringField
      FieldName = 'cNmTabTipoPedido'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTabTipoPedido
    Left = 438
    Top = 393
  end
  object ER2Excel1: TER2Excel
    Titulo = 'Rel. Pedido/Item Cancelado'
    AutoSizeCol = True
    Background = clWhite
    Transparent = False
    OnBeforeExport = ER2Excel1BeforeExport
    OnAfterExport = ER2Excel1AfterExport
    Left = 544
    Top = 368
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryLojaBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja    = :nPK'
      '   AND nCdEmpresa = :nCdEmpresa')
    Left = 206
    Top = 361
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 214
    Top = 393
  end
  object dsDepartamento: TDataSource
    DataSet = qryDepartamento
    Left = 310
    Top = 393
  end
  object dsCategoria: TDataSource
    DataSet = qryCategoria
    Left = 342
    Top = 393
  end
  object dsSubCategoria: TDataSource
    DataSet = qrySubCategoria
    Left = 374
    Top = 393
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento'
      '      ,cNmDepartamento'
      '  FROM Departamento'
      ' WHERE nCdDepartamento = :nPK')
    Left = 302
    Top = 361
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qrySubCategoria: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qrySubCategoriaBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCategoria'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSubCategoria'
      '      ,cNmSubCategoria'
      '  FROM SubCategoria'
      ' WHERE nCdSubCategoria = :nPK'
      '   AND nCdCategoria    = :nCdCategoria')
    Left = 366
    Top = 361
    object qrySubCategorianCdSubCategoria: TIntegerField
      FieldName = 'nCdSubCategoria'
    end
    object qrySubCategoriacNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
  end
  object qryCategoria: TADOQuery
    Connection = frmMenu.Connection
    BeforeOpen = qryCategoriaBeforeOpen
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdDepartamento'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCategoria'
      '      ,cNmCategoria'
      '  FROM Categoria'
      ' WHERE nCdCategoria    = :nPK'
      '   AND nCdDepartamento = :nCdDepartamento'
      '')
    Left = 334
    Top = 361
    object qryCategorianCdCategoria: TIntegerField
      FieldName = 'nCdCategoria'
    end
    object qryCategoriacNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
  end
end
