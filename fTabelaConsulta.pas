unit fTabelaConsulta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ADODB, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, DBCtrls, Mask, GridsEh, DBGridEh, ImgList, DBGridEhGrouping,
  cxPC, cxControls, ToolCtrlsEh;

type
  TfrmTabelaConsulta = class(TfrmCadastro_Padrao)
    qryMasternCdTabelaConsulta: TIntegerField;
    qryMastercNmTabelaConsulta: TStringField;
    qryMasterComandoSQL: TMemoField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DBMemo1: TDBMemo;
    Label3: TLabel;
    qryFiltroTabelaConsulta: TADOQuery;
    dsFiltroTabelaConsulta: TDataSource;
    qryFiltroTabelaConsultanCdTabelaConsulta: TIntegerField;
    qryFiltroTabelaConsultacNmCampoTabela: TStringField;
    qryFiltroTabelaConsultacTipoDado: TStringField;
    qryFiltroTabelaConsultacNmCampoTela: TStringField;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    procedure btIncluirClick(Sender: TObject);
    procedure qryFiltroTabelaConsultaBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTabelaConsulta: TfrmTabelaConsulta;

implementation

{$R *.dfm}

uses
  fMenu;

procedure TfrmTabelaConsulta.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit1.SetFocus;
end;

procedure TfrmTabelaConsulta.qryFiltroTabelaConsultaBeforePost(
  DataSet: TDataSet);
begin
  if (qryMaster.IsEmpty) then
      Exit;

  if (qryMaster.State = dsInsert) then
      qryMaster.Post;

  inherited;

  qryFiltroTabelaConsultanCdTabelaConsulta.Value := qryMasternCdTabelaConsulta.Value;
end;

procedure TfrmTabelaConsulta.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryFiltroTabelaConsulta.Close;
  PosicionaQuery(qryFiltroTabelaConsulta, qryMasternCdTabelaConsulta.AsString);
end;

procedure TfrmTabelaConsulta.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryFiltroTabelaConsulta.Close ;

end;

procedure TfrmTabelaConsulta.FormShow(Sender: TObject);
begin
  cNmTabelaMaster   := 'TABELACONSULTA' ;
  nCdTabelaSistema  := 5 ;
  nCdConsultaPadrao := 7 ;
  bCodigoAutomatico := False;
  inherited;

  DBMemo1.Font.Name := 'Consolas';
end;

procedure TfrmTabelaConsulta.ToolButton10Click(Sender: TObject);
var
  objForm : TForm;
  cScript : TMemo;
begin
  inherited;

  if not (qryMaster.Active) then
      exit;

   objForm := TForm.Create(nil);

   objForm.Width    := 650;
   objForm.Height   := 350;
   objForm.Position := poDesktopCenter;
   objForm.Caption  := 'Script SQL';

   cScript            := TMemo.Create(nil);
   cScript.Parent     := objForm;
   cScript.Align      := alClient;
   cScript.ScrollBars := ssBoth;
   cScript.Font.Name  := 'Consolas';

   cScript.Lines.Add('INSERT INTO TabelaConsulta (nCdTabelaConsulta');
   cScript.Lines.Add('                           ,cNmTabelaConsulta');
   cScript.Lines.Add('                           ,ComandoSQL)');
   cScript.Lines.Add('                    VALUES (' + qryMasternCdTabelaConsulta.AsString);
   cScript.Lines.Add('                           ,' + #39 + qryMastercNmTabelaConsulta.Value + #39);
   cScript.Lines.Add('                           ,' + #39 + qryMasterComandoSQL.AsString + #39 + ')');

   if not (qryFiltroTabelaConsulta.Eof) then
   begin

       qryFiltroTabelaConsulta.First;

       while not qryFiltroTabelaConsulta.Eof do
       begin
           cScript.Lines.Add('');
           cScript.Lines.Add('INSERT INTO FiltroTabelaConsulta (nCdTabelaConsulta');
           cScript.Lines.Add('                                 ,cNmCampoTabela');
           cScript.Lines.Add('                                 ,cTipoDado');
           cScript.Lines.Add('                                 ,cNmCampoTela)');
           cScript.Lines.Add('                       VALUES (' + qryFiltroTabelaConsultanCdTabelaConsulta.AsString );
           cScript.Lines.Add('                              ,' + #39 + qryFiltroTabelaConsultacNmCampoTabela.AsString + #39);
           cScript.Lines.Add('                              ,' + #39 + qryFiltroTabelaConsultacTipoDado.AsString + #39);
           cScript.Lines.Add('                              ,' + #39 + qryFiltroTabelaConsultacNmCampoTela.AsString + #39 + ')');

           qryFiltroTabelaConsulta.Next;
       end;

   end;

   showForm(objForm,True);
end;

procedure TfrmTabelaConsulta.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Informe o c�digo da consulta.');
      DBEdit1.SetFocus;
      Abort;
  end;

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o da consulta.');
      DBEdit2.SetFocus;
      Abort;
  end;

  if (DBMemo1.Lines.Text = '') then
  begin
      MensagemAlerta('Informe o comando a ser executado na consulta.');
      DBMemo1.SetFocus;
      Abort;
  end;

  inherited;
end;

procedure TfrmTabelaConsulta.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;

  qryFiltroTabelaConsulta.Close;
end;

initialization
    RegisterClass(tFrmTabelaConsulta) ;

end.
