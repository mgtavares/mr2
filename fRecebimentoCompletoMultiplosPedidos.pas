unit fRecebimentoCompletoMultiplosPedidos;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  DBGridEhGrouping, GridsEh, DBGridEh, DB, ADODB, StdCtrls, Mask, DBCtrls;

type
  TfrmRecebimentoCompletoMultiplosPedidos = class(TfrmProcesso_Padrao)
    DBGridEh1: TDBGridEh;
    qryItemPedidoItemRecebimento: TADOQuery;
    dsItemPedidoItemRecebimento: TDataSource;
    qryItemPedidoItemRecebimentonCdItemRecebimentoItemPedido: TIntegerField;
    qryItemPedidoItemRecebimentonCdItemRecebimento: TIntegerField;
    qryItemPedidoItemRecebimentonCdItemPedido: TIntegerField;
    qryItemPedidoItemRecebimentonQtde: TBCDField;
    qryItemPedidoItemRecebimentonCdPedido: TIntegerField;
    qryItemPedidoItemRecebimentodDtPedido: TDateField;
    qryItemPedidoItemRecebimentocNmTerceiro: TStringField;
    qryItemPedidoItemRecebimentocNmGrupoEconomico: TStringField;
    qryItemPedidoItemRecebimentonSaldoPedido: TFloatField;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    qryItemPedido: TADOQuery;
    qryItemPedidonCdPedido: TIntegerField;
    qryItemPedidodDtPedido: TDateTimeField;
    qryItemPedidocNmTerceiro: TStringField;
    qryItemPedidocNmGrupoEconomico: TStringField;
    qryItemPedidonQtdeSaldo: TBCDField;
    qryAux: TADOQuery;
    qryItemRecebimento: TADOQuery;
    qryItemRecebimentocCdProduto: TStringField;
    qryItemRecebimentocNmItem: TStringField;
    qryItemRecebimentonQtde: TBCDField;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    procedure qryItemPedidoItemRecebimentoBeforePost(DataSet: TDataSet);
    procedure qryItemPedidoItemRecebimentoCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdItemRecebimento , nCdProduto , nCdRecebimento : integer ;
    nCdTerceiro , nCdTipoReceb , nCdLoja : integer ;
    nQtdeReceb : double ;
    bSomenteLeitura : boolean ;
  end;

var
  frmRecebimentoCompletoMultiplosPedidos: TfrmRecebimentoCompletoMultiplosPedidos;

implementation

uses fMenu, fConsultaItemPedidoAberto;

{$R *.dfm}

var
   objConsultaItemPedidoAberto : TfrmConsultaItemPedidoAberto ;

procedure TfrmRecebimentoCompletoMultiplosPedidos.qryItemPedidoItemRecebimentoBeforePost(
  DataSet: TDataSet);
begin

  if (qryItemPedidoItemRecebimentonCdItemPedido.Value = 0) then
  begin
      MensagemAlerta('Utilize o bot�o Selecionar Pedido para incluir pedidos nesta lista.') ;
      abort ;
  end ;

  if (qryItemPedidoItemRecebimentonQtde.Value <= 0) then
  begin
      MensagemAlerta('Quantidade inv�lida.') ;
      abort ;
  end ;

  if (qryItemPedidoItemRecebimento.State = dsEdit) then
  begin

      if ((qryItemPedidoItemRecebimentonQtde.Value - strToFloat(qryItemPedidoItemRecebimentonQtde.OldValue) + DBGridEh1.SumList.SumCollection.Items[2].SumValue) > qryItemRecebimentonQtde.Value ) then
      begin
          MensagemAlerta('A soma da quantidade selecionada n�o pode ser maior que a quantidade recebida na nota fiscal. Verifique.') ;
          abort ;
      end ;

  end
  else
  begin

      if ((qryItemPedidoItemRecebimentonQtde.Value + DBGridEh1.SumList.SumCollection.Items[2].SumValue) > qryItemRecebimentonQtde.Value ) then
      begin
          MensagemAlerta('A soma da quantidade selecionada n�o pode ser maior que a quantidade recebida na nota fiscal. Verifique.') ;
          abort ;
      end ;

  end ;


  inherited;

  if (qryItemPedidoItemRecebimento.State = dsInsert) then
  begin
      qryItemPedidoItemRecebimentonCdItemRecebimentoItemPedido.Value := frmMenu.fnProximoCodigo('ITEMRECEBIMENTOITEMPEDIDO') ;
      qryItemPedidoItemRecebimentonCdItemRecebimento.Value           := nCdItemRecebimento ;
  end ;

end;

procedure TfrmRecebimentoCompletoMultiplosPedidos.qryItemPedidoItemRecebimentoCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  if (qryItemPedidoItemRecebimentocNmTerceiro.Value = '') then
  begin

      qryItemPedido.Close;
      PosicionaQuery( qryItemPedido , qryItemPedidoItemRecebimentonCdItemPedido.AsString ) ;

      if ( not qryItemPedido.isEmpty ) then
      begin

          qryItemPedidoItemRecebimentonCdPedido.Value         := qryItemPedidonCdPedido.Value ;
          qryItemPedidoItemRecebimentocNmTerceiro.Value       := qryItemPedidocNmTerceiro.Value ;
          qryItemPedidoItemRecebimentocNmGrupoEconomico.Value := qryItemPedidocNmGrupoEconomico.Value ;
          qryItemPedidoItemRecebimentodDtPedido.Value         := qryItemPedidodDtPedido.Value ;
          qryItemPedidoItemRecebimentonSaldoPedido.Value      := qryItemPedidonQtdeSaldo.Value ;

      end ;

  end ;

end;

procedure TfrmRecebimentoCompletoMultiplosPedidos.FormShow(
  Sender: TObject);
begin
  inherited;

  objConsultaItemPedidoAberto := TfrmConsultaItemPedidoAberto.Create( Self ) ;

  qryItemRecebimento.Close;
  PosicionaQuery( qryItemRecebimento , intToStr( nCdItemRecebimento ) ) ;

  qryItemPedidoItemRecebimento.Close;
  PosicionaQuery( qryItemPedidoItemRecebimento , intToStr( nCdItemRecebimento ) ) ;

  DBGridEh1.ReadOnly := bSomenteLeitura ;
  
  DBGridEh1.SetFocus;

end;

procedure TfrmRecebimentoCompletoMultiplosPedidos.ToolButton5Click(
  Sender: TObject);
begin
  inherited;

  objConsultaItemPedidoAberto.nCdTerceiro    := nCdTerceiro ;
  objConsultaItemPedidoAberto.nCdTipoReceb   := nCdTipoReceb ;
  objConsultaItemPedidoAberto.nCdLoja        := nCdLoja ;
  objConsultaItemPedidoAberto.nCdRecebimento := nCdRecebimento ;
  objConsultaItemPedidoAberto.nCdPedido      := 0 ;
  objConsultaItemPedidoAberto.nCdProduto     := nCdProduto ;

  showForm( objConsultaItemPedidoAberto , FALSE ) ;

  if (objConsultaItemPedidoAberto.qryItemPedido.Active) and (objConsultaItemPedidoAberto.bSelecionado) and (objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.Value > 0) then
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM ItemRecebimentoItemPedido WHERE nCdItemRecebimento = ' + intToStr( nCdItemRecebimento ) + ' AND nCdItemPedido = ' + objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.asString ) ;
      qryAux.Open;

      if (not qryAux.IsEmpty) then
      begin
          MensagemAlerta('O Item de pedido selecionado j� est� relacionado na lista de pedidos vinculados.') ;
          qryAux.Close;
          abort ;
      end ;

      qryAux.Close;

      qryItemPedidoItemRecebimento.Insert ;
      qryItemPedidoItemRecebimentonCdItemPedido.Value := objConsultaItemPedidoAberto.qryItemPedidonCdItemPedido.Value ;

      if ((nQtdeReceb - DBGridEh1.SumList.SumCollection.Items[2].SumValue) >= objConsultaItemPedidoAberto.qryItemPedidonQtde.Value) then
          qryItemPedidoItemRecebimentonQtde.Value := objConsultaItemPedidoAberto.qryItemPedidonQtde.Value
      else if ((nQtdeReceb - DBGridEh1.SumList.SumCollection.Items[2].SumValue) > 0) then
               qryItemPedidoItemRecebimentonQtde.Value := (nQtdeReceb - DBGridEh1.SumList.SumCollection.Items[2].SumValue)
           else qryItemPedidoItemRecebimentonQtde.Value := 0 ;

      qryItemPedidoItemRecebimento.Post ;

      DBGridEh1.Col := 7 ;
      DBGridEh1.SetFocus;

      if (qryItemRecebimentonQtde.Value > DBGridEh1.SumList.SumCollection.Items[2].SumValue) then
          ToolButton5.Click;

  end ;

end;

procedure TfrmRecebimentoCompletoMultiplosPedidos.FormKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

procedure TfrmRecebimentoCompletoMultiplosPedidos.FormKeyUp(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  {inherited;}

end;

end.
