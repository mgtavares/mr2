unit fMotBloqPed;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh;

type
  TfrmMotBloqPed = class(TfrmCadastro_Padrao)
    qryMasternCdMotBloqPed: TIntegerField;
    qryMastercNmMotBloqPed: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    qryTipoPedidoMotBloqPed: TADOQuery;
    dsTipoPedidoMotBloqPed: TDataSource;
    qryTipoPedido: TADOQuery;
    qryTipoPedidoMotBloqPednCdMotBloqPed: TIntegerField;
    qryTipoPedidoMotBloqPednCdTipoPedido: TIntegerField;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTipoPedidoMotBloqPedcNmTipoPedido: TStringField;
    DBGridEh1: TDBGridEh;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryTipoPedidoMotBloqPedBeforePost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btSalvarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMotBloqPed: TfrmMotBloqPed;

implementation

uses fLookup_Padrao;

{$R *.dfm}
procedure TfrmMotBloqPed.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'MOTBLOQPED' ;
  nCdTabelaSistema  := 49 ;
  nCdConsultaPadrao := 100 ;
end;

procedure TfrmMotBloqPed.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBEdit2.SetFocus ;
end;

procedure TfrmMotBloqPed.qryTipoPedidoMotBloqPedBeforePost(
  DataSet: TDataSet);
begin

  if (qryTipoPedidoMotBloqPednCdTipoPedido.Value = 0) or (qryTipoPedidoMotBloqPedcNmTipoPedido.Value = '') then
  begin
      MensagemAlerta('Selecione o tipo de pedido.') ;
      abort ;
  end ;

  qryTipoPedidoMotBloqPednCdMotBloqPed.Value := qryMasternCdMotBloqPed.Value ;
  inherited;

end;

procedure TfrmMotBloqPed.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMastercNmMotBloqPed.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o do motivo.') ;
      DBEdit2.SetFocus ;
      abort ;
  end ;

  inherited;

end;

procedure TfrmMotBloqPed.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryTipoPedidoMotBloqPed, qryMasternCdMotBloqPed.AsString) ;
  
end;

procedure TfrmMotBloqPed.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryTipoPedidoMotBloqPed.Close ;
  
end;

procedure TfrmMotBloqPed.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.State <> dsBrowse) then
      qryMaster.Post ;

end;

procedure TfrmMotBloqPed.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryTipoPedidoMotBloqPed, qryMasternCdMotBloqPed.AsString) ;
end;

procedure TfrmMotBloqPed.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryTipoPedidoMotBloqPed.State = dsBrowse) then
             qryTipoPedidoMotBloqPed.Edit ;
        

        if (qryTipoPedidoMotBloqPed.State = dsInsert) or (qryTipoPedidoMotBloqPed.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(71);

            If (nPK > 0) then
            begin
                qryTipoPedidoMotBloqPednCdTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmMotBloqPed.btSalvarClick(Sender: TObject);
begin
  inherited;
  qryTipoPedidoMotBloqPed.Close ;
end;

initialization
    RegisterClass(TfrmMotBloqPed) ;

end.
