unit fServidorReplicacao_FilaTransmissao;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, cxPC,
  cxControls, DB, ADODB, GridsEh, DBGridEh, Menus, cxStyles, cxCustomData,
  cxGraphics, cxFilter, cxData, cxDataStorage, cxEdit, cxDBData,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGridLevel,
  cxClasses, cxGridCustomView, cxGrid, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmServidorReplicacao_FilaTransmissao = class(TfrmProcesso_Padrao)
    qryTempRegistro: TADOQuery;
    dsTempRegistro: TDataSource;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxTabSheet2: TcxTabSheet;
    qryTempRegistronCdTempRegistro: TAutoIncField;
    qryTempRegistrocTabela: TStringField;
    qryTempRegistrocFlgAcao: TStringField;
    qryTempRegistrocNmServidor: TStringField;
    qryErros: TADOQuery;
    AutoIncField1: TAutoIncField;
    StringField1: TStringField;
    StringField2: TStringField;
    StringField3: TStringField;
    dsErros: TDataSource;
    qryErrosdDtUltTentativa: TDateTimeField;
    qryErrosiQtdeTentativas: TIntegerField;
    qryErroscFlgBroken: TIntegerField;
    qryErroscMsgErro: TMemoField;
    DBGridEh2: TDBGridEh;
    PopupMenu1: TPopupMenu;
    Atualizar1: TMenuItem;
    PopupMenu2: TPopupMenu;
    MenuItem1: TMenuItem;
    qryTempRegistrodDtInclusao: TDateTimeField;
    qryErrosdDtInclusao: TDateTimeField;
    ExibirErroCompleto1: TMenuItem;
    cxTabSheet3: TcxTabSheet;
    PopupMenu3: TPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    qryDescartados: TADOQuery;
    AutoIncField2: TAutoIncField;
    StringField4: TStringField;
    StringField5: TStringField;
    StringField6: TStringField;
    DateTimeField1: TDateTimeField;
    IntegerField3: TIntegerField;
    IntegerField4: TIntegerField;
    MemoField1: TMemoField;
    DateTimeField2: TDateTimeField;
    dsDescartados: TDataSource;
    Retransmitir1: TMenuItem;
    qryAux: TADOQuery;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    cxGridDBTableView1nCdTempRegistro: TcxGridDBColumn;
    cxGridDBTableView1cTabela: TcxGridDBColumn;
    cxGridDBTableView1iIDRegistro: TcxGridDBColumn;
    cxGridDBTableView1cFlgAcao: TcxGridDBColumn;
    cxGridDBTableView1cNmServidor: TcxGridDBColumn;
    cxGridDBTableView1dDtInclusao: TcxGridDBColumn;
    cxGrid1: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView2nCdTempRegistro: TcxGridDBColumn;
    cxGridDBTableView2cTabela: TcxGridDBColumn;
    cxGridDBTableView2iIDRegistro: TcxGridDBColumn;
    cxGridDBTableView2cFlgAcao: TcxGridDBColumn;
    cxGridDBTableView2cNmServidor: TcxGridDBColumn;
    cxGridDBTableView2dDtUltTentativa: TcxGridDBColumn;
    cxGridDBTableView2iQtdeTentativas: TcxGridDBColumn;
    cxGridDBTableView2cMsgErro: TcxGridDBColumn;
    cxGridDBTableView2dDtInclusao: TcxGridDBColumn;
    cxTabSheet4: TcxTabSheet;
    cxGrid3: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridLevel3: TcxGridLevel;
    qryPendentesServidor: TADOQuery;
    dsPendentesServidor: TDataSource;
    qryPendentesServidornCdServidor: TIntegerField;
    qryPendentesServidorcNmServidor: TStringField;
    qryPendentesServidoriQtdePendente: TIntegerField;
    cxGridDBTableView3nCdServidor: TcxGridDBColumn;
    cxGridDBTableView3cNmServidor: TcxGridDBColumn;
    cxGridDBTableView3iQtdePendente: TcxGridDBColumn;
    PopupMenu4: TPopupMenu;
    MenuItem4: TMenuItem;
    cxTabSheet5: TcxTabSheet;
    qryPendentesTabela: TADOQuery;
    dsPendentesTabela: TDataSource;
    qryPendentesTabelanCdTabelaSistema: TIntegerField;
    qryPendentesTabelacTabela: TStringField;
    qryPendentesTabelacNmTabela: TStringField;
    qryPendentesTabelaiQtdePendente: TIntegerField;
    cxGrid4: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridLevel4: TcxGridLevel;
    cxGridDBTableView4nCdTabelaSistema: TcxGridDBColumn;
    cxGridDBTableView4cTabela: TcxGridDBColumn;
    cxGridDBTableView4cNmTabela: TcxGridDBColumn;
    cxGridDBTableView4iQtdePendente: TcxGridDBColumn;
    PopupMenu5: TPopupMenu;
    MenuItem5: TMenuItem;
    qryTempRegistroiIDRegistro: TLargeintField;
    qryDescartadosiIDRegistro: TLargeintField;
    qryErrosiIDRegistro: TLargeintField;
    procedure FormShow(Sender: TObject);
    procedure Atualizar1Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure ExibirErroCompleto1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure Retransmitir1Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmServidorReplicacao_FilaTransmissao: TfrmServidorReplicacao_FilaTransmissao;

implementation

{$R *.dfm}

uses
  fMenu;

procedure TfrmServidorReplicacao_FilaTransmissao.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.Align           := alClient ;
  cxPageControl1.ActivePageIndex := 0 ;

  DBGridEh2.Align      := alClient ;
  
end;

procedure TfrmServidorReplicacao_FilaTransmissao.Atualizar1Click(
  Sender: TObject);
begin
  inherited;

  qryTempRegistro.Close;
  qryTempRegistro.Open ;
  
end;

procedure TfrmServidorReplicacao_FilaTransmissao.MenuItem1Click(
  Sender: TObject);
begin
  inherited;

  qryErros.Close;
  qryErros.Open;

end;

procedure TfrmServidorReplicacao_FilaTransmissao.ExibirErroCompleto1Click(
  Sender: TObject);
begin
  inherited;

  if not qryErros.Eof then
      ShowMessage(qryErroscMsgErro.Value) ;
  
end;

procedure TfrmServidorReplicacao_FilaTransmissao.MenuItem2Click(
  Sender: TObject);
begin
  inherited;

  qryDescartados.Close;
  qryDescartados.Open ;
  
end;

procedure TfrmServidorReplicacao_FilaTransmissao.MenuItem3Click(
  Sender: TObject);
begin
  inherited;

  if not qryDescartados.eof then
      ShowMessage(MemoField1.Value) ;

end;

procedure TfrmServidorReplicacao_FilaTransmissao.Retransmitir1Click(
  Sender: TObject);
begin
  inherited;

  if not qryDescartados.Eof then
  begin

      if (MessageDlg('Confirma a retransmissão deste registro ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE TempRegistro SET iQtdeTentativas = 0, cFlgErro = 0, cFlgBroken = 0, nCdPackageRepl = NULL WHERE nCdTempRegistro = ' + AutoIncField2.AsString) ;
      qryAux.ExecSQL;

      qryDescartados.Requery();

  end ;

end;

procedure TfrmServidorReplicacao_FilaTransmissao.MenuItem4Click(
  Sender: TObject);
begin
  inherited;

  qryPendentesServidor.Close;
  qryPendentesServidor.Open ;

end;

procedure TfrmServidorReplicacao_FilaTransmissao.MenuItem5Click(
  Sender: TObject);
begin
  inherited;

  qryPendentesTabela.Close;
  qryPendentesTabela.Open;

end;

end.
