inherited frmCadastroCentroProdutivo: TfrmCadastroCentroProdutivo
  Left = 230
  Top = 144
  Width = 1013
  Height = 586
  Caption = 'Centro Produtivo'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 997
    Height = 525
  end
  object Label1: TLabel [1]
    Left = 60
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 55
    Top = 62
    Width = 43
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
    FocusControl = DBEdit1
  end
  object Label3: TLabel [3]
    Left = 49
    Top = 86
    Width = 49
    Height = 13
    Alignment = taRightJustify
    Caption = 'Descri'#231#227'o'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 14
    Top = 110
    Width = 84
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro de Custo'
  end
  object Label5: TLabel [5]
    Left = 11
    Top = 134
    Width = 87
    Height = 13
    Alignment = taRightJustify
    Caption = 'Local de Estoque'
  end
  object Label6: TLabel [6]
    Left = 12
    Top = 158
    Width = 86
    Height = 13
    Alignment = taRightJustify
    Caption = 'Setor Requisi'#231#227'o'
    FocusControl = DBEdit4
  end
  inherited ToolBar2: TToolBar
    Width = 997
  end
  object DBEdit1: TDBEdit [8]
    Left = 104
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdCentroProdutivo'
    DataSource = dsMaster
    TabOrder = 1
  end
  object edtEmpresa: TER2LookupDBEdit [9]
    Left = 104
    Top = 56
    Width = 65
    Height = 19
    DataField = 'nCdEmpresa'
    DataSource = dsMaster
    TabOrder = 2
    CodigoLookup = 8
    QueryLookup = qryEmpresa
  end
  object DBEdit2: TDBEdit [10]
    Tag = 1
    Left = 176
    Top = 56
    Width = 577
    Height = 19
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 3
  end
  object DBEdit3: TDBEdit [11]
    Left = 104
    Top = 80
    Width = 649
    Height = 19
    DataField = 'cNmCentroProdutivo'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBEdit6: TDBEdit [12]
    Tag = 1
    Left = 176
    Top = 104
    Width = 577
    Height = 19
    DataField = 'cNmCC'
    DataSource = dsCentroCusto
    TabOrder = 5
  end
  object DBEdit7: TDBEdit [13]
    Tag = 1
    Left = 176
    Top = 128
    Width = 577
    Height = 19
    DataField = 'cNmLocalEstoque'
    DataSource = dsLocalEstoque
    TabOrder = 6
  end
  object edtCC: TER2LookupDBEdit [14]
    Left = 104
    Top = 104
    Width = 65
    Height = 19
    DataField = 'nCdCC'
    DataSource = dsMaster
    TabOrder = 7
    CodigoLookup = 28
    QueryLookup = qryCentroCusto
  end
  object edtLocalEstoque: TER2LookupDBEdit [15]
    Left = 104
    Top = 128
    Width = 65
    Height = 19
    DataField = 'nCdLocalEstoqueProcesso'
    DataSource = dsMaster
    TabOrder = 8
    CodigoLookup = 87
    QueryLookup = qryLocalEstoque
  end
  object cxPageControl1: TcxPageControl [16]
    Left = 8
    Top = 192
    Width = 745
    Height = 337
    ActivePage = cxTabSheet1
    LookAndFeel.NativeStyle = True
    TabOrder = 10
    ClientRectBottom = 333
    ClientRectLeft = 4
    ClientRectRight = 741
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Linhas de Produ'#231#227'o'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 737
        Height = 309
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsLinhaProducao
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        OnEnter = DBGridEh1Enter
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdLinhaProducao'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'C'#243'digo'
          end
          item
            EditButtons = <>
            FieldName = 'nCdCentroProdutivo'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'cNmLinhaProducao'
            Footers = <>
            Title.Caption = 'Descri'#231#227'o'
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object edtSetorRequisicao: TER2LookupDBEdit [17]
    Left = 104
    Top = 152
    Width = 65
    Height = 19
    DataField = 'nCdSetorRequisicao'
    DataSource = dsMaster
    TabOrder = 9
    CodigoLookup = 122
    QueryLookup = qrySetorRequisicao
  end
  object DBEdit4: TDBEdit [18]
    Tag = 1
    Left = 176
    Top = 152
    Width = 577
    Height = 19
    DataField = 'cNmSetor'
    DataSource = dsSetorRequisicao
    TabOrder = 11
  end
  inherited qryMaster: TADOQuery
    BeforeClose = qryMasterBeforeClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM CentroProdutivo'
      'WHERE nCdCentroProdutivo = :nPK')
    Left = 456
    Top = 184
    object qryMasternCdCentroProdutivo: TIntegerField
      FieldName = 'nCdCentroProdutivo'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMastercNmCentroProdutivo: TStringField
      FieldName = 'cNmCentroProdutivo'
      Size = 50
    end
    object qryMasternCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMasternCdLocalEstoqueProcesso: TIntegerField
      FieldName = 'nCdLocalEstoqueProcesso'
    end
    object qryMasternCdSetorRequisicao: TIntegerField
      FieldName = 'nCdSetorRequisicao'
    end
  end
  inherited dsMaster: TDataSource
    Left = 504
    Top = 184
  end
  inherited qryID: TADOQuery
    Left = 552
    Top = 184
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 560
    Top = 240
  end
  inherited qryStat: TADOQuery
    Left = 464
    Top = 256
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 528
    Top = 296
  end
  inherited ImageList1: TImageList
    Left = 664
    Top = 280
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa, cNmEmpresa'
      'FROM Empresa'
      'WHERE nCdEmpresa = :nPK')
    Left = 136
    Top = 288
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 152
    Top = 320
  end
  object qryCentroCusto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC'
      '      ,cNmCC '
      '  FROM CentroCusto'
      ' WHERE nCdCC = :nPK')
    Left = 176
    Top = 288
    object qryCentroCustonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCentroCustocNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object qryLocalEstoque: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLocalEstoque'
      '      ,cNmLocalEstoque'
      '  FROM LocalEstoque'
      ' WHERE nCdLocalEstoque = :nPK')
    Left = 216
    Top = 288
    object qryLocalEstoquenCdLocalEstoque: TIntegerField
      FieldName = 'nCdLocalEstoque'
    end
    object qryLocalEstoquecNmLocalEstoque: TStringField
      FieldName = 'cNmLocalEstoque'
      Size = 50
    end
  end
  object dsCentroCusto: TDataSource
    DataSet = qryCentroCusto
    Left = 192
    Top = 320
  end
  object dsLocalEstoque: TDataSource
    DataSet = qryLocalEstoque
    Left = 232
    Top = 320
  end
  object qryLinhaProducao: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryLinhaProducaoBeforePost
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM LinhaProducao'
      ' WHERE nCdCentroProdutivo = :nPK')
    Left = 260
    Top = 288
    object qryLinhaProducaonCdLinhaProducao: TIntegerField
      FieldName = 'nCdLinhaProducao'
    end
    object qryLinhaProducaonCdCentroProdutivo: TIntegerField
      FieldName = 'nCdCentroProdutivo'
    end
    object qryLinhaProducaocNmLinhaProducao: TStringField
      FieldName = 'cNmLinhaProducao'
      Size = 50
    end
  end
  object dsLinhaProducao: TDataSource
    DataSet = qryLinhaProducao
    Left = 276
    Top = 320
  end
  object qrySetorRequisicao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdSetor'
      '             ,cNmSetor'
      '  FROM Setor'
      ' WHERE nCdSetor = :nPK')
    Left = 308
    Top = 288
    object qrySetorRequisicaonCdSetor: TIntegerField
      FieldName = 'nCdSetor'
    end
    object qrySetorRequisicaocNmSetor: TStringField
      FieldName = 'cNmSetor'
      Size = 50
    end
  end
  object dsSetorRequisicao: TDataSource
    DataSet = qrySetorRequisicao
    Left = 320
    Top = 320
  end
end
