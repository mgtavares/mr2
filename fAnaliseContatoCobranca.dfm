inherited frmAnaliseContatoCobranca: TfrmAnaliseContatoCobranca
  Left = 169
  Top = 12
  Width = 1166
  Height = 709
  Caption = 'An'#225'lise Contato Cobranca'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 249
    Width = 1150
    Height = 422
  end
  inherited ToolBar1: TToolBar
    Width = 1150
    inherited ToolButton1: TToolButton
      Caption = '&Consultar'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
  end
  object gbFiltro: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 1150
    Height = 220
    Align = alTop
    Caption = ' Filtros '
    TabOrder = 1
    object Label9: TLabel
      Tag = 1
      Left = 95
      Top = 32
      Width = 20
      Height = 13
      Alignment = taRightJustify
      Caption = 'Loja'
    end
    object Label7: TLabel
      Tag = 1
      Left = 82
      Top = 80
      Width = 33
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cliente'
    end
    object Label1: TLabel
      Tag = 1
      Left = 53
      Top = 104
      Width = 62
      Height = 13
      Alignment = taRightJustify
      Caption = 'Tipo Contato'
    end
    object Label2: TLabel
      Tag = 1
      Left = 200
      Top = 128
      Width = 16
      Height = 13
      Caption = 'at'#233
    end
    object Label3: TLabel
      Tag = 1
      Left = 22
      Top = 126
      Width = 93
      Height = 13
      Alignment = taRightJustify
      Caption = 'Per'#237'odo de Contato'
    end
    object Label4: TLabel
      Tag = 1
      Left = 13
      Top = 56
      Width = 102
      Height = 13
      Alignment = taRightJustify
      Caption = 'Analista de Cobran'#231'a'
    end
    object Label5: TLabel
      Tag = 1
      Left = 307
      Top = 128
      Width = 268
      Height = 13
      Caption = '(deixar em branco para os contatos dos '#250'ltimos 30 dias)'
    end
    object er2LkpLoja: TER2LookupMaskEdit
      Left = 120
      Top = 24
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 0
      Text = '         '
      CodigoLookup = 59
      QueryLookup = qryLoja
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 187
      Top = 24
      Width = 606
      Height = 21
      DataField = 'cNmLoja'
      DataSource = dsLoja
      TabOrder = 1
    end
    object er2LkpCliente: TER2LookupMaskEdit
      Left = 120
      Top = 72
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 4
      Text = '         '
      CodigoLookup = 200
      QueryLookup = qryTerceiro
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 187
      Top = 72
      Width = 142
      Height = 21
      DataField = 'cCNPJCPF'
      DataSource = dsTerceiro
      TabOrder = 5
    end
    object DBEdit3: TDBEdit
      Tag = 1
      Left = 331
      Top = 72
      Width = 350
      Height = 21
      DataField = 'cNmTerceiro'
      DataSource = dsTerceiro
      TabOrder = 6
    end
    object DBEdit4: TDBEdit
      Tag = 1
      Left = 683
      Top = 72
      Width = 110
      Height = 21
      DataField = 'cNmStatus'
      DataSource = dsTerceiro
      TabOrder = 7
    end
    object er2LkpTipoResultContato: TER2LookupMaskEdit
      Left = 120
      Top = 96
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 8
      Text = '         '
      CodigoLookup = 196
      QueryLookup = qryTipoResultadoContato
    end
    object DBEdit5: TDBEdit
      Tag = 1
      Left = 187
      Top = 96
      Width = 494
      Height = 21
      DataField = 'cNmTipoResultadoContato'
      DataSource = dsTipoResultadoContato
      TabOrder = 9
    end
    object gbOpcao: TGroupBox
      Left = 120
      Top = 152
      Width = 241
      Height = 57
      Caption = ' Op'#231#245'es '
      TabOrder = 12
      object checkUltContato: TCheckBox
        Left = 12
        Top = 24
        Width = 217
        Height = 17
        Caption = 'Considerar somente '#250'lt. contato da lista.'
        TabOrder = 0
      end
    end
    object mskDtInicial: TMaskEdit
      Left = 120
      Top = 120
      Width = 70
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 10
      Text = '  /  /    '
    end
    object mskDtFinal: TMaskEdit
      Left = 224
      Top = 120
      Width = 69
      Height = 21
      EditMask = '!99/99/9999;1;_'
      MaxLength = 10
      TabOrder = 11
      Text = '  /  /    '
    end
    object rgModoInclusao: TRadioGroup
      Left = 367
      Top = 152
      Width = 239
      Height = 57
      Caption = ' Modo de Inclus'#227'o '
      Columns = 3
      ItemIndex = 2
      Items.Strings = (
        'Manual'
        'Autom'#225'tico'
        'Todos')
      TabOrder = 13
    end
    object er2LkpAnalista: TER2LookupMaskEdit
      Left = 120
      Top = 48
      Width = 65
      Height = 21
      EditMask = '#########;1; '
      MaxLength = 9
      TabOrder = 2
      Text = '         '
      CodigoLookup = 197
      QueryLookup = qryUsuario
    end
    object DBEdit6: TDBEdit
      Tag = 1
      Left = 187
      Top = 48
      Width = 606
      Height = 21
      DataField = 'cNmUsuario'
      DataSource = DataSource1
      TabOrder = 3
    end
  end
  object cxPageControl: TcxPageControl [3]
    Left = 0
    Top = 249
    Width = 1150
    Height = 422
    ActivePage = tabAnaliseGeral
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 2
    ClientRectBottom = 418
    ClientRectLeft = 4
    ClientRectRight = 1146
    ClientRectTop = 24
    object tabAnaliseGeral: TcxTabSheet
      Caption = 'An'#225'lise Geral'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 1142
        Height = 394
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTempContatoCobranca
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'nCdContatoCobranca'
            Footers = <>
            Title.Caption = 'C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            Title.Caption = 'Loja|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmLoja'
            Footers = <>
            Title.Caption = 'Loja|Loja'
            Width = 110
          end
          item
            EditButtons = <>
            FieldName = 'cNmTerceiro'
            Footers = <>
            Title.Caption = 'Cliente|Nome'
            Width = 200
          end
          item
            EditButtons = <>
            FieldName = 'cNmPessoaContato'
            Footers = <>
            Title.Caption = 'Cliente|Pessoa Contato'
            Width = 200
          end
          item
            EditButtons = <>
            FieldName = 'cTelefoneContato'
            Footers = <>
            Title.Caption = 'Cliente|Tel. Contato'
            Width = 110
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            Title.Caption = 'Contato|Analista'
            Width = 150
          end
          item
            EditButtons = <>
            FieldName = 'dDtContato'
            Footers = <>
            Title.Caption = 'Contato|Data'
          end
          item
            EditButtons = <>
            FieldName = 'cHoraContato'
            Footers = <>
            Title.Caption = 'Contato|Hora'
            Width = 70
          end
          item
            EditButtons = <>
            FieldName = 'cTextoContato'
            Footers = <>
            Title.Caption = 'Contato|Anota'#231#245'es'
            Width = 250
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoResultadoContato'
            Footers = <>
            Title.Caption = 'Tipo Result. Contato|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoResultadoContato'
            Footers = <>
            Title.Caption = 'Tipo Result. Contato|Descri'#231#227'o'
            Width = 150
          end
          item
            EditButtons = <>
            FieldName = 'cModoInclusao'
            Footers = <>
            Title.Caption = 'Inclus'#227'o'
            Width = 75
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object tabAnaliseSemanal: TcxTabSheet
      Caption = 'An'#225'lise Semanal'
      ImageIndex = 1
      object DBGridEh2: TDBGridEh
        Left = 0
        Top = 0
        Width = 1142
        Height = 394
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTempResumoContatoDiario
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'iSemana'
            Footers = <>
            Title.Caption = 'Contato|Nr.'
            Width = 30
          end
          item
            EditButtons = <>
            FieldName = 'cDiaSemana'
            Footers = <>
            Title.Caption = 'Contato|Semana'
            Width = 112
          end
          item
            Color = clWhite
            EditButtons = <>
            FieldName = 'dDtContato'
            Footers = <>
            Title.Caption = 'Contato|Data'
          end
          item
            EditButtons = <>
            FieldName = 'nCdTipoContato'
            Footers = <>
            Title.Caption = 'Tipo Result. Contato|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmTipoContato'
            Footers = <>
            Title.Caption = 'Tipo Result. Contato|Descri'#231#227'o'
          end
          item
            EditButtons = <>
            FieldName = 'iQtdeContato'
            Footers = <>
            Title.Caption = 'Qtde Contato'
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'nPercParticipacao'
            Footers = <>
            Title.Caption = '% Participa'#231#227'o Semanal'
            Width = 75
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
    object tabDesempAnalista: TcxTabSheet
      Caption = 'Desempenho Analista'
      ImageIndex = 3
      object DBGridEh3: TDBGridEh
        Left = 0
        Top = 0
        Width = 1142
        Height = 394
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsTempResumoDesempAnalista
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        UseMultiTitle = True
        Columns = <
          item
            Color = clSilver
            EditButtons = <>
            FieldName = 'nCdUsuarioAnalista'
            Footers = <>
            Title.Caption = 'Analista|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            Title.Caption = 'Analista|Nome'
            Width = 300
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            Title.Caption = 'Loja|C'#243'd.'
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmLoja'
            Footers = <>
            Title.Caption = 'Loja|Descri'#231#227'o'
            Width = 300
          end
          item
            EditButtons = <>
            FieldName = 'iQtdeContatoManual'
            Footers = <>
            Title.Caption = 'Qtde Contato|Manual'
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'iQtdeContatoAuto'
            Footers = <>
            Title.Caption = 'Qtde Contato|Autom'#225'tico'
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'iQtdeContato'
            Footers = <>
            Title.Caption = 'Qtde Contato|Total'
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'nPercParticipacao'
            Footers = <>
            Title.Caption = '% Participa'#231#227'o Analista'
            Width = 75
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 824
    Top = 168
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003E39340039343000332F
      2B002C29250027242100201D1B00E7E7E700333130000B0A0900070706000404
      0300000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000000000000046413B00857A7000C3B8
      AE007C7268007F756B0036322D00F2F2F1004C4A470095897D00BAAEA2007C72
      68007F756B000101010000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF00000084000000000000000000000000004D47410083786F00CCC3
      BA00786F65007B71670034302D00FEFEFE002C2A270095897D00C2B8AD00786F
      65007C7268000605050000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000000000000554E480083786F00CCC3
      BA007970660071685F00585550000000000049464500857A7000C2B8AD00786F
      65007B7167000D0C0B0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF0000008400000000000000000000000000817B76009F928600CCC3
      BA00C0B4AA00A6988B00807D79000000000074726F0090847900C2B8AD00C0B4
      AA00A89B8E004947470000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000FCFCFC0060595200423D38005851
      4A003D383300332F2B0039373400D3D3D3005F5E5C001A181600252220001917
      15000F0E0D0012121200FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF00000084000000000000000000FDFDFD009D918500B1A396007F75
      6B007C726800776D64006C635B002E2A2600564F480080766C007C726800776D
      640070675E0001010100FAFAFA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000FEFDFD00B8ACA100BAAEA2008277
      6D0082776D00AA917B00BAA79400B8A69000B09781009F8D7D00836D5B007163
      570095897D0023232200FCFCFC00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000FDFCFC00DDDAD7009B8E82009D91
      8500867B7100564F4800504A440080766C006E665D00826C5800A6917D009484
      7400564F48008B8A8A00FEFEFE00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000000000000000000000746B6200A497
      8A0095897D009F9286003E393400000000004C4640007E746A00857A70003E39
      340085817E00F5F5F500FDFDFD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF000000840000000000000000000000000000000000000000000000
      00009B918700C3B8AE00655D5500000000007C726800A89B8E00A69B90000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF0000000000000000000000000000000000000000000000
      0000A79C9100BCB0A4009D91850000000000AEA093009D9185007B756E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFFFFFF0000FE00FFFFFFFF0000
      FE00C00380030000000080018003000000008001800300000000800181030000
      0000800181030000000080010001000000008001000100000000800100010000
      000080010001000000008001C101000000018001F11F000000038001F11F0000
      0077C003FFFF0000007FFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja'
      '      ,cNmLoja'
      '  FROM Loja'
      ' WHERE nCdLoja = :nPK')
    Left = 696
    Top = 133
    object qryLojanCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 696
    Top = 168
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      '      ,cNmTerceiro'
      '      ,cCNPJCPF'
      '      ,cNmStatus'
      '  FROM vwClienteVarejo'
      
        '       INNER JOIN Status ON Status.nCdStatus = vwClienteVarejo.n' +
        'CdStatus'
      ' WHERE nCdTerceiro = :nPK')
    Left = 760
    Top = 133
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 100
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 760
    Top = 168
  end
  object qryTipoResultadoContato: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      '  FROM TipoResultadoContato'
      ' WHERE nCdTipoResultadoContato = :nPK')
    Left = 792
    Top = 136
    object qryTipoResultadoContatonCdTipoResultadoContato: TIntegerField
      FieldName = 'nCdTipoResultadoContato'
    end
    object qryTipoResultadoContatocNmTipoResultadoContato: TStringField
      FieldName = 'cNmTipoResultadoContato'
      Size = 50
    end
    object qryTipoResultadoContatocFlgAtivo: TIntegerField
      FieldName = 'cFlgAtivo'
    end
    object qryTipoResultadoContatocFlgContatado: TIntegerField
      FieldName = 'cFlgContatado'
    end
    object qryTipoResultadoContatonCdTabTipoRestricaoVenda: TIntegerField
      FieldName = 'nCdTabTipoRestricaoVenda'
    end
  end
  object dsTipoResultadoContato: TDataSource
    DataSet = qryTipoResultadoContato
    Left = 792
    Top = 168
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT Usuario.nCdUsuario'
      '      ,Usuario.cNmUsuario'
      '  FROM Usuario'
      ' WHERE nCdUsuario           = :nPK'
      '   AND cFlgAnalistaCobranca = 1')
    Left = 728
    Top = 136
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryUsuario
    Left = 728
    Top = 168
  end
  object qryTempContatoCobranca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdTerceiro'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdTipoResultadoContato'
        Size = -1
        Value = Null
      end
      item
        Name = 'cDtContatoInicial'
        Size = -1
        Value = Null
      end
      item
        Name = 'cDtContatoFinal'
        Size = -1
        Value = Null
      end
      item
        Name = 'cFlgConsideraUltContato'
        Size = -1
        Value = Null
      end
      item
        Name = 'cFlgModoInclusao'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempContatoCobranca'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempContatoCobranca(nCdContatoCobranca      in' +
        't'
      
        '                                     ,nCdTerceiro             in' +
        't'
      
        '                                     ,nCdUsuario              in' +
        't'
      
        '                                     ,nCdLoja                 in' +
        't'
      
        '                                     ,nCdTipoResultadoContato in' +
        't'
      
        '                                     ,dDtContato              da' +
        'tetime'
      
        '                                     ,cNmPessoaContato        va' +
        'rchar(50)'
      
        '                                     ,cTelefoneContato        va' +
        'rchar(50)'
      
        '                                     ,cTextoContato           va' +
        'rchar(100)'
      
        '                                     ,cModoInclusao           va' +
        'rchar(10)'
      
        '                                     ,nCdItemListaCobranca    in' +
        't'
      
        '                                     ,nCdListaCobranca        in' +
        't)'
      ''
      'END'
      ''
      'EXEC SPREL_CONTATO_COBRANCA :nCdLoja'
      '                           ,:nCdTerceiro'
      '                           ,:nCdUsuario'
      '                           ,:nCdTipoResultadoContato'
      '                           ,:cDtContatoInicial'
      '                           ,:cDtContatoFinal'
      '                           ,:cFlgConsideraUltContato'
      '                           ,:cFlgModoInclusao'
      '                           ,1')
    Left = 728
    Top = 197
    object qryTempContatoCobrancanCdContatoCobranca: TIntegerField
      FieldName = 'nCdContatoCobranca'
    end
    object qryTempContatoCobrancanCdLoja: TStringField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryTempContatoCobrancacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryTempContatoCobrancacNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      ReadOnly = True
      Size = 83
    end
    object qryTempContatoCobrancacNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      ReadOnly = True
      Size = 133
    end
    object qryTempContatoCobrancadDtContato: TDateTimeField
      FieldName = 'dDtContato'
      ReadOnly = True
    end
    object qryTempContatoCobrancacHoraContato: TStringField
      FieldName = 'cHoraContato'
      ReadOnly = True
      Size = 30
    end
    object qryTempContatoCobrancacNmPessoaContato: TStringField
      FieldName = 'cNmPessoaContato'
      Size = 50
    end
    object qryTempContatoCobrancacTelefoneContato: TStringField
      FieldName = 'cTelefoneContato'
      Size = 50
    end
    object qryTempContatoCobrancacTextoContato: TStringField
      FieldName = 'cTextoContato'
      Size = 100
    end
    object qryTempContatoCobrancanCdTipoResultadoContato: TIntegerField
      FieldName = 'nCdTipoResultadoContato'
    end
    object qryTempContatoCobrancacNmTipoResultadoContato: TStringField
      FieldName = 'cNmTipoResultadoContato'
      Size = 50
    end
    object qryTempContatoCobrancacModoInclusao: TStringField
      FieldName = 'cModoInclusao'
      Size = 10
    end
  end
  object qryTempResumoContatoDiario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdTipoResultadoContato'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cDtContatoInicial'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'cDtContatoFinal'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'cFlgConsideraUltContato'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cFlgModoInclusao'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempResumoContatoDiario'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempResumoContatoDiario(dDtContato        date' +
        'time'
      '                                         ,iSemana           int'
      
        '                                         ,cDiaSemana        varc' +
        'har(15)'
      '                                         ,nCdTipoContato    int'
      
        '                                         ,cNmTipoContato    varc' +
        'har(50)'
      '                                         ,iQtdeContato      int'
      
        '                                         ,nPercParticipacao deci' +
        'mal(12,4))'
      ''
      'END'
      ''
      'EXEC SPREL_CONTATO_COBRANCA :nCdLoja'
      '                           ,:nCdTerceiro'
      '                           ,:nCdUsuario'
      '                           ,:nCdTipoResultadoContato'
      '                           ,:cDtContatoInicial'
      '                           ,:cDtContatoFinal'
      '                           ,:cFlgConsideraUltContato'
      '                           ,:cFlgModoInclusao'
      '                           ,2')
    Left = 760
    Top = 197
    object qryTempResumoContatoDiariodDtContato: TDateTimeField
      FieldName = 'dDtContato'
    end
    object qryTempResumoContatoDiarioiSemana: TIntegerField
      FieldName = 'iSemana'
    end
    object qryTempResumoContatoDiariocDiaSemana: TStringField
      FieldName = 'cDiaSemana'
      Size = 15
    end
    object qryTempResumoContatoDiarionCdTipoContato: TIntegerField
      FieldName = 'nCdTipoContato'
    end
    object qryTempResumoContatoDiariocNmTipoContato: TStringField
      FieldName = 'cNmTipoContato'
      Size = 50
    end
    object qryTempResumoContatoDiarioiQtdeContato: TIntegerField
      FieldName = 'iQtdeContato'
    end
    object qryTempResumoContatoDiarionPercParticipacao: TBCDField
      FieldName = 'nPercParticipacao'
      Precision = 12
    end
  end
  object qryTempResumoDesempAnalista: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoja'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdTerceiro'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdUsuario'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdTipoResultadoContato'
        Size = -1
        Value = Null
      end
      item
        Name = 'cDtContatoInicial'
        Size = -1
        Value = Null
      end
      item
        Name = 'cDtContatoFinal'
        Size = -1
        Value = Null
      end
      item
        Name = 'cFlgConsideraUltContato'
        Size = -1
        Value = Null
      end
      item
        Name = 'cFlgModoInclusao'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempResumoDesempAnalista'#39') IS NULL)'
      'BEGIN'
      ''
      
        '    CREATE TABLE #TempResumoDesempAnalista(nCdUsuarioAnalista in' +
        't'
      
        '                                          ,nCdLoja            in' +
        't'
      
        '                                          ,iQtdeContato       in' +
        't'
      
        '                                          ,iQtdeContatoManual in' +
        't'
      
        '                                          ,iQtdeContatoAuto   in' +
        't'
      
        '                                          ,nPercParticipacao dec' +
        'imal(12,4))'
      ''
      'END'
      ''
      'EXEC SPREL_CONTATO_COBRANCA :nCdLoja'
      '                           ,:nCdTerceiro'
      '                           ,:nCdUsuario'
      '                           ,:nCdTipoResultadoContato'
      '                           ,:cDtContatoInicial'
      '                           ,:cDtContatoFinal'
      '                           ,:cFlgConsideraUltContato'
      '                           ,:cFlgModoInclusao'
      '                           ,3')
    Left = 792
    Top = 197
    object qryTempResumoDesempAnalistanCdUsuarioAnalista: TIntegerField
      FieldName = 'nCdUsuarioAnalista'
    end
    object qryTempResumoDesempAnalistacNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryTempResumoDesempAnalistanCdLoja: TStringField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryTempResumoDesempAnalistacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
    object qryTempResumoDesempAnalistaiQtdeContato: TIntegerField
      FieldName = 'iQtdeContato'
    end
    object qryTempResumoDesempAnalistaiQtdeContatoManual: TIntegerField
      FieldName = 'iQtdeContatoManual'
    end
    object qryTempResumoDesempAnalistaiQtdeContatoAuto: TIntegerField
      FieldName = 'iQtdeContatoAuto'
    end
    object qryTempResumoDesempAnalistanPercParticipacao: TBCDField
      FieldName = 'nPercParticipacao'
      Precision = 12
    end
  end
  object dsTempContatoCobranca: TDataSource
    DataSet = qryTempContatoCobranca
    Left = 728
    Top = 232
  end
  object dsTempResumoContatoDiario: TDataSource
    DataSet = qryTempResumoContatoDiario
    Left = 760
    Top = 232
  end
  object dsTempResumoDesempAnalista: TDataSource
    DataSet = qryTempResumoDesempAnalista
    Left = 792
    Top = 232
  end
  object ImageList2: TImageList
    Left = 824
    Top = 197
  end
end
