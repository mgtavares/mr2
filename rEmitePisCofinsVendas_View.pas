unit rEmitePisCofinsVendas_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, QuickRpt, QRCtrls, jpeg, DB, ADODB, StdCtrls;

type
  TrEmitePisCofins_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape1: TQRShape;
    QRShape2: TQRShape;
    QRBand4: TQRBand;
    QRLabel11: TQRLabel;
    QRBand5: TQRBand;
    QRBand2: TQRBand;
    uspRelatorio: TADOStoredProc;
    QRLabel13: TQRLabel;
    QRLabel1: TQRLabel;
    DataSource1: TDataSource;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRExpr1: TQRExpr;
    QRLabel12: TQRLabel;
    QRLabel15: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRExpr2: TQRExpr;
    QRExpr3: TQRExpr;
    QRDBText12: TQRDBText;
    QRLabel17: TQRLabel;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    uspRelatorionCdPedido: TIntegerField;
    uspRelatoriodDtPedido: TDateTimeField;
    uspRelatoriocNmTerceiro: TStringField;
    uspRelatorionCdItemPedido: TIntegerField;
    uspRelatorionCdItemDoctoFiscal: TIntegerField;
    uspRelatorionCdProduto: TIntegerField;
    uspRelatoriocNmItem: TStringField;
    uspRelatorionCdGrupoImposto: TIntegerField;
    uspRelatorionCdDoctoFiscal: TIntegerField;
    uspRelatoriocEmpresa: TStringField;
    uspRelatoriocCdLoja: TStringField;
    uspRelatorionCdTerceiro: TIntegerField;
    uspRelatorioiNrDocto: TIntegerField;
    uspRelatoriodDtEmissao: TDateTimeField;
    uspRelatoriocNCM: TStringField;
    uspRelatoriocNmTipoPedido: TStringField;
    uspRelatorionVrPis: TBCDField;
    uspRelatorionVrCofins: TBCDField;
    uspRelatorionVrPisDev: TBCDField;
    uspRelatorionVrCofinsDev: TBCDField;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRExpr4: TQRExpr;
    QRExpr5: TQRExpr;
    uspRelatorioVrProdVenda: TBCDField;
    uspRelatorioVrProdDev: TBCDField;
    QRLabel20: TQRLabel;
    QRDBText15: TQRDBText;
    QRExpr6: TQRExpr;
   // QRLabel21: TQRLabel;
    QRLabel22: TQRLabel;
    QRLabel23: TQRLabel;
    QRShape4: TQRShape;
    QRShape8: TQRShape;
    QRLabel36: TQRLabel;
    QRShape5: TQRShape;
    QRLabel14: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rEmitePisCofins_View: TrEmitePisCofins_View;

implementation

{$R *.dfm}

end.
