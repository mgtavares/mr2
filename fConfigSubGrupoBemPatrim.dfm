inherited frmConfigSubGrupoBemPatrim: TfrmConfigSubGrupoBemPatrim
  Left = 249
  Top = 174
  Caption = 'Configura'#231#227'o  SubGrupo Patrimonial'
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 89
    Height = 384
  end
  inherited ToolBar1: TToolBar
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 89
    Width = 792
    Height = 384
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 384
    ClientRectRight = 792
    ClientRectTop = 23
    object cxTabSheet1: TcxTabSheet
      Caption = 'Configura'#231#227'o SubGrupo'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 792
        Height = 361
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsConfigSubGrupoBemPatrim
        Flat = False
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Tahoma'
        FooterFont.Style = []
        RowDetailPanel.Color = clBtnFace
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdConfigSubGrupoBemPatrim'
            Footers = <>
          end
          item
            EditButtons = <>
            FieldName = 'cNmConfigSubGrupoBemPatrim'
            Footers = <>
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 792
    Height = 60
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Tag = 1
      Left = 16
      Top = 28
      Width = 47
      Height = 13
      Caption = 'SubGrupo'
    end
    object DBEdit1: TDBEdit
      Tag = 1
      Left = 67
      Top = 20
      Width = 65
      Height = 21
      DataField = 'nCdSubGrupoBemPatrim'
      DataSource = dsSubGrupoBemPatrimAux
      TabOrder = 0
    end
    object DBEdit2: TDBEdit
      Tag = 1
      Left = 135
      Top = 20
      Width = 489
      Height = 21
      DataField = 'cNmSubGrupoBemPatrim'
      DataSource = dsSubGrupoBemPatrimAux
      TabOrder = 1
    end
  end
  inherited ImageList1: TImageList
    Left = 632
    Top = 72
  end
  object qryConfigSubGrupoBemPatrim: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryConfigSubGrupoBemPatrimBeforePost
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT * '
      '      FROM ConfigSubGrupoBemPatrim '
      'WHERE nCdSubGrupoBemPatrim = :nPk')
    Left = 616
    Top = 184
    object qryConfigSubGrupoBemPatrimnCdConfigSubGrupoBemPatrim: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'nCdConfigSubGrupoBemPatrim'
      ReadOnly = True
    end
    object qryConfigSubGrupoBemPatrimnCdSubGrupoBemPatrim: TIntegerField
      FieldName = 'nCdSubGrupoBemPatrim'
    end
    object qryConfigSubGrupoBemPatrimcNmConfigSubGrupoBemPatrim: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'cNmConfigSubGrupoBemPatrim'
      Size = 50
    end
  end
  object dsConfigSubGrupoBemPatrim: TDataSource
    DataSet = qryConfigSubGrupoBemPatrim
    Left = 648
    Top = 184
  end
  object qrySubGrupoBemPatrimAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT nCdSubGrupoBemPatrim'
      '              ,nCdGrupoBemPatrim'
      '              ,cNmSubGrupoBemPatrim'
      '   FROM SubGrupoBemPatrim'
      'WHERE nCdSubGrupoBemPatrim = :nPK')
    Left = 704
    Top = 184
    object qrySubGrupoBemPatrimAuxnCdSubGrupoBemPatrim: TAutoIncField
      FieldName = 'nCdSubGrupoBemPatrim'
      ReadOnly = True
    end
    object qrySubGrupoBemPatrimAuxnCdGrupoBemPatrim: TIntegerField
      FieldName = 'nCdGrupoBemPatrim'
    end
    object qrySubGrupoBemPatrimAuxcNmSubGrupoBemPatrim: TStringField
      FieldName = 'cNmSubGrupoBemPatrim'
      Size = 50
    end
  end
  object dsSubGrupoBemPatrimAux: TDataSource
    DataSet = qrySubGrupoBemPatrimAux
    Left = 736
    Top = 184
  end
end
