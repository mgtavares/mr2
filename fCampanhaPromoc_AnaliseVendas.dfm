inherited frmCampanhaPromoc_AnaliseVendas: TfrmCampanhaPromoc_AnaliseVendas
  Left = 0
  Top = 0
  Width = 1440
  Height = 870
  Caption = 'Campanha Promocional - An'#225'lise de Vendas'
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1424
    Height = 805
  end
  inherited ToolBar1: TToolBar
    Width = 1424
    ButtonWidth = 93
    inherited ToolButton1: TToolButton
      Caption = 'Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 93
    end
    inherited ToolButton2: TToolButton
      Left = 101
    end
  end
  object cxGrid2: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 1424
    Height = 805
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGridDBTableView1: TcxGridDBTableView
      OnDblClick = cxGridDBTableView1DblClick
      DataController.DataSource = dsVendas
      DataController.Summary.DefaultGroupSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'nValTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          FieldName = 'nSaldoTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          Column = cxGridDBTableView1nValDesconto
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          Column = cxGridDBTableView1nValAcrescimo
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          Column = cxGridDBTableView1nValCustoUnit
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          Column = cxGridDBTableView1nValTotalItem
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          Column = cxGridDBTableView1nValCMV
        end
        item
          Format = '#,##0.00'
          Position = spFooter
          Column = cxGridDBTableView1nMargemBruta
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Position = spFooter
          Column = cxGridDBTableView1nValTotalBruto
        end
        item
          Format = '#,##0.00'
          Position = spFooter
          Column = cxGridDBTableView1nMargemReal
        end>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nValTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          FieldName = 'nSaldoTit'
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValDesconto
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValAcrescimo
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValCustoUnit
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValTotalItem
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValCMV
        end
        item
          Format = '#,##0.00'
          Column = cxGridDBTableView1nMargemBruta
        end
        item
          Format = '#,##0.00'
          Kind = skSum
          Column = cxGridDBTableView1nValTotalBruto
        end
        item
          Format = '#,##0.00'
          Column = cxGridDBTableView1nMargemReal
        end>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.Footer = True
      OptionsView.GridLines = glVertical
      OptionsView.GroupFooters = gfAlwaysVisible
      object cxGridDBTableView1nCdItemPedido: TcxGridDBColumn
        DataBinding.FieldName = 'nCdItemPedido'
        Visible = False
      end
      object cxGridDBTableView1nCdPedido: TcxGridDBColumn
        Caption = 'Pedido'
        DataBinding.FieldName = 'nCdPedido'
      end
      object cxGridDBTableView1cNmLoja: TcxGridDBColumn
        Caption = 'Loja'
        DataBinding.FieldName = 'cNmLoja'
        Visible = False
        GroupIndex = 0
      end
      object cxGridDBTableView1dDtPedido: TcxGridDBColumn
        Caption = 'Data Pedido'
        DataBinding.FieldName = 'dDtPedido'
        Width = 140
      end
      object cxGridDBTableView1nCdProduto: TcxGridDBColumn
        Caption = 'Produto'
        DataBinding.FieldName = 'nCdProduto'
      end
      object cxGridDBTableView1cNmItem: TcxGridDBColumn
        Caption = 'Descri'#231#227'o Produto'
        DataBinding.FieldName = 'cNmItem'
        Width = 299
      end
      object cxGridDBTableView1nQtdeExpRec: TcxGridDBColumn
        Caption = 'Quant.'
        DataBinding.FieldName = 'nQtdeExpRec'
        Width = 51
      end
      object cxGridDBTableView1nValUnitario: TcxGridDBColumn
        Caption = 'Val. Unit'#225'rio'
        DataBinding.FieldName = 'nValUnitario'
      end
      object cxGridDBTableView1nValTotalBruto: TcxGridDBColumn
        Caption = 'Total Bruto'
        DataBinding.FieldName = 'nValTotalBruto'
        Width = 105
      end
      object cxGridDBTableView1nValCMV: TcxGridDBColumn
        Caption = 'Custo da Venda'
        DataBinding.FieldName = 'nValCMV'
      end
      object cxGridDBTableView1nMargemBruta: TcxGridDBColumn
        Caption = '% Lucro Bruto'
        DataBinding.FieldName = 'nMargemBruta'
      end
      object cxGridDBTableView1nValDesconto: TcxGridDBColumn
        Caption = 'Val. Desconto'
        DataBinding.FieldName = 'nValDesconto'
      end
      object cxGridDBTableView1nValAcrescimo: TcxGridDBColumn
        Caption = 'Val. Acr'#233'scimo'
        DataBinding.FieldName = 'nValAcrescimo'
      end
      object cxGridDBTableView1nValCustoUnit: TcxGridDBColumn
        Caption = 'Custo Final Unit.'
        DataBinding.FieldName = 'nValCustoUnit'
      end
      object cxGridDBTableView1nValTotalItem: TcxGridDBColumn
        Caption = 'Total do Item'
        DataBinding.FieldName = 'nValTotalItem'
      end
      object cxGridDBTableView1nMargemReal: TcxGridDBColumn
        Caption = '% Lucro Real'
        DataBinding.FieldName = 'nMargemReal'
      end
      object cxGridDBTableView1cNmDepartamento: TcxGridDBColumn
        Caption = 'Departamento'
        DataBinding.FieldName = 'cNmDepartamento'
        Width = 155
      end
      object cxGridDBTableView1cNmCategoria: TcxGridDBColumn
        Caption = 'Categoria'
        DataBinding.FieldName = 'cNmCategoria'
        Width = 155
      end
      object cxGridDBTableView1cNmSubCategoria: TcxGridDBColumn
        Caption = 'Sub Categoria'
        DataBinding.FieldName = 'cNmSubCategoria'
        Width = 155
      end
      object cxGridDBTableView1cNmSegmento: TcxGridDBColumn
        Caption = 'Segmento'
        DataBinding.FieldName = 'cNmSegmento'
        Width = 155
      end
      object cxGridDBTableView1cNmLinha: TcxGridDBColumn
        Caption = 'Linha'
        DataBinding.FieldName = 'cNmLinha'
        Width = 155
      end
      object cxGridDBTableView1cNmMarca: TcxGridDBColumn
        Caption = 'Marca'
        DataBinding.FieldName = 'cNmMarca'
        Width = 155
      end
      object cxGridDBTableView1cNmTerceiro: TcxGridDBColumn
        Caption = 'Cliente'
        DataBinding.FieldName = 'cNmTerceiro'
        Width = 155
      end
    end
    object cxGridLevel1: TcxGridLevel
      GridView = cxGridDBTableView1
    end
  end
  object qryVendas: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT ItemPedido.nCdItemPedido'
      '      ,Pedido.nCdPedido'
      
        '      ,dbo.fn_ZeroEsquerda(Pedido.nCdLoja,4) + '#39' - '#39' + cNmLoja a' +
        's cNmLoja'
      '      ,Pedido.dDtPedido'
      '      ,ItemPedido.nCdProduto'
      '      ,ItemPedido.cNmItem'
      '      ,ItemPedido.nQtdeExpRec'
      '      ,ItemPedido.nValUnitario'
      
        '      ,(ItemPedido.nValUnitario*ItemPedido.nQtdeExpRec) as nValT' +
        'otalBruto'
      '      ,ItemPedido.nValCMV'
      
        '      ,Convert(DECIMAL(12,2),(CASE WHEN (ItemPedido.nValUnitario' +
        '*ItemPedido.nQtdeExpRec) = 0 AND ItemPedido.nValCMV = 0 THEN 0'
      
        '                                   WHEN (ItemPedido.nValUnitario' +
        '*ItemPedido.nQtdeExpRec) > 0 AND ItemPedido.nValCMV = 0 THEN 100'
      
        '                                   ELSE ((((ItemPedido.nValUnita' +
        'rio*ItemPedido.nQtdeExpRec) / ItemPedido.nValCMV)*100)-100)'
      '                              END)) nMargemBruta'
      '      ,ItemPedido.nValDesconto'
      '      ,ItemPedido.nValAcrescimo'
      '      ,ItemPedido.nValCustoUnit'
      '      ,ItemPedido.nValTotalItem'
      
        '      ,Convert(DECIMAL(12,2),(CASE WHEN ItemPedido.nValTotalItem' +
        ' = 0 AND ItemPedido.nValCMV = 0 THEN 0'
      
        '                                   WHEN ItemPedido.nValTotalItem' +
        ' > 0 AND ItemPedido.nValCMV = 0 THEN 100'
      
        '                                   ELSE (((ItemPedido.nValTotalI' +
        'tem / ItemPedido.nValCMV)*100)-100)'
      '                              END)) nMargemReal'
      '      ,Departamento.cNmDepartamento'
      '      ,Categoria.cNmCategoria'
      '      ,SubCategoria.cNmSubCategoria'
      '      ,Segmento.cNmSegmento'
      '      ,Linha.cNmLinha'
      '      ,Marca.cNmMarca'
      '      ,Terceiro.cNmTerceiro'
      '      ,Pedido.nCdLanctoFin'
      '  FROM ItemPedido'
      
        '       INNER JOIN Pedido       ON Pedido.nCdPedido             =' +
        ' ItemPedido.nCdPedido'
      
        '       LEFT  JOIN Terceiro     ON Terceiro.nCdTerceiro         =' +
        ' Pedido.nCdTerceiro'
      
        '       LEFT  JOIN Produto      ON Produto.nCdProduto           =' +
        ' ItemPedido.nCdProduto'
      
        '       LEFT  JOIN Loja         ON Loja.nCdLoja                 =' +
        ' Pedido.nCdLoja'
      
        '       LEFT  JOIN Departamento ON Departamento.nCdDepartamento =' +
        ' Produto.nCdDepartamento'
      
        '       LEFT  JOIN Categoria    ON Categoria.nCdCategoria       =' +
        ' Produto.nCdCategoria'
      
        '       LEFT  JOIN SubCategoria ON SubCategoria.nCdSubCategoria =' +
        ' Produto.nCdSubCategoria'
      
        '       LEFT  JOIN Segmento     ON Segmento.nCdSegmento         =' +
        ' Produto.nCdSegmento'
      
        '       LEFT  JOIN Linha        ON Linha.nCdLinha               =' +
        ' Produto.nCdLinha'
      
        '       LEFT  JOIN Marca        ON Marca.nCdMarca               =' +
        ' Produto.nCdMarca'
      ' WHERE ItemPedido.nCdCampanhaPromoc = :nPK'
      
        '   AND Pedido.nCdTabStatusPed       IN (4,5) --Somente Pedidos F' +
        'aturados'
      ' ORDER BY cNmLoja, nCdPedido')
    Left = 248
    Top = 184
    object qryVendasnCdItemPedido: TAutoIncField
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryVendasnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryVendascNmLoja: TStringField
      FieldName = 'cNmLoja'
      ReadOnly = True
      Size = 68
    end
    object qryVendasdDtPedido: TDateTimeField
      FieldName = 'dDtPedido'
    end
    object qryVendasnCdProduto: TIntegerField
      FieldName = 'nCdProduto'
    end
    object qryVendascNmItem: TStringField
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryVendasnQtdeExpRec: TBCDField
      FieldName = 'nQtdeExpRec'
      Precision = 12
    end
    object qryVendasnValUnitario: TBCDField
      FieldName = 'nValUnitario'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryVendasnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryVendasnValAcrescimo: TBCDField
      FieldName = 'nValAcrescimo'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryVendasnValCustoUnit: TBCDField
      FieldName = 'nValCustoUnit'
      DisplayFormat = '#,##0.00'
      Precision = 14
      Size = 6
    end
    object qryVendasnValTotalItem: TBCDField
      FieldName = 'nValTotalItem'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryVendasnValCMV: TBCDField
      FieldName = 'nValCMV'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryVendasnMargemBruta: TBCDField
      FieldName = 'nMargemBruta'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryVendascNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
    object qryVendascNmCategoria: TStringField
      FieldName = 'cNmCategoria'
      Size = 50
    end
    object qryVendascNmSubCategoria: TStringField
      FieldName = 'cNmSubCategoria'
      Size = 50
    end
    object qryVendascNmSegmento: TStringField
      FieldName = 'cNmSegmento'
      Size = 50
    end
    object qryVendascNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
    object qryVendascNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
    object qryVendascNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
    object qryVendasnValTotalBruto: TBCDField
      FieldName = 'nValTotalBruto'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 27
      Size = 10
    end
    object qryVendasnMargemReal: TBCDField
      FieldName = 'nMargemReal'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryVendasnCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
  end
  object dsVendas: TDataSource
    DataSet = qryVendas
    Left = 296
    Top = 184
  end
end
