unit fPDVConsProdutoDetalhe;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, jpeg, ExtCtrls, StdCtrls, cxLookAndFeelPainters, cxButtons,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, ADODB, Mask, DBCtrls;

type
  TfrmPDVConsProdutoDetalhe = class(TForm)
    Image2: TImage;
    edtCdProduto: TEdit;
    Label2: TLabel;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    btConsProd: TcxButton;
    btFechar: TcxButton;
    qryProduto: TADOQuery;
    qryProdutoGrade: TADOQuery;
    qryProdutoGradenCdProduto: TIntegerField;
    qryProdutoGradecNmTamanho: TStringField;
    qryProdutoGradenQtdeEstoqueTotal: TBCDField;
    qryProdutoGradenQtdeEstoqueLoja: TIntegerField;
    dsProdutoGrade: TDataSource;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryProdutonQtdeEstoqueTotal: TBCDField;
    qryProdutonQtdeEstoqueLoja: TIntegerField;
    qryProdutonValVenda: TBCDField;
    qryProdutocNmTamanho: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    qryEstoqueLoja: TADOQuery;
    qryEstoqueLojanCdLoja: TIntegerField;
    qryEstoqueLojanQtdeEstoque: TBCDField;
    GroupBox1: TGroupBox;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    GroupBox2: TGroupBox;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1nCdProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmTamanho: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeEstoqueTotal: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeEstoqueLoja: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    dsEstoqueLoja: TDataSource;
    cxGridDBTableView1nCdLoja: TcxGridDBColumn;
    cxGridDBTableView1nQtdeEstoque: TcxGridDBColumn;
    GroupBox3: TGroupBox;
    cxGrid3: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    qryProdutoCores: TADOQuery;
    dsProdutoCores: TDataSource;
    qryProdutoGradenCdProdutoPai: TIntegerField;
    qrySelectTop: TADOQuery;
    qrySelectTopnCdProduto: TIntegerField;
    qryProdutoCoresnCdProduto: TIntegerField;
    qryProdutoCorescNmMaterial: TStringField;
    qryProdutoCorescNmCor: TStringField;
    qryProdutoCoresnQtdeEstoqueTotal: TIntegerField;
    qryProdutoCoresnQtdeEstoqueLoja: TIntegerField;
    cxGridDBTableView2nCdProduto: TcxGridDBColumn;
    cxGridDBTableView2cNmMaterial: TcxGridDBColumn;
    cxGridDBTableView2cNmCor: TcxGridDBColumn;
    cxGridDBTableView2nQtdeEstoqueTotal: TcxGridDBColumn;
    cxGridDBTableView2nQtdeEstoqueLoja: TcxGridDBColumn;
    qryBuscaPromocao: TADOQuery;
    qryBuscaPromocaonCdCampanhaPromoc: TIntegerField;
    qryBuscaPromocaonValPromocao: TBCDField;
    qryBuscaPromocaocChaveCampanha: TStringField;
    qryBuscaPromocaocNmVigencia: TStringField;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    DataSource2: TDataSource;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    qryEstoqueLojanQtdeEmpenhado: TBCDField;
    cxGridDBTableView1DBColumn1: TcxGridDBColumn;
    procedure edtCdProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btFecharClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btConsProdClick(Sender: TObject);
    procedure SelecionaProduto();
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryProdutoAfterScroll(DataSet: TDataSet);
    procedure cxGridDBTableView2DblClick(Sender: TObject);
    procedure qryProdutoAfterCancel(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPDVConsProdutoDetalhe: TfrmPDVConsProdutoDetalhe;

implementation

uses fMenu, fPdvConsProduto;

{$R *.dfm}

procedure TfrmPDVConsProdutoDetalhe.edtCdProdutoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin

    if (Key = VK_RETURN) and (trim(edtCdProduto.Text) <> '') then
    begin

        SelecionaProduto() ;

    end ;

    if (Key = VK_F4) then
        btConsProd.Click;

end;

procedure TfrmPDVConsProdutoDetalhe.btFecharClick(Sender: TObject);
begin

    Close;

end;

procedure TfrmPDVConsProdutoDetalhe.FormShow(Sender: TObject);
begin

    edtCdProduto.MaxLength := StrToint(frmMenu.LeParametro('TMMXBRPRPDV')) ;

    qryBuscaPromocao.Close ;
    
    Label6.Visible  := ((qryBuscaPromocao.Active) and (qryBuscaPromocaonCdCampanhaPromoc.AsString <> '')) ;
    Label7.Visible  := ((qryBuscaPromocao.Active) and (qryBuscaPromocaonCdCampanhaPromoc.AsString <> '')) ;
    DBEdit3.Visible := ((qryBuscaPromocao.Active) and (qryBuscaPromocaonCdCampanhaPromoc.AsString <> '')) ;
    DBEdit6.Visible := ((qryBuscaPromocao.Active) and (qryBuscaPromocaonCdCampanhaPromoc.AsString <> '')) ;

    qryProduto.Close;
    qryProdutoGrade.Close;
    qryEstoqueLoja.Close ;
    qryProdutoCores.Close;

    edtCdProduto.Text := '' ;
    edtCdProduto.SetFocus;

end;

procedure TfrmPDVConsProdutoDetalhe.btConsProdClick(Sender: TObject);
var
    objForm : TfrmPdvConsProduto ;
begin

    objForm := TfrmPdvConsProduto.Create( Self ) ;

    edtCdProduto.Text := IntToStr(objForm.ConsultaProduto()) ;

    freeAndNil( objForm ) ;

    if (edtCdProduto.Text = '0') then
        edtCdProduto.Text := '' ;

    if (trim(edtCdProduto.Text) <> '') then
        SelecionaProduto() ;

end;

procedure TfrmPDVConsProdutoDetalhe.SelecionaProduto;
begin

    qryProduto.Close;
    qryProduto.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva ;
    qryProduto.Parameters.ParamByName('cCdProduto').Value := edtCdProduto.Text;
    qryProduto.Open;

    qryProdutoGrade.Close;

    if (qryProduto.eof) then
    begin
        frmMenu.MensagemAlerta('Produto n�o cadastrado.');
        edtCdProduto.Text := '' ;
        edtCdProduto.SetFocus;
        abort ;
    end ;

    qryProdutoGrade.Close;
    qryProdutoGrade.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva ;
    qryProdutoGrade.Parameters.ParamByName('nCdProduto').Value := qryProdutonCdProduto.asString;
    qryProdutoGrade.Open;

    qryProdutoCores.Close;
    qryProdutoCores.Parameters.ParamByName('nCdProdutoPai').Value := qryProdutoGradenCdProdutoPai.AsString;
    qryProdutoCores.Parameters.ParamByName('nCdLoja').Value       := frmMenu.nCdLojaAtiva ;
    qryProdutoCores.Open;

    cxGrid1.SetFocus;

end;

procedure TfrmPDVConsProdutoDetalhe.cxGrid1DBTableView1DblClick(
  Sender: TObject);
begin

    if (qryProdutoGradenCdProduto.AsString <> '') then
    begin
        qryProduto.Close;
        qryProduto.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva ;
        qryProduto.Parameters.ParamByName('cCdProduto').Value := qryProdutoGradenCdProduto.asString;
        qryProduto.Open;

        edtCdProduto.Text := qryProdutoGradenCdProduto.AsString ;
    end ;

end;

procedure TfrmPDVConsProdutoDetalhe.cxGrid1DBTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin

    if (Key = VK_RETURN) and (qryProdutoGradenCdProduto.AsString <> '') then
    begin

        qryProduto.Close;
        qryProduto.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva ;
        qryProduto.Parameters.ParamByName('cCdProduto').Value := qryProdutoGradenCdProduto.asString;
        qryProduto.Open;

        edtCdProduto.Text := qryProdutoGradenCdProduto.AsString ;

    end ;

end;

procedure TfrmPDVConsProdutoDetalhe.qryProdutoAfterScroll(
  DataSet: TDataSet);
begin

    qryEstoqueLoja.Close;
    qryEstoqueLoja.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva ;
    qryEstoqueLoja.Parameters.ParamByName('nCdProduto').Value := qryProdutonCdProduto.Value;
    qryEstoqueLoja.Open;

    qryBuscaPromocao.Close;
    qryBuscaPromocao.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
    qryBuscaPromocao.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
    qryBuscaPromocao.Parameters.ParamByName('nCdProduto').Value := qryProdutonCdProduto.Value ;
    qryBuscaPromocao.Open;

    Label6.Visible  := ((qryBuscaPromocao.Active) and (qryBuscaPromocaonCdCampanhaPromoc.AsString <> '')) ;
    Label7.Visible  := ((qryBuscaPromocao.Active) and (qryBuscaPromocaonCdCampanhaPromoc.AsString <> '')) ;
    DBEdit3.Visible := ((qryBuscaPromocao.Active) and (qryBuscaPromocaonCdCampanhaPromoc.AsString <> '')) ;
    DBEdit6.Visible := ((qryBuscaPromocao.Active) and (qryBuscaPromocaonCdCampanhaPromoc.AsString <> '')) ;

end;

procedure TfrmPDVConsProdutoDetalhe.cxGridDBTableView2DblClick(
  Sender: TObject);
begin
   if (qryProdutoCoresnCdProduto.AsString <> '') then
    begin
        qrySelectTop.Close;
        qrySelectTop.Parameters.ParamByName('nCdProdutoPai').Value := qryProdutoCoresnCdProduto.asString;
        qrySelectTop.Open;

        edtCdProduto.Text := qrySelectTopnCdProduto.AsString ;
        SelecionaProduto();
    end ;
end;

procedure TfrmPDVConsProdutoDetalhe.qryProdutoAfterCancel(
  DataSet: TDataSet);
begin

    qryBuscaPromocao.Close;

end;

end.
