unit dcCompraRepresPeriodo_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, PivotMap_SRC, PivotCube_SRC, PivotGrid_SRC, PivotToolBar_SRC;

type
  TdcmCompraRepresPeriodo_view = class(TfrmProcesso_Padrao)
    ADODataSet1: TADODataSet;
    PVRowToolBar1: TPVRowToolBar;
    PVMeasureToolBar1: TPVMeasureToolBar;
    PVColToolBar1: TPVColToolBar;
    PivotGrid1: TPivotGrid;
    PivotCube1: TPivotCube;
    PivotMap1: TPivotMap;
    ADODataSet1Ano: TDateTimeField;
    ADODataSet1Mes: TDateTimeField;
    ADODataSet1cNmTerceiroRepres: TStringField;
    ADODataSet1cNmDepartamento: TStringField;
    ADODataSet1cNmCategoria: TStringField;
    ADODataSet1cNmSubCategoria: TStringField;
    ADODataSet1cNmSegmento: TStringField;
    ADODataSet1cNmMarca: TStringField;
    ADODataSet1cNmLinha: TStringField;
    ADODataSet1cNmClasseProduto: TStringField;
    ADODataSet1nValor: TBCDField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dcmCompraRepresPeriodo_view: TdcmCompraRepresPeriodo_view;

implementation

{$R *.dfm}

end.
