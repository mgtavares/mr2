unit fAgrupamentoContabil;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, DBCtrls, ER2Lookup, Mask, Menus;

type
  TfrmAgrupamentoContabil = class(TfrmCadastro_Padrao)
    qryMasternCdAgrupamentoContabil: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMastercNmAgrupamentoContabil: TStringField;
    qryMastercMascara: TStringField;
    qryMasternCdTabTipoAgrupamentoContabil: TIntegerField;
    qryMasternCdStatus: TIntegerField;
    qryMastercFlgAnaliseVertical: TIntegerField;
    qryMasteriMaxNiveis: TIntegerField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    edtEmpresa: TER2LookupDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    edtTipoAgrupamento: TER2LookupDBEdit;
    DBRadioGroup5: TDBRadioGroup;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    DBEdit4: TDBEdit;
    DataSource1: TDataSource;
    Label4: TLabel;
    Label6: TLabel;
    qryTabTipoAgrupamentoContabil: TADOQuery;
    qryTabTipoAgrupamentoContabilnCdTabTipoAgrupamentoContabil: TIntegerField;
    qryTabTipoAgrupamentoContabilcNmTabTipoAgrupamentoContabil: TStringField;
    DBEdit5: TDBEdit;
    DataSource2: TDataSource;
    DBRadioGroup1: TDBRadioGroup;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    qryMasteriAno: TIntegerField;
    Label7: TLabel;
    DBEdit7: TDBEdit;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    PopupMenu1: TPopupMenu;
    DuplicarAgrupamento1: TMenuItem;
    procedure btIncluirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure ToolButton10Click(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure DuplicarAgrupamento1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAgrupamentoContabil: TfrmAgrupamentoContabil;

implementation

uses fMenu, fAgrupamentoContabil_Estrutura, DateUtils,fCopiaAgrupamentoContabil;

{$R *.dfm}

procedure TfrmAgrupamentoContabil.btIncluirClick(Sender: TObject);
begin
  inherited;
  
  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  edtEmpresa.PosicionaQuery;

  qryMasternCdStatus.Value := 1 ;
  
  edtEmpresa.SetFocus;

end;

procedure TfrmAgrupamentoContabil.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'AGRUPAMENTOCONTABIL' ;
  nCdTabelaSistema  := 217 ;
  nCdConsultaPadrao := 219 ;

end;

procedure TfrmAgrupamentoContabil.qryMasterBeforePost(DataSet: TDataSet);
var
  iNiveis, i : integer;
begin

  if (DBEdit4.Text = '') then
  begin
      MensagemAlerta('Selecione a empresa.') ;
      edtEmpresa.SetFocus;
      abort;
  end ;

  if (trim(DBEdit2.Text) = '') then
  begin
      MensagemAlerta('Informe a descri��o do agrupamento.') ;
      DBEdit2.SetFocus;
      abort;
  end ;

  if (trim(DBEdit3.Text) = '') then
  begin
      MensagemAlerta('Informe a m�scara do agrupamento.') ;
      DBEdit3.SetFocus;
      abort;
  end ;

  if (not frmMenu.fnValidaMascaraContabil( DBEdit3.Text )) then
  begin
      MensagemAlerta('M�scara inv�lida.') ;
      DBEdit3.SetFocus;
      abort ;
  end ;

  if (DBEdit5.Text = '') then
  begin
      MensagemAlerta('Selecione o tipo de agrupamento.') ;
      edtTipoAgrupamento.SetFocus;
      abort;
  end ;

  inherited;

  iNiveis := 1 ;

  for i := 0 to length( trim( DBEdit3.Text ) ) do
  begin

      if ( Copy( trim( DBEdit3.Text ) , i , 1 ) = '.') then
          inc( iNiveis ) ;

  end ;

  qryMasteriMaxNiveis.Value := iNiveis;


end;

procedure TfrmAgrupamentoContabil.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  qryTabTipoAgrupamentoContabil.Close;
  
end;

procedure TfrmAgrupamentoContabil.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close;
  qryTabTipoAgrupamentoContabil.Close;

  PosicionaQuery( qryEmpresa , qryMasternCdEmpresa.asString ) ;
  PosicionaQuery( qryTabTipoAgrupamentoContabil , qryMasternCdTabTipoAgrupamentoContabil.AsString ) ;
   
end;

procedure TfrmAgrupamentoContabil.ToolButton10Click(Sender: TObject);
var
  objForm : TfrmAgrupamentoContabil_Estrutura ;
begin
  inherited;

  if (qryMaster.isEmpty) then
  begin
      MensagemAlerta('Selecione um agrupamento cont�bil.') ;
      abort;
  end ;

  if (qryMaster.State in [dsInsert,dsEdit]) then
      qryMaster.Post; 

  objForm := TfrmAgrupamentoContabil_Estrutura.Create(Self) ;
  objForm.nCdAgrupamentoContabil := qryMasternCdAgrupamentoContabil.Value;
  objForm.cNmAgrupamentoContabil := qryMastercNmAgrupamentoContabil.Value;
  objForm.cFormatoMascara        := qryMastercMascara.Value;

  showForm( objForm, TRUE ) ;

end;

procedure TfrmAgrupamentoContabil.btSalvarClick(Sender: TObject);
begin
    DBEdit1.SetFocus;
    
    if (qryMasteriAno.Value < 2000) or (qryMasteriAno.Value > (YearOf(Now)+1))then
    begin
        ShowMessage('Exerc�cio inv�lido');
        Abort;
    end;

  inherited;

end;

procedure TfrmAgrupamentoContabil.DuplicarAgrupamento1Click(
  Sender: TObject);

var
  objForm : TfrmCopiaAgrupamentoContabil;
begin
    if (not qryMaster.Active) or (dsMaster.State = dsInsert)then 
    begin
        MensagemAlerta('Selecione um agrupamento');
        abort;
    end;

    objForm := TfrmCopiaAgrupamentoContabil.Create(nil);
    objForm.nCdAgrupamento := qryMasternCdAgrupamentoContabil.Value;

    showForm(objForm, true);

    qryMaster.Close;

  inherited;

end;

initialization
    registerClass(TfrmAgrupamentoContabil) ;

end.
