inherited frmBaixaColetiva_ConsTitulo: TfrmBaixaColetiva_ConsTitulo
  Left = 77
  Top = 136
  Width = 934
  Height = 594
  BorderIcons = [biSystemMenu]
  Caption = 'Consulta de T'#237'tulos'
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 918
    Height = 255
  end
  inherited ToolBar1: TToolBar
    Width = 918
    ButtonWidth = 114
    inherited ToolButton1: TToolButton
      Caption = '&Adicionar T'#237'tulos'
      ImageIndex = 2
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 114
    end
    inherited ToolButton2: TToolButton
      Left = 122
    end
  end
  object GroupBox2: TGroupBox [2]
    Left = 0
    Top = 29
    Width = 918
    Height = 255
    Align = alClient
    Caption = 'T'#237'tulos em Aberto'
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 2
      Top = 15
      Width = 914
      Height = 238
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        DataController.DataSource = dsTituloPend
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '#,##0.00'
            Kind = skSum
            Column = cxGrid1DBTableView1nSaldoTit
          end>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GridLines = glVertical
        OptionsView.GroupByBox = False
        Styles.Header = frmMenu.Header
        object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
          Caption = 'ID'
          DataBinding.FieldName = 'nCdTitulo'
        end
        object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
          Caption = 'N'#250'm. T'#237'tulo'
          DataBinding.FieldName = 'cNrTit'
        end
        object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
          Caption = 'Dt. Vencto'
          DataBinding.FieldName = 'dDtVenc'
        end
        object cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn
          Caption = 'Esp'#233'cie T'#237'tulo'
          DataBinding.FieldName = 'cNmEspTit'
          Width = 207
        end
        object cxGrid1DBTableView1nSaldoAnterior: TcxGridDBColumn
          Caption = 'Saldo Atual'
          DataBinding.FieldName = 'nSaldoAnterior'
          HeaderAlignmentHorz = taRightJustify
          Width = 105
        end
        object cxGrid1DBTableView1nValJuro: TcxGridDBColumn
          Caption = 'Juros'
          DataBinding.FieldName = 'nValJuro'
          HeaderAlignmentHorz = taRightJustify
        end
        object cxGrid1DBTableView1nValDesconto: TcxGridDBColumn
          Caption = 'Desconto Antecip.'
          DataBinding.FieldName = 'nValDesconto'
          HeaderAlignmentHorz = taRightJustify
        end
        object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
          Caption = 'Saldo Final'
          DataBinding.FieldName = 'nSaldoTit'
          HeaderAlignmentHorz = taRightJustify
          Width = 108
        end
        object cxGrid1DBTableView1cFlgAux: TcxGridDBColumn
          DataBinding.FieldName = 'cFlgAux'
          Visible = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 284
    Width = 918
    Height = 274
    Align = alBottom
    Caption = 'T'#237'tulos Selecionados'
    TabOrder = 2
    object cxGrid2: TcxGrid
      Left = 2
      Top = 15
      Width = 914
      Height = 257
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Consolas'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGridDBTableView1: TcxGridDBTableView
        OnDblClick = cxGridDBTableView1DblClick
        DataController.DataSource = dsTituloSel
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '#,##0.00'
            Kind = skSum
          end
          item
            Format = '#,##0.00'
            Kind = skSum
            Column = cxGridDBTableView1nSaldoTit
          end>
        DataController.Summary.SummaryGroups = <>
        NavigatorButtons.ConfirmDelete = False
        NavigatorButtons.First.Visible = True
        NavigatorButtons.PriorPage.Visible = True
        NavigatorButtons.Prior.Visible = True
        NavigatorButtons.Next.Visible = True
        NavigatorButtons.NextPage.Visible = True
        NavigatorButtons.Last.Visible = True
        NavigatorButtons.Insert.Visible = True
        NavigatorButtons.Delete.Visible = True
        NavigatorButtons.Edit.Visible = True
        NavigatorButtons.Post.Visible = True
        NavigatorButtons.Cancel.Visible = True
        NavigatorButtons.Refresh.Visible = True
        NavigatorButtons.SaveBookmark.Visible = True
        NavigatorButtons.GotoBookmark.Visible = True
        NavigatorButtons.Filter.Visible = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.GridLines = glVertical
        OptionsView.GroupByBox = False
        Styles.Header = frmMenu.Header
        object cxGridDBTableView1nCdTitulo: TcxGridDBColumn
          Caption = 'ID'
          DataBinding.FieldName = 'nCdTitulo'
        end
        object cxGridDBTableView1cNrTit: TcxGridDBColumn
          Caption = 'N'#250'm. T'#237'tulo'
          DataBinding.FieldName = 'cNrTit'
        end
        object cxGridDBTableView1dDtVenc: TcxGridDBColumn
          Caption = 'Dt. Vencto'
          DataBinding.FieldName = 'dDtVenc'
        end
        object cxGridDBTableView1cNmEspTit: TcxGridDBColumn
          Caption = 'Esp'#233'cie T'#237'tulo'
          DataBinding.FieldName = 'cNmEspTit'
          Width = 207
        end
        object cxGridDBTableView1nSaldoAnterior: TcxGridDBColumn
          Caption = 'Saldo Atual'
          DataBinding.FieldName = 'nSaldoAnterior'
          Width = 104
        end
        object cxGridDBTableView1nValJuro: TcxGridDBColumn
          Caption = 'Juros'
          DataBinding.FieldName = 'nValJuro'
          HeaderAlignmentHorz = taRightJustify
        end
        object cxGridDBTableView1nValDesconto: TcxGridDBColumn
          Caption = 'Desconto'
          DataBinding.FieldName = 'nValDesconto'
          HeaderAlignmentHorz = taRightJustify
        end
        object cxGridDBTableView1nSaldoTit: TcxGridDBColumn
          Caption = 'Saldo Final'
          DataBinding.FieldName = 'nSaldoTit'
          HeaderAlignmentHorz = taRightJustify
          Width = 101
        end
        object cxGridDBTableView1cFlgAux: TcxGridDBColumn
          DataBinding.FieldName = 'cFlgAux'
          Visible = False
        end
      end
      object cxGridLevel1: TcxGridLevel
        GridView = cxGridDBTableView1
      end
    end
  end
  inherited ImageList1: TImageList
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000666666003D3D3D003D3D
      3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D
      3D003D3D3D00A7A7A700F0F0F000F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006666660055DFD4003D3D3D0055DF
      FF0055DFFF0055DFFF0055DFFF0055DFFF0055DFD40055DFFF0055DFD40055DF
      FF0055C0D4003D3D3D00F0F0F000F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000084A2100084A2100084A2100084A
      2100084A2100084A2100084A2100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006666660000F2FF0055DFD4003D3D
      3D00A9FFFF0055DFFF0055DFFF0055DFFF0055DFFF0055DFFF0055DFFF0055DF
      D40055DFFF0055DFD4003D3D3D00F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A2100000000000000000000000000000084000000
      8400000084000000840000008400000084000000840000008400000084000000
      8400000084000000840000000000000000007373730054FFFF0000F2FF0055DF
      D4003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D3D003D3D
      3D003D3D3D003D3D3D003D3D3D003D3D3D000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D6A58C00D6A58C00D6A5
      8C00D6A58C00D6A58C00D6A58C00D6A58C000000000052B5F7000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00000084000000840000008400000084000000
      FF000000FF000000840000000000000000007373730054FFFF0054FFFF0000F2
      FF0055DFD40000F2FF0055DFD40099F8FF0099F8FF0099F8FF0099F8FF0099F8
      FF0099F8FF0099F8FF00B4B4B400F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF0052B5F7000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF0000008400000084000000
      84000000840000008400000000000000000081818100A9FFFF0054FFFF0054FF
      FF0000F2FF00EFAD00007F5B0000EFAD0000AAFFFF0099F8FF00AAFFFF0099F8
      FF00AAFFFF0099F8FF0076767600F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0052B5
      F7000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      84000000FF000000840000000000000000008181810054FFFF00A9FFFF0054FF
      FF0054FFFF007F5B0000D9A77D007F5B0000FFFFFF00AAFFFF0099F8FF00AAFF
      FF0099F8FF00AAFFFF0076767600F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE0000000000EFFFFF00EFFFFF00EFFFFF00EFFFFF0000FFFF0000FFFF00EFFF
      FF000000000018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF000000840000000000000000009A9A9A00A9FFFF0054FFFF00EFAD
      0000A2760000A2760000D9A77D00A37700007F5B0000EFAD0000AAFFFF0099F8
      FF00AAFFFF0099F8FF0076767600F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00000000000000000000000000000000000000000000FFFF00EFFFFF000000
      000018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF00000084000000
      84000000FF000000840000000000000000009A9A9A0054FFFF00A9FFFF00AA7F
      0000FFFFCC00D9A77D00D9A77D00D9A77D00D9A77D007F5B0000AAFFFF00AAFF
      FF0099F8FF00AAFFFF0081818100F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C0000000000EFFFFF000000000018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000
      FF000000FF00000084000000000000000000A7A7A700A7A7A70055C0D400F7D0
      6C00E5B72600E2B62900F6CF6D00AA7F0000AA7F0000F7CF6C00AAFFFF0099F8
      FF00AAFFFF0099F8FF008E8E8E00F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000000000000018A54A0018A5
      4A0018A54A0018A54A00084A21000000000000000000000000009C9CFF000000
      FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000FF000000
      FF000000FF00000084000000000000000000F0F0F000A7A7A70000F2FF0000F2
      FF0000F1FF00F1BF2B00FFFFCC00AA7F0000AAFFFF00AAFFFF00AAFFFF0055DF
      FF0055DFFF0055C0D4009A9A9A00F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000018A54A0018A54A0018A5
      4A0018A54A0031636300316363000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF00EFFFFF00EFFFFF0000008400000084000000
      FF000000FF00000084000000000000000000F0F0F0009B9B9B0054FFFF0067F4
      FF0067F4FF00F8D06D00FDC83100F7CF6C00AAFFFF00AAFFFF00B4B4B400A0A0
      A0008D8D8D0081818100A7A7A700F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00A56B5A00A56B5A00D6A58C00000000009CD6B5009CD6B5009CD6
      B5009CD6B50031636300000000000000000000000000000000009C9CFF000000
      FF000000FF000000FF000000FF000000FF00EFFFFF00EFFFFF000000FF000000
      FF000000FF00000084000000000000000000F0F0F000F0F0F0009B9B9B008D8D
      8D008D8D8D00B4B4B40090909000FFFFFF00AAFFFF00FFFFFF00A1A1A100E6E6
      E600DADADA00DADADA00B4B4B400F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00FFEFDE00FFEF
      DE00FFEFDE00FFEFDE00FFEFDE00D6A58C000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009C9CFF009C9C
      FF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9CFF009C9C
      FF009C9CFF009C9CFF000000000000000000F0F0F000F0F0F000F0F0F000F0F0
      F000F0F0F000F0F0F00091919100FFFFFF00FFFFFF00AAFFFF00A7A7A700FFFF
      FF00E7E7E700B4B4B400F0F0F000F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EFFFFF00EFFFFF00EFFF
      FF00EFFFFF00EFFFFF00EFFFFF00EFFFFF000000000000000000000000000000
      0000C6DEC6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F0F0F000F0F0F000F0F0F000F0F0
      F000F0F0F000F0F0F00091919100FFFFFF00FFFFFF00FFFFFF008D8D8D00FFFF
      FF00B4B4B400F0F0F000F0F0F000F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F0F0F000F0F0F000F0F0F000F0F0
      F000F0F0F000F0F0F000B4B4B4009A9A9A009A9A9A008E8E8E0081818100C1C1
      C100F0F0F000F0F0F000F0F0F000F0F0F0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00FFFFFFFF80000000FE00FFFF00000000
      FE00C00300000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000008001000000000000800100000000
      0000800100000000000080010000000000018001000000000003800100000000
      0077C00300000000007FFFFF0000000000000000000000000000000000000000
      000000000000}
  end
  object qryTituloPend: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF OBJECT_ID('#39'tempdb..#TempTitulo'#39') IS NULL'
      'BEGIN'
      ''
      '    CREATE TABLE #TempTitulo (nCdTitulo      int'
      '                             ,cNrTit         varchar(20)'
      '                             ,dDtVenc        datetime'
      '                             ,cNmEspTit      varchar(50)'
      
        '                             ,nSaldoAnterior decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nValJuro       decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nValDesconto   decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nSaldoTit      decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,cFlgAux        int           defau' +
        'lt 0 not null)'
      ''
      'END'
      ''
      ''
      'SELECT *'
      '  FROM #TempTitulo'
      ' WHERE cFlgAux = 0'
      ' ORDER BY dDtVenc, cNrTit')
    Left = 232
    Top = 109
    object qryTituloPendnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTituloPendcNrTit: TStringField
      FieldName = 'cNrTit'
    end
    object qryTituloPenddDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTituloPendcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTituloPendnSaldoAnterior: TBCDField
      FieldName = 'nSaldoAnterior'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloPendnValJuro: TBCDField
      FieldName = 'nValJuro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloPendnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloPendnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloPendcFlgAux: TIntegerField
      FieldName = 'cFlgAux'
    end
  end
  object dsTituloPend: TDataSource
    DataSet = qryTituloPend
    Left = 272
    Top = 109
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF OBJECT_ID('#39'tempdb..#TempTitulo'#39') IS NULL '
      'BEGIN'
      ''
      '    CREATE TABLE #TempTitulo (nCdTitulo      int'
      '                             ,cNrTit         varchar(20)'
      '                             ,dDtVenc        datetime'
      '                             ,cNmEspTit      varchar(50)'
      
        '                             ,nSaldoAnterior decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nValJuro       decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nValDesconto   decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nSaldoTit      decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,cFlgAux        int           defau' +
        'lt 0 not null)'
      ''
      'END')
    Left = 88
    Top = 117
  end
  object dsTituloSel: TDataSource
    DataSet = qryTituloSel
    Left = 272
    Top = 149
  end
  object qryTituloSel: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF OBJECT_ID('#39'tempdb..#TempTitulo'#39') IS NULL'
      'BEGIN'
      ''
      '    CREATE TABLE #TempTitulo (nCdTitulo      int'
      '                             ,cNrTit         varchar(20)'
      '                             ,dDtVenc        datetime'
      '                             ,cNmEspTit      varchar(50)'
      
        '                             ,nSaldoAnterior decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nValJuro       decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nValDesconto   decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nSaldoTit      decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,cFlgAux        int           defau' +
        'lt 0 not null)'
      ''
      'END'
      ''
      ''
      'SELECT *'
      '  FROM #TempTitulo'
      ' WHERE cFlgAux = 1'
      ' ORDER BY dDtVenc, cNrTit')
    Left = 232
    Top = 149
    object qryTituloSelnCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryTituloSelcNrTit: TStringField
      FieldName = 'cNrTit'
    end
    object qryTituloSeldDtVenc: TDateTimeField
      FieldName = 'dDtVenc'
    end
    object qryTituloSelcNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
    object qryTituloSelnSaldoAnterior: TBCDField
      FieldName = 'nSaldoAnterior'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloSelnValJuro: TBCDField
      FieldName = 'nValJuro'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloSelnValDesconto: TBCDField
      FieldName = 'nValDesconto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloSelnSaldoTit: TBCDField
      FieldName = 'nSaldoTit'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTituloSelcFlgAux: TIntegerField
      FieldName = 'cFlgAux'
    end
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 200
    Top = 181
  end
  object qryPopulaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'dDtMov'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'cSenso'
        DataType = ftString
        Size = 1
        Value = 'C'
      end
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end
      item
        Name = 'nCdLoteLiq'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'IF OBJECT_ID('#39'tempdb..#TempTitulo'#39') IS NULL'
      'BEGIN'
      ''
      '    CREATE TABLE #TempTitulo (nCdTitulo      int'
      '                             ,cNrTit         varchar(20)'
      '                             ,dDtVenc        datetime'
      '                             ,cNmEspTit      varchar(50)'
      
        '                             ,nSaldoAnterior decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nValJuro       decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nValDesconto   decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nSaldoTit      decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,cFlgAux        int           defau' +
        'lt 0 not null)'
      ''
      'END'
      ''
      'SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED'
      'SET NOCOUNT ON'
      ''
      'TRUNCATE TABLE #TempTitulo'
      ''
      'DECLARE @dDtMov varchar(10)'
      ''
      'Set @dDtMov = :dDtMov'
      ''
      'INSERT INTO #TempTitulo (nCdTitulo'
      '                        ,cNrTit'
      '                        ,dDtVenc'
      '                        ,cNmEspTit'
      '                        ,nSaldoAnterior'
      '                        ,nValJuro'
      '                        ,nValDesconto'
      '                        ,nSaldoTit'
      '                        ,cFlgAux)'
      '                  SELECT nCdTitulo'
      '                        ,cNrTit'
      '                        ,dDtVenc'
      '                        ,cNmEspTit'
      '                        ,nSaldoTit'
      
        '                        ,dbo.fn_SimulaJurosTitulo(nCdTitulo,Conv' +
        'ert(DATETIME,@dDtMov,103)) + dbo.fn_SimulaMultaTitulo(nCdTitulo)'
      
        '                        ,dbo.fn_SimulaDescontoAntecipTitulo(nCdT' +
        'itulo,Convert(DATETIME,@dDtMov,103))'
      '                        ,0'
      '                        ,0'
      '                    FROM Titulo'
      
        '                         INNER JOIN EspTit ON EspTit.nCdEspTit =' +
        ' Titulo.nCdEspTit'
      '                   WHERE cSenso           = :cSenso'
      '                     AND nCdTerceiro      = :nCdTerceiro'
      '                     AND nSaldoTit        > 0'
      '                     AND dDtCancel       IS NULL'
      '                     AND dDtBloqTit      IS NULL'
      #9#9#9'               AND EspTit.cFlgPrev  = 0'
      '                     AND nCdEmpresa       = :nCdEmpresa'
      '                     AND NOT EXISTS(SELECT 1'
      '                                      FROM TituloLoteLiq'
      
        '                                     WHERE TituloLoteLiq.nCdLote' +
        'Liq = :nCdLoteLiq'
      
        '                                       AND TituloLoteLiq.nCdTitu' +
        'lo  = Titulo.nCdTitulo)'
      ''
      ''
      'UPDATE #TempTitulo'
      '   SET nSaldoTit = nSaldoAnterior + nValJuro - nValDesconto')
    Left = 88
    Top = 165
  end
  object qryGravaTituloSelecionado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdLoteLiq'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'IF OBJECT_ID('#39'tempdb..#TempTitulo'#39') IS NULL'
      'BEGIN'
      ''
      '    CREATE TABLE #TempTitulo (nCdTitulo      int'
      '                             ,cNrTit         varchar(20)'
      '                             ,dDtVenc        datetime'
      '                             ,cNmEspTit      varchar(50)'
      
        '                             ,nSaldoAnterior decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nValJuro       decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nValDesconto   decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,nSaldoTit      decimal(12,2) defau' +
        'lt 0 not null'
      
        '                             ,cFlgAux        int           defau' +
        'lt 0 not null)'
      ''
      'END'
      ''
      'DECLARE @nSaldoLote    DECIMAL(12,2)'
      '       ,@nSaldoTit     DECIMAL(12,2)'
      '       ,@nSaldoAux     DECIMAL(12,2)'
      '       ,@nValAux       DECIMAL(12,2)'
      '       ,@nCdEmpresa    int'
      '       ,@nCdTitulo     int'
      '       ,@nValJuro      DECIMAL(12,2)'
      '       ,@nValDesconto  DECIMAL(12,2)'
      '       ,@nCdLoteLiq    int'
      '       ,@iID           int'
      ''
      'Set @nCdLoteLiq = :nCdLoteLiq'
      ''
      'SELECT @nSaldoLote = nSaldoLote'
      '      ,@nCdEmpresa = nCdEmpresa'
      '  FROM LOTELIQ'
      ' WHERE nCdLoteLiq = @nCdLoteLiq'
      ''
      'DECLARE curTitulos CURSOR'
      '    FOR SELECT nCdTitulo'
      '              ,nValJuro'
      '              ,nValDesconto'
      '              ,nSaldoTit'
      #9#9#9'    FROM #TempTitulo'
      '         WHERE cFlgAux = 1'
      #9#9#9'   ORDER by dDtVenc'
      #9#9#9#9#9'       ,nCdTitulo'
      ''
      'OPEN curTitulos'
      ''
      'FETCH NEXT'
      ' FROM curTitulos'
      ' INTO @nCdTitulo'
      '     ,@nValJuro'
      '     ,@nValDesconto'
      '     ,@nSaldoTit'
      ''
      'Set @nSaldoAux = @nSaldoLote'
      ''
      'WHILE (@@FETCH_STATUS = 0)'
      'BEGIN'
      ''
      '    IF (@nSaldoAux > 0)'
      '    BEGIN'
      ''
      '        IF ((@nSaldoAux - @nSaldoTit) >= 0)'
      '        BEGIN'
      '            SET @nValAux = @nSaldoTit'
      '        END'
      ''
      '        IF ((@nSaldoAux - @nSaldoTit) < 0)'
      '        BEGIN'
      '            SET @nValAux      = @nSaldoAux'
      '            SET @nSaldoTit    = @nSaldoTit + @nValDesconto'
      
        '            SET @nValDesconto = 0 --Quando o saldo n'#227'o '#233' suficie' +
        'nte para liquidar o titulo, zera o desconto'
      '        END'
      ''
      '        Set @nSaldoAux = @nSaldoAux - @nValAux'
      ''
      '        Set @iID = NULL'
      ''
      '        EXEC usp_ProximoID  '#39'TITULOLOTELIQ'#39
      '                           ,@iID OUTPUT'
      ''
      '        INSERT INTO TituloLoteLiq (nCdTituloLoteLiq'
      '                                  ,nCdLoteLiq'
      '                                  ,nCdTitulo'
      '                                  ,nValLiq'
      '                                  ,nSaldoTit'
      '                                  ,nValJuro'
      '                                  ,nValDesconto)'
      '                            VALUES(@iID'
      '                                  ,@nCdLoteLiq'
      '                                  ,@nCdTitulo'
      '                                  ,@nValAux'
      '                                  ,@nSaldoTit'
      '                                  ,@nValJuro'
      '                                  ,@nValDesconto)'
      ''
      '        UPDATE LoteLiq'
      '           SET nValTitLiq = nValTitLiq + @nValAux'
      '              ,nSaldoLote = nSaldoLote - @nValAux'
      '         WHERE nCdLoteLiq = @nCdLoteLiq'
      ''
      '    END'
      ''
      '    FETCH NEXT'
      '     FROM curTitulos'
      '     INTO @nCdTitulo'
      '         ,@nValJuro'
      '         ,@nValDesconto'
      '         ,@nSaldoTit'
      ''
      'END'
      ''
      'CLOSE curTitulos'
      'DEALLOCATE curTitulos'
      ''
      '')
    Left = 88
    Top = 205
  end
end
