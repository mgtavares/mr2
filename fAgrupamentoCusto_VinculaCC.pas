unit fAgrupamentoCusto_VinculaCC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, StdCtrls,
  Mask, ER2Lookup, DB, DBCtrls, ADODB;

type
  TfrmAgrupamentoCusto_VinculaCC = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    edtMascaraSuperior: TEdit;
    Label2: TLabel;
    edtCC: TER2LookupMaskEdit;
    Label3: TLabel;
    edtCodigo: TMaskEdit;
    Label4: TLabel;
    edtMascara: TEdit;
    qrySubConta: TADOQuery;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryCC: TADOQuery;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryProximaSequencia: TADOQuery;
    qryProximaSequenciaiProximaSequencia: TIntegerField;
    qryAgrupamentoCusto: TADOQuery;
    qryContaAgrupamentoCusto: TADOQuery;
    qryAux: TADOQuery;
    qrySubContanCdContaAgrupamentoCusto: TIntegerField;
    qrySubContanCdContaAgrupamentoCustoPai: TIntegerField;
    qrySubContanCdAgrupamentoCusto: TIntegerField;
    qrySubContacMascara: TStringField;
    qrySubContacNmContaAgrupamentoCusto: TStringField;
    qrySubContaiNivel: TIntegerField;
    qrySubContaiSequencia: TIntegerField;
    qrySubContanCdCC: TIntegerField;
    qrySubContacFlgAnalSintetica: TStringField;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    qryCCnCdCC1: TIntegerField;
    qryCCnCdCC2: TIntegerField;
    qryCCcCdCC: TStringField;
    qryCCcCdHie: TStringField;
    qryCCiNivel: TSmallintField;
    qryCCcFlgLanc: TIntegerField;
    qryCCnCdStatus: TIntegerField;
    qryCCnCdServidorOrigem: TIntegerField;
    qryCCdDtReplicacao: TDateTimeField;
    qryAgrupamentoCustonCdAgrupamentoCusto: TIntegerField;
    qryAgrupamentoCustonCdEmpresa: TIntegerField;
    qryAgrupamentoCustocNmAgrupamentoCusto: TStringField;
    qryAgrupamentoCustocMascara: TStringField;
    qryAgrupamentoCustonCdTabTipoAgrupamentoContabil: TIntegerField;
    qryAgrupamentoCustonCdStatus: TIntegerField;
    qryAgrupamentoCustoiMaxNiveis: TIntegerField;
    qryContaAgrupamentoCustonCdContaAgrupamentoCusto: TIntegerField;
    qryContaAgrupamentoCustonCdContaAgrupamentoCustoPai: TIntegerField;
    qryContaAgrupamentoCustonCdAgrupamentoCusto: TIntegerField;
    qryContaAgrupamentoCustocMascara: TStringField;
    qryContaAgrupamentoCustocNmContaAgrupamentoCusto: TStringField;
    qryContaAgrupamentoCustoiNivel: TIntegerField;
    qryContaAgrupamentoCustoiSequencia: TIntegerField;
    qryContaAgrupamentoCustonCdCC: TIntegerField;
    qryContaAgrupamentoCustocFlgAnalSintetica: TStringField;
    procedure novoVinculo( nCdContaAgrupamentoCusto : integer ) ;
    procedure FormShow(Sender: TObject);
    procedure edtCodigoChange(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    function fnRemoveMascara( cString : string ) : string;
  private
    { Private declarations }
    bInsert : boolean;
  public
    { Public declarations }
  end;

var
  frmAgrupamentoCusto_VinculaCC: TfrmAgrupamentoCusto_VinculaCC;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmAgrupamentoCusto_VinculaConta }

procedure TfrmAgrupamentoCusto_VinculaCC.novoVinculo(
  nCdContaAgrupamentoCusto: integer);
var
  cMascaraFormat : string ;
  i : integer ;
  iNivelCorrente : integer ;
begin

    qryContaAgrupamentoCusto.Close;
    PosicionaQuery( qryContaAgrupamentoCusto , '0' ) ;

    bInsert := TRUE ;

    qrySubConta.Close;
    PosicionaQuery( qrySubConta , intToStr( nCdContaAgrupamentoCusto ) ) ;

    qryAgrupamentoCusto.Close;
    PosicionaQuery( qryAgrupamentoCusto , qrySubContanCdAgrupamentoCusto.AsString ) ;

    if not qrySubConta.IsEmpty then
        edtMascaraSuperior.Text := frmMenu.fnAplicaMascaraContabil( qrySubContacMascara.Value , qryAgrupamentoCustocMascara.Value ) ;

    qryProximaSequencia.Close;
    PosicionaQuery( qryProximaSequencia , intToStr( nCdContaAgrupamentoCusto ) ) ;

    if not qryProximaSequencia.IsEmpty then
        edtCodigo.Text := qryProximaSequenciaiProximaSequencia.AsString ;

    {-- de acordo com o n�vel da subconta, gera a m�scara no campo c�digo --}
    cMascaraFormat := '' ;
    iNivelCorrente := 1 ;

    for i := 1 to length( qryAgrupamentoCustocMascara.Value ) do
    begin

        if ( Copy( qryAgrupamentoCustocMascara.Value , i , 1 ) = '.' ) then
        begin

            inc( iNivelCorrente ) ;

            if ( iNivelCorrente > ( qrySubContaiNivel.Value + 1 ) ) then
                break ;

        end
        else
        begin

            if ( iNivelCorrente = ( qrySubContaiNivel.Value + 1 ) ) then
                cMascaraFormat := cMascaraFormat + Copy( qryAgrupamentoCustocMascara.Value , i , 1 ) ;

        end ;

    end ;

    edtCodigo.EditMask := trim( cMascaraFormat ) + ';0;_'  ;
    edtCodigo.Text     := frmMenu.ZeroEsquerda(edtCodigo.Text,(length(edtCodigo.EditMask)-4)) ;

    Self.ShowModal;

end;

procedure TfrmAgrupamentoCusto_VinculaCC.FormShow(Sender: TObject);
begin
  inherited;

  if (not edtCC.ReadOnly) then
      edtCC.SetFocus
  else edtCodigo.SetFocus;
  
end;

procedure TfrmAgrupamentoCusto_VinculaCC.edtCodigoChange(
  Sender: TObject);
begin
  inherited;

  if (edtMascaraSuperior.Text <> '') then
      edtMascara.Text := edtMascaraSuperior.Text + '.' + edtCodigo.Text
  else edtMascara.Text := edtCodigo.Text ;
  
end;

procedure TfrmAgrupamentoCusto_VinculaCC.ToolButton1Click(
  Sender: TObject);
begin
  inherited;

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Selecione um centro de custo.') ;
      edtCC.SetFocus;
      abort;
  end ;

  if (trim( edtCodigo.Text ) = '') then
  begin
      MensagemAlerta('Informe o c�digo da sequ�ncia.') ;
      edtCodigo.SetFocus;
      abort;
  end ;

  if (bInsert) then
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM ContaAgrupamentoCusto WHERE nCdAgrupamentoCusto = ' + qryAgrupamentoCustonCdAgrupamentoCusto.AsString + ' AND cMascara = ' + Chr(39) +  fnRemoveMascara( Trim(edtMascara.Text) ) + Chr(39) ) ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin

          qryAux.Close;
          MensagemAlerta('J� existe uma conta com a m�scara ' + edtMascara.Text + ' neste agrupamento cont�bil. Verifique.') ;
          abort ;

      end ;

  end
  else
  begin

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('SELECT 1 FROM ContaAgrupamentoCusto WHERE nCdAgrupamentoCusto = ' + qryAgrupamentoCustonCdAgrupamentoCusto.AsString + ' AND nCdContaAgrupamentoCusto != ' + qryContaAgrupamentoCustonCdContaAgrupamentoCusto.asString + ' AND cMascara = ' + Chr(39) +  fnRemoveMascara( Trim(edtMascara.Text) ) + Chr(39) ) ;
      qryAux.Open;

      if not qryAux.IsEmpty then
      begin

          qryAux.Close;
          MensagemAlerta('J� existe uma conta com a m�scara ' + edtMascara.Text + ' neste agrupamento cont�bil. Verifique.') ;
          abort ;

      end ;

  end ;

  if ( MessageDlg('Confirma a vincula��o desta conta ?',mtConfirmation,[mbYes,mbNo],0) = MRNO ) then
  begin
      edtCodigo.SetFocus;
      exit;
  end ;

  try
      if ( bInsert ) then
      begin

          qryContaAgrupamentoCusto.Insert ;
          qryContaAgrupamentoCustonCdContaAgrupamentoCusto.Value    := frmMenu.fnProximoCodigo('CONTAAGRUPAMENTOCUSTO') ;
          qryContaAgrupamentoCustonCdAgrupamentoCusto.Value         := qryAgrupamentoCustonCdAgrupamentoCusto.Value ;
          qryContaAgrupamentoCustonCdContaAgrupamentoCustoPai.Value := qrySubContanCdContaAgrupamentoCusto.Value ;

      end
      else qryContaAgrupamentoCusto.Edit ;

      qryContaAgrupamentoCustocMascara.Value                    := fnRemoveMascara( edtMascara.Text );
      qryContaAgrupamentoCustocNmContaAgrupamentoCusto.Value    := qryCCcNmCC.Value;
      qryContaAgrupamentoCustoiNivel.Value                      := ( qrySubContaiNivel.Value + 1 ) ;
      qryContaAgrupamentoCustoiSequencia.Value                  := strToInt( edtCodigo.Text ) ;
      qryContaAgrupamentoCustocFlgAnalSintetica.Value           := 'A' ;
      qryContaAgrupamentoCustonCdCC.Value                       := qryCCnCdCC.Value ;

      qryContaAgrupamentoCusto.Post;
  except
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  Close;
  
end;

function TfrmAgrupamentoCusto_VinculaCC.fnRemoveMascara(
  cString: string): string;
var
  x: Integer;
begin

  result := '';

  for x := 1 to length( cString ) do
      if ( Copy( cString , x , 1 ) <> '.' ) then
          result := result + Copy( cString , x , 1 ) ;

end;

end.
