unit rFichaCobradora_Individual_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, ADODB, QuickRpt, QRCtrls, ExtCtrls;

type
  TrptFichaCobradora_Individual_view = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand3: TQRBand;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText55: TQRDBText;
    QRDBText56: TQRDBText;
    QRGroup1: TQRGroup;
    QRDBText1: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText8: TQRDBText;
    QRBand2: TQRBand;
    QRLabel13: TQRLabel;
    QRExpr1: TQRExpr;
    QRExpr2: TQRExpr;
    QRBand4: TQRBand;
    QRBand5: TQRBand;
    QRShape5: TQRShape;
    QRLabel15: TQRLabel;
    QRLabel71: TQRLabel;
    QRChildBand1: TQRChildBand;
    QRDBText11: TQRDBText;
    QRLabel11: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText14: TQRDBText;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRDBText15: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRLabel20: TQRLabel;
    QRDBText18: TQRDBText;
    QRLabel21: TQRLabel;
    QRDBText19: TQRDBText;
    QRLabel22: TQRLabel;
    QRDBText9: TQRDBText;
    QRLabel23: TQRLabel;
    QRDBText20: TQRDBText;
    QRLabel24: TQRLabel;
    QRLabel25: TQRLabel;
    QRDBText21: TQRDBText;
    QRLabel26: TQRLabel;
    QRDBText22: TQRDBText;
    QRLabel27: TQRLabel;
    QRDBText24: TQRDBText;
    QRLabel29: TQRLabel;
    QRLabel30: TQRLabel;
    QRDBText25: TQRDBText;
    QRLabel31: TQRLabel;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    QRLabel32: TQRLabel;
    QRDBText28: TQRDBText;
    QRLabel33: TQRLabel;
    QRDBText29: TQRDBText;
    QRLabel34: TQRLabel;
    QRDBText31: TQRDBText;
    QRLabel37: TQRLabel;
    QRDBText32: TQRDBText;
    QRLabel38: TQRLabel;
    QRDBText23: TQRDBText;
    QRLabel28: TQRLabel;
    QRDBText30: TQRDBText;
    QRLabel35: TQRLabel;
    QRDBText33: TQRDBText;
    QRLabel39: TQRLabel;
    QRLabel40: TQRLabel;
    QRDBText34: TQRDBText;
    QRLabel41: TQRLabel;
    QRDBText35: TQRDBText;
    QRLabel42: TQRLabel;
    QRLabel43: TQRLabel;
    QRDBText36: TQRDBText;
    QRDBText41: TQRDBText;
    QRLabel48: TQRLabel;
    QRDBText42: TQRDBText;
    QRLabel49: TQRLabel;
    QRDBText43: TQRDBText;
    QRLabel50: TQRLabel;
    QRDBText44: TQRDBText;
    QRLabel51: TQRLabel;
    QRDBText45: TQRDBText;
    QRLabel52: TQRLabel;
    QRLabel53: TQRLabel;
    QRDBText46: TQRDBText;
    QRDBText47: TQRDBText;
    QRLabel55: TQRLabel;
    QRDBText37: TQRDBText;
    QRLabel44: TQRLabel;
    QRLabel36: TQRLabel;
    QRDBText51: TQRDBText;
    QRDBText52: TQRDBText;
    QRDBText53: TQRDBText;
    QRDBText54: TQRDBText;
    QRLabel66: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel68: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel70: TQRLabel;
    QRLabel12: TQRLabel;
    QRChildBand2: TQRChildBand;
    QRDBText13: TQRDBText;
    QRLabel16: TQRLabel;
    QRLabel45: TQRLabel;
    QRDBText38: TQRDBText;
    QRDBText39: TQRDBText;
    QRLabel46: TQRLabel;
    QRDBText40: TQRDBText;
    QRLabel47: TQRLabel;
    QRDBText48: TQRDBText;
    QRLabel54: TQRLabel;
    QRLabel56: TQRLabel;
    QRDBText49: TQRDBText;
    QRDBText50: TQRDBText;
    QRLabel57: TQRLabel;
    QRLabel59: TQRLabel;
    QRLabel60: TQRLabel;
    QRLabel61: TQRLabel;
    QRLabel62: TQRLabel;
    QRLabel63: TQRLabel;
    QRLabel64: TQRLabel;
    QRLabel65: TQRLabel;
    QRLabel67: TQRLabel;
    QRLabel58: TQRLabel;
    QRLabel69: TQRLabel;
    qryFicha: TADOQuery;
    qryFichanCdTerceiro: TStringField;
    qryFichacFlgTipoPessoa: TStringField;
    qryFichacNmTerceiro: TStringField;
    qryFichacCNPJCPF: TStringField;
    qryFichacRG: TStringField;
    qryFichacNmTabTipoSexo: TStringField;
    qryFichacNmTabTipoEstadoCivil: TStringField;
    qryFichacEndereco: TStringField;
    qryFichacComplemento: TStringField;
    qryFichacBairro: TStringField;
    qryFichacCidade: TStringField;
    qryFichacCEP: TStringField;
    qryFichacTelefone1: TStringField;
    qryFichacTelefoneMovel: TStringField;
    qryFichadDtNasc: TDateTimeField;
    qryFichacNmEmpTrab: TStringField;
    qryFichacTelefoneEmpTrab: TStringField;
    qryFichacRamalEmpTrab: TStringField;
    qryFichacEnderecoEmpTrab: TStringField;
    qryFichacBairroEmpTrab: TStringField;
    qryFichacCidadeEmpTrab: TStringField;
    qryFichacCEPEmpTrab: TStringField;
    qryFichacCargoTrab: TStringField;
    qryFichadDtAdmissao: TDateTimeField;
    qryFichanValRendaBruta: TBCDField;
    qryFichacNmCjg: TStringField;
    qryFichadDtNascCjg: TDateTimeField;
    qryFichacRGCjg: TStringField;
    qryFichacUFRGCjg: TStringField;
    qryFichacCPFCjg: TStringField;
    qryFichacTelefoneCelCjg: TStringField;
    qryFichacNmEmpresaTrabCjg: TStringField;
    qryFichacTelefoneEmpTrabCjg: TStringField;
    qryFichacRamalEmpTrabCjg: TStringField;
    qryFichacCargoTrabCjg: TStringField;
    qryFichadDtAdmissaoCjg: TDateTimeField;
    qryFichanValRendaBrutaCjg: TBCDField;
    qryFichacNmPai: TStringField;
    qryFichacNmMae: TStringField;
    qryFichanCdTitulo: TStringField;
    qryFichacNrTit: TStringField;
    qryFichacNmEspTit: TStringField;
    qryFichadDtEmissao: TDateTimeField;
    qryFichadDtVenc: TDateTimeField;
    qryFichaiParcela: TStringField;
    qryFichacTelefoneRes: TStringField;
    qryFichacTelefoneRec1: TStringField;
    qryFichacTelefoneRec2: TStringField;
    qryFichacNmContatoRec1: TStringField;
    qryFichacNmContatoRec2: TStringField;
    qryFichanValOriginal: TBCDField;
    qryFichanValEsperado: TBCDField;
    qryFichanCdLojaTit: TStringField;
    procedure QRGroup1AfterPrint(Sender: TQRCustomBand;
      BandPrinted: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptFichaCobradora_Individual_view: TrptFichaCobradora_Individual_view;

implementation

{$R *.dfm}

procedure TrptFichaCobradora_Individual_view.QRGroup1AfterPrint(
  Sender: TQRCustomBand; BandPrinted: Boolean);
begin
    qrChildBand1.Enabled := (qryFichacFlgTipoPessoa.Value = 'F') ;
    qrChildBand2.Enabled := (qryFichacFlgTipoPessoa.Value = 'J') ;

end;

end.
