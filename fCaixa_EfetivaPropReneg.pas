unit fCaixa_EfetivaPropReneg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxLookAndFeelPainters, cxCurrencyEdit, StdCtrls, cxButtons,
  cxControls, cxContainer, cxEdit, cxTextEdit, jpeg, ExtCtrls, DB, ADODB;

type
  TfrmCaixa_EfetivaPropReneg = class(TForm)
    Label2: TLabel;
    Image2: TImage;
    Label3: TLabel;
    edtQtdeParcelas: TcxTextEdit;
    btnConfirmar: TcxButton;
    btnCancelar: TcxButton;
    edtValEntrada: TcxCurrencyEdit;
    Label4: TLabel;
    edtQtdeParcelasMax: TcxTextEdit;
    edtSaldoParcelado: TcxCurrencyEdit;
    Label5: TLabel;
    edtValParcela: TcxCurrencyEdit;
    Label6: TLabel;
    edtValNegociado: TcxCurrencyEdit;
    Label7: TLabel;
    edtValEntradaMin: TcxCurrencyEdit;
    Label10: TLabel;
    qryPropostaReneg: TADOQuery;
    qryPropostaRenegnCdPropostaReneg: TAutoIncField;
    qryPropostaRenegnCdEmpresa: TIntegerField;
    qryPropostaRenegnCdLoja: TIntegerField;
    qryPropostaRenegnCdTerceiro: TIntegerField;
    qryPropostaRenegdDtProposta: TDateTimeField;
    qryPropostaRenegnCdUsuarioGeracao: TIntegerField;
    qryPropostaRenegnCdUsuarioAprov: TIntegerField;
    qryPropostaRenegdDtAprov: TDateTimeField;
    qryPropostaRenegdDtValidade: TDateTimeField;
    qryPropostaRenegnCdTabStatusPropostaReneg: TIntegerField;
    qryPropostaRenegnCdItemListaCobranca: TIntegerField;
    qryPropostaRenegnCdLojaEfetiv: TIntegerField;
    qryPropostaRenegnCdUsuarioEfetiv: TIntegerField;
    qryPropostaRenegdDtEfetiv: TDateTimeField;
    qryPropostaRenegnCdUsuarioCancel: TIntegerField;
    qryPropostaRenegdDtCancel: TDateTimeField;
    qryPropostaRenegiDiasAtraso: TIntegerField;
    qryPropostaRenegiParcelas: TIntegerField;
    qryPropostaRenegnValOriginal: TBCDField;
    qryPropostaRenegnValJuros: TBCDField;
    qryPropostaRenegnValDescMaxOriginal: TBCDField;
    qryPropostaRenegnValDescOriginal: TBCDField;
    qryPropostaRenegnValDescMaxJuros: TBCDField;
    qryPropostaRenegnValDescJuros: TBCDField;
    qryPropostaRenegnSaldoNegociado: TBCDField;
    qryPropostaRenegnValEntrada: TBCDField;
    qryPropostaRenegnValEntradaMin: TBCDField;
    qryPropostaRenegnValParcela: TBCDField;
    qryPropostaRenegnValParcelaMin: TBCDField;
    qryPropostaRenegcOBS: TMemoField;
    procedure exibeProposta(nCdPropostaReneg:integer);
    procedure FormShow(Sender: TObject);
    procedure btnCancelarClick(Sender: TObject);
    procedure edtValEntradaKeyPress(Sender: TObject; var Key: Char);
    procedure edtQtdeParcelasKeyPress(Sender: TObject; var Key: Char);
    procedure btnConfirmarClick(Sender: TObject);
    procedure edtQtdeParcelasExit(Sender: TObject);
    procedure edtValEntradaExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    bEfetivarProposta : boolean ;
  end;

var
  frmCaixa_EfetivaPropReneg: TfrmCaixa_EfetivaPropReneg;

implementation

uses fMenu;

{$R *.dfm}

{ TfrmCaixa_EfetivaPropReneg }

procedure TfrmCaixa_EfetivaPropReneg.exibeProposta(
  nCdPropostaReneg: integer);
begin

    qryPropostaReneg.Close;
    qryPropostaReneg.Parameters.ParamByName('nPK').Value := nCdPropostaReneg ;
    qryPropostaReneg.Open;

    if not qryPropostaReneg.eof then
    begin

        edtValNegociado.Value    := qryPropostaRenegnSaldoNegociado.Value;
        edtValEntrada.Value      := qryPropostaRenegnValEntrada.Value;
        edtValEntradaMin.Value   := qryPropostaRenegnValEntrada.Value;
        edtSaldoParcelado.Value  := (qryPropostaRenegnSaldoNegociado.Value - qryPropostaRenegnValEntrada.Value) ;
        edtQtdeParcelas.Text     := qryPropostaRenegiParcelas.asString ;
        edtQtdeParcelasMax.Text  := qryPropostaRenegiParcelas.asString ;
        edtValParcela.Value      := qryPropostaRenegnValParcela.Value ;

    end ;

    Self.ShowModal;

end;

procedure TfrmCaixa_EfetivaPropReneg.FormShow(Sender: TObject);
begin

    bEfetivarProposta := False ;
    
    edtValEntrada.SetFocus ;

end;

procedure TfrmCaixa_EfetivaPropReneg.btnCancelarClick(Sender: TObject);
begin

    if (frmMenu.MessageDlg('Deseja cancelar a efetiva��o desta proposta ?',mtConfirmation,[mbYes,mbNo],0)=MrYes) then
        Close ;

end;

procedure TfrmCaixa_EfetivaPropReneg.edtValEntradaKeyPress(Sender: TObject;
  var Key: Char);
begin

    if (key = #13) then
        edtQtdeParcelas.SetFocus;

end;

procedure TfrmCaixa_EfetivaPropReneg.edtQtdeParcelasKeyPress(
  Sender: TObject; var Key: Char);
begin

    if (key = #13) then
        btnConfirmar.SetFocus;

end;

procedure TfrmCaixa_EfetivaPropReneg.btnConfirmarClick(Sender: TObject);
begin

    if (edtValEntrada.Value > edtValNegociado.Value) then
    begin
        frmMenu.MensagemErro('Valor da entrada maior que o valor total renegociado.') ;
        edtValEntrada.SetFocus;
        abort;
    end ;

    if (edtValEntrada.Value < edtValEntradaMin.Value) then
    begin
        frmMenu.MensagemErro('Valor da entrada menor que o valor m�nimo negociado.') ;
        edtValEntrada.SetFocus;
        abort;
    end ;

    if (StrToint(edtQtdeParcelas.Text) > StrToint(edtQtdeParcelasMax.Text)) then
    begin
        frmMenu.MensagemErro('Quantidade de parcelas maior que a quantidade de parcelas negociada.') ;
        edtQtdeParcelas.SetFocus;
        abort;
    end ;

    if (StrToInt(edtQtdeParcelas.Text) < 0) then
    begin
        frmMenu.MensagemErro('Quantidade de parcelas inv�lida.') ;
        edtQtdeParcelas.SetFocus;
        abort;
    end ;

    if (frmMenu.MessageDlg('Confirma a efetiva��o desta renegocia��o ?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
    begin
        edtValEntrada.SetFocus;
        exit ;
    end ;

    if (edtSaldoParcelado.Value = 0) then
    begin
        edtQtdeParcelas.Text := '0' ;
        edtValParcela.Value  := 0;
    end ;

    bEfetivarProposta := True ;

    Close;

end;

procedure TfrmCaixa_EfetivaPropReneg.edtQtdeParcelasExit(Sender: TObject);
begin

    try
        StrToInt(edtQtdeParcelas.Text);
    except
        edtQtdeParcelas.Text := edtQtdeParcelasMax.Text;
        frmMenu.MensagemErro('Quantidade de parcelas inv�lida.') ;
    end ;

    if (StrToInt(edtQtdeParcelas.Text) <= 0) and (edtSaldoParcelado.Value > 0) then
        edtQtdeParcelas.Text := '1' ;

    if (edtSaldoParcelado.Value <= 0) then
    begin
        edtValParcela.Value  := 0 ;
        edtQtdeParcelas.Text := '0' ;
    end
    else
    begin
        edtValParcela.Value := frmMenu.TBRound(edtSaldoParcelado.Value / StrToInt(edtQtdeParcelas.Text),2);
    end ;

end;

procedure TfrmCaixa_EfetivaPropReneg.edtValEntradaExit(Sender: TObject);
begin

    if (edtValEntrada.Value < 0) then
    begin
        frmMenu.MensagemErro('Valor da entrada n�o pode ser menor que zero.') ;
        edtValEntrada.Value := 0 ;
    end ;

    if (edtValEntrada.Value > edtValNegociado.Value) then
    begin
        frmMenu.MensagemErro('Valor da entrada n�o pode ser maior que o valor renegociado.');
        edtValEntrada.Value := edtValNegociado.Value;
    end ;

    edtSaldoParcelado.Value := edtValNegociado.Value - edtValEntrada.Value ;

    if (StrToInt(edtQtdeParcelas.Text) <= 0) and (edtSaldoParcelado.Value > 0) then
        edtQtdeParcelas.Text := '1' ;

    if (edtSaldoParcelado.Value <= 0) then
    begin
        edtValParcela.Value  := 0 ;
        edtQtdeParcelas.Text := '0' ;
    end
    else
    begin
        edtValParcela.Value := frmMenu.TBRound(edtSaldoParcelado.Value / StrToInt(edtQtdeParcelas.Text),2);
    end ;

end;

end.
