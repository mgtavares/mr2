inherited frmBaixaProvisao_FormaPagto: TfrmBaixaProvisao_FormaPagto
  Left = 241
  Top = 137
  Width = 466
  Height = 384
  BorderIcons = [biSystemMenu]
  Caption = 'Formas de Pagamento Provisionadas'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 450
    Height = 317
  end
  inherited ToolBar1: TToolBar
    Width = 450
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 450
    Height = 317
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsFormaPagto
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    FooterRowCount = 1
    IndicatorOptions = [gioShowRowIndicatorEh]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    SumList.Active = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'cNmFormaPagto'
        Footers = <>
        Width = 264
      end
      item
        EditButtons = <>
        FieldName = 'nValTotal'
        Footer.Font.Charset = DEFAULT_CHARSET
        Footer.Font.Color = clWindowText
        Footer.Font.Height = -11
        Footer.Font.Name = 'Consolas'
        Footer.Font.Style = []
        Footers = <>
        Width = 144
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryFormaPagto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtPagto'
        DataType = ftString
        Size = 10
        Value = '01/01/1900'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdContaBancaria'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdEmpresa int'
      '       ,@dDtPagto   VARCHAR(10)'
      '       ,@nCdUsuario int'
      '       ,@nCdContaBancaria int'
      ''
      'Set @nCdEmpresa       = :nCdEmpresa'
      'Set @dDtPagto         = :dDtPagto'
      'Set @nCdUsuario       = :nCdUsuario'
      'Set @nCdContaBancaria = :nCdContaBancaria'
      ''
      'SELECT cNmFormaPagto'
      '      ,ProvisaoTit.nCdFormaPagto'
      '      ,Sum(nValProvisao) nValTotal'
      '  FROM ProvisaoTit'
      
        '       INNER JOIN FormaPagto    ON FormaPagto.nCdFormaPagto     ' +
        '  = ProvisaoTit.nCdFormaPagto'
      
        '       LEFT  JOIN ContaBancaria ON ContaBancaria.nCdContaBancari' +
        'a = ProvisaoTit.nCdContaBancaria'
      ' WHERE dDtCancel   IS NULL'
      '   AND cFlgBaixado  = 0'
      '   AND dDtPagto     = Convert(DATETIME,@dDtPagto,103)'
      '   AND ProvisaoTit.nCdEmpresa = @nCdEmpresa'
      '   AND ProvisaoTit.nCdContaBancaria = @nCdContaBancaria'
      '   AND (EXISTS(SELECT 1'
      '                FROM UsuarioContaBancaria UCB'
      
        '               Where UCB.nCdContaBancaria = ContaBancaria.nCdCon' +
        'taBancaria'
      
        '                 AND UCB.nCdUsuario = @nCdUsuario) OR (cFlgCofre' +
        ' = 1 AND (   nCdUsuarioOperador IS NULL'
      
        '                                                                ' +
        '          OR nCdUsuarioOperador = @nCdUsuario)))'
      ' GROUP BY cNmFormaPagto'
      '         ,ProvisaoTit.nCdFormaPagto'
      '')
    Left = 160
    Top = 128
    object qryFormaPagtocNmFormaPagto: TStringField
      DisplayLabel = 'Formas de Pagamento|Descri'#231#227'o'
      FieldName = 'cNmFormaPagto'
      Size = 50
    end
    object qryFormaPagtonValTotal: TBCDField
      DisplayLabel = 'Formas de Pagamento|Valor'
      FieldName = 'nValTotal'
      ReadOnly = True
      Precision = 32
      Size = 2
    end
    object qryFormaPagtonCdFormaPagto: TIntegerField
      FieldName = 'nCdFormaPagto'
    end
  end
  object dsFormaPagto: TDataSource
    DataSet = qryFormaPagto
    Left = 192
    Top = 128
  end
end
