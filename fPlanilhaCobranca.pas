unit fPlanilhaCobranca;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, GridsEh,
  DBGridEh, StdCtrls, DB, ADODB, cxControls, cxContainer, cxEdit,
  cxTextEdit, PrnDbgeh, Mask, DBCtrls, DBGridEhGrouping;

type
  TfrmPlanilhaCobranca = class(TfrmProcesso_Padrao)
    Panel1: TPanel;
    DBGridEh1: TDBGridEh;
    RadioGroup1: TRadioGroup;
    SP_PLANILHA_COBRANCA: TADOStoredProc;
    SP_PLANILHA_COBRANCAnCdTerceiro: TIntegerField;
    SP_PLANILHA_COBRANCAcNmTerceiro: TStringField;
    SP_PLANILHA_COBRANCAcUF: TStringField;
    SP_PLANILHA_COBRANCAnTotalVencer: TBCDField;
    SP_PLANILHA_COBRANCAnTotalVencido: TBCDField;
    SP_PLANILHA_COBRANCAnTotalCD: TBCDField;
    SP_PLANILHA_COBRANCAnTotal: TBCDField;
    SP_PLANILHA_COBRANCAnPerc: TBCDField;
    SP_PLANILHA_COBRANCAnFatMes1: TBCDField;
    SP_PLANILHA_COBRANCAnFatMes2: TBCDField;
    SP_PLANILHA_COBRANCAnFatMes3: TBCDField;
    SP_PLANILHA_COBRANCAnFatMes4: TBCDField;
    SP_PLANILHA_COBRANCAnFatMes5: TBCDField;
    SP_PLANILHA_COBRANCAnFatMes6: TBCDField;
    DataSource1: TDataSource;
    SP_PLANILHA_COBRANCAdDtProxAcao: TDateTimeField;
    Label37: TLabel;
    cxTextEdit2: TcxTextEdit;
    PrintDBGridEh1: TPrintDBGridEh;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    SP_PLANILHA_COBRANCAcTelefone1: TStringField;
    SP_PLANILHA_COBRANCAcTelefone2: TStringField;
    SP_PLANILHA_COBRANCAcTelefoneMovel: TStringField;
    SP_PLANILHA_COBRANCAcFax: TStringField;
    SP_PLANILHA_COBRANCAcEmail: TStringField;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    SP_PLANILHA_COBRANCAcFlgAcaoHoje: TIntegerField;
    Label6: TLabel;
    cxTextEdit1: TcxTextEdit;
    SP_PLANILHA_COBRANCAnTotalVenc7D: TBCDField;
    SP_PLANILHA_COBRANCAnTotalVenc15D: TBCDField;
    procedure ToolButton1Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure ToolButton5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPlanilhaCobranca: TfrmPlanilhaCobranca;

implementation

uses fMenu, fPlanilhaCobranca_Ocorrencias, rPlanilhaCobranca_view;

{$R *.dfm}
procedure TfrmPlanilhaCobranca.ToolButton1Click(Sender: TObject);
begin
  inherited;

  try
      SP_PLANILHA_COBRANCA.Close ;
      SP_PLANILHA_COBRANCA.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      SP_PLANILHA_COBRANCA.Parameters.ParamByName('@cFlgTipo').Value   := RadioGroup1.ItemIndex;
      SP_PLANILHA_COBRANCA.Open ;
  except
      raise ;
  end ;

end;

procedure TfrmPlanilhaCobranca.DBGridEh1DblClick(Sender: TObject);
var
  objForm : TfrmPlanilhaCobranca_Ocorrencias ;
begin
  inherited;

  if SP_PLANILHA_COBRANCA.Active and (SP_PLANILHA_COBRANCAnCdTerceiro.Value > 0) then
  begin
      objForm := TfrmPlanilhaCobranca_Ocorrencias.Create(nil) ;

      PosicionaQuery(objForm.qryFollowUp, SP_PLANILHA_COBRANCAnCdTerceiro.AsString) ;
      objForm.nCdTerceiro := SP_PLANILHA_COBRANCAnCdTerceiro.Value ;
      showForm ( objForm , TRUE ) ;
      
  end ;

end;

procedure TfrmPlanilhaCobranca.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  if (SP_PLANILHA_COBRANCAdDtProxAcao.Value = StrToDate(DateToStr(Now()))) then
  begin

    if (DataCol = 0) or (DataCol = 1) then
    begin
      DBGridEh1.Canvas.Brush.Color := $0080FFFF ; //$00A6FFFF;
      DBGridEh1.Canvas.Font.Color  := clBlue ;
      DBGridEh1.Canvas.FillRect(Rect);
      DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
    end ;

  end ;

  if (SP_PLANILHA_COBRANCAcFlgAcaoHoje.Value = 1) then
  begin

    if (DataCol = 0) or (DataCol = 1) then
    begin
      DBGridEh1.Canvas.Brush.Color := $00C7AE9E ; //$00A6FFFF;
      DBGridEh1.Canvas.Font.Color  := clWhite ;
      DBGridEh1.Canvas.FillRect(Rect);
      DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
    end ;

  end ;

end;

procedure TfrmPlanilhaCobranca.ToolButton5Click(Sender: TObject);
begin
  inherited;

  frmMenu.ImprimeDBGrid(DBGridEh1,'Planilha de Cobran�a');
end;

initialization
    RegisterClass(TfrmPlanilhaCobranca) ;

end.
