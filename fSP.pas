unit fSP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, DB, ADODB, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, ImgList, RpCon, RpConDS,
  RpDefine, RpRave, cxPC, cxControls, cxLookAndFeelPainters, cxButtons,
  DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmSP = class(TfrmCadastro_Padrao)
    qryMasternCdSP: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMastercNrTit: TStringField;
    qryMastercSerieTit: TStringField;
    qryMasterdDtReceb: TDateTimeField;
    qryMasterdDtEmissao: TDateTimeField;
    qryMasternValSP: TBCDField;
    qryMasterdDtCad: TDateTimeField;
    qryMasterdDtLiber: TDateTimeField;
    qryMasterdDtAutor: TDateTimeField;
    qryMasterdDtCancel: TDateTimeField;
    qryTerceiro: TADOQuery;
    qryEmpresa: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit8: TDBEdit;
    Label7: TLabel;
    DBEdit9: TDBEdit;
    Label8: TLabel;
    DBEdit10: TDBEdit;
    Label9: TLabel;
    DBEdit11: TDBEdit;
    Label10: TLabel;
    DBEdit12: TDBEdit;
    Label11: TLabel;
    DBEdit13: TDBEdit;
    Label12: TLabel;
    DBEdit14: TDBEdit;
    Label13: TLabel;
    DBEdit15: TDBEdit;
    Label14: TLabel;
    DBEdit16: TDBEdit;
    qryUsuarioTipoSP: TADOQuery;
    qryUsuarioTipoSPnCdTipoSP: TIntegerField;
    qryUsuarioTipoSPcNmTipoSP: TStringField;
    qryUsuarioTipoSPcFlgTipoSP: TStringField;
    qryUsuarioTipoSPcFlgExigeNF: TIntegerField;
    qryUsuarioTipoSPcFlgImposto: TIntegerField;
    qryUsuarioTipoSPnCdEspTit: TIntegerField;
    qryTerceironCdStatus: TIntegerField;
    qryMasternCdTipoSP: TIntegerField;
    Label3: TLabel;
    DBEdit17: TDBEdit;
    qryValidaTitulo: TADOQuery;
    qryItemSP: TADOQuery;
    qryItemSPnCdItemSP: TAutoIncField;
    qryItemSPnCdSP: TIntegerField;
    qryItemSPdDtRef: TDateTimeField;
    qryItemSPnValPagto: TBCDField;
    qryItemSPnCdCategFinanc: TIntegerField;
    qryItemSPnCdCC: TIntegerField;
    qryItemSPnCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegocio: TADOQuery;
    qryCC: TADOQuery;
    qryCategFinanc: TADOQuery;
    qryUnidadeNegocionCdUnidadeNegocio: TIntegerField;
    qryUnidadeNegociocNmUnidadeNegocio: TStringField;
    qryCategFinancnCdCategFinanc: TIntegerField;
    qryCategFinanccNmCategFinanc: TStringField;
    qryCategFinancnCdGrupoCategFinanc: TIntegerField;
    qryCategFinanccFlgRecDes: TStringField;
    dsItemSP: TDataSource;
    qryValidaCC: TADOQuery;
    qryPrazoSP: TADOQuery;
    dsPrazoSP: TDataSource;
    qryPrazoSPnCdPrazoSP: TAutoIncField;
    qryPrazoSPnCdSP: TIntegerField;
    qryPrazoSPdDtVenc: TDateTimeField;
    qryPrazoSPnValPagto: TBCDField;
    qryMasternCdUsuarioCad: TIntegerField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryUsuario: TADOQuery;
    qryUsuariocNmUsuario: TStringField;
    dsUsuario: TDataSource;
    DBEdit19: TDBEdit;
    usp_AutorizaSP: TADOStoredProc;
    qryMasternCdUsuarioCancel: TIntegerField;
    qryMasterdDtContab: TDateTimeField;
    Label15: TLabel;
    DBEdit20: TDBEdit;
    qryParametro: TADOQuery;
    qryParametrocValor: TStringField;
    qryMasternCdContratoFinanc: TIntegerField;
    Label16: TLabel;
    DBEdit21: TDBEdit;
    qryPrazoSPcNrTit: TStringField;
    qryPrazoSPcFlgDocCobranca: TIntegerField;
    Label17: TLabel;
    qryItemSPcNmCategFinanc: TStringField;
    qryItemSPcNmCC: TStringField;
    qryCCnCdCC: TIntegerField;
    qryCCcNmCC: TStringField;
    qryItemSPcNmUnidadeNegocio: TStringField;
    DBEdit6: TDBEdit;
    dsTerceiro: TDataSource;
    dsEmpresa: TDataSource;
    SP_GERA_ALERTA: TADOStoredProc;
    qryMasternCdLojaSP: TIntegerField;
    Label18: TLabel;
    DBEdit3: TDBEdit;
    qryLoja: TADOQuery;
    qryLojacNmLoja: TStringField;
    qryLojanCdLoja: TIntegerField;
    DBEdit4: TDBEdit;
    dsLoja: TDataSource;
    qryUsuarioAut: TADOQuery;
    StringField1: TStringField;
    DBEdit22: TDBEdit;
    dsUsuarioAut: TDataSource;
    qryMastercFlgAutomatica: TIntegerField;
    qryMastercOBSSP: TMemoField;
    qryFormaPagto: TADOQuery;
    qryMasterdDtIntegracao: TDateTimeField;
    qryMasternCdFormaPagtoSP: TIntegerField;
    Label19: TLabel;
    DBEdit23: TDBEdit;
    qryFormaPagtonCdFormaPagto: TIntegerField;
    qryFormaPagtocNmFormaPagto: TStringField;
    DBEdit24: TDBEdit;
    dsFormaPagto: TDataSource;
    qryTerceironCdFormaPagto: TIntegerField;
    qryPrazoSPcCodBarra: TStringField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    cxPageControl2: TcxPageControl;
    cxTabSheet2: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cxTabSheet3: TcxTabSheet;
    DBMemo1: TDBMemo;
    DBGridEh2: TDBGridEh;
    btLiberarSP: TcxButton;
    btCancelarSP: TcxButton;
    btImprimirSP: TcxButton;
    qryItemSPcObs: TStringField;
    qryUsuarioTipoSPcHistoricoPadrao: TStringField;
    DBEdit2: TDBEdit;
    dsUsuarioTipoSP: TDataSource;
    Label2: TLabel;
    DBEdit7: TDBEdit;
    DBEdit18: TDBEdit;
    qryItemSPnCdProjeto: TIntegerField;
    qryItemSPcNmProjeto: TStringField;
    qryProjeto: TADOQuery;
    qryProjetonCdProjeto: TIntegerField;
    qryProjetocNmProjeto: TStringField;
    qryProjetonCdStatus: TIntegerField;
    qryProjetoCategFinanc: TADOQuery;
    qryProjetoCategFinanciRetorno: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure DBEdit17KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryItemSPBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryPrazoSPBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure btSalvarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure ToolButton15Click(Sender: TObject);
    procedure DBEdit5Exit(Sender: TObject);
    procedure qryItemSPCalcFields(DataSet: TDataSet);
    procedure qryPrazoSPAfterInsert(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure DBEdit3Exit(Sender: TObject);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit23Exit(Sender: TObject);
    procedure ToolButton5Click(Sender: TObject);
    procedure btImprimirSPClick(Sender: TObject);
    procedure btCancelarSPClick(Sender: TObject);
    procedure btLiberarSPClick(Sender: TObject);
    procedure qryItemSPnValPagtoChange(Sender: TField);
    procedure DBEdit17Exit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    bEdicao : boolean;
  end;

var
  frmSP: TfrmSP;

implementation

uses fMenu, fLookup_Padrao, rImpSP, fSP_ProvisaoAberta;

{$R *.dfm}

procedure TfrmSP.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'SP' ;
  nCdTabelaSistema  := 20 ;
  nCdConsultaPadrao := 34 ;
end;

procedure TfrmSP.FormShow(Sender: TObject);
begin
  inherited;
  qryUsuarioTipoSP.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryMaster.Parameters.ParamByName('nCdLoja').Value    := frmMenu.nCdLojaAtiva;
  qryMaster.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

end;

procedure TfrmSP.btIncluirClick(Sender: TObject);
begin
  inherited;
  DBGridEh1.ReadOnly := False ;
  DBGridEh2.ReadOnly := False ;

  DBEdit17.Enabled := True ;
  DBEdit5.Enabled  := True ;
  DBEdit23.Enabled := True ;

  qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;

  PosicionaQuery(qryEmpresa, IntToStr(frmMenu.nCdEmpresaAtiva)) ;

  btLiberarSP.Enabled := True ;

  if (Dbedit3.Enabled) then
  begin
      qryMasternCdLojaSP.Value := frmMenu.nCdLojaAtiva;
      PosicionaQuery(qryLoja, qryMasternCdLojaSP.AsString) ; 
      DBEdit3.SetFocus
  end
  else DbEdit17.SetFocus ;

end;

procedure TfrmSP.DBEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmSP.DBEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmSP.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMasternCdTerceiro.Value = 0) or (DBEdit6.Text = '') then
  begin
      MensagemAlerta('Informe o terceiro') ;
      DBEdit5.SetFocus ;
      Abort ;
  end ;

  If (qryTerceironCdStatus.Value <> 1) then
  begin
      MensagemAlerta('O Terceiro deve estar ativo para gerar uma SP') ;
      DBEdit5.SetFocus ;
      Abort ;
  end ;

  If (qryUsuarioTipoSPcFlgExigeNF.Value = 1) and (qryMastercNrTit.Value = '') then
  begin
      MensagemAlerta('Este tipo de SP exige a digita��o do n�mero da nota fiscal') ;
      DBEdit8.SetFocus;
      Abort ;
  end ;

  if (qryMasterdDtReceb.asString = '') then
  begin
      MensagemAlerta('Informe a data de recebimento do documento a pagar') ;
      DBEdit10.SetFocus;
      abort ;
  end ;

  if (qryMasterdDtEmissao.asString = '') then
  begin
      MensagemAlerta('Informe a data de emiss�o do documento a pagar.') ;
      DBEdit11.SetFocus;
      abort ;
  end ;

  if (qryMasterdDtReceb.Value < qryMasterdDtEmissao.Value) then
  begin
      MensagemAlerta('A data do recebimento n�o pode ser inferior a data de emiss�o da Nota Fiscal.') ;
      DBEdit10.SetFocus;
      Abort ;
  end ;

  if (qryMasterdDtEmissao.Value > Now()) then
  begin
      MensagemAlerta('A data de emiss�o do documento n�o pode ser maior que a data de hoje.') ;
      DBEdit11.SetFocus ;
      Abort ;
  end ;

  if (qryMasterdDtReceb.Value > Now()) then
  begin
      MensagemAlerta('A data de recebimento do documento n�o pode ser maior que a data de hoje.') ;
      DBEdit10.SetFocus ;
      Abort ;
  end ;

  if (qryMasternValSP.Value <= 0) then
  begin
      MensagemAlerta('Valor da SP n�o informado ou inv�lido') ;
      DBEdit12.SetFocus;
      Abort ;
  end ;

  if (DBEdit3.Enabled) And (DbEdit4.Text = '') then
  begin
      MensagemAlerta('Selecione a loja tomadora da despesa.') ;
      DbEdit3.SetFocus ;
      abort ;
  end ;

  If (qryMastercSerieTit.Value = '') then
      qryMastercSerieTit.Value := 'UN' ;

  if (qryMastercNrTit.Value <> '') and (qryMasterdDtCancel.asString = '') then
  begin
      qryValidaTitulo.Close ;
      qryValidaTitulo.Parameters.ParamByName('cNrTit').Value      := qryMastercNrTit.Value      ;
      qryValidaTitulo.Parameters.ParamByName('cSerieTit').Value   := qryMastercSerieTit.Value   ;
      qryValidaTitulo.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
      qryValidaTitulo.Parameters.ParamByName('nCdSP').Value       := qryMasternCdSP.Value       ;
      qryValidaTitulo.Open ;

      If not qryValidaTitulo.Eof then
      begin
          MensagemAlerta('J� existe uma SP emitido por este credor com este n�mero de nota fiscal.') ;
          DBEdit8.SetFocus ;
          abort ;
      end ;
  end ;

  if (qryMasternCdSP.Value = 0) then
  begin
      qryMasterdDtCad.Value := Now() ;
  end ;

  if (qryMasternCdUsuarioCad.Value = 0) then
      qryMasternCdUsuarioCad.Value := frmMenu.nCdUsuarioLogado ;

  inherited;

  If (qryMastercNrTit.Value = '') then
  begin
      qryMastercNrTit.Value := 'SP:' + qryMasternCdSP.AsString ;
  end ;

  if (qryMasternCdFormaPagtoSP.Value > 0)  And (DbEdit24.Text = '') then
  begin
      MensagemAlerta('Forma de pagto invalida.') ;
      DBEdit23.SetFocus ;
      Abort ;
  end ;

end;

procedure TfrmSP.DBEdit17KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(31);

            If (nPK > 0) then
            begin
                qryMasternCdTipoSP.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmSP.qryItemSPBeforePost(DataSet: TDataSet);
begin
  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  if (qryItemSPdDtRef.asString = '') then
  begin
      MensagemAlerta('Informe a data de refer�ncia') ;
      Abort ;
  end ;

  qryParametro.Close ;
  qryParametro.Parameters.ParamByName('cParametro').Value := 'DTCONTAB' ;
  qryParametro.Open ;

  if (qryItemSPdDtRef.Value <= StrToDateTime(qryParametrocValor.Value)) then
  begin
      MensagemAlerta('A data de refer�ncia da despesa n�o pode ser inferior a data da �ltima contabiliza��o financeira.') ;
      Abort ;
  end ;

  if (qryItemSPnValPagto.Value <= 0) then
  begin
      MensagemAlerta('Valor inv�lido ou n�o informado') ;
      Abort ;
  end ;

  if (qryItemSPnCdCategFinanc.Value = 0) or (qryItemSPcNmCategFinanc.Value = '') then
  begin
      MensagemAlerta('Informe a categoria financeira') ;
      Abort ;
  end ;

  if (qryItemSPnCdCC.Value = 0) or (qryItemSPcNmCC.Value = '') then
  begin
      MensagemAlerta('Informe o centro de custo') ;
      Abort ;
  end ;

  if (qryItemSPnCdUnidadeNegocio.Value = 0) or (qryItemSPcNmUnidadeNegocio.Value = '') then
  begin
      MensagemAlerta('Informe a unidade de neg�cio') ;
      Abort ;
  end ;

  if (qryItemSPcNmProjeto.Value <> '') then
  begin

      if (qryProjetonCdStatus.Value > 1) then
      begin
          MensagemAlerta('Projeto inativo n�o pode ser movimentado.');
          abort;
      end ;

      qryProjetoCategFinanc.Close;
      qryProjetoCategFinanc.Parameters.ParamByName('nCdProjeto').Value     := qryItemSPnCdProjeto.Value ;
      qryProjetoCategFinanc.Parameters.ParamByName('nCdCategFinanc').Value := qryItemSPnCdCategFinanc.Value ;
      qryProjetoCategFinanc.Open;

      if qryProjetoCategFinanc.IsEmpty then
      begin
          MensagemAlerta('Este projeto n�o tem v�nculo com essa categoria financeira.') ;
          abort ;
      end ;

  end ;

  if (Trim(qryItemSPcOBS.Value) = '') then
  begin
      MensagemAlerta('Informe o hist�rico da despesa.') ;
      Abort ;
  end ;

  inherited;

  qryItemSPcOBS.Value := Uppercase( qryItemSPcOBS.Value ) ;
  
  if (qryItemSP.State = dsInsert) then
      qryItemSPnCdItemSP.Value := frmMenu.fnProximoCodigo('ITEMSP') ;

  qryItemSPnCdSP.Value := qryMasternCdSP.Value ;

end;

procedure TfrmSP.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) and (qryMaster.State <> dsInsert) then
  begin
      MensagemAlerta('Esta SP n�o pertence a esta empresa.') ;
      btCancelar.Click;
  end ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.AsString) ;
  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.AsString) ;
  PosicionaQuery(qryItemSP, qryMasternCdSP.AsString) ;
  PosicionaQuery(qryPrazoSP, qryMasternCdSP.asString) ;
  PosicionaQuery(qryFormaPagto, qryMasternCdFormaPagtoSP.asString) ;

  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, qryMasternCdLojaSP.asString) ;

  qryUsuario.Close ;
  qryUsuario.Parameters.ParamByName('nCdUsuario').Value := qryMasternCdUsuarioCad.Value ;
  qryUsuario.Open ;

  qryUsuarioAut.Close ;
  qryUsuarioAut.Parameters.ParamByName('nCdUsuario').Value := qryMasternCdUsuarioAutor.Value ;
  qryUsuarioAut.Open ;

  qryUsuarioTipoSP.Close;
  PosicionaQuery( qryUsuarioTipoSP , qryMasternCdTipoSP.AsString ) ;

  btLiberarSP.Enabled := True ;

  DBGridEh1.ReadOnly := False ;
  DBGridEh2.ReadOnly := False ;

  if (qryMasterdDtLiber.asString <> '') or (qryMasterdDtCancel.asString <> '') then
  begin

      if (not bEdicao) then
      begin

          DBGridEh1.ReadOnly := True ;
          DBGridEh2.ReadOnly := True ;

          If (qryMasterdDtCancel.asString <> '') then
              MensagemAlerta('A SP est� cancelada, somente a visualiza��o � permitida.') ;

          btSalvar.Enabled    := False ;
          btIncluir.Enabled   := False ;
          btLiberarSP.Enabled := False ;
          DBEdit23.Enabled    := False ;

      end ;

  end ;

  //Permitir alterar se n�o tiver autorizada - RENSZ chamado 581
  //DBEdit17.Enabled := False ;
  //DBEdit5.Enabled  := False ;

end;

procedure TfrmSP.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryTerceiro.Close ;
  qryItemSP.Close ;
  qryUsuario.Close ;
  qryPrazoSP.Close ;
  qryEmpresa.Close ;
  qryLoja.Close ;
  qryUsuarioAut.CLose ;
  qryFormaPagto.Close ;
  qryUsuarioTipoSP.Close;

end;

procedure TfrmSP.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if (qryMaster.Active) then
  begin

      if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
          qryMaster.Post ;

      PosicionaQuery(qryItemSP, qryMasternCdSP.AsString) ;
      PosicionaQuery(qryPrazoSP, qryMasternCdSP.asString) ;

  end ;

end;

procedure TfrmSP.DBGridEh1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        // categoria financeira
        if (DBGridEh1.Col = 4) then
        begin

            if (qryItemSP.State = dsBrowse) then
                qryItemSP.Edit ;


            if (qryItemSP.State = dsInsert) or (qryItemSP.State = dsEdit) then
            begin

                {-- verifica no parametro se deve ou n�o filtrar as categorias financeiras por usu�rio --}
                if (frmMenu.LeParametro('CATEGFINUSUSP') = 'N') then
                    nPK := frmLookup_Padrao.ExecutaConsulta(32)
                else nPK := frmLookup_Padrao.ExecutaConsulta2(32,'EXISTS (SELECT 1 FROM UsuarioCategFinanc UCF WHERE UCF.nCdCategFinanc = CategFinanc.nCdCategFinanc AND UCF.nCdUsuario = ' + intToStr(frmMenu.nCdUsuarioLogado) + ')') ;

                If (nPK > 0) then
                begin
                    qryItemSPnCdCategFinanc.Value := nPK ;
                end ;

            end ;

        end ;

        if (DBGridEh1.Col = 6) then
        begin

            if (qryItemSP.State = dsBrowse) then
                qryItemSP.Edit ;


            if (qryItemSP.State = dsInsert) or (qryItemSP.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(33,'EXISTS(SELECT 1 FROM CentroCustoCategFinanc CCCF WHERE CCCF.nCdCC = CentroCusto.nCdCC AND CCCF.nCdCategFinanc = ' + qryItemSPnCdCategFinanc.AsString + ')');

                If (nPK > 0) then
                begin
                    qryItemSPnCdCC.Value := nPK ;
                end ;

            end ;

        end ;

        if (DBGridEh1.Col = 8) then
        begin

            if (qryItemSP.State = dsBrowse) then
                qryItemSP.Edit ;


            if (qryItemSP.State = dsInsert) or (qryItemSP.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(35,'EXISTS(SELECT 1 FROM EmpresaUnidadeNegocio EUN WHERE EUN.nCdUnidadeNegocio = UnidadeNegocio.nCdUnidadeNegocio AND EUN.nCdEmpresa = @@Empresa)');

                If (nPK > 0) then
                begin
                    qryItemSPnCdUnidadeNegocio.Value := nPK ;
                end ;

            end ;

        end ;

        if (DBGridEh1.Col = 10) then
        begin

            if (qryItemSP.State = dsBrowse) then
                qryItemSP.Edit ;


            if (qryItemSP.State = dsInsert) or (qryItemSP.State = dsEdit) then
            begin

                nPK := frmLookup_Padrao.ExecutaConsulta2(227,'nCdStatus = 1') ;

                If (nPK > 0) then
                begin
                    qryItemSPnCdProjeto.Value := nPK ;
                end ;

            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmSP.qryPrazoSPBeforePost(DataSet: TDataSet);
begin

  if (qryMaster.FieldList[0].Value = 0) or (qryMaster.FieldList[0].Value = null) then
      qryMaster.Post ;

  qryPrazoSPnCdSP.Value := qryMasternCdSP.Value ;

  qryParametro.Close ;
  qryParametro.Parameters.ParamByName('cParametro').Value := 'DTCONTAB' ;
  qryParametro.Open ;

  if (qryPrazoSPdDtVenc.Value <= StrToDateTime(qryParametrocValor.Value)) then
  begin
      MensagemAlerta('A data de vencimento da parcela n�o pode ser inferior a data da contabiliza��o financeira.') ;
      Abort ;
  end ;

  if (qryPrazoSPcNrTit.Value = '') and (qryPrazoSPcFlgDocCobranca.Value = 1) then
  begin
      MensagemAlerta('Informe o n�mero do t�tulo da parcela.') ;
      abort ;
  end ;

  if (qryPrazoSP.State = dsInsert) then
      qryPrazoSPnCdPrazoSP.Value := frmMenu.fnProximoCodigo('PRAZOSP') ;

  inherited;

end;

procedure TfrmSP.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  btLiberarSP.Enabled := True ;

end;

procedure TfrmSP.btSalvarClick(Sender: TObject);
begin
//  if (qryMasterdDtLiber.asString <> '') then
//  begin
//      MensagemAlerta('N�o � permitido alterar esta SP.') ;
//      btCancelar.Click;
//      abort ;
//  end ;

  if (qryMasterdDtAutor.asString <> '') then
  begin
      MensagemAlerta('N�o � permitido alterar esta SP, pois a mesma j� est� Autorizada.') ;
      btCancelar.Click;
      abort ;
  end ;

  if (qryMasterdDtCancel.asString <> '') then
  begin
      MensagemAlerta('N�o � permitido alterar esta SP.') ;
      btCancelar.Click;
      abort ;
  end ;

  inherited;

end;

procedure TfrmSP.btCancelarClick(Sender: TObject);
begin
  inherited;
  DBEdit17.Enabled := True ;
  DBEdit5.Enabled  := True ;

  DBGridEh1.ReadOnly := False ;
  DBGridEh2.ReadOnly := False ;

end;

procedure TfrmSP.ToolButton15Click(Sender: TObject);
begin
  inherited;
  if not (qryMaster.Active) then
      exit ;

  if (qryMasternCdSP.Value = 0) then
      exit ;

  if (qryMasterdDtLiber.AsString = '') then
  begin
      MensagemAlerta('A SP precisa ser liberada antes de autorizada.') ;
      exit ;
  end ;

  if (qryMasterdDtAutor.AsString <> '') then
  begin
      MensagemAlerta('SP j� foi autorizada.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a Autoriza��o desta SP ?',mtConfirmation,mbYesNoCancel,0) of
      mrNo: Abort ;
      mrCancel:Abort;
  end;

  frmMenu.Connection.BeginTrans ;

  try
      usp_AutorizaSP.Close ;
      usp_AutorizaSP.Parameters.ParamByName('@nCdSP').Value := qryMasternCdSP.Value ;
      usp_AutorizaSP.Parameters.ParamByName('@nCdUsuarioAutor').Value := frmMenu.nCdUsuarioLogado ;
      usp_AutorizaSP.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro na autoriza��o da SP.') ;
      Raise ;
      exit ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('SP autorizada com sucesso.') ;

  btImprimirSP.Click;

end;

procedure TfrmSP.DBEdit5Exit(Sender: TObject);
var
   objForm : TfrmSP_ProvisaoAberta ;
begin
  inherited;

  qryTerceiro.Close;
  PosicionaQuery(qryTerceiro, dbEdit5.Text) ;

  if (qryMaster.State = dsInsert) then
  begin

      if not qryTerceiro.eof then
      begin

          if (qryTerceironCdFormaPagto.Value > 0) then
          begin

              qryMasternCdFormaPagtoSP.Value := qryTerceironCdFormaPagto.Value;
              PosicionaQuery(qryFormaPagto, qryMasternCdFormaPagtoSP.asString);

          end ;

          objForm := TfrmSP_ProvisaoAberta.Create( Self ) ;
          objForm.qryProvisoes.Close;
          objForm.qryProvisoes.Parameters.ParamByName('nCdEmpresa').Value  := qryMasternCdEmpresa.Value ;
          objForm.qryProvisoes.Parameters.ParamByName('nCdLojaTit').Value  := qryMasternCdLojaSp.Value ;
          objForm.qryProvisoes.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value ;
          objForm.qryProvisoes.Open ;

          if not objForm.qryProvisoes.IsEmpty then
          begin

              try

                  case MessageDlg('Existem Provis�es em aberto para este credor. Deseja visualizar ?',mtConfirmation,mbYesNoCancel,0) of
                      mrYes: objForm.ShowModal;
                  end;

              finally

                  freeAndNil( objForm ) ;

              end ;

          end ;

      end ;

  end;

end;

procedure TfrmSP.qryItemSPCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryItemSPnCdCategFinanc.Value > 0) then
  begin
      PosicionaQuery(qryCategFinanc, qryItemSPnCdCategFinanc.AsString) ;

      if not qryCategFinanc.eof then
          qryItemSPcNmCategFinanc.Value := qryCategFinanccNmCategFinanc.Value ;
  end ;

  if (qryItemSPnCdCC.Value > 0) then
  begin
      qryCC.Parameters.ParamByName('nCdCategFinanc').Value := qryItemSPnCdCategFinanc.Value ;

      PosicionaQuery(qryCC, qryItemSPnCdCC.AsString) ;

      if not qryCC.Eof then
          qryItemSPcNmCC.Value := qryCCcNmCC.Value ;

  end ;

  if (qryItemSPnCdUnidadeNegocio.Value > 0) then
  begin
      qryUnidadeNegocio.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
      PosicionaQuery(qryUnidadeNegocio, qryItemSPnCdUnidadeNegocio.AsString) ;

      if not qryUnidadeNegocio.Eof then
          qryItemSPcNmUnidadeNegocio.Value := qryUnidadeNegociocNmUnidadeNegocio.Value ;

  end ;

  if (qryItemSPnCdProjeto.Value > 0) then
  begin
  
      qryProjeto.Close;
      PosicionaQuery( qryProjeto , qryItemSPnCdProjeto.AsString ) ;

      if not qryProjeto.eof then
          qryItemSPcNmProjeto.Value := qryProjetocNmProjeto.Value ;

  end ;

end;

procedure TfrmSP.qryPrazoSPAfterInsert(DataSet: TDataSet);
begin
  inherited;

  qryPrazoSPcFlgDocCobranca.Value := 1 ;
  qryPrazoSPcNrTit.Value          := qryMastercNrTit.Value ;
  
end;

procedure TfrmSP.FormActivate(Sender: TObject);
begin
  inherited;

  if (frmMenu.LeParametro('VAREJO') = 'N') then
      DBEdit3.Enabled := False ;

end;

procedure TfrmSP.DBEdit3Exit(Sender: TObject);
begin
  inherited;

  qryLoja.Close ;
  PosicionaQuery(qryLoja, DBEdit3.Text) ;
  
end;

procedure TfrmSP.DBEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLojaSP.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmSP.DBEdit23KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(15,'cFlgPermFinanceiro=1');

            If (nPK > 0) then
            begin
                qryMasternCdFormaPagtoSP.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmSP.DBEdit23Exit(Sender: TObject);
begin
  inherited;

  qryFormaPagto.Close ;
  PosicionaQuery(qryFormaPagto, DBEdit23.Text) ;

end;

procedure TfrmSP.ToolButton5Click(Sender: TObject);
begin
  if not qryMaster.Eof then
  begin
     if ((qryMastercFlgAutomatica.Value = 1) and (qryMasterdDtLiber.AsString = '')) then
     begin
        case MessageDlg('Confirma Fechamento sem Libera��o ?',mtConfirmation,mbYesNoCancel,0) of
           mrNo: Abort ;
           mrCancel:Abort;
        end;
     end;
  end;

  inherited;

end;

procedure TfrmSP.btImprimirSPClick(Sender: TObject);
var
  objRel : TrptImpSP ;
begin
  if not (qryMaster.Active) then
      exit ;

  if (qryMasternCdSP.Value = 0) then
      exit ;

  if (qryMasterdDtLiber.AsString = '') then
  begin
      MensagemAlerta('� necess�rio liberar a SP antes de imprimir.') ;
      exit ;
  end ;

  if (qryMasterdDtCancel.AsString <> '') then
  begin
      MensagemAlerta('N�o � poss�vel imprimir uma SP cancelada.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a impress�o desta SP ?',mtConfirmation,mbYesNoCancel,0) of
      mrNo: Abort ;
      mrCancel:Abort;
  end;

  objRel := TrptImpSP.Create(Self) ;

  objRel.qryImpSP.Close ;
  objRel.qryImpSP.Parameters.ParamByName('nCdSP').Value := qryMasternCdSP.Value ;
  objRel.qryImpSP.Open ;

  objRel.qryPrazo.Close ;
  objRel.qryPrazo.Parameters.ParamByName('nCdSP').Value := qryMasternCdSP.Value ;
  objRel.qryPrazo.Open ;

  objRel.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva;

  try
      try
          {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;
  finally
      FreeAndNil(objRel);
  end;

end;

procedure TfrmSP.btCancelarSPClick(Sender: TObject);
begin
  if not (qryMaster.Active) then
      exit ;

  if (qryMasternCdSP.Value = 0) then
      exit ;

  if (qryMasterdDtCancel.AsString <> '') then
      exit ;

  if (qryMasterdDtAutor.AsString <> '') then
  begin
      MensagemAlerta('SP j� autorizada, imposs�vel cancelar.') ;
      exit ;
  end ;

  {if (qryMastercFlgAutomatica.Value = 1) then
  begin
      MensagemAlerta('SP gerada automaticamente n�o pode ser cancelada.') ;
      exit ;
  end ;}

  case MessageDlg('Confirma o cancelamento desta SP ?',mtConfirmation,mbYesNoCancel,0) of
      mrNo: Abort ;
      mrCancel:Abort;
  end;

  frmMenu.Connection.BeginTrans ;

  try
      qryMaster.Edit ;
      qryMasterdDtCancel.Value := Now() ;
      qryMaster.Post ;

      frmMenu.LogAuditoria(20,4,qryMasternCdSP.Value,'SP Cancelada') ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processo de cancelamento da SP.') ;
      raise ;
      exit ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('SP Cancelada com sucesso!') ;

  btCancelar.Click;

end;

procedure TfrmSP.btLiberarSPClick(Sender: TObject);
var
    qryAux : TADOQuery ;
begin

  if not (qryMaster.Active) then
      exit ;

  if (qryMasternCdSP.Value = 0) then
      exit ;

  if (qryMasterdDtLiber.AsString <> '') then
  begin
      MensagemAlerta('SP j� foi liberada.') ;
      exit ;
  end ;

  case MessageDlg('Confirma a libera��o desta SP ?',mtConfirmation,mbYesNoCancel,0) of
      mrNo: Abort ;
      mrCancel:Abort;
  end;

  if (qryMastercNrTit.Value <> '') then
  begin
      qryValidaTitulo.Close ;
      qryValidaTitulo.Parameters.ParamByName('cNrTit').Value      := qryMastercNrTit.Value      ;
      qryValidaTitulo.Parameters.ParamByName('cSerieTit').Value   := qryMastercSerieTit.Value   ;
      qryValidaTitulo.Parameters.ParamByName('nCdTerceiro').Value := qryMasternCdTerceiro.Value ;
      qryValidaTitulo.Parameters.ParamByName('nCdSP').Value       := qryMasternCdSP.Value       ;
      qryValidaTitulo.Open ;

      If not qryValidaTitulo.Eof then
      begin
          MensagemAlerta('J� existe uma SP emitido por este credor com este n�mero de nota fiscal.') ;
          DBEdit8.SetFocus ;
          abort ;
      end ;
  end ;


  if (qryMasternCdUsuarioCad.Value <> frmMenu.nCdUsuarioLogado) then
  begin
      MensagemAlerta('Somente o usu�rio que digitou a SP pode liberar.') ;
      exit ;
  end ;

  if (qryMaster.State = dsEdit) then
      qryMaster.Post ;

  qryAux := TADOQuery.Create(Self) ;

  qryAux.Connection := frmMenu.Connection ;

  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT Sum(nValPagto) FROM ItemSP WHERE nCdSP = ' + qryMasternCdSP.asString) ;
  qryAux.Open ;

  If qryAux.FieldList[0].Value <> qryMasternValSP.Value then
  begin
      qryAux.Close ;
      qryAux := nil ;
      MensagemAlerta('O total dos itens da SP n�o confere com o valor total da SP.') ;
      exit ;
  end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT Sum(nValPagto) FROM PrazoSP WHERE nCdSP = ' + qryMasternCdSP.asString) ;
  qryAux.Open ;

  If qryAux.FieldList[0].Value <> qryMasternValSP.Value then
  begin
      qryAux.Close ;
      qryAux := nil ;
      MensagemAlerta('O total das parcelas da SP n�o confere com o valor total da SP.') ;
      exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Edit ;
      qryMasterdDtLiber.Value := Now() ;
      qryMaster.Post ;

      SP_GERA_ALERTA.Close ;
      SP_GERA_ALERTA.Parameters.ParamByName('@nCdTipoAlerta').Value := 6 ;
      SP_GERA_ALERTA.Parameters.ParamByName('@cAssunto').Value      := 'Autorizar SP' ;
      SP_GERA_ALERTA.Parameters.ParamByName('@cMensagem').Value     := ('A solicita��o de pagamento No ' + qryMasternCdSP.AsString + ' (' + Trim(dbEdit6.Text) + ') aguarda ser autorizada.') ;
      SP_GERA_ALERTA.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('SP liberada com sucesso.') ;

  if qryMastercFlgAutomatica.Value = 1 then
  begin
     btCancelar.Click;
     ToolButton5.Click;
  end;

  btImprimirSP.Click;
  btCancelar.Click;

end;

procedure TfrmSP.qryItemSPnValPagtoChange(Sender: TField);
begin
  inherited;

  if (qryItemSP.State = dsInsert) then
      qryItemSPcOBS.Value := qryUsuarioTipoSPcHistoricoPadrao.Value;
      
end;

procedure TfrmSP.DBEdit17Exit(Sender: TObject);
begin
  inherited;

  qryUsuarioTipoSP.Close;
  PosicionaQuery( qryUsuarioTipoSP , DBEdit17.Text ) ;

end;

initialization
    RegisterClass(tFrmSP) ;

end.
