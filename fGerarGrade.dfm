inherited frmGerarGrade: TfrmGerarGrade
  Left = 284
  Top = 207
  Width = 834
  Height = 448
  BorderIcons = [biMinimize, biMaximize]
  Caption = 'Gerar Grade'
  OldCreateOrder = True
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 137
    Width = 818
    Height = 273
  end
  inherited ToolBar1: TToolBar
    Width = 818
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 137
    Width = 818
    Height = 273
    Align = alClient
    DataGrouping.GroupLevels = <>
    DataSource = dsTemp
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    IndicatorOptions = [gioShowRowIndicatorEh]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdCor'
        Footers = <>
        Visible = False
      end
      item
        EditButtons = <>
        FieldName = 'cMaterial'
        Footers = <>
        ReadOnly = True
        Width = 290
      end
      item
        EditButtons = <>
        FieldName = 'cCor'
        Footers = <>
        ReadOnly = True
        Width = 290
      end
      item
        EditButtons = <>
        FieldName = 'nValCusto'
        Footers = <>
        ReadOnly = True
        Width = 100
      end
      item
        EditButtons = <>
        FieldName = 'nValVenda'
        Footers = <>
        ReadOnly = True
        Width = 100
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object GroupBox1: TGroupBox [3]
    Left = 0
    Top = 29
    Width = 818
    Height = 108
    Align = alTop
    Caption = ' Gera'#231#227'o de Grade '
    TabOrder = 2
    object Label3: TLabel
      Tag = 1
      Left = 11
      Top = 80
      Width = 55
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Custo'
    end
    object Label4: TLabel
      Tag = 1
      Left = 144
      Top = 80
      Width = 57
      Height = 13
      Alignment = taRightJustify
      Caption = 'Valor Venda'
    end
    object Label5: TLabel
      Tag = 1
      Left = 49
      Top = 56
      Width = 17
      Height = 13
      Alignment = taRightJustify
      Caption = 'Cor'
    end
    object Label7: TLabel
      Tag = 1
      Left = 28
      Top = 32
      Width = 38
      Height = 13
      Alignment = taRightJustify
      Caption = 'Material'
    end
    object Edit1: TEdit
      Left = 72
      Top = 48
      Width = 201
      Height = 21
      Hint = 
        'Digite o nome da cor e pressione ENTER. Ou, deixa a cor em branc' +
        'o e pressiona ENTER para prosseguir'
      CharCase = ecUpperCase
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnExit = Edit1Exit
      OnKeyDown = Edit1KeyDown
    end
    object Edit2: TEdit
      Tag = 1
      Left = 275
      Top = 48
      Width = 377
      Height = 21
      CharCase = ecUpperCase
      Enabled = False
      TabOrder = 3
    end
    object Edit3: TEdit
      Left = 72
      Top = 24
      Width = 201
      Height = 21
      Hint = 
        'Digite o nome do material e pressione ENTER. Ou, deixa o materia' +
        'l em branco e pressiona ENTER para prosseguir'
      CharCase = ecUpperCase
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnExit = Edit3Exit
      OnKeyDown = Edit3KeyDown
    end
    object Edit4: TEdit
      Tag = 1
      Left = 275
      Top = 24
      Width = 377
      Height = 21
      CharCase = ecUpperCase
      Enabled = False
      TabOrder = 4
    end
    object btAdd: TcxButton
      Left = 274
      Top = 71
      Width = 129
      Height = 25
      Caption = 'Adicionar na Lista'
      TabOrder = 2
      OnClick = btAddClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000C40E0000C40E00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5F0E7A0C8A6569C5E3F
        8F493C8D454C955398C19BE1EDE3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFC4DEC957A0644199507DC28F96D0A696CFA678BE89368D42418D48B9D5
        BCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC6E0CC55A06464B478A8DBB587CC9866
        BC7D64BA7C86CB98A5D9B458AA6B35863DB9D5BCFFFFFFFFFFFFFFFFFFE9F3EB
        67AC766AB97DA8DBB260BC775CBA7359B87059B56F58B56F5BB774A5D9B35AAA
        6C428F49E2EEE3FFFFFFFFFFFFAFD5B853AB68AADDB464C1795FBE7160BC77FD
        FDFDFDFDFD59B87058B56E5CB774A6DAB4388F4397C19BFFFFFFFFFFFF77B888
        8ACC9889D3966BC67A63C17055AB65FDFDFDFDFDFD59B87059B8705BB97285CC
        977BBE8D4D9655FFFFFFFFFFFF6AB27FA9DDB37DCF8A75CC81FDFDFDFDFDFDFD
        FDFDFDFDFDFDFDFDFDFDFD59B87067BE7D9CD4AB3B8C44FFFFFFFFFFFF6EB583
        B6E2BE8BD5977AC986FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD59B87069C1
        7E9DD4AA3F8F49FFFFFFFFFFFF82BF95ACDDB6A6DFAF81CB8C7CC9866EBD79FD
        FDFDFDFDFD5BAC6A60BC775CBA738BD19980C592589E61FFFFFFFFFFFFB8DCC4
        85C797D2EED795D9A08AD3947FC889FDFDFDFDFDFD79CD856BC37C6FC77EACDF
        B5459E57A1C9A7FFFFFFFFFFFFECF6EF7FBF93AADAB7D8F1DC92D89D88CD9384
        CC8E8BD4968AD49583D28EAFE0B76BB97D5BA367E6F1E8FFFFFFFFFFFFFFFFFF
        D1E9D976BB8CAFDCBBDCF2E0B6E4BD9BDBA596D9A0A5DFAFC0E8C579C28A58A2
        66C5DECAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD1E9D97FBF9394CEA4C3E6CBCF
        EBD4C9E9CEAFDDB86DB97F68AE78C7E0CDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFECF6EFB9DDC582C09571B7866EB58279B98AB1D6BAE9F3EBFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      LookAndFeel.Kind = lfOffice11
    end
    object Edit5: TEdit
      Left = 432
      Top = 72
      Width = 89
      Height = 21
      TabOrder = 5
      Text = 'Edit5'
      Visible = False
    end
    object btLimpaCor: TcxButton
      Left = 680
      Top = 47
      Width = 121
      Height = 25
      Caption = 'Limpar Cor'
      TabOrder = 6
      OnClick = btLimpaCorClick
      Glyph.Data = {
        DE030000424DDE03000000000000360000002800000011000000120000000100
        180000000000A803000000000000000000000000000000000000EEEEEEEEEEEE
        EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EEEEEEEEEEEEEEEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        DED8F7D4CCF5EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EE00EEEEEEEEEEEEEEEEEEEEEEEEEEEEEECEC5F38770E28770E2BCAFEFEEEEEE
        EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEEEE
        EEEECEC5F38770E28770E2A898EAEBE8FAEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EEEEEEEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEEDFD9F79784E69784E6A898EAEE
        EEEEDCD5F68770E2C2B7F0EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE00EEEE
        EEEEEEEEEEEEEECEC5F39784E6A898EAEEEEEEDDD6F78770E28770E28770E2BE
        B2EFEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEEEEEEEEB9AD
        EEEAE6FADED8F79987E79784E68770E28770E28770E2BFB4F0EEEEEEEEEEEEEE
        EEEEEEEEEE00EEEEEEEBEBEBD7D7D7EEEEEEEEEEEEEEEEEE9B88E79784E69784
        E69784E68770E28770E28770E2BFB4F0EEEEEEEEEEEEEEEEEE00EEEEEEE1E1E1
        000000CDCDCDEEEEEEEEEEEEBAAEEF9784E69784E69784E69784E68770E28770
        E28770E2BFB4F0EEEEEEEEEEEE00EEEEEEEEEEEE000000919191D7D7D7DBDBDB
        EEEEEEB9ADEE9784E69784E69784E69784E68770E28770E28770E2CEC5F3EEEE
        EE00EEEEEEEEEEEEB9B9B9000000000000000000000000EEEEEEB9ADEE9784E6
        9784E69784E69784E68770E28770E2C5BBF1EEEEEE00EEEEEEEEEEEEEBEBEB00
        0000CDCDCDEEEEEEC3C3C3000000EEEEEEB9ADEE9784E69784E69784E69784E6
        B3A5EDEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEE0000009B9B9BEEEEEE91919100
        0000EEEEEEEEEEEEB9ADEE9784E69784E6B1A2ECEEEEEEEEEEEEEEEEEE00EEEE
        EEEEEEEEEEEEEEC3C3C3000000EBEBEB000000C3C3C3EEEEEEEEEEEEEEEEEECC
        C2F3C3B8F1EEEEEEEEEEEEEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEEEEEEEE0000
        00878787000000EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EEEEEEEEEE00EEEEEEEEEEEEEEEEEEEEEEEEA5A5A50000009B9B9BEEEEEEEEEE
        EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE00EEEEEEEEEEEE
        EEEEEEEEEEEEEEEEEED7D7D7EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EEEEEEEEEEEEEEEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EE00}
      LookAndFeel.Kind = lfOffice11
      Margin = 10
    end
    object btLimpaMat: TcxButton
      Left = 680
      Top = 22
      Width = 121
      Height = 25
      Caption = 'Limpar Material'
      TabOrder = 7
      OnClick = btLimpaMatClick
      Glyph.Data = {
        DE030000424DDE03000000000000360000002800000011000000120000000100
        180000000000A803000000000000000000000000000000000000EEEEEEEEEEEE
        EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EEEEEEEEEEEEEEEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        DED8F7D4CCF5EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EE00EEEEEEEEEEEEEEEEEEEEEEEEEEEEEECEC5F38770E28770E2BCAFEFEEEEEE
        EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEEEE
        EEEECEC5F38770E28770E2A898EAEBE8FAEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EEEEEEEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEEDFD9F79784E69784E6A898EAEE
        EEEEDCD5F68770E2C2B7F0EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE00EEEE
        EEEEEEEEEEEEEECEC5F39784E6A898EAEEEEEEDDD6F78770E28770E28770E2BE
        B2EFEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEEEEEEEEB9AD
        EEEAE6FADED8F79987E79784E68770E28770E28770E2BFB4F0EEEEEEEEEEEEEE
        EEEEEEEEEE00EEEEEEEBEBEBD7D7D7EEEEEEEEEEEEEEEEEE9B88E79784E69784
        E69784E68770E28770E28770E2BFB4F0EEEEEEEEEEEEEEEEEE00EEEEEEE1E1E1
        000000CDCDCDEEEEEEEEEEEEBAAEEF9784E69784E69784E69784E68770E28770
        E28770E2BFB4F0EEEEEEEEEEEE00EEEEEEEEEEEE000000919191D7D7D7DBDBDB
        EEEEEEB9ADEE9784E69784E69784E69784E68770E28770E28770E2CEC5F3EEEE
        EE00EEEEEEEEEEEEB9B9B9000000000000000000000000EEEEEEB9ADEE9784E6
        9784E69784E69784E68770E28770E2C5BBF1EEEEEE00EEEEEEEEEEEEEBEBEB00
        0000CDCDCDEEEEEEC3C3C3000000EEEEEEB9ADEE9784E69784E69784E69784E6
        B3A5EDEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEE0000009B9B9BEEEEEE91919100
        0000EEEEEEEEEEEEB9ADEE9784E69784E6B1A2ECEEEEEEEEEEEEEEEEEE00EEEE
        EEEEEEEEEEEEEEC3C3C3000000EBEBEB000000C3C3C3EEEEEEEEEEEEEEEEEECC
        C2F3C3B8F1EEEEEEEEEEEEEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEEEEEEEE0000
        00878787000000EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EEEEEEEEEE00EEEEEEEEEEEEEEEEEEEEEEEEA5A5A50000009B9B9BEEEEEEEEEE
        EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE00EEEEEEEEEEEE
        EEEEEEEEEEEEEEEEEED7D7D7EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EEEEEEEEEEEEEEEEEEEEEEEEEE00EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
        EE00}
      LookAndFeel.Kind = lfOffice11
      Margin = 10
    end
    object Edit6: TEdit
      Left = 654
      Top = 24
      Width = 25
      Height = 21
      Enabled = False
      TabOrder = 8
      Text = '0'
    end
    object Edit7: TEdit
      Left = 654
      Top = 48
      Width = 25
      Height = 21
      Enabled = False
      TabOrder = 9
      Text = '0'
    end
    object dxCurValCusto: TdxCurrencyEdit
      Left = 72
      Top = 72
      Width = 65
      Style.BorderStyle = xbsFlat
      Style.HotTrack = False
      Style.Shadow = False
      TabOrder = 10
      DisplayFormat = '##,##0.00;-0.00'
      StoredValues = 0
    end
    object dxCurValVenda: TdxCurrencyEdit
      Left = 208
      Top = 72
      Width = 65
      Style.BorderStyle = xbsFlat
      TabOrder = 11
      DisplayFormat = '0.00;-0.00'
      StoredValues = 0
    end
  end
  inherited ImageList1: TImageList
    Left = 384
    Top = 248
  end
  object qryCor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Precision = 50
        Size = 50
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM COR'
      'WHERE cNmCor = :nPK')
    Left = 288
    Top = 216
    object qryCornCdCor: TAutoIncField
      FieldName = 'nCdCor'
      ReadOnly = True
    end
    object qryCorcNmCor: TStringField
      FieldName = 'cNmCor'
      Size = 50
    end
    object qryCorcNmCorPredom: TStringField
      FieldName = 'cNmCorPredom'
      Size = 50
    end
  end
  object dsCor: TDataSource
    DataSet = qryCor
    Left = 288
    Top = 248
  end
  object qryMaterial: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Precision = 50
        Size = 50
        Value = Null
      end
      item
        Name = 'nCdMarca'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM MATERIAL'
      'WHERE cNmMaterial  = :nPK'
      'AND nCdMarca = :nCdMarca')
    Left = 320
    Top = 216
    object qryMaterialnCdMaterial: TAutoIncField
      FieldName = 'nCdMaterial'
      ReadOnly = True
    end
    object qryMaterialnCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMaterialcNmMaterial: TStringField
      FieldName = 'cNmMaterial'
      Size = 50
    end
    object qryMaterialnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
  end
  object dsMaterial: TDataSource
    DataSet = qryMaterial
    Left = 320
    Top = 248
  end
  object usp_Gera_Grade: TADOStoredProc
    Connection = frmMenu.Connection
    ProcedureName = 'SP_GERA_GRADE;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdProduto'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdUsuario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end>
    Left = 384
    Top = 216
  end
  object cmdCriarTemp: TADOCommand
    CommandText = 
      'IF (OBJECT_ID('#39'tempdb..#Temp_Grade'#39') IS NOT NULL) '#13#10'  DROP TABLE' +
      ' #Temp_Grade'#13#10#13#10'CREATE TABLE #Temp_Grade (nCdCor    int'#13#10'       ' +
      '                  ,cCor      VARCHAR(1000)'#13#10'                    ' +
      '     ,cMaterial VARCHAR(1000)'#13#10'                         ,nValCus' +
      'to DECIMAL(12,2) default 0 not null'#13#10'                         ,n' +
      'ValVenda DECIMAL(12,2) default 0 not null)'
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 416
    Top = 248
  end
  object dsTemp: TDataSource
    DataSet = qryTemp
    Left = 352
    Top = 248
  end
  object qryTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#Temp_Grade'#39') IS NULL) '
      'CREATE TABLE #Temp_Grade (nCdCor    int'
      '                         ,cCor      VARCHAR(1000)'
      '                         ,cMaterial VARCHAR(1000)'
      
        '                         ,nValCusto DECIMAL(12,2) default 0 not ' +
        'null'
      
        '                         ,nValVenda DECIMAL(12,2) default 0 not ' +
        'null)'
      ''
      'SELECT *'
      'FROM #Temp_Grade')
    Left = 352
    Top = 216
    object qryTempnCdCor: TIntegerField
      DisplayLabel = 'Lista de Grades|'
      FieldName = 'nCdCor'
    end
    object qryTempcCor: TStringField
      DisplayLabel = 'Lista de Grades|Nome Cor'
      FieldName = 'cCor'
      Size = 1000
    end
    object qryTempcMaterial: TStringField
      DisplayLabel = 'Lista de Grades|Nome Material'
      FieldName = 'cMaterial'
      Size = 1000
    end
    object qryTempnValCusto: TBCDField
      DisplayLabel = 'Lista de Grades|Valor Custo'
      FieldName = 'nValCusto'
      Precision = 12
      Size = 2
    end
    object qryTempnValVenda: TBCDField
      DisplayLabel = 'Lista de Grades|Valor Venda'
      FieldName = 'nValVenda'
      Precision = 12
      Size = 2
    end
  end
  object cmdAddItem: TADOCommand
    CommandText = 
      'INSERT INTO #Temp_Grade (nCdCor   '#13#10'                        ,cCo' +
      'r     '#13#10'                        ,cMaterial'#13#10'                    ' +
      '    ,nValCusto'#13#10'                        ,nValVenda)'#13#10'           ' +
      '       VALUES(:nCdCor'#13#10'                        ,:cCor'#13#10'         ' +
      '               ,:cMaterial'#13#10'                        ,:nValCusto'#13 +
      #10'                        ,:nValVenda)'
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCor'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'cCor'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 232
        Size = 1000
        Value = Null
      end
      item
        Name = 'cMaterial'
        Attributes = [paNullable]
        DataType = ftString
        Precision = 232
        Size = 1000
        Value = Null
      end
      item
        Name = 'nValCusto'
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Size = 19
        Value = Null
      end
      item
        Name = 'nValVenda'
        DataType = ftBCD
        NumericScale = 2
        Precision = 12
        Size = 19
        Value = Null
      end>
    Left = 416
    Top = 216
  end
  object qryCorAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmCor'
      'FROM Cor'
      'WHERE nCdCor = :nPK')
    Left = 256
    Top = 248
    object qryCorAuxcNmCor: TStringField
      FieldName = 'cNmCor'
      Size = 100
    end
  end
  object qryMaterialAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmMaterial'
      'FROM Material'
      'WHERE nCdMaterial = :nPK')
    Left = 256
    Top = 216
    object qryMaterialAuxcNmMaterial: TStringField
      FieldName = 'cNmMaterial'
      Size = 100
    end
  end
end
