unit fPedCompraContrato;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  cxLookAndFeelPainters, cxButtons, cxContainer, cxEdit, cxTextEdit,
  DBCtrlsEh, DBLookupEh, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmPedCompraContrato = class(TfrmCadastro_Padrao)
    qryMasternCdPedido: TIntegerField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryMasternCdTipoPedido: TIntegerField;
    qryMasternCdTabTipoPedido: TIntegerField;
    qryMasternCdTerceiro: TIntegerField;
    qryMasternCdTerceiroColab: TIntegerField;
    qryMasternCdTerceiroTransp: TIntegerField;
    qryMasternCdTerceiroPagador: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMasternCdCondPagto: TIntegerField;
    qryMasternCdTabStatusPed: TIntegerField;
    qryMasternCdIncoterms: TIntegerField;
    qryMasternPercDesconto: TBCDField;
    qryMasternPercAcrescimo: TBCDField;
    qryMasternValProdutos: TBCDField;
    qryMasternValServicos: TBCDField;
    qryMasternValImposto: TBCDField;
    qryMasternValDesconto: TBCDField;
    qryMasternValAcrescimo: TBCDField;
    qryMasternValFrete: TBCDField;
    qryMasternValOutros: TBCDField;
    qryMasternValPedido: TBCDField;
    qryMastercNrPedTerceiro: TStringField;
    qryMastercOBS: TMemoField;
    qryMasterdDtAutor: TDateTimeField;
    qryMasternCdUsuarioAutor: TIntegerField;
    qryMasterdDtRejeicao: TDateTimeField;
    qryMasternCdUsuarioRejeicao: TIntegerField;
    qryMastercMotivoRejeicao: TMemoField;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    qryLoja: TADOQuery;
    qryLojanCdLoja: TAutoIncField;
    qryLojacNmLoja: TStringField;
    qryTipoPedido: TADOQuery;
    qryTipoPedidonCdTipoPedido: TIntegerField;
    qryTipoPedidocNmTipoPedido: TStringField;
    qryTipoPedidonCdTabTipoPedido: TIntegerField;
    qryTipoPedidocFlgCalcImposto: TIntegerField;
    qryTipoPedidocFlgComporRanking: TIntegerField;
    qryTipoPedidocFlgVenda: TIntegerField;
    qryTipoPedidocGerarFinanc: TIntegerField;
    qryTipoPedidocExigeAutor: TIntegerField;
    qryTipoPedidonCdEspTitPrev: TIntegerField;
    qryTipoPedidonCdEspTit: TIntegerField;
    qryTipoPedidonCdCategFinanc: TIntegerField;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceiroColab: TADOQuery;
    qryTerceiroColabnCdTerceiro: TIntegerField;
    qryTerceiroColabcCNPJCPF: TStringField;
    qryTerceiroColabcNmTerceiro: TStringField;
    qryTerceiroTransp: TADOQuery;
    qryTerceiroTranspnCdTerceiro: TIntegerField;
    qryTerceiroTranspcCNPJCPF: TStringField;
    qryTerceiroTranspcNmTerceiro: TStringField;
    qryIncoterms: TADOQuery;
    qryIncotermsnCdIncoterms: TIntegerField;
    qryIncotermscSigla: TStringField;
    qryIncotermscNmIncoterms: TStringField;
    qryIncotermscFlgPagador: TStringField;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryMoeda: TADOQuery;
    qryMoedanCdMoeda: TIntegerField;
    qryMoedacSigla: TStringField;
    qryMoedacNmMoeda: TStringField;
    qryEstoque: TADOQuery;
    qryEstoquenCdEstoque: TAutoIncField;
    qryEstoquenCdEmpresa: TIntegerField;
    qryEstoquenCdLoja: TIntegerField;
    qryEstoquenCdTipoEstoque: TIntegerField;
    qryEstoquecNmEstoque: TStringField;
    qryEstoquenCdStatus: TIntegerField;
    qryStatusPed: TADOQuery;
    qryStatusPednCdTabStatusPed: TIntegerField;
    qryStatusPedcNmTabStatusPed: TStringField;
    qryTerceiroPagador: TADOQuery;
    qryTerceiroPagadornCdTerceiro: TIntegerField;
    qryTerceiroPagadorcCNPJCPF: TStringField;
    qryTerceiroPagadorcNmTerceiro: TStringField;
    qryMastercNmContato: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit4: TDBEdit;
    Label5: TLabel;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    Label7: TLabel;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    DBEdit12: TDBEdit;
    DataSource1: TDataSource;
    DataSource2: TDataSource;
    DBEdit14: TDBEdit;
    DataSource3: TDataSource;
    DBEdit15: TDBEdit;
    DataSource4: TDataSource;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DataSource5: TDataSource;
    qryMasternCdEstoqueMov: TIntegerField;
    DataSource6: TDataSource;
    DataSource7: TDataSource;
    Label17: TLabel;
    DBEdit24: TDBEdit;
    DataSource8: TDataSource;
    DBEdit27: TDBEdit;
    DataSource9: TDataSource;
    DataSource10: TDataSource;
    cxPageControl1: TcxPageControl;
    TabItemEstoque: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryUsuarioTipoPedido: TADOQuery;
    qryUsuarioTipoPedidonCdTipoPedido: TIntegerField;
    qryUsuarioLoja: TADOQuery;
    qryUsuarioLojanCdLoja: TIntegerField;
    qryTerceironCdTerceiroPagador: TIntegerField;
    Label19: TLabel;
    DBEdit30: TDBEdit;
    Label20: TLabel;
    DBEdit31: TDBEdit;
    Label21: TLabel;
    DBEdit32: TDBEdit;
    Label22: TLabel;
    DBEdit33: TDBEdit;
    Label25: TLabel;
    DBEdit36: TDBEdit;
    Label26: TLabel;
    DBEdit37: TDBEdit;
    Label27: TLabel;
    DataSource11: TDataSource;
    TabParcela: TcxTabSheet;
    qryItemEstoque: TADOQuery;
    qryItemEstoquenCdProduto: TIntegerField;
    qryItemEstoquecNmItem: TStringField;
    qryItemEstoquenQtdePed: TBCDField;
    qryItemEstoquenQtdeExpRec: TBCDField;
    qryItemEstoquenQtdeCanc: TBCDField;
    qryItemEstoquenValUnitario: TBCDField;
    qryItemEstoquenValDesconto: TBCDField;
    qryItemEstoquenValSugVenda: TBCDField;
    qryItemEstoquenValTotalItem: TBCDField;
    qryItemEstoquenPercIPI: TBCDField;
    qryItemEstoquenValIPI: TBCDField;
    dsItemEstoque: TDataSource;
    qryItemEstoquenCdPedido: TIntegerField;
    qryItemEstoquenCdTipoItemPed: TIntegerField;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    qryItemEstoquecCalc: TStringField;
    qryTerceironCdTerceiroTransp: TIntegerField;
    qryTerceironCdIncoterms: TIntegerField;
    qryTerceironCdCondPagto: TIntegerField;
    qryTerceironPercDesconto: TBCDField;
    qryTerceironPercAcrescimo: TBCDField;
    DBGridEh2: TDBGridEh;
    usp_Grade: TADOStoredProc;
    dsGrade: TDataSource;
    qryTemp: TADOQuery;
    usp_Gera_SubItem: TADOStoredProc;
    qryItemEstoquenCdItemPedido: TAutoIncField;
    cmdExcluiSubItem: TADOCommand;
    qryAux: TADOQuery;
    cxButton1: TcxButton;
    qryItemEstoquenValAcrescimo: TBCDField;
    qryItemEstoquenValCustoUnit: TBCDField;
    DBGridEh3: TDBGridEh;
    qryPrazoPedido: TADOQuery;
    dsPrazoPedido: TDataSource;
    qryPrazoPedidonCdPrazoPedido: TAutoIncField;
    qryPrazoPedidonCdPedido: TIntegerField;
    qryPrazoPedidodVencto: TDateTimeField;
    qryPrazoPedidoiDias: TIntegerField;
    qryPrazoPedidonValPagto: TBCDField;
    btSugParcela: TcxButton;
    usp_Sugere_Parcela: TADOStoredProc;
    Image3: TImage;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    qryProdutonCdGrade: TIntegerField;
    qryItemAD: TADOQuery;
    qryItemADcCdProduto: TStringField;
    qryItemADcNmItem: TStringField;
    qryItemADnQtdePed: TBCDField;
    qryItemADnQtdeExpRec: TBCDField;
    qryItemADnQtdeCanc: TBCDField;
    qryItemADnValTotalItem: TBCDField;
    qryItemADnValCustoUnit: TBCDField;
    qryItemADnCdPedido: TIntegerField;
    qryItemADnCdTipoItemPed: TIntegerField;
    qryItemADnCdItemPedido: TAutoIncField;
    dsItemAD: TDataSource;
    qryItemADcSiglaUnidadeMedida: TStringField;
    qryTipoPedidocFlgAtuPreco: TIntegerField;
    qryTipoPedidocFlgItemEstoque: TIntegerField;
    qryTipoPedidocFlgItemAD: TIntegerField;
    qryTipoPedidonCdTipoPedido_1: TIntegerField;
    qryTipoPedidonCdUsuario: TIntegerField;
    usp_Finaliza: TADOStoredProc;
    ToolButton12: TToolButton;
    Label23: TLabel;
    Label24: TLabel;
    Label29: TLabel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxTextEdit3: TcxTextEdit;
    qryDadoAutorz: TADOQuery;
    qryDadoAutorzcDadoAutoriz: TStringField;
    DBEdit34: TDBEdit;
    DataSource12: TDataSource;
    qryMastercFlgCritico: TIntegerField;
    qryMasternSaldoFat: TBCDField;
    TabFollowUp: TcxTabSheet;
    DBGridEh5: TDBGridEh;
    DBMemo1: TDBMemo;
    qryFollow: TADOQuery;
    qryFollowdDtFollowUp: TDateTimeField;
    qryFollowcNmUsuario: TStringField;
    qryFollowcOcorrenciaResum: TStringField;
    qryFollowdDtProxAcao: TDateTimeField;
    qryFollowcOcorrencia: TMemoField;
    DataSource13: TDataSource;
    qryItemFormula: TADOQuery;
    qryItemFormulacNmItem: TStringField;
    qryItemFormulanQtdePed: TBCDField;
    qryItemFormulanQtdeExpRec: TBCDField;
    qryItemFormulanQtdeCanc: TBCDField;
    qryItemFormulanValTotalItem: TBCDField;
    qryItemFormulanValCustoUnit: TBCDField;
    qryItemFormulanCdPedido: TIntegerField;
    qryItemFormulanCdTipoItemPed: TIntegerField;
    qryItemFormulanCdItemPedido: TAutoIncField;
    qryItemFormulacSiglaUnidadeMedida: TStringField;
    dsItemFormula: TDataSource;
    qryItemFormulanCdProduto: TIntegerField;
    qryProdutoFormulado: TADOQuery;
    qryProdutoFormuladonCdProduto: TIntegerField;
    qryProdutoFormuladocNmProduto: TStringField;
    usp_gera_item_embalagem: TADOStoredProc;
    qryTipoPedidocFlgExibeAcomp: TIntegerField;
    qryTipoPedidocFlgCompra: TIntegerField;
    qryTipoPedidocFlgItemFormula: TIntegerField;
    cxButton3: TcxButton;
    qryMastercOBSFinanc: TMemoField;
    qryMasternPercDescontoVencto: TBCDField;
    qryMasternPercAcrescimoVendor: TBCDField;
    TabLocalEntrega: TcxTabSheet;
    TabCentroCusto: TcxTabSheet;
    DBGridEh7: TDBGridEh;
    qryItemLocalEntrega: TADOQuery;
    qryItemLocalEntreganCdItemPedido: TAutoIncField;
    qryItemLocalEntreganCdPedido: TIntegerField;
    qryItemLocalEntreganCdItemPedidoPai: TIntegerField;
    qryItemLocalEntreganCdProduto: TIntegerField;
    qryItemLocalEntregacCdProduto: TStringField;
    qryItemLocalEntreganCdTipoItemPed: TIntegerField;
    qryItemLocalEntreganCdEstoqueMov: TIntegerField;
    qryItemLocalEntregacNmItem: TStringField;
    qryItemLocalEntreganQtdePed: TBCDField;
    qryItemLocalEntreganQtdeExpRec: TBCDField;
    qryItemLocalEntreganQtdeCanc: TBCDField;
    qryItemLocalEntreganValUnitario: TBCDField;
    qryItemLocalEntreganPercIPI: TBCDField;
    qryItemLocalEntreganValIPI: TBCDField;
    qryItemLocalEntreganValDesconto: TBCDField;
    qryItemLocalEntreganValCustoUnit: TBCDField;
    qryItemLocalEntreganValSugVenda: TBCDField;
    qryItemLocalEntreganValTotalItem: TBCDField;
    qryItemLocalEntreganValAcrescimo: TBCDField;
    qryItemLocalEntregacSiglaUnidadeMedida: TStringField;
    qryItemLocalEntregaiColuna: TIntegerField;
    qryItemLocalEntreganCdTabStatusItemPed: TIntegerField;
    qryItemLocalEntreganValUnitarioEsp: TBCDField;
    ADOQuery1: TADOQuery;
    dsItemLocalEntrega: TDataSource;
    qryLocalEstoque: TADOQuery;
    qryLocalEstoquenCdLocalEstoque: TIntegerField;
    qryLocalEstoquecNmLocalEstoque: TStringField;
    qryItemLocalEntregacNmLocalEstoque: TStringField;
    Image2: TImage;
    Label14: TLabel;
    DBGridEh8: TDBGridEh;
    qryItemCC: TADOQuery;
    qryItemCCnCdItemPedido: TAutoIncField;
    qryItemCCcNmItem: TStringField;
    dsItemCC: TDataSource;
    qryCCItemPedido: TADOQuery;
    dsCCItemPedido: TDataSource;
    qryCCItemPedidonCdItemPedido: TIntegerField;
    qryCCItemPedidonCdCC: TIntegerField;
    qryCCItemPedidonPercent: TBCDField;
    qryCentroCusto: TADOQuery;
    qryCentroCustonCdCC: TIntegerField;
    qryCentroCustocNmCC: TStringField;
    qryCentroCustonCdCC1: TIntegerField;
    qryCentroCustonCdCC2: TIntegerField;
    qryCentroCustocCdCC: TStringField;
    qryCentroCustocCdHie: TStringField;
    qryCentroCustoiNivel: TSmallintField;
    qryCentroCustocFlgLanc: TIntegerField;
    qryCentroCustonCdStatus: TIntegerField;
    qryCCItemPedidocNmCC: TStringField;
    DBComboItens: TDBLookupComboboxEh;
    cxButton4: TcxButton;
    usp_copia_cc: TADOStoredProc;
    qryTerceiroRepres: TADOQuery;
    dsTerceiroRepres: TDataSource;
    qryTerceiroRepresnCdTerceiro: TIntegerField;
    qryTerceiroReprescNmTerceiro: TStringField;
    qryMasternCdMotBloqPed: TIntegerField;
    qryMasternCdTerceiroRepres: TIntegerField;
    qryTerceironCdTerceiroRepres: TIntegerField;
    ToolButton13: TToolButton;
    qryItemEstoquedDtEntregaIni: TDateTimeField;
    qryItemEstoquedDtEntregaFim: TDateTimeField;
    qryItemEstoquenQtdePrev: TBCDField;
    qryItemEstoquenPercentCompra: TBCDField;
    qryItemLocalEntreganQtdeLibFat: TBCDField;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    DBEdit7: TDBEdit;
    qryComprador: TADOQuery;
    qryCompradornCdUsuario: TIntegerField;
    qryCompradorcNmUsuario: TStringField;
    DBEdit8: TDBEdit;
    DataSource14: TDataSource;
    qryMasternCdUsuarioComprador: TIntegerField;
    DBEdit9: TDBEdit;
    qryItemEstoquecSiglaUnidadeMedida: TStringField;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure DBEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit20KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryTerceiroAfterScroll(DataSet: TDataSet);
    procedure DBEdit23KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit25KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit24KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit5Exit(Sender: TObject);
    procedure DBEdit38KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBEdit4Exit(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure DBEdit24Exit(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure qryItemEstoqueBeforePost(DataSet: TDataSet);
    procedure qryItemEstoqueCalcFields(DataSet: TDataSet);
    procedure DBGridEh1ColExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure DBEdit19KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueAfterPost(DataSet: TDataSet);
    procedure qryMasterBeforePost(DataSet: TDataSet);
    procedure qryTempAfterPost(DataSet: TDataSet);
    procedure qryItemEstoqueBeforeDelete(DataSet: TDataSet);
    procedure cxButton1Click(Sender: TObject);
    procedure qryItemEstoquenValUnitarioValidate(Sender: TField);
    procedure qryItemEstoquenValAcrescimoValidate(Sender: TField);
    procedure qryItemEstoqueAfterDelete(DataSet: TDataSet);
    procedure qryPrazoPedidoBeforePost(DataSet: TDataSet);
    procedure btSugParcelaClick(Sender: TObject);
    procedure ToolButton10Click(Sender: TObject);
    procedure qryTempAfterCancel(DataSet: TDataSet);
    procedure qryItemADBeforePost(DataSet: TDataSet);
    procedure qryItemADAfterPost(DataSet: TDataSet);
    procedure qryItemADBeforeDelete(DataSet: TDataSet);
    procedure qryItemADAfterDelete(DataSet: TDataSet);
    procedure DBGridEh1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure DBGridEh4Enter(Sender: TObject);
    procedure ToolButton12Click(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
    procedure qryItemFormulaBeforePost(DataSet: TDataSet);
    procedure qryItemFormulaAfterPost(DataSet: TDataSet);
    procedure qryItemFormulaBeforeDelete(DataSet: TDataSet);
    procedure qryItemFormulaAfterDelete(DataSet: TDataSet);
    procedure DBGridEh6KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxButton2Click(Sender: TObject);
    procedure DBGridEh6Enter(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure DBGridEh7Enter(Sender: TObject);
    procedure qryItemLocalEntregaCalcFields(DataSet: TDataSet);
    procedure qryItemLocalEntregaBeforePost(DataSet: TDataSet);
    procedure DBGridEh7KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure qryCCItemPedidoBeforePost(DataSet: TDataSet);
    procedure cxPageControl1Change(Sender: TObject);
    procedure qryCCItemPedidoCalcFields(DataSet: TDataSet);
    procedure DBGridEh8KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBComboItensChange(Sender: TObject);
    procedure cxButton4Click(Sender: TObject);
    procedure ToolButton13Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPedCompraContrato: TfrmPedCompraContrato;

implementation

uses Math,fMenu, fLookup_Padrao, fEmbFormula,
  fPrecoEspPedCom, rPedCom_Contrato;

{$R *.dfm}


procedure TfrmPedCompraContrato.GradHorizontal(Canvas:TCanvas; Rect:TRect; FromColor, ToColor:TColor) ;
var
  X:integer;
  dr,dg,db:Extended;
  C1,C2:TColor;
  r1,r2,g1,g2,b1,b2:Byte;
  R,G,B:Byte;
  cnt:integer;
begin
  C1 := FromColor;
  R1 := GetRValue(C1) ;
  G1 := GetGValue(C1) ;
  B1 := GetBValue(C1) ;

  C2 := ToColor;
  R2 := GetRValue(C2) ;
  G2 := GetGValue(C2) ;
  B2 := GetBValue(C2) ;

  dr := (R2-R1) / Rect.Right-Rect.Left;
  dg := (G2-G1) / Rect.Right-Rect.Left;
  db := (B2-B1) / Rect.Right-Rect.Left;

  cnt := 0;
  for X := Rect.Left to Rect.Right-1 do
  begin
    R := R1+Ceil(dr*cnt) ;
    G := G1+Ceil(dg*cnt) ;
    B := B1+Ceil(db*cnt) ;

    Canvas.Pen.Color := RGB(R,G,B) ;
    Canvas.MoveTo(X,Rect.Top) ;
    Canvas.LineTo(X,Rect.Bottom) ;
    inc(cnt) ;
  end;
end;

procedure TfrmPedCompraContrato.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PEDIDO' ;
  nCdTabelaSistema  := 30 ;
  nCdConsultaPadrao := 58 ;
  bLimpaAposSalvar  := False ;

  DBGridEh2.Top  := 408;
  DBGridEh2.Left := 80 ;

end;

procedure TfrmPedCompraContrato.btIncluirClick(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  if not qryEmpresa.Active then
  begin
      qryEmpresa.Close ;
      qryEmpresa.Parameters.ParamByName('nPK').Value := frmMenu.nCdEmpresaAtiva ;
      qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
      qryEmpresa.Open ;

      if not qryEmpresa.eof then
      begin
          qryMasternCdEmpresa.Value := frmMenu.nCdEmpresaAtiva ;
      end ;
  end ;

  qryUsuarioTipoPedido.Close ;
  qryUsuarioTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryUsuarioTipoPedido.Open ;

  if (qryUsuarioTipoPedido.eof) then
  begin
      ShowMessage('Nenhum tipo de pedido comercial vinculado para este usu�rio.') ;
      btCancelar.Click;
      abort ;
  end ;

  if (qryUsuarioTipoPedido.RecordCount = 1) then
  begin
      PosicionaQuery(qryTipoPedido,qryUsuarioTipoPedidonCdTipoPedido.AsString) ;
      qryMasternCdTipoPedido.Value := qryUsuarioTipoPedidonCdTipoPedido.Value ;
  end ;

  qryUsuarioTipoPedido.Close ;

  if (qryMasternCdTipoPedido.Value = 0) then
      DbEdit4.SetFocus
  Else DBEdit5.SetFocus ;

  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryEstoque.Parameters.ParamByName('nCdLoja').Value    := qryMasternCdLoja.Value ;

  DBGridEh1.ReadOnly   := False ;
  DBGridEh3.ReadOnly   := False ;
  btSugParcela.Enabled := True ;

end;

procedure TfrmPedCompraContrato.btCancelarClick(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;
  qryLoja.Close ;
  qryTipoPedido.Close ;

end;

procedure TfrmPedCompraContrato.DBEdit2KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(8);

            If (nPK > 0) then
            begin
                qryMasternCdEmpresa.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedCompraContrato.DBEdit3KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(59);

            If (nPK > 0) then
            begin
                qryMasternCdLoja.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedCompraContrato.DBEdit4KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(60);

            If (nPK > 0) then
            begin
                qryMasternCdTipoPedido.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedCompraContrato.DBEdit5KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            if (Trim(DbEdit4.Text) = '') then
            begin
                ShowMessage('Informe o Tipo de Pedido.') ;
                DBEdit4.SetFocus ;
                exit ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(17,'EXISTS(SELECT 1 FROM TerceiroTipoTerceiro TTT  INNER JOIN TipoPedidoTipoTerceiro TTTP ON TTTP.nCdTipoTerceiro = TTT.nCdTipoTerceiro AND TTTp.nCdTipoPedido = ' + DbEdit4.Text + ' WHERE TTT.nCdTerceiro = vTerceiros.nCdTerceiro)');

            If (nPK > 0) then
            begin
                qryMasternCdTerceiro.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedCompraContrato.DBEdit20KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(19);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroTransp.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedCompraContrato.qryTerceiroAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State <> dsBrowse) then
  begin
    if (qryTerceironCdTerceiroPagador.Value <> 0) and (qryMasternCdTerceiroPagador.Value = 0) then
    begin
      PosicionaQuery(qryTerceiroPagador,qryTerceironCdTerceiroPagador.AsString) ;
      qryMasternCdTerceiroPagador.Value := qryTerceironCdTerceiroPagador.Value ;
    end ;
  end ;

end;

procedure TfrmPedCompraContrato.DBEdit23KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(17);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroPagador.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedCompraContrato.DBEdit25KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(24);

            If (nPK > 0) then
            begin
                qryMasternCdIncoterms.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedCompraContrato.DBEdit24KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(61);

            If (nPK > 0) then
            begin
                qryMasternCdCondPagto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedCompraContrato.DBEdit5Exit(Sender: TObject);
begin
  inherited;

  if not qryTipoPedido.Active then
  begin
      ShowMessage('Informe o Tipo do Pedido.') ;
      DbEdit4.SetFocus ;
      exit ;
  end ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, dbEdit5.Text) ;

  if not qryTerceiro.active then
      exit ;

  if (qryMaster.State = dsInsert) then
  begin
      if (qryTerceironCdTerceiroTransp.Value > 0) then
      begin
          qryMasternCdTerceiroTransp.Value := qryTerceironCdTerceiroTransp.Value ;
          PosicionaQuery(qryTerceiroTransp,qryTerceironCdTerceiroTransp.asString) ;
      end ;

      if (qryTerceironCdIncoterms.Value > 0) then
      begin
          qryMasternCdIncoterms.Value := qryTerceironCdIncoterms.Value ;
          PosicionaQuery(qryIncoterms,qryTerceironCdIncoterms.asString) ;
      end ;

      if (qryTerceironCdCondPagto.Value > 0) then
      begin
          qryMasternCdCondPagto.Value := qryTerceironCdCondPagto.Value ;
          PosicionaQuery(qryCondPagto, qryTerceironCdCondPagto.asString) ;
      end ;

      if (qryTerceironCdTerceiroRepres.Value > 0) then
      begin
          qryMasternCdTerceiroRepres.Value := qryTerceironCdTerceiroRepres.Value ;
          PosicionaQuery(qryTerceiroRepres, qryTerceironCdTerceiroRepres.AsString) ;
      end ;
      
      qryMasternPercDesconto.Value := qryTerceironPercDesconto.Value ;
      qryMasternPercAcrescimo.Value := qryTerceironPercAcrescimo.Value ;
  end ;

end;

procedure TfrmPedCompraContrato.DBEdit38KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(62);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroColab.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmPedCompraContrato.DBEdit4Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryTipoPedido, DBEdit4.Text) ;

  TabItemEstoque.Enabled := False ;
  TabCentroCusto.Enabled := False ;

  if not qryTipoPedido.eof then
  begin
    if (qryTipoPedidocFlgItemEstoque.Value = 1) then
        TabItemEstoque.Enabled := True ;

    if (qryTipoPedidocGerarFinanc.Value = 1) then
    begin
        TabCentroCusto.Enabled := True ;
        TabParcela.Enabled     := True ;
    end ;

  end ;

  if tabItemEstoque.Enabled then
      cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmPedCompraContrato.DBEdit2Exit(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryEmpresa, DBEdit2.Text) ;
  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;

end;

procedure TfrmPedCompraContrato.DBEdit24Exit(Sender: TObject);
begin
  inherited;
  qryCondPagto.Close ;
  PosicionaQuery(qryCondPagto, DBEdit24.Text) ;
end;

procedure TfrmPedCompraContrato.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if (qryMaster.State = dsInsert) then
      exit ;

  if (qryMasternCdEmpresa.Value <> frmMenu.nCdEmpresaAtiva) then
  begin
      ShowMessage('Este pedido n�o pertence a esta empresa.') ;
      btCancelar.Click;
      exit ;
  end ;

  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

  PosicionaQuery(qryEmpresa, qryMasternCdEmpresa.asString) ;
  PosicionaQuery(qryTipoPedido, qryMasternCdTipoPedido.asString) ;

  if (qryTipoPedido.eof) then
  begin
      ShowMessage('Voc� n�o tem autoriza��o para visualizar este tipo de pedido.') ;
      btCancelar.Click;
      exit;
  end ;

  PosicionaQuery(qryLoja, qryMasternCdLoja.asString) ;

  if not qryLoja.Eof then
      qryEstoque.Parameters.ParamByName('nCdLoja').Value := qryLojanCdLoja.Value ;
      
  qryEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryEmpresanCdEmpresa.Value ;
  PosicionaQuery(qryEstoque, qryMasternCdEstoqueMov.asString) ;
  PosicionaQuery(qryTerceiroColab, qryMasternCdTerceiroColab.asString) ;
  PosicionaQuery(qryTerceiroPagador,qryMasternCdTerceiroPagador.asString) ;
  PosicionaQuery(qryCondPagto, qryMasternCdCondPagto.asString) ;
  PosicionaQuery(qryIncoterms,qryMasternCdIncoterms.asString) ;
  PosicionaQuery(qryTerceiroTransp, qryMasternCdTerceiroTransp.asString) ;

  qryTerceiro.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

  PosicionaQuery(qryTerceiro, qryMasternCdTerceiro.asString) ;

  PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryDadoAutorz,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryFollow,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryItemFormula,qryMasternCdPedido.asString) ;
  PosicionaQuery(qryTerceiroRepres, qryMasternCdTerceiroRepres.AsString) ;
  Posicionaquery(qryComprador, qryMasternCdUsuarioComprador.AsString) ;


  DBGridEh1.ReadOnly   := False ;
  DBGridEh3.ReadOnly   := False ;

  btSugParcela.Enabled := True ;

  if (qryMasternCdTabStatusPed.Value > 1) then
  begin
      DBGridEh1.ReadOnly   := True ;
      DBGridEh3.ReadOnly   := True ;
      btSugParcela.Enabled := False ;
  end ;

  TabItemEstoque.Enabled := False ;

  if (qryTipoPedidocFlgItemEstoque.Value = 1) then
      TabItemEstoque.Enabled := True ;

  if (qryTipoPedidocGerarFinanc.Value = 1) then
  begin
      TabCentroCusto.Enabled := True ;
      TabParcela.Enabled     := True ;
  end ;

  if tabItemEstoque.Enabled then
      cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmPedCompraContrato.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;

  qryEmpresa.Close ;
  qryTipoPedido.Close ;
  qryLoja.Close ;
  qryEstoque.Close ;
  qryTerceiroColab.Close ;
  qryTerceiroPagador.Close ;
  qryCondPagto.Close ;
  qryIncoterms.Close ;
  qryTerceiroTransp.Close ;
  qryTerceiro.Close ;
  qryItemEstoque.Close ;
  qryStatusPed.Close ;
  qryPrazoPedido.Close ;
  qryItemAD.Close ;
  qryDadoAutorz.Close ;
  qryFollow.Close ;
  qryItemFormula.Close ;
  qryTerceiroRepres.Close ;
  qryComprador.Close ;

  qryItemCC.Close ;
  qryItemLocalEntrega.Close ;
  qryCCItemPedido.Close ;

  TabCentroCusto.Enabled := False ;
  TabParcela.Enabled     := False ;
  TabItemEstoque.Enabled := False ;

  cxPageControl1.ActivePageIndex := 0 ;

end;

procedure TfrmPedCompraContrato.qryItemEstoqueBeforePost(DataSet: TDataSet);
begin

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      ShowMessage('Informe o produto.') ;
      abort ;
  end ;

  qryItemEstoquenCdPedido.Value      := qryMasternCdPedido.Value ;

  if (qryProdutonCdGrade.Value = 0) then
      qryItemEstoquenCdTipoItemPed.Value := 2
  else  qryItemEstoquenCdTipoItemPed.Value := 1 ;

  inherited;

  qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) ;

  if (qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) > 0 then
      qryItemEstoquenValCustoUnit.Value := qryItemEstoquenValCustoUnit.Value + 0.005 ;

  qryItemEstoquenValTotalItem.Value := (qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value) ; //+ 0.005  ;

  if (qryItemEstoque.State = dsEdit) then
  begin
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloat(qryItemEstoquenValTotalItem.OldValue) ;
  end ;

  qryItemEstoquecNmItem.Value := Uppercase(qryItemEstoquecNmItem.Value) ;


end;

procedure TfrmPedCompraContrato.qryItemEstoqueCalcFields(DataSet: TDataSet);
begin
  inherited;

  if (qryItemEstoquecNmItem.Value = '') then
  begin
      qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
      PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

      if not qryProduto.eof then
          qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;
  end ;

end;

procedure TfrmPedCompraContrato.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  if (DbGridEh1.Col = 2) then
      DBGridEh1.Col := 3 ;
  
  If (DBGridEh1.Col = 1) then
  begin

      DBGridEh1.Columns[2].ReadOnly   := True ;

      if (qryItemEstoque.State <> dsBrowse) then
      begin

        {qryAux.Close ;
        qryAux.SQL.Clear ;
        qryAux.SQL.Add('SELECT 1 FROM ItemPedido WHERE nCdPedido = ' + qryMasternCdPedido.asString + ' AND nCdProduto = ' + qryItemEstoquenCdProduto.asString) ;
        qryAux.Open ;

        if not qryAux.Eof then
        begin
            qryAux.Close ;
            ShowMessage('Item j� digitado neste pedido.') ;
            abort ;
        end ;

        qryAux.Close ;}

        qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;

        PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

        if not qryProduto.eof and (qryProdutonCdGrade.Value > 0) then
        begin
            qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;

            DBGridEh2.AutoFitColWidths := True ;

            usp_Grade.Close ;
            usp_Grade.Parameters.ParamByName('@nCdPedido').Value  := qryMasternCdPedido.Value ;
            usp_Grade.Parameters.ParamByName('@nCdProduto').Value := qryItemEstoquenCdProduto.Value ;
            usp_Grade.ExecProc ;

            qryTemp.Close ;
            qryTemp.SQL.Clear ;
            qryTemp.SQL.Add(usp_Grade.Parameters.ParamByName('@cSQLRetorno').Value);
            qryTemp.Open ;

            qryTemp.Edit ;

            DBGridEh2.Columns.Items[0].Visible := False ;

            DBGridEh2.Visible := True ;

            DBGridEh2.SetFocus ;

            DBGridEh1.Col := 3 ;
        end ;

        if not qryProduto.eof and (qryProdutonCdGrade.Value = 0) then
        begin

            DBGridEh1.Columns[2].ReadOnly   := False ;
            DBGridEh1.Col := 2 ;

        end ;

      end ;

  end ;


end;

procedure TfrmPedCompraContrato.FormShow(Sender: TObject);
begin
  inherited;
  cxPageControl1.ActivePageIndex := 0 ;

  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
  qryLoja.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  qryTipoPedido.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;

end;

procedure TfrmPedCompraContrato.DBEdit19KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryMaster.State = dsInsert) or (qryMaster.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta(103);

            If (nPK > 0) then
            begin
                qryMasternCdTerceiroRepres.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedCompraContrato.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  PosicionaQuery(qryStatusPed,qryMasternCdTabStatusPed.asString) ;

  if not qryItemEstoque.Active then
      PosicionaQuery(qryItemEstoque,qryMasternCdPedido.asString) ;

  if not qryItemAD.Active then
      PosicionaQuery(qryItemAD,qryMasternCdPedido.asString) ;

  if not qryItemFormula.Active then
      PosicionaQuery(qryItemFormula,qryMasternCdPedido.asString) ;
end;

procedure TfrmPedCompraContrato.qryItemEstoqueAfterPost(DataSet: TDataSet);
begin
  usp_Gera_SubItem.Close ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdPedido').Value     := qryMasternCdPedido.Value ;
  usp_Gera_SubItem.Parameters.ParamByName('@nCdItemPedido').Value := qryItemEstoquenCdItemPedido.Value ;
  usp_Gera_SubItem.ExecProc ;

  inherited;
  qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemEstoquenValTotalItem.Value ;
  qryMaster.Post ;
end;

procedure TfrmPedCompraContrato.qryMasterBeforePost(DataSet: TDataSet);
begin

  if (qryMaster.State = dsInsert) then
  begin
      qryMasternCdTabTipoPedido.Value := qryTipoPedidonCdTabTipoPedido.Value ;
      qryMasternCdTabStatusPed.Value  := 1 ;
  end ;

  if (qryMasternCdEmpresa.Value = 0) then
  begin
      MensagemAlerta('Informe a empresa.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTipoPedido.Value = 0) or (DbEdit14.Text = '') then
  begin
      MensagemAlerta('Informe o tipo de pedido.') ;
      DbEdit4.SetFocus ;
      abort ;
  end ;

  if (qryMasternCdTerceiro.Value = 0) or (DbEdit15.Text = '') then
  begin
      MensagemAlerta('Informe o Terceiro.') ;
      DbEdit5.SetFocus ;
      abort ;
  end ;

  if (qryMasterdDtPedido.asString = '') then
  begin
      MensagemAlerta('Informe a data do pedido.') ;
      DbEdit6.SetFocus ;
      abort ;
  end ;

  inherited;

  if (qryMaster.State = dsInsert) then
      qryMasternCdTabStatusPed.Value := 1;

  qryMasternValPedido.Value := qryMasternValOutros.Value + qryMasternValAcrescimo.Value - qryMasternValDesconto.Value + qryMasternValImposto.Value + qryMasternValServicos.Value + qryMasternValProdutos.Value ;
end;

procedure TfrmPedCompraContrato.qryTempAfterPost(DataSet: TDataSet);
var
  i     : integer ;
  iQtde : integer ;
begin
  inherited;

  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmPedCompraContrato.qryItemEstoqueBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemEstoquenCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemEstoquenValTotalItem.Value ;


end;

procedure TfrmPedCompraContrato.cxButton1Click(Sender: TObject);
begin
  if not qryItemEstoque.Active then
  begin
      MensagemAlerta('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
  PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

  if qryProduto.eof then
  begin
      MensagemAlerta('Selecione um item.') ;
      exit ;
  end ;

  if (qryProdutonCdGrade.Value = 0) then
  begin
      MensagemAlerta('Este item n�o est� configurado para trabalhar em grade.') ;
      exit ;
  end ;


  DBGridEh2.AutoFitColWidths := True ;

  usp_Grade.Close ;
  usp_Grade.Parameters.ParamByName('@nCdPedido').Value  := qryMasternCdPedido.Value ;
  usp_Grade.Parameters.ParamByName('@nCdProduto').Value := qryItemEstoquenCdProduto.Value ;
  usp_Grade.ExecProc ;

  qryTemp.Close ;
  qryTemp.SQL.Clear ;
  qryTemp.SQL.Add(usp_Grade.Parameters.ParamByName('@cSQLRetorno').Value);
  qryTemp.Open ;

  qryTemp.First ;

  if (qryTemp.State = dsBrowse) then
      qryTemp.Edit
  else qryTemp.Insert ;

  qryTemp.FieldList[0].Value := qryTemp.FieldList[0].Value ; 

  DBGridEh2.Columns.Items[0].Visible := False ;

  DBGridEh2.Visible := True ;
  DBGridEh2.SetFocus ;

end;

procedure TfrmPedCompraContrato.qryItemEstoquenValUnitarioValidate(
  Sender: TField);
begin
  inherited;

  if (qryMasternPercDesconto.Value > 0) then
      qryItemEstoquenValDesconto.Value := qryItemEstoquenValUnitario.Value * (qryMasternPercDesconto.AsFloat  / 100) ;

  if (qryMasternPercAcrescimo.Value > 0) then
      qryItemEstoquenValAcrescimo.Value := qryItemEstoquenValUnitario.Value * (qryMasternPercAcrescimo.AsFloat / 100) ;

end;

procedure TfrmPedCompraContrato.qryItemEstoquenValAcrescimoValidate(
  Sender: TField);
begin
  inherited;
  qryItemEstoquenValCustoUnit.Value := (qryItemEstoquenValUnitario.Value - qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) ;

  if (qryItemEstoquenValDesconto.Value + qryItemEstoquenValAcrescimo.Value) > 0 then
      qryItemEstoquenValCustoUnit.Value := qryItemEstoquenValCustoUnit.Value + 0.005 ;
  
  qryItemEstoquenValTotalItem.Value := qryItemEstoquenQtdePed.Value * qryItemEstoquenValCustoUnit.Value ;

end;

procedure TfrmPedCompraContrato.qryItemEstoqueAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmPedCompraContrato.qryPrazoPedidoBeforePost(DataSet: TDataSet);
begin
  inherited;

  qryPrazoPedidonCdPedido.Value := qryMasternCdPedido.Value ;

  if (qryPrazoPedidoiDias.Value > 0) then
      qryPrazoPedidodVencto.Value := qryMasterdDtPrevEntIni.Value + qryPrazoPedidoiDias.Value ;

  if (qryPrazoPedidodVencto.asString = '') then
  begin
      MensagemAlerta('Informe a data do vencimento.') ;
      abort ;
  end ;

  if (qryPrazoPedidonValPagto.Value <= 0) then
  begin
      MensagemAlerta('Informe o valor da parcela.') ;
      abort ;
  end ;

end;

procedure TfrmPedCompraContrato.btSugParcelaClick(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternValPedido.Value = 0) then
  begin
      MensagemAlerta('Pedido sem valor.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_Sugere_Parcela.Close ;
      usp_Sugere_Parcela.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Sugere_Parcela.ExecProc ;
  except
      frmMenu.Connection.RollbackTrans ;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans ;

  PosicionaQuery(qryPrazoPedido,qryMasternCdPedido.asString) ;

end;

procedure TfrmPedCompraContrato.ToolButton10Click(Sender: TObject);
begin

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMaster.State = dsInsert) then
  begin
      MensagemAlerta('Salve o pedido antes de utilizar esta fun��o.') ;
  end ;

  if (qryMaster.State = dsEdit) then
  begin
      qryMaster.Post ;
  end ;

  if (qryMasternValPedido.Value = 0) then
  begin

      case MessageDlg('Pedido sem valor. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
        mrNo:Abort;
      end;

  end ;

  if (qryTipoPedidocGerarFinanc.Value = 1) then
  begin
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT Sum(nValPagto) FROM PrazoPedido WHERE nCdPedido = ' + qryMasternCdPedido.asString) ;
      qryAux.Open ;

      If (qryAux.Eof and (qryMasternValPedido.Value > 0)) or (qryAux.FieldList[0].Value <> qryMasternValPedido.Value) then
      begin
          qryAux.Close ;
          MensagemAlerta('Valor da soma das parcelas � diferente do valor total dos pedidos') ;
          exit ;
      end ;

      qryAux.Close ;


      // Confere os centros de custo
      if (frmMenu.LeParametroEmpresa('USACCCOMPRA') = 'S') then
      begin

          cxPageControl1.ActivePage := TabCentroCusto ;

          qryItemCC.First;

          while not qryItemCC.Eof do
          begin

              qryAux.Close ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('SELECT Sum(nPercent) FROM CentroCustoItemPedido WHERE nCdItemPedido = ' + qryItemCCnCdItemPedido.AsString);
              qryAux.Open ;

              if qryAux.Eof or (qryAux.FieldList[0].Value <> 100) then
              begin
                  MensagemAlerta('O rateio de centro de custo do item � diferente de 100%.' + #13#13 + 'Item: ' + qryItemCCcNmItem.Value) ;
                  exit ;
              end ;

              qryItemCC.Next;
          end ;

          qryItemCC.First;
          
      end ;

  end ;

  case MessageDlg('O pedido ser� finalizado e n�o poder� sofrer altera��es. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Abort;
  end;

  frmMenu.Connection.BeginTrans ;

  try
      qryMaster.Edit ;
      // verifica se o tipo do pedido exige autoriza��o
      if (qryTipoPedidocExigeAutor.Value = 1) then
          qryMasternCdTabStatusPed.Value := 2  // Aguardando aprova��o
      else begin
          qryMasternCdTabStatusPed.Value := 3 ; // Aprovado
          qryMasterdDtAutor.Value        := Now() ;
          qryMasternCdUsuarioAutor.Value := frmMenu.nCdUsuarioLogado;

      end ;

      qryMasternSaldoFat.Value := qryMasternValPedido.Value ;

      usp_Finaliza.Close ;
      usp_Finaliza.Parameters.ParamByName('@nCdPedido').Value := qryMasternCdPedido.Value ;
      usp_Finaliza.ExecProc;
      
      qryMaster.Post ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Pedido finalizado com sucesso.') ;
  btCancelar.Click;



end;

procedure TfrmPedCompraContrato.qryTempAfterCancel(DataSet: TDataSet);
var
    iQtde : integer ;
    i : Integer ;
begin
  inherited;
  iQtde := 0 ;

  for i := 1 to qryTemp.FieldList.Count-1 do
  begin
      iQtde := iQtde + qryTemp.Fields[i].Value ;
  end ;


  if (qryMasternCdTabStatusPed.Value <= 1) then
  begin

      if (qryItemEstoque.State = dsBrowse) then
          qryItemEstoque.Edit ;

      qryItemEstoquenQtdePed.Value := iQtde ;

      DBGridEh1.Col := 3 ;
  end ;

  DBGridEh1.SetFocus ;
  DBGridEh2.Visible := False ;

end;

procedure TfrmPedCompraContrato.qryItemADBeforePost(DataSet: TDataSet);
begin

  if (qryItemADcNmItem.Value = '') then
  begin
      ShowMessage('Informe a descri��o do item.') ;
      abort ;
  end ;

  if (qryItemADcSiglaUnidadeMedida.Value = '') then
  begin
      ShowMessage('Informe a unidade de medida do item.') ;
      abort ;
  end ;

  if (qryItemADnQtdePed.Value <= 0) then
  begin
      ShowMessage('Informe a quantidade do item.') ;
      abort ;
  end ;

  if (qryItemADnValCustoUnit.Value < 0) then
  begin
      ShowMessage('Valor unit�rio inv�lido.') ;
      abort ;
  end ;

  inherited;

  if (qryItemAD.State = dsInsert) then
  begin
      qryItemADnCdPedido.Value      := qryMasternCdPedido.Value ;
      qryItemADnCdTipoItemPed.Value := 5 ;
      qryItemADcCdProduto.Value     := 'AD' + qryItemADnCdItemPedido.AsString;
  end ;

  qryItemADcNmItem.Value             := UpperCase(qryItemADcNmItem.Value) ;
  qryItemADcSiglaUnidadeMedida.Value := UpperCase(qryItemADcSiglaUnidadeMedida.Value) ;

  qryItemADnValTotalItem.Value := qryItemADnQtdePed.Value * qryItemADnValCustoUnit.Value ;

  if (qryItemAD.State = dsEdit) then
  begin
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloat(qryItemADnValTotalItem.OldValue) ;
  end ;

end;

procedure TfrmPedCompraContrato.qryItemADAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryItemAD.Edit ;
  qryItemADcCdProduto.Value   := 'AD' + qryItemADnCdItemPedido.AsString;
  //qryItemAD.Post ;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemADnValTotalItem.Value ;
  qryMaster.Post ;

end;

procedure TfrmPedCompraContrato.qryItemADBeforeDelete(DataSet: TDataSet);
begin
  inherited;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemADnCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    MensagemErro('Erro no processamento.') ;
    raise ;
  end ;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemADnValTotalItem.Value ;

end;

procedure TfrmPedCompraContrato.qryItemADAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;
end;

procedure TfrmPedCompraContrato.DBGridEh1KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemEstoque.State = dsBrowse) then
             qryItemEstoque.Edit ;

        if (qryItemEstoque.State = dsInsert) or (qryItemEstoque.State = dsEdit) then
        begin
            If (qryMasternCdTipoPedido.asString = '') then
            begin
                ShowMessage('Selecione um tipo de pedido.') ;
                abort ;
            end ;

            nPK := frmLookup_Padrao.ExecutaConsulta2(68,'EXISTS(SELECT 1 FROM GrupoProdutoTipoPedido GPTP WHERE GPTP.nCdGrupoProduto = Departamento.nCdGrupoProduto AND GPTP.nCdTipoPedido = ' + qryMasternCdTipoPedido.asString + ')');

            If (nPK > 0) then
            begin
                qryItemEstoquenCdProduto.Value := nPK ;

                qryProduto.Parameters.ParamByName('nCdTipoPedido').Value := qryTipoPedidonCdTipoPedido.Value ;
                PosicionaQuery(qryProduto, qryItemEstoquenCdProduto.asString) ;

                if not qryProduto.eof then
                    qryItemEstoquecNmItem.Value := qryProdutocNmProduto.Value ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedCompraContrato.DBGridEh1Enter(Sender: TObject);
begin
  inherited;

  if not qryMaster.Active then
      exit ;

  if (qryMaster.State = dsInsert) then
  begin
      btSalvar.Click ;
  end ;

  DBGridEh1.Columns[8].ReadOnly := True ;

  if (qryTipoPedidocFlgAtuPreco.Value = 1) then
      DBGridEh1.Columns[8].ReadOnly := false ;


end;

procedure TfrmPedCompraContrato.DBGridEh4Enter(Sender: TObject);
begin
  inherited;
  if (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmPedCompraContrato.ToolButton12Click(Sender: TObject);
begin
  if not qryMaster.active then
  begin
      ShowMessage('Nenhum pedido ativo.') ;
      exit ;
  end ;

  PosicionaQuery(rptPedCom_Contrato.qryPedido,qryMasternCdPedido.asString) ;
  PosicionaQuery(rptPedCom_Contrato.qryItemEstoque_Grade,qryMasternCdPedido.asString) ;
  PosicionaQuery(rptPedCom_Contrato.qryItemAD,qryMasternCdPedido.asString) ;
  PosicionaQuery(rptPedCom_Contrato.qryItemFormula,qryMasternCdPedido.asString) ;

  rptPedCom_Contrato.QRSubDetail1.Enabled := True ;
  rptPedCom_Contrato.QRSubDetail2.Enabled := True ;
  rptPedCom_Contrato.QRSubDetail3.Enabled := True ;

  if (rptPedCom_Contrato.qryItemEstoque_Grade.eof) then
      rptPedCom_Contrato.QRSubDetail1.Enabled := False ;

  if (rptPedCom_Contrato.qryItemAD.eof) then
      rptPedCom_Contrato.QRSubDetail2.Enabled := False ;

  if (rptPedCom_Contrato.qryItemFormula.eof) then
      rptPedCom_Contrato.QRSubDetail3.Enabled := False ;

  rptPedCom_Contrato.lblEmpresa.Caption := frmMenu.cNmEmpresaAtiva ;

  rptPedCom_Contrato.QuickRep1.PreviewModal;

end;

procedure TfrmPedCompraContrato.DBGridEh1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);
begin
  inherited;

  If (dataCol = 3) and not(gdSelected in State) then
  begin

      // recebimento parcial
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) < qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clYellow;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

      // recebimento total
      if ((qryItemEstoquenQtdeExpRec.Value+qryItemEstoquenQtdeCanc.Value) >= qryItemEstoquenQtdePed.Value) and (qryItemEstoquenQtdeExpRec.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clBlue;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

  If (dataCol = 4) and not(gdSelected in State) then
  begin

      // item cancelado
      if (qryItemEstoquenQtdeCanc.Value > 0) then
      begin
        DBGridEh1.Canvas.Brush.Color := clRed;
        DBGridEh1.Canvas.FillRect(Rect);
        DBGridEh1.DefaultDrawDataCell(Rect,Column.Field,State);
      end ;

  end ;

end;

procedure TfrmPedCompraContrato.qryItemFormulaBeforePost(DataSet: TDataSet);
begin

  if (qryItemFormulacNmItem.Value = '') then
  begin
      ShowMessage('Informe a descri��o do item.') ;
      abort ;
  end ;

  if (qryItemFormulanQtdePed.Value <= 0) then
  begin
      ShowMessage('Informe a quantidade do item.') ;
      abort ;
  end ;

  if (qryItemFormulanValCustoUnit.Value < 0) then
  begin
      ShowMessage('Valor unit�rio inv�lido.') ;
      abort ;
  end ;

  inherited;

  if (qryItemFormula.State = dsInsert) then
  begin
      qryItemFormulanCdPedido.Value      := qryMasternCdPedido.Value ;
      qryItemFormulanCdTipoItemPed.Value := 6 ;
  end ;

  qryItemFormulanValTotalItem.Value := qryItemFormulanQtdePed.Value * qryItemFormulanValCustoUnit.Value ;

  if (qryItemFormula.State = dsEdit) then
  begin
      qryMasternValProdutos.Value := qryMasternValProdutos.Value - StrToFloat(qryItemFormulanValTotalItem.OldValue) ;
  end ;

end;

procedure TfrmPedCompraContrato.qryItemFormulaAfterPost(DataSet: TDataSet);
begin
  inherited;

  qryMasternValProdutos.Value := qryMasternValProdutos.Value + qryItemFormulanValTotalItem.Value ;
  qryMaster.Post ;

end;

procedure TfrmPedCompraContrato.qryItemFormulaBeforeDelete(
  DataSet: TDataSet);
begin
  inherited;
  qryMasternValProdutos.Value := qryMasternValProdutos.Value - qryItemFormulanValTotalItem.Value ;

  try
    cmdExcluiSubItem.Parameters.ParamByName('nPK').Value := qryItemFormulanCdItemPedido.Value ;
    cmdExcluiSubItem.Execute;
  except
    ShowMessage('Erro no processamento.') ;
    raise ;
  end ;

end;

procedure TfrmPedCompraContrato.qryItemFormulaAfterDelete(DataSet: TDataSet);
begin
  inherited;
  qryMaster.Post ;

end;

procedure TfrmPedCompraContrato.DBGridEh6KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemFormula.State = dsBrowse) then
             qryItemFormula.Edit ;


        if (qryItemFormula.State = dsInsert) or (qryItemFormula.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(70,'EXISTS(SELECT 1 FROM FormulaTipoPedido PTP WHERE PTP.nCdProdutoPai = Produto.nCdProduto AND nCdTipoPedido = ' + qryMasternCdTipoPedido.AsString + ')');

            If (nPK > 0) then
            begin
                qryItemFormulanCdProduto.Value := nPK ;
            end ;

        end ;

    end ;

  end ;
end;

procedure TfrmPedCompraContrato.cxButton2Click(Sender: TObject);
begin

  if not qryItemFormula.Active then
  begin
      ShowMessage('Nenhum item de estoque selecionado.') ;
      exit ;
  end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT TOP 1 1 FROM FormulaColuna WHERE nCdProdutoPai = ' + qryItemFormulanCdProduto.asString) ;
  qryAux.Open ;

  if not qryAux.eof then
  begin

      frmEmbFormula.nCdPedido       := qryMasternCdPedido.Value          ;
      frmEmbFormula.nCdProduto      := qryItemFormulanCdProduto.Value    ;
      frmEmbFormula.nCdItemPedido   := qryItemFormulanCdItemPedido.Value ;
      frmEmbFormula.nCdTabStatusPed := qryMasternCdTabStatusPed.Value    ;
      frmEmbFormula.RenderizaGrade() ;

      // calcula o pre�o
      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT Sum(CASE WHEN nCdProduto IS NOT NULL THEN (IsNull(nQtdeProduto,0) * IsNull(nValUnitProduto,0))') ;
      qryAux.SQL.Add('                ELSE 0       ') ;
      qryAux.SQL.Add('           END)              ') ;
      qryAux.SQL.Add('      ,Sum(CASE WHEN nCdAmostra IS NOT NULL THEN (IsNull(nQtdeAmostra,0) * IsNull(nValUnitAmostra,0))') ;
      qryAux.SQL.Add('                ELSE 0       ') ;
      qryAux.SQL.Add('           END)              ') ;
      qryAux.SQL.Add('  FROM ##Temp_Grade_Embalagem') ;
      qryAux.Open ;

      if (qryMasternCdTabStatusPed.Value <= 1) then
      begin
      
          if (qryItemFormula.State = dsBrowse) then
              qryItemFormula.Edit ;

          qryItemFormulanValCustoUnit.Value := qryAux.FieldList[0].Value + qryAux.FieldList[1].Value ;

      end ;

    end
    else begin
        ShowMessage('Produto n�o tem configura��o de embalagem.') ;
    end ;

    qryAux.Close ;


end;

procedure TfrmPedCompraContrato.DBGridEh6Enter(Sender: TObject);
begin
  inherited;
  if (qryMasternCdPedido.Value = 0) then
  begin
      btSalvar.Click ;
  end ;

end;

procedure TfrmPedCompraContrato.cxButton3Click(Sender: TObject);
begin

    if not qryMaster.active then
        exit ;

    if (qryMasternCdPedido.Value = 0) then
    begin
        ShowMessage('Nenhum pedido ativo.') ;
        exit ;
    end ;

    frmPrecoEspPedCom.nCdPedido := qryMasternCdPedido.Value ;
    frmPrecoEspPedCom.ShowModal ;

end;

procedure TfrmPedCompraContrato.DBGridEh7Enter(Sender: TObject);
begin
  inherited;
  if qryMaster.Active and (qryMasternCdPedido.Value > 0) then
      PosicionaQuery(qryItemLocalEntrega, qryMasternCdPedido.AsString) ;

end;

procedure TfrmPedCompraContrato.qryItemLocalEntregaCalcFields(
  DataSet: TDataSet);
begin
  inherited;

  qryLocalEstoque.Close ;
  qryLocalEstoque.Parameters.ParamByName('nCdEmpresa').Value := qryMasternCdEmpresa.Value ;
  qryLocalEstoque.Parameters.ParamByName('nCdProduto').Value := qryItemLocalEntreganCdProduto.Value ;
  PosicionaQuery(qryLocalEstoque, qryItemLocalEntreganCdEstoqueMov.AsString) ;

  if not qryLocalEstoque.eof then
      qryItemLocalEntregacNmLocalEstoque.Value := qryLocalEstoquecNmLocalEstoque.Value ;
  
end;

procedure TfrmPedCompraContrato.qryItemLocalEntregaBeforePost(
  DataSet: TDataSet);
begin
  if (qryItemLocalEntreganCdEstoqueMov.Value > 0) and (qryItemLocalEntregacNmLocalEstoque.Value = '') then
  begin
      MensagemAlerta('Local de Estoque inv�lido.') ;
      abort ;
  end ;

  inherited;


end;

procedure TfrmPedCompraContrato.DBGridEh7KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryItemLocalEntrega.State = dsBrowse) then
             qryItemLocalEntrega.Edit ;
        
        if (qryItemLocalEntrega.State = dsInsert) or (qryItemLocalEntrega.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(87,'LocalEstoque.nCdEmpresa = ' +qryMasternCdEmpresa.asString);

            If (nPK > 0) then
            begin
                qryItemLocalEntreganCdEstoqueMov.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedCompraContrato.qryCCItemPedidoBeforePost(DataSet: TDataSet);
begin

  if (qryCCItemPedidonCdCC.Value > 0) and (qryCCItemPedidocNmCC.Value = '') then
  begin
      MensagemAlerta('Informe um centro de custo.') ;
      abort ;
  end ;

  qryCCItemPedidonCdItemPedido.Value := qryItemCCnCdItemPedido.Value ;
  inherited;

end;

procedure TfrmPedCompraContrato.cxPageControl1Change(Sender: TObject);
begin
  inherited;

  if (cxPageControl1.ActivePage = TabLocalEntrega) then
  begin
      PosicionaQuery(qryItemLocalEntrega, qryMasternCdPedido.AsString) ;
  end ;

  if (cxPageControl1.ActivePage = TabCentroCusto) then
  begin
      PosicionaQuery(qryItemCC, qryMasternCdPedido.AsString) ;

      qryItemCC.First;
  end ;

end;

procedure TfrmPedCompraContrato.qryCCItemPedidoCalcFields(DataSet: TDataSet);
begin
  inherited;

  qryCentroCusto.Parameters.ParamByName('nCdTipoPedido').Value := qryMasternCdTipoPedido.Value ;
  
  PosicionaQuery(qryCentroCusto, qryCCItemPedidonCdCC.AsString) ;

  if not qryCentroCusto.Eof then
      qryCCItemPedidocNmCC.Value := qryCentroCustocNmCC.Value ;

end;

procedure TfrmPedCompraContrato.DBGridEh8KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (qryCCItemPedido.State = dsBrowse) then
             qryCCItemPedido.Edit ;
        
        if (qryCCItemPedido.State = dsInsert) or (qryCCItemPedido.State = dsEdit) then
        begin

            nPK := frmLookup_Padrao.ExecutaConsulta2(29,'EXISTS(SELECT 1 FROM CentroCustoTipoPedido CCTP WHERE CCTP.nCdCC = CentroCusto.nCdCC AND CCTP.nCdTipoPedido = ' + qryMasternCdTipoPedido.AsString + ')');

            If (nPK > 0) then
            begin
                qryCCItemPedidonCdCC.Value := nPK ;
            end ;

        end ;

    end ;

  end ;

end;

procedure TfrmPedCompraContrato.DBComboItensChange(Sender: TObject);
begin
  inherited;
  PosicionaQuery(qryCCItemPedido, qryItemCCnCdItemPedido.AsString) ;

end;

procedure TfrmPedCompraContrato.cxButton4Click(Sender: TObject);
begin
  inherited;

  if qryItemCC.Eof or (DBComboItens.Text = '') then
  begin
      MensagemAlerta('Selecione o item a ser copiado.') ;
      exit ;
  end ;

  frmMenu.Connection.BeginTrans;

  try
      usp_copia_cc.Close;
      usp_copia_cc.Parameters.ParamByName('@nCdItemPedido').Value := qryItemCCnCdItemPedido.Value ;
      usp_copia_cc.ExecProc;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

end;

procedure TfrmPedCompraContrato.ToolButton13Click(Sender: TObject);
begin
  inherited;

  if not qryMaster.active then
  begin
      MensagemAlerta('Nenhum pedido ativo.') ;
      exit ;
  end ;

  if (qryMasternCdTabStatusPed.Value > 2) then
  begin
      MensagemAlerta('O status do pedido n�o permite mais cancelamento.') ;
      exit ;
  end ;

  case MessageDlg('O pedido ser� cancelado. Confirma ?', mtConfirmation,[mbYes,mbNo],0) of
    mrNo:Exit;
  end;

  frmMenu.Connection.BeginTrans;

  try
      qryMaster.Edit ;
      qryMasternCdTabStatusPed.Value := 10 ;
      qryMaster.Post ;

      frmMenu.LogAuditoria(30,3,qryMasternCdPedido.Value,'Pedido Cancelado') ;

  except
      frmMenu.Connection.RollbackTrans;
      qryMaster.Cancel;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;
  
  ShowMessage('Pedido cancelado com sucesso.') ;
  btCancelar.Click;

end;

initialization
    RegisterClass(TfrmPedCompraContrato) ;

end.
