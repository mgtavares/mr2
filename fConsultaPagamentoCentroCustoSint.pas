unit fConsultaPagamentoCentroCustoSint;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid, ADODB;

type
  TfrmConsultaPagamentoCentroCustoSint = class(TfrmProcesso_Padrao)
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    uspConsulta: TADOStoredProc;
    DataSource1: TDataSource;
    uspConsultanCdCC: TIntegerField;
    uspConsultaiNivel: TIntegerField;
    uspConsultacNmCC: TStringField;
    uspConsultanValCC: TBCDField;
    uspConsultanCdOrdem: TIntegerField;
    uspConsultacCdCC: TStringField;
    uspConsultanValPercNivelAnt: TBCDField;
    uspConsultanValPercTotalGer: TBCDField;
    uspConsultanCdCC1: TIntegerField;
    uspConsultanCdCC2: TIntegerField;
    uspConsultanCdEmpresa: TIntegerField;
    uspConsultacNmEmpresa: TStringField;
    uspConsultacNmCC2: TStringField;
    uspConsultacNmCC3: TStringField;
    cxGrid1DBTableView1nCdCC: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCC: TcxGridDBColumn;
    cxGrid1DBTableView1nValCC: TcxGridDBColumn;
    cxGrid1DBTableView1nValPercNivelAnt: TcxGridDBColumn;
    cxGrid1DBTableView1nValPercTotalGer: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCC2: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCC3: TcxGridDBColumn;
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaPagamentoCentroCustoSint: TfrmConsultaPagamentoCentroCustoSint;

implementation

uses fConsultaPagamentoCentroCustoAnalit,fConsultaPagamentoCentroCusto, fMenu,
  rPagamentoCentroCusto_view;

{$R *.dfm}

procedure TfrmConsultaPagamentoCentroCustoSint.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmConsultaPagamentoCentroCustoAnalit ;
begin
  inherited;

  if (uspConsultanValCC.Value > 0) then
  begin

     objForm := TfrmConsultaPagamentoCentroCustoAnalit.Create(nil) ;

     objForm.uspConsulta.Close ;
     objForm.uspConsulta.Parameters.ParamByName('@nCdEmpresa').Value             :=  uspConsulta.Parameters.ParamByName('@nCdEmpresa').Value;
     objForm.uspConsulta.Parameters.ParamByName('@nCdCentroCustoNivel1').Value   :=  uspConsultanCdCC1.Value;
     objForm.uspConsulta.Parameters.ParamByName('@nCdCentroCustoNivel2').Value   :=  uspConsultanCdCC2.Value;
     objForm.uspConsulta.Parameters.ParamByName('@nCdCentroCustoNivel3').Value   :=  uspConsultanCdCC.Value;
     objForm.uspConsulta.Parameters.ParamByName('@nCdTerceiro').Value            :=  uspConsulta.Parameters.ParamByName('@nCdTerceiro').Value ;
     objForm.uspConsulta.Parameters.ParamByName('@dDtInicial').Value             :=  uspConsulta.Parameters.ParamByName('@dDtInicial').Value ;
     objForm.uspConsulta.Parameters.ParamByName('@dDtFinal').Value               :=  uspConsulta.Parameters.ParamByName('@dDtFinal').Value ;
     objForm.uspConsulta.Parameters.ParamByName('@nCdUsuario').Value             :=  frmMenu.nCdUsuarioLogado;

     if (frmMenu.LeParametro('VAREJO') = 'N') then
        objForm.uspConsulta.Parameters.ParamByName('@nCdLoja').Value       := 0
     else objForm.uspConsulta.Parameters.ParamByName('@nCdLoja').Value     := uspConsulta.Parameters.ParamByName('@nCdLoja').Value ;

     objForm.uspConsulta.Open ;

     showForm ( objForm , TRUE ) ;

  end;

end;

end.
