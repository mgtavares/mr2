unit rProvisaoPagamentoBaixada_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, ExtCtrls, jpeg, DB, ADODB;

type
  TrptProvisaoPagamentoBaixada_View = class(TForm)
    QuickRep1: TQuickRep;
    QRGroup1: TQRGroup;
    QRBand2: TQRBand;
    QRBand3: TQRBand;
    QRBand4: TQRBand;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRShape1: TQRShape;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape3: TQRShape;
    QRLabel1: TQRLabel;
    QRLabel4: TQRLabel;
    QRLabel5: TQRLabel;
    QRLabel6: TQRLabel;
    QRLabel7: TQRLabel;
    QRLabel8: TQRLabel;
    QRLabel9: TQRLabel;
    QRLabel10: TQRLabel;
    QRLabel11: TQRLabel;
    QRLabel12: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel14: TQRLabel;
    QRLabel15: TQRLabel;
    QRLabel17: TQRLabel;
    QRLabel18: TQRLabel;
    QRLabel19: TQRLabel;
    QRBand5: TQRBand;
    QRImage1: TQRImage;
    QRLabel20: TQRLabel;
    QRLabel36: TQRLabel;
    QRShape5: TQRShape;
    QRShape4: TQRShape;
    uspRelatorio: TADOStoredProc;
    uspRelatorioiNrCheque: TIntegerField;
    uspRelatorionValMov: TBCDField;
    uspRelatorionCdTitulo: TIntegerField;
    uspRelatorionCdTerceiro: TIntegerField;
    uspRelatorionCdLojaTit: TIntegerField;
    uspRelatorioiParcela: TIntegerField;
    uspRelatoriocNrTit: TStringField;
    uspRelatorionCdEspTit: TIntegerField;
    uspRelatoriodDtVenc: TDateTimeField;
    uspRelatorionCdProvisaoTit: TAutoIncField;
    uspRelatorionCdContaBancaria: TIntegerField;
    uspRelatorionCdFormaPagto: TIntegerField;
    uspRelatoriodDtBaixa: TDateTimeField;
    uspRelatorionValProvisao: TBCDField;
    uspRelatorionCdBanco: TIntegerField;
    uspRelatoriocAgencia: TIntegerField;
    uspRelatorionCdConta: TStringField;
    uspRelatoriocNmTitular: TStringField;
    uspRelatoriocNmFormaPagto: TStringField;
    uspRelatoriocNmEspTit: TStringField;
    uspRelatoriocNmTerceiro: TStringField;
    DataSource1: TDataSource;
    QRDBText1: TQRDBText;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRShape2: TQRShape;
    QRDBText18: TQRDBText;
    uspRelatorionCdEmpresa: TIntegerField;
    uspRelatoriocNrNf: TStringField;
    uspRelatoriocontaBancaria: TStringField;
    QRLabel21: TQRLabel;
    QRDBText16: TQRDBText;
    uspRelatoriodDtPagto: TDateTimeField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptProvisaoPagamentoBaixada_View: TrptProvisaoPagamentoBaixada_View;

implementation

{$R *.dfm}

end.
