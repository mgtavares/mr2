unit fRazaoAuxiliarFornecedores_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ADODB, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmRazaoAuxiliarFornecedores_View = class(TfrmRelatorio_Padrao)
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1cNmTerceiro: TcxGridDBColumn;
    cxGrid1DBTableView1dDtMovto: TcxGridDBColumn;
    cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn;
    cxGrid1DBTableView1cNrNF: TcxGridDBColumn;
    cxGrid1DBTableView1cNrTit: TcxGridDBColumn;
    cxGrid1DBTableView1nCdParc: TcxGridDBColumn;
    cxGrid1DBTableView1cNmOperacao: TcxGridDBColumn;
    cxGrid1DBTableView1nValCredito: TcxGridDBColumn;
    cxGrid1DBTableView1nValDebito: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldo: TcxGridDBColumn;
    cxGrid1DBTableView1cHistorico: TcxGridDBColumn;
    cxGrid1DBTableView1cDocumento: TcxGridDBColumn;
    cxGrid1DBTableView1cNmUsuario: TcxGridDBColumn;
    cxGrid1DBTableView1cContaBancaria: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    uspRelatorio: TADOStoredProc;
    uspRelatorionCdTerceiro: TIntegerField;
    uspRelatoriocNmTerceiro: TStringField;
    uspRelatorionCdTitulo: TIntegerField;
    uspRelatoriodDtMovto: TDateTimeField;
    uspRelatoriocNrNF: TStringField;
    uspRelatorionCdParc: TIntegerField;
    uspRelatoriocNrTit: TStringField;
    uspRelatoriocNmOperacao: TStringField;
    uspRelatorionValCredito: TBCDField;
    uspRelatorionValDebito: TBCDField;
    uspRelatorionSaldo: TBCDField;
    uspRelatoriocDocumento: TStringField;
    uspRelatoriocNmUsuario: TStringField;
    uspRelatoriocContaBancaria: TStringField;
    uspRelatoriocNmEspTit: TStringField;
    uspRelatoriocFlgExibeRazao: TIntegerField;
    uspRelatoriocHistorico: TStringField;
    DataSource1: TDataSource;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmRazaoAuxiliarFornecedores_View: TfrmRazaoAuxiliarFornecedores_View;

implementation

{$R *.dfm}

end.
