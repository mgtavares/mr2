unit fMRP;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  StdCtrls, Mask, DB, ADODB, DBCtrls;

type
  TfrmMRP = class(TfrmProcesso_Padrao)
    edtVendasAte: TMaskEdit;
    Label3: TLabel;
    Label5: TLabel;
    edtGrupoProduto: TMaskEdit;
    Label1: TLabel;
    edtGrupoMRP: TMaskEdit;
    edtProduto: TMaskEdit;
    Label2: TLabel;
    edtRecebAte: TMaskEdit;
    Label4: TLabel;
    chkEstEmpenhado: TCheckBox;
    chkEstTerceiro: TCheckBox;
    chkAgregarReceb: TCheckBox;
    chkItemZero: TCheckBox;
    qryGrupoMRP: TADOQuery;
    qryGrupoMRPnCdGrupoMRP: TIntegerField;
    qryGrupoMRPcNmGrupoMRP: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    qryGrupoProduto: TADOQuery;
    qryGrupoProdutonCdGrupoProduto: TIntegerField;
    qryGrupoProdutocNmGrupoProduto: TStringField;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    qryProduto: TADOQuery;
    qryProdutonCdProduto: TIntegerField;
    qryProdutocNmProduto: TStringField;
    DBEdit3: TDBEdit;
    DataSource3: TDataSource;
    chkLeadCumulativo: TCheckBox;
    procedure edtGrupoMRPExit(Sender: TObject);
    procedure edtGrupoProdutoExit(Sender: TObject);
    procedure edtProdutoExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure edtGrupoMRPKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtProdutoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMRP: TfrmMRP;

implementation

uses fMRP_Sugestao, fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmMRP.edtGrupoMRPExit(Sender: TObject);
begin
  inherited;

  qryGrupoMRP.Close ;
  qryGrupoMRP.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryGrupoMRP, edtGrupoMRP.Text) ;
  
end;

procedure TfrmMRP.edtGrupoProdutoExit(Sender: TObject);
begin
  inherited;

  qryGrupoProduto.Close;
  PosicionaQuery(qryGrupoProduto, edtGrupoProduto.Text) ;
  
end;

procedure TfrmMRP.edtProdutoExit(Sender: TObject);
begin
  inherited;

  qryProduto.Close ;
  PosicionaQuery(qryProduto, edtProduto.Text) ;

end;

procedure TfrmMRP.ToolButton1Click(Sender: TObject);
var
   objForm : TfrmMRP_Sugestao ;
begin
  {inherited;}

  objForm := TfrmMRP_Sugestao.Create(nil) ;

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione um grupo de MRP.') ;
      edtGrupoMRP.SetFocus ;
      exit ;
  end ;

  if (trim(edtRecebAte.Text) = '/  /') then
  begin
      case MessageDlg('Nenhuma previs�o de recebimento foi informada. ' +#13#13 + 'O MRP ir� calcular as necessidades de ressuprimento sem utilizar a previs�o de recebimentos na f�rmula de c�lculo. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo: begin
              edtRecebAte.SetFocus ;
              exit ;
          end ;
      end ;
  end ;

  if (trim(edtRecebAte.Text) <> '/  /') and (frmMenu.ConvData(edtRecebAte.Text) < Date) then
  begin
      MensagemAlerta('A previs�o de recebimentos n�o pode ser menor que hoje.') ;
      edtRecebAte.SetFocus ;
      exit ;
  end ;

  objForm.qryPreparaTemp.ExecSQL;
  
  objForm.SP_CALCULA_MRP.Close ;
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva;
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@nCdGrupoMRP').Value      := frmMenu.ConvInteiro(edtGrupoMRP.Text) ;
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@nCdGrupoProduto').Value  := frmMenu.ConvInteiro(edtGrupoProduto.Text);
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@nCdProduto').Value       := frmMenu.ConvInteiro(edtProduto.Text) ;
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@cFlgEstTerceiro').Value  := 0;
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@cFlgEstEmpenhado').Value := 0;
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@cDtPrevVenda').Value     := frmMenu.ConvData(edtVendasAte.Text);
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@cDtPrevReceb').Value     := frmMenu.ConvData(edtRecebAte.Text);
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@cFlgEstTerceiro').Value  := 0;
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@cFlgEstEmpenhado').Value := 0;
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@cFlgItemZero').Value     := 0;
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@cFlgAgregaReceb').Value  := 0;
  objForm.SP_CALCULA_MRP.Parameters.ParamByName('@nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;

  if (chkEstTerceiro.Checked) then
      objForm.SP_CALCULA_MRP.Parameters.ParamByName('@cFlgEstTerceiro').Value  := 1;

  if (chkEstEmpenhado.Checked) then
      objForm.SP_CALCULA_MRP.Parameters.ParamByName('@cFlgEstEmpenhado').Value := 1;

  if (chkItemZero.Checked) then
      objForm.SP_CALCULA_MRP.Parameters.ParamByName('@cFlgItemZero').Value     := 1;

  if (chkAgregarReceb.Checked) then
      objForm.SP_CALCULA_MRP.Parameters.ParamByName('@cFlgAgregaReceb').Value  := 1;

  {if (chkLeadCumulativo.Checked) then
      objForm.SP_CALCULA_MRP.Parameters.ParamByName('@iLeadCumulativo').Value  := 1;}

  objForm.SP_CALCULA_MRP.Open ;

  if objForm.SP_CALCULA_MRP.Eof then
  begin
      MensagemAlerta('Nenhuma sugest�o de compra.') ;
      exit ;
  end ;

  //objForm.cxGrid1DBTableView1.ViewData.Expand(True) ;

  objForm.Parent := Self ;
  objForm.ToolBar1.Enabled := True ;
  ToolBar1.Enabled := False ;
  objForm.Show;

  Self.Activate;
  edtGrupoMRP.SetFocus ;

end;

procedure TfrmMRP.edtGrupoMRPKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(175);

        If (nPK > 0) then
        begin
            edtGrupoMRP.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmMRP.edtGrupoProdutoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(69);

        If (nPK > 0) then
        begin
            edtGrupoProduto.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmMRP.edtProdutoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(176);

        If (nPK > 0) then
        begin
            edtProduto.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

initialization
    RegisterClass(TfrmMRP) ;
    
end.
