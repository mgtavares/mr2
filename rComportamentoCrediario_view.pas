unit rComportamentoCrediario_view;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptComportamentoCrediario_view = class(TForm)
    QuickRep1: TQuickRep;
    SPREL_COMPORTAMENTO_CREDIARIO: TADOStoredProc;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRBand5: TQRBand;
    QRLabel16: TQRLabel;
    lblFiltro1: TQRLabel;
    QRShape4: TQRShape;
    QRShape1: TQRShape;
    QRLabel7: TQRLabel;
    QRShape2: TQRShape;
    QRLabel2: TQRLabel;
    QRLabel13: TQRLabel;
    QRLabel1: TQRLabel;
    QRLabel14: TQRLabel;
    QRDBText2: TQRDBText;
    QRDBText3: TQRDBText;
    QRDBText6: TQRDBText;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotal: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloTotal: TIntegerField;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotalAberto: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloAberto: TIntegerField;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotalPago: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloPago: TIntegerField;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotalPagoAdiant: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloPagoAdiant: TIntegerField;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotalPago10D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloPago10D: TIntegerField;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotalPago20D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloPago20D: TIntegerField;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotalPago30D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloPago30D: TIntegerField;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotalPago60D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloPago60D: TIntegerField;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotalPago90D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloPago90D: TIntegerField;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotalPago120D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloPago120D: TIntegerField;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotalPago150D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloPago150D: TIntegerField;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotalPago180D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloPago180D: TIntegerField;
    SPREL_COMPORTAMENTO_CREDIARIOnValTotalPagoMais180D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOiQtdeTituloPagoMais180D: TIntegerField;
    QRLabel4: TQRLabel;
    SPREL_COMPORTAMENTO_CREDIARIOnPercTotal: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnPercAberto: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnValJuroPago: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnPercPago: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnValJuroPagoAdiant: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnPercPagoAdiant: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnValJuroPago10D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnPercPago10D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnValJuroPago20D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnPercPago20D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnValJuroPago30D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnPercPago30D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnValJuroPago60D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnPercPago60D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnValJuroPago90D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnPercPago90D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnValJuroPago120D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnPercPago120D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnValJuroPago150D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnPercPago150D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnValJuroPago180D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnPercPago180D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnValJuroPagoMais180D: TFloatField;
    SPREL_COMPORTAMENTO_CREDIARIOnPercPagoMais180D: TFloatField;
    QRDBText1: TQRDBText;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText7: TQRDBText;
    QRDBText8: TQRDBText;
    QRDBText9: TQRDBText;
    QRLabel8: TQRLabel;
    QRDBText10: TQRDBText;
    QRDBText11: TQRDBText;
    QRDBText12: TQRDBText;
    QRLabel9: TQRLabel;
    QRDBText13: TQRDBText;
    QRDBText14: TQRDBText;
    QRDBText15: TQRDBText;
    QRDBText16: TQRDBText;
    QRDBText17: TQRDBText;
    QRLabel10: TQRLabel;
    QRDBText18: TQRDBText;
    QRDBText19: TQRDBText;
    QRDBText20: TQRDBText;
    QRDBText21: TQRDBText;
    QRLabel11: TQRLabel;
    QRDBText22: TQRDBText;
    QRDBText23: TQRDBText;
    QRDBText24: TQRDBText;
    QRDBText25: TQRDBText;
    QRLabel12: TQRLabel;
    QRDBText26: TQRDBText;
    QRDBText27: TQRDBText;
    QRDBText28: TQRDBText;
    QRDBText29: TQRDBText;
    QRLabel17: TQRLabel;
    QRDBText30: TQRDBText;
    QRDBText31: TQRDBText;
    QRDBText32: TQRDBText;
    QRDBText33: TQRDBText;
    QRLabel18: TQRLabel;
    QRDBText34: TQRDBText;
    QRDBText35: TQRDBText;
    QRDBText36: TQRDBText;
    QRDBText37: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText38: TQRDBText;
    QRDBText39: TQRDBText;
    QRDBText40: TQRDBText;
    QRDBText41: TQRDBText;
    QRLabel20: TQRLabel;
    QRDBText42: TQRDBText;
    QRDBText43: TQRDBText;
    QRDBText44: TQRDBText;
    QRDBText45: TQRDBText;
    QRLabel21: TQRLabel;
    QRDBText46: TQRDBText;
    QRDBText47: TQRDBText;
    QRDBText48: TQRDBText;
    QRDBText49: TQRDBText;
    QRLabel22: TQRLabel;
    QRDBText50: TQRDBText;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptComportamentoCrediario_view: TrptComportamentoCrediario_view;

implementation

{$R *.dfm}

end.
