unit fIdentificaCredito;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ADODB, cxGridLevel, cxClasses, cxControls,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid;

type
  TfrmIdentificaCredito = class(TfrmProcesso_Padrao)
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    qryLanctos: TADOQuery;
    qryLanctosnCdLanctoFin: TAutoIncField;
    qryLanctosnCdBanco: TIntegerField;
    qryLanctoscAgencia: TIntegerField;
    qryLanctosnCdConta: TStringField;
    qryLanctosdDtExtrato: TDateTimeField;
    qryLanctosnValLancto: TBCDField;
    dsLanctos: TDataSource;
    cxGrid1DBTableView1nCdLanctoFin: TcxGridDBColumn;
    cxGrid1DBTableView1nCdBanco: TcxGridDBColumn;
    cxGrid1DBTableView1cAgencia: TcxGridDBColumn;
    cxGrid1DBTableView1nCdConta: TcxGridDBColumn;
    cxGrid1DBTableView1dDtExtrato: TcxGridDBColumn;
    cxGrid1DBTableView1nValLancto: TcxGridDBColumn;
    qryAux: TADOQuery;
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIdentificaCredito: TfrmIdentificaCredito;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmIdentificaCredito.FormShow(Sender: TObject);
begin
  inherited;

  qryLanctos.Close;
  PosicionaQuery(qryLanctos, IntToStr(frmMenu.nCdEmpresaAtiva)) ;
end;

procedure TfrmIdentificaCredito.ToolButton1Click(Sender: TObject);
begin
  inherited;

  qryLanctos.Close ;
  qryLanctos.Open ;

end;

procedure TfrmIdentificaCredito.cxGrid1DBTableView1DblClick(
  Sender: TObject);
var
    nPK : integer ;
begin
  inherited;

  if (qryLanctos.Active) then
  begin

      if (qryLanctosnCdLanctoFin.Value > 0) then
      begin

          case MessageDlg('Deseja identificar o terceiro respons�vel por este cr�dito ?',mtConfirmation,[mbYes,mbNo],0) of
              mrNo:exit ;
          end ;

          nPK := frmLookup_Padrao.ExecutaConsulta(17);

          If (nPK > 0) then
          begin

              case MessageDlg('Confirma a identifica��o de terceiro para o cr�dito selecionado ?'+#13#13+'ESTE PROCESSO N�O PODE SER ESTORNADO!',mtConfirmation,[mbYes,mbNo],0) of
                  mrNo:exit ;
              end ;

              qryAux.Close ;
              qryAux.SQL.Clear ;
              qryAux.SQL.Add('UPDATE LanctoFin Set nCdTerceiroDep = ' + IntToStr(nPK) + ' WHERE nCdLanctoFin = ' + qryLanctosnCdLanctoFin.AsString) ;

              frmMenu.Connection.BeginTrans;

              try
                  qryAux.ExecSQL;
              except
                  frmMenu.Connection.RollbackTrans;
                  MensagemErro('Erro no processamento.') ;
                  raise ;
              end ;

              frmMenu.Connection.CommitTrans;

              ShowMessage('Cr�dito identificado com sucesso!') ;

              qryLanctos.Close ;
              qryLanctos.Open ;

          end ;

      end ;

  end ;

end;

initialization
    RegisterClass(TfrmIdentificaCredito) ;

end.
