inherited frmProvisaoPagtoTituloAdiantamento: TfrmProvisaoPagtoTituloAdiantamento
  Left = 129
  Top = 164
  Width = 929
  Caption = 'T'#237'tulos de Adiantamento'
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 913
  end
  inherited ToolBar1: TToolBar
    Width = 913
    inherited ToolButton1: TToolButton
      Visible = False
    end
    inherited ToolButton3: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 913
    Height = 435
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 431
    ClientRectLeft = 4
    ClientRectRight = 909
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'T'#237'tulos de Adiantamento'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 905
        Height = 407
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGrid1DBTableView1: TcxGridDBTableView
          OnDblClick = cxGrid1DBTableView1DblClick
          DataController.DataSource = DataSource1
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnHiding = True
          OptionsCustomize.ColumnMoving = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GridLines = glVertical
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView1nCdTitulo: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTitulo'
          end
          object cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn
            DataBinding.FieldName = 'nCdEmpresa'
          end
          object cxGrid1DBTableView1nCdLojaTit: TcxGridDBColumn
            DataBinding.FieldName = 'nCdLojaTit'
          end
          object cxGrid1DBTableView1cNrNF: TcxGridDBColumn
            DataBinding.FieldName = 'cNrNF'
            Width = 76
          end
          object cxGrid1DBTableView1cNrTit: TcxGridDBColumn
            DataBinding.FieldName = 'cNrTit'
            Width = 87
          end
          object cxGrid1DBTableView1iParcela: TcxGridDBColumn
            Caption = 'Parc.'
            DataBinding.FieldName = 'iParcela'
          end
          object cxGrid1DBTableView1nCdSP: TcxGridDBColumn
            DataBinding.FieldName = 'nCdSP'
          end
          object cxGrid1DBTableView1nCdTerceiro: TcxGridDBColumn
            DataBinding.FieldName = 'nCdTerceiro'
          end
          object cxGrid1DBTableView1dDtEmissao: TcxGridDBColumn
            DataBinding.FieldName = 'dDtEmissao'
          end
          object cxGrid1DBTableView1dDtVenc: TcxGridDBColumn
            DataBinding.FieldName = 'dDtVenc'
          end
          object cxGrid1DBTableView1nValTit: TcxGridDBColumn
            DataBinding.FieldName = 'nValTit'
          end
          object cxGrid1DBTableView1nSaldoTit: TcxGridDBColumn
            DataBinding.FieldName = 'nSaldoTit'
          end
          object cxGrid1DBTableView1cSenso: TcxGridDBColumn
            DataBinding.FieldName = 'cSenso'
            Width = 69
          end
          object cxGrid1DBTableView1cNmEspTit: TcxGridDBColumn
            DataBinding.FieldName = 'cNmEspTit'
            Width = 179
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object qryTitulosAdiantamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '      ,EspTit.cNmEspTit'
      '  FROM Titulo'
      '       INNER JOIN EspTit ON EspTit.nCdEspTit = Titulo.nCdEspTit'
      ' WHERE Titulo.cFlgAdiantamento  = 1'
      '   AND Titulo.nSaldoTit         > 0'
      '   AND Titulo.nCdTerceiro       = :nPK'
      '   AND Titulo.dDtCancel        IS NULL'
      ''
      ''
      '')
    Left = 736
    Top = 144
    object qryTitulosAdiantamentonCdTitulo: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'nCdTitulo'
    end
    object qryTitulosAdiantamentonCdEmpresa: TIntegerField
      DisplayLabel = 'Empresa'
      FieldName = 'nCdEmpresa'
    end
    object qryTitulosAdiantamentonCdEspTit: TIntegerField
      DisplayLabel = 'Esp. T'#237'tulo'
      FieldName = 'nCdEspTit'
    end
    object qryTitulosAdiantamentocNrTit: TStringField
      DisplayLabel = 'Nr T'#237'tulo'
      FieldName = 'cNrTit'
      FixedChar = True
      Size = 17
    end
    object qryTitulosAdiantamentoiParcela: TIntegerField
      DisplayLabel = 'Parcela'
      FieldName = 'iParcela'
    end
    object qryTitulosAdiantamentonCdTerceiro: TIntegerField
      DisplayLabel = 'Terceiro'
      FieldName = 'nCdTerceiro'
    end
    object qryTitulosAdiantamentonCdCategFinanc: TIntegerField
      DisplayLabel = 'Cat. Financeira'
      FieldName = 'nCdCategFinanc'
    end
    object qryTitulosAdiantamentonCdMoeda: TIntegerField
      DisplayLabel = 'Moeda'
      FieldName = 'nCdMoeda'
    end
    object qryTitulosAdiantamentocSenso: TStringField
      DisplayLabel = 'Senso'
      FieldName = 'cSenso'
      FixedChar = True
      Size = 1
    end
    object qryTitulosAdiantamentodDtEmissao: TDateTimeField
      DisplayLabel = 'Dt. Emiss'#227'o'
      FieldName = 'dDtEmissao'
    end
    object qryTitulosAdiantamentodDtReceb: TDateTimeField
      DisplayLabel = 'Dt.Recebimento'
      FieldName = 'dDtReceb'
    end
    object qryTitulosAdiantamentodDtVenc: TDateTimeField
      DisplayLabel = 'Dt. Vencimento'
      FieldName = 'dDtVenc'
    end
    object qryTitulosAdiantamentodDtLiq: TDateTimeField
      DisplayLabel = 'Dt. Liquida'#231#227'o'
      FieldName = 'dDtLiq'
    end
    object qryTitulosAdiantamentodDtCad: TDateTimeField
      DisplayLabel = 'Dt. Cadastro'
      FieldName = 'dDtCad'
    end
    object qryTitulosAdiantamentodDtCancel: TDateTimeField
      FieldName = 'dDtCancel'
    end
    object qryTitulosAdiantamentonValTit: TBCDField
      DisplayLabel = 'Vr. T'#237'tulo'
      FieldName = 'nValTit'
      DisplayFormat = '###,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulosAdiantamentonValLiq: TBCDField
      FieldName = 'nValLiq'
      Precision = 12
      Size = 2
    end
    object qryTitulosAdiantamentonSaldoTit: TBCDField
      DisplayLabel = 'Saldo T'#237'tulo'
      FieldName = 'nSaldoTit'
      DisplayFormat = '###,##0.00'
      Precision = 12
      Size = 2
    end
    object qryTitulosAdiantamentonValJuro: TBCDField
      FieldName = 'nValJuro'
      Precision = 12
      Size = 2
    end
    object qryTitulosAdiantamentonValDesconto: TBCDField
      FieldName = 'nValDesconto'
      Precision = 12
      Size = 2
    end
    object qryTitulosAdiantamentocObsTit: TMemoField
      DisplayLabel = 'Observa'#231#227'o'
      FieldName = 'cObsTit'
      BlobType = ftMemo
    end
    object qryTitulosAdiantamentonCdSP: TIntegerField
      DisplayLabel = 'Nr SP'
      FieldName = 'nCdSP'
    end
    object qryTitulosAdiantamentonCdBancoPortador: TIntegerField
      FieldName = 'nCdBancoPortador'
    end
    object qryTitulosAdiantamentodDtRemPortador: TDateTimeField
      FieldName = 'dDtRemPortador'
    end
    object qryTitulosAdiantamentodDtBloqTit: TDateTimeField
      FieldName = 'dDtBloqTit'
    end
    object qryTitulosAdiantamentocObsBloqTit: TStringField
      FieldName = 'cObsBloqTit'
      Size = 50
    end
    object qryTitulosAdiantamentonCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryTitulosAdiantamentonCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryTitulosAdiantamentodDtContab: TDateTimeField
      FieldName = 'dDtContab'
    end
    object qryTitulosAdiantamentonCdDoctoFiscal: TIntegerField
      FieldName = 'nCdDoctoFiscal'
    end
    object qryTitulosAdiantamentodDtCalcJuro: TDateTimeField
      FieldName = 'dDtCalcJuro'
    end
    object qryTitulosAdiantamentonCdRecebimento: TIntegerField
      FieldName = 'nCdRecebimento'
    end
    object qryTitulosAdiantamentonCdCrediario: TIntegerField
      FieldName = 'nCdCrediario'
    end
    object qryTitulosAdiantamentonCdTransacaoCartao: TIntegerField
      FieldName = 'nCdTransacaoCartao'
    end
    object qryTitulosAdiantamentonCdLanctoFin: TIntegerField
      FieldName = 'nCdLanctoFin'
    end
    object qryTitulosAdiantamentocFlgIntegrado: TIntegerField
      FieldName = 'cFlgIntegrado'
    end
    object qryTitulosAdiantamentocFlgDocCobranca: TIntegerField
      FieldName = 'cFlgDocCobranca'
    end
    object qryTitulosAdiantamentocNrNF: TStringField
      DisplayLabel = 'Nr NF'
      FieldName = 'cNrNF'
      FixedChar = True
      Size = 15
    end
    object qryTitulosAdiantamentonCdProvisaoTit: TIntegerField
      FieldName = 'nCdProvisaoTit'
    end
    object qryTitulosAdiantamentocFlgDA: TIntegerField
      FieldName = 'cFlgDA'
    end
    object qryTitulosAdiantamentonCdContaBancariaDA: TIntegerField
      FieldName = 'nCdContaBancariaDA'
    end
    object qryTitulosAdiantamentonCdTipoLanctoDA: TIntegerField
      FieldName = 'nCdTipoLanctoDA'
    end
    object qryTitulosAdiantamentonCdLojaTit: TIntegerField
      DisplayLabel = 'Loja'
      FieldName = 'nCdLojaTit'
    end
    object qryTitulosAdiantamentocFlgInclusaoManual: TIntegerField
      FieldName = 'cFlgInclusaoManual'
    end
    object qryTitulosAdiantamentodDtVencOriginal: TDateTimeField
      FieldName = 'dDtVencOriginal'
    end
    object qryTitulosAdiantamentocFlgCartaoConciliado: TIntegerField
      FieldName = 'cFlgCartaoConciliado'
    end
    object qryTitulosAdiantamentonCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryTitulosAdiantamentonCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryTitulosAdiantamentonValTaxaOperadora: TBCDField
      FieldName = 'nValTaxaOperadora'
      Precision = 12
      Size = 2
    end
    object qryTitulosAdiantamentonCdLoteConciliacaoCartao: TIntegerField
      FieldName = 'nCdLoteConciliacaoCartao'
    end
    object qryTitulosAdiantamentodDtIntegracao: TDateTimeField
      FieldName = 'dDtIntegracao'
    end
    object qryTitulosAdiantamentonCdParcContratoEmpImobiliario: TIntegerField
      FieldName = 'nCdParcContratoEmpImobiliario'
    end
    object qryTitulosAdiantamentonValAbatimento: TBCDField
      FieldName = 'nValAbatimento'
      Precision = 12
      Size = 2
    end
    object qryTitulosAdiantamentonValMulta: TBCDField
      FieldName = 'nValMulta'
      Precision = 12
      Size = 2
    end
    object qryTitulosAdiantamentodDtNegativacao: TDateTimeField
      FieldName = 'dDtNegativacao'
    end
    object qryTitulosAdiantamentonCdUsuarioNeg: TIntegerField
      FieldName = 'nCdUsuarioNeg'
    end
    object qryTitulosAdiantamentonCdItemListaCobrancaNeg: TIntegerField
      FieldName = 'nCdItemListaCobrancaNeg'
    end
    object qryTitulosAdiantamentonCdLojaNeg: TIntegerField
      FieldName = 'nCdLojaNeg'
    end
    object qryTitulosAdiantamentodDtReabNeg: TDateTimeField
      FieldName = 'dDtReabNeg'
    end
    object qryTitulosAdiantamentocFlgCobradora: TIntegerField
      FieldName = 'cFlgCobradora'
    end
    object qryTitulosAdiantamentonCdCobradora: TIntegerField
      FieldName = 'nCdCobradora'
    end
    object qryTitulosAdiantamentodDtRemCobradora: TDateTimeField
      FieldName = 'dDtRemCobradora'
    end
    object qryTitulosAdiantamentonCdUsuarioRemCobradora: TIntegerField
      FieldName = 'nCdUsuarioRemCobradora'
    end
    object qryTitulosAdiantamentonCdItemListaCobRemCobradora: TIntegerField
      FieldName = 'nCdItemListaCobRemCobradora'
    end
    object qryTitulosAdiantamentocFlgReabManual: TIntegerField
      FieldName = 'cFlgReabManual'
    end
    object qryTitulosAdiantamentonCdUsuarioReabManual: TIntegerField
      FieldName = 'nCdUsuarioReabManual'
    end
    object qryTitulosAdiantamentonCdFormaPagtoTit: TIntegerField
      FieldName = 'nCdFormaPagtoTit'
    end
    object qryTitulosAdiantamentonPercTaxaJurosDia: TBCDField
      FieldName = 'nPercTaxaJurosDia'
      Precision = 12
    end
    object qryTitulosAdiantamentonPercTaxaMulta: TBCDField
      FieldName = 'nPercTaxaMulta'
      Precision = 12
    end
    object qryTitulosAdiantamentoiDiaCarenciaJuros: TIntegerField
      FieldName = 'iDiaCarenciaJuros'
    end
    object qryTitulosAdiantamentocFlgRenegociado: TIntegerField
      FieldName = 'cFlgRenegociado'
    end
    object qryTitulosAdiantamentocCodBarra: TStringField
      FieldName = 'cCodBarra'
      Size = 100
    end
    object qryTitulosAdiantamentonValEncargoCobradora: TBCDField
      FieldName = 'nValEncargoCobradora'
      Precision = 12
      Size = 2
    end
    object qryTitulosAdiantamentocFlgRetCobradoraManual: TIntegerField
      FieldName = 'cFlgRetCobradoraManual'
    end
    object qryTitulosAdiantamentonCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryTitulosAdiantamentodDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryTitulosAdiantamentonValIndiceCorrecao: TBCDField
      FieldName = 'nValIndiceCorrecao'
      Precision = 12
      Size = 8
    end
    object qryTitulosAdiantamentonValCorrecao: TBCDField
      FieldName = 'nValCorrecao'
      Precision = 12
      Size = 2
    end
    object qryTitulosAdiantamentocFlgNegativadoManual: TIntegerField
      FieldName = 'cFlgNegativadoManual'
    end
    object qryTitulosAdiantamentocIDExternoTit: TStringField
      FieldName = 'cIDExternoTit'
    end
    object qryTitulosAdiantamentonCdTipoAdiantamento: TIntegerField
      FieldName = 'nCdTipoAdiantamento'
    end
    object qryTitulosAdiantamentocFlgAdiantamento: TIntegerField
      FieldName = 'cFlgAdiantamento'
    end
    object qryTitulosAdiantamentonCdMTituloGerador: TIntegerField
      FieldName = 'nCdMTituloGerador'
    end
    object qryTitulosAdiantamentocNmEspTit: TStringField
      FieldName = 'cNmEspTit'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryTitulosAdiantamento
    Left = 728
    Top = 104
  end
end
