unit fClienteVarejoPessoaFisica_TitulosNeg;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, GridsEh, DBGridEh, DB, ADODB, ImgList,
  ComCtrls, ToolWin, ExtCtrls, DBGridEhGrouping, ToolCtrlsEh;

type
  TfrmClienteVarejoPessoaFisica_TitulosNeg = class(TfrmProcesso_Padrao)
    qryTitulos: TADOQuery;
    qryTitulosnCdTitulo: TIntegerField;
    qryTituloscNrTit: TStringField;
    qryTitulosiParcela: TIntegerField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTitulosnValTit: TBCDField;
    qryTitulosnSaldoTit: TBCDField;
    qryTitulosdDtLiq: TDateTimeField;
    qryTituloscNmEspTit: TStringField;
    qryTitulosnCdLojaTit: TStringField;
    dsTitulos: TDataSource;
    DBGridEh1: TDBGridEh;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmClienteVarejoPessoaFisica_TitulosNeg: TfrmClienteVarejoPessoaFisica_TitulosNeg;

implementation

{$R *.dfm}

end.
