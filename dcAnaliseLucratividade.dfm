inherited dcmAnaliseLucratividade: TdcmAnaliseLucratividade
  Left = 218
  Top = 198
  Width = 863
  Height = 356
  Caption = 'Data Analysis - An'#225'lise de Lucratividade'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 847
    Height = 289
  end
  object Label1: TLabel [1]
    Left = 65
    Top = 48
    Width = 41
    Height = 13
    Caption = 'Empresa'
  end
  object Label5: TLabel [2]
    Left = 73
    Top = 96
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Cliente'
  end
  object Label3: TLabel [3]
    Left = 37
    Top = 240
    Width = 69
    Height = 13
    Caption = 'Per'#237'odo Venda'
  end
  object Label6: TLabel [4]
    Left = 188
    Top = 240
    Width = 16
    Height = 13
    Caption = 'at'#233
  end
  object Label2: TLabel [5]
    Left = 24
    Top = 72
    Width = 82
    Height = 13
    Caption = 'Grupo Econ'#244'mico'
  end
  object Label4: TLabel [6]
    Left = 37
    Top = 144
    Width = 69
    Height = 13
    Caption = 'Departamento'
  end
  object Label7: TLabel [7]
    Left = 77
    Top = 168
    Width = 29
    Height = 13
    Caption = 'Marca'
  end
  object Label8: TLabel [8]
    Left = 81
    Top = 192
    Width = 25
    Height = 13
    Caption = 'Linha'
  end
  object Label9: TLabel [9]
    Left = 75
    Top = 216
    Width = 31
    Height = 13
    Caption = 'Classe'
  end
  object Label10: TLabel [10]
    Left = 16
    Top = 120
    Width = 90
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ramo de Atividade'
  end
  inherited ToolBar1: TToolBar
    Width = 847
    TabOrder = 10
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object DBEdit2: TDBEdit [12]
    Tag = 1
    Left = 180
    Top = 40
    Width = 60
    Height = 21
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 11
  end
  object DBEdit3: TDBEdit [13]
    Tag = 1
    Left = 243
    Top = 40
    Width = 587
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 12
  end
  object DBEdit9: TDBEdit [14]
    Tag = 1
    Left = 180
    Top = 88
    Width = 120
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = dsTerceiro
    TabOrder = 14
  end
  object DBEdit10: TDBEdit [15]
    Tag = 1
    Left = 303
    Top = 88
    Width = 527
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 15
  end
  object MaskEdit1: TMaskEdit [16]
    Left = 112
    Top = 232
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 8
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [17]
    Left = 208
    Top = 232
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 9
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [18]
    Left = 112
    Top = 40
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 0
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object MaskEdit6: TMaskEdit [19]
    Left = 112
    Top = 88
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 2
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
  end
  object DBEdit1: TDBEdit [20]
    Tag = 1
    Left = 180
    Top = 64
    Width = 650
    Height = 21
    DataField = 'cNmGrupoEconomico'
    DataSource = dsGrupoEconomico
    TabOrder = 16
  end
  object MaskEdit4: TMaskEdit [21]
    Left = 112
    Top = 64
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object MaskEdit5: TMaskEdit [22]
    Left = 112
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 4
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object DBEdit4: TDBEdit [23]
    Tag = 1
    Left = 180
    Top = 136
    Width = 650
    Height = 21
    DataField = 'cNmDepartamento'
    DataSource = dsDepartamento
    TabOrder = 13
  end
  object MaskEdit7: TMaskEdit [24]
    Left = 112
    Top = 160
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 5
    Text = '      '
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object DBEdit5: TDBEdit [25]
    Tag = 1
    Left = 180
    Top = 160
    Width = 650
    Height = 21
    DataField = 'cNmMarca'
    DataSource = dsMarca
    TabOrder = 17
  end
  object MaskEdit8: TMaskEdit [26]
    Left = 112
    Top = 184
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = MaskEdit8Exit
    OnKeyDown = MaskEdit8KeyDown
  end
  object DBEdit6: TDBEdit [27]
    Tag = 1
    Left = 180
    Top = 184
    Width = 650
    Height = 21
    DataField = 'cNmLinha'
    DataSource = dsLinha
    TabOrder = 18
  end
  object MaskEdit9: TMaskEdit [28]
    Left = 112
    Top = 208
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 7
    Text = '      '
    OnExit = MaskEdit9Exit
    OnKeyDown = MaskEdit9KeyDown
  end
  object DBEdit7: TDBEdit [29]
    Tag = 1
    Left = 180
    Top = 208
    Width = 650
    Height = 21
    DataField = 'cNmClasseProduto'
    DataSource = dsClasseProduto
    TabOrder = 19
  end
  object MaskEdit10: TMaskEdit [30]
    Left = 112
    Top = 112
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 3
    Text = '      '
    OnExit = MaskEdit10Exit
    OnKeyDown = MaskEdit10KeyDown
  end
  object DBEdit8: TDBEdit [31]
    Tag = 1
    Left = 180
    Top = 112
    Width = 650
    Height = 21
    DataField = 'cNmRamoAtividade'
    DataSource = dsRamoAtividade
    TabOrder = 20
  end
  inherited ImageList1: TImageList
    Left = 744
    Top = 248
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 488
    Top = 248
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nCdTerceiro')
    Left = 552
    Top = 248
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 488
    Top = 280
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 552
    Top = 280
  end
  object qryGrupoEconomico: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoEconomico'
      'WHERE nCdGrupoEconomico = :nPK')
    Left = 520
    Top = 248
    object qryGrupoEconomiconCdGrupoEconomico: TIntegerField
      FieldName = 'nCdGrupoEconomico'
    end
    object qryGrupoEconomicocNmGrupoEconomico: TStringField
      FieldName = 'cNmGrupoEconomico'
      Size = 50
    end
  end
  object dsGrupoEconomico: TDataSource
    DataSet = qryGrupoEconomico
    Left = 520
    Top = 280
  end
  object qryDepartamento: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdDepartamento, cNmDepartamento'
      'FROM Departamento'
      'WHERE nCdDepartamento = :nPK')
    Left = 616
    Top = 248
    object qryDepartamentonCdDepartamento: TIntegerField
      FieldName = 'nCdDepartamento'
    end
    object qryDepartamentocNmDepartamento: TStringField
      FieldName = 'cNmDepartamento'
      Size = 50
    end
  end
  object qryMarca: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdMarca, cNmMarca'
      'FROM Marca'
      'WHERE nCdMarca = :nPK')
    Left = 648
    Top = 248
    object qryMarcanCdMarca: TIntegerField
      FieldName = 'nCdMarca'
    end
    object qryMarcacNmMarca: TStringField
      FieldName = 'cNmMarca'
      Size = 50
    end
  end
  object qryLinha: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'npk'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLinha, cNmLinha'
      'FROM Linha'
      'WHERE nCdLinha = :npk')
    Left = 680
    Top = 248
    object qryLinhanCdLinha: TAutoIncField
      FieldName = 'nCdLinha'
      ReadOnly = True
    end
    object qryLinhacNmLinha: TStringField
      FieldName = 'cNmLinha'
      Size = 50
    end
  end
  object qryClasseProduto: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT * '
      'FROM ClasseProduto'
      'WHERE nCdClasseProduto = :nPK')
    Left = 712
    Top = 248
    object qryClasseProdutonCdClasseProduto: TAutoIncField
      FieldName = 'nCdClasseProduto'
      ReadOnly = True
    end
    object qryClasseProdutocNmClasseProduto: TStringField
      FieldName = 'cNmClasseProduto'
      Size = 50
    end
  end
  object dsDepartamento: TDataSource
    DataSet = qryDepartamento
    Left = 616
    Top = 280
  end
  object dsMarca: TDataSource
    DataSet = qryMarca
    Left = 648
    Top = 280
  end
  object dsLinha: TDataSource
    DataSet = qryLinha
    Left = 680
    Top = 280
  end
  object dsClasseProduto: TDataSource
    DataSet = qryClasseProduto
    Left = 712
    Top = 280
  end
  object qryRamoAtividade: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM RamoAtividade'
      'WHERE nCdRamoAtividade = :nPK')
    Left = 584
    Top = 248
    object qryRamoAtividadenCdRamoAtividade: TIntegerField
      FieldName = 'nCdRamoAtividade'
    end
    object qryRamoAtividadecNmRamoAtividade: TStringField
      FieldName = 'cNmRamoAtividade'
      Size = 50
    end
  end
  object dsRamoAtividade: TDataSource
    DataSet = qryRamoAtividade
    Left = 584
    Top = 280
  end
end
