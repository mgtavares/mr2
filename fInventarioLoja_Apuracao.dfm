inherited frmInventarioLoja_Apuracao: TfrmInventarioLoja_Apuracao
  Left = 112
  Top = 133
  Caption = 'Inventario Loja - Apura'#231#227'o'
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    ButtonWidth = 93
    inherited ToolButton1: TToolButton
      Caption = 'Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 93
    end
    inherited ToolButton2: TToolButton
      Left = 101
    end
  end
  object DBGridEh1: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    DataGrouping.GroupLevels = <>
    DataSource = dsInventario
    Flat = False
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    RowDetailPanel.Color = clBtnFace
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    OnDblClick = DBGridEh1DblClick
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdInventario'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nCdLoja'
        Footers = <>
        Width = 41
      end
      item
        EditButtons = <>
        FieldName = 'cNmLocalEstoque'
        Footers = <>
        Width = 201
      end
      item
        EditButtons = <>
        FieldName = 'cNmInventario'
        Footers = <>
        Width = 229
      end
      item
        EditButtons = <>
        FieldName = 'dDtAbertura'
        Footers = <>
        Tag = 2
      end
      item
        EditButtons = <>
        FieldName = 'cNmUsuario'
        Footers = <>
        Width = 173
      end
      item
        EditButtons = <>
        FieldName = 'dDtContagemEncerrada'
        Footers = <>
        Tag = 2
      end>
    object RowDetailData: TRowDetailPanelControlEh
    end
  end
  object qryInventario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdEmpresa'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdLoja'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT Inventario.nCdInventario'
      '      ,dbo.fn_ZeroEsquerda(Inventario.nCdLoja,3) as nCdLoja'
      '      ,Inventario.dDtAbertura'
      '      ,Inventario.cNmInventario'
      
        '      ,dbo.fn_ZeroEsquerda(Inventario.nCdLocalEstoque,3) + '#39'-'#39' +' +
        ' LocalEstoque.cNmLocalEstoque as cNmLocalEstoque'
      '      ,Usuario.cNmUsuario'
      '      ,Inventario.dDtContagemEncerrada'
      '      ,Inventario.cFlgApuradoDivergencia'
      '  FROM Inventario'
      
        '       INNER JOIN LocalEstoque ON LocalEstoque.nCdLocalEstoque =' +
        ' Inventario.nCdLocalEstoque'
      
        '       LEFT  JOIN Usuario      ON Usuario.nCdUsuario           =' +
        ' Inventario.nCdUsuarioAbertura'
      ' WHERE Inventario.nCdEmpresa = :nCdEmpresa'
      '   AND Inventario.nCdLoja    = :nCdLoja'
      '   AND Inventario.dDtFech   IS NULL'
      '   AND Inventario.dDtCancel IS NULL'
      ' ORDER BY Inventario.nCdInventario')
    Left = 272
    Top = 224
    object qryInventarionCdInventario: TIntegerField
      DisplayLabel = 'Invent'#225'rios Abertos|C'#243'd'
      FieldName = 'nCdInventario'
    end
    object qryInventarionCdLoja: TStringField
      DisplayLabel = 'Invent'#225'rios Abertos|Loja'
      FieldName = 'nCdLoja'
      ReadOnly = True
      Size = 15
    end
    object qryInventariodDtAbertura: TDateTimeField
      DisplayLabel = 'Invent'#225'rios Abertos|Dt. Abertura'
      FieldName = 'dDtAbertura'
    end
    object qryInventariocNmInventario: TStringField
      DisplayLabel = 'Invent'#225'rios Abertos|Nome Invent'#225'rio'
      FieldName = 'cNmInventario'
      Size = 50
    end
    object qryInventariocNmLocalEstoque: TStringField
      DisplayLabel = 'Invent'#225'rios Abertos|Local Estoque'
      FieldName = 'cNmLocalEstoque'
      ReadOnly = True
      Size = 66
    end
    object qryInventariocNmUsuario: TStringField
      DisplayLabel = 'Invent'#225'rios Abertos|Usu'#225'rio Abertura'
      FieldName = 'cNmUsuario'
      Size = 50
    end
    object qryInventariodDtContagemEncerrada: TDateTimeField
      DisplayLabel = 'Invent'#225'rios Abertos|Dt. Encerramento Contagem'
      FieldName = 'dDtContagemEncerrada'
    end
    object qryInventariocFlgApuradoDivergencia: TIntegerField
      FieldName = 'cFlgApuradoDivergencia'
    end
  end
  object dsInventario: TDataSource
    DataSet = qryInventario
    Left = 304
    Top = 224
  end
  object SP_APURA_DIVERGENCIA_INVENTARIO_LOJA: TADOStoredProc
    Connection = frmMenu.Connection
    CommandTimeout = 0
    ProcedureName = 'SP_APURA_DIVERGENCIA_INVENTARIO_LOJA;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@nCdInventario'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = 0
      end>
    Left = 416
    Top = 160
  end
end
