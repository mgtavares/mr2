unit fListaCobranca_Itens_GerarCarta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, StdCtrls, Mask, cxLookAndFeelPainters, cxButtons, DBCtrls, RDprint;

type
  TfrmListaCobranca_Itens_GerarCarta = class(TfrmProcesso_Padrao)
    edtModeloCarta: TMaskEdit;
    Label1: TLabel;
    qryModeloCarta: TADOQuery;
    qryModeloCartanCdModeloCarta: TIntegerField;
    qryModeloCartacNmModeloCarta: TStringField;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    cxButton1: TcxButton;
    qryItemLista: TADOQuery;
    qryTerceiro: TADOQuery;
    qryTitulos: TADOQuery;
    qryItemListanCdTerceiro: TIntegerField;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocNmTerceiro: TStringField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocRG: TStringField;
    qryTerceirocNmFantasia: TStringField;
    qryTitulosnCdTitulo: TIntegerField;
    qryTitulosiParcela: TIntegerField;
    qryTitulosdDtEmissao: TDateTimeField;
    qryTitulosdDtVenc: TDateTimeField;
    qryTitulosnValTit: TBCDField;
    RDprint1: TRDprint;
    qryModeloCartanCdEmpresa: TIntegerField;
    qryModeloCartanCdTabTipoModeloCarta: TIntegerField;
    qryModeloCartaiLinhaDestinatario: TIntegerField;
    qryModeloCartaiColunaDestinatario: TIntegerField;
    qryModeloCartacFlgNaoExibeDest: TIntegerField;
    qryModeloCartaiLinhaTextoCorpo: TIntegerField;
    qryModeloCartacTextoCorpo: TMemoField;
    qryModeloCartaiLinhaTitulo: TIntegerField;
    qryModeloCartacTextoPosTitulo: TMemoField;
    qryModeloCartacFlgAtivo: TIntegerField;
    qryModeloCartacTamanhoFonteDest: TStringField;
    qryModeloCartacTamanhoFonteCorpo: TStringField;
    qryModeloCartacTamanhoFonteRodape: TStringField;
    qryItensLista: TADOQuery;
    qryItensListanCdItemListaCobranca: TAutoIncField;
    qryEndereco: TADOQuery;
    qryEnderecocEndereco: TStringField;
    qryEnderecocBairro: TStringField;
    qryEnderecocCidade: TStringField;
    qryTerceirocCepCxPostal: TStringField;
    qryTerceirocCaixaPostal: TStringField;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    qryCidadeEmpresa: TADOQuery;
    qryCidadeEmpresacCidade: TStringField;
    CheckBox1: TCheckBox;
    qryGeraContatoCarta: TADOQuery;
    qryTituloscNrTit: TStringField;
    procedure edtModeloCartaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtModeloCartaExit(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure ImprimeCartas(nCdModeloCarta, nCdEmpresa, nCdListaCobranca: integer);
    procedure FormShow(Sender: TObject);
    procedure ImprimeCartaItemLista(nCdItemListaCobranca: integer);
    procedure GeraRegistroContato(nCdListaCobrancaAtual:integer);
    procedure RDprint1Preview(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    nCdListaCobrancaAtual : integer ;
  end;

var
  frmListaCobranca_Itens_GerarCarta: TfrmListaCobranca_Itens_GerarCarta;
  cDataHoraCidade: string ;

implementation

uses fLookup_Padrao, fMenu;

{$R *.dfm}

procedure TfrmListaCobranca_Itens_GerarCarta.edtModeloCartaKeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(198);

        If (nPK > 0) then
            edtModeloCarta.Text := intToStr(nPK) ;

    end ;

  end ;

end;

procedure TfrmListaCobranca_Itens_GerarCarta.edtModeloCartaExit(
  Sender: TObject);
begin
  inherited;

  qryModeloCarta.Close ;
  PosicionaQuery(qryModeloCarta, edtModeloCarta.Text) ;
  
end;

procedure TfrmListaCobranca_Itens_GerarCarta.cxButton1Click(
  Sender: TObject);
begin

  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Selecione o modelo da carta.') ;
      edtModeloCarta.SetFocus;
      exit ;
  end ;

  rdPrint1.OpcoesPreview.BotaoImprimir := Inativo ;

  if (CheckBox1.Checked) then
      rdPrint1.OpcoesPreview.BotaoImprimir := Ativo
  else MensagemAlerta('Somente a visualiza��o das cartas ser� permitida.' +#13#13 +'Para liberar a impress�o, selecione a op��o de gera��o de registro de contato.') ;

  ImprimeCartas(qryModeloCartanCdModeloCarta.Value, frmMenu.nCdEmpresaAtiva, nCdListaCobrancaAtual) ;

end;


procedure TfrmListaCobranca_Itens_GerarCarta.ImprimeCartas(nCdModeloCarta,
  nCdEmpresa, nCdListaCobranca: integer);
var
    iQtdeCarta : integer ;
    iAno, iMes, iDia : word ;
    cMes : string ;
begin

    qryCidadeEmpresa.Close;
    PosicionaQuery(qryCidadeEmpresa, intToStr(frmMenu.nCdEmpresaAtiva)) ;

    DecodeDate(Date,iAno,iMes,iDia) ;

    case iMes Of
        1: cMes := 'Janeiro' ;
        2: cMes := 'Fevereiro';
        3: cMes := 'Mar�o' ;
        4: cMes := 'Abril' ;
        5: cMes := 'Maio' ;
        6: cMes := 'Junho' ;
        7: cMes := 'Julho' ;
        8: cMes := 'Agosto' ;
        9: cMes := 'Setembro' ;
       10: cMes := 'Outubro' ;
       11: cMes := 'Novembro' ;
       12: cMes := 'Dezembro' ;
    end ;

    cDataHoraCidade := Trim(qryCidadeEmpresacCidade.Value) ;
    cDataHoraCidade := cDataHoraCidade + ', '+ frmMenu.ZeroEsquerda(intToStr(iDia),2) + ' de ' + cMes + ' de ' + intToStr(iAno) ;

    if (qryModeloCarta.Eof) then
    begin

        qryModeloCarta.Close;
        qryModeloCarta.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
        PosicionaQuery(qryModeloCarta, intToStr(nCdModeloCarta)) ;

    end ;

    qryItensLista.Close ;
    PosicionaQuery(qryItensLista, intToStr(nCdListaCobranca)) ;

    qryItensLista.First;

    if (qryItensLista.Eof) then
        MensagemAlerta('Nenhum cliente nesta lista de cobran�a.') ;

    iQtdeCarta := 0 ;
    while not qryItensLista.Eof do
    begin

        iQtdeCarta := iQtdeCarta + 1 ;

        if (iQtdeCarta = 1) then
            rdPrint1.Abrir;

        if (iQtdeCarta > 1) then
            rdPrint1.Novapagina;

        ImprimeCartaItemLista(qryItensListanCdItemListaCobranca.Value);
        qryItensLista.Next;
    end ;

    qryItensLista.Close ;

    if (iQtdeCarta > 0) then
        rdPrint1.Fechar;


end;

procedure TfrmListaCobranca_Itens_GerarCarta.FormShow(Sender: TObject);
begin
  inherited;

  qryModeloCarta.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva ;
  edtModeloCarta.Text := '' ;
  qryModeloCarta.Close;
  CheckBox1.Checked := False ;
  edtModeloCarta.SetFocus;
  
end;

procedure TfrmListaCobranca_Itens_GerarCarta.ImprimeCartaItemLista(
  nCdItemListaCobranca: integer);
var
    iLinha, I, iQtde : integer ;
    sLinha : string ;
begin

    qryItemLista.Close;
    PosicionaQuery(qryItemLista, intToStr(nCdItemListaCobranca)) ;

    if not (qryItemLista.Eof) then
    begin

        qryTerceiro.Close;
        PosicionaQuery(qryTerceiro, qryItemListanCdTerceiro.AsString) ;

        qryEndereco.Close;
        PosicionaQuery(qryEndereco, qryTerceironCdTerceiro.AsString) ;

        qryTitulos.Close ;
        qryTitulos.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva;
        qryTitulos.Parameters.ParamByName('nCdTerceiro').Value := qryTerceironCdTerceiro.Value;
        qryTitulos.Open;

        {-- Imprime Identifica��o Cliente --}
        if (qryModeloCartacFlgNaoExibeDest.Value = 0) then
        begin
            iLinha := qryModeloCartaiLinhaDestinatario.Value ;

            if (qryModeloCartacTamanhoFonteDest.Value = 'NORMAL') then
            begin
                {-- Imprime o nome do cliente --}
                rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryTerceirocNmTerceiro.Value,[Normal]) ;
                iLinha := iLinha + 1 ;

                {-- Imprime a caixa postal ou o endere�o --}
                if (qryTerceirocCaixaPostal.Value <> '') then
                begin
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryTerceirocCaixaPostal.Value,[Normal]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,'CEP: ' + qryTerceirocCepCxPostal.Value,[Normal]) ;
                end
                else
                begin
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocEndereco.Value,[Normal]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocBairro.Value,[Normal]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocCidade.Value,[Normal]) ;
                end ;

                iLinha := iLinha + 1 ;
            end ;

            if (qryModeloCartacTamanhoFonteDest.Value = 'EXPANDIDO') then
            begin
                {-- Imprime o nome do cliente --}
                rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryTerceirocNmTerceiro.Value,[EXPANDIDO]) ;
                iLinha := iLinha + 1 ;

                {-- Imprime a caixa postal ou o endere�o --}
                if (qryTerceirocCaixaPostal.Value <> '') then
                begin
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryTerceirocCaixaPostal.Value,[EXPANDIDO]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,'CEP: ' + qryTerceirocCepCxPostal.Value,[EXPANDIDO]) ;
                end
                else
                begin
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocEndereco.Value,[EXPANDIDO]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocBairro.Value,[EXPANDIDO]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocCidade.Value,[EXPANDIDO]) ;
                end ;

                iLinha := iLinha + 1 ;
            end ;

            if (qryModeloCartacTamanhoFonteDest.Value = 'COMP12') then
            begin
                {-- Imprime o nome do cliente --}
                rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryTerceirocNmTerceiro.Value,[COMP12]) ;
                iLinha := iLinha + 1 ;

                {-- Imprime a caixa postal ou o endere�o --}
                if (qryTerceirocCaixaPostal.Value <> '') then
                begin
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryTerceirocCaixaPostal.Value,[COMP12]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,'CEP: ' + qryTerceirocCepCxPostal.Value,[COMP12]) ;
                end
                else
                begin
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocEndereco.Value,[COMP12]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocBairro.Value,[COMP12]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocCidade.Value,[COMP12]) ;
                end ;

                iLinha := iLinha + 1 ;
            end ;

            if (qryModeloCartacTamanhoFonteDest.Value = 'COMP17') then
            begin
                {-- Imprime o nome do cliente --}
                rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryTerceirocNmTerceiro.Value,[COMP17]) ;
                iLinha := iLinha + 1 ;

                {-- Imprime a caixa postal ou o endere�o --}
                if (qryTerceirocCaixaPostal.Value <> '') then
                begin
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryTerceirocCaixaPostal.Value,[COMP17]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,'CEP: ' + qryTerceirocCepCxPostal.Value,[COMP17]) ;
                end
                else
                begin
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocEndereco.Value,[COMP17]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocBairro.Value,[COMP17]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocCidade.Value,[COMP17]) ;
                end ;

                iLinha := iLinha + 1 ;
            end ;

            if (qryModeloCartacTamanhoFonteDest.Value = 'COMP20') then
            begin
                {-- Imprime o nome do cliente --}
                rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryTerceirocNmTerceiro.Value,[COMP20]) ;
                iLinha := iLinha + 1 ;

                {-- Imprime a caixa postal ou o endere�o --}
                if (qryTerceirocCaixaPostal.Value <> '') then
                begin
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryTerceirocCaixaPostal.Value,[COMP20]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,'CEP: ' + qryTerceirocCepCxPostal.Value,[COMP20]) ;
                end
                else
                begin
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocEndereco.Value,[COMP20]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocBairro.Value,[COMP20]) ;
                    iLinha := iLinha + 1 ;
                    rdPrint1.ImpF(iLinha,qryModeloCartaiColunaDestinatario.Value,qryEnderecocCidade.Value,[COMP20]) ;
                end ;

                iLinha := iLinha + 1 ;
            end ;

        end ;

    end ;

  {-- Imprime corpo carta --}
  iLinha := qryModeloCartaiLinhaTextoCorpo.Value ;

  for i := 1 to DBMemo1.Lines.Count do
  begin

      sLinha := DBMemo1.Lines.Strings[i-1] ;
      sLinha := StringReplace(sLinha,'&&DataHojeCidade&&'     ,cDataHoraCidade                  ,[rfReplaceAll,rfIgnoreCase]) ;
      sLinha := StringReplace(sLinha,'&&NomeCliente&&'        ,qryTerceirocNmTerceiro.Value     ,[rfReplaceAll,rfIgnoreCase]) ;
      sLinha := StringReplace(sLinha,'&&CNPJCPFCliente&&'     ,qryTerceirocCNPJCPF.Value        ,[rfReplaceAll,rfIgnoreCase]) ;
      sLinha := StringReplace(sLinha,'&&RGIECliente&&'        ,qryTerceirocRG.Value             ,[rfReplaceAll,rfIgnoreCase]) ;
      sLinha := StringReplace(sLinha,'&&RazaoSocialEmpresa&&' ,frmMenu.cNmEmpresaAtiva          ,[rfReplaceAll,rfIgnoreCase]) ;
      sLinha := StringReplace(sLinha,'&&NomeFantasiaEmpresa&&',frmMenu.cNmFantasiaEmpresa       ,[rfReplaceAll,rfIgnoreCase]) ;

      if (qryModeloCartacTamanhoFonteCorpo.Value = 'NORMAL') then
          rdPrint1.ImpF(iLinha,1,sLinha,[NORMAL]) ;

      if (qryModeloCartacTamanhoFonteCorpo.Value = 'EXPANDIDO') then
          rdPrint1.ImpF(iLinha,1,sLinha,[EXPANDIDO]) ;

      if (qryModeloCartacTamanhoFonteCorpo.Value = 'COMP12') then
          rdPrint1.ImpF(iLinha,1,sLinha,[COMP12]) ;

      if (qryModeloCartacTamanhoFonteCorpo.Value = 'COMP17') then
          rdPrint1.ImpF(iLinha,1,sLinha,[COMP17]) ;

      if (qryModeloCartacTamanhoFonteCorpo.Value = 'COMP20') then
          rdPrint1.ImpF(iLinha,1,sLinha,[COMP20]) ;

      iLinha := iLinha + 1;

  end ;

  {-- Imprime os t�tulos --}
  iLinha := qryModeloCartaiLinhaTitulo.Value ;

  rdPrint1.ImpF(iLinha,6,'T�tulo',[COMP17,negrito]) ;
  rdPrint1.ImpF(iLinha,12,'Parc.',[COMP17,negrito]) ;
  rdPrint1.ImpF(iLinha,16,'Dt. Compra',[COMP17,negrito]) ;
  rdPrint1.ImpF(iLinha,23,'Dt. Vencimento',[COMP17,negrito]) ;
  rdPrint1.ImpD(iLinha,36,'Valor',[COMP17,negrito]) ;

  if (qryTitulos.RecordCount > 1) then
  begin

      rdPrint1.ImpF(iLinha,41,'T�tulo',[COMP17,negrito]) ;
      rdPrint1.ImpF(iLinha,47,'Parc.',[COMP17,negrito]) ;
      rdPrint1.ImpF(iLinha,51,'Dt. Compra',[COMP17,negrito]) ;
      rdPrint1.ImpF(iLinha,58,'Dt. Vencimento',[COMP17,negrito]) ;
      rdPrint1.ImpD(iLinha,71,'Valor',[COMP17,negrito]) ;

  end ;
  
  iLinha := iLinha + 1 ;

  qryTitulos.First;

  iQtde := 0 ;

  while not qryTitulos.Eof do
  begin
      iQtde := iQtde + 1 ;

      if ((iQtde Mod 2) <> 0) then
      begin
          rdPrint1.ImpF(iLinha,6,Trim(qryTituloscNrTit.Value),[COMP17]) ;
          rdPrint1.ImpD(iLinha,14,frmMenu.ZeroEsquerda(qryTitulosiParcela.AsString,3),[COMP17]) ;
          rdPrint1.ImpF(iLinha,16,qryTitulosdDtEmissao.AsString,[COMP17]) ;
          rdPrint1.ImpF(iLinha,24,qryTitulosdDtVenc.AsString,[COMP17]) ;
          rdPrint1.ImpVal(iLinha,31,'###,##0.00',qryTitulosnValTit.Value,[COMP17]) ;
      end
      else
      begin
          rdPrint1.ImpF(iLinha,41,Trim(qryTituloscNrTit.Value),[COMP17]) ;
          rdPrint1.ImpD(iLinha,49,frmMenu.ZeroEsquerda(qryTitulosiParcela.AsString,3),[COMP17]) ;
          rdPrint1.ImpF(iLinha,51,qryTitulosdDtEmissao.AsString,[COMP17]) ;
          rdPrint1.ImpF(iLinha,59,qryTitulosdDtVenc.AsString,[COMP17]) ;
          rdPrint1.ImpVal(iLinha,66,'###,##0.00',qryTitulosnValTit.Value,[COMP17]);
          iLinha := iLinha + 1 ;
      end ;

      qryTitulos.Next;
  end ;

  qryTitulos.Close;

  {-- Imprime o rodap� carta --}
  iLinha := iLinha + 5 ;

  for i := 1 to DBMemo2.Lines.Count do
  begin

      sLinha := DBMemo2.Lines.Strings[i-1] ;
      sLinha := StringReplace(sLinha,'&&DataHojeCidade&&'     ,cDataHoraCidade                  ,[rfReplaceAll,rfIgnoreCase]) ;
      sLinha := StringReplace(sLinha,'&&NomeCliente&&'        ,qryTerceirocNmTerceiro.Value     ,[rfReplaceAll,rfIgnoreCase]) ;
      sLinha := StringReplace(sLinha,'&&CNPJCPFCliente&&'     ,qryTerceirocCNPJCPF.Value        ,[rfReplaceAll,rfIgnoreCase]) ;
      sLinha := StringReplace(sLinha,'&&RGIECliente&&'        ,qryTerceirocRG.Value             ,[rfReplaceAll,rfIgnoreCase]) ;
      sLinha := StringReplace(sLinha,'&&RazaoSocialEmpresa&&' ,frmMenu.cNmEmpresaAtiva          ,[rfReplaceAll,rfIgnoreCase]) ;
      sLinha := StringReplace(sLinha,'&&NomeFantasiaEmpresa&&',frmMenu.cNmFantasiaEmpresa       ,[rfReplaceAll,rfIgnoreCase]) ;

      if (qryModeloCartacTamanhoFonteRodape.Value = 'NORMAL') then
          rdPrint1.ImpF(iLinha,1,sLinha,[NORMAL]) ;

      if (qryModeloCartacTamanhoFonteRodape.Value = 'EXPANDIDO') then
          rdPrint1.ImpF(iLinha,1,sLinha,[EXPANDIDO]) ;

      if (qryModeloCartacTamanhoFonteRodape.Value = 'COMP12') then
          rdPrint1.ImpF(iLinha,1,sLinha,[COMP12]) ;

      if (qryModeloCartacTamanhoFonteRodape.Value = 'COMP17') then
          rdPrint1.ImpF(iLinha,1,sLinha,[COMP17]) ;

      if (qryModeloCartacTamanhoFonteRodape.Value = 'COMP20') then
          rdPrint1.ImpF(iLinha,1,sLinha,[COMP20]) ;

      iLinha := iLinha + 1;

  end ;

  iLinha := 1 ;


end;

procedure TfrmListaCobranca_Itens_GerarCarta.GeraRegistroContato(
  nCdListaCobrancaAtual: integer);
begin

    frmMenu.Connection.BeginTrans;

    try
        qryGeraContatoCarta.Close;
        qryGeraContatoCarta.Parameters.ParamByName('nCdLoja').Value          := frmMenu.nCdLojaAtiva;
        qryGeraContatoCarta.Parameters.ParamByName('nCdListaCobranca').Value := nCdListaCobrancaAtual;
        qryGeraContatoCarta.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
        qryGeraContatoCarta.ExecSQL;
    except
        frmMenu.Connection.RollbackTrans;
        MensagemErro('Erro no processamento.') ;
        raise ;
    end ;

    frmMenu.Connection.CommitTrans;

end;

procedure TfrmListaCobranca_Itens_GerarCarta.RDprint1Preview(
  Sender: TObject);
begin
  inherited;


  if (rdPrint1.OpcoesPreview.BotaoImprimir = Ativo) then
      GeraRegistroContato(nCdListaCobrancaAtual) ;

end;

end.
