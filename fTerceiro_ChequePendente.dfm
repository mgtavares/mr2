inherited frmTerceiro_ChequePendente: TfrmTerceiro_ChequePendente
  Left = 44
  Top = 153
  Width = 1049
  Height = 549
  Caption = 'Cheques Pendentes'
  OldCreateOrder = True
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1033
    Height = 484
  end
  inherited ToolBar1: TToolBar
    Width = 1033
    inherited ToolButton1: TToolButton
      Visible = False
    end
  end
  object cxPageControl1: TcxPageControl [2]
    Left = 0
    Top = 29
    Width = 1033
    Height = 484
    ActivePage = cxTabSheet1
    Align = alClient
    LookAndFeel.NativeStyle = True
    TabOrder = 1
    ClientRectBottom = 480
    ClientRectLeft = 4
    ClientRectRight = 1029
    ClientRectTop = 24
    object cxTabSheet1: TcxTabSheet
      Caption = 'Cheque Pendente'
      ImageIndex = 0
      object cxGrid1: TcxGrid
        Left = 0
        Top = 0
        Width = 1025
        Height = 456
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGrid1DBTableView1: TcxGridDBTableView
          DataController.DataSource = DataSource1
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Kind = skSum
              Position = spFooter
            end
            item
              Kind = skSum
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '###,#00.00'
              Kind = skSum
              FieldName = 'nValCheque'
              Column = cxGrid1DBTableView1nValCheque
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnHiding = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GridLines = glVertical
          Styles.Header = frmMenu.Header
          object cxGrid1DBTableView1nCdCheque: TcxGridDBColumn
            DataBinding.FieldName = 'nCdCheque'
            Width = 45
          end
          object cxGrid1DBTableView1nCdEmpresa: TcxGridDBColumn
            DataBinding.FieldName = 'nCdEmpresa'
            Width = 92
          end
          object cxGrid1DBTableView1nCdBanco: TcxGridDBColumn
            DataBinding.FieldName = 'nCdBanco'
          end
          object cxGrid1DBTableView1cAgencia: TcxGridDBColumn
            DataBinding.FieldName = 'cAgencia'
            Width = 83
          end
          object cxGrid1DBTableView1cConta: TcxGridDBColumn
            DataBinding.FieldName = 'cConta'
          end
          object cxGrid1DBTableView1cDigito: TcxGridDBColumn
            DataBinding.FieldName = 'cDigito'
            Width = 67
          end
          object cxGrid1DBTableView1cCNPJCPF: TcxGridDBColumn
            DataBinding.FieldName = 'cCNPJCPF'
            Width = 111
          end
          object cxGrid1DBTableView1iNrCheque: TcxGridDBColumn
            DataBinding.FieldName = 'iNrCheque'
            Width = 97
          end
          object cxGrid1DBTableView1nValCheque: TcxGridDBColumn
            DataBinding.FieldName = 'nValCheque'
            Width = 100
          end
          object cxGrid1DBTableView1cNmTabStatusCheque: TcxGridDBColumn
            Caption = 'Situa'#231#227'o'
            DataBinding.FieldName = 'cNmTabStatusCheque'
            Width = 184
          end
        end
        object cxGrid1Level1: TcxGridLevel
          GridView = cxGrid1DBTableView1
        end
      end
    end
    object cxTabSheet2: TcxTabSheet
      Caption = 'Concentra'#231#227'o de Risco'
      ImageIndex = 1
      object cxGrid2: TcxGrid
        Left = 0
        Top = 288
        Width = 949
        Height = 150
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Consolas'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.NativeStyle = True
        object cxGridDBTableView1: TcxGridDBTableView
          DataController.DataSource = DataSource2
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Kind = skSum
              Position = spFooter
            end
            item
              Kind = skSum
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = '###,#00.00'
              Kind = skSum
              FieldName = 'nValCheque'
            end>
          DataController.Summary.SummaryGroups = <>
          NavigatorButtons.ConfirmDelete = False
          NavigatorButtons.First.Visible = True
          NavigatorButtons.PriorPage.Visible = True
          NavigatorButtons.Prior.Visible = True
          NavigatorButtons.Next.Visible = True
          NavigatorButtons.NextPage.Visible = True
          NavigatorButtons.Last.Visible = True
          NavigatorButtons.Insert.Visible = True
          NavigatorButtons.Delete.Visible = True
          NavigatorButtons.Edit.Visible = True
          NavigatorButtons.Post.Visible = True
          NavigatorButtons.Cancel.Visible = True
          NavigatorButtons.Refresh.Visible = True
          NavigatorButtons.SaveBookmark.Visible = True
          NavigatorButtons.GotoBookmark.Visible = True
          NavigatorButtons.Filter.Visible = True
          OptionsCustomize.ColumnHiding = True
          OptionsCustomize.ColumnMoving = False
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.Footer = True
          OptionsView.GridLines = glVertical
          OptionsView.GroupByBox = False
          Styles.Header = frmMenu.Header
          object cxGridDBTableView1cCNPJCPF: TcxGridDBColumn
            DataBinding.FieldName = 'cCNPJCPF'
            Width = 138
          end
          object cxGridDBTableView1cNmEmissor: TcxGridDBColumn
            DataBinding.FieldName = 'cNmEmissor'
          end
          object cxGridDBTableView1cOBS: TcxGridDBColumn
            DataBinding.FieldName = 'cOBS'
          end
        end
        object cxGridLevel1: TcxGridLevel
          GridView = cxGridDBTableView1
        end
      end
      object cxPageControl2: TcxPageControl
        Left = 0
        Top = 0
        Width = 1025
        Height = 456
        ActivePage = cxTabSheet3
        Align = alClient
        LookAndFeel.NativeStyle = True
        TabOrder = 1
        ClientRectBottom = 452
        ClientRectLeft = 4
        ClientRectRight = 1021
        ClientRectTop = 24
        object cxTabSheet3: TcxTabSheet
          Caption = 'Cheques'
          ImageIndex = 0
          object cxGrid4: TcxGrid
            Left = 0
            Top = 0
            Width = 1017
            Height = 428
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Consolas'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView3: TcxGridDBTableView
              DataController.DataSource = DataSource2
              DataController.Summary.DefaultGroupSummaryItems = <
                item
                  Kind = skSum
                  Position = spFooter
                end
                item
                  Kind = skSum
                end
                item
                  Format = '###,##0.00'
                  Kind = skSum
                  Position = spFooter
                  Column = cxGridDBTableView3nValCheque
                end
                item
                  Format = '###,##0.00'
                  Kind = skSum
                  Column = cxGridDBTableView3nValCheque
                end>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '###,#00.00'
                  Kind = skSum
                  FieldName = 'nValCheque'
                end
                item
                  Format = '###,##0.00'
                  Kind = skSum
                  Column = cxGridDBTableView3nValCheque
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsCustomize.ColumnHiding = True
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              OptionsView.GridLines = glVertical
              Styles.Header = frmMenu.Header
              object cxGridDBTableView3nCdCheque: TcxGridDBColumn
                DataBinding.FieldName = 'nCdCheque'
                Width = 47
              end
              object cxGridDBTableView3nCdEmpresa: TcxGridDBColumn
                DataBinding.FieldName = 'nCdEmpresa'
                Width = 77
              end
              object cxGridDBTableView3nCdBanco: TcxGridDBColumn
                DataBinding.FieldName = 'nCdBanco'
                Width = 72
              end
              object cxGridDBTableView3cAgencia: TcxGridDBColumn
                DataBinding.FieldName = 'cAgencia'
                Width = 82
              end
              object cxGridDBTableView3cConta: TcxGridDBColumn
                DataBinding.FieldName = 'cConta'
                Width = 61
              end
              object cxGridDBTableView3cDigito: TcxGridDBColumn
                DataBinding.FieldName = 'cDigito'
                Width = 64
              end
              object cxGridDBTableView3cCNPJCPF: TcxGridDBColumn
                DataBinding.FieldName = 'cCNPJCPF'
                Width = 90
              end
              object cxGridDBTableView3iNrCheque: TcxGridDBColumn
                DataBinding.FieldName = 'iNrCheque'
                Width = 93
              end
              object cxGridDBTableView3nValCheque: TcxGridDBColumn
                DataBinding.FieldName = 'nValCheque'
                Width = 119
              end
              object cxGridDBTableView3dDtDeposito: TcxGridDBColumn
                DataBinding.FieldName = 'dDtDeposito'
              end
              object cxGridDBTableView3cNmTabStatusCheque: TcxGridDBColumn
                Caption = 'Situa'#231#227'o'
                DataBinding.FieldName = 'cNmTabStatusCheque'
                Width = 180
              end
            end
            object cxGridLevel3: TcxGridLevel
              GridView = cxGridDBTableView3
            end
          end
        end
        object cxTabSheet4: TcxTabSheet
          Caption = 'Emissor'
          ImageIndex = 1
          object cxGrid3: TcxGrid
            Left = 0
            Top = 0
            Width = 1017
            Height = 428
            Align = alClient
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Consolas'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            LookAndFeel.NativeStyle = True
            object cxGridDBTableView2: TcxGridDBTableView
              DataController.DataSource = DataSource3
              DataController.Summary.DefaultGroupSummaryItems = <
                item
                  Kind = skSum
                  Position = spFooter
                end
                item
                  Kind = skSum
                end
                item
                  Format = '###,##0.00'
                  Kind = skSum
                  Position = spFooter
                  Column = cxGridDBTableView2TOTAL
                end
                item
                  Format = '###,##0.00'
                  Kind = skSum
                  Column = cxGridDBTableView2TOTAL
                end>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '###,#00.00'
                  Kind = skSum
                  FieldName = 'TOTAL'
                end
                item
                  Format = '###,##0.00'
                  Kind = skSum
                  Column = cxGridDBTableView2TOTAL
                end>
              DataController.Summary.SummaryGroups = <>
              NavigatorButtons.ConfirmDelete = False
              NavigatorButtons.First.Visible = True
              NavigatorButtons.PriorPage.Visible = True
              NavigatorButtons.Prior.Visible = True
              NavigatorButtons.Next.Visible = True
              NavigatorButtons.NextPage.Visible = True
              NavigatorButtons.Last.Visible = True
              NavigatorButtons.Insert.Visible = True
              NavigatorButtons.Delete.Visible = True
              NavigatorButtons.Edit.Visible = True
              NavigatorButtons.Post.Visible = True
              NavigatorButtons.Cancel.Visible = True
              NavigatorButtons.Refresh.Visible = True
              NavigatorButtons.SaveBookmark.Visible = True
              NavigatorButtons.GotoBookmark.Visible = True
              NavigatorButtons.Filter.Visible = True
              OptionsCustomize.ColumnHiding = True
              OptionsCustomize.ColumnMoving = False
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.Footer = True
              OptionsView.GridLines = glVertical
              OptionsView.GroupByBox = False
              Styles.Header = frmMenu.Header
              object cxGridDBTableView2cCNPJCPF: TcxGridDBColumn
                DataBinding.FieldName = 'cCNPJCPF'
                Width = 104
              end
              object cxGridDBTableView2EMISSOR: TcxGridDBColumn
                DataBinding.FieldName = 'EMISSOR'
              end
              object cxGridDBTableView2TOTAL: TcxGridDBColumn
                DataBinding.FieldName = 'TOTAL'
              end
            end
            object cxGridLevel2: TcxGridLevel
              GridView = cxGridDBTableView2
            end
          end
        end
      end
    end
  end
  inherited ImageList1: TImageList
    Left = 384
    Top = 128
  end
  object qryChequePendente: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT  nCdCheque'
      '       ,nCdEmpresa'
      '       ,nCdBanco'
      '       ,cAgencia'
      '       ,cConta'
      '       ,cDigito'
      '       ,cCNPJCPF'
      '       ,iNrCheque'
      '       ,nValCheque'
      '       ,dDtDeposito'
      '       ,TabStatusCheque.cNmTabStatusCheque'
      '  FROM Cheque (NOLOCK)'
      
        '       LEFT JOIN TabStatusCheque ON TabStatusCheque.nCdTabStatus' +
        'Cheque = Cheque.nCdTabStatusCheque'
      ' WHERE Cheque.nCdTerceiroResp = :nCdTerceiro'
      
        '   AND ((Cheque.nCdTerceiroPort IS NULL) OR (Cheque.nCdTerceiroP' +
        'ort IS NOT NULL AND Cheque.dDtDeposito >= (dbo.fn_OnlyDate(GetDa' +
        'te())+14)))'
      '   AND Cheque.nCdTabStatusCheque IN (1,2,4,5,6,8,9,10)')
    Left = 480
    Top = 140
    object qryChequePendentenCdCheque: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'nCdCheque'
    end
    object qryChequePendentenCdEmpresa: TIntegerField
      DisplayLabel = 'Empresa'
      FieldName = 'nCdEmpresa'
    end
    object qryChequePendentenCdBanco: TIntegerField
      DisplayLabel = 'Banco'
      FieldName = 'nCdBanco'
    end
    object qryChequePendentecAgencia: TStringField
      DisplayLabel = 'Agencia'
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryChequePendentecConta: TStringField
      DisplayLabel = 'Conta'
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryChequePendentecDigito: TStringField
      DisplayLabel = 'Digito'
      FieldName = 'cDigito'
      FixedChar = True
      Size = 1
    end
    object qryChequePendentecCNPJCPF: TStringField
      DisplayLabel = 'CNPJ / CPF'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequePendenteiNrCheque: TIntegerField
      DisplayLabel = 'Nr. Cheque'
      FieldName = 'iNrCheque'
    end
    object qryChequePendentenValCheque: TBCDField
      DisplayLabel = 'Vr. Cheque'
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryChequePendentedDtDeposito: TDateTimeField
      DisplayLabel = 'Dt. Deposito'
      FieldName = 'dDtDeposito'
    end
    object qryChequePendentecNmTabStatusCheque: TStringField
      FieldName = 'cNmTabStatusCheque'
      Size = 50
    end
  end
  object DataSource1: TDataSource
    DataSet = qryChequePendente
    Left = 512
    Top = 140
  end
  object qryChequeConcentracaoRisco: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT  nCdCheque'
      '       ,nCdEmpresa'
      '       ,nCdBanco'
      '       ,cAgencia'
      '       ,cConta'
      '       ,cDigito'
      '       ,cCNPJCPF'
      '       ,iNrCheque'
      '       ,nValCheque'
      '       ,dDtDeposito'
      '       ,TabStatusCheque.cNmTabStatusCheque'
      '  FROM Cheque (NOLOCK)'
      
        '       LEFT JOIN TabStatusCheque ON TabStatusCheque.nCdTabStatus' +
        'Cheque = Cheque.nCdTabStatusCheque'
      ' WHERE Cheque.nCdTerceiroResp = :nCdTerceiro'
      
        '   AND ((Cheque.nCdTerceiroPort IS NULL) OR (Cheque.nCdTerceiroP' +
        'ort IS NOT NULL AND Cheque.dDtDeposito >= (dbo.fn_OnlyDate(GetDa' +
        'te())+14)))'
      '   AND Cheque.nCdTabStatusCheque IN (1,2,4,5,6,8,9,10)'
      '   AND EXISTS(SELECT TOP 1 cCNPJCPF'
      '                FROM TerceiroCpfRisco'
      
        '               WHERE TerceiroCpfRisco.cCNPJCPF = Cheque.cCNPJCPF' +
        ')')
    Left = 480
    Top = 172
    object qryChequeConcentracaoRisconCdCheque: TIntegerField
      DisplayLabel = 'ID'
      FieldName = 'nCdCheque'
    end
    object qryChequeConcentracaoRisconCdEmpresa: TIntegerField
      DisplayLabel = 'Empresa'
      FieldName = 'nCdEmpresa'
    end
    object qryChequeConcentracaoRisconCdBanco: TIntegerField
      DisplayLabel = 'Banco'
      FieldName = 'nCdBanco'
    end
    object qryChequeConcentracaoRiscocAgencia: TStringField
      DisplayLabel = 'Ag'#234'ncia'
      FieldName = 'cAgencia'
      FixedChar = True
      Size = 4
    end
    object qryChequeConcentracaoRiscocConta: TStringField
      DisplayLabel = 'Conta'
      FieldName = 'cConta'
      FixedChar = True
      Size = 15
    end
    object qryChequeConcentracaoRiscocDigito: TStringField
      DisplayLabel = 'D'#237'gito'
      FieldName = 'cDigito'
      FixedChar = True
      Size = 1
    end
    object qryChequeConcentracaoRiscocCNPJCPF: TStringField
      DisplayLabel = 'CNPJ / CPF'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequeConcentracaoRiscoiNrCheque: TIntegerField
      DisplayLabel = 'Nr. Cheque'
      FieldName = 'iNrCheque'
    end
    object qryChequeConcentracaoRisconValCheque: TBCDField
      DisplayLabel = 'Vr. Cheque'
      FieldName = 'nValCheque'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryChequeConcentracaoRiscodDtDeposito: TDateTimeField
      DisplayLabel = 'Dt. Dep'#243'sito'
      FieldName = 'dDtDeposito'
    end
    object qryChequeConcentracaoRiscocNmTabStatusCheque: TStringField
      FieldName = 'cNmTabStatusCheque'
      Size = 50
    end
  end
  object DataSource2: TDataSource
    DataSet = qryChequeConcentracaoRisco
    Left = 512
    Top = 172
  end
  object qryChequeConcentracaoRiscoEmissor: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdTerceiro'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @nCdTerceiro INT'
      ''
      'SET  @nCdTerceiro  =  :nCdTerceiro'
      ''
      'SELECT cCNPJCPF'
      
        '       ,(SELECT TOP 1 cNmEmissor FROM TerceiroCpfRisco WHERE Ter' +
        'ceiroCpfRisco.cCNPJCPF = Cheque.cCNPJCPF AND nCdTerceiro = @nCdT' +
        'erceiro) AS EMISSOR'
      '       ,SUM(nValCheque)  AS  TOTAL'
      '  FROM Cheque '
      ' WHERE Cheque.nCdTerceiroResp = @nCdTerceiro'
      
        '   AND ((Cheque.nCdTerceiroPort IS NULL) OR (Cheque.nCdTerceiroP' +
        'ort IS NOT NULL AND Cheque.dDtDeposito >= (dbo.fn_OnlyDate(GetDa' +
        'te())+14)))'
      '   AND Cheque.nCdTabStatusCheque IN (1,2,4,5,6,8,9,10) '
      
        '   AND EXISTS(SELECT TOP 1 cCNPJCPF FROM TerceiroCpfRisco  WHERE' +
        ' TerceiroCpfRisco.cCNPJCPF = Cheque.cCNPJCPF AND nCdTerceiro = @' +
        'nCdTerceiro)'
      ' GROUP BY cCNPJCPF ')
    Left = 480
    Top = 205
    object qryChequeConcentracaoRiscoEmissorcCNPJCPF: TStringField
      DisplayLabel = 'CNPJ / CPF'
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryChequeConcentracaoRiscoEmissorEMISSOR: TStringField
      DisplayLabel = 'Emissor'
      FieldName = 'EMISSOR'
      ReadOnly = True
      Size = 50
    end
    object qryChequeConcentracaoRiscoEmissorTOTAL: TBCDField
      DisplayLabel = 'Vr. Cheques'
      FieldName = 'TOTAL'
      ReadOnly = True
      DisplayFormat = '#,##0.00'
      Precision = 32
      Size = 2
    end
  end
  object DataSource3: TDataSource
    DataSet = qryChequeConcentracaoRiscoEmissor
    Left = 512
    Top = 204
  end
end
