inherited frmOperadoraCartao: TfrmOperadoraCartao
  Left = 204
  Top = 83
  Width = 907
  Height = 541
  Caption = 'Operadora Cart'#227'o'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 899
    Height = 489
  end
  object Label1: TLabel [1]
    Left = 77
    Top = 38
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 59
    Top = 62
    Width = 56
    Height = 13
    Alignment = taRightJustify
    Caption = 'Operadora'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 18
    Top = 110
    Width = 97
    Height = 13
    Alignment = taRightJustify
    Caption = 'Dias Cr'#233'dito Conta'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 24
    Top = 134
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Tipo de Opera'#231#227'o'
    FocusControl = DBEdit4
  end
  object Label7: TLabel [5]
    Left = 149
    Top = 132
    Width = 124
    Height = 13
    Alignment = taRightJustify
    Caption = '(C - Cr'#233'dito / D - D'#233'bito)'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [6]
    Left = 8
    Top = 88
    Width = 108
    Height = 13
    Alignment = taRightJustify
    Caption = 'Grupo de Operadora'
    FocusControl = DBEdit5
  end
  object Label6: TLabel [7]
    Left = 36
    Top = 158
    Width = 79
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome Rede TEF'
    FocusControl = DBEdit7
  end
  object Label9: TLabel [8]
    Left = 83
    Top = 206
    Width = 32
    Height = 13
    Alignment = taRightJustify
    Caption = 'Status'
    FocusControl = DBEdit7
  end
  object Label8: TLabel [9]
    Left = 33
    Top = 182
    Width = 82
    Height = 13
    Alignment = taRightJustify
    Caption = 'Credenc. Cart'#227'o'
    FocusControl = DBEdit7
  end
  object Label10: TLabel [10]
    Left = 481
    Top = 182
    Width = 129
    Height = 13
    Caption = '(Obrigat'#243'rio para o CF-e)'
    FocusControl = DBEdit7
  end
  object Label11: TLabel [11]
    Left = 481
    Top = 158
    Width = 78
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo Cappta'
    FocusControl = DBEdit3
  end
  inherited ToolBar2: TToolBar
    Width = 899
    TabOrder = 11
    inherited ToolButton9: TToolButton
      Visible = False
    end
  end
  object DBEdit1: TDBEdit [13]
    Tag = 1
    Left = 120
    Top = 32
    Width = 65
    Height = 19
    DataField = 'nCdOperadoraCartao'
    DataSource = dsMaster
    TabOrder = 0
  end
  object DBEdit2: TDBEdit [14]
    Left = 120
    Top = 56
    Width = 650
    Height = 19
    DataField = 'cNmOperadoraCartao'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit3: TDBEdit [15]
    Left = 120
    Top = 104
    Width = 65
    Height = 19
    DataField = 'iFloating'
    DataSource = dsMaster
    TabOrder = 4
  end
  object DBCheckBox1: TDBCheckBox [16]
    Left = 189
    Top = 107
    Width = 161
    Height = 17
    Caption = 'Contar por dias '#250'teis.'
    DataField = 'cFlgDiaUtil'
    DataSource = dsMaster
    TabOrder = 5
    ValueChecked = '1'
    ValueUnchecked = '0'
  end
  object DBEdit4: TDBEdit [17]
    Left = 120
    Top = 128
    Width = 25
    Height = 19
    DataField = 'cFlgTipoOperacao'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit5: TDBEdit [18]
    Left = 120
    Top = 80
    Width = 65
    Height = 19
    DataField = 'nCdGrupoOperadoraCartao'
    DataSource = dsMaster
    TabOrder = 2
    OnExit = DBEdit5Exit
    OnKeyDown = DBEdit5KeyDown
  end
  object DBEdit6: TDBEdit [19]
    Tag = 1
    Left = 188
    Top = 80
    Width = 582
    Height = 19
    DataField = 'cNmGrupoOperadoraCartao'
    DataSource = dsGrupoOperadora
    TabOrder = 3
  end
  object DBEdit7: TDBEdit [20]
    Left = 120
    Top = 152
    Width = 353
    Height = 19
    DataField = 'cNmRedeTEF'
    DataSource = dsMaster
    TabOrder = 7
  end
  object cxPageControl: TcxPageControl [21]
    Left = 8
    Top = 232
    Width = 873
    Height = 257
    ActivePage = tabLojaOperadora
    LookAndFeel.NativeStyle = True
    TabOrder = 10
    ClientRectBottom = 257
    ClientRectRight = 873
    ClientRectTop = 23
    object tabLojaOperadora: TcxTabSheet
      Caption = 'Loja x Operadora Cart'#227'o'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 873
        Height = 234
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsLojaOperadora
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnDblClick = DBGridEh1DblClick
        OnEnter = DBGridEh1Enter
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdOperadoraCartao'
            Footers = <>
            Visible = False
          end
          item
            EditButtons = <>
            FieldName = 'nCdLoja'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmLoja'
            Footers = <>
            ReadOnly = True
            Width = 307
          end
          item
            EditButtons = <>
            FieldName = 'nCdContaBancaria'
            Footers = <>
            Width = 65
          end
          item
            EditButtons = <>
            FieldName = 'cNmContaBancaria'
            Footers = <>
            ReadOnly = True
            Width = 239
          end
          item
            EditButtons = <>
            FieldName = 'nTaxaOperacao'
            Footers = <>
            Width = 75
          end
          item
            EditButtons = <>
            FieldName = 'nTaxaOperacaoPrazo'
            Footers = <>
            Width = 75
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object DBEdit8: TDBEdit [22]
    Left = 120
    Top = 200
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 8
    OnKeyDown = DBEdit8KeyDown
  end
  object DBEdit9: TDBEdit [23]
    Tag = 1
    Left = 188
    Top = 200
    Width = 190
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsMaster
    TabOrder = 9
  end
  object DBEdit10: TDBEdit [24]
    Left = 120
    Top = 176
    Width = 65
    Height = 19
    DataField = 'nCdTabTipoCredencCartao'
    DataSource = dsMaster
    TabOrder = 12
    OnKeyDown = DBEdit10KeyDown
  end
  object DBEdit11: TDBEdit [25]
    Tag = 1
    Left = 188
    Top = 176
    Width = 285
    Height = 19
    DataField = 'cNmTabTipoCredencCartao'
    DataSource = dsMaster
    TabOrder = 13
  end
  object DBEdit12: TDBEdit [26]
    Left = 568
    Top = 152
    Width = 65
    Height = 19
    DataField = 'nCodigoBandeiraCartao'
    DataSource = dsMaster
    TabOrder = 14
    OnExit = DBEdit5Exit
    OnKeyDown = DBEdit5KeyDown
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM OperadoraCartao'
      'WHERE nCdOperadoraCartao = :nPK')
    Left = 416
    Top = 280
    object qryMasternCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryMastercNmOperadoraCartao: TStringField
      FieldName = 'cNmOperadoraCartao'
      Size = 50
    end
    object qryMasteriFloating: TIntegerField
      FieldName = 'iFloating'
    end
    object qryMastercFlgDiaUtil: TIntegerField
      FieldName = 'cFlgDiaUtil'
    end
    object qryMastercFlgTipoOperacao: TStringField
      FieldName = 'cFlgTipoOperacao'
      FixedChar = True
      Size = 1
    end
    object qryMasternCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryMastercNmRedeTEF: TStringField
      FieldName = 'cNmRedeTEF'
      Size = 50
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMastercNmStatus: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmStatus'
      LookupDataSet = qryStat
      LookupKeyFields = 'nCdStatus'
      LookupResultField = 'cNmStatus'
      KeyFields = 'nCdStatus'
      LookupCache = True
      Size = 50
      Lookup = True
    end
    object qryMasternCdTabTipoCredencCartao: TIntegerField
      FieldName = 'nCdTabTipoCredencCartao'
    end
    object qryMastercNmTabTipoCredencCartao: TStringField
      FieldKind = fkLookup
      FieldName = 'cNmTabTipoCredencCartao'
      LookupDataSet = qryTabTipoCredencCartao
      LookupKeyFields = 'nCdTabTipoCredencCartao'
      LookupResultField = 'cNmTabTipoCredencCartao'
      KeyFields = 'nCdTabTipoCredencCartao'
      LookupCache = True
      Size = 200
      Lookup = True
    end
    object qryMasternCodigoBandeiraCartao: TIntegerField
      FieldName = 'nCodigoBandeiraCartao'
    end
  end
  inherited dsMaster: TDataSource
    Left = 416
    Top = 312
  end
  inherited qryID: TADOQuery
    Left = 544
    Top = 280
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 576
    Top = 312
  end
  inherited qryStat: TADOQuery
    Left = 512
    Top = 280
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 576
    Top = 280
  end
  inherited ImageList1: TImageList
    Left = 608
    Top = 280
  end
  object qryLojaOperadora: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryLojaOperadoraBeforePost
    OnCalcFields = qryLojaOperadoraCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM LojaOperadoraCartao'
      ' WHERE nCdOperadoraCartao = :nPK')
    Left = 480
    Top = 280
    object qryLojaOperadoranCdOperadoraCartao: TIntegerField
      FieldName = 'nCdOperadoraCartao'
    end
    object qryLojaOperadoranCdLoja: TIntegerField
      DisplayLabel = 'Loja|C'#243'd'
      FieldName = 'nCdLoja'
    end
    object qryLojaOperadoracNmLoja: TStringField
      DisplayLabel = 'Loja|Descri'#231#227'o'
      FieldKind = fkCalculated
      FieldName = 'cNmLoja'
      Size = 50
      Calculated = True
    end
    object qryLojaOperadoranCdContaBancaria: TIntegerField
      DisplayLabel = 'Conta Banc'#225'ria Cr'#233'dito|C'#243'd'
      FieldName = 'nCdContaBancaria'
    end
    object qryLojaOperadoracNmContaBancaria: TStringField
      DisplayLabel = 'Conta Banc'#225'ria Cr'#233'dito|Dados Conta'
      FieldKind = fkCalculated
      FieldName = 'cNmContaBancaria'
      Size = 50
      Calculated = True
    end
    object qryLojaOperadoranTaxaOperacao: TBCDField
      DisplayLabel = '(%) Taxa de Opera'#231#227'o|A Vista '
      FieldName = 'nTaxaOperacao'
      Precision = 12
    end
    object qryLojaOperadoranTaxaOperacaoPrazo: TBCDField
      DisplayLabel = '(%) Taxa de Opera'#231#227'o|A Prazo'
      FieldName = 'nTaxaOperacaoPrazo'
      Precision = 12
    end
    object qryLojaOperadoranCdLojaOperadoraCartao: TAutoIncField
      FieldName = 'nCdLojaOperadoraCartao'
    end
  end
  object dsLojaOperadora: TDataSource
    DataSet = qryLojaOperadora
    Left = 480
    Top = 312
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK')
    Left = 512
    Top = 312
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryContaBancaria: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdContaBancaria'
      
        '      ,Convert(VARCHAR,nCdBanco) + '#39' - '#39' + RTRIM(cAgencia) + '#39' -' +
        ' '#39' + RTRIM(nCdConta) as cNmContaBancaria'
      '  FROM ContaBancaria'
      ' WHERE cFlgCaixa    = 0'
      '   AND cFlgCofre    = 0'
      '   AND cFlgDeposito = 1'
      '   AND nCdContaBancaria = :nPK')
    Left = 544
    Top = 312
    object qryContaBancarianCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryContaBancariacNmContaBancaria: TStringField
      FieldName = 'cNmContaBancaria'
      ReadOnly = True
      Size = 63
    end
  end
  object qryGrupoOperadora: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM GrupoOperadoraCartao'
      'WHERE nCdGrupoOperadoraCartao = :nPK')
    Left = 448
    Top = 280
    object qryGrupoOperadoranCdGrupoOperadoraCartao: TIntegerField
      FieldName = 'nCdGrupoOperadoraCartao'
    end
    object qryGrupoOperadoracNmGrupoOperadoraCartao: TStringField
      FieldName = 'cNmGrupoOperadoraCartao'
      Size = 50
    end
  end
  object dsGrupoOperadora: TDataSource
    DataSet = qryGrupoOperadora
    Left = 448
    Top = 312
  end
  object qryTabTipoCredencCartao: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT *'
      '  FROM TabTipoCredencCartao')
    Left = 612
    Top = 312
    object qryTabTipoCredencCartaonCdTabTipoCredencCartao: TIntegerField
      FieldName = 'nCdTabTipoCredencCartao'
    end
    object qryTabTipoCredencCartaocNmTabTipoCredencCartao: TStringField
      FieldName = 'cNmTabTipoCredencCartao'
      Size = 200
    end
  end
end
