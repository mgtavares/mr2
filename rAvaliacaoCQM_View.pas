unit rAvaliacaoCQM_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, QRCtrls, QuickRpt, DB, ADODB, ExtCtrls, QRExport, jpeg;

type
  TrptAvaliacaoCQM_View = class(TForm)
    QuickRep1: TQuickRep;
    QRBand1: TQRBand;
    QRBand3: TQRBand;
    QRGroup1: TQRGroup;
    QRLabel1: TQRLabel;
    QRLabel2: TQRLabel;
    QRLabel3: TQRLabel;
    QRSysData1: TQRSysData;
    QRSysData2: TQRSysData;
    lblEmpresa: TQRLabel;
    QRDBText4: TQRDBText;
    QRDBText5: TQRDBText;
    QRDBText6: TQRDBText;
    QRBand2: TQRBand;
    QRBand5: TQRBand;
    QRDBText8: TQRDBText;
    QRLabel10: TQRLabel;
    QRLabel12: TQRLabel;
    qryAvaliacao: TADOQuery;
    qryAvaliacaocNrDocto: TStringField;
    qryAvaliacaocNmTerceiro: TStringField;
    qryAvaliacaocNmTipoReceb: TStringField;
    qryAvaliacaodDtEntrega: TDateTimeField;
    qryAvaliacaocNmTipoResultadoCQM: TStringField;
    qryAvaliacaodDtCad: TDateTimeField;
    qryAvaliacaocEmitente: TStringField;
    qryAvaliacaocAvaliador: TStringField;
    qryAvaliacaocNmLocalEntrega: TStringField;
    qryAvaliacaocRevisaoQuestionario: TStringField;
    qryAvaliacaocCodFormulario: TStringField;
    qryAvaliacaocInformacaoAdicional: TMemoField;
    qryAvaliacaocPergunta: TStringField;
    qryAvaliacaocResposta: TStringField;
    qryAvaliacaoiPeso: TIntegerField;
    QRDBText1: TQRDBText;
    QRLabel14: TQRLabel;
    QRDBText11: TQRDBText;
    QRLabel16: TQRLabel;
    QRLabel17: TQRLabel;
    QRDBText12: TQRDBText;
    QRDBText13: TQRDBText;
    QRLabel18: TQRLabel;
    QRDBText14: TQRDBText;
    QRLabel19: TQRLabel;
    QRDBText15: TQRDBText;
    QRLabel20: TQRLabel;
    QRDBText2: TQRDBText;
    QRLabel4: TQRLabel;
    QRDBText3: TQRDBText;
    QRLabel5: TQRLabel;
    QRDBText7: TQRDBText;
    QRLabel6: TQRLabel;
    QRDBText9: TQRDBText;
    QRLabel7: TQRLabel;
    QRDBText10: TQRDBText;
    qryAvaliacaonCdAvaliacaoCQM: TStringField;
    QRDBText16: TQRDBText;
    QRLabel8: TQRLabel;
    QRShape5: TQRShape;
    QRLabel36: TQRLabel;
    QRLabel15: TQRLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptAvaliacaoCQM_View: TrptAvaliacaoCQM_View;

implementation

{$R *.dfm}

end.
