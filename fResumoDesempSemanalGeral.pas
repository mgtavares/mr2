unit fResumoDesempSemanalGeral;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxLookAndFeelPainters, DBGridEhGrouping, GridsEh, DBGridEh, cxPC,
  cxControls, DB, ADODB, StdCtrls, cxButtons, DBCtrls, Mask;

type
  TfrmResumoDesempSemanalGeral = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    cxButton1: TcxButton;
    edtEmpresa: TMaskEdit;
    DBEdit1: TDBEdit;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacNmEmpresa: TStringField;
    dsEmpresa: TDataSource;
    SP_RESUMO_DESEMP_SEMANAL_GERAL: TADOStoredProc;
    dsResumoDesempSemanal: TDataSource;
    DBGridEh1: TDBGridEh;
    qryNomeLoja: TADOQuery;
    RadioGroup1: TRadioGroup;
    gpPeriodo: TGroupBox;
    RadioGroup2: TRadioGroup;
    Label3: TLabel;
    edtMesAno: TMaskEdit;
    Label4: TLabel;
    ComboBox1: TComboBox;
    gpData: TGroupBox;
    DateTimePicker1: TDateTimePicker;
    Label5: TLabel;
    qryNomeLojacNmLoja: TStringField;
    procedure cxButton1Click(Sender: TObject);
    procedure DBGridEh1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumnEh; State: TGridDrawState);
    procedure edtEmpresaExit(Sender: TObject);
    procedure edtEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure RadioGroup2Click(Sender: TObject);
    procedure edtEmpresaChange(Sender: TObject);
    procedure edtMesAnoChange(Sender: TObject);
    procedure ComboBox1Change(Sender: TObject);
    procedure RadioGroup1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmResumoDesempSemanalGeral: TfrmResumoDesempSemanalGeral;

implementation

uses fMenu, fLookup_Padrao;

{$R *.dfm}

procedure TfrmResumoDesempSemanalGeral.cxButton1Click(Sender: TObject);
var
  iMes, iAno,nColumn, i, n : integer;
  ColumnHead,ColumnHeadM : string;
label
  SemResumo;

begin
  inherited;

  {-- quando o modo de visualiza��o for por per�odo --
   -- verifica se a data digitada � valida --}
  if (RadioGroup2.ItemIndex = 0) then
  begin
      if (trim(edtMesAno.Text) = '/') then
      begin
          edtMesAno.Text := Copy(DateToStr(Date),4,7) ;
      end ;

      edtMesAno.Text := Trim(frmMenu.ZeroEsquerda(edtMesAno.Text,6)) ;

      try
          StrToDate('01/' + edtMesAno.Text) ;
      except
          MensagemErro('M�s/Ano inv�lido.'+#13#13+'Utilize: mm/aaaa') ;
          edtMesAno.SetFocus;
          abort ;
      end ;

      iMes := StrToInt(Trim(Copy(edtMesAno.Text,1,2)));
      iAno := StrToInt(Trim(Copy(edtMesAno.Text,4,4)));

  end
  else
  begin
      iMes := 0;
      iAno := 0;
  end;


  {-- se os campos de empresa ou loja estiverem em branco usa a loja e/ou empresa ativa --}

  if (Trim(edtEmpresa.Text) = '') then
  begin
      edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);

      qryEmpresa.Close;
      PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
  end;

  cxButton1.Caption := 'Aguarde...';

  {-- executa a procedure para montar a tabela din�mica --}

  try
      SP_RESUMO_DESEMP_SEMANAL_GERAL.Close;
      SP_RESUMO_DESEMP_SEMANAL_GERAL.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(edtEmpresa.Text);

      SP_RESUMO_DESEMP_SEMANAL_GERAL.Parameters.ParamByName('@iAno').Value              := iAno;
      SP_RESUMO_DESEMP_SEMANAL_GERAL.Parameters.ParamByName('@iMes').Value              := iMes;
      SP_RESUMO_DESEMP_SEMANAL_GERAL.Parameters.ParamByName('@iSemana').Value           := ComboBox1.ItemIndex;

      SP_RESUMO_DESEMP_SEMANAL_GERAL.Parameters.ParamByName('@dDtExibe').Value          := DateToStr(DateTimePicker1.Date);
      SP_RESUMO_DESEMP_SEMANAL_GERAL.Parameters.ParamByName('@nCdTipoComparacao').Value := RadioGroup1.ItemIndex;
      SP_RESUMO_DESEMP_SEMANAL_GERAL.Open;

      {-- se n�o houver nenhum resumo para os filtros selecionados,
       -- fecha a procedure, remove as colunas e faz um desvio de fluxo --}

      if (SP_RESUMO_DESEMP_SEMANAL_GERAL.Fields.Count <= 3) then
      begin
          SP_RESUMO_DESEMP_SEMANAL_GERAL.Close;
          DBGridEh1.Columns.Clear;
          MensagemAlerta('N�o existe nenhum resumo previsto para o filtro selecionado.');
          goto SemResumo;
      end;

      SP_RESUMO_DESEMP_SEMANAL_GERAL.DisableControls;

      SP_RESUMO_DESEMP_SEMANAL_GERAL.FieldList.Update;

      DBGridEh1.Columns.AddAllColumns(true);

      nColumn := DBGridEh1.Columns.Count - 1;

      {-- nesse espa�o faz o tratamento das colunas --}

      DBGridEh1.Columns[0].Visible       := false;
      DBGridEh1.Columns[1].Width         := 118;
      DBGridEh1.Columns[1].Title.Caption := '';

      if (RadioGroup1.ItemIndex = 0) then
          DBGridEh1.Columns[2].Title.Caption := 'TOTAL DO PER�ODO';

      DBGridEh1.Columns[2].Color         := $00F2F2F2;

      for i:= (3 - RadioGroup1.ItemIndex) to nColumn do
      begin

          if (RadioGroup1.ItemIndex = 1) then
          begin
              n := i * 2;
              if (n <= (nColumn - 1)) then
                  DBGridEh1.Columns[n].Color := $00F2F2F2;
          end;

          ColumnHead  := DBGridEh1.Columns[i].Title.Caption;
          ColumnHeadM := DBGridEh1.Columns[i].Title.Caption;

          if (Copy(ColumnHead,1,8) = 'nCdLoja_') then
              ColumnHead := Copy(ColumnHead,9,Length(ColumnHead)-8)
          else if (Copy(ColumnHead,1,11) = 'iTotalMeta_') then
              ColumnHead := Copy(ColumnHead,12,Length(ColumnHead)-11);

          qryNomeLoja.Close;
          qryNomeLoja.Parameters.ParamByName('nCdLoja').Value := StrToInt(ColumnHead);
          qryNomeLoja.Open;

          if ((Copy(ColumnHeadM,1,8) = 'nCdLoja_') and (RadioGroup1.ItemIndex = 0)) then
              DBGridEh1.Columns[i].Title.Caption := frmMenu.ZeroEsquerda(ColumnHead,3) + '|' + Copy(qryNomeLojacNmLoja.Value,1,15)
          else if ((Copy(ColumnHeadM,1,8) = 'nCdLoja_') and (RadioGroup1.ItemIndex = 1)) then
              DBGridEh1.Columns[i].Title.Caption := frmMenu.ZeroEsquerda(ColumnHead,3) + '|' + Copy(qryNomeLojacNmLoja.Value,1,15) + '|REALIZADO'
          else if ((Copy(ColumnHeadM,1,11) = 'iTotalMeta_') and (RadioGroup1.ItemIndex = 1)) then
              DBGridEh1.Columns[i].Title.Caption := frmMenu.ZeroEsquerda(ColumnHead,3) + '|' + Copy(qryNomeLojacNmLoja.Value,1,15) + '|META';


      end;

      for i:= 1 to nColumn do
      begin

          DBGridEh1.Columns[i].Title.Font.Size  := 8;
          DBGridEh1.Columns[i].Title.Font.Style := [fsBold];
          DBGridEh1.Columns.Items[i].Font.Color := clTeal;
          DBGridEh1.Columns[i].Alignment        := taRightJustify;

          if ((i > 1) and (RadioGroup1.ItemIndex = 0)) then
              DBGridEh1.Columns[i].Width        := 109
          else if ((i > 1) and (RadioGroup1.ItemIndex = 1)) then
              DBGridEh1.Columns[i].Width        := 76;
      end;

      DBGridEh1.ReadOnly := true;

      {-- aqui esta o desvio de fluxo, caso n�o exista nenhum resumo para
       -- o filtro selecionado, pula o processo de formata��o das colunas --}
       
      SemResumo:
  finally
      cxButton1.Caption := 'Exibir Resumo';
  end;

  {-- fim do tratamento das colunas --}

  SP_RESUMO_DESEMP_SEMANAL_GERAL.EnableControls;

end;

procedure TfrmResumoDesempSemanalGeral.DBGridEh1DrawColumnCell(
  Sender: TObject; const Rect: TRect; DataCol: Integer; Column: TColumnEh;
  State: TGridDrawState);

var
  bitmap                    : TBitmap;
  fixRect                   : TRect;
  bmpWidth, imgIndex        : integer;
  nValConfronto, nValResult : Double;

begin
  inherited;

  fixRect := Rect;

  {-- Remove o conteudo das celulas e ajusta as dimens�es da imagem em rela��o � celula --}
  DBGridEh1.Canvas.FillRect(Rect);

  fixRect.Top    := fixRect.Top + 7;
  fixRect.Bottom := fixRect.Bottom - 7;

  if ((Column.Index >= 3) and (DBGridEh1.Columns[0].Field.Value > 8) and ((Odd(Column.Index)) OR (RadioGroup1.ItemIndex = 0)))  then
  begin
      {-- compara o resultado obtido pelos vendedores --
       -- com o resultado da loja --}
      if (RadioGroup1.ItemIndex = 0) then
          nValConfronto := StrToFloat(StringReplace(DBGridEh1.Columns[2].Field.Value,'.','',[rfReplaceAll]))
      else nValConfronto := StrToFloat(StringReplace(DBGridEh1.Columns[Column.Index - 1].Field.Value,'.','',[rfReplaceAll]));
      
      nValResult    := StrToFloat(StringReplace(Column.Field.Value,'.','',[rfReplaceAll]));

      if (nValResult >= nValConfronto) then
          imgIndex := 4 // imagem de meta atingida
      else
          imgIndex := 3; // imagem de meta n�o atingida

      {-- cria a imagem de acordo com os crit�rios acima --}
      bitmap := TBitmap.Create;
      
      try

          ImageList1.GetBitmap(imgIndex,bitmap);

          {-- fixa as dimens�es da imagem --}
          bmpWidth                := 15;
          fixRect.Right           := Rect.Left + bmpWidth;
          bitmap.TransparentColor := clWhite;
          bitmap.Transparent      := true;
          
          {-- desenha a imagem --}
          DBGridEh1.Canvas.StretchDraw(fixRect,bitmap);
      finally
          bitmap.Free;
      end;    

      {-- adiciona o espa�o da imagem dentro da celula --}
      fixRect        := Rect;
      fixRect.Top    := fixRect.Top + 7;
      fixRect.Bottom := fixRect.Bottom - 7;
      fixRect.Left   := fixRect.Left + bmpWidth;
  end;

  {-- aqui desenha a celula com os valores definidos --}
  DBGridEh1.DefaultDrawColumnCell(fixRect,DataCol,Column,State);


end;

procedure TfrmResumoDesempSemanalGeral.edtEmpresaExit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
end;

procedure TfrmResumoDesempSemanalGeral.edtEmpresaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            qryEmpresa.Close;

            edtEmpresa.Text := IntToStr(nPK);
            PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;
        end ;

    end ;

  end ;
end;

procedure TfrmResumoDesempSemanalGeral.FormShow(Sender: TObject);
begin
  inherited;
  edtEmpresa.Text := IntToStr(frmMenu.nCdEmpresaAtiva);

  qryEmpresa.Close;
  qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryEmpresa, edtEmpresa.Text) ;

  if (RadioGroup2.ItemIndex = 0) then
  begin
      gpPeriodo.Visible := true;
      gpData.Visible    := false;
  end
  else
  begin
      DateTimePicker1.Date := Now();
      gpPeriodo.Visible := false;
      gpData.Visible    := true;
  end;
end;

procedure TfrmResumoDesempSemanalGeral.RadioGroup2Click(Sender: TObject);
begin
  inherited;
  SP_RESUMO_DESEMP_SEMANAL_GERAL.Close;
  DBGridEh1.Columns.Clear;
  
  if (RadioGroup2.ItemIndex = 0) then
  begin
      gpPeriodo.Visible := true;
      gpData.Visible    := false;
  end
  else
  begin
      DateTimePicker1.Date := Now();
      gpPeriodo.Visible    := false;
      gpData.Visible       := true;
  end;
end;

procedure TfrmResumoDesempSemanalGeral.edtEmpresaChange(Sender: TObject);
begin
  inherited;

  SP_RESUMO_DESEMP_SEMANAL_GERAL.Close;
  DBGridEh1.Columns.Clear;
end;

procedure TfrmResumoDesempSemanalGeral.edtMesAnoChange(Sender: TObject);
begin
  inherited;

  SP_RESUMO_DESEMP_SEMANAL_GERAL.Close;
  DBGridEh1.Columns.Clear;
end;

procedure TfrmResumoDesempSemanalGeral.ComboBox1Change(Sender: TObject);
begin
  inherited;

  SP_RESUMO_DESEMP_SEMANAL_GERAL.Close;
  DBGridEh1.Columns.Clear;
end;

procedure TfrmResumoDesempSemanalGeral.RadioGroup1Click(Sender: TObject);
begin
  inherited;
  
  SP_RESUMO_DESEMP_SEMANAL_GERAL.Close;
  DBGridEh1.Columns.Clear;
end;

initialization
    RegisterClass(TfrmResumoDesempSemanalGeral);

end.
