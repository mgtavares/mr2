inherited frmContaCaixaCofre: TfrmContaCaixaCofre
  Left = 286
  Top = 66
  Width = 911
  Height = 617
  Caption = 'Conta Caixa/Cofre'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 895
    Height = 554
  end
  object Label1: TLabel [1]
    Left = 61
    Top = 46
    Width = 38
    Height = 13
    Alignment = taRightJustify
    Caption = 'C'#243'digo'
    FocusControl = DBEdit1
  end
  object Label2: TLabel [2]
    Left = 35
    Top = 70
    Width = 64
    Height = 13
    Alignment = taRightJustify
    Caption = 'Nome Conta'
    FocusControl = DBEdit2
  end
  object Label3: TLabel [3]
    Left = 11
    Top = 94
    Width = 88
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja Respons'#225'vel'
    FocusControl = DBEdit3
  end
  object Label4: TLabel [4]
    Left = 6
    Top = 118
    Width = 93
    Height = 13
    Alignment = taRightJustify
    Caption = 'Usu'#225'rio Operador'
    FocusControl = DBEdit4
  end
  object Label5: TLabel [5]
    Left = 40
    Top = 167
    Width = 59
    Height = 13
    Alignment = taRightJustify
    Caption = 'Saldo Atual'
    FocusControl = DBEdit5
  end
  object Label8: TLabel [6]
    Left = 67
    Top = 142
    Width = 32
    Height = 13
    Caption = 'Status'
  end
  inherited ToolBar2: TToolBar
    Width = 895
  end
  object DBEdit1: TDBEdit [8]
    Tag = 1
    Left = 104
    Top = 40
    Width = 65
    Height = 19
    DataField = 'nCdContaBancaria'
    DataSource = dsMaster
    TabOrder = 1
  end
  object DBEdit2: TDBEdit [9]
    Left = 104
    Top = 64
    Width = 195
    Height = 19
    DataField = 'nCdConta'
    DataSource = dsMaster
    TabOrder = 2
  end
  object DBEdit3: TDBEdit [10]
    Left = 104
    Top = 88
    Width = 65
    Height = 19
    DataField = 'nCdLoja'
    DataSource = dsMaster
    TabOrder = 3
    OnExit = DBEdit3Exit
    OnKeyDown = DBEdit3KeyDown
  end
  object DBEdit4: TDBEdit [11]
    Left = 104
    Top = 112
    Width = 65
    Height = 19
    DataField = 'nCdUsuarioOperador'
    DataSource = dsMaster
    TabOrder = 4
    OnExit = DBEdit4Exit
    OnKeyDown = DBEdit4KeyDown
  end
  object DBEdit5: TDBEdit [12]
    Tag = 1
    Left = 104
    Top = 161
    Width = 97
    Height = 19
    DataField = 'nSaldoConta'
    DataSource = dsMaster
    TabOrder = 6
  end
  object DBEdit6: TDBEdit [13]
    Tag = 1
    Left = 172
    Top = 88
    Width = 485
    Height = 19
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 7
  end
  object DBEdit7: TDBEdit [14]
    Tag = 1
    Left = 172
    Top = 112
    Width = 485
    Height = 19
    DataField = 'cNmUsuario'
    DataSource = dsUsuario
    TabOrder = 8
  end
  object cxPageControl1: TcxPageControl [15]
    Left = 7
    Top = 259
    Width = 650
    Height = 311
    ActivePage = TabUsuarios
    LookAndFeel.Kind = lfOffice11
    LookAndFeel.NativeStyle = True
    TabOrder = 9
    ClientRectBottom = 307
    ClientRectLeft = 4
    ClientRectRight = 646
    ClientRectTop = 24
    object TabUsuarios: TcxTabSheet
      Caption = 'Usu'#225'rios'
      ImageIndex = 0
      object DBGridEh1: TDBGridEh
        Left = 0
        Top = 0
        Width = 642
        Height = 283
        Align = alClient
        DataGrouping.GroupLevels = <>
        DataSource = dsUsuariosContaCofre
        Flat = True
        FooterColor = clWindow
        FooterFont.Charset = DEFAULT_CHARSET
        FooterFont.Color = clWindowText
        FooterFont.Height = -11
        FooterFont.Name = 'Segoe UI'
        FooterFont.Style = []
        IndicatorOptions = [gioShowRowIndicatorEh]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'Segoe UI'
        TitleFont.Style = []
        UseMultiTitle = True
        OnKeyUp = DBGridEh1KeyUp
        Columns = <
          item
            EditButtons = <>
            FieldName = 'nCdUsuario'
            Footers = <>
            Title.Caption = 'Usu'#225'rios que acessam esta conta cofre|C'#243'digo'
          end
          item
            EditButtons = <>
            FieldName = 'cNmUsuario'
            Footers = <>
            ReadOnly = True
            Title.Caption = 'Usu'#225'rios que acessam esta conta cofre|Nome Usu'#225'rio'
            Width = 535
          end>
        object RowDetailData: TRowDetailPanelControlEh
        end
      end
    end
  end
  object ER2LkpStatus: TER2LookupDBEdit [16]
    Left = 104
    Top = 136
    Width = 65
    Height = 19
    DataField = 'nCdStatus'
    DataSource = dsMaster
    TabOrder = 5
    CodigoLookup = 2
    QueryLookup = qryStatus
  end
  object DBEdit8: TDBEdit [17]
    Tag = 1
    Left = 172
    Top = 136
    Width = 485
    Height = 19
    DataField = 'cNmStatus'
    DataSource = dsStatus
    TabOrder = 10
  end
  object gbOpcao: TGroupBox [18]
    Left = 8
    Top = 192
    Width = 649
    Height = 57
    Caption = ' Configura'#231#245'es '
    TabOrder = 11
    object DBCheckBox1: TDBCheckBox
      Left = 16
      Top = 24
      Width = 57
      Height = 17
      Caption = 'Cofre'
      DataField = 'cFlgCofre'
      DataSource = dsMaster
      TabOrder = 0
      ValueChecked = '1'
      ValueUnchecked = '0'
      OnClick = DBCheckBox1Click
      OnExit = DBCheckBox1Exit
    end
    object DBCheckBox2: TDBCheckBox
      Left = 96
      Top = 24
      Width = 161
      Height = 17
      Caption = 'Permitir Provis'#227'o de T'#237'tulo'
      DataField = 'cFlgProvTit'
      DataSource = dsMaster
      TabOrder = 1
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox3: TDBCheckBox
      Left = 280
      Top = 24
      Width = 145
      Height = 17
      Caption = 'Compor Fluxo de Caixa'
      DataField = 'cFlgFluxo'
      DataSource = dsMaster
      TabOrder = 2
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
    object DBCheckBox4: TDBCheckBox
      Left = 448
      Top = 24
      Width = 193
      Height = 17
      Caption = 'Bloquear Valor Dinheiro Remessa'
      DataField = 'cFlgBloqDinheiroRemessa'
      DataSource = dsMaster
      TabOrder = 3
      ValueChecked = '1'
      ValueUnchecked = '0'
    end
  end
  inherited qryMaster: TADOQuery
    AfterClose = qryMasterAfterClose
    AfterScroll = qryMasterAfterScroll
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM ContaBancaria'
      'WHERE nCdContaBancaria = :nPK'
      'AND cFlgCaixa = 1'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 704
    Top = 40
    object qryMasternCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryMasternCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryMasternCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryMastercAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryMasternCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryMasternCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryMastercFlgFluxo: TIntegerField
      FieldName = 'cFlgFluxo'
    end
    object qryMasteriUltimoCheque: TIntegerField
      FieldName = 'iUltimoCheque'
    end
    object qryMasteriUltimoBordero: TIntegerField
      FieldName = 'iUltimoBordero'
    end
    object qryMasternValLimiteCredito: TBCDField
      FieldName = 'nValLimiteCredito'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgDeposito: TIntegerField
      FieldName = 'cFlgDeposito'
    end
    object qryMastercFlgEmiteCheque: TIntegerField
      FieldName = 'cFlgEmiteCheque'
    end
    object qryMastercFlgCaixa: TIntegerField
      FieldName = 'cFlgCaixa'
    end
    object qryMasternCdLoja: TIntegerField
      FieldName = 'nCdLoja'
    end
    object qryMasternCdUsuarioOperador: TIntegerField
      FieldName = 'nCdUsuarioOperador'
    end
    object qryMasternSaldoConta: TBCDField
      FieldName = 'nSaldoConta'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
    object qryMastercFlgCofre: TIntegerField
      FieldName = 'cFlgCofre'
    end
    object qryMasterdDtUltConciliacao: TDateTimeField
      FieldName = 'dDtUltConciliacao'
    end
    object qryMastercFlgEmiteBoleto: TIntegerField
      FieldName = 'cFlgEmiteBoleto'
    end
    object qryMastercNmTitular: TStringField
      FieldName = 'cNmTitular'
      Size = 50
    end
    object qryMastercFlgProvTit: TIntegerField
      FieldName = 'cFlgProvTit'
    end
    object qryMasternCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryMasternSaldoInicial: TBCDField
      FieldName = 'nSaldoInicial'
      Precision = 12
      Size = 2
    end
    object qryMasterdDtSaldoInicial: TDateTimeField
      FieldName = 'dDtSaldoInicial'
    end
    object qryMastercFlgReapresentaCheque: TIntegerField
      FieldName = 'cFlgReapresentaCheque'
    end
    object qryMasternCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryMasterdDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryMasteriUltimoBoleto: TIntegerField
      FieldName = 'iUltimoBoleto'
    end
    object qryMastercPrimeiraInstrucaoBoleto: TStringField
      FieldName = 'cPrimeiraInstrucaoBoleto'
      Size = 100
    end
    object qryMastercSegundaInstrucaoBoleto: TStringField
      FieldName = 'cSegundaInstrucaoBoleto'
      Size = 100
    end
    object qryMastercCedenteAgencia: TStringField
      FieldName = 'cCedenteAgencia'
      Size = 5
    end
    object qryMastercCedenteAgenciaDigito: TStringField
      FieldName = 'cCedenteAgenciaDigito'
      FixedChar = True
      Size = 1
    end
    object qryMastercCedenteConta: TStringField
      FieldName = 'cCedenteConta'
      Size = 10
    end
    object qryMastercCedenteContaDigito: TStringField
      FieldName = 'cCedenteContaDigito'
      FixedChar = True
      Size = 1
    end
    object qryMastercCedenteCarteira: TStringField
      FieldName = 'cCedenteCarteira'
      Size = 3
    end
    object qryMastercCedenteCNPJCPF: TStringField
      FieldName = 'cCedenteCNPJCPF'
      Size = 14
    end
    object qryMastercCedenteNome: TStringField
      FieldName = 'cCedenteNome'
      Size = 30
    end
    object qryMastercCedenteCodCedente: TStringField
      FieldName = 'cCedenteCodCedente'
    end
    object qryMasternCdTabTipoModeloImpBoleto: TIntegerField
      FieldName = 'nCdTabTipoModeloImpBoleto'
    end
    object qryMasternCdTabTipoRespEmissaoBoleto: TIntegerField
      FieldName = 'nCdTabTipoRespEmissaoBoleto'
    end
    object qryMasternCdTabTipoLayOutRemessa: TIntegerField
      FieldName = 'nCdTabTipoLayOutRemessa'
    end
    object qryMasteriSeqNossoNumero: TIntegerField
      FieldName = 'iSeqNossoNumero'
    end
    object qryMasteriSeqArquivoRemessa: TIntegerField
      FieldName = 'iSeqArquivoRemessa'
    end
    object qryMasteriDiasProtesto: TIntegerField
      FieldName = 'iDiasProtesto'
    end
    object qryMastercFlgAceite: TIntegerField
      FieldName = 'cFlgAceite'
    end
    object qryMastercDiretorioArquivoRemessa: TStringField
      FieldName = 'cDiretorioArquivoRemessa'
      Size = 50
    end
    object qryMastercDiretorioArquivoLicenca: TStringField
      FieldName = 'cDiretorioArquivoLicenca'
      Size = 50
    end
    object qryMasteriPrioridade: TIntegerField
      FieldName = 'iPrioridade'
    end
    object qryMastercFlgBloqDinheiroRemessa: TIntegerField
      FieldName = 'cFlgBloqDinheiroRemessa'
    end
  end
  inherited dsMaster: TDataSource
    Left = 704
    Top = 72
  end
  inherited qryID: TADOQuery
    Left = 864
    Top = 72
  end
  inherited usp_ProximoID: TADOStoredProc
    Left = 704
    Top = 104
  end
  inherited qryStat: TADOQuery
    Left = 864
    Top = 40
  end
  inherited usp_ValidaPermissao: TADOStoredProc
    Left = 736
    Top = 104
  end
  inherited ImageList1: TImageList
    Left = 800
    Top = 104
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdEmpresa'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLoja, cNmLoja'
      'FROM Loja'
      'WHERE nCdLoja = :nPK'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 768
    Top = 40
    object qryLojanCdLoja: TAutoIncField
      FieldName = 'nCdLoja'
      ReadOnly = True
    end
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object qryUsuario: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario, cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK')
    Left = 800
    Top = 40
    object qryUsuarionCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariocNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 768
    Top = 72
  end
  object dsUsuario: TDataSource
    DataSet = qryUsuario
    Left = 800
    Top = 72
  end
  object qryUsuariosContaCofre: TADOQuery
    Connection = frmMenu.Connection
    BeforePost = qryUsuariosContaCofreBeforePost
    OnCalcFields = qryUsuariosContaCofreCalcFields
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT *'
      '  FROM UsuarioContaBancaria'
      ' WHERE nCdContaBancaria = :nPK')
    Left = 832
    Top = 40
    object qryUsuariosContaCofrenCdUsuarioContaBancaria: TAutoIncField
      FieldName = 'nCdUsuarioContaBancaria'
    end
    object qryUsuariosContaCofrenCdContaBancaria: TIntegerField
      FieldName = 'nCdContaBancaria'
    end
    object qryUsuariosContaCofrenCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuariosContaCofrenCdServidorOrigem: TIntegerField
      FieldName = 'nCdServidorOrigem'
    end
    object qryUsuariosContaCofredDtReplicacao: TDateTimeField
      FieldName = 'dDtReplicacao'
    end
    object qryUsuariosContaCofrecNmUsuario: TStringField
      FieldKind = fkCalculated
      FieldName = 'cNmUsuario'
      Size = 30
      Calculated = True
    end
  end
  object dsUsuariosContaCofre: TDataSource
    DataSet = qryUsuariosContaCofre
    Left = 832
    Top = 72
  end
  object qryUsuarios: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdUsuario, cNmUsuario'
      'FROM Usuario'
      'WHERE nCdUsuario = :nPK'
      '')
    Left = 768
    Top = 104
    object qryUsuariosnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryUsuarioscNmUsuario: TStringField
      FieldName = 'cNmUsuario'
      Size = 50
    end
  end
  object qryStatus: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    SQL.Strings = (
      'SELECT *'
      'FROM Status'
      'WHERE nCdStatus = :nPK')
    Left = 736
    Top = 40
    object qryStatusnCdStatus: TIntegerField
      FieldName = 'nCdStatus'
    end
    object qryStatuscNmStatus: TStringField
      FieldName = 'cNmStatus'
      Size = 50
    end
  end
  object dsStatus: TDataSource
    DataSet = qryStatus
    Left = 736
    Top = 72
  end
end
