inherited frmImportaAliquotaIBPT: TfrmImportaAliquotaIBPT
  Left = 301
  Top = 264
  Height = 219
  Caption = 'Importar Al'#237'quota IBPT'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Top = 25
    Height = 156
  end
  object Label2: TLabel [1]
    Left = 12
    Top = 66
    Width = 118
    Height = 13
    Alignment = taRightJustify
    Caption = 'Caminho Arquivo (*.csv)'
  end
  object Label1: TLabel [2]
    Left = 97
    Top = 42
    Width = 33
    Height = 13
    Alignment = taRightJustify
    Caption = 'Estado'
  end
  inherited ToolBar1: TToolBar
    Height = 25
    TabOrder = 2
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object edtCaminho: TEdit [4]
    Left = 134
    Top = 58
    Width = 498
    Height = 21
    TabOrder = 1
  end
  object cxButton1: TcxButton [5]
    Left = 637
    Top = 56
    Width = 28
    Height = 28
    Hint = 'Selecionar Arquivo'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnClick = cxButton1Click
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      18000000000000030000C40E0000C40E00000000000000000000A2CAEE76B2E6
      3E91DB348CD9348CD9348CD9348CD9348CD9348CD9348CD9348CD9348BD9398F
      DA85B9E9C9C9C9C9C9C94799DDDEF1FAA8DDF49EDBF496DAF38ED8F386D7F37F
      D4F279D3F272D2F16CD0F169CFF1C2EAF83F95DBC9C9C9C9C9C93B97DBEFFAFE
      A1E9F991E5F881E1F772DEF663DAF554D7F447D3F339D0F22ECDF126CBF0CAF2
      FB3B97DBC9C9C9C9C9C93C9DDBF2FAFDB3EDFAA4E9F995E6F885E2F781E1F77A
      E0F76FDDF662DAF554D6F347D3F2E8F9FD3594DAC9C9C9C9C9C93BA3DBF6FCFE
      C8F2FCB9EFFBACECFA8CE4F88AE3F882E1F779DFF76DDDF661DAF557D7F4E7F8
      FD3594DAC9C9C9C9C9C93BA8DBFEFFFFF8FDFFF6FDFFF5FCFFE8FAFEAFECFA8E
      E4F887E3F87DE0F772DDF668DBF5E9F9FD3594DAC9C9C9C9C9C939ADDBE8F6FB
      7EC5EA5BAEE351A8E160AFE4EBFAFDECFAFEE5F5FCE5F6FCE4F5FCE4F5FCFEFF
      FF3594DAC9C9C9C9C9C940AEDCF1FAFD94DEF593DCF481D5F260C0E94FAEE135
      94DA3594DA3594DA3594DA3594DA3594DA3594DA3836343D3D3C41B4DCF7FCFE
      8EE4F891DEF59FE0F5ACE1F6EFFBFE635B54A79A8D70675E44403BA3AAAC5C55
      4E968A7F665E561B1A193CB5DBFDFEFEFEFFFFFEFEFFFDFEFFFEFFFFEAF7FB71
      685FC1B6AB7970664B55556097A86E665DBDB1A57E746A33302F59C2E061C3E2
      63C4E363C4E363C4E362C4E356C0E0786F67C4B9AFA79A8D7D7C78CDD3D46C64
      5EBBAFA4A6988B54524FEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE70
      6861696158564F484D4B488C8C8B403C3946423C3D38332D2C2BEEEEEEEEEEEE
      EEEEEEEEEEEEEEEEEEEEEEEEEEEEEE9A98955B544DB0A090695D518674656358
      4EAA98885A514A868584EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
      EEEE6D665E85786F4A413B95897E564A409283744F4740E8E8E7EEEEEEEEEEEE
      EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEC5BFB89C8F83524D48CDCAC8544D
      4790867C9C9A97EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
      EEEEEEEEEE797067766D66DFDBD76F675F6C6864EEEEEEEEEEEE}
    LookAndFeel.NativeStyle = True
  end
  object er2LkpEstado: TER2LookupMaskEdit [6]
    Left = 134
    Top = 34
    Width = 65
    Height = 21
    EditMask = '#########;1; '
    MaxLength = 9
    TabOrder = 0
    Text = '         '
    CodigoLookup = 204
    QueryLookup = qryEstado
  end
  object DBEdit1: TDBEdit [7]
    Tag = 1
    Left = 202
    Top = 34
    Width = 29
    Height = 21
    DataField = 'cUF'
    DataSource = dsEstado
    TabOrder = 4
  end
  object DBEdit2: TDBEdit [8]
    Tag = 1
    Left = 234
    Top = 34
    Width = 398
    Height = 21
    DataField = 'cNmEstado'
    DataSource = dsEstado
    TabOrder = 5
  end
  inherited ImageList1: TImageList
    Left = 216
    Top = 120
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.csv'
    Filter = 'Arquivos CSV (*.csv)|*.csv'
    Title = 'Selecione a NFe'
    Left = 248
    Top = 120
  end
  object qryTabIBPT: TADOQuery
    Connection = frmMenu.Connection
    CursorType = ctStatic
    Parameters = <>
    Left = 280
    Top = 88
    object qryTabIBPTnCdTabIBPT: TIntegerField
      FieldName = 'nCdTabIBPT'
    end
    object qryTabIBPTcNCM: TStringField
      FieldName = 'cNCM'
      Size = 8
    end
    object qryTabIBPTiTabela: TIntegerField
      FieldName = 'iTabela'
    end
    object qryTabIBPTcDescricao: TStringField
      FieldName = 'cDescricao'
      Size = 1000
    end
    object qryTabIBPTnAliqNacional: TBCDField
      FieldName = 'nAliqNacional'
      Precision = 12
      Size = 2
    end
    object qryTabIBPTnAliqImportado: TBCDField
      FieldName = 'nAliqImportado'
      Precision = 12
      Size = 2
    end
    object qryTabIBPTcFlgManual: TIntegerField
      FieldName = 'cFlgManual'
    end
    object qryTabIBPTnCdEstado: TIntegerField
      FieldName = 'nCdEstado'
    end
    object qryTabIBPTnAliqEstadual: TBCDField
      FieldName = 'nAliqEstadual'
      Precision = 12
      Size = 2
    end
    object qryTabIBPTnAliqMunicipal: TBCDField
      FieldName = 'nAliqMunicipal'
      Precision = 12
      Size = 2
    end
    object qryTabIBPTcVersao: TStringField
      FieldName = 'cVersao'
    end
    object qryTabIBPTdDtValidInicial: TDateTimeField
      FieldName = 'dDtValidInicial'
    end
    object qryTabIBPTdDtValidFinal: TDateTimeField
      FieldName = 'dDtValidFinal'
    end
    object qryTabIBPTcChave: TStringField
      FieldName = 'cChave'
    end
  end
  object ACBrIBPTax1: TACBrIBPTax
    ProxyPort = '8080'
    Left = 280
    Top = 120
  end
  object qryIBPTEmUso: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TabIBPT.cNCM '
      '      ,TabIBPT.nCdTabIBPT  '
      '  FROM TabIBPT'
      
        '       INNER JOIN GrupoImposto GI ON GI.nCdTabIBPT = TabIBPT.nCd' +
        'TabIBPT  '
      ' GROUP BY TabIBPT.cNCM '
      '         ,TabIBPT.nCdTabIBPT'
      '')
    Left = 248
    Top = 88
    object qryIBPTEmUsocNCM: TStringField
      FieldName = 'cNCM'
      FixedChar = True
      Size = 8
    end
    object qryIBPTEmUsonCdTabIBPT: TIntegerField
      FieldName = 'nCdTabIBPT'
    end
  end
  object qryEstado: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEstado ,cUF '
      '      ,cNmEstado '
      '  FROM Estado '
      ' WHERE nCdEstado = :nPK')
    Left = 184
    Top = 88
    object qryEstadocUF: TStringField
      FieldName = 'cUF'
      FixedChar = True
      Size = 2
    end
    object qryEstadocNmEstado: TStringField
      FieldName = 'cNmEstado'
      Size = 50
    end
    object qryEstadonCdEstado: TIntegerField
      FieldName = 'nCdEstado'
    end
  end
  object dsEstado: TDataSource
    DataSet = qryEstado
    Left = 184
    Top = 120
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cNCM'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'cDescricao'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'iTabela'
        DataType = ftLargeint
        Size = -1
        Value = '0'
      end
      item
        Name = 'nAliqNacional'
        DataType = ftFloat
        Size = -1
        Value = 0.000000000000000000
      end
      item
        Name = 'nAliqImportado'
        DataType = ftFloat
        Size = -1
        Value = 0.000000000000000000
      end
      item
        Name = 'nAliqEstadual'
        DataType = ftFloat
        Size = -1
        Value = 0.000000000000000000
      end
      item
        Name = 'nAliqMunicipal'
        DataType = ftFloat
        Size = -1
        Value = 0.000000000000000000
      end
      item
        Name = 'dDtValidInicial'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'dDtValidFinal'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'cVersao'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'cChave'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'cFlgManual'
        DataType = ftString
        Size = -1
        Value = ''
      end
      item
        Name = 'nCdEstado'
        DataType = ftLargeint
        Size = -1
        Value = '0'
      end
      item
        Name = 'nCdTabIBPT'
        DataType = ftLargeint
        Size = -1
        Value = '0'
      end>
    SQL.Strings = (
      'UPDATE TabIBPT'
      '   SET cNCM            = :cNCM'
      '      ,cDescricao      = :cDescricao'
      '      ,iTabela         = :iTabela'
      '      ,nAliqNacional   = :nAliqNacional'
      '      ,nAliqImportado  = :nAliqImportado'
      '      ,nAliqEstadual   = :nAliqEstadual'
      '      ,nAliqMunicipal  = :nAliqMunicipal'
      '      ,dDtValidInicial = CONVERT(datetime,:dDtValidInicial,103)'
      '      ,dDtValidFinal   = CONVERT(datetime,:dDtValidFinal,103)'
      '      ,cVersao         = :cVersao'
      '      ,cChave          = :cChave'
      '      ,cFlgManual      = :cFlgManual'
      '      ,nCdEstado       = :nCdEstado'
      ' WHERE nCdTabIBPT      = :nCdTabIBPT')
    Left = 216
    Top = 88
  end
end
