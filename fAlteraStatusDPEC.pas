unit fAlteraStatusDPEC;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, DB, ADODB, ImgList, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, cxLookAndFeelPainters, cxButtons;

type
  TfrmAlteraStatusDPEC = class(TfrmProcesso_Padrao)
    qryStatusDPEC: TADOQuery;
    DataSource1: TDataSource;
    qryStatusDPECnCdStatusDPEC: TIntegerField;
    qryStatusDPECcMotivoDPEC: TStringField;
    qryStatusDPECdDtInicioDPEC: TDateTimeField;
    qryStatusDPECcNmStatusDPEC: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    edtMotivo: TDBEdit;
    Label3: TLabel;
    DBEdit3: TDBEdit;
    btnAlteraStatus1: TcxButton;
    qryAlteraStatus: TADOQuery;
    btnAlteraStatus2: TcxButton;
    procedure FormShow(Sender: TObject);
    procedure btnAlteraStatus1Click(Sender: TObject);
    procedure btnAlteraStatus2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAlteraStatusDPEC: TfrmAlteraStatusDPEC;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmAlteraStatusDPEC.FormShow(Sender: TObject);
begin
  inherited;

  qryStatusDPEC.Close;
  PosicionaQuery(qryStatusDPEC,IntToStr(frmMenu.nCdEmpresaAtiva));

  if (qryStatusDPECnCdStatusDPEC.Value = 1) then
  begin
      btnAlteraStatus2.Visible := True;
      btnAlteraStatus1.Visible := False;
      desativaDBEdit(edtMotivo);
  end else
  begin
      btnAlteraStatus2.Visible := False;
      btnAlteraStatus1.Visible := True;
      ativaDBEdit(edtMotivo);
  end;

  edtMotivo.SetFocus;

end;

procedure TfrmAlteraStatusDPEC.btnAlteraStatus1Click(Sender: TObject);
begin
  inherited;

  if ((Trim(qryStatusDPECcMotivoDPEC.Value) = '') AND (qryStatusDPECnCdStatusDPEC.Value = 2)) then
  begin
      MensagemAlerta('Digite o Motivo do inicio da Contingência.');
      edtMotivo.SetFocus;
      abort;
  end;

  if (Length(Trim(qryStatusDPECcMotivoDPEC.Value)) < 15) then
  begin
      MensagemAlerta('O Motivo da Contingência deve ser maior ou igual a 15 caracteres.');
      edtMotivo.SetFocus;
      abort;
  end;

  if (qryStatusDPEC.State = dsEdit) then
      qryStatusDPEC.Post;

  frmMenu.Connection.BeginTrans;

  try
      qryAlteraStatus.Close;
      qryAlteraStatus.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      qryAlteraStatus.ExecSQL;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro ao processar.');
      raise;
  end;

  frmMenu.Connection.CommitTrans;

  qryStatusDPEC.Close;
  PosicionaQuery(qryStatusDPEC,IntToStr(frmMenu.nCdEmpresaAtiva));

  btnAlteraStatus2.Visible := True;
  btnAlteraStatus1.Visible := False;
  desativaDBEdit(edtMotivo);

end;

procedure TfrmAlteraStatusDPEC.btnAlteraStatus2Click(Sender: TObject);
begin
  inherited;

  frmMenu.Connection.BeginTrans;

  try
      qryAlteraStatus.Close;
      qryAlteraStatus.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
      qryAlteraStatus.ExecSQL;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro ao processar.');
      raise;
  end;

  frmMenu.Connection.CommitTrans;

  qryStatusDPEC.Close;
  PosicionaQuery(qryStatusDPEC,IntToStr(frmMenu.nCdEmpresaAtiva));

  btnAlteraStatus2.Visible := False;
  btnAlteraStatus1.Visible := True;
  ativaDBEdit(edtMotivo);
  edtMotivo.SetFocus;
  
end;

initialization
    RegisterClass(TfrmAlteraStatusDPEC);

end.
