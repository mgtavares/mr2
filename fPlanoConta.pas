unit fPlanoConta;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, StdCtrls, DBCtrls, Mask, DB, ImgList, ADODB,
  ComCtrls, ToolWin, ExtCtrls, GridsEh, DBGridEh;

type
  TfrmPlanoConta = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoPlanoConta: TIntegerField;
    qryMastercNmGrupoPlanoConta: TStringField;
    qryMastercFlgExibeDRE: TIntegerField;
    qryMastercFlgRecDes: TStringField;
    qryContas: TADOQuery;
    qryContasnCdPlanoConta: TAutoIncField;
    qryContascNmPlanoConta: TStringField;
    qryContasnCdGrupoPlanoConta: TIntegerField;
    qryContascFlgRecDes: TStringField;
    DBGridEh1: TDBGridEh;
    dsConta: TDataSource;
    StaticText1: TStaticText;
    qryContascMascara: TStringField;
    qryContascFlgLanc: TIntegerField;
    qryContascSenso: TStringField;
    qryAux: TADOQuery;
    procedure FormCreate(Sender: TObject);
    procedure qryContasBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure DBGridEh1ColExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPlanoConta: TfrmPlanoConta;

implementation

{$R *.dfm}

procedure TfrmPlanoConta.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOPLANOCONTA' ;
  nCdTabelaSistema  := 23 ;
  nCdConsultaPadrao := 40 ;
end;

procedure TfrmPlanoConta.qryContasBeforePost(DataSet: TDataSet);
begin

  if (qryContascNmPlanoConta.Value = '') then
  begin
      MensagemAlerta('Informe a descri��o da conta.') ;
      abort ;
  end ;

  qryContascFlgRecDes.Value := 'D' ;

  if (qryContascSenso.Value = 'C') then
      qryContascFlgRecDes.Value := 'R' ;

  inherited;

end;

procedure TfrmPlanoConta.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryContas.Close ;
end;

procedure TfrmPlanoConta.FormShow(Sender: TObject);
begin
  inherited;

  qryContas.Close ;
  qryContas.Open ;
end;

procedure TfrmPlanoConta.DBGridEh1ColExit(Sender: TObject);
begin
  inherited;

  if (dbGridEh1.Col = 2) and (qryContas.State = dsInsert) then
  begin

      qryAux.Close ;
      qryAux.SQL.Clear ;
      qryAux.SQL.Add('SELECT cSenso FROM PlanoConta WHERE cMascara = ' + Chr(39) + Copy(qryContascMascara.asString,1,1) + '. . .'+ Chr(39)) ;
      qryAux.Open ;

      if not (qryAux.Eof) then
      begin
          qryContascSenso.Value := qryAux.FieldList[0].Value ;
      end ;

  end ;

end;

initialization
    RegisterClass(tFrmPlanoConta) ;
    
end.
