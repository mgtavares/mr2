unit fConsultaVendaDesconto;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGridLevel, cxClasses, cxControls, cxGridCustomView,
  cxGrid, ADODB, StdCtrls, Mask, DBCtrls;

type
  TfrmConsultaVendaDesconto = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    qryResultado: TADOQuery;
    dsResultado: TDataSource;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    qryLoja: TADOQuery;
    qryCondPagto: TADOQuery;
    qryCondPagtonCdCondPagto: TIntegerField;
    qryCondPagtocNmCondPagto: TStringField;
    qryLojanCdLoja: TIntegerField;
    qryLojacNmLoja: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    DataSource1: TDataSource;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    DataSource2: TDataSource;
    edtLoja: TMaskEdit;
    edtCondPagto: TMaskEdit;
    Label3: TLabel;
    MaskEdit2: TMaskEdit;
    Label4: TLabel;
    MaskEdit3: TMaskEdit;
    Label5: TLabel;
    qryResultadonCdPedido: TIntegerField;
    qryResultadocNmLoja: TStringField;
    qryResultadodDtVenda: TDateTimeField;
    qryResultadocNmCondPagto: TStringField;
    qryResultadocNmVendedor: TStringField;
    qryResultadonCdLanctoFin: TIntegerField;
    qryResultadonValProdutos: TBCDField;
    qryResultadonValDesconto: TBCDField;
    qryResultadonValPedido: TBCDField;
    qryResultadonValPercDesconto: TBCDField;
    cxGridDBTableView1nCdPedido: TcxGridDBColumn;
    cxGridDBTableView1cNmLoja: TcxGridDBColumn;
    cxGridDBTableView1dDtVenda: TcxGridDBColumn;
    cxGridDBTableView1cNmCondPagto: TcxGridDBColumn;
    cxGridDBTableView1cNmVendedor: TcxGridDBColumn;
    cxGridDBTableView1nCdLanctoFin: TcxGridDBColumn;
    cxGridDBTableView1nValProdutos: TcxGridDBColumn;
    cxGridDBTableView1nValDesconto: TcxGridDBColumn;
    cxGridDBTableView1nValPedido: TcxGridDBColumn;
    cxGridDBTableView1nValPercDesconto: TcxGridDBColumn;
    qryResultadonValDevolucao: TBCDField;
    cxGridDBTableView1nValDevolucao: TcxGridDBColumn;
    edtPercDesconto: TEdit;
    procedure edtLojaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtLojaExit(Sender: TObject);
    procedure edtCondPagtoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCondPagtoExit(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGridDBTableView1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConsultaVendaDesconto: TfrmConsultaVendaDesconto;

implementation

uses fLookup_Padrao, fMenu, fConsultaVendaProduto_Dados;

{$R *.dfm}

procedure TfrmConsultaVendaDesconto.edtLojaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta2(147,'EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja = Loja.nCdLoja AND UL.nCdUsuario = @@Usuario)');

        If (nPK > 0) then
        begin
            edtLoja.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TfrmConsultaVendaDesconto.edtLojaExit(Sender: TObject);
begin
  inherited;

  qryLoja.Close;
  qryLoja.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
  PosicionaQuery(qryLoja, edtLoja.Text) ;

end;

procedure TfrmConsultaVendaDesconto.edtCondPagtoKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        if (Trim(edtLoja.Text) = '') then
        begin
            MensagemAlerta('Selecione uma loja antes de escolher a Condi��o de Pagamento.');
            edtLoja.SetFocus;
            abort;
        end
        else begin
            nPK := frmLookup_Padrao.ExecutaConsulta2(138,'EXISTS (SELECT 1 FROM LojaCondPagto WHERE nCdLoja = ' + edtLoja.Text + ' AND LojaCondPagto.nCdCondPagto = CondPagto.nCdCondPagto)');

            If (nPK > 0) then
            begin
                edtCondPagto.Text := IntToStr(nPK) ;
            end ;
        end;
    end ;

  end ;
end;

procedure TfrmConsultaVendaDesconto.edtCondPagtoExit(Sender: TObject);
begin
  inherited;

  qryCondPagto.Close;
  qryCondPagto.Parameters.ParamByName('nCdLoja').Value := edtLoja.Text;
  PosicionaQuery(qryCondPagto, edtCondPagto.Text) ;

end;

procedure TfrmConsultaVendaDesconto.ToolButton1Click(Sender: TObject);
begin
  inherited;

  edtPercDesconto.Text := StringReplace(edtPercDesconto.Text,'.',',',[rfReplaceAll]);

  if ((Trim(edtPercDesconto.Text) = '') or (Trim(edtPercDesconto.Text) = ',')) then
      edtPercDesconto.Text := '0,00';

  try
      StrToFloat(edtPercDesconto.Text);
  except
      MensagemAlerta('Valor incorreto para o campo de porcentagem.');
      edtPercDesconto.SetFocus;
      edtPercDesconto.Text := '';
      exit;
  end;

  if (StrToFloat(edtPercDesconto.Text) < 0) then
  begin
      MensagemAlerta('Porcentagem de Desconto n�o pode ser inferior a Zero.');
      edtPercDesconto.SetFocus;
      abort;
  end;

  if (StrToFloat(edtPercDesconto.Text) > 100) then
  begin
      MensagemAlerta('Porcentagem de Desconto n�o pode ser maior do que 100%.');
      edtPercDesconto.SetFocus;
      abort;
  end;

  if (MaskEdit2.Text = '  /  /    ') then
      MaskEdit2.Text := DateToStr(Now());

  if (MaskEdit3.Text = '  /  /    ') then
      MaskEdit3.Text := DateToStr(Now());

  qryResultado.Close;
  qryResultado.Parameters.ParamByName('nCdLoja').Value          := frmMenu.ConvInteiro(edtLoja.Text);
  qryResultado.Parameters.ParamByName('nCdCondPagto').Value     := frmMenu.ConvInteiro(edtCondPagto.Text);
  qryResultado.Parameters.ParamByName('dDtInicial').Value       := MaskEdit2.Text;
  qryResultado.Parameters.ParamByName('dDtFinal').Value         := MaskEdit3.Text;
  qryResultado.Parameters.ParamByName('nValPercDesconto').Value := edtPercDesconto.Text;
  qryResultado.Open;
end;

procedure TfrmConsultaVendaDesconto.FormShow(Sender: TObject);
begin
  inherited;
  edtPercDesconto.Text := '0,00';
end;

procedure TfrmConsultaVendaDesconto.cxGridDBTableView1DblClick(
  Sender: TObject);
var
  objForm : TfrmConsultaVendaProduto_Dados;
begin
  inherited;
  if (qryResultado.Eof) then
      exit;

  objForm := TfrmConsultaVendaProduto_Dados.Create(nil);

  PosicionaQuery(objForm.qryItemPedido, qryResultadonCdLanctoFin.AsString) ;
  PosicionaQuery(objForm.qryCondicao  , qryResultadonCdLanctoFin.AsString) ;
  PosicionaQuery(objForm.qryPrestacoes, qryResultadonCdLanctoFin.AsString) ;
  PosicionaQuery(objForm.qryCheques   , qryResultadonCdLanctoFin.AsString) ;
  showForm(objForm,true);

end;

initialization
    RegisterClass(TfrmConsultaVendaDesconto) ;

end.
