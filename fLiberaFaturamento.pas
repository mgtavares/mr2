unit fLiberaFaturamento;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls, GridsEh, DBGridEh, cxPC, cxControls,
  DBCtrlsEh, DBLookupEh, cxStyles, cxCustomData, cxGraphics, cxFilter,
  cxData, cxDataStorage, cxEdit, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses,
  cxGridCustomView, cxGrid, DBGridEhGrouping, ToolCtrlsEh;

type                                                     
  TfrmLiberaFaturamento = class(TfrmCadastro_Padrao)
    qryMasternCdPedido: TIntegerField;
    qryMasterdDtPedido: TDateTimeField;
    qryMasterdDtPrevEntIni: TDateTimeField;
    qryMasterdDtPrevEntFim: TDateTimeField;
    qryMastercNmTipoPedido: TStringField;
    qryMastercNmTerceiro: TStringField;
    DataSource1: TDataSource;
    qryItemPedido: TADOQuery;
    qryItemPedidonCdItemPedido: TAutoIncField;
    qryItemPedidonCdProduto: TIntegerField;
    qryItemPedidocNmItem: TStringField;
    qryItemPedidonQtdePed: TBCDField;
    qryItemPedidonQtdeExpRec: TBCDField;
    qryItemPedidonCdTabStatusItemPed: TIntegerField;
    qryTabStatusItemPed: TADOQuery;
    qryTabStatusItemPednCdTabStatusItemPed: TIntegerField;
    qryTabStatusItemPedcNmTabStatusItemPed: TStringField;
    qryTabStatusItemPedcFlgFaturar: TIntegerField;
    qryItemPedidocNmTabStatusItemPed: TStringField;
    qryItemPedidonQtdePend: TBCDField;
    qryItemPedidonCdTipoItemPed: TIntegerField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    TabLote: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    qryProdLote: TADOQuery;
    qryProdLotenCdProduto: TIntegerField;
    qryProdLotecNmProduto: TStringField;
    qryProdLotenQtdeLibFat: TBCDField;
    qryProdLotecNmProduto2: TStringField;
    Label28: TLabel;
    DBComboProdutos: TDBLookupComboboxEh;
    Image4: TImage;
    DBGridEh2: TDBGridEh;
    qryLoteProd: TADOQuery;
    qryLoteProdnCdPedido: TIntegerField;
    qryLoteProdnCdProduto: TIntegerField;
    qryLoteProdcNrLote: TStringField;
    qryLoteProddDtFabricacao: TDateTimeField;
    qryLoteProddDtValidade: TDateTimeField;
    qryLoteProdnQtde: TBCDField;
    qryLoteProdcFlgFaturado: TIntegerField;
    dsProdLote: TDataSource;
    dsLoteProd: TDataSource;
    qryAux: TADOQuery;
    qryConfereLoteProduto: TADOQuery;
    qryConfereLoteProdutonSaldoLote: TBCDField;
    qryMasternCdEmpresa: TIntegerField;
    qryMasternCdLoja: TIntegerField;
    qryLoteProdnCdLoteProduto: TIntegerField;
    qryConfereLoteProdutonCdLoteProduto: TAutoIncField;
    qryItemPedidonQtdeLibFat: TBCDField;
    cmdPreparaTemp: TADOCommand;
    qryPopulaTemp: TADOQuery;
    qryMastercFlgFaturar: TIntegerField;
    qryMastercNmTabStatusPed: TStringField;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label8: TLabel;
    qryMastercNmCondPagto: TStringField;
    Label3: TLabel;
    DBEdit8: TDBEdit;
    qryItemPedidocFlgPedidoWeb: TIntegerField;
    qryItemPedidocFlgPedidoTransfEst: TIntegerField;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure qryMasterAfterScroll(DataSet: TDataSet);
    procedure qryMasterAfterClose(DataSet: TDataSet);
    procedure DBGridEh1Enter(Sender: TObject);
    procedure qryItemPedidoBeforePost(DataSet: TDataSet);
    procedure qryMasterAfterPost(DataSet: TDataSet);
    procedure qryLoteProdBeforePost(DataSet: TDataSet);
    procedure cxPageControl1Change(Sender: TObject);
    procedure DBComboProdutosChange(Sender: TObject);
    procedure btSalvarClick(Sender: TObject);
    procedure qryMasterAfterCancel(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLiberaFaturamento: TfrmLiberaFaturamento;

implementation

uses fMenu;

{$R *.dfm}

procedure TfrmLiberaFaturamento.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'PEDIDO' ;
  nCdTabelaSistema  := 30 ;
  nCdConsultaPadrao := 81 ;
  bLimpaAposSalvar  := False ;
end;

procedure TfrmLiberaFaturamento.FormShow(Sender: TObject);
begin
  inherited;

  qryMaster.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;

end;

procedure TfrmLiberaFaturamento.qryMasterAfterScroll(DataSet: TDataSet);
begin
  inherited;

  if not qryMaster.eof then
  begin

      qryMaster.EnableControls ;

      cmdPreparaTemp.Execute;

      qryPopulaTemp.Close ;
      qryPopulaTemp.Parameters.ParamByName('nPK').Value := qryMaster.FieldList[0].Value ;
      qryPopulaTemp.ExecSQL;

      qryItemPedido.Close ;
      qryItemPedido.Open ;
  end ;

end;

procedure TfrmLiberaFaturamento.qryMasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  qryItemPedido.Close ;
end;

procedure TfrmLiberaFaturamento.DBGridEh1Enter(Sender: TObject);
begin
  inherited;
  DBGridEh1.Col := 7 ;
end;

procedure TfrmLiberaFaturamento.qryItemPedidoBeforePost(DataSet: TDataSet);
begin
  inherited;

  if (qryItemPedidonQtdeLibFat.Value < 0) then
  begin
      MensagemAlerta('Quantidade inv�lida.') ;
      abort ;
  end ;

  if (qryItemPedidonQtdePed.Value - qryItemPedidonQtdeExpRec.Value) < qryItemPedidonQtdeLibFat.Value then
  begin
      MensagemAlerta('Quantidade liberada superior a quantidade pendente de faturamento.') ;
      abort ;
  end ;

  if (qryItemPedidonQtdePed.Value - qryItemPedidonQtdeExpRec.Value) <> qryItemPedidonQtdeLibFat.Value then
  begin

      if (qryItemPedidonCdTipoItemPed.Value = 1) and (qryItemPedidocFlgPedidoWeb.Value = 0) and (qryItemPedidocFlgPedidoTransfEst.Value = 0) then
      begin

          MensagemAlerta('Item de grade n�o pode ser faturado parcial.') ;
          abort ;

      end ;

  end ;

  if (qryItemPedidonQtdeLibFat.Value > 0) then
      qryItemPedidonCdTabStatusItemPed.Value := 2
  else
  begin
      qryItemPedidonCdTabStatusItemPed.Value := 1 ;
  end ;

end;

procedure TfrmLiberaFaturamento.qryMasterAfterPost(DataSet: TDataSet);
begin
  inherited;
  qryItemPedido.Close ;

end;

procedure TfrmLiberaFaturamento.qryLoteProdBeforePost(DataSet: TDataSet);
begin
  if (qryLoteProdcNrLote.Value = '') then
  begin
      MensagemAlerta('Informe o N�mero do Lote.') ;
      abort ;
  end ;

  if (qryLoteProddDtFabricacao.Value > qryLoteProddDtValidade.Value) or (qryLoteProddDtFabricacao.asString = '') or (qryLoteProddDtValidade.asString = '') then
  begin
      MensagemAlerta('Data de Validade/Fabrica��o Inv�lida.') ;
      abort ;
  end ;

  if (qryLoteProddDtValidade.Value < StrToDate(DateToStr(Now()))) then
  begin
      MensagemAlerta('Lote Vencido! Imposs�vel receber.') ;
      abort ;
  end ;

  if (qryLoteProdnQtde.Value <= 0) then
  begin
      MensagemAlerta('Quantidade inv�lida.') ;
      abort ;
  end ;

  qryConfereLoteProduto.Close ;
  qryConfereLoteProduto.Parameters.ParamByName('nCdEmpresa').Value    := qryMasternCdEmpresa.Value ;
  qryConfereLoteProduto.Parameters.ParamByName('nCdLoja').Value       := frmMenu.ConvInteiro(qryMasternCdLoja.asString);
  qryConfereLoteProduto.Parameters.ParamByName('nCdProduto').Value    := qryProdLotenCdProduto.Value ;
  qryConfereLoteProduto.Parameters.ParamByName('cNrLote').Value       := qryLoteProdcNrLote.Value ;
  qryConfereLoteProduto.Parameters.ParamByName('dDtFabricacao').Value := frmMenu.ConvData(qryLoteProddDtFabricacao.asString) ;
  qryConfereLoteProduto.Parameters.ParamByName('dDtValidade').Value   := frmMenu.ConvData(qryLoteProddDtValidade.asString) ;
  qryConfereLoteProduto.Open ;

  if (qryConfereLoteProduto.Eof) then
  begin
      MensagemAlerta('Lote de produtos n�o encontrado.') ;
      abort ;
  end ;

  if (qryConfereLoteProdutonSaldoLote.Value < qryLoteProdnQtde.Value) then
  begin
      MensagemAlerta('Quantidade digitada superior ao saldo do lote.' + #13#13 + 'Saldo: ' + qryConfereLoteProdutonSaldoLote.AsString) ;
      abort ;
  end ;

  inherited;

  qryLoteProdnCdLoteProduto.Value := qryConfereLoteProdutonCdLoteProduto.Value ;
  qryLoteProdcNrLote.Value        := Uppercase(qryLoteProdcNrLote.Value) ;
  qryLoteProdnCdPedido.Value      := qryMasternCdPedido.Value ;
  qryLoteProdnCdProduto.Value     := qryProdLotenCdProduto.Value ;
  qryLoteProdcFlgFaturado.Value   := 0 ;

end;

procedure TfrmLiberaFaturamento.cxPageControl1Change(Sender: TObject);
begin
  inherited;

  if (cxPageControl1.ActivePage = TabLote) then
  begin
  
      qryProdLote.Close ;
      qryLoteProd.Close ;

      if not qryMaster.Active then
          exit ;

      PosicionaQuery(qryProdLote, qryMasternCdPedido.AsString) ;

      if qryProdlote.eof then
      begin
          ShowMessage('Nenhum produto com lote controlado ou nenhuma quantidade liberada para faturamento.') ;
      end ;

  end ;

end;

procedure TfrmLiberaFaturamento.DBComboProdutosChange(Sender: TObject);
begin
  inherited;

  qryLoteProd.Parameters.ParamByName('nCdPedido').Value := qryMasternCdPedido.Value ;
  PosicionaQuery(qryLoteProd, qryProdLotenCdProduto.AsString) ;

end;

procedure TfrmLiberaFaturamento.btSalvarClick(Sender: TObject);
var
  cQtdeLibFat : String;
begin

  if not qryMaster.Active then
      exit ;

  if (frmMenu.LeParametroEmpresa('LOTESAIDA') = 'S') then
  begin
      PosicionaQuery(qryProdLote, qryMasternCdPedido.AsString) ;

      qryProdLote.First;

      while not qryProdLote.Eof do
      begin

          qryAux.Close;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('SELECT Sum(nQtde) FROM LotePedido WHERE nCdPedido = ' + qryMasternCdPedido.asString + ' AND nCdProduto = ' + qryProdLotenCdProduto.AsString) ;
          qryAux.Open ;

          if qryAux.Eof or (qryAux.FieldList[0].Value <> qryProdLotenQtdeLibFat.Value) then
          begin
              qryAux.Close ;
              MensagemAlerta('O Total dos lotes de fabrica��o n�o confere com o total liberado.' + #13#13 + 'Item: ' + qryProdLotecNmProduto.Value) ;
              exit ;
          end ;

          qryProdLote.Next;
      end ;

  end ;

  case MessageDlg('Confirma as quantidades digitadas para faturamento ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo:exit ;
  end ;

  qryAux.Close ;
  qryAux.SQL.Clear ;
  qryAux.SQL.Add('SELECT TOP 1 nCdItemPedido FROM #Temp_ItemPedido WHERE nQtdeLibFat < nQtdePend') ;
  qryAux.Open ;

  if not qryAux.Eof then
  begin

      case MessageDlg('Alguns itens foram liberados parcialmente. Confirma ?',mtConfirmation,[mbYes,mbNo],0) of
          mrNo:exit ;
      end ;

  end ;

  qryAux.Close ;

  frmMenu.Connection.BeginTrans ;

  try
      qryItemPedido.First;

      while not qryItemPedido.Eof do
      begin
          cQtdeLibFat := StringReplace(qryItemPedidonQtdeLibFat.AsString,',','.',[rfReplaceAll, rfIgnoreCase]);

          { -- verifica se item do pedido possui saldo para libera��o do faturamento -- }
          { -- regra inclusa para evitar que pedidos liberados com saldos divergentes -- }
          qryAux.Close;
          qryAux.SQL.Clear;
          qryAux.SQL.Add('SELECT (nQtdePed - nQtdeExpRec - nQtdeCanc) - ' + cQtdeLibFat );
          qryAux.SQL.Add('  FROM ItemPedido                                            ');
          qryAux.SQL.Add(' WHERE nCdItemPedido = ' + qryItemPedidonCdItemPedido.AsString);
          qryAux.Open;

          if (qryAux.Fields[0].Value < 0) then
          begin
              Raise Exception.Create('Item ' + qryItemPedidonCdItemPedido.AsString + ' - ' + qryItemPedidocNmItem.Value
                                   + ' n�o possui saldo suficiente para libera��o no faturamento.'
                                   + ' Consulte o pedido novamente e efetue o processo de libera��o.');
          end;

          qryAux.Close ;
          qryAux.SQL.Clear ;
          qryAux.SQL.Add('UPDATE ItemPedido Set nQtdeLibFat = ' + cQtdeLibFat + ' WHERE nCdItemPedido = ' + qryItemPedidonCdItemPedido.AsString) ;
          qryAux.ExecSQL;

          qryItemPedido.Next;

      end ;

  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.');
      Raise;
  end;

  frmMenu.Connection.CommitTrans ;
  
  ShowMessage('Itens atualizados com sucesso.') ;

  inherited;

  qryProdLote.Close ;
  qryLoteProd.Close ;

  cxPageControl1.ActivePageIndex := 0 ;

  btCancelar.Click ;

end;

procedure TfrmLiberaFaturamento.qryMasterAfterCancel(DataSet: TDataSet);
begin
  inherited;

  qryItemPedido.Close ;
end;

initialization
    RegisterClass(TfrmLiberaFaturamento) ;

end.
