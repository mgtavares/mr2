unit fCalendarioCobrancaDiario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls, DB,
  ADODB, GridsEh, DBGridEh, Menus, cxPC, cxControls, StdCtrls,
  cxLookAndFeelPainters, cxButtons, Mask, DBCtrls, DBGridEhGrouping,
  ToolCtrlsEh;

type
  TfrmCalendarioCobrancaDiario = class(TfrmProcesso_Padrao)
    qryPreparaTemp: TADOQuery;
    qryPopulaTemp: TADOQuery;
    dsPopulaTemp: TDataSource;
    qryPopulaTempcNmEtapaCobranca: TStringField;
    qryPopulaTempnCdEtapaCobranca: TIntegerField;
    qryPopulaTempdDiaCobranca1: TDateTimeField;
    qryPopulaTempdDiaCobranca2: TDateTimeField;
    qryPopulaTempdDiaCobranca3: TDateTimeField;
    qryPopulaTempdDiaCobranca4: TDateTimeField;
    qryPopulaTempdDiaCobranca5: TDateTimeField;
    qryPopulaTempdDiaCobranca6: TDateTimeField;
    qryPopulaTempdDiaCobranca7: TDateTimeField;
    qryPopulaTempdDiaCobranca8: TDateTimeField;
    qryPopulaTempdDiaCobranca9: TDateTimeField;
    qryPopulaTempdDiaCobranca10: TDateTimeField;
    qryPopulaTempdDiaCobranca11: TDateTimeField;
    qryPopulaTempdDiaCobranca12: TDateTimeField;
    qryPopulaTempdDiaCobranca13: TDateTimeField;
    qryPopulaTempdDiaCobranca14: TDateTimeField;
    qryPopulaTempdDiaCobranca15: TDateTimeField;
    qryPopulaTempdDiaCobranca16: TDateTimeField;
    qryPopulaTempdDiaCobranca17: TDateTimeField;
    qryPopulaTempdDiaCobranca18: TDateTimeField;
    qryPopulaTempdDiaCobranca19: TDateTimeField;
    qryPopulaTempdDiaCobranca20: TDateTimeField;
    qryPopulaTempdDiaCobranca21: TDateTimeField;
    qryPopulaTempdDiaCobranca22: TDateTimeField;
    qryPopulaTempdDiaCobranca23: TDateTimeField;
    qryPopulaTempdDiaCobranca24: TDateTimeField;
    qryPopulaTempdDiaCobranca25: TDateTimeField;
    qryPopulaTempdDiaCobranca26: TDateTimeField;
    qryPopulaTempdDiaCobranca27: TDateTimeField;
    qryPopulaTempdDiaCobranca28: TDateTimeField;
    qryPopulaTempdDiaCobranca29: TDateTimeField;
    qryPopulaTempdDiaCobranca30: TDateTimeField;
    qryPopulaTempdDiaCobranca31: TDateTimeField;
    qryPopulaTempdDiaVencto1: TDateTimeField;
    qryPopulaTempdDiaVencto2: TDateTimeField;
    qryPopulaTempdDiaVencto3: TDateTimeField;
    qryPopulaTempdDiaVencto4: TDateTimeField;
    qryPopulaTempdDiaVencto5: TDateTimeField;
    qryPopulaTempdDiaVencto6: TDateTimeField;
    qryPopulaTempdDiaVencto7: TDateTimeField;
    qryPopulaTempdDiaVencto8: TDateTimeField;
    qryPopulaTempdDiaVencto9: TDateTimeField;
    qryPopulaTempdDiaVencto10: TDateTimeField;
    qryPopulaTempdDiaVencto11: TDateTimeField;
    qryPopulaTempdDiaVencto12: TDateTimeField;
    qryPopulaTempdDiaVencto13: TDateTimeField;
    qryPopulaTempdDiaVencto14: TDateTimeField;
    qryPopulaTempdDiaVencto15: TDateTimeField;
    qryPopulaTempdDiaVencto16: TDateTimeField;
    qryPopulaTempdDiaVencto17: TDateTimeField;
    qryPopulaTempdDiaVencto18: TDateTimeField;
    qryPopulaTempdDiaVencto19: TDateTimeField;
    qryPopulaTempdDiaVencto20: TDateTimeField;
    qryPopulaTempdDiaVencto21: TDateTimeField;
    qryPopulaTempdDiaVencto22: TDateTimeField;
    qryPopulaTempdDiaVencto23: TDateTimeField;
    qryPopulaTempdDiaVencto24: TDateTimeField;
    qryPopulaTempdDiaVencto25: TDateTimeField;
    qryPopulaTempdDiaVencto26: TDateTimeField;
    qryPopulaTempdDiaVencto27: TDateTimeField;
    qryPopulaTempdDiaVencto28: TDateTimeField;
    qryPopulaTempdDiaVencto29: TDateTimeField;
    qryPopulaTempdDiaVencto30: TDateTimeField;
    qryPopulaTempdDiaVencto31: TDateTimeField;
    qryDiaListaGeradaPQ: TADOQuery;
    qryDiaListaGeradaPQiDia: TIntegerField;
    qryDiaListaGeradaPQcFlgListaGerada: TIntegerField;
    qryDiaListaGeradaPQdDtCobranca: TDateTimeField;
    ToolButton4: TToolButton;
    btGerarLista: TToolButton;
    qryCronogramaCob: TADOQuery;
    qryCronogramaCobnCdCronogramaCob: TIntegerField;
    qryCronogramaCobnCdEmpresa: TIntegerField;
    qryCronogramaCobnCdLoja: TIntegerField;
    qryCronogramaCobiAnoMes: TIntegerField;
    qryCronogramaCobdDtGeracao: TDateTimeField;
    qryCronogramaCobnCdUsuarioGeracao: TIntegerField;
    qryGeraListaCobranca: TADOQuery;
    qryDiaListaGeradaSQ: TADOQuery;
    qryDiaListaGeradaSQiDia: TIntegerField;
    qryDiaListaGeradaSQcFlgListaGerada: TIntegerField;
    qryDiaListaGeradaSQdDtCobranca: TDateTimeField;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    DBGridEh1: TDBGridEh;
    cxTabSheet2: TcxTabSheet;
    DBGridEh2: TDBGridEh;
    qryDatasSegundaQuinz: TADOQuery;
    qryDatasSegundaQuinzcNmEtapaCobranca: TStringField;
    qryDatasSegundaQuinznCdEtapaCobranca: TIntegerField;
    qryDatasSegundaQuinzdDiaCobranca1: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca2: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca3: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca4: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca5: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca6: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca7: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca8: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca9: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca10: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca11: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca12: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca13: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca14: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca15: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca16: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca17: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca18: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca19: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca20: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca21: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca22: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca23: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca24: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca25: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca26: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca27: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca28: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca29: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca30: TDateTimeField;
    qryDatasSegundaQuinzdDiaCobranca31: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto1: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto2: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto3: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto4: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto5: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto6: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto7: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto8: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto9: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto10: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto11: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto12: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto13: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto14: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto15: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto16: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto17: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto18: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto19: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto20: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto21: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto22: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto23: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto24: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto25: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto26: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto27: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto28: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto29: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto30: TDateTimeField;
    qryDatasSegundaQuinzdDiaVencto31: TDateTimeField;
    dsDatasSegundaQuinz: TDataSource;
    ToolButton5: TToolButton;
    btExibeLista: TToolButton;
    qryDescobreCronogramaDia: TADOQuery;
    qryDescobreCronogramaDianCdCronogramaCobDia: TIntegerField;
    GroupBox1: TGroupBox;
    edtMesAno: TMaskEdit;
    Label3: TLabel;
    btExibeCronograma: TcxButton;
    qryLojaCobradora: TADOQuery;
    qryLojaCobradoranCdLoja: TIntegerField;
    qryLojaCobradoracNmLoja: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    dsLojaCobradora: TDataSource;
    DBEdit2: TDBEdit;
    SP_GERA_CRONOGRAMA_DIARIO_COBRANCA: TADOStoredProc;
    function diaSemana(dDia:TDateTime):string;
    procedure DBGridEh1CellClick(Column: TColumnEh);
    procedure btGerarListaClick(Sender: TObject);
    procedure DBGridEh2CellClick(Column: TColumnEh);
    procedure cxPageControl1Change(Sender: TObject);
    procedure btExibeListaClick(Sender: TObject);
    procedure btExibeCronogramaClick(Sender: TObject);
    function PrimeiroDiaMes ( dData : TDate ) : TDate;
    function AnoMes(dData : TDate) : integer;
    procedure edtMesAnoChange(Sender: TObject);
    procedure FormShow(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCalendarioCobrancaDiario: TfrmCalendarioCobrancaDiario;
  bGerarLista : boolean ;
  dDtCobranca,dDtVencto: TDateTime ;
  cGeraListaMaiorHoje:string;

implementation

uses fMenu, fAdmListaCobranca_Listas;

{$R *.dfm}

function TfrmCalendarioCobrancaDiario.diaSemana(dDia: TDateTime): string;
begin

    if (dDia = 0) then
    begin
        Result := ' ' ;
        exit ;
    end ;

    Case DayOfWeek(dDia) of
        1: Result := 'domingo' ;
        2: Result := 'segunda' ;
        3: Result := 'ter�a'   ;
        4: Result := 'quarta'  ;
        5: Result := 'quinta'  ;
        6: Result := 'sexta'   ;
        7: Result := 's�bado'  ;
    end ;

end;

procedure TfrmCalendarioCobrancaDiario.DBGridEh1CellClick(
  Column: TColumnEh);
begin
  inherited;

  dDtCobranca := 0;
  dDtVencto   := 0;

  btGerarLista.Enabled := False ;
  btExibeLista.Enabled := False ;

  if (Column.Title.Caption <> 'Etapa Cobran�a') then
  begin
      dDtCobranca := StrToDate(Copy(Column.Title.Caption,(Length(Column.Title.Caption)-9),10));
      dDtVencto   := StrToDate(Column.Field.Value);

      btGerarLista.Enabled := (Column.Title.Color <> clYellow) ;
      btExibeLista.Enabled := (Column.Title.Color = clYellow) ;
  end ;

end;

procedure TfrmCalendarioCobrancaDiario.btGerarListaClick(Sender: TObject);
begin
  inherited;

  if (MessageDlg('Confirma a gera��o da(s) lista(s) de cobran�a do dia ' + DateToStr(dDtCobranca) + ' (' + diaSemana(dDtCobranca) + ')  ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
      exit ;

  if (dDtCobranca > Date) then
  begin

      if (cGeraListaMaiorHoje = 'N') then
      begin
          MensagemErro('N�o � permitido gerar lista de cobran�as para data maior que hoje.') ;
          abort ;
      end ;

      if (MessageDlg('Esta data de cobran�a � maior que o dia de hoje. Tem certeza que deseja gerar a(s) lita(s) ?',mtConfirmation,[mbYes,mbNo],0) = MrNo) then
          exit ;
          
  end ;

  frmMenu.Connection.BeginTrans;

  try
      qryGeraListaCobranca.Close;
      qryGeraListaCobranca.Parameters.ParamByName('nCdCronogramaCob').Value := qryCronogramaCobnCdCronogramaCob.Value;
      qryGeraListaCobranca.Parameters.ParamByName('dDtVencto').Value        := DateToStr(dDtVencto);
      qryGeraListaCobranca.Parameters.ParamByName('dDtCobranca').Value      := DateToStr(dDtCobranca);
      qryGeraListaCobranca.Parameters.ParamByName('nCdUsuario').Value       := frmMenu.nCdUsuarioLogado;
      qryGeraListaCobranca.Parameters.ParamByName('nCdEmpresa').Value       := frmMenu.nCdEmpresaAtiva;
      qryGeraListaCobranca.Parameters.ParamByName('nCdLojaCobradora').Value := frmMenu.nCdLojaAtiva;
      qryGeraListaCobranca.ExecSQL ;
  except
      frmMenu.Connection.RollbackTrans;
      MensagemErro('Erro no processamento.') ;
      raise ;
  end ;

  frmMenu.Connection.CommitTrans;

  ShowMessage('Lista(s) gerada(s) com sucesso.') ;

  btExibeCronograma.Click;

end;

procedure TfrmCalendarioCobrancaDiario.DBGridEh2CellClick(
  Column: TColumnEh);
begin
  inherited;

  dDtCobranca := 0;
  dDtVencto   := 0;

  btGerarLista.Enabled := False ;
  btExibeLista.Enabled := False ;

  if (Column.Title.Caption <> 'Etapa Cobran�a') then
  begin
      dDtCobranca := StrToDate(Copy(Column.Title.Caption,(Length(Column.Title.Caption)-9),10));
      dDtVencto   := StrToDate(Column.Field.Value);
      btGerarLista.Enabled := (Column.Title.Color <> clYellow) ;
      btExibeLista.Enabled := (Column.Title.Color = clYellow) ;
  end ;

end;

procedure TfrmCalendarioCobrancaDiario.cxPageControl1Change(
  Sender: TObject);
begin
  inherited;

  btGerarLista.Enabled := False ;
  btExibeLista.Enabled := False ;
end;

procedure TfrmCalendarioCobrancaDiario.btExibeListaClick(Sender: TObject);
var
  objForm : TfrmAdmListaCobranca_Listas;
begin
  inherited;

  qryDescobreCronogramaDia.Close;
  qryDescobreCronogramaDia.Parameters.ParamByName('nCdCronogramaCob').Value := qryCronogramaCobnCdCronogramaCob.Value;
  qryDescobreCronogramaDia.Parameters.ParamByName('dDtCobranca').Value      := DateToStr(dDtCobranca) ;
  qryDescobreCronogramaDia.Open ;

  if qryDescobreCronogramaDia.Eof then
  begin
      MensagemErro('As listas de cobran�a desse dia n�o foram geradas.') ;
      abort ;
  end ;

  objForm := TfrmAdmListaCobranca_Listas.Create(nil);

  objForm.qryLista.Close;
  objForm.qryLista.Parameters.ParamByName('nCdEmpresa').Value              := frmMenu.nCdEmpresaAtiva;
  objForm.qryLista.Parameters.ParamByName('nCdUsuarioResp').Value          := 0;
  objForm.qryLista.Parameters.ParamByName('nCdListaCobranca').Value        := 0;
  objForm.qryLista.Parameters.ParamByName('cFlgSomenteNaoEncerrada').Value := 0;
  objForm.qryLista.Parameters.ParamByName('cFlgSomenteNaoAtrib').Value     := 0;
  objForm.qryLista.Parameters.ParamByName('nCdCronogramaCob').Value        := qryCronogramaCobnCdCronogramaCob.Value;
  objForm.qryLista.Parameters.ParamByName('dDtCobranca').Value             := DateToStr(dDtCobranca) ;
  objForm.qryLista.Parameters.ParamByName('dDtCobrancaFim').Value          := DateToStr(dDtCobranca) ;
  objForm.qryLista.Parameters.ParamByName('nCdLoja').Value                 := frmMenu.nCdLojaAtiva;
  objForm.qryLista.Open;

  showForm(objForm,true) ;
end;

procedure TfrmCalendarioCobrancaDiario.btExibeCronogramaClick(Sender: TObject);
var
    i : integer ;
begin

  if (trim(edtMesAno.Text) = '/') then
  begin
      edtMesAno.Text := Copy(DateToStr(Date),4,7) ;
  end ;

  edtMesAno.Text := frmMenu.ZeroEsquerda(edtMesAno.Text,6) ;

  try
      StrToDate('01/' + edtMesAno.Text) ;
  except
      MensagemErro('M�s/Ano inv�lido.'+#13#13+'Utilize: mm/aaaa') ;
      edtMesAno.SetFocus;
      abort ;
  end ;

  dDtCobranca := 0;
  dDtVencto   := 0;

  btGerarLista.Enabled := False ;
  btExibeLista.Enabled := False ;

  qryCronogramaCob.Close;
  qryCronogramaCob.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
  qryCronogramaCob.Parameters.ParamByName('nCdLoja').Value    := frmmenu.nCdLojaAtiva;
  qryCronogramaCob.Parameters.ParamByName('iAnoMes').Value    := AnoMes(StrToDate('01/' + edtMesAno.Text)) ;
  qryCronogramaCob.Open;

  if (qryCronogramaCob.Eof) then
  begin
      MensagemAlerta('Nenhum cronograma encontrado para esse m�s/ano.') ;

      {--Se o mes/ano digitado for o m�s corrente ou um m�s posterior, pergunta se quer gerar o cronograma --}
      if (strToDate('01/' + edtMesAno.Text) >= PrimeiroDiaMes(Date)) then
      begin
          if (MessageDlg('Deseja gerar o cronograma de cobran�a para esse m�s/ano?',mtConfirmation,[mbYes,mbNo],0)=MrNo) then
              exit ;

          frmMenu.Connection.BeginTrans;

          try
              SP_GERA_CRONOGRAMA_DIARIO_COBRANCA.Close;
              SP_GERA_CRONOGRAMA_DIARIO_COBRANCA.Parameters.ParamByName('@nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
              SP_GERA_CRONOGRAMA_DIARIO_COBRANCA.Parameters.ParamByName('@nCdLoja').Value    := frmMenu.nCdLojaAtiva;
              SP_GERA_CRONOGRAMA_DIARIO_COBRANCA.Parameters.ParamByName('@iAnoMes').Value    := AnoMes(StrToDate('01/' + edtMesAno.Text)) ;
              SP_GERA_CRONOGRAMA_DIARIO_COBRANCA.Parameters.ParamByName('@nCdUsuario').Value := frmMenu.nCdUsuarioLogado;
              SP_GERA_CRONOGRAMA_DIARIO_COBRANCA.ExecProc;
          except
              frmMenu.Connection.RollbackTrans;
              MensagemErro('Erro no processamento.') ;
              raise ;
          end ;

          frmMenu.Connection.CommitTrans;
          ShowMessage('Cronograma gerado com sucesso!') ;

          qryCronogramaCob.Close;
          qryCronogramaCob.Parameters.ParamByName('nCdEmpresa').Value := frmMenu.nCdEmpresaAtiva;
          qryCronogramaCob.Parameters.ParamByName('nCdLoja').Value    := frmmenu.nCdLojaAtiva;
          qryCronogramaCob.Parameters.ParamByName('iAnoMes').Value    := AnoMes(StrToDate('01/' + edtMesAno.Text)) ;
          qryCronogramaCob.Open;
          
      end
      else abort ;
  end ;

  btExibeCronograma.Caption := 'Aguarde...' ;

  qryPreparaTemp.ExecSQL;
  qryPopulaTemp.Close;
  qryPopulaTemp.Parameters.ParamByName('nCdCronogramaCob').Value := qryCronogramaCobnCdCronogramaCob.Value;
  qryPopulaTemp.Open;

  qryDatasSegundaQuinz.Close;
  qryDatasSegundaQuinz.Open ;

  qryDatasSegundaQuinz.DisableControls;
  qryPopulaTemp.DisableControls;

  DBGridEh1.Visible := False ;
  DBGridEh2.Visible := False ;

  qryPopulaTemp.FieldList.Update;

  qryDiaListaGeradaPQ.Close;
  PosicionaQuery(qryDiaListaGeradaPQ,qryCronogramaCobnCdCronogramaCob.AsString) ;

  qryDiaListaGeradaSQ.Close;
  PosicionaQuery(qryDiaListaGeradaSQ,qryCronogramaCobnCdCronogramaCob.AsString) ;

  DBGridEh1.Columns.AddAllColumns(True);
  DBGridEh2.Columns.AddAllColumns(True);

  if qryPopulaTemp.Eof then
  begin
      btExibeCronograma.Caption := 'Exibir Cronograma' ;
      exit ;
  end ;

  {--Trata o grid da primeira quinzena--}

  DBGridEh1.Align                    := alTop ;
  DBGridEh1.Columns[0].Width         := 160 ;
  DBGridEh1.Columns[0].Title.Caption := 'Etapa Cobran�a' ;

  for i := 1 to 32 do
      DBGridEh1.Columns[i].Visible := False ;

  DBGridEh1.Columns[33].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca1.Value)  + ' ' + Copy(qryPopulaTempdDiaCobranca1.asString,1,10) ;
  DBGridEh1.Columns[34].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca2.Value)  + ' ' + Copy(qryPopulaTempdDiaCobranca2.asString,1,10) ;
  DBGridEh1.Columns[35].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca3.Value)  + ' ' + Copy(qryPopulaTempdDiaCobranca3.asString,1,10) ;
  DBGridEh1.Columns[36].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca4.Value)  + ' ' + Copy(qryPopulaTempdDiaCobranca4.asString,1,10) ;
  DBGridEh1.Columns[37].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca5.Value)  + ' ' + Copy(qryPopulaTempdDiaCobranca5.asString,1,10) ;
  DBGridEh1.Columns[38].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca6.Value)  + ' ' + Copy(qryPopulaTempdDiaCobranca6.asString,1,10) ;
  DBGridEh1.Columns[39].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca7.Value)  + ' ' + Copy(qryPopulaTempdDiaCobranca7.asString,1,10) ;
  DBGridEh1.Columns[40].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca8.Value)  + ' ' + Copy(qryPopulaTempdDiaCobranca8.asString,1,10) ;
  DBGridEh1.Columns[41].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca9.Value)  + ' ' + Copy(qryPopulaTempdDiaCobranca9.asString,1,10) ;
  DBGridEh1.Columns[42].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca10.Value) + ' ' + Copy(qryPopulaTempdDiaCobranca10.asString,1,10) ;
  DBGridEh1.Columns[43].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca11.Value) + ' ' + Copy(qryPopulaTempdDiaCobranca11.asString,1,10) ;
  DBGridEh1.Columns[44].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca12.Value) + ' ' + Copy(qryPopulaTempdDiaCobranca12.asString,1,10) ;
  DBGridEh1.Columns[45].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca13.Value) + ' ' + Copy(qryPopulaTempdDiaCobranca13.asString,1,10) ;
  DBGridEh1.Columns[46].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca14.Value) + ' ' + Copy(qryPopulaTempdDiaCobranca14.asString,1,10) ;
  DBGridEh1.Columns[47].Title.Caption := diaSemana(qryPopulaTempdDiaCobranca15.Value) + ' ' + Copy(qryPopulaTempdDiaCobranca15.asString,1,10) ;

  {--Padroniza as colunas com 76 de largura --}
  for i := 33 to 47 do
      DBGridEh1.Columns[i].Width := 76 ;

  {--Padroniza o texto das t�tulos com branco --}
  for i := 0 to 47 do
      DBGridEh1.Columns[i].Title.Font.Color := clBlack ;

  {--Padroniza o texto das colunas com Azul --}
  for i := 0 to 47 do
      DBGridEh1.Columns[i].Font.Color := clBlue ;

  {--Oculta as colunas indesejadas--}
  for i := 48 to 63 do
      DBGridEh1.Columns[i].Visible := False ;

  {--Colori a coluna da etapa da cobran�a--}
  DBGridEh1.Columns[0].Color  := $00CCF8FF ;

  {--Intercala as colunas--}
  DBGridEh1.Columns[33].Color := $00DADADA ;
  DBGridEh1.Columns[35].Color := $00DADADA ;
  DBGridEh1.Columns[37].Color := $00DADADA ;
  DBGridEh1.Columns[39].Color := $00DADADA ;
  DBGridEh1.Columns[41].Color := $00DADADA ;
  DBGridEh1.Columns[43].Color := $00DADADA ;
  DBGridEh1.Columns[45].Color := $00DADADA ;
  DBGridEh1.Columns[47].Color := $00DADADA ;

  {--Colori o t�tulo das colunas onde a data ja teve a lista gerada--}

  qryDiaListaGeradaPQ.First ;

  while not qryDiaListaGeradaPQ.Eof do
  begin
      {-- se a lista do dia foi gerada, pinta de amarelo o cabe�alho --}
      {-- 32 � o c�digo da coluna antes da primeira coluna de dias --}
      if (qryDiaListaGeradaPQcFlgListaGerada.Value = 1) then
      begin
          DBGridEh1.Columns[32+qryDiaListaGeradaPQiDia.Value].Title.Color      := clYellow ;
          DBGridEh1.Columns[32+qryDiaListaGeradaPQiDia.Value].Title.Font.Color := clBlack ;
      end ;

      {-- Se for final de semana, pinta a coluna com outra cor --}
      if    (DayOfWeek(qryDiaListaGeradaPQdDtCobranca.Value) = 7)
         or (DayOfWeek(qryDiaListaGeradaPQdDtCobranca.Value) = 1) then
      begin
          DBGridEh1.Columns[32+qryDiaListaGeradaPQiDia.Value].Color      := clSilver ;
          DBGridEh1.Columns[32+qryDiaListaGeradaPQiDia.Value].Font.Color := clBlack ;
      end ;

      qryDiaListaGeradaPQ.Next ;
  end ;

  qryDiaListaGeradaPQ.First ;

  {--Trata o grid da segunda quinzena--}

  DBGridEh2.Align                    := alTop ;
  DBGridEh2.Columns[0].Width         := 160 ;
  DBGridEh2.Columns[0].Title.Caption := 'Etapa Cobran�a' ;

  {--Oculta as colunas indesejadas--}
  for i := 1 to 47 do
      DBGridEh2.Columns[i].Visible := False ;

  DBGridEh2.Columns[48].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca16.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca16.asString,1,10) ;
  DBGridEh2.Columns[49].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca17.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca17.asString,1,10) ;
  DBGridEh2.Columns[50].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca18.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca18.asString,1,10) ;
  DBGridEh2.Columns[51].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca19.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca19.asString,1,10) ;
  DBGridEh2.Columns[52].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca20.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca20.asString,1,10) ;
  DBGridEh2.Columns[53].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca21.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca21.asString,1,10) ;
  DBGridEh2.Columns[54].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca22.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca22.asString,1,10) ;
  DBGridEh2.Columns[55].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca23.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca23.asString,1,10) ;
  DBGridEh2.Columns[56].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca24.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca24.asString,1,10) ;
  DBGridEh2.Columns[57].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca25.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca25.asString,1,10) ;
  DBGridEh2.Columns[58].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca26.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca26.asString,1,10) ;
  DBGridEh2.Columns[59].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca27.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca27.asString,1,10) ;
  DBGridEh2.Columns[60].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca28.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca28.asString,1,10) ;
  DBGridEh2.Columns[61].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca29.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca29.asString,1,10) ;
  DBGridEh2.Columns[62].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca30.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca30.asString,1,10) ;
  DBGridEh2.Columns[63].Title.Caption := diaSemana(qryDatasSegundaQuinzdDiaCobranca31.Value) + ' ' + Copy(qryDatasSegundaQuinzdDiaCobranca31.asString,1,10) ;

  {--Padroniza as colunas com 76 de largura --}
  for i := 48 to 63 do
      DBGridEh2.Columns[i].Width := 76 ;

  {--Padroniza o texto das t�tulos com branco --}
  for i := 0 to 63 do
      DBGridEh2.Columns[i].Title.Font.Color := clBlack ;

  {--Padroniza o texto das colunas com Azul --}
  for i := 0 to 63 do
      DBGridEh2.Columns[i].Font.Color := clBlue ;

  {--Colori a coluna da etapa da cobran�a--}
  DBGridEh2.Columns[0].Color  := $00CCF8FF ;

  {--Intercala as colunas--}
  DBGridEh2.Columns[48].Color := $00DADADA ;
  DBGridEh2.Columns[50].Color := $00DADADA ;
  DBGridEh2.Columns[52].Color := $00DADADA ;
  DBGridEh2.Columns[54].Color := $00DADADA ;
  DBGridEh2.Columns[56].Color := $00DADADA ;
  DBGridEh2.Columns[58].Color := $00DADADA ;
  DBGridEh2.Columns[60].Color := $00DADADA ;
  DBGridEh2.Columns[62].Color := $00DADADA ;

  {--Colori o t�tulo das colunas onde a data ja teve a lista gerada--}

  qryDiaListaGeradaSQ.First ;

  while not qryDiaListaGeradaSQ.Eof do
  begin
      {-- se a lista do dia foi gerada, pinta de amarelo o cabe�alho --}
      {-- 32 � o c�digo da coluna antes da primeira coluna de dias--}
      if (qryDiaListaGeradaSQcFlgListaGerada.Value = 1) then
      begin
          DBGridEh2.Columns[32+qryDiaListaGeradaSQiDia.Value].Title.Color      := clYellow ;
          DBGridEh2.Columns[32+qryDiaListaGeradaSQiDia.Value].Title.Font.Color := clBlack ;
      end ;

      {-- Se for final de semana, pinta a coluna com outra cor --}
      if    (DayOfWeek(qryDiaListaGeradaSQdDtCobranca.Value) = 7)
         or (DayOfWeek(qryDiaListaGeradaSQdDtCobranca.Value) = 1) then
      begin
          DBGridEh2.Columns[32+qryDiaListaGeradaSQiDia.Value].Color      := clSilver ;
          DBGridEh2.Columns[32+qryDiaListaGeradaSQiDia.Value].Font.Color := clBlack ;
      end ;

      qryDiaListaGeradaSQ.Next ;
  end ;

  qryDiaListaGeradaSQ.First ;

  {-- Oculta as colunas sem Title, que indica que o m�s n�o tem aquele dia, exemplo, fevereiro, s� at� dia 28 --}
  for i := 0 to 63 do
      if (trim(DBGridEh2.Columns[i].Title.Caption) = '') then
          DBGridEh2.Columns[i].Visible := False ;


  qryPopulaTemp.EnableControls;
  qryDatasSegundaQuinz.EnableControls;

  DBGridEh1.Visible := True ;
  DBGridEh2.Visible := True ;

  btExibeCronograma.Caption := 'Exibir Cronograma' ;


end;

function TfrmCalendarioCobrancaDiario.PrimeiroDiaMes(dData: TDate): TDate;
var
  dia , mes , ano : Word ;
begin
  Decodedate ( dData , ano , mes , dia );
  Result := Encodedate ( ano , mes , 01 );

end;

function TfrmCalendarioCobrancaDiario.AnoMes(dData: TDate): integer;
var
  dia , mes , ano : Word ;
begin
  Decodedate ( dData , ano , mes , dia );
  Result := strToint(intTostr(Ano) + frmMenu.ZeroEsquerda(intTostr(Mes),2));
end;

procedure TfrmCalendarioCobrancaDiario.edtMesAnoChange(Sender: TObject);
var
    i : integer ;
begin
  inherited;

  qryPopulaTemp.Close ;
  qryDatasSegundaQuinz.Close ;

  if (DBGridEh1.Columns.Count > 1) then
      for i := 1 to DBGridEh1.Columns.Count do
          DBGridEh1.Columns[i-1].Visible := false ;

  if (DBGridEh2.Columns.Count > 1) then
      for i := 1 to DBGridEh2.Columns.Count do
          DBGridEh2.Columns[i-1].Visible := false ;

end;

procedure TfrmCalendarioCobrancaDiario.FormShow(Sender: TObject);
begin
  inherited;

  cxPageControl1.ActivePageIndex := 0 ;
  cGeraListaMaiorHoje := frmMenu.LeParametro('GRLISCROMAIORHJ');

  PosicionaQuery(qryLojaCobradora, intToStr(frmMenu.nCdLojaAtiva)) ;
  
end;

initialization
    RegisterClass(TfrmCalendarioCobrancaDiario) ;

end.
