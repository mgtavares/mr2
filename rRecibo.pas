unit rRecibo;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, DBCtrls, StdCtrls, Mask, ImgList, ComCtrls,
  ToolWin, ExtCtrls, DB, ADODB;

type
  TrptRecibo = class(TfrmRelatorio_Padrao)
    Label1: TLabel;
    MaskEdit3: TMaskEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    Label6: TLabel;
    Label3: TLabel;
    qryEmpresa: TADOQuery;
    qryEmpresanCdEmpresa: TIntegerField;
    qryEmpresacSigla: TStringField;
    qryEmpresacNmEmpresa: TStringField;
    DataSource1: TDataSource;
    qryTerceiro: TADOQuery;
    qryTerceironCdTerceiro: TIntegerField;
    qryTerceirocCNPJCPF: TStringField;
    qryTerceirocNmTerceiro: TStringField;
    MaskEdit4: TMaskEdit;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DataSource2: TDataSource;
    DBEdit4: TDBEdit;
    MaskEdit6: TMaskEdit;
    Label4: TLabel;
    qryEspTit: TADOQuery;
    qryEspTitnCdEspTit: TIntegerField;
    qryEspTitnCdGrupoEspTit: TIntegerField;
    qryEspTitcNmEspTit: TStringField;
    qryEspTitcTipoMov: TStringField;
    qryEspTitcFlgPrev: TIntegerField;
    DBEdit5: TDBEdit;
    DataSource3: TDataSource;
    MaskEdit7: TMaskEdit;
    Label5: TLabel;
    qryTipoParcela: TADOQuery;
    qryTipoParcelanCdTabTipoParcContratoEmpImobiliario: TIntegerField;
    qryTipoParcelacNmTabTipoParcContratoEmpImobiliario: TStringField;
    DBEdit6: TDBEdit;
    DataSource4: TDataSource;
    usp_CorrecaoMonetaria: TADOStoredProc;
    procedure MaskEdit3Exit(Sender: TObject);
    procedure MaskEdit3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure ToolButton1Click(Sender: TObject);
    procedure MaskEdit4KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit4Exit(Sender: TObject);
    procedure MaskEdit6Exit(Sender: TObject);
    procedure MaskEdit6KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure MaskEdit7Exit(Sender: TObject);
    procedure MaskEdit7KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  rptRecibo: TrptRecibo;

implementation

uses fMenu, fLookup_Padrao, rReciboView;

{$R *.dfm}

procedure TrptRecibo.MaskEdit3Exit(Sender: TObject);
begin
  inherited;
  qryEmpresa.Close ;

  If (Trim(MaskEdit3.Text) <> '') then
  begin

    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := MaskEdit3.Text ;
    qryEmpresa.Open ;
  end ;

end;

procedure TrptRecibo.MaskEdit3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(8);

        If (nPK > 0) then
        begin
            MaskEdit3.Text := IntToStr(nPK) ;

            qryEmpresa.Close ;
            qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
            qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := nPK ;
            qryEmpresa.Open ;
        end ;

    end ;

  end ;

end;
procedure TrptRecibo.FormShow(Sender: TObject);
var
  iAux : Integer ;
begin
  inherited;

  iAux := frmMenu.nCdEmpresaAtiva;

  if (iAux = -1) then
  begin
      ShowMessage('Nenhuma empresa vinculada para este usu�rio.') ;
      close ;
  end ;

  if (iAux > 0) then
  begin

    MaskEdit3.Text := IntToStr(iAux) ;

    qryEmpresa.Close ;
    qryEmpresa.Parameters.ParamByName('nCdUsuario').Value := frmMenu.nCdUsuarioLogado ;
    qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value := iAux ;
    qryEmpresa.Open ;

  end ;

end;

procedure TrptRecibo.ToolButton1Click(Sender: TObject);
var
  objRel : TrptReciboView ;
begin
  inherited;

  objRel := TrptReciboView.Create(nil) ;

  objRel.qryEmpresa.Close;
  objRel.qryEmpresa.Parameters.ParamByName('nCdEmpresa').Value  := frmMenu.nCdEmpresaAtiva ;
  objRel.qryEmpresa.Open;

  objRel.qryTerceiro.Close;
  objRel.qryTerceiro.Parameters.ParamByName('nCdTerceiroEmpresa').Value  := objRel.qryEmpresanCdTerceiroEmp.Value ;
  objRel.qryTerceiro.Open;

  objRel.qryEndereco.Close;
  objRel.qryEndereco.Parameters.ParamByName('nCdTerceiro').Value  := objRel.qryTerceironCdTerceiro.Value ;
  objRel.qryEndereco.Open;

  {-- chama a procedure --}
  usp_CorrecaoMonetaria.Close;
  usp_CorrecaoMonetaria.Parameters.ParamByName('@cSenso').Value         := 'C' ;
  usp_CorrecaoMonetaria.Parameters.ParamByName('@nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva ;
  usp_CorrecaoMonetaria.Parameters.ParamByName('@dDtInicial').Value     := frmMenu.ConvData(MaskEdit1.Text) ;
  usp_CorrecaoMonetaria.Parameters.ParamByName('@dDtFinal').Value       := frmMenu.ConvData(MaskEdit2.Text) ;
  usp_CorrecaoMonetaria.Parameters.ParamByName('@nCdTerceiro').Value    := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  usp_CorrecaoMonetaria.Parameters.ParamByName('@nCdTitulo').Value      := 0; //frmMenu.ConvInteiro(MaskEdit5.Text) ;
  usp_CorrecaoMonetaria.Parameters.ParamByName('@nCdEspTit').Value      := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  usp_CorrecaoMonetaria.Parameters.ParamByName('@nCdTipoParcela').Value := frmMenu.ConvInteiro(MaskEdit7.Text) ;
  usp_CorrecaoMonetaria.ExecProc;

  objRel.usp_Relatorio.Close;
  objRel.usp_Relatorio.Parameters.ParamByName('@cSenso').Value         := 'C' ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEmpresa').Value     := frmMenu.nCdEmpresaAtiva ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtInicial').Value     := frmMenu.ConvData(MaskEdit1.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@dDtFinal').Value       := frmMenu.ConvData(MaskEdit2.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdTerceiro').Value    := frmMenu.ConvInteiro(MaskEdit4.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdTitulo').Value      := 0; //frmMenu.ConvInteiro(MaskEdit5.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdEspTit').Value      := frmMenu.ConvInteiro(MaskEdit6.Text) ;
  objRel.usp_Relatorio.Parameters.ParamByName('@nCdTipoParcela').Value := frmMenu.ConvInteiro(MaskEdit7.Text) ;
  objRel.usp_Relatorio.Open;

  try

      if (objRel.usp_Relatorio.Eof) then
      begin
          MensagemAlerta('Nenhum recibo encontrado para impress�o.');
          abort ;
      end ;

      try
         {--visualiza o relat�rio--}

          objRel.QuickRep1.PreviewModal;

      except
          MensagemErro('Erro na cria��o do relat�rio');
          raise;
      end;

  finally
      FreeAndNil(objRel);
  end;

end;

procedure TrptRecibo.MaskEdit4KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;
  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(17);

        If (nPK > 0) then
        begin
            MaskEdit4.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptRecibo.MaskEdit4Exit(Sender: TObject);
begin
  inherited;
  qryTerceiro.Close ;

  If (Trim(MaskEdit4.Text) <> '') then
  begin

    qryTerceiro.Parameters.ParamByName('nCdTerceiro').Value := MaskEdit4.Text ;
    qryTerceiro.Open ;
  end ;

end;

procedure TrptRecibo.MaskEdit6Exit(Sender: TObject);
begin
  inherited;
  qryEspTit.Close ;

  If (Trim(MaskEdit6.Text) <> '') then
  begin

    qryEspTit.Parameters.ParamByName('nCdEspTit').Value := MaskEdit6.Text ;
    qryEspTit.Open ;
  end ;

end;

procedure TrptRecibo.MaskEdit6KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
    vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(10);

        If (nPK > 0) then
        begin
            Maskedit6.Text := IntToStr(nPK) ;
        end ;

    end ;

  end ;

end;

procedure TrptRecibo.MaskEdit7Exit(Sender: TObject);
begin
  inherited;
  qryTipoParcela.Close ;

  If (Trim(MaskEdit7.Text) <> '') then
  begin
    qryTipoParcela.Parameters.ParamByName('nPK').Value := MaskEdit7.Text ;
    qryTipoParcela.Open ;
  end ;

end;

procedure TrptRecibo.MaskEdit7KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
    nPK : Integer ;
begin
  inherited;

  case key of
   vk_F4 : begin

        nPK := frmLookup_Padrao.ExecutaConsulta(1000);

        If (nPK > 0) then
        begin
            Maskedit7.Text := IntToStr(nPK) ;
        end ;

    end ;

 end ;

end;

initialization
     RegisterClass(TrptRecibo) ;

end.
