unit fOscilacaoEstoque_View;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fRelatorio_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, ADODB, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxControls,
  cxGridCustomView, cxGrid;

type
  TfrmOscilacaoEstoque_View = class(TfrmRelatorio_Padrao)
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1cNmGrupoProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cNmProduto: TcxGridDBColumn;
    cxGrid1DBTableView1cUnidadeMedida: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoAnt: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeEnt: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeSai: TcxGridDBColumn;
    cxGrid1DBTableView1nSaldoPos: TcxGridDBColumn;
    cxGrid1DBTableView1nQtdeOsc: TcxGridDBColumn;
    cxGrid1DBTableView1nValorAnt: TcxGridDBColumn;
    cxGrid1DBTableView1nValorEnt: TcxGridDBColumn;
    cxGrid1DBTableView1nValorSai: TcxGridDBColumn;
    cxGrid1DBTableView1nValorPos: TcxGridDBColumn;
    cxGrid1DBTableView1nValorOsc: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    uspRelatorio: TADOStoredProc;
    uspRelatorionCdGrupoProduto: TIntegerField;
    uspRelatoriocNmGrupoProduto: TStringField;
    uspRelatorionCdProduto: TIntegerField;
    uspRelatoriocNmProduto: TStringField;
    uspRelatoriocUnidadeMedida: TStringField;
    uspRelatorionSaldoAnt: TBCDField;
    uspRelatorionQtdeEnt: TBCDField;
    uspRelatorionQtdeSai: TBCDField;
    uspRelatorionSaldoPos: TBCDField;
    uspRelatorionQtdeOsc: TBCDField;
    uspRelatorionValorAnt: TBCDField;
    uspRelatorionValorEnt: TBCDField;
    uspRelatorionValorSai: TBCDField;
    uspRelatorionValorPos: TBCDField;
    uspRelatorionValorOsc: TBCDField;
    uspRelatorionValCusto: TBCDField;
    uspRelatorionValMedio: TBCDField;
    uspRelatoriocNmDepartamento: TStringField;
    uspRelatoriocNmCategoria: TStringField;
    uspRelatoriocNmSubCategoria: TStringField;
    DataSource1: TDataSource;
    uspRelatorionCdLocalEstoque: TIntegerField;
    uspRelatoriocNmLocalEstoque: TStringField;
    cxGrid1DBTableView1cNmLocalEstoque: TcxGridDBColumn;
    cxGrid1DBTableView1cNmDepartamento: TcxGridDBColumn;
    cxGrid1DBTableView1cNmCategoria: TcxGridDBColumn;
    cxGrid1DBTableView1cNmSubCategoria: TcxGridDBColumn;
    uspRelatorionValPrecoUltRec: TBCDField;
    uspRelatoriodDtUltReceb: TDateTimeField;
    cxGrid1DBTableView1nValPrecoUltRec: TcxGridDBColumn;
    cxGrid1DBTableView1dDtUltReceb: TcxGridDBColumn;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOscilacaoEstoque_View: TfrmOscilacaoEstoque_View;

implementation

{$R *.dfm}

end.
