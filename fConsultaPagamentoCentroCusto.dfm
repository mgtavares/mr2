inherited frmConsultaPagamentoCentroCusto: TfrmConsultaPagamentoCentroCusto
  Left = -8
  Top = -8
  Width = 1168
  Height = 850
  Caption = 'frmConsultaPagamentoCentroCusto'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1152
    Height = 785
  end
  object Label6: TLabel [1]
    Left = 187
    Top = 194
    Width = 16
    Height = 13
    Alignment = taRightJustify
    Caption = 'at'#233
  end
  object Label4: TLabel [2]
    Left = 63
    Top = 44
    Width = 41
    Height = 13
    Alignment = taRightJustify
    Caption = 'Empresa'
  end
  object Label9: TLabel [3]
    Left = 84
    Top = 69
    Width = 20
    Height = 13
    Alignment = taRightJustify
    Caption = 'Loja'
  end
  object Label1: TLabel [4]
    Left = 5
    Top = 94
    Width = 99
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro Custo N'#237'vel 1'
  end
  object Label2: TLabel [5]
    Left = 5
    Top = 119
    Width = 99
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro Custo N'#237'vel 2'
  end
  object Label3: TLabel [6]
    Left = 5
    Top = 144
    Width = 99
    Height = 13
    Alignment = taRightJustify
    Caption = 'Centro Custo N'#237'vel 3'
  end
  object Label5: TLabel [7]
    Left = 65
    Top = 169
    Width = 39
    Height = 13
    Alignment = taRightJustify
    Caption = 'Terceiro'
  end
  object Label7: TLabel [8]
    Left = 11
    Top = 194
    Width = 93
    Height = 13
    Alignment = taRightJustify
    Caption = 'Per'#237'odo Pagamento'
  end
  inherited ToolBar1: TToolBar
    Width = 1152
    inherited ToolButton1: TToolButton
      OnClick = ToolButton1Click
    end
  end
  object MaskEdit4: TMaskEdit [10]
    Left = 106
    Top = 36
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 1
    Text = '      '
    OnExit = MaskEdit4Exit
    OnKeyDown = MaskEdit4KeyDown
  end
  object DBEdit13: TDBEdit [11]
    Tag = 1
    Left = 176
    Top = 36
    Width = 59
    Height = 21
    DataField = 'cSigla'
    DataSource = dsEmpresa
    TabOrder = 2
  end
  object DBEdit14: TDBEdit [12]
    Tag = 1
    Left = 240
    Top = 36
    Width = 648
    Height = 21
    DataField = 'cNmEmpresa'
    DataSource = dsEmpresa
    TabOrder = 3
  end
  object MaskEdit8: TMaskEdit [13]
    Left = 106
    Top = 61
    Width = 65
    Height = 21
    TabOrder = 4
    OnExit = MaskEdit8Exit
    OnKeyDown = MaskEdit8KeyDown
  end
  object DBEdit1: TDBEdit [14]
    Tag = 1
    Left = 176
    Top = 61
    Width = 648
    Height = 21
    DataField = 'cNmLoja'
    DataSource = dsLoja
    TabOrder = 5
  end
  object MaskEdit5: TMaskEdit [15]
    Left = 106
    Top = 86
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 6
    Text = '      '
    OnExit = MaskEdit5Exit
    OnKeyDown = MaskEdit5KeyDown
  end
  object DBEdit11: TDBEdit [16]
    Tag = 1
    Left = 176
    Top = 86
    Width = 314
    Height = 21
    DataField = 'cNmCC'
    DataSource = dsCentroCustoNivel1
    TabOrder = 7
  end
  object MaskEdit7: TMaskEdit [17]
    Left = 106
    Top = 111
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 8
    Text = '      '
    OnExit = MaskEdit7Exit
    OnKeyDown = MaskEdit7KeyDown
  end
  object DBEdit4: TDBEdit [18]
    Tag = 1
    Left = 176
    Top = 111
    Width = 60
    Height = 21
    DataField = 'cCdCC'
    DataSource = dsCentroCustoNivel2
    TabOrder = 9
  end
  object DBEdit12: TDBEdit [19]
    Tag = 1
    Left = 241
    Top = 111
    Width = 316
    Height = 21
    DataField = 'cNmCC'
    DataSource = dsCentroCustoNivel2
    TabOrder = 10
  end
  object MaskEdit3: TMaskEdit [20]
    Left = 106
    Top = 136
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 11
    Text = '      '
    OnExit = MaskEdit3Exit
    OnKeyDown = MaskEdit3KeyDown
  end
  object DBEdit2: TDBEdit [21]
    Tag = 1
    Left = 176
    Top = 136
    Width = 94
    Height = 21
    DataField = 'cCdCC'
    DataSource = dsCentroCustoNivel3
    TabOrder = 12
  end
  object DBEdit3: TDBEdit [22]
    Tag = 1
    Left = 276
    Top = 136
    Width = 646
    Height = 21
    DataField = 'cNmCC'
    DataSource = dsCentroCustoNivel3
    TabOrder = 13
  end
  object MaskEdit6: TMaskEdit [23]
    Left = 106
    Top = 161
    Width = 65
    Height = 21
    EditMask = '######;1; '
    MaxLength = 6
    TabOrder = 14
    Text = '      '
    OnExit = MaskEdit6Exit
    OnKeyDown = MaskEdit6KeyDown
    OnKeyPress = MaskEdit6KeyPress
  end
  object DBEdit9: TDBEdit [24]
    Tag = 1
    Left = 176
    Top = 161
    Width = 125
    Height = 21
    DataField = 'cCNPJCPF'
    DataSource = dsTerceiro
    TabOrder = 15
  end
  object DBEdit10: TDBEdit [25]
    Tag = 1
    Left = 307
    Top = 161
    Width = 650
    Height = 21
    DataField = 'cNmTerceiro'
    DataSource = dsTerceiro
    TabOrder = 16
  end
  object MaskEdit1: TMaskEdit [26]
    Left = 106
    Top = 186
    Width = 64
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 17
    Text = '  /  /    '
  end
  object MaskEdit2: TMaskEdit [27]
    Left = 210
    Top = 186
    Width = 65
    Height = 21
    EditMask = '!99/99/9999;1;_'
    MaxLength = 10
    TabOrder = 18
    Text = '  /  /    '
  end
  inherited ImageList1: TImageList
    Left = 976
    Top = 40
  end
  object qryEmpresa: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdEmpresa'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdEmpresa'
      '      ,cSigla'
      '      ,cNmEmpresa'
      '  FROM Empresa'
      ' WHERE EXISTS(SELECT 1'
      '                FROM UsuarioEmpresa UE'
      '               WHERE UE.nCdEmpresa = Empresa.nCdEmpresa'
      '                 AND UE.nCdUsuario = :nCdUsuario)'
      'AND nCdEmpresa = :nCdEmpresa')
    Left = 792
    Top = 256
    object qryEmpresanCdEmpresa: TIntegerField
      FieldName = 'nCdEmpresa'
    end
    object qryEmpresacSigla: TStringField
      FieldName = 'cSigla'
      FixedChar = True
      Size = 5
    end
    object qryEmpresacNmEmpresa: TStringField
      FieldName = 'cNmEmpresa'
      Size = 50
    end
  end
  object dsEmpresa: TDataSource
    DataSet = qryEmpresa
    Left = 824
    Top = 256
  end
  object qryCentroCustoNivel3: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCC1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nCdCC2'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cCdCC,cNmCC'
      'FROM CentroCusto'
      'WHERE iNivel = 3 '
      'AND nCdCC1 = :nCdCC1'
      'AND nCdCC2 = :nCdCC2'
      'AND nCdCC = :nPK')
    Left = 792
    Top = 288
    object qryCentroCustoNivel3nCdCC: TIntegerField
      FieldName = 'nCdCC'
    end
    object qryCentroCustoNivel3cCdCC: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object qryCentroCustoNivel3cNmCC: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object dsCentroCustoNivel3: TDataSource
    DataSet = qryCentroCustoNivel3
    Left = 824
    Top = 288
  end
  object qryTerceiro: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdTerceiro'
      ',cCNPJCPF'
      ',cNmTerceiro'
      'FROM Terceiro           '
      'WHERE nCdTerceiro = :nPK')
    Left = 792
    Top = 320
    object qryTerceironCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryTerceirocCNPJCPF: TStringField
      FieldName = 'cCNPJCPF'
      Size = 14
    end
    object qryTerceirocNmTerceiro: TStringField
      FieldName = 'cNmTerceiro'
      Size = 50
    end
  end
  object dsTerceiro: TDataSource
    DataSet = qryTerceiro
    Left = 824
    Top = 320
  end
  object qryCentroCustoNivel1: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cCdCC,cNmCC'
      'FROM CentroCusto'
      'WHERE iNivel = 1'
      'AND nCdCC = :nPK')
    Left = 792
    Top = 352
    object IntegerField1: TIntegerField
      FieldName = 'nCdCC'
    end
    object StringField1: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object StringField2: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object dsCentroCustoNivel1: TDataSource
    DataSet = qryCentroCustoNivel1
    Left = 824
    Top = 352
  end
  object qryCentroCustoNivel2: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdCC1'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdCC, cCdCC,cNmCC'
      'FROM CentroCusto'
      'WHERE iNivel = 2  AND nCdCC1 = :nCdCC1'
      'AND nCdCC = :nPK')
    Left = 792
    Top = 384
    object IntegerField2: TIntegerField
      FieldName = 'nCdCC'
    end
    object StringField3: TStringField
      FieldName = 'cCdCC'
      FixedChar = True
      Size = 8
    end
    object StringField4: TStringField
      FieldName = 'cNmCC'
      Size = 50
    end
  end
  object dsCentroCustoNivel2: TDataSource
    DataSet = qryCentroCustoNivel2
    Left = 824
    Top = 384
  end
  object qryUnidadeNegocio: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPk'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'select * from UnidadeNegocio where nCdUnidadeNegocio = :nPk')
    Left = 792
    Top = 416
    object qryUnidadeNegocionCdUnidadeNegocio: TIntegerField
      FieldName = 'nCdUnidadeNegocio'
    end
    object qryUnidadeNegociocNmUnidadeNegocio: TStringField
      FieldName = 'cNmUnidadeNegocio'
      Size = 50
    end
  end
  object dsUnidadeNegocio: TDataSource
    DataSet = qryUnidadeNegocio
    Left = 824
    Top = 416
  end
  object qryLoja: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'nCdUsuario'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT cNmLoja'
      '   FROM Loja'
      ' WHERE nCdLoja = :nPK'
      
        '      AND EXISTS(SELECT 1 FROM UsuarioLoja UL WHERE UL.nCdLoja =' +
        ' Loja.nCdLoja AND UL.nCdUsuario = :nCdUsuario)'
      '')
    Left = 795
    Top = 450
    object qryLojacNmLoja: TStringField
      FieldName = 'cNmLoja'
      Size = 50
    end
  end
  object dsLoja: TDataSource
    DataSet = qryLoja
    Left = 826
    Top = 449
  end
end
