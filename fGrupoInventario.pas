unit fGrupoInventario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fCadastro_Template, ImgList, DB, ADODB, ComCtrls, ToolWin,
  ExtCtrls, StdCtrls, Mask, DBCtrls;

type
  TfrmGrupoInventario = class(TfrmCadastro_Padrao)
    qryMasternCdGrupoInventario: TIntegerField;
    qryMastercNmGrupoInventario: TStringField;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    procedure FormCreate(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure qryMasterBeforePost(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmGrupoInventario: TfrmGrupoInventario;

implementation

{$R *.dfm}

procedure TfrmGrupoInventario.FormCreate(Sender: TObject);
begin
  inherited;
  cNmTabelaMaster   := 'GRUPOINVENTARIO' ;
  nCdTabelaSistema  := 67 ;
  nCdConsultaPadrao := 153 ;
  bCodigoAutomatico := False ;

end;

procedure TfrmGrupoInventario.btIncluirClick(Sender: TObject);
begin
  inherited;

  DBEdit1.SetFocus ;
  
end;

procedure TfrmGrupoInventario.qryMasterBeforePost(DataSet: TDataSet);
begin
  if (DBEdit1.Text = '') then
  begin
      MensagemAlerta('Informe o c�digo do grupo.') ;
      DbEdit1.SetFocus ;
      abort ;
  end ;

  if (DBEdit2.Text = '') then
  begin
      MensagemAlerta('Informe a descri��o do grupo.') ;
      DbEdit2.SetFocus ;
      abort ;
  end ;

  inherited;


end;

initialization
    RegisterClass(TfrmGrupoInventario) ;

end.
