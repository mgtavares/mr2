inherited frmConsultaItemPedidoAberto_SomenteItens: TfrmConsultaItemPedidoAberto_SomenteItens
  Left = 5
  Top = 295
  Width = 1240
  Height = 358
  Caption = 'Consulta de Itens'
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 1224
    Height = 293
  end
  inherited ToolBar1: TToolBar
    Width = 1224
  end
  object DBGridEh2: TDBGridEh [2]
    Left = 0
    Top = 29
    Width = 1224
    Height = 293
    Align = alClient
    DataSource = DataSource2
    Flat = True
    FooterColor = clWindow
    FooterFont.Charset = DEFAULT_CHARSET
    FooterFont.Color = clWindowText
    FooterFont.Height = -11
    FooterFont.Name = 'Tahoma'
    FooterFont.Style = []
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
    UseMultiTitle = True
    Columns = <
      item
        EditButtons = <>
        FieldName = 'nCdItemPedido'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'cNmTipoItemPed'
        Footers = <>
        Width = 201
      end
      item
        EditButtons = <>
        FieldName = 'nCdProduto'
        Footers = <>
      end
      item
        EditButtons = <>
        FieldName = 'nQtde'
        Footers = <>
        Width = 89
      end
      item
        EditButtons = <>
        FieldName = 'cSiglaUnidadeMedida'
        Footers = <>
        Width = 26
      end
      item
        EditButtons = <>
        FieldName = 'cNmItem'
        Footers = <>
        Width = 731
      end>
  end
  object qryItemPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'nCdRecebimento'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'SELECT nCdItemPedido'
      '      ,cNmTipoItemPed'
      '      ,nCdProduto'
      '      ,cNmItem'
      '      ,(nQtdePed - nQtdeExpRec - nQtdeCanc) nQtde'
      '      ,cSiglaUnidadeMedida'
      '  FROM ItemPedido'
      
        '       INNER JOIN TipoItemPed ON TipoItemPed.nCdTipoItemPed = It' +
        'emPedido.nCdTipoItemPed'
      ' WHERE (nQtdePed - nQtdeExpRec - nQtdeCanc) > 0'
      '   AND nCdItemPedidoPai IS NULL'
      '   AND nCdPedido = :nPK'
      '   AND NOT EXISTS(SELECT 1'
      '                    FROM ItemRecebimento'
      '                   WHERE nCdRecebimento = :nCdRecebimento'
      
        '                     AND ItemRecebimento.nCdItemPedido  = ItemPe' +
        'dido.nCdItemPedido)')
    Left = 240
    Top = 80
    object qryItemPedidonCdItemPedido: TAutoIncField
      DisplayLabel = 'Item|ID'
      FieldName = 'nCdItemPedido'
      ReadOnly = True
    end
    object qryItemPedidocNmTipoItemPed: TStringField
      DisplayLabel = 'Item|Tipo de Item'
      FieldName = 'cNmTipoItemPed'
      Size = 50
    end
    object qryItemPedidonCdProduto: TIntegerField
      DisplayLabel = 'Item|C'#243'digo'
      FieldName = 'nCdProduto'
    end
    object qryItemPedidocNmItem: TStringField
      DisplayLabel = 'Item|Descri'#231#227'o'
      FieldName = 'cNmItem'
      Size = 150
    end
    object qryItemPedidonQtde: TBCDField
      DisplayLabel = 'Quantidade|em aberto'
      FieldName = 'nQtde'
      ReadOnly = True
      Precision = 14
    end
    object qryItemPedidocSiglaUnidadeMedida: TStringField
      DisplayLabel = 'Quantidade|UM'
      FieldName = 'cSiglaUnidadeMedida'
      FixedChar = True
      Size = 2
    end
  end
  object DataSource2: TDataSource
    DataSet = qryItemPedido
    Left = 200
    Top = 80
  end
end
