unit fFollowUP_Ocorrencia;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, fProcesso_Padrao, ImgList, ComCtrls, ToolWin, ExtCtrls,
  cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxDataStorage,
  cxEdit, DB, cxDBData, StdCtrls, DBCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxControls, cxGridCustomView, cxGrid, cxContainer, cxTextEdit, ADODB,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore,
  dxPSContainerLnk, ER2Excel;

type
  TfrmFollowUP_Ocorrencia = class(TfrmProcesso_Padrao)
    GroupBox1: TGroupBox;
    rgModoExibicao: TRadioGroup;
    GroupBox4: TGroupBox;
    Label29: TLabel;
    Label24: TLabel;
    cxTextEdit3: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    GroupBox2: TGroupBox;
    cxGridRequisicao: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGridDBTableView3DBColumn_nCdOcorrencia: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_dDtPrazoAtendIni: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_nCdLoja: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cNmTipoOcorrencia: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cNmSolicitante: TcxGridDBColumn;
    cxGridDBTableView3DBColumn_cNmSituacao: TcxGridDBColumn;
    cxGridLevel3: TcxGridLevel;
    GroupBox3: TGroupBox;
    cxGridOcorrencia: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBTableView1dDtFollowUp: TcxGridDBColumn;
    cxGridDBTableView1cNmUsuario: TcxGridDBColumn;
    cxGridDBTableView1cOcorrenciaResum: TcxGridDBColumn;
    cxGridDBTableView1dDtProxAcao: TcxGridDBColumn;
    cxGridDBTableView1cOcorrencia: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    DBMemo1: TDBMemo;
    qryFollowUp: TADOQuery;
    qryFollowUpdDtFollowUp: TDateTimeField;
    qryFollowUpcNmUsuario: TStringField;
    qryFollowUpcOcorrenciaResum: TStringField;
    qryFollowUpdDtProxAcao: TDateTimeField;
    qryFollowUpcOcorrencia: TMemoField;
    dsFollowUp: TDataSource;
    SP_FOLLOWUP_OCORRENCIA: TADOStoredProc;
    dsSP_FOLLOWUP_OCORRENCIA: TDataSource;
    SP_FOLLOWUP_OCORRENCIAnCdOcorrencia: TIntegerField;
    SP_FOLLOWUP_OCORRENCIAdDtPrazoAtendIni: TDateTimeField;
    SP_FOLLOWUP_OCORRENCIAdDtPrazoAtendFinal: TDateTimeField;
    SP_FOLLOWUP_OCORRENCIAnCdLoja: TIntegerField;
    SP_FOLLOWUP_OCORRENCIAcNmLoja: TStringField;
    SP_FOLLOWUP_OCORRENCIAcNmTipoOcorrencia: TStringField;
    SP_FOLLOWUP_OCORRENCIAcNmSituacao: TStringField;
    SP_FOLLOWUP_OCORRENCIAcStatus: TStringField;
    SP_FOLLOWUP_OCORRENCIAcNmSolicitante: TStringField;
    SP_FOLLOWUP_OCORRENCIAcUltimoAcomp: TStringField;
    btFollowUP: TToolButton;
    btMarcarCritico: TToolButton;
    btFiltros: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    cxGridDBTableView3DBColumn_dDtPrazoAtendFinal: TcxGridDBColumn;
    qryAux: TADOQuery;
    ToolButton4: TToolButton;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxCustomContainerReportLink;
    ER2Excel1: TER2Excel;
    procedure btFiltrosClick(Sender: TObject);
    procedure btFollowUPClick(Sender: TObject);
    procedure cxGridDBTableView3CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure btMarcarCriticoClick(Sender: TObject);
    procedure cxGridDBTableView3StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
    procedure ToolButton4Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rgModoExibicaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmFollowUP_Ocorrencia: TfrmFollowUP_Ocorrencia;

implementation

{$R *.dfm}

uses fFollowUP_Ocorrencia_Filtro,fFollowUP_Ocorrencia_NovoFollowUP,fMenu;

procedure TfrmFollowUP_Ocorrencia.btFiltrosClick(Sender: TObject);
var
  objForm : TfrmFollowUp_Ocorrencia_Filtro;
begin
  inherited;

  try
      objForm := TfrmFollowUp_Ocorrencia_Filtro.Create(Self);

      showForm(objForm,false);

      { -- modo de exibi��o -- }
      case (rgModoExibicao.ItemIndex) of
          0 : SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@cTipoExibicao').Value := 'T'; //todos
          1 : SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@cTipoExibicao').Value := 'A'; //atraso
          2 : SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@cTipoExibicao').Value := 'C'; //cr�tico
      end;

          SP_FOLLOWUP_OCORRENCIA.Close;
          SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@nCdUsuario').Value        := frmMenu.nCdUsuarioLogado;
          SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@nCdEmpresa').Value        := frmMenu.ConvInteiro(objForm.edtEmpresa.Text);
          SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@nCdLoja').Value           := frmMenu.ConvInteiro(objForm.er2LkpLoja.Text);
          SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@nCdTipoOcorrencia').Value := frmMenu.ConvInteiro(objForm.er2LkpTipoOcorrencia.Text);
          SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@dDtPrevAtendIni').Value   := frmMenu.ConvData(objForm.mskDtAtendInicial.Text);
          SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@dDtPrevAtendFim').Value   := frmMenu.ConvData(objForm.mskDtAtendFinal.Text);
          SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@dDtOcorrenciaIni').Value  := frmMenu.ConvData(objForm.mskDtOcorrenciaInicial.Text);
          SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@dDtOcorrenciaFim').Value  := frmMenu.ConvData(objForm.mskDtOcorrenciaFinal.Text);
          SP_FOLLOWUP_OCORRENCIA.Open;
  finally
      FreeAndNil(objForm);
  end;

end;

procedure TfrmFollowUP_Ocorrencia.btFollowUPClick(Sender: TObject);
var
  objForm : TfrmFollowUP_Ocorrencia_NovoFollowUP;
begin
  inherited;

  if (SP_FOLLOWUP_OCORRENCIA.Eof) then
      Exit;

  objForm               := TfrmFollowUP_Ocorrencia_NovoFollowUP.Create(Self);
  objForm.nCdOcorrencia := SP_FOLLOWUP_OCORRENCIAnCdOcorrencia.Value;

  showForm(objForm, True);

  PosicionaQuery(qryFollowUp,SP_FOLLOWUP_OCORRENCIAnCdOcorrencia.AsString);

end;

procedure TfrmFollowUP_Ocorrencia.cxGridDBTableView3CellClick(
  Sender: TcxCustomGridTableView;
  ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
  AShift: TShiftState; var AHandled: Boolean);
begin
  inherited;

  if not SP_FOLLOWUP_OCORRENCIA.Eof then
      PosicionaQuery(qryFollowUp,SP_FOLLOWUP_OCORRENCIAnCdOcorrencia.AsString);

end;

procedure TfrmFollowUP_Ocorrencia.btMarcarCriticoClick(Sender: TObject);
begin
  inherited;

  if (SP_FOLLOWUP_OCORRENCIA.Eof) then
      Exit;

  if (SP_FOLLOWUP_OCORRENCIAcNmSituacao.Value = 'Cr�tico') then
  begin
      ShowMessage('Ocorr�ncia j� est� em situa��o cr�tica.');
      Exit;
  end;

  case MessageDlg('Confirma a altera��o da situa��o desta ocorr�ncia ?',mtConfirmation,[mbYes,mbNo],0) of
      mrNo: Exit;
  end;

  try
      frmMenu.Connection.BeginTrans;

      qryAux.Close;
      qryAux.SQL.Clear;
      qryAux.SQL.Add('UPDATE Ocorrencia SET cFlgCritico = 1 WHERE nCdOcorrencia = ' + SP_FOLLOWUP_OCORRENCIAnCdOcorrencia.AsString);
      qryAux.ExecSQL;

      frmMenu.Connection.CommitTrans;

      ShowMessage('Situa��o da ocorr�ncia alterada com sucesso.');

      qryAux.Close;

      SP_FOLLOWUP_OCORRENCIA.Requery();
  except
      frmMenu.Connection.RollbackTrans;
      ShowMessage('Erro no processamento.');
      Raise;
  end;
end;

procedure TfrmFollowUP_Ocorrencia.cxGridDBTableView3StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; out AStyle: TcxStyle);
begin
  inherited;

  if (AItem = cxGridDBTableView3DBColumn_nCdOcorrencia) then
  begin
      try
          if (ARecord.Values[6] <> 'Normal') then
              AStyle := frmMenu.LinhaAmarela;

          if (ARecord.Values[6] = 'Cr�tico') then
              AStyle := frmMenu.LinhaVermelha;
      except
      end;
  end;
end;

procedure TfrmFollowUP_Ocorrencia.ToolButton4Click(Sender: TObject);
begin
  inherited;

  dxComponentPrinter1.Preview(True,nil);
end;

procedure TfrmFollowUP_Ocorrencia.FormShow(Sender: TObject);
begin
  inherited;
  btFiltros.Click;
end;

procedure TfrmFollowUP_Ocorrencia.rgModoExibicaoClick(Sender: TObject);
begin
  inherited;

  { -- modo de exibi��o -- }
  case (rgModoExibicao.ItemIndex) of
      0 : SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@cTipoExibicao').Value := 'T'; //todos
      1 : SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@cTipoExibicao').Value := 'A'; //atraso
      2 : SP_FOLLOWUP_OCORRENCIA.Parameters.ParamByName('@cTipoExibicao').Value := 'C'; //cr�tico
  end;

  { -- atualiza consulta -- }
  SP_FOLLOWUP_OCORRENCIA.Requery;
end;

initialization
    RegisterClass(TfrmFollowUP_Ocorrencia) ;

end.
