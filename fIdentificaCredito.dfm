inherited frmIdentificaCredito: TfrmIdentificaCredito
  Caption = 'Identifica'#231#227'o de Cr'#233'dito'
  OldCreateOrder = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited ToolBar1: TToolBar
    ButtonWidth = 100
    inherited ToolButton1: TToolButton
      Caption = '&Atualizar Tela'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 100
    end
    inherited ToolButton2: TToolButton
      Left = 108
    end
  end
  object cxGrid1: TcxGrid [2]
    Left = 0
    Top = 29
    Width = 784
    Height = 435
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Consolas'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object cxGrid1DBTableView1: TcxGridDBTableView
      OnDblClick = cxGrid1DBTableView1DblClick
      DataController.DataSource = dsLanctos
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      NavigatorButtons.ConfirmDelete = False
      NavigatorButtons.First.Visible = True
      NavigatorButtons.PriorPage.Visible = True
      NavigatorButtons.Prior.Visible = True
      NavigatorButtons.Next.Visible = True
      NavigatorButtons.NextPage.Visible = True
      NavigatorButtons.Last.Visible = True
      NavigatorButtons.Insert.Visible = True
      NavigatorButtons.Delete.Visible = True
      NavigatorButtons.Edit.Visible = True
      NavigatorButtons.Post.Visible = True
      NavigatorButtons.Cancel.Visible = True
      NavigatorButtons.Refresh.Visible = True
      NavigatorButtons.SaveBookmark.Visible = True
      NavigatorButtons.GotoBookmark.Visible = True
      NavigatorButtons.Filter.Visible = True
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsView.GridLineColor = clSilver
      OptionsView.GridLines = glVertical
      OptionsView.GroupByBox = False
      Styles.Content = frmMenu.FonteSomenteLeitura
      object cxGrid1DBTableView1nCdLanctoFin: TcxGridDBColumn
        Caption = 'Lancto'
        DataBinding.FieldName = 'nCdLanctoFin'
        Width = 96
      end
      object cxGrid1DBTableView1nCdBanco: TcxGridDBColumn
        Caption = 'Banco'
        DataBinding.FieldName = 'nCdBanco'
        Width = 71
      end
      object cxGrid1DBTableView1cAgencia: TcxGridDBColumn
        Caption = 'Ag'#234'ncia'
        DataBinding.FieldName = 'cAgencia'
        Width = 90
      end
      object cxGrid1DBTableView1nCdConta: TcxGridDBColumn
        Caption = 'N'#250'mero Conta'
        DataBinding.FieldName = 'nCdConta'
        Width = 110
      end
      object cxGrid1DBTableView1dDtExtrato: TcxGridDBColumn
        Caption = 'Data Extrato Cr'#233'dito'
        DataBinding.FieldName = 'dDtExtrato'
        Width = 161
      end
      object cxGrid1DBTableView1nValLancto: TcxGridDBColumn
        Caption = 'Valor Lan'#231'amento'
        DataBinding.FieldName = 'nValLancto'
        Width = 151
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object qryLanctos: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nPK'
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    SQL.Strings = (
      'SELECT nCdLanctoFin'
      '      ,ContaBancaria.nCdBanco'
      '      ,ContaBancaria.cAgencia'
      '      ,ContaBancaria.nCdConta'
      '      ,LanctoFin.dDtExtrato'
      '      ,LanctoFin.nValLancto'
      '  FROM LanctoFin'
      
        '       INNER JOIN ContaBancaria ON ContaBancaria.nCdContaBancari' +
        'a = LanctoFin.nCdContaBancaria'
      ' WHERE cFlgCredNaoIdent = 1'
      '   AND nCdTerceiroDep   IS NULL'
      '   AND nCdEmpresa       = :nPK'
      '   AND cFlgConciliado   = 1'
      '   AND nValLancto       > 0'
      ' ORDER BY dDtExtrato DESC')
    Left = 184
    Top = 128
    object qryLanctosnCdLanctoFin: TAutoIncField
      FieldName = 'nCdLanctoFin'
      ReadOnly = True
    end
    object qryLanctosnCdBanco: TIntegerField
      FieldName = 'nCdBanco'
    end
    object qryLanctoscAgencia: TIntegerField
      FieldName = 'cAgencia'
    end
    object qryLanctosnCdConta: TStringField
      FieldName = 'nCdConta'
      FixedChar = True
      Size = 15
    end
    object qryLanctosdDtExtrato: TDateTimeField
      FieldName = 'dDtExtrato'
    end
    object qryLanctosnValLancto: TBCDField
      FieldName = 'nValLancto'
      DisplayFormat = '#,##0.00'
      Precision = 12
      Size = 2
    end
  end
  object dsLanctos: TDataSource
    DataSet = qryLanctos
    Left = 224
    Top = 128
  end
  object qryAux: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    Left = 320
    Top = 120
  end
end
