inherited frmNovo_FollowUp: TfrmNovo_FollowUp
  Width = 798
  Height = 368
  Caption = 'FollowUp'
  OldCreateOrder = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited Image1: TImage
    Width = 790
    Height = 308
  end
  object Label1: TLabel [1]
    Left = 11
    Top = 48
    Width = 101
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ocorr'#234'ncia Resumida'
  end
  object Label2: TLabel [2]
    Left = 21
    Top = 72
    Width = 91
    Height = 13
    Alignment = taRightJustify
    Caption = 'Data Pr'#243'xima A'#231#227'o'
  end
  object Label3: TLabel [3]
    Left = 8
    Top = 120
    Width = 104
    Height = 13
    Alignment = taRightJustify
    Caption = 'Ocorr'#234'ncia Detalhada'
  end
  object Label4: TLabel [4]
    Left = 15
    Top = 96
    Width = 97
    Height = 13
    Alignment = taRightJustify
    Caption = 'Previs'#227'o de Entrega'
  end
  object Label5: TLabel [5]
    Left = 230
    Top = 96
    Width = 6
    Height = 13
    Alignment = taRightJustify
    Caption = #224
  end
  inherited ToolBar1: TToolBar
    Width = 790
    ButtonWidth = 66
    inherited ToolButton1: TToolButton
      Caption = 'Gravar'
      OnClick = ToolButton1Click
    end
    inherited ToolButton3: TToolButton
      Left = 66
    end
    inherited ToolButton2: TToolButton
      Left = 74
    end
  end
  object MaskEdit1: TMaskEdit [7]
    Left = 120
    Top = 64
    Width = 95
    Height = 21
    EditMask = '##/##/####;1;_'
    MaxLength = 10
    TabOrder = 2
    Text = '  /  /    '
  end
  object Memo1: TMemo [8]
    Left = 120
    Top = 112
    Width = 657
    Height = 201
    TabOrder = 5
  end
  object Edit1: TEdit [9]
    Left = 120
    Top = 40
    Width = 657
    Height = 21
    MaxLength = 50
    TabOrder = 1
  end
  object MaskEdit2: TMaskEdit [10]
    Left = 120
    Top = 88
    Width = 95
    Height = 21
    EditMask = '##/##/####;1;_'
    MaxLength = 10
    TabOrder = 3
    Text = '  /  /    '
  end
  object MaskEdit3: TMaskEdit [11]
    Left = 248
    Top = 88
    Width = 95
    Height = 21
    EditMask = '##/##/####;1;_'
    MaxLength = 10
    TabOrder = 4
    Text = '  /  /    '
  end
  object qryInsereFollow: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'cOcorrenciaResum'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'cOcorrencia'
        DataType = ftString
        Size = 1
        Value = '0'
      end
      item
        Name = 'dDtProxAcao'
        DataType = ftString
        Size = 1
        Value = '0'
      end>
    SQL.Strings = (
      'DECLARE @cOcorrenciaResum  varchar(50)'
      '       ,@cOcorrencia       varchar(Max)'
      '       ,@dDtProxAcao       varchar(10)'
      ''
      'SET @cOcorrenciaResum =:cOcorrenciaResum'
      'SET @cOcorrencia      =:cOcorrencia'
      'SET @dDtProxAcao      =:dDtProxAcao'
      ''
      'INSERT INTO FollowUp (nCdPedido'
      '                     ,dDtFollowUp'
      '                     ,nCdUsuario'
      '                     ,cOcorrenciaResum'
      '                     ,cOcorrencia'
      '                     ,dDtProxAcao)'
      '               SELECT nCdPedido'
      '                     ,dDtFollowUp'
      '                     ,nCdUsuario'
      '                     ,@cOcorrenciaResum'
      '                     ,@cOcorrencia'
      '                     ,Convert(datetime,@dDtProxAcao,103)'
      '                 FROM #TempFollowUP')
    Left = 208
    Top = 168
  end
  object qryTempFollowUP: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT * '
      '  FROM #TempFollowUP')
    Left = 192
    Top = 136
    object qryTempFollowUPnCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryTempFollowUPdDtFollowUp: TDateTimeField
      FieldName = 'dDtFollowUp'
    end
    object qryTempFollowUPnCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryTempFollowUPcOcorrenciaResum: TStringField
      FieldName = 'cOcorrenciaResum'
      Size = 50
    end
    object qryTempFollowUPcOcorrencia: TMemoField
      FieldName = 'cOcorrencia'
      BlobType = ftMemo
    end
    object qryTempFollowUPdDtProxAcao: TDateTimeField
      FieldName = 'dDtProxAcao'
    end
  end
  object qryFollow: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'SELECT TOP 0 *'
      'FROM FollowUp')
    Left = 320
    Top = 152
    object qryFollownCdFollowUp: TAutoIncField
      FieldName = 'nCdFollowUp'
      ReadOnly = True
    end
    object qryFollownCdPedido: TIntegerField
      FieldName = 'nCdPedido'
    end
    object qryFollownCdTitulo: TIntegerField
      FieldName = 'nCdTitulo'
    end
    object qryFollowdDtFollowUp: TDateTimeField
      FieldName = 'dDtFollowUp'
    end
    object qryFollownCdUsuario: TIntegerField
      FieldName = 'nCdUsuario'
    end
    object qryFollowcOcorrenciaResum: TStringField
      FieldName = 'cOcorrenciaResum'
      Size = 50
    end
    object qryFollowcOcorrencia: TMemoField
      FieldName = 'cOcorrencia'
      BlobType = ftMemo
    end
    object qryFollowdDtProxAcao: TDateTimeField
      FieldName = 'dDtProxAcao'
    end
    object qryFollowcFlgOK: TIntegerField
      FieldName = 'cFlgOK'
    end
    object qryFollownCdTerceiro: TIntegerField
      FieldName = 'nCdTerceiro'
    end
    object qryFollownCdCheque: TIntegerField
      FieldName = 'nCdCheque'
    end
  end
  object qryAlteraPrevisaoPedido: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <
      item
        Name = 'nCdPedido'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtInicial'
        Size = -1
        Value = Null
      end
      item
        Name = 'dDtFinal'
        Size = -1
        Value = Null
      end>
    SQL.Strings = (
      'DECLARE @nCdPedido  int'
      '       ,@dDtInicial varchar(10)'
      '       ,@dDtFinal   varchar(10)'
      ''
      'SET @nCdPedido  = :nCdPedido'
      'SET @dDtInicial = :dDtInicial'
      'SET @dDtFinal   = :dDtFinal'
      ''
      'UPDATE Pedido'
      '   SET dDtPrevEntIni = Convert(datetime,@dDtInicial,103)'
      '      ,dDtPrevEntFim = Convert(datetime,@dDtFinal,103)'
      ' WHERE nCdPedido     = @nCdPedido'
      ''
      'UPDATE ItemPedido'
      '   SET dDtEntregaIni = Convert(datetime,@dDtInicial,103)'
      '      ,dDtEntregaFim = Convert(datetime,@dDtFinal,103)'
      ' WHERE nCdPedido     = @nCdPedido'
      ''
      'EXEC SP_ENVIA_PEDIDO_TEMPREGISTRO @nCdPedido')
    Left = 440
    Top = 96
  end
  object qryPreparaTemp: TADOQuery
    Connection = frmMenu.Connection
    Parameters = <>
    SQL.Strings = (
      'IF (OBJECT_ID('#39'tempdb..#TempFollowUP'#39') IS NULL)'
      'BEGIN'
      '    CREATE TABLE #TempFollowUP (nCdPedido        int'
      '                               ,dDtFollowUp      datetime'
      '                               ,nCdUsuario       int'
      '                               ,cOcorrenciaResum varchar(50)'
      '                               ,cOcorrencia      text'
      '                               ,dDtProxAcao      datetime)'
      'END'
      ''
      'TRUNCATE TABLE #TempFollowUP')
    Left = 176
    Top = 216
  end
end
